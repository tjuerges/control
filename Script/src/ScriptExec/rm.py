#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# rhiriart  2006-11-13  created
#

import maciErrType
from configurator import Configurator
from util import LoggerAdaptor, Singleton
from exceptions import Exception

def get_component(container, compname):
    """Get a reference to an ACS component.
            
    A look at the ContainerServices implementation in 
    ACS/LGPL/CommonSoftware/acspy/src/Acspy/Servants/ContainerServices.py
    shows that ContainerServices.getComponent() performs the following
    operations:
    1. dynamically import the stubs for the CORBA object
    2. get the CORBA object from the Manager
    3. narrow the CORBA object to the stub
    Only step 2 will throw exceptions. Steps 1 and 3 will fail silently.
    TODO: Something should be done about this.
        
    On the other hand, another look at ACS/LGPL/CommonSoftware/maciidl/ws/idl/maci.idl
    shows that we need to be prepared for four exceptions in the call to the
    Manager:
    * maciErrType::NoPermissionEx
    * maciErrType::CannotGetComponentEx
    * maciErrType::ComponentNotAlreadyActivatedEx
    * maciErrType::ComponentConfigurationNotFoundEx
    
    Parameters:
      container - ContainerServices.
      compname  - Component name.
      
    Raises:
      ResourceErrorEx - ACS Error exception.
    """
    from ScriptExecutorExceptionsImpl import ResourceErrorExImpl
    logger = container.getLogger()
    logger.logDebug("Getting component '" + compname + "'")
    try:
        compref = container.getComponent(compname)
    except maciErrType.NoPermissionEx, ex:
        s = "No permission exception when trying to get component '"
        s = s + compname + ": " + str(ex)
        logger.logCritical(s)
        helper = ResourceErrorExImpl(exception=ex)
        raise helper
    except maciErrType.CannotGetComponentEx, ex:
        logger.logCritical("Cannot get component '" + compname + "': " + str(ex))
        helper = ResourceErrorExImpl(exception=ex)
        raise helper
    except maciErrType.ComponentNotAlreadyActivatedEx, ex:
        s = "Error when trying to get component '" + compname
        s = s + "'. Component has not been activated at the requested time: "
        s = s + str(ex) 
        logger.logCritical(s)
        helper = ResourceErrorExImpl(exception=ex)
        raise helper
    except maciErrType.ComponentConfigurationNotFoundEx:
        s = "Error when trying to get component '" + compname
        s = s + "'. The component configuration could not be found: " + str(ex)
        logger.logCritical(s)
        helper = ResourceErrorExImpl(exception=ex)
        raise helper
        
    return compref

def release_component(container, compname):
    logger = container.getLogger()
    logger.logDebug("Releasing component '" + compname + "'")
    container.releaseComponent(compname)

class NoSuchResource(Exception):
    def __init__(self, resname, reslist):
        self.resname = resname
        self.reslist = reslist
    def __repr__(self):
        s = "Resouce " + self.resname + "is not in resource list. Resource list: " + str(self.reslist) 

class ResourceManager(Singleton):
    """Internal class to be used by the ScriptExecutor.
    
    The ResourceManager provides a way to get the logic of getting and releasing
    resources away from the component. A Singleton is provided, which should be
    initialized with a given Configuration object and its acquire_resources() method
    called once in the component implementation. After this, other classes in the
    component implementation collaboration can create a ResourceManager (there is only
    one because it is a Singleton) an use it to access the acquired resources. As part
    of the compoment shutdown process, the ResourceManager release_resources() should be
    called.

    This allows to implement a component with classes that doesn't have any
    dependency with ACS. They are "plain old" classes, which can be implemented and
    tested without having ACS running. Even if it is necessary to have ACS running,
    this approach facilitates testing, as the resources that a component class needs
    can be replaced by "mock" objects (plain classes or test components), allowing to
    test a component without using other components or subsystems.
    """
    initialized = []

    def __init__(self, container=None, conf=None, logger=None):
        """Constructor.

        Parameters:

        container - A ContainerServices object or a string. A ContainerServices object should
                    be passed in the initialization of the ResourceManager, i.e., when this
                    singleton is first created. If a string is passed, then there are two cases:
                    (1) the string corresponds to the name of the container that was passed
                    in the first initialization of the ResourceManager, and (2) the string
                    doesn't correspond to any previously passed container. In the first case
                    the ResourceManager instance will be the same that was previously
                    created for the container, in the second the instance will be created
                    assuming that ACS is not running.
                    In the latter case resource management should be done through add_resource() and
                    get_resource().
                    If no container is passed, the name of the instance is obtained through the
                    bootstrap module. If there is already an instance with the same name, then it is
                    returned. Otherwise a new instance is created using a PySimpleClient.
        conf - A Configurator instance.
        logger - The logger.

        Examples:

        All this explanation is somewhat complicated, but the use of this class
        is simple for the use cases it was designed:

        1) Using it in a Python component:

        In the Python main component implementation:

          # In Python the ContainerServices is one of the component base class
          rm = ResourceManager(self, conf, self.getLogger())

        In one of the classes that implement the component

          rm = ResourceManager(comp_name) # if the class knows the comp. name or
          rm = ResourceManager(container) # if the class knows the ContainerServices

        2) In a test case without ACS:

          rm = ResourceManager('foo')

        3) In a test case with ACS:

          rm = ResourceManager()

          Because in this case there was no ResourceManager previously created, the
          class creates a PySimpleClient internally.

        4) In CCL objects running inside the ScriptExecutor:

          rm = ResourceManager()

        In this case the class gets its 'context' name from the bootstrap module.
        Because there was a previously created instance with the same 'context' name,
        this instance is returned, already containing the ContainerServices.

        5) CCL objects tested outside the ScriptExecutor:

          This is the same case as (3).
        """

        if not logger and container and type(container) != str:
            self.logger = container.getLogger()
        elif not logger:
            self.logger = LoggerAdaptor('ResourceManager')
        else:
            self.logger = logger
        
        # self.logger.logInfo('ResourceManager.initialized = ' + str(ResourceManager.initialized))
        if container == None:
            import bootstrap
            name = bootstrap.getContextName()
        else:
            if type(container) == str:
                name = container
            else:
                name = container.getName()
        
        # self.logger.logInfo("RM context: " + name)
                    
        if not name in ResourceManager.initialized:
            
            # self.logger.logInfo("Initializing RM for context '" + name + "'")
        
            if container != None and type(container) != str:
                self.container = container
            elif container != None and type(container) == str:
                self.container = None
            else:
                from Acspy.Clients.SimpleClient import PySimpleClient
                self.container = PySimpleClient(name)
        
            self.configurator = conf
        
            self.resources = {}
            self.temp_resources = []

            ResourceManager.initialized.append(name)
                    
    def __del__(self):
        ResourceManager.initialized.remove(self.container.getName())

    def get_component(self, compname):
        """Internal function to get a reference to an ACS component.
        """
        if self.container:
            return get_component(self.container, compname)
    
    def release_component(self, compname):
        if self.container:
            release_component(self.container, compname)
    
    def acquire_resources(self, get_comp_function=None):
        """Acquire pre-configured resources.
        This function should be called just once by the ScriptExecutor
        component.
        """
        self.logger.logDebug('Accessing resources...')
        if self.configurator:
            for c in self.configurator.get_configurations():
                if c.get_parameter('startup') == 'yes':
                    name = c.get_parameter('name')
                    comp_name = c.get_parameter('compname')
                    self.logger.logDebug("Accessing component '%s' (%s)" % (name, comp_name))
                    if not get_comp_function:
                        self.resources[name] = self.get_component(comp_name)
                    else:
                        self.resources[name] = apply(get_comp_function, (comp_name,))
    
    def release_resources(self):
        """Release pre-configured resources.
        This function should be called just once by the ScriptExecutor
        component.
        """
        self.logger.logDebug('Releasing resources...')
        if self.configurator:
            for c in self.configurator.get_configurations():
                if c.get_parameter('startup') == 'yes':
                    name = c.get_parameter('name')
                    comp_name = c.get_parameter('compname')
                    self.logger.logDebug("Releasing component '%s' (%s)" % (name, comp_name))
                    self.release_component(comp_name)
        ResourceManager.initialized.remove(self.container.getName())    

    def get_resource(self, resname):
        if not resname in self.resources.keys():
            raise NoSuchResource, NoSuchResource(resname, self.resources.keys())
        return self.resources[resname]

    def add_resource(self, resname, res, temporary=False):
        self.resources[resname] = res
        if temporary and not resname in self.temp_resources:
            self.temp_resources.append(resname)
        # self.logger.logInfo("add_resource: " + str(self.temp_resources))
    
    def release_temp_resources(self):
        # self.logger.logInfo("release_temp_resources.")
        for comp_name in self.temp_resources:
            # self.logger.logInfo("releasing '"+comp_name+"'")
            self.release_component(comp_name)
            self.resources.pop(comp_name)
        self.temp_resources = []

    def release_temp_resource(self, comp_name):
        if not comp_name in self.temp_resources:
            return 
        self.release_component(comp_name)
        self.resources.pop(comp_name)
        self.temp_resources.remove(comp_name)

    def get_logger(self):
        return self.logger
    
    def get_startup_components_info(self):
        comps = []
        if self.configurator:
            for c in self.configurator.get_configurations():
                if c.get_parameter('startup') == 'yes':
                    comps.append((c.get_parameter('name'), c.get_parameter('compname')))
        return comps
    
    def get_container_services(self):
        return self.container

def getComponent(comp_name):
        rm = ResourceManager()
        try:
            comp = rm.get_resource(comp_name)
            return comp
        except NoSuchResource, ex:
            pass
        comp = rm.get_container_services().getComponent(comp_name)
        rm.add_resource(comp_name, comp, True)
        return comp

def getDynamicComponent(name, idl, impl, cont):
        rm = ResourceManager()
        try:
            comp = rm.get_resource(idl)
            return comp
        except NoSuchResource, ex:
            pass
        comp = rm.get_container_services().getDynamicComponent(name,
                                                               idl,
                                                               impl,
                                                               cont)
        rm.add_resource(comp._get_name(), comp, True)
        return comp

def getCollocatedComp(comp_spec, mark_as_default, target_comp):
        '''
        See Acspy.Servants.ContainerServices for a description of
        this function and its parameters.
        '''
        rm = ResourceManager()
        try:
            comp = rm.get_resource(comp_spec.component_name)
            return comp
        except NoSuchResource, ex:
            pass
        comp = rm.get_container_services().getCollocatedComp(comp_spec,
                                                             mark_as_default,
                                                             target_comp)
        rm.add_resource(comp._get_name(), comp, True)
        return comp

def getComponentNonSticky(comp_name):
        rm = ResourceManager()
        try:
            comp = rm.get_resource(comp_name)
            return comp
        except NoSuchResource, ex:
            pass
        comp = rm.get_container_services().getComponentNonSticky(comp_name)
        return comp    
    
def getLogger():
        rm = ResourceManager()
        return rm.get_container_services().getLogger()
    
def releaseComponents():
        rm = ResourceManager()
        rm.release_temp_resources()

def releaseComponent(comp_name):
        rm = ResourceManager()
        rm.release_temp_resource(comp_name)

def addMock(mockName, mockObject):
        rm = ResourceManager("client").add_resource(mockName, mockObject)

if __name__ == '__main__':
    r1 = ResourceManager()
    print id(r1)
    r2 = ResourceManager()
    print id(r2)
    
#
# ___oOo___
