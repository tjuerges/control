import logging
    
#class Singleton(object):
#    """ A Pythonic Singleton """
#    def __new__(cls, *args, **kwargs):
#        if '_inst' not in vars(cls):
#            cls._inst = super(Singleton, cls).__new__(cls, *args, **kwargs)
#        return cls._inst

class Singleton(object):
    """ A Pythonic Singleton """
    _singletons = {}
    def __new__(cls, *args, **kwargs):
#        print 'Constructor list arguments: ' + str(args)
#        print 'Constructor keyword arguments: ' + str(kwargs.keys())
#        print 'Singletons: ' + str(cls._singletons)
        if len(args) > 0 and args[0] != None:
            if type(args[0]) == str:
                name = args[0]
            else:
                name = args[0].getName()
        else:
            import bootstrap
            name = bootstrap.getContextName()
        if name not in cls._singletons.keys():
#            print "ResourceManager: Creating new instance for '" + name + "'"
            cls._singletons[name] = super(Singleton, cls).__new__(cls, *args, **kwargs)
        return cls._singletons[name]

class LoggerAdaptor:
    """Unfortunately the ACS Logger is incompatible with the normal
    Python logging facility. This class provides an Adaptor that
    translates between these two interfaces.
    """
    _initialized = False
    def __init__(self, name):
        self.logger = logging.getLogger()
        if not LoggerAdaptor._initialized:
            self.logger.setLevel(logging.DEBUG)
            self.handler = logging.StreamHandler()
            self.formatter = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')
            self.handler.setFormatter(self.formatter)
            self.logger.addHandler(self.handler)
            LoggerAdaptor._initialized = True
        
    def logInfo(self, msg):
        self.logger.info(msg)
        
    def logAlert(self, msg):
        self.logger.critical(msg)
    
    def logCritical(self, msg):
        self.logger.critical(msg)
    
    def logDebug(self, msg):
        self.logger.debug(msg)
    
    def logEmergency(self, msg):
        self.logger.critical(msg)
    
    def logNotice(self, msg):
        self.logger.info(msg)
    
    def logTrace(self, msg):
        self.logger.debug(msg)
    
    def logWarning(self, msg):
        self.logger.error(msg)
    
    def logXML(self, msg):
        self.logger.info(msg)
    
    def logErrorTrace(self, msg):
        self.logger.error(str(msg))
        
    def logNotSoTypeSafe(self, priority, msg, audience=None, array=None, antenna=None):
        l = "PRIORITY: " + str(priority) + "; "
        l = l + "AUDIENCE: " + str(audience) + "; "
        l = l + "ARRAY: " + str(array) + "; "
        l = l + "ANTENNA: " + str(antenna) + "; "
        l = l + msg
        self.logger.info(l)
        
class Output:
    '''
    This class is used to replace stdout and stderr streams in the execution
    of a script.
    '''
    def __init__(self):
        self.text = ''
    def write(self, string):
        self.text = self.text + string
    def writelines(self, lines):
        for line in lines: self.write(line)
        
def print_code(code):
    """This function is used to print Python code in log messages.
    It just prints the source code with line numbers a the left.
    """
    s = ""
    c = code.strip().split('\n')
    for ln in range(len(c)):
        s = s + "%03d   %s\n" % (ln+1, c[ln])
    return s

def format_errortrace(ex):
    """In Python, the exception helper method log() doesn't always work.
    """
    errorTrace = ex.getErrorTrace()
            
    e = 0
    errMsg = ""
    indent = "   "
    while errorTrace != None:
        e = e + 1
        errMsg = errMsg + indent*e + "ErrorTrace " + str(e) + ":\n"
        errMsg = errMsg + indent*e + "File = " + errorTrace.file + "\n"
        errMsg = errMsg + indent*e + "Line number = " + str(errorTrace.lineNum) + "\n"
        errMsg = errMsg + indent*e + "Routine = " + str(errorTrace.routine) + "\n"
        errMsg = errMsg + indent*e + "Host = " + str(errorTrace.host) + "\n"
        errMsg = errMsg + indent*e + "Process = " + str(errorTrace.process) + "\n"
        errMsg = errMsg + indent*e + "Thread = " + str(errorTrace.thread) + "\n"
        errMsg = errMsg + indent*e + "TimeStamp = " + str(errorTrace.timeStamp) + "\n"
        errMsg = errMsg + indent*e + "ErrorType = " + str(errorTrace.errorType) + "\n"
        errMsg = errMsg + indent*e + "ErrorCode = " + str(errorTrace.errorCode) + "\n"
        errMsg = errMsg + indent*e + "Severity = " + str(errorTrace.severity) + "\n"
        errMsg = errMsg + indent*e + "Short decription = " + str(errorTrace.shortDescription) + "\n"
        errMsg = errMsg + indent*e + "Data:\n"
        for data in errorTrace.data:
            errMsg = errMsg + indent*e + data.name + " = " + data.value + "\n"
        if len(errorTrace.previousError) == 1:
            errorTrace = errorTrace.previousError[0]
        else:
            errorTrace = None
    return errMsg
 
