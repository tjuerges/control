#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# rhiriart  2006-06-06  created
#

"""
This module is part of the Control Command Language.
Defines the SBObjectifier class.
"""

import sys
import math
from xml.dom.ext.reader import PyExpat
from xml.xpath import Evaluate
from xml.dom.ext import PrettyPrint
from Acspy.Util.XmlObjectifier import XmlObject
import ControlSB
import Control
import Correlator
import ScanIntentMod

#-------------------------------------------------------------------------------
class Output:
    """
    Small class to create an output stream like object. This is an internal 
    class. It's not part of the CCL user interface.
    """
    def __init__(self):
        self.text = ''
    def write(self, string):
        self.text = self.text + string
    def writelines(self, lines):
        for line in lines: self.write(line)

#-------------------------------------------------------------------------------
class SBObjectifier:
    """
    Generates a Scheduling Block (SB) Python object from XML.

    The generated python object is an instance of Acs.Util.XmlObjectifier.XmlObject.
    See the documentation for the module Acspy.Util.XmlObjectifier for complete
    information about this class. In general, the generated python binding object
    follows the same structure as the XML document, and the methods getValue() and
    getAttribute() are used to retrieve information. For example, for the following
    (partial) Scheduling Block:
    
        <?xml version="1.0" encoding="UTF-8"?>
        <SchedBlock xmlns="Alma/ObsPrep/SchedBlock"
                    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                    almatype="APDM::SchedBlock"
                    xsi:type="SchedBlock">
            ...
            <HolographyParameters almatype="APDM::HolographyParameters"
                entityPartId="1152541031116/16321306 3671128994"
                towerName="Holography_Tower_1" scanDirection="AzScan" calMode="FIVEPOINT">
                <name>Holography (Default values)</name>
                <frequency unit="GHz">104.0</frequency>
                <resolution unit="cm">20.0</resolution>
                <startFraction>0.0</startFraction>
                <speed unit="arcsec/s">200.0</speed>
                <rowsCal>5</rowsCal>
                <calTime unit="sec">60.0</calTime>
            </HolographyParameters>
            ...
        </SchedBlock>     
    
    the following script can be used to print some of the holography parameters:
    
        # Example script to print some content from a Scheduling Block.
        objectifier = getSBObjectifier()
        schedBlk = objectifier.getSB()
        
        print 'Some holography parameters'
        towerName = schedBlk.SchedBlock.HolographyParameters.getAttribute('towerName')
        print 'Tower name: ' + towerName
        freq = schedBlk.SchedBlock.HolographyParameters.frequency.getValue()
        frequnit = schedBlk.SchedBlock.HolographyParameters.frequency.getAttribute('unit')
        print 'Frequency: ' + freq + ' ' + frequnit
        res = schedBlk.SchedBlock.HolographyParameters.resolution.getValue()
        resunit = schedBlk.SchedBlock.HolographyParameters.resolution.getAttribute('unit')
        print 'Resolution: ' + res + ' ' + resunit
        
        # End example.
    
    SBObjectifier rely heavily in the use XmlObjectifier. In general, the toolkit does
    most of the work, although in order to facilitate the use of the Scheduling Block
    python object, some utility methods are provided.
    
    One example of this is the need to resolve the internal references 
    in the Scheduling Block XML. This is necessary to retrieve the abstract instrument
    specification, the field source, and the observing parameter elements from references
    in the Target element. Functions are provided to retrieve these elements without
    having to navigate through element collections.

    For example, consider the following SB:
    
        <?xml version="1.0" encoding="UTF-8"?>
        <SchedBlock xmlns="Alma/ObsPrep/SchedBlock"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            almatype="APDM::SchedBlock" xsi:type="SchedBlock">
            ...
            <OpticalCameraSpec almatype="APDM::OpticalCameraSpec"
                entityPartId="1152548792442/341322 2961012348" filter="night">
                <name>trcOP.scat.6.polar.stars.bright</name>
                <minIntegrationTime unit="sec">2.0</minIntegrationTime>
            </OpticalCameraSpec>
            <FieldSource almatype="APDM::FieldSource" 
                         entityPartId="1152548792468/10688173 3188856610">
                ...
            </FieldSource>
            <FieldSource almatype="APDM::FieldSource" 
                         entityPartId="1152548792581/10185078 3188856610">
                ...
            </FieldSource>
            ...
            <OpticalPointingParameters
                almatype="APDM::OpticalPointingParameters"
                entityPartId="1152548792418/22178007 1822728600">
                <antennaPositionTolerance unit="arcsec">0.1</antennaPositionTolerance>
                <elevationLimit unit="degrees">15.0</elevationLimit>
                <maxMagnitude>
                    <value">0.0</value>
                </maxMagnitude>
                <minMagnitude>
                    <value">13.0</value>
                </minMagnitude>
            </OpticalPointingParameters>
            ...
            <Target almatype="APDM::Target">
                <abstractInstrumentSpecId>1152548792442/341322 2961012348</abstractInstrumentSpecId>
                <fieldSourceId>1152548792468/10688173 3188856610</fieldSourceId>
                <observingParametersIdList>1152548792418/22178007 1822728600</observingParametersIdList>
                <TotalPowerMeasurement almatype="APDM::TotalPowerMeasurement"/>
            </Target>
            ...
        </SchedBlock>
    
    The following script retrieves the referenced data from the Target:
    
        # Example script that exemplifies some of the SBObjectifier utility functions.
        
        objectifier = getSBObjectifier()
        schedBlk = objectifier.getSB()
        
        target = schedBlk.SchedBlock.Target
        
        # Get the OpticalCameraSpec element
        absInstrID = target.abstractInstrumentSpecId.getValue()
        cameraSpec = objectifier.getAbstractInstrumentSpec(absInstrID)

        # Get the FieldSource
        sourceID = target.fieldSourceId.getValue()
        source = objectifier.getFieldSource(sourceID)
        
        # Get the OpticalPointingParameters
        obsParamsID = target.observingParametersIdList.getValue()
        optPntParams = objectifier.getObservingParameters(obsParamsID)
        
        # End script.
    
    In addition, two static methods are provided to convert the SpectralSpec and 
    CorrelatorConfig Python objects to IDL. These IDL structures are necessary because 
    they are passed to Correlator and DataCapturer.
    
    See also:
    Acspy.Util.XmlObjectifier
    
    Notes:
    - This class is not that easy to use. Probably an SB CCL python object will be
    defined to wrap it.
    """

    #---------------------------------------------------------------------------
    def __init__(self, xml):
        """Constructor.
        """
        self.output = Output()
        self.reader = PyExpat.Reader()
        self.dom = self.reader.fromString(xml)       
        self.xmlObject = XmlObject(xmlString=xml)
    
    #---------------------------------------------------------------------------
    def getSB(self):
        """Get the Scheduling Block Python binding object.
        
        Returns: XmlObject, representing the Scheduling Block.
        """
        return self.xmlObject

    #---------------------------------------------------------------------------
    def aggregate(self, object):
        """
        Utility function provided to get a uniform access to those elements that
        can occur more than once in the XML document. If there's more than one,
        XMLObjectifier will generate a list of objects. However, if there's only
        one, then just the object will be generated. 
        
        This function checks whether the object is a list, and if it is not, 
        it wraps it in a list.
        """
        # This is the old comparison, I found that it failed on strings
        # so switchted to the comparision below.
        #if hasattr(object, 'count'):
        if type(object) == type([]):
            return object
        else:
            return [object]

    def getValueOrDefault(self, object, default):
        """
        Utility function to get the value of a given element or a default.
        This is used to protect the case where getValue() is used but there are no
        child nodes.
        """
        if len(object.childNodes) == 0:
            return default
        else:
            return object.getValue()
        
    #---------------------------------------------------------------------------
    def getAbstractInstrumentSpec(self, instrSpecId):
        """
        Get the abstract instrument specification referenced by an Id.
        
        Parameters:
        instrSpecId - Instrument specification ID. Should have the form:
                      "XXXXXXXXXXXXX/XXXXXXXX XXXXXXXXXX"
        Returns:
        A tuple (type, spec), where type is a string and spec is a
        XmlObject.
        
        Example: See the class documentation for an example.
        """

        instrSpecTypes = ["SpectralSpec", "OpticalCameraSpec"]
        for type in instrSpecTypes:
            path = "//" + type + "[@entityPartId='" + instrSpecId + "']"
            instrSpecs = Evaluate(path, self.dom)
            if len(instrSpecs) == 1:
                self.output.text = ""
                PrettyPrint(instrSpecs[0], stream=self.output)
                if type == "SpectralSpec":
                    return type, XmlObject(xmlString=self.output.text).SpectralSpec
                elif type == "OpticalCameraSpec":
                    return type, XmlObject(xmlString=self.output.text).OpticalCameraSpec
                # etc.
                
        raise "Could not find instrument specification for abstractInstrumentSpecId = " + \
            instrSpecId
           
    #---------------------------------------------------------------------------
    def getFieldSource(self, fieldSourceId):
        """
        Get the field source referenced by an ID.
        
        Parameters:
        fieldSourceId - Field source ID.
        
        Returns: An XmlObject for the FieldSource element in the SB.
        
        Raises: An exception in the case the FieldSource for the specified
                ID could not be found.
        
        Example: See the class documentation for an example.
        """

        path = "//FieldSource[@entityPartId='" + fieldSourceId + "']"
        fieldSources = Evaluate(path, self.dom)
        if len(fieldSources) != 1:
            raise "Could not find field source"
        self.output.text = ""
        PrettyPrint(fieldSources[0], stream=self.output)
        
        return XmlObject(xmlString=self.output.text).FieldSource
    
    #---------------------------------------------------------------------------
    def getObservingParameters(self, obsParametersId):
        """
        Get the observing parameters from an entity part ID. This is an utility
        function aimed to simplify the access to the observing parameters in
        the SB python object.
        
        The scheduling block can contain different types of observing parameters.
        These are:
           - AmplitudeCalParameters
           - BandpassCalParameters
           - FocusCalParameters
           - HolographyParameters
           - OpticalPointingParameters
           - PhaseCalParameters
           - PointingCalParameters
           - PolarizationCalParameters
           - ReservationParameters
           - ScienceParameters
           
        Note: The purpose (or intent) of the scheduling block scans is deduced from
        the type of the observation parameters.
        
        Arguments:
            obsParametersId - Observation parameter identifier
        Returns:
            An XmlObject for the observing parameter element in the scheduling
            block.
        Raises:
            An exception in the case that the observing parameters for the
            specified ID could not be found.
            
        Example: See the class documentation for an example.
        """

        paramTypes = ["AmplitudeCalParameters", "BandpassCalParameters",
            "FocusCalParameters", "HolographyParameters", 
            "OpticalPointingParameters", "PhaseCalParameters",
            "PointingCalParameters", "PolarizationCalParameters",
            "ReservationParameters", "ScienceParameters"]
        for type in paramTypes:
            path = "//" + type + "[@entityPartId='" + obsParametersId + "']"
            obsParams = Evaluate(path, self.dom)
            if len(obsParams) == 1:
                self.output.text = ""
                PrettyPrint(obsParams[0], stream=self.output)
                return XmlObject(xmlString=self.output.text)
                
        raise "Could not find observing parameters for obsParametersId =" + \
            obsParametersId
      
    #---------------------------------------------------------------------------
    def getPurposes(self, target):
        """
        The Target element in the SchedBlock references a list of observing
        parameter lists in its observingParametersIdList element. Based on
        the type of the observing parameters referenced the purpose of the
        scan is deduced. The observing parameter list can be a choice of:
           - AmplitudeCalParameters
           - BandpassCalParameters
           - FocusCalParameters
           - HolographyParameters
           - OpticalPointingParameters
           - PhaseCalParameters
           - PointingCalParameters
           - PolarizationCalParameters
           - ReservationParameters
           - ScienceParameters
           
        Based on which one of these element do we find in the SchedBlock, an array
        of Control.Purpose elements is assembled. Control.Purpose defines the
        following purposes (see ICD/CONTROL/idl/ControlInterfaces.midl):
            - FOCUS
            - HOLOGRAPHY
            - POINTING
            - ATMOSPHERE
            - POINTINGMODEL
            - LAST
            - TARGET
            - PHASECAL
            - AMPLICAL
            - DELAY
            - ANTENNAPOSITIONS
            - PHASECURVE
            - AMPLICURVE
            - SKYDIP
            - POLARIZATION
            - BANDPASS
        """
        purposes = []    
        for obsp in self.aggregate(target.observingParametersIdList):
            id = obsp.getValue()
            obsParam = self.getObservingParameters(id)
            nodeName = obsParam.childNodes[0].nodeName
            if nodeName == u"ScienceParameters":
                purposes.append(ScanIntentMod.OBSERVE_TARGET)
            elif nodeName == u"PhaseCalParameters":
                purposes.append(ScanIntentMod.CALIBRATE_PHASE)
            elif nodeName == u"OpticalPointingParameters":
                purposes.append(ScanIntentMod.CALIBRATE_POINTING)
            elif nodeName == u"FocusCalParameters":
                purposes.append(ScanIntentMod.CALIBRATE_FOCUS)
            elif nodeName == u"HolographyParameters":
                purposes.append(ScanIntentMod.MAP_ANTENNA_SURFACE)
            elif nodeName == u"PointingCalParameters":
                purposes.append(ScanIntentMod.CALIBRATE_POINTING)
        return purposes
        
    #---------------------------------------------------------------------------
    def getSpectralSpecIDL(self, spectSpec):
        """
        Transform the spectral specification from the Python XML binding class
        to IDL.
        """

        try:
            integrationTime = float(spectSpec.integrationTime.getValue())
        except:
            integrationTime = 0.0
        try:    
            dynamicRange = float(spectSpec.dynamicRange.getValue())
        except:
            dynamicRange = 0.0
        restFrequency = float(spectSpec.FrequencySetup.restFrequency.getValue())
        try:
            transitionName = spectSpec.FrequencySetup.transitionName.getValue()
        except:
            transitionName = ""
            
        # I think this has something to do with the new enumeration framework
        try:
            receiverBand = ControlSB.ReceiverBandType._item(int(spectSpec.FrequencySetup.getAttribute("receiverBand")))
        except:
            receiverBand = ControlSB.ReceiverBandType._item(0)
            
        # Check whether lO1Frequency has no content in XML.
        if len(spectSpec.lO1Frequency.childNodes) == 0:
            LO1 = -1.0
        else:
            LO1 = float(spectSpec.lO1Frequency.getValue())
        
        spectSpecIDL = ControlSB.SpectralSpec(integrationTime, \
                                              dynamicRange, \
                                              restFrequency, \
                                              transitionName, \
                                              receiverBand, \
                                              LO1)
        return spectSpecIDL

    #---------------------------------------------------------------------------
    def getCorrelatorConfigurationIDL(self, corrConf):
        """
        Transform the correlator configuration from the Python XML binding class
        to IDL.        
        """
        baseBandConfigs = self.aggregate(corrConf.BaseBandConfig)
        nBaseBandConfigs = len(baseBandConfigs)
        baseBandConfigsIDL = []
        for bb in range(nBaseBandConfigs):

            # Spectral Windows      
            spectralSubbandSets = self.aggregate(baseBandConfigs[bb].SpectralSubbandSet)  
            nSpectSubBandSets = len(spectralSubbandSets)
            spectralWindowsIDL = []
            for ssbs in range(nSpectSubBandSets):
            
                if spectralSubbandSets[ssbs].centerFrequency.getAttribute('unit') == 'MHz' :
                    centerFrequencyMHZ = float(spectralSubbandSets[ssbs].centerFrequency.getValue())
                elif spectralSubbandSets[ssbs].centerFrequency.getAttribute('unit') == 'GHz' :
                    centerFrequencyMHZ = float(spectralSubbandSets[ssbs].centerFrequency.getValue()) * 1000.0
                else:
                    raise "getCorrelatorConfigurationIDL only knows about MHz and GHz for centerFrequency"
                tFBMode = int(spectralSubbandSets[ssbs].tFBMode.getValue())
                # correlatorFraction
                fraction = int(spectralSubbandSets[ssbs].correlatorFraction.getValue())
                if fraction == 100:
                    correlatorFraction = Correlator.FRACTION_1_1
                elif fraction == 50:
                    correlatorFraction = Correlator.FRACTION_1_2
                elif fraction == 25:
                    correlatorFraction = Correlator.FRACTION_1_4
                elif fraction == 12:
                    correlatorFraction = Correlator.FRACTION_1_8
                elif fraction == 6:
                    correlatorFraction = Correlator.FRACTION_1_16
                elif fraction == 3:
                    correlatorFraction = Correlator.FRACTION_1_32
                else:
                    raise "Wrong correlator fraction: " + str(fraction)
                numOverlappedChannels = int(spectralSubbandSets[ssbs].numOverlapChannels.getValue())
                spectralAveragingFactor = int(spectralSubbandSets[ssbs].spectralAveragingFactor.getValue())
            
                spectralWindowIDL = Correlator.SpectralWindow(centerFrequencyMHZ, \
                                                              tFBMode, \
                                                              correlatorFraction, \
                                                              numOverlappedChannels, \
                                                              spectralAveragingFactor)
                spectralWindowsIDL.append(spectralWindowIDL)

            baseBandIndex = int(baseBandConfigs[bb].baseBandIndex.getValue())

            #Now that the cAM is correctly set in the CORR Config, the
            # the following fails so we check by hand
            #cAM = Correlator.eCAM._item(int(baseBandConfigs[bb].cAM.getValue()))
            if int(baseBandConfigs[bb].cAM.getValue()) == 16:
                cAM = Correlator.eCAM._item(1)
            else:
                cAM = Correlator.eCAM._item(0)

            # dataProducts
            dp = baseBandConfigs[bb].getAttribute("dataProducts")
            if dp == "AUTO_CROSS_PRODUCTS":
                dataProducts = Correlator.AUTO_CROSS_PRODUCTS
            elif dp == "AUTO_ONLY_PRODUCTS":
                dataProducts = Correlator.AUTO_ONLY_PRODUCTS
            elif dp == "CROSS_ONLY_PRODUCTS":
                dataProducts = Correlator.CROSS_ONLY_PRODUCTS
            else:
                raise "Invalid data product: " + dp
            binMode = int(baseBandConfigs[bb].binMode.getValue())

            timeUnit = baseBandConfigs[bb].bin0Duration.getAttribute('unit')
            if timeUnit == 'msec':
                bin0Duration = int(float(baseBandConfigs[bb].bin0Duration.getValue()) * 10000)
            elif timeUnit == 'sec':
                bin0Duration = int(float(baseBandConfigs[bb].bin0Duration.getValue()) * 10000000)
            else:
                raise "Bin 0 Duration must be in msec or sec, not in "+timeUnit

            timeUnit = baseBandConfigs[bb].bin1Duration.getAttribute('unit')
            if timeUnit == 'msec':
                bin1Duration = int(float(baseBandConfigs[bb].bin1Duration.getValue()) * 10000)
            elif timeUnit == 'sec':
                bin1Duration = int(float(baseBandConfigs[bb].bin1Duration.getValue()) * 10000000)
            else:
                raise "Bin 1 Duration must be in either msec or sec, not in "+timeUnit
            numSwitchCycles = int(baseBandConfigs[bb].numSwitchCycles.getValue())
            aPCDataSets = Correlator.eAPCData._item(int(baseBandConfigs[bb].aPCDataSets.getValue()))
            if baseBandConfigs[bb].quantizationCorrection.getValue():
                quantCorr = 1
            else:
                quantCorr = 0
            if baseBandConfigs[bb].fFT.getValue():
                fFT = 1
            else:
                fFT = 0
            # windowFunction
            wf = baseBandConfigs[bb].getAttribute("windowFunction").upper()
            if wf == "UNIFORM":
                windowFunction = Correlator.UNIFORM
            elif wf == "HANNING":
                windowFunction = Correlator.HANNING
            elif wf == "HAMMING":
                windowFunction = Correlator.HAMMING
            elif wf == "BARTLETT":
                windowFunction = Correlator.BARTLETT
            elif wf == "BLACKMAN":
                windowFunction = Correlator.BLACKMAN
            elif wf == "BLACKMAN_HARRIS":
                windowFunction = Correlator.BLACKMAN_HARRIS
            elif wf == "WELCH":
                windowFunction = Correlator.WELCH
            else:
                raise "Invalid window function: " + wf
              
            timeUnit = baseBandConfigs[bb].channelAverageDuration.getAttribute('unit')
            if timeUnit != 'msec':
                raise "Channel Average Duration must be in msec, not in "+timeUnit
            channelAvgDuration = int(float(baseBandConfigs[bb].channelAverageDuration.getValue()) * 10000)    

            # channelAvgRegions
            channelAvgBands = self.aggregate(baseBandConfigs[bb].ChannelAverageBands)
            nChannelAvgBands = len(channelAvgBands)
            channelAvgRegionsIDL = []
            for cab in range(nChannelAvgBands):
                startChannel = int(channelAvgBands[cab].startChannel.getValue())
                numberChannels = int(channelAvgBands[cab].numberChannels.getValue())
                spectralWindowIndex = int(channelAvgBands[cab].subbandSetIndex.getValue())
                channelAvgBandIDL = Correlator.ChannelAverageRegion(startChannel, \
                                                                    numberChannels, \
                                                                    spectralWindowIndex)
                channelAvgRegionsIDL.append(channelAvgBandIDL)                                                                   
                
            baseBandConfigIDL = Correlator.BaseBandConfig(baseBandIndex, \
                                                          cAM, \
                                                          dataProducts, \
                                                          binMode, \
                                                          bin0Duration, \
                                                          bin1Duration, \
                                                          numSwitchCycles, \
                                                          aPCDataSets, \
                                                          quantCorr, \
                                                          fFT, \
                                                          windowFunction, \
                                                          channelAvgDuration, \
                                                          channelAvgRegionsIDL, \
                                                          spectralWindowsIDL)
            baseBandConfigsIDL.append(baseBandConfigIDL)

        integrationDuration = int(corrConf.integrationDuration.getValue())
        subScanDuration = int(corrConf.subScanDuration.getValue())
       
        corrConfIDL = Correlator.CorrelatorConfiguration(integrationDuration, \
                                                         subScanDuration, \
                                                         baseBandConfigsIDL)
        return corrConfIDL

    #---------------------------------------------------------------------------
    def getSourceIDL(self, source):
        """
        Transform the FieldSource Python binding object to the Source IDL.
        This function should be deprecated as soon as Array and DataCapture are modified
        so they don't use ControlSB Source anymore. Instead, they will use the Java and
        Python binding classes.
        
        Parameters:
            source - FieldSource Python binding class
            
        Returns: Source CORBA IDL object
            
        See also:
        - ICD/OFFLINE/idl/ControlSB.idl for documentation about the Source IDL
        structure.
        """
        sourceName = str(source.sourceName.getValue())
        try:
            ra = float(source.sourceCoordinates.longitude.getValue())
        except:
            ra = 0.0 # Ephemeris objects doesn't have coordinates
        try:
            dec = float(source.sourceCoordinates.latitude.getValue())
        except:
            dec = 0.0
        # Converting the coordinates from degrees to radians
        ra = ra * math.pi / 180.0
        dec = dec * math.pi / 180.0
        coord = ControlSB.J2000Coord(ra, dec)
        sourceVelocity = float(source.sourceVelocity.centerVelocity.getValue())
        # Some SchedBlocks come without Ephemeris.
        # TODO: is this required???
        try:
            sourceEphemeris = str(source.sourceEphemeris.getValue())
        except:
            sourceEphemeris = "Ephemeris"

        try:
            pMRa = float(source.pMRA.getValue())
            pMRa = pMRa*math.pi/3600/180/1000/(365.25*24*3600)
        except:
            pMRa = 0.0
            
        try:
            pMDec = float(source.pMDec.getValue())
            pMDec = pMDec*math.pi/3600/180/1000/(365.25*24*3600)
        except:
            pMDec = 0.0

        if source.nonSiderealMotion.getValue():
            nonSiderealMotion = 1
        else:
            nonSiderealMotion = 0
        solarSystemObject = ControlSB.Emphemeris
        
        try:
            longitude = float(source.Reference.referenceCoordinates.longitude.getValue())
        except:
            # Hardcode some value
            longitude = 0.0
        try:
            latitude = float(source.Reference.referenceCoordinates.latitude.getValue())
        except:
            # Hardcode some values
            latitude = 0.0
        referenceCoord = ControlSB.J2000Coord(longitude, latitude)

        try:
            if len(source.Reference.intTimeReference.childNodes) == 0:
                intTimeReference = 0.0
            else:
                intTimeReference = float(source.Reference.intTimeReference.getValue())
        except:
            # Hardcode a value
            intTimeReference = 0.0

        try:
            if len(source.Reference.intTimeSource.childNodes) == 0:
                timeSource = 0
            else:
                timeSource = float(source.Reference.intTimeSource.getValue())
        except:
            # Hardcode a value
            timeSource = 0
        
        try:            
            parallax = float(source.parallax.getValue())
        except:
            parallax = 0.0
        
        sourcePropertiesList = []
        pattern = ControlSB.AbsolutePointing
        dummyCoord = ControlSB.J2000Coord(0.0, 0.0)
        circle = ControlSB.CirclePattern(dummyCoord)
        spiral = ControlSB.SpiralPattern(dummyCoord)
        absPointing = ControlSB.AbsolutePointingPattern([coord])
        offPointing = ControlSB.OffsetPointingPattern([dummyCoord])
        rectangle = ControlSB.RectanglePattern(dummyCoord, \
                                               0.0, \
                                               0.0, \
                                               0.0, \
                                               0.0, \
                                               ControlSB.Longitude, \
                                               0.0, \
                                               0.0)

        sourceIDL = ControlSB.Source(sourceName, \
                                     coord, \
                                     sourceVelocity, \
                                     sourceEphemeris, \
                                     pMRa, \
                                     pMDec, \
                                     nonSiderealMotion, \
                                     solarSystemObject, \
                                     referenceCoord, \
                                     intTimeReference, \
                                     timeSource, \
                                     sourcePropertiesList, \
                                     pattern, \
                                     circle, \
                                     spiral, \
                                     absPointing, \
                                     offPointing, \
                                     rectangle, \
                                     parallax)
                                     
                                     
        # temporarily overriding this
        
        #    coord = ControlSB.J2000Coord(0.3, 0.7)
        #    circlePatt = ControlSB.CirclePattern(coord)        
        #    spiralPatt = ControlSB.SpiralPattern(coord)
        #    absPatt = ControlSB.AbsolutePointingPattern([coord])
        #    offPatt = ControlSB.OffsetPointingPattern([coord])
        #    rectPatt = ControlSB.RectanglePattern(coord, # J2000Coord phaseCenter;
        #                                          0.0, # double longitudeLength;
        #                                          0.0, # double longitudeStep;
        #                                          0.0, # double latitudeLength;
        #                                          0.0, # double latitudeStep;
        #                                          ControlSB.Longitude, # ScanDirection direction;
        #                                          0.0, # double scanVelocity;
        #                                          0.0) # double orientiation;
        #    source = ControlSB.Source("sourceName",   # string sourceName
        #                              coord,  # J2000Coord coord;
        #                              0.0, # double sourceVelocity;
        #                              "sourceEphemeris", # string sourceEphemeris;
        #                              0.1, # double pMRa;
        #                              0.1, # double pMDec;
        #                              1, # boolean nonSiderealMotion;
        #                              ControlSB.Emphemeris, # SolarSystem solarSystemObject;
        #                              coord, # J2000Coord referenceCoord;
        #                              0.0, # double intTimeReference;
        #                              0.0, # double timeSource;
        #                              [], # SourcePropertiesSeq sourcePropertiesList;
        #                              ControlSB.Circle, # FieldPattern pattern;
        #                              circlePatt, # CirclePattern circle;
        #                              spiralPatt, # SpiralPattern spiral;
        #                              absPatt, # AbsolutePointingPattern absPointing;
        #                              offPatt, # OffsetPointingPattern offPointing;
        #                              rectPatt, # RectanglePattern rectangle;
        #                              0.0) # double parallax;                      
        return sourceIDL                             
                
    #---------------------------------------------------------------------------
    def printCorrelatorConfigurationIDL(self, corrConf):
        """
        Prints the CorrelatorConfiguration IDL structure.
        """
        print "-"*70
        print "CorrelatorConfiguration:"
        print "\tIntegrationDuration: " + str(corrConf.IntegrationDuration)
        print "\tSubScanDuration: " + str(corrConf.SubScanDuration)
        for bb in range(len(corrConf.BaseBands)):
            print "\tBaseBands[" + str(bb) + "/" + str(len(corrConf.BaseBands)-1) + "]:"
            print "\t\tBaseBandIndex: " + str(corrConf.BaseBands[bb].BaseBandIndex)
            print "\t\tCAM: " + str(corrConf.BaseBands[bb].CAM)
            print "\t\tDataProducts: " + str(corrConf.BaseBands[bb].DataProducts)
            print "\t\tBinMode: " + str(corrConf.BaseBands[bb].BinMode)
            print "\t\tBin0Duration: " + str(corrConf.BaseBands[bb].Bin0Duration)
            print "\t\tBin1Duration: " + str(corrConf.BaseBands[bb].Bin1Duration)
            print "\t\tNumSwitchCycles: " + str(corrConf.BaseBands[bb].NumSwitchCycles)
            print "\t\tAPCDataSets: " + str(corrConf.BaseBands[bb].APCDataSets)
            print "\t\tQuantizationCorrection: " + str(corrConf.BaseBands[bb].QuantizationCorrection)
            print "\t\tFFT: " + str(corrConf.BaseBands[bb].FFT)
            print "\t\tWindowFunction: " + str(corrConf.BaseBands[bb].WindowFunction)
            print "\t\tChannelAverageDuration: " + str(corrConf.BaseBands[bb].ChannelAverageDuration)
            for car in range(len(corrConf.BaseBands[bb].ChannelAverageRegions)):
                print "\t\tChannelAverageRegions[" + str(car) + "/" + str(len(corrConf.BaseBands[bb].ChannelAverageRegions)-1) + "]:"
                print "\t\t\tstartChannel: " + str(corrConf.BaseBands[bb].ChannelAverageRegions[car].startChannel)            
                print "\t\t\tnumberChannels: " + str(corrConf.BaseBands[bb].ChannelAverageRegions[car].numberChannels)            
                print "\t\t\tspectralWindowIndex: " + str(corrConf.BaseBands[bb].ChannelAverageRegions[car].spectralWindowIndex)            
            for sp in range(len(corrConf.BaseBands[bb].SpectralWindows)):
                print "\t\tSpectralWindows[" + str(sp) + "/" + str(len(corrConf.BaseBands[bb].SpectralWindows)-1) + "]:"
                print "\t\t\tcenterFrequencyMHz: " + str(corrConf.BaseBands[bb].SpectralWindows[sp].centerFrequencyMHz)            
                print "\t\t\tTFBMode: " + str(corrConf.BaseBands[bb].SpectralWindows[sp].TFBMode)            
                print "\t\t\tcorrelatorFraction: " + str(corrConf.BaseBands[bb].SpectralWindows[sp].correlatorFraction)            
                print "\t\t\tnumOverlappedChannels: " + str(corrConf.BaseBands[bb].SpectralWindows[sp].numOverlappedChannels)            
                print "\t\t\tspectralAveragingFactor: " + str(corrConf.BaseBands[bb].SpectralWindows[sp].spectralAveragingFactor)            
           
    #---------------------------------------------------------------------------
    def printSpectralSpecIDL(self, spectSpec):
        """
        Prints the SpectralSpec IDL structure.
        """
        print "-"*70
        print "SpectralSpec:"
        print "\tintegrationTime: " + str(spectSpec.integrationTime)
        print "\tdynamicRange: " + str(spectSpec.dynamicRange)
        print "\trestFrequency: " + str(spectSpec.restFrequency)
        print "\ttransitionName: " + str(spectSpec.transitionName)
        print "\treceiverBand: " + str(spectSpec.receiverBand)
        print "\tLO1: " + str(spectSpec.LO1)
                
#
# ___oOo___
