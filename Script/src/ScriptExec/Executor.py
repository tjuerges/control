#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#

import ACSErr
import Control
import sys
import code

from util import Output
import CCL.Global

# Executes code only when container is either the container container services or
# a Python simple client.

class Executor:
    """This class runs Python code. It is used by the
    ScriptExecutor component.
    """
    
    def __init__(self, container, logger=None):
        
        self.container = container
        if logger is None:
            self.logger = container.getLogger()
        else:
            self.logger = logger
        
        # This is a Python Code object, used in the manual mode.
        # The execution is performed in two steps, in the first the
        # script is compiled and if everything goes well the resultant
        # code object is stored in this member variable. In the second
        # step, this code object is executed.
        self.code = None
        self.code_str = ""
        
        self._locals = {}
        self._globals = {}

    def setArrayName(self, arrayName):
        script = """from CCL.Global import * 
setArrayName('""" + arrayName + """')
"""
        exec script in self._globals
    
    def setSB(self, sbXML):
        script = """from CCL.Global import setSB
schedBlock = \"\"\"""" + sbXML + """\"\"\"
setSB(schedBlock)
"""
        exec script in self._globals

    def getLocals(self):
        return self._locals
        
    def runSource(self, source):
        """Run a Python script. This is meant to be used by the ScriptExecutor
        component running script in automatic mode.
        
        Parameters:
          source - String. The Python script. (Actually, it could be a code object
                   or a file object as well.)
                   
        Raises:
          - Python built in exceptions.
          - ACS Error exceptions thrown by statements executing CONTROL
            device object methods.
        """

        self.logger.logTrace("called... source = '" + source + "'")
        try:
            savestreams = sys.stdout, sys.stderr
            sys.stdout = Output()
            sys.stderr = Output()

            # execute the code object
            exec source in self._globals, self._locals
            result = sys.stdout.text.encode('ascii')

            # restore the standard streams
            sys.stdout, sys.stderr = savestreams

            self.logger.logTrace("exiting... results = '" + result + "'")

            return result

        except Control.AbortionException, ex:
            self.logger.logDebug("Execution aborted.")
            # stop SB execution cleanly
            CCL.Global.getArray().endExecution(Control.PARTIAL, "SB execution aborted.")

    def compileScript(self, code_str):
        '''
        Compiles a chunck of code. This function, along with executeScript(), is 
        meant to be used in manual mode.
        
        The ScriptExecutor in manual mode can work in two different sub-modes: local and
        remote. In local mode a code.InteractiveConsole object interacts with this class
        directly, following a protocol to compile and run Python statements interactively.
        In remote mode the InteractiveConsole uses the ScriptExecutor component instead,
        following the same protocol. The ScriptExecutor, on the other side, uses this class.
        The difference resides on where is the process that runs scripts. Running manual
        mode in local sub-mode gives access to local resources. Running manual mode
        in remote sub-mode allows to run statements in exactly the same execution
        enviroment (code versions, libraries, etc.) that scripts in automatic mode will
        run.
        
        See the Python code library and the mmc Python utility (in this module) for details
        about the protocol followed by interactive consoles. In summary compileScript is
        called until it returns false, which indicates that a complete Python sentence
        has been compiled and a code object produced. executeScript() is called to execute
        the code object afterwards.  
        
        Parameters:
          source_str - String. The Python script. (Actually, it could be a code object
                       or a file object as well.)
        
        Returns:
          True  - If the statement passed in the code_str parameter is incomplete.
          False - ~ 
        
        Raises:
          - Python built in exceptions.
          - ACS Error exceptions thrown by statements executing CONTROL
            device object methods.
        '''
        self.logger.logDebug("compileScript called. code = '" + code_str + "'")
        self.code = code.compile_command(code_str)
        self.code_str = code_str

        if not self.code:
            self.logger.logDebug("incomplete code")
            return True
        else:
            self.logger.logDebug("complete code")
            return False
        
    def executeScript(self):
        '''
        Executes the Code object stored in self.code using the execution context 
        hold in self.context member variable.
        
        See the documentation for compileScript() for more complete documentation
        about this function.
        
        Parameters:
          inString - deprecated, it will be removed soon.
        
        Raises:
          - Python built in exceptions.
          - ACS Error exceptions thrown by statements executing CONTROL
            device object methods.
        '''        
        self.logger.logDebug("called...")

        # before executing the code object, capture
        # the stdout and stderr
        saved_streams = sys.stdout, sys.stderr
        sys.stdout = Output()
        sys.stderr = Output()

        # execute de code object
        if self.code != None:
            exec self.code in self._globals
            result = sys.stdout.text.encode('ascii')
            reserr = sys.stderr.text.encode('ascii')

        # restore the standard streams
        sys.stdout, sys.stderr = saved_streams

        self.logger.logDebug("Results:\n-------\n" + result)
        self.logger.logDebug("Stderr:\n-------\n" + reserr)
        return result
        
#
# ___oOo___
