#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------    ----------------------------------------------
# rhiriart  2005-12-28  created
#

#************************************************************************
#   NAME
# 
#   SYNOPSIS
# 
#   DESCRIPTION
#
#   FILES
#
#   ENVIRONMENT
#
#   RETURN VALUES
#
#   CAUTIONS
#
#   EXAMPLES
#
#   SEE ALSO
#
#   BUGS     
#
#------------------------------------------------------------------------
#


import sys
import code
import readline
import atexit
import os
import traceback
import exceptions
import time
import getopt

import Control
import ControlCommon
import ScriptExecutorExceptions
import ScriptExecutorExceptionsImpl
import CCLExceptions
import CCLExceptionsImpl

from Acspy.Clients.SimpleClient import PySimpleClient

class ACSConsole(code.InteractiveConsole):

    def __init__(self, scriptExec, histfile=os.path.expanduser("~/.ccl-history")):
        sys.ps1 = 'ccl >>> '
        sys.ps2 = 'ccl ... '

        self.scriptExec = scriptExec

        code.InteractiveConsole.__init__(self)
        self.init_history(histfile)

    ## Initialize the command history with commands read from the
    # history file.
    #
    def init_history(self, histfile):
        readline.parse_and_bind("tab: complete")
        if hasattr(readline, "read_history_file"):
            try:
                readline.read_history_file(histfile)
            except IOError:
                pass
            atexit.register(self.save_history, histfile)

    ## Save the command history to the history file.
    #
    def save_history(self, histfile):
        readline.write_history_file(histfile)

    def __del__(self): pass        

    ## Start interacting with the console user.
    #
    def interact(self, banner=None):        
        code.InteractiveConsole.interact(self, banner)

    ## Compile and run some source in the ScriptExecutor.
    #
    def runsource(self, source, filename="<input>", symbol="single"):

        try:
            # Calling the Script Executor to compile the source
            needs_more = self.scriptExec.compileScript(source);

        except ScriptExecutorExceptions.SyntaxErrorEx, ex:
            errHelper = ScriptExecutorExceptionsImpl.SyntaxErrorExImpl(exception=ex,
                                                                       create=1)
            errHelper.Print()

            # Case 1. There is an error.
            return False

        except ScriptExecutorExceptions.CompilationErrorEx, ex:
            errHelper = ScriptExecutorExceptionsImpl.CompilationErrorExImpl(exception=ex,
                                                                            create=1)
            errHelper.Print()

            # Case 1. Error.
            return False

        if needs_more:
            # Case 2. Incomplete input.
            return True

        # Case 3. The input is complete.
        self.runcode(code)
        
        return False

    ## Execute a code object.
    #
    def runcode(self, code):

        try:
            output = self.scriptExec.executeScript("mmc")
        except SystemExit:
            raise
        except CCLExceptions.DeviceErrorEx, ex:
            print "ERROR: CCL Device error"
            exhelper = CCLExceptionsImpl.DeviceErrorExImpl(exception=ex, create=1)
            exhelper.Print()
        except CCLExceptions.NotYetImplementedEx, ex:
            print "ERROR: CCL function is not implemented yet"
            exhelper = CCLExceptionsImpl.NotYetImplementedExImpl(exception=ex, create=1)
            exhelper.Print()
        except CCLExceptions.CCLErrorEx, ex:
            print "ERROR: CCL error"
            exhelper = CCLExceptionsImpl.CCLErrorExImpl(exception=ex, create=1)
            exhelper.Print()            
        except CCLExceptions.CCLExceptionsEx, ex:
            print "ERROR: CCL exception"
            exhelper = CCLExceptionsImpl.CCLExceptionsExImpl(exception=ex, create=1)
            exhelper.Print()            
        except ScriptExecutorExceptions.ExecutionErrorEx, ex:
            print "ERROR: Execution error"
            exhelper = ScriptExecutorExceptionsImpl.ExecutionErrorExImpl(exception=ex, create=1)
            exhelper.Print()
        else:
            sys.stdout.write(output)

class ScriptRunner:
    '''
    Runs an entire script in non-interactive mode.
    '''
    
    def __init__(self, scriptExec):
        self.scriptExec = scriptExec

    def run(self, source):

        try:
            compl, output = self.scriptExec.runSource(source)
            print "Output:\n" + output
        except ScriptExecutorExceptions.SyntaxErrorEx, ex:
            print "ERROR: Sytax error"
            exhelper = ScriptExecutorExceptionsImpl.SyntaxErrorExImpl(exception=ex, create=1)
            exhelper.Print()
        except CCLExceptions.DeviceErrorEx, ex:
            print "ERROR: CCL Device exception"
            exhelper = CCLExceptionsImpl.DeviceErrorExImpl(exception=ex, create=1)
            exhelper.Print()
        except CCLExceptions.NotYetImplementedEx, ex:
            print "ERROR: CCL function is not yet implemented"
            exhelper = CCLExceptionsImpl.NotYetImplementedExImpl(exception=ex, create=1)
            exhelper.Print()
        except CCLExceptions.CCLErrorEx, ex:
            print "ERROR: CCL exception"
            exhelper = CCLExceptionsImpl.CCLErrorExImpl(exception=ex, create=1)
            exhelper.Print()
        except CCLExceptions.CCLExceptionsEx, ex:
            print "ERROR: CCL exception"
            exhelper = CCLExceptionsImpl.CCLExceptionsExImpl(exception=ex, create=1)
            exhelper.Print()
        except ScriptExecutorExceptions.CompilationErrorEx, ex:
            print "ERROR: Compilation error"
            exhelper = ScriptExecutorExceptionsImpl.CompilationErrorExImpl(exception=ex, create=1)
            exhelper.Print()
        except ScriptExecutorExceptions.ExecutionErrorEx, ex:
            print "ERROR: Execution error"
            exhelper = ScriptExecutorExceptionsImpl.ExecutionErrorExImpl(exception=ex, create=1)
            exhelper.Print()
        except:
            # print the traceback
            type, value, tb = sys.exc_info()
            print "ERROR: Unhandled exception"
            print type.__doc__
            print value.__doc__
            tblist = traceback.extract_tb(tb)
            print traceback.format_list(tblist)

def help():
        print """
NAME:
    mmc - CCL Manual Model Console

SYNOPSIS:
    mmc [OPTION]... [FILE]

DESCRIPTION:
    Runs the Control Command Language (CCL) Manual Mode Console (MMC). The MMC
    is an maintainance and debugging interface to the CONTROL subsystem.

    If a FILE is specified, the mmc will execute it in non-interactive mode. If
    no FILE is specified, the mmc will start a console where CCL commands can be
    executed interactively.

    The mmc will create a manual array component, which in turn will create a
    script executor component. This last component will execute the CCL commands
    entered in the mmc. By default the mmc will create the manual array using
    all available antennas. It is possible to select the antennas that will be 
    allocated in the manual array using the -a or --antennas options.

    The mmc supports the following options:

    -h, --help
        prints this help document

    -a "ANTENNA1, ANTENNA2, ...", --antennas="ANTENNA1, ANTENNA2, ..."
        creates a manual array using antennas ANTENNA1, ANTENNA2, ...

    --schedblock=SCHEDBLKFILE (Not implemeted yet, coming soon)
        reads a Scheduling Block from the SCHEDBLKFILE and introduce it in the
        CCL execution context

AUTHOR:
    Rafael Hiriart (rhiriart@nrao.edu)

SEE ALSO:
    CCL Reference Manual
"""

def readscript(fn):
    """Read a script from a file.

       Parameters:
           fn - Script file name

       Returns: a string with the script file contents.
    """
    f = open(fn, 'r')
    script = f.read() 
    f.close()
    return script

class ControlWrapper:
    """Wrapper for the CONTROL subsystem components involved in a manual
    mode interaction.
    """
    
    def __init__(self):
        self.client = PySimpleClient()
        
        self.initializeMaster = False
        self.masterName = "CONTROL/MASTER"
        self.master = client.getComponent(self.masterName)
        if (self.master == None):
            print "Unable to activate component '" + masterName + "'. Exiting."
            sys.exit(-1)
    
    def __del__(self):
        print "Releasing ScriptExecutor instance '" + scexecName + "'"
        client.releaseComponent(scexecName)

        print "Releasing ManualArray instance '" + manualArrayName + "'"
        client.releaseComponent(manualArrayName)

        master.destroyArray(manualArrayName)
        
        self.shutdownControlSystem()
        
        self.client.releaseComponent(self.masterName)
        self.client.disconnect()
    
    def initializeControlSystem(self):
        masterState = (master.getMasterState(), master.getMasterSubstate())
        if masterState != (Control.OPERATIONAL, Control.NOERROR):        
            print "Initializing CONTROL subsystem... ",
            self.initializeMaster = True
            master.startupPass1()
            master.startupPass2()
            time.sleep(20)        
            masterState = (master.getMasterState(), master.getMasterSubstate())
            if masterState != (Control.OPERATIONAL, Control.NOERROR):
                print "Error initializing '" + masterName + "'. Exiting."
                sys.exit(-1)
            print "Done"
    
    def shutdownControlSystem(self):
        if self.initializeMaster:
            self.master.shutdownPass1()
            self.master.shutdownPass2()
    
    def createManualArray(self, antennas = []):
    
        availableAntennas = master.getAvailableAntennas()

        if len(antennas) == 0:
            # The default is to use all the available antennas
            arrayAntennas = availableAntennas
        else:
            # Verify that the antennas passed in the CL are available
            wrongAntennas = []    
            for a in antennas:
                if not a in availableAntennas:
                    wrongAntennas.append(a)
            if len(wrongAntennas) > 0:
                print "ERROR: The following antennas are not available: " + str(wrongAntennas)
                print "       Available antennas are: " + str(availableAntennas)
                sys.exit(-2)
                
        arrayAntennas = antennas

        print "Creating a manual array with antennas: " + str(arrayAntennas)

        # Create a Manual array
    
        # This should be fixed!
        for i in range(len(arrayAntennas)):
            if arrayAntennas[i].find('CONTROL/') >= 0:
                arrayAntennas[i] = arrayAntennas[i].replace('CONTROL/', '')
    
        self.manualArrayName = self.master.createManualArray(arrayAntennas)
        self.manualarr = client.getComponent(self.manualArrayName)
        if self.manualarr == None:
            print "Error getting a reference to '" + manualArrayName + "'. Exiting."
            sys.exit(-1)

    def getScriptExecutor(self):
        # Create the ScriptExecutor
        self.scexecName = manualarr.getExecutor()
        self.scexec = client.getComponent(scexecName)
        if self.scexec == None:
            print "Error getting a reference to '" + scexecName + "'. Exiting."
            sys.exit(-1)

    def setSchedulingBlock(self, schedblk):
        # Set the Scheduling Block in the execution context of the ScriptExecutor.
        self.scexec.setXMLSB(schedblk)

if __name__ == "__main__":

    acsBanner = """Atacama Large Milimitter Array
Script Executor CCL Console"""

    script = None
    schedblk = None
    antennas = []

    # Parse the options and arguments
    shortargspec = ""
    longargspec = ['help', 'schedblock=', 'antennas=']
    opts, args = getopt.getopt(sys.argv[1:], shortargspec, longargspec)

    optkeyw = map(lambda t: t[0], opts)
    if '-h' in optkeyw or '--help' in optkeyw:
        help()
        sys.exit(0)

    if len(args) > 0:
        script = readscript(args[0])
    elif len(args) > 1:
        print "WARNING: More than one script has been given as argument.",
        print " mmc will only execute the first script."

    if '--schedblock' in optkeyw:
        sbfname = filter(lambda t: t[0] == '--schedblock', opts)[0][1]
        print "Reading Scheduling Block from file '" + sbfname + "'"
        sbf = open(sbfname, 'r')
        schedblk = sbf.read()
        sbf.close()

    if '--antennas' in optkeyw:
        temp = filter(lambda t: t[0] == '--antennas', opts)[0][1]
        for a in temp.split(','):
            antennas.append(a.strip())
    

    control = ControlWrapper()
    
    control.initializeControlSystem()
    control.createManualArray(antennas)
    scexec = control.getScriptExecutor()
    if schedblk:
        control.setSchedulingBlock(schedblk)

    if script:
        # Run a script passed as argument in the command line.
        print "Running mmc in non-interactive mode. Executing script '" + args[0] + "'"
        runner = ScriptRunner(scexec) 
        runner.run(script)
    else:
        # Run an interactive console.
        con = ACSConsole(scexec)
        con.interact(acsBanner)

# __oOo__