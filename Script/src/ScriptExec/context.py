#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# rhiriart  2006-11-13  created
#

from rm import ResourceManager
from sb import SBObjectifier
from CCL.array import Array
from CCL.schedblock import SB
from Acspy.Util.XmlObjectifier import XmlObject

class ExecutionContext:
    """Execution context.
    
    The execution context is composed by a number of CCL objects and functions 
    that are created and placed in the namespace of the script execution.
    
    This class provides methods to create the initial hierarchy of CCL objects
    given a Configuration object.
    """

    def __init__(self, conf, container):
        # ResourceManager is a singleton. It was initialized
        # elsewhere.
        self.rm = ResourceManager(container)
        self.conf = conf
        self.logger = container.getLogger()
        
        self.locals = {}
        self.globals = {}
        
        array_conf = self.conf.get_array_conf()
        array_name = array_conf.get_parameter('name')
        if len(array_conf.get_parameter('antennas')) > 0:
            antennas = array_conf.get_parameter('antennas').split(',')
        else:
            antennas = []
        self.array = Array(array_name, container, self.rm.get_logger(), antennas)
        # Place the getArray function in the execution context
        self.globals["getArray"] = self.getArray

        self.sb = None        
        # Place the getSB function in the execution context
        self.globals["getSB"] = self.getSB

        self.sb_objectifier = None
        # Place a function to get the SBObjectifier in the execution context
        self.globals["getSBObjectifier"] = self.getSBObjectifier

    def __del__(self):
        # Clean up local namespace.
        # Note: If any object created by the scheduling block script needs to get
        # its destructor called in order to free resources, it can call the destructor
        # explicitly in the script (using 'del obj'). However, in case this is not
        # done, cleaning the local namespace will do it.
        self.clean_locals()
                
    def __str__(self):
#        s = "Locals:\n"
#        for o in self.locals.keys():
#            s = s + ">>> " + o + " = " + str(self.locals[o]) + "\n"
        s = "Locals: " + str(self.locals.keys()) + "\n"
#        s = s + "Globals:\n"
#        for o in self.globals.keys():
#            s = s + ">>> " + o + " = " + str(self.globals[o]) + "\n"
        s = "Globals: " + str(self.globals.keys()) + "\n"
        return s
        
    def add_global_exec_functions(self, executor):
        """Add the ScriptExecutor functions in the execution context.
        """
        
        # Place the Array callbacks function in the execution context
        self.globals["beginExecution"] = executor.beginExecution
        self.globals["endExecution"] = executor.endExecution

        # Place the logging functions in the execution context
        self.globals["logAlert"]     = executor.get_logger().logAlert
        self.globals["logCritical"]  = executor.get_logger().logCritical
        self.globals["logDebug"]     = executor.get_logger().logDebug
        self.globals["logEmergency"] = executor.get_logger().logEmergency
        self.globals["logInfo"]      = executor.get_logger().logInfo
        self.globals["logNotice"]    = executor.get_logger().logNotice
        self.globals["logTrace"]     = executor.get_logger().logTrace
        self.globals["logWarning"]   = executor.get_logger().logWarning
        self.globals["logXML"]       = executor.get_logger().logXML
        self.globals["getLogger"]    = executor.get_logger

    def set_scheduling_block(self, xml):
        self.sb_objectifier = SBObjectifier(xml)
        
    def getArray(self):
        return self.array

    def getSB(self):
        if self.sb_objectifier is not None: 
            return SB(self.sb_objectifier.getSB())
        else:
            return SB(XmlObject("<SchedBlock/>"))

    def getSBObjectifier(self):
        return self.sb_objectifier
    
    def get_locals(self):
        return self.locals
    
    def get_globals(self):
        return self.globals
    
    def clean_locals(self):
        # self.logger.logDebug("Cleaning local objects: " + str(self.locals))
        for obj_name in self.locals.keys():
            del self.locals[obj_name]
        self.locals = {}

#
# ___oOo___
