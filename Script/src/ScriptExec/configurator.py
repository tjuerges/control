#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# rhiriart  2006-11-13  created
#

from exceptions import Exception

class MissingParameter(Exception):
    """Exception to be thrown when there are missing configuration values.
    """
    
    def __init__(self, missing):
        self.missing_keywords = missing
        
    def __repr__(self):
        msg = "Missing configuration information. The following parameters are not defined: "
        msg = msg + str(self.missing_keywords)
        return msg

class MissingResourceConfiguration(Exception):
    def __init__(self, resname):
        self.name = resname
        
    def __repr__(self):
        msg = "Missing configuration. '%s' is not defined." % resname
        return msg

class Configuration:
    """Configuration encapsulates configuration information, which is represented
    as a collection of name/value pairs. Configuration adds the capability to check
    if a given collection is complete. It also has a simple way to handle default
    values.
    """
    
    def __init__(self, conf_name):        
        # Configuration name
        self.name = conf_name
        
        # A map stores the configuration information
        self.conf = {}
        self.required_keys = []

    def get_name(self):
        return self.name

    def set_parameter(self, name, value):
        self.conf[name] = value
        
    def verify(self):
        """Verifies a given configuration.
        The version in the base class just checks that all the parameters defined
        in the required_keys member are present in the configuration.
        Override this function for specialized verifications.
        """
        missing_keywords = []
        kws = self.conf.keys()
        for k in self.required_keys:
            if not k in kws:
                missing_keywords.append(k)
        if not len(missing_keywords) == 0:
            raise MissingParameter, MissingParameter(missing_keywords)

    def get_parameter(self, name):
        if not name in self.conf.keys():
            raise MissingParameter, MissingParameter([name])
        return self.conf[name]
        
    def __str__(self):
        s = "Configuration '" + self.name + "':\n"
        for k in self.conf.keys():
            s = s + ">>> " + k + " = " + self.conf[k] + "\n"
        s = "Required parameters: " + str(self.required_keys)

class ArrayConfiguration(Configuration):
    def __init__(self, confname):
        self.required_keys = ['name', 'compname']
        Configuration.__init__(self, confname)
        # default values
        self.conf['startup'] = 'yes'

class AntennaConfiguration(Configuration):
    def __init__(self, confname):
        self.required_keys = ['name', 'compname']
        Configuration.__init__(self, confname)
        # default values
        self.conf['startup'] = 'no'

class Configurator:
    """This class encapsulates configuration information for the ScriptExecutor.
    Configurations are implemented as maps with string keys and values.
    
    Encapsulating this information in a class allows some flexibility in the
    way this information is gathered. Possible sources are:
    - other components in CONTROL, such as the Master or the Array,
    - the TMCDB,
    - the CDB,
    - test cases,
    - configuration files.
    """
    def __init__(self, confname):
        self.conf_name = confname
        self.configurations = {}
            
    def get_debug_configurator():
        """Returns the Configurator for debugging and test cases. Static method.
        """
        return DEBUG_CONFIGURATOR

    def populate_from_IDL(self, script_exec_conf):
        """Populates a set of configurations from a Script::ScriptExecConf IDL
        structure.
        """
        array_name = script_exec_conf.arrayConfig.arrayName
        array_comp_name = script_exec_conf.arrayConfig.arrayCompName
        antennas = ""
        for a in script_exec_conf.arrayConfig.antennas:
            antennas = antennas + a + ","
        antennas = antennas[:-1]
        
        array_conf = ArrayConfiguration(array_name)
        array_conf.set_parameter('name', array_name)
        array_conf.set_parameter('compname', array_comp_name)
        array_conf.set_parameter('antennas', antennas)
        self.add_configuration(array_conf)

        for a in script_exec_conf.arrayConfig.antennas:
            ant_conf = AntennaConfiguration(a)
            ant_conf.set_parameter('name', a)
            ant_conf.set_parameter('compname', 'CONTROL/'+a)
            ant_conf.set_parameter('startup', 'yes')
            self.add_configuration(ant_conf)
        
        self.verify()

    def get_configurations(self):
        return self.configurations.values()

    def add_configuration(self, conf):
        self.configurations[conf.get_name()] = conf

    def get_configuration(self, conf_name):
        if not conf_name in self.configurations.keys():
            raise MissingResourceConfiguration, MissingResourceConfiguration(conf_name)
        return self.configurations[conf_name]

    def verify(self):
        for c in self.configurations.values():
            c.verify()

    def get_array_conf(self):
        for c in self.configurations.values():
            if isinstance(c, ArrayConfiguration):
                return c

    def get_antenna_conf(self):
        antconfs = []
        for c in self.configurations.values():
            if isinstance(c, AntennaConfiguration):
                antconfs.append(c)
        return antconfs

    def __str__(self):
        s = "Configurator '" + self.conf_name + "':\n"
        for conf in self.configurations:
            s = s + str(conf) + "\n"
        return s

    get_debug_configurator = staticmethod(get_debug_configurator)

DEBUG_CONFIGURATOR = Configurator("Debug Configuration")

DEBUG_ARRAY_CONF = ArrayConfiguration("ArrayDebug01")
DEBUG_ARRAY_CONF.set_parameter('name', 'ArrayDebug01')
DEBUG_ARRAY_CONF.set_parameter('compname', 'CONTROL/AutomaticArrayMock')
DEBUG_ARRAY_CONF.set_parameter('startup', 'yes')
DEBUG_ARRAY_CONF.set_parameter('antennas', 'AntSim01,AntSim02')
DEBUG_CONFIGURATOR.add_configuration(DEBUG_ARRAY_CONF)

DEBUG_ANTENNA01_CONF = AntennaConfiguration("AntSim01")
DEBUG_ANTENNA01_CONF.set_parameter('name', 'AntSim01')
DEBUG_ANTENNA01_CONF.set_parameter('compname', 'CONTROL/AntennaMock01')
DEBUG_ANTENNA01_CONF.set_parameter('startup', 'yes')
DEBUG_CONFIGURATOR.add_configuration(DEBUG_ANTENNA01_CONF)

DEBUG_ANTENNA02_CONF = AntennaConfiguration("AntSim02")
DEBUG_ANTENNA02_CONF.set_parameter('name', 'AntSim02')
DEBUG_ANTENNA02_CONF.set_parameter('compname', 'CONTROL/AntennaMock02')
DEBUG_ANTENNA02_CONF.set_parameter('startup', 'yes')
DEBUG_CONFIGURATOR.add_configuration(DEBUG_ANTENNA02_CONF)

DEBUG_CONFIGURATOR.verify()
    
#
# ___oOo___
