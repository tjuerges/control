#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#

# Three use cases for the ResourceManager
#
# (1) Run CCL objects inside the ScriptExecutor component. In this
#     case use the component ContainerServices.
#       First initialization in the component impl. class:
#           resMng = ResourceManager(self) # In Python ContainerServices is a component
#                                          # base class.
#       Afterwards set the context name:
#           ScriptExec.ResourceManager._contextName = "<COMP-NAME>"
#       Other classes in the component implementation:
#           resMng = ResourceManager() <-- Context is taken from the context name
# (2) Run CCL objects outside the ScriptExecutor component. In this
#     case just use PySimpleClient.
#       Initialization:
#           resMng = ResourceManager(PySimpleClient)
#           context name is taken from the PySimpleClient
#       Use:
#           resMng = ResourceManager()
# (3) Run CCL objects without ACS running. Don't use any ContainerServices.
#       resMng = ResourceManager() or
#       resMng = ResourceManager(None)

# The global context name is always used when we don't pass a
# ContainerServices. The tricky thing is that the CCL objects don't
# know in what of the above use cases they're running. The
# ResourceManager takes care of this using the context name.

from util import LoggerAdaptor

# CCL SimpleClient Context Name
CCL_SC_CTX_NAME = "CCLSimpleClient"
# Set it to 'on' to turn on debugging messages
DEBUG='off'
_contextName = CCL_SC_CTX_NAME

def getContextName():
    return _contextName

def debug(mesg):
    if DEBUG=='on': print(mesg)

class Singleton(object):
    """A Pythonic Singleton.
    The __new__ method is called before __init__.
    The singleton is unique in the context of an ACS component.
    """
    _singletons = {}

    def cleanUp():
        Singleton._singletons.clear()
    cleanUp = staticmethod(cleanUp)
    
    def __new__(cls, *args, **kwargs):
        debug("Singleton.__new__ entering...")
        if len(args) > 0 and args[0] != None:
            name = args[0].getName() # args[0] <-- ContainerServices
        else:
            name = getContextName()
        debug("Context name: " + name)
        if name not in cls._singletons.keys():
            # This call will construct a new object, calling __init__
            # in the derived class.
            debug("Creating new instance for context name " + name)
            cls._singletons[name] = super(Singleton, cls).__new__(cls, *args, **kwargs)
        return cls._singletons[name]

class ResourceManager(Singleton):
    """A singleton that manages a collection of ACS Components.
    """
    
    # This class attribute stores the names of the ResourceManager instances
    # that have been already initialized.
    _initialized = []

    def cleanUp():
        Singleton.cleanUp()
        ResourceManager._initialized = []
    cleanUp = staticmethod(cleanUp)

    def __init__(self, container=None, logger=None):

        debug("ResourceManager.__init__ entering...")        
        if container == None:
            name = getContextName()
        else:
            name = container.getName()

        if not name in ResourceManager._initialized:
            
            if not logger and container:
                self.logger = container.getLogger()
            elif not logger:
                self.logger = LoggerAdaptor(name)
            else:
                self.logger = logger

            if name == CCL_SC_CTX_NAME:
                debug("Getting PySimpleClient")
                from Acspy.Clients.SimpleClient import PySimpleClient
                container = PySimpleClient(CCL_SC_CTX_NAME)

            self.container = container
        
            self.resources = {}
            self.temporaryResources = []

            ResourceManager._initialized.append(name)
                    
    def __del__(self):
        ResourceManager._initialized.remove(self.container.getName())

    def acquireResources(self):
        if self.container is not None:
            for rn in self.resources.keys():
                self.resources[rn] = self.container.getComponent(rn)
    
    def releaseResources(self):
        if self.container is not None:
            for rn in self.resources.keys():
                try:
                    self.container.releaseComponent(rn)
                except:
                    pass

    def getResource(self, resname):
        return self.resources[resname]

    def addResource(self, resname, res=None, isTemporary=False):
        self.resources[resname] = res
        if isTemporary:
            self.temporaryResources.append(resname)

    def deleteResource(self, resname):
        self.resources.pop(resname)
        if resname in self.temporaryResources:
            self.temporaryResources.remove(resname)
    
    def releaseTemporaryResources(self):
        for rn in self.temporaryResources:
            try:
                self.container.releaseComponent(rn)
            except:
                pass
            self.resources.pop(rn)
        self.temporaryResources = []

    def releaseTemporaryResource(self, resName):
        if resName in self.temporaryResources:
            try:
                self.container.releaseComponent(resName)
            except:
                pass
            self.resources.pop(resName)
            self.temporaryResources.remove(resName)

    def getContainerServices(self):
        return self.container


if __name__ == "__main__":
    if getContextName() == CCL_SC_CTX_NAME:
        # Construct the ResourceManager for use case (2), "running
        # CCL objects outside a component".
        from Acspy.Clients.SimpleClient import PySimpleClient
        _client = PySimpleClient(getContextName())
        _resMng = ResourceManager(client)

#
# ___oOo___
