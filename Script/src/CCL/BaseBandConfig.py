#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import APDMSchedBlock
from utils import aggregate

class AbstractBaseBandConfig:
    def __init__(self,
                 BaseBandSpecification,
                 CAMValues = None,
                 DataProducts = None):
        if CAMValues is not None:
            self.cAM = CAMValues
        if DataProducts is not None:
            self.dataProducts = DataProducts
        self.linkBaseBandSpecification(BaseBandSpecification)
        
class BLBaseBandConfig(APDMSchedBlock.BLBaseBandConfig,
                       AbstractBaseBandConfig):
    def __init__(self,
                 BaseBandSpecification,
                 CAMValues = None,
                 DataProducts = None,
                 SpectralWindow = None):
        APDMSchedBlock.BLBaseBandConfig.__init__(self)
        AbstractBaseBandConfig.__init__(self,
                                        BaseBandSpecification,
                                        CAMValues,
                                        DataProducts)
        if SpectralWindow is not None:
            self.BLSpectralWindow = aggregate(SpectralWindow)


class ACABaseBandConfig(APDMSchedBlock.ACABaseBandConfig,
                       AbstractBaseBandConfig):
    def __init__(self,
                 BaseBandSpecification,
                 CAMValues = None,
                 DataProducts = None,
                 SpectralWindow = None):
        APDMSchedBlock.ACABaseBandConfig.__init__(self)
        AbstractBaseBandConfig.__init__(self, BaseBandSpecification, CAMValues,
                                        DataProducts)
        if SpectralWindow is not None:
            self.ACASpectralWindow = aggregate(SpectralWindow)


