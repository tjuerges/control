#! /usr/bin/env python
# "@(#) $Id$"
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2008 - 2010
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import CCL.SchedBlock

_arrayName = ""
_array = None
_schedBlock = None

def setArrayName(arrayName):
    global _arrayName
    _arrayName = arrayName

def loadSB(fileName):
    global _schedBlock
    _schedBlock = CCL.SchedBlock.CreateFromFile(fileName)

def setSB(sbXMLStr):
    global _schedBlock
    _schedBlock = CCL.SchedBlock.CreateFromDocument(sbXMLStr)

def getArray():
    from CCL.Array import Array
    global _array
    if not _array:
        _array = Array(_arrayName)
    return _array

def getSB():
    global _schedBlock
    return _schedBlock

def releaseComponents():
    from Container import releaseComponents
    releaseComponents()

import atexit
atexit.register(releaseComponents)
