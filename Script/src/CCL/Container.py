#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#

from ScriptExec.ResourceManager import ResourceManager

def getComponent(compName):
    rm = ResourceManager()
    try:
        comp = rm.getResource(compName)
        return comp
    except KeyError:
        pass
    comp = rm.getContainerServices().getComponent(compName)
    rm.addResource(compName, comp, True)
    return comp

def getDefaultComponent(compType):
    rm = ResourceManager()
    try:
        comp = rm.getResource(compType)
        return comp
    except KeyError:
        pass
    comp = rm.getContainerServices().getDefaultComponent(compType)
    rm.addResource(compType, comp, True)
    return comp

def getDynamicComponent(name, idl, impl, cont):
    rm = ResourceManager()
    try:
        comp = rm.getResource(idl)
        return comp
    except KeyError:
        pass
    comp = rm.getContainerServices().getDynamicComponent(name,
                                                         idl,
                                                         impl,
                                                         cont)
    rm.addResource(comp._get_name(), comp, True)
    return comp

def getCollocatedComp(comp_spec, mark_as_default, target_comp):
    '''
    See Acspy.Servants.ContainerServices for a description of
    this function and its parameters.
    '''
    rm = ResourceManager()
    try:
        comp = rm.getResource(comp_spec.component_name)
        return comp
    except KeyError:
        pass
    comp = rm.getContainerServices().getCollocatedComp(comp_spec,
                                                       mark_as_default,
                                                       target_comp)
    rm.addResource(comp._get_name(), comp, True)
    return comp

def getComponentNonSticky(comp_name):
    rm = ResourceManager()
    try:
        comp = rm.getResource(comp_name)
        return comp
    except KeyError:
        pass
    comp = rm.getContainerServices().getComponentNonSticky(comp_name)
    return comp    
    
def releaseComponents():
    ResourceManager().releaseTemporaryResources()

def releaseComponent(comp_name):
    ResourceManager().releaseTemporaryResource(comp_name)

def activateOffShoot(comp_obj):
    return ResourceManager().getContainerServices().activateOffShoot(comp_obj)

# No deactivateOffshoot in Python interfaces!!!

def getContainerServices():
    return ResourceManager().getContainerServices()
