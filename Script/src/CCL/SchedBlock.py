#! /usr/bin/env python
# "@(#) $Id$"
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2010 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

from ControlExceptionsImpl import IllegalParameterErrorExImpl
from CCL.utils import aggregate
from CCL.CalibratorCatalogue import CalibratorCatalogue
import pyxb
import APDMEntities.SchedBlock
import APDMSchedBlock

pyxb.namespace.builtin.XMLSchema_instance.ProcessTypeAttribute\
     (pyxb.namespace.builtin.XMLSchema_instance.PT_skip)
pyxb.RequireValidWhenParsing(False)

# IMPORTANT: READ THIS!
# Because the APDM uses a anonymous type to hold the SchedBlock we need
# to deduce which class to inherit/ override.
sbClass = APDMSchedBlock.findImplementingBindingClass("APDM::SchedBlock")

class SchedBlock(sbClass):
    '''
    This is a wrapper around the generated SchedBlock class used to
    supply some of the more complicated functionality that we need.
    '''
    def __init__(self, *args, **kw):
        super(sbClass, self).__init__(*args, **kw)
        self.__allInstrumentSpecs = None
        self.__allObservingParameters = None
        #self.__targetMap = None
        self.__instrumentSpecMap = None
        self.__fieldSourceMap = None
        self._calibratorCatalog = None

    def __getRidOfNamespaces(self, xml):
        import re
        xml = re.sub('sbl:', '', xml)
        return xml

    def expandVirtualSources(self):
        '''
        This method will take any virtual source found in the SB and
        evaluate the query. This is more complicated because the single
        virtual source may evaluate into many concrete sources and we need to
        generate a target for each one.

        Some Details:
        * The virtual field source will be removed
        * The new sources will be given part IDs made up of the original partId
          with a postpended .XXX where X is the number of the returned field
          source in sequence.
        * The original Target will always be removed, we will create however
          many needed targets to accomodate the number of returned sources.
        * The new targets will be inserted into the group list at the point of
          the original target.
        '''

        virtualFieldSourceList = []
        for fs in self.FieldSource:
            if fs.isQuery:
                virtualFieldSourceList.append(fs)

        for vfs in virtualFieldSourceList:
            newFieldSourceIds = self.__getQueryResult(vfs)
            newTargetMap = self.__replaceVirtualTargets(vfs, newFieldSourceIds)
            self.__updateGroupReferences(newTargetmap)
            
            self.FieldSource.remove(vfs)

    def processPlanets(self):
        '''
        Post-cronstruction task to correctly handle planetary sources. This function should
        be called after the ScheBlock object is constructed. It is not possible to call it
        from the constructor itself because pyxb first call the constructor and then it
        parses the object from XML, setting up the fields accordingly. This post-processing
        task need that the fields be already assigned. For this reason, this function is called
        from Observation.SchedulingBlock.SchedulingBlock.
        '''
        for fs in self.FieldSource:
            if fs.isPlanetOrEphemeris():
                fs.processPlanetOrEphemeris()


    def __getQueryResult(self, virtualFieldSource):
        '''
        This method takes a virtual field source and returns a list
        of the entity part Ids of the new sources which have been added to
        the sb.
        '''
        
        # Do the query here
        if self._calibratorCatalog is None:
            self._calibratorCatalog = CalibratorCatalog()

        queryResult = self._calibratorCatalog.query\
                      (virtualFieldSource.QuerySource)
        if len(queryResult) == 0:
            # Throw an exception here to stop execution
            msg = "The query for source %s yeilded no results." % \
                  virtualFieldSource.sourceName
            raise InvalidRequestExImpl(msg)
        
        newEntityPartIds = []
            
        # Change the entity part IDs so they are unique and we
        # know where they came from
        for idx in range(len(queryResult)):
            queryResult[idx].entityPartId = virtualFieldSource.entityPartId +\
                                            (".03d" % idx)
            newEntityPartIds.append(queryResult[idx].entityPartId)
            self.FieldSource.append(queryResult[idx])

            return newEntityPartIds

    def __replaceVirtualTargets(self, virtualFieldSource, newPartIds):
        '''
        This method takes in a Field Source and replaces any target
        referencing it with set of targets referencing the list of
        new part IDs.
        
        Any targets referecing the virtual field source are removed
        as part of this process.
        '''
        newTargetMap = {}
        referencingTargets = self.getReferencingTargets(virtualFieldSource)
        
        # Now find the Targets referencing the virtualFS
        for refTarg in referencingTargets:
            replacementTargets = []
            for idx in range(len(newPartIds)):
                newTarg = copy.copy(refTarg)
                newTarg.entityPartId += (".03d" % idx)
                newTarg.FieldSourceRef.partId = newPartIds[idx]
                self.Target.append(newTarg)
                replacementTargets.append(newTarg.entityPartId)
            newTargetMap[refTarg.entityPartId] = replacementTargets
            self.Target.remove(refTarg)
                
        return newTargetMap
                    
    def __updateGroupReferences(self, newTargetMap):
        '''
        This method will go through each group and replace any targets
        referenced in the keys of the newTargetMap with the set of
        targets associated with it.

        ToDo: Correctly handle the indexes
        '''
        for og in self.ObservingGroup:
            for ot in og.OrderedTarget:
                if ot.TargetRef.partId in newTargetMap:
                    for newRef in newTargetMap[ot.TargetRef.partId]:
                        newOT = copy.copy(ot)
                        newOT.TargetRef.partId = newRef
                        og.OrderedTarget.append(newOT)
                    og.OrderedTarget.remove(ot)

    def getAllInstrumentSpecs(self):
        '''
        Possible Instrument Specs are:
           * OpticalCameraSpec
           * SpectralSpec
        '''
        if self.__allInstrumentSpecs is None:
            self.__allInstrumentSpecs  = self.OpticalCameraSpec
            self.__allInstrumentSpecs += self.SpectralSpec
        return self.__allInstrumentSpecs

    def getAllObservingParameters(self):
        '''
        Possible Observing Parameters are:
           * HolographyParameters
           * ScienceParameters
           * AmplitudeCalParameters
           * AtmosphericCalParameters
           * BandpassCalParameters
           * PhaseCalParameters
           * PointingCalParameters
           * FocusCalParameters
           * DelayCalParameters
        '''
        if self.__allObservingParameters is None:
            self.__allObservingParameters =  self.HolographyParameters
            self.__allObservingParameters += self.ScienceParameters
            self.__allObservingParameters += self.AmplitudeCalParameters
            self.__allObservingParameters += self.AtmosphericCalParameters
            self.__allObservingParameters += self.BandpassCalParameters
            self.__allObservingParameters += self.PhaseCalParameters
            self.__allObservingParameters += self.PointingCalParameters
            self.__allObservingParameters += self.FocusCalParameters
            self.__allObservingParameters += self.DelayCalParameters
        return self.__allObservingParameters

    def getReferencedInstrumentSpec(self, Target):
        '''
        Given a target find the instrument spec that it references.
        '''
        if self.__instrumentSpecMap is None:
            self.__instrumentSpecMap = {}
            for spec in self.getAllInstrumentSpecs():
                self.__instrumentSpecMap[spec.entityPartId] = spec

        try:
            return self.__instrumentSpecMap\
                   [Target.AbstractInstrumentSpecRef.partId]
        except KeyError:
            msg = "Unable to find an Instrument Spec with Part ID: %s"
            raise IllegalParameterErrorExImpl\
                  (msg % Target.AbstractInstrumentSpecRef.partId)

    def getReferencedFieldSource(self, Target):
        '''
        Given a target find the FieldSource it references.
        '''
        if self.__fieldSourceMap is None:
            self.__fieldSourceMap = {}
            for source in self.FieldSource:
                self.__fieldSourceMap[source.entityPartId] = source

        try:
            return self.__fieldSourceMap[Target.FieldSourceRef.partId]
        except KeyError:
            msg = "Unable to find a Field Source with Part ID: %s"
            raise IllegalParameterErrorExImpl\
                  (msg % Target.FieldSourceRef.partId)

    def getReferencedObservingParameters(self, Target):
        '''
        Given a target return all of the reference observing parameters.
        '''
        referencedParams = []
        referenceList = map(lambda ref: ref.partId,
                            Target.ObservingParametersRef)
        for obsParam in self.getAllObservingParameters():
            if obsParam.entityPartId in referenceList:
                referencedParams.append(obsParam)

        if len(referencedParams) == 0:
            msg = "Unable to find Calibration parameters for target"
            raise IllegalParameterErrorExImpl(msg)

        return referencedParams

    def getReferencedTargets(self, targetReferences):
        '''
        Given a list of target references, return all of the
        corresponding targets. Observing groups contain target
        references so this can be used to obtain all the targets in a
        group.
        TODO. Return the targets in the order specified by the index
        field.

        '''
        # references is the partId's of all the targetReferences
        references = map(lambda ref: ref.TargetRef.partId,
                            targetReferences)

        # Now go through all the targets and find those which have a
        # entityPartId that matches the partId in the target reference
        referencedTargets = []
        for target in self.getTargets():
            if target.entityPartId in references:
                referencedTargets.append(target)

        if len(referencedTargets) is not len(targetReferences):
            msg = "Unable to find a target for all the target references"
            raise IllegalParameterErrorExImpl(msg)

        return referencedTargets
    
    def getReferencingTargets(self, Object):
        '''
        Given a referenced object (Cal Param, FieldSource, or InstrumentSpec)
        return a list of all targets which reference this object.

        This method can also accept a list of Objects in which case any
        target that references any object in the parameter list is returned.
        '''
        instrumentSpecIds = []
        fieldSourceIds = []
        obsParamIds = []

        for obj in aggregate(Object):
            if isinstance(obj, APDMEntities._nsgroup.AbstractInstrumentSpecT):
                instrumentSpecIds.append(obj.entityPartId)
            elif isinstance(obj, APDMEntities._nsgroup.FieldSourceT):
                fieldSourceIds.append(obj.entityPartId)
            elif isinstance(obj, APDMEntities._nsgroup.ObservingParametersT):
                obsParamIds.append(obj.entityPartId)
            else:
                msg = "At least one object specified was an unsupported type"
                raise IllegalParameterErrorExImpl(msg)

        targetList = []
        for target in self.Target:
            if target.AbstractInstrumentSpecRef.partId in instrumentSpecIds:
                targetList.append(target)
                continue
            if target.FieldSourceRef.partId in fieldSourceIds:
                targetList.append(target)
                continue
            for obsRef in target.ObservingParametersRef:
                if obsRef in obsParamIds:
                    targetList.append(target)
                    break

        if len(targetList) == 0:
            msg = "Unable to find any targets referencing objects"
            raise IllegalParameterErrorExImpl(msg)
        return targetList

    # ----------- All of the Following are really no longer needed --------
    def getSpectralSpecs(self):
        """
        Returns a list with all the Spectral Specs in the scheduling block
        represented as DOM Elements
        """
        return self.SpectralSpec
    
    def getObservingMode(self):
        return str(self.modeName)

    def getMaximumExecutionTime(self):
        return self.SchedBlockControl.sBMaximumTime.get()
        
    def getTargets(self):
        """
        Returns a list with all the Targets in the scheduling block,
        represented as DOM Elements.
        """
        return self.Target

    def getGroups(self):
        """
        Returns a list with all of the groups in the scheduling block.
        """
        return self.ObservingGroup
    
    def getExpertParameters(self):
        """
        Returns the list of expert parameters associated with the SB.
        Represented as DOM Elements.
        """
        return self.expertParameter


    def getFieldSources(self):
        '''
        Returns a list with all the FieldSources in the scheduling block,
        wrapped as SubscanFieldSource objects.
        '''
        return self.FieldSource

sbClass._SetSupersedingClass(SchedBlock)

def CreateFromDocument(xmlString):
    '''
    This is a helper funciton to simplify creating documents it
    is equivelent to calling the same method on the
    APDMEntities.SchedBlock module.
    '''
    return APDMEntities.SchedBlock.CreateFromDocument(xmlString)

def CreateFromFile(filename):
    '''
    This is a helper function to simplify creating documents.  filename
    should contain the complete path to the file.
    '''
    f = open(filename)
    xmlString = f.read()
    f.close()
    return APDMEntities.SchedBlock.CreateFromDocument(xmlString)

    
