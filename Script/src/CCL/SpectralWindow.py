#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import APDMSchedBlock

class AbstractSpectralWindow:
    def __init__(self,
                 CenterFrequency = None,
                 EffectiveBandwidth = None,
                 EffectiveNumberOfChannels = None,
                 SideBand = None,
                 PolarizationProducts = None,
                 ChannelAverageRegion = None):
        if CenterFrequency is not None:
            self.centerFrequency.set(CenterFrequency)
        if EffectiveBandwidth is not None:
            self.effectiveBandwidth.set(EffectiveBandwidth)
        if EffectiveNumberOfChannels is not None:
            self.effectiveNumberOfChannels = EffectiveNumberOfChannels
        if SideBand is not None:
            self.sideBand = SideBand
        if PolarizationProducts is not None:
            self.polnProducts = PolarizationProducts
        if ChannelAverageRegion is not None:
            self.ChannelAverageRegion = aggregate(ChannelAverageRegion)
                    
class BLSpectralWindow(APDMSchedBlock.BLSpectralWindow,
                       AbstractSpectralWindow):    
    def __init__(self,
                 CenterFrequency = None,
                 EffectiveBandwidth = None,
                 EffectiveNumberOfChannels = None,
                 SideBand = None,
                 PolarizationProducts = None,
                 ChannelAverageRegion = None):
        APDMSchedBlock.BLSpectralWindow.__init__(self)
        AbstractSpectralWindow.__init__(self,
                                        CenterFrequency,
                                        EffectiveBandwidth,
                                        EffectiveNumberOfChannels,
                                        SideBand,
                                        PolarizationProducts,
                                        ChannelAverageRegion)
    

class ACASpectralWindow(APDMSchedBlock.ACASpectralWindow,
                       AbstractSpectralWindow):    
    def __init__(self,
                 CenterFrequency = None,
                 EffectiveBandwidth = None,
                 EffectiveNumberOfChannels = None,
                 SideBand = None,
                 PolarizationProducts = None,
                 ChannelAverageRegion = None):
        APDMSchedBlock.ACASpectralWindow.__init__(self)
        AbstractSpectralWindow.__init__(self,
                                        CenterFrequency,
                                        EffectiveBandwidth,
                                        EffectiveNumberOfChannels,
                                        SideBand,
                                        PolarizationProducts,
                                        ChannelAverageRegion)

