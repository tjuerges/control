#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#

import ControlExceptions
import Acspy.Util.XmlObjectifier

def getEnumeration(inputValue, enumerationMod):
    '''
    This utility method attempts to translate an input into a member of
    the defined enumeration module.  If it cannot coerce the input a
    ControlExcptions..IllegalParameterErrorEx is raised.

    Vaild Input types are:
      * Acspy.Util.XmlObjectifier.XmlElement with the enumeration as
        a string stored as the value.
      * A string (case insensitive)
      * The enumeration proper
    '''
    # Translate XmlObjects to a String
    if isinstance(inputValue, Acspy.Util.XmlObjectifier.XmlElement):
        inputValue = inputValue.value

    # Translate the enumeration to a String
    if not isinstance(inputValue,str):
        inputValue = str(inputValue)

    try:
        return enumerationMod.__getattribute__(inputValue.upper())
    except AttributeError, ex:
        raise ControlExceptions.IllegalParameterErrorEx(inputValue +
              ' is not a member of the ' + enumerationMod.__name__ +
                                                        ' enumeration')

def getBoolean(inputValue):
    '''
    This utility method attempts to translate an input into a
    boolean value.  If it cannot coerce the input a
    ControlExcptions::IllegalParameterErrorEx is raised.

    Vaild Input types are:
      * Acspy.Util.XmlObjectifier.XmlElement with a boolean value
      * An integer (0 = false everything else is true)
      * A string (case insensitive), T and F work as well
      * Python boolean values
    '''

    if isinstance(inputValue, bool):
        return inputValue
    
    # Translate XmlObjects to a String
    if isinstance(inputValue, Acspy.Util.XmlObjectifier.XmlElement):
        inputValue = inputValue.getValue()

    # Handle integer values here
    if isinstance(inputValue, int):
        return bool(int)

    if not isinstance(inputValue, str):
        inputValue = str(inputValue)

    if inputValue.upper() == "T" or inputValue.upper() == "TRUE":
        return True
    elif inputValue.upper() == "F" or inputValue.upper() == "FALSE":
        return False
    else:
        raise ControlExceptions.IllegalParameterErrorEx("Unable to convert "+
               inputValue + " to a boolean.")

  
