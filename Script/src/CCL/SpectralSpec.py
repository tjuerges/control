#! /usr/bin/env python
# @(#) $Id$
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2008 - 2010
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

from utils import aggregate
import APDMSchedBlock
import Control
from ControlExceptionsImpl import IllegalParameterErrorExImpl
from ControlExceptionsImpl import InvalidRequestExImpl
import CorrelatorConfiguration

# The object inheratence allows definition of class methods
class SpectralSpec(APDMSchedBlock.SpectralSpec):
    def __init__(self):
        APDMSchedBlock.SpectralSpec.__init__(self)
        if self.name is None:
            self.name = ''
            
    def addBaseband(self, BaseBandSpecification, BaseBandConfig = None):
        if self.hasCorrelatorConfiguration() and BaseBandConfig is None:
            msg = "You must add a BaseBand Configuration as well as " +\
                  "the specification since this spectral spec has a " +\
                  "Correlator Configuration"
            raise IllegalParameterErrorExImpl(msg)

        if len(self.FrequencySetup.BaseBandSpecification) > 3:
            msg = "Unable to add baseband, all basebands are already in use"
            raise InvalidRequestExImpl(msg)
            
        
        for bbs in self.FrequencySetup.BaseBandSpecification:
            if BaseBandSpecification.baseBandName == bbs.baseBandName:
                msg = "Unable to add new specification for baseband: " +\
                      BaseBandSpecification.baseBandName + \
                      " the baseband is already in use."
                raise IllegalParameterErrorExImpl(msg)

        self.FrequencySetup.BaseBandSpecification.append(BaseBandSpecification)

        if BaseBandConfig is not None:

            BaseBandConfig.linkBaseBandSpecification(BaseBandSpecification)
            if self.hasBLCorrelatorConfiguration():
                self.BLCorrelatorConfiguration.BLBaseBandConfig.\
                                              append(BaseBandConfig)
            elif self.hasACACorrelatorConfiguration():
                self.ACACorrelatorConfiguration.ACABaseBandConfig.\
                                              append(BaseBandConfig)

    def setLOOffsetting(self,offsettingMode=Control.LOOffsettingMode_NONE):
        if isinstance(offsettingMode, str):
            if offsettingMode.upper() == 'NONE' or \
                   offsettingMode.upper() == "LOOffsettingMode_NONE":
                offsettingMode=Control.LOOffsettingMode_NONE
            elif offsettingMode.upper() == 'TWO_LOS' or \
                   offsettingMode.upper() == "LOOffsettingMode_TWO_LOS":
                offsettingMode=Control.LOOffsettingMode_TWO_LOS
            elif offsettingMode.upper() == 'THREE_LOS' or \
                   offsettingMode.upper() == "LOOffsettingMode_THREE_LOS":
                offsettingMode=Control.LOOffsettingMode_THREE_LOS

        if not self.hasCorrelatorConfiguration():
            msg = "You must have a Correlator Configuration to"+\
                  " set the LO offsetting mode."
            raise IllegalParameterErrorExImpl(msg)

        if offsettingMode == Control.LOOffsettingMode_NONE:
            self.getCorrelatorConfiguration().lOOffsettingMode = u'NONE'
        elif offsettingMode == Control.LOOffsettingMode_TWO_LOS:
            self.getCorrelatorConfiguration().lOOffsettingMode = u'TWO_LOS'
        elif offsettingMode == Control.LOOffsettingMode_THREE_LOS:
            self.getCorrelatorConfiguration().lOOffsettingMode = u'THREE_LOS'
        else:
            msg = "Invalid offsettingMode, NONE, TWO_LOS,"+\
                  "THREE_LOS and their "+\
                  "Control.LOOffsettingMode enumerations are allowed."
            raise IllegalParameterErrorExImpl(msg)

    def getLOOffsettingMode(self) :
        if self.hasCorrelatorConfiguration():
            return self.getCorrelatorConfiguration().lOOffsettingMode
        else :
            msg = "You must have a Correlator Configuration to set the LO offsetting mode."
            raise IllegalParameterErrorExImpl(msg)        

    def set180degSwitching(self,turnOn=False) :
        if self.hasCorrelatorConfiguration():
            self.getCorrelatorConfiguration().enable180DegreeWalshFunction = turnOn
        else :
            msg = "You must have a Correlator Configuration to set the LO offsetting mode."
            raise IllegalParameterErrorExImpl(msg)        

    def get180degSwitching(self) :
        if self.hasCorrelatorConfiguration():
            return self.getCorrelatorConfiguration().enable180DegreeWalshFunction
        else :
            msg = "You must have a Correlator Configuration to set the LO offsetting mode."
            raise IllegalParameterErrorExImpl(msg)        
        
      
    @staticmethod    
    def CreateBLCorrelatorSpec(dump = 0.096,
                               channelAverage = 0.96,
                               integration = 5.0,
                               frequency='86.243 GHz',
                               corrMode = 'FDM',
                               autoOnly = False,
                               channelAverageList = None,
                               bwd = 1.0,
                               pol= '1X',
                               baseband = ['BB_1'],
                               basebandFrequency = None,
                               if2Freq='3000 MHz',
                               apc='AP_UNCORRECTED',
                               obsMode = None):
        
        '''
        This method is a port of the AIV Spectral Spec to the new APDM
        framework.
        '''
        
        # Paramter translations
        polProducts = {'1X':'XX', '1Y':'YY', '2':'XX,YY', '4':'XX,YY,XY,YX'}
        chanDivisor = {'1X':1, '1Y':1, '2':2, '4':4}
        minDumpDuration = {'1X':0.048, '1Y':0.048, '2':0.048, '4':0.048}
        minChannelAvg   = {'1X':0.048, '1Y':0.048, '2':0.048, '4':0.048}

        # Imports
        from BaseBandSpecification import BaseBandSpecification
        from BaseBandConfig import BLBaseBandConfig
        from SpectralWindow import BLSpectralWindow
        from ChannelAverageRegion import ChannelAverageRegion

        # Parameter Checking
        baseband = aggregate(baseband)

        
        if basebandFrequency is not None:
            basebandFrequency = aggregate(basebandFrequency)
            if len(basebandFrequency) != 1 and \
               len(basebandFrequency) != len(baseband):
                msg = "Mismatch between the number of baseband frequencies " +\
                      "and the number of basebands.  The number of " +\
                      "frequencies specified must be 0, 1 or equal to the "+\
                      "number of basebands."
                raise IllegalParameterErrorExImpl(msg)

        if channelAverageList is not None:
            if len(channelAverageList) != 1 and \
               len(channelAverageList) != len(baseband):
                msg = "Mismatch between the number of channel average " +\
                      "regions and the number of basebands.  The number of "+\
                      "channel average regions specified must be 0, 1 or "+\
                      "equal to the number of basebands."
                raise IllegalParameterErrorExImpl(msg)
     
        ss = SpectralSpec()

        # Now setup the Frequency Setup
        ss.FrequencySetup.restFrequency.set(frequency)
        ss.FrequencySetup.dopplerReference = u'topo'
        ss.FrequencySetup.receiverBand="ALMA_RB_03"

        # Add a Baseline Correlator Configuration
        ss.BLCorrelatorConfiguration = CorrelatorConfiguration.\
            BLCorrelatorConfiguration(integration, channelAverage, apc, dump)

        if ss.BLCorrelatorConfiguration.dumpDuration.get() < \
           minDumpDuration[pol]:
            ss.BLCorrelatorConfiguration.dumpDuration.set(minDumpDuration[pol])
        if ss.BLCorrelatorConfiguration.channelAverageDuration.get() < \
               minChannelAvg[pol]:
            ss.BLCorrelatorConfiguration.channelAverageDuration.\
                                                       set(minChannelAvg[pol])

        # Now add each baseband
        for idx in range(len(baseband)):
            if basebandFrequency is None:
                bbFreq = frequency
            elif len(basebandFrequency) == 1:
                bbFreq = basebandFrequency[0]
            else:
                bbFreq = basebandFrequency[idx]
                
            bbs = BaseBandSpecification(BaseBandName = baseband[idx],
                                        Frequency = bbFreq)
            #ss.FrequencySetup.BaseBandSpecification.append(bbs)

            bbc = BLBaseBandConfig(bbs, 'NORMAL')
            if autoOnly:
                bbc.dataProducts = 'AUTO_ONLY'

            # Now we need to add a spectral window
            if channelAverageList is None:
                chanAvgRegList = None
            elif len(channelAverageList) == 1:
                chanAvgRegList = channelAverageList[0]
            else:
                chanAvgRegList = channelAverageList[idx]

            sw = BLSpectralWindow(if2Freq)
            sw.polnProducts=polProducts[pol]
            if corrMode == 'TDM':
                sw.effectiveNumberOfChannels=256/chanDivisor[pol]
                if chanAvgRegList is None:
                    chanAvgRegList = [[0,256/chanDivisor[pol]]]
            elif corrMode == 'FDM':
                sw.effectiveNumberOfChannels=7680/chanDivisor[pol]
                if chanAvgRegList is None:
                    chanAvgRegList = [[0,7680/chanDivisor[pol]]]
                sw.effectiveBandwidth.set(1875.0e6/bwd)
            else:
                msg = "Unrecognized Correlator mode: " + str(corrMode)
                raise IllegalParameterErrorExImpl(msg)

            for region in chanAvgRegList:
                sw.ChannelAverageRegion.append\
                                   (ChannelAverageRegion(region[0], region[1]))
            sw.sideBand = u'USB'
            bbc.BLSpectralWindow.append(sw)

            ss.addBaseband(bbs, bbc)

        ss.roundTimes()        
        if obsMode is not None:
            lo = obsMode.getLOObservingMode()
            lo.setSpectrum(ss)
            ts = lo.getTuningSolution(lo.currentTuningSolution())
            ss.FrequencySetup.receiverBand= str(ts.receiverBand)
            ss.FrequencySetup.lO1Frequency.set((ts.LSFrequency+ts.FLOOGFrequency)*ts.coldMultiplier)
            for bb_id in range(len(baseband)) :
                bbc = ss.BLCorrelatorConfiguration.BLBaseBandConfig[bb_id]
                bbs = ss.FrequencySetup.BaseBandSpecification[bb_id]
                bb_ts = ts.LO2[bb_id]
                #bbc.BLSpectralWindow[0].sideBand = \
                #                          u'%s' % str(bb_ts.skyFreqSideband)
                sw = bbc.BLSpectralWindow[0]
                sw.sideBand = u'%s' % str(bb_ts.skyFreqSideband)
                if bb_ts.tuneHigh : signVal = 1
                else : signVal = -1
                bbs.lO2Frequency.set(bb_ts.DYTOFrequency+signVal*bb_ts.FTSFrequency)
        return ss




##     def roundTimes(self):
##         """
##         If a correlator configuration is defined:
##         This method will first call round times on the CorrelatorConfig
##         (if one is defined) and then adjust the subscan duration to be
##         a multiple of the integration time.

##         If a square law detector config is defined:
##         This will round the subscan duration to the nearest multiple of
##         48 ms. This is necessary for the pointing data to be of the proper
##         length.
##         """
##         if self.hasCorrConfig():
##             cc = self.getCorrelatorConfig()
##             cc.roundTimes()
##             self.setCorrelatorConfig(cc)

##             int = cc.getIntegrationDuration()
##             sub = self.getSubscanDuration()
##             sub = max(int, round(sub/int) * int)
##             self.setSubscanDuration(sub)

##         elif self.hasSquareLawConfig():
##             sub = self.getSubscanDuration()
##             sub = max(0.048, round(sub/0.048) * 0.048)
##             self.setSubscanDuration(sub)


##     def validate(self, NumAntennas):
##          """
##          This method will use the Corelator Validator to ensure that
##          the spectral spec is valid.  The number of antennas
##          must be passed in.
##          """
##          import Acspy.Clients.SimpleClient
##          client = Acspy.Clients.SimpleClient.PySimpleClient()
##          ssObj = Acspy.Util.XmlObjectifier.XmlObject(self.__xml)
##          ssXml = str(ssObj.SubscanSpectralSpec.SpectralSpec.toxml())
##          try:
##              if self.hasBLCorrConfig():
##                  validator =client.getComponent('CORR/CONFIGURATION_VALIDATOR')
##                  result = validator.validateConfigurationSS(ssXml, NumAntennas)
##              if self.hasACACorrConfig():
##                  validator = client.getComponent('ACACORR/CONFIGURATION_VALIDATOR')
##                  result = validator.validateConfigurationSS(ssXml, NumAntennas)
##          except:
##              msg =  "Unable to validate configuration!"
##              self._logger.logNotSoTypeSafe(ACSLog.ACS_LOG_WARNING, msg,
##                                             OPERATOR)
##              return

##          if (result[0]):
##              msg =  "Correlator Configuration Valid"
##              self._logger.logNotSoTypeSafe(ACSLog.ACS_LOG_INFO, msg,
##                                             OPERATOR)
##          else:
##              msg =""
##              for error in result[1]:
##                  msg += error + "\n"
##              self._logger.logNotSoTypeSafe(ACSLog.ACS_LOG_INFO, msg,
##                                            OPERATOR)
