#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import xmlentity
import Acspy.Util.XmlObjectifier
import ControlExceptionsImpl
import CCL.SIConverter
import math
import re

class RectanglePattern(object):
    """
    This class encapsulates a Rectangular Pattern suitable for raster
    scans.
    """

    def __init__(self, rectanglePattern = None):
        """
        Initialize a rectangular pattern.

        If a rectanglePattern object is passed in the other fields are ignored.
        """
        if rectanglePattern is None:
            self.__xml = self.__getDefaultRectanglePattern().toxml()
        else:
            self.__xml = rectanglePattern.toxml()

        # The following bit of code rips the Namespaces out of the
        # xml so the parser is happy.
        self.__xml = re.sub('ns[0-9]+:', '', self.__xml)
        self.__xml = re.sub('xmlns:ns[0-9]+=".*"', '', self.__xml)

        self._biDirectional = False

    def getCenterCoordinates(self):
        """
        Get the center coordinates in the currently specified system.
        
        Coordinates are always returned as a tuple (longitude, latitude)
        """
        obj = Acspy.Util.XmlObjectifier.XmlObject(self.__xml).RectanglePattern
        pCC = obj.patternCenterCoordinates
        return (CCL.SIConverter.toRadians(pCC.longitude),
                CCL.SIConverter.toRadians(pCC.latitude))

    def setCenterCoordinates(self, longitude, latitude):
        """
        Set the center coordinates of the pattern in the currently specified
        system.
        """
        long = CCL.SIConverter.toRadians(longitude)
        lat = CCL.SIConverter.toRadians(latitude)
        obj = Acspy.Util.XmlObjectifier.XmlObject(self.__xml).RectanglePattern
        pCC = obj.patternCenterCoordinates
        pCC.longitude.setValue(long)
        pCC.longitude.setAttribute("unit", "rad")
        pCC.latitude.setValue(lat)
        pCC.latitude.setAttribute("unit", "rad")
        self.__xml = obj.toxml()
        
    def getCenterCoordinateSystem(self):
        """
        Get the coordinate system that the Center Coordinates are
        specified in.
        """
        obj = Acspy.Util.XmlObjectifier.XmlObject(self.__xml).RectanglePattern
        pCC = obj.patternCenterCoordinates
        return pCC.getAttribute("system")

    def setCenterCoordinateSystem(self, system):
        """
        Set the coordinate system that the Center Coordinates are
        specified in.

        Options:
          B1950    - Not Supported (yet)
          J2000
          appradec - Not Supported (yet)
          galactic - Not Supported (yet)
          ecliptic - Not Supported (yet)
          horizon
          azel     - Not Supported (yet)
          hadec    - Not Supported (yet)
          mount    - Not Supported (yet)
        """
        validOptions = [ "B1950", "J2000", "appradec", "galactic",
                         "ecliptic", "horizon", "azel", "hadec", "mount"]
        supportedOptions = ["J2000", "horizon"]
        if validOptions.count(system) == 0:
            ex = ControlExceptionsImpl.IllegalParameterErrorExImpl()
            ex.setData("Message", "The system %s is not a valid option" %
                       system)
            raise ex
        if supportedOptions.count(system) == 0:
            ex = ControlExceptionsImpl.IllegalParameterErrorExImpl()
            ex.setData("Message",
                       "The system %s is not a (currently) supported option" %
                       system)
            raise ex
        obj = Acspy.Util.XmlObjectifier.XmlObject(self.__xml).RectanglePattern
        pCC = obj.patternCenterCoordinates
        pCC.setAttribute("system", system)
        self.__xml = obj.toxml()

    def getCenterCoordinateType(self):
        """
        Get the coordinate system type this is either ABSOLUTE or
        RELATIVE.
        """
        obj = Acspy.Util.XmlObjectifier.XmlObject(self.__xml).RectanglePattern
        pCC = obj.patternCenterCoordinates
        return pCC.getAttribute("type")

    def setCenterCoordinateType(self, type):
        """
        Get the coordinate system type this is either ABSOLUTE or
        RELATIVE.

        Options:
           ABSOLUTE
           RELATIVE
        """
        validOptions = ["ABSOLUTE", "RELATIVE"]
        if validOptions.count(type) == 0:
            ex = ControlExceptionsImpl.IllegalParameterErrorExImpl()
            ex.setData("Message", "The type %s is not a valid option" %
                       type)
            raise ex
        obj = Acspy.Util.XmlObjectifier.XmlObject(self.__xml).RectanglePattern
        pCC = obj.patternCenterCoordinates
        pCC.setAttribute("type", type)
        self.__xml = obj.toxml()

    def getLongitudeLength(self):
        """
        Return the length of the rectangle pattern in longitude.
        """
        obj = Acspy.Util.XmlObjectifier.XmlObject(self.__xml).RectanglePattern
        return CCL.SIConverter.toRadians(obj.longitudeLength)
        
    def setLongitudeLength(self, length):
        """
        Set the length of the rectangle pattern in latitude.
        """
        obj = Acspy.Util.XmlObjectifier.XmlObject(self.__xml).RectanglePattern
        obj.longitudeLength.setValue(CCL.SIConverter.toRadians(length))
        obj.longitudeLength.setAttribute("unit","rad")
        self.__xml = obj.toxml()
        
    def getLatitudeLength(self):
        """
        Return the length of the rectangle pattern in latitude.
        """
        obj = Acspy.Util.XmlObjectifier.XmlObject(self.__xml).RectanglePattern
        return CCL.SIConverter.toRadians(obj.latitudeLength)
        
    def setLatitudeLength(self, length):
        """
        Set the length of the rectangle pattern in latitude.
        """
        obj = Acspy.Util.XmlObjectifier.XmlObject(self.__xml).RectanglePattern
        obj.latitudeLength.setValue(CCL.SIConverter.toRadians(length))
        obj.latitudeLength.setAttribute("unit","rad")
        self.__xml = obj.toxml()
    
    def getOrthogonalStep(self):
        """
        Return size of the step in the orthogonal direction.
        """
        obj = Acspy.Util.XmlObjectifier.XmlObject(self.__xml).RectanglePattern
        return CCL.SIConverter.toRadians(obj.orthogonalStep)
        
    def setOrthogonalStep(self, length):
        """
        Set the size of the step in the orthogonal direction.
        """
        obj = Acspy.Util.XmlObjectifier.XmlObject(self.__xml).RectanglePattern
        obj.orthogonalStep.setValue(CCL.SIConverter.toRadians(length))
        obj.orthogonalStep.setAttribute("unit","rad")
        self.__xml = obj.toxml()

    def getOrientation(self):
        """
        Return the position angle of the rectangle in the scanning
        coordinate frame.
        """
        obj = Acspy.Util.XmlObjectifier.XmlObject(self.__xml).RectanglePattern
        return CCL.SIConverter.toRadians(obj.orientation)
        
    def setOrientation(self, angle):
        """
        Set the position angle of the rectangle in the scanning
        coordinate frame.
        """
        obj = Acspy.Util.XmlObjectifier.XmlObject(self.__xml).RectanglePattern
        obj.orientation.setValue(CCL.SIConverter.toRadians(angle))
        obj.orientation.setAttribute("unit","rad")
        self.__xml = obj.toxml()

    def getScanDirection(self):
        """
        Get the direction of the scans this is either longitude or
        latitude.
        """
        obj = Acspy.Util.XmlObjectifier.XmlObject(self.__xml).RectanglePattern
        return obj.getAttribute("scanDirection")

    def setScanDirection(self, direction):
        """
        Set the direction of the scans.

        Options:
           longitude
           latitude
        """              
        validOptions = ["longitude", "latitude"]
        if validOptions.count(direction) == 0:
            ex = ControlExceptionsImpl.IllegalParameterErrorExImpl()
            ex.setData("Message", "The direction %s is not a valid option" %
                       direction)
            raise ex
        obj = Acspy.Util.XmlObjectifier.XmlObject(self.__xml).RectanglePattern
        obj.setAttribute("scanDirection", direction)
        self.__xml = obj.toxml()

    def getScanCoordinateSystem(self):
        """
        Get the coordinate system that the Center Coordinates are
        specified in.
        """
        obj = Acspy.Util.XmlObjectifier.XmlObject(self.__xml).RectanglePattern
        return obj.getAttribute("scanningCoordinateSystem")


    def setScanCoordinateSystem(self, system):
        """
        Set the coordinate system the rectangle pattern in which the pattern
        is defined.

        Options:
          B1950    - Not Supported (yet)
          J2000
          appradec - Not Supported (yet)
          galactic - Not Supported (yet)
          ecliptic - Not Supported (yet)
          horizon
          azel     - Not Supported (yet)
          hadec    - Not Supported (yet)
          mount    - Not Supported (yet)
        """
        validOptions = [ "B1950", "J2000", "appradec", "galactic",
                         "ecliptic", "horizon", "azel", "hadec", "mount"]
        supportedOptions = ["J2000", "horizon"]
        if validOptions.count(system) == 0:
            ex = ControlExceptionsImpl.IllegalParameterErrorExImpl()
            ex.setData("Message", "The system %s is not a valid option" %
                       system)
            raise ex
        if supportedOptions.count(system) == 0:
            ex = ControlExceptionsImpl.IllegalParameterErrorExImpl()
            ex.setData("Message",
                       "The system %s is not a (currently) supported option" %
                       system)
            raise ex
        obj = Acspy.Util.XmlObjectifier.XmlObject(self.__xml).RectanglePattern
        obj.setAttribute("scanningCoordinateSystem", system)
        self.__xml = obj.toxml()

    def setBidirectionalScan(self, biDirectional):
        """
        Method to set if the map is done with uni-directional strokes
        or bi-directional.  Set to True to allow bi-directional strokes.
        """
        self._biDirectional=biDirectional

    def getBidirectionalScan(self):
        """
        Method to return is the rectangular pattern is preformed with
        uni-directional or bi-directional strokes.
        """
        return self._biDirectional

    def generateSubscanList(self, subscanDuration):
        """
        This method generates a list of tuples
        (longOffsetStart, latOffsetStart, longLength, latLength)
        which define the strokes for this raster map.

        The subscan duration is needed to covert the length of strokes
        into velocities.
        """
        latLength  = self.getLatitudeLength()
        longLength = self.getLongitudeLength()
        orthStep   = self.getOrthogonalStep()

        longStart =  -longLength/2.0
        latStart  =  -latLength/2.0

        unrotatedList = []
        if self.getScanDirection() == "longitude":
            nSubscans = int(round(latLength/orthStep)) + 1
            for subscan in range(nSubscans):
                offset = latStart + (orthStep * subscan)
                if self.getBidirectionalScan() and subscan % 2 != 0:
                    unrotatedList.append((-longStart, offset, -longLength, 0))
                else:
                    unrotatedList.append((longStart, offset, longLength, 0))
                
        elif self.getScanDirection() == "latitude":
            nSubscans = int(round(longLength/orthStep)) + 1
            for subscan in range(nSubscans):
                offset = longStart + (orthStep * subscan)
                if self.getBidirectionalScan() and subscan % 2 != 0:
                    unrotatedList.append((offset, -latStart, 0, -latLength))
                else:
                    unrotatedList.append((offset, latStart, 0, latLength))
        else:
            ex = ControlExceptionsImpl.IllegalParameterErrorExImpl()
            ex.setData("Message",
                       "Unrecognized scan direction '%s' in rectangle pattern"%
                       self.getScanDirection())
            raise ex

        # Now to run this through a rotation matrix
        rotatedList = []
        co = math.cos(self.getOrientation())
        so = math.sin(self.getOrientation())
        for scan in unrotatedList:
            rotatedList.append(((scan[0]*co) - (scan[1] * so),
                                (scan[0]*so) + (scan[1] * co),
                                (scan[2]*co) - (scan[3] * so),
                                (scan[2]*so) + (scan[3] * co)))


        # Now apply any offset of the centerpoint
        if self.getCenterCoordinateType() == "RELATIVE":
            (longOffset, latOffset) = self.getCenterCoordinates()
        else:
            # TODO get this right, will probably need to pass in the
            # Field Source object
            #ex = ControlExceptionsImpl.IllegalParameterErrorExImpl()
            #ex.setData("Message",
            #           "ABSOLUTE patterns are not yet supported.")
            #raise ex
            print "ABSOLUTE patterns are not yet supported" + \
                  " pretending you meant REALITIVE"
            (longOffset, latOffset) = self.getCenterCoordinates()
            
        subscanList = []
        for scan in rotatedList:
            subscanList.append((scan[0] + longOffset, scan[1] + latOffset,
                                scan[2]/subscanDuration,
                                scan[3]/subscanDuration))

        return subscanList

    def getNode(self):
        return Acspy.Util.XmlObjectifier.XmlObject(self.__xml).RectanglePattern
    
    def __getDefaultRectanglePattern(self):
        defaultXML = """
        <RectanglePattern xmlns:avt='Alma/ValueTypes'
                          type="rectangle"
                          scanDirection="longitude"
                          scanningCoordinateSystem="azel">
            <patternCenterCoordinates system="J2000" type="RELATIVE">
                <avt:longitude unit="deg">0.0</avt:longitude>
                <avt:latitude unit="deg">0.0</avt:latitude>
            </patternCenterCoordinates>
            <longitudeLength unit="deg">0.0</longitudeLength>
            <latitudeLength unit="deg">0.0</latitudeLength>
            <orthogonalStep unit="arcsec">0.0</orthogonalStep>
            <orientation unit="deg">0.0</orientation>
        </RectanglePattern>
        """
        return Acspy.Util.XmlObjectifier.XmlObject(defaultXML).RectanglePattern
