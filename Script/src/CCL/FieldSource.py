#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

from AcsutilPy.FindFile import findFile

import ControlExceptionsImpl
import APDMSchedBlock
import APDMValueTypes

class EquatorialSource(APDMSchedBlock.FieldSource):
    def __init__(self, ra, dec, sourceName=None, pmRA=0.0, pmDec=0.0,
                 parallax=0.0, system="J2000"):
        '''
        This creates an Equatorial source, any string entries are parsed
        using the standard CCL libraries.  As a reminder for ra and dec
        entries can be in:
           hours-minutes-seconds e.g., "11:20:30.123"  (uses a ':' delimiter)
           degrees-minutes-seconds e.g., "160.20.30.123" (uses a '.' delimiter)
           degrees e.g. " 160.34521 deg"
        Any numeric entry is assumed to be in radians
        '''
        APDMSchedBlock.FieldSource.__init__(self)
        self.setRADec(ra, dec)
        self.setProperMotion(pmRA, pmDec)
        self.parallax.set(parallax)
        self.setSystem(system)
        if sourceName is not None:
            self.sourceName= sourceName
        self.nonSiderealMotion = False
        
        # We need a field pattern; create a Pointing Pattern
        fp = APDMSchedBlock.PointingPattern()
        sc = APDMValueTypes.SkyCoordinates()
        sc.longitude.set(0)
        sc.latitude.set(0)
        sc.system = u'J2000'
        sc.type = u'RELATIVE'
        fp.phaseCenterCoordinates.append(sc)
        self.setFieldPattern(fp)

        
    def setRADec(self, ra, dec):
        self.sourceCoordinates.longitude.set(ra)
        self.sourceCoordinates.latitude.set(dec)

    def getRADec(self):
        return (self.sourceCoordinates.longitude.get(),
                self.sourceCoordinates.latitude.get())
    
    def setProperMotion(self, pmRA, pmDec):
        self.pMRA.set(pmRA)
        self.pMDec.set(pmDec)

    def getProperMotion(self):
        return (self.pMRA.get(), self.pMDec.get())

    def setSystem(self, system):
        self.sourceCoordinates.system = system

    def getSystem(self):
        return self.sourceCoordinates.system

class PlanetSource(APDMSchedBlock.FieldSource):
    def __init__(self, planetName, ephemerisFile = None):
         '''
         This helper function creates a FieldSource structure for
         a given planet.
    
         Available Sources:
         planetName: "Mercury", "Venus", "Moon", "Mars", "Jupiter",
                     "Saturn", "Uranus", "Neptune", "Pluto", "Sun",
                     "Io", "Callisto", "Europa", "Ganymede", "Titan",
                     "Ceres", "Pallas", "Juno", "Vesta"
         '''
         APDMSchedBlock.FieldSource.__init__(self)

         self.__solarSystemObjects = ['Mercury', 'Venus', 'Moon', 'Mars',
                                      'Jupiter', 'Saturn', 'Uranus', 'Neptune',
                                      'Pluto', 'Ephemeris', 'Sun']
         self.__ephemerisObjects = ['Io', 'Callisto', 'Europa', 'Ganymede',
                                    'Titan', 'Ceres', 'Pallas', 'Juno',
                                    'Vesta']

         self.nonSiderealMotion = True
         self.processPlanetOrEphemeris(planetName)
            
    def setPlanet(self, planetName):
        planetName = planetName.capitalize()

        if planetName in self.__solarSystemObjects:
            self.solarSystemObject = planetName
            self.sourceName = planetName
        elif planetName in self.__ephemerisObjects:
            # These are actually treated internally as ephemeris objects
            filename = "HorizonsEphemeris%s.txt" % planetName
            filePath = findFile(filename)[0]
            if filePath == '':
                # This means we didn't find the file
                ex = ControlExceptionsImpl.IllegalParameterErrorExImpl()
                ex.setData("Message",
                           "Unable to find ephemeris file for '%s'" %
                           planetName)
                raise ex
            self.loadSourceEphemeris(filePath)
            self.sourceName=planetName
        else:
            ex = ControlExceptionsImpl.IllegalParameterErrorExImpl()
            ex.setData("Message", 'The object ' + planetName +
                       ' is not recognized as a solar system object')
            raise ex

class EphemerisSource(APDMSchedBlock.FieldSource):
    def __init__(self, sourceName, ephemerisFile):
         '''
         This helper function creates a FieldSource structure for
         a given ephemeris file
         '''
         APDMSchedBlock.FieldSource.__init__(self)
         self.nonSiderealMotion = True
         self.sourceName = sourceName
         self.setEphemeris(ephemerisFile)
            
    def setEphemeris(self, filename):
        filePath = findFile(filename)[0]
        if filePath == '':
            # This means we didn't find the file
            ex = ControlExceptionsImpl.IllegalParameterErrorExImpl()
            ex.setData("Message",
                       "Unable to find ephemeris file '%s'" %
                       filename)
            raise ex
        self.loadSourceEphemeris(filePath)

class HorizonSource(APDMSchedBlock.FieldSource):
    def __init__(self, az, el, sourceName=None):
         '''
         This creates an horizon source, any string entries are parsed
         using the standard CCL libraries. 
         '''
         APDMSchedBlock.FieldSource.__init__(self)
         # This seems counter-intuitive, but usage of this variable equates
         # nonSiderealMotion to mean only a Planet or Ephemeris object
         self.nonSiderealMotion = False
         if sourceName is not None:
             self.sourceName = sourceName
         self.setAzEl(az, el)
         
    def setAzEl(self, az, el):
        self.sourceCoordinates.longitude.set(az)
        self.sourceCoordinates.latitude.set(el)
        self.sourceCoordinates.system = u'azel'

    def getAzEl(self):
        return (self.sourceCoordinates.longitude.get(),
                self.sourceCoordinates.latitude.get())
