#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warrant of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
import Acspy.Util.XmlObjectifier
import CCL.SIConverter
import math


class SquareLawConfig(object):
    """
    This class contains the setup for the square law detectors.
    The SquareLawConfig which is passed into the constructor is a
    XML Dom object
    """
    def __init__(self, squareLawConfig = None):
        """
        Initialize the Correlator config
        """
        if squareLawConfig is None:
            squareLawConfig = self.__getDefaultSquareLawConfig()
        
        self.__xml = squareLawConfig.toxml()

    def getIntegrationDuration(self):
        """
        Return the correlator Dump  Duration in seconds.
        """
        configObj = Acspy.Util.XmlObjectifier.XmlObject(self.__xml).SquareLawSetup
        return CCL.SIConverter.toSeconds(configObj.integrationDuration)

    
    def setIntegrationDuration(self, duration):
        """
        Set the Square Law Detector integration Duration in seconds.

        Note this value is automatically rounded to be a multiple of
        the underlying 0.5ms (2kHz) hardware integration duration.
        """
        duration = CCL.SIConverter.toSeconds(duration)

        # Dump duration must be a multiple of 0.5 ms
        duration = max( 0.0005, round(duration/0.0005)*0.0005)
        configObj = Acspy.Util.XmlObjectifier.XmlObject(self.__xml).SquareLawSetup
        configObj.integrationDuration.setValue(duration)
        configObj.integrationDuration.setAttribute('unit', 's')
        self.__xml = configObj.toxml()

    def _getSquareLawConfig(self):
        confObj = Acspy.Util.XmlObjectifier.XmlObject(self.__xml)
        return confObj.SquareLawSetup

    def __getDefaultSquareLawConfig(self):
        """The default integration duration is 0.1 seconds."""
        defaultConfig="""
        <SquareLawSetup>
            <integrationDuration unit="s">0.10</integrationDuration>
        </SquareLawSetup>
        """
        return Acspy.Util.XmlObjectifier.XmlObject(defaultConfig).SquareLawSetup
