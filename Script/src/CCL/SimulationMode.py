#!/usr/env/python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
#

from CCL.ObservingModeBase import ObservingModeBase
from CCL.utils import aggregate

class SimulationObsMode(ObservingModeBase):
    def __init__(self, verbose = False):
        print "VERBOSE IS: ", verbose
        ObservingModeBase.__init__(self)
        self.elapsedTime = 0
        self.numSubscans = 0
        self.numScans    = 0
        self.scanStarted = False
        self._verbose    = verbose
        self.log         = []
        self._source = None
        self._spectralSpec  = None

    def getElapsedTime(self):
        '''
        This method returns the amount of time (seconds) since the begin
        execution method was called.

        For this observing mode the elapsed time is simulated
        '''
        return self.elapsedTime
    
    def __generateTimestamp(self):
        min = int(self.elapsedTime / 60)
        sec = (self.elapsedTime % 60)
        timestamp = "%02d:%02d.%03d" % (min, int(sec), int((sec%1)*1000))
        return timestamp

    def printReport(self):
        print "              Total Time: " + self.__generateTimestamp()
        print "                   Scans: %d " % self.numScans
        print "                 Subcans: %d " % self.numSubscans
        print "Observing Sequence:"

        for line in self.log:
            print line
        
    def isObservable(self, source, duration=0):
        '''
        All observing modes must implement this
        '''
        return True

    def setTarget(self, source, spectralSpec):
        self._source = source
        self._spectralSpec = spectralSpec

    def setSource(self, source):
        self._source = source

    def setSpectrum(self, spectralSpec):
        self._spectralSpec = spectralSpec

    def setHorizonOffset(self, longOff, latOff):
        pass

    def beginScan(self, intent):
        self.scanStarted = True

        if self._source is None:
            sourceName = "No Source Specified"
        else:
            sourceName = self._source.getSourceName()

        printIntent= ""
        for oneIntent in aggregate(intent):
            printIntent += "%s " % oneIntent 
        self._appendLogLine("Scan %d Started (%s): Source %s" %
                             (self.numScans +1, printIntent,
                              sourceName))
                              
    def endScan(self):
        if not self.scanStarted:
            print "Error: Scan ended without starting"
        self._scanStarted = False
        self.numScans += 1
        if (self._verbose):
            self._appendLogLine("Scan %d Ended" % self.numScans)

    def doSubscan(self, intent):
        self.elapsedTime += self._spectralSpec.getSubscanDuration()
        self.numSubscans += 1
        if (self._verbose):
            self._appendLogLine("\tSubscan Executed %s" % intent)

    def _appendLogLine(self, message):
        self.log.append(self.__generateTimestamp() +"\t" + message)
    

    def setCalibrationDevice(self, type):
        pass
        
    def getAntennas(self):
        return ["DV01", "PM03", "DV02"]
