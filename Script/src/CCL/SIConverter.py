#! /usr/bin/env python
#*****************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#

from casac import homefinder
import ControlExceptionsImpl
import Acspy.Util.XmlObjectifier

import APDMEntities.ValueTypes


__doc__ = '''

    This Module uses the CASA Quanta tool to convert input values
    into cannonical (SI) units.

    All functions in this module return values in SI
    units. Specifically angles are in radians, angular velocities are
    in radians/second, frequencies are in Hertz and time intervals are
    in seconds. Absolute times are in ACS time units which are the
    number of 100 nanosecond intervals since the start of Gregorian
    calendar.

    Parameters for all functions should also use SI units if a numeric
    value is given. Alternatively the user can specify the parameter
    using a string which contains a numeric value and a unit
    e.g., "10 deg". If a string is used the units are checked to ensure
    the dimensionality is correct (although wavelength to frequency
    conversions are automatically done).

    The allowed strings are identical to those used by the casa quanta
    library (as this is the underlying code that does these
    conversions). Some examples of allowed units are:
    * Angles
      degrees e.g., "1 deg",
      radians e.g., "1 rad",
      arc-minutes, e.g., "1 arcmin",
      arcseconds e.g., "1 arcsec",
      milli arc-seconds e.g., "1 mas",
      hours-minutes-seconds e.g., "11:20:30.123"  (uses a ':' delimiter)
      degrees-minutes-seconds e.g., "160.20.30.123" (uses a '.' delimiter)
    * Time intervals
      seconds  e.g., "1 s",
      minutes  e.g., "1 min",
      hours e.g., "1 h"
      days e.g., "1 d"
      milliseconds  e.g., "1 ms",
    * Anglular velocity
      Any combination of an angular unit and a time interval unit
      separated by a '/'
      e.g., "1 deg/min", "20 arcsec/s"
    * Frequency
      Hertz e.g., "1 Hz"
      kiloHertz e.g., "1 kHz"
      MegaHertz e.g., "1 MHz"
      GigaHertz e.g., "1 GHz"
      TeraHertz e.g., "1 THz"
      In addition frequencies can be specified using a wavelength. The
      conversion is automatically done using the free-space frequency of light.
      meters  e.g., "1 m"
      centimeters  e.g., "1 cm"
      millimeters  e.g., "1 mm"
      micrometers  e.g., "1 um"
      nanometers  e.g., "1 nm"
      Angstroms  e.g., "1 Angstrom"
'''

_qatool = homefinder.find_home_by_name('quantaHome').create()

def toSIUnits(value):
    '''
    Convert a string value in to cannoical units.
    '''
    if _qatool.check(value):
        return _qatool.canon(_qatool.quantity(value))
    else:
        ex = ControlExceptionsImpl.IllegalParameterErrorExImpl()
        ex.setData("Message", "The parameter '" + value + \
                   "' does not have recognizable units.");
        raise ex

def toSI(value):
    '''
    Return the value of a string in cannonical units
    '''
    if isinstance(value, APDMEntities.ValueTypes.DoubleWithUnitT):
        value = _apdmValueToString(value)
    if (isinstance(value, Acspy.Util.XmlObjectifier.XmlElement)):
        value = _xmlElementToString(value)
    return toSIUnits(value)["value"]

def toRadians(value):
    '''
    Convert the given angle to Radians
    '''
    if isinstance(value, APDMEntities.ValueTypes.DoubleWithUnitT):
        value = _apdmValueToString(value)
    elif (isinstance(value, Acspy.Util.XmlObjectifier.XmlElement)):
        value = _xmlElementToString(value)
    elif (not isinstance(value, str)):
        return value
    siUnit = toSIUnits(value)
    if (_qatool.compare(siUnit, "rad")):
        return _qatool.getvalue(siUnit)
    else:
        ex = ControlExceptionsImpl.IllegalParameterErrorExImpl()
        ex.setData("Message", "The parameter '" + value + \
                   "' is not an angle.");
        raise ex

def toRadiansPerSecond(value):
    '''
    Convert the given angular velocity to Radians per Second
    '''
    if (isinstance(value, APDMEntities.ValueTypes.DoubleWithUnitT)):
        value = _apdmValueToString(value)
    elif (isinstance(value, Acspy.Util.XmlObjectifier.XmlElement)):
        value = _xmlElementToString(value)
    elif (not isinstance(value, str)):
        return value
    siUnit = toSIUnits(value)
    if (_qatool.compare(siUnit, "rad/s")):
        return _qatool.getvalue(siUnit)
    else:
        ex = ControlExceptionsImpl.IllegalParameterErrorExImpl()
        ex.setData("Message", "The parameter '" + value + \
                   "' is not an angular velocity.");
        raise ex

def toSeconds(value):
    '''
    Convert the given time to Seconds
    '''
    if (isinstance(value, APDMEntities.ValueTypes.DoubleWithUnitT)):
        value = _apdmValueToString(value)
    elif (isinstance(value, Acspy.Util.XmlObjectifier.XmlElement)):
        value = _xmlElementToString(value)
    elif (not isinstance(value, str)):
        return value
    siUnit = toSIUnits(value)
    if (_qatool.compare(siUnit, "s")):
        return _qatool.getvalue(siUnit)
    else:
        ex = ControlExceptionsImpl.IllegalParameterErrorExImpl()
        ex.setData("Message", "The parameter '" + value + \
                   "' is not a time interval.");
        raise ex

def toHertz(value):
    '''
    Convert the given frequency to Hertz.  If a length is specified
    it is treated as a wavelength and converted to Hz.
    '''
    if (isinstance(value, APDMEntities.ValueTypes.DoubleWithUnitT)):
        value = _apdmValueToString(value)
    elif (isinstance(value, Acspy.Util.XmlObjectifier.XmlElement)):
        value = _xmlElementToString(value)
    elif (not isinstance(value, str)):
        return value
    siUnit = toSIUnits(value)
    if (_qatool.checkfreq(siUnit)):
        return  _qatool.getvalue(_qatool.convertfreq(value))
    else:
        ex = ControlExceptionsImpl.IllegalParameterErrorExImpl()
        ex.setData("Message", "The parameter '" + value + \
                   "' is not a frequency or a wavelength.");
        raise ex

def toMeters(value):
    '''
    Convert the given length to meters.
    '''
    if (isinstance(value, APDMEntities.ValueTypes.DoubleWithUnitT)):
        value = _apdmValueToString(value)
    elif (isinstance(value, Acspy.Util.XmlObjectifier.XmlElement)):
        value = _xmlElementToString(value)
    elif (not isinstance(value, str)):
        return value
    siUnit = toSIUnits(value)
    if (_qatool.compare(siUnit, "m")):
        return _qatool.getvalue(siUnit)
    else:
        ex = ControlExceptionsImpl.IllegalParameterErrorExImpl()
        ex.setData("Message", "The parameter '" + value + \
                   "' is not a length.");
        raise ex

def toMetersPerSecond(value):
    '''
    Convert the given angular velocity to Radians per Second
    '''
    if (isinstance(value, APDMEntities.ValueTypes.DoubleWithUnitT)):
        value = _apdmValueToString(value)
    elif (isinstance(value, Acspy.Util.XmlObjectifier.XmlElement)):
        value = _xmlElementToString(value)
    elif (not isinstance(value, str)):
        return value
    siUnit = toSIUnits(value)
    if (_qatool.compare(siUnit, "m/s")):
        return _qatool.getvalue(siUnit)
    else:
        ex = ControlExceptionsImpl.IllegalParameterErrorExImpl()
        ex.setData("Message", "The parameter '" + value + \
                   "' is not a velocity.");
        raise ex

def toJanskys(value):
    if (isinstance(value, APDMEntities.ValueTypes.DoubleWithUnitT)):
        value = str(str(value.value()) + value.unit)
    elif (isinstance(value, Acspy.Util.XmlObjectifier.XmlElement)):
        value = _xmlElementToString(value)
    elif (not isinstance(value, str)):
        return value
    siUnit = toSIUnits(value)
    if (_qatool.compare(siUnit, "Jy")):
        return _qatool.getvalue(_qatool.convert(siUnit,"Jy"))
    else:
        ex = ControlExceptionsImpl.IllegalParameterErrorExImpl()
        ex.setData("Message", "The parameter '" + value + \
                   "' is not a recognized flux.");
        raise ex

def _xmlElementToString(value):
    string = str(value.getValue()) + value.getAttribute("unit")
    return string

def _apdmValueToString(value):
    # To avoid getting a unicode value back the extra str
    if value.unit is not None:
        string = str(str(value.value()) + value.unit)
    else:
        string = str(value.value()) 
    return string


#------- Everything below this line is deprecated

from CCL.logging import getLogger
from log_audience import DEVELOPER
import ACSLog

class SIConverter:
    '''
    This deprecated class simply exposes another interface to the methods
    in this module.
    '''
    def __init__(self):
        self._logger = getLogger()
        msg = "Constructing deprecated class SIConverter."
        self._logger.logNotSoTypeSafe(ACSLog.ACS_LOG_WARNING, msg,
                                      DEVELOPER)

    
    def toSIUnits(self, value):
        '''
        Convert a string value in to cannoical units.
        '''
        msg = "Using deprecated class SIConverter."
        self._logger.logNotSoTypeSafe(ACSLog.ACS_LOG_WARNING, msg,
                                      DEVELOPER)
        return toSIUnits(value)
    
    def toSI(self, value):
        '''
        Return the value of a string in cannonical units
        '''
        msg = "Using deprecated class SIConverter."
        self._logger.logNotSoTypeSafe(ACSLog.ACS_LOG_WARNING, msg,
                                      DEVELOPER)
        return toSI(value)

    
    def toRadians(self, value):
        '''
        Convert the given angle to Radians
        '''
        msg = "Using deprecated class SIConverter."
        self._logger.logNotSoTypeSafe(ACSLog.ACS_LOG_WARNING, msg,
                                      DEVELOPER)
        return toRadians(value)

    
    def toRadiansPerSecond(self, value):
        '''
        Convert the given angular velocity to Radians per Second
        '''
        msg = "Using deprecated class SIConverter."
        self._logger.logNotSoTypeSafe(ACSLog.ACS_LOG_WARNING, msg,
                                      DEVELOPER)
        return toRadiansPerSecond(value)

    
    def toSeconds(self, value):
        '''
        Convert the given time to Seconds
        '''
        msg = "Using deprecated class SIConverter."
        self._logger.logNotSoTypeSafe(ACSLog.ACS_LOG_WARNING, msg,
                                      DEVELOPER)
        return toSeconds(value)

    
    def toHertz(self, value):
        '''
        Convert the given frequency to Hertz.  If a length is specified
        it is treated as a wavelength and converted to Hz.
        '''
        msg = "Using deprecated class SIConverter."
        self._logger.logNotSoTypeSafe(ACSLog.ACS_LOG_WARNING, msg,
                                      DEVELOPER)
        return toHertz(value)

    
    def toMeters(self, value):
        '''
        Convert the given length to meters.
        '''
        msg = "Using deprecated class SIConverter."
        self._logger.logNotSoTypeSafe(ACSLog.ACS_LOG_WARNING, msg,
                                      DEVELOPER)
        return toMeters(value)
   
