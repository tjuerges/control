#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warrant of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
import Acspy.Util.XmlObjectifier
import CCL.BaseBandConfig
import CCL.SIConverter
import math
from ControlExceptions import IllegalParameterErrorEx


class CorrelatorConfig(object):
    """
    This class is the base class for the Correlator Configuration
    it should never be used directly but rather through either
    a BLCorrelatorConfig or ACACorrelatorConfig class.
    """
    def __init__(self, corrConfig):
        """
        Initialize the Correlator config
        """
        self.__xml = corrConfig.toxml()

    def getIntegrationDuration(self):
        """
        Return the integration time in seconds.
        This time is only used in the scripting I believe.
        """
        configObj = self._getCorrelatorConfig()
        return CCL.SIConverter.toSeconds(configObj.integrationDuration)

    def setIntegrationDuration(self, duration):
        """
        Set the integration time in seconds.
        This time is only used in the scripting I believe.
        """
        duration = CCL.SIConverter.toSeconds(duration)
        # We know that this cannot be specified any finer than 1 ms
        # to avoid python precision problems round to the ms here.
        
        
        configObj = self._getCorrelatorConfig()
        configObj.integrationDuration.setValue(duration)
        configObj.integrationDuration.setAttribute('unit', 's')
        self.__xml = configObj.toxml()

    def getChannelAverageDuration(self):
        """
        Return  the Channel Average Duration in seconds.
        """
        configObj = self._getCorrelatorConfig()
        return CCL.SIConverter.toSeconds(configObj.channelAverageDuration)

    def setChannelAverageDuration(self, duration):
        """
        Set  the Channel Average Duration in seconds.
        """
        duration = CCL.SIConverter.toSeconds(duration)
        configObj = self._getCorrelatorConfig()
        configObj.channelAverageDuration.setValue(duration)
        configObj.channelAverageDuration.setAttribute('unit', 's')
        self.__xml = configObj.toxml()

    def roundTimes(self):
        """
        This method rounds up the Channel Average, and Integration
        durations so that they are a multiple of the dump duration.
        """
        dump = self.getDumpDuration()
        chav = self.getChannelAverageDuration()
        chav = max(dump, round(chav/dump)*dump)
        self.setChannelAverageDuration(chav)
        
        int = self.getIntegrationDuration()
        int = max(chav, round(int/chav) * chav)
        self.setIntegrationDuration(int)
        
    def setBaseBandConfig(self, BaseBandConfig, BaseBandName = 0):
        """
        Method to add a new Base Band Configuration to the Correlator
        Configuration.

        BaseBandConfig should be a CCL.BaseBandConfiguration object

        Currebtly Ignoring the BaseBandName
        """
        self.removeBaseBandConfig(BaseBandName)
        bbObj = BaseBandConfig._getBaseBandConfig()
        copy = bbObj.cloneNode(1)
        corrObj = self._getCorrelatorConfig()
        corrObj.appendChild(copy)
        self.__xml = corrObj.toxml()

    def getBaseBandConfig(self, BaseBandName = 0):
        """
        This method is very simple right now and returns the only
        base band configuration in the structure.  Multiple base bands
        are not yet supported
        """
        corrObj = self._getCorrelatorConfig()
        for node in corrObj.childNodes:
            if node.localName == 'BLBaseBandConfig':
                return CCL.BaseBandConfig.BLBaseBandConfig(node)
            if node.localName == 'ACABaseBandConfig':
                return CCL.BaseBandConfig.ACABaseBandConfig(node)

    def removeBaseBandConfig(self, BaseBandName = 0):
        """
        Currently ignoring the BaseBandName and working with the only
        base band we have.
        """
        corrObj = self._getCorrelatorConfig()
        for node in corrObj.childNodes:
            if node.localName == 'BLBaseBandConfig':
                corrObj.removeChild(node)
            if node.localName == 'ACABaseBandConfig':
                corrObj.removeChild(node)
        self.__xml = corrObj.toxml()
        
class BLCorrelatorConfig(CorrelatorConfig):
    
    def __init__(self, corrConfig= None):
        """
        Initialize the BLCorrelator Config
        """
        if corrConfig is None:
            corrConfig = self.__getDefaultBLCorrelatorConfig()
        
        CorrelatorConfig.__init__(self, corrConfig)

    def getDumpDuration(self):
        """
        Return the correlator Dump  Duration in seconds.
        """
        configObj =  self._getCorrelatorConfig()
        return CCL.SIConverter.toSeconds(configObj.dumpDuration)

    def setDumpDuration(self, duration):
        """
        Set the correlator Dump  Duration in seconds.

        Note this value is automatically rounded to to the nearest 16 ms
        boundary.
        """
        duration = CCL.SIConverter.toSeconds(duration)

        # Dump duration must be a multiple of 16 ms
        duration = max( 0.016, round(duration/0.016)*0.016)
        configObj =  self._getCorrelatorConfig()
        configObj.dumpDuration.setValue(duration)
        configObj.dumpDuration.setAttribute('unit', 's')
        self._CorrelatorConfig__xml = configObj.toxml()

    def __getDefaultBLCorrelatorConfig(self):
        defaultConfig="""\
        <BLCorrelatorConfiguration aPCDataSets="AP_UNCORRECTED">
            <integrationDuration unit="s">10.080</integrationDuration>
            <channelAverageDuration unit="s">10.08</channelAverageDuration>
            <dumpDuration unit="s">10.08</dumpDuration>
        </BLCorrelatorConfiguration>
        """
        return Acspy.Util.XmlObjectifier.XmlObject(defaultConfig).BLCorrelatorConfiguration

    def _getCorrelatorConfig(self):
        """
        This is necessary to allow us to handle the ACA and BL configuration
        in parallel
        """
        confObj = Acspy.Util.XmlObjectifier.XmlObject(self._CorrelatorConfig__xml)
        return confObj.BLCorrelatorConfiguration

    # Factory methods that simplify generating Correlator Configurations
    def __createDefaultTDMCorrelatorConfig():
        cc = BLCorrelatorConfig()
        bbc = CCL.BaseBandConfig.BLBaseBandConfig.createDefaultTDM()
        cc.setBaseBandConfig(bbc)
        return cc
    createDefaultTDM = staticmethod(__createDefaultTDMCorrelatorConfig)

    def __createDefaultFDMCorrelatorConfig():
        cc = BLCorrelatorConfig()
        bbc = CCL.BaseBandConfig.BLBaseBandConfig.createDefaultFDM()
        cc.setBaseBandConfig(bbc)
        return cc
    createDefaultFDM = staticmethod(__createDefaultFDMCorrelatorConfig)

class ACACorrelatorConfig(CorrelatorConfig):
    
    def __init__(self, corrConfig= None):
        """
        Initialize the ACS Correlator Config
        """
        if corrConfig is None:
            corrConfig = self.__getDefaultACACorrelatorConfig()
        
        CorrelatorConfig.__init__(self, corrConfig)

    def __getDefaultACACorrelatorConfig(self):
        defaultConfig="""\
        <ACACorrelatorConfiguration aPCDataSets="AP_UNCORRECTED">
            <integrationDuration unit="s">1.008</integrationDuration>
            <channelAverageDuration unit="s">.336</channelAverageDuration>
            <ACAPhaseSwitchingConfiguration>
               <doD180modulation>true</doD180modulation>
               <doD180demodulation>true</doD180demodulation>
               <d180Duration unit="ms">25.0</d180Duration>
             </ACAPhaseSwitchingConfiguration>
        </ACACorrelatorConfiguration>"""
        return Acspy.Util.XmlObjectifier.XmlObject(defaultConfig).ACACorrelatorConfiguration
    
    # Factory methods that simplify generating Correlator Configurations
    def __createDefaultCorrelatorConfig():
        cc = ACACorrelatorConfig()
        bbc = CCL.BaseBandConfig.ACABaseBandConfig.createDefault()
        cc.setBaseBandConfig(bbc)
        return cc
    createDefault = staticmethod(__createDefaultCorrelatorConfig)
    
    def _getCorrelatorConfig(self):
        """
        This is necessary to allow us to handle the ACA and BL configuration
        in parallel
        """
        confObj = Acspy.Util.XmlObjectifier.XmlObject(self._CorrelatorConfig__xml)
        return confObj.ACACorrelatorConfiguration
       

    def getDumpDuration(self):
        """
        The ACA Correlator always dumps at 16 ms.  For convience just return
        this.
        """
        return 0.016

