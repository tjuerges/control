#! /usr/bin/env python
# "@(#) $Id$"
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2010 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

import APDMEntities.ValueTypes
import CCL.SIConverter as SIConverter
import math
import pyxb

__doc__ = '''

This module provides wrappers for the Value types defined in the
generated ValueTypes module of the APDM.

All of the Classes in the APDMEntities::ValueTypes are listed here but
only the ones I find I need are Implemented.
'''


class Angle(APDMEntities.ValueTypes.AngleT):
    def __init__(self, *args, **kw):
        super(APDMEntities.ValueTypes.AngleT, self).__init__(*args, **kw)
        if self.unit is None:
            self.unit = "rad"

    def set(self, value):
        self.reset()
        self.append(SIConverter.toRadians(value))
        self.unit = "rad"

    def get(self):
        return SIConverter.toRadians(self)

# Override the class in the APDMEntities
APDMEntities.ValueTypes.AngleT._SetSupersedingClass(Angle)


class Frequency(APDMEntities.ValueTypes.FrequencyT):
    def __init__(self, *args, **kw):
        super(APDMEntities.ValueTypes.FrequencyT, self).__init__(*args, **kw)
        if self.unit is None:
            self.unit = "Hz"

    def set(self, value):
        self.reset()
        self.append(SIConverter.toHertz(value))
        self.unit = "Hz"

    def get(self):
        return SIConverter.toHertz(self)

# Override the class in the APDMEntities
APDMEntities.ValueTypes.FrequencyT._SetSupersedingClass(Frequency)

class Time(APDMEntities.ValueTypes.TimeT):
    def __init__(self, *args, **kw):
        super(APDMEntities.ValueTypes.TimeT, self).__init__(*args, **kw)
        if self.unit is None:
            self.unit = "s"

    def set(self, value):
        self.reset()
        self.append(SIConverter.toSeconds(value))
        self.unit = "s"

    def get(self):
        return SIConverter.toSeconds(self)

# Override the class in the APDMEntities
APDMEntities.ValueTypes.TimeT._SetSupersedingClass(Time)

class Flux(APDMEntities.ValueTypes.FluxT):
    def __init__(self, *args, **kw):
        super(APDMEntities.ValueTypes.FluxT, self).__init__(*args, **kw)
        if self.unit is None:
            self.unit = "Jy"

    def set(self, value):
        self.reset()
        self.append(SIConverter.toJanskys(value))
        self.unit = "Jy"

    def get(self):
        return SIConverter.toJanskys(self)

# Override the class in the APDMEntities
APDMEntities.ValueTypes.FluxT._SetSupersedingClass(Flux)


#MagnitudeT: Not Implemented
#UserAngleT: Not Implemented

class Velocity(APDMEntities.ValueTypes.VelocityT):
    def __init__(self, *args, **kw):
        super(APDMEntities.ValueTypes.VelocityT, self).__init__(*args, **kw)
        # This is a complex type so we need to set default values
        if self.centerVelocity is None:
            self.centerVelocity = Speed(0)

     
# Override the class in the APDMEntities
APDMEntities.ValueTypes.VelocityT._SetSupersedingClass(Velocity)

## #KeywordValueT: Not Implemented
## #IntTimeReferenceT: Not Implemented
## #StorageVolumeT: Not Implemented
class Sensitivity(APDMEntities.ValueTypes.SensitivityT):
    def __init__(self, *args, **kw):
        super(APDMEntities.ValueTypes.SensitivityT, self).__init__(*args, **kw)
        if self.unit is None:
            self.unit = "Jy"

    def set(self, value):
        self.reset()
        self.append(SIConverter.toJanskys(value))
        self.unit = "Jy"

    def get(self):
        return SIConverter.toJanskys(self)

# Override the class in the APDMEntities
APDMEntities.ValueTypes.SensitivityT._SetSupersedingClass(Sensitivity)

class SkyCoordinates(APDMEntities.ValueTypes.SkyCoordinatesT):
    def __init__(self, *args, **kw):
        super(APDMEntities.ValueTypes.SkyCoordinatesT, self)\
                                                       .__init__(*args, **kw)
        # This is a complex type so we need to set default values
        if self.longitude is None:
            self.longitude = Longitude(0)
        if self.latitude is None:
            self.latitude = Latitude(0)
        if self.type is None:
            self.type = "ABSOLUTE"
        if self.system is None:
            self.system = u'J2000'
        if self.fieldName is None:
            self.fieldName = ""
# Override the class in the APDMEntities
APDMEntities.ValueTypes.SkyCoordinatesT._SetSupersedingClass(SkyCoordinates)


## #ExecBlockRefT: Not Implemented
class Speed(APDMEntities.ValueTypes.SpeedT):
    def __init__(self, *args, **kw):
        super(APDMEntities.ValueTypes.SpeedT, self).__init__(*args, **kw)
        if self.unit is None:
            self.unit = "m/s"

    def set(self, value):
        self.reset()
        self.append(SIConverter.toMetersPerSecond(value))
        self.unit = "m/s"

    def get(self):
        return SIConverter.toMetersPerSecond(self)

# Override the class in the APDMEntities
APDMEntities.ValueTypes.SpeedT._SetSupersedingClass(Speed)

## #SmallAngleT: Not Implemented

class AngularVelocity(APDMEntities.ValueTypes.AngularVelocityT):
    # Because rad/s is not a valid unit for the model we need to
    # store the values in deg/s.  But the get method still returns
    # rad/s
    def __init__(self, *args, **kw):
        super(APDMEntities.ValueTypes.AngularVelocityT, self)\
                                                        .__init__(*args, **kw)
        if self.unit is None:
            self.unit = "deg/s"

    def set(self, value):
        self.reset()
        self.append(math.degrees(SIConverter.toRadiansPerSecond(value)))
        self.unit = "deg/s"

    def get(self):
        return SIConverter.toRadiansPerSecond(self)

# Override the class in the APDMEntities
APDMEntities.ValueTypes.AngularVelocityT._SetSupersedingClass(AngularVelocity)

class Length(APDMEntities.ValueTypes.LengthT):
    def __init__(self, *args, **kw):
        super(APDMEntities.ValueTypes.LengthT, self).__init__(*args, **kw)
        if self.unit is None:
            self.unit = "m"

    def set(self, value):
        self.reset()
        self.append(SIConverter.toMeters(value))
        self.unit = "m"

    def get(self):
        return SIConverter.toMeters(self)

# Override the class in the APDMEntities
APDMEntities.ValueTypes.LengthT._SetSupersedingClass(Length)


## LatitudeT
class Latitude(APDMEntities.ValueTypes.LatitudeT):
    def __init__(self, *args, **kw):
        super(APDMEntities.ValueTypes.LatitudeT, self).__init__(*args, **kw)
        if self.unit is None:
            self.unit = "rad"

    def set(self, value):
        self.reset()
        self.append(SIConverter.toRadians(value))
        self.unit = "rad"

    def get(self):
        return SIConverter.toRadians(self)

# Override the class in the APDMEntities
APDMEntities.ValueTypes.LatitudeT._SetSupersedingClass(Latitude)

## LongitudeT
class Longitude(APDMEntities.ValueTypes.LongitudeT):
    def __init__(self, *args, **kw):
        super(APDMEntities.ValueTypes.LongitudeT, self).__init__(*args, **kw)
        if self.unit is None:
            self.unit = "rad"

    def set(self, value):
        self.reset()
        self.append(SIConverter.toRadians(value))
        self.unit = "rad"

    def get(self):
        return SIConverter.toRadians(self)

# Override the class in the APDMEntities
APDMEntities.ValueTypes.LongitudeT._SetSupersedingClass(Longitude)

#UserFrequencyT: Not Implemented
#DataRateT: Not Implemented
#TimeIntervalT: Not Implemented
#UserSensitivityT: Not Implemented
class IntTimeSource(APDMEntities.ValueTypes.IntTimeSourceT):
    def __init__(self, *args, **kw):
        super(APDMEntities.ValueTypes.IntTimeSourceT, self).\
                                                      __init__(*args, **kw)
        if self.unit is None:
            self.unit = "s"

    def set(self, value):
        self.reset()
        self.append(SIConverter.toSeconds(value))
        self.unit = "s"

    def get(self):
        return SIConverter.toSeconds(self)

APDMEntities.ValueTypes.IntTimeSourceT._SetSupersedingClass(IntTimeSource)

#StatusT: Not Implemented
