#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2010
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import Control
import CCL.SIConverter

from ControlExceptionsImpl import IllegalParameterErrorExImpl
import CCL.SIConverter
from CCL.utils import aggregate
from PyDataModelEnumeration import PySubscanIntent
from PyDataModelEnumeration import PyCalibrationDevice
from CCL.EnumerationHelper import getEnumeration

class SubscanList:
    """
    A Subscan List contains all of the subscans required to do
    an observation of the source.  Iteration on this and interleaving of
    references etc. are done at a higher level.
    """
    def __init__(self):
        self.clear()

    def clear(self):
        '''
        This method removed all subscans from the list
        '''
        self._sourceList = []
        self._spectralList = []
        
        self._subscanList = []
        self.__elapsedTime = 0.0
        
    def addSubscan(self, Source, SpectralSpec, SubscanDuration,
                   PointingOffsetSpec = None, DelayOffsetSpec = None,
                   SubreflectorOffset = [],
                   CalibrationDevice = PyCalibrationDevice.NONE,
                   SubscanIntent = PySubscanIntent.ON_SOURCE):
        
        '''
        This returns an estimated elapsed time for the entire list.
        The Source, SpectralSpec and SubscanDuration Fields are required,
        All other entries have the following interpretation:
        Pointing Offset is either a single Control.SourceOffset in which
          case it is assumed to apply to the default array or a list
          of (SourceOffset, ArrayName) tuples.
        DelayOffsetSpec is a single Control.SourceOffset
        '''
        # Handle translation of the SubscanDurations
        # Check Type of Source and Spectral Spec        
        SubscanIntent = getEnumeration(SubscanIntent, PySubscanIntent)

        SubscanDuration = \
                        CCL.SIConverter.toSeconds(SubscanDuration)

        
        pointingOffset = self.processSourceOffset(PointingOffsetSpec)
        if DelayOffsetSpec is None:
            DelayOffsetSpec = Control.SourceOffset(0,0,0,0,Control.EQUATORIAL)
        if not isinstance(DelayOffsetSpec, Control.SourceOffset):
            msg = "Delay Offset must be a Control.SourceOffset object"
            raise IllegalParameterErrorExImpl(msg)

        if not isinstance(SubreflectorOffset, list):
            ex = IllegalParameterErrorExImpl()
            ex.addData("Message", "The Subreflector Offset must be a list")
            raise ex
        if len(SubreflectorOffset) != 0 and len(SubreflectorOffset) != 3:
            ex = IllegalParameterErrorExImpl()
            ex.addData("Message",
                       "The Subreflector Offset must be of length 0 or 3" +
                       " the supplied value has %d." % len(SubreflectorOffset))
            raise ex

        if Source not in self._sourceList:
            self._sourceList.append(Source)
        sourceId = self._sourceList.index(Source)

        if SpectralSpec not in self._spectralList:
            self._spectralList.append(SpectralSpec)
        spectraId = self._spectralList.index(SpectralSpec)

        subreflectorOffset = [];
        self._subscanList.append(Control.SubscanSpec(aggregate(SubscanIntent),
                                                     sourceId,
                                                     spectraId,
                                                     SubscanDuration,
                                                     pointingOffset,
                                                     DelayOffsetSpec,
                                                     subreflectorOffset,
                                                     CalibrationDevice,
                                                     -1))
        
        self.__updateElapsedTime()
        return self.__elapsedTime

    def getElapsedTime(self):
        '''
        Return the current amount of wall time that this subscan list is
        expected to take.  This is a (badly) estimated exectuion time.
        '''
        return self.__elapsedTime

    def getTimeOnSource(self):
        '''
        Return the amount of time actually spent integrating on source
        '''
        timeOnSource = 0
        for subscan in self._subscanList:
            if PySubscanIntent.ON_SOURCE in subscan.intent:
                timeOnSource += subscan.duration
        return timeOnSource

    def getSubscanSequenceSpecification(self):
        '''
        This method takes the current content of the subscan List and
        converts it to a subscanSequenceSpecification which can be
        sent to the Observing Mode.
        '''
        return Control.SubscanSequenceSpec(self._sourceList,
                                           self._spectralList,
                                           self._subscanList)
       
    def __updateElapsedTime(self):
        self.__elapsedTime = 0
        if len(self._subscanList) == 0:
            return 0

        self.__elapsedTime = 1.5 # Assume 15 seconds to get on source
        self.__elapsedTime += self._subscanList[0].duration
        
        for idx in range(1,len(self._subscanList)):
            # This really needs to be better
            overhead = 0
            if self._subscanList[idx].sourceId != \
               self._subscanList[idx-1].sourceId:
                overhead = max(overhead,1.5) # Time to change source
            
            if self._subscanList[idx].spectralId != \
               self._subscanList[idx-1].spectralId:
                overhead = max(overhead,1.5) # Time to change source

            self.__elapsedTime += overhead
            self.__elapsedTime += self._subscanList[idx].duration

    def processSourceOffset(self, offset):
        '''
        This method ensures that the specified pointing offset is valid.
        It follows the following pattern:
        If offset is None, an empty list is returned
        If offset is an Control.SourceOffset a copy is returned with
          each value having been converted to radians or radians per second
        If offset is a list or tuple of length 5 a new
          Control.SourceOffset is returned with the values used in longitude offset,
          latitude offset, longitude velocity, latitude velocity order. 
        If offset is a list or tuple of length 4 a new
          Control.SourceOffset is returned with the values used in longitude offset,
          latitude offset, longitude velocity, latitude velocity order.
          The frame is set to Control.Horizon
        If offset is a list or tuple of length 2 a new
          Control.SourceOffset is returned with the values assumed to be longitude
          & latitude offsets. The velocities are set to zero and the frame is set to Horizon
        Lists of all the above types are also allowed (for pointing subarrays).
        In all other cases an IllegalParameterError exception should be thrown
        '''
        if offset is None:
            offset = Control.SourceOffset(0,0,0,0, Control.HORIZON)

        if isinstance(offset, Control.SourceOffset):
            # This is just a single offset put it in a pair with "default"
            offset = (offset, "Default")

        if not isinstance(offset, list) and not isinstance(offset, tuple):
            # Some sort of illegal parameter
            msg = "Illegal parameter in adding pointing offset to " +\
                  "subscan list."
            raise IllegalParameterErrorExImpl(msg)
            
        if isinstance(offset[0], Control.SourceOffset):
            # This is a single offset / array pair wrap it
            offset = [offset]
                
        pointingOffset = []
        for (offObj, array)  in offset:
            if not isinstance(offObj, Control.SourceOffset):
                msg = "The first member of each pair must be " +\
                      "a Control.Source object"
                raise IllegalParameterErrorExImpl(msg)
            pointingOffset.append(Control.ArrayOffset(offObj, array))
            
        return pointingOffset
