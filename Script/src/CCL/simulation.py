#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#

from ScriptExec.ResourceManager import ResourceManager

def addMock(mockName, mockObject):
    """Adds a mock object into the system.

    A mock object is a simple Python object that will replace an ACS
    component when running observation scripts in development environments
    that doesn't require having ACS running.

    This function provides a way to introduce a mock object into the
    system.

    Parameters:
    mockName    The name of the mock object. This should be the ACS component
                name that this mock object is replacing.
    mockObject  The mock object. This is a plain Python object that implements
                the same interface as the real ACS component.
    """
    ResourceManager().addResource(mockName, mockObject)

