#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import time
import math

from CCL.APDMValueTypes import Frequency
from CCL.APDMValueTypes import Flux
from APDMEntities.SchedBlock import SourcePropertyT
from CCL.SIConverter import toRadians
from CCL.SIConverter import toHertz
from CCL.SIConverter import toJanskys
from CCL.SIConverter import toSeconds
from CCL.Container import getDefaultComponent
from sourcecat import SourceQuery #@UnresolvedImport
from sourcecat import ConeConstraints #@UnresolvedImport
from sourcecat import FrequencyConstraints #@UnresolvedImport
from sourcecat import FluxConstraints #@UnresolvedImport
from sourcecat import DateIntervallConstraints #@UnresolvedImport
from sourcecat import Date #@UnresolvedImport
from CCL.APDMSchedBlock import FieldSource

class CalibratorCatalogue:
    """This is a wrapper over ARCHIVE SourceCatalogueSearch component, which in turn
    wraps the ARCHIVE Source Catalog service, a Plone Web service deployed in
    asa.alma.cl.
    """

    def __init__(self):
        """Constructs a wrapper for ARCHIVE Source Catalog component. The component
        is activated as the default component for the interface
        'IDL:alma/sourcecat/SourceCatalogueSearch:1.0'.
        """
        self.NO_DATE = Date(0, 0, 0, 0, 0, 0)
        self.NO_SOURCE_NAME = ''
        self.SORT_CRITERIA = []
        self.ARCH_SOURCE_CATALOG_INTERFACE = 'IDL:alma/sourcecat/SourceCatalogueSearch:1.0'
        self.__archCalCat = getDefaultComponent(self.ARCH_SOURCE_CATALOG_INTERFACE)

    def listCatalogues(self):
        """Lists all the catalogs available in the service.
        """
        return self.__archCalCat.listCatalogues()
    
    def query(self, querySource, catalogs = []):
        """Queries the Source Catalog service, returning a list of FieldSource objects.
        The query is specified as a QuerySource object. As an option, it is possible to
        specify the catalogs that should be searched, passing a list containing their
        identifiers.
        
        For example:
           from CCL.CalibratorCatalogue import CalibratorCatalogue
           from CCL.APDMSchedBlock import QuerySource
           cat = CalibratorCatalogue()
           qs = QuerySource()
           qs.minFrequency.set('1 MHz')
           qs.maxFrequency.set('90 MHz')
           cals = self.cat.query(qs, [2])
        """
        sources = self.__submit(querySource, catalogs)
        fieldSources = []
        for source in sources:
            fs = self.__toFieldSource(source)
            if fs is not None:
                fieldSources.append(fs)
        return fieldSources
    
    def __submit(self, querySource, catalogs = []):
        """Submits a query to the Source Catalog service, returning the IDL output
        from the SourceCatalogueSearch component directly, without translating it to
        a list of FieldSource entities, as query() does. The query is specified as a
        QuerySource object. As an option, it is possible to specify the catalogs that
        should be searched, passing a list containing their identifiers.
        """
        query = SourceQuery(catalogs,
                            self.__getConeConstraints(querySource), 
                            self.__getFrequencyConstraints(querySource),
                            self.__getFluxConstraints(querySource), 
                            self.__getDateIntervalConstraints(querySource), 
                            self.NO_DATE,
                            self.NO_SOURCE_NAME,
                            self.SORT_CRITERIA,
                            self.__getMaxSouces(querySource))
        return self.__archCalCat.submit(query)
    
    def __getConeConstraints(self, querySource):
        """Creates an IDL sourcecat.ConeConstraints object from a QuerySource pyxb entity. 
        """
        ra = toRadians(querySource.queryCenter.longitude.value()) * 180.0 / math.pi
        dec = toRadians(querySource.queryCenter.latitude.value()) * 180.0 / math.pi
        raDelta = toRadians(querySource.searchRadius.value()) * 180.0 / math.pi
        decDelta = toRadians(querySource.searchRadius.value()) * 180.0 / math.pi
        return ConeConstraints(ra, dec, raDelta, decDelta)
        
    def __getFrequencyConstraints(self, querySource):
        """Creates an IDL sourcecat.FrequencyConstraints object from a QuerySource pyxb entity. 
        """
        minFreq = toHertz(querySource.minFrequency.value())
        maxFreq = toHertz(querySource.maxFrequency.value())
        return FrequencyConstraints(minFreq, maxFreq)
        
    def __getFluxConstraints(self, querySource):
        """Creates an IDL sourcecat.FluxConstraints object from a QuerySource pyxb entity. 
        """
        minFlux = toJanskys(querySource.minFlux.value())
        maxFlux = toJanskys(querySource.maxFlux.value())
        return FluxConstraints(minFlux, maxFlux)
    
    def __getDateIntervalConstraints(self, querySource):
        """Creates an IDL sourcecat.DateIntervalConstraints object from a QuerySource pyxb entity.
        The QuerySource entity specifies a min/max time since observation, while
        the DateIntervalConstraints defines the start/end absolute times of observation.
        This function handles this interface mismatch. 
        """
        maxTimeSinceObserved = toSeconds(querySource.maxTimeSinceObserved.value())
        minTimeSinceObserved = toSeconds(querySource.minTimeSinceObserved.value())
        if (maxTimeSinceObserved == 0) or (minTimeSinceObserved == 0):
            return DateIntervallConstraints(self.NO_DATE, self.NO_DATE)
        now = time.time()
        obsStart = now - maxTimeSinceObserved # in seconds from the Epoch
        start = time.gmtime(obsStart) # as a tuple in UTC
        startDate = Date(start.tm_year, start.tm_month, start.tm_mday,
                         start.tm_hour, start.tm_min, start.tm_sec)
        obsEnd = now - minTimeSinceObserved # in seconds from the Epoch
        end = time.gmtime(obsEnd) # as a tuple in UTC
        endDate = Date(end.tm_year, end.tm_month, end.tm_mday,
                       end.tm_hour, end.tm_min, end.tm_sec)
        return DateIntervallConstraints(startDate, endDate)
    
    def __getMaxSouces(self, querySource):
        """Get the maximum number of sources to be retrieved from the QuerySource pyxb entity.
        """
        return querySource.maxSources

    def __toFieldSource(self, source):
        """Converts an IDL sourcecat.Source structure to a FieldSource pyxb entity.
        """
        fs = FieldSource()
        if len(source.names) == 0:
            return None
        # Use the first name
        name = source.names[0].name
        fs.sourceName = name
        fs.name = name
        if len(source.positions) == 0:
            return None
        # Use the first position
        pos = source.positions[0]
        fs.sourceCoordinates.longitude.set(pos.ra)
        fs.sourceCoordinates.longitude.unit = 'deg'
        fs.sourceCoordinates.latitude.set(pos.dec)
        fs.sourceCoordinates.latitude.unit = 'deg'
        sourceProperties = []
        for b in pos.brightnesses:
            sp = SourcePropertyT()
            sp.sourceFrequency = Frequency(b.frequencyMax)
            # Stoke parameter I is a measure of the total power in the wave
            sp.sourceFluxI = Flux(b.flux)
            sourceProperties.append(sp)
        fs.SourceProperty = sourceProperties
        return fs
        
    