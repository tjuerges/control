#! /usr/bin/env python
# @(#) $Id$
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2010
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warrant of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

import APDMSchedBlock


class AbstractCorrelatorConfiguration:
    """
    This class is the base class for the Correlator Configuration
    it should never be used directly but rather through either
    a BLCorrelatorConfig or ACACorrelatorConfig class.
    """
    def __init__(self,
                 IntegrationDuration = None,
                 ChannelAverageDuration = None,
                 APCDataSets = None):
        """
        Initialize the Correlator config
        """
        if IntegrationDuration is not None:
            self.integrationDuration.set(IntegrationDuration)
        if ChannelAverageDuration is not None:
            self.channelAverageDuration.set(ChannelAverageDuration)
        if APCDataSets is not None:
            self.aPCDataSets = APCDataSets
        self.enable180DegreeWalshFunction = False
        self.enable90DegreeWalshFunction = False
        self.lOOffsettingMode = "NONE"
        self.cAM = 'NORMAL'    

class BLCorrelatorConfiguration(APDMSchedBlock.BLCorrelatorConfiguration,
                                AbstractCorrelatorConfiguration):
    """
    This class is the base class for the Correlator Configuration
    it should never be used directly but rather through either
    a BLCorrelatorConfig or ACACorrelatorConfig class.
    """
    def __init__(self,
                 IntegrationDuration = None,
                 ChannelAverageDuration = None,
                 APCDataSets = None,
                 DumpDuration = None):
        """
        Initialize the Correlator config
        """
        APDMSchedBlock.BLCorrelatorConfiguration.__init__(self)
        AbstractCorrelatorConfiguration.__init__(self,
                                                 IntegrationDuration,
                                                 ChannelAverageDuration,
                                                 APCDataSets)
        if DumpDuration is not None:
            self.dumpDuration.set(DumpDuration)
        self.roundTimes()

class ACACorrelatorConfiguration(APDMSchedBlock.ACACorrelatorConfiguration,
                                 AbstractCorrelatorConfiguration):
    """
    This class is the base class for the Correlator Configuration
    it should never be used directly but rather through either
    a ACACorrelatorConfig or ACACorrelatorConfig class.
    """
    def __init__(self,
                 IntegrationDuration = None,
                 ChannelAverageDuration = None,
                 APCDataSets = None):
        """
        Initialize the Correlator config
        """
        APDMSchedBlock.ACACorrelatorConfiguration.__init__(self)
        AbstractCorrelatorConfiguration.__init__(self,
                                                 IntegrationDuration,
                                                 ChannelAverageDuration,
                                                 APCDataSets)
        self.roundTimes()
