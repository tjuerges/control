#! /usr/bin/env python
# "@(#) $Id$"
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2010, 2011
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA


import pyxb
import math
import hashlib

import APDMValueTypes
import APDMEntities.SchedBlock

import Control #For Control.SourceOffset (and enums)
import ControlExceptionsImpl

pyxb.RequireValidWhenGenerating(False)
pyxb.RequireValidWhenParsing(False)

from AcsutilPy.FindFile import findFile

__partIdCounter = 0

def generateNextPartId():
    '''
    This method generates a Local Part ID to
    allow references to be created.
    '''
    # This goes away once I make this a module?
    global __partIdCounter 
    __partIdCounter += 1
    return "LocalPartId%04d" % __partIdCounter

def findImplementingBindingClass(almatype):
    '''
    Function to find the implementing anonymous class for a given alma type
    Note: Only classes of the form "CTD_ANON*" are searched.
    '''
    import APDMEntities._nsgroup
    for className in APDMEntities._nsgroup.__dict__:
        if className[:8] == 'CTD_ANON':
            anon = APDMEntities._nsgroup.__dict__[className]()
            if anon.almatype == almatype:
                return APDMEntities._nsgroup.__dict__[className]
    msg = "Unable to find Anonymous type for alma type: %s"
    raise ControlExceptionsImpl.IllegalParameterErrorExImpl(msg % almatype)

def getCheckSum(xmlString):
    m = hashlib.md5()
    m.update(xmlString)
    return m.digest()

class XMLFragment:
    def __init__(self, TypeName):
        self.__typeName = unicode(TypeName)

    def getXMLString(self):
        xmlString = self.toxml()
        xmlString = xmlString.replace(u'<?xml version="1.0" ?>\n','')
        xmlString = xmlString.replace(unicode(self.__typeName + 'T'),
                                      self.__typeName)
        # Castor seems to have trouble wit namespaces so remove them
        # from the fragment as well.
        namespaces = []
        for clause in xmlString.split():
            if clause.startswith("xmlns:"):
                namespaces.append(clause[clause.find(':')+1:clause.find('=')])
        for namespace in namespaces:
            xmlString=xmlString.replace(namespace+':','')
            
        return str(xmlString)

    def getCheckSum(self):
        return getCheckSum(self.getXMLString())


class QuerySource(APDMEntities.SchedBlock.QuerySourceT,
                  XMLFragment):

    def __init__(self, *args, **kw):
        super(APDMEntities.SchedBlock.QuerySourceT, self).__init__(*args, **kw)
        XMLFragment.__init__(self, 'QuerySource')
        
        # Check to make sure everything gets initialized
        if self.queryCenter is None:
            self.queryCenter = APDMValueTypes.SkyCoordinates()
        if self.searchRadius is None:
            self.searchRadius = APDMValueTypes.Angle(0)
        if self.minFrequency is None:
            self.minFrequency = APDMValueTypes.Frequency(0.0)
        if self.maxFrequency is None:
            self.maxFrequency = APDMValueTypes.Frequency(0.0)
        if self.minFlux is None:
            self.minFlux = APDMValueTypes.Flux(0.0)
        if self.maxFlux is None:
            self.maxFlux = APDMValueTypes.Flux(0.0)
        if self.minTimeSinceObserved is None:
            self.minTimeSinceObserved = APDMValueTypes.Time(0)
        if self.maxTimeSinceObserved is None:
            self.maxTimeSinceObserved = APDMValueTypes.Time(0)
        if self.use is None:
            self.use = 'true'
        if self.maxSources is None:
            self.maxSources = 0
        if self.intendedUse is None:
            self.intendedUse = 'Amplitude'        

    @staticmethod
    def CreateFromDocument(xmlString):
        # We can only create this from the SchedBlock level since we
        # need an entity.
        if xmlString[:22] == '<?xml version="1.0" ?>':
            xmlString = xmlString[22:]
   
        wrappedString = '''<?xml version="1.0" ?>
        <ns1:SchedBlock xmlns:ns1="Alma/ObsPrep/SchedBlock">
        <ns1:FieldSource> %s </ns1:FieldSource>
        </ns1:SchedBlock>''' % xmlString
        sb = APDMEntities.SchedBlock.CreateFromDocument(wrappedString)
        return sb.FieldSource[0].QuerySource

APDMEntities.SchedBlock.QuerySourceT._SetSupersedingClass(QuerySource)

## SchedBlock
## #?SchedBlockEntityT

## TargetT 
## OrderedTargetT 
## #ObservingGroupT 

class FieldSource(APDMEntities.SchedBlock.FieldSourceT,
                  XMLFragment):
    def __init__(self, *args, **kw):
        super(APDMEntities.SchedBlock.FieldSourceT, self).\
                                                        __init__(*args, **kw)
        XMLFragment.__init__(self,'FieldSource')
        if self.sourceCoordinates is None:
            self.sourceCoordinates = APDMValueTypes.SkyCoordinates()
        if self.sourceName is None:
            self.sourceName = ""
        if self.sourceVelocity is None:
            self.sourceVelocity = APDMValueTypes.Velocity()
        if self.pMRA is None:
            self.pMRA = APDMValueTypes.AngularVelocity(0)
        if self.pMDec is None:
            self.pMDec = APDMValueTypes.AngularVelocity(0)
        if self.nonSiderealMotion is None:
            self.nonSiderealMotion = False
        if self.solarSystemObject is None:
            self.solarSystemObject = 'Ephemeris'
        if self.parallax is None:
            self.parallax = APDMValueTypes.Angle(0)
        if self.sourceEphemeris is None:
            self.sourceEphemeris = 'None'
        if self.name is None:
            self.name = ''
        if self.isQuery is None:
            self.isQuery = False
#         if not self.__hasFieldPattern():
#             # We need a field pattern create a Pointing Pattern
#             fp = PointingPattern()
#             sc = APDMValueTypes.SkyCoordinates()
#             sc.longitude.set(0)
#             sc.latitude.set(0)
#             sc.system = u'J2000'
#             sc.type = u'RELATIVE'
#             fp.phaseCenterCoordinates.append(sc)
#             self.setFieldPattern(fp)

        self.__solarSystemObjects = ['Mercury', 'Venus', 'Moon', 'Mars',
                                     'Jupiter', 'Saturn', 'Uranus', 'Neptune',
                                     'Pluto', 'Ephemeris', 'Sun']
        self.__ephemerisObjects = ['Io', 'Callisto', 'Europa', 'Ganymede',
                                   'Titan', 'Ceres', 'Pallas', 'Juno',
                                   'Vesta']

    def isPlanetOrEphemeris(self):
        '''
        Returns True if this FieldSource represents one of the planetary sources
        currently supported by the system. These are:

        "Mercury", "Venus", "Moon", "Mars", "Jupiter",
        "Saturn", "Uranus", "Neptune", "Pluto", "Sun",
        "Io", "Callisto", "Europa", "Ganymede", "Titan",
        "Ceres", "Pallas", "Juno", "Vesta"
        '''
        if self.nonSiderealMotion and self.solarSystemObject is not None:
            if ( self.solarSystemObject in self.__solarSystemObjects ) or ( self.solarSystemObject in self.__ephemerisObjects ):
                return True
        return False    
        
    def processPlanetOrEphemeris(self, planetName = None):
        '''
        Correctly sets several data fields that need to be specially processed in the case
        of the source being a planetary source.
        
        This function is not meant to be called directly from the user. It will be called either from
        CCL.SchedBlock.SchedBlock.processPlanet() (which in turn is called from
        Observation.SchedulingBlock.SchedulingBlock); or from the child class 
        CCL.FieldSource.PlanetSource.
        '''
        if self.nonSiderealMotion:
            if planetName is None:
                planetName = self.solarSystemObject
            planetName = planetName.capitalize()
            if planetName in self.__solarSystemObjects:
                self.solarSystemObject = planetName
                self.sourceName = planetName
            elif planetName in self.__ephemerisObjects:
                # These are actually treated internally as ephemeris objects
                filename = "HorizonsEphemeris%s.txt" % planetName
                filePath = findFile(filename)[0]
                if filePath == '':
                    # This means we didn't find the file
                    ex = ControlExceptionsImpl.IllegalParameterErrorExImpl()
                    ex.setData("Message",
                               "Unable to find ephemeris file for '%s'" %
                               planetName)
                    raise ex
                self.loadSourceEphemeris(filePath)
                self.sourceName=planetName
            else:
                ex = ControlExceptionsImpl.IllegalParameterErrorExImpl()
                ex.setData("Message", 'The object ' + planetName +
                           ' is not recognized as a solar system object')
                raise ex

    def setQuery(self, QuerySource):
        '''
        Helper function that set the query object and the isQuery flag.
        '''
        self.QuerySource = QuerySource
        self.isQuery = True

    def resetQuery(self):
        '''
        Helper function that removes the Query Source
        '''
        self.QuerySource = None
        self.isQuery = False
 
    def loadSourceEphemeris(self, ephemerisFileName, duration="1 d"):
        from CCL.HorizonResultParser import HorizonResultParser
        parser = HorizonResultParser(ephemerisFileName)
        self.solarSystemObject = 'Ephemeris'
        self.nonSiderealMotion = True
        self.sourceEphemeris = parser.getEphemerisString(duration)

    def getFieldPattern(self):
        '''
        This method will return whatever field pattern is defined on this
        FieldSource
        '''
        if self.CirclePattern is not None:
            return self.CirclePattern
        if self.SpiralPattern is not None:
            return self.SpiralPattern
        if self.RectanglePattern is not None:
            return self.RectanglePattern
        if self.CrossPattern is not None:
            return self.CrossPattern
        if self.PointingPattern is not None:
            return self.PointingPattern
        
    def setFieldPattern(self, FieldPattern):
        '''
        This method will clear whatever field pattern is currently defined
        and replace it with the new one.
        '''
        
        if isinstance(FieldPattern, APDMEntities.SchedBlock.RectanglePatternT):
            self.__clearFieldPattern()
            self.RectanglePattern = FieldPattern
        elif isinstance(FieldPattern, APDMEntities.SchedBlock.SpiralPatternT):
            self.__clearFieldPattern()
            self.SpiralPattern = FieldPattern
        elif isinstance(FieldPattern, APDMEntities.SchedBlock.CirclePatternT):
            self.__clearFieldPattern()
            self.CirclePattern = FieldPattern
        elif isinstance(FieldPattern, APDMEntities.SchedBlock.CrossPatternT):
            self.__clearFieldPattern()
            self.CrossPattern = FieldPattern
        elif isinstance(FieldPattern,
                        APDMEntities.SchedBlock.PointingPatternT):
            self.__clearFieldPattern()
            self.PointingPattern = FieldPattern
        else:
            msg = "Unrecognized type of FieldPattern"
            raise ControlExceptionsImpl.IllegalParameterErrorExImpl(msg)
 
    def __hasFieldPattern(self):
        '''
        This method returns true only if no patterns are defined.  This should
        NEVER be the case for an fully instanciated class.
        '''
        patternDefined = self.CirclePattern is not None
        patternDefined |= self.SpiralPattern is not None
        patternDefined |= self.RectanglePattern is not None
        patternDefined |= self.CrossPattern is not None
        patternDefined |= self.PointingPattern is not None
        return patternDefined

    def __clearFieldPattern(self):
        '''
        This method should only be used when about to define a new pattern
        '''
        self.CirclePattern = None
        self.SpiralPattern = None
        self.RectanglePattern = None
        self.CrossPattern = None
        self.PointingPattern = None

    #def getFieldPatternSubscanList(self, subscanDuration):
    #    if self.pattern is None:
    #        return []
    #    return self.pattern.generateSubscanList(subscanDuration)

APDMEntities.SchedBlock.FieldSourceT._SetSupersedingClass(FieldSource)


    
class Reference(APDMEntities.SchedBlock.ReferenceT):
    def __init__(self, *args, **kw):
        super(APDMEntities.SchedBlock.ReferenceT, self).\
                                                  __init__(*args, **kw)
        if self.referenceCoordinates is None:
            self.referenceCoordinates = APDMValueTypes.SkyCoordinates()
        if self.integrationTime is None:
            self.integrationTime = APDMValueTypes.Time(0)
        if self.cycleTime is None:
            self.cycleTime = APDMValueTypes.Time(0)
        if self.subScanDuration is None:
            self.subScanDuration = APDMValueTypes.Time(0)

    def setCoordinates(self, longitude, latitude, system = u'J2000',
                       type = u'RELATIVE'):
        '''
        This is just a helper method to simplify setting the coordinates
        '''
        self.referenceCoordinates.longitude.set(longitude)
        self.referenceCoordinates.latitude.set(latitude)
        self.referenceCoordinates.system = system
        self.referenceCoordinates.type = type

APDMEntities.SchedBlock.ReferenceT._SetSupersedingClass(Reference)

class FieldPatternSubscanSpec:
    '''
    This class holds the information returned from a pointing pattern
    that is necessary for specifing a pointing pattern.

    The returned structures are both Control.SourceOffsets
    
    The constructor takes two values in if either of them is None
    then the corresponding value is set to zero.  As a convience
    if the pointingOffset only has two values the velocities are set to 0.
    '''
    def __init__(self, PointingOffset = None, DelayCenterOffset = None):
        if PointingOffset is None:
            PointingOffset = Control.SourceOffset(0,0,0,0, Control.HORIZON)
        if DelayCenterOffset is None:
            DelayCenterOffset = Control.SourceOffset(0,0,0,0,
                                                     Control.EQUATORIAL)
        if not isinstance(PointingOffset, Control.SourceOffset) or \
           not isinstance(DelayCenterOffset, Control.SourceOffset):
            msg = "Pointing and Delay Center offsets must be Source Offsets"
            raise ControlExceptionsImpl.IllegalParameterErrorExImpl(msg)
        
        self.pointingOffset = PointingOffset
        self.delayCenterOffset = DelayCenterOffset
    
    
## FieldPatternT 
## #CirclePatternT
class PointingPattern(APDMEntities.SchedBlock.PointingPatternT):
    def __init__(self, *args, **kw):
        super(APDMEntities.SchedBlock.PointingPatternT, self).\
                                                        __init__(*args, **kw)
        self.type = u'point'

    def generateSubscanList(self, subscanDuration):
        # For now we only support RELATIVE positions.  Need to
        # disucss with Ralph and Nuria how to do ABSOLUTE
        offsetList = []
        for skyCoordinates in self.phaseCenterCoordinates:
            onemasInRad = 4.8481368110953598e-06;
            if (skyCoordinates.system != "J2000") and\
                   (abs(skyCoordinates.latitude.get()) > onemasInRad or
                    abs(skyCoordinates.longitude.get()) > onemasInRad):
                msg = "Pointing Patterns must be specified in J2000 for now"
                raise ControlExceptionsImpl.IllegalParameterErrorExImpl(msg)
            if skyCoordinates.type != "RELATIVE":
                msg = "Only Relative pointing patterns are currently supported"
                raise ControlExceptionsImpl.IllegalParameterErrorExImpl(msg)
            offset = Control.SourceOffset(skyCoordinates.longitude.get(),
                                          skyCoordinates.latitude.get(),
                                          0,0,Control.EQUATORIAL)  
            offsetList.append(FieldPatternSubscanSpec(offset, offset))
        return offsetList
APDMEntities.SchedBlock.PointingPatternT._SetSupersedingClass(PointingPattern)
                   
class RectanglePattern(APDMEntities.SchedBlock.RectanglePatternT):
    def __init__(self, *args, **kw):
        super(APDMEntities.SchedBlock.RectanglePatternT, self)\
                                                         .__init__(*args, **kw)
        self.type = u'rectangle'
        if self.patternCenterCoordinates is None:
            self.patternCenterCoordinates = APDMValueTypes.SkyCoordinates()
        if self.longitudeLength is None:
            self.longitudeLength = APDMValueTypes.Angle(0)
        if self.latitudeLength is None:
            self.latitudeLength = APDMValueTypes.Angle(0)
        if self.orthogonalStep is None:
            self.orthogonalStep = APDMValueTypes.Angle(0)
        if self.scanDirection is None:
            self.scanDirection = u'longitude'
        if self.orientation is None:
            self.orientation = APDMValueTypes.Angle(0)
        if self.scanningCoordinateSystem is None:
            self.scanningCoordinateSystem = u'azel'
        if self.uniDirectionalScan is None:
            self.uniDirectionalScan = False
            
    def generateSubscanList(self, subscanDuration):
        """
        This method generates a list of Control.SubscanOffset objects
        (longOffsetStart, latOffsetStart, longVelocity, latVelocity)
        which define the strokes for this raster map.

        The subscan duration is needed to covert the length of strokes
        into velocities.
        """
        latLength  = self.latitudeLength.get()
        longLength = self.longitudeLength.get()
        orthStep   = self.orthogonalStep.get()

        longStart =  -longLength/2.0
        latStart  =  -latLength/2.0

        unrotatedList = []
        if self.scanDirection == u'longitude':
            nSubscans = int(round(latLength/orthStep)) + 1
            for subscan in range(nSubscans):
                offset = latStart + (orthStep * subscan)
                if not self.uniDirectionalScan and subscan % 2 != 0:
                    unrotatedList.append((-longStart, offset,
                                          -longLength/subscanDuration, 0))
                else:
                    unrotatedList.append((longStart, offset,
                                          longLength/subscanDuration, 0))
                
        else:
            nSubscans = int(round(longLength/orthStep)) + 1
            for subscan in range(nSubscans):
                offset = longStart + (orthStep * subscan)
                if not self.uniDirectionalScan and subscan % 2 != 0:
                    unrotatedList.append((offset, -latStart,
                                          0, -latLength/subscanDuration))
                else:
                    unrotatedList.append((offset, latStart,
                                          0, latLength/subscanDuration))

        # Now to run this through a rotation matrix
        rotatedList = []
        co = math.cos(self.orientation.get())
        so = math.sin(self.orientation.get())
        for scan in unrotatedList:
            rotatedList.append(((scan[0]*co) - (scan[1] * so),
                                (scan[0]*so) + (scan[1] * co),
                                (scan[2]*co) - (scan[3] * so),
                                (scan[2]*so) + (scan[3] * co)))


        # Now apply any offset of the centerpoint
        if self.patternCenterCoordinates.type == u'ABSOLUTE':
            longOffset = 0
            latOffset  = 0
        else:
            longOffset = self.patternCenterCoordinates.longitude.get()
            latOffset  = self.patternCenterCoordinates.latitude.get()

        if self.scanningCoordinateSystem == "azel":
            offsetFrame = Control.HORIZON
        elif self.scanningCoordinateSystem == "J2000":
            offsetFrame = Control.EQUATORIAL
        else:
            msg = "Unsupported scanning coordinate system " + \
                  self.scanningCoordinateSystem
            raise ControlExceptionsImpl.IllegalParameterErrorExImpl(msg)

        subscanList = []
        for scan in rotatedList:
            offset = Control.SourceOffset(scan[0] + longOffset,
                                          scan[1] + latOffset,
                                          scan[2], scan[3],
                                          offsetFrame)
            subscanList.append(FieldPatternSubscanSpec(offset, None))

        return subscanList

    @staticmethod
    def CreateFromDocument(xmlString):
        # We can only create this from the SchedBlock level since we
        # need an entity.
        if xmlString[:22] == '<?xml version="1.0" ?>':
            xmlString = xmlString[22:]
   
        wrappedString = '''<?xml version="1.0" ?>
        <ns1:SchedBlock xmlns:ns1="Alma/ObsPrep/SchedBlock">
        <ns1:FieldSource> %s </ns1:FieldSource>
        </ns1:SchedBlock>''' % xmlString
        sb = APDMEntities.SchedBlock.CreateFromDocument(wrappedString)
        return sb.FieldSource[0].RectanglePattern

APDMEntities.SchedBlock.RectanglePatternT._SetSupersedingClass(RectanglePattern)
## CrossPatternT 
## #SpiralPatternT 


class FrequencySetup(APDMEntities.SchedBlock.FrequencySetupT):
    def __init__(self,  *args, **kw):
        super(APDMEntities.SchedBlock.FrequencySetupT, self)\
                                                       .__init__(*args, **kw)

        if self.restFrequency is None:
            self.restFrequency = APDMValueTypes.Frequency(0)
        if self.transitionName is None:
            self.transitionName = ''
        if self.receiverBand is None:
            self.receiverBand = u'ALMA_RB_03'
        if self.lO1Frequency is None:
            self.lO1Frequency = APDMValueTypes.Frequency(0)
        if self.isUserSpecifiedLO1 is None:
            self.isUserSpecifiedLO1 = False
        if self.hasHardwareSetup is None:
            self.hasHardwareSetup = False
        if self.floog is None:
            self.floog = APDMValueTypes.Frequency(0)
        
                                
APDMEntities.SchedBlock.FrequencySetupT._SetSupersedingClass(FrequencySetup)
    
class BaseBandSpecification(APDMEntities.SchedBlock.BaseBandSpecificationT):
    def __init__(self,  *args, **kw):
        super(APDMEntities.SchedBlock.BaseBandSpecificationT, self)\
                                                        .__init__(*args, **kw)
        if self.baseBandName is None:
            self.baseBandName = 'BB_1'
        if self.almatype is None:
            self.almatype = "APDM::BaseBandSpecification"
        if self.centerFrequency is None:
            self.centerFrequency = APDMValueTypes.Frequency(0)
        if self.frequencySwitching is None:
            self.frequencySwitching = False
        if self.lO2Frequency is None:
            self.lO2Frequency = APDMValueTypes.Frequency(0)
        if self.weighting is None:
            self.weighting = 100.0
        if self.useUSB is None:
            self.useUSB = True
        if self.use12GHzFilter is None:
            self.use12GHzFilter = False
        if self.entityPartId is None:
            self.entityPartId = generateNextPartId()

    def getReference(self):
        reference = APDMEntities.SchedBlock.SchedBlockRefT()
        reference.partId = self.entityPartId
        reference.entityTypeName  = u'SchedBlock'
        reference.documentVersion = u'1'
        reference.entityId = u'uid://X00/X0000/X0'
        return reference



## #AbstractSwitchingCycleT
## #BaseBandSwitchingCycleT
## #SwitchingCycleT 


class SpectralSpec(APDMEntities.SchedBlock.SpectralSpecT,
                   XMLFragment):
    def __init__(self,  *args, **kw):
        super(APDMEntities.SchedBlock.SpectralSpecT, self)\
                                                     .__init__(*args, **kw)
        XMLFragment.__init__(self,'SpectralSpec')

        if self.switchingType is None:
            self.switchingType = u'NO_SWITCHING'
        if self.receiverType is None:
            self.receiverType = u'SSB'
        if self.FrequencySetup is None:
            self.FrequencySetup = FrequencySetup()
            

    def setCorrelatorConfiguration(self, CorrelatorConfiguration):
        if isinstance(CorrelatorConfiguration,
                      APDMEntities.SchedBlock.BLCorrelatorConfigurationT):
            self.BLCorrelatorConfiguration = CorrelatorConfiguration
            self.ACACorrelatorConfiguration = None
        else:
            self.BLCorrelatorConfiguration = None
            self.ACACorrelatorConfiguration = CorrelatorConfiguration

    def getCorrelatorConfiguration(self):
        if self.hasBLCorrelatorConfiguration():
            return self.BLCorrelatorConfiguration
        if self.hasACACorrelatorConfiguration():
            return self.ACACorrelatorConfiguration
        return None
            
    def hasBLCorrelatorConfiguration(self):
        return self.BLCorrelatorConfiguration is not None

    def hasACACorrelatorConfiguration(self):
        return self.ACACorrelatorConfiguration is not None

    def hasCorrelatorConfiguration(self):
        return self.hasBLCorrelatorConfiguration() or \
               self.hasACACorrelatorConfiguration()

    def hasSquareLawSetup(self):
        return self.SquareLawSetup is not None

    def roundTimes(self):
        if self.hasBLCorrelatorConfiguration():
            self.BLCorrelatorConfiguration.roundTimes()
        if self.hasACACorrelatorConfiguration():
            self.ACACorrelatorConfiguration.roundTimes()
        if self.hasSquareLawSetup():
            self.SquareLawSetup.roundTimes()

    def getMeanFrequency(self):
        '''
        This method returns the mean frequency of all specified basebands.
        '''
        freqSum = 0
        for bbs in self.FrequencySetup.BaseBandSpecification:
            freqSum += bbs.centerFrequency.get()
        nbbs = len(self.FrequencySetup.BaseBandSpecification)
        if (nbbs is not 0):
            return freqSum/nbbs
        else:
            return 0;

APDMEntities.SchedBlock.SpectralSpecT._SetSupersedingClass(SpectralSpec)
           

class SquareLawSetup(APDMEntities.SchedBlock.SquareLawSetupT):
    def __init__(self,  *args, **kw):
        super(APDMEntities.SchedBlock.SquareLawSetupT, self)\
                                                        .__init__(*args, **kw)
        if self.integrationDuration is None:
            self.integrationDuration = APDMValueTypes.Time(0)

    def roundTimes(self):
        '''
        This method ensures that the integrationDuration specified is an
        integer number of the 2 kHz samples.
        '''
        SAMPLE_DURATION = 0.0005
        
        self.integrationDuration.set(max(SAMPLE_DURATION,\
              round(self.integrationDuration.get()/SAMPLE_DURATION) *
                                         SAMPLE_DURATION))
        
APDMEntities.SchedBlock.SquareLawSetupT._SetSupersedingClass(SquareLawSetup)
        
class OpticalCameraSpec(APDMEntities.SchedBlock.OpticalCameraSpecT):
    def __init__(self,  *args, **kw):
        super(APDMEntities.SchedBlock.OpticalCameraSpecT, self)\
                                                        .__init__(*args, **kw)
        if self.minIntegrationTime is None:
            self.minIntegrationTime = APDMValueTypes.Time(0)
        if self.filter is None:
            self.filter = u'night'
            
APDMEntities.SchedBlock.OpticalCameraSpecT.\
                        _SetSupersedingClass(OpticalCameraSpec)
## #?ObsProcedureT 

# ------------Correlator Config ----------------------
class AbstractCorrelatorConfiguration:
    def __init__(self):
        if self.cAM is None:
            self.cAM = u'NORMAL'
        if self.integrationDuration is None:
            self.integrationDuration = APDMValueTypes.Time(0)
        if self.channelAverageDuration is None:
            self.channelAverageDuration = APDMValueTypes.Time(0)
        if self.aPCDataSets is None:
            self.aPCDataSets = u'AP_UNCORRECTED'

    def roundTimes(self, dumpDuration):
        chAv = self.channelAverageDuration.get()
        chAv = max(dumpDuration, round(chAv/dumpDuration) *dumpDuration)
        self.channelAverageDuration.set(chAv)

        intDur = self.integrationDuration.get()
        intDur = max(chAv, round(intDur/chAv) *chAv)
        self.integrationDuration.set(intDur)

class BLCorrelatorConfiguration\
      (APDMEntities.SchedBlock.BLCorrelatorConfigurationT,
       AbstractCorrelatorConfiguration):
    def __init__(self,  *args, **kw):
        super(APDMEntities.SchedBlock.BLCorrelatorConfigurationT, self)\
                                                        .__init__(*args, **kw)
        AbstractCorrelatorConfiguration.__init__(self)
        if self.dumpDuration is None:
            self.dumpDuration = APDMValueTypes.Time(0)

    def roundTimes(self):
        '''
        If dump duration is less than 48 ms we round it to the nearest 16 ms
        boundary.  If it is greater than 48 ms we round it to the
        nearest 48 ms boundary
        
        This method checks that the dump duration is a multiple of
        16 ms and then rounds up the Channel Average so it is a multiple
        of the dump duration.  Finally the Integration duration is adjusted
        so that it is a multiple of the dump duration and 48 ms
        '''
        dump = self.dumpDuration.get()
        dump = max(0.016, round(dump/0.016) * 0.016)
        self.dumpDuration.set(dump)

        AbstractCorrelatorConfiguration.roundTimes(self, dump)

            
APDMEntities.SchedBlock.BLCorrelatorConfigurationT.\
                 _SetSupersedingClass(BLCorrelatorConfiguration)


class ACACorrelatorConfiguration\
      (APDMEntities.SchedBlock.ACACorrelatorConfigurationT,
       AbstractCorrelatorConfiguration):
    def __init__(self,  *args, **kw):
        super(APDMEntities.SchedBlock.ACACorrelatorConfigurationT, self)\
                                                        .__init__(*args, **kw)
        AbstractCorrelatorConfiguration.__init__(self)


    def roundTimes(self):
        AbstractCorrelatorConfiguration.roundTimes(self, 0.016)

APDMEntities.SchedBlock.ACACorrelatorConfigurationT.\
                 _SetSupersedingClass(ACACorrelatorConfiguration)

class ACAPhaseSwitchingConfiguration\
      (APDMEntities.SchedBlock.ACAPhaseSwitchingConfigurationT):
    def __init__(self,  *args, **kw):
        super(APDMEntities.SchedBlock.ACAPhaseSwitchingConfigurationT, self)\
                                                        .__init__(*args, **kw)
        if self.doD180modulation is None:
            self.doD180modulation = True
        if self.doD180demodulation is None:
            self.doD180demodulation = True
        if self.d180Duration is None:
            self.d180Duration = APDMValueTypes.Time(0)
APDMEntities.SchedBlock.ACAPhaseSwitchingConfigurationT.\
                 _SetSupersedingClass(ACAPhaseSwitchingConfiguration)

class AbstractBaseBandConfig:
    def __init__(self):
        if self.dataProducts is None:
            self.dataProducts = u'CROSS_AND_AUTO'
        else:
            # Something funny about default values makes this line
            # necessary so the dataProducts show up in the XML
            self.dataProducts = self.dataProducts
        
    def linkBaseBandSpecification(self, BaseBandSpecification):
        self.BaseBandSpecificationRef = BaseBandSpecification.getReference()
        

class BLBaseBandConfig(APDMEntities.SchedBlock.BLBaseBandConfigT,
                       AbstractBaseBandConfig):
    def __init__(self,  *args, **kw):
        super(APDMEntities.SchedBlock.BLBaseBandConfigT, self)\
                                                        .__init__(*args, **kw)
        AbstractBaseBandConfig.__init__(self)
       
APDMEntities.SchedBlock.BLBaseBandConfigT.\
                              _SetSupersedingClass(BLBaseBandConfig)

class ACABaseBandConfig(APDMEntities.SchedBlock.ACABaseBandConfigT,
                        AbstractBaseBandConfig):
    def __init__(self,  *args, **kw):
        super(APDMEntities.SchedBlock.ACABaseBandConfigT, self)\
                                                        .__init__(*args, **kw)
        AbstractBaseBandConfig.__init__(self)
    
        # ACA Specific portion
        if self.centerFreqOfResidualDelay is None:
            centerFreqOfResidualDelay = APDMValueTypes.Frequency(3.0E9)
        if self.polarizationMode is None:
            self.polarizationMode = u'ACA_STANDARD'

APDMEntities.SchedBlock.ACABaseBandConfigT.\
                              _SetSupersedingClass(ACABaseBandConfig)

class AbstractSpectralWindow:
    def __init__(self):
        if self.centerFrequency is None:
            self.centerFrequency = APDMValueTypes.Frequency(3.0E9)
        if self.spectralAveragingFactor is None:
            self.spectralAveragingFactor = 1
        if self.name is None:
            self.name = ''
        if self.effectiveBandwidth is None:
            self.effectiveBandwidth = APDMValueTypes.Frequency(2.0E9)
        if self.effectiveNumberOfChannels is None:
            self.effectiveNumberOfChannels = 256
        if self.sideBand is None:
            self.sideBand = u'USB'
        if self.associatedSpectralWindowNumberInPair is None:
            self.associatedSpectralWindowNumberInPair = 0
        if self.useThisSpectralWindow is None:
            self.useThisSpectralWindow = True
        if self.windowFunction is None:
            self.windowFunction = u'HANNING'
        if self.polnProducts is None:
            self.polnProducts = u'XX'
       

class BLSpectralWindow(APDMEntities.SchedBlock.BLSpectralWindowT,
                       AbstractSpectralWindow):
    def __init__(self,  *args, **kw):
        super(APDMEntities.SchedBlock.BLSpectralWindowT, self)\
                                                        .__init__(*args, **kw)
        AbstractSpectralWindow.__init__(self)

        if self.correlationBits is None:
            self.correlationBits = u'BITS_2x2'
        if self.correlationNyquistOversampling is None:
            self.correlationNyquistOversampling = False
        if self.quantizationCorrection is None:
            self.quantizationCorrection = True

APDMEntities.SchedBlock.BLSpectralWindowT.\
                              _SetSupersedingClass(BLSpectralWindow)

class ACASpectralWindow(APDMEntities.SchedBlock.ACASpectralWindowT,
                        AbstractSpectralWindow):
    def __init__(self,  *args, **kw):
        super(APDMEntities.SchedBlock.ACASpectralWindowT, self)\
                                                        .__init__(*args, **kw)
        AbstractSpectralWindow.__init__(self)

        # ACASpecific Portion
        if self.frqChProfReproduction is None:
            self.frqChProfReproduction = True

APDMEntities.SchedBlock.ACASpectralWindowT.\
                              _SetSupersedingClass(ACASpectralWindow)

class ChannelAverageRegion(APDMEntities.SchedBlock.ChannelAverageRegionT):
    def __init__(self, *args, **kw):
        super(APDMEntities.SchedBlock.ChannelAverageRegionT, self)\
                                                        .__init__(*args, **kw)
        if self.startChannel is None:
            self.startChannel = 0
        if self.numberChannels is None:
            self.numberChannels = 0
APDMEntities.SchedBlock.ChannelAverageRegionT.\
                        _SetSupersedingClass(ChannelAverageRegion)        

class ObservingParameters:
    def __init__(self):
        if self.name is None:
            self.name = ''
        if self.expertParameter is None:
            self.expertParameter = []

class CalibratorParameters(ObservingParameters):
    def __init__(self):
        ObservingParameters.__init__(self)        
        if self.cycleTime is None:
            self.cycletTime = APDMValueTypes.Time(0)
        if self.defaultIntegrationTime is None:
            self.defaultIntegrationTime =  APDMValueTypes.Time(0)
        if self.dataOrigin is None:
            self.dataOrigin = 'CHANNEL_AVERAGE_CROSS'
        if self.subScanDuration is None:
            self.subScanDuration = APDMValueTypes.Time(0)

class AmplitudeCalParameters(APDMEntities.SchedBlock.AmplitudeCalParametersT,
                             CalibratorParameters):
    def __init__(self, *args, **kw):
        super(APDMEntities.SchedBlock.AmplitudeCalParametersT, self)\
                                                     .__init__(*args, **kw)
        CalibratorParameters.__init__(self)
APDMEntities.SchedBlock.AmplitudeCalParametersT.\
                        _SetSupersedingClass(AmplitudeCalParameters)

class AtmosphericCalParameters\
          (APDMEntities.SchedBlock.AtmosphericCalParametersT,
           CalibratorParameters):
    def __init__(self, *args, **kw):
        super(APDMEntities.SchedBlock.AtmosphericCalParametersT, self)\
                                                     .__init__(*args, **kw)
        CalibratorParameters.__init__(self)

        if self.useHotLoad is None:
            self.useHotLoad = True
        
APDMEntities.SchedBlock.AtmosphericCalParametersT.\
                        _SetSupersedingClass(AtmosphericCalParameters)

class BandpassCalParameters(APDMEntities.SchedBlock.BandpassCalParametersT,
                             CalibratorParameters):
    def __init__(self, *args, **kw):
        super(APDMEntities.SchedBlock.BandpassCalParametersT, self)\
                                                     .__init__(*args, **kw)
        CalibratorParameters.__init__(self)

        if self.desiredAmplitudeAccuracy is None:
            self.desiredAmplitudeAccuracy = 5.0
        if self.maximumElapsedTime is None:
            self.maximumElapsedTime = APDMValueTypes.Time(0)
        if self.desiredPhaseAccuracy is None:
            self.desiredPhaseAccuracy = APDMValueTypes.Angle(0)

APDMEntities.SchedBlock.BandpassCalParametersT.\
                        _SetSupersedingClass(BandpassCalParameters)

class DelayCalParameters(APDMEntities.SchedBlock.DelayCalParametersT,
                             CalibratorParameters):
    def __init__(self, *args, **kw):
        super(APDMEntities.SchedBlock.DelayCalParametersT, self)\
                                                     .__init__(*args, **kw)
        CalibratorParameters.__init__(self)
APDMEntities.SchedBlock.DelayCalParametersT.\
                                _SetSupersedingClass(DelayCalParameters)

class FocusCalParameters(APDMEntities.SchedBlock.FocusCalParametersT,
                             CalibratorParameters):
    def __init__(self, *args, **kw):
        super(APDMEntities.SchedBlock.FocusCalParametersT, self)\
                                                     .__init__(*args, **kw)
        CalibratorParameters.__init__(self)

        if self.AxisToAdjust is None:
            self.AxisToAdjust = u'ZAxis'

        
APDMEntities.SchedBlock.FocusCalParametersT.\
                                    _SetSupersedingClass(FocusCalParameters)

## OpticalPointingParametersT 
class PhaseCalParameters(APDMEntities.SchedBlock.PhaseCalParametersT,
                             CalibratorParameters):
    def __init__(self, *args, **kw):
        super(APDMEntities.SchedBlock.PhaseCalParametersT, self)\
                                                     .__init__(*args, **kw)
        CalibratorParameters.__init__(self)
APDMEntities.SchedBlock.PhaseCalParametersT.\
                        _SetSupersedingClass(PhaseCalParameters)

class PointingCalParameters(APDMEntities.SchedBlock.PointingCalParametersT,
                             CalibratorParameters):
    def __init__(self, *args, **kw):
        super(APDMEntities.SchedBlock.PointingCalParametersT, self)\
                                                     .__init__(*args, **kw)
        CalibratorParameters.__init__(self)
        
        if self.desiredAccuracy is None:
            self.desiredAccuracy = APDMValueTypes.Angle(0)
        if self.pointingMethod is None:
            self.pointingMethod = u'FIVE_POINT'
        if self.maximumElapsedTime is None:
            self.maximumElapsedTime = APDMValueTypes.Time(0)
        if self.excursion is None:
            self.excursion = APDMValueTypes.Angle(0)
        
APDMEntities.SchedBlock.PointingCalParametersT.\
                        _SetSupersedingClass(PointingCalParameters)

class PolarizationCalParameters(APDMEntities.SchedBlock.PolarizationCalParametersT,
                             CalibratorParameters):
    def __init__(self, *args, **kw):
        super(APDMEntities.SchedBlock.PolarizationCalParametersT, self)\
                                                     .__init__(*args, **kw)
        CalibratorParameters.__init__(self)

        if self.desiredPolAccuracy is None:
            self.desiredPolAccuracy = 0.0
            
APDMEntities.SchedBlock.PolarizationCalParametersT.\
                        _SetSupersedingClass(PolarizationCalParameters)

## #ReservationParametersT 
class ScienceParameters(APDMEntities.SchedBlock.ScienceParametersT, 
                        ObservingParameters):
    def __init__(self, *args, **kw):
        super(APDMEntities.SchedBlock.ScienceParametersT, self)\
                                                     .__init__(*args, **kw)
        ObservingParameters.__init__(self)
        if self.representativeBandwidth is None:
            self.representativeBandwidth = APDMValueTypes.Frequency(0)
        if self.representativeFrequency is None:
            self.representativeFrequency = APDMValueTypes.Frequency(0)
        if self.sensitivityGoal is None:
            self.sensitivityGoal =  APDMValueTypes.Sensitivity(0)
        if self.subScanDuration is None:
            self.subScanDuration = APDMValueTypes.Time(0)

APDMEntities.SchedBlock.ScienceParametersT.\
                        _SetSupersedingClass(ScienceParameters)


    
## #?HolographyParametersT 
class HolographyParameters(APDMEntities.SchedBlock.HolographyParametersT, 
                        ObservingParameters):
    def __init__(self, *args, **kw):
        super(APDMEntities.SchedBlock.HolographyParametersT, self)\
                                                     .__init__(*args, **kw)
        ObservingParameters.__init__(self)
        if self.frequency is None:
            self.frequency = APDMValueTypes.Frequency(0)
            self.frequency.set('104.02GHz')
        if self.startFraction is None:
            self.startFraction = 0.0
        if self.speed is None:
            self.speed = APDMValueTypes.AngularVelocity(0)
            self.speed.set('18 arcsec/s')
        if self.rowsCal is None:
            self.rowsCal =  4
        if self.calTime is None:
            self.calTime = APDMValueTypes.Time(0)
            self.calTime.set('10.08s');
        if self.nRows is None:
            self.nRows = 18
        if self.rowSize is None:    
            self.rowSize =  APDMValueTypes.Angle(0)
            self.rowSize.set("540 arcsec")
        if self.uniDirectionalScan is None:
            self.uniDirectioinalScan = True

APDMEntities.SchedBlock.HolographyParametersT.\
                        _SetSupersedingClass(HolographyParameters)
## #?SchedBlockControlT 

## #TemporalConstraintsT 
## #RadiometricPointingParametersT 

## SchedulingConstraintsT 
## #SpectralLineT 

#
# ___oOo___

