#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

# The object inheratence allows definition of class methods

from Control import ScanIntentData
from PyDataModelEnumeration import PyScanIntent
from PyDataModelEnumeration import PyCalDataOrigin
from PyDataModelEnumeration import PyCalibrationSet
from PyDataModelEnumeration import PyCalibrationFunction
from PyDataModelEnumeration import PyAntennaMotionPattern

from CCL.EnumerationHelper import getEnumeration
import ControlExceptions


class ScanIntent(object):
    def __init__(self, scanIntent, calibrationDataOrigin = None,
                 calibrationSet = None, calibrationFunction = None,
                 onlineProcessing = None):
        """
        The Scan Intent is a structure containing all of the information
        about the purpose of a scan it is passed to Data Capture and
        eventually finds it's way into the ASDM.

        This class simplifies the creation of Scan intents and provides
        [hopefully] intellegent defaults for the various intents.       
        """
        self.__createDefaultScanIntentData()

        self.setScanIntent(scanIntent, True)

        if calibrationDataOrigin is not None:
            self.setCalibrationDataOrigin(calibrationDataOrigin)
        if calibrationSet is not None:
            self.setCalibrationSet(calibrationSet)
        if calibrationFunction is not None:
            self.setCalibrationFunction(calibrationFunction)
        if onlineProcessing is not None:
            self.setOnlineProcessing(onlineProcessing)

        
    def getScanIntent(self):
        """
        Return the currently specified Scan Intent

        Result is a memeber of the ScanIntentMod enumeration:
        CALIBRATE_AMPLI, CALIBRATE_ATMOSPHERE, CALIBRATE_BANDPASS,
        CALIBRATE_DELAY, CALIBRATE_FLUX, CALIBRATE_FOCUS, CALIBRATE_FOCUS_X,
        CALIBRATE_FOCUS_Y, CALIBRATE_PHASE, CALIBRATE_POINTING,
        CALIBRATE_POLARIZATION, CALIBRATE_SIDEBAND_RATIO, CALIBRATE_WVR,
        DO_SKYDIP, MAP_ANTENNA_SURFACE, MAP_PRIMARY_BEAM, OBSERVE_TARGET,
        TEST, UNSPECIFIED
        """
        return self._scanIntentData.scanIntent

    def setScanIntent(self, scanIntent, useDefaults = True):
        """
        Return the currently specified Scan Intent

        Scan Intent must be a memeber of the ScanIntentMod enumeration:
        CALIBRATE_AMPLI, CALIBRATE_ATMOSPHERE, CALIBRATE_BANDPASS,
        CALIBRATE_DELAY, CALIBRATE_FLUX, CALIBRATE_FOCUS, CALIBRATE_FOCUS_X,
        CALIBRATE_FOCUS_Y, CALIBRATE_PHASE, CALIBRATE_POINTING,
        CALIBRATE_POLARIZATION, CALIBRATE_SIDEBAND_RATIO, CALIBRATE_WVR,
        DO_SKYDIP, MAP_ANTENNA_SURFACE, MAP_PRIMARY_BEAM, OBSERVE_TARGET,
        TEST, UNSPECIFIED

        It is valid to either pass the enumeration or a string representation.

        If useDefaults is true, then a "best guess" is made of the
        correct values for the remaining fields.  If useDefaults is false
        then the remaining values are left unchanged.
        """
        self._scanIntentData.scanIntent = getEnumeration(scanIntent,
                                                         PyScanIntent)
        if useDefaults:
            self.__getDefaultForIntent()

    def getCalibrationDataOrigin(self):
        """
        Return the origin of data used to derive calibration results

        Should be a memeber of the CalDataOriginMod Enumeration:
        CHANNEL_AVERAGE_AUTO, CHANNEL_AVERAGE_CROSS, 
        FULL_RESOLUTION_AUTO, FULL_RESOLUTION_CROSS, HOLOGRAPHY,
        OPTICAL_POINTING, TOTAL_POWER, WVR

        It is valid to either pass the enumeration or a string representation.

        """
        
        return self._scanIntentData.calDataOrig
            
    def setCalibrationDataOrigin(self, dataOrigin):
        """
        Set the origin of data used to derive calibration results

        Should be a memeber of the CalDataOriginMod Enumeration:
        CHANNEL_AVERAGE_AUTO, CHANNEL_AVERAGE_CROSS, 
        FULL_RESOLUTION_AUTO, FULL_RESOLUTION_CROSS, HOLOGRAPHY,
        OPTICAL_POINTING, TOTAL_POWER, WVR

        It is valid to either pass the enumeration or a string representation.
        """
        self._scanIntentData.calDataOrig = getEnumeration(dataOrigin,
                                                          PyCalDataOrigin)

    def getCalibrationSet(self):
        """
        Return the set of calibration scans to be reduced together
        for a result.

        Should be a memeber of the CalibrationSetMod enumeration:
        AMPLI_CURVE, ANTENNA_POSITIONS, NONE, PHASE_CURVE,
        POINTING_MODEL, TEST, UNSPECIFIED
        """
        return self._scanIntentData.calSet
        
    def setCalibrationSet(self, calibrationDataSet):
        """
        Define the set of calibration scans to be reduced together
        for a result.

        Should be a member of the CalibrationSetMod enumeration:
        AMPLI_CURVE, ANTENNA_POSITIONS, NONE, PHASE_CURVE,
        POINTING_MODEL, TEST, UNSPECIFIED

        It is valid to either pass the enumeration or a string representation.
        """
        self._scanIntentData.calSet = getEnumeration(calibrationDataSet,
                                                     PyCalibrationSet)

    def getCalibrationFunction(self):
        """
        Returns the function of a scan in a calibration set.

        Should be a member of the CalibrationFunctionMod enumeration:
        FIRST, LAST, UNSPECIFIED
        """
        return self._scanIntentData.calFunction

    def setCalibrationFunction(self, calibrationFunction):
        """
        Set the function of a scan in a calibration set.

        Should be a member of the CalibrationFunctionMod enumeration:
        FIRST, LAST, UNSPECIFIED

        It is valid to either pass the enumeration or a string representation.
        """
        self._scanIntentData.calFunction =getEnumeration(calibrationFunction,
                                                         PyCalibrationFunction)

    def getAntennaMotionPattern(self):
        """
        Returns the Antenna motion pattern defined for this intent strucure

        Should be a member of the PyAntennaMotionPattern enumeration:
        NONE, CROSS_SCAN, SPIRAL, CIRCLE, THREE_POINTS, FOUR_POINTS,
        FIVE_POINTS, TEST, UNSPECIFIED
        """
        return self._scanIntentData.antennaMotionPattern

    def setAntennaMotionPattern(self, antennaMotionPattern):
        """
        Set the antenna motion pattern defined in the intent structure

        Should be a member of the PyAntennaMotionPattern enumeration:
        NONE, CROSS_SCAN, SPIRAL, CIRCLE, THREE_POINTS, FOUR_POINTS,
        FIVE_POINTS, TEST, UNSPECIFIED

        It is valid to either pass the enumeration or a string representation.
        """
        self._scanIntentData.antennaMotionPattern = \
                                   getEnumeration(antennaMotionPattern,
                                                  PyAntennaMotionPattern)

    def getOnlineProcessing(self):
        """
        Get the current state of the online processing flag.  If True
        then TelCal will attempt to produce a calibration result.  Otherwise
        the scan will be ignored by the Telcal system.
        """
        return self._scanIntentData.onlineCalibration

    def setOnlineProcessing(self, enabled):
        """
        Set the online processing flag.  If enabled is True then TelCal
        will attempt to produce a calibration result.  Otherwise the
        scan will be ignored by the Telcal system.
        """
        self._scanIntentData.onlineCalibration = enabled

    def getScanIntentData(self):
        """
        Method to return the IDL structure suitable for making CORBA calls.
        """
        return self._scanIntentData

        
    def __createDefaultScanIntentData(self):
        """
        This method creates a Default Scan Intent Mod.
        """
        
        # This is for now, tomorrow we should have none                    
        self._scanIntentData = \
                ScanIntentData(PyScanIntent.UNSPECIFIED,
                               PyCalDataOrigin.NONE,
                               PyCalibrationSet.NONE,
                               PyCalibrationFunction.UNSPECIFIED,
                               PyAntennaMotionPattern.NONE,
                               True)

    def __getDefaultForIntent(self):
        """
        This method puts in hopefully intellegent options for the
        various intents.
        """
        if self._scanIntentData.scanIntent == PyScanIntent.CALIBRATE_AMPLI:
            self.setCalibrationDataOrigin(PyCalDataOrigin.CHANNEL_AVERAGE_CROSS)
            self.setOnlineProcessing(True)
        elif self._scanIntentData.scanIntent == PyScanIntent.CALIBRATE_ATMOSPHERE:
            self.setCalibrationDataOrigin(PyCalDataOrigin. TOTAL_POWER)
            self.setOnlineProcessing(True)
        elif self._scanIntentData.scanIntent == PyScanIntent.CALIBRATE_BANDPASS:
            self.setCalibrationDataOrigin(PyCalDataOrigin.FULL_RESOLUTION_CROSS)
            self.setOnlineProcessing(True)
        elif self._scanIntentData.scanIntent == PyScanIntent.CALIBRATE_DELAY:
            self.setCalibrationDataOrigin(PyCalDataOrigin.FULL_RESOLUTION_CROSS)
            self.setOnlineProcessing(True)
        elif self._scanIntentData.scanIntent == PyScanIntent.CALIBRATE_FLUX:
            self.setCalibrationDataOrigin(PyCalDataOrigin.CHANNEL_AVERAGE_CROSS)
            self.setOnlineProcessing(False)
        elif self._scanIntentData.scanIntent == PyScanIntent.CALIBRATE_FOCUS:
            self.setCalibrationDataOrigin(PyCalDataOrigin.TOTAL_POWER)
            self.setOnlineProcessing(True)
        elif self._scanIntentData.scanIntent == PyScanIntent.CALIBRATE_FOCUS_X:
            self.setCalibrationDataOrigin(PyCalDataOrigin.TOTAL_POWER)
            self.setOnlineProcessing(True)
        elif self._scanIntentData.scanIntent == PyScanIntent.CALIBRATE_FOCUS_Y:
            self.setCalibrationDataOrigin(PyCalDataOrigin.TOTAL_POWER)
            self.setOnlineProcessing(True)
        elif self._scanIntentData.scanIntent == PyScanIntent.CALIBRATE_PHASE:
            self.setCalibrationDataOrigin(PyCalDataOrigin.CHANNEL_AVERAGE_CROSS)
            self.setOnlineProcessing(True)
        elif self._scanIntentData.scanIntent == PyScanIntent.CALIBRATE_POINTING:
            self.setCalibrationDataOrigin(PyCalDataOrigin.TOTAL_POWER)
            self.setAntennaMotionPattern(PyAntennaMotionPattern.UNSPECIFIED)
            self.setOnlineProcessing(True)
        elif self._scanIntentData.scanIntent == PyScanIntent.CALIBRATE_POLARIZATION:
            self.setCalibrationDataOrigin(PyCalDataOrigin.HOLOGRAPHY)
            self.setOnlineProcessing(False)
        elif self._scanIntentData.scanIntent == PyScanIntent.CALIBRATE_SIDEBAND_RATIO:
            self.setCalibrationDataOrigin(PyCalDataOrigin.CHANNEL_AVERAGE_CROSS)
            self.setOnlineProcessing(False)
        elif self._scanIntentData.scanIntent == PyScanIntent.CALIBRATE_WVR:
            self.setCalibrationDataOrigin(PyCalDataOrigin.WVR)
            self.setOnlineProcessing(False)
        elif self._scanIntentData.scanIntent == PyScanIntent.DO_SKYDIP:
            self.setCalibrationDataOrigin(PyCalDataOrigin.TOTAL_POWER)
            self.setOnlineProcessing(True)
        elif self._scanIntentData.scanIntent == PyScanIntent.MAP_ANTENNA_SURFACE:
            self.setCalibrationDataOrigin(PyCalDataOrigin.HOLOGRAPHY)
            self.setOnlineProcessing(False)
        elif self._scanIntentData.scanIntent == PyScanIntent.MAP_PRIMARY_BEAM:
            self.setCalibrationDataOrigin(PyCalDataOrigin.TOTAL_POWER)
            self.setOnlineProcessing(False)
        elif self._scanIntentData.scanIntent == PyScanIntent.OBSERVE_TARGET:
            self.setCalibrationDataOrigin(PyCalDataOrigin.NONE) 
            self.setOnlineProcessing(False)

        # The following intents do not have meaningful defaults:
        #    TEST
        #    UNSPECIFIED
