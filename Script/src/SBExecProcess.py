#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#

import sys, os, time, signal, pickle
import CORBA
import ScriptExecutorExceptionsImpl
import ScriptExecutorExceptions
from ScriptExec.ResourceManager import ResourceManager
from ScriptExec.Executor import Executor
from CCLExceptions import CCLExceptionsEx
from CCLExceptions import DeviceErrorEx
from CCLExceptions import NotYetImplementedEx
from CCLExceptions import CCLErrorEx
from Acspy.Common.Err import ACSError
from Acspy.Common.Err import pyExceptionToCORBA
from CCL.AntModeController import AntModeController
from CCL.ObservingMode import ObservingMode
from CCL.logging import getLogger

resMng = ResourceManager()
container = resMng.getContainerServices()
logger = getLogger()

# get the ScriptExecutor component
scriptExecName = sys.argv[1]
logger.logInfo("ScriptExecutor name: " + scriptExecName)
logger.logInfo("Getting ScriptExecutor component '" + scriptExecName + "'")
scriptExec = container.getComponent(scriptExecName)
resMng.addResource(scriptExecName, scriptExec, True)

scriptExec.setChildProcessPID(os.getpid())

# get the array name
try:
    arrayName = scriptExec.getArrayName()
except ScriptExecutorExceptions.ConfigurationErrorEx:
    logger.logErrorTrace("the array name has not been set in the ScriptExecutor")
    os._exit(1)
    
logger.logInfo("Array name: " + arrayName)

# get the source code to execute from the ScriptExecutor component
try:
    srcCode = scriptExec.getObservationScript()
except ScriptExecutorExceptions.ConfigurationErrorEx:
    logger.logErrorTrace("the observation script has not been set in the ScriptExecutor")
    os._exit(1)
logger.logInfo("Source code: " + srcCode)

# get the SB
try:
    schedBlock = scriptExec.getSB()
except ScriptExecutorExceptions.ConfigurationErrorEx:
    logger.logErrorTrace("the SchedBlock has not been set in the ScriptExecutor")
    os._exit(1)    
if len(schedBlock) > 20: n=20
else: n = len(schedBlock)
logger.logInfo("Scheduling Block: " + schedBlock[:n] + "\n...")

logger.logInfo("Starting SB executor process")

executor = Executor(container)
executor.setArrayName(arrayName)
executor.setSB(schedBlock)

def onSignal(signum, stackframe):
    logger.logInfo("Inside signal handler")
    locals = executor.getLocals()
    logger.logDebug("---> Locals: " + str(locals))
    for omobj in locals.values():
        if isinstance(omobj, ObservingMode):
            logger.logInfo("Found one observing mode in local ns: " + str(omobj))
            if not omobj.isInterrupted(): omobj.interrupt()
    for mcobj in locals.values():
        if isinstance(mcobj, AntModeController):
            logger.logInfo("Found one mode controller in local ns: " + str(mcobj))
            if not mcobj.isInterrupted(): mcobj.interrupt()
    if '__abort__' in locals:
        apply(locals['__abort__'])
    ResourceManager().releaseTemporaryResources()
    ResourceManager().releaseResources()
    os._exit(-1)

# install signal handler
signal.signal(signal.SIGABRT, onSignal)

try:
    result = "" # If the execution is stopped, then there's no result.
    result = executor.runSource(srcCode)
    scriptExec.setExecResult(result)
except SyntaxError, ex:
    acserr_ex = pyExceptionToCORBA(ex)
    msg = "Syntax error when executing script. "
    msg = msg + "Error in line number " + str(ex.lineno) + ", offset " + str(ex.offset)
    msg = msg + ", text = '" + ex.text + "'."
    logger.logCritical(msg)
    scexec_ex = ScriptExecutorExceptionsImpl.ExecutionErrorExImpl(exception=acserr_ex)
    scriptExec.setExecError(scexec_ex.errorTrace)
except (CCLExceptionsEx, DeviceErrorEx, NotYetImplementedEx, CCLErrorEx), ex:
    scriptExec.setExecError(ex.errorTrace)
except:
    (ex_type, ex_value, ex_tb) = sys.exc_info()
    msg = "Unexpected exception when executing script: "
    if isinstance(ex_value, CORBA.UserException) and hasattr(ex_value, 'errorTrace'):
        logger.logDebug('Exception is an ACS Error exception: ' + str(ex_value))
        acserr_ex = ex_value
    else:
        logger.logDebug('Exception is a Python exception: ' + str(ex_value))
        acserr_ex = pyExceptionToCORBA(ex_value)
    msg = msg + str(acserr_ex)
    logger.logCritical(msg)
    logger.logDebug('Wrapping exception in an ExecutionErrorEx')
    try:
        # Sometimes this fails.
        scexec_ex = ScriptExecutorExceptionsImpl.ExecutionErrorExImpl(exception=acserr_ex)
    except:
        scexec_ex = ScriptExecutorExceptionsImpl.ExecutionErrorExImpl()
    logger.logDebug('Setting error in ScriptExecutor')
    scriptExec.setExecError(scexec_ex.errorTrace)
    
ResourceManager().releaseTemporaryResources()
ResourceManager().releaseResources()
ResourceManager().getContainerServices().disconnect()

logger.logInfo("SB executor process finished.")
os._exit(0)

#
# __oOo__
