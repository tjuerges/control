#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#

import sys
from optparse import OptionParser
import ACSLog

parser = OptionParser()
parser.add_option("-y", "--yNotUseThisForTheScript", dest="script",
                  help = "Name of the Standard Observing Script to Execute")
parser.add_option("-x", "--xml", dest = "schedBlockName",
                  help = "Filename of Scheduling Block (XML file) to Execute")
parser.add_option("-s", "--subscan", dest = "subscan", default = False,
                  action="store_true",
                  help="Include subscan details in the report")
parser.add_option("-f", "--full", dest = "fullReport", default = False,
                  action="store_true",
                  help="Generate a complete report with all possible details")
parser.add_option("-p", "--pointing", dest = "reportPointing", default = False,
                  action="store_true",
                  help="Include detailed information about the pointing")
parser.add_option("-d", "--delay", dest = "reportDelay", default = False,
                  action="store_true",
                  help="Include detailed information about the delay center")
parser.add_option("-b", "--baseband", dest = "reportBaseband", default = False,
                  action="store_true",
                  help="Include detailed information baseband frequencies")
parser.add_option("-o", "--outputFile", dest = "outputFile",
                  help="Output file to save output to, if none is specified"+\
                  " results are dumped to the terminal")

(options, args) = parser.parse_args()


if options.script is None:
    print "You must specify a script on the command line, use '-h' for help"
    sys.exit()
if options.schedBlockName is None:
    print "You must specify a scheduling block filename on the command line,"\
          " use '-h' for help"
    sys.exit()

from ObservingModeSimulation.ObservingModeSimulator \
     import ObservingModeSimulator


simulator = ObservingModeSimulator()
simulator.setScript(options.script)
simulator.setSchedBlock(options.schedBlockName)
simulator.execute()

simulator._array.reportSubscans(options.subscan)
simulator._array.reportPointingOffset(options.reportPointing)
simulator._array.reportDelayCenter(options.reportDelay)
simulator._array.reportBasebandFreq(options.reportBaseband)

if options.fullReport:
    simulator._array.reportSubscans(True)
    simulator._array.reportPointingOffset(True)
    simulator._array.reportDelayCenter(True)
    simulator._array.reportBasebandFreq(True)

# Configure the output of the simulation
reportLines = simulator._array.generateReport()

if options.outputFile is not None:
    f = open(options.outputFile,'w')
    for line in reportLines:
        f.write(line+'\n')
    f.close()
else:
    for line in reportLines:
        print line
