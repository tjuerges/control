#!/usr/bin/env python

import sys
from optparse import OptionParser

parser = OptionParser()
parser.add_option("-s", "--script", dest="script",
                  help = "Name of the Standard Observing Script to Execute")
parser.add_option("-f", "--file", dest = "schedBlockName",
                  help = "Filename of Scheduling Block to Execute")
parser.add_option("-a", "--arrayname", dest = "array",
                  help = "Name of the manual mode array to use")
(options, args) = parser.parse_args()


if options.script is None:
    print "You must specify a script on the command line, use '-h' for help"
    sys.exit()
if options.schedBlockName is None:
    print "You must specify a scheduling block filename on the command line,"\
          " use '-h' for help"
    sys.exit()
if options.array is None:
    print "You must specify the name of a manual mode array, use '-h' for help"
    sys.exit()

from AcsutilPy.FindFile import findFile

import CCL.Global

CCL.Global.setArrayName(options.array)
CCL.Global.loadSB(options.schedBlockName)

scriptName = findFile(options.script)[0]
if scriptName == '':
    scriptName = options.script

print "Executing Script on %s" % options.array
print "\tScript is: %s" % scriptName
print "\tSchedBlock is: %s" % options.schedBlockName

try:
    execfile(scriptName)
except Exception, e:
    # Something bad happened end execution
    array = getArray()
    import Control
    array.endExecution(Control.FAIL, "Exception detected")
    raise e
