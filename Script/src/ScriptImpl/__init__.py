"""
This module describes the Control Command Language (CCL) for the
ALMA project. The CCL is a high-level scripting language that is
used within the Control subsystem to control the ALMA telescope.
It has two functions: to serve as the language in which standard
"observing scripts" are written and to serve as a suite of interactive
commands to be used by hardware engineers, testing or debugging
equipment, or staff astronomers, developing new observing procedures.
The language itself can be used in two modes, reflecting the
purposes just stated.

A scheduling block to be executed by the Control subsystem will contain
a standard script or a so-called "expert" script. There will be a special
flag in the scheduling block that designates whether a script is an
"expert" or standard script. These scripts are written in the CCL
and executed by Control. These scripts represent the first mode of
usage of the CCL. This is a restricted mode; high-level commands are
restricted to the array-level. Access to the antenna level and to
components within an antenna is not permitted in this mode.

The observing script written in the CCL is embedded in a scheduling block
in the form of a string. This string is extracted by the Control subsystem
and executed. At runtime, this script will have access to other portions
of the scheduling block, such as its list of sources to observe or
correlator configurations. Another source of data that this script will be
able to access is certain real-time measurements, such as data representing
weather conditions.

In order to get access to these global context objects without
having to introduce arbitrary names, a number of global functions are
provided. See for CCLGlobal.getArray() and CCLGlobal.getSB().

Arrays can also be operated in manual mode. This mode is not used for
science observing. In this mode the user has access to and can command all defined
objects in the hierarchy, including antennas and components within antennas.
This mode is intended to be a special development and debugging mode and is
not intended for the casual user. In manual mode, a member of the ALMA
staff is in charge of the antennas in the manual array. Commands may be entered
in the form of scripts or they may be entered interactively. It is expected
that only qualified staff astronomers and engineering staff will use this
mode, primarily for developing standard observing modes and diagnosing
equipment problems.

When operating in manual mode, the CCL provides a way to get
direct access to the CORBA stubs for the hardware device
drivers. See CCLObject.CCLObject.controllers() and
CCLObject.CCLObject.getController().

LANGUAGE DETAILS

The basis for the CCL is Python. Since the language is object oriented,
every command executed will need to be provided by an object in the
environment (but for a few global methods for bootstraping purposes).
At this stage all commands are made available through an instance of the
CCLArray class, which gives access from the CCL to the
Control subsystem Array component. An Array component
(actually one of its subclasses AutomaticArray or ManualArray) is
created whenever the Control's Master component groups a set
of antennas together to be commanded as a unit, usually at the
request of the Scheduling subsystem. When a scheduling block is
submitted for execution, it is bound to a Python object
in the execution environment. In order to accomplish
the goals of the high level commands, it is necessary to issue
many lower level commands. These commands are implemented by other
objects which are not accessible to "standard" observing scripts.

Also because the base language for the CCL is Python, it is possible
to use the extensive collection of libraries included in the Python
distribution when writing CCL scripts. For example, it is posible to
use the thread or threading libraries to parallelize sections of
the script.

LANGUAGE STRUCTURE

The CCL is designed to follow a hierarchical structure. This hierarchy is
expressed in the objects that are exposed to script writers.

At the highest levels of the language are the more abstract objects which
encapsulate several different lower level components of the ALMA system.
For instance, the highest level object exposed to script writers in the CCL
is the CCLArray object. This object represents a collection
of antennas that accept commands as a single unit. It also encapsulates
many aspects of the Correlator subsystem. It is expected that the
mayority of scripts will be written by taking advantage of the
CCLArray and the ancillary objects supporting it.

The next lower level of the CCL objects is represented by the CCLAntenna
object. This single object represents everything needed to produce a working
antenna object in the real world. It includes the antenna's mount, the LO's,
FTS, Baseband Processor, etc. Through the CCLAntenna object, commands may be 
passed to any of these components.

Below the level of the CCLAntenna object, users may get access to components
that make up an antenna structure. In the current implementation, only the
Receiver object is accessible at this level, but others will be exposed as the
CCL develops.

One artifact of this hierarchical structure is that there may appear to
be redundant commands, when the CCL is considered in its entirity. For
instance, there is a setDirection command both in the CCLAntenna and 
CCLArray objects. Both accomplish the same end result, although in the 
former case, it affects only a single antenna, while in the latter, all 
antennas in an array are directed to a new pointing direction. This redundancy 
has been intentionally designed in the language in order to simplify the
transition between the higher level objects and the lower level
objects.

LANGUAGE OBJECTS

There are two different types of objects in the CCL: "native" CCL objects, 
which are a collection of Python classes necessary to implement the CCL language;
and "imported" CCL structures, which are created by other components
in the Control and Correlator subsystems and are introduced into the
CCL execution context before the script execution begins. These structures
doesn't have methods, but just data attributes, which are filled from the
information contained in the scheduling block.

The imported CCL structures are defined in IDL, and the IDL compiler
transform them to Python objects following the CORBA IDL to Python
mappings. In terms of the CCL language, there is not really a big
difference between native and imported objects: IDL structures are mapped
as normal Python classes (without methods); IDL sequences are normal
Python lists; and other IDL definitions such as enumeration members can be
regarded as opaque objects.
"""