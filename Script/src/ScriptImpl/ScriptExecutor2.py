#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# rhiriart  2006-12-06  created
#

from Acspy.Common.Err import ACSError
from Acspy.Common.Err import pyExceptionToCORBA
from Acspy.Servants.ACSComponent       import ACSComponent
from Acspy.Servants.ComponentLifecycle import ComponentLifecycle
from Acspy.Servants.ContainerServices  import ContainerServices
from ScriptExec.configurator import Configurator
from ScriptExec.executor import Executor
from ScriptExec.rm import ResourceManager
from ScriptExec.rm import get_component
from CCLExceptions import CCLExceptionsEx, DeviceErrorEx, NotYetImplementedEx, CCLErrorEx
import ACSErr
import Control
import Control__POA
import ScriptExec.util
import ScriptExecutorExceptionsImpl
import ScriptExecutorExceptions
import sys
import thread
import CORBA
import os, time, signal, pickle

if not 'ScriptExecutor2' in dir(Control__POA):
    reload(Control__POA)

class ScriptExecutor2(Control__POA.ScriptExecutor2,
                      ACSComponent,
                      ContainerServices,
                      ComponentLifecycle):
    """ScriptExecutor component implementation.
    """
    
    #------------------------------------------------------------------------------
    #--Constructor-----------------------------------------------------------------
    #------------------------------------------------------------------------------
    def __init__(self):
        '''Constructor.
        '''
        ACSComponent.__init__(self)
        ContainerServices.__init__(self)

        self.logger = self.getLogger()

        self.name = self._get_name()

        # Configuration
        self.conf = None
        
        # ResourceManager
        self.rm = None
        
        # Executor
        self.executor = None
        
        # Error ocurred in the last execution. In case of no errors this
        # is an empty string.
        self.error = None
        
        # The result of an SB execution. This is whatever it is printed to stdout
        # or stderr during the execution.
        self.result = ""
        
        # The PID of the SB execution process. If there's no currently running
        # process, then its value is < 0.
        self.newpid = -1
        
    #------------------------------------------------------------------------------
    #--Override ComponentLifecycle methods-----------------------------------------
    #------------------------------------------------------------------------------
    def initialize(self):
        '''
        Override this method inherited from ComponentLifecycle
        '''
        self.getLogger().logInfo("called...")
        self.name = self._get_name()
        
    #------------------------------------------------------------------------------
    def cleanUp(self):
        '''
        Override this method inherited from ComponentLifecycle
        '''
        self.getLogger().logInfo("called...")
        if self.rm:
            self.getLogger().logDebug("Freeing resources in ResourceManager.")
            self.rm.release_temp_resources()
            self.rm.release_resources()
        if not self.executor is None:
            self.getLogger().logDebug("Cleaning executor context local namespace.")
            self.executor.getContext().clean_locals()
            
    #------------------------------------------------------------------------------
    def getName(self):
        return self.name
    
    #------------------------------------------------------------------------------
    #--Implementation of IDL methods-----------------------------------------------
    #------------------------------------------------------------------------------
    def setParent(self, arrayName):
        """This seems to be deprecated. The array parent is setup in configure().
        TODO: If this is the case eliminate the function from here and from the
              IDL.
        """
        pass
    
    #------------------------------------------------------------------------------
    def configure(self, conf):
        """Configures the ScriptExecutor component, setting its execution
        context ready to run CCL scripts.
        TODO: the SB should be incorporated as a parameter.
        
        This function will load the components needed to bootstrap the
        execution environment of the CCL (currently the Array and the Antennas.)
        These components will be released in the component cleanUp() method.
        
        Parameters:
          conf - Control.ScriptExecConf IDL structure (defined in 
                 CONTROL/Script/ScriptExecutor.idl)
                 
        Raises:
          ScriptExecutorExceptions.ResourceErrorEx
        """
        
        self.conf = Configurator("Runtime ScriptExecutor Configuration")
        self.conf.populate_from_IDL(conf)
        self.logger.logDebug("Configuring ScriptExecutor with Configurator:\n" +
                             str(self.conf) + "\n")

        # This design -- elegant as it is -- doesn't work because of that bug
        # in the ACS Python implementation: problems finding and loading the
        # component stubs.
        #   self.rm = ResourceManager(self, self.conf, self.logger)
        #   self.rm.acquire_resources()
        # Instead, I implemented the following workaround:
        self.rm = ResourceManager(self, self.conf, self.logger)
        comps_info = self.rm.get_startup_components_info()
        self.logger.logInfo(str(comps_info))
        for c in comps_info:
            if "Array" in c[0]:
                reload(Control)
                reload(Control__POA)
            else:
                reload(Control) # the stubs bug again
                reload(Control__POA) # the stubs bug again
            # Function self.getComponent() could be used, but we should
            # be prepared to handle of the exceptions thrown by this function.
            # Instead, I use my own function, that does the error handling.
            comp = get_component(self, c[1])
            self.rm.add_resource(c[0], comp)
            
        self.executor = Executor(self.conf, self, self.logger)

    #------------------------------------------------------------------------------
    def setSB(self, schedBlk):
        """I think this became outdated. It will dissapear when I'm sure.
        """
        pass
    
    #------------------------------------------------------------------------------
    def setXMLSB(self, sched_blk_xml):
        """Set the Scheduling Block in the execution context.
        
        Parameters:
          sched_blk_xml - String containing the SchedulingBlock XML document.
        """
        self.logger.logTrace("called... sched_blk_xml='" + sched_blk_xml + "'")
        self.executor.getContext().set_scheduling_block(sched_blk_xml)
    
    #------------------------------------------------------------------------------
    def runSource(self, src_code):
        """Run a Python script. This is meant to be used by the ScriptExecutor
        component running script in automatic mode.
        
        Parameters:
          source - String. The Python script. (Actually, it could be a code object
                   or a file object as well.)
                   
        Returns:
           (compl, result) - A tuple containing an ACS Error Completion object
                             ScriptExecutorExceptions.ExecutionEndCompletion and
                             a string containing whatever went to STDOUT during the
                             execution of the script.
                   
        Raises:
           - ScriptExecutorExceptions.ExecutionErrorEx
           - CCLExceptions.CCLExceptionsEx
           - CCLExceptions.DeviceErrorEx
           - CCLExceptions.NotYetImplemmentedEx
        """        
        self.logger.logTrace("called... src_code='" + src_code + "'")
        try:
            self.logger.logInfo("Executing script:\n" + ScriptExec.util.print_code(src_code))
            result =  self.executor.runSource(src_code)
        except SyntaxError, ex:
            acserr_ex = pyExceptionToCORBA(ex)
            # My policy to handle errors:
            # 1) Log as much information as possible in order to help the log
            #    reader to solve the problem. In this sense, the more information
            #    the better. There is sometimes a concern with flooding error
            #    channels with duplicated information, but this is hardly the case
            #    here.
            msg = "Syntax error when executing script. "
            msg = msg + "Error in line number " + str(ex.lineno) + ", offset " + str(ex.offset)
            msg = msg + ", text = '" + ex.text + "'."
            self.logger.logCritical(msg)
            # 2) Log the received exception. If it is an ACS Error exception, this
            #    will log the error trace. If it is a native Python exception, be
            #    sure to override the __str__ or __repr__ function with a meaningful
            #    message. 
            # TODO: the log function doesn't seem to work reliable. (again).
            # acserr_ex.log()
            self.logger.logDebug(acserr_ex.errorTraceToString(acserr_ex.getErrorTrace(), ""))
            # 3) If it is possible to recover from this error, do your best. If not,
            #    consider wrapping up the exception in a higher level exception or
            #    otherwise re-raise the received exception.
            scexec_ex = ScriptExecutorExceptionsImpl.ExecutionErrorExImpl(exception=acserr_ex)
            self.rm.release_temp_resources()
            raise scexec_ex
        except (CCLExceptionsEx, DeviceErrorEx, NotYetImplementedEx, CCLErrorEx), ex:
            # In this case the received exception is already in right level of
            # abstraction, so it is simply re-raised.
            self.rm.release_temp_resources()
            raise ex
        except:
            # Consider general "catch-alls", but be sure to address the problem
            # or re-raise the received exception.
            # This is useful in Python and Java because functions can throw
            # unspecified exceptions. It is not the case for C++.
            # What is important here is to avoid loosing information that could
            # help the caller understand what went wrong.
            # In this case, as this is a component implementation function, we 
            # make sure that the execution was successful or an ExecutionException
            # is thrown. If we receive an ACS Error exception, it
            # is rewrapped in the declared exception and re-thrown. If a native 
            # Python exception is received, it is converted to an ACS Error exception,
            # wrapped and rethrown. 
            (ex_type, ex_value, ex_tb) = sys.exc_info()
            msg = "Unexpected exception when executing script: "
            if isinstance(ex_value, CORBA.UserException) and hasattr(ex_value, 'errorTrace'):
                acserr_ex = ex_value
            else:
                acserr_ex = pyExceptionToCORBA(ex_value)
            msg = msg + str(acserr_ex)
            self.logger.logCritical(msg)
            # acserr_ex.log()
            self.logger.logDebug(acserr_ex.errorTraceToString(acserr_ex.getErrorTrace(), ""))
            scexec_ex = ScriptExecutorExceptionsImpl.ExecutionErrorExImpl(exception=acserr_ex)
            self.rm.release_temp_resources()
            raise scexec_ex
        
        self.rm.release_temp_resources()

        # self.logger.logDebug("Cleaning execution context.")       
        # self.executor.getContext().clean_locals()
        
        # Create a successful completion.
        # Using the ExecutionEndCompletion just to get the error type
        # and code information.
        execEndCompl = ScriptExecutorExceptionsImpl.ExecutionEndCompletionImpl()

        compl = ACSErr.Completion(execEndCompl.timeStamp,
                                  execEndCompl.getErrorType(),
                                  execEndCompl.getErrorCode(),
                                  [])
        self.logger.logInfo("Execution successful, results:\n" + ScriptExec.util.print_code(result))
        self.logger.logTrace("exiting... result = '" + result.strip() + "'")
        return compl, result

    #------------------------------------------------------------------------------
    def runSource2(self, src_code):
        
        self.logger.logInfo("called...")
        
        self.error = None
        self.result = ""
        
        stdin_fd = sys.stdin.fileno()

        child_in, parent_out = os.pipe() # pipe() -> (read_end, write_end)
        
        self.newpid = os.fork()
        
        if self.newpid == 0: #child
            os.close(parent_out)
            os.dup2(child_in, stdin_fd)
            os.execlp('SBExecutor.py', 'SBExecutor.py') # overlay process

        else: #parent

            os.close(child_in)

            fout = os.fdopen(parent_out, 'w')

            pickle.dump(self.conf, fout)
            fout.flush()

            pickle.dump(self._get_name(), fout)
            fout.flush()

            pickle.dump(src_code, fout)
            fout.flush()

            sb = self.executor.getContext().getSB()
            pickle.dump(sb.toxml(), fout)
            fout.flush()

            pid, status = os.wait()
            self.logger.logInfo("Child completed %d %d" % (pid, status))
            self.newpid = -1
            
            if status == -1: self.result = "Process aborted"
            
            self.logger.logDebug("Error is " + str(self.error))
            if not self.error is None:
                scexec_ex = ScriptExecutorExceptions.ExecutionErrorEx(self.error)
                raise scexec_ex


        self.logger.logInfo("Execution successful, results:\n" + ScriptExec.util.print_code(self.result))
        return self.result

    #------------------------------------------------------------------------------
    def runSourceAsync(self, script, callback):
        thread.start_new_thread(self.__spawnSBExecProcess, (script, callback))

    #------------------------------------------------------------------------------
    def __spawnSBExecProcess(self, script, callback):
        self.error = None
        
        stdin_fd = sys.stdin.fileno()

        child_in, parent_out = os.pipe() # pipe() -> (read_end, write_end)
        
        self.newpid = os.fork()
        
        if self.newpid == 0: #child
            os.close(parent_out)
            os.dup2(child_in, stdin_fd)
            os.execlp('SBExecutor.py', 'SBExecutor.py') # overlay process

        else: #parent

            os.close(child_in)

            fout = os.fdopen(parent_out, 'w')

            pickle.dump(self.conf, fout)
            fout.flush()

            pickle.dump(self._get_name(), fout)
            fout.flush()

            pickle.dump(script, fout)
            fout.flush()

            sb = self.executor.getContext().getSB()
            pickle.dump(sb.toxml(), fout)
            fout.flush()

            pid, status = os.wait()
            self.logger.logInfo("Child completed %d %d" % (pid, status))
            self.newpid = -1
            
            # if status == -1: self.result = "Process aborted"
            # Probably express the abortion of the process in the error trace.
            
            self.logger.logDebug("Error is " + str(self.error))
            errorList = []
            if not self.error is None:
                scexec_ex = ScriptExecutorExceptions.ExecutionErrorEx(self.error.errorTrace)
                errorList.append(scexec_ex)

            execEndCompl = ScriptExecutorExceptionsImpl.ExecutionEndCompletionImpl()
            # Is there a function to get the Completion (or a child of Completion)
            # from the CompletionImpl?
            compl = ACSErr.Completion(execEndCompl.timeStamp,
                                      execEndCompl.getErrorType(),
                                      execEndCompl.getErrorCode(),
                                      errorList)
            self.logger.logDebug("Completion is " + str(compl))
            callback.report(compl)

    #------------------------------------------------------------------------------
    def setExecError(self, error):
        self.logger.logDebug("Setting error: " + str(error))
        self.error = error

    #------------------------------------------------------------------------------
    def setExecResult(self, result):
        self.result = result

    #------------------------------------------------------------------------------
    def abort(self):
        if self.newpid > 0:
            self.logger.logInfo('Aborting execution')
            os.kill(self.newpid, signal.SIGABRT)
        else:
            self.logger.logDebug('User tried to abort SB execution, but there is no process running')
                        
    #------------------------------------------------------------------------------
    def compileScript(self, src_code):
        '''
        Compiles a chunck of code. This function, along with executeScript(), is 
        meant to be used in manual mode.

        The ScriptExecutor in manual mode works with consoles created with
        code.InteractiveConsole to run Python statements interactively. These
        Python statement will be compiled and executed in the process running the
        ScriptExecutor component.
        
        See the Python code library and the mmc Python utility (in this module) for details
        about the protocol followed by interactive consoles. In summary compileScript is
        called until it returns false, which indicates that a complete Python sentence
        has been compiled and a code object produced. executeScript() is called to execute
        the code object afterwards.  
        
        Parameters:
          source_str - String. The Python script. (Actually, it could be a code object
                       or a file object as well.)
        
        Returns:
          True  - If the statement passed in the code_str parameter is incomplete.
          False - ~ 
        
        Raises:
           - ScriptExecutorExceptions.ExecutionErrorEx
           - ScriptExecutorExceptions.CompilationErrorEx
        '''
        self.logger.logInfo("called... src_code = '" + src_code + "'")
        try:
            needs_more = self.executor.compileScript(src_code)
            self.logger.logTrace("exiting... needs_more = " + str(needs_more))
            return needs_more
        except SyntaxError, ex:
            acserr_ex = pyExceptionToCORBA(ex)
            msg = "Syntax error when compiling Python code. "
            msg = msg + "Error in line number " + str(ex.lineno) + ", offset " + str(ex.offset)
            msg = msg + ", text = '" + ex.text + "'."
            self.logger.logCritical(msg)
            self.logger.logDebug(acserr_ex.errorTraceToString(acserr_ex.getErrorTrace(), ""))
            scexec_ex = ScriptExecutorExceptionsImpl.SyntaxErrorExImpl(exception=acserr_ex)
            raise scexec_ex    
        except:
            (ex_type, ex_value, ex_tb) = sys.exc_info()
            msg = "Unexpected exception when compiling Python code: "
            if isinstance(ex_value, CORBA.UserException) and hasttr(ex_value, 'errorTrace'):
                acserr_ex = ex_value
            else:
                acserr_ex = pyExceptionToCORBA(ex_value)
            msg = msg + str(acserr_ex)
            self.logger.logCritical(msg)
            # acserr_ex.log()
            self.logger.logDebug(acserr_ex.errorTraceToString(acserr_ex.getErrorTrace(), ""))
            scexec_ex = ScriptExecutorExceptionsImpl.CompilationErrorExImpl(exception=acserr_ex)
            raise scexec_ex

    #------------------------------------------------------------------------------
    def executeScript(self, dummy):
        '''
        Executes a Code object previously produced with a call to compileScript().
        
        See the documentation for compileScript() for more complete documentation
        about this function.
        
        Parameters:
          dummy - deprecated, it will be removed soon.
        
        Raises:
           - ScriptExecutorExceptions.ExecutionErrorEx
           - CCLExceptions.CCLExceptionsEx
           - CCLExceptions.DeviceErrorEx
           - CCLExceptions.NotYetImplemmentedEx
        '''        
        self.logger.logInfo('Executing code')
        try:        
            result = self.executor.executeScript(dummy)
            self.logger.logInfo("exiting... result = '" + result + "'")
            return result
        except (CCLExceptionsEx, DeviceErrorEx, NotYetImplementedEx, CCLErrorEx), ex:
            raise ex
        except:
            (ex_type, ex_value, ex_tb) = sys.exc_info()
            msg = "Unexpected exception when executing Python code: "
            if isinstance(ex_value, CORBA.UserException) and hasttr(ex_value, 'errorTrace'):
                acserr_ex = ex_value
            else:
                acserr_ex = pyExceptionToCORBA(ex_value)
            msg = msg + str(acserr_ex)
            self.logger.logCritical(msg)
            # acserr_ex.log()
            self.logger.logDebug(acserr_ex.errorTraceToString(acserr_ex.getErrorTrace(), ""))
            scexec_ex = ScriptExecutorExceptionsImpl.ExecutionErrorExImpl(exception=acserr_ex)
            raise scexec_ex        

#----------------------------------------------------------------------------------
#--Main defined only for generic testing-------------------------------------------
#----------------------------------------------------------------------------------
if __name__ == "__main__":
    print "Creating an object"
    s = ScriptExecutor2()
    print "Done..."

#
# ___oOo___
