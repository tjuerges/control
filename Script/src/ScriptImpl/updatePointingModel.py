#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# rhiriart  2006-01-31  created
#

#************************************************************************
#   NAME
# 
#   SYNOPSIS
# 
#   DESCRIPTION
#
#   FILES
#
#   ENVIRONMENT
#
#   RETURN VALUES
#
#   CAUTIONS
#
#   EXAMPLES
#
#   SEE ALSO
#
#   BUGS     
#
#------------------------------------------------------------------------
#

import sys, getopt
import ScriptExecutorExceptions
import ScriptExecutorExceptionsImpl
import Control
from Acspy.Clients.SimpleClient import PySimpleClient
from xml.sax import make_parser
from xml.sax.saxutils import prepare_input_source
from xml.sax.handler import ContentHandler
from xml.sax import parseString

#------------------------------------------------------------------------
class ASDMHandler(ContentHandler):
    """
    A handler to get the CalPointingModel table UID from the
    ASDM container.
    """
    def __init__(self):
        self.calPointingModelUID = None
        self.calDataUID = None
        self.calReductionUID = None
    
    def startElement(self, name, attrs):
        if name == 'Entity':
            attnames = attrs.getNames()
            entType = None
            entUID = None
            if 'entityTypeName' in attnames:
                entType = attrs.getValue('entityTypeName')
            if 'entityId' in attnames:
                entUID = attrs.getValue('entityId')
                
            if entType == "CalPointingModelTable":
                # print "CalPointingModelTable UID = " + entUID
                self.calPointingModelUID = entUID
            elif entType == "CalDataTable":
                # print "CalDataTable UID = " + entUID
                self.calDataUID = entUID
            elif entType == "CalReductionTable":
                # print "CalReductionTable UID = " + entUID
                self.calReductionUID = entUID

    def characters(self, characters): pass

    def endElement(self, name): pass

#------------------------------------------------------------------------
class CalPointingModelHandler(ContentHandler):
    """
    A handler to get the pointing model coeficcients from the
    CalPointingModel table.
    """
    def __init__(self):
        self.inCoeffVal = False
        self.coeffValBuff = ""
        
    def startElement(self, name, attrs):
        if name == 'coeffVal':
            self.inCoeffVal = True

    def characters(self, characters):
        if self.inCoeffVal:
            self.coeffValBuff = self.coeffValBuff + characters

    def endElement(self, name):
        if name == 'coeffVal':
            self.inCoeffVal = False

    def getCoeffValues(self):
        return self.coeffValBuff.split(" ")
    

#------------------------------------------------------------------------
def usage():
    print """
updatePointingModel

Update the Antenna pointing model.

Usage: 
   updatePointingModel ANTENNA1 [ANTENNA2 ...] ASDM-UID
"""

## \defgroup utilities Utilities
#

## Update the Antenna pointing model.
#
# This function will update the pointing model in the Antennas with
# the coefficients found from in the ASDM CalPointingModelTable.
# This function performs the following steps:
#    -# Get the ASDM from the Archive using the \a asdmUID argument.
#    -# Extract the UID for the CalPointingModelTable from the ASDM.
#    -# Get the CalPointingModelTable from the Archive.
#    -# Extract the pointing model coefficients from the \c coeffVal XML element.
#    -# Get a reference to the Antenna devices.
#    -# Get a reference to the Antenna MountController devices.
#    -# Call the MountController \c setPointingModelCoefficients() function.
#
# \param antennaList List of names of the antennas to update, e.g., 'ALMA001',
# 'ALMA002', etc.
# \param asdmUID ASDM UID.
# \ingroup utilities
# \warning The ASDM pointing model coefficients and the MountController pointing
# model coefficients do not map one to one.
# \todo Revise the ASDM and MountController pointing models.
def updatePointingModel(antennaList, asdmUID):
    
    client = PySimpleClient()

    archConn = client.getComponent('ARCHIVE_CONNECTION')
    archOp = archConn.getOperational('updatePointingModel')
    archId = client.getComponent('ARCHIVE_IDENTIFIER')

    asdmEnt = archOp.retrieveDirty(asdmUID)
    asdmXML = asdmEnt.xmlString

    asdmHandler = ASDMHandler()
    parseString(asdmXML, asdmHandler)

    if asdmHandler.calPointingModelUID != None:
        pntModelUID = asdmHandler.calPointingModelUID
        print "This is the UID: " + pntModelUID
    else:
        print "No CalPointingModelTable in ASDM!"
        return

    pntModelEnt = archOp.retrieveDirty(str(pntModelUID))
    pntModelXML = pntModelEnt.xmlString

    pntModelHandler = CalPointingModelHandler()
    parseString(pntModelXML, pntModelHandler)
    
    coeff = pntModelHandler.getCoeffValues()

    # get the coefficients.
    # It should be better to construct a map with the coeffName's as
    # labels. Include also the coeff errors. TODO.
    ia = float(coeff[2])
    ie = float(coeff[3])
    hasa2 = float(coeff[4])
    haca2 = float(coeff[5])
    hece = float(coeff[6])
    hesa2 = float(coeff[7])
    heca2 = float(coeff[8])
    npae = float(coeff[9])
    hvsa2 = float(coeff[10])
    hvca2 = float(coeff[11])
    ca = float(coeff[12])
    an = float(coeff[13])
    aw = float(coeff[14])

    # Get a reference to the Antenna device
    antennaCURL = 'CONTROL/' + antennaList[0]
    antennaDev = client.getComponent(antennaCURL)

    mountControllerCURL = ''
    devList = antennaDev.getSubDeviceList()
    for dev in devList:
        if dev.ReferenceName == 'MountController':
            mountControllerCURL = dev.FullName
    if len(mountControllerCURL) == 0:
        print 'MountController sub-device not found in ' + antennaCURL + ' Antenna!'
        return

    mountControllerDev = client.getComponent(mountControllerCURL)

    pntModelParams = Control.PointingModelParameters(1,   # use software model?
                                                     0,   # band?
                                                     ia,
                                                     ie,
                                                     0.0, # hasa?
                                                     0.0, # haca?
                                                     0.0, # hese?
                                                     hece,
                                                     0.0, # hesa?
                                                     hasa2,
                                                     haca2,
                                                     hesa2,
                                                     heca2,
                                                     0.0, # haca3?
                                                     0.0, # heca3?
                                                     0.0, # hesa3?
                                                     npae,
                                                     ca,
                                                     an,
                                                     aw)

    mountControllerDev.setPointingModelCoefficients(pntModelParams)

    # cleanup
    client.releaseComponent(antennaCURL)
    client.releaseComponent(mountControllerCURL)
    
    client.releaseComponent('ARCHIVE_IDENTIFIER')
    client.releaseComponent('ARCHIVE_CONNECTION')
    client.disconnect()

if __name__ == "__main__":
    # No options right now, may be in the future.
    try:
        opts, args = getopt.getopt(sys.argv[1:], "", [])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    if len(args) < 2:
        print "Either the antenna list or the ASDM UID is missing."
        usage()
        sys.exit(2)
    
    asdmUID = args[-1:]
    antennas = []
    for a in args[:-1]:
        antennas.append(a)
        
    updatePointingModel(antennas, asdmUID)

#
# ___oOo___
