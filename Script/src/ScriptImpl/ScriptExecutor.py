#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#

from Acspy.Common.Err import pyExceptionToCORBA
from Acspy.Common.ErrorTrace import ErrorTraceHelper
from Acspy.Servants.ACSComponent       import ACSComponent
from Acspy.Servants.ComponentLifecycle import ComponentLifecycle
from Acspy.Servants.ContainerServices  import ContainerServices
from ScriptExec.Executor import Executor
import ACSErr
import ACSLog
import Control__POA
import ScriptExec.util
import ScriptExecutorExceptionsImpl
import ScriptExecutorExceptions
import log_audience
import sys, thread, CORBA
import os, time, signal, pickle
import ScriptExec.ResourceManager


class ScriptExecutor(Control__POA.ScriptExecutor,
                     ACSComponent,
                     ContainerServices,
                     ComponentLifecycle):
    """ScriptExecutor component implementation.
    """
    
    def __init__(self):

        ACSComponent.__init__(self)
        ContainerServices.__init__(self)
        self.logger = self.getLogger()
        self.name = self._get_name()

        # ResourceManager
        self.resMng = None
        
        # Executor
        self.executor = None
        
        # Error ocurred in the last execution.
        self.error = None
        
        # The result of an SB execution. This is whatever it is printed to stdout
        # or stderr during the execution.
        self.result = ""
        
        # The PID of the SB execution process. If there's no currently running
        # process, then its value is < 0.
        self.newpid = -1

        # Execution process status
        self.status = 0

        # Array name
        self.arrayName = ""

        # Scheduling block XML document
        self.schedBlock = ""
        
        # Currently running Script
        self.script = None
        
        self.childpid = -1
        
    #------------------------------------------------------------------------------
    #--ComponentLifecycle methods--------------------------------------------------
    #------------------------------------------------------------------------------
    
    def initialize(self):
        self.logger.logInfo("called...")
        self.name = self._get_name()

        # Create the ResourceManager singleton.
        # This will actually create a new instance.
        self.resMng = ScriptExec.ResourceManager.ResourceManager(self)

        # This will set the ResourceManager context.
        # From now on, calling ResourceManager() without parameters will return the same
        # instance created above.
        ScriptExec.ResourceManager._contextName = self.name

        # Add resources to ResourceManager and acquire them here.
        
    def cleanUp(self):
        self.logger.logTrace("called...")
        if self.resMng:
            self.logger.logDebug("Freeing resources in ResourceManager.")
            self.resMng.releaseTemporaryResources()
            self.resMng.releaseResources()            
        ScriptExec.ResourceManager.ResourceManager.cleanUp()
            
    def getName(self):
        return self.name
    
    #------------------------------------------------------------------------------
    #--Implementation of IDL methods-----------------------------------------------
    #------------------------------------------------------------------------------
    
    def configure(self, arrayName, schedBlock):
        self.arrayName = arrayName
        # self.schedBlock = schedBlock
        self.executor = Executor(self)
        self.executor.setArrayName(arrayName)
        # self.executor.setSB(schedBlock)

    def setSB(self, schedBlock):
        if self.executor is not None:
            self.schedBlock = schedBlock
            self.executor.setSB(schedBlock)

    def getArrayName(self):
        if len(self.arrayName) > 0:
            return self.arrayName
        raise ScriptExecutorExceptionsImpl.ConfigurationErrorExImpl()
    
    def getSB(self):
        if len(self.schedBlock) > 0:
            return self.schedBlock
        raise ScriptExecutorExceptionsImpl.ConfigurationErrorExImpl()

    def getObservationScript(self):
        if self.script is not None:
            return self.script
        raise ScriptExecutorExceptionsImpl.ConfigurationErrorExImpl()

    def runObservationScript(self, script):
        self.logger.logInfo("called...")
        
        self.__runSBExecProcess(script)
        
        self.logger.logDebug("Error is " + str(self.error))
        if self.error is not None:
            scexec_ex = ScriptExecutorExceptions.ExecutionErrorEx(self.error)
            raise scexec_ex

        self.logger.logInfo("Execution successful, results:\n" + ScriptExec.util.print_code(self.result))
        return self.result


    def runObservationScriptAsync(self, script, callback):
        thread.start_new_thread(self.__spawnSBExecProcess, (script, callback))

    def setExecError(self, error):
        msg = "Error in the script execution: " + str(error)
        self.logger.logCritical(msg)
        self.logger.logNotSoTypeSafe(ACSLog.ACS_LOG_CRITICAL, msg, log_audience.OPERATOR, None, None)
        self.error = error

    def setExecResult(self, result):
        self.result = result

    def setChildProcessPID(self, pid):
        self.childpid = pid

    def abort(self):
        if self.childpid > 0:
            self.logger.logInfo('aborting execution, child process PID: ' + str(self.childpid))
            os.kill(self.childpid, signal.SIGABRT)
        else:
            self.logger.logInfo('user tried to abort SB execution, but there is no process running')
                        
    def compileScript(self, src_code):
        '''
        Compiles a chunck of code. This function, along with executeScript(), is 
        meant to be used in manual mode.

        The ScriptExecutor in manual mode works with consoles created with
        code.InteractiveConsole to run Python statements interactively. These
        Python statement will be compiled and executed in the process running the
        ScriptExecutor component.
        
        See the Python code library and the mmc Python utility (in this module) for details
        about the protocol followed by interactive consoles. In summary compileScript is
        called until it returns false, which indicates that a complete Python sentence
        has been compiled and a code object produced. executeScript() is called to execute
        the code object afterwards.  
        
        Parameters:
          source_str - String. The Python script. (Actually, it could be a code object
                       or a file object as well.)
        
        Returns:
          True  - If the statement passed in the code_str parameter is incomplete.
          False - ~ 
        
        Raises:
           - ScriptExecutorExceptions.ExecutionErrorEx
           - ScriptExecutorExceptions.CompilationErrorEx
        '''
        self.logger.logInfo("called... src_code = '" + src_code + "'")
        try:
            needs_more = self.executor.compileScript(src_code)
            self.logger.logTrace("exiting... needs_more = " + str(needs_more))
            return needs_more
        except SyntaxError, ex:
            acserr_ex = pyExceptionToCORBA(ex)
            msg = "Syntax error when compiling Python code. "
            msg = msg + "Error in line number " + str(ex.lineno) + ", offset " + str(ex.offset)
            msg = msg + ", text = '" + ex.text + "'."
            self.logger.logCritical(msg)            
            hlp = ErrorTraceHelper()
            self.logger.logDebug(hlp.errorTraceToString(acserr_ex.errorTrace, ''))            
            scexec_ex = ScriptExecutorExceptionsImpl.SyntaxErrorExImpl(exception=acserr_ex)
            raise scexec_ex    
        except:
            (ex_type, ex_value, ex_tb) = sys.exc_info()
            msg = "Unexpected exception when compiling Python code: "
            if isinstance(ex_value, CORBA.UserException) and hasattr(ex_value, 'errorTrace'):
                acserr_ex = ex_value
            else:
                acserr_ex = pyExceptionToCORBA(ex_value)
            msg = msg + str(acserr_ex)
            self.logger.logCritical(msg)
            hlp = ErrorTraceHelper()
            self.logger.logDebug(hlp.errorTraceToString(acserr_ex.errorTrace, ''))            
            scexec_ex = ScriptExecutorExceptionsImpl.CompilationErrorExImpl(exception=acserr_ex)
            raise scexec_ex


    def executeScript(self):
        '''
        Executes a Code object previously produced with a call to compileScript().
        
        See the documentation for compileScript() for more complete documentation
        about this function.
        
        Parameters:
          dummy - deprecated, it will be removed soon.
        
        Raises:
           - ScriptExecutorExceptions.ExecutionErrorEx
        '''        
        self.logger.logInfo('Executing code')
        try:        
            result = self.executor.executeScript()
            self.logger.logInfo("exiting... result = '" + result + "'")
            return result
        except:
            (ex_type, ex_value, ex_tb) = sys.exc_info()
            msg = "Unexpected exception when executing Python code: "
            if isinstance(ex_value, CORBA.UserException) and hasattr(ex_value, 'errorTrace'):
                acserr_ex = ex_value
            else:
                acserr_ex = pyExceptionToCORBA(ex_value)
            msg = msg + str(acserr_ex)
            self.logger.logCritical(msg)
            hlp = ErrorTraceHelper()
            self.logger.logDebug(hlp.errorTraceToString(acserr_ex.errorTrace, ''))
            scexec_ex = ScriptExecutorExceptionsImpl.ExecutionErrorExImpl(exception=acserr_ex)
            raise scexec_ex

        
    #------------------------------------------------------------------------------
    #--Internal methods------------------------------------------------------------
    #------------------------------------------------------------------------------

    def __runSBExecProcess(self, srcCode):
        
        self.error = None
        self.result = ""
        self.script = srcCode
        
        self.newpid = os.fork()
 
        if self.newpid == 0: #child
            os.execlp('python', 'SBExecProcess.py', 'SBExecProcess.py', self.name) # overlay process

        else: #parent
            self.logger.info("Child PID for " + self.name + ": " + str(self.newpid))       
            pid, status = os.waitpid(self.newpid, 0)
            self.logger.info("Child completed in %s: %d %d" % (self.name, pid, status))
            self.newpid = -1
            self.childpid = -1
            self.status = status            
            if self.status == -1: self.result = "Process aborted"
            self.script = None

    def __spawnSBExecProcess(self, script, callback):
        self.__runSBExecProcess(script)
        
        # if status == -1: self.result = "Process aborted"
        # Probably express the abortion of the process in the error trace.
        
        self.logger.logDebug("Error is " + str(self.error))

        errorList = []
        if not self.error is None:
            errorList.append(self.error)

        execEndCompl = ScriptExecutorExceptionsImpl.ExecutionEndCompletionImpl()
            
        # It should be:
        #
        # if not self.error is None:
        #     execEndCompl = ScriptExecutorExceptionsImpl.ExecutionEndCompletionImpl(exception=self.error)
        # else:
        #     execEndCompl = ScriptExecutorExceptionsImpl.ExecutionEndCompletionImpl()
        # raise callback.report(execEndCompl)
        #
        # But there's a bug in ACS error generated classes. The previous exception is just discarded.
        #
        compl = ACSErr.Completion(execEndCompl.timeStamp,
                                  execEndCompl.getErrorType(),
                                  execEndCompl.getErrorCode(),
                                  errorList)
        self.logger.logInfo("Callback object: " + str(callback))
        self.logger.logInfo("Reporting callback for array " + self.arrayName + " from " + self.name)
        callback.report(self.arrayName, compl)
