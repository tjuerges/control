#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# rhiriart  2006-05-16  created
#

import sys, os, time, signal, pickle
import CORBA
import ScriptExecutorExceptionsImpl
import ScriptExecutorExceptions
from ScriptExec.rm import ResourceManager
from ScriptExec.executor import Executor
from CCLExceptions import CCLExceptionsEx
from CCLExceptions import DeviceErrorEx
from CCLExceptions import NotYetImplementedEx
from CCLExceptions import CCLErrorEx
from Acspy.Common.Err import ACSError
from Acspy.Common.Err import pyExceptionToCORBA
from CCL.AntModeController import AntModeController
from CCL.ObservingMode import ObservingMode

# get the Configurator from the ScriptExecutor component
conf = pickle.load(sys.stdin)

# get the ScriptExecutor component name
script_exec_name = pickle.load(sys.stdin)

rm = ResourceManager(None, conf, None)
container = rm.get_container_services()
# logger = rm.get_logger() # Given how the ResourceManager was created, this will return a
#                          # LoggerAdaptor instance. 
# logger.logger.name = 'SBExecutor'
logger = container.getLogger()
rm.acquire_resources()

logger.logInfo("Start SB executor process.")

logger.logInfo("Getting ScriptExecutor component '" + script_exec_name + "'")
script_exec = container.getComponent(script_exec_name)
rm.add_resource(script_exec_name, script_exec, True)
executor = Executor(conf, container, logger)

# get the source code to execute from the ScriptExecutor component
src_code = pickle.load(sys.stdin)

# get the SB
sched_block = pickle.load(sys.stdin)
executor.getContext().set_scheduling_block(sched_block)

def onSignal(signum, stackframe):
    logger.logInfo("Inside signal handler")
    context = executor.getContext()
    locals = context.get_locals()
    logger.logDebug("---> Locals: " + str(locals))
    for omobj in locals.values():
        if isinstance(omobj, ObservingMode):
            logger.logInfo("Found one observing mode in local ns: " + str(omobj))
            if not omobj.isInterrupted(): omobj.interrupt()
    for mcobj in locals.values():
        if isinstance(mcobj, AntModeController):
            logger.logInfo("Found one mode controller in local ns: " + str(mcobj))
            if not mcobj.isInterrupted(): mcobj.interrupt()
    if '__abort__' in locals:
        apply(locals['__abort__'])
    rm = ResourceManager()
    rm.release_temp_resources()
    rm.release_resources()
    os._exit(-1)

# install signal handler
signal.signal(signal.SIGABRT, onSignal)

try:
    result = "" # If the execution is stopped, then there's no result.
    result = executor.runSource(src_code)
    script_exec.setExecResult(result)
except SyntaxError, ex:
    acserr_ex = pyExceptionToCORBA(ex)
    msg = "Syntax error when executing script. "
    msg = msg + "Error in line number " + str(ex.lineno) + ", offset " + str(ex.offset)
    msg = msg + ", text = '" + ex.text + "'."
    logger.logDebug(msg)
    scexec_ex = ScriptExecutorExceptionsImpl.ExecutionErrorExImpl(exception=acserr_ex)
    script_exec.setExecError(scexec_ex.errorTrace)
except (CCLExceptionsEx, DeviceErrorEx, NotYetImplementedEx, CCLErrorEx), ex:
    script_exec.setExecError(ex.errorTrace)
except:
    (ex_type, ex_value, ex_tb) = sys.exc_info()
    msg = "Unexpected exception when executing script: "
    if isinstance(ex_value, CORBA.UserException) and hasattr(ex_value, 'errorTrace'):
        acserr_ex = ex_value
    else:
        acserr_ex = pyExceptionToCORBA(ex_value)
    msg = msg + str(acserr_ex)
    logger.logDebug(msg)
    scexec_ex = ScriptExecutorExceptionsImpl.ExecutionErrorExImpl(exception=acserr_ex)
    script_exec.setExecError(scexec_ex.errorTrace)
    
rm.release_temp_resources()

rm.release_resources()
logger.logInfo("SB executor process finished.")
os._exit(0)

#
# __oOo__
