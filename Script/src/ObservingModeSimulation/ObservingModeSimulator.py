#!/usr/bin/env python

import CCL.Global
from AcsutilPy.FindFile import findFile

from ObservingModeSimulation.ArraySimulator import SimulatedArray

# The CCL.Global import registers an atexit method
# there is no very good way to remove this so we'll do it
# the dirty way
import atexit
for triplet in atexit._exithandlers:
    if triplet[0].func_name == "releaseComponents":
        atexit._exithandlers.remove(triplet)

class ObservingModeSimulator:
    def __init__(self):
        self._array = SimulatedArray()
        self._script = None

    def setScript(self, scriptName):
        self._script = findFile(scriptName)[0]
        if self._script == '':
            self._script = scriptName
            
    def setSchedBlock(self, schedBlock):
        CCL.Global.loadSB(schedBlock)

    def execute(self):
        CCL.Global.getArray = self.getProxyArray
        execfile(self._script)

    def getProxyArray(self):
        return self._array

