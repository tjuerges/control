#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------    ----------------------------------------------
# rhiriart  2005-12-28  created
#

import sys
import code
import readline
import atexit
import os
import traceback
import exceptions
import time
import getopt

import Control
import ScriptExecutorExceptions
import ScriptExecutorExceptionsImpl
import CCLExceptions
import CCLExceptionsImpl

from Acspy.Clients.SimpleClient import PySimpleClient
from ManualMode import ACSConsole

if __name__ == "__main__":

    acsBanner = """Atacama Large Milimitter Array
Script Executor CCL Console"""

    client = PySimpleClient()
    scexec = client.getComponent("CONTROL/SCRIPT")
    scexec.configure("CONTROL/AutomaticArrayMock", "<SchedBlock/>")    

    con = ACSConsole(scexec)
    con.interact()
    
    client.releaseComponent("CONTROL/SCRIPT")
    client.disconnect()

# __oOo__
