#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import unittest
from CCL.SpectralSpec import SpectralSpec
import CCL.APDMSchedBlock
import APDMEntities.SchedBlock
from ControlExceptionsImpl import IllegalParameterErrorExImpl
from ControlExceptionsImpl import InvalidRequestExImpl


class SpectralSpecTest(unittest.TestCase):


    def testInstanciation(self):
        ss = SpectralSpec()
        self.assertTrue(isinstance(ss, SpectralSpec))
        self.assertTrue(isinstance(ss, CCL.APDMSchedBlock.SpectralSpec))
        self.assertTrue(isinstance(ss, APDMEntities.SchedBlock.SpectralSpecT))
                         
    def testAddingBasebands(self):
        # First test adding a BasebandSpec with no Correlator Config
        ss = SpectralSpec()
        bbs = CCL.APDMSchedBlock.BaseBandSpecification()
        ss.addBaseband(bbs)

        # Test that we cannot add conflicting names
        ss = SpectralSpec()
        for idx in range(3):
            bbs = CCL.APDMSchedBlock.BaseBandSpecification()
            if idx < 1:
                ss.addBaseband(bbs)
            else:
                self.assertRaises(IllegalParameterErrorExImpl,
                                  ss.addBaseband, bbs)        

        # Test that we cannot add more than 4 Basebands
        ss = SpectralSpec()
        for idx in range(6):
            bbs = CCL.APDMSchedBlock.BaseBandSpecification()
            bbs.baseBandName = "BB_%d" % idx
            if idx < 4:
                ss.addBaseband(bbs)
            else:
                self.assertRaises(InvalidRequestExImpl, ss.addBaseband, bbs)
                
        # Now test that adding just a BasebandSpec to a Correlator Fails
        ss = SpectralSpec()
        cc = CCL.APDMSchedBlock.BLCorrelatorConfiguration()
        ss.setCorrelatorConfiguration(cc)
        bbs = CCL.APDMSchedBlock.BaseBandSpecification()

        self.assertRaises(IllegalParameterErrorExImpl, ss.addBaseband, bbs)
        
    def testCreateBLCorrelatorSpec(self):
        # Default Constructor
        ss = SpectralSpec.CreateBLCorrelatorSpec()

        self.__checkFrequencySetup(ss.FrequencySetup,
                                   86.243E9,
                                   {"BB_1":86.243E9})
        self.__checkCorrelatorConfig(ss.BLCorrelatorConfiguration,
                                     u'AP_UNCORRECTED',
                                     0.096,
                                     0.96,
                                     4.8,
                                     u'NORMAL',
                                     "XX",
                                     7680,
                                     1.875E9,
                                     3.0E9,
                                     [[(0,7680)]])

        # ---------------------------------------------------------------
        # Test TDM Mode and the duration setting
        ss = SpectralSpec.CreateBLCorrelatorSpec(dump = 0.031,
                                                 channelAverage = 0.096,
                                                 integration = 10,
                                                 frequency = '91.3 GHz',
                                                 corrMode = 'TDM',
                                                 autoOnly = True,
                                                 pol = '2',
                                                 apc='AP_CORRECTED')
        
        self.__checkFrequencySetup(ss.FrequencySetup,
                                   91.3E9,
                                   {"BB_1":91.3E9})
        self.__checkCorrelatorConfig(ss.BLCorrelatorConfiguration,
                                     u'AP_CORRECTED',
                                     0.096,
                                     0.192,
                                     9.984,
                                     u'NORMAL',
                                     "XX,YY",
                                     128,
                                     2E9,
                                     3.0E9,
                                     [[(0,128)]])

        # ------------------------------------------------------------------
        # Designed to test two basebands
        ss = SpectralSpec.CreateBLCorrelatorSpec\
             (bwd = 4,
              if2Freq = '3250 MHz',
              baseband=['BB_1', 'BB_3'],
              basebandFrequency=["87.0 GHz", "89.0 GHz"],
              channelAverageList=[[(100,5500)],[(100,50),(200,50)]])
        
        self.__checkFrequencySetup(ss.FrequencySetup,
                                   86.243E9,
                                   {"BB_1":87.0E9, "BB_3":89.0E9})
        self.__checkCorrelatorConfig(ss.BLCorrelatorConfiguration,
                                     u'AP_UNCORRECTED',
                                     0.096,
                                     0.96,
                                     4.8,
                                     u'NORMAL',
                                     "XX",
                                     7680,
                                     468.75E6,
                                     3.250E9,
                                     [[(100,5500)], [(100,50),(200,50)]])



    def __checkSpectralSpec(self, spectralSpec):
        print spectralSpec.toxml()


    def __checkFrequencySetup(self,
                              frequencySetup,
                              RestFrequency,
                              BaseBandMap):
        '''
        The frequencySetup is the structure to be compared.
        The RestFrequency is the specified rest frequency
        '''
        self.assertAlmostEqual(frequencySetup.restFrequency.get(),
                               RestFrequency)
        self.assertEqual(len(frequencySetup.BaseBandSpecification),
                         len(BaseBandMap))

        for bbs in frequencySetup.BaseBandSpecification:
            self.assertTrue(BaseBandMap.has_key(bbs.baseBandName))
            self.assertAlmostEqual(BaseBandMap[bbs.baseBandName],
                                   bbs.centerFrequencyRest.get())

    def __checkCorrelatorConfig(self, cc,
                                APCDataSets,
                                DumpDuration,
                                ChannelAverageDuration,
                                IntegrationDuration,
                                CAM,
                                PolnProducts,
                                NumChannels,
                                BandWidth,
                                IFCenterFreq,
                                ChannelAvgRegions):
        self.assertEqual(cc.aPCDataSets, APCDataSets)
        self.assertAlmostEqual(cc.dumpDuration.get(), DumpDuration)
        self.assertAlmostEqual(cc.channelAverageDuration.get(),
                               ChannelAverageDuration)
        self.assertAlmostEqual(cc.integrationDuration.get(),
                               IntegrationDuration)

        for bbc in cc.BLBaseBandConfig:
            sw = bbc.BLSpectralWindow[0]
            self.assertEqual(bbc.cAM, CAM)
            self.assertEqual(sw.polnProducts, PolnProducts)
            self.assertEqual(sw.effectiveNumberOfChannels, NumChannels)
            self.assertAlmostEqual(sw.effectiveBandwidth.get(), BandWidth)
            self.assertAlmostEqual(sw.centerFrequency.get(), IFCenterFreq)
            cars = []
            for car in sw.ChannelAverageRegion:
                cars.append((car.startChannel, car.numberChannels))
            self.assertTrue(cars in ChannelAvgRegions)
            ChannelAvgRegions.remove(cars)
            
            













if __name__ == "__main__":
    suite = unittest.makeSuite(SpectralSpecTest, 'test')
    unittest.TextTestRunner(verbosity=2).run(suite)
