#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#

import shutil
shutil.rmtree("../lib/python/site-packages/Control__POA", True)
shutil.rmtree("../lib/python/site-packages/Control", True)

import unittest
import Control

from CCL.Global import *
from CCL.Foo import Foo
from CCL.logging import getLogger

class GlobalCCLTest(unittest.TestCase):
    
    def setUp(self):
        self.logger = getLogger()
        setArrayName("AutomaticArrayMock")
        
    def tearDown(self): pass

    def testGetArray(self):
        self.logger.logInfo("Testing getArray")
        array = getArray()
        beginExecution()        
        foo = Foo("FOO")
        endExecution(Control.SUCCESS, "The end")

    def testSetSB(self):
        f = open("schedblks/SixBrightPolarStars.xml")
        xml = f.read()
        setSB(xml)
        sb = getSB()
        self.assertEquals("trcOP.scat.6.polar.stars.bright", sb.SchedBlock.name.getValue())

    def testLoadAndGetSB(self):
        self.logger.logInfo("Loading a scheduling block")
        loadSB("schedblks/SixBrightPolarStars.xml")
        sb = getSB()
        self.assertEquals("trcOP.scat.6.polar.stars.bright", sb.SchedBlock.name.getValue())

def suite():
    suite = unittest.TestSuite()
    suite.addTest(GlobalCCLTest("testGetArray"))
    suite.addTest(GlobalCCLTest("testSetSB"))
    suite.addTest(GlobalCCLTest("testLoadAndGetSB"))
    return suite

if __name__ == '__main__':
    unittest.main(defaultTest='suite')
