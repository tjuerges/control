#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2010
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import unittest


from CCL.SubscanList import SubscanList
from CCL.Source import PlanetSource
from CCL.SpectralSpec import SpectralSpec

from Control import OffsetSpec
from Control import ArrayOffset

from ControlExceptionsImpl import IllegalParameterErrorExImpl

class SubscanListTestCase(unittest.TestCase):
    def setUp(self):
        self.ssList = SubscanList()

    def testInstanciation(self):
        self.assertTrue(isinstance(self.ssList, SubscanList))
        self.assertEqual(self.ssList.getElapsedTime(), 0)
        
    def testAddSimpleSubscan(self):
        src = PlanetSource("Venus")
        ss  = SpectralSpec()
        self.assertEqual(self.ssList.addSubscan(src, ss, 30.0), 31.5)

        ssSpec = self.ssList.getSubscanSequenceSpecification()
        # Check the size of the fields
        self.assertEqual(len(ssSpec.sources), 1)
        self.assertEqual(len(ssSpec.spectra), 1)
        self.assertEqual(len(ssSpec.subscans), 1)

        # Now check the values
        self.assertEqual(ssSpec.subscans[0].arrayOffset, [])
        self.assertEqual(ssSpec.subscans[0].duration, 30.0)
        self.assertEqual(ssSpec.subscans[0].sourceId, 0)
        self.assertEqual(ssSpec.subscans[0].spectralId, 0)
        

    def testAddingSubscanTwice(self):
        src = PlanetSource("Venus")
        ss  = SpectralSpec()
        self.assertEqual(self.ssList.addSubscan(src, ss, 30.0), 31.5)
        self.assertEqual(self.ssList.addSubscan(src, ss, 30.0), 61.5)

        ssSpec = self.ssList.getSubscanSequenceSpecification()
        # Check the size of the fields
        self.assertEqual(len(ssSpec.sources), 1)
        self.assertEqual(len(ssSpec.spectra), 1)
        self.assertEqual(len(ssSpec.subscans), 2)

        # Now check the values
        for subscan in ssSpec.subscans:
            self.assertEqual(subscan.arrayOffset, [])
            self.assertEqual(subscan.duration, 30.0)
            self.assertEqual(subscan.sourceId, 0)
            self.assertEqual(subscan.spectralId, 0)


    def testChangingSources(self):
        src1 = PlanetSource("Venus")
        src2 = PlanetSource("Mars")
        ss  = SpectralSpec()
        self.assertEqual(self.ssList.addSubscan(src1, ss, 30.0), 31.5)
        self.assertEqual(self.ssList.addSubscan(src2, ss, 30.0), 63.0)

        ssSpec = self.ssList.getSubscanSequenceSpecification()
        # Check the size of the fields
        self.assertEqual(len(ssSpec.sources), 2)
        self.assertEqual(len(ssSpec.spectra), 1)
        self.assertEqual(len(ssSpec.subscans), 2)

        # Now check the values
        self.assertEqual(ssSpec.sources[0], src1)
        self.assertEqual(ssSpec.sources[1], src2)
        self.assertEqual(ssSpec.subscans[0].sourceId, 0)
        self.assertEqual(ssSpec.subscans[1].sourceId, 1)
        for subscan in ssSpec.subscans:
            self.assertEqual(subscan.arrayOffset, [])
            self.assertEqual(subscan.duration, 30.0)
            self.assertEqual(subscan.spectralId, 0)


    def testChangingSpectralSpecs(self):
        src = PlanetSource("Venus")
        ss1  = SpectralSpec()
        ss2  = SpectralSpec()
        self.assertEqual(self.ssList.addSubscan(src, ss1, 30.0), 31.5)
        self.assertEqual(self.ssList.addSubscan(src, ss2, 30.0), 63.0)
        self.assertEqual(self.ssList.addSubscan(src, ss1, 30.0), 94.5)
        
        ssSpec = self.ssList.getSubscanSequenceSpecification()
        # Check the size of the fields
        self.assertEqual(len(ssSpec.sources), 1)
        self.assertEqual(len(ssSpec.spectra), 2)
        self.assertEqual(len(ssSpec.subscans), 3)

        # Now check the values
        self.assertEqual(ssSpec.spectra[0], ss1)
        self.assertEqual(ssSpec.spectra[1], ss2)
        self.assertEqual(ssSpec.subscans[0].spectralId, 0)
        self.assertEqual(ssSpec.subscans[1].spectralId, 1)
        self.assertEqual(ssSpec.subscans[2].spectralId, 0)
        for subscan in ssSpec.subscans:
            self.assertEqual(subscan.arrayOffset, [])
            self.assertEqual(subscan.duration, 30.0)
            self.assertEqual(subscan.sourceId, 0)


    def testProcessOffsetSpec(self):
        testList = []
        testResult = []
        testList.append(None)
        testResult.append([])
        testList.append([1, 2, 3, 4])
        testResult.append([ArrayOffset(OffsetSpec(1, 2, 3, 4), "Default")]);
        testList.append((1.0,2.0,3.0,4.0))
        testResult.append([ArrayOffset(OffsetSpec(1, 2, 3, 4), "Default")]);
        testList.append([["Reference", [1, 2, 3, 4]]])
        testResult.append([ArrayOffset(OffsetSpec(1, 2, 3, 4), "Reference")]);
        testList.append([("Reference", [1, 2, 3, 4])])
        testResult.append([ArrayOffset(OffsetSpec(1, 2, 3, 4), "Reference")]);
        testList.append([("Default", [1, 2, 3, 4]),
                         ("Reference", (0, 1, 2, 3)),
                         ("Special", OffsetSpec(-1, -2, -3, -4))])
        testResult.append([ArrayOffset(OffsetSpec(1, 2, 3, 4), "Default"),
                           ArrayOffset(OffsetSpec(0, 1, 2, 3), "Reference"),
                           ArrayOffset(OffsetSpec(-1, -2, -3, -4), "Special")
                           ])
        for i in range(len(testList)):
            rtndArray = self.ssList.processOffsetSpec(testList[i])
            self.assertArrayOffsetEqual(rtndArray, testResult[i])

    def testCreateOffsetSpec(self):
        arcsec = 4.8481368110953598e-06
        degree = 0.017453292519943295

        # Tests that work
        goodOffsets = [(0.1,"1 arcsec"), [0.1, "1 arcsec"]]
        for pointingOffset in goodOffsets:
            offset = self.ssList.createOffsetSpec(pointingOffset)
            self.assertAlmostEqual(offset.longitudeOffset, 0.1);
            self.assertAlmostEqual(offset.latitudeOffset, arcsec);
            self.assertAlmostEqual(offset.longitudeVelocity, 0.0);
            self.assertAlmostEqual(offset.latitudeVelocity, 0.0);
            
        goodOffsets = [OffsetSpec(arcsec, 0.2, arcsec*30, degree),
                       OffsetSpec("1 arcsec", 0.2,"30 arcsec/s",
                                           "1 deg/s"),
                       ["1 arcsec", 0.2,"30 arcsec/s", "1 deg/s"],
                       ("1 arcsec", 0.2,"30 arcsec/s", "1 deg/s")]
        for pointingOffset in goodOffsets:
            offset = self.ssList.createOffsetSpec(pointingOffset)
            self.assertAlmostEqual(offset.longitudeOffset, arcsec);
            self.assertAlmostEqual(offset.latitudeOffset, 0.2);
            self.assertAlmostEqual(offset.longitudeVelocity, arcsec*30);
            self.assertAlmostEqual(offset.latitudeVelocity, degree);

        # Tests that should throw
        badOffsets = [OffsetSpec("1 deg/s", 0.2, "30 arcsec",
                                          "1 deg/s"), 
                      1, (1), [1], (1,2,3), (1,2,3,4,5)]

        for pointingOffset in badOffsets:
            self.assertRaises(IllegalParameterErrorExImpl,
                              self.ssList.createOffsetSpec,
                              pointingOffset)

    def testSettingOffsets(self):
        src = PlanetSource("Venus")
        ss  = SpectralSpec()
        offset = OffsetSpec(0,1,2,3)
        self.assertEqual(self.ssList.addSubscan(src, ss, 30.0),31.5)
        self.assertEqual(self.ssList.addSubscan(src, ss, 30.0, offset),61.5)
        self.assertEqual(self.ssList.addSubscan(src, ss, 30.0, offset),91.5)

        offset.latitudeOffset = 5.0
        self.assertEqual(self.ssList.addSubscan(src, ss, 30.0, offset),121.5)

        ssSpec = self.ssList.getSubscanSequenceSpecification()

        # Check the size of the fields
        self.assertEqual(len(ssSpec.sources), 1)
        self.assertEqual(len(ssSpec.spectra), 1)
        self.assertEqual(len(ssSpec.subscans), 4)

        # Now check the values
        self.assertEqual(ssSpec.spectra[0], ss)
        self.assertEqual(ssSpec.sources[0], src)

        self.assertArrayOffsetEqual(ssSpec.subscans[0].arrayOffset, [])
        self.assertArrayOffsetEqual(ssSpec.subscans[1].arrayOffset,
                                    [ArrayOffset(OffsetSpec(0, 1, 2, 3),"Default")])
        self.assertArrayOffsetEqual(ssSpec.subscans[2].arrayOffset,
                                    [ArrayOffset(OffsetSpec(0, 1, 2, 3),"Default")])
        self.assertArrayOffsetEqual(ssSpec.subscans[3].arrayOffset,
                                    [ArrayOffset(OffsetSpec(0, 5, 2, 3),"Default")])

        for subscan in ssSpec.subscans:
            self.assertEqual(subscan.duration, 30.0)
            self.assertEqual(subscan.sourceId, 0)
            self.assertEqual(subscan.spectralId, 0)
            
    def assertArrayOffsetEqual(self, arrayOffset1, arrayOffset2):
        self.assertEqual(len(arrayOffset1),len(arrayOffset2))
        for idx in range(len(arrayOffset1)):
            self.assertAlmostEqual(arrayOffset1[idx].offset.latitudeOffset,
                                   arrayOffset2[idx].offset.latitudeOffset)
            self.assertAlmostEqual(arrayOffset1[idx].offset.longitudeOffset,
                                   arrayOffset2[idx].offset.longitudeOffset)
            self.assertAlmostEqual(arrayOffset1[idx].offset.latitudeVelocity,
                                   arrayOffset2[idx].offset.latitudeVelocity)
            self.assertAlmostEqual(arrayOffset1[idx].offset.longitudeVelocity,
                                   arrayOffset2[idx].offset.longitudeVelocity)
            self.assertEqual(arrayOffset1[idx].name,arrayOffset2[idx].name)
        
if __name__ == '__main__':
    suite = unittest.makeSuite(SubscanListTestCase)
    unittest.TextTestRunner(verbosity=2).run(suite)
