#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# rhiriart  2005-12-16  created
#

import sys
import traceback
import unittest

from Acspy.Clients.SimpleClient import PySimpleClient
from Acspy.Common.Log import getLogger

import Script
import ScriptExecutorExceptions
import ScriptExecutorExceptionsImpl

class TestScriptExecutor(unittest.TestCase):

    ## @brief Sets up the test case "fixture".
    #
    # This method is called before each test case method.
    def setUp(self):
        
        self.client = PySimpleClient()
        
        # Get the ScriptExecutor component.
        self.script = self.client.getComponent("CONTROL/SCRIPT")

        self.logger = getLogger("TestScriptExecutor")
        
    ## @brief Tear down the test case "fixture".
    #
    # This method is called after each test case method.
    def tearDown(self):
        self.client.releaseComponent("CONTROL/SCRIPT")
        self.client.disconnect()


    def testExecutionException(self):

        try:
            self.script.runSource("a=")
        except ScriptExecutorExceptions.SyntaxErrorEx, ex:
            print "Syntax error exception"
            helperExcObj = ScriptExecutorExceptionsImpl.SyntaxErrorExImpl(exception=ex,
                                                                          create=1)
            helperExcObj.Print()
            helperExcObj.log()
            #helperExcObj.printET(helperExcObj.getErrorTrace())
        except ScriptExecutorExceptions.ExecutionErrorEx, ex:
            print "Execution exception"
            helperExcObj = ScriptExecutorExceptionsImpl.ExecutionErrorExImpl(exception=ex,
                                                                             create=1)
            helperExcObj.Print()
            helperExcObj.log()
        except:
            type, value, tb = sys.exc_info()
            print "Other exception"
            print type.__doc__
            print value.__doc__
            tblist = traceback.extract_tb(tb)
            print traceback.format_list(tblist)

    def testSyntaxErrorException(self):
            
        self.assertRaises(ScriptExecutorExceptions.SyntaxErrorEx,
                          self.script.runSource,
                          "a=")
        
def suite():
    suite = unittest.TestSuite()
    suite.addTest(TestScriptExecutor("testExecutionException"))
    #suite.addTest(TestScriptExecutor("testSyntaxErrorException"))    
    return suite

if __name__ == '__main__':
    unittest.main(defaultTest='suite')



#
# ___oOo___
