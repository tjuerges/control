#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import unittest
import CCL.APDMSchedBlock as APDMSchedBlock
import APDMEntities.SchedBlock
from CCL.FrequencySetup import FrequencySetup

class FrequencySetupTest(unittest.TestCase):
    def testDefaultInstanciation(self):
        fs = FrequencySetup()
        self.assertTrue(isinstance(fs, FrequencySetup))
        self.assertTrue(isinstance(fs, APDMSchedBlock.FrequencySetup))
        self.assertTrue\
            (isinstance(fs, APDMEntities.SchedBlock.FrequencySetupT))


if __name__ == "__main__":
    alltests = unittest.makeSuite(FrequencySetupTest,'test')
    unittest.TextTestRunner(verbosity=2).run(alltests)

