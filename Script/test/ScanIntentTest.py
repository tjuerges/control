#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import unittest
from CCL.ScanIntent import *

class ScanIntentTest(unittest.TestCase):
    
    #Test setting the Integration Duration
    def testSettingScanIntentAsEnum(self):
        from PyDataModelEnumeration import PyScanIntent
        for value in PyScanIntent.ScanIntentSet():
            scanIntent = ScanIntent(PyScanIntent.fromString(value))
            self.failIf(scanIntent.getScanIntent() !=
                        PyScanIntent.fromString(value))

    def testSettingScanIntentAsString(self):
        from PyDataModelEnumeration import PyScanIntent
        for value in PyScanIntent.ScanIntentSet():
            scanIntent = ScanIntent(value)
            self.failIf(scanIntent.getScanIntent() !=
                        PyScanIntent.fromString(value))

    def testSettingCalDataOriginAsEnum(self):
        from PyDataModelEnumeration import PyCalDataOrigin
        for value in PyCalDataOrigin.CalDataOriginSet():
            scanIntent = ScanIntent('UNSPECIFIED')
            scanIntent.setCalibrationDataOrigin(PyCalDataOrigin.fromString(value))
            self.failIf(scanIntent.getCalibrationDataOrigin() !=
                        PyCalDataOrigin.fromString(value))

    def testSettingCalDataOriginAsString(self):
        from PyDataModelEnumeration import PyCalDataOrigin
        for value in PyCalDataOrigin.CalDataOriginSet():
            scanIntent = ScanIntent('UNSPECIFIED')
            scanIntent.setCalibrationDataOrigin(value)
            self.failIf(scanIntent.getCalibrationDataOrigin() !=
                        PyCalDataOrigin.fromString(value))
            
    def testSettingCalSetAsEnum(self):
        from PyDataModelEnumeration import PyCalibrationSet
        for value in PyCalibrationSet.CalibrationSetSet():
            scanIntent = ScanIntent("UNSPECIFIED")
            scanIntent.setCalibrationSet(PyCalibrationSet.fromString(value))
            self.failIf(scanIntent.getCalibrationSet() != 
                        PyCalibrationSet.fromString(value))

    def testSettingCalSetAsString(self):
        from PyDataModelEnumeration import PyCalibrationSet
        for value in PyCalibrationSet.CalibrationSetSet():
            scanIntent = ScanIntent("UNSPECIFIED")
            scanIntent.setCalibrationSet(value)
            self.failIf(scanIntent.getCalibrationSet() != 
                        PyCalibrationSet.fromString(value))

    def testSettingCalFunctionAsEnum(self):
        from PyDataModelEnumeration import PyCalibrationFunction
        for value in PyCalibrationFunction.CalibrationFunctionSet():
            scanIntent = ScanIntent("UNSPECIFIED")
            scanIntent.setCalibrationFunction(PyCalibrationFunction.fromString(value))
            self.failIf(scanIntent.getCalibrationFunction() !=
                        PyCalibrationFunction.fromString(value))

    def testSettingCalFunctionAsString(self):
        from PyDataModelEnumeration import PyCalibrationFunction
        for value in PyCalibrationFunction.CalibrationFunctionSet():
            scanIntent = ScanIntent("UNSPECIFIED")
            scanIntent.setCalibrationFunction(value)
            self.failIf(scanIntent.getCalibrationFunction() !=
                        PyCalibrationFunction.fromString(value))

    def testSettingAntennaMotionPatternAsEnum(self):
        from PyDataModelEnumeration import PyAntennaMotionPattern
        for value in PyAntennaMotionPattern.AntennaMotionPatternSet():
            scanIntent = ScanIntent("UNSPECIFIED")
            scanIntent.setAntennaMotionPattern(PyAntennaMotionPattern.fromString(value))
            self.failIf(scanIntent.getAntennaMotionPattern() !=
                        PyAntennaMotionPattern.fromString(value))

    def testSettingAntennaMotionPatternAsString(self):
        from PyDataModelEnumeration import PyAntennaMotionPattern
        for value in PyAntennaMotionPattern.AntennaMotionPatternSet():
            scanIntent = ScanIntent("UNSPECIFIED")
            scanIntent.setAntennaMotionPattern(value)
            self.failIf(scanIntent.getAntennaMotionPattern() !=
                        PyAntennaMotionPattern.fromString(value))

    def testSettingOnlineProcessing(self):
          from ScanIntentMod import UNSPECIFIED
          enumList = [True, False]
          for value in enumList:
              scanIntent = ScanIntent(UNSPECIFIED)
              scanIntent.setOnlineProcessing(value)
              self.failIf(scanIntent.getOnlineProcessing() != value)


        
def suite():
    alltests =  unittest.makeSuite(ScanIntentTest,'test')
    return alltests


if __name__ == "__main__":
    unittest.main(defaultTest='suite')
