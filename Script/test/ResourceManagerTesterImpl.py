#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# rhiriart  2006-12-06  created
#

from Acspy.Servants.ACSComponent       import ACSComponent
from Acspy.Servants.ComponentLifecycle import ComponentLifecycle
from Acspy.Servants.ContainerServices  import ContainerServices
from CCL.Foo import Foo

import ControlTest__POA
import ScriptExec.ResourceManager

class ResourceManagerTesterImpl(ControlTest__POA.ResourceManagerTester,
                                ACSComponent,
                                ContainerServices,
                                ComponentLifecycle):
    def __init__(self):
        ACSComponent.__init__(self)
        ContainerServices.__init__(self)
        self.logger = self.getLogger()
        self.name = self._get_name()
        self.resMng = None
        
    def initialize(self):
        self.logger = self.getLogger()
        self.name = self._get_name()
        self.logger.logInfo("Initializing "+self.name)
        
    def cleanUp(self):
        self.logger.logInfo("called...")
        # Release all resources. "BAR" will be released and also
        # "FOO", if it hasn't been released yet.
        self.resMng.releaseResources()
        ScriptExec.ResourceManager.ResourceManager.cleanUp()
            
    def getName(self):
        return self.name

    def initializeResourceManager(self):

        # Create the ResourceManager singleton.
        # This will actually create a new instance.
        self.resMng = ScriptExec.ResourceManager.ResourceManager(self)

        # This will set the ResourceManager context.
        # From now on, calling ResourceManager() without parameters will return the same
        # instance created above.
        # In practice the call above will need to be executed using Python
        # exec function.
        ScriptExec.ResourceManager._contextName = self.name

        # This is a resource which reference need to be maintained for the life of the
        # component.
        # The reference will be maintained until the call ResourceManager.releaseResources()
        # which should be called in cleanUp().
        self.resMng.addResource("BAR")

        # This call will actually get the ACS references.
        self.resMng.acquireResources()
    
    def createCCLObjects(self):

        # Create a CCL object. The CCL objects get components which need to be maintained only
        # during a given script execution. This is a temporary object, its reference will be
        # maintained until the call to ResourceManager.releaseTemporaryResources().
        self.foo = Foo("FOO")

    def destroyCCLObjects(self):

        # Destroy all temporary resources. "FOO" will be released.
        self.resMng.releaseTemporaryResources()
