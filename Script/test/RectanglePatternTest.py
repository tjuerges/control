#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import unittest
import math
#import ControlExceptionsImpl

import CCL.APDMSchedBlock
import CCL.APDMValueTypes

import APDMEntities
#from CCL.Global import *


class RectanglePatternTest(unittest.TestCase):
    def testInstanciation(self):
        rp = CCL.APDMSchedBlock.RectanglePattern()
        self.assertTrue(isinstance(rp, CCL.APDMSchedBlock.RectanglePattern))
        self.assertTrue(isinstance(rp,
                                   APDMEntities.SchedBlock.RectanglePatternT))

    def testDefaultValues(self):
        rp = CCL.APDMSchedBlock.RectanglePattern()

        self.assertEqual(rp.type, u'rectangle')
        self.assertNotEqual(rp.patternCenterCoordinates, None)
        self.assertNotEqual(rp.longitudeLength, None)
        self.assertNotEqual(rp.latitudeLength, None)
        self.assertNotEqual(rp.orthogonalStep, None)
        self.assertEqual(rp.scanDirection, u'longitude')
        self.assertNotEqual(rp.orientation, None)
        self.assertEqual(rp.scanningCoordinateSystem, u'azel')
        self.assertFalse(rp.uniDirectionalScan)

    def testInstanciationFromXML(self):
        defaultXML = '''<?xml version="1.0" ?>
        <ns1:RectanglePattern type="rectangle"
                               xmlns:ns1="Alma/ObsPrep/SchedBlock"
                               xmlns:ns2="Alma/ValueTypes">
          <ns1:orientation unit="deg">45.0</ns1:orientation>
          <ns1:uniDirectionalScan>true</ns1:uniDirectionalScan>
          <ns1:latitudeLength unit="arcmin">1.0</ns1:latitudeLength>
          <ns1:longitudeLength unit="arcmin">2.0</ns1:longitudeLength>
          <ns1:patternCenterCoordinates type="RELATIVE"
                                        system="J2000">
            <ns2:fieldName>RandomField</ns2:fieldName>
            <ns2:longitude unit="arcmin">5.0</ns2:longitude>
            <ns2:latitude unit="arcmin">-5.0</ns2:latitude>
          </ns1:patternCenterCoordinates>
          <ns1:orthogonalStep unit="arcsec">10</ns1:orthogonalStep>
        </ns1:RectanglePattern>
        '''        
        rp = CCL.APDMSchedBlock.RectanglePattern.CreateFromDocument(defaultXML)
        self.assertTrue(isinstance(rp, CCL.APDMSchedBlock.RectanglePattern))

        # Check that the Fields are all the wrapped Classes
        self.assertTrue(isinstance(rp.patternCenterCoordinates,
                                   CCL.APDMValueTypes.SkyCoordinates))
        self.assertTrue(isinstance(rp.orientation,
                                   CCL.APDMValueTypes.Angle))
        self.assertTrue(isinstance(rp.latitudeLength,
                                   CCL.APDMValueTypes.Angle))
        self.assertTrue(isinstance(rp.longitudeLength,
                                   CCL.APDMValueTypes.Angle))
        self.assertTrue(isinstance(rp.orthogonalStep,
                                   CCL.APDMValueTypes.Angle))

        # Now check that the values come correctly from the XML
        self.assertEqual(rp.patternCenterCoordinates.type, u'RELATIVE')
        self.assertAlmostEqual(rp.patternCenterCoordinates.longitude.get(),
                               math.radians(5/60.))
        self.assertAlmostEqual(rp.patternCenterCoordinates.latitude.get(),
                               math.radians(-5/60.))
        self.assertAlmostEqual(rp.orientation.get(), math.radians(45))
        self.assertAlmostEqual(rp.latitudeLength.get(), math.radians(1/60.))
        self.assertAlmostEqual(rp.longitudeLength.get(), math.radians(2/60.))
        self.assertAlmostEqual(rp.orthogonalStep.get(), math.radians(10/3600.))
        
class SubscanDefinitionTest(unittest.TestCase):
    '''
    This test verifies that the subscan lists that are generated are correct.
    '''

    def testLongitudeScan(self):
        rp = CCL.APDMSchedBlock.RectanglePattern()
        rp.patternCenterCoordinates.longitude.set(0)
        rp.patternCenterCoordinates.latitude.set(0)
        rp.patternCenterCoordinates.type=u'RELATIVE'
        rp.longitudeLength.set("0.5 rad")
        rp.latitudeLength.set("1 rad")
        rp.orthogonalStep.set("0.25 rad")
        rp.scanDirection= u'longitude'
        rp.uniDirectionalScan = True
        ssl = rp.generateSubscanList(1.0)

        longitudeStart = [-0.25] * 5
        latitudeStart = [-0.5, -0.25, 0.0, 0.25, 0.5]
        longitudeLength =  [0.5] * 5
        latitudeLength = [0.0] * 5
        self._compareSubscanList(longitudeStart, latitudeStart,
                                 longitudeLength, latitudeLength, ssl) 

    def testCalculationOfVelocity(self):
        rp = CCL.APDMSchedBlock.RectanglePattern()
        rp.patternCenterCoordinates.longitude.set(0)
        rp.patternCenterCoordinates.latitude.set(0)
        rp.patternCenterCoordinates.type=u'RELATIVE'
        rp.longitudeLength.set("0.5 rad")
        rp.latitudeLength.set("1 rad")
        rp.orthogonalStep.set("0.25 rad")
        rp.scanDirection = u'longitude'
        rp.uniDirectionalScan = True
        ssl = rp.generateSubscanList(10)

        longitudeStart = [-0.25] * 5
        latitudeStart = [-0.5, -0.25, 0.0, 0.25, 0.5]
        longitudeLength =  [0.05] * 5
        latitudeLength = [0.0] * 5
        self._compareSubscanList(longitudeStart, latitudeStart,
                                 longitudeLength, latitudeLength, ssl) 

    def testLatitudeScan(self):
        rp = CCL.APDMSchedBlock.RectanglePattern()
        rp.patternCenterCoordinates.longitude.set(0)
        rp.patternCenterCoordinates.latitude.set(0)
        rp.patternCenterCoordinates.type=u'RELATIVE'
        rp.longitudeLength.set("1.5 rad")
        rp.latitudeLength.set("1 rad")
        rp.orthogonalStep.set("0.25 rad")
        rp.scanDirection = u'latitude'
        rp.uniDirectionalScan = True
        ssl = rp.generateSubscanList(1.0)

        longitudeStart = [-0.75, -0.5, -0.25, 0.0, 0.25, 0.5, 0.75]
        latitudeStart = [-0.5] * 7
        longitudeLength =  [0] * 7
        latitudeLength = [1.0] * 7
        self._compareSubscanList(longitudeStart, latitudeStart,
                                 longitudeLength, latitudeLength, ssl) 

    def testRotatedScan(self):
        rp = CCL.APDMSchedBlock.RectanglePattern()
        rp.patternCenterCoordinates.longitude.set(0)
        rp.patternCenterCoordinates.latitude.set(0)
        rp.patternCenterCoordinates.type=u'RELATIVE'
        rp.longitudeLength.set("0.5 rad")
        rp.latitudeLength.set("1 rad")
        rp.orthogonalStep.set("0.25 rad")
        rp.scanDirection = u'longitude'
        rp.orientation.set("30 deg")
        rp.uniDirectionalScan = True
        ssl = rp.generateSubscanList(1.0)

        longitudeStart = []
        latitudeStart = []
        for idx in range (-2,3):
            longitudeStart.append((-math.sqrt(3) - idx)/8.0)
            latitudeStart.append((idx*math.sqrt(3) - 1)/8.0)
        longitudeLength =  [math.sqrt(3)/4] * 5
        latitudeLength = [0.25] * 5
        
        self._compareSubscanList(longitudeStart, latitudeStart,
                                 longitudeLength, latitudeLength, ssl) 

    def testRelativeOffsetScan(self):
        rp = CCL.APDMSchedBlock.RectanglePattern()
        rp.patternCenterCoordinates.longitude.set(1.0)
        rp.patternCenterCoordinates.latitude.set(0.25)
        rp.patternCenterCoordinates.type=u'RELATIVE'
        rp.longitudeLength.set("0.5 rad")
        rp.latitudeLength.set("1 rad")
        rp.orthogonalStep.set("0.25 rad")
        rp.scanDirection = u'longitude'
        rp.orientation.set("0 deg")
        rp.uniDirectionalScan = True
        ssl = rp.generateSubscanList(1.0)

        longitudeStart = [0.75] * 5
        latitudeStart = [-0.25, 0.0, 0.25, 0.5, 0.75]
        longitudeLength =  [0.5] * 5
        latitudeLength = [0.0] * 5
        self._compareSubscanList(longitudeStart, latitudeStart,
                                 longitudeLength, latitudeLength, ssl) 

    def testRelativeOffsetWithRotation(self):
        rp = CCL.APDMSchedBlock.RectanglePattern()
        rp.patternCenterCoordinates.longitude.set(-0.5)
        rp.patternCenterCoordinates.latitude.set(0.25)
        rp.patternCenterCoordinates.type=u'RELATIVE'
        rp.longitudeLength.set("0.5 rad")
        rp.latitudeLength.set("1 rad")
        rp.orthogonalStep.set("0.25 rad")
        rp.scanDirection = u'longitude'
        rp.orientation.set("30 deg")
        rp.uniDirectionalScan = True
        ssl = rp.generateSubscanList(1.0)

        longitudeStart = []
        latitudeStart = []
        for idx in range (-2,3):
            longitudeStart.append(((-math.sqrt(3) - idx)/8.0)-0.5)
            latitudeStart.append(((idx*math.sqrt(3) - 1)/8.0)+0.25)
        longitudeLength =  [math.sqrt(3)/4] * 5
        latitudeLength = [0.25] * 5
        self._compareSubscanList(longitudeStart, latitudeStart,
                                 longitudeLength, latitudeLength, ssl) 

    def _compareSubscanList(self, longStart, latStart, longLength, latLength,
                            subscanList):
        self.failIf(len(subscanList) != len(longStart),
                    "Subscan and longitude start lists unequal size")
        self.failIf(len(subscanList) != len(latStart),
                    "Subscan and latitude start lists unequal size")
        self.failIf(len(subscanList) != len(longLength),
                    "Subscan and longitude length lists unequal size")
        self.failIf(len(subscanList) != len(latLength),
                    "Subscan and latidtude length lists unequal size")
        for idx in range(len(subscanList)):
            self.failIf(abs(subscanList[idx].pointingOffset[0] - longStart[idx]) > 1E-8,
                        "Starting Longitude does not match subscan %d." % idx)
            self.failIf(abs(subscanList[idx].pointingOffset[1] - latStart[idx]) > 1E-8,
                        "Starting latitude does not match subscan %d." % idx)
            self.failIf(abs(subscanList[idx].pointingOffset[2] - longLength[idx]) > 1E-8,
                        "Longitude Length does not match subscan %d." % idx)
            self.failIf(abs(subscanList[idx].pointingOffset[3] - latLength[idx]) > 1E-8,
                        "Latitude Length does not match subscan %d." % idx)
    
if __name__ == "__main__":
    CCLSuite     = unittest.makeSuite(RectanglePatternTest,'test')
    SubscanSuite = unittest.makeSuite(SubscanDefinitionTest,'test')
    suite = unittest.TestSuite((CCLSuite, SubscanSuite))

    
    unittest.TextTestRunner(verbosity=2).run(suite)
