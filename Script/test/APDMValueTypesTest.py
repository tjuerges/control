#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002
# (c) European Southern Observatory, 2002
#
# @(#) $Id$
#------------------------------------------------------------------------------


import unittest
import math
from CCL.APDMValueTypes import *

class APDMValueTypesTest(unittest.TestCase):
    def testAngle(self):
        ang = Angle(math.radians(30))
        self.assertAlmostEqual(ang.get(), math.radians(30))
        ang.set("50 deg")
        self.assertAlmostEqual(ang.get(), math.radians(50))
    
    def testFrequency(self):
        freq = Frequency("3E9")
        self.assertAlmostEqual(freq.get(), 3E9)
        freq.set("30 kHz")
        self.assertAlmostEqual(freq.get(), 3E4)
        freq.set(31.25E6)
        self.assertAlmostEqual(freq.get(), 31.25E6)

    def testTime(self):
        t = Time("15")
        self.assertAlmostEqual(t.get(), 15)
        t.set("30 min")
        self.assertAlmostEqual(t.get(), 1800)
        t.set(21)
        self.assertAlmostEqual(t.get(), 21.0)

    def testFlux(self):
        f = Flux("0.01")
        self.assertAlmostEqual(f.get(), 0.01)
        f.set("30 uJy")
        self.assertAlmostEqual(f.get(), 30E-6)
        f.set("1E-26 kg.s-2")
        self.assertAlmostEqual(f.get(), 1.0)

    def testVelocity(self):
        v = Velocity()
        self.assertNotEqual(v.centerVelocity, None)
        self.assertNotEqual(v.referenceSystem, None)
        self.assertNotEqual(v.dopplerCalcType, None)

        self.assertEqual(v.centerVelocity.get(), 0)
        v.centerVelocity.set("3600 km/h")
        self.assertAlmostEqual(v.centerVelocity.get(), 1000.)

    def testSkyCoordinates(self):
        sc = SkyCoordinates()
        # Test the constructor
        self.assertNotEqual(sc.longitude, None)
        self.assertNotEqual(sc.latitude, None)
        self.assertNotEqual(sc.type, None)
        self.assertNotEqual(sc.system, None)
        self.assertNotEqual(sc.fieldName, None)

        self.assertEqual(sc.longitude.get(), math.radians(0))
        self.assertEqual(sc.latitude.get(), math.radians(0))

        sc.fieldName = "MySource"
        sc.longitude.set("8:15:00")
        sc.latitude.set("-50.15.00")

        self.assertAlmostEqual(sc.longitude.get(), math.radians(123.75))
        self.assertAlmostEqual(sc.latitude.get(), math.radians(-50.25))
        self.assertEqual(sc.fieldName, "MySource")

    def testSpeed(self):
        s = Speed("1.2")
        self.assertAlmostEqual(s.get(), 1.2)
        s.set("3 km/min")
        self.assertAlmostEqual(s.get(), 50.0)
        
    def testAngularVelocity(self):
        av = AngularVelocity("1.3")
        self.assertAlmostEqual(av.get(), 1.3)
        av.set("2 deg/min")
        self.assertAlmostEqual(av.get(), math.radians(2.0/60.0))

    def testLength(self):
        l = Length("1.3")
        self.assertAlmostEqual(l.get(), 1.3)
        l.set("5 cm")
        self.assertAlmostEqual(l.get(), 0.05)

    def testLatitude(self):
        lat = Latitude(math.radians(30))
        self.assertAlmostEqual(lat.get(), math.radians(30))
        lat.set("50 deg")
        self.assertAlmostEqual(lat.get(), math.radians(50))
        lat.set("12:15:00")
        self.assertAlmostEqual(lat.get(), math.radians(183.75))

    def testLongitude(self):
        long = Longitude(math.radians(30))
        self.assertAlmostEqual(long.get(), math.radians(30))
        long.set("50 deg")
        self.assertAlmostEqual(long.get(), math.radians(50))
        long.set("45.30.00")
        self.assertAlmostEqual(long.get(), math.radians(45.5))

    def testIntTimeSource(self):
        intTime = IntTimeSource(5)
        self.assertAlmostEqual(intTime.get(), 5)
        intTime.set("2 min")
        self.assertAlmostEqual(intTime.get(), 120.0)
        

if __name__ == "__main__":
    suite = unittest.makeSuite(APDMValueTypesTest, 'test')
    unittest.TextTestRunner(verbosity=2).run(suite)
