#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# rhiriart  2005-08-31  created
#

#************************************************************************
#   NAME
#   TestUpdatePointingModel.py - Test the updatePointingModel utility.
#
#   SYNOPSIS
# 
#   DESCRIPTION
#
#   FILES
#
#   ENVIRONMENT
#
#   RETURN VALUES
#
#   CAUTIONS
#
#   EXAMPLES
#
#   SEE ALSO
#
#   BUGS     
#
#------------------------------------------------------------------------
#

import unittest
import xmlentity
import re

from Acspy.Clients.SimpleClient import PySimpleClient
from time import sleep
from ScriptImpl.updatePointingModel import updatePointingModel

class TestUpdatePointingModel(unittest.TestCase):

    ## @brief Sets up the test case "fixture".
    #
    # This method is called before each test case method.
    def setUp(self):                
        self.client = PySimpleClient()
        self.archConn = self.client.getComponent('ARCHIVE_CONNECTION')
        self.archOp = self.archConn.getOperational('updatePointingModel')
        self.archId = self.client.getComponent('ARCHIVE_IDENTIFIER')
        
    ## @brief Tear down the test case "fixture".
    #
    # This method is called after each test case method.
    def tearDown(self):
        self.client.releaseComponent('ARCHIVE_IDENTIFIER')
        self.client.releaseComponent('ARCHIVE_CONNECTION')
        self.client.disconnect()

    def testUpdatePointingModel(self):

        # Read the tables
        calDataFile = open('./pntmodel/CalData.xml', 'r')
        lines = calDataFile.readlines()
        calDataXML = ""
        for l in lines:
            calDataXML = calDataXML + l
        
        calPointingModelFile = open('./pntmodel/CalPointingModel.xml', 'r')
        lines = calPointingModelFile.readlines()
        calPointingModelXML = ""
        for l in lines:
            calPointingModelXML = calPointingModelXML + l

        calReductionFile = open('./pntmodel/CalReduction.xml', 'r')
        lines = calReductionFile.readlines()
        calReductionXML = ""
        for l in lines:
            calReductionXML = calReductionXML + l

        asdmFile = open('./pntmodel/ASDM.xml', 'r')
        lines = asdmFile.readlines()
        asdmXML = ""
        for l in lines:
            asdmXML = asdmXML + l

        # Get UIDs for these tables
        ids = self.archId.getUIDs(4)
        calDataUID = ids[0]
        calPointingModelUID = ids[1]
        calReductionUID = ids[2]
        asdmUID = ids[3]

        # Replace UID placeholders in the XML strings
        calDataXML = re.sub("container-entity-goes-here", asdmUID, calDataXML)
        calDataXML = re.sub("entity-goes-here", calDataUID, calDataXML)
        
        calPointingModelXML = re.sub("container-entity-goes-here", asdmUID, calPointingModelXML)
        calPointingModelXML = re.sub("entity-goes-here", calPointingModelUID, calPointingModelXML)

        calReductionXML = re.sub("container-entity-goes-here", asdmUID, calReductionXML)
        calReductionXML = re.sub("entity-goes-here", calReductionUID, calReductionXML)

        asdmXML = re.sub("container-entity-goes-here", asdmUID, asdmXML)
        asdmXML = re.sub("CalData-entity-goes-here", calDataUID, asdmXML)
        asdmXML = re.sub("CalPointingModel-entity-goes-here", calPointingModelUID, asdmXML)
        asdmXML = re.sub("CalReduction-entity-goes-here", calReductionUID, asdmXML)

        # Store the tables
        print "Storing CalDataTable. UID = " + calDataUID
        calDataEnt = xmlentity.XmlEntityStruct(calDataXML, calDataUID, "CalDataTable", "1", "0")
        self.archOp.store(calDataEnt)

        print "Storing CalPointingModelTable. UID = " + calPointingModelUID
        calPointingModelEnt = xmlentity.XmlEntityStruct(calPointingModelXML, calPointingModelUID, "CalPointingModelTable", "1", "0")
        self.archOp.store(calPointingModelEnt)

        print "Storing CalReductionTable. UID = " + calReductionUID
        calReductionEnt = xmlentity.XmlEntityStruct(calReductionXML, calReductionUID, "CalReductionTable", "1", "0")
        self.archOp.store(calReductionEnt)

        print "Storing ASDM. UID = " + asdmUID
        asdmEnt = xmlentity.XmlEntityStruct(asdmXML, asdmUID, "ASDM", "1", "0")
        self.archOp.store(asdmEnt)

        # Call the updatePointingModel utility
        updatePointingModel(['ALMA001'], asdmUID)
                
def suite():
    suite = unittest.TestSuite()
    suite.addTest(TestUpdatePointingModel("testUpdatePointingModel"))
    return suite

if __name__ == '__main__':
    unittest.main(defaultTest='suite')

#
# ___oOo___
