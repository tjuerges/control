#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# rhiriart  2007-02-05  created
#

from Acspy.Clients.SimpleClient import PySimpleClient
import sys, getopt
import TMCDB_IDL
import asdmIDLTypes

TMCDB_CURL = "IDL:alma/TMCDB/TMCDBComponent:1.0"
MASTER_CURL = "CONTROL/MASTER"

hrxConfig = TMCDB_IDL.AssemblyLocationIDL("Holography Receiver",
                                          "HoloRx",
                                           0x1c,
                                           0,
                                           0)
hdspConfig = TMCDB_IDL.AssemblyLocationIDL("Holography DSP",
                                           "HoloDSP",
                                            0x1d,
                                            0,
                                            0)
otConfig = TMCDB_IDL.AssemblyLocationIDL( "optical telescope", 
                                          "PrototypeOpticalTelescope", 
                                           0x1f, 
                                           0, 
                                           0 )
fgConfig = TMCDB_IDL.AssemblyLocationIDL( "frame grabber", 
                                          "FrameGrabber", 
                                          0, 
                                          0, 
                                          0 )
mountConfig = TMCDB_IDL.AssemblyLocationIDL( "Mount", 
                                             "Mount", 
                                             0, 
                                             0, 
                                             0 )

subDeviceTypes = {'HoloRx': hrxConfig, 'HoloDSP': hdspConfig, 'PrototypeOpticalTelescope': otConfig,
                  'FrameGrabber': fgConfig, 'Mount': mountConfig}

def setupTMCDB(tmcdb, antennaName, subdevices):
    """Sets the TMCDB with a basic configuration.
    Parameters:
    antennaName - Antenna name.
    subdevices - List of subdevices, which can be:
                 'HoloRx',
                 'HoloDSP',
                 'PrototypeOpticalTelescope'
                 'FrameGrabber'
                 'Mount'
    """
    sd = []
    for dev in subdevices:
        sd.append(subDeviceTypes[dev])
    
    sai = TMCDB_IDL.StartupAntennaIDL( antennaName, 
                                      "padname", 
                                      "",
                                      0, 
                                      [], 
                                      sd )

    tmcdb.setStartupAntennasInfo([sai])

    ai = TMCDB_IDL.AntennaIDL(0,
                              0,
                              0,
                              antennaName,
                              "",
                              asdmIDLTypes.IDLLength(0.0),
                              asdmIDLTypes.IDLArrayTime(0),
                              asdmIDLTypes.IDLLength(0.0),
                              asdmIDLTypes.IDLLength(0.0),
                              asdmIDLTypes.IDLLength(0.0),
                              asdmIDLTypes.IDLLength(0.0),
                              asdmIDLTypes.IDLLength(0.0),
                              asdmIDLTypes.IDLLength(0.0),
                              0)
    tmcdb.setAntennaInfo(antennaName, ai)

    pi = TMCDB_IDL.PadIDL(0,
                          0,
                          0,
                          "padName",
                          asdmIDLTypes.IDLArrayTime(0),
                          asdmIDLTypes.IDLLength(0.0),
                          asdmIDLTypes.IDLLength(0.0),
                          asdmIDLTypes.IDLLength(0.0))
    tmcdb.setAntennaPadInfo(antennaName, pi)


if __name__ == "__main__":

    # Parse the options and arguments
    shortargspec = "s:"
    longargspec = ['antenna=', 'subdevices=']
    opts, args = getopt.getopt(sys.argv[1:], shortargspec, longargspec)

    optkeyw = map(lambda t: t[0], opts)
#    if '-h' in optkeyw or '--help' in optkeyw:
#        help()
#        sys.exit(0)

    if '--antenna' in optkeyw:
        antennaName = filter(lambda t: t[0] == '--antenna', opts)[0][1]
        print "Setting TMCDB for antenna '" + antennaName + "'"

    subDevices = []
    if '--subdevices' in optkeyw:
        temp = filter(lambda t: t[0] == '--subdevices', opts)[0][1]
        for dev in temp.split(' '):
            subDevices.append(dev.strip())
    
    client = PySimpleClient("setupTMCDB")
    tmcdb = client.getDefaultComponent(TMCDB_CURL)    
    setupTMCDB(tmcdb, antennaName, subDevices)
    print "TMCDB has been setup."
    
    client.disconnect()

#    
# __oOo__
