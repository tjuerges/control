#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

#import unittest
#from CCL.CorrelatorConfig import *
#from CCL.BaseBandConfig import *
#from CCL.Global import *

import unittest
import CCL.APDMSchedBlock as APDMSchedBlock
import APDMEntities.SchedBlock
from CCL.CorrelatorConfiguration import AbstractCorrelatorConfiguration
from CCL.CorrelatorConfiguration import BLCorrelatorConfiguration
from CCL.CorrelatorConfiguration import ACACorrelatorConfiguration

class BLCorrelatorConfigurationTest(unittest.TestCase):
    def testDefaultInstanciation(self):
        cc = BLCorrelatorConfiguration()
        self.assertTrue(isinstance(cc, BLCorrelatorConfiguration))
        self.assertTrue(isinstance(cc, AbstractCorrelatorConfiguration))
        self.assertTrue(isinstance(cc,
                                   APDMSchedBlock.BLCorrelatorConfiguration))
        self.assertTrue(isinstance(cc,
                         APDMEntities.SchedBlock.BLCorrelatorConfigurationT))

    def testConstructor(self):
        cc = BLCorrelatorConfiguration("1 min",
                                       "15 s",
                                       "AP_UNCORRECTED",
                                       "60 ms")
        self.assertAlmostEqual(cc.integrationDuration.get(), 59.904)
        self.assertAlmostEqual(cc.channelAverageDuration.get(), 14.976)
        self.assertAlmostEqual(cc.dumpDuration.get(), 0.064)
        self.assertEqual(cc.aPCDataSets, "AP_UNCORRECTED")

    def testRoundTimes(self):
        cc = BLCorrelatorConfiguration()
        cc.dumpDuration.set("1 ms")
        cc.channelAverageDuration.set("1 ms")
        cc.integrationDuration.set("1 ms")
        cc.roundTimes()
        self.assertAlmostEqual(cc.dumpDuration.get(), 0.016)
        self.assertAlmostEqual(cc.channelAverageDuration.get(), 0.016)
        self.assertAlmostEqual(cc.integrationDuration.get(), 0.016)

        cc.dumpDuration.set("12 ms")
        cc.channelAverageDuration.set("36 ms")
        cc.integrationDuration.set("1 ms")
        cc.roundTimes()
        self.assertAlmostEqual(cc.dumpDuration.get(), 0.016)
        self.assertAlmostEqual(cc.channelAverageDuration.get(), 0.032)
        self.assertAlmostEqual(cc.integrationDuration.get(), 0.032)

        cc.dumpDuration.set("16 ms")
        cc.channelAverageDuration.set("40 ms")
        cc.integrationDuration.set("80 ms")
        cc.roundTimes()
        self.assertAlmostEqual(cc.dumpDuration.get(), 0.016)
        self.assertAlmostEqual(cc.channelAverageDuration.get(), 0.048)
        self.assertAlmostEqual(cc.integrationDuration.get(), 0.096)

        cc.dumpDuration.set("18 ms")
        cc.roundTimes()
        self.assertAlmostEqual(cc.dumpDuration.get(), 0.016)
        
        cc.dumpDuration.set("30 ms")
        cc.roundTimes()
        self.assertAlmostEqual(cc.dumpDuration.get(), 0.032)
        
        cc.dumpDuration.set("40 ms")
        cc.roundTimes()
        self.assertAlmostEqual(cc.dumpDuration.get(), 0.048)
        
        cc.dumpDuration.set("98 ms")
        cc.roundTimes()
        self.assertAlmostEqual(cc.dumpDuration.get(), 0.096)
        
        cc.dumpDuration.set("190 ms")
        cc.roundTimes()
        self.assertAlmostEqual(cc.dumpDuration.get(), 0.192)
        
class ACACorrelatorConfigurationTest(unittest.TestCase):
    def testDefaultInstanciation(self):
        cc = ACACorrelatorConfiguration()
        self.assertTrue(isinstance(cc, ACACorrelatorConfiguration))
        self.assertTrue(isinstance(cc, AbstractCorrelatorConfiguration))
        self.assertTrue(isinstance(cc,
                                   APDMSchedBlock.ACACorrelatorConfiguration))
        self.assertTrue(isinstance(cc,
                         APDMEntities.SchedBlock.ACACorrelatorConfigurationT))

    def testConstructor(self):
        cc = ACACorrelatorConfiguration("1 min",
                                       "15 s",
                                       "AP_UNCORRECTED")
        self.assertAlmostEqual(cc.integrationDuration.get(), 60.032)
        self.assertAlmostEqual(cc.channelAverageDuration.get(), 15.008)
        self.assertEqual(cc.aPCDataSets, "AP_UNCORRECTED")

    def testRoundTimes(self):
        cc = ACACorrelatorConfiguration()
        cc.channelAverageDuration.set("1 ms")
        cc.integrationDuration.set("1 ms")
        cc.roundTimes()
        self.assertAlmostEqual(cc.channelAverageDuration.get(), 0.016)
        self.assertAlmostEqual(cc.integrationDuration.get(), 0.016)

        cc.channelAverageDuration.set("36 ms")
        cc.integrationDuration.set("1 ms")
        cc.roundTimes()
        self.assertAlmostEqual(cc.channelAverageDuration.get(), 0.032)
        self.assertAlmostEqual(cc.integrationDuration.get(), 0.032)

        cc.channelAverageDuration.set("40 ms")
        cc.integrationDuration.set("80 ms")
        cc.roundTimes()
        self.assertAlmostEqual(cc.channelAverageDuration.get(), 0.048)
        self.assertAlmostEqual(cc.integrationDuration.get(), 0.096)



if __name__ == "__main__":
    BLSuite  = unittest.makeSuite(BLCorrelatorConfigurationTest,'test')
    ACASuite = unittest.makeSuite(ACACorrelatorConfigurationTest,'test')
    alltests = unittest.TestSuite((BLSuite, ACASuite))
    unittest.TextTestRunner(verbosity=2).run(alltests)
