from CCL.Container import getComponent
from CCL.logging import getLogger

class Foo:

    def __init__(self, compName):
        self.logger = getLogger()
        self.logger.logInfo("Foo: Initializing component...")
        
        self.logger.logDebug("Getting component "+compName)
        self._foo = getComponent(compName)
