from CCL.Container import getComponent
from CCL.logging import getLogger

class Bar:
    def __init__(self, compName):
        self.logger = getLogger()
        self.logger.logTrace("Bar: Initializing CCL object...")
        self.logger.logDebug("Getting component "+compName)
        self._bar = getComponent(compName)
