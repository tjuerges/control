#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import unittest
from CCL.SquareLawConfig import *
from CCL.Global import *

class SquareLawConfigTest(unittest.TestCase):

    #Test setting the Integration Duration
    def integrationDurationTest(self, sqConf):
        currentDuration = sqConf.getIntegrationDuration()
        sqConf.setIntegrationDuration("%f min" % currentDuration)
        self.failIf(abs(sqConf.getIntegrationDuration() -
                        (currentDuration * 60) > 0.0001),
                    "Unable to correctly set integration duration!")
        
    def testIntegrationDuration(self):
        self.integrationDurationTest(SquareLawConfig())

    def testintegrationDurationSB(self):
        loadSB('schedblks/SpectralSpecTPTest.xml')
        sqConf = getSB().schedblock.SchedBlock.SpectralSpec.SquareLawSetup
        self.integrationDurationTest(SquareLawConfig(sqConf))
        
def suite():
    alltests =  unittest.makeSuite(SquareLawConfigTest,'test')
    return alltests


if __name__ == "__main__":
    unittest.main(defaultTest='suite')
