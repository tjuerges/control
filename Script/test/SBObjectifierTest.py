#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# rhiriart  2006-06-06  created
#

import unittest
import glob
from ScriptExec.sb import SBObjectifier
import Control
import ScanIntentMod

#-------------------------------------------------------------------------------
class SBObjectifierTest(unittest.TestCase):
    """
    Test cases for the SBObjectifier class.
    """

    #---------------------------------------------------------------------------
    def setUp(self):
        """
        Sets up the test case 'fixture'.
        
        This method is called before each test case method.
        """
        
        self.SCHEDBLKS_DIR = "schedblks"
        
        # Read each one of the files in the scheduling block test file
        # directory and leave their contents in the xmlContents map.
        fileNames = glob.glob(self.SCHEDBLKS_DIR + "/*.xml")
        self.xmlContents = {}
        for file in fileNames:
            f = open(file)
            xml = f.read()
            f.close()
            self.xmlContents[file] = xml
        
    #---------------------------------------------------------------------------
    def tearDown(self):
        """
        Tear down the test case 'fixture'
        
        This method is called after each test case method.
        """
        pass

    #---------------------------------------------------------------------------
    def testSBCreation(self):
    
        # this one is for optical pointing
        # f1 = open("schedblks/SixBrightPolarStars.xml")
        # this one is for shared simulator
        f1 = open("schedblks/oneTargetSB.xml")
        xml = f1.read()
        f1.close()

        objectifier = SBObjectifier(xml)
        sbObj = objectifier.getSB()

        # Performing the same operation that are in the scheduling block
        # print "Number of correlator configurations = " + str(len(objectifier.aggregate(sbObj.SchedBlock.SpectralSpec.CorrelatorConfig)))
        # print "Number of spectral specifications = " + str(len(objectifier.aggregate(sbObj.SchedBlock.SpectralSpec)))
        print "Number of sources = " + str(len(objectifier.aggregate(sbObj.SchedBlock.FieldSource)))

        for target in objectifier.aggregate(sbObj.SchedBlock.Target):
            print "Purposes = " + str(objectifier.getPurposes(target))

            # get the spectral specification
            spectralSpecId = target.abstractInstrumentSpecId.getValue()
            (type, spectralSpec) = objectifier.getAbstractInstrumentSpec(spectralSpecId)
            spectSpecIDL = None
            if type == "SpectralSpec":
                spectSpecIDL = objectifier.getSpectralSpecIDL(spectralSpec)

                # get the correlator configuration
                corrConf = spectralSpec.CorrelatorConfig
                corrConfIDL = objectifier.getCorrelatorConfigurationIDL(corrConf)
                
                objectifier.printCorrelatorConfigurationIDL(corrConfIDL)
                objectifier.printSpectralSpecIDL(spectSpecIDL)

            # get the source
            sourceId = target.fieldSourceId.getValue()
            source = objectifier.getFieldSource(sourceId)
            sourceIDL = objectifier.getSourceIDL(source)
            print "Source name = " + source.sourceName.getValue()
            print "(RA, DEC) = (" + str(source.sourceCoordinates.longitude.getValue()) + \
                ", " + str(source.sourceCoordinates.latitude.getValue()) + ")"

    #---------------------------------------------------------------------------
    def testPurposes(self):
        """
        Test SBObjectifier getPurposes() function.
        
        TODO: This test assumes that each target has only one purpose. Extend
        this to multi-purpose targets.        
        """

        # Map with the expected purposes for each target in the sheduling block
        # files. 
#        expPurposes = {self.SCHEDBLKS_DIR + "/1PhaseCalTargetSB.xml" : ["PHASECAL"],
#                       self.SCHEDBLKS_DIR + "/1ObsTargetSB.xml" : ["TARGET"],
#                       self.SCHEDBLKS_DIR + "/2ObsTargetsSB.xml" : ["TARGET", "TARGET"],
#                       self.SCHEDBLKS_DIR + "/1OptPointingTargetSB.xml" : ["POINTINGMODEL"]}
        
#        for fname in self.xmlContents.keys():
#            sbobj = SBObjectifier(self.xmlContents[fname])
#            sb = sbobj.getSB()
#            targets = sbobj.aggregate(sb.SchedBlock.Target)
#            t = 0
#            for t in range(len(targets)):
#                purposes = sbobj.getPurposes(targets[t])
#                self.assertEquals(expPurposes[fname][t], str(purposes[0]))
#                print fname + " > " + str(purposes)
#                t = t + 1
        f1 = open("schedblks/SixBrightPolarStars.xml")
        xml = f1.read()
        f1.close()
        
        sbobj = SBObjectifier(xml)
        sb = sbobj.getSB()
        targets = sbobj.aggregate(sb.SchedBlock.Target)
        for target in targets:
            purposes = sbobj.getPurposes(target)
            self.assertEquals([ScanIntentMod.POINTING_MODEL], purposes)


    #---------------------------------------------------------------------------
    def testGetInstrumentSpec(self):
        """
        Test SBObjectifier function getAbstractInstrumentSpec().
        """
        
        f1 = open("schedblks/SixBrightPolarStars.xml")
        xml = f1.read()
        f1.close()
        
        sbobj = SBObjectifier(xml)
        sb = sbobj.getSB()
        
        targets = sbobj.aggregate(sb.SchedBlock.Target)
        for target in targets:
            (instrType, instrSpec) = sbobj.getAbstractInstrumentSpec(target.abstractInstrumentSpecId.getValue())
            if instrType == "OpticalCameraSpec":
                self.assertEquals(2.0, instrSpec.minIntegrationTime.getValue())
                # Unfortunately, XmlObjectifier defines the 'name' attribute
                # (see XmlObjectifier.py line 325, in the acspy ACS module)
                # so it is necessary to use the low level dom interfaces to get
                # the name element. 
                for node in instrSpec.childNodes:
                    if node.nodeName == "name":
                        if len(node.childNodes) == 1:
                            name = node.childNodes[0].data
                        else:
                            name = ""
                        self.assertEquals("trcOP.scat.6.polar.stars.bright", name)
                # TODO complete the rest of the cases

    #---------------------------------------------------------------------------
    def testGetObservingParameters(self):
        """
        Test SBObjectifier function getObservingParameters().
        """

        f1 = open("schedblks/SixBrightPolarStars.xml")
        xml = f1.read()
        f1.close()
        
        sbobj = SBObjectifier(xml)
        sb = sbobj.getSB()
        
        targets = sbobj.aggregate(sb.SchedBlock.Target)
        for target in targets:
            obsParamDoc = sbobj.getObservingParameters(target.observingParametersIdList.getValue())
            # the document should have only one single node
            self.assertEquals(1, len(obsParamDoc.childNodes))
            if obsParamDoc.childNodes[0].nodeName == "OpticalPointingParameters":
                obsParam = obsParamDoc.OpticalPointingParameters
            self.assertEquals(0.1, obsParam.antennaPositionTolerance.getValue())
            self.assertEquals(15.0, obsParam.elevationLimit.getValue())
            self.assertEquals(0.0, obsParam.maxMagnitude.value.getValue())            
            self.assertEquals(13.0, obsParam.minMagnitude.value.getValue())            

    #---------------------------------------------------------------------------
    def testGetFieldSource(self):
        """
        Test SBObjectifier function getFieldSource().
        """
        
        sourceNames = ["Optical40_Cas_456",
                       "OpticalOri_1686", 
                       "Opticalalp_UMa_4301", 
                       "Opticalbeta_UMi_5563",
                       "Opticaldel_Dra_7310",
                       "Opticaldel_Aur_2077"]
        
        f1 = open("schedblks/SixBrightPolarStars.xml")
        xml = f1.read()
        f1.close()
        
        sbobj = SBObjectifier(xml)
        sb = sbobj.getSB()
        
        targets = sbobj.aggregate(sb.SchedBlock.Target)
        t = 0
        for target in targets:
            source = sbobj.getFieldSource(target.fieldSourceId.getValue())
            self.assertEquals(0.0, source.pMRA.getValue())
            # Unfortunately, XmlObject hides the 'name' attribute
            # (see XmlObjectifier.py, line 325, in the acspy ACS module)
            # so it is necessary to use the low level DOM interfaces to get
            # the name element. 
            for node in source.childNodes:
                if node.nodeName == "name":
                    if len(node.childNodes) == 1:
                        name = node.childNodes[0].data
                    else:
                        name = ""
                    self.assertEquals(sourceNames[t], name)
            t = t + 1
            # etc.

    #---------------------------------------------------------------------------
    def testGetHolographyParameters(self):
        """
        Test the way of getting HolographyParameters from an holography SB.
        """
        
        f1 = open("schedblks/HolographySB.xml")
        xml = f1.read()
        f1.close()
        
        sbobj = SBObjectifier(xml)
        sb = sbobj.getSB()
        
        self.assertEquals(104.0, sb.SchedBlock.HolographyParameters.frequency.getValue(), 0.01)
        self.assertEquals(20.0, sb.SchedBlock.HolographyParameters.resolution.getValue(), 0.01)
        self.assertEquals(0.0, sb.SchedBlock.HolographyParameters.startFraction.getValue(), 0.01)
        self.assertEquals(200.0, sb.SchedBlock.HolographyParameters.speed.getValue(), 0.01)
        self.assertEquals(5, sb.SchedBlock.HolographyParameters.rowsCal.getValue())
        self.assertEquals(60.0, sb.SchedBlock.HolographyParameters.calTime.getValue(), 0.01)

        # Unfortunately, XmlObjectifier defines the 'name' attribute
        # (see XmlObjectifier.py line 325, in the acspy ACS module)
        # so it is necessary to use the low level dom interfaces to get
        # the name element.
        name = "" 
        for node in sb.SchedBlock.HolographyParameters.childNodes:
            if node.nodeName == "name":
                if len(node.childNodes) == 1:
                    name = node.childNodes[0].data
                else:
                    name = ""
        self.assertEquals("Holography (Default values)", name)
        
        
def suite():
    suite = unittest.TestSuite()
    suite.addTest(SBObjectifierTest("testSBCreation"))
    suite.addTest(SBObjectifierTest("testPurposes"))
    suite.addTest(SBObjectifierTest("testGetInstrumentSpec"))
    suite.addTest(SBObjectifierTest("testGetObservingParameters"))
    suite.addTest(SBObjectifierTest("testGetFieldSource"))
    suite.addTest(SBObjectifierTest("testGetHolographyParameters"))
    return suite

if __name__ == '__main__':
    unittest.main(defaultTest='suite')


#
# ___oOo___
