#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# rhiriart  2006-11-13  created
#

import shutil
shutil.rmtree("../lib/python/site-packages/Control__POA", True)
shutil.rmtree("../lib/python/site-packages/Control", True)

from Acspy.Clients.SimpleClient import PySimpleClient
from ScriptExec.Executor import Executor
from ScriptExec.ResourceManager import ResourceManager
import unittest

class ExecutorTest(unittest.TestCase):
    
    def setUp(self):
        self.client = PySimpleClient()
        self.resMng = ResourceManager()
        self.executor = Executor(self.client)
        self.executor.setArrayName("AutomaticArrayMock")
        self.simulator = self.client.getComponent("SIMULATOR")

    def tearDown(self):
        self.client.releaseComponent(self.simulator._get_name())
    
    def testRunSource(self):
        script = "print 'Hello, world'"
        result = self.executor.runSource(script)
        self.assertEquals("Hello, world\n", result)

    def testGetArray(self):
        simCode = "['DV01', 'DA41']"
        self.simulator.setMethod('CONTROL/AutomaticArrayMock', 'getAntennas', simCode, 0)
        
        script = """array = getArray()
antennas = array.antennas()
print antennas
"""     
        result = self.executor.runSource(script)
        self.assertEquals("['DV01', 'DA41']\n", result)

    def testManualMode(self):
        script = "for i in range(3):\n"
        needs_more = self.executor.compileScript(script)
        self.assertEquals(True, needs_more)
        script = script + "   print i\n"
        needs_more = self.executor.compileScript(script)
        self.assertEquals(False, needs_more)
        result = self.executor.executeScript()
        self.assertEquals("0\n1\n2\n", result)

    def testSetSB(self):
        f = open("schedblks/SixBrightPolarStars.xml")
        xml = f.read()
        self.executor.setSB(xml)
        script = """sb = getSB()
print sb.SchedBlock.name.getValue()"""
        result = self.executor.runSource(script)
        self.assertEquals("trcOP.scat.6.polar.stars.bright\n", result)


        
def suite():
    suite = unittest.TestSuite()
    suite.addTest(ExecutorTest("testRunSource"))
    suite.addTest(ExecutorTest("testGetArray"))
    suite.addTest(ExecutorTest("testManualMode"))
    suite.addTest(ExecutorTest("testSetSB"))
    return suite

if __name__ == '__main__':
    unittest.main(defaultTest='suite')

#
# ___oOo___    
