#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#

import shutil
shutil.rmtree("../lib/python/site-packages/Control__POA", True)
shutil.rmtree("../lib/python/site-packages/Control", True)

import unittest
import CCL.SSRSetup
import CCL.Foo

from CCL.simulation import addMock
from CCL.logging import getLogger

# Change the ACSCorba.getManager function so it doesn't block
# if we try to run this script without ACS running.
class Manager:
    def login(self, foo): pass
def getManager(): return Manager()
import Acspy.Util.ACSCorba
Acspy.Util.ACSCorba.getManager = getManager

class ResourceManagerSSRCCLTest(unittest.TestCase):
    
    def setUp(self): pass
        
    def tearDown(self): pass

    def testCreateCCLObject(self):
        class FooMock: pass
        addMock("FOO", FooMock())
        foo = CCL.Foo.Foo("FOO")
        
    def testUseLogger(self):
        logger = getLogger()
        logger.logInfo("Hello world!")

def suite():
    suite = unittest.TestSuite()
    suite.addTest(ResourceManagerSSRCCLTest("testCreateCCLObject"))
    suite.addTest(ResourceManagerSSRCCLTest("testUseLogger"))
    return suite

if __name__ == '__main__':
    unittest.main(defaultTest='suite')
