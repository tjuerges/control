#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# rhiriart  2006-12-06  created
#

import shutil
shutil.rmtree("../lib/python/site-packages/Control__POA", True)
shutil.rmtree("../lib/python/site-packages/Control", True)

import sys
import traceback
import unittest
import thread
import time

from Acspy.Clients.SimpleClient import PySimpleClient
from SBExecCallback import SBExecCallbackImpl

import Control
import ScriptExecutorExceptions
import ScriptExecutorExceptionsImpl

class ScriptExecutorTest(unittest.TestCase):

    def setUp(self):        
        self.client = PySimpleClient()
        self.script = self.client.getComponent("CONTROL/SCRIPT")
        self.logger = self.client.getLogger()
        self.aborted = False
        self.abort_mutex = thread.allocate_lock()

    def tearDown(self):
        self.client.releaseComponent("CONTROL/SCRIPT")
        self.client.disconnect()

    def testConfigure(self):
        self.script.configure("CONTROL/AutomaticArrayMock", "<SchedBlock/>")

    def testRunSource(self):
        self.script.configure("CONTROL/AutomaticArrayMock", "<SchedBlock/>")
        
        script = "print 'Hello, world'"
        result = self.script.runObservationScript(script)
        self.assertEquals("Hello, world\n", result)

    def testSyntaxErrorEx(self):
        self.script.configure("CONTROL/AutomaticArrayMock", "<SchedBlock/>")
        
        script = "a="
        try:
            result = self.script.runObservationScript(script)
        except ScriptExecutorExceptions.ExecutionErrorEx, ex:
            err_hlp = ScriptExecutorExceptionsImpl.ExecutionErrorExImpl(exception=ex)
            # err_hlp.log()
            return
        except:
            (ex_type, ex_value, ex_tb) = sys.exc_info()
            self.fail("Other exception was received: " + str(ex_value))            
        self.fail("No exception was received at all!")
        
    def testRunSourceOtherEx(self):
        """Test the runSource() function when an exception (NameError) is thrown. 
        """
        self.script.configure("CONTROL/AutomaticArrayMock", "<SchedBlock/>")
        self.assertRaises(ScriptExecutorExceptions.ExecutionErrorEx,
                          self.script.runObservationScript,
                          "print jim")
        
    def testSetSB(self):
        """Test the setXMLSB() function.
        """
        
        f = open("schedblks/Orion-SFI-Test.xml")
        xml = f.read()
        self.script.configure("CONTROL/AutomaticArrayMock", xml)
        
        script = """sb = getSB()
targets = sb.SchedBlock.Target
source = sb.getFieldSource(targets[0])
print "%.1f %.1f" % (source.sourceCoordinates.longitude.getValue(), source.sourceCoordinates.latitude.getValue())
"""
        result = self.script.runObservationScript(script)
        self.assertEquals("83.8 -5.4\n", result)                

    def testExecutionAbortion(self):
        """Test the runObservationScript function, which executes a script in a
        different process.
        In this test an infinite execution is aborted.
        """
        self.aborted = False
        self.script.configure("CONTROL/AutomaticArrayMock", "<SchedBlock/>")
        
        script = """from CCL.logging import getLogger
logger = getLogger()
i = 0
while 1:
   i = i + 1
   if i % 100000 == 0:
      logger.logInfo("n = "+ str(i))
"""
        thread.start_new_thread(self.__callRunObservationScript, (script,))

        time.sleep(5) # allow the process to execute for some time
        self.script.abort()
        while self.abort_mutex.locked(): pass
        self.assertTrue(self.aborted)

    def __callRunObservationScript(self, script):
        try:
            self.abort_mutex.acquire()
            result = self.script.runObservationScript(script)
            self.aborted = True
            self.abort_mutex.release()
        except ScriptExecutorExceptions.ExecutionErrorEx, ex:
            err_hlp = ScriptExecutorExceptionsImpl.ExecutionErrorExImpl(exception=ex)
            err_hlp.log()
            
    def testExecAbortionObjectCleanup(self):
        
        self.script.configure("CONTROL/AutomaticArrayMock", "<SchedBlock/>")        
        script = """import signal
from CCL.TestMC import TestMC
from CCL.TestOM import TestOM
om = TestOM()
def om_interrupt():
    import os
    f = open(os.getenv('ACSDATA')+"/tmp/om_interrupted", "w")
    f.close()
om.interrupt = om_interrupt
mc = TestMC()
def mc_interrupt():
    import os
    f = open(os.getenv('ACSDATA')+"/tmp/mc_interrupted", "w")
    f.close()
mc.interrupt = mc_interrupt
signal.pause()
"""
        thread.start_new_thread(self.__callRunObservationScript, (script,))

        time.sleep(5) # allow the process to execute for some time
        self.script.abort()
        time.sleep(3) # give it some time to complete the abortion
        import glob, os
        self.assertTrue(len(glob.glob(os.getenv('ACSDATA')+"/tmp/om_interrupted"))==1)
        self.assertTrue(len(glob.glob(os.getenv('ACSDATA')+"/tmp/mc_interrupted"))==1)
        os.unlink(os.getenv('ACSDATA')+"/tmp/om_interrupted")
        os.unlink(os.getenv('ACSDATA')+"/tmp/mc_interrupted")

    def testExecAbortionGeneralCleanup(self):

        self.script.configure("CONTROL/AutomaticArrayMock", "<SchedBlock/>")                
        
        script = """import signal
def __abort__():
    import os
    f = open(os.getenv('ACSDATA')+"/tmp/script_interrupted", "w")
    f.close()
signal.pause()
"""
        thread.start_new_thread(self.__callRunObservationScript, (script,))

        time.sleep(5) # allow the process to execute for some time
        self.script.abort()
        time.sleep(3) # give it some time to complete the abortion
        import glob, os
        self.assertTrue(len(glob.glob(os.getenv('ACSDATA')+"/tmp/script_interrupted"))==1)
        os.unlink(os.getenv('ACSDATA')+"/tmp/script_interrupted")
        
    def testLoggingMethods(self):
        
        self.script.configure("CONTROL/AutomaticArrayMock", "<SchedBlock/>")                
        
        script = """from CCL.logging import getLogger
logger = getLogger()
logger.logAlert('logAlert here')
logger.logCritical('logCritical here')
logger.logDebug('logDebug here')
logger.logEmergency('logEmergency here')
logger.logInfo('logInfo here')
logger.logNotice('logNotice here')
logger.logTrace('logTrace here')
logger.logWarning('logWarning here')
logger.logXML('logXML here')"""
        
        try:
            result = self.script.runObservationScript(script)
        except ScriptExecutorExceptions.ExecutionErrorEx, ex:
            ex_hlp = ScriptExecutorExceptionsImpl.ExecutionErrorExImpl(exception=ex)
            ex_hlp.log(self.logger)
            
    def testManualMode(self):
        self.script.configure("CONTROL/AutomaticArrayMock", "<SchedBlock/>")        
        script = """def f():"""
        incmpl = self.script.compileScript(script)
        if not incmpl: self.script.executeScript()

    def testRunObservationScriptAsync(self):
        """Test the runSourceAsync function, which executes a script asynchronously.
        """
        self.script.configure("CONTROL/AutomaticArrayMock", "<SchedBlock/>")        
        
        cbImpl = SBExecCallbackImpl()
        cb = self.client.activateOffShoot(cbImpl)
        
        script = "print 'Hello, world'"        
        self.script.runObservationScriptAsync(script, cb)
        counter = 0
        timeout = 10 # timeout in seconds
        while cbImpl.completion is None:
            time.sleep(1)
            counter = counter + 1
            if counter >= timeout: break
        self.assertNotEquals(None, cbImpl.completion)
        self.assertEquals(0, len(cbImpl.completion.previousError))
        
    def testRunObservationScriptAsyncWithError(self):
        """Test the runSourceAsync function, which executes a script asynchronously,
        when an exception is thrown.
        """
        self.script.configure("CONTROL/AutomaticArrayMock", "<SchedBlock/>")        
        
        cbImpl = SBExecCallbackImpl()
        cb = self.client.activateOffShoot(cbImpl)

        script = """import ControlExceptionsImpl
raise ControlExceptionsImpl.CAMBErrorExImpl()
"""
        self.script.runObservationScriptAsync(script, cb)
        counter = 0
        timeout = 10 # timeout in seconds        
        while cbImpl.completion is None:
            time.sleep(1)
            counter = counter + 1
            if counter >= timeout: break
        self.assertNotEquals(None, cbImpl.completion)
        self.assertEquals(1, len(cbImpl.completion.previousError))
        
def suite():
    suite = unittest.TestSuite()
    suite.addTest(ScriptExecutorTest("testConfigure"))
    suite.addTest(ScriptExecutorTest("testRunSource"))
    suite.addTest(ScriptExecutorTest("testSyntaxErrorEx"))
    suite.addTest(ScriptExecutorTest("testRunSourceOtherEx"))
    suite.addTest(ScriptExecutorTest("testSetSB"))
    suite.addTest(ScriptExecutorTest("testExecutionAbortion"))
    suite.addTest(ScriptExecutorTest("testExecAbortionObjectCleanup"))
    suite.addTest(ScriptExecutorTest("testExecAbortionGeneralCleanup"))
    suite.addTest(ScriptExecutorTest("testLoggingMethods"))
    suite.addTest(ScriptExecutorTest("testManualMode"))
    suite.addTest(ScriptExecutorTest("testRunObservationScriptAsync"))
    suite.addTest(ScriptExecutorTest("testRunObservationScriptAsyncWithError"))
    return suite

if __name__ == '__main__':
    unittest.main(defaultTest='suite')

#
# ___oOo___
