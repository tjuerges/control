#!/usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import unittest
import math
import CCL.FieldSource
from Acspy.Common.TimeHelper import getTimeStamp

class PlanetSourceTest(unittest.TestCase):
    def testMajorBodies(self):
        targetList = ["Mercury", "Venus", "Moon", "Mars", "Jupiter",
                      "Saturn", "Uranus", "Neptune", "Pluto", "Sun"]
        for target in targetList:
            src = CCL.FieldSource.PlanetSource(target)
            self.assertEqual(src.sourceName, target)
            self.assertEqual(src.solarSystemObject, unicode(target))
            self.assertTrue(src.nonSiderealMotion)
            self.assertEqual(src.sourceEphemeris, 'None')

    def testEphemeris(self):
        targetList =  ["Io", "Callisto", "Europa", "Ganymede", "Titan",
                       "Ceres", "Pallas", "Juno", "Vesta"]
        for target in targetList:
            src = CCL.FieldSource.PlanetSource(target)
            self.assertEqual(src.sourceName, target)
            self.assertEqual(src.solarSystemObject, u'Ephemeris')
            self.assertTrue(src.nonSiderealMotion)
            ephem = src.sourceEphemeris.split(';')
            ephem.remove('') #We get a blank at the end from the last ';'
            self.assertEqual(len(ephem), 58)
            currentTime = getTimeStamp().value
            for line in ephem:
                splitline = line.split(',')
                self.assertEqual(len(splitline),4)
                self.assertTrue(abs(long(splitline[0]) - currentTime) <
                                936000000000L) #26 Hours
                self.assertTrue(float(splitline[1]) >= 0 and
                                float(splitline[1]) < (2*math.pi))
                self.assertTrue(float(splitline[2]) >= -math.pi and
                                float(splitline[2]) <= math.pi)

def suite():
    PlanetSuite = unittest.makeSuite(PlanetSourceTest,'test')
    alltests = unittest.TestSuite((PlanetSuite))
    return alltests

if __name__ == "__main__":
    unittest.main(defaultTest='suite')
