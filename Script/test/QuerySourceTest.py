#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002
# (c) European Southern Observatory, 2002
#
# @(#) $Id$
#------------------------------------------------------------------------------


import unittest
import math

import CCL.APDMSchedBlock
import CCL.APDMValueTypes

import APDMEntities.SchedBlock


class QuerySourceTest(unittest.TestCase):
    def testInstanciation(self):
        qs = CCL.APDMSchedBlock.QuerySource()
        self.assertTrue(isinstance(qs, CCL.APDMSchedBlock.QuerySource))
        self.assertTrue(isinstance(qs, APDMEntities.SchedBlock.QuerySourceT))

    def testDefaultValues(self):
        qs = CCL.APDMSchedBlock.QuerySource()
        self.assertNotEqual(qs.queryCenter, None)
        self.assertEqual(qs.searchRadius.get(), 0.0)
        self.assertEqual(qs.minFrequency.get(), 0.0)
        self.assertEqual(qs.maxFrequency.get(), 0.0)
        self.assertEqual(qs.minFlux.get(), 0.0)
        self.assertEqual(qs.maxFlux.get(), 0.0)
        self.assertEqual(qs.minTimeSinceObserved.get(), 0.0)
        self.assertEqual(qs.maxTimeSinceObserved.get(), 0.0)
        self.assertEqual(qs.maxSources, 0.0)

    def testInstanciationFromXML(self):
        defaultXML = '''<?xml version="1.0" ?>
        <ns1:QuerySource intendedUse="Amplitude"
                          xmlns:ns1="Alma/ObsPrep/SchedBlock"
                          xmlns:ns2="Alma/ValueTypes">
          <ns1:queryCenter system="J2000">
            <ns2:longitude unit="deg">127.5</ns2:longitude>
            <ns2:latitude unit="deg">-5.62</ns2:latitude>
            <ns2:fieldName>RandomField</ns2:fieldName>
          </ns1:queryCenter>
          <ns1:searchRadius unit="deg">10.0</ns1:searchRadius>
          <ns1:minFrequency unit="MHz">123.0</ns1:minFrequency>
          <ns1:maxFrequency unit="GHz">456.0</ns1:maxFrequency>
          <ns1:minFlux unit="uJy">10.0</ns1:minFlux>
          <ns1:maxFlux unit="mJy">0.1</ns1:maxFlux>
          <ns1:minTimeSinceObserved unit="d">10.0</ns1:minTimeSinceObserved>
          <ns1:maxTimeSinceObserved unit="d">100.0</ns1:maxTimeSinceObserved>
          <ns1:use>True</ns1:use>
          <ns1:maxSources>6</ns1:maxSources>
        </ns1:QuerySource>
        '''
        qs = CCL.APDMSchedBlock.QuerySource.CreateFromDocument(defaultXML)
        self.assertTrue(isinstance(qs, CCL.APDMSchedBlock.QuerySource))

        # Check that the Fields are all the wrapped Classes
        self.assertTrue(isinstance(qs.queryCenter,
                                   CCL.APDMValueTypes.SkyCoordinates))
        self.assertTrue(isinstance(qs.searchRadius,
                                   CCL.APDMValueTypes.Angle))
        self.assertTrue(isinstance(qs.minFrequency,
                                   CCL.APDMValueTypes.Frequency))
        self.assertTrue(isinstance(qs.maxFrequency,
                                   CCL.APDMValueTypes.Frequency))
        self.assertTrue(isinstance(qs.minFlux, CCL.APDMValueTypes.Flux))
        self.assertTrue(isinstance(qs.maxFlux, CCL.APDMValueTypes.Flux))

        self.assertTrue(isinstance(qs.minTimeSinceObserved,
                                   CCL.APDMValueTypes.Time))
        self.assertTrue(isinstance(qs.maxTimeSinceObserved,
                                   CCL.APDMValueTypes.Time))

        # Now check that the values come correctly from the XML
        self.assertAlmostEqual(qs.queryCenter.longitude.get(),
                               math.radians(127.5))
        self.assertAlmostEqual(qs.queryCenter.latitude.get(),
                               math.radians(-5.62))
        self.assertEqual(qs.queryCenter.fieldName, u'RandomField')
        self.assertEqual(qs.searchRadius.get(), math.radians(10))
        self.assertAlmostEqual(qs.minFrequency.get(), 123.0E6)
        self.assertAlmostEqual(qs.maxFrequency.get(), 456.0E9)
        self.assertAlmostEqual(qs.minFlux.get(), 10E-6)
        self.assertAlmostEqual(qs.maxFlux.get(), 1E-4)
        self.assertEqual(qs.minTimeSinceObserved.get(), 864000)
        self.assertEqual(qs.maxTimeSinceObserved.get(), 8640000)
        self.assertEqual(qs.maxSources, 6)
        
if __name__ == "__main__":
    suite = unittest.makeSuite(QuerySourceTest, 'test')
    unittest.TextTestRunner(verbosity=2).run(suite)
