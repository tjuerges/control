#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import unittest
import CCL.APDMSchedBlock as APDMSchedBlock
import APDMEntities.SchedBlock
from CCL.SpectralWindow import AbstractSpectralWindow
from CCL.SpectralWindow import BLSpectralWindow
from CCL.SpectralWindow import ACASpectralWindow

class BLSpectralWindowTest(unittest.TestCase):
    def testDefaultInstanciation(self):
        sw = BLSpectralWindow()
        self.assertTrue(isinstance(sw, BLSpectralWindow))
        self.assertTrue(isinstance(sw, AbstractSpectralWindow))
        self.assertTrue(isinstance(sw, APDMSchedBlock.BLSpectralWindow))
        self.assertTrue(isinstance(sw,
                                   APDMEntities.SchedBlock.BLSpectralWindowT))

    def testConstructor(self):
        sw = BLSpectralWindow(CenterFrequency = "93.2 GHz",
                              EffectiveBandwidth = "1.6 GHz",
                              EffectiveNumberOfChannels = 1234,
                              SideBand = 'LSB',
                              PolarizationProducts = 'XX,YY,XY,YX',
                              ChannelAverageRegion = None)

        self.assertAlmostEqual(sw.centerFrequency.get(),93.2E9)
        self.assertAlmostEqual(sw.effectiveBandwidth.get(),1.6E9)
        self.assertEqual(sw.effectiveNumberOfChannels, 1234)
        self.assertEqual(sw.sideBand, 'LSB')
        self.assertEqual(sw.polnProducts, 'XX,YY,XY,YX')
        


class ACASpectralWindowTest(unittest.TestCase):
    def testDefaultInstanciation(self):
        sw = ACASpectralWindow()
        self.assertTrue(isinstance(sw, ACASpectralWindow))
        self.assertTrue(isinstance(sw, AbstractSpectralWindow))
        self.assertTrue(isinstance(sw, APDMSchedBlock.ACASpectralWindow))
        self.assertTrue(isinstance(sw,
                                   APDMEntities.SchedBlock.ACASpectralWindowT))

    def testConstructor(self):
        sw = ACASpectralWindow(CenterFrequency = "93.2 GHz",
                              EffectiveBandwidth = "1.6 GHz",
                              EffectiveNumberOfChannels = 1234,
                              SideBand = 'LSB',
                              PolarizationProducts = 'XX,YY,XY,YX',
                              ChannelAverageRegion = None)

        self.assertAlmostEqual(sw.centerFrequency.get(),93.2E9)
        self.assertAlmostEqual(sw.effectiveBandwidth.get(),1.6E9)
        self.assertEqual(sw.effectiveNumberOfChannels, 1234)
        self.assertEqual(sw.sideBand, 'LSB')
        self.assertEqual(sw.polnProducts, 'XX,YY,XY,YX')

if __name__ == "__main__":
    BLSuite  = unittest.makeSuite(BLSpectralWindowTest,'test')
    ACASuite = unittest.makeSuite(ACASpectralWindowTest,'test')
    alltests = unittest.TestSuite((BLSuite, ACASuite))
    unittest.TextTestRunner(verbosity=2).run(alltests)
