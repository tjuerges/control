#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#

import shutil
shutil.rmtree("../lib/python/site-packages/Control__POA", True)
shutil.rmtree("../lib/python/site-packages/Control", True)

import unittest
import sys
import os
import pickle
import logging
import ACSSim
from Acspy.Clients.SimpleClient import PySimpleClient
from ScriptExec.util import print_code


class SBExecProcessTest(unittest.TestCase):
    
    def setUp(self):
        self.client = PySimpleClient()
        
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.DEBUG)
        self.handler = logging.StreamHandler()
        self.formatter = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')
        self.handler.setFormatter(self.formatter)
        self.logger.addHandler(self.handler)

        self.scriptExecName = "CONTROL/SCRIPT_EXECUTOR_MOCK"
        self.arrayName = "CONTROL/AutomaticArrayMock"
        self.schedBlock = "<SchedBlock/>"

        self.simulator = self.client.getComponent("SIMULATOR")
        script = """LOGGER.logInfo('setExecResult called...')
LOGGER.logInfo('Result: ' + parameters[0])
setGlobalData('Result', parameters[0])
None"""
        self.simulator.setMethod('CONTROL/SCRIPT_EXECUTOR_MOCK', 'setExecResult', script, 0)
        script = """LOGGER.logInfo('setExecError called...')
import pickle
setGlobalData('Error', pickle.dumps(parameters[0]))
None"""
        self.simulator.setMethod('CONTROL/SCRIPT_EXECUTOR_MOCK', 'setExecError', script, 0)
        
    def tearDown(self):
        self.client.releaseComponent(self.simulator._get_name())
        self.client.disconnect()

    def testNormalExecution(self):
        self.logger.info("called...")

        srcCode = "print 'Hello, world!'"
        
        self.error = None
        self.result = ""        
        self.__runSBExecProcess(srcCode)
        
        self.assertEquals("Hello, world!\n", self.result)

    def testWrongExecution(self):
        self.logger.info("called...")

        srcCode = "print a"
        
        self.error = None
        self.result = ""        
        self.__runSBExecProcess(srcCode)
        
        self.assertEquals("General ScriptExecutor runtime error", self.error.shortDescription)
        

    def __runSBExecProcess(self, srcCode):

        self.simulator.setGlobalData('Result', '')
        self.simulator.setGlobalData('Error', '')
        
        stdin_fd = sys.stdin.fileno()

        child_in, parent_out = os.pipe() # pipe() -> (read_end, write_end)
        
        self.newpid = os.fork()
        
        if self.newpid == 0: #child
            os.close(parent_out)
            os.dup2(child_in, stdin_fd)
            os.execlp('SBExecProcess.py', 'SBExecProcess.py') # overlay process

        else: #parent
            os.close(child_in)

            fout = os.fdopen(parent_out, 'w')

            pickle.dump(self.scriptExecName, fout)
            fout.flush()

            pickle.dump(self.arrayName, fout)
            fout.flush()

            pickle.dump(srcCode, fout)
            fout.flush()

            pickle.dump(self.schedBlock, fout)
            fout.flush()

            pid, status = os.wait()
            self.logger.info("Child completed %d %d" % (pid, status))
            self.newpid = -1
            
            if status == -1: self.result = "Process aborted"

            try:
                error = self.simulator.getGlobalData('Error')
                if len(error) > 0:
                    self.error = pickle.loads(error)
            except ACSSim.NoSuchDataEx, ex:
                self.logger.info("No global data error in simulator")
            self.logger.debug("Error is " + str(self.error))

        self.result = self.simulator.getGlobalData('Result')
        self.logger.info("Execution successful, results:\n" + print_code(self.result))
        

def suite():
    suite = unittest.TestSuite()
    suite.addTest(SBExecProcessTest("testNormalExecution"))
    suite.addTest(SBExecProcessTest("testWrongExecution"))
    return suite

if __name__ == '__main__':
    unittest.main(defaultTest='suite')
