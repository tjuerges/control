/* ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.ControlTest.APDMMarshallingTestImpl;

import java.util.logging.Logger;
import java.io.StringReader;

import alma.ACS.ComponentStates;
import alma.acs.component.ComponentLifecycle;
import alma.acs.container.ContainerServices;
//import alma.acs.nc.CorbaPublisher;
import alma.ControlTest.APDMMarshallingTestOperations;

import alma.entity.xmlbinding.obsproject.QuerySourceT;
import alma.entity.xmlbinding.schedblock.FieldSourceT;
import alma.entity.xmlbinding.schedblock.SpectralSpecT;

public class APDMMarshallingTestImpl implements ComponentLifecycle,
        APDMMarshallingTestOperations {

    private ContainerServices m_containerServices;

    private Logger m_logger;
    //    private CorbaPublisher m_channel;

    /////////////////////////////////////////////////////////////
    // Implementation of ComponentLifecycle
    /////////////////////////////////////////////////////////////

    public void initialize(ContainerServices containerServices) {
        m_containerServices = containerServices;
        m_logger = m_containerServices.getLogger();

        m_logger.finest("initialize() called...");
    }

    public void execute() {
        m_logger.finest("execute() called...");
    }

    public void cleanUp() {
        m_logger.finest("cleanUp() called");
    }

    public void aboutToAbort() {
        cleanUp();
        m_logger.finest("managed to abort...");
        System.out.println("DummyComponent component managed to abort...");
    }

    /////////////////////////////////////////////////////////////
    // Implementation of ACSComponent
    /////////////////////////////////////////////////////////////

    public ComponentStates componentState() {
        return m_containerServices.getComponentStateManager().getCurrentState();
    }

    public String name() {
        return m_containerServices.getName();
    }

    /////////////////////////////////////////////////////////////
    // Implementation of APDMMarshallingTestOperations
    /////////////////////////////////////////////////////////////
    
    public boolean testQuerySource(String xmlString) {
        // The incoming string should be a fragment of an XML document
        // which contains the description of a Query Source.
        QuerySourceT qSource = null;
        try {
            qSource = QuerySourceT.unmarshalQuerySourceT
                (new StringReader(xmlString)); 
        } catch (org.exolab.castor.xml.MarshalException ex) {
            // Failed to unmarshal the query source.
            m_logger.warning("Error Unarshalling QuerySource: " + ex);
            return true;
        } catch (org.exolab.castor.xml.ValidationException ex) {
            m_logger.warning("Error Validating QuerySource: " + ex);
            return true;
        }
        return false;
    }
    
    public boolean testFieldSource(String xmlString) {
        // The incoming string should be a fragment of an XML document
        // which contains the description of a Field Source.
        FieldSourceT fSource = null;
        try {
            fSource = FieldSourceT.unmarshalFieldSourceT
                (new StringReader(xmlString)); 
        } catch (org.exolab.castor.xml.MarshalException ex) {
            // Failed to unmarshal the query source.
            m_logger.warning("Error Unarshalling FieldSource: " + ex);
            return true;
        } catch (org.exolab.castor.xml.ValidationException ex) {
            m_logger.warning("Error Validating FieldSource: " + ex);
            return true;
        }
        return false;
    }

    public boolean testSpectralSpec(String xmlString) {
        // The incoming string should be a fragment of an XML document
        // which contains the description of a Field Source.
        SpectralSpecT sSpec = null;
        try {
            sSpec = SpectralSpecT.unmarshalSpectralSpecT
                (new StringReader(xmlString)); 
        } catch (org.exolab.castor.xml.MarshalException ex) {
            // Failed to unmarshal the query source.
            m_logger.warning("Error Unarshalling SpectralSpec: " + ex);
            return true;
        } catch (org.exolab.castor.xml.ValidationException ex) {
            m_logger.warning("Error Validating SpectralSpec: " + ex);
            return true;
        }
        return false;
    }

}