#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002
# (c) European Southern Observatory, 2002
#
# @(#) $Id$
#------------------------------------------------------------------------------


import unittest
import Acspy.Clients.SimpleClient
#import math

import CCL.APDMSchedBlock

import CCL.FieldSource
import CCL.SpectralSpec
#import CCL.APDMValueTypes

#import APDMEntities.SchedBlock

# Make sure that the local directory is in the python path
import sys
sys.path.append("../lib/python/site-packages")


class APDMMarshalingTest(unittest.TestCase):
    def setUp(self):
        self.client = Acspy.Clients.SimpleClient.PySimpleClient()
        self.ref = self.client.getComponent("APDMMarshalingTester")

    def tearDown(self):
        self.client.releaseComponent(self.ref._get_name())

    def testQuerySource(self):
        qs = CCL.APDMSchedBlock.QuerySource()
        # Put some values other than default in
        qs.minTimeSinceObserved.set("7 d")
        qs.maxTimeSinceObserved.set("14 d")
        qs.minFlux.set("1 mJy")
        qs.maxFlux.set("10 Jy")
        qs.minFrequency.set("100 GHz")
        qs.maxFrequency.set("200 GHz")
        qs.queryCenter.longitude.set("12:00:00")
        qs.queryCenter.latitude.set("45.00.00")
        qs.searchRadius.set("5 deg")
        qs.maxSources=1
        self.assertFalse(self.ref.testQuerySource(qs.getXMLString()))

    def testFieldSource(self):
        # Here we want to try all of our basic field source objects
        for fs in [CCL.FieldSource.PlanetSource("Venus"),
                   CCL.FieldSource.PlanetSource("Io"),
                   CCL.FieldSource.EquatorialSource("12:34:56", "-12.34.56",
                                                    "MySource"),
                   CCL.FieldSource.HorizonSource("12.34.56", "12.34.56",
                                                 "MySource")]:
            self.assertFalse(self.ref.testFieldSource(fs.getXMLString()))
            
    def testSpectralSpec(self):
        ss = CCL.SpectralSpec.SpectralSpec.CreateBLCorrelatorSpec()
        self.assertFalse(self.ref.testSpectralSpec(ss.getXMLString()))

        
if __name__ == "__main__":
    suite = unittest.makeSuite(APDMMarshalingTest, 'test')
    unittest.TextTestRunner(verbosity=2).run(suite)
