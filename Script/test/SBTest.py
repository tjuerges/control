#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# rhiriart  2006-06-06  created
#

import shutil
shutil.rmtree("../lib/python/site-packages/Control__POA", True)
shutil.rmtree("../lib/python/site-packages/Control", True)

from Acspy.Util.XmlObjectifier import XmlObject
import unittest
import glob
import sys
from CCL.schedblock import SB

class SBTest(unittest.TestCase):
    
    def setUp(self):
        """
        Sets up the test case 'fixture'.
        
        This method is called before each test case method.
        """
        
        self.SCHEDBLKS_DIR = "schedblks"
        
        fileNames = glob.glob(self.SCHEDBLKS_DIR + "/*.xml")
        self.xmlObjects = {}
        for file in fileNames:
            # print "Reading", file
            f = open(file)
            xml = f.read()
            f.close()
            self.xmlObjects[file] = XmlObject(xmlString=xml)

    def tearDown(self): pass
    
    def testSB(self):
        sb = SB(self.xmlObjects['schedblks/1ObsTargetSB.xml'])
        
    def testGetAttr(self):
        sb = SB(self.xmlObjects['schedblks/1ObsTargetSB.xml'])
        self.assertEquals('ReferenceId-4', sb.SchedBlock.SpectralSpec.identifier.getValue())
        
    def testGetTargets(self):
        sb = SB(self.xmlObjects['schedblks/1ObsTargetSB.xml'])
        targets = sb.getTargets()
        self.assertEquals(1, len(targets))
        self.assertEquals('ReferenceId-0', targets[0].identifier.getValue())
        self.assertEquals('ReferenceId-3', targets[0].fieldSourceId.getValue())
    
    def testGetInstrumentSpec(self):
        sb = SB(self.xmlObjects["schedblks/SixBrightPolarStars.xml"])
        targets = sb.getTargets()
        for target in targets:
            (instrType, instrSpec) = sb.getAbstractInstrumentSpec(target)
            if instrType == "OpticalCameraSpec":
                self.assertEquals(2.0, instrSpec.minIntegrationTime.getValue())
                # Unfortunately, XmlObjectifier defines the 'name' attribute
                # (see XmlObjectifier.py line 325, in the acspy ACS module)
                # so it is necessary to use the low level dom interfaces to get
                # the name element. 
                for node in instrSpec.childNodes:
                    if node.nodeName == "name":
                        if len(node.childNodes) == 1:
                            name = node.childNodes[0].data
                        else:
                            name = ""
                        self.assertEquals("trcOP.scat.6.polar.stars.bright", name)
                # TODO complete the rest of the cases
                
    def testGetObservingParameters(self):
        """
        Test function getObservingParameters().
        """

        sb = SB(self.xmlObjects["schedblks/SixBrightPolarStars.xml"])
        targets = sb.getTargets()
        for target in targets:
            obsParameters = sb.getObservingParameters(target)
            # the documen should have only one single node
            self.assertEquals(1, len(obsParameters))
            self.assertEquals(1, len(obsParameters[0].childNodes))
            if obsParameters[0].childNodes[0].nodeName == "OpticalPointingParameters":
                obsParam = obsParameters[0].OpticalPointingParameters
            self.assertEquals(60.0, obsParam.antennaPositionTolerance.getValue())
            self.assertEquals(15.0, obsParam.elevationLimit.getValue())
            self.assertEquals(0.0, obsParam.maxMagnitude.value.getValue())            
            self.assertEquals(13.0, obsParam.minMagnitude.value.getValue())
            
    def testGetFieldSource(self):
        """
        Test SBObjectifier function getFieldSource().
        """
        sb = SB(self.xmlObjects["schedblks/SixBrightPolarStars.xml"])
        targets = sb.getTargets()
        
        sourceNames = ["Optical40_Cas_456",
                       "OpticalOri_1686", 
                       "Opticalalp_UMa_4301", 
                       "Opticalbeta_UMi_5563",
                       "Opticaldel_Dra_7310",
                       "Opticaldel_Aur_2077"]
        
        t = 0
        for target in targets:
            source = sb.getFieldSource(target)
            self.assertEquals(0.0, source.pMRA.getValue())
            # Unfortunately, XmlObject hides the 'name' attribute
            # (see XmlObjectifier.py, line 325, in the acspy ACS module)
            # so it is necessary to use the low level DOM interfaces to get
            # the name element. 
            for node in source.childNodes:
                if node.nodeName == "name":
                    if len(node.childNodes) == 1:
                        name = node.childNodes[0].data
                    else:
                        name = ""
                    self.assertEquals(sourceNames[t], name)
            t = t + 1
            # etc.

    def testGetHolographyParameters(self):
        """
        Test the way of getting HolographyParameters from an holography SB.
        """
        
        sb = SB(self.xmlObjects["schedblks/HolographySB.xml"])
        
        self.assertEquals(104.0, sb.SchedBlock.HolographyParameters.frequency.getValue(), 0.01)
        self.assertEquals(20.0, sb.SchedBlock.HolographyParameters.resolution.getValue(), 0.01)
        self.assertEquals(0.0, sb.SchedBlock.HolographyParameters.startFraction.getValue(), 0.01)
        self.assertEquals(200.0, sb.SchedBlock.HolographyParameters.speed.getValue(), 0.01)
        self.assertEquals(5, sb.SchedBlock.HolographyParameters.rowsCal.getValue())
        self.assertEquals(60.0, sb.SchedBlock.HolographyParameters.calTime.getValue(), 0.01)

        # Unfortunately, XmlObjectifier defines the 'name' attribute
        # (see XmlObjectifier.py line 325, in the acspy ACS module)
        # so it is necessary to use the low level dom interfaces to get
        # the name element.
        name = "" 
        for node in sb.SchedBlock.HolographyParameters.childNodes:
            if node.nodeName == "name":
                if len(node.childNodes) == 1:
                    name = node.childNodes[0].data
                else:
                    name = ""
        self.assertEquals("Holography (Default values)", name)
        
    def testGetScienceTargets(self):
        """
        Test getScienceTargets.
        """
        sb = SB(self.xmlObjects['schedblks/Orion-SFI-Test.xml'])
        targets = sb._getScienceReferences()
        self.assertEquals(1, len(targets))

    def testGetFieldSources(self):
        """
        Test getFieldSources.
        """
        sb = SB(self.xmlObjects['schedblks/Orion-SFI-Test.xml'])
        fieldSources = sb._getFieldSources()
        self.assertEquals(3, len(fieldSources))

    def testGetTargetsForFieldSource(self):
        """
        Test getTargetsForFieldSource.
        """
        sb = SB(self.xmlObjects['schedblks/Orion-SFI-Test.xml'])
        fieldSources = sb._getFieldSources()
        fs = fieldSources[0]
        targets = sb._getTargetsForFieldSource(fs)
        self.assertEquals(1, len(targets))
                
    def testGetSubscanFieldSources(self):
        """
        Test getFieldSources.
        """
        sb = SB(self.xmlObjects['schedblks/Orion-SFI-Test.xml'])
        subscanFieldSources = sb.getSubscanFieldSources()
        self.assertEquals(3, len(subscanFieldSources))
        
#    def testGetTargetsForSubscanFieldSource(self):
#        """
#        Test getTargetsForSubscanFieldSource.
#        """
#        sb = SB(self.xmlObjects['schedblks/SFI_BL_NEW_APDM.xml'])
#        for ssfs in sb.getSubscanFieldSources():
#            targets = sb.getTargetsForSubscanFieldSource(ssfs)
                
                
# --------------------------------------------------
# MAIN Program
# --------------------------------------------------
if __name__ == '__main__':

    runner = unittest.TextTestRunner()
    if len( sys.argv ) > 1:
        suite = unittest.makeSuite(SBTest, sys.argv[1])
    else:
        suite = unittest.makeSuite(SBTest)
    runner.run(suite)
