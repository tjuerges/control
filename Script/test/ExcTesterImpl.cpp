/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 *
 */

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <ExcTesterImpl.h>

using namespace baci;

ExcTesterImpl::ExcTesterImpl(const ACE_CString &name,
    maci::ContainerServices* containerServices) :
    acscomponent::ACSComponentImpl(name, containerServices)
{

    ACS_TRACE("ExcTesterImpl constructor");
    ACS_TRACE(name.c_str());
    ACS_TRACE("::ExcTesterImpl::ExcTesterImpl");
}

ExcTesterImpl::~ExcTesterImpl()
{
    ACS_TRACE("::ExcTesterImpl::~ExcTesterImpl");
}


void ExcTesterImpl::initialize()
    throw(acsErrTypeLifeCycle::LifeCycleExImpl) {
    ACS_TRACE("ExLogTesterImpl initialize");
}

void ExcTesterImpl::logException() 
    throw (CORBA::SystemException, CorrErr::InvalidConfigEx) {

    CorrErr::InvalidConfigExImpl invCfgErr(__FILE__, __LINE__, "logException");
    throw invCfgErr.getInvalidConfigEx();
}

/* --------------- [ MACI DLL support functions ] -----------------*/
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(ExcTesterImpl)
/* ----------------------------------------------------------------*/


/*___oOo___*/
