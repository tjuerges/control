#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import unittest
import CCL.APDMSchedBlock as APDMSchedBlock
import APDMEntities.SchedBlock
from CCL.BaseBandSpecification import BaseBandSpecification
from CCL.SpectralWindow import BLSpectralWindow
from CCL.SpectralWindow import ACASpectralWindow
from CCL.BaseBandConfig import AbstractBaseBandConfig
from CCL.BaseBandConfig import BLBaseBandConfig
from CCL.BaseBandConfig import ACABaseBandConfig


class BLBaseBandConfigTest(unittest.TestCase):
    def testDefaultInstanciation(self):
        bbs = BaseBandSpecification()
        bbc = BLBaseBandConfig(bbs)
        self.assertTrue(isinstance(bbc, BLBaseBandConfig))
        self.assertTrue(isinstance(bbc, AbstractBaseBandConfig))
        self.assertTrue(isinstance(bbc, APDMSchedBlock.BLBaseBandConfig))
        self.assertTrue(isinstance(bbc,
                                   APDMEntities.SchedBlock.BLBaseBandConfigT))
        self.assertEqual(bbs.entityPartId, bbc.BaseBandSpecificationRef.partId)

    def testConstructor(self):
        bbs = BaseBandSpecification()
        sw  = BLSpectralWindow()
        bbc = BLBaseBandConfig(bbs, 'FAST', 'AUTO_ONLY', sw)
        self.assertEqual(bbs.entityPartId, bbc.BaseBandSpecificationRef.partId)
        self.assertEqual(bbc.cAM, 'FAST')
        self.assertEqual(bbc.dataProducts, 'AUTO_ONLY')
        self.assertEqual(bbc.BLSpectralWindow,[sw])
        

class ACABaseBandConfigTest(unittest.TestCase):
    def testDefaultInstanciation(self):
        bbs = BaseBandSpecification()
        bbc = ACABaseBandConfig(bbs)
        self.assertTrue(isinstance(bbc, ACABaseBandConfig))
        self.assertTrue(isinstance(bbc, AbstractBaseBandConfig))
        self.assertTrue(isinstance(bbc, APDMSchedBlock.ACABaseBandConfig))
        self.assertTrue(isinstance(bbc,
                                   APDMEntities.SchedBlock.ACABaseBandConfigT))
        self.assertEqual(bbs.entityPartId, bbc.BaseBandSpecificationRef.partId)

    def testConstructor(self):
        bbs = BaseBandSpecification()
        sw  = ACASpectralWindow()
        bbc = ACABaseBandConfig(bbs, 'FAST', 'AUTO_ONLY', sw)
        self.assertEqual(bbs.entityPartId, bbc.BaseBandSpecificationRef.partId)
        self.assertEqual(bbc.cAM, 'FAST')
        self.assertEqual(bbc.dataProducts, 'AUTO_ONLY')
        self.assertEqual(bbc.ACASpectralWindow,[sw])

if __name__ == "__main__":
    BLSuite  = unittest.makeSuite(BLBaseBandConfigTest,'test')
    ACASuite = unittest.makeSuite(ACABaseBandConfigTest,'test')
    alltests = unittest.TestSuite((BLSuite, ACASuite))
    unittest.TextTestRunner(verbosity=2).run(alltests)

