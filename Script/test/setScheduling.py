#!/usr/bin/env python
from Acspy.Clients.SimpleClient import PySimpleClient

client = PySimpleClient()

simulator = client.getDefaultComponent("IDL:alma/ACSSim/Simulator:1.0")

code = '''LOGGER.logInfo('startManualModeSession called...')
from asdmIDLTypes import IDLEntityRef
sbref = IDLEntityRef("uid://X00000000/X0", "X0", "SchedBlock", "1.0")
sessionref = IDLEntityRef("uid://X00000000/X0", "X0", "Session", "1.0")
(sbref, sessionref)'''
simulator.setMethod('SCHEDULING_MASTERSCHEDULER', 'startManualModeSession', code, 0)

client.releaseComponent(simulator._get_name())
client.disconnect()
