#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import unittest
import CCL.APDMSchedBlock as APDMSchedBlock
import APDMEntities.SchedBlock
from CCL.BaseBandSpecification import BaseBandSpecification

class BaseBandSpecificationTest(unittest.TestCase):
    def testDefaultInstanciation(self):
        bbs = BaseBandSpecification()
        self.assertTrue(isinstance(bbs, BaseBandSpecification))
        self.assertTrue(isinstance(bbs,
                                   APDMSchedBlock.BaseBandSpecification))
        self.assertTrue\
            (isinstance(bbs, APDMEntities.SchedBlock.BaseBandSpecificationT))

    def testConstructor(self):
        bbs = BaseBandSpecification('BB_2', '123 GHz')
        self.assertEqual(bbs.baseBandName, 'BB_2')
        self.assertAlmostEqual(bbs.centerFrequencyRest.get(), 123.0E9)


if __name__ == "__main__":
    alltests = unittest.makeSuite(BaseBandSpecificationTest,'test')
    unittest.TextTestRunner(verbosity=2).run(alltests)

