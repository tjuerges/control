#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import unittest
import CCL.APDMSchedBlock as APDMSchedBlock

import CCL.SchedBlock
import APDMEntities.SchedBlock

#pyxb.namespace.builtin.XMLSchema_instance.ProcessTypeAttribute\
#     (pyxb.namespace.builtin.XMLSchema_instance.PT_skip)
#pyxb.RequireValidWhenParsing(False)
#import pyxb
#pyxb.RequireValidWhenGenerating(False)


class SchedBlockParsingTest(unittest.TestCase):

    def testParsingExampleProject(self):
        f = open('schedblks/ExampleProject/SchedBlock0.xml','r')
        xmlString = f.read()
        f.close()
        sb = APDMEntities.SchedBlock.CreateFromDocument(xmlString)
        self.assertTrue(isinstance(sb,
            APDMSchedBlock.findImplementingBindingClass("APDM::SchedBlock")))
        self.assertTrue(isinstance(sb, CCL.SchedBlock.SchedBlock))

        
class SchedBlockTest(unittest.TestCase):
    def setUp(self):
        filename = 'schedblks/ExampleProject/SchedBlock0.xml'
        self.sb = CCL.SchedBlock.CreateFromFile(filename)
        
    def testInstanciation(self):
        self.assertTrue(isinstance(self.sb,
            APDMSchedBlock.findImplementingBindingClass("APDM::SchedBlock")))
        self.assertTrue(isinstance(self.sb, CCL.SchedBlock.SchedBlock))

    def testGetReferencedInstrumentSpec(self):
        # Only one Spectral Spec so this is easy:
        for target in self.sb.Target:
            self.assertEqual(self.sb.getReferencedInstrumentSpec(target),
                             self.sb.SpectralSpec[0])

    def testGetReferencedFieldSource(self):
        # The map from the Target::entityPartId to the FieldSource name
        resultMap = {'X26736606':'T1',
                     'X15450155':'T1',
                     'X24919121':'T1',
                     'X32121999':'T2',
                     'X28158998':'T2',
                     'X2166607':'T2',
                     'X6739818':'T2',
                     'X20373545':'T3',
                     'X19193212':'T3'}

        for target in self.sb.Target:
            self.assertEqual(self.sb.getReferencedFieldSource(target).name,
                             resultMap[target.entityPartId])

    def testGetReferencedObservingParameters(self):
        # The map is from the Target::entityPartId to the ObservingParameters
        # name
        resultMap = {'X26736606':'TurisasSciParams',
                     'X15450155':'phasecalparams',
                     'X24919121':'test12pointingcal',
                     'X32121999':'test12pointingcal',
                     'X28158998':'test12AtmCal',
                     'X2166607':'phasecalparams',
                     'X6739818':'test12AmpCal',
                     'X20373545':'TurisasSciParams',
                     'X19193212':'phasecalparams'}

        for target in self.sb.Target:
            paramList = self.sb.getReferencedObservingParameters(target)
            self.assertTrue(isinstance(paramList,list))
            self.assertEqual(len(paramList),1)
            self.assertEqual(paramList[0].name,
                             resultMap[target.entityPartId])


    def testGetReferencingTarget(self):
        # Do the simple case first all targets should be returned for
        # the spectral spec
        targetList = self.sb.getReferencingTargets(self.sb.SpectralSpec[0])
        for t in self.sb.Target:
            try:
                targetList.remove(t)
            except ValueError:
                self.fail("Failed to get all targets")
        if len(targetList) != 0:
            self.fail("Unknown or duplicate target detected")

        # Test that we don't get any duplicates using the Field Source list
        targetList = self.sb.getReferencingTargets(self.sb.FieldSource)
        for t in self.sb.Target:
            try:
                targetList.remove(t)
            except ValueError:
                self.fail("Failed to get all targets")
        if len(targetList) != 0:
            self.fail("Unknown or duplicate target detected")
        


if __name__ == "__main__":
    ParsingSuite  = unittest.makeSuite(SchedBlockParsingTest,'test')
    SBSuite = unittest.makeSuite(SchedBlockTest,'test')
    alltests = unittest.TestSuite((ParsingSuite, SBSuite))
    unittest.TextTestRunner(verbosity=2).run(alltests)

