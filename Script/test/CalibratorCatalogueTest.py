#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import unittest

from CCL.CalibratorCatalogue import CalibratorCatalogue
from CCL.APDMSchedBlock import QuerySource

class CalibratorCatalogueTest(unittest.TestCase):

    def setUp(self):        
        self.cat = CalibratorCatalogue()

    def tearDown(self):
        pass
    
    def testFreqConstraints(self):
        qs = QuerySource()
        qs.minFrequency.set(1000000)
        qs.maxFrequency.set(90000000)
        cals = self.cat.query(qs, [2])
        self.assertTrue( len(cals) > 0 )

if __name__ == "__main__":
    suite = unittest.makeSuite(CalibratorCatalogueTest, 'test')
    unittest.TextTestRunner(verbosity=2).run(suite)
