#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002
# (c) European Southern Observatory, 2002
#
# @(#) $Id$
#------------------------------------------------------------------------------


import unittest
import CCL.APDMSchedBlock
from ControlExceptionsImpl import IllegalParameterErrorExImpl

class FindImplementingBindingClassTest(unittest.TestCase):
    def testSimpleExection(self):
        for almatype in ["APDM::ObsReview", "APDM::SBStatus",
                         "APDM::SchedBlock", "APDM::OUSStatus",
                         "APDM::ObsProject", "APDM::ProjectStatus",
                         "APDM::ObsProposal"]:
            foo = CCL.APDMSchedBlock.findImplementingBindingClass(almatype)()
            self.assertEqual(almatype, foo.almatype)
            
    def testErrorFindingClass(self):
        self.assertRaises(IllegalParameterErrorExImpl,
                          CCL.APDMSchedBlock.findImplementingBindingClass,
                          "NotAType")

if __name__ == "__main__":
    suite = unittest.makeSuite(FindImplementingBindingClassTest, 'test')
    unittest.TextTestRunner(verbosity=2).run(suite)
