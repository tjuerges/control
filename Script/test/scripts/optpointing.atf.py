# Test optical pointing script to be used in the STE.
#
# This script illustrates the use of the CCL objects and operations
# that will be used for optical pointing.
#
# $Id$
#

import math, time
import Control
import ControlSB
import OpticalPointingExceptions

# Optical Pointing observing parameters
OBS_TIME = 'day'               # 'day' or 'night'
EXP_TIME = 10.0                # exposure time, in seconds
DARK_EXP_TIME = 10.0           # exposure time, in seconds
ZERO_MAG_EXP_TIME = 5.0        # zero magnitude exposure time
SNR_THRESHOLD = 1.0            # signal to noise ratio parameter 
SOURCE_TRACKING_ERROR = 0.1    # maximum source tracking error
EXP_TIMEOUT = 2 * max(EXP_TIME, DARK_EXP_TIME)   # timeout for dark or source exposures
TRACK_TIMEOUT = 180            # timeout for the antenna to move and track a source
SLEEP_TIME = 1.0               # sleep time between asking the pointing error to the antenna
NO_PERR_TO_DISCARD = 5         # number of pointing error invocations to discard, to give the
                               # antenna some time to adjust its state between source trackings
SNR_THRESHOLD = 0.001          # signal to noise threshold

logInfo('Beginning optical pointing execution.')

# This instruction marks the beginning of the execution.
# As a result, the Control Array component will send messages to
# the notification channel and invoke DataCapture methods.
beginExecution()

# Get the Scheduling Block
schedBlk = getSB()

logInfo('Project ID: ' + schedBlk.projectId)
logInfo('Observation Unit ID: ' + schedBlk.obsUnitSetId)
logInfo('Scheduling Block ID: ' + schedBlk.sbId)
logInfo('Session ID: ' + schedBlk.sessionId)
# Check that this is really an optical pointing scheduling block.
if schedBlk.obsMode != ControlSB.PointingCalibrationSession:
    logCritical('Warning this is does not appear to be an optical pointing script.  Attempting execution anyway. obsMode is ' + str(schedBlk.obsMode))
    #endExecution(Control.FAIL, 'This is not an optical pointing script.')

# Get the array object
array = getArray()
status = array.status()
logInfo("Got the array object. Array status: " + status)
# if not array.isOperational()
#     logCritical('Array is not operational')
#     endExecution(Control.FAIL, 'Array is not operational')

# Get the antenna object.
# Because optical pointing deals with a single antenna we interact directly with
# the antenna CCL object in this script.
# In general this will not be necessary in other observation modes where the interaction
# will take place mainly at the array level.

# Here we check that the antenna is ALMA001.  This is a temporary workaround until we have
# the full telescope configuration database to ensure we have a optical telescope installed.
antennaNames = array.antennas()
if len(antennaNames) != 1 and antennaNames[0] != 'CONTROL/ALMA001':
    logCritical("Wrong Array. It doesn't contain ALMA001 antenna.")
    endExecution(Control.FAIL, "Wrong Array. It doesn't contain CONTROL/ALMA001 antenna.")
    

antenna = array.getAntenna(antennaNames[0])
status = antenna.status()
logInfo('Got the Antenna object. Antenna status: ' + status)
if not antenna.isOperational():
    logCritical('Antenna is not operational')
    endExecution(Control.FAIL, 'Antenna is not operational')
    

# Get the Optical Telescope installed in this antenna.
optTel = antenna.getOptTelescope()
logInfo('Got Optical Telescope. ')

# The optical telescope object requires to be initialized before being
# ready to observe. The initialize method will initialize the optical
# telescope hardware device (open the shutter, etc.) and take an initial
# dark exposure.
optTel.initialize()
status = optTel.status()
logInfo('Got the Optical Telescope object. Optical Telescope status: ' + status)
if not optTel.isOperational():
    logCritical('Optical Telescope is not operational')
    endExecution(Control.FAIL, 'Optical Telescope is not operational')
    

if OBS_TIME == 'day':
    optTel.dayTimeObserving()
elif OBS_TIME == 'night':
    optTel.nightTimeObserving()
else:
    msg = "Wrong observation time (OBS_TIME) parameter. OBS_TIME can only be 'night' "
    msg = msg + "or 'day', not '" + OBS_TIME + "'. Check script parameters."
    logCritical(msg)
    endExecution(Control.FAIL, 'Wrong OBS_TIME parameter.')

optTel.setSignalToNoiseThreshold(SNR_THRESHOLD)

# Get the list of sources from the Scheduling Block.
sourceList = schedBlk.sourceList

for source in sourceList:

    # This instruction marks the beginning of a scan.
    # As a result, the Control Array component will send messages to
    # the notification channel and invoke DataCapture methods.
    array.beginScan()

    # This instruction marks the beginning of the subscan.
    # As a result, the Control Array component will send messages to
    # the notification channel and invoke DataCapture methods.
    # We pass None in the second and third parameters as the
    # spectral specification and the correlator configuration are
    # not used in the optical pointing mode.
    array.beginSubscan(source, None, None)
    
    # Point the antenna to the source.
    sleepTime = 1.0    # Time to sleep between each cycle
    elapsedTime = 0

    # The following is necessary because the ra and dec from the source
    # coming out of the sched block are reversed and in degrees
    tempra = source.coord.dec * math.pi / 180.0
    source.coord.dec = source.coord.ra * math.pi /180.0
    source.coord.ra  = tempra

    logInfo('Pointing the antenna to source ' + source.sourceName)
    logInfo("(RA, DEC) = (" + str(source.coord.ra * 180.0 / (math.pi * 15.0)) +
            ", " + str(source.coord.dec * 180.0 / math.pi) + ")")
    antenna.setDirection(source)

    error = 2.0 * SOURCE_TRACKING_ERROR
    logInfo("Tracking source...")
    elapsedTime = 0
    count = 1
    while error > SOURCE_TRACKING_ERROR and elapsedTime < TRACK_TIMEOUT:
        time.sleep(SLEEP_TIME)
        if count > NO_PERR_TO_DISCARD:
            (xerr, yerr) = antenna.getPointingError()
            error = math.sqrt(xerr * xerr + yerr * yerr)
            logInfo("Pointing error = (" + str(xerr) + ", " + str(yerr) + ")" +
                    "[" + str(error) + "]")
        else:
            logInfo("Discarding " + str(count) + " pointing error")
        elapsedTime = elapsedTime + SLEEP_TIME
        count = count + 1
    if elapsedTime >= TRACK_TIMEOUT:
        logCritical("Timeout reached when waiting for the system to track the source")
        array.endSubscan()
        array.endScan()
        continue
        
    # Good. We're tracking the source.
    logInfo('Now on Source.. Beginning Exposure')
    # Take a source exposure now.
    optTel.startExposure(EXP_TIME)
    
    if (optTel.waitExposure()):
        logCritical('Error in source exposure. Timeout reached.')
        endExecution(Control.FAIL, 'Error in source exposure. Timeout reached.')
        
        
    # Get the calibration results.
    try:
        data = optTel.getSubScanData()
    except OpticalPointingExceptions.NoStarEx:
        logCritical('Error finding source in image. Cancelling this subscan.')
        array.endSubscan()
        array.endScan()
        continue

    # Pass them to the array to be sent to DataCapture.    
    array.sendOpticalPointingData(data)
    
    # This instruction marks the end of a subscan.
    # The Control Array component will send messages to the notification
    # channel and invoke DataCapture methods.
    array.endSubscan()

    # This instruction marks the end of a scan.
    # The Control Array component will send messages to the notification
    # channel and invoke DataCapture methods.
    array.endScan()

optTel.shutdown()

# Move the antenna to the stow position. In the stow position
# the antenna points to the east with 15 degrees of elevation.
antenna.setDirection(azel=(math.pi/2, math.pi/8))

# This instruction marks the end of the execution.
# The Control Array component will send messages to the notification
# channel and invoke DataCapture methods.
endExecution(Control.SUCCESS, 'Optical Pointing script succeeded')
