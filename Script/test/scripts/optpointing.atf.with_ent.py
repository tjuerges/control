# Test optical pointing script to be used in the STE.&#xa;
#&#xa;
# This script illustrates the use of the CCL objects and operations&#xa;
# that will be used for optical pointing.&#xa;
#&#xa;
# $Id$&#xa;
#&#xa;
&#xa;
import math, time&#xa;
import Control&#xa;
import ControlSB&#xa;
import OpticalPointingExceptions&#xa;
&#xa;
# Optical Pointing observing parameters&#xa;
OBS_TIME = 'day'               # 'day' or 'night'&#xa;
EXP_TIME = 10.0                # exposure time, in seconds&#xa;
DARK_EXP_TIME = 10.0           # exposure time, in seconds&#xa;
ZERO_MAG_EXP_TIME = 5.0        # zero magnitude exposure time&#xa;
SNR_THRESHOLD = 1.0            # signal to noise ratio parameter &#xa;
SOURCE_TRACKING_ERROR = 0.1    # maximum source tracking error&#xa;
EXP_TIMEOUT = 2 * max(EXP_TIME, DARK_EXP_TIME)   # timeout for dark or source exposures&#xa;
TRACK_TIMEOUT = 180            # timeout for the antenna to move and track a source&#xa;
SLEEP_TIME = 1.0               # sleep time between asking the pointing error to the antenna&#xa;
NO_PERR_TO_DISCARD = 5         # number of pointing error invocations to discard, to give the&#xa;
                               # antenna some time to adjust its state between source trackings&#xa;
SNR_THRESHOLD = 0.001          # signal to noise threshold&#xa;
&#xa;
logInfo('Beginning optical pointing execution.')&#xa;
&#xa;
# This instruction marks the beginning of the execution.&#xa;
# As a result, the Control Array component will send messages to&#xa;
# the notification channel and invoke DataCapture methods.&#xa;
beginExecution()&#xa;
&#xa;
# Get the Scheduling Block&#xa;
schedBlk = getSB()&#xa;
&#xa;
logInfo('Project ID: ' + schedBlk.projectId)&#xa;
logInfo('Observation Unit ID: ' + schedBlk.obsUnitSetId)&#xa;
logInfo('Scheduling Block ID: ' + schedBlk.sbId)&#xa;
logInfo('Session ID: ' + schedBlk.sessionId)&#xa;
# Check that this is really an optical pointing scheduling block.&#xa;
if schedBlk.obsMode != ControlSB.PointingCalibrationSession:&#xa;
    logCritical('Warning this is does not appear to be an optical pointing script.  Attempting execution anyway. obsMode is ' + str(schedBlk.obsMode))&#xa;
    #endExecution(Control.FAIL, 'This is not an optical pointing script.')&#xa;
&#xa;
# Get the array object&#xa;
array = getArray()&#xa;
status = array.status()&#xa;
logInfo("Got the array object. Array status: " + status)&#xa;
# if not array.isOperational()&#xa;
#     logCritical('Array is not operational')&#xa;
#     endExecution(Control.FAIL, 'Array is not operational')&#xa;
&#xa;
# Get the antenna object.&#xa;
# Because optical pointing deals with a single antenna we interact directly with&#xa;
# the antenna CCL object in this script.&#xa;
# In general this will not be necessary in other observation modes where the interaction&#xa;
# will take place mainly at the array level.&#xa;
&#xa;
# Here we check that the antenna is ALMA001.  This is a temporary workaround until we have&#xa;
# the full telescope configuration database to ensure we have a optical telescope installed.&#xa;
antennaNames = array.antennas()&#xa;
if len(antennaNames) != 1 and antennaNames[0] != 'CONTROL/ALMA001':&#xa;
    logCritical("Wrong Array. It doesn't contain ALMA001 antenna.")&#xa;
    endExecution(Control.FAIL, "Wrong Array. It doesn't contain CONTROL/ALMA001 antenna.")&#xa;
    &#xa;
&#xa;
antenna = array.getAntenna(antennaNames[0])&#xa;
status = antenna.status()&#xa;
logInfo('Got the Antenna object. Antenna status: ' + status)&#xa;
if not antenna.isOperational():&#xa;
    logCritical('Antenna is not operational')&#xa;
    endExecution(Control.FAIL, 'Antenna is not operational')&#xa;
    &#xa;
&#xa;
# Get the Optical Telescope installed in this antenna.&#xa;
optTel = antenna.getOptTelescope()&#xa;
logInfo('Got Optical Telescope. ')&#xa;
&#xa;
# The optical telescope object requires to be initialized before being&#xa;
# ready to observe. The initialize method will initialize the optical&#xa;
# telescope hardware device (open the shutter, etc.) and take an initial&#xa;
# dark exposure.&#xa;
optTel.initialize()&#xa;
status = optTel.status()&#xa;
logInfo('Got the Optical Telescope object. Optical Telescope status: ' + status)&#xa;
if not optTel.isOperational():&#xa;
    logCritical('Optical Telescope is not operational')&#xa;
    endExecution(Control.FAIL, 'Optical Telescope is not operational')&#xa;
    &#xa;
&#xa;
if OBS_TIME == 'day':&#xa;
    optTel.dayTimeObserving()&#xa;
elif OBS_TIME == 'night':&#xa;
    optTel.nightTimeObserving()&#xa;
else:&#xa;
    msg = "Wrong observation time (OBS_TIME) parameter. OBS_TIME can only be 'night' "&#xa;
    msg = msg + "or 'day', not '" + OBS_TIME + "'. Check script parameters."&#xa;
    logCritical(msg)&#xa;
    endExecution(Control.FAIL, 'Wrong OBS_TIME parameter.')&#xa;
&#xa;
optTel.setSignalToNoiseThreshold(SNR_THRESHOLD)&#xa;
&#xa;
# Get the list of sources from the Scheduling Block.&#xa;
sourceList = schedBlk.sourceList&#xa;
&#xa;
for source in sourceList:&#xa;
&#xa;
    # This instruction marks the beginning of a scan.&#xa;
    # As a result, the Control Array component will send messages to&#xa;
    # the notification channel and invoke DataCapture methods.&#xa;
    array.beginScan()&#xa;
&#xa;
    # This instruction marks the beginning of the subscan.&#xa;
    # As a result, the Control Array component will send messages to&#xa;
    # the notification channel and invoke DataCapture methods.&#xa;
    # We pass None in the second and third parameters as the&#xa;
    # spectral specification and the correlator configuration are&#xa;
    # not used in the optical pointing mode.&#xa;
    array.beginSubscan(source, None, None)&#xa;
    &#xa;
    # Point the antenna to the source.&#xa;
    sleepTime = 1.0    # Time to sleep between each cycle&#xa;
    elapsedTime = 0&#xa;
&#xa;
    # The following is necessary because the ra and dec from the source&#xa;
    # coming out of the sched block are reversed and in degrees&#xa;
    tempra = source.coord.dec * math.pi / 180.0&#xa;
    source.coord.dec = source.coord.ra * math.pi /180.0&#xa;
    source.coord.ra  = tempra&#xa;
&#xa;
    logInfo('Pointing the antenna to source ' + source.sourceName)&#xa;
    logInfo("(RA, DEC) = (" + str(source.coord.ra * 180.0 / (math.pi * 15.0)) +&#xa;
            ", " + str(source.coord.dec * 180.0 / math.pi) + ")")&#xa;
    antenna.setDirection(source)&#xa;
&#xa;
    error = 2.0 * SOURCE_TRACKING_ERROR&#xa;
    logInfo("Tracking source...")&#xa;
    elapsedTime = 0&#xa;
    count = 1&#xa;
    while error &gt; SOURCE_TRACKING_ERROR and elapsedTime &lt; TRACK_TIMEOUT:&#xa;
        time.sleep(SLEEP_TIME)&#xa;
        if count &gt; NO_PERR_TO_DISCARD:&#xa;
            (xerr, yerr) = antenna.getPointingError()&#xa;
            error = math.sqrt(xerr * xerr + yerr * yerr)&#xa;
            logInfo("Pointing error = (" + str(xerr) + ", " + str(yerr) + ")" +&#xa;
                    "[" + str(error) + "]")&#xa;
        else:&#xa;
            logInfo("Discarding " + str(count) + " pointing error")&#xa;
        elapsedTime = elapsedTime + SLEEP_TIME&#xa;
        count = count + 1&#xa;
    if elapsedTime &gt;= TRACK_TIMEOUT:&#xa;
        logCritical("Timeout reached when waiting for the system to track the source")&#xa;
        array.endSubscan()&#xa;
        array.endScan()&#xa;
        continue&#xa;
        &#xa;
    # Good. We're tracking the source.&#xa;
    logInfo('Now on Source.. Beginning Exposure')&#xa;
    # Take a source exposure now.&#xa;
    optTel.startExposure(EXP_TIME)&#xa;
    &#xa;
    if (optTel.waitExposure()):&#xa;
        logCritical('Error in source exposure. Timeout reached.')&#xa;
        endExecution(Control.FAIL, 'Error in source exposure. Timeout reached.')&#xa;
        &#xa;
        &#xa;
    # Get the calibration results.&#xa;
    try:&#xa;
        data = optTel.getSubScanData()&#xa;
    except OpticalPointingExceptions.NoStarEx:&#xa;
        logCritical('Error finding source in image. Cancelling this subscan.')&#xa;
        array.endSubscan()&#xa;
        array.endScan()&#xa;
        continue&#xa;
&#xa;
    # Pass them to the array to be sent to DataCapture.    &#xa;
    array.sendOpticalPointingData(data)&#xa;
    &#xa;
    # This instruction marks the end of a subscan.&#xa;
    # The Control Array component will send messages to the notification&#xa;
    # channel and invoke DataCapture methods.&#xa;
    array.endSubscan()&#xa;
&#xa;
    # This instruction marks the end of a scan.&#xa;
    # The Control Array component will send messages to the notification&#xa;
    # channel and invoke DataCapture methods.&#xa;
    array.endScan()&#xa;
&#xa;
optTel.shutdown()&#xa;
&#xa;
# Move the antenna to the stow position. In the stow position&#xa;
# the antenna points to the east with 15 degrees of elevation.&#xa;
antenna.setDirection(azel=(math.pi/2, math.pi/8))&#xa;
&#xa;
# This instruction marks the end of the execution.&#xa;
# The Control Array component will send messages to the notification&#xa;
# channel and invoke DataCapture methods.&#xa;
endExecution(Control.SUCCESS, 'Optical Pointing script succeeded')&#xa;
