import math
import time
import Control
import ControlSB

array = getArray()

antenna = array.getAntenna('CONTROL/ALMA001')

optTel = antenna.getOptTelescope()

optTel.initialize()

time.sleep(10)

optTel.shutdown()

antenna.setDirection(azel=(math.pi/4, math.pi/8))
