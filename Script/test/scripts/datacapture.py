# This script exemplifies the use of the execution, scan and subscan
# notification functions.

import Control
import asdmIDLTypes
import time
import acstime

# This instruction marks the beginning of the execution.
# As a result, the Control Array component will send messages to
# the notification channel and invoke DataCapture methods.
beginExecution()

# Get the Array and the Scheduling Block.
array = getArray()
schedBlk = getSB()

angle01 = asdmIDLTypes.IDLAngle(1.0)
temp01 = asdmIDLTypes.IDLTemperature(180.0)

logInfo("Number of sources: " + str(len(schedBlk.sourceList)))

def getACSTime():
   return int(time.time() * 10e7 + acstime.ACE_BEGIN)

for source in schedBlk.sourceList:
   logInfo("Source name: '" + source.sourceName + "'")

   # This instruction marks the beginning of a scan.
   # As a result, the Control Array component will send messages to
   # the notification channel and invoke DataCapture methods.
   array.beginScan()
   
   # This instruction marks the beginning of the subscan.
   # As a result, the Control Array component will send messages to
   # the notification channel and invoke DataCapture methods.
   # We pass None in the second and third parameters as the
   # spectral specification and the correlator configuration are
   # not used in the optical pointing mode.
   array.beginSubscan(source, None, None)

   startTime = getACSTime()
   
   # endTime = startTime + 1 min.
   endTime = startTime + int(6e9)

   # A SubScanOpticalPointingData structure is created with hardcoded
   # values.
   data = Control.SubScanOpticalPointingData('ALMA001', startTime, endTime,
                                             angle01, angle01, angle01,
                                             angle01, angle01, angle01,
                                             angle01, angle01, temp01)

   # Send this data structure to the Array component, who will send it
   # to DataCapture to be archived at the end of the execution.
   array.sendOpticalPointingData(data)

   # This instruction marks the end of a subscan.
   # The Control Array component will send messages to the notification
   # channel and invoke DataCapture methods.
   array.endSubscan()

   # This instruction marks the end of a scan.
   # The Control Array component will send messages to the notification
   # channel and invoke DataCapture methods.
   array.endScan()

# This instruction marks the end of the execution.
# The Control Array component will send messages to the notification
# channel and invoke DataCapture methods. As a result, DataCapture will
# store the buffered subscan data into the Archive.
endExecution(Control.SUCCESS, 'datacapture example end')
