# Sample Optical Pointing script.
#
# This script illustrates the use of the CCL objects and operations
# that will be used for optical pointing.
#

import math, time
import Control
import ControlSB

# Optical Pointing observing parameters
OBS_TIME = 'day'               # 'day' or 'night'
EXP_TIME = 10.0                # exposure time, in seconds
DARK_EXP_TIME = 10.0           # exposure time, in seconds
ZERO_MAG_EXP_TIME = 5.0        # zero magnitude exposure time
SNR_THRESHOLD = 1.0            # signal to noise ratio parameter 
SOURCE_TRACKING_ERROR = 0.1    # maximum source tracking error
EXP_TIMEOUT = 2 * max(EXP_TIME, DARK_EXP_TIME)   # timeout for dark or source exposures
TRACK_TIMEOUT = 180            # timeout for the antenna to move and track a source

logInfo('Beginning optical pointing execution.')

# This instruction marks the beginning of the execution.
# As a result, the Control Array component will send messages to
# the notification channel and invoke DataCapture methods.
beginExecution()

# Get the Scheduling Block
schedBlk = getSB()

logInfo('Project ID: ' + schedBlk.projectId)
logInfo('Observation Unit ID: ' + schedBlk.obsUnitSetId)
logInfo('Scheduling Block ID: ' + schedBlk.sbId)
logInfo('Session ID: ' + schedBlk.sessionId)
# Check that this is really an optical pointing scheduling block.
if schedBlk.observingMode != ControlSB.PointingCalibration:
    logCritical('This is not an optical pointing script.')
    endExecution(Control.FAIL, 'This is not an optical pointing script.')

# Get the array object
array = getArray()
status = array.status()
logInfo("Got the array object. Array status: " + status)
if not array.isOperational()
    logCritical('Array is not operational')
    endExecution(Control.FAIL, 'Array is not operational')

# Get the antenna object.
# Because optical pointing deals with a single antenna we interact directly with
# the antenna CCL object in this script.
# In general this will not be necessary in other observation modes where the interaction
# will take place mainly at the array level.

# Here we check that the antenna is ALMA001.  This is a temporary workaround until we have
# the full telescope configuration database to ensure we have a optical telescope installed.
antennaNames = array.antennas()
if len(antennaNames) != 1 and antennaNames[0] != 'ALMA001':
    logCritical('Wrong Array. It doesn't contain ALMA001 antenna.')
    endExecution(Control.FAIL, 'Wrong Array. It doesn't contain ALMA001 antenna.')
antenna = array.getAntenna(antennaNames[0])
status = antenna.status()
logInfo('Got the Antenna object. Antenna status: ' + status)
if not antenna.isOperational():
    logCritical('Antenna is not operational')
    endExecution(Control.FAIL, 'Antenna is not operational')

# Get the Optical Telescope installed in this antenna.
optTel = antenna.getOptTelescope()

# The optical telescope object requires to be initialized before being
# ready to observe. The initialize method will initialize the optical
# telescope hardware device (open the shutter, etc.) and take an initial
# dark exposure.
optTel.initialize()
status = optTel.status()
logInfo('Got the Optical Telescope object. Optical Telescope status: ' + status)
if not optTel.isOperational():
    logCritical('Optical Telescope is not operational')
    endExecution(Control.FAIL, 'Optical Telescope is not operational')

if OBS_TIME == 'day':
    optTel.dayTimeObserving()
elif OBS_TIME == 'night':
    optTel.nightTimeObserving()
else:
    msg = "Wrong observation time (OBS_TIME) parameter. OBS_TIME can only be 'night' "
    msg = msg + "or 'day', not '" + OBS_TIME + "'. Check script parameters."
    logCritical(msg)
    endExecution(Control.FAIL, 'Wrong OBS_TIME parameter.')

# It is possible to check if the IR filter is installed
# directly as well:
if optTel.usingIRFilter():
    logInfo('Using IR Filter.')
else:
    logInfo('Not using IR Filter.')
    
optTel.setZeroMagnitudeExposureTime(ZERO_MAG_EXP_TIME)
logInfo('Zero magnitude exposure time: ' + optTel.zeroMagnitudeExposureTime())
optTel.setSignalToNoiseThreshold(SNR_THRESHOLD)
logInfo('Signal to noise threshold: ' + optTel.signalToNoiseThreshold())

# Although the optical telescope's initialize() function already took a
# dark exposure,  another one can be taken if needed:
optTel.startDarkExposure(DARK_EXP_TIME)
if (optTel.waitExposure(EXP_TIMEOUT)):
    logCritical('Error taking dark exposure. Timeout reached.')
    endExecution(Control.FAIL, 'Error taking dark exposure. Timeout reached.')


# Get the list of sources from the Scheduling Block.
sourceList = schedBld.sourceList

for source in sourceList:

    # This instruction marks the beginning of a scan.
    # As a result, the Control Array component will send messages to
    # the notification channel and invoke DataCapture methods.
    array.beginScan([Control.POINTINGMODEL])

    # This instruction marks the beginning of the subscan.
    # As a result, the Control Array component will send messages to
    # the notification channel and invoke DataCapture methods.
    # We pass None in the second and third parameters as the
    # spectral specification and the correlator configuration are
    # not used in the optical pointing mode.
    array.beginSubscan(source, None, None)
    
    # Point the antenna to the source.
    sleepTime = 1.0    # Time to sleep between each cycle
    elapsedTime = 0
    logInfo('Pointing the antenna to source ' + source.sourceName)
    logInfo('RA = ' + str(source.coord.ra))
    logInfo('DEC = ' + str(source.coord.dec))
    # This instruction will make the antenna move and start
    # tracking the source.
    antenna.setDirection(source)
    # Wait until the antenna is tracking the source
    (xerr, yerr) = antenna.getPointingError()
    error = math.sqrt(xerr*xerr+yerr*yerr)
    while error > SOURCE_TRACKING_ERROR and elapsedTime < TRACK_TIMEOUT:
        (xerr, yerr) = antenna.getPointingError()
        error = math.sqrt(xerr*xerr+yerr*yerr)
        time.sleep(sleepTime)
        elapsedTime = elapsedTime + sleepTime
    if elapsedTime >= timeout:
        logCritical('Error pointing the antenna. Timeout reached.')
        endExecution(Control.FAIL, 'Error pointing the antenna, Timeout reached.')

    # Good. We're tracking the source.

    # Take a source exposure now.
    optTel.startExposure(EXP_TIME)
    # In this case the exposure time is being forced. An alternative is
    # to leave the optical telescope to calculate the exposure time from
    # the zero magnitude exposure time parameter and the source attributes:
    #
    #    optTel.startExposure(source)
    #
    # Another way to do it is by using a user defined calculation function:
    #
    #    def calcExpTime(source):
    #       ...
    #
    #    optTel.startExposure(calcExpTime(source))
    #
    # The OpticalPointing device is currently being developed, so their
    # interfaces could be subject to changes.
    
    if (optTel.waitExposure(EXP_TIMEOUT)):
        logCritical('Error in source exposure. Timeout reached.')
        endExecution(Control.FAIL, 'Error in source exposure. Timeout reached.')

    # Find the location of the source in the image frame.
    (xoffset, yoffset) = optTel.reduceExposure()
    logInfo('Offset (pixels) of image source: ' + str((xoffset, yoffset)))
    # If there were problems finding the location of the source in the
    # image, this subscan is cancelled, but the others continue.
    # The error management for reduceExposure() is not completely defined
    # yet, so this section could be subject to changes.
    if xoffset < 0 or yoffset < 0:
        logError('Error finding source in image. Cancelling this subscan.')
        array.endSubscan()
        continue

    # Get the calibration results.
    results = optTel.getSubScanData()
    
    # Pass them to the array to be sent to DataCapture.
    array.storeOptPointingResults(results)

    # This instruction marks the end of a subscan.
    # The Control Array component will send messages to the notification
    # channel and invoke DataCapture methods.
    array.endSubscan()

    # This instruction marks the end of a scan.
    # The Control Array component will send messages to the notification
    # channel and invoke DataCapture methods.
    array.endScan()

optTel.shutdown()

# Move the antenna to the stow position. In the stow position
# the antenna points to the east with 15 degrees of elevation.
antenna.setDirection(azel=(math.pi/2, math.pi/8))

# This instruction marks the end of the execution.
# The Control Array component will send messages to the notification
# channel and invoke DataCapture methods.
endExecution(Control.SUCCESS, 'Optical Pointing script succeeded')
