import ControlSB
import time
import math

array = getArray()
antenna = array.getAntenna('CONTROL/ALMA001')

coord = ControlSB.J2000Coord(0.0, 0.0)

circlePatt = ControlSB.CirclePattern(coord) # phaseCenter
spiralPatt = ControlSB.SpiralPattern(coord) # phaseCenter
absPatt = ControlSB.AbsolutePointingPattern([coord]) # phaseCenter
offPatt = ControlSB.OffsetPointingPattern([coord]) # phaseCenter
rectPatt = ControlSB.RectanglePattern(coord,                # phaseCenter
                                      1.0,                  # longitudeLength
                                      1.0,                  # longitudeStep
                                      1.0,                  # latitudeLength
                                      1.0,                  # latitudeStep
                                      ControlSB.Longitude,  # direction
                                      0.0,                  # scanVelocity
                                      0.0)                  # orientiation

source = ControlSB.Source("",                   #     string sourceName;
                          coord,                #     J2000Coord coord;
                          0.0,                  #     double sourceVelocity;
                          "",                   #     string sourceEphemeris;
                          0.0,                  #     double pMRa;
                          0.0,                  #     double pMDec;
                          0,                    #     boolean nonSiderealMotion;
                          ControlSB.Emphemeris, #     SolarSystem solarSystemObject;
                          coord,                #     J2000Coord referenceCoord;
                          10.0,                 #     double intTimeReference;
                          0.1,                  #     double timeSource;
                          [],                   #     SourcePropertiesSeq sourcePropertiesList;
                          ControlSB.Circle,     #     FieldPattern pattern;
                          circlePatt,           #     CirclePattern circle;
                          spiralPatt,           #     SpiralPattern spiral;
                          absPatt,              #     AbsolutePointingPattern absPointing;
                          offPatt,              #     OffsetPointingPattern offPointing;
                          rectPatt,             #     RectanglePattern rectangle;
                          0.0)                  #     double parallax;

TRACKING_TIME = 60.0
POLLING_TIME = 1.0
TRACKING_ERROR = 0.01
NO_PERR_TO_DISCARD = 5

source.coord.ra = 21.0 * 15.0 * math.pi / 180.0
source.coord.dec = 70.0 * math.pi / 180.0

logInfo("Moving to the first source (RA, DEC) = (" + str(source.coord.ra * 180.0 / (math.pi * 15.0)) +
        ", " + str(source.coord.dec * 180.0 / math.pi) + ")")
antenna.setDirection(source)

error = 2.0 * TRACKING_ERROR
logInfo("Tracking source...")
elapsedTime = 0
count = 1
while error > TRACKING_ERROR and elapsedTime < TRACKING_TIME:
    time.sleep(POLLING_TIME)
    if count > NO_PERR_TO_DISCARD:
        (xerr, yerr) = antenna.getPointingError()
        error = math.sqrt(xerr * xerr + yerr * yerr)
        logInfo("Pointing error = (" + str(xerr) + ", " + str(yerr) + ")" +
                "[" + str(error) + "]")
    else:
        logInfo("Discarding " + str(count) + " pointing error")
    elapsedTime = elapsedTime + POLLING_TIME
    count = count + 1
if elapsedTime >= TRACKING_TIME:
    logCritical("Timeout reached when waiting for the system to track the source")
else:
    logInfo("Tracking for 1 minute...")
    time.sleep(60)


source.coord.ra = 21.0 * 15.0 * math.pi / 180.0
source.coord.dec = 10.0 * math.pi / 180.0

logInfo("Moving to the second source (RA, DEC) = (" + str(source.coord.ra * 180.0 / (math.pi * 15.0)) +
        ", " + str(source.coord.dec * 180.0 / math.pi) + ")")
antenna.setDirection(source)

error = 2.0 * TRACKING_ERROR
logInfo("Tracking source...")
elapsedTime = 0
count = 1
while error > TRACKING_ERROR and elapsedTime < TRACKING_TIME:
    time.sleep(POLLING_TIME)
    if count > NO_PERR_TO_DISCARD:
        (xerr, yerr) = antenna.getPointingError()
        error = math.sqrt(xerr * xerr + yerr * yerr)
        logInfo("Pointing error = (" + str(xerr) + ", " + str(yerr) + ")" +
                "[" + str(error) + "]")
    else:
        logInfo("Discarding " + str(count) + " pointing error")
    elapsedTime = elapsedTime + POLLING_TIME
    count = count + 1
if elapsedTime >= TRACKING_TIME:
    logCritical("Timeout reached when waiting for the system to track the source")
else:
    logInfo("Tracking for 1 minute...")
    time.sleep(60)

logInfo("End test")
