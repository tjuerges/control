# Get the Array CCL object.
array = getArray()
logInfo("Array object status: " + array.status())

# Get the Scheduling Block.
schedBlk = getSB()
logInfo("Project ID: " + schedBlk.projectId)
logInfo("Observation Unit ID: " + schedBlk.obsUnitSetId)
logInfo("Scheduling Block ID: " + schedBlk.sbId)
logInfo("Session ID: " + schedBlk.sessionId)
