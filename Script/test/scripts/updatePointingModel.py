#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# rhiriart  2006-01-31  created
#

#************************************************************************
#   NAME
# 
#   SYNOPSIS
# 
#   DESCRIPTION
#
#   FILES
#
#   ENVIRONMENT
#
#   RETURN VALUES
#
#   CAUTIONS
#
#   EXAMPLES
#
#   SEE ALSO
#
#   BUGS     
#
#------------------------------------------------------------------------
#

import sys, getopt
import ScriptExecutorExceptions
import ScriptExecutorExceptionsImpl
from Acspy.Clients.SimpleClient import PySimpleClient

MASTER_COMP='CONTROL/MASTER'

def usage():
    print """
updatePointingModel

Updates the pointing model. This function creates a ManualArray with
the provided antenna names and updates their pointing models by means
of the ManualArray's ScriptExecutor.

Usage: 
   updatePointingModel ANTENNA1 [ANTENNA2 ...] POINTING-MODEL-FILE
"""

def updatePointingModel(antennaList, pmFileName):
    client = PySimpleClient()
    master = client.getComponent(MASTER_COMP)

    manualArrayName = master.createManualArray(antennaList)
    manualArray = client.getComponent(manualArrayName)

    scexecName = manualArray.getExecutor()
    scexec = client.getComponent(scexecName)

    # Incomplete. The update pointing model CCL commands should be
    # invoked. For now, just getting the status of the array.
    
    try:
        scexec.runSource("array=getArray()\nlogInfo(array.status())")
    except ScriptExecutorExceptions.SyntaxErrorEx, ex:
        print "Syntax error exception"
        helperExcObj = ScriptExecutorExceptionsImpl.SyntaxErrorExImpl(exception=ex)
        helperExcObj.Print()
    except ScriptExecutorExceptions.ExecutionErrorEx, ex:
        print "Execution exception"
        helperExcObj = ScriptExecutorExceptionsImpl.ExecutionErrorExImpl(exception=ex)
        helperExcObj.Print()

    client.releaseComponent(scexecName)
    client.releaseComponent(manualArrayName)
    client.releaseComponent(MASTER_COMP)
    client.disconnect()

if __name__ == "__main__":
    # No options right now, may be in the future.
    try:
        opts, args = getopt.getopt(sys.argv[1:], "", [])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    if len(args) < 2:
        print "Either the antenna list or the pointing model is missing."
        usage()
        sys.exit(2)
    
    pmFile = args[-1:]
    antennas = []
    for a in args[:-1]:
        antennas.append(a)
        
    updatePointingModel(antennas, pmFile)

#
# ___oOo___
