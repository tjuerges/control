# Example script.
# This script demonstrates the use of CCLAntenna setDirection() and getPointingError()
# functions.
#
# $Id$

import ControlSB
import time
import math

TRACK_TIMEOUT = 180            # timeout for the antenna to move and track a source
SOURCE_TRACKING_ERROR = 0.1    # maximum source tracking error

array = getArray()
antenna = array.getAntenna('CONTROL/ALMA001')

#----------------------------------------------------------------
# Create a source object with hardcoded information.

logInfo("Moving to the first source.")

ra = 21.0 * 15.0 * math.pi / 180.0
dec = 70.0 * math.pi / 180.0

coord = ControlSB.J2000Coord(ra,   # RA
                             dec)   # Dec
circlePatt = ControlSB.CirclePattern(coord) # phaseCenter
spiralPatt = ControlSB.SpiralPattern(coord) # phaseCenter
absPatt = ControlSB.AbsolutePointingPattern([coord]) # phaseCenter
offPatt = ControlSB.OffsetPointingPattern([coord]) # phaseCenter
rectPatt = ControlSB.RectanglePattern(coord,                # phaseCenter
                                      1.0,                  # longitudeLength
                                      1.0,                  # longitudeStep
                                      1.0,                  # latitudeLength
                                      1.0,                  # latitudeStep
                                      ControlSB.Longitude,  # direction
                                      0.0,                  # scanVelocity
                                      0.0)                  # orientiation

source1 = ControlSB.Source("",                   #     string sourceName;
                           coord,                #     J2000Coord coord;
                           0.0,                  #     double sourceVelocity;
                           "",                   #     string sourceEphemeris;
                           0.0,                  #     double pMRa;
                           0.0,                  #     double pMDec;
                           0,                    #     boolean nonSiderealMotion;
                           ControlSB.Emphemeris, #     SolarSystem solarSystemObject;
                           coord,                #     J2000Coord referenceCoord;
                           10.0,                 #     double intTimeReference;
                           0.1,                  #     double timeSource;
                           [],                   #     SourcePropertiesSeq sourcePropertiesList;
                           ControlSB.Circle,     #     FieldPattern pattern;
                           circlePatt,           #     CirclePattern circle;
                           spiralPatt,           #     SpiralPattern spiral;
                           absPatt,              #     AbsolutePointingPattern absPointing;
                           offPatt,              #     OffsetPointingPattern offPointing;
                           rectPatt,             #     RectanglePattern rectangle;
                           0.0)                  #     double parallax;

# Move the antenna to the source object.
antenna.setDirection(source1)

sleepTime = 1.0    # Time to sleep between each cycle
elapsedTime = 0

# Wait until the antenna is tracking the source
(xerr, yerr) = antenna.getPointingError()
error = math.sqrt(xerr*xerr+yerr*yerr)
logInfo('Pointing Error: ('+ str(xerr) + ", " + str(yerr) + ") [" + str(error) +"]")
while error > SOURCE_TRACKING_ERROR and elapsedTime < TRACK_TIMEOUT:
    (xerr, yerr) = antenna.getPointingError()
    error = math.sqrt(xerr*xerr+yerr*yerr)
    logInfo('Pointing Error: ('+ str(xerr) + ", " + str(yerr) + ") [" + str(error) +"]")
    time.sleep(sleepTime)
    elapsedTime = elapsedTime + sleepTime
if elapsedTime >= TRACK_TIMEOUT:
    logCritical('Error pointing the antenna. Timeout reached.')

# Track the source for some time.
logInfo("Tracking the source for 30 seconds...")
time.sleep(30)

#----------------------------------------------------------------
# Try another source
logInfo("Moving to the second source.")
ra2 = 15.0 * 15.0 * math.pi / 180.0
dec2 = 50.0 * math.pi / 180.0

coord2 = ControlSB.J2000Coord(ra2,   # RA
                              dec2)   # Dec

source2 = ControlSB.Source("",                   #     string sourceName;
                           coord2,                #     J2000Coord coord;
                           0.0,                  #     double sourceVelocity;
                           "",                   #     string sourceEphemeris;
                           0.0,                  #     double pMRa;
                           0.0,                  #     double pMDec;
                           0,                    #     boolean nonSiderealMotion;
                           ControlSB.Emphemeris, #     SolarSystem solarSystemObject;
                           coord2,                #     J2000Coord referenceCoord;
                           10.0,                 #     double intTimeReference;
                           0.1,                  #     double timeSource;
                           [],                   #     SourcePropertiesSeq sourcePropertiesList;
                           ControlSB.Circle,     #     FieldPattern pattern;
                           circlePatt,           #     CirclePattern circle;
                           spiralPatt,           #     SpiralPattern spiral;
                           absPatt,              #     AbsolutePointingPattern absPointing;
                           offPatt,              #     OffsetPointingPattern offPointing;
                           rectPatt,             #     RectanglePattern rectangle;
                           0.0)                  #     double parallax;

# Move the antenna to the source object.
antenna.setDirection(source2)

elapsedTime = 0

# Wait until the antenna is tracking the source
(xerr, yerr) = antenna.getPointingError()
error = math.sqrt(xerr*xerr+yerr*yerr)
logInfo('Pointing Error: ('+ str(xerr) + ", " + str(yerr) + ") [" + str(error) +"]")
while error > SOURCE_TRACKING_ERROR and elapsedTime < TRACK_TIMEOUT:
    (xerr, yerr) = antenna.getPointingError()
    error = math.sqrt(xerr*xerr+yerr*yerr)
    logInfo('Pointing Error: ('+ str(xerr) + ", " + str(yerr) + ") [" + str(error) +"]")
    time.sleep(sleepTime)
    elapsedTime = elapsedTime + sleepTime
if elapsedTime >= TRACK_TIMEOUT:
    logCritical('Error pointing the antenna. Timeout reached.')

# Track the source for some time.
logInfo("Tracking the source for 30 seconds ...")
time.sleep(30)

#----------------------------------------------------------------
# Move the antenna now with horizon coordinates.
logInfo("Moving antenna to azimuth = 90.0 deg, elevation = 15.0 deg.")
azimuth = math.pi / 2.0
elevation = 15.0 * math.pi / 180.0
antenna.setDirection(azel=(azimuth, elevation))
