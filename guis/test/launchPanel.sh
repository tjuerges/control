#! /bin/sh
#A simple shell script to launch the AntennaStatusGui for stand alone testing.
#
#David Hunter <dhunter@nrao.edu>

acsStartJava alma/exec/acsplugins/test/PluginStarter alma.control.gui.antennachessboard.AntennaChessboardPlugin guiTestProperties &> panelTest.log

#
# O_o

