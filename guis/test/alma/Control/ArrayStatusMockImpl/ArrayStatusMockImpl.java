package alma.Control.ArrayStatusMockImpl;

import alma.ACS.ComponentStates;
import alma.Control.ArrayCorrelatorData;
import alma.Control.ArrayObservingData;
import alma.Control.ArrayPointingData;
import alma.Control.ArrayReceiverData;
import alma.Control.ArrayStatusMockOperations;
import alma.Control.ArraySummaryData;
import alma.Control.ArrayWeatherData;
import alma.Control.Baseband;
import alma.ControlExceptions.ArrayStatusException;
import alma.acs.component.ComponentLifecycle;
import alma.acs.component.ComponentLifecycleException;
import alma.acs.container.ContainerServices;

/**
 * Class which mocks up an API for populating the array status panel.
 * @author Steve Harrington
 *
 */
public class ArrayStatusMockImpl implements ComponentLifecycle,
		ArrayStatusMockOperations 
{
	private ContainerServices containerServices;
	private float ambTemp = 0.0f;
	private float opacity230GHz = 0.0f;
	private short windDirection = 0;
	private int counter = 0;
	
	
	public void aboutToAbort() 
	{
		// TODO Auto-generated method stub
	}

	public void cleanUp() 
	{
		// TODO Auto-generated method stub
	}

	public void execute() throws ComponentLifecycleException 
	{
		// TODO Auto-generated method stub
	}

	public void initialize(ContainerServices containerServices)
			throws ComponentLifecycleException 
	{
		this.containerServices = containerServices;
	}

	public ArrayCorrelatorData getArrayCorrelatorData()
			throws ArrayStatusException 
	{
		ArrayCorrelatorData retVal = new ArrayCorrelatorData();
		
		retVal.referenceFrequency = 5.678f;
		retVal.firstLoFrequency = 6.789f;
		retVal.basebandOne = new Baseband(((short)1), 1.5f, 1.6f, alma.NetSidebandMod.NetSideband.LSB);
		retVal.basebandTwo = retVal.basebandOne;
		retVal.basebandThree = retVal.basebandTwo;
		retVal.basebandFour = retVal.basebandThree;
		
		return retVal;
	}

	public ArrayObservingData getArrayObservingData()
			throws ArrayStatusException 
	{
		ArrayObservingData retVal = new ArrayObservingData();
		
		retVal.scan = 1;
		retVal.schedBlockElapsedTime = 650 + counter;
		retVal.schedBlockEstimatedTime = 750 + counter++;
		retVal.schedBlockName = "Orion test 12/24/2007";
		retVal.schedMode = "interactive";
	
		return retVal;
	}

	public ArrayPointingData getArrayPointingData() throws ArrayStatusException 
	{
		ArrayPointingData retVal = new ArrayPointingData();
		
		retVal.az = Math.toRadians(90.0) + Math.random();
		retVal.azOffset = 10.123f;
		
		retVal.el = retVal.az;
		retVal.elOffset = retVal.azOffset;
		retVal.maxAzErr = 1.123f;
		retVal.maxElErr = 1.123f;
		
		retVal.dec = Math.toRadians(180.0) + Math.random();
		retVal.decOffset = 1.123f;
		
		retVal.ra = retVal.dec;
		retVal.raOffset = retVal.decOffset;
		
		return retVal;
	}

	public ArrayReceiverData getArrayReceiverData() throws ArrayStatusException 
	{
		ArrayReceiverData retVal = new ArrayReceiverData();
		retVal.currentBand = 1;
		retVal.standbyBandOne = 2;
		retVal.standbyBandTwo = 3;
		return retVal;
	}

	public ArraySummaryData getArraySummaryData() throws ArrayStatusException 
	{
		ArraySummaryData retVal = new ArraySummaryData();
		retVal.arrayName = "CONTROL/Array001";
		retVal.sourceName = "Orion";
		
		retVal.onlineAntennaCount = 5;
		retVal.degradedAntennaCount = (short)Math.floor(Math.random() * 100);
				
		return retVal;
	}

	public ArrayWeatherData getArrayWeatherData() throws ArrayStatusException 
	{
		ArrayWeatherData retVal = new ArrayWeatherData();
		retVal.ambientTemp = ambTemp += .05;
		retVal.opacity230GHz = opacity230GHz +=.05;
		retVal.opacityRefFrequency = retVal.opacity230GHz + .03f;
		retVal.seeing = retVal.opacity230GHz + .02f;
		retVal.windDirection = windDirection++;
		retVal.windSpeed = retVal.opacity230GHz += .04;

		return retVal;
	}

	public ComponentStates componentState() {
		return containerServices.getComponentStateManager().getCurrentState();
	}

	public String name() {
		return containerServices.getName();
	}
}
