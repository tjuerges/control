/*
 * ALMA - Atacama Large Millimiter Array (c) European Southern Observatory, 2011
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */
package alma.control.mountpanel.test;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import alma.control.gui.antennamount.pointingpanel.ExpertPanel;
import alma.control.gui.antennamount.utils.editor.TextEditor;

/**
 * A very simple class to test the values inserted in the text fields.
 * <P>
 * TextFields are built the same way they are in the {@link ExpertPanel}.
 * 
 * @author acaproni
 *
 */
public class TextFieldsTester extends JFrame implements ActionListener {
	
	// The widgets to insert Az/El, RA/Dec, epoch, equinoxTE, parallax, proper motion
    private TextEditor azTE;
    private TextEditor elTE;
    private TextEditor raTE;
    private TextEditor decTE;
    
    private final JButton showValuesBtn = new JButton("Show values");
	
	/**
	 * Constructor
	 */
	public TextFieldsTester() {
		super("TextFieldTester");
		setLayout(new BorderLayout());
		
		JPanel fieldsPanel = new JPanel();
		BoxLayout bLayout = new BoxLayout(fieldsPanel, BoxLayout.X_AXIS);
		fieldsPanel.setLayout(bLayout);
		
		
		// The separators are the same for Az and El
		char[] seps = new char[] {0xb0, '\'', '.' }; 
		
		// Set min and max for Az
		int[] minsAz = new int[] { -270,0,0,0 };
		int[] maxsAz = new int[] { +270,59,59,99 };
		
		// Set min and max for El
		int[] minsEl = new int[] { 2,0,0,0 };
		int[] maxsEl = new int[] { 88,59,59,99 };
		
		// Builds the widgets
		azTE = new TextEditor(seps,minsAz,maxsAz,-2700000.00,2700000.00);
		azTE.setColumns(14);
		elTE = new TextEditor(seps,minsEl,maxsEl,20000.00,885359.99);
		elTE.setColumns(14);
		
		// DA/DEC
		
		// The separators for RA and dec
		char[] decSeps = new char[] {0xb0, '\'', '.' };
		char[] raSeps=new char[] {':',':','.' };
		
		// Set min and max for RA
		int[] minsRA = new int[] { 0,0,0,0 };
		int[] maxsRA = new int[] { 23,59,59,99 };
		
		// Set min and max for Dec
		int[] minsDec = new int[] { -90,0,0,0 };
		int[] maxsDec = new int[] { 90,59,59,99 };
		
		// Builds the widgets
		raTE = new TextEditor(raSeps,minsRA,maxsRA,0.0,235959.99);
		raTE.setColumns(14);
		decTE = new TextEditor(decSeps,minsDec,maxsDec,-900000.00,900000.00);
		decTE.setColumns(14);
		
		JPanel azPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		azPanel.add(new JLabel("AZ="));
		azPanel.add(azTE);
		
		JPanel elPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		azPanel.add(new JLabel("EL="));
		azPanel.add(elTE);
		
		JPanel raPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		azPanel.add(new JLabel("RA="));
		azPanel.add(raTE);
		
		JPanel decPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		azPanel.add(new JLabel("DC="));
		azPanel.add(decTE);
		
		fieldsPanel.add(azPanel);
		fieldsPanel.add(elPanel);
		fieldsPanel.add(raPanel);
		fieldsPanel.add(decPanel);
		add(fieldsPanel,BorderLayout.CENTER);
		
		JPanel btnPnl = new JPanel(new FlowLayout(FlowLayout.CENTER));
		btnPnl.add(showValuesBtn);
		add(btnPnl,BorderLayout.SOUTH);
		showValuesBtn.addActionListener(this);
		
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setResizable(false);
		pack();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==showValuesBtn) {
			System.out.println("*** AZ ***");
			System.out.println("Text=  ["+azTE.getText()+"]");
			System.out.println("Value= ["+azTE.getValue()+"]");
			
			System.out.println("*** EL ***");
			System.out.println("Text=  ["+elTE.getText()+"]");
			System.out.println("Value= ["+elTE.getValue()+"]");
			
			System.out.println("*** RA ***");
			System.out.println("Text=  ["+raTE.getText()+"]");
			System.out.println("Value= ["+raTE.getValue()+"]");
			
			System.out.println("*** DEC ***");
			System.out.println("Text=  ["+decTE.getText()+"]");
			System.out.println("Value= ["+decTE.getValue()+"]");
		}
		
	}
	
	public static void main(String[] args) {
		TextFieldsTester tester = new TextFieldsTester();
		tester.setVisible(true);
	}
	
}
