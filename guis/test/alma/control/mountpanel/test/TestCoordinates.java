package alma.control.mountpanel.test;

import alma.control.gui.antennamount.mount.ISubreflector.Coordinates;

import junit.framework.TestCase;

public class TestCoordinates extends TestCase {
	
	/**
	 * Constructor
	 *
	 */
	public TestCoordinates() {
		super("ISubreflectorCoordsTest");
	}
	
	/**
	 * Constructor
	 *
	 */
	public TestCoordinates(String str) {
		super(str);
	}
	
	/**
	 * @see junit.framework.TestCase
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}
	
	/**
	 * @see junit.framework.TestCase
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Check the conversion between coordinates
	 * 
	 * @param args
	 */
	public void testConversion() throws Exception {
		Coordinates coords = new Coordinates(-5.005,0.0,11.27); 
		double[] mmCoords = coords.toMMCoordinate();
		assertEquals(3, mmCoords.length);
		
		Coordinates meter=Coordinates.fromMM(mmCoords[0], mmCoords[1], mmCoords[2]);
		
		assertEquals(coords,meter);
	}
	
	/**
	 * Check if the equals method works
	 * @throws Exception
	 */
	public void testEquals() throws Exception {
		Coordinates coords = new Coordinates(5.005,-0.0001,0.27);
		assertTrue(coords.equals(coords));
		
		Coordinates coords2 = new Coordinates(coords.x.doubleValue(),coords.y.doubleValue(),coords.z.doubleValue());
		
		assertEquals(coords,coords2);
		assertEquals(coords2,coords);
		
		Coordinates coords3 = new Coordinates(-1.5,0.025,-2);
		assertFalse(coords3.equals(coords2));
		assertFalse(coords2.equals(coords3));
	}

}
