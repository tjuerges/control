/*
 * ALMA - Atacama Large Millimiter Array (c) European Southern Observatory, 2007
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */
package alma.control.gui.dartboard;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JRootPane;

import alma.acs.container.ContainerServices;
import alma.acs.gui.util.panel.IPanel;

/**
 * The component to launch the dardboard outside of the OMC.
 * 
 * @author acaproni
 *
 */
public class DartBoardPanel extends JRootPane implements IPanel {
	

	/**
	 * The {@link JFrame}
	 */
	private JFrame frame=null;
	
	/**
	 * The dartboard
	 */
	private DartboardPlugin dartboard;
	
	/**
	 * Container services
	 */
	private ContainerServices containerSvcs=null;

	/**
	 * Constructor
	 */
	public DartBoardPanel() {
		super();
		initialize();
	}
	
	/**
	 * Constructor
	 * 
	 * @param frame The frame showing this panel
	 */
	public DartBoardPanel(JFrame frame) {
		super();
		this.frame=frame;
		initialize();
	}
	
	/**
	 * Init the GUI
	 */
	private void initialize() {
		setLayout(new BorderLayout());
		
		dartboard=new DartboardPlugin(this);
		add(dartboard,BorderLayout.CENTER);
	}
	
	/**
	 * @see IPanel
	 */
	@Override
	public boolean isOMCPlugin() {
		return false;
	}

	/**
	 * @see IPanel
	 */
	@Override
	public void setACSContainerServices(ContainerServices csvcs) {
		containerSvcs=csvcs;
	}

	/**
	 * @see IPanel
	 */
	@Override
	public void start() throws Exception {
		if (containerSvcs==null) {
			throw new IllegalStateException("ContainerService not initialized");
		}
		dartboard.setACSContainerServices(containerSvcs);
		dartboard.start();
	}

	/**
	 * @see IPanel
	 */
	@Override
	public void stop() throws Exception {
		if (containerSvcs==null) {
			throw new IllegalStateException("ContainerService not initialized");
		}
		dartboard.stop();
	}
}
