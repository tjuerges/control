/*
 * ALMA - Atacama Large Millimiter Array (c) European Southern Observatory, 2008
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

/** 
 * @author  acaproni
 * @version $Id$
 * @since    
 */

package alma.control.gui.dartboard;

import org.omg.CORBA.DoubleHolder;
import org.omg.CORBA.LongHolder;

import alma.ACSErr.ErrorTrace;
import alma.Control.MountController;
import alma.Control.HorizonDirection;
import alma.Control.MountControllerPackage.PointingData;
import alma.Control.Common.Util;
import alma.ModeControllerExceptions.MountFaultEx;
import alma.acs.container.ContainerServices;
import alma.common.gui.components.selector.ComponentSelector;
import alma.common.gui.components.selector.SelectorComponentEvent;
import alma.common.gui.components.selector.SelectorComponentListener;
import alma.control.gui.antennamount.HeartbeatChecker;
import alma.control.gui.antennamount.StatusLine;
import alma.control.gui.antennamount.VisibleConnectionStatus;
import alma.control.gui.antennamount.mount.ACSComponentsManager;
import alma.control.gui.antennamount.mount.MountConnectionListener;
import alma.control.gui.antennamount.mount.ValueHolder;
import alma.exec.extension.subsystemplugin.PluginContainerServices;
import alma.exec.extension.subsystemplugin.SubsystemPlugin;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.FlowLayout;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import dartboard.DartboardPanel;

/**
 * 
 * A plugin for exec showing the dartboard
 *
 */
public class DartboardPlugin extends JComponent
implements SubsystemPlugin, 
Runnable,
SelectorComponentListener 
   {
	
	// The dartboard
	private DartboardPanel dartboard = new DartboardPanel();
	
	// The mount controller
	private MountController controller;
	
	/**
	 * The name of the component to connect to returned by the selector
	 */
	private String dartboardComponentName=null;

	// The wildcarded IDL (i.e. the type of the component) to select
	private static final String ANTENNA_IDL="*/Antenna:*";
	
	// The plugin container services
	private ContainerServices pluginCS;
	
	// The selector and its panel
	private ComponentSelector selector;
	private JPanel selectorPnl;
	
	// The thread to update the position of the telescope
	private Thread thread=null; 
	
	// Signal the thread to terminate
	private volatile boolean terminateThread;
	
	// The time interval (msec) to refresh the position of the telescope
	private static final int POSITION_UPDATE_INTERVAL=1500;
	
	// Max number of consecutive errors from the mount component before
	// giving up and stop the update thread.
	private static final int MAX_ERRORS=10;
	
	// The helper to connect and release components
	private ACSComponentsManager compManager;
	
	/**
     * An object to check and report if all the thread are runnign as expected
     */
    private final HeartbeatChecker hbChecker = new HeartbeatChecker();
	
	// The status line at the bottom of the panel
	private StatusLine statusLine=new StatusLine(hbChecker);
	
	// To know if the plugin is running inside OMC
	private boolean runsInsideOMC=false;
	
	// The connection listener
    private MountConnectionListener connectionListener = new VisibleConnectionStatus(statusLine);
    
    /**
     * <code>true</code> it the plugin has been stopped.
     */
    private volatile boolean stopped=false;
    
    /**
     * The panel showing this plugin
     */
    private final alma.control.gui.dartboard.DartBoardPanel owner;
    
    /**
	 * Constructor
	 *
	 */
	public DartboardPlugin() {
		super();
		this.owner=null;
		setLayout(new BorderLayout());
		initialize();
		
		// Init dartboard values to 0
		dartboard.setTelescopeAzimuth(0);
		dartboard.setTelescopeElevation(0);
		dartboard.setTelescopeDestinationAzimuth(0);
		dartboard.setTelescopeDestinationElevation(0);
	}
    
	/**
	 * Constructor
	 *
	 * @param owner The panel showing this plugin
	 */
	public DartboardPlugin(alma.control.gui.dartboard.DartBoardPanel owner) {
		super();
		this.owner=owner;
		setLayout(new BorderLayout());
		initialize();
		
		// Init dartboard values to 0
		dartboard.setTelescopeAzimuth(0);
		dartboard.setTelescopeElevation(0);
		dartboard.setTelescopeDestinationAzimuth(0);
		dartboard.setTelescopeDestinationElevation(0);
	}
	
	/**
	 * Initialize the plugin
	 *
	 */
	private void initialize() {
		setLayout(new BorderLayout());
		dartboard.setPreferredSize(new Dimension(120,200));
		add(dartboard,BorderLayout.CENTER);
		add(statusLine,BorderLayout.SOUTH);
		dartboard.setError(true);
		
		selectorPnl = new JPanel(new FlowLayout(FlowLayout.CENTER));
		add(selectorPnl,BorderLayout.NORTH);
		selectorPnl.setVisible(false);
	}
	
	/**
	 * Method used by the plugin interface in EXEC.
	 * Start the application 
	 * @see alma.exec.extension.subsystemplugin.IPauseResume
	 * 
	 * @throws Exception
	 */
	public void start() throws Exception {
		stopped=false;
		if (!runsInsideOMC) {
			selector = new ComponentSelector(ANTENNA_IDL,pluginCS,this);
			selectorPnl.add(selector);
			selectorPnl.setVisible(true);
		}
		validate();
	}
	
	
	/**
	 * Method used by the plugin interface in EXEC.
	 * Stop the application 
	 * @see alma.exec.extension.subsystemplugin.IPauseResume
	 * 
	 * @throws Exception
	 */
	public void stop() throws Exception {
		hbChecker.close();
		stopped=true;
		close();
		pluginCS=null;
	}
	
	/**
	 * Set if the Dartboard is an OMC plugin
	 */
	public void setAsOMCPlugin() {
		runsInsideOMC=true;
	}
	
	/**
	 * Release all the components
	 */
	private void close() {
		class CloseThread extends Thread {
			public void run() {
				stopThread(true);
				disconnectComponents();
			}
		}
		Thread stopThread = new CloseThread();
		stopThread.setDaemon(true);
		stopThread.setName("DartboardPlugin.stop");
		stopThread.start();
	}
	
	/**
	 * Set the plugin container services
	 * 
	 * @see alma.exec.extension.subsystemplugin.SubsystemPlugin
	 */
	public void setServices (PluginContainerServices ctrl) {
		if (ctrl==null) {
			throw new IllegalArgumentException("Invalid null PluginContainerServices");
		}
		setACSContainerServices(ctrl);
	}
	
	/**
	 * Set ACS container services
	 */
	public void setACSContainerServices (ContainerServices ctrl) {
		pluginCS=(ContainerServices)ctrl;
	}
	
	/**
	 * Run in restricted mode
	 * 
	 * @see alma.exec.extension.subsystemplugin.SubsystemPlugin
	 */
	public boolean runRestricted (boolean restricted) throws Exception {
		return restricted;
	}

	/**
	 * The event generated when the user presses connect in the 
	 * dialog to select a controller
	 */
	public void connectPerformed(SelectorComponentEvent sce) {
		if (sce.getComponentName()==null || sce.getComponentName().length()==0) {
			// It is an error in the selector because it should always return
			// a name of a component
			//
			// We ignore the problem but write a message in stderr
			System.err.println("DartboardPlugin.connectPerformed(...): Wrong component name got from ComponentSelector");
		}
		selectorPnl.setEnabled(false);
		//System.out.println("Conecting to " +sce.getComponentName());
		compManager=new ACSComponentsManager((ContainerServices)pluginCS);
		class ConnectionRunnable implements Runnable {
			public String compName;
		     public void run() {
		    	 try {
		    		 statusLine.startProgressAnimation();
		    		 statusLine.setMessage("Connecting "+compName);
		    		 controller=null;
	    			// Connect mount and mount controller
	    			compManager.connectComponents(compName);
	    			controller=compManager.getController();
	    			connectionListener.componentConnected(compName);
	    			statusLine.setMessage(compName+" connected");
		    	 } catch (Throwable t) {
		    		 t.printStackTrace(System.err);
		    		JOptionPane.showInternalMessageDialog(
		    				DartboardPlugin.this.owner,
		    				"Error connecting components:\n"+t.getMessage(), 
		    				"Connection error", 
		    				JOptionPane.ERROR_MESSAGE);
		    		compManager.close();
		    		dartboard.setError(true);
		    		return;
		    	 } finally {
		    		 statusLine.stopProgressAnimation();
		    		 selectorPnl.setEnabled(true);
		    		 DartboardPlugin.this.validate();
		    	 }
		    	 dartboard.setError(false);
		    	 startThread();
		     }
		};
		ConnectionRunnable doConnection = new ConnectionRunnable();
		dartboardComponentName=sce.getComponentName();
		doConnection.compName=sce.getComponentName();
		Thread connectThread = new Thread(doConnection);
		connectThread.setName("connectThread");
		connectThread.start();
	}
	
	/**
	 * Start the thread if it is not already started
	 *
	 */
	private void startThread() {
		if (thread!=null) {
			if (thread.isAlive()) {
				// It is already running
				return;
			}
		}
		terminateThread=false;
		thread=new Thread(this);
		thread.setName("DartboardPlugin");
		thread.setDaemon(true);
		thread.start();
		hbChecker.register(thread);
	}
	
	/**
	 * Stop the thread synchronously or asynchronously
	 * 
	 * @param sync If true the method waits the termination of the thread 
	 *             before returning
	 */
	private void stopThread(boolean sync) {
		terminateThread=true;
		if (!sync || thread==null) {
			return;
		}
		thread.interrupt();
		
		// Wait for the thread to terminate
		while (thread.isAlive()) {
			try {
				Thread.sleep(250);
			} catch (Exception e) {}
		}
		thread=null;
	}
	
	/**
	 * The thread to update the position of the telescope
	 */
	public void run() {
		LongHolder time=new LongHolder();
		ValueHolder<Double> az=new ValueHolder<Double>();
		ValueHolder<Double> el=new ValueHolder<Double>();
		ValueHolder<Double> commandedAz=new ValueHolder<Double>();
		ValueHolder<Double> commandedEl=new ValueHolder<Double>();
		int attempt=0;
		Throwable t=null;
		while (!terminateThread) {
			try {
				PointingData pData= controller.getPointingData();
				
				// Commanded AZ/EL
		        HorizonDirection pointing = pData.pointing;
				if (!pData.stopped) {
					HorizonDirection commanded = pData.commanded;
					commandedAz.setValue(commanded.az+pointing.az , pData.timestamp);
					commandedEl.setValue(commanded.el+pointing.el, pData.timestamp);
				} else {
					commandedAz.setValue(null);
					commandedEl.setValue(null);
				}
				
				// Actual AZ/EL
				HorizonDirection measured = pData.measured;
				az.setValue(measured.az + pointing.az, pData.timestamp);
				el.setValue(measured.el + pointing.el, pData.timestamp);
				
				
				dartboard.setTelescopeAzimuth(Math.toDegrees(az.getValue()));
				dartboard.setTelescopeElevation(Math.toDegrees(el.getValue()));
				dartboard.setTelescopeDestinationAzimuth(Math.toDegrees(commandedAz.getValue()));
				dartboard.setTelescopeDestinationElevation(Math.toDegrees(commandedEl.getValue()));
				attempt=0;
				connectionListener.componentResponseTimeOk();
			} catch (Throwable th) {
				if (th instanceof MountFaultEx) {
					ErrorTrace et = ((MountFaultEx)th).errorTrace;
					System.err.println(Util.getNiceErrorTraceString(et));
				} else {
					th.printStackTrace();
				} 
				t=th;
				attempt++;
				if (attempt>MAX_ERRORS) {
					//break; // give up.. too many errors!
				}
				connectionListener.componentTransientError();
			} finally {
				az.setValue(null);
				el.setValue(null);
				commandedAz.setValue(null);
				commandedEl.setValue(null);
			}
			hbChecker.ping(thread);
			try {
				Thread.sleep(POSITION_UPDATE_INTERVAL);
			} catch (Exception e) {
				continue;
			}
		}
		hbChecker.unregister(thread);
		attempt=0;
		/*if (attempt>MAX_ERRORS) {
			String msg ="Error getting positions from the controller";
			if (t!=null) {
				t.printStackTrace();
				msg+=":\n"+t.getMessage()+"\n";
			}
			msg+="Update interrupted.";
			dartboard.setError(true);
			JOptionPane.showMessageDialog(
					this.getRootPane(), 
					msg, 
					"Error", JOptionPane.ERROR_MESSAGE);
			return;
		}*/
	}
	
	private void disconnectComponents() {
		statusLine.setMessage("Disconnecting "+dartboardComponentName);
		statusLine.startProgressAnimation();
		try {
			if (compManager!=null) {
				compManager.close();
			}
		} catch (Throwable t) {
			System.err.println("Exception closing the components manager "+t.getMessage());
			t.printStackTrace();
		}
		compManager=null;
		controller=null;
		statusLine.stopProgressAnimation();statusLine.setMessage(dartboardComponentName+" disconnected");
		connectionListener.componentUnreliable(dartboardComponentName);
		dartboardComponentName=null;
	}
	
//	/**
//	 * {@link ComponentConnectionLost}
//	 */
//	public void componentConnLost(String name) {
//		if (stopped) {
//			return;
//		}
//		class CompDownThread extends Thread {
//			public void run() {
//				disconnectComponents();
//			}
//		}
//		CompDownThread t = new CompDownThread();
//		t.setDaemon(true);
//		t.setName("CompDownThread");
//		t.start();
//		JOptionPane.showMessageDialog(
//				this, 
//				name+" is down:\nDisconnecting components.", 
//				"Component unavailable", 
//				JOptionPane.ERROR_MESSAGE);
//	}
}
