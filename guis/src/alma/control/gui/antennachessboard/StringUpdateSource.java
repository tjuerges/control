package alma.control.gui.antennachessboard;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of the methods (and supporting data structures) for StringUpdatePublisher
 * @author Steve Harrington
 */
public class StringUpdateSource implements StringUpdatePublisher 
{
	private List<StringUpdateListener> listeners = new ArrayList<StringUpdateListener>();
	private String titleLine = null;
	private String line1 = "";
	private String line2 = "";
	
	public void addListener(StringUpdateListener listener) 
	{
		if(null != listener)
		{
			listeners.add(listener);
		}
	}

	public void removeListener(StringUpdateListener listener) 
	{
		if(null != listener)
		{
			listeners.remove(listener);
		}
	}
	
	public void notifyListeners() 
	{
		for(StringUpdateListener listener : listeners)
		{
			StringUpdateEvent updateEvt = new StringUpdateEvent(titleLine, line1, line2);
			listener.updateStrings(updateEvt);
		}
	}
	
	/**
	 * Sets both of the text lines and notifies listeners of the changes.
	 * @param line1 the new value for the first line of text.
	 * @param line2 the new value for the second line of text.
	 */
	public void setLineOneAndLineTwo(String line1, String line2)
	{
		this.line1 = line1;
		this.line2 = line2;
		notifyListeners();
	}
	
	/**
	 * Sets both of the text lines and notifies listeners of the changes.
	 * @param line1 the new value for the first line of text.
	 */
	public void setLineOne(String line1)
	{
		this.line1 = line1;
		notifyListeners();
	}
	
	/**
	 * Setter for the title line.
	 * @param title the new title line value.
	 */
	public void setTitleLine(String title)
	{
		this.titleLine = title;
		notifyListeners();
	}
}
