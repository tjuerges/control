package alma.control.gui.antennachessboard;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.ToolTipManager;

import alma.common.gui.chessboard.ChessboardDetailsPlugin;
import alma.common.gui.chessboard.ChessboardDetailsPluginListener;
import alma.control.gui.antennamount.chessboard.plugins.MountStatusPluginHolder;
import alma.exec.extension.subsystemplugin.PluginContainerServices;
import alma.exec.extension.subsystemplugin.SessionProperties;

/**
 * Panel which contains painted rectangles which depict the devices in an antenna's signal path.
 * @author Steve Harrington
 * @author David Hunter
 */
@SuppressWarnings("serial")
public class AntennaChessboardDetailsPlugin extends ChessboardDetailsPlugin implements MouseMotionListener, AntennaPresentationModelListener
{
    /**
     * Constructor
     * @param antennaNames the names of the antennas that were selected
     * @param owningChessboard the chessboard which 'owns' this plugin 
     * (in other words, the chessboard which launched this details plugin).
     * @param services the plugin container services.
     * @param pluginTitle the unique title of the plugin (to be used in the title bar of the plugin window on screen) and
     *        by the docking framework of EXEC to give the plugin a name which must be unique.
     * @param selectionTitle the title of the cell that was selected in the chessboard when this details plugin was launched;
     *        this may not be unique if, for example, multiple instances of a details display have been shown for a single 
     *        cell in the chessboard; say, cell S1 might have selectionTitle value of 'S1', pluginTitle values of:
     *        'S1 - 1', 'S1 - 2', 'S1 - 3', etc.
     */
    public AntennaChessboardDetailsPlugin(String antennaNames[], ChessboardDetailsPluginListener owningChessboard,
            PluginContainerServices services, String pluginTitle, String selectionTitle)
    {
        super(antennaNames, owningChessboard, services, pluginTitle, selectionTitle);
        this.antennaName = antennaNames[0];
        SessionProperties sessionProperties = services.getSessionProperties();
        this.arrayName = (sessionProperties.getProperty(ARRAY_NAME_PROPERTY) == null) ? UNKNOWN_ARRAY_NAME : sessionProperties.getProperty(ARRAY_NAME_PROPERTY);
        init();
    }

    @Override
    public String getPluginName() 
    {
        return PLUGIN_TITLE;
    }

    @Override
    public void replaceContents(String[] newAntennaNames) 
    {
        this.antennaName = newAntennaNames[0];
        this.devs.get("ai").setFirstLineTextValue(newAntennaNames[0]);
        updatePresentationModel();
        repaint();
    }

    @Override
    public boolean runRestricted(boolean restricted) throws Exception 
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void specializedStart()
    {
        this.defaultTooltipDismissDelay = ToolTipManager.sharedInstance().getDismissDelay();
        ToolTipManager.sharedInstance().setDismissDelay(120000);
    }

    @Override
    public void specializedStop() 
    {
        this.presentationModel.stopPolling();
        ToolTipManager.sharedInstance().setDismissDelay(this.defaultTooltipDismissDelay);
    }

    @Override
    public void notifyOfNotBusy()
    {
        this.setCursor(Cursor.getDefaultCursor());	
    }

    @Override
    public void notifyOfFullyInitialized() 
    {
        this.addPresentationModelListeners();
        this.presentationModel.startPolling();
        this.repaint();
    }

    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        for(SingleInstanceDeviceRectangle rect : rectangles)
        {
            rect.draw((Graphics2D)g, this);
        }
        Stroke oldStroke = ((Graphics2D)g).getStroke();
        ((Graphics2D)g).setStroke(new BasicStroke(3));

        for(ColoredLine2D line : lines) 
        {
            g.setColor(line.color);
            ((Graphics2D)g).draw(line.line);
        }
        ((Graphics2D)g).setStroke(oldStroke);
    }

    public void mouseDragged(MouseEvent e) 
    {
        // noop
    }

    public void mouseMoved(MouseEvent e) 
    {
        for(SingleInstanceDeviceRectangle rect : rectangles)
        {
            if(!rect.contains(e.getX(), e.getY()))
            {
                if(rect.isMouseInside()) 
                {
                    rect.mouseExited(e);
                }
            }
        }

        for(SingleInstanceDeviceRectangle rect : rectangles)
        {
            if(rect.contains(e.getX(), e.getY()))
            {
                rect.mouseEntered(e); 
            }
        }
    }

    private class ColoredLine2D
    {
        private Color color;
        private Line2D line;

        /**
         * Constructor.
         * @param color the color of the line.
         * @param line the 2d line.
         */
        public ColoredLine2D(Color color, Line2D line)
        {
            this.line = line;
            this.color = color;
        }

        /**
         * Constructor for a line with default color (black).
         * @param line
         */
        public ColoredLine2D(Line2D line)
        {
            this(Color.BLACK, line);
        }
    }

    private Dimension getMinSize()
    {
        int offsetX = 0;
        int offsetY = 0;

        for(SingleInstanceDeviceRectangle rect : rectangles)
        {
            if(rect.x > offsetX) {
                offsetX = rect.x;
            }
            if(rect.y > offsetY) {
                offsetY = rect.y;
            }
        }

        Dimension retVal = new Dimension(this.totalWidth + offsetX + PADDING, this.totalHeight + offsetY + PADDING);	
        return retVal;
    }

    private void updatePresentationModel() 
    {
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        this.presentationModel.stopPolling();
        this.presentationModel = new AntennaPresentationModel(pluginContainerServices, this.antennaName, this);
        this.presentationModel.start();
    }

    private void addPresentationModelListeners()
    {
        this.presentationModel.addPSAListener(devs.get("psa"));
        this.presentationModel.addPSAStatusListener(devs.get("psa"));
        this.presentationModel.addLlcListener(devs.get("llc"));
        this.presentationModel.addLlcStatusListener(devs.get("llc"));
        this.presentationModel.addSasListener(devs.get("sas"));
        this.presentationModel.addSasStatusListener(devs.get("sas"));
        this.presentationModel.addAntennaInfoListener(devs.get("ai"));
        this.presentationModel.addMountListener(devs.get("mount"));
        this.presentationModel.addMountStatusListener(devs.get("mount"));
        this.presentationModel.addPSDListener(devs.get("psd"));
        this.presentationModel.addPSDStatusListener(devs.get("psd"));
        this.presentationModel.addFloogListener(devs.get("floog"));
        this.presentationModel.addFloogStatusListener(devs.get("floog"));
        this.presentationModel.addLo2Pair0Listener(devs.get("lo2_0"));
        this.presentationModel.addLo2Pair0StatusListener(devs.get("lo2_0"));
        this.presentationModel.addLo2Pair1Listener(devs.get("lo2_1"));
        this.presentationModel.addLo2Pair1StatusListener(devs.get("lo2_1"));
        this.presentationModel.addLo2Pair2Listener(devs.get("lo2_2"));
        this.presentationModel.addLo2Pair2StatusListener(devs.get("lo2_2"));
        this.presentationModel.addLo2Pair3Listener(devs.get("lo2_3"));
        this.presentationModel.addLo2Pair3StatusListener(devs.get("lo2_3"));
        this.presentationModel.addCurrentBandStatusListener(devs.get("currentBand"));
        this.presentationModel.addCurrentBandListener(devs.get("currentBand"));
        this.presentationModel.addStandbyBand1Listener(devs.get("standbyBand1"));
        this.presentationModel.addStandbyBand1StatusListener(devs.get("standbyBand1"));
        this.presentationModel.addStandbyBand2Listener(devs.get("standbyBand2"));
        this.presentationModel.addStandbyBand2StatusListener(devs.get("standbyBand2"));
        this.presentationModel.addIfprocPol0Listener(devs.get("IFProc0"));
        this.presentationModel.addIfprocPol1StatusListener(devs.get("IFProc0"));
        this.presentationModel.addIfprocPol1Listener(devs.get("IFProc1"));
        this.presentationModel.addIfprocPol1StatusListener(devs.get("IFProc1"));
        //TODO: make sure that rectangles don't get 2 listners attached to them - right now they do
        for (String s: devs.keySet())
            this.presentationModel.addStatusListener(devs.get(s), s);
    }

    private void createRectanglesAndLines()
    {
        /* first column */
        Rectangle aiLocation = new Rectangle(START_X, START_Y, WIDTH, HEIGHT);
        HashMap<String, Object> aiDetails = new HashMap<String, Object>();
        aiDetails.put("deviceStatus", DeviceStatus.INVISIBLE_DEVICE);
        aiDetails.put("boxTitle", ANTENNA_TITLE);
        aiDetails.put("setFirstLineTextValue", this.antennaName);
        aiDetails.put("setFirstLineTextCentered", false);
        aiDetails.put("deviceName", "ai");
        aiDetails.put("secondLineTitle",  ARRAY_TITLE);
        aiDetails.put("setSecondLineTextValue", this.arrayName);
        aiDetails.put("setThirdLineTextTitle", TEMP_TITLE);
        aiDetails.put("setThirdLineTextValue", ZERO_TWO_DECIMAL_VALUE);
        aiDetails.put("setAllLinesBold", true);
        aiDetails.put("setAllLinesSameFontSize", true);
        buildDevRectangle(aiLocation, aiDetails);
        
        //Add the PSA
        Rectangle psaLocation = new Rectangle(START_X, START_Y + devs.get("ai").height + PAD_Y, WIDTH, HEIGHT);
        HashMap<String, Object> psaDetails = new HashMap<String, Object>();
        psaDetails.put("boxTitle", PSA_TITLE);
        psaDetails.put("deviceName", "psa");
        psaDetails.put("detailsPlugin", alma.Control.device.gui.PSA.DeviceChildPlugin.class);
        psaDetails.put("secondLineTitle",  OUTPUT_TITLE);
        psaDetails.put("setSecondLineTextValue", ON_VALUE + QUESTIONABLE_TEXT);
        psaDetails.put("setThirdLineTextTitle", TEMP_TITLE);
        psaDetails.put("setThirdLineTextValue", ZERO_TWO_DECIMAL_VALUE + AntennaPresentationModel.DEGREES_CHAR + CELSIUS_UNITS + QUESTIONABLE_TEXT);
        buildDevRectangle(psaLocation, psaDetails);
        
        //Add the LORR
        Rectangle lorrLocation = new Rectangle(START_X, devs.get("psa").y + devs.get("psa").height + PAD_Y * 3,
                                               WIDTH, HEIGHT);
        HashMap<String, Object> lorrDetails = new HashMap<String, Object>();
        lorrDetails.put("boxTitle", LORR_TITLE);
        lorrDetails.put("deviceName", LORR_TITLE);
        lorrDetails.put("detailsPlugin", alma.Control.device.gui.LORR.DeviceChildPlugin.class);
        buildDevRectangle(lorrLocation, lorrDetails);

        //Add the LLC
        Rectangle llcLocation = new Rectangle(START_X, devs.get("LORR").y + devs.get("LORR").height + PAD_Y,
                                              WIDTH, HEIGHT);
        HashMap<String, Object> llcDetails = new HashMap<String, Object>();
        llcDetails.put("boxTitle", LLC_TITLE);
        llcDetails.put("deviceName", "llc");
        //TODO: if we ever make a LLC GUI, put a link to it here.
        llcDetails.put("detailsPlugin", null);
        llcDetails.put("secondLineTitle",  PHOTONIC_REF_TITLE);
        llcDetails.put("setSecondLineTextValue", PHOTONIC_REF_1_VALUE + QUESTIONABLE_TEXT);
        buildDevRectangle(llcLocation, llcDetails);
        
        //Add the SAS
        Rectangle sasLocation = new Rectangle(START_X, devs.get("llc").y + devs.get("llc").height + PAD_Y, WIDTH, HEIGHT);
        HashMap<String, Object> sasDetails = new HashMap<String, Object>();
        sasDetails.put("boxTitle", SAS_TITLE);
        sasDetails.put("deviceName", "sas");
        //TODO: if we ever make a SAS GUI, put a link to it here.
        sasDetails.put("detailsPlugin", null);
        sasDetails.put("secondLineTitle",  TIME_REMAINING_TITLE);
        sasDetails.put("setSecondLineTextValue", ZERO_MINUTES_VALUE + QUESTIONABLE_TEXT);
        buildDevRectangle(sasLocation, sasDetails);

        /* second column */
        //Add mount
        Rectangle mountLocation = new Rectangle(START_X + devs.get("psa").width + PAD_X, START_Y, WIDTH, HEIGHT);
        HashMap<String, Object> mountDetails = new HashMap<String, Object>();
        mountDetails.put("boxTitle", MOUNT_TITLE);
        mountDetails.put("deviceName", "mount");
        mountDetails.put("detailsPlugin", MountStatusPluginHolder.class);
        mountDetails.put("secondLineTitle",  RA_TITLE);
        mountDetails.put("setSecondLineTextValue", RA_DEC_ZERO_VALUE + QUESTIONABLE_TEXT);
        mountDetails.put("setThirdLineTextTitle", DEC_TITLE);
        mountDetails.put("setThirdLineTextValue", RA_DEC_ZERO_VALUE + QUESTIONABLE_TEXT);
        buildDevRectangle(mountLocation, mountDetails);
        
        //Add the PSD
        Rectangle psdLocation = new Rectangle(START_X + devs.get("psa").width + PAD_X,
                                               START_Y + devs.get("mount").height + PAD_Y-2, WIDTH, HEIGHT);
        HashMap<String, Object> psdDetails = new HashMap<String, Object>();
        psdDetails.put("boxTitle", "PSD");
        psdDetails.put("deviceName", "psd");
        psdDetails.put("detailsPlugin", alma.Control.device.gui.PSD.DeviceChildPlugin.class);
        psdDetails.put("secondLineTitle",  OUTPUT_TITLE);
        psdDetails.put("setSecondLineTextValue", ON_VALUE + QUESTIONABLE_TEXT);
        psdDetails.put("setThirdLineTextTitle", TEMP_TITLE);
        psdDetails.put("setThirdLineTextValue", ZERO_TWO_DECIMAL_VALUE + AntennaPresentationModel.DEGREES_CHAR + CELSIUS_UNITS + QUESTIONABLE_TEXT);
        buildDevRectangle(psdLocation, psdDetails);
        
        //Add the FLOOG
        Rectangle floogLocation = new Rectangle(START_X + devs.get("psa").width + PAD_X,
                                               devs.get("psd").y + devs.get("psd").height + PAD_Y-2,
                                               WIDTH, HEIGHT);
        HashMap<String, Object> floogDetails = new HashMap<String, Object>();
        floogDetails.put("boxTitle", FLOOG_TITLE);
        floogDetails.put("deviceName", "floog");
        floogDetails.put("detailsPlugin", alma.Control.device.gui.FLOOG.DeviceChildPlugin.class);
        floogDetails.put("secondLineTitle",  FRINGE_TRACKING_TITLE);
        floogDetails.put("setSecondLineTextValue", ON_VALUE + QUESTIONABLE_TEXT);
        floogDetails.put("setThirdLineTextTitle", FREQUENCY_TITLE);
        floogDetails.put("setThirdLineTextValue", ZERO_MHZ_FREQ_VALUE + QUESTIONABLE_TEXT);
        buildDevRectangle(floogLocation, floogDetails);

        //Add LO2 0
        Rectangle lo2Bbpr0Location = new Rectangle(START_X + devs.get("floog").width + PAD_X,
                                                  devs.get("floog").y + devs.get("floog").height + PAD_Y-2,
                                                  WIDTH, HEIGHT);
        HashMap<String, Object> lo2Bbpr0Details = new HashMap<String, Object>();
        lo2Bbpr0Details.put("boxTitle", "LO2: BBPr0");
        lo2Bbpr0Details.put("deviceName", "lo2_0");
        //TODO: Once there is an LO2 GUI, put the plugin in here.
        lo2Bbpr0Details.put("detailsPlugin", null);
        lo2Bbpr0Details.put("deviceInstance", 0);
        lo2Bbpr0Details.put("secondLineTitle",  FRINGE_TRACKING_TITLE);
        lo2Bbpr0Details.put("setSecondLineTextValue", ON_VALUE + QUESTIONABLE_TEXT);
        lo2Bbpr0Details.put("setThirdLineTextTitle", FREQUENCY_TITLE);
        lo2Bbpr0Details.put("setThirdLineTextValue", ZERO_GHZ_FREQ_VALUE + QUESTIONABLE_TEXT);
        buildDevRectangle(lo2Bbpr0Location, lo2Bbpr0Details);
        
        //Add LO2 1
        Rectangle lo2Bbpr1Location = new Rectangle(START_X + devs.get("floog").width + PAD_X,
                                                  devs.get("lo2_0").y + devs.get("lo2_0").height + SMALL_PAD,
                                                  WIDTH, HEIGHT);
        HashMap<String, Object> lo2Bbpr1Details = new HashMap<String, Object>();
        //TODO: Once there is an LO2 GUI, put the plugin in here.
        lo2Bbpr1Details.put("boxTitle", "LO2: BBPr1");
        lo2Bbpr1Details.put("deviceName", "lo2_1");
        lo2Bbpr1Details.put("detailsPlugin", null);
        lo2Bbpr1Details.put("deviceInstance", 1);
        lo2Bbpr1Details.put("secondLineTitle",  FRINGE_TRACKING_TITLE);
        lo2Bbpr1Details.put("setSecondLineTextValue", ON_VALUE + QUESTIONABLE_TEXT);
        lo2Bbpr1Details.put("setThirdLineTextTitle", FREQUENCY_TITLE);
        lo2Bbpr1Details.put("setThirdLineTextValue", ZERO_GHZ_FREQ_VALUE + QUESTIONABLE_TEXT);
        buildDevRectangle(lo2Bbpr1Location, lo2Bbpr1Details);
        //Add LO2 2
        Rectangle lo2Bbpr2Location = new Rectangle(START_X + devs.get("floog").width + PAD_X,
                                                  devs.get("lo2_1").y + devs.get("lo2_1").height + SMALL_PAD,
                                                  WIDTH, HEIGHT);
        HashMap<String, Object> lo2Bbpr2Details = new HashMap<String, Object>();
        lo2Bbpr2Details.put("boxTitle", LO2_BBPR2_TITLE);
        lo2Bbpr2Details.put("deviceName", "lo2_2");
        lo2Bbpr2Details.put("detailsPlugin", null);
        lo2Bbpr2Details.put("deviceInstance", 2);
        lo2Bbpr2Details.put("secondLineTitle",  FRINGE_TRACKING_TITLE);
        lo2Bbpr2Details.put("setSecondLineTextValue", ON_VALUE + QUESTIONABLE_TEXT);
        lo2Bbpr2Details.put("setThirdLineTextTitle", FREQUENCY_TITLE);
        lo2Bbpr2Details.put("setThirdLineTextValue", ZERO_GHZ_FREQ_VALUE + QUESTIONABLE_TEXT);
        buildDevRectangle(lo2Bbpr2Location, lo2Bbpr2Details);

        //Add LO2 3
        Rectangle lo2Bbpr3Location = new Rectangle(START_X + devs.get("floog").width + PAD_X,
                                                  devs.get("lo2_2").y + devs.get("lo2_2").height + SMALL_PAD,
                                                  WIDTH, HEIGHT);
        HashMap<String, Object> lo2Bbpr3Details = new HashMap<String, Object>();
        lo2Bbpr3Details.put("boxTitle", LO2_BBPR3_TITLE);
        lo2Bbpr3Details.put("deviceName", "lo2_3");
        //TODO: Once there is an LO2 GUI, put the plugin in here.
        lo2Bbpr3Details.put("detailsPlugin", null);
        lo2Bbpr3Details.put("deviceInstance", 3);
        lo2Bbpr3Details.put("secondLineTitle",  FRINGE_TRACKING_TITLE);
        lo2Bbpr3Details.put("setSecondLineTextValue", ON_VALUE + QUESTIONABLE_TEXT);
        lo2Bbpr3Details.put("setThirdLineTextTitle", FREQUENCY_TITLE);
        lo2Bbpr3Details.put("setThirdLineTextValue", ZERO_GHZ_FREQ_VALUE + QUESTIONABLE_TEXT);
        buildDevRectangle(lo2Bbpr3Location, lo2Bbpr3Details);

        /* third column */
        //Add the Front End
        Rectangle feLocation = new Rectangle(devs.get("lo2_3").x + devs.get("lo2_3").width + PAD_X,
                                               START_Y, WIDTH * 2 + 3 * PAD_Y, HEIGHT * 2 + 2 * PAD_X);
        HashMap<String, Object> feDetails = new HashMap<String, Object>();
        feDetails.put("boxTitle", FRONT_END_TITLE);
        feDetails.put("deviceName", "fe");
        feDetails.put("detailsPlugin", null);
        feDetails.put("deviceStatus", DeviceStatus.COMPOSITE_DEVICE);
        buildDevRectangle(feLocation, feDetails);
        totalWidth = totalWidth + devs.get("fe").width + PAD_X;

        
        //Add current band
        Rectangle currentBandLocation = new Rectangle(devs.get("fe").x + PAD_X/3,
                                                  devs.get("fe").y + 2 * HEIGHT + PAD_Y,
                                                  WIDTH, HEIGHT);
        HashMap<String, Object> currentBandDetails = new HashMap<String, Object>();
        currentBandDetails.put("boxTitle", CARTRIDGE_TITLE);
        currentBandDetails.put("deviceName", "currentBand");
        currentBandDetails.put("detailsPlugin", null);
        currentBandDetails.put("setFirstLineTextValue", NONE);
        currentBandDetails.put("setFirstLineTextCentered", false);
        currentBandDetails.put("setTextOutsideBoundsOfRectangleTop", CURRENT_BAND_TITLE);
        buildDevRectangle(currentBandLocation, currentBandDetails);
        devs.get("fe").addNestedRectangle(devs.get("currentBand"));
        
        //Add Standby Band 1
        Rectangle standby1Location = new Rectangle(devs.get("fe").x + PAD_X + devs.get("currentBand").width,
                                                  devs.get("fe").y + 2 * PAD_Y, WIDTH, HEIGHT);
        HashMap<String, Object> standby1Details = new HashMap<String, Object>();
        standby1Details.put("boxTitle", CARTRIDGE_TITLE);
        standby1Details.put("deviceName", "standbyBand1");
        standby1Details.put("detailsPlugin", null);
        standby1Details.put("setFirstLineTextValue", NONE);
        standby1Details.put("setFirstLineTextCentered", false);
        standby1Details.put("setTextOutsideBoundsOfRectangleTop", STANDBY_BAND_TITLE);
        buildDevRectangle(standby1Location, standby1Details);
        devs.get("fe").addNestedRectangle(devs.get("standbyBand1"));
        
        //Add Standby Band 2
        Rectangle standby2Location = new Rectangle(devs.get("fe").x + PAD_X + devs.get("currentBand").width,
                                                  devs.get("fe").y + 2 * HEIGHT + PAD_Y, WIDTH, HEIGHT);
        HashMap<String, Object> standby2Details = new HashMap<String, Object>();
        standby2Details.put("boxTitle", CARTRIDGE_TITLE);
        standby2Details.put("deviceName", "standbyBand2");
        standby2Details.put("detailsPlugin", null);
        standby2Details.put("setFirstLineTextCentered", false);
        buildDevRectangle(standby2Location, standby2Details);
        devs.get("fe").addNestedRectangle(devs.get("standbyBand2"));

        //Add IFProc0
        Rectangle IFProc0Location = new Rectangle(devs.get("currentBand").x,
                                                  devs.get("fe").y + devs.get("fe").height + 2 * PAD_Y,
                                                  WIDTH, HEIGHT);
        HashMap<String, Object> IFProc0Details = new HashMap<String, Object>();
        IFProc0Details.put("boxTitle", IFPROC_POL_TITLE + " 0");
        IFProc0Details.put("deviceName", IFPROC_DEV_NAME + "0");
        IFProc0Details.put("detailsPlugin", alma.Control.device.gui.IFProc.DeviceChildPlugin.class);
        IFProc0Details.put("deviceInstance", 0);
        IFProc0Details.put("secondLineTitle",  BBPR_0_1_TITLE);
        IFProc0Details.put("setSecondLineTextValue", USB_BASEBAND_VALUE + QUESTIONABLE_TEXT);
        IFProc0Details.put("setThirdLineTextTitle", BBPR_2_3_TITLE);
        IFProc0Details.put("setThirdLineTextValue", LSB_BASEBAND_VALUE + QUESTIONABLE_TEXT);
        buildDevRectangle(IFProc0Location, IFProc0Details);

        //Add IFProc1
        Rectangle IFProc1Location = new Rectangle(devs.get("currentBand").x,
                                                  devs.get("IFProc0").y + devs.get("IFProc0").height + 2 * PAD_Y,
                                                  WIDTH, HEIGHT);
        HashMap<String, Object> IFProc1Details = new HashMap<String, Object>();
        IFProc1Details.put("boxTitle", IFPROC_POL_TITLE + " 1");
        IFProc1Details.put("deviceName", IFPROC_DEV_NAME + "1");
        IFProc1Details.put("detailsPlugin", alma.Control.device.gui.IFProc.DeviceChildPlugin.class);
        IFProc1Details.put("deviceInstance", 1);
        IFProc1Details.put("secondLineTitle",  BBPR_0_1_TITLE);
        IFProc1Details.put("setSecondLineTextValue", USB_BASEBAND_VALUE + QUESTIONABLE_TEXT);
        IFProc1Details.put("setThirdLineTextTitle", BBPR_2_3_TITLE);
        IFProc1Details.put("setThirdLineTextValue", LSB_BASEBAND_VALUE + QUESTIONABLE_TEXT);
        buildDevRectangle(IFProc1Location, IFProc1Details);

        //Add the DGCK
        Rectangle dgckLocation = new Rectangle(devs.get("IFProc1").x,
                                               devs.get("IFProc1").y+HEIGHT+HEIGHT/2+PAD_Y, WIDTH, HEIGHT/2);
        HashMap<String, Object> dgckDetails = new HashMap<String, Object>();
        dgckDetails.put("boxTitle", DGCK_TITLE);
        dgckDetails.put("deviceName", DGCK_TITLE);
        dgckDetails.put("detailsPlugin", alma.Control.device.gui.DGCK.DeviceChildPlugin.class);
        buildDevRectangle(dgckLocation, dgckDetails);

        /*Fourth Column*/
        //Add DTXBBpr0
        Rectangle dtx0Location = new Rectangle(devs.get("IFProc0").x + devs.get("IFProc0").width + PAD_X,
                                               devs.get("IFProc0").y - PAD_Y / 3, WIDTH/3, HEIGHT);
        HashMap<String, Object> dtx0Details = new HashMap<String, Object>();
        dtx0Details.put("boxTitle", DTX_TITLE);
        dtx0Details.put("deviceName", DTX_TITLE+ BB_PR_TITLE + "0");
        dtx0Details.put("detailsPlugin", alma.Control.device.gui.DTX.DeviceChildPlugin.class);
        dtx0Details.put("deviceInstance", 0);
        dtx0Details.put("secondLineTitle", BB_PR_TITLE + "0");
        buildDevRectangle(dtx0Location, dtx0Details);

        //Add DTXBBpr1
        Rectangle dtx1Location = new Rectangle(devs.get("DTXBBpr0").x,
                                               devs.get("DTXBBpr0").y + devs.get("DTXBBpr0").height + SMALL_PAD,
                                               WIDTH/3, HEIGHT);
        HashMap<String, Object> dtx1Details = new HashMap<String, Object>();
        dtx1Details.put("boxTitle", DTX_TITLE);
        dtx1Details.put("deviceName", DTX_TITLE+ BB_PR_TITLE + "1");
        dtx1Details.put("detailsPlugin", alma.Control.device.gui.DTX.DeviceChildPlugin.class);
        dtx1Details.put("deviceInstance", 1);
        dtx1Details.put("secondLineTitle", BB_PR_TITLE + "1");
        buildDevRectangle(dtx1Location, dtx1Details);

        //Add DTXBBpr2
        Rectangle dtx2Location = new Rectangle(devs.get("DTXBBpr0").x,
                                               devs.get("DTXBBpr1").y + devs.get("DTXBBpr1").height + SMALL_PAD,
                                               WIDTH/3, HEIGHT);
        HashMap<String, Object> dtx2Details = new HashMap<String, Object>();
        dtx2Details.put("boxTitle", DTX_TITLE);
        dtx2Details.put("deviceName", DTX_TITLE+ BB_PR_TITLE + "2");
        dtx2Details.put("detailsPlugin", alma.Control.device.gui.DTX.DeviceChildPlugin.class);
        dtx2Details.put("deviceInstance", 2);
        dtx2Details.put("secondLineTitle", BB_PR_TITLE + "2");
        buildDevRectangle(dtx2Location, dtx2Details);
        
        //Add DTXBBpr3
        Rectangle dtx3Location = new Rectangle(devs.get("DTXBBpr0").x,
                                               devs.get("DTXBBpr2").y + devs.get("DTXBBpr2").height + SMALL_PAD,
                                               WIDTH/3, HEIGHT);
        HashMap<String, Object> dtx3Details = new HashMap<String, Object>();
        dtx3Details.put("boxTitle", DTX_TITLE);
        dtx3Details.put("deviceName", DTX_TITLE+ BB_PR_TITLE + "3");
        dtx3Details.put("detailsPlugin", alma.Control.device.gui.DTX.DeviceChildPlugin.class);
        dtx3Details.put("deviceInstance", 3);
        dtx3Details.put("secondLineTitle", BB_PR_TITLE + "3");
        buildDevRectangle(dtx3Location, dtx3Details);

        //Add the FOAD
        Rectangle foadLocation = new Rectangle(devs.get("DTXBBpr0").x + devs.get("DTXBBpr3").width + SMALL_PAD,
                                               devs.get("DTXBBpr0").y, WIDTH/3, HEIGHT*4+SMALL_PAD*3);
        HashMap<String, Object> foadDetails = new HashMap<String, Object>();
        foadDetails.put("boxTitle", FOAD_TITLE);
        foadDetails.put("deviceName", FOAD_TITLE);
        foadDetails.put("detailsPlugin", null);
        buildDevRectangle(foadLocation, foadDetails);
        //Add DRXBBpr0
        Rectangle drx0Location = new Rectangle(devs.get("DTXBBpr0").x + devs.get("DTXBBpr0").width + devs.get("FOAD").width + 2 * SMALL_PAD,
                                               devs.get("DTXBBpr0").y, WIDTH/3, HEIGHT);
        HashMap<String, Object> drx0Details = new HashMap<String, Object>();
        drx0Details.put("boxTitle", DRX_TITLE);
        drx0Details.put("deviceName", DRX_TITLE+ BB_PR_TITLE + "0");
        drx0Details.put("detailsPlugin", alma.Control.device.gui.DRX.DeviceChildPlugin.class);
        drx0Details.put("deviceInstance", 0);
        drx0Details.put("secondLineTitle", BB_PR_TITLE + "0");
        buildDevRectangle(drx0Location, drx0Details);

        //Add DRXBBpr1
        Rectangle drx1Location = new Rectangle(devs.get("DRXBBpr0").x, devs.get("DTXBBpr1").y, WIDTH/3, HEIGHT);
        HashMap<String, Object> drx1Details = new HashMap<String, Object>();
        drx1Details.put("boxTitle", DRX_TITLE);
        drx1Details.put("deviceName", DRX_TITLE+ BB_PR_TITLE + "1");
        drx1Details.put("detailsPlugin", alma.Control.device.gui.DRX.DeviceChildPlugin.class);
        drx1Details.put("deviceInstance", 1);
        drx1Details.put("secondLineTitle", BB_PR_TITLE + "1");
        buildDevRectangle(drx1Location, drx1Details);
        
        //Add DRXBBpr2
        Rectangle drx2Location = new Rectangle(devs.get("DRXBBpr0").x, devs.get("DTXBBpr2").y, WIDTH/3, HEIGHT);
        HashMap<String, Object> drx2Details = new HashMap<String, Object>();
        drx2Details.put("boxTitle", DRX_TITLE);
        drx2Details.put("deviceName", DRX_TITLE+ BB_PR_TITLE + "2");
        drx2Details.put("detailsPlugin", alma.Control.device.gui.DRX.DeviceChildPlugin.class);
        drx2Details.put("deviceInstance", 2);
        drx2Details.put("secondLineTitle", BB_PR_TITLE + "2");
        buildDevRectangle(drx2Location, drx2Details);

        //Add DRXBBpr3
        Rectangle drx3Location = new Rectangle(devs.get("DRXBBpr0").x, devs.get("DTXBBpr3").y, WIDTH/3, HEIGHT);
        HashMap<String, Object> drx3Details = new HashMap<String, Object>();
        drx3Details.put("boxTitle", DRX_TITLE);
        drx3Details.put("deviceName", DRX_TITLE+ BB_PR_TITLE + "3");
        drx3Details.put("detailsPlugin", alma.Control.device.gui.DRX.DeviceChildPlugin.class);
        drx3Details.put("deviceInstance", 3);
        drx3Details.put("secondLineTitle", BB_PR_TITLE + "3");
        buildDevRectangle(drx3Location, drx3Details);
        
        // add connections
        addConnections(PAD_X, SMALL_PAD);
    }

    private void addConnections(final int STD_PADDING_X, final int SMALL_PADDING) 
    {
        final Color BLUE_LINE = Color.BLUE;
        final Color PURPLE = new Color(118, 47, 140);

        //Add the LLC to SAS line
        Point2D[] llcToSasPoints = new Point2D[2];
        llcToSasPoints[0]=devs.get("sas").getTopCenter();
        llcToSasPoints[1]=devs.get("llc").getBottomCenter();
        addALine(llcToSasPoints, Color.BLACK);
        
        //Add the SAS to LORR line
        Point2D[] sasToLorrPoints = new Point2D[2];
        sasToLorrPoints[0]=devs.get("llc").getTopCenter();
        sasToLorrPoints[1]=devs.get(LORR_TITLE).getBottomCenter();
        addALine(sasToLorrPoints, Color.BLACK);
        
        //Add the LORR to FLOOG line
        Point2D[] lorrToFloogPoints = new Point2D[2];
        lorrToFloogPoints[0]=devs.get(LORR_TITLE).getRightCenter();
        lorrToFloogPoints[1]=devs.get("floog").getLeftCenter();
        addASquigglyInYLine(lorrToFloogPoints, Color.BLACK);
        
        //Add the LORR to LO2 0 line
        Point2D[] lorrToLo2_0Points = new Point2D[2];
        lorrToLo2_0Points[0]=devs.get(LORR_TITLE).getRightCenter();
        lorrToLo2_0Points[1]=devs.get("lo2_0").getLeftCenter();
        addASquigglyInYLine(lorrToLo2_0Points, Color.BLACK);
        
        //Add the LORR to LO2 1 line
        Point2D[] lorrToLo2_1Points = new Point2D[2];
        lorrToLo2_1Points[0]=devs.get(LORR_TITLE).getRightCenter();
        lorrToLo2_1Points[1]=devs.get("lo2_1").getLeftCenter();
        addASquigglyInYLine(lorrToLo2_1Points, Color.BLACK);
        
        //Add the LORR to LO2 2 line
        Point2D[] lorrToLo2_2Points = new Point2D[2];
        lorrToLo2_2Points[0]=devs.get(LORR_TITLE).getRightCenter();
        lorrToLo2_2Points[1]=devs.get("lo2_2").getLeftCenter();
        addASquigglyInYLine(lorrToLo2_2Points, Color.BLACK);
        
        //Add the LORR to LO2 3 line
        Point2D[] lorrToLo2_3Points = new Point2D[2];
        lorrToLo2_3Points[0]=devs.get(LORR_TITLE).getRightCenter();
        lorrToLo2_3Points[1]=devs.get("lo2_3").getLeftCenter();
        addASquigglyInYLine(lorrToLo2_3Points, Color.BLACK);
        
        //Add the LORR to DGCK line
        Point2D[] lorrToDgckPoints = new Point2D[3];
        lorrToDgckPoints[0]=new Point2D.Double((devs.get(LORR_TITLE).getMaxX()+devs.get("lo2_3").getMinX())/2,
                                                devs.get("lo2_3").getCenterY());
        lorrToDgckPoints[1]=new Point2D.Double(lorrToDgckPoints[0].getX(), devs.get(DGCK_TITLE).getCenterY());
        lorrToDgckPoints[2]=devs.get(DGCK_TITLE).getLeftCenter();
        addALine(lorrToDgckPoints, Color.BLACK);
        
        //Add the FLOOG to Current Band rectangle
        //The current band side of this line is a bit off center.
        //If the layout is heavily changed this may need fixed, but for now it is fine.
        Point2D[] floogToCbPoints = new Point2D[2];
        floogToCbPoints[0]=devs.get("floog").getRightCenter();
        floogToCbPoints[1]=new Point2D.Double(devs.get("currentBand").x, floogToCbPoints[0].getY());
        addALine(floogToCbPoints, Color.BLACK);
        
        //Add the CurrentBand to IFProc 0 line
        Point2D[] cbToIfp0Points = new Point2D[2];
        cbToIfp0Points[0]=devs.get("currentBand").getBottomCenter();
        cbToIfp0Points[1]=devs.get(IFPROC_DEV_NAME + "0").getTopCenter();
        addALine(cbToIfp0Points, PURPLE);
        
        //Add the CurrentBand to IFProc 1 line
        //This one is long and crooked since it has to go around IFProc 0.
        Point2D[] cbToIfp1Points = new Point2D[6];
        cbToIfp1Points[0]=new Point2D.Double(devs.get("currentBand").getCenterX()-OFFSET_FOR_DOUBLE_LINE, devs.get("currentBand").getMaxY());
        cbToIfp1Points[1]=new Point2D.Double(cbToIfp1Points[0].getX(), devs.get(IFPROC_DEV_NAME + "0").getMinY()-PAD_Y);
        cbToIfp1Points[2]=new Point2D.Double(devs.get(IFPROC_DEV_NAME + "0").getMinX()-PAD_Y+3, cbToIfp1Points[1].getY());
        cbToIfp1Points[3]=new Point2D.Double(cbToIfp1Points[2].getX(), 
                (devs.get(IFPROC_DEV_NAME + "0").getMaxY()+devs.get(IFPROC_DEV_NAME + "1").getMinY())/2);
        cbToIfp1Points[4]=new Point2D.Double(cbToIfp1Points[0].getX(), cbToIfp1Points[3].getY());
        cbToIfp1Points[5]=new Point2D.Double(cbToIfp1Points[0].getX(), devs.get(IFPROC_DEV_NAME + "1").getMinY());
        addALine(cbToIfp1Points, Color.BLUE);
        
        /* All LO2s need connected to all the IFProcs.
         * The way that the lines stack on each other we can create this appearance by connecting the 
         * top 2 LO2s to the bottom IFP, and bottom 2 LO2s to the top IFP. Since the lines are just for
         * looks this works well. But if the GUI is redone then this may need reconsidered.
         */
        //Add the LO2 0 to IFProc1 line
        Point2D[] lo2_0ToIFProc1Points = new Point2D[2];
        lo2_0ToIFProc1Points[0]=devs.get("lo2_0").getRightCenter();
        lo2_0ToIFProc1Points[1]=devs.get(IFPROC_DEV_NAME + "1").getLeftCenter();
        addASquigglyInYLine(lo2_0ToIFProc1Points, Color.BLACK);
        
        //Add the LO2 1 to IFProc1 line
        Point2D[] lo2_1ToIFProc1Points = new Point2D[2];
        lo2_1ToIFProc1Points[0]=devs.get("lo2_1").getRightCenter();
        lo2_1ToIFProc1Points[1]=devs.get(IFPROC_DEV_NAME + "1").getLeftCenter();
        addASquigglyInYLine(lo2_1ToIFProc1Points, Color.BLACK);
        
        //Add the LO2 2 to IFProc1 line
        Point2D[] lo2_2ToIFProc0Points = new Point2D[2];
        lo2_2ToIFProc0Points[0]=devs.get("lo2_2").getRightCenter();
        lo2_2ToIFProc0Points[1]=devs.get(IFPROC_DEV_NAME + "0").getLeftCenter();
        addASquigglyInYLine(lo2_2ToIFProc0Points, Color.BLACK);
        
        //Add the LO2 3 to IFProc1 line
        Point2D[] lo2_3ToIFProc0Points = new Point2D[2];
        lo2_3ToIFProc0Points[0]=devs.get("lo2_3").getRightCenter();
        lo2_3ToIFProc0Points[1]=devs.get(IFPROC_DEV_NAME + "0").getLeftCenter();
        addASquigglyInYLine(lo2_3ToIFProc0Points, Color.BLACK);
        
        /**
         * There are 3 sets of lines drawn here: each IFP to each DTX, and the DGCK to the DTX.
         * The two IFProc lines are offset with the DGCK line in the middle.
         */
        //Add the IFProc 0 to DTXBBpr0 line
        Point2D[] ifp0ToDtx0Points = new Point2D[2];
        ifp0ToDtx0Points[0]=devs.get(IFPROC_DEV_NAME + "0").getRightCenter();
        ifp0ToDtx0Points[1]=devs.get(DTX_TITLE+ BB_PR_TITLE + "0").getLeftCenter();
        addASquigglyInYLine(ifp0ToDtx0Points, -OFFSET_FOR_DOUBLE_LINE, -OFFSET_FOR_DOUBLE_LINE, PURPLE);

        //Add the IFProc 0 to DTXBBpr1 line
        Point2D[] ifp0ToDtx1Points = new Point2D[2];
        ifp0ToDtx1Points[0]=devs.get(IFPROC_DEV_NAME + "0").getRightCenter();
        ifp0ToDtx1Points[1]=devs.get(DTX_TITLE+ BB_PR_TITLE + "1").getLeftCenter();
        addASquigglyInYLine(ifp0ToDtx1Points, -OFFSET_FOR_DOUBLE_LINE, -OFFSET_FOR_DOUBLE_LINE, PURPLE);
        
        //Add the IFProc 0 to DTXBBpr2 line
        Point2D[] ifp0ToDtx2Points = new Point2D[2];
        ifp0ToDtx2Points[0]=devs.get(IFPROC_DEV_NAME + "0").getRightCenter();
        ifp0ToDtx2Points[1]=devs.get(DTX_TITLE+ BB_PR_TITLE + "2").getLeftCenter();
        addASquigglyInYLine(ifp0ToDtx2Points, -OFFSET_FOR_DOUBLE_LINE, -OFFSET_FOR_DOUBLE_LINE, PURPLE);
        
        //Add the IFProc 0 to DTXBBpr3 line
        Point2D[] ifp0ToDtx3Points = new Point2D[2];
        ifp0ToDtx3Points[0]=devs.get(IFPROC_DEV_NAME + "0").getRightCenter();
        ifp0ToDtx3Points[1]=devs.get(DTX_TITLE+ BB_PR_TITLE + "3").getLeftCenter();
        addASquigglyInYLine(ifp0ToDtx3Points, -OFFSET_FOR_DOUBLE_LINE, -OFFSET_FOR_DOUBLE_LINE, PURPLE);
        
        //Add the IFProc 1 to DTXBBpr0 line
        Point2D[] ifp1ToDtx0Points = new Point2D[2];
        ifp1ToDtx0Points[0]=devs.get(IFPROC_DEV_NAME + "1").getRightCenter();
        ifp1ToDtx0Points[1]=devs.get(DTX_TITLE+ BB_PR_TITLE + "0").getLeftCenter();
        addASquigglyInYLine(ifp1ToDtx0Points, OFFSET_FOR_DOUBLE_LINE, OFFSET_FOR_DOUBLE_LINE, Color.BLUE);

        //Add the IFProc 1 to DTXBBpr1 line
        Point2D[] ifp1ToDtx1Points = new Point2D[2];
        ifp1ToDtx1Points[0]=devs.get(IFPROC_DEV_NAME + "1").getRightCenter();
        ifp1ToDtx1Points[1]=devs.get(DTX_TITLE+ BB_PR_TITLE + "1").getLeftCenter();
        addASquigglyInYLine(ifp1ToDtx1Points, OFFSET_FOR_DOUBLE_LINE, OFFSET_FOR_DOUBLE_LINE, Color.BLUE);
        
        //Add the IFProc 1 to DTXBBpr2 line
        Point2D[] ifp1ToDtx2Points = new Point2D[2];
        ifp1ToDtx2Points[0]=devs.get(IFPROC_DEV_NAME + "1").getRightCenter();
        ifp1ToDtx2Points[1]=devs.get(DTX_TITLE+ BB_PR_TITLE + "2").getLeftCenter();
        addASquigglyInYLine(ifp1ToDtx2Points, OFFSET_FOR_DOUBLE_LINE, OFFSET_FOR_DOUBLE_LINE, Color.BLUE);
        
        //Add the IFProc 1 to DTXBBpr3 line
        Point2D[] ifp1ToDtx3Points = new Point2D[2];
        ifp1ToDtx3Points[0]=devs.get(IFPROC_DEV_NAME + "1").getRightCenter();
        ifp1ToDtx3Points[1]=devs.get(DTX_TITLE+ BB_PR_TITLE + "3").getLeftCenter();
        addASquigglyInYLine(ifp1ToDtx3Points, OFFSET_FOR_DOUBLE_LINE, OFFSET_FOR_DOUBLE_LINE, Color.BLUE);
        
        //Add the DGCK to DTXBBpr0 line
        Point2D[] dgckToDtx0Points = new Point2D[2];
        dgckToDtx0Points[0]=devs.get(DGCK_TITLE).getRightCenter();
        dgckToDtx0Points[1]=devs.get(DTX_TITLE+ BB_PR_TITLE + "0").getLeftCenter();
        addASquigglyInYLine(dgckToDtx0Points, Color.BLACK);
        
        //Add the DGCK to DTXBBpr1 line
        Point2D[] dgckToDtx1Points = new Point2D[2];
        dgckToDtx1Points[0]=devs.get(DGCK_TITLE).getRightCenter();
        dgckToDtx1Points[1]=devs.get(DTX_TITLE+ BB_PR_TITLE + "1").getLeftCenter();
        addASquigglyInYLine(dgckToDtx1Points, Color.BLACK);
        
        //Add the DGCK to DTXBBpr2 line
        Point2D[] dgckToDtx2Points = new Point2D[2];
        dgckToDtx2Points[0]=devs.get(DGCK_TITLE).getRightCenter();
        dgckToDtx2Points[1]=devs.get(DTX_TITLE+ BB_PR_TITLE + "2").getLeftCenter();
        addASquigglyInYLine(dgckToDtx2Points, Color.BLACK);
        
        //Add the DGCK to DTXBBpr3 line
        Point2D[] dgckToDtx3Points = new Point2D[2];
        dgckToDtx3Points[0]=devs.get(DGCK_TITLE).getRightCenter();
        dgckToDtx3Points[1]=devs.get(DTX_TITLE+ BB_PR_TITLE + "3").getLeftCenter();
        addASquigglyInYLine(dgckToDtx3Points, Color.BLACK);
        }

    /**
     * This function adds a line through the given points with the given color. It starts at points[0], goes
     * to points[1], etc, until there are no more points.
     * @param points  An array of POint2D.Double that describes the line to make.
     * @param color   The color to make the line.
     */
    private void addALine(Point2D[] points, Color color) {
        for (int c = 0; c<(points.length-1); c++)
        {
            Line2D.Float aLine = new Line2D.Float(points[c], points[c+1]);
            ColoredLine2D newLine = new ColoredLine2D(color, aLine);
            lines.add(newLine);
        }
    }
    
    /**
     * This function builds a 2 bend squiggly line, starting at points[0] and finishing at points[1]. The line
     * will move left or right from the first point (as needed), then up or down halfway between the 2
     * points, and then left or right to the second point. Surplus points in points[] will be ignored.
     * @param points    The 2 points to connect.
     * @param color     The color to make the line.
     */
    private void addASquigglyInYLine(Point2D[] points, Color color) {
        addASquigglyInYLine(points, 0, 0, color);
    }
    
    /**
     * This function builds a 2 bend squiggly line, starting at points[0] and finishing at points[1]. The line
     * will move left or right from the first point (as needed), then up or down halfway between the 2
     * points, and then left or right to the second point. Surplus points in points[] will be ignored.
     * @param points    The 2 points to connect.
     * @param color     The color to make the line.
     * @param offsetX   The offset from center to add to the horizontal lines.
     * @param offsetY   The offset from center to add to the vertical lines.
     */
    private void addASquigglyInYLine(Point2D[] points, double offsetX, double offsetY, Color color) {
        Point2D[] newPoints = new Point2D[4];
        newPoints[0]=new Point2D.Double(points[0].getX(), points[0].getY()+offsetY);
        //Calculate the first bend by averaging the X values and using the first Y value
        newPoints[1]=new Point2D.Double((points[0].getX()+points[1].getX())/2+offsetX, points[0].getY()+offsetY);
        //Calculate the first bend by averaging the X values and using the second Y value
        newPoints[2]=new Point2D.Double((points[0].getX()+points[1].getX())/2+offsetX, points[1].getY()+offsetY);
        newPoints[3]=new Point2D.Double(points[1].getX(), points[1].getY()+offsetY);
        addALine(newPoints, color);
    }
    
    /**
     * A generic build rectangle function. You give it a rectangle, tell it where to put it,
     * and what device to attach it do. Fill in null for any unused parameters.
     * @param location  A java.awt.Rectangle describing where this rectangle should go (X, Y, height, width)
     * @param details   A HashMap<String, Object> containing the details of this rectangle. Each hash key
     *                  describes what the specific detail associated with it. The string must exactly match
     *                  the detail namne given here
     *
     * The required details are:
     *      detailsPlugin   The DevicePlugin for the Control Device GUI that this rectangle should link to
     *      boxTitle        The name of the device to be given to the rectangle constructor.
     *      deviceName      The name of the component to attach to this rectangle (e.g. DGCK or DRXBBpr0)
     * 
     * The optional details are:
     *      secondLineTitle The title section of the second line of test.
     *      deviceInstance  The instance number of this device (required if there are several of these device per antenna)
     */
    private void buildDevRectangle(Rectangle location, HashMap<String, Object> details){
        SingleInstanceDeviceRectangle aRectangle;
        //Get the base rectangle created using the the required details.
        Class<? extends ChessboardDetailsPlugin> detailsPlugin=(Class<? extends ChessboardDetailsPlugin>) details.get("detailsPlugin");
        String boxTitle = (String) details.get("boxTitle");
        DeviceStatus devStatus;
        if (details.get("deviceStatus") != null)
            devStatus=(DeviceStatus) details.get("deviceStatus");
        else
            devStatus=DeviceStatus.NOT_INSTALLED;
        //If an instance is given we use a multi-dev rectangle - otherwise a single dev rectangle.
        if (details.get("deviceInstance")!=null)
            aRectangle = new MultiInstanceDeviceRectangle(devStatus, 
                    this.pluginContainerServices, detailsPlugin, this.antennaNames, boxTitle,
                    (Integer)details.get("deviceInstance"));
        else
            aRectangle = new SingleInstanceDeviceRectangle(devStatus, 
                    this.pluginContainerServices, detailsPlugin, this.antennaNames, boxTitle);

        //Set some more required details
        aRectangle.height = location.height;
        aRectangle.width = location.width;
        aRectangle.x = location.x;
        aRectangle.y = location.y;
        aRectangle.setFirstLineTextTitle(boxTitle);
        
        //Set the rest of the optional details
        if (details.get("setTextOutsideBoundsOfRectangleTop") != null)
            aRectangle.setTextOutsideBoundsOfRectangleTop((String)details.get("setTextOutsideBoundsOfRectangleTop"));
        if (details.get("setFirstLineTextValue") != null)
            aRectangle.setFirstLineTextValue((String)details.get("setFirstLineTextValue"));
        if (details.get("setFirstLineTextCentered") != null)
            aRectangle.setFirstLineTextCentered((Boolean)details.get("setFirstLineTextCentered"));
        if (details.get("secondLineTitle") != null)
            aRectangle.setSecondLineTextTitle((String)details.get("secondLineTitle"));
        if (details.get("setSecondLineTextValue") != null)
            aRectangle.setSecondLineTextValue((String)details.get("setSecondLineTextValue"));
        if (details.get("setThirdLineTextTitle") != null)
            aRectangle.setThirdLineTextTitle((String)details.get("setThirdLineTextTitle"));
        if (details.get("setThirdLineTextValue") != null)
            aRectangle.setThirdLineTextValue((String)details.get("setThirdLineTextValue"));
        if (details.get("setAllLinesBold") != null)
            aRectangle.setAllLinesBold((Boolean)details.get("setAllLinesBold"));
        if (details.get("setAllLinesSameFontSize") != null)
            aRectangle.setAllLinesBold((Boolean)details.get("setAllLinesSameFontSize"));
        rectangles.add(aRectangle);
        devs.put((String)details.get("deviceName"), aRectangle);
    }

    private void addMouseListeners()
    {
        for(SingleInstanceDeviceRectangle rect : rectangles)
        {
            this.addMouseListener(rect);
            for(SingleInstanceDeviceRectangle innerRect : rect.getNestedRectangles())
                this.addMouseListener(innerRect);
        }
        this.addMouseMotionListener(this);
    }

    private void init()
    {
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        createRectanglesAndLines();
        this.setPreferredSize(getMinSize());
        repaint();
        addMouseListeners();
        this.presentationModel = new AntennaPresentationModel(pluginContainerServices, this.antennaName, this);
        this.presentationModel.start();
    }
    
    private static final String ARRAY_NAME_PROPERTY = "Array name";
    private static final String ARRAY_TITLE = "Array: ";
    private static final String ANTENNA_TITLE = "Antenna: ";
    private static final String BB_PR_TITLE = "BBpr";
    private static final String BBPR_0_1_TITLE = "BBPr 0,1: ";
    private static final String BBPR_2_3_TITLE = "BBPr 2,3: ";
    private static final String CARTRIDGE_TITLE = "Cartridge: ";
    private static final String CELSIUS_UNITS = "C";
    private static final String CURRENT_BAND_TITLE = "Current Band";
    private static final String DEC_TITLE = "DEC: ";
    private static final String DEWAR_TEMP_TITLE = "Dewar Temp: ";
    private static final String DGCK_TITLE = "DGCK";
    private static final String DRX_TITLE = "DRX";
    private static final String DTX_TITLE = "DTX";
    private static final String FLOOG_TITLE = "FLOOG";
    private static final String FOAD_TITLE = "FOAD";
    private static final String FREQUENCY_TITLE = "Frequency: ";
    private static final String FRINGE_TRACKING_TITLE = "Fringe Tracking: ";
    private static final String FRONT_END_TITLE = "Front End";
    private static final String IFPROC_DEV_NAME = "IFProc";
    private static final String IFPROC_POL_TITLE = "IFProc Pol";
    private static final int HEIGHT = 60;
    private static final String LLC_TITLE = "LLC";
    private static final String LO2_BBPR3_TITLE = "LO2: BBPr3";
    private static final String LO2_BBPR2_TITLE = "LO2: BBPr2";
    private static final String LORR_TITLE = "LORR";
    private static final String LSB_BASEBAND_VALUE = "LSB";
    private static final String MOUNT_TITLE = "Mount";
    private static final String NONE = "none";
    private static final String ON_VALUE = "ON";
    private static final Double OFFSET_FOR_DOUBLE_LINE = 4.0;
    private static final String OUTPUT_TITLE = "Output: ";
    private static final int PAD_X = 50;
    private static final int PAD_Y = 20;
    private static final int PADDING = 20;
    private static final String PHOTONIC_REF_1_VALUE = "1";
    private static final String PHOTONIC_REF_TITLE = "Photonic Ref: ";
    private static final String PLUGIN_TITLE = "Antenna ";
    private static final String PSA_TITLE = "PSA";
    private static final String QUESTIONABLE_TEXT = " ?";
    private static final String RA_DEC_ZERO_VALUE = "00:00:00";
    private static final String RA_TITLE = "RA:   ";
    private static final String SAS_TITLE = "SAS";
    private static final int SMALL_PAD = 2;
    private static final String STANDBY_BAND_TITLE = "Standby Band";
    private static final int START_X = 10;
    private static final int START_Y = 10;
    private static final String TEMP_TITLE = "Temp: ";
    private static final String TIME_REMAINING_TITLE = "Time remaining: ";
    private static final String UNKNOWN_ARRAY_NAME = "Not assigned";
    private static final String USB_BASEBAND_VALUE = "USB";
    private static final int WIDTH = 180;
    private static final String ZERO_GHZ_FREQ_VALUE = "0 GHz";
    private static final String ZERO_K_TEMP_VALUE = "0 K";
    private static final String ZERO_MHZ_FREQ_VALUE = " 0 MHz";
    private static final String ZERO_MINUTES_VALUE = "0 min";
    private static final String ZERO_TWO_DECIMAL_VALUE = "0.00 ";
    
    private String antennaName;
    private String arrayName;
    private int defaultTooltipDismissDelay;
    private List<ColoredLine2D> lines = new ArrayList<ColoredLine2D>();
    private AntennaPresentationModel presentationModel;
    private List<SingleInstanceDeviceRectangle> rectangles = new ArrayList<SingleInstanceDeviceRectangle>();
    private int totalWidth = 0;
    private int totalHeight = 0;
    
    private HashMap<String, SingleInstanceDeviceRectangle> devs = new HashMap<String, SingleInstanceDeviceRectangle>();
}
