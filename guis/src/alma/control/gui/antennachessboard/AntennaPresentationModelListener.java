package alma.control.gui.antennachessboard;

/**
 * Listener for initialization of the presentation model.
 * @author Steve Harrington
 *
 */
public interface AntennaPresentationModelListener 
{
	/**
	 * Called when the presentation model is fully initialized.
	 */
	public void notifyOfFullyInitialized();
	
	/**
	 * Called to notify the listener that the initial data is ready; can
	 * be used to e.g. hide a wait cursor.
	 */
	public void notifyOfNotBusy();
}
