package alma.control.gui.antennachessboard;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;

import org.omg.CORBA.LongHolder;

import alma.Control.Antenna;
import alma.Control.AntennaHelper;
import alma.Control.DeviceNameMap;
import alma.Control.FLOOG;
import alma.Control.FLOOGHelper;
import alma.Control.FrontEnd;
import alma.Control.FrontEndHelper;
import alma.Control.HardwareController;
import alma.Control.HardwareControllerHelper;
import alma.Control.HardwareDevice;
import alma.Control.HardwareDeviceHelper;
import alma.Control.IFProc;
import alma.Control.IFProcHelper;
import alma.Control.LLC;
import alma.Control.LLCHelper;
import alma.Control.LO2;
import alma.Control.LO2Helper;
import alma.Control.MountController;
import alma.Control.MountControllerHelper;
import alma.Control.PSA;
import alma.Control.PSAHelper;
import alma.Control.PSD;
import alma.Control.PSDHelper;
import alma.Control.SAS;
import alma.Control.SASHelper;
import alma.Control.Common.Name;
import alma.Control.HardwareDevicePackage.HwState;
import alma.Control.device.gui.common.Pollable;
import alma.Control.device.gui.common.PollingManager;
import alma.ReceiverBandMod.ReceiverBand;
import alma.acs.component.ComponentQueryDescriptor;
import alma.common.gui.chessboard.ChessboardEntry;
import alma.common.gui.chessboard.ChessboardStatusAdapter;
import alma.common.gui.chessboard.ChessboardStatusEvent;
import alma.common.gui.chessboard.ChessboardStatusListener;
import alma.control.gui.antennamount.mount.ACSComponentsManager;
import alma.control.gui.antennamount.mount.ValueHolder;
import alma.control.gui.antennamount.utils.DMSAngleConverter;
import alma.control.gui.antennamount.utils.HMSAngleConverter;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

/**
 * Class which will be the 'controller' (e.g. in the MVC pattern) for interfacing the
 * CONTROL system data sources (model) with the antenna status GUI (view). This serves to
 * isolate all interaction with the CONTROL system to a single place, rather than having the GUI 
 * coupled in many places to the CONTROL system.
 * 
 * @author Steve Harrington
 */
public class AntennaPresentationModel
{
    private static final String CELSIUS_SYMBOL = "C";
    private static final String COMPONENT_NOT_ACTIVATED_TEXT = " component not activated";
    /**
     * Constant to use when rendering a degree symbol (e.g. for temperature units) on screen
     */
    public final static char DEGREES_CHAR = (char)186;
    private static final String DGCK_REFERENCE_NAME = "DGCK";
    private static final String DRXBBPR_BASE_REFERENCE_NAME = "DRXBBpr";
    private static final String DTXBBPR_BASE_REFERENCE_NAME = "DTXBBpr";
    private static final String ERROR_PREFIX = "Error: ";
    private static final double FLOATING_POINT_CHANGE_THRESHOLD = .000001;
    private static final String FOAD_REFERENCE_NAME = "FOAD";
    private static final String IFPROC_BASE_REFERENCE_NAME = "IFProc";
    private static final String FLOOG_REFERENCE_NAME = "FLOOG";
    private static final String FRONT_END_REFERENCE_NAME = "FrontEnd";
    /**
     * Offset used to convert Kelvin values report by hardware to Celsius values for display.
     */
    private static final float KELVIN_TO_CELSIUS_OFFSET = 273.15f;
    private static final String LLC_REFERENCE_NAME = "LLC";
    private static final String LO2_B_BPR_BASE_REFERENCE_NAME = "LO2BBpr";
    private static final String LORR_REFERENCE_NAME = "LORR";
    private static final String MOUNT_REFERENCE_NAME = "Mount";
    private static final String NO_COMMUNICATION_TEXT = "No communication";
    private static final String NOT_INSTALLED_TEXT = "Not installed";
    private static final String NOT_OPERATIONAL_TEXT = "Not operational";
    private static final String OPERATIONAL_TEXT = "Operational";
    private static final String PSA_REFERENCE_NAME = "PSA";
    private static final String PSD_REFERENCE_NAME = "PSD";
    private static final String QUESTIONABLE_VALUE_STRING = " ?";
    private static final String SAS_REFERENCE_NAME = "SAS";
    
    private OnOffFormatter onOffFormatter = new OnOffFormatter();
    private PluginContainerServices pluginContainerServices;
    private String antennaName;
    private Antenna antennaComponent;
    private StringUpdateSource antennaInfoSource;
    private IPollableDevice antennaInfoDevice;
    private Logger logger;
    private PollingManager pollingMgr;
    
    private StringUpdateSource psaSource;
    private PollableDevice psaStatusUpdater;
    
    private StringUpdateSource psdSource;
    private PollableDevice psdStatusUpdater;
    
    private StringUpdateSource llcSource;
    private PollableDevice llcStatusUpdater;
    
    private StringUpdateSource sasSource;
    private PollableDevice sasStatusUpdater;
    
    private StringUpdateSource floogSource;
    private PollableDevice floogStatusUpdater;
    
    private StringUpdateSource lo2Pair0Source;
    private PollableDevice lo2Pair0StatusUpdater;
    
    private StringUpdateSource lo2Pair1Source;
    private PollableDevice lo2Pair1StatusUpdater;
    
    private StringUpdateSource lo2Pair2Source;
    private PollableDevice lo2Pair2StatusUpdater;    
    
    private StringUpdateSource lo2Pair3Source;
    private PollableDevice lo2Pair3StatusUpdater;
    
    private StringUpdateSource ifprocPol0Source;
    private PollableDevice ifprocPol0StatusUpdater;    
    
    private StringUpdateSource ifprocPol1Source;
    private PollableDevice ifprocPol1StatusUpdater;
    
    private StringUpdateSource mountSource;
    private PollableDevice mountStatusUpdater;
    
    private StringUpdateSource frontEndSource;
    private PollableDevice frontEndStatusUpdater;
    private ChessboardStatusAdapter currentBandStatusUpdater;
    private StringUpdateSource currentBandSource;
    private ChessboardStatusAdapter standbyBand1StatusUpdater;
    private StringUpdateSource standbyBand1Source;
    private ChessboardStatusAdapter standbyBand2StatusUpdater;
    private StringUpdateSource standbyBand2Source;
    
    private HashMap<String, PollableDevice> devices = new HashMap<String, PollableDevice>();
    private PollableDevice foadStatusUpdater;
    private Map<String, String> installedDevicesComponentNameMap = new HashMap<String, String>();
    private AntennaPresentationModelListener presentationModelListener;
    
    /**
     * Constructor.
     * @param services the container services which will be used for 
     * calls to getComponent, etc.
     * @param antennaName the name of the antenna to which this presentation model will be connected.
     * @param presentationModelListener object to notify when presentation model is fully initialized.
     */
    public AntennaPresentationModel(PluginContainerServices services, String antennaName, final AntennaPresentationModelListener presentationModelListener)
    {
        this.pluginContainerServices = services;
        this.antennaName = antennaName;
        this.presentationModelListener = presentationModelListener;
        this.logger=this.pluginContainerServices.getLogger();
    }
    
    /**
     * Should be called by external parties to start the presentation model. 
     */
    public void start()
    {
        Runnable initializer = new Runnable() 
        {
            public void run()
            {
                init(presentationModelListener);
            }
        };
        Thread initializerThread = new Thread(initializer);
        initializerThread.start();    
    }
    
    /**
     * Starts the polling.
     */
    public void startPolling()
    {
        pollingMgr.startPolling();
    }
    
    /**
     * Stops the polling.
     */
    public void stopPolling()
    {
        pollingMgr.stopPolling();
    }
    
    /**
     * Adds a listener for status changes to a generic device.
     * @param listener    the listener to add.
     * @param deviceName  The full name of the device (e.g. "LORR" or "DTXBBpr0")
     */
    public void addStatusListener(ChessboardStatusListener listener, String deviceName)
    {
        if(null != devices.get(deviceName))
        {
            devices.get(deviceName).addChessboardStatusListener(listener);
        }
    }
    
    /**
     * Adds a listener for string changes to the PSA.
     * @param listener the listener to add.
     */
    public void addPSAListener(StringUpdateListener listener)
    {
        if(null != psaSource)
        {
            psaSource.addListener(listener);
        }
    }
    
    /**
     * Removes a listener for string changes to the PSA.
     * @param listener the listener to remove.
     */
    public void removePSAListener(StringUpdateListener listener)
    {
        if(null != psaSource)
        {
            psaSource.removeListener(listener);
        }
    }

    /**
     * Adds a listener for string changes to antenna info (e.g. array that antenna is assigned to).
     * @param listener the listener to add.
     */
    public void addAntennaInfoListener(StringUpdateListener listener)
    {
        if(null != antennaInfoSource)
        {
            antennaInfoSource.addListener(listener);
        }
    }
    
    /**
     * Removes a listener for string changes to antenna info (e.g. array that antenna is assigned to).
     * @param listener the listener to remove.
     */
    public void removeAntennaInfoListener(StringUpdateListener listener)
    {
        if(null != antennaInfoSource)
        {
            antennaInfoSource.removeListener(listener);
        }
    }
    
    /**
     * Adds a listener for string changes to the PSD.
     * @param listener the listener to add.
     */
    public void addPSDListener(StringUpdateListener listener)
    {
        if(null != psdSource)
        {
            psdSource.addListener(listener);
        }
    }
    
    /**
     * Removes a listener for string changes to the PSD.
     * @param listener the listener to remove.
     */
    public void removePSDListener(StringUpdateListener listener)
    {
        if(null != psdSource)
        {
            psdSource.removeListener(listener);
        }
    }
    
    /**
     * Adds a listener for string changes to the LLC.
     * @param listener the listener to add.
     */
    public void addLlcListener(StringUpdateListener listener)
    {
        if(null != llcSource)
        {
            llcSource.addListener(listener);
        }
    }
    
    /**
     * Removes a listener for string changes to the LLC.
     * @param listener the listener to remove.
     */
    public void removeLlcListener(StringUpdateListener listener)
    {
        if(null != llcSource)
        {
            llcSource.removeListener(listener);
        }
    }
    
    /**
     * Adds a listener for string changes to the SAS.
     * @param listener the listener to add.
     */
    public void addSasListener(StringUpdateListener listener)
    {
        if(null != sasSource)
        {
            sasSource.addListener(listener);
        }
    }
    
    /**
     * Removes a listener for string changes to the SAS.
     * @param listener the listener to remove.
     */
    public void removeSasListener(StringUpdateListener listener)
    {
        if(null != sasSource)
        {
            sasSource.removeListener(listener);
        }
    }
    
    /**
     * Adds a listener for string changes to the FLOOG.
     * @param listener the listener to add.
     */
    public void addFloogListener(StringUpdateListener listener)
    {
        if(null != floogSource)
        {
            floogSource.addListener(listener);
        }
    }
    
    /**
     * Removes a listener for string changes to the FLOOG.
     * @param listener the listener to remove.
     */
    public void removeFloogListener(StringUpdateListener listener)
    {
        if(null != floogSource)
        {
            floogSource.removeListener(listener);
        }
    }
    
    /**
     * Adds a listener for string changes to the LO2 (baseband pair 0).
     * @param listener the listener to add.
     */
    public void addLo2Pair0Listener(StringUpdateListener listener)
    {
        if(null != lo2Pair0Source)
        {
            lo2Pair0Source.addListener(listener);
        }
    }
    
    /**
     * Removes a listener for string changes to the LO2 (baseband pair 0).
     * @param listener the listener to remove.
     */
    public void removeLo2Pair0Listener(StringUpdateListener listener)
    {
        if(null != lo2Pair0Source)
        {
            lo2Pair0Source.removeListener(listener);
        }
    }
    
    /**
     * Adds a listener for string changes to the LO2 (baseband pair 1).
     * @param listener the listener to add.
     */
    public void addLo2Pair1Listener(StringUpdateListener listener)
    {
        if(null != lo2Pair1Source)
        {
            lo2Pair1Source.addListener(listener);
        }
    }
    
    /**
     * Removes a listener for string changes to the LO2 (baseband pair 1).
     * @param listener the listener to remove.
     */
    public void removeLo2Pair1Listener(StringUpdateListener listener)
    {
        if(null != lo2Pair1Source)
        {
            lo2Pair1Source.removeListener(listener);
        }
    }
    
    /**
     * Adds a listener for string changes to the LO2 (baseband pair 2).
     * @param listener the listener to add.
     */
    public void addLo2Pair2Listener(StringUpdateListener listener)
    {
        if(null != lo2Pair2Source)
        {
            lo2Pair2Source.addListener(listener);
        }
    }
    
    /**
     * Removes a listener for string changes to the LO2 (baseband pair 2).
     * @param listener the listener to remove.
     */
    public void removeLo2Pair2Listener(StringUpdateListener listener)
    {
        if(null != lo2Pair2Source)
        {
            lo2Pair2Source.removeListener(listener);
        }
    }
    
    /**
     * Adds a listener for string changes to the LO2 (baseband pair 3).
     * @param listener the listener to add.
     */
    public void addLo2Pair3Listener(StringUpdateListener listener)
    {
        if(null != lo2Pair3Source)
        {
            lo2Pair3Source.addListener(listener);
        }
    }
    
    /**
     * Removes a listener for string changes to the LO2 (baseband pair 3).
     * @param listener the listener to remove.
     */
    public void removeLo2Pair3Listener(StringUpdateListener listener)
    {
        if(null != lo2Pair3Source)
        {
            lo2Pair3Source.removeListener(listener);
        }
    }
    
    /**
     * Adds a listener for string changes to the IFProc Pol 0.
     * @param listener the listener to add.
     */
    public void addIfprocPol0Listener(StringUpdateListener listener)
    {
        if(null != ifprocPol0Source)
        {
            ifprocPol0Source.addListener(listener);
        }
    }
    
    /**
     * Removes a listener for string changes to the IFProc Pol 0.
     * @param listener the listener to remove.
     */
    public void removeIfprocPol0Listener(StringUpdateListener listener)
    {
        if(null != ifprocPol0Source)
        {
            ifprocPol0Source.removeListener(listener);
        }
    }
    
    /**
     * Adds a listener for string changes to the IFProc Pol 1.
     * @param listener the listener to add.
     */
    public void addIfprocPol1Listener(StringUpdateListener listener)
    {
        if(null != ifprocPol1Source)
        {
            ifprocPol1Source.addListener(listener);
        }
    }
    
    /**
     * Removes a listener for string changes to the IFProc Pol 1.
     * @param listener the listener to remove.
     */
    public void removeIfprocPol1Listener(StringUpdateListener listener)
    {
        if(null != ifprocPol1Source)
        {
            ifprocPol1Source.removeListener(listener);
        }
    }
    
    /**
     * Adds a listener for string changes to the mount.
     * @param listener the listener to add.
     */
    public void addMountListener(StringUpdateListener listener)
    {
        if(null != mountSource)
        {
            mountSource.addListener(listener);
        }
    }
    
    /**
     * Removes a listener for string changes to the mount.
     * @param listener the listener to remove.
     */
    public void removeMountListener(StringUpdateListener listener)
    {
        if(null != mountSource)
        {
            mountSource.removeListener(listener);
        }
    }
    
    /**
     * Adds a listener for string changes to the mount.
     * @param listener the listener to add.
     */
    public void addFrontEndListener(StringUpdateListener listener)
    {
        if(null != frontEndSource)
        {
            frontEndSource.addListener(listener);
        }
    }
    
    /**
     * Removes a listener for string changes to the mount.
     * @param listener the listener to remove.
     */
    public void removeFrontEndListener(StringUpdateListener listener)
    {
        if(null != frontEndSource)
        {
            frontEndSource.removeListener(listener);
        }
    }
    
    /**
     * Adds a listener for string changes to the mount.
     * @param listener the listener to add.
     */
    public void addCurrentBandListener(StringUpdateListener listener)
    {
        if(null != currentBandSource)
        {
            currentBandSource.addListener(listener);
        }
    }
    
    /**
     * Removes a listener for string changes to the mount.
     * @param listener the listener to remove.
     */
    public void removeCurrentBandListener(StringUpdateListener listener)
    {
        if(null != currentBandSource)
        {
            currentBandSource.removeListener(listener);
        }
    }
    
    /**
     * Adds a listener for string changes to the mount.
     * @param listener the listener to add.
     */
    public void addStandbyBand1Listener(StringUpdateListener listener)
    {
        if(null != standbyBand1Source)
        {
            standbyBand1Source.addListener(listener);
        }
    }
    
    /**
     * Removes a listener for string changes to the mount.
     * @param listener the listener to remove.
     */
    public void removeStandbyBand1Listener(StringUpdateListener listener)
    {
        if(null != standbyBand1Source)
        {
            standbyBand1Source.removeListener(listener);
        }
    }
    
    /**
     * Adds a listener for string changes to the mount.
     * @param listener the listener to add.
     */
    public void addStandbyBand2Listener(StringUpdateListener listener)
    {
        if(null != standbyBand2Source)
        {
            standbyBand2Source.addListener(listener);
        }
    }
    
    /**
     * Removes a listener for string changes to the mount.
     * @param listener the listener to remove.
     */
    public void removeStandbyBand2Listener(StringUpdateListener listener)
    {
        if(null != standbyBand2Source)
        {
            standbyBand2Source.removeListener(listener);
        }
    }
    
    /**
     * Adds a listener for status changes to the PSA.
     * @param listener the listener to add.
     */
    public void addPSAStatusListener(ChessboardStatusListener listener)
    {
        if(null != psaStatusUpdater)
        {
            psaStatusUpdater.addChessboardStatusListener(listener);
        }
    }
    
    /**
     * Removes a listener for changes to the PSA.
     * @param listener the listener to remove.
     */
    public void removePSAStatusListener(ChessboardStatusListener listener)
    {
        if(null != psaStatusUpdater)
        {
            psaStatusUpdater.removeChessboardStatusListener(listener);
        }
    }
    
    /**
     * Adds a listener for status changes to the PSD.
     * @param listener the listener to add.
     */
    public void addPSDStatusListener(ChessboardStatusListener listener)
    {
        if(null != psdStatusUpdater)
        {
            psdStatusUpdater.addChessboardStatusListener(listener);
        }
    }
    
    /**
     * Removes a listener for status changes to the PSD.
     * @param listener the listener to remove.
     */
    public void removePSDStatusListener(ChessboardStatusListener listener)
    {
        if(null != psdStatusUpdater)
        {
            psdStatusUpdater.removeChessboardStatusListener(listener);
        }
    }
    
    /**
     * Adds a listener for status changes to the LLC.
     * @param listener the listener to add.
     */
    public void addLlcStatusListener(ChessboardStatusListener listener)
    {
        if(null != llcStatusUpdater)
        {
            llcStatusUpdater.addChessboardStatusListener(listener);
        }
    }
    
    /**
     * Removes a listener for status changes to the LLC.
     * @param listener the listener to remove.
     */
    public void removeLlcStatusListener(ChessboardStatusListener listener)
    {
        if(null != llcStatusUpdater)
        {
            llcStatusUpdater.removeChessboardStatusListener(listener);
        }
    }
    
    /**
     * Adds a listener for status changes to the SAS.
     * @param listener the listener to add.
     */
    public void addSasStatusListener(ChessboardStatusListener listener)
    {
        if(null != sasStatusUpdater)
        {
            sasStatusUpdater.addChessboardStatusListener(listener);
        }
    }
    
    /**
     * Removes a listener for changes to the SAS.
     * @param listener the listener to remove.
     */
    public void removeSasStatusListener(ChessboardStatusListener listener)
    {
        if(null != sasStatusUpdater)
        {
            sasStatusUpdater.removeChessboardStatusListener(listener);
        }
    }
    
    /**
     * Adds a listener for status changes to the FLOOG.
     * @param listener the listener to add.
     */
    public void addFloogStatusListener(ChessboardStatusListener listener)
    {
        if(null != floogStatusUpdater)
        {
            floogStatusUpdater.addChessboardStatusListener(listener);
        }
    }
    
    /**
     * Removes a listener for status changes to the FLOOG.
     * @param listener the listener to remove.
     */
    public void removeFloogStatusListener(ChessboardStatusListener listener)
    {
        if(null != floogStatusUpdater)
        {
            floogStatusUpdater.removeChessboardStatusListener(listener);
        }
    }
    
    /**
     * Adds a listener for status changes to the LO2 (baseband pair 0).
     * @param listener the listener to add.
     */
    public void addLo2Pair0StatusListener(ChessboardStatusListener listener)
    {
        if(null != lo2Pair0StatusUpdater)
        {
            lo2Pair0StatusUpdater.addChessboardStatusListener(listener);
        }
    }
    
    /**
     * Removes a listener for status changes to the LO2 (baseband pair 0).
     * @param listener the listener to remove.
     */
    public void removeLo2Pair0StatusListener(ChessboardStatusListener listener)
    {
        if(null != lo2Pair0StatusUpdater)
        {    
            lo2Pair0StatusUpdater.removeChessboardStatusListener(listener);
        }
    }
    
    /**
     * Adds a listener for status changes to the LO2 (baseband pair 1).
     * @param listener the listener to add.
     */
    public void addLo2Pair1StatusListener(ChessboardStatusListener listener)
    {
        if(null != lo2Pair1StatusUpdater)
        {    
            lo2Pair1StatusUpdater.addChessboardStatusListener(listener);
        }
    }
    
    /**
     * Removes a listener for status changes to the LO2 (baseband pair 1).
     * @param listener the listener to remove.
     */
    public void removeLo2Pair1StatusListener(ChessboardStatusListener listener)
    {
        if(null != lo2Pair1StatusUpdater)
        {    
            lo2Pair1StatusUpdater.removeChessboardStatusListener(listener);
        }
    }
    
    /**
     * Adds a listener for status changes to the LO2 (baseband pair 2).
     * @param listener the listener to add.
     */
    public void addLo2Pair2StatusListener(ChessboardStatusListener listener)
    {
        if(null != lo2Pair2StatusUpdater)
        {    
            lo2Pair2StatusUpdater.addChessboardStatusListener(listener);
        }
    }
    
    /**
     * Removes a listener for status changes to the LO2 (baseband pair 2).
     * @param listener the listener to remove.
     */
    public void removeLo2Pair2StatusListener(ChessboardStatusListener listener)
    {
        if(null != lo2Pair2StatusUpdater)
        {    
            lo2Pair2StatusUpdater.removeChessboardStatusListener(listener);
        }
    }
    
    /**
     * Adds a listener for status changes to the LO2 (baseband pair 3).
     * @param listener the listener to add.
     */
    public void addLo2Pair3StatusListener(ChessboardStatusListener listener)
    {
        if(null != lo2Pair3StatusUpdater)
        {    
            lo2Pair3StatusUpdater.addChessboardStatusListener(listener);
        }
    }
    
    /**
     * Removes a listener for status changes to the LO2 (baseband pair 3).
     * @param listener the listener to remove.
     */
    public void removeLo2Pair3StatusListener(ChessboardStatusListener listener)
    {
        if(null != lo2Pair3StatusUpdater)
        {    
            lo2Pair3StatusUpdater.removeChessboardStatusListener(listener);
        }
    }
    
    /**
     * Adds a listener for status changes to the IFProc Pol 0.
     * @param listener the listener to add.
     */
    public void addIfprocPol0StatusListener(ChessboardStatusListener listener)
    {
        if(null != ifprocPol0StatusUpdater)
        {    
            ifprocPol0StatusUpdater.addChessboardStatusListener(listener);
        }
    }
    
    /**
     * Removes a listener for status changes to the IFProc Pol 0.
     * @param listener the listener to remove.
     */
    public void removeIfprocPol0StatusListener(ChessboardStatusListener listener)
    {
        if(null != ifprocPol0StatusUpdater)
        {    
            ifprocPol0StatusUpdater.removeChessboardStatusListener(listener);
        }
    }
    
    /**
     * Adds a listener for status changes to the IFProc Pol 1.
     * @param listener the listener to add.
     */
    public void addIfprocPol1StatusListener(ChessboardStatusListener listener)
    {
        if(null != ifprocPol1StatusUpdater)
        {    
            ifprocPol1StatusUpdater.addChessboardStatusListener(listener);
        }
    }
    
    /**
     * Removes a listener for status changes to the IFProc Pol 1.
     * @param listener the listener to remove.
     */
    public void removeIfprocPol1StatusListener(ChessboardStatusListener listener)
    {
        if(null != ifprocPol1StatusUpdater)
        {    
            ifprocPol1StatusUpdater.removeChessboardStatusListener(listener);
        }
    }
    
    /**
     * Adds a listener for status changes to the mount.
     * @param listener the listener to add.
     */
    public void addMountStatusListener(ChessboardStatusListener listener)
    {
        if(null != mountStatusUpdater)
        {    
            mountStatusUpdater.addChessboardStatusListener(listener);
        }
    }
    
    /**
     * Removes a listener for status changes to the mount.
     * @param listener the listener to remove.
     */
    public void removeMountStatusListener(ChessboardStatusListener listener)
    {
        if(null != mountStatusUpdater)
        {    
            mountStatusUpdater.removeChessboardStatusListener(listener);
        }
    }
    
    /**
     * Adds a listener for status changes to the current band.
     * @param listener the listener to add.
     */
    public void addCurrentBandStatusListener(ChessboardStatusListener listener)
    {
        if(null != currentBandStatusUpdater)
        {    
            currentBandStatusUpdater.addChessboardStatusListener(listener);
        }
    }
    
    /**
     * Removes a listener for status changes to the current band.
     * @param listener the listener to remove.
     */
    public void removeCurrentBandStatusListener(ChessboardStatusListener listener)
    {
        if(null != currentBandStatusUpdater)
        {    
            currentBandStatusUpdater.removeChessboardStatusListener(listener);
        }
    }
    
    /**
     * Adds a listener for status changes to the first standby band.
     * @param listener the listener to add.
     */
    public void addStandbyBand1StatusListener(ChessboardStatusListener listener)
    {
        if(null != standbyBand1StatusUpdater)
        {    
            standbyBand1StatusUpdater.addChessboardStatusListener(listener);
        }
    }
    
    /**
     * Removes a listener for status changes to the first standby band.
     * @param listener the listener to remove.
     */
    public void removeStandbyBand1StatusListener(ChessboardStatusListener listener)
    {
        if(null != standbyBand1StatusUpdater)
        {    
            standbyBand1StatusUpdater.removeChessboardStatusListener(listener);
        }
    }
    
    /**
     * Adds a listener for status changes to the second standby band.
     * @param listener the listener to add.
     */
    public void addStandbyBand2StatusListener(ChessboardStatusListener listener)
    {
        if(null != standbyBand2StatusUpdater)
        {    
            standbyBand2StatusUpdater.addChessboardStatusListener(listener);
        }
    }
    
    /**
     * Removes a listener for status changes to the second standby band.
     * @param listener the listener to remove.
     */
    public void removeStandbyBand2StatusListener(ChessboardStatusListener listener)
    {
        if(null != standbyBand2StatusUpdater)
        {    
            standbyBand2StatusUpdater.removeChessboardStatusListener(listener);
        }
    }
    
    /**
     * Adds a listener for status changes to the FOAD.
     * @param listener the listener to add.
     */
    public void addFoadStatusListener(ChessboardStatusListener listener)
    {
        if(null != foadStatusUpdater)
        {    
            foadStatusUpdater.addChessboardStatusListener(listener);
        }
    }
    
    /**
     * Removes a listener for status changes to the FOAD.
     * @param listener the listener to remove.
     */
    public void removeFoadStatusListener(ChessboardStatusListener listener)
    {
        if(null != foadStatusUpdater)
        {    
            foadStatusUpdater.removeChessboardStatusListener(listener);
        }
    }
    
    private void determineInstalledDevices()
    {
        antennaComponent = null;
        try {
            org.omg.CORBA.Object antennaObject = this.pluginContainerServices.getComponentNonSticky(Name.ControlPrefix + this.antennaName);
            antennaComponent = AntennaHelper.narrow(antennaObject);
        } catch (Throwable ex) {
            // TODO: log something as a serious/fatal error condition
            antennaComponent = null;
        }
        if(null != antennaComponent)
        {
            DeviceNameMap[] deviceNameMapArray = antennaComponent.getSubdeviceList();
            for(DeviceNameMap deviceNameMap : deviceNameMapArray)
            {
//                logger.fine("AntennaPresentationModel->determineInstalledDevices: Ref name is: "
//                           +deviceNameMap.ReferenceName + " and full name is: "+deviceNameMap.FullName);
                this.installedDevicesComponentNameMap.put(deviceNameMap.ReferenceName, deviceNameMap.FullName);
            }
        }
    }
    
    private void init(final AntennaPresentationModelListener listener)
    {
        determineInstalledDevices();
        
        List<IPollableDevice> deviceList = new ArrayList<IPollableDevice>();
        
        if(null != antennaComponent) {
            antennaInfoSource = new StringUpdateSource();
            this.antennaInfoDevice = new AntennaInfoDevice(antennaInfoSource);
            deviceList.add(antennaInfoDevice);
        }
        
        if(null != this.installedDevicesComponentNameMap.get(FRONT_END_REFERENCE_NAME))
        {
            String componentName = this.installedDevicesComponentNameMap.get(FRONT_END_REFERENCE_NAME);
            frontEndSource = new StringUpdateSource();
            frontEndStatusUpdater = new FrontEndDevice(this.pluginContainerServices, this.antennaName, componentName, frontEndSource);
            deviceList.add(frontEndStatusUpdater);
        }
        
        if(null!= this.installedDevicesComponentNameMap.get(PSA_REFERENCE_NAME))
        {
            String componentName = this.installedDevicesComponentNameMap.get(PSA_REFERENCE_NAME);
            psaSource = new StringUpdateSource();
            psaStatusUpdater = new PsaDevice(this.pluginContainerServices, this.antennaName, componentName, psaSource);
            deviceList.add(psaStatusUpdater);
        }

        if(null!= this.installedDevicesComponentNameMap.get(PSD_REFERENCE_NAME))
        {
            String componentName = this.installedDevicesComponentNameMap.get(PSD_REFERENCE_NAME);
            psdSource = new StringUpdateSource();
            psdStatusUpdater = new PsdDevice(this.pluginContainerServices, this.antennaName, componentName, psdSource);
            deviceList.add(psdStatusUpdater);
        }
        
        if(null!= this.installedDevicesComponentNameMap.get(LLC_REFERENCE_NAME))
        {
            String componentName = this.installedDevicesComponentNameMap.get(LLC_REFERENCE_NAME);
            llcSource = new StringUpdateSource();
            llcStatusUpdater = new LlcDevice(this.pluginContainerServices, this.antennaName, componentName, llcSource);
            deviceList.add(llcStatusUpdater);
        }

        if(null!= this.installedDevicesComponentNameMap.get(SAS_REFERENCE_NAME))
        {
            String componentName = this.installedDevicesComponentNameMap.get(SAS_REFERENCE_NAME);
            sasSource = new StringUpdateSource();
            sasStatusUpdater = new SasDevice(this.pluginContainerServices, this.antennaName, componentName, sasSource);
            deviceList.add(sasStatusUpdater);
        }
        
        if(null!= this.installedDevicesComponentNameMap.get(FLOOG_REFERENCE_NAME))
        {
            String componentName = this.installedDevicesComponentNameMap.get(FLOOG_REFERENCE_NAME);
            floogSource = new StringUpdateSource();
            floogStatusUpdater = new FloogDevice(this.pluginContainerServices, this.antennaName, componentName, floogSource);
            deviceList.add(floogStatusUpdater);
        }
        
        if(null!= this.installedDevicesComponentNameMap.get(LO2_B_BPR_BASE_REFERENCE_NAME + "0"))
        {
            String componentName = this.installedDevicesComponentNameMap.get(LO2_B_BPR_BASE_REFERENCE_NAME + "0");
            lo2Pair0Source = new StringUpdateSource();
            lo2Pair0StatusUpdater = new Lo2Device(this.pluginContainerServices, this.antennaName, componentName, lo2Pair0Source);
            deviceList.add(lo2Pair0StatusUpdater);
        }
        
        if(null!= this.installedDevicesComponentNameMap.get(LO2_B_BPR_BASE_REFERENCE_NAME + "1"))
        {
            String componentName = this.installedDevicesComponentNameMap.get(LO2_B_BPR_BASE_REFERENCE_NAME + "1");
            lo2Pair1Source = new StringUpdateSource();
            lo2Pair1StatusUpdater = new Lo2Device(this.pluginContainerServices, this.antennaName, componentName, lo2Pair1Source);
            deviceList.add(lo2Pair1StatusUpdater);
        }
        
        if(null!= this.installedDevicesComponentNameMap.get(LO2_B_BPR_BASE_REFERENCE_NAME + "2"))
        {
            String componentName = this.installedDevicesComponentNameMap.get(LO2_B_BPR_BASE_REFERENCE_NAME + "2");
            lo2Pair2Source = new StringUpdateSource();
            lo2Pair2StatusUpdater = new Lo2Device(this.pluginContainerServices, this.antennaName, componentName, lo2Pair2Source);
            deviceList.add(lo2Pair2StatusUpdater);
        }

        if(null!= this.installedDevicesComponentNameMap.get(LO2_B_BPR_BASE_REFERENCE_NAME + "3"))
        {
            String componentName = this.installedDevicesComponentNameMap.get(LO2_B_BPR_BASE_REFERENCE_NAME + "3");
            lo2Pair3Source = new StringUpdateSource();
            lo2Pair3StatusUpdater = new Lo2Device(this.pluginContainerServices, this.antennaName, componentName, lo2Pair3Source);
            deviceList.add(lo2Pair3StatusUpdater);
        }

        if(null!= this.installedDevicesComponentNameMap.get(IFPROC_BASE_REFERENCE_NAME + "0"))
        {
            String componentName = this.installedDevicesComponentNameMap.get(IFPROC_BASE_REFERENCE_NAME + "0");
            ifprocPol0Source = new StringUpdateSource();
            ifprocPol0StatusUpdater = new IfProcDevice(this.pluginContainerServices, this.antennaName, componentName, ifprocPol0Source);
            deviceList.add(ifprocPol0StatusUpdater);
        }

        if(null!= this.installedDevicesComponentNameMap.get(IFPROC_BASE_REFERENCE_NAME + "1"))
        {
            String componentName = this.installedDevicesComponentNameMap.get(IFPROC_BASE_REFERENCE_NAME + "1");
            ifprocPol1Source = new StringUpdateSource();
            ifprocPol1StatusUpdater = new IfProcDevice(this.pluginContainerServices, this.antennaName, componentName, ifprocPol1Source);
            deviceList.add(ifprocPol1StatusUpdater);
        }

        if(null!= this.installedDevicesComponentNameMap.get(MOUNT_REFERENCE_NAME))
        {
            String componentName = this.installedDevicesComponentNameMap.get(MOUNT_REFERENCE_NAME);
            mountSource = new StringUpdateSource();
            mountStatusUpdater = new MountDevice(this.pluginContainerServices, this.antennaName, componentName, mountSource);
            deviceList.add(mountStatusUpdater);
        }

        if(null!= this.installedDevicesComponentNameMap.get(LORR_REFERENCE_NAME))
        {
            String componentName = this.installedDevicesComponentNameMap.get(LORR_REFERENCE_NAME);
            PollableDevice lorrStatusUpdater = new NoExtraDataDevice(this.pluginContainerServices, this.antennaName, componentName);
            deviceList.add(lorrStatusUpdater);
            devices.put(LORR_REFERENCE_NAME, lorrStatusUpdater);
        }
        
        if(null!= this.installedDevicesComponentNameMap.get(DGCK_REFERENCE_NAME))
        {
            String componentName = this.installedDevicesComponentNameMap.get(DGCK_REFERENCE_NAME);
            PollableDevice dgckStatusUpdater = new NoExtraDataDevice(this.pluginContainerServices, this.antennaName, componentName);
            deviceList.add(dgckStatusUpdater);
            devices.put(DGCK_REFERENCE_NAME, dgckStatusUpdater);
        }
        
        if(null!= this.installedDevicesComponentNameMap.get(DTXBBPR_BASE_REFERENCE_NAME + "0"))
        {
            String componentName = this.installedDevicesComponentNameMap.get(DTXBBPR_BASE_REFERENCE_NAME + "0");
            PollableDevice dtxPair0StatusUpdater = new NoExtraDataDevice(this.pluginContainerServices, this.antennaName, componentName);
            deviceList.add(dtxPair0StatusUpdater);
            devices.put(DTXBBPR_BASE_REFERENCE_NAME + "0", dtxPair0StatusUpdater);
        }

        if(null!= this.installedDevicesComponentNameMap.get(DTXBBPR_BASE_REFERENCE_NAME + "1"))
        {
            String componentName = this.installedDevicesComponentNameMap.get(DTXBBPR_BASE_REFERENCE_NAME + "1");
            PollableDevice dtxPair1StatusUpdater = new NoExtraDataDevice(this.pluginContainerServices, this.antennaName, componentName);
            deviceList.add(dtxPair1StatusUpdater);
            devices.put(DTXBBPR_BASE_REFERENCE_NAME + "1", dtxPair1StatusUpdater);
        }

        if(null!= this.installedDevicesComponentNameMap.get(DTXBBPR_BASE_REFERENCE_NAME + "2"))
        {
            String componentName = this.installedDevicesComponentNameMap.get(DTXBBPR_BASE_REFERENCE_NAME + "2");
            PollableDevice dtxPair2StatusUpdater = new NoExtraDataDevice(this.pluginContainerServices, this.antennaName, componentName);
            deviceList.add(dtxPair2StatusUpdater);
            devices.put(DTXBBPR_BASE_REFERENCE_NAME + "2", dtxPair2StatusUpdater);
        }

        if(null!= this.installedDevicesComponentNameMap.get(DTXBBPR_BASE_REFERENCE_NAME + "3"))
        {
            String componentName = this.installedDevicesComponentNameMap.get(DTXBBPR_BASE_REFERENCE_NAME + "3");
            PollableDevice dtxPair3StatusUpdater = new NoExtraDataDevice(this.pluginContainerServices, this.antennaName, componentName);
            deviceList.add(dtxPair3StatusUpdater);
            devices.put(DTXBBPR_BASE_REFERENCE_NAME + "3", dtxPair3StatusUpdater);
        }

        if(null!= this.installedDevicesComponentNameMap.get(DRXBBPR_BASE_REFERENCE_NAME + "0"))
        {
            String componentName = this.installedDevicesComponentNameMap.get(DRXBBPR_BASE_REFERENCE_NAME + "0");
            PollableDevice drxPair0StatusUpdater = new NoExtraDataDevice(this.pluginContainerServices, this.antennaName, componentName);
            deviceList.add(drxPair0StatusUpdater);
            devices.put(DRXBBPR_BASE_REFERENCE_NAME + "0", drxPair0StatusUpdater);
        }

        if(null!= this.installedDevicesComponentNameMap.get(DRXBBPR_BASE_REFERENCE_NAME + "1"))
        {
            String componentName = this.installedDevicesComponentNameMap.get(DRXBBPR_BASE_REFERENCE_NAME + "1");
            PollableDevice drxPair1StatusUpdater = new NoExtraDataDevice(this.pluginContainerServices, this.antennaName, componentName);
            deviceList.add(drxPair1StatusUpdater);
            devices.put(DRXBBPR_BASE_REFERENCE_NAME + "1", drxPair1StatusUpdater);
        }

        if(null!= this.installedDevicesComponentNameMap.get(DRXBBPR_BASE_REFERENCE_NAME + "2"))
        {
            String componentName = this.installedDevicesComponentNameMap.get(DRXBBPR_BASE_REFERENCE_NAME + "2");
            PollableDevice drxPair2StatusUpdater = new NoExtraDataDevice(this.pluginContainerServices, this.antennaName, componentName);
            deviceList.add(drxPair2StatusUpdater);
            devices.put(DRXBBPR_BASE_REFERENCE_NAME + "2", drxPair2StatusUpdater);
        }

        if(null!= this.installedDevicesComponentNameMap.get(DRXBBPR_BASE_REFERENCE_NAME + "3"))
        {
            String componentName = this.installedDevicesComponentNameMap.get(DRXBBPR_BASE_REFERENCE_NAME + "3");
            PollableDevice drxPair3StatusUpdater = new NoExtraDataDevice(this.pluginContainerServices, this.antennaName, componentName);
            deviceList.add(drxPair3StatusUpdater);
            devices.put(DRXBBPR_BASE_REFERENCE_NAME + "3", drxPair3StatusUpdater);
        }
        
        if(null!= this.installedDevicesComponentNameMap.get(FOAD_REFERENCE_NAME))
        {
            String componentName = this.installedDevicesComponentNameMap.get(FOAD_REFERENCE_NAME);
            foadStatusUpdater = new NoExtraDataDevice(this.pluginContainerServices, this.antennaName, componentName);
            deviceList.add(foadStatusUpdater);
        }

        PollableDeviceGroup deviceGroup = new PollableDeviceGroup(pluginContainerServices, antennaName, deviceList, listener);
        pollingMgr = new PollingManager(deviceGroup);
        
        // notify GUI/listener on swing thread
        Thread runner = new Thread() {
            public void run() {
                listener.notifyOfFullyInitialized();        
            }
        };
        // Note: notifyOfFullyInitialized() eventually causes a JPanel repaint.
        SwingUtilities.invokeLater(runner);
        try {
            runner.join();
            deviceGroup.pollSlowMonitorPoints();
        }
        catch(InterruptedException ex)
        {
            // noop
        }
    }
    
    private class PollableDeviceGroup implements Pollable
    {
        private List<IPollableDevice> deviceList;
        private AntennaPresentationModelListener listener;
        private boolean initialized = false;
        
        /**
         * Constructor
         * @param services the plugin container services to use for things like getComponent calls, etc.
         * @param antennaName the name of the antenna for which we are polling devices.
         * @param deviceList a List of PollableDevice objects to poll.
         * @param listener an antenna presentation model listener to notify when we've done our first round of polling.
         */
        public PollableDeviceGroup(PluginContainerServices services, String antennaName, List<IPollableDevice> deviceList, AntennaPresentationModelListener listener)
        {
            this.deviceList = deviceList;
            this.listener = listener;
        }
        
        public void pollSlowMonitorPoints() 
        {
            for(IPollableDevice device : deviceList)
            {
                device.pollForSlowChanges();
            }
        }

        public void pollFastMonitorPoints() 
        {
            Runnable pollRunner = new Runnable()
            {
                public void run()
                {
                    for(IPollableDevice device : deviceList)
                    {
                        device.pollForFastChanges();
                    }                    
                }
            };
            Thread pollRunnerThread = new Thread(pollRunner);
            pollRunnerThread.start();
            // TODO: Remove outer if(!initialized).
            if(!initialized)
            {
                synchronized(this)
                {
                    if(!initialized)
                    {
                        initialized = true;
                        listener.notifyOfNotBusy();
                    }
                }
            }
        }
        
        /**
         * Report if the device is ready for polling.
         * 
         * @return true if the device is ready for polling.
         */
        public boolean readyForPolling() {
            // TODO: Review class and see if this needs a more detailed implementation.
            return true;
        }
        

    }
    
    private class PsaDevice extends PollableDevice
    {
        private DecimalFormat decimalFormat = new DecimalFormat("0.00");
        private Boolean currentOutput = false;
        private Float currentTemp = 0f;
        private PSA psaDevice = null;
        
        /**
         * Constructor.
         * @param services the PluginContainerServices object to use for getComponent calls etc.
         * @param antennaName the name of the antenna to which the device 'belongs'.
         * @param deviceName the component name to use for the device.
         */
        public PsaDevice(PluginContainerServices services, String antennaName, String deviceName)
        {
            this(services, antennaName, deviceName, null);
        }
 
        /**
         * Constructor.
         * @param services the PluginContainerServices object to use for getComponent calls etc.
         * @param antennaName the name of the antenna to which the device 'belongs'.
         * @param deviceName the component name to use for the device.
         * @param updater the StringUpdateSource used to communicate string/value changes 
         * (as opposed to status/color changes) to the GUI.
         */
        public PsaDevice(PluginContainerServices services, String antennaName, String deviceName, StringUpdateSource updater)
        {
            super(services, antennaName, deviceName, updater);
            if (deviceName.isEmpty())
               logger.severe("AntennaPresentationModel->PsaDevice->constructor, Error: empty deviceName given.");
        }

        @Override
        protected void specializedInterrogateComponentForSlowChanges() 
        {
            Boolean newOutput = false;
            Float newTemp = 0f;

            try
            {
                if(null == psaDevice)
                {
                    psaDevice = PSAHelper.narrow(deviceComponent);
                }
                // Convert temperature from the K reported by PSA hardware to C for display.
                newTemp = psaDevice.GET_AMBIENT_TEMPERATURE(new LongHolder()) - KELVIN_TO_CELSIUS_OFFSET;
                newOutput = psaDevice.GET_PS_SHUTDOWN(new LongHolder());
                newOutput = !newOutput;
                if(newOutput != currentOutput || (Math.abs(newTemp-currentTemp) >= AntennaPresentationModel.FLOATING_POINT_CHANGE_THRESHOLD))
                {
                    guiUpdateThread.setLineOneAndLineTwo(onOffFormatter.format(newOutput), decimalFormat.format(newTemp) + " " + DEGREES_CHAR + CELSIUS_SYMBOL);
                    SwingUtilities.invokeLater(guiUpdateThread);
                    currentOutput = newOutput;
                    currentTemp = newTemp;
                }
            }
            catch(Throwable ex)
            {
                psaDevice = null;
                guiUpdateThread.setLineOneAndLineTwo(onOffFormatter.format(currentOutput) + QUESTIONABLE_VALUE_STRING, 
                        decimalFormat.format(currentTemp) + " " + DEGREES_CHAR + CELSIUS_SYMBOL + QUESTIONABLE_VALUE_STRING);
                SwingUtilities.invokeLater(guiUpdateThread);
            }    
        }
    }
       
    private class PsdDevice extends PollableDevice
    {
        private DecimalFormat decimalFormat = new DecimalFormat("0.00");
        private Boolean currentOutput = false;
        private Float currentTemp = 0f;
        private PSD psdComponent = null;
        
        /**
         * Constructor.
         * @param services the PluginContainerServices object to use for getComponent calls etc.
         * @param antennaName the name of the antenna to which the device 'belongs'.
         * @param deviceName the component name to use for the device.
         */
        public PsdDevice(PluginContainerServices services, String antennaName, String deviceName)
        {
            this(services, antennaName, deviceName, null);
        }
 
        /**
         * Constructor.
         * @param services the PluginContainerServices object to use for getComponent calls etc.
         * @param antennaName the name of the antenna to which the device 'belongs'.
         * @param deviceName the component name to use for the device.
         * @param updater the StringUpdateSource used to communicate string/value changes 
         * (as opposed to status/color changes) to the GUI.
         */
        public PsdDevice(PluginContainerServices services, String antennaName, String deviceName, StringUpdateSource updater)
        {
            super(services, antennaName, deviceName, updater);
            if (deviceName.isEmpty())
                logger.severe("AntennaPresentationModel->PsdDevice->constructor, Error: empty deviceName given.");
        }

        @Override
        protected void specializedInterrogateComponentForSlowChanges() 
        {
            Boolean newOutput = false;
            Float newTemp = 0f;
            try
            {
                if(null == psdComponent)
                {
                    psdComponent = PSDHelper.narrow(deviceComponent);
                }
                // Convert temperatures from the K reported by PSD hardware to C for display.
                newTemp = psdComponent.GET_AMBIENT_TEMPERATURE(new LongHolder()) - KELVIN_TO_CELSIUS_OFFSET;
                newOutput = psdComponent.GET_PS_SHUTDOWN(new LongHolder());
                newOutput = !newOutput;
                if(newOutput != currentOutput || (Math.abs(newTemp-currentTemp) >= AntennaPresentationModel.FLOATING_POINT_CHANGE_THRESHOLD))
                {
                    guiUpdateThread.setLineOneAndLineTwo(onOffFormatter.format(newOutput), decimalFormat.format(newTemp) + " " + DEGREES_CHAR + "C");
                    SwingUtilities.invokeLater(guiUpdateThread);
                    currentOutput = newOutput;
                    currentTemp = newTemp;
                }
            }
            catch(Throwable ex)
            {
                psdComponent = null;
                guiUpdateThread.setLineOneAndLineTwo(onOffFormatter.format(currentOutput) + QUESTIONABLE_VALUE_STRING, 
                        decimalFormat.format(currentTemp) + " " + DEGREES_CHAR + CELSIUS_SYMBOL + QUESTIONABLE_VALUE_STRING);
                SwingUtilities.invokeLater(guiUpdateThread);
            }
        }
    }
    
    private class LlcDevice extends PollableDevice
    {
        private static final String MIN_UNITS = " min";
        private DecimalFormat decimalFormat = new DecimalFormat("0.0");
        private Float currentTime = 0f;
        private LLC llcComponent = null;
        
        /**
         * Constructor.
         * @param services the PluginContainerServices object to use for getComponent calls etc.
         * @param antennaName the name of the antenna to which the device 'belongs'.
         * @param deviceName the component name to use for the device.
         */
        public LlcDevice(PluginContainerServices services, String antennaName, String deviceName)
        {
            this(services, antennaName, deviceName, null);
        }
 
        /**
         * Constructor.
         * @param services the PluginContainerServices object to use for getComponent calls etc.
         * @param antennaName the name of the antenna to which the device 'belongs'.
         * @param deviceName the component name to use for the device.
         * @param updater the StringUpdateSource used to communicate string/value changes 
         * (as opposed to status/color changes) to the GUI.
         */
        public LlcDevice(PluginContainerServices services, String antennaName, String deviceName, StringUpdateSource updater)
        {
            super(services, antennaName, deviceName, updater);
            if (deviceName.isEmpty())
                logger.severe("AntennaPresentationModel->LlcDevice->constructor, Error: empty deviceName given.");
        }

        @Override
        protected void specializedInterrogateComponentForSlowChanges() 
        {
            Float newTime = 0f;
            try
            {
                if(null == llcComponent)
                {
                    llcComponent = LLCHelper.narrow(deviceComponent);
                }
                // ntroncos 2010-11-04:getTimeToLimit is under heavy refactoring.. maybe its not even caculable.
                // Comenting it out for now since i removed it from the interface.
                //newTime = llcComponent.getTimeToLimit();
                if(Math.abs(newTime-currentTime) >= AntennaPresentationModel.FLOATING_POINT_CHANGE_THRESHOLD)
                {
                    guiUpdateThread.setLineOne(decimalFormat.format(newTime) + MIN_UNITS);
                    SwingUtilities.invokeLater(guiUpdateThread);
                    currentTime = newTime;
                }
            }
            catch(Throwable ex)
            {
                llcComponent = null;
                guiUpdateThread.setLineOne(decimalFormat.format(currentTime) + MIN_UNITS + QUESTIONABLE_VALUE_STRING);
                SwingUtilities.invokeLater(guiUpdateThread);
            }
        }
    }
    
    private class FrontEndDevice extends PollableDevice
    {
        private static final String NONE_STRING = "none";
        private DecimalFormat decimalFormat = new DecimalFormat("0.0");
        private FrontEnd frontEndComponent;
        private ReceiverBand currentBandCurrentValue = ReceiverBand.from_int(ReceiverBand._UNSPECIFIED);
        private ReceiverBand standbyBand1CurrentValue = ReceiverBand.from_int(ReceiverBand._UNSPECIFIED);
        private ReceiverBand standbyBand2CurrentValue = ReceiverBand.from_int(ReceiverBand._UNSPECIFIED);
        
        // special case; FrontEnd is not a HardwareDevice (i.e. doesn't implement the proper IDL interface)
        // but is, instead, a HardwareController; so we will have a different type here.
        private HardwareController hwControllerComponent = null;
        
        /**
         * Constructor.
         * @param services the PluginContainerServices object to use for getComponent calls etc.
         * @param antennaName the name of the antenna to which the device 'belongs'.
         * @param deviceName the component name to use for the device.
         */
        public FrontEndDevice(PluginContainerServices services, String antennaName, String deviceName)
        {
            this(services, antennaName, deviceName, null);
        }
 
        /**
         * Constructor.
         * @param services the PluginContainerServices object to use for getComponent calls etc.
         * @param antennaName the name of the antenna to which the device 'belongs'.
         * @param deviceName the component name to use for the device.
         * @param updater the StringUpdateSource used to communicate string/value changes 
         * (as opposed to status/color changes) to the GUI.
         */
        public FrontEndDevice(PluginContainerServices services, String antennaName, String deviceName, StringUpdateSource updater)
        {
            super(services, antennaName, deviceName, updater);
            if (deviceName.isEmpty())
                logger.severe("AntennaPresentationModel->FrontEndDevice->constructor, Error: empty deviceName given.");
            currentBandStatusUpdater = new FrontEndCartridgeDevice();
            currentBandSource = new StringUpdateSource();
            standbyBand1StatusUpdater = new FrontEndCartridgeDevice();
            standbyBand1Source = new StringUpdateSource();
            standbyBand2StatusUpdater = new FrontEndCartridgeDevice();
            standbyBand2Source = new StringUpdateSource();
        }

        public void pollForSlowChanges()
        {
            if(null == this.hwControllerComponent || null == this.frontEndComponent)
            {
                acquireComponent();
            }
            interrogateComponentForSlowChanges();            
        }
        
        public void pollForFastChanges()
        {
            // NOOP
        }
        
        protected synchronized void acquireComponent() 
        {
            // potentially long-running methods must execute off the swing thread to avoid hanging GUI
            Runnable componentGetter = new Runnable()
            {
                public void run() 
                {
                    try
                    {
                        hwControllerComponent = HardwareControllerHelper.narrow(
                                pluginContainerServices.getComponentNonSticky(deviceName));
                        frontEndComponent = FrontEndHelper.narrow(hwControllerComponent);
                    }
                    catch (Throwable e) 
                    {
                        hwControllerComponent = null;
                        frontEndComponent = null;
                    }
                }
            };
            Thread componentGetterThread = new Thread(componentGetter);
            componentGetterThread.start();
        }
        
        // overriding the base class method to get specialized behavior unique to FrontEnd
        protected void interrogateComponentForSlowChanges()
        {
            specializedInterrogateComponentForSlowChanges();
        }
        
        @Override
        protected void specializedInterrogateComponentForSlowChanges() 
        {
            Runnable corbaRunner = new Runnable()
            {
                public void run()
                {
                    try
                    {
             
                        ReceiverBand currentBandNewValue = frontEndComponent.getCurrentBand();
                        if(!currentBandNewValue.equals(currentBandCurrentValue))
                        {
                            if(currentBandNewValue.equals(ReceiverBand.UNSPECIFIED))
                            {
                                NotifyChessboardStatusListenersThread guiThread = 
                                    new NotifyChessboardStatusListenersThread(currentBandStatusUpdater, 
                                            new ChessboardStatusEvent("", "", DeviceStatus.NOT_INSTALLED, NOT_INSTALLED_TEXT));
                                SwingUtilities.invokeLater(guiThread);
                                TwoStringGuiUpdateThread updateThread = new TwoStringGuiUpdateThread(currentBandSource); 
                                updateThread.setTitleLine(NONE_STRING);
                                SwingUtilities.invokeLater(updateThread);
                            }
                            else 
                            {
                                NotifyChessboardStatusListenersThread guiThread = 
                                    new NotifyChessboardStatusListenersThread(currentBandStatusUpdater,
                                            new ChessboardStatusEvent("", "", DeviceStatus.OPERATIONAL, OPERATIONAL_TEXT));
                                SwingUtilities.invokeLater(guiThread);
                                TwoStringGuiUpdateThread updateThread = new TwoStringGuiUpdateThread(currentBandSource); 
                                updateThread.setTitleLine(currentBandNewValue.toString());
                                SwingUtilities.invokeLater(updateThread);
                            }
                            currentBandCurrentValue = currentBandNewValue;
                        }
                        ReceiverBand standbyBandsNewValues[] = frontEndComponent.getPoweredBands();
                        standbyBandsNewValues = filterCurrentBandFromPoweredBands(standbyBandsNewValues, currentBandCurrentValue);
                        if(standbyBandsNewValues.length >= 1 && null != standbyBandsNewValues[0])
                        {
                            if(!standbyBandsNewValues[0].equals(standbyBand1CurrentValue))
                            {
                                if(standbyBandsNewValues[0].equals(ReceiverBand.UNSPECIFIED))
                                {
                                    NotifyChessboardStatusListenersThread guiThread = 
                                        new NotifyChessboardStatusListenersThread(standbyBand1StatusUpdater,
                                                new ChessboardStatusEvent("", "", DeviceStatus.NOT_INSTALLED, NOT_INSTALLED_TEXT));
                                    SwingUtilities.invokeLater(guiThread);
                                    TwoStringGuiUpdateThread updateThread = new TwoStringGuiUpdateThread(standbyBand1Source);
                                    updateThread.setTitleLine(NONE_STRING);
                                    SwingUtilities.invokeLater(updateThread);
                                }
                                else
                                {
                                    NotifyChessboardStatusListenersThread guiThread = 
                                        new NotifyChessboardStatusListenersThread(standbyBand1StatusUpdater,
                                                new ChessboardStatusEvent("", "", DeviceStatus.OPERATIONAL, OPERATIONAL_TEXT));
                                    SwingUtilities.invokeLater(guiThread);
                                    TwoStringGuiUpdateThread updateThread = new TwoStringGuiUpdateThread(standbyBand1Source);
                                    updateThread.setTitleLine(standbyBandsNewValues[0].toString());
                                    SwingUtilities.invokeLater(updateThread);
                                }
                                standbyBand1CurrentValue = standbyBandsNewValues[0];
                            }
                        }
                        else
                        {
                            NotifyChessboardStatusListenersThread guiThread = 
                                new NotifyChessboardStatusListenersThread(standbyBand1StatusUpdater,
                                        new ChessboardStatusEvent("", "", DeviceStatus.NOT_INSTALLED, NOT_INSTALLED_TEXT));
                            SwingUtilities.invokeLater(guiThread);
                            TwoStringGuiUpdateThread updateThread = new TwoStringGuiUpdateThread(standbyBand1Source);
                            updateThread.setTitleLine(NONE_STRING);
                            SwingUtilities.invokeLater(updateThread);
                        }
                        if(standbyBandsNewValues.length >= 2 && null != standbyBandsNewValues[1])
                        {
                            if(!standbyBandsNewValues[1].equals(standbyBand2CurrentValue))
                            {
                                if(standbyBandsNewValues[1].equals(ReceiverBand.UNSPECIFIED))
                                {
                                    NotifyChessboardStatusListenersThread guiThread = 
                                        new NotifyChessboardStatusListenersThread(standbyBand2StatusUpdater,
                                                new ChessboardStatusEvent("", "", DeviceStatus.NOT_INSTALLED, NOT_INSTALLED_TEXT));
                                    SwingUtilities.invokeLater(guiThread);
                                    TwoStringGuiUpdateThread updateThread = new TwoStringGuiUpdateThread(standbyBand2Source);
                                    updateThread.setTitleLine(NONE_STRING);
                                    SwingUtilities.invokeLater(updateThread);
                                }
                                else
                                {
                                    NotifyChessboardStatusListenersThread guiThread = 
                                        new NotifyChessboardStatusListenersThread(standbyBand2StatusUpdater,
                                                new ChessboardStatusEvent("", "", DeviceStatus.OPERATIONAL, OPERATIONAL_TEXT));    
                                    SwingUtilities.invokeLater(guiThread);
                                    TwoStringGuiUpdateThread updateThread = new TwoStringGuiUpdateThread(standbyBand2Source);
                                    updateThread.setTitleLine(standbyBandsNewValues[1].toString());
                                    SwingUtilities.invokeLater(updateThread);
                                }
                                standbyBand2CurrentValue = standbyBandsNewValues[1];
                            }
                        }
                        else
                        {
                            NotifyChessboardStatusListenersThread guiThread = 
                                new NotifyChessboardStatusListenersThread(standbyBand2StatusUpdater, 
                                        new ChessboardStatusEvent("", "", DeviceStatus.NOT_INSTALLED, NOT_INSTALLED_TEXT));
                            SwingUtilities.invokeLater(guiThread);
                            TwoStringGuiUpdateThread updateThread = new TwoStringGuiUpdateThread(standbyBand2Source);
                            updateThread.setTitleLine(NONE_STRING);
                            SwingUtilities.invokeLater(updateThread);
                        }
                    }
                    catch(Throwable ex)
                    {
                        frontEndComponent = null;
                        NotifyChessboardStatusListenersThread guiThread = 
                            new NotifyChessboardStatusListenersThread(currentBandStatusUpdater,
                                    new ChessboardStatusEvent("", "", DeviceStatus.NOT_INSTALLED, NOT_INSTALLED_TEXT));
                        SwingUtilities.invokeLater(guiThread);
                        TwoStringGuiUpdateThread updateThread = new TwoStringGuiUpdateThread(currentBandSource);
                        updateThread.setTitleLine(NONE_STRING);
                        SwingUtilities.invokeLater(updateThread);
                        
                        NotifyChessboardStatusListenersThread guiThread2 = 
                            new NotifyChessboardStatusListenersThread(standbyBand1StatusUpdater,
                                    new ChessboardStatusEvent("", "", DeviceStatus.NOT_INSTALLED, NOT_INSTALLED_TEXT));
                        SwingUtilities.invokeLater(guiThread2);
                        TwoStringGuiUpdateThread updateThread2 = new TwoStringGuiUpdateThread(standbyBand1Source);
                        updateThread2.setTitleLine(NONE_STRING);
                        SwingUtilities.invokeLater(updateThread2);
                        
                        NotifyChessboardStatusListenersThread guiThread3 = 
                            new NotifyChessboardStatusListenersThread(standbyBand2StatusUpdater,
                                    new ChessboardStatusEvent("", "", DeviceStatus.NOT_INSTALLED, NOT_INSTALLED_TEXT));
                        SwingUtilities.invokeLater(guiThread3);
                        TwoStringGuiUpdateThread updateThread3 = new TwoStringGuiUpdateThread(standbyBand2Source);
                        updateThread3.setTitleLine(NONE_STRING);
                        SwingUtilities.invokeLater(updateThread3);
                    }
                }
            };
            Thread corbaThread = new Thread(corbaRunner);
            corbaThread.start();
        }
        
        private ReceiverBand[] filterCurrentBandFromPoweredBands(ReceiverBand[] originalArray, 
            ReceiverBand filterBand)
        {
            ReceiverBand[] retVal = new ReceiverBand[0];
            List<ReceiverBand> filteredList = new ArrayList<ReceiverBand>();
            for(ReceiverBand band : originalArray) 
            {
                if(null != band) 
                {
                    if(null != filterBand)
                    {
                        if(!band.equals(filterBand)) 
                        {
                            filteredList.add(band);
                        }
                    }
                    else 
                    {
                        filteredList.add(band);
                    }
                }
            }
            retVal = (ReceiverBand[])filteredList.toArray(retVal);
            return retVal;
        }
    }
    
    private class AntennaInfoDevice implements IPollableDevice
    {
        private static final String NOT_ASSIGNED = "not assigned";
        private String antennaArrayName = NOT_ASSIGNED;
        private TwoStringGuiUpdateThread guiUpdateThread;
        
        /**
         * Constructor.
         */
        public AntennaInfoDevice(StringUpdateSource updater) 
        {
            this.guiUpdateThread = new TwoStringGuiUpdateThread(updater);
        }
        
        @Override
        public void pollForFastChanges() {
            // noop
        }

        @Override
        public void pollForSlowChanges() 
        {
            if(null != antennaComponent) 
            {
                try {
                    String newAntennaArrayName = antennaComponent.getArray();
                    updateArrayNameOnGuiIfNecessary(newAntennaArrayName);
                }
                catch(alma.ControlExceptions.INACTErrorEx ex)
                {
                    String newAntennaArrayName = NOT_ASSIGNED;
                    updateArrayNameOnGuiIfNecessary(newAntennaArrayName);
                }
            }
        }
 
        private void updateArrayNameOnGuiIfNecessary(String newAntennaArrayName)
        {
            if(!newAntennaArrayName.equals(this.antennaArrayName)) {
                this.guiUpdateThread.setTitleLine(antennaName);
                this.guiUpdateThread.setLineOne(newAntennaArrayName);
                SwingUtilities.invokeLater(guiUpdateThread);
                this.antennaArrayName = newAntennaArrayName;
            }
        }
    }
    
    private class FrontEndCartridgeDevice extends ChessboardStatusAdapter
    {
        @Override
        public ChessboardEntry[][] getStates() 
        {
            // Not applicable in this context since we are using chessboard status adapter
            // in a different context than a chessboard; will just stub this out to return null.
            return null;
        }

        @Override
        public void stop() 
        {
            // Not applicable in this context since we are using ChessboardStatusAdapter
            // in a different context than a chessboard; will just stub this out be a noop.    
        }
    }
    
    private class SasDevice extends PollableDevice
    {
        private String currentPhotonicRef = "";
        private SAS sasComponent = null;
        
        /**
         * Constructor.
         * @param services the PluginContainerServices object to use for getComponent calls etc.
         * @param antennaName the name of the antenna to which the device 'belongs'.
         * @param deviceName the component name to use for the device.
         */
        public SasDevice(PluginContainerServices services, String antennaName, String deviceName)
        {
            this(services, antennaName, deviceName, null);
        }
 
        /**
         * Constructor.
         * @param services the PluginContainerServices object to use for getComponent calls etc.
         * @param antennaName the name of the antenna to which the device 'belongs'.
         * @param deviceName the component name to use for the device.
         * @param updater the StringUpdateSource used to communicate string/value changes 
         * (as opposed to status/color changes) to the GUI.
         */
        public SasDevice(PluginContainerServices services, String antennaName, String deviceName, StringUpdateSource updater)
        {
            super(services, antennaName, deviceName, updater);
            if (deviceName.isEmpty())
                logger.severe("AntennaPresentationModel->SasDevice->" +
                        "constructor, Error: empty deviceName given.");
        }

        @Override
        protected void specializedInterrogateComponentForSlowChanges() 
        {
            String newPhotonicRef = "";
            try
            {
                if(null == sasComponent)
                {
                    sasComponent = SASHelper.narrow(deviceComponent);
                }
                newPhotonicRef = sasComponent.getPhotonicReference();
                if(!newPhotonicRef.equals(currentPhotonicRef))
                {
                    guiUpdateThread.setLineOne(newPhotonicRef);
                    SwingUtilities.invokeLater(guiUpdateThread);
                    currentPhotonicRef = newPhotonicRef;
                }
            }
            catch(Throwable ex)
            {
                sasComponent = null;
                guiUpdateThread.setLineOne(currentPhotonicRef + QUESTIONABLE_VALUE_STRING);
                SwingUtilities.invokeLater(guiUpdateThread);
            }
        }
    }
     
    private class Lo2Device extends PollableDevice
    {
        private static final String GHZ_UNITS = " GHz";
        private DecimalFormat decimalFormat = new DecimalFormat("0.000");
        private Boolean currentFringeTracking = false;
        private Double currentFrequency = 0d;
        private LO2 lo2Component = null;
               
        /**
         * Constructor.
         * @param services the PluginContainerServices object to use for getComponent calls etc.
         * @param antennaName the name of the antenna to which the device 'belongs'.
         * @param deviceName the component name to use for the device.
         */
        public Lo2Device(PluginContainerServices services, String antennaName, String deviceName)
        {
            this(services, antennaName, deviceName, null);
        }
 
        /**
         * Constructor.
         * @param services the PluginContainerServices object to use for getComponent calls etc.
         * @param antennaName the name of the antenna to which the device 'belongs'.
         * @param deviceName the component name to use for the device.
         * @param updater the StringUpdateSource used to communicate string/value changes 
         * (as opposed to status/color changes) to the GUI.
         */
        public Lo2Device(PluginContainerServices services, String antennaName, String deviceName, StringUpdateSource updater)
        {
            super(services, antennaName, deviceName, updater);
            if (deviceName.isEmpty())
                logger.severe("AntennaPresentationModel->Lo2Device->" +
                        "constructor, Error: empty deviceName given.");
        }

        @Override
        protected void specializedInterrogateComponentForSlowChanges() 
        {
            Boolean newFringeTracking = false;
            Double newFrequency = 0d;
            try
            {
                if(null == lo2Component)
                {
                    lo2Component = LO2Helper.narrow(deviceComponent);
                }

                newFringeTracking = lo2Component.FringeTrackingEnabled();
                newFrequency = lo2Component.GetNominalFrequency() / 1e9;
                if(newFringeTracking != currentFringeTracking || (Math.abs(newFrequency-currentFrequency) >= FLOATING_POINT_CHANGE_THRESHOLD))
                {
                    guiUpdateThread.setLineOneAndLineTwo(onOffFormatter.format(newFringeTracking), decimalFormat.format(newFrequency) + GHZ_UNITS);
                    SwingUtilities.invokeLater(guiUpdateThread);
                    currentFringeTracking = newFringeTracking;
                    currentFrequency = newFrequency;
                }
            }
            catch(Throwable ex)
            {
                lo2Component = null;
                guiUpdateThread.setLineOneAndLineTwo(onOffFormatter.format(currentFringeTracking) + QUESTIONABLE_VALUE_STRING, 
                        decimalFormat.format(currentFrequency) + GHZ_UNITS + QUESTIONABLE_VALUE_STRING);
                SwingUtilities.invokeLater(guiUpdateThread);
            }
        }
    }
    
    private class OnOffFormatter
    {
        private final static String ON = "ON";
        private final static String OFF = "OFF";
        
        /**
         * Formats a boolean to a string of either "On" or "Off"
         * @param value the value to format
         * @return a string representation of the value, either "On" or "Off"
         */
        public String format(boolean value)
        {
            String retVal = OFF;
            if(value)
            {
                retVal = ON;
            }
            return retVal;
        }
    }
    
    private class IfProcDevice extends PollableDevice
    {
        private String currentZeroOneBasebandPair = "USB ?";
        private String currentTwoThreeBasebandPair  = "USB ?";
        private IFProc ifprocComponent = null;
               
        /**
         * Constructor.
         * @param services the PluginContainerServices object to use for getComponent calls etc.
         * @param antennaName the name of the antenna to which the device 'belongs'.
         * @param deviceName the component name to use for the device.
         */
        public IfProcDevice(PluginContainerServices services, String antennaName, String deviceName)
        {
            this(services, antennaName, deviceName, null);
        }
 
        /**
         * Constructor.
         * @param services the PluginContainerServices object to use for getComponent calls etc.
         * @param antennaName the name of the antenna to which the device 'belongs'.
         * @param deviceName the component name to use for the device.
         * @param updater the StringUpdateSource used to communicate string/value changes 
         * (as opposed to status/color changes) to the GUI.
         */
        public IfProcDevice(PluginContainerServices services, String antennaName, String deviceName, StringUpdateSource updater)
        {
            super(services, antennaName, deviceName, updater);
            if (deviceName.isEmpty())
                logger.severe("AntennaPresentationModel->IFProcDevice->" +
                        "constructor, Error: empty deviceName given.");
        }

        @Override
        protected void specializedInterrogateComponentForSlowChanges() 
        {
            String newZeroOneBasebandPair = "USB";
            String newTwoThreeBasebandPair = "USB";
            try
            {
                if(null == ifprocComponent)
                {
                    ifprocComponent = IFProcHelper.narrow(deviceComponent);
                }
                boolean[] signalPathsResultArray = ifprocComponent.GET_SIGNAL_PATHS(new LongHolder());

                newZeroOneBasebandPair = signalPathsResultArray[4] == true ? "USB" : "LSB";
                newTwoThreeBasebandPair = signalPathsResultArray[5] == true ? "USB" : "LSB";
                if(!newZeroOneBasebandPair.equals(currentZeroOneBasebandPair) || !newTwoThreeBasebandPair.equals(currentTwoThreeBasebandPair))
                {
                    guiUpdateThread.setLineOneAndLineTwo(newZeroOneBasebandPair, newTwoThreeBasebandPair);
                    SwingUtilities.invokeLater(guiUpdateThread);
                    currentZeroOneBasebandPair = newZeroOneBasebandPair;
                    currentTwoThreeBasebandPair = newTwoThreeBasebandPair;
                }
            }
            catch(Throwable ex)
            {
                ifprocComponent = null;
                String zeroOneString = currentZeroOneBasebandPair;
                String oneTwoString = currentTwoThreeBasebandPair;
                if(!currentZeroOneBasebandPair.contains("?"))
                {
                    zeroOneString += QUESTIONABLE_VALUE_STRING;
                }
                if(!currentTwoThreeBasebandPair.contains("?"))
                {
                    oneTwoString += QUESTIONABLE_VALUE_STRING;
                }
                guiUpdateThread.setLineOneAndLineTwo(zeroOneString, oneTwoString);
                SwingUtilities.invokeLater(guiUpdateThread);
            }
        }
    }
    
    private class MountDevice extends PollableDevice
    {
        private Double currentRa = 0d;
        private Double currentDec = 0d;
        private MountController mountComponent = null;
               
        /**
         * Constructor.
         * @param services the PluginContainerServices object to use for getComponent calls etc.
         * @param antennaName the name of the antenna to which the device 'belongs'.
         * @param deviceName the component name to use for the device.
         */
        public MountDevice(PluginContainerServices services, String antennaName, String deviceName)
        {
            this(services, antennaName, deviceName, null);
        }
 
        /**
         * Constructor.
         * @param services the PluginContainerServices object to use for getComponent calls etc.
         * @param antennaName the name of the antenna to which the device 'belongs'.
         * @param deviceName the component name to use for the device.
         * @param updater the StringUpdateSource used to communicate string/value changes 
         * (as opposed to status/color changes) to the GUI.
         */
        public MountDevice(PluginContainerServices services, String antennaName, String deviceName, StringUpdateSource updater)
        {
            super(services, antennaName, deviceName, updater);
            if (deviceName.isEmpty())
                logger.severe("AntennaPresentationModel->MountDevice->constructor, Error: empty deviceName given.");
        }

        @Override
        protected void specializedInterrogateComponentForSlowChanges() 
        {
            // NOOP for slow poll - see fast poll method
        }
        
        protected void specializedInterrogateComponentForFastChanges()
        {
            Double newRa = 0d;
            Double newDec = 0d;
            try
            {
                if(null == mountComponent)
                {
                    acquireMountController();
                }

                newRa = mountComponent.getPointingData().measuredTarget.ra;
                newDec = mountComponent.getPointingData().measuredTarget.dec;
                if(newRa != currentRa || newDec != currentDec)
                {
                    ValueHolder<Double> raToConvert = new ValueHolder<Double>();
                    raToConvert.setValue(newRa);
                    ValueHolder<Double> decToConvert = new ValueHolder<Double>();
                    decToConvert.setValue(newDec);
                    HMSAngleConverter raConverter = new HMSAngleConverter(raToConvert);
                    DMSAngleConverter decConverter = new DMSAngleConverter(decToConvert);
                    guiUpdateThread.setLineOneAndLineTwo(raConverter.getString(), decConverter.getString());
                    SwingUtilities.invokeLater(guiUpdateThread);
                    currentRa = newRa;
                    currentDec = newDec;
                }
            }
            catch(Throwable ex)
            {
                mountComponent = null;
                ValueHolder<Double> raToConvert = new ValueHolder<Double>();
                raToConvert.setValue(currentRa);
                ValueHolder<Double> decToConvert = new ValueHolder<Double>();
                decToConvert.setValue(currentDec);
                HMSAngleConverter raConverter = new HMSAngleConverter(raToConvert);
                HMSAngleConverter decConverter = new HMSAngleConverter(decToConvert);
                guiUpdateThread.setLineOneAndLineTwo(raConverter.getString() + QUESTIONABLE_VALUE_STRING, 
                        decConverter.getString() + QUESTIONABLE_VALUE_STRING);
                SwingUtilities.invokeLater(guiUpdateThread);
            }
        }
        
        private synchronized void acquireMountController()
        {
            String antennaComponentName = Name.ControlPrefix + this.antennaName;
            Antenna antenna = null;
            try {
                Object antennaObj = this.pluginContainerServices.getComponentNonSticky(antennaComponentName);
                antenna = AntennaHelper.narrow(antennaObj);
                String mountControllerName = antenna.getMountControllerName();
                ComponentQueryDescriptor descriptor = new ComponentQueryDescriptor(mountControllerName, ACSComponentsManager.MOUNTCONTROLLER_IDL);
                Object mountControllerObject = this.pluginContainerServices.getCollocatedComponent(descriptor, false, antennaComponentName);
                mountComponent = MountControllerHelper.narrow(mountControllerObject);
                mountComponent.allocate(this.antennaName);
            }
            catch(Throwable ex)
            {
                // TODO - log something?
                mountComponent = null;
            }
        }
    }
    
    private class NoExtraDataDevice extends PollableDevice
    {     
        /**
         * Constructor.
         * @param services the PluginContainerServices object to use for getComponent calls etc.
         * @param antennaName the name of the antenna to which the device 'belongs'.
         * @param deviceName the component name to use for the device.
         */
        public NoExtraDataDevice(PluginContainerServices services, String antennaName, String deviceName)
        {
            this(services, antennaName, deviceName, null);
        }
 
        /**
         * Constructor.
         * @param services the PluginContainerServices object to use for getComponent calls etc.
         * @param antennaName the name of the antenna to which the device 'belongs'.
         * @param deviceName the component name to use for the device.
         * @param updater the StringUpdateSource used to communicate string/value changes 
         * (as opposed to status/color changes) to the GUI.
         */
        public NoExtraDataDevice(PluginContainerServices services, String antennaName, String deviceName, StringUpdateSource updater)
        {
            super(services, antennaName, deviceName, updater);
            if (deviceName.isEmpty())
                logger.severe("AntennaPresentationModel->NoExtraDataDevice->constructor, Error: empty deviceName given.");
        }

        @Override
        protected void specializedInterrogateComponentForSlowChanges() 
        {
            // Nothing to do; NOOP
        }
    }
    
    private class TwoStringGuiUpdateThread extends Thread
    {
        private StringUpdateSource guiStringUpdater;
        private String lineOne = "";
        private String lineTwo = "";
        private String titleLine = null;
        
        /**
         * Constructor.
         * @param guiStringUpdater the StringUpdateSource object to use for GUI string updates.
         */
        public TwoStringGuiUpdateThread(StringUpdateSource guiStringUpdater)
        {
            this.guiStringUpdater = guiStringUpdater;
        }
        
        public void run()
        {
            guiStringUpdater.setLineOneAndLineTwo(lineOne, lineTwo);
            guiStringUpdater.setTitleLine(titleLine);
        }
        
        /**
         * Sets line one and line two for this object, which will later be written back to the GUI for screen updates.
         * @param lineOne
         * @param lineTwo
         */
        public void setLineOneAndLineTwo(String lineOne, String lineTwo)
        {
            this.lineOne = lineOne;
            this.lineTwo = lineTwo;
        }
        
        /**
         * Sets line one and line two for this object, which will later be written back to the GUI for screen updates.
         * @param lineOne
         */
        public void setLineOne(String lineOne)
        {
            this.lineOne = lineOne;
        }
        
        /**
         * Setter for the title string.
         * @param title
         */
        public void setTitleLine(String title)
        {
            this.titleLine = title;
        }
    }
    
    private class NotifyChessboardStatusListenersThread extends Thread
    {
        private ChessboardStatusAdapter adapter;
        private ChessboardStatusEvent event;
        
        /**
         * Constructor.
         * @param adapter class to use to notify listeners.
         * @param event the event to notify the listeners with.
         */
        public NotifyChessboardStatusListenersThread(ChessboardStatusAdapter adapter, ChessboardStatusEvent event)
        {
            this.adapter = adapter;
            this.event = event;
        }
        
        public void run()
        {
            adapter.notifyListeners(event);
        };    
    }
    
    private class FloogDevice extends PollableDevice
    {
        private static final String MHZ_UNITS = " MHz";
        private DecimalFormat decimalFormat = new DecimalFormat("0.00");
        private Boolean currentFringeTracking = false;
        private Double currentFrequency = 10d;
        private FLOOG floogComponent = null; 
        
        /**
         * Constructor.
         * @param services the PluginContainerServices object to use for getComponent calls etc.
         * @param antennaName the name of the antenna to which the device 'belongs'.
         * @param deviceName the component name to use for the device.
         */
        public FloogDevice(PluginContainerServices services, String antennaName, String deviceName)
        {
            this(services, antennaName, deviceName, null);
        }
 
        /**
         * Constructor.
         * @param services the PluginContainerServices object to use for getComponent calls etc.
         * @param antennaName the name of the antenna to which the device 'belongs'.
         * @param deviceName the component name to use for the device.
         * @param updater the StringUpdateSource used to communicate string/value changes 
         * (as opposed to status/color changes) to the GUI.
         */
        public FloogDevice(PluginContainerServices services, String antennaName, String deviceName, StringUpdateSource updater)
        {
            super(services, antennaName, deviceName, updater);
            if (deviceName.isEmpty())
                logger.severe("AntennaPresentationModel->FloogDevice->constructor, Error: empty deviceName given.");
        }

        @Override
        protected void specializedInterrogateComponentForSlowChanges() 
        {
            Double newFrequency = 0d;
            Boolean newFringeTracking = false;
            try
            {
                if(null == floogComponent)
                {
                    floogComponent = FLOOGHelper.narrow(deviceComponent);
                }
                newFrequency = floogComponent.GetNominalFrequency() / 1e6;
                newFringeTracking = floogComponent.FringeTrackingEnabled();
                if(newFringeTracking != currentFringeTracking || (Math.abs(newFrequency-currentFrequency) >= FLOATING_POINT_CHANGE_THRESHOLD))
                {
                    guiUpdateThread.setLineOneAndLineTwo(onOffFormatter.format(newFringeTracking), decimalFormat.format(newFrequency) + MHZ_UNITS);
                    SwingUtilities.invokeLater(guiUpdateThread);
                    currentFringeTracking = newFringeTracking;
                    currentFrequency = newFrequency;
                }
            }
            catch(Throwable ex)
            {
                floogComponent = null;
                guiUpdateThread.setLineOneAndLineTwo(onOffFormatter.format(currentFringeTracking) + QUESTIONABLE_VALUE_STRING, 
                        decimalFormat.format(currentFrequency) + MHZ_UNITS + QUESTIONABLE_VALUE_STRING);
                SwingUtilities.invokeLater(guiUpdateThread);
            }
        }
    }
    
    private abstract class PollableDevice extends ChessboardStatusAdapter implements IPollableDevice
    {
        protected PluginContainerServices pluginContainerServices;
        protected String antennaName;
        protected String deviceName;
        protected StringUpdateSource guiStringUpdater;
        protected HardwareDevice deviceComponent = null;
        protected HwState currentState = HwState.Undefined;
        protected TwoStringGuiUpdateThread guiUpdateThread;

        /**
         * Constructor.deviceName
         * @param services the PluginContainerServices object to use for getComponent calls etc.
         * @param antennaName the name of the antenna to which the device 'belongs'.
         * @param deviceName the component name to use for the device.
         */
        public PollableDevice(PluginContainerServices services, String antennaName, String deviceName)
        {
            this(services, antennaName, deviceName, null);
        }
        
        /**
         * Constructor.
         * @param services the PluginContainerServices object to use for getComponent calls etc.
         * @param antennaName the name of the antenna to which the device 'belongs'.
         * @param deviceName the component name to use for the device.
         * @param updater the StringUpdateSource used to communicate string/value changes 
         * (as opposed to status/color changes) to the GUI.
         */
        public PollableDevice(PluginContainerServices services, String antennaName, String deviceName, StringUpdateSource updater)
        {
            this.pluginContainerServices = services;
            this.antennaName = antennaName;
            this.deviceName = deviceName;
            this.guiStringUpdater = updater;
            this.guiUpdateThread = new TwoStringGuiUpdateThread(updater);
            if (deviceName.isEmpty())
               logger.severe("AntennaPresentationModel->PollableDevice->constructor, Error: empty deviceName given.");
        }
        
        private void notifyListenersOnGuiThread(ChessboardStatusEvent evt) 
        {
            NotifyChessboardStatusListenersThread guiThread = new NotifyChessboardStatusListenersThread(this, evt);
            SwingUtilities.invokeLater(guiThread);
        }

        protected synchronized void acquireComponent() 
        {
            if(null == this.deviceComponent)
            {
                Runnable corbaRunner = new Runnable()
                {
                    public void run()
                    {
                        if (!deviceName.isEmpty() && deviceName != null){
                            try 
                            {
                                deviceComponent = HardwareDeviceHelper.narrow(
                                        pluginContainerServices.getComponentNonSticky(deviceName));
                            } 
                            catch (Throwable e) 
                            {
                                deviceComponent = null;
                            }
                        }
                        else
                            logger.severe("AntennaPresentationModel->PollableDevice->acquireComponent; " +
                                          "Error: deviceName is empty when atempting to get a device!");
                    }
                };
                Thread corbaThread = new Thread(corbaRunner);
                corbaThread.start();
            }
        }
        
        protected void interrogateComponentForSlowChanges()
        {
            Runnable corbaRunner = new Runnable()
            {
                public void run()
                {
                    HwState newState = HwState.Stop;
                    boolean inError = false;
                    String errorMessage = ERROR_PREFIX;

                    try
                    {
                        if(null != deviceComponent)
                        {
                            newState = deviceComponent.getHwState();
                            inError = deviceComponent.inErrorState();
                            errorMessage = ERROR_PREFIX + deviceComponent.getErrorMessage();
                        }
                        else
                        {
                            newState = HwState.Stop;
                            inError = true;
                            errorMessage= ERROR_PREFIX + COMPONENT_NOT_ACTIVATED_TEXT;
                        }
                    }
                    catch(Throwable ex)
                    {
                        newState = HwState.Stop;
                        inError = true;
                        errorMessage= ERROR_PREFIX + COMPONENT_NOT_ACTIVATED_TEXT;
                    }

                    if(!currentState.equals(newState))
                    {
                        switch (newState.value())
                        {
                        case HwState._Operational:
                            if(inError)
                            {
                                notifyListenersOnGuiThread(new ChessboardStatusEvent("", "", DeviceStatus.ERROR, errorMessage));                
                            }
                            else 
                            {
                                notifyListenersOnGuiThread(new ChessboardStatusEvent("", "", DeviceStatus.OPERATIONAL, OPERATIONAL_TEXT));
                            }
                            break;
                        case HwState._Undefined:
                            notifyListenersOnGuiThread(new ChessboardStatusEvent("", "", DeviceStatus.UNKNOWN, NO_COMMUNICATION_TEXT));
                        default:
                            notifyListenersOnGuiThread(new ChessboardStatusEvent("", "", DeviceStatus.NOT_OPERATIONAL, NOT_OPERATIONAL_TEXT));
                        break;
                        }
                        currentState = newState;
                    }
                    specializedInterrogateComponentForSlowChanges();
                }
            };
            Thread corbaThread = new Thread(corbaRunner);
            corbaThread.start();
        }
        
        protected void interrogateComponentForFastChanges()
        {
            Runnable corbaRunner = new Runnable()
            {
                public void run()
                {
                    if(null != deviceComponent)
                    {
                        specializedInterrogateComponentForFastChanges();
                    }
                }
            };
            Thread corbaThread = new Thread(corbaRunner);
            corbaThread.start();
        }
        
        protected abstract void specializedInterrogateComponentForSlowChanges();
        protected void specializedInterrogateComponentForFastChanges() 
        {
            // default is a NOOP; subclasses may override as needed.
        }
        
        public void pollForSlowChanges()
        {
            if(null == this.deviceComponent)
            {
                acquireComponent();
            }
            interrogateComponentForSlowChanges();
        }
        
        public void pollForFastChanges()
        {
            if(null == this.deviceComponent)
            {
                acquireComponent();
            }
            interrogateComponentForFastChanges();
        }
        
        @Override
        public ChessboardEntry[][] getStates() 
        {
            // Not applicable in this context since we are using chessboard status adapter
            // in a different context than a chessboard; will just stub this out to return null.
            return null;
        }

        @Override
        public void stop() 
        {
            // Not applicable in this context since we are using ChessboardStatusAdapter
            // in a different context than a chessboard; will just stub this out be a noop.    
        }
    }
    
    private interface IPollableDevice 
    {
        /**
         * Polls for any changes (on a slow poll interval) in the state of the device, and notifies 
         * any interested observers if changes are detected. This encompasses both 
         * general status (e.g. Operational, Error, or Not Operational) and specific data 
         * values.
         */
        public void pollForSlowChanges();
        
        /**
         * Polls for any changes (on a faster poll interval) in the state of the device, and notifies 
         * any interested observers if changes are detected. This encompasses both 
         * general status (e.g. Operational, Error, or Not Operational) and specific data 
         * values.
         */
        public void pollForFastChanges();
    }

 }
