package alma.control.gui.antennachessboard;

/**
 * Classes that are interested in receiving string updates from some source
 * can implement this interface, provided the source implements the StringUpdatePublisher
 * interface.
 * 
 * @author Steve Harrington
 * @see StringUpdatePublisher
 * @see StringUpdateEvent
 */
public interface StringUpdateListener 
{
	/**
	 * Called when strings have changed.
	 * @param evt an event containing details about the change.
	 */
	public void updateStrings(StringUpdateEvent evt);
}
