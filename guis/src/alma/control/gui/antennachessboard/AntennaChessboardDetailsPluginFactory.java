package alma.control.gui.antennachessboard;

import alma.common.gui.chessboard.ChessboardDetailsPluginFactory;
import alma.common.gui.chessboard.ChessboardDetailsPlugin;
import alma.common.gui.chessboard.ChessboardDetailsPluginListener;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

/**
 * This class provides the details for the <code>AntennaChessboardPlugin</code>;
 * in other words, it provides the display for the details on a chessboard cell, when
 * the user asks for details via double-click or button push. For the chessboard in question here,
 * the details display(s) will be the antenna user interface.
 * 
 * @author Steve Harrington
 *
 */
public class AntennaChessboardDetailsPluginFactory implements ChessboardDetailsPluginFactory 
{
    private static final int INSTANCES = 1;

    /**
     * @see alma.common.gui.chessboard.ChessboardDetailsPluginFactory
     */
    public ChessboardDetailsPlugin[] instantiateDetailsPlugin(
            String[] names, PluginContainerServices containerServices,
	    ChessboardDetailsPluginListener listener, String pluginTitle, String selectionTitle) {

        AntennaChessboardDetailsPlugin[] antennaPlugin = new AntennaChessboardDetailsPlugin[INSTANCES];
        if(null != names && names.length > 0) {
            antennaPlugin[0] = new AntennaChessboardDetailsPlugin(names, listener, containerServices, pluginTitle, selectionTitle);
        }
	return antennaPlugin;
    }
}


