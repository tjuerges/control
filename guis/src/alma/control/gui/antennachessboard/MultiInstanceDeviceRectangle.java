package alma.control.gui.antennachessboard;

import alma.common.gui.chessboard.ChessboardDetailsPlugin;
import alma.common.gui.chessboard.ChessboardDetailsPluginListener;
import alma.exec.extension.subsystemplugin.PluginContainerException;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

import java.awt.event.MouseEvent;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Display a rectangle with one, two, or three text labels (which may change) 
 * and a status with color coding (which may also change) that show key properties
 * of a hardware device in an antenna.  Allow the user to launch a Device GUI for a 
 * detailed view of the associated device.  
 * 
 * This class supports hardware devices that have several instances in an antenna.
 * 
 * @author Steve Harrington sharring@nrao.edu
 * @author Scott Rankin     srankin@nrao.edu
 */
@SuppressWarnings("serial")
public class MultiInstanceDeviceRectangle extends SingleInstanceDeviceRectangle {

    public MultiInstanceDeviceRectangle(DeviceStatus initialState,
            PluginContainerServices containerServices) {
        super(initialState, containerServices);
    }

    /**
     * Constructor.
     * @param initialState the initial state of the rectangle.
     * @param services the container services used to launch child plugins.
     * @param detailsPluginClass the class, if any, which we will instantiate to 
     *        launch a plugin upon double click of the rectangle.
     * @param antennaNames the names of the antenna(s) for this rectangle; 
     *        used when launching device plugins from a rectangle upon double click.
     * @param deviceText the text to use for title bar of device plugin
     *        that is launched from double click.
     * @param deviceInstance identifies which instance of a device to connect a GUI to.
     */
    public MultiInstanceDeviceRectangle(DeviceStatus initialState,
            PluginContainerServices services,
            Class<? extends ChessboardDetailsPlugin> detailsPluginClass,
            String[] antennaNames, String deviceText, int deviceInstance) {
        super(initialState, services, detailsPluginClass, antennaNames,
                deviceText);
        this.deviceInstance = deviceInstance;
    }

    public void mouseClicked(MouseEvent e) 
    {
        if(contains(e.getX(), e.getY())) 
        {
            if(2 == e.getClickCount()) 
            {
                if(null != this.devicePluginClass && null != this.containerServices)
                {
                    try {
                        //ChessboardDetailsPlugin pluginToSpawn = ; 
                        Constructor constructor = null;
                        try {
                            constructor = this.devicePluginClass.getConstructor(String[].class, ChessboardDetailsPluginListener.class, PluginContainerServices.class, String.class, String.class, int.class);                           
                        } catch (SecurityException e1) {
                            this.containerServices.getLogger().severe("Security exception while trying to create an instance of a Device GUI panel: " + devicePluginClass.getName());
                            e1.printStackTrace();
                        } catch (NoSuchMethodException e1) {
                            this.containerServices.getLogger().severe("NoSuchMethod exception while trying to create an instance of a Device GUI panel: " + devicePluginClass.getName());
                            e1.printStackTrace();
                        }
                        if(null != constructor)
                        {
                            ChessboardDetailsPlugin pluginToLaunch = null;
                            try {
                                pluginToLaunch = (ChessboardDetailsPlugin)constructor.newInstance(this.antennaNames, null, this.containerServices, 
                                            this.deviceNameText + this.antennaNames[0], this.deviceNameText + this.antennaNames[0], this.deviceInstance);
                            } catch (IllegalArgumentException e1) {
                                this.containerServices.getLogger().warning("Illegal argument exception, trying to continue.");
                            } catch (InstantiationException e1) {
                                this.containerServices.getLogger().warning("Instantiation exception, trying to continue.");
                            } catch (IllegalAccessException e1) {
                                this.containerServices.getLogger().warning("Access exception, trying to continue.");
                            } catch (InvocationTargetException e1) {
                                this.containerServices.getLogger().warning("Invocation exception, trying to continue.");
                            }
                            if(null != pluginToLaunch)
                            {
                                this.containerServices.startChildPlugin(pluginToLaunch.getUniqueName(), pluginToLaunch);
                            } 
                            else 
                            {
                                this.containerServices.getLogger().severe("Failed to create an instance of a Device GUI panel: " + devicePluginClass.getName());
                            }
                        }
                    } 
                    catch (PluginContainerException e1) 
                    {
                        this.containerServices.getLogger().warning("Could not launch details plugin");
                    }
                }
            }
        }
    }
    
    private int deviceInstance = Integer.MIN_VALUE;
}
