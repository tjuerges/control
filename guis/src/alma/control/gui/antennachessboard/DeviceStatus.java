package alma.control.gui.antennachessboard;

import java.awt.Color;

import alma.common.gui.chessboard.ChessboardStatus;

/**
 * Class used to denote the status of a device in the antenna summary GUI.
 * @author Steve Harrington
 */
public class DeviceStatus implements ChessboardStatus 
{
	private final Color DARK_RED = new Color(195, 15, 15);
	private final Color DARK_YELLOW = new Color(247, 230, 42);
	private final Color DARK_GREEN = new Color(19, 90, 51);
	private final Color DARK_BLUE = new Color(98, 159, 187);
	
	private static final String NOT_INSTALLED_STRING = "Not installed";
	private static final String NOT_OPERATIONAL_STRING = "Not operational";
	private static final String ERROR_STRING = "Error";
	private static final String OPERATIONAL_STRING = "Operational";
	private static final String UNKNOWN_STRING = "No communication; device status unknown";
	private static final String COMPOSITE_DEVICE_STRING = "Composite device";
	private static final String INVISIBLE_DEVICE_STRING = "";
	
	private boolean shouldFlash;
	private String description;
	private Color bgColor;
	private Color fgColor;
	private boolean isSelectable;
	private boolean hasBorder;
	private static ChessboardStatus[] values = null;
	
	/**
	 * Private constructor enforces typesafe enum pattern.
	 * @param description the description of the enum
	 */
	private DeviceStatus(String description)
	{
		this.bgColor = null;
		this.fgColor = null;
		this.description = description;
		this.shouldFlash = false;
		this.hasBorder = false;
		
		// configure the status with proper color and/or flashing attributes
		if(description.equals(NOT_INSTALLED_STRING)) {
			this.bgColor = Color.GRAY;
			this.fgColor = Color.WHITE;
			this.isSelectable = true;
		} 
		else if(description.equals(NOT_OPERATIONAL_STRING)) {
			this.bgColor = DARK_RED;
			this.fgColor = Color.WHITE;
			this.isSelectable = true;
		} 
		else if(this.description.equals(ERROR_STRING)) {
			this.bgColor = DARK_YELLOW;
			this.fgColor = Color.BLACK;
			this.isSelectable = true;
			this.hasBorder = true;
		} 
		else if(this.description.equals(OPERATIONAL_STRING)) {
			this.bgColor = DARK_GREEN;
			this.fgColor = Color.WHITE;
			this.isSelectable = true;
		} 
		else if(this.description.equals(UNKNOWN_STRING)) {
			this.bgColor = Color.BLACK;
			this.fgColor = Color.WHITE;
			this.isSelectable = false;
		}
		else if(this.description.equals(COMPOSITE_DEVICE_STRING)) {
			this.bgColor = DARK_BLUE;
			this.fgColor = Color.BLACK;
			this.isSelectable = false;
		}
		else if(this.description.equals(INVISIBLE_DEVICE_STRING)) {
			this.bgColor = null;
			this.fgColor = null;
			this.isSelectable = false;
		}
	}
	
	// Create the fixed set of allowed instances (these will be the enum values).
	/** 
	 * Status for devices that are not installed.
	 */
	public final static DeviceStatus NOT_INSTALLED = 
		new DeviceStatus(NOT_INSTALLED_STRING);
	
	/** 
	 * Status for devices that are not operational.
	 */
	public final static DeviceStatus NOT_OPERATIONAL = 
		new DeviceStatus(NOT_OPERATIONAL_STRING);
	
	/** 
	 * Status for devices that are installed and operational, but in an error state.
	 */
	public final static DeviceStatus ERROR =
		new DeviceStatus(ERROR_STRING);
	
	/** 
	 * Status for devices that are operational.
	 */
	public final static DeviceStatus OPERATIONAL =
		new DeviceStatus(OPERATIONAL_STRING);
	
	/** 
	 * Status for devices whose status is "unkown" e.g. if communication to the device is lost.
	 */
	public final static DeviceStatus UNKNOWN =
		new DeviceStatus(UNKNOWN_STRING);
	
	/** 
	 * Status for virtual devices that are comprised of other devices (e.g. the front end).
	 */
	public final static DeviceStatus COMPOSITE_DEVICE =
		new DeviceStatus(COMPOSITE_DEVICE_STRING);
	
	/** 
	 * Status for invisible devices that are comprised of other devices (e.g. the front end).
	 * This is a HACK to help draw boxes that don't show up; not really a status in the literal sense.
	 */
	public final static DeviceStatus INVISIBLE_DEVICE =
		new DeviceStatus(INVISIBLE_DEVICE_STRING);
	
	/** 
	 * Required method of <code>ChessboardStatus</code> interface.
	 * 
	 * @return the background color that should be used to render the status in the user interface.
	 */
	public Color getBgColor() {
		return this.bgColor;
	}

	/** 
	 * Required method of <code>ChessboardStatus</code> interface.
	 * 
	 * @return the foreground color that should be used to render the status in the user interface.
	 */
	public Color getFgColor() {
		return this.fgColor;
	}
	
	/**
	 * Required method of the <code>ChessboardStatus</code> interface.
	 * 
	 * @return boolean indicating whether this status should be selectable in the UI.
	 */
	public boolean isSelectable() 
	{
		return this.isSelectable;
	}
	
	/** 
	 * Required method of <code>ChessboardStatus</code> interface.
	 * 
	 * @return the description of the status.
	 */
	public String getDescription() {
		return this.description;
	}
	
	/**
	 * Overriding generic toString method from <code>Object</code> to get
	 * something more user-friendly.
	 * 
	 * @return a string representation of this status which is the same as the status name.
	 */
	@Override
	public String toString() {
		return this.description;
	}
	
	/** 
	 * Required method of <code>ChessboardStatus</code> interface.
	 * 
	 * @return boolean indicating whether the status should be rendered
	 * in the user interface as a flashing cell.
	 */
	public boolean shouldFlash() {
		return this.shouldFlash;
	}

	/** 
	 * Getter for whether the status should have a border, when drawn on the screen.
	 * 
	 * @return boolean indicating whether the status should be rendered
	 * in the user interface with a border.
	 */
	public boolean hasBorder() {
		return this.hasBorder;
	}
	
	/** 
	 * Required method of the <code>ChessboardStatus</code> interface. Returns
	 * all of the valid instances of this enum class. This is mostly just used 
	 * for testing. Method is synchronized because it employs a singleton
	 * for the values array. For an example of its use in a test setting, 
	 * see the <code>ChessboardTest</code> and <code>ExampleChessboardStatusProvider</code> classes.
	 * 
	 * @return the full set of statuses for this enum.
	 */
	public synchronized ChessboardStatus[] values() 
	{
		// use a singleton-style mechanism to return the values
		if(null == DeviceStatus.values) 
		{
			DeviceStatus.values = new DeviceStatus[7];

			DeviceStatus.values[0] = DeviceStatus.NOT_OPERATIONAL;
			DeviceStatus.values[1] = DeviceStatus.NOT_INSTALLED;
			DeviceStatus.values[2] = DeviceStatus.ERROR;
			DeviceStatus.values[3] = DeviceStatus.OPERATIONAL;
			DeviceStatus.values[4] = DeviceStatus.UNKNOWN;
			DeviceStatus.values[5] = DeviceStatus.COMPOSITE_DEVICE;
			DeviceStatus.values[6] = DeviceStatus.INVISIBLE_DEVICE;
		}
		
		return DeviceStatus.values;
	}
}
