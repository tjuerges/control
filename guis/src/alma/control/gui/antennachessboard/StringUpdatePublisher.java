package alma.control.gui.antennachessboard;

/**
 * Interface which can be implemented by parties wishing to notify listeners of changes to string values.
 * @author Steve Harrington
 * @see StringUpdateListener
 * @see StringUpdateEvent
 */
public interface StringUpdatePublisher 
{
	/**
	 * Adds a listener to the list of classes which will be notified when there is a 
	 * change to the string values that are being 'watched'. 
	 * @param listener the listener to add.
	 */
	public void addListener(StringUpdateListener listener);
	
	/**
	 * Removes a listener from the list of classes which will be notified when there is a
	 * change to the string values that are being 'watched'.
	 * @param listener the listener to remove.
	 */
	public void removeListener(StringUpdateListener listener);
	
	/**
	 * Notifies all listeners of a change to the string values that are being 'watched'.
	 */
	public void notifyListeners();
}
