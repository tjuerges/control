package alma.control.gui.antennachessboard;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import alma.control.gui.antennachessboard.AntennaChessboardPresentationModel.ProblemListener;
import alma.acsnc.EventDescription;
import alma.common.gui.chessboard.ChessboardEntry;
import alma.common.gui.chessboard.ChessboardPlugin;
import alma.common.gui.chessboard.ChessboardDetailsPluginFactory;
import alma.common.gui.chessboard.ChessboardPopupMenu;
import alma.common.gui.chessboard.ChessboardStatusProvider;

/**
 * This is the plugin used to display a chessboard that is
 * used to launch the antenna user interface (for an 
 * antenna of the user's choosing).
 * 
 * @author Steve Harrington
 */
public class AntennaChessboardPlugin extends ChessboardPlugin 
{
	private final static String STANDALONE_LOGGER_NAME = "AntennaChessboardPresentationModelLogger";
	/**
	 * TODO - serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	private AntennaChessboardPresentationModel presentationModel;
	private Logger logger;

	/**
	 * No-args constructor required by Exec's plugin framework.
	 */
	public AntennaChessboardPlugin()
	{
                super(true); // multi-cell selection enabled (true)

		createPopupMenu();
		logger = (pluginContainerServices == null) ? 
				Logger.getLogger(STANDALONE_LOGGER_NAME) : 
				pluginContainerServices.getLogger();	
	}

	private void createPopupMenu() 
	{
		ChessboardPopupMenu popupMenu = new AntennaChessboardPopupMenu(this);
		this.setOptionalRightClickPopupMenu(popupMenu);
	}

	@Override
	protected ChessboardDetailsPluginFactory getDetailsProvider() 
	{
		detailsProvider = new AntennaChessboardDetailsPluginFactory();
		return detailsProvider;
	}

	@Override
	protected ChessboardStatusProvider getStatusProvider() 
	{
		presentationModel = new AntennaChessboardPresentationModel(pluginContainerServices);
		statusProvider = presentationModel;
		return statusProvider;
	}
	
	public void start() throws Exception
	{
		String controlChannelName = null;
		//this.pluginContainerServices.addNotificationChannelListener(controlChannelName, this);
		super.start();
	}
	
	public void stop() throws Exception
	{
		String controlChannelName = null;
		//this.pluginContainerServices.removeNotificationChannelListener(controlChannelName, this);
		super.stop();
	}
	
    /**
     * private class for a popup menu item which will reinitialize an antenna, when chosen by the user.
     */
    @SuppressWarnings("serial")
	private class ReinitializeAntennaPopupMenuItem extends JMenuItem
    {
    	private ChessboardPlugin owningChessboardPlugin;
    	private final static String POPUP_MENU_TEXT = "Reinitialize antenna";
    	
    	/**
    	 * Constructor
    	 * @param itemName the name of the menu item, which will be displayed on the popup menu to the user.
    	 * @param owningPlugin the plugin which created this popup menu item; used to get the selected entries in the chessboard.
    	 */
    	public ReinitializeAntennaPopupMenuItem(ChessboardPlugin owningPlugin)
    	{
    		super(POPUP_MENU_TEXT);
    		this.owningChessboardPlugin = owningPlugin;
    		this.addActionListener(new ActionListener() {
    			public void actionPerformed(ActionEvent evt) {
    				for(ChessboardEntry entry : owningChessboardPlugin.getSelectedEntries())
    				{
    					presentationModel.reinitializeAntenna(entry.getDisplayName(), 
    							new PresentationModelProblemListener(owningChessboardPlugin));
    				}
    			}
    		});
    	}
    }
    
    /**
     * private class for a popup menu item which will stow an antenna for maintenance, when chosen by the user.
     */
    @SuppressWarnings("serial")
	private class MaintenanceStowAntennaPopupMenuItem extends JMenuItem
    {
    	private ChessboardPlugin owningChessboardPlugin;
    	private final static String POPUP_MENU_TEXT = "Maintenance stow";
    	
    	/**
    	 * Constructor
    	 * @param itemName the name of the menu item, which will be displayed on the popup menu to the user.
    	 * @param owningPlugin the plugin which created this popup menu item; used to get the selected entries in the chessboard.
    	 */
    	public MaintenanceStowAntennaPopupMenuItem(ChessboardPlugin owningPlugin)
    	{
    		super(POPUP_MENU_TEXT);
    		this.owningChessboardPlugin = owningPlugin;
    		this.addActionListener(new ActionListener() {
    			public void actionPerformed(ActionEvent evt) {
    				for(ChessboardEntry entry : owningChessboardPlugin.getSelectedEntries())
    				{
    					presentationModel.maintenanceStowAntenna(entry.getDisplayName(), 
    							new PresentationModelProblemListener(owningChessboardPlugin));
    				}
    			}
    		});
    	}
    }
    
    /**
     * private class for a popup menu item which will stow an antenna in the survival position, when chosen by the user.
     */
    @SuppressWarnings("serial")
	private class SurvivalStowAntennaPopupMenuItem extends JMenuItem
    {
    	private ChessboardPlugin owningChessboardPlugin;
    	private final static String POPUP_MENU_TEXT = "Survival stow";
    	
    	/**
    	 * Constructor
    	 * @param itemName the name of the menu item, which will be displayed on the popup menu to the user.
    	 * @param owningPlugin the plugin which created this popup menu item; used to get the selected entries in the chessboard.
    	 */
    	public SurvivalStowAntennaPopupMenuItem(ChessboardPlugin owningPlugin)
    	{
    		super(POPUP_MENU_TEXT);
    		this.owningChessboardPlugin = owningPlugin;
    		this.addActionListener(new ActionListener() {
    			public void actionPerformed(ActionEvent evt) {
    				for(ChessboardEntry entry : owningChessboardPlugin.getSelectedEntries())
    				{
    					presentationModel.survivalStowAntenna(entry.getDisplayName(),
    							new PresentationModelProblemListener(owningChessboardPlugin));
    				}
    			}
    		});
    	}
    }

    /**
     * private class for a popup menu item which will stop an antenna, when chosen by the user.
     */
    @SuppressWarnings("serial")
	private class StopAntennaPopupMenuItem extends JMenuItem
    {
    	private ChessboardPlugin owningChessboardPlugin;
    	private final static String POPUP_MENU_TEXT = "Stop mount";
    	
    	/**
    	 * Constructor
    	 * @param itemName the name of the menu item, which will be displayed on the popup menu to the user.
    	 * @param owningPlugin the plugin which created this popup menu item; used to get the selected entries in the chessboard.
    	 */
    	public StopAntennaPopupMenuItem(ChessboardPlugin owningPlugin)
    	{
    		super(POPUP_MENU_TEXT);
    		this.owningChessboardPlugin = owningPlugin;
    		this.addActionListener(new ActionListener() {
    			public void actionPerformed(ActionEvent evt) {
    				for(ChessboardEntry entry : owningChessboardPlugin.getSelectedEntries())
    				{
    					presentationModel.stopMountForAntenna(entry.getDisplayName(),
    							new PresentationModelProblemListener(owningChessboardPlugin));
    				}
    			}
    		});
    	}
    }
    
    private class AntennaChessboardPopupMenu extends ChessboardPopupMenu
    {
		/**
		 * TODO - something more meaningful here?
		 */
		private static final long serialVersionUID = 1L;
		
		private JMenuItem maintenanceStowMenuItem, survivalStowMenuItem, stopMenuItem, reinitializeMenuItem;
		
		/**
		 * Constructor.
		 * @param owningPlugin the AntennaChessboardPlugin which 'owns' the menu.
		 */
		public AntennaChessboardPopupMenu(AntennaChessboardPlugin owningPlugin)
		{
			maintenanceStowMenuItem = new MaintenanceStowAntennaPopupMenuItem(owningPlugin);
			add(maintenanceStowMenuItem);
			
			survivalStowMenuItem = new SurvivalStowAntennaPopupMenuItem(owningPlugin);
			add(survivalStowMenuItem);
			
			stopMenuItem = new StopAntennaPopupMenuItem(owningPlugin);
			add(stopMenuItem);
			
			addSeparator();
			reinitializeMenuItem = new ReinitializeAntennaPopupMenuItem(owningPlugin);
			add(reinitializeMenuItem);
			addSeparator();	
		}
		
		@Override
		public void enableOrDisableChoicesForSelections(ChessboardEntry[] currentSelections) 
		{		
			if(null == currentSelections || !(currentSelections[0].getCurrentStatus() instanceof AntennaChessboardStatus))
			{
				return;
			} 
			else 
			{
				maintenanceStowMenuItem.setEnabled(true);
				survivalStowMenuItem.setEnabled(true);
				stopMenuItem.setEnabled(true);
				reinitializeMenuItem.setEnabled(true);
				
				for(ChessboardEntry entry: currentSelections) 
				{
					AntennaChessboardStatus status = null;
					if(entry.getCurrentStatus() instanceof AntennaChessboardStatus) {
						status = ((AntennaChessboardStatus)(entry.getCurrentStatus()));
					} else {
						break;
					}
					if((status.equals(AntennaChessboardStatus.ANTENNA_NOT_ACCESSIBLE)) 
							|| (status.equals(AntennaChessboardStatus.ANTENNA_NOT_INSTALLED))
							|| (status.equals(AntennaChessboardStatus.ANTENNA_SHUTDOWN))) 
					{
						maintenanceStowMenuItem.setEnabled(false);
						survivalStowMenuItem.setEnabled(false);
						stopMenuItem.setEnabled(false);
					} 
					else if(status.equals(AntennaChessboardStatus.ANTENNA_NOT_IN_ARRAY)) 
					{
						maintenanceStowMenuItem.setEnabled(false);
						survivalStowMenuItem.setEnabled(false);
						stopMenuItem.setEnabled(false);
						reinitializeMenuItem.setEnabled(false);
					}
				}
			}
		}	
    }
    
    private class PresentationModelProblemListener implements ProblemListener
    {
    	ChessboardPlugin owningPlugin;
    	
    	/**
    	 * Constructor
    	 * @param owningPlugin the owning plugin.
    	 */
    	public PresentationModelProblemListener(ChessboardPlugin owningPlugin)
    	{
    		this.owningPlugin = owningPlugin;
    	}
    	
    	public void notifyOfProblem(String message) 
		{
    		ExecuteOnSwingThread threadToShowError = new ExecuteOnSwingThread(message);
    		SwingUtilities.invokeLater(threadToShowError);
		}
    	
    	/**
    	 * Private class to execute updates on the GUI on a swing thread.
    	 * @author sharring
    	 */
    	private class ExecuteOnSwingThread extends Thread
    	{
    		private String message = null;
    		ExecuteOnSwingThread(String message)
    		{
    			this.message = message;
    		}
    		
    		public void run()
    		{
    			if(null != message) {
    				JOptionPane.showMessageDialog(owningPlugin, message,
    	    			    "Error", JOptionPane.ERROR_MESSAGE);
    			}
    		}
    	}
    }
}


