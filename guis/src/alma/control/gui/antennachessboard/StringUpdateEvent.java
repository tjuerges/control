package alma.control.gui.antennachessboard;

/**
 * Used by the observer pattern (StringUpdatePublisher, StringUpdateListener) to propagate change information to
 * interested parties.
 * 
 * @author Steve Harrington
 * @see StringUpdatePublisher
 * @see StringUpdateListener
 */
public class StringUpdateEvent 
{
	private String titleLine;
	private String line1;
	private String line2;
	
	/**
	 * Constructor.
	 * @param title title line text
	 * @param line1 first line text
	 * @param line2 second line text
	 */
	public StringUpdateEvent(String title, String line1, String line2)
	{
		this.titleLine = title;
		this.line1 = line1;
		this.line2 = line2;
	}
	
	/**
	 * Constructor.
	 * @param line1 first line text
	 * @param line2 second line text
	 */
	public StringUpdateEvent(String line1, String line2)
	{
		this(null, line1, line2);
	}
	
	/**
	 * Getter for the title line of text.
	 * @return the title line as a string.
	 */
	public String getTitleLine()
	{
		return titleLine;
	}
	
	/**
	 * Getter for the first line of text.
	 * @return the first line as a string.
	 */
	public String getLine1()
	{
		return line1;
	}

	/**
	 * Getter for the second line of text.
	 * @return the second line as a string.
	 */
	public String getLine2()
	{
		return line2;
	}
}
