package alma.control.gui.antennachessboard;

import java.util.HashMap;

import javax.swing.SwingWorker;

import alma.Control.Antenna;
import alma.Control.AntennaHelper;
import alma.Control.AntennaMonitor;
import alma.Control.AntennaMonitorHelper;
import alma.Control.AntennaStateEvent;
import alma.Control.ControlMaster;
import alma.Control.ControlMasterHelper;
import alma.Control.InaccessibleException;
import alma.Control.InvalidRequest;
import alma.Control.MountController;
import alma.Control.MountControllerHelper;
import alma.Control.Common.Name;
import alma.Control.gui.hardwaredevice.common.ControlChessboardPresentationModel;
import alma.ControlExceptions.INACTErrorEx;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.ModeControllerExceptions.BadConfigurationEx;
import alma.ModeControllerExceptions.CannotGetAntennaEx;
import alma.ModeControllerExceptions.MountFaultEx;
import alma.ModeControllerExceptions.UnallocatedEx;
import alma.acs.component.ComponentQueryDescriptor;
import alma.common.gui.chessboard.ChessboardStatus;
import alma.common.gui.chessboard.DefaultChessboardStatus;
import alma.common.log.ExcLog;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

/**
 * Presentation model for the antenna chessboard.
 * @author Steve Harrington
 */
public class AntennaChessboardPresentationModel extends
		ControlChessboardPresentationModel 
{
	private static final String MOUNT_CONTROLLER_IDL_STRING = "IDL:alma/Control/MountController:1.0";

	private static final String CONTROL_MASTER_CURL = Name.Master;
	
	private HashMap<String, Boolean> survivalStowInProgressMap = new HashMap<String, Boolean>();
	private HashMap<String, Boolean> maintenanceStowInProgressMap = new HashMap<String, Boolean>();
	private HashMap<String, Boolean> shutdownInProgressMap = new HashMap<String, Boolean>();
	private HashMap<String, Boolean> reinitializeInProgressMap = new HashMap<String, Boolean>();
	
	/**
	 * Constructor.
	 * @param services the container services to use for things like getting components, etc.
	 */
	public AntennaChessboardPresentationModel(PluginContainerServices services) 
	{
		super(services);
	}

	@Override
	protected AntennaChessboardStatus getStatusForCell(String antennaId) 
	{
		AntennaChessboardStatus retVal = AntennaChessboardStatus.ANTENNA_NOT_ACCESSIBLE;
		
		try {
			AntennaMonitor antennaMonitor = AntennaMonitorHelper.narrow(this.pluginContainerServices.getComponentNonSticky(Name.ControlPrefix + antennaId));
			if(null != antennaMonitor) {
				AntennaStateEvent state = antennaMonitor.getAntennaState();
				retVal = AntennaChessboardStatus.getInstanceForState(state);
			} else {
				retVal = AntennaChessboardStatus.ANTENNA_NOT_ACCESSIBLE;
			}
		} catch (AcsJContainerServicesEx e) {
			retVal = AntennaChessboardStatus.ANTENNA_NOT_ACCESSIBLE;
			logger.warning("AntennaChessboardPresentationModel could not get component for antenna: " + antennaId);
		} catch(org.omg.CORBA.BAD_PARAM e2) {
			retVal = AntennaChessboardStatus.ANTENNA_NOT_ACCESSIBLE;
    		logger.warning("AntennaChessboardPresentationModel could not narrow component for antenna: " + antennaId);
    	}
		
		return retVal;
	}
	
	 
    @Override
    protected ChessboardStatus getStatus(String antennaName)
	{
    	ChessboardStatus retVal = DefaultChessboardStatus.ANTENNA_NOT_INSTALLED;

		if(!isAntennaInstalled(antennaName)) {
			// antenna not installed
			retVal = DefaultChessboardStatus.ANTENNA_NOT_INSTALLED;
		} 
		else if(null != this.arrayName && !isAntennaInArray(antennaName)) {
			// special logic for Antenna chessboards:
			// if this chessboard was launched in the context of an array,
			// i.e. this.arrayName is not null, then deal only with antennas in that 
			// array; else this is a "global" chessboard dealing with ALL antennas.
			// this logic is unique to Antenna chessboards, which is why we are 
			// overriding the protected method getStatus from our superclass.
			retVal = DefaultChessboardStatus.ANTENNA_NOT_IN_ARRAY;
		}
		else {
			// otherwise get the state for the cell
			retVal = getStatusForCell(antennaName);
		}
		return retVal;	
	}
	
	void reinitializeAntenna(final String antennaId, final ProblemListener listener)
	{
		// if the user has already started a reinitialize, allow it to complete
		if(isReinitializeInProgress(antennaId)) {
			return;
		}
		
		SwingWorker worker = new SwingWorker<Void, Void>() 
		{
		    @Override
		    public Void doInBackground() 
		    {
		    	reinitializeStarted(antennaId);
		    	try 
		    	{
		    		// invoke reinitialize method of control's master component
		    		ControlMaster master = ControlMasterHelper.narrow(pluginContainerServices.getComponentNonSticky(CONTROL_MASTER_CURL));
		    		try {
			    		if(null != master) {
			    			master.reinitializeAntenna(antennaId);	    			
			    		} else {
			    			logger.warning("AntennaChessboardPresentationModel could not reinitialize; narrow failed for antenna: " + antennaId);
			    		}
		    		} catch(InaccessibleException e1) {
		    			logger.warning("AntennaChessboardPresentationModel could not reinitialize; unable to access antenna: " + antennaId + ExcLog.details(e1));
		    			listener.notifyOfProblem("Reinitialize of antenna: " + antennaId + " failed; antenna not accessible");
		    		} catch(InvalidRequest e2) {
		    			logger.warning("AntennaChessboardPresentationModel could not reinitialize; got invalid request exception for antenna: " + antennaId + ExcLog.details(e2));
		    			listener.notifyOfProblem("Reinitialize of antenna: " + antennaId + " failed; invalid request");
		    		} 
		    	} 
		    	catch(AcsJContainerServicesEx ex) {
		    		logger.warning("AntennaChessboardPresentationModel could not reinitialize; got container services exception for antenna: " + antennaId + ExcLog.details(ex));
		    		listener.notifyOfProblem("Reinitialize of antenna: " + antennaId + " failed; container services exception");
		    	}
		    	catch(org.omg.CORBA.BAD_PARAM ex2) {
		    		logger.warning("AntennaChessboardPresentationModel could not reinitialize; exception narrowing component object for antenna: " + antennaId + ExcLog.details(ex2));
		    		listener.notifyOfProblem("Reinitialize of antenna: " + antennaId + " failed; CORBA problem");
		    	} finally {
			    	reinitializeDone(antennaId);
		    	}
		    	return null;
		    }
		};
		worker.execute();
	}
	
	void stopMountForAntenna(final String antennaId, final ProblemListener listener) 
	{
		// if the user has already started a shutdown, allow it to complete
		if(isShutdownInProgress(antennaId)) {
			return;
		}
		
		SwingWorker worker = new SwingWorker<Void, Void>() 
		{
		    @Override
		    public Void doInBackground() 
		    {
		    	shutdownStarted(antennaId);
		    	String mountControllerCURL = null;
		    	MountController mount = null;
				// invoke shutdown method on mount controller	
		    	try 
		    	{
		    		Antenna antenna = AntennaHelper.narrow(pluginContainerServices.getComponentNonSticky(Name.ControlPrefix + antennaId));
		    		mountControllerCURL = null;
		    		try {
		    			mountControllerCURL = antenna.getMountControllerName();
		    		} catch(INACTErrorEx ex) {
		    			logger.warning("AntennaChessboardPresentationModel stop failed; could not get mount for antenna: " + antennaId);
		    			listener.notifyOfProblem("Stop mount of antenna: " + antennaId + " failed; cannot get mount");
		    			mountControllerCURL = null;
		    		}
		    		if(mountControllerCURL != null) 
		    		{
		    			ComponentQueryDescriptor compDescriptor = new ComponentQueryDescriptor(mountControllerCURL, MOUNT_CONTROLLER_IDL_STRING);
		    			mount = MountControllerHelper.narrow(pluginContainerServices.getCollocatedComponent(compDescriptor, false, Name.ControlPrefix + antennaId));
		    			try {
							mount.allocate(antennaId);
						} catch (CannotGetAntennaEx e) {
							logger.warning("AntennaChessboardPresentationModel stop mount failed; could not get antenna to allocate mount for antenna: " + antennaId + ExcLog.details(e));
				    		listener.notifyOfProblem("stop mount of antenna: " + antennaId + " failed; could not get antenna to allocate mount");
				    		mount = null;
						} catch (BadConfigurationEx e) {
							logger.warning("AntennaChessboardPresentationModel stop mount failed; bad configuration for antenna: " + antennaId + ExcLog.details(e));
				    		listener.notifyOfProblem("stop mount of antenna: " + antennaId + " failed; bad configuration for antenna");
				    		mount = null;
						}
		    			if(mount != null) {
		    				mount.stop();
		    			} 
		    		}
		    	} catch (UnallocatedEx e) {
		    		logger.warning("AntennaChessboardPresentationModel could not stop mount; UnallocatedEx exception: " + antennaId + ExcLog.details(e));
		    		listener.notifyOfProblem("Stop mount of antenna: " + antennaId + " failed; unallocated");
				} catch (MountFaultEx e) {
					logger.warning("AntennaChessboardPresentationModel could not stop mount; MountFaultEx exception: " + antennaId + ExcLog.details(e));
		    		listener.notifyOfProblem("Stop mount of antenna: " + antennaId + " failed; mount exception");
		    	} catch(AcsJContainerServicesEx ex4) {
		    		logger.warning("AntennaChessboardPresentationModel could not stop mount; container services exception for antenna: " + antennaId + ExcLog.details(ex4));
		    		listener.notifyOfProblem("Stop mount of antenna: " + antennaId + " failed; problem communicating with container");
		    	} catch(org.omg.CORBA.BAD_PARAM ex5) {
		    		logger.warning("AntennaChessboardPresentationModel could not stop mount; exception narrowing component for antenna: " + antennaId + ExcLog.details(ex5));
		    		listener.notifyOfProblem("Stop mount of antenna: " + antennaId + " failed; CORBA problem");
		    	} 
				finally {
			    	shutdownDone(antennaId);
			    	if(null != mountControllerCURL && null != mount) {
			    		pluginContainerServices.releaseComponent(mountControllerCURL);
			    	}
		    	}
		    	return null;
		    }
		};
		worker.execute();
	}
	
	void survivalStowAntenna(final String antennaId, final ProblemListener listener)
	{
		// if the user has already started a survival stow, allow it to complete
		if(isSurvivalStowInProgress(antennaId)) {
			return;
		}
		
		SwingWorker worker = new SwingWorker<Void, Void>() 
		{
		    @Override
		    public Void doInBackground() {
		    	survivalStowStarted(antennaId);
		    	String mountControllerCURL = null;
		    	MountController mount = null;
				// invoke survival stow method on mount controller
		    	try 
		    	{
		    		Antenna antenna = AntennaHelper.narrow(pluginContainerServices.getComponentNonSticky(Name.ControlPrefix + antennaId));
		    		mountControllerCURL = null;
		    		try {
		    			mountControllerCURL = antenna.getMountControllerName();
		    		} catch(INACTErrorEx ex) {
		    			logger.warning("AntennaChessboardPresentationModel survival stow failed; could not get mount for antenna: " + antennaId);
		    			listener.notifyOfProblem("survival stow of antenna: " + antennaId + " failed; cannot get mount");
		    			mountControllerCURL = null;
		    		}
		    		if(mountControllerCURL != null) 
		    		{
		    			ComponentQueryDescriptor compDescriptor = new ComponentQueryDescriptor(mountControllerCURL, MOUNT_CONTROLLER_IDL_STRING);
		    			mount = MountControllerHelper.narrow(pluginContainerServices.getCollocatedComponent(compDescriptor, false, Name.ControlPrefix + antennaId));
		    			try {
							mount.allocate(antennaId);
						} catch (CannotGetAntennaEx e) {
							logger.warning("AntennaChessboardPresentationModel survival stow failed; could not get antenna to allocate mount for antenna: " + antennaId + ExcLog.details(e));
				    		listener.notifyOfProblem("Survival stow of antenna: " + antennaId + " failed; could not get antenna to allocate mount");
				    		mount = null;
						} catch (BadConfigurationEx e) {
							logger.warning("AntennaChessboardPresentationModel survival stow failed; bad configuration for antenna: " + antennaId + ExcLog.details(e));
				    		listener.notifyOfProblem("Survival stow of antenna: " + antennaId + " failed; bad configuration for antenna");
				    		mount = null;
						}
		    			if(mount != null) {
		    				mount.survivalStow();
		    			} 
		    		}
		    	} catch (UnallocatedEx e) {
		    		logger.warning("AntennaChessboardPresentationModel could not survival stow; unallocated exception: " + antennaId + ExcLog.details(e));
		    		listener.notifyOfProblem("Survival stow of antenna: " + antennaId + " failed; unallocated");
				} catch (MountFaultEx e) {
					logger.warning("AntennaChessboardPresentationModel could not survival stow; mount fault: " + antennaId + ExcLog.details(e));
		    		listener.notifyOfProblem("Survival stow of antenna: " + antennaId + " failed; mount fault");
		    	} catch(AcsJContainerServicesEx ex4) {
		    		logger.warning("AntennaChessboardPresentationModel could not survival stow; container services exception for antenna: " + antennaId + ExcLog.details(ex4));
		    		listener.notifyOfProblem("Survival stow of antenna: " + antennaId + " failed; problem communicating with container");
		    	} catch(org.omg.CORBA.BAD_PARAM ex5) {
		    		logger.warning("AntennaChessboardPresentationModel could not survival stow; exception narrowing component for antenna: " + antennaId + ExcLog.details(ex5));
		    		listener.notifyOfProblem("Survival stow of antenna: " + antennaId + " failed; CORBA error");
		    	} 
				finally {
			    	survivalStowDone(antennaId);
			    	if(null != mountControllerCURL && null != mount) {
			    		pluginContainerServices.releaseComponent(mountControllerCURL);
			    	}
		    	} 
		    	return null;
		    }
		};
		worker.execute();
	}
	
	void maintenanceStowAntenna(final String antennaId, final ProblemListener listener)
	{
		// if the user has already started a maintenance stow, allow it to complete
		if(isMaintenanceStowInProgress(antennaId)) {
			return;
		}
		
		SwingWorker worker = new SwingWorker<Void, Void>() 
		{
		    @Override
		    public Void doInBackground() {
		    	maintenanceStowStarted(antennaId);
				// invoke maintenance stow method on mount controller
		    	String mountControllerCURL = null;
		    	MountController mount = null;
		    	try 
		    	{
		    		Antenna antenna = AntennaHelper.narrow(pluginContainerServices.getComponentNonSticky(Name.ControlPrefix + antennaId));
		    		try {
		    			mountControllerCURL = antenna.getMountControllerName();
		    		} catch(INACTErrorEx ex) {
		    			logger.warning("AntennaChessboardPresentationModel maintenance stow failed; could not get mount for antenna: " + antennaId);
		    			listener.notifyOfProblem("maintenance stow of antenna: " + antennaId + " failed; cannot get mount");
		    			mountControllerCURL = null;
		    		}
		    		if(mountControllerCURL != null) 
		    		{
		    			ComponentQueryDescriptor compDescriptor = new ComponentQueryDescriptor(mountControllerCURL, MOUNT_CONTROLLER_IDL_STRING);
		    			mount = MountControllerHelper.narrow(pluginContainerServices.getCollocatedComponent(compDescriptor, false, Name.ControlPrefix + antennaId));
		    			try {
							mount.allocate(antennaId);
						} catch (CannotGetAntennaEx e) {
							logger.warning("AntennaChessboardPresentationModel maintenance stow failed; could not get anntena to allocate mount for antenna: " + antennaId + ExcLog.details(e));
				    		listener.notifyOfProblem("Maintenance stow of antenna: " + antennaId + " failed; could not get antenna to allocate mount");
				    		mount = null;
						} catch (BadConfigurationEx e) {
							logger.warning("AntennaChessboardPresentationModel maintenance stow failed; bad configuration for antenna: " + antennaId + ExcLog.details(e));
				    		listener.notifyOfProblem("Maintenance stow of antenna: " + antennaId + " failed; bad configuration for antenna");
				    		mount = null;
						}
		    			if(mount != null) {
		    				mount.maintenanceStow();
		    			} 
		    		}
		    	} catch (UnallocatedEx e) {
		    		logger.warning("AntennaChessboardPresentationModel could not maintenance stow; unallocated exception: " + antennaId + ExcLog.details(e));
		    		listener.notifyOfProblem("maintenance stow of antenna: " + antennaId + " failed; unallocated");
				} catch (MountFaultEx e) {
					logger.warning("AntennaChessboardPresentationModel could not maintenance stow; mount fault: " + antennaId + ExcLog.details(e));
		    		listener.notifyOfProblem("maintenance stow of antenna: " + antennaId + " failed; mount fault");
		    	} 
				catch(AcsJContainerServicesEx ex4) {
		    		logger.warning("AntennaChessboardPresentationModel could not maintenance stow; container services exception for antenna: " + antennaId + ExcLog.details(ex4));
		    		listener.notifyOfProblem("maintenance stow of antenna: " + antennaId + " failed; problem communicating with container");
		    	} catch(org.omg.CORBA.BAD_PARAM ex5) {
		    		logger.warning("AntennaChessboardPresentationModel could not maintenance stow; exception narrowing component for antenna: " + antennaId + ExcLog.details(ex5));
		    		listener.notifyOfProblem("maintenance stow of antenna: " + antennaId + " failed; CORBA problem");
		    	} finally {
			    	maintenanceStowDone(antennaId);
			    	if(null != mountControllerCURL && null != mount) {
			    		pluginContainerServices.releaseComponent(mountControllerCURL);
			    	}
		    	}
		    	return null;
		    }
		};
		worker.execute();
	}
	
	private synchronized boolean isReinitializeInProgress(String antennaId) 
	{
		boolean retVal = false;
		Boolean valueInMap = reinitializeInProgressMap.get(antennaId);
		if(null != valueInMap) {
			retVal = valueInMap;
		}
		return retVal;
	}
	
	private synchronized void reinitializeStarted(String antennaId) 
	{
		reinitializeInProgressMap.put(antennaId, true);
	}
	
	private synchronized void reinitializeDone(String antennaId) 
	{
		reinitializeInProgressMap.remove(antennaId);
	}
	
	private synchronized boolean isSurvivalStowInProgress(String antennaId) 
	{
		boolean retVal = false;
		Boolean valueInMap = survivalStowInProgressMap.get(antennaId);
		if(null != valueInMap) {
			retVal = valueInMap;
		}
		return retVal;
	}
	
	private synchronized void survivalStowStarted(String antennaId) 
	{
		survivalStowInProgressMap.put(antennaId, true);
	}
	
	private synchronized void survivalStowDone(String antennaId) 
	{
		survivalStowInProgressMap.remove(antennaId);
	}
	
	private synchronized boolean isMaintenanceStowInProgress(String antennaId) 
	{
		boolean retVal = false;
		Boolean valueInMap = maintenanceStowInProgressMap.get(antennaId);
		if(null != valueInMap) {
			retVal = valueInMap;
		}
		return retVal;
	}
	
	private synchronized void maintenanceStowStarted(String antennaId) 
	{
		maintenanceStowInProgressMap.put(antennaId, true);
	}
	
	private synchronized void maintenanceStowDone(String antennaId) 
	{
		maintenanceStowInProgressMap.remove(antennaId);
	}
	
	private synchronized boolean isShutdownInProgress(String antennaId) 
	{
		boolean retVal = false;
		Boolean valueInMap = shutdownInProgressMap.get(antennaId);
		if(null != valueInMap) {
			retVal = valueInMap;
		}
		return retVal;
	}
	
	private synchronized void shutdownStarted(String antennaId) 
	{
		shutdownInProgressMap.put(antennaId, true);
	}
	
	private synchronized void shutdownDone(String antennaId) 
	{
		shutdownInProgressMap.remove(antennaId);
	}
	
	interface ProblemListener 
	{
		/**
		 * Used to notify the GUI that a problem occurred, so that a popup window or some other notification
		 * can be displayed to the user to let them know of the situation.
		 * @param message the error message text to display to the user.
		 */
		public void notifyOfProblem(String message);
	}
}


