package alma.control.gui.antennachessboard;

import java.awt.Color;

import alma.Control.AntennaState;
import alma.Control.AntennaStateEvent;
import alma.common.gui.chessboard.ChessboardStatus;

/**
 * Class used to denote the status of a cell in an antenna chessboard.
 * @author Steve Harrington
 */
public class AntennaChessboardStatus implements ChessboardStatus 
{
	private static final String ANTENNA_NOT_INSTALLED_STRING = "ANTENNA_NOT_INSTALLED";
	private static final String ANTENNA_NOT_IN_ARRAY_STRING = "ANTENNA_NOT_IN_ARRAY";
	private static final String ANTENNA_DEGRADED_STRING = "DEGRADED";
	private static final String ANTENNA_NOT_ACCESSIBLE_STRING = "NOT ACCESSIBLE";
	private static final String ANTENNA_SHUTDOWN_STRING = "SHUTDOWN";
	private static final String ANTENNA_OPERATIONAL_STRING = "OPERATIONAL";
	
	// not currently used 
	private static final String INFO_STRING = "INFO";
	private static final String WARNING_STRING = "WARNING";
	private static final String FATAL_ERROR_STRING = "FATAL_ERROR";

	private String description;
	private Color bgColor;
	private Color fgColor;
	private boolean colorFlashes;
	private boolean isSelectable = true;
	private static ChessboardStatus[] values = null;

	/**
	 * Private constructor enforces typesafe enum pattern and
	 * makes it impossible for status values other than those 
	 * allowed (i.e. the predefined public final static instances above)
	 * to be created.
	 * 
	 * @param name the name of the status
	 */
	private AntennaChessboardStatus(String name)
	{
		this.isSelectable = true;
		this.colorFlashes = false;
		this.description = name;

		// configure the status with proper color and/or flashing attributes
		 if(description.equals(ANTENNA_NOT_INSTALLED_STRING)) {
			// use the default bg/fg colors
			this.bgColor = null;
			this.fgColor = null;
			this.isSelectable = false;
		} else if(description.equals(ANTENNA_NOT_IN_ARRAY_STRING)) {
			this.bgColor = Color.LIGHT_GRAY;
			this.fgColor = Color.DARK_GRAY;
			this.isSelectable = false;
		} else if(description.equals(ANTENNA_DEGRADED_STRING)) {
			this.bgColor = Color.YELLOW;
			this.fgColor = Color.BLACK;
		} else if(description.equals(ANTENNA_NOT_ACCESSIBLE_STRING)) {
			this.bgColor = Color.RED;
			this.fgColor = Color.WHITE;
		} else if(name.equals(ANTENNA_SHUTDOWN_STRING)) {
			this.bgColor = Color.LIGHT_GRAY;
			this.fgColor = Color.DARK_GRAY;
		} else if(description.equals(ANTENNA_OPERATIONAL_STRING)) {
			this.bgColor = Color.GREEN;
			this.fgColor = Color.BLACK;
		} else if(name.equals(FATAL_ERROR_STRING)) {
				this.colorFlashes = true;
				this.bgColor = Color.RED;
				this.fgColor = Color.WHITE;
		} else if(description.equals(WARNING_STRING)) {
			this.bgColor = Color.YELLOW;
			this.fgColor = Color.BLACK;
		} else if(name.equals(INFO_STRING)) {
			this.bgColor = Color.WHITE;
			this.fgColor = Color.BLACK;
		}
	}

	// Create the fixed set of allowed instances (these will be the enum values).
	/**
	 * Status indicating a fatal error - this is more severe than 'severe' error status.
	 */
	public static final AntennaChessboardStatus FATAL_ERROR = 
		new AntennaChessboardStatus(FATAL_ERROR_STRING); 

	/**
	 * Status indicating an error - less severe than 'severe' and 'fatal' errors.
	 */
	public static final AntennaChessboardStatus WARNING = 
		new AntennaChessboardStatus(WARNING_STRING);

	/**
	 * Status indicating some information is available.
	 */
	public static final AntennaChessboardStatus INFO = 
		new AntennaChessboardStatus(INFO_STRING);

	/**
	 * Status indicating that the device of interest is not present in the antenna.
	 */
	public static final AntennaChessboardStatus ANTENNA_DEGRADED = 
		new AntennaChessboardStatus(ANTENNA_DEGRADED_STRING);

	/**
	 * Status indicating the antenna is offline.
	 */
	public static final AntennaChessboardStatus ANTENNA_SHUTDOWN = 
		new AntennaChessboardStatus(ANTENNA_SHUTDOWN_STRING);

	/**
	 * Status indicating normal operational status.
	 */
	public static final AntennaChessboardStatus ANTENNA_OPERATIONAL = 
		new AntennaChessboardStatus(ANTENNA_OPERATIONAL_STRING);
	
	/**
	 * Status indicating that the antenna is not installed / not in the system.
	 */
	public static final AntennaChessboardStatus ANTENNA_NOT_INSTALLED = 
		new AntennaChessboardStatus(ANTENNA_NOT_INSTALLED_STRING);

	/**
	 * Status indicating that the antenna is installed, but not present in the selected array.
	 */
	public static final AntennaChessboardStatus ANTENNA_NOT_IN_ARRAY = 
		new AntennaChessboardStatus(ANTENNA_NOT_IN_ARRAY_STRING);

	/**
	 * Status indicating that the antenna is not accessible.
	 */
	public static final AntennaChessboardStatus ANTENNA_NOT_ACCESSIBLE = 
		new AntennaChessboardStatus(ANTENNA_NOT_ACCESSIBLE_STRING);

	/**
	 * Static method used to get the instance corresponding to the antenna state passed in.
	 * @param state the state of the antenna for which we want the corresponding AntennaChessboardStatus instance.
	 * @return the AntennaChessboardStatus instance corresponding to the passed in AntennaState.
	 */
	public static AntennaChessboardStatus getInstanceForState(AntennaStateEvent state)
	{
		AntennaChessboardStatus retVal = AntennaChessboardStatus.ANTENNA_NOT_INSTALLED;
		switch(state.newState.value()) 
		{
		case AntennaState._AntennaDegraded:
			retVal = AntennaChessboardStatus.ANTENNA_DEGRADED;
			break;
		case AntennaState._AntennaInaccessable:
			retVal = AntennaChessboardStatus.ANTENNA_NOT_ACCESSIBLE;
			break;
		case AntennaState._AntennaOperational:
			retVal = AntennaChessboardStatus.ANTENNA_OPERATIONAL;
			break;
		case AntennaState._AntennaShutdown:
			retVal = AntennaChessboardStatus.ANTENNA_SHUTDOWN;
			break;
		}
		return retVal;
	}

	/** 
	 * Required method of <code>ChessboardStatus</code> interface.
	 * 
	 * @return the background color that should be used to render the status in the user interface.
	 */
	public Color getBgColor()
	{
		return this.bgColor;
	}

	/** 
	 * Required method of <code>ChessboardStatus</code> interface.
	 * 
	 * @return the foreground color that should be used to render the background color for the status in the user interface.
	 */
	public Color getFgColor()
	{
		return this.fgColor;
	}

	/**
	 * Overriding generic toString method from <code>Object</code> to get
	 * something more user-friendly.
	 * 
	 * @return a string representation of this status which is the same as the status name.
	 */
	@Override
	public String toString()
	{
		return this.description;
	}

	/** 
	 * Required method of <code>ChessboardStatus</code> interface.
	 * 
	 * @return the description of the status.
	 */
	public String getDescription()
	{
		return this.description;
	}

	/**
	 * Required method of the <code>ChessboardStatus</code> interface.
	 * 
	 * @return boolean indicating whether this status should be rendered as flashing in the
	 * user interface (visual representation).
	 */
	public boolean shouldFlash() 
	{
		return this.colorFlashes;
	}

	/**
	 * Required method of the <code>ChessboardStatus</code> interface.
	 * 
	 * @return boolean indicating whether this status should be selectable in the UI.
	 */
	public boolean isSelectable()
	{
		return this.isSelectable;
	}

	/** 
	 * Required method of the <code>ChessboardStatus</code> interface. Returns
	 * all of the valid instances of this enum class. This is primarily used
	 * for testing at the present, could be useful in some other situations potentially.
	 * Method is synchronized because it employs a singleton
	 * for the values array. For an example of its use in a test setting, 
	 * see the <code>ChessboardTest</code> and <code>ExampleChessboardStatusProvider</code> classes.
	 * 
	 * @return the full set of statuses for this enum.
	 */
	public synchronized ChessboardStatus[] values() 
	{
		// use a singleton-style mechanism to return the values
		if(AntennaChessboardStatus.values == null)
		{
			AntennaChessboardStatus.values = new ChessboardStatus[8];

			AntennaChessboardStatus.values[0] = AntennaChessboardStatus.INFO;
			AntennaChessboardStatus.values[1] = AntennaChessboardStatus.WARNING;
			AntennaChessboardStatus.values[2] = AntennaChessboardStatus.ANTENNA_SHUTDOWN;
			AntennaChessboardStatus.values[3] = AntennaChessboardStatus.FATAL_ERROR;
			AntennaChessboardStatus.values[4] = AntennaChessboardStatus.ANTENNA_OPERATIONAL;
			AntennaChessboardStatus.values[5] = AntennaChessboardStatus.ANTENNA_NOT_INSTALLED;
			AntennaChessboardStatus.values[6] = AntennaChessboardStatus.ANTENNA_NOT_IN_ARRAY;
			AntennaChessboardStatus.values[7] = AntennaChessboardStatus.ANTENNA_DEGRADED;
		}

		return AntennaChessboardStatus.values;
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		boolean retVal = false;
		if(!(obj instanceof AntennaChessboardStatus)) {
			retVal = false;
		} else {
			AntennaChessboardStatus antStatus = (AntennaChessboardStatus)obj;
			if((antStatus.bgColor == this.bgColor) && (antStatus.isSelectable == this.isSelectable)
					&& (antStatus.colorFlashes == this.colorFlashes) && (antStatus.description.equals(this.description)
					&& (antStatus.fgColor == this.fgColor) ))
			{
				retVal = true;
			}
		}
		return retVal;
	}
	 
}


