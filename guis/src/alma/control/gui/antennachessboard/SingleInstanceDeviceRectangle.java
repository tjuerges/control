package alma.control.gui.antennachessboard;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.ToolTipManager;

import alma.common.gui.chessboard.ChessboardDetailsPlugin;
import alma.common.gui.chessboard.ChessboardDetailsPluginListener;
import alma.common.gui.chessboard.ChessboardStatusEvent;
import alma.common.gui.chessboard.ChessboardStatusListener;
import alma.exec.extension.subsystemplugin.PluginContainerException;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

/**
 * Display a rectangle with one, two, or three text labels (which may change) 
 * and a status with color coding (which may also change) that show key properties
 * of a hardware device in an antenna.  Allow the user to launch a Device GUI for a 
 * detailed view of the associated device.  
 * 
 * This class supports hardware devices that are unique in an antenna.
 * 
 * @author Steve Harrington sharring@nrao.edu
 * @author Scott Rankin     srankin@nrao.edu
 */
@SuppressWarnings("serial")
public class SingleInstanceDeviceRectangle extends Rectangle implements MouseListener, StringUpdateListener, ChessboardStatusListener
{
	private static final int DEFAULT_TITLE_FONT_SIZE = 14;
	private static final int DEFAULT_DATA_FONT_SIZE = 12;
	private static final int STD_PADDING = 5;
	
	private String firstLineTextTitle = "";
	private String firstLineTextValue = "";
	private String secondLineTextTitle = "";
	private String secondLineTextValue = "";
	private String thirdLineTextTitle = "";
	private String thirdLineTextValue = "";
	private String textOutsideBoundsOfRectangleTop = null;
	
	private boolean mouseInside = false;
	private List<SingleInstanceDeviceRectangle> nestedRectangles = new ArrayList<SingleInstanceDeviceRectangle>();
	private int transformAngleForLabelText = 0;
	private boolean allLinesBold = false;
	private boolean allLinesSameFontSize = false;
	private boolean firstLineTextCentered = true;
	private boolean secondAndThirdLineTextCentered = false;
	private Graphics2D graphics2d;
	private JComponent owningComp;
	
	private Color borderColor = Color.BLACK;
	private Color colorForTextOutsideRectangle = Color.BLACK;
	private DeviceStatus status = null;
	private String tooltipText = DeviceStatus.NOT_INSTALLED.getDescription();
	protected Class<? extends ChessboardDetailsPlugin> devicePluginClass;
	protected PluginContainerServices containerServices;
	protected String[] antennaNames;
	protected String deviceNameText;
	
	/**
	 * Constructor.
	 * @param initialState the initial state of the rectangle.
	 */
	public SingleInstanceDeviceRectangle(DeviceStatus initialState, PluginContainerServices containerServices)
	{
		this(initialState, containerServices, null, null, null);
	}
	
	/**
	 * Constructor.
	 * @param initialState the initial state of the rectangle.
	 * @param services the container services used to launch child plugins.
	 * @param detailsPluginClass the class, if any, which we will instantiate to 
	 *        launch a plugin upon double click of the rectangle.
	 * @param antennaNames the names of the antenna(s) for this rectangle; 
	 *        used when launching device plugins from a rectangle upon double click.
	 * @param deviceText the text to use for title bar of device plugin
	 *        that is launched from double click.
	 */
	public SingleInstanceDeviceRectangle(DeviceStatus initialState, PluginContainerServices services, 
			Class<? extends ChessboardDetailsPlugin> detailsPluginClass, String[] antennaNames, String deviceText)
	{
		this.status = initialState;
		this.containerServices = services;
		this.devicePluginClass = detailsPluginClass;
		this.antennaNames = antennaNames;
		this.deviceNameText = deviceText;
	}
	    
	/**
	 * Setter for the border color.
	 * @param color
	 */
	public void setBorderColor(Color color)
	{
		this.borderColor = color;
	}
	
	/**
	 * Setter for whether all lines of text should be rendered in a bold font.
	 * @param bold true indicates all lines should be rendered bold; false indicates that only first line is bold.
	 */
	public void setAllLinesBold(boolean bold)
	{
		this.allLinesBold = bold;
	}
	
	/**
	 * Setter for whether all lines of text should be rendered in the same font size.
	 * @param sameSize true indicates all lines should be rendered in the same font size; 
	 * false indicates that first line is larger.
	 */
	public void setAllLinesSameFontSize(boolean sameSize)
	{
		this.allLinesSameFontSize = sameSize;
	}
	
	/**
	 * Setter for whether the label text should be centered or not.
	 * @param center
	 */
	public void setFirstLineTextCentered(boolean center)
	{
		this.firstLineTextCentered = center;
	}
	
	/**
	 * Setter for whether the second and third lines of text should be centered or not.
	 * @param center
	 */
	public void setSecondAndThirdLineTextCentered(boolean center)
	{
		this.secondAndThirdLineTextCentered = center;
	}

	/**
	 * Setter for text that will be drawn outside the bounds of the rectangle, above it.
	 * @param text the string to draw outside the bounds of the rectangle (above).
	 */
	public void setTextOutsideBoundsOfRectangleTop(String text)
	{
		this.textOutsideBoundsOfRectangleTop = text;
	}
	
	/**
	 * Setter for color of text that will be drawn outside the bounds of the rectangle, above it.
	 * @param color the color to use for the text that is drawn outside the bounds of the rectangle (above).
	 */
	public void setColorForTextOutsideBoundsOfRectangleTop(Color color)
	{
		this.colorForTextOutsideRectangle = color;
	}
		
	/**
	 * Setter for the first line text title;
	 * @param text the title string with which to label the rectangle.
	 */
	public void setFirstLineTextTitle(String text)
	{
		this.firstLineTextTitle = text;
	}
	
	/**
	 * Setter for the first line text value;
	 * @param text the value string, if any, for the first line of text in the rectangle.
	 */
	public void setFirstLineTextValue(String text)
	{
		this.firstLineTextValue = text;
	}
	
	/**
	 * Setter for the 2nd line text title;
	 * @param text the title string with which to label the 2nd line of text for the rectangle.
	 */
	public void setSecondLineTextTitle(String text)
	{
		this.secondLineTextTitle = text;
	}
	
	/**
	 * Setter for the 2nd line text value;
	 * @param text the value string, if any, for the 2nd line of text in the rectangle.
	 */
	public void setSecondLineTextValue(String text)
	{
		this.secondLineTextValue = text;
	}
	
	/**
	 * Setter for the 3rd line text title;
	 * @param text the title string with which to label the 3rd line of text for the rectangle.
	 */
	public void setThirdLineTextTitle(String text)
	{
		this.thirdLineTextTitle = text;
	}
	
	/**
	 * Setter for the third line text value;
	 * @param text the value string, if any, for the 3rd line of text in the rectangle.
	 */
	public void setThirdLineTextValue(String text)
	{
		this.thirdLineTextValue = text;
	}
	
	/**
	 * Setter for the transform angle (used to rotate the label text).
	 * @param angle the angle to rotate the label text.
	 */
	public void setTransformAngleForLabelText(int angle)
	{
		this.transformAngleForLabelText = angle;
	}
	
	/**
	 * draws the rectangle using the supplied graphics object.
	 * @param graphics the graphics object with which to draw the shape.
	 * @param owningComp the component which 'owns' the labeled rectangle; used for 
	 * repaints to update the screen properly when things are being redrawn due to changes.
	 */
	public void draw(Graphics2D graphics, JComponent owningComp)
	{
		cloneGraphics(graphics);
		this.owningComp = owningComp;
		
		this.graphics2d.setPaint(getBackground());
		this.graphics2d.fill(this);	
		if(this.status.hasBorder())
		{
			this.graphics2d.setPaint(this.borderColor);
			this.graphics2d.draw(this);
			this.graphics2d.setPaint(getBackground());
		}
		
		if(0 == transformAngleForLabelText)
		{
			drawTextHorizontal(this.graphics2d);
		}
		else 
		{
			drawTextTransformed(this.graphics2d);	
		}
	
		for(SingleInstanceDeviceRectangle rect : nestedRectangles)
		{
			rect.draw(this.graphics2d, owningComp);
		}
	}
	
    /**
     * Get the bottom center point of this rectangle. Used for drawing lines to and from it.
     * @return a Point2D.Double located at the bottom center of this rectangle.
     */
    public Point2D.Double getBottomCenter(){
        return new Point2D.Double (this.getCenterX(), this.getMaxY());
    }
    
    /**
     * Get the left center point of this rectangle. Used for drawing lines to and from it.
     * @return a Point2D.Double located at the left center of this rectangle.
     */
    public Point2D.Double getLeftCenter(){
        return new Point2D.Double (this.getMinX(), this.getCenterY());
    }
    
    /**
     * Getter for the nested rectangles contained within this rectangle.
     * @return the nested rectangles as a list.
     */
    public List<SingleInstanceDeviceRectangle> getNestedRectangles()
    {
        return this.nestedRectangles;
    }

    /**
     * Get the right center point of this rectangle. Used for drawing lines to and from it.
     * @return a Point2D.Double located at the right center of this rectangle.
     */
    public Point2D.Double getRightCenter(){
        return new Point2D.Double (this.getMaxX(), this.getCenterY());
    }

    /**
     * Get the top center point of this rectangle. Used for drawing lines to and from it.
     * @return a Point2D.Double located at the top center of this rectangle.
     */
    public Point2D.Double getTopCenter(){
        return new Point2D.Double (this.getCenterX(), this.getMinY());
    }
	
	/**
	 * Adds an "inner" rectangle to the (outer) rectangle.
	 * @param rect the inner rectangle to add.
	 */
	public void addNestedRectangle(SingleInstanceDeviceRectangle rect)
	{
		final int CUSHION = 5;
		
		nestedRectangles.add(rect);
		
		// tweak size of outer rectangle, if necessary
		this.width = rect.width > this.width ? rect.width * 2 : this.width;
		this.height = rect.height > this.height ? rect.height * 2 : this.height;
		
		rect.y += DEFAULT_TITLE_FONT_SIZE;
		
		this.width = (rect.x + rect.width > this.x + this.width) ? 
				(this.width + ((rect.x + rect.width) - (this.x + this.width)) + CUSHION) : this.width;
		this.height = (rect.y + rect.height > this.y + this.height) ? 
					(this.height + ((rect.y + rect.height) - (this.y + this.height)) + CUSHION) : this.height;
					
	}

	public void mouseClicked(MouseEvent e) 
	{
		if(contains(e.getX(), e.getY())) 
		{
			if(2 == e.getClickCount()) 
			{
				if(null != this.devicePluginClass && null != this.containerServices)
				{
					try {
						//ChessboardDetailsPlugin pluginToSpawn = ; 
						Constructor constructor = null;
						try {
    					    constructor = this.devicePluginClass.getConstructor(String[].class, ChessboardDetailsPluginListener.class, PluginContainerServices.class, String.class, String.class);
						} catch (SecurityException e1) {
							this.containerServices.getLogger().severe("Security exception while trying to create an instance of a Device GUI panel: " + devicePluginClass.getName());
							e1.printStackTrace();
						} catch (NoSuchMethodException e1) {
							this.containerServices.getLogger().severe("NoSuchMethod exception while trying to create an instance of a Device GUI panel: " + devicePluginClass.getName());
							e1.printStackTrace();
						}
						if(null != constructor)
						{
							ChessboardDetailsPlugin pluginToLaunch = null;
							try {
							    pluginToLaunch = (ChessboardDetailsPlugin)constructor.newInstance(this.antennaNames, null, this.containerServices, 
								        this.deviceNameText + this.antennaNames[0], this.deviceNameText + this.antennaNames[0]);
							} catch (IllegalArgumentException e1) {
								this.containerServices.getLogger().warning("Illegal argument exception, trying to continue.");
							} catch (InstantiationException e1) {
								this.containerServices.getLogger().warning("Instantiation exception, trying to continue.");
							} catch (IllegalAccessException e1) {
								this.containerServices.getLogger().warning("Access exception, trying to continue.");
							} catch (InvocationTargetException e1) {
								this.containerServices.getLogger().warning("Invocation exception, trying to continue.");
							}
							if(null != pluginToLaunch)
							{
								this.containerServices.startChildPlugin(pluginToLaunch.getUniqueName(), pluginToLaunch);
							} 
							else 
							{
								this.containerServices.getLogger().severe("Failed to create an instance of a Device GUI panel: " + devicePluginClass.getName());
							}
						}
					} 
					catch (PluginContainerException e1) 
					{
						this.containerServices.getLogger().warning("Could not launch details plugin");
					}
				}
			}
		}
	}

	public void mouseEntered(MouseEvent e) 
	{
		if(!this.mouseInside == true)
		{
			this.mouseInside = true;
			if(this.status != DeviceStatus.INVISIBLE_DEVICE && null != this.owningComp)
			{
				this.owningComp.setToolTipText(this.tooltipText);
				ToolTipManager.sharedInstance().setEnabled(true);
			}
		}
		
		for(SingleInstanceDeviceRectangle innerRect : this.nestedRectangles)
		{
			if(innerRect.contains(e.getX(), e.getY()) && false == innerRect.mouseInside)
			{
				innerRect.mouseEntered(e);
			}
			else if(!innerRect.contains(e.getX(), e.getY()))
			{
				innerRect.mouseExited(e);
			}
		}
	}

	public void mouseExited(MouseEvent e) 
	{
		if(this.mouseInside)
		{
			this.mouseInside = false;
			if(this.status != DeviceStatus.INVISIBLE_DEVICE && null != this.owningComp)
			{
				this.owningComp.setToolTipText(null);
				ToolTipManager.sharedInstance().setEnabled(false);
			}
		}
		ToolTipManager.sharedInstance().setEnabled(true);		
	}

	public void mousePressed(MouseEvent e) 
	{
//		if(contains(e.getX(), e.getY())) 
//		{
//			System.out.println("mouse pressed being handled for: " + firstLineTextTitle);			
//		}
	}

	public void mouseReleased(MouseEvent e) 
	{
//		if(contains(e.getX(), e.getY())) 
//		{
//			System.out.println("mouse released being handled for: " + firstLineTextTitle);			
//		}
	}

	/**
	 * Getter for whether the mouse cursor is inside the rectangle.
	 * @return boolean indicating whether the mouse cursor is inside the rectangle (true) or not (false).
	 */
	public boolean isMouseInside() {
		return mouseInside;
	}
	
	public void updateStrings(StringUpdateEvent evt) 
	{
		if(null != evt.getTitleLine())
		{
			this.firstLineTextValue = evt.getTitleLine();
		}
		this.secondLineTextValue = evt.getLine1();
		this.thirdLineTextValue = evt.getLine2();
		if(null != this.graphics2d)
		{
			this.draw(this.graphics2d, owningComp);
			owningComp.repaint();
		}
	}
	
	@Override
	public void processStatusChange(ChessboardStatusEvent event) 
	{
		// remove any question marks from values, if we were in the UNKNOWN state but are
		// updating to some other state (no longer unknown).
		if(this.status.equals(DeviceStatus.UNKNOWN) && event.getStatus() != DeviceStatus.UNKNOWN)
		{
			if(this.firstLineTextValue != null && this.firstLineTextValue != "")
			{
				this.firstLineTextValue.replace(" ?", "");
			}
			if(this.secondLineTextValue != null && this.secondLineTextValue != "")
			{
				this.secondLineTextValue.replace(" ?", "");
			}
			if(this.thirdLineTextValue != null && this.thirdLineTextValue != "")
			{
				this.thirdLineTextValue.replace(" ?", "");
			}
		}
		
		this.status = (DeviceStatus)event.getStatus();
		if(null != this.graphics2d)
		{
			this.draw(this.graphics2d, owningComp);
			owningComp.repaint();
		}
		
		// add question marks if we're entering the UNKNOWN state
		if(status.equals(DeviceStatus.UNKNOWN)) 
		{
			if(this.firstLineTextValue != null && this.firstLineTextValue != "" && this.firstLineTextValue.indexOf("?") == -1)
			{
				this.firstLineTextValue += " ?";
			}
			if(this.secondLineTextValue != null && this.secondLineTextValue != "" && this.secondLineTextValue.indexOf("?") == -1)
			{
				this.secondLineTextValue += " ?";
			}
			if(this.thirdLineTextValue != null && this.thirdLineTextValue != "" && this.thirdLineTextValue.indexOf("?") == -1)
			{
				this.thirdLineTextValue += " ?";
			}
		}
		
		if(null != event.getToolTipText() && !event.getToolTipText().equals("") && !this.tooltipText.equals(event.getToolTipText())) {
			this.tooltipText = event.getToolTipText();
		} 
		else if(!this.tooltipText.equals(status.getDescription())) {
			this.tooltipText = status.getDescription();
		}
		if(null != this.owningComp && this.mouseInside) {
			this.owningComp.setToolTipText(this.tooltipText);
		}
	}
	
	private String getFirstLineText()
	{
		String retVal = firstLineTextTitle + firstLineTextValue;
		return retVal;
	}
	
	private String getSecondLineText()
	{
		String retVal = secondLineTextTitle + secondLineTextValue;
		return retVal;
	}

	private String getThirdLineText()
	{
		String retVal = thirdLineTextTitle + thirdLineTextValue;
		return retVal;
	}

	private synchronized void cloneGraphics(Graphics2D graphics)
	{
		this.graphics2d = (Graphics2D)graphics.create();
	}
	
	private void setSecondAndThirdLinesFont(Graphics2D graphics)
	{
		int fontSize = SingleInstanceDeviceRectangle.DEFAULT_TITLE_FONT_SIZE;	 
		if(!this.allLinesSameFontSize) 
		{
			fontSize = SingleInstanceDeviceRectangle.DEFAULT_DATA_FONT_SIZE;
		}

		if(this.allLinesBold)
		{
			graphics.setFont(new Font("", Font.BOLD, fontSize));			
		}
		else
		{
			graphics.setFont(new Font("", Font.PLAIN, fontSize));
		}
		graphics.setColor(getForeground());
	}
	
	private void setFirstLineFont(Graphics2D graphics)
	{
		int fontSize = DEFAULT_TITLE_FONT_SIZE;
		graphics.setFont(new Font("", Font.BOLD, fontSize));
		graphics.setColor(getForeground());
	}
	
	private void drawTextTransformed(Graphics2D graphics) 
	{
		setFirstLineFont(graphics);

		FontMetrics fontMetrics = graphics.getFontMetrics();
		int stringWidth = fontMetrics.stringWidth(getFirstLineText());
		int stringHeight = fontMetrics.getHeight();
		float xLocation = this.x + this.width/2;
		float yLocation = this.y + this.height/2 + stringWidth/2;
		
		AffineTransform translation = AffineTransform.getTranslateInstance(xLocation, yLocation);
		AffineTransform rotator = new AffineTransform();
		rotator.rotate(Math.toRadians(transformAngleForLabelText), 0, 0);
		rotator.concatenate(translation);
		Font rotatedFont = graphics.getFont().deriveFont(rotator);
		FontRenderContext renderContext = new FontRenderContext(null, true, true);
		TextLayout layout = new TextLayout(getFirstLineText(), rotatedFont, renderContext);
		layout.draw(graphics, 0, 0);

		setSecondAndThirdLinesFont(graphics);
		
		if(getSecondLineText().length() > 0)
		{
			xLocation = this.x + this.width/2 + stringHeight;
			translation = AffineTransform.getTranslateInstance(xLocation, yLocation);
			rotator = new AffineTransform();
			rotator.rotate(Math.toRadians(transformAngleForLabelText), 0, 0);
			rotator.concatenate(translation);
			rotatedFont = graphics.getFont().deriveFont(rotator);
			renderContext = new FontRenderContext(null, true, true);
			layout = new TextLayout(getSecondLineText(), rotatedFont, renderContext);
			layout.draw(graphics, 0, 0);
		}
		
		if(getThirdLineText().length() > 0)
		{
			xLocation = this.x + this.width/2 + 1.8f * stringHeight;
			translation = AffineTransform.getTranslateInstance(xLocation, yLocation);
			rotator = new AffineTransform();
			rotator.rotate(Math.toRadians(transformAngleForLabelText), 0, 0);
			rotator.concatenate(translation);
			rotatedFont = graphics.getFont().deriveFont(rotator);
			renderContext = new FontRenderContext(null, true, true);
			layout = new TextLayout(getThirdLineText(), rotatedFont, renderContext);
			layout.draw(graphics, 0, 0);
		}
	}

	private void drawTextHorizontal(Graphics2D graphics) 
	{
		final int INTER_TEXT_PADDING = 2;
		FontMetrics fontMetrics = graphics.getFontMetrics();
		int stringHeight = fontMetrics.getHeight();
		int stringWidth = fontMetrics.stringWidth(getFirstLineText());
	
		setFirstLineFont(graphics);
		if(this.firstLineTextCentered)
		{
			graphics.drawString(getFirstLineText(), (this.x + (this.width - stringWidth)/2), (this.y + stringHeight));
		}
		else 
		{
			int xLocation = this.x + STD_PADDING;
			graphics.drawString(getFirstLineText(), xLocation, (this.y + stringHeight));
		}
		
	
		setSecondAndThirdLinesFont(graphics);
		if(this.secondAndThirdLineTextCentered)
		{
			stringWidth = fontMetrics.stringWidth(getSecondLineText());
			graphics.drawString(getSecondLineText(), (this.x + (this.width - stringWidth)/2), this.y + 2 * stringHeight + 2 * INTER_TEXT_PADDING);
			graphics.drawString(getThirdLineText(), (this.x + (this.width - stringWidth)/2), this.y + 3 * stringHeight + 3 * INTER_TEXT_PADDING);
		}
		else 
		{
			int xLocation = this.x + STD_PADDING;
			
			// special case to handle alignment of text for rectangles that have rectangles inside of them
			if(this.nestedRectangles.size() > 0) 
			{
				// this is a hack (and not a very good one); it will only work if 
				// we find the innerRect of interest (which we wish to use for text alignment) *first* in the list,
				// which depends on the ordering of how the nested rectangles were added to the list; this works
				// for the case of the antenna status GUI but could fail (if others use this code) in other contexts.
				for(SingleInstanceDeviceRectangle innerRect : nestedRectangles) 
				{
					if(innerRect.textOutsideBoundsOfRectangleTop != null) 
					{
						int offset = (innerRect.width - (fontMetrics.stringWidth(innerRect.textOutsideBoundsOfRectangleTop))) / 2;
						xLocation = innerRect.x + offset;
						break;
					}
				}
			}
			
			graphics.drawString(getSecondLineText(), xLocation, this.y + 2 * stringHeight + 2 * INTER_TEXT_PADDING);
			graphics.drawString(getThirdLineText(), xLocation, this.y + 3 * stringHeight + 3 * INTER_TEXT_PADDING);
		}
		
		if(null != this.textOutsideBoundsOfRectangleTop)
		{
			stringWidth = fontMetrics.stringWidth(this.textOutsideBoundsOfRectangleTop);
			graphics.setPaint(this.colorForTextOutsideRectangle);
			graphics.drawString(this.textOutsideBoundsOfRectangleTop, (this.x + (this.width - stringWidth)/2), (this.y - 2 * INTER_TEXT_PADDING));
		}
	}
	
	private Color getBackground()
	{
		Color retValue = null;
		if(null == status.getBgColor() && null != owningComp) {
			retValue = owningComp.getBackground();
		} 
		else {
			retValue = status.getBgColor();
		}
		return retValue;
	}
	
	private Color getForeground()
	{
		Color retValue = null;
		if(null == status.getFgColor() && null != owningComp) {
			retValue = owningComp.getForeground();
		} 
		else {
			retValue = status.getFgColor();
		}
		return retValue;		
	}
}
