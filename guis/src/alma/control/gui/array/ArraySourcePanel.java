package alma.control.gui.array;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import alma.Control.ArraySummaryData;
import alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel;

/**
 * Sub-panel for source information within the array summary plugin.
 * @author Steve Harrington
 */
@SuppressWarnings("serial")
public class ArraySourcePanel extends ArrayStatusBasePanel
{
	private static final String ARRAY_LABEL_TEXT = "Array: ";
	private static final String SOURCE_LABEL_TEXT = "Source: ";
	private static final String ONLINE_ANTENNAS_LABEL_TEXT = "Online antennas: ";
	private static final String BAD_ANTENNAS_LABEL_TEXT = "Degraded antennas: ";
	private static final String SUMMARY_PANEL_TITLE = "Summary";
	
	private ArrayList<QuestionMarkLabel> questionMarkLabels = new ArrayList<QuestionMarkLabel>();
	
	private JLabel arrayNameTextLabel;
	private QuestionMarkLabel arrayNameQuestionableLabel;
	private JLabel arrayNameValueLabel;
	
	private JLabel sourceNameTextLabel;
	private QuestionMarkLabel sourceNameQuestionableLabel;
	private JLabel sourceNameValueLabel;
	
	private JLabel badAntennasTextLabel;
	private QuestionMarkLabel badAntennasQuestionableLabel;
	private JLabel badAntennasValueLabel;
	
	private JLabel onlineAntennasTextLabel;
	private QuestionMarkLabel onlineAntennasQuestionableLabel;
	private JLabel onlineAntennasValueLabel;
	
	/**
	 * Constructor.
	 * @param logger the logger to use for logging informational/debug messages.
	 */
	public ArraySourcePanel(Logger logger)
	{
		super(logger);
		logger.entering("ArraySourcePanel", "constructor");
		initialize();
		logger.exiting("ArraySourcePanel", "constructor");
	}
	
	public void communicationEstablished(CommunicationEstablishedListener.Aspect aspect, Object dataPayload) 
	{
		logger.entering("ArraySourcePanel", "communicationEstablished");
		if(aspect == CommunicationEstablishedListener.Aspect.SUMMARY) 
		{
			this.communicationEstablished = true;
			MiscUtilities.updateQuestionMarkLabels(questionMarkLabels, communicationEstablished);
			if(dataPayload != null && dataPayload instanceof ArraySummaryData)
			{
				ArraySummaryData summaryData = (ArraySummaryData) dataPayload;
				this.arrayNameValueLabel.setText(summaryData.arrayName);
				this.sourceNameValueLabel.setText(summaryData.sourceName);
				this.badAntennasValueLabel.setText(Short.toString(summaryData.degradedAntennaCount));
				this.onlineAntennasValueLabel.setText(Short.toString(summaryData.onlineAntennaCount));
				setForegroundColorOnValueLabels(null);
			}
		}
		logger.exiting("ArraySourcePanel", "communicationEstablished");
	}

	public void communicationLost(CommunicationEstablishedListener.Aspect aspect) 
	{
		logger.entering("ArraySourcePanel", "communicationLost");
		if(aspect == CommunicationEstablishedListener.Aspect.SUMMARY) {
			this.communicationEstablished = false;
			MiscUtilities.updateQuestionMarkLabels(questionMarkLabels, communicationEstablished);
			setForegroundColorOnValueLabels(ArrayStatusPlugin.STALE_DATA_COLOR);
		}
		logger.exiting("ArraySourcePanel", "communicationLost");
	}
	
	private void setForegroundColorOnValueLabels(Color color)
	{
		this.badAntennasValueLabel.setForeground(color);
		this.onlineAntennasValueLabel.setForeground(color);
		this.sourceNameValueLabel.setForeground(color);
	}
	
	private void initialize()
	{
		logger.entering("ArraySourcePanel", "initialize");
		this.setLayout(new GridBagLayout());
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.ipadx = 10;
		constraints.gridx = GridBagConstraints.RELATIVE;
		constraints.gridy = 0;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		
		JPanel arraySummaryPanel = createArraySummaryPanel();
		this.add(arraySummaryPanel, constraints);
		
		logger.exiting("ArraySourcePanel", "exiting");
	}

	/**
	 * @return a sub-panel containing summary info for the array
	 */
	private JPanel createArraySummaryPanel() 
	{
		logger.entering("ArraySourcePanel", "createArraySummaryPanel");
		JPanel arraySummaryPanel = new JPanel(new GridBagLayout());
		arraySummaryPanel.setBorder(BorderFactory.createTitledBorder(SUMMARY_PANEL_TITLE));
		GridBagConstraints innerConstraints = new GridBagConstraints();
		innerConstraints.fill = GridBagConstraints.NONE;
		innerConstraints.anchor = GridBagConstraints.NORTHWEST;
		innerConstraints.gridx = 0;
		innerConstraints.gridy = GridBagConstraints.RELATIVE;
		
		// Array name
		JPanel arrayNamePanel = createArrayNamePanel();
		arraySummaryPanel.add(arrayNamePanel, innerConstraints);
		
		// Source name
		JPanel sourceNamePanel = createSourceNamePanel();
		arraySummaryPanel.add(sourceNamePanel, innerConstraints);
		
		innerConstraints.weightx = 1.0;
		innerConstraints.weighty = 1.0;

		// Degraded antenna count
		JPanel antCountPanel = createOnlineAndDegradedAntennaCountPanel();
		arraySummaryPanel.add(antCountPanel, innerConstraints);

		logger.exiting("ArraySourcePanel", "createArraySummaryPanel", arraySummaryPanel);
		return arraySummaryPanel;
	}

	private JPanel createOnlineAndDegradedAntennaCountPanel()
	{
		logger.entering("ArraySourcePanel", "createOnlineAndDegradedAntennaCountPanel");
		JPanel retPanel = new JPanel(new GridBagLayout());
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.NONE;
		constraints.weighty = 0.0;
		constraints.gridy = 0;
		constraints.ipadx = 0;
		constraints.ipady = 0;
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.weightx = 0.0;
		constraints.gridy++;

		// Online antennas
		onlineAntennasTextLabel = new JLabel(ONLINE_ANTENNAS_LABEL_TEXT);
		retPanel.add(onlineAntennasTextLabel, constraints);

		badAntennasValueLabel = new JLabel("N/A");
		retPanel.add(badAntennasValueLabel, constraints);

		constraints.weightx = 0.0;
		constraints.ipadx = 5;
		badAntennasQuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		questionMarkLabels.add(badAntennasQuestionableLabel);
		retPanel.add(badAntennasQuestionableLabel, constraints);
		
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		constraints.ipadx = 0;
		constraints.gridy++;
		
		// Degraded antennas
		badAntennasTextLabel = new JLabel(BAD_ANTENNAS_LABEL_TEXT);
		retPanel.add(badAntennasTextLabel, constraints);

		constraints.ipadx = 5;
		onlineAntennasValueLabel = new JLabel("N/A");
		retPanel.add(onlineAntennasValueLabel, constraints);

		constraints.weightx = 1.0;
		onlineAntennasQuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		questionMarkLabels.add(onlineAntennasQuestionableLabel);
		retPanel.add(onlineAntennasQuestionableLabel, constraints);
		
		logger.exiting("ArraySourcePanel", "createOnlineAndDegradedAntennaCountPanel", retPanel);
		return retPanel;
	}
	
	/**
	 * @return a sub-panel containing the source name information
	 */
	private JPanel createSourceNamePanel() 
	{
		logger.entering("ArraySourcePanel", "createSourceNamePanel");
		JPanel sourceNamePanel = new JPanel(new GridBagLayout());
		GridBagConstraints innerConstraints = new GridBagConstraints();
		innerConstraints.fill = GridBagConstraints.NONE;
		innerConstraints.anchor = GridBagConstraints.NORTHWEST;
		sourceNameTextLabel = new JLabel(SOURCE_LABEL_TEXT);
		sourceNamePanel.add(sourceNameTextLabel, innerConstraints);
		
		innerConstraints.ipadx = 5;
		sourceNameValueLabel = new JLabel(("unknown"));
		sourceNamePanel.add(sourceNameValueLabel, innerConstraints);
		
		sourceNameQuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		questionMarkLabels.add(sourceNameQuestionableLabel);
		sourceNamePanel.add(sourceNameQuestionableLabel, innerConstraints);
		
		logger.exiting("ArraySourcePanel", "createSourceNamePanel", sourceNamePanel);
		return sourceNamePanel;
	}

	private JPanel createArrayNamePanel()
	{
		logger.entering("ArraySourcePanel", "createArrayNamePanel");
		JPanel arrayNamePanel = new JPanel(new GridBagLayout());
		GridBagConstraints innerConstraints = new GridBagConstraints();
		innerConstraints.fill = GridBagConstraints.NONE;
		innerConstraints.anchor = GridBagConstraints.NORTHWEST;
		
		arrayNameTextLabel = new JLabel(ARRAY_LABEL_TEXT);
		arrayNamePanel.add(arrayNameTextLabel, innerConstraints);
		
		innerConstraints.ipadx = 5;
		arrayNameValueLabel = new JLabel(("unknown"));
		arrayNamePanel.add(arrayNameValueLabel, innerConstraints);
		
		arrayNameQuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		questionMarkLabels.add(arrayNameQuestionableLabel);
		arrayNamePanel.add(arrayNameQuestionableLabel, innerConstraints);

		logger.exiting("ArraySourcePanel", "createArrayNamePanel", arrayNamePanel);
		return arrayNamePanel;
	}
	
	//	 for testing only
	private static void createAndShowGUI()
	{
		JFrame mainFrame = new JFrame();
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.add(new ArraySourcePanel(Logger.getLogger("test logger")));
		mainFrame.pack();
		mainFrame.setVisible(true);
	}
	
	/**
	 * @param args
	 * for testing only
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() { ArraySourcePanel.createAndShowGUI();}
		});
	}
}
