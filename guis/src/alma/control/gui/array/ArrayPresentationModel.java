package alma.control.gui.array;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;

import alma.Control.ArrayCorrelatorData;
import alma.Control.ArrayObservingData;
import alma.Control.ArrayPointingData;
import alma.Control.ArrayReceiverData;
import alma.Control.ArrayStatusMock;
import alma.Control.ArrayStatusMockHelper;
import alma.Control.ArraySummaryData;
import alma.Control.ArrayWeatherData;
import alma.Control.device.gui.common.Pollable;
import alma.Control.device.gui.common.PollingManager;
import alma.acs.logging.RepeatGuardLogger;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

/**
 * Controller used to interact with the control subsystem; the GUI should use 
 * this class as a facade for interactions with the ACS/CORBA components of
 * the CONTROL subsystem. This will serve to decouple the GUI from changes, 
 * providing a single point of maintenance for such changes.
 * 
 * TODO: remove stack trace printouts && move them to logging system.
 * 
 * @author Steve Harrington
 */
public class ArrayPresentationModel implements Pollable, CommunicationEstablishedPublisher
{
	private final static String COMPONENT_NAME = "ARRAY_STATUS_MOCK";
	private PluginContainerServices containerServices;
	private Logger logger;
	private PollingManager pollingManager;
	private ArrayStatusMock dataComponent;
	private ArrayList<CommunicationEstablishedListener> listeners = new ArrayList<CommunicationEstablishedListener>();
	private final static long REPEAT_GUARD_INTERVAL = 10;
	private RepeatGuardLogger repeatLogger = new RepeatGuardLogger(REPEAT_GUARD_INTERVAL, TimeUnit.SECONDS, 1);

	/**
	 * Constructor.
	 * @param services the plugin container services used for things like getting components, getting a logger, etc.
	 */
	public ArrayPresentationModel(PluginContainerServices services)
	{
		this.containerServices = services;
		this.logger = containerServices.getLogger();
		logger.entering("ArrayPresentationModel", "constructor");
		this.pollingManager = new PollingManager(this);
		pollingManager.setLogger(logger);
		getDataComponent();		
		logger.exiting("ArrayPresentationModel", "constructor");
	}

	/**
	 * Starts the presentation model, e.g. the polling manager, etc.
	 */
	public void start()
	{
		logger.entering("ArrayPresentationModel", "start");
		if(!pollingManager.nowPolling()) {
			pollingManager.startPolling();
		}
		logger.exiting("ArrayPresentationModel", "start");
		
	}
	
	/**
	 * Stops the presentation model, e.g. the polling manager, etc.
	 */
	public void stop()
	{
		logger.entering("ArrayPresentationModel", "stop");
		pollingManager.stopPolling();
		logger.exiting("ArrayPresentationModel", "stop");
	}
	
	public void pollSlowMonitorPoints() 
	{
		logger.entering("ArrayPresentationModel", "pollSlowMonitorPoints");
		
		if(null == this.dataComponent) 
		{
			getDataComponent();
		}
		
		// 1) poll summary info;
		getSummaryData();
		
		// 2) poll observing (scheduling) info
		getObservingData();
		
		// 3) poll receiver info
		getReceiverData();
		
		// 5) poll weather info
		getWeatherData();
		
		// 6) poll correlator info
		getCorrelatorData();
		
		logger.exiting("ArrayPresentationModel", "pollSlowMonitorPoints");
	}

	public void pollFastMonitorPoints() 
	{
		logger.entering("ArrayPresentationModel", "pollFastMonitorPoints");
		if(null == this.dataComponent) 
		{
			getDataComponent();
		}
		getPointingData();
		logger.exiting("ArrayPresentationModel", "pollFastMonitorPoints");
	}

    /**
     * Report if the device is ready for polling.
     * 
     * @return true if the device is ready for polling.
     */
    public boolean readyForPolling() {
        // TODO: Review class and see if this needs a more detailed implementation.
        return true;
    }

    public void addCommunicationEstablishedListener(CommunicationEstablishedListener listener) 
	{
		this.listeners.add(listener);
	}

	public void removeCommunicationEstablishedListener(CommunicationEstablishedListener listener) 
	{
		this.listeners.remove(listener);
	}

	private void getDataComponent() 
	{
		try {
			Object corbaObj = containerServices.getComponent(COMPONENT_NAME);
			if(null != corbaObj) {
				this.dataComponent = ArrayStatusMockHelper.narrow(corbaObj);
			}
		} catch (Throwable e) {
			repeatLogger.logAndIncrement(logger, Level.WARNING, "Cannot get component for GUI! ");
			this.dataComponent = null;

		}
	}

	/**
	 * Gets the summary data for the array. 
	 */
	private void getSummaryData() 
	{
		logger.entering("ArrayPresentationModel", "getSummaryData");
		if(null != this.dataComponent) {
			try {
				ArraySummaryData data = dataComponent.getArraySummaryData();
				this.notifyListenersOfEstablishedCommunications(CommunicationEstablishedListener.Aspect.SUMMARY, data);
				logger.finest("Got the summary data!" + data);
			}
			catch(Throwable ex) {
				this.dataComponent = null;
				repeatLogger.logAndIncrement(logger, Level.WARNING,"Could not get summary data because of unexpected exception - container down? " + ex);
				this.notifyListenersOfLostCommunications(CommunicationEstablishedListener.Aspect.SUMMARY);
			}
		}
		else {
			repeatLogger.logAndIncrement(logger, Level.WARNING, "Could not get summary data because data component was null.");
			this.notifyListenersOfLostCommunications(CommunicationEstablishedListener.Aspect.SUMMARY);
		}
		logger.exiting("ArrayPresentationModel", "getSummaryData");
	}
	
	/**
	 * Gets the observing data for the array. 
	 */
	private void getObservingData() 
	{
		logger.entering("ArrayPresentationModel", "getObservingData");
		if(null != this.dataComponent) {
			try {
				ArrayObservingData data = dataComponent.getArrayObservingData();
				this.notifyListenersOfEstablishedCommunications(CommunicationEstablishedListener.Aspect.OBSERVING, data);
				logger.finest("Got the observing data!" + data);
			}
			catch(Throwable ex) {
				this.dataComponent = null;
				repeatLogger.logAndIncrement(logger, Level.WARNING,"Could not get observing data because of unexpected exception - container down? " + ex);
				this.notifyListenersOfLostCommunications(CommunicationEstablishedListener.Aspect.OBSERVING);
			}
		}
		else {
			repeatLogger.logAndIncrement(logger, Level.WARNING, "Could not get observing data because data component was null.");
			this.notifyListenersOfLostCommunications(CommunicationEstablishedListener.Aspect.OBSERVING);
		}
		logger.exiting("ArrayPresentationModel", "getObservingData");
	}
	
	/**
	 * Gets the receiver data for the array. 
	 */
	private void getReceiverData() 
	{
		logger.entering("ArrayPresentationModel", "getReceiverData");
		if(null != this.dataComponent) {
			try {
				ArrayReceiverData data = dataComponent.getArrayReceiverData();
				logger.finest("Got the receiver data!" + data);
				this.notifyListenersOfEstablishedCommunications(CommunicationEstablishedListener.Aspect.RECEIVER, data);
			}
			catch(Throwable ex) {
				this.dataComponent = null;
				repeatLogger.logAndIncrement(logger, Level.WARNING,"Could not get receiver data because of unexpected exception - container down? " + ex);
				this.notifyListenersOfLostCommunications(CommunicationEstablishedListener.Aspect.RECEIVER);
			}
		}
		else {
			repeatLogger.logAndIncrement(logger, Level.WARNING, "Could not get receiver data because data component was null.");
			this.notifyListenersOfLostCommunications(CommunicationEstablishedListener.Aspect.RECEIVER);
		}
		logger.exiting("ArrayPresentationModel", "getReceiverData");
	}
	
	/**
	 * Gets the weather data for the array. 
	 */
	private void getWeatherData() 
	{
		logger.entering("ArrayPresentationModel", "getWeatherData");
		if(null != this.dataComponent) {
			try {
				ArrayWeatherData data = dataComponent.getArrayWeatherData();
				this.notifyListenersOfEstablishedCommunications(CommunicationEstablishedListener.Aspect.WEATHER, data);
				logger.finest("Got the weather data!" + data);
			}
			catch(Throwable ex) {
				this.dataComponent = null;
				repeatLogger.logAndIncrement(logger, Level.WARNING,"Could not get weather data because of unexpected exception - container down? " + ex);
				this.notifyListenersOfLostCommunications(CommunicationEstablishedListener.Aspect.WEATHER);
			}
		}
		else {
			repeatLogger.logAndIncrement(logger, Level.WARNING, "Could not get weather data because data component was null.");
			this.notifyListenersOfLostCommunications(CommunicationEstablishedListener.Aspect.WEATHER);
		}
		logger.exiting("ArrayPresentationModel", "getWeatherData");
	}
	
	/**
	 * Gets the correlator data for the array. 
	 */
	private void getCorrelatorData() 
	{
		logger.entering("ArrayPresentationModel", "getCorrelatorData");
		if(null != this.dataComponent) {
			try {
				ArrayCorrelatorData data = dataComponent.getArrayCorrelatorData();
				this.notifyListenersOfEstablishedCommunications(CommunicationEstablishedListener.Aspect.CORRELATOR, data);
				logger.finest("Got the corr data!" + data);
			}
			catch(Throwable ex) {
				this.dataComponent = null;
				repeatLogger.logAndIncrement(logger, Level.WARNING,"Could not get corr data because of unexpected exception - container down? " +ex);
				this.notifyListenersOfLostCommunications(CommunicationEstablishedListener.Aspect.CORRELATOR);
			}
		}
		else {
			repeatLogger.logAndIncrement(logger, Level.WARNING, "Could not get corr data because data component was null.");
			this.notifyListenersOfLostCommunications(CommunicationEstablishedListener.Aspect.CORRELATOR);
		}
		logger.exiting("ArrayPresentationModel", "getCorrelatorData");
	}

	/**
	 * Gets the weather data for the array. 
	 */
	private void getPointingData() 
	{
		logger.entering("ArrayPresentationModel", "getPointingData");
		if(null != this.dataComponent) {
			try {
				ArrayPointingData data = dataComponent.getArrayPointingData();
				this.notifyListenersOfEstablishedCommunications(CommunicationEstablishedListener.Aspect.POINTING, data);
				logger.finest("Got the pointing data!" + data);
			}
			catch(Throwable ex) {
				this.dataComponent = null;
				repeatLogger.logAndIncrement(logger, Level.WARNING,"Could not get pointing data because of unexpected exception - container down? " + ex);
				this.notifyListenersOfLostCommunications(CommunicationEstablishedListener.Aspect.POINTING);
			}
		}
		else {
			repeatLogger.logAndIncrement(logger, Level.WARNING, "Could not get pointing data because data component was null.");
			this.notifyListenersOfLostCommunications(CommunicationEstablishedListener.Aspect.POINTING);
		}
		logger.exiting("ArrayPresentationModel", "getPointingData");
	}	
	
	private void notifyListenersOfEstablishedCommunications(CommunicationEstablishedListener.Aspect aspect, Object data)
	{
		for(CommunicationEstablishedListener listener : listeners) 
		{
			CommunicationEstablishedUpdateThread updateThread = new CommunicationEstablishedUpdateThread(listener, data, aspect);
			SwingUtilities.invokeLater(updateThread);
		}
	}
	
	private void notifyListenersOfLostCommunications(CommunicationEstablishedListener.Aspect aspect)
	{
		for(CommunicationEstablishedListener listener : listeners) 
		{
			CommunicationLostSwingThread updateThread = new CommunicationLostSwingThread(listener, aspect);
			SwingUtilities.invokeLater(updateThread);
		}	
	}
	
	/**
	 * Private class to execute communication established updates on the GUI on the swing thread.
	 * @author sharring
	 */
	private class CommunicationEstablishedUpdateThread extends Thread
	{
		private CommunicationEstablishedListener listener;
		private Object data;
		private CommunicationEstablishedListener.Aspect aspect;
		
		CommunicationEstablishedUpdateThread(CommunicationEstablishedListener listener, 
				Object data, 
				CommunicationEstablishedListener.Aspect aspect)
		{
			this.data = data;
			this.listener = listener;
			this.aspect = aspect;
		}
		
		public void run()
		{
			listener.communicationEstablished(aspect, data);
		}
	}

	/**
	 * Private class to execute communication lost updates on the GUI on the swing thread.
	 * @author sharring
	 */
	private class CommunicationLostSwingThread extends Thread
	{
		private CommunicationEstablishedListener listener;
		private CommunicationEstablishedListener.Aspect aspect;
		
		CommunicationLostSwingThread(CommunicationEstablishedListener listener, 
				CommunicationEstablishedListener.Aspect aspect)
		{
			this.listener = listener;
			this.aspect = aspect;
		}
		
		public void run()
		{
			listener.communicationLost(aspect);
		}
	}
}
