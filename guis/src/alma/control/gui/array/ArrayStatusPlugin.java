package alma.control.gui.array;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.logging.Logger;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import alma.exec.extension.subsystemplugin.IPauseResume;
import alma.exec.extension.subsystemplugin.PluginContainerServices;
import alma.exec.extension.subsystemplugin.SubsystemPlugin;

/**
 * Test class for mocking up designs for the array status panel/GUI.
 * 
 * @author Steve Harrington
 */
@SuppressWarnings("serial")
public class ArrayStatusPlugin extends JPanel implements SubsystemPlugin, IPauseResume
{
	private PluginContainerServices containerServices;
	private ArrayPresentationModel presentationModel;
	private Logger logger;
	private ArraySourcePanel arraySourcePanel;
	private ArrayPointingPanel arrayPointingPanel;
	private ArraySchedulingInfoPanel arraySchedulingInfoPanel;
	private ArrayWeatherPanel arrayWeatherPanel;
	private ArrayReceiverPanel arrayReceiverPanel;
	private ArrayCorrPanel arrayCorrPanel;
	static final Color STALE_DATA_COLOR = Color.ORANGE;
	
	/**
	 * Constructor.
	 *
	 */
	public ArrayStatusPlugin()
	{
		logger = Logger.getLogger("ArrayStatusPlugin logger");
		initialize();
	}
	
	public boolean runRestricted(boolean restricted) throws Exception 
	{
		// TODO Auto-generated method stub
		return false;
	}

	public void setServices(PluginContainerServices services) 
	{
		this.containerServices = services;
		this.presentationModel = new ArrayPresentationModel(services);
		this.logger = containerServices.getLogger();
		arraySourcePanel.setLogger(logger);
		arraySourcePanel.configureListening(presentationModel);
		arrayPointingPanel.setLogger(logger);
		arrayPointingPanel.configureListening(presentationModel);
		arraySchedulingInfoPanel.setLogger(logger);
		arraySchedulingInfoPanel.configureListening(presentationModel);
		arrayWeatherPanel.setLogger(logger);
		arrayWeatherPanel.configureListening(presentationModel);
		arrayReceiverPanel.setLogger(logger);
		arrayReceiverPanel.configureListening(presentationModel);
		arrayCorrPanel.setLogger(logger);
		arrayCorrPanel.configureListening(presentationModel);
	}

	public void start() throws Exception 
	{
		logger.entering("ArrayStatusPlugin","start");
		presentationModel.start();
		logger.exiting("ArrayStatusPlugin", "start");
	}

	public void stop() throws Exception 
	{
		logger.entering("ArrayStatusPlugin","stop");
		presentationModel.stop();
		logger.exiting("ArrayStatusPlugin","stop");
	}
	

	public void pause() throws Exception 
	{
		logger.entering("ArrayStatusPlugin","pause");
		presentationModel.stop();
		logger.exiting("ArrayStatusPlugin", "pause");	
	}

	public void resume() throws Exception 
	{
		logger.entering("ArrayStatusPlugin","resume");
		presentationModel.start();
		logger.exiting("ArrayStatusPlugin", "resume");
	}
	
	private void initialize()
	{
		JPanel scrolledPanel = new JPanel(new GridBagLayout());
		scrolledPanel.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.BOTH;
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.gridx = GridBagConstraints.RELATIVE;
		constraints.weightx = 0.2;
		constraints.weighty = 0.0;
		constraints.gridy = 0;
		constraints.insets = new Insets(4,2,4,2);
		
		arraySourcePanel = new ArraySourcePanel(logger);
		scrolledPanel.add(arraySourcePanel, constraints);
		
		constraints.weightx = 0.8;
		arrayPointingPanel = new ArrayPointingPanel(logger);
		scrolledPanel.add(arrayPointingPanel, constraints);
		
		constraints.gridy++;
		constraints.weightx = 0.2;

		arraySchedulingInfoPanel = new ArraySchedulingInfoPanel(logger);
		scrolledPanel.add(arraySchedulingInfoPanel, constraints);

		arrayWeatherPanel = new ArrayWeatherPanel(logger);
		scrolledPanel.add(arrayWeatherPanel, constraints);
			
		constraints.gridy++;
		constraints.weightx = 0.2;
		constraints.weighty = 1.0;
		
		arrayReceiverPanel = new ArrayReceiverPanel(logger);
		scrolledPanel.add(arrayReceiverPanel, constraints);
		
		constraints.weightx = 0.8;
		arrayCorrPanel = new ArrayCorrPanel(logger);
		scrolledPanel.add(arrayCorrPanel, constraints);		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportView(scrolledPanel);
		this.setLayout(new BorderLayout());
		this.add(scrollPane, BorderLayout.CENTER);
		this.validate();
		this.setVisible(true);
	}
}
