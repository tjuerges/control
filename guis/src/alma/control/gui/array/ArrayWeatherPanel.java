package alma.control.gui.array;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import alma.Control.ArrayWeatherData;
import alma.Control.gui.hardwaredevice.common.util.FormattedFloat;
import alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel;

/**
 * Panel to contain weather information for the array status plugin.
 * @author Steve Harrington
 */
@SuppressWarnings("serial")
public class ArrayWeatherPanel extends ArrayStatusBasePanel
{
	private static final String WIND_DIRECTION_LABEL_TEXT = "Wind direction:";
	private static final String METERS_PER_SEC_UNITS_LABEL_TEXT = "m/s";
	private static final String WIND_LABEL_TEXT = "Wind speed:";
	private static final String CELCIUS_UNITS_LABEL_TEXT = "C";
	private static final String DEGREE_SIGN_ASCII_TEXT = "\u00B0";
	private static final String AMB_TEMP_LABEL_TEXT = "Ambient temp:";
	private static final String ASEC_UNITS_LABEL_TEXT = "asec";
	private static final String SEEING_LABEL_TEXT = "Seeing:";
	private static final String NEPERS_UNITS_LABEL_TEXT = "nepers";
	private static final String OPACITY230_LABEL_TEXT = "Opacity 230 GHz:";
	private static final String OPACITYLO_LABEL_TEXT = "Opacity ref freq:";
	private static final String WEATHER_PANEL_TITLE = "Weather";
	
	private ArrayList<QuestionMarkLabel> questionMarkLabels = new ArrayList<QuestionMarkLabel>();
	
	private JLabel opacityLOTextLabel;
	private JLabel opacityLOValueLabel;
	private QuestionMarkLabel opacityLOQuestionableLabel;
	private JLabel opacityLOUnitsLabel;
	
	private JLabel opacity230TextLabel;
	private JLabel opacity230ValueLabel;
	private QuestionMarkLabel opacity230QuestionableLabel;
	private JLabel opacity230UnitsLabel;
	
	private JLabel seeingTextLabel;
	private JLabel seeingValueLabel;
	private QuestionMarkLabel seeingQuestionableLabel;
	private JLabel seeingUnitsLabel;
	
	private JLabel ambientTempTextLabel;
	private JLabel ambientTempValueLabel;
	private QuestionMarkLabel ambientTempQuestionableLabel;
	private JLabel ambientTempUnitsLabel;
	
	private JLabel windSpeedTextLabel;
	private JLabel windSpeedValueLabel;
	private QuestionMarkLabel windSpeedQuestionableLabel;
	private JLabel windSpeedUnitsLabel;
	
	private JLabel windDirectionTextLabel;
	private QuestionMarkLabel windDirectionQuestionableLabel;
	private JLabel windDirectionValueLabel;
	
	/**
	 * Constructor.
	 * 
	 * @param logger to use for logging informational/debug messages.
	 */
	public ArrayWeatherPanel(Logger logger)
	{
		super(logger);
		logger.entering("ArrayWeatherPanel", "constructor");
		initialize();
		logger.exiting("ArrayWeatherPanel", "constructor");
	}
	
	public void communicationEstablished(CommunicationEstablishedListener.Aspect aspect, Object dataPayload) 
	{
		logger.entering("ArrayWeatherPanel", "communicationEstablished");
		if(aspect == CommunicationEstablishedListener.Aspect.WEATHER) 
		{
			this.communicationEstablished = true;
			MiscUtilities.updateQuestionMarkLabels(questionMarkLabels, communicationEstablished);
			if(null != dataPayload && dataPayload instanceof ArrayWeatherData)
			{
				ArrayWeatherData weatherData = (ArrayWeatherData) dataPayload;
				this.ambientTempValueLabel.setText(new FormattedFloat(weatherData.ambientTemp, "%3.1f").toString().trim());
				this.opacity230ValueLabel.setText(new FormattedFloat(weatherData.opacity230GHz, "%9.2f").toString().trim());
				this.opacityLOValueLabel.setText(new FormattedFloat(weatherData.opacityRefFrequency, "%9.2f").toString().trim());
				this.seeingValueLabel.setText(new FormattedFloat(weatherData.seeing, "%9.2f").toString().trim());
				this.windSpeedValueLabel.setText(new FormattedFloat(weatherData.windSpeed, "%9.1f").toString().trim());
				this.windDirectionValueLabel.setText(Short.toString(weatherData.windDirection) + DEGREE_SIGN_ASCII_TEXT);
				setForegroundColors(null);
			}
		}
		logger.exiting("ArrayWeatherPanel", "communicationEstablished");
	}

	public void communicationLost(CommunicationEstablishedListener.Aspect aspect) 
	{
		logger.entering("ArrayWeatherPanel", "communicationLost");
		if(aspect == CommunicationEstablishedListener.Aspect.WEATHER) {
			this.communicationEstablished = false;
			MiscUtilities.updateQuestionMarkLabels(questionMarkLabels, communicationEstablished);
			setForegroundColors(ArrayStatusPlugin.STALE_DATA_COLOR);
		}
		logger.exiting("ArrayWeatherPanel", "communicationLost");
	}

	private void setForegroundColors(Color color)
	{
		this.ambientTempValueLabel.setForeground(color);
		this.opacity230ValueLabel.setForeground(color);
		this.opacityLOValueLabel.setForeground(color);
		this.seeingValueLabel.setForeground(color);
		this.windSpeedValueLabel.setForeground(color);
		this.windDirectionValueLabel.setForeground(color);
	}
	
	private void initialize() 
	{
		logger.entering("ArrayWeatherPanel", "initialize");
		this.setLayout(new GridBagLayout());
		this.setBorder(BorderFactory.createTitledBorder(WEATHER_PANEL_TITLE));
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.ipadx = 0;
		constraints.ipady = 0;
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		
		this.add(createOpacitiesPanel(), constraints);
		
		constraints.gridy++;
		
		this.add(createSeeingPanel(), constraints);
		
		constraints.gridy++;
		
		this.add(createAmbientTempPanel(), constraints);
		
		constraints.gridy++;
		
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		this.add(createWindSpeedAndDirPanel(), constraints);
		logger.exiting("ArrayWeatherPanel", "initialize");
	}
	
	private JPanel createOpacitiesPanel()
	{
		logger.entering("ArrayWeatherPanel", "createOpacitiesPanel");
		JPanel retPanel = new JPanel(new GridBagLayout());

		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.ipadx = 5;
		constraints.ipady = 0;
		constraints.gridx = GridBagConstraints.RELATIVE;
		constraints.gridy = 0;
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		
		opacityLOTextLabel = new JLabel(OPACITYLO_LABEL_TEXT);
		retPanel.add(opacityLOTextLabel, constraints);
		
		opacityLOValueLabel = new JLabel("00.00");
		retPanel.add(opacityLOValueLabel, constraints);
		
		opacityLOUnitsLabel = new JLabel(NEPERS_UNITS_LABEL_TEXT);
		retPanel.add(opacityLOUnitsLabel, constraints);

		constraints.weightx = 1.0;
		opacityLOQuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		questionMarkLabels.add(opacityLOQuestionableLabel);
		retPanel.add(opacityLOQuestionableLabel, constraints);
		
		constraints.weightx = 0.0;
		constraints.gridy++;
		
		opacity230TextLabel = new JLabel(OPACITY230_LABEL_TEXT);
		retPanel.add(opacity230TextLabel, constraints);
		
		opacity230ValueLabel = new JLabel("00.00");
		retPanel.add(opacity230ValueLabel, constraints);
		
		opacity230UnitsLabel = new JLabel(NEPERS_UNITS_LABEL_TEXT);
		retPanel.add(opacity230UnitsLabel, constraints);

		constraints.weightx = 1.0;
		opacity230QuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		questionMarkLabels.add(opacity230QuestionableLabel);
		retPanel.add(opacity230QuestionableLabel, constraints);
		
		logger.exiting("ArrayWeatherPanel", "createOpacitiesPanel", retPanel);
		return retPanel;
	}
	
	
	private JPanel createSeeingPanel()
	{
		logger.entering("ArrayWeatherPanel", "createSeeingPanel");
		JPanel seeingPanel = new JPanel(new GridBagLayout());

		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.ipadx = 5;
		constraints.ipady = 0;
		constraints.gridx = GridBagConstraints.RELATIVE;
		constraints.gridy = 0;
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		
		seeingTextLabel = new JLabel(SEEING_LABEL_TEXT);
		seeingPanel.add(seeingTextLabel, constraints);
		
		seeingValueLabel = new JLabel("00.00");
		seeingPanel.add(seeingValueLabel, constraints);
		
		seeingUnitsLabel = new JLabel(ASEC_UNITS_LABEL_TEXT);
		seeingPanel.add(seeingUnitsLabel, constraints);
		
		seeingQuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		questionMarkLabels.add(seeingQuestionableLabel);
		seeingPanel.add(seeingQuestionableLabel, constraints);
		
		logger.exiting("ArrayWeatherPanel", "createSeeingPanel", seeingPanel);
		return seeingPanel;
	}
	
	private JPanel createAmbientTempPanel()
	{
		logger.entering("ArrayWeatherPanel", "createAmbientTempPanel");
		JPanel ambientTempPanel = new JPanel(new GridBagLayout());

		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.ipadx = 5;
		constraints.ipady = 0;
		constraints.gridx = GridBagConstraints.RELATIVE;
		constraints.gridy = 0;
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		
		ambientTempTextLabel = new JLabel(AMB_TEMP_LABEL_TEXT);
		ambientTempPanel.add(ambientTempTextLabel, constraints);
		
		ambientTempValueLabel = new JLabel("00.0");
		ambientTempPanel.add(ambientTempValueLabel, constraints);
		
		ambientTempUnitsLabel = new JLabel(DEGREE_SIGN_ASCII_TEXT + CELCIUS_UNITS_LABEL_TEXT);
		ambientTempPanel.add(ambientTempUnitsLabel, constraints);
		
		ambientTempQuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		questionMarkLabels.add(ambientTempQuestionableLabel);
		ambientTempPanel.add(ambientTempQuestionableLabel, constraints);
		
		logger.exiting("ArrayWeatherPanel", "createAmbientTempPanel", ambientTempPanel);
		return ambientTempPanel;
	}
	
	private JPanel createWindSpeedPanel()
	{
		logger.entering("ArrayWeatherPanel", "createWindSpeedPanel");
		JPanel windPanel = new JPanel(new GridBagLayout());

		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.ipadx = 5;
		constraints.ipady = 0;
		constraints.gridx = GridBagConstraints.RELATIVE;
		constraints.gridy = 0;
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		
		windSpeedTextLabel = new JLabel(WIND_LABEL_TEXT);
		windPanel.add(windSpeedTextLabel, constraints);
		
		windSpeedValueLabel = new JLabel("00.0");
		windPanel.add(windSpeedValueLabel, constraints);
		
		windSpeedUnitsLabel = new JLabel(METERS_PER_SEC_UNITS_LABEL_TEXT);
		windPanel.add(windSpeedUnitsLabel, constraints);
		
		windSpeedQuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		questionMarkLabels.add(windSpeedQuestionableLabel);
		windPanel.add(windSpeedQuestionableLabel, constraints);
		
		logger.exiting("ArrayWeatherPanel", "createWindSpeedPanel", windPanel);
		return windPanel;
	}
	
	private JPanel createWindDirectionPanel()
	{
		logger.entering("ArrayWeatherPanel", "createWindDirectionPanel");
		JPanel windPanel = new JPanel(new GridBagLayout());

		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.ipadx = 5;
		constraints.ipady = 0;
		constraints.gridx = GridBagConstraints.RELATIVE;
		constraints.gridy = 0;
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;

		windDirectionTextLabel = new JLabel(WIND_DIRECTION_LABEL_TEXT);
		windPanel.add(windDirectionTextLabel, constraints);
		
		windDirectionValueLabel = new JLabel("00" + DEGREE_SIGN_ASCII_TEXT);
		windPanel.add(windDirectionValueLabel, constraints);
		
		windDirectionQuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		questionMarkLabels.add(windDirectionQuestionableLabel);
		windPanel.add(windDirectionQuestionableLabel, constraints);
			
		logger.entering("ArrayWeatherPanel", "createWindDirectionPanel", windPanel);
		return windPanel;
	}
	
	private JPanel createWindSpeedAndDirPanel()
	{
		logger.entering("ArrayWeatherPanel", "createWindSpeedAndDirPanel");
		JPanel retPanel = new JPanel(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.ipadx = 5;
		constraints.ipady = 0;
		constraints.gridx = GridBagConstraints.RELATIVE;
		constraints.gridy = 0;
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		
		retPanel.add(createWindSpeedPanel(), constraints);
		retPanel.add(createWindDirectionPanel(), constraints);
		
		logger.exiting("ArrayWeatherPanel", "createWindSpeedAndDirPanel", retPanel);
		return retPanel;
	}
	
	//	 for testing only
	private static void createAndShowGUI()
	{
		JFrame mainFrame = new JFrame();
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.add(new ArrayWeatherPanel(Logger.getLogger("test logger")));
		mainFrame.pack();
		mainFrame.setVisible(true);
	}
	
	/**
	 * @param args
	 * for testing only
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() { ArrayWeatherPanel.createAndShowGUI();}
		});
	}
}
