package alma.control.gui.array;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import alma.Control.ArrayPointingData;
import alma.Control.gui.hardwaredevice.common.util.FormattedDouble;
import alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel;

/**
 * Panel for pointing information within the array summary plugin.
 * @author Steve Harrington
 *
 */
@SuppressWarnings("serial")
public class ArrayPointingPanel extends ArrayStatusBasePanel
{
	private static final String ELERR_LABEL_TEXT = "ELerr max:";
	private static final String AZERR_LABEL_TEXT = "AZerr max:";
	private static final String EL_LABEL_TEXT = "El:";
	private static final String AZ_LABEL_TEXT = "Az:";
	private static final String RA_LABEL_TEXT = "RA:";
	private static final String ASEC_UNITS_LABEL_TEXT = "asec";
	private static final String OFFSET_LABEL_TEXT = "offset:";
	private static final String DEC_LABEL_TEXT = "DEC:";
	private static final String POINTING_PANEL_TITLE = "Pointing";
	
	private ArrayList<QuestionMarkLabel> questionMarkLabels = new ArrayList<QuestionMarkLabel>();
	
	private JLabel raTextLabel;
	private JLabel raValueLabel;
	private QuestionMarkLabel raQuestionableLabel;
	
	private JLabel decTextLabel;
	private JLabel decValueLabel;
	private QuestionMarkLabel decQuestionableLabel;
	
	private JLabel raOffsetTextLabel;
	private JLabel raOffsetValueLabel;
	private QuestionMarkLabel raOffsetQuestionableLabel;
	private JLabel raOffsetUnitsLabel;
	
	private JLabel decOffsetTextLabel;
	private JLabel decOffsetValueLabel;
	private QuestionMarkLabel decOffsetQuestionableLabel;
	private JLabel decOffsetUnitsLabel;
	
	private JLabel cmdAzTextLabel;
	private JLabel cmdAzValueLabel;
	private QuestionMarkLabel cmdAzQuestionableLabel;
	
	private JLabel azOffsetTextLabel;
	private JLabel azOffsetValueLabel;
	private QuestionMarkLabel azOffsetQuestionableLabel;
	private JLabel azOffsetUnitsLabel;
	
	private JLabel azErrTextLabel;
	private JLabel azErrValueLabel;
	private QuestionMarkLabel azErrQuestionableLabel;
	private JLabel azErrUnitsLabel;
	
	private JLabel cmdElTextLabel;
	private JLabel cmdElValueLabel;
	private QuestionMarkLabel cmdElQuestionableLabel;

	private JLabel elOffsetTextLabel;
	private JLabel elOffsetValueLabel;
	private QuestionMarkLabel elOffsetQuestionableLabel;
	private JLabel elOffsetUnitsLabel;
	
	private JLabel elErrTextLabel;
	private JLabel elErrValueLabel;
	private QuestionMarkLabel elErrQuestionableLabel;
	private JLabel elErrUnitsLabel;
	
	/**
	 * Constructor.
	 * @param logger the logger to use for informational/debug messages
	 */
	public ArrayPointingPanel(Logger logger)
	{
		super(logger);
		logger.entering("ArrayPointingPanel", "constructor");
		initialize();
		logger.exiting("ArrayPointingPanel", "constructor");
	}
	
	public void communicationEstablished(CommunicationEstablishedListener.Aspect aspect, Object dataPayload) 
	{
		logger.entering("ArrayPointingPanel", "communicationEstablished");
		if(aspect == CommunicationEstablishedListener.Aspect.POINTING) 
		{
			this.communicationEstablished = true;
			MiscUtilities.updateQuestionMarkLabels(questionMarkLabels, communicationEstablished);
			if(dataPayload != null && dataPayload instanceof ArrayPointingData)
			{
				ArrayPointingData pointingData = (ArrayPointingData)dataPayload;
				
				this.azErrValueLabel.setText(new FormattedDouble(pointingData.maxAzErr, "%9.3f").toString().trim());
				this.elErrValueLabel.setText(new FormattedDouble(pointingData.maxElErr, "%9.3f").toString().trim());
				
				this.elOffsetValueLabel.setText(new FormattedDouble(pointingData.elOffset, "%9.3f").toString().trim());
				this.azOffsetValueLabel.setText(new FormattedDouble(pointingData.azOffset, "%9.3f").toString().trim());
				this.raOffsetValueLabel.setText(new FormattedDouble(pointingData.raOffset, "%9.3f").toString().trim());
				this.decOffsetValueLabel.setText(new FormattedDouble(pointingData.decOffset, "%9.3f").toString().trim());
				
				this.cmdAzValueLabel.setText(convertRadiansToDegreesMinutesSecondsString(pointingData.az, "%2.2f"));
				this.cmdElValueLabel.setText(convertRadiansToDegreesMinutesSecondsString(pointingData.el, "%2.2f"));
				this.raValueLabel.setText(convertRadiansToDegreesMinutesSecondsString(pointingData.ra, "%2.4f"));
				this.decValueLabel.setText(convertRadiansToDegreesMinutesSecondsString(pointingData.dec, "%2.4f"));
				setForegroundTextOnValueLabels(null);
			}
		}
		logger.exiting("ArrayPointingPanel", "communicationEstablished");
	}

	public void communicationLost(CommunicationEstablishedListener.Aspect aspect) 
	{
		logger.entering("ArrayPointingPanel", "communicationLost");
		if(aspect == CommunicationEstablishedListener.Aspect.POINTING) {
			this.communicationEstablished = false;
			MiscUtilities.updateQuestionMarkLabels(questionMarkLabels, communicationEstablished);
		}
		setForegroundTextOnValueLabels(ArrayStatusPlugin.STALE_DATA_COLOR);
		
		logger.exiting("ArrayPointingPanel", "communicationLost");
	}

	/**
	 * 
	 */
	private void setForegroundTextOnValueLabels(Color color) 
	{
		this.azErrValueLabel.setForeground(color);
		this.azOffsetValueLabel.setForeground(color);
		this.cmdAzValueLabel.setForeground(color);
		this.cmdElValueLabel.setForeground(color);
		this.decOffsetValueLabel.setForeground(color);
		this.decValueLabel.setForeground(color);
		this.raValueLabel.setForeground(color);
		this.raOffsetValueLabel.setForeground(color);
		this.elErrValueLabel.setForeground(color);
		this.elOffsetValueLabel.setForeground(color);
	}
	
	private String convertRadiansToDegreesMinutesSecondsString(double valueInRadians, String precisionString)
	{
		StringBuffer retBuffer = new StringBuffer();
		
		double valueInDegrees = Math.toDegrees(valueInRadians);
		
		// first, get the degree portion of the angle
		int degrees = (int)Math.floor(valueInDegrees);
		if(degrees < 100 && degrees > 10) 
		{
			// special case to prepend a zero for values less than 3 digits
			retBuffer.append("0");
		}
		else if(degrees < 10) 
		{
			// special case to prepend a zero for values less than 2 digits
			retBuffer.append("00");
		}
		retBuffer.append(Integer.toString(degrees));
		retBuffer.append(":");
		
		// second, get the minutes portion of the angle
		double minutesDouble = (valueInDegrees - (double)degrees) * 60;
		int minutes = (int)Math.floor(minutesDouble);
		if(minutes < 10) 
		{
			// special case to prepend a zero for values less than 2 digits
			retBuffer.append("0");
		}
		retBuffer.append(Integer.toString(minutes));
		retBuffer.append(":");
		
		// third, get the seconds portion of the angle
		double seconds = (minutesDouble - (double)minutes);
		if(seconds < 10.0)
		{
			// special case to prepend a zero for values less than 2 digits
			retBuffer.append("0");
		}
		retBuffer.append(new FormattedDouble(seconds, precisionString).toString());
		return retBuffer.toString();
	}
	
	private void initialize()
	{
		logger.entering("ArrayPointingPanel", "initialize");
		this.setLayout(new GridBagLayout());
		this.setBorder(BorderFactory.createTitledBorder(POINTING_PANEL_TITLE));
	
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.ipadx = 0;
		constraints.gridx = GridBagConstraints.RELATIVE;
		constraints.gridy = 0;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.weightx = 1.0;

		this.add(createCommandedPositionAndOffsetPanel(), constraints);
	
		constraints.gridy++;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		constraints.insets = new Insets(10, 0, 0, 0);

		this.add(createAzAndElErrPanel(), constraints);
		logger.exiting("ArrayPointingPanel", "initialize");
	}
	
	private JPanel createAzAndElErrPanel()
	{
		logger.entering("ArrayPointingPanel", "createAzAndElErrPanel");
		JPanel retPanel = new JPanel(new GridBagLayout());

		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.ipadx = 0;
		constraints.gridx = GridBagConstraints.RELATIVE;
		constraints.gridy = 0;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.weightx = 0.0;
		constraints.ipadx = 5;
		
		// AzErr
		azErrTextLabel = new JLabel(AZERR_LABEL_TEXT);
		retPanel.add(azErrTextLabel, constraints);
		
		azErrValueLabel = new JLabel("000.000");
		retPanel.add(azErrValueLabel, constraints);
		
		azErrUnitsLabel = new JLabel(ASEC_UNITS_LABEL_TEXT);
		retPanel.add(azErrUnitsLabel, constraints);

		constraints.weightx = 1.0;
		azErrQuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		questionMarkLabels.add(azErrQuestionableLabel);
		retPanel.add(azErrQuestionableLabel, constraints);
		
		constraints.gridy++;
		constraints.weightx = 0.0;
		constraints.weighty = 1.0;
		
		// ElErr
		elErrTextLabel = new JLabel(ELERR_LABEL_TEXT);
		retPanel.add(elErrTextLabel, constraints);
		
		elErrValueLabel = new JLabel("000.000");
		retPanel.add(elErrValueLabel, constraints);
		
		elErrUnitsLabel = new JLabel(ASEC_UNITS_LABEL_TEXT);
		retPanel.add(elErrUnitsLabel, constraints);

		constraints.weightx = 1.0;
		elErrQuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		questionMarkLabels.add(elErrQuestionableLabel);
		retPanel.add(elErrQuestionableLabel, constraints);
		
		logger.exiting("ArrayPointingPanel", "communicationLost", retPanel);
		return retPanel;
	}

	private JPanel createCommandedPositionAndOffsetPanel() 
	{
		logger.entering("ArrayPointingPanel", "createCommandedPositionAndOffsetPanel");
		JPanel retPanel = new JPanel(new GridBagLayout());
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.gridx = GridBagConstraints.RELATIVE;
		constraints.gridy = 0;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.weightx = 0.0;
		constraints.ipadx = 5;
		
		raTextLabel = new JLabel(RA_LABEL_TEXT);
		retPanel.add(raTextLabel, constraints);
		raValueLabel = new JLabel("00:00:00.0000");
		retPanel.add(raValueLabel, constraints);
		raQuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		questionMarkLabels.add(raQuestionableLabel);
		retPanel.add(raQuestionableLabel, constraints);
		constraints.insets = new Insets(0, 10, 0, 0);
		raOffsetTextLabel = new JLabel(OFFSET_LABEL_TEXT);
		retPanel.add(raOffsetTextLabel, constraints);
		constraints.insets = new Insets(0, 00, 0, 0);
		raOffsetValueLabel = new JLabel("000.000");
		retPanel.add(raOffsetValueLabel, constraints);
		raOffsetUnitsLabel = new JLabel(ASEC_UNITS_LABEL_TEXT);
		retPanel.add(raOffsetUnitsLabel, constraints);
		constraints.weightx = 1.0;
		raOffsetQuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		questionMarkLabels.add(raOffsetQuestionableLabel);
		retPanel.add(raOffsetQuestionableLabel, constraints);
		
		constraints.gridy++;
		constraints.weightx = 0.0;
		
		decTextLabel = new JLabel(DEC_LABEL_TEXT);
		retPanel.add(decTextLabel, constraints);
		decValueLabel = new JLabel("00:00:00.0000");
		retPanel.add(decValueLabel, constraints);
		decQuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		questionMarkLabels.add(decQuestionableLabel);
		retPanel.add(decQuestionableLabel, constraints);
		constraints.insets = new Insets(0, 10, 0, 0);
		decOffsetTextLabel = new JLabel(OFFSET_LABEL_TEXT);
		retPanel.add(decOffsetTextLabel, constraints);
		constraints.insets = new Insets(0, 0, 0, 0);
		decOffsetValueLabel = new JLabel("000.000");
		retPanel.add(decOffsetValueLabel, constraints);
		decOffsetUnitsLabel = new JLabel(ASEC_UNITS_LABEL_TEXT);
		retPanel.add(decOffsetUnitsLabel, constraints);
		constraints.weightx = 1.0;
		decOffsetQuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		questionMarkLabels.add(decOffsetQuestionableLabel);
		retPanel.add(decOffsetQuestionableLabel, constraints);
		
		constraints.gridy++;
		constraints.weightx = 0.0;

		cmdAzTextLabel = new JLabel(AZ_LABEL_TEXT);
		retPanel.add(cmdAzTextLabel, constraints);
		cmdAzValueLabel = new JLabel("00:00:00.00");
		retPanel.add(cmdAzValueLabel, constraints);
		cmdAzQuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		questionMarkLabels.add(cmdAzQuestionableLabel);
		retPanel.add(cmdAzQuestionableLabel, constraints);
		constraints.insets = new Insets(0, 10, 0, 0);
		azOffsetTextLabel = new JLabel(OFFSET_LABEL_TEXT);
		retPanel.add(azOffsetTextLabel, constraints);
		constraints.insets = new Insets(0, 0, 0, 0);
		azOffsetValueLabel = new JLabel("000.000");
		retPanel.add(azOffsetValueLabel, constraints);
		azOffsetUnitsLabel = new JLabel(ASEC_UNITS_LABEL_TEXT);
		retPanel.add(azOffsetUnitsLabel, constraints);
		constraints.weightx = 1.0;
		azOffsetQuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		questionMarkLabels.add(azOffsetQuestionableLabel);
		retPanel.add(azOffsetQuestionableLabel, constraints);
		
		constraints.gridy++;
		constraints.weightx = 0.0;
		constraints.insets = new Insets(0, 0, 0, 0);
		
		cmdElTextLabel = new JLabel(EL_LABEL_TEXT);
		retPanel.add(cmdElTextLabel, constraints);
		cmdElValueLabel = new JLabel("00:00:00.00");
		retPanel.add(cmdElValueLabel, constraints);
		cmdElQuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		questionMarkLabels.add(cmdElQuestionableLabel);
		retPanel.add(cmdElQuestionableLabel, constraints);
		constraints.insets = new Insets(0, 10, 0, 0);
		elOffsetTextLabel = new JLabel(OFFSET_LABEL_TEXT);
		retPanel.add(elOffsetTextLabel, constraints);
		constraints.insets = new Insets(0, 0, 0, 0);
		elOffsetValueLabel = new JLabel("000.000");
		retPanel.add(elOffsetValueLabel, constraints);
		elOffsetUnitsLabel = new JLabel(ASEC_UNITS_LABEL_TEXT);
		retPanel.add(elOffsetUnitsLabel, constraints);
		constraints.weightx = 1.0;
		elOffsetQuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		questionMarkLabels.add(elOffsetQuestionableLabel);
		retPanel.add(elOffsetQuestionableLabel, constraints);
		
		logger.exiting("ArrayPointingPanel", "createCommandedPositionAndOffsetPanel", retPanel);
		return retPanel;
	}
	
	//	 for testing only
	private static void createAndShowGUI()
	{
		JFrame mainFrame = new JFrame();
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.add(new ArrayPointingPanel(Logger.getLogger("test logger")));
		mainFrame.pack();
		mainFrame.setVisible(true);
	}
	
	/**
	 * @param args
	 * for testing only
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() { ArrayPointingPanel.createAndShowGUI();}
		});
	}
}
