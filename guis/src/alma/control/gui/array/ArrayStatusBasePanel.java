package alma.control.gui.array;

import java.util.logging.Logger;

import javax.swing.JPanel;

/**
 * Abstract base class for all panels used by the array status plugin.
 * @author Steve Harrington
 */
public abstract class ArrayStatusBasePanel extends JPanel implements CommunicationEstablishedListener 
{
	boolean communicationEstablished = false;
	Logger logger;
	
	/**
	 * Constructor
	 * @param logger the logger to use for logging error/info/warning/debug/etc messages.
	 */
	public ArrayStatusBasePanel(Logger logger)
	{
		logger.entering("ArrayStatusBasePanel", "constructor");
		this.logger = logger;
		logger.exiting("ArrayStatusBasePanel", "constructor");
	}
	
	void setLogger(Logger logger)
	{
		logger.entering("ArrayStatusBasePanel", "setLogger");
		this.logger = logger;
		logger.exiting("ArrayStatusBasePanel", "setLogger");
	}
	
	/**
	 * @param publisher the object which will inform us if communications are established and/or lost; used for placing
	 * a visual indicator such as a question mark next to data that is unreliable.
	 */
	void configureListening(CommunicationEstablishedPublisher publisher)
	{
		publisher.addCommunicationEstablishedListener(this);
	}
}
