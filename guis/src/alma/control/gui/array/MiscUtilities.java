package alma.control.gui.array;

import java.util.ArrayList;

import alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel;

/**
 * Miscellaneous utilities useful in the array status plugin.
 * @author Steve Harrington
 */
public class MiscUtilities 
{
    /**
     * Updates question mark labels depending on whether communication is established or not;
     * for example, adds question marks when no communication is established and removes the 
     * question marks if communication has been established.
     * 
     * @param questionMarkLabels an array list of question mark labels to be updated
     * @param communicationEstablished boolean indicating whether communication is established or not.
     */
    public static void updateQuestionMarkLabels(ArrayList<QuestionMarkLabel> questionMarkLabels, boolean communicationEstablished)
	{
		for(QuestionMarkLabel label : questionMarkLabels)
		{
			label.setQuestionMarkShown(!communicationEstablished);
		}
	}


}
