package alma.control.gui.array;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;

import alma.Control.ArrayReceiverData;

/**
 * Panel for weather and receiver band information.
 * @author Steve Harrington
 */
@SuppressWarnings("serial")
public class ArrayReceiverPanel extends ArrayStatusBasePanel
{
	private static final String VALUE_TABLE_HEADER_TEXT = "Band#";
	private static final String BAND_TABLE_HEADER_TEXT = "State";
	private static final String RECEIVER_PANEL_TITLE = "Receiver";

	private static final String[] columnNames = 
	{ 
		BAND_TABLE_HEADER_TEXT, 
		VALUE_TABLE_HEADER_TEXT 
	};
	
	private String[][] data = 
	{
			{"Current", "1"}, 
			{"Standby", "2"}, 
			{"Standby", "3"}
	};
	
	private static final int[] questionableColumns = { 1 };
	
	private JTable receiverTable;
	private DefaultTableCellRenderer coloredRenderer;
	
	/**
	 * Constructor.
	 * @param logger the logger to use for informational/debug messages.
	 */
	public ArrayReceiverPanel(Logger logger)
	{
		super(logger);
		logger.entering("ArrayReceiverPanel", "constructor");
		initialize();
		logger.exiting("ArrayReceiverPanel", "constructor");
	}

	public void communicationEstablished(CommunicationEstablishedListener.Aspect aspect, Object dataPayload) 
	{
		logger.entering("ArrayReceiverPanel", "communicationEstablished");
		if(aspect == CommunicationEstablishedListener.Aspect.RECEIVER) 
		{
			this.communicationEstablished = true;
			updateQuestionMarks();
			if(dataPayload != null && dataPayload instanceof ArrayReceiverData)
			{
				ArrayReceiverData receiverData = (ArrayReceiverData)dataPayload;
				this.data[0][1] = Short.toString(receiverData.currentBand);
				this.data[1][1] = Short.toString(receiverData.standbyBandOne);
				this.data[2][1] = Short.toString(receiverData.standbyBandTwo);
				((AbstractTableModel)receiverTable.getModel()).fireTableRowsUpdated(0, data.length);
				setForegroundColorOnValues(null);
			}
		}
	    logger.exiting("ArrayReceiverPanel", "communicationEstablished");
	}

	public void communicationLost(CommunicationEstablishedListener.Aspect aspect) 
	{
		logger.entering("ArrayReceiverPanel", "communicationLost");
		if(aspect == CommunicationEstablishedListener.Aspect.RECEIVER) {
			this.communicationEstablished = false;
			updateQuestionMarks();
			setForegroundColorOnValues(ArrayStatusPlugin.STALE_DATA_COLOR);
		}
	    logger.exiting("ArrayReceiverPanel", "communicationLost");
	}
	
	private void setForegroundColorOnValues(Color color)
	{
		coloredRenderer.setForeground(color);
	}
	
	private void updateQuestionMarks()
	{
		TableUtilities.addQuestionMarks(communicationEstablished, data, questionableColumns);
		for(int i = 0; i < data.length; i++)
        {
			for(int j = 0; j < questionableColumns.length; j++)
        	{
        		((AbstractTableModel)receiverTable.getModel()).fireTableCellUpdated(i, questionableColumns[j]);

        	}
        }	
	}
	
	private void initialize()
	{
		logger.entering("ArrayReceiverPanel", "initialize");
		this.setLayout(new GridBagLayout());
		this.setBorder(BorderFactory.createTitledBorder(RECEIVER_PANEL_TITLE));
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.ipadx = 10;
		constraints.ipady = 10;
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		constraints.insets = new Insets(0,10,0,10);
		
		this.add(createReceiverPanel(), constraints);
		
		constraints.gridy++;
		logger.exiting("ArrayReceiverPanel", "initialize");
	}
	
	private JPanel createReceiverPanel()
	{
		logger.entering("ArrayReceiverPanel", "createReceiverPanel");
		JPanel receiverTablePanel = new JPanel(new GridBagLayout());
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.ipadx = 10;
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
	
		receiverTable = new JTable(data, columnNames);
		coloredRenderer = new DefaultTableCellRenderer();
		receiverTable.getColumn(VALUE_TABLE_HEADER_TEXT).setCellRenderer(coloredRenderer);
		receiverTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		receiverTable.setShowGrid(true);
		receiverTable.getTableHeader().setReorderingAllowed(false);
		receiverTable.setCellSelectionEnabled(false);
		
		JScrollPane scrollPane = new JScrollPane(receiverTable);
		
		// fix the size assuming question marks; then set the question marks to the correct state
		TableUtilities.addQuestionMarks(true, data, questionableColumns);
		TableUtilities.autoResizeTable(receiverTable, true, 10);
		TableUtilities.addQuestionMarks(communicationEstablished, data, questionableColumns);

		receiverTable.setPreferredScrollableViewportSize(receiverTable.getPreferredSize());
		TableUtilities.setVisibleRowCount(receiverTable, data.length);
		scrollPane.setMinimumSize(scrollPane.getPreferredSize());
		
		receiverTablePanel.add(scrollPane, constraints);
		
		logger.exiting("ArrayReceiverPanel", "createReceiverPanel", receiverTablePanel);
		return receiverTablePanel;
	}
	
	//	 for testing only
	private static void createAndShowGUI()
	{
		JFrame mainFrame = new JFrame();
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.add(new ArrayReceiverPanel(Logger.getLogger("test logger")));
		mainFrame.pack();
		mainFrame.setVisible(true);
	}
	
	/**
	 * @param args
	 * for testing only
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() { ArrayReceiverPanel.createAndShowGUI();}
		});
	}
}
