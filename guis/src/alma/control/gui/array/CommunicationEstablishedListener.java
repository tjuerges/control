package alma.control.gui.array;

/**
 * Interface used to inform panels when communication has been established or lost;
 * this is used by the GUI to display, for example, question marks for data that
 * is 'stale'.
 * 
 * @author Steve Harrington
 */
public interface CommunicationEstablishedListener 
{
	/**
	 * Denotes the aspect of the array status information which has lost communication.
	 * @author Steve Harrington
	 */
	enum Aspect 
	{ 
		/**
		 * Summary information gained and/or lost communication.
		 */
		SUMMARY,
		
		/**
		 * Observing (i.e. scheduling) information gained and/or lost communication.
		 */
		OBSERVING,
		
		/**
		 * Weather information gained and/or lost communication.
		 */
		WEATHER,
		
		/**
		 * Receiver information gained and/or lost communication.
		 */
		RECEIVER,
		
		/**
		 * Correlator information gained and/or lost communication.
		 */
		CORRELATOR,
		
		/**
		 * Pointing information gained and/or lost communication.
		 */
		POINTING 
	};
	
	/**
	 * Called on the listener when communication is established.
	 * @param aspect the aspect which has gained communication.
	 * @param data the data payload; this must be cast to the proper type.
	 * This is not the ideal implementation from an OO design angle, in that
	 * we are requiring a cast and losing some type checking. However, the alternative
	 * was an explosion in the number of event types and listener types; it was a
	 * conscious trade-off design decision to go this route. We may reevaluate if
	 * it becomes a maintenance problem in the future.
	 */
	public void communicationEstablished(Aspect aspect, Object data);
	
	/**
	 * Called on the listener when communication has been lost.
	 * @param aspect the aspect which has lost communication.
	 */
	public void communicationLost(Aspect aspect);
}
