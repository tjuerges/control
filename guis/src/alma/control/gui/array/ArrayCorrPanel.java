package alma.control.gui.array;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;

import alma.Control.ArrayCorrelatorData;
import alma.Control.Baseband;
import alma.Control.gui.hardwaredevice.common.util.FormattedFloat;
import alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel;

/**
 * Sub-panel for array status plugin; contains information related to the observing mode of the array.
 * @author Steve Harrington
 *
 */
@SuppressWarnings("serial")
public class ArrayCorrPanel extends ArrayStatusBasePanel
{	
	private static final String LO2_FREQ_TABLE_COLUMN_TITLE = "f-LO2";
	private static final String FSKY_TABLE_COLUMN_TITLE = "f-sky";
	private static final String SB_TABLE_COLUMN_TITLE = "SB";
	private static final String BB_TABLE_COLUMN_TITLE = "BB-pr";
	private static final String CORRELATOR_PANEL_TITLE = "LO Config";
	private static final String GHZ_UNITS_LABEL_TEXT = "GHz";
	private static final String REFERENCE_FREQUENCY_LABEL_TEXT = "Reference frequency:";
	private static final String FIRST_LO_FREQUENCY_LABEL_TEXT = "First LO frequency:";
	private static final CommunicationEstablishedListener.Aspect CORR_ASPECT = CommunicationEstablishedListener.Aspect.CORRELATOR;
	private DefaultTableCellRenderer coloredRenderer = new DefaultTableCellRenderer();
	
	private static final String[] corrTableColumnNames = 
	{ 
		BB_TABLE_COLUMN_TITLE, 
		FSKY_TABLE_COLUMN_TITLE, 
		LO2_FREQ_TABLE_COLUMN_TITLE,
		SB_TABLE_COLUMN_TITLE
	};

	private static final int[] questionableColumns = { 1, 2 };

	private String[][] corrTableData = 
	{
			{ "1", "000.000 GHz", "000.000 GHz", "N/A" }, 
			{ "2", "000.000 GHz", "000.000 GHz", "N/A"},
			{ "3", "000.000 GHz", "000.000 GHz", "N/A" }, 
			{ "4", "000.000 GHz", "000.000 GHz", "N/A"}
	};
	
	private JTable corrTable;
	
	// reference frequency related gui components
	private JLabel referenceFrequencyTextLabel;
	private JLabel referenceFrequencyValueLabel;
	private QuestionMarkLabel referenceFrequencyQuestionableLabel;
	private JLabel referenceFrequencyUnitsLabel;
	
	// first LO frequency related gui components
	private JLabel firstLoFrequencyTextLabel;
	private JLabel firstLoFrequencyValueLabel;
	private QuestionMarkLabel firstLoFrequencyQuestionableLabel;
	private JLabel firstLoFrequencyUnitsLabel;

	/**
	 * Constructor.
	 * @param logger the logger to use for logging messages.
	 */
	public ArrayCorrPanel(Logger logger)
	{
		super(logger);
		logger.entering("ArrayCorrPanel", "constructor");
		initialize();
		logger.exiting("ArrayCorrPanel", "constructor");
	}
	
	public void communicationEstablished(CommunicationEstablishedListener.Aspect aspect, Object dataPayload) 
	{
		logger.entering("ArrayCorrPanel", "communicationEstablished");
		if(aspect == CORR_ASPECT) 
		{
			this.communicationEstablished = true;
			updateQuestionMarks();
			if(dataPayload != null && dataPayload instanceof ArrayCorrelatorData) 
			{
				ArrayCorrelatorData corrData = (ArrayCorrelatorData)dataPayload;
				for(int i = 0; i < corrTableData.length; i++)
				{
					Baseband baseband = null;
					switch(i) {
						case 0: 
							baseband = corrData.basebandOne;
							break;
						case 1:
							baseband = corrData.basebandTwo;
							break;
						case 2:
							baseband = corrData.basebandThree;
							break;
						case 3: 
							baseband = corrData.basebandFour;
							break;
					}
					
					this.corrTableData[i][0] = Short.toString(baseband.baseBandNumber);
					this.corrTableData[i][1] = new FormattedFloat(baseband.skyFrequency, "%9.3f").toString().trim() + " GHz";
					this.corrTableData[i][2] = new FormattedFloat(baseband.lo2Frequency, "%9.3f").toString().trim() + " GHz";
					this.corrTableData[i][3] = baseband.sideBand.toString();		
					((AbstractTableModel)corrTable.getModel()).fireTableRowsUpdated(0, corrTableData.length);
				}
				this.referenceFrequencyValueLabel.setText(new FormattedFloat(corrData.referenceFrequency, "%9.3f").toString().trim());
				this.firstLoFrequencyValueLabel.setText(new FormattedFloat(corrData.firstLoFrequency, "%9.3f").toString().trim());
				setColors(null);
			}
		}
        logger.exiting("ArrayCorrPanel", "communicationEstablished");
	}

	public void communicationLost(CommunicationEstablishedListener.Aspect aspect) 
	{
		logger.entering("ArrayCorrPanel", "communicationLost");
		if(aspect == CORR_ASPECT) {
			this.communicationEstablished = false;
			updateQuestionMarks();
			setColors(ArrayStatusPlugin.STALE_DATA_COLOR);
		}
        logger.exiting("ArrayCorrPanel", "communicationLost");
	}
	
	private void setColors(Color color)
	{
		this.firstLoFrequencyValueLabel.setForeground(color);
		this.referenceFrequencyValueLabel.setForeground(color);
		this.coloredRenderer.setForeground(color);
		coloredRenderer.setForeground(color);
	}
	
	private void updateQuestionMarks()
	{
		logger.entering("ArrayCorrPanel", "updateQuestionMarks");
		referenceFrequencyQuestionableLabel.setQuestionMarkShown(!communicationEstablished);
		firstLoFrequencyQuestionableLabel.setQuestionMarkShown(!communicationEstablished);
        TableUtilities.addQuestionMarks(communicationEstablished, corrTableData, questionableColumns);
        for(int i = 0; i < corrTableData.length; i++)
        {
        	for(int j = 0; j < questionableColumns.length; j++)
        	{
        		((AbstractTableModel)corrTable.getModel()).fireTableCellUpdated(i, questionableColumns[j]);
        	}
        }
        logger.exiting("ArrayCorrPanel", "updateQuestionMarks");
	}
	
	private void initialize()
	{
		logger.entering("ArrayCorrPanel", "initialize");
		this.setLayout(new GridBagLayout());
		this.setBorder(BorderFactory.createTitledBorder(CORRELATOR_PANEL_TITLE));
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.ipadx = 10;
		constraints.ipady = 0;
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.weightx = 1.0;
		constraints.weighty = 0.0;

		this.add(createReferenceFrequencyPanel(), constraints);
		
		constraints.ipady = 10;
		constraints.gridy++;
		this.add(createFirstLoFrequencyPanel(), constraints);
		
		constraints.gridy++;
		constraints.weighty = 1.0;
		constraints.ipady = 0;
		this.add(createCorrTablePanel(), constraints);
		
		logger.exiting("ArrayCorrPanel", "initialize");
	}
	
	private JPanel createReferenceFrequencyPanel()
	{
		logger.entering("ArrayCorrPanel", "createReferenceFrequencyPanel");
		JPanel retPanel = new JPanel(new GridBagLayout());
		GridBagConstraints innerConstraints = new GridBagConstraints();
		innerConstraints.fill = GridBagConstraints.NONE;
		innerConstraints.anchor = GridBagConstraints.NORTHWEST;
		innerConstraints.ipadx = 0;
		innerConstraints.insets = new Insets(0, 10, 0, 0);
		
		referenceFrequencyTextLabel = new JLabel(REFERENCE_FREQUENCY_LABEL_TEXT);
		retPanel.add(referenceFrequencyTextLabel, innerConstraints);

		innerConstraints.insets = new Insets(0, 5, 0, 0);
		
		referenceFrequencyValueLabel = new JLabel("123.456");
		retPanel.add(referenceFrequencyValueLabel, innerConstraints);
		
		referenceFrequencyUnitsLabel = new JLabel(GHZ_UNITS_LABEL_TEXT);
		retPanel.add(referenceFrequencyUnitsLabel, innerConstraints);

		innerConstraints.weightx = 1.0;
		innerConstraints.weighty = 1.0;
		referenceFrequencyQuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		retPanel.add(referenceFrequencyQuestionableLabel, innerConstraints);

		logger.exiting("ArrayCorrPanel", "createReferenceFrequencyPanel", retPanel);
		return retPanel;
	}

	private JPanel createFirstLoFrequencyPanel()
	{
		logger.entering("ArrayCorrPanel", "createFirstLoFrequencyPanel");
		JPanel retPanel = new JPanel(new GridBagLayout());
		GridBagConstraints innerConstraints = new GridBagConstraints();
		innerConstraints.fill = GridBagConstraints.NONE;
		innerConstraints.anchor = GridBagConstraints.NORTHWEST;
		innerConstraints.ipadx = 0;
		innerConstraints.insets = new Insets(0, 10, 0, 0);
		
		firstLoFrequencyTextLabel = new JLabel(FIRST_LO_FREQUENCY_LABEL_TEXT);
		retPanel.add(firstLoFrequencyTextLabel, innerConstraints);

		innerConstraints.insets = new Insets(0, 5, 0, 0);
		
		firstLoFrequencyValueLabel = new JLabel("123.456");
		retPanel.add(firstLoFrequencyValueLabel, innerConstraints);
		
		firstLoFrequencyUnitsLabel = new JLabel(GHZ_UNITS_LABEL_TEXT);
		retPanel.add(firstLoFrequencyUnitsLabel, innerConstraints);

		innerConstraints.weightx = 1.0;
		innerConstraints.weighty = 1.0;
		firstLoFrequencyQuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		retPanel.add(firstLoFrequencyQuestionableLabel, innerConstraints);

		logger.exiting("ArrayCorrPanel", "createFirstLoFrequencyPanel", retPanel);
		return retPanel;
	}

	private JPanel createCorrTablePanel()
	{
		logger.entering("ArrayCorrPanel", "createCorrTablePanel");
		JPanel corrTablePanel = new JPanel(new GridBagLayout());
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.ipadx = 0;
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		constraints.insets = new Insets(0, 10, 0, 10);
		
		corrTable = new JTable(corrTableData, corrTableColumnNames);
		corrTable.getColumn(FSKY_TABLE_COLUMN_TITLE).setCellRenderer(coloredRenderer);
		corrTable.getColumn(LO2_FREQ_TABLE_COLUMN_TITLE).setCellRenderer(coloredRenderer);
		corrTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		corrTable.setShowGrid(true);
		corrTable.getTableHeader().setReorderingAllowed(false);
		corrTable.setCellSelectionEnabled(false);
		
		JScrollPane scrollPane = new JScrollPane(corrTable);
		
		// fix the size assuming question marks; then set the question marks to the correct state
		TableUtilities.addQuestionMarks(true, corrTableData, questionableColumns);
		TableUtilities.autoResizeTable(corrTable, true, 10);
		TableUtilities.addQuestionMarks(communicationEstablished, corrTableData, questionableColumns);

		corrTable.setPreferredScrollableViewportSize(corrTable.getPreferredSize());
		TableUtilities.setVisibleRowCount(corrTable, corrTableData.length);
		scrollPane.setMinimumSize(scrollPane.getPreferredSize());
		
		corrTablePanel.add(scrollPane, constraints);
		
		logger.exiting("ArrayCorrPanel", "createCorrTablePanel", corrTablePanel);
		return corrTablePanel;
	}
		
	//	 for testing only
	private static void createAndShowGUI()
	{
		JFrame mainFrame = new JFrame();
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.add(new ArrayCorrPanel(Logger.getLogger("test logger")));
		mainFrame.pack();
		mainFrame.setVisible(true);
	}
	
	/**
	 * @param args
	 * for testing only
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() { ArrayCorrPanel.createAndShowGUI();}
		});
	}
}
