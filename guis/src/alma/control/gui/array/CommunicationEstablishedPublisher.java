package alma.control.gui.array;

/**
 * Classes which wish to publish notifications about communications established/lost
 * should implement this interface. This is frequently used in the GUI to display
 * a visual indicator of some sort (e.g. a question mark) next to data that is
 * either stale or otherwise unreliable.
 * 
 * @author Steve Harrington
 */
public interface CommunicationEstablishedPublisher 
{
	/**
	 * Used to register a listener for updates to the status of communications being established and/or lost.
	 * @param listener the listener which wishes to be notified when communications are established and/or lost.
	 */
	public void addCommunicationEstablishedListener(CommunicationEstablishedListener listener);
	
	/**
	 * Used to remove a listener that no longer wishes to be notified when communications are established and/or lost.
	 * @param listener the listener to remove.
	 */
	public void removeCommunicationEstablishedListener(CommunicationEstablishedListener listener);
}
