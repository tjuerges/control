package alma.control.gui.array;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import alma.Control.ArrayObservingData;
import alma.Control.gui.hardwaredevice.common.util.FormattedFloat;
import alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel;

/**
 * Sub-panel for array status plugin; contains information related to the observing mode of the array.
 * @author Steve Harrington
 *
 */
@SuppressWarnings("serial")
public class ArraySchedulingInfoPanel extends ArrayStatusBasePanel
{

	private static final String SB_LABEL_TEXT = "SB: ";
	private static final String SCHED_MODE_LABEL_TEXT = "Sched mode:";
	private static final String SB_STIMATED_TIME_LABEL_TEXT = "SB estimated time:";
	private static final String SB_ELAPSED_TIME_LABEL_TEXT =  "SB elapsed time:";
	private static final String MIN_UNITS_LABEL_TEXT = "min";
	private static final String SCAN_NUMBER_LABEL_TEXT = "Scan# ";
	private static final String OBSERVING_PANEL_TITLE = "Observing";
	
	private ArrayList<QuestionMarkLabel> questionMarkLabels = new ArrayList<QuestionMarkLabel>();
	
	private JLabel schedModeTextLabel;
	private QuestionMarkLabel schedModeQuestionableLabel;
	private JLabel schedModeValueLabel;
	
	private JLabel schedBlockNameTextLabel;
	private QuestionMarkLabel schedBlockQuestionableLabel;
	private JLabel schedBlockNameValueLabel;
	
	private JLabel schedBlockElapsedTimeTextLabel;
	private JLabel schedBlockElapsedTimeValueLabel;
	private QuestionMarkLabel schedBlockElapsedTimeQuestionableLabel;
	private JLabel schedBlockElapsedTimeUnitsLabel;
	
	private JLabel schedBlockEstimatedTimeTextLabel;
	private JLabel schedBlockEstimatedTimeValueLabel;
	private QuestionMarkLabel schedBlockEstimatedTimeQuestionableLabel;
	private JLabel schedBlockEstimatedTimeUnitsLabel;
	
	private JLabel scanNumberTextLabel;
	private QuestionMarkLabel scanNumberQuestionableLabel;
	private JLabel scanNumberValueLabel;
	
	/**
	 * Constructor.
	 * @param logger the logger to use for informational/debug messages.
	 */
	public ArraySchedulingInfoPanel(Logger logger)
	{
        super(logger);
		logger.entering("ArraySchedulingInfoPanel", "constructor");
		initialize();
		logger.exiting("ArraySchedulingInfoPanel", "constructor");
	}
	
	public void communicationEstablished(CommunicationEstablishedListener.Aspect aspect, Object dataPayload) 
	{
		logger.entering("ArraySchedulingInfoPanel", "communicationEstablished");
		if(aspect == CommunicationEstablishedListener.Aspect.OBSERVING) 
		{
			this.communicationEstablished = true;
			MiscUtilities.updateQuestionMarkLabels(questionMarkLabels, communicationEstablished);
			if(dataPayload != null && dataPayload instanceof ArrayObservingData)
			{
				ArrayObservingData observingData = (ArrayObservingData)dataPayload;
				this.scanNumberValueLabel.setText(Integer.toString(observingData.scan));
				this.schedModeValueLabel.setText(observingData.schedMode);
				this.schedBlockNameValueLabel.setText(observingData.schedBlockName);
				
				// must convert from integer seconds to floating point minutes for display purposes
				float minutesElapsed = (float)observingData.schedBlockElapsedTime / 60;
				float minutesEstimated = (float)observingData.schedBlockEstimatedTime / 60;
				this.schedBlockElapsedTimeValueLabel.setText(new FormattedFloat(minutesElapsed, "%9.1f").toString().trim());
				this.schedBlockEstimatedTimeValueLabel.setText(new FormattedFloat(minutesEstimated, "%9.1f").toString().trim());
				setForegroundColor(null);
			}
		}
		logger.exiting("ArraySchedulingInfoPanel", "communicationEstablished");
	}

	public void communicationLost(CommunicationEstablishedListener.Aspect aspect) 
	{
		logger.entering("ArraySchedulingInfoPanel", "communicationLost");
		if(aspect == CommunicationEstablishedListener.Aspect.OBSERVING) {
			this.communicationEstablished = false;
			MiscUtilities.updateQuestionMarkLabels(questionMarkLabels, communicationEstablished);
			setForegroundColor(ArrayStatusPlugin.STALE_DATA_COLOR);
		}
		logger.exiting("ArraySchedulingInfoPanel", "communicationLost");
	}
	
	private void setForegroundColor(Color color)
	{
		this.scanNumberValueLabel.setForeground(color);
		this.schedModeValueLabel.setForeground(color);
		this.schedBlockNameValueLabel.setForeground(color);
		this.schedBlockElapsedTimeValueLabel.setForeground(color);
		this.schedBlockEstimatedTimeValueLabel.setForeground(color);
		
	}
	
	private void initialize()
	{
		logger.entering("ArraySchedulingInfoPanel", "initialize");
		this.setLayout(new GridBagLayout());
		this.setBorder(BorderFactory.createTitledBorder(OBSERVING_PANEL_TITLE));
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.BOTH;
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.ipadx = 0;
		constraints.ipady = 0;
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.weightx = 1.0;
		constraints.weighty = 0.0;
		
		this.add(createScanNumberPanel(), constraints);
		
		constraints.gridy++;
		
		this.add(createSchedModePanel(), constraints);
		
		constraints.gridy++;
		
		this.add(createSBNamePanel(), constraints);
		
		constraints.gridy++;
		this.add(createSBEstimatedAndElapsedTimePanel(), constraints);
		logger.exiting("ArraySchedulingInfoPanel", "communicationLost");
	}
	
	private JPanel createSBEstimatedAndElapsedTimePanel()
	{
		logger.entering("ArraySchedulingInfoPanel", "createSBEstimatedAndElapsedTimePanel");
		JPanel retPanel = new JPanel(new GridBagLayout());

		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.NONE;
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		constraints.ipadx = 5;
		constraints.gridy = 0;
		
		schedBlockElapsedTimeTextLabel = new JLabel(SB_ELAPSED_TIME_LABEL_TEXT);
		retPanel.add(schedBlockElapsedTimeTextLabel, constraints);

		schedBlockElapsedTimeValueLabel = new JLabel("00.0");
		retPanel.add(schedBlockElapsedTimeValueLabel, constraints);
		
		schedBlockElapsedTimeUnitsLabel = new JLabel(MIN_UNITS_LABEL_TEXT);
		retPanel.add(schedBlockElapsedTimeUnitsLabel, constraints);

		constraints.weightx = 1.0;
		schedBlockElapsedTimeQuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		questionMarkLabels.add(schedBlockElapsedTimeQuestionableLabel);
		retPanel.add(schedBlockElapsedTimeQuestionableLabel, constraints);
		
		constraints.gridy++;
		constraints.weightx = 0.0;
		
		schedBlockEstimatedTimeTextLabel = new JLabel(SB_STIMATED_TIME_LABEL_TEXT);
		retPanel.add(schedBlockEstimatedTimeTextLabel, constraints);

		schedBlockEstimatedTimeValueLabel = new JLabel("00.0");
		retPanel.add(schedBlockEstimatedTimeValueLabel, constraints);
		
		schedBlockEstimatedTimeUnitsLabel = new JLabel(MIN_UNITS_LABEL_TEXT);
		retPanel.add(schedBlockEstimatedTimeUnitsLabel, constraints);

		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		schedBlockEstimatedTimeQuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		questionMarkLabels.add(schedBlockEstimatedTimeQuestionableLabel);
		retPanel.add(schedBlockEstimatedTimeQuestionableLabel, constraints);
		
		logger.exiting("ArraySchedulingInfoPanel", "createSBEstimatedAndElapsedTimePanel", retPanel);
		return retPanel;
	}
	
	private JPanel createScanNumberPanel()
	{
		logger.entering("ArraySchedulingInfoPanel", "createScanNumberPanel");
		JPanel scanNumberPanel = new JPanel(new GridBagLayout());
		GridBagConstraints innerConstraints = new GridBagConstraints();
		innerConstraints.fill = GridBagConstraints.BOTH;
		innerConstraints.anchor = GridBagConstraints.NORTHWEST;
		innerConstraints.weightx = 0.0;
		innerConstraints.weighty = 1.0;
		innerConstraints.ipadx = 5;
		
		scanNumberTextLabel = new JLabel(SCAN_NUMBER_LABEL_TEXT);
		scanNumberPanel.add(scanNumberTextLabel, innerConstraints);

		scanNumberValueLabel = new JLabel("0");
		scanNumberPanel.add(scanNumberValueLabel, innerConstraints);

		innerConstraints.weightx = 1.0;
		scanNumberQuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		questionMarkLabels.add(scanNumberQuestionableLabel);
		scanNumberPanel.add(scanNumberQuestionableLabel, innerConstraints);
		
		logger.exiting("ArraySchedulingInfoPanel", "createScanNumberPanel", scanNumberPanel);
		return scanNumberPanel;
	}
		
	/**
	 * @return a sub-panel containing the sched mode information.
	 */
	private JPanel createSchedModePanel() 
	{
		logger.entering("ArraySchedulingInfoPanel", "createSchedModePanel");
		JPanel schedModePanel = new JPanel(new GridBagLayout());
		GridBagConstraints innerConstraints = new GridBagConstraints();
		innerConstraints.fill = GridBagConstraints.BOTH;
		innerConstraints.anchor = GridBagConstraints.NORTHWEST;
		innerConstraints.ipadx = 5;
		innerConstraints.weighty = 1.0;
		
		schedModeTextLabel = new JLabel(SCHED_MODE_LABEL_TEXT);
		schedModePanel.add(schedModeTextLabel, innerConstraints);

		schedModeValueLabel = new JLabel("unknown");
		schedModePanel.add(schedModeValueLabel, innerConstraints);

		innerConstraints.weightx = 1.0;
		schedModeQuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		questionMarkLabels.add(schedModeQuestionableLabel);
		schedModePanel.add(schedModeQuestionableLabel, innerConstraints);

		logger.exiting("ArraySchedulingInfoPanel", "createSchedModePanel", schedModePanel);
		return schedModePanel;
	}
	
	/**
	 * @return a sub-panel containing the sched mode information.
	 */
	private JPanel createSBNamePanel() 
	{
		logger.entering("ArraySchedulingInfoPanel", "createSBNamePanel");
		JPanel sbNamePanel = new JPanel(new GridBagLayout());
		GridBagConstraints innerConstraints = new GridBagConstraints();
		innerConstraints.fill = GridBagConstraints.BOTH;
		innerConstraints.anchor = GridBagConstraints.NORTHWEST;
		innerConstraints.ipadx = 5;
		innerConstraints.weighty = 1.0;
		
		schedBlockNameTextLabel = new JLabel(SB_LABEL_TEXT);
		sbNamePanel.add(schedBlockNameTextLabel, innerConstraints);
		
		schedBlockNameValueLabel = new JLabel("unknown");
		sbNamePanel.add(schedBlockNameValueLabel, innerConstraints);

		innerConstraints.weightx = 1.0;
		schedBlockQuestionableLabel = new QuestionMarkLabel(!communicationEstablished);
		questionMarkLabels.add(schedBlockQuestionableLabel);
		sbNamePanel.add(schedBlockQuestionableLabel, innerConstraints);
		
		logger.exiting("ArraySchedulingInfoPanel", "createSBNamePanel", sbNamePanel);
		return sbNamePanel;
	}
	
	// for testing only
	private static void createAndShowGUI()
	{
		JFrame mainFrame = new JFrame();
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.add(new ArraySchedulingInfoPanel(Logger.getLogger("test logger")));
		mainFrame.pack();
		mainFrame.setVisible(true);
	}
	
	/**
	 * @param args
	 * for testing only
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() { ArraySchedulingInfoPanel.createAndShowGUI();}
		});
	}
}
