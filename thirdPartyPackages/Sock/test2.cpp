// Test the time it takes to fetch FITS Images from the OPTS server

// test2 <first #> <last #>

// Requests Images <first #> through <last #> one at a time 
// Quits if an Error message is received

/* Version of Sock that also writes FITS input to disk */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

#include "sock.h"

#define MAXFITS 1024*1024*2+10000 /* Maximum number of bytes in a FITS image */

char *getNewCtime(int offset);

int main(int argc, char *argv[])
{
  char *ptr;
  char message[MAXFITS+1], line[501];
  int len, first, last, i;
  struct SOCK *send;

  if( argc != 3 ) {
    printf( "\nusage: %s <first #> <last #>\n", argv[0]);
    exit(0);
  }
  first =  atoi(*++argv);
  last =  atoi(*++argv);
  send = sock_connect("OPT");  
  sprintf(line, "Get Image %d", first);
  printf("%s Sending %s\n", getNewCtime(0), line);
  sock_bufct(MAXFITS);
  if (sock_send(send, line) == 0) {
    printf("send failed\n");
    exit(1);
  }
  for (i=first+1; i<=last; i++) {
    switch(sock_sel( message, &len, NULL, 0, 0, 1)) {
    case 0: 
      if (!strncmp(message, "QUIT", 4)) {
	exit(0);
      }
      sock_send( send, message); 
      break;
    case -1:
    case -2:
    case -3:
      printf("Error\n");
      exit(1);
      break;
    default:
      if (strncmp(message,"SIMPLE  =", 9) != 0) {
	/* Not FITS.  Print no more than the first 500 characters */
	strncpy(line, message, 500);
	printf("%s message received:\n%s\n", getNewCtime(0), line );
	if (strncmp(line, "Error", 5) == 0)
	  exit(0);
      } else {
	/* FITS input.  Print the header sequence number */
	printf("%s FITS received:\n", getNewCtime(0));
	for (ptr=message; strncmp(ptr, "IMAGENUM=", 9); ptr += 80)
	  continue;
	strncpy(line, ptr, 80);
	printf("%s\n", line);
	if (i != last) {
	  sprintf(line, "Get Image %d", i);
	  printf("%s Sending %s\n", getNewCtime(0), line);
	  if (sock_send(send, line) == 0) {
	    printf("send failed\n");
	    exit(1);
	  }
	}
      }
    }
  }
  exit(1);
}

char *getNewCtime(int offset)
{
  double timed;
  long usec;
  static char timestr[256];
  struct timeval tvv,*tp;
  struct tm *tmnow;
  long sec, toff;

  tp = &tvv;
  gettimeofday(tp,0);
  tp->tv_usec += 500;               /* Add .0005 sec for rounding to millisec */
  if (tp->tv_usec >= 1000000)
  {
    tp->tv_usec -= 1000000;
    tp->tv_sec++;
  }

  toff = offset * 3600;
  sec = tp->tv_sec + toff;                                      /* add offset */
  tmnow = localtime(&sec);
  timed = ((double)tp->tv_usec / 1000000.0) * 1000.0;     /* Rounded ms */
  usec = (long)timed;

  sprintf(timestr,"%02d:%02d:%02d.%03ld ",
		tmnow->tm_hour,
		tmnow->tm_min,
		tmnow->tm_sec,
		usec);
  return(timestr);
}
