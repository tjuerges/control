/* Sock: Tool for typing messages to send to the specified Socket Library 
 * mailbox and printing responses 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sock.h"

int main(int argc, char *argv[])
{
  char *sendname, *myname;
  char *message;
  int len, timeout;
  struct SOCK *send = 0;
  int quit_flag = 0;
  int quiet_flag = 1;
  int response_flag = 0;
  int slam_flag = 0;
  int ct_flag = 8192;

  sendname = NULL;
  myname = "ANONYMOUS";
  if( argc <= 1 ) {
    printf( "\nusage: %s [-b myName] [-s] [-r] [-sz sz] [-v] [toName]\n", argv[0] );
    printf( "  -b myName - mailbox name for this program\n" );
    printf( "  destname  - the mailbox name to which messages are sent\n" );
    printf( "  -r response is expected\n\n" );
    printf( "  -s  slam it down one line at a time with out looking for a response\n");
    printf( "  -sz  sz  maximum size of messages\n");
    printf( "  -v Verbose: \"message received\" precedes incoming messages\n");
    exit(0);
  }

  while( *++argv ) {
    if( strcmp( *argv , "-b" ) == 0 )
      myname = *++argv;
    else if( strcmp( *argv , "-r" ) == 0 )
      response_flag = 1;
    else if( strcmp( *argv , "-s" ) == 0 )
      slam_flag = 1;
    else if( strcmp( *argv , "-sz" ) == 0 )
      ct_flag = atoi(*++argv);
    else if( strcmp( *argv , "-v" ) == 0 )
      quiet_flag = 0;
    else
      sendname = *argv;
  }

  message = (char *)malloc(ct_flag);

  if( myname )
    sock_bind(myname);

  sock_bufct(ct_flag);
  if( sendname )
    send = sock_connect(sendname);

  if( response_flag )
    timeout = 1;
  else
    timeout = 0;

  if( slam_flag ) {
    while( fgets(message, ct_flag-1, stdin))
      sock_send( send, message );
     
  } else  {
    while(1) {
      switch(sock_sel( message, &len, NULL, 0, timeout, !quit_flag)) {
        case 0: 
          if (!sock_send( send, message) && myname && !sendname)
	    send = 0; 
          break;
        case -1:
          if( quit_flag )
            exit(1);
        case -2:
        case -3:
          if( response_flag )
            quit_flag = 1;
          else
            exit(1);
          break;
        default:
	  if (send == 0)
	    send = sock_last();
          if( response_flag || quiet_flag)
            printf("%s\n", message );
          else
            printf("message received %s\n", message );
          break;
      }
    }
  }
  return 1;
}

