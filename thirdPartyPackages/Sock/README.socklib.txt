			Socklib Quick Start

    Some documentation can be found in two files:

	 socklib.overview.txt
and	 socklib.functions.txt

    In addition to the library itself, there are several programs and a
mailbox definition file, mailboxdefs.  The latter only defines three
mailboxes, OPT, OPT_GUI, and OPT_ALMA_GUI.  And the host name for each is
simply "localhost".  This file should (eventually) be edited and localhost
replaced with actual host names.  The software is currently configured to
accept up to 100 mailbox definitions in mailboxdefs.

    The default location of the mailboxdefs file, /etc/mailboxdefs, is
specified in mbdef.h.  This may be edited to move it to another
location if needed.  After that, recompile the library with

    make clean ; make

    One of the programs provided is called sock.  It is useful for testing
the library and for manually sending messages to applications.  For instance,

    sock OPT

will connect to the OPT mailbox.  To send a message to the program bound to
the OPT mailbox, type your message and strike <RETURN>.  The response, if any,
to this message will be printed.  Strike <CTRL-C> or <CTRL-D> to exit sock.

    sock -b OPT

will bind this instance of sock to mailbox OPT.  (If another program has
already bound to mailbox OPT, sock will wait for it to exit while typing
"Address already in use" complaints periodically.)  If sock is invoked
this way, it will initially have no mailbox to send messages to.  But if
something is sent to the OPT mailbox (by sock OPT, for example), that message
will be printed and any replies that you type will be sent to the program
that sent a message to OPT.

    sock -b OPT OPT_GUI

will be similar, except that this instance will only send messages to
OPT_GUI, even if another mailbox sends a message to OPT.  (An indefinite
number of programs can send messages to a given mailbox.)  So a good test
that the socket library is working is to execute

     sock -b OPT OPT_GUI

in one window and

     sock -b OPT_GUI OPT

in another and see if messages typed in one window appear in the other.
(To see other options supported by sock, simply type sock with no
parameters.)

    Another test program is sockfits.  It is simply sock modified so
that if it receives a FITS image, it will write the data received to
the file specified by the -o option (default image.fits).  It will
only type the FITS header in the window.  It assumes that any message
whose first 9 characters are "SIMPLE =" is a FITS image and FITS
images can be no longer than needed for a 1024x1024 size image:
1024*1024*2+10000.  If the user types QUIT for a message, it will
close the FITS output file and terminate sockfits instead of sending
"QUIT" to the other mailbox.

    Two example programs are provided, blocking_examp and polling_examp.
They show how to write a program that not only looks for incoming messages
to its mailbox but also responds to inputs from stdin and to a named pipe.
