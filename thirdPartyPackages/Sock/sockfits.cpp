/* Version of Sock that also writes FITS input to disk */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

#include "sock.h"

#define min(A,B) ((A) < (B) ? (A) : (B))
#define max(A,B) ((A) > (B) ? (A) : (B))

#define MAXFITS 1024*1024*2+10000 /* Maximum number of bytes in a FITS image */

char *getNewCtime(int offset);

int main(int argc, char *argv[])
{
  char *sendname, *myname, *out_file = "image.fits", *ptr;
  char message[MAXFITS+1], line[501];
  int len, cur_timeout, ret;
  struct SOCK *send;
  int quit_flag = 0;
  int quiet_flag = 1;
  int response_flag = 0;
  int slam_flag = 0;
  int sz_flag = MAXFITS;
  int timeout = 15;
  int brief_flag = 0;
  long int poll_flag = 0;
  FILE *output;
  struct timespec rqtp
;
  sendname = NULL;
  myname = "ANONYMOUS";
  if( argc <= 1 ) {
    printf( "\nusage: %s [-b myName] [-s] [-r] [-sz sz] [-v] [-t timeout] [-brief] [-o fitsOutput] [-p nano] [toName]\n", argv[0] );
    printf( "  -b myName - mailbox name for this program\n" );
    printf( "  destname  - the mailbox name to which messages are sent\n" );
    printf( "  -r response is expected\n\n" );
    printf( "  -s  slam it down one line at a time with out looking for a response\n");
    printf( "  -sz  sz  - maximum size of messages\n");
    printf( "  -t timeout - Waits timeout seconds before exiting after receiving\n"
            "               a FITS file (default 15, 0 -> no timeout)\n"); 
    printf( "  -v Verbose: timestamp & \"message received\" precedes incoming messages\n");
    printf( "  -o Name of the file to which any FITS messages received will be written\n");
    printf( "  -brief Instead of the entire FITS header, only print the IMAGENUM line\n");
    printf( "  -p nano Use the polling instead of the blocking sock_sel call\n"
	    "              where nano is the polling interval in nanoseconds\n");
    exit(0);
  }
  while( *++argv ) {
    if( strcmp( *argv , "-b" ) == 0 )
      myname = *++argv;
    else if( strcmp( *argv , "-r" ) == 0 )
      response_flag = 1;
    else if( strcmp( *argv , "-s" ) == 0 )
      slam_flag = 1;
    else if( strcmp( *argv , "-sz" ) == 0 )
      sz_flag = atoi(*++argv);
    else if( strcmp( *argv , "-v" ) == 0 )
      quiet_flag = 0;
    else if( strcmp( *argv , "-o" ) == 0 )
      out_file = *++argv;
    else if( strcmp( *argv , "-t" ) == 0 )
      timeout = atoi(*++argv);
    else if (strcmp( *argv , "-brief" ) == 0 )
      brief_flag = 1;
    else if (strcmp( *argv , "-p" ) == 0 ) {
      poll_flag = atoi(*++argv);
      if (poll_flag <= 0) {
	printf("Illegal nano value for -p: %s\n", *argv);
	exit(0);
      }
    } else
      sendname = *argv;
  }

  rqtp.tv_sec = 0;
  rqtp.tv_nsec = poll_flag;

  sock_bufct(sz_flag);

  output = fopen(out_file, "w");
  if (output == NULL) {
    printf("Could not create %s\n", out_file);
    exit(0);
  }

  if( myname )
    sock_bind(myname);

  if( sendname )
    send = sock_connect(sendname);

  if( response_flag )
    cur_timeout = 1;
  else
    cur_timeout = 0;

  if( slam_flag ) {
    while( fgets(message, sizeof(message)-1, stdin))
      sock_send( send, message );
     
  } else  {
    while(1) {
      if (poll_flag == 0)
	ret = sock_sel( message, &len, NULL, 0, cur_timeout, !quit_flag); 
      else {			// Poll mode:  Check for a message pending
	ret = sock_sel( message, &len, NULL, 0, -1, !quit_flag);
	if (ret == 0) {
	  nanosleep(&rqtp, NULL); // No message.  Sleep for specified time
	  continue;
	}
	ret = sock_sel( message, &len, NULL, 0, cur_timeout, !quit_flag);
      }
      switch(ret) {
        case 0: 
          if (!strncmp(message, "QUIT", 4)) {
	    fclose(output);
	    exit(0);
	  }
          sock_send( send, message); 
          break;
        case -1:
          if( quit_flag )
            exit(1);
	  if (cur_timeout == timeout) {
	    fflush(output);		/* Flush the output file */
	    cur_timeout = 0;
	  }
        case -2:
        case -3:
          if( response_flag )
            quit_flag = 1;
          else
            exit(1);
          break;
        default:
	  if (strncmp(message,"SIMPLE  =", 9) != 0) {
/* Not FITS.  Print no more than the first 500 characters */
	    strncpy(line, message, 500);
	    if( response_flag || quiet_flag)
	      printf("%s\n", line );
	    else
	      printf("%s message received:\n%s\n", getNewCtime(0), line );
	  } else {
/* FITS input.  Print the header and write it to the specified file */
	    if ( !quiet_flag)
	      printf("%s FITS received:\n", getNewCtime(0));
	    if ( brief_flag) {
	      for (ptr=message; strncmp(ptr, "IMAGENUM=", 9); ptr += 80)
		continue;
	      strncpy(line, ptr, 80);
	      printf("%s\n", line);
	    } else {
	      line[80] = '\0';
	      for (ptr=message; strncmp(ptr, "END      ", 9); ptr += 80) {
		strncpy(line, ptr, 80);
		printf("%s\n", line);
	      }
	    }
	    printf("END\n\n");
	    fwrite(message, 1, len, output);
	    fflush(output);
	    cur_timeout = timeout;	/* Terminate if idle for timeout seconds */
	  }
          break;
      }
    }
  }
  return 1;
}

char *getNewCtime(int offset)
{
  double timed;
  long usec;
  static char timestr[256];
  struct timeval tvv,*tp;
  struct tm *tmnow;
  long sec, toff;

  tp = &tvv;
  gettimeofday(tp,0);
  tp->tv_usec += 500;               /* Add .0005 sec for rounding to millisec */
  if (tp->tv_usec >= 1000000)
  {
    tp->tv_usec -= 1000000;
    tp->tv_sec++;
  }

  toff = offset * 3600;
  sec = tp->tv_sec + toff;                                      /* add offset */
  tmnow = localtime(&sec);
  timed = ((double)tp->tv_usec / 1000000.0) * 1000.0;     /* Rounded ms */
  usec = (long)timed;

  sprintf(timestr,"%02d:%02d:%02d.%03ld ",
		tmnow->tm_hour,
		tmnow->tm_min,
		tmnow->tm_sec,
		usec);
  return(timestr);
}
