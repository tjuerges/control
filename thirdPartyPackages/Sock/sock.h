#ifndef SOCK_H
#define SOCK_H

#include <netinet/in.h>

#define SOCK_DEF_BUFCT 4096        	 /* default buffer count             */

// sock_sel return values
#define SOCK_SEL_STDIN    0              /* message came from standard input */
#define SOCK_SEL_TIMEOUT -1              /* socket timeout occurred          */
#define SOCK_SEL_ERROR   -2              /* socket error occurred            */
#define SOCK_SEL_SIGNAL  -3              /* interrupted by a signal          */

// sock_only return values
#define SOCK_ONLY_OK       0
#define SOCK_ONLY_TIMEOUT -1
#define SOCK_ONLY_ERROR   -2
#define SOCK_ONLY_SIGNAL  -3

// sock_intr parameters
#define SOCK_INTR_RETRY    0
#define SOCK_INTR_NO_RETRY 1

#define SOCK_SIZE_COUNT 4	/* Length of byte count */
struct BOUND {
  int bind_fd;
  struct DEST *dest;
  struct SOCK *head; /* head of list */
  struct sockaddr_in sin;
  unsigned int bufct;
  int interrupt;
  int isxview; /* true if xview */
  int isaio;   /* true if aioread is turned on (Solaris2) */
};

struct SOCK {
  struct SOCK *next;
  struct DEST *dest;
  int fd;
  struct sockaddr_in sin;
};

struct BOUND *sock_bind(const char *);
struct SOCK *sock_connect(const char *);
struct SOCK *sock_last(void);
char *sock_name(struct SOCK *handle);
struct SOCK *sock_find(const char *);
int sock_bufct(int);
int sock_send(struct SOCK *, const char *);
int sock_write(struct SOCK *, const char *, int);
int sock_sel(char *message, int *len, int *ptr, int nptr, int tim, int rd_in);
int sock_ssel(struct SOCK *xhndl[], int nhndl, char *message, int *len, 
	      int *ptr, int nptr, int tim, int rd_in);
int sock_fastsel(char *message, int *len, int *ptr, 
		 int nptr, struct timeval *tim, int rd_in);
int sock_only(struct SOCK *handle, char *message, int tim );
int sock_only_f(struct SOCK *handle, char *message, double ftime);
int sock_poll(struct SOCK *handle, char *message, int *plen );
void sock_close(char *name);
int sock_fd(struct SOCK *handle);
void sock_intr(int flag);

int log_msg(const char *format, ...);

/* ARO logging library adds a timestamp & writes the perror output to a file */
#define log_perror perror

#endif

