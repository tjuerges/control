#include <stdio.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>

#include "mbdef.h"
#include "sock.h"


/*
      This includes the definitions of sock_write and sock_send for the 
      non-xview users
      xview support is obtained by using the library in sockxviewlib.c
      and this module will not be linked.
*/

extern struct BOUND *bnd;
extern struct SOCK sock_kbd;

void bind_any(int);

#ifdef log_perror
int logCallingSockFlag = 0;	/* Not using logging library or eventd */
#else
extern int logCallingSockFlag;
#endif

/*
  like sock_send but takes pointer and count as in write
*/

int sock_write( struct SOCK *hndl, const char *message, int mlen )
{
  int ret, err, flag = 0;
  unsigned long count;
  unsigned long net;
  socklen_t len;
  char *name;

  if( (long)hndl == -1 || !message || mlen <= 0 )
    return(0);

  if( !hndl )
  {
    if(!logCallingSockFlag)
      log_msg("no mailbox for: %s", message );

    return(0);
  }

  while( flag < 2 ) {
    if( hndl->fd < 0 ) {
      if( strcmp( hndl->dest->dname, "ANONYMOUS") == 0 )
        return(0);

      if( (hndl->fd = socket(hndl->sin.sin_family, SOCK_STREAM, 0 )) <0) {

	if(hndl->dest->dname) {
          char buf[80];

          sprintf(buf,"sock_write, socket(%s)", hndl->dest->dname);
          log_perror(buf);
	}
	else
          log_perror("sock_write, socket");

        return(0);
      }
      bind_any(hndl->fd);

      if( connect( hndl->fd, (struct sockaddr *)&hndl->sin, sizeof(struct sockaddr_in) ) <0 ) {
        close(hndl->fd);
        hndl->fd = -1;
	if(hndl->dest->dname) {
          char buf[80];

          sprintf(buf,"sock_write, connect(%s)", hndl->dest->dname);
          log_perror(buf);
	}
	else
          log_perror("sock_write, connect");

        return(0);
      }

      if ( ! hndl->dest->raw && ! bnd->dest->raw) {
/* It's not a raw socket.  Send the mailbox name */
	if( bnd->bind_fd >0 ) {
	  count = strlen(bnd->dest->dname);
	  name =  bnd->dest->dname;
	} else { /* if he never bound then send ANONYMOUS */
	  count = 9;
	  name = "ANONYMOUS";
	}
	net = htonl(count);
	if( (ret = write( hndl->fd, &net, 4))>0 )
	  ret = write( hndl->fd, name, count);
      }
    }
    count = mlen;
    if ( ! hndl->dest->raw && ! bnd->dest->raw) {
/* It's not a raw socket.  Send the byte count first */
      net = htonl(count);
      ret = write( hndl->fd, &net, SOCK_SIZE_COUNT);
    } else
      ret = 1;			/* Raw socket always writes the data */

    if (ret > 0) {
	int ct, ix;

	ct = count;
	ix = 0;
      while( ct > 0 && ret > 0 ) {
        ret = write( hndl->fd, &message[ix], ct);
        ct -= ret;
        ix += ret;
      }
      if (ret > 0)
	ret = ix;
    }

/*
   this checks to see if vxworks box (or sun) has rebooted 
*/
    usleep(1);
    len = 4;
    err = 0;
    if( getsockopt( hndl->fd, SOL_SOCKET, SO_ERROR, (char *)&err, &len ) < 0 || err != 0 )
      ret = count + 1;

    if( ret != (int)count ) {
      if(ret <0 )
      {
	if(hndl->dest->dname) {
          char buf[80];

          sprintf(buf,"sock_write(%s)", hndl->dest->dname);
          log_perror(buf);
	}
	else
          log_perror("sock_write");
      }

      if(!logCallingSockFlag)
        log_msg("closing out mailbox: %s", hndl->dest->dname );

      close( hndl->fd);
      hndl->fd = -1;
      flag += 1;
   } else
      break;
  }
  return(1);
}

int sock_send( struct SOCK *hndl, const char *message )
{
  return( sock_write( hndl, message, strlen(message)));
}

