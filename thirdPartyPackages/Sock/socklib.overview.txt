	    		ARO Socket Library Overview

	 The ARO Socket Library is a communication package by which a
program can exchange messages and data with another program or a
device via TCP/IP sockets.  If the communicating programs are running
on the same computer, the same protocol is used although no actual
ethernet transmission occurs.  (The original version of the Socket
Library was written for the NRAO 12m telescope control system by Jeff
Hagen.  It is also used at the Heinrich Hertz Telescope at Mt Graham.
The library was modified by Bill Peters so that it could also
communicate with devices that have a TCP/IP socket interface.)

	The communication is based upon what the library calls
mailboxes.  A mailbox is simply the name used to refer to a TCP port
on a particular computer/device.  A database is defined that
associates the name of a given mailbox with a unique pair of TCP port
and computer host name (or IP address).  A program that accepts
messages or queries will Bind to its mailbox.  A program that wishes
to communicate with a mailbox first Connects to it (which ultimately
establishes an TCP socket to that mailbox).  More than one program can
Connect to a mailbox.  A program that Connects need not have its own
mailbox to which it Binds, as a Connection to a mailbox is a socket
can both send and receive messages.  Binding is only needed for
programs that will accept unsolicited messages.  And the number of
active Connections that can be established is limited only by the
system resources (such as the number of sockets that can be opened at
one time).

	Programs that communicate via the socket library need not be
started in any particular order.  That is because a program is allowed
to declare a Connection to a mailbox before the owner of the mailbox
begins running.  No attempt to establish a TCP socket is done until
the program asks to send a message to a mailbox.  If at that time, the
mailbox owner is still not running, there will of course be a failure.
A failure may also happen after a socket is opened if either of the
programs associated with the socket stops executing or fails to
respond within a specified time limit.  A failure is handled by
returning an error code to the caller, closing the socket, and
ignoring the unsent message (if any).  The next time the caller sends
a message to that mailbox, a new socket is opened (if possible).  (It
is up to the caller to decide whether to send the failed message
again.)

	Messages exchanged are arbitrary.  They might be ASCII
characters with or without newlines or they might be binary data.  And
they need not be a particular length.  To implement varying length
messages, each message is usually preceded by a 4-byte count defining
the size of the message that follows.  That way, if a message is too
long to be read with a single read call, the library knows to read the
remainder of the message before returning to the caller.  And when a
socket is first opened, the first message sent to the mailbox owner is
the name of the mailbox owned by the originator.  (If the originator
has not done a Bind to its own mailbox, the name ANONYMOUS is sent
instead.)

	The 4-byte count convention is also used to perform a sanity
check on incoming messages.  If the count exceeds a limit set, the
message is discarded and an error is returned to the caller.  This is
to prevent the program from hanging in order to read a ridiculously
long message (which might happen if part of the previous message was
not read).  The caller defines what the limit will be on this sanity
check prior to reading its messages.

	When messages are exchanged with a device or a program that
isn't using this socket library conventions, the corresponding mailbox
can be defined as a "raw" mailbox.  These mailboxes skip the byte
count (and the name of the mailbox on a newly opened socket).  The
caller will have to decide if its messages received are incomplete and
therefore that additional reads are needed.  (Fortunately, most such
devices send fairly short messages, so it is unlikely that they will
exceed the system buffer size that requires more than one read to be
executed to read the entire message.)
