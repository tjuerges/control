// This is an example showing how one could use the ethernet socket library

// In this example, the program binds to its own mailbox, OPT, and polls
// the socket library for incoming ethernet messages or from a named pipe 
// or from stdin.  If nothing is pending, it executes a function, do_chores().
// If appropriate, the program replies to the ethernet message received or 
// sends a message to the OPT_GUI mailbox
// The example assumes that the OPT_GUI recognizes a message beginning with
// "LOG " as an informational message to be saved/displayed
// 

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "sock.h"

#define MAXFITS 1000000		/* Maximum number of bytes in a FITS image */
#define NUMFD 1			/* Number of file descriptors for sock_sel */

void do_chores(void);
void process_stdin(char *buffer, int length);
void process_pipe(int fd);
void process_gui_message(char *buffer, int length, char *response, 
			 int *len_response);
void process_other_message(struct SOCK *who, char *buffer, int length, 
			   char *response, int *len_response);

struct SOCK *sock_gui;		/* Handle of socket to OPT_GUI */
int opt_pipe;			/* File descriptor of pipe */

int main()
{

  int sel;
  int fd[NUMFD], nfd = NUMFD;	/* Array of file descriptors */
  char *ptr;
  struct SOCK *who;
  char buffer[MAXFITS+1];	/* Buffer for incoming messages */
  int length;		 	/* Will receive the length of the message */
  char response[MAXFITS];	/* Buffer for outgoing messages */
  int len_response;	 	/* Length of response */

/* Time out for sock_sel call (seconds, 0=none)
 * If negative, sock_sel is put in polling mode: immediate return and
 * no read is done.  Returns 0 for no input pending, non-zero if a 
 * non-polling call will immediately read and return with a message.
 */
  int time_out = 0;		

/* Set to non-zero if expecting input from stdin */
  int rd_in = 1;

  sock_bufct(MAXFITS);		/* Define maximum size of ethernet messages */

  if (sock_bind("OPT") == 0) {
    printf("Error binding to OPT mailbox\n");
    exit(1);			/* Error binding to OPT */
  }
  sock_gui = sock_connect("OPT_GUI");
  if (sock_gui == 0) {
    printf("Mailbox OPT_GUI not found\n");
    exit(2);		/* No such mailbox */
  }

  opt_pipe = open("/home/opt/opt_pipe", O_RDWR);
  if (opt_pipe == -1) {
    ptr = "LOG Unable to open opt_pipe";
    perror(ptr+4);
    nfd--;			// Skip the opt_pipe file descriptor
  } else {
    ptr = "LOG OPT startup";
    fd[nfd-1] = opt_pipe;	// Put the opt_pipe fd at the end of the array
  }

  if (sock_send(sock_gui, ptr) == 0) {
    printf("Cannot send startup message to OPT_GUI\n");
/*
 * (This could happen if OPT_GUI isn't running at this time
 * Once OPT_GUI starts, subsequent sock_send's to it will succeed.)
*/
  }

    
/* Loop indefinitely */
  while(1) {
/* Poll sock_sel for incoming messages */
    if (sock_sel(buffer, &length, fd, nfd, -1, rd_in) == 0) {
      do_chores();
    } else {
/* There's a message pending. Call sock_sel again in the normal way to get it */
      sel = sock_sel(buffer, &length, fd, nfd, time_out, rd_in);
      if (sel == -1)
	continue;		/* Time out (Say something about it?) */
      else if (sel == -2)
	continue;		/* Error return (usually temporary) */
      else if (sel == -3)
	continue;		/* Interrupted by a signal (ditto) */
      else if (sel == 0)
	process_stdin(buffer, length); 	/* Message from stdin */
      else if (sel == opt_pipe) {
	process_pipe(sel);
      } else {
/* It's a socket message */
	who = sock_last();		/* Socket that sent the message */
	printf("%d character message from mailbox %s\n", length, sock_name(who));
	if (buffer[length-1] == '\n')
	  buffer[--length] = 0;	/* Remove trailing newline */
	if (who == sock_gui)
	  process_gui_message(buffer, length, response, &len_response);
	else
	  process_other_message(who, buffer, length, response, &len_response);
	if (len_response != 0) {
	  if (sock_write(who, response, len_response) == 0)
	    printf("Could not send response to %s\n", sock_name(who));
	}
      }
    }
  }
}

void do_chores(void)
{
  usleep(10000);	/* Nothing to do but get some shuteye */
}

void process_stdin(char *buffer, int length)
{
  char input[80] = "LOG ";
  strncat(input, buffer, 80-4);
  sock_send(sock_gui, input);
}

void process_pipe(int fd)
{
  char buffer[10];
  int i, length;

  length = read(fd, buffer, 10);
  printf("Got %d bytes from pipe:", length);
  for (i=0; i<length; i++)
    printf(" 0x%x", buffer[i]);
  printf("\n");
}

void process_gui_message(char *buffer, int length, char *response, 
			 int *len_response)
{
  char *token, input[80];

  strncat(input, buffer, 80-4);
  printf("GUI sent %d characters: %s\n", length, input);
  token = strtok(buffer, " ");
  if (strcmp(token, "QUIT") == 0) {
    printf("Quitting\n");
    if (close(opt_pipe))
      perror("Error closing opt_pipe");
    exit(0);
  }
  if (strcmp(token, "STATUS") != 0) {
    sprintf(response, "LOG %s not implemented yet", input);
    *len_response = strlen(response);
  } else {
    strcpy(response, "OPT is ready");
    *len_response = strlen(response);
  }
}

void process_other_message(struct SOCK *who, char *buffer, int length, 
			   char *response, int *len_response)
{
  int start=1, abort=-1, ret;

  if (strcmp(buffer, "START") == 0) {
    ret = write(opt_pipe, &start, sizeof(int));
    *len_response = 0;
    sock_send(sock_gui, "LOG Exposure started");
    usleep(1000);	/* Wait for pipe to be read */
  } else if (strcmp(buffer, "STOP") == 0) {
    write(opt_pipe, &abort, sizeof(int));
    *len_response = 0;
    sock_send(sock_gui, "LOG Exposure aborted");
    usleep(1000);	/* Wait for pipe to be read */
  } else {
    sprintf(response, "ERROR %s not implemented yet", buffer);
    *len_response = strlen(response);
  }
}
