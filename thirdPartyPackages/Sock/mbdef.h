/*
 * structure of mailboxdefs file
 *
 */

/* every destination has a port # as defined by the mailboxdefs file */

#define MAXDEST 100	/* Maximum number of entries in mailboxdefs file */
#define MAXNAME  50	/* Maximum size of mailbox name & host name */

// Default mailboxdefs file
#define MAILBOXDEFS_DEFAULT "/etc/mailboxdefs"

struct DEST {
  int dport;				/* Port number */
  char host[MAXNAME];			/* Computer that owns this mailbox */
  char dname[MAXNAME];			/* Mailbox name */
  short raw;				/* Non-zero if this is a `raw' socket */
};

struct DEST *find_dest(const char *name);
struct DEST *find_port(int fport);
