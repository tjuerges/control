#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <stdarg.h>

#include "mbdef.h"
#include "sock.h"

/*
    (Based upon Jeff Hagan's socket library written for the NRAO 12m)

    -  Only one bound socket is permitted

    -  Binding must be done to receive messages

    Routines:
      sock_bind(name)  - binds this process to name as defined in mailboxdefs
      sock_connect(name)  - returns a handle that is passed to sock_send
      sock_send( handle, message ) - sends message to mailbox associated 
        with a handle from sock_connect
      sock_write( handle, data, n ) - sends data to mailbox associated 
        with a handle from sock_connect

      sock_sel(message, l, p, n, tim, rd) - blocks on connections and messages.
        message is the buffer where a socket message will be put.
        l is the length of message (returned);
        p is an array of file descriptors that sock_sel will select on
        n is the length of p;
        tim is a timeout in seconds
        rd true means that sock_sel will read stdin
        sock_sel returns:
          -1 if a timeout.
          -2 if an error
          -3 if interrupted by a signal
          0 if standard in is ready for input
          or the file descriptor from 'p' that is ready.
          or the file descriptor of the socket that filled in message.
      sock_ssel(s, ns, message, l, p, n, tim, rd) - Same as sock_sel, but
	does not block on messages for the sockets in array, s (sock_connect 
	return values).
	ns is the length of s
	rest of parameters same as sock_sel.
      sock_close(name) - close opened connection - if NULL close all
      sock_name(sock_last()) - returns a pointer to the maibox's name
      sock_find(name) - returns handle for name
      sock_only(handle, message, tim) - blocks only on handle or timeout
      sock_only_f(handle, message, ftime) - same but timeout is floating point
      sock_poll(handle, message, tim, plen) - polls one socket and returns len
      sock_fd(handle) - returns a fd associated with handle 
      sock_intr(flag) - sock_sel will return when select is interrupted if flag
        is true.
      sock_bufct(n) - set out-of-sync size normally 4096

      xview support is obtained by using the library in sockxviewlib.c
*/

/* 
 * Replacement for ARO logging library function.  
 * Original version added a timestamp and wrote the results
 * to a file and/or stdout
 */

int log_msg(const char *format, ...)
{
  int ret;
  va_list argp;
  va_start(argp, format);
  ret = vprintf(format, argp);
  va_end(argp);
  printf("\n");
  return ret;
}

static struct DEST sock_ano = { 0, "localhost", "ANONYMOUS", 0 };
struct BOUND sock_bound = { 0, &sock_ano, NULL, {0}, 4096, 0, 0, 0 };

/*
    sock_bind is used to establish a destination socket
      that is defined in the mailboxdefs file
      you should only call this once.
*/

struct BOUND *bnd = &sock_bound;
void accept_sock(void);
static int readtm(int fd, void *buf, int len );
struct SOCK sock_kbd = { 0, &sock_ano, 0, {0} };
struct SOCK *last_conn;

struct BOUND *sock_bind(const char *mbname)
{
  int i, on, count, anon;
  struct hostent *hploc;
  char host[80];

  if( bnd->bind_fd > 0 )
    return bnd;			/* Sock_bind was already called */

  anon = strcmp(mbname, "ANONYMOUS") == 0;

  if( anon ) {
    bnd->dest = &sock_ano;
  } else if( (bnd->dest = find_dest(mbname)) == NULL ) {
    log_msg("sock_bind:unknown mailbox");
    return NULL;
  }
 
  if (strcmp(bnd->dest->host, "localhost")) {
/* Not explicitly "localhost".  Check that this mailbox is owned by this host */
    gethostname(host, 80);		/* Current host name (w/o) domain */
    count = strlen(host);
    i = strlen(bnd->dest->host);
    if( strncmp( host, bnd->dest->host, i < count ? i : count )) {
      log_msg("sock_bind: This program is not running on %s", bnd->dest->host);
      return NULL;
    }
  }

  if( !anon && (hploc = gethostbyname(bnd->dest->host)) == NULL ) {
    log_msg("sock_bind:can't find %s", bnd->dest->host );
    return NULL;
  }


  if( (bnd->bind_fd = socket( AF_INET, SOCK_STREAM, 0)) <0 ) {
    log_perror("socket");
    return NULL;
  }

  bnd->sin.sin_port = htons(bnd->dest->dport);
  bnd->sin.sin_addr.s_addr = INADDR_ANY;

  on = 1;
  setsockopt(bnd->bind_fd, SOL_SOCKET, SO_REUSEADDR, (void *)&on, sizeof(on));

  count = 0;
  while( bind( bnd->bind_fd, (struct sockaddr *)&bnd->sin, sizeof(struct sockaddr_in) ) <0 ){
    log_perror("bind");
    if( ++count > 50 )
      exit(0);
    sleep(10);
  }
  if( count > 0 )
    log_msg("finally bound\n");

  if( listen( bnd->bind_fd, 5 ) < 0  ) {
    log_perror("listen");
    exit(0);
  }
  signal( SIGPIPE, SIG_IGN );
  return bnd;
}

/*
    sock_connect returns a pointer to a structure that can be later passed to
       sock_send for sending a message
*/

struct SOCK *sock_connect(const char *mbname)
{
  struct SOCK *hndl;
  struct DEST *dst;
  struct hostent *hp;
  int bflag = 1;
  extern struct DEST sock_ano;

  hndl = bnd->head;

  if(strcmp( mbname, "ANONYMOUS" )) {
/* Mailbox is not ANONYMOUS */
    while(hndl) {
      if( hndl->dest && strcmp( mbname, hndl->dest->dname )==0 )
        return(hndl);	  /* Already have done a sock_connect to this mailbox */
      hndl = hndl->next;
    }
/* It's a new sock_connect */
    if((dst = find_dest(mbname)) == NULL ) {
      log_msg("sock_connect:unknown mailbox %s", mbname );
      return(0);
    }
  } else {
/* sock_connect to ANONYMOUS (done by accept_sock) */
/* Look for an ANONYMOUS mailbox SOCK struct with an inactive file descriptor */
    while(hndl) {
      if( hndl->dest && strcmp( mbname, hndl->dest->dname )==0 && hndl->fd < 0 )
        break;			/* ANONYMOUS with an inactive file descriptor */
      hndl = hndl->next;
    }
    dst = &sock_ano;
    bflag = 0;
  }

  if( !hndl ) {
/* Didn't find a SOCK for this mailbox, so allocate a new one */
    hndl = new struct SOCK;
    bzero(hndl, sizeof(struct SOCK ));
    hndl->fd = -1;
    hndl->dest = dst;
    hndl->next = bnd->head;
    bnd->head = hndl;
  }

  if( bflag ) { /* if anon then it wont ever connect */
    if( (hp = gethostbyname(hndl->dest->host)) == NULL ) {
      log_msg("sock_connect:can't find host");
      return(0);
    }

    hndl->sin.sin_family = hp->h_addrtype;		/* Will always be AF_INET */
    bcopy( hp->h_addr, &hndl->sin.sin_addr, hp->h_length );	/* IP number */
    hndl->sin.sin_port = htons(hndl->dest->dport);	/* Port number */
  }

  return hndl;
}

int read_sock(struct SOCK *hndl,char *msg, int *len)
{
  unsigned int count, ct, net;
  int n;
  
  last_conn = &sock_kbd;
  if (hndl->dest->raw || bnd->dest->raw) {
/* It's a `raw' socket; read up to max */
    if( (n = read( hndl->fd, msg, sock_bound.bufct-1 ))<=0) {
/* Error return */
      log_perror("read_sock, connection closed");
      *len = 0;
      close(hndl->fd);
      hndl->fd = -1;
      return(-2);
    }
    *len = n;
    msg[n] = '\0';
    last_conn = hndl;
    return(hndl->fd);
  }
/* Normal socket.  Read the byte count */
  if( (n=read(hndl->fd, &net, SOCK_SIZE_COUNT )) >0 ) {
    count = ntohl(net);
    if( count <= sock_bound.bufct ) {
      ct = count;
/* (May have to do several reads to get all `count' bytes) */
      while( (n=read(hndl->fd, &msg[count - ct], ct )) < (int)ct && n>0 )
	ct -= n;
    } else 				/* count is too big */
      log_msg("read count out of sync %lu %lu\n", count, net);
  }
  *len = count;
  if( n<0 )
    log_perror("read_sock, read");

  if( n<=0 || count > sock_bound.bufct ) {
    close(hndl->fd);
    hndl->fd = -1;
    return(-2);
  }
  msg[count] = '\0';
  last_conn = hndl;
  return(hndl->fd);
}


int sock_print_fds(void)
{
  struct SOCK *hndl;

  hndl = bnd->head;
  while(hndl) {
    if(hndl->dest->dname)
      log_msg("sock_print_fds: %d %s", hndl->fd, hndl->dest->dname);

    hndl = hndl->next;
  }

  return(0);
}

void sock_close(char *name)
{
  struct SOCK *hndl, *hp;

  if(name) { /* close name and de-queue */
    hndl = bnd->head;
    hp = NULL;
    while(hndl) {
      if( hndl->dest && strcmp( name, hndl->dest->dname )==0 )
        break;
      hp = hndl;
      hndl = hndl->next;
    }
    if( hndl ) {
      close(hndl->fd);
      if( hp )
        hp->next = hndl->next;
      else
        bnd->head = NULL;
      delete hndl;
    }
   } else { /* close all */
    hndl = bnd->head;
    while(hndl) {
      close( hndl->fd );
      hp = hndl->next;
      delete hndl;
      hndl = hp;
    }
    close(bnd->bind_fd);
    bnd->bind_fd = -1;
    bnd->dest = NULL;
  }
}

struct SOCK *sock_last(void) { return last_conn; }

char *sock_name(struct SOCK *handle)
{
  return( handle ? handle->dest->dname : NULL );
}

int sock_fd(struct SOCK *handle)
{
  return( handle ? handle->fd: -1 );
}


int sock_bufct(int bufct)
{
  if(bufct)
    sock_bound.bufct = bufct;
  return(bufct);
}


struct SOCK *sock_find(const char *mbname)
{
struct SOCK *hndl;
  hndl = bnd->head;
  while(hndl) {
    if( hndl->dest && strcmp( mbname, hndl->dest->dname )==0 )
      return hndl;
    hndl = hndl->next;
  }
  return(0);
}

/* set file descriptor to blocking/non-blocking */

void sock_block(struct SOCK *hndl, int onoroff) 
{
/*
  int flag, fd;

  fd = hndl->fd;
  if(fd<0)
    return;

  if( (flag = fcntl(fd, F_GETFL, 0 ))<0 ) {
    perror("get fcntl");
  }
  if( onoroff )
    flag |= FNDELAY;
  else
    flag &= ~FNDELAY;

  flag |= FASYNC;
  if( fcntl(fd, F_SETFL, flag )<0 ) {
    perror("set fcntl");
  }
*/
}

void sock_intr(int flag) { sock_bound.interrupt = flag; };


/*
   blocks for a message on all bound sockets except those listed in ss.
     xhndl is a pointer to an array of sockets (sock_connect return values)
     nhndl is the length of xhndl
     ptr is a pointer to an array of additional file descriptors to block on.
     nptr is the length of ptr.
      returns -1 on timeout.
              or the socket number on success
     timeout is in seconds; if <0, does a poll only (returns 0 if nothing ready)
     the size of message is up to the caller
     returns without reading when the 'ptr' descriptors are detected
*/

int sock_ssel(struct SOCK *xhndl[], int nhndl, char *message, int *len, 
	      int *ptr, int nptr, int tim, int rd_in)
{
  fd_set rfd;
  int sel, i;
  static struct timeval timeout;
  static int rd_dead = 0;
  struct timeval *pt;
  struct SOCK *hndl;

  if( !message && tim >= 0)
    return(-3);

  if( bnd->isxview ) {
   log_msg("Can't call sock_ssel if xview enabled");
   return(-3);
  }

  while(1) {

    FD_ZERO(&rfd);
    if( rd_in && !rd_dead )
      FD_SET(0, &rfd );

    if(bnd->bind_fd > 0 )
      FD_SET(bnd->bind_fd, &rfd );

    for(hndl=bnd->head; hndl; hndl = hndl->next ) {
      if( hndl->fd > 0 ) {
        for(i=0; i < nhndl && xhndl[i] != hndl; i++ )
	  continue;
	if (i >= nhndl)
          FD_SET( hndl->fd, &rfd );
      }
    }
    if( ptr && nptr > 0 )
      for(i=0; i<nptr; i++ )
        FD_SET( ptr[i], &rfd );

    if(tim > 0 ) {
      timeout.tv_sec = tim;
      timeout.tv_usec = 0; 
      pt = &timeout;
    } else if( tim == 0 )
      pt = NULL;
    else {
      timeout.tv_sec = 0;		/* Do a poll only */
      timeout.tv_usec = 0; 
      pt = &timeout;
    }
    sel = select( FD_SETSIZE, &rfd, NULL, NULL, pt );

    if(sel < 0 ) {
      if( errno != EINTR)
        log_perror("select");
      else if( bnd->interrupt )
        return(-3);
      continue;
    }
    if (tim < 0)
    	return sel;			/* Was only polling */

    if( sel == 0 )
      return(-1);

    if( bnd->bind_fd > 0 && FD_ISSET(bnd->bind_fd, &rfd ))
      accept_sock();

    if( ptr && nptr > 0 )
      for(i=0; i<nptr; i++ )
        if(FD_ISSET( ptr[i], &rfd ))
          return(ptr[i]);

    for(hndl=bnd->head; hndl; hndl = hndl->next )
      if( hndl->fd > 0 ) 
        if( FD_ISSET( hndl->fd, &rfd ))
          if((sel = read_sock(hndl, message, len))>0 )
            return(sel);

    if(rd_in && ! rd_dead && FD_ISSET(0, &rfd )) {
      last_conn = &sock_kbd;
      if( (i=read(0, message, 256 ))<=0) {
          rd_dead = 1;
          return(-2);
      }
      *len = i;
      message[i] = '\0';
      return(0);
    }
  }
}

/*
   blocks for a message on all bound sockets
     ptr is a pointer to an array of additional file descriptors to block on.
     nptr is the length of ptr.
      returns -1 on timeout.
              or the socket number on success
     timeout is in seconds
     the size of message is up to the caller
     returns without reading when the 'ptr' descriptors are detected
*/

int sock_sel(char *message, int *len, int *ptr, int nptr, int tim, int rd_in)
{
  return sock_ssel(NULL, 0, message, len, ptr, nptr, tim, rd_in);
}

int sock_fastsel(char *message, int *len, int *ptr, 
		 int nptr, struct timeval *tim, int rd_in)
{
  fd_set rfd;
  int sel, i;
  static int rd_dead = 0;
  struct SOCK *hndl;

  if( !message )
    return(-3);

  if( bnd->isxview ) {
   log_msg("Can't call sock_ssel if xview enabled");
   return(-3);
  }

  while(1) {

    FD_ZERO(&rfd);
    if( rd_in && !rd_dead )
      FD_SET(0, &rfd );

    if(bnd->bind_fd > 0 )
      FD_SET(bnd->bind_fd, &rfd );

    for(hndl=bnd->head; hndl; hndl = hndl->next ) {
      if( hndl->fd > 0 ) {
        FD_SET( hndl->fd, &rfd );
      }
    }
    if( ptr && nptr > 0 )
      for(i=0; i<nptr; i++ )
        FD_SET( ptr[i], &rfd );

    sel = select( FD_SETSIZE, &rfd, NULL, NULL, tim );

    if(sel < 0 ) {
      if( errno != EINTR)
        log_perror("select");
      else if( bnd->interrupt )
        return(-3);
      continue;
    }

    if( sel == 0 )
      return(-1);

    if( bnd->bind_fd > 0 && FD_ISSET(bnd->bind_fd, &rfd ))
      accept_sock();

    if( ptr && nptr > 0 )
      for(i=0; i<nptr; i++ )
        if(FD_ISSET( ptr[i], &rfd ))
          return(ptr[i]);

    for(hndl=bnd->head; hndl; hndl = hndl->next )
      if( hndl->fd > 0 ) 
        if( FD_ISSET( hndl->fd, &rfd ))
          if((sel = read_sock(hndl, message, len))>0 )
            return(sel);

    if(rd_in && ! rd_dead && FD_ISSET(0, &rfd )) {
      last_conn = &sock_kbd;
      if( (i=read(0, message, 256 ))<=0) {
          rd_dead = 1;
          return(-2);
      }
      *len = i;
      message[i] = '\0';
      return(0);
    }
  }
}


/* sock_only_f is the same as sock_only but with a floating point time out */

int sock_only_f(struct SOCK *handle, char *message, double ftime)
{
  fd_set rfd;
  int sel, len;
  static struct timeval timeout;
  struct timeval *pt;

  if( !handle )
    return(-3);

  if( bnd->isxview ) {
   log_msg("Can't call sock_only if xview enabled");
   return(-3);
  }

  while(1) {

    FD_ZERO(&rfd);
    if( bnd->bind_fd > 0 )
      FD_SET(bnd->bind_fd, &rfd );

    if( handle->fd > 0 )
      FD_SET( handle->fd, &rfd );

    if(ftime > 0 ) {
      timeout.tv_sec = (time_t)ftime;
      timeout.tv_usec = (suseconds_t)(1.e6*(ftime-timeout.tv_sec));
       pt = &timeout;
    } else if( ftime == 0 )
      pt = NULL;
    else
      pt = &timeout;

    if( (sel = select( FD_SETSIZE, &rfd, NULL, NULL, pt )) <0 ) {
      if( errno == EINTR )
        return(-3);
      log_perror("select");
      continue;
    }

    if( sel == 0 )
      return(-1);

    if( bnd->bind_fd > 0 && FD_ISSET(bnd->bind_fd, &rfd ))
      accept_sock();

    if( handle->fd > 0 )
      if( FD_ISSET( handle->fd, &rfd ))
        if((sel = read_sock(handle, message, &len))>0 )
          return(len); /* change return from 'sel' by twf 11/12/02 */
  }
}

int sock_only(struct SOCK *handle, char *message, int tim )
{
  return sock_only_f(handle, message, (double)tim);
}

int sock_poll(struct SOCK *handle, char *message, int *plen )
{
  fd_set rfd;
  int sel;
  static struct timeval timeout;
  struct timeval *pt;

  *plen = 0;
  if( !handle )
    return(-3);

  if( bnd->isxview ) {
   log_msg("Can't call sock_poll if xview enabled");
   return(-3);
  }

  while(1) {

    FD_ZERO(&rfd);
    if( bnd->bind_fd > 0 )
      FD_SET(bnd->bind_fd, &rfd );

    if( handle->fd > 0 )
      FD_SET( handle->fd, &rfd );

    timeout.tv_sec = 0;
    timeout.tv_usec = 0;
    pt = &timeout;

    if( (sel = select( FD_SETSIZE, &rfd, NULL, NULL, pt )) <0 ) {
      if( errno == EINTR )
        return(-3);
      log_perror("select");
      continue;
    }

    if( sel == 0 )
      return(-1);

    if( bnd->bind_fd > 0 && FD_ISSET(bnd->bind_fd, &rfd ))
      accept_sock();

    if( handle->fd > 0 )
      if( FD_ISSET( handle->fd, &rfd ))
        if((sel = read_sock(handle, message, plen))>0 )
          return(sel);
  }
}

void accept_sock(void)
{
  int port;
  char dname[SOCK_DEF_BUFCT];
  struct SOCK sock, *hndl;
  struct DEST *dst;
  unsigned int count, ct, net;
  int n, raw;
  struct sockaddr_in addr;
  struct hostent *hp;
  socklen_t addrlen;

  addrlen = sizeof(struct sockaddr_in);

  if((sock.fd = accept( bnd->bind_fd, (struct sockaddr *)&addr, &addrlen ))<0){
    log_perror("accept_sock, accept");
    return;
  }
  raw = bnd->dest->raw;			/* Non-zero if we are a raw mailbox */

/* Find out if the new connecton is from a `raw' socket mailbox */
  port = ntohs(addr.sin_port);		/* Port that has connected */
  do {
    dst = find_port(port);
    if (dst) {
/* This has the same port number.  See if it also has the same IP address */
      hp = gethostbyname(dst->host);
      if (strncmp(hp->h_addr_list[0], (char *)&addr.sin_addr.s_addr,
							   hp->h_length) == 0) {
	raw = raw || dst->raw;			/* Found it! */
	break;
      }
      port = 0;				/* Continue the search for that port */
    }
  } while (dst);

  if (raw) {
/* Raw socket mailbox */
    if (dst) 
      strcpy(dname, dst->dname);		/* Fetch the mailbox name */
    else
      strcpy(dname, "ANONYMOUS");	/* Didn't find it.  Make it ANONYMOUS */
    count = strlen(dname);
  } else {
/* Normal mailbox */
/* can't use read_sock because I want a timeout on read */

    if( (n=readtm(sock.fd, (void *)&net, SOCK_SIZE_COUNT )) >0 ) {
      count = ntohl(net);
      if( count <= SOCK_DEF_BUFCT ) {
	ct = count;
	while((n=readtm(sock.fd, (void *)&dname[count - ct], ct )) < (int)ct && n>0 )
	  ct -= n;
      } else
	log_msg("read count out of sync\n");
    }

    if( n<0 )
      log_perror("read_sock, read");

    if( n<=0 || count > bnd->bufct ) {
      close(sock.fd);
      sock.fd = -1;
      return;
    }
    dname[count] = '\0';
  }

  if( (hndl = sock_connect(dname)) == 0 )
    hndl = sock_connect("ANONYMOUS"); 

  if (hndl->sin.sin_port == 0)
    hndl->sin = addr;	/* ANONYMOUS socket.  Use values from accept() */

  if( hndl->fd >= 0 )
    close(hndl->fd);

  hndl->fd = sock.fd; 
}

/* just like read with a 5 second timeout */

static int readtm(int fd, void *buf, int len )
{
  fd_set rfd;
  int sel;
  struct timeval timeout;

  FD_ZERO(&rfd);
  FD_SET( fd, &rfd );

  timeout.tv_sec = 5;
  timeout.tv_usec = 0; 

  if( (sel = select( FD_SETSIZE, &rfd, NULL, NULL, &timeout )) <0 ) {
    log_perror("select");
    return(-3);
  }

  if( sel == 0 )
    return(-1);

  return(read( fd, buf, len ));
}

