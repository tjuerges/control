#ifndef VXWORKS

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

#else

#include   "vxWorks.h"
#include   "stdioLib.h"
#include   "systime.h"
#include   "ioLib.h"
#include   "socket.h"
#include   "inetLib.h"
#include   "in.h"
#include   "netinet/tcp.h"
static char *strtok();

#endif

#include "mbdef.h"
#include "sock.h"

static void init_dest();

static int destflag = 0;
static struct DEST mailboxes[MAXDEST];

static struct DEST def_mailboxes[MAXDEST] = {
  {55555, "opt1", "OPT1", 0},	 /* default to these if not found */
  {55555, "opt2", "OPT2", 0},
  {55555, "opt3", "OPT3", 0},
  {55555, "opt4", "OPT4", 0},
  {55555, "opt5", "OPT5", 0},
  {55555, "opt6", "OPT6", 0},
  {55555, "localhost", "OPTSIM", 0},
  {55556, "localhost", "OPT_GUI", 0},
  {55557, "localhost", "OPT_ALMA_GUI", 0},
  {0,    "",          "",	 0}
};


/* Return a pointer to the struct DEST for mailbox `name' (or NULL if none) */

struct DEST *find_dest(const char *name)
{
  int i;

  if( destflag == 0 )
    init_dest();

  for( i=0; i<MAXDEST; i++ ) {
    if( mailboxes[i].dname[0] == '\0' ) {
      break;
    } else if( strcmp( mailboxes[i].dname, name ) == 0 ) {
      return( &mailboxes[i] );
    }
  }

  for( i=0; i<MAXDEST; i++ ) {
    if( def_mailboxes[i].dname[0] == '\0' ) {
      break;
    } else if( strcmp( def_mailboxes[i].dname, name ) == 0 ) {
      return( &def_mailboxes[i] );
    }
  }
  return(NULL);
}


/*
 * Return a pointer to the first struct DEST for a mailbox with a port number 
 * `p' (or NULL if none).  If p=0, continue the search for the previous port 
 * number beginning after the last one found (or NULL if no more found)
 */

struct DEST *find_port(int fport)
{
  static int port, i_standard, i_default;

  if (fport) {
    i_standard = 0;		/* Start from the beginning */
    i_default = 0;
    port = fport;		/* And remember the port number */
  }

  if( destflag == 0 )
    init_dest();

  for( ; i_standard<MAXDEST; i_standard++ )
    if( mailboxes[i_standard].dname[0] == '\0' )
      break;
    else if( mailboxes[i_standard].dport == port )
      return( &mailboxes[i_standard++] );

  for( ; i_default<MAXDEST; i_default++ )
    if( def_mailboxes[i_default].dname[0] == '\0' )
      break;
    else if( def_mailboxes[i_default].dport == port )
      return( &def_mailboxes[i_default++] );

  return(NULL);
}


/* Read the mailboxdefs file and store in mailboxes[] */
static void init_dest(void)
{
  static char delim[] = ", \t\n";
  FILE *fd;
  int i, count;
  char buf[256], *mbptr;

#ifndef VXWORKS
  if( (mbptr = (char *)getenv("MAILBOXDEFS")) == NULL )
#endif
    mbptr = MAILBOXDEFS_DEFAULT;

  bzero( &mailboxes[0], sizeof(struct DEST)*MAXDEST);
  if( (fd = fopen( mbptr, "r" ) )== NULL ) {
    destflag = 1;
    return;
  }

  count = 0;
  while( fgets(buf, 256, fd ) != NULL ) {
    if( buf[0] == '#' || buf[0] == '\n' )
      continue;
    if( (i = atoi(strtok( buf, delim ))) == 0 )
      continue;
    mailboxes[count].dport = abs(i);	/* Ports are always positive */
    mailboxes[count].raw = (i < 0);	/* Negative port means a `raw' socket */
    strncpy(mailboxes[count].host,  strtok(NULL, delim), MAXNAME );
    strncpy(mailboxes[count].dname, strtok(NULL, delim), MAXNAME );

    if( ++count >= MAXDEST ) {
      printf("too many entries in mailboxdefs\n");
      break;
    }
  }
  fclose(fd);
  destflag = 1;
}

/*
    bind before connect to avoid known OS hang bug 
*/

void bind_any(int fd)
{

#ifndef VXWORKS
  int port, ret;
  struct sockaddr_in sin;

  port = 30000;

  while(1) {
    bzero( &sin, sizeof( struct sockaddr_in ) );
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_port = htons(port);

    if((ret = bind(fd, (struct sockaddr *)&sin, sizeof (sin)))>= 0)
      break;

    if( port-- < 1800 || errno != EADDRINUSE )
      break;
  }
  if( ret < 0 )
    perror("bind_any");
#endif
}

#ifdef VXWORKS

/* 
  Jeff version of the unix standard call
*/

static char *strtok(char *s, char *delim)
{
  static char *last = NULL, lastchar;
  char *d, *start, looking;

  if( s == NULL )  {
    if( last == NULL )
      return(NULL);
    s = last;
    *s = lastchar;
  }

  if( s == NULL || delim == NULL || *delim == '\0' )
    return(NULL);

  last = start = NULL;
  looking = 0;
  d = delim;
  while(*s) {
    while(*d) {
      if( *s == *d )
        break;
      d++;
    }
    if( *d ) {
      if( looking ){ /* found it */
        last = s;
        lastchar = *s;
        *s = '\0';
        break; 
      }
    } else if( start == NULL ) {
      start = s; 
      looking = 1;
    }
    
    d = delim;
    s++;
  }

  return(start); 
}

#endif
