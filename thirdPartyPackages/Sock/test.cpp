// test <FITS file>

// Test program to send the FITS file specified to the OPT mailbox

#include <stdio.h>
#include <stdlib.h>

#include "sock.h"

#define MAXFITS 4000000

int main (int argc, char *argv[])
{
  FILE *input;
  char buffer[MAXFITS];
  int n;
  struct SOCK *handle;

  handle = sock_connect("OPT");
  if (handle == NULL) {
    printf ("Could not open OPT mailbox\n");
    exit(1);
  }

  if (argc <= 1) {
    printf("Specify a fits file\n");
    exit(1);
  }
  argv++;
  input = fopen(*argv, "r");
  if (! input) {
    printf("Could not open %s\n", *argv);
    exit(1);
  }

  n = fread(buffer, 1, MAXFITS, input);
  if (n == 0) {
    printf("%s is empty\n", *argv);
    exit(1);
  }

  if (sock_write(handle, buffer, n) == 0) {
    printf("Write to OPT mailbox failed\n");
    exit(1);
  }
  return 0;
}
