#! /bin/awk -f
# Calculate timings for test2 output

BEGIN { tmin = 999999; tmax = 0; sum = 0; num=0; }

/Sending/ {
  sending = substr($1, 7, 6);
}

/FITS/ {
  getting = substr($1, 7, 6);
  diff = getting - sending;
  if (diff < 0) diff += 60;
  if (tmin == 999999)
    first = sending;
  if (tmax < diff)
    tmax = diff;
  if (tmin > diff)
    tmin = diff;
  sum += diff;
  num++;
}
END {
  if (num > 0)
    sum /= num;
  printf("Single read min/mean/max: %.3f/%.3f/%.3f seconds\n", tmin ,sum ,tmax);
  total = getting - first;
  print "Total for " num " Gets: " total " seconds";
}
