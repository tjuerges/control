      SUBROUTINE PANA
      IMPLICIT NONE
C
C     PANA adds entries to the table of contents for the feedhorn rotation
C     routine text message, and the feedhorn rotation angle value.
C
C     Calling sequence -
C             Input variables:  none
C             Output variables: none
C
C 1.2.2 COMMON BLOCKS USED -
C
      INCLUDE 'ccon.i'
C            VARIABLES 'FROM':
C              1.  KPANC  -  The feedhorn rotation routine flow control flag.
C              2.  KPAND  -  The feedhorn rotation routine debug flag.
C
C 1.2.3 PROGRAM SPECIFICATIONS -
C
C 1.2.4 DATA BASE ACCESS -
C            'ADD' Variables:
C              1.  'PAN MESS'  -  The database access code for the
C                                 feedhorn rotation routine text message.
C              2.  'PARANGLE'  -  The database access code for the
C                                 feedhorn rotation angles.
C
C 1.2.5 EXTERNAL INPUT/OUTPUT - None
C
C 1.2.6 SUBROUTINE INTERFACE -
C             CALLER SUBROUTINES: TOCUP
C             CALLED SUBROUTINES: ADDA, ADDR
C
C 1.2.7 CONSTANTS USED - NONE
C
C 1.2.8 PROGRAM VARIABLES - NONE
C
C 1.2.9 PROGRAMMER -
C            GREGG COOKE  05/20/89   (CREATION)
C            Jim Ryan  89.07.07 Comments simplified.
C            Jim Ryan  89.12.12 UNIX-like database interface implimented.
C            Jim Ryan  89:12:16 Bug in Richmond code fixed.
C            D. Gordon 94.06.08 Some format statements fixed, double and single 
C                      quotes reversed.        
C            D. Gordon 95.07.24 Text cleanup
C
C     PANA PROGRAM STRUCTURE
C
C     ADDA for paralactic angle module text message.
      CALL ADDA (1,'PAN MESS','Feedhorn rot. angle mod. ident. ',
     1     40, 1, 1 )
C
C     ADD for paralactic angle module contribution.
      CALL ADDR (2,'PARANGLE','Feedhorn rot. angle, ref and rem',
     1     2, 1, 1 )
C
C     ADD for feed box corrections for standard observables.
      CALL ADDR (2,'FEED.COR','Feedhorn corr. in CORFIL scheme ',
     1     2, 2, 1 )
  500 RETURN
      END
C
C******************************************************************************
      SUBROUTINE PANI
      IMPLICIT NONE
C
C     PANI is the feedhorn rotation angle routine input and initialization
C     section.
C
C 3.1.2 RESTRICTIONS - NONE
C
C 3.1.3 REFERENCES - NONE
C
C 3.2   PANI PROGRAM INTERFACE
C
C 3.2.1 CALLING SEQUENCE - NONE
C
C 3.2.2 COMMON BLOCKS USED -
      INCLUDE 'ccon.i'
C            VARIABLES 'FROM':
C              1.  KPANC  -  THE THEORY ROUTINE FLOW CONTROL FLAG.
C              2.  KPAND  -  THE THEORY ROUTINE DEBUG OUTPUT FLAG.
C
C 3.2.3 PROGRAM SPECIFICATIONS -
      INTEGER*2      LPANG_ON(40),  LPANG_OF(40)
      CHARACTER*40 C_LPANG_ON(2), C_LPANG_OF(2)
      EQUIVALENCE (  LPANG_ON, C_LPANG_ON)
      EQUIVALENCE (  LPANG_OF, C_LPANG_OF)
C
      DATA C_LPANG_ON  /
     .'Feedhorn Rotation Angle Module, Last Mod',
     .'ified 95JUL24, D. Gordon                '/
      DATA C_LPANG_OF  /
     .'Feedhorn rotation angle zeroed out.     ',
     .'                                        '/
C
C 3.2.4 DATA BASE ACCESS -
C            'PUT' VARIABLES:
C              1. LPANG_ON(40) - The parallactic angle routine test message.
C              2. LPANG_OF(40) - The parallactic angle turned OFF message.
C            ACCESS CODES:
C              1. 'PAN MESS' - The data base access code for the feedhorn
C                              rotation routine text message.
C
C 3.2.5 EXTERNAL INPUT/OUTPUT - None
C
C 3.2.6 SUBROUTINE INTERFACE -
C             CALLER SUBROUTINES: INITL
C             CALLED SUBROUTINES: PUTA
C
C 3.2.7 CONSTANTS USED - NONE
C
C 3.2.8 PROGRAM VARIABLES - NONE
C
C 3.2.9 PROGRAMMER - GREGG COOKE  05/20/89  (CREATION)
C            Jim Ryan  89.07.07 Comments simplified.
C            Jim Ryan  89.12.12 UNIX-like database interface implemented.
C            D. Gordon 95.07.24 Text cleanup, 'Zeroed out' message added.
C
C     PANI PROGRAM STRUCTURE
C
C     PUT for paralactic angle routine text message.
      if (kpanc .eq. 0) then
        CALL PUTA ('PAN MESS      ', LPANG_ON, 40, 1, 1 )
      else
        CALL PUTA ('PAN MESS      ', LPANG_OF, 40, 1, 1 )
      endif
C
      RETURN
      END
C
C*****************************************************************************
      SUBROUTINE PANG (AZ, ELEV, STAR, SITLAT, KAXIS, PANGL)
      IMPLICIT NONE
C
C 6.    PANG
C
C 6.1   PANG PROGRAM SPECIFICATION
C
C 6.1.1 PANG is the geometry section of the feedhorn rotation angle
C       module. PANG computes the feedhorn rotation angle due to feed
C       box rotation for each site depending on its antenna type.
C
C 6.1.2 RESTRICTIONS - NONE
C
C 6.1.3 REFERENCES - NONE
C
C 6.2   PANG PROGRAM INTERFACE
C
C 6.2.1 CALLING SEQUENCE -
C       INPUT VARIABLES:
C         1. AZ(2,2)   - The azimuth angle of the source and its CT time
C                        derivative at each observing site. (RAD, RAD/SEC)
C         2. ELEV(2,2) - The elevation angle of the source and its CT time
C                        derivative at each observing site. (RAD, RAD/SEC)
C         3. SITLAT(2) - The site geodetic latitudes (RAD).
C         4. KAXIS(2)  - The antenna axis types for each observing site.
C         5. STAR(3)   - The J2000.0 source unit vector. (unitless)
C       OUTPUT VARIABLES:
C         1. PANGL(2)  - The feedhorn rotation angle for each site.
C
C 6.2.2 COMMON BLOCKS -
      COMMON /CMATH/ PI, TWOPI, HALFPI, CONVD, CONVDS, CONVHS, SECDAY
      REAL*8         PI, TWOPI, HALFPI, CONVD, CONVDS, CONVHS, SECDAY
C
C       VARIABLES 'FROM' :
C         1. TWOPI   - 2 * PI.  (UNITLESS)
C         2. CONVD   - Radians per degree.
C
      INCLUDE 'ccon.i'
C       VARIABLES 'FROM' :
C         1. KPANC  - The feedhorn rotation module flow control flag.
C         2. KPAND  - The feedhorn rotation module debug flag.
C
C 6.2.3  PROGRAM SPECIFICATIONS -
      REAL*8 AZ(2,2), ELEV(2,2), STAR(3), SITLAT(2), PANGL(2),
     .       PANGL_DOT(2), ARG1, ARG1_DOT, ARG2, ARG2_DOT, REF_FREQ,
     .       CDEC, SDEC, CEL, SEL, SLAT, CLAT, CAZ, SAZ, SX, CX,
     .       X_ANGLE_DOT, X_ANGLE, FEED_COR(2,2)
      INTEGER*2 NDO(3), I, KERR, KAXIS(2)
C
C 6.2.4  DATA BASE ACCESS -
C            'PUT' VARIABLES:
C               1. PANGL(2) - Feedbox rotation angles at site 1 and 2 in degrees
C                             of phase.
C             ACCESS CODES:
C               1. 'PARANGLE'- The database access code of the feedbox rotation 
C                              angles.
C
C 6.2.5 EXTERNAL INPUT/OUTPUT - POSSIBLE DEBUG OUTPUT
C
C 6.2.6 SUBROUTINE INTERFACE -
C             CALLER SUBROUTINES: DRIVR
C             CALLED SUBROUTINES: PUT4
C
C 6.2.7 CONSTANTS USED - TWOPI
C
C 6.2.8 PROGRAM VARIABLES -
C
C 6.2.9 PROGRAMMER - GREGG COOKE   05/20/89   (CREATION)
C            Jim Ryan  89.07.07 Comments simplified.
C            Jim Ryan  89.12.12 UNIX-like database interface implimented.
C            Jim Ryan  89.12.28 Feed box rotation rate code added.
C            D. Gordon 95.07.24 Text cleanup, code modified for module turned
C                      OFF option (KPANC=1).
C
C     PANG PROGRAM STRUCTURE
C
C    First check KPANC to see if this module is to be turned off.
      IF (KPANC .EQ. 1) THEN
        PANGL(1) = 0.D0
        PANGL(2) = 0.D0
        PANGL_DOT(1) = 0.D0
        PANGL_DOT(2) = 0.D0
        go to 111
      ENDIF
C
C   Compute SIN and COSINE of source declination.
      SDEC = STAR(3)
      CDEC = DCOS (DASIN (STAR(3) ) )
C
C   Get the reference frequency for the phase delay rate observations
C   from the database. Convert it from MHz to Hz.
      CALL GET4('REF FREQ      ',REF_FREQ,1,1,1,NDO,KERR)
      IF(KERR.NE.0) then
        write(6,'("PAN Module: Failure to obtain ref frequency.")')
        CALL KILL(6HPANG  ,1,KERR)
      Endif
      REF_FREQ = REF_FREQ*1.D6
C
C Calculate the feedhorn rotation angle for each site:
      DO I = 1, 2                                      ! Loop over sites
C
C   Calculate SIN and COS of source azimuth and elevation at each site, and 
C    of each site's geodetic latitude.
        SAZ = DSIN (AZ(I,1))
        CAZ = DCOS (AZ(I,1))
        CEL = DCOS (ELEV(I,1))
        SEL = DSIN (ELEV(I,1))
        SLAT = DSIN (SITLAT(I))
        CLAT = DCOS (SITLAT(I))
C
C    Calculate feedhorn rotation angles for 5 different axis types:
C       1 - Equatorial mount
C       2 - XY mount, fixed axis N-S (MOJAVE)
C       3 - Az-El mount
C       4 - XY mount, fixed axis E-W
C       5 - Richmond, Fla., special case.
C
C    Initialize feedbox rotation angles to zero, which is the
C    feedbox angle for equatorial mount (IAXIS = 1).
        PANGL(I)     = 0.D0
        PANGL_DOT(I) = 0.D0
C
C    Feedhorn rotation angle for XY mount, N-S axis fixed (IAXIS = 2).
        IF (KAXIS(I).EQ.2) THEN                                  ! Kaxis=2
          X_ANGLE =  DATAN2 (SAZ*CEL, SEL)
          SX = DSIN (X_ANGLE)
          CX = DCOS (X_ANGLE)
          X_ANGLE_DOT =  (CX**2)
     .                 * ( CAZ*CEL*AZ(I,2)/SEL
     .                    -SAZ*SEL*ELEV(I,2)/SEL
     .                    -SAZ*(CEL**2)*ELEV(I,2)/(SEL**2) )
C
          ARG1     = -(SLAT * SX) / CDEC
          ARG1_DOT = -(SLAT * CX * X_ANGLE_DOT) / CDEC
C
          ARG2     = -(SX * SDEC * CEL * CAZ - SX * CLAT) /
     .                (CDEC * SAZ * CEL)
          ARG2_DOT = -( ( CX * SDEC * CEL * CAZ * X_ANGLE_DOT
     .                   -SX * SDEC * SEL * CAZ * ELEV(I,2)
     .                   -SX * SDEC * CEL * SAZ * AZ  (I,2)
     .                   -CX * CLAT *             X_ANGLE_DOT)/
     .                   (CDEC * SAZ * CEL)
     .                 + (SX * SDEC * CEL * CAZ - SX * CLAT)
     .                 *(-1D0/(CDEC * SAZ * CEL)**2) *
     .                  ( CDEC * CAZ * CEL * AZ(I,2)
     .                   -CDEC * SAZ * SEL * ELEV(I,2))   )
C
          PANGL(I)     = -DATAN2 (ARG1, ARG2)
          PANGL_DOT(I) = -(DCOS(PANGL(I))**2) *
     .                   (ARG1_DOT*ARG2-ARG1*ARG2_DOT)/(ARG2**2)
        ENDIF                                                    ! Kaxis=2
C
C    Feedhorn rotation angle for AZ-EL mount (IAXIS = 3).
        IF (KAXIS(I).EQ.3) THEN                                  ! Kaxis=3
          ARG1     = -CLAT * SAZ / CDEC
          ARG1_DOT = -CLAT * CAZ * AZ(I,2) /CDEC
C
          ARG2     = - (SDEC * SEL - SLAT) / (CDEC * CEL)
          ARG2_DOT = - (SDEC/CDEC)*ELEV(I,2)
     .               - (SDEC*SEL-SLAT)*SEL*ELEV(I,2)/(CDEC*CEL**2)
 
          PANGL(I) = DATAN2 (ARG1, ARG2)
          PANGL_DOT(I) = (DCOS(PANGL(I))**2) *
     .                   (ARG1_DOT*ARG2-ARG1*ARG2_DOT)/(ARG2**2)
        ENDIF                                                    ! Kaxis=3
C
C    Feedhorn rotation angle for XY mount  E-W fixed axis (IAXIS = 4).
C
C       ** Not yet implimented **
C
C    Feedhorn rotation angle for Richmond, Fla. (IAXIS = 5).
        IF (KAXIS(I).EQ.5) THEN                                  ! Kaxis=5
          SLAT = DSIN ( HALFPI - 39.06*CONVD + SITLAT(I) )
          CLAT = DCOS ( HALFPI - 39.06*CONVD + SITLAT(I) )
C
          ARG1     = -CLAT * SAZ / CDEC
          ARG1_DOT = -CLAT * CAZ * AZ(I,2) /CDEC
C
          ARG2     = -(SDEC * SEL - SLAT) / (CDEC * CEL)
          ARG2_DOT = - (SDEC/CDEC)*ELEV(I,2)
     .               - (SDEC*SEL-SLAT)*SEL*ELEV(I,2)/(CDEC*CEL**2)
C
          PANGL(I) = DATAN2 (ARG1, ARG2)
          PANGL_DOT(I) = (DCOS(PANGL(I))**2) *
     .                   (ARG1_DOT*ARG2-ARG1*ARG2_DOT)/(ARG2**2)
        ENDIF                                                    ! Kaxis=5
C
C    Convert feedhorn rotation angle to degrees and flip the sign
C    per Jim Ray's empirical tests.
        PANGL(I) = -PANGL(I) * 360.D0 / TWOPI
C
C       Convert the feedhorn rotation rate to sec/sec and flip sign.
        PANGL_DOT(I) = (-PANGL_DOT(I)/TWOPI)/REF_FREQ
      ENDDO                                               ! Loop over sites
C
 111   Continue
C
C    PUT the feedhorn rotation angles in the database.
      CALL PUT4 ('PARANGLE      ', PANGL, 2, 1, 1 )
C
C    Generate the feed box rotation corrections for group delay and phase delay
C    rates - the standard VLBI observables. The units are seconds and sec/sec.
C    This is in a form which can be picked up and used by the CORFIL scheme in 
C    SOLVE. Also note that corrections for the group delay are always zero.
C
C    The FEED_COR rates are the derivatives of PANGL with the same sign
C    conventions as PANGL (but with units converted.)  Thus, the corrected rate
C    theoretical = the raw rate theoretical + FEED_COR(1,2) - FEED_COR(2,2).
C
      FEED_COR(1,1) = 0.D0
      FEED_COR(2,1) = 0.D0
      FEED_COR(1,2) = PANGL_DOT(1)
      FEED_COR(2,2) = PANGL_DOT(2)
      CALL PUT4 ('FEED.COR      ', FEED_COR,2,2,1)
C
C    Check KPAND for debug.
      IF ( KPAND .ne. 0 ) then 
      WRITE (6, 9100)
 9100 FORMAT (1X, 'Debug output for subroutine PANG.' )
      WRITE (6,9200) PANGL, SDEC, CDEC, AZ, ELEV, STAR, SITLAT, KAXIS
 9200 FORMAT (1X, 'PANGL = ', 2( D30.16, 10X ), /, 1X,
     1            'SDEC  = ', D30.16, 10X, /, 1X,
     2            'CDEC  = ', D30.16, 10X, /, 1X,
     3            'AZ    = ', 2( 2(D30.16, 10X, /, 1X)), /, 1X,
     4            'ELEV  = ', 2( 2(D30.16, 10X, /, 1X)), /, 1X,
     5            'STAR  = ', 3( D15.8, 10X ), /, 1X,
     6            'SITLAT= ', 2( D30.16, 10X ), /, 1X,
     7            'KAXIS = ', 2I2)
   7  FORMAT(A,5D25.16)
      WRITE(6,7) 'ARG1_DOT    ',ARG1_DOT
      WRITE(6,7) 'ARG2_DOT    ',ARG2_DOT
      WRITE(6,7) 'PANGL_DOT   ',PANGL_DOT
      WRITE(6,7) 'X_ANGLE     ',X_ANGLE
      WRITE(6,7) 'X_ANGLE_DOT ',X_ANGLE_DOT
      endif
C 
      RETURN
      END
C
C******************************************************************************
      SUBROUTINE PANP
C
C     This subroutine is only a 'stub'. It does nothing but return.
C
      RETURN
      END
C******************************************************************************
      SUBROUTINE PANC
C
C     This subroutine is only a 'stub'. It does nothing but return.
C
      RETURN
      END
