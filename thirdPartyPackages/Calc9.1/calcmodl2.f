      SUBROUTINE CALCMODL2 (MJD,TIME,SRCID,STNAID,STNBID,DELAY,RATE,
     *                     U,V,W, ATMOS, DATMOS, ACCEL,
     *                     RISETIME, SETTIME, XELEV, RELEV,
     *                     XAZ, RAZ, PARTIALS, IRETURN)
C-------------------------------------------------------------------
C
C     CALCMODL calls the CALC driver subroutine (DRIVR).
C
      IMPLICIT DOUBLE PRECISION (A-H, O-Z) 
      REAL*8   D_FTOC                                   
      REAL*8   TIME,UTCSEC,DELAY,RATE,U,V,W, TWOPI, XAZ(2), RAZ(2)
      REAL*8   XDELAY,XRATE,SEQ, BSLN(3,2), SRC(3), EARTH(3,3)
      REAL*8   RISETIME,SETTIME,NEXTELEV,SRCELEV(2,2)
      REAL*8   ACCEL,UT1UTC, XPOLE, YPOLE, ATMOS(2,2), DATMOS(2,2)
      REAL*8   XELEV(2), RELEV(2), RC_PLUS_EPS2(3), DLYSAVE(2), EPS2(3)
      REAL*8   CLIGHT, R2_SSB(3), RC_SSB(3), R2_GEO(3), RC_GEO(3)
      REAL*8   BETA(3), BETA2(3), B_SQR, B_SQR2, GAMMA, BDOTR_SSB
      REAL*8   BETADOTSTN, BETADOTRC, PARALLAX, RDIST, BETADOTBETA2
      REAL*8   RSOURCE, ONE_LY, PARTIALS(28)
      REAL*8   EPS_SQR, RCDOTEPS, RCDOTB2, EPS_CURVE(3), DLYCRV
      REAL*8   DEL_CURVE
      INTEGER*4  MJD, SRCID, STNAID, STNBID, YEAR, MONTH, DAY
      INTEGER*4  UTCTAG(5),I, J, ISOL, ITER_SOL, IRETURN
C
      COMMON /CALCM/ LSEG(3,11),NSEG
C
      include 'CALCIO.INC'
      include 'ccon.i'
C
C--------------------------------------------------------------------
C
      IRETURN = 0
      TWOPI = 6.2831853071795864769D0
      CLIGHT = 299792458.0D0
C
C     One light year in meters
      ONE_LY = 9.4605D+15
C
C----------------------------------------------------------------------
C     Normally CALC calls OBSNT to get the observing date and time,
C     stations and source. We to it by hand here.
C----------------------------------------------------------------------
C
      UTCTAG(4) = TIME * 24.0D0 / TWOPI
      UTCTAG(5) = ((TIME* 24.0D0 / TWOPI - DFLOAT(UTCTAG(4)))*60.0D0
     *          + 0.01)
      UTCSEC    = TIME * 86400.D0 / TWOPI - DFLOAT(UTCTAG(4))*3600.D0
     *          - DFLOAT(UTCTAG(5))*60.D0
C      I         = UTCSEC + 0.1
C      UTCSEC    = DFLOAT(I)
C
      CALL MJD2DAY(MJD,YEAR,MONTH,DAY)
      UTCTAG(1) = YEAR 
      UTCTAG(2) = MONTH
      UTCTAG(3) = DAY
      MJDATE    = MJD
C
C     Load date and time into COMMON variable
C
      DO I = 1, 5
         GETTAG(I) = UTCTAG(I)
      END DO
C
      GETSEC = UTCSEC
C
      GETSRC = SRCID + 1
      GETSIT(1) = STNAID + 1
      GETSIT(2) = STNBID + 1
C
C----------------------------------------------------------------------
C     Call the CALC DRIVR subroutine that does it all.
C----------------------------------------------------------------------
C
      CALL DRIVR (BSLN,SRC,EARTH,SRCELEV)
      IF (IRET.EQ.1) THEN
         IRETURN = IRET
         RETURN
      END IF
C
C     Retrieve CALC delay and rate from COMMON variables
      XDELAY = PUTDLY(1) + PUTDLY(2)
      XRATE  = PUTRAT
      DELAY  = XDELAY
      RATE   = XRATE
C
C     Get the Calc atmos delay and rate from common in CALCIO.INC
C     Pass the dry and wet delays and rates. First index is station a
C     or station b, second index is dry atm or wet atm.
C
      ATMOS(1,1)  = ATMDLY(1)
      ATMOS(2,1)  = ATMDLY(2)
C      ATMOS(1,2)  = ATMDLY(3)
C      ATMOS(2,2)  = ATMDLY(4)
C
      DATMOS(1,1) = ATMRATE(1)
      DATMOS(2,1) = ATMRATE(2)
C      DATMOS(1,2) = ATMRATE(3)
C      DATMOS(2,2) = ATMRATE(4)
C
C
C----------------------------------------------------------------------
C     Calculate U,V,W based on source and baseline vectors from CALC.
C     U,V,W are in meters and in the J2000 frame. Diurnal and annual
C     aberration corrections are NOT applied to U,V,W.
C----------------------------------------------------------------------
C
      SEQ = DSQRT(1.0D0 - SRC(3)*SRC(3))
      U = -BSLN(1,1)*SRC(2)/SEQ + BSLN(2,1)*SRC(1)/SEQ
      V = -BSLN(1,1)*SRC(1)*SRC(3)/SEQ - BSLN(2,1)*SRC(2)*SRC(3)/SEQ
     +    +BSLN(3,1)*SEQ
      W =  BSLN(1,1)*SRC(1) + BSLN(2,1)*SRC(2) + BSLN(3,1)*SRC(3)
C
C----------------------------------------------------------------------
C     Calculate the projection of the earth's accelaration (orbital)
C     in the direction of the source
C----------------------------------------------------------------------
C
      ACCEL = SRC(1)*EARTH(1,3)
     *      + SRC(2)*EARTH(2,3)
     *      + SRC(3)*EARTH(3,3)
C
C
C----------------------------------------------------------------------
C     Calculate the source rise and set times. Correlator will flag
C     records when the source is below the horizon.
C----------------------------------------------------------------------
C
C     The VLBA antenna elevation limit is 2.25 degrees
C
      RISETIME = DFLOAT(MJD) + 0.0
      SETTIME  = DFLOAT(MJD) + 1.0
      NEXTELEV = SRCELEV(2,1) + SRCELEV(2,2) * 120.0
      XELEV(1) = PUTEL(1,1)
      XELEV(2) = PUTEL(2,1)
      RELEV(1) = PUTEL(1,2)
      RELEV(2) = PUTEL(2,2)
      XAZ(1)   = PUTAZ(1,1)
      XAZ(2)   = PUTAZ(2,1)
      RAZ(1)   = PUTAZ(1,2)
      RAZ(2)   = PUTAZ(2,2)
C
C     The source is below the elevation limit for the next two mins.
C
      IF (SRCELEV(2,1).LE.3.927D-2 .AND. NEXTELEV.LE.3.927D-2) THEN
         RISETIME = DFLOAT(MJD) + 1.0
         SETTIME  = DFLOAT(MJD) + 2.0
      END IF
C
C     The source rises during the next two minutes.
C
      IF (SRCELEV(2,1).LT.3.927D-2 .AND. NEXTELEV.GE.3.927D-2) THEN
         RISETIME = DFLOAT(MJD) + TIME / TWOPI 
     *            + (3.927D-2 - SRCELEV(2,1)) / (SRCELEV(2,2)*86400.0)
      END IF
C
C     The source sets during the next two minutes.
C
      IF (SRCELEV(2,1).GE.3.927D-2 .AND. NEXTELEV.LT.3.927D-2) THEN
         SETTIME  = DFLOAT(MJD) + TIME / TWOPI 
     *            + (3.927D-2 - SRCELEV(2,1)) / (SRCELEV(2,2)*86400.0)
      END IF
C
C----------------------------------------------------------------------
C     Load the Calc partial derivatives in CALCIO.INC common into the
C     argument variable PARTIALS.
C----------------------------------------------------------------------
C
C      DO I = 1, 2
C         PARTIALS(I)   = DRYATMP(I)
C         PARTIALS(I+2) = WETATMP(I)
C         PARTIALS(I+4) = AXOP(I)
C         PARTIALS(I+6) = SITDLYP(I)
C      END DO
C      DO I = 1, 4
C         PARTIALS(I+8)  = SITDLYP(I+2)
C         PARTIALS(I+12) = SRCDLYP(I)
C         PARTIALS(I+16) = UT1P(I)
C         PARTIALS(I+20) = WOBP(I)
C      END DO
C
C----------------------------------------------------------------------
C     Get the source parallax from the job script structure. If non-zero
C     calculate a near-field delay correction.
C----------------------------------------------------------------------
C
      PARALLAX = D_FTOC (JOBNUM, 'SOURCE', SRCID , 'PARALLAX')  
C      write (6,*) "parallax = ", PARALLAX   
      IF (PARALLAX .LE. 0.0)
     +   GO TO 900
C
C     Calculate the near-field delay correction following the article
C     "Astrometry and Geodesy with Radio Interferomety: Experiments,
C      Models, Results", Sovers, Fanselow, and Jacobs. 1998. 
C     Reviews of Modern Physics, Vol. 4, Oct 1998.
C
C
C     Calculate the distance to the source in meters
C
      RSOURCE = 206265.0D0 * 499.004782D0 * CLIGHT / PARALLAX
C      write (6,*)"rsource = ", RSOURCE
C      write (6,*)"rdist (secs) = ", RSOURCE/CLIGHT
C
C     Don't calculate a near-field correction beyond one light year
C
      IF (RSOURCE .GE. ONE_LY)
     +   GO TO 900
C
C     Calculate BETA (geocentric velocity in solar system barycenter)
C     Calculate BETA2 = BETA + station #2 velocity
C
      B_SQR = 0.0D0
      B_SQR2 = 0.0D0
      DO I = 1, 3
         BETA(I)  = EARTH(I,2) / CLIGHT
         BETA2(I) = BETA(I) - BSLN(I,2) / CLIGHT
         B_SQR    = B_SQR + BETA(I) * BETA(I)
         B_SQR2   = B_SQR2 + BETA2(I) * BETA2(I)
      ENDDO
C
      GAMMA = 1.0D0 / DSQRT (1.0D0 - B_SQR)
C
C     Run the JPL approximation for delta delay due to a curved
C     wavefront
C
      RDIST = RSOURCE
C
C     Load the geocentric frame station and source vectors.
C
         DO I = 1, 3
            R2_GEO(I) = BSLN(I,1)
            RC_GEO(I) = -SRC(I) * RDIST
         ENDDO
C
C
      BETADOTSTN = 0.0D0
      BETADOTRC  = 0.0D0
      BETADOTBETA2 = 0.0D0
      DO I = 1, 3
         BETADOTBETA2 = BETADOTBETA2 + BETA(I) * BETA2(I)
         BETADOTSTN = BETADOTSTN + BETA(I) * R2_GEO(I)
         BETADOTRC  = BETADOTRC  + BETA(I) * RC_GEO(I)
      ENDDO
C
C
C     Transform the station and source vectors into SSB frame.
C     Eqn. 3.158 in above reference.
C
      BDOTR_SSB = 0.0D0
      DO I = 1, 3
         R2_SSB(I) = R2_GEO(I)
     +             + (GAMMA - 1.0D0) * BETADOTSTN * BETA(I) / B_SQR
     +             - GAMMA * BETADOTSTN * BETA2(I)
         RC_SSB(I) = RC_GEO(I)
     +             + (GAMMA - 1.0D0) * BETADOTRC * BETA(I) / B_SQR
     +             - GAMMA * BETADOTRC * BETA2(I)
         BDOTR_SSB = BDOTR_SSB + BETA(I) * R2_SSB(I)
      ENDDO
C
C
C     Use the Calc delay solution as a starting point
C
      DLYCRV = -DELAY * 1.0D-6
C      
	RCDOTEPS = 0.0
        RCDOTB2  = 0.0
        EPS_SQR  = 0.0
	DO I = 1, 3
           EPS_CURVE(I) = -(R2_SSB(I) / CLIGHT + DLYCRV * BETA2(I))
     +                    / (RDIST / CLIGHT)
           RCDOTEPS = RCDOTEPS - RC_GEO(I) * EPS_CURVE(I) / RDIST
           RCDOTB2  = RCDOTB2  - RC_GEO(I) * BETA2(I)
           EPS_SQR  = EPS_SQR  + EPS_CURVE(I) * EPS_CURVE(I) 
        ENDDO
C        write (6,*) "eps_curve = ",EPS_CURVE(1),EPS_CURVE(2),
C     +                             EPS_CURVE(3)
C        write (6,*) "rcdoteps  = ", RCDOTEPS
C        write (6,*) "rcdotb2   = ", RCDOTB2
C
        DEL_CURVE = EPS_SQR 
     +            - (RCDOTEPS*RCDOTEPS)
     +            - (RCDOTEPS*RCDOTEPS*RCDOTEPS)
     +            + (RCDOTEPS*EPS_SQR)                 
C        write (6,*)"del_curve = ", DEL_CURVE
        DELCRV = ((RDIST / CLIGHT) * DEL_CURVE)
     +         / 2.0 * (1.0 - RCDOTB2 / RDIST)
C        write(6,*) "del_c (sec) = ", DELCRV
 
C        write(6,*) "uncorrected dly = ", DELAY
        DELAY = DELAY + DELCRV*1.0E6
C        write(6,*) "corrected dly = ", DELAY

      ITER_SOL = 0;
      IF (ITER_SOL .EQ. 1) THEN
C
C     Loop through the near-field delay calculations twice.
C     Once for a source distance of 1.0 ly for plane-wave 
C     solution, and again at the actual source distance. Take
C     difference between the two solutions and apply it as a
C     correction to the Calc plane-wave delay.
C
      DO ISOL = 1, 2
C
         IF (ISOL .EQ. 1) RDIST = 1.0 * 9.4605D+15
         IF (ISOL .EQ. 2) RDIST = RSOURCE
C
C     Load the geocentric frame station and source vectors.
C
         DO I = 1, 3
            R2_GEO(I) = BSLN(I,1)
            RC_GEO(I) = -SRC(I) * RDIST
         ENDDO
C
         BETADOTSTN = 0.0D0
         BETADOTRC  = 0.0D0
         BETADOTBETA2 = 0.0D0
         DO I = 1, 3
            BETADOTBETA2 = BETADOTBETA2 + BETA(I) * BETA2(I)
            BETADOTSTN = BETADOTSTN + BETA(I) * R2_GEO(I)
            BETADOTRC  = BETADOTRC  + BETA(I) * RC_GEO(I)
         ENDDO
C
C     Transform the station and source vectors into SSB frame.
C     Eqn. 3.158 in above reference.
C
         BDOTR_SSB = 0.0D0
         DO I = 1, 3
            R2_SSB(I) = R2_GEO(I)
     +                + (GAMMA - 1.0D0) * BETADOTSTN * BETA(I) / B_SQR
     +                - GAMMA * BETADOTSTN * BETA2(I)
            RC_SSB(I) = RC_GEO(I)
     +                + (GAMMA - 1.0D0) * BETADOTRC * BETA(I) / B_SQR
     +                - GAMMA * BETADOTRC * BETA2(I)
            BDOTR_SSB = BDOTR_SSB + BETA(I) * R2_SSB(I)
         ENDDO
C
         IF (ISOL .EQ. 20) THEN
            write (6,2001) "SRC      = ", SRC
            write (6,2002) "R2_VELS  = ", 
     +                      BSLN(1,2),BSLN(2,2),BSLN(3,2)
            write (6,2002) "VEARTH   = ", 
     +                      EARTH(1,2),EARTH(2,2),EARTH(3,2)
            write (6,2000) "R2_GEO   = ", R2_GEO
            write (6,2000) "RC_GEO   = ", RC_GEO
            write (6,2000) "R2_SSB   = ", R2_SSB
            write (6,2000) "RC_SSB   = ", RC_SSB
            write (6,2001) "BETA     = ", BETA
            write (6,2001) "BETA2    = ", BETA2
 2000       format (1x,A,f16.2,f16.2,f16.2)
 2001       format (1x,A,d16.8,d16.8,d16.8)
 2002       format (1x,A,f14.6,f14.6,f14.6)
         END IF
C
C     Use the Calc delay solution as a starting point
C
         DLYCRV = -DELAY * 1.0D-6
         DO I = 1, 5
            DO J = 1, 3
               EPS2(J) = R2_SSB(J) - DLYCRV * BETA2(J) * CLIGHT
               RC_PLUS_EPS2(J) = EPS2(J) + RC_SSB(J)
            ENDDO
C
C     Calculate the geocentric delay of the curved wavefront.
C     Still in SSB frame.
C         
            DLYCRV = DSQRT (RC_PLUS_EPS2(1) * RC_PLUS_EPS2(1)
     +                   +  RC_PLUS_EPS2(2) * RC_PLUS_EPS2(2)
     +                   +  RC_PLUS_EPS2(3) * RC_PLUS_EPS2(3))
     +             - DSQRT (RC_SSB(1) * RC_SSB(1)
     +                   +  RC_SSB(2) * RC_SSB(2)
     +                   +  RC_SSB(3) * RC_SSB(3))
            DLYCRV = DLYCRV / CLIGHT
         ENDDO
C
C     Transform the delay from SSB frame back to geocentric frame.
C
         DLYCRV = GAMMA * (1.0D0 - BETADOTBETA2) * DLYCRV
     +          - GAMMA * BDOTR_SSB / CLIGHT
C
         DLYSAVE(ISOL) = DLYCRV
C
      ENDDO
C
C     Near-field delay correction
C
      DLYCRV = DLYSAVE(2) - DLYSAVE(1)
C
C     Apply the correction to the Calc delay
C
      DELAY = DELAY - DLYCRV * 1.0D+06
      END IF
C
 900  CONTINUE
      RETURN
      END                                                                     















