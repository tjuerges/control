/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2005 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2005-07-30  created 
*/

#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestCaller.h>
#include <cppunit/ui/text/TestRunner.h>

#include <stdlib.h>
#include <calcExec.h>
#include <structures.h>
#include <iomanip>

class CalcExecTest : public CppUnit::TestFixture
{
public:
  CalcExecTest() : CppUnit::TestFixture(){}
  CalcExecTest( const CalcExecTest &toCopy){*this=toCopy;}
  CalcExecTest &operator=(const CalcExecTest&)
     {return *this;}
  ~CalcExecTest(){};

  void setUp() {
    setenv("CALC_USER","C",1); // Set for Correlator usage
    setenv("WET_ATM","N",1);   // By default turn off the Wet Atmosphere

    // The Ephemeris is in ../config get the full path
    char jplPath[255];
    sprintf(jplPath,"%s/../config/JPLEPH", getenv("PWD"));
    setenv("JPLEPH",jplPath,1); // Set for Correlator usage

  }


  void tearDown() {
      }

  void testEnvironment() {
    char* envValue;
    envValue = getenv("CALC_USER");
    CPPUNIT_ASSERT_MESSAGE("CALC_USER Environment not found",
                           envValue != NULL);
    CPPUNIT_ASSERT_MESSAGE("CALC_USER Environment not set correctly",
                           strcmp(envValue, "C") == 0);
    
    envValue = getenv("WET_ATM");
    CPPUNIT_ASSERT_MESSAGE("WET_ATM Environment not found",
                           envValue != NULL);
    CPPUNIT_ASSERT_MESSAGE("WET_ATM Environment not set correctly",
                           strcmp(envValue, "N") == 0);
                           
    envValue = getenv("JPLEPH");
    CPPUNIT_ASSERT_MESSAGE("JPLEPH Environment not found",
                           envValue != NULL);
  }


  void testExecution() {
    const double standardResult = -0.019105144421136;

    getCALC_arg calcArg;
    CALCRecord  calcRecord;

    getDefaultCalcArg(calcArg);
    
    // Put the source directly over the antenna (El =1.5708)
    calcArg.ra = 2.00406;
    calcExec(&calcArg, &calcRecord);
       
    CPPUNIT_ASSERT_MESSAGE("Calculated delay is outide acceptable range",
                           fabs(standardResult - calcRecord.delay[0]) < 
                           1E-9);
  }

  
  static CppUnit::Test *suite(){
    CppUnit::TestSuite *suiteOfTests = new CppUnit::TestSuite("CalcExecTest");
   
    suiteOfTests->addTest(new CppUnit::TestCaller<CalcExecTest>( 
                          "testEnvironment", 
                          &CalcExecTest::testEnvironment));
    suiteOfTests->addTest(new CppUnit::TestCaller<CalcExecTest>( 
                          "testExecution", 
                          &CalcExecTest::testExecution));
    return suiteOfTests;
  }

  void getDefaultCalcArg(getCALC_arg& arg) {
    arg.request_id = 0;        /* RPC request id number, user's choice */
    arg.date = 54882;          /* CALC model date (MJD) */
    arg.ref_frame = 0;         /* CALC reference frame: 0 = geocentric */
    //long.dummy;
    //    short int kflags[64];   /* CALC model component control flags */
    arg.time = 0.9;            /* CALC model time UTC (fraction of day) */

    arg.a_x = 0.0;             /* geocentric right-hand x coord (meters) */
    arg.a_y = 0.0;             /* geocentric right-hand y coord (meters) */
    arg.a_z = 0.0;             /* geocentric right-hand z coord (meters) */
    arg.axis_off_a = 0.0;      /* non-intersecting axis offset (meters) */
    arg.b_x = 6000000.0;       /* geocentric right-hand x coord (meters) */
    arg.b_y = 0.0;       /* geocentric right-hand y coord (meters) */ 
    arg.b_z = 0.0;             /* geocentric right-hand z coord (meters) */
    arg.axis_off_b = 0.0;      /* non-intersecting axis offset (meters) */
    arg.ra = 0.0;              /* J2000.0 coordinates (radians) */
    arg.dec = 0.0;             /* J2000.0 coordinates (radians) */
    arg.dra = 0.0;             /* J2000.0 arcsecs/year */
    arg.ddec = 0.0;            /* J2000.0 arcsecs/year */
    arg.depoch = 51544.5;          /* reference date for which proper motion
                                    * corrections are zero, mjd.fract_day */
    arg.parallax = 0.0;        /* source distance in arcsecs of annual
                                * parallax, = 206265.0 / distance (au) */

    for (int idx = 0; idx < 5; idx++) {
      arg.EOP_time[idx] = arg.date + idx - 1;  /* EOP epoch date.time (MJD) */
      arg.tai_utc[idx] = 0.0;       /* TAI - UTC (secs) */
      arg.ut1_utc[idx] = 0.0;      /* UT1 - UTC (secs) */
      arg.xpole[idx] = 0.0;        /* earth pole offset, x (arcsec) */
      arg.ypole[idx] = 0.0;        /* earth pole offset, y (arcsecs) */
    }
    
    arg.pressure_a = 0.0;      /* surface pressure stna (millibars) 
                                * enter 0.0 for none availiable */
    arg.pressure_b = 0.0;      /* surface pressure stnb (millibars) 
                                * enter 0.0 for none availiable */

    arg.station_a = strdup("Reference");       /* station A name */
    arg.axis_type_a = strdup("altz");  /* station A mount type, 'altz', 'equa',
                                          ,xyns', 'xyew' */
    arg.station_b = strdup("Antenna");       /* station B name */
    arg.axis_type_b = strdup("altz"); /* station B mount type, 'altz', 'equa',
                                         'xyns', 'xyew' */
    arg.source= strdup("Source");          /* source name */

    arg.kflags[0] = -1; // Use VLBA default setup for CALC.
    for(int i = 1; i < 64; i++){
      arg.kflags[i] = 0;//Initialize the flags to zero
    }
  }

  void printCalcArg(getCALC_arg& arg) {
    std::cout << "CALC Request Arguments Dump: " << std::endl;
    std::cout << "\trequest_id: " << arg.request_id << std::endl;
    std::cout << "\tdate: " << arg.date << std::endl;
    std::cout << "\tref_frame: " << arg.ref_frame << std::endl;
    std::cout << "\ttime: " << arg.time << std::endl;

    std::cout << "\tAntenna A (x,y,z, k): (" << arg.a_x << ", "
              << arg.a_y << ", " 
              << arg.a_z << ", "
              << arg.axis_off_a << ")" << std::endl;
    
    std::cout << "\tAntenna A (x,y,z, k): (" << arg.b_x << ", "
              << arg.b_y << ", " 
              << arg.b_z << ", "
              << arg.axis_off_b << ")" << std::endl;
    
    std::cout << "\tRA (dRA):\t" << arg.ra << "\t("
              << arg.dra << ")" << std::endl;
    std::cout << "\tDec (dDec):\t" << arg.dec << "\t("
              << arg.ddec << ")" << std::endl;
    std::cout << "\tdepoch: " << arg.depoch << std::endl;
    std::cout << "\tparallax: " << arg.parallax << std::endl;

    std::cout << "\tIERS Data:" << std::endl;
    std::cout << "\t\t MJD        tai_utc   ut1_utc       xpole       ypole" 
              << std::endl;
    for (int idx = 0; idx < 5; idx++) {
      std::cout << "\t\t" 
                << std::setw(5)  << arg.EOP_time[idx] << "    "
                << std::setw(7)  << arg.tai_utc[idx] << "     " 
                << std::setw(8) << arg.ut1_utc[idx] << "    "
                << std::setw(8) << arg.xpole[idx] << "    " 
                << std::setw(8) << arg.ypole[idx] << std::endl;
    }

    std::cout << "\tpressure a/b: " << arg.pressure_a << "\t"
              << arg.pressure_b << std::endl;
    std::cout << "\tStation A (type): " << arg.station_a << "\t("
              << arg.axis_type_a << ")" << std::endl;
    std::cout << "\tStation B (type): " << arg.station_b << "\t("
              << arg.axis_type_b << ")" << std::endl;
    std::cout << "\tSource: " << arg.source << std::endl;
  }
    
  void printCalcRecord(CALCRecord& arg){
    std::cout << "CALC Result Structure Dump: " << std::endl;
    std::cout << "\trequest_id: " << arg.request_id << std::endl;
    std::cout << "\tdate: " << arg.date << std::endl;
    std::cout << "\ttime: " << arg.time << std::endl;

    std::cout << "\tDelay (dDelay): " << arg.delay[0] << "\t(" 
              << arg.delay[1] << ")" <<std::endl;
    std::cout << "\tDry Atmosphere: " << arg.dry_atmos[1] <<"\t("
              << arg.dry_atmos[3] << ")" << std::endl;
    std::cout << "\tWet Atmosphere: " << arg.wet_atmos[1] <<"\t("
                   << arg.wet_atmos[3] << ")" << std::endl;

    std::cout << "\t\tAz\t\tEl" << std::endl 
              << "\t" << arg.az[0] << " (" << arg.az[2] << ")" << "\t"
              << "\t" << arg.el[0] << " (" << arg.el[2] << ")" << std::endl  
              << "\t" << arg.az[1] << " (" << arg.az[3] << ")" << "\t"
              << "\t" << arg.el[1] << " (" << arg.el[3] << ")" << std::endl;

    //double UV[3];      /* u, v, w coordinates in J2000.0 frame (meters) */
    //double riseset[2]; /* estimated rise, set times for stnb VLBA Correlator */
  }

};

int main( int argc, char **argv)
{
  CppUnit::TextUi::TestRunner runner;
  runner.addTest( CalcExecTest::suite() );
  runner.run();
  return 0;
}
