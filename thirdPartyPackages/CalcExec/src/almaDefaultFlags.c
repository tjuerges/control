


void getALMAFlags(short int* kflags) {
  /* These flags are based on the flags defined by the VLBA
     modify only if you are sure you know what your doing */

  
  /* KATMC: kflags[00]
     KATMC = -1 Special flag to use VLBA settings
     KATMC = 0 turn off application of atmospheric delay contribution
     but still calculate the atmopheric delays.
     KATMC = 1 apply atmospheric delays to CALC total delay
  */
  kflags[0] = 0;

  /* KATMD: kflags[01]
   */
  kflags[1] = 0;

  /* KAXOC: kflags[02]
   */
  kflags[2] = 0;

  /* KAXOD: kflags[03]
   */
  kflags[3] = 0;

  /* KPTDC: kflags[04]
   */
  kflags[4] = 0;

  /* KPTDD: kflags[05]
   */
  kflags[5] = 0;

  /* KDNPC: kflags[06]
   */
  kflags[6] = 0;

  /* KDNPD: kflags[07]
   */
  kflags[7] = 0;

  /* KETDC: kflags[08]
     Switch off earth tides for speed test, KETDC = 1
   */
  kflags[8] = 0;

  /* KETDD: kflags[09]
   */
  kflags[9] = 0;

  /* KIONC: kflags[10]
   */
  kflags[10] = 0;

  /* KIOND: kflags[11]
   */
  kflags[11] = 0;

  /* KNUTC: kflags[12]
     Nutation model: = 0, new 1996 IERS model,
                     = 2, old WAHR model
  */
  kflags[12] = 0;

  /* KNUTD: kflags[13]
   */
  kflags[13] = 0;

  /* KPREC: kflags[14]
   */
  kflags[14] = 0;

  /* KPRED: kflags[15]
   */
  kflags[15] = 0;

  /* KRELC: kflags[16]
   */
  kflags[16] = 0;

  /* KRELD: kflags[17]
   */
  kflags[17] = 0;

  /* KSITC: kflags[18]
   */
  kflags[18] = 0;

  /* KSITD: kflags[19]
   */
  kflags[19] = 0;

  /* KSTRC: kflags[20]
     Do not use proper motions, KSTRC = 0
     KSTRC = 0
     KSTRC = 2
     KSTRC = 3
  */
  kflags[20] = 0;

  /* KSTRD: kflags[21]
   */
  kflags[21] = 0;

  /* KUT1C: kflags[22]
   */
  kflags[22] = 0;

  /* KUT1D: kflags[23]
   */
  kflags[23] = 0;

  /* KWOBC: kflags[24]
   */
  kflags[24] = 0;

  /* KWOBD: kflags[25]
   */
  kflags[25] = 0;

  /* KUTCC: kflags[26]
   */
  kflags[26] = 0;

  /* KUTCD: kflags[27]
   */
  kflags[27] = 0;



  /* KATIC: kflags[28]
   */
  kflags[28] = 0;

  /* KATID: kflags[29]
   */
  kflags[29] = 0;

  /* KCTIC: kflags[30]
   */
  kflags[30] = 0;

  /* KCTID: kflags[31]
   */
  kflags[31] = 0;

  /* KPEPC: kflags[32]
     Get solar system values from calls to JPL DE403, = 0
     Get solar system values from calls to GET4, = 1
     Remember, for KPEPC = 1 to work, cpepumod.f must be substituted
     for cpepu.f...
  */
  kflags[32] = 0;

  /* KPEPD: kflags[33]
   */
  kflags[33] = 0;

  /* KDIUC: kflags[34]
   */
  kflags[34] = 0;

  /* KDIUD: kflags[35]
   */
  kflags[35] = 0;

  /* KM20C: kflags[36]
   */
  kflags[36] = 0;

  /* KM20D: kflags[37]
   */
  kflags[37] = 0;

  /* KROSC: kflags[38]
   */
  kflags[38] = 0;

  /* KROSD: kflags[39]
   */
  kflags[39] = 0;

  /* KSTEC: kflags[40]
   */
  kflags[40] = 0;

  /* KSTED: kflags[41]
   */
  kflags[41] = 0;

  /* KSUNC: kflags[42]
   */
  kflags[42] = 0;

  /* KSUND: kflags[43]
   */
  kflags[43] = 0;

  /* KSARC: kflags[44]
   */
  kflags[44] = 0;

  /* KSARD: kflags[45]
   */
  kflags[45] = 0;

  /* KTHEC: kflags[46]
   */
  kflags[46] = 0;

  /* KTHED: kflags[47]
   */
  kflags[47] = 0;

  /* KMATC: kflags[48]
   */
  kflags[48] = 0;

  /* KMATD: kflags[49]
   */
  kflags[49] = 0;

  /* KVECC: kflags[50]
   */
  kflags[50] = 0;

  /* KVECD: kflags[51]
   */
  kflags[51] = 0;

  /* KOCEC: kflags[52]
     Ocean loading: = 0, calculate but don't apply, 
                    = 1, turned off
                    = 2, calculate and apply to delays
     IF Calc_user.eq.'C' .and. Apply_ocean.eq.'Y' applies the ocean
     loading regardless of the KOCEC swx, except for KOCEC = 1 (off).
   */
  kflags[52] = 2;

  /* KOCED: kflags[53]
   */
  kflags[53] = 0;

  /* KASTC: kflags[54]
     Turn on the calculation of U and V. KASTC = 1, on.
     Calc 9.1 U and V do not use aberrated start position.
  */
  kflags[54] = 0;

  /* KASTD: kflags[55]
   */
  kflags[55] = 0;

  /* KSTAC: kflags[56]
   */
  kflags[56] = 0;

  /* KSTAD: kflags[57]
   */
  kflags[57] = 0;

  /* KPLXC: kflags[58]
     Turn parallax module off
  */
  kflags[58] = 0;

  /* KPLXD: kflags[59]
   */
  kflags[59] = 0;

  /* KPANC: kflags[60]
     Feed horn rotation turned off
  */
  kflags[60] = 1;

  /* KPAND: kflags[61]
   */
  kflags[61] = 0;

  /* Kspace: kflags[62]
   */
  kflags[62] = 0;
  kflags[63] = 0;
}
