#ifndef CALCVLB_H
#define CALCVLB_H

#include "MATHCNST.H"
#include "STDDEFS.H"
#include <string.h>
#include <stdio.h>

/* File: time2str.c */
char *timeMjd2str(double inTime, char *pOutStr);
char *time2str(double inTime, char *pFormat, char *pOutStr);
char *rad2str(double angle, char *pFormat, char *pOutStr);
char *rad2strg(double angle, char *pFormat, char *pOutStr, BOOL roundFlag);

/* File: mjd2str.c */
char *mjd2str(long mjd, char *pstring);
STATUS mjd2dayno(long mjd, int *pDayNo);
STATUS mjd2date(long mjd, int *pYear, int *pMonth, int *pDay);	
#endif //CALCVLB_H

