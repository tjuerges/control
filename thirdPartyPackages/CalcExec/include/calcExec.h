#ifndef CALCEXEC_H
#define CALCEXEC_H

#include <MATHCNST.H>
#include <STDDEFS.H>
#include <stdio.h>    /* defines stderr */
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <structures.h>     /* RPCGEN creates this from CALCServer.x */
#define INCocean
#include <ocean.h>
#include <calcVlb.h>

/* externals */
int  calcinit_ ();
int  calcmodl2_ ();

struct JPL_Horizons_Tbl {
       double  ref_time;   /* Reference time (Julian Date) */
       double  ra;         /* Source RA (J2000/ICRF) */
       double  dec;        /* Source DEC (J2000/ICRF) */
       double  delta;      /* Geocentric range in AU */
       double  deldot;     /* Geocentric delta range in km/sec */
} solar_target[5000], *p_ss_targ;

int  n_Horizons_rows;
int  ilog_Horizons = 1;
int  iparallax = 1;
char Horizons_srcname[24];
char Horizons_filename[64];

int  ifirst, ilogdate = 1;
FILE  *flog;
char spawn_time[64];
char Version[48];
getCALC_arg  *p_request;

/* internals */
char *cvrtuc ();
char *cvrtlc ();
void date2str ();
double vla_refraction();

extern "C" {
/*++****************************************************************************
*/
void calcExec (getCALC_arg*, CALCRecord*);

/* Method to set the default ALMA behavior of the CALC program */
void getALMAFlags(short int*);

/*++***********************************************************************/
double d_mathcnst_ ( char*, long);

/*++***********************************************************************/
double d_ftoc_ ( int*, char*, int*, char*, long, long);
 
/*++***********************************************************************/
double mjd_ftoc_ ( int*, char*, int*, char*, long, long);
 
/*++***********************************************************************/
double f_ftoc_ ( int*, char*, int*, char*, long, long);

/*++***********************************************************************/
int i_ftoc_ ( int*, char*, int*, char*, long, long);

/*++***********************************************************************/
void c_ftoc_( char*, long, int*, char*, int*, char*, long, long); //UNFINISHED?

/*++***********************************************************************/
int n_rows_ ( int*, char*, long);

/*++****************************************************************************
*/
double d_fetch ( int, char*, int, char*, char*);

/*++**************************************************************************/
float f_fetch ( int, char*, int, char*, char*);

/*++****************************************************************************
*/
int i_fetch (int, char*, int, char*, char*);

/*++****************************************************************************
*/
char *c_fetch (int, char*, int, char*, char*);

/*++****************************************************************************
*/
double mjd_fetch (int, char*, int, char*, char*);

/*******************************************************************************
*/
char *cvrtuc		/* convert to upper case */
    (
    char *		/* pointer to string to be converted */
    );

/*******************************************************************************
*/
char *cvrtlc		/* convert to lower case */
    (
    char*		/* pointer to string to be converted */
    );

/*******************************************************************************
*/
STATUS loadOcean(char*, int);

/*++****************************************************************************
*/
int  oce_fetch_ ( int*, int*, double*);
/*******************************************************************************
*/
void            date2str (double, char*);
/*++****************************************************************************
*/
void c1write_ (char*, long);
/*++****************************************************************************
*/
void c1log_ (char*, long);

/*++****************************************************************************
*/
void c2write_ ( char*, char*, long, long);
/*++****************************************************************************
*/
void cnwrite_ ( char*, char*, short*, long, long);
/*++****************************************************************************
*/
void cfwrite_ ( char*, char*, short*, long, long);
/*++****************************************************************************
*/
void c2logerror_ ( char*, char*, long, long);

/*++****************************************************************************
*/
void d1write_ ( char*, double*, long);

/*++****************************************************************************
*/
void d2write_ ( char*, double*, double*, long);
/*++****************************************************************************
*/
void c2i2writ_ ( char*, char*, int*, int*, long, long);

/*++****************************************************************************
*/
void d10write_ (char*, double*, double*, double*, double*,
		double*, double*, double*, double*,
		double*, double*, long);

/*++****************************************************************************
*/
void r1write_ ( char*, float*, long);

/*++****************************************************************************
*/
void i1write_ ( char*, int*, long);

/*++****************************************************************************
*/
void i2write_ ( char*, int*, int*, long);

/*++****************************************************************************
*/
int fstr2cpy_ (char*, char*, long, long);

/*++****************************************************************************
*/
int fstrcmp_ (char*, char*, long, long);

/*++****************************************************************************
*/
int fstrcpy_ (char*, char*, long, long);
/*******************************************************************************
*/
double         asteroid (double, int);
/*******************************************************************************
*/
double         ast_dopplr (double, double, double, double);
/*******************************************************************************
*/
double         vla_refraction (double, double);

}
#endif //CALCEXEC_h
