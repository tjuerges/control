/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ControlMasterComponent.java
 */
package alma.Control.Master;

import java.util.logging.Logger;

import alma.ACS.MasterComponentImpl.MasterComponentImplBase;
import alma.ACS.MasterComponentImpl.statemachine.AlmaSubsystemActions;
import alma.Control.Master2;
import alma.Control.Master2Helper;
import alma.Control.SystemState;
import alma.Control.SystemSubstate;
import alma.Control.Common.Name;
//import alma.Control.Common.Result;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.acs.container.ContainerServices;
import alma.acs.genfw.runtime.sm.AcsStateActionException;

public class ControlMasterComponent2 extends MasterComponentImplBase 
        implements AlmaSubsystemActions {

    private static final String pgmname = "ControlMasterComponent";
    private Master2 master;
//    private Master master;
    private ContainerServices container;
    private Logger logger;

    /**
     * Create a Control System Component.
     */
    public ControlMasterComponent2() {
        super();
        master = null;
        container = null;
        logger = null;
    }

    private void info(String msg) {
        m_logger.fine(pgmname + ": " + msg);
    }

    private void err(String msg) {
        m_logger.severe(pgmname + ": " + msg);
    }

    private void error(String msg, String pass) throws AcsStateActionException {
        String s = pgmname + " " + pass + ": " + msg;
        m_logger.severe(s);
        throw new AcsStateActionException(s);
    }
    
    private String createMaster() {
        master = null;
        try {
            org.omg.CORBA.Object x = container.getComponent(Name.Master);
            if (x == null)
                return "Cannot get " + Name.Master + " from container!";
            master = Master2Helper.narrow(x);
//            master = MasterHelper.narrow(x);
            if (master == null)
                return "Could not create master.  MasterHelper.narrow() failed!";
        } catch (AcsJContainerServicesEx err) {
            return err.toString();
        } catch (Exception err) {
            return err.toString();
        }
        info("Control's master created.");
        return null;
    }

    private void releaseMaster() {
        if (container != null && master != null) {
            try {
                container.releaseComponent(Name.Master);
            } catch (Exception e) {
                err("Error releasing " + Name.Master + " component!");
                master = null;
                // return Result.Fail_Continue;
            }
            master = null;
        }
        // return Result.Success_Continue;
    }
    
    /**
     * @see alma.ACS.MasterComponentImpl.MasterComponentImplBase#getActionHandler()
     */
    protected AlmaSubsystemActions getActionHandler() {
        info("GetActionHandler() method called");
        return this;
    }

    ///////////////////////////////////////////////////////
    // Implementation of AlmaSubsystemActions            //
    ///////////////////////////////////////////////////////

    /**
     * @see alma.ACS.MasterComponentImpl.statemachine.AlmaSubsystemActions#initSubsysPass1()
     */
    public void initSubsysPass1() throws AcsStateActionException {
        info("initSubsysPass1() method called");
        container = getComponentContainerServices();
        logger = container.getLogger();
        String err = createMaster();
        if (err != null) {
            throw new AcsStateActionException("Error creating Control's internal master component: " + 
                    Name.Master + "  " + err);
        }
        try {
            master.startupPass1();
        } catch (Exception e) {
            throw new AcsStateActionException("Error initializing Control's internal master component: " + 
                    Name.Master + "  " + e.toString());            
        }
        if (master.getMasterState() != SystemState.INACCESSIBLE &&
            master.getMasterSubstate() != SystemSubstate.STARTED_UP_PASS1) {
            throw new AcsStateActionException("Error initializing Control's internal master component: " + 
                    Name.Master + "  See ACS log.");
        }
     }
    
    /**
     * @see alma.ACS.MasterComponentImpl.statemachine.AlmaSubsystemActions#initSubsysPass2()
     */
    public void initSubsysPass2() throws AcsStateActionException {
        info("initSubsysPass2() method called");
        if (master == null)
            throw new AcsStateActionException("Cannot access Control's internal master component: " +
                    Name.Master);
        try {
            master.startupPass2();
        } catch (Exception e) {
            throw new AcsStateActionException("Error initializing Control's internal master component: " + 
                    Name.Master + "  " + e.toString());            
        }
        if (master.getMasterState() != SystemState.OPERATIONAL) {
            throw new AcsStateActionException("Error initializing Control's internal master component: " + 
                    Name.Master + "  See ACS log.");
        }
    }
    
    /**
     * @see alma.ACS.MasterComponentImpl.statemachine.AlmaSubsystemActions#reinitSubsystem()
     */
    public void reinitSubsystem() throws AcsStateActionException {
        info("ReinitSubsystem() method called.");
        if (master == null)
            throw new AcsStateActionException("Cannot access Control's internal master component: " + Name.Master);
        try {
            shutDownSubsysPass1();
            shutDownSubsysPass2();
            initSubsysPass1();
            initSubsysPass2();
        } catch (Exception e) {
            throw new AcsStateActionException("Error re-initializing Control's internal master component: " + 
                    Name.Master + "  " + e.toString());            
        }
        if (master.getMasterState() != SystemState.OPERATIONAL) {
            throw new AcsStateActionException("Error initializing Control's internal master component: " + 
                    Name.Master + "  See ACS log.");
        }
    }
    
    /**
     * @see alma.ACS.MasterComponentImpl.statemachine.AlmaSubsystemActions#shutDownSubsysPass1()
     */
    public void shutDownSubsysPass1() throws AcsStateActionException {
        info("shutDownSubsysPass1() method called.");
        if (master == null)
            throw new AcsStateActionException("Cannot access Control's internal master component: " + Name.Master);
        try {
            master.shutdownPass1();
        } catch (Exception e) {
            throw new AcsStateActionException("Error shutting down Control's internal master component: " + 
                    Name.Master + "  " + e.toString());            
        }
        if (master.getMasterState() != SystemState.INACCESSIBLE &&
                master.getMasterSubstate() != SystemSubstate.SHUT_DOWN_PASS1) {
            throw new AcsStateActionException("Error shutting down Control's internal master component: " + 
                    Name.Master + "  See ACS log.");
        }
    }
    
    /**
     * @see alma.ACS.MasterComponentImpl.statemachine.AlmaSubsystemActions#shutDownSubsysPass2()
     */
    public void shutDownSubsysPass2() throws AcsStateActionException {
        info("shutDownSubsysPass2() method called.");
        if (master == null)
            throw new AcsStateActionException("Cannot access Control's internal master component: " + Name.Master);
        try {
            master.shutdownPass2();
            if (master.getMasterState() != SystemState.INACCESSIBLE &&
                    master.getMasterSubstate() != SystemSubstate.STOPPED) {
                throw new AcsStateActionException("Error shutting down Control's internal master component: " + 
                        Name.Master + "  See ACS log.");
            }
            releaseMaster();
            master = null;
        } catch (Exception e) {
            throw new AcsStateActionException("Error shutting down Control's internal master component: " + 
                    Name.Master + "  " + e.toString());            
        }
    }

}
