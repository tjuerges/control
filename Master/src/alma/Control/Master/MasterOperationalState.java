/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.Control.Master;

import java.util.ArrayList;
import java.util.List;

import alma.ACSErrTypeCommon.wrappers.AcsJBadParameterEx;
import alma.ArrayExceptions.wrappers.AcsJInvalidRequestEx;
import alma.Control.AntennaStateEvent;
import alma.Control.CentralLO;
import alma.Control.CorrelatorType;
import alma.Control.ResourceId;
import alma.Control.Common.Resource;
import alma.Control.Common.ResourceManager;
import alma.Control.Common.State;
import alma.ResourceExceptions.wrappers.AcsJBadResourceEx;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.ResourceExceptions.wrappers.AcsJResourceExceptionsEx;
import alma.ResourceExceptions.wrappers.AcsJUnknownResourceEx;

public class MasterOperationalState extends MasterStateBase {

    private Configurator configurator;
    private AntennaManager antennaMng;
    
	public MasterOperationalState(MasterInternal master) {
		super(master, State.Operational_NoError);
	}

    @Override
    public void initialize() {
        configurator = master.getConfigurator();
        antennaMng = master.getAntennaManager();
        assert configurator != null;
        assert antennaMng != null;
    }
    
    @Override
    public void shutdownPass1() throws AcsJInvalidRequestEx {
        master.setState("SHUTTINGDOWN_STATE");
        master.shutdownPass1();
    }
    
    @Override
    public String createAutomaticArray(String[] antennaNames,
                                       String[] photonicReferences,
                                       CorrelatorType corrType) 
        throws AcsJResourceExceptionsEx,
               alma.ControlExceptions.wrappers.AcsJInvalidRequestEx {
        return antennaMng.createAutomaticArray(antennaNames, photonicReferences, corrType)
                         .getName();
    }

    @Override
    public String createManualArray(String[] antennaNames,
                                    String[] photonicReferences,
                                    CorrelatorType corrType) 
        throws AcsJResourceExceptionsEx,
               alma.ControlExceptions.wrappers.AcsJInvalidRequestEx {    
        return antennaMng.createManualArray(antennaNames, photonicReferences, corrType)
                         .getName();
    }

    @Override
    public void destroyArray(String arrayName) throws AcsJBadParameterEx {
        antennaMng.destroyArray(arrayName);
    }

	@Override
	public String getArrayComponentName(String arrayName)
			throws AcsJInvalidRequestEx {
		ResourceId[] arrays = getAutomaticArrayResources();
		for (ResourceId arr : arrays) {
			if (arr.ResourceName.equals(arrayName))
				return arr.ComponentName;
		}
		AcsJInvalidRequestEx ex = new AcsJInvalidRequestEx();
		ex.setProperty("Details", "Invalid array name");
		throw ex;
	}
    
    @Override
    public void reinitializeAntenna(String antennaName) 
        throws AcsJBadParameterEx, AcsJResourceErrorEx {
        Antenna antenna = antennaMng.getAntenna(antennaName);
        antenna.reinitialize();
    }
    
//    public void killAntenna(String antennaName) throws AcsJBadParameterEx {
//        Antenna antenna = antennaMng.getAntenna(antennaName);
//        antenna.kill();        
//    }

    public void resucitateAntenna(String antennaName) throws AcsJBadParameterEx {
        Antenna antenna = antennaMng.getAntenna(antennaName);
        antenna.resucitate();        
    }
    
    @Override
    public void setAntennaOffline(String antennaName) throws AcsJBadParameterEx {
        Antenna antenna = antennaMng.getAntenna(antennaName);
        antenna.offline();
    }
    
    @Override
    public void setAntennaOnline(String antennaName) throws AcsJBadParameterEx {
        Antenna antenna = antennaMng.getAntenna(antennaName);
        antenna.online();        
    }

    @Override
    public ResourceId[] getAutomaticArrayResources() {
        AutomaticArray[] arrays = antennaMng.getAutomaticArrays();
        ResourceId[] resources = new ResourceId[arrays.length];
        for (int i=0; i<resources.length; i++) {
            resources[i] = new ResourceId();
            resources[i].ResourceName = arrays[i].getName();
            resources[i].ComponentName = arrays[i].getComponentName();
        }
        return resources;
    }

    @Override
    public ResourceId[] getManualArrayResources() {
        ManualArray[] arrays = antennaMng.getManualArrays();
        ResourceId[] resources = new ResourceId[arrays.length];
        for (int i=0; i<resources.length; i++) {
            resources[i] = new ResourceId();
            resources[i].ResourceName = arrays[i].getName();
            resources[i].ComponentName = arrays[i].getComponentName();
        }
        return resources;
    }

    @Override
    public ResourceId[] getAvailableAntennaResources() {
        Antenna[] antennas = antennaMng.getAvailableAntennas();
        ResourceId[] resources = new ResourceId[antennas.length];
        for (int i=0; i<resources.length; i++) {
            resources[i] = new ResourceId();
            resources[i].ResourceName = antennas[i].getAntennaName();
            resources[i].ComponentName = antennas[i].getAntennaComponentName();
        }
        return resources;
    }

    @Override
    public ResourceId[] getOfflineAntennaResources() {
        Antenna[] antennas = antennaMng.getOfflineAntennas();
        ResourceId[] resources = new ResourceId[antennas.length];
        for (int i=0; i<resources.length; i++) {
            resources[i] = new ResourceId();
            resources[i].ResourceName = antennas[i].getAntennaName();
            resources[i].ComponentName = antennas[i].getAntennaComponentName();
        }
        return resources;
    }
    
    @Override
    public ResourceId[] getAvailablePhotonicReferenceResources()
        throws AcsJResourceExceptionsEx {
        Resource<CentralLO> clo = ResourceManager
                                  .getInstance(container)
                                  .getResource(CentralLOResource.RESOURCE_NAME);
        CentralLOResource clo2 = (CentralLOResource) clo;
        return clo2.getAvailablePhotonicReferences();
    }
    
    @Override
    public AntennaStateEvent[] getAvailableAntennaStates() {
        Antenna[] antennas = antennaMng.getAvailableAntennas();
        AntennaStateEvent[] states = new AntennaStateEvent[antennas.length];
        for (int i=0; i<antennas.length; i++)
            states[i] = antennas[i].getAntennaComponent().getAntennaState();
        return states;
    }
}
