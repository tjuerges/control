/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.Control.Master;

import alma.Control.AntennaCharacteristics;
import alma.Control.CorrelatorType;
import alma.Control.ResourceId;
import alma.Control.Common.ResourceManager;
import alma.Control.CommonCallbacks.AntennaCallbackImpl;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.acs.container.ContainerServices;
import alma.ControlExceptions.wrappers.AcsJTimeoutEx;
import alma.ControlExceptions.wrappers.AcsJInvalidRequestEx;
import alma.ControlCommonExceptions.wrappers.AcsJAsynchronousFailureEx;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;

public class ManualArray {
    
    /** ACS ContainerServices */
    private ContainerServices container;
    
    /** Array name. */
    private String name;
    
    /** Array parent name. Arrays are designed to be used hierarchycally. */
    private String parentName;
    
    /** Antenna list */
    private Antenna[] antennas;

    /** Object to access configuration information */
    private Configurator configurator;
    
    /** 
     * Array resource. This object takes care of the lifecycle of the array's respective
     * ACS component.
     */
    private ManualArrayResource arrayResource;
    
    private ResourceManager resMng;
    
    private String correlatorBulkDataDistributor;
    
    private String totalPowerBulkDataDistributor;
    
    private ResourceId[] photonicReferences;
    
    private CorrelatorType correlatorType;
    
    public ManualArray(ContainerServices container,
                 String name,
                 String parentName, 
                 Antenna[] antennas,
                 String corrBulkDataDistr,
                 String tpBulkDataDistr,
                 ResourceId[] photRefs,
                 Configurator configurator,
                 CorrelatorType correlatorType) throws AcsJResourceErrorEx {
        
        this.container = container;
        this.name = name;
        this.parentName = parentName;
        this.antennas = antennas;
        this.configurator = configurator;
        this.correlatorBulkDataDistributor = corrBulkDataDistr;
        this.totalPowerBulkDataDistributor = tpBulkDataDistr;
        this.photonicReferences = photRefs;
        this.correlatorType = correlatorType;
        
        this.resMng = ResourceManager.getInstance(container);
        this.arrayResource = new ManualArrayResource(container, this);
        
        resMng.addResource(arrayResource);
        resMng.acquireResource(arrayResource.getName());
        
        AntennaCallbackImpl cb;
        try {
              cb = new AntennaCallbackImpl(container);
        } catch (alma.JavaContainerError.wrappers.AcsJContainerServicesEx ex) {
              throw new AcsJResourceErrorEx(ex);          
        }

        for (int i=0; i<antennas.length; i++) {
            try {
                cb.addExpectedResponse(antennas[i].getAntennaName());
                antennas[i].allocate(name, cb);
            } catch (AcsJInvalidRequestEx ex) {
                throw new AcsJResourceErrorEx(ex);
            }
        }
        try {
            cb.waitForCompletion(180); //3 minutes
        } catch (AcsJAsynchronousFailureEx ex) {
            throw new AcsJResourceErrorEx(ex);
        } catch (AcsJTimeoutEx ex){
            throw new AcsJResourceErrorEx(ex);
        }

    }

    public void release() {
        for (int i=0; i<antennas.length; i++)
            antennas[i].deallocate();        
        resMng.releaseResource(arrayResource.getName());
    }
    
    protected String getName() {
        return name;
    }
    
    protected String getParentName() {
        return parentName;
    }

    protected Antenna[] getAntennas() {
        return antennas;
    }
    
    /**
     * Returns an array containing the array antenna names.
     * 
     * The antenna names are arbitrary strings, and are related with the antenna
     * component names through some deployment convention.
     * 
     * @return
     */
    protected String[] getAntennaNames() {
        if (antennas == null)
            return new String[0];
        String[] names = new String[antennas.length];
        for (int i=0; i<antennas.length; i++) {
            names[i] = antennas[i].getAntennaName();
        }
        return names;
    }
    
    protected AntennaCharacteristics[] getAntennaCharacteristics() {
        if (antennas == null)
            return new AntennaCharacteristics[0];
        AntennaCharacteristics[] ac = new AntennaCharacteristics[antennas.length];
        for (int i=0; i<antennas.length; i++) {
            ac[i] = antennas[i].getAntennaCharacteristics();
        }
        return ac;
    }
    
    protected Configurator getConfigurator() {
        return configurator;
    }
    
    protected String getComponentName() {
        return arrayResource.getName();
    }
    
    protected String getCorrelatorBulkDataDistributor() {
        return correlatorBulkDataDistributor;
    }
    
    protected String getTotalPowerBulkDataDistributor() {
        return totalPowerBulkDataDistributor;
    }
    
    protected ResourceId[] getPhotonicReferences() {
        return photonicReferences;
    }
    
    protected CorrelatorType getCorrelatorType() {
    	return correlatorType;
    }
}
