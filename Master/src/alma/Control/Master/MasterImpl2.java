/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.Control.Master;

import java.util.logging.Logger;

import alma.ACS.ComponentStates;
import alma.ACSErrTypeCommon.wrappers.AcsJBadParameterEx;
import alma.ArrayExceptions.wrappers.AcsJInitializationErrorEx;
import alma.ArrayExceptions.wrappers.AcsJInvalidRequestEx;
import alma.Control.AntennaMode;
import alma.Control.AntennaStateEvent;
import alma.Control.ArrayIdentifier;
import alma.Control.CorrelatorMode;
import alma.Control.CorrelatorType;
import alma.Control.ErrorEnum;
import alma.Control.IDLError;
import alma.Control.IDLResult;
import alma.Control.InaccessibleException;
import alma.Control.InvalidReason;
import alma.Control.InvalidRequest;
import alma.Control.Master2Operations;
import alma.Control.ResourceEnum;
import alma.Control.ResourceId;
import alma.Control.SystemState;
import alma.Control.SystemSubstate;
import alma.Control.Common.ResourceManager;
import alma.Control.Common.Util;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.ResourceExceptions.wrappers.AcsJResourceExceptionsEx;
import alma.acs.component.ComponentLifecycle;
import alma.acs.component.ComponentLifecycleException;
import alma.acs.container.ContainerServices;

/**
 * Master component implementation.
 * 
 */
public class MasterImpl2 implements Master2Operations, ComponentLifecycle, MasterInternal {

	private MasterState SHUTDOWN_STATE;
	private MasterState STARTING_UP_STATE;
	private MasterState OPERATIONAL_STATE;
	private MasterState SHUTTING_DOWN_STATE;
	
	private ContainerServices container;
	private Logger logger;
	private MasterState state;
	private String parentName;
    private Configurator configurator;
    private AntennaManager antennaMng;
	
	public MasterImpl2() {
	}
	
    ////////////////////////////////////////////////////////////
    // ComponentLifecycle Operations
    //
    ////////////////////////////////////////////////////////////    

	public void initialize(ContainerServices containerServices) 
		throws ComponentLifecycleException {
		container = containerServices;
		logger = container.getLogger();
        
        SHUTDOWN_STATE = new MasterShutdownState(this);
        STARTING_UP_STATE = new MasterStartingUpState(this);
        OPERATIONAL_STATE = new MasterOperationalState(this);
        SHUTTING_DOWN_STATE = new MasterShuttingDownState(this);
        
		state = SHUTDOWN_STATE;
	}
	
	public void execute() throws ComponentLifecycleException {
		logger.finest("execute called");
	}

	public void aboutToAbort() {
		logger.finest("aboutToAbort called");
	}

	public void cleanUp() {
		logger.finest("cleanUp called");
        ResourceManager.getInstance(container).releaseResources();
        ResourceManager.getInstance(container).freeAllResources();
        ResourceManager.freeInstance(container);
	}

    ////////////////////////////////////////////////////////////
    // ACSComponent Operations
    //
    ////////////////////////////////////////////////////////////    
    
    public ComponentStates componentState() {
    	return container.getComponentStateManager().getCurrentState();
    }

    public String name() {
    	return container.getName();
    }

    ////////////////////////////////////////////////////////////
    // ControlMaster Operations
    //
    ////////////////////////////////////////////////////////////    
    
    public ArrayIdentifier createAutomaticArray(String[] antennaNames,
            String[] photonicReferences, CorrelatorType corrType)
            throws InaccessibleException, InvalidRequest {
        String arrayName = null;
        try {
            arrayName = state.createAutomaticArray(antennaNames, photonicReferences, corrType);
        } catch (AcsJResourceExceptionsEx ex) {
            logger.severe("Error creating automatic array component:\n" +
                    Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
            InvalidRequest invReqEx = new InvalidRequest();
            invReqEx.name = "Resource Exception in AutomaticArray Creation";
            invReqEx.reason = InvalidReason.CannotAccess;
            invReqEx.msg = "";
            throw invReqEx;
            // An ACS Error exception should be thrown.
            // Change the ICD first.
        } catch (alma.ControlExceptions.wrappers.AcsJInvalidRequestEx ex) {
            logger.severe("Invalid request:\n" + 
                    Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
            InvalidRequest invReqEx = new InvalidRequest();
            invReqEx.name = "Invalid Request in AutomaticArray Creation";
            invReqEx.reason = InvalidReason.BadAntennaId;
            invReqEx.msg = "";
            throw invReqEx;
            // An ACS Error exception should be thrown.
            // Change the ICD first.
        }
        
        ArrayIdentifier arrid = new ArrayIdentifier();
        arrid.arrayName = arrayName;
        arrid.arrayComponentName = "CONTROL/"+arrayName; 
        return arrid;
    }

    public ArrayIdentifier createManualArray(String[] antennaNames,
            String[] photonicReferences, CorrelatorType corrType)
            throws InaccessibleException, InvalidRequest {
        String arrayName = null;
        try {
            arrayName = state.createManualArray(antennaNames, photonicReferences, corrType);
        } catch (AcsJResourceExceptionsEx ex) {
            logger.severe("Error creating manual array component:\n" +
                    Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
            // An ACS Error exception should be thrown.
            // Change the ICD first.
        } catch (alma.ControlExceptions.wrappers.AcsJInvalidRequestEx ex) {
            logger.severe("Invalid request:\n" + 
                    Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
            // An ACS Error exception should be thrown.
            // Change the ICD first.
        }
        ArrayIdentifier arrid = new ArrayIdentifier();
        arrid.arrayName = arrayName;
        arrid.arrayComponentName = "CONTROL/"+arrayName; 
        return arrid;
    }

    public void destroyArray(String arrayName) throws InaccessibleException,
            InvalidRequest {
        try {
            state.destroyArray(arrayName.replaceFirst("CONTROL/", ""));
        } catch (AcsJBadParameterEx ex) {
            logger.severe("Error destroying array:\n" +
                    Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
            // An ACS Error exception should be thrown.
            // Change the ICD first.
        }
    }

    public void reinitializeAntenna(String antennaName) 
        throws InaccessibleException, InvalidRequest {
        try {
            state.reinitializeAntenna(antennaName);
        } catch (AcsJBadParameterEx ex) {
            logger.severe("Error reinitializing Antenna. Bad parameter:\n" +
                    Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
            // An ACS Error exception should be thrown.
            // Change the ICD first.
        } catch (AcsJResourceErrorEx ex) {
            logger.severe("Error reinitializing Antenna:\n" +
                    Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
            // An ACS Error exception should be thrown.
            // Change the ICD first.
        }
    }

//    public void killAntenna(String antennaName) {
//        try {
//            state.killAntenna(antennaName);
//        } catch (AcsJBadParameterEx ex) {
//            logger.severe("Error reinitializing Antenna. Bad parameter:\n" +
//                    Util.getNiceErrorTraceString(ex.getErrorTrace()));
//            ex.log(logger);
//            // An ACS Error exception should be thrown.
//            // Change the ICD first.
//        }        
//    }
    
    public void resucitateAntenna(String antennaName) {
        try {
            state.resucitateAntenna(antennaName);
        } catch (AcsJBadParameterEx ex) {
            logger.severe("Error reinitializing Antenna. Bad parameter:\n" +
                    Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
            // An ACS Error exception should be thrown.
            // Change the ICD first.
        }        
    }
    
    /**
     * Set an Antenna online/offline.
     * 
     * @param antennaName Antenna name.
     * @param mode Online or offline, an IDL enumeration.
     * @param now If true, put the antenna offline immediately; if false,
     * wait for the antenna to become idle.
     * 
     * TODO:
     * This operation gets complicated in the case where the Antenna
     * has been allocated to an Array. In this case the Antenna should stop
     * sending blanking events. The Antenna component should be modified so
     * it gets to an offline state where it sends an exception when any of
     * its methods are called. I think this should be the case also for all of
     * its subdevices. It is not clear whether the software component(s) that
     * represent the antenna should be destroyed or not.
     * The antenna should be taken away from the array as well.
     */
    public void setAntennaMode(String antennaName, AntennaMode mode, boolean now)
        throws InaccessibleException, InvalidRequest {
        if (mode == AntennaMode.ANTENNA_OFFLINE) {
            try {
                state.setAntennaOffline(antennaName);
            } catch (AcsJBadParameterEx ex) {
                logger.severe("Error setting Antenna offline. Bad parameter:\n" +
                        Util.getNiceErrorTraceString(ex.getErrorTrace()));
                ex.log(logger);
                // An ACS Error exception should be thrown.
                // Change the ICD first.
            }
        } else {
            try {
                state.setAntennaOnline(antennaName);
            } catch (AcsJBadParameterEx ex) {
                logger.severe("Error setting Antenna online. Bad parameter:\n" +
                        Util.getNiceErrorTraceString(ex.getErrorTrace()));
                ex.log(logger);
                // An ACS Error exception should be thrown.
                // Change the ICD first.
            }
        }
    }

    
    public void setAlmaCorrelatorMode(CorrelatorMode mode)
            throws InaccessibleException, InvalidRequest {
        throw new InvalidRequest(name(), 
                                 InvalidReason.CannotAccess,
                                 "Not implemented yet");
    }

    public void setAlmaCorrelatorQuadMode(int quadrant, CorrelatorMode mode)
            throws InaccessibleException, InvalidRequest {
        throw new InvalidRequest(name(), 
                                 InvalidReason.CannotAccess,
                                 "Not implemented yet");
    }

    ////////////////////////////////////////////////////////////
    // ControlMasterMonitor Operations
    //
    ////////////////////////////////////////////////////////////

	@Override
	public String getArrayComponentName(String arrayName)
			throws InaccessibleException, InvalidRequest {
		try {
			return state.getArrayComponentName(arrayName);
		} catch (AcsJInvalidRequestEx ex) {
			throw new InvalidRequest(name(), 
                    				 InvalidReason.BadArrayId,
                    				 "Invalid array name");
		}		
	}
    
    public String[] getAutomaticArrays() throws InaccessibleException {
        return getNamesFromResourceIds(state.getAutomaticArrayResources());
    }

    public ResourceId[] getAutomaticArrayComponents()
            throws InaccessibleException {
        ResourceId[] ids = state.getAutomaticArrayResources();
        if (ids == null)
            ids = new ResourceId[0];
        return ids;
    }

    public String[] getManualArrays() throws InaccessibleException {
        return getNamesFromResourceIds(state.getManualArrayResources());
    }

    public ResourceId[] getManualArrayComponents() throws InaccessibleException {
        ResourceId[] ids = state.getManualArrayResources();
        if (ids == null)
            ids = new ResourceId[0];
        return ids;
    }

    @Override
    public String[] getAvailablePhotonicReferences()
            throws InaccessibleException {
        return getNamesFromResourceIds(getAvailablePhotonicReferenceComponents());
    }

    @Override
    public ResourceId[] getAvailablePhotonicReferenceComponents()
            throws InaccessibleException {
        ResourceId[] ids;
        try {
            ids = state.getAvailablePhotonicReferenceResources();
        } catch (AcsJResourceExceptionsEx ex) {
            // If there's any problem with the CentralLO.
            return new ResourceId[0];
        }            
        if (ids == null) // If not in the operational state
            // TODO for regularity, these state functions should throw an
            // exception instead of returning null
            return new ResourceId[0];
        return ids;
    }
    
    public String[] getAvailableAntennas() throws InaccessibleException {
        return getNamesFromResourceIds(state.getAvailableAntennaResources());
    }

    public ResourceId[] getAvailableAntennaComponents()
            throws InaccessibleException {
        ResourceId[] ids = state.getAvailableAntennaResources();
        if (ids == null)
            ids = new ResourceId[0];
        return ids;        
    }

    public AntennaStateEvent[] getAvailableAntennaStates()
            throws InaccessibleException {
        return state.getAvailableAntennaStates();
    }

    public String[] getOfflineAntennas() throws InaccessibleException {
        return getNamesFromResourceIds(state.getOfflineAntennaResources());
    }

    public ResourceId[] getWeatherStationComponents()
            throws InaccessibleException {
        return new ResourceId[0]; // not implemented yet
    }

    public String[] getWeatherStations() throws InaccessibleException {
        return new String[0]; // not implemented yet
    }

    public String[] getBadResources() {
        return ResourceManager.getInstance(container).getErroneousResources();
    }

    public SystemState getMasterState() {
        return SystemState.from_int(state.getStateValue());
    }

    public SystemSubstate getMasterSubstate() {
        return SystemSubstate.from_int(state.getSubstateValue());
    }
    
    ////////////////////////////////////////////////////////////
    // Controller Operations
    //
    ////////////////////////////////////////////////////////////

    public void setParentName(String name) {
        this.parentName = name;
    }

    public void shutdownPass1() {
        try {
            state.shutdownPass1();
        } catch (AcsJInvalidRequestEx ex) {
            logger.severe("Invalid request error:\n" + 
                    Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);            
            // An ACSError exception should be thrown:
            // throw ex.toInvalidRequestEx();
            // However, the IDL for this component should be changed
            // first.
        } catch (Exception ex) {
            logger.fine("Unexpected exception in shutting down pass1:");
            ex.printStackTrace();
//            ResourceManager.freeInstance(container);
//            this.resMng = ResourceManager.getInstance(container);
        }
    }

    public void shutdownPass2() {
        try {
            state.shutdownPass2();
        } catch (AcsJInvalidRequestEx ex) {
            logger.severe("Invalid request error:\n" + 
                    Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);            
            // An ACSError exception should be thrown:
            // throw ex.toInvalidRequestEx();
            // However, the IDL for this component should be changed
            // first.
        } catch (Exception ex) {
            logger.fine("Unexpected exception in shutting down pass1:");
            ex.printStackTrace();
//            ResourceManager.freeInstance(container);
//            this.resMng = ResourceManager.getInstance(container);
        }
    }

    public void startupPass1() {
        try {
            state.startupPass1();
        } catch (AcsJInitializationErrorEx ex) {
            logger.severe("Initialization error:\n" + 
                    Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
            // An ACSError exception should be thrown:
            // throw ex.toInitializationErrorEx();
            // However, the IDL for this component should be changed
            // first.
        } catch (AcsJInvalidRequestEx ex) {
            logger.severe("Invalid request error:\n" + 
                    Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
            // An ACSError exception should be thrown:
            // throw ex.toInvalidRequestEx();
            // However, the IDL for this component should be changed
            // first.
        }
    }

    public void startupPass2() {
        try {
            state.startupPass2();
        } catch (AcsJInitializationErrorEx ex) {
            logger.severe("Initialization error:\n" + 
                    Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
            // An ACSError exception should be thrown:
            // throw ex.toInitializationErrorEx();
            // However, the IDL for this component should be changed
            // first.
        } catch (AcsJInvalidRequestEx ex) {
            logger.severe("Invalid request error:\n" + 
                    Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
            // An ACSError exception should be thrown:
            // throw ex.toInvalidRequestEx();
            // However, the IDL for this component should be changed
            // first.
        }
    }

    public void waitShutdownPass1() {
        try {
            state.waitShutdownPass1();
        } catch (AcsJInvalidRequestEx ex) {
            logger.severe("Invalid request error:\n" + 
                    Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
            // An ACSError exception should be thrown:
            // throw ex.toInvalidRequestEx();
            // However, the IDL for this component should be changed
            // first.
        }
    }
    
    public IDLError[] getBadResourcesIDL() {
        String[] badResources = ResourceManager.getInstance(container).getErroneousResources();
        IDLError[] idlErrors = new IDLError[badResources.length];
        for(int i=0; i<badResources.length; i++) {
            // (RHV) I don't quite like to keep enumerations of all possible
            // resources and all possible errors. I think it is preferrable to
            // just retrieve a polymorphic list of exceptions.
            // In order to maintain the IDL interface, I'm doing sort of a
            // conversion here, but I hope to modify this later.
            idlErrors[i] = new IDLError(badResources[i],
                                        ResourceEnum.MASTER_COMPONENT,
                                        ErrorEnum.INTERNAL_ERROR,
                                        0L,
                                        "");
        }
        return idlErrors;
    }

    public String getComponentName() {
        return name();
    }

    public int getResourceTypeValue() {
        return 0;
    }

    public int getStateValue() {
        return state.getStateValue();
    }

    public int getSubstateValue() {
    	return state.getSubstateValue();
    }

    public IDLResult reportRemoteError(String resourceName,
            ResourceEnum resourceType, ErrorEnum errorType, long time,
            String message) {
    	// Not quite understand this method.
        return null;
    }

    public void reportRemoteStateChange(String resourceName,
            ResourceEnum resourceType, SystemState newState,
            SystemSubstate newSubstate) {
    	// Not quite understand this method neither.
    }

    ////////////////////////////////////////////////////////////////////
    // MasterInternal operations
    ////////////////////////////////////////////////////////////////////
    
    public void setState(String stateName) {
        
        // Shutdown the current state
        state.shutdown();
        
        // This possibly could be changed to use Java reflection.
        if (stateName.equals("SHUTDOWN_STATE"))
            state = SHUTDOWN_STATE;
        else if (stateName.equals("STARTINGUP_STATE"))
            state = STARTING_UP_STATE;
        else if (stateName.equals("OPERATIONAL_STATE"))
            state = OPERATIONAL_STATE;
        else if (stateName.equals("SHUTTINGDOWN_STATE"))
            state = SHUTTING_DOWN_STATE;
        else
            assert(false);
        
        // Initialize the next state
        state.initialize();
    }
    
    public Logger getLogger() {
        return logger;
    }

    public ContainerServices getContainerServices() {
        return container;
    }
    
    public void setConfigurator(Configurator conf) {
        configurator = conf;
    }
    
    public Configurator getConfigurator() {
        return configurator;
    }
    
    public void setAntennaManager(AntennaManager am) {
        antennaMng = am;
    }
    
    public AntennaManager getAntennaManager() {
        return antennaMng;
    }
    
    ////////////////////////////////////////////////////////////////////
    // Private methods
    ////////////////////////////////////////////////////////////////////

    private String[] getNamesFromResourceIds(ResourceId[] resources) {
        if (resources == null)
            return new String[0];
        String[] names = new String[resources.length];
        for (int i=0; i<resources.length; i++)
            names[i] = resources[i].ResourceName;
        return names;        
    }
}

// __oOo__
