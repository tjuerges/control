/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.Control.Master;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;

import alma.Control.DeviceConfig;
import alma.Control.SiteData;
import alma.TMCDB.Access;
import alma.TMCDB.TMCDBComponent;
import alma.TMCDB_IDL.StartupAOSTimingIDL;
import alma.TMCDB_IDL.StartupAntennaIDL;
import alma.TMCDB_IDL.StartupCLOIDL;
import alma.TMCDB_IDL.StartupPhotonicReferenceIDL;
import alma.TMCDB_IDL.StartupWeatherStationControllerIDL;
import alma.TmcdbErrType.TmcdbDuplicateKeyEx;
import alma.TmcdbErrType.TmcdbErrTypeEx;
import alma.TmcdbErrType.TmcdbErrorEx;
import alma.TmcdbErrType.TmcdbNoSuchRowEx;
import alma.TmcdbErrType.TmcdbRowAlreadyExistsEx;
import alma.TmcdbErrType.TmcdbSqlEx;
import alma.asdmIDLTypes.IDLAngle;
import alma.asdmIDLTypes.IDLLength;

public class TMCDBConfigurator implements Configurator {

    private Logger logger;
    private Access tmcdb;
    private Map<String, DeviceConfig> antennaDeviceConfigurations;
    private DeviceConfig aosTimingConfiguration;
    private DeviceConfig weatherStationCtrlConfiguration;
    private DeviceConfig cloConfiguration;
    private DeviceConfig weatherConfiguration;
    private int timeout;
    private String configurationName;
    
    public TMCDBConfigurator(Access tmcdb, Logger logger) {
        
        if (tmcdb == null)
            throw new NullPointerException("TMCDB Component is null");
        if (logger == null)
            throw new NullPointerException("Logger is null");
        
        this.tmcdb = tmcdb;
        this.antennaDeviceConfigurations = new HashMap<String, DeviceConfig>();
        this.aosTimingConfiguration = null;
        this.weatherConfiguration = null;
        this.timeout = 300000; // default timeout
        this.logger = logger;
        
        getConfiguration();
    }

    public String getConfigurationName() {
        return configurationName;
    }

    public String[] getAntennas() {
        return antennaDeviceConfigurations.keySet().toArray(new String[0]);
    }
    
    public DeviceConfig getAntennaDeviceConfiguration(String antennaName) {
        return antennaDeviceConfigurations.get(antennaName);
    }
    
    public alma.TMCDBComponentImpl.Antenna getAntennaConfiguration(String antennaName)
        throws TmcdbErrTypeEx, TmcdbSqlEx, TmcdbDuplicateKeyEx, TmcdbNoSuchRowEx {
        
        alma.TMCDBComponentImpl.Antenna ac = new alma.TMCDBComponentImpl.Antenna();
        ac.fromIDL(tmcdb.getAntennaInfo(antennaName));
        return ac;
    }
    
    public alma.TMCDBComponentImpl.Pad getPadConfiguration(String antennaName)
        throws TmcdbErrTypeEx, TmcdbSqlEx, TmcdbDuplicateKeyEx, TmcdbNoSuchRowEx, TmcdbRowAlreadyExistsEx {
        
        alma.TMCDBComponentImpl.Pad pc = new alma.TMCDBComponentImpl.Pad();
        pc.fromIDL(tmcdb.getCurrentAntennaPadInfo(antennaName));
        return pc;
    }

    /**
     * Get site data. This is not in the TMCDB right now, so a
     * dummy structure is returned.
     * @return Site data.
     */
    public SiteData getSiteData() {
        
        SiteData siteData = new SiteData ();
        try {
            siteData.telescopeName = tmcdb.getTelescopeName();
        } catch (TmcdbErrorEx e) {
            logger.severe("error getting the TelescopeName from TMCDB: " + e);
            e.printStackTrace();
            siteData.telescopeName = "OSF";
        } catch (TmcdbNoSuchRowEx e) {
            logger.severe("error getting the TelescopeName from TMCDB: " + e);
            e.printStackTrace();
            siteData.telescopeName = "OSF";
        }
        siteData.arrayConfigurationName = "A";
        siteData.minBaseRange = new IDLLength(0);
        siteData.maxBaseRange = new IDLLength(0);;
        siteData.baseRmsMinor = new IDLLength(0);;
        siteData.baseRmsMajor = new IDLLength(0);;
        siteData.basePa = new IDLAngle(0.0);
        siteData.siteLongitude = new IDLAngle(0.0);
        siteData.siteLatitude = new IDLAngle(0.0);
        siteData.siteAltitude = new IDLLength(0);
        siteData.releaseDate = 0L;

        return siteData;
    }
    
    @Override
    public String toString() {
        String repr = super.toString();
        repr += "{Antennas=[";
        Iterator<String> iter = antennaDeviceConfigurations.keySet().iterator();
        while(iter.hasNext())
            repr += iter.next() + ", ";
        repr = repr.substring(0, repr.length()-2) + "]}";        
        return repr;
    }
    
    private void getConfiguration() {
        
        try {
            configurationName = tmcdb.getConfigurationName();
        } catch (TmcdbErrorEx e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        
        StartupAntennaIDL[] startUpAntennas = null;
        try {
            startUpAntennas = tmcdb.getStartupAntennasInfo();
        } catch (TmcdbErrorEx ex) {
            // TODO Auto-generated catch block
            ex.printStackTrace();
        }
        
        for(int i = 0; i < startUpAntennas.length; i++) {
            String antennaName = startUpAntennas[i].antennaName;
            DeviceConfig devConf = new DeviceConfig();
            
            devConf.Name = "CONTROL/" + antennaName;
            devConf.subdevice = new DeviceConfig[startUpAntennas[i].antennaAssembly.length];
            for(int sd = 0; sd < devConf.subdevice.length; sd++) {
                devConf.subdevice[sd] = new DeviceConfig();
                devConf.subdevice[sd].Name = startUpAntennas[i].antennaAssembly[sd].assemblyRoleName;
                
                if (devConf.subdevice[sd].Name.equals("FrontEnd")) {
                    DeviceConfig[] frontEndSubDevices = 
                        new DeviceConfig[startUpAntennas[i].frontEndAssembly.length];
                    for (int fesd = 0; fesd < frontEndSubDevices.length; fesd++) {
                        frontEndSubDevices[fesd] = new DeviceConfig();
                        frontEndSubDevices[fesd].Name =
                            startUpAntennas[i].frontEndAssembly[fesd].assemblyRoleName;
                        frontEndSubDevices[fesd].subdevice = new DeviceConfig[0];                        
                    }
                    devConf.subdevice[sd].subdevice = frontEndSubDevices;
                } else {
                    devConf.subdevice[sd].subdevice = new DeviceConfig[0];
                }
            }            
            antennaDeviceConfigurations.put(antennaName, devConf);
        }

        StartupAOSTimingIDL startUpAOSTiming = null;
        try {
            startUpAOSTiming = tmcdb.getStartupAOSTimingInfo();
        } catch (TmcdbErrorEx e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        aosTimingConfiguration = new DeviceConfig();
        aosTimingConfiguration.Name = "CONTROL/AOSTiming";
        DeviceConfig[] sds = new DeviceConfig[startUpAOSTiming.assemblies.length];
        for (int i=0; i < sds.length; i++) {
            sds[i] = new DeviceConfig();
            sds[i].Name = startUpAOSTiming.assemblies[i].assemblyRoleName;
            sds[i].subdevice = new DeviceConfig[0];
        }
        aosTimingConfiguration.subdevice = sds;


        StartupWeatherStationControllerIDL startUpWeatherStCtrl = null;
        try {
            startUpWeatherStCtrl = tmcdb.getStartupWeatherStationControllerInfo();
        } catch (TmcdbErrorEx e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        weatherStationCtrlConfiguration = new DeviceConfig();
        weatherStationCtrlConfiguration.Name = "CONTROL/WeatherStationController";
        DeviceConfig[] wsds = new DeviceConfig[startUpWeatherStCtrl.assemblies.length];
        for (int i=0; i < wsds.length; i++) {
            wsds[i] = new DeviceConfig();
            wsds[i].Name = startUpWeatherStCtrl.assemblies[i].assemblyRoleName;
            wsds[i].subdevice = new DeviceConfig[0];
        }
        weatherStationCtrlConfiguration.subdevice = wsds;

        /* --- Get the configuration information for the CLO */
        StartupCLOIDL startUpCLO = null;
        try {
            startUpCLO = tmcdb.getStartupCLOInfo();
        } catch (TmcdbErrorEx e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        cloConfiguration = new DeviceConfig();
        cloConfiguration.Name = "CONTROL/CentralLO";
        int numPhotoRef = startUpCLO.photonicReferences.length;
        DeviceConfig[] cloSd = 
            new DeviceConfig[startUpCLO.assemblies.length + numPhotoRef];
        for (int i=0; i < numPhotoRef; i++) {
            StartupPhotonicReferenceIDL photoRef = 
                startUpCLO.photonicReferences[i];
            cloSd[i] = new DeviceConfig();
            cloSd[i].Name =  photoRef.name;
            cloSd[i].subdevice = new DeviceConfig[photoRef.assemblies.length];
            for (int j = 0; j <photoRef.assemblies.length; j++) {
                cloSd[i].subdevice[j] = new DeviceConfig();
                cloSd[i].subdevice[j].Name = 
                    photoRef.assemblies[j].assemblyRoleName; 
                cloSd[i].subdevice[j].subdevice = new DeviceConfig[0];
            }
        }

        for (int i=0; i < startUpCLO.assemblies.length; i++) {
            cloSd[i+numPhotoRef] = new DeviceConfig();
            cloSd[i+numPhotoRef].Name = 
                startUpCLO.assemblies[i].assemblyRoleName;
            cloSd[i+numPhotoRef].subdevice = new DeviceConfig[0];
        }
        cloConfiguration.subdevice = cloSd;

	// GET WeatherStationController subdevice information

        StartupWeatherStationControllerIDL startUpWSC = null;
        try {
            startUpWSC = tmcdb.getStartupWeatherStationControllerInfo();
        } catch (TmcdbErrorEx e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        weatherConfiguration = new DeviceConfig();
        weatherConfiguration.Name = "CONTROL/WeatherStationController";
        DeviceConfig[] wscSds = new DeviceConfig[startUpWSC.assemblies.length];
        for (int i=0; i < wscSds.length; i++) {
            wscSds[i] = new DeviceConfig();
            wscSds[i].Name = startUpWSC.assemblies[i].assemblyRoleName;
            wscSds[i].subdevice = new DeviceConfig[0];
        }
        weatherConfiguration.subdevice = wscSds;

    }

    public int getAntennaTimeout() {
        return timeout;
    }

    public void setAntennaTimeout(int timeout) {
        this.timeout = timeout;
    }

    public DeviceConfig getAOSTimingDeviceConfiguration() {
        return aosTimingConfiguration;
    } 

    public DeviceConfig getCentralLOConfiguration() {
        return cloConfiguration;
    }

    public DeviceConfig getWeatherStationConfiguration() {
        return weatherConfiguration;
    } 

    public DeviceConfig getWeatherStCtrlConfiguration() {
        return weatherStationCtrlConfiguration;
    }
}
