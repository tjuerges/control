/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.Control.Master;

import alma.ArrayExceptions.wrappers.AcsJInitializationErrorEx;
import alma.ArrayExceptions.wrappers.AcsJInvalidRequestEx;
import alma.Control.Common.PublisherResource;
import alma.Control.Common.Resource;
import alma.Control.Common.ResourceManager;
import alma.Control.Common.State;
import alma.Control.Common.StaticResource;
import alma.ControlSocketServer.AmbSocketServer;
import alma.ResourceExceptions.wrappers.AcsJBadResourceEx;
import alma.ResourceExceptions.wrappers.AcsJNotYetAcquiredEx;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.ResourceExceptions.wrappers.AcsJResourceExceptionsEx;
import alma.ResourceExceptions.wrappers.AcsJUnknownResourceEx;
import alma.TMCDB.Access;
import alma.TMCDB.TMCDBComponent;

/**
 * This represents the state of the Master component when it is starting.
 *
 */
public class MasterStartingUpState extends MasterStateBase {

    private int antennaTimeout = -1;

    /**
     * Constructor.
     *
     * @param master
     *      MasterInternal interface, this interface is implemented by the
     *      Master component, and it is passed to the state at runtime.
     */
    public MasterStartingUpState(MasterInternal master) {
        super(master, State.Inaccessible_StartingUpPass1);
    }

    /**
     * StartupPass1 lifecycle method. This method should access and initialize all
     * resources that deal with ACS and ARCHIVE, leaving resources in other subsystems
     * to StartupPass2.
     */
    @Override
    public void startupPass1() throws AcsJInvalidRequestEx, AcsJInitializationErrorEx {
        setState(State.Inaccessible_StartingUpPass1);

        ResourceManager resMng = ResourceManager.getInstance(container);

        // Telescope & Monitor Configuration Database (TMCDB) Resource
        logger.info("Accessing TMCDB default component ("+TMCDB_RESOURCE_NAME+").");
        Configurator configurator = null;
        try {
            resMng.acquireResource(
                    new StaticResource<Access>(
                        container,
                        TMCDB_RESOURCE_NAME,
                        TMCDB_HELPER_CLASS,
                        true, // this is a critical component
                        true, // and is defaulted in the CDB as well
                        logger)
                );
            configurator =
                new TMCDBConfigurator(
                        (Access)resMng.getComponent(TMCDB_RESOURCE_NAME),
                        logger);
        } catch (AcsJResourceExceptionsEx ex) {
            // Default to the previous implementation
            try {
                resMng.acquireResource(
                        new StaticResource<Access>(
                            container,
                            OLD_TMCDB_RESOURCE_NAME,
                            OLD_TMCDB_HELPER_CLASS,
                            true, // this is a critical component
                            true, // and is defaulted in the CDB as well
                            logger)
                    );
                configurator =
                    new OldTMCDBConfigurator(
                            (TMCDBComponent)resMng.getComponent(OLD_TMCDB_RESOURCE_NAME),
                            logger);                
            } catch (AcsJResourceExceptionsEx e) {
                String msg = "Error accessing TMCDB resource";
                AcsJInitializationErrorEx initEx = new AcsJInitializationErrorEx(ex);
                initEx.setProperty("Details", msg);
                logger.warning(msg);
                throw initEx;                
            }
        }

        logger.config("Current Master Configuration:"+configurator.toString());
        master.setConfigurator(configurator);

        // Add CONTROL's external publisher resource
        logger.info("Accessing 'CONTROL_SYSTEM' Notification Channel");
        Resource<?> resource =
            new PublisherResource(container,
                                  EXT_NC_RESOURCE_NAME,
                                  true,
                                  logger);
        resMng.addResource(resource);

        // Add CONTROL's internal publisher resource
        logger.info("Accessing 'CONTROL_REALTIME' Notification Channel");
        resource = new PublisherResource(container,
                                         INT_NC_RESOUCE_NAME,
                                         true,
                                         logger);
        resMng.addResource(resource);

        try {
            resMng.acquireResources(); // get all resources added so far
        } catch (AcsJResourceErrorEx ex) {
            String msg = "Error acquiring resources.";
            AcsJInitializationErrorEx initEx = new AcsJInitializationErrorEx(ex);
            initEx.setProperty("Details", msg);
            logger.warning(msg);
            throw initEx;
        }

        // CONTROL/AOSTiming
        logger.info("Accessing CONTROL/AOSTiming");
        try {
            resMng.acquireResource(
                new AOSTimingResource(configurator,
                                      container,
                                      logger)
            );
        } catch (AcsJResourceErrorEx ex) {
            // AOSTimingResource is not critical, so an exception should not
            // have been thrown. This is a programming error.
            throw new AssertionError("This exception should not have been thrown");
        }

        setState(State.Inaccessible_StartedUpPass1);
        logger.info("Finished Master first pass initialization. " +
        		"Resources controlled by Master:\n" + resMng);
    }

    /**
     * StartupPass2 lifecycle method. This method should initialize all resources
     * that depend on other subsystems.
     */
    @Override
    public void startupPass2() throws AcsJInvalidRequestEx, AcsJInitializationErrorEx {

        // Get the configurator acquired on StartUp Pass1
        Configurator configurator = master.getConfigurator();

        ResourceManager resMng = ResourceManager.getInstance(container);

        // Start all the weather stations
        logger.info("Starting all weather stations");
        try {
            resMng.acquireResource(
                new WeatherStationControllerResource(configurator,
                                                     container,
                                                     logger));
        } catch (AcsJResourceErrorEx ex) {
            // WeatherStationControllerResource is not critical, so an exception
            // should not have been thrown. This is a programming error.
            throw new AssertionError("This exception should not have been thrown");
        }

        /* This can probably be done in parallel with the weather stations
           but must be completed before we start the antennas 
           JSK 2009-07-29
        */
        logger.info("Starting the Central LO");
        try {
            resMng.acquireResource(new CentralLOResource(configurator,
                                                         container,
                                                         logger));
        } catch (AcsJResourceErrorEx ex) {
            // The Central LO resource is not critical (but it's very close),
            // so an exception should not have been thrown. This is 
            // a programming error.
            throw new AssertionError("This exception should not have been thrown");
        }

        // Start antennas
        int timeout;
        if (antennaTimeout < 0)
            timeout = configurator.getAntennaTimeout();
        else
            timeout = antennaTimeout;
        logger.info("Starting antennas... (Timeout is "+timeout/1000+" s.)");
        // The AntennaManager creates and manages the AntennaResources
        AntennaManager antennaMng =
            new AntennaManager(container, configurator, logger);
        master.setAntennaManager(antennaMng);
        logger.fine("Waiting for antennas to be initialized...");
        antennaMng.initializeAntennas();

        // CONTROL/AmbSocketServer
        logger.info("Accessing CONTROL/AmbSocketServer.");
        try {
            resMng.acquireResource(
                new StaticResource<AmbSocketServer>(
                        container,
                        AMB_SOCKET_SERVER_RESOURCE_NAME,
                        AMB_SOCKET_SERVER_HELPER_CLASS,
                        false,
                        false,
                        logger)
            );
        } catch (AcsJResourceErrorEx ex) {
            // This resource is not critical, so an exception
            // should not have been thrown. This is a programming error.
            throw new AssertionError("This exception should not have been thrown");
        }

        setState(State.Inaccessible_StartingUpPass2);
        master.setState("OPERATIONAL_STATE");
        
        logger.info("Finished Master initialization. Resources controlled by Master:\n" +
                    resMng);
    }

    protected void setAntennaTimeout(int timeout) {
        antennaTimeout = timeout;
    }
}

// __oOo__
