/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.Control.Master;

import java.util.logging.Logger;

import alma.ACSErrTypeCommon.wrappers.AcsJBadParameterEx;
import alma.ArrayExceptions.wrappers.AcsJInitializationErrorEx;
import alma.ArrayExceptions.wrappers.AcsJInvalidRequestEx;
import alma.Control.AntennaStateEvent;
import alma.Control.CorrelatorType;
import alma.Control.ResourceId;
import alma.Control.Common.State;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.ResourceExceptions.wrappers.AcsJResourceExceptionsEx;
import alma.acs.container.ContainerServices;

public class MasterStateBase implements MasterState {
	
	public static final String TMCDB_HELPER_CLASS = "alma.TMCDB.AccessHelper";
    public static final String TMCDB_MONITOR_HELPER_CLASS = "alma.Control.MonitorInterfaceHelper";
    public static final String AMB_SOCKET_SERVER_HELPER_CLASS = "alma.ControlSocketServer.AmbSocketServerHelper";
    public static final String AMB_SOCKET_SERVER_RESOURCE_NAME = "CONTROL/AmbSocketServer";
    public static final String TMCDB_MONITOR_RESOURCE_NAME = "TMCDB_MONITOR";
    public static final String INT_NC_RESOUCE_NAME = "CONTROL_REALTIME";
    public static final String EXT_NC_RESOURCE_NAME = "CONTROL_SYSTEM";
    public static final String TMCDB_RESOURCE_NAME = "IDL:alma/TMCDB/Access:1.0";
    // to be removed after complete migration of the TMCDBComponent:
    public static final String OLD_TMCDB_HELPER_CLASS = "alma.TMCDB.TMCDBComponentHelper";
    public static final String OLD_TMCDB_RESOURCE_NAME = "IDL:alma/TMCDB/TMCDBComponent:1.0";
    protected MasterInternal master;
	protected State extState;
    protected ContainerServices container;
    protected Logger logger;
	
	public MasterStateBase(MasterInternal master, State state) {
		this.master = master;
		this.extState = state;
        this.container = master.getContainerServices();
        this.logger = master.getLogger();
	}

    ////////////////////////////////////////////////////////////////////
    // Public interface
    ////////////////////////////////////////////////////////////////////    

	public int getStateValue() {
		return extState.getStateValue();
	}

	public int getSubstateValue() {
		return extState.getSubstateValue();
	}

	public void shutdownPass1() throws AcsJInvalidRequestEx {
        String msg = "shutdownPass1() cannot be executed when the state is "
            + extState.toString();
        AcsJInvalidRequestEx invEx = new AcsJInvalidRequestEx();
        invEx.setProperty("State", extState.toString());
        invEx.setProperty("Message", msg);
        throw invEx;
	}

	public void shutdownPass2() throws AcsJInvalidRequestEx {
        String msg = "shutdownPass2() cannot be executed when the state is "
            + extState.toString();
        AcsJInvalidRequestEx invEx = new AcsJInvalidRequestEx();
        invEx.setProperty("State", extState.toString());
        invEx.setProperty("Message", msg);
        throw invEx;
	}

	public void startupPass1() throws AcsJInvalidRequestEx, AcsJInitializationErrorEx {
        String msg = "startupPass1() cannot be executed when the state is "
            + extState.toString();
        AcsJInvalidRequestEx invEx = new AcsJInvalidRequestEx();
        invEx.setProperty("State", extState.toString());
        invEx.setProperty("Message", msg);
        throw invEx;
	}

	public void startupPass2() throws AcsJInvalidRequestEx, AcsJInitializationErrorEx {
        String msg = "startupPass2() cannot be executed when the state is "
            + extState.toString();
        AcsJInvalidRequestEx invEx = new AcsJInvalidRequestEx();
        invEx.setProperty("State", extState.toString());
        invEx.setProperty("Message", msg);
        throw invEx;
	}

	public void waitShutdownPass1() throws AcsJInvalidRequestEx {
        String msg = "waitShutdownPass1() cannot be executed when the state is "
            + extState.toString();
        AcsJInvalidRequestEx invEx = new AcsJInvalidRequestEx();
        invEx.setProperty("State", extState.toString());
        invEx.setProperty("Message", msg);
        throw invEx;
	}
	
	public String getArrayComponentName(String arrayName)
		throws AcsJInvalidRequestEx {
        String msg = "getArrayComponentName() cannot be executed when the state is "
            + extState.toString();
        AcsJInvalidRequestEx invEx = new AcsJInvalidRequestEx();
        invEx.setProperty("State", extState.toString());
        invEx.setProperty("Message", msg);
        throw invEx;
	}
	
    public void initialize() {}
    
    public void shutdown() {}
    
    public String createAutomaticArray(String[] antennas, String[] photonicReferences, CorrelatorType corrType)
        throws AcsJResourceExceptionsEx, alma.ControlExceptions.wrappers.AcsJInvalidRequestEx {
        return null;
    }

    public String createManualArray(String[] antennas, String[] photonicReferences, CorrelatorType corrType)
        throws AcsJResourceExceptionsEx, alma.ControlExceptions.wrappers.AcsJInvalidRequestEx {
        return null;
    }

    public void destroyArray(String arrayName) throws AcsJBadParameterEx {}    
    
    public void reinitializeAntenna(String antennaName) 
        throws AcsJBadParameterEx, AcsJResourceErrorEx {}
    
//    public void killAntenna(String antennaName) throws AcsJBadParameterEx {
//    }

    public void resucitateAntenna(String antennaName) throws AcsJBadParameterEx {
    }    
    public void setAntennaOffline(String antennaName) throws AcsJBadParameterEx {}
    
    public void setAntennaOnline(String antennaName) throws AcsJBadParameterEx {}

    public ResourceId[] getAutomaticArrayResources() {
        return null;
    }

    public ResourceId[] getManualArrayResources() {
        return null;
    }

    public ResourceId[] getAvailableAntennaResources() {
        return null;
    }

    public ResourceId[] getAvailablePhotonicReferenceResources()
        throws AcsJResourceExceptionsEx {
        return null;
    }

    public ResourceId[] getOfflineAntennaResources() {
        return null;
    }
    
    public AntennaStateEvent[] getAvailableAntennaStates() {
        return null;
    }
    
    ////////////////////////////////////////////////////////////////////
    // Private methods
    ////////////////////////////////////////////////////////////////////    
    
    // ...
    
    ////////////////////////////////////////////////////////////////////
    // Protected methods
    ////////////////////////////////////////////////////////////////////    

    protected void setState(State state) {
        this.extState = state;
    }
}
