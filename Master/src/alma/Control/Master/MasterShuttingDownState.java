/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.Control.Master;

import alma.ArrayExceptions.wrappers.AcsJInvalidRequestEx;
import alma.Control.Common.ResourceManager;
import alma.Control.Common.State;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;

public class MasterShuttingDownState extends MasterStateBase {

    private static final int SHUTDOWN_TIMEOUT = 300000; // 5 minutes should be enough?
    
	public MasterShuttingDownState(MasterInternal master) {
		super(master, State.Inaccessible_ShuttingDownPass1);
	}
	
    @Override
    public void shutdownPass1() throws AcsJInvalidRequestEx {
        ResourceManager resMng = 
            ResourceManager.getInstance(master.getContainerServices());
        
        try {
            // Shutdown the CentralLO
            resMng.releaseResource(CentralLOResource.RESOURCE_NAME);
        } catch (Exception ex) {
            // nothing to be done
            ex.printStackTrace();
        }

        try {
            // Shutdown the AOSTiming
            resMng.releaseResource(AOSTimingResource.RESOURCE_NAME);
        } catch (Exception ex) {
            // nothing to be done
            ex.printStackTrace();
        }            

        try {
            // then shutdown antennas
            master.getAntennaManager().shutdownAntennas();
        } catch (Exception ex) {
            // nothing to be done
            ex.printStackTrace();
        }
            
        // finally shutdown all the remaining resources
        try {
            if (!resMng.releaseResources(SHUTDOWN_TIMEOUT))
                logger.warning("Time out releasing resources");
        } catch (Exception ex) {
            // nothing to be done
            ex.printStackTrace();
        }

        try {
            resMng.freeAllResources();
        } catch (Exception ex) {
            // nothing to be done
            ex.printStackTrace();            
        }
        
        setState(State.Inaccessible_ShuttingDownPass2);
        logger.finest("RETURN");
    }
    
    @Override
    public void shutdownPass2() throws AcsJInvalidRequestEx {
        master.setState("SHUTDOWN_STATE");
    }
}
