/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ControlMasterComponentHelper.java
 */
package alma.Control.Master;

import java.util.logging.Logger;

import alma.ACS.MasterComponentOperations;
import alma.ACS.MasterComponentPOATie;
import alma.acs.component.ComponentLifecycle;
import alma.acs.container.ComponentHelper;


/**
 * The ControlMasterComponentCreator is required by the ACS container.
 *
 * @version 1.0 Mar 17, 2005
 * @author Allen Farris
 */
public class ControlMasterComponent2Creator extends ComponentHelper {

    /**
     * @param containerLogger The container's logger.
     */
    public ControlMasterComponent2Creator(Logger containerLogger) {
        super(containerLogger);
    }

    /** 
     * @see alma.acs.container.ComponentHelper#_createComponentImpl()
     */
    protected ComponentLifecycle _createComponentImpl() {
        return new ControlMasterComponent2();
    }

    /**
     * @see alma.acs.container.ComponentHelper#_getPOATieClass()
     */
    protected Class _getPOATieClass() {
        return MasterComponentPOATie.class;
    }

    /**
     * @see alma.acs.container.ComponentHelper#_getOperationsInterface()
     */
    protected Class _getOperationsInterface() {
        return MasterComponentOperations.class;
    }

    /** 
     * @see alma.acs.container.ComponentHelper#_getComponentMethodsExcludedFromInvocationLogging()
     */
    protected String[] _getComponentMethodsExcludedFromInvocationLogging() {
	return new String[] {"OFFSHOOT::alma.ACS.ROstringSeq#get_sync"};
    }


}
