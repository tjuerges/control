/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.Control.Master;

import alma.ACSErrTypeCommon.wrappers.AcsJBadParameterEx;
import alma.ArrayExceptions.wrappers.AcsJInitializationErrorEx;
import alma.ArrayExceptions.wrappers.AcsJInvalidRequestEx;
import alma.Control.AntennaStateEvent;
import alma.Control.CorrelatorType;
import alma.Control.ResourceId;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.ResourceExceptions.wrappers.AcsJResourceExceptionsEx;

/**
 * Master state interface. Reifying the State pattern.
 * 
 * @author rhiriart@nrao.edu
 *
 */
public interface MasterState {

    public int getStateValue();
    
    public int getSubstateValue();
    
    public void shutdownPass1() throws AcsJInvalidRequestEx;
    
    public void shutdownPass2() throws AcsJInvalidRequestEx;
    
    public void startupPass1() throws AcsJInvalidRequestEx, AcsJInitializationErrorEx;
    
    public void startupPass2() throws AcsJInvalidRequestEx, AcsJInitializationErrorEx;
    
    public void waitShutdownPass1() throws AcsJInvalidRequestEx;
	
    public void initialize();
    
    public void shutdown();
    
    public String createAutomaticArray(String[] antennas, String[] photonicReferences,
    		CorrelatorType correlatorType)
        throws AcsJResourceExceptionsEx, alma.ControlExceptions.wrappers.AcsJInvalidRequestEx;
    
    public String createManualArray(String[] antennas, String[] photonicReferences,
    		CorrelatorType correlatorType)
        throws AcsJResourceExceptionsEx, alma.ControlExceptions.wrappers.AcsJInvalidRequestEx;

    public void destroyArray(String arrayName) throws AcsJBadParameterEx;
    
    public void reinitializeAntenna(String antennaName) 
        throws AcsJBadParameterEx, AcsJResourceErrorEx;
    
//    public void killAntenna(String antennaName) throws AcsJBadParameterEx;
    
    public void resucitateAntenna(String antennaName) throws AcsJBadParameterEx;
   
    public void setAntennaOffline(String antennaName) throws AcsJBadParameterEx;
    
    public void setAntennaOnline(String antennaName) throws AcsJBadParameterEx;
    
    public ResourceId[] getAutomaticArrayResources();
    
    public ResourceId[] getManualArrayResources();
    
    public ResourceId[] getAvailableAntennaResources();

    public ResourceId[] getOfflineAntennaResources();

    public ResourceId[] getAvailablePhotonicReferenceResources()
        throws AcsJResourceExceptionsEx;
    
    public AntennaStateEvent[] getAvailableAntennaStates();

	public String getArrayComponentName(String arrayName)
		throws AcsJInvalidRequestEx;

}
