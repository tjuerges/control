/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * $Id$
 */
package alma.Control.Master;

import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;

import alma.ArrayExceptions.wrappers.AcsJInitializationErrorEx;
import alma.Control.AntennaCharacteristics;
import alma.Control.AntennaHelper;
import alma.Control.DeviceConfig;
import alma.Control.Common.ResourceManager;
import alma.Control.Common.ResourceState;
import alma.Control.Common.StaticResource;
import alma.Control.CommonCallbacks.AntennaCallbackImpl;
import alma.ControlCommonExceptions.AsynchronousFailureEx;
import alma.ControlCommonExceptions.OSErrorEx;
import alma.ControlExceptions.HardwareErrorEx;
import alma.ControlExceptions.INACTErrorEx;
import alma.ControlExceptions.IllegalParameterErrorEx;
import alma.ControlExceptions.InvalidRequestEx;
import alma.ControlExceptions.TimeoutEx;
import alma.ResourceExceptions.wrappers.AcsJNotYetAcquiredEx;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.ResourceExceptions.wrappers.AcsJResourceExceptionsEx;
import alma.ResourceExceptions.wrappers.AcsJUnknownResourceEx;
import alma.TMCDBComponentImpl.Pad;
import alma.TmcdbErrType.TmcdbDuplicateKeyEx;
import alma.TmcdbErrType.TmcdbErrTypeEx;
import alma.TmcdbErrType.TmcdbNoSuchRowEx;
import alma.TmcdbErrType.TmcdbRowAlreadyExistsEx;
import alma.TmcdbErrType.TmcdbSqlEx;
import alma.acs.container.ContainerServices;
import alma.maciErrType.CannotGetComponentEx;

/**
 * Helper class used by the Master to manage antennas and its related
 * data structures.
 * 
 * @author rhiriart
 *
 */
public class Antenna implements Observer {
    private static final String cn = Antenna.class.getName();
    
    /** Antenna configuration information, from the TMCDB */
	private alma.TMCDBComponentImpl.Antenna antennaConfiguration;
    
    /** Antenna pad configuration information, from the TMCDB */
	private alma.TMCDBComponentImpl.Pad padConfiguration;
    
    /** Antenna resource */
    private AntennaResource antennaResource;
    
    /** 
     * Antenna device configuration. This structure holds configuration
     * information for the antenna and its subdevices. It is created from the
     * TMCDB antenna startup information and used by the ControlDevice layer.
     */
    private DeviceConfig antennaDeviceConfig;
    
    /** Is the antenna online? */
	private boolean isOnline;	
	
    /** Is the antenna OK or is in an error state? */
    private boolean isOk;
    
    /** The array this antenna has been allocated too. Null if it is not allocated. */
    private String ownerArray;
    
    private ResourceManager resMng;
    
    private Logger logger;

    private ContainerServices container;
    
    private String antennaComponentName;
    
    public Antenna(ContainerServices container, Configurator configurator, String antennaName)
        throws AcsJInitializationErrorEx {
        this(container, configurator, antennaName, null);
    }
    
    public Antenna(ContainerServices container, Configurator configurator, 
                   String antennaName, Logger logger)
        throws AcsJInitializationErrorEx {
        
        this.container = container;
        resMng = ResourceManager.getInstance(container);
        if (logger == null)
            this.logger = container.getLogger();
        else
            this.logger = logger;
                
        antennaComponentName = "CONTROL/" + antennaName;
        antennaDeviceConfig = configurator.getAntennaDeviceConfiguration(antennaName);        
        try {
            antennaResource = 
                new AntennaResource(new StaticResource<alma.Control.Antenna>(container, 
                        antennaComponentName, "alma.Control.AntennaHelper", false, false, logger),
                                    container,
                                    antennaDeviceConfig,
                                    logger);
        } catch (AcsJResourceExceptionsEx ex) {
            throw new AcsJInitializationErrorEx(ex);
        }
        
        resMng.addResource(antennaResource);
        
        try {
            antennaConfiguration = configurator.getAntennaConfiguration(antennaName);
        } catch (TmcdbErrTypeEx ex) {
            AcsJInitializationErrorEx initEx = new AcsJInitializationErrorEx(ex);
            throw initEx;
        } catch (TmcdbSqlEx ex) {
            AcsJInitializationErrorEx initEx = new AcsJInitializationErrorEx(ex);
            throw initEx;
        } catch (TmcdbDuplicateKeyEx ex) {
            AcsJInitializationErrorEx initEx = new AcsJInitializationErrorEx(ex);
            throw initEx;
        } catch (TmcdbNoSuchRowEx ex) {
            AcsJInitializationErrorEx initEx = new AcsJInitializationErrorEx(ex);
            throw initEx;
        }
        try {
            padConfiguration = configurator.getPadConfiguration(antennaName);
        } catch (TmcdbErrTypeEx ex) {
            AcsJInitializationErrorEx initEx = new AcsJInitializationErrorEx(ex);
            throw initEx;
        } catch (TmcdbSqlEx ex) {
            AcsJInitializationErrorEx initEx = new AcsJInitializationErrorEx(ex);
            throw initEx;
        } catch (TmcdbDuplicateKeyEx ex) {
            AcsJInitializationErrorEx initEx = new AcsJInitializationErrorEx(ex);
            throw initEx;
        } catch (TmcdbNoSuchRowEx ex) {
            AcsJInitializationErrorEx initEx = new AcsJInitializationErrorEx(ex);
            throw initEx;
        } catch (TmcdbRowAlreadyExistsEx ex) {
            AcsJInitializationErrorEx initEx = new AcsJInitializationErrorEx(ex);
            throw initEx;
        }
        
        antennaResource.addObserver(this);
    }

    /**
     * Get the antenna name.
     * @return Antenna name.
     */
	public String getAntennaName() {
		return antennaConfiguration.getAntennaName();
	}

    /**
     * Get the antenna component name.
     * @return Antenna component name or null if the antenna component
     * hasn't been acquired and initialized yet.
     */
	public String getAntennaComponentName() {
        return antennaResource.getName();
	}
	
    /**
     * Get the pad name.
     * @return Pad name
     */
	public String getPadName() {
		return padConfiguration.getPadName();
	}

    /**
     * Get the antenna configuration.
     * @return Antenna configuration
     */
    public alma.TMCDBComponentImpl.Antenna getAntennaConfiguration() {
        return antennaConfiguration;
    }

    /**
     * Get the antenna's pad configuration.
     * @return Pad configuration
     */
    public Pad getPadConfiguration() {
        return padConfiguration;
    }

    /**
     * Get the antenna component.
     * 
     * If the antenna component hasn't been acquired and initialized yet, null
     * is returned.
     * @return Antenna component reference or null if the Antenna component hasn't
     * been acquired and initialized.
     */
    public alma.Control.Antenna getAntennaComponent() {
        String fn = "getAntennaComponent";
        String id = getAntennaName();
        String lp = "{"+id+"//"+cn+"::"+fn+"} ";
        
        alma.Control.Antenna antComp = null;
        try {
            antComp = antennaResource.getComponent();
        } catch (AcsJNotYetAcquiredEx ex) {
            logger.fine(lp+"Antenna component hasn't been acquired yet. Returning null.");
            return null;
        }
        
        // Basic sanity check.
        if (antComp != null) {            
            try {
                String name = antComp.getName();
            } catch (INACTErrorEx ex) {
                logger.fine(lp+"ERROR: Antenna is inactive: "+ex+". Returning null.");
                return null;
            } catch (org.omg.CORBA.SystemException ex) {
                logger.fine(lp+"ERROR: CORBA system exception: "+ex+". Returning null.");
                return null;
            }
        }
        
        return antComp;
    }

    /**
     * Get antenna device configuration.
     * @return Antenna device configuration.
     */
    public DeviceConfig getAntennaDeviceConfig() {
        return antennaDeviceConfig;
    }

    /**
     * Get antenna characteristics.
     * 
     * AntennaCharacteristics is an IDL structure that is used to pass around
     * antenna information.
     * 
     * Note: could be replaced for the the IDL version of alma.TMCDB.generated.Antenna?
     *  
     * @return Antenna characteristics.
     */
    public AntennaCharacteristics getAntennaCharacteristics() {
        
        AntennaCharacteristics ac = new AntennaCharacteristics();
        ac = new alma.Control.AntennaCharacteristics ();
        ac.antennaName = antennaConfiguration.getAntennaName();
        ac.dishDiameter = antennaConfiguration.getDishDiameter().toIDLLength();
        ac.xPosition = antennaConfiguration.getXPosition().toIDLLength();
        ac.yPosition = antennaConfiguration.getYPosition().toIDLLength();
        ac.zPosition = antennaConfiguration.getZPosition().toIDLLength();
        ac.xOffset = antennaConfiguration.getXOffset().toIDLLength();
        ac.yOffset = antennaConfiguration.getYOffset().toIDLLength();
        ac.zOffset = antennaConfiguration.getZOffset().toIDLLength();
        ac.dateOfCommission = antennaConfiguration.getCommissionDate().get();
        ac.padId = padConfiguration.getPadName();
        ac.padxPosition = padConfiguration.getXPosition().toIDLLength();
        ac.padyPosition = padConfiguration.getYPosition().toIDLLength();
        ac.padzPosition = padConfiguration.getZPosition().toIDLLength();
        ac.cableDelay = 0;
        return ac;
    }
    
    /**
     * Is the antenna online?
     * @return True if the antenna is online. False otherwise.
     */
    public boolean isOnline() {
        return isOnline;
    }

    /**
     * Set the antenna online.
     *
     */
	public void offline() {
		isOnline = false;
	}
    
    /**
     * Set the antenna offline.
     *
     */
	public void online() {
		isOnline = true;
	}

    /**
     * Is the antenna OK or in some error state?
     * @return True if the antenna is OK. False otherwise.
     */
    public boolean isOk() {
        return isOk;
    }
    
    /**
     * Sets the state of the antenna to OK.
     *
     */
    protected void ok() {
        isOk = true;
    }
    
    /**
     * Sets the state of the antenna to not-OK, i.e., it is in some error state.
     *
     */
    protected void error() {
        isOk = false;
    }
    
    public boolean isAvailable() {
        return (ownerArray == null);
    }
    
    /**
     * Allocates the antenna to the specified array.
     * 
     * Precondition: the antenna is not allocated already.
     * 
     * @param arrayName Array.
     * 
     */
    public void allocate(String arrayName, AntennaCallbackImpl cb) {
        String fn = "allocate";
        String id = getAntennaName();
        String lp = "{"+id+"//"+cn+"::"+fn+"} ";

        assert ownerArray == null: "Antenna is already allocated";
        ownerArray = arrayName;
        try {
            antennaResource.getComponent().assignAsynch(arrayName, cb.getExternalInterface());
    //    } catch (INACTErrorEx ex) {
    //        logger.warning(lp+"Unable to assign " + id + " Antenna is shutdown"); 
        } catch (AcsJNotYetAcquiredEx ex) {
            logger.fine(lp+"Antenna component hasn't been acquired yet.");
            //FIXME-> report to the callback this failure.
        }
    // Left this commented block as manner of documentation. It should be removed at some point.
    //    } catch (CannotGetComponentEx ex) {
    //        // TODO Array should be left in degraded state.
    //        logger.fine(lp+"Antenna component couldn't be assigned.");
    //    } catch (org.omg.CORBA.SystemException ex) {
    //        logger.fine(lp+"ERROR: CORBA system exception: "+ex+". Returning null.");
    //    } catch (InvalidRequestEx e) {
    //        // TODO Auto-generated catch block
    //        logger.warning(lp+"Unable to assign " + id + " there was an internal error during the assigment");
    //        e.printStackTrace();
    //    } catch (HardwareErrorEx e) {
    //        // TODO Auto-generated catch block
    //        logger.warning(lp+"Unable to assign " + id + " there was an internal error during the assigment");
    //        e.printStackTrace();
    //    } catch (OSErrorEx e) {
    //        // TODO Auto-generated catch block
    //        logger.warning(lp+"Unable to assign " + id + " there was an internal error during the assigment");
    //        e.printStackTrace();
    //    } catch (AsynchronousFailureEx e) {
    //        // TODO Auto-generated catch block
    //        logger.warning(lp+"Unable to assign " + id + " there was an internal error during the assigment");
    //        e.printStackTrace();
    //    } catch (TimeoutEx e) {
    //        // TODO Auto-generated catch block
    //        logger.warning(lp+"Unable to assign " + id + " there was an internal error during the assigment");
    //        e.printStackTrace();
    //    } catch (IllegalParameterErrorEx e) {
    //        // TODO Auto-generated catch block
    //        logger.warning(lp+"Unable to assign " + id + " there was an internal error during the assigment");
    //        e.printStackTrace();
    //    }
    }
    
    public void deallocate() {
        String fn = "deallocate";
        String id = getAntennaName();
        String lp = "{"+id+"//"+cn+"::"+fn+"} ";

        ownerArray = null;
        try {
            antennaResource.getComponent().unassign();
        } catch (INACTErrorEx ex) {
            logger.warning(lp+"Unable to unassign " + id + " Antenna is shutdown");
        } catch (AcsJNotYetAcquiredEx ex) {
            logger.fine(lp+"Antenna component hasn't been acquired yet.");
        } catch (org.omg.CORBA.SystemException ex) {
            logger.fine(lp+"ERROR: CORBA system exception: "+ex+". Returning null.");
        }
    }
    
    public void reinitialize() throws AcsJResourceErrorEx {
        String fn = "reinitialize";
        String id = getAntennaName();
        String lp = "{"+id+"//"+cn+"::"+fn+"} ";
        
        logger.finest(lp+"ENTRY");
        
//        try {
//            resMng.refreshResource(antennaResource.getName(), 60000);
//        } catch (AcsJUnknownResourceEx ex) {
//            // This shouldn't happen. It would mean that there is some kind
//            // of internal bug.
//            logger.severe("Antenna is not being managed by the ResourceManager: " + 
//                          antennaResource.getName());
//            ex.printStackTrace();
//            throw new AssertionError();
//        }
        antennaResource.reinitialize();
        
        logger.finest(lp+"RETURN");
    }

//    public void kill() {
//        resMng.forgetResource(antennaResource.getName());
//        antennaResource = null;
//    }

    public void resucitate() {
        try {            
            antennaResource.getConcreteResource().acquire(true);
            antennaResource.startup();
//            alma.Control.Antenna a =
//                AntennaHelper.narrow(container.getComponent(antennaComponentName));
//            a.createSubdevices(antennaDeviceConfig, "");
//            a.controllerOperational();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
    public void update(Observable o, Object arg) {
        String fn = "update";
        String id = getAntennaName();
        String lp = "{"+id+"//"+cn+"::"+fn+"} ";

        ResourceState rs = (ResourceState) arg;
        logger.finer(lp+"Resouce State :"+rs.toString());
        if (rs == ResourceState.OPERATIONAL) {
            logger.finer(lp+"Antenna " + getAntennaName() + " is online");
            ok();
            online();
        } else if (rs == ResourceState.ERROR) {
            logger.finer(lp+"Antenna " + getAntennaName() + " is in error state");
            error();
            online();
        }
    }

    @Override
    public String toString() {
        return super.toString()+"{name="+getAntennaName()+"}";
    }
}
