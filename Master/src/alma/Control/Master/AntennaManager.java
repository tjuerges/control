/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.Control.Master;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.cosylab.CDB.DAO;

import alma.ACSErrTypeCommon.wrappers.AcsJBadParameterEx;
import alma.ArrayExceptions.wrappers.AcsJInitializationErrorEx;
import alma.Control.CentralLO;
import alma.Control.CorrelatorType;
import alma.Control.ResourceId;
import alma.Control.Common.Resource;
import alma.Control.Common.ResourceManager;
import alma.Control.Common.Util;
import alma.ControlExceptions.wrappers.AcsJInvalidRequestEx;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.ResourceExceptions.wrappers.AcsJBadResourceEx;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.ResourceExceptions.wrappers.AcsJResourceExceptionsEx;
import alma.ResourceExceptions.wrappers.AcsJUnknownResourceEx;
import alma.acs.container.ContainerServices;
import alma.cdbErrType.CDBRecordDoesNotExistEx;
import alma.cdbErrType.CDBXMLErrorEx;

/**
 * The AntennaManager manages a collection of Antennas.
 * 
 * It is responsible of checking the logical conditions that relate Arrays and
 * Antennas. For example, an Array can only be created if the antennas are available
 * and online.
 * 
 * @author Rafael Hiriart (rhiriart@nrao.edu)
 *
 */
public class AntennaManager {

    /** ACS Container Services */
    private ContainerServices container;
    
    /** The logger */
    private Logger logger;
    
    /** Configuration information provider, mainly from the TMCDB */
    private Configurator configurator;
    
    /** All the antennas the system knows about */
    private Map<String, Antenna> antennas;

    /** Active AutomaticArrays */
    private Map<String, AutomaticArray> automaticArrays;
    
    /** Active ManualArrays */
    private Map<String, ManualArray> manualArrays;
    
    /** Counter used to generate array names */
    private int arrayCounter;

    private Map<String, Boolean> corrBulkDataDistributors;
    
    private Map<String, Boolean> tpBulkDataDistributors;
    
    /**
     * Constructor.
     * @param container ACS ContainerServices
     * @param configurator Configuration information provider
     * @throws AcsJInitializationErrorEx
     */
    public AntennaManager(ContainerServices container, Configurator configurator)
        throws AcsJInitializationErrorEx {
        this(container, configurator, null);
    }
    
    /**
     * Constructor.
     * @param container ACS ContainerServices.
     * @param configurator Configurator. It's a configuration information provider
     * (a wrapper over the TMCDB, in other words.)
     */
    public AntennaManager(ContainerServices container, Configurator configurator,
                          Logger logger)
        throws AcsJInitializationErrorEx {
        
        this.container = container;
        this.configurator = configurator;
        if (logger != null)
            this.logger = logger;
        else
            this.logger = container.getLogger();
        
        this.logger.fine("Creating antennas...");
        String[] antennaNames = configurator.getAntennas();
        Antenna[] ants = new Antenna[antennaNames.length];
        for (int i=0; i < antennaNames.length; i++) {
            ants[i] = new Antenna(container,
                                  configurator,
                                  antennaNames[i],
                                  logger);
        }

        this.antennas = new HashMap<String, Antenna>();
        for (int i=0; i<ants.length; i++)
            this.antennas.put(ants[i].getAntennaName(), ants[i]);
        
        automaticArrays = new HashMap<String, AutomaticArray>();
        manualArrays = new HashMap<String, ManualArray>();
        arrayCounter = 0;

        // Read from the CDB/TMCDB the bulkdata distributors that have been defined.
        // The convention is to name the correlator bulk data distributors as
        // CORRELATOR_BULKDATA_DISTRIBUTOR_1, CORRELATOR_BULKDATA_DISTRIBUTOR_2, etc.,
        // and the total power bulk data distributors as TOTALPOWER_BULKDATA_DISTRIBUTOR_1, 
        // TOTALPOWER_BULKDATA_DISTRIBUTOR_2, etc.
        corrBulkDataDistributors = new HashMap<String, Boolean>();
        tpBulkDataDistributors = new HashMap<String, Boolean>();
        try {
            String daoList = container.getCDB().list_daos("MACI/Components");
            String[] daos = daoList.split(" ");
            for (String dao : daos) {
                if (dao.startsWith("CORRELATOR_BULKDATA_DISTRIBUTOR_")) {
                    logger.info("adding " + dao + " into the correlator bulk data distributor list");
                    corrBulkDataDistributors.put(dao, true);
                } else if (dao.startsWith("TOTALPOWER_BULKDATA_DISTRIBUTOR_")) {
                    logger.info("adding " + dao + " into the total power bulk data distributor list");
                    tpBulkDataDistributors.put(dao, true);
                }
            }
        } catch (AcsJContainerServicesEx ex) {
            throw new AcsJInitializationErrorEx(ex);
        }
    }
    
    /**
     * Creates an AutomaticArray.
     * 
     * @param antennaNames Antenna names. These are the antenna names, <STRONG>not</STRONG> the
     * antenna component names.
     * @return AutomaticArray instance.
     * @throws AcsJResourceErrorEx If there was a problem creating the AutomaticArray
     * component.
     * @throws AcsJInvalidRequestEx 
     */
    synchronized public AutomaticArray createAutomaticArray(String[] antennaNames,
            String[] photonicReferences, CorrelatorType correlatorType)
        throws AcsJResourceExceptionsEx, AcsJInvalidRequestEx {
        
        if (antennaNames.length == 0) {
            AcsJInvalidRequestEx invReqEx = new AcsJInvalidRequestEx();
            String msg = "It is not allowed to create an Array with no antennas.";
            invReqEx.setProperty("Details", msg);
            throw invReqEx;
        }
        checkBulkDataDistributorsAvailability();
            
        ResourceId[] photRefs = checkPhotonicReferences(photonicReferences);
        List<Antenna> arrayAntennas = null;
        try {
            arrayAntennas = getAntennaSubList(antennaNames);
            checkAntennasAreAvailable(arrayAntennas);
            checkAntennasAreOnline(arrayAntennas);
        } catch(IllegalArgumentException ex) {
            AcsJInvalidRequestEx invReqEx = new AcsJInvalidRequestEx(ex);
            throw invReqEx;
        }

        Resource<CentralLO> clo = ResourceManager
                                  .getInstance(container)
                                  .getResource(CentralLOResource.RESOURCE_NAME);
        CentralLOResource clo2 = (CentralLOResource) clo;
        
        AutomaticArray array = null;
        String arrayName = generateArrayName();
        array = new AutomaticArray(container, 
                               arrayName,
                               "Master",
                               arrayAntennas.toArray(new Antenna[0]),
                               getNextAvailableCorrelatorBulkDataDistributor(),
                               getNextAvailableTotalPowerBulkDataDistributor(),
                               photRefs,
                               configurator,
                               correlatorType);
        // needs to be called after the Array has been created
        // because the PhotoReferences will access the Array
        clo2.assignPhotonicReferences(arrayName, photRefs);
        automaticArrays.put(array.getName(), array);
        return array;
    }

    /**
     * Creates a ManualArray.
     * 
     * @param antennaNames Antenna names. These are the antenna names, <STRONG>not</STRONG> the
     * antenna component names.
     * @return ManualArray instance.
     * @throws AcsJResourceErrorEx If there was a problem creating the ManualArray
     * component.
     * @throws AcsJInvalidRequestEx 
     */
    synchronized public ManualArray createManualArray(String[] antennaNames,
            String[] photonicReferences, CorrelatorType correlatorType)
        throws AcsJResourceExceptionsEx, AcsJInvalidRequestEx {

        if (antennaNames.length == 0) {
            AcsJInvalidRequestEx invReqEx = new AcsJInvalidRequestEx();
            String msg = "It is not allowed to create an Array with no antennas.";
            invReqEx.setProperty("Details", msg);
            throw invReqEx;
        }
        checkBulkDataDistributorsAvailability();

        ResourceId[] photRefs = checkPhotonicReferences(photonicReferences);

        List<Antenna> arrayAntennas = null;
        try {
            arrayAntennas = getAntennaSubList(antennaNames);
        
            checkAntennasAreAvailable(arrayAntennas);
            checkAntennasAreOnline(arrayAntennas);
        } catch(IllegalArgumentException ex) {
            AcsJInvalidRequestEx invReqEx = new AcsJInvalidRequestEx(ex);
            throw invReqEx;
        }
        
        Resource<CentralLO> clo = ResourceManager
                                  .getInstance(container)
                                  .getResource(CentralLOResource.RESOURCE_NAME);
        CentralLOResource clo2 = (CentralLOResource) clo;

        ManualArray array = null;
        String arrayName = generateArrayName();
        array =
            new ManualArray(container, 
                            arrayName,
                            "Master",
                            arrayAntennas.toArray(new Antenna[0]),
                            getNextAvailableCorrelatorBulkDataDistributor(),
                            getNextAvailableTotalPowerBulkDataDistributor(),
                            photRefs,
                            configurator,
                            correlatorType);
        // needs to be called after the Array has been created
        // because the PhotoReferences will access the Array
        clo2.assignPhotonicReferences(arrayName, photRefs);
        manualArrays.put(array.getName(), array);
        return array;
    }
    
    /**
     * Get all active AutomaticArrays.
     * @return AutomaticArray array.
     */
    public AutomaticArray[] getAutomaticArrays() {
        return automaticArrays.values().toArray(new AutomaticArray[0]);
    }
    
    /**
     * Get all active ManualArrays.
     * @return ManualArray array.
     */
    public ManualArray[] getManualArrays() {
        return manualArrays.values().toArray(new ManualArray[0]);
    }
    
    /**
     * Destroy an array.
     * @param arrayName Name of the array to destroy.
     * @throws AcsJBadParameterEx If there's not an array with the passed name.
     */
    public void destroyArray(String arrayName)
        throws AcsJBadParameterEx {
        
        AutomaticArray aarray = automaticArrays.get(arrayName);
        ManualArray marray = manualArrays.get(arrayName);
        CentralLOResource clo2 = null;
        try {
            Resource<CentralLO> clo = ResourceManager
                                      .getInstance(container)
                                      .getResource(CentralLOResource.RESOURCE_NAME);
            clo2 = (CentralLOResource) clo;
        } catch (AcsJResourceExceptionsEx ex) {
            ex.printStackTrace();
        }

        if (aarray != null) {
            if (clo2 != null) {
                try {
                    clo2.unassignPhotonicReferences(aarray.getPhotonicReferences());
                } catch (AcsJInvalidRequestEx ex) {
                    ex.printStackTrace();
                }
            }
            corrBulkDataDistributors.put(aarray.getCorrelatorBulkDataDistributor(), true);
            tpBulkDataDistributors.put(aarray.getTotalPowerBulkDataDistributor(), true);
            aarray.release();
            automaticArrays.remove(aarray.getName());
        } else {
            if (marray != null) {
                if (clo2 != null) { 
                    try {
                        clo2.unassignPhotonicReferences(marray.getPhotonicReferences());
                    } catch (AcsJInvalidRequestEx ex) {
                        ex.printStackTrace();
                    }
                }
                corrBulkDataDistributors.put(marray.getCorrelatorBulkDataDistributor(), true);
                tpBulkDataDistributors.put(marray.getTotalPowerBulkDataDistributor(), true);
                marray.release();
                manualArrays.remove(marray.getName());
            }
        }
        
        if (aarray == null && marray == null) {
            AcsJBadParameterEx badParamEx = new AcsJBadParameterEx();
            badParamEx.setParameter("arrayName");
            badParamEx.setParameterValue(arrayName);
            String arrNames = "";
            if (automaticArrays.size()>0 || manualArrays.size()>0) {
                Iterator<String> iter = automaticArrays.keySet().iterator();
                while (iter.hasNext())
                    arrNames += iter.next() + ", ";
                Iterator<String> iter2 = automaticArrays.keySet().iterator();
                while (iter2.hasNext())
                    arrNames += iter.next() + ", ";
                arrNames = arrNames.substring(0, arrNames.length() - 2);
                badParamEx.setProperty("Allowed values", arrNames);
            } else {
                arrNames = "No allowed values. It is empty.";
            }
            throw badParamEx;
        }        
    }

    public Antenna[] getAntennas() {
        return antennas.values().toArray(new Antenna[0]);
    }
    
    public Antenna getAntenna(String antennaName) throws AcsJBadParameterEx {
        if (!antennas.containsKey(antennaName)) {
            AcsJBadParameterEx badParamEx = new AcsJBadParameterEx();
            badParamEx.setParameter("antennaName");
            badParamEx.setParameterValue(antennaName);
            String antNames = "";
            if (antennas.size()>0) {
                Iterator<String> iter = antennas.keySet().iterator();
                while (iter.hasNext())
                    antNames += iter.next() + ", ";
                antNames = antNames.substring(0, antNames.length() - 2);
                badParamEx.setProperty("Allowed values", antNames);
            } else {
                antNames = "No allowed values. Empty.";
            }
            throw badParamEx;            
        }
        return antennas.get(antennaName);
    }
    
    public Antenna[] getAvailableAntennas() {
        List<Antenna> availableAntennas = new ArrayList<Antenna>();
        Iterator<Antenna> iter = antennas.values().iterator();
        while(iter.hasNext()) {
            Antenna ant = iter.next();
            if (ant.isAvailable())
                availableAntennas.add(ant);
        }        
        return availableAntennas.toArray(new Antenna[0]);
    }
    
    public Antenna[] getOfflineAntennas() {
        List<Antenna> availableAntennas = new ArrayList<Antenna>();
        Iterator<Antenna> iter = antennas.values().iterator();
        while(iter.hasNext()) {
            Antenna ant = iter.next();
            if (!ant.isOnline())
                availableAntennas.add(ant);
        }        
        return availableAntennas.toArray(new Antenna[0]);        
    }

    public boolean initializeAntennas() {
        
        String[] antennaNames = configurator.getAntennas();
        String[] antennaCompNames = new String[antennaNames.length];
        for (int i = 0; i < antennaNames.length; i++)
            antennaCompNames[i] = "CONTROL/" + antennaNames[i];
        logger.fine("Waiting for antennas to be initialized...");
        boolean timedout = false;
        try {
            timedout = ResourceManager.getInstance(container).
                acquireResources(configurator.getAntennaTimeout(), antennaCompNames);
        } catch (AcsJResourceErrorEx ex) {
            logger.severe("Error starting antennas: "+
                    Util.getNiceErrorTraceString(ex.getErrorTrace()));
        } catch (InterruptedException ex) {
            logger.severe("Resource acquisition thread has been interrupted");
        }
        if (!timedout) {
            logger.severe("Timeout initializing antennas.");            
        } else
            logger.info("All antennas ready.");
        return timedout;
    }

    public boolean shutdownAntennas() {
        String[] antennaNames = configurator.getAntennas();
        String[] antennaCompNames = new String[antennaNames.length];
        for (int i = 0; i < antennaNames.length; i++)
            antennaCompNames[i] = "CONTROL/" + antennaNames[i];
        boolean timedout = false;
        try {
            timedout = ResourceManager.getInstance(container).
                releaseResources(configurator.getAntennaTimeout(),
                                 antennaCompNames);
        } catch (AcsJResourceErrorEx ex) {
            logger.severe("Error starting antennas: "+
                    Util.getNiceErrorTraceString(ex.getErrorTrace()));
        }
        return timedout;
    }
    
    //////////////////////////////////////////////////////////////////////////
    // Private methods
    //
    //////////////////////////////////////////////////////////////////////////

    synchronized void checkBulkDataDistributorsAvailability() throws AcsJInvalidRequestEx {
        if (!checkTotalPowerBulkDataDistributorAvailability()) {
            AcsJInvalidRequestEx ex = new AcsJInvalidRequestEx();
            ex.setProperty("Details", "There are no total power distributors available");
            throw ex;
        }
        if (!checkCorrelatorBulkDataDistributorAvailability()) {
            AcsJInvalidRequestEx ex = new AcsJInvalidRequestEx();
            ex.setProperty("Details", "There are no correlator distributors available");
            throw ex;
        }
    }
    
    synchronized boolean checkTotalPowerBulkDataDistributorAvailability() {
        boolean tpDistAvailable = false;
        Iterator<String> iter = tpBulkDataDistributors.keySet().iterator();
        while(iter.hasNext()) {
            String distr = iter.next();
            if (tpBulkDataDistributors.get(distr)) {
                tpDistAvailable = true;
            }
        }
        return tpDistAvailable;
    }

    synchronized boolean checkCorrelatorBulkDataDistributorAvailability() {
        boolean corrDistAvailable = false;
        Iterator<String> iter = corrBulkDataDistributors.keySet().iterator();
        while(iter.hasNext()) {
            String distr = iter.next();
            if (corrBulkDataDistributors.get(distr)) {
                corrDistAvailable = true;
            }
        }
        return corrDistAvailable;
    }
    
    synchronized private String getNextAvailableTotalPowerBulkDataDistributor()
        throws AcsJInvalidRequestEx {
        Iterator<String> iter = tpBulkDataDistributors.keySet().iterator();
        while(iter.hasNext()) {
            String distr = iter.next();
            if (tpBulkDataDistributors.get(distr)) {
                tpBulkDataDistributors.put(distr, false);
                return distr;
            }
        }
        AcsJInvalidRequestEx ex = new AcsJInvalidRequestEx();
        ex.setProperty("Details", "There are no total power distributors available");
        throw ex;
    }

    synchronized private String getNextAvailableCorrelatorBulkDataDistributor()
        throws AcsJInvalidRequestEx {
        Iterator<String> iter = corrBulkDataDistributors.keySet().iterator();
        while(iter.hasNext()) {
            String distr = iter.next();
            if (corrBulkDataDistributors.get(distr)) {
                corrBulkDataDistributors.put(distr, false);
                return distr;
            }
        }
        AcsJInvalidRequestEx ex = new AcsJInvalidRequestEx();
        ex.setProperty("Details", "There are no correlator distributors available");
        throw ex;
    }    
    
    /**
     * Selects from the antenna list a sub-list containing the antennas
     * which correspond to the antenna names array passed as parameter.
     * @param antennaNames Names of the antennas to select for the sub-list.
     * @return List with the selection of antennas.
     */
    private List<Antenna> getAntennaSubList(String[] antennaNames) {
        
        List<Antenna> sublist = new ArrayList<Antenna>();
        for (int i=0; i<antennaNames.length; i++) {
            if (antennas.keySet().contains(antennaNames[i]))
                sublist.add(antennas.get(antennaNames[i]));
            else
                throw new IllegalArgumentException("Antenna name '" + antennaNames[i] + "' is" +
                        " not a valid antenna");
        }
        return sublist;
    }
    
    /**
     * Checks if all the antennas in the list parameter are available.
     * @param antennas List with the antennas to check for availability.
     */
    private void checkAntennasAreAvailable(List<Antenna> antennas) {
        
        Iterator<Antenna> iter = antennas.iterator();        
        while (iter.hasNext()) {
            Antenna ant = iter.next();
            if (!ant.isAvailable())
                throw new IllegalArgumentException("Antenna '" + ant.getAntennaName() +
                        "' is already allocated");
        }        
    }
    
    /**
     * Checks if all the antennas in the list parameter are online.
     * @param antennas List with the antennas to check for online state.
     */
    private void checkAntennasAreOnline(List<Antenna> antennas) {
        
        Iterator<Antenna> iter = antennas.iterator();        
        while (iter.hasNext()) {
            Antenna ant = iter.next();
            if (!ant.isOnline())
                throw new IllegalArgumentException("Antenna '" + ant.getAntennaName() +
                        "' is not online");
        }        
    }

    /**
     * Generates an array name. 
     * @return Array name.
     */
    private String generateArrayName() {
        arrayCounter++;
        return String.format("Array%03d", arrayCounter);
    }
    
    private ResourceId[] checkPhotonicReferences(String[] photonicReferences)
        throws AcsJResourceExceptionsEx {
        
        // HACK - HACK - HACK - HACK - HACK - HACK - HACK
        // While SCHED implements their part
        if (photonicReferences.length == 0) {
            ResourceId[] prs = new ResourceId[1];
            prs[0] = new ResourceId();
            prs[0].ResourceName = "PhotonicReference1";
            prs[0].ComponentName = "CONTROL/CentralLO/PhotonicReference1";
        }
        // HACK - HACK - HACK - HACK - HACK - HACK - HACK
        
        Resource<CentralLO> clo = ResourceManager
                                  .getInstance(container)
                                  .getResource(CentralLOResource.RESOURCE_NAME);
        CentralLOResource clo2 = (CentralLOResource) clo;
        // Check which ones of the photonicReferences are really available
        ResourceId[] availPhotRefs = clo2.getAvailablePhotonicReferences();
        List<ResourceId> actPhotRefs = new ArrayList<ResourceId>();
        for ( String reqPhotRefName : photonicReferences ) {
            for ( ResourceId availPhotRef : availPhotRefs ) {
                if (availPhotRef.ResourceName.equals(reqPhotRefName)) {
                    actPhotRefs.add(availPhotRef);
                    break;
                }
            }
        }
        // Throw an exception if not all requested PhotonicReferences are
        // available.
        if (actPhotRefs.size() != photonicReferences.length) {
            AcsJResourceErrorEx ex = new AcsJResourceErrorEx();
            String msg = "Some of the PhotonicReferences are not available. ";
            msg += "Requested PhotoReferences: ";
            for ( String reqPhotRefName : photonicReferences )
                msg += reqPhotRefName + ", ";
            msg = msg.substring(0, msg.length()-2);
            msg += "; Available subset: ";
            for ( ResourceId availPhotRef : availPhotRefs )
                msg += availPhotRef.ResourceName + ", ";
            msg = msg.substring(0, msg.length()-2);
            ex.setProperty("Details", msg);
            throw ex;
        }
        return actPhotRefs.toArray(new ResourceId[0]);
    }
}

// __oOo__
