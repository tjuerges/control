/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.Control.Master;

import alma.Control.DeviceConfig;
import alma.Control.SiteData;
import alma.TmcdbErrType.TmcdbDuplicateKeyEx;
import alma.TmcdbErrType.TmcdbErrTypeEx;
import alma.TmcdbErrType.TmcdbNoSuchRowEx;
import alma.TmcdbErrType.TmcdbRowAlreadyExistsEx;
import alma.TmcdbErrType.TmcdbSqlEx;

public interface Configurator {

    public String getConfigurationName();
    
    public String[] getAntennas();

    public DeviceConfig getAntennaDeviceConfiguration(String antennaName);

    public DeviceConfig getAOSTimingDeviceConfiguration();

    public DeviceConfig getWeatherStationConfiguration();

    public DeviceConfig getCentralLOConfiguration();
    
    public alma.TMCDBComponentImpl.Antenna getAntennaConfiguration(String antennaName)
        throws TmcdbErrTypeEx, TmcdbSqlEx, TmcdbDuplicateKeyEx, TmcdbNoSuchRowEx;

    public alma.TMCDBComponentImpl.Pad getPadConfiguration(String antennaName)
        throws TmcdbErrTypeEx, TmcdbSqlEx, TmcdbDuplicateKeyEx, TmcdbNoSuchRowEx,
            TmcdbRowAlreadyExistsEx;

    public SiteData getSiteData();
    
    void setAntennaTimeout(int timeout);
    
    int getAntennaTimeout();
}
