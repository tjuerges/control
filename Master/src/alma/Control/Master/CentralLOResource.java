/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.Control.Master;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.omg.CORBA.SystemException;

import alma.Control.CentralLO;
import alma.Control.DeviceNameMap;
import alma.Control.PhotonicReference;
import alma.Control.PhotonicReferenceHelper;
import alma.Control.ResourceId;
import alma.Control.Common.ResourceState;
import alma.Control.Common.StaticResource;
import alma.ControlDeviceExceptions.IllegalConfigurationEx;
import alma.ControlExceptions.INACTErrorEx;
import alma.ControlExceptions.wrappers.AcsJINACTErrorEx;
import alma.ControlExceptions.wrappers.AcsJInvalidRequestEx;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.ResourceExceptions.wrappers.AcsJNotYetAcquiredEx;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.acs.container.ContainerServices;
import alma.maciErrType.CannotGetComponentEx;

public class CentralLOResource extends StaticResource<CentralLO>{

    public static final String RESOURCE_NAME = "CONTROL/CentralLO";
    public static final String HELPER_CLASS = "alma.Control.CentralLOHelper";
    
    private Configurator configurator;
    
    public CentralLOResource(Configurator configurator,
                             ContainerServices container,
                             Logger logger) {
        super(container,
              RESOURCE_NAME,
              HELPER_CLASS,
              false,
              false,
              logger);
        this.configurator = configurator;
    }
    
    @Override
    public void startup() throws AcsJResourceErrorEx {
        setState(ResourceState.STARTING_UP);
        try {
            CentralLO clo = getComponent();
            clo.createSubdevices(configurator.getCentralLOConfiguration(), "");
            clo.controllerOperational();
        } catch (AcsJNotYetAcquiredEx ex) {
            throw new AssertionError("startup() called in CentralLO before component has been acquired");
        } catch (IllegalConfigurationEx ex) {
            setState(ResourceState.ERROR);
            throw new AcsJResourceErrorEx(ex);
        }
        setState(ResourceState.OPERATIONAL);
    }
    
    @Override
    public void shutdown() throws AcsJResourceErrorEx {
        setState(ResourceState.SHUTTING_DOWN);
        try {
            CentralLO clo = getComponent();
            clo.controllerShutdown();
            clo.releaseSubdevices();
        } catch (AcsJNotYetAcquiredEx ex) {
        	// If the Resource hasn't been acquired, then there's nothing
        	// to be done.
        }
        setState(ResourceState.INITIALIZED);
    }
    
    /**
     * Returns an array with the names (both reference name and component
     * name) of all the available PhotonicReferences. "Available" means that
     * the PhotonicReference is accessible and is in the active state,
     * therefore it can be used by an array. 
     * 
     * @return Array with names of all available PhotonicReferences.
     * @throws AcsJNotYetAcquiredEx If the CentralLO hasn't been acquired yet. 
     */
    protected ResourceId[] getAvailablePhotonicReferences()
        throws AcsJNotYetAcquiredEx {
        List<ResourceId> resources = new ArrayList<ResourceId>();
            CentralLO clo = getComponent();
            DeviceNameMap[] photRefs = clo.getPhotonicReferences();
            for ( DeviceNameMap dev : photRefs ) {
                try {
                    PhotonicReference photRef = 
                        PhotonicReferenceHelper.narrow(container.getComponent(dev.FullName));
                    if ( !photRef.isAssigned() ) {
                        resources.add(new ResourceId(dev.ReferenceName,
                                                     dev.FullName));
                    }
                } catch (AcsJContainerServicesEx ex) {
                    // If getting a reference to one of the PhotoReferences failed, this
                    // could mean that there is an internal error in the CentralLO, because
                    // getPhotonicReferences() returned an invalid list. It could also
                    // mean that one of the ABMs is no longer accessible. This condition
                    // will be detected eventually by the Master. Here we just continue,
                    // as this PhotoReferece is obviously not available.
                    ex.printStackTrace();
                    ex.log(logger);
                } catch (INACTErrorEx ex) {
                    // If PhotoReference is not active, then it is not available.
                    ex.printStackTrace();
                    (new AcsJINACTErrorEx(ex)).log(logger);
                } finally {
                    container.releaseComponent(dev.FullName);
                }
            }
        return resources.toArray(new ResourceId[0]);        
    }
    
    /**
     * Assign PhotonicReferences to an Array.
     * @param arrayName Array name
     * @param photonicReferences Photonic references
     * @throws AcsJInvalidRequestEx
     */
    protected void assignPhotonicReferences(String arrayName,
                                            ResourceId[] photonicReferences)
        throws AcsJInvalidRequestEx {
        for ( ResourceId photRefRes : photonicReferences ) {
            try {
                PhotonicReference photRef = 
                    PhotonicReferenceHelper
                    .narrow(container.getComponent(photRefRes.ComponentName));
                photRef.assign(arrayName);
            } catch (Exception ex) {
                AcsJInvalidRequestEx ex2 = new AcsJInvalidRequestEx();
                ex2.setProperty("Details",
                                "Cannot assign PhotoReference to an Array");
                ex2.setProperty("PhotoReference", photRefRes.ResourceName);
                throw ex2;
            } finally {
                container.releaseComponent(photRefRes.ComponentName);
            }
            
        }
    }

    /**
     * Unassign PhotonicReferences to an Array.
     * @param arrayName Array name
     * @param photonicReferences Photonic references
     * @throws AcsJInvalidRequestEx
     */
    protected void unassignPhotonicReferences(ResourceId[] photonicReferences)
        throws AcsJInvalidRequestEx {
        for (ResourceId photRefRes : photonicReferences) {
            try {
                PhotonicReference photRef = PhotonicReferenceHelper
                        .narrow(container
                                .getComponent(photRefRes.ComponentName));
                photRef.unassign();
            } catch (Exception ex) {
                AcsJInvalidRequestEx ex2 = new AcsJInvalidRequestEx();
                ex2.setProperty("Details",
                                "Cannot unassign PhotoReference to an Array");
                ex2.setProperty("PhotoReference", photRefRes.ResourceName);
                throw ex2;
            } finally {
                container.releaseComponent(photRefRes.ComponentName);
            }
        }
    }
}
