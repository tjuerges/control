/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.Control.Master;

import java.util.logging.Logger;

import alma.Control.Antenna;
import alma.Control.AntennaState;
import alma.Control.AntennaStateEvent;
import alma.Control.AntennaSubstate;
import alma.Control.DeviceConfig;
import alma.Control.Common.AsyncResource2;
import alma.Control.Common.InternalResource;
import alma.Control.Common.ResourceManager;
import alma.Control.Common.ResourceState;
import alma.Control.Common.Util;
import alma.Control.Common.VerboseLogger;
import alma.ControlDeviceExceptions.IllegalConfigurationEx;
import alma.ControlDeviceExceptions.wrappers.AcsJIllegalConfigurationEx;
import alma.ResourceExceptions.wrappers.AcsJNotYetAcquiredEx;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.ResourceExceptions.wrappers.AcsJResourceExceptionsEx;
import alma.acs.container.ContainerServices;
import alma.acs.exceptions.AcsJException;
import alma.acs.nc.SimpleSupplier;

public class AntennaResource extends AsyncResource2<Antenna> {
	
    /**
     * Default Antenna timeout.
     * It should never be reached, unless the AntennaResource is not
     * being started from the AntennaManager, which defines its own
     * timeout for the entire collection of Antennas, and it is really the
     * amount of time that the Master will wait for the antennas to be
     * initialized.
     */
	public static final long DEFAULT_ANTENNA_TIMEOUT = 600000;
    
	private DeviceConfig deviceConfig;
	private VerboseLogger logger;
    private SimpleSupplier publisher;

	public AntennaResource(InternalResource<Antenna> resource,
	                       ContainerServices container, 
                           DeviceConfig config,
                           Logger logger)
        throws AcsJResourceExceptionsEx {
		super(resource, container, DEFAULT_ANTENNA_TIMEOUT, logger);
		this.deviceConfig = config;
        if (logger != null)
            this.logger = new VerboseLogger(logger, AntennaResource.class.getName());
        else
            this.logger = new VerboseLogger(container.getLogger(),
                                            AntennaResource.class.getName());
        
        ResourceManager resMng = ResourceManager.getInstance(container);
        publisher = resMng.getComponent("CONTROL_SYSTEM");
	}

    @Override
	public void shutdown() throws AcsJResourceErrorEx {
		Antenna antenna = null;
        try {
            antenna = getComponent();
        } catch (AcsJNotYetAcquiredEx ex) {
            // Fine, the resource was never acquired.
            // There's nothing to shutdown.
            logger.finer("Antenna component already released");
            return;
        }
        try {
            antenna.controllerShutdown();
            antenna.releaseSubdevices();
        } catch(org.omg.CORBA.SystemException ex) {
            ex.printStackTrace();
            sendAntennaStateEvent(AntennaState.AntennaInaccessable,
                                  AntennaSubstate.AntennaError);
            return;
        } catch(Exception ex) {
            ex.printStackTrace();
            sendAntennaStateEvent(AntennaState.AntennaInaccessable,
                    AntennaSubstate.AntennaError);
            return;
        }
        
        try {
            AntennaStateEvent state = antenna.getAntennaState();
            sendAntennaStateEvent(state.newState, state.newSubstate);
        } catch(org.omg.CORBA.SystemException ex) {
            // nothing to be done
            ex.printStackTrace();
        } catch(Exception ex) {
            // nothing to be done
            ex.printStackTrace();
        }
	}

	@Override
	public void startup() throws AcsJResourceErrorEx {
		Antenna antenna = null;
		
        try {
			antenna = getComponent();
		} catch (AcsJNotYetAcquiredEx ex) {
			throw new AcsJResourceErrorEx(ex);
		}
        
		try {
            antenna.createSubdevices(deviceConfig, "");
        } catch (IllegalConfigurationEx ex) {
            AcsJIllegalConfigurationEx illEx = new AcsJIllegalConfigurationEx(ex);
            throw new AcsJResourceErrorEx(illEx);
        }
        
		antenna.controllerOperational();		
		AntennaStateEvent state = null;
        state = antenna.getAntennaState();
        
        sendAntennaStateEvent(state.newState, state.newSubstate);
	}
	
	@Override
	public ResourceState getUpAsync() throws Exception {
        logger.finest(getName(), "getUpAsync", "ENTRY");
        
        acquire();
        
		Antenna antenna = null;
        antenna = getComponent();
        
		try {
            antenna.createSubdevices(deviceConfig, "");
        } catch (IllegalConfigurationEx ex) {
            AcsJIllegalConfigurationEx illEx = new AcsJIllegalConfigurationEx(ex);
            throw illEx;
        }
        
		antenna.controllerOperational();		
		AntennaStateEvent state = null;
        state = getComponent().getAntennaState();
        logger.finer(getName(),
                     "getUpAsync",
                     "State after initialization = "+ state.newState.toString()+
                     "."+state.newSubstate.toString());
        
        ResourceState rc;
		if (state.newState == AntennaState.AntennaOperational && 
		    state.newSubstate == AntennaSubstate.AntennaNoError) {
            sendAntennaStateEvent(state.newState, state.newSubstate);
            rc = ResourceState.OPERATIONAL;
		} else {
            sendAntennaStateEvent(state.newState, state.newSubstate);
			rc = ResourceState.ERROR;
		}
        logger.finest(getName(), "getUpAsync", "RETURN");
        
        return rc;
	}

	@Override
	public ResourceState getDownAsync() {
        
		Antenna antenna = null;
        try {
            antenna = getComponent();
        } catch (AcsJNotYetAcquiredEx ex) {
            // Fine, the resource was never acquired.
            // There's nothing to shutdown.
            logger.finer("Antenna component already released");
            return ResourceState.UNINITIALIZED;
        }
        
        logger.finer(getName(), "shutdown", "Shutting down antenna...");
        try {
            antenna.controllerShutdown();
            antenna.releaseSubdevices();
        } catch(org.omg.CORBA.SystemException ex) {
            logger.finer(getName(), "shutdown", "CORBA System Exception: "+ex.toString()+". " +
                    "The Antenna container seems to have crashed.");
            ex.printStackTrace();
            sendAntennaStateEvent(AntennaState.AntennaInaccessable,
                                  AntennaSubstate.AntennaError);
            return ResourceState.ERROR;
        } catch(Exception ex) {
            logger.finer(getName(), "shutdown", "Error shutting down antenna.");
            ex.printStackTrace();
            sendAntennaStateEvent(AntennaState.AntennaInaccessable,
                                  AntennaSubstate.AntennaError);
            return ResourceState.ERROR;
        }

        logger.finer(getName(), "shutdown", "Antenna shutdown");
        try {
            AntennaStateEvent state = antenna.getAntennaState();
            sendAntennaStateEvent(state.newState, state.newSubstate);
        } catch(Exception ex) {
            // nothing to be done
            ex.printStackTrace();
        }
        
        try {
            release();
        } catch(Exception ex) {
            // nothing to be done
            ex.printStackTrace();
        }
        
		return ResourceState.UNINITIALIZED;
	}

	protected void reinitialize() throws AcsJResourceErrorEx {
	    shutdown();
	    release();
	    acquire();
	    startup();
	}
	
    private void sendAntennaStateEvent(AntennaState state, AntennaSubstate substate) {
        long now = Util.arrayTimeToACSTime(Util.getArrayTime().get());
        AntennaStateEvent event = new AntennaStateEvent();
        event.antennaName = getName();
        event.eventTime = now;
        event.newState = AntennaState.AntennaInaccessable;
        event.newSubstate = AntennaSubstate.AntennaNoError;
        try {
            publisher.publishEvent(event);
        } catch (AcsJException e) {
            e.log(logger.getInternalLogger());
        }

    }
}
