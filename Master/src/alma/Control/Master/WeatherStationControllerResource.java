/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.Control.Master;

import java.util.logging.Logger;

import alma.Control.WeatherStationController;
import alma.Control.Common.ResourceState;
import alma.Control.Common.StaticResource;
import alma.ControlDeviceExceptions.IllegalConfigurationEx;
import alma.ResourceExceptions.wrappers.AcsJNotYetAcquiredEx;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.acs.container.ContainerServices;

public class WeatherStationControllerResource
    extends StaticResource<WeatherStationController> {

    public static final String RESOURCE_NAME = "CONTROL/WeatherStationController";
    public static final String HELPER_NAME = "alma.Control.WeatherStationControllerHelper";
    
    Configurator configurator;
    
    public WeatherStationControllerResource(Configurator configurator,
                                  ContainerServices container,
                                  Logger logger) {
        super(container,
              RESOURCE_NAME,
              HELPER_NAME,
              false,
              false,
              logger);
        this.configurator = configurator;
    }
    
    @Override
    public void startup() throws AcsJResourceErrorEx {
        setState(ResourceState.STARTING_UP);
        try {
            WeatherStationController wsc = getComponent();
            wsc.createSubdevices(configurator.getWeatherStationConfiguration(), "");
            wsc.controllerOperational();
        } catch (AcsJNotYetAcquiredEx ex) {
            throw new AssertionError("startup() called in AOSTiming before component has been acquired");
        } catch (IllegalConfigurationEx ex) {
            setState(ResourceState.ERROR);
            throw new AcsJResourceErrorEx(ex);
        } catch (Exception ex) {
            setState(ResourceState.ERROR);
            throw new AcsJResourceErrorEx(ex);            
        }
        setState(ResourceState.OPERATIONAL);
    }
    
    @Override
    public void shutdown() throws AcsJResourceErrorEx {
        setState(ResourceState.SHUTTING_DOWN);
        try {
            WeatherStationController wsc = getComponent();
            wsc.controllerShutdown();
            wsc.releaseSubdevices();
        } catch (AcsJNotYetAcquiredEx ex) {
            setState(ResourceState.ERROR);
        } catch (Exception ex) {
            setState(ResourceState.ERROR);
        }
        setState(ResourceState.INITIALIZED);
    }
}
