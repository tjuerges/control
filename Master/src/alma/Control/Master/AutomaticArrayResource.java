/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.Control.Master;

import alma.Control.AutomaticArray2;
import alma.Control.SiteData;
import alma.Control.WeatherStationCharacteristics;
import alma.Control.Common.DynamicResource;
import alma.Control.Common.ResourceManager;
import alma.Control.Common.StaticResource;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.acs.container.ContainerServices;
import java.util.logging.Level;
import alma.acs.logging.AcsLogger;
import alma.log_audience.OPERATOR;
import alma.Control.WeatherStationControllerHelper;
import alma.Control.WeatherStationController;
import alma.Control.CurrentWeatherPackage.Location;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;

/**
 * In general a Resource object takes care of the creation and initialization of
 * external ACS components and services. It is designed to be used with the
 * ResourceManager framework (in CONTROL/Common/Define).
 * 
 * The ArrayResource is a specialized DynamicResource that takes care of the
 * process of initializing AutomaticArrays.
 *  
 * @author rhiriart
 *
 */
public class AutomaticArrayResource extends DynamicResource<AutomaticArray2> {

    /** AutomaticArray IDL type */
    public static final String arrayIFType = "IDL:alma/Control/AutomaticArray2:1.0";

    /** The Master Array wrapper, which uses this class */
    private AutomaticArray array;

    /** The reference to the weather station controller */
    private WeatherStationController wsc = null;

    /** A list of all the weather stations */
    private Location[] wsList = null;

    /** Container Services */
    private ContainerServices container;
    
    /**
     * Constructor.
     * 
     * @param container ACS ContainerServices
     * @param name Array name
     */
    public AutomaticArrayResource(ContainerServices container, AutomaticArray array) {
        super(container, "CONTROL/"+array.getName(), arrayIFType, true);
        this.container=container;
        this.array = array;
    }

    /**
     * Creates the ACS component and initialize it.
     */
    @Override
    public void acquire() throws AcsJResourceErrorEx {
        super.acquire(); // this will create the ACS component
    }

    @Override
    public void startup() throws AcsJResourceErrorEx {        
        alma.Control.AutomaticArray2 arrComp = getComponent();
        
        arrComp.setParentName(array.getParentName());
        arrComp.setAntennaList(array.getAntennaNames());
        
        SiteData siteData = array.getConfigurator().getSiteData();

	// We get the list of weather stations from the weather station controller.
	if (wsc == null)
	{
          // get a reference to the weather station controller  
          // Getting reference of Task and Report Database
          try {          
                  wsc=WeatherStationControllerHelper.narrow(container.getComponent("CONTROL/WeatherStationController"));
          }
          catch (AcsJContainerServicesEx e) {
               throw new AcsJResourceErrorEx(String.format("Cannot get a reference to a the weather station controller %s:", e.getMessage()), e);
          }
	}
        // we fill the WeatherStationCharacteristics from the list of WS returned by WS controller
	WeatherStationCharacteristics[] wsAll=null;

	if (wsList == null)
	    wsList = wsc.weatherStationLocations();

	if (wsList.length == 0) 
	{    
	    String msg = "There are no weather stations available.";      
            container.getLogger().logToAudience(Level.SEVERE, msg, OPERATOR.value);
        } else {
	    wsAll = new WeatherStationCharacteristics[wsList.length];
	    for(int i=0;i<wsList.length;i++)
	    {
	        WeatherStationCharacteristics wsChar=new WeatherStationCharacteristics();
                wsChar.weatherStationName = wsList[i].name;
                wsChar.xPosition = wsList[i].x; 
                wsChar.yPosition = wsList[i].y;
                wsChar.zPosition = wsList[i].z;
                wsAll[i] = wsChar; 
             }
        }


        arrComp.setConfigInfo(array.getConfigurator().getConfigurationName(),
                              array.getAntennaCharacteristics(),
                              wsAll, 
                              siteData,
                              array.getPhotonicReferences(),
                              array.getCorrelatorType());        
        arrComp.setBulkDataDistributors(array.getCorrelatorBulkDataDistributor(),
                                        array.getTotalPowerBulkDataDistributor());
        // CONTROL's lifecycle methods
        arrComp.startupPass1();
        arrComp.startupPass2();
        super.startup();
    }    
    /**
     * Shuts down the ACS component and releases it.
     */
    @Override
    public void release() {
        super.release(); // this will release the ACS component
    }

    @Override
    public void shutdown() {
        // CONTROL's lifecycle methods
        alma.Control.AutomaticArray2 arr = getComponent();
        if (arr != null) {
            arr.shutdownPass1();
            arr.shutdownPass2();
        }        
    }
    
//    /**
//     * Returns the array component state.
//     */
//    @Override
//    public ResourceState status() {
//        if (getComponent() != null) {
//            SystemState state = getComponent().getArrayState();
//            SystemSubstate substate = getComponent().getArraySubstate();
//            ResourceState rs = ResourceState.UNINITIALIZED;
//            if (state == SystemState.INACCESSIBLE)
//                rs = ResourceState.UNINITIALIZED;
//            else if (state == SystemState.OPERATIONAL)
//                rs = ResourceState.OPERATIONAL;
//            return rs;
//        } else
//            return ResourceState.UNINITIALIZED;
//    }    
}

// __oOo__
