#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2010 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#

import sys
import time
import os
from subprocess import Popen, PIPE
from Acspy.Clients.SimpleClient import PySimpleClient

class AntennaRestart:
    
    def __init__(self, antenna):
        self.antenna = antenna
        self.client = PySimpleClient()
        self.master = self.client.getComponent("CONTROL/MASTER")
        self.host = self.antenna.lower() + "-abm"
        self.container = "CONTROL/%s/cppContainer" % self.antenna

    def __del__(self):
        pass

    def killAntennaContainer(self):
        cmd = 'ssh %s sudo -u almaproc /usr/bin/pkill -9 -U almaproc maciContainer' % self.host
        print '%s Killing maciContainer in %s' % (self._get_time(), self.host)
        process = Popen(cmd , stdout=PIPE , stderr=PIPE , shell=True)
        sts = process.wait()
        if sts != 0:
            print self._turn_red("This command was not successful, exit code (%d): %s " % (sts , cmd))
            print self._turn_red(process.stdout.read().strip())
            return
        
    def startAntennaContainer(self):
        cmd = '/alma/ACS-9.0/ACSSW/bin/acsdaemonStartContainer -i 0 -t cpp -c %s -H %s' % (self.container, self.host)
        print '%s Starting maciContainer in %s' % (self._get_time(), self.host)
        process = Popen(cmd , stdout=PIPE , stderr=PIPE , shell=True)
        sts = process.wait()
        if sts != 0:
            print self._turn_red("This command was not successful, exit code (%d): %s " % (sts , cmd))
            print self._turn_red(process.stdout.read().strip())
            return

    def resucitateAntenna(self):
        print "%s Initializing antenna %s" % (self._get_time(), self.antenna)
        self.master.resucitateAntenna(self.antenna)

    def _turn_red(self, msg):
        '''
        Turns the message in red
        '''
        return  "\033[31m%s\033[0m" % msg

    def _get_time(self):
        '''
        Get timestamp for an action
        '''
        return str(time.strftime("%Y-%m-%dT%H:%M:%S", time.gmtime()))

if __name__ == "__main__":
    usage = """usage:
    FullAntennaRestart <Antenna>
"""
    if len(sys.argv) < 2:
        print usage
    antenna = sys.argv[1].upper()
    restart = AntennaRestart(antenna)
    restart.killAntennaContainer()
    time.sleep(10)
    restart.startAntennaContainer()
    time.sleep(60)
    restart.resucitateAntenna()
    
#
# ___oOo___