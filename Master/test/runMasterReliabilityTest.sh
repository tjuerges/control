#!/bin/bash

##############################################################################
# ALMA - Atacama Large Millimiter Array
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration),
# All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston,
# MA 02111-1307  USA
#
# $Id$
#

# Documentation about the test goes here.
#
#

export CLASSPATH=$CLASSPATH:../lib/ControlMasterTest.jar

declare TEST_OPTIONS="-Djava.util.logging.config.file=logging.properties"
declare TEST_CLASS=alma.Control.Master.MasterReliabilityTest
declare TEST_LOG_FILE=tmp/MasterReliabilityTest.log

acsStopContainer simulationContainer 2>&1 > "$TEST_LOG_FILE"
acsStartJava -Dsuite=all "$TEST_OPTIONS" -endorsed junit.textui.TestRunner "$TEST_CLASS" 2>&1 >> "$TEST_LOG_FILE"
acsStartContainer -py simulationContainer 2>&1 >> tmp/container-1.log &

RESULT=$?
if [ "$RESULT" = "0" ]; then
    printf "OK\n"
else
    printf "ERROR\n"
fi

# __oOo__
