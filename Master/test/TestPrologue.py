#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# rhiriart  2006-12-06  created
#

import sys, os, logging
import traceback
import unittest
import thread
import time
from TestCommon import *
                
# Create the temporary directory.
if os.path.exists('./tmp'):
    for f in os.listdir('./tmp'): os.unlink('./tmp/' + f)
    os.rmdir('./tmp')
os.mkdir('./tmp')
        
# Start database (if necessary.) If ORACLE is used instead of HSQLDB, it is asumed
# running and configured.
if dbBackend == HSQLDB:
    logging.info('Starting HSQLDB...')
    os.system('java -cp $INTROOT/lib/hsqldb.jar org.hsqldb.Server -port 8081 &> ./tmp/HSQLStart.log &')
    time.sleep(1)
            
# Configure the database.
# if dbBackend == HSQLDB:
#     os.system("java -jar ../lib/hsqldb.jar --rcFile ./sqltool.rc localhost-sa SQL/DropAllTables.sql")
#     os.system("java -jar ../lib/hsqldb.jar --rcFile ./sqltool.rc localhost-sa SQL/CreateHsqldbTables.sql")
#     os.system("java -jar ../lib/hsqldb.jar --rcFile ./sqltool.rc localhost-sa SQL/hsqldb_test_setup.sql")            

# Start ACS (only if it hasn't been started yet.)
# Using fork/exec combination here to be able to modify the
# environment variables.
inst_file = "%s/tmp/ACS_INSTANCE.%d" % (os.environ['ACSDATA'], ACS_INSTANCE)
if not os.path.exists(inst_file):
    logging.info('Starting ACS...')
    logf = open('./tmp/acsStart.log', 'w') # log file
    acs_pid = os.fork()
    if acs_pid == 0: # child
        os.dup2(logf.fileno(), sys.stdout.fileno()) # connect stdout to log file
        os.dup2(logf.fileno(), sys.stderr.fileno()) # connect stderr to log file
        env = os.environ
        env['ACS_CDB'] = os.getcwd() # define ACS_CDB env. variable
        env['ENABLE_TMCDB'] = 'true'
        env['TMCDB_CONFIGURATION_NAME'] = 'Test'
        env['IFRHACK'] = 'true'
        os.execlpe('acsStart', env)
    else:            # parent
        pid, status = os.wait() # wait for the child process
        pidf = open('./tmp/acs.pid', 'w')
        pidf.write(str(acs_pid))
        pidf.close()
    logf.close()

# Start the containers
logging.info('Starting CONTROL/ACC/javaContainer container')
cont_pid = os.fork()
logf = open('./tmp/CONTROL_ACC_javaContainer.log', 'w')
if cont_pid == 0: # child
    os.dup2(logf.fileno(), sys.stdout.fileno()) # connect stdout to log file
    os.dup2(logf.fileno(), sys.stderr.fileno()) # connect stderr to log file
    os.execlp('acsStartContainer', 'acsStartContainer', '-java', 'CONTROL/ACC/javaContainer')
else:               # parent
    pidf = open('./tmp/CONTROL_ACC_javaContainer.Start.pid', 'w')
    pidf.write(str(cont_pid))
    pidf.close()
    time.sleep(5) # TODO fix this.
                
#
# __oOo__
