/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.Control.Master;

import java.util.logging.Logger;

import junit.framework.Test;
import junit.framework.TestSuite;

import alma.ACSSim.Simulator;
import alma.ACSSim.SimulatorHelper;
import alma.Control.CorrelatorType;
import alma.Control.InvalidRequest;
import alma.Control.ResourceId;
import alma.Control.SystemState;
import alma.Control.SystemSubstate;
import alma.Control.Master2;
import alma.Control.Master2Helper;
import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.container.ContainerServices;

/**
 * Test the Master component.
 * 
 * Containers:
 * <OL>
 * <LI>javaContainer - Java
 * <LI>simulationContainer - Python (unset DISPLAY to avoid the IDL Simulator GUI to
 * pop up.)
 * </OL>
 * 
 * CDB: In the test directory.
 * 
 * @author rhiriart
 *
 */
public class MasterTest extends ComponentClientTestCase {
	
    /** The IDL Simulator Server */
    Simulator simulator;
    
    /** The Master component */
	Master2 master;
    
    /** ACS ContainerServices */
    ContainerServices container;
    
    /** The ubiquitous logger */
    Logger logger;
    
    /**
     * Constructor
     * 
     * @param name Test name.
     * @throws Exception
     */
	public MasterTest(String name) throws Exception {
		super(name);
	}

    /**
     * Test fixture setup. This method is called before each one of the test
     * methods.
     */
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
        container = getContainerServices();
        logger = container.getLogger();
        
		simulator = 
			SimulatorHelper.narrow(container.getDefaultComponent("IDL:alma/ACSSim/Simulator:1.0"));
		String code = "from Control import AntennaStateEvent\n" +
		              "import Control\n" +
		              "state = Control.AntennaOperational\n" +
		              "substate = Control.AntennaNoError\n" +
		              "AntennaStateEvent('CONTROL/DV01', 0L, state, substate)";
        simulator.setMethod("CONTROL/DV01", "getAntennaState", code, 0);
        code = "from Control import AntennaStateEvent\n" +
               "import Control\n" +
               "state = Control.AntennaOperational\n" +
               "substate = Control.AntennaNoError\n" +
               "AntennaStateEvent('CONTROL/DA41', 0L, state, substate)";
        simulator.setMethod("CONTROL/DA41", "getAntennaState", code, 0);
        code = "None";
        simulator.setMethod("CONTROL/DV01", "createSubdevices", code, 0);
        simulator.setMethod("CONTROL/DA41", "createSubdevices", code, 0);
        
        Object obj = container.getComponent("CONTROL/MASTER");
		master = Master2Helper.narrow(obj);
        
	}

    /**
     * Test fixture cleanup. This method is called after each one of the test
     * methods.
     */
	@Override
	protected void tearDown() throws Exception {
        container.releaseComponent(simulator.name());
        container.releaseComponent("CONTROL/MASTER");
		super.tearDown();
	}

    public void testLoadComponent() {}
    
    /**
     * Test Master lifecycle.
     * 
     * @throws Exception
     */
	public void testMasterLifecycle() throws Exception {

        SystemState state = master.getMasterState();
        SystemSubstate substate = master.getMasterSubstate();
        assertEquals(SystemState.INACCESSIBLE, state);
        assertEquals(SystemSubstate.STOPPED, substate);
        
		master.startupPass1();
        master.startupPass2();
        
        state = master.getMasterState();
        substate = master.getMasterSubstate();        
        assertEquals(SystemState.OPERATIONAL, state);
        assertEquals(SystemSubstate.NOERROR, substate);
        
        master.shutdownPass1();
        master.shutdownPass2();
        
        state = master.getMasterState();
        substate = master.getMasterSubstate();
        assertEquals(SystemState.INACCESSIBLE, state);
        assertEquals(SystemSubstate.STOPPED, substate);
        
	}

    /**
     * Test the Master lifecycle a couple of times.
     * 
     * @throws Exception
     */
    public void testTwoMasterLifecycles() throws Exception {

        assertEquals(SystemState.INACCESSIBLE, master.getMasterState());
        assertEquals(SystemSubstate.STOPPED, master.getMasterSubstate());
        
        master.startupPass1();
        master.startupPass2();
        
        assertEquals(SystemState.OPERATIONAL, master.getMasterState());
        assertEquals(SystemSubstate.NOERROR, master.getMasterSubstate());
        
        master.shutdownPass1();
        master.shutdownPass2();
        
        assertEquals(SystemState.INACCESSIBLE, master.getMasterState());
        assertEquals(SystemSubstate.STOPPED, master.getMasterSubstate());

        master.startupPass1();
        master.startupPass2();
        
        assertEquals(SystemState.OPERATIONAL, master.getMasterState());
        assertEquals(SystemSubstate.NOERROR, master.getMasterSubstate());
        
        master.shutdownPass1();
        master.shutdownPass2();
        
        assertEquals(SystemState.INACCESSIBLE, master.getMasterState());
        assertEquals(SystemSubstate.STOPPED, master.getMasterSubstate());

    }    
    
    /**
     * Test Master behavior when one antenna doesn't go to operational after
     * its initialization.
     */
    public void testMasterStartupOneBadAntenna() throws Exception {

        String code = "from Control import AntennaStateEvent\n" +
                      "import Control\n" +
                      "state = Control.AntennaDegraded\n" +
                      "substate = Control.AntennaError\n" +
                      "AntennaStateEvent('CONTROL/DV01', 0L, state, substate)";
        simulator.setMethod("CONTROL/DV01", "getAntennaState", code, 0);

        assertEquals(SystemState.INACCESSIBLE, master.getMasterState());
        assertEquals(SystemSubstate.STOPPED, master.getMasterSubstate());
        
        master.startupPass1();
        master.startupPass2();
        
        String badResources[] = master.getBadResources();
        assertEquals(1, badResources.length);
        assertEquals("CONTROL/DV01", badResources[0]);
        
        assertEquals(SystemState.OPERATIONAL, master.getMasterState());
        assertEquals(SystemSubstate.NOERROR, master.getMasterSubstate());
        
        master.shutdownPass1();
        master.shutdownPass2();
        
        assertEquals(SystemState.INACCESSIBLE, master.getMasterState());
        assertEquals(SystemSubstate.STOPPED, master.getMasterSubstate());
    }
    
    /**
     * Test the creation of an Automatic Array.
     */
    public void testCreateAutomaticArray() throws Exception {
        
        assertEquals(SystemState.INACCESSIBLE, master.getMasterState());
        assertEquals(SystemSubstate.STOPPED, master.getMasterSubstate());
        
        master.startupPass1();
        master.startupPass2();
        
        assertEquals(SystemState.OPERATIONAL, master.getMasterState());
        assertEquals(SystemSubstate.NOERROR, master.getMasterSubstate());

        String[] availableAntennas = master.getAvailableAntennas();
        assertEquals(2, availableAntennas.length);
        
        String arrayName = master.createAutomaticArray(availableAntennas, new String[0],
        		CorrelatorType.BL).arrayComponentName;
        String[] arrays = master.getAutomaticArrays();
        assertEquals(1, arrays.length);
        assertEquals("Array001", arrays[0]);
        
        assertEquals(0, master.getAvailableAntennas().length);
        
        master.destroyArray(arrayName);
        assertEquals(0, master.getAutomaticArrays().length);
        
        master.shutdownPass1();
        master.shutdownPass2();
        
        assertEquals(SystemState.INACCESSIBLE, master.getMasterState());
        assertEquals(SystemSubstate.STOPPED, master.getMasterSubstate());
                
    }

    /**
     * Test createAutomaticArray() passing a zero-length array of antenna
     * names. In this case the AutomaticArray should throw an exception. 
     */
    public void testCreateAutomaticZeroAntennas() throws Exception {
        
        assertEquals(SystemState.INACCESSIBLE, master.getMasterState());
        assertEquals(SystemSubstate.STOPPED, master.getMasterSubstate());
        
        master.startupPass1();
        master.startupPass2();
        
        assertEquals(SystemState.OPERATIONAL, master.getMasterState());
        assertEquals(SystemSubstate.NOERROR, master.getMasterSubstate());

        try {
            String[] antennas = new String[0];
            master.createAutomaticArray(antennas, new String[0], CorrelatorType.BL);
        } catch(InvalidRequest ex) {
            logger.info("Fine, got the InvalidRequest exception.");
            return;
        } finally {
            master.shutdownPass1();
            master.shutdownPass2();
            assertEquals(SystemState.INACCESSIBLE, master.getMasterState());
            assertEquals(SystemSubstate.STOPPED, master.getMasterSubstate());
        }
        fail("InvalidRequest exception was not thrown.");
    }

    /**
     * Test createAutomaticArray() passing wrong antenna names.
     * In this case the AutomaticArray should throw an exception. 
     */
    public void testCreateAutomaticArrayBadAntennas() throws Exception {
        
        assertEquals(SystemState.INACCESSIBLE, master.getMasterState());
        assertEquals(SystemSubstate.STOPPED, master.getMasterSubstate());
        
        master.startupPass1();
        master.startupPass2();
        
        assertEquals(SystemState.OPERATIONAL, master.getMasterState());
        assertEquals(SystemSubstate.NOERROR, master.getMasterSubstate());

        try {
            String[] antennas = {"FOO", "BAR"};
            master.createAutomaticArray(antennas, new String[0], CorrelatorType.BL);
        } catch(InvalidRequest ex) {
            logger.info("Fine, got the InvalidRequest exception.");
            return;
        } finally {
            master.shutdownPass1();
            master.shutdownPass2();
            assertEquals(SystemState.INACCESSIBLE, master.getMasterState());
            assertEquals(SystemSubstate.STOPPED, master.getMasterSubstate());
        }
        fail("InvalidRequest exception was not thrown.");
    }
    
    public void testReinitializeAntenna() throws Exception {
        
        assertEquals(SystemState.INACCESSIBLE, master.getMasterState());
        assertEquals(SystemSubstate.STOPPED, master.getMasterSubstate());
        
        logger.info("Starting Up Master");
        master.startupPass1();
        master.startupPass2();
        
        assertEquals(SystemState.OPERATIONAL, master.getMasterState());
        assertEquals(SystemSubstate.NOERROR, master.getMasterSubstate());

        logger.info("Reinitializing Antenna");
        master.reinitializeAntenna("DV01");
        
        logger.info("Shutting Down Master");
        master.shutdownPass1();
        master.shutdownPass2();
        
        assertEquals(SystemState.INACCESSIBLE, master.getMasterState());
        assertEquals(SystemSubstate.STOPPED, master.getMasterSubstate());                
    }

    public void testQueryPhotonicReferences() throws Exception {
       String code =
           "from Control import DeviceNameMap\n" +
           "pr1 = DeviceNameMap('PhotonicReference1', 'CONTROL/PhotonicReference1')\n" +
           "pr2 = DeviceNameMap('PhotonicReference2', 'CONTROL/PhotonicReference2')\n" +
           "return [pr1, pr2]";       
       simulator.setMethod("CONTROL/CentralLO", "getPhotonicReferences", code, 0);
       simulator.setMethod("CONTROL/PhotonicReference1", "isAssigned", "return False", 0);
       simulator.setMethod("CONTROL/PhotonicReference2", "isAssigned", "return False", 0);
       master.startupPass1();
       master.startupPass2();
       String[] photRefNames = master.getAvailablePhotonicReferences();
       String msg = "Retrieved Photonic Reference Names:\n";
       int count = 1;
       for ( String name : photRefNames ) {
           msg += "Photonic Reference " + count++ + ": " + name + "\n";
       }
       logger.info(msg);
       assertEquals(2, photRefNames.length);
       String[] expectedNames = {"PhotonicReference1", "PhotonicReference2"};
       int yetToBeFound = expectedNames.length;
       for ( String expName : expectedNames ) {
           for ( String name : photRefNames ) {
               if (name.equals(expName)) {
                   yetToBeFound--;
                   break;
               }
           }
       }
       assertEquals(0, yetToBeFound);
       ResourceId[] photRefs = master.getAvailablePhotonicReferenceComponents();
       assertEquals(2, photRefs.length);
       simulator.setMethod("CONTROL/PhotonicReference1", "isAssigned", "return True", 0);
       photRefNames = master.getAvailablePhotonicReferences();
       assertEquals(1, photRefNames.length);
       expectedNames = new String[1];
       expectedNames[0] = "PhotonicReference2";
       yetToBeFound = expectedNames.length;
       for ( String expName : expectedNames ) {
           for ( String name : photRefNames ) {
               if (name.equals(expName)) {
                   yetToBeFound--;
                   break;
               }
           }
       }
       assertEquals(0, yetToBeFound);
       photRefs = master.getAvailablePhotonicReferenceComponents();
       assertEquals(1, photRefs.length);
       master.shutdownPass1();
       master.shutdownPass2();
    }
    public static Test suite() throws Exception {
        
        String s = System.getProperty("suite");
        TestSuite suite = new TestSuite();
        if (s.equals("1")) {
            suite.addTest(new MasterTest("testLoadComponent"));            
        } else if (s.equals("2")) {
            suite.addTest(new MasterTest("testMasterLifecycle"));            
        } else if (s.equals("3")) {
            suite.addTest(new MasterTest("testTwoMasterLifecycles"));            
        } else if (s.equals("4")) {
            suite.addTest(new MasterTest("testMasterStartupOneBadAntenna"));            
        } else if (s.equals("5")) {
            suite.addTest(new MasterTest("testCreateAutomaticArray"));            
        } else if (s.equals("6")) {
            suite.addTest(new MasterTest("testCreateAutomaticZeroAntennas"));            
        } else if (s.equals("7")) {
            suite.addTest(new MasterTest("testCreateAutomaticArrayBadAntennas"));            
        } else if (s.equals("8")) {
            suite.addTest(new MasterTest("testReinitializeAntenna"));
        } else if (s.equals("9")) {
            suite.addTest(new MasterTest("testQueryPhotonicReferences"));
        }
        
        return suite;
    }
    
}

// __oOo__
