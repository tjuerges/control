/*
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2005 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * $Id$
 */

package alma.Control.Master;

import java.util.logging.Logger;

import alma.Control.CorrelatorType;
import alma.Control.SystemState;
import alma.Control.SystemSubstate;
import alma.Control.AutomaticArray;
import alma.Control.AutomaticArray2;
import alma.Control.AutomaticArray2Helper;
import alma.Control.AutomaticArrayHelper;
import alma.Control.Master2;
import alma.Control.Master2Helper;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.TMCDB.TMCDBComponent;
import alma.TMCDB.TMCDBComponentHelper;
import alma.TMCDB_IDL.AntennaIDL;
import alma.TMCDB_IDL.AssemblyLocationIDL;
import alma.TMCDB_IDL.PadIDL;
import alma.TMCDB_IDL.StartupAntennaIDL;
import alma.acs.component.client.ComponentClientTestCase;

/**
 * Test the creation of an AutomaticArray by the Master component.
 * 
 */
public class AutomaticArrayCreationTest extends ComponentClientTestCase {

    private static final String MASTER_CURL = "CONTROL/MASTER";
    private static final String TMCDB_CURL = "IDL:alma/TMCDB/TMCDBComponent:1.0";
    
    private Logger logger;
    private Master2 master;
    private TMCDBComponent tmcdb;
    private AutomaticArray2 array;

    public AutomaticArrayCreationTest() throws Exception {
        super(AutomaticArrayCreationTest.class.getName());
    }

    /**
     * Sets up a test TMCDB configuration.
     *
     */
    private void setupTMCDBConfiguration() {
        
        StartupAntennaIDL[] sai = new StartupAntennaIDL[1];
        sai[0] = new StartupAntennaIDL();
        sai[0].antennaName = "AntSimVA1";
        sai[0].padName = "padName";
        sai[0].frontEndName = "";
        sai[0].frontEndAssembly = new AssemblyLocationIDL[0];
        sai[0].antennaAssembly = new AssemblyLocationIDL[0];
        tmcdb.setStartupAntennasInfo(sai);
        
        AntennaIDL ai = new AntennaIDL();
        ai.AntennaName = "AntSimVA1";
        ai.AntennaType = "";
        ai.DishDiameter = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.CommissionDate = new alma.asdmIDLTypes.IDLArrayTime(0);
        ai.XPosition = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.YPosition = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.ZPosition = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.XOffset = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.YOffset = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.ZOffset = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.ComponentId = 0;
        tmcdb.setAntennaInfo("AntSimVA1", ai);
        
        PadIDL pi = new PadIDL();
        pi.PadName = "padName";
        pi.CommissionDate = new alma.asdmIDLTypes.IDLArrayTime(0);
        pi.XPosition = new alma.asdmIDLTypes.IDLLength(0.0);
        pi.YPosition = new alma.asdmIDLTypes.IDLLength(0.0);
        pi.ZPosition = new alma.asdmIDLTypes.IDLLength(0.0);
        tmcdb.setAntennaPadInfo("AntSimVA1", pi);        
    }    
    
    protected void setUp() throws Exception {

        super.setUp();

        // Get the Logger.
        logger = getContainerServices().getLogger();

        // Get the Master component.
        try {
            master = Master2Helper.narrow(getContainerServices().getComponent(MASTER_CURL));
        } catch (AcsJContainerServicesEx ex) {
            logger.severe("Error accessing '" + MASTER_CURL + "': " + ex.toString());
        }
        assertNotNull(master);

        // Get the TMCDB component.
        try {
            tmcdb = TMCDBComponentHelper.narrow(getContainerServices().getDefaultComponent(TMCDB_CURL));
        } catch (AcsJContainerServicesEx ex) {
            logger.severe("Error accessing '" + TMCDB_CURL + "': " + ex.toString());
            fail();            
        }
        
        setupTMCDBConfiguration();
    }

    protected void tearDown() throws Exception {
        // Release the TMCDB component.
        getContainerServices().releaseComponent(TMCDB_CURL);
        // Release Master component.
        getContainerServices().releaseComponent(MASTER_CURL);

        super.tearDown();
    }

    public void testArrayWithOneAntenna() 
        throws alma.Control.InaccessibleException,
               alma.Control.InvalidRequest {
	
        // Call the Master startup transition methods.
        master.startupPass1();
        master.startupPass2();

        // Ask the Master to create an AutomaticArray component,
        // with all the available antennas.
        String[] antennaNames = null;
        antennaNames = master.getAvailableAntennas();
        String arrayName = null;
        try {
            arrayName = master.createAutomaticArray(antennaNames, new String[0], CorrelatorType.BL)
                              .arrayComponentName;
            logger.info("Array name from Master.createAutomaticArray: '" + arrayName + "'");
        } catch (Exception ex) {
            logger.severe("Error creating Automatic Array: " + ex.toString());
            master.shutdownPass1();
            master.shutdownPass2();
            fail();
        }

        // Verify the Master arrays' structures.
        String[] arrayNames = null;
        arrayNames = master.getAutomaticArrays();
        // There should be only one.
        assertEquals(1, arrayNames.length);
        logger.info("Array name from Master.getAutomaticArrays: '" + arrayNames[0] + "'");

        // Get a reference to the recently created AutomaticArray.
        try {
            array = AutomaticArray2Helper.narrow(getContainerServices().getComponent("CONTROL/" + arrayNames[0]));
        } catch (AcsJContainerServicesEx ex) {
            logger.severe("Cannot access AutomaticArray component: " + ex.toString());
            master.shutdownPass1();
            master.shutdownPass2();
            fail();
        } catch (Exception ex) {
            logger.severe("Error while accessing '" + arrayNames[0] + 
                          "' AutomaticArray component: " + ex.toString());
            master.shutdownPass1();
            master.shutdownPass2();
            fail();
        }

        // Check the state of the AutomaticArray.
        SystemState state = null;
        SystemSubstate substate = null;

        state = master.getMasterState();
        substate = master.getMasterSubstate();
        logger.info("State = " + state.value());
        logger.info("Substate + " + substate.value());

        assertEquals(SystemState.OPERATIONAL, state);
        assertEquals(SystemSubstate.NOERROR, substate);

        // Release the AutomaticArray component.
        getContainerServices().releaseComponent(arrayNames[0]);

        // Destroy the array.
        master.destroyArray("CONTROL/" + arrayNames[0]);
        arrayNames = master.getAutomaticArrays();
        // There should be zero arrays now.
        assertEquals(0, arrayNames.length);
        
        // Shutdown the Master component.
        master.shutdownPass1();
        master.shutdownPass2();

    }

    public static void main(String[] args) {
        junit.textui.TestRunner.run(AutomaticArrayCreationTest.class);
    }

}
