package alma.Control.Master;

import java.io.File;
import java.io.FileOutputStream;
import java.util.logging.Logger;

import alma.ACSSim.Simulator;
import alma.ACSSim.SimulatorHelper;
import alma.Control.ResourceId;
import alma.Control.SystemState;
import alma.Control.SystemSubstate;
import alma.Control.Master2;
import alma.Control.Master2Helper;
import alma.acs.component.client.ComponentClient;
import alma.acs.container.ContainerServices;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class MasterReliabilityTest extends TestCase {

    private static final String MASTER_CURL = "CONTROL/MASTER";
    
    /** Total number of tests */
    static final int NTESTS = suite().countTestCases();
    
    /** Counter for the test number. Necessary to shutdown ACS. */
    static int testCount = NTESTS;
    
    /** Whether ACS was started by the test suite */
    static boolean startedACS = false;
    
    /** ACS Component client */
    static ComponentClient compClient = null;
    
    /** Log file streams */
    static FileOutputStream[] logs;
    
    private ACSTestEnvironment acsenv = null;
    private ContainerServices container = null;
    
    private Logger logger;
    private Master2 master;
    private Simulator simulator;
        
    public MasterReliabilityTest(String name) {
        super(name);
    }

    protected void setUp() throws Exception {
        
        ContainerSpec[] cont = new ContainerSpec[0];
        String[] env = {"ACS_CDB=."};
        acsenv = new ACSTestEnvironment(cont, env);
        
        File tempDir = new File("./tmp");
        if (tempDir.mkdir())
            System.out.println("--- ./tmp directory created");
        else
            System.out.println("--- ./tmp directory already exists");
        
        if (!acsenv.isACSRunning(0)) {
            logs = new FileOutputStream[3];
            logs[0] = new FileOutputStream("./tmp/javaContainer.log");
            logs[1] = new FileOutputStream("./tmp/acs.log");
            logs[2] = new FileOutputStream("./tmp/tomcat.log");
            acsenv.startACS(logs[1]);
            acsenv.startContainers(System.out);
            acsenv.startContainer("javaContainer", "java", logs[0]);
            acsenv.startArchive(logs[2]);
            Thread.sleep(5000); // TODO I should find a way to synch. with the containers
            startedACS = true;
        }
        else {
            System.out.println("--- ACS is already running.");
            if (testCount == NTESTS)
                startedACS = false;
        }
            
        String managerLoc = System.getenv("MANAGER_REFERENCE");
        System.out.println("--- managerLoc = " + managerLoc);
        if (compClient == null)
            compClient = new ComponentClient(null, managerLoc, "MasterReliabilityTest");
        container = compClient.getContainerServices();
        logger = container.getLogger();

        // Get the Master component.
        logger.info("Getting the Master component...");
        master = Master2Helper.narrow(container.getComponent(MASTER_CURL));
        assertNotNull(master);

        
        super.setUp();
    }

    protected void tearDown() throws Exception {
        testCount--;
        
        container.releaseComponent(MASTER_CURL);
        
        if (startedACS && testCount == 0) {
            acsenv.stopArchive(logs[4]);
            acsenv.shutdownContainers(System.out);
            acsenv.shutdownACS(logs[3]);            
            for(int i=0; i<logs.length; i++)
                logs[i].close();
        }
                
        super.tearDown();
    }

    public void testShutdownWithContainerCrash() throws Exception {
        acsenv.startContainer("simulationContainer", "py", System.out);
        Thread.sleep(5000);
        
        // Activate the Simulator component and install some code in the
        // Antenna initialization methods.
        this.simulator = 
            SimulatorHelper.narrow(container.getDefaultComponent("IDL:alma/ACSSim/Simulator:1.0"));
        String code = "from Control import AntennaStateEvent\n" +
                      "import Control\n" +
                      "state = Control.AntennaOperational\n" +
                      "substate = Control.AntennaNoError\n" +
                      "AntennaStateEvent('CONTROL/ALMA01', 0L, state, substate)";
        simulator.setMethod("CONTROL/ALMA01", "getAntennaState", code, 0);
        simulator.setMethod("CONTROL/ALMA02", "getAntennaState", code, 0);
        code = "None";
        simulator.setMethod("CONTROL/ALMA01", "createSubdevices", code, 0);
        simulator.setMethod("CONTROL/ALMA02", "createSubdevices", code, 0);
        
        master.startupPass1();
        master.startupPass2();
        
        acsenv.shutdownContainer("simulationContainer", System.out);

        ResourceId[] rids = master.getAvailableAntennaComponents();
        assertEquals(2, rids.length);
        
        master.shutdownPass1();

        SystemState state = master.getMasterState();
        SystemSubstate subState = master.getMasterSubstate();
//        assertEquals(SystemState._INACCESSIBLE, state.value());
//        assertEquals(SystemSubstate._SHUTTING_DOWN_PASS2, subState.value());
        
        master.shutdownPass2();
        
        state = master.getMasterState();
        subState = master.getMasterSubstate();
        assertEquals(SystemState._INACCESSIBLE, state.value());
        assertEquals(SystemSubstate._STOPPED, subState.value());
        
    }

    public void testReinitializeAntennaWithContainerCrash() throws Exception {
        acsenv.startContainer("simulationContainer", "py", System.out);
        Thread.sleep(5000);
        
        simulator = 
            SimulatorHelper.narrow(container.getDefaultComponent("IDL:alma/ACSSim/Simulator:1.0"));
        String code = "from Control import AntennaStateEvent\n" +
                      "import Control\n" +
                      "state = Control.AntennaOperational\n" +
                      "substate = Control.AntennaNoError\n" +
                      "AntennaStateEvent('CONTROL/ALMA01', 0L, state, substate)";
        simulator.setMethod("CONTROL/ALMA01", "getAntennaState", code, 0);
        simulator.setMethod("CONTROL/ALMA02", "getAntennaState", code, 0);
        code = "None";
        simulator.setMethod("CONTROL/ALMA01", "createSubdevices", code, 0);
        simulator.setMethod("CONTROL/ALMA02", "createSubdevices", code, 0);
        
        master.startupPass1();
        master.startupPass2();
        
        container.releaseComponent(simulator.name());
        acsenv.shutdownContainer("simulationContainer", System.out);
        
        acsenv.startContainer("simulationContainer", "py", System.out);
        Thread.sleep(5000);

        master.reinitializeAntenna("ALMA01");

        acsenv.shutdownContainer("simulationContainer", System.out);        
    }
        
    public static Test suite() {
        TestSuite suite = new TestSuite();
        String testSuite = System.getProperty("suite");
        if (testSuite == null)
            testSuite = "all"; // Default test suite.
        try {
            if (testSuite.equals("1"))
                suite.addTest(new MasterReliabilityTest("testShutdownWithContainerCrash"));
            else if (testSuite.equals("2"))
                suite.addTest(new MasterReliabilityTest("testReinitializeAntennaWithContainerCrash"));
            else if (testSuite.equals("all")) {
                suite.addTest(new MasterReliabilityTest("testShutdownWithContainerCrash"));
                suite.addTest(new MasterReliabilityTest("testReinitializeAntennaWithContainerCrash"));                
            }
        } catch (Exception ex) {
            System.err.println("Error when creating MasterReliabilityTest: "
                    + ex.toString());
        }
        return suite;
    }
    
}
