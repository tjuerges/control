/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.Control.Master;

import java.util.logging.Logger;

import alma.ACSSim.Simulator;
import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.container.ContainerServices;
import alma.ACSSim.SimulatorHelper;
import alma.Control.Common.Resource;
import alma.Control.Common.ResourceManager;
import java.util.Arrays;
import java.util.List;

import junit.framework.Test;
import junit.framework.TestSuite;

public class MasterStartingUpStateTest extends ComponentClientTestCase {

	private class DummyMaster implements MasterInternal {

		private ContainerServices container;
        private Configurator configurator;
        public String state;
		
		public DummyMaster(ContainerServices cont) {
			container = cont;
		}
		
		public ContainerServices getContainerServices() {
			return container;
		}

		public Logger getLogger() {
            return logger;
		}

		public void setState(String state) {this.state = state;}

		public void shutdownPass1() {}

		public void shutdownPass2() {}

		public void startupPass1() {}

		public void startupPass2() {}

		public void waitShutdownPass1() {}

        public void setAntennas(Antenna[] antennas) {}

        public Configurator getConfigurator() {
            return configurator;
        }

        public void setConfigurator(Configurator configurator) {
            this.configurator = configurator;
        }
        
        public void setAntennaManager(AntennaManager am) {}
        
        public AntennaManager getAntennaManager() {
            return null;
        }
	}
	
	private DummyMaster master;
	private MasterStartingUpState state;
    private ResourceManager resMng;
    private Simulator simulator;
    private Logger logger;
	
	public MasterStartingUpStateTest(String name) throws Exception {
		super(name);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		logger = getContainerServices().getLogger();
		simulator = 
			SimulatorHelper.narrow(getContainerServices().getDefaultComponent("IDL:alma/ACSSim/Simulator:1.0"));
		String code = "from Control import AntennaStateEvent\n" +
		              "import Control\n" +
		              "state = Control.AntennaOperational\n" +
		              "substate = Control.AntennaNoError\n" +
		              "AntennaStateEvent('CONTROL/DV01', 0L, state, substate)";
        simulator.setMethod("CONTROL/DV01", "getAntennaState", code, 0);
        simulator.setMethod("CONTROL/DA41", "getAntennaState", code, 0);
        
		master = new DummyMaster(getContainerServices());
		state = new MasterStartingUpState(master);
        
        resMng = ResourceManager.getInstance(getContainerServices(),
                                             logger);
        state.setAntennaTimeout(5000);
	}

	@Override
	protected void tearDown() throws Exception {        
        resMng.releaseResources();
        getContainerServices().releaseComponent(simulator.name());
		super.tearDown();
	}

	public void testStartup() throws Exception {
		state.startupPass1();
        state.startupPass2();
        String[] resources =  resMng.getResourceNames();
        List<String> rl = Arrays.asList(resources);
        String[] exptRes = {"CONTROL/DV01", "CONTROL/DA41"};
        for(int i=0; i<exptRes.length; i++)
            if (!rl.contains(exptRes[i]))
                fail("Resource "+exptRes[i]+" is not in ResourceManager");
        assertEquals(0, resMng.getErroneousResources().length);
        assertEquals(0, resMng.getWorkingResources().length);
        assertEquals("OPERATIONAL_STATE", master.state);
	}
    
    public void testStartupOneBadAntenna() throws Exception {
        String code = "from Control import AntennaStateEvent\n" +
                      "import Control\n" +
                      "state = Control.AntennaDegraded\n" +
                      "substate = Control.AntennaError\n" +
                      "AntennaStateEvent('CONTROL/ALMA01', 0L, state, substate)";
        simulator.setMethod("CONTROL/DV01", "getAntennaState", code, 0);
        state.startupPass1();
        state.startupPass2();
        assertEquals(1, resMng.getErroneousResources().length);
        assertEquals("CONTROL/DV01", resMng.getErroneousResources()[0]);
        assertEquals("OPERATIONAL_STATE", master.state);
    }
    
    public void testStartupAnotherBadAntenna() throws Exception {
        String code = "import ControlDeviceExceptionsImpl\n" +
                      "raise ControlDeviceExceptionsImpl.IllegalConfigurationExImpl()";
        simulator.setMethod("CONTROL/DV01", "createSubdevices", code, 0);
        state.startupPass1();
        state.startupPass2();
        assertEquals(1, resMng.getErroneousResources().length);
        assertEquals("CONTROL/DV01", resMng.getErroneousResources()[0]);
        Resource<?> res = resMng.getResourceEvenIfBad("CONTROL/DV01");
        assertTrue(res.getLastException() instanceof 
                alma.ControlDeviceExceptions.wrappers.AcsJIllegalConfigurationEx);
        assertEquals("OPERATIONAL_STATE", master.state);
    }
    
    /**
     * Unfortunately there is a bug in CorbaPublisher that prevents all tests to run
     * in a row. They need to be run one by one.
     * 
     * @return Test suite.
     */
    public static Test suite() throws Exception {
        String s = System.getProperty("suite");
        if (s == null)
            s = "1";
        TestSuite suite = new TestSuite();
        if (s.equals("1")) {
            suite.addTest(new MasterStartingUpStateTest("testStartup"));            
        } else if (s.equals("2")) {
            suite.addTest(new MasterStartingUpStateTest("testStartupOneBadAntenna"));            
        } else if (s.equals("3")) {
            suite.addTest(new MasterStartingUpStateTest("testStartupAnotherBadAntenna"));            
        }
        return suite;
    }
}
