/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * $Id$
 */
package alma.Control.Master;

import java.util.logging.Logger;

import junit.framework.Test;
import junit.framework.TestSuite;
import alma.ACSErrTypeCommon.wrappers.AcsJBadParameterEx;
import alma.ACSSim.Simulator;
import alma.ACSSim.SimulatorHelper;
import alma.Control.CorrelatorType;
import alma.Control.DeviceConfig;
import alma.Control.SiteData;
import alma.Control.Common.PublisherResource;
import alma.Control.Common.ResourceManager;
import alma.Control.Common.Util;
import alma.ControlExceptions.wrappers.AcsJInvalidRequestEx;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.TMCDBComponentImpl.Antenna;
import alma.TMCDBComponentImpl.Pad;
import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.container.ContainerServices;
import alma.asdmIDLTypes.IDLAngle;
import alma.asdmIDLTypes.IDLLength;
import alma.common.log.ExcLog;

public class AntennaManagerTest extends ComponentClientTestCase {

    private ContainerServices container;
    private Logger logger;
    private ResourceManager resMng;
    private Simulator simulator;

    private class TestConfigurator implements Configurator {

        public Antenna getAntennaConfiguration(String antennaName) {
            
            Antenna antConf = null;
            if (antennaName.equals("ALMA01")) {
                antConf = new Antenna();
                antConf.setAntennaName("ALMA01");
                antConf.setAntennaType("");
                antConf.setCommissionDate(new alma.hla.runtime.asdm.types.ArrayTime(2006,10,1,0,0,0.0));
                antConf.setDishDiameter(new alma.hla.runtime.asdm.types.Length(12.0));

            } else if (antennaName.equals("ALMA02")) {
                antConf = new Antenna();
                antConf.setAntennaName("ALMA02");
                antConf.setAntennaType("");
                antConf.setCommissionDate(new alma.hla.runtime.asdm.types.ArrayTime(2006,10,1,0,0,0.0));
                antConf.setDishDiameter(new alma.hla.runtime.asdm.types.Length(12.0));
            }            
            antConf.setComponentId(0);
            antConf.setXOffset(new alma.hla.runtime.asdm.types.Length(0.0));
            antConf.setYOffset(new alma.hla.runtime.asdm.types.Length(0.0));
            antConf.setZOffset(new alma.hla.runtime.asdm.types.Length(0.0));
            antConf.setXPosition(new alma.hla.runtime.asdm.types.Length(0.0));
            antConf.setYPosition(new alma.hla.runtime.asdm.types.Length(0.0));
            antConf.setZPosition(new alma.hla.runtime.asdm.types.Length(0.0));
            return antConf;
        }

        public DeviceConfig getAntennaDeviceConfiguration(String antennaName) {
            DeviceConfig devConf = null;
            if (antennaName.equals("ALMA01")) {
                devConf = new DeviceConfig();
                devConf.Name = "ALMA01";
                devConf.subdevice = new DeviceConfig[0];
            } else if (antennaName.equals("ALMA02")) {
                devConf = new DeviceConfig();
                devConf.Name = "ALMA02";
                devConf.subdevice = new DeviceConfig[0];                
            }
            return devConf;
        }

        public String[] getAntennas() {
            String[] antennas = {"ALMA01", "ALMA02"};
            return antennas;
        }

        public Pad getPadConfiguration(String antennaName) {
            
            Pad pad = null;
            if (antennaName.equals("ALMA01")) {
                pad = new Pad();
                pad.setPadName("PAD01");
                pad.setCommissionDate(new alma.hla.runtime.asdm.types.ArrayTime(2006,10,1,0,0,0.0));
                pad.setXPosition(new alma.hla.runtime.asdm.types.Length(-1601361.555455));
                pad.setYPosition(new alma.hla.runtime.asdm.types.Length(-5042191.805932));
                pad.setZPosition(new alma.hla.runtime.asdm.types.Length(3554531.803007));
            } else if (antennaName.equals("ALMA02")) {
                pad = new Pad();
                pad.setPadName("PAD02");
                pad.setCommissionDate(new alma.hla.runtime.asdm.types.ArrayTime(2006,10,1,0,0,0.0));
                pad.setXPosition(new alma.hla.runtime.asdm.types.Length(-1601361.555455));
                pad.setYPosition(new alma.hla.runtime.asdm.types.Length(-5042191.805932));
                pad.setZPosition(new alma.hla.runtime.asdm.types.Length(3554531.803007));
            }
            return pad;
        }

        public SiteData getSiteData() {
            
            SiteData siteData = new SiteData ();
            siteData.telescopeName = "test";
            siteData.arrayConfigurationName = "A";
            siteData.minBaseRange = new IDLLength(0);
            siteData.maxBaseRange = new IDLLength(0);;
            siteData.baseRmsMinor = new IDLLength(0);;
            siteData.baseRmsMajor = new IDLLength(0);;
            siteData.basePa = new IDLAngle(0.0);
            siteData.siteLongitude = new IDLAngle(0.0);
            siteData.siteLatitude = new IDLAngle(0.0);
            siteData.siteAltitude = new IDLLength(0);
            siteData.releaseDate = 0L;

            return siteData;
        }

        public int getAntennaTimeout() {
            return 5000;
        }

        public void setAntennaTimeout(int timeout) {}

        public DeviceConfig getAOSTimingDeviceConfiguration() {
            // TODO Auto-generated method stub
            return null;
        }

        public String getConfigurationName() {
            return "Test";
        }

        @Override
        public DeviceConfig getCentralLOConfiguration() {
            DeviceConfig cloConfiguration = new DeviceConfig();
            cloConfiguration.Name = "CONTROL/CentralLO";
            String[] photRefs = {"PhotoReference1", "PhotoReference2"};
            DeviceConfig[] cloSd = new DeviceConfig[photRefs.length];
            for (int i=0; i < photRefs.length; i++) {
                cloSd[i] = new DeviceConfig();
                cloSd[i].Name = photRefs[i];
                cloSd[i].subdevice = new DeviceConfig[0];
            }
            cloConfiguration.subdevice = cloSd;
            return cloConfiguration;
        }

		@Override
		public DeviceConfig getWeatherStationConfiguration() {
			// TODO Auto-generated method stub
			return null;
		}
        
    }
    
	public AntennaManagerTest(String name) throws Exception {
		super(name);        
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();

        container = getContainerServices();
        logger = container.getLogger();
        
		simulator = 
			SimulatorHelper.narrow(container.getDefaultComponent("IDL:alma/ACSSim/Simulator:1.0"));
        String code = "from Control import AntennaStateEvent\n" +
                      "import Control\n" +
                      "state = Control.AntennaOperational\n" +
                      "substate = Control.AntennaNoError\n" +
                      "AntennaStateEvent('CONTROL/ALMA01', 0L, state, substate)";
        simulator.setMethod("CONTROL/ALMA01", "getAntennaState", code, 0);
        simulator.setMethod("CONTROL/ALMA02", "getAntennaState", code, 0);
        code =
            "from Control import DeviceNameMap\n" +
            "pr1 = DeviceNameMap('PhotonicReference1', 'CONTROL/PhotonicReference1')\n" +
            "pr2 = DeviceNameMap('PhotonicReference2', 'CONTROL/PhotonicReference2')\n" +
            "return [pr1, pr2]";       
        simulator.setMethod("CONTROL/CentralLO", "getPhotonicReferences", code, 0);
        simulator.setMethod("CONTROL/PhotonicReference1", "isAssigned", "return False", 0);
        simulator.setMethod("CONTROL/PhotonicReference2", "isAssigned", "return False", 0);
        
        resMng = ResourceManager.getInstance(container, logger);
        PublisherResource pubRes = new PublisherResource(container, "CONTROL_SYSTEM", true,
                logger);
        resMng.addResource(pubRes);
        resMng.addResource(new CentralLOResource(new TestConfigurator(),
                                                     container,
                                                     logger));
        try {
            resMng.acquireResources();
        } catch(AcsJResourceErrorEx ex) {
            logger.severe(Util.getNiceErrorTraceString(ex.getErrorTrace()));
            fail("Error accessing initial resources.");
        }
        
	}

	@Override
	protected void tearDown() throws Exception {
        resMng.releaseResources();
        container.releaseComponent(simulator.name());
		super.tearDown();
	}

	public void testArrayCreation() throws Exception {
		        
        Configurator configurator = new TestConfigurator();
        AntennaManager antMng = new AntennaManager(container, configurator, logger);
        antMng.initializeAntennas();
        String[] an = {"ALMA01", "ALMA02"};
        AutomaticArray array = antMng.createAutomaticArray(an, new String[0], CorrelatorType.BL);
        logger.info("Array '"+array.getName()+" 'has been created successfully.");
        
        AutomaticArray[] arrays = antMng.getAutomaticArrays();
        assertEquals(1, arrays.length);
        
        assertEquals(array, arrays[0]);
        antMng.shutdownAntennas();
	}
    
    public void testTwoArrayCreation() throws Exception {
        
        Configurator configurator = new TestConfigurator();
        AntennaManager antMng = new AntennaManager(container, configurator, logger);
        antMng.initializeAntennas();
        String[] an1 = {"ALMA01"};
        AutomaticArray array1 = antMng.createAutomaticArray(an1, new String[0], CorrelatorType.BL);
        String[] an2 = {"ALMA02"};
        AutomaticArray array2 = antMng.createAutomaticArray(an2, new String[0], CorrelatorType.BL);
        
        AutomaticArray[] arrays = antMng.getAutomaticArrays();
        assertEquals(2, arrays.length);
        antMng.shutdownAntennas();
    }
    
    public void testArrayCreationBadAntenna() throws Exception {

        Configurator configurator = new TestConfigurator();
        AntennaManager antMng = new AntennaManager(container, configurator, logger);
        antMng.initializeAntennas();
        String[] an = {"ALMA01", "FOO"};
        try {
            AutomaticArray array = antMng.createAutomaticArray(an, new String[0], CorrelatorType.BL);
        } catch(AcsJInvalidRequestEx ex) {
            // ex.log(logger);
            logger.severe("Error creating array:\n" + Util.getNiceErrorTraceString(ex.getErrorTrace()));
            antMng.shutdownAntennas();
            return;
        }
        fail("Invalid request was not thrown");
    }
    
    public void testArrayCreationAntennaAlreadyAllocated() throws Exception {
        
        Configurator configurator = new TestConfigurator();
        AntennaManager antMng = new AntennaManager(container, configurator, logger);
        antMng.initializeAntennas();
        String[] an = {"ALMA01"};
        AutomaticArray array1 = antMng.createAutomaticArray(an, new String[0], CorrelatorType.BL);
        
        try {
            AutomaticArray array = antMng.createAutomaticArray(an, new String[0], CorrelatorType.BL);
        } catch(AcsJInvalidRequestEx ex) {
            // ex.log(logger);
            logger.severe("Error creating array:\n" + Util.getNiceErrorTraceString(ex.getErrorTrace()));
            antMng.shutdownAntennas();
            return;
        }                
        fail("Invalid request was not thrown");        
    }
    
    public void testArrayDestruction() throws Exception {
        logger.finest("testArrayDestruction: ENTRY");
        
        Configurator configurator = new TestConfigurator();
        AntennaManager antMng = new AntennaManager(container, configurator, logger);
        antMng.initializeAntennas();
        String[] an = {"ALMA01"};
        logger.info("Creating first AutomaticArray");
        AutomaticArray array = antMng.createAutomaticArray(an, new String[0], CorrelatorType.BL);

        logger.info("Destroying first AutomaticArray");
        antMng.destroyArray(array.getName());
        
        AutomaticArray[] arrays = antMng.getAutomaticArrays();
        for (int i=0; i<arrays.length; i++)
            if (arrays[i].equals(array.getName()))
                fail("Array '"+array.getName()+"' still exists after destroyed");
        
        logger.info("Creating second AutomaticArray");
        array = antMng.createAutomaticArray(an, new String[0], CorrelatorType.BL);
        
        logger.finest("testArrayDestruction: RETURN");
        antMng.shutdownAntennas();
    }
    
    public void testArrayDestructionBadArrayName() throws Exception {
        Configurator configurator = new TestConfigurator();
        AntennaManager antMng = new AntennaManager(container, configurator, logger);
        antMng.initializeAntennas();
        try {
            antMng.destroyArray("FOO");
        } catch (AcsJBadParameterEx ex) {
            String stackTrace = ExcLog.details(ex);
            if (stackTrace.length()>0)
                logger.finest("Got parameter exception. Stacktrace: "+stackTrace);
            logger.severe("Bad parameter exception. ErrorTrace:\n"+
                    Util.getNiceErrorTraceString(ex.getErrorTrace()));
            return;
        }
        fail("Bad parameter exception was not thrown");
        antMng.shutdownAntennas();
    }
    
    public void testManualArrayCreation() throws Exception {
        Configurator configurator = new TestConfigurator();
        AntennaManager antMng = new AntennaManager(container, configurator, logger);
        antMng.initializeAntennas();
        String[] an = {"ALMA01", "ALMA02"};
        ManualArray array = null;
        try {
            array = antMng.createManualArray(an, new String[0], CorrelatorType.BL);
        } catch (AcsJResourceErrorEx ex) {
            String stackTrace = ExcLog.details(ex);
            if (stackTrace.length()>0)
                logger.fine("Error creating ManualArray. StackTrace:"+stackTrace);
            fail("Error creating ManualArray");
        }
        logger.info("Array '"+array.getName()+" 'has been created successfully.");
        
        ManualArray[] arrays = antMng.getManualArrays();
        assertEquals(1, arrays.length);
        
        assertEquals(array, arrays[0]);        
        antMng.shutdownAntennas();
    }
    
    public void testGetAvailableAntennas() throws Exception {
        Configurator configurator = new TestConfigurator();
        AntennaManager antMng = new AntennaManager(container, configurator, logger);
        antMng.initializeAntennas();
        alma.Control.Master.Antenna[] antennas = antMng.getAvailableAntennas();
        assertEquals(2, antennas.length);
        logger.info("Antenna 1: " + antennas[0].getAntennaName());
        logger.info("Antenna 2: " + antennas[1].getAntennaName());        
        antMng.shutdownAntennas();
    }
    
    /**
     * Unfortunately there is a bug in CorbaPublisher that prevents all tests to run
     * in a row. They need to be run one by one.
     * 
     * @return Test suite.
     */
    public static Test suite() throws Exception {
        String s = System.getProperty("suite");
        TestSuite suite = new TestSuite();
        if (s.equals("1")) {
            suite.addTest(new AntennaManagerTest("testArrayCreation"));            
        } else if (s.equals("2")) {
            suite.addTest(new AntennaManagerTest("testTwoArrayCreation"));            
        } else if (s.equals("3")) {
            suite.addTest(new AntennaManagerTest("testArrayCreationBadAntenna"));            
        } else if (s.equals("4")) {
            suite.addTest(new AntennaManagerTest("testArrayCreationAntennaAlreadyAllocated"));            
        } else if (s.equals("5")) {
            suite.addTest(new AntennaManagerTest("testArrayDestruction"));            
        } else if (s.equals("6")) {
            suite.addTest(new AntennaManagerTest("testArrayDestructionBadArrayName"));            
        } else if (s.equals("7")) {
            suite.addTest(new AntennaManagerTest("testManualArrayCreation"));            
        } else if (s.equals("8")) {
            suite.addTest(new AntennaManagerTest("testGetAvailableAntennas"));
        }
        return suite;
    }
}
