/**
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2005 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * $Id$
 */

package alma.Control.Master;

import java.util.logging.Logger;

import alma.Control.Master2;
import alma.Control.Master2Helper;
import alma.Control.DeviceConfig;
import alma.TMCDB.TMCDBComponent;
import alma.TMCDB.TMCDBComponentHelper;
import alma.TMCDB_IDL.AntennaIDL;
import alma.TMCDB_IDL.AssemblyLocationIDL;
import alma.TMCDB_IDL.PadIDL;
import alma.TMCDB_IDL.StartupAntennaIDL;
import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.nc.Receiver;
import alma.acs.nc.AbstractNotificationChannel;
import alma.Control.AntennaStateEvent;
import alma.Control.AntennaState;
import alma.Control.AntennaSubstate;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;

/**
 * Test the antenna state reporting facilities for the CONTROL subsystem.
 * 
 * The CONTROL/MASTER component accesses the antennas and initialize them during the 
 * statupPass1() lifecycle function. Before getting the antennas to operational, it sends an
 * AntennaStateEvent with the state as AntennaInaccessible. When the CONTROL/MASTER component 
 * shuts down the system resources during the shutdownPass2() lifecycle function, it shuts 
 * down the antennas and sends another AntennaStateEvent with AntennaInaccessible state.
 * 
 * The Antenna device themselves, on the other hand, send an AntennaStateEvent every time their 
 * state changes.
 * 
 * Also, the function getAvailableAntennaStates() allows to query for the current state of the 
 * available antennas, i.e., the antennas that haven't been allocated to an array. This function, 
 * together with the Array getAntennaStates() permits to query for the states of the full set of 
 * antennas.
 * 
 * This test case has two test case functions. The first one initializes the CONTROL/MASTER 
 * component and waits for the AntennaStateEvent to come from the notification channel. It then 
 * shuts down the CONTROL/MASTER component and waits for the second AntennaStateEvent.
 * 
 * The second test function initializes the CONTROL/MASTER component and query the antenna 
 * states through the getAvailableAntennaStates() function.
 *  
 * @author rhiriart
 *
 */
public class AntennaStatusReportingTest extends ComponentClientTestCase {

    private static final String MASTER_CURL = "CONTROL/MASTER";
    private static final String CONTROL_EXT_NC = "CONTROL_SYSTEM";
    private static final String TMCDB_CURL = "IDL:alma/TMCDB/TMCDBComponent:1.0";
    
    private Logger logger;
    private Master2 master;
    private TMCDBComponent tmcdb;
    private AntennaStateEvent antennaEvent;
    private boolean antInaccEventReceived;
    
    public AntennaStatusReportingTest() throws Exception {
        super(AntennaStatusReportingTest.class.getName());
    }

    /**
     * Sets up a test TMCDB configuration.
     *
     */
    private void setupTMCDBConfiguration() {
        
        StartupAntennaIDL[] sai = new StartupAntennaIDL[1];
        sai[0] = new StartupAntennaIDL();
        sai[0].antennaName = "AntSimVA1";
        sai[0].padName = "padName";
        sai[0].frontEndName = "";
        sai[0].frontEndAssembly = new AssemblyLocationIDL[0];
        sai[0].antennaAssembly = new AssemblyLocationIDL[0];
        tmcdb.setStartupAntennasInfo(sai);
        
        AntennaIDL ai = new AntennaIDL();
        ai.AntennaName = "AntSimVA1";
        ai.AntennaType = "";
        ai.DishDiameter = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.CommissionDate = new alma.asdmIDLTypes.IDLArrayTime(0);
        ai.XPosition = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.YPosition = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.ZPosition = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.XOffset = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.YOffset = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.ZOffset = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.ComponentId = 0;
        tmcdb.setAntennaInfo("AntSimVA1", ai);
        
        PadIDL pi = new PadIDL();
        pi.PadName = "padName";
        pi.CommissionDate = new alma.asdmIDLTypes.IDLArrayTime(0);
        pi.XPosition = new alma.asdmIDLTypes.IDLLength(0.0);
        pi.YPosition = new alma.asdmIDLTypes.IDLLength(0.0);
        pi.ZPosition = new alma.asdmIDLTypes.IDLLength(0.0);
        tmcdb.setAntennaPadInfo("AntSimVA1", pi);        
    }        
    
    /**
     * Test case fixture setup.
     * 
     * A reference to the CONTROL/MASTER component is acquired.
     * 
     */
    protected void setUp() throws Exception {
        super.setUp();

        // Get the Logger.
        logger = getContainerServices().getLogger();

        // Get the Master component.
        try {
            master = Master2Helper.narrow(getContainerServices().getComponent(MASTER_CURL));
        } catch (AcsJContainerServicesEx ex) {
            logger.severe("Error accessing '" + MASTER_CURL + "': " + ex.toString());
        }
        assertNotNull(master);

        // Get the TMCDB component.
        try {
            tmcdb = TMCDBComponentHelper.narrow(getContainerServices().getDefaultComponent(TMCDB_CURL));
        } catch (AcsJContainerServicesEx ex) {
            logger.severe("Error accessing '" + TMCDB_CURL + "': " + ex.toString());
            fail();            
        }
        
        setupTMCDBConfiguration();        
        
        antennaEvent = null;
        antInaccEventReceived = false;
    }

    /**
     * Test case fixture clean up.
     */
    protected void tearDown() throws Exception {
        getContainerServices().releaseComponent(MASTER_CURL);
        super.tearDown();
    }

    /**
     * Receiver callback function for the AntennaStateEvent.
     * 
     * @param antEvent The antenna state event.
     */
    public void receive(AntennaStateEvent antEvent) {
        logger.info("Received AntennaStateEvent");
        
        // Check that the AntennaStateEvent is really the one we're waiting for,
        // i.e., the one with AntennaInaccessible state inside.
        String[] antennaStates = {"AntennaInaccessable",
                                  "AntennaOperational",
                                  "AntennaShutdown",
                                  "AntennaDegraded"};
        String[] antennaSubStates = {"AntennaError",
                                     "AntennaNoError"};
        logger.info("Event state: " + antennaStates[antEvent.newState.value()]);
        logger.info("Event substate: " + antennaSubStates[antEvent.newSubstate.value()]);
        
        if (antEvent.newState == AntennaState.AntennaInaccessable) {
            antennaEvent = antEvent;
            antInaccEventReceived = true;
        }
    }
    
    /**
     * Test that the CONTROL/MASTER component is sending AntennaStateEvent's just before
     * the antennas are initialized and just afterward the antennas are shutdown.
     * 
     * @throws Exception
     */
    public void testAntennaEvents() throws Exception {
        
        // Create a consumer for the AntennaStateEvent.
        Receiver receiver;
        logger.info("Creating NC Receiver and subscribing to events...");
        receiver = AbstractNotificationChannel.getReceiver(AbstractNotificationChannel.CORBA,
                                                           CONTROL_EXT_NC, getContainerServices());
        receiver.attach("alma.Control.AntennaStateEvent", this);
        receiver.begin();

        // Initialize the Master component.
        master.startupPass1();
        master.startupPass2();        
        
        // Wait with a timeout for the arrival of the first (AntennaInaccessible) 
        // AntennaStateEvent.
        int timeout = 30; // timeout in seconds
        int sleepInterval = 500; // sleep interval in milliseconds
        int timeoutCount = (int) 1000.0 * timeout / sleepInterval; // timeout in cycle counts
        int count = 0;
        while (count < timeoutCount && !antInaccEventReceived) {
            try {
                Thread.sleep(sleepInterval);
            } catch (InterruptedException ie) {
                logger.severe("Interrupt exception while sleeping: " + ie.toString());
            }
            count++;
        }
        if (!antInaccEventReceived) {
            logger.severe("Timeout when waiting for AntennaStateEvent");
            fail("AntennaInaccessible AntennaStateEvent has not been received after antenna creation");
        }

        antennaEvent = null;
        antInaccEventReceived = false;
        
        // Shutdown the CONTROL subsystem
        master.shutdownPass1();
        master.shutdownPass2();

        // Wait with a timeout for the arrival of the second AntennaInaccessible 
        // AntennaStateEvent.
        timeout = 30; // timeout in seconds
        sleepInterval = 500; // sleep interval in milliseconds
        timeoutCount = (int) 1000.0 * timeout / sleepInterval; // timeout in cycle counts
        count = 0;
        while (count < timeoutCount && !antInaccEventReceived) {
            try {
                Thread.sleep(sleepInterval);
            } catch (InterruptedException ie) {
                logger.severe("Interrupt exception while sleeping: " + ie.toString());
            }
            count++;
        }
        if (!antInaccEventReceived) {
            logger.severe("Timeout when waiting for AntennaStateEvent");
            fail("AntennaInaccessible AntennaStateEvent has not been received after antenna destruction");
        }
        
        receiver.end();        
    }
    
    /**
     * Test the CONTROL/MASTER getAvailableAntennaStates() function.
     * 
     * @throws Exception
     */
    public void testGetAvailableAntennaStates() throws Exception {
        
        // Initialize the Master component.
        master.startupPass1();
        master.startupPass2();        

        // Wait a couple of seconds for the antennas to actually be initialized
        // TODO: This will be replaced by a polling function in the Master.
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ie) {
            logger.severe("Interrupt exception while sleeping: " + ie.toString());
        }        
        
        AntennaStateEvent[] states = master.getAvailableAntennaStates();
        
        assertEquals(1, states.length);
        assertEquals("CONTROL/AntSimVA1", states[0].antennaName);
        assertEquals(AntennaState.AntennaOperational, states[0].newState);
        assertEquals(AntennaSubstate.AntennaNoError, states[0].newSubstate);
        
        // Shutdown the CONTROL subsystem
        master.shutdownPass1();
        master.shutdownPass2();        
    }
}
