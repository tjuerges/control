/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */

package alma.Control.Master;

import java.util.logging.Logger;

import alma.ACSSim.Simulator;
import alma.ACSSim.SimulatorHelper;
import alma.Control.DeviceConfig;
import alma.TMCDB.TMCDBComponent;
import alma.TMCDB.TMCDBComponentHelper;
import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.container.ContainerServices;

public class TMCDBConfiguratorTest extends ComponentClientTestCase {

    private ContainerServices container;
    private Logger logger;
    private TMCDBComponent tmcdbIF;
    private Simulator simulator;
    
    public TMCDBConfiguratorTest(String name) throws Exception {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
        container = getContainerServices();
        logger = container.getLogger();
        simulator = 
            SimulatorHelper.narrow(container.getDefaultComponent("IDL:alma/ACSSim/Simulator:1.0"));
        tmcdbIF = TMCDBComponentHelper.narrow(container.getComponent("SIM_TMCDB"));
    }

    protected void tearDown() throws Exception {
        container.releaseComponent("TMCDB");
        super.tearDown();
    }

    public void testDeviceConfigs() throws Exception {

        String code = "import TMCDB_IDL\n" +
                      "wcaloc = TMCDB_IDL.AssemblyLocationIDL(assemblyTypeName='WCAATF'," +
                      "                                       assemblyRoleName='WCAATF'," +
                      "                                       rca=0," +
                      "                                       channelNumber=10," +
                      "                                       baseAddress=3)\n" +
                      "dgloc = TMCDB_IDL.AssemblyLocationIDL(assemblyTypeName='DGCK'," +
                      "                                      assemblyRoleName='DGCK'," +
                      "                                      rca=0," +
                      "                                      channelNumber=15," +
                      "                                      baseAddress=8)\n" +
                      "feloc = TMCDB_IDL.AssemblyLocationIDL(assemblyTypeName='FrontEnd'," +
                      "                                      assemblyRoleName='FrontEnd'," +
                      "                                      rca=0," +
                      "                                      channelNumber=0," +
                      "                                      baseAddress=0)\n" +
                      "sa = TMCDB_IDL.StartupAntennaIDL(antennaName='DV01'," +
                      "                                 padName='Pad01'," +
                      "                                 frontEndName='FrontEnd'," +
                      "                                 uiDisplayOrder=1," +
                      "                                 frontEndAssembly=[wcaloc]," +
                      "                                 antennaAssembly=[feloc, dgloc])\n" +
                      "[sa]";
        simulator.setMethod("SIM_TMCDB", "getStartupAntennasInfo", code, 0.0);
        
        TMCDBConfigurator conf = new TMCDBConfigurator(tmcdbIF, logger);
        
        String[] antennas = conf.getAntennas();
        assertEquals(1, antennas.length);
        
        DeviceConfig devConf = conf.getAntennaDeviceConfiguration(antennas[0]);
        assertEquals("CONTROL/DV01", devConf.Name);
        
        assertEquals(2, devConf.subdevice.length);
        assertEquals("FrontEnd", devConf.subdevice[0].Name);
        assertEquals("DGCK", devConf.subdevice[1].Name);
        
        assertEquals(1, devConf.subdevice[0].subdevice.length);
        assertEquals("WCAATF", devConf.subdevice[0].subdevice[0].Name);
    }

    public void testAOSTimingDeviceConfigs() {
        String code = "import TMCDB_IDL\n" +
                      "cvr = TMCDB_IDL.AssemblyLocationIDL(assemblyTypeName='CVR'," +
                      "                                       assemblyRoleName='CVR'," +
                      "                                       rca=0," +
                      "                                       channelNumber=10," +
                      "                                       baseAddress=3)\n" +
                      "gps = TMCDB_IDL.AssemblyLocationIDL(assemblyTypeName='GPS'," +
                      "                                      assemblyRoleName='GPS'," +
                      "                                      rca=0," +
                      "                                      channelNumber=15," +
                      "                                      baseAddress=8)\n" +
                      "opll = TMCDB_IDL.AssemblyLocationIDL(assemblyTypeName='OPLL'," +
                      "                                      assemblyRoleName='OPLL'," +
                      "                                      rca=0," +
                      "                                      channelNumber=0," +
                      "                                      baseAddress=0)\n" +
                      "sa = TMCDB_IDL.StartupAOSTimingIDL([cvr, gps, opll])\n" +
                      "sa";
        simulator.setMethod("SIM_TMCDB", "getStartupAOSTimingInfo", code, 0.0);
        
        TMCDBConfigurator conf = new TMCDBConfigurator(tmcdbIF, logger);
        DeviceConfig devConfs = conf.getAOSTimingDeviceConfiguration();
        assertEquals("CONTROL/AOSTiming", devConfs.Name);
        assertEquals(3, devConfs.subdevice.length);
        
    }
    
//    public void testATFConfiguration() throws Exception {
//        String tmcdbATFName = "TMCDB_ATF";
//        TMCDBComponent tmcdb = TMCDBComponentHelper.narrow(container.getComponent(tmcdbATFName));
//        TMCDBConfigurator conf = new TMCDBConfigurator(tmcdb, logger);
//        logger.info("Configuration: " + conf.toString());
//        container.releaseComponent(tmcdbATFName);
//    }
}
