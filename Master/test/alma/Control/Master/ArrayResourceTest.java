package alma.Control.Master;

import java.util.logging.Logger;

import alma.ACSSim.Simulator;
import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.container.ContainerServices;
import alma.asdmIDLTypes.IDLAngle;
import alma.asdmIDLTypes.IDLLength;
import alma.ACSSim.SimulatorHelper;
import alma.Control.CorrelatorType;
import alma.Control.DeviceConfig;
import alma.Control.ResourceId;
import alma.Control.SiteData;
import alma.Control.Common.ResourceManager;

public class ArrayResourceTest extends ComponentClientTestCase {

    private ContainerServices container;
    private Logger logger;
    private ResourceManager resMng;

    private class TestConfigurator implements Configurator {

        public alma.TMCDBComponentImpl.Antenna getAntennaConfiguration(String antennaName) {
            // TODO Auto-generated method stub
            return null;
        }

        public DeviceConfig getAntennaDeviceConfiguration(String antennaName) {
            // TODO Auto-generated method stub
            return null;
        }

        public String[] getAntennas() {
            // TODO Auto-generated method stub
            return null;
        }

        public alma.TMCDBComponentImpl.Pad getPadConfiguration(String antennaName) {
            // TODO Auto-generated method stub
            return null;
        }

        public SiteData getSiteData() {
            
            SiteData siteData = new SiteData ();
            siteData.telescopeName = "test";
            siteData.arrayConfigurationName = "A";
            siteData.minBaseRange = new IDLLength(0);
            siteData.maxBaseRange = new IDLLength(0);;
            siteData.baseRmsMinor = new IDLLength(0);;
            siteData.baseRmsMajor = new IDLLength(0);;
            siteData.basePa = new IDLAngle(0.0);
            siteData.siteLongitude = new IDLAngle(0.0);
            siteData.siteLatitude = new IDLAngle(0.0);
            siteData.siteAltitude = new IDLLength(0);
            siteData.releaseDate = 0L;

            return siteData;
        }

        public int getAntennaTimeout() {
            return 5000;
        }

        public void setAntennaTimeout(int timeout) {
        }

        public DeviceConfig getAOSTimingDeviceConfiguration() {
            // TODO Auto-generated method stub
            return null;
        }

        public String getConfigurationName() {
            return "Test";
        }

        @Override
        public DeviceConfig getCentralLOConfiguration() {
            // TODO Auto-generated method stub
            return null;
        }

		@Override
		public DeviceConfig getWeatherStationConfiguration() {
			// TODO Auto-generated method stub
			return null;
		}
        
    }
    
	public ArrayResourceTest(String name) throws Exception {
		super(name);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();

        container = getContainerServices();
        logger = container.getLogger();
        
		Simulator simulator = 
			SimulatorHelper.narrow(getContainerServices().getDefaultComponent("IDL:alma/ACSSim/Simulator:1.0"));
                
        resMng = ResourceManager.getInstance(getContainerServices());
        
	}

	@Override
	protected void tearDown() throws Exception {
        resMng.releaseResources();
		super.tearDown();
	}

	public void testArrayCreation() throws Exception {
		
        Configurator conf = new TestConfigurator();
        AutomaticArray array =
            new AutomaticArray(container,
                               "TestArray",
                               "",
                               null,
                               "distr1",
                               "distr2",
                               new ResourceId[0],
                               conf,
                               CorrelatorType.BL);
        AutomaticArrayResource arrayResource = new AutomaticArrayResource(container, array);
        arrayResource.acquire();
        arrayResource.release();
	}
}
