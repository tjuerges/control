/**
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2005 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * $Id$
 */

package alma.Control.Master;

import java.util.logging.Logger;

import alma.Control.SystemState;
import alma.Control.SystemSubstate;
import alma.Control.Master2;
import alma.Control.Master2Helper;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.TMCDB.TMCDBComponent;
import alma.TMCDB.TMCDBComponentHelper;
import alma.TMCDB_IDL.AntennaIDL;
import alma.TMCDB_IDL.AssemblyLocationIDL;
import alma.TMCDB_IDL.PadIDL;
import alma.TMCDB_IDL.StartupAntennaIDL;
import alma.acs.component.client.ComponentClientTestCase;

/**
 * TestMasterLifecycle tests that the Master is capable to walk through
 * its normal lifecycle states without problems.
 *
 * Components needed: Master (Java container)
 */
public class MasterLifecycleTest extends ComponentClientTestCase {

    private static final String MASTER_CURL = "CONTROL/MASTER";
    private static final String TMCDB_CURL = "IDL:alma/TMCDB/TMCDBComponent:1.0";

    private Logger logger;
    private Master2 master;
    private TMCDBComponent tmcdb;

    public MasterLifecycleTest() throws Exception {
        super(MasterLifecycleTest.class.getName());
    }

    /**
     * Sets up a test TMCDB configuration.
     *
     */
    private void setupTMCDBConfiguration() {
        
        StartupAntennaIDL[] sai = new StartupAntennaIDL[1];
        sai[0] = new StartupAntennaIDL();
        sai[0].antennaName = "AntSimVA1";
        sai[0].padName = "padName";
        sai[0].frontEndName = "";
        sai[0].frontEndAssembly = new AssemblyLocationIDL[0];
        sai[0].antennaAssembly = new AssemblyLocationIDL[0];
        tmcdb.setStartupAntennasInfo(sai);
        
        AntennaIDL ai = new AntennaIDL();
        ai.AntennaName = "AntSimVA1";
        ai.AntennaType = "";
        ai.DishDiameter = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.CommissionDate = new alma.asdmIDLTypes.IDLArrayTime(0);
        ai.XPosition = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.YPosition = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.ZPosition = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.XOffset = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.YOffset = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.ZOffset = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.ComponentId = 0;
        tmcdb.setAntennaInfo("AntSimVA1", ai);
        
        PadIDL pi = new PadIDL();
        pi.PadName = "padName";
        pi.CommissionDate = new alma.asdmIDLTypes.IDLArrayTime(0);
        pi.XPosition = new alma.asdmIDLTypes.IDLLength(0.0);
        pi.YPosition = new alma.asdmIDLTypes.IDLLength(0.0);
        pi.ZPosition = new alma.asdmIDLTypes.IDLLength(0.0);
        tmcdb.setAntennaPadInfo("AntSimVA1", pi);        
    }
    
    protected void setUp() throws Exception {

        super.setUp();

        // Get the Logger.
        logger = getContainerServices().getLogger();

        // Get the Master component.
        try {
            master = Master2Helper.narrow(getContainerServices().getComponent(MASTER_CURL));
        } catch(AcsJContainerServicesEx ex) {
            logger.severe("Error accessing '" + MASTER_CURL + "': " + ex.toString());
            fail();
        }        
        assertNotNull(master);

        try {
            tmcdb = TMCDBComponentHelper.narrow(getContainerServices().getDefaultComponent(TMCDB_CURL));
        } catch (AcsJContainerServicesEx ex) {
            logger.severe("Error accessing '" + TMCDB_CURL + "': " + ex.toString());
            fail();            
        }
        
        setupTMCDBConfiguration();
    }

    protected void tearDown() throws Exception {
        
        getContainerServices().releaseComponent(TMCDB_CURL);
        getContainerServices().releaseComponent(MASTER_CURL);

        super.tearDown();
    }

    /**
     * Test CONTROL/MASTER normal lifecycle.
     * 
     * The initial state of the CONTROL/MASTER is (INACCESSIBLE, STOPPED).
     * Its normal lifecycle is executed by calling the following functions:
     * <OL>
     * <LI> startupPass1() - state goes to (INACCESSIBLE, STARTED_UP_PASS1)
     * <LI> startupPass2() - state goes to (OPERATIONAL, NOERROR)
     * <LI> shutdownPass1() - state goes to (INACCESSIBLE, SHUT_DOWN_PASS1)
     * <LI> shutdownPass2() - state goes to (INACCESSIBLE, STOPPED)
     * </OL>
     * 
     */
    public void testNormalLifecycle() {

        SystemState state;
        SystemSubstate substate;

        state = master.getMasterState();
        substate = master.getMasterSubstate();
        logger.info("beginning");
        logger.info("State = " + state.value());
        logger.info("Substate + " + substate.value());

        assertEquals(SystemState.INACCESSIBLE, state);
        assertEquals(SystemSubstate.STOPPED, substate);

        master.startupPass1();
        state = master.getMasterState();
        substate = master.getMasterSubstate();

        logger.info("startupPass1");
        logger.info("State = " + state.value());
        logger.info("Substate + " + substate.value());

        assertEquals(SystemState.INACCESSIBLE, state);
        assertEquals(SystemSubstate.STARTED_UP_PASS1, substate);

        master.startupPass2();
        state = master.getMasterState();
        substate = master.getMasterSubstate();

        logger.info("statupPass2");
        logger.info("State = " + state.value());
        logger.info("Substate + " + substate.value());

        assertEquals(SystemState.OPERATIONAL, state);
        assertEquals(SystemSubstate.NOERROR, substate);

        try {
            Thread.sleep(2000L);
        } catch(InterruptedException ex) {
            logger.severe("Error while sleeping: " + ex.toString());
        }

        master.shutdownPass1();
        state = master.getMasterState();
        substate = master.getMasterSubstate();

        logger.info("shutdownPass1");
        logger.info("State = " + state.value());
        logger.info("Substate + " + substate.value());

        assertEquals(SystemState.INACCESSIBLE, state);
        assertEquals(SystemSubstate.SHUT_DOWN_PASS1, substate);

        master.shutdownPass2();
        state = master.getMasterState();
        substate = master.getMasterSubstate();

        logger.info("shutdownPass2");
        logger.info("State = " + state.value());
        logger.info("Substate + " + substate.value());

        assertEquals(SystemState.INACCESSIBLE, state);
        assertEquals(SystemSubstate.STOPPED, substate);
    }

    public static void main(String[] args) {
        junit.textui.TestRunner.run(MasterLifecycleTest.class);
    }

}
