/*
ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*/
package alma.Control.gui.monitordb;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import java.util.ArrayList;
import java.util.TreeMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.logging.Logger;

import alma.hla.runtime.asdm.types.ArrayTime;
import alma.hla.runtime.asdm.types.Interval;

import alma.TMCDB.Query.TMCDBCommonQueries;
import alma.TmcdbErrType.wrappers.AcsJTmcdbErrTypeEx;
import alma.acs.util.UTCUtility;
import alma.TMCDB.TimeValue;

/**
 * Facade for dealing with the backend (e.g. CONTROL, MonitorDB via JDBC, 
 * or what not); this isolates the places in the GUI which interact with the 
 * backend to a single spot, so that we don't have the GUI code overly 
 * coupled to the backend.
 * 
 * @author Jeff Kern
 * @author Steve Harrington
 * @author Nicolas Barriga
 */
public class MonitorDatabasePresentationModel {
	private static final String NO_DATA = "No data";
	/* I use Tree maps because I want sorted order */
	private TreeMap<String, AntennaLink> antennaMap  = new TreeMap<String, AntennaLink>();
	private TreeMap<String, DeviceLink>  deviceMap   = new TreeMap<String, DeviceLink>();
	private TreeMap<String, PropertyLink>  propertyMap = new TreeMap<String, PropertyLink>();

	private List<String> antennaFilter = new ArrayList<String>();
	private List<String> deviceFilter  = new ArrayList<String>();
	private List<String> propertyFilter = new ArrayList<String>();
	private Logger logger;
	
	private TMCDBCommonQueries TMCDBQuery;
    
	/**
	 * Enum used to denote success or failure of db operations.
	 */
	public enum QueryResult {
		/**
		 * Operation completed successfully.
		 */
		SUCCESS, 
		/**
		 * Operation completed with error.
		 */
		FAILURE;
	}
	
	private enum PropertyType
	{
		/**
		 * Binary large object (blob) property type.
		 */
		BLOB,
		/**
		 * Boolean (true/false) property type.
		 */
	    BOOLEAN,
	    /**
	     * Double property type.
	     */
	    DOUBLE, 
	    /**
	     * Enumerated property type.
	     */
	    ENUM,
	    /**
	     * Float property type.
	     */
	    FLOAT,
	    /**
	     * Integer property type.
	     */
	    INTEGER,
	    /**
	     * Character string property type.
	     */
	    STRING;
		
		private static final String STRING_TYPE = "STRING";
		private static final String INTEGER_TYPE = "INTEGER";
		private static final String FLOAT_TYPE = "FLOAT";
		private static final String ENUM_TYPE = "ENUM";
		private static final String DOUBLE_TYPE = "DOUBLE";
		private static final String BOOLEAN_TYPE = "BOOLEAN";
		private static final String BLOB_TYPE = "BLOB";

		/**
		 * converts a string to an enum value
		 * @param propertyString the string representation
		 * @return the enum value corresponding to the passed-in string.
		 */
		public static PropertyType stringToPropertyType(String propertyString)
		{
			PropertyType retVal = PropertyType.BLOB;
			if(propertyString.equalsIgnoreCase(BLOB_TYPE)) {
				retVal = BLOB;
			}
			else if(propertyString.equalsIgnoreCase(BOOLEAN_TYPE)) {
				retVal = BOOLEAN;
			}
			else if(propertyString.equalsIgnoreCase(DOUBLE_TYPE)) {
				retVal = DOUBLE;
			}
			else if(propertyString.equalsIgnoreCase(ENUM_TYPE)) {
				retVal = ENUM;
			}
			else if(propertyString.equalsIgnoreCase(FLOAT_TYPE)) {
				retVal = FLOAT;
			}
			else if(propertyString.equalsIgnoreCase(INTEGER_TYPE)) {
				retVal = INTEGER;
			}
			else if(propertyString.equalsIgnoreCase(STRING_TYPE)) {
				retVal = STRING;
			}
			return retVal;
		}
	}
	
	
	private boolean simulation = false;
	private class AntennaLink {
		private HashSet<String> deviceLink;
		private HashSet<String> propertyLink;

		/**
		 * Constructor.
		 */
		public AntennaLink(){
			deviceLink = new HashSet<String>();
			propertyLink = new HashSet<String>();
		}
	}

	private class DeviceLink {
		private HashSet<String> antennaLink;
		private HashSet<String> propertyLink;

		/**
		 * Constructor.
		 */
		public DeviceLink(){
			antennaLink = new HashSet<String>();
			propertyLink = new HashSet<String>();
		}
	}

	private class PropertyLink {
		private HashSet<String> antennaLink;
		private HashSet<String> deviceLink;
  	    private PropertyType          propertyType;

		/**
		 * Constructor.
		 * @param propertyType the type of the property.
		 */
		public PropertyLink(PropertyType propertyType){
		    this.propertyType = propertyType;
			antennaLink = new HashSet<String>();
			deviceLink = new HashSet<String>();
		}

	        protected PropertyType type() {
		    return this.propertyType;
		} 
	}


	/**
	 * Constructor.
	 * @param logger logger to use for logging messages.
	 */
	public MonitorDatabasePresentationModel(Logger logger) 
	{
		this.logger = logger;
		/* For Debugging only */
		if(simulation)
		{
			addComponent("CONTROL/DV01/FLOOG");
			addComponent("CONTROL/DA41/FLOOG");
			addComponent("CONTROL/DV02/FLOOG");
			addComponent("CONTROL/DV01/LO2BBPr0");
			addComponent("CONTROL/DA41/LO2BBPr0");
		}
		else {
			// initialize TMCDBCommonQueries
			TMCDBQuery = new TMCDBCommonQueries();
			TMCDBQuery.connectToDB();
			String configName = TMCDBQuery.readConfigName();
			try{
				TMCDBQuery.initialize(configName);//need to call TMCDBQuery.terminate() somewhere(in the "destructor"...)
			}catch(AcsJTmcdbErrTypeEx ex){
				//exception handling in here
				//popup error window? no clue how to do that
			}
			// add antennas
			String[] antennaes = TMCDBQuery.getAntennaNames().toArray(new String[0]);
			for(int i=0;i<antennaes.length;i++){
				antennaMap.put(antennaes[i], new AntennaLink());
			}
			// add assemblies
			for(int i=0;i<antennaes.length;i++){
				String[] assemblies = TMCDBQuery.getAssemblyNames(antennaes[i]).toArray(new String[0]);
				for(int j=0;j<assemblies.length;j++){
					String deviceName  = assemblies[j];
                    if(deviceName.equals("Mount")){
                        deviceName = TMCDBQuery.getAssemblyName(TMCDBQuery.getAssemblyIDforSN("CONTROL/"+antennaes[i]+"/Mount"));
                    }
					addDevice(deviceName);
                    
					// Add the Antenna to the Device map 
					deviceMap.get(deviceName).antennaLink.add(antennaes[i]);

					// Add the Device to the Antenna Map 
					antennaMap.get(antennaes[i]).deviceLink.add(deviceName);						    
					// Add all properties associated with the device to antenna map
					// and the Antenna to the property map

					Iterator<String> iter = deviceMap.get(deviceName).propertyLink.iterator();
					while (iter.hasNext()){
						String prop = iter.next();
						propertyMap.get(prop).antennaLink.add(antennaes[i]);
						antennaMap.get(antennaes[i]).propertyLink.add(prop);
					}

				}
			}
		}
	}

	/* This method adds a new component i.e. CONTROL/DA41/LORR to
	   the list.
	 */
	private void addComponent(String componentName) {
		String[] splitList = componentName.split("/");
		String antennaName = splitList[1];
		String deviceName  = splitList[2];

		if (!antennaMap.containsKey(antennaName)) {
			antennaMap.put(antennaName, new AntennaLink());
		}

		if (!deviceMap.containsKey(deviceName)) {
			addDevice(deviceName);
		}

		/* Add the Antenna to the Device map */
		deviceMap.get(deviceName).antennaLink.add(antennaName);

		/* Add the Device to the Antenna Map */
		antennaMap.get(antennaName).deviceLink.add(deviceName);

		/* Add all properties associated with the device to antenna map 
		 * and the Antenna to the property map
		 */	    
		Iterator<String> iter = 
			deviceMap.get(deviceName).propertyLink.iterator();
		while (iter.hasNext()){
			String prop = iter.next();
			propertyMap.get(prop).antennaLink.add(antennaName);
			antennaMap.get(antennaName).propertyLink.add(prop);
		}
	}
	     
        private void addDevice(String exampleDevice) {
	    //String[] splitList = exampleDevice.split("/");
	    //String deviceName  = splitList[2];
	    String deviceName  = exampleDevice;
	    
	    if(deviceMap.get(deviceName)==null){
		    deviceMap.put(deviceName, new DeviceLink());
	    }

	    String[] properties = getDeviceProperties(exampleDevice);

	    for (String property : properties) {
		if (!propertyMap.containsKey(property)) {
		    PropertyType type = getPropertyType(property);
		    propertyMap.put(property, new PropertyLink(type));
		}

		/* Add the property to the device Map */
		deviceMap.get(deviceName).propertyLink.add(property);
		propertyMap.get(property).deviceLink.add(deviceName);
	    }
	}

	private String[] getDeviceProperties(String exampleDevice){
		List<String> propertyList = new ArrayList<String>();
		//String[] splitList = exampleDevice.split("/");
		//String deviceName  = splitList[2];
		String deviceName  = exampleDevice;
		if(simulation){
			if (deviceName.equals("FLOOG")) {
				propertyList.add("Temperature");
				propertyList.add("FTS_Freq");
				propertyList.add("MissedCommand");
			}

			if (deviceName.equals("LO2BBPr0")) {
				propertyList.add("Temperature");
				propertyList.add("FTS_Freq");
				propertyList.add("MissedCommand");
				propertyList.add("CoarseFreq");
			}
		}else{
			List<String> propertiesList = TMCDBQuery.getPropertyNames(deviceName);
			ListIterator<String> propertiesIt = propertiesList.listIterator();
			try{
				while(true){
					propertyList.add(propertiesIt.next());
				}
			}catch(NoSuchElementException ex){}
		}
		return (String[]) propertyList.toArray(new String[0]);
        }

	private PropertyType getPropertyType(String propertyName) {
		if(simulation){
			if (propertyName.equals("Temperature")) {return PropertyType.FLOAT;}
			if (propertyName.equals("FTS_Freq")) {return PropertyType.FLOAT;}
			if (propertyName.equals("MissedCommand")) {return PropertyType.BOOLEAN;}
			if (propertyName.equals("CoarseFreq")) {return PropertyType.FLOAT;}
			return PropertyType.FLOAT;
		} else{
			return PropertyType.stringToPropertyType(TMCDBQuery.getPropertyDataType(propertyName));
		}
	}
	/**
	 * Adds an antenna filter
	 * @param antennaName the name of the antenna to add.
	 */
	public void addAntennaFilter(String antennaName){
		antennaFilter.add(antennaName);
	}

	/**
	 * Resets the antenna filter
	 */
	public void resetAntennaFilter() {
		antennaFilter.clear();
	}

	/**
	 * Getter for antenna filter; TODO: explain...
	 * @return an array of strings containing the names of the antennas in the filter.
	 */
	public String[] getAntennaFilter() {
		return (String[]) antennaFilter.toArray(new String[0]);
	}

	/**
	 * Adds a device filter.
	 * @param deviceName the name of the device to add to the filters.
	 */
	public void addDeviceFilter(String deviceName){
		deviceFilter.add(deviceName);
	}

	/**
	 * Resets (clears) the device filters.
	 */
	public void resetDeviceFilter() {
		deviceFilter.clear();
	}

	/**
	 * Getter for the device filter.
	 * @return an array of strings containing the devices in the filter.
	 */
	public String[] getDeviceFilter() {
		return (String[]) deviceFilter.toArray(new String[0]);
	}

	/**
	 * Adds a property filter.
	 * @param propertyName the name of the property to add to the filter.
	 */
	public void addPropertyFilter(String propertyName){
		propertyFilter.add(propertyName);
	}

	/**
	 * Resets (clears) the property filter.
	 */
	public void resetPropertyFilter() {
		propertyFilter.clear();
	}

	/**
	 * Getter for the property filter.
	 * @return an array of strings containing the properties in the filter.
	 */
	public String[] getPropertyFilter() {
		return (String[]) propertyFilter.toArray(new String[0]);
	}

	/**
	 * Getter for the filtered antennas.
	 * @return an array of strings containing the filtered antennas.
	 */
	public String[] getFilteredAntennas() {
		List<String> filterResult = new ArrayList<String>();
		Iterator<String> antIter = antennaMap.keySet().iterator();

		if (deviceFilter.isEmpty() && propertyFilter.isEmpty()){
			/* No Filtering Done on Antenna */
			while (antIter.hasNext()) {
				filterResult.add(antIter.next());
			}
			return (String[]) filterResult.toArray(new String[0]);
		}

		while (antIter.hasNext()) {
			String antennaName = antIter.next();
			boolean valid = true;

			Iterator<String> filterIter;

			filterIter = deviceFilter.iterator();
			while (filterIter.hasNext()) {
				if (!antennaMap.get(antennaName).deviceLink.
						contains(filterIter.next())) {
					valid = false;
					break;
				}
			}

			/* Don't bother filtering on properties if we already know
               it's not good
			 */
			if (!valid) { continue; }

			/* Now filter on the properties */
			filterIter = propertyFilter.iterator();
			while (filterIter.hasNext()) {
				if (!antennaMap.get(antennaName).propertyLink.
						contains(filterIter.next())) {
					valid = false;
					break;
				}
			}


			if (valid) {
				filterResult.add(antennaName);
			}
		}
		return (String[]) filterResult.toArray(new String[0]);
	}

	/**
	 * Getter for the filtered devices.
	 * @return an array of strings containing the filtered devices.
	 */
	public String[] getFilteredDevices() {
		List<String> filterResult = new ArrayList<String>();
		Iterator<String> deviceIter = deviceMap.keySet().iterator();

		if (antennaFilter.isEmpty() && propertyFilter.isEmpty()){
			/* No Filtering Done on Antenna */
			while (deviceIter.hasNext()) {
				filterResult.add(deviceIter.next());
			}
			return (String[]) filterResult.toArray(new String[0]);
		}

		while (deviceIter.hasNext()) {
			String deviceName = deviceIter.next();
			boolean valid = true;

			Iterator<String> filterIter;

			filterIter = antennaFilter.iterator();
			while (filterIter.hasNext()) {
				if (!deviceMap.get(deviceName).antennaLink.
						contains(filterIter.next())) {
					valid = false;
					break;
				}
			}

			/* Don't bother filtering on properties if we already know
               it's not good
			 */
			if (!valid) { continue; }

			/* Now filter on the properties */
			filterIter = propertyFilter.iterator();
			while (filterIter.hasNext()) {
				if (!deviceMap.get(deviceName).propertyLink.
						contains(filterIter.next())) {
					valid = false;
					break;
				}
			}


			if (valid) {
				filterResult.add(deviceName);
			}
		}
		return (String[]) filterResult.toArray(new String[0]);
	}


	/**
	 * Getter for the filtered properties.
	 * @return an array of strings containing the filtered properties.
	 */
	public String[] getFilteredProperties() {
		List<String> filterResult = new ArrayList<String>();
		Iterator<String> propIter = propertyMap.keySet().iterator();

		if (antennaFilter.isEmpty() && deviceFilter.isEmpty()){
			/* No Filtering Done on Antenna */
			while (propIter.hasNext()) {
				filterResult.add(propIter.next());
			}
			return (String[]) filterResult.toArray(new String[0]);
		}

		while (propIter.hasNext()) {
			String propName = propIter.next();
			boolean valid = true;

			Iterator<String> filterIter;

			filterIter = antennaFilter.iterator();
			while (filterIter.hasNext()) {
				if (!propertyMap.get(propName).antennaLink.
						contains(filterIter.next())) {
					valid = false;
					break;
				}
			}

			/* Don't bother filtering on properties if we already know
               it's not good
			 */
			if (!valid) { continue; }

			/* Now filter on the device */
			filterIter = deviceFilter.iterator();
			while (filterIter.hasNext()) {
				if (!propertyMap.get(propName).deviceLink.
						contains(filterIter.next())) {
					valid = false;
					break;
				}
			}

			if (valid) {
				filterResult.add(propName);
			}
		}
		return (String[]) filterResult.toArray(new String[0]);
	}

   /**
    * Getter for the query items.
    * @return an array of QueryItem objects representing the items to query.
    */
	public QueryItem[] getQueryItems() {
		List<QueryItem> resultList = new ArrayList<QueryItem>();

		/* The Logic here is if the user has selected something use
	   that selection only, if there are no selections then use
	   all filtered antennas */
		String[] filteredAntennas;
		String[] filteredDevices;
		String[] filteredProperties;

		if (antennaFilter.isEmpty()) {
			filteredAntennas = getFilteredAntennas();
		} else {
			filteredAntennas = (String[]) antennaFilter.toArray(new String[0]);
		}

		if (deviceFilter.isEmpty()) {
			filteredDevices = getFilteredDevices();
		} else {
			filteredDevices = (String[]) deviceFilter.toArray(new String[0]);
		}

		if (propertyFilter.isEmpty()) {
			filteredProperties = getFilteredProperties();
		} else {
			filteredProperties = 
				(String[]) propertyFilter.toArray(new String[0]);
		}

		for (String antenna : filteredAntennas) {
			for (String device : filteredDevices) {
				if (antennaMap.get(antenna).deviceLink.contains(device)) {
					for (String property : filteredProperties) {
						if (deviceMap.get(device).propertyLink.
								contains(property)) {
							
							switch(propertyMap.get(property).type()) {
							case BLOB:
								resultList.add(new QueryBlobItem(antenna,
										device,
										property));
								break;
							case BOOLEAN:
								resultList.add(new QueryBooleanItem(antenna,
										device,
										property));
								break;
							case DOUBLE:
								resultList.add(new QueryDoubleItem(antenna,
										device,
										property));
								break;
							case ENUM:
								resultList.add(new QueryEnumItem(antenna,
										device,
										property));
								break;
							case FLOAT:
								resultList.add(new QueryFloatItem(antenna,
										device, 
										property));
								break;
							case INTEGER:
								resultList.add(new QueryIntegerItem(antenna,
										device,
										property));
								break;
							case STRING:
								resultList.add(new QueryStringItem(antenna,
										device,
										property));
								break;
							}
						}
					}
				}
			}
		}
	
	return (QueryItem[]) resultList.toArray(new QueryItem[0]);
    }

    /**
	 * TODO - execute the query, saving results to the file 
	 * that was specified by the user; for now, just printing
	 * a message indicating the specified output file. 
	 * 
	 * @param fileInWhichToSaveResults - the file where the results will be written.
	 * @param itemsToQuery the properties that we wish to query.
	 * @param startDate the start date for the query.
	 * @param endDate the end date for the query.
	 * @param timeSpan the span of time for averaging (seconds).
	 * @return result as an enum (failure or success)
	 */
	public QueryResult getValuesWithStatisticsAndSaveResults(File fileInWhichToSaveResults,
					       QueryItem[] itemsToQuery,
					       Date startDate, Date endDate,
					       int timeSpan) 
    {
		QueryResult retVal = QueryResult.FAILURE;
		
		logger.fine("Actually Going to perform data query:");
		logger.fine("From: " + startDate);
		logger.fine("To:   " + endDate);
		logger.fine("By:   " + timeSpan);

		List<ArrayTime> endTimes = new ArrayList<ArrayTime>();
		List<ArrayTime> centroids = new ArrayList<ArrayTime>();

		Interval  avgInt    = new Interval(timeSpan, "sec");

		ArrayTime startTime = new ArrayTime(UTCUtility.utcJavaToOmg(startDate.getTime())*100L - 8712576000000000000L);
		ArrayTime endTime = new ArrayTime(UTCUtility.utcJavaToOmg(endDate.getTime())*100L - 8712576000000000000L);

		/* Round the Start Time off to the averageInterval */
		startTime.set(startTime.get() - (startTime.get() % avgInt.get()), "nanosec"); 

		ArrayTime nextEndTime= ArrayTime.add(startTime,avgInt);	
		while (nextEndTime.compareTo(endTime) <= 0) {
			centroids.add(ArrayTime.sub(nextEndTime,Interval.div(avgInt,2)));
			endTimes.add(nextEndTime);
			nextEndTime = ArrayTime.add(nextEndTime, avgInt);	
		}

		List<List<Stats>> stats = new ArrayList<List<Stats>>(itemsToQuery.length);//ArrayList of "# of items" ArrayLists with the stats for each timespan

		for (QueryItem queryItem : itemsToQuery) {
			logger.fine("Property " + queryItem.fullyQualifiedPropertyName());
			if(simulation) {
				ArrayList<Stats> stat=new ArrayList<Stats>();
				for(int i=0;i<centroids.size();i++){
					Stats<Double> mStat=new Stats<Double>();
					mStat.mean=10.0;
					stat.add(mStat);
				}
				stats.add(stat); 
			} else { //if(!simulation)
				stats.add(calculateStats(TMCDBQuery.getMonitorData(queryItem.getAntennaName(), queryItem.getDeviceName(), queryItem.getPropertyName(), startTime, endTime), endTimes, queryItem));
			}
		}


		retVal = saveStats(fileInWhichToSaveResults, centroids, stats, itemsToQuery);

		//	for (int idx = 0; idx < endTimes.size(); idx++) {
		//	    logger.fine(centroids.get(idx)+"\t\t" + endTimes.get(idx));
		//	}

		logger.fine("TODO - perform actual DB query and save results to file: " 
				+ fileInWhichToSaveResults.getAbsolutePath());

		//for testing only...
		for(QueryItem queryItem : itemsToQuery)
		{
			logger.fine("Query item: " + queryItem.toString());
		}
		logger.fine("Start date was: " + startDate + "; End date was: " + endDate + "; timespan was: " + timeSpan);
		return retVal;
	}
	
	/**
	* creates a file with the statistics asked by the user.
	* sample file:
	* TIME                            FTS_Freq(mean)
	* A                               R
	* 2007-12-05T22:03:25.000000000   10.0
	* @param fileInWhichToSaveResults file In Which To Save Results
	* @param centroids List of ArrayTime holding the midpoint of the timeslots on which statistics were calculated
	* @param stats List of "# of items" List with the stats for each timeslot
	* @param itemsToQuery the properties that were queried.
	* @return result as an enum (failure or success)
	*/
	private QueryResult saveStats(File fileInWhichToSaveResults, List<ArrayTime> centroids, List<List<Stats>> stats, QueryItem[] itemsToQuery)
	{
		QueryResult retVal = QueryResult.FAILURE;
		try {
			FileWriter outFile = new FileWriter(fileInWhichToSaveResults);
			if(itemsToQuery == null || itemsToQuery.length == 0) 
			{ 
				outFile.append("No items to query!");
				return retVal;
			}

			//First line: Column names
			outFile.append("TIME\t\t\t\t");
			for(int i = 0; i < itemsToQuery.length; i++) {
				if(itemsToQuery[i].shouldCalculateMean()) {
					outFile.append(itemsToQuery[i].getPropertyName()+"(mean)\t");
				}if(itemsToQuery[i].shouldCalculateMedian()) {
					outFile.append(itemsToQuery[i].getPropertyName()+"(median)\t");
				}if(itemsToQuery[i].shouldCalculateMode()) {
					outFile.append(itemsToQuery[i].getPropertyName()+"(mode)\t");
				}if(itemsToQuery[i].shouldCalculateStandardDeviation()) {
					outFile.append(itemsToQuery[i].getPropertyName()+"(stdDev)\t");
				}if(itemsToQuery[i].shouldCalculateN()) {
					outFile.append(itemsToQuery[i].getPropertyName()+"(N)\t");
				}if(itemsToQuery[i].shouldCalculateMax()) {
					outFile.append(itemsToQuery[i].getPropertyName()+"(max)\t");
				}if(itemsToQuery[i].shouldCalculateMin()) {
					outFile.append(itemsToQuery[i].getPropertyName()+"(min)\t");
				}

			}
			outFile.append("\n");

			//second line: column types
			outFile.append("A\t\t\t\t");
			for(int i = 0; i < itemsToQuery.length; i++) {
				if(itemsToQuery[i].shouldCalculateMean()) {
					outFile.append(itemsToQuery[i].meanType()+"\t");
				}if(itemsToQuery[i].shouldCalculateMedian()) {
					outFile.append(itemsToQuery[i].medianType()+"\t");
				}if(itemsToQuery[i].shouldCalculateMode()) {
					outFile.append(itemsToQuery[i].modeType()+"\t");
				}if(itemsToQuery[i].shouldCalculateStandardDeviation()) {
					outFile.append(itemsToQuery[i].stdDevType()+"\t");
				}if(itemsToQuery[i].shouldCalculateN()) {
					outFile.append(itemsToQuery[i].nType()+"\t");
				}if(itemsToQuery[i].shouldCalculateMax()) {
					outFile.append(itemsToQuery[i].maxType()+"\t");
				}if(itemsToQuery[i].shouldCalculateMin()) {
					outFile.append(itemsToQuery[i].minType()+"\t");
				}
			}
			outFile.append("\n");

			//third column and next: actual data
			//System.out.println("Centroids: "+centroids.size()+", stats: "+stats.size());
			for(int i = 0; i < centroids.size(); i++)
			{
				outFile.append(centroids.get(i).toFITS() + "\t");
				for(int j = 0; j < stats.size(); j++) 
				{
					//System.out.println("MP: "+i+", item: "+j);
					List<Stats> currStat = stats.get(j);
					if(currStat != null && currStat.size() > 0 && i < currStat.size() && null != currStat.get(i))
					{
						//System.out.println("stats[j].size(): "+stats.get(j).size());
						//outFile.append(stats.get(j).get(i).mean+"\t");
						if(itemsToQuery[j].shouldCalculateMean()) 
						{
							outFile.append(currStat.get(i).mean + "\t");
						} 
						if(itemsToQuery[j].shouldCalculateMedian()) 
						{
							outFile.append(currStat.get(i).median + "\t");
						} 
						if(itemsToQuery[j].shouldCalculateMode()) 
						{
							outFile.append(currStat.get(i).mode + "\t");
						} 
						if(itemsToQuery[j].shouldCalculateStandardDeviation()) 
						{
							outFile.append(currStat.get(i).stdDev + "\t");
						} 
						if(itemsToQuery[j].shouldCalculateN()) 
						{
							outFile.append(currStat.get(i).N + "\t");
						} 
						if(itemsToQuery[j].shouldCalculateMax()) 
						{
							outFile.append(currStat.get(i).max + "\t");
						} 
						if(itemsToQuery[j].shouldCalculateMin()) 
						{
							outFile.append(currStat.get(i).min + "\t");
						}
					}
					else 
					{
						outFile.append("No_data\t");
					}
				}
				outFile.append("\n");
			}

			outFile.close();
			retVal = QueryResult.SUCCESS;
		} catch(IOException ex) {
			retVal = QueryResult.FAILURE;
			logger.fine("Error accesing file:" + ex.toString());
		} catch(Throwable ex) {
			retVal = QueryResult.FAILURE;
			logger.fine("Caught an unexpected throwable in saveStats: " + ex.toString());
			ex.printStackTrace();
		}
		return retVal;
	}

	/**
	* Support method to calculate some statistics. The stats are calculated in segments defined by the endTimes passed as parameters.
	* @param rawData the data to calculate statiscis from
	* @param endTimes the ending times of the periods to calculate the statistics
	* @param queryItem has info on which statistics are needed
	* @return a list of statistics
	*/
	private List<Stats> calculateStats(TimeValue[] rawData, List<ArrayTime> endTimes, QueryItem queryItem)
	{
		int numData = rawData.length;
		List<Stats> retVal = new ArrayList<Stats>();

		// check for no data to calculate statistics for; if so, just return
		if(null == rawData || rawData.length == 0) 
		{
			logger.warning("No raw data when attempting to calculate statistics for: " + queryItem.fullyQualifiedPropertyName());
			retVal.add(null);
			return retVal;
		}

		List<Object> values = new ArrayList<Object>();

		int j = 0;
		while(endTimes.get(0).compareTo(rawData[j].getTime()) > 0) 
		{
			values.add(rawData[j].getValue());
			j++;
		}
		if(null != values && values.size() > 0) 
		{
			retVal.add(queryItem.calculateStatistics(values));
		}
		else 
		{
			retVal.add(null);
		}

		for(int i = 1; i < endTimes.size(); i++)
		{
			values.clear();
			while(j < numData && (endTimes.get(i-1).compareTo(rawData[j].getTime()) <= 0) && (endTimes.get(i).compareTo(rawData[j].getTime()) > 0)) 
			{
				values.add(rawData[j].getValue());
				j++;
			}
			if(null != values && values.size() > 0) 
			{
				retVal.add(queryItem.calculateStatistics(values));
			}
			else 
			{
				retVal.add(null);
			}
		}

		return retVal;
	}

	/**
	 * Gets the current values for the desired properties.
	 * 
	 * @param itemsToQuery the properties for which we want to get the current values.
	 * @return a 2-d array of strings; each row in the array
 	 * contains 3 strings: (property name, time, property value). So, for each item
	 * queried, there will be row with 3 columns containing strings for property name,
	 * time, and property value.

	 */
	public String[][] getCurrentValues(QueryItem[] itemsToQuery) 
	{
		// allocate an array with enough rows for all the items to query
		// and enough columns (5) for the antenna name, device name, property name, time, and value.
		String[][] retArray = new String[itemsToQuery.length][5];
		TimeValue[] rawData = new TimeValue[itemsToQuery.length];
		
		for(int i=0;i<itemsToQuery.length;i++){
		    if(!simulation) {
		    	rawData[i] = TMCDBQuery.getLastMonitorData(itemsToQuery[i].getAntennaName(), itemsToQuery[i].getDeviceName(), itemsToQuery[i].getPropertyName());
		    	if(null == rawData[i]) {
		    		// if no data exists, then populate the value with a string indicating that
		    		rawData[i] = new TimeValue(new ArrayTime(), NO_DATA);
		    	}
		    } else{
				rawData[i] = new TimeValue(new ArrayTime(),"dummy");
		    }
		}
		
		// for each item queried, add a row to the 2-d array containing
		// the name, time, and value for the queried property.
		for(int i=0;i<itemsToQuery.length;i++) 
		{
			// add the property name for the row
		    retArray[i][0] = itemsToQuery[i].getAntennaName();
		    retArray[i][1] = itemsToQuery[i].getDeviceName(); 
		    retArray[i][2] = itemsToQuery[i].getPropertyName();
		   
		   	// add the time for the row
		    if(!rawData[i].getValue().toString().equals(NO_DATA)) {
		    	retArray[i][3] = rawData[i].getTime().toFITS();
		    } else {
	    		// if no data exists, then populate the time with a string indicating that
		    	retArray[i][3] = NO_DATA;
		    }
		    
		   	// add the value for the row
		   	retArray[i][4] = rawData[i].getValue().toString();
		}
		
		// return the 2-d array, containing lists inside for each item's property/time/value
		return retArray;
	}
	
	/**
	 * Gets the raw values for the desired properties.
	 * TODO: start and end times should be received also, similar to performQueryAndSaveResults.
	 * (and eliminate the declaring of the Date variables.)
	 * 
	 * @param fileInWhichToSaveResults the full path to the file in which the results are to be saved.
	 * @param itemToQuery the property for which we want to get the raw value.
	 * @param startDate the start date for the query.
	 * @param endDate the end date for the query.
	 * @return result as an enum (success or failure)
	 */
	public QueryResult getRawValuesAndSaveResults(File fileInWhichToSaveResults, QueryItem itemToQuery, 
			Date startDate, Date endDate) 
	{
		QueryResult retVal = QueryResult.FAILURE;
		ArrayTime startTime = new ArrayTime(UTCUtility.utcJavaToOmg(startDate.getTime())*100L - 8712576000000000000L);
		ArrayTime endTime = new ArrayTime(UTCUtility.utcJavaToOmg(endDate.getTime())*100L - 8712576000000000000L);
		TimeValue[] rawValues = null;
		logger.fine("Property " + itemToQuery.fullyQualifiedPropertyName());
		if(!simulation){
			rawValues = TMCDBQuery.getMonitorData(itemToQuery.getAntennaName(), itemToQuery.getDeviceName(), itemToQuery.getPropertyName(), startTime, endTime);

		} else{
			rawValues = new TimeValue[3];
			for(int i=0;i<3;i++){
				rawValues[i]=new TimeValue(startTime,2.0);
			}
		}

		try {
			FileWriter outFile = new FileWriter(fileInWhichToSaveResults);

			//First line: Column names
			outFile.append("TIME\t\t\t\t");
			outFile.append(itemToQuery.getPropertyName()+"\t");
			outFile.append("\n");

			//second line: column types
			outFile.append("A\t\t\t\t");				
			outFile.append(itemToQuery.getTypeString() + "\t");
			outFile.append("\n");

			for(int i=0;i<rawValues.length;i++){
				outFile.append(rawValues[i].getTime().toFITS()+"\t"+rawValues[i].getValue()+"\n");
			}

			outFile.close();
			retVal = QueryResult.SUCCESS;

		} catch(IOException ex){
			logger.fine("Error accesing file:" + ex.toString());
			retVal = QueryResult.FAILURE;
		}
		return retVal;
	}

	/**
	 * Returns the logger for the presentation model.
	 * @return the logger used by this presentation model.
	 */
	public Logger getLogger() {
		return logger;
	}
}

/**
* Helper class to hold the statistics calculated for some property
*/
class Stats<T>
{
	/**
	 * The statistical mean.
	 */
    public double mean;

    /**
	 * The statistical median.
	 */
    public T median;
    
	/**
	 * The statistical mode.
	 */
    public T mode;
    
	/**
	 * The statistical standard deviation.
	 */
    public Double stdDev;
    
	/**
	 * The maximum value.
	 */
    public T max;
    
	/**
	 * The minimum value.
	 */
    public T min;

    /**
	 * The number, N, of items.
	 */
    public int N;
}
