package alma.Control.gui.monitordb;

import java.util.List;


/**
 * Contains all the information for a monitor point query item.
 * 
 * @author Jeff Kern
 */
public class QueryFloatItem extends QueryItem
{
    
	private static final String TYPE_STRING = "R";

	/**
	 * Constructor.
	 * @param antennaName the name of the antenna.
	 * @param deviceName the name of the device.
	 * @param propertyName the name of the property.
	 */
    public QueryFloatItem(String antennaName, String deviceName, 
			  String propertyName)
    {
	super(antennaName, deviceName, propertyName);
    }

    /* These methods define which statistical quantites we have */
    public boolean hasMean()  {return true;}
    public boolean hasMedian(){return true;}
    public boolean hasMode()  {return false;}
    public boolean hasStandardDeviation(){return true;}
    public boolean hasMax()   {return true;}
    public boolean hasMin()   {return true;}
	
	public String meanType(){return "R";}
	public String medianType(){return "R";}
	public String stdDevType(){return "R";}
	public String nType(){return "I";}
	public String maxType(){return "R";}
	public String minType(){return "R";}

    
    public Stats calculateStatistics(List<?> values)
    {
    	Stats<Float> floatStats = new Stats<Float>();
		// calculate the appropriate stats
		if(shouldCalculateMean()) {
		    floatStats.mean = Statistics.mean(values.toArray(new Float[0]));
		}
		if(shouldCalculateMedian()) {
			floatStats.median = Statistics.median(values.toArray(new Float[0]));
		}
		if(shouldCalculateStandardDeviation()) {
			floatStats.stdDev = Statistics.stdDev(values.toArray(new Float[0]));
		}
		if(shouldCalculateN()) {
			floatStats.N = values.size();
		}
		if(shouldCalculateMin()) {
			floatStats.min = Statistics.min(values.toArray(new Float[0]));
		}
		if(shouldCalculateMax()) {
			floatStats.max = Statistics.max(values.toArray(new Float[0]));
		}
		return floatStats;	
    }
    
    public String getTypeString()
    {
    	return TYPE_STRING;
    }
}
