package alma.Control.gui.monitordb;

import java.util.logging.Logger;

/**
 * AntennaSelectionPanel is a panel which allows the user to select 
 * one or more antennas from a list.
 * 
 * @author Steve Harrington
 */
@SuppressWarnings("serial")
public class AntennaSelectionPanel extends SelectionPanelBase
{
    // for testing only!
	protected final static String[] ANTENNA_TEST_DATA = 
	{
		"T01", "DV01", "T02", "T26" 
	};

	// hack to properly size the scroll pane - using extra white space 
	// in the label due to the custom resize logic, it was difficult to
	// get the size just right and the easiest solution to make it reasonable
	// was to pad the label text probably not good form, but it works.
	final static String ANTENNA_LABEL_TEXT = "Location   ";

	/**
	 * Constructor for an antenna selection panel, used to select one 
	 * (or more) antennas.
	 * @param presentationModel the presentationModel to use for 
	 * interactions with the backend (db, control system, etc.)
	 */
	public AntennaSelectionPanel(MonitorDatabasePresentationModel presentationModel)
	{
		super(ANTENNA_LABEL_TEXT, presentationModel);
		this.list.setEnabled(true);
	}

	/**
	 * No args constructor; should be used ONLY for testing.
	 */
	public AntennaSelectionPanel()
	{
		super("only for testing!...", new MonitorDatabasePresentationModel(Logger.getLogger("TestLogger")));
		this.list.setEnabled(true);
		logger.warning("WARNING: this constructor should only be used for testing!");
	}
	
	public void update() {
		this.setListValues(presentationModel.getFilteredAntennas());
		this.selectListValues(presentationModel.getAntennaFilter());
	}

	/** 
	 * For testing only...
	 * @param args command line arguments, if any
	 */
	public static void main(String[] args)
	{
		// Schedule a job for the event-dispatching thread:
		//creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI(AntennaSelectionPanel.class, ANTENNA_TEST_DATA);
			}
		});
	}
}
