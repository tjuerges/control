package alma.Control.gui.monitordb;

import java.util.List;


/**
 * Contains all the information for a monitor point query item.
 * 
 * @author Jeff Kern
 */
public class QueryStringItem extends QueryItem
{
    
	private static final String TYPE_STRING = "A";

	/**
	 * Constructor.
	 * @param antennaName the name of the antenna.
	 * @param deviceName the name of the device.
	 * @param propertyName the name of the property.
	 */
    public QueryStringItem(String antennaName, String deviceName, 
			  String propertyName)
    {
	super(antennaName, deviceName, propertyName);
    }

    /* These methods define which statistical quantites we have */
    public boolean hasMean()  {return false;}
    public boolean hasMedian(){return false;}
    public boolean hasMode()  {return false;}
    public boolean hasStandardDeviation(){return false;}
    public boolean hasMax()   {return false;}
    public boolean hasMin()   {return false;}
	
	public String nType(){return "I";}

    public Stats calculateStatistics(List<?> values)
    {
    	Stats<String> stringStats = new Stats<String>();
		if(shouldCalculateN()){
			stringStats.N = values.size();
		}
		return stringStats;
    }
    
    public String getTypeString()
    {
    	return TYPE_STRING;
    }
}
