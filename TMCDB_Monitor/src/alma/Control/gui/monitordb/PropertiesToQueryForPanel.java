package alma.Control.gui.monitordb;

import java.util.logging.Logger;

/**
 * PropertiesToQueryForPanel is a panel which allows
 * displays a list of selected properties that have been added,
 * and allows selections to be removed.
 * 
 * @author Steve Harrington
 */
@SuppressWarnings("serial")
public class PropertiesToQueryForPanel extends SelectionPanelBase 
{
	private final static String 
	SELECTED_PROPERTIES_LABEL = "Properties to Query";

	/**
	 * Constructor.
	 * @param presentationModel the presentation model to use for 
	 * interaction with the backend (e.g. control subsystem, DB, etc.)
	 */
	public PropertiesToQueryForPanel(MonitorDatabasePresentationModel presentationModel)
	{
		super(SELECTED_PROPERTIES_LABEL, presentationModel);
	}

	/**
	 * No args constructor; should be used ONLY for testing.
	 */
	public PropertiesToQueryForPanel()
	{
		super("only for testing!...", new MonitorDatabasePresentationModel(Logger.getLogger("TestLogger")));
		this.setEnabled(true);
		logger.warning("WARNING: this constructor should only be used for testing!");
	}
	
	@Override
	public void update() {
		// This is a noop for this class, because the list is *always* 
		// empty initially; it is only populated based on user action,
		// which will update the list using the "updateList" method 
		// (from SelectionPanelBase super class) when appropriate.
	}

	/** 
	 * For testing only...
	 * @param args command line arguments, if any
	 */
	public static void main(String[] args)
	{
		// Schedule a job for the event-dispatching thread:
		//creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI(PropertiesToQueryForPanel.class, 
						PropertySelectionPanel.PROPERTY_TEST_DATA);
			}
		});
	}
}
