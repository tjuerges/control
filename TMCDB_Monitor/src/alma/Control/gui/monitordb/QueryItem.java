package alma.Control.gui.monitordb;

import java.util.List;


/**
 * Contains all the information for a monitor point query item.
 * 
 * @author Steve Harrington
 */
public abstract class QueryItem 
{
	final static String ANTENNA_DEVICE_SEPARATOR = "/";
	final static String DEVICE_PROPERTY_SEPARATOR = ":";
	
	private String antennaName;
	private String deviceName;
	private String propertyName;

	private boolean calculateMean;
	private boolean calculateMedian;
	private boolean calculateMode;
	private boolean calculateStandardDeviation;
	private boolean calculateN;
	private boolean calculateMax;
	private boolean calculateMin;


    /**
	 * Constructor.
	 * @param antennaName the name of the antenna
	 * @param deviceName the name of the device
	 * @param propertyName the name of the property
	 */
	public QueryItem(String antennaName, String deviceName, String propertyName)
	{
		this.antennaName = antennaName;
		this.deviceName = deviceName;
		this.propertyName = propertyName;
		init();
	}

	/**
	 * Alternate constructor taking a single string, describing the fully qualified property name.
	 * 
	 * @param fullyQualifiedPropertyName the fully qualified property name (e.g.
	 * AntennaName/DeviceName:PropertyName - where / is represented by 
	 * the ANTENNA_DEVICE_SEPARATOR constant and : is represented by the
	 * DEVICE_PROPERTY_SEPARATOR constant.
	 */
	public QueryItem(String fullyQualifiedPropertyName)
	{
		String[] splitList = fullyQualifiedPropertyName.split(ANTENNA_DEVICE_SEPARATOR);
		String[] secondSplit = splitList[1].split(DEVICE_PROPERTY_SEPARATOR);
		
		this.antennaName = splitList[0];
		this.deviceName = secondSplit[0];
		this.propertyName = secondSplit[1];
		
		init();
	}
	
	@Override
	public String toString()
	{
		return "AntennaName: " + antennaName + " DeviceName: " + deviceName + " propertyName: " + propertyName
			+ " mean: " + calculateMean + " median: " + calculateMedian + " standardDeviation: " + calculateStandardDeviation 
			+ " N: " + calculateN + " min: " + calculateMin + " max: " + calculateMax;
	}
	
	/**
	 * Returns the fully qualified property name, e.g. AntennaName/DeviceName:PropertyName
	 * @return fully qualified property name.
	 */
	public String fullyQualifiedPropertyName()
	{
		return getAntennaName() + ANTENNA_DEVICE_SEPARATOR + getDeviceName() + DEVICE_PROPERTY_SEPARATOR + getPropertyName();	
	}
	
	/**
	 * Getter for the antenna name.
	 * @return the antenna name.
	 */
	public String getAntennaName() {
		return antennaName;
	}

	/**
	 * Getter for the boolean indicating whether to calculate the mean.
	 * @return the flag which indicates whether we should calculate the mean (true) or not (false).
	 */
	public Boolean shouldCalculateMean() {
		return calculateMean;
	}

	/**
	 * Getter for the boolean indicating whether to calculate the median.
	 * @return the flag which indicates whether we should calculate the median (true) or not (false).
	 */	
	public Boolean shouldCalculateMedian() {
		return calculateMedian;
	}
	
	/**
	 * Getter for the boolean indicating whether to calculate the mode.
	 * @return the flag which indicates whether we should calculate the mode (true) or not (false).
	 */	
	public Boolean shouldCalculateMode() {
		return calculateMode;
	}

	/**
	 * Getter for the boolean indicating whether to calculate the standard deviation.
	 * @return the flag which indicates whether we should calculate the standard deviation (true) or not (false).
	 */	
	public Boolean shouldCalculateStandardDeviation() {
		return calculateStandardDeviation;
	}

	/**
	 * Getter for the boolean indicating whether to calculate N.
	 * @return the flag which indicates whether we should calculate the N (true) or not (false).
	 */	
	public Boolean shouldCalculateN() {
		return calculateN;
	}
	
	/**
	 * Getter for the boolean indicating whether to calculate max.
	 * @return the flag which indicates whether we should calculate the max (true) or not (false).
	 */	
	public Boolean shouldCalculateMax() {
		return calculateMax;
	}
	
	/**
	 * Getter for the boolean indicating whether to calculate min.
	 * @return the flag which indicates whether we should calculate the min (true) or not (false).
	 */	
	public Boolean shouldCalculateMin() {
		return calculateMin;
	}

	/**
	 * Getter for the device name.
	 * @return the device name.
	 */
	public String getDeviceName() {
		return deviceName;
	}

	/**
	 * Getter for the property name.
	 * @return the property name.
	 */
	public String getPropertyName() {
		return propertyName;
	}

	/**
	 * Getter for the flag indicating whether the max should be calculated for this query item.
	 * @param calculateMax true indicates max should be calculated; false indicates it shouldn't be calculated.
	 */
	public void setCalculateMax(boolean calculateMax) {
		this.calculateMax = calculateMax;
	}

	/**
	 * Getter for the flag indicating whether the mean should be calculated for this query item.
	 * @param calculateMean true indicates mean should be calculated; false indicates it shouldn't be calculated.
	 */
	public void setCalculateMean(boolean calculateMean) {
		this.calculateMean = calculateMean;
	}

	/**
	 * Getter for the flag indicating whether the mode should be calculated for this query item.
	 * @param calculateMode true indicates mode should be calculated; false indicates it shouldn't be calculated.
	 */
	public void setCalculateMode(boolean calculateMode) {
		this.calculateMode = calculateMode;
	}
	
	/**
	 * Getter for the flag indicating whether the median should be calculated for this query item.
	 * @param calculateMedian true indicates median should be calculated; false indicates it shouldn't be calculated.
	 */
	public void setCalculateMedian(boolean calculateMedian) {
		this.calculateMedian = calculateMedian;
	}

	/**
	 * Getter for the flag indicating whether the min should be calculated for this query item.
	 * @param calculateMin true indicates min should be calculated; false indicates it shouldn't be calculated.
	 */
	public void setCalculateMin(boolean calculateMin) {
		this.calculateMin = calculateMin;
	}
	
	/**
	 * Getter for the flag indicating whether the N should be calculated for this query item.
	 * @param calculateN true indicates N should be calculated; false indicates 
	 * it shouldn't be calculated.
	 */
	public void setCalculateN(boolean calculateN) {
		this.calculateN = calculateN;
	}

	/**
	 * Getter for the flag indicating whether the standard deviation should be calculated for this query item.
	 * @param calculateStandardDeviation true indicates standard deviation should be calculated; false indicates it shouldn't be calculated.
	 */
	public void setCalculateStandardDeviation(boolean calculateStandardDeviation) {
		this.calculateStandardDeviation = calculateStandardDeviation;
	}	
	
        /* These abstract method define which statistical quantites we have */
	/**
	 * Method which indicates whether a given query item should allow statistical quantity 
	 * of mean to be calculated.
	 * 
	 * @return boolean indicating whether the query item should allow calculation of the statistical 
	 * mean (true) or not (false).
	 */
	public abstract boolean hasMean();
	
	/**
	 * Method which indicates whether a given query item should allow statistical quantity 
	 * of median to be calculated.
	 * 
	 * @return boolean indicating whether the query item should allow calculation of the statistical 
	 * median (true) or not (false).
	 */
	public abstract boolean hasMedian();

	/**
	 * Method which indicates whether a given query item should allow statistical quantity
	 * of mode to be calculated.
	 * 
	 * @return boolean indicating whether the query item should allow calculation of the statistical 
	 * mode (true) or not (false).
	 */
	public abstract boolean hasMode();
	
	/**
	 * Method which indicates whether a given query item should allow statistical quantity 
	 * of standard deviation to be calculated.
	 * 
	 * @return boolean indicating whether the query item should allow calculation of the statistical 
	 * standard deviation (true) or not (false).
	 */
	public abstract boolean hasStandardDeviation();
	
	/**
	 * Method which indicates whether a given query item should allow statistical quantity 
	 * of max to be calculated.
	 * 
	 * @return boolean indicating whether the query item should allow calculation of the statistical 
	 * max (true) or not (false).
	 */
	public abstract boolean hasMax();
	
	/**
	 * Method which indicates whether a given query item should allow statistical quantity 
	 * of min to be calculated.
	 * 
	 * @return boolean indicating whether the query item should allow calculation of the statistical 
	 * min (true) or not (false).
	 */
	public abstract boolean hasMin();
	
	/**
	 * Method which calculates statistics for a collection of values of the proper type.
	 * @param values a list of values for which we want to calculate statistics
	 * @return statistics calculated for the values 
	 */
	public abstract Stats calculateStatistics(List<?> values);
	
	/**
	 * Returns a type string indicating the type of the query item.
	 * @return a string indicating the type of the query item.
	 */
	public abstract String getTypeString();

	private void init()
	{
		this.calculateMean = false;
		this.calculateMedian = false;
		this.calculateMode = false;
		this.calculateStandardDeviation = false;
		this.calculateN = false;
		this.calculateMax = false;
		this.calculateMin = false;
	}

	/**
	 * Returns a type string indicating the type of the statistic for this queryitem.
	 * @return a string indicating the type of the statistic for this query item.
	 */
	public String meanType(){return "";}
	
	/**
	 * Returns a type string indicating the type of the statistic for this queryitem.
	 * @return a string indicating the type of the statistic for this query item.
	 */
	public String medianType(){return "";}

	/**
	 * Returns a type string indicating the type of the statistic for this queryitem.
	 * @return a string indicating the type of the statistic for this query item.
	 */
	public String modeType(){return "";}

	/**
	 * Returns a type string indicating the type of the statistic for this queryitem.
	 * @return a string indicating the type of the statistic for this query item.
	 */
	public String stdDevType(){return "";}
	
	/**
	 * Returns a type string indicating the type of the statistic for this queryitem.
	 * @return a string indicating the type of the statistic for this query item.
	 */
	public String nType(){return "";}
	
	/**
	 * Returns a type string indicating the type of the statistic for this queryitem.
	 * @return a string indicating the type of the statistic for this query item.
	 */
	public String maxType(){return "";}
	
	/**
	 * Returns a type string indicating the type of the statistic for this queryitem.
	 * @return a string indicating the type of the statistic for this query item.
	 */
	public String minType(){return "";}
}
