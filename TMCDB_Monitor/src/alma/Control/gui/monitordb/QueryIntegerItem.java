package alma.Control.gui.monitordb;

import java.util.List;


/**
 * Contains all the information for a monitor point query item.
 * 
 * @author Jeff Kern
 */
public class QueryIntegerItem extends QueryItem
{
 
	private static final String TYPE_STRING = "I";

	/**
	 * Constructor.
	 * @param antennaName the name of the antenna.
	 * @param deviceName the name of the device.
	 * @param propertyName the name of the property.
	 */
    public QueryIntegerItem(String antennaName, String deviceName, 
			  String propertyName)
    {
	super(antennaName, deviceName, propertyName);
    }

    /* These methods define which statistical quantites we have */
    public boolean hasMean()  {return true;}
    public boolean hasMedian(){return true;}
    public boolean hasMode()  {return true;}
    public boolean hasStandardDeviation(){return true;}
    public boolean hasMax()   {return true;}
    public boolean hasMin()   {return true;}
    
	public String meanType(){return "R";}
	public String medianType(){return "I";}
	public String modeType(){return "I";}
	public String stdDevType(){return "R";}
	public String nType(){return "I";}
	public String maxType(){return "I";}
	public String minType(){return "I";}

    public Stats calculateStatistics(List<?> values)
    {
    	Stats<Integer> intStats = new Stats<Integer>();
		// calculate the appropriate stats
		if(shouldCalculateMean()){
			intStats.mean = Statistics.mean(values.toArray(new Integer[0]));
		}
		if(shouldCalculateMedian()){
			intStats.median = Statistics.median(values.toArray(new Integer[0]));
		}
		if(shouldCalculateStandardDeviation()){
			intStats.stdDev = Statistics.stdDev(values.toArray(new Integer[0]));
		}
		if(shouldCalculateN()){
			intStats.N = values.size();
		}
		if(shouldCalculateMin()){
			intStats.min = Statistics.min(values.toArray(new Integer[0]));
		}
		if(shouldCalculateMax()){
			intStats.max = Statistics.max(values.toArray(new Integer[0]));
		}
		if(shouldCalculateN()){
			intStats.N = values.size();
		}
		return intStats;
    }
    
    public String getTypeString()
    {
    	return TYPE_STRING;
    }
}
