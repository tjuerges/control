package alma.Control.gui.monitordb;

import java.text.ParseException;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;

/**
 * Private class used to verify formatted text fields, 
 * updating the status information to indicate error or success.
 * @author Steve Harrington
 */
public class StatusUpdatingInputVerifier extends InputVerifier 
{
	private String errorText;
	private StatusUpdateable updateableStatus;
	
	/**
	 * Constructor.
	 * @param errorText the text to display upon erroneously 
	 * formatted input.
	 * @param updateableStatus object that is notified of error or "ok" status after input is verified.
	 */
	public StatusUpdatingInputVerifier(String errorText, StatusUpdateable updateableStatus)
	{
		this.errorText = errorText;
		this.updateableStatus = updateableStatus;
	}

	@Override
	public boolean verify(JComponent input) {
		boolean retVal = false;
		if(!(input instanceof JFormattedTextField)) {
			retVal = true;
		}
		JFormattedTextField textField = (JFormattedTextField)input;
		JFormattedTextField.AbstractFormatter formatter = textField.getFormatter();
		if(formatter == null) {
			retVal = true;
		}
		if(textField.getText() != null && textField.getText().length() > 0)
		{
			try {
				formatter.stringToValue(textField.getText());
				updateableStatus.statusOk();
				return true;
			}
			catch(ParseException ex)
			{
				updateableStatus.statusError(errorText);
				retVal = false;
			}
		}
		else 
		{
			updateableStatus.statusOk();
			retVal = true;
		}
		return retVal;
	}
}
