package alma.Control.gui.monitordb;

import java.util.List;


/**
 * Contains all the information for a monitor point query item.
 * 
 * @author Jeff Kern
 */
public class QueryEnumItem extends QueryItem
{
    private static final String TYPE_STRING = "A";

	/**
     * Constructor.
     * @param antennaName the name of the antenna.
     * @param deviceName the name of the device.
     * @param propertyName the name of the property.
     */
    public QueryEnumItem(String antennaName, String deviceName, 
			  String propertyName)
    {
	super(antennaName, deviceName, propertyName);
    }

    /* These methods define which statistical quantites we have */
    public boolean hasMean()  {return false;}
    public boolean hasMedian(){return false;}
    public boolean hasMode()  {return true;}
    public boolean hasStandardDeviation(){return false;}
    public boolean hasMax()   {return false;}
    public boolean hasMin()   {return false;}
	
	public String modeType(){return "I";}
	public String nType(){return "I";}

    
    public Stats calculateStatistics(List<?> values)
    {
    	Stats<Integer> enumStats = new Stats<Integer>();
		if(shouldCalculateN()){
			enumStats.N = values.size();
		}
		if(shouldCalculateMode()){
		    enumStats.mode = Statistics.mode(values.toArray(new Integer[0]));
		}
		return enumStats;
    }
    
    public String getTypeString()
    {
    	return TYPE_STRING;
    }
}
