package alma.Control.gui.monitordb;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.logging.Logger;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.text.Position;

/**
 * SelectionPanelBase is a panel which allows the user to select 
 * one or more properties from a list.
 * 
 * @author Steve Harrington
 */
@SuppressWarnings("serial")
public abstract class SelectionPanelBase extends JPanel 
{	
	private final static int MAX_VISIBLE_ROWS = 15;
	protected String labelString;
	protected JLabel label;
	protected JList list;
	protected JPanel listAndLabelPanel;
	protected JScrollPane scrollPane;
	protected DefaultListModel listModel;
	protected MonitorDatabasePresentationModel presentationModel;
	protected Logger logger;

	/**
	 * Constructor.
	 * 
	 * @param labelString the text to use for the label that goes above the list box
	 * @param presModel the presentation model which will be used to interact
	 * with the backend (db, control subsystem, or what not).
	 */
	public SelectionPanelBase(String labelString, MonitorDatabasePresentationModel presModel)
	{
		this.presentationModel = presModel;
		this.logger = presModel.getLogger();
		this.labelString = labelString;
		initializeGUIComponents();
		update();
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		this.list.setEnabled(enabled);
	}

	/**
	 * This method must be implemented by subclasses; it is used to 
	 * populate the list, e.g. from the backend or from user 
	 * action in the GUI, depending on the context of the particular
	 * subclass in question.
	 */
	public abstract void update();

	/**
	 * Method to update (clear all existing entries and replace with new entries)
	 * a list. The list is maintained in alphabetically sorted order.
	 * 
	 * @param newListEntries the items to place in the list, 
	 *        after first clearing any existing entries; entries
	 *        will be alphabetized before adding to the list.
	 */
	protected void setListValues(String[] newListEntries)
	{
		// clear the existing (old) entries from the list
		this.listModel.clear();

		// alphabetize the new items to be added to the list
		// to make things easier for the user to find
		Arrays.sort(newListEntries);

		// add the new items to the list
		for(String item: newListEntries) {
			this.listModel.addElement(item);
		}
	}

	protected void selectListValues(String[] newSelectionList) {
		int[] indexList = new int[newSelectionList.length];
		for (int i = 0; i < newSelectionList.length; i++) {
			indexList[i] = this.list.getNextMatch(newSelectionList[i],0,
					Position.Bias.Forward);
		}
		this.list.setSelectedIndices(indexList);
	}       

	/**
	 * Method to add to (without clearing any existing entries)
	 * a list. The list is maintained in sorted (alphabetically) order.
	 * 
	 * @param newListEntries the items to place in the list, 
	 *        after first clearing any existing entries; entries
	 *        will be alphabetized before adding to the list.
	 */
	protected void appendListValues(String[] itemsToAdd) {
		Enumeration listEnum = this.listModel.elements();
		ArrayList<String> newList = new ArrayList<String>();
		while(listEnum.hasMoreElements()) 
		{
			newList.add((String)listEnum.nextElement());
		}
		for(String item : itemsToAdd)
		{
			if(!newList.contains(item)) 
			{
				newList.add(item);
			}
		}
		String[] sortArray = newList.toArray(new String[0]);
		Arrays.sort(sortArray);

		this.listModel.clear();
		for(String item : sortArray)
		{
			this.listModel.addElement(item);
		}
	}

	/**
	 * Private method which constructs the Panel.
	 */
	private void initializeGUIComponents()
	{
		this.listAndLabelPanel = new JPanel();
		this.label = new JLabel(labelString);
		this.listModel = new DefaultListModel();
		this.list = new JList(listModel);

		listAndLabelPanel.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.weightx = 1.0;
		constraints.weighty = 0.0;
		constraints.gridx = 0;
		constraints.fill = GridBagConstraints.NONE;
		constraints.anchor = GridBagConstraints.NORTHWEST;
		listAndLabelPanel.add(label, constraints);

		this.scrollPane = new JScrollPane(list);
		constraints.weighty = 2.0;
		constraints.fill = GridBagConstraints.BOTH;
		listAndLabelPanel.add(scrollPane, constraints);

		this.setLayout(new BorderLayout());
		this.add(listAndLabelPanel, BorderLayout.NORTH);
		this.addComponentListener(new ResizeComponentListener());

		this.list.setEnabled(false);
		this.list.setVisibleRowCount(MAX_VISIBLE_ROWS);
	}

	/**
	 * Private class used to resize the scrollpane; could not find a way to have it
	 * resize automagically with normal layout managers / mechanisms.
	 * 
	 * @author sharring
	 */
	private class ResizeComponentListener extends ComponentAdapter
	{
		Dimension dimension;

		/**
		 * Constructor.
		 */
		public ResizeComponentListener()
		{
			dimension = new Dimension();
		}

		/**
		 * Invoked when the component's size changes. This custom resize logic
		 * is used to resize the scrollpane, which seemed to not resize of its
		 * own accord.
		 */
		public void componentResized(ComponentEvent e) 
		{    	
			// resize the scrollpane's panel (listAndLabelPanel)
			dimension.width = getWidth(); 
			dimension.height = getHeight();
			listAndLabelPanel.setPreferredSize(dimension);

			// revalidate to allow proper repaint behavior
			revalidate();
		}
	} 

	/**
	 * For testing only...
	 */
	protected static void createAndShowGUI(Class<?> panelClass, String[] testData)
	{
		// Create and set up the window.
		JFrame frame = new JFrame("Test of " + panelClass.getSimpleName());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Constructor constructor = null;
		try {
			constructor = panelClass.getConstructor();
		} catch (SecurityException e1) {
			e1.printStackTrace();
			System.exit(1);
		} catch (NoSuchMethodException e1) {
			e1.printStackTrace();
			System.exit(1);
		}
		SelectionPanelBase myPanel = null;
		try {
			myPanel = (SelectionPanelBase)constructor.newInstance();
			myPanel.setListValues(testData);
			frame.getContentPane().add(myPanel);
			frame.pack();
			frame.setVisible(true);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (InstantiationException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}	
}
