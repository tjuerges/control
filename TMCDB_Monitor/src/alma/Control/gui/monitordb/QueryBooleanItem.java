package alma.Control.gui.monitordb;

import java.util.List;


/**
 * Contains all the information for a monitor point query item.
 * 
 * @author Jeff Kern
 */
public class QueryBooleanItem extends QueryItem
{
	private static final String TYPE_STRING = "B";

	/**
	 * Constructor
	 * @param antennaName the name of the antenna
	 * @param deviceName the name of the device
	 * @param propertyName the name of the property
	 */
    public QueryBooleanItem(String antennaName, String deviceName, 
			    String propertyName) {
	super(antennaName, deviceName, propertyName);
    }

    /* These methods define which statistical quantites we have */
    public boolean hasMean()  {return false;}
    public boolean hasMedian(){return false;}
    public boolean hasMode()  {return true;}
    public boolean hasStandardDeviation(){return false;}
    public boolean hasMax()   {return false;}
    public boolean hasMin()   {return false;}
   
	public String modeType(){return "B";}
	public String nType(){return "I";}


    public Stats calculateStatistics(List<?> values)
    {
    	Stats<Boolean> boolStats = new Stats<Boolean>();
	    if(shouldCalculateMode()){
		    boolStats.mode = Statistics.mode(values.toArray(new Boolean[0]));
	    }
	    if(shouldCalculateN()){
		    boolStats.N = values.size();
	    }
	    return boolStats;
    }
    
    public String getTypeString()
    {
    	return TYPE_STRING;
    }
}
