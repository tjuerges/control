package alma.Control.gui.monitordb;

import java.util.logging.Logger;

/**
 * DeviceSelectionPanel is a panel which allows the user to select 
 * one or more devices from a list.
 * 
 * @author Steve Harrington
 */
@SuppressWarnings("serial")
public class DeviceSelectionPanel extends SelectionPanelBase
{
	protected final static String[] DEVICE_TEST_DATA = 
	{ 
		"OpticalTelescope", "FLOOG", "LORR" 
	};
	
	final static String DEVICE_LABEL_TEXT = "Device";

	/**
	 * Constructor.
	 * @param presentationModel the presentation model to use for interaction
	 * with the backend (e.g. control subsystem, DB, etc.)
	 */
	public DeviceSelectionPanel(MonitorDatabasePresentationModel presentationModel)
	{
		super(DEVICE_LABEL_TEXT, presentationModel);
		this.setEnabled(true);
	}
	
	/**
	 * No args constructor; should be used ONLY for testing.
	 */
	public DeviceSelectionPanel()
	{
		super("only for testing!...", new MonitorDatabasePresentationModel(Logger.getLogger("TestLogger")));
		this.setEnabled(true);
		logger.warning("WARNING: this constructor should only be used for testing!");
	}

	public void update() {
		this.setListValues(presentationModel.getFilteredDevices());
		this.selectListValues(presentationModel.getDeviceFilter());
	}

	/** 
	 * For testing only...
	 * @param args command line arguments, if any
	 */
	public static void main(String[] args)
	{
		// Schedule a job for the event-dispatching thread:
		//creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI(DeviceSelectionPanel.class,
						DEVICE_TEST_DATA);
			}
		});
	}
}
