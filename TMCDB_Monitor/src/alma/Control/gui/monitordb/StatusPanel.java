package alma.Control.gui.monitordb;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Panel that will display status (results of user-initiated actions/operations).
 * @author Steve Harrington
 */
public class StatusPanel extends JPanel implements StatusUpdateable 
{
	private String okText;
	private JLabel statusLabel;
	private JLabel statusTextLabel;
	
	private static final String STATUS_TEXT = "Status: ";
	private static final int INSET_RIGHT = 4;
	private static final int INSET_BOTTOM = 4;
	private static final int INSET_LEFT = 4;
	private static final int INSET_TOP = 4;
	/**
	 * TODO - define something meaningful for serialVersionUID, if necessary.
	 */
	private static final long serialVersionUID = 1L;
	
	
	
	/**
	 * Constructor.
	 * @param okText text to display for normal or non-error status.
	 */
	public StatusPanel(String okText)
	{
		this.okText = okText;
		
		this.setLayout(new GridBagLayout());
		
		statusLabel = new JLabel(STATUS_TEXT);
		statusLabel.setForeground(Color.BLACK);
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.SOUTHWEST;
		constraints.fill = GridBagConstraints.NONE;
		constraints.gridx = GridBagConstraints.RELATIVE;
		constraints.gridy = 0;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		
		this.add(statusLabel, constraints);
		statusTextLabel = new JLabel(okText);
		constraints.weightx = 1.0;
		this.add(statusTextLabel, constraints);
		this.setBorder(BorderFactory.createEmptyBorder(2 * INSET_TOP, 
				2 * INSET_LEFT, 2 * INSET_BOTTOM, INSET_RIGHT));
	}

	/**
	 * Constructor.
	 * @param okText text to display for normal or non-error status.
	 * @param borderTitle title for border of panel.
	 */
	public StatusPanel(String okText, String borderTitle)
	{
		this(okText);
		this.setBorder(BorderFactory.createTitledBorder(borderTitle));
	}
	
	public void statusOk()
	{
		if(!statusLabel.getText().equals(okText)) {
			statusTextLabel.setText(okText);
			statusLabel.setForeground(Color.BLACK);
		}
	}

	public void statusError(String errorText)
	{
		statusTextLabel.setText(errorText);
		statusLabel.setForeground(Color.RED);
	}
	
	public void setStatusText(String statusText, Color textColor)
	{
		statusTextLabel.setText(statusText);
		statusLabel.setForeground(textColor);
	}
}
