package alma.Control.gui.monitordb;

import java.awt.Color;

/**
 * Interface that can be implemented by classes that wish to have an indication of error on input verification.
 * @author Steve Harrington
 *
 */
public interface StatusUpdateable 
{
	/**
	 * Indicates that the status condition is abnormal or "not ok", i.e. in error.
	 * @param errorText text message to display for error.
	 */
	public void statusError(String errorText);
	
	/**
	 * Indicates that the status condition is normal or "ok", i.e. not in error.
	 */
	public void statusOk();

	/**
	 * Sets status manually to a specific string/color.
	 * @param statusText text to display.
	 * @param textColor color of the text.
	 */
	public void setStatusText(String statusText, Color textColor);
}
