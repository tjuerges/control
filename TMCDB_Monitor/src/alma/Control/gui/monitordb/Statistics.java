/*
ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*/
/** 
 * @author  nbarriga
 * @version $Id$
 * @since    
 */
package alma.Control.gui.monitordb;

import java.util.Arrays;
import java.util.logging.Logger;
import java.lang.Math;


/**
 * Collection of simple statistics routines. This is a preliminary approach, just to get the algorithms right.
 * Need to think of a way to write only once the code for all the data types(templates?)
 *
 * Maybe it's a good idea to calculate all metrics at once, that way we traverse the array only once.(or sort it only once to
 * calculate median and mode)
 * 
 * @author  nbarriga
 */
public class Statistics
{	
//------------------------------BLOB-------------------------------------------
//------------------------------BOOLEAN----------------------------------------
	/**
	 * Calculating the mode for Booleans is just counting the 0s or 1s.
	 * @param items an array of Booleans, for which we want to find the mode.
	 * @return the mode of the items in the array.
	 */
	public static Boolean mode(Boolean[] items)
	{
		if(items.length<1)return false;
		int ones = 0;
		for(int i=0;i<items.length;i++)
		    if(items[i])
			ones++;
		if(ones > items.length/2)
		    return true;
		else 
		    return false;
	}
//------------------------------DOUBLE-----------------------------------------
	/**
	 * Method to calculate the mean of an array of Doubles.
	 * @param items the array of Doubles for which to calculate the mean.
	 * @return a Double representing the mean that was calculated.
	 */
	public static Double mean(Double[] items)
	{
		if(items.length<1)return 0.0;
		double res = 0;
		for(int i=0;i<items.length;i++)
                        res += items[i];//this is vulnerable to overflow/precision loss
		return res / items.length;
	}
	/**
	* This method finds the median faster than quicksort and doesn't change the array.
	* @param items an array of Doubles for which we want to find the median.
	* @return the median of the values.
  	*/
	public static Double median(Double[] items)
	{
		if(items.length<1)return 0.0;

		int         i, less, greater, equal, n = items.length;
		double  min, max, guess, maxltguess, mingtguess;

		min = max = items[0] ;
		for (i=1 ; i<n ; i++) {
			if (items[i]<min) min=items[i];
			if (items[i]>max) max=items[i];
		}

		while (true) {
			guess = (min+max)/2;
			less = 0; greater = 0; equal = 0;
			maxltguess = min ;
			mingtguess = max ;
			for (i=0; i<n; i++) {
				if (items[i]<guess) {
					less++;
					if (items[i]>maxltguess) maxltguess = items[i] ;
				} else if (items[i]>guess) {
					greater++;
					if (items[i]<mingtguess) mingtguess = items[i] ;
				} else equal++;
			}
			if (less <= (n+1)/2 && greater <= (n+1)/2) break ;
			else if (less>greater) max = maxltguess ;
			else min = mingtguess;
		}
		if (less >= (n+1)/2) return maxltguess;
		else if (less+equal >= (n+1)/2) return guess;
		else return mingtguess;
	}

	/**
	* e^2 = 1/(n-1)*(sum(x_i^2) - n*X^2)
	* need to find a ^2 and square function that operates on Doubles instead of doubles.
	* @param items an array of Doubles, for which we want to find the standard deviation.
	* @return the standard deviation of the items in the array.
	*/
	public static Double stdDev(Double[] items)
	{
		if(items.length <= 1) {
			return 0.0;
		}
		double sum = 0, sumSq = 0;
		for(int i=0;i<items.length;i++){
			sum += items[i];
			sumSq += Math.pow(items[i],2);
		}
		return Math.sqrt((sumSq - (Math.pow(sum,2))/items.length)/(items.length-1));
	}

	/**
	 * Method to find the max in an array of Doubles.
	 * @param items an array of Doubles for which we want to find the max value in the array.
	 * @return the max value that was in the array.
	 */
	public static Double max(Double[] items)
	{
		if(items.length<1)return 0.0;
		double max;
		max = items[0];
		for(int i=1;i<items.length;i++)
			if(max < items[i])
				max = items[i];
		
		return max;
	}

	/**
	 * Method to find the min in an array of Doubles.
	 * @param items an array of Doubles for which we want to find the min value in the array.
	 * @return the min value that was in the array.
	 */
	public static Double min(Double[] items)
	{
		if(items.length<1)return 0.0;
		double min;
		min = items[0];
		for(int i=1;i<items.length;i++)
			if(min > items[i])
				min = items[i];
		
		return min;
	}
//------------------------------ENUM-------------------------------------------
	/**
	* Enums are just Integers.
	* Couldn't find a way to find the mode faster than sorting the array.
	* Another way would be to fill a HashMap with the numbers as keys and the # of ocurrences as values,
	* but I doubt it's faster.
	* @param items an array of Integers, for which we want to find the mode.
	* @return the mode of the items in the array.
	*/
/*	public static Integer mode(Integer[] items)
	{
		Arrays.sort(items);//variation of quicksort
		int mode = 0;//initialized so java doesn't complain
		int times_max = -1, times = 0, i = 0;
		do{
			do{
				times++;
				i++;
			}while((i<items.length)&&(items[i-1]==items[i]));
			if(times > times_max){
				times_max = times;
				mode = items[i-1];
			}
			times = 0;
		}while(i<items.length);
					
		return mode;

	}
*/
//------------------------------FLOAT------------------------------------------
	/**
	 * Method to calculate the mean of an array of Floats.
	 * @param items the array of Floats for which to calculate the mean.
	 * @return a Float representing the mean that was calculated.
	 */
	public static Float mean(Float[] items)
	{
		if(items.length<1)return new Float(0);
		float res = 0;
		for(int i=0;i<items.length;i++)
                        res += items[i];//this is vulnerable to overflow/precision loss, and I think it's causing loss of precision in preliminary tests
		return res / items.length;
	}
	/**
	* This method finds the median faster than quicksort and doesn't change the array.
	* @param items an array of Floats for which we want to find the median.
	* @return the median of the values.
  	*/
	public static Float median(Float[] items)
	{
		if(items.length < 1) return new Float(0);

		int         i, less, greater, equal, n = items.length;
		float  min, max, guess, maxltguess, mingtguess;

		min = max = items[0] ;
		for (i=1 ; i<n ; i++) {
			if (items[i]<min) min=items[i];
			if (items[i]>max) max=items[i];
		}

		while (true) {
			guess = (min+max)/2;
			less = 0; greater = 0; equal = 0;
			maxltguess = min ;
			mingtguess = max ;
			for (i=0; i<n; i++) {
				if (items[i]<guess) {
					less++;
					if (items[i]>maxltguess) maxltguess = items[i] ;
				} else if (items[i]>guess) {
					greater++;
					if (items[i]<mingtguess) mingtguess = items[i] ;
				} else equal++;
			}
			if (less <= (n+1)/2 && greater <= (n+1)/2) break ;
			else if (less>greater) max = maxltguess ;
			else min = mingtguess;
		}
		if (less >= (n+1)/2) return maxltguess;
		else if (less+equal >= (n+1)/2) return guess;
		else return mingtguess;


	}
	/**
	* e^2 = 1/(n-1)*(sum(x_i^2) - n*X^2)
	* need to find a ^2 and square function that operates on Floats instead of doubles.
	* @param items an array of Floats, for which we want to find the standard deviation.
	* @return the standard deviation of the items in the array.
	*/
	public static Double stdDev(Float[] items)
	{
		if(items.length <= 1) {
			return 0.0; 
		}
		float sum = 0, sumSq = 0;
		for(int i=0;i<items.length;i++){
			sum += items[i];
			sumSq += Math.pow(items[i],2);
		}
		return Math.sqrt((sumSq - (Math.pow(sum,2))/items.length)/(items.length-1));
	}

	/**
	 * Method to find the max in an array of Floats.
	 * @param items an array of Floats for which we want to find the max value in the array.
	 * @return the max value that was in the array.
	 */
	public static Float max(Float[] items)
	{
		if(items.length < 1) return new Float(0);
		float max;
		max = items[0];
		for(int i=1;i<items.length;i++)
			if(max < items[i])
				max = items[i];
		
		return max;
	}

	/**
	 * Method to find the min in an array of Floats.
	 * @param items an array of Floats for which we want to find the min value in the array.
	 * @return the min value that was in the array.
	 */
	public static Float min(Float[] items)
	{
		if(items.length < 1) return new Float(0);
		float min;
		min = items[0];
		for(int i=1;i<items.length;i++)
			if(min > items[i])
				min = items[i];
		
		return min;
	}
//------------------------------INTEGER----------------------------------------
	/**
	 * Method to calculate the mean of an array of Integers.
	 * @param items the array of Integers for which to calculate the mean.
	 * @return a Integer representing the mean that was calculated.
	 */
	public static Float mean(Integer[] items)
	{
		if(items.length<1)return new Float(0);
		float res = 0;
		for(int i=0;i<items.length;i++)
                        res += items[i];//this is vulnerable to overflow/precision loss, and I think it's causing loss of precision in preliminary tests
		return res / items.length;
	}
	/**
	* This method finds the median faster than quicksort and doesn't change the array.
	* @param items an array of Integers for which we want to find the median.
	* @return the median of the values.
  	*/
	public static Integer median(Integer[] items)
	{
		if(items.length<1)return 0;
		int         i, less, greater, equal, n = items.length;
		int  min, max, guess, maxltguess, mingtguess;

		min = max = items[0] ;
		for (i=1 ; i<n ; i++) {
			if (items[i]<min) min=items[i];
			if (items[i]>max) max=items[i];
		}

		while (true) {
			guess = (min+max)/2;
			less = 0; greater = 0; equal = 0;
			maxltguess = min ;
			mingtguess = max ;
			for (i=0; i<n; i++) {
				if (items[i]<guess) {
					less++;
					if (items[i]>maxltguess) maxltguess = items[i] ;
				} else if (items[i]>guess) {
					greater++;
					if (items[i]<mingtguess) mingtguess = items[i] ;
				} else equal++;
			}
			if (less <= (n+1)/2 && greater <= (n+1)/2) break ;
			else if (less>greater) max = maxltguess ;
			else min = mingtguess;
		}
		if (less >= (n+1)/2) return maxltguess;
		else if (less+equal >= (n+1)/2) return guess;
		else return mingtguess;


	}
	/**
	* Couldn't find a way to find the mode faster than sorting the array.
	* Another way would be to fill a HashMap with the numbers as keys and the # of ocurrences as values,
	* but I doubt it's faster.
	* @param items an array of Integers, for which we want to find the mode.
	* @return the mode of the items in the array.
	*/
	public static Integer mode(Integer[] items)
	{
		if(items.length<1)return 0;
		Arrays.sort(items);//variation of quicksort
		int mode = 0;//initialized so java doesn't complain
		int times_max = -1, times = 0, i = 0;
		do{
			do{
				times++;
				i++;
			}while((i<items.length)&&(items[i-1]==items[i]));
			if(times > times_max){
				times_max = times;
				mode = items[i-1];
			}
			times = 0;
		}while(i<items.length);
					
		return mode;

	}
	/**
	* e^2 = 1/(n-1)*(sum(x_i^2) - n*X^2)
	* need to find a ^2 and square function that operates on Integers instead of doubles.
	* @param items an array of Integers, for which we want to find the standard deviation.
	* @return the standard deviation of the items in the array.
	*/
	public static Double stdDev(Integer[] items)
	{
		if(items.length <= 1) {
			return 0.0; 
		}
		float sum = 0, sumSq = 0;
		for(int i=0;i<items.length;i++){
			sum += items[i];
			sumSq += Math.pow(items[i],2);
		}
		return Math.sqrt((sumSq - (Math.pow(sum,2))/items.length)/(items.length-1));
	}

	/**
	 * Method to find the max in an array of Integers.
	 * @param items an array of Integers for which we want to find the max value in the array.
	 * @return the max value that was in the array.
	 */
	public static Integer max(Integer[] items)
	{
		if(items.length<1)return 0;
		int max;
		max = items[0];
		for(int i=1;i<items.length;i++)
			if(max < items[i])
				max = items[i];
		
		return max;
	}

	/**
	 * Method to find the min in an array of Integers.
	 * @param items an array of Integers for which we want to find the min value in the array.
	 * @return the min value that was in the array.
	 */
	public static Integer min(Integer[] items)
	{
		if(items.length<1)return 0;
		int min;
		min = items[0];
		for(int i=1;i<items.length;i++)
			if(min > items[i])
				min = items[i];
		
		return min;
	}
	/**
	* This main is just for testing purposes
	* @param args the command-line arguments.
	*/
	public static void main(String[] args)
	{
		int MAX = 500000, ITER = 1000000;
		Integer nums[] = new Integer[MAX];

		for(int i=0;i<MAX;i++){
			nums[i]=new Integer(i);
		}
		for(int i=0;i<ITER;i++){
			int randA = (int)(Math.random()*MAX);
			int randB = (int)(Math.random()*MAX);
			Integer temp = nums[randA];
			nums[randA] = nums[randB];
			nums[randB] = temp;
		}

		Logger logger = Logger.getLogger("TestLogger");
		//logger.info("Mean: %f, Median: %f, StdDev: %f, Min: %f, Max: %f",mean(nums),median(nums),stdDev(nums),min(nums),max(nums));
		logger.info("Mean: "+mean(nums).toString()+", Median: "+median(nums).toString()+", Mode: "+mode(nums).toString()+", StdDev: "+stdDev(nums).toString()+", Min: "+min(nums).toString()+", Max: "+max(nums).toString());
	}
}
