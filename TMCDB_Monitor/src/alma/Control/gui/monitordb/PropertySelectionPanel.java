package alma.Control.gui.monitordb;

import java.util.logging.Logger;

/**
 * Class to use for selection of properties.
 * @author Steve Harrington
 */
@SuppressWarnings("serial")
public class PropertySelectionPanel extends SelectionPanelBase 
{
	final static String PROPERTY_LABEL_TEXT = "Property";
	protected final static String[] PROPERTY_TEST_DATA = 
	{"temp2", "temp1", "temp3", "temp4", "temp5", 
		"temp6", "temp7", "temp8", "temp9" };

	/**
	 * Constructor.
	 * @param presentationModel the presentation model to use for 
	 * interaction with the backend (e.g. control subsystem, DB, etc.)
	 */
	public PropertySelectionPanel(MonitorDatabasePresentationModel presentationModel) {
		super(PROPERTY_LABEL_TEXT, presentationModel);
		this.setEnabled(true);
	}
	
	/**
	 * No-args constructor; for testing ONLY!
	 */
	public PropertySelectionPanel()
	{
		super("for testing only!!", new MonitorDatabasePresentationModel(Logger.getLogger("TestLogger")));
		this.setEnabled(true);
		logger.warning("WARNING: no-args constructor is for testing ONLY!");
	}

	public void update() {
		this.setListValues(presentationModel.getFilteredProperties());
		this.selectListValues(presentationModel.getPropertyFilter());
	}

	/** 
	 * For testing only...
	 * @param args command line arguments, if any
	 */
	public static void main(String[] args) {
		// Schedule a job for the event-dispatching thread:
		//creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI(PropertySelectionPanel.class, 
						PROPERTY_TEST_DATA);
			}
		});
	}
}
