package alma.Control.gui.monitordb;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingWorker;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.text.DateFormatter;

import alma.Control.gui.monitordb.MonitorDatabasePresentationModel.QueryResult;

/**
 * GUI used to allow user to choose properties (for export to a file and
 * subsequent import into CASA) from the monitor database.
 * 
 * @author Steve Harrington
 */
@SuppressWarnings("serial")
public class MonitorDatabaseQueryFrame extends JFrame 
{
	private static final String END_BORDER_TEXT = "End";
	private static final String START_BORDER_TEXT = "Start";
	private static final String FRAME_TITLE = "Monitor Database Query";
	private static final String HH_MM_SS = "HH:mm:ss";
	private static final String YYYY_MM_DD = "yyyy-MM-dd";
	private final static String DATE_ERROR_STATUS_TEXT = "invalid date format; must be " + YYYY_MM_DD.toLowerCase();
	private final static String INTEGER_ERROR_STATUS_TEXT = "invalid value; must be integer";
	private final static String TIME_ERROR_STATUS_TEXT = "invalid time format; must be " + HH_MM_SS.toLowerCase();
	private static final String SECONDS = "seconds  ";
	private static final String AVERAGING_TITLE_TEXT = "Averaging";
	private static final String OK_TEXT = "ok";
	private static final String QUERY_SUCCEEDED_TEXT = "query successful";
	private static final String QUERY_FAILED_TEXT = "query failed!";
	private static final String QUERY_INTERRUPTED_TEXT = "query interrupted!";
	private static final String QUERY_IN_PROGRESS_TEXT = "querying database";
	private static final String MONITOR_DATABASE_GUI_LOGGER_NAME = "MonitorDbQueryLogger";
	private static final int INSET_RIGHT = 4;
	private static final int INSET_BOTTOM = 4;
	private static final int INSET_LEFT = 4;
	private static final int INSET_TOP = 4;
	private static final int DEFAULT_TIME_AVERAGING_IN_SECONDS = 10;

	private MonitorDatabasePresentationModel presentationModel;
	private AntennaSelectionPanel antennaPanel;
	private DeviceSelectionPanel devicePanel;
	private PropertySelectionPanel propertyPanel;
	private StatusPanel statusPanel;
	private PropertiesToQueryForPanelWithStatistics propertiesToQueryForPanel;
	private JButton addButton;
	private JButton removeButton;
	private JButton queryStatisticsButton;
	private JButton getCurrentButton;
	private JButton getRawButton;
	private JPanel mainPanel;
	private JPanel fourListsPanel;
	private JPanel mainBottomPanel;
	private JPanel endDateTimePanel;
	private JPanel startDateTimePanel;
	private JFormattedTextField startDateTextField;
	private JFormattedTextField startTimeTextField;
	private JFormattedTextField endDateTextField;
	private JFormattedTextField endTimeTextField;
	private JFormattedTextField timeAveragingTextField;

	private AntennaListSelectionListener  antennaListSelectionListener;
	private DeviceListSelectionListener   deviceListSelectionListener;
	private PropertyListSelectionListener propertyListSelectionListener;
	
	private boolean previousAddButtonState;
	private boolean previousRemoveButtonState;
	private boolean previousGetRawButtonState;
	private boolean previousQueryStatisticsButtonState;
	private boolean previousGetCurrentButtonState;
	
	private Logger logger;
	
	/**
	 * Constructor.
	 */
	public MonitorDatabaseQueryFrame() {
		super(FRAME_TITLE);
		logger = Logger.getLogger(MONITOR_DATABASE_GUI_LOGGER_NAME);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.presentationModel = new MonitorDatabasePresentationModel(logger);
		initializeGUIComponents();
	}

	/**
	 * Private method which constructs the GUI.
	 */
	private void initializeGUIComponents() 
	{
		// create a panel for the status display; should be done first
		statusPanel = new StatusPanel(OK_TEXT);
		
		// create the main panels for selecting antennas/devices/properties, etc.
		createMainPanelsAndListeners();

		// make a main/outer container panel to hold all the other panels
		mainPanel = new JPanel();
		mainPanel.setLayout(new GridBagLayout());

		// constraints for placing panels
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridx = 0;
		constraints.gridy = GridBagConstraints.RELATIVE;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;

		// make a panel for the lists (and the 'add' and 'remove' buttons)
		buildFourListsPanel();

		// add the lists panel to the main/outer container panel
		mainPanel.add(fourListsPanel, constraints);

		// make the bottom panel for the text fields
		buildMainBottomPanel();

		// add the bottom panel (containing text fields for time ranges, etc)
		// to the main/outer container panel
		constraints.fill = GridBagConstraints.NONE;
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.weightx = 0.0;
		constraints.gridx = 0;
		constraints.gridy = 1;
		mainBottomPanel.setBorder(BorderFactory.createEmptyBorder(
				3 * INSET_TOP, 0, INSET_BOTTOM, INSET_RIGHT));
		mainPanel.add(mainBottomPanel, constraints);

		// add a panel for the submit button(s)
		constraints.anchor = GridBagConstraints.SOUTHEAST;
		constraints.weightx = 1.0;
		mainPanel.add(createSubmitButtonPanel(), constraints);

		// add status panel to the main panel
		constraints.gridy += 1;
		constraints.anchor = GridBagConstraints.WEST;
		mainPanel.add(statusPanel, constraints);

		this.add(mainPanel);
	}

	private JPanel createSubmitButtonPanel()
	{
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHEAST;
		constraints.gridx = GridBagConstraints.RELATIVE;
		constraints.gridy = 0;
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0;
		constraints.weighty = 0;
		constraints.insets = new Insets(0, INSET_LEFT, 0, INSET_RIGHT);
		
		JPanel submitButtonPanel = new JPanel();
		submitButtonPanel.setLayout(new GridBagLayout());
		
		getCurrentButton = new GetCurrentButton();
		submitButtonPanel.add(getCurrentButton, constraints);
		
		getRawButton = new GetRawButton();
		submitButtonPanel.add(getRawButton, constraints);
		
		queryStatisticsButton = new QueryStatisticsButton();
		submitButtonPanel.add(queryStatisticsButton, constraints);
		
		submitButtonPanel.setBorder(BorderFactory.createEmptyBorder(
				3 * INSET_TOP, 2 * INSET_LEFT, 3 * INSET_BOTTOM, INSET_RIGHT));
		return submitButtonPanel;
	}
	
	private void createMainPanelsAndListeners()	{
		// create the panel used to select the antenna(s)
		this.antennaPanel = new AntennaSelectionPanel(presentationModel);
		this.antennaListSelectionListener = new AntennaListSelectionListener();
		this.antennaPanel.list.addListSelectionListener(antennaListSelectionListener);
		this.antennaPanel.list.getModel().addListDataListener(new AddButtonListDataModelListener());

		// create the panel used to select the device(s)
		this.devicePanel = new DeviceSelectionPanel(presentationModel);
		this.deviceListSelectionListener = new  DeviceListSelectionListener();
		this.devicePanel.list.addListSelectionListener(deviceListSelectionListener);
		this.devicePanel.list.getModel().addListDataListener(new AddButtonListDataModelListener());
		
		// create the panel used to select the property/properties
		this.propertyPanel = new PropertySelectionPanel(presentationModel);
		this.propertyListSelectionListener = new  PropertyListSelectionListener();
		this.propertyPanel.list.addListSelectionListener(propertyListSelectionListener);
		this.propertyPanel.list.getModel().addListDataListener(new AddButtonListDataModelListener());
		
		// create the panel which displays the properties that will be queried 
		this.propertiesToQueryForPanel = new PropertiesToQueryForPanelWithStatistics();
		this.propertiesToQueryForPanel.addListSelectionListener(new PropertiesToQueryForListSelectionListener());
		this.propertiesToQueryForPanel.addTableModelListener(new PropertiesToQueryForTableModelListener());
	}

	private void buildMainBottomPanel() 
	{
		// create the main panel 
		mainBottomPanel = new JPanel();
		mainBottomPanel.setLayout(new GridBagLayout());

		// constraints for placement of internal panels
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 1.0;
		constraints.weighty = 0.0;
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.insets = new Insets(INSET_TOP, INSET_LEFT, INSET_BOTTOM,
				INSET_RIGHT);

		// create & add the panels for the start/end date and time
		createStartAndEndDateTimePanels();
		mainBottomPanel.add(startDateTimePanel, constraints);
		mainBottomPanel.add(endDateTimePanel, constraints);

		// create & add a panel for the averaging 
		JPanel timeAveragingPanel = new JPanel();
		timeAveragingPanel.setLayout(new BorderLayout());
		timeAveragingPanel.setBorder(BorderFactory.createTitledBorder(AVERAGING_TITLE_TEXT));
		JPanel timeAveragingInnerPanel = new JPanel();
		timeAveragingInnerPanel.setLayout(new BorderLayout());
		timeAveragingInnerPanel.setBorder(BorderFactory.createEmptyBorder(INSET_TOP,
				INSET_LEFT, INSET_BOTTOM, INSET_RIGHT));
		NumberFormat numFormat = NumberFormat.getInstance();
		numFormat.setParseIntegerOnly(true);
		timeAveragingTextField = new JFormattedTextField(numFormat);
		timeAveragingTextField.setText(Integer.toString(DEFAULT_TIME_AVERAGING_IN_SECONDS));
		timeAveragingTextField.setPreferredSize(timeAveragingTextField.getPreferredSize());
		timeAveragingTextField.setInputVerifier(new StatusUpdatingInputVerifier(INTEGER_ERROR_STATUS_TEXT, statusPanel));
		timeAveragingInnerPanel.add(timeAveragingTextField, BorderLayout.CENTER);
		JLabel timeAveragingLabel = new JLabel(SECONDS);
		timeAveragingInnerPanel.add(timeAveragingLabel, BorderLayout.NORTH);
		timeAveragingPanel.add(timeAveragingInnerPanel, BorderLayout.CENTER);
		mainBottomPanel.add(timeAveragingPanel, constraints);
	}

	private void createStartDateTimePanel(Date oneHourAgo)
	{
		// panel for the start date
		startDateTimePanel = new JPanel();
		startDateTimePanel.setLayout(new BorderLayout());
		startDateTimePanel.setBorder(BorderFactory.createTitledBorder(START_BORDER_TEXT));
		JPanel startDatePanel = new JPanel();
		startDatePanel.setLayout(new BorderLayout());
		SimpleDateFormat dateFormat = new SimpleDateFormat(YYYY_MM_DD);
		dateFormat.setLenient(false);
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		startDateTextField = 
			new JFormattedTextField(new DateFormatter(dateFormat));
		
		startDateTextField.setText(dateFormat.format(oneHourAgo));
		startDateTextField.setPreferredSize(startDateTextField.getPreferredSize());
		StatusUpdatingInputVerifier dateInputVerifier = new StatusUpdatingInputVerifier(DATE_ERROR_STATUS_TEXT, statusPanel);
		startDateTextField.setInputVerifier(dateInputVerifier);

		startDatePanel.add(startDateTextField, BorderLayout.CENTER);
		JLabel startDateLabel = new JLabel(YYYY_MM_DD.toLowerCase());
		startDatePanel.add(startDateLabel, BorderLayout.NORTH);
		startDatePanel.setBorder(BorderFactory.createEmptyBorder(INSET_TOP,
				INSET_LEFT, INSET_BOTTOM, INSET_RIGHT));

		startDateTimePanel.add(startDatePanel, BorderLayout.WEST);

		// panel for the start time
		JPanel startTimePanel = new JPanel();
		startTimePanel.setLayout(new BorderLayout());
		SimpleDateFormat timeFormat = new SimpleDateFormat(HH_MM_SS);
		timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		startTimeTextField = new JFormattedTextField(
				new DateFormatter(timeFormat));
		startTimeTextField.setText(timeFormat.format(oneHourAgo));
		startTimeTextField.setPreferredSize(startTimeTextField.getPreferredSize());
		StatusUpdatingInputVerifier timeInputVerifier = new StatusUpdatingInputVerifier(TIME_ERROR_STATUS_TEXT, statusPanel);
		startTimeTextField.setInputVerifier(timeInputVerifier);
		startTimePanel.add(startTimeTextField, BorderLayout.CENTER);
		JLabel startTimeLabel = new JLabel(HH_MM_SS.toLowerCase());
		startTimePanel.setBorder(BorderFactory.createEmptyBorder(INSET_TOP,
				INSET_LEFT, INSET_BOTTOM, INSET_RIGHT));
		startTimePanel.add(startTimeLabel, BorderLayout.NORTH);
		startDateTimePanel.add(startTimePanel, BorderLayout.EAST);		
	}
	
	private void createEndDateTimePanel(Date now)
	{
		//	panel for the end date
		endDateTimePanel = new JPanel();
		endDateTimePanel.setLayout(new BorderLayout());
		endDateTimePanel.setBorder(BorderFactory.createTitledBorder(END_BORDER_TEXT));
		JPanel endDatePanel = new JPanel();
		endDatePanel.setLayout(new BorderLayout());
		SimpleDateFormat dateFormat = new SimpleDateFormat(YYYY_MM_DD);
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		endDateTextField = 
			new JFormattedTextField(dateFormat);
		endDateTextField.setText(dateFormat.format(now));
		endDateTextField.setPreferredSize(endDateTextField.getPreferredSize());
		StatusUpdatingInputVerifier dateInputVerifier = new StatusUpdatingInputVerifier(DATE_ERROR_STATUS_TEXT, statusPanel);
		endDateTextField.setInputVerifier(dateInputVerifier);

		endDatePanel.add(endDateTextField, BorderLayout.CENTER);
		JLabel endDateLabel = new JLabel(YYYY_MM_DD.toLowerCase());
		endDatePanel.add(endDateLabel, BorderLayout.NORTH);
		endDatePanel.setBorder(BorderFactory.createEmptyBorder(INSET_TOP,
				INSET_LEFT, INSET_BOTTOM, INSET_RIGHT));

		endDateTimePanel.add(endDatePanel, BorderLayout.WEST);

		// panel for the end time
		JPanel endTimePanel = new JPanel();
		endTimePanel.setLayout(new BorderLayout());
		SimpleDateFormat timeFormat = new SimpleDateFormat(HH_MM_SS);
		timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		endTimeTextField = 
			new JFormattedTextField(new DateFormatter(timeFormat));
		StatusUpdatingInputVerifier timeInputVerifier = new StatusUpdatingInputVerifier(TIME_ERROR_STATUS_TEXT, statusPanel);
		endTimeTextField.setInputVerifier(timeInputVerifier);
		endTimeTextField.setText(timeFormat.format(now));
		endTimeTextField.setPreferredSize(endTimeTextField.getPreferredSize());

		endTimePanel.add(endTimeTextField, BorderLayout.CENTER);
		JLabel endTimeLabel = new JLabel(HH_MM_SS.toLowerCase());
		endTimePanel.add(endTimeLabel, BorderLayout.NORTH);
		endTimePanel.setBorder(BorderFactory.createEmptyBorder(INSET_TOP,
				INSET_LEFT, INSET_BOTTOM, INSET_RIGHT));
		endDateTimePanel.add(endTimePanel, BorderLayout.EAST);
	}
	
	private void createStartAndEndDateTimePanels()
	{
		// get some current date/time info to populate some default values 
		Calendar calendar = Calendar.getInstance();
		Date now = calendar.getTime();
		calendar.add(Calendar.HOUR_OF_DAY, -1);
		Date oneHourAgo = calendar.getTime();
	
		createStartDateTimePanel(oneHourAgo);
		createEndDateTimePanel(now);		
	}
	
	private void buildFourListsPanel() {
		// main panel
		fourListsPanel = new JPanel();
		fourListsPanel.setLayout(new GridBagLayout());

		// constraints used for placement of inner panels
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridx = GridBagConstraints.RELATIVE;
		constraints.gridy = 0;
		constraints.gridwidth = 1;
		constraints.fill = GridBagConstraints.VERTICAL;
		constraints.weightx = 0.0;
		constraints.weighty = 1.0;
		constraints.insets = new Insets(INSET_TOP, INSET_LEFT, INSET_BOTTOM,
				INSET_RIGHT);
		constraints.anchor = GridBagConstraints.NORTHWEST;

		// add a panel to the list panel for antenna selection
		antennaPanel.setMaximumSize(antennaPanel.getPreferredSize());
		antennaPanel.setMinimumSize(antennaPanel.getPreferredSize());
		fourListsPanel.add(antennaPanel, constraints);

		// add a panel to the list panel for device selection
		devicePanel.setMaximumSize(devicePanel.getPreferredSize());
		devicePanel.setMinimumSize(devicePanel.getPreferredSize());
		fourListsPanel.add(devicePanel, constraints);

		// add a panel to the list panel for property selection
		constraints.fill = GridBagConstraints.BOTH;
		constraints.weightx = 0.35;
		fourListsPanel.add(propertyPanel, constraints);

		// construct a panel for the 'add' and 'remove' buttons
		JPanel innerButtonPanel = new JPanel();
		innerButtonPanel.setLayout(new GridLayout(2, 1));
		addButton = new AddButton();
		innerButtonPanel.add(addButton);
		removeButton = new RemoveButton();
		innerButtonPanel.add(removeButton);
		JPanel addRemoveButtonPanel = new JPanel();
		addRemoveButtonPanel.setLayout(new BorderLayout());
		addRemoveButtonPanel.add(innerButtonPanel, BorderLayout.CENTER);
		constraints.fill = GridBagConstraints.NONE;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.weightx = 0.0;
		fourListsPanel.add(addRemoveButtonPanel, constraints);

		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.weightx = 0.65;
		constraints.gridwidth = GridBagConstraints.REMAINDER;
		fourListsPanel.add(propertiesToQueryForPanel, constraints);
	}

	private Date getStartDateWithTime()
	{
		// the GUI has separate fields for the data & time; we need to 
		// compose those two fields into a single date
		Date retDate = ((Date)startDateTextField.getValue());
		if(null != startTimeTextField.getValue()) {
			Date startTime = ((Date)startTimeTextField.getValue());
			Calendar startDateCalendar = new GregorianCalendar();
			startDateCalendar.setTime(retDate);
			Calendar startTimeCalendar = new GregorianCalendar();
			startTimeCalendar.setTime(startTime);
			startDateCalendar.set(Calendar.HOUR_OF_DAY, startTimeCalendar.get(Calendar.HOUR_OF_DAY));
			startDateCalendar.set(Calendar.MINUTE, startTimeCalendar.get(Calendar.MINUTE));
			startDateCalendar.set(Calendar.SECOND, startTimeCalendar.get(Calendar.SECOND));
			retDate = startDateCalendar.getTime();
		}	
		return retDate;
	}
	
	private Date getEndDateWithTime()
	{
		// the GUI has separate fields for the data & time; we need to 
		// compose those two fields into a single date
		Date retDate = ((Date)endDateTextField.getValue());
		if(null != endTimeTextField.getValue()) {
			Date endTime = ((Date)endTimeTextField.getValue());
			Calendar endDateCalendar = new GregorianCalendar();
			endDateCalendar.setTime(retDate);
			Calendar endTimeCalendar = new GregorianCalendar();
			endTimeCalendar.setTime(endTime);
			endDateCalendar.set(Calendar.HOUR_OF_DAY, endTimeCalendar.get(Calendar.HOUR_OF_DAY));
			endDateCalendar.set(Calendar.MINUTE, endTimeCalendar.get(Calendar.MINUTE));
			endDateCalendar.set(Calendar.SECOND, endTimeCalendar.get(Calendar.SECOND));
			retDate = endDateCalendar.getTime();
		}
		return retDate;
	}
	
	/**
	 * Validates whether the fields that are required have properly formatted information 
	 * and commits any pending edits.
	 * 
	 * @return boolean indicating validation passed (true) or failed (false)
	 */
	private boolean requiredFieldsAreValid()
	{
		boolean retVal = true;
		try {
			startDateTextField.commitEdit();
		}
		catch(ParseException ex) {
			statusPanel.statusError(DATE_ERROR_STATUS_TEXT);
			startDateTextField.requestFocus();
			retVal = false;
		}
		try {
			endDateTextField.commitEdit();
		}
		catch(ParseException ex) {
			statusPanel.statusError(DATE_ERROR_STATUS_TEXT);
			endDateTextField.requestFocus();
			retVal = false;
		}
		try {
			startTimeTextField.commitEdit();
		}
		catch(ParseException ex) {
			statusPanel.statusError(TIME_ERROR_STATUS_TEXT);
			startTimeTextField.requestFocus();
			retVal = false;
		}
		try {
			endTimeTextField.commitEdit();
		}
		catch(ParseException ex) {
			statusPanel.statusError(TIME_ERROR_STATUS_TEXT);
			endTimeTextField.requestFocus();
			retVal = false;
		}	
		try {
			timeAveragingTextField.commitEdit();
		} catch (ParseException e) {
			statusPanel.statusError(INTEGER_ERROR_STATUS_TEXT);
			timeAveragingTextField.requestFocus();
			retVal = false;
		}
		return retVal;
	}
	
	/**
	 * Private class for the 'add' button.
	 * 
	 * @author Steve Harrington
	 * 
	 */
	private class AddButton extends JButton {
		private final static String BUTTON_TEXT = "Add Query";

		/**
		 * Constructor for the add button.
		 */
		public AddButton() {
			super(BUTTON_TEXT);
			this.setEnabled(true);
			addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					QueryItem[] queryItems = presentationModel.getQueryItems();
					propertiesToQueryForPanel.appendListValues(queryItems);
				}
			});
		}
	}

	/**
	 * Private class for the 'remove' button.
	 * 
	 * @author Steve Harrington
	 */
	private class RemoveButton extends JButton 
	{
		private final static String BUTTON_TEXT = "Remove Query";

		/**
		 * Constructor for the remove button.
		 */
		public RemoveButton() {
			super(BUTTON_TEXT);
			this.setEnabled(false);
			addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					propertiesToQueryForPanel.removeSelectedRows();
				}
			});
		}
	}

	/**
	 * Button to submit the query which, when clicked, gets the values of properties and associated statistics.
	 * 
	 * @author Steve Harrington
	 */
	private class QueryStatisticsButton extends JButton
	{
		private static final String SAVE_DIALOG_TITLE = "Save Query Results to File";
		private final static String BUTTON_TEXT = "Query Statistics";
		private JFileChooser fileChooser = null;
		
		/**
		 * Constructor for the 'query statistics' button.
		 */
		public QueryStatisticsButton() 
		{
			super(BUTTON_TEXT);
			this.setEnabled(false);
			fileChooser = new JFileChooser();
			fileChooser.setDialogTitle(SAVE_DIALOG_TITLE);
			fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			initActionListener(this);	
		}

		private class QueryStatisticsSwingWorker extends SwingWorker<QueryResult, Void>
		{	
			private Date start;
			private Date end;
			private File resultsFile;
			int averagingSpan;
			
			/**
			 * Constructor.
			 * @param file the file in which to save results.
			 * @param startDate the start date for the query; properties will be queried between start/end dates.
			 * @param endDate the end date for the query; properties will be queried between start/end dates.
			 * @param span the span of the query.
			 */
			public QueryStatisticsSwingWorker(File file, Date startDate, Date endDate, int span)
			{
				this.start = startDate;
				this.end = endDate;
				this.resultsFile = file;
				this.averagingSpan = span;
			}
			
			@Override
			public QueryResult doInBackground()
			{
				QueryResult retVal = QueryResult.FAILURE;
				try {
					retVal = presentationModel.getValuesWithStatisticsAndSaveResults(resultsFile, propertiesToQueryForPanel.getQueryItems(), 
							start, end, averagingSpan);
				}
				catch(Throwable ex) {
					logger.warning("Throwable caught attempting to get values from db: \n" + ex.toString());
					ex.printStackTrace();
					retVal = QueryResult.FAILURE;
				}
				return retVal;
			}
			
			@Override
			public void done() 
			{
				QueryResult result = QueryResult.FAILURE;
				try {
					result = get();
				}
				catch(ExecutionException ex)
				{
					statusPanel.statusError(QUERY_FAILED_TEXT);
					logger.warning("ExecutionException caught attempting to get values from db: \n" + ex.toString());
				}
				catch(InterruptedException ex) {
					statusPanel.statusError(QUERY_INTERRUPTED_TEXT);
					logger.warning("InterruptedException caught attempting to get values from db: \n" + ex.toString());
				}
				
				switch(result) 
				{
					case SUCCESS:
						updateStatusForSuccessfulQuery();
						break;
					case FAILURE:
						statusPanel.statusError(QUERY_FAILED_TEXT);
						break;
				}
				finishedBackgroundJob();
			}
		}
		
		private void initActionListener(final QueryStatisticsButton button)
		{
			addActionListener(new ActionListener() 
			{
				public void actionPerformed(ActionEvent evt) 
				{			
					if(!requiredFieldsAreValid()) {
						return;
					}
					
					int saveFileResult = fileChooser.showSaveDialog(null);
					if(saveFileResult == JFileChooser.APPROVE_OPTION) 
					{
						int averagingSpan = 0;
						File file = fileChooser.getSelectedFile();
						if(((Number)timeAveragingTextField.getValue()) != null) {
							averagingSpan = ((Number)timeAveragingTextField.getValue()).intValue();
						}
						Date startDate = getStartDateWithTime();
						Date endDate = getEndDateWithTime();

						if(null != file) {
							waitForBackgroundJob();
							QueryStatisticsSwingWorker worker = new QueryStatisticsSwingWorker(file, startDate, endDate, averagingSpan);
							worker.execute();							
						} else {
							statusPanel.statusOk();
						}
					}
				}
			});
		}
	}

	/**
	 * Button to submit the query which, when clicked, gets raw values for the specified properties.
	 * 
	 * @author Steve Harrington
	 */
	private class GetRawButton extends JButton
	{
		private final static String BUTTON_TEXT = "Query Raw";
		private static final String SAVE_DIALOG_TITLE = "Save Query Results to File";
		private JFileChooser fileChooser = null;

		/**
		 * Constructor for the 'get raw' button.
		 */
		public GetRawButton() 
		{
			super(BUTTON_TEXT);
			this.setEnabled(false);
			fileChooser = new JFileChooser();
			fileChooser.setDialogTitle(SAVE_DIALOG_TITLE);
			fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			initActionListener(this);	
		}

		class GetRawSwingWorker extends SwingWorker<QueryResult, Void>
		{	
			private Date start;
			private Date end;
			private File resultsFile;
			
			/**
			 * Constructor.
			 * @param file the file in which to save results.
			 * @param startDate the start date of the query; properties will be queried between start/end dates.
			 * @param endDate the end date of the query; properties will be queried between start/end dates.
			 */
			public GetRawSwingWorker(File file, Date startDate, Date endDate)
			{
				this.start = startDate;
				this.end = endDate;
				this.resultsFile = file;
			}
			
			@Override
			public QueryResult doInBackground()
			{
				QueryResult retVal = QueryResult.FAILURE;
				try {
					retVal = presentationModel.getRawValuesAndSaveResults(resultsFile, propertiesToQueryForPanel.getSelectedQueryItems()[0], start, end);
				}
				catch(Throwable ex) {
					logger.warning("Throwable caught attempting to get values from db: \n" + ex.toString());
					ex.printStackTrace();
					retVal = QueryResult.FAILURE;
				}
				return retVal;
			}
			
			@Override
			public void done() 
			{
				QueryResult result = QueryResult.FAILURE;
				try {
					result = get();
					switch(result) 
					{
						case SUCCESS:
							updateStatusForSuccessfulQuery();
							break;
						case FAILURE:
							statusPanel.statusError(QUERY_FAILED_TEXT);
							break;
					}
				}
				catch(ExecutionException ex)
				{
					statusPanel.statusError(QUERY_FAILED_TEXT);
					logger.warning("ExecutionException caught attempting to get values from db: \n" + ex.toString());
				}
				catch(InterruptedException ex) {
					statusPanel.statusError(QUERY_INTERRUPTED_TEXT);
					logger.warning("InterruptedException caught attempting to get values from db: \n" + ex.toString());
				}
				
				finishedBackgroundJob();
			}
		}
		
		private void initActionListener(final GetRawButton button)
		{
			addActionListener(new ActionListener() 
			{
				public void actionPerformed(ActionEvent evt) 
				{	
					if(!requiredFieldsAreValid()) {
						return;
					}
					int saveFileResult = fileChooser.showSaveDialog(null);
					if(saveFileResult == JFileChooser.APPROVE_OPTION) 
					{
						File file = fileChooser.getSelectedFile();
						Date startDate = getStartDateWithTime();
						Date endDate = getEndDateWithTime();

						// "get raw" only queries for one item at a time, so we will take the selected item from the query list
						// note that for this button, only 1 item should be selected in the table, else the button would have been
						// disabled and this action would not be taking place.
						if(null != file) {			
							waitForBackgroundJob();
							GetRawSwingWorker worker = new GetRawSwingWorker(file, startDate, endDate);
							worker.execute();
						}
						else {
							statusPanel.statusOk();
						}
					}
				}
			});
		}
	}
	
	/**
	 * Button to submit the query which, when clicked, gets the current values for properties
	 * and displays the results.
	 * 
	 * @author Steve Harrington
	 */
	private class GetCurrentButton extends JButton
	{
		private final static String BUTTON_TEXT = "Get Current";
		 
		/**
		 * Constructor for the 'get current' button.
		 */
		public GetCurrentButton() 
		{
			super(BUTTON_TEXT);
			this.setEnabled(false);
			initActionListener(this);	
		}

		private void initActionListener(final GetCurrentButton button)
		{
			addActionListener(new ActionListener() 
			{
				public void actionPerformed(ActionEvent evt) 
				{		
					waitForBackgroundJob();
					GetCurrentValuesFrame resultsWindow = 
						new GetCurrentValuesFrame(presentationModel,  propertiesToQueryForPanel.getQueryItems(), logger);
					resultsWindow.setVisible(true);
					finishedBackgroundJob();
					statusPanel.statusOk();
				}
			});
		}	    
	}
	
	/**
	 * Listener used to update the device and properties panels based on
	 * selections in the antenna panel.
	 * 
	 * @author Steve Harrington
	 */
	private class AntennaListSelectionListener 
	implements ListSelectionListener 
	{
		private boolean enableState = true;

		/**
		 * Method to enable selection
		 * @param enableState boolean indicating whether to enable (true) or disable (false).
		 */
		public void enable(boolean enableState) {
			this.enableState = enableState;
		}

		public void valueChanged(ListSelectionEvent evt) {
			if (!evt.getValueIsAdjusting() && enableState) 
			{
				Object[] selectedAntennas = antennaPanel.list
				.getSelectedValues();

				presentationModel.resetAntennaFilter();
				for (Object selectedAntenna : selectedAntennas) {
					presentationModel.addAntennaFilter(selectedAntenna.toString());
				}

				deviceListSelectionListener.enable(false);
				devicePanel.update();
				deviceListSelectionListener.enable(true);

				propertyListSelectionListener.enable(false);
				propertyPanel.update();
				propertyListSelectionListener.enable(true);
				statusPanel.statusOk();
			}
		}
	}

	/**
	 * Listener used to update the properties panel based on selections in the
	 * device panel.
	 * 
	 * @author Steve Harrington
	 */
	private class DeviceListSelectionListener 
	implements ListSelectionListener {

		private boolean enableState = true;

		/**
		 * Method to enable or disable selection
		 * @param enableState boolean indicating whether to enable (true) or disable (false).
		 */
		public void enable(boolean enableState) {
			this.enableState = enableState;
		}

		public void valueChanged(ListSelectionEvent evt) {
			if (!evt.getValueIsAdjusting() && enableState) 
			{
				Object[] selectedDevices = devicePanel.list.getSelectedValues();

				presentationModel.resetDeviceFilter();
				for (Object selectedDevice : selectedDevices) {
					presentationModel.addDeviceFilter(selectedDevice.toString());
				}

				antennaListSelectionListener.enable(false);
				antennaPanel.update();
				antennaListSelectionListener.enable(true);

				propertyListSelectionListener.enable(false);
				propertyPanel.update();
				propertyListSelectionListener.enable(true);

				statusPanel.statusOk();
			}
		}
	}

	/**
	 * Listener used to enable the 'add' and 'remove' buttons when one or more
	 * properties are selected in the property list.
	 * 
	 * @author Steve Harrington
	 */
	private class PropertyListSelectionListener 
	implements ListSelectionListener {
		private boolean enableState = true;

		/**
		 * Method to enable or disable selection
		 * @param enableState boolean indicating whether to enable (true) or disable (false).
		 */
		public void enable(boolean enableState) {
			this.enableState = enableState;
		}


		public void valueChanged(ListSelectionEvent evt) {
			if (!evt.getValueIsAdjusting() && enableState) 
			{
				Object[] selectedProperties = propertyPanel.list.getSelectedValues();
				
				presentationModel.resetPropertyFilter();
				for (Object selectedProperty : selectedProperties) {
					presentationModel.addPropertyFilter(selectedProperty.toString());
				}

				antennaListSelectionListener.enable(false);
				antennaPanel.update();
				antennaListSelectionListener.enable(true);

				deviceListSelectionListener.enable(false);
				devicePanel.update();
				deviceListSelectionListener.enable(true);
				statusPanel.statusOk();
			}
		}
	}

	/**
	 * Listener used to enable the 'remove' and 'add' buttons when one or more
	 * properties are selected in the query list.
	 * 
	 * @author Steve Harrington
	 */
	private class PropertiesToQueryForListSelectionListener 
		implements ListSelectionListener {

		public void valueChanged(ListSelectionEvent evt) {
			if (!evt.getValueIsAdjusting()) {
				if (propertiesToQueryForPanel.getSelectedRowCount() > 0) 
				{
					removeButton.setEnabled(true);
				} else {
					removeButton.setEnabled(false);
				}
				if(propertiesToQueryForPanel.getSelectedRowCount() == 1)
				{
					getRawButton.setEnabled(true);
				}
				else 
				{
					getRawButton.setEnabled(false);
				}
				statusPanel.statusOk();
			}
		}
	}

	/**
	 * Used to enable/disable the 'submit query' button.
	 * 
	 * @author Steve Harrington
	 * 
	 */
	private class PropertiesToQueryForTableModelListener implements
		TableModelListener 
	{
		public void tableChanged(TableModelEvent evt) 
		{
			if (propertiesToQueryForPanel.getRowCount() > 0) 
			{
				if(propertiesToQueryForPanel.someStatisticSelected()) {
					queryStatisticsButton.setEnabled(true);
				} else {
					queryStatisticsButton.setEnabled(false);
				}
				propertiesToQueryForPanel.setEnabled(true);
				getCurrentButton.setEnabled(true);
				if(propertiesToQueryForPanel.getSelectedRowCount() == 1)
				{
					getRawButton.setEnabled(true);
				}
				else 
				{
					getRawButton.setEnabled(false);
				}
			} else {
				propertiesToQueryForPanel.setEnabled(false);
				queryStatisticsButton.setEnabled(false);
				getRawButton.setEnabled(false);
				getCurrentButton.setEnabled(false);
			}
			statusPanel.statusOk();
		}
	}

	// private class used to disable the 'add' button when nothing can be added
	private class AddButtonListDataModelListener implements ListDataListener
	{
		private void enableOrDisableAddButton()
		{
			if(antennaPanel.list.getModel().getSize() == 0 ||
				devicePanel.list.getModel().getSize() == 0 ||
				propertyPanel.list.getModel().getSize() == 0) 
			{
				addButton.setEnabled(false);
			} 
			else 
			{
				addButton.setEnabled(true);
			}
		}
		
		public void contentsChanged(ListDataEvent evt) {
			enableOrDisableAddButton();
	    }
		
	    public void intervalAdded(ListDataEvent evt) {
	    	contentsChanged(evt);
	    }
	    
	    public void intervalRemoved(ListDataEvent evt) {
	    	contentsChanged(evt);
	    }
	}
	
	private void waitForBackgroundJob() 
	{
		updateStatusForPendingQuery();
		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		
		previousAddButtonState = addButton.isEnabled();
		previousRemoveButtonState = removeButton.isEnabled();
		previousGetRawButtonState = getRawButton.isEnabled();
		previousQueryStatisticsButtonState = queryStatisticsButton.isEnabled();
		previousGetCurrentButtonState = getCurrentButton.isEnabled();
		
		getCurrentButton.setEnabled(false);
		getRawButton.setEnabled(false);
		addButton.setEnabled(false);
		removeButton.setEnabled(false);
		queryStatisticsButton.setEnabled(false);
	}
	
	private void finishedBackgroundJob()
	{
		setCursor(null);
		getCurrentButton.setEnabled(previousGetCurrentButtonState);
		getRawButton.setEnabled(previousGetRawButtonState);
		addButton.setEnabled(previousAddButtonState);
		removeButton.setEnabled(previousRemoveButtonState);
		queryStatisticsButton.setEnabled(previousQueryStatisticsButtonState);
	}
	
	private void updateStatusForSuccessfulQuery() {
		statusPanel.setStatusText(QUERY_SUCCEEDED_TEXT, Color.BLACK);
	}
	
	private void updateStatusForPendingQuery() {
		statusPanel.setStatusText(QUERY_IN_PROGRESS_TEXT, Color.BLUE);
	}
	
	/**
	 * Used for creating and displaying the GUI.
	 */
	private static void createAndShowGUI() {
		MonitorDatabaseQueryFrame frame = new MonitorDatabaseQueryFrame();
		frame.pack();
		frame.setVisible(true);
	}

	/**
	 * Main method to launch the GUI.
	 * 
	 * @param args command-line arguments
	 */
	public static void main(String[] args) {
		// Schedule a job for the event-dispatching thread:
		// creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
