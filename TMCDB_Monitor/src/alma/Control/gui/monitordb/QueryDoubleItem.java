package alma.Control.gui.monitordb;

import java.util.List;


/**
 * Contains all the information for a monitor point query item.
 * 
 * @author Jeff Kern
 */
public class QueryDoubleItem extends QueryItem
{
    private static final String TYPE_STRING = "D";

	/**
     * Constructor.
     * @param antennaName the name of the antenna.
     * @param deviceName the name of the device.
     * @param propertyName the name of the property.
     */
    public QueryDoubleItem(String antennaName, String deviceName, 
			  String propertyName)
    {
	super(antennaName, deviceName, propertyName);
    }

    /* These methods define which statistical quantites we have */
    public boolean hasMean()  {return true;}
    public boolean hasMedian(){return true;}
    public boolean hasMode()  {return false;}
    public boolean hasStandardDeviation(){return true;}
    public boolean hasMax()   {return true;}
    public boolean hasMin()   {return true;}
    
	public String meanType(){return "R";}
	public String medianType(){return "R";}
	public String stdDevType(){return "R";}
	public String nType(){return "I";}
	public String maxType(){return "R";}
	public String minType(){return "R";}

    public Stats calculateStatistics(List<?> values)
    {
    	Stats<Double> doubleStats = new Stats<Double>();
		// calculate the appropriate stats
		if(shouldCalculateMean()){
		    doubleStats.mean = Statistics.mean(values.toArray(new Double[0]));
		}
		if(shouldCalculateMedian()){
		    doubleStats.median = Statistics.median(values.toArray(new Double[0]));
		}
		if(shouldCalculateStandardDeviation()){
			doubleStats.stdDev = Statistics.stdDev(values.toArray(new Double[0]));
		}
		if(shouldCalculateN()){
			doubleStats.N = values.size();
		}
		if(shouldCalculateMin()){
			doubleStats.min = Statistics.min(values.toArray(new Double[0]));
		}
		if(shouldCalculateMax()){
			doubleStats.max = Statistics.max(values.toArray(new Double[0]));
		}
		return doubleStats;
    }
    
    public String getTypeString()
    {
    	return TYPE_STRING;
    }
}
