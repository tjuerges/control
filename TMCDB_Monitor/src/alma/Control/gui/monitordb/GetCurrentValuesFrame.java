package alma.Control.gui.monitordb;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Enumeration;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import alma.Control.gui.monitordb.MonitorDatabasePresentationModel.QueryResult;

/**
 * Class to display the current values of properties from the monitor database.
 * @author Steve Harrington
 */
public class GetCurrentValuesFrame extends JFrame 
{
	private static final int REFRESH_INTERVAL_TEXT_FIELD_NUM_COLUMNS = 4;
	/**
	 * TODO - real serialVersionUID if needed later.
	 */
	private static final long serialVersionUID = 1L;
	private static final String INTEGER_ERROR_STATUS_TEXT = "int required";
	private final String REFRESH_INTERVAL_TEXT = "Seconds";
	private final String UPDATE_TEXT = "Update every: ";
	private final String REFRESH_RATE_TITLE = "Refresh rate";
	
	private JScrollPane scrollPane;
	private JTable resultsTable;
	private JPanel mainPanel;
	private StatusPanel statusPanel;
	private JPanel refreshIntervalPanel;
	private MonitorDatabasePresentationModel presentationModel;
	private QueryItem[] propertiesToQueryFor;
	private JFormattedTextField refreshIntervalTextField;
	private Logger logger;
	private QueryThread queryThread;
	
	private static final String OK_TEXT = "connected";
	private static final String VALUE_HEADER_STRING = "Value";
	private static final String TIME_HEADER_STRING = "Time";
	private static final String PROPERTY_HEADER_STRING = "Property";
	private static final String ANTENNA_HEADER_STRING = "Location";
	private static final String DEVICE_HEADER_STRING = "Device";
	
	private final Object[] headerNames = {ANTENNA_HEADER_STRING, DEVICE_HEADER_STRING, PROPERTY_HEADER_STRING, TIME_HEADER_STRING, VALUE_HEADER_STRING };
	private final int NUM_COLUMNS = headerNames.length;
	private final static String TITLE = "Current values";
	private static final String QUERY_FAILED_TEXT = "error";
	private static final String QUERY_INTERRUPTED_TEXT = "interrupted!";
	private static final String QUERY_IN_PROGRESS_TEXT = "querying";
	private static final int DEFAULT_QUERY_INTERVAL_SECONDS = 60;
	
	/**
	 * Constructor.
	 * @param presentationModel the presentation model to use for querying the values of the properties.
	 * @param propertiesToQueryFor the properties that we want to query for.
	 * @param logger the logger to use for log messages.
	 */
	public GetCurrentValuesFrame(MonitorDatabasePresentationModel presentationModel, 
			QueryItem[] propertiesToQueryFor, Logger logger)
	{
		super(TITLE);
		this.presentationModel = presentationModel;
		this.propertiesToQueryFor = propertiesToQueryFor;
		this.logger = logger;
		
		initialize();
		queryThread = new QueryThread(DEFAULT_QUERY_INTERVAL_SECONDS);
		queryThread.start();
		
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) 
		    {
				queryThread.shouldStop();
		    }
		});
		
	}
	
	private void createRefreshIntervalPanel()
	{
		JPanel innerRefreshIntervalPanel = new JPanel(new GridBagLayout());
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridx = GridBagConstraints.RELATIVE;
		constraints.gridy = 0;
		constraints.anchor = GridBagConstraints.SOUTHWEST;
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		constraints.weighty = 1.0;
		
		JLabel updateLabel = new JLabel(UPDATE_TEXT);
		updateLabel.setBorder(BorderFactory.createEmptyBorder(0, 2 * MonitorDbUtils.INSET_RIGHT, 0, 2 * MonitorDbUtils.INSET_RIGHT));
		innerRefreshIntervalPanel.add(updateLabel, constraints);
		
		NumberFormat numFormat = NumberFormat.getInstance();
		numFormat.setParseIntegerOnly(true);
		refreshIntervalTextField = new JFormattedTextField(numFormat);
		refreshIntervalTextField.setInputVerifier(new StatusUpdatingInputVerifier(INTEGER_ERROR_STATUS_TEXT, statusPanel));
		refreshIntervalTextField.setText(Integer.toString(DEFAULT_QUERY_INTERVAL_SECONDS));
		refreshIntervalTextField.setColumns(REFRESH_INTERVAL_TEXT_FIELD_NUM_COLUMNS);
		innerRefreshIntervalPanel.add(refreshIntervalTextField, constraints);
		
		JLabel refreshLabel = new JLabel(REFRESH_INTERVAL_TEXT);
		refreshLabel.setBorder(BorderFactory.createEmptyBorder(0, 2 * MonitorDbUtils.INSET_RIGHT, 0, 2 * MonitorDbUtils.INSET_RIGHT));
		innerRefreshIntervalPanel.add(refreshLabel, constraints);
		
		constraints.weightx = 1.0;
		constraints.anchor = GridBagConstraints.SOUTHEAST;
		ApplyButton applyButton = new ApplyButton();
		innerRefreshIntervalPanel.add(applyButton, constraints);
		
		refreshIntervalPanel = new JPanel(new GridBagLayout());
		constraints.anchor = GridBagConstraints.SOUTHWEST;
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		refreshIntervalPanel.add(innerRefreshIntervalPanel, constraints);
		refreshIntervalPanel.setBorder(BorderFactory.createEmptyBorder(MonitorDbUtils.INSET_TOP, 
				MonitorDbUtils.INSET_LEFT, MonitorDbUtils.INSET_BOTTOM, MonitorDbUtils.INSET_RIGHT));
		refreshIntervalPanel.setBorder(BorderFactory.createTitledBorder(REFRESH_RATE_TITLE));
	}
	
	private void createStatusPanel()
	{
		// create a panel for the status display
		statusPanel = new StatusPanel(OK_TEXT, "Database");
	}
	
	private void createResultsTable()
	{
		resultsTable = new JTable();
		scrollPane = new JScrollPane(resultsTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setBorder(BorderFactory.createEmptyBorder(0, 0, 2 * MonitorDbUtils.INSET_BOTTOM, 0));
		
		try {
			String[][] results = presentationModel.getCurrentValues(propertiesToQueryFor);
			resultsTable.setModel(new DefaultTableModel(headerNames, results.length));
			resultsTable.setAutoCreateRowSorter(true);
								
			int columnIndex = 0;
			Enumeration<TableColumn> columnsEnum = resultsTable.getTableHeader().getColumnModel().getColumns();
			while(columnsEnum.hasMoreElements()) 
			{
				TableColumn column = columnsEnum.nextElement();
				column.setHeaderValue(headerNames[columnIndex]);
				columnIndex++;
			}

			for(int row = 0; row < results.length; row++) {
				for(int col = 0; col < NUM_COLUMNS; col++) {
					resultsTable.getModel().setValueAt(results[row][col], row, col);
				}
			}
			MonitorDbUtils.autoResizeTable(resultsTable, true);
			resultsTable.setPreferredScrollableViewportSize(resultsTable.getPreferredSize());
			statusPanel.statusOk();
		}
		catch(Throwable ex)
		{
			statusPanel.statusError(ex.getMessage());
		}
	}
	
	private void createMainPanel()
	{
		mainPanel = new JPanel();
		mainPanel.setLayout(new GridBagLayout());
	}
	
	private void initialize()
	{
		setLayout(new BorderLayout());
		
		// create main panel
		createMainPanel();

		// create status panel
		createStatusPanel();
		
		// create results table
		createResultsTable();

		// create refresh interval panel
		createRefreshIntervalPanel();
		
		// adjust height of status panel to match refresh panel, for aesthetics
		Dimension refreshDim = refreshIntervalPanel.getPreferredSize();
		Dimension statusDim = statusPanel.getPreferredSize();
		statusDim.height = refreshDim.height;
		statusPanel.setPreferredSize(statusDim);
		
		// add scroll pane to main panel
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridwidth = 2;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		mainPanel.add(scrollPane, constraints);	
		
		// add status panel to main panel
		constraints.gridwidth = 1;
		constraints.anchor = GridBagConstraints.SOUTHWEST;
		constraints.gridy = 1;
		constraints.gridx = 0;
		constraints.weighty = 0.0;
		constraints.weightx = 0.5;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		mainPanel.add(statusPanel, constraints);
		
		// add refresh interval panel to main panel
		constraints.gridx = 1;
		mainPanel.add(refreshIntervalPanel, constraints);
		
		// add main panel to frame's content pane
		getContentPane().add(mainPanel, BorderLayout.CENTER);
		pack();
	}
	
	/**
	 * Validates whether the fields that are required have properly formatted information 
	 * and commits any pending edits.
	 * 
	 * @return boolean indicating validation passed (true) or failed (false)
	 */
	private boolean requiredFieldsAreValid()
	{
		boolean retVal = true;	
		try {
			refreshIntervalTextField.commitEdit();
		} catch (ParseException e) {
			refreshIntervalTextField.requestFocus();
			retVal = false;
		}
		return retVal;
	}
	
	/**
	 * Button to apply changes to refresh rate.
	 * @author sharring
	 */
	private class ApplyButton extends JButton
	{
		/**
		 * TODO - create a meaningful serialVersionUID if necessary.
		 */
		private static final long serialVersionUID = 1L;
		private static final String APPLY_BUTTON_TEXT = "Apply";
		
		/**
		 * Constructor.
		 */
		public ApplyButton()
		{
			super(APPLY_BUTTON_TEXT);
			initActionListener();
		}
		
		private void initActionListener()
		{
			addActionListener(new ActionListener() 
			{
				public void actionPerformed(ActionEvent evt) 
				{	
					if(!requiredFieldsAreValid()) {
						return;
					}
					queryThread.setRefreshIntervalInSeconds(Integer.parseInt(refreshIntervalTextField.getText()));
					queryThread.interrupt();
					statusPanel.statusOk();
					// TODO - apply refresh rate from text field to background thread.
				}
			});
		}
	}
	
	private class QueryThread extends Thread
	{
		private boolean shouldStop;
		private int refreshIntervalInSeconds;
		private boolean queryWorkerRunning;
		
		/**
		 * Constructor.
		 * @param refreshIntervalInSeconds amount of time to sleep between refreshes, in seconds.
		 */
		public QueryThread(int refreshIntervalInSeconds)
		{
			this.refreshIntervalInSeconds = refreshIntervalInSeconds;
			this.queryWorkerRunning = false;
		}
		
		/**
		 * Setter for the refresh interval in seconds.
		 * @param refreshInt refresh interval in seconds.
		 */
		public void setRefreshIntervalInSeconds(int refreshInt)
		{
			this.refreshIntervalInSeconds = refreshInt;
		}
		
		/**
		 * Informs the thread to stop at the earliest possible opportunity.
		 *
		 */
		public void shouldStop()
		{
			this.shouldStop = true;
			this.interrupt();
		}

		private synchronized void setQueryWorkerRunning(boolean val)
		{
			this.queryWorkerRunning = val;
		}

		private synchronized boolean isQueryWorkerRunning()
		{
			return this.queryWorkerRunning;
		}
		
		public void run()
		{
			while(!shouldStop) 
			{
				// allow only one thread at a time querying.
				if(!isQueryWorkerRunning())
				{
					logger.finest("Starting thread for querying current values.");
					QuerySwingWorker queryWorker = new QuerySwingWorker();
					queryWorker.execute();
					setQueryWorkerRunning(true);
				}
				try {
					Thread.sleep(refreshIntervalInSeconds * 1000);
				}
				catch(InterruptedException ex) {
					statusPanel.statusOk();
				}
			}
			logger.finest("Stopping thread for querying current values.");
		}

		private class QuerySwingWorker extends SwingWorker<QueryResult, Void>
		{	
			/**
			 * Constructor.
			 */
			public QuerySwingWorker()
			{
			}
		
			@Override
			public QueryResult doInBackground()
			{
				QueryResult retVal = QueryResult.FAILURE;
				statusPanel.setStatusText(QUERY_IN_PROGRESS_TEXT, Color.BLUE);
				try {
					logger.finest("Querying current values.");
					String[][] results = presentationModel.getCurrentValues(propertiesToQueryFor);

					// clear the table 
					((DefaultTableModel)resultsTable.getModel()).setRowCount(0);
				
					// add new data to the table
					((DefaultTableModel)resultsTable.getModel()).setRowCount(results.length);
					for(int row = 0; row < results.length; row++) {
						for(int col = 0; col < NUM_COLUMNS; col++) {
							resultsTable.getModel().setValueAt(results[row][col], row, col);
						}
					}
		        
					statusPanel.statusOk();
					retVal = QueryResult.SUCCESS;
				}
				catch(Throwable ex) {
					logger.warning("Throwable caught attempting to get values from db: \n" + ex.toString());
					ex.printStackTrace();
					retVal = QueryResult.FAILURE;
				}
				return retVal;
			}
		
			@Override
			public void done() 
			{
				QueryResult result = QueryResult.FAILURE;
				try 
				{
					result = get();
					switch(result) 
					{
						case SUCCESS:
							statusPanel.statusOk();
							break;
						case FAILURE:
							statusPanel.statusError(QUERY_FAILED_TEXT);
							break;
					}
				}
				catch(ExecutionException ex)
				{
					statusPanel.statusError(QUERY_FAILED_TEXT);
					logger.warning("ExecutionException caught attempting to get values from db: \n" + ex.toString());
				}
				catch(InterruptedException ex) {
					statusPanel.statusError(QUERY_INTERRUPTED_TEXT);
					logger.warning("InterruptedException caught attempting to get values from db: \n" + ex.toString());
				}
				finally {
					setQueryWorkerRunning(false);
				}
			}
		}
	}
}
