package alma.Control.gui.monitordb;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashSet;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelListener;
import javax.swing.plaf.UIResource;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

/**
 * This is a panel displaying the properties that the user wishes to query
 * from the monitor database; it supports a fancier display, with checkboxes
 * for things like statistical information.
 * 
 * @author Steve Harrington
 */
@SuppressWarnings("serial")
public class PropertiesToQueryForPanelWithStatistics extends JPanel 
{
	private JTable table;
	private PropertiesToQueryTableModel tableModel;
	private static final String PROPERTIES_TO_QUERY_LABEL_TEXT = "Properties to Query";
	private JPanel tableAndLabelPanel;
	private JLabel label;
	private JScrollPane scrollPane;
	private int maxColumnWidth = 0;
	private HashSet<String> queryItemsList;

	/**
	 * Constructor.
	 */
	public PropertiesToQueryForPanelWithStatistics()
	{
		this.queryItemsList = new HashSet<String>();
		initializeGUI();
	}

	/**
	 * Returns the number of selected rows in the table; this merely delegates to the table's method.
	 * @return the number of rows that are selected in the table.
	 */
	public int getSelectedRowCount()
	{
		return table.getSelectedRowCount();
	}

	/**
	 * Returns the number of rows in the table; this merely delegates to the table's method.
	 * @return the number of rows that are in the table.
	 */
	public int getRowCount()
	{
		return table.getRowCount();
	}

	/**
	 * Gets the items in the table, and returns them as an array of query item objects.
	 * @return the items which the user wishes to query.
	 */
	public QueryItem[] getQueryItems()
	{
		QueryItem[] retVal = new QueryItem[getRowCount()];

		for(int i = 0; i < getRowCount(); i++) {
			retVal[i] = tableModel.getRow(i);
		}

		return retVal;
	}

	/**
	 * Returns whether there is a statistic selected in ANY of the checkboxes; used for enabling 'query statistics' button in main GUI frame.
	 */
	public boolean someStatisticSelected()
	{
		boolean retVal = false;
		QueryItem[] queryItems = getQueryItems();
		for(QueryItem item : queryItems) 
		{
			if(item.shouldCalculateMean() ||
				item.shouldCalculateMedian() ||
				item.shouldCalculateMode() ||
				item.shouldCalculateStandardDeviation() ||
				item.shouldCalculateN() ||
				item.shouldCalculateMax() ||
				item.shouldCalculateMin()) 
			{
				retVal = true; 
				break;
			}
		}
		return retVal;
	}

	/**
	 * Gets the query items that are selected in the table and returns them as an array of query item objects.
	 * @return the items which are selected.
	 */
	public QueryItem[] getSelectedQueryItems()
	{
		int[] selectedRows = table.getSelectedRows();
		QueryItem[] retVal = selectedRows.length > 0 ? new QueryItem[selectedRows.length] : null;
		
		int i = 0;
		for(int row: selectedRows) 
		{
			retVal[i] = tableModel.getRow(row);
			i++;
		}
		return retVal;
	}
	
	/**
	 * Removes all rows which are selected; merely passes through to the table model.
	 */
	public void removeSelectedRows()
	{
		tableModel.removeSelectedRows();
	}

	/**
	 * Adds a list selection listener to the table.
	 * @param listener the listener to add.
	 */
	public void addListSelectionListener(ListSelectionListener listener)
	{
		table.getSelectionModel().addListSelectionListener(listener);
	}

	/**
	 * Adds a table model listener to the table's model.
	 * @param listener the listener to add.
	 */
	public void addTableModelListener(TableModelListener listener)
	{
		table.getModel().addTableModelListener(listener);
	}

	/**
	 * Method to append items to the list.
	 * @param valuesToAdd
	 */
	public void appendListValues(QueryItem[] valuesToAdd) {
		for(QueryItem item : valuesToAdd) {
			if (queryItemsList.add(item.fullyQualifiedPropertyName())) {
				tableModel.addRow(item);
			}
		}
	}

	private void initializeTable()
	{
		table = new JTable();
		table.setShowHorizontalLines(true);
		table.setShowVerticalLines(true);
		tableModel = new PropertiesToQueryTableModel();
		table.getTableHeader().setReorderingAllowed(false);
		table.setModel(tableModel);
		table.getTableHeader().setVisible(false);
		for(int i = 0; i < PropertiesToQueryTableModel.NUMBER_OF_COLUMNS; i++)
		{
			if(i != PropertiesToQueryTableModel.PROPERTY_COLUMN_INDEX)
			{
				table.getColumnModel().getColumn(i).setCellRenderer(new BooleanRenderer());
			}
		}
	}

	private void initializeGUI()
	{
		initializeTable();

		tableAndLabelPanel = new JPanel();
		tableAndLabelPanel.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		constraints.gridx = 0;
		constraints.fill = GridBagConstraints.NONE;
		constraints.anchor = GridBagConstraints.NORTHWEST;
		label = new JLabel(PROPERTIES_TO_QUERY_LABEL_TEXT);
		tableAndLabelPanel.add(label, constraints);

		this.scrollPane = new JScrollPane(table);
		scrollPane.getViewport().setBackground(table.getBackground());
		constraints.weighty = 1.0;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.BOTH;
		tableAndLabelPanel.add(scrollPane, constraints);

		Dimension dimension = tableAndLabelPanel.getPreferredSize();
		dimension.width *= 1.5;
		tableAndLabelPanel.setPreferredSize(dimension);
		this.setLayout(new BorderLayout());
		this.add(tableAndLabelPanel, BorderLayout.NORTH);

		this.addComponentListener(new ResizeComponentListener());
	}

	/**
	 * Private class used to resize the scrollpane; could not find a way to have it
	 * resize automagically with normal layout managers / mechanisms.
	 * 
	 * @author sharring
	 */
	private class ResizeComponentListener extends ComponentAdapter
	{
		Dimension dimension;

		/**
		 * Constructor.
		 */
		public ResizeComponentListener()
		{
			dimension = new Dimension();
		}

		/**
		 * Invoked when the component's size changes. This custom resize logic
		 * is used to resize the scrollpane, which seemed to not resize of its
		 * own accord.
		 */
		public void componentResized(ComponentEvent e) 
		{    	
			// resize the scrollpane's panel (listAndLabelPanel)
			dimension.width = getWidth(); 
			dimension.height = getHeight();
			tableAndLabelPanel.setPreferredSize(dimension);
			scrollPane.getViewport().setPreferredSize(dimension);
			// revalidate to allow proper repaint behavior
			revalidate();
		}
	} 

	/**
	 * Private class representing the table's data.
	 * @author sharring
	 */
	private class PropertiesToQueryTableModel extends AbstractTableModel
	{
		private final static int PROPERTY_COLUMN_INDEX = 0;
		private final static int MEAN_COLUMN_INDEX = 1;
		private final static int MEDIAN_COLUMN_INDEX = 2;
		private final static int MODE_COLUMN_INDEX = 3;
		private final static int N_COLUMN_INDEX = 4;
		private final static int STANDARD_DEVIATION_COLUMN_INDEX = 5;
		private final static int MAX_COLUMN_INDEX = 6;
		private final static int MIN_COLUMN_INDEX = 7;
		private final static int NUMBER_OF_COLUMNS = 8;

		private final static String PROPERTY_COLUMN_HEADER = "Property";
		private final static String MEAN_COLUMN_HEADER = "Mean";
		private final static String MEDIAN_COLUMN_HEADER = "Median";
		private final static String MODE_COLUMN_HEADER = "Mode";
		private final static String N_COLUMN_HEADER = "N";
		private final static String STANDARD_DEVIATION_COLUMN_HEADER = "StdDev";
		private final static String MAX_COLUMN_HEADER = "Max";
		private final static String MIN_COLUMN_HEADER = "Min";

		private String[] columnNames;
		private ArrayList<QueryItem> data;
		private boolean headersInitialized = false;
		private Color tableHeaderBackground;

		/**
		 * Constructor.
		 *
		 */
		public PropertiesToQueryTableModel()
		{
			columnNames = new String[NUMBER_OF_COLUMNS];
			columnNames[PROPERTY_COLUMN_INDEX] = PROPERTY_COLUMN_HEADER;
			columnNames[MEAN_COLUMN_INDEX] = MEAN_COLUMN_HEADER;
			columnNames[MEDIAN_COLUMN_INDEX] = MEDIAN_COLUMN_HEADER;
			columnNames[MODE_COLUMN_INDEX] = MODE_COLUMN_HEADER;
			columnNames[N_COLUMN_INDEX] = N_COLUMN_HEADER;
			columnNames[STANDARD_DEVIATION_COLUMN_INDEX] = STANDARD_DEVIATION_COLUMN_HEADER;
			columnNames[MAX_COLUMN_INDEX] = MAX_COLUMN_HEADER;
			columnNames[MIN_COLUMN_INDEX] = MIN_COLUMN_HEADER;

			data = new ArrayList<QueryItem>();
		}

		public int getColumnCount() {
			return columnNames.length;
		}

		public int getRowCount() {
			return data.size();
		}

		private QueryItem getRow(int row) {
			return (QueryItem)data.get(row);
		}

		public String getColumnName(int col) 
		{
			String retVal = "";
			if(headersInitialized)
			{
				retVal = columnNames[col];
			}
			return retVal;
		}

		public Object getValueAt(int row, int col) {
			Object returnValue = null;
			QueryItem queryItem = (QueryItem) data.get(row);
			switch (col) {
			case PROPERTY_COLUMN_INDEX:
				returnValue = queryItem.fullyQualifiedPropertyName();
				break;
			case MEAN_COLUMN_INDEX:
				returnValue = queryItem.shouldCalculateMean();
				break;
			case MEDIAN_COLUMN_INDEX:
				returnValue = queryItem.shouldCalculateMedian();
				break;
			case MODE_COLUMN_INDEX:
				returnValue = queryItem.shouldCalculateMode();
				break;
			case N_COLUMN_INDEX:
				returnValue = queryItem.shouldCalculateN();
				break;
			case STANDARD_DEVIATION_COLUMN_INDEX:
				returnValue = queryItem.shouldCalculateStandardDeviation();
				break;
			case MAX_COLUMN_INDEX:
				returnValue = queryItem.shouldCalculateMax();
				break;
			case MIN_COLUMN_INDEX:
				returnValue = queryItem.shouldCalculateMin();
				break;
			}
			return returnValue;
		}

		public void setValueAt(Object value, int row, int col) 
		{
			QueryItem queryItem = (QueryItem) data.get(row);
			switch (col) {
			case PROPERTY_COLUMN_INDEX:
				/* This should never happen */
				break;
			case MEAN_COLUMN_INDEX:
				queryItem.setCalculateMean((Boolean)value);
				break;
			case MEDIAN_COLUMN_INDEX:
				queryItem.setCalculateMedian((Boolean)value);
				break;
			case MODE_COLUMN_INDEX:
				queryItem.setCalculateMode((Boolean)value);
				break;
			case N_COLUMN_INDEX:
				queryItem.setCalculateN((Boolean)value);
				break;
			case STANDARD_DEVIATION_COLUMN_INDEX:
				queryItem.setCalculateStandardDeviation((Boolean)value);
				break;
			case MAX_COLUMN_INDEX:
				queryItem.setCalculateMax((Boolean)value);
				break;
			case MIN_COLUMN_INDEX:
				queryItem.setCalculateMin((Boolean)value);
				break;
			}
			this.fireTableCellUpdated(row, col);
		}

		/**
		 * Adds a row to the table model.
		 * @param queryItem value of the row (i.e. the query item that the row will display).
		 */
		public void addRow(QueryItem queryItem) 
		{
			// add column header names if this is the first row added 
			// to the table model
			if(data.size() == 0)
			{
				addColumnHeaderNames();
			}

			// add the new row
			data.add(queryItem);

			// check the length of the text in the column (not the length in characters,
			// but the length in how it will be rendered on screen); if it is longer than the
			// maximum for the column, set the column's preferred size so that the longest string
			// will fit in the column w/o being truncated.
			String text = (String)getValueAt(data.size()-1, 0);
			Component comp = table.getCellRenderer(data.size()-1, 0).
				getTableCellRendererComponent(table, table.getValueAt(data.size()-1, 0), 
					false, false, data.size()-1, 0);
			Font font = comp.getFont();
			FontMetrics fontMetrics = comp.getFontMetrics ( font );
			int textWidth = SwingUtilities.computeStringWidth ( fontMetrics, text );
			maxColumnWidth = Math.max ( maxColumnWidth, textWidth );
			table.getColumnModel().getColumn(0).setPreferredWidth(maxColumnWidth);

			// fire an event so that things are redrawn properly
			fireTableDataChanged();
		}

		/**
		 * Removes all rows which are selected.
		 */
		public void removeSelectedRows()
		{
			int[] selectedRows = table.getSelectedRows();
			ArrayList<QueryItem> removeList = new ArrayList<QueryItem>();
			for(int row : selectedRows) {
				removeList.add(data.get(row));
				queryItemsList.remove(data.get(row).fullyQualifiedPropertyName());
			}
			data.removeAll(removeList);

			if(data.size() == 0)
			{
				removeColumnHeaderNames();
			}
			fireTableDataChanged();
		}

		/*
		 * JTable uses this method to determine the default renderer/
		 * editor for each cell.  If we didn't implement this method,
		 * then the last column would contain text ("true"/"false"),
		 * rather than a check box.
		 */
		public Class<?> getColumnClass(int c) {
			return getValueAt(0, c).getClass();
		}

		public boolean isCellEditable(int row, int col) 
		{
			boolean retVal = false;
			switch(col) 
			{
			case PROPERTY_COLUMN_INDEX:
				retVal = false;
				break;
			case MEAN_COLUMN_INDEX:
				if(data.get(row).hasMean()) {
					retVal = true;
				}
				else {
					retVal = false;
				}
				break;
			case MEDIAN_COLUMN_INDEX:
				if(data.get(row).hasMedian()) {
					retVal = true;
				}
				else {
					retVal = false;
				}
				break;
			case MODE_COLUMN_INDEX:
				if(data.get(row).hasMode()) {
					retVal = true;
				}
				else {
					retVal = false;
				}
				break;
			case N_COLUMN_INDEX:
				retVal = true;
				break;
			case STANDARD_DEVIATION_COLUMN_INDEX:
				if(data.get(row).hasStandardDeviation()) {
					retVal = true;
				}
				else {
					retVal = false;
				}
				break;
			case MAX_COLUMN_INDEX:
				if(data.get(row).hasMax()) {
					retVal = true;
				}
				else {
					retVal = false;
				}
				break;
			case MIN_COLUMN_INDEX:
				if(data.get(row).hasMin()) {
					retVal = true;
				}
				else {
					retVal = false;
				}
				break;
			}
			return retVal;
		}

		private void initHeaders()
		{
			if(!headersInitialized)
			{
				int viewColumn = table.convertColumnIndexToView(PROPERTY_COLUMN_INDEX);
				TableColumn column = table.getColumnModel().getColumn(viewColumn);
				column.setHeaderValue(PROPERTY_COLUMN_HEADER);

				viewColumn = table.convertColumnIndexToView(MEAN_COLUMN_INDEX);
				column = table.getColumnModel().getColumn(viewColumn);
				column.setHeaderValue(MEAN_COLUMN_HEADER);

				viewColumn = table.convertColumnIndexToView(MODE_COLUMN_INDEX);
				column = table.getColumnModel().getColumn(viewColumn);
				column.setHeaderValue(MODE_COLUMN_HEADER);

				viewColumn = table.convertColumnIndexToView(MEDIAN_COLUMN_INDEX);
				column = table.getColumnModel().getColumn(viewColumn);
				column.setHeaderValue(MEDIAN_COLUMN_HEADER);

				viewColumn = table.convertColumnIndexToView(N_COLUMN_INDEX);
				column = table.getColumnModel().getColumn(viewColumn);
				column.setHeaderValue(N_COLUMN_HEADER);

				viewColumn = table.convertColumnIndexToView(MAX_COLUMN_INDEX);
				column = table.getColumnModel().getColumn(viewColumn);
				column.setHeaderValue(MAX_COLUMN_HEADER);

				viewColumn = table.convertColumnIndexToView(MIN_COLUMN_INDEX);
				column = table.getColumnModel().getColumn(viewColumn);
				column.setHeaderValue(MIN_COLUMN_HEADER);

				viewColumn = table.convertColumnIndexToView(STANDARD_DEVIATION_COLUMN_INDEX);
				column = table.getColumnModel().getColumn(viewColumn);
				column.setHeaderValue(STANDARD_DEVIATION_COLUMN_HEADER);

				TableColumn tc = table.getColumnModel().getColumn(MEAN_COLUMN_INDEX);
				BooleanColumnSelectAllHeader cb = new BooleanColumnSelectAllHeader();
				tc.setHeaderRenderer(cb);

				tc = table.getColumnModel().getColumn(MEDIAN_COLUMN_INDEX);
				tc.setHeaderRenderer(new BooleanColumnSelectAllHeader());

				tc = table.getColumnModel().getColumn(MODE_COLUMN_INDEX);
				tc.setHeaderRenderer(new BooleanColumnSelectAllHeader());

				tc = table.getColumnModel().getColumn(N_COLUMN_INDEX);
				tc.setHeaderRenderer(new BooleanColumnSelectAllHeader());

				tc = table.getColumnModel().getColumn(STANDARD_DEVIATION_COLUMN_INDEX);
				tc.setHeaderRenderer(new BooleanColumnSelectAllHeader());

				tc = table.getColumnModel().getColumn(MAX_COLUMN_INDEX);
				tc.setHeaderRenderer(new BooleanColumnSelectAllHeader());

				tc = table.getColumnModel().getColumn(MIN_COLUMN_INDEX);
				tc.setHeaderRenderer(new BooleanColumnSelectAllHeader());	

				headersInitialized = true;
			}
		}

		private void removeHeaders()
		{
			if(headersInitialized)
			{
				int viewColumn = table.convertColumnIndexToView(PROPERTY_COLUMN_INDEX);
				TableColumn column = table.getColumnModel().getColumn(viewColumn);
				column.setHeaderValue("");

				viewColumn = table.convertColumnIndexToView(MEAN_COLUMN_INDEX);
				column = table.getColumnModel().getColumn(viewColumn);
				column.setHeaderValue("");

				viewColumn = table.convertColumnIndexToView(MEDIAN_COLUMN_INDEX);
				column = table.getColumnModel().getColumn(viewColumn);
				column.setHeaderValue("");

				viewColumn = table.convertColumnIndexToView(MODE_COLUMN_INDEX);
				column = table.getColumnModel().getColumn(viewColumn);
				column.setHeaderValue("");

				viewColumn = table.convertColumnIndexToView(N_COLUMN_INDEX);
				column = table.getColumnModel().getColumn(viewColumn);
				column.setHeaderValue("");

				viewColumn = table.convertColumnIndexToView(MAX_COLUMN_INDEX);
				column = table.getColumnModel().getColumn(viewColumn);
				column.setHeaderValue("");

				viewColumn = table.convertColumnIndexToView(MIN_COLUMN_INDEX);
				column = table.getColumnModel().getColumn(viewColumn);
				column.setHeaderValue("");

				viewColumn = table.convertColumnIndexToView(STANDARD_DEVIATION_COLUMN_INDEX);
				column = table.getColumnModel().getColumn(viewColumn);
				column.setHeaderValue("");

				TableColumn tc = table.getColumnModel().getColumn(MEAN_COLUMN_INDEX);
				tc.setHeaderRenderer(null);

				tc = table.getColumnModel().getColumn(MEDIAN_COLUMN_INDEX);
				tc.setHeaderRenderer(null);

				tc = table.getColumnModel().getColumn(MODE_COLUMN_INDEX);
				tc.setHeaderRenderer(null);

				tc = table.getColumnModel().getColumn(N_COLUMN_INDEX);
				tc.setHeaderRenderer(null);

				tc = table.getColumnModel().getColumn(STANDARD_DEVIATION_COLUMN_INDEX);
				tc.setHeaderRenderer(null);

				tc = table.getColumnModel().getColumn(MAX_COLUMN_INDEX);
				tc.setHeaderRenderer(null);

				tc = table.getColumnModel().getColumn(MIN_COLUMN_INDEX);
				tc.setHeaderRenderer(null);

				headersInitialized = false;
			}
		}

		private void removeColumnHeaderNames()
		{	
			removeHeaders();
			table.getTableHeader().setVisible(false);
		}

		private void addColumnHeaderNames()
		{
			initHeaders();
			table.getTableHeader().setBackground(tableHeaderBackground);
			table.getTableHeader().setVisible(true);
		}
	}

	/**
	 * Custom jtable header renderer used for columns of booleans (checkboxes) in which
	 * the header allows 'select all' functionality.
	 *  
	 * @author sharring
	 */
	@SuppressWarnings("serial")
	private class BooleanColumnSelectAllHeader extends JButton 
	implements TableCellRenderer, MouseListener 
	{
		protected boolean allChecked = false;
		protected int column;
		protected boolean mousePressed = false;
		
		/**
		 * Constructor.
		 */
		public BooleanColumnSelectAllHeader() {
			this.addItemListener(itemListener);
		}

		private void toggleBooleanColumn(int column) 
		{
			for(int x = 0; x < table.getRowCount(); x++)
			{
				switch(column) {
				case PropertiesToQueryTableModel.PROPERTY_COLUMN_INDEX:
					break;
				case PropertiesToQueryTableModel.MAX_COLUMN_INDEX:
					if(tableModel.data.get(x).hasMax()) {
						table.setValueAt(new Boolean(!this.allChecked),x, column);
					}
				break;
				case PropertiesToQueryTableModel.MIN_COLUMN_INDEX:
					if(tableModel.data.get(x).hasMin()) {
						table.setValueAt(new Boolean(!this.allChecked),x, column);
					}
				break;
				case PropertiesToQueryTableModel.MEDIAN_COLUMN_INDEX:
					if(tableModel.data.get(x).hasMedian()) {
						table.setValueAt(new Boolean(!this.allChecked),x, column);
					}
				break;				
				case PropertiesToQueryTableModel.N_COLUMN_INDEX:
					table.setValueAt(new Boolean(!this.allChecked),x, column);
				break;
				case PropertiesToQueryTableModel.STANDARD_DEVIATION_COLUMN_INDEX:
					if(tableModel.data.get(x).hasStandardDeviation()) {
						table.setValueAt(new Boolean(!this.allChecked),x, column);
					}
				break;
				case PropertiesToQueryTableModel.MODE_COLUMN_INDEX:
					if(tableModel.data.get(x).hasMode()) {
						table.setValueAt(new Boolean(!this.allChecked),x, column);
					}
				break;
				case PropertiesToQueryTableModel.MEAN_COLUMN_INDEX:
					if(tableModel.data.get(x).hasMean()) {
						table.setValueAt(new Boolean(!this.allChecked),x, column);
					}
				break;
				}
			}
			this.allChecked = !this.allChecked;
		}

		public Component getTableCellRendererComponent(
				JTable table, Object value,
				boolean isSelected, boolean hasFocus, int row, int column) 
		{
			if (table != null) {
				JTableHeader header = table.getTableHeader();
				if (header != null) {
					this.setForeground(header.getForeground());
					this.setBackground(header.getBackground());
					this.setFont(header.getFont());
					this.setBorder(header.getBorder());
					header.addMouseListener(this);
				}
			}
			setColumn(column);
			switch(column)
			{
			case PropertiesToQueryTableModel.MEAN_COLUMN_INDEX:
				this.setText(PropertiesToQueryTableModel.MEAN_COLUMN_HEADER);
				break;
			case PropertiesToQueryTableModel.MEDIAN_COLUMN_INDEX:
				this.setText(PropertiesToQueryTableModel.MEDIAN_COLUMN_HEADER);
				break;
			case PropertiesToQueryTableModel.MODE_COLUMN_INDEX:
				this.setText(PropertiesToQueryTableModel.MODE_COLUMN_HEADER);
				break;
			case PropertiesToQueryTableModel.N_COLUMN_INDEX:
				this.setText(PropertiesToQueryTableModel.N_COLUMN_HEADER);
				break;
			case PropertiesToQueryTableModel.STANDARD_DEVIATION_COLUMN_INDEX:
				this.setText(PropertiesToQueryTableModel.STANDARD_DEVIATION_COLUMN_HEADER);
				break;
			case PropertiesToQueryTableModel.MAX_COLUMN_INDEX:
				this.setText(PropertiesToQueryTableModel.MAX_COLUMN_HEADER);
				break;
			case PropertiesToQueryTableModel.MIN_COLUMN_INDEX:
				this.setText(PropertiesToQueryTableModel.MIN_COLUMN_HEADER);
				break;
			default:
				break;
			}

			setBorder(UIManager.getBorder("TableHeader.cellBorder"));
			return this;
		}

		protected void setColumn(int column) {
			this.column = column;
		}

		/**
		 * Getter for the column.
		 * @return the column (offset) as an integer.
		 */
		public int getColumn() {
			return column;
		}

		protected void handleClickEvent(MouseEvent e) {
			if (mousePressed) {
				JTableHeader header = (JTableHeader)(e.getSource());
				JTable tableView = header.getTable();
				TableColumnModel columnModel = tableView.getColumnModel();
				int viewColumn = columnModel.getColumnIndexAtX(e.getX());
				int col = tableView.convertColumnIndexToModel(viewColumn);

				if (viewColumn == this.column && e.getClickCount() == 1 && col != -1) {
					doClick();
					toggleBooleanColumn(col);
				}
				mousePressed=false;
			}
		}

		public void mouseClicked(MouseEvent e) {
			handleClickEvent(e);
			((JTableHeader)e.getSource()).repaint();
		}

		public void mousePressed(MouseEvent e) {
			mousePressed = true;
		}

		public void mouseReleased(MouseEvent e) {
		}

		public void mouseEntered(MouseEvent e) {
		}

		public void mouseExited(MouseEvent e) {
		}
	}
	
	private class BooleanRenderer extends JCheckBox implements TableCellRenderer, UIResource
	{
		private final Border noFocusBorder = new EmptyBorder(1, 1, 1, 1);

		/**
		 * Constructor.
		 */
		public BooleanRenderer() {
			super();
			setHorizontalAlignment(JLabel.CENTER);
			setBorderPainted(true);
		}

		public Component getTableCellRendererComponent(JTable table, Object value,
				boolean isSelected, boolean hasFocus, int row, int column) 
		{
			if (isSelected) {
				setForeground(table.getSelectionForeground());
				super.setBackground(table.getSelectionBackground());
			}
			else {
				setForeground(table.getForeground());
				setBackground(table.getBackground());
			}
			setSelected((value != null && ((Boolean)value).booleanValue()));

			if (hasFocus) {
				setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
			} else {
				setBorder(noFocusBorder);
			}
			
			switch(column)
			{
			case PropertiesToQueryTableModel.MAX_COLUMN_INDEX:
				if(tableModel.data.get(row).hasMax())
				{
					this.setEnabled(true);
				}
				else {
					this.setEnabled(false);
				}
				break;
			case PropertiesToQueryTableModel.MIN_COLUMN_INDEX:
				if(tableModel.data.get(row).hasMin())
				{
					this.setEnabled(true);
				}
				else {
					this.setEnabled(false);
				}
				break;
			case PropertiesToQueryTableModel.MEAN_COLUMN_INDEX:
				if(tableModel.data.get(row).hasMean())
				{
					this.setEnabled(true);
				}
				else {
					this.setEnabled(false);
				}
				break;
			case PropertiesToQueryTableModel.MEDIAN_COLUMN_INDEX:
				if(tableModel.data.get(row).hasMedian())
				{
					this.setEnabled(true);
				}
				else {
					this.setEnabled(false);
				}
				break;
			case PropertiesToQueryTableModel.MODE_COLUMN_INDEX:
				if(tableModel.data.get(row).hasMode())
				{
					this.setEnabled(true);
				}
				else {
					this.setEnabled(false);
				}
				break;
			case PropertiesToQueryTableModel.N_COLUMN_INDEX:
				this.setEnabled(true);
				break;
			case PropertiesToQueryTableModel.STANDARD_DEVIATION_COLUMN_INDEX:
				if(tableModel.data.get(row).hasStandardDeviation())
				{
					this.setEnabled(true);
				}
				else {
					this.setEnabled(false);
				}
				break;
			case PropertiesToQueryTableModel.PROPERTY_COLUMN_INDEX:
				break;
			}

			return this;
		}
	}
	
	/**
	 * For testing only!
	 */
	private static void showPanelForTesting()
	{
		JFrame frame = new JFrame("Test of PropertiesToQueryForPanelWithStatistics");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		PropertiesToQueryForPanelWithStatistics myPanel = new PropertiesToQueryForPanelWithStatistics();
		QueryItem[] array = new QueryItem[7];
		
		array[0] = new QueryBooleanItem("testAntenna1", "testDevice1", "testPropertyBoolean");
		array[1] =  new QueryFloatItem("testAntenna1", "testDevice1", "testPropertyFloat");
		array[2]= new QueryIntegerItem("testAntenna1", "testDevice1", "testPropertyInteger");
		array[3]= new QueryDoubleItem("testAntenna1", "testDevice1", "testPropertyDouble");
		array[4]= new QueryBlobItem("testAntenna1", "testDevice1", "testPropertyBlob");
		array[5]= new QueryEnumItem("testAntenna1", "testDevice1", "testPropertyEnum");
		array[6]= new QueryEnumItem("testAntenna1", "testDevice1", "testPropertyString");
		
		myPanel.appendListValues(array);
		
		frame.getContentPane().add(myPanel);
		frame.pack();
		frame.setVisible(true);
	}
	
	/**
	 * For testing only!
	 * @param args
	 */
	public static void main(String args[])
	{
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				PropertiesToQueryForPanelWithStatistics.showPanelForTesting();
			}
		});
	}
}

