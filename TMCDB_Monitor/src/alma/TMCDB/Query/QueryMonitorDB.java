/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 */

package alma.TMCDB.Query;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.logging.Logger;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.lang.Long;
import org.hsqldb.jdbc.jdbcDataSource;
import oracle.jdbc.pool.OracleDataSource;

import gnu.getopt.Getopt;
import gnu.getopt.LongOpt;

import alma.acstime.Epoch;
import alma.acs.time.TimeHelper;


public class QueryMonitorDB {

	private Logger logger;
	enum DBType {
		ORACLE,
		HSQLDB
	};
	private DBType dbType;    
	private String dbUser;
	private String dbPassword;
	private String dbUrl;
	private Connection conn;
	private Writer output;

	public QueryMonitorDB() throws IOException {
		this(null);
	}

	public QueryMonitorDB(String of) throws IOException {

		logger = Logger.getLogger(this.getClass().getName());

		if (of != null) {
			output = new FileWriter(of);
		} else {
			output = new OutputStreamWriter(System.out);
		}

		getConfiguration();

		if (dbType == DBType.HSQLDB)
			connectToHSQLDB();
		else if (dbType == DBType.ORACLE)
			connectToOracleDB();
	}

	public void disconnect() throws IOException, SQLException {        
		conn.close();
		output.close();
	}

	public void queryMonitorPoints() throws IOException, SQLException {

		Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				ResultSet.CONCUR_READ_ONLY);
		ResultSet rs;
		String query = "SELECT COUNT(*) FROM FLOATPROPERTY";
		rs = stmt.executeQuery(query);            
		if (rs.next()) {
			logger.info("Number of float monitor points: " + rs.getString(1));
			output.write(rs.getString(1)+"\n");
		} else {
			output.write("-1\n");
		}

		query = "SELECT COUNT(*) FROM DOUBLEPROPERTY";
		rs = stmt.executeQuery(query);            
		if (rs.next()) {
			logger.info("Number of double monitor points: " + rs.getString(1));
			output.write(rs.getString(1)+"\n");
		} else {
			output.write("-1\n");
		}

		query = "SELECT COUNT(*) FROM INTEGERPROPERTY";
		rs = stmt.executeQuery(query);            
		if (rs.next()) {
			logger.info("Number of integer monitor points: " + rs.getString(1));
			output.write(rs.getString(1)+"\n");
		} else {
			output.write("-1\n");
		}

		query = "SELECT COUNT(*) FROM BOOLEANPROPERTY";
		rs = stmt.executeQuery(query);            
		if (rs.next()) {
			logger.info("Number of boolean monitor points: " + rs.getString(1));
			output.write(rs.getString(1)+"\n");
		} else {
			output.write("-1\n");
		}

		query = "SELECT COUNT(*) FROM ENUMPROPERTY";
		rs = stmt.executeQuery(query);            
		if (rs.next()) {
			logger.info("Number of enum monitor points: " + rs.getString(1));
			output.write(rs.getString(1)+"\n");
		} else {
			output.write("-1\n");
		}

		query = "SELECT COUNT(*) FROM STRINGPROPERTY";
		rs = stmt.executeQuery(query);            
		if (rs.next()) {
			logger.info("Number of string monitor points: " + rs.getString(1));
			output.write(rs.getString(1)+"\n");
		} else {
			output.write("-1\n");
		}
	}

	private void getConfiguration() {

		Properties props = new Properties();

		String cwdConfLoc = "./dbConfig.properties";
		File cwdConf = new File(cwdConfLoc);

		String introotPath = System.getenv("ACSDATA");
		String introotConfLoc = introotPath + "/config/dbConfig.properties";
		File introotConf = new File(introotConfLoc);

		FileInputStream fis = null;
		if (cwdConf.exists()) {
			logger.info("Reading configuration file from: " + cwdConfLoc);
			try {
				fis = new FileInputStream(cwdConfLoc);
			} catch (FileNotFoundException ex) {
				ex.printStackTrace();
			}
		} else if (introotConf.exists()) {
			logger.info("Reading configuration file from: " + introotConfLoc);
			try {
				fis = new FileInputStream(introotConfLoc);
			} catch (FileNotFoundException ex) {
				ex.printStackTrace();
			}
		} else {
			// handle the case where there's no dbConfig.properties
		}

		try {
			props.load(fis);
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String tmcdbBackend = props.getProperty("tmcdb.db.backend");
		if (tmcdbBackend.equals("oracle")) {
			dbType = DBType.ORACLE;
			dbUser = props.getProperty("tmcdb.oracle.user");
			dbPassword = props.getProperty("tmcdb.oracle.passwd");
			dbUrl = props.getProperty("tmcdb.oracle.url");
			logger.info("dbUser=" + dbUser + 
					"; dbPassword=" + dbPassword.replaceAll(".", "*") + 
					"; dbUrl=" + dbUrl + ".");
		} else if (tmcdbBackend.equals("hsqldb")) {
			dbType = DBType.HSQLDB;
			dbUser = props.getProperty("tmcdb.hsqldb.user");
			dbPassword = props.getProperty("tmcdb.hsqldb.passwd");
			dbUrl = props.getProperty("tmcdb.hsqldb.url");            
			logger.info("dbUser=" + dbUser + 
					"; dbPassword=" + dbPassword.replaceAll(".", "*") + 
					"; dbUrl=" + dbUrl + ".");
		} else {
			// Bad setting
		}        
	}

	private void connectToOracleDB() {

		OracleDataSource ds = null;
		try {
			ds = new OracleDataSource();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		ds.setURL(dbUrl);
		ds.setUser(dbUser);
		ds.setPassword(dbPassword);
		try {
			conn = ds.getConnection();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}

	private void connectToHSQLDB() {

		jdbcDataSource ds = null;
		ds = new jdbcDataSource();
		ds.setDatabase(dbUrl);
		ds.setUser(dbUser);
		ds.setPassword(dbPassword);
		try {
			conn = ds.getConnection();
			conn.setAutoCommit(false);
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	private void queryAssemblies() throws IOException, SQLException{

		Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				ResultSet.CONCUR_READ_ONLY);
		ResultSet rs;
		//check if we need ASSEMBLYID, ASSEMBLYNAME or SERIALNUMBER
		String query = "SELECT SERIALNUMBER FROM ASSEMBLY";
		rs = stmt.executeQuery(query);
		output.write("Assemblies:\n");
		while (rs.next()) {
			output.write(rs.getString(1)+"\n");
		} 


	}
	private void queryProperties(String serialNumber)throws IOException, SQLException{
		Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				ResultSet.CONCUR_READ_ONLY);
		ResultSet rs;
		String assemblyName=null;
		String query = "SELECT ASSEMBLYNAME FROM ASSEMBLY WHERE SERIALNUMBER='"+serialNumber+"'";
		rs = stmt.executeQuery(query);
		if(rs.next()) {
			assemblyName = rs.getString(1);
		}else{
			//error
		}

		query = "SELECT PROPERTYNAME FROM PROPERTYTYPE WHERE ASSEMBLYNAME='"+assemblyName+"'";
		rs = stmt.executeQuery(query);
		output.write("Properties:\n");
		while (rs.next()) {
			output.write(rs.getString(1)+"\n");
		} 


	}
	private void queryPropertyValues(String serialNumber, String propertyName, long startTime, long endTime)throws IOException, SQLException{
		Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				ResultSet.CONCUR_READ_ONLY);
		ResultSet rs;
		String tableName=null, assemblyName=null, propertyTypeId=null, assemblyId=null;
		String 	query = "SELECT ASSEMBLYNAME, ASSEMBLYID FROM ASSEMBLY WHERE SERIALNUMBER='"+serialNumber+"'";
		rs = stmt.executeQuery(query);
		if(rs.next()) {
			assemblyName = rs.getString(1);
			assemblyId = rs.getString(2);
		}else{
			//error
		}
		query = "SELECT PROPERTYTYPEID FROM PROPERTYTYPE WHERE ASSEMBLYNAME='"+assemblyName+"' AND PROPERTYNAME='"+propertyName+"'";
		rs = stmt.executeQuery(query);
		if (rs.next()) {
			propertyTypeId = rs.getString(1);
		} else {
			//wrong propertytypeid
		}
		query = "SELECT TABLENAME FROM PROPERTYTYPE WHERE PROPERTYTYPEID='"+propertyTypeId+"'";
		rs = stmt.executeQuery(query);
		if (rs.next()) {
			tableName = rs.getString(1);
		} else {
			//wrong propertytypeid
		}
		String type=null;
		if(tableName.equalsIgnoreCase("FloatProperty"))type="R";
		else if(tableName.equalsIgnoreCase("DoubleProperty"))type="D";
		else if(tableName.equalsIgnoreCase("BooleanProperty"))type="B";
		else if(tableName.equalsIgnoreCase("StringProperty"))type="A";
		else if(tableName.equalsIgnoreCase("IntegerProperty"))type="I";
		else if(tableName.equalsIgnoreCase("EnumProperty"))type="A";
		else if(tableName.equalsIgnoreCase("BLOBProperty"))type="A";


		query = "SELECT SAMPLETIME, VALUE FROM "+tableName+" WHERE PROPERTYTYPEID='"+propertyTypeId+"' AND ASSEMBLYID='"+assemblyId+"' AND SAMPLETIME>'"+startTime+"' AND SAMPLETIME<'"+endTime+"'";
		rs = stmt.executeQuery(query);
		output.write("TIME\t\t\t"+propertyName+"\n");
		output.write("A\t\t\t"+type+"\n");
		while(rs.next()){
			if(type.equalsIgnoreCase("A")){
				output.write(sql2isoTime(new Long(rs.getString(1)))+"\t'"+rs.getString(2)+"'\n");
			} else {
				output.write(sql2isoTime(new Long(rs.getString(1)))+"\t"+rs.getString(2)+"\n");
			}
		}	

	}
	private static void printUsage(){
		System.out.println("Usage: QueryMonitorDB -c 'command' [options]\n");
		System.out.println("		Available commands:");
		System.out.println("					getAssemblies: list all assemblies.");
		System.out.println("					getProperties: list all properties.");
		System.out.println("							must specify: 	-a, --assembly)");
		System.out.println("					getValues: list values of selected properties.");
		System.out.println("							must specify: 	-a, --assembly");
		System.out.println("									-p, --property");
		System.out.println("									-s, --startTime");
		System.out.println("									-e, --endTime\n\n");
		System.out.println("		Options:");
		System.out.println("			-a, --assembly: the assembly you want, as listed with getAssemblies command.");
		System.out.println("			-p, --property: the property you want, as listed with getProperties command.");
		System.out.println("			-s, --startTime: starting time of the values you want, in format YYYY-MM-DDTHH:MM:SS.mmm");
		System.out.println("			-e, --endTime: ending time of the values you want, in format YYYY-MM-DDTHH:MM:SS.mmm");
		System.out.println("			-f, --filename: send data to a file instead of standard output.\n\n");
		System.out.println("		Example:");
		System.out.println("			QueryMonitorDB -c getAssemblies");
		System.out.println("			QueryMonitorDB -c getProperties -a CONTROL/DV01/FLOOG");
		System.out.println("			QueryMonitorDB --command getValues --assembly CONTROL/DV01/FLOOG --property FREQ --startTime 2007-09-25T00:00:00.000 --endTime 2007-09-25T22:21:30.000");

	} 
	private static long iso2sqlTime(String isoTime){
		SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		//TimeStamp time=new Timestamp(isoFormat.parse(isoTime).getTime());
		Epoch acsTime= new Epoch(0);
		try{
			acsTime = TimeHelper.javaToEpoch(isoFormat.parse(isoTime).getTime());
		}catch(java.text.ParseException e){
			e.printStackTrace();
		}
		return (100L * acsTime.value - 8712576000000000000L);
	}
	private static String sql2isoTime(long sqlTime){
		Epoch acsTime=new Epoch((sqlTime/100L + 87125760000000000L));//carefull with overflow!!!divide first, then add
		String isoTime=TimeHelper.getUTCDate(acsTime);
		return isoTime;
	}
	public static void main(String[] args) {

		QueryMonitorDB mqt=null;
		int c;
		String arg;
		LongOpt[] longopts = new LongOpt[7];
		longopts[0] = new LongOpt("help", LongOpt.NO_ARGUMENT, null, 'h');
		longopts[1] = new LongOpt("filename", LongOpt.REQUIRED_ARGUMENT, null, 'f'); 
		longopts[2] = new LongOpt("assembly", LongOpt.REQUIRED_ARGUMENT, null, 'a');
		longopts[3] = new LongOpt("property", LongOpt.REQUIRED_ARGUMENT, null, 'p');
		longopts[4] = new LongOpt("command", LongOpt.REQUIRED_ARGUMENT, null, 'c');
		longopts[5] = new LongOpt("startTime", LongOpt.REQUIRED_ARGUMENT, null, 's');
		longopts[6] = new LongOpt("endTime", LongOpt.REQUIRED_ARGUMENT, null, 'e');

		Getopt g = new Getopt("QueryMonitorDB", args, "hf:a:p:c:s:e:", longopts);
		g.setOpterr(true); // We'll do our own error handling
		String command=null;
		String filename=null;
		String assemblyId=null;
		String property=null;
		String startTime=null;
		String endTime=null;
		while ((c = g.getopt()) != -1)
			switch (c)
			{
				case 'h':
					printUsage();
					return;
				case 'f':
					filename=g.getOptarg();
					break;
				case 'a':
					assemblyId=g.getOptarg();
					break;
				case 'p':
					property=g.getOptarg();
					break;
				case 'c':
					command=g.getOptarg();	
					break;
				case 's':
					startTime=g.getOptarg();
					break;
				case 'e':
					endTime=g.getOptarg();
					break;


			}

		try{	
			if(command==null){
				printUsage();
				return;
			}
			if(command.equalsIgnoreCase("getAssemblies")){
				if(filename==null)
					mqt = new QueryMonitorDB();
				else
					mqt = new QueryMonitorDB(filename);
				mqt.queryAssemblies();		
			}else if(command.equalsIgnoreCase("getProperties")){
				if(assemblyId==null){
					printUsage();
					return;
				}else{
					if(filename==null)
						mqt = new QueryMonitorDB();
					else
						mqt = new QueryMonitorDB(filename);
					mqt.queryProperties(assemblyId);
				}
			}else if(command.equalsIgnoreCase("getValues")){
				if(property==null||assemblyId==null||startTime==null||endTime==null){
					printUsage();
					return;
				}else{
					if(filename==null)
						mqt = new QueryMonitorDB();
					else
						mqt = new QueryMonitorDB(filename);
					mqt.queryPropertyValues(assemblyId,property,iso2sqlTime(startTime),iso2sqlTime(endTime));
				}
			}else{
				printUsage();
				return;
			}
		}catch(IOException ex) {
			ex.printStackTrace();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		try {
			if(mqt!=null)mqt.disconnect();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}
}
