/*
 * ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

/**
 * @author  srankin
 * @version $Id$
 * @since
 */

package alma.TMCDB.Query;

import alma.hla.runtime.asdm.types.ArrayTime;
import alma.TMCDB.TimeValue;
import alma.TMCDB.TMCDB;
import alma.TmcdbErrType.wrappers.AcsJTmcdbErrTypeEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbInvalidDataTypeEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbSqlEx;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/*
 * This class is currently a mess!
 * 
 * Most code was designed following the current (2007-10) design of the TMCDB.
 * Parts have been quickly hacked to use data now at the ATF, which is collected
 * by software designed before the current TMCDB design was completed. All of
 * this code must be updated to reflect design changes driven by ACS 7.0 and the
 * ACACORR FBT.
 * 
 * TODO: Review the ACACORR FBT TMCDB work and correct this class as required.
 * 
 * TMCDBCommonQueries provides what we expect to be commonly used queries to
 * collect data about the current configuration of the telescope or the past or
 * current state of assemblies in the telescope.
 * 
 * To set up an instance of this class, do the following:
 * 
 *     TMCDBCommonQueries tmcdb = new TMCDBCommonQueries(); 
 *     tmcdb.connectToDB(); 
 *     try {
 *         tmcdb.initialize("TMCDB Configuration Name"); 
 *     } catch (AcsJTmcdbErrTypeEx ex) { 
 *         // TODO: Handle any exceptions that may occur 
 *     }
 * 
 * where "TMCDB Configuration Name" is the name of a configuration already in
 * the TMCDB. After this you may call query methods.
 * 
 * When done with the tmcdb instance created above:
 * 
 *     tmcdb.terminate();
 * 
 * This is necessary to release the database connection and clean up the super
 * classes.
 */
public class TMCDBCommonQueries extends TMCDB {

    //
    // Initialization state must be tracked because this class uses a database connection,
    // which must be initialized before use and released when no longer required.
    private enum InitializationState {
        NotReady, Ready, Terminated
    };

    protected PreparedStatement antennaNamesQuery;
    protected PreparedStatement assemblyIdFromAssemblyNameAntennaNameQuery;
    protected PreparedStatement assemblyIdFromAssemblySNQuery;
    protected PreparedStatement assemblyIdsFromBaseElementIdQuery;
    protected PreparedStatement assemblyIdsWithNameQuery;
    protected PreparedStatement assemblyIdsQuery;
    protected PreparedStatement assemblyNameFromAssemblyIdQuery;
    protected PreparedStatement assemblyNamesQuery;
    protected PreparedStatement assemblySNsQuery;
    protected PreparedStatement baseElementIdsFromAntennaNameQuery;
    protected PreparedStatement propertyDataTypeFromPropertyNameQuery;
    protected PreparedStatement propertyIdFromPropertyNameQuery;
    protected PreparedStatement propertyNameFromAssemblyNameQuery;

    protected PreparedStatement lastFloatMonitorDataTime;
    protected PreparedStatement lastFloatMonitorDataValue;
    protected PreparedStatement lastDoubleMonitorDataTime;
    protected PreparedStatement lastDoubleMonitorDataValue;
    protected PreparedStatement lastIntegerMonitorDataTime;
    protected PreparedStatement lastIntegerMonitorDataValue;
    protected PreparedStatement lastStringMonitorDataTime;
    protected PreparedStatement lastStringMonitorDataValue;
    protected PreparedStatement lastBooleanMonitorDataTime;
    protected PreparedStatement lastBooleanMonitorDataValue;
    protected PreparedStatement lastEnumMonitorDataTime;
    protected PreparedStatement lastEnumMonitorDataValue;

    private InitializationState tmcdbState;

    //
    // Instance life cycle methods

    public TMCDBCommonQueries() {
        super();
        tmcdbState = InitializationState.NotReady;
    }

    /**
     * Connect to the back end database.
     * 
     * This MUST be called after the constructor, but before initialize().
     */
    public void connectToDB() {
        try {
            connectDB();
        } catch (AcsJTmcdbErrTypeEx ex) {
            logger.severe(
                "Failed to connect to DB in TMCDBCommonQueries.connectToDB() - not recoverable - please report a bug.");
            ex.printStackTrace();
        }
    }

    /**
     * Initialize instance for the named TMCDB configuration.
     * 
     * This MUST be called after connectDB(), but before any other methods.
     */
    public void initialize(String config) throws AcsJTmcdbErrTypeEx {
        setConfigurationName(config);
        super.initialize();
        prepareStatements();
        tmcdbState = InitializationState.Ready;
    }

    public String readConfigName() {
	Properties props = new Properties();

	String cwdConfLoc = "./dbConfig.properties";
	File cwdConf = new File(cwdConfLoc);

	String introotPath = System.getenv("ACSDATA");
	String introotConfLoc = introotPath + "/config/dbConfig.properties";
	File introotConf = new File(introotConfLoc);

	FileInputStream fis = null;
	if (cwdConf.exists()) {
	    logger.info("Reading configuration file from: " + cwdConfLoc);
	    try {
	        fis = new FileInputStream(cwdConfLoc);
	    } catch (FileNotFoundException ex) {
	        ex.printStackTrace();
	    }
	} else if (introotConf.exists()) {
	    logger.info("Reading configuration file from: " + introotConfLoc);
	    try {
	        fis = new FileInputStream(introotConfLoc);
	    } catch (FileNotFoundException ex) {
	        ex.printStackTrace();
	    }
	} else {
	// handle the case where there's no dbConfig.properties
	}

	try {
	    props.load(fis);
	} catch (IOException ex) {
	    ex.printStackTrace();
	}

	return props.getProperty("tmcdb.confname");
    }

    /**
     * Terminate this instance of the TMCDB, disconnect from the database, and
     * release superclass resources.
     * 
     * This MUST be called when done with this class.
     */
    public void terminate() {

        if (tmcdbState == InitializationState.Terminated)
            return;

        tmcdbState = InitializationState.Terminated;
        try {
            super.release();
        } catch (AcsJTmcdbErrTypeEx ex) {
            logger.severe(
                "super.release() threw a TMCDBException in TMCDBCommonQueries.terminate() - please report a bug.");
            ex.printStackTrace();
        }
    }

    /**
     * Call terminate() explicitly.  This is only a weak safety net.
     */
    protected void finalize() throws Throwable {
        try {
            terminate();
        } finally {
            super.finalize();
        }
    }

    /**
     * Before doing ANYTHING with the TMCDB, ensure it is ready for use.
     */
    private void ensureTmcdbReady() {
        if (tmcdbState != InitializationState.Ready)
            throw new IllegalStateException("TMCDBCommonQueries instance not ready for queries.");
    }

    /**
     * Initialize all prepared statements.
     * 
     * Where ConfigurationId matters, use the stored ConfigurationId.
     */
    private void prepareStatements() {

        // TODO: Audit all queries and verify ConfigurationId is used where
        // available.
        try {
            antennaNamesQuery = conn
                    .prepareStatement("SELECT AntennaName FROM Antenna WHERE ConfigurationId = ?");
            antennaNamesQuery.setInt(1, configurationId);
            assemblyIdFromAssemblySNQuery = conn
                    .prepareStatement("SELECT AssemblyId FROM Assembly WHERE ConfigurationId = ? AND SerialNumber = ?");
            assemblyIdFromAssemblySNQuery.setInt(1, configurationId);
            assemblyIdsQuery = conn
                    .prepareStatement("SELECT AssemblyId FROM Assembly WHERE ConfigurationId = ?");
            assemblyIdsQuery.setInt(1, configurationId);
            assemblyIdsWithNameQuery = conn
                    .prepareStatement("SELECT AssemblyId FROM Assembly WHERE ConfigurationId = ? AND AssemblyName = ?");
            assemblyIdsWithNameQuery.setInt(1, configurationId);
            baseElementIdsFromAntennaNameQuery = conn
                    .prepareStatement("SELECT BaseElementId FROM BaseElement WHERE ConfigurationId = ? AND BaseElementName = ?");
            baseElementIdsFromAntennaNameQuery.setInt(1, configurationId);
            assemblyIdsFromBaseElementIdQuery = conn
                    .prepareStatement("SELECT AssemblyId FROM BaseElementAssemblyList WHERE BaseElementId = ?");
            assemblyNameFromAssemblyIdQuery = conn
                    .prepareStatement("SELECT AssemblyName FROM Assembly WHERE AssemblyId = ?");
            assemblyNamesQuery = conn
                    .prepareStatement("SELECT AssemblyName FROM Assembly WHERE ConfigurationId = ?");
            assemblyNamesQuery.setInt(1, configurationId);
            assemblySNsQuery = conn
                    .prepareStatement("SELECT SerialNumber FROM Assembly WHERE ConfigurationId = ?");
            assemblySNsQuery.setInt(1, configurationId);
            propertyNameFromAssemblyNameQuery = conn
                    .prepareStatement("SELECT PropertyName FROM PropertyType WHERE AssemblyName = ?");
            propertyDataTypeFromPropertyNameQuery = conn
                    .prepareStatement("SELECT DataType FROM PropertyType WHERE PropertyName = ?");
            propertyIdFromPropertyNameQuery = conn
                    .prepareStatement("SELECT PropertyTypeId FROM PropertyType WHERE PropertyName = ? AND AssemblyName = ?");

            lastFloatMonitorDataTime = conn
                    .prepareStatement("SELECT MAX(SampleTime) FROM FloatProperty WHERE AssemblyId = ? AND PropertyTypeId = ?");
            lastFloatMonitorDataValue = conn
                    .prepareStatement("SELECT Value FROM FloatProperty WHERE AssemblyId = ? AND PropertyTypeId = ? AND SampleTime = ?");
            lastDoubleMonitorDataTime = conn
                    .prepareStatement("SELECT MAX(SampleTime) FROM DoubleProperty WHERE AssemblyId = ? AND PropertyTypeId = ?");
            lastDoubleMonitorDataValue = conn
                    .prepareStatement("SELECT Value FROM DoubleProperty WHERE AssemblyId = ? AND PropertyTypeId = ? AND SampleTime = ?");
            lastIntegerMonitorDataTime = conn
                    .prepareStatement("SELECT MAX(SampleTime) FROM IntegerProperty WHERE AssemblyId = ? AND PropertyTypeId = ?");
            lastIntegerMonitorDataValue = conn
                    .prepareStatement("SELECT Value FROM IntegerProperty WHERE AssemblyId = ? AND PropertyTypeId = ? AND SampleTime = ?");
            lastStringMonitorDataTime = conn
                    .prepareStatement("SELECT MAX(SampleTime) FROM StringProperty WHERE AssemblyId = ? AND PropertyTypeId = ?");
            lastStringMonitorDataValue = conn
                    .prepareStatement("SELECT Value FROM StringProperty WHERE AssemblyId = ? AND PropertyTypeId = ? AND SampleTime = ?");
            lastBooleanMonitorDataTime = conn
                    .prepareStatement("SELECT MAX(SampleTime) FROM BooleanProperty WHERE AssemblyId = ? AND PropertyTypeId = ?");
            lastBooleanMonitorDataValue = conn
                    .prepareStatement("SELECT Value FROM BooleanProperty WHERE AssemblyId = ? AND PropertyTypeId = ? AND SampleTime = ?");
            lastEnumMonitorDataTime = conn
                    .prepareStatement("SELECT MAX(SampleTime) FROM EnumProperty WHERE AssemblyId = ? AND PropertyTypeId = ?");
            lastEnumMonitorDataValue = conn
                    .prepareStatement("SELECT Value FROM EnumProperty WHERE AssemblyId = ? AND PropertyTypeId = ? AND SampleTime = ?");

        } catch (SQLException err) {
            logger
                    .warning("SQLException in alma.TMCDB.Query.TMCDBCommonQueries.prepareStatements() "
                            + err.toString());
        }
    }

    //
    // Query methods

    /**
     * Get all antenna names from the TMCDB.
     * 
     * @return All antenna names found.
     */
    public List<String> getAntennaNames() {

        ensureTmcdbReady();
        List<String> names = new ArrayList<String>();

        // try {
        // ResultSet result = antennaNamesQuery.executeQuery();
        // while (result.next()) {
        // names.add(result.getString("AntennaName"));
        // }
        // } catch (SQLException err) {
        // logger.warning("SQLException in
        // alma.TMCDB.Query.TMCDBCommonQueries.getAntennaNames() - please report
        // a bug.");
        // }

        /*
         * The following code was quickly hacked together to deal with missing
         * configuration data in the current (2007-11-13) TMCDB at the ATF.
         * 
         * The only way to get a list of antennas at the ATF now is to depend on
         * the convention that assembly serial numbers have the form "CONTROL/<antenna
         * name>/<assembly>" where <antenna name> is the name of the antenna
         * containing the assembly, and <assembly> is the name of the assembly
         * (device).
         */

        List<String> assemblySNs = getAssemblySNs();
        String antennaName;
        for (String assemblySN : assemblySNs) {
            antennaName = getAntennaFromSN(assemblySN);
            if (!names.contains(antennaName))
                names.add(antennaName);
        }
        return names;
    }

    /**
     * Get the antenna name from an assembly serial number.
     * 
     * CAUTION: This is based on the current conventions for assembly serial
     * numbers. This will break when we start using serial numbers from AMBSIs.
     * When this happens, we must modify all code that uses this method.
     * 
     * @param serialNumber
     *            of the assembly
     * @return the antenna name embedded in the serialNumber
     */
    String getAntennaFromSN(String serialNumber) {
        int antennaNameIndex = 1;
        String[] snParts = serialNumber.split("/");
        try {
            return snParts[antennaNameIndex];
        } catch (ArrayIndexOutOfBoundsException err) {
            return "";
        }
    }

    /**
     * Get the assembly name from an assembly serial number.
     * 
     * CAUTION: This is based on the current conventions for assembly serial
     * numbers. This will break when we start using serial numbers from AMBSIs.
     * When this happens, we must modify all code that uses this method.
     * 
     * @param serialNumber
     *            of the assembly
     * @return the assembly name embedded in the serialNumber
     * 
     * Note: The returned assembly name may contain extra characters that
     * uniquely identify this assembly in a collection of similar assemblies.
     */
    String getAssemblyFromSN(String serialNumber) {
        int assemblyNameIndex = 2;
        String[] snParts;

	if (serialNumber.contains("FrontEnd"))
	    snParts = serialNumber.split("/",3);
	else
	    snParts = serialNumber.split("/");

        try {
            return snParts[assemblyNameIndex];
        } catch (ArrayIndexOutOfBoundsException err) {
            return "";
        }
    }

    /**
     * Get the ID for the given assembly serial number.
     * 
     * @param serialNumber
     *            the assembly to search for
     * @return the ID of the assembly found
     */
    public int getAssemblyIDforSN(String serialNumber) {

        ensureTmcdbReady();
        List<Integer> ids = new ArrayList<Integer>();
        try {
            assemblyIdFromAssemblySNQuery.setString(2, serialNumber);
            ResultSet result = assemblyIdFromAssemblySNQuery.executeQuery();
            while (result.next())
                ids.add(result.getInt("AssemblyId"));
        } catch (SQLException err) {
            logger.warning(
                "SQLException in alma.TMCDB.Query.TMCDBCommonQueries.getAssembyIds() - please report a bug.");
        }
        if (ids.size() == 0)
            return 0;
        if (ids.size() == 1)
            return ids.get(0);
        logger.warning(
            "Found more than 1 assembly ID for assembly serial number in alma.TMCDB.Query.TMCDBCommonQueries.getAssemblyIDforSN(serialNumber) - possible TMCDB issue or bug in this method.");
        return ids.get(0);
    }

    /**
     * Get the ID for the given assembly name in the given antenna.
     * 
     * @param assemblyName
     *            assembly to search for
     * @param antennaName
     *            antenna to search in
     * @return the ID of the assembly found
     */
    public int getAssemblyId(String assemblyName, String antennaName) {

        ensureTmcdbReady();
        List<Integer> assemblyIdsInAntenna = getAssemblyIdsInAntenna(antennaName);
        List<Integer> assemblyIdsForAssemblyName = getAssemblyIdsWithName(assemblyName);
        List<Integer> intersection = new ArrayList<Integer>();
        for (int idInAntenna : assemblyIdsInAntenna)
            if (assemblyIdsForAssemblyName.contains(idInAntenna))
                intersection.add(idInAntenna);

        if (intersection.size() == 0)
            return 0;
        if (intersection.size() == 1)
            return intersection.get(0);
        logger.warning(
            "Found more than 1 assembly ID for assembly name in antenna in alma.TMCDB.Query.TMCDBCommonQueries.getAssemblyId(assemblyName, antennaName) - possible TMCDB issue or bug in this method.");
        return intersection.get(0);
    }

    /**
     * Get all assembly IDs from the TMCDB.
     * 
     * @return All assembly IDs found.
     */
    public List<Integer> getAssemblyIds() {

        ensureTmcdbReady();
        List<Integer> ids = new ArrayList<Integer>();
        try {
            ResultSet result = assemblyIdsQuery.executeQuery();
            while (result.next()) {
                ids.add(result.getInt("AssemblyId"));
            }
        } catch (SQLException err) {
            logger.warning(
                "SQLException in alma.TMCDB.Query.TMCDBCommonQueries.getAssembyIds() - please report a bug.");
        }
        return ids;
    }

    /**
     * Get all assembly IDs from the given base element ID.
     * 
     * @param baseElementId
     *            the ID of the base element to look for.
     * @return all assembly IDs found for the given base element ID.
     */
    List<Integer> getAssemblyIds(int baseElementId) {

        ensureTmcdbReady();
        List<Integer> ids = new ArrayList<Integer>();
        try {
            assemblyIdsFromBaseElementIdQuery.setInt(1, baseElementId);
            ResultSet result = assemblyIdsFromBaseElementIdQuery.executeQuery();
            while (result.next()) {
                ids.add(result.getInt("AssemblyId"));
            }
        } catch (SQLException err) {
            logger.warning(
                "Caught SQLException in alma.TMCDB.Query.TMCDBCommonQueries.getAssemblyId(baseElementId) "
                + err.toString());
        }
        return ids;
    }

    /**
     * Get all assembly IDs contained in the given antenna.
     * 
     * @param antennaName
     *            the name of the antenna to check.
     * @return all assembly IDs found for the given antenna name.
     */
    List<Integer> getAssemblyIdsInAntenna(String antennaName) {

        /*
         * The following code was quickly hacked together to deal with missing
         * configuration data in the current (2007-11-13) TMCDB at the ATF.
         */

        ensureTmcdbReady();
        List<Integer> assemblyIds = new ArrayList<Integer>();
        // List<Integer> baseElementIds = getBaseElementIds(antennaName);
        // for (int baseElementId: baseElementIds) {
        // assemblyIds.addAll(getAssemblyIds(baseElementId));
        // }
        List<String> assemblySNs = getAssemblySNs();
        String antName;
        for (String assemblySN : assemblySNs) {
            antName = assemblySN.split("/")[1];
            if (antName.equals(antennaName)) {
                assemblyIds.add(getAssemblyIDforSN(assemblySN));
            }
        }
        return assemblyIds;
    }

    /**
     * Get all assembly IDs with the given assembly name.
     * 
     * @param assemblyName
     *            the name of the assembly to find IDs for.
     * @return all assembly IDs found for the given assembly name.
     */
    List<Integer> getAssemblyIdsWithName(String assemblyName) {

        ensureTmcdbReady();
        List<Integer> ids = new ArrayList<Integer>();

        /*
         * The following code was quickly hacked together to deal with missing
         * configuration data in the current (2007-11-13) TMCDB at the ATF.
         */

        // try {
        // assemblyIdsWithNameQuery.setString(2, assemblyName);
        // ResultSet result = assemblyIdsWithNameQuery.executeQuery();
        // while (result.next()) {
        // ids.add(result.getInt("AssemblyId"));
        // }
        // } catch (SQLException err) {
        // logger.warning(
        // "Caught SQLException in
        // alma.TMCDB.Query.TMCDBCommonQueries.getPropertyName(assemblyName) " +
        // err.toString());
        // }
        List<String> allAssemblySNs = getAssemblySNs();
        List<String> candidateSNs = new ArrayList<String>();
        for (String candidateAssemblySN : allAssemblySNs)
            if (getAssemblyFromSN(candidateAssemblySN).equals(assemblyName))
                candidateSNs.add(candidateAssemblySN);
        for (String assemblySN : candidateSNs) {
            ids.add(getAssemblyIDforSN(assemblySN));
        }
        return ids;
    }

    /**
     * @param assemblyId
     *            the Id of the assembly to look for.
     * @return the AssemblyName found in the TMCDB for a given AssemblyId.
     */
    public String getAssemblyName(int assemblyId) {

        ensureTmcdbReady();
        List<String> names = new ArrayList<String>();
        try {
            assemblyNameFromAssemblyIdQuery.setInt(1, assemblyId);
            ResultSet result = assemblyNameFromAssemblyIdQuery.executeQuery();
            while (result.next()) {
                names.add(result.getString("AssemblyName"));
            }
        } catch (SQLException err) {
            logger.warning(
                "Caught SQLException in alma.TMCDB.Query.TMCDBCommonQueries.getAssemblyName(assemblyId) "
                + err.toString());
        }
        if (names.size() == 0)
            return "";
        else if (names.size() == 1)
            return names.get(0);
        else {
            logger.warning(
                "alma.TMCDB.Query.TMCDBCommonQueries.getAssemblyName(assemblyId) found more than one name for assemblyId: "
                + assemblyId 
                + ", returning the fist name found.");
            return names.get(0);
        }
    }

    /**
     * Find all assembly names for assemblies.
     * 
     * @return a List of names of assemblies.
     */
    public List<String> getAssemblyNames() {

        ensureTmcdbReady();
        List<String> names = new ArrayList<String>();
        try {
            ResultSet result = assemblyNamesQuery.executeQuery();
            String assemblyName;
            while (result.next()) {
                assemblyName = result.getString("AssemblyName");
                if (!names.contains(assemblyName))
                    names.add(assemblyName);
            }
        } catch (SQLException err) {
            logger.warning(
                "Caught SQLException in alma.TMCDB.Query.TMCDBCommonQueries.getAssemblyId(baseElementId) "
                + err.toString());
        }
        return names;
    }

    /**
     * Find all assembly names for assemblies associated with the given antenna.
     * 
     * @param antennaName
     *            the name of the antenna containing assemblies we want names
     *            for.
     * @return a List of names of assemblies in named antenna.
     */
    public List<String> getAssemblyNames(String antennaName) {

        /*
         * The following code was quickly hacked together to deal with missing
         * configuration data in the current (2007-11-13) TMCDB at the ATF.
         * 
         * The only way to get assemblies contained in antennas at the ATF now
         * is to depend on the convention that assembly serial numbers have the
         * form "CONTROL/<antenna name>/<assembly>" where <antenna name> is
         * the name of the antenna containing the assembly, and <assembly> is
         * the name of the assembly (device).
         */

        ensureTmcdbReady();
        List<String> names = new ArrayList<String>();

        // List<Integer> IDs = getAssemblyIdsInAntenna(antennaName);
        // for (int ID: IDs) {
        // names.add(getAssemblyName(ID));
        // }

        List<String> serialNumbers = getAssemblySNs();

        String antenna = null;
        for (String SN : serialNumbers) {
            antenna = getAntennaFromSN(SN);
            if (antenna.equals(antennaName)) {
                names.add(getAssemblyFromSN(SN));
            }
        }
        return names;
    }

    // TODO: document
    List<String> getAssemblySNs() {

        ensureTmcdbReady();
        List<String> serialNumbers = new ArrayList<String>();
        try {
            ResultSet result = assemblySNsQuery.executeQuery();
            while (result.next()) {
                serialNumbers.add(result.getString("SerialNumber"));
            }
        } catch (SQLException err) {
            logger.warning(
                "Caught SQLException in alma.TMCDB.Query.TMCDBCommonQueries.getAssemblySNs() "
                + err.toString());
        }
        return serialNumbers;
    }

    /**
     * @param antennaName
     *            the name of the antenna to look for.
     * @return the BaseElementIds found in the TMCDB for the given antennaName.
     */
    List<Integer> getBaseElementIds(String antennaName) {

        ensureTmcdbReady();
        List<Integer> ids = new ArrayList<Integer>();
        try {
            baseElementIdsFromAntennaNameQuery.setString(2, antennaName);
            ResultSet result = baseElementIdsFromAntennaNameQuery
                    .executeQuery();
            while (result.next()) {
                ids.add(result.getInt("BaseElementId"));
            }
        } catch (SQLException err) {
            logger.warning(
                "Caught SQLException in alma.TMCDB.Query.TMCDBCommonQueries.getBaseElementId(antennaName) "
                + err.toString());
        }
        return ids;
    }

    /**
     * Get monitor data for the given antenna, assembly, and property within the
     * given time range.
     * 
     * @param antennaName
     *            the antenna containing the assembly to monitor
     * @param assemblyName
     *            the assembly to monitor
     * @param propertyName
     *            the property of the assembly to monitor
     * @param begin
     *            ArrayTime for the beginning of the time range of interest
     * @param end
     *            ArrayTime for the end of the time range of interest
     * @return an array of time-value pairs
     * 
     * Implementation details: The method converts from data types useful for
     * UIs to data expected by TMCDBMonitor.getMonitorData().
     */
    public TimeValue[] getMonitorData(String antennaName, String assemblyName,
            String propertyName, ArrayTime begin, ArrayTime end) {

        ensureTmcdbReady();
        TimeValue[] monitorData = new TimeValue[0];
        int assemblyId = getAssemblyId(assemblyName, antennaName);
        int propertyId = getPropertyId(propertyName, getGeneralAssemblyName(assemblyName));
        String dataType = getPropertyDataType(propertyName);
        if (dataType == "")
            return monitorData;
        try {
            monitorData = getMonitorData(assemblyId, propertyId, dataType,
                    begin, end);
        } catch (AcsJTmcdbErrTypeEx err) {
            logger.warning(
                "Caught AcsJTmcdbErrTypeEx in alma.TMCDB.Query.TMCDBCommonQueries.getMonitorData) "
                + err.toString());
        }
        return monitorData;
    }

    /**
     * Get the latest(one) monitor data point for the given antenna, assembly,
     * and property. TODO Implement this method
     * 
     * @param antennaName
     *            the antenna containing the assembly to monitor
     * @param assemblyName
     *            the assembly to monitor
     * @param propertyName
     *            the property of the assembly to monitor
     * @return a time-value pair
     * 
     */
    public TimeValue getLastMonitorData(String antennaName,
            String assemblyName, String propertyName) {
        ensureTmcdbReady();
        TimeValue monitorData = null;

        int assemblyId = getAssemblyId(assemblyName, antennaName);
        int propertyId = getPropertyId(propertyName, getGeneralAssemblyName(assemblyName));
        String dataType = getPropertyDataType(propertyName);
        if (dataType == "")
            return monitorData;
        try {
            monitorData = getMonitorData(assemblyId, propertyId, dataType);
        } catch (AcsJTmcdbErrTypeEx err) {
            logger.warning(
                "Caught AcsJTmcdbErrTypeEx in alma.TMCDB.Query.TMCDBCommonQueries.getMonitorData) "
                + err.toString());
        }
        return monitorData;
    }

    /**
     * Get the last (in time order) Float monitor data point for the specified
     * assembly.
     * 
     * @param assemblyId
     *            the database id of the assembly.
     * @param propertyTypeId
     *            the database id of the property.
     * 
     * @return the value found, or null if none found.
     */
    private TimeValue getLastFloatMonitorDataPoint(int assemblyId,
            int propertyTypeId) throws AcsJTmcdbSqlEx {

        try {
            //
            // The query in lastFloatMonitorDataTime returns 0 or 1 results
            // only.
            lastFloatMonitorDataTime.setInt(1, assemblyId);
            lastFloatMonitorDataTime.setInt(2, propertyTypeId);
            ResultSet timeStampResults = lastFloatMonitorDataTime
                    .executeQuery();
            if (timeStampResults.next() == false)
                return null;
            long lastTimeStamp = timeStampResults.getLong(1);

            //
            // The query in lastFloatMonitorDataValue may return 0, 1, or > 1
            // results.
            // Log a warning in case of 0 or > 1 result.
            lastFloatMonitorDataValue.setInt(1, assemblyId);
            lastFloatMonitorDataValue.setInt(2, propertyTypeId);
            lastFloatMonitorDataValue.setLong(3, lastTimeStamp);
            ResultSet valueResults = lastFloatMonitorDataValue.executeQuery();
            if (valueResults.next() == false) {
                logger.warning(
                    "Did not find Value data matching time stamp in alma.TMCDB.Query.TMCDBCommonQueries.getLastFloatMonitorDataPoint()");
                return null;
            }

            float lastValue = valueResults.getFloat(1);
            if (valueResults.next() != false) {
                logger.warning(
                    "Found more than one Value data matching time stamp in alma.TMCDB.Query.TMCDBCommonQueries.getLastFloatMonitorDataPoint()");
            }

            TimeValue result = new TimeValue(new ArrayTime(lastTimeStamp),
                    lastValue);
            return result;

        } catch (SQLException err) {
            err.printStackTrace();
            throw new AcsJTmcdbSqlEx(err);
        }
    }

    /**
     * Get the last (in time order) Double monitor data point for the specified
     * assembly.
     * 
     * @param assemblyId
     *            the database id of the assembly.
     * @param propertyTypeId
     *            the database id of the property.
     * 
     * @return the value found, or null if none found.
     */
    private TimeValue getLastDoubleMonitorDataPoint(int assemblyId,
            int propertyTypeId) throws AcsJTmcdbSqlEx {

        try {
            //
            // The query in lastDoubleMonitorDataTime returns 0 or 1 results
            // only.
            lastDoubleMonitorDataTime.setInt(1, assemblyId);
            lastDoubleMonitorDataTime.setInt(2, propertyTypeId);
            ResultSet timeStampResults = lastDoubleMonitorDataTime
                    .executeQuery();
            if (timeStampResults.next() == false)
                return null;
            long lastTimeStamp = timeStampResults.getLong(1);

            //
            // The query in lastDoubleMonitorDataValue may return 0, 1, or > 1
            // results. Log a warning in case of 0 or > 1 result.
            lastDoubleMonitorDataValue.setInt(1, assemblyId);
            lastDoubleMonitorDataValue.setInt(2, propertyTypeId);
            lastDoubleMonitorDataValue.setLong(3, lastTimeStamp);
            ResultSet valueResults = lastDoubleMonitorDataValue.executeQuery();
            if (valueResults.next() == false) {
                logger.warning(
                    "Did not find Value data matching time stamp in alma.TMCDB.Query.TMCDBCommonQueries.getLastDoubleMonitorDataPoint()");
                return null;
            }

            double lastValue = valueResults.getDouble(1);
            if (valueResults.next() != false) {
                logger.warning(
                    "Found more than one Value data matching time stamp in alma.TMCDB.Query.TMCDBCommonQueries.getLastDoubleMonitorDataPoint()");
            }

            TimeValue result = new TimeValue(new ArrayTime(lastTimeStamp),
                    lastValue);
            return result;

        } catch (SQLException err) {
            err.printStackTrace();
            throw new AcsJTmcdbSqlEx(err);
        }
    }

    /**
     * Get the last (in time order) Integer monitor data point for the specified
     * assembly.
     * 
     * @param assemblyId
     *            the database id of the assembly.
     * @param propertyTypeId
     *            the database id of the property.
     * 
     * @return the value found, or null if none found.
     */
    private TimeValue getLastIntegerMonitorDataPoint(int assemblyId,
            int propertyTypeId) throws AcsJTmcdbSqlEx {

        try {
            //
            // The query in lastIntegerMonitorDataTime returns 0 or 1 results
            // only.
            lastIntegerMonitorDataTime.setInt(1, assemblyId);
            lastIntegerMonitorDataTime.setInt(2, propertyTypeId);
            ResultSet timeStampResults = lastIntegerMonitorDataTime
                    .executeQuery();
            if (timeStampResults.next() == false)
                return null;
            long lastTimeStamp = timeStampResults.getLong(1);

            //
            // The query in lastIntegerMonitorDataValue may return 0, 1, or > 1
            // results.
            // Log a warning in case of 0 or > 1 result.
            lastIntegerMonitorDataValue.setInt(1, assemblyId);
            lastIntegerMonitorDataValue.setInt(2, propertyTypeId);
            lastIntegerMonitorDataValue.setLong(3, lastTimeStamp);
            ResultSet valueResults = lastIntegerMonitorDataValue.executeQuery();
            if (valueResults.next() == false) {
                logger.warning(
                    "Did not find Value data matching time stamp in alma.TMCDB.Query.TMCDBCommonQueries.getLastIntegerMonitorDataPoint()");
                return null;
            }

            int lastValue = valueResults.getInt(1);
            if (valueResults.next() != false) {
                logger.warning(
                    "Found more than one Value data matching time stamp in alma.TMCDB.Query.TMCDBCommonQueries.getLastIntegerMonitorDataPoint()");
            }

            TimeValue result = new TimeValue(new ArrayTime(lastTimeStamp), lastValue);
            return result;

        } catch (SQLException err) {
            err.printStackTrace();
            throw new AcsJTmcdbSqlEx(err);
        }
    }

    /**
     * Get the last (in time order) String monitor data point for the specified
     * assembly.
     * 
     * @param assemblyId
     *            the database id of the assembly.
     * @param propertyTypeId
     *            the database id of the property.
     * 
     * @return the value found, or null if none found.
     */
    private TimeValue getLastStringMonitorDataPoint(int assemblyId,
            int propertyTypeId) throws AcsJTmcdbSqlEx {

        try {
            //
            // The query in lastStringMonitorDataTime returns 0 or 1 results
            // only.
            lastStringMonitorDataTime.setInt(1, assemblyId);
            lastStringMonitorDataTime.setInt(2, propertyTypeId);
            ResultSet timeStampResults = lastStringMonitorDataTime
                    .executeQuery();
            if (timeStampResults.next() == false)
                return null;
            long lastTimeStamp = timeStampResults.getLong(1);

            //
            // The query in lastStringMonitorDataValue may return 0, 1, or > 1
            // results.
            // Log a warning in case of 0 or > 1 result.
            lastStringMonitorDataValue.setInt(1, assemblyId);
            lastStringMonitorDataValue.setInt(2, propertyTypeId);
            lastStringMonitorDataValue.setLong(3, lastTimeStamp);
            ResultSet valueResults = lastStringMonitorDataValue.executeQuery();
            if (valueResults.next() == false) {
                logger.warning(
                    "Did not find Value data matching time stamp in alma.TMCDB.Query.TMCDBCommonQueries.getLastStringMonitorDataPoint()");
                return null;
            }

            String lastValue = valueResults.getString(1);
            if (valueResults.next() != false) {
                logger.warning(
                    "Found more than one Value data matching time stamp in alma.TMCDB.Query.TMCDBCommonQueries.getLastStringMonitorDataPoint()");
            }

            TimeValue result = new TimeValue(new ArrayTime(lastTimeStamp),
                    lastValue);
            return result;

        } catch (SQLException err) {
            err.printStackTrace();
            throw new AcsJTmcdbSqlEx(err);
        }
    }

    /**
     * Get the last (in time order) Boolean monitor data point for the specified
     * assembly.
     * 
     * @param assemblyId
     *            the database id of the assembly.
     * @param propertyTypeId
     *            the database id of the property.
     * 
     * @return the value found, or null if none found.
     */
    private TimeValue getLastBooleanMonitorDataPoint(int assemblyId,
            int propertyTypeId) throws AcsJTmcdbSqlEx {

        try {
            //
            // The query in lastBooleanMonitorDataTime returns 0 or 1 results
            // only.
            lastBooleanMonitorDataTime.setInt(1, assemblyId);
            lastBooleanMonitorDataTime.setInt(2, propertyTypeId);
            ResultSet timeStampResults = lastBooleanMonitorDataTime
                    .executeQuery();
            if (timeStampResults.next() == false)
                return null;
            long lastTimeStamp = timeStampResults.getLong(1);

            //
            // The query in lastBooleanMonitorDataValue may return 0, 1, or > 1
            // results.
            // Log a warning in case of 0 or > 1 result.
            lastBooleanMonitorDataValue.setInt(1, assemblyId);
            lastBooleanMonitorDataValue.setInt(2, propertyTypeId);
            lastBooleanMonitorDataValue.setLong(3, lastTimeStamp);
            ResultSet valueResults = lastBooleanMonitorDataValue.executeQuery();
            if (valueResults.next() == false) {
                logger.warning(
                    "Did not find Value data matching time stamp in alma.TMCDB.Query.TMCDBCommonQueries.getLastBooleanMonitorDataPoint()");
                return null;
            }

            boolean lastValue = valueResults.getBoolean(1);
            if (valueResults.next() != false) {
                logger.warning(
                    "Found more than one Value data matching time stamp in alma.TMCDB.Query.TMCDBCommonQueries.getLastBooleanMonitorDataPoint()");
            }

            TimeValue result = new TimeValue(new ArrayTime(lastTimeStamp),
                    lastValue);
            return result;

        } catch (SQLException err) {
            err.printStackTrace();
            throw new AcsJTmcdbSqlEx(err);
        }
    }

    /**
     * Get the last (in time order) Enum monitor data point for the specified
     * assembly.
     * 
     * @param assemblyId
     *            the database id of the assembly.
     * @param propertyTypeId
     *            the database id of the property.
     * 
     * @return the value found, or null if none found.
     */
    private TimeValue getLastEnumMonitorDataPoint(int assemblyId,
            int propertyTypeId) throws AcsJTmcdbSqlEx {

        try {
            //
            // The query in lastEnumMonitorDataTime returns 0 or 1 results only.
            lastEnumMonitorDataTime.setInt(1, assemblyId);
            lastEnumMonitorDataTime.setInt(2, propertyTypeId);
            ResultSet timeStampResults = lastEnumMonitorDataTime.executeQuery();
            if (timeStampResults.next() == false)
                return null;
            long lastTimeStamp = timeStampResults.getLong(1);

            //
            // The query in lastEnumMonitorDataValue may return 0, 1, or > 1
            // results.
            // Log a warning in case of 0 or > 1 result.
            lastEnumMonitorDataValue.setInt(1, assemblyId);
            lastEnumMonitorDataValue.setInt(2, propertyTypeId);
            lastEnumMonitorDataValue.setLong(3, lastTimeStamp);
            ResultSet valueResults = lastEnumMonitorDataValue.executeQuery();
            if (valueResults.next() == false) {
                logger.warning(
                    "Did not find Value data matching time stamp in alma.TMCDB.Query.TMCDBCommonQueries.getLastEnumMonitorDataPoint()");
                return null;
            }

            int lastValue = valueResults.getInt(1);
            if (valueResults.next() != false) {
                logger.warning(
                    "Found more than one Value data matching time stamp in alma.TMCDB.Query.TMCDBCommonQueries.getLastEnumMonitorDataPoint()");
            }

            TimeValue result = new TimeValue(new ArrayTime(lastTimeStamp),
                    lastValue);
            return result;

        } catch (SQLException err) {
            err.printStackTrace();
            throw new AcsJTmcdbSqlEx(err);
        }
    }

    /**
     * Get the last monitored data for the specified property of the specified
     * assembly.
     * 
     * @param assemblyId
     *            database id of the assembly.
     * @param propertyTypeId
     *            database id of the property.
     * @param datatype
     *            data type of the property.
     * @return The data of interest as an array of time-value pairs ordered by
     *         increasing time.
     * @throws AcsJTmcdbSqlEx
     *             Thrown if any database error occurs.
     */
    public TimeValue getMonitorData(int assemblyId, int propertyTypeId,
            String datatype) throws AcsJTmcdbInvalidDataTypeEx, AcsJTmcdbSqlEx {
        if (datatype.equals("float"))
            return getLastFloatMonitorDataPoint(assemblyId, propertyTypeId);
        else if (datatype.equals("double"))
            return getLastDoubleMonitorDataPoint(assemblyId, propertyTypeId);
        else if (datatype.equals("integer"))
            return getLastIntegerMonitorDataPoint(assemblyId, propertyTypeId);
        else if (datatype.equals("string"))
            return getLastStringMonitorDataPoint(assemblyId, propertyTypeId);
        else if (datatype.equals("boolean"))
            return getLastBooleanMonitorDataPoint(assemblyId, propertyTypeId);
        else if (datatype.equals("enum"))
            return getLastEnumMonitorDataPoint(assemblyId, propertyTypeId);

        //
        // We failed to match on a valid datatype
        AcsJTmcdbInvalidDataTypeEx ex = new AcsJTmcdbInvalidDataTypeEx();
        ex.setProperty("Datatype", datatype);
        throw ex;
    }

    /**
     * Find the property data type for the given property name.
     * 
     * @param propertyName
     *            the name of the property to find the data type for.
     * @return a String containing the data type of the named property.
     */
    public String getPropertyDataType(String propertyName) {

        ensureTmcdbReady();
        String dataType = "";
        try {
            propertyDataTypeFromPropertyNameQuery.setString(1, propertyName);
            ResultSet result = propertyDataTypeFromPropertyNameQuery
                    .executeQuery();
            // This should give 0 or 1 results.
            if (result.next())
                dataType = result.getString("DataType");
        } catch (SQLException err) {
            logger.warning(
                "Caught SQLException in alma.TMCDB.Query.TMCDBCommonQueries.getPropertyName(assemblyName) "
                + err.toString());
        }
        return dataType;
    }

    /**
     * Find the property ID for the given property name.
     * 
     * @param propertyName
     *            the name of the property to find the ID for.
     * @return an int containing the id of the named property.
     */
    public int getPropertyId(String propertyName, String assemblyName) {

        ensureTmcdbReady();
        int id = 0;
        try {
            propertyIdFromPropertyNameQuery.setString(1, propertyName);
            propertyIdFromPropertyNameQuery.setString(2, assemblyName);
            ResultSet result = propertyIdFromPropertyNameQuery.executeQuery();
            // This should give 0 or 1 results.
            if (result.next())
                id = result.getInt("PropertyTypeId");
        } catch (SQLException err) {
            logger.warning(
                "Caught SQLException in alma.TMCDB.Query.TMCDBCommonQueries.getPropertyName(assemblyName) "
                 + err.toString());
        }
        return id;
    }

    private String getGeneralAssemblyName(String specificAssemblyName) {
        // TODO: This conversion is brittle, but it is required until the full TMCDB table design is 
        // implemented.
        if (specificAssemblyName.startsWith("DRX"))
        	return "DRX";
        if (specificAssemblyName.startsWith("DTX"))
           	return "DTX";
        if (specificAssemblyName.startsWith("IFProc"))
            return "IFProc";
        if (specificAssemblyName.startsWith("LO2"))
            return "LO2";
        if (specificAssemblyName.startsWith("FrontEnd"))
            return specificAssemblyName.split("/")[1];
        return specificAssemblyName;
    }

    /**
     * Find all property names for the given assembly.
     * 
     * @param assemblyName
     *            the name of the assembly to find properties for.
     * @return a List of names of properties for named assembly.
     */
    public List<String> getPropertyNames(String assemblyName) {

        ensureTmcdbReady();
        List<String> names = new ArrayList<String>();

        /*
         * The following code was quickly hacked together to deal with missing
         * configuration data in the current (2007-11-13) TMCDB at the ATF.
         */

        try {
            String generalAssemblyName = getGeneralAssemblyName(assemblyName);
            propertyNameFromAssemblyNameQuery.setString(1, generalAssemblyName);
            ResultSet result = propertyNameFromAssemblyNameQuery.executeQuery();
            while (result.next()) {
                names.add(result.getString("PropertyName"));
            }
        } catch (SQLException err) {
            logger.warning(
                "Caught SQLException in alma.TMCDB.Query.TMCDBCommonQueries.getPropertyName(assemblyName) "
                + err.toString());
        }
        return names;
    }

    /**
     * Call super class method using database connection already stored.
     * 
     * @param filename
     *            of file containing SQL code to pass to database.
     */
    public void loadAndExecuteScript(String filename) throws Exception {
        // super.loadAndExecuteScript(conn, filename);

        /*
         * The following is temporary code to allow progress while
         * TMCDBUtil.loadAndExecuteScript() is being redesigned. After that
         * redesign is complete, the following code will be removed.
         */
        // TODO: Delete the following after TMCDBUtil.loadAndExecuteScript() is complete.
        BufferedReader csr = null;
        Statement stmt = null;

        try {
            stmt = conn.createStatement();
            csr = new BufferedReader(new FileReader(filename));
        } catch (SQLException e) {
            // TODO Replace auto-generated catch block.
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Replace auto-generated catch block.
            e.printStackTrace();
        }
        String line;
        StringBuffer sb = new StringBuffer();
        if (csr != null && stmt != null)
            try {
                while ((line = csr.readLine()) != null) {
                    if (line.startsWith("--"))
                        continue; // SQL Comment
                    if (line.contains(";")) {
                        line = line.replace(";", "");
                        sb.append(line);
                        line = sb.toString();
                        try {
                            stmt.execute(line);
                        } catch (SQLException e) {
                            // TODO Replace Auto-generated catch block.
                            e.printStackTrace();
                        }
                        sb = new StringBuffer();
                    } else {
                        sb.append(line);
                    }
                }
            } catch (IOException e) {
                // TODO Replace auto-generated catch block.
                e.printStackTrace();
            }
    }


} // class

//O_o
