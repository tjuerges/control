/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File MonitorException.java
 */
package alma.TMCDB.Monitor;

import alma.hla.runtime.asdm.types.ArrayTime;

public class MonitorException extends Exception {
	
	public static final int DatabaseError = 0;
	public static final int IOError = 1;
	public static final int InvalidArgument = 2;
	public static final int Unknown = 3;
	private static final String[] errorTypeName = { 
		"DatabaseError", "IOError", "InvalidArgument", "Unknown"
	};
	
	private String componentName;
	private String className;
	private ArrayTime timestamp;
	private int errorType;
	private String message;
	
	public MonitorException (String componentName, String className, int errorType, String message) {
		super(componentName + "." + className + " [" + Monitor.currentTime().toFITS() + "] " + 
				errorTypeName[errorType] + ": " + message);
		this.componentName = componentName;
		this.className = className;
		this.timestamp = Monitor.currentTime();
		this.errorType = (errorType >= 0 && errorType <= Unknown) ? errorType : Unknown;
		this.message = message;
	}

	public String toString() {
		return componentName + "." + className + " [" + timestamp.toFITS() + "] " + 
			errorTypeName[errorType] + ": " + message;
	}

	public String getClassName() {
		return className;
	}

	public String getComponentName() {
		return componentName;
	}

	public int getErrorType() {
		return errorType;
	}

	public String getMessage() {
		return message;
	}

	public ArrayTime getTimestamp() {
		return timestamp;
	}
	
	
}


