/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * $Id$
 */

package alma.TMCDB.Monitor;

import java.util.logging.Logger;

import alma.Control.MonitorInterfaceOperations;
import alma.Control.MonitorInterfacePOATie;
import alma.acs.component.ComponentLifecycle;
import alma.acs.container.ComponentHelper;
import alma.maciErrType.wrappers.AcsJComponentCreationEx;

public class MonitorHelper extends ComponentHelper {

	/**
     * Provide the interface necessary to create a Monitor component.
     */
    public MonitorHelper(Logger containerLogger) {
        super(containerLogger);
    }

    /**
     * Create a Monitor component implementation instance.
     * 
     * @see alma.acs.container.ComponentHelper#_createComponentImpl()
     */
    protected ComponentLifecycle _createComponentImpl() throws AcsJComponentCreationEx {
        return new Monitor();
    }

    /**
     * Get the Monitor component POA Tie class.
     * 
     * @see alma.acs.container.ComponentHelper#_getPOATieClass()
     */
    protected Class _getPOATieClass() {
        return MonitorInterfacePOATie.class;
    }

    /**
     * Get the Monitor component CORBA server stub class.
     * 
     * @see alma.acs.container.ComponentHelper#_getOperationsInterface()
     */
    protected Class _getOperationsInterface() {
        return MonitorInterfaceOperations.class;
    }

	/**
	 * Makes sure the java container does not log the frequent invocations to the receive methods.
	 * This is important even when these invocation logs are of a low enough level to be filtered out later in the log processing,
	 * because given the high rate of these invocations even the more construction of these logs should be avoided. 
	 */
	protected String[] _getComponentMethodsExcludedFromInvocationLogging() {
		return new String[] {
			"receiveFloat",
			"receiveDouble",
			"receiveInteger",
			"receiveString",
			"receiveBoolean",
			"receiveEnum"
		};
	}

}
