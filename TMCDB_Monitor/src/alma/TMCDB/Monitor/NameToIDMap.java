/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File NameToIDMap.java
 */
package alma.TMCDB.Monitor;

import java.util.HashMap;
import java.util.Iterator;

/**
 * The NameToIDMap is a class that takes two arrays that contain data
 * about Properties and Assemblies and creates a mapping between 
 * serial numbers and property names on the one hand and database
 * identifiers on the other.  The maps are created in the constructor
 * and no additions are allowed.  The main method is the 'getID'
 * method that returns a pair of identifiers, given the serial number
 * and the property name.
 * 
 * The serial number reflects current naming conventions used in Control.  These
 * conventions should change in the future.  The current convention in Control 
 * for naming devices is the following.  A device-name returned in a monitored 
 * BACI property is:
 *     "CONTROL/<antenna-name>/<role-name>"
 * So, for antenna DV01 and IFProc number 0, the device name is:
 *     "CONTROL/DV01/IFProc0"
 * The serial number that is being used in the Assembly table is this
 * device-name.  Clearly this is inadequate and will have to be changed 
 * in the future.
 * 
 * In the current implementation, the mapping goes only in one direction - 
 * from strings to identifiers.  There are no methods to go from identifiers
 * to names.
 */
public class NameToIDMap {

    private HashMap<String,PropertyValue> propertyMap;
	private HashMap<String,Integer> assemblyMap;
	
	public NameToIDMap(AssemblyProperty[] property, 
			AssemblyIdentification[] assembly) {
		assemblyMap = new HashMap<String,Integer> ();
		for (int i = 0; i < assembly.length; ++i)
			assemblyMap.put(assembly[i].getSerialNumber(), assembly[i].getAssemblyId());
		propertyMap = new HashMap<String,PropertyValue> ();
		for (int i = 0; i < property.length; ++i) {
			PropertyValue value = new PropertyValue (property[i].getPropertyTypeId(),property[i].getDatatype());
			propertyMap.put(property[i].getSerialNumber() + "." + property[i].getPropertyName(),value);
		}
	}
	
	public IDPair getID(String serialNumber, String propertyName) {
		Integer id1 = assemblyMap.get(serialNumber);
		if (id1 == null)
			return null;
		PropertyValue id2 = propertyMap.get(serialNumber + "." + propertyName);
		if (id2 == null)
			return null;
		return new IDPair(id1,id2);
	}
    
    @Override
    public String toString() {
        String rep = "";
        
        rep += super.toString() + ":\n";
        rep += "> assemblyMap:\n";        
        Iterator<String> iter = assemblyMap.keySet().iterator();
        String key;
        while(iter.hasNext()) {
            key = iter.next();
            rep += ">   " + key + ": " + assemblyMap.get(key) + "\n";
        }

        rep += "> propertyMap:\n";        
        iter = propertyMap.keySet().iterator();
        PropertyValue value;
        while(iter.hasNext()) {
            key = iter.next();
            value = propertyMap.get(key);
            rep += ">   " + key + ": (" + value.getPropertyTypeId() + 
                   ", " + value.getDatatype() + ")\n";
        }

        return rep;
    }
}


