/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File MonitorThreadControl.java
 */
package alma.TMCDB.Monitor;

import alma.TMCDB.TMCDB;
import alma.TMCDB.generated.BooleanProperty;
import alma.TMCDB.generated.DoubleProperty;
import alma.TMCDB.generated.EnumProperty;
import alma.TMCDB.generated.FloatProperty;
import alma.TMCDB.generated.IntegerProperty;
import alma.TMCDB.generated.StringProperty;

import alma.TMCDBMonitorErrType.wrappers.AcsJTMCDBMonitorErrTypeEx;
import alma.TMCDBMonitorErrType.wrappers.AcsJInvalidArgumentEx;

import alma.hla.runtime.asdm.types.ArrayTime;

import java.util.logging.Logger;
import java.util.ArrayList;

import java.io.File;

/**
 * The MonitorThreadControl is a simple class that allows the Monitor
 * object to control the actions of the thread that writes monitor data to
 * the database.  There are a number of options that can be used on-the-fly
 * to control the actions of the thread.
 *   
 * @author afarris
 *
 */
public class MonitorThreadControl {

	// The following fields are fixed in the constructor.
	// The database to which the monitor data is to be written.
	private TMCDB database;
	// The Logger for log messages.
	private Logger log;
	// The Monitor component name.
	private String componentName;
	// Queues that house the monitor data.
	private Buffer<FloatProperty> qFloat;
	private Buffer<DoubleProperty> qDouble;
	private Buffer<IntegerProperty> qInteger;
	private Buffer<StringProperty> qString;
	private Buffer<BooleanProperty> qBoolean;
	private Buffer<EnumProperty> qEnum;
	// The WriteMonitorData thread object.
	private WriteMonitorData writeDataThread;
	
	// The following fields are dynamic and use synchronized get and set methods. 
	// Whether to stop the monitoring thread or not.
	private boolean stopMonitoring;
	// Whether to write the monitor data to a Text file or not.
	private boolean toText;
	// Whether to write the monitor data to the database or not.
	private boolean toDatabase;
	// The File object to which the text file is to be written.
	private File textFile;
	// The delimiter to be used in writing the text file.
	private String delimiter;
	// The time interval, in milliseconds, to sleep between writing the data.
	private long sleepTimeInterval;
	// The time interval, in milliseconds, for the watch-dog process to sleep.
	private long watchDogSleepTime;
	// The ArrayList for error messages.
	private ArrayList<AcsJTMCDBMonitorErrTypeEx> error;
	// The time of the last write.
	private ArrayTime timeOfLastWrite;
	
	public MonitorThreadControl (TMCDB database, Logger log, String componentName, 
			Buffer<FloatProperty> qFloat, Buffer<DoubleProperty> qDouble, Buffer<IntegerProperty> qInteger, 
			Buffer<StringProperty> qString, Buffer<BooleanProperty> qBoolean, Buffer<EnumProperty> qEnum) 
			throws AcsJInvalidArgumentEx {
		if (componentName == null || componentName.length() == 0)
            throw new AcsJInvalidArgumentEx("The component name is null!");
		if (database == null)
            throw new AcsJInvalidArgumentEx("The database object is null!");
		if (log == null)
            throw new AcsJInvalidArgumentEx("The TMCDB database object in MonitorThreadControl is null!");
		if (qFloat == null)
            throw new AcsJInvalidArgumentEx("The buffer object for float data in MonitorThreadControl is null!");
		if (qDouble == null)
            throw new AcsJInvalidArgumentEx("The buffer object for double data in MonitorThreadControl is null!");
		if (qInteger == null)
            throw new AcsJInvalidArgumentEx("The buffer object for integer data in MonitorThreadControl is null!");
		if (qString == null)
            throw new AcsJInvalidArgumentEx("The buffer object for string data in MonitorThreadControl is null!");
		if (qBoolean == null)
            throw new AcsJInvalidArgumentEx("The buffer object for boolean data in MonitorThreadControl is null!");
		if (qEnum == null)
            throw new AcsJInvalidArgumentEx("The buffer object for enum data in MonitorThreadControl is null!");
		this.database = database;
		this.log = log;
		this.componentName = componentName;
		this.qFloat = qFloat;
		this.qDouble = qDouble;
		this.qInteger = qInteger;
		this.qString = qString;
		this.qBoolean = qBoolean;
		this.qEnum = qEnum;
		this.writeDataThread = null;		
		stopMonitoring = false;
		toText = false;
		toDatabase = true;
		this.textFile = null;
		delimiter = "\t";
		sleepTimeInterval = 60000L;
		watchDogSleepTime = 10000L;
		error = new ArrayList<AcsJTMCDBMonitorErrTypeEx> ();
		timeOfLastWrite = new ArrayTime ();
	}
	
	public void setWriteDataThread(WriteMonitorData writeDataThread) throws AcsJInvalidArgumentEx {
		if (writeDataThread == null)
			throw new AcsJInvalidArgumentEx("The WriteMonitorData thread object in MonitorThreadControl is null!");
		this.writeDataThread = writeDataThread;
	}

	// Error Handling
	public synchronized void addError (AcsJTMCDBMonitorErrTypeEx err) {
		if (err == null)
			return;
		error.add(err);
	}
	public synchronized AcsJTMCDBMonitorErrTypeEx[] getErrors() {
		AcsJTMCDBMonitorErrTypeEx[] x = new AcsJTMCDBMonitorErrTypeEx [error.size()];
		x = error.toArray(x);
		return x;
	}
	public synchronized void clearErrors() {
		error.clear();
	}
	
	// Time handling.
	public synchronized void setTimeOfLastWrite() {
		timeOfLastWrite =  Monitor.currentTime();	
	}
	public synchronized ArrayTime getTimeOfLastWrite() {
		return timeOfLastWrite;
	}
	
	// Sleep handling
	public synchronized void setSleepTimeInterval(long sleepTimeInterval) throws AcsJInvalidArgumentEx {
		if (sleepTimeInterval < 0L)
			throw new AcsJInvalidArgumentEx("The sleepTimeInterval cannot be less than 0!");
		this.sleepTimeInterval = sleepTimeInterval;
	}
	public synchronized long getSleepTimeInterval() {
		return sleepTimeInterval;
	}
	public synchronized void setWatchDogSleepTime(long watchDogSleepTime) throws AcsJInvalidArgumentEx {
		if (watchDogSleepTime < 0L)
			throw new AcsJInvalidArgumentEx("The watchDogSleepTime cannot be less than 0!");
		this.watchDogSleepTime = watchDogSleepTime;
	}
	public synchronized long getWatchDogSleepTime() {
		return watchDogSleepTime;
	}
	
	// Stopping monitoring
	public synchronized void stopMonitoring() {
		this.stopMonitoring = true;
        
        synchronized(writeDataThread) {
            writeDataThread.notify();
        }
	}
	public synchronized boolean isStopMonitoring() {
		return stopMonitoring;
	}
	
	// Database actions
	public synchronized void setDatabaseOn() {
		toDatabase = true;
	}
	public synchronized void setDatabaseOff() {
		toDatabase = false;
	}
	public synchronized boolean isToDatabase() {
		return toDatabase;
	}
	
	// Text file actions
	public synchronized void setTextOn(String textFileName) throws AcsJInvalidArgumentEx {
		if (textFileName == null || textFileName.length() == 0)
			throw new AcsJInvalidArgumentEx("The textFile name cannot be null!");
		this.textFile = new File(textFileName);
		if (textFile.isDirectory())
			throw new AcsJInvalidArgumentEx("The textFile (" + textFileName + ") is a directory and not a file!");
		toText = true;
	}
	public synchronized void setTextOn(String textFileName, String delimiter) throws AcsJInvalidArgumentEx {
		if (textFileName == null || textFileName.length() == 0)
			throw new AcsJInvalidArgumentEx("The textFile name cannot be null!");
		this.textFile = new File(textFileName);
		if (textFile.isDirectory())
			throw new AcsJInvalidArgumentEx("The textFile (" + textFileName + ") is a directory and not a file!");
		if (delimiter == null || delimiter.length() == 0)
			throw new AcsJInvalidArgumentEx("The text file delimiter cannot be null!");
		this.delimiter = delimiter;
		toText = true;
	}
	public synchronized void setTextOff() {
		toText = false;
		textFile = null;
	}
	public synchronized boolean isToText() {
		return toText;
	}
	public synchronized File getTextFile() {
		return textFile;
	}
	public synchronized String getDelimiter() {
		return delimiter;
	}
	
	// Getters 
	public TMCDB getDatabase() {
		return database;
	}
	public Logger getLog() {
		return log;
	}
	public String getComponentName() {
		return componentName;
	}
	public Buffer<BooleanProperty> getQBoolean() {
		return qBoolean;
	}
	public Buffer<DoubleProperty> getQDouble() {
		return qDouble;
	}
	public Buffer<EnumProperty> getQEnum() {
		return qEnum;
	}
	public Buffer<FloatProperty> getQFloat() {
		return qFloat;
	}
	public Buffer<IntegerProperty> getQInteger() {
		return qInteger;
	}
	public Buffer<StringProperty> getQString() {
		return qString;
	}
	public WriteMonitorData getWriteDataThread () {
		return writeDataThread;
	}
	
}


