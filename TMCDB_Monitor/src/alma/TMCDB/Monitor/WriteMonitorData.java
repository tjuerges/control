/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File WriteMonitorData.java
 */
package alma.TMCDB.Monitor;

import java.io.PrintStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.sql.Timestamp;
import java.util.Properties;


import alma.acsErrTypeAlarmSourceFactory.ACSASFactoryNotInitedEx;
import alma.acsErrTypeAlarmSourceFactory.FaultStateCreationErrorEx;
import alma.acsErrTypeAlarmSourceFactory.SourceCreationErrorEx;
import alma.acsErrTypeAlarmSourceFactory.wrappers.AcsJACSASFactoryNotInitedEx;
import alma.acsErrTypeAlarmSourceFactory.wrappers.AcsJFaultStateCreationErrorEx;
import alma.acsErrTypeAlarmSourceFactory.wrappers.AcsJSourceCreationErrorEx;
import alma.alarmsystem.source.ACSAlarmSystemInterface;
import alma.alarmsystem.source.ACSAlarmSystemInterfaceFactory;
import alma.alarmsystem.source.ACSFaultState;


import alma.TMCDB.TMCDB;
import alma.TMCDB.TMCDBException;
import alma.TMCDB.generated.BooleanProperty;
import alma.TMCDB.generated.DoubleProperty;
import alma.TMCDB.generated.EnumProperty;
import alma.TMCDB.generated.FloatProperty;
import alma.TMCDB.generated.IntegerProperty;
import alma.TMCDB.generated.StringProperty;
import alma.TmcdbErrType.wrappers.AcsJTmcdbErrTypeEx;
import alma.TMCDBMonitorErrType.wrappers.AcsJTMCDBMonitorErrTypeEx;
import alma.TMCDBMonitorErrType.wrappers.AcsJDatabaseErrorEx;
import alma.TMCDBMonitorErrType.wrappers.AcsJIOErrorEx;
import alma.TMCDBMonitorErrType.wrappers.AcsJInvalidArgumentEx;
import alma.TMCDBMonitorErrType.wrappers.AcsJUnknownEx;

public class WriteMonitorData extends Thread {

    private MonitorThreadControl control;

    // The database to which the monitor data is to be written.
    private TMCDB database;

    // The Logger for log messages.
    private Logger log;

    // Queues that house the monitor data.
    private Buffer<FloatProperty> qFloat;

    private Buffer<DoubleProperty> qDouble;

    private Buffer<IntegerProperty> qInteger;

    private Buffer<StringProperty> qString;

    private Buffer<BooleanProperty> qBoolean;

    private Buffer<EnumProperty> qEnum;

    private PrintStream text;

    private String delimiter;

    private Boolean activeAlarm;

    public WriteMonitorData(ThreadGroup group, MonitorThreadControl control)
            throws AcsJInvalidArgumentEx,AcsJIOErrorEx {
        super(group, "WriteMonitorData");
        if (control == null){
                throw new AcsJInvalidArgumentEx("The control object is null!");
        }
        this.control = control;
        this.database = control.getDatabase();
        this.log = control.getLog();
        this.qFloat = control.getQFloat();
        this.qDouble = control.getQDouble();
        this.qInteger = control.getQInteger();
        this.qString = control.getQString();
        this.qBoolean = control.getQBoolean();
        this.qEnum = control.getQEnum();
        this.delimiter = control.getDelimiter();
        this.activeAlarm = false;
        text = null;
        if (control.isToText()) {
            openTextFile();
        }
    }

    private void openTextFile() throws AcsJIOErrorEx {
        // Close any open text file.
        if (text != null)
            closeTextFile();
        try {
            // Create the new text file.
            text = new PrintStream(control.getTextFile());
            this.delimiter = control.getDelimiter();
        } catch (IOException e) {
            AcsJIOErrorEx ex =  new AcsJIOErrorEx("Cannot create the specified text file",e);
            ex.setFileName(control.getTextFile().getAbsolutePath());
            throw ex;
        }
    }

    private void closeTextFile() {
        if (this.text == null)
            return;
        text.close();
        this.text = null;
    }

    private void writeData(FloatProperty[] data) {
        if (control.isToDatabase()) {
            // Add rows to the database.
            try {
                database.addProperty(data);
            } catch (AcsJTmcdbErrTypeEx err) {
                AcsJDatabaseErrorEx ex = new AcsJDatabaseErrorEx(err);
                control.addError(ex);
                ex.log(log);
            }
        }
        if (control.isToText()) {
            if (this.text == null) {
                try {
                    openTextFile();
                } catch (AcsJIOErrorEx ex) {
                    control.addError(ex);
                    ex.log(log);
                }
            }
            if (this.text != null) {
                for (int i = 0; i < data.length; ++i)
                    text.println(data[i].getAssemblyId() + delimiter
                            + data[i].getPropertyTypeId() + delimiter
                            + data[i].getSampleTime() + delimiter
                            + data[i].getValue());
            }
        }
    }

    // Note: The code within the writeData methods for DoubleProperty,
    // IntegerProperty, StringProperty, BooleanProperty, and
    // EnumProperty, is exactly like that of FloatProperty.
    private void writeData(DoubleProperty[] data) {
        if (control.isToDatabase()) {
            // Add rows to the database.
            try {
                database.addProperty(data);
            } catch (AcsJTmcdbErrTypeEx err) {
                AcsJDatabaseErrorEx ex = new AcsJDatabaseErrorEx(err);
                control.addError(ex);
                ex.log(log);
            }
        }
        if (control.isToText()) {
            if (this.text == null) {
                try {
                    openTextFile();
                } catch (AcsJIOErrorEx ex) {
                    control.addError(ex);
                    ex.log(log);
                }
            }
            if (this.text != null) {
                for (int i = 0; i < data.length; ++i)
                    text.println(data[i].getAssemblyId() + delimiter
                            + data[i].getPropertyTypeId() + delimiter
                            + data[i].getSampleTime() + delimiter
                            + data[i].getValue());
            }
        }
    }

    private void writeData(IntegerProperty[] data) {
        if (control.isToDatabase()) {
            // Add rows to the database.
            try {
                database.addProperty(data);
            } catch (AcsJTmcdbErrTypeEx err) {
                AcsJDatabaseErrorEx ex = new AcsJDatabaseErrorEx(err);
                control.addError(ex);
                ex.log(log);
            }
        }
        if (control.isToText()) {
            if (this.text == null) {
                try {
                    openTextFile();
                } catch (AcsJIOErrorEx ex) {
                    control.addError(ex);
                    ex.log(log);
                }
            }
            if (this.text != null) {
                for (int i = 0; i < data.length; ++i)
                    text.println(data[i].getAssemblyId() + delimiter
                            + data[i].getPropertyTypeId() + delimiter
                            + data[i].getSampleTime() + delimiter
                            + data[i].getValue());
            }
        }
    }

    private void writeData(StringProperty[] data) {
        if (control.isToDatabase()) {
            // Add rows to the database.
            try {
                database.addProperty(data);
            } catch (AcsJTmcdbErrTypeEx err) {
                AcsJDatabaseErrorEx ex = new AcsJDatabaseErrorEx(err);
                control.addError(ex);
                ex.log(log);
            }
        }
        if (control.isToText()) {
            if (this.text == null) {
                try {
                    openTextFile();
                } catch (AcsJIOErrorEx ex) {
                    control.addError(ex);
                    ex.log(log);
                }
            }
            if (this.text != null) {
                for (int i = 0; i < data.length; ++i)
                    text.println(data[i].getAssemblyId() + delimiter
                            + data[i].getPropertyTypeId() + delimiter
                            + data[i].getSampleTime() + delimiter
                            + data[i].getValue());
            }
        }
    }

    private void writeData(BooleanProperty[] data) {
        if (control.isToDatabase()) {
            // Add rows to the database.
            try {
                database.addProperty(data);
            } catch (AcsJTmcdbErrTypeEx err) {
                AcsJDatabaseErrorEx ex = new AcsJDatabaseErrorEx(err);
                control.addError(ex);
                ex.log(log);
            }
        }
        if (control.isToText()) {
            if (this.text == null) {
                try {
                    openTextFile();
                } catch (AcsJIOErrorEx ex) {
                    control.addError(ex);
                    ex.log(log);
                }
            }
            if (this.text != null) {
                for (int i = 0; i < data.length; ++i)
                    text.println(data[i].getAssemblyId() + delimiter
                            + data[i].getPropertyTypeId() + delimiter
                            + data[i].getSampleTime() + delimiter
                            + data[i].getValue());
            }
        }
    }

    private void writeData(EnumProperty[] data) {
        if (control.isToDatabase()) {
            // Add rows to the database.
            try {
                database.addProperty(data);
            } catch (AcsJTmcdbErrTypeEx err) {
                AcsJDatabaseErrorEx ex = new AcsJDatabaseErrorEx(err);
                control.addError(ex);
                ex.log(log);
            }
        }
        if (control.isToText()) {
            if (this.text == null) {
                try {
                    openTextFile();
                } catch (AcsJIOErrorEx ex) {
                    control.addError(ex);
                    ex.log(log);
                }
            }
            if (this.text != null) {
                for (int i = 0; i < data.length; ++i)
                    text.println(data[i].getAssemblyId() + delimiter
                            + data[i].getPropertyTypeId() + delimiter
                            + data[i].getSampleTime() + delimiter
                            + data[i].getValue());
            }
        }
    }

    public void run() {
        log.info(control.getComponentName()
                + ": WriteMonitorData thread is starting.");
        while (true) {
            
            long startTime = System.currentTimeMillis();
            
            // Switch the buffers.
            ArrayList<FloatProperty> fp = qFloat.switchBuffer();
            ArrayList<DoubleProperty> dp = qDouble.switchBuffer();
            ArrayList<IntegerProperty> ip = qInteger.switchBuffer();
            ArrayList<StringProperty> sp = qString.switchBuffer();
            ArrayList<BooleanProperty> bp = qBoolean.switchBuffer();
            ArrayList<EnumProperty> ep = qEnum.switchBuffer();

//            // A little sleep between steps.
//            try {
//                Thread.sleep(1000L);
//            } catch (InterruptedException err) {
//            }

            // Write float properties.
            int fpSize = fp.size();
            if (fpSize > 0) {
                FloatProperty[] fpa = new FloatProperty[fpSize];
                fpa = fp.toArray(fpa);
                writeData(fpa);
                int fpSize2 = fp.size();
                // Check for the pathological case.
                if (fpSize2 > fpSize) {
                    log.config("FloatProperty ArrayList grew from "+fpSize+" to "+fpSize2+" while writing the data to the database.");
                    fpa = new FloatProperty[fpSize2 - fpSize];
                    for (int i = 0; i < fpa.length; ++i)
                        fpa[i] = fp.get(fpSize+i);
                    writeData(fpa);
                    fpSize = fpSize2;
                }
                // Clear the buffer.
                fp.clear();
            }

            // Write double properties.
            int dpSize = dp.size();
            if (dpSize > 0) {
                DoubleProperty[] dpa = new DoubleProperty[dpSize];
                dpa = dp.toArray(dpa);
                writeData(dpa);
                int dpSize2 = dp.size();
                // Check for the pathological case.
                if (dpSize2 > dpSize) {
                    log.config("DoubleProperty ArrayList grew from "+dpSize+" to "+dpSize2+" while writing the data to the database.");
                    dpa = new DoubleProperty[dpSize2 - dpSize];
                    for (int i = 0; i < dpa.length; ++i)
                        dpa[i] = dp.get(dpSize+i);
                    writeData(dpa);
                    dpSize = dpSize2;
                }
                // Clear the buffer.
                dp.clear();
            }

            // Write integer properties.
            int ipSize = ip.size();
            if (ipSize > 0) {
                IntegerProperty[] ipa = new IntegerProperty[ipSize];
                ipa = ip.toArray(ipa);
                writeData(ipa);
                int ipSize2 = ip.size();
                // Check for the pathological case.
                if (ipSize2 > ipSize) {
                    log.config("IntegerProperty ArrayList grew from "+ipSize+" to "+ipSize2+" while writing the data to the database.");
                    ipa = new IntegerProperty[ipSize2 - ipSize];
                    for (int i = 0; i < ipa.length; ++i)
                        ipa[i] = ip.get(ipSize+i);
                    writeData(ipa);
                    ipSize = ipSize2;
                }
                // Clear the buffer.
                ip.clear();
            }

            // Write string properties.
            int spSize = sp.size();
            if (spSize > 0) {
                StringProperty[] spa = new StringProperty[spSize];
                spa = sp.toArray(spa);
                writeData(spa);
                int spSize2 = sp.size();
                // Check for the pathological case.
                if (spSize2 > spSize) {
                    log.config("StringProperty ArrayList grew from "+spSize+" to "+spSize2+" while writing the data to the database.");
                    spa = new StringProperty[spSize2 - spSize];
                    for (int i = 0; i < spa.length; ++i)
                        spa[i] = sp.get(spSize+i);
                    writeData(spa);
                    spSize = spSize2;
                }
                // Clear the buffer.
                sp.clear();
            }

            // Write boolean properties.
            int bpSize = bp.size();
            if (bpSize > 0) {
                BooleanProperty[] bpa = new BooleanProperty[bpSize];
                bpa = bp.toArray(bpa);
                writeData(bpa);
                int bpSize2 = bp.size();
                // Check for the pathological case.
                if (bpSize2 > bpSize) {
                    log.config("BooleanProperty ArrayList grew from "+bpSize+" to "+bpSize2+" while writing the data to the database.");
                    bpa = new BooleanProperty[bpSize2 - bpSize];
                    for (int i = 0; i < bpa.length; ++i)
                        bpa[i] = bp.get(bpSize+i);
                    writeData(bpa);
                    bpSize = bpSize2;
                }
                // Clear the buffer.
                bp.clear();
            }

            // Write enumeration properties.
            int epSize = ep.size();
            if (epSize > 0) {
                EnumProperty[] epa = new EnumProperty[epSize];
                epa = ep.toArray(epa);
                writeData(epa);
                int epSize2 = ep.size();
                // Check for the pathological case.
                if (epSize2 > epSize) {
                    log.config("EnumProperty ArrayList grew from "+epSize+" to "+epSize2+" while writing the data to the database.");
                    epa = new EnumProperty[epSize2 - epSize];
                    for (int i = 0; i < epa.length; ++i)
                        epa[i] = ep.get(epSize+i);
                    writeData(epa);
                    epSize = epSize2;
                }
                // Clear the buffer.
                ep.clear();
            }

            log.config(control.getComponentName()
                    + ".WriteMonitorData thread: Added monitor database rows: "
                    + fpSize + " floats, " + dpSize + " doubles, " + ipSize
                    + " integers, " + spSize + " strings, " + bpSize
                    + " booleans, " + epSize + " enumerations");

            // If there are any errors we will throw an Illegal thread state
            // exception.
            if (control.getErrors().length > 0) {
                AcsJTMCDBMonitorErrTypeEx [] errors = control.getErrors();
                for(int i =0; i< errors.length;i++){
                    errors[i].log(log);
                }
                control.clearErrors();
            }

            if (control.isStopMonitoring())
                break;

            long elapsedTime = System.currentTimeMillis() - startTime;
            log.config("Elapsed time: " + elapsedTime);
            
            if (elapsedTime > control.getSleepTimeInterval()) {
                log.severe("Storage time exceeded budget (" + control.getSleepTimeInterval() + 
                        "): " + elapsedTime);
                //will continue to try to archive. No abort. Just send alarm.
                //we need to do this safer though
                //return;
                if(!activeAlarm){
                    try{
                        send_alarm("MonitorInterface",control.getComponentName(),1,ACSFaultState.ACTIVE);
                        activeAlarm = true;
                    } catch (ACSASFactoryNotInitedEx e) {
                        AcsJACSASFactoryNotInitedEx ex = new AcsJACSASFactoryNotInitedEx(e);
                        ex.log(log);
                    } catch (SourceCreationErrorEx e) {
                        AcsJSourceCreationErrorEx ex = new AcsJSourceCreationErrorEx(e);
                        ex.log(log);
                    } catch (FaultStateCreationErrorEx e) {
                        AcsJFaultStateCreationErrorEx ex = new AcsJFaultStateCreationErrorEx(e);
                        ex.log(log);
                    }
                }
            }else if(activeAlarm){
                try{
                    send_alarm("MonitorInterface",control.getComponentName(),1,ACSFaultState.TERMINATE);
                    activeAlarm = false;
                } catch (ACSASFactoryNotInitedEx e) {
                    AcsJACSASFactoryNotInitedEx ex = new AcsJACSASFactoryNotInitedEx(e);
                    ex.log(log);
                } catch (SourceCreationErrorEx e) {
                    AcsJSourceCreationErrorEx ex = new AcsJSourceCreationErrorEx(e);
                    ex.log(log);
                } catch (FaultStateCreationErrorEx e) {
                    AcsJFaultStateCreationErrorEx ex = new AcsJFaultStateCreationErrorEx(e);
                    ex.log(log);
                }
            }
            
            synchronized(this) {
                try {
                    long sleepTime = control.getSleepTimeInterval()-elapsedTime;
                    if(sleepTime>0){
                        wait(control.getSleepTimeInterval()-elapsedTime);
                    }
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    log.config("WriteMonitorData thread interrupted during sleep: "+e.toString());
                }
            }            
        }
        if (text != null)
            closeTextFile();
        log.info(control.getComponentName()
                + ": WriteMonitorData thread has stopped.");
    }
    /**
     * sends an alarm to the ACS alarm system. Alarms must be configured in the CDB.
     * @param faultFamily
     * @param faultMember
     * @param faultCode
     * @param faultState
     * @throws ACSASFactoryNotInitedEx
     * @throws SourceCreationErrorEx
     * @throws FaultStateCreationErrorEx
     */
    protected void send_alarm(
            String faultFamily,
            String faultMember,
            int faultCode,
            String faultState) throws ACSASFactoryNotInitedEx, SourceCreationErrorEx, FaultStateCreationErrorEx
    {
        ACSAlarmSystemInterface alarmSource =
            ACSAlarmSystemInterfaceFactory.createSource(faultMember);
        ACSFaultState fs =
            ACSAlarmSystemInterfaceFactory.createFaultState(
                    faultFamily, faultMember, faultCode);
        fs.setDescriptor(faultState);
        fs.setUserTimestamp(new Timestamp(System.currentTimeMillis()));
        Properties props = new Properties();
        props.setProperty(ACSFaultState.ASI_PREFIX_PROPERTY, "prefix");
        props.setProperty(ACSFaultState.ASI_SUFFIX_PROPERTY, "suffix");
        fs.setUserProperties(props);
        alarmSource.push(fs);
    }

}
