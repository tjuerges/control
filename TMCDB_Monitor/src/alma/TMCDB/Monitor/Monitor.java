/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File Monitor.java
 */
package alma.TMCDB.Monitor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.Properties;

import alma.ACS.ComponentStates;
import alma.Control.MonitorInterfaceOperations;
import alma.Control.StoppedException;
import alma.Control.BadDataException;

import alma.TMCDB.TMCDB;
import alma.TMCDB.TMCDBException;
import alma.TMCDB.generated.BooleanProperty;
import alma.TMCDB.generated.DoubleProperty;
import alma.TMCDB.generated.EnumProperty;
import alma.TMCDB.generated.FloatProperty;
import alma.TMCDB.generated.IntegerProperty;
import alma.TMCDB.generated.StringProperty;
import alma.TMCDB.generated.Assembly;
import alma.TMCDB.generated.PropertyType;
import alma.TmcdbErrType.TmcdbErrorEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbConnectionFailureEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbErrTypeEx;
import alma.TmcdbErrType.wrappers.AcsJTmcdbSqlEx;
import alma.TMCDBMonitorErrType.wrappers.AcsJInvalidArgumentEx;
import alma.TMCDBMonitorErrType.wrappers.AcsJDatabaseErrorEx;
import alma.TMCDBMonitorErrType.wrappers.AcsJIOErrorEx;
import alma.acs.component.ComponentLifecycle;
import alma.acs.component.ComponentLifecycleException;
import alma.acs.container.ContainerServices;
import alma.acs.exceptions.AcsJException;
import alma.acs.nc.ArchiveConsumer;

import alma.hla.runtime.asdm.types.ArrayTime;

public class Monitor implements ComponentLifecycle, MonitorInterfaceOperations {
	
	/**
	 * Convert ACS time to ArrayTime
	 * 
	 * ACS: hundreds of nanoseconds since 15 October 1582 00:00:00 UTC
	 * ArrayTime: nanoseconds since 17 November 1858 00:00:00 UTC base
	 * difference in days: JD(17 November 1858 00:00:00 UTC) minus JD(15 October
	 * 1582 00:00:00 UTC) ArrayTime = (ACS.time * 100) minus (baseDifference *
	 * 86400000000000) ArrayTime = 100*ACS.time - (2400000.5 - 2299160.5) *
	 * 86400000000000 ArrayTime = 100*ACS.time - (100840) * 86400000000000
	 * ArrayTime = 100*ACS.time - 8712576000000000000
	 * 
	 * @param acsTime
	 *            The ACS time.
	 * @return the ArrayTime as a long.
	 */
	private static long acsTimeToArrayTime(long acsTime) {
		return 100L * acsTime - 8712576000000000000L;
	}
	public static ArrayTime convertACSTime(long acsTime) {
		return new ArrayTime(acsTimeToArrayTime(acsTime));
	}
	public static ArrayTime currentTime() {
		return new ArrayTime(System.currentTimeMillis() / 86400000.0 + 40587.0);
	}
	
	// The database to which the monitor data is to be written.
	private TMCDB database;
	
	// The Logger for log messages.
	private Logger log;
	
	// The Monitor component name.
	private String componentName;
	
	// Queues that house the monitor data.
	private Buffer<FloatProperty> qFloat;
	private Buffer<DoubleProperty> qDouble;
	private Buffer<IntegerProperty> qInteger;
	private Buffer<StringProperty> qString;
	private Buffer<BooleanProperty> qBoolean;
	private Buffer<EnumProperty> qEnum;
	
	// The object controlling the WriteMonitorData thread.
	private MonitorThreadControl control;
	// The WriteMonitorData thread.
	private WriteMonitorData writeDataThread;
	
	// The watch-dog thread.
	private WatchDog watchdog;
	// An object, shared with the watch-dog thread, used to abort the Monitor.
	private StopState stopState;
	private StoppedException hasStopped;

	// The name-to-id map.
	private NameToIDMap nameMap;

    enum DBType {
        ORACLE,
        HSQLDB
    };
    private DBType dbType;
    
	// The name of the database configuration.
	private String configurationName;
	// Database access parameters.
	private String dbUser;
	private String dbPassword;
	private String dbUrl;
    
    private ContainerServices container;
    private ArchiveConsumer consumer;

	private void logInfo(String msg) {
		log.info(configurationName + ".Monitor: " + msg);
	}
	
	private boolean checkStopState() {
		if (stopState.isStop()) {
			return true;
		}
		return false;
	}

	public Monitor() {
		database = null;
		log = null;
		componentName = "";
		qFloat = new Buffer<FloatProperty> ();
		qDouble = new Buffer<DoubleProperty> ();
		qInteger = new Buffer<IntegerProperty> ();
		qString = new Buffer<StringProperty> ();
		qBoolean = new Buffer<BooleanProperty> ();
		qEnum = new Buffer<EnumProperty> ();
		control = null;
		writeDataThread = null;
		watchdog = null;
		stopState = new StopState();
		hasStopped = null;
		nameMap = null;
		configurationName = null;
		dbUser = "";
		dbPassword = "";
		dbUrl = "";
	}

	// Component lifecycle methods.
	
	public void aboutToAbort() {
		cleanUp();
	}
	public void cleanUp() {
		try {
			stopMonitoring();
		} catch (StoppedException e) {
		}
	}
	public void execute() throws ComponentLifecycleException {
		logInfo("Execute complete.");
	}
	public void initialize(ContainerServices containerServices) throws ComponentLifecycleException {
        container = containerServices;
		log = containerServices.getLogger();
		componentName = containerServices.getName();
		hasStopped = new StoppedException (componentName + ".Monitor: Cannot execute request.  Monitoring has been stopped.");
        
        getConfiguration();
        
        try {
            setDatabaseAccess(dbUser, dbPassword, dbUrl);
        } catch (StoppedException ex) {
            ex.printStackTrace();
        } catch (BadDataException ex) {
            ex.printStackTrace();
        }
        
		try {
			accessDB();
			// Create a "default" control object.
			control = new MonitorThreadControl(database,log,componentName,qFloat,qDouble,qInteger,
					qString,qBoolean,qEnum);
			logInfo("Initialization complete.");
		} catch (AcsJInvalidArgumentEx e) {
			throw new ComponentLifecycleException(e);
		} catch (AcsJDatabaseErrorEx e) {
			throw new ComponentLifecycleException(e);
		}
        
	}

    private void getConfiguration() {
        
        Properties props = new Properties();
        
        String cwdConfLoc = "./dbConfig.properties";
        File cwdConf = new File(cwdConfLoc);
        
        String introotPath = System.getenv("ACSDATA");
        String introotConfLoc = introotPath + "/config/dbConfig.properties";
        File introotConf = new File(introotConfLoc);
        
        FileInputStream fis = null;
        if (cwdConf.exists()) {
            log.info("Reading configuration file from: " + cwdConfLoc);
            try {
                fis = new FileInputStream(cwdConfLoc);
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
            }
        } else if (introotConf.exists()) {
            log.info("Reading configuration file from: " + introotConfLoc);
            try {
                fis = new FileInputStream(introotConfLoc);
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
            }
        } else {
            // handle the case where there's no dbConfig.properties
        }
        
        try {
            props.load(fis);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        String tmcdbBackend = props.getProperty("tmcdb.db.backend");
        if (tmcdbBackend.equals("oracle")) {
            dbType = DBType.ORACLE;
            dbUser = props.getProperty("tmcdb.oracle.user");
            dbPassword = props.getProperty("tmcdb.oracle.passwd");
            dbUrl = props.getProperty("tmcdb.oracle.url");
            // log.info("dbUser="+dbUser+"; dbPassword="+dbPassword+"; dbUrl="+dbUrl+".");
        } else if (tmcdbBackend.equals("hsqldb")) {
            dbType = DBType.HSQLDB;
            dbUser = props.getProperty("tmcdb.hsqldb.user");
            dbPassword = props.getProperty("tmcdb.hsqldb.passwd");
            dbUrl = props.getProperty("tmcdb.hsqldb.url");            
            // log.info("dbUser="+dbUser+"; dbPassword="+dbPassword+"; dbUrl="+dbUrl+".");
        } else {
            // Bad setting
        }
        
        try {
            String cn = props.getProperty("tmcdb.confname");
            setConfigurationName(cn);
        } catch (StoppedException ex) {
            ex.printStackTrace();
        } catch (BadDataException ex) {
            ex.printStackTrace();
        }
    }
    
	// Other required methods.
    public ComponentStates componentState() {
        return container.getComponentStateManager().getCurrentState();
    }
    
    public String name() {
        return container.getName();
    }
    
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	public boolean equals(Object arg0) {
		return super.equals(arg0);
	}
	protected void finalize() throws Throwable {
		super.finalize();
	}
	public int hashCode() {
		return super.hashCode();
	}
	public String toString() {
		return super.toString();
	}

	
	public void setConfigurationName(String name) throws StoppedException, BadDataException {
		if (checkStopState())
			throw hasStopped;
		if (name == null || name.length() == 0)
			throw new BadDataException(componentName + ".Monitor: Configuration name in 'setConfigurationName' is null.");
		this.configurationName = name;
	}
	
	public void setDatabaseAccess(String dbUser, String dbPassword, String dbUrl) throws StoppedException, BadDataException {
		if (checkStopState())
			throw hasStopped;
		if (dbUser == null || dbUser.length() == 0)
			throw new BadDataException(componentName + ".Monitor: Database user name in 'setDatabaseAccess' is null.");
//		if (dbPassword == null || dbPassword.length() == 0)
//			throw new BadDataException(componentName + ".Monitor: Database password in 'setDatabaseAccess' is null.");
		if (dbUrl == null || dbUrl.length() == 0)
			throw new BadDataException(componentName + ".Monitor: Database URL in 'setDatabaseAccess' is null.");
		this.dbUser = dbUser;
		this.dbPassword = dbPassword;
		this.dbUrl = dbUrl;
	}
	
	/**
	 * Start the monitoring activity.
	 */
	public void startMonitoring() throws StoppedException, BadDataException {
		if (checkStopState())
			throw hasStopped;
		try {
			logInfo("Beginning startMonitoring.");
			accessNC();
			createNameMap();
			watchdog = new WatchDog(control,stopState);
			writeDataThread = new WriteMonitorData(watchdog,control);
			control.setWriteDataThread(writeDataThread);
			writeDataThread.start();
			activateNC();
			logInfo("StartMonitoring complete.");
		} catch (AcsJInvalidArgumentEx e) {
			throw new BadDataException(e.toString());
		} catch (AcsJDatabaseErrorEx e) {
			throw new BadDataException(e.toString());
		} catch (AcsJIOErrorEx e) {
			throw new BadDataException(e.toString());
		}
	}
	/**
	 * Start the monitoring activity with no notification-channel reader.
	 */
	public void startMonitoringNoNCReader() throws StoppedException, BadDataException {
		if (checkStopState())
			throw hasStopped;
		try {
			logInfo("Beginning startMonitoringNoNCReader.");
			accessDB();
			createNameMap();
			watchdog = new WatchDog(control,stopState);
			writeDataThread = new WriteMonitorData(watchdog,control);
			control.setWriteDataThread(writeDataThread);
			writeDataThread.start();
			logInfo("StartMonitoringNoNCReader complete.");
		} catch (AcsJInvalidArgumentEx e) {
			throw new BadDataException(e.toString());
		} catch (AcsJDatabaseErrorEx e) {
			throw new BadDataException(e.toString());
		} catch (AcsJIOErrorEx e) {
			throw new BadDataException(e.toString());
		}
	}
	/**
	 * Stop the monitoring activity.
	 */
	public void stopMonitoring() throws StoppedException {
		if (checkStopState())
			throw hasStopped;
		control.stopMonitoring();
		if (writeDataThread != null) {
			try {
				writeDataThread.join();
			} catch (InterruptedException e) {
			}
		}
		try {
			releaseDB();
			releaseNC();
		} catch (AcsJDatabaseErrorEx e) {
			e.log(log);
		}
		logInfo("StopMonitoring complete.");
	}

    public void receive(Long timestamp, String serialNumber,
            String propertyName, Object value) {
        log.finest("Received monitoring event.");
        log.finest("Timestamp: " + timestamp);
        log.finest("Serial Number: " + serialNumber);
        log.finest("Property Name: " + propertyName);
        log.finest("Property Value: " + value.toString());
        log.finest("Property Type: " + value.getClass().getName());
        
        IDPair idp = nameMap.getID(serialNumber, propertyName);
        if (idp == null) {
            log.finest("Property not found in nameMap!");
            return;
        } else {
            log.finest("Property found in nameMap.");
        }
        
        if (value instanceof Double) {
            double p;
            Double t = (Double) value;
            p = t.doubleValue();
            log.finest("Value: " + p);
            qDouble.store((new DoubleProperty(idp.getAssemblyId(),
                                              idp.getPropertyValue().getPropertyTypeId(),
                                              convertACSTime(timestamp),
                                              p)));
        } else if (value instanceof Float) {
            float f;
            Float t = (Float) value;
            f = t.floatValue();
            log.finest("Value: " + f);
            qFloat.store((new FloatProperty(idp.getAssemblyId(),
                                            idp.getPropertyValue().getPropertyTypeId(),
                                            convertACSTime(timestamp),
                                              f)));
        } else if (value instanceof Integer) {
            int i;
            Integer t = (Integer) value;
            i = t.intValue();
            log.finest("Value: " + i);
            qInteger.store((new IntegerProperty(idp.getAssemblyId(),
                                                idp.getPropertyValue().getPropertyTypeId(),
                                                convertACSTime(timestamp),
                                                i)));
        } else if (value instanceof String) {
            String s = (String) value;
            log.finest("Value: " + s);
            qString.store((new StringProperty(idp.getAssemblyId(),
                                                idp.getPropertyValue().getPropertyTypeId(),
                                                convertACSTime(timestamp),
                                                s)));
        } else if (value instanceof alma.ACS.Bool) {
            boolean b;
            alma.ACS.Bool t = (alma.ACS.Bool) value;
            if (t == alma.ACS.Bool.acsTRUE)
                b = true;
            else
                b = false;
            log.finest("Value: " + b);
            qBoolean.store((new BooleanProperty(idp.getAssemblyId(),
                                                idp.getPropertyValue().getPropertyTypeId(),
                                                convertACSTime(timestamp),
                                                b)));
        } else if (idp.getPropertyValue().getDatatype() == 6) {
            int i;
            Integer iv = null;
            Class cls = value.getClass();
            Method m = null;
            try {
                m = cls.getMethod("value", new Class[0]);
            } catch (SecurityException ex) {
                ex.printStackTrace();
            } catch (NoSuchMethodException ex) {
                ex.printStackTrace();
            }
            try {
                iv = (Integer) m.invoke(value, new Object[0]);
            } catch (IllegalArgumentException ex) {
                ex.printStackTrace();
            } catch (IllegalAccessException ex) {
                ex.printStackTrace();
            } catch (InvocationTargetException ex) {
                ex.printStackTrace();
            }
            i = iv.intValue();
            log.finest("Enum value: " + i);
            qEnum.store((new EnumProperty(idp.getAssemblyId(),
                                          idp.getPropertyValue().getPropertyTypeId(),
                                          convertACSTime(timestamp),
                                          i)));
        } else {
            log.finest("Unrecognized type: " + value.toString());
        }
    }
    

    public void receiveFloat(String serialNumber, String propertyName, long timestamp, float value) {
		if (checkStopState())
			return;
        if (nameMap == null) return;
		IDPair p = nameMap.getID(serialNumber, propertyName);
		if (p == null)
			return;
		if (p.getPropertyValue().getDatatype() != Monitor.FLOAT) {
			log.severe(componentName + ".Monitor:  For " + serialNumber + "." + propertyName + " at " + convertACSTime(timestamp).toFITS() + 
					" the property value has the wrong datatype! (Is " + Monitor.FLOAT + " should be " + p.getPropertyValue().getDatatype() + ")");
			return;
		}
		qFloat.store((new FloatProperty(p.getAssemblyId(),p.getPropertyValue().getPropertyTypeId(),convertACSTime(timestamp),value)));
	}
	public void receiveDouble(String serialNumber, String propertyName, long timestamp, double value) {
		if (checkStopState())
			return;
        if (nameMap == null) return;
		IDPair p = nameMap.getID(serialNumber, propertyName);
		if (p == null)
			return;
		if (p.getPropertyValue().getDatatype() != Monitor.DOUBLE) {
			log.severe(componentName + ".Monitor:  For " + serialNumber + "." + propertyName + " at " + convertACSTime(timestamp).toFITS() + 
					" the property value has the wrong datatype! (Is " + Monitor.DOUBLE + " should be " + p.getPropertyValue().getDatatype() + ")");
			return;
		}
		qDouble.store((new DoubleProperty(p.getAssemblyId(),p.getPropertyValue().getPropertyTypeId(),convertACSTime(timestamp),value)));
	}
	public void receiveInteger(String serialNumber, String propertyName, long timestamp, int value) {
		if (checkStopState())
			return;
        if (nameMap == null) return;
		IDPair p = nameMap.getID(serialNumber, propertyName);
		if (p == null)
			return;
		if (p.getPropertyValue().getDatatype() != Monitor.INTEGER) {
			log.severe(componentName + ".Monitor:  For " + serialNumber + "." + propertyName + " at " + convertACSTime(timestamp).toFITS() + 
					" the property value has the wrong datatype! (Is " + Monitor.INTEGER + " should be " + p.getPropertyValue().getDatatype() + ")");
			return;
		}
		qInteger.store((new IntegerProperty(p.getAssemblyId(),p.getPropertyValue().getPropertyTypeId(),convertACSTime(timestamp),value)));
	}
	public void receiveString(String serialNumber, String propertyName, long timestamp, String value) {
		if (checkStopState())
			return;
        if (nameMap == null) return;
		IDPair p = nameMap.getID(serialNumber, propertyName);
		if (p == null)
			return;
		if (p.getPropertyValue().getDatatype() != Monitor.STRING) {
			log.severe(componentName + ".Monitor:  For " + serialNumber + "." + propertyName + " at " + convertACSTime(timestamp).toFITS() + 
					" the property value has the wrong datatype! (Is " + Monitor.STRING + " should be " + p.getPropertyValue().getDatatype() + ")");
			return;
		}
		qString.store((new StringProperty(p.getAssemblyId(),p.getPropertyValue().getPropertyTypeId(),convertACSTime(timestamp),value)));
	}
	public void receiveBoolean(String serialNumber, String propertyName, long timestamp, boolean value) {
		if (checkStopState())
			return;
        if (nameMap == null) return;
		IDPair p = nameMap.getID(serialNumber, propertyName);
		if (p == null)
			return;
		if (p.getPropertyValue().getDatatype() != Monitor.BOOLEAN) {
			log.severe(componentName + ".Monitor:  For " + serialNumber + "." + propertyName + " at " + convertACSTime(timestamp).toFITS() + 
					" the property value has the wrong datatype! (Is " + Monitor.BOOLEAN + " should be " + p.getPropertyValue().getDatatype() + ")");
			return;
		}
		qBoolean.store((new BooleanProperty(p.getAssemblyId(),p.getPropertyValue().getPropertyTypeId(),convertACSTime(timestamp),value)));
	}
	public void receiveEnum(String serialNumber, String propertyName, long timestamp, byte value) {
		if (checkStopState())
			return;
        if (nameMap == null) return;
		IDPair p = nameMap.getID(serialNumber, propertyName);
		if (p == null)
			return;
		if (p.getPropertyValue().getDatatype() != Monitor.ENUM) {
			log.severe(componentName + ".Monitor:  For " + serialNumber + "." + propertyName + " at " + convertACSTime(timestamp).toFITS() + 
					" the property value has the wrong datatype! (Is " + Monitor.ENUM + " should be " + p.getPropertyValue().getDatatype() + ")");
			return;
		}
		qEnum.store((new EnumProperty(p.getAssemblyId(),p.getPropertyValue().getPropertyTypeId(),convertACSTime(timestamp),value)));
	}

	/**
	 * Turn off writing to the database.
	 */
	public void databaseOff() throws StoppedException {
		if (checkStopState())
			throw hasStopped;
		control.setDatabaseOff();
	}
	
	/**
	 * turn off writing to the database.
	 */
	public void databaseOn() throws StoppedException {
		if (checkStopState())
			throw hasStopped;
		control.setDatabaseOn();
	}
	
	/**
	 * Set the interval between dumping the data.
	 */
	public void setDumpInterval(int milliseconds) throws StoppedException, BadDataException {
		if (checkStopState())
			throw hasStopped;
		try {
			control.setSleepTimeInterval((long)milliseconds);
		} catch (AcsJInvalidArgumentEx err) {
			throw new BadDataException(err.toString());
		}
	}
	
	/**
	 * Turn off writing to the text file.
	 */
	public void textFileOff() throws StoppedException {
		if (checkStopState())
			throw hasStopped;
		control.setTextOff();
	}
	
	/**
	 * Turn on writing to the text file.
	 */
	public void textFileOn(String fullPathFilename) throws StoppedException, BadDataException {
		if (checkStopState())
			throw hasStopped;
		try {
			control.setTextOn(fullPathFilename);
		} catch (AcsJInvalidArgumentEx err) {
			throw new BadDataException(err.toString());
		}
	}
	
	/**
	 * Turn on writing to the text file and set the field delimiter.
	 */
	public void textFileOnAlt(String fullPathFilename, String delimiter) throws StoppedException, BadDataException {
		if (checkStopState())
			throw hasStopped;
		try {
			control.setTextOn(fullPathFilename, delimiter);
		} catch (AcsJInvalidArgumentEx err) {
			throw new BadDataException(err.toString());
		}
	}

	public static final int FLOAT 		= 0;
	public static final int DOUBLE 		= 1;
	public static final int INTEGER 	= 2;
	public static final int STRING 		= 3;
	public static final int BOOLEAN 	= 4;
	public static final int ENUM 		= 5;
	public static final int NOT_SUPPORTED	= 6;
	private int getType(org.omg.CORBA.Any value) {
		try {
			org.omg.CORBA.TypeCode typeCode = value.type();
			return getType(typeCode.toString());
		} catch (org.omg.CORBA.BAD_OPERATION err) {
			log.severe(componentName + ".Monitor:  " + err.toString());
			return NOT_SUPPORTED;
		}
	}
	private int getType(String type) {
		if (type.equals("float"))
			return FLOAT;
		else if (type.equals("double"))
			return DOUBLE;
		else if (type.equals("integer"))
			return INTEGER;
		else if (type.equals("string"))
			return STRING;
		else if (type.equals("boolean"))
			return BOOLEAN;
		else if (type.equals("octet"))
			return ENUM;
		else
			return NOT_SUPPORTED;					
	}

	private void accessDB() throws AcsJInvalidArgumentEx, AcsJDatabaseErrorEx {
		try {
			if (configurationName == null)
				throw new AcsJInvalidArgumentEx("Database configuration name is null.");
			database = new TMCDB();
            if (dbType == DBType.HSQLDB)
                database.connectHsqldb(dbUser, dbPassword, dbUrl);
            else if (dbType == DBType.ORACLE)
                database.connect(dbUser,dbPassword,dbUrl);
			logInfo("Connection to TMCDB established.");
			database.setConfigurationName(configurationName);
			logInfo("TMCDB configuration name set to " + database.getConfigurationName());
			logInfo("TMCDB configuration ID set to " + database.getConfigurationId());
			database.initialize();
			logInfo("TMCDB initialized.");
		} catch (AcsJTmcdbErrTypeEx e) {
            e.printStackTrace();
			throw new AcsJDatabaseErrorEx(e);
		}
	}

	private void releaseDB() throws AcsJDatabaseErrorEx {
		try {
			database.release();
		} catch (AcsJTmcdbErrTypeEx e) {
			throw new AcsJDatabaseErrorEx(e);
		}
	}

	private void createNameMap() throws AcsJDatabaseErrorEx {
		logInfo("Creating the property name map.");
		Assembly[] a = null;
		ArrayList<AssemblyProperty> list = null;
		try {
			a = database.getAssemblies();
			list = new ArrayList<AssemblyProperty> ();
			for (int i = 0; i < a.length; ++i) {
				log.info(">>> Getting properties for Assembly " + a[i].getAssemblyName());
				PropertyType[] p = database.getAssemblyProperties(a[i].getAssemblyName());
				for (int j = 0; j < p.length; ++j) {
					log.info(">>> Property " + p[j].getPropertyName());
					AssemblyProperty x = new AssemblyProperty(a[i].getSerialNumber(), 
							p[j].getPropertyName(),getType(p[j].getDataType()), p[j].getPropertyTypeId());
					list.add(x);
				}
			}
			log.info(">>> Got assemblies and properties.");
		} catch (AcsJTmcdbErrTypeEx e) {
			throw new AcsJDatabaseErrorEx(e);
		}
		AssemblyIdentification[] assembly = new AssemblyIdentification [a.length];
		for (int i = 0; i < assembly.length; ++i)
			assembly[i] = new AssemblyIdentification(a[i].getSerialNumber(),a[i].getAssemblyId());
		AssemblyProperty[] property = new AssemblyProperty [list.size()];
		property = list.toArray(property);
		nameMap = new NameToIDMap(property, assembly);
		logInfo("Property name map created: " + nameMap.toString());
	}
	
	private void accessNC() {
        try {
            consumer = new ArchiveConsumer(container, this);            
        } catch (AcsJException ex) {
            ex.printStackTrace();
        }
	}

	private void activateNC() {
        try {
            consumer.consumerReady();
        } catch (AcsJException ex) {
            ex.log(log);
        }
	}

	private void releaseNC() {
        // Not sure what to do here.
	}

}

class StopState {
	
	private boolean stop; 
	
	public StopState () {
		stop = false;
	}
	
	public synchronized boolean isStop() {
		return stop;
	}
	
	public synchronized void setStop() {
		stop = true;
	}
}

