/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File Queue.java
 */
package alma.TMCDB.Monitor;

import java.util.ArrayList;

/**
 * The Buffer class is a double buffered list of objects that are
 * stored in ArrayLists.  ArrayLists are used to avoid synchronization.
 * The double buffering scheme is used to avoid having to depend on
 * synchronization.
 * 
 * @author afarris
 *
 * @param <DataType> The data types are objects in the TMCDB Java
 * classes that represent the property tables: FloatProperty, 
 * DoubleProperty, etc.
 */
class Buffer<DataType> {
	private int bufferNumber;
	private ArrayList<DataType>[] buffer;
	
	public Buffer () {
		bufferNumber = 0;
		buffer = new ArrayList [2];
		buffer[0] = new ArrayList<DataType> ();
		buffer[1] = new ArrayList<DataType> ();
	}
	
	synchronized public ArrayList<DataType> switchBuffer() {
		ArrayList<DataType> a = buffer[bufferNumber];
		bufferNumber = bufferNumber == 0 ? 1 : 0;
		return a;
	}
	
	synchronized public void store (DataType x) {
		buffer[bufferNumber].add(x);
	}
}


