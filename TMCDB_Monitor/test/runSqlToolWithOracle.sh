#! /bin/sh
#
# Run HSQLDB's sqltool, optionally loading a file.

if [ ! -d ../tmp ]; then  mkdir ../tmp; fi

export CLASSPATH=../lib/hsqldb.jar:../lib/classes12.zip:$CLASSPATH
java org.hsqldb.util.SqlTool --rcFile ./sqltool.rc monitordata-atf $*


#
# O_o
