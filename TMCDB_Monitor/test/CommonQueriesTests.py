#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2007 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# srankin  2007-10-25  created
#

#************************************************************************
#   NAME
# 
#   SYNOPSIS
# 
#   DESCRIPTION
#
#   FILES
#
#   ENVIRONMENT
#
#   RETURN VALUES
#
#   CAUTIONS
#
#   EXAMPLES
#
#   SEE ALSO
#
#   BUGS     
#
#------------------------------------------------------------------------
#

import logging
import os

logging.info('Testing against Empty TMCDB...')
os.system("acsStartJava junit.textui.TestRunner alma.TMCDB.Query.CommonQueriesWithEmptyTMCDBTests")

logging.info('Loading Test TMCDB...')
os.system("java -jar ../lib/hsqldb.jar --rcFile ./sqltool.rc localhost-sa SQL/loadTypicalTestData-hsqldb.sql")

logging.info('Testing against Test TMCDB...')
os.system("acsStartJava junit.textui.TestRunner alma.TMCDB.Query.CommonQueriesWithTestTMCDBTests")

#
# ___oOo___
