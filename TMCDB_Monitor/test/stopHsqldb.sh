#! /bin/sh
#
# Stop the local copy of HSQLDB for local test.

pidFile="../tmp/Hsqldb.pid"

if [ ! -d ../tmp ]; then exit; fi
if [ ! -e ${pidFile} ]; then exit; fi

PID=`cat ${pidFile}`
# kill -9 ${PID}

java -jar ../lib/hsqldb.jar --rcFile ./sqltool.rc --sql "SHUTDOWN;" localhost-sa

ps aux | grep ${PID} | grep -v grep
if [ $? ]
then 
  rm -f ${pidFile}
else
  echo "Failed to stop Hsqldb process with PID=${PID}"
fi

#
# O_o
