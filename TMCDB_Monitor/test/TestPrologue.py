#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# rhiriart  2006-12-06  created
#

import sys, os, logging
import traceback
import unittest
import thread
import time
from TestCommon import *
                
# Create the temporary directory.
if os.path.exists('./tmp'):
    for f in os.listdir('./tmp'): os.unlink('./tmp/' + f)
    os.rmdir('./tmp')
os.mkdir('./tmp')
        
# Start database (if necessary.) If ORACLE is used instead of HSQLDB, it is asumed
# running and configured.
if dbBackend == HSQLDB:
    logging.info('Starting HSQLDB...')
    os.system('java -cp ../lib/hsqldb.jar org.hsqldb.Server &> ../tmp/HSQLStart.log &')
    time.sleep(1)
            
# Configure the database.
if dbBackend == HSQLDB:
    os.system("java -jar ../lib/hsqldb.jar --rcFile ./sqltool.rc localhost-sa SQL/DropAllTables.sql")
    os.system("java -jar ../lib/hsqldb.jar --rcFile ./sqltool.rc localhost-sa SQL/CreateHsqldbTables.sql")

#
# __oOo__
