INSERT INTO Configuration VALUES (
1,
'TMCDB-Common-Queries-Test',
'TMCDB Common Queries Test Database',
0,
''
);

INSERT INTO Computer VALUES (
1,
'dv01abm',
1,
'dv01abm',
'true',
'uni',
'Azimuth Platform'
);

INSERT INTO LoggingConfig VALUES (
1,
1,
2,
5,
'Log',
10,
10,
10,
1000
);

INSERT INTO Container VALUES (
1,
'container TBD',
'container Path TBD',
1,
1,
1,
'cpp',
'type modifiers TBD',
'true',
'ABM',
'kernel module location TBD',
'kernel module TBD',
0,
'command line args TBD',
-1,
5,
10,
240,
'true',
'autoload shared libraries TBD'
);

INSERT INTO ComponentType VALUES (
1,
'idl TBD 1'
);

INSERT INTO Component VALUES (
1,
'component name TBD',
1,
1,
1,
'cpp',
'true',
'code TBD',
'path TBD',
'false',
'false',
'false',
1,
-1,
-1
);

INSERT INTO BaseElement VALUES (
1,
1,
'Antenna',
'DV01',
1
);

INSERT INTO Antenna VALUES (
1,
1,
1,
'DV01',
'VA',
12.00,
1,
1.0,
1.0,
1.0,
1.0,
1.0,
1.0,
1
);

INSERT INTO LRUType VALUES (
'LRU type A',
'LRU full name TBD',
1,
0,
'LRU description TBD',
'LRU notes TBD'
);
INSERT INTO LRUType VALUES (
'LRU type B',
'LRU full name TBD',
1,
0,
'LRU description TBD',
'LRU notes TBD'
);

INSERT INTO AssemblyType VALUES (
'AssemblyA',
'LRU type A',
'Full name TBD',
'node address TBD',
1,
'base address TBD',
'description TBD',
'notes TBD',
1
);
INSERT INTO AssemblyType VALUES (
'AssemblyB',
'LRU type B',
'Full name TBD',
'node address TBD',
1,
'base address TBD',
'description TBD',
'notes TBD',
1
);

INSERT INTO Assembly VALUES (
1,
'AssemblyA',
1,
'CONTROL/DV01/AssemblyA1',
''
);

INSERT INTO Assembly VALUES (
2,
'AssemblyB',
1,
'CONTROL/DV01/AssemblyB1',
''
);

INSERT INTO Assembly VALUES (
3,
'AssemblyA',
1,
'CONTROL/DV02/AssemblyA2',
''
);

INSERT INTO Assembly VALUES (
4,
'AssemblyB',
1,
'CONTROL/DV02/AssemblyB2',
''
);

INSERT INTO BaseElementAssemblyList VALUES (
1,
1,
'Group Name TBD',
0,
'Role Name TBD',
0,
'Node Address TBD',
'Base Address TBD'
);

INSERT INTO BaseElementAssemblyList VALUES (
2,
1,
'Group Name TBD',
0,
'Role Name TBD',
0,
'Node Address TBD',
'Base Address TBD'
);

INSERT INTO PropertyType VALUES (
1,
'property name 1',
'AssemblyA',
'float',
'FloatProperty',
'rca TBD',
'false',
'raw data type TBD',
'world data type TBD',
'units TBD',
1.0,
0.0,
-5.0,
5.0,
1.0,
-5.0,
5.0,
'graph format TBD',
'graph title TBD',
'description TBD'
);

INSERT INTO PropertyType VALUES (
2,
'property name 2',
'AssemblyA',
'double',
'DoubleProperty',
'rca TBD',
'false',
'raw data type TBD',
'world data type TBD',
'units TBD',
1.0,
0.0,
-5.0,
5.0,
1.0,
-5.0,
5.0,
'graph format TBD',
'graph title TBD',
'description TBD'
);

INSERT INTO FloatProperty VALUES (
1,
1,
4698727800000000000,
1.0
);

COMMIT;
