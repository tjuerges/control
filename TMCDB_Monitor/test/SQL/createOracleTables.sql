-- TMCDB SQL TABLE DEFINITIONS Version 1.4 2007-MAR-9
--
-- /////////////////////////////////////////////////////////////////
-- // WARNING!  DO NOT MODIFY THIS FILE!                          //
-- //  ---------------------------------------------------------  //
-- // | This is generated code!  Do not modify this file.       | //
-- // | Any changes will be lost when the file is re-generated. | //
-- //  ---------------------------------------------------------  //
-- /////////////////////////////////////////////////////////////////
CREATE  TABLE Configuration (
    ConfigurationId NUMBER (10)     NOT NULL ,
    ConfigurationName VARCHAR    (32) NOT NULL ,
    FullName VARCHAR    (80) NOT NULL ,
    CreationTime NUMBER (19)     NOT NULL ,
    Description VARCHAR    (1024) NOT NULL ,
	CONSTRAINT ConfigKey PRIMARY KEY (ConfigurationId),
	CONSTRAINT ConfigAltKey UNIQUE (ConfigurationName)
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE Config_seq;
-- create trigger
CREATE OR REPLACE TRIGGER Config_SETID_TBI
 BEFORE INSERT ON Configuration
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select Config_seq.nextval INTO :new.ConfigurationId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE Computer (
    ComputerId NUMBER (10)     NOT NULL ,
    ComputerName VARCHAR    (80) NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    HostName VARCHAR    (80) NOT NULL ,
    RealTime CHAR (1) NOT NULL     ,
    ProcessorType CHAR    (3) NOT NULL ,
    PhysicalLocation VARCHAR    (1024) NULL ,
    CONSTRAINT ComputerRealTime CHECK (RealTime IN ('0', '1')),
	CONSTRAINT ComputerKey PRIMARY KEY (ComputerId),
	CONSTRAINT ComputerAltKey UNIQUE (ComputerName, ConfigurationId),
    CONSTRAINT ComputerConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
,
    CONSTRAINT ComputerProcessorType CHECK (ProcessorType IN ('uni', 'smp'))
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE Computer_seq;
-- create trigger
CREATE OR REPLACE TRIGGER Computer_SETID_TBI
 BEFORE INSERT ON Computer
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select Computer_seq.nextval INTO :new.ComputerId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE LoggingConfig (
    LoggingConfigId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    MinLogLevelDefault NUMBER (3)     DEFAULT 2,
    MinLogLevelLocalDefault NUMBER (3)     DEFAULT 2,
    CentralizedLogger VARCHAR    (16) DEFAULT 'Log',
    DispatchPacketSize NUMBER (3)     DEFAULT 10,
    ImmediateDispatchLevel NUMBER (3)     DEFAULT 10,
    FlushPeriodSeconds NUMBER (3)     DEFAULT 10,
    MaxLogQueueSize NUMBER (10)     DEFAULT 1000,
	CONSTRAINT LogginCKey PRIMARY KEY (LoggingConfigId),
    CONSTRAINT LoggingConfigConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE LogginC_seq;
-- create trigger
CREATE OR REPLACE TRIGGER LogginC_SETID_TBI
 BEFORE INSERT ON LoggingConfig
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select LogginC_seq.nextval INTO :new.LoggingConfigId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE NamedLoggerConfig (
    NamedLoggerConfigId NUMBER (10)     NOT NULL ,
    LoggingConfigId NUMBER (10)     NOT NULL ,
    Name VARCHAR    (64) NOT NULL ,
    MinLogLevel NUMBER (3)     DEFAULT 2,
    MinLogLevelLocal NUMBER (3)     DEFAULT 2,
	CONSTRAINT NamedLCKey PRIMARY KEY (NamedLoggerConfigId),
	CONSTRAINT NamedLCAltKey UNIQUE (LoggingConfigId, Name),
    CONSTRAINT NamedLoggerConfigLoggingConfig FOREIGN KEY (LoggingConfigId) REFERENCES LoggingConfig
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE NamedLC_seq;
-- create trigger
CREATE OR REPLACE TRIGGER NamedLC_SETID_TBI
 BEFORE INSERT ON NamedLoggerConfig
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select NamedLC_seq.nextval INTO :new.NamedLoggerConfigId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE Manager (
    ManagerId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    LoggingConfigId NUMBER (10)     NOT NULL ,
    Startup VARCHAR    (256) NULL ,
    ServiceComponents VARCHAR    (256) NULL ,
    Timeout NUMBER (10)     DEFAULT 50,
    ClientPingInterval NUMBER (10)     DEFAULT 60,
    AdministratorPingInterval NUMBER (10)     DEFAULT 45,
    ContainerPingInterval NUMBER (10)     DEFAULT 30,
    ServerThreads NUMBER (3)     DEFAULT 10,
    DeadlockTimeout NUMBER (10)     DEFAULT 180,
	CONSTRAINT ManagerKey PRIMARY KEY (ManagerId),
    CONSTRAINT ManagerLoggingConfig FOREIGN KEY (LoggingConfigId) REFERENCES LoggingConfig
,
    CONSTRAINT ManagerConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE Manager_seq;
-- create trigger
CREATE OR REPLACE TRIGGER Manager_SETID_TBI
 BEFORE INSERT ON Manager
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select Manager_seq.nextval INTO :new.ManagerId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE Container (
    ContainerId NUMBER (10)     NOT NULL ,
    ContainerName VARCHAR    (80) NOT NULL ,
    Path VARCHAR    (80) NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    LoggingConfigId NUMBER (10)     NOT NULL ,
    ComputerId NUMBER (10)     NULL ,
    ImplLang VARCHAR    (6) NOT NULL ,
    TypeModifiers VARCHAR    (64) NULL ,
    RealTime CHAR (1) DEFAULT    '0',
    RealTimeType VARCHAR    (4) DEFAULT 'NONE',
    KernelModuleLocation VARCHAR    (1024) NULL ,
    KernelModule VARCHAR    (1024) NULL ,
    AcsInstance NUMBER (3)     DEFAULT 0,
    CmdLineArgs VARCHAR    (256) NULL ,
    KeepAliveTime NUMBER (10)     DEFAULT -1,
    ServerThreads NUMBER (3)     DEFAULT 5,
    ManagerRetry NUMBER (10)     DEFAULT 10,
    CallTimeout NUMBER (10)     DEFAULT 240,
    Recovery CHAR (1) DEFAULT    '1',
    AutoloadSharedLibs VARCHAR    (64) NULL ,
    CONSTRAINT ContainerRealTime CHECK (RealTime IN ('0', '1')),
    CONSTRAINT ContainerRecovery CHECK (Recovery IN ('0', '1')),
	CONSTRAINT ContainerKey PRIMARY KEY (ContainerId),
	CONSTRAINT ContainerAltKey UNIQUE (ContainerName, Path, ConfigurationId),
    CONSTRAINT ContainerConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
,
    CONSTRAINT ContainerLoggingConfig FOREIGN KEY (LoggingConfigId) REFERENCES LoggingConfig
,
    CONSTRAINT ContainerImplLang CHECK (ImplLang IN ('java', 'cpp', 'py'))
,
    CONSTRAINT ContainerRealTimeType CHECK (RealTimeType IN ('NONE', 'ABM', 'CORR'))
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE Container_seq;
-- create trigger
CREATE OR REPLACE TRIGGER Container_SETID_TBI
 BEFORE INSERT ON Container
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select Container_seq.nextval INTO :new.ContainerId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE ComponentType (
    ComponentTypeId NUMBER (10)     NOT NULL ,
    IDL VARCHAR    (80) NOT NULL ,
	CONSTRAINT ComponTKey PRIMARY KEY (ComponentTypeId),
	CONSTRAINT ComponTAltKey UNIQUE (IDL)
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE ComponT_seq;
-- create trigger
CREATE OR REPLACE TRIGGER ComponT_SETID_TBI
 BEFORE INSERT ON ComponentType
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select ComponT_seq.nextval INTO :new.ComponentTypeId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE Component (
    ComponentId NUMBER (10)     NOT NULL ,
    ComponentName VARCHAR    (80) NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    ContainerId NUMBER (10)     NOT NULL ,
    ComponentInterfaceId NUMBER (10)     NOT NULL ,
    ImplLang VARCHAR    (6) NOT NULL ,
    RealTime CHAR (1) NOT NULL     ,
    Code VARCHAR    (80) NOT NULL ,
    Path VARCHAR    (80) NOT NULL ,
    IsAutostart CHAR (1) NOT NULL     ,
    IsDefault CHAR (1) NOT NULL     ,
    IsStandaloneDefined CHAR (1) NULL     ,
    KeepAliveTime NUMBER (10)     NOT NULL ,
    MinLogLevel NUMBER (3)     DEFAULT -1,
    MinLogLevelLocal NUMBER (3)     DEFAULT -1,
    CONSTRAINT ComponentRealTime CHECK (RealTime IN ('0', '1')),
    CONSTRAINT ComponentIsAutostart CHECK (IsAutostart IN ('0', '1')),
    CONSTRAINT ComponentIsDefault CHECK (IsDefault IN ('0', '1')),
    CONSTRAINT ComponentIsStanD CHECK (IsStandaloneDefined IN ('0', '1')),
	CONSTRAINT ComponentKey PRIMARY KEY (ComponentId),
	CONSTRAINT ComponentAltKey UNIQUE (Path, ComponentName, ConfigurationId),
    CONSTRAINT ComponentIDL FOREIGN KEY (ComponentInterfaceId) REFERENCES ComponentType
,
    CONSTRAINT ComponentConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
,
    CONSTRAINT ComponentImplLang CHECK (ImplLang IN ('java', 'cpp', 'py'))
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE Component_seq;
-- create trigger
CREATE OR REPLACE TRIGGER Component_SETID_TBI
 BEFORE INSERT ON Component
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select Component_seq.nextval INTO :new.ComponentId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE LRUType (
    LRUName VARCHAR    (32) NOT NULL ,
    FullName VARCHAR    (80) NOT NULL ,
    ICD VARCHAR    (80) NOT NULL ,
    ICDDate NUMBER (19)     NOT NULL ,
    Description VARCHAR    (1024) NOT NULL ,
    Notes VARCHAR    (1024) NULL ,
    CONSTRAINT LRUTypeKey PRIMARY KEY (LRUName)
);
CREATE  TABLE AssemblyType (
    AssemblyName VARCHAR    (80) NOT NULL ,
    LRUName VARCHAR    (32) NOT NULL ,
    FullName VARCHAR    (80) NOT NULL ,
    NodeAddress VARCHAR    (16) NOT NULL ,
    ChannelNumber NUMBER (3)     NOT NULL ,
    BaseAddress VARCHAR    (16) NULL ,
    Description VARCHAR    (1024) NOT NULL ,
    Notes VARCHAR    (1024) NULL ,
    ComponentTypeId NUMBER (10)     NOT NULL ,
    CONSTRAINT AssemblyTypeKey PRIMARY KEY (AssemblyName),
    CONSTRAINT AssemblyTypeLRUName FOREIGN KEY (LRUName) REFERENCES LRUType
,
    CONSTRAINT AssemblyTypeComponent FOREIGN KEY (ComponentTypeId) REFERENCES ComponentType
);
CREATE  TABLE PropertyType (
    PropertyTypeId NUMBER (10)     NOT NULL ,
    PropertyName VARCHAR    (32) NOT NULL ,
    AssemblyName VARCHAR    (80) NOT NULL ,
    DataType VARCHAR    (16) NOT NULL ,
    TableName VARCHAR    (32) NOT NULL ,
    RCA VARCHAR    (16) NOT NULL ,
    TeRelated CHAR (1) NOT NULL     ,
    RawDataType VARCHAR    (24) NOT NULL ,
    WorldDataType VARCHAR    (24) NOT NULL ,
    Units VARCHAR    (24) NULL ,
    Scale BINARY_DOUBLE     NULL ,
    Offset BINARY_DOUBLE     NULL ,
    MinRange BINARY_DOUBLE     NULL ,
    MaxRange BINARY_DOUBLE     NULL ,
    SamplingInterval BINARY_FLOAT     NOT NULL ,
    GraphMin BINARY_FLOAT     NULL ,
    GraphMax BINARY_FLOAT     NULL ,
    GraphFormat VARCHAR    (16) NULL ,
    GraphTitle VARCHAR    (80) NULL ,
    Description VARCHAR    (1024) NOT NULL ,
    CONSTRAINT PropertyTypeTeRelated CHECK (TeRelated IN ('0', '1')),
	CONSTRAINT PropertyTypeKey PRIMARY KEY (PropertyTypeId),
	CONSTRAINT PropertyTypeAltKey UNIQUE (PropertyName, AssemblyName),
    CONSTRAINT PropertyTypeAssemblyName FOREIGN KEY (AssemblyName) REFERENCES AssemblyType
,
    CONSTRAINT PropertyTypeDatatype CHECK (DataType IN ('float', 'double', 'boolean', 'string', 'integer', 'enum', 'blob'))
,
    CONSTRAINT PropertyTypeTableName CHECK (TableName IN ('FloatProperty', 'DoubleProperty', 'BooleanProperty', 'StringProperty', 'IntegerProperty', 'EnumProperty', 'BLOBProperty'))
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE PropertyType_seq;
-- create trigger
CREATE OR REPLACE TRIGGER PropertyType_SETID_TBI
 BEFORE INSERT ON PropertyType
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select PropertyType_seq.nextval INTO :new.PropertyTypeId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE BACIPropertyType (
    PropertyTypeId NUMBER (10)     NOT NULL ,
    PropertyName VARCHAR    (32) NOT NULL ,
    AssemblyName VARCHAR    (80) NOT NULL ,
    description VARCHAR    (1024) NOT NULL ,
    format VARCHAR    (16) NOT NULL ,
    units VARCHAR    (24) NOT NULL ,
    resolution NUMBER (10)     NOT NULL ,
    archive_priority NUMBER (10)     NOT NULL ,
    archive_min_int BINARY_DOUBLE     NOT NULL ,
    archive_max_int BINARY_DOUBLE     NOT NULL ,
    default_timer_trig BINARY_DOUBLE     NOT NULL ,
    min_timer_trig BINARY_DOUBLE     NOT NULL ,
    initialize_devio CHAR (1) NOT NULL     ,
    min_delta_trig BINARY_DOUBLE     NULL ,
    default_value VARCHAR    (1024) NOT NULL ,
    graph_min BINARY_DOUBLE     NULL ,
    graph_max BINARY_DOUBLE     NULL ,
    min_step BINARY_DOUBLE     NULL ,
    archive_delta BINARY_DOUBLE     NOT NULL ,
    alarm_high_on BINARY_DOUBLE     NULL ,
    alarm_low_on BINARY_DOUBLE     NULL ,
    alarm_high_off BINARY_DOUBLE     NULL ,
    alarm_low_off BINARY_DOUBLE     NULL ,
    alarm_timer_trig BINARY_DOUBLE     NULL ,
    min_value BINARY_DOUBLE     NULL ,
    max_value BINARY_DOUBLE     NULL ,
    bitDescription VARCHAR    (1024) NULL ,
    whenSet VARCHAR    (1024) NULL ,
    whenCleared VARCHAR    (1024) NULL ,
    statesDescription VARCHAR    (1024) NULL ,
    condition VARCHAR    (1024) NULL ,
    alarm_on VARCHAR    (1024) NULL ,
    alarm_off VARCHAR    (1024) NULL ,
    Data VARCHAR    (1024) NULL ,
    CONSTRAINT BACIPrTinitia CHECK (initialize_devio IN ('0', '1')),
	CONSTRAINT BACIPrTKey PRIMARY KEY (PropertyTypeId),
	CONSTRAINT BACIPrTAltKey UNIQUE (PropertyName, AssemblyName),
    CONSTRAINT BACIPropertyTypeAssemblyName FOREIGN KEY (AssemblyName) REFERENCES AssemblyType
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE BACIPrT_seq;
-- create trigger
CREATE OR REPLACE TRIGGER BACIPrT_SETID_TBI
 BEFORE INSERT ON BACIPropertyType
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select BACIPrT_seq.nextval INTO :new.PropertyTypeId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE EnumWord (
    PropertyTypeId NUMBER (10)     NOT NULL ,
    OrderNumber NUMBER (3)     NOT NULL ,
    Word VARCHAR    (32) NOT NULL ,
    CONSTRAINT EnumWordKey PRIMARY KEY (PropertyTypeId, OrderNumber),
    CONSTRAINT EnumWordPropertyType FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType
);
CREATE  TABLE Assembly (
    AssemblyId NUMBER (10)     NOT NULL ,
    AssemblyName VARCHAR    (80) NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    SerialNumber VARCHAR    (80) NOT NULL ,
    Data VARCHAR    (1024) NULL ,
	CONSTRAINT AssemblyKey PRIMARY KEY (AssemblyId),
	CONSTRAINT AssemblyAltKey UNIQUE (SerialNumber, ConfigurationId),
    CONSTRAINT AssemblyConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
,
    CONSTRAINT AssemblyName FOREIGN KEY (AssemblyName) REFERENCES AssemblyType
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE Assembly_seq;
-- create trigger
CREATE OR REPLACE TRIGGER Assembly_SETID_TBI
 BEFORE INSERT ON Assembly
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select Assembly_seq.nextval INTO :new.AssemblyId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE FloatProperty (
    AssemblyId NUMBER (10)     NOT NULL ,
    PropertyTypeId NUMBER (10)     NOT NULL ,
    SampleTime NUMBER (19)     NOT NULL ,
    Value BINARY_FLOAT     NOT NULL ,
    CONSTRAINT FloatPKey PRIMARY KEY (AssemblyId, PropertyTypeId, SampleTime),
    CONSTRAINT FloatPropertyAssemblyId FOREIGN KEY (AssemblyId) REFERENCES Assembly
,
    CONSTRAINT FloatPropertyTypeId FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType
);
CREATE  TABLE StringProperty (
    AssemblyId NUMBER (10)     NOT NULL ,
    PropertyTypeId NUMBER (10)     NOT NULL ,
    SampleTime NUMBER (19)     NOT NULL ,
    Value VARCHAR    (24) NOT NULL ,
    CONSTRAINT StringPKey PRIMARY KEY (AssemblyId, PropertyTypeId, SampleTime),
    CONSTRAINT StringPropertyAssemblyId FOREIGN KEY (AssemblyId) REFERENCES Assembly
,
    CONSTRAINT StringPropertyTypeId FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType
);
CREATE  TABLE BooleanProperty (
    AssemblyId NUMBER (10)     NOT NULL ,
    PropertyTypeId NUMBER (10)     NOT NULL ,
    SampleTime NUMBER (19)     NOT NULL ,
    Value CHAR (1) NOT NULL     ,
    CONSTRAINT BooleaPValue CHECK (Value IN ('0', '1')),
    CONSTRAINT BooleaPKey PRIMARY KEY (AssemblyId, PropertyTypeId, SampleTime),
    CONSTRAINT BooleanPropertyAssemblyId FOREIGN KEY (AssemblyId) REFERENCES Assembly
,
    CONSTRAINT BooleanPropertyTypeId FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType
);
CREATE  TABLE DoubleProperty (
    AssemblyId NUMBER (10)     NOT NULL ,
    PropertyTypeId NUMBER (10)     NOT NULL ,
    SampleTime NUMBER (19)     NOT NULL ,
    Value BINARY_DOUBLE     NOT NULL ,
    CONSTRAINT DoublePKey PRIMARY KEY (AssemblyId, PropertyTypeId, SampleTime),
    CONSTRAINT DoublePropertyAssemblyId FOREIGN KEY (AssemblyId) REFERENCES Assembly
,
    CONSTRAINT DoublePropertyTypeId FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType
);
CREATE  TABLE IntegerProperty (
    AssemblyId NUMBER (10)     NOT NULL ,
    PropertyTypeId NUMBER (10)     NOT NULL ,
    SampleTime NUMBER (19)     NOT NULL ,
    Value NUMBER (10)     NOT NULL ,
    CONSTRAINT IntegePKey PRIMARY KEY (AssemblyId, PropertyTypeId, SampleTime),
    CONSTRAINT IntegerPropertyAssemblyId FOREIGN KEY (AssemblyId) REFERENCES Assembly
,
    CONSTRAINT IntegerPropertyTypeId FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType
);
CREATE  TABLE EnumProperty (
    AssemblyId NUMBER (10)     NOT NULL ,
    PropertyTypeId NUMBER (10)     NOT NULL ,
    SampleTime NUMBER (19)     NOT NULL ,
    Value NUMBER (3)     NOT NULL ,
    CONSTRAINT EnumPropertyKey PRIMARY KEY (AssemblyId, PropertyTypeId, SampleTime),
    CONSTRAINT EnumPropertyAssemblyId FOREIGN KEY (AssemblyId) REFERENCES Assembly
,
    CONSTRAINT EnumPropertyTypeId FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType
);
CREATE  TABLE BlobProperty (
    AssemblyId NUMBER (10)     NOT NULL ,
    PropertyTypeId NUMBER (10)     NOT NULL ,
    SampleTime NUMBER (19)     NOT NULL ,
    DataType VARCHAR    (8) NOT NULL ,
    NumberItems NUMBER (5)     NOT NULL ,
    Value BLOB     NOT NULL ,
    CONSTRAINT BlobPropertyKey PRIMARY KEY (AssemblyId, PropertyTypeId, SampleTime),
    CONSTRAINT BlobPropertyAssemblyId FOREIGN KEY (AssemblyId) REFERENCES Assembly
,
    CONSTRAINT BlobPropertyTypeId FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType
,
    CONSTRAINT BlobPropertyValue CHECK (DataType IN ('float', 'double', 'boolean', 'string', 'integer'))
);
CREATE  TABLE BaseElement (
    BaseElementId NUMBER (10)     NOT NULL ,
    BaseId NUMBER (10)     NOT NULL ,
    BaseType VARCHAR    (24) NOT NULL ,
    BaseElementName VARCHAR    (24) NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
	CONSTRAINT BaseElementKey PRIMARY KEY (BaseElementId),
	CONSTRAINT BaseElementAltKey UNIQUE (BaseId, BaseType, ConfigurationId),
    CONSTRAINT BEConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
,
    CONSTRAINT BEType CHECK (BaseType IN ('Antenna', 'Pad', 'CorrQuadrant', 'FrontEnd', 'WeatherStation', 'CentralRack', 'MasterClock', 'HolographyTower', 'Array'))
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE BaseElement_seq;
-- create trigger
CREATE OR REPLACE TRIGGER BaseElement_SETID_TBI
 BEFORE INSERT ON BaseElement
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select BaseElement_seq.nextval INTO :new.BaseElementId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE BaseElementOnline (
    BaseElementOnlineId NUMBER (10)     NOT NULL ,
    BaseElementId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    StartTime NUMBER (19)     NOT NULL ,
    EndTime NUMBER (19)     NULL ,
    NormalTermination CHAR (1) NOT NULL     ,
    CONSTRAINT BaseElONormalT CHECK (NormalTermination IN ('0', '1')),
	CONSTRAINT BaseElOKey PRIMARY KEY (BaseElementOnlineId),
	CONSTRAINT BaseElOAltKey UNIQUE (BaseElementId, ConfigurationId, StartTime),
    CONSTRAINT BEOnlineId FOREIGN KEY (BaseElementId) REFERENCES BaseElement
,
    CONSTRAINT BEOnlineConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE BaseElO_seq;
-- create trigger
CREATE OR REPLACE TRIGGER BaseElO_SETID_TBI
 BEFORE INSERT ON BaseElementOnline
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select BaseElO_seq.nextval INTO :new.BaseElementOnlineId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE Antenna (
    AntennaId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    BaseElementId NUMBER (10)     NOT NULL ,
    AntennaName VARCHAR    (32) NOT NULL ,
    AntennaType VARCHAR    (4) NOT NULL ,
    DishDiameter BINARY_DOUBLE     NOT NULL ,
    CommissionDate NUMBER (19)     NOT NULL ,
    XPosition BINARY_DOUBLE     NOT NULL ,
    YPosition BINARY_DOUBLE     NOT NULL ,
    ZPosition BINARY_DOUBLE     NOT NULL ,
    XOffset BINARY_DOUBLE     NOT NULL ,
    YOffset BINARY_DOUBLE     NOT NULL ,
    ZOffset BINARY_DOUBLE     NOT NULL ,
    ComponentId NUMBER (10)     NOT NULL ,
	CONSTRAINT AntennaKey PRIMARY KEY (AntennaId),
	CONSTRAINT AntennaAltKey UNIQUE (AntennaName, ConfigurationId),
    CONSTRAINT AntennaConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
,
    CONSTRAINT AntennaBEId FOREIGN KEY (BaseElementId) REFERENCES BaseElement
,
    CONSTRAINT AntennaComponent FOREIGN KEY (ComponentId) REFERENCES Component
,
    CONSTRAINT AntennaType CHECK (AntennaType IN ('VA', 'AEC', 'ACA'))
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE Antenna_seq;
-- create trigger
CREATE OR REPLACE TRIGGER Antenna_SETID_TBI
 BEFORE INSERT ON Antenna
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select Antenna_seq.nextval INTO :new.AntennaId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE Pad (
    PadId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    BaseElementId NUMBER (10)     NOT NULL ,
    PadName VARCHAR    (32) NOT NULL ,
    CommissionDate NUMBER (19)     NOT NULL ,
    XPosition BINARY_DOUBLE     NOT NULL ,
    YPosition BINARY_DOUBLE     NOT NULL ,
    ZPosition BINARY_DOUBLE     NOT NULL ,
	CONSTRAINT PadKey PRIMARY KEY (PadId),
	CONSTRAINT PadAltKey UNIQUE (PadName, ConfigurationId),
    CONSTRAINT PadConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
,
    CONSTRAINT PadBEId FOREIGN KEY (BaseElementId) REFERENCES BaseElement
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE Pad_seq;
-- create trigger
CREATE OR REPLACE TRIGGER Pad_SETID_TBI
 BEFORE INSERT ON Pad
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select Pad_seq.nextval INTO :new.PadId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE CorrQuadrant (
    CorrQuadrantId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    BaseElementId NUMBER (10)     NOT NULL ,
    CorrName VARCHAR    (32) NOT NULL ,
    Quadrant NUMBER (3)     NOT NULL ,
    NumberOfAntennas NUMBER (3)     NOT NULL ,
    CommissionDate NUMBER (19)     NOT NULL ,
    ComponentId NUMBER (10)     NOT NULL ,
    NumberOfRacks NUMBER (3)     NOT NULL ,
    NumberOfBins NUMBER (3)     NOT NULL ,
	CONSTRAINT CorrQuadrantKey PRIMARY KEY (CorrQuadrantId),
	CONSTRAINT CorrQuadrantAltKey UNIQUE (CorrName, Quadrant, ConfigurationId),
    CONSTRAINT CorrQuadConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
,
    CONSTRAINT CorrQuadBEId FOREIGN KEY (BaseElementId) REFERENCES BaseElement
,
    CONSTRAINT CorrQuadComponent FOREIGN KEY (ComponentId) REFERENCES Component
,
    CONSTRAINT CorrQuadNumber CHECK (Quadrant IN ('0', '1', '2', '3'))
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE CorrQuadrant_seq;
-- create trigger
CREATE OR REPLACE TRIGGER CorrQuadrant_SETID_TBI
 BEFORE INSERT ON CorrQuadrant
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select CorrQuadrant_seq.nextval INTO :new.CorrQuadrantId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE CorrQuadrantRack (
    CorrQuadrantRackId NUMBER (10)     NOT NULL ,
    CorrQuadrantId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    RackName VARCHAR    (32) NOT NULL ,
    NumberOfBins NUMBER (3)     NOT NULL ,
	CONSTRAINT CorrQuRKey PRIMARY KEY (CorrQuadrantRackId),
	CONSTRAINT CorrQuRAltKey UNIQUE (RackName, CorrQuadrantId, ConfigurationId),
    CONSTRAINT CorrQuadRackConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE CorrQuR_seq;
-- create trigger
CREATE OR REPLACE TRIGGER CorrQuR_SETID_TBI
 BEFORE INSERT ON CorrQuadrantRack
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select CorrQuR_seq.nextval INTO :new.CorrQuadrantRackId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE CorrQuadrantBin (
    CorrQuadrantBinId NUMBER (10)     NOT NULL ,
    CorrQuadrantRackId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    BinName VARCHAR    (32) NOT NULL ,
    NumberOfCards NUMBER (5)     NOT NULL ,
	CONSTRAINT CorrQuBKey PRIMARY KEY (CorrQuadrantBinId),
	CONSTRAINT CorrQuBAltKey UNIQUE (BinName, CorrQuadrantRackId, ConfigurationId),
    CONSTRAINT CorrQuadBinConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE CorrQuB_seq;
-- create trigger
CREATE OR REPLACE TRIGGER CorrQuB_SETID_TBI
 BEFORE INSERT ON CorrQuadrantBin
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select CorrQuB_seq.nextval INTO :new.CorrQuadrantBinId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE FrontEnd (
    FrontEndId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    BaseElementId NUMBER (10)     NOT NULL ,
    FrontEndName VARCHAR    (32) NOT NULL ,
    CommissionDate NUMBER (19)     NOT NULL ,
    ComponentId NUMBER (10)     NOT NULL ,
	CONSTRAINT FrontEndKey PRIMARY KEY (FrontEndId),
	CONSTRAINT FrontEndAltKey UNIQUE (FrontEndName, ConfigurationId),
    CONSTRAINT FrontEndConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
,
    CONSTRAINT FrontEndBEId FOREIGN KEY (BaseElementId) REFERENCES BaseElement
,
    CONSTRAINT FrontEndComponent FOREIGN KEY (ComponentId) REFERENCES Component
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE FrontEnd_seq;
-- create trigger
CREATE OR REPLACE TRIGGER FrontEnd_SETID_TBI
 BEFORE INSERT ON FrontEnd
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select FrontEnd_seq.nextval INTO :new.FrontEndId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE WeatherStation (
    WeatherStationId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    BaseElementId NUMBER (10)     NOT NULL ,
    SerialNumber VARCHAR    (32) NOT NULL ,
    WeatherStationName VARCHAR    (32) NOT NULL ,
    WeatherStationType VARCHAR    (32) NOT NULL ,
    CommissionDate NUMBER (19)     NOT NULL ,
    XPosition BINARY_DOUBLE     NOT NULL ,
    YPosition BINARY_DOUBLE     NOT NULL ,
    ZPosition BINARY_DOUBLE     NOT NULL ,
    ComponentId NUMBER (10)     NOT NULL ,
	CONSTRAINT WeatheSKey PRIMARY KEY (WeatherStationId),
	CONSTRAINT WeatheSAltKey UNIQUE (WeatherStationName, ConfigurationId),
    CONSTRAINT WeatherStationComponent FOREIGN KEY (ComponentId) REFERENCES Component
,
    CONSTRAINT WeatherStationConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
,
    CONSTRAINT WeatherStationBEId FOREIGN KEY (BaseElementId) REFERENCES BaseElement
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE WeatheS_seq;
-- create trigger
CREATE OR REPLACE TRIGGER WeatheS_SETID_TBI
 BEFORE INSERT ON WeatherStation
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select WeatheS_seq.nextval INTO :new.WeatherStationId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE CentralRack (
    CentralRackId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    BaseElementId NUMBER (10)     NOT NULL ,
    CentralRackName VARCHAR    (32) NOT NULL ,
    CommissionDate NUMBER (19)     NOT NULL ,
    ComponentId NUMBER (10)     NOT NULL ,
	CONSTRAINT CentralRackKey PRIMARY KEY (CentralRackId),
	CONSTRAINT CentralRackAltKey UNIQUE (CentralRackName, ConfigurationId),
    CONSTRAINT CentralRackComponent FOREIGN KEY (ComponentId) REFERENCES Component
,
    CONSTRAINT CentralRackConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
,
    CONSTRAINT CentralRackBEId FOREIGN KEY (BaseElementId) REFERENCES BaseElement
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE CentralRack_seq;
-- create trigger
CREATE OR REPLACE TRIGGER CentralRack_SETID_TBI
 BEFORE INSERT ON CentralRack
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select CentralRack_seq.nextval INTO :new.CentralRackId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE MasterClock (
    MasterClockId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    BaseElementId NUMBER (10)     NOT NULL ,
    MasterClockName VARCHAR    (32) NOT NULL ,
    CommissionDate NUMBER (19)     NOT NULL ,
    ComponentId NUMBER (10)     NOT NULL ,
	CONSTRAINT MasterClockKey PRIMARY KEY (MasterClockId),
	CONSTRAINT MasterClockAltKey UNIQUE (MasterClockName, ConfigurationId),
    CONSTRAINT MasterClockComponent FOREIGN KEY (ComponentId) REFERENCES Component
,
    CONSTRAINT MasterClockConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
,
    CONSTRAINT MasterClockBEId FOREIGN KEY (BaseElementId) REFERENCES BaseElement
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE MasterClock_seq;
-- create trigger
CREATE OR REPLACE TRIGGER MasterClock_SETID_TBI
 BEFORE INSERT ON MasterClock
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select MasterClock_seq.nextval INTO :new.MasterClockId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE HolographyTower (
    HolographyTowerId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    BaseElementId NUMBER (10)     NOT NULL ,
    HolographyTowerName VARCHAR    (32) NOT NULL ,
    CommissionDate NUMBER (19)     NOT NULL ,
    XPosition BINARY_DOUBLE     NOT NULL ,
    YPosition BINARY_DOUBLE     NOT NULL ,
    ZPosition BINARY_DOUBLE     NOT NULL ,
	CONSTRAINT HologrTKey PRIMARY KEY (HolographyTowerId),
	CONSTRAINT HologrTAltKey UNIQUE (HolographyTowerName, ConfigurationId),
    CONSTRAINT HolographyTowerConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
,
    CONSTRAINT HolographyTowerBEId FOREIGN KEY (BaseElementId) REFERENCES BaseElement
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE HologrT_seq;
-- create trigger
CREATE OR REPLACE TRIGGER HologrT_SETID_TBI
 BEFORE INSERT ON HolographyTower
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select HologrT_seq.nextval INTO :new.HolographyTowerId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE Array (
    ArrayId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    BaseElementId NUMBER (10)     NOT NULL ,
    ArrayName VARCHAR    (32) NOT NULL ,
    Type VARCHAR    (9) NOT NULL ,
    UserId VARCHAR    (80) NULL ,
    StartTime NUMBER (19)     NOT NULL ,
    EndTime NUMBER (19)     NULL ,
    NormalTermination CHAR (1) NOT NULL     ,
    ComponentId NUMBER (10)     NOT NULL ,
    CONSTRAINT ArrayNormalT CHECK (NormalTermination IN ('0', '1')),
	CONSTRAINT ArrayKey PRIMARY KEY (ArrayId),
	CONSTRAINT ArrayAltKey UNIQUE (ArrayName, StartTime, ConfigurationId),
    CONSTRAINT ArrayConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
,
    CONSTRAINT ArrayComponent FOREIGN KEY (ComponentId) REFERENCES Component
,
    CONSTRAINT ArrayBEId FOREIGN KEY (BaseElementId) REFERENCES BaseElement
,
    CONSTRAINT ArrayType CHECK (Type IN ('automatic', 'manual'))
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE Array_seq;
-- create trigger
CREATE OR REPLACE TRIGGER Array_SETID_TBI
 BEFORE INSERT ON Array
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select Array_seq.nextval INTO :new.ArrayId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE AntennaToPad (
    AntennaId NUMBER (10)     NOT NULL ,
    PadId NUMBER (10)     NOT NULL ,
    StartTime NUMBER (19)     NOT NULL ,
    EndTime NUMBER (19)     NULL ,
    Planned CHAR (1) NOT NULL     ,
    CONSTRAINT AntennaToPadPlanned CHECK (Planned IN ('0', '1')),
    CONSTRAINT AntennaToPadKey PRIMARY KEY (AntennaId, PadId, StartTime),
    CONSTRAINT AntennaToPadAntennaId FOREIGN KEY (AntennaId) REFERENCES Antenna
,
    CONSTRAINT AntennaToPadPadId FOREIGN KEY (PadId) REFERENCES Pad
);
CREATE  TABLE AntennaPointingModel (
    PointingModelId NUMBER (10)     NOT NULL ,
    AntennaId NUMBER (10)     NOT NULL ,
    PadId NUMBER (10)     NOT NULL ,
    StartValidTime NUMBER (19)     NOT NULL ,
    EndValidTime NUMBER (19)     NULL ,
    AsdmUID VARCHAR    (80) NOT NULL ,
	CONSTRAINT AntennPMKey PRIMARY KEY (PointingModelId),
	CONSTRAINT AntennPMAltKey UNIQUE (AntennaId, PadId, StartValidTime),
    CONSTRAINT AntennaPMAntennaId FOREIGN KEY (AntennaId) REFERENCES Antenna
,
    CONSTRAINT AntennaPMPadId FOREIGN KEY (PadId) REFERENCES Pad
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE AntennPM_seq;
-- create trigger
CREATE OR REPLACE TRIGGER AntennPM_SETID_TBI
 BEFORE INSERT ON AntennaPointingModel
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select AntennPM_seq.nextval INTO :new.PointingModelId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE AntennaPointingModelTerm (
    PointingModelId NUMBER (10)     NOT NULL ,
    CoeffName VARCHAR    (32) NOT NULL ,
    CoeffValue BINARY_FLOAT     NOT NULL ,
    CoeffError BINARY_FLOAT     NOT NULL ,
    CONSTRAINT AntennPMTKey PRIMARY KEY (PointingModelId, CoeffName),
    CONSTRAINT AntPMTermPointingModelId FOREIGN KEY (PointingModelId) REFERENCES AntennaPointingModel
);
CREATE  TABLE AntennaDelayTerms (
    AntennaId NUMBER (10)     NOT NULL ,
    PadId NUMBER (10)     NOT NULL ,
    TimeOfMeasurement NUMBER (19)     NOT NULL ,
    ParameterName VARCHAR    (32) NOT NULL ,
    ParameterValue BINARY_DOUBLE     NOT NULL ,
    CONSTRAINT AntennDTKey PRIMARY KEY (AntennaId, PadId, TimeOfMeasurement),
    CONSTRAINT AntennaDTAntennaId FOREIGN KEY (AntennaId) REFERENCES Antenna
,
    CONSTRAINT AntennaDTPadId FOREIGN KEY (PadId) REFERENCES Pad
);
CREATE  TABLE AntennaToFrontEnd (
    AntennaId NUMBER (10)     NOT NULL ,
    FrontEndId NUMBER (10)     NOT NULL ,
    StartTime NUMBER (19)     NOT NULL ,
    EndTime NUMBER (19)     NULL ,
    CONSTRAINT AntennTFEKey PRIMARY KEY (AntennaId, FrontEndId, StartTime),
    CONSTRAINT AntennaToFEAntennaId FOREIGN KEY (AntennaId) REFERENCES Antenna
,
    CONSTRAINT AntennaToFEFrontEndId FOREIGN KEY (FrontEndId) REFERENCES FrontEnd
);
CREATE  TABLE AntennaToArray (
    AntennaId NUMBER (10)     NOT NULL ,
    ArrayId NUMBER (10)     NOT NULL ,
    CONSTRAINT AntennTAKey PRIMARY KEY (AntennaId, ArrayId),
    CONSTRAINT AntennaToArrayAntennaId FOREIGN KEY (AntennaId) REFERENCES Antenna
,
    CONSTRAINT AntennaToArrayArrayid FOREIGN KEY (ArrayId) REFERENCES Array
);
CREATE  TABLE SBExecution (
    ArrayId NUMBER (10)     NOT NULL ,
    SbUID VARCHAR    (80) NOT NULL ,
    StartTime NUMBER (19)     NOT NULL ,
    EndTime NUMBER (19)     NULL ,
    NormalTermination CHAR (1) NOT NULL     ,
    CONSTRAINT SBExecutionNormalT CHECK (NormalTermination IN ('0', '1')),
    CONSTRAINT SBExecutionKey PRIMARY KEY (ArrayId, SbUID, StartTime),
    CONSTRAINT SBExecutionArrayId FOREIGN KEY (ArrayId) REFERENCES Array
);
CREATE  TABLE AntennaToCorr (
    AntennaId NUMBER (10)     NOT NULL ,
    CorrId NUMBER (10)     NOT NULL ,
    AntennaNumber NUMBER (3)     NOT NULL ,
    StartTime NUMBER (19)     NOT NULL ,
    EndTime NUMBER (19)     NULL ,
    CONSTRAINT AntennTCKey PRIMARY KEY (AntennaId, CorrId, StartTime),
    CONSTRAINT AntToCorrAntennaId FOREIGN KEY (AntennaId) REFERENCES Antenna
,
    CONSTRAINT AntToCorrId FOREIGN KEY (CorrId) REFERENCES CorrQuadrant
);
CREATE  TABLE BaseElementAssemblyGroup (
    GroupName VARCHAR    (32) NOT NULL ,
    BaseElementId NUMBER (10)     NOT NULL ,
    CONSTRAINT BaseElAGKey PRIMARY KEY (GroupName, BaseElementId),
    CONSTRAINT BEAssemblyGroupId FOREIGN KEY (BaseElementId) REFERENCES BaseElement
);
CREATE  TABLE BaseElementAssemblyList (
    AssemblyId NUMBER (10)     NOT NULL ,
    BaseElementId NUMBER (10)     NOT NULL ,
    GroupName VARCHAR    (32) NULL ,
    OrderTag NUMBER (5)     NULL ,
    RoleName VARCHAR    (32) NULL ,
    ChannelNumber NUMBER (3)     NULL ,
    NodeAddress VARCHAR    (16) NULL ,
    BaseAddress VARCHAR    (16) NULL ,
    CONSTRAINT BaseElALKey PRIMARY KEY (AssemblyId, BaseElementId),
    CONSTRAINT BEAssemblyListId FOREIGN KEY (BaseElementId) REFERENCES BaseElement
,
    CONSTRAINT BEAssemblyListAssemblyId FOREIGN KEY (AssemblyId) REFERENCES Assembly
);
CREATE  TABLE MasterComponent (
    MasterComponentId NUMBER (10)     NOT NULL ,
    ComponentId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    SubsystemName VARCHAR    (80) NOT NULL ,
	CONSTRAINT MasterCKey PRIMARY KEY (MasterComponentId),
	CONSTRAINT MasterCAltKey UNIQUE (ComponentId, ConfigurationId),
    CONSTRAINT MComponentConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
,
    CONSTRAINT MComponentId FOREIGN KEY (ComponentId) REFERENCES Component
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE MasterC_seq;
-- create trigger
CREATE OR REPLACE TRIGGER MasterC_SETID_TBI
 BEFORE INSERT ON MasterComponent
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select MasterC_seq.nextval INTO :new.MasterComponentId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE ComputerExecution (
    ComputerExecutionId NUMBER (10)     NOT NULL ,
    ComputerId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    StartTime NUMBER (19)     NOT NULL ,
    EndTime NUMBER (19)     NULL ,
    NormalStart CHAR (1) NOT NULL     ,
    NormalTermination CHAR (1) NOT NULL     ,
    CONSTRAINT ComputENormalStart CHECK (NormalStart IN ('0', '1')),
    CONSTRAINT ComputENormalT CHECK (NormalTermination IN ('0', '1')),
	CONSTRAINT ComputEKey PRIMARY KEY (ComputerExecutionId),
	CONSTRAINT ComputEAltKey UNIQUE (ComputerId, ConfigurationId, StartTime),
    CONSTRAINT ComputerExecConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
,
    CONSTRAINT ComputerExecComputer FOREIGN KEY (ComputerId) REFERENCES Computer
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE ComputE_seq;
-- create trigger
CREATE OR REPLACE TRIGGER ComputE_SETID_TBI
 BEFORE INSERT ON ComputerExecution
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select ComputE_seq.nextval INTO :new.ComputerExecutionId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE ContainerExecution (
    ContainerExecutionId NUMBER (10)     NOT NULL ,
    ContainerId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    ComputerId NUMBER (10)     NOT NULL ,
    StartTime NUMBER (19)     NOT NULL ,
    EndTime NUMBER (19)     NULL ,
    NormalStart CHAR (1) NOT NULL     ,
    NormalTermination CHAR (1) NOT NULL     ,
    CONSTRAINT ContaiENormalStart CHECK (NormalStart IN ('0', '1')),
    CONSTRAINT ContaiENormalT CHECK (NormalTermination IN ('0', '1')),
	CONSTRAINT ContaiEKey PRIMARY KEY (ContainerExecutionId),
	CONSTRAINT ContaiEAltKey UNIQUE (ContainerId, ComputerId, StartTime, ConfigurationId),
    CONSTRAINT ContainerExecConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
,
    CONSTRAINT ContainerExecContainer FOREIGN KEY (ContainerId) REFERENCES Container
,
    CONSTRAINT ContainerExecComputer FOREIGN KEY (ComputerId) REFERENCES Computer
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE ContaiE_seq;
-- create trigger
CREATE OR REPLACE TRIGGER ContaiE_SETID_TBI
 BEFORE INSERT ON ContainerExecution
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select ContaiE_seq.nextval INTO :new.ContainerExecutionId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE ComponentExecution (
    ComponentExecutionId NUMBER (10)     NOT NULL ,
    ComponentExecutionAcsId NUMBER (19)     NOT NULL ,
    ComponentId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    ContainerId NUMBER (10)     NOT NULL ,
    StartTime NUMBER (19)     NOT NULL ,
    EndTime NUMBER (19)     NULL ,
    NormalStart CHAR (1) NOT NULL     ,
    NormalTermination CHAR (1) NOT NULL     ,
    BaseElementOnlineId NUMBER (10)     NULL ,
    AssemblyId NUMBER (10)     NULL ,
    CONSTRAINT ComponENormalStart CHECK (NormalStart IN ('0', '1')),
    CONSTRAINT ComponENormalT CHECK (NormalTermination IN ('0', '1')),
	CONSTRAINT ComponEKey PRIMARY KEY (ComponentExecutionId),
	CONSTRAINT ComponEAltKey UNIQUE (ComponentId, StartTime, ConfigurationId),
    CONSTRAINT ComponentExecConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
,
    CONSTRAINT ComponentExecComponent FOREIGN KEY (ComponentId) REFERENCES Component
,
    CONSTRAINT ComponentExecContainer FOREIGN KEY (ContainerId) REFERENCES Container
,
    CONSTRAINT ComponentExecBaseElement FOREIGN KEY (BaseElementOnlineId) REFERENCES BaseElementOnline
,
    CONSTRAINT ComponentExecAssembly FOREIGN KEY (AssemblyId) REFERENCES Assembly
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE ComponE_seq;
-- create trigger
CREATE OR REPLACE TRIGGER ComponE_SETID_TBI
 BEFORE INSERT ON ComponentExecution
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select ComponE_seq.nextval INTO :new.ComponentExecutionId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE MasterComponentExecution (
    MasterComponentExecId NUMBER (10)     NOT NULL ,
    MasterComponentId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    ContainerId NUMBER (10)     NOT NULL ,
    StartTime NUMBER (19)     NOT NULL ,
    EndTime NUMBER (19)     NULL ,
    NormalStart CHAR (1) NOT NULL     ,
    NormalTermination CHAR (1) NOT NULL     ,
    CONSTRAINT MasterCENormalStart CHECK (NormalStart IN ('0', '1')),
    CONSTRAINT MasterCENormalT CHECK (NormalTermination IN ('0', '1')),
	CONSTRAINT MasterCEKey PRIMARY KEY (MasterComponentExecId),
	CONSTRAINT MasterCEAltKey UNIQUE (MasterComponentId, ContainerId, StartTime, ConfigurationId),
    CONSTRAINT MComponentExecConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
,
    CONSTRAINT MComponentExecComponent FOREIGN KEY (MasterComponentId) REFERENCES MasterComponent
,
    CONSTRAINT MComponentExecContainer FOREIGN KEY (ContainerId) REFERENCES Container
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE MasterCE_seq;
-- create trigger
CREATE OR REPLACE TRIGGER MasterCE_SETID_TBI
 BEFORE INSERT ON MasterComponentExecution
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select MasterCE_seq.nextval INTO :new.MasterComponentExecId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE ACS (
    ACSId NUMBER (10)     NOT NULL ,
    ACSVersion VARCHAR    (80) NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    ComputerId NUMBER (10)     NOT NULL ,
    Var1 VARCHAR    (80) NULL ,
    Var2 VARCHAR    (80) NULL ,
    Var3 VARCHAR    (80) NULL ,
	CONSTRAINT ACSKey PRIMARY KEY (ACSId),
	CONSTRAINT ACSAltKey UNIQUE (ACSVersion, ConfigurationId, ComputerId),
    CONSTRAINT ACSComputer FOREIGN KEY (ComputerId) REFERENCES Computer
,
    CONSTRAINT ACSConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE ACS_seq;
-- create trigger
CREATE OR REPLACE TRIGGER ACS_SETID_TBI
 BEFORE INSERT ON ACS
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select ACS_seq.nextval INTO :new.ACSId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE ACSExecution (
    ACSId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    StartTime NUMBER (19)     NOT NULL ,
    EndTime NUMBER (19)     NULL ,
    NormalStart CHAR (1) NOT NULL     ,
    NormalTermination CHAR (1) NOT NULL     ,
    CONSTRAINT ACSExecutionNormalStart CHECK (NormalStart IN ('0', '1')),
    CONSTRAINT ACSExecutionNormalT CHECK (NormalTermination IN ('0', '1')),
    CONSTRAINT ACSExecutionKey PRIMARY KEY (ACSId, ConfigurationId, StartTime),
    CONSTRAINT ACSExecACS FOREIGN KEY (ACSId) REFERENCES ACS
,
    CONSTRAINT ACSExecConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
CREATE  TABLE NotificationChannel (
    NCId NUMBER (10)     NOT NULL ,
    NCName VARCHAR    (80) NOT NULL ,
    SubsystemName VARCHAR    (80) NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    QOS1 VARCHAR    (80) NULL ,
    QOS2 VARCHAR    (80) NULL ,
    QOS3 VARCHAR    (80) NULL ,
	CONSTRAINT NotifiCKey PRIMARY KEY (NCId),
	CONSTRAINT NotifiCAltKey UNIQUE (NCName, ConfigurationId),
    CONSTRAINT NCConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE NotifiC_seq;
-- create trigger
CREATE OR REPLACE TRIGGER NotifiC_SETID_TBI
 BEFORE INSERT ON NotificationChannel
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select NotifiC_seq.nextval INTO :new.NCId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE NCExecution (
    NCId NUMBER (10)     NOT NULL ,
    ACSId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    StartTime NUMBER (19)     NOT NULL ,
    EndTime NUMBER (19)     NULL ,
    NormalStart CHAR (1) NOT NULL     ,
    NormalTermination CHAR (1) NOT NULL     ,
    CONSTRAINT NCExecutionNormalStart CHECK (NormalStart IN ('0', '1')),
    CONSTRAINT NCExecutionNormalT CHECK (NormalTermination IN ('0', '1')),
    CONSTRAINT NCExecutionKey PRIMARY KEY (NCId, ConfigurationId, StartTime),
    CONSTRAINT NCExecNC FOREIGN KEY (NCId) REFERENCES NotificationChannel
,
    CONSTRAINT NCExecConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
,
    CONSTRAINT NCExecACS FOREIGN KEY (ACSId) REFERENCES ACS
);
CREATE  TABLE Startup (
    StartupId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    StartupName VARCHAR    (80) NOT NULL ,
	CONSTRAINT StartupKey PRIMARY KEY (StartupId),
	CONSTRAINT StartupAltKey UNIQUE (StartupName, ConfigurationId),
    CONSTRAINT StartupConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE Startup_seq;
-- create trigger
CREATE OR REPLACE TRIGGER Startup_SETID_TBI
 BEFORE INSERT ON Startup
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select Startup_seq.nextval INTO :new.StartupId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE SystemExecution (
    StartupId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (10)     NOT NULL ,
    StartTime NUMBER (19)     NOT NULL ,
    EndTime NUMBER (19)     NULL ,
    NormalStart CHAR (1) NOT NULL     ,
    NormalTermination CHAR (1) NOT NULL     ,
    CONSTRAINT SystemENormalStart CHECK (NormalStart IN ('0', '1')),
    CONSTRAINT SystemENormalT CHECK (NormalTermination IN ('0', '1')),
    CONSTRAINT SystemEKey PRIMARY KEY (StartupId, ConfigurationId, StartTime),
    CONSTRAINT SystemExecConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
,
    CONSTRAINT SystemExecStartupId FOREIGN KEY (StartupId) REFERENCES Startup
);
CREATE  TABLE DeploymentStartup (
    StartupId NUMBER (10)     NOT NULL ,
    OrderNumber NUMBER (5)     NOT NULL ,
    DependsOn NUMBER (5)     NOT NULL ,
    AllWait CHAR (1) NOT NULL     ,
    StartupType VARCHAR    (10) NOT NULL ,
    NameId NUMBER (5)     NOT NULL ,
    AssociatedType VARCHAR    (10) NULL ,
    AssociatedId NUMBER (5)     NULL ,
    CONSTRAINT DeploySAllWait CHECK (AllWait IN ('0', '1')),
    CONSTRAINT DeploySKey PRIMARY KEY (StartupId, OrderNumber),
    CONSTRAINT DeployStartupId FOREIGN KEY (StartupId) REFERENCES Startup
,
    CONSTRAINT DeployStartupType CHECK (StartupType IN ('ACS', 'computer', 'container', 'component'))
,
    CONSTRAINT DeployAssociatedType CHECK (AssociatedType IN ('computer', 'container'))
);
CREATE  TABLE BaseElementStartup (
    BaseElementId NUMBER (10)     NOT NULL ,
    StartupId NUMBER (10)     NOT NULL ,
    CONSTRAINT BaseElSKey PRIMARY KEY (BaseElementId, StartupId),
    CONSTRAINT BEStartupId FOREIGN KEY (StartupId) REFERENCES Startup
,
    CONSTRAINT BEStartupIdBE FOREIGN KEY (BaseElementId) REFERENCES BaseElement
);
CREATE  TABLE AssociatedBaseElement (
    BaseElementId NUMBER (10)     NOT NULL ,
    StartupId NUMBER (10)     NOT NULL ,
    AssociatedId NUMBER (10)     NOT NULL ,
    AssociationType VARCHAR    (24) NOT NULL ,
    CONSTRAINT AssociBEKey PRIMARY KEY (BaseElementId, StartupId, AssociatedId),
    CONSTRAINT ABEStartupIdBE FOREIGN KEY (BaseElementId) REFERENCES BaseElement
,
    CONSTRAINT ABEStartupId FOREIGN KEY (StartupId) REFERENCES Startup
,
    CONSTRAINT ABEAssociated FOREIGN KEY (AssociatedId) REFERENCES BaseElement
,
    CONSTRAINT ABEAssociationType CHECK (AssociationType IN ('AntennaToPad', 'AntennaToFrontEnd', 'AntennaToCorr'))
);
CREATE  TABLE AssemblyGroupStartup (
    GroupName VARCHAR    (32) NOT NULL ,
    BaseElementId NUMBER (10)     NOT NULL ,
    StartupId NUMBER (10)     NOT NULL ,
    CONSTRAINT AssembGSKey PRIMARY KEY (GroupName, BaseElementId, StartupId),
    CONSTRAINT AGStartupId FOREIGN KEY (StartupId) REFERENCES Startup
,
    CONSTRAINT AGStartupIdBE FOREIGN KEY (BaseElementId) REFERENCES BaseElement
);
CREATE  TABLE AssemblyStartup (
    AssemblyId NUMBER (10)     NOT NULL ,
    BaseElementId NUMBER (10)     NOT NULL ,
    StartupId NUMBER (10)     NOT NULL ,
    ComponentId NUMBER (10)     NOT NULL ,
    GroupName VARCHAR    (32) NULL ,
    OrderTag NUMBER (5)     NULL ,
    RoleName VARCHAR    (32) NULL ,
    ChannelNumber NUMBER (3)     NULL ,
    NodeAddress VARCHAR    (16) NULL ,
    BaseAddress VARCHAR    (16) NULL ,
    CONSTRAINT AssembSKey PRIMARY KEY (AssemblyId, BaseElementId, StartupId),
    CONSTRAINT AssemblyStartupId FOREIGN KEY (StartupId) REFERENCES Startup
,
    CONSTRAINT AssemblyStartupIdA FOREIGN KEY (AssemblyId) REFERENCES Assembly
,
    CONSTRAINT AssemblyStartupIdBE FOREIGN KEY (BaseElementId) REFERENCES BaseElement
,
    CONSTRAINT AssemblyStartupComponent FOREIGN KEY (ComponentId) REFERENCES Component
);
CREATE  TABLE SystemCounters (
    ConfigurationId NUMBER (10)     NOT NULL ,
    UpdateTime NUMBER (19)     NOT NULL ,
    AutoArrayCount NUMBER (5)     NOT NULL ,
    ManArrayCount NUMBER (5)     NOT NULL ,
    DataCaptureCount NUMBER (5)     NOT NULL ,
    CONSTRAINT SystemCKey PRIMARY KEY (ConfigurationId),
    CONSTRAINT SystemCountersConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
CREATE  TABLE Location (
    LocationId NUMBER (10)     NOT NULL ,
    Building VARCHAR    (80) NULL ,
    Floor VARCHAR    (32) NULL ,
    Room VARCHAR    (80) NULL ,
    Mnemonic VARCHAR    (80) NULL ,
    LocationPosition VARCHAR    (80) NULL ,
	CONSTRAINT LocationKey PRIMARY KEY (LocationId)
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE Location_seq;
-- create trigger
CREATE OR REPLACE TRIGGER Location_SETID_TBI
 BEFORE INSERT ON Location
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select Location_seq.nextval INTO :new.LocationId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE Contact (
    ContactId NUMBER (10)     NOT NULL ,
    ContactName VARCHAR    (80) NOT NULL ,
    Email VARCHAR    (80) NULL ,
    Gsm VARCHAR    (80) NULL ,
	CONSTRAINT ContactKey PRIMARY KEY (ContactId),
	CONSTRAINT ContactAltKey UNIQUE (ContactName)
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE Contact_seq;
-- create trigger
CREATE OR REPLACE TRIGGER Contact_SETID_TBI
 BEFORE INSERT ON Contact
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select Contact_seq.nextval INTO :new.ContactId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE Category (
    CategoryName VARCHAR    (32) NOT NULL ,
    Description VARCHAR    (1024) NOT NULL ,
    Path VARCHAR    (80) NOT NULL ,
    IsDefault CHAR (1) NOT NULL     ,
    CONSTRAINT CategoryIsDefault CHECK (IsDefault IN ('0', '1')),
    CONSTRAINT CategoryKey PRIMARY KEY (CategoryName)
);
CREATE  TABLE FaultFamily (
    FaultFamilyId NUMBER (10)     NOT NULL ,
    FamilyName VARCHAR    (80) NOT NULL ,
    CategoryName VARCHAR    (32) NULL ,
    AlarmSource VARCHAR    (80) DEFAULT 'ALARM_SYSTEM_SOURCES',
    HelpURL VARCHAR    (80) NULL ,
    ContactId NUMBER (10)     NOT NULL ,
	CONSTRAINT FaultFamilyKey PRIMARY KEY (FaultFamilyId),
	CONSTRAINT FaultFamilyAltKey UNIQUE (FamilyName),
    CONSTRAINT FaultFamilyContact FOREIGN KEY (ContactId) REFERENCES Contact
,
    CONSTRAINT FaultFamilyCategoryName FOREIGN KEY (CategoryName) REFERENCES Category
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE FaultFamily_seq;
-- create trigger
CREATE OR REPLACE TRIGGER FaultFamily_SETID_TBI
 BEFORE INSERT ON FaultFamily
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select FaultFamily_seq.nextval INTO :new.FaultFamilyId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
CREATE  TABLE FaultMember (
    MemberName VARCHAR    (80) NOT NULL ,
    FaultFamilyId NUMBER (10)     NOT NULL ,
    LocationId NUMBER (10)     NULL ,
    CONSTRAINT FaultMemberKey PRIMARY KEY (MemberName),
    CONSTRAINT MemberFaultFamilyRef FOREIGN KEY (FaultFamilyId) REFERENCES FaultFamily
,
    CONSTRAINT LocationRef FOREIGN KEY (LocationId) REFERENCES Location
);
CREATE  TABLE DefaultMember (
    DefaultMemberId NUMBER (10)     NOT NULL ,
    FaultFamilyId NUMBER (10)     NOT NULL ,
    LocationID NUMBER (10)     NULL ,
    CONSTRAINT DefaulMKey PRIMARY KEY (DefaultMemberId),
    CONSTRAINT DefaultMemberFaultFamilyRef FOREIGN KEY (FaultFamilyId) REFERENCES FaultFamily
,
    CONSTRAINT DefaultMemberLocationRef FOREIGN KEY (LocationId) REFERENCES Location
);
CREATE  TABLE FaultCode (
    FaultCodeId NUMBER (10)     NOT NULL ,
    FaultFamilyId NUMBER (10)     NOT NULL ,
    CodeValue NUMBER (10)     NOT NULL ,
    Priority NUMBER (10)     NOT NULL ,
    Cause VARCHAR    (80) NULL ,
    Action VARCHAR    (1024) NULL ,
    Consequence VARCHAR    (1024) NULL ,
    ProblemDescription VARCHAR    (1024) NOT NULL ,
    IsInstant CHAR (1) NOT NULL     ,
    CONSTRAINT FaultCodeIsInstant CHECK (IsInstant IN ('0', '1')),
	CONSTRAINT FaultCodeKey PRIMARY KEY (FaultCodeId),
	CONSTRAINT FaultCodeAltKey UNIQUE (FaultFamilyId, CodeValue),
    CONSTRAINT CodeFaultFamilyRef FOREIGN KEY (FaultFamilyId) REFERENCES FaultFamily
,
    CONSTRAINT PriorityValue CHECK (Priority IN (0,1,2,3))
);
-- create sequence for auto-generated primary key
CREATE SEQUENCE FaultCode_seq;
-- create trigger
CREATE OR REPLACE TRIGGER FaultCode_SETID_TBI
 BEFORE INSERT ON FaultCode
   FOR EACH ROW
    DECLARE
       cannot_generate_id EXCEPTION;
    BEGIN
      select FaultCode_seq.nextval INTO :new.FaultCodeId from dual;
    EXCEPTION
       WHEN cannot_generate_id  THEN
             raise_application_error(-20000, 'Cannot generate unique ID');
	END;
/  
