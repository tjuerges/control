INSERT INTO Configuration VALUES (
1,
'TMCDB-Common-Queries-Test',
'TMCDB Common Queries Test Database',
0,
''
);

INSERT INTO Computer VALUES (
1,
'abm001',
1,
'abm001',
'true',
'uni',
'Azimuth Platform'
);

INSERT INTO LoggingConfig VALUES (
1,
1,
2,
5,
'Log',
10,
10,
10,
1000
);

INSERT INTO Container VALUES (
1,
'CONTROL/DV01/cppContainer',
'container Path TBD',
1,
1,
1,
'cpp',
'type modifiers TBD',
'true',
'ABM',
'kernel module location TBD',
'kernel module TBD',
0,
'command line args TBD',
-1,
5,
10,
240,
'true',
'autoload shared libraries TBD'
);

INSERT INTO ComponentType VALUES (
1,
'idl TBD 1'
);

INSERT INTO Component VALUES (
1,
'component name TBD',
1,
1,
1,
'cpp',
'true',
'component code TBD',
'component path TBD',
'false',
'false',
'false',
1,
-1,
-1
);

INSERT INTO BaseElement VALUES (
1,
1,
'Antenna',
'DV01',
1
);

INSERT INTO Antenna VALUES (
1,
1,
1,
'DV01',
'VA',
12.00,
1,
1.0,
1.0,
1.0,
1.0,
1.0,
1.0,
1
);

INSERT INTO LRUType VALUES (
'IFProc',
'Intermediate Frequency Down Converter',
1,
0,
'LRU description TBD',
'LRU notes TBD'
);

INSERT INTO AssemblyType VALUES (
'IFProc',
'IFProc',
'Full name TBD',
'node address TBD',
1,
'base address TBD',
'description TBD',
'notes TBD',
1
);

INSERT INTO Assembly VALUES (
1,
'IFProc',
1,
'CONTROL/DV01/IFProc0',
'TBD'
);

INSERT INTO Assembly VALUES (
2,
'IFProc',
1,
'CONTROL/DV01/IFProc1',
'TBD'
);

INSERT INTO Assembly VALUES (
3,
'IFProc',
1,
'CONTROL/DA41/IFProc0',
'TBD'
);

INSERT INTO Assembly VALUES (
4,
'IFProc',
1,
'CONTROL/DA41/IFProc1',
'TBD'
);

COMMIT;
