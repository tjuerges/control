-- TMCDB SQL TABLE DEFINITIONS Version 1.4 2007-MAR-9
--
-- /////////////////////////////////////////////////////////////////
-- // WARNING!  DO NOT MODIFY THIS FILE!                          //
-- //  ---------------------------------------------------------  //
-- // | This is generated code!  Do not modify this file.       | //
-- // | Any changes will be lost when the file is re-generated. | //
-- //  ---------------------------------------------------------  //
-- /////////////////////////////////////////////////////////////////
DROP TABLE SystemCounters;
DROP TABLE AssemblyStartup;
DROP TABLE AssemblyGroupStartup;
DROP TABLE AssociatedBaseElement;
DROP TABLE BaseElementStartup;
DROP TABLE DeploymentStartup;
DROP TABLE Startup;
DROP TABLE SystemExecution;
DROP TABLE NCExecution;
DROP TABLE NotificationChannel;
DROP TABLE ACSExecution;
DROP TABLE ACS;
DROP TABLE MasterComponentExecution;
DROP TABLE ComponentExecution;
DROP TABLE ContainerExecution;
DROP TABLE ComputerExecution;
DROP TABLE MasterComponent;
DROP TABLE BaseElementAssemblyList;
DROP TABLE BaseElementAssemblyGroup;
DROP TABLE AntennaToCorr;
DROP TABLE SBExecution;
DROP TABLE AntennaToArray;
DROP TABLE AntennaToFrontEnd;
DROP TABLE AntennaDelayTerms;
DROP TABLE AntennaPointingModelTerm;
DROP TABLE AntennaPointingModel;
DROP TABLE AntennaToPad;
DROP TABLE Array;
DROP TABLE HolographyTower;
DROP TABLE MasterClock;
DROP TABLE CentralRack;
DROP TABLE WeatherStation;
DROP TABLE FrontEnd;
DROP TABLE CorrQuadrantBin;
DROP TABLE CorrQuadrantRack;
DROP TABLE CorrQuadrant;
DROP TABLE Pad;
DROP TABLE Antenna;
DROP TABLE BaseElementOnline;
DROP TABLE BaseElement;
DROP TABLE BlobProperty;
DROP TABLE EnumProperty;
DROP TABLE IntegerProperty;
DROP TABLE DoubleProperty;
DROP TABLE BooleanProperty;
DROP TABLE StringProperty;
DROP TABLE FloatProperty;
DROP TABLE Assembly;
DROP TABLE EnumWord;
DROP TABLE PropertyType;
DROP TABLE AssemblyType;
DROP TABLE LRUType;
DROP TABLE Component;
DROP TABLE Container;
DROP TABLE Manager;
DROP TABLE NamedLoggerConfig;
DROP TABLE LoggingConfig;
DROP TABLE Computer;
DROP TABLE Configuration;
