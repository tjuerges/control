SET SERVEROUTPUT ON;

DECLARE
    propid NUMBER;

BEGIN

SELECT COUNT(propertytypeid) INTO propid FROM propertytype;

INSERT INTO LRUType VALUES (
    'MountAlcatelPrototype', 
    'Antenna Control Unit', 
    'ALMA-EUR-ICD-9', 
    '4610304000000000000',
    'The purpose of this document is to define the interface between the mount component running in an ABM and the ACU. The ICD provides the interface definitions for all monitor and control points accepted by the ACU as part of the low level functionality which is identified at present for the control of the antenna.', 
    ''
);

INSERT INTO AssemblyType VALUES (
    'MountAlcatelPrototype', 
    'MountAlcatelPrototype', 
    'Antenna Control Unit', 
    '0x0',
    '2',   
    'none',
    'The purpose of this document is to define the interface between the mount component running in an ABM and the ACU. The ICD provides the interface definitions for all monitor and control points accepted by the ACU as part of the low level functionality which is identified at present for the control of the antenna.', 
    '',
    '1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_MOTOR',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0018', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Azimuth Motor Status', 
    'Get azimuth motor status.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_MOTOR_POWER_ON',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0018', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Motor Status (true): motor power on', 
    'byte0, bit 0 azimuth motor power (set=switch on)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_MOTOR_DRIVER_ENABLED',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0018', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Motor Status (true): motor driver enabled', 
    'byte 0, bit 1 azimuth motor driver (set=enabled)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_MOTOR_CURRENTS',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0019', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Azimuth Motor Currents', 
    'Motor currents in all azimuth axis drive motors.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_MOTOR_CURRENTS_MOTOR1',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0019', 
    '0',  
    'int16',
    'short',
    'ampere',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '-255',   
    '255',   
    '2d', 
    'Azimuth Motor Currents: Motor 1', 
    'Motor currents in  azimuth axis drive motor 1.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_MOTOR_CURRENTS_MOTOR2',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0019', 
    '0',  
    'int16',
    'short',
    'ampere',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '-255',   
    '255',   
    '2d', 
    'Azimuth Motor Currents: Motor 2', 
    'Motor currents in  azimuth axis drive motor 2.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_MOTOR_TEMPS',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x001a', 
    '0',  
    'uint16',
    'unsigned short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Azimuth Motor Temperatures', 
    'Motor temperatures in all azimuth axis drive motors.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_MOTOR_TEMPS_MOTOR1',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x001a', 
    '0',  
    'uint16',
    'short',
    'kelvin',
    '1.0',   
    '-323.0',   
    '-323',   
    '-68',   
     '0',     
    '0',   
    '255',   
    '2d', 
    'Azimuth Motor Temperatures: Motor 1', 
    'Motor temperatures in azimuth axis drive motor 1.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_MOTOR_TEMPS_MOTOR2',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x001a', 
    '0',  
    'uint16',
    'short',
    'kelvin',
    '1.0',   
    '-323.0',   
    '-323',   
    '-68',   
     '0',     
    '0',   
    '255',   
    '2d', 
    'Azimuth Motor Temperatures: Motor 2', 
    'Motor temperatures in azimuth axis drive motor 2.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_MOTOR_TORQUE',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0015', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Azimuth Motor Torques', 
    'Motor torques in all azimuth axis drive motors.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_MOTOR_TORQUE_MOTOR1',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0015', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '-32768',   
    '32767',   
    '2d', 
    'Azimuth Motor Torques: Motor 1', 
    'Motor torques in azimuth axis drive motor 1.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_MOTOR_TORQUE_MOTOR2',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0015', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '-32768',   
    '32767',   
    '2d', 
    'Azimuth Motor Torques: Motor 2', 
    'Motor torques in azimuth axis drive motor 2.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Azimuth Axis Status', 
    'Status of azimuth axis. Conditions may be fault conditions or status information. Fault conditions require the use of the CLEAR_FAULT_CMD to clear, while status information will clear then the hardware condition is cleared.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_SW_CW_PRELIMIT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true):  software cw prelimit set', 
    'byte 0, bit 0 software CW prelimit (set=in limit)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_HW_CW_PRELIMIT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): hardware cw prelimit set', 
    'byte 0, bit 1 hadware CW prelimit (set=in limit)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_CW_FINAL_LIMIT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): cw fimal limit set', 
    'byte 0, bit 2 CW final limit (set=in limit)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_CW_SHUTDOWN_DUE_TO_LIMIT_CONDITION',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): cw shutdown due to limit condition occurred', 
    'byte 0, bit 3 CW shutdown due to limit condition (set=ocurred)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_SW_CCW_PRELIMIT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true):  software ccw prelimit set', 
    'byte 0, bit 4 software CCW prelimit (set=in limit)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_HW_CCW_PRELIMIT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): hardware ccw prelimit set', 
    'byte 0, bit 5 hadware CCW prelimit (set=in limit)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_CCW_FINAL_LIMIT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): ccw fimal limit set', 
    'byte 0, bit 6 CCW final limit (set=in limit)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_CCW_SHUTDOWN_DUE_TO_LIMIT_CONDITION',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): ccw shutdown due to limit condition occurred', 
    'byte 0, bit 7 CCW shutdown due to limit condition (set=ocurred)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_MOTORS_OVERSPEED',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): motor overspeed', 
    'byte 1, bit 0 motors overspeed (set=true)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_MOTOR_OVERCURRENT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): motor overcurrent', 
    'byte 1, bit 1 motors overcurrent (set=true)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_MOTOR1_OVERHEATING',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): motor 1 overheating', 
    'byte 1, bit 2 motor 1 overheating (set=true)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_MOTOR2_OVERHEATING',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): motor 2 overheating', 
    'byte 1, bit 3 motor 2 overheating (set=true)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_MOTOR_ENABLE_FAULT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): motor enabled fault', 
    'byte 2, bit 0 motor enable fault (set=fault)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_MOTOR_DRIVER_1_FAULT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): motor driver 1 fault', 
    'byte 2, bit 1 motor driver 1 fault (set=fault)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_MOTOR_DRIVER_2_FAULT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): motor driver 2 fault', 
    'byte 2, bit 2 motor driver 2 fault (set=fault)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_MOTOR_DRIVERS_READY',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): motor drivers ready', 
    'byte 2, bit 3 motor drivers ready (set=ready)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_AXIS_HW_INTERLOCK',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): axis hardware interlock set', 
    'byte 2, bit 4 axis hardware interlock (set=true)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_ENCODER_HALL_INCONSISTENCY',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): encoder/hall inconsistency', 
    'byte 2, bit 5 encoder/hall inconsistency'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_STOW_PIN1_POSITION_ERROR',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): pin1 position error', 
    'byte 3, bit 0 pin 1 position error (set=true)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_MOTOR1_THERMAL_PROTECTION',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): motor 1 thermal protection', 
    'byte 3, bit 1 motor 1 thermal protection (set=ocurred)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_STOW_PIN_TIMEOUT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): stow pin timeout', 
    'byte 3, bit 2 stow pin timeout (set=true)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_STOW_CENTERED',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): stow centered', 
    'byte 3, bit 3 stow centered'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_BRAKE_POSITION_ERROR',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): brake position error', 
    'byte 4, bit 0 brake position error (set=true)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_BRAKE_WEAR',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): brake wear', 
    'byte 4, bit 1 brake wear (set=worn)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_BRAKE_LOCAL_MODE',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): brake local mode', 
    'byte 4, bit 2 brake local mode'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_ENCODER_VALUE_FAULT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): encoder value fault', 
    'byte 5, bit 0 encoder value fault (set=fault)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_ABS_ENCODER_POS_NA',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): absolute encoder position not available', 
    'byte 5, bit 1 absolute encoder position not available (set=true)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_ENCODER_HEAD1_STATUS',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): encoder head 1 fault', 
    'byte 5, bit 2 encoder head1 status (set=fault)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_ENCODER_HEAD2_STATUS',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): encoder head 2 fault', 
    'byte 5, bit 3 encoder head2 status (set=fault)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_ENCODER_HEAD3_STATUS',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): encoder head 3 fault', 
    'byte 5, bit 4 encoder head3 status (set=fault)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_ENCODER_HEAD4_STATUS',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): encoder head 4 fault', 
    'byte 5, bit 5 encoder head4 status (set=fault)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_ENCODER_VALUE_VALIDATION',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): encoder value validation values old', 
    'byte 5, bit 6 encoder validation (unset=values ok, set=values old)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_SERVO_OSCILLATION',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): servo oscillation', 
    'byte 5, bit 7 servo oscillation (set=true)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_MOTOR',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0008', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Elevation Motor Status', 
    'Get elevation motor status.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_MOTOR_POWER_ON',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0008', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Motor Status (true): motor power on', 
    'byte0, bit 0 elevation motor power (set=switch on)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_MOTOR_DRIVER_ENABLED',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0008', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Motor Status (true): motor driver enabled', 
    'byte 0, bit 1 elevation motor driver (set=enabled)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_MOTOR_CURRENTS',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0009', 
    '0',  
    'int16',
    'short',
    'ampere',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '-255',   
    '255',   
    '2d', 
    'Elevation Motor Current', 
    'Motor currents in all elevation axis drive motors.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_MOTOR_TEMPS',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x000a', 
    '0',  
    'uint16',
    'unsigned short',
    'kelvin',
    '1.0',   
    '-323.0',   
    '-68',   
    '0',   
     '5',     
    '0',   
    '255',   
    '2d', 
    'Elevation Motor Temperature', 
    'Motor temeperatures in all elevation axis drive motors.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_MOTOR_TORQUE',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0005', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '-32768',   
    '32767',   
    '2d', 
    'Elevation Motor Torque', 
    'Motor torques in all elevation axis drive motors.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Elevation Axis Status', 
    'Status of elevation axis. Conditions may be fault conditions or status information. Fault conditions require the use of the CLEAR_FAULT_CMD to clear, while status information will clear then the hardware condition is cleared.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_SW_UP_PRELIMIT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true):  software cw prelimit set', 
    'byte 0, bit 0 software up prelimit (set=in limit)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_HW_UP_PRELIMIT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): hardware cw prelimit set', 
    'byte 0, bit 1 hadware up prelimit (set=in limit)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_UP_FINAL_LIMIT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): cw fimal limit set', 
    'byte 0, bit 2 up final limit (set=in limit)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_UP_SHUTDOWN_DUE_TO_LIMIT_CONDITION',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): cw shutdown due to limit condition occurred', 
    'byte 0, bit 3 up shutdown due to limit condition (set=ocurred)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_SW_DOWN_PRELIMIT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true):  software ccw prelimit set', 
    'byte 0, bit 4 software down prelimit (set=in limit)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_HW_DOWN_PRELIMIT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): hardware ccw prelimit set', 
    'byte 0, bit 5 hadware down prelimit (set=in limit)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_DOWN_FINAL_LIMIT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): ccw fimal limit set', 
    'byte 0, bit 6 down final limit (set=in limit)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_DOWN_SHUTDOWN_DUE_TO_LIMIT_CONDITION',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): ccw shutdown due to limit condition occurred', 
    'byte 0, bit 7 down shutdown due to limit condition (set=ocurred)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_MOTORS_OVERSPEED',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): motor overspeed', 
    'byte 1, bit 0 motors overspeed (set=true)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_MOTOR1_OVERCURRENT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): motor overcurrent', 
    'byte 1, bit 1 motors overcurrent (set=true)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_MOTOR1_OVERHEATING',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): motor 1 overheating', 
    'byte 1, bit 2 motor 1 overheating (set=true)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_MOTOR3_OVERHEATING',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): motor 2 overheating', 
    'byte 1, bit 3 motor 3 overheating (set=true)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_MOTOR_ENABLE_FAULT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): motor enabled fault', 
    'byte 2, bit 0 motor enable fault (set=fault)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_MOTOR_DRIVER_1_FAULT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): motor driver 1 fault', 
    'byte 2, bit 1 motor driver 1 fault (set=fault)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_MOTOR_DRIVER_2_FAULT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): motor driver 2 fault', 
    'byte 2, bit 2 motor driver 2 fault (set=fault)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_MOTOR_DRIVERS_READY',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): motor drivers ready', 
    'byte 2, bit 3 motor drivers ready (set=ready)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_AXIS_HW_INTERLOCK',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): axis hardware interlock set', 
    'byte 2, bit 4 axis hardware interlock (set=true)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_ENCODER_HALL_INCONSISTENCY',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): encoder/hall inconsistency', 
    'byte 2, bit 5 encoder/hall inconsistency'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_STOW_PIN1_POSITION_ERROR',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): pin1 position error', 
    'byte 3, bit 0 pin 1 position error (set=true)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_MOTOR1_THERMAL_PROTECTION',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): motor 1 thermal protection', 
    'byte 3, bit 1 motor 1 thermal protection (set=ocurred)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_STOW_PIN2_POSITION_ERROR',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): pin 2 position error', 
    'byte 3, bit 2 pin 2 position error (set=true)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_MOTOR2_THERMAL_PROTECTION',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): motor 2 thermal protection', 
    'byte 3, bit 3 motor 2 thermal protection (set=ocurred)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_STOW_PIN1_TIMEOUT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): stow pin 1 timeout', 
    'byte 3, bit 4 stow pin 1 timeout (set=true)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_STOW_PIN2_TIMEOUT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): stow pin 2 timeout', 
    'byte 3, bit 5 stow pin 2 timeout (set=true)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_15_DEG_STOW_CENTERED',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): 15 deg stow centered', 
    'byte 3, bit 6 15 deg stow centered'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_90_DEG_STOW_CENTERED',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): 90 deg stow centered', 
    'byte 3, bit 7 90 deg stow centered'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_BRAKE_POSITION_ERROR',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): brake position error', 
    'byte 4, bit 0 brake position error (set=true)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_BRAKE_WEAR',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): brake wear', 
    'byte 4, bit 1 brake wear (set=worn)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_BRAKE_LOCAL_MODE',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): brake local mode', 
    'byte 4, bit 2 brake local mode'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_ENCODER_VALUE_FAULT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): encoder value fault', 
    'byte 5, bit 0 encoder value fault (set=fault)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_ABS_ENCODER_POS_NA',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): absolute encoder position not available', 
    'byte 5, bit 1 absolute encoder position not available (set=true)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_ENCODER_HEAD1_STATUS',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): encoder head 1 fault', 
    'byte 5, bit 2 encoder head1 status (set=fault)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_ENCODER_HEAD2_STATUS',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): encoder head 2 fault', 
    'byte 5, bit 3 encoder head2 status (set=fault)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_ENCODER_HEAD3_STATUS',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): encoder head 3 fault', 
    'byte 5, bit 4 encoder head3 status (set=fault)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_ENCODER_HEAD4_STATUS',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): encoder head 4 fault', 
    'byte 5, bit 5 encoder head4 status (set=fault)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_ENCODER_VALUE_VALIDATION',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): encoder value validation values old', 
    'byte 5, bit 6 encoder validation (unset=values ok, set=values old)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_SERVO_OSCILLATION',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): servo oscillation', 
    'byte 5, bit 7 servo oscillation (set=true)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'System Status', 
    'Status of antenna related systems. Conditions may be fault conditions or status information. Fault conditions require the use of the CLEAR_FAULT_CMD to clear, while status information will clear then the hardware condition is cleared.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_EMERGENCY_STOP',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): emergency stop applied', 
    'byte 0, bit 0 emergency stop (set=applied)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_ACCESS_INTERLOCK',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true):access interlock applied', 
    'byte 0, bit 1 access interlock (set=applied)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_HANDLING_INTERLOCK',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): handling interlock applied', 
    'byte 0, bit 2 handling interlock (set=applied)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_SMOKE_ALARM_CONDITION',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): smoke alarm condition set', 
    'byte 0, bit 3 smoke alarm condition (set=applied)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_BASE1_INTERLOCK',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): base 1 interlock applied', 
    'byte 0, bit 4 base 1 interlock (set=applied)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_BASE2_INTERLOCK',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): base 2 interlock applied', 
    'byte 0, bit 5 base 2 interlock (set=applied)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_REC_CABIN_INTERLOCK',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): rec cabin interlock applied', 
    'byte 0, bit 6 rec cabin interlock (set=applied)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_ACU_INTERLOCK',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): ACU interlock applied', 
    'byte 0, bit 7 ACU interlock (set=applied)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_PCU1_INTERLOCK',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): PCU 1 interlock applied', 
    'byte 1, bit 0 PCU 1 interlock (set=applied)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_PCU2_INTERLOCK',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): PCU 2 interlock applied', 
    'byte 1, bit 1 PCU 2 interlock (set=applied)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_PCU3_INTERLOCK',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): PCU 3 interlock applied', 
    'byte 1, bit 2 PCU 3 interlock (set=applied)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_AZ_INTERLOCK',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): azimuth interlock applied', 
    'byte 1, bit 3 azimuth interlock'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_EL_INTERLOCK',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): elevation interlock applied', 
    'byte 1, bit 4 elevation interlock'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_SDU_FAULT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): SDU fault', 
    'byte 1, bit 5 SDU fault'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_ACU_BOOTING_FAILURE',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): ACU booting failure', 
    'byte 2, bit 0 ACU booting failure (set=failed)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_SURVIVAL_STOW_MISSING_CMD_AFTER_IDLE_TIME',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): survival stow due to missing commands after idle time applied', 
    'byte 2, bit 1 survival stow due to missing commands after idle time (set=applied)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_SURVIVAL_STOW_MISSING_TIME_PULSE_AFTER_IDLE_TIME',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): survival stow due to missing timing pulse after idle time applied', 
    'byte 2, bit 2 survival stow due to missing timeing pulse after idle time (set=applied)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_ACU_TASK_FAILURE',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): ACU task failure', 
    'byte 2, bit 3 ACU task failure (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_PCU_PLUG_DISCONNECTED',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): PCU plug disconnected', 
    'byte 2, bit 4 PCU plug disconnected (set=true)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_LOCAL_TERMINAL_CONTROL_ACTIVE',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): local terminal control active', 
    'byte 2, bit 7 local terminal control active (set=active)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_SHUTTER_TIMEOUT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): shutter timeout', 
    'byte 3, bit 0 shutter timeout (set=timeout)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_BRAKES_THERMAL_PROTECTION',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): brakes thernal protection', 
    'byte 3, bit 1 brakes thermal protection'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'STOW_PIN',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0024', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Stow Pin Status', 
    'Position of antenna stow pins.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'STOW_PIN_AZ_PIN1_INSERTED',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0024', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Stow Pin Status (true): azimuth pin 1 inserted', 
    'byte 0, bit 0 azimuth stow pin 1 inserted (set=inserted)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'STOW_PIN_AZ_PIN1_RELEASED',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0024', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Stow Pin Status (true): azimuth pin 1 released', 
    'byte 1, bit 1 azimuth stow pin 1 released (set=released)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'STOW_PIN_EL_PIN1_INSERTED',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0024', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Stow Pin Status (true): elevation pin 1 inserted', 
    'byte 1, bit 0 elevation stow pin 1 inserted (set=inserted)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'STOW_PIN_EL_PIN1_RELEASED',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0024', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Stow Pin Status (true): elevation pin 1 released', 
    'byte 1, bit 1 elevation stow pin 1 inserted (set=released)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'STOW_PIN_EL_PIN2_INSERTED',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0024', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Stow Pin Status (true): elevation pin 2 inserted', 
    'byte 1, bit 2 elevation stow pin 2 inserted (set=inserted)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'STOW_PIN_EL_PIN2_RELEASED',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0024', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Stow Pin Status (true): elevation pin 2 released', 
    'byte 1, bit 3 elevation stow pin 2 inserted (set=released)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0029', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Subreflector Status', 
    'Get subreflector mechanism status'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_LOCAL_CONTROLLER_NOT_READY',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0029', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): local controller not ready', 
    'byte 0, bit 0 local controller not ready (set=not ready)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_LOCAL_CONTROLLER_GENERAL_FAILURE',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0029', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): local controller general failure', 
    'byte 0, bit 1 local controller general failure (set=fault)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_X_AXIS_MOTOR_AMPLIFIER_FAILURE',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0029', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): x axis motor amplifier failure', 
    'byte 1, bit 0 X axis motor amplifier failure (set=fault)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_X_AXIS_FOLLOWING_ERROR',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0029', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): x axis following error', 
    'byte 1, bit 1 X axis following error (set=fault)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_X_AXIS_DESIRED_POSITION_REACHED',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0029', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): x axis desired position reached', 
    'byte 1, bit 2 X axis desired position reached (set=reached)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_Y_AXIS_MOTOR_AMPLIFIER_FAILURE',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0029', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): y axis motor amplifier failure', 
    'byte 2, bit 0 Y axis motor amplifier failure (set=fault)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_Y_AXIS_FOLLOWING_ERROR',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0029', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): y axis following error', 
    'byte 2, bit 1 Y axis following error (set=fault)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_Y_AXIS_DESIRED_POSITION_REACHED',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0029', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): y axis desired position reached', 
    'byte 2, bit 2 Y axis desired position reached (set=reached)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_Z_AXIS_MOTOR_AMPLIFIER_FAILURE',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0029', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): z axis motor amplifier failure', 
    'byte 3, bit 0 Z axis motor amplifier failure (set=fault)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_Z_AXIS_FOLLOWING_ERROR',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0029', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): z axis following error', 
    'byte 3, bit 1 Z axis following error (set=fault)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_Z_AXIS_DESIRED_POSITION_REACHED',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0029', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): z axis desired position reached', 
    'byte 3, bit 2 Z axis desired position reached (set=reached)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_0',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0032', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Status Tilt 0', 
    'Get metrology status tillt 0'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_0_X',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0032', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '-32768',   
    '32767',   
    '2d', 
    'Metrology Status Tilt 0: x tilt', 
    'X  tilt'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_0_Y',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0032', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '-32768',   
    '32767',   
    '2d', 
    'Metrology Status Tilt 0: y tilt', 
    'Y  tilt'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_0_TEMP',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0032', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '-32768',   
    '32767',   
    '2d', 
    'Metrology Status Tilt 0: tiltmeter temperature', 
    'Tiltmeter temperature'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_1',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0033', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Status Tilt 1', 
    'Get metrology status tillt 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_1_X',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0033', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '-32768',   
    '32767',   
    '2d', 
    'Metrology Status Tilt 1: x tilt', 
    'X  tilt'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_1_Y',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0033', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '-32768',   
    '32767',   
    '2d', 
    'Metrology Status Tilt 1: y tilt', 
    'Y  tilt'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_1_TEMP',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0033', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '-32768',   
    '32767',   
    '2d', 
    'Metrology Status Tilt 1: tiltmeter temperature', 
    'Tiltmeter temperature'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_STATUS',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Power Status', 
    'Get power and UPS status'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_STATUS_POWER_MONITOR_ALARM',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Power Status (true): power monitor alarm set', 
    'byte 0, bit 0 power monitoring alarm (set=alarm)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_STATUS_POWER_FROM_TRANSPORTER',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Power Status (true): power from transporter active', 
    'byte 0, bit 1 power from transporter (set=active)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_STATUS_UPS_POWER_SUPPLY_MONITOR',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Power Status (true): ups power supply ok', 
    'byte 0, bit 2 UPS power supply monitor (set=ok)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_STATUS_LOAD_ON_INVERTER',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Power Status (true): load on inverter', 
    'byte 0, bit 3 load on inverter (set=true)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_STATUS_LOAD_ON_MAINS',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Power Status (true): load on mains', 
    'byte 0, bit 4 load on mains (set=true)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_STATUS_UPS_BYPASS_FAILURE',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Power Status (true): ups bypass ok', 
    'byte 0, bit 5 UPS bypass failure (set=false)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_STATUS_LOAD_ON_MAINTENANCE',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Power Status (true): load on maintenance bypass', 
    'byte 0, bit 6 load on maintenance bypass'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_STATUS_UPS_LOW_BATTERY',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Power Status (true): UPS low battery', 
    'byte 0, bit 7 UPS low battery (set=true)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_STATUS_24VDC_AUXILIARY_POWER_STATUS',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Power Status (true): 24VDC auxiliary power status ok', 
    'byte 1, bit 0 24VDC auxiliary power status (set=ok)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_STATUS_24VDC_FAULT_INTERLOCK_POWER_STATUS',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Power Status (true): 24VDC interlock power status ok', 
    'byte 1, bit 1 24VDC fault interlock power status (set=ok)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_STATUS_24VDC_FAULT_AZIMUTH_CIRCUIT_POWER_STATUS',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Power Status (true): 24VDC azimuth circuit power ok', 
    'byte 1, bit 2 24VDC fault azimuth circuit power (set=ok)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_STATUS_24VDC_FAULT_ELEVATION_CIRCUIT_POWER_STATUS',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Power Status (true): 24VDC elevation circuit power ok', 
    'byte 1, bit 3 24VDC fault elevation circuit power (set=ok)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_STATUS_24VDC_FAULT_INTERFACE_CIRCUIT_POWER_STATUS',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Power Status (true): 24VDC interface circuit power ok', 
    'byte 1, bit 4 24VDC fault interface circuit power (set=ok)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AC_STATUS',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x002c', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Air Conditioning Status', 
    'Get air conditioning subsytems status'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AC_STATUS_CHILLER_FUNCTIONING',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x002c', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Air Conditioning Status (true): chiller functioning', 
    'byte 0, bit 0 chiller funtioning (set=on)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AC_STATUS_CHILLER_STATUS',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x002c', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Air Conditioning Status (true): chiller failure', 
    'byte 0, bit 1 chiller status (set=fault)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AC_STATUS_PUMP_STATUS',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x002c', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Air Conditioning Status (true): pump failure', 
    'byte 0, bit 2 pump status (set=fault)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AC_STATUS_ATU_FAN_STATUS',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x002c', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Air Conditioning Status (true): ATU fan failure', 
    'byte 0, bit 3 ATU fan status (set=fault)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SHUTTER',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x002e', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Shutter Status', 
    'Shutter mechanism status.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SHUTTER_OPEN_POSITION',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x002e', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Shutter Status (true): Open', 
    'byte 0, bit 0 shutter open position (set=open)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SHUTTER_CLOSE_POSITION',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x002e', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Shutter Status (true): Closed', 
    'byte 0, bit 1 shutter close position (set=closed)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SHUTTER_MOTOR_ON',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x002e', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Shutter Status (true): Motor shutter on', 
    'byte 0, bit 2 motor shutter on (set=switch on)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SHUTTER_LOCAL_SYSTEM_ERROR',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x002e', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Shutter Status (true): Local system error', 
    'byte 0, bit 3 local system error (set=error)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_ABS_POSN',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0026', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Subreflector Absolute Positions', 
    'Get absolute position of subreflector mechanism'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_ABS_POSN_X_AXIS',
    'MountAlcatelPrototype',
    'float',
    'FloatProperty',     
    '0x0026', 
    '0',  
    'int16',
    'float',
    'meter',
    '1.0E-6',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '-3.2766999999999998E-2',   
    '3.2766999999999998E-2',   
    'f', 
    'Subreflector Absolute Positions: X axis', 
    'X axis absolute position of subreflector mechanism'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_ABS_POSN_Y_AXIS',
    'MountAlcatelPrototype',
    'float',
    'FloatProperty',     
    '0x0026', 
    '0',  
    'int16',
    'float',
    'meter',
    '1.0E-6',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '-3.2766999999999998E-2',   
    '3.2766999999999998E-2',   
    'f', 
    'Subreflector Absolute Positions: Y axis', 
    'Y axis absolute position of subreflector mechanism'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_ABS_POSN_Z_AXIS',
    'MountAlcatelPrototype',
    'float',
    'FloatProperty',     
    '0x0026', 
    '0',  
    'int16',
    'float',
    'meter',
    '1.0E-6',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '-3.2766999999999998E-2',   
    '3.2766999999999998E-2',   
    'f', 
    'Subreflector Absolute Positions: Z axis', 
    'Z axis absolute position of subreflector mechanism'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_DELTA_POSN',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0027', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Subreflector Delta Positions', 
    'Get delta positions of subreflector mechanism'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_DELTA_POSN_X_AXIS',
    'MountAlcatelPrototype',
    'float',
    'FloatProperty',     
    '0x0027', 
    '0',  
    'int16',
    'float',
    'meter',
    '1.0E-6',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '-3.2766999999999998E-2',   
    '3.2766999999999998E-2',   
    'f', 
    'Subreflector Delta Positions: X axis', 
    'X axis delta positions of subreflector mechanism'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_DELTA_POSN_Y_AXIS',
    'MountAlcatelPrototype',
    'float',
    'FloatProperty',     
    '0x0027', 
    '0',  
    'int16',
    'float',
    'meter',
    '1.0E-6',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '-3.2766999999999998E-2',   
    '3.2766999999999998E-2',   
    'f', 
    'Subreflector Delta Positions: Y axis', 
    'Y axis delta positions of subreflector mechanism'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_DELTA_POSN_Z_AXIS',
    'MountAlcatelPrototype',
    'float',
    'FloatProperty',     
    '0x0027', 
    '0',  
    'int16',
    'float',
    'meter',
    '1.0E-6',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '-3.2766999999999998E-2',   
    '3.2766999999999998E-2',   
    'f', 
    'Subreflector Delta Positions: Z axis', 
    'Z axis delta positions of subreflector mechanism'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0028', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Subreflector Limits Status', 
    'Get subreflector mechanism limit status'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_X_AXIS_UPPER_SW_POSITION_LIMIT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0028', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limits Status (true): X axis upper software position exceeded', 
    'byte 0, bit 0 X axis upper software position limit (set=exceeded)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_X_AXIS_LOWER_SW_POSITION_LIMIT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0028', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limits Status (true): X axis lower software position exceeded', 
    'byte 0, bit 1 X axis lower software position limit (set=exceeded)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_X_AXIS_UPPER_LIMIT_SWITCH',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0028', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limits Status (true): X axis upper limit switch reached', 
    'byte 0, bit 2 X axis upper limit switch (set=reached)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_X_AXIS_LOWER_LIMIT_SWITCH',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0028', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limits Status (true): X axis lower limit switch reached', 
    'byte 0, bit 3 X axis lower limit switch (set=reached)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_Y_AXIS_UPPER_SW_POSITION_LIMIT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0028', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limits Status (true): Y axis upper software position exceeded', 
    'byte 0, bit 0 Y axis upper software position limit (set=exceeded)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_Y_AXIS_LOWER_SW_POSITION_LIMIT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0028', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limits Status (true): Y axis lower software position exceeded', 
    'byte 0, bit 1 Y axis lower software position limit (set=exceeded)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_Y_AXIS_UPPER_LIMIT_SWITCH',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0028', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limits Status (true): Y axis upper limit switch reached', 
    'byte 0, bit 2 Y axis upper limit switch (set=reached)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_Y_AXIS_LOWER_LIMIT_SWITCH',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0028', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limits Status (true): Y axis lower limit switch reached', 
    'byte 0, bit 3 Y axis lower limit switch (set=reached)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_Z_AXIS_UPPER_SW_POSITION_LIMIT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0028', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limits Status (true): Z axis upper software position exceeded', 
    'byte 0, bit 0 Z axis upper software position limit (set=exceeded)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_Z_AXIS_LOWER_SW_POSITION_LIMIT',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0028', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limits Status (true): Z axis lower software position exceeded', 
    'byte 0, bit 1 Z axis lower software position limit (set=exceeded)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_Z_AXIS_UPPER_LIMIT_SWITCH',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0028', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limits Status (true): Z axis upper limit switch reached', 
    'byte 0, bit 2 Z axis upper limit switch (set=reached)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_Z_AXIS_LOWER_LIMIT_SWITCH',
    'MountAlcatelPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0028', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limits Status (true): Z axis lower limit switch reached', 
    'byte 0, bit 3 Z axis lower limit switch (set=reached)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_DELTAS',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0034', 
    '1',  
    'int32',
    'int',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '4.8000000000000001E-2',     
    '0',   
    '0',   
    '2d', 
    'Metrology Deltas', 
    'Get azimuth and elevation delta correction applied by the metrology'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_DELTAS_AZ_CORRECTION',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0034', 
    '0',  
    'int32',
    'int',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Metrology Deltas: Azimuth', 
    'Azimuth delta correction'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_DELTAS_EL_CORRECTION',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0034', 
    '0',  
    'int32',
    'int',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Metrology Deltas: Elevation', 
    'Elevation delta correction'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_BRAKE',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0014', 
    '0',  
    'ubyte',
    'unsigned int',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    'none', 
    'Azimuth brake', 
    'Get azimuth brake status.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_BRAKE',
    'MountAlcatelPrototype',
    'integer',
    'IntegerProperty',     
    '0x0004', 
    '0',  
    'ubyte',
    'unsigned int',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    'none', 
    'Elevation brake', 
    'Get elevation brake status.'
);

COMMIT;
END;
/
.
