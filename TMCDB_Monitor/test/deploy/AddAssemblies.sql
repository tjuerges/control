SET SERVEROUTPUT ON;

INSERT INTO Assembly VALUES (
	'1',
	'DGCK',
	'1',
	'CONTROL/ALMA01/DGCK'
);

INSERT INTO Assembly VALUES (
	'2',
	'DGCK',
	'1',
	'CONTROL/ALMA02/DGCK'
);

INSERT INTO Assembly VALUES (
	'3',
	'IFProc',
	'1',
	'CONTROL/ALMA01/IFProc0'
);

INSERT INTO Assembly VALUES (
	'4',
	'IFProc',
	'1',
	'CONTROL/ALMA01/IFProc1'
);
INSERT INTO Assembly VALUES (
	'5',
	'IFProc',
	'1',
	'CONTROL/ALMA02/IFProc0'
);
INSERT INTO Assembly VALUES (
	'6',
	'IFProc',
	'1',
	'CONTROL/ALMA02/IFProc1'
);

INSERT INTO Assembly VALUES (
	'7',
	'FLOOG',
	'1',
	'CONTROL/ALMA01/FLOOG'
);

INSERT INTO Assembly VALUES (
	'8',
	'FLOOG',
	'1',
	'CONTROL/ALMA02/FLOOG'
);

INSERT INTO Assembly VALUES (
	'9',
	'LORR',
	'1',
	'CONTROL/ALMA01/LORR'
);

INSERT INTO Assembly VALUES (
	'10',
	'LORR',
	'1',
	'CONTROL/ALMA02/LORR'
);

INSERT INTO Assembly VALUES (
	'11',
	'PSA',
	'1',
	'CONTROL/ALMA01/PSA'
);

INSERT INTO Assembly VALUES (
	'12',
	'PSA',
	'1',
	'CONTROL/ALMA02/PSA'
);

INSERT INTO Assembly VALUES (
	'13',
	'LO2',
	'1',
	'CONTROL/ALMA01/LO2BBpr0'
);

INSERT INTO Assembly VALUES (
	'14',
	'LO2',
	'1',
	'CONTROL/ALMA02/LO2BBpr0'
);

INSERT INTO Assembly VALUES (
	'15',
	'LO2',
	'1',
	'CONTROL/ALMA01/LO2BBpr1'
);

INSERT INTO Assembly VALUES (
	'16',
	'LO2',
	'1',
	'CONTROL/ALMA02/LO2BBpr1'
);
INSERT INTO Assembly VALUES (
	'17',
	'LO2',
	'1',
	'CONTROL/ALMA01/LO2BBpr2'
);

INSERT INTO Assembly VALUES (
	'18',
	'LO2',
	'1',
	'CONTROL/ALMA02/LO2BBpr2'
);
INSERT INTO Assembly VALUES (
	'19',
	'LO2',
	'1',
	'CONTROL/ALMA01/LO2BBpr3'
);

INSERT INTO Assembly VALUES (
	'20',
	'LO2',
	'1',
	'CONTROL/ALMA02/LO2BBpr3'
);

INSERT INTO Assembly VALUES (
	'21',
	'MountVertexPrototype',
	'1',
	'CONTROL/ALMA01/Mount'
);

INSERT INTO Assembly VALUES (
	'22',
	'MountAlcatelPrototype',
	'1',
	'CONTROL/ALMA02/Mount'
);

COMMIT;

/
.

