SET SERVEROUTPUT ON;

DECLARE
    propid NUMBER;

BEGIN

SELECT COUNT(propertytypeid) INTO propid FROM propertytype;

INSERT INTO LRUType VALUES (
    'LO2', 
    'Second Local Oscillator', 
    'ALMA-55.05.00.00-70.35.30.00-C-ICD', 
    '4654972800000000000',
    'The purpose of this document is to define the Monitor and Control interface between the Second Local Oscillator (LO2) and the Computing Monitor and Control software. A Fine Tuning Synthesizer is an integral part of the LO2. There is a separate ICD for Back-End/FTS to ICD Interface between Back End/Fine Tuning Synthesizer and Computing/Control Software. In the LO2, the FTS is a slave device. As such, the FTS relative CAN addresses (RCA) must be offset by 0x18000 (Hex)', 
    ''
);

INSERT INTO AssemblyType VALUES (
    'LO2', 
    'LO2', 
    'Second Local Oscillator', 
    'parm',
    '0',   
    'none',
    'The purpose of this document is to define the Monitor and Control interface between the Second Local Oscillator (LO2) and the Computing Monitor and Control software. A Fine Tuning Synthesizer is an integral part of the LO2. There is a separate ICD for Back-End/FTS to ICD Interface between Back End/Fine Tuning Synthesizer and Computing/Control Software. In the LO2, the FTS is a slave device. As such, the FTS relative CAN addresses (RCA) must be offset by 0x18000 (Hex)', 
    '',
    '1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MODULE_STATUS',
    'LO2',
    'integer',
    'IntegerProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Module Status', 
    'Logical status of the switches and loop lock status in the LO2.The PLL Lock bit (bit 2) provides a realtime indication of whether or not the synthesizer is locked. Bit 3 is a latched version of the bit 2 PLL Lock bit. It will latch high when the second LO goes out of lock for any detectable amount of time, i.e., 5 microseconds or longer. It can be unlatched using the RESET_LOCK_STATUS command (assuming the second LO is again locked as indicated by bit 2). Bit 3 will always be high and bit 2 low when the FM coil voltage is not between +/-7.5V.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'TUNE_LOCK_SWITCH',
    'LO2',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Module Status', 
    'bit 0: Tune/Lock Switch Position (0->Lock, 1->Tune)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'HIGH_LOW_SWITCH',
    'LO2',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Module Status', 
    'bit 1: High/Low Switch Position (0->Low, 1->High)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'PLL_LOCK_IND',
    'LO2',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Module Status', 
    'bit 2: PLL Lock Indicator (0->Not Locked, 1-> Locked)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'PLL_OUT_OF_LOCK',
    'LO2',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Module Status', 
    'bit 3: PLL Out of Lock indicator (1-> Has gone out of Lock)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_SUPPLY_1_VALUE',
    'LO2',
    'double',
    'DoubleProperty',     
    '0x00003', 
    '0',  
    'uint16',
    'double',
    'volt',
    '0.004885',   
    '0.0',   
    '4.9',   
    '5.1',   
     '10',     
    '0',   
    '10',   
    '2d', 
    'Plus 5 supply voltage after regulation', 
    'Actual voltage level of the +5V supply voltage after regulation. This is the voltage supplied to the FTS.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_SUPPLY_2_VALUE',
    'LO2',
    'double',
    'DoubleProperty',     
    '0x00004', 
    '0',  
    'uint16',
    'double',
    'volt',
    '0.00977',   
    '0.0',   
    '14.4',   
    '15.8',   
     '10',     
    '0',   
    '20',   
    '2d', 
    'Plus 15 supply voltage after regulation', 
    'Actual voltage level of the +15V supply voltage after regulation. This is the voltage supplied to the phase lock loop (PLL) PCB.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_SUPPLY_3_VALUE',
    'LO2',
    'double',
    'DoubleProperty',     
    '0x00005', 
    '0',  
    'uint16',
    'double',
    'volt',
    '-0.009766',   
    '0.0',   
    '-15.7',   
    '-14.3',   
     '10',     
    '-20',   
    '0',   
    '2d', 
    'Negative 20 supply voltage after regulation', 
    'Actual voltage level of the -15V supply voltage after regulation. This is the voltage supplied to the PLL PCB and the DYTO (heater only).'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_SUPPLY_4_VALUE',
    'LO2',
    'double',
    'DoubleProperty',     
    '0x00006', 
    '0',  
    'uint16',
    'double',
    'volt',
    '0.00977',   
    '0.0',   
    '14.4',   
    '15.8',   
     '10',     
    '0',   
    '20',   
    '2d', 
    'Plus 15 supply voltage after regulation', 
    'Actual voltage level of the +15V supply voltage after regulation. This voltage is supplied to the +15V input to the DYTO and the signal amplifier.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'DETECTED_RF_POWER',
    'LO2',
    'double',
    'DoubleProperty',     
    '0x00007', 
    '0',  
    'uint16',
    'double',
    'watt',
    '9.766E-6',   
    '0.0',   
    '0.5',   
    '1.5',   
     '10',     
    '0',   
    '20',   
    '2d', 
    'Detected RF Power', 
    'The RF Power of the DYTO is detected in the loop of the PLL. The detector provides a negative voltage'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'DETECTED_IF_POWER',
    'LO2',
    'double',
    'DoubleProperty',     
    '0x00008', 
    '0',  
    'uint16',
    'double',
    'watt',
    '1.0',   
    '0.0',   
    '-18',   
    '-10',   
     '10',     
    '0',   
    '10',   
    '2d', 
    'Detected IF Power', 
    'The IF Power in the loop of the PLL is detected. The detector provides a positive voltage.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'DETECTED_FTS_POWER',
    'LO2',
    'double',
    'DoubleProperty',     
    '0x00009', 
    '0',  
    'uint16',
    'double',
    'watt',
    '1.0',   
    '0.0',   
    '-18',   
    '-10',   
     '10',     
    '0',   
    '10',   
    '2d', 
    'Detected FTS Power', 
    'The FTS Power in the loop of the PLL is detected. The detector provides a positive voltage.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'FM_COIL_VOLTAGE',
    'LO2',
    'double',
    'DoubleProperty',     
    '0x0000A', 
    '0',  
    'int16',
    'double',
    'volt',
    '0.004885',   
    '0.0',   
    '-7.5',   
    '7.5',   
     '10',     
    '-10',   
    '10',   
    '2d', 
    'FM Coil Voltage', 
    'The voltage across the FM coil of the DYTO is measured directly. The Second LO Synthesizer works best when the FM Voltage is nominally 0V. The FM Voltage is driven to zero by changing the coarse tune setting of the DYTO. The phase lock loop can only lock properly when the FM voltage is between +/-7.5V.'
);

COMMIT;
END;
/
.
