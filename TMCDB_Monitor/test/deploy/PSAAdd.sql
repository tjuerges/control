SET SERVEROUTPUT ON;

DECLARE
    propid NUMBER;

BEGIN

SELECT COUNT(propertytypeid) INTO propid FROM propertytype;

INSERT INTO LRUType VALUES (
    'PSA', 
    'Power Supply Analog', 
    'ALMA-57.03.00.00-70.35.30.00-A-ICD', 
    '4688496000000000000',
    'Power supply for analog racks.  The PSAnalog AC-DC Power Supply has three 17V modules and two 7V modules to produce the +17A, +17B, -17, +7, and -7 voltages.', 
    ''
);

INSERT INTO AssemblyType VALUES (
    'PSA', 
    'PSA', 
    'Power Supply Analog', 
    '0x60',
    '0',   
    'none',
    'Power supply for analog racks.  The PSAnalog AC-DC Power Supply has three 17V modules and two 7V modules to produce the +17A, +17B, -17, +7, and -7 voltages.', 
    '',
    '1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'ERROR_AND_ALARMS',
    'PSA',
    'integer',
    'IntegerProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '0',   
    'none', 
    'Error and Alarms', 
    'Error Flags. The bits in this monitor point summarize the overall condition of the power supply.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUPPLY_ERROR',
    'PSA',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '0',   
    'none', 
    'Error from Supply', 
    'Byte 1, Bit 0 indicates that the outputs from all supply modules have been disabled due to an internal condition trip (over/under voltage, over/under current). The outputs can only be re-enabled by writing a 0 to SHUTDOWN_COM.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'OUTPUT_SHUTDOWN',
    'PSA',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '0',   
    'none', 
    'All Outputs Shutdown', 
    'Byte 1, Bit 1 indicates if the power supply outputs have been disabled. The bit will be 1 if the supply has shutdown due to either a command shutdown or an internal condition trip. The modules have been shutdown by command if and only if this bit is 1 and bit 0 is 0. The modules have been shutdown by an internal error if and only if this bit is 1 and bit 0 is 1.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'ERROR_MINUS_17V',
    'PSA',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '0',   
    'none', 
    'Error on the -17v module', 
    'Error on the -17v module'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'ERROR_MINUS_7V',
    'PSA',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '0',   
    'none', 
    'Error on the -7v module', 
    'Error on the -7v module'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'ERROR_7V',
    'PSA',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '0',   
    'none', 
    'Error on the +7v module', 
    'Error on the +7v module'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'ERROR_17V_B',
    'PSA',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '0',   
    'none', 
    'Error on the +17vB module', 
    'Error on the +17vB module'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'ERROR_17V_A',
    'PSA',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '0',   
    'none', 
    'Error on the +17vA module', 
    'Error on the +17vA module'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MODULE_17V_B_ON',
    'PSA',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '0',   
    'none', 
    '+17vB module is on', 
    'Module +17vB  is on'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MODULE_MINUS_17V_ON',
    'PSA',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '0',   
    'none', 
    '-17v module is on', 
    'Module  -7v is on'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MODULE_17V_A_ON',
    'PSA',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '0',   
    'none', 
    '+17vA module is on', 
    'Module  +17vA is on'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MODULE_7V_ON',
    'PSA',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '0',   
    'none', 
    '+7v module is on', 
    'Module  +7v is on'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MODULE_MINUS_7V_ON',
    'PSA',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '0',   
    'none', 
    '-7v module is on', 
    'Module  -17v is on'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'OVER_TEMP_OK',
    'PSA',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '0',   
    'none', 
    'Over Temperature Status is OK', 
    'Over Temperature Status is OK'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'FAN_OK',
    'PSA',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '0',   
    'none', 
    'Fan Status is OK', 
    'Fan Status is OK'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AC_OK',
    'PSA',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '0',   
    'none', 
    'AC Status is OK', 
    'AC Status is OK'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'DC_OK',
    'PSA',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '0',   
    'none', 
    'Global DC Status is OK', 
    'Global DC Status is OK'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'CURRENT_LIMIT_OK',
    'PSA',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '0',   
    'none', 
    'Current Limit Status is OK', 
    'Current Limit Status is OK'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'OVER_VOLTAGE_OK',
    'PSA',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '0',   
    'none', 
    'Over Voltage Protection Status is OK', 
    'Over volt age Protection Status is OK'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'FAN_SPEED_OK',
    'PSA',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '0',   
    'none', 
    'Fan Speed Status is OK', 
    'Fan Speed Status is OK'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'VOLTAGE_7V',
    'PSA',
    'float',
    'FloatProperty',     
    '0x00005', 
    '0',  
    'uint16',
    'float',
    'volt',
    '0.00977517106549',   
    '0.0',   
    '6.8',   
    '7.2',   
     '60',     
    '6.8',   
    '7.2',   
    '8.3f', 
    'Measured voltage on the +7v AC-DC converter.', 
    'Measured volt age on the +7v AC-DC converter.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'CURRENT_7V',
    'PSA',
    'float',
    'FloatProperty',     
    '0x00006', 
    '0',  
    'uint16',
    'float',
    'ampere',
    '0.035606195478',   
    '0.0',   
    '0',   
    '12',   
     '60',     
    '0',   
    '12',   
    '8.3f', 
    'Measured current on the +7v AC-DC converter.', 
    'Measured current on the +7v AC-DC converter.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'VOLTAGE_MINUS_7V',
    'PSA',
    'float',
    'FloatProperty',     
    '0x00007', 
    '0',  
    'uint16',
    'float',
    'volt',
    '-0.00977517106549',   
    '0.0',   
    '-7.2',   
    '-6.8',   
     '60',     
    '-7.2',   
    '-6.8',   
    '8.3f', 
    'Measured voltage on the -7v AC-DC converter.', 
    'Measured volt age on the -7v AC-DC converter.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'CURRENT_MINUS_7V',
    'PSA',
    'float',
    'FloatProperty',     
    '0x00008', 
    '0',  
    'uint16',
    'float',
    'ampere',
    '0.035606195478',   
    '0.0',   
    '0',   
    '4',   
     '60',     
    '0',   
    '4',   
    '8.3f', 
    'Measured current on the -7v AC-DC converter.', 
    'Measured current on the -7v AC-DC converter.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'VOLTAGE_17V_A',
    'PSA',
    'float',
    'FloatProperty',     
    '0x00009', 
    '0',  
    'uint16',
    'float',
    'volt',
    '0.0229716070936',   
    '0.0',   
    '16',   
    '18',   
     '60',     
    '16',   
    '18',   
    '8.3f', 
    'Measured voltage on the +17vA AC-DC converter.', 
    'Measured volt age on the +17vA AC-DC converter.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'CURRENT_17V_A',
    'PSA',
    'float',
    'FloatProperty',     
    '0x0000a', 
    '0',  
    'uint16',
    'float',
    'ampere',
    '0.0206232341356',   
    '0.0',   
    '0',   
    '16',   
     '60',     
    '0',   
    '16',   
    '8.3f', 
    'Measured current on the +17vA AC-DC converter.', 
    'Measured current on the +17vA AC-DC converter.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'VOLTAGE_MINUS_17V',
    'PSA',
    'float',
    'FloatProperty',     
    '0x0000b', 
    '0',  
    'uint16',
    'float',
    'volt',
    '0.0229716070936',   
    '0.0',   
    '-18',   
    '-16',   
     '60',     
    '-18',   
    '-16',   
    '8.3f', 
    'Measured voltage on the -17v AC-DC converter.', 
    'Measured volt age on the -17v AC-DC converter.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'CURRENT_MINUS_17V',
    'PSA',
    'float',
    'FloatProperty',     
    '0x0000c', 
    '0',  
    'uint16',
    'float',
    'ampere',
    '0.0206232341356',   
    '0.0',   
    '0',   
    '8',   
     '60',     
    '0',   
    '8',   
    '8.3f', 
    'Measured current on the -17v AC-DC converter.', 
    'Measured current on the -17v AC-DC converter.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'VOLTAGE_17V_B',
    'PSA',
    'float',
    'FloatProperty',     
    '0x0000d', 
    '0',  
    'uint16',
    'float',
    'volt',
    '0.0229716070936',   
    '0.0',   
    '16',   
    '18',   
     '60',     
    '16',   
    '18',   
    '8.3f', 
    'Measured voltage on the +17vB AC-DC converter.', 
    'Measured volt age on the +17vB AC-DC converter.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'CURRENT_17V_B',
    'PSA',
    'float',
    'FloatProperty',     
    '0x0000e', 
    '0',  
    'uint16',
    'float',
    'ampere',
    '0.0206232341356',   
    '0.0',   
    '0',   
    '16',   
     '60',     
    '0',   
    '16',   
    '8.3f', 
    'Measured current on the +17vB AC-DC converter.', 
    'Measured current on the +17vB AC-DC converter.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MAX_MIN_7V',
    'PSA',
    'integer',
    'IntegerProperty',     
    '0x00020', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '60',     
    '0',   
    '0',   
    'none', 
    'none', 
    'Max and Min volt age and Max current on the +7V AC-DC converter.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MAX_7V',
    'PSA',
    'float',
    'FloatProperty',     
    '0x00020', 
    '0',  
    'int16',
    'float',
    'volt',
    '0.00977517106549',   
    '0.0',   
    '6.8',   
    '7.2',   
     '60',     
    '0',   
    '7.2',   
    '8.3f', 
    'Max +7V voltage', 
    'Max +7V volt age'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MIN_7V',
    'PSA',
    'float',
    'FloatProperty',     
    '0x00020', 
    '0',  
    'int16',
    'float',
    'volt',
    '0.00977517106549',   
    '0.0',   
    '6.8',   
    '7.2',   
     '60',     
    '0',   
    '7.2',   
    '8.3f', 
    'Min +7V voltage', 
    'Min +7V volt age'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MAX_7_CURRENT',
    'PSA',
    'float',
    'FloatProperty',     
    '0x00020', 
    '0',  
    'int16',
    'float',
    'ampere',
    '0.035606195478',   
    '0.0',   
    '0',   
    '12',   
     '60',     
    '0',   
    '12',   
    '8.3f', 
    'Max +7V current', 
    'Max +7V current'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MAX_MIN_MINUS_7V',
    'PSA',
    'integer',
    'IntegerProperty',     
    '0x00021', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '60',     
    '0',   
    '0',   
    'none', 
    'none', 
    'Max and Min volt age and Max current on the -7V AC-DC converter.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MAX_MINUS_7V',
    'PSA',
    'float',
    'FloatProperty',     
    '0x00021', 
    '0',  
    'int16',
    'float',
    'volt',
    '0.00977517106549',   
    '0.0',   
    '-7.2',   
    '-6.8',   
     '60',     
    '-7.2',   
    '0',   
    '8.3f', 
    'Max -7 vvoltage', 
    'Max -7 vvolt age'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MIN_MINUS_7V',
    'PSA',
    'float',
    'FloatProperty',     
    '0x00021', 
    '0',  
    'int16',
    'float',
    'volt',
    '0.00977517106549',   
    '0.0',   
    '-7.2',   
    '-6.8',   
     '60',     
    '-7.2',   
    '0',   
    '8.3f', 
    'Min -7 voltage', 
    'Min -7 volt age'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MAX_MINUS_7_CURRENT',
    'PSA',
    'float',
    'FloatProperty',     
    '0x00021', 
    '0',  
    'int16',
    'float',
    'ampere',
    '0.035606195478',   
    '0.0',   
    '0',   
    '4',   
     '60',     
    '0',   
    '4',   
    '8.3f', 
    'Max -7 current', 
    'Max -7 current'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MAX_MIN_17V_A',
    'PSA',
    'integer',
    'IntegerProperty',     
    '0x00022', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '60',     
    '0',   
    '0',   
    'none', 
    'none', 
    'Max and Min volt age and Max current on the +17V_A AC-DC converter.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MAX_17V',
    'PSA',
    'float',
    'FloatProperty',     
    '0x00022', 
    '0',  
    'int16',
    'float',
    'volt',
    '0.0229716070936',   
    '0.0',   
    '16',   
    '18',   
     '60',     
    '0',   
    '18',   
    '8.3f', 
    'Max 17V voltage', 
    'Max 17V volt age'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MIN_17V',
    'PSA',
    'float',
    'FloatProperty',     
    '0x00022', 
    '0',  
    'int16',
    'float',
    'volt',
    '0.0229716070936',   
    '0.0',   
    '16',   
    '18',   
     '60',     
    '0',   
    '18',   
    '8.3f', 
    'Min 17V vvoltage', 
    'Min 17V vvolt age'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MAX_17_CURRENT',
    'PSA',
    'float',
    'FloatProperty',     
    '0x00022', 
    '0',  
    'int16',
    'float',
    'ampere',
    '0.0206232341356',   
    '0.0',   
    '0',   
    '16',   
     '60',     
    '0',   
    '16',   
    '8.3f', 
    'Max 17V vcurrent', 
    'Max 17V vcurrent'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MAX_MIN_MINUS_17V',
    'PSA',
    'integer',
    'IntegerProperty',     
    '0x00023', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '60',     
    '0',   
    '0',   
    'none', 
    'none', 
    'Max and Min volt age and Max current on the -17V AC-DC converter.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MAX_MINUS_17V',
    'PSA',
    'float',
    'FloatProperty',     
    '0x00023', 
    '0',  
    'int16',
    'float',
    'volt',
    '0.0229716070936',   
    '0.0',   
    '-18',   
    '-16',   
     '60',     
    '-18',   
    '0',   
    '8.3f', 
    'Max -17V vvoltage', 
    'Max -17V vvolt age'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MIN_MINUS_17V',
    'PSA',
    'float',
    'FloatProperty',     
    '0x00023', 
    '0',  
    'int16',
    'float',
    'volt',
    '0.0229716070936',   
    '0.0',   
    '-18',   
    '-16',   
     '60',     
    '-18',   
    '0',   
    '8.3f', 
    'Min -17V voltage', 
    'Min -17V volt age'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MAX_MINUS_17_CURRENT',
    'PSA',
    'float',
    'FloatProperty',     
    '0x00023', 
    '0',  
    'int16',
    'float',
    'ampere',
    '0.0206232341356',   
    '0.0',   
    '0',   
    '8',   
     '60',     
    '0',   
    '16',   
    '8.3f', 
    'Max-17V  current', 
    'Max-17V  current'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MAX_MIN_17V_B',
    'PSA',
    'integer',
    'IntegerProperty',     
    '0x00024', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '60',     
    '0',   
    '0',   
    'none', 
    'none', 
    'Max and Min volt age and Max current on the +17V_B AC-DC converter.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MAX_17V_B',
    'PSA',
    'float',
    'FloatProperty',     
    '0x00024', 
    '0',  
    'int16',
    'float',
    'volt',
    '0.0229716070936',   
    '0.0',   
    '16',   
    '18',   
     '60',     
    '0',   
    '18',   
    '8.3f', 
    'Max 17V_B voltage', 
    'Max 17V_B volt age'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MIN_17V_B',
    'PSA',
    'float',
    'FloatProperty',     
    '0x00024', 
    '0',  
    'int16',
    'float',
    'volt',
    '0.0229716070936',   
    '0.0',   
    '16',   
    '18',   
     '60',     
    '0',   
    '18',   
    '8.3f', 
    'Min 17V_B voltage', 
    'Min 17V_B volt age'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MAX_17V_B_CURRENT',
    'PSA',
    'float',
    'FloatProperty',     
    '0x00024', 
    '0',  
    'int16',
    'float',
    'ampere',
    '0.0206232341356',   
    '0.0',   
    '0',   
    '16',   
     '60',     
    '0',   
    '16',   
    '8.3f', 
    'Max 17V_B current', 
    'Max 17V_B current'
);

COMMIT;
END;
/
.
