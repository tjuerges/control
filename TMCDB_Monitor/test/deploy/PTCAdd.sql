SET SERVEROUTPUT ON;

DECLARE
    propid NUMBER;

BEGIN

SELECT COUNT(propertytypeid) INTO propid FROM propertytype;

INSERT INTO LRUType VALUES (
    'PTC', 
    'PTC Unit', 
    'ALMA-US-ICD-9', 
    '4571683200000000000',
    'The purpose of this document is to define the interface between the mount component running in an ABM and the PTC. The ICD provides the interface definitions for all monitor and control points accepted by the ACU as part of the low level functionality which is identified at present for the control of the antenna.', 
    ''
);

INSERT INTO AssemblyType VALUES (
    'PTC', 
    'PTC', 
    'PTC Unit', 
    '0x1',
    '2',   
    '0x00000',
    'The purpose of this document is to define the interface between the mount component running in an ABM and the PTC. The ICD provides the interface definitions for all monitor and control points accepted by the ACU as part of the low level functionality which is identified at present for the control of the antenna.', 
    '',
    '1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AC_STATUS',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0002', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Air Conditioning System Status', 
    'Get air conditioning subsytems status'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AC_STATUS_SYSTEM_1_ON',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0002', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Air Conditioning Status (true): System 1 ON', 
    'byte 0, bit 0 Air conditioning system 1 on/off'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AC_STATUS_SYSTEM_2_ON',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0002', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Air Conditioning Status (true): System 2 ON', 
    'byte 0, bit 1 Air conditioning system 1 on/off'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AC_STATUS_SYSTEM_AUTOMATIC',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0002', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Air Conditioning Status (true): System automatic', 
    'byte 0, bit 2 Air conditioning system automatic'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AC_STATUS_LOCAL',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0002', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Air Conditioning Status (true): System in Local', 
    'byte 0, bit 3 Air conditioning system in local'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AC_STATUS_SYSTEM_1_ALARM',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0002', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Air Conditioning Status (true): System 1 Alarm', 
    'byte 0, bit 4 Air conditioning system 1 alarm'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AC_STATUS_SYSTEM_2_ALARM',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0002', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Air Conditioning Status (true): System 2 Alarm', 
    'byte 0, bit 5 Air conditioning system 1 alarm'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_DISPL_0',
    'PTC',
    'float',
    'FloatProperty',     
    '0x6000', 
    '0',  
    'int16',
    'float',
    'meter',
    '1.0E-7',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    'f', 
    'Metrology Displacement Sensor 0 Readout', 
    'Metrology system displacement sensor readouts. There will be 4-6 displacements sensors. This assumes that sensors measuring displacements will be included in the design.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_DISPL_1',
    'PTC',
    'float',
    'FloatProperty',     
    '0x6001', 
    '0',  
    'int16',
    'float',
    'meter',
    '1.0E-7',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    'f', 
    'Metrology Displacement Sensor 1 Readout', 
    'Metrology system displacement sensor readouts. There will be 4-6 displacements sensors. This assumes that sensors measuring displacements will be included in the design.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_EQUIP_STATUS',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0010', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology System Status', 
    'Metrology system equipment status'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_EQUIP_STATUS_LOCAL_CAN_BUS_ERROR',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0010', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Metrology System Fault (true): local CAN bus error', 
    'byte 0, bit 0 local CAN bus error'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_EQUIP_STATUS_TILTMETER_1_READOUT_ERROR',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0010', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Metrology System Fault (true): Tiltmeter 1 readout error', 
    'byte 0, bit 1 tiltmeter 1 readout error'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_EQUIP_STATUS_TILTMETER_2_READOUT_ERROR',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0010', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Metrology System Fault (true): Tiltmeter 2 readout error', 
    'byte 0, bit 2 tiltmeter 2 readout error'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_EQUIP_STATUS_TILTMETER_3_READOUT_ERROR',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0010', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Metrology System Fault (true): Tiltmeter 3 readout error', 
    'byte 0, bit 3 tiltmeter 3 readout error'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_EQUIP_STATUS_TILTMETER_4_READOUT_ERROR',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0010', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Metrology System Fault (true): Tiltmeter 4 readout error', 
    'byte 0, bit 4 tiltmeter 4 readout error'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_EQUIP_STATUS_TILTMETER_5_READOUT_ERROR',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0010', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Metrology System Fault (true): Tiltmeter 5 readout error', 
    'byte 0, bit 5 tiltmeter 5 readout error'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_EQUIP_STATUS_TEMPERATURE_SENSOR_READOUT_ERROR',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0010', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Metrology System Fault (true): Temperature Sensor Readout Error', 
    'byte 0, bit 6 temperature sensor readout error'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_MODE',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0011', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '600',     
    '0',   
    '0',   
    '2d', 
    'Metrology Mode', 
    'Get metrology mode. The bits corresponding to specific devices assume that these devices will be included in the design.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_MODE_STANDARD_POINTING_ERROR_MODEL_ENABLED',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0011', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Metrology Mode Status (true): Standard Pointing Error Model Enabled', 
    'byte 0, bit 0 Standard pointing error model enabled'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_MODE_TILTMETER_COMP_ENABLED',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0011', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Metrology Mode Status (true): Tiltmeter Compensation Enabled', 
    'byte 0, bit 1 Tiltmeter compensation enabled'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_MODE_TEMP_COMP_ENABLED',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0011', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Metrology Mode Status (true): Temperature Compensation Enabled', 
    'byte 0, bit 2 Temperature compensation enabled'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_MODE_LASER_BASED_COMP_ENABLED',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0011', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Metrology Mode Status (true): Laser Based Compensation Enabled', 
    'byte 0, bit 3 Laser based compensation enabled'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_MODE_FRICTION_COMP_ALGORITHM_ENABLED',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0011', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Metrology Mode Status (true): Friction Compensation Algorithm Enabled', 
    'byte 0, bit 4 Friction compensatiopn algorithm enabled'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_MODE_AUTOMATIC_SUBREF_POSN_CORRECTION_ENABLED',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0011', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Metrology Mode Status (true): Automatic Subreflector Position Correction Enabled', 
    'byte 0, bit 5 Automatic sub-reflector position correction enabled'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_MODE_MOUNT_ENC_CORRECTION_BASED_ENABLED',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0011', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Metrology Mode Status (true): Correction Based on Encoder Mount Sensors Enabled', 
    'byte 0, bit 6 Correction based on encoder mount sensors enabled'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_00',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x4000', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Temperatures Sensor Pack 00', 
    'Get value of metrology temperature sensor pack 00'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_00',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4000', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 00', 
    'Metrology temperature sensor 00'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_01',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4000', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 01', 
    'Metrology temperature sensor 01'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_02',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4000', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 02', 
    'Metrology temperature sensor 02'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_03',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4000', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 03', 
    'Metrology temperature sensor 03'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_01',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x4001', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Temperatures Sensor Pack 01', 
    'Get value of metrology temperature sensor pack 01'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_04',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4001', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 04', 
    'Metrology temperature sensor 04'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_05',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4001', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 05', 
    'Metrology temperature sensor 05'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_06',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4001', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 06', 
    'Metrology temperature sensor 06'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_07',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4001', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 07', 
    'Metrology temperature sensor 07'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_02',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x4002', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Temperatures Sensor Pack 02', 
    'Get value of metrology temperature sensor pack 02'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_08',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4002', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 08', 
    'Metrology temperature sensor 08'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_09',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4002', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 09', 
    'Metrology temperature sensor 09'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_10',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4002', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 10', 
    'Metrology temperature sensor 10'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_11',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4002', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 11', 
    'Metrology temperature sensor 11'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_03',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x4003', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Temperatures Sensor Pack 03', 
    'Get value of metrology temperature sensor pack 03'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_12',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4003', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 12', 
    'Metrology temperature sensor 12'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_13',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4003', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 13', 
    'Metrology temperature sensor 13'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_14',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4003', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 14', 
    'Metrology temperature sensor 14'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_15',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4003', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 15', 
    'Metrology temperature sensor 15'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_04',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x4004', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Temperatures Sensor Pack 04', 
    'Get value of metrology temperature sensor pack 04'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_16',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4004', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 16', 
    'Metrology temperature sensor 16'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_17',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4004', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 17', 
    'Metrology temperature sensor 17'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_18',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4004', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 18', 
    'Metrology temperature sensor 18'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_19',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4004', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 19', 
    'Metrology temperature sensor 19'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_05',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x4005', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Temperatures Sensor Pack 05', 
    'Get value of metrology temperature sensor pack 05'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_20',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4005', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 20', 
    'Metrology temperature sensor 20'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_21',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4005', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 21', 
    'Metrology temperature sensor 21'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_22',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4005', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 22', 
    'Metrology temperature sensor 22'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_23',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4005', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 23', 
    'Metrology temperature sensor 23'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_06',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x4006', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Temperatures Sensor Pack 06', 
    'Get value of metrology temperature sensor pack 06'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_24',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4006', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 24', 
    'Metrology temperature sensor 24'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_25',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4006', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 25', 
    'Metrology temperature sensor 25'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_26',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4006', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 26', 
    'Metrology temperature sensor 26'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_27',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4006', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 27', 
    'Metrology temperature sensor 27'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_07',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x4007', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Temperatures Sensor Pack 07', 
    'Get value of metrology temperature sensor pack 07'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_28',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4007', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 28', 
    'Metrology temperature sensor 28'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_29',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4007', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 29', 
    'Metrology temperature sensor 29'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_30',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4007', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 30', 
    'Metrology temperature sensor 30'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_31',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4007', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 31', 
    'Metrology temperature sensor 31'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_08',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x4008', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Temperatures Sensor Pack 08', 
    'Get value of metrology temperature sensor pack 08'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_32',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4008', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 32', 
    'Metrology temperature sensor 32'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_33',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4008', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 33', 
    'Metrology temperature sensor 33'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_34',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4008', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 34', 
    'Metrology temperature sensor 34'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_35',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4008', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 35', 
    'Metrology temperature sensor 35'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_09',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x4009', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Temperatures Sensor Pack 09', 
    'Get value of metrology temperature sensor pack 09'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_36',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4009', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 36', 
    'Metrology temperature sensor 36'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_37',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4009', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 37', 
    'Metrology temperature sensor 37'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_38',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4009', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 38', 
    'Metrology temperature sensor 38'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_39',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4009', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 39', 
    'Metrology temperature sensor 39'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_0A',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x400a', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Temperatures Sensor Pack 0A', 
    'Get value of metrology temperature sensor pack 0A'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_40',
    'PTC',
    'float',
    'FloatProperty',     
    '0x400a', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 40', 
    'Metrology temperature sensor 40'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_41',
    'PTC',
    'float',
    'FloatProperty',     
    '0x400a', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 41', 
    'Metrology temperature sensor 41'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_42',
    'PTC',
    'float',
    'FloatProperty',     
    '0x400a', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 42', 
    'Metrology temperature sensor 42'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_43',
    'PTC',
    'float',
    'FloatProperty',     
    '0x400a', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 43', 
    'Metrology temperature sensor 43'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_0B',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x400b', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Temperatures Sensor Pack 0B', 
    'Get value of metrology temperature sensor pack 0B'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_44',
    'PTC',
    'float',
    'FloatProperty',     
    '0x400b', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 44', 
    'Metrology temperature sensor 44'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_45',
    'PTC',
    'float',
    'FloatProperty',     
    '0x400b', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 45', 
    'Metrology temperature sensor 45'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_46',
    'PTC',
    'float',
    'FloatProperty',     
    '0x400b', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 46', 
    'Metrology temperature sensor 46'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_47',
    'PTC',
    'float',
    'FloatProperty',     
    '0x400b', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 47', 
    'Metrology temperature sensor 47'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_0C',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x400c', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Temperatures Sensor Pack 0C', 
    'Get value of metrology temperature sensor pack 0C'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_48',
    'PTC',
    'float',
    'FloatProperty',     
    '0x400c', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 48', 
    'Metrology temperature sensor 48'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_49',
    'PTC',
    'float',
    'FloatProperty',     
    '0x400c', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 49', 
    'Metrology temperature sensor 49'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_50',
    'PTC',
    'float',
    'FloatProperty',     
    '0x400c', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 50', 
    'Metrology temperature sensor 50'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_51',
    'PTC',
    'float',
    'FloatProperty',     
    '0x400c', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 51', 
    'Metrology temperature sensor 51'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_0D',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x400d', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Temperatures Sensor Pack 0D', 
    'Get value of metrology temperature sensor pack 0D'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_52',
    'PTC',
    'float',
    'FloatProperty',     
    '0x400d', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 52', 
    'Metrology temperature sensor 52'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_53',
    'PTC',
    'float',
    'FloatProperty',     
    '0x400d', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 53', 
    'Metrology temperature sensor 53'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_54',
    'PTC',
    'float',
    'FloatProperty',     
    '0x400d', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 54', 
    'Metrology temperature sensor 54'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_55',
    'PTC',
    'float',
    'FloatProperty',     
    '0x400d', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 55', 
    'Metrology temperature sensor 55'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_0E',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x400e', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Temperatures Sensor Pack 0E', 
    'Get value of metrology temperature sensor pack 0E'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_56',
    'PTC',
    'float',
    'FloatProperty',     
    '0x400e', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 56', 
    'Metrology temperature sensor 56'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_57',
    'PTC',
    'float',
    'FloatProperty',     
    '0x400e', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 57', 
    'Metrology temperature sensor 57'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_58',
    'PTC',
    'float',
    'FloatProperty',     
    '0x400e', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 58', 
    'Metrology temperature sensor 58'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_59',
    'PTC',
    'float',
    'FloatProperty',     
    '0x400e', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 59', 
    'Metrology temperature sensor 59'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_0F',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x400f', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Temperatures Sensor Pack 0F', 
    'Get value of metrology temperature sensor pack 0F'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_60',
    'PTC',
    'float',
    'FloatProperty',     
    '0x400f', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 60', 
    'Metrology temperature sensor 60'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_61',
    'PTC',
    'float',
    'FloatProperty',     
    '0x400f', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 61', 
    'Metrology temperature sensor 61'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_62',
    'PTC',
    'float',
    'FloatProperty',     
    '0x400f', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 62', 
    'Metrology temperature sensor 62'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_63',
    'PTC',
    'float',
    'FloatProperty',     
    '0x400f', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 63', 
    'Metrology temperature sensor 63'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_10',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x4010', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Temperatures Sensor Pack 10', 
    'Get value of metrology temperature sensor pack 10'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_64',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4010', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 64', 
    'Metrology temperature sensor 64'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_65',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4010', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 65', 
    'Metrology temperature sensor 65'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_66',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4010', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 66', 
    'Metrology temperature sensor 66'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_67',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4010', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 67', 
    'Metrology temperature sensor 67'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_11',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x4011', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Temperatures Sensor Pack 11', 
    'Get value of metrology temperature sensor pack 11'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_68',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4011', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 68', 
    'Metrology temperature sensor 68'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_69',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4011', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 69', 
    'Metrology temperature sensor 69'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_70',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4011', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 70', 
    'Metrology temperature sensor 70'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_71',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4011', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 71', 
    'Metrology temperature sensor 71'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_12',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x4012', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Temperatures Sensor Pack 12', 
    'Get value of metrology temperature sensor pack 12'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_72',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4012', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 72', 
    'Metrology temperature sensor 72'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_73',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4012', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 73', 
    'Metrology temperature sensor 73'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_74',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4012', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 74', 
    'Metrology temperature sensor 74'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_75',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4012', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 75', 
    'Metrology temperature sensor 75'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_13',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x4013', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Temperatures Sensor Pack 13', 
    'Get value of metrology temperature sensor pack 13'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_76',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4013', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 76', 
    'Metrology temperature sensor 76'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_77',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4013', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 77', 
    'Metrology temperature sensor 77'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_78',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4013', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 78', 
    'Metrology temperature sensor 78'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TEMPS_SENSOR_79',
    'PTC',
    'float',
    'FloatProperty',     
    '0x4013', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Metrology Temperature Sensor 79', 
    'Metrology temperature sensor 79'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_0',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x5000', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Tiltmeter 0 Readout', 
    'Get metrology system tiltmeter 0 readout'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_0_X',
    'PTC',
    'float',
    'FloatProperty',     
    '0x5000', 
    '0',  
    'int16',
    'float',
    'none',
    '0.01',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Tiltmeter 0: tilt x', 
    'tiltmeter 0, tilt x'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_0_Y',
    'PTC',
    'float',
    'FloatProperty',     
    '0x5000', 
    '0',  
    'int16',
    'float',
    'none',
    '0.01',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Tiltmeter 0: tilt y', 
    'tiltmeter 0, tilt y'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_0_TEMPERATURE',
    'PTC',
    'float',
    'FloatProperty',     
    '0x5000', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Tiltmeter 0: temperature', 
    'tiltmeter 0, tilt temperature'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_1',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x5001', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Tiltmeter 1 Readout', 
    'Get metrology system tiltmeter 1 readout'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_1_X',
    'PTC',
    'float',
    'FloatProperty',     
    '0x5001', 
    '0',  
    'int16',
    'float',
    'none',
    '0.01',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Tiltmeter 1: tilt x', 
    'tiltmeter 1, tilt x'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_1_Y',
    'PTC',
    'float',
    'FloatProperty',     
    '0x5001', 
    '0',  
    'int16',
    'float',
    'none',
    '0.01',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Tiltmeter 1: tilt y', 
    'tiltmeter 1, tilt y'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_1_TEMPERATURE',
    'PTC',
    'float',
    'FloatProperty',     
    '0x5001', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Tiltmeter 1: temperature', 
    'tiltmeter 1, tilt temperature'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_2',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x5002', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Tiltmeter 2 Readout', 
    'Get metrology system tiltmeter 2 readout'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_2_X',
    'PTC',
    'float',
    'FloatProperty',     
    '0x5002', 
    '0',  
    'int16',
    'float',
    'none',
    '0.01',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Tiltmeter 2: tilt x', 
    'tiltmeter 2, tilt x'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_2_Y',
    'PTC',
    'float',
    'FloatProperty',     
    '0x5002', 
    '0',  
    'int16',
    'float',
    'none',
    '0.01',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Tiltmeter 2: tilt y', 
    'tiltmeter 2, tilt y'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_2_TEMPERATURE',
    'PTC',
    'float',
    'FloatProperty',     
    '0x5002', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Tiltmeter 2: temperature', 
    'tiltmeter 2, tilt temperature'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_3',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x5003', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Tiltmeter 3 Readout', 
    'Get metrology system tiltmeter 3 readout'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_3_X',
    'PTC',
    'float',
    'FloatProperty',     
    '0x5003', 
    '0',  
    'int16',
    'float',
    'none',
    '0.01',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Tiltmeter 3: tilt x', 
    'tiltmeter 3, tilt x'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_3_Y',
    'PTC',
    'float',
    'FloatProperty',     
    '0x5003', 
    '0',  
    'int16',
    'float',
    'none',
    '0.01',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Tiltmeter 3: tilt y', 
    'tiltmeter 3, tilt y'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_3_TEMPERATURE',
    'PTC',
    'float',
    'FloatProperty',     
    '0x5003', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Tiltmeter 3: temperature', 
    'tiltmeter 3, tilt temperature'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_4',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x5004', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Metrology Tiltmeter 4 Readout', 
    'Get metrology system tiltmeter 4 readout'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_4_X',
    'PTC',
    'float',
    'FloatProperty',     
    '0x5004', 
    '0',  
    'int16',
    'float',
    'none',
    '0.01',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Tiltmeter 4: tilt x', 
    'tiltmeter 4, tilt x'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_4_Y',
    'PTC',
    'float',
    'FloatProperty',     
    '0x5004', 
    '0',  
    'int16',
    'float',
    'none',
    '0.01',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Tiltmeter 4: tilt y', 
    'tiltmeter 4, tilt y'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'METR_TILT_4_TEMPERATURE',
    'PTC',
    'float',
    'FloatProperty',     
    '0x5004', 
    '0',  
    'int16',
    'float',
    'kelvin',
    '0.1',   
    '-273.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Tiltmeter 4: temperature', 
    'tiltmeter 4, tilt temperature'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'OTHER_STATUS',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x003a', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Receiver Cabin and Auxiliary Status', 
    'Receiver cabin and auxiliary status information'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'OTHER_STATUS_DOOR_OPEN_RECEIVER_CABIN',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x003a', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Receiver Cabin Status (true): Door open', 
    'byte 0, bit 0 Door open receiver cabin'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'OTHER_STATUS_SMOKE_ALARM_RECEIVER_CABIN',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x003a', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Receiver Cabin Fault (true): Smoke Alarm Receiver Cabin', 
    'byte 0, bit 1 Smoke alarm receiver cabin'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'OTHER_STATUS_SMOKE_ALARM_ANTENNA_TOWER',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x003a', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Receiver Cabin Fault (true): Smoke Alarm Antenna Tower', 
    'byte 0, bit 2 Smoke alarm antenna tower'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'OTHER_STATUS_SMOKE_ALARM_UPS_CABINET',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x003a', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Receiver Cabin Fault (true): Smoke Alarm UPS Cabinet', 
    'byte 0, bit 3 Smoke alarm UPS cabinet'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'OTHER_STATUS_OVERTEMP_RECEIVER_CABIN',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x003a', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Receiver Cabin Status (true): Over-temperature receiver cabin', 
    'byte 0, bit 4 Over-temperature receiver cabin'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'OTHER_STATUS_OVERTEMP_SERVO_SWITCH_CABINET',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x003a', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Receiver Cabin Status (true): Over-temperature Servo Switch Cabinet', 
    'byte 0, bit 5 Over-temperature servo switch cabinet'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'OTHER_STATUS_OVERTEMP_UPS_CABINET',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x003a', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Receiver Cabin Status (true): Over-temperature UPS Cabinet', 
    'byte 0, bit 6 Over-temperature UPS cabinet'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_STATUS',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0003', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Power Distribution Status', 
    'Power Distribution Status and Alarms'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_STATUS_MAIN_SWITCH_IN_POS_TRANSP_PS',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0003', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Power Distribution Status (true): Main Switch in Pos. Transportable PS', 
    'byte 0, bit 0 Main switch in position. Transportable power supply'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_STATUS_MAIN_SWITCH_IN_POS_ANTENNA_BASE_PS',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0003', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Power Distribution Status (true): Main Switch in Pos. Antenna Base PS', 
    'byte 0, bit 1 Main switch in position. Antenna base power supply'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_STATUS_MAIN_CIRCUIT_BREAKER',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0003', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Power Distribution Status (true): Main Cicuit Breaker ON', 
    'byte 0, bit 2 Main circuit breaker: ON/OFF'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_STATUS_REM_CONTROLLED_RELAY_OPEN',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0003', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Power Distribution Fault (true): Remotely Controller Relay Open', 
    'byte 0, bit 3 Remotely controller relay open'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_STATUS_LIGHTNING_ARRESTOR_TRIPPED',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0003', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Power Distribution Status (true): Lightning Arrestor Tripped', 
    'byte 0, bit 5 Lightning arrestor tripped'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_STATUS_SINGLE_PHASE_INTERLOCK',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0003', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Power Distribution Fault (true): Single Phase Protection Released', 
    'byte 1, bit 0 Single phase interlock'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_STATUS_REVERSE_PHASE_PROTECTION_RELEASED',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0003', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Power Distribution Fault (true): Reverse Phase Protection Released', 
    'byte 1, bit 1 Reverse phase protection released'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_STATUS_CB_CRITICAL_ELECTRONIC_BUS_ON',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0003', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Power Distribution Status (true): Circuit Breaker Critical Electronic Bus ON', 
    'byte 1, bit 2 Circuit breaker critical electronic bus ON'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_STATUS_CB_CRITICAL_CRYO_BUS_ON',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0003', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Power Distribution Status (true): Circuit Breaker Critical', 
    'byte 1, bit 3 Circuit breaker critical cryogenic bus ON'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_STATUS_CB_NON_CRITICAL_BUS_ON',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0003', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Power Distribution Status (true): Circuit Breaker Non-critical', 
    'byte 1, bit 4 Circuit breaker non-critical bus ON'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_ABS_POSN',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0020', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Subreflector Absolute Positions', 
    'Get absolute position of subreflector mechanism'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_ABS_POSN_X',
    'PTC',
    'float',
    'FloatProperty',     
    '0x0020', 
    '0',  
    'int16',
    'float',
    'meter',
    '1.0E-6',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Subreflector Absolute X Position', 
    'Subreflector X absolute position'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_ABS_POSN_Y',
    'PTC',
    'float',
    'FloatProperty',     
    '0x0020', 
    '0',  
    'int16',
    'float',
    'meter',
    '1.0E-6',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Subreflector Absolute Y Position', 
    'Subreflector Y absolute position'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_ABS_POSN_Z',
    'PTC',
    'float',
    'FloatProperty',     
    '0x0020', 
    '0',  
    'int16',
    'float',
    'meter',
    '1.0E-6',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Subreflector Absolute Z Position', 
    'Subreflector Z absolute position'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_DELTA_POSN',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0021', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Subreflector Delta Positions', 
    'Get delta positions of subreflector mechanism'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_DELTA_POSN_X',
    'PTC',
    'float',
    'FloatProperty',     
    '0x0021', 
    '0',  
    'int16',
    'float',
    'meter',
    '1.0E-6',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Subreflector X Delta Position', 
    'Subreflector X delta position'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_DELTA_POSN_Y',
    'PTC',
    'float',
    'FloatProperty',     
    '0x0021', 
    '0',  
    'int16',
    'float',
    'meter',
    '1.0E-6',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Subreflector Y Delta Position', 
    'Subreflector Y delta position'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_DELTA_POSN_Z',
    'PTC',
    'float',
    'FloatProperty',     
    '0x0021', 
    '0',  
    'int16',
    'float',
    'meter',
    '1.0E-6',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Subreflector Z Delta Position', 
    'Subreflector Z delta position'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Subreflector Limit Status', 
    'Get subreflector mechanism limit status'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_ACTUATOR_1_NEGATIVE_LIMIT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Actuator 1 Negative Limit', 
    'byte 0, bit 0 Actuator 1 negative limit'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_ACTUATOR_2_NEGATIVE_LIMIT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Actuator 2 Negative Limit', 
    'byte 0, bit 1 Actuator 2 negative limit'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_ACTUATOR_3_NEGATIVE_LIMIT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Actuator 3 Negative Limit', 
    'byte 0, bit 2 Actuator 3 negative limit'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_ACTUATOR_4_NEGATIVE_LIMIT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Actuator 4 Negative Limit', 
    'byte 0, bit 3 Actuator 4 negative limit'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_ACTUATOR_5_NEGATIVE_LIMIT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Actuator 5 Negative Limit', 
    'byte 0, bit 4 Actuator 5 negative limit'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_ACTUATOR_6_NEG_LIMIT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Actuator 6 Negative Limit', 
    'byte 0, bit 5 Actuator 6 negative limit'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_ACTUATOR_1_POSITIVE_LIMIT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Actuator 1 Positive Limit', 
    'byte 1, bit 0 Actuator 1 positive limit'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_ACTUATOR_2_POSITIVE_LIMIT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Actuator 2 Positive Limit', 
    'byte 1, bit 1 Actuator 2 positive limit'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_ACTUATOR_3_POSITIVE_LIMIT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Actuator 3 Positive Limit', 
    'byte 1, bit 2 Actuator 3 positive limit'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_ACTUATOR_4_POSITIVE_LIMIT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Actuator 4 Positive Limit', 
    'byte 1, bit 3 Actuator 3 positive limit'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_ACTUATOR_5_POSITIVE_LIMIT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Actuator 5 Positive Limit', 
    'byte 1, bit 4 Actuator 4 positive limit'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_ACTUATOR_6_POSITIVE_LIMIT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Actuator 6 Positive Limit', 
    'byte 1, bit 5 Actuator 5 positive limit'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_HEXAPOD_NEGATIVE_X_LIMIT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Hexapod Negative X Limit', 
    'byte 2, bit 0 hexapod negative x limit'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_HEXAPOD_POSITIVE_X_LIMIT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Hexapod Positive X Limit', 
    'byte 2, bit 1 hexapod positive x limit'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_HEXAPOD_NEGATIVE_Y_LIMIT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Hexapod Negative Y Limit', 
    'byte 2, bit 2 hexapod negative y limit'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_HEXAPOD_POSITIVE_Y_LIMIT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Hexapod Positive Y Limit', 
    'byte 2, bit 3 hexapod positive y limit'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_HEXAPOD_NEGATIVE_Z_LIMIT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Hexapod Negative Z Limit', 
    'byte 2, bit 4 hexapod negative z limit'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_HEXAPOD_POSITIVE_Z_LIMIT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Hexapod Positive Z Limit', 
    'byte 2, bit 5 hexapod positive z limit'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_HEXAPOD_NEGATIVE_THETA_X_LIMIT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Hexapod Negative Theta X Limit', 
    'byte 3, bit 0 hexapod negative thetax limit'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_HEXAPOD_POSITIVE_THETA_X_LIMIT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Hexapod Positive Theta X Limit', 
    'byte 3, bit 1 hexapod positive thetax limit'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_HEXAPOD_NEGATIVE_THETA_Y_LIMIT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Hexapod NegativeTheta Y Limit', 
    'byte 3, bit 2 hexapod negative thetay limit'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_HEXAPOD_POSITIVE_THETA_Y_LIMIT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Hexapod PositiveTheta Y Limit', 
    'byte 3, bit 3 hexapod positive thetay limit'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_HEXAPOD_NEGATIVE_THETA_Z_LIMIT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Hexapod Negative Theta Z Limit', 
    'byte 3, bit 4 hexapod negative thetaz limit'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_HEXAPOD_POSITIVE_THETA_Z_LIMIT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Hexapod Positive Theta Z Limit', 
    'byte 3, bit 5 hexapod positive thetaz limit'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_HEXAPOD_COLLISSION_SWITCH_1',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Hexapod Collision Switch 1 encoutered', 
    'byte 4, bit 0 collision switch 1 encountered'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_HEXAPOD_COLLISSION_SWITCH_2',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Hexapod Collision Switch 2 encoutered', 
    'byte 4, bit 1 collision switch 2 encountered'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_HEXAPOD_COLLISSION_SWITCH_3',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Hexapod Collision Switch 3 encoutered', 
    'byte 4, bit 2 collision switch 3 encountered'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_HEXAPOD_COLLISSION_SWITCH_4',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Hexapod Collision Switch 4 encoutered', 
    'byte 4, bit 3 collision switch 4 encountered'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_HEXAPOD_COLLISSION_SWITCH_5',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Hexapod Collision Switch 5 encoutered', 
    'byte 4, bit 4 collision switch 5 encountered'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_HEXAPOD_COLLISSION_SWITCH_6',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Hexapod Collision Switch 6 encoutered', 
    'byte 4, bit 5 collision switch 6 encountered'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_HEXAPOD_COLLISSION_SWITCH_7',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Hexapod Collision Switch 7 encoutered', 
    'byte 4, bit 6 collision switch 7 encountered'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_LIMITS_HEXAPOD_COLLISSION_SWITCH_8',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0022', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Limit Status (true): Hexapod Collision Switch 8 encoutered', 
    'byte 4, bit 7 collision switch 8 encountered'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_ROTATION',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0024', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Subreflector Rotation Position', 
    'Subreflector rotation position.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_ROTATION_X_TIP',
    'PTC',
    'float',
    'FloatProperty',     
    '0x0024', 
    '0',  
    'int16',
    'float',
    'none',
    '0.0010',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Subreflector Rotation X Tip', 
    'Subreflector rotation x tip'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_ROTATION_Y_TILT',
    'PTC',
    'float',
    'FloatProperty',     
    '0x0024', 
    '0',  
    'int16',
    'float',
    'none',
    '0.0010',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Subreflector Rotation Y Tilt', 
    'Subreflector rotation y tilt'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_ROTATION_Z_ROTATION',
    'PTC',
    'float',
    'FloatProperty',     
    '0x0024', 
    '0',  
    'int16',
    'float',
    'none',
    '0.0010',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    'f', 
    'Subreflector  Z Rotation', 
    'Subreflector rotation z rotation'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Subreflector Status', 
    'Get subreflector mechanism limit status'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_AMPLIFIER_ACTUATOR_1_FAULT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): Amplifier Actuator 1 fault', 
    'byte 0, bit 0 amplifier actuator 1 faulty'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_AMPLIFIER_ACTUATOR_2_FAULT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): Amplifier Actuator 2 fault', 
    'byte 0, bit 1 amplifier actuator 2 faulty'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_AMPLIFIER_ACTUATOR_3_FAULT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): Amplifier Actuator 3 fault', 
    'byte 0, bit 2 amplifier actuator 3 faulty'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_AMPLIFIER_ACTUATOR_4_FAULT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): Amplifier Actuator 4 fault', 
    'byte 0, bit 3 amplifier actuator 4 faulty'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_AMPLIFIER_ACTUATOR_5_FAULT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): Amplifier Actuator 5 fault', 
    'byte 0, bit 4 amplifier actuator 5 faulty'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_AMPLIFIER_ACTUATOR_6_FAULT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): Amplifier Actuator 6 fault', 
    'byte 0, bit 5 amplifier actuator 6 faulty'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_AMPLIFIER_ACTUATOR_1_NOT_RESPONDING',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): Amplifier  Actuator 1 Not Responding', 
    'byte 1, bit 0 amplifier actuator 1 not responding'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_AMPLIFIER_ACTUATOR_2_NOT_RESPONDING',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): Amplifier  Actuator 2 Not Responding', 
    'byte 1, bit 1 amplifier actuator 2 not responding'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_AMPLIFIER_ACTUATOR_3_NOT_RESPONDING',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): Amplifier  Actuator 3 Not Responding', 
    'byte 1, bit 2 amplifier actuator 3 not responding'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_AMPLIFIER_ACTUATOR_4_NOT_RESPONDING',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): Amplifier  Actuator 4 Not Responding', 
    'byte 1, bit 3 amplifier actuator 4 not responding'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_AMPLIFIER_ACTUATOR_5_NOT_RESPONDING',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): Amplifier  Actuator 5 Not Responding', 
    'byte 1, bit 4 amplifier actuator 5 not responding'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_AMPLIFIER_ACTUATOR_6_NOT_RESPONDING',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): Amplifier  Actuator 6 Not Responding', 
    'byte 1, bit 5 amplifier actuator 6 not responding'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_ACTUATOR_1_NOT_MOVING',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): Actuator 1 does not move', 
    'byte 2, bit 0 actuator 1 does not move'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_ACTUATOR_2_NOT_MOVING',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): Actuator 2 does not move', 
    'byte 2, bit 1 actuator 2 does not move'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_ACTUATOR_3_NOT_MOVING',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): Actuator 3 does not move', 
    'byte 2, bit 2 actuator 3 does not move'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_ACTUATOR_4_NOT_MOVING',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): Actuator 4 does not move', 
    'byte 2, bit 3 actuator 4 does not move'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_ACTUATOR_5_NOT_MOVING',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): Actuator 5 does not move', 
    'byte 2, bit 4 actuator 5 does not move'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_ACTUATOR_6_NOT_MOVING',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): Actuator 6 does not move', 
    'byte 2, bit 5 actuator 6 does not move'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_ACTUATOR_1_NOT_INITIALIZED',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): Actuator 1 Not Initialized', 
    'byte 3, bit 0 actuator 1 not initialized'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_ACTUATOR_2_NOT_INITIALIZED',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): Actuator 2 Not Initialized', 
    'byte 3, bit 1 actuator 2 not initialized'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_ACTUATOR_3_NOT_INITIALIZED',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): Actuator 3 Not Initialized', 
    'byte 3, bit 2 actuator 3 not initialized'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_ACTUATOR_4_NOT_INITIALIZED',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): Actuator 4 Not Initialized', 
    'byte 3, bit 3 actuator 4 not initialized'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_ACTUATOR_5_NOT_INITIALIZED',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): Actuator 5 Not Initialized', 
    'byte 3, bit 4 actuator 5 not initialized'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_ACTUATOR_6_NOT_INITIALIZED',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): Actuator 6 Not Initialized', 
    'byte 3, bit 5 actuator 6 not initialized'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_DRIVE_PS_FAILURE',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): Drive Poswer Supply Failure', 
    'byte 4, bit 0 drive power supply failure'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SUBREF_STATUS_LOCAL_CAN_BUS_FAULT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Subreflector Status (true): local CAN bus fault. Status N/A', 
    'byte 4, bit 1 local CAN bus fault, status not available'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_1',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'UPS System 1 Alarms Status', 
    'Alarms Status UPS System 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_1_MAINS_FAILURE',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 1 Alarms Status (true): Mains failure', 
    'byte 0, bit 0 Mains failure'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_1_RECTIFIER_FAILURE',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 1 Alarms Status (true): Rectifier failure', 
    'byte 0, bit 1 Rectifier failure'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_1_RECTIFIER_FUSE_BLOWN',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 1 Alarms Status (true): Rectifier fuse blown', 
    'byte 0, bit 2 Rectifier fuse blown'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_1_THERMAL_IMAGE',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 1 Alarms Status (true): Thermal image', 
    'byte 0, bit 3 Thermal image'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_1_RECTIFIER_OUT_VOLTAGE_OUT_OF_TOLERANCE',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 1 Alarms Status (true): Rectifier output voltage out of tolerance', 
    'byte 0, bit 4 Rectifier output voltage out of tolerance'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_1_UPS_PJASE_SEQ_INCORRECT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 1 Alarms Status (true): UPS phase sequence not correct', 
    'byte 0, bit 5 UPS phase sequence not correct'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_1_BATTERY_SWITCH_OPEN',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 1 Alarms Status (true): Battery switch open', 
    'byte 0, bit 6 Battery switch open'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_1_BATTERY_ALREADY_DISCHARGED',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 1 Alarms Status (true): Battery is already discharged', 
    'byte 0, bit 7 Battery is already discharged'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_1_BATTERY_CAPACITY_CLOSE_TO_ZERO',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 1 Alarms Status (true): Battery capacity close to zero', 
    'byte 1, bit 0 Battery capacity close to zero'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_1_BATTERY_FAULT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 1 Alarms Status (true): Battery fault', 
    'byte 1, bit 1 Battery fault'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_1_PERFORMING_BATTERY_TEST',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 1 Alarms Status (true): Performing battery test', 
    'byte 1, bit 2 Performing battery test'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_1_PLL_FAULT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 1 Alarms Status (true): PLL fault', 
    'byte 1, bit 3 PLL fault'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_1_INVERTER_VOLTAGE_OUT_OF_TOLERANCE',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 1 Alarms Status (true): Inverter voltage out of range', 
    'byte 1, bit 4 Inverter voltage out of range'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_1_OVERLOAD',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 1 Alarms Status (true): Overload', 
    'byte 1, bit 5 Overload'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_1_EMERGENCY_BYPASS_NA',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 1 Alarms Status (true): Emergency bypass not available', 
    'byte 1, bit 6 Emergency bypass not available'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_1_LOAD_SUPPLIED_BY_BYPASS',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 1 Alarms Status (true): Load supplied by by pass', 
    'byte 1, bit 7 Load supplied by by pass'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_1_RETRANFER_BYPASS_INVERTER_BLOCKED',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 1 Alarms Status (true): Retransfer between bypass and inverter is blocked', 
    'byte 2, bit 0 Retransfer between bypass and inverter is blocked'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_1_MANUAL_BYPASS_SWITCH_CLOSED',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 1 Alarms Status (true): Manual bypass switch is closed', 
    'byte 2, bit 1 Manual bypass switch is closed'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_1_OUTPUT_CIRCUIT_BREAKER_OPEN',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 1 Alarms Status (true): Output circuit breaker is open', 
    'byte 2, bit 2 Output circuit breaker is open'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_1_OPTION',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 1 Alarms Status (true): Option', 
    'byte 2, bit 3 Option'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_1_HIGH_TEMPERATURE',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 1 Alarms Status (true): High temperature', 
    'byte 2, bit 4 High temperature'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_1_BYPASS_SWITCH_IN_POSITION',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 1 Alarms Status (true): Bypass switch in position', 
    'byte 2, bit 5 Bypass switch in position'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_1_ELECTRONIC_POWER_OFF',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 1 Alarms Status (true): Electronic power off', 
    'byte 2, bit 6 Electronic power off'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_1_INVERTER_BRIDGE_BLOCKED',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0030', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 1 Alarms Status (true): Inverter bridge is blocked due to current', 
    'byte 2, bit 7 Inverter bridge is blocked due to current'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_2',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0040', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'UPS System 2 Alarms Status', 
    'Alarms Status UPS System 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_2_MAINS_FAILURE',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0040', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 2 Alarms Status (true): Mains failure', 
    'byte 0, bit 0 Mains failure'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_2_RECTIFIER_FAILURE',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0040', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 2 Alarms Status (true): Rectifier failure', 
    'byte 0, bit 1 Rectifier failure'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_2_RECTIFIER_FUSE_BLOWN',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0040', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 2 Alarms Status (true): Rectifier fuse blown', 
    'byte 0, bit 2 Rectifier fuse blown'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_2_THERMAL_IMAGE',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0040', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 2 Alarms Status (true): Thermal image', 
    'byte 0, bit 3 Thermal image'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_2_RECTIFIER_OUT_VOLTAGE_OUT_OF_TOLERANCE',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0040', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 2 Alarms Status (true): Rectifier output voltage out of tolerance', 
    'byte 0, bit 4 Rectifier output voltage out of tolerance'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_2_UPS_PJASE_SEQ_INCORRECT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0040', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 2 Alarms Status (true): UPS phase sequence not correct', 
    'byte 0, bit 5 UPS phase sequence not correct'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_2_BATTERY_SWITCH_OPEN',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0040', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 2 Alarms Status (true): Battery switch open', 
    'byte 0, bit 6 Battery switch open'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_2_BATTERY_ALREADY_DISCHARGED',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0040', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 2 Alarms Status (true): Battery is already discharged', 
    'byte 0, bit 7 Battery is already discharged'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_2_BATTERY_CAPACITY_CLOSE_TO_ZERO',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0040', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 2 Alarms Status (true): Battery capacity close to zero', 
    'byte 1, bit 0 Battery capacity close to zero'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_2_BATTERY_FAULT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0040', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 2 Alarms Status (true): Battery fault', 
    'byte 1, bit 1 Battery fault'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_2_PERFORMING_BATTERY_TEST',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0040', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 2 Alarms Status (true): Performing battery test', 
    'byte 1, bit 2 Performing battery test'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_2_PLL_FAULT',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0040', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 2 Alarms Status (true): PLL fault', 
    'byte 1, bit 3 PLL fault'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_2_INVERTER_VOLTAGE_OUT_OF_TOLERANCE',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0040', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 2 Alarms Status (true): Inverter voltage out of range', 
    'byte 1, bit 4 Inverter voltage out of range'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_2_OVERLOAD',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0040', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 2 Alarms Status (true): Overload', 
    'byte 1, bit 5 Overload'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_2_EMERGENCY_BYPASS_NA',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0040', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 2 Alarms Status (true): Emergency bypass not available', 
    'byte 1, bit 6 Emergency bypass not available'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_2_LOAD_SUPPLIED_BY_BYPASS',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0040', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 2 Alarms Status (true): Load supplied by by pass', 
    'byte 1, bit 7 Load supplied by by pass'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_2_RETRANFER_BYPASS_INVERTER_BLOCKED',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0040', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 2 Alarms Status (true): Retransfer between bypass and inverter is blocked', 
    'byte 2, bit 0 Retransfer between bypass and inverter is blocked'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_2_MANUAL_BYPASS_SWITCH_CLOSED',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0040', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 2 Alarms Status (true): Manual bypass switch is closed', 
    'byte 2, bit 1 Manual bypass switch is closed'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_2_OUTPUT_CIRCUIT_BREAKER_OPEN',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0040', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 2 Alarms Status (true): Output circuit breaker is open', 
    'byte 2, bit 2 Output circuit breaker is open'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_2_OPTION',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0040', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 2 Alarms Status (true): Option', 
    'byte 2, bit 3 Option'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_2_HIGH_TEMPERATURE',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0040', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 2 Alarms Status (true): High temperature', 
    'byte 2, bit 4 High temperature'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_2_BYPASS_SWITCH_IN_POSITION',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0040', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 2 Alarms Status (true): Bypass switch in position', 
    'byte 2, bit 5 Bypass switch in position'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_2_ELECTRONIC_POWER_OFF',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0040', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 2 Alarms Status (true): Electronic power off', 
    'byte 2, bit 6 Electronic power off'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_ALARMS_2_INVERTER_BRIDGE_BLOCKED',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0040', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS System 2 Alarms Status (true): Inverter bridge is blocked due to current', 
    'byte 2, bit 7 Inverter bridge is blocked due to current'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_BATTERY_OUTPUT_1',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0031', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Battery Voltage and Current System 1', 
    'Battery Voltage and Current System 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_BATTERY_OUTPUT_1_VOLTAGE',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0031', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Battery Voltage System 1', 
    'Battery Voltage System 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_BATTERY_OUTPUT_1_CURRENT',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0031', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Battery Current System 1', 
    'Battery Current System 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_BATTERY_OUTPUT_2',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0041', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Battery Voltage and Current System 2', 
    'Battery Voltage and Current System 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_BATTERY_OUTPUT_2_VOLTAGE',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0041', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Battery Voltage System 2', 
    'Battery Voltage System 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_BATTERY_OUTPUT_2_CURRENT',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0041', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Battery Current System 2', 
    'Battery Current System 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_BATTERY_STATUS_1',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0032', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Nominal Battery Autonomy System 1', 
    'Nominal Battery Autonomy System 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_BATTERY_STATUS_1_NOMINAL_AUTONOMY_MINUTES',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0032', 
    '0',  
    'int16',
    'short',
    'none',
    '60.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Nominal Battery Autonomy System 1 in minutes', 
    'Nominal Battery Autonomy System 1 in minutes'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_BATTERY_STATUS_1_NOMINAL_AUTONOMY_PERCENTAGE',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0032', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Nominal Battery Autonomy System 1 in percentage', 
    'Nominal Battery Autonomy System 1 in percentage'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_BATTERY_STATUS_2',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0042', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Nominal Battery Autonomy System 2', 
    'Nominal Battery Autonomy System 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_BATTERY_STATUS_2_NOMINAL_AUTONOMY_MINUTES',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0042', 
    '0',  
    'int16',
    'short',
    'none',
    '60.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Nominal Battery Autonomy System 2 in minutes', 
    'Nominal Battery Autonomy System 2 in minutes'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_BATTERY_STATUS_2_NOMINAL_AUTONOMY_PERCENTAGE',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0042', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Nominal Battery Autonomy System 2 in percentage', 
    'Nominal Battery Autonomy System 2 in percentage'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_BYPASS_VOLTS_1',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0033', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Bypass Voltages by Phase System 1', 
    'Bypass Voltages by Phase System 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_BYPASS_VOLTS_1_PHASE_1',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0033', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Bypass Voltage Phase 1 System 1', 
    'Bypass Voltage Phase 1 System 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_BYPASS_VOLTS_1_PHASE_2',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0033', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Bypass Voltage Phase 2 System 1', 
    'Bypass Voltage Phase 2 System 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_BYPASS_VOLTS_1_PHASE_3',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0033', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Bypass Voltage Phase 3 System 1', 
    'Bypass Voltage Phase 3 System 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_BYPASS_VOLTS_2',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0043', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Bypass Voltages by Phase System 2', 
    'Bypass Voltages by Phase System 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_BYPASS_VOLTS_2_PHASE_1',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0043', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Bypass Voltage Phase 1 System 2', 
    'Bypass Voltage Phase 1 System 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_BYPASS_VOLTS_2_PHASE_2',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0043', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Bypass Voltage Phase 2 System 2', 
    'Bypass Voltage Phase 2 System 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_BYPASS_VOLTS_2_PHASE_3',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0043', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Bypass Voltage Phase 3 System 2', 
    'Bypass Voltage Phase 3 System 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_FREQS_1',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0034', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Bypass and Inverter Frequency System 1', 
    'Bypass and Inverter Frequency System 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_FREQS_1_BYPASS',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0034', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Bypass Frequency System 1', 
    'Bypass Frequency System 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_FREQS_1_INVERTER',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0034', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Inverter Frequency System 1', 
    'Inverter Frequency System 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_FREQS_2',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0044', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Bypass and Inverter Frequency System 2', 
    'Bypass and Inverter Frequency System 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_FREQS_2_BYPASS',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0044', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Bypass Frequency System 2', 
    'Bypass Frequency System 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_FREQS_2_INVERTER',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0044', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Inverter Frequency System 2', 
    'Inverter Frequency System 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_INVERTER_SW_1',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0035', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Inverter Switch Position System 1', 
    'Inverter Switch Position System 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_INVERTER_SW_2',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0045', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Inverter Switch Position System 2', 
    'Inverter Switch Position System 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_INVERTER_VOLTS_1',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0036', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Inverter Voltages by Phase System 1', 
    'Inverter Voltages by Phase System 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_INVERTER_VOLTS_1_PHASE_1',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0036', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Inverter Voltages Phase 1 System 1', 
    'Inverter Voltages Phase 1 System 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_INVERTER_VOLTS_1_PHASE_2',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0036', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Inverter Voltages Phase 2 System 1', 
    'Inverter Voltages Phase 2 System 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_INVERTER_VOLTS_1_PHASE_3',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0036', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Inverter Voltages Phase 3 System 1', 
    'Inverter Voltages Phase 3 System 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_INVERTER_VOLTS_2',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0046', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Inverter Voltages by Phase System 2', 
    'Inverter Voltages by Phase System 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_INVERTER_VOLTS_2_PHASE_1',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0046', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Inverter Voltages Phase 1 System 2', 
    'Inverter Voltages Phase 1 System 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_INVERTER_VOLTS_2_PHASE_2',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0046', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Inverter Voltages Phase 2 System 2', 
    'Inverter Voltages Phase 2 System 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_INVERTER_VOLTS_2_PHASE_3',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0046', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Inverter Voltages Phase 3 System 2', 
    'Inverter Voltages Phase 3 System 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_OUTPUT_CURRENT_1',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0037', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Output Currents by Phase System 1', 
    'Output Currents by Phase System 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_OUTPUT_CURRENT_1_PHASE_1',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0037', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Output Currents Phase 1 System 1', 
    'Output Currents Phase 1 System 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_OUTPUT_CURRENT_1_PHASE_2',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0037', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Output Currents Phase 2 System 1', 
    'Output Currents Phase 2 System 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_OUTPUT_CURRENT_1_PHASE_3',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0037', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Output Currents Phase 3 System 1', 
    'Output Currents Phase 3 System 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_OUTPUT_CURRENT_2',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0047', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Output Currents by Phase System 2', 
    'Output Currents by Phase System 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_OUTPUT_CURRENT_2_PHASE_1',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0047', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Output Currents Phase 1 System 2', 
    'Output Currents Phase 1 System 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_OUTPUT_CURRENT_2_PHASE_2',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0047', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Output Currents Phase 2 System 2', 
    'Output Currents Phase 2 System 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_OUTPUT_CURRENT_2_PHASE_3',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0047', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Output Currents Phase 3 System 2', 
    'Output Currents Phase 3 System 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_OUTPUT_VOLTS_1',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0038', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Output Voltage by Phase System 1', 
    'Output Voltage by Phase System 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_OUTPUT_VOLTS_1_PHASE_1',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0038', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Output Voltage Phase 1 System 1', 
    'Output Voltage Phase 1 System 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_OUTPUT_VOLTS_1_PHASE_2',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0038', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Output Voltage Phase 2 System 1', 
    'Output Voltage Phase 2 System 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_OUTPUT_VOLTS_1_PHASE_3',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0038', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Output Voltage Phase 3 System 1', 
    'Output Voltage Phase 3 System 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_OUTPUT_VOLTS_2',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0048', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Output Voltage by Phase System 2', 
    'Output Voltage by Phase System 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_OUTPUT_VOLTS_2_PHASE_1',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0048', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Output Voltage Phase 1 System 2', 
    'Output Voltage Phase 1 System 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_OUTPUT_VOLTS_2_PHASE_2',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0048', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Output Voltage Phase 2 System 2', 
    'Output Voltage Phase 2 System 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_OUTPUT_VOLTS_2_PHASE_3',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0048', 
    '0',  
    'int16',
    'short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Output Voltage Phase 3 System 2', 
    'Output Voltage Phase 3 System 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_STATUS_1',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0039', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'UPS Status System 1', 
    'UPS Status System 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_STATUS_1_DC_VOLTAGE_WITHIN_TOLERANCE',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0039', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS Status System 1 (true): DC voltage within tolerance', 
    'byte 0, bit 0 DC voltage within tolerance'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_STATUS_1_BATTERY_NORMAL_CAPACITANCE',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0039', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS Status System 1 (true): Battery has normal capacitance', 
    'byte 0, bit 1 Battery has normal capacitance'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_STATUS_1_INVERTER_WITHIN_TOLERANCE',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0039', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS Status System 1 (true): Inverter within tolerance', 
    'byte 0, bit 2 Inverter within tolerance'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_STATUS_1_INVERTER_FEEDS_LOAD',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0039', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS Status System 1 (true): Inverter feeds load', 
    'byte 0, bit 3 Inverter feeds load'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_STATUS_1_INVERTER_SYNCHRONY',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0039', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS Status System 1 (true): Inverter synchrony', 
    'byte 0, bit 4 Inverter synchrony'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_STATUS_1_BYPASS_WITHIN_TOLERANCE',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0039', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS Status System 1 (true): Bypass within tolerance', 
    'byte 0, bit 5 Bypass within tolerance'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_STATUS_2',
    'PTC',
    'integer',
    'IntegerProperty',     
    '0x0049', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'UPS Status System 2', 
    'UPS Status System 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_STATUS_2_DC_VOLTAGE_WITHIN_TOLERANCE',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0049', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS Status System 2 (true): DC voltage within tolerance', 
    'byte 0, bit 0 DC voltage within tolerance'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_STATUS_2_BATTERY_NORMAL_CAPACITANCE',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0049', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS Status System 2 (true): Battery has normal capacitance', 
    'byte 0, bit 1 Battery has normal capacitance'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_STATUS_2_INVERTER_WITHIN_TOLERANCE',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0049', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS Status System 2 (true): Inverter within tolerance', 
    'byte 0, bit 2 Inverter within tolerance'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_STATUS_2_INVERTER_FEEDS_LOAD',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0049', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS Status System 2 (true): Inverter feeds load', 
    'byte 0, bit 3 Inverter feeds load'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_STATUS_2_INVERTER_SYNCHRONY',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0049', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS Status System 2 (true): Inverter synchrony', 
    'byte 0, bit 4 Inverter synchrony'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'UPS_STATUS_2_BYPASS_WITHIN_TOLERANCE',
    'PTC',
    'boolean',
    'BooleanProperty',     
    '0x0049', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'UPS Status System 2 (true): Bypass within tolerance', 
    'byte 0, bit 5 Bypass within tolerance'
);

COMMIT;
END;
/
.
