SET SERVEROUTPUT ON;

DECLARE
    propid NUMBER;

BEGIN

SELECT COUNT(propertytypeid) INTO propid FROM propertytype;

INSERT INTO LRUType VALUES (
    'FLOOG', 
    'Fine Tune Synthesizer First Local Oscillator', 
    'ALMA-55.07.00.00-70.35.30.00-B-ICD', 
    '4655059200000000000',
    'Of these functions, fine tuning will be implemented using the FTS in the second LO, fringe rotation and sideband separation will be implemented using the FTS in both the first and second LOs and phase switching and side band suppression will be implemented using the FTS of the first LO.', 
    ''
);

INSERT INTO AssemblyType VALUES (
    'FLOOG', 
    'FLOOG', 
    'Fine Tune Synthesizer First Local Oscillator', 
    '0x32',
    '0',   
    'none',
    'Of these functions, fine tuning will be implemented using the FTS in the second LO, fringe rotation and sideband separation will be implemented using the FTS in both the first and second LOs and phase switching and side band suppression will be implemented using the FTS of the first LO.', 
    '',
    '1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'FREQ',
    'FLOOG',
    'float',
    'FloatProperty',     
    '0x00001', 
    '0',  
    'uint48',
    'float',
    'hertz',
    '4.44E-7',   
    '0.0',   
    '20000000',   
    '45000000',   
     '4.8000000000000001E-2',     
    '20',   
    '45',   
    '2d', 
    'Frequency vs. Time', 
    'This is a read back of the SET_FREQ command. It is used to verify that the FTS has received the previous SET_FREQ command. It will return zero if the SET_FREQ command has never been called. The default output frequency upon power up of the FTS is DC or zero hertz.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'FTS_STATUS',
    'FLOOG',
    'integer',
    'IntegerProperty',     
    '0x00006', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    'none', 
    'FTS Status', 
    'Status of the output RF power level of the FTS. One bit will latch if a transient problem in the FTS output RF power level is detected. Status of clock cycles between timing events. One of two bits is latched high if the number of clock cycles between TE is not exactly 6000000. The number of clock cycles between TE is counted for both inverted and non-inverted clocks. All latched bits except bit 5 can be unlatched using the RESET_FTS_STATUS command. Bit 5 can be changed with the SET_CLOCK_INVERSION command.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'OUTPUT_RF_LEVEL',
    'FLOOG',
    'boolean',
    'BooleanProperty',     
    '0x00006', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '5',     
    '0',   
    '0',   
    'none', 
    'OUTPUT RF LEVEL', 
    'Status of the output RF power level of the FTS. One bit will latch if a transient problem in the FTS output RF power level is detected. Status of clock cycles between timing events. One of two bits is latched high if the number of clock cycles between TE is not exactly 6000000. The number of clock cycles between TE is counted for both inverted and non-inverted clocks. All latched bits except bit 5 can be unlatched using the RESET_FTS_STATUS command. Bit 5 can be changed with the SET_CLOCK_INVERSION command.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'FEW_NON_INV_CYCLES',
    'FLOOG',
    'boolean',
    'BooleanProperty',     
    '0x00006', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '5',     
    '0',   
    '0',   
    'none', 
    'TOO FEW NON-INVERTED CYCLES', 
    'Status of the output RF power level of the FTS. One bit will latch if a transient problem in the FTS output RF power level is detected. Status of clock cycles between timing events. One of two bits is latched high if the number of clock cycles between TE is not exactly 6000000. The number of clock cycles between TE is counted for both inverted and non-inverted clocks. All latched bits except bit 5 can be unlatched using the RESET_FTS_STATUS command. Bit 5 can be changed with the SET_CLOCK_INVERSION command.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MANY_NON_INV_CYCLES',
    'FLOOG',
    'boolean',
    'BooleanProperty',     
    '0x00006', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '5',     
    '0',   
    '0',   
    'none', 
    'TOO MANY NON-INVERTED CYCLES', 
    'Status of the output RF power level of the FTS. One bit will latch if a transient problem in the FTS output RF power level is detected. Status of clock cycles between timing events. One of two bits is latched high if the number of clock cycles between TE is not exactly 6000000. The number of clock cycles between TE is counted for both inverted and non-inverted clocks. All latched bits except bit 5 can be unlatched using the RESET_FTS_STATUS command. Bit 5 can be changed with the SET_CLOCK_INVERSION command.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'FEW_INV_CYCLES',
    'FLOOG',
    'boolean',
    'BooleanProperty',     
    '0x00006', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '5',     
    '0',   
    '0',   
    'none', 
    'TOO FEW INVERTED CYCLES', 
    'Status of the output RF power level of the FTS. One bit will latch if a transient problem in the FTS output RF power level is detected. Status of clock cycles between timing events. One of two bits is latched high if the number of clock cycles between TE is not exactly 6000000. The number of clock cycles between TE is counted for both inverted and non-inverted clocks. All latched bits except bit 5 can be unlatched using the RESET_FTS_STATUS command. Bit 5 can be changed with the SET_CLOCK_INVERSION command.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MANY_INV_CYCLES',
    'FLOOG',
    'boolean',
    'BooleanProperty',     
    '0x00006', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '5',     
    '0',   
    '0',   
    'none', 
    'TOO MANY INVERTED CYCLES', 
    'Status of the output RF power level of the FTS. One bit will latch if a transient problem in the FTS output RF power level is detected. Status of clock cycles between timing events. One of two bits is latched high if the number of clock cycles between TE is not exactly 6000000. The number of clock cycles between TE is counted for both inverted and non-inverted clocks. All latched bits except bit 5 can be unlatched using the RESET_FTS_STATUS command. Bit 5 can be changed with the SET_CLOCK_INVERSION command.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'CLOCK_STATUS',
    'FLOOG',
    'boolean',
    'BooleanProperty',     
    '0x00006', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '5',     
    '0',   
    '0',   
    'none', 
    '1=inverted, 0=non-inverted', 
    '1=inverted, 0=non-inverted'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'PHASE_OFFSET',
    'FLOOG',
    'float',
    'FloatProperty',     
    '0x00007', 
    '1',  
    'uint24',
    'float',
    'second',
    '8.0E-9',   
    '0.0080',   
    '8.0000000000000002E-3',   
    '1.5999600000000001E-3',   
     '4.8000000000000001E-2',     
    '8',   
    '15.999599999999999',   
    '2d', 
    'Phase Offset vs. Time', 
    'This is the read back of SET_PHASE_OFFSET. It is the amount by which the phase switching is delayed. The delay of the first phase switching value is a minimum of 8 ms and a maximum of 15.9996 ms with 8 ns of resolution. This monitor will return 8 ms if the SET_PHASE_OFFSET control point has never been called i.e., after power up.'
);

COMMIT;
END;
/
.
