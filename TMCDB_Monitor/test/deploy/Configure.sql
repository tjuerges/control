SET SERVEROUTPUT ON;

INSERT INTO Configuration VALUES (
	'1',
	'ATF1',
	'ALMA Test Facility as of Nov. 26, 2007',
	'0',
	'Initial test of the monitor database of the TMCDB'
);

INSERT INTO Component VALUES (
	'1',
	'CONTROL/ALMA01/Dummy',
	'1',
	'1',
	'cpp',
	'1',
	'DummyImpl',
	'IDL:alma/Control/DummyImpl:1.0',
	'alma/Control',
	'0',
	'0',
	'0',
	'-1',
	'-1',
	'-1'
);



COMMIT;
/
.
