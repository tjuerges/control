SET SERVEROUTPUT ON;

DECLARE
    propid NUMBER;

BEGIN

SELECT COUNT(propertytypeid) INTO propid FROM propertytype;

INSERT INTO LRUType VALUES (
    'DGCK', 
    'DTS Digitizer Clock', 
    'ALMA-53.04.00.00-70.35.30.00-A-ICD', 
    '4601491200000000000',
    'A description of the performance requirements for timing and delay is given in Notes on Delay Tracking for ALMA: Resolutions and Tolerance, L. DAddario 2003-Febuary-02.  This memo describes how adjusting the sampler phase is part of an overall delay that is introduced to align the sampled data from different antennas. Additional, coarser delay, is done in the correlator. Adjusting the sampler phase allows control of the delays at the intervals of less than one sample. The coordination and partitioning of the delays, between the DTS Digitizer Clock and the correlator is done, in the ACC, by the control software. The important part of the performance specifications are that the sampler clocks need to be adjusted at most once every 10.8 milliseconds, that the phase needs to be controlled in steps no coarser than 1/8 of a sample clock period and that there is no significant degradation if phase is updated on millisecond boundaries.', 
    ''
);

INSERT INTO AssemblyType VALUES (
    'DGCK', 
    'DGCK', 
    'DTS Digitizer Clock', 
    '0x30',
    '0',   
    'none',
    'A description of the performance requirements for timing and delay is given in Notes on Delay Tracking for ALMA: Resolutions and Tolerance, L. DAddario 2003-Febuary-02.  This memo describes how adjusting the sampler phase is part of an overall delay that is introduced to align the sampled data from different antennas. Additional, coarser delay, is done in the correlator. Adjusting the sampler phase allows control of the delays at the intervals of less than one sample. The coordination and partitioning of the delays, between the DTS Digitizer Clock and the correlator is done, in the ACC, by the control software. The important part of the performance specifications are that the sampler clocks need to be adjusted at most once every 10.8 milliseconds, that the phase needs to be controlled in steps no coarser than 1/8 of a sample clock period and that there is no significant degradation if phase is updated on millisecond boundaries.', 
    '',
    '1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'DGCK_STATUS',
    'DGCK',
    'integer',
    'IntegerProperty',     
    '0x00063', 
    '1',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '1',   
    'none', 
    'na', 
    'This monitor point indicates the status of various parts of the digitizer clock. Bit 0 lets the software know if the phase locked loop has been unlocked since last readout. This monitor point voltage is latched so that if the loop goes out of lock for a short period of time, before returning to lock, this monitor point will remain set. The only way to reset this monitor point is to call the PLL_LOCK_RESET command.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'PLL_OUT_OF_LOCK',
    'DGCK',
    'boolean',
    'BooleanProperty',     
    '0x00063', 
    '1',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '0',     
    '0',   
    '1',   
    'none', 
    'PLL is out of lock', 
    '1 if the PLL is out of lock.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'MISSED_COMMAND',
    'DGCK',
    'integer',
    'IntegerProperty',     
    '0x00064', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '5',     
    '0',   
    '1',   
    'none', 
    'na', 
    'This monitor point indicates if the Digitizer clock hardware has not received a PHASE_COMMAND for every 48ms timing interval since the last MISSED_COMMAND_RESET command was sent.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'PHASE_COMMAND_MISSED',
    'DGCK',
    'boolean',
    'BooleanProperty',     
    '0x00064', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '0',     
    '0',   
    '1',   
    'none', 
    'Phase command missing', 
    '1 if a phase command was missing'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'PS_VOLTAGE_CLOCK',
    'DGCK',
    'float',
    'FloatProperty',     
    '0x00071', 
    '0',  
    'uint16',
    'float',
    'volt',
    '0.009775171065494',   
    '0.0',   
    '0',   
    '10',   
     '60',     
    '0',   
    '10',   
    '8.3f', 
    'Power Supply Voltage', 
    'The measured voltage of the clock module +6V power supply.'
);

COMMIT;
END;
/
.
