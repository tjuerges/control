SET SERVEROUTPUT ON;

DECLARE
    propid NUMBER;

BEGIN

SELECT COUNT(propertytypeid) INTO propid FROM propertytype;

INSERT INTO LRUType VALUES (
    'MountVertexPrototype', 
    'Antenna Control Unit', 
    'ALMA-US-ICD-9', 
    '4571683200000000000',
    'The purpose of this document is to define the interface between the mount component running in an ABM and the ACU. The ICD provides the interface definitions for all monitor and control points accepted by the ACU as part of the low level functionality which is identified at present for the control of the antenna.', 
    ''
);

INSERT INTO AssemblyType VALUES (
    'MountVertexPrototype', 
    'MountVertexPrototype', 
    'Antenna Control Unit', 
    '0x0',
    '2',   
    'none',
    'The purpose of this document is to define the interface between the mount component running in an ABM and the ACU. The ICD provides the interface definitions for all monitor and control points accepted by the ACU as part of the low level functionality which is identified at present for the control of the antenna.', 
    '',
    '1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_ENC_STATUS',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x0018', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '1',     
    '0',   
    '0',   
    '2d', 
    'Azimuth Encoder Status', 
    'Status of azimuth encoder.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_ENC_READ_HEAD_ERROR',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0018', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Encoder Fault (true): Encoder read head error', 
    'byte 0, bit 0 Encoder read head error.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_ENC_EEU_COMPUTER_FAILURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0018', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Encoder Fault (true): EEU computer failure', 
    'byte 0, bit 1 EEU computer failure.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_ENC_EEU_DOES_NOT_RESPOND',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0018', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Encoder Fault (true): EEU does not respond', 
    'byte 0, bit 3 EEU does not respond.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_ENC_EEU_DATA_ERROR',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0018', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Encoder Fault (true): EEU data error', 
    'byte 0, bit 4 EEU data error.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_ENC_POSITION_MEANINGLESS',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0018', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Encoder Fault (true): Position read from encoder does not make sense', 
    'byte 0, bit 5 Position read from encoder does not make sense.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_MOTOR_CURRENTS',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x0019', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Azimuth Motor Currents', 
    'Motor currents in all azimuth axis drive motors.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_MOTOR_CURRENTS_MOTOR1',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x0019', 
    '0',  
    'ubyte',
    'unsigned char',
    'ampere',
    '1.0',   
    '0.0',   
    '0',   
    '255',   
     '0',     
    '0',   
    '255',   
    '2d', 
    'Azimuth Motor Currents: Motor 1', 
    'Motor currents in azimuth axis drive motor 1.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_MOTOR_CURRENTS_MOTOR2',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x0019', 
    '0',  
    'ubyte',
    'unsigned char',
    'ampere',
    '1.0',   
    '0.0',   
    '0',   
    '255',   
     '0',     
    '0',   
    '255',   
    '2d', 
    'Azimuth Motor Currents: Motor 2', 
    'Motor currents in azimuth axis drive motor 2.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_MOTOR_TEMPS',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x001a', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Azimuth Motor Temperatures', 
    'Motor temperatures in all azimuth axis drive motors.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_MOTOR_TEMPS_MOTOR1',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x001a', 
    '0',  
    'ubyte',
    'short',
    'kelvin',
    '1.0',   
    '-323.0',   
    '-323',   
    '-68',   
     '0',     
    '-323',   
    '-68',   
    '2d', 
    'Azimuth Motor Temperatures: Motor 1', 
    'Motor temperature in azimuth axis drive motor 1.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_MOTOR_TEMPS_MOTOR2',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x001a', 
    '0',  
    'ubyte',
    'short',
    'kelvin',
    '1.0',   
    '-323.0',   
    '-323',   
    '-68',   
     '0',     
    '-323',   
    '-68',   
    '2d', 
    'Azimuth Motor Temperatures: Motor 2', 
    'Motor temperature in azimuth axis drive motor 2.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_MOTOR_TORQUE',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x0015', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Azimuth Motor Torques', 
    'Motor torques in all azimuth axis drive motors.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_MOTOR_TORQUE_MOTOR1',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x0015', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '255',   
     '0',     
    '0',   
    '255',   
    '2d', 
    'Azimuth Motor Torques: Motor 1', 
    'Motor torque in azimuth axis drive motor 1.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_MOTOR_TORQUE_MOTOR2',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x0015', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '255',   
     '0',     
    '0',   
    '255',   
    '2d', 
    'Azimuth Motor Torques: Motor 2', 
    'Motor torque in azimuth axis drive motor 2.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Azimuth Axis Status', 
    'Status of azimuth axis. Conditions may be fault conditions or status information. Fault conditions require the use of the CLEAR_FAULT_CMD to clear, while status information will clear then the hardware condition is cleared.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_PRELIMIT_CW',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): prelimit cw', 
    'byte0, bit 0 prelimit cw (set=in limit)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_PRELIMIT_CCW',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): prelimit ccw', 
    'byte0, bit 1 prelimit ccw (set=in limit)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_LIMIT_CW',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): limit cw', 
    'byte0, bit 2 limit cw (set=in limit)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_LIMIT_CCW',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): limit ccw', 
    'byte0, bit 3 limit ccw (set=in limit)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_EMERGENCY_LIMIT_CW',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): emergency limit cw', 
    'byte0, bit 4 emergency limit cw (set=in limit)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_EMERGENCY_LIMIT_CCW',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): emergency limit ccw', 
    'byte0, bit 5 emergency limit ccw (set=in limit)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_SECOND_EMERGENCY_LIMIT_CW',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): second emergency limit cw', 
    'byte0, bit 6 second emergency limit cw (set=in limit)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_SECOND_EMERGENCY_LIMIT_CCW',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): second emergency limit ccw', 
    'byte0, bit 7 second emergency limit ccw (set=in limit)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_SERVO_FAILURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Failure (true): servo failure', 
    'byte1, bit 0 servo failure (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_OVERSPEED',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Failure (true): overspeed', 
    'byte1, bit 1 overspeed (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_NO_MOTION',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Failure (true): no motion', 
    'byte1, bit 2 no motion (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_SPEED_ZERO',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): speed zero', 
    'byte1, bit 3 speed zero'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_STOW_POSITION',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): stow position', 
    'byte1, bit 4 stow position'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_ENCODER_FAILURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Failure (true): encoder failure', 
    'byte1, bit 5 encoder failure (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_INSANE_VELOCITY_FEEDBACK',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Failure (true): insane velocity feedback', 
    'byte1, bit 6 insane velocity feedback (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_DC_BUS_OVERVOLTAGE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Failure (true): DC bus over-voltage', 
    'byte1, bit 7 DC bus over-voltage (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_BRAKE1_FAILURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Failure (true): brake 1 failure', 
    'byte2, bit 0 brake 1 failure (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_BRAKE2_FAILURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Failure (true): brake 2 failure', 
    'byte2, bit 1 brake 2 failure (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_AMPLIFIER1_FAILURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Failure (true): amplifier 1 failure', 
    'byte3, bit 0 amplifier 1 failure (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_AMPLIFIER2_FAILURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Failure (true): amplifier 2 failure', 
    'byte3, bit 1 amplifier 2 failure (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_MOTOR1_OVER_TEMPERATURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Failure (true): motor 1 over temperature', 
    'byte4, bit 0 motor 1 over temperature (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_MOTOR2_OVER_TEMPERATURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Failure (true): motor 2 over temperature', 
    'byte4, bit 1 motor 2 over temperature (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_REGENERATION_RESISTOR_OVERTEMPERATURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Failure (true): regeneration resistor over temperature', 
    'byte4, bit 4 regeneration resistor over temperature (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_SERVO_OSCILLATION',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Failure (true): servo oscillation', 
    'byte4, bit 5 servo oscillation (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_AUX_MOTOR1_OFF',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): AUX - motor 1 off', 
    'byte6, bit 0 AUX - motor 1 off'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_AUX_MOTOR2_OFF',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): AUX - motor 2 off', 
    'byte6, bit 1 AUX - motor 2 off'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_COMPUTER_DISABLED',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): computer disabled', 
    'byte7, bit 0 computer disabled'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_AXIS_DISABLED',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): axis disabled', 
    'byte7, bit 1 axis disabled'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_PCU_OPERATION',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true):  PCU operation', 
    'byte7, bit 2 PCU operation'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_AXIS_IN_STOP',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): axis in stop', 
    'byte7, bit 3 axis in stop'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_STATUS_FLIPFLOP_BUFFER_POSITION_INCORRECT',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x001b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Azimuth Axis Status (true): flip-flop buffer position incorrectly', 
    'byte7, bit 4 flip-flop buffer position incorrectly'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_MOTOR_CURRENTS',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x0009', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Elevation Motor Currents', 
    'Motor currents in all elevation axis drive motors.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_MOTOR_CURRENTS_MOTOR1',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x0009', 
    '0',  
    'ubyte',
    'unsigned char',
    'ampere',
    '1.0',   
    '0.0',   
    '0',   
    '255',   
     '0',     
    '0',   
    '255',   
    '2d', 
    'Elevation Motor Currents: Motor 1', 
    'Motor currents in elevation axis drive motor 1.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_MOTOR_CURRENTS_MOTOR2',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x0009', 
    '0',  
    'ubyte',
    'unsigned char',
    'ampere',
    '1.0',   
    '0.0',   
    '0',   
    '255',   
     '0',     
    '0',   
    '255',   
    '2d', 
    'Elevation Motor Currents: Motor 2', 
    'Motor currents in elevation axis drive motor 2.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_MOTOR_CURRENTS_MOTOR3',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x0009', 
    '0',  
    'ubyte',
    'unsigned char',
    'ampere',
    '1.0',   
    '0.0',   
    '0',   
    '255',   
     '0',     
    '0',   
    '255',   
    '2d', 
    'Elevation Motor Currents: Motor 3', 
    'Motor currents in elevation axis drive motor 3.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_MOTOR_CURRENTS_MOTOR4',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x0009', 
    '0',  
    'ubyte',
    'unsigned char',
    'ampere',
    '1.0',   
    '0.0',   
    '0',   
    '255',   
     '0',     
    '0',   
    '255',   
    '2d', 
    'Elevation Motor Currents: Motor 4', 
    'Motor currents in elevation axis drive motor 4.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_MOTOR_TEMPS',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x000a', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Elevation Motor Temperatures', 
    'Motor temperatures in all elevation axis drive motors.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_MOTOR_TEMPS_MOTOR1',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x000a', 
    '0',  
    'ubyte',
    'short',
    'kelvin',
    '1.0',   
    '-323.0',   
    '-323',   
    '-68',   
     '0',     
    '-323',   
    '-68',   
    '2d', 
    'Elevation Motor Temperatures: Motor 1', 
    'Motor temperatures in elevation axis drive motor 1.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_MOTOR_TEMPS_MOTOR2',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x000a', 
    '0',  
    'ubyte',
    'short',
    'kelvin',
    '1.0',   
    '-323.0',   
    '-323',   
    '-68',   
     '0',     
    '-323',   
    '-68',   
    '2d', 
    'Elevation Motor Temperatures: Motor 2', 
    'Motor temperatures in elevation axis drive motor 2.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_MOTOR_TEMPS_MOTOR3',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x000a', 
    '0',  
    'ubyte',
    'short',
    'kelvin',
    '1.0',   
    '-323.0',   
    '-323',   
    '-68',   
     '0',     
    '-323',   
    '-68',   
    '2d', 
    'Elevation Motor Temperatures: Motor 3', 
    'Motor temperatures in elevation axis drive motor 3.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_MOTOR_TEMPS_MOTOR4',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x000a', 
    '0',  
    'ubyte',
    'short',
    'kelvin',
    '1.0',   
    '-323.0',   
    '-323',   
    '-68',   
     '0',     
    '-323',   
    '-68',   
    '2d', 
    'Elevation Motor Temperatures: Motor 4', 
    'Motor temperatures in elevation axis drive motor 4.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_MOTOR_TORQUE',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x0005', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Elevation Motor Torques', 
    'Motor temperatures in all elevation axis drive motors.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_MOTOR_TORQUE_MOTOR1',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x0005', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '255',   
    '2d', 
    'Elevation Motor Torques: Motor 1', 
    'Motor temperatures in elevation axis drive motor 1.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_MOTOR_TORQUE_MOTOR2',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x0005', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '255',   
    '2d', 
    'Elevation Motor Torques: Motor 2', 
    'Motor temperatures in elevation axis drive motor 2.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_MOTOR_TORQUE_MOTOR3',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x0005', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '255',   
    '2d', 
    'Elevation Motor Torques: Motor 3', 
    'Motor temperatures in elevation axis drive motor 3.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_MOTOR_TORQUE_MOTOR4',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x0005', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '255',   
    '2d', 
    'Elevation Motor Torques: Motor 4', 
    'Motor temperatures in elevation axis drive motor 4.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_ENC_STATUS',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x0008', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '1',     
    '0',   
    '0',   
    '2d', 
    'Elevation Encoder Status', 
    'Status of elevation encoders.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_ENC_1_READ_HEAD_ERROR',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0008', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Encoder Fault (true): Encoder read head error', 
    'byte 0, bit 0 Encoder read head error (encoder 1).'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_ENC_1_EEU_COMPUTER_FAILURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0008', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Encoder Fault (true):  EEU computer failure', 
    'byte 0, bit 1 EEU computer failure (encoder 1).'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_ENC_1_EEU_DOES_NOT_RESPOND',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0008', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Encoder Fault (true):  EEU does not respond', 
    'byte 0, bit 3EEU does not respond (encoder 1).'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_ENC_1_EEU_DATA_ERROR',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0008', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Encoder Fault (true): EEU data error', 
    'byte 0, bit 4 EEU data error (encoder 1).'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_ENC_1_POSITION_MEANINGLESS',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0008', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Encoder Fault (true): Position read from encoder does not make sense', 
    'byte 0, bit 5 Position read from encoder does not make sense (encoder 1).'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_ENC_2_READ_HEAD_ERROR',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0008', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Encoder Fault (true): Encoder read head error', 
    'byte 1, bit 0 Encoder read head error (encoder 2).'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_ENC_2_EEU_COMPUTER_FAILURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0008', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Encoder Fault (true):  EEU computer failure', 
    'byte 1, bit 1 EEU computer failure (encoder 2).'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_ENC_2_EEU_DOES_NOT_RESPOND',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0008', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Encoder Fault (true):  EEU does not respond', 
    'byte 1, bit 3 EEU does not respond (encoder 2).'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_ENC_2_EEU_DATA_ERROR',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0008', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Encoder Fault (true):  EEU data error', 
    'byte 1, bit 4 EEU data error (encoder 2).'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_ENC_2_POSITION_MEANINGLESS',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0008', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Encoder Fault (true): Position read from encoder does not make sense', 
    'byte 1, bit 5 Position read from encoder does not make sense (encoder 2).'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_ENC_ELEVATION_ENCODERS_DISAGREE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0008', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Encoder Status (true): Elevation encoders disagree', 
    'byte 2, bit 0 Elevation encoders disagree.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Elevation Axis Status', 
    'Status of elevation axis. Conditions may be fault conditions or status information. Fault conditions require the use of the CLEAR_FAULT_CMD to clear, while status information will clear then the hardware condition is cleared.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_PRELIMIT_UP',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): prelimit up', 
    'byte0, bit 0 prelimit up (set=in limit)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_PRELIMIT_DOWN',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): prelimit down', 
    'byte0, bit 1 prelimit down (set=in limit)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_LIMIT_UP',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): limit up', 
    'byte0, bit 2 limit up (set=in limit)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_LIMIT_DOWN',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): limit down', 
    'byte0, bit 3 limit down (set=in limit)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_EMERGENCY_LIMIT_UP',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): emergency limit up', 
    'byte0, bit 4 emergency limit up (set=in limit)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_EMERGENCY_LIMIT_DOWN',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): emergency limit down', 
    'byte0, bit 5 emergency limit down (set=in limit)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_SERVO_FAILURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Failure (true): servo failure', 
    'byte1, bit 0 servo failure (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_OVERSPEED',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Failure (true): overspeed', 
    'byte1, bit 1 overspeed (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_NO_MOTION',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Failure (true): no motion', 
    'byte1, bit 2 no motion (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_SPEED_ZERO',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): speed zero', 
    'byte1, bit 3 speed zero'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_STOW_POSITION',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): stow position', 
    'byte1, bit 4 stow position'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_ENCODER_FAILURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Failure (true): encoder failure', 
    'byte1, bit 5 encoder failure (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_INSANE_VELOCITY_FEEDBACK',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Failure (true): insane velocity feedback', 
    'byte1, bit 6 insane velocity feedback (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_DC_BUS_OVERVOLTAGE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Failure (true): DC bus over-voltage', 
    'byte1, bit 7 DC bus over-voltage (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_BRAKE1_FAILURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Failure (true): brake 1 failure', 
    'byte2, bit 0 brake 1 failure (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_BRAKE2_FAILURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Failure (true): brake 2 failure', 
    'byte2, bit 1 brake 2 failure (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_BRAKE3_FAILURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Failure (true): brake 3 failure', 
    'byte2, bit 2 brake 3 failure (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_BRAKE4_FAILURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Failure (true): brake 4 failure', 
    'byte2, bit 3 brake 4 failure (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_AMPLIFIER1_FAILURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Failure (true): amplifier 1 failure', 
    'byte3, bit 0 amplifier 1 failure (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_AMPLIFIER2_FAILURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Failure (true): amplifier 2 failure', 
    'byte3, bit 1 amplifier 2 failure (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_AMPLIFIER3_FAILURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Failure (true): amplifier 3 failure', 
    'byte3, bit 2 amplifier 3 failure (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_AMPLIFIER4_FAILURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Failure (true): amplifier 4 failure', 
    'byte3, bit 3 amplifier 3 failure (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_MOTOR1_OVER_TEMPERATURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Failure (true): motor 1 over temperature', 
    'byte4, bit 0 motor 1 over temperature (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_MOTOR2_OVER_TEMPERATURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Failure (true): motor 2 over temperature', 
    'byte4, bit 1 motor 2 over temperature (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_MOTOR3_OVER_TEMPERATURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Failure (true): motor 3 over temperature', 
    'byte4, bit 2 motor 3 over temperature (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_MOTOR4_OVER_TEMPERATURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Failure (true): motor 4 over temperature', 
    'byte4, bit 3 motor 4 over temperature (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_REGENERATION_RESISTOR_OVERTEMPERATURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Failure (true): regeneration resistor over temperature', 
    'byte4, bit 4 regeneration resistor over temperature (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_SERVO_OSCILLATION',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Failure (true): servo oscillation', 
    'byte4, bit 5 servo oscillation (set=failure)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_AUX_MOTOR1_OFF',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): AUX - motor 1 off', 
    'byte6, bit 0 AUX - motor 1 off'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_AUX_MOTOR2_OFF',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): AUX - motor 2 off', 
    'byte6, bit 1 AUX - motor 2 off'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_AUX_MOTOR3_OFF',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): AUX - motor 3 off', 
    'byte6, bit 2 AUX - motor 3 off'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_AUX_MOTOR4_OFF',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): AUX - motor 4 off', 
    'byte6, bit 3 AUX - motor 4 off'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_COMPUTER_DISABLED',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): computer disabled', 
    'byte7, bit 0 computer disabled'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_AXIS_DISABLED',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): axis disabled', 
    'byte7, bit 1 axis disabled'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_PCU_OPERATION',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true):  PCU operation', 
    'byte7, bit 2 PCU operation'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_AXIS_IN_STOP',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): axis in stop', 
    'byte7, bit 3 axis in stop'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_STATUS_ELEVATION_ABOVE_90_DEG',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x000b', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Elevation Axis Status (true): elevation above 90 degress', 
    'byte7, bit 4 elevation above 90 degrees'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SHUTTER',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x002e', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '2',   
    '2d', 
    'Shutter position', 
    'Shutter mechanism status.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'STOW_PIN',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x0024', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'Stow Pin positions', 
    'Position of antenna stow pins.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'STOW_PIN_AZ_INSERTED',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0024', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Stow Pin (true): Azimuth inserted', 
    'byte0, bit 0 azimuth stow pin inserted (set= inserted)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'STOW_PIN_AZ_RELEASED',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0024', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Stow Pin (true): Azimuth released', 
    'byte0, bit 1 azimuth stow pin released (set=released)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'STOW_PIN_EL_INSERTED',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0024', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Stow Pin (true): Elevation inserted', 
    'byte1, bit 0 elevation stow pin inserted (set=inserted)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'STOW_PIN_EL_RELEASED',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0024', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Stow Pin (true): Elevation released', 
    'byte1, bit 1 elevation stow pin released (set=released)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    '2d', 
    'System Status', 
    'Status of antenna related systems. Conditions may be fault conditions or status information. Fault conditions require the use of the CLEAR_FAULT_CMD to clear, while status information will clear then the hardware condition is cleared.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_SAFE_SWITCH',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): Safe switch', 
    'byte0, bit 0 safe switch'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_POWER_FAILURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Failure (true): Power failure', 
    'byte0, bit 1 power failure'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_24V_FAILURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Failure (true): 24 V failure', 
    'byte0, bit 2 24V power failure'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_BREAKER_FAILURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Failure (true): Breaker failure', 
    'byte0, bit 3 breaker failure'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_COMM_ERROR_ACU_PTC',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Failure (true): Communication error ACU-PTC', 
    'byte0, bit 4 communication error ACU-PTC'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_COMM_ERROR_ACU_PLC',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Failure (true): Communication error ACU-PLC', 
    'byte0, bit 5 communication error ACU-PLC'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_CABINET_OVERTEMPERATURE',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Failure (true): Cabinet over temperature', 
    'byte0, bit 6 cabinet over temperature'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_CABINET_OPEN_DOOR',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): Cabinet door open', 
    'byte0, bit 7 cabinet door open'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_PLATFORM2_HANDRAILS_NOT_LOWERED',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): Platform 2 handrails not lowered', 
    'byte1, bit 0 platform 2 handrails not lowered'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_RAMP_TO_RECEIVER_CABIN_NOT_TILTED_UP',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): Ramp to receiver cabin not tilted up', 
    'byte1, bit 1 ramp to receiber cabin not tilted up'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_HOIST_INTERLOCK',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): Hoist interlock', 
    'byte1, bit 2 hoist interlock'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_ACCESS_BAR_TO_PLATFORM2_OPEN',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): Access bat to platform 2 open', 
    'byte1, bit 3 access bar to platform 2 open'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_ACCESS_DOOR_TO_PLATFORM1_OPEN',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): Access door to platform 1 open', 
    'byte1, bit 4 access door to platform 1 open'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_RECEIVER_CABIN_DOOR_OPEN',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): Receiver cabin door open', 
    'byte1, bit 5 receiver cabin door open'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_TIMING_PULSE_MISSING',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Status (true): Timing pulse missing', 
    'byte1, bit 6 timing pulse missing'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_CABINET_EMERGENCY_STOP',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Failure (true): Cabinet emergency stop engaged', 
    'byte2, bit 0 cabinet (equiment rack) emergency stop (set=engaged)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_EQUIPMENT_RACK_EMERGENCY_STOP',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Failure (true): Equipment rack emergency stop engaged', 
    'byte2, bit 1 equipment rack emergency stop (set=engaged)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_ANTENNA_BASE_EMERGENCY_STOP',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Failure (true): Antenna base emergency stop engaged', 
    'byte2, bit 2 antenna base (set=engaged)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_PLATFORM2_EMERGENCY_STOP',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Failure (true): Platform 2 emergency stop engaged', 
    'byte2, bit 3 platform 2 (set=engaged)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_STAIRWAY_TO_PLATFORM1_EMERGENCY_STOP',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Failure (true): Stairway to platform 1 emergency stop engaged', 
    'byte2, bit 4 stairway to platform 1 (set=engaged)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'SYSTEM_STATUS_RECEIVER_CABIN_EMERGENCY_STOP',
    'MountVertexPrototype',
    'boolean',
    'BooleanProperty',     
    '0x0023', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'System Failure (true): Receiver cabin emergency stop engaged', 
    'byte2, bit 5 receiver cabin (set=engaged)'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'AZ_BRAKE',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x0014', 
    '0',  
    'ubyte',
    'unsigned int',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    'none', 
    'Azimuth brake', 
    'Get azimuth brake status.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EL_BRAKE',
    'MountVertexPrototype',
    'integer',
    'IntegerProperty',     
    '0x0004', 
    '0',  
    'ubyte',
    'unsigned int',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '5',     
    '0',   
    '0',   
    'none', 
    'Elevation brake', 
    'Get elevation brake status.'
);

COMMIT;
END;
/
.
