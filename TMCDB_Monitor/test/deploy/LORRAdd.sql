SET SERVEROUTPUT ON;

DECLARE
    propid NUMBER;

BEGIN

SELECT COUNT(propertytypeid) INTO propid FROM propertytype;

INSERT INTO LRUType VALUES (
    'LORR', 
    'Local Oscillator Reference Receiver', 
    'ALMA-55.04.00.00-70.35.30.00-B-ICD', 
    '4683744000000000000',
    'The Local Oscillator Reference Receiver (LORR) is located in the receiver cabin of each antenna. It is connected, via fibre optic cable, to the ALMA Operations Site Technical Building and it demodulates the signals transmitted by the central reference distributor to provide, at the antenna, the four fundamental reference/timing signals (2GHz, 125MHz, 25MHz and 48ms).', 
    ''
);

INSERT INTO AssemblyType VALUES (
    'LORR', 
    'LORR', 
    'Local Oscillator Reference Receiver', 
    '0x22',
    '0',   
    'none',
    'The Local Oscillator Reference Receiver (LORR) is located in the receiver cabin of each antenna. It is connected, via fibre optic cable, to the ALMA Operations Site Technical Building and it demodulates the signals transmitted by the central reference distributor to provide, at the antenna, the four fundamental reference/timing signals (2GHz, 125MHz, 25MHz and 48ms).', 
    '',
    '1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'STATUS',
    'LORR',
    'integer',
    'IntegerProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '63',   
     '4.8000000000000001E-2',     
    '0',   
    '255',   
    '3d', 
    'Status', 
    'This monitor point provides a number of staus bits that summarize the operation of critical parts of the LORR.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'DCM_LOCKED',
    'LORR',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '0',     
    '0',   
    '1',   
    'na', 
    'DCM Locked if true', 
    'Indicates if the lock status of the DCM. 1 being locked.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_SUPPLY_12V_NOT_OK',
    'LORR',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '0',     
    '0',   
    '1',   
    'na', 
    '12V Power Supply OK', 
    'Indicates if the 12V power supply is out of range. The range that will set this bit is identical to that specified for the VDC_12 monitor point.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'POWER_SUPPLY_15V_NOT_OK',
    'LORR',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '0',     
    '0',   
    '1',   
    'na', 
    '15V Power Supply OK', 
    'Indicates if the 15V power supply is out of range. The range that will set this bit is identical to that specified for the VDC_15 monitor point.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'OPTICAL_POWER_OFF',
    'LORR',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '0',     
    '0',   
    '1',   
    'na', 
    'Optical Power is present', 
    'This bit is an optical power monitor and indicates if the input optical signal is present. This bit is set if there is inadequate received optical power.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'OUTPUT_2GHZ_LOCKED',
    'LORR',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '0',     
    '0',   
    '1',   
    'na', 
    '2 GHz output locked', 
    'Indicates the 2GHz reference frequency phase lock status. This bit will be set during normal operation indicating phase lock to the central reference. If it is unset (0) or toggling, on a timescale of about 10 seconds, the LORR is having trouble synchronizing.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'OUTPUT_125MHZ_LOCKED',
    'LORR',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '0',     
    '0',   
    '1',   
    'na', 
    '125 MHz output locked', 
    'Indicates the 125MHz reference frequency phase lock status. This bit will be set during normal operation and will only be cleared while the LORR is initializing. If it is unset (0) or toggling, on a timescale of about 10 seconds, the LORR is having trouble synchronizing.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'TE_SHORT_FLAG_SET',
    'LORR',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '0',     
    '0',   
    '1',   
    'na', 
    'TE Short Flag Set', 
    'If the TE detected on the composite is fewer that 6000000 cycles of the 125MHz clock, this flag is set. The flag is reset with CLEAR_TE_FLAGS.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'TE_LONG_FLAG_SET',
    'LORR',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '0',     
    '0',   
    '1',   
    'na', 
    'TE Long Flag Set', 
    'If the TE detected on the composite is more that 6000000 cycles of the 125MHz clock, this flag is set. The flag is reset with CLEAR_TE_FLAGS.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EFC_125_MHZ',
    'LORR',
    'float',
    'FloatProperty',     
    '0x00002', 
    '0',  
    'int16',
    'float',
    'volt',
    '0.00488400488',   
    '0.0',   
    '1',   
    '9',   
     '60',     
    '0',   
    '10',   
    '8.3f', 
    '125MHz Electronic Frequency Control Voltage', 
    'Electronic Frequency Control (tuning) voltage of 125 MHz PLL. This voltage should be steady; fluctuating voltage indicates a problem.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EFC_COMB_LINE_PLL',
    'LORR',
    'float',
    'FloatProperty',     
    '0x00003', 
    '0',  
    'int16',
    'float',
    'volt',
    '0.00488400488',   
    '0.0',   
    '1',   
    '9',   
     '60',     
    '0',   
    '10',   
    '8.3f', 
    'Comb Line Electronic Frequency Control Voltage', 
    'Electronic Frequency Control (tuning) voltage of 2 GHz PLL. This voltage should be steady; fluctuating voltage indicates a problem.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'PWR_25_MHZ',
    'LORR',
    'float',
    'FloatProperty',     
    '0x00004', 
    '0',  
    'int16',
    'float',
    'volt',
    '0.00488400488',   
    '0.0',   
    '0',   
    '10',   
     '60',     
    '-1',   
    '11',   
    '8.3f', 
    '25MHz RF Output Power', 
    '25 MHz RF output power level. The output power is nominally in dBm but the conversion factor between volts and power is variable. 25 MHz to FE'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'PWR_125_MHZ',
    'LORR',
    'float',
    'FloatProperty',     
    '0x00005', 
    '0',  
    'int16',
    'float',
    'volt',
    '0.00488400488',   
    '0.0',   
    '0',   
    '10',   
     '60',     
    '-1',   
    '11',   
    '8.3f', 
    '125MHz RF Output Power', 
    '125 MHz RF output power level. The output power is nominally in dBm but the conversion factor between volts and power is variable.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'PWR_2_GHZ',
    'LORR',
    'float',
    'FloatProperty',     
    '0x00006', 
    '0',  
    'int16',
    'float',
    'volt',
    '0.00488400488',   
    '0.0',   
    '0',   
    '10',   
     '60',     
    '-1',   
    '11',   
    '8.3f', 
    '2GHz RF Output Power', 
    '2 GHz RF output power level. The output power is nominally in dBm but the conversion factor between volts and power is variable.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'RX_OPT_PWR',
    'LORR',
    'float',
    'FloatProperty',     
    '0x00007', 
    '0',  
    'int16',
    'float',
    'watt',
    '4.88400488E-6',   
    '0.0',   
    '2.5000000000000001E-5',   
    '1E-4',   
     '60',     
    '1',   
    '10',   
    '8.3f', 
    'Received Optical Power', 
    'Received optical power. The actual value is a voltage representation of the photodiode receiver current.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'VDC_7',
    'LORR',
    'float',
    'FloatProperty',     
    '0x00008', 
    '1',  
    'int16',
    'float',
    'volt',
    '0.004884005',   
    '0.0',   
    '6.5',   
    '7.5',   
     '60',     
    '0',   
    '20',   
    '7.2f', 
    '7V Power Supply Voltage', 
    '7 VDC voltage regulator output monitor.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'VDC_12',
    'LORR',
    'float',
    'FloatProperty',     
    '0x00009', 
    '0',  
    'int16',
    'float',
    'volt',
    '0.009768009768',   
    '0.0',   
    '11',   
    '13',   
     '60',     
    '0',   
    '20',   
    '7.2f', 
    '12V Power Supply Voltage', 
    '12 VDC voltage regulator output monitor. The 12 V runs through a  divide by 2 voltage divider to accommodate the ADC range.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'VDC_15',
    'LORR',
    'float',
    'FloatProperty',     
    '0x0000a', 
    '0',  
    'int16',
    'float',
    'volt',
    '0.009768009768',   
    '0.0',   
    '14',   
    '16',   
     '60',     
    '0',   
    '20',   
    '7.2f', 
    '15V Power Supply Voltage', 
    '15 VDC voltage regulator output monitor. The 15 V runs through a divide by 2 voltage divider to accommodate the ADC range.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'VDC_MINUS_7',
    'LORR',
    'float',
    'FloatProperty',     
    '0x0000b', 
    '1',  
    'int16',
    'float',
    'volt',
    '0.00488400488',   
    '0.0',   
    '-7.5',   
    '-6.5',   
     '60',     
    '0',   
    '20',   
    '7.2f', 
    'Minus 7 Power Supply Voltage', 
    'Negative 7 VDC voltage regulator output monitor.'
);

COMMIT;
END;
/
.
