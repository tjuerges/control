SET SERVEROUTPUT ON;

DECLARE
    propid NUMBER;

BEGIN

SELECT COUNT(propertytypeid) INTO propid FROM propertytype;

INSERT INTO LRUType VALUES (
    'IFProc', 
    'Intermediate Frequency Down Converter', 
    'ALMA-52.00.00.00-70.35.30.00-C-ICD', 
    '4666032000000000000',
    'Define the interface between the prototype and production IF Downconverter (IFDC) and the Computing Monitor and Control software.', 
    ''
);

INSERT INTO AssemblyType VALUES (
    'IFProc', 
    'IFProc', 
    'Intermediate Frequency Down Converter', 
    'parm',
    '0',   
    'none',
    'Define the interface between the prototype and production IF Downconverter (IFDC) and the Computing Monitor and Control software.', 
    '',
    '1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_STATUS',
    'IFProc',
    'integer',
    'IntegerProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '1',     
    '0',   
    '0',   
    '2d', 
    'Read Status', 
    'Status bits and FIFO Depths. The voltage reference bits indicate that there is a voltage reference to the relative digitizer, but not on the correctness of the voltage. The power voltages will indicate that the voltage is within +/- 5%. FIFO Depths are uint16. Unless otherwise noted, a 1 in a bit indicates the described condition is true, a 0 indicates the condition is false.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'ETHERNET_CONNECTED',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '1',   
    '2d', 
    'Ethernet connected if true', 
    'Bit 0: Ethernet Connected'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'ROUTER_NOT_FOUND',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '1',   
    '2d', 
    'Router not found', 
    'Bit 1: 1 -> Router not found'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'TCP_CONNECTED',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '1',   
    '2d', 
    'TCP connected if true', 
    'Bit 2: TCP connected'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'ARP_FULL',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '1',   
    '2d', 
    'ARP table full', 
    'Bit 3: 1 -> ARP table full'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'DATA_TRANSFER_ACTIVE',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '1',   
    '2d', 
    'Data transfer active if true', 
    'Bit 4: Data Transfer Active'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'TCP_DISCONN_CMD',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '1',   
    '2d', 
    'Disconnected by command if true', 
    'Bit 5: TCP Disconnected by Command'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'TCP_DISCONN_ERROR',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '1',   
    '2d', 
    'Disconnected by error if true', 
    'Bit 6: TCP Disconnected by Error'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'FIFO_FULL',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '1',   
    '2d', 
    'FIFO Full - lost data flag if true', 
    'Bit 7: FIFO Full - Lost Data Flag'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'DIGITAL_1DOT2_GOOD',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '1',   
    '2d', 
    '1.2 V digital power supply good if true', 
    'Bit 0: Digital 1.2V Good'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'DIGITAL_2DOT5_GOOD',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '1',   
    '2d', 
    '2.5 V digital power supply good if true', 
    'Bit 1: Digital 2.5V Good'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'DIGITAL_3DOT3_GOOD',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '1',   
    '2d', 
    '3.3 V digital power supply good if true', 
    'Bit 2: Digital 3.3V Good'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'DIGITAL_5_GOOD',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '1',   
    '2d', 
    '5 V digital power supply good if true', 
    'Bit 3: Digital 5V Good'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'ETHER_CONT_VOLTAGE_GOOD',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '1',   
    '2d', 
    'Ethernet controller voltage good if true', 
    'Bit 4: Ethernet Controller Voltage Good'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'ANALOG_5_GOOD',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '1',   
    '2d', 
    '5 V analog power supply good if true', 
    'Bit 5: Analog 5V Good'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'S0_VREF_GOOD',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '1',   
    '2d', 
    'Sideband 0 voltage reference is good if true', 
    'Bit 0: S0 Vref Good'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'S1_VREF_GOOD',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '1',   
    '2d', 
    'Sideband 1 voltage reference is good if true', 
    'Bit 1: S1 Vref Good'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'A_VREF_GOOD',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '1',   
    '2d', 
    'IFDC Channel A voltage reference is good if true', 
    'Bit 2: A Vref Good'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'B_VREF_GOOD',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '1',   
    '2d', 
    'IFDC Channel B voltage reference is good if true', 
    'Bit 3: B Vref Good'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'C_VREF_GOOD',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '1',   
    '2d', 
    'IFDC Channel C voltage reference is good if true', 
    'Bit 4: C Vref Good'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'D_VREF_GOOD',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '1',     
    '0',   
    '1',   
    '2d', 
    'IFDC Channel D voltage reference is good if true', 
    'Bit 5: D Vref Good'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_STATUS_START_COMM_TEST',
    'IFProc',
    'integer',
    'IntegerProperty',     
    '0x00001', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '1',     
    '0',   
    '0',   
    '2d', 
    'Read back of the START_COMM_TEST command', 
    'Byte 3: Read back of the START_COMM_TEST command Status bits and FIFO Depths. The voltage reference bits indicate that there is a voltage reference to the relative digitizer, but not on the correctness of the voltage. The power voltages will indicate that the voltage is within +/- 5%. FIFO Depths are uint16. Unless otherwise noted, a 1 in a bit indicates the described condition is true, a 0 indicates the condition is false.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_TIMING_ERROR_FLAG',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00002', 
    '1',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '0',     
    '0',   
    '1',   
    '2d', 
    'Indication of a timing error between the 125MHz clock and the 48ms timing event.', 
    'Indication of a timing error between the 125MHz clock and the 48ms timing event.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_DATA_MONITOR_1',
    'IFProc',
    'integer',
    'IntegerProperty',     
    '0x00003', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '3.8147E-5',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Monitor of the Total Power Data.', 
    'Monitor of the Total Power Data. The value is averaged over many samples determined by READ_DATA_AVE_LENGTH.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'TOTAL_POWER_SIDEBAND_0',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00003', 
    '0',  
    'uint16',
    'float',
    'volt',
    '3.8147E-5',   
    '0.0',   
    '0',   
    '2',   
     '0',     
    '0',   
    '2',   
    '2d', 
    'Monitor of the Total Power Data. Sideband 0', 
    'Bytes 0,1: TPS0'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'TOTAL_POWER_SIDEBAND_1',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00003', 
    '0',  
    'uint16',
    'float',
    'volt',
    '3.8147E-5',   
    '0.0',   
    '0',   
    '2',   
     '0',     
    '0',   
    '2',   
    '2d', 
    'Monitor of the Total Power Data. Sideband 1', 
    'Bytes 2,3: TPS1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_DATA_MONITOR_2',
    'IFProc',
    'integer',
    'IntegerProperty',     
    '0x00004', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '3.8147E-5',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Monitor of the Total Power Data.', 
    'Monitor of the Total Power Data. The value is averaged over many samples determined by READ_DATA_AVE_LENGTH.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'TOTAL_POWER_A',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00004', 
    '0',  
    'uint16',
    'float',
    'volt',
    '3.8147E-5',   
    '0.0',   
    '0',   
    '2',   
     '0',     
    '0',   
    '2',   
    '2d', 
    'Monitor of the Total Power Data. IFDC A', 
    'Bytes 0,1: TP-A'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'TOTAL_POWER_B',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00004', 
    '0',  
    'uint16',
    'float',
    'volt',
    '3.8147E-5',   
    '0.0',   
    '0',   
    '2',   
     '0',     
    '0',   
    '2',   
    '2d', 
    'Monitor of the Total Power Data. IFDC B', 
    'Bytes 2,3: TP-B'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'TOTAL_POWER_C',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00004', 
    '0',  
    'uint16',
    'float',
    'volt',
    '3.8147E-5',   
    '0.0',   
    '0',   
    '2',   
     '0',     
    '0',   
    '2',   
    '2d', 
    'Monitor of the Total Power Data. IFDC C', 
    'Bytes 4,5: TP-C'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'TOTAL_POWER_D',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00004', 
    '0',  
    'uint16',
    'float',
    'volt',
    '3.8147E-5',   
    '0.0',   
    '0',   
    '2',   
     '0',     
    '0',   
    '2',   
    '2d', 
    'Monitor of the Total Power Data. IFDC D', 
    'Bytes 6,7: TP-D'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_FIFO_DEPTHS',
    'IFProc',
    'integer',
    'IntegerProperty',     
    '0x00005', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '1',     
    '0',   
    '0',   
    '2d', 
    'Reads the internal and external FIFO depths.', 
    'Reads the internal and external FIFO depths. If one of the FIFOs is full during a write, the FIFO_FULL bit will be set in READ_STATUS'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'FPGA_FIFO_DEPTH',
    'IFProc',
    'integer',
    'IntegerProperty',     
    '0x00005', 
    '0',  
    'uint16',
    'unsigned int',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '2047',   
     '1',     
    '0',   
    '2047',   
    '2d', 
    'FPGA FIFO Depth', 
    'Bytes 0,1: FPGA FIFO Depth - Max of 2047'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'EXTERNAL_FIFO_DEPTH',
    'IFProc',
    'integer',
    'IntegerProperty',     
    '0x00005', 
    '0',  
    'uint16',
    'unsigned int',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '43689',   
     '1',     
    '0',   
    '43689',   
    '2d', 
    'External FIFO Depth', 
    'Bytes 2,3: External FIFO Depth - Max of 43689'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_MODULE_IP_ADDR',
    'IFProc',
    'integer',
    'IntegerProperty',     
    '0x00007', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Ethernet controller IP Address', 
    'The current IP address of the Ethernet controller 4 bytes: Byte 0: first byte of the IP address Byte 1: second byte of the IP address Byte 2: third byte of the IP address Byte 3: fourth byte of the IP address'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_DEST_IP_ADDR',
    'IFProc',
    'integer',
    'IntegerProperty',     
    '0x00008', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'TCP connection IP address', 
    'The current destination IP address for the TCP connection 4 bytes: Byte 0: first byte of the IP address Byte 1: second byte of the IP address Byte 2: third byte of the IP address Byte 3: fourth byte of the IP address'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_TCP_PORT',
    'IFProc',
    'integer',
    'IntegerProperty',     
    '0x0000A', 
    '0',  
    'uint16',
    'unsigned short',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '600',     
    '0',   
    '0',   
    '2d', 
    'TCP Port', 
    'Reads the TCP port for the TCP connection.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_BDB_PER_PACKET',
    'IFProc',
    'integer',
    'IntegerProperty',     
    '0x0000B', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '1.0',   
    '0.0',   
    '32',   
    '255',   
     '0',     
    '32',   
    '255',   
    '2d', 
    'Number of Basic Data Blocks', 
    'Reads the number of Basic Data Blocks in each TCP Packet.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_IFDC_SPI_STATUS',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00010', 
    '0',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '600',     
    '0',   
    '0',   
    '2d', 
    'Status of SPI interface', 
    'Reads the status of the SPI interface to the IF and BB channels.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_VOLTAGES_1',
    'IFProc',
    'integer',
    'IntegerProperty',     
    '0x00011', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '0.00488',   
    '0.0',   
    '0',   
    '0',   
     '10',     
    '0',   
    '0',   
    '2d', 
    'Voltage levels', 
    'Reads the voltage levels in the TPD Board'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_DIG_1DOT2_VOLTAGES',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00011', 
    '0',  
    'int16',
    'float',
    'volt',
    '0.00488',   
    '0.0',   
    '-10',   
    '10',   
     '10',     
    '-10',   
    '10',   
    '2d', 
    '1.2 V digital power supply voltage', 
    'Bytes 0,1: Digital 1.2V'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_DIG_2DOT5_VOLTAGES',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00011', 
    '0',  
    'int16',
    'float',
    'volt',
    '0.00488',   
    '0.0',   
    '-10',   
    '10',   
     '10',     
    '-10',   
    '10',   
    '2d', 
    '2.5 V digital power supply voltage', 
    'Bytes 2,3: Digital 2.5V'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_DIG_3DOT3_VOLTAGES',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00011', 
    '0',  
    'int16',
    'float',
    'volt',
    '0.00488',   
    '0.0',   
    '-10',   
    '10',   
     '10',     
    '-10',   
    '10',   
    '2d', 
    '3.3 V digital power supply voltage', 
    'Bytes 4,5: Digital 3.3V'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_ETHER_CONT_VOLTAGES',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00011', 
    '0',  
    'int16',
    'float',
    'volt',
    '0.00488',   
    '0.0',   
    '-10',   
    '10',   
     '10',     
    '-10',   
    '10',   
    '2d', 
    'Ethernet controller voltage', 
    'Bytes 6,7: Ethernet controller 3.3V'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_VOLTAGES_2',
    'IFProc',
    'integer',
    'IntegerProperty',     
    '0x00012', 
    '0',  
    'ubyte',
    'unsigned char',
    'none',
    '0.00488',   
    '0.0',   
    '0',   
    '0',   
     '10',     
    '0',   
    '0',   
    '2d', 
    'Voltage levels', 
    'Reads the voltage levels in the TPD Board'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_DIG_5_VOLTAGES',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00012', 
    '0',  
    'int16',
    'float',
    'volt',
    '0.00488',   
    '0.0',   
    '-10',   
    '10',   
     '10',     
    '-10',   
    '10',   
    '2d', 
    '5 V digital power supply voltage', 
    'Bytes 0,1: Digital 5V'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_ANALOG_5_VOLTAGES',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00012', 
    '0',  
    'int16',
    'float',
    'volt',
    '0.00488',   
    '0.0',   
    '-10',   
    '10',   
     '10',     
    '-10',   
    '10',   
    '2d', 
    '5 V analog power supply voltage', 
    'Bytes 2,3: Analog 5V'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_7V_POWER_IN',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00012', 
    '0',  
    'int16',
    'float',
    'volt',
    '0.00488',   
    '0.0',   
    '-10',   
    '10',   
     '10',     
    '-10',   
    '10',   
    '2d', 
    '7 V power in supply voltage', 
    'Bytes 4,5:  7V Power In'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_GAINS',
    'IFProc',
    'integer',
    'IntegerProperty',     
    '0x00101', 
    '1',  
    'ubyte',
    'unsigned char',
    'none',
    '0.125',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Attenuator settings for the IFDC', 
    'Reads the current value of the attenuator settings in the IFDC. GS0 and GS1 are gains, the rest are attenuations.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'GA',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00101', 
    '1',  
    'ubyte',
    'float',
    'decibel',
    '0.125',   
    '0.0',   
    '0',   
    '31.5',   
     '0',     
    '0',   
    '31.5',   
    '2d', 
    'Gain A', 
    'Byte 0: GA'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'GB',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00101', 
    '1',  
    'ubyte',
    'float',
    'decibel',
    '0.125',   
    '0.0',   
    '0',   
    '31.5',   
     '0',     
    '0',   
    '31.5',   
    '2d', 
    'Gain B', 
    'Byte 1: GB'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'GC',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00101', 
    '1',  
    'ubyte',
    'float',
    'decibel',
    '0.125',   
    '0.0',   
    '0',   
    '31.5',   
     '0',     
    '0',   
    '31.5',   
    '2d', 
    'Gain C', 
    'Byte 2: GC'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'GD',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00101', 
    '1',  
    'ubyte',
    'float',
    'decibel',
    '0.125',   
    '0.0',   
    '0',   
    '31.5',   
     '0',     
    '0',   
    '31.5',   
    '2d', 
    'Gain D', 
    'Byte 3: GD'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_SIGNAL_PATHS',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00103', 
    '1',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '0',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Filter values in the IFDC', 
    'Reads the current value of the filters in the IFDC.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_FILTER_A_HIGH',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00103', 
    '1',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Channel A', 
    'Bit 0: FA, 1 connects channel A to the high filter, 0 to low'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_FILTER_B_HIGH',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00103', 
    '1',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Channel B', 
    'Bit 1: FB, 1 connects channel B to the high filter, 0 to low'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_FILTER_C_HIGH',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00103', 
    '1',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Channel C', 
    'Bit 2: FC, 1 connects channel C to the high filter, 0 to low'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_FILTER_D_HIGH',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00103', 
    '1',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Channel D', 
    'Bit 3: FD, 1 connects channel D to the high filter, 0 to low'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_SIDEBAND_AB_S1',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00103', 
    '1',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Sideband AB', 
    'Bit 4: SAB, 1 connects channels A and B to S1, 0 to S0'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_SIDEBAND_CD_S1',
    'IFProc',
    'boolean',
    'BooleanProperty',     
    '0x00103', 
    '1',  
    'ubyte',
    'bool',
    'none',
    '1.0',   
    '0.0',   
    '0',   
    '1',   
     '0',     
    '0',   
    '0',   
    '2d', 
    'Sideband CD', 
    'Bit 5: SCD, 1 connects channels C and D to S1, 0 to S0'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_TEMPS_1',
    'IFProc',
    'integer',
    'IntegerProperty',     
    '0x00104', 
    '1',  
    'ubyte',
    'unsigned char',
    'none',
    '0.4',   
    '223.15',   
    '0',   
    '0',   
     '60',     
    '0',   
    '0',   
    '2d', 
    'Temperatures in the IFDC', 
    'Read the temperatures in the IFDC'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_TEMP_SEC_1',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00104', 
    '1',  
    'uint16',
    'float',
    'kelvin',
    '0.4',   
    '223.15',   
    '248.15',   
    '373.15',   
     '60',     
    '-25',   
    '100',   
    '2d', 
    'Temp Section 1', 
    'Bytes 0,1: Temp Output Section 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_TEMP_SEC_2',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00104', 
    '1',  
    'uint16',
    'float',
    'kelvin',
    '0.4',   
    '223.15',   
    '248.15',   
    '373.15',   
     '60',     
    '-25',   
    '100',   
    '2d', 
    'Temp Section 2', 
    'Bytes 2,3: Temp Output Section 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_TEMP_SEC_A',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00104', 
    '1',  
    'uint16',
    'float',
    'kelvin',
    '0.4',   
    '223.15',   
    '248.15',   
    '373.15',   
     '60',     
    '-25',   
    '100',   
    '2d', 
    'Temp Section A', 
    'Bytes 4,5: Temp Output Section A'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_TEMP_SEC_B',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00104', 
    '1',  
    'uint16',
    'float',
    'kelvin',
    '0.4',   
    '223.15',   
    '248.15',   
    '373.15',   
     '60',     
    '-25',   
    '100',   
    '2d', 
    'Temp Section B', 
    'Bytes 6,7: Temp Output Section B'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_TEMPS_2',
    'IFProc',
    'integer',
    'IntegerProperty',     
    '0x00105', 
    '1',  
    'ubyte',
    'unsigned char',
    'none',
    '0.4',   
    '223.15',   
    '0',   
    '0',   
     '60',     
    '0',   
    '0',   
    '2d', 
    'Temperatures in the IFDC', 
    'Read the temperatures in the IFDC'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_TEMP_SEC_C',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00105', 
    '1',  
    'uint16',
    'float',
    'kelvin',
    '0.4',   
    '223.15',   
    '248.15',   
    '373.15',   
     '60',     
    '-25',   
    '100',   
    '2d', 
    'Temp Section C', 
    'Bytes 0,1: Temp Output Section C'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_TEMP_SEC_D',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00105', 
    '1',  
    'uint16',
    'float',
    'kelvin',
    '0.4',   
    '223.15',   
    '248.15',   
    '373.15',   
     '60',     
    '-25',   
    '100',   
    '2d', 
    'Temp Section D', 
    'Bytes 2,3: Temp Output Section D'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_TEMP_SWITCH_MATRIX_1',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00105', 
    '1',  
    'uint16',
    'float',
    'kelvin',
    '0.4',   
    '223.15',   
    '248.15',   
    '373.15',   
     '60',     
    '-25',   
    '100',   
    '2d', 
    'Temp Switch Matrix 1', 
    'Bytes 4,5: Temp Switch Matrix 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_TEMP_SWITCH_MATRIX_2',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00105', 
    '1',  
    'uint16',
    'float',
    'kelvin',
    '0.4',   
    '223.15',   
    '248.15',   
    '373.15',   
     '60',     
    '-25',   
    '100',   
    '2d', 
    'Temp Switch Matrix 2', 
    'Bytes 6,7: Temp Switch Matrix 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_TEMPS_3',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00106', 
    '1',  
    'ubyte',
    'float',
    'kelvin',
    '0.04',   
    '223.15',   
    '248.15',   
    '373.15',   
     '60',     
    '-25',   
    '100',   
    '2d', 
    'Temps in Comm modules', 
    'Bytes 0,1: Temp Communication Module  Read the temperature of the Communication Module in the IFDC'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_10V_CURRENTS',
    'IFProc',
    'integer',
    'IntegerProperty',     
    '0x00107', 
    '1',  
    'ubyte',
    'unsigned char',
    'none',
    '0.0040',   
    '0.0',   
    '0',   
    '0',   
     '60',     
    '0',   
    '0',   
    '2d', 
    'Current in 10 V power supply', 
    'Read the currents on the 10V power in the IFDC'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_10V_CURRENT_SEC_1',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00107', 
    '1',  
    'uint16',
    'float',
    'ampere',
    '0.0040',   
    '0.0',   
    '0',   
    '0',   
     '60',     
    '0',   
    '0',   
    '2d', 
    '10 V Current section 1', 
    'Bytes 0,1: 10V Current Section 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_10V_CURRENT_SEC_2',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00107', 
    '1',  
    'uint16',
    'float',
    'ampere',
    '0.0040',   
    '0.0',   
    '0',   
    '0',   
     '60',     
    '0',   
    '0',   
    '2d', 
    '10 V Current section 2', 
    'Bytes 2,3: 10V Current Section 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_10V_CURR_SW_MATRIX_1',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00107', 
    '1',  
    'uint16',
    'float',
    'ampere',
    '0.0040',   
    '0.0',   
    '0',   
    '0',   
     '60',     
    '0',   
    '0',   
    '2d', 
    '10 V Current Switch matrix 1', 
    'Bytes 4,5: 10V Current Switch Matrix 1'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_10V_CURR_SW_MATRIX_2',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00107', 
    '1',  
    'uint16',
    'float',
    'ampere',
    '0.0040',   
    '0.0',   
    '0',   
    '0',   
     '60',     
    '0',   
    '0',   
    '2d', 
    '10 V Current Switch matrix 2', 
    'Bytes 6,7: 10V Current Switch Matrix 2'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_6_5V_CURRENTS',
    'IFProc',
    'integer',
    'IntegerProperty',     
    '0x00108', 
    '1',  
    'ubyte',
    'unsigned char',
    'none',
    '0.0040',   
    '0.0',   
    '0',   
    '0',   
     '60',     
    '0',   
    '0',   
    '2d', 
    'Current in 6.5 V power supply', 
    'Read the currents on the 6.5V power in the IFDC'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_6_5V_CURRENT_SEC_A',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00108', 
    '1',  
    'uint16',
    'float',
    'ampere',
    '0.0040',   
    '0.0',   
    '0',   
    '0',   
     '60',     
    '0',   
    '0',   
    '2d', 
    '6.5 V Current in section A', 
    'Bytes 0,1: 6.5V Current Section A'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_6_5V_CURRENT_SEC_B',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00108', 
    '1',  
    'uint16',
    'float',
    'ampere',
    '0.0040',   
    '0.0',   
    '0',   
    '0',   
     '60',     
    '0',   
    '0',   
    '2d', 
    '6.5 V Current in section B', 
    'Bytes 2,3: 6.5V Current Section B'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_6_5V_CURRENT_SEC_C',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00108', 
    '1',  
    'uint16',
    'float',
    'ampere',
    '0.0040',   
    '0.0',   
    '0',   
    '0',   
     '60',     
    '0',   
    '0',   
    '2d', 
    '6.5 V Current in section C', 
    'Bytes 4,5: 6.5V Current Section C'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_6_5V_CURRENT_SEC_D',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00108', 
    '1',  
    'uint16',
    'float',
    'ampere',
    '0.0040',   
    '0.0',   
    '0',   
    '0',   
     '60',     
    '0',   
    '0',   
    '2d', 
    '6.5 V Current in section D', 
    'Bytes 6,7: 6.5V Current Section D'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_8V_CURRENTS',
    'IFProc',
    'integer',
    'IntegerProperty',     
    '0x00109', 
    '1',  
    'ubyte',
    'unsigned char',
    'none',
    '0.0040',   
    '0.0',   
    '0',   
    '0',   
     '60',     
    '0',   
    '0',   
    '2d', 
    'Current in 8 V power supply', 
    'Read the currents on the 8V power in the IFDC'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_8V_CURRENT_SEC_A',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00109', 
    '1',  
    'uint16',
    'float',
    'ampere',
    '0.0040',   
    '0.0',   
    '0',   
    '0',   
     '60',     
    '0',   
    '0',   
    '2d', 
    '8 V Current in section A', 
    'Bytes 0,1: 8V Current Section A'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_8V_CURRENT_SEC_B',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00109', 
    '1',  
    'uint16',
    'float',
    'ampere',
    '0.0040',   
    '0.0',   
    '0',   
    '0',   
     '60',     
    '0',   
    '0',   
    '2d', 
    '8 V Current in section B', 
    'Bytes 2,3:8V Current Section B'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_8V_CURRENT_SEC_C',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00109', 
    '1',  
    'uint16',
    'float',
    'ampere',
    '0.0040',   
    '0.0',   
    '0',   
    '0',   
     '60',     
    '0',   
    '0',   
    '2d', 
    '8 V Current in section C', 
    'Bytes 4,5: 8V Current Section C'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    'READ_8V_CURRENT_SEC_D',
    'IFProc',
    'float',
    'FloatProperty',     
    '0x00109', 
    '1',  
    'uint16',
    'float',
    'ampere',
    '0.0040',   
    '0.0',   
    '0',   
    '0',   
     '60',     
    '0',   
    '0',   
    '2d', 
    '8 V Current in section D', 
    'Bytes 6,7: 8V Current Section D'
);

COMMIT;
END;
/
.
