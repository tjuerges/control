-- TMCDB SQL TABLE DEFINITIONS Version 1.4 2007-MAR-9
--
-- /////////////////////////////////////////////////////////////////
-- // WARNING!  DO NOT MODIFY THIS FILE!                          //
-- //  ---------------------------------------------------------  //
-- // | This is generated code!  Do not modify this file.       | //
-- // | Any changes will be lost when the file is re-generated. | //
-- //  ---------------------------------------------------------  //
-- /////////////////////////////////////////////////////////////////
CREATE  TABLE Configuration (
    ConfigurationId NUMBER (3)     NOT NULL ,
    ConfigurationName VARCHAR    (32) NOT NULL ,
    FullName VARCHAR    (80) NOT NULL ,
    CreationTime NUMBER (19)     NOT NULL ,
    Description VARCHAR    (1024) NOT NULL ,
    CONSTRAINT ConfigKey PRIMARY KEY (ConfigurationId)
);
CREATE  TABLE Computer (
    ComputerId NUMBER (5)     NOT NULL ,
    ComputerName VARCHAR    (80) NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    HostName VARCHAR    (80) NOT NULL ,
    RealTime CHAR (1) NOT NULL     ,
    ProcessorType CHAR    (3) NOT NULL ,
    PhysicalLocation VARCHAR    (1024) NULL ,
    CONSTRAINT ComputerRealTime CHECK (RealTime IN ('0', '1')),
    CONSTRAINT ComputerKey PRIMARY KEY (ComputerId),
    CONSTRAINT ComputerConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT ComputerProcessorType CHECK (ProcessorType IN ('uni', 'smp'))
);
CREATE  TABLE LoggingConfig (
    LoggingConfigId NUMBER (10)     NOT NULL ,
    MinLogLevelDefault NUMBER (3)     DEFAULT 2,
    MinLogLevelLocalDefault NUMBER (3)     DEFAULT 2,
    CentralizedLogger VARCHAR    (16) DEFAULT 'Log',
    DispatchPacketSize NUMBER (3)     DEFAULT 10,
    ImmediateDispatchLevel NUMBER (3)     DEFAULT 10,
    FlushPeriodSeconds NUMBER (3)     DEFAULT 10,
    MaxLogQueueSize NUMBER (10)     DEFAULT 1000,
    CONSTRAINT LogginCKey PRIMARY KEY (LoggingConfigId)
);
CREATE  TABLE NamedLoggerConfig (
    NamedLoggerConfigId NUMBER (10)     NOT NULL ,
    LoggingConfigId NUMBER (10)     NOT NULL ,
    Name VARCHAR    (64) NOT NULL ,
    MinLogLevel NUMBER (3)     DEFAULT 2,
    MinLogLevelLocal NUMBER (3)     DEFAULT 2,
    CONSTRAINT NamedLCKey PRIMARY KEY (NamedLoggerConfigId),
    CONSTRAINT NamedLoggerConfigLoggingConfig FOREIGN KEY (LoggingConfigId) REFERENCES LoggingConfig
);
CREATE  TABLE Manager (
    ManagerId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    LoggingConfigId NUMBER (10)     NOT NULL ,
    Startup VARCHAR    (256) NULL ,
    ServiceComponents VARCHAR    (256) NULL ,
    Timeout NUMBER (10)     DEFAULT 50,
    ClientPingInterval NUMBER (10)     DEFAULT 60,
    AdministratorPingInterval NUMBER (10)     DEFAULT 45,
    ContainerPingInterval NUMBER (10)     DEFAULT 30,
    ServerThreads NUMBER (3)     DEFAULT 10,
    DeadlockTimeout NUMBER (10)     DEFAULT 180,
    CONSTRAINT ManagerKey PRIMARY KEY (ManagerId),
    CONSTRAINT ManagerConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT ManagerLoggingConfig FOREIGN KEY (LoggingConfigId) REFERENCES LoggingConfig
);
CREATE  TABLE Container (
    ContainerId NUMBER (10)     NOT NULL ,
    ContainerName VARCHAR    (80) NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    LoggingConfigId NUMBER (10)     NOT NULL ,
    ComputerId NUMBER (3)     NULL ,
    ImplLang VARCHAR    (6) NOT NULL ,
    TypeModifiers VARCHAR    (64) NULL ,
    RealTime CHAR (1) DEFAULT    '0',
    RealTimeType VARCHAR    (4) DEFAULT 'NONE',
    KernelModuleLocation VARCHAR    (1024) NULL ,
    KernelModule VARCHAR    (1024) NULL ,
    AcsInstance NUMBER (3)     DEFAULT 0,
    CmdLineArgs VARCHAR    (256) NULL ,
    KeepAliveTime NUMBER (10)     DEFAULT -1,
    ServerThreads NUMBER (3)     DEFAULT 5,
    ManagerRetry NUMBER (10)     DEFAULT 10,
    CallTimeout NUMBER (10)     DEFAULT 240,
    Recovery CHAR (1) DEFAULT    '1',
    AutoloadSharedLibs VARCHAR    (64) NULL ,
    CONSTRAINT ContainerRealTime CHECK (RealTime IN ('0', '1')),
    CONSTRAINT ContainerRecovery CHECK (Recovery IN ('0', '1')),
    CONSTRAINT ContainerKey PRIMARY KEY (ContainerId),
    CONSTRAINT ContainerConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT ContainerLoggingConfig FOREIGN KEY (LoggingConfigId) REFERENCES LoggingConfig,
    CONSTRAINT ContainerImplLang CHECK (ImplLang IN ('java', 'cpp', 'py')),
    CONSTRAINT ContainerRealTimeType CHECK (RealTimeType IN ('NONE', 'ABM', 'CORR'))
);
CREATE  TABLE Component (
    ComponentId NUMBER (10)     NOT NULL ,
    ComponentName VARCHAR    (80) NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    ContainerId NUMBER (3)     NOT NULL ,
    ImplLang VARCHAR    (6) NOT NULL ,
    RealTime CHAR (1) NOT NULL     ,
    Code VARCHAR    (80) NOT NULL ,
    IDL VARCHAR    (80) NOT NULL ,
    Path VARCHAR    (80) NOT NULL ,
    IsAutostart CHAR (1) NOT NULL     ,
    IsDefault CHAR (1) NOT NULL     ,
    IsStandaloneDefined CHAR (1) NULL     ,
    KeepAliveTime NUMBER (10)     NOT NULL ,
    MinLogLevel NUMBER (3)     DEFAULT -1,
    MinLogLevelLocal NUMBER (3)     DEFAULT -1,
    CONSTRAINT ComponentRealTime CHECK (RealTime IN ('0', '1')),
    CONSTRAINT ComponentIsAutostart CHECK (IsAutostart IN ('0', '1')),
    CONSTRAINT ComponentIsDefault CHECK (IsDefault IN ('0', '1')),
    CONSTRAINT ComponentIsStanD CHECK (IsStandaloneDefined IN ('0', '1')),
    CONSTRAINT ComponentKey PRIMARY KEY (ComponentId),
    CONSTRAINT ComponentConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT ComponentImplLang CHECK (ImplLang IN ('java', 'cpp', 'py'))
);
CREATE  TABLE LRUType (
    LRUName VARCHAR    (32) NOT NULL ,
    FullName VARCHAR    (80) NOT NULL ,
    ICD VARCHAR    (80) NOT NULL ,
    ICDDate NUMBER (19)     NOT NULL ,
    Description VARCHAR    (1024) NOT NULL ,
    Notes VARCHAR    (1024) NULL ,
    CONSTRAINT LRUTypeKey PRIMARY KEY (LRUName)
);
CREATE  TABLE AssemblyType (
    AssemblyName VARCHAR    (32) NOT NULL ,
    LRUName VARCHAR    (32) NOT NULL ,
    FullName VARCHAR    (80) NOT NULL ,
    NodeAddress VARCHAR    (16) NOT NULL ,
    ChannelNumber NUMBER (3)     NOT NULL ,
    BaseAddress VARCHAR    (16) NULL ,
    Description VARCHAR    (1024) NOT NULL ,
    Notes VARCHAR    (1024) NULL ,
    ComponentId NUMBER (10)     NOT NULL ,
    CONSTRAINT AssemblyTypeKey PRIMARY KEY (AssemblyName),
    CONSTRAINT AssemblyTypeLRUName FOREIGN KEY (LRUName) REFERENCES LRUType,
    CONSTRAINT AssemblyTypeComponent FOREIGN KEY (ComponentId) REFERENCES Component
);
CREATE  TABLE PropertyType (
    PropertyTypeId NUMBER (5)     NOT NULL ,
    PropertyName VARCHAR    (128) NOT NULL ,
    AssemblyName VARCHAR    (32) NOT NULL ,
    DataType VARCHAR    (16) NOT NULL ,
    TableName VARCHAR    (32) NOT NULL ,
    RCA VARCHAR    (16) NOT NULL ,
    TeRelated CHAR (1) NOT NULL     ,
    RawDataType VARCHAR    (24) NOT NULL ,
    WorldDataType VARCHAR    (24) NOT NULL ,
    Units VARCHAR    (24) NULL ,
    Scale BINARY_DOUBLE     NULL ,
    Offset BINARY_DOUBLE     NULL ,
    MinRange BINARY_DOUBLE     NULL ,
    MaxRange BINARY_DOUBLE     NULL ,
    SamplingInterval BINARY_FLOAT     NOT NULL ,
    GraphMin BINARY_FLOAT     NULL ,
    GraphMax BINARY_FLOAT     NULL ,
    GraphFormat VARCHAR    (16) NULL ,
    GraphTitle VARCHAR    (128) NULL ,
    Description VARCHAR    (1024) NOT NULL ,
    CONSTRAINT PropertyTypeTeRelated CHECK (TeRelated IN ('0', '1')),
    CONSTRAINT PropertyTypeKey PRIMARY KEY (PropertyTypeId),
    CONSTRAINT PropertyTypeAssemblyName FOREIGN KEY (AssemblyName) REFERENCES AssemblyType,
    CONSTRAINT PropertyTypeDatatype CHECK (DataType IN ('float', 'double', 'boolean', 'string', 'integer', 'enum', 'blob')),
    CONSTRAINT PropertyTypeTableName CHECK (TableName IN ('FloatProperty', 'DoubleProperty', 'BooleanProperty', 'StringProperty', 'IntegerProperty', 'EnumProperty', 'BLOBProperty'))
);
CREATE  TABLE EnumWord (
    PropertyTypeId NUMBER (5)     NOT NULL ,
    OrderNumber NUMBER (3)     NOT NULL ,
    Word VARCHAR    (32) NOT NULL ,
    CONSTRAINT EnumWordKey PRIMARY KEY (PropertyTypeId, OrderNumber),
    CONSTRAINT EnumWordPropertyType FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType
);
CREATE  TABLE Assembly (
    AssemblyId NUMBER (10)     NOT NULL ,
    AssemblyName VARCHAR    (32) NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    SerialNumber VARCHAR    (80) NOT NULL ,
    CONSTRAINT AssemblyKey PRIMARY KEY (AssemblyId),
    CONSTRAINT AssemblyConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT AssemblyName FOREIGN KEY (AssemblyName) REFERENCES AssemblyType
);
CREATE  TABLE FloatProperty (
    AssemblyId NUMBER (10)     NOT NULL ,
    PropertyTypeId NUMBER (5)     NOT NULL ,
    SampleTime NUMBER (19)     NOT NULL ,
    Value BINARY_FLOAT     NOT NULL ,
    CONSTRAINT FloatPKey PRIMARY KEY (AssemblyId, PropertyTypeId, SampleTime),
    CONSTRAINT FloatPropertyAssemblyId FOREIGN KEY (AssemblyId) REFERENCES Assembly,
    CONSTRAINT FloatPropertyTypeId FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType
);
CREATE  TABLE StringProperty (
    AssemblyId NUMBER (10)     NOT NULL ,
    PropertyTypeId NUMBER (5)     NOT NULL ,
    SampleTime NUMBER (19)     NOT NULL ,
    Value VARCHAR    (24) NOT NULL ,
    CONSTRAINT StringPKey PRIMARY KEY (AssemblyId, PropertyTypeId, SampleTime),
    CONSTRAINT StringPropertyAssemblyId FOREIGN KEY (AssemblyId) REFERENCES Assembly,
    CONSTRAINT StringPropertyTypeId FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType
);
CREATE  TABLE BooleanProperty (
    AssemblyId NUMBER (10)     NOT NULL ,
    PropertyTypeId NUMBER (5)     NOT NULL ,
    SampleTime NUMBER (19)     NOT NULL ,
    Value CHAR (1) NOT NULL     ,
    CONSTRAINT BooleaPValue CHECK (Value IN ('0', '1')),
    CONSTRAINT BooleaPKey PRIMARY KEY (AssemblyId, PropertyTypeId, SampleTime),
    CONSTRAINT BooleanPropertyAssemblyId FOREIGN KEY (AssemblyId) REFERENCES Assembly,
    CONSTRAINT BooleanPropertyTypeId FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType
);
CREATE  TABLE DoubleProperty (
    AssemblyId NUMBER (10)     NOT NULL ,
    PropertyTypeId NUMBER (5)     NOT NULL ,
    SampleTime NUMBER (19)     NOT NULL ,
    Value BINARY_DOUBLE     NOT NULL ,
    CONSTRAINT DoublePKey PRIMARY KEY (AssemblyId, PropertyTypeId, SampleTime),
    CONSTRAINT DoublePropertyAssemblyId FOREIGN KEY (AssemblyId) REFERENCES Assembly,
    CONSTRAINT DoublePropertyTypeId FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType
);
CREATE  TABLE IntegerProperty (
    AssemblyId NUMBER (10)     NOT NULL ,
    PropertyTypeId NUMBER (5)     NOT NULL ,
    SampleTime NUMBER (19)     NOT NULL ,
    Value NUMBER (10)     NOT NULL ,
    CONSTRAINT IntegePKey PRIMARY KEY (AssemblyId, PropertyTypeId, SampleTime),
    CONSTRAINT IntegerPropertyAssemblyId FOREIGN KEY (AssemblyId) REFERENCES Assembly,
    CONSTRAINT IntegerPropertyTypeId FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType
);
CREATE  TABLE EnumProperty (
    AssemblyId NUMBER (10)     NOT NULL ,
    PropertyTypeId NUMBER (5)     NOT NULL ,
    SampleTime NUMBER (19)     NOT NULL ,
    Value NUMBER (3)     NOT NULL ,
    CONSTRAINT EnumPropertyKey PRIMARY KEY (AssemblyId, PropertyTypeId, SampleTime),
    CONSTRAINT EnumPropertyAssemblyId FOREIGN KEY (AssemblyId) REFERENCES Assembly,
    CONSTRAINT EnumPropertyTypeId FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType
);
CREATE  TABLE BlobProperty (
    AssemblyId NUMBER (10)     NOT NULL ,
    PropertyTypeId NUMBER (5)     NOT NULL ,
    SampleTime NUMBER (19)     NOT NULL ,
    DataType VARCHAR    (8) NOT NULL ,
    NumberItems NUMBER (5)     NOT NULL ,
    Value BLOB     NOT NULL ,
    CONSTRAINT BlobPropertyKey PRIMARY KEY (AssemblyId, PropertyTypeId, SampleTime),
    CONSTRAINT BlobPropertyAssemblyId FOREIGN KEY (AssemblyId) REFERENCES Assembly,
    CONSTRAINT BlobPropertyTypeId FOREIGN KEY (PropertyTypeId) REFERENCES PropertyType,
    CONSTRAINT BlobPropertyValue CHECK (DataType IN ('float', 'double', 'boolean', 'string', 'integer'))
);
CREATE  TABLE BaseElement (
    BaseElementId NUMBER (10)     NOT NULL ,
    BaseId NUMBER (10)     NOT NULL ,
    BaseType VARCHAR    (24) NOT NULL ,
    BaseElementName VARCHAR    (24) NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    CONSTRAINT BaseElementKey PRIMARY KEY (BaseElementId),
    CONSTRAINT BEConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT BEType CHECK (BaseType IN ('Antenna', 'Pad', 'CorrQuadrant', 'FrontEnd', 'WeatherStation', 'CentralRack', 'MasterClock', 'HolographyTower', 'Array'))
);
CREATE  TABLE BaseElementOnline (
    BaseElementOnlineId NUMBER (10)     NOT NULL ,
    BaseElementId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    StartTime NUMBER (19)     NOT NULL ,
    EndTime NUMBER (19)     NULL ,
    NormalTermination CHAR (1) NOT NULL     ,
    CONSTRAINT BaseElONormalT CHECK (NormalTermination IN ('0', '1')),
    CONSTRAINT BaseElOKey PRIMARY KEY (BaseElementOnlineId),
    CONSTRAINT BEOnlineId FOREIGN KEY (BaseElementId) REFERENCES BaseElement,
    CONSTRAINT BEOnlineConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
CREATE  TABLE Antenna (
    AntennaId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    AntennaName VARCHAR    (32) NOT NULL ,
    AntennaType VARCHAR    (4) NOT NULL ,
    DishDiameter BINARY_DOUBLE     NOT NULL ,
    CommissionDate NUMBER (19)     NOT NULL ,
    XPosition BINARY_DOUBLE     NOT NULL ,
    YPosition BINARY_DOUBLE     NOT NULL ,
    ZPosition BINARY_DOUBLE     NOT NULL ,
    XOffset BINARY_DOUBLE     NOT NULL ,
    YOffset BINARY_DOUBLE     NOT NULL ,
    ZOffset BINARY_DOUBLE     NOT NULL ,
    ComputerId NUMBER (5)     NOT NULL ,
    ContainerId NUMBER (10)     NOT NULL ,
    ComponentId NUMBER (10)     NOT NULL ,
    CONSTRAINT AntennaKey PRIMARY KEY (AntennaId),
    CONSTRAINT AntennaConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT AntennaComputer FOREIGN KEY (ComputerId) REFERENCES Computer,
    CONSTRAINT AntennaContainer FOREIGN KEY (ContainerId) REFERENCES Container,
    CONSTRAINT AntennaComponent FOREIGN KEY (ComponentId) REFERENCES Component,
    CONSTRAINT AntennaType CHECK (AntennaType IN ('VA', 'AEC', 'ACA'))
);
CREATE  TABLE Pad (
    PadId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    PadName VARCHAR    (32) NOT NULL ,
    CommissionDate NUMBER (19)     NOT NULL ,
    XPosition BINARY_DOUBLE     NOT NULL ,
    YPosition BINARY_DOUBLE     NOT NULL ,
    ZPosition BINARY_DOUBLE     NOT NULL ,
    CONSTRAINT PadKey PRIMARY KEY (PadId),
    CONSTRAINT PadConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
CREATE  TABLE CorrQuadrant (
    CorrQuadrantId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    CorrName VARCHAR    (32) NOT NULL ,
    Quadrant NUMBER (3)     NOT NULL ,
    NumberOfAntennas NUMBER (3)     NOT NULL ,
    CommissionDate NUMBER (19)     NOT NULL ,
    ComputerId NUMBER (5)     NOT NULL ,
    ContainerId NUMBER (10)     NOT NULL ,
    ComponentId NUMBER (10)     NOT NULL ,
    NumberOfRacks NUMBER (3)     NOT NULL ,
    NumberOfBins NUMBER (3)     NOT NULL ,
    CONSTRAINT CorrQuadrantKey PRIMARY KEY (CorrQuadrantId),
    CONSTRAINT CorrQuadConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT CorrQuadComputer FOREIGN KEY (ComputerId) REFERENCES Computer,
    CONSTRAINT CorrQuadContainer FOREIGN KEY (ContainerId) REFERENCES Container,
    CONSTRAINT CorrQuadComponent FOREIGN KEY (ComponentId) REFERENCES Component,
    CONSTRAINT CorrQuadNumber CHECK (Quadrant IN ('0', '1', '2', '3'))
);
CREATE  TABLE CorrQuadrantRack (
    CorrQuadrantRackId NUMBER (10)     NOT NULL ,
    CorrQuadrantId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    RackName VARCHAR    (32) NOT NULL ,
    NumberOfBins NUMBER (3)     NOT NULL ,
    CONSTRAINT CorrQuRKey PRIMARY KEY (CorrQuadrantRackId),
    CONSTRAINT CorrQuadRackConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
CREATE  TABLE CorrQuadrantBin (
    CorrQuadrantBinId NUMBER (10)     NOT NULL ,
    CorrQuadrantRackId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    BinName VARCHAR    (32) NOT NULL ,
    NumberOfCards NUMBER (5)     NOT NULL ,
    CONSTRAINT CorrQuBKey PRIMARY KEY (CorrQuadrantBinId),
    CONSTRAINT CorrQuadBinConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
CREATE  TABLE FrontEnd (
    FrontEndId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    FrontEndName VARCHAR    (32) NOT NULL ,
    CommissionDate NUMBER (19)     NOT NULL ,
    ComponentId NUMBER (10)     NOT NULL ,
    CONSTRAINT FrontEndKey PRIMARY KEY (FrontEndId),
    CONSTRAINT FrontEndConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT FrontEndComponent FOREIGN KEY (ComponentId) REFERENCES Component
);
CREATE  TABLE WeatherStation (
    WeatherStationId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    SerialNumber VARCHAR    (32) NOT NULL ,
    WeatherStationName VARCHAR    (32) NOT NULL ,
    WeatherStationType VARCHAR    (32) NOT NULL ,
    CommissionDate NUMBER (19)     NOT NULL ,
    XPosition BINARY_DOUBLE     NOT NULL ,
    YPosition BINARY_DOUBLE     NOT NULL ,
    ZPosition BINARY_DOUBLE     NOT NULL ,
    ComponentId NUMBER (10)     NOT NULL ,
    CONSTRAINT WeatheSKey PRIMARY KEY (WeatherStationId),
    CONSTRAINT WeatherStationComponent FOREIGN KEY (ComponentId) REFERENCES Component,
    CONSTRAINT WeatherStationConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
CREATE  TABLE CentralRack (
    CentralRackId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    CentralRackName VARCHAR    (32) NOT NULL ,
    CommissionDate NUMBER (19)     NOT NULL ,
    ComputerId NUMBER (5)     NOT NULL ,
    ContainerId NUMBER (10)     NOT NULL ,
    ComponentId NUMBER (10)     NOT NULL ,
    CONSTRAINT CentralRackKey PRIMARY KEY (CentralRackId),
    CONSTRAINT CentralRackComputer FOREIGN KEY (ComputerId) REFERENCES Computer,
    CONSTRAINT CentralRackContainer FOREIGN KEY (ContainerId) REFERENCES Container,
    CONSTRAINT CentralRackComponent FOREIGN KEY (ComponentId) REFERENCES Component,
    CONSTRAINT CentralRackConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
CREATE  TABLE MasterClock (
    MasterClockId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    MasterClockName VARCHAR    (32) NOT NULL ,
    CommissionDate NUMBER (19)     NOT NULL ,
    ComponentId NUMBER (10)     NOT NULL ,
    CONSTRAINT MasterClockKey PRIMARY KEY (MasterClockId),
    CONSTRAINT MasterClockComponent FOREIGN KEY (ComponentId) REFERENCES Component,
    CONSTRAINT MasterClockConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
CREATE  TABLE HolographyTower (
    HolographyTowerId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    HolographyTowerName VARCHAR    (32) NOT NULL ,
    CommissionDate NUMBER (19)     NOT NULL ,
    XPosition BINARY_DOUBLE     NOT NULL ,
    YPosition BINARY_DOUBLE     NOT NULL ,
    ZPosition BINARY_DOUBLE     NOT NULL ,
    CONSTRAINT HologrTKey PRIMARY KEY (HolographyTowerId),
    CONSTRAINT HolographyTowerConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
CREATE  TABLE Array (
    ArrayId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    ArrayName VARCHAR    (32) NOT NULL ,
    Type VARCHAR    (9) NOT NULL ,
    UserId VARCHAR    (80) NULL ,
    StartTime NUMBER (19)     NOT NULL ,
    EndTime NUMBER (19)     NULL ,
    NormalTermination CHAR (1) NOT NULL     ,
    ComponentId NUMBER (10)     NOT NULL ,
    CONSTRAINT ArrayNormalT CHECK (NormalTermination IN ('0', '1')),
    CONSTRAINT ArrayKey PRIMARY KEY (ArrayId),
    CONSTRAINT ArrayConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT ArrayComponent FOREIGN KEY (ComponentId) REFERENCES Component,
    CONSTRAINT ArrayType CHECK (Type IN ('automatic', 'manual'))
);
CREATE  TABLE AntennaToPad (
    AntennaId NUMBER (10)     NOT NULL ,
    PadId NUMBER (10)     NOT NULL ,
    StartTime NUMBER (19)     NOT NULL ,
    EndTime NUMBER (19)     NULL ,
    Planned CHAR (1) NOT NULL     ,
    CONSTRAINT AntennaToPadPlanned CHECK (Planned IN ('0', '1')),
    CONSTRAINT AntennaToPadKey PRIMARY KEY (AntennaId, PadId, StartTime),
    CONSTRAINT AntennaToPadAntennaId FOREIGN KEY (AntennaId) REFERENCES Antenna,
    CONSTRAINT AntennaToPadPadId FOREIGN KEY (PadId) REFERENCES Pad
);
CREATE  TABLE AntennaPointingModel (
    PointingModelId NUMBER (10)     NOT NULL ,
    AntennaId NUMBER (10)     NOT NULL ,
    PadId NUMBER (10)     NOT NULL ,
    StartValidTime NUMBER (19)     NOT NULL ,
    EndValidTime NUMBER (19)     NULL ,
    AsdmUID VARCHAR    (80) NOT NULL ,
    CONSTRAINT AntennPMKey PRIMARY KEY (PointingModelId),
    CONSTRAINT AntennaPMAntennaId FOREIGN KEY (AntennaId) REFERENCES Antenna,
    CONSTRAINT AntennaPMPadId FOREIGN KEY (PadId) REFERENCES Pad
);
CREATE  TABLE AntennaPointingModelTerm (
    PointingModelId NUMBER (10)     NOT NULL ,
    CoeffName VARCHAR    (32) NOT NULL ,
    CoeffValue BINARY_FLOAT     NOT NULL ,
    CoeffError BINARY_FLOAT     NOT NULL ,
    CONSTRAINT AntennPMTKey PRIMARY KEY (PointingModelId, CoeffName),
    CONSTRAINT AntPMTermPointingModelId FOREIGN KEY (PointingModelId) REFERENCES AntennaPointingModel
);
CREATE  TABLE AntennaDelayTerms (
    AntennaId NUMBER (10)     NOT NULL ,
    PadId NUMBER (10)     NOT NULL ,
    TimeOfMeasurement NUMBER (19)     NOT NULL ,
    ParameterName VARCHAR    (32) NOT NULL ,
    ParameterValue BINARY_DOUBLE     NOT NULL ,
    CONSTRAINT AntennDTKey PRIMARY KEY (AntennaId, PadId, TimeOfMeasurement),
    CONSTRAINT AntennaDTAntennaId FOREIGN KEY (AntennaId) REFERENCES Antenna,
    CONSTRAINT AntennaDTPadId FOREIGN KEY (PadId) REFERENCES Pad
);
CREATE  TABLE AntennaToFrontEnd (
    AntennaId NUMBER (10)     NOT NULL ,
    FrontEndId NUMBER (10)     NOT NULL ,
    StartTime NUMBER (19)     NOT NULL ,
    EndTime NUMBER (19)     NULL ,
    CONSTRAINT AntennTFEKey PRIMARY KEY (AntennaId, FrontEndId, StartTime),
    CONSTRAINT AntennaToFEAntennaId FOREIGN KEY (AntennaId) REFERENCES Antenna,
    CONSTRAINT AntennaToFEFrontEndId FOREIGN KEY (FrontEndId) REFERENCES FrontEnd
);
CREATE  TABLE AntennaToArray (
    AntennaId NUMBER (10)     NOT NULL ,
    ArrayId NUMBER (10)     NOT NULL ,
    CONSTRAINT AntennTAKey PRIMARY KEY (AntennaId, ArrayId),
    CONSTRAINT AntennaToArrayAntennaId FOREIGN KEY (AntennaId) REFERENCES Antenna,
    CONSTRAINT AntennaToArrayArrayid FOREIGN KEY (ArrayId) REFERENCES Array
);
CREATE  TABLE SBExecution (
    ArrayId NUMBER (10)     NOT NULL ,
    SbUID VARCHAR    (80) NOT NULL ,
    StartTime NUMBER (19)     NOT NULL ,
    EndTime NUMBER (19)     NULL ,
    NormalTermination CHAR (1) NOT NULL     ,
    CONSTRAINT SBExecutionNormalT CHECK (NormalTermination IN ('0', '1')),
    CONSTRAINT SBExecutionKey PRIMARY KEY (ArrayId, SbUID, StartTime),
    CONSTRAINT SBExecutionArrayId FOREIGN KEY (ArrayId) REFERENCES Array
);
CREATE  TABLE AntennaToCorr (
    AntennaId NUMBER (10)     NOT NULL ,
    CorrId NUMBER (10)     NOT NULL ,
    AntennaNumber NUMBER (3)     NOT NULL ,
    StartTime NUMBER (19)     NOT NULL ,
    EndTime NUMBER (19)     NULL ,
    CONSTRAINT AntennTCKey PRIMARY KEY (AntennaId, CorrId, StartTime),
    CONSTRAINT AntToCorrAntennaId FOREIGN KEY (AntennaId) REFERENCES Antenna,
    CONSTRAINT AntToCorrId FOREIGN KEY (CorrId) REFERENCES CorrQuadrant
);
CREATE  TABLE BaseElementAssemblyGroup (
    GroupName VARCHAR    (32) NOT NULL ,
    BaseElementId NUMBER (10)     NOT NULL ,
    CONSTRAINT BaseElAGKey PRIMARY KEY (GroupName, BaseElementId),
    CONSTRAINT BEAssemblyGroupId FOREIGN KEY (BaseElementId) REFERENCES BaseElement
);
CREATE  TABLE BaseElementAssemblyList (
    AssemblyId NUMBER (10)     NOT NULL ,
    BaseElementId NUMBER (10)     NOT NULL ,
    GroupName VARCHAR    (32) NULL ,
    OrderTag NUMBER (5)     NULL ,
    RoleName VARCHAR    (32) NULL ,
    ChannelNumber NUMBER (3)     NULL ,
    NodeAddress VARCHAR    (16) NULL ,
    BaseAddress VARCHAR    (16) NULL ,
    CONSTRAINT BaseElALKey PRIMARY KEY (AssemblyId, BaseElementId),
    CONSTRAINT BEAssemblyListId FOREIGN KEY (BaseElementId) REFERENCES BaseElement,
    CONSTRAINT BEAssemblyListAssemblyId FOREIGN KEY (AssemblyId) REFERENCES Assembly
);
CREATE  TABLE MasterComponent (
    MasterComponentId NUMBER (5)     NOT NULL ,
    MasterComponentName VARCHAR    (80) NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    SubsystemName VARCHAR    (80) NOT NULL ,
    Code VARCHAR    (80) NOT NULL ,
    IDL VARCHAR    (80) NOT NULL ,
    CONSTRAINT MasterCKey PRIMARY KEY (MasterComponentId),
    CONSTRAINT MComponentConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
CREATE  TABLE ComputerExecution (
    ComputerExecutionId NUMBER (10)     NOT NULL ,
    ComputerId NUMBER (5)     NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    StartTime NUMBER (19)     NOT NULL ,
    EndTime NUMBER (19)     NULL ,
    NormalTermination CHAR (1) NOT NULL     ,
    CONSTRAINT ComputENormalT CHECK (NormalTermination IN ('0', '1')),
    CONSTRAINT ComputEKey PRIMARY KEY (ComputerExecutionId),
    CONSTRAINT ComputerExecConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT ComputerExecComputer FOREIGN KEY (ComputerId) REFERENCES Computer
);
CREATE  TABLE ContainerExecution (
    ContainerExecutionId NUMBER (10)     NOT NULL ,
    ContainerId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    ComputerId NUMBER (5)     NOT NULL ,
    StartTime NUMBER (19)     NOT NULL ,
    EndTime NUMBER (19)     NULL ,
    NormalTermination CHAR (1) NOT NULL     ,
    CONSTRAINT ContaiENormalT CHECK (NormalTermination IN ('0', '1')),
    CONSTRAINT ContaiEKey PRIMARY KEY (ContainerExecutionId),
    CONSTRAINT ContainerExecConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT ContainerExecContainer FOREIGN KEY (ContainerId) REFERENCES Container,
    CONSTRAINT ContainerExecComputer FOREIGN KEY (ComputerId) REFERENCES Computer
);
CREATE  TABLE ComponentExecution (
    ComponentExecutionId NUMBER (10)     NOT NULL ,
    ComponentId NUMBER (10)     NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    ContainerId NUMBER (10)     NOT NULL ,
    StartTime NUMBER (19)     NOT NULL ,
    EndTime NUMBER (19)     NULL ,
    NormalTermination CHAR (1) NOT NULL     ,
    BaseElementOnlineId NUMBER (10)     NULL ,
    AssemblyId NUMBER (10)     NULL ,
    CONSTRAINT ComponENormalT CHECK (NormalTermination IN ('0', '1')),
    CONSTRAINT ComponEKey PRIMARY KEY (ComponentExecutionId),
    CONSTRAINT ComponentExecConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT ComponentExecComponent FOREIGN KEY (ComponentId) REFERENCES Component,
    CONSTRAINT ComponentExecContainer FOREIGN KEY (ContainerId) REFERENCES Container,
    CONSTRAINT ComponentExecBaseElement FOREIGN KEY (BaseElementOnlineId) REFERENCES BaseElementOnline,
    CONSTRAINT ComponentExecAssembly FOREIGN KEY (AssemblyId) REFERENCES Assembly
);
CREATE  TABLE MasterComponentExecution (
    MasterComponentExecId NUMBER (10)     NOT NULL ,
    MasterComponentId NUMBER (5)     NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    ContainerId NUMBER (10)     NOT NULL ,
    StartTime NUMBER (19)     NOT NULL ,
    EndTime NUMBER (19)     NULL ,
    NormalTermination CHAR (1) NOT NULL     ,
    CONSTRAINT MasterCENormalT CHECK (NormalTermination IN ('0', '1')),
    CONSTRAINT MasterCEKey PRIMARY KEY (MasterComponentExecId),
    CONSTRAINT MComponentExecConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT MComponentExecComponent FOREIGN KEY (MasterComponentId) REFERENCES MasterComponent,
    CONSTRAINT MComponentExecContainer FOREIGN KEY (ContainerId) REFERENCES Container
);
CREATE  TABLE ACS (
    ACSId NUMBER (3)     NOT NULL ,
    ACSVersion VARCHAR    (80) NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    ComputerId NUMBER (5)     NOT NULL ,
    Var1 VARCHAR    (80) NULL ,
    Var2 VARCHAR    (80) NULL ,
    Var3 VARCHAR    (80) NULL ,
    CONSTRAINT ACSKey PRIMARY KEY (ACSId),
    CONSTRAINT ACSComputer FOREIGN KEY (ComputerId) REFERENCES Computer,
    CONSTRAINT ACSConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
CREATE  TABLE ACSExecution (
    ACSId NUMBER (3)     NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    StartTime NUMBER (19)     NOT NULL ,
    EndTime NUMBER (19)     NULL ,
    NormalTermination CHAR (1) NOT NULL     ,
    CONSTRAINT ACSExecutionNormalT CHECK (NormalTermination IN ('0', '1')),
    CONSTRAINT ACSExecutionKey PRIMARY KEY (ACSId, ConfigurationId, StartTime),
    CONSTRAINT ACSExecACS FOREIGN KEY (ACSId) REFERENCES ACS,
    CONSTRAINT ACSExecConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
CREATE  TABLE NotificationChannel (
    NCId NUMBER (3)     NOT NULL ,
    NCName VARCHAR    (80) NOT NULL ,
    SubsystemName VARCHAR    (80) NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    QOS1 VARCHAR    (80) NULL ,
    QOS2 VARCHAR    (80) NULL ,
    QOS3 VARCHAR    (80) NULL ,
    CONSTRAINT NotifiCKey PRIMARY KEY (NCId),
    CONSTRAINT NCConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
CREATE  TABLE NCExecution (
    NCId NUMBER (3)     NOT NULL ,
    ACSId NUMBER (3)     NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    StartTime NUMBER (19)     NOT NULL ,
    EndTime NUMBER (19)     NULL ,
    NormalTermination CHAR (1) NOT NULL     ,
    CONSTRAINT NCExecutionNormalT CHECK (NormalTermination IN ('0', '1')),
    CONSTRAINT NCExecutionKey PRIMARY KEY (NCId, ConfigurationId, StartTime),
    CONSTRAINT NCExecNC FOREIGN KEY (NCId) REFERENCES NotificationChannel,
    CONSTRAINT NCExecConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration,
    CONSTRAINT NCExecACS FOREIGN KEY (ACSId) REFERENCES ACS
);
CREATE  TABLE SystemExecution (
    ConfigurationId NUMBER (3)     NOT NULL ,
    StartTime NUMBER (19)     NOT NULL ,
    EndTime NUMBER (19)     NULL ,
    NormalTermination CHAR (1) NOT NULL     ,
    CONSTRAINT SystemENormalT CHECK (NormalTermination IN ('0', '1')),
    CONSTRAINT SystemEKey PRIMARY KEY (ConfigurationId, StartTime),
    CONSTRAINT SystemExecConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
CREATE  TABLE Startup (
    StartupId NUMBER (5)     NOT NULL ,
    ConfigurationId NUMBER (3)     NOT NULL ,
    StartupName VARCHAR    (80) NOT NULL ,
    CONSTRAINT StartupKey PRIMARY KEY (StartupId),
    CONSTRAINT StartupConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
CREATE  TABLE DeploymentStartup (
    StartupId NUMBER (5)     NOT NULL ,
    OrderNumber NUMBER (5)     NOT NULL ,
    DependsOn NUMBER (5)     NOT NULL ,
    AllWait CHAR (1) NOT NULL     ,
    StartupType VARCHAR    (10) NOT NULL ,
    NameId NUMBER (5)     NOT NULL ,
    AssociatedType VARCHAR    (10) NULL ,
    AssociatedId NUMBER (5)     NULL ,
    CONSTRAINT DeploySAllWait CHECK (AllWait IN ('0', '1')),
    CONSTRAINT DeploySKey PRIMARY KEY (StartupId, OrderNumber),
    CONSTRAINT DeployStartupId FOREIGN KEY (StartupId) REFERENCES Startup,
    CONSTRAINT DeployStartupType CHECK (StartupType IN ('ACS', 'computer', 'container', 'component')),
    CONSTRAINT DeployAssociatedType CHECK (AssociatedType IN ('computer', 'container'))
);
CREATE  TABLE BaseElementStartup (
    BaseElementId NUMBER (10)     NOT NULL ,
    StartupId NUMBER (5)     NOT NULL ,
    CONSTRAINT BaseElSKey PRIMARY KEY (BaseElementId, StartupId),
    CONSTRAINT BEStartupId FOREIGN KEY (StartupId) REFERENCES Startup,
    CONSTRAINT BEStartupIdBE FOREIGN KEY (BaseElementId) REFERENCES BaseElement
);
CREATE  TABLE AssociatedBaseElement (
    BaseElementId NUMBER (10)     NOT NULL ,
    StartupId NUMBER (5)     NOT NULL ,
    AssociatedId NUMBER (10)     NOT NULL ,
    AssociationType VARCHAR    (24) NOT NULL ,
    CONSTRAINT AssociBEKey PRIMARY KEY (BaseElementId, StartupId, AssociatedId),
    CONSTRAINT ABEStartupIdBE FOREIGN KEY (BaseElementId) REFERENCES BaseElement,
    CONSTRAINT ABEStartupId FOREIGN KEY (StartupId) REFERENCES Startup,
    CONSTRAINT ABEAssociated FOREIGN KEY (AssociatedId) REFERENCES BaseElement,
    CONSTRAINT ABEAssociationType CHECK (AssociationType IN ('AntennaToPad', 'AntennaToFrontEnd', 'AntennaToCorr'))
);
CREATE  TABLE AssemblyGroupStartup (
    GroupName VARCHAR    (32) NOT NULL ,
    BaseElementId NUMBER (10)     NOT NULL ,
    StartupId NUMBER (5)     NOT NULL ,
    CONSTRAINT AssembGSKey PRIMARY KEY (GroupName, BaseElementId, StartupId),
    CONSTRAINT AGStartupId FOREIGN KEY (StartupId) REFERENCES Startup,
    CONSTRAINT AGStartupIdBE FOREIGN KEY (BaseElementId) REFERENCES BaseElement
);
CREATE  TABLE AssemblyStartup (
    AssemblyId NUMBER (10)     NOT NULL ,
    BaseElementId NUMBER (10)     NOT NULL ,
    StartupId NUMBER (5)     NOT NULL ,
    ContainerId NUMBER (10)     NOT NULL ,
    ComponentId NUMBER (10)     NOT NULL ,
    GroupName VARCHAR    (32) NULL ,
    OrderTag NUMBER (5)     NULL ,
    RoleName VARCHAR    (32) NULL ,
    ChannelNumber NUMBER (3)     NULL ,
    NodeAddress VARCHAR    (16) NULL ,
    BaseAddress VARCHAR    (16) NULL ,
    CONSTRAINT AssembSKey PRIMARY KEY (AssemblyId, BaseElementId, StartupId),
    CONSTRAINT AssemblyStartupId FOREIGN KEY (StartupId) REFERENCES Startup,
    CONSTRAINT AssemblyStartupIdA FOREIGN KEY (AssemblyId) REFERENCES Assembly,
    CONSTRAINT AssemblyStartupIdBE FOREIGN KEY (BaseElementId) REFERENCES BaseElement,
    CONSTRAINT AssemblyStartupContainer FOREIGN KEY (ContainerId) REFERENCES Container,
    CONSTRAINT AssemblyStartupComponent FOREIGN KEY (ComponentId) REFERENCES Component
);
CREATE  TABLE SystemCounters (
    ConfigurationId NUMBER (3)     NOT NULL ,
    UpdateTime NUMBER (19)     NOT NULL ,
    AutoArrayCount NUMBER (5)     NOT NULL ,
    ManArrayCount NUMBER (5)     NOT NULL ,
    DataCaptureCount NUMBER (5)     NOT NULL ,
    CONSTRAINT SystemCKey PRIMARY KEY (ConfigurationId),
    CONSTRAINT SystemCountersConfig FOREIGN KEY (ConfigurationId) REFERENCES Configuration
);
