SET SERVEROUTPUT ON;

DECLARE
    lruid NUMBER;
    assid NUMBER;
    asstid NUMBER;
    propid NUMBER;

BEGIN

SELECT COUNT(lrutypeid) INTO lruid FROM lrutype;
SELECT COUNT(assemblyId) INTO assid FROM assembly;
SELECT COUNT(assemblytypeid) INTO asstid FROM assemblytype;
SELECT COUNT(propertytypeid) INTO propid FROM propertytype;

lruid := lruid + 1;
INSERT INTO LRUType VALUES (
    lruid,
    'PSD', 
    'Power Supply Digital', 
    'ALMA-57.03.00.00-70.35.30.00-A-ICD', 
    '06-AUG-18',
    'Power supply for digital racks.  The PSDigital AC-DC Power Supply uses two internal 24V modules connected in series to produce 48V. Both of the 24V modules must be monitored and controlled by communicating with each one individually or by issuing group commands.', 
    ''
);

asstid := asstid + 1;
INSERT INTO AssemblyType VALUES (
    asstid, 
    lruid,
    'PSD', 
    'Power Supply Digital', 
    '0x3e', 
    '0', 
    'Power supply for digital racks.  The PSDigital AC-DC Power Supply uses two internal 24V modules connected in series to produce 48V. Both of the 24V modules must be monitored and controlled by communicating with each one individually or by issuing group commands.', 
    ''
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    asstid,
    'integer',
    'IntegerProperty',
    'ERROR_AND_ALARMS', 
    '0x00001', 
    'Y',   
    'ubyte',
    'long',
    'na',
    '0',   
    '0',   
    '0',   
    '1',   
    '1',   
    '0',   
    '0',   
    'na', 
    'Error and Alarms', 
    'Error Flags. The bits in this monitor point summarize the overall condition of the power supply.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    asstid,
    'float',
    'FloatProperty',
    'VOLTAGE_24V_A', 
    '0x00009', 
    'Y',   
    'uint16',
    'float',
    'Volts',
    '3.3235841531499999E-2',   
    '0',   
    '21',   
    '28',   
    '60',   
    '16',   
    '18',   
    '8.3f', 
    'Measured voltage on the +24vA AC-DC converter.', 
    'Measured voltage on the +24vA AC-DC converter.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    asstid,
    'float',
    'FloatProperty',
    'CURRENT_24V_A', 
    '0x0000a', 
    'Y',   
    'uint16',
    'float',
    'Amps',
    '2.172260236776E-2',   
    '0',   
    '0',   
    '15',   
    '60',   
    '0',   
    '16',   
    '8.3f', 
    'Measured current on the +24vA AC-DC converter.', 
    'Measured current on the +24vA AC-DC converter.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    asstid,
    'float',
    'FloatProperty',
    'VOLTAGE_24V_B', 
    '0x0000d', 
    'Y',   
    'uint16',
    'float',
    'Volts',
    '3.3235841531499999E-2',   
    '0',   
    '21',   
    '28',   
    '60',   
    '16',   
    '18',   
    '8.3f', 
    'Measured voltage on the +24vB AC-DC converter.', 
    'Measured voltage on the +24vB AC-DC converter.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    asstid,
    'float',
    'FloatProperty',
    'CURRENT_24V_B', 
    '0x0000e', 
    'Y',   
    'uint16',
    'float',
    'Amps',
    '2.172260236776E-2',   
    '0',   
    '0',   
    '15',   
    '60',   
    '0',   
    '16',   
    '8.3f', 
    'Measured current on the +24vB AC-DC converter.', 
    'Measured current on the +24vB AC-DC converter.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    asstid,
    'float',
    'FloatProperty',
    '(MAX_MIN_24V_A)', 
    '0x00022', 
    'Y',   
    'uint16',
    'float',
    'na',
    '0',   
    '0',   
    '0',   
    '0',   
    '60',   
    '0',   
    '0',   
    'na', 
    'na', 
    'Max and Min voltage and Max current on the +24V_A AC-DC converter.'
);

propid := propid + 1;
INSERT INTO PropertyType VALUES (
    propid,
    asstid,
    'float',
    'FloatProperty',
    '(MAX_MIN_24V_B)', 
    '0x00024', 
    'Y',   
    'uint16',
    'float',
    'na',
    '0',   
    '0',   
    '0',   
    '0',   
    '60',   
    '0',   
    '0',   
    'na', 
    'na', 
    'Max and Min voltage and Max current on the +24V_B AC-DC converter.'
);

COMMIT;
END;
/
.
