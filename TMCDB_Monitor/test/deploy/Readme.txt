Outline of special test

The sql files should be executed in the follwing order.

1. DropAllTables.sql
2. CreateOracleTables.sql
3. Configure.sql
4. DGCKAdd.sql
5. IFProcAdd.sql
6. FLOOGAdd.sql
7. LO2Add.sql
8. AddAssemblies.sql

The file DropAllTables.sql destroys all existing tables.

The file CreateOracleTables.sql creates all tables in the TMCDB.

The file Configure.sql creates records in the following tables:
	Configuration: 1 record
	Computer: 1 record 
	Container: 1 C++ container
	Component: 1 component
However the only important one is the Configuration record.

The files DGCKAdd.sql, IFProcAdd.sql, etc. adds files describing
the DigiClock and IFProcessors.  

The file AddAssemblies.sql adds specific entries for 16 assemblies.
This file is very sensitive to names and must be edited for 
different configurations.

