/*
 * ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

/** 
 * @author  srankin
 * @version $Id$
 * @since    
 */

package alma.TMCDB.Query;

import alma.hla.runtime.asdm.types.ArrayTime;
import alma.TMCDB.TimeValue;
import alma.TmcdbErrType.wrappers.AcsJTmcdbErrTypeEx;
import java.util.ArrayList;
import java.util.List;
import junit.framework.TestCase;
import junit.textui.TestRunner;
import junitx.framework.ArrayAssert;
import junitx.framework.ListAssert;


/**
 * Tests for CommonQueries against a copy of the TMCDB from the ATF.
 */
public class TMCDBCommonQueriesTestsWithATFTestDB extends TestCase {

    String msgPrefix = "Actual and expected do not match in ";
    String msgSuffix = " with ATF test data.";

    public TMCDBCommonQueriesTestsWithATFTestDB() {
        super();
    }


    // 
    // Public method tests   
    public void testGetAntennaNames() {
        List<String> expected = new ArrayList<String>();
        expected.add("DV01");
        expected.add("DA41");
        List<String> actual = tmcdb.getAntennaNames();
        String failureMessage = msgPrefix + "testGetAntennaNames()" + msgSuffix;
        ListAssert.assertEquals(failureMessage, expected, actual);
    }

    public void testGetAssemblyIDforSerialNumber() {
        int expected = 1;
        int actual = tmcdb.getAssemblyIDforSN("CONTROL/DV01/IFProc0");
        String failureMessage = msgPrefix + "testGetAssemblyIDforSN('CONTROL/DV01/IFProc0')" + msgSuffix;
        assertEquals(failureMessage, expected, actual);    	
    }

    public void testGetAssemblyIds() {
        List<Integer> expected = new ArrayList<Integer>();
        expected.add(1);
        expected.add(2);
        expected.add(3);
        expected.add(4);
        List<Integer> actual = tmcdb.getAssemblyIds();
        String failureMessage = msgPrefix + "testGetAssemblyIds()" + msgSuffix;
        ListAssert.assertEquals(failureMessage, expected, actual);
    }

    public void testGetAssemblyNamesFromAntennaName() {
        // TODO: update for ATF data.
        List<String> expected = new ArrayList<String>();
        List<String> actual = tmcdb.getAssemblyNames("TBD");
        String failureMessage = msgPrefix + "testGetAssemblyNames()" + msgSuffix;
        ListAssert.assertEquals(failureMessage, expected, actual);
    }

    public void testGetPropertyNamesFromAssemblyName() {
        // TODO: update for ATF data.
        List<String> expected = new ArrayList<String>();
        List<String> actual = tmcdb.getPropertyNames("IFProc");
        String failureMessage = msgPrefix + "testGetPropertyNames()" + msgSuffix;
        ListAssert.assertEquals(failureMessage, expected, actual);        
    }
    
    public void testGetPropertyDataTypeFromPropertyName() {
        // TODO: update for ATF data.
        String expected = "";
        String actual = tmcdb.getPropertyDataType("TBD");
        String failureMessage = msgPrefix + "testGetPropertyDataType()" + msgSuffix;
        assertEquals(failureMessage, expected, actual);        
    }

//    public void testGetPropertyIdFromPropertyName() {
//        // TODO: update for ATF data.
//    	int expected = 0;
//        int actual = tmcdb.getPropertyId("TBD");
//        String failureMessage = msgPrefix + "testGetPropertyId()" + msgSuffix;
//        assertEquals(failureMessage, expected, actual);        
//    }

    public void testGetMonitorData() {
        // TODO: update for ATF data.
        TimeValue[] expected = new TimeValue[0];
        TimeValue[] actual = tmcdb.getMonitorData(
            "DV01",
            "IFProc0",
            "property name 1",
            new ArrayTime(2006,1,1,0,0,0.0),
            new ArrayTime(2007,12,31,23,59,59.99)
            );
        String failureMessage = msgPrefix + "testGetMonitorData()" + msgSuffix;
        ArrayAssert.assertEquals(failureMessage, expected, actual);        
    }


    //
    // Non-public method tests
    public void testGetAssemblySNs() {
        List<String> expected = new ArrayList<String>();
        expected.add("CONTROL/DV01/IFProc0");
        expected.add("CONTROL/DV01/IFProc1");
        expected.add("CONTROL/DA41/IFProc0");
        expected.add("CONTROL/DA41/IFProc1");
        List<String> actual = tmcdb.getAssemblySNs();
        String failureMessage = msgPrefix + "testGetAssemblySNs()" + msgSuffix;
        ListAssert.assertEquals(failureMessage, expected, actual);
    }

    public void testGetBaseElementIdsFromAntennaName() {
        // TODO: update for ATF data.
        List<Integer> expected = new ArrayList<Integer>();
        List<Integer> actual = tmcdb.getBaseElementIds("TBD");
        String failureMessage = msgPrefix + "testGetBaseElementIdsFromAntennaName()" + msgSuffix;
        ListAssert.assertEquals(failureMessage, expected, actual);
    }

    public void testGetAssemblyIdsFromBaseElementId() {
        // TODO: update for ATF data.
        List<Integer> expected = new ArrayList<Integer>();
        List<Integer> actual = tmcdb.getAssemblyIds(1);
        String failureMessage = msgPrefix + "testGetAssemblyIdsFromBaseElementId()" + msgSuffix;
        ListAssert.assertEquals(failureMessage, expected, actual);
    }

    public void testGetAssemblyIdsFromAntennaName() {
    	// TODO: update for ATF data.
        List<Integer> expected = new ArrayList<Integer>();
        expected.add(1);
        expected.add(2);
        List<Integer> actual = tmcdb.getAssemblyIdsInAntenna("DV01");
        String failureMessage = msgPrefix + "testGetAssemblyIdsFromAntennaName()" + msgSuffix;
        ListAssert.assertEquals(failureMessage, expected, actual);   	
    }
    
    public void testGetAssemblyIdFromAssemblyNameAndAntennaName() {
    	int expected = 1;
    	int actual = tmcdb.getAssemblyId("IFProc0", "DV01");
        String failureMessage = msgPrefix + "testGetAssemblyIdFromAssemblyNameAndAntennaName()" + msgSuffix;
        assertEquals(failureMessage, expected, actual);    	
    }

    public void testGetAssemblyNameFromAssemblyId() {
        // TODO: update for ATF data.
        String expected = "IFProc";
        String actual = tmcdb.getAssemblyName(1);
        String failureMessage = msgPrefix + "testGetAssemblyNameFromAssemblyId()" + msgSuffix;
        assertEquals(failureMessage, expected, actual);
    }

    public void testGetAssemblyIdsWithName() {
        // TODO: update for ATF data.
        List<Integer> expected = new ArrayList<Integer>();
        expected.add(1);
        expected.add(3);
        List<Integer> actual = tmcdb.getAssemblyIdsWithName("IFProc0");
        String failureMessage = msgPrefix + "testGetAssemblyIdsWithName()" + msgSuffix;
        ListAssert.assertEquals(failureMessage, expected, actual);   	    	
    }

    
    /**
     * Implementation details users of this class don't care about.
     */
    
	private TMCDBCommonQueries tmcdb;

    protected void setUp()
    {
    	// Note: dbConfig.properties must be copied from TMCDB_Monitor/test/dbConfig.properties to
    	// TMCDB_Monitor/dbConfig.properties to run these tests in Eclipse.

    	// Each test must run against a clean copy of a database with only required data loaded.
        String dropTables = "SQL/dropAllTables.sql";
        String loadTables = "SQL/createHsqldbTables.sql";
        String loadTestData = "SQL/loadHsqldbATFTestDB.sql";
        
        tmcdb = new TMCDBCommonQueries();
        tmcdb.connectToDB();
        try {
        tmcdb.loadAndExecuteScript(dropTables);
        tmcdb.loadAndExecuteScript(loadTables);
        tmcdb.loadAndExecuteScript(loadTestData);
        } catch (Exception e) {
            // TODO: better error handling and reporting.
            e.printStackTrace();
        }
        try {
            tmcdb.initialize("TMCDB-Common-Queries-Test");
        } catch (AcsJTmcdbErrTypeEx e) {
            // TODO: better error handling and reporting.
            e.printStackTrace();           
        }
    }
    
    protected void tearDown() {
        tmcdb.terminate();
    }
    
	/**
	 * main used to support testing with TAT or other command line tools.
	 * 
	 * @param args - not used.
	 */
	public static void main(String[] args) {
		TestRunner.run(TMCDBCommonQueriesTestsWithATFTestDB.class);
	}

} // class

//
//O_o
