/*
 * ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

/** 
 * @author  srankin
 * @version $Id$
 * @since    
 */

package alma.TMCDB.Query;

import alma.hla.runtime.asdm.types.ArrayTime;
import alma.TMCDB.TimeValue;
import alma.TmcdbErrType.wrappers.AcsJTmcdbErrTypeEx;
import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;
import junit.textui.TestRunner;
import junitx.framework.ArrayAssert;
import junitx.framework.ListAssert;


/** 
 * Tests for TMCDBCommonQueries against a database with empty tables.  In particular, 
 * this tests TMCDBCommonQueries handling of the no results boundary condition.
 */
public class TMCDBCommonQueriesTestsWithEmptyDB extends TestCase {
    
    String msgPrefix = "Actual and expected do not match in ";
    String msgSuffix = " with Empty database tables.";

    public TMCDBCommonQueriesTestsWithEmptyDB() {
        super();
    }


    // 
    // Public method tests
    public void testGetAntennaNames() {
        List<String> expected = new ArrayList<String>();
        List<String> actual = tmcdb.getAntennaNames();
        String failureMessage = msgPrefix + "testGetAntennaNames()" + msgSuffix;
        ListAssert.assertEquals(failureMessage, expected, actual);
    }
    
    public void testGetAntennaNamesThrowsExceptionAfterTMCDBTerminated() {
    	tmcdb.terminate();
    	try {
    		List<String> actual = tmcdb.getAntennaNames();
    		fail("Call to getAntennaNames() should have thrown an exception, but did not.");
    	} catch (IllegalStateException ex) {
    		// Do nothing.
    	}    	
    }

    
    public void testGetAssemblyIDforSN() {
        int expected = 0;
        int actual = tmcdb.getAssemblyIDforSN("BogusSN");
        String failureMessage = msgPrefix + "testGetAssemblyIDforSN('BogusSN')" + msgSuffix;
        assertEquals(failureMessage, expected, actual);    	
    }
    
    public void testGetAssemblyIDforSerialNumberThrowsExceptionAfterTMCDBTerminated() {
    	tmcdb.terminate();
    	try {
    		int actual = tmcdb.getAssemblyIDforSN("BogusSN");
    		fail("Call to getAssemblyId() should have thrown an exception, but did not.");
    	} catch (IllegalStateException ex) {
    		// Do nothing.
    	}    	
    }

    
    public void testGetAssemblyIds() {
        List<Integer> expected = new ArrayList<Integer>();
        List<Integer> actual = tmcdb.getAssemblyIds();
        String failureMessage = msgPrefix + "testGetAssemblyIds()" + msgSuffix;
        ListAssert.assertEquals(failureMessage, expected, actual);
    }
    
    public void testGetAssemblyIdsThrowsExceptionAfterTMCDBTerminated() {
    	tmcdb.terminate();
    	try {
            List<Integer> actual = tmcdb.getAssemblyIds();
    		fail("Call to getAssemblyIds() should have thrown an exception, but did not.");
    	} catch (IllegalStateException ex) {
    		// Do nothing.
    	}    	
    }
    
    
    public void testGetAssemblyNames() {
        List<String> expected = new ArrayList<String>();
        List<String> actual = tmcdb.getAssemblyNames();
        String failureMessage = msgPrefix + "testGetAssemblyNames()" + msgSuffix;
        ListAssert.assertEquals(failureMessage, expected, actual);
    }

    public void testGetAssemblyNamesFromAntennaName() {
        List<String> expected = new ArrayList<String>();
        List<String> actual = tmcdb.getAssemblyNames("DV01");
        String failureMessage = msgPrefix + "testGetAssemblyNames()" + msgSuffix;
        ListAssert.assertEquals(failureMessage, expected, actual);
    }
    
    public void testGetAssemblyNamesFromAntennaNameThrowsExceptionAfterTMCDBTerminated() {
    	tmcdb.terminate();
    	try {
            List<String> actual = tmcdb.getAssemblyNames("DV01");
    		fail("Call to getAssemblyNames('DV01') should have thrown an exception, but did not.");
    	} catch (IllegalStateException ex) {
    		// Do nothing.
    	}    	
    }

    
    public void testGetPropertyNamesFromAssemblyName() {
        List<String> expected = new ArrayList<String>();
        List<String> actual = tmcdb.getPropertyNames("assembly name 1");
        String failureMessage = msgPrefix + "testGetPropertyNames()" + msgSuffix;
        ListAssert.assertEquals(failureMessage, expected, actual);        
    }
    
    public void testGetPropertyNamesFromAssemblyNameThrowsExceptionAfterTMCDBTerminated() {
    	tmcdb.terminate();
    	try {
            List<String> actual = tmcdb.getPropertyNames("assembly name 1");
    		fail("Call to getPropertyNames('assembly name 1') should have thrown an exception, but did not.");
    	} catch (IllegalStateException ex) {
    		// Do nothing.
    	}    	
    }

    
    public void testGetPropertyDataTypeFromPropertyName() {
        String expected = "";
        String actual = tmcdb.getPropertyDataType("property name 1");
        String failureMessage = msgPrefix + "testGetPropertyDataType()" + msgSuffix;
        assertEquals(failureMessage, expected, actual);        
    }
    
    public void testGetPropertyDataTypeFromPropertyNameThrowsExceptionAfterTMCDBTerminated() {
    	tmcdb.terminate();
    	try {
            String actual = tmcdb.getPropertyDataType("property name 1");
    		fail("Call to getPropertyDataType('assembly name 1') should have thrown an exception, but did not.");
    	} catch (IllegalStateException ex) {
    		// Do nothing.
    	}    	
    }

    
//    public void testGetPropertyIdFromPropertyName() {
//        int expected = 0;
//        int actual = tmcdb.getPropertyId("property name 1");
//        String failureMessage = msgPrefix + "testGetPropertyId()" + msgSuffix;
//        assertEquals(failureMessage, expected, actual);        
//    }
//
//    public void testGetPropertyIdFromPropertyNameThrowsExceptionAfterTMCDBTerminated() {
//    	tmcdb.terminate();
//    	try {
//    		int actual = tmcdb.getPropertyId("property name 1");
//    		fail("Call to getPropertyId('assembly name 1') should have thrown an exception, but did not.");
//    	} catch (IllegalStateException ex) {
//    		// Do nothing.
//    	}    	
//    }

    
    public void testGetMonitorData() {
        TimeValue[] expected = new TimeValue[0];
        TimeValue[] actual = tmcdb.getMonitorData(
            "DV01",
            "assembly name 1",
            "property name 2",
            new ArrayTime(2006,1,1,0,0,0.0),
            new ArrayTime(2007,12,31,23,59,59.99)
            );
        String failureMessage = msgPrefix + "testGetMonitorData()" + msgSuffix;
        ArrayAssert.assertEquals(failureMessage, expected, actual);        
    }
    
    public void testGetMonitorDataThrowsExceptionAfterTMCDBTerminated() {
    	tmcdb.terminate();
    	try {
            TimeValue[] actual = tmcdb.getMonitorData(
                "DV01",
                "assembly name 1",
                "property name 2",
                new ArrayTime(2006,1,1,0,0,0.0),
                new ArrayTime(2007,12,31,23,59,59.99)
                );
    		fail("Call to getMonitorData() should have thrown an exception, but did not.");
    	} catch (IllegalStateException ex) {
    		// Do nothing.
    	}    	
    }
    
    //
    // Non-public method tests
    
    public void testGetAssemblySNs() {
        List<String> expected = new ArrayList<String>();
        List<String> actual = tmcdb.getAssemblySNs();
        String failureMessage = msgPrefix + "testGetAssemblySNs()" + msgSuffix;
        ListAssert.assertEquals(failureMessage, expected, actual);
    }
    
    public void testGetAssemblySNsThrowsExceptionAfterTMCDBTerminated() {
    	tmcdb.terminate();
    	try {
            List<String> actual = tmcdb.getAssemblySNs();
    		fail("Call to getAssemblySNs() should have thrown an exception, but did not.");
    	} catch (IllegalStateException ex) {
    		// Do nothing.
    	}    	
    }
    

    public void testGetBaseElementIdsFromAntennaName() {
        List<Integer> expected = new ArrayList<Integer>();
        List<Integer> actual = tmcdb.getBaseElementIds("DV01");
        String failureMessage = msgPrefix + "testGetBaseElementIdsFromAntennaName()" + msgSuffix;
        ListAssert.assertEquals(failureMessage, expected, actual);
    }
    
    public void testGetBaseElementIdsFromAntennaNameThrowsExceptionAfterTMCDBTerminated() {
    	tmcdb.terminate();
    	try {
            List<Integer> actual = tmcdb.getBaseElementIds("DV01");
    		fail("Call to getBaseElementIds('DV01') should have thrown an exception, but did not.");
    	} catch (IllegalStateException ex) {
    		// Do nothing.
    	}    	
    }
    
    
    public void testGetAssemblyIdsFromBaseElementId() {
        List<Integer> expected = new ArrayList<Integer>();
        List<Integer> actual = tmcdb.getAssemblyIds(1);
        String failureMessage = msgPrefix + "testGetAssemblyIdsFromBaseElementId()" + msgSuffix;
        ListAssert.assertEquals(failureMessage, expected, actual);
    }
    
    public void testGetAssemblyIdsFromBaseElementIdThrowsExceptionAfterTMCDBTerminated() {
    	tmcdb.terminate();
    	try {
            List<Integer> actual = tmcdb.getAssemblyIds(1);
    		fail("Call to getAssemblyIds(1) should have thrown an exception, but did not.");
    	} catch (IllegalStateException ex) {
    		// Do nothing.
    	}    	
    }
    
    public void testGetAssemblyIdsFromAntennaName() {
        List<Integer> expected = new ArrayList<Integer>();
        List<Integer> actual = tmcdb.getAssemblyIdsInAntenna("DV01");
        String failureMessage = msgPrefix + "testGetAssemblyIdsFromAntennaName()" + msgSuffix;
        ListAssert.assertEquals(failureMessage, expected, actual);   	
    }

    public void testGetAssemblyIdsFromAntennaNameThrowsExceptionAfterTMCDBTerminated() {
    	tmcdb.terminate();
    	try {
            List<Integer> actual = tmcdb.getAssemblyIdsInAntenna("DV01");
    		fail("Call to getAssemblyIds('DV01') should have thrown an exception, but did not.");
    	} catch (IllegalStateException ex) {
    		// Do nothing.
    	}    	
    }

    public void testGetAssemblyIdFromAssemblyNameAndAntennaName() {
    	int expected = 0;
    	int actual = tmcdb.getAssemblyId("assembly name 1", "DV01");
        String failureMessage = msgPrefix + "testGetAssemblyIdFromAssemblyNameAndAntennaName()" + msgSuffix;
        assertEquals(failureMessage, expected, actual);    	
    }
    
    public void testGetAssemblyIdFromAssemblyNameAndAntennaNameThrowsExceptionAfterTMCDBTerminated() {
    	tmcdb.terminate();
    	try {
    		int actual = tmcdb.getAssemblyId("assembly name 1", "DV01");
    		fail("Call to getAssemblyId('assembly name 1', 'DV01') should have thrown an exception, but did not.");
    	} catch (IllegalStateException ex) {
    		// Do nothing.
    	}    	
    }
    
    
    public void testGetAssemblyNameFromAssemblyId() {
        String expected = "";
        String actual = tmcdb.getAssemblyName(1);
        String failureMessage = msgPrefix + "testGetAssemblyNameFromAssemblyId()" + msgSuffix;
        assertEquals(failureMessage, expected, actual);
    }
    
    public void testGetAssemblyNameFromAssemblyIdThrowsExceptionAfterTMCDBTerminated() {
    	tmcdb.terminate();
    	try {
            String actual = tmcdb.getAssemblyName(1);
    		fail("Call to getAssemblyName(1) should have thrown an exception, but did not.");
    	} catch (IllegalStateException ex) {
    		// Do nothing.
    	}    	
    }

    public void testGetAssemblyIdsWithName() {
        List<Integer> expected = new ArrayList<Integer>();
        List<Integer> actual = tmcdb.getAssemblyIdsWithName("assembly name 1");
        String failureMessage = msgPrefix + "testGetAssemblyIdsWithName()" + msgSuffix;
        ListAssert.assertEquals(failureMessage, expected, actual);   	    	
    }
    
    public void testGetAssemblyIdsWithNameThrowsExceptionAfterTMCDBTerminated() {
    	tmcdb.terminate();
    	try {
            List<Integer> actual = tmcdb.getAssemblyIdsWithName("assembly name 1");
    		fail("Call to getAssemblyIdsWithName('assembly name 1') should have thrown an exception, but did not.");
    	} catch (IllegalStateException ex) {
    		// Do nothing.
    	}    	
    }
    
    
    /**
     * Implementation details users of this class don't care about.
     */
    
    private TMCDBCommonQueries tmcdb;

    protected void setUp()
    {
    	// Note: dbConfig.properties must be copied from TMCDB_Monitor/test/dbConfig.properties to
    	// TMCDB_Monitor/dbConfig.properties to run these tests in Eclipse.
    	
        // Each test must run against a database with empty tables.
        String dropTables = "SQL/dropAllTables.sql";
        String loadTables = "SQL/createHsqldbTables.sql";
        String loadTestData = "SQL/loadHsqldbEmptyDB.sql";
        
        tmcdb = new TMCDBCommonQueries();
        tmcdb.connectToDB();
        try {
            tmcdb.loadAndExecuteScript(dropTables);
        } catch (Exception e) {
        	// running drop tables will generate exceptions if the database has never contained data
        }
        try {
            tmcdb.loadAndExecuteScript(loadTables);
            tmcdb.loadAndExecuteScript(loadTestData);
        } catch (Exception e) {
            // TODO: better error handling and reporting.
            e.printStackTrace(); 
        }
        try {
            tmcdb.initialize("TMCDB-Common-Queries-Empty");
        } catch (AcsJTmcdbErrTypeEx e) {
            // TODO: better error handling and reporting.
            e.printStackTrace();           
        }
     }
    
    protected void tearDown() {
    	tmcdb.terminate();
    }
    
	/**
	 * main used to support testing with TAT or other command line tools.
	 * 
	 * @param args - not used.
	 */
	public static void main(String[] args) {
		TestRunner.run(TMCDBCommonQueriesTestsWithEmptyDB.class);
	}

} // class

//
//O_o
