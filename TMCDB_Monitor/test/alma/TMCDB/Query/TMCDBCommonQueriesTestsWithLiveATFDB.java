/*
 * ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

/** 
 * @author  srankin
 * @version $Id$
 * @since    
 */

package alma.TMCDB.Query;

import alma.hla.runtime.asdm.types.ArrayTime;
import alma.TMCDB.TimeValue;
// import alma.TMCDB.TMCDBException;
import alma.TmcdbErrType.wrappers.AcsJTmcdbErrTypeEx;
import junit.framework.TestCase;
import junit.textui.TestRunner;
import java.util.ArrayList;
import java.util.List;
// import junitx.framework.ArrayAssert;
import junitx.framework.ListAssert;


public class TMCDBCommonQueriesTestsWithLiveATFDB extends TestCase {

	String msgPrefix = "Actual and expected do not match in ";
    String msgSuffix = " with live ATF database.";

    public void testGetAntennaNames() {
        List<String> expected = new ArrayList<String>();
        expected.add("DV01");
        expected.add("DA41");
        List<String> actual = tmcdb.getAntennaNames();
        String failureMessage = msgPrefix + "testGetAntennaNames()" + msgSuffix;
        ListAssert.assertEquals(failureMessage, expected, actual);
    }
    
    public void testGetPropertyNamesFromAssemblyName() {
        List<String> expected = new ArrayList<String>();
        expected.add("FREQ");
        expected.add("FTS_STATUS");
        expected.add("OUTPUT_RF_LEVEL");
        expected.add("FEW_NON_INV_CYCLES");
        expected.add("MANY_NON_INV_CYCLES");
        expected.add("FEW_INV_CYCLES");
        expected.add("MANY_INV_CYCLES");
        expected.add("CLOCK_STATUS");
        expected.add("PHASE_OFFSET");
        List<String> actual = tmcdb.getPropertyNames("FLOOG");
        // DEBUG
//        if (actual.size() > 0) {
//        	System.out.println("Properties found for FLOOG: ");
//        	for (int i = 0; i < actual.size(); i++)
//        		System.out.println("  " + actual.get(i));
//        }
        // \DEBUG
        String failureMessage = msgPrefix + "testGetPropertyNames()" + msgSuffix;
        ListAssert.assertEquals(failureMessage, expected, actual);        
    }

    public void testGetAssemblyIdFromAssemblyNameAndAntennaName() {
    	int expected = 7;
    	int actual = tmcdb.getAssemblyId("FLOOG", "DV01");
        String failureMessage = msgPrefix + "testGetAssemblyIdFromAssemblyNameAndAntennaName()" + msgSuffix;
        assertEquals(failureMessage, expected, actual);    	
    }
    
//    public void testGetPropertyIdFromPropertyName() {
//        int expected = 100;
//        int actual = tmcdb.getPropertyId("PHASE_OFFSET");
//        String failureMessage = msgPrefix + "testGetPropertyId()" + msgSuffix;
//        assertEquals(failureMessage, expected, actual);        
//    }
    
    public void testGetPropertyDataTypeFromPropertyName() {
        String expected = "float";
        String actual = tmcdb.getPropertyDataType("PHASE_OFFSET");
        String failureMessage = msgPrefix + "testGetPropertyDataType()" + msgSuffix;
        assertEquals(failureMessage, expected, actual);        
    }

    public void testGetMonitorData() {
    	// Note: Due to issues with assertes on TimeValue data, test results are dumped to the console.
        TimeValue[] expected = {
            new TimeValue(new ArrayTime(2007,11,26,21,36,43.405938000), 0.0080),
            new TimeValue(new ArrayTime(2007,11,26,21,36,52.060965000), 0.0080)
        };
        TimeValue[] actual = tmcdb.getMonitorData(
            "DV01",
            "FLOOG",
            "PHASE_OFFSET",
            new ArrayTime(2007,11,26,21,36,40.0),
            new ArrayTime(2007,11,26,21,37,0.0)
            );
        // DEBUG
            System.out.println();
            System.out.println("PHASE_OFFSET monitor point query result length: " + actual.length);
            if (actual.length > 0) {
            	System.out.println("PHASE_OFFSET monitor point values found for FLOOG in ALAM001: ");
            	for (int i = 0; i < actual.length; i++)
            		System.out.println("  " + actual[i]);
            }
        // \DEBUG
        // String failureMessage = msgPrefix + "testGetMonitorData()" + msgSuffix;
        // ArrayAssert.assertEquals(failureMessage, expected, actual);        
    }
	
	private TMCDBCommonQueries tmcdb;

    protected void setUp()
    {
    	// Each test must run against a new database connection.
    	tmcdb = new TMCDBCommonQueries();
        tmcdb.connectToDB();
        try {
            tmcdb.initialize("ATF1");
        } catch (AcsJTmcdbErrTypeEx e) {
            // TODO: better error handling and reporting.
            e.printStackTrace();           
        }
    }

	protected void tearDown() {
        tmcdb.terminate();
    }
	
	/**
	 * @param args - not used.
	 */
	public static void main(String[] args) {
		TestRunner.run(TMCDBCommonQueriesTestsWithLiveATFDB.class);
	}

} // class

//
//O_o

