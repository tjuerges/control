#! /bin/sh
#
# Start a local copy of HSQLDB for local test.

if [ ! -d ../tmp ]; then  mkdir ../tmp; fi

java -cp ../lib/hsqldb.jar org.hsqldb.Server &> ../tmp/startHsqldb.log &
echo $! > ../tmp/Hsqldb.pid

#
# Set dbConfig.properties for following tests
cp dbConfig.properties.hsqldb dbConfig.properties

#
# Load tables to prevent noise errors in test logs
./runSqlTool.sh SQL/createHsqldbTables.sql &> /dev/null

exit 0

#
# O_o
