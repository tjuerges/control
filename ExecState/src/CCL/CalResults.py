#! /usr/bin/env python
# $Id$
#*******************************************************************************
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2008 - 2010
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
#

from Acspy.Common.EpochHelper import EpochHelper
from CCL.Container import getComponent
from CCL.logging import getLogger
from Control import PointingCalResult
from Control import FocusCalResult
import acstime
import math

class CalResults:
    """This class provide a way to retrieve TELCAL calibration results
    when these are available.
    
    Simple usage:
    
        array = getArray()
        calResults = array.getCalResults()
        ...
        obs_mode.beginScan(...)
        obs_mode.endScan()
        if calResults.isPhaseCalAvailable(timeout=60):
            phaseCalResult = calResults.getPhaseCalResult()
            
    This class is actually a wrapper over CONTROL ExecutionState component,
    which receives events from TELCAL when calibration results are available
    and retrieves them from TELCAL GetTelCalResuls component.
    """
    
    def __init__(self, execStateCompName):
        """Create an instance of the class.
        
        This constructor shouldn't be used directly from CCL observation scripts.
        CCL.Array.getCalResults() should be used instead.
        
        Parameters:
        execStateCompName - ExecutionState component name.
        """
        self.logger = getLogger()
        self.logger.logDebug("Getting component "+execStateCompName)
        self._execState = getComponent(execStateCompName)
        
    def isPhaseCalAvailable(self, timeout=60.0):
        """Waits for PhaseCal results.
        
        Parameters:
        timeout - Timeout in seconds
        
        Return:
        True if PhaseCal results are available.
        False if the timeout expired.
        """
        return self._execState.isPhaseCalAvailable(timeout)
    
    def getPhaseCalResult(self):
        """Get PhaseCal results.
        
        This function should be called after isPhaseCalAvailable() returns True.
        
        If no PhaseCal result is available, an ExecStateExceptions.NoCalibrationResultEx
        exception will be raised.
        
        Return:
        asdmIDL.ASDMDataSetIDL object.
        """
        return self._execState.getPhaseCalResult()

    def isPointingCalAvailable(self, timeout=60.0):
        """Waits for PointingCal results.
        
        Parameters:
        timeout - Timeout in seconds
        
        Return:
        True if PointingCal results are available.
        False if the timeout expired.
        """
        return self._execState.isPointingCalAvailable(timeout)
    
    def getPointingCalResult(self):
        """Get PointingCal results.
        
        This function should be called after isPointingCalAvailable() returns True.
        
        If no PointingCal result is available, an ExecStateExceptions.NoCalibrationResultEx
        exception will be raised.
        
        Return:
        Control.PointingCalResult object.
        """
        r = self._execState.getPointingCalResult()
        pointingResult = []
        for cprow in r.calPointing.row:
            for ip in range(cprow.numReceptor):
                for pol in cprow.polarizationTypes:
                    corrAz = seconds(cprow.collOffsetRelative[ip][0].value)
                    corrEl = seconds(cprow.collOffsetRelative[ip][1].value)
                    corrAzErr = seconds(cprow.collError[ip][0].value)
                    corrElErr = seconds(cprow.collError[ip][1].value)
                    peak = cprow.peakIntensity[ip].value
                    peakErr = 0.0
                    if not cprow.peakIntensityWasFixed:
                        peakErr = cprow.peakIntensityError[ip].value
                    beam = seconds(cprow.beamWidth[ip][0].value)
                    beamErr = 0.0
                    if not cprow.beamWidthWasFixed[0]:
                        beamErr = seconds(cprow.beamWidthError[ip][0].value)
                    pr = PointingCalResult(cprow.antennaName, pol,
                                           corrAz, corrAzErr, corrEl, corrElErr,
                                           peak, peakErr, beam, beamErr)
                    pointingResult.append(pr)
        return pointingResult

    def getRawPointingCalResult(self):
        """Get raw PointingCal results.
        
        The "raw" means that the result is returned as a asdmIDL.ASDMDataSetIDL object.
        
        This function should be called after isPointingCalAvailable() returns True.
        
        If no PointingCal result is available, an ExecStateExceptions.NoCalibrationResultEx
        exception is raised.
        
        Return:
        asdmIDL.ASDMDataSetIDL object.
        """
        return self._execState.getPointingCalResult()
    
    def isFocusCalAvailable(self, timeout=60.0):
        """Waits for FocusCal results.
        
        Parameters:
        timeout - Timeout in seconds
        
        Return:
        True if FocusCal results are available.
        False if the timeout expired.
        """
        return self._execState.isFocusCalAvailable(timeout)

    def getFocusCalResult(self):
        """Get FocusCal results.
        
        This function should be called after isFocusCalAvailable() returns True.
        
        If no FocusCal result is available, an ExecStateExceptions.NoCalibrationResultEx
        exception will be raised.
        
        Return:
        Control.FocusCalResult object.
        """
        r = self._execState.getFocusCalResult()
        focusResult = []
        for cprow in r.calFocus.row:
            for ip in range(cprow.numReceptor):
                for pol in cprow.polarizationTypes:
                    focusCorr = [0.0, 0.0, 0.0]
                    focusCorrErr = [0.0, 0.0, 0.0]
                    axisFixed = [True, True, True]
                    for axis in range(3):
                        if not cprow.wereFixed[axis]:
                            focusCorr[axis] = cprow.offset[ip][axis].value
                            focusCorrErr[axis] = cprow.offsetError[ip][axis].value
                            axisFixed[axis] = False
                    fr = FocusCalResult(cprow.antennaName, pol, cprow.receiverBand,
                                        focusCorr[0], focusCorr[1], focusCorr[2],
                                        focusCorrErr[0], focusCorrErr[1], focusCorrErr[2],
                                        axisFixed[0], axisFixed[1], axisFixed[2])
                    focusResult.append(fr)
        return focusResult
    
    def getRawFocusCalResult(self):
        """Get raw FocusCal results.
        
        The "raw" means that the result is returned as a asdmIDL.ASDMDataSetIDL object.
        
        This function should be called after isFocusCalAvailable() returns True.
        
        If no FocusCal result is available, an ExecStateExceptions.NoCalibrationResultEx
        exception is raised.
        
        Return:
        asdmIDL.ASDMDataSetIDL object.
        """
        return self._execState.getFocusCalResult()

    def isWVRCalAvailable(self, timeout=60.0):
        """Waits for WVR calibration results.
        
        Parameters:
        timeout - Timeout in seconds
        
        Return:
        True if WVR calibration results are available.
        False if the timeout expired.
        """
        return self._execState.isWVRCalAvailable(timeout)
    
    def getRawWVRCalResult(self):
        """Get raw WVR calibration results.
        
        The "raw" means that the result is returned as an asdmIDL.ASDMDataSetIDL object.
        
        This function should be called after isWVRCalAvailable() returns True.
        
        If no WVR calibration result is available, an ExecStateExceptions.NoCalibrationResultEx
        exception is raised.
        
        Return:
        asdmIDL.ASDMDataSetIDL object.
        """
        return self._execState.getWVRCalResult()

# ------------------------------------------------------------------
# Some utility functions, they should be moved to a common place.
# ------------------------------------------------------------------

def timeString(t):
    """
    Convert a ACS time value into a FITS type string.
    """
    e1 = EpochHelper()
    e1.value(t)
    return e1.toString(acstime.TSArray,"%Y-%m-%dT%H:%M:%S.%q", 0L, 0L)

def sdmTimeString(t):

    """
    Convert a time value (as used by ASDM, i.e. MJD in nanoseconds) into a FITS type string.
    """
    e1 = EpochHelper()
    t = t/100+100840*864000000000L
    e1.value(t)
    return e1.toString(acstime.TSArray,"%Y-%m-%dT%H:%M:%S.%q", 0L, 0L)

def seconds(x):
    """ Convert an angle into arc seconds. """
    return x*180.*3600./math.pi

# Use math.degrees
def degrees(x):
    """ Convert an angle into degrees. """
    return x*180./math.pi
