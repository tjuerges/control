/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File State.java
 */
package alma.Control.ExecState;

import alma.Control.IDLState;
import alma.Control.SystemState;
import alma.Control.SystemSubstate;
import alma.Control.IDLError;

import java.util.ArrayList;

/**
 * The State class.
 *
 * @version 1.0 Mar 31, 2005
 * @author Allen Farris
 */
public class State {
        
    // States
    private static final String[] stateName = { 
            "Inaccessible",
            "Operational"
    };
    private static final int Inaccessible 	= 0;
    private static final int Operational	= 1;
    
    // Sub-states
    private static final String[] substateName = { 
            "StartingUpPass1",
            "StartedUpPass1",
            "StartingUpPass2",
            "Waiting",
            "ShuttingDownPass1",
            "ShutDownPass1",
            "ShuttingDownPass2",
            "Stopped",
            "NoError",
            "Error"
    };
    private static final int StartingUpPass1	= 0;
    private static final int StartedUpPass1		= 1;
    private static final int StartingUpPass2	= 2;
    private static final int Waiting			= 3;
    private static final int ShuttingDownPass1	= 4;
    private static final int ShutDownPass1		= 5;
    private static final int ShuttingDownPass2	= 6;
    private static final int Stopped			= 7;
    private static final int NoError			= 8;
    private static final int Error				= 9;

    public static final State Inaccessible_StartingUpPass1   = new State(Inaccessible,StartingUpPass1);
    public static final State Inaccessible_StartedUpPass1    = new State(Inaccessible,StartedUpPass1);
    public static final State Inaccessible_StartingUpPass2	 = new State(Inaccessible,StartingUpPass2);
    public static final State Inaccessible_Waiting       	 = new State(Inaccessible,Waiting);
    public static final State Inaccessible_ShuttingDownPass1 = new State(Inaccessible,ShuttingDownPass1);
    public static final State Inaccessible_ShutDownPass1     = new State(Inaccessible,ShutDownPass1);
    public static final State Inaccessible_ShuttingDownPass2 = new State(Inaccessible,ShuttingDownPass2);
    public static final State Inaccessible_Stopped       	 = new State(Inaccessible,Stopped);
    public static final State Operational_NoError        	 = new State(Operational,NoError);
    public static final State Operational_Error          	 = new State(Operational,Error);

    
    private int state;
    private int substate;
    private int previousState;
    private int previousSubstate;
    private ArrayList bad;
    
    private State(int state, int substate) {
        setState(state,substate);
        previousState = state;
        previousSubstate = substate;
        bad = new ArrayList ();
    }
    
    public State(State x) {
        setState(x.state,x.substate);
        bad = new ArrayList ();        
    }
    
    public State(IDLState s) {
        state = s.stateValue.value();
        substate = s.substateValue.value();
        for (int i = 0; i < s.badResource.length; ++i) {
            bad.add(new Error(s.badResource[i]));
        }
    }
    
    public IDLState toIDL() {
        Error[] e = getBadResource();
        IDLError[] bad = new IDLError [e.length];
        for (int i = 0; i < bad.length; ++i) {
            bad[i] = e[i].toIDL();
        }
        return new IDLState(SystemState.from_int(state), SystemSubstate.from_int(substate), bad);
    }
    
    public boolean equals (State x) {
        return (state == x.state && substate == x.substate) ? true : false;
    }
    
    public String toString() {
        return stateName[state] + "_" + substateName[substate];
    }
    
    public String getState() {
        return stateName[state];
    }
    
    public String getSubstate() {
        return substateName[substate];
    }
    
    public int getStateValue() {
        return state;
    }
    
    public int getSubstateValue() {
        return substate;
    }
    
    public boolean isOperational() {
        return state == State.Operational;
    }
    
    public boolean isInaccessible() {
        return state == State.Inaccessible;
    }
    
    private void setState(int state, int substate) {
        previousState = state;
        previousSubstate = substate;
        if (state == Inaccessible) {
            this.state = Inaccessible;
            switch (substate) {
            case StartingUpPass1:	this.substate = StartingUpPass1; break;
            case StartedUpPass1:	this.substate = StartedUpPass1; break;
            case StartingUpPass2:	this.substate = StartingUpPass2; break;
            case Waiting:			this.substate = Waiting; break;
            case ShuttingDownPass1:	this.substate = ShuttingDownPass1; break;
            case ShutDownPass1:		this.substate = ShutDownPass1; break;
            case ShuttingDownPass2:	this.substate = ShuttingDownPass2; break;
            case Stopped:			this.substate = Stopped; break;
            default:				throw new IllegalArgumentException ("Invalid substate: " + substate);
            }
        } else if (state == Operational) {
            this.state = Operational;
            switch (substate) {
            case NoError: 		this.substate = NoError; break;
            case Error: 		this.substate = Error; break;
            default:			throw new IllegalArgumentException ("Invalid substate: " + substate);            
            }
        } else {
            throw new IllegalArgumentException ("Invalid state: " + state);
        }
    }
    
    public void setState(State x) {
        setState(x.state,x.substate);
    }
    
    public void addBadResource(Error err) {
        bad.add(err);
        if (state == Operational) {
            substate = Error;
        }
    }
    
    public void removeBadResource(Error err) {
        if (!bad.remove(err)) {
            throw new IllegalArgumentException("Specified resource is not on the bad-resource list.");
        }
    }
    
    public void clearBadResource() {
        bad.clear();
    }
    
    public boolean isError() {
        return bad.size() > 0;
    }
    
    public Error[] getBadResource() {
        Error[] tmp = new Error [bad.size()];
        if (bad.size() > 0) {
            tmp = (Error[])bad.toArray(tmp);
        }
        return tmp;
    }

    public static int getStateValue(String name) {
        for (int i = 0; i < stateName.length; ++i) {
            if (stateName[i].equals(name)) {
                return i;
            }
        }
        return -1;
    }

    public static int getSubstateValue(String name) {
        for (int i = 0; i < substateName.length; ++i) {
            if (substateName[i].equals(name)) {
                return i;
            }
        }
        return -1;
    }

    public String getPreviousState() {
        return stateName[state];
    }
    
    public String getPreviousSubstate() {
        return substateName[substate];
    }
    
    public int getPreviousStateValue() {
        return previousState;
    }
    
    public int getPreviousSubstateValue() {
        return previousSubstate;
    }
    
    

}
