/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File Result.java
 */
package alma.Control.ExecState;

import alma.Control.Completion;
import alma.Control.ResultAction;
import alma.Control.IDLResult;

/**
 * A Result object specifies the result of a process.  It has two 
 * aspects: an attribute specifying the completion state of the 
 * process and an indicator of whether to stop or continue.  The 
 * Result object is primarily used by the Error Analyzer to direct
 * the flow of control after taking action as a result of an error.
 * 
 * There are four completion states:
 * <ul>
 * <li> Fail
 * <li> Success 
 * <li> Partial (success)
 * <li> Timeout (unknown due to timeout)
 * </ul>
 * 
 * @version 1.0 Apr 12, 2005
 * @author Allen Farris
 */
public class Result {
    
    private static final String[] conditionName = {
            "fail",
            "success",
            "partial",
            "timout"
    };
    
    private static final String[] actionName = {
            "stop",
            "continue"
    };
    
    private static final int Fail 		= 0;
    private static final int Success 	= 1;
    private static final int Partial 	= 2;
    private static final int Timeout 	= 3;
    
    private static final int Stop		= 0;
    private static final int Continue	= 1;

    /**
     * A Result specifying the completion is Fail and the action to take is Stop.
     */
    public static final Result Fail_Stop 		= new Result (Fail,Stop);
    /**
     * A Result specifying the completion is Success and the action to take is Stop.
     */
    public static final Result Success_Stop 	= new Result (Success,Stop);
    /**
     * A Result specifying the completion is Partial and the action to take is Stop.
     */
    public static final Result Partial_Stop 	= new Result (Partial,Stop);
    /**
     * A Result specifying the completion is Timeout and the action to take is Stop.
     */
    public static final Result Timeout_Stop 	= new Result (Timeout,Stop);
    /**
     * A Result specifying the completion is Fail and the action to take is Continue.
     */
    public static final Result Fail_Continue 	= new Result (Fail,Continue);
    /**
     * A Result specifying the completion is Success and the action to take is Continue.
     */
    public static final Result Success_Continue = new Result (Success,Continue);
    /**
     * A Result specifying the completion is Partial and the action to take is Continue.
     */
    public static final Result Partial_Continue = new Result (Partial,Continue);
    /**
     * A Result specifying the completion is Timeout and the action to take is Continue.
     */
    public static final Result Timeout_Continue = new Result (Timeout,Continue);
    
    private int condition;
    private int action;
    
    private Result (int condition, int action) {
        this.condition = condition;
        this.action = action;
    }
    
    public Result(Completion condition, ResultAction action) {
        this.condition = condition.value();
        this.action = action.value();
    }
    
    public IDLResult toIDL() {
        return new IDLResult(Completion.from_int(condition),ResultAction.from_int(action));
    }
    
    /**
     * Return true if and only if the action to be taken is to continue with the process.
     * @return
     */
    public boolean isContinue() {
        return action == Continue;
    }
    
    /**
     * Return true if and only if the action to be taken is to stop the process.
     * @return
     */
    public boolean isStop() {
        return action == Stop;
    }
    
    /**
     * Return the result of the process as a string.
     */
    public String toString() {
        return conditionName[condition];
    }
        
}
