/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File Controller.java
 */
package alma.Control.ExecState;

import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.Iterator;

import alma.Control.ControllerOperations;
import alma.Control.ErrorEnum;
import alma.Control.IDLResult;
import alma.Control.ResourceEnum;
import alma.ACS.ComponentStates;
import alma.Control.TimeSource;
import alma.Control.IDLError;
import alma.Control.InaccessibleException;
import alma.Control.SystemState;
import alma.Control.SystemSubstate;
import alma.Control.Common.Name;
import alma.Control.Common.Util;
import alma.acs.component.ComponentLifecycle;
import alma.acs.component.ComponentLifecycleException;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.acs.container.ContainerServices;
import si.ijs.maci.ComponentSpec;

/**
 * A Controller is a component that has state and an error analyzer.
 * It is also hierarchical; it has a parent controller, unless it is
 * the master controller, of course.  It may also control subordinate
 * devices or resources and it maintains a list of the resources it
 * controls and their states.  Controllers also have access to the 
 * Master Clock.
 * <p>
 * The ControllerComponent is an abstract class.  Component lifecycle
 * methods are fully implemented.  Extended classes are required to 
 * implement the following methods.
 * <ul>
 * <li>void startupPass1();
 * <li>void startupPass2();
 * <li>void waitShutdownPass1();
 * <li>void shutdownPass1();
 * <li>void shutdownPass2()
 * </ul>
 * The follwing public methods are implemented by the ControllerComponent.
 * <ul>
 * <li>Result reportError(String resourceName, ResourceType resourceType, int errorType, long time, String msg)
 * <li>void reportStateChange (String resourceName, String state, String substate)
 * <li>IDLResult reportRemoteError(String resourceName, ResourceEnum resourceType, ErrorEnum errorType, long time, String msg)
 * <li>void reportRemoteStateChange(String resourceName, ResourceEnum resourceType, SystemState newState, SystemSubstate newSubstate)
 * <li>public ErrorAnalyzer getAnalyzer()
 * <li>TimeSource getClock()
 * <li>ContainerServices getContainer()
 * <li>Logger getLogger()
 * <li>ControllerComponent getParent()
 * <li>State getState()
 * <li>ResourceType getResourceType()
 * <li>String getName()
 * <li>String getComponentName()
 * <li>int getResourceTypeValue()
 * <li>int getStateValue()
 * <li>int getSubstateValue()
 * <li>void setParent(String name)
 * <li>IDLError[] getBadResource()
 * <li>String[] getBadResourceList() {
 * <li>String name()
 * </ul>
 *
 * @version 1.0 Apr 10, 2005
 * @author Allen Farris
 */
public abstract class ControllerImpl implements ControllerOperations, ComponentLifecycle {

    /**
     * The name of this Controller, i.e. the component name.
     */
    protected String instanceName;

    /**
     * The ACS container services.
     */
    protected ContainerServices container;
    
    /**
     * The ACS Logger.
     */
    protected Logger logger;

    /**
     * The parent of this Controller.  This object will be null
     * for the Master.  The parent of this controller must set the 
     * parentName.  The extended class must set the CORBA parent object.
     */
    protected String parentName;
    protected ControllerImpl parent;
    
    /**
     * The type of resource of this Controller.
     */
    protected ResourceType resourceType;
    
    /**
     * The name of this resource (NOT the component name!).
     */
    protected String resourceName;
    
    /**
     * The Error Analyzer -- extended classes must create this object.
     */
    protected ErrorAnalyzer analyzer;
    
    /**
     * The state of this controller.
     */
    protected State state;
    
    /**
     * The Master Clock.
     */
    protected TimeSource clock;

    /**
     * Maintain a list of resources and their current states.
     * This list is an array of Resource objects.
     */
    protected ArrayList resource;
    
    private ComponentStates componentState = ComponentStates.COMPSTATE_NEW;
    
    /**
     * Create a Controller.
     * @param container
     * @param logger
     * @param parent
     * @param resourceType
     * @param name
     */
    public ControllerImpl(ContainerServices container, 
            Logger logger, String parentName, ResourceType resourceType, String name) {
        this.container = container;
        this.logger = logger;
        this.parent = null;
        this.parentName = parentName;
        this.resourceType = resourceType;
        setName(name);
        this.analyzer = null;
        this.state = new State(State.Inaccessible_Stopped);
        this.clock = null;
        this.resource = new ArrayList ();
    }

    /**
     * Create a controller.  The creator must set the container, the logger,
     * and the parent prior to its use.
     *
     */
    public ControllerImpl() {
        this.container = null;
        this.logger = null;
        this.parent = null;
        this.parentName = null;
        this.resourceType = null;
        this.resourceName = null;
        this.instanceName = null;
        this.analyzer = null;
        this.state = new State(State.Inaccessible_Stopped);
        this.clock = null;
        this.resource = new ArrayList ();
    }

    /**
     * @param container The container to set.
     */
    protected void setContainer(ContainerServices container) {
        this.container = container;
    }
    
    /**
     * @param logger The logger to set.
     */
    protected void setLogger(Logger logger) {
        this.logger = logger;
    }
    
    /**
     * 
     * @param resourceType The type of this resource.
     */
    protected void setResourceType(ResourceType resourceType) {
        this.resourceType = resourceType;
    }
    
    /**
     * 
     * @param name The name of this resource.
     */
    protected void setName(String name) {
    	setComponentName(name);
    }
    
    protected void setComponentName(String name) {
        this.instanceName = name;
        // This should be changed in the future, but it is OK for now.
        if (name.startsWith(Name.ControlPrefix))
        	this.resourceName = name.substring(Name.ControlPrefix.length());
        else
        	this.resourceName = name;
    }
    
    protected void setResourceName(String name) {
    	this.resourceName = name;
    }
    
    //////////////////////////////////////////////////////
    // Startup and Shutdown Methods						//
    //////////////////////////////////////////////////////
    
    //private ControllerComponent thisDevice;
    public Result createDynamicComponent(String name, 
            String type, String code, String container) {
        //ComponentSpec spec = new ComponentSpec(name,type,code,container);
        //try {
        //    thisDevice = ScriptExecutorHelper.narrow...
        //     do this with reflection.
        //} catch (Exception e) {
        //    return reportError(...);
        //}
        return Result.Success_Continue;
    }
    
    public Result createStaticComponent(String name) {
        // We have a similar problem here.
        return Result.Success_Continue;
    }
    
    public Result powerUp() {
        return Result.Success_Continue;
    }

    public Result powerDown() {
        return Result.Success_Continue;
    }

    public Result restart() {
        return Result.Success_Continue;
    }

    public Result initialize() {
        if (parentName == null)
            return Result.Success_Continue;
        // Access the parent component.
        
        return Result.Success_Continue;
    }

    static ControllerImpl access(String deviceName) {
        return null;
    }

    static ControllerImpl release(String deviceName) {
        return null;
    }

    /**
     * Pass 1 of the initialization procedure.  The extended
     * class must override this method and set the final state.
     */
    public void startupPass1() {
        beginStartupPass1();
        // Do nothing.
        endStartupPass1();
    }
    protected void beginStartupPass1() {
        // Make sure we are in the proper state.
        if (!state.equals(State.Inaccessible_Stopped)) {
            String s = instanceName + ": Cannot execute this request - Automatic Array Component must be Inaccessible and Stopped.";
            logger.severe(s);
            throw new IllegalStateException (s);
        }
        state.setState(State.Inaccessible_StartingUpPass1);
    }
    protected void endStartupPass1() {
        state.setState(State.Inaccessible_StartedUpPass1);
    }

    /**
     * Pass 2 of the initialization procedure.
     */
    public void startupPass2() {
        beginStartupPass2();
        // Do Nothing.
        endStartupPass2();

    }
    protected void beginStartupPass2() {
        // Make sure we are in the proper state.
        if (!state.equals(State.Inaccessible_StartedUpPass1)) {
            String s = instanceName + ": Cannot execute this request - Automatic Array Component must be Inaccessible and StartedUpPass1.";
            logger.severe(s);
            throw new IllegalStateException (s);
        }
        state.setState(State.Inaccessible_StartingUpPass2);
    }
    protected void endStartupPass2() {
        if (state.getBadResource().length == 0)
            state.setState(State.Operational_NoError);
        else
            state.setState(State.Operational_Error);
        info("Component is operational.");        
    }

    /**
     * Wait until all current tasks are complete and then
     * execute pass 1 of the shutdown procedure.
     */
    public void waitShutdownPass1() {
        // Make sure we are in the proper state.
        if (!(state.isOperational())) {
            String s = instanceName + ": Cannot execute this request - Automatic Array Component is not operational.";
            logger.severe(s);
            throw new IllegalStateException (s);            
        }
        shutdownPass1();
    }

    /**
     * Pass 1 of the shutdown procedure.
     */
    public void shutdownPass1() {
        beginShutdownPass1();
        // Do nothing.
        endShutdownPass1();
    }
    protected void beginShutdownPass1() {
        // Make sure we are in the proper state.
        if (!(state.isOperational())) {
            String s = instanceName + ": Cannot execute this request - Automatic Array Component is not operational.";
            logger.severe(s);
            throw new IllegalStateException (s);            
        }
        state.setState(State.Inaccessible_ShuttingDownPass1);
    }
    protected void endShutdownPass1() {
        state.setState(State.Inaccessible_ShutDownPass1);
    }

    /**
     * Pass 2 of the shutdown procedure.
     */
    public void shutdownPass2() {
        beginShutdownPass2();
        // Do nothing.
        endShutdownPass2();
    }
    protected void beginShutdownPass2() {
        // Make sure we are in the proper state.
        if (!state.equals(State.Inaccessible_ShutDownPass1)) {
            String s = instanceName + ": Cannot execute this request - Automatic Array Component must be Inaccessible and ShutDownPass1.";
            logger.severe(s);
            throw new IllegalStateException (s);
        }
        state.setState(State.Inaccessible_ShuttingDownPass2);
    }
    protected void endShutdownPass2() {
        state.setState(State.Inaccessible_Stopped);
    }
    
    //////////////////////////////////////////////////////
    // Lifecycle Methods								//
    //////////////////////////////////////////////////////
    
    public void aboutToAbort() {
        cleanUp();
    }
    
    public void cleanUp() {
        componentState = ComponentStates.COMPSTATE_ABORTING;
        if (!state.equals(State.Inaccessible_Stopped)) {
            if (state.equals(State.Operational_Error) || 
                state.equals(State.Operational_NoError) ||
                state.equals(State.Inaccessible_StartedUpPass1) ||
                state.equals(State.Inaccessible_Waiting)) {
                shutdownPass1();
                shutdownPass2();
            }
            // This only leaves the transient states: starting up and shutting down.
            // But these are not interruptible.
        }
        componentState = ComponentStates.COMPSTATE_DEFUNCT;
    }
    
    public void execute() throws ComponentLifecycleException {
        componentState = ComponentStates.COMPSTATE_OPERATIONAL;
    }
    
    public void initialize(ContainerServices cs) throws ComponentLifecycleException {
        componentState = ComponentStates.COMPSTATE_INITIALIZING;
        setContainer(cs);
        setName(container.getName());
        setLogger(container.getLogger());
        componentState = ComponentStates.COMPSTATE_INITIALIZED;
    }
    
    public ComponentStates componentState() {
        return componentState;
    }

    //////////////////////////////////////////////////////
    // Resource Mangement								//
    //////////////////////////////////////////////////////

    protected void addResource(String resourceName, ResourceType resourceType, String state, String substate, Object remoteObject) {
        Resource2 x = getResource(resourceName);
        if (x != null)
            throw new IllegalArgumentException("The resource " + resourceName + " is already in the list of resources.");
        Resource2 r = new Resource2(resourceName, resourceType, state, substate,remoteObject);
        resource.add(r);
    }
    
    protected void removeResource(String resourceName) {
        // resourceName must be in the list.
        Resource2 x = getResource(resourceName);
        if (x == null)
            throw new IllegalArgumentException("The resource " + resourceName + " is not in the list of resources.");
        resource.remove(x);
    }
    
    protected void clearResource() {
        resource.clear();
    }
    
    protected Resource2 getResource(String resourceName) {
        // Find resourceName in the list and return the item in the list.
        Resource2 x = null;
        Iterator iter = resource.iterator();
        while (iter.hasNext()) {
            x = (Resource2)iter.next();
            if (x.getResourceName().equals(resourceName))
                return x;
        }
        return null;
    }
    
    protected Resource2[] getResources() {
        Resource2[] tmp = new Resource2 [resource.size()];
        if (resource.size() > 0) {
            tmp = (Resource2[])resource.toArray(tmp);
        }
        return tmp;
   }
    
   /**
    * Report an error in the Control system to the Error Analyzer.
    * @param resourceName The name of the specific component causing the error.
    * @param resourceType The type of resource causing the error.
    * @param errorType The type of error.
    * @param time The ACS time at whihc the error occurred.
    * @param msg A text message identifying the error.
    * @return Appropriate action is taken and a Result object returned, that
    * can be used to determine whether to stop or continue.
    */
    public Result reportError(String resourceName, 
            ResourceType resourceType, int errorType, long time, String msg) {
        Error err = new Error(resourceName,resourceType,errorType,time,msg);
        return analyzer.takeAction(err);
    }
    
    public void reportStateChange (String resourceName, String state, String substate) {
        Resource2 s = getResource(resourceName);
        if (s == null)
            throw new IllegalArgumentException("The resource " + resourceName + " is not in the list of resources.");
        s.setState(state);
        s.setSubstate(substate);
    }

    /**
     * Report an error in a subordinate remote object to the Error Analyzer in this Array component.
     * @param resourceName The name of the specific component causing the error.
     * @param resourceType The type of resource causing the error.
     * @param errorType The type of error.
     * @param time The ACS time at whihc the error occurred.
     * @param msg A text message identifying the error.
     * @return Appropriate action is taken and a Result object returned, that
     * can be used to determine whether to stop or continue.     */
    public IDLResult reportRemoteError(String resourceName, ResourceEnum resourceType,
            ErrorEnum errorType, long time, String msg) {
        // TODO
        return null;
    }
        
    /**
     * @see alma.Control.ControllerOperations#reportStateChange(java.lang.String, alma.Control.ResourceEnum, alma.Control.SystemState, alma.Control.SystemSubstate)
     */
    public void reportRemoteStateChange(String resourceName, ResourceEnum resourceType,
            SystemState newState, SystemSubstate newSubstate) {
        // TODO Auto-generated method stub
    }

    //////////////////////////////////////////////////////
    // Utilities										//
    //////////////////////////////////////////////////////

    /**
     * Write a complex entry into the log.  The "logln" method
     * adds a newline to the message and accumulates it in a buffer.
     * The buffer is written to the log by the "log" method. 
     */
    private static final String newLine = System.getProperty("line.separator");
    private static final String banner = "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||";  
    private StringBuffer logLine = new StringBuffer ().append(newLine + banner + newLine);
    public void log(String s) {
        logln(s);
        logLine.append(banner + newLine);
        logger.fine(logLine.toString());
        logLine = new StringBuffer ().append(newLine + banner + newLine);
    }
    public void logln(String s) {
        logLine.append("||" + instanceName + ": " + s + newLine);
    }

    /**
     * Write an information message to the log.
     * @param msg The message to be written.
     */
    public void info(String msg) {
        logger.fine(instanceName + ": " + msg);
    }
    
    /**
     * Check whether this component is operational or not.  This method is usually
     * used within public, external commands used by clients to avoid attempting
     * to satisfy requests when the underlying object has not been completely 
     * intialized.
     * @throws InvalidRequest Thrown if this component is not operational.
     */
    protected void checkOperational() throws InaccessibleException {
        if (!state.isOperational()) {
            String s = instanceName + ": Cannot execute this request - component is not operational.";
            logger.severe(s);
            throw new InaccessibleException (getName(), 
                    SystemSubstate.from_int(getState().getSubstateValue()), s);
        }
    }

    //////////////////////////////////////////////////////
    // Time Management.									//
    //////////////////////////////////////////////////////
    
    /**
     * Access the Master Clock.
     * @return Result
     */
    protected Result accessMasterClock() {
        try {
            clock = alma.Control.TimeSourceHelper.narrow(
                    container.getComponent(Name.MasterClockComponent));
            info("Master Clock created.");
            return Result.Success_Continue;
        } catch (AcsJContainerServicesEx err) {
            return reportError(Name.MasterClockComponent,ResourceType.MasterClock,Error.Create,getACSTime(),
                    "Error creating Control Master Clock: " + err.toString());
        } catch (Exception err) {
            return reportError(Name.MasterClockComponent,ResourceType.MasterClock,Error.Create,getACSTime(),
                    "Error creating Control Master clock: " + err.toString());
        }
    }
    
    /**
     * Release the Master Clock
     * @return Result
     */
    protected Result releaseMasterClock() {
        if (container != null && clock != null) {
             container.releaseComponent(Name.MasterClockComponent);
             clock = null;
        }
        return Result.Success_Continue;
    }
        
    /**
     * Get the current time as array time.
     * @return ArrrayTime as a long.
     */
    public long getArrayTime() {
        return Util.getArrayTime().get();
    }
    
    /**
     * Get the current time as ACS time.
     * @return ACSTime as a long.
     */
    public long getACSTime() {
        return Util.arrayTimeToACSTime(Util.getArrayTime().get());
    }

    //////////////////////////////////////////////////////
	// Routine getter methods							//
	//////////////////////////////////////////////////////
	
    /**
     * @return Returns the analyzer.
     */
    public ErrorAnalyzer getAnalyzer() {
        return analyzer;
    }
    /**
     * @return Returns the clock.
     */
    public TimeSource getClock() {
        return clock;
    }
    /**
     * @return Returns the container.
     */
    public ContainerServices getContainer() {
        return container;
    }
    /**
     * @return Returns the logger.
     */
    public Logger getLogger() {
        return logger;
    }
    /**
     * @return Returns the parent.
     */
    public ControllerImpl getParent() {
        return parent;
    }
    
    public String getParentName() {
        return parentName;
    }
    
    /**
     * @return Returns the state.
     */
    public State getState() {
        return state;
    }
    
    /**
     * @return Returns the resourceType.
     */
    public ResourceType getResourceType() {
        return resourceType;
    }
    
    public String getName() {
        return instanceName;
    }

    /* 
     * @see alma.Control.ControllerOperations#getComponentName()
     */
    public String getComponentName() {
        return instanceName;
    }

    public String getResourceName() {
    	return this.resourceName;
    }

    /* 
     * @see alma.ACS.ACSComponentOperations#name()
     */
    public String name() {
        return instanceName;
    }

   /* 
     * @see alma.Control.ControllerOperations#getResourceTypeValue()
     */
    public int getResourceTypeValue() {
        return resourceType.getValue();
    }

    /* 
     * @see alma.Control.ControllerOperations#getStateValue()
     */
    public int getStateValue() {
        return state.getStateValue();
    }

    /* 
     * @see alma.Control.ControllerOperations#getSubstateValue()
     */
    public int getSubstateValue() {
        return state.getSubstateValue();
    }

    /**
     * @param parentName The name of the parent to set.
     */
    public void setParentName(String parentName) {
        this.parentName = parentName;
    }
    
    /* 
     * @see alma.Control.ControllerOperations#getBadResource()
     */
    public IDLError[] getBadResourcesIDL() {
        Error[] err = state.getBadResource();
        IDLError[] x = new IDLError [err.length];
        for (int i = 0; i < x.length; ++i) {
            x[i] = err[i].toIDL();
        }
        return x;
    }

}   

