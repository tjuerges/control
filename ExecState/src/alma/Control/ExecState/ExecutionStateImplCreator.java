/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ExecutionStateComponentCreator.java
 */
package alma.Control.ExecState;

import java.util.logging.Logger;

import alma.acs.component.ComponentLifecycle;
import alma.acs.container.ComponentHelper;
import alma.Control.ExecutionStateOperations;
import alma.Control.ExecutionStatePOATie;

/**
 * Description
 *
 * @version 1.0 Jul 18, 2005
 * @author Allen Farris
 */
public class ExecutionStateImplCreator extends ComponentHelper {

    /**
     * Provide the interface necessary to create an Execution State component.
     */
    public ExecutionStateImplCreator(Logger containerLogger) {
        super(containerLogger);
    }

    /**
     * @see alma.acs.container.ComponentHelper#_createComponentImpl()
     */
    protected ComponentLifecycle _createComponentImpl() {
        return new ExecutionStateImpl();
    }

    /**
     * @see alma.acs.container.ComponentHelper#_getPOATieClass()
     */
    protected Class _getPOATieClass() {
        return ExecutionStatePOATie.class;
    }

    /**
     * @see alma.acs.container.ComponentHelper#_getOperationsInterface()
     */
    protected Class _getOperationsInterface() {
        return ExecutionStateOperations.class;
    }

}
