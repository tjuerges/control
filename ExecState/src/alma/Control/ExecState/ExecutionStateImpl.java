// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2005 - 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

package alma.Control.ExecState;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

import alma.ACSErr.ErrorTrace;
import alma.Control.ExecBlockEndedEvent;
import alma.Control.ExecutionStateOperations;
import alma.Control.ExecutionStateUpdateable;
import alma.Control.ExecutionStateUpdateableHelper;
import alma.Control.SBExecState;
import alma.Control.ScanEndedEvent;
import alma.Control.ScanStartedEvent;
import alma.Control.SubscanStartedEvent;
import alma.Control.Common.Name;
import alma.Control.Common.Util;
import alma.ExecStateExceptions.NoCalibrationResultEx;
import alma.ExecStateExceptions.wrappers.AcsJNoCalibrationResultEx;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.TelCalErrType.NoSuchResultEx;
import alma.telcal.CHANNELNAME_TELCALPUBLISHER;
import alma.telcal.CompletionEnum;
import alma.telcal.FocusReducedEvent;
import alma.telcal.GetTelCalResults;
import alma.telcal.GetTelCalResultsHelper;
import alma.telcal.PhaseCalReducedEvent;
import alma.telcal.PointingReducedEvent;
import alma.telcal.WVRReducedEvent;
import alma.acs.container.ContainerServices;
import alma.acs.nc.CorbaNotificationChannel;
import alma.acs.nc.Receiver;
import alma.asdmIDL.ASDMDataSetIDL;
import alma.offline.ASDMArchivedEvent;
import alma.offline.DataCapturerId;
import alma.offline.DataCapturerStartedEvent;

/**
 * The Execution State component maintains information about
 * the state of execution of a scheduling block and provides 
 * that information upon request.
 * 
 * Basically the Execution State component has two main functions:
 * 
 * 1. It listens for various events, such as calibration results
 *        weather conditions, and when data capture is ready.
 * 2. It makes information on the state of the execution available
 *        to any client on request.
 * 
 * One of the important clients of the execution state component is
 * the script executor that is attached to the same Automatic Array
 * component as this Execution State component.  In particular, it
 * uses the getTelCalResult methods and the getWeatherData methods.
 * <p>
 * Note:  For this implementation, the only event the ExecutionStateComponent
 * receives is the "Data Capture is ready" event.
 *
 * @version 1.0 Jul 18, 2005
 * @author Allen Farris
 */
public class ExecutionStateImpl extends ControllerImpl 
    implements ExecutionStateOperations {

    private Receiver controlReceiverChannel;
    private Receiver telcalReceiverChannel;
    private ExecutionStateUpdateable array;
    private GetTelCalResults telCalResults;
    private ScanStartedEvent scanStartedEvent;
    private ScanEndedEvent scanEndedEvent;
    
    private boolean execBlkReceived;
    private boolean asdmArchived;
    private ExecBlockEndedEvent execBlockEndedEvent;
    private ASDMArchivedEvent asdmArchivedEvent;

    private SBExecState executionState = SBExecState.SB_IDLE;
    private DataCapturerId currentDataCapturer;
    
    private PhaseCalReducedEvent phaseCalReducedEvent;
    private ASDMDataSetIDL phaseCalResults;
    private Lock phaseCalLock = new ReentrantLock();
    private Condition phaseCalCV = phaseCalLock.newCondition();
    
    private PointingReducedEvent pointingReducedEvent;
    private ASDMDataSetIDL pointingResults;
    private Lock pointingLock = new ReentrantLock();
    private Condition pointingCV = pointingLock.newCondition();

    private FocusReducedEvent focusReducedEvent;
    private ASDMDataSetIDL focusResults;
    private Lock focusLock = new ReentrantLock();
    private Condition focusCV = focusLock.newCondition();

    private WVRReducedEvent wvrReducedEvent;
    private ASDMDataSetIDL wvrResults;
    private Lock wvrLock = new ReentrantLock();
    private Condition wvrCV = wvrLock.newCondition();
    
    public ExecutionStateImpl(ContainerServices container, Logger logger,
            String parentName, ResourceType resourceType, String name) {
        super(container, logger, parentName, resourceType, name);
    }

    public ExecutionStateImpl() {
        super();
    }

    public void setContainer(ContainerServices container) {
        super.setContainer(container);
    }
    
    public void setLogger(Logger logger) {
        super.setLogger(logger);
    }

    ////////////////////////////////////////////////////////////////////
    // Lifecycle methods
    //
    ////////////////////////////////////////////////////////////////////
    
    public void startupPass1() {
        logger.fine("startupPass1 called");
        state.setState(State.Inaccessible_Stopped);
        beginStartupPass1();
        
        try {
            accessAutomaticArray();
        } catch (AcsJContainerServicesEx ex) {
            String msg = "Error accessing parent array component:\n" +
                Util.getNiceErrorTraceString(ex.getErrorTrace());
            logger.severe(msg);
        } catch (Exception ex) {
            String msg = "Error accessing parent array component: " + ex;
            logger.severe(msg);
        }
        
        try {
            accessTelCalResults();
        } catch (AcsJContainerServicesEx ex) {
            String msg = "Error accessing GetTelcalResults component:\n" +
                Util.getNiceErrorTraceString(ex.getErrorTrace());
            logger.severe(msg);
        }
        
        // Create the receiver channel.
        controlReceiverChannel = CorbaNotificationChannel.getCorbaReceiver(Name.NCExternal, container);
        logger.fine("CONTROL: Accessed " + Name.NCExternal + " notification channel.");
        controlReceiverChannel.attach("alma.offline.DataCapturerStartedEvent", this);
        controlReceiverChannel.attach("alma.Control.ExecBlockEndedEvent", this);
        controlReceiverChannel.attach("alma.offline.ASDMArchivedEvent", this);
        controlReceiverChannel.attach("alma.Control.SubscanStartedEvent", this);
        controlReceiverChannel.attach("alma.Control.ScanStartedEvent", this);
        controlReceiverChannel.attach("alma.Control.ScanEndedEvent", this);
        controlReceiverChannel.begin();
        
        telcalReceiverChannel = CorbaNotificationChannel.getCorbaReceiver(
                CHANNELNAME_TELCALPUBLISHER.value,
                container);
        telcalReceiverChannel.attach(
            "alma.telcal.PhaseCalReducedEvent", this);
        telcalReceiverChannel.attach(
            "alma.telcal.PointingReducedEvent", this);
        telcalReceiverChannel.attach(
            "alma.telcal.FocusReducedEvent", this);
        telcalReceiverChannel.attach(
                "alma.telcal.WVRReducedEvent", this);
        telcalReceiverChannel.begin();
        
        endStartupPass1();
    }

    public void shutdownPass1() {
        phaseCalResults = null;
        phaseCalReducedEvent = null;
        pointingResults = null;
        pointingReducedEvent = null;
        focusResults = null;
        focusReducedEvent = null;
        
        controlReceiverChannel.detach("alma.offline.DataCapturerStartedEvent", this);
        controlReceiverChannel.detach("alma.Control.ExecBlockEndedEvent", this);
        controlReceiverChannel.detach("alma.offline.ASDMArchivedEvent", this);
        controlReceiverChannel.detach("alma.Control.SubscanStartedEvent", this);
        controlReceiverChannel.detach("alma.Control.ScanStartedEvent", this);
        controlReceiverChannel.detach("alma.Control.ScanEndedEvent", this);
        controlReceiverChannel.end();

        telcalReceiverChannel.detach("alma.telcal.PhaseCalReducedEvent", this);
        telcalReceiverChannel.detach("alma.telcal.PointingReducedEvent", this);
        telcalReceiverChannel.detach("alma.telcal.FocusReducedEvent", this);
        telcalReceiverChannel.detach("alma.telcal.WVRReducedEvent", this);
        telcalReceiverChannel.end();
        
        beginShutdownPass1();
        releaseAutomaticArray();        
        releaseTelCalResults();
        endShutdownPass1();
    }
    
    ////////////////////////////////////////////////////////////////////
    // Public interface
    //
    ////////////////////////////////////////////////////////////////////
    
    public ASDMDataSetIDL getPhaseCalResult() throws NoCalibrationResultEx {
        if (phaseCalResults == null) {
            throw new AcsJNoCalibrationResultEx().toNoCalibrationResultEx();
        }
        return phaseCalResults;
    }

    public ASDMDataSetIDL getPointingCalResult() throws NoCalibrationResultEx {
        if (pointingResults == null) {
            throw new AcsJNoCalibrationResultEx().toNoCalibrationResultEx();
        }
        return pointingResults;
    }

    public ASDMDataSetIDL getFocusCalResult() throws NoCalibrationResultEx {
        if (focusResults == null) {
            throw new AcsJNoCalibrationResultEx().toNoCalibrationResultEx();
        }
        return focusResults;
    }

    public ASDMDataSetIDL getWVRCalResult() throws NoCalibrationResultEx {
        if (wvrResults == null) {
            throw new AcsJNoCalibrationResultEx().toNoCalibrationResultEx();
        }
        return wvrResults;
    }

    /**
     * Is phase calibration result available.
     * 
     * If phase cal is available, then this function returns True.
     * If phase cal is not available, then there's two cases: either the phase cal event
     * arrives, and True is returned if the phase cal result was obtained; or the wait
     * expires and False is returned.
     */
    public boolean isPhaseCalAvailable(double timeout) {
        logger.fine("isPhaseCalAvailable called");
        boolean ready = true;
        try {
            phaseCalLock.lock();
            // wait for the PhaseCalReducedEvent to arrive
            while(phaseCalReducedEvent == null) {
                try {
                    logger.fine("Waiting in phaseCal Condition Variable");
                    int timeoutInUs = (int) (timeout*1E6);
                    ready = phaseCalCV.await(timeoutInUs, TimeUnit.MICROSECONDS);
                } catch (InterruptedException ex) {
                    // Most likely the container was shutdown while waiting.
                    // Nothing really to be done in the case.
                    logger.severe("Thread waiting for phase cal results have been" +
                            "interrupted.");
                }
                if (!ready) {
                    logger.info("phaseCal Condition Variable wait timed out.");
                    break;
                }
            }
        } finally {
            phaseCalLock.unlock();
        }
        logger.fine("Returning from isPhaseCalAvailable. Ready is " + ready +
                    "; and phaseCalResults is " + phaseCalResults);
        return ready && (phaseCalResults != null);
    }

    /**
     * Is pointing calibration result available.
     * 
     * If pointing cal is available, then this function returns True.
     * If pointing cal is not available, then there's two cases: either the pointing cal event
     * arrives, and True is returned if the pointing cal result was obtained; or the wait
     * expires and False is returned.
     */
    public boolean isPointingCalAvailable(double timeout) {
        logger.fine("isPointingCalAvailable called");
        boolean ready = true;
        try {
            pointingLock.lock();
            // wait for the PointingReducedEvent to arrive
            while(pointingReducedEvent == null) {
                try {
                    logger.fine("Waiting in pointing Condition Variable");
                    int timeoutInUs = (int) (timeout*1E6);
                    ready = pointingCV.await(timeoutInUs, TimeUnit.MICROSECONDS);
                } catch (InterruptedException ex) {
                    // Most likely the container was shutdown while waiting.
                    // Nothing really to be done in the case.
                    logger.severe("Thread waiting for pointing cal results have been" +
                            "interrupted.");
                }
                if (!ready) {
                    logger.info("Pointing Condition Variable wait timed out.");
                    break;
                }
            }
        } finally {
            pointingLock.unlock();
        }
        logger.fine("Returning from isPointingCalAvailable. Ready is " + ready +
                    "; and pointingResults is " + pointingResults);
        return ready && (pointingResults != null);
    }

    /**
     * Is focus calibration result available.
     * 
     * If focus cal is available, then this function returns True.
     * If focus cal is not available, then there's two cases: either the focus cal event
     * arrives, and True is returned if the focus cal result was obtained; or the wait
     * expires and False is returned.
     */
    public boolean isFocusCalAvailable(double timeout) {
        logger.fine("isFocusCalAvailable called");
        boolean ready = true;
        try {
            focusLock.lock();
            // wait for the PointingReducedEvent to arrive
            while(focusReducedEvent == null) {
                try {
                    logger.fine("Waiting in focus Condition Variable");
                    int timeoutInUs = (int) (timeout*1E6);
                    ready = focusCV.await(timeoutInUs, TimeUnit.MICROSECONDS);
                } catch (InterruptedException ex) {
                    // Most likely the container was shutdown while waiting.
                    // Nothing really to be done in the case.
                    logger.severe("Thread waiting for focus cal results have been" +
                            "interrupted.");
                }
                if (!ready) {
                    logger.info("Focus Condition Variable wait timed out.");
                    break;
                }
            }
        } finally {
            focusLock.unlock();
        }
        logger.fine("Returning from isFocusCalAvailable. Ready is " + ready +
                    "; and focusResults is " + focusResults);
        return ready && (focusResults != null);
    }

    /**
     * Is WVR calibration result available.
     * 
     * If WVR cal is available, then this function returns True.
     * If WVR cal is not available, then there's two cases: either the WVR cal event
     * arrives, and True is returned if the WVR cal result was obtained; or the wait
     * expires and False is returned.
     */
    public boolean isWVRCalAvailable(double timeout) {
        logger.fine("isWVRCalAvailable called");
        boolean ready = true;
        try {
            wvrLock.lock();
            // wait for the PointingReducedEvent to arrive
            while(wvrReducedEvent == null) {
                try {
                    logger.fine("Waiting in WVR Condition Variable");
                    int timeoutInUs = (int) (timeout*1E6);
                    ready = wvrCV.await(timeoutInUs, TimeUnit.MICROSECONDS);
                } catch (InterruptedException ex) {
                    // Most likely the container was shutdown while waiting.
                    // Nothing really to be done in the case.
                    logger.severe("Thread waiting for WVR cal results have been" +
                            "interrupted.");
                }
                if (!ready) {
                    logger.info("WVR Condition Variable wait timed out.");
                    break;
                }
            }
        } finally {
            wvrLock.unlock();
        }
        logger.fine("Returning from isWVRCalAvailable. Ready is " + ready +
                    "; and wvrResults is " + wvrResults);
        return ready && (wvrResults != null);
    }

    
    public String getWeather() {
        return null;
    }
    
    public SBExecState getExecState() {
        return executionState;
    }
    
    /**
     * Get the last scheduling block execution UID.
     * 
     * @return Last execution UID
     */
    public String getLastExecUID() {
        logger.fine("execBlockEndedEvent.execId.entityId = " + execBlockEndedEvent.execId.entityId);
        if ( (executionState == SBExecState.SB_DONE) || (executionState == SBExecState.SB_ARCHIVING) )
            return execBlockEndedEvent.execId.entityId;
        else
            return null;
    }

    public ErrorTrace[] getLastError() {
        if (execBlockEndedEvent != null)
            return execBlockEndedEvent.errorTrace;
        else
            return new ErrorTrace[0];
    }

    ////////////////////////////////////////////////////////////////////
    // Event reception methods
    //
    ////////////////////////////////////////////////////////////////////
    
    public void receive(DataCapturerStartedEvent e) {
        if (isThisMyDataCapturer(e.workingDCId)) {
            logger.fine("ExecutionState: Got alma.offline.DataCapturerStartedEvent for array "
                    + parentName);
            executionState = SBExecState.SB_RUNNING;
            array.dataCaptureReady();
        }
    }
    
    public void receive(SubscanStartedEvent event) {
        if (event.arrayName.equals(parentName.replaceFirst("CONTROL/", ""))) {
            logger.fine("ExecutionState: Got alma.Control.SubscanStartedEvent for array "
                    + parentName);
        }
    }
    
    /**
     * Receiver callback function for the ExecBlockEndedEvent.
     * 
     * The ExecutionState listen to the Control external notification channel
     * (CONTROL_SYSTEM).
     * It knows that an observation has completed when receives the
     * ExecBlockEndedEvent.
     * @param execBlkEndEvent Execution block ended event
     */
    public synchronized void receive(ExecBlockEndedEvent execBlkEndEvent) {
        if (execBlkEndEvent.arrayName.equals(parentName.replaceFirst("CONTROL/", ""))) {
            logger.info("Received ExecBlockEndedEvent for array " + parentName);
            execBlkReceived = true;
            execBlockEndedEvent = execBlkEndEvent;
        
            if (execBlkEndEvent.status.equals(alma.Control.Completion.FAIL)) {
                executionState = SBExecState.SB_ERROR;
                return;
            }
                    
            if (asdmArchived)
                executionState = SBExecState.SB_DONE;
            else
                executionState = SBExecState.SB_ARCHIVING;
        }
    }    

    /**
     * Receiver callback function for the ASDMArchivedEvent.
     * 
     * The ExecutionState listen to the CONTROL external notification channel
     * (CONTROL_SYSTEM). It knows that the ASDM has been archived when it
     * receives the ASDMArchivedEvent, which sent by the DataCapturer component.
     * @param asdmArchivedEvent ASDM archived event
     */
    public synchronized void receive(ASDMArchivedEvent asdmArchivedEvent) {
        if (isThisMyDataCapturer(asdmArchivedEvent.workingDCId)) {
            logger.info("Received ASDMArchivedEvent for array " + parentName);
            asdmArchived = true;
            if (execBlkReceived)
                executionState = SBExecState.SB_DONE;
            array.asdmArchived(asdmArchivedEvent);
        }
    }    

    public void receive(ScanStartedEvent scanStartedEvent) {
        logger.fine("Received ScanStartedEvent for array " + scanStartedEvent.arrayName);
        if (scanStartedEvent.arrayName.equals(parentName.replaceFirst("CONTROL/", ""))) {
            logger.fine("Processing ScanStartedEvent for array " + parentName);
            this.scanStartedEvent = scanStartedEvent;
            logger.fine("Scan number: " + scanStartedEvent.scanNumber);
            logger.fine("Resetting calibration events and results");
            this.phaseCalResults = null;
            this.phaseCalReducedEvent = null;
            this.pointingResults = null;
            this.pointingReducedEvent = null;
            this.focusResults = null;
            this.focusReducedEvent = null;
        }
    }

    public void receive(ScanEndedEvent scanEndedEvent) {
        logger.fine("Received ScanEndedEvent for array " + scanEndedEvent.arrayName);
        if (scanEndedEvent.arrayName.equals(parentName.replaceFirst("CONTROL/", ""))) {
            logger.fine("Processing ScanEndedEvent for array " + parentName);
            this.scanEndedEvent = scanEndedEvent;        
        }
    }
    
    public void receive(PhaseCalReducedEvent phaseCalReducedEvent) {
        logger.fine("Received PhaseCalReducedEvent for ExecBlockID " +
                    phaseCalReducedEvent.execBlockId.entityId);
        try {
            if (scanStartedEvent.execId.entityId.equals(phaseCalReducedEvent.execBlockId.entityId)) {
                logger.fine("Received PhaseCalReducedEvent for array " + parentName);
                try {
                    phaseCalLock.lock();            
                    this.phaseCalReducedEvent = phaseCalReducedEvent;
                    if ((this.phaseCalReducedEvent.completion != CompletionEnum.SUCCESS) &&
                                (this.phaseCalReducedEvent.completion != CompletionEnum.PARTIAL)) {
                        logger.warning("Event was received but calibration was not computed");
                        phaseCalResults  = null;
                    } else {
                            try {
                                phaseCalResults = telCalResults
                                                  .getPhaseCalResult(phaseCalReducedEvent.execBlockId,
                                                          scanStartedEvent.scanNumber);
                            } catch (NoSuchResultEx ex) {
                                String msg = "Error getting Phase Cal results:\n" +
                                    Util.getNiceErrorTraceString(ex.errorTrace);
                                logger.severe(msg);
                                phaseCalResults = null;
                            }
                    }
                    logger.fine("Signaling phaseCal Condition Variable");                       
                    phaseCalCV.signal();
                } finally {
                        phaseCalLock.unlock();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void receive(PointingReducedEvent pointingReducedEvent) {
        logger.fine("Received PointingReducedEvent for ExecBlockID " +
                    pointingReducedEvent.execBlockId.entityId);
        try {
            if (scanStartedEvent.execId.entityId.equals(pointingReducedEvent.execBlockId.entityId)) {
                logger.fine("Received PointingReducedEvent for array " + parentName);
                try {
                    pointingLock.lock();            
                    this.pointingReducedEvent = pointingReducedEvent;
                    if ((this.pointingReducedEvent.completion != CompletionEnum.SUCCESS) &&
                                (this.pointingReducedEvent.completion != CompletionEnum.PARTIAL)) {
                        logger.warning("Event was received but calibration was not computed");
                        pointingResults  = null;
                    } else {    
                            try {
                                pointingResults = telCalResults
                                                  .getPointingResult(pointingReducedEvent.execBlockId,
                                                                     scanStartedEvent.scanNumber);
                            } catch (NoSuchResultEx ex) {
                                String msg = "Error getting Pointing Cal results:\n" +
                                    Util.getNiceErrorTraceString(ex.errorTrace);
                                logger.severe(msg);
                                pointingResults = null;
                            }
                    }
                    logger.fine("Signaling pointing Condition Variable");
                    pointingCV.signal();
                } finally {
                    pointingLock.unlock();            
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void receive(FocusReducedEvent focusReducedEvent) {
        logger.fine("Received FocusReducedEvent for ExecBlockID " +
                    focusReducedEvent.execBlockId.entityId);
        try {
            if (scanStartedEvent.execId.entityId.equals(focusReducedEvent.execBlockId.entityId)) {
                logger.fine("Received FocusReducedEvent for array " + parentName);
                try {
                    focusLock.lock();            
                    this.focusReducedEvent = focusReducedEvent;
                    if ((this.focusReducedEvent.completion != CompletionEnum.SUCCESS) &&
                                (this.focusReducedEvent.completion != CompletionEnum.PARTIAL)) {
                        logger.warning("Event was received but calibration was not computed");
                        focusResults  = null;
                    } else {    
                                try {
                                focusResults = telCalResults
                                               .getFocusResult(focusReducedEvent.execBlockId,
                                                               scanStartedEvent.scanNumber);
                            } catch (NoSuchResultEx ex) {
                                String msg = "Error getting Focus Cal results:\n" +
                                    Util.getNiceErrorTraceString(ex.errorTrace);
                                logger.severe(msg);
                                focusResults = null;
                            }
                    }
                    logger.fine("Signaling pointing Condition Variable");
                    focusCV.signal();
                } finally {
                    focusLock.unlock();            
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void receive(WVRReducedEvent wvrReducedEvent) {
        logger.fine("Received WVRReducedEvent for ExecBlockID " +
                wvrReducedEvent.execBlockId.entityId);
            try {
                if (scanStartedEvent.execId.entityId.equals(wvrReducedEvent.execBlockId.entityId)) {
                    logger.fine("Received WVRReducedEvent for array " + parentName);
                    try {
                        wvrLock.lock();            
                        this.wvrReducedEvent = wvrReducedEvent;
                        if ((this.wvrReducedEvent.completion != CompletionEnum.SUCCESS) &&
                                (this.wvrReducedEvent.completion != CompletionEnum.PARTIAL)) {
                            logger.warning("Event was received but calibration was not computed");
                            wvrResults  = null;
                        } else {    
                                try {
                                wvrResults = telCalResults
                                             .getWVRResult(wvrReducedEvent.execBlockId,
                                                           scanStartedEvent.scanNumber);
                            } catch (NoSuchResultEx ex) {
                                String msg = "Error getting WVR Cal results:\n" +
                                    Util.getNiceErrorTraceString(ex.errorTrace);
                                logger.severe(msg);
                                wvrResults = null;
                            }
                        }
                        logger.fine("Signaling WVR Condition Variable");
                        wvrCV.signal();
                    } finally {
                        wvrLock.unlock();            
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        
    }
    
    ////////////////////////////////////////////////////////////////////
    // Private methods
    //
    ////////////////////////////////////////////////////////////////////
    
    private void accessAutomaticArray() throws AcsJContainerServicesEx {
        if (parentName == null) {
            throw new NullPointerException("Parent name is null");
        }
        array = ExecutionStateUpdateableHelper.narrow(container.getComponent(parentName));
    }
    
    private void releaseAutomaticArray() {
        if (container != null && array != null) {
            container.releaseComponent(parentName);
            parentName = null;
            array = null;
        }
    }

    private void accessTelCalResults() throws AcsJContainerServicesEx {
        telCalResults = GetTelCalResultsHelper
                        .narrow(container
                        .getDefaultComponent("IDL:alma/telcal/GetTelCalResults:1.0"));
    }
    
    private void releaseTelCalResults() {
        if (telCalResults != null)
            container.releaseComponent(telCalResults.name());
        telCalResults = null;
    }
    
    private boolean isThisMyDataCapturer(DataCapturerId dcId) {
        logger.fine("parentName="+parentName+"; dcId.array="+dcId.array);
        return parentName.replaceFirst("CONTROL/", "").equals(dcId.array);
    }
}
