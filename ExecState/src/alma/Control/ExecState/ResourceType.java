/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ResourceType.java
 */
package alma.Control.ExecState;

import alma.Control.ResourceEnum;

/**
 * The ResourceType class contains an enumeration of the
 * types of resources within the Control system.
 *
 * @version 1.0 Mar 31, 2005
 * @author Allen Farris
 */
public class ResourceType {
    
    // NOTE:
    //		Other types of resources will be added to this list
    //		as they are needed: for example,
    //				Photonics LO
    //				ARTM
    //				network links to the high site
    
    public static final ResourceType Archive 				= new ResourceType(0);
    public static final ResourceType ACS 					= new ResourceType(1);
    public static final ResourceType NChannel 				= new ResourceType(2);
    public static final ResourceType DeviceMonitor 			= new ResourceType(3);
    public static final ResourceType MasterClock 			= new ResourceType(4);
    public static final ResourceType WeatherStation 		= new ResourceType(5);
    public static final ResourceType Antenna 				= new ResourceType(6);
    public static final ResourceType Correlator 			= new ResourceType(7);
    public static final ResourceType Operator	 			= new ResourceType(8);
    public static final ResourceType AutomaticArray 		= new ResourceType(9);
    public static final ResourceType ManualArray 			= new ResourceType(10);
    public static final ResourceType Master					= new ResourceType(11);
    public static final ResourceType DataCapture			= new ResourceType(12);
    public static final ResourceType ScriptExecutor			= new ResourceType(13);
    public static final ResourceType ExecutionState			= new ResourceType(14);
    public static final ResourceType DelayServer			= new ResourceType(15);
    public static final ResourceType TMCDB					= new ResourceType(16);
    // Resource types
    private static final String[] resourceName = {
            "Archive",
            "ACS",
            "NChannel",
            "DeviceMonitor",
            "MasterClock",
            "WeatherStation",
            "Antenna",
            "Correlator",
            "Operator",
            "AutomaticArray",
            "ManualArray",
            "Master",
            "DataCapture",
            "ScriptExecutor",
            "ExecutionState",
            "DelayServer",
            "TMCDB"
    };
    // Resources
    private static final ResourceType[] resources = {
            Archive,
            ACS,
            NChannel,
            DeviceMonitor,
            MasterClock,
            WeatherStation,
            Antenna,
            Correlator,
            Operator,
            AutomaticArray,
            ManualArray,
            Master,
            DataCapture,
            ScriptExecutor,
            ExecutionState,
            DelayServer,
            TMCDB
    };
    
    private int value;
    
    private ResourceType(int value) {
        this.value = value;
    }

    public ResourceType(ResourceEnum t) {
        value = t.value();
    }
    
    /**
     * Return the name of the resource type as a string. 
     */
    public String toString() {
        return resourceName[value];
    }
    
    public ResourceEnum toIDL() {
        return ResourceEnum.from_int(value);
    }
    
    /**
     * Return the value of this resource type as an int.
     * @return
     */
    public int getValue() {
        return value;
    }
    
    public static int getResourceTypeValue(String name) {
        for (int i = 0; i < resourceName.length; ++i) {
            if (resourceName[i].equals(name)) {
                return i;
            }
        }
        return -1;
    }

    public static ResourceType getResourceType(int value) {
        if (value < 0 || value >= resourceName.length) {
            return null;
        }
        return resources[value];
    }

}
