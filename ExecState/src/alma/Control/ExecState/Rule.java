/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File Rule.java
 */
package alma.Control.ExecState;

/**
 * The Rule is used by the Error Analyzer.  Internally, the Error Analyzer
 * contains an array of rules to be used to determine the action to be
 * taken in the event of an error.  It specifies:
 * <ul>
 * <li> the state of the controller,
 * <li> the substate of the controller,
 * <li> the type of resource causing the error, and
 * <li> the type of error,
 * followed by a list of names specifying the action to be taken.  A Rule 
 * can be thought of as an If-Then construct:  "If the state is A and the 
 * substate is B and the type of resource is C and the type or error is D,
 * then do X, Y, and Z.  The final name in the action list must be either
 * Stop or Continue and is used to direct the flow of control following the
 * error.
 *
 * @version 1.0 Apr 4, 2005
 * @author Allen Farris
 */
public class Rule {

    private int state;
    private int substate;
    private int resourceType;
    private int errorType;
    private String[] action;
    
    public Rule(int state, int substate,  int resourceType, int errorType, String[] action) {
        this.state = state;
        this.substate = substate;
        this.resourceType = resourceType;
        this.errorType = errorType;
        this.action = action;
    }

    /**
     * @return Returns the action.
     */
    public String[] getAction() {
        return action;
    }
    /**
     * @return Returns the errorType.
     */
    public int getErrorType() {
        return errorType;
    }
    /**
     * @return Returns the resourceType.
     */
    public int getResourceType() {
        return resourceType;
    }
    /**
     * @return Returns the state.
     */
    public int getState() {
        return state;
    }
    /**
     * @return Returns the substate.
     */
    public int getSubstate() {
        return substate;
    }
}
