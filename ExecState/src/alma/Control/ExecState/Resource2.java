/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ResourceState.java
 */
package alma.Control.ExecState;

/**
 * Description
 *
 * @version 1.0 Apr 19, 2005
 * @author Allen Farris
 */
public class Resource2 {

    private String resourceName; 
    private ResourceType resourceType;
    private String state;
    private String substate;
    private Object remoteObject; // The CORBA object implementing this resource.
    
    public Resource2(String resourceName, ResourceType resourceType, 
            String state, String substate, Object remoteObject) {
        this.resourceName = resourceName;
        this.resourceType = resourceType;
        this.state = state;
        this.substate = substate;
        this.remoteObject = remoteObject;
    }
    
    
    /**
     * @return Returns the resourceName.
     */
    public String getResourceName() {
        return resourceName;
    }
    
    /**
     * @return Returns the resourceType.
     */
    public ResourceType getResourceType() {
        return resourceType;
    }
    
    /**
     * @return Returns the state.
     */
    public String getState() {
        return state;
    }
    
    /**
     * @return Returns the substate.
     */
    public String getSubstate() {
        return substate;
    }
    /**
     * @return Returns the remoteObject.
     */
    public Object getRemoteObject() {
        return remoteObject;
    }
    /**
     * @param state The state to set.
     */
    public void setState(String state) {
        this.state = state;
    }
    /**
     * @param substate The substate to set.
     */
    public void setSubstate(String substate) {
        this.substate = substate;
    }
}
