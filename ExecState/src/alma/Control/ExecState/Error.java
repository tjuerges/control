/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File Error.java
 */
package alma.Control.ExecState;

import alma.Control.IDLError;
import alma.Control.ResourceEnum;
import alma.Control.ErrorEnum;
import alma.Control.Common.Util;

/**
 * Description
 *
 * @version 1.0 Mar 31, 2005
 * @author Allen Farris
 */
public class Error {
    
    private static final String[] errName = {
            "Create",
            "Access",
            "Test",
            "Release",
            "Stop",
            "Read",
            "Update",
            "Internal",
            "Publish"
    };
    public static final int Create		= 0;
    public static final int Access		= 1;
    public static final int Test		= 2;
    public static final int Release 	= 3;
    public static final int Stop		= 4;
    public static final int Read 		= 5;
    public static final int Update 		= 6;
    public static final int Internal	= 7;
    public static final int Publish		= 8;

    private String resourceName;
    private ResourceType resourceType;
    private int errorType;
    private long time;
    private String errMsg;
    
    /**
     * Create an Error object.
     * 
     * @param resourceName The specific name of the particular component or device that
     * generated the error, for example, DV13.
     * @param resourceType The type of resource of the particular component or device that
     * generated the error, for example, Resource.Antenna.
     * @param errorType The type of error as identified in the list of types of errors, for
     * example, Error.CreateComponent.
     * @param time The ACS time at which this error was detected.
     * @param errMsg A brief text describing the error.
     */
    public Error(String resourceName, ResourceType resourceType, int errorType, long time, String errMsg) {
        this.resourceName = resourceName;
        this.resourceType = resourceType;
        this.time = time;
        this.errMsg = errMsg;
        switch (errorType) {
        case Create: 			this.errorType = Create; break;
        case Access: 			this.errorType = Access; break;
        case Test:				this.errorType = Test; break;
        case Release: 			this.errorType = Release; break;
        case Stop: 				this.errorType = Stop; break;
        case Read:				this.errorType = Read; break;
        case Update:			this.errorType = Update; break;
        case Internal: 			this.errorType = Internal; break;
        case Publish:			this.errorType = Publish; break;
        default: throw new IllegalArgumentException ("Invalid error type: " + errorType);
        }
    }
    
    public Error(IDLError e) {
        this.resourceName = e.resourceName;
        this.resourceType = ResourceType.getResourceType(e.resourceType.value());
        this.errorType = e.errorType.value();
        this.time = e.time;
        this.errMsg = e.errMsg;
    }

    public IDLError toIDL() {
        return new IDLError (resourceName,
                ResourceEnum.from_int(resourceType.getValue()),
                ErrorEnum.from_int(errorType),
                time, errMsg);
    }
    
    public String toString() {
        return "Resource " + resourceName + " of type " + resourceType +
    		" encountered an error of type " + errorType + " at time " + 
    		Util.acsTimeToString(time) + ".  Details: " + errMsg;
    }
    
    /**
     * @return Returns the errMsg.
     */
    public String getErrMsg() {
        return errMsg;
    }
    /**
     * @return Returns the errorType.
     */
    public int getErrorType() {
        return errorType;
    }
    /**
     * @return Returns the resourceName.
     */
    public String getResourceName() {
        return resourceName;
    }
    /**
     * @return Returns the resourceType.
     */
    public ResourceType getResourceType() {
        return resourceType;
    }
    /**
     * @return Returns the time.
     */
    public long getTime() {
        return time;
    }
    
    public static int getErrValue(String name) {
        for (int i = 0; i < errName.length; ++i) {
            if (errName[i].equals(name)) {
                return i;
            }
        }
        return -1;
    }
}
