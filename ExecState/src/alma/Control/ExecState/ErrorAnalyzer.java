/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File GenericErrorAnalyzer.java
 */
package alma.Control.ExecState;

import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.NoSuchElementException;
import java.util.logging.Logger;
import java.lang.reflect.*;

/**
 * Description
 *
 * @version 1.0 Apr 4, 2005
 * @author Allen Farris
 */
public abstract class ErrorAnalyzer {

    protected static Rule[] rule = null;
    protected static String[] defaultAction = null;

	protected static Rule[] parseRule(String[] text) {
	    ArrayList ruleList = new ArrayList ();
	    Rule rule = null;
	    // The syntax of a rule is: 
	    // "state substate resourceType errorType -> Action1 Action2 ...", or
	    // "Default -> Action1 Action2 ...".  
	    String stateName = null;
	    String substateName = null;
	    String resourceTypeName = null;
	    String errorTypeName = null;
	    int state = 0;
	    int substate = 0;
	    int resource = 0;
	    int error = 0;
	    String line = null;
	    StringTokenizer token = null;
	    String s = null;
	    ArrayList list = null;
	    String[] action = null;
	    for (int i = 0; i < text.length; ++i) {
	        line = text[i];
	        token = new StringTokenizer(line);
	        try {
	            s = token.nextToken();
	            if (s.equals("Default")) {
		    	    s = token.nextToken();
		    	    if (!s.equals("->"))
		                throw new IllegalArgumentException ("Invalid syntax in error rule: " + line);
		    	    list = new ArrayList ();
		    	    while (token.hasMoreTokens()) {
		    	        s = token.nextToken();
		    	        list.add(s);
		    	    }
		    	    action = new String [list.size()];
		    	    defaultAction = (String[])list.toArray(action);
		    	    continue;
	            }
	            stateName = s;
	            substateName = token.nextToken();
	            resourceTypeName = token.nextToken();
	            errorTypeName = token.nextToken();
	            state = State.getStateValue(stateName);
	            substate = State.getSubstateValue(substateName);
	            resource = ResourceType.getResourceTypeValue(resourceTypeName);
	            error = Error.getErrValue(errorTypeName);
	            if (state == -1 || substate == -1 || resource == -1 || error == -1)
	                throw new IllegalArgumentException ("Invalid syntax in error rule: " + line);
	    	    s = token.nextToken();
	    	    if (!s.equals("->"))
	                throw new IllegalArgumentException ("Invalid syntax in error rule: " + line);
	    	    list = new ArrayList ();
	    	    while (token.hasMoreTokens()) {
	    	        s = token.nextToken();
	    	        list.add(s);
	    	    }
	    	    action = new String [list.size()];
	    	    action = (String[])list.toArray(action);
	        } catch (NoSuchElementException err) {
	            throw new IllegalArgumentException ("Invalid syntax in error rules.");
	        }
	        rule = new Rule(state,substate,resource,error,action);
	        ruleList.add(rule);
	    }
	    Rule[] rules = new Rule [ruleList.size()];
		return (Rule[])ruleList.toArray(rules);
	}

	protected State state;
	protected Logger logger;
	protected Error error;
	protected ControllerImpl thisController;
	
    protected ErrorAnalyzer(State state, Logger logger) {
        this.state = state;
        this.logger = logger;
    }
    
    protected void setThisController(ControllerImpl controller) {
        this.thisController = controller;
    }

	protected String[] getActionList(Error err) {
        for (int i = 0; i < rule.length; ++i) {
            if (rule[i].getState() == state.getStateValue() &&
                rule[i].getSubstate() == state.getSubstateValue() &&
                rule[i].getResourceType() == err.getResourceType().getValue() &&
                rule[i].getErrorType() == err.getErrorType()) {
                return rule[i].getAction();
            }
        }
        if (defaultAction == null)
            return new String [0];
        return defaultAction;
	}

	private String formatError(Error e) {
	    String s = thisController.getName() + ": An error of type " + 
	    	e.getErrorType() + " occurred at " + e.getTime() + " in " + 
	    	e.getResourceName() + " of type " + e.getResourceType() + 
	    	". Details: " + e.getErrMsg();
	    return s;
	}

    public Result takeAction(Error e) {
        // Save the error.
        error = e;
        // Log the error.
        logger.severe(formatError(e));
        // Get the action list.
        String[] action = getActionList(e);
        // Perform the actions.
        int i = 0;
        for (; i < action.length - 1; ++i) {
            // Use reflection to invoke the action.
            try {
                this.getClass().getMethod(action[i],null).invoke(this,null);
            } catch (SecurityException err) {
                logger.severe("Internal Error in ErrorAnalyzer: " + err.toString());
                return Result.Fail_Stop;
            } catch (NoSuchMethodException err) {
                logger.severe("Internal Error in ErrorAnalyzer: " + err.toString());
                return Result.Fail_Stop;
            } catch (IllegalArgumentException err) {
                logger.severe("Internal Error in ErrorAnalyzer: " + err.toString());
                return Result.Fail_Stop;
            } catch (IllegalAccessException err) {
                logger.severe("Internal Error in ErrorAnalyzer: " + err.toString());
                return Result.Fail_Stop;
            } catch (InvocationTargetException err) {
                logger.severe("Internal Error in ErrorAnalyzer: " + err.toString());
                return Result.Fail_Stop;
            }
        }
        // Return either Stop or Continue.
        if (action[i].equals("Continue"))
        	return Result.Fail_Continue;
        if (action[i].equals("Stop"))
        	return Result.Fail_Stop;
        logger.severe("Internal Error in ErrorAnalyzer: Invalid termination in error analyzer rule (" + 
                action[i] + ")" );
        return Result.Fail_Stop;
    }

}
