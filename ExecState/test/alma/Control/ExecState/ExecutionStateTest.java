/*
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2005 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * $Id$
 */

package alma.Control.ExecState;

import java.util.logging.Logger;

import junit.framework.Test;
import junit.framework.TestSuite;

import alma.ACSSim.Simulator;
import alma.ACSSim.SimulatorHelper;
import alma.Control.ExecutionState;
import alma.Control.ExecutionStateHelper;
import alma.Control.ScanEndedEvent;
import alma.Control.ScanStartedEvent;
import alma.ExecStateExceptions.NoCalibrationResultEx;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.TelCalPublisher.PhaseCalReducedEvent;
import alma.TelCalPublisher.PointingReducedEvent;
import alma.acs.component.ComponentQueryDescriptor;
import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.nc.CorbaPublisher;
import alma.asdmIDLTypes.IDLEntityRef;
import alma.asdmIDL.ASDMDataSetIDL;
import alma.TelCalPublisher.CHANNELNAME_TELCALPUBLISHER;

/**
 * Test the ExecutionState component in-container.
 * 
 */
public class ExecutionStateTest extends ComponentClientTestCase {

    private Logger logger;
    private ExecutionState execState;
    protected Simulator simulator;
    
    public ExecutionStateTest() throws Exception {
        super(ExecutionStateTest.class.getName());
    }

    public ExecutionStateTest(String test) throws Exception {
        super(test);
    }
    /**
     * Test case fixture setup.
     */
    protected void setUp() throws Exception {
        
        System.out.println("ComponentClientTestCase.setUp()...");
        super.setUp();

        // Get the Logger.
        System.out.println("Getting the logger...");
        logger = getContainerServices().getLogger();
        
        this.simulator = 
            SimulatorHelper.narrow(getContainerServices().getDefaultComponent("IDL:alma/ACSSim/Simulator:1.0"));
  
        ComponentQueryDescriptor spec = new ComponentQueryDescriptor();
        spec.setComponentName("ExecutionState");
        spec.setComponentType("IDL:alma/Control/ExecutionState:1.0");
        
        try {
            org.omg.CORBA.Object obj = getContainerServices().getDynamicComponent(spec, false);
            execState = ExecutionStateHelper.narrow(obj);
        } catch (AcsJContainerServicesEx ex) {
            logger.severe("Error creating ExecutionState component: "
                    + ex.toString());
            ex.log(logger);
            fail();
        }
        
        execState.setParentName("CONTROL/Array001");
        execState.startupPass1();
        execState.startupPass2();
    }

    /**
     * Test case fixture clean up.
     */
    protected void tearDown() throws Exception {
        execState.shutdownPass1();
        execState.shutdownPass2();
        getContainerServices().releaseComponent(execState.name());
        getContainerServices().releaseComponent(simulator.name());
        super.tearDown();
    }

    /**
     * Simple tests that just activates and deactives the ExecutionState
     * component. All the work is done in setUp() and tearDown().
     */
    public void testCreateExecutionState() {}
    
    public void testReceivePhaseCalReducedEvent() throws Exception {
        CorbaPublisher p =
            new CorbaPublisher("CONTROL_SYSTEM", getContainerServices());
        CorbaPublisher tp =
            new CorbaPublisher(CHANNELNAME_TELCALPUBLISHER.value,
                               getContainerServices());
        
        IDLEntityRef ref1 = new IDLEntityRef("uid://X0/X0", "X0", "Scan", "1.0");
        ScanStartedEvent sse = new ScanStartedEvent(ref1, "Array001", 1,
                new alma.Control.ScanIntentData[] {
                    new alma.Control.ScanIntentData(
                        alma.ScanIntentMod.ScanIntent.CALIBRATE_PHASE,
                        alma.CalDataOriginMod.CalDataOrigin.NONE,
                        alma.CalibrationSetMod.CalibrationSet.UNSPECIFIED,
                        alma.CalibrationFunctionMod.CalibrationFunction.UNSPECIFIED,
                        alma.AntennaMotionPatternMod.AntennaMotionPattern.NONE,
                        true)
                }, 
                0L);
        p.publish(sse);

        ScanEndedEvent see = new ScanEndedEvent(ref1, "Array001", 1,
                alma.Control.Completion.SUCCESS, 0L);
        p.publish(see);
        
        // Give some time so the NC message is processed.
        Thread.sleep(500);
        
        IDLEntityRef ref2 = new IDLEntityRef("uid://X0/X0", "X0", "PhaseCal", "1.0");
        PhaseCalReducedEvent pcre = new PhaseCalReducedEvent(ref2, 0L, 1,
                alma.TelCalPublisher.CompletionEnum.SUCCESS,
                alma.TelCalPublisher.QualityEnum.GOOD);
        tp.publish(pcre);
        
        // Give some time so the NC message is processed.
        Thread.sleep(500);
        
        if (execState.isPhaseCalAvailable(10000)) {
            ASDMDataSetIDL asdm = execState.getPhaseCalResult();
        } else {
            fail("Phase Cal Result not available");
        }
        
        p.disconnect();
        tp.disconnect();
    }
    
    public void testReceivePhaseCalReducedEvent2() throws Exception {
        CorbaPublisher p =
            new CorbaPublisher("CONTROL_SYSTEM", getContainerServices());
        CorbaPublisher tp =
            new CorbaPublisher(CHANNELNAME_TELCALPUBLISHER.value,
                               getContainerServices());
        
        IDLEntityRef ref1 = new IDLEntityRef("uid://X0/X0", "X0", "Scan", "1.0");
        ScanStartedEvent sse =
            new ScanStartedEvent(ref1, "Array001", 1,
                new alma.Control.ScanIntentData[] {
                    new alma.Control.ScanIntentData(
                        alma.ScanIntentMod.ScanIntent.CALIBRATE_PHASE,
                        alma.CalDataOriginMod.CalDataOrigin.NONE,
                        alma.CalibrationSetMod.CalibrationSet.UNSPECIFIED,
                        alma.CalibrationFunctionMod.CalibrationFunction.UNSPECIFIED,
                        alma.AntennaMotionPatternMod.AntennaMotionPattern.NONE,
                        true)
                }, 
            0L);
        p.publish(sse);

        ScanEndedEvent see = new ScanEndedEvent(ref1, "Array001", 1,
                alma.Control.Completion.SUCCESS, 0L);
        p.publish(see);
        
        new Thread(new Runnable() {
            public void run() {
                if (execState.isPhaseCalAvailable(10000)) {
                    try {
                        ASDMDataSetIDL asdm = execState.getPhaseCalResult();
                    } catch (NoCalibrationResultEx ex) {
                        fail("No calibration result available.");
                    }
                } else {
                    fail("Phase Cal Result not available");
                }                
            }
        }).start(); 
        
        Thread.sleep(3000); // Just to get some more excitement.
        
        IDLEntityRef ref2 = new IDLEntityRef("uid://X0/X0", "X0", "PhaseCal", "1.0");
        PhaseCalReducedEvent pcre = new PhaseCalReducedEvent(ref2, 0L, 1,
                alma.TelCalPublisher.CompletionEnum.SUCCESS,
                alma.TelCalPublisher.QualityEnum.GOOD);
        tp.publish(pcre);
                
        p.disconnect();
        tp.disconnect();
    }

    public void testNoPhaseCal() {
        try {
            execState.getPhaseCalResult();
        } catch (NoCalibrationResultEx e) {
            return;
        }
        fail("A NoCalibrationResultEx exception should have been raised");
    }

    public void testGetPhaseCalTimeout() {
        if (execState.isPhaseCalAvailable(1000))
            fail("Timeout should return a False value");
    }

    public void testReceivePointingCalReducedEvent() throws Exception {
        CorbaPublisher p =
            new CorbaPublisher("CONTROL_SYSTEM", getContainerServices());
        CorbaPublisher tp =
            new CorbaPublisher(CHANNELNAME_TELCALPUBLISHER.value,
                               getContainerServices());
        
        IDLEntityRef ref1 = new IDLEntityRef("uid://X0/X0", "X0", "Scan", "1.0");
        ScanStartedEvent sse = new ScanStartedEvent(ref1, "Array001", 1,
                new alma.Control.ScanIntentData[] {
                    new alma.Control.ScanIntentData(
                        alma.ScanIntentMod.ScanIntent.CALIBRATE_POINTING,
                        alma.CalDataOriginMod.CalDataOrigin.NONE,
                        alma.CalibrationSetMod.CalibrationSet.UNSPECIFIED,
                        alma.CalibrationFunctionMod.CalibrationFunction.UNSPECIFIED,
                        alma.AntennaMotionPatternMod.AntennaMotionPattern.NONE,
                        true)
                }, 
                0L);
        p.publish(sse);

        ScanEndedEvent see = new ScanEndedEvent(ref1, "Array001", 1,
                alma.Control.Completion.SUCCESS, 0L);
        p.publish(see);
        
        // Give some time so the NC message is processed.
        Thread.sleep(500);
        
        IDLEntityRef ref2 = new IDLEntityRef("uid://X0/X0", "X0", "PointingCal", "1.0");
        PointingReducedEvent pcre = new PointingReducedEvent(ref2, 0L, 1,
                alma.TelCalPublisher.CompletionEnum.SUCCESS,
                alma.TelCalPublisher.QualityEnum.GOOD);
        tp.publish(pcre);
        
        // Give some time so the NC message is processed.
        Thread.sleep(500);
        
        if (execState.isPointingCalAvailable(10000)) {
            ASDMDataSetIDL asdm = execState.getPointingCalResult();
        } else {
            fail("Pointing Cal Result not available");
        }
        
        p.disconnect();
        tp.disconnect();
    }

    public static Test suite() {
        TestSuite suite = new TestSuite();
        String testSuite = System.getProperty("suite");
        if (testSuite == null)
            testSuite = "0"; // Default test suite.
        try {
            if (testSuite.equals("0")) {
                suite.addTest(new ExecutionStateTest("testCreateExecutionState"));
            } else if (testSuite.equals("1")) {
                suite.addTest(new ExecutionStateTest("testReceivePhaseCalReducedEvent"));
            } else if (testSuite.equals("2")) {
                suite.addTest(new ExecutionStateTest("testReceivePhaseCalReducedEvent2"));
            } else if (testSuite.equals("3")) {
                suite.addTest(new ExecutionStateTest("testNoPhaseCal"));
            } else if (testSuite.equals("4")) {
                suite.addTest(new ExecutionStateTest("testGetPhaseCalTimeout"));
            } else if (testSuite.equals("5")) {
                suite.addTest(new ExecutionStateTest("testReceivePointingCalReducedEvent"));
            }
        } catch (Exception ex) {
            System.err.println("Error when creating ExecutionStateTest: "
                    + ex.toString());
        }
        return suite;
    }
}
