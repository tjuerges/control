#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#

import asdmIDL
import TelCalPublisher__POA

from Acspy.Servants.ContainerServices  import ContainerServices
from Acspy.Servants.ComponentLifecycle import ComponentLifecycle
from Acspy.Servants.ACSComponent       import ACSComponent

class TelcalResults(TelCalPublisher__POA.GetTelCalResults,
                    ACSComponent,
                    ContainerServices,
                    ComponentLifecycle):

    #------------------------------------------------------------------------------
    #--Constructor-----------------------------------------------------------------
    #------------------------------------------------------------------------------
    def __init__(self):
        ACSComponent.__init__(self)
        ContainerServices.__init__(self)
        self.logger = self.getLogger()
        self.asdm = asdmIDL.ASDMDataSetIDL(asdmIDL.MainTableIDL([]),
                                           asdmIDL.AlmaRadiometerTableIDL([]),
                                           asdmIDL.AnnotationTableIDL([]),
                                           asdmIDL.AntennaTableIDL([]),
                                           asdmIDL.BeamTableIDL([]),
                                           asdmIDL.CalAmpliTableIDL([]),
                                           asdmIDL.CalAtmosphereTableIDL([]),
                                           asdmIDL.CalBandpassTableIDL([]),
                                           asdmIDL.CalCurveTableIDL([]),
                                           asdmIDL.CalDataTableIDL([]),
                                           asdmIDL.CalDelayTableIDL([]),
                                           asdmIDL.CalDeviceTableIDL([]),
                                           asdmIDL.CalFluxTableIDL([]),
                                           asdmIDL.CalFocusTableIDL([]),
                                           asdmIDL.CalFocusModelTableIDL([]),
                                           asdmIDL.CalGainTableIDL([]),
                                           asdmIDL.CalHolographyTableIDL([]),
                                           asdmIDL.CalPhaseTableIDL([]),
                                           asdmIDL.CalPointingTableIDL([]),
                                           asdmIDL.CalPointingModelTableIDL([]),
                                           asdmIDL.CalPositionTableIDL([]),
                                           asdmIDL.CalPrimaryBeamTableIDL([]),
                                           asdmIDL.CalReductionTableIDL([]),
                                           asdmIDL.CalSeeingTableIDL([]),
                                           asdmIDL.CalWVRTableIDL([]),
                                           asdmIDL.ConfigDescriptionTableIDL([]),
                                           asdmIDL.CorrelatorModeTableIDL([]),
                                           asdmIDL.DataDescriptionTableIDL([]),
                                           asdmIDL.DelayModelTableIDL([]),
                                           asdmIDL.DopplerTableIDL([]),
                                           asdmIDL.EphemerisTableIDL([]),
                                           asdmIDL.ExecBlockTableIDL([]),
                                           asdmIDL.FeedTableIDL([]),
                                           asdmIDL.FieldTableIDL([]),
                                           asdmIDL.FlagCmdTableIDL([]),
                                           asdmIDL.FocusTableIDL([]),
                                           asdmIDL.FocusModelTableIDL([]),
                                           asdmIDL.FreqOffsetTableIDL([]),
                                           asdmIDL.GainTrackingTableIDL([]),
                                           asdmIDL.HistoryTableIDL([]),
                                           asdmIDL.HolographyTableIDL([]),
                                           asdmIDL.ObservationTableIDL([]),
                                           asdmIDL.PointingTableIDL([]),
                                           asdmIDL.PointingModelTableIDL([]),
                                           asdmIDL.PolarizationTableIDL([]),
                                           asdmIDL.ProcessorTableIDL([]),
                                           asdmIDL.ReceiverTableIDL([]),
                                           asdmIDL.SBSummaryTableIDL([]),
                                           asdmIDL.ScanTableIDL([]),
                                           asdmIDL.SeeingTableIDL([]),
                                           asdmIDL.SourceTableIDL([]),
                                           asdmIDL.SpectralWindowTableIDL([]),
                                           asdmIDL.SquareLawDetectorTableIDL([]),
                                           asdmIDL.StateTableIDL([]),
                                           asdmIDL.StationTableIDL([]),
                                           asdmIDL.SubscanTableIDL([]),
                                           asdmIDL.SwitchCycleTableIDL([]),
                                           asdmIDL.SysCalTableIDL([]),
                                           asdmIDL.TotalPowerTableIDL([]),
                                           asdmIDL.WVMCalTableIDL([]),
                                           asdmIDL.WeatherTableIDL([]))

    #------------------------------------------------------------------------------
    #--Override ComponentLifecycle methods-----------------------------------------
    #------------------------------------------------------------------------------
    def initialize(self):
        self.logger.logDebug("called...")

    #------------------------------------------------------------------------------
    def cleanUp(self):
        self.logger.logDebug("called...")

    #------------------------------------------------------------------------------
    def getName(self):
        return "TelCalResults"

    #------------------------------------------------------------------------------
    #--Implementation of TelCal IDL methods----------------------------
    #------------------------------------------------------------------------------

    #------------------------------------------------------------------------------
    def getPhaseCalResult(self, execBlockId, scanNum):
        return self.asdm
    
    def getPointingResult(self, execBlockId, scanNum):
        return self.asdm
    
    def getFocusResult(self, execBlockId, scanNum):
        return self.asdm
