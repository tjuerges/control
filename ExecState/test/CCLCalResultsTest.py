#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#

from Acspy.Clients.SimpleClient import PySimpleClient
from Acspy.Nc.Supplier import Supplier
from CCL.CalResults import CalResults
from CCL.Global import *
from CCL.logging import getLogger
from asdmIDLTypes import IDLEntityRef
import Control
import ScanIntentMod
import TelCalPublisher
import thread
import time
import unittest
import ExecStateExceptions

class CCLCalResultsTest(unittest.TestCase):
    
    def setUp(self):
        """Setup. This function is called before each test.
        """
        self.client = PySimpleClient()
        self.logger = getLogger()
        self.execState = self.client.getComponent(comp_name="ExecState",
                                                  comp_idl_type="IDL:alma/Control/ExecutionState:1.0",
                                                  is_dynamic=1)
        self.supplier = Supplier("CONTROL_SYSTEM")
        self.telcalSupplier = Supplier("TelCalPublisherEventNC")

        self.execState.setParentName("CONTROL/Array001")
        self.execState.startupPass1()
        self.execState.startupPass2()

    def tearDown(self):
        """Cleans up. This function is called after each test.
        """
        self.execState.shutdownPass1()
        self.execState.shutdownPass2()
        
        self.logger.logInfo("Releasing ExecState component")
        self.client.releaseComponent("ExecState")
        
        # Release components accessed inside the CCL Objects
        releaseComponents()
    
    def testGetPhaseCalResults(self):
        """Test the normal case.
        When a PhaseCalReducedEvent event is received, the ExecutionState component
        will retrieve calibration results from TELCAL GetCalResults component.
        The CCL.CalResults object can be used to retrieve these calibration results
        from the ExecutionState component. 
        """
        
        # Here a CCL.CalResult instance is created directly. In observing scripts
        # CCL.Array.getCalResults() should be used. 
        calResults = CalResults(self.execState._get_name())
        self.sendScanStartedEvent()
        time.sleep(0.5)
        self.sendScanEndedEvent()
        time.sleep(0.5)
        self.sendPhaseCalReducedEvent()
        
        # Blocks until the phase cal results are available or the time out expires.
        if calResults.isPhaseCalAvailable(timeout=10):
            # Retrieves PhaseCal results.
            calResults = calResults.getPhaseCalResult()
        else:
            self.fail("No phase cal results available")

    def testGetPhaseCalTimeout(self):
        """The isPhaseCalAvailable() function should timeout when no PhaseCalReducedEvent
        has been sent.
        """
        calResults = CalResults(self.execState._get_name())
        if calResults.isPhaseCalAvailable(timeout=10):
            self.fail("Shouldn't get phase cal results. No event has been sent.")

    def testGetPhaseCalResultException(self):
        """If CalResults.getPhaseCalResult() is called when no PhaseCalReducedEvent
        has been received, and therefore no calibration has been retrieved from
        GetTelCalResults component, then a NoCalibrationResultEx exception is thrown. 
        """
        calResults = CalResults(self.execState._get_name())
        self.assertRaises(ExecStateExceptions.NoCalibrationResultEx,
                          calResults.getPhaseCalResult)
            
    def sendScanStartedEvent(self):
        """Utility function that will send a ScanStartedEvent to the
        CONTROL_SYSTEM notification channel filled with dummy values. 
        """
        ref = IDLEntityRef("uid://X0/X0", "X0", "Scan", "1.0")
        from Control import ScanIntentData
        import CalDataOriginMod
        import CalibrationSetMod
        import CalibrationFunctionMod
        import AntennaMotionPatternMod
        sid = ScanIntentData(ScanIntentMod.CALIBRATE_PHASE,
                             CalDataOriginMod.NONE,
                             CalibrationSetMod.UNSPECIFIED,
                             CalibrationFunctionMod.UNSPECIFIED,
                             AntennaMotionPatternMod.NONE,
                             True)
        sse = Control.ScanStartedEvent(ref, "Array001", 1, [sid], 0L);
        self.supplier.publishEvent(sse);

    def sendScanEndedEvent(self):
        """Utility function that will send a ScanEndedEvent to the
        CONTROL_SYSTEM notification channel filled with dummy values. 
        """
        ref = IDLEntityRef("uid://X0/X0", "X0", "Scan", "1.0")
        see = Control.ScanEndedEvent(ref, "Array001", 1, Control.SUCCESS, 0L)
        self.supplier.publishEvent(see);

    def sendPhaseCalReducedEvent(self):
        """Utility function that will send a PhaseCalReducedEvent to the
        CONTROL_SYSTEM notification channel filled with dummy values. 
        """
        ref = IDLEntityRef("uid://X0/X0", "X0", "PhaseCal", "1.0")
        pcre = TelCalPublisher.PhaseCalReducedEvent(ref, 0L, 1,
                                                    TelCalPublisher.SUCCESS,
                                                    TelCalPublisher.GOOD)
        self.telcalSupplier.publishEvent(pcre);
        
def suite():
    suite = unittest.TestSuite()
    suite.addTest(CCLCalResultsTest("testGetPhaseCalResults"))
#    suite.addTest(CCLCalResultsTest("testGetPhaseCalTimeout"))
#    suite.addTest(CCLCalResultsTest("testGetPhaseCalResultException"))
    return suite

if __name__ == '__main__':
    unittest.main(defaultTest='suite')

#
# ___oOo___    
