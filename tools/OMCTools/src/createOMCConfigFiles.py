#!/bin/env python
import os

# Replacements
rep = {'user': os.environ['USER'],
       'cdbpath': os.environ['ACS_CDB'],
       'machine': os.environ['HOST']}

def replaceKeywords(line, repl):
    """Replaces a number of keywords embedded in a string in the
       form '{keyword}' with a value.

       Parameters:
       line - A string.
       repl - Replacements. A map with keyword:value pairs.

       Returns: line with replacements
    """
    for k in repl.keys():
        keyword = '{' + k + '}'
        value = repl[k] 
        if line.find(keyword):
            line = line.replace(keyword, value)
    return line

def createProjectFile(template, file):
    """ Create the OMC project file. """

    prjf = open(file, 'w')
    tmplf = open(template)
    lines = tmplf.readlines()

    for l in lines:
        prjf.write(replaceKeywords(l, rep))

    tmplf.close()
    prjf.close()

def createExecConfigFile(template, file):
    """Create the exec config file. """
    os.system('cp ' + template + " " + file)

def createSchedPolicyFile(template, file):
    """Create the scheduling policy file. """
    os.system('cp ' + template + ' ' +  file)

if __name__ == '__main__':
    createProjectFile('../config/AcsProject.xml.tmpl', '../config/AcsProject.xml')
    createExecConfigFile('../config/ExecConfig.xml.tmpl', '../config/ExecConfig.xml')
    createSchedPolicyFile('../config/SchedulingPolicy.xml.tmpl', '../config/SchedulingPolicy.xml')

