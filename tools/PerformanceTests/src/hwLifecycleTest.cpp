/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2009 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2009-06-03  created
*/

/************************************************************************
*   NAME
*   
* 
*   SYNOPSIS
*   
* 
*   DESCRIPTION
*
*   FILES
*
*   ENVIRONMENT
*
*   COMMANDS
*
*   RETURN VALUES
*
*   CAUTIONS 
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS   
* 
*------------------------------------------------------------------------
*/

// Uncomment this if you are using the VLT environment
// #include "vltPort.h"


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <maciSimpleClient.h>
#include <HardwareDeviceC.h>

using maci::SimpleClient;
using std::string;

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);



class LifecycleController {
public:
  LifecycleController(SimpleClient* client, string deviceName) {
    client_m     = client;
    deviceName_m = deviceName;
  };

  ~LifecycleController(){}

//   void setComponentName(string componentName) {
//     componentName_m = componentName;
//   }

//   string getComponentName() {
//     return componentName_m;
//   }
    
  void getComponent() {
    try {
      device_m = client_m->getComponent<Control::HardwareDevice>
        (deviceName_m.c_str(),0,true);
    } catch(...) {
      std::cout << "Failed to get component: " << deviceName_m << std::endl;
    }
  }

  void doLifecycle() {
    startTime = ::getTimeStamp();
    device_m->hwConfigure();
    std::cout << "State: " << device_m->getHwState() << std::endl;
    device_m->hwInitialize();
    std::cout << "State: " << device_m->getHwState() << std::endl;
    device_m->hwOperational();
    std::cout << "State: " << device_m->getHwState() << std::endl;
    endTime = ::getTimeStamp();
  }

  void goToStart() {
    device_m->hwStop();
    device_m->hwStart();
  }
    

protected:
  ACS::Time startTime;
  ACS::Time endTime;
  bool      success;

  SimpleClient*               client_m;
  string                      deviceName_m;
  Control::HardwareDevice_var device_m;
//   string    componentName_m;

  
};


void* threadMethod(void* controller) {
  reinterpret_cast<LifecycleController*>(controller)->doLifecycle();
  return NULL;
}


// class ContainerStartup{
// public:
//   ContainerStartup(SimpleClient* client,
//                    string containerPrefix) {
    
//     populateDeviceList();
//     containerPrefix_m = containerPrefix; 

//     std::vector<string>::iterator iter;
//     for (iter = deviceNames_m.begin(); iter != deviceNames_m.end(); iter++) {
//       ComponentAccessor ca(client);
//       ca.setComponentName(containerPrefix + *iter);
//       componentAccessors_m.push_back(ca);
//     }
//   }

//   ~ContainerStartup(){};

//   void serialStartup() {
//     std::vector<ComponentAccessor>::iterator iter;
//     startTime = ::getTimeStamp();
//     for (iter = componentAccessors_m.begin();
//          iter != componentAccessors_m.end();
//          iter++) {
//       iter->getComponent();
//     }
//     endTime = ::getTimeStamp();
//   }

//   void parallelStartup() {
//     threadIDs_m.resize(componentAccessors_m.size());
//     startTime = ::getTimeStamp();
//     for (unsigned int idx = 0; idx < componentAccessors_m.size(); idx++) {
//       pthread_create(&threadIDs_m[idx], NULL, threadMethod, 
//                      &componentAccessors_m[idx]);
//     }

//     for (unsigned int idx = 0; idx < componentAccessors_m.size(); idx++) {
//       pthread_join(threadIDs_m[idx], NULL);
//     }
//     endTime = ::getTimeStamp();
//   }

//   void printReport() {
//     std::cout << "Container: " << containerPrefix_m << std::endl;
//     std::vector<ComponentAccessor>::iterator iter;
//     for (iter = componentAccessors_m.begin();
//          iter != componentAccessors_m.end();
//          iter++) {
//       std::cout << "\t" << iter->getComponentName() << "\t";
//       if (iter->getSuccess()) {
//         std::cout << iter->getElapsedTime() << std::endl;
//       } else {
//         std::cout << "FAILED" << std::endl;
//       }
//     }
//     std::cout << "\t Total Time: " << "\t" 
//               << (endTime - startTime) << std::endl;
//   }

// protected:
//   void populateDeviceList() {

std::vector<string> getDeviceList(){
  std::vector<string> deviceList;
//     deviceList.push_back("CONTROL/DA41/ACD");
  deviceList.push_back("CONTROL/DA41/FLOOG");
//     deviceList.push_back("CONTROL/DA41/Mount");
//     deviceList.push_back("CONTROL/DA41/SAS");
  deviceList.push_back("CONTROL/DA41/DGCK");
//     deviceList.push_back("CONTROL/DA41/IFProc0");
//     deviceList.push_back("CONTROL/DA41/IFProc1");
  deviceList.push_back("CONTROL/DA41/LORR");
//     deviceList.push_back("CONTROL/DA41/LLC");
//     deviceList.push_back("CONTROL/DA41/WVR");
  
//     deviceList.push_back("CONTROL/DA41/DRXBBpr0");
//     deviceList.push_back("CONTROL/DA41/DRXBBpr1");
//     deviceList.push_back("CONTROL/DA41/DRXBBpr2");
//     deviceList.push_back("CONTROL/DA41/DRXBBpr3");

//     deviceList.push_back("CONTROL/DA41/DTXBBpr0");
//     deviceList.push_back("CONTROL/DA41/DTXBBpr1");
//     deviceList.push_back("CONTROL/DA41/DTXBBpr2");
//     deviceList.push_back("CONTROL/DA41/DTXBBpr3");

//     deviceList.push_back("CONTROL/DA41/LO2BBpr0");
//     deviceList.push_back("CONTROL/DA41/LO2BBpr1");
//     deviceList.push_back("CONTROL/DA41/LO2BBpr2");
//     deviceList.push_back("CONTROL/DA41/LO2BBpr3");
  
//     deviceList.push_back("CONTROL/DA41/PSA");
//     deviceList.push_back("CONTROL/DA41/PSD  ");
//     deviceList.push_back("CONTROL/DA41/PSLLC");
//     deviceList.push_back("CONTROL/DA41/PSSAS");

//     deviceList.push_back("CONTROL/DA41/FrontEnd/ColdCart3");
//     deviceList.push_back("CONTROL/DA41/FrontEnd/ColdCart6");
//     deviceList.push_back("CONTROL/DA41/FrontEnd/ColdCart7");
//     deviceList.push_back("CONTROL/DA41/FrontEnd/ColdCart9");

//     deviceList.push_back("CONTROL/DA41/FrontEnd/PowerDist3");
//     deviceList.push_back("CONTROL/DA41/FrontEnd/PowerDist6");
//     deviceList.push_back("CONTROL/DA41/FrontEnd/PowerDist7");
//     deviceList.push_back("CONTROL/DA41/FrontEnd/PowerDist9");

//     deviceList.push_back("CONTROL/DA41/FrontEnd/WCA3");
//     deviceList.push_back("CONTROL/DA41/FrontEnd/WCA6");
//     deviceList.push_back("CONTROL/DA41/FrontEnd/WCA7");
//     deviceList.push_back("CONTROL/DA41/FrontEnd/WCA9");

//     deviceList.push_back("CONTROL/DA41/FrontEnd/Cryostat");
//     deviceList.push_back("CONTROL/DA41/FrontEnd/IFSwitch");
//     deviceList.push_back("CONTROL/DA41/FrontEnd/LPR");
  return deviceList;
}

//   string              containerPrefix_m;
//   std::vector<string> deviceList;
//   std::vector<ComponentAccessor> componentAccessors_m;
//   std::vector<pthread_t>         threadIDs_m;

//   ACS::Time startTime;
//   ACS::Time endTime;

// };


// void* startContainerParallel(void* containerStartup) {
//   reinterpret_cast<ContainerStartup*>(containerStartup)->parallelStartup();
//   return NULL;
// }

// void* startContainerSerial(void* containerStartup) {
//   reinterpret_cast<ContainerStartup*>(containerStartup)->serialStartup();
//   return NULL;
// }


int main(int argc, char *argv[])
{
  SimpleClient client;
  client.init(argc, argv);
  client.login();

  std::vector<LifecycleController> controllers;
  std::vector<string>              deviceList = getDeviceList();


  for (std::vector<string>::iterator siter = deviceList.begin();
       siter != deviceList.end(); siter++) {
    controllers.push_back(LifecycleController(&client, *siter));
  }

  std::vector<LifecycleController>::iterator iter;
  for (iter = controllers.begin(); iter != controllers.end(); iter++) {
    iter->getComponent();
  }


  /* Serial Approach */
  ACS::Time serialStart = ::getTimeStamp();
  for (iter = controllers.begin(); iter != controllers.end(); iter++) {
    iter->doLifecycle();
  }
  ACS::Time serialStop = ::getTimeStamp();

  std::cout << "Serial Time:   " << (serialStop - serialStart)/1.0E7
            << std::endl;

  for (iter = controllers.begin(); iter != controllers.end(); iter++) {
    iter->goToStart();
  }

  std::vector<pthread_t>  threadIDs(controllers.size());

  for (unsigned int idx = 0; idx < threadIDs.size(); idx++) {
    pthread_create(&threadIDs[idx], NULL, threadMethod, 
                   &controllers[idx]);
  }

  ACS::Time parallelStart = ::getTimeStamp();
  for (unsigned int idx = 0; idx < controllers.size(); idx++) {
    pthread_join(threadIDs[idx], NULL);
   }
  ACS::Time parallelStop = ::getTimeStamp();

  std::cout << "Parallel Time: " << (parallelStop - parallelStart)/1.0E7
            << std::endl;

  client.logout();
  return 0;
}
