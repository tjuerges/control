/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2009 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2009-06-03  created
*/

/************************************************************************
*   NAME
*   
* 
*   SYNOPSIS
*   
* 
*   DESCRIPTION
*
*   FILES
*
*   ENVIRONMENT
*
*   COMMANDS
*
*   RETURN VALUES
*
*   CAUTIONS 
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS   
* 
*------------------------------------------------------------------------
*/

// Uncomment this if you are using the VLT environment
// #include "vltPort.h"


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <maciSimpleClient.h>

using maci::SimpleClient;
using std::string;

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);


class ComponentAccessor {
public:
  ComponentAccessor(SimpleClient* client) {
    clientRef = client;
  };

  ~ComponentAccessor(){}

  void setComponentName(string componentName) {
    componentName_m = componentName;
  }

  string getComponentName() {
    return componentName_m;
  }
    
  void getComponent() {
    startTime = ::getTimeStamp();
    try {
      clientRef->getComponent(componentName_m.c_str(),0,true);
      success = true;
    } catch(...) {
      success = false;
    }
    endTime = ::getTimeStamp();
  }

  ACS::Time getElapsedTime() {
    return endTime - startTime;
  };

  bool getSuccess() {
    return success;
  }

protected:
  ACS::Time startTime;
  ACS::Time endTime;
  bool      success;

  string    componentName_m;

  SimpleClient* clientRef;
};


void* threadMethod(void* componentAccessor) {
  reinterpret_cast<ComponentAccessor*>(componentAccessor)->getComponent();
  return NULL;
}


class ContainerStartup{
public:
  ContainerStartup(SimpleClient* client,
                   string containerPrefix) {
    
    populateDeviceList();
    containerPrefix_m = containerPrefix; 

    std::vector<string>::iterator iter;
    for (iter = deviceNames_m.begin(); iter != deviceNames_m.end(); iter++) {
      ComponentAccessor ca(client);
      ca.setComponentName(containerPrefix + *iter);
      componentAccessors_m.push_back(ca);
    }
  }

  ~ContainerStartup(){};

  void serialStartup() {
    std::vector<ComponentAccessor>::iterator iter;
    startTime = ::getTimeStamp();
    for (iter = componentAccessors_m.begin();
         iter != componentAccessors_m.end();
         iter++) {
      iter->getComponent();
    }
    endTime = ::getTimeStamp();
  }

  void parallelStartup() {
    threadIDs_m.resize(componentAccessors_m.size());
    startTime = ::getTimeStamp();
    for (unsigned int idx = 0; idx < componentAccessors_m.size(); idx++) {
      pthread_create(&threadIDs_m[idx], NULL, threadMethod, 
                     &componentAccessors_m[idx]);
    }

    for (unsigned int idx = 0; idx < componentAccessors_m.size(); idx++) {
      pthread_join(threadIDs_m[idx], NULL);
    }
    endTime = ::getTimeStamp();
  }

  void printReport() {
    std::cout << "Container: " << containerPrefix_m << std::endl;
    std::vector<ComponentAccessor>::iterator iter;
    for (iter = componentAccessors_m.begin();
         iter != componentAccessors_m.end();
         iter++) {
      std::cout << "\t" << iter->getComponentName() << "\t";
      if (iter->getSuccess()) {
        std::cout << iter->getElapsedTime() << std::endl;
      } else {
        std::cout << "FAILED" << std::endl;
      }
    }
    std::cout << "\t Total Time: " << "\t" 
              << (endTime - startTime) << std::endl;
  }

protected:
  void populateDeviceList() {
    deviceNames_m.push_back("ACD");
    deviceNames_m.push_back("FLOOG");
    deviceNames_m.push_back("Mount");
    deviceNames_m.push_back("SAS");
    deviceNames_m.push_back("DGCK");
    deviceNames_m.push_back("IFProc0");
    deviceNames_m.push_back("IFProc1");
    deviceNames_m.push_back("LORR  ");
    deviceNames_m.push_back("LLC");
    deviceNames_m.push_back("WVR");
  
    deviceNames_m.push_back("DRXBBpr0");
    deviceNames_m.push_back("DRXBBpr1");
    deviceNames_m.push_back("DRXBBpr2");
    deviceNames_m.push_back("DRXBBpr3");

    deviceNames_m.push_back("DTXBBpr0");
    deviceNames_m.push_back("DTXBBpr1");
    deviceNames_m.push_back("DTXBBpr2");
    deviceNames_m.push_back("DTXBBpr3");

    deviceNames_m.push_back("LO2BBpr0");
    deviceNames_m.push_back("LO2BBpr1");
    deviceNames_m.push_back("LO2BBpr2");
    deviceNames_m.push_back("LO2BBpr3");
  
    deviceNames_m.push_back("PSA");
    deviceNames_m.push_back("PSD  ");
    deviceNames_m.push_back("PSLLC");
    deviceNames_m.push_back("PSSAS");

    deviceNames_m.push_back("FrontEnd/ColdCart3");
    deviceNames_m.push_back("FrontEnd/ColdCart6");
    deviceNames_m.push_back("FrontEnd/ColdCart7");
    deviceNames_m.push_back("FrontEnd/ColdCart9");

    deviceNames_m.push_back("FrontEnd/PowerDist3");
    deviceNames_m.push_back("FrontEnd/PowerDist6");
    deviceNames_m.push_back("FrontEnd/PowerDist7");
    deviceNames_m.push_back("FrontEnd/PowerDist9");

    deviceNames_m.push_back("FrontEnd/WCA3");
    deviceNames_m.push_back("FrontEnd/WCA6");
    deviceNames_m.push_back("FrontEnd/WCA7");
    deviceNames_m.push_back("FrontEnd/WCA9");

    deviceNames_m.push_back("FrontEnd/Cryostat");
    deviceNames_m.push_back("FrontEnd/IFSwitch");
    deviceNames_m.push_back("FrontEnd/LPR");
  }

  string              containerPrefix_m;
  std::vector<string> deviceNames_m;
  std::vector<ComponentAccessor> componentAccessors_m;
  std::vector<pthread_t>         threadIDs_m;

  ACS::Time startTime;
  ACS::Time endTime;

};


void* startContainerParallel(void* containerStartup) {
  reinterpret_cast<ContainerStartup*>(containerStartup)->parallelStartup();
  return NULL;
}

void* startContainerSerial(void* containerStartup) {
  reinterpret_cast<ContainerStartup*>(containerStartup)->serialStartup();
  return NULL;
}


int main(int argc, char *argv[])
{
  SimpleClient client;
  client.init(argc, argv);
  client.login();
  std::vector<pthread_t>        threadIDs;
  std::vector<ContainerStartup> containers;
  ACS::Time                     startTime;
  ACS::Time                     endTime;

  /* Set up the containers here */
  containers.push_back(ContainerStartup(&client, "CONTROL/DA41/"));
  threadIDs.resize(containers.size());
  
  startTime = ::getTimeStamp();
  /* For parallelization across containers */
  for (unsigned int idx = 0; idx < containers.size(); idx++) {
    /* For Parallel startup of all containers */
    //pthread_create(&threadIDs[idx], NULL, startContainerParallel, 
    //               &containers[idx]);
    /* For Serial Startup of all containers */
    pthread_create(&threadIDs[idx], NULL, startContainerSerial, 
                   &containers[idx]);
  }

  for (unsigned int idx = 0; idx < containers.size(); idx++) {
    pthread_join(threadIDs[idx], NULL);
  }
  endTime = ::getTimeStamp();



  std::cout << "Total Startup Time: " << endTime - startTime << std::endl;
  for (unsigned int idx = 0; idx < containers.size(); idx++) {
    containers[idx].printReport();
  }

  client.logout();
  return 0;
}









