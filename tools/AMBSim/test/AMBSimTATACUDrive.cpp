// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it
// under
// the terms of the GNU Library General Public License as published by the
// Free
// Software Foundation; either version 2 of the License, or (at your option)
// any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for
// more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation,
// Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include "ACUDrive.h"
#include <unistd.h>
#include <string>
using namespace std;

int main(int argc,char** argv)
{
    ACUDrive acu;
    unsigned char newmodeAz = (unsigned char) 0;
    unsigned char newmodeEl = (unsigned char) 0;
    unsigned char newmode   = (unsigned char) 0;

    std::vector<char> acuMode(2);
    int mode;

    cout << "Test begin" << endl;

    acuMode = acu.getACUMode();
    cout << "Initial ACU Mode: "  << (int) acuMode[0] << endl;

    // Az modes
    cout << "Testing Az ACU Modes: "  << endl;    
    for(mode=0;mode<=6;mode++)
	{
	newmodeAz = (unsigned char) mode;
	newmode = newmodeAz | newmodeEl;
	cout << " New mode is " << (int) newmode << endl;
	if(!acu.setACUMode(newmode))
	    {
	    acuMode = acu.getACUMode();
	    if ( ((int)acuMode[0])==((int)newmode) )
		{
		cout << " New mode correctly set: " << (int) acuMode[0] << endl;
		}
	    else
		{	    
		cout << " New mode not set correctly" << (int) acuMode[0] << endl;
		}
	    }
	else
	    {
	    cout << " New mode not set (invalid mode value): " << (int) newmode << endl;
	    }
	}
    newmodeAz = (unsigned char) 0;
    newmode = newmodeAz | newmodeEl;
    acu.setACUMode(newmode);

    // El modes
    cout << "Testing El ACU Modes: " << endl;    
    for(mode=0;mode<=6;mode++)
	{
	newmodeEl = ((unsigned char) mode) << 4;
	newmode = newmodeAz | newmodeEl;
	cout << " New mode is " << (int) newmode << endl;
	if(!acu.setACUMode(newmode))
	    {
	    acuMode = acu.getACUMode();
	    if ( ((int)acuMode[0])==((int)newmode) )
		{
		cout << " New mode correctly set: " << (int) acuMode[0] << endl;
		}
	    else
		{
		cout << " New mode not set correctly" << (int) acuMode[0] << endl;
		}
	    }
	else
	    {
	    cout << " New mode not set (invalid mode value): " << (int) newmode << endl;
	    }
	}
    newmodeEl = ((unsigned char) 0) << 4;
    newmode = newmodeAz | newmodeEl;
    acu.setACUMode(newmode);
	
    // Az, El modes
    cout << "Testing Az,El ACU Modes: " << endl;    
    for(mode=0;mode<=6;mode++)
	{
	newmodeAz = (unsigned char) mode;
	newmodeEl = ((unsigned char) mode) << 4;
	newmode = newmodeAz | newmodeEl;
	cout << " New mode is " << (int) newmode << endl;
	if(!acu.setACUMode(newmode))
	    {
	    acuMode = acu.getACUMode();
	    if ( ((int)acuMode[0])==((int)newmode) )
		{
		cout << " New mode correctly set: " << (int) acuMode[0] << endl;
		}
	    else
		{
		cout << " New mode not set correctly" << (int) acuMode[0] << endl;
		}
	    }
	else
	    {
	    cout << " New mode not set (invalid mode value): " << (int) newmode << endl;
	    }
	}

    // Returning to initial conditions
    newmodeAz = (unsigned char) 0;
    newmodeEl = ((unsigned char) 0) << 4;
    newmode = newmodeAz | newmodeEl;
    acu.setACUMode(newmode);

    cout << "Test end" << endl;

    return 0;
}
