// @(#) $Id$
//
// Copyright (C) 2002
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

/** This tests the function calls of the LORR
 ** classes which represent the LORR Measurement Equipment device
 ** simulator.
 ** This test displays 'Tests Successful' if all tests pass, otherwise
 ** error messages appear ending with 'Tests Failed'.
 */

#include <LORR.h>

#include <AMBUtil.h>
#include <iostream>
#include <string>

using namespace std;
// Forward declaration for checking value for LORR object
bool doLORRTests();

template <class T>
 void checkExpectedLORRValue(const vector<CAN::byte_t>& monitorBytes, 
 			    T expectedValue,
 			    unsigned int size, 
 			    const string& pMonitorName);
void printCANMsg(const vector<CAN::byte_t>& CANMsg, const string& pMsg);

bool gTestsSuccessful = true;

int main() {
  // Construct 2 CAN nodes
  if( !doLORRTests( ) )
    gTestsSuccessful = false;
  
  if( gTestsSuccessful )
    cout << "Tests Successful" << endl;
  else
    cout << "Tests Failed" << endl;
}

//-----------------------------------------------------------------------------
bool doLORRTests() {
  bool bTestsSuccessful = true;
  AMB::node_t LORRNode = 0x22;

  vector<CAN::byte_t> LORRSn(8);

  // LORR Recevier S/N looks like 10660000
  LORRSn[0] = 0x01;
  LORRSn[1] = 0x02;
  LORRSn[2] = 0x04;
  LORRSn[3] = 0x08;
  LORRSn[4] = 0x10;
  LORRSn[5] = 0x20;
  LORRSn[6] = 0x40;
  LORRSn[7] = 0x80;
  
  AMB::LORR LORR(LORRNode, LORRSn );
  vector<CAN::byte_t> monitorBytes;
  
  // To check the # of CAN transactions monitor points, keep track of the
  // # of times we call monitor & control points
  int expectedNumberCANTransactions = 0;
  
  vector<CAN::byte_t> sn = LORR.serialNumber();
  for( int i = 0; i < 8; i++ ) {
    if( sn[i] != LORRSn[i] ) {
      bTestsSuccessful = false;
      cerr << "LORR: S/N check failed on digit " << i << endl;
    }
  }
  if( LORRNode != LORR.node() ) {
    bTestsSuccessful = false;
    cerr << "LORR: node check failed: " << LORR.node() << " should be: "
 	 << LORRNode << endl;
  }

  // Monitor points specific to LORR
  monitorBytes = LORR.monitor(AMB::LORR::EFC_25_MHZ);
  checkExpectedLORRValue(monitorBytes, LORR.getEFC_25_MHZ(), 2, "EFC_25_MHZ");
  expectedNumberCANTransactions++;
  
  monitorBytes = LORR.monitor(AMB::LORR::EFC_2_GHZ);
  checkExpectedLORRValue(monitorBytes, LORR.getEFC_2_GHZ(), 2, "EFC_2_GHZ");
  expectedNumberCANTransactions++;
  
  monitorBytes = LORR.monitor(AMB::LORR::PWR_25_MHZ);
  checkExpectedLORRValue(monitorBytes, LORR.getPWR_25_MHZ(), 2, "PWR_25_MHZ");
  expectedNumberCANTransactions++;

  monitorBytes = LORR.monitor(AMB::LORR::PWR_125_MHZ);
  checkExpectedLORRValue(monitorBytes, LORR.getPWR_125_MHZ(), 2, 
			 "PWR_125_MHZ");
  expectedNumberCANTransactions++;

  monitorBytes = LORR.monitor(AMB::LORR::PWR_2_GHZ);
  checkExpectedLORRValue(monitorBytes, LORR.getPWR_2_GHZ(), 2, "PWR_2_GHZ");
  expectedNumberCANTransactions++;
  
  monitorBytes = LORR.monitor(AMB::LORR::RX_OPT_PWR);
  checkExpectedLORRValue(monitorBytes, LORR.getRX_OPT_PWR(), 2, "RX_OPT_PWR");
  expectedNumberCANTransactions++;
  
  monitorBytes = LORR.monitor(AMB::LORR::VDC_12);
  checkExpectedLORRValue(monitorBytes, LORR.getVDC_12(), 2, "VDC_12");
  expectedNumberCANTransactions++;
  
  monitorBytes = LORR.monitor(AMB::LORR::VDC_15);
  checkExpectedLORRValue(monitorBytes, LORR.getVDC_15(), 2, "VDC_15");
  expectedNumberCANTransactions++;
  
//   // Do control functions
//   vector<CAN::byte_t> data1(1), data2(2);
//   data1[0] = 0;
//   LORR.control(AMB::LORR::XILINX_RESET, data1);
//   expectedNumberCANTransactions++;
//   // Now check that XILINX_Reset works
  
//   // Xilinx status should be 1 after successful reset/software load
//   monitorBytes = LORR.monitor( AMB::LORR::XILINX_READY);
//   expectedNumberCANTransactions++;
//   if( monitorBytes[0] != 1 ) {
//     cerr << "Didn't set reset Xilinx status correctly: " << (int)monitorBytes[0] <<
//       " should be 1\n";
//     bTestsSuccessful = false;
//   }
    
  // Check basic CAN functions
  monitorBytes = LORR.monitor(AMB::LORR::GET_CAN_ERROR );
  expectedNumberCANTransactions++;
  {
    const short shortVal= AMB::dataToShort(monitorBytes);
    if (shortVal != 0) {
      cerr << "CAN Error msg: " << shortVal << " expected 0" << endl;
      bTestsSuccessful = false;
    }
  }
  
    
  monitorBytes = LORR.monitor( AMB::LORR::GET_PROTOCOL_REV_LEVEL );
  expectedNumberCANTransactions++;
  {
    const short shortVal= AMB::dataToShort(monitorBytes);
    if (shortVal != 0 ) {
      cerr << "Protocol Rev. Level Error: " << shortVal 
	   << " expected 0" << endl;
      bTestsSuccessful = false;
    }
  }
  
  monitorBytes = LORR.monitor( AMB::LORR::GET_AMBIENT_TEMPERATURE );
  expectedNumberCANTransactions++;
  {
    const short shortVal= AMB::dataToShort(monitorBytes);
    if ( shortVal != 0x2C ) {
      cerr << "Ambient Temperature error : " << shortVal 
	   << " expected 0x2C" << endl;
      bTestsSuccessful = false;
    }
  }
  

  monitorBytes = LORR.monitor( AMB::LORR::GET_TRANS_NUM );
  expectedNumberCANTransactions++;
  {
    const long longVal = AMB::dataToLong(monitorBytes);
    if (longVal != expectedNumberCANTransactions ) {
      cerr << "Number of CAN Transactions Error: " << longVal << " expected " 
	   << expectedNumberCANTransactions << endl;
      bTestsSuccessful = false;
    }
    return bTestsSuccessful;
  }
}

//-----------------------------------------------------------------------------
template <class T> 
void checkExpectedLORRValue( const vector<CAN::byte_t>& monitorBytes,
			     T expectedValue, unsigned int size,
			     const string& pMonitorName) {
  if (monitorBytes.size() != size ) {
    cerr << "Incorrect LORR size: " 
	      << monitorBytes.size() << " expected " << size
	      << " for " << pMonitorName << endl;
    gTestsSuccessful = false;
    return;
  }

  vector<CAN::byte_t> tmp(monitorBytes);
  short actualValue;
  if( size == 1 ) {
    actualValue = static_cast<short>(monitorBytes[0]);
  } else if ( size == 2 ) {
    actualValue = AMB::dataToShort( tmp );
  } else { //if( size == 4 )
    return;
  }

// Commented out for now! FIX THIS! Once a better philosophy for handing unit
// tests of simulators is established.
//   if (actualValue != expectedValue ) {
//     cerr << "LORR value: " << actualValue 
// 	 << " != expected value: " << expectedValue 
// 	 << " for " << pMonitorName << endl;
//     gTestsSuccessful = false;
//   }
}

//-----------------------------------------------------------------------------
void printCANMsg(const vector<CAN::byte_t> &CANMsg, const string& pMsg ) {
  cout << pMsg << ' ';
  for (int i = 0; i < 8; i++ ) {
    cout << static_cast<int>(CANMsg[i]) << ' ';
  }
  cout << endl;
}

