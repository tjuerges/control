/*******************************************************************************
*    ALMA - Atacama Large Millimiter Array
*    (c) European Southern Observatory, 2002
*    Copyright by ESO (in the framework of the ALMA collaboration)
*    and Cosylab 2002, All rights reserved
*
*    This library is free software; you can redistribute it and/or
*    modify it under the terms of the GNU Lesser General Public
*    License as published by the Free Software Foundation; either
*    version 2.1 of the License, or (at your option) any later version.
*
*    This library is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    Lesser General Public License for more details.
*
*    You should have received a copy of the GNU Lesser General Public
*    License along with this library; if not, write to the Free Software
*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*/

#include "vltPort.h"
#include <math.h>

#include "AMBDevice.h"
#include "ambDevIOTestDevice.h"
#include "HWSimulator.h"


static char *rcsId="@(#) $Id:"; 
static void *use_rcsId = ((void)(void)&use_rcsId,(void *) &rcsId);

#include <cppunit/extensions/HelperMacros.h>

/* 
 * A test case for the HWSimulator class
 *
 */

class HWSimulatorTestCase : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE( HWSimulatorTestCase );
    CPPUNIT_TEST( test_lifecycle );
    CPPUNIT_TEST( test_simulation );
    CPPUNIT_TEST_SUITE_END();
    
  public:
    void setUp();

  protected:
    void test_lifecycle();
    void test_simulation();

    AMB::Device* device_m;
    HWSimulator simulator_m;
    
};

CPPUNIT_TEST_SUITE_REGISTRATION( HWSimulatorTestCase );

void HWSimulatorTestCase::setUp()
{
    std::vector<CAN::byte_t> sn(8, '0');
    //device_m = new AMB::EchoDevice(0x0, sn);
    device_m = new AMB::AmbDevIOTestDevice(0x0, sn);
    
}

void HWSimulatorTestCase::test_lifecycle()
{
    CPPUNIT_ASSERT_EQUAL(false, simulator_m.isSimulating());
    CPPUNIT_ASSERT_THROW(simulator_m.start(),ControlExceptions::CAMBErrorExImpl);

    CPPUNIT_ASSERT_NO_THROW(simulator_m.setDevice(device_m));

    CPPUNIT_ASSERT_NO_THROW(simulator_m.start());
    CPPUNIT_ASSERT_EQUAL(true, simulator_m.isSimulating());

    ACE_OS::sleep(2);
    CPPUNIT_ASSERT_NO_THROW(simulator_m.stop());
    CPPUNIT_ASSERT_EQUAL(false, simulator_m.isSimulating());
	
}

void HWSimulatorTestCase::test_simulation()
{
    const unsigned short MAX_COMMANDS = 100;
    CPPUNIT_ASSERT_NO_THROW(simulator_m.setDevice(device_m));

    CPPUNIT_ASSERT_NO_THROW(simulator_m.start());
    CPPUNIT_ASSERT_EQUAL(true, simulator_m.isSimulating());

    AmbTransaction_t requestType;
    AmbRelativeAddr  requestRCA;
    ACS::Time   requestTime[MAX_COMMANDS];
    AmbDataLength_t dataLength[MAX_COMMANDS]; 
    AmbDataMem_t data[MAX_COMMANDS];
    sem_t        synchLock[MAX_COMMANDS];
    Time         timestamp[MAX_COMMANDS];
    AmbErrorCode_t status[MAX_COMMANDS];
    

    requestType = AMB_CONTROL;
    requestTime[0] = 0;
    requestRCA  = 0x00081;

    data[0]       = static_cast<AmbDataMem_t>(true);
    dataLength[0] = static_cast<AmbDataLength_t>(sizeof(bool));
    sem_init(&synchLock[0],0,0);
    simulator_m.insert(requestType, requestTime[0], requestRCA,
		       dataLength[0], 
		       &data[0], 
		       &synchLock[0], 
		       &timestamp[0],
		       &status[0]);
    sem_wait(&synchLock[0]);
    sem_destroy(&synchLock[0]);
    CPPUNIT_ASSERT_EQUAL(AMBERR_NOERR, status[0]);
    CPPUNIT_ASSERT_EQUAL(static_cast<ACS::Time>(0), timestamp[0]);

    requestType = AMB_MONITOR;
    requestTime[0] = 1;
    requestRCA  = 0x00001;

    data[0]       = static_cast<AmbDataMem_t>(true);
    dataLength[0] = static_cast<AmbDataLength_t>(0);
    sem_init(&synchLock[0],0,0);
    simulator_m.insert(requestType, requestTime[0], requestRCA,
		       dataLength[0], 
		       &data[0], 
		       &synchLock[0], 
		       &timestamp[0],
		       &status[0]);
    sem_wait(&synchLock[0]);
    sem_destroy(&synchLock[0]);
    CPPUNIT_ASSERT_EQUAL(AMBERR_NOERR, status[0]);
    CPPUNIT_ASSERT_EQUAL(static_cast<ACS::Time>(1), timestamp[0]);
    CPPUNIT_ASSERT_EQUAL(1, static_cast<int>(dataLength[0]));
    CPPUNIT_ASSERT_EQUAL(false, static_cast<bool>(data[0]));

    for(unsigned short index = 1; index < MAX_COMMANDS; index++)
	{
	dataLength[index] = static_cast<AmbDataLength_t>(0);
	requestType = AMB_MONITOR;
	requestTime[index] = MAX_COMMANDS-index;
	requestRCA  = 0x00001;
	sem_init(&synchLock[index],0,0);
	simulator_m.insert(requestType, requestTime[index], requestRCA,
			   dataLength[index], 
			   &data[index], 
			   &synchLock[index], 
			   &timestamp[index],
			   &status[index]);
	}

    ACS::Time        flushedTimestamp;
    AmbErrorCode_t   flushedStatus;
    simulator_m.flush(static_cast<ACS::Time>(50),
		      &flushedTimestamp,
		      &flushedStatus);

    for(unsigned short index = 1; index < MAX_COMMANDS; index++)
	{
	sem_wait(&synchLock[index]);
	sem_destroy(&synchLock[index]);
	}

    CPPUNIT_ASSERT_EQUAL(AMBERR_NOERR, status[MAX_COMMANDS-1]);
    CPPUNIT_ASSERT_EQUAL(static_cast<ACS::Time>(1), timestamp[MAX_COMMANDS-1]);
    CPPUNIT_ASSERT_EQUAL(1, static_cast<int>(dataLength[MAX_COMMANDS-1]));
    CPPUNIT_ASSERT_EQUAL(false, static_cast<bool>(data[MAX_COMMANDS-1]));

    CPPUNIT_ASSERT_EQUAL(AMBERR_FLUSHED, status[1]);

    // TEST LONG
    {
    long testData = 10;
    requestType = AMB_CONTROL;
    requestTime[0] = 0;
    requestRCA  = 0x0008A;

    AmbDataLength_t dataLength;
    AmbDataMem_t* controlData;
    dataLength    = static_cast<AmbDataLength_t>(sizeof(long));
    controlData   = reinterpret_cast<AmbDataMem_t*>(&testData);
    sem_init(&synchLock[0],0,0);
    simulator_m.insert(requestType, requestTime[0], requestRCA,
		       dataLength, 
		       controlData, 
		       &synchLock[0], 
		       &timestamp[0],
		       &status[0]);
    sem_wait(&synchLock[0]);
    sem_destroy(&synchLock[0]);
    CPPUNIT_ASSERT_EQUAL(AMBERR_NOERR, status[0]);
    CPPUNIT_ASSERT_EQUAL(static_cast<ACS::Time>(0), timestamp[0]);

    long replyData=0;
    AmbDataLength_t replyLength=0;
    requestType = AMB_MONITOR;
    requestTime[0] = 1;
    requestRCA  = 0x0000A;
    
    sem_init(&synchLock[0],0,0);
    simulator_m.insert(requestType, requestTime[0], requestRCA,
		       replyLength, 
		       reinterpret_cast< AmbDataMem_t* >(&replyData), 
		       &synchLock[0], 
		       &timestamp[0],
		       &status[0]);
    sem_wait(&synchLock[0]);
    sem_destroy(&synchLock[0]);
    CPPUNIT_ASSERT_EQUAL(AMBERR_NOERR, status[0]);
    CPPUNIT_ASSERT_EQUAL(static_cast<ACS::Time>(1), timestamp[0]);
    CPPUNIT_ASSERT_EQUAL(sizeof(long), static_cast<unsigned int>(replyLength));
    CPPUNIT_ASSERT_EQUAL(10L, replyData);
    }



    CPPUNIT_ASSERT_NO_THROW(simulator_m.stop());
    CPPUNIT_ASSERT_EQUAL(false, simulator_m.isSimulating());
	
}

/*
 * Main function running the tests
 */ 
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>

int
main( int argc, char* argv[] )
{
  // Create the event manager and test controller
  CPPUNIT_NS::TestResult controller;

  // Add a listener that colllects test result
  CPPUNIT_NS::TestResultCollector result;
  controller.addListener( &result );        

  // Add a listener that print dots as test run.
  CPPUNIT_NS::BriefTestProgressListener progress;
  controller.addListener( &progress );      

  // Add the top suite to the test runner
  CPPUNIT_NS::TestRunner runner;
  runner.addTest( CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest() );
  runner.run( controller );

  // Print test in a compiler compatible format.
  CPPUNIT_NS::CompilerOutputter outputter( &result, std::cerr );
  outputter.write(); 

  return result.wasSuccessful() ? 0 : 1;
}

