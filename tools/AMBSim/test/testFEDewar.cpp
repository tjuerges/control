/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2002 
*
* This code is under GNU General Public Licence (GPL). 
* Correspondence concerning ALMA Software should be addressed to:
* alma-sw-admin@nrao.edu
*
* Copyright (C) 2001
* Associated Universities, Inc. Washington DC, USA.
*
* Produced for the ALMA project
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Library General Public License
* as published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Library General Public License for more details.
*
* You should have received a copy of the GNU Library General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* aperrigo  30/11/02  created 
* aperrigo  03/12/02  modification due to new ICD
*/

#include "FEDewar.h"
#include "AMBUtil.h"
#include <iostream>
#include <assert.h>
#include <math.h>

using namespace std;

// Compare the direct result of a function call with the IEEE float value 
// that is obtained through the monitor
bool nearIEEE(const AMB::FEDewar &Device,
	      AMB::FEDewar::Monitor_t monitorName, 
	      float directValue, float trueValue)
{
    vector<CAN::byte_t> tmp = Device.monitor(monitorName);
    long tmpLong = AMB::dataToLong(tmp);
    float *monitorValue;
    monitorValue = reinterpret_cast<float *> (&tmpLong);
    assert(tmp.size() == 0);
    // cout << *monitorValue << directValue << endl;
    return 
	(fabs(*monitorValue - directValue) < .001) &&
	(fabs(*monitorValue - trueValue) < .001);
}

//------------------------------------------------------------------------------

int main()
{
    // Construct a FEDewar CAN device
    // FEDewar(node_t node, const vector<CAN::byte_t> &serialNumber);

    AMB::node_t nodeIn = 1234;
    vector<CAN::byte_t> snIn(8);
    for (int i=0; i<8; i++) {
	snIn[i] = i;
    }
    AMB::FEDewar FEDew(nodeIn, snIn);

    cout << "The only output following this line should be 'OK' and zero "
	"exit status" << endl;


    {
    // virtual ~FEDewar();
    // ??????? AMB::FEDewar FEDewTmp(nodeIn, snIn);
    // At the end of this block the destructor will be called.
    }

    // virtual node_t node() const;
    // virtual vector<CAN::byte_t> serialNumber() const;

    AMB::node_t nodeOut = FEDew.node();
    vector<CAN::byte_t> snOut = FEDew.serialNumber();
    assert(nodeOut == nodeIn);
    assert(snOut == snIn);

    // virtual vector<CAN::byte_t> monitor(rca_t rca) const;
    // virtual vector<CAN::byte_t> get_can_error() const;

    vector<CAN::byte_t> can_error_Out = FEDew.get_can_error();
    assert(can_error_Out[0] == 0 &&
	   can_error_Out[1] == 0 &&
	   can_error_Out[2] == 0);

    vector<CAN::byte_t> can_error_Out2 = 
      FEDew.monitor(AMB::FEDewar::GET_CAN_ERROR);
    assert(can_error_Out == can_error_Out2);
    
    // virtual vector<CAN::byte_t>  get_protocol_rev_level() const;
    vector <CAN::byte_t> prot_level_out = FEDew.get_protocol_rev_level();
    prot_level_out = FEDew.monitor(AMB::FEDewar::GET_PROTOCOL_REV_LEVEL);
    assert( (prot_level_out == 
	FEDew.monitor(AMB::FEDewar::GET_PROTOCOL_REV_LEVEL)) &&
	    (prot_level_out[0] == 0) &&
	    (prot_level_out[1] == 0) &&
	    (prot_level_out[2] == 0) );

    // virtual void control(rca_t rca, const vector<CAN::byte_t> &data);
    // void set_clr_intrf_ls218(CAN::byte_t dummy);
    vector<CAN::byte_t> dummy;
    dummy = AMB::charToData(dummy, CAN::byte_t(1));
    FEDew.control(AMB::FEDewar::SET_CLR_INTRF_LS218, dummy);

    // virtual unsigned int get_trans_num() const;
    assert(FEDew.get_trans_num() == 4);
    vector<CAN::byte_t> tmp = FEDew.monitor(AMB::FEDewar::GET_TRANS_NUM);
    assert(AMB::dataToLong(tmp) == 5 && tmp.size()==0); 

    // float get_ambient_temperature() const;
    assert (FEDew.get_ambient_temperature() == 18.0);
    tmp = FEDew.monitor(AMB::FEDewar::GET_AMBIENT_TEMPERATURE);
    assert(fabs(AMB::dataToFloatDS1820(tmp) - 18.0) < 0.01);
    assert(tmp.size() == 0);

    // float get_gm_supply_pressure() const;
    assert(nearIEEE(FEDew, AMB::FEDewar::GET_GM_SUPPLY_PRESSURE,
		FEDew.get_gm_supply_pressure(), 19.0));

    // float get_gm_return_pressure() const;
    assert(nearIEEE(FEDew, AMB::FEDewar::GET_GM_RETURN_PRESSURE,
		FEDew.get_gm_return_pressure(), 20.0));

    // float get_jt_supply_pressure() const;
    assert(nearIEEE(FEDew, AMB::FEDewar::GET_JT_SUPPLY_PRESSURE,
		FEDew.get_jt_supply_pressure(), 21.0));

    // float get_jt_return_pressure() const;
    assert(nearIEEE(FEDew, AMB::FEDewar::GET_JT_RETURN_PRESSURE,
		FEDew.get_jt_return_pressure(), 22.0));

    // float get_jt_return_flow() const;
    assert(nearIEEE(FEDew, AMB::FEDewar::GET_JT_RETURN_FLOW,
		FEDew.get_jt_return_flow(), 23.0));

    // vector<CAN::byte_t> get_dewar_valve_state() const;
    vector<CAN::byte_t> valve_state = 
	FEDew.monitor(AMB::FEDewar::GET_DEWAR_VALVE_STATE);
    assert(valve_state.size() == 1 && 
	   valve_state[0] == 1 );

    // float get_gm_1st_stage() const;
    assert(nearIEEE(FEDew, AMB::FEDewar::GET_GM_1ST_STAGE,
	     FEDew.get_gm_1st_stage(), 24.0));

    // float get_gm_2nd_stage() const;
    assert(nearIEEE(FEDew, AMB::FEDewar::GET_GM_2ND_STAGE,
	     FEDew.get_gm_2nd_stage(), 25.0));

    // float get_4k_stage() const;
    assert(nearIEEE(FEDew, AMB::FEDewar::GET_4K_STAGE,
	     FEDew.get_4k_stage(), 26.0));

    // float get_top_shield_temp() const;
    assert(nearIEEE(FEDew, AMB::FEDewar::GET_TOP_SHIELD_TEMP,
	     FEDew.get_top_shield_temp(), 27.0));

    // float get_band3_t1() const;
    assert(nearIEEE(FEDew, AMB::FEDewar::GET_BAND3_T1,
	     FEDew.get_band3_t1(), 28.0));

    // float get_band3_t2() const;
    assert(nearIEEE(FEDew, AMB::FEDewar::GET_BAND3_T2,
	     FEDew.get_band3_t2(), 29.0));

    // float get_band6_t1() const;
    assert(nearIEEE(FEDew, AMB::FEDewar::GET_BAND6_T1,
	     FEDew.get_band6_t1(), 30.0));

    // float get_band6_t2() const;
    assert(nearIEEE(FEDew, AMB::FEDewar::GET_BAND6_T2,
	     FEDew.get_band6_t2(), 31.0));

    // float get_dwr_vacuum() const;
    assert(nearIEEE(FEDew, AMB::FEDewar::GET_DWR_VACCUM,
	     FEDew.get_dwr_vacuum(), 32.0));

    // float get_pump_port_vacuum() const;
    assert(nearIEEE(FEDew, AMB::FEDewar::GET_PUMP_PORT_VACUUM,
	     FEDew.get_pump_port_vacuum(), 33.0));

    // vector<CAN::byte_t> get_interface_status() const;
    vector<CAN::byte_t> interface_status = 
	FEDew.monitor(AMB::FEDewar::GET_INTERFACE_STATUS);
    assert(interface_status.size() == 8 && 
	   interface_status[0] == 'N' &&
	   interface_status[1] == 'o' &&
	   interface_status[2] == 'E' &&
	   interface_status[3] == 'r' &&
	   interface_status[4] == 'r' &&
	   interface_status[5] == 'o' &&
	   interface_status[6] == 'r' &&
	   interface_status[7] == 0);

    cout << "OK" << endl;
    return 0;
}
