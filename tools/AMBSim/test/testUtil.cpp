// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include "AMBUtil.h"
#include <iostream>
#include <assert.h>
#include <math.h>
// !!! DELETE ME !!!
#include <stdio.h>
// !!! DELETE ME !!!
#include <iomanip>

using namespace std;
//
// Test the static AMB::functions in AMBUtil.h and AMBUtil.cpp
//

#undef DEBUG

//------------------------------------------------------------------------------

int main()
{
    cout << "The only output should be this line, followed by 'OK' and zero "
	"exit status" << endl;

    vector<CAN::byte_t> msg(8);
    const vector<CAN::byte_t> nostring;

    ///////////////////////////////////////////////////////////////////////
    //                                                                   //
    //               test doubleToData(), dataToDouble                   //
    //                                                                   //
    ///////////////////////////////////////////////////////////////////////
    try
	{
	for (double inData = -0.123456789; 
	     inData < 0.123456789; inData += 0.023)
	    {
	    msg = AMB::doubleToData(inData);
	    double outData = AMB::dataToDouble(msg);
	    assert(inData == outData);
	    }
	}
    catch(...)
	{
	cout << "exception from dataToDouble(), doubleToData()" << endl;
	}

    ///////////////////////////////////////////////////////////////////////
    //                                                                   //
    //                test floatToData(), dataToFloat                    //
    //                                                                   //
    ///////////////////////////////////////////////////////////////////////
    try
	{
	for (double inData = 0.0; inData <= 500.0; inData += 1.234)
	    {
	    for (int nbits = 4; nbits <= 32; nbits += 4)
		{
		double low = 0.0;
		double high = 500.0;
		msg = AMB::floatToData(nostring,inData,low,high,nbits);
		double outData = AMB::dataToFloat(msg,low,high,nbits);
		double tolerance = (high - low) / ((long long)1 << nbits);
		assert(fabs(inData - outData) < tolerance);
		}
	    }
	}
    catch(...)
	{
	cout << "exception from dataToFloat(), floatToData()" << endl;
	}

    ///////////////////////////////////////////////////////////////////////
    //                                                                   //
    //          test floatDS1820ToData(), dataToFloatDS1820              //
    //                                                                   //
    ///////////////////////////////////////////////////////////////////////
    try
	{
	for (double inTemp = -40.0; inTemp < 40; inTemp += 1.0)
	    {
	    msg = AMB::floatDS1820ToData(nostring,inTemp);
	    double outTemp = AMB::dataToFloatDS1820(msg);
	    double tolerance = 100.0 / 256;
	    assert(fabs(inTemp - outTemp) < tolerance);
	    }
	}
    catch(...)
	{
	cout << "exception from dataToFloatDS1820(), floatDS1820ToData()"
	     << endl;
	}

    ///////////////////////////////////////////////////////////////////////
    //                                                                   //
    //             test longlongToData(), dataToLonglong                 //
    //                                                                   //
    ///////////////////////////////////////////////////////////////////////
    try
	{
	for (int i = 1; i < 9; i++)
	    {
	    unsigned long long inData = (unsigned long long)0x0123456789ABCDEF;
	    int shift = 64 - i * 8;
	    inData <<= shift;
	    inData >>= shift;
	    
	    msg = AMB::longlongToData(nostring,inData,i);
	    assert(msg.size() == (unsigned)i);
#ifdef DEBUG
	    cout << "longlong inData=0x" << hex << inData << endl;
	    for (int j = 0; j < i; j++)
		cout << "0x" << hex << (int)msg[j] << " ";
	    cout << endl;
#endif /* DEBUG */

	    unsigned long long outData = AMB::dataToLonglong(msg,i);
	    assert(msg.size() == 0);
#ifdef DEBUG
	    cout << "longlong outData=0x" << hex << outData << endl;
#endif /* DEBUG */
	    assert(inData == outData);
	    }
	}
    catch(...)
	{
	cout << "exception from longlongToData(), dataToLonglong()" << endl;
	}

    ///////////////////////////////////////////////////////////////////////
    //                                                                   //
    //                 test longToData(), dataToLong                     //
    //                                                                   //
    ///////////////////////////////////////////////////////////////////////
    try
	{
	for (long i = -2147483647; i < 2000000000; i += 8589934)  // ~500 steps
	    {
	    long inData1 = i;
	    long inData2 = (long)0x89ABCDEF;
	    
	    msg = AMB::longToData(nostring,inData1);
	    assert(msg.size() == 4);
	    msg = AMB::longToData(msg,inData2);
	    assert(msg.size() == 8);
#ifdef DEBUG
	    cout << "long inData1=0x" << hex << inData1 
		    << ", inData2=0x" << hex << inData2 << endl;
	    for (int j = 0; j < 8; j++)
		cout << "0x" << hex << (int)msg[j] << " ";
	    cout << endl;
#endif /* DEBUG */

	    long outData1 = AMB::dataToLong(msg);
	    assert(msg.size() == 4);
	    long outData2 = AMB::dataToLong(msg);
	    assert(msg.size() == 0);
#ifdef DEBUG
	    cout << "long outData1=0x" << hex << outData1 
		    << ", outData2=0x" << hex << outData2 << endl;
#endif /* DEBUG */
	    assert(inData1 == outData1);
	    assert(inData2 == outData2);
	    }
	}
    catch(...)
	{
	cout << "exception from longToData(), dataToLong()" << endl;
	}

    ///////////////////////////////////////////////////////////////////////
    //                                                                   //
    //                test shortToData(), dataToShort                    //
    //                                                                   //
    ///////////////////////////////////////////////////////////////////////
    try
	{
	for (short i = -32767; i < 30000; i += 131)  // ~500 steps
	    {
	    short inData1 = i;
	    short inData2 = (short)0xFEDC;
	    short inData3 = i + 62;
	    short inData4 = (short)0x89AB;
	    
	    msg = AMB::shortToData(nostring,inData1);
	    assert(msg.size() == 2);
	    msg = AMB::shortToData(msg,inData2);
	    assert(msg.size() == 4);
	    msg = AMB::shortToData(msg,inData3);
	    assert(msg.size() == 6);
	    msg = AMB::shortToData(msg,inData4);
	    assert(msg.size() == 8);
#ifdef DEBUG
	    cout << "short inData1=0x" << hex << inData1 
		    << ", inData2=0x" << hex << inData2 << endl;
	    for (int j = 0; j < 8; j++)
		cout << "0x" << hex << (int)msg[j] << " ";
	    cout << endl;
#endif /* DEBUG */

	    short outData1 = AMB::dataToShort(msg);
	    assert(msg.size() == 6);
	    short outData2 = AMB::dataToShort(msg);
	    assert(msg.size() == 4);
	    short outData3 = AMB::dataToShort(msg);
	    assert(msg.size() == 2);
	    short outData4 = AMB::dataToShort(msg);
	    assert(msg.size() == 0);
#ifdef DEBUG
	    cout << "short outData1=0x" << hex << outData1 
		    << ", outData2=0x" << hex << outData2 << endl;
#endif /* DEBUG */
	    assert(inData1 == outData1);
	    assert(inData2 == outData2);
	    assert(inData3 == outData3);
	    assert(inData4 == outData4);
	    }
	}
    catch(...)
	{
	cout << "exception from shortToData(), dataToShort()" << endl;
	}

    ///////////////////////////////////////////////////////////////////////
    //                                                                   //
    //                 test charToData(), dataToChar                     //
    //                                                                   //
    ///////////////////////////////////////////////////////////////////////
    try
	{
	for (char i = -127; i < 100; i += 1)
	    {
	    char inData[8];
	    for (int j = 0; j < 8; j++)
		{
		if (j % 3 == 1)
		    inData[j] = (char)0xEF - j * 2;
		else
		    inData[j] = i + j;
		}
    
	    msg = nostring;
	    for (int j = 0; j < 8; j++)
		{
		msg = AMB::charToData(msg,inData[j]);
		assert(msg.size() == (unsigned int)j + 1);
		}
#ifdef DEBUG
	    for (int j = 0; j < 8; j++)
		cout << "0x" << hex << (int)msg[j] << " ";
	    cout << endl;
#endif /* DEBUG */

	    char outData[8];
	    for (int j = 0; j < 8; j++)
		{
		outData[j] = AMB::dataToChar(msg);
		assert(msg.size() == 7 - (unsigned int)j);
		}
	    for (int j = 0; j < 8; j++)
		assert(inData[j] == outData[j]);
	    }
	}
    catch(...)
	{
	cout << "exception from charToData(), dataToChar()" << endl;
	}

    cout << "OK" << endl;
    return 0;
}
