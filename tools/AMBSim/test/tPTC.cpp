// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include "PTC.h"
#include "AMBUtil.h"
#include <iostream>
#include <assert.h>
#include <math.h>

using namespace std;
/*
 * This is a test I wrote when I was trying to find a problem reading and 
 * writing the PTC coefficients.  Much of it was taken directly from 
 * tCompressor.cpp.  It is far from a complete test of the PTC.
 * R.Heald  2002-07-26
 */

//------------------------------------------------------------------------------

int main()
{
    cout << "The only output should be this line, followed by 'OK' and zero "
	"exit status" << endl;

    AMB::node_t nodeIn = 1234;
    vector<CAN::byte_t> snIn(8);
    for (int i=0; i<8; i++) {
	snIn[i] = i;
    }
    AMB::PTC ptc(nodeIn, snIn);

    // call destructor
    {
	AMB::PTC ptcTmp(nodeIn, snIn);
	// At the end of this block the destructor will be called.
    }

    // test node, serial no.
    AMB::node_t nodeOut = ptc.node();
    vector<CAN::byte_t> snOut = ptc.serialNumber();
    assert(nodeOut == nodeIn);
    assert(snOut == snIn);

    // test can error count
    vector<CAN::byte_t> can_error_Out = ptc.get_can_error();
    assert(can_error_Out[0] == 0
	&& can_error_Out[1] == 0
	&& can_error_Out[2] == 0);
    vector<CAN::byte_t> can_error_Out2 = 
      ptc.monitor(AMB::PTC::GET_CAN_ERROR);
    assert(can_error_Out == can_error_Out2);

    // test protocol revision level
    vector <CAN::byte_t> prot_level_out = ptc.get_protocol_rev_level();
    assert( (prot_level_out == 
	ptc.monitor(AMB::PTC::GET_PROTOCOL_REV_LEVEL)) &&
	    (prot_level_out[0] == 0)
	 && (prot_level_out[1] == 0)
	 && (prot_level_out[2] == 0));

    // test transaction count
    assert(ptc.get_trans_num() == 2);
    vector<CAN::byte_t> tmp = ptc.monitor(AMB::PTC::GET_TRANS_NUM); //+1
    assert(AMB::dataToLong(tmp) == 3 && tmp.size() == 0); 

    // test control() and monitor() with model coefficients
    const double tolerance = 1.0e-14;
    for (double value = -123.456; value < 123.456; value += 2.45)
	{
	for (int i = 0; i < 7; i++)
	    {
	    double additive = 0.12345 * i;
	    vector<CAN::byte_t> CANin;
	    CANin = AMB::doubleToData(value + additive);
	    ptc.control(AMB::PTC::SET_PT_MODEL_COEFF_0 + i,CANin);
	    vector<CAN::byte_t> CANout;
	    CANout = ptc.monitor(AMB::PTC::GET_PT_MODEL_COEFF_0 + i);
	    assert(CANin == CANout);
	    double outVal = AMB::dataToDouble(CANout);
	    assert(fabs(outVal - (value + additive)) < tolerance);
	    }
	}

    cout << "OK" << endl;
    return 0;
}
