// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include "Compressor.h"
#include "AMBUtil.h"
#include <iostream>
#include <assert.h>
#include <math.h>

using namespace std;

// Compare the direct result of a function call with the value that is obtained
// through the monitor
bool near(const AMB::Compressor &onCompressor,
	  const AMB::Compressor &offCompressor,
	  AMB::Compressor::Monitor_t monitorName, 
	  float directOnValue, float directOffValue,
	  float trueOnValue, float trueOffValue,
	  float low, float high, int nbits)
{
    vector<CAN::byte_t> tmp = onCompressor.monitor(monitorName);
    float monitorOnValue = AMB::dataToFloat(tmp, low, high, nbits);
    assert(tmp.size() == 0);
    tmp = offCompressor.monitor(monitorName);
    float monitorOffValue = AMB::dataToFloat(tmp, low, high, nbits);
    assert(tmp.size() == 0);

    // 12 bits and a range of 0-500 implies values should be good to nearly 0.1.
    // So, 0.2 should be plenty of slack.
    return 
	(fabs(monitorOnValue - directOnValue) < 0.2) &&
	(fabs(monitorOnValue - trueOnValue) < 0.2) &&
	(fabs(monitorOffValue - directOffValue) < 0.2) &&
	(fabs(monitorOffValue - trueOffValue) < 0.2);

    //    return fabs(value - compareTo) < 0.2; // 0.2 should be plenty.
}

//------------------------------------------------------------------------------

int main()
{
    cout << "The only output should be this line, followed by 'OK' and zero "
	"exit status" << endl;

    // This is still a fairly simple mined test program (hey, it exists!).
    // Mostly just execute all the member functions in a straightforward way
    // with the varying fridge states. It is considered to be useful to test
    // both the direct function call and the monitor() and control() versions
    // to make sure that the switch statements are OK.

//  	Compressor(node_t node, const vector<CAN::byte_t> &serialNumber);

    AMB::node_t nodeIn = 1234;
    vector<CAN::byte_t> snIn(8);
    for (int i=0; i<8; i++) {
	snIn[i] = i;
    }
    AMB::Compressor comp(nodeIn, snIn);

    {
//  	virtual ~Compressor();
	AMB::Compressor compTmp(nodeIn, snIn);
	// At the end of this block the destructor will be called.
    }

//  	virtual node_t node() const;
//  	virtual vector<CAN::byte_t> serialNumber() const;
    AMB::node_t nodeOut = comp.node();
    vector<CAN::byte_t> snOut = comp.serialNumber();
    assert(nodeOut == nodeIn);
    assert(snOut == snIn);

//  	virtual vector<CAN::byte_t> monitor(rca_t rca) const;
//  	virtual vector<CAN::byte_t>  get_can_error() const;

    vector<CAN::byte_t> can_error_Out = comp.get_can_error();
    assert(can_error_Out[0] == 0 &&
	   can_error_Out[1] == 0 &&
	   can_error_Out[2] == 0);

    vector<CAN::byte_t> can_error_Out2 = 
      comp.monitor(AMB::Compressor::GET_CAN_ERROR);
    assert(can_error_Out == can_error_Out2);

    
//  	virtual vector<CAN::byte_t>  get_protocol_rev_level() const;

    vector <CAN::byte_t> prot_level_out = comp.get_protocol_rev_level();
    assert( (prot_level_out == 
	comp.monitor(AMB::Compressor::GET_PROTOCOL_REV_LEVEL)) &&
	    (prot_level_out[0] == 0) &&
	    (prot_level_out[1] == 0) &&
	    (prot_level_out[2] == 0) );

//  	CAN::byte_t get_comp_state() const;
//  	CAN::byte_t get_fridge_drive_state() const;
    assert(comp.get_comp_state() == 0);
    assert(comp.get_fridge_drive_state() == 0);

//  	virtual void control(rca_t rca, const vector<CAN::byte_t> &data);
//  	void set_comp_state(CAN::byte_t state);
//  	void set_fridge_state(CAN::byte_t state);
    {
      vector<CAN::byte_t> tmp;
      tmp = AMB::charToData(tmp, CAN::byte_t(1));
      comp.control(AMB::Compressor::SET_COMP_STATE, tmp);
      comp.control(AMB::Compressor::SET_FRIDGE_STATE, tmp);
      assert(comp.get_comp_state() == 1);
      assert(comp.get_fridge_drive_state() == 1);
      tmp = comp.monitor(AMB::Compressor::GET_COMP_STATE);
      CAN::byte_t tmp2 = AMB::dataToChar(tmp);
      assert( (tmp2 == 1) && (tmp.size() == 0) );

      comp.set_fridge_state(0);
      assert(comp.get_fridge_drive_state() == 0);
      comp.set_fridge_state(1); // Leave it on
    }

//  	virtual unsigned int get_trans_num() const;
    assert(comp.get_trans_num() == 5);
    vector<CAN::byte_t> tmp = comp.monitor(AMB::Compressor::GET_TRANS_NUM); //+1
    assert(AMB::dataToLong(tmp) == 6 && tmp.size()==0); 

//  	float get_ambient_temperature() const;
    assert (comp.get_ambient_temperature() == 18.0);
    tmp = comp.monitor(AMB::Compressor::GET_AMBIENT_TEMPERATURE);
    assert(fabs(AMB::dataToFloatDS1820(tmp) - 18.0) < 0.01);
    assert(tmp.size() == 0);

    // Make a compressor in which the drive and fridge are OFF to save always
    // toggling them back and forth.
    AMB::Compressor compOff(nodeIn, snIn);

//  	float get_control_box_temp() const;
    assert(near(comp, compOff,
		AMB::Compressor::GET_CONTROL_BOX_TEMP,
		comp.get_control_box_temp(), compOff.get_control_box_temp(),
		40.0, 20.0, 
		0.0, 500.0, 12));

//  	float get_crosshead_current() const;
    assert(near(comp, compOff,
		AMB::Compressor::GET_CROSSHEAD_CURRENT,
		comp.get_crosshead_current(), compOff.get_crosshead_current(),
		2.5, 0.0, 
		0.0, 5.0, 12));

//  	float get_elec_cage_temp() const;
    assert(near(comp, compOff,
		AMB::Compressor::GET_ELEC_CAGE_TEMP,
		comp.get_elec_cage_temp(), compOff.get_elec_cage_temp(),
		41.0, 21.0, 
		0.0, 500.0, 12));

//  	float get_fan_air_temp() const;
    assert(near(comp, compOff,
		AMB::Compressor::GET_FAN_AIR_TEMP,
		comp.get_fan_air_temp(), compOff.get_fan_air_temp(),
		42.0, 22.0, 
		0.0, 500.0, 12));

//  	float get_fan_motor_temp() const;
    assert(near(comp, compOff,
		AMB::Compressor::GET_FAN_MOTOR_TEMP,
		comp.get_fan_motor_temp(), compOff.get_fan_motor_temp(),
		43.0, 23.0, 
		0.0, 500.0, 12));

//  	float get_gm_head_temp() const;
    assert(near(comp, compOff,
		AMB::Compressor::GET_GM_HEAD_TEMP,
		comp.get_gm_head_temp(), compOff.get_gm_head_temp(),
		44.0, 24.0, 
		0.0, 500.0, 12));

//  	float get_gm_htex_temp() const;
    assert(near(comp, compOff,
		AMB::Compressor::GET_GM_HTEX_TEMP,
		comp.get_gm_htex_temp(), compOff.get_gm_htex_temp(),
		45.0, 25.0, 
		0.0, 500.0, 12));

//  	float get_gm_oil_temp() const;
    assert(near(comp, compOff,
		AMB::Compressor::GET_GM_OIL_TEMP,
		comp.get_gm_oil_temp(), compOff.get_gm_oil_temp(),
		46.0, 26.0, 
		0.0, 500.0, 12));

//  	float get_gm_oilhex_temp() const;
    assert(near(comp, compOff,
		AMB::Compressor::GET_GM_OILHEX_TEMP,
		comp.get_gm_oilhex_temp(), compOff.get_gm_oilhex_temp(),
		47.0, 27.0, 
		0.0, 500.0, 12));

//  	float get_gm_return_pressure() const;
    assert(near(comp, compOff,
		AMB::Compressor::GET_GM_RETURN_PRESSURE,
		comp.get_gm_return_pressure(), compOff.get_gm_return_pressure(),
		280.0, 200.0, 
		0.0, 500.0, 12));

//  	float get_gm_supply_pressure() const;
    assert(near(comp, compOff,
		AMB::Compressor::GET_GM_SUPPLY_PRESSURE,
		comp.get_gm_supply_pressure(), compOff.get_gm_supply_pressure(),
		280.0, 200.0, 
		0.0, 500.0, 12));

//  	float get_jt_head_temp() const;
    assert(near(comp, compOff,
		AMB::Compressor::GET_JT_HEAD_TEMP,
		comp.get_jt_head_temp(), compOff.get_jt_head_temp(),
		48.0, 28.0, 
		0.0, 500.0, 12));

//  	float get_jt_oil_temp() const;
    assert(near(comp, compOff,
		AMB::Compressor::GET_JT_OIL_TEMP,
		comp.get_jt_oil_temp(), compOff.get_jt_oil_temp(),
		49.0, 29.0, 
		0.0, 500.0, 12));

//  	float get_jt_oilhex_temp() const;
    assert(near(comp, compOff,
		AMB::Compressor::GET_JT_OILHEX_TEMP,
		comp.get_jt_oilhex_temp(), compOff.get_jt_oilhex_temp(),
		50.0, 30.0, 
		0.0, 500.0, 12));

//  	float get_jt_return_pressure() const;
    assert(near(comp, compOff,
		AMB::Compressor::GET_JT_RETURN_PRESSURE,
		comp.get_jt_return_pressure(), compOff.get_jt_return_pressure(),
		-10.5, 100.0,
		-14.6, 128.65, 12));

//  	float get_res_tank_pressure() const;
    assert(near(comp, compOff,
		AMB::Compressor::GET_RES_TANK_PRESSURE,
		comp.get_res_tank_pressure(), compOff.get_res_tank_pressure(),
		120.0, 80.0,
		0.0, 500.0, 12));

//  	vector<CAN::byte_t> get_status() const;
    vector<CAN::byte_t> statusOut = comp.monitor(AMB::Compressor::GET_STATUS);
    assert(statusOut.size() == 2 && statusOut[0] == 0 &&
	   statusOut[1] == 7);
    statusOut = compOff.get_status();
    assert(statusOut.size() == 2 && statusOut[0] == 0 &&
	   statusOut[1] == 0);

    cout << "OK" << endl;
    return 0;
}
