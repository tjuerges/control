/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2002 
*
* This code is under GNU General Public Licence (GPL). 
* Correspondence concerning ALMA Software should be addressed to:
* alma-sw-admin@nrao.edu
*
* Copyright (C) 2001
* Associated Universities, Inc. Washington DC, USA.
*
* Produced for the ALMA project
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Library General Public License
* as published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Library General Public License for more details.
*
* You should have received a copy of the GNU Library General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* aperrigo  08/12/02  created 
*/

#include "LO1stCont.h"
#include "AMBUtil.h"
#include <iostream>
#include <assert.h>
#include <math.h>

using namespace std;

// Compare the direct result of a function call with the IEEE float value 
// that is obtained through the monitor
bool nearIEEE(const AMB::LO1stCont &Device,
	      AMB::LO1stCont::Monitor_t monitorName, 
	      float directValue, float trueValue)
{
    vector<CAN::byte_t> tmp = Device.monitor(monitorName);
    long tmpLong = AMB::dataToLong(tmp);
    float *monitorValue;
    monitorValue = reinterpret_cast<float *> (&tmpLong);
    assert(tmp.size() == 0);
    // cout << *monitorValue << directValue << endl;
    return 
	(fabs(*monitorValue - directValue) < .001) &&
	(fabs(*monitorValue - trueValue) < .001);
}
//------------------------------------------------------------------------------

int main()
{
    vector<CAN::byte_t> tmp;
    vector<CAN::byte_t> data; 
    vector<CAN::byte_t> cmd(1); 

    // Construct a LO1stCont CAN device
    // LO1stCont(node_t node, const vector<CAN::byte_t> &serialNumber);

    AMB::node_t nodeIn = 1234;
    vector<CAN::byte_t> snIn(8);
    for (int i=0; i<8; i++) {
	snIn[i] = i;
    }
    AMB::LO1stCont LO(nodeIn, snIn);

    cout << "The only output following this line should be 'OK' and zero "
	"exit status" << endl;

    // virtual node_t node() const;
    // virtual vector<CAN::byte_t> serialNumber() const;

    AMB::node_t nodeOut = LO.node();
    vector<CAN::byte_t> snOut = LO.serialNumber();
    assert(nodeOut == nodeIn);
    assert(snOut == snIn);

    // virtual vector<CAN::byte_t> monitor(rca_t rca) const;
    // virtual vector<CAN::byte_t> get_can_error() const;

    vector<CAN::byte_t> can_error_Out = LO.get_can_error();
    assert(can_error_Out[0] == 0 &&
	   can_error_Out[1] == 0 &&
	   can_error_Out[2] == 0);

    vector<CAN::byte_t> can_error_Out2 = 
      LO.monitor(AMB::LO1stCont::GET_CAN_ERROR);
    assert(can_error_Out == can_error_Out2);
    
    // virtual vector<CAN::byte_t>  get_protocol_rev_level() const;
    vector <CAN::byte_t> prot_level_out = LO.get_protocol_rev_level();
    prot_level_out = LO.monitor(AMB::LO1stCont::GET_PROTOCOL_REV_LEVEL);
    assert((prot_level_out == 
	    LO.monitor(AMB::LO1stCont::GET_PROTOCOL_REV_LEVEL)) &&
	   (prot_level_out[0] == 0) &&
	   (prot_level_out[1] == 0) &&
	   (prot_level_out[2] == 0) );

    // virtual void control(rca_t rca, const vector<CAN::byte_t> &data);
    // void AMB::LO1stCont::set_lo_power_b6(vector<CAN::byte_t> data)
    data = AMB::shortToData(tmp, 0x5005);
    LO.control(AMB::LO1stCont::SET_LO_POWER_B6, data);

    // void AMB::LO1stCont::set_modulator_servo(CAN::byte_t cmd)
    cmd[0] = 1;
    LO.control(AMB::LO1stCont::SET_MODULATOR_SERVO, cmd);

    // void AMB::LO1stCont::set_drive_enable(CAN::byte_t cmd)
    cmd[0] = 1;
    LO.control(AMB::LO1stCont::SET_DRIVE_ENABLE, cmd);

    // void AMB::LO1stCont::set_loop_enable(CAN::byte_t cmd)
    cmd[0] = 1;
    LO.control(AMB::LO1stCont::SET_LOOP_ENABLE, cmd);

    // void AMB::LO1stCont::set_nominal_voltage(float value)
    data = LO.IEEEFloatToData(20.);
    LO.control(AMB::LO1stCont::SET_NOMINAL_VOLTAGE, data);

    // void AMB::LO1stCont::set_gunn_loop_gain(float value)
    data = LO.IEEEFloatToData(21.);
    LO.control(AMB::LO1stCont::SET_GUNN_LOOP_GAIN, data);

    // void AMB::LO1stCont::set_gunn_tune_range(float value)
    data = LO.IEEEFloatToData(22.);
    LO.control(AMB::LO1stCont::SET_GUNN_TUNE_RANGE, data);

    // void AMB::LO1stCont::set_gunn_select(CAN::byte_t cmd)
    cmd[0] = 1;
    LO.control(AMB::LO1stCont::SET_GUNN_SELECT, cmd);

    // void AMB::LO1stCont::set_fts_frequency(float value)
    data = LO.IEEEFloatToData(23.);
    LO.control(AMB::LO1stCont::SET_FTS_FREQUENCY, data);

    // void AMB::LO1stCont::set_photomixer_b3(CAN::byte_t cmd)
    cmd[0] = 1;
    LO.control(AMB::LO1stCont::SET_PHOTOMIXER_B3, cmd);

    // void AMB::LO1stCont::set_photomixer_b6(CAN::byte_t cmd)
    cmd[0] = 1;
    LO.control(AMB::LO1stCont::SET_PHOTOMIXER_B6, cmd);

    // void AMB::LO1stCont::set_band_select(CAN::byte_t cmd)
    cmd[0] = 1;
    LO.control(AMB::LO1stCont::SET_BAND_SELECT, cmd);

    // virtual unsigned int get_trans_num() const;
    assert(LO.get_trans_num() == 15);
    tmp = LO.monitor(AMB::LO1stCont::GET_TRANS_NUM);
    assert(AMB::dataToLong(tmp) == 16 && tmp.size()==0); 

    // float get_ambient_temperature() const;
    assert(LO.get_ambient_temperature() == 18.0);
    data = LO.monitor(AMB::LO1stCont::GET_AMBIENT_TEMPERATURE);
    assert(fabs(AMB::dataToFloatDS1820(data) - 18.0) < 0.01);
    assert(data.size() == 0);

    // vector<CAN::byte_t> get_lo_control_status() const;
    vector<CAN::byte_t> lo_control_status = 
	LO.monitor(AMB::LO1stCont::GET_LO_CONTROL_STATUS);
    assert(lo_control_status.size() == 1 && 
	   lo_control_status[0] == 0x55 );

    // float get_gunn_voltage_b3() const
    assert(nearIEEE(LO, AMB::LO1stCont::GET_GUNN_VOLTAGE_B3,
		LO.get_gunn_voltage_b3(), 10.0));

    // float get_gunn_voltage_b6() const
    assert(nearIEEE(LO, AMB::LO1stCont::GET_GUNN_VOLTAGE_B6,
		LO.get_gunn_voltage_b6(), 11.0));

    //
    data = AMB::shortToData(tmp, 0x1001);
    LO.control(AMB::LO1stCont::SET_GUNN_TUNE_COUNTS_B3, data);
    data = LO.monitor(AMB::LO1stCont::GET_GUNN_TUNE_COUNTS_B3);
    assert(data.size() == 2 && AMB::dataToShort(data) == 0x1001);

    //
    data = AMB::shortToData(tmp, 0x2002);
    LO.control(AMB::LO1stCont::SET_GUNN_BACKSHORT_COUNTS_B3, data);
    data = LO.monitor(AMB::LO1stCont::GET_GUNN_BACKSHORT_COUNTS_B3);
    assert(data.size() == 2 && AMB::dataToShort(data) == 0x2002);

    //
    data = AMB::shortToData(tmp, 0x3003);
    LO.control(AMB::LO1stCont::SET_GUNN_TUNE_COUNTS_B6, data);
    data = LO.monitor(AMB::LO1stCont::GET_GUNN_TUNE_COUNTS_B6);
    assert(data.size() == 2 && AMB::dataToShort(data) == 0x3003);

    //
    data = AMB::shortToData(tmp, 0x4004);
    LO.control(AMB::LO1stCont::SET_GUNN_BACKSHORT_COUNTS_B6, data);
    data = LO.monitor(AMB::LO1stCont::GET_GUNN_BACKSHORT_COUNTS_B6);
    assert(data.size() == 2 && AMB::dataToShort(data) == 0x4004);

    // float get_if_power() const
    assert(nearIEEE(LO, AMB::LO1stCont::GET_IF_POWER,
		LO.get_if_power(), 12.0));

    // float get_gunn_temp_b3() const
    assert(nearIEEE(LO, AMB::LO1stCont::GET_GUNN_TEMP_B3,
		LO.get_gunn_temp_b3(), 13.0));

    // float get_gunn_temp_b6() const
    assert(nearIEEE(LO, AMB::LO1stCont::GET_GUNN_TEMP_B6,
		LO.get_gunn_temp_b6(), 14.0));

    // float get_photo_current_b3() const
    assert(nearIEEE(LO, AMB::LO1stCont::GET_PHOTO_CURRENT_B3,
		LO.get_photo_current_b3(), 15.0));

    // float get_photo_voltage_b3() const
    assert(nearIEEE(LO, AMB::LO1stCont::GET_PHOTO_VOLTAGE_B3,
		LO.get_photo_voltage_b3(), 16.0));

    // float get_photo_current_b6() const
    assert(nearIEEE(LO, AMB::LO1stCont::GET_PHOTO_CURRENT_B6,
		LO.get_photo_current_b6(), 17.0));

    // float get_photo_voltage_b6() const
    assert(nearIEEE(LO, AMB::LO1stCont::GET_PHOTO_VOLTAGE_B6,
		LO.get_photo_voltage_b6(), 18.0));

    // float get_dro_mon_voltage() const
    assert(nearIEEE(LO, AMB::LO1stCont::GET_DRO_MON_VOLTAGE,
		LO.get_dro_mon_voltage(), 19.0));

    cout << "OK" << endl;
    return 0;
}
