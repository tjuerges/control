// @(#) $Id$
//
// Copyright (C) 2002
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

/** This tests the function calls of the HolographyReceiver & HolographyDSP
 ** classes which represent the Holography Measurement Equipment device
 ** simulator.
 ** This test displays 'Tests Successful' if all tests pass, otherwise
 ** error messages appear ending with 'Tests Failed'.
 */

#include "FEBias.h"
#include "AMBUtil.h"
#include <iostream>
#include <math.h>

using namespace std;

// Forward declaration for checking value for FEBias object
bool printFunctionConclusion(int expected, int actual,const char *pMonitorName, bool success); 
bool doGenericTests(AMB::FEBias &FEB);
bool doLNATests(AMB::FEBias &FEB);
bool doSchottkyTests(AMB::FEBias &FEB);
bool doSISMixerTests(AMB::FEBias &FEB);
bool doMagnetTests(AMB::FEBias &FEB);
bool monitorMatchesIEEEFloat( const vector<CAN::byte_t> &monitorBytes,
				float IEEEFloat,  const char *pMonitorName);


//------------------------------------------------------------------------------
/* This tests the FEBias class. It basically tests the get data point 
** monitor and control points.
*/

//------------------------------------------------------------------------------
// Perform tests of generic monitors included in the front-end bias & control module simulator. 
bool doGenericTests(AMB::FEBias &FEB)
{
    bool bTestsSuccessful = true;
    vector<CAN::byte_t> monitorBytes;
    int expectedNumberCANTransactions = 0;

    // Do monitor checks on FEBias generic monitor points
    // CAN errors should be 0
    monitorBytes = FEB.monitor( FEB.GET_CAN_ERROR );
    expectedNumberCANTransactions++;
    short shortTmp;
    if( (shortTmp = AMB::dataToShort(monitorBytes)) != 0 )
    {
	cerr << "CAN Error msg: " << shortTmp << " expected 0" << endl;
	bTestsSuccessful = false;
    }
    
    // Protocol rev level should be 0
    monitorBytes = FEB.monitor( FEB.GET_PROTOCOL_REV_LEVEL );
    expectedNumberCANTransactions++;
    if( (shortTmp = AMB::dataToShort(monitorBytes)) != 0 )
    {
	cerr << "Protocol Rev. Level Error: " << shortTmp << " expected 0" << endl;
	bTestsSuccessful = false;
    }

    // Ambient temperature is always 18.0
    float floatTmp;
    monitorBytes = FEB.monitor( FEB.GET_AMBIENT_TEMPERATURE );
    expectedNumberCANTransactions++;
    floatTmp = fabs(AMB::dataToFloatDS1820(monitorBytes));
    if( (floatTmp - 18.0) > 0.01)
    {
	cerr << "Ambient Temperature error : " << floatTmp << " expected 18.0 C" << endl;
	bTestsSuccessful = false;
    }

    // Let's check some exceptions
    try
    {
	expectedNumberCANTransactions++;
	monitorBytes = FEB.monitor( FEB.INVALID_MONITOR_RCA );
	cerr << "Invalid monitor command did not cause exception as expected." << endl;
    }
    catch(AMB::FEBias::FEBiasError e)
    {
    // Nothing to do... invalid number puposefully passed 
    }

    try
    {
	expectedNumberCANTransactions++;
	FEB.control( FEB.INVALID_CONTROL_RCA, monitorBytes);
	cerr << "Invalid control command did not cause exception as expected." << endl;
    }
    catch(AMB::FEBias::FEBiasError e)
    {
    // Nothing to do... invalid number puposefully passed 
    }

    monitorBytes = FEB.monitor( FEB.GET_TRANS_NUM );
    expectedNumberCANTransactions++;

    int intTmp = static_cast<int>(AMB::dataToLong(monitorBytes));
    bTestsSuccessful = printFunctionConclusion(expectedNumberCANTransactions, intTmp, 
					       "Generic monitor ", bTestsSuccessful);
   return bTestsSuccessful;
}

//------------------------------------------------------------------------------
// Perform tests of quad LNA bias simulator monitors and controls
bool doLNATests(AMB::FEBias &FEB)
{
    bool bTestsSuccessful = true;
    vector<CAN::byte_t> monitorBytes, tmpBytes;
    AMB::FEBias::Monitor_t rcaDrainVoltage;
    AMB::FEBias::Monitor_t rcaDrainCurrent;;
    AMB::FEBias::Monitor_t rcaGateVoltage;
    // To check the # of CAN transactions monitor points, keep track of the
    // # of times we call monitor & control points
    int expectedNumberCANTransactions = 0;
    int startNumberCANTransactions = FEB.get_trans_num();

    try
    {
	// Set state of B6 channel 1 LNAs and B3 channel 2 to enabled
	CAN::byte_t enabledState = 1;
	tmpBytes = AMB::charToData(tmpBytes,enabledState);
	// Set state of B6 channel 1 LNAs and B3 channel 2 LNAs to enabled
	expectedNumberCANTransactions++;
	FEB.control(FEB.SET_B6_LNA1_STATE,tmpBytes);
	expectedNumberCANTransactions++;
	FEB.control(FEB.SET_B3_LNA2_STATE,tmpBytes);
	// Read all LNA voltages and currents  and verify against expected
	for (int i = 1; i < (FEB.NUMBER_OF_LNAS) - 1; i++) 
	{ // This loop verifies B6 channel 1 and B3 channel 2 enabled LNAs voltages and currents
	    // This gets the drain voltage RCA
	    AMB::FEBias::Monitor_t rcaLNAStart = FEB.LNAMonitorIndexToMonitorRCA(
								FEB.LNAStateToLNAMonitor(i));
	    for (int j = 0; j < FEB.STAGES_PER_LNA[i]; j++)
	    {// Loops throught all stages
		// This gets the drain voltage RCA
		rcaDrainVoltage = static_cast<AMB::FEBias::Monitor_t>(rcaLNAStart + 
								 j * FEB.LNA_MONITORS_PER_STAGE);
		// This gets the drain current RCA
		rcaDrainCurrent = static_cast<AMB::FEBias::Monitor_t>(rcaDrainVoltage + 1); 
		// This gets the gate voltage RCA
		rcaGateVoltage = static_cast<AMB::FEBias::Monitor_t>(rcaDrainVoltage + 2); 
		expectedNumberCANTransactions++;
		// First check drain voltage
		monitorBytes = FEB.monitor(rcaDrainVoltage);
		if (bTestsSuccessful) 
		    bTestsSuccessful = monitorMatchesIEEEFloat(monitorBytes,
						FEB.LNAEnabledDrainVoltage,"LNA tests 1");
		if (bTestsSuccessful)
		{
		    // Compare value received through the monitor to the value returned directly 
		    // by the access function.  This essentially test the switch in the monitor 
		    // function.
		    tmpBytes = FEB.getLNADrainVoltage(rcaDrainVoltage);
		    float tmpFloat;
		    bTestsSuccessful = FEB.validFloat(tmpBytes, tmpFloat);
		    if (bTestsSuccessful)
		    {
			bTestsSuccessful = monitorMatchesIEEEFloat(tmpBytes,
						       FEB.LNAEnabledDrainVoltage, "LNA tests 2");
		    }
		}
		// Then check drain current
		expectedNumberCANTransactions++;
		monitorBytes = FEB.monitor(rcaDrainCurrent);
		if (bTestsSuccessful) 
		    bTestsSuccessful = monitorMatchesIEEEFloat(monitorBytes,
						FEB.LNAEnabledDrainCurrent,"LNA tests 3");
		if (bTestsSuccessful)
		{
		    // Compare value received through the monitor to the value returned directly 
		    // by the access function.  This essentially test the switch in the monitor 
		    //function.
		    tmpBytes = FEB.getLNADrainCurrent(rcaDrainCurrent);
		    float tmpFloat;
		    bTestsSuccessful = FEB.validFloat(tmpBytes, tmpFloat);
		    if (bTestsSuccessful)
		    {
			bTestsSuccessful = monitorMatchesIEEEFloat(tmpBytes,
						       FEB.LNAEnabledDrainCurrent, "LNA tests 4");
		    }
		}
		// Finally check gate voltage
		expectedNumberCANTransactions++;
		monitorBytes = FEB.monitor(rcaGateVoltage);
		if (bTestsSuccessful) 
		    bTestsSuccessful = monitorMatchesIEEEFloat(monitorBytes,
						FEB.LNAEnabledGateVoltage,"LNA tests 5");
		if (bTestsSuccessful)
		{
		    // Compare value received through the monitor to the value returned directly 
		    // by the access function.  This essentially test the switch in the monitor 
		    //function.
		    tmpBytes = FEB.getLNAGateVoltage(rcaGateVoltage);
		    float tmpFloat;
		    bTestsSuccessful = FEB.validFloat(tmpBytes, tmpFloat);
		    if (bTestsSuccessful)
		    {
			bTestsSuccessful = monitorMatchesIEEEFloat(tmpBytes,
							FEB.LNAEnabledGateVoltage, "LNA tests 6");
		    }
		}
	    } 
	}
	for (int i = 0; i < FEB.NUMBER_OF_LNAS; i += 3)
	{ // This loop gets B6 channel 2 and B3 channel 1 disabled LNAs voltages and currents 
	    // This gets the drain voltage RCA
	    AMB::FEBias::Monitor_t rcaLNAStart = FEB.LNAMonitorIndexToMonitorRCA(
								FEB.LNAStateToLNAMonitor(i));
	    for (int j = 0; j < FEB.STAGES_PER_LNA[i]; j++)
	    {// Loops throught all stages
		// This gets the drain voltage RCA
		rcaDrainVoltage = static_cast<AMB::FEBias::Monitor_t>(rcaLNAStart + 
								 j * FEB.LNA_MONITORS_PER_STAGE);
		// This gets the drain current RCA
		rcaDrainCurrent = static_cast<AMB::FEBias::Monitor_t>(rcaDrainVoltage + 1); 
		// This gets the gate voltage RCA
		rcaGateVoltage = static_cast<AMB::FEBias::Monitor_t>(rcaDrainVoltage + 2); 
		expectedNumberCANTransactions++;
		// First check drain voltage
		monitorBytes = FEB.monitor(rcaDrainVoltage);
		// Compare value received through the monitor to 0.0 since disabled LNAs 
		// should have all drain voltage and currents as well as gate voltages 
		// set to 0.0.		
		if (bTestsSuccessful) 
		    bTestsSuccessful = monitorMatchesIEEEFloat(monitorBytes, 0.0,"LNA tests 7");
		// Then check drain current
		expectedNumberCANTransactions++;
		monitorBytes = FEB.monitor(rcaDrainCurrent);
		if (bTestsSuccessful) 
		    bTestsSuccessful = monitorMatchesIEEEFloat(monitorBytes, 0.0,"LNA tests 8");
		// Finally check gate voltage
		expectedNumberCANTransactions++;
		monitorBytes = FEB.monitor(rcaGateVoltage);
		if (bTestsSuccessful) 
		    bTestsSuccessful = monitorMatchesIEEEFloat(monitorBytes, 0.0,"LNA tests 9");
	    } 
	}
    }
    catch(AMB::FEBias::FEBiasError e)
    {
	cout << e  << endl;
	bTestsSuccessful = false;
    }

    int LNAControlEnd = FEB.LNA_CONTROL_START + FEB.NUMBER_OF_LNA_STAGES * 
				FEB.LNA_CONTROLS_PER_STAGE;
    float drainVoltage = 0.0;
    float  drainCurrent = 0.0;
    AMB::FEBias::Controls_t rcaControl;
    try
    {
	for (int LNAControl = FEB.LNA_CONTROL_START; LNAControl < LNAControlEnd; LNAControl +=
	     FEB.LNA_CONTROLS_PER_STAGE)
	{ // This loop will set all drain voltages and currents to different values using the 
	  // control function and then read them back using the monitor function and compare 
	  // the read values to what they were set.
	    drainVoltage += 0.18;
	    drainCurrent += 0.062;
	    tmpBytes = FEB.IEEEFloatToData(drainVoltage);
	    expectedNumberCANTransactions++;
	    rcaControl = static_cast<AMB::FEBias::Controls_t>(LNAControl);
	    FEB.control(rcaControl, tmpBytes);
	    rcaDrainVoltage = FEB.LNAMonitorIndexToMonitorRCA(
					FEB.LNAControlRCAToIndex(rcaControl));
	    expectedNumberCANTransactions++;
	    monitorBytes = FEB.monitor(rcaDrainVoltage);
	    if (bTestsSuccessful) 
		bTestsSuccessful = monitorMatchesIEEEFloat(monitorBytes, drainVoltage,
								"LNA tests 10");
	    // Set the LNA drain current/voltage
	    tmpBytes = FEB.IEEEFloatToData(drainCurrent);
	    expectedNumberCANTransactions++;
	    rcaControl = static_cast<AMB::FEBias::Controls_t>(LNAControl + 1);
	    FEB.control(rcaControl, tmpBytes);
	    // Read the LNA current/voltage an verify against what was set
	    rcaDrainCurrent = static_cast<AMB::FEBias::Monitor_t>
		(FEB.LNAMonitorIndexToMonitorRCA(FEB.LNAControlRCAToIndex(rcaControl)) + 1);
	    expectedNumberCANTransactions++;
	    monitorBytes = FEB.monitor(rcaDrainCurrent);
	    if (bTestsSuccessful) 
		bTestsSuccessful = monitorMatchesIEEEFloat(monitorBytes, drainCurrent,
								"LNA tests 11");
	}
    } 
    catch(AMB::FEBias::FEBiasError e)
    {
	cout << e  << endl;
	bTestsSuccessful = false;
    }

    try // cause an exception
    {
	// Set drain voltage for stage 2 of channel 2 of B3 out ot range -- shouldn't let us
	tmpBytes = FEB.IEEEFloatToData(5.0);
	expectedNumberCANTransactions++;
	FEB.control(FEB.SET_B3_LNA2_S2_DRAIN_V,tmpBytes);
	cerr << "LNA out of range condition did not cause exception as expected." << endl;
    }
    catch(AMB::FEBias::FEBiasError e)
    {
	// Nothing to do... we purposely passed an out of range number
    }

    bTestsSuccessful = printFunctionConclusion(expectedNumberCANTransactions, 
					       FEB.get_trans_num() - startNumberCANTransactions, 
					       "LNA ", bTestsSuccessful);
    return bTestsSuccessful;
}

//------------------------------------------------------------------------------
// Perform tests of dual Shottky mixer bias simulator monitors and controls
bool doSchottkyTests(AMB::FEBias &FEB)
{
    bool bTestsSuccessful = true;
    vector<CAN::byte_t> monitorBytes, tmpBytes;
    // To check the # of CAN transactions monitor points, keep track of the
    // # of times we call monitor & control points
    int expectedNumberCANTransactions = 0;
    int startNumberCANTransactions = FEB.get_trans_num();

    // Set Schottky 2 bias to value in range 0 - 15V
    try
    {
	tmpBytes = FEB.IEEEFloatToData(8.0);
	expectedNumberCANTransactions++;
	// Setting the bias voltage affects the junction currents
	FEB.control(FEB.SET_B3_SCHOTTKY2,tmpBytes);
	// Check the junction currents against the 0 - 5 mA and -5 to 0 mA range
	// This gets the positive current RCA
	AMB::FEBias::Monitor_t rcaPos = FEB.schottkyIndexToMonitorRCA(
					FEB.schottkyControlRCAToIndex(FEB.SET_B3_SCHOTTKY2));
	// This gets the negative current RCA
	AMB::FEBias::Monitor_t rcaNeg = static_cast<AMB::FEBias::Monitor_t>(rcaPos + 1); 
	float posCurrent = 2.666667; // This positive current corresponds to 8 V bias in this sim.
	float negCurrent = -2.666667; // This negative current corresponds to 8 V bias in this sim.
	AMB::FEBias::Monitor_t rca;
	float current;
	for (rca = rcaPos, current = posCurrent; rca <= rcaNeg; static_cast<int>(rca)++, 
									current = negCurrent)
	{
	    expectedNumberCANTransactions++;
	    monitorBytes = FEB.monitor(rca);
	    if (bTestsSuccessful) 
		bTestsSuccessful = monitorMatchesIEEEFloat(monitorBytes,current,
								"Schottky tests 1");
	    if (bTestsSuccessful)
	    {
		// Compare value received through the monitor to the value returned directly by 
		// the access function.  This essentially test the switch in the monitor function.
		if (rca == rcaPos)
		    tmpBytes = FEB.getSchottkyPosCurrent(rca);
		else
		    tmpBytes = FEB.getSchottkyNegCurrent(rca);
		float tmpFloat;
		bTestsSuccessful = FEB.validFloat(tmpBytes, tmpFloat);
		if (bTestsSuccessful)
		{
		    bTestsSuccessful = monitorMatchesIEEEFloat(tmpBytes,current,
								"Schottky tests 2");
		}
	    }
	}
    }
    catch(AMB::FEBias::FEBiasError e)
    {
	cout << e  << endl;
	bTestsSuccessful = false;
    }

    try // cause an exception
    {
	// Set bias voltage 1 out ot range -- shouldn't let us
	tmpBytes = FEB.IEEEFloatToData(-3.0);
	expectedNumberCANTransactions++;
	FEB.control(FEB.SET_B3_SCHOTTKY1,tmpBytes);
	cerr << "Schottky out of range condition did not cause exception as expected." << endl;
    }
    catch(AMB::FEBias::FEBiasError e)
    {
	// Nothing to do... we purposely passed an out of range number
    }

    bTestsSuccessful = printFunctionConclusion(expectedNumberCANTransactions, 
					       FEB.get_trans_num() - startNumberCANTransactions, 
					       "Schottky ", bTestsSuccessful);
    return bTestsSuccessful;
}

//------------------------------------------------------------------------------
// Perform tests of dual SIS mixer bias simulator monitors and controls
bool doSISMixerTests(AMB::FEBias &FEB)
{
    bool bTestsSuccessful = true;
    vector<CAN::byte_t> monitorBytes, tmpBytes;
    AMB::FEBias::Monitor_t rca;
    float junction;
    // To check the # of CAN transactions monitor points, keep track of the
    // # of times we call monitor & control points
    int expectedNumberCANTransactions = 0;
    int startNumberCANTransactions = FEB.get_trans_num();

    float biasVoltage = 10.0; // bias voltage set to 10 mV
    float jVoltageScalingFactor = 1.0; // used for scaling junction voltage
    float jCurrentScalingFactor = 20.0; // used for scaling junction current
    float jVoltage = biasVoltage * jVoltageScalingFactor; // This junction voltage corresponds 
							  // to 10 mV bias in this simulator
    float jCurrent = biasVoltage * jCurrentScalingFactor; // This junction current corresponds
							  // to 10 mV bias in this simulator

    // This gets the junction voltage RCA for SIS mixer 1
    AMB::FEBias::Monitor_t rcaVoltage = FEB.SISIndexToMonitorRCA(
	FEB.SISMixerControlRCAToIndex(FEB.SET_B6_SIS1));
    // This gets the junction current RCA for SIS mixer 1
    AMB::FEBias::Monitor_t rcaCurrent = static_cast<AMB::FEBias::Monitor_t>(rcaVoltage + 1);
 
    // Set SIS mixer 1 bias to value in range 0 - 20 mV
    try
    {
	tmpBytes = FEB.IEEEFloatToData(biasVoltage);
	expectedNumberCANTransactions++;
	// Setting the bias voltage affects the junction voltage and current
	FEB.control(FEB.SET_B6_SIS1,tmpBytes);
	// Check the junction current and voltage against the 0 - 20 mV and 0 to 400 mA range
	for (rca = rcaVoltage, junction = jVoltage; rca <= rcaCurrent; static_cast<int>(rca)++, 
									junction = jCurrent)
	{
	    expectedNumberCANTransactions++;
	    monitorBytes = FEB.monitor(rca);
	    if (bTestsSuccessful) 
		bTestsSuccessful = monitorMatchesIEEEFloat(monitorBytes, junction,"SIS tests 1");
	    if (bTestsSuccessful)
	    {
		// Compare value received through the monitor to the value returned directly by the
		// access function.  This essentially test the switch in the monitor function. 
		if (rca == rcaVoltage)
		    tmpBytes = FEB.getSISVoltage(rca);
		else
		    tmpBytes = FEB.getSISCurrent(rca);
		float tmpFloat;
		bTestsSuccessful = FEB.validFloat(tmpBytes, tmpFloat);
		if (bTestsSuccessful)
		{
		    bTestsSuccessful = monitorMatchesIEEEFloat(tmpBytes,junction,"SIS tests 2");
		}
	    }
	}
    }
    catch(AMB::FEBias::FEBiasError e)
    {
	cout << e  << endl;
	bTestsSuccessful = false;
    }

    // Let's play with offsets now -- first the bias voltage zero-bias error offset 
    float v0BiasBiasOffs = -1.0; // Set the zero-bias error offset bias voltage from -1 to 1 mV
    try
    {
	tmpBytes = FEB.IEEEFloatToData(v0BiasBiasOffs);
	expectedNumberCANTransactions++;
	// Setting zero-bias error offset for the bias voltage affects the bias and junction 
	// voltages as well as the junction current.  We can only check the effects on the 
	// junction voltage and current.
	FEB.control(FEB.SET_B6_SIS1_BIAS_CAL_VB,tmpBytes);
	biasVoltage = biasVoltage - v0BiasBiasOffs;
	jVoltage = biasVoltage * jVoltageScalingFactor;
	jCurrent = biasVoltage * jCurrentScalingFactor; 
	for (rca = rcaVoltage, junction = jVoltage; rca <= rcaCurrent; 
					   static_cast<int>(rca)++, junction = jCurrent)
	{
	    expectedNumberCANTransactions++;
	    monitorBytes = FEB.monitor(rca);
	    if (bTestsSuccessful) 
		bTestsSuccessful = monitorMatchesIEEEFloat(monitorBytes, junction,"SIS offset tests 1");
	}
    }
    catch(AMB::FEBias::FEBiasError e)
    {
	cout << e  << endl;
	bTestsSuccessful = false;
    }

    // Then the junction voltage zero-bias error offset 
    float v0BiasOffs = 0.5; // Set the voltage zero-bias error offset from -1 to 1 mV
    try
    {
	tmpBytes = FEB.IEEEFloatToData(v0BiasOffs);
	expectedNumberCANTransactions++;
	// Setting zero-bias error offset for the junction voltage affects only the 
	// junction voltage
	FEB.control(FEB.SET_B6_SIS1_BIAS_CAL_VJ,tmpBytes);
	jVoltage = jVoltage - v0BiasOffs;
	expectedNumberCANTransactions++;
	monitorBytes = FEB.monitor(rcaVoltage);
	if (bTestsSuccessful) 
	    bTestsSuccessful = monitorMatchesIEEEFloat(monitorBytes,jVoltage, 
								"SIS offset tests 2");
    }
    catch(AMB::FEBias::FEBiasError e)
    {
	cout << e  << endl;
	bTestsSuccessful = false;
    }

    // Then the junction current zero-bias error offset 
    float c0BiasOffs = -5.0; // Set the current zero-bias error offset from -10 to 10 uA
    try
    {
	tmpBytes = FEB.IEEEFloatToData(c0BiasOffs);
	expectedNumberCANTransactions++;
	// Setting zero-bias error offset for the junction current affects only the 
	// junction current
	FEB.control(FEB.SET_B6_SIS1_BIAS_CAL_IJ,tmpBytes);
	jCurrent = jCurrent - c0BiasOffs;
	expectedNumberCANTransactions++;
	monitorBytes = FEB.monitor(rcaCurrent);
	if (bTestsSuccessful) 
	    bTestsSuccessful = monitorMatchesIEEEFloat(monitorBytes,jCurrent, 
								"SIS offset tests 3");
    }
    catch(AMB::FEBias::FEBiasError e)
    {
	cout << e  << endl;
	bTestsSuccessful = false;
    }

    try // cause an exception
    {
	// Set bias voltage 2 out ot range -- shouldn't let us
	tmpBytes = FEB.IEEEFloatToData(22.0);
	expectedNumberCANTransactions++;
	FEB.control(FEB.SET_B6_SIS2,tmpBytes);
	cerr << "SIS mixer out of range condition did not cause exception as expected." << endl;
    }
    catch(AMB::FEBias::FEBiasError e)
    {
	// Nothing to do... we purposely passed an out of range number
    }

   bTestsSuccessful = printFunctionConclusion(expectedNumberCANTransactions, 
					       FEB.get_trans_num() - startNumberCANTransactions, 
					       "SIS mixer ", bTestsSuccessful);
    return bTestsSuccessful;
}

//------------------------------------------------------------------------------
// Perform tests of dual magnet current bias simulator monitors and controls
bool doMagnetTests(AMB::FEBias &FEB)
{
    bool bTestsSuccessful;
    vector<CAN::byte_t> monitorBytes, tmpBytes;
    // To check the # of CAN transactions monitor points, keep track of the
    // # of times we call monitor & control points
    int expectedNumberCANTransactions = 0;
    int startNumberCANTransactions = FEB.get_trans_num();

    // Set the magnet current 1 to value in range
    try
    {
	tmpBytes = FEB.IEEEFloatToData(150.0);
	expectedNumberCANTransactions++;
	FEB.control(FEB.SET_B6_MAGNET1,tmpBytes);
	// Compare the value received through the monitor to what we set it to
	expectedNumberCANTransactions++;
	// Get RCA for monitor point
	AMB::FEBias::Monitor_t rca = FEB.SISMagnetIndexToMonitorRCA(
					FEB.SISMagnetControlRCAToIndex(FEB.SET_B6_MAGNET1));
	monitorBytes = FEB.monitor(rca);
	bTestsSuccessful = monitorMatchesIEEEFloat(monitorBytes, 150.0, "Magnet tests 1");
	if (bTestsSuccessful)
	{
	    // Compare value received through the monitor to the value returned directly by the
	    // access function.  This essentially test the switch in the monitor function.
	    vector<CAN::byte_t> getBytes;
	    getBytes = FEB.getMagnetCurrent(rca);
	    float tmpFloat;
	    bTestsSuccessful = FEB.validFloat(getBytes, tmpFloat);
	    if (bTestsSuccessful)
	    {
		bTestsSuccessful = monitorMatchesIEEEFloat(monitorBytes, tmpFloat,
									"Magnet tests 2");
	    }
	}
    }
    catch(AMB::FEBias::FEBiasError e)
    {
	cout << e << endl;
	bTestsSuccessful = false;
    }

    try // cause an exception
    {
	// Set magnet current 2 out ot range -- shouldn't let us
	tmpBytes = FEB.IEEEFloatToData(500.0);
	expectedNumberCANTransactions++;
	FEB.control(FEB.SET_B6_MAGNET2,tmpBytes);
	cerr << "Magnet out of range condition did not cause exception as expected." << endl;
    }
    catch(AMB::FEBias::FEBiasError e)
    {
	// Nothing to do... we purposely passed an out of range number
    }

    bTestsSuccessful = printFunctionConclusion(expectedNumberCANTransactions, 
					       FEB.get_trans_num() - startNumberCANTransactions, 
					       "Magnet ", bTestsSuccessful);
    return bTestsSuccessful;
}


//------------------------------------------------------------------------------
// Utility function that determines whether the passed-in IEEE float matches
// the bytes in passed-in vector.
bool monitorMatchesIEEEFloat( const vector<CAN::byte_t> &monitorBytes,
				 float IEEEFloat,  const char *pMonitorName)
{

    if( monitorBytes.size() != 4 )
    {
	cerr << "Incorrect data point size: " << monitorBytes.size() << " expected 4" 
		 << " for " << pMonitorName << endl;
	return false;
    }

    vector<CAN::byte_t> tmp(monitorBytes);
    float *actualValue;
    long tmpVal =  AMB::dataToLong(tmp);
    actualValue = reinterpret_cast<float *> (&tmpVal);
    if( fabs(*actualValue - IEEEFloat) > 0.01 )
    {
	cerr << "Incorrect value: " << actualValue << " != expected value: " << IEEEFloat
	     << " for " << pMonitorName << endl;
	return false;
    }
    return true;
}

//------------------------------------------------------------------------------
// Utility function that prints the concluding remarks to each test function
bool printFunctionConclusion(int expected, int actual,const char *pMonitorName, bool success)
{
    bool bTestsSuccessful = success;
    if (actual != expected )
    {
	cerr << pMonitorName << " tests: Number of CAN Transactions Error: " << actual 
	     << " expected "  << expected << endl;
	if (bTestsSuccessful)
	    bTestsSuccessful = false;
    }

    if (bTestsSuccessful)
	cout << pMonitorName << "tests were successful. ";
    else
	cerr << pMonitorName << "tests failed. ";
    cout << "Number of transactions : " << dec << expected << endl;
    return bTestsSuccessful;
}
 
//------------------------------------------------------------------------------
int main()
{
    bool bTestsSuccessful = true;

    AMB::node_t FEBiasNode = 0x13;

    vector<CAN::byte_t> FEBiasSn(8);

    //  FEBias S/N looks like 00FEB1A5
    FEBiasSn[0] = 0x00;
    FEBiasSn[1] = 0x00;
    FEBiasSn[2] = 0x0f;
    FEBiasSn[3] = 0x0e;
    FEBiasSn[4] = 0x0b;
    FEBiasSn[5] = 0x01;
    FEBiasSn[6] = 0x0a;
    FEBiasSn[7] = 0x05;

    AMB::FEBias FEB( FEBiasNode, FEBiasSn ); 

    //* Constructor checks
    // Serial # check
    vector<CAN::byte_t> sn = FEB.serialNumber();
    for( int i = 0; i < 8; i++ )
    {
	if( sn[i] != FEBiasSn[i] )
	{
	    bTestsSuccessful = false;
	    cerr << "FEBias: S/N check failed on digit " << i << endl;
	}
    }
    
    // Node # check
    if( FEBiasNode != FEB.node() )
    {
	bTestsSuccessful = false;
	cerr << "FEBias: node check failed: " << FEB.node() << " should be: "
	     << FEBiasNode << endl;
    }
    
    if (!doGenericTests(FEB))
	bTestsSuccessful = false;

    if( !doLNATests(FEB))
	bTestsSuccessful = false;

    if( !doSchottkyTests(FEB))
	bTestsSuccessful = false;

    if( !doSISMixerTests(FEB))
	bTestsSuccessful = false;

    if( !doMagnetTests(FEB))
	bTestsSuccessful = false;
    
    if( bTestsSuccessful )
	cout << "Tests Successful.  " ; 
    else
        cerr << "Tests Failed.  " ;
    cout << "Total number of transactions: " << FEB.get_trans_num() << endl;
}

