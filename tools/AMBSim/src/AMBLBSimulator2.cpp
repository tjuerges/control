// @(#) $Id$
//
// WARNING! THIS IS AN EXAMPLE FILE!
//  ---------------------------------------------------------
// | This is only an example file for a further refactoring  |
// | of the AMBLBSimulator class. It works with the new      |
// | simulation classes and is intended to work with the     |
// | configuration file under ../config.                     |
// | A proper Makefile for this class is Makefile2.          |
//  ---------------------------------------------------------
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu


#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include <rtai_fifos.h>
#include <signal.h>
#include <ace/Signal.h>
#include "AMBSimDispatcher.h"
#include "SharedSimInterface.h"

// Device header file includes
#include <PTCHWSimImpl.h>
#include <FECHWSimImpl.h>
#include <FEMCHWSimImpl.h>
#include <FEDHWSimImpl.h>
#include <HOLORXHWSimImpl.h>
#include <HOLODSPHWSimImpl.h>
#include <OpticalTelescopeHWSimImpl.h>
#include <CRDHWSimImpl.h>
#include <LORRHWSimImpl.h>
#include <WVRHWSimImpl.h>
#include <IFProcHWSimImpl.h>
#include <DGCKHWSimImpl.h>
#include <FLOOGHWSimImpl.h>
#include <OPLLHWSimImpl.h>
#include <CTNLLHWSimImpl.h>
#include <LO2HWSimImpl.h>
#include <DTSTransmitterHWSimImpl.h>
#include <PSAHWSimImpl.h>
#include <PSDHWSimImpl.h>
#include <WCAATFHWSimImpl.h>

// run() timeout, double seconds
#define DISPATCHER_TIMEOUT  -1.0
#define NONE_INST -1

// Pointer to the SharedSimulator component
SharedSimInterface * psim = NULL;

// The dispatcher, which hadles the device instances
AMB::SimDispatcher dispatcher;

// Flag to know if the process is still running
int isRunning = 1;


// Get a device instance of a known type
static void addDevice(const std::string type, const AMB::node_t node,
	const CAN::byte_t serial, const short instance)
{
	AMB::Device* device = NULL;
	std::vector<CAN::byte_t> sn(8, '0');
	sn[7] = serial;
	
	if (type == "PTC") {
		device = new AMB::PTCHWSimImpl(node, sn);
	}
	else if (type == "FEC") {
		device = new AMB::FECHWSimImpl(node, sn);
	}
	else if (type == "FEMC") {
		device = new AMB::FEMCHWSimImpl(node, sn);
	}
	else if (type == "FED") {
		device = new AMB::FEDHWSimImpl(node, sn);
	}
	else if (type == "HOLORX") {
		device = new AMB::HOLORXHWSimImpl(node, sn);
	}
	else if (type == "HOLODSP") {
		device = new AMB::HOLODSPHWSimImpl(node, sn);
	}
	else if (type == "OpticalTelescope") {
		device = new AMB::OpticalTelescopeHWSimImpl(node, sn);
	}
	else if (type == "CRD") {
		device = new AMB::CRDHWSimImpl(node, sn);
	}
	else if (type == "LORR") {
		device = new AMB::LORRHWSimImpl(node, sn);
	}
	else if (type == "WVR") {
		device = new AMB::WVRHWSimImpl(node, sn);
	}
	else if (type == "IFProc") {
		device = new AMB::IFProcHWSimImpl(node, sn, instance);
		device->setSharedSimulator(psim);
	}
	else if (type == "DGCK") {
		device = new AMB::DGCKHWSimImpl(node, sn);
	}
	else if (type == "FLOOG") {
		device = new AMB::FLOOGHWSimImpl(node, sn);
	}
	else if (type == "OPLL") {
		device = new AMB::OPLLHWSimImpl(node, sn);
	}
	else if (type == "CTNLL") {
		device = new AMB::CTNLLHWSimImpl(node, sn);
	}
	else if (type == "LO2") {
		device = new AMB::LO2HWSimImpl(node, sn, instance);
		device->setSharedSimulator(psim);
	}
	else if (type == "DTSTransmitter") {
		device = new AMB::DTSTransmitterHWSimImpl(node, sn);
	}
	else if (type == "PSA") {
		device = new AMB::PSAHWSimImpl(node, sn);
	}
	else if (type == "PSD") {
		device = new AMB::PSDHWSimImpl(node, sn);
	}
	else if (type == "WCAATF") {
		device = new AMB::WCAATFHWSimImpl(node, sn);
	}
	else {
		std::cout << " WARNING: Device type " << type << " not known." << std::endl;
		return;
	}
	
	// Add the new device to the dispatcher
	dispatcher.addDevice(device);
	
	//// Device start message (FIXME: currently in each constructor)
	//std::cout << "Creating node 0x" << std::hex << std::setw(2) << std::setfill('0')
	//	<< static_cast<int>(node) << std::dec << ", s/n 0x";
	//for (int i = 0; i < 8; i++) {
	//	std::cout << std::hex << std::setw(2) << std::setfill('0')
	//		<< static_cast<int>(sn[i]);
    //}
	//std::cout << ", " << type << " device";
	//if (instance != NONE_INST)
	//	std::cout << " - inst " << instance << std::endl;
	//else
	//	std::cout << std::endl;
}

// Initialization of all devices and insertion into the dispatcher.
// These are the default values and this method is only executed if
// no configuration file was found.
static void defaultInitialize()
{
	// Notice only fake serial numbers are used here!
	// This eliminates the conflict if a real device was sharing the bus with
	// the simulator. It does mean the fake serial numbers will appear in
	// the CDB, but they should be easy to change there.
	AMB::node_t node;
	CAN::byte_t serial;

//	// Create Vertex Antenna Control Unit (ACU) device
//	serial = 0x00;
//	node = 0x00;
//	addDevice("ACU", node, serial, NONE_INST);

	// Create Vertex PoinTing Computer (PTC) device
	serial = 0x01;
	node = 0x01;
	addDevice("PTC", node, serial, NONE_INST);

//	// Create AmbDevIO Test Device
//	// (used by the AMBDevice unit tests)
//	serial = 0x02;
//	node = 0x02;
//	addDevice("AmbDevIOTestDevice", node, serial, NONE_INST);

//	// Create AEC Antenna Control Unit (ACU) device
//	// (this simulator shoul dbe merged with the ACU one above)
//	serial = 0x03;
//	node = 0x03;
//	addDevice("ACUAEC", node, serial, NONE_INST);

//	// Create Echo device
//	serial = 0x04;
//	node = 0x04;
//	addDevice("EchoDevice", node, serial, NONE_INST);

	// Create FEC (Compressor) device
	serial = 0x05;
	node = 0x10;
	addDevice("FEC", node, serial, NONE_INST);

//	//  Create Down Converter device
//	serial = 0x06;
//	node = 0x11;
//	addDevice("DownConverter", node, serial, NONE_INST);

//	//  Create Total Power device
//	serial = 0x07;
//	node = 0x12;
//	addDevice("TotalPower", node, serial, NONE_INST);

	// Create FEMC (FEBias) device
	serial = 0x08;
	node = 0x13;
	addDevice("FEMC", node, serial, NONE_INST);

	// Create FED (FEDewar) device
	serial = 0x09;
	node = 0x14;
	addDevice("FED", node, serial, NONE_INST);

//	// Create First LO control device
//	serial = 0x0A;
//	node = 0x1B;
//	addDevice("LO1stCont", node, serial, NONE_INST);

	// Create Holography Receiver (HOLORX) device
	// (part of the Holography)
	serial = 0x0B;
	node = 0x1C;
	addDevice("HOLORX", node, serial, NONE_INST);

	// Create Holography DSP device
	// (part of the Holography)
	serial = 0x0C;
	node = 0x1D;
	addDevice("HOLODSP", node, serial, NONE_INST);

	// Create Optical Telescope device
	serial = 0x0D;
	node = 0x1F;
	addDevice("OpticalTelescope", node, serial, NONE_INST);

	// Create CRD Device
	serial = 0x0E;
	node = 0x20;
	addDevice("CRD", node, serial, NONE_INST);

//	// Create Nutator device
//	serial = 0x0F;
//	node = 0x21;
//	addDevice("Nutator", node, serial, NONE_INST);

	// Create LORR device
	serial = 0x10;
	node = 0x22;
	addDevice("LORR", node, serial, NONE_INST);

	// Create WVR device
	serial = 0x11;
	node = 0x24;
	addDevice("WVR", node, serial, NONE_INST);

	// Create 2 IFProcessor devices
	serial = 0x12;
	node = 0x29;
	addDevice("IFProc", node, serial, 0);

	serial = 0x13;
	node = 0x2A;
	addDevice("IFProc", node, serial, 1);

	// Create Digi Clock (DGCK) Device
	serial = 0x14;
	node = 0x30;
	addDevice("DGCK", node, serial, NONE_INST);

	// Create FTS (FLOOG) device
	serial = 0x15;
	node = 0x32;
	addDevice("FLOOG", node, serial, NONE_INST);

	// Create OPLL device
	// (part of the laser synthesizer)
	serial = 0x16;
	node = 0x38;
	addDevice("OPLL", node, serial, NONE_INST);

	// Create CTNLL device
	// (part of the laser synthesizer)
	serial = 0x17;
	node = 0x39;
	addDevice("CTNLL", node, serial, NONE_INST);

	// Create 4 Second LO (LO2) devices
	serial = 0x18;
	node = 0x40;
	addDevice("LO2", node, serial, 0);

	serial = 0x19;
	node = 0x41;
	addDevice("LO2", node, serial, 1);

	serial = 0x20;
	node = 0x42;
	addDevice("LO2", node, serial, 2);

	serial = 0x2A;
	node = 0x43;
	addDevice("LO2", node, serial, 3);

	// Create 4 DTS Transmitter devices
	// (one for each baseband pair)
	serial = 0x2B;
	node = 0x50;
	addDevice("DTSTransmitter", node, serial, 0);

	serial = 0x2C;
	node = 0x51;
	addDevice("DTSTransmitter", node, serial, 1);

	serial = 0x2D;
	node = 0x52;
	addDevice("DTSTransmitter", node, serial, 2);

	serial = 0x2E;
	node = 0x53;
	addDevice("DTSTransmitter", node, serial, 3);

	// Create DC to DC Power Supply (PSD) device
	serial = 0x2F;
	node = 0x60;
	addDevice("PSA", node, serial, NONE_INST);

	// Create PSD power supply digital device
	serial = 0x30;
	node = 0x61;
	addDevice("PSD", node, serial, NONE_INST);

//	// Create ACA Antenna Control Unit (ACU) device
//	serial = 0x31;
//	node = 0x80;
//	addDevice("ACUACA", node, serial, NONE_INST);

	// Create Warm Cartridge Assembly (WCAATF) device
	serial = 0x32;
	node = 0x1A;
	addDevice("WCAATF", node, serial, NONE_INST);
}

// Initialization of all devices and insertion into the dispatcher.
static void initialize()
{
	bool exist;
	
	/*TODO: See if there is any XML configuration file*/
	exist = false;
	
	// If no configuration file is present, do the default initialization
	if (!exist) {
		defaultInitialize();
		return;
	}
	
	/*TODO: PARSE XML*/
	std::string type;
	AMB::node_t node = 0;
	CAN::byte_t serial = 0;
	short instance = 0;
	
	//for (/*TODO: each elements in file*/) {
		//type = getAttribute("devname");
		//node = getAttribute("nodenumber");
		//serial = getAttribute("serialnumber");
		//instance = getAttribute("instancenumber");
		
		addDevice(type, node, serial, instance);
	//}
}

// Signal handler to catch the [Ctrl]-C signal
static void signalhandler(int signo)
{
	if ((signo == SIGINT) && isRunning) {
		std::cout << "AMBLBSimulator Interrupted." << std::endl;
		isRunning = 0;
		if (psim != NULL)
			delete psim;
		exit(0);
	}
}


int main(int argc, char* argv[])
{
	using AMB::node_t;
	using AMB::rca_t;

	// Register a signal handler to exit cleanly from a
	// [Ctrl]-C keystroke
	ACE_Sig_Action sa(signalhandler);
	ACE_Sig_Set ss;
	ss.sig_add(SIGINT);
	sa.mask(ss);
	sa.register_action(SIGINT);

	// Create an interface object for TELCAL SharedSimulator
	// The interface process the following command line arguments:
	// -sharedsim           Use the SharedSimulator. If this argument is not
	//                      specified, the SharedSimInterface does nothing.
	// -antenna <number>    Antenna number for simulation. If not
	//                      passed, the antenna number is 0.
	psim = new SharedSimInterface(argc, argv);

	// Do the device initilizations and insert them into the dispatcher
	sleep(1);
	try {
		initialize();
	}
	catch (const CAN::Error &x) {
		std::cerr << "SLAVE: " << x.errorMsg << std::endl <<
		"Exiting" << std::endl;
		exit(1);
	}
	catch (...) {
		std::cerr << "Unknown exception" << std::endl;
		exit(1);
	}

	// This gets sent out so we can know when the simulator is running
	std::cout << "AMBLBSimulator Ready" << std::endl;

	// Now run the dispatcher
	while (1) {
		try {
			dispatcher.run(DISPATCHER_TIMEOUT);
		}
		catch (const CAN::Error &x) {
			std::cerr << "Caught a CAN::Error exception. The message is:" <<std::endl
		  	<< x.errorMsg << std::endl
		  	<< "...Carrying on." << std::endl;
		}
		catch (...) {
			std::cerr << "Caught an unknown exception. Exiting." << std::endl;
			exit(1);
		}
		double duration;
		long mcount = 0, ccount = 0;
		dispatcher.statistics(mcount, ccount, duration);
		double mrate = (duration != 0) ? mcount * 1.0 / duration : 0;
		double crate = (duration != 0) ? ccount * 1.0 / duration : 0;
		std::cerr << "Monitor rate: " << mrate << " " <<
			"Control rate: " << crate << std::endl;
		dispatcher.resetStatistics();
	}

	exit(0);
}
