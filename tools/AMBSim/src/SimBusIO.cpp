// @(#) $Id$
//
// Copyright (C) 2001, 2005
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include <cstdlib>
#include <rtai_fifos.h>

#include <sys/time.h>
#include <sys/types.h>
#include <pthread.h>
#include <unistd.h>

#include "SimBusIO.h"
#include "CANError.h"
#include "tpmc901.h"
#include <rtTools.h>
#include <iomanip>

#include <acstimeEpochHelper.h>
#include <acstimeTimeUtil.h> // for TimeUtil
// -----------------------------------------------------------------------------

CAN::BusIO::BusIO()
{

    simIFIFOd = open(TP_WRITE_FIFO_DEV, O_RDWR);
    if (simIFIFOd < 0) {
      std::cerr << "BusIO::BusIO problem with opening "
        << TP_WRITE_FIFO_DEV << std::endl;;
    }

    simOFIFOd = open(TP_READ_FIFO_DEV, O_RDWR);
    if (simOFIFOd < 0) {
      std::cerr << "BusIO::BusIO problem with opening "
        << TP_READ_FIFO_DEV << std::endl;
    }
}

// ----------------------------------------------------------------------------

CAN::BusIO::~BusIO()
{
}

// ----------------------------------------------------------------------------

bool CAN::BusIO::getMessage(id_t& id, std::vector<CAN::byte_t>& data,
                double waitForMaxSeconds) const
{
    TP901_MSG_BUF msg;
    const int msgSize = sizeof(msg);
    EpochHelper now;
    if (waitForMaxSeconds < 0) {
      const int n = rtf_read_all_at_once(simIFIFOd, &msg, msgSize);
      now.value(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
      if (n != msgSize) {
    std::cerr << "Warning FIFO read did not return the expected number of bytes."
          << " Expected: " << msgSize
          << " Got: " << n << std::endl;

    std::cerr << "Ignoring read." << std::endl;
    return false;
      }
    } else {
      const int n = rtf_read_timed(simIFIFOd, &msg, msgSize,
                   (int)(waitForMaxSeconds * 1000));
      now.value(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
      if (n < msgSize) {
    std::cerr << "timeout " << waitForMaxSeconds << std::endl;
    return false;
      }
    }

#ifdef SNIFF
    // Define SNIFF to see all received messages. Should be able to turn this
    // on at run time.
    string timeString = now.toString(acstime::TSArray, "%H:%M:%S.%6q", 0, 0);
    timeString.erase(timeString.size() - 1);
    std::cout 
      << timeString
      << " Recieved a message for address:  "
      << std::hex << msg.identifier;
    if (msg.msg_len != 0) {
      std::cout << " Data:";
      for (unsigned int i = 0; i < msg.msg_len; i++) {
    std::cout << " " << std::hex << std::setw(2) << std::setfill('0')
          << static_cast<int>(msg.data[i]);
      }
    } else {
      std::cout << " Monitor request";
    }
    std::cout << std::endl;
#endif
    id = msg.identifier;
    const char *memoryIn = (const char*) (&msg.data);
    data = std::vector<CAN::byte_t>(memoryIn, memoryIn + msg.msg_len);

    return true;
}


// -----------------------------------------------------------------------------

void CAN::BusIO::postMessage(id_t id, const std::vector<CAN::byte_t>& data)
{
    TP901_MSG_BUF msg;

    msg.identifier = id;
    msg.msg_len = data.size();
    memcpy(&msg.data, &data[0], data.size());

    write(simOFIFOd, &msg, sizeof(msg));
#ifdef SNIFF
    // Define SNIFF this to see all transmitted messages. Should be able to
    // turn this on at run time.
    EpochHelper now(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
    string timeString = now.toString(acstime::TSArray, "%H:%M:%S.%6q", 0, 0);
    timeString.erase(timeString.size() - 1);
    std::cout 
      << timeString << " Sent a CAN message with address: " 
      <<  std::hex << msg.identifier;
    if (data.size() != 0) {
      std::cout << " Data:";
      for (unsigned int i = 0; i < msg.msg_len; i++) {
        std::cout << " " << std::hex << std::setw(2) << std::setfill('0')
                  << static_cast<int>(msg.data[i]);
      }
    } else {
      std::cout << " Zero length message";
    }
    std::cout << std::endl;
#endif
}
