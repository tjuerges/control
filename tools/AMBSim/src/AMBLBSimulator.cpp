// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

/*Some modifications were added to AMBLBSimulator so that all Hardware Devices have
 * different serial numbers across antennas. The method for getting such
 * simulated serial number follows this signature:
 * unsigned long long AMB::Utils::getSimSerialNumber(<componentName>,<AssemblyName>)
 *
 * Example: AMB::Utils::getSimSerialNumber("CONTROL/"+antennaName+"/PSD","PSD")
 *
 * This modifications are part of the STE simulation deployment for Monitoring Archive FBT I.
 */
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include <rtai_fifos.h>
#include "AMBSimDispatcher.h"
#include "SharedSimInterface.h"
#include <signal.h>
#include <ace/Signal.h>
#include <SimulatedSerialNumber.h>
#include <Utils.h>

#include "HolographyDSP.h"
#include "EchoDevice.h"
#include "Compressor.h"
#include "ACU.h"
#include "PTC.h"
#include "OptTel.h"
#include "HolographyReceiver.h"
#include "Nutator.h"
#include "DownConverter.h"
#include "TotalPower.h"


///
/// TODO
/// Thomas, Mar 10, 2009
/// Replace LORR.h with
/// #include "LORRHWSimImpl.h"
#include "LORR.h"

#include "ACUAEC.h"
#include "ACUACA.h"

///
/// TODO
/// Thomas, Mar 10, 2009
/// Replace CRD.h with
/// #include "CRDHWSimImpl.h"
#include "CRD.h"
#include "FTS.h"
#include "SecondLO.h"
#include "ambDevIOTestDevice.h"
#include "IFProc.h"
#include "DTXHWSimImpl.h"
#include "WVRHWSimImpl.h"
#include "CTNLL.h"
#include "OPLL.h"
#include "LLCHWSimImpl.h"
#include "DRXHWSimImpl.h"
#include "DGCKHWSimImpl.h"
#include "PSDHWSimImpl.h"
#include "PSAHWSimImpl.h"
#include "PSLLCHWSimImpl.h"
#include "PSSASHWSimImpl.h"
#include "ACDHWSimImpl.h"

#define DISPATCHER_TIMEOUT  -1.0   // run() timeout, double seconds

// pointer to the SharedSimulator component
SharedSimInterface * psim = NULL;

int isRunning = 1;
// signal handler to catch the [Ctrl]-C signal
static void signalhandler(int signo) {
	if ((signo == SIGINT) && isRunning) {
		std::cout << "AMBLBSimulator Interrupted." << std::endl;
		isRunning = 0;
		if (psim != NULL)
			delete psim;
		exit(0);
	}
}

// Help message for clueless users
void help() {
  std::cout << "Usage: AMBLBSimulator [(-a|--antennaName) <antennaName>] ";
  std::cout << " [(-antenna)  <number> ]";
  std::cout <<        "[(-sharedsim)]\n";
  std::cout  << "  where:\n";
  std::cout << "  -a (--antennaName) is the antenna name\n";
  std::cout << "              defaults to DV01 if not specified.\n";
  std::cout << "  -sharedsim  Use the SharedSimulator. If this argument is not \n";
  std::cout << "              specified, the SharedSimInterface does nothing.\n";
  std::cout << "  -antenna <number> Antenna number for simulation. If not\n";
  std::cout << "              passed, the antenna number is 0.\n";
  exit(1);
}


int main(int argc, char* argv[])
{
	//AMBLB simulator defaults to DV01 antenna if none is specified
	char* antName="DV01";
	std::string antennaName;
	for(int i = 1; i < argc; ++i) {
	   if (strcmp(argv[i], "-a") == 0 ||
	        strcmp(argv[i], "--antennaName") == 0) {
	      if (i+1 == argc) {
	        cout << "Invalid " << argv[i];
	        cout << " parameter: no antenna name specified\n";
	        help();
	        exit(1);
	      }
	      antName=argv[i+1];
	      cout << " Using  antenna name " << antName <<" specified\n";
	      //argc=argc-2;
	      char* temp[argc-2];
	      for (int j=0; j<i;++j	)
	      {
	    	  temp[j]=argv[j];
	      }
	      for (int j=i+2; j<argc;++j	)
	      {
	      	  temp[j-2]=argv[j];
	      }
	      argc=argc-2;
	      for (int j=0; j<argc;++j	)
	      {
	      	      	  argv[j]=temp[j];
	      }
	      argv[argc]='\0';

	    }
	   antennaName=antName;

	  }


	using AMB::node_t;
	using AMB::rca_t;

	// Register a signal handler to exit cleanly from a
	// [Ctrl]-C keystroke.
	ACE_Sig_Action sa(signalhandler);
	ACE_Sig_Set ss;
	ss.sig_add(SIGINT);
	sa.mask(ss);
	sa.register_action(SIGINT);

	// Create an interface object for TELCAL SharedSimulator
	// The interface process the following command line arguments:
	// -sharedsim           Use the SharedSimulator. If this argument is not
	//                      specified, the SharedSimInterface does nothing.
	// -antenna <number>    Antenna number for simulation. If not
	//                      passed, the antenna number is 0.
	psim = new SharedSimInterface(argc, argv);

	AMB::SimDispatcher dispatcher;
	sleep(1);

	// Notice only fake serial numbers are used here!
	// This elminates the conflict if a real device was sharing the bus with
	// the simulator.  It does mean the fake serial numbers will appear in
	// the CDB but they should be easy to change there.
	try {
		std::vector< CAN::byte_t > sn(8, '0');


		// Create Vertex Antenna Control Unit (ACU) device
		sn[7] = 0x00;
		AMB::Device* acu = new AMB::ACU(0x00, sn);
		acu->setSharedSimulator(psim);
		dispatcher.addDevice(acu);

		// Create Vertex PoinTing Computer (PTC) device
		sn[7] = 0x01;
		AMB::Device* ptc = new AMB::PTC(0x01, sn);
		dispatcher.addDevice(ptc);

		// Create AmbDevIO Test Device
		// (used by the AMBDevice unit tests)
		sn[7] = 0x02;
		AMB::Device* devIOTest = new AMB::AmbDevIOTestDevice(0x02, sn);
		dispatcher.addDevice(devIOTest);

		// Create AEC Antenna Control Unit (ACU) device
		// (this simulator shoul dbe merged with the ACU one above)
		sn[7] = 0x03;
		AMB::Device* acuaec = new AMB::ACUAEC(0x03, sn);
		dispatcher.addDevice(acuaec);

		// Create Echo device
		sn[7] = 0x04;
		AMB::Device* ed = new AMB::EchoDevice(0x04, sn);
		dispatcher.addDevice(ed);

		// Create Compressor device
		sn[7] = 0x05;
		AMB::Device* compressor = new AMB::Compressor(0x10, sn);
		dispatcher.addDevice(compressor);

		//  Create Down Converter device
		sn[7] = 0x06;
		AMB::Device* DC = new AMB::DownConverter(0x11, sn);
		dispatcher.addDevice(DC);

		//  Create Total Power device
		sn[7] = 0x07;
		AMB::Device* TP = new AMB::TotalPower(0x12, sn);
		dispatcher.addDevice(TP);

		// Create Holography Receiver device
		sn[7] = 0x0B;
		AMB::Device* holoRcvr = new AMB::HolographyReceiver(0x1C, sn);
		dispatcher.addDevice(holoRcvr);

		// Create Holography DSP device
		sn[7] = 0x0C;
		AMB::Device* holoDSP = new AMB::HolographyDSP(0x1D, sn);
		dispatcher.addDevice(holoDSP);

		// Create Optical Telescope device
		sn[7] = 0x0D;
		AMB::Device* ot = new AMB::OptTel(0x1F, sn);
		dispatcher.addDevice(ot);

		// Create CRD Device
		sn[7] = 0x0E;
		AMB::Device* crd(new AMB::CRD(0x20, sn));
		//AMB::Device* crd(new AMB::CRDHWSimImpl(0x20, sn));
		dispatcher.addDevice(crd);

		// Create Nutator device
		sn[7] = 0x0F;
		AMB::Device* nut = new AMB::Nutator(0x21, sn);
		dispatcher.addDevice(nut);

		// Create LORR device
		//sn[7] = 0x10;
		AMB::TypeConversion::valueToData(sn,AMB::Utils::getSimSerialNumber("CONTROL/"+antennaName+"/LORR","LORR"), 8U);
		AMB::Device* lorr(new AMB::LORR(0x22, sn));
		//AMB::Device* lorr(new AMB::LORRHWSimImpl(0x22, sn));
		dispatcher.addDevice(lorr);

		// Create WVR device
		sn[7] = 0x11;
		//AMB::Device* wvr = new AMB::WVR(0x24, sn);
		AMB::Device* wvr = new AMB::WVRHWSimImpl(0x24, sn);
		dispatcher.addDevice(wvr);

		// Create 2 IFProcessor devices

		AMB::TypeConversion::valueToData(sn,AMB::Utils::getSimSerialNumber("CONTROL/"+antennaName+"/IFProc0","IFProc"), 8U);
		AMB::Device*  IFPROC0x29= new AMB::IFProc(0x29,sn,0);
		IFPROC0x29->setSharedSimulator(psim);
		dispatcher.addDevice(IFPROC0x29);


		AMB::TypeConversion::valueToData(sn,AMB::Utils::getSimSerialNumber("CONTROL/"+antennaName+"/IFProc1","IFProc"), 8U);
		AMB::Device*  IFPROC0x2a= new AMB::IFProc(0x2a,sn,1);
		IFPROC0x2a->setSharedSimulator(psim);
		dispatcher.addDevice(IFPROC0x2a);

		// Create DGCK
		//sn[7] = 0x14;
		AMB::TypeConversion::valueToData(sn,AMB::Utils::getSimSerialNumber("CONTROL/"+antennaName+"/DGCK","DGCK"), 8U);
		AMB::Device* dgck = new AMB::DGCKHWSimImpl(0x30,sn);
		dispatcher.addDevice(dgck);

		// Create FTS (FLOOG) device
		AMB::TypeConversion::valueToData(sn,AMB::Utils::getSimSerialNumber("CONTROL/"+antennaName+"/FLOOG","FLOOG"), 8U);
		AMB::Device* fts = new AMB::FTS(0x32, sn);
		dispatcher.addDevice(fts);

		// Create OPLL device
		// (part of the laser synthesizer)
		sn[7] = 0x16;
		AMB::Device* opll = new AMB::OPLL(0x38, sn);
		dispatcher.addDevice(opll);

		// Create CTNLL device
		// (part of the laser synthesizer)
		sn[7] = 0x17;
		AMB::Device* ctnll = new AMB::CTNLL(0x39, sn);
		dispatcher.addDevice(ctnll);

		// Create 4 Second LO (LO2) devices
		//sn[7] = 0x18;
		AMB::TypeConversion::valueToData(sn,AMB::Utils::getSimSerialNumber("CONTROL/"+antennaName+"/LO2BBpr0","LO2"), 8U);
		AMB::Device* secondLOBB0 = new AMB::SecondLO(0x40, sn, 0);
		secondLOBB0->setSharedSimulator(psim);
		dispatcher.addDevice(secondLOBB0);

		//sn[7] = 0x19;
		AMB::TypeConversion::valueToData(sn,AMB::Utils::getSimSerialNumber("CONTROL/"+antennaName+"/LO2BBpr1","LO2"), 8U);
		AMB::Device* secondLOBB1 = new AMB::SecondLO(0x41, sn, 1);
		secondLOBB1->setSharedSimulator(psim);
		dispatcher.addDevice(secondLOBB1);

		//sn[7] = 0x20;
		AMB::TypeConversion::valueToData(sn,AMB::Utils::getSimSerialNumber("CONTROL/"+antennaName+"/LO2BBpr2","LO2"), 8U);
		AMB::Device* secondLOBB2 = new AMB::SecondLO(0x42, sn, 2);
		secondLOBB2->setSharedSimulator(psim);
		dispatcher.addDevice(secondLOBB2);

		//sn[7] = 0x2A;
		AMB::TypeConversion::valueToData(sn,AMB::Utils::getSimSerialNumber("CONTROL/"+antennaName+"/LO2BBpr3","LO2"), 8U);
		AMB::Device* secondLOBB3 = new AMB::SecondLO(0x43, sn, 3);
		secondLOBB3->setSharedSimulator(psim);
		dispatcher.addDevice(secondLOBB3);

		// Create 4 DTS Transmitter devices
		// (one for each baseband pair)
		//sn[7] = 0x2B;
		AMB::TypeConversion::valueToData(sn,AMB::Utils::getSimSerialNumber("CONTROL/"+antennaName+"/DTXBBpr0","DTX"), 8U);
		AMB::Device* dtxBBpr0 = new AMB::DTXHWSimImpl(0x50, sn);
		dispatcher.addDevice(dtxBBpr0);

		//sn[7] = 0x2C;
		AMB::TypeConversion::valueToData(sn,AMB::Utils::getSimSerialNumber("CONTROL/"+antennaName+"/DTXBBpr1","DTX"), 8U);
		AMB::Device* dtxBBpr1 = new AMB::DTXHWSimImpl(0x51, sn);
		dispatcher.addDevice(dtxBBpr1);

		//sn[7] = 0x2D;
		AMB::TypeConversion::valueToData(sn,AMB::Utils::getSimSerialNumber("CONTROL/"+antennaName+"/DTXBBpr2","DTX"), 8U);
		AMB::Device* dtxBBpr2 = new AMB::DTXHWSimImpl(0x52, sn);
		dispatcher.addDevice(dtxBBpr2);

		//sn[7] = 0x2E;
		AMB::TypeConversion::valueToData(sn,AMB::Utils::getSimSerialNumber("CONTROL/"+antennaName+"/DTXBBpr3","DTX"), 8U);
		AMB::Device* dtxBBpr3 = new AMB::DTXHWSimImpl(0x53, sn);
		dispatcher.addDevice(dtxBBpr3);


		// Create PSA Power Supply Analog BackEnd

		AMB::TypeConversion::valueToData(sn,AMB::Utils::getSimSerialNumber("CONTROL/"+antennaName+"/PSA","PSA"), 8U);
		AMB::Device* psa = new AMB::PSAHWSimImpl(0x60,sn);
		dispatcher.addDevice(psa);

		// Create PSD power supply digital BackEnd using simulation stuff

		AMB::TypeConversion::valueToData(sn,AMB::Utils::getSimSerialNumber("CONTROL/"+antennaName+"/PSD","PSD"), 8U);
		AMB::Device* psd = new AMB::PSDHWSimImpl(0x61,sn);
		dispatcher.addDevice(psd);

		// Create ACA Antenna Control Unit (ACU) device

		AMB::TypeConversion::valueToData(sn,AMB::Utils::getSimSerialNumber("CONTROL/"+antennaName,"ACA"), 8U);
		AMB::Device* acuaca = new AMB::ACUACA(0x80, sn);
		dispatcher.addDevice(acuaca);

        // Create Line Length Corrector (LLC) device
        sn[7] = 0x33;
        AMB::Device* llc = new AMB::LLCHWSimImpl(0x18, sn);
        dispatcher.addDevice(llc);

        // Create Line Length Corrector (LLC) device
        sn[7] = 0x34;
        AMB::Device* llc2 = new AMB::LLCHWSimImpl(0x19, sn);
        dispatcher.addDevice(llc2);

		// Create DTS Receiver (DRX) device
        //sn[7] = 0x35;
        AMB::TypeConversion::valueToData(sn,AMB::Utils::getSimSerialNumber("CONTROL/"+antennaName+"/DRXBBpr0","DRX"), 8U);
        AMB::Device* drxBBpr0 = new AMB::DRXHWSimImpl(0x100, sn);
        dispatcher.addDevice(drxBBpr0);

        //sn[7] = 0x36;
        AMB::TypeConversion::valueToData(sn,AMB::Utils::getSimSerialNumber("CONTROL/"+antennaName+"/DRXBBpr1","DRX"), 8U);
        AMB::Device* drxBBpr1 = new AMB::DRXHWSimImpl(0x140, sn);
        dispatcher.addDevice(drxBBpr1);

        //sn[7] = 0x37;
        AMB::TypeConversion::valueToData(sn,AMB::Utils::getSimSerialNumber("CONTROL/"+antennaName+"/DRXBBpr2","DRX"), 8U);
        AMB::Device* drxBBpr2 = new AMB::DRXHWSimImpl(0x180, sn);
        dispatcher.addDevice(drxBBpr2);

        //sn[7] = 0x38;
        AMB::TypeConversion::valueToData(sn,AMB::Utils::getSimSerialNumber("CONTROL/"+antennaName+"/DRXBBpr3","DRX"), 8U);
        AMB::Device* drxBBpr3 = new AMB::DRXHWSimImpl(0x1C0, sn);
        dispatcher.addDevice(drxBBpr3);

        // Create PSLLC power supply for LLC Rack
        //sn[7]=0x39;
        AMB::TypeConversion::valueToData(sn,AMB::Utils::getSimSerialNumber("CONTROL/"+antennaName+"/PSLLC","PSLLC"), 8U);
        AMB::Device* psllc = new AMB::PSLLCHWSimImpl(0x58,sn);
        dispatcher.addDevice(psllc);

		 // Create PSSAS power supply for SAS Rack
        //sn[7]=0x3A;
        AMB::TypeConversion::valueToData(sn,AMB::Utils::getSimSerialNumber("CONTROL/"+antennaName+"/PSSAS","PSSAS"), 8U);
        AMB::Device* pssas = new AMB::PSSASHWSimImpl(0x5E,sn);
        dispatcher.addDevice(pssas);

		// Create ALMA Calibration Device (ACD) device
		//sn[7] = 0x3B;
        AMB::TypeConversion::valueToData(sn,AMB::Utils::getSimSerialNumber("CONTROL/"+antennaName+"/ACD","ACD"), 8U);
		AMB::Device* acd = new AMB::ACDHWSimImpl(0x90, sn);
		dispatcher.addDevice(acd);
	}
	catch (const CAN::Error &x) {
		std::cerr << "SLAVE: " << x.errorMsg << std::endl <<
		"Exiting" << std::endl;
	  	exit(1);
	}
	catch (...) {
		std::cerr << "Unknown exception" << std::endl;
		exit(1);
	}

	// This gets sent out so we can know when the simulator is running
	std::cout << "AMBLBSimulator Ready" << std::endl;

	// now run the dispatcher
	while (1) {
		try {
			dispatcher.run(DISPATCHER_TIMEOUT);
		}
		catch (const CAN::Error &x) {
			std::cerr << "Caught a CAN::Error exception. The message is:" <<std::endl
		  	<< x.errorMsg << std::endl
		  	<< "...Carrying on." << std::endl;
		}
		catch (...) {
			std::cerr << "Caught an unknown exception. Exiting." << std::endl;
			exit(1);
		}
		double duration;
		long mcount = 0, ccount = 0;
		dispatcher.statistics(mcount, ccount, duration);
		double mrate = (duration != 0) ? mcount * 1.0 / duration : 0;
		double crate = (duration != 0) ? ccount * 1.0 / duration : 0;
		std::cerr << "Monitor rate: " << mrate << " " <<
			"Control rate: " << crate << std::endl;
		dispatcher.resetStatistics();
	}

	exit(0);
}
