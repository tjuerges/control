// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include <iostream>
#include <iomanip>

#include "AMBSimDispatcher.h"
#include "CANError.h"
#include "AMBUtil.h"


// -----------------------------------------------------------------------------

AMB::SimDispatcher::SimDispatcher()
    : io_m()
{
    resetStatistics();
}

// -----------------------------------------------------------------------------

AMB::SimDispatcher::~SimDispatcher()
{
    // Delete all devices, must have come from new
    std::map<node_t, AMB::Device*>::iterator i = devices_m.begin();
    while (i != devices_m.end()) {
	delete i->second;
	++i;
    }
}

// -----------------------------------------------------------------------------

void AMB::SimDispatcher::addDevice(AMB::Device *fromNew)
{
    if (fromNew == 0) {
	throw CAN::Error("Cannot have a device which is null");
    }
    node_t node = fromNew->node();
    std::vector<CAN::byte_t> sn = fromNew->serialNumber();

    std::map<node_t, AMB::Device*>::iterator iter = devices_m.begin();
    while(iter != devices_m.end()) {
	if (iter->second->node() == node) {
	    throw CAN::Error("Adding a node which has already been defined");
	}
	if (iter->second->serialNumber() == sn) {
	    throw CAN::Error("Adding an already existing serial number");
	}
	++iter;
    }
    // OK, add it to the map.
    devices_m[node] = fromNew;
}

// -----------------------------------------------------------------------------

void AMB::SimDispatcher::removeDevice(node_t node)
{
    std::map<node_t, AMB::Device*>::iterator found = devices_m.find(node);
    if (found == devices_m.end()) {
	throw CAN::Error("Do not have device at the specified node");
    }

    // Call the device destructor in case some cleanup is required (and to
    // prevent memory leak)
    delete found->second;
    devices_m.erase(found);
}

// -----------------------------------------------------------------------------

void AMB::SimDispatcher::statistics(long &monitors, long &controls,
				    double &duration) const
{
    duration = duration_m;
    monitors = monitor_count_m;
    controls = control_count_m;
}

// -----------------------------------------------------------------------------

void AMB::SimDispatcher::resetStatistics()
{
    monitor_count_m = control_count_m = 0;
    duration_m = 0.0;
}

// -----------------------------------------------------------------------------

void AMB::SimDispatcher::run(double howLongInSeconds)
{
    id_t id = 0;
    std::vector<CAN::byte_t> data;
    std::map<node_t, AMB::Device*>::iterator found;
    timeval startTime;
    gettimeofday(&startTime, 0);


    while (1) // Forever or until we time out
	{
	if (howLongInSeconds > 0)
	    {
	    double currentInterval = AMB::difftime(startTime);
	    bool haveMessage = false;
	    if (currentInterval < howLongInSeconds)
		{
		haveMessage = io_m.getMessage(
		    id, data, (howLongInSeconds - currentInterval));
	    }
	    if ( ! haveMessage)
		{
		// OK, we've timed out and no message
		duration_m += AMB::difftime(startTime);
		return;
		}
	    }
	else
	    {
	    io_m.getMessage(id, data);
	    }
	if (id > BROADCAST_TOP) {

	    // Monitor or control
	    node_t node;
	    rca_t rca;
	    AMB::fromID(node, rca, id);
	    found = devices_m.find(node);

	    if (found == devices_m.end())
		{
	        // We do not have the node ourselves, so probably this is
	        // intended for some other device so we should ignore it.
		std::cout << "found == devices_m.end()\n";
		std::cerr << "No device for that node 0x"
            << std::hex << node
            << std::dec << "\n";
	        continue;
	    }

	    if (data.empty() == true)
		{
		// Monitor
		data = found->second->monitor(rca);
		io_m.postMessage(id, data);
		monitor_count_m++;
		} else {
		// Control
		found->second->control(rca, data);
		control_count_m++;
	    }

	} else {
	    // BROADCAST
	    switch(id) {
	    case BUS_INIT:
		{
		    // Iterate through our devices sending our serial
		    // numbers.
		    std::map<node_t, AMB::Device*>::iterator i = devices_m.begin();
		    while (i != devices_m.end()) {
			node_t node = i->second->node();
			id_t id = AMB::toID(node, 0);
			std::vector<CAN::byte_t> sn = i->second->serialNumber();
			io_m.postMessage(id, sn);
			++i;
		    }
		}
		break;
	    default:
		throw CAN::Error("Do not understand broadcast request");
	    }
	}
    }
}

