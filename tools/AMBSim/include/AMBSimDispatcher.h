#if !defined(AMB_SIM_DISPATCHER_H)
#define AMB_SIM_DISPATCHER_H

// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include <map>
//#include <string>
#include "SimBusIO.h"
#include "AMBDevice.h"

// For the time related stuff
#include <sys/time.h>
#include <unistd.h>

namespace AMB {

    /** Turn raw CAN IO messages into AMB messages and dispatch to
	simulated device.
	The role of this class is to attach to get traffic from a CAN
	device, interpret them as AMB messages (defined in ALMA
	Computing Memo #7), and then dispatch the messages to the
	correct Device simulator. An arbitrary number of simulated
	devices may be held.
	After construction, which attaches to the CAN interface board, 
	some number of simulated devices are added, and then the loop
	is started which dispatches messages to the simulated devices. */

  class SimDispatcher
  {
  public:

	/** Attach the dispatcher to the CAN interface device.
	    @param device  name of the device, e.g. /dev/dpm_00
	    @param terminate set bus termination on the board. A feature of the 
	        JANZ CAN hardware. For a point to point connection 
	        it doesn't hurt to set it even if the cable has
	        termination.
	    @exception CAN::Error if the device open fails. */
	SimDispatcher();

	/** Relinquish the underlying CAN interface and 
	    deletes/destructs the simulated devices which have been
	    added to the dispatcher. */
	~SimDispatcher();

	/** Add a simulated device to the dispatcher.
	    @param fromNew a pointer to an instance of a class derived from
	        Device. The pointer must have been obtained from new because
	        upon destruction the dispatcher will delete it.
	    @exception CAN::Error if the node or serial number of the device is
	        already in use, or if the pointer is null.If an exception is
	        thrown then the caller is responsible for deleting the
	        pointer. */
	void addDevice(AMB::Device *fromNew);
    
	/** Remove a device with the specified node number from the dispatcher.
	    @param node node number to be removed
	    @exception CAN::Error if there is no device with the specified node
	        number. 
	 */
	void removeDevice(node_t node);

	/** Listen to the CAN bus dispatching messages to the simulated devices.
	    @param howLongInSeconds How long (s) to run before exiting the
	        loop. Less than 0 implies forever. You might want to exit
	        the loop periodically to print statistics, add or remove
	        devices, or some other purpose.
	    @exception CAN::Error if a monitor or control request is received
	        for a non-existent node number, or if an exception
	        is thrown by the underlying device. */
	void run(double howLongInSeconds = -1.0);

	/** Obtain statistics on monitor and control rates.Statistics are only
	    kept, and the duration only increases, while in the run() method.
	    @param monitors The number of monitor requests received.
	    @param controls The number of control requests received.
	    @param duration The length of time (s) in which the monitor/control 
	        requests were received.
	 */
	void statistics(long &monitors, long &controls, double &duration) const;

	/** Reset the statistics count. For example, if you are breaking out of 
	    the run() method every minute (say) you might want to reset the
	    statistics rather than reporting the overall (cumulative) values. */
	void resetStatistics();

  private:
	/// Copying not sensible - Undefined and inaccessible.
	SimDispatcher(const SimDispatcher &);
	/// Copying not sensible - Undefined and inaccessible.
	SimDispatcher &operator=(const SimDispatcher &);

	/** A map containing the simulated devices. It's a map for efficiency
	    during lookup. A hash_map would be even more efficient but is not
	    standard. */
	std::map<node_t, AMB::Device*> devices_m;

	/// The object that performs the actual raw CAN IO.
	CAN::BusIO io_m;

	/** Duration (s) for which the monitor and control counts have been
	    obtained. */
	double duration_m;
	/// Count for return in statistics().
	long monitor_count_m;
	/// Count for return in statistics().
	long control_count_m;
    };

} // namespace AMB

#endif
