#if !defined(CAN_INTERFACE_H)
#define CAN_INTERFACE_H

// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include "CANTypes.h"

#include <string>
#include <queue>

namespace CAN
{
    class BusIO
    {
    public:
    BusIO();
    ~BusIO();

    bool getMessage(id_t &id, std::vector<CAN::byte_t> &data,
        double maxWaitInSeconds = -1.0) const;

    void postMessage(id_t id, const std::vector<CAN::byte_t> &data);

    private:
    // Undefined and inaccessible
    BusIO(const BusIO &);
    BusIO &operator=(const BusIO &);

    int simIFIFOd;
    int simOFIFOd;
    };

} // namespace CAN

#endif /* CAN_INTERFACE_H */
