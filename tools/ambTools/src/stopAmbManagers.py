#! /usr/bin/env python
# $Id$


import CCL.Antenna
import CCL.AOSTiming


try:
    aos = CCL.AOSTiming.AOSTiming()
    dv01 = CCL.Antenna.Antenna("DV01")
    da41 = CCL.Antenna.Antenna("DA41")
except Exception, ex:
    print "Caught an exception during instanciation of the controllers.  Exiting."
    exit


print

source = "ARTM"
try:
    if aos.isAmbManagerRunning(source) == True:
        aos.stopAmbManager(source)
        print "%s: AmbManager stopped." % source
    else:
        print "%s: AmbManager is already stopped." % source
except Exception, ex:
    print "%s: An exception was caught." % source

source = "DMC"
try:
    if aos.isAmbManagerRunning(source) == True:
        aos.stopAmbManager(source)
        print "%s: AmbManager stopped." % source
    else:
        print "%s: AmbManager is already stopped." % source
except Exception, ex:
    print "%s: An exception was caught." % source

source = "DA41"
try:
    if da41.isAmbManagerRunning() == True:
        da41.stopAmbManager()
        print "%s: AmbManager stopped." % source
    else:
        print "%s: AmbManager is already stopped." % source
except Exception, ex:
    print "%s: An exception was caught." % source

source = "DV01"
try:
    if dv01.isAmbManagerRunning() == True:
        dv01.stopAmbManager()
        print "%s: AmbManager stopped." % source
    else:
        print "%s: AmbManager is already stopped." % source
except Exception, ex:
    print "%s: An exception was caught." % source
