#!/usr/bin/env python
# $Id$
#
# Copyright (C) 2004
# Associated Universities, Inc. Washington DC, USA.
#
# Produced for the ALMA project
#
# This library is free software; you can redistribute it and/or modify
# it under the terms of the GNU Library General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
# Correspondence concerning ALMA should be addressed as follows:
# Internet email: alma-sw-admin@nrao.edu
#
"""
Definition of a pyunit (or unittest) test case class for low-level
hardware testing using only the AmbManager for AMB transactions, and a
script to run the tests from the command line. The use of pyunit
imposes some awkward restrictions when creating tests that can take
parameters (from the command line, for instance), and this module
attempts to address those in order to make it fairly easy to create
new TestCase classes.

This module may be imported into other modules defining new
AMBMCTest.TestCase subclasses. As an example, assume we have an AMBSI
device that defines only a single additional monitor point (rca=0x100)
returning two bytes, the top four bits of which are always zero. The
following module (named 'MyDev') would be sufficient to create the
subclass and a script to execute some tests.

    import AMBMCTest
    class TestCase(AMBMCTest.TestCase):
        def testMyMonitor(self):
            result = self.monitorSizeTest(0x100, 2)
            self.failUnlessEqual(ord(result.value[0]) & 0xF0, 0,
                                 'Upper four bits not all zero')
            return
    if __name__ == '__main__':
        AMBMCTest.main(TestCase, __file__)

An example of how to execute the test script on devices connected to
the ABM named 'ABM001' on channel 2 with node ids 64, 65, and 66:

./AMBMCTest.py ABM001 2 64 65 66
"""
__version__ = '$Id$'

import unittest
import CCL.AmbManager
import Control
import ControlExceptions
import new
import __builtin__

class TestCase(unittest.TestCase):
    """
    Base class for implementing 'unit tests' for hardware. All testing
    is done using the AmbManager, and so is at a rudimentary level
    (unless you care to write functions for converting byte arrays
    to/from typed values). This class and its descendants are meant to
    be used in the pyunit framework.

    This class implements three tests based on the standard AMBSI
    interface:
    * testPresence(), for testing the presence of a given node on
      the AMB;
    * testSN(), for testing response to serial number monitor point;
    * testTemperature(), for testing response to temperature monitor
      point.
    Other methods in the class are intended to support the
    implementation and use of descendant classes.

    Because of restrictions on the constructor of unittest.TestCase
    subclasses in the unittest framework, you should not directly
    instantiate members of this class, nor its subclasses defined in
    other modules. Always use the makeTestCaseClass() class method to
    create a subclass of the class you need, and create instances from
    those subclasses. The instances of the dynamically created classes
    returned by makeTestCaseClass() may be used in the unittest
    framework.
    """

    def makeTestCaseClass(cls, machine, channel, node_id, attributes = None):
        """Create a (TestCase) subclass of the class for given
        machine name, AMB channel, and AMB node id. Instances of the
        returned class may be used in the pyunit framework.

        Parameters:
          machine - ABM machine name (string)
          channel - AMB channel
          node_id - AMB node id
          attributes - dictionary of additional class attributes

        Return:
          subclass

        Example:
          Assume a AMBMCTest.TestCase subclass named MyTest.TestCase:
          # the class
          MyTestCase = MyTest.TestCase.makeTestCaseClass('DV01', 0, 10)
          # the instance (this is not always required, see main())
          tc = MyTestCase()
        """
        klassname = 'TestCase_%s_%s_%d_%d' % \
                    (cls.__module__, machine, channel, node_id)
        def init(self, methodName = 'runTest'):
            """Initialize a hardware test class using AmbManager
            for hardware access. The machine name, AMB channel
            number and node id for this test class were determined
            at the time of class creation."""
            cls.__init__(self, methodName)
            return
        if attributes is not None:
            dict = attributes
        else:
            dict = {}
        dict['__init__'] = init
        dict['_ambman'] = CCL.AmbManager.AmbManager(machine)
        dict['_channel'] = channel
        dict['_node_id'] = node_id
        klass = new.classobj(klassname, (cls,), dict)
        return klass
    makeTestCaseClass = classmethod(makeTestCaseClass)

    def cmp(t1, t2):
        """Normal string comparison, with the exception that
        'testPresence' will always be first."""
        if t1 == 'testPresence':
            return -(t2 != 'testPresence')
        return __builtin__.cmp(t1, t2)
    cmp = staticmethod(cmp)

    def __init__(self, methodName = 'runTest'):
        """Instances of this class are largely useless since important
        instance variables are not initialized here. Use
        'makeTestCaseClass' to create a subclass whose instances are
        useful."""
        unittest.TestCase.__init__(self, methodName)
        return

    def monitor(self, rca):
        """Monitor transaction with given relative CAN address.

        Parameters:
          rca - relative CAN address

        Return:
          result of call to AmbManager.monitor, i.e., (value, timestamp)
        """
        return self._ambman.monitor(self._channel, self._node_id, rca)

    def command(self, rca, data):
        """Control transaction with given relative CAN address and
        command value.

        Parameters:
          rca - relative CAN address
          data - command data (AmbData type)

        Return:
          result of call to AmbManager.control, i.e., timestamp
        """
        return self._ambman.command(self._channel, self._node_id, rca, data)

    def runTest(self):
        """Default test method. Executes all test methods, which are
        determined by searching for instance method names prefixed by
        'test'. Note that this is not the test run by the standard
        function main(). This method is included to facilitate use of
        this class in other unittest settings."""
        testFnNames = [fn for fn in dir(self) if
                       fn[:len('test')] == 'test' and
                       'im_func' in dir(self)[fn]]
        testFnNames.sort(self.cmp)
        for testFn in testFnNames:
            exec 'self.%s()' % testFn
        return

    def testPresence(self):
        """Test for presence of node."""
        try:
            num_channels, ts = self._ambman.getNumberOfChannels()
        except ControlExceptions.CAMBErrorEx:
            self.fail('Failed AMB transaction getting number of channels')
        self.failUnless(num_channels > self._channel)
        try:
            nodes, ts = self._ambman.getNodes(self._channel)
        except ControlExceptions.CAMBErrorEx:
            self.fail('Failed AMB transaction getting node list')
        def findNode():
            for n in nodes:
                if n.node == self._node_id:
                    return 1
            return 0
        self.failUnless(findNode())
        return

    def testSN(self):
        """Test response to AMBSI serial number monitor request."""
        try:
            sn, ts = self._ambman.findSN(self._channel, self._node_id)
        except ControlExceptions.CAMBErrorEx:
            self.fail('Failed AMB transaction getting serial number')
        return

    def testTemperature(self):
        """Test response to AMBSI temperature monitor request."""
        try:
            temp, ts = self._ambman.getTemperature(self._channel,
                                                   self._node_id)
        except ControlExceptions.CAMBErrorEx:
            self.fail('Failed AMB temperature transaction getting temperature')
        except:
            # looking at the AmbManager code, any other exception
            # indicates a failure to convert the data (i.e, struct.unpack)
            self.fail('Temperature data conversion failed')
        return

    def monitorSizeTest(self, rca, size):
        """Test the size of a monitor response. This method is meant
        to be used by other test methods.

        Parameters:
          rca - relative CAN address
          size - number of bytes to be returned by the monitor point

        Return:
          the result of the monitor transaction
        """
        try:
            result = self.monitor(rca)
        except ControlExceptions.CAMBErrorEx:
            self.fail('Failed AMB transaction')
        self.failUnlessEqual(len(result[0]), size, 'Wrong monitor data length')
        return result

def usage(filename):
    """Print usage message for command line invocation of main()."""
    print 'usage: %s <amb machine name> <channel number> <node id>+' % \
          filename
    return

def main(testCase, filename):
    """Run standalone unit test from the command line. This is meant
    to be called from executable Python modules that define TestCase
    subclasses. The tests carried out by this function use the
    unittest.TextTestRunner to conduct the test. All methods in the
    given testCase class that begin with the prefix 'test' will be
    invoked in the test. (It was decided not to use runTest(), since
    using doing would obscure the individual test functions in the
    output, at least to a degree.)

    The function takes parameters from the command line to determine
    which AMB node to test.

    Parameters:
      testCase - test case class from which a subclass will be created
                 for instantiation to run the unit tests
      filename - the file name from which this function is called
    """
    import sys
    if len(sys.argv) < 4:
        usage(filename)
    else:
        suite = unittest.TestSuite()
        unittest.defaultTestLoader.sortTestMethodsUsing = testCase.cmp

        # send stdout to /dev/null temporarily to quiet some annoying
        # output of AmbManager
        sys.stdout = file('/dev/null', 'w')

        # create test class for each node id given in the command line
        machine = sys.argv[1]
        channel = int(sys.argv[2])
        node_id = int(sys.argv[3])
        for i in range(3, len(sys.argv)):
            TCClass = testCase.makeTestCaseClass(machine, channel, node_id)
            node_test = \
                unittest.defaultTestLoader.loadTestsFromTestCase(TCClass)
            suite.addTest(node_test)
        sys.stdout = sys.__stdout__

        # run the unit tests
        tr = unittest.TextTestRunner(verbosity=2)
        tr.run(suite)
    return

if __name__ == '__main__':
    main(TestCase, __file__)
