#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# jkern  2006-03-29  created
#

'''
AmbSocketServer: 

OVERVIEW
The AmbSocketServer provides a simple socket interface, which can be used to
access the Low Level AMB Bus throughout the CONTROL system.

The server is run on the ACC computer and services port 2000.

There are essentially 4 modes of operation for the AmbSocketServer Monitor,
Command, Reset, and GetNodes.  In general the communication protocol for all 4
modes is the same.  A request structure is submitted to the socket, and a
return structure is then returned.  The only exception to this protocol is the
getNodes mode described below.


REQUEST STRUCTURE
The Request Structure consists of 18 bytes (Byte Numbers 0-17):

Byte[0,1] are the Resource identifier.  Valid values are -5 to 108 inclusive.
1-40 refers to the Antenna Bus master on antenna DV01 - DV40, 
41-80 refers to the Antenna Bus master on antenna DA41 - DA80,
81-84 refers to the Antenna Bus master on antenna PM01 - PM04, 
85-96 refers to the Antenna Bus master on the 7m Melco antenna CM01 - CM12,   
97-108 refers to the holography minirack LA01-LA11
0 refers to the ARTM,
-1 to -4 refer to DMCs 1 to 4
and -5 refers to the LMC.

Byte[2,3] are the bus identifier, bus numbering starts at 0,

Byte[4,7] Are the Complete 29 bit Can Identifier,  The top 11 bits of the 29
bit address are the Node Number and the remaining 18 bits are the Relative Can
Address.

Byte[8] is an integer Identifying the requested mode:
       0 is a monitor
       1 is a command
       2 is a getNodes
       3 is a reset

Byte[9] is the Length of the valid data.  8 Bytes of data are always sent, but
only the first <Byte[9]> of them are valid.  For all modes but command this
value should be 0.

Byte[10-17] contain the data used in a command, this field is ignored in all
other cases.


RESPOSE STRUCTURE
The Response Structure consists of 13 bytes (Byte Numbers 0-12)
Byte[0-3] contain the error code.  In the case of a successful completion 0 is
returned, otherwise a negative number is returned.  The meaning of the error
codes is specified below.

Byte[4] contains the number of valid data bytes in the data field, in case of
error, or a command this will be 0.  In the case of getNodes (mode 2) this
byte has special meaning (see below)

Byte[5-12] contain the returned data, 8 bytes are always returned but only the
first <Byte[4]> of these bytes are valid.


GETNODES (Mode 2) PROTOCOL
Like any other command this starts with a request, the mode byte (Byte[8])
should be set to 2.

The return will be a standard Response structure. If there is an error returned
(Byte[0-3] != 0) then the protocol exits immediately.  If no error is returned
then Byte[4] contains the number of nodes found on the specified server / bus
combination.

The client _must_ then read Byte[4] structures of 12 bytes each.  These
structures contain:

Byte[0-3] contains the Node ID.
Byte[4-11] contain the Node Serial Number

ERROR CODES
The possible error Codes and their meanings are:

TIMEOUT_ERROR       5002   CAN Bus Timeout
REQUEST_ERROR       5003   Received request could not be parsed
MONUNPK_ERROR       5004   Unable to unpack monitor data
MANAGER_UNAVBLE     5005   Requested Manager Unavailable
ILLEGAL_MGRNAME     5006   Request for an unknown Manager
CANDRV_ERROR        5007   Low level can driver failure
CONUNPK_ERROR       5008   Error unpacking Control data structure
BADCHAN_ERROR       5009   Requested Channel unavailable for manager
CANWRT_ERROR        5010   Error writing to the CAN bus
CANREAD_ERROR       5011   Error reading from the CAB bus
FLUSHED_ERROR       5012   Operation was flushed by Server
READ_PERM_DENIED    5013   Permission to read from specified AmbManager Denied
WRITE_PERM_DENIED   5014   Permission to write to specified AmbManager Denied
ACCESS_DENIED       5015   The AmbManager denied read/write access
UNKNOWN_ERROR       5999   Otherwise undefined error condition

'''

import sys
from SocketServer import *
from socket import *
import threading
from select import *
import struct

import ControlSocketServer__POA

from Acspy.Servants.ContainerServices  import ContainerServices
from Acspy.Servants.ComponentLifecycle import ComponentLifecycle
from Acspy.Servants.ComponentLifecycle import ComponentLifecycleException
from Acspy.Servants.ACSComponent       import ACSComponent
from ACS import COMPSTATE_ERROR

from AmbSocketServerExceptions import IllegalResourceNameEx
from AmbSocketServerExceptionsImpl import IllegalResourceNameExImpl

import ControlExceptions
import omniORB

#------------------ Defines and Contants -----------------------
read_format  = '!2hL2B8s'
write_format = '!lB8s'

UNKNOWN_ERROR       = "Unknown error occured"
REQUEST_ERROR       = "Received request could not be parsed"
TIMEOUT_ERROR       = "CAN Bus Timeout"
MONUNPK_ERROR       = "Unable to unpack monitor data"
MANAGER_UNAVBLE     = "Requested Manager Unavailable"
ILLEGAL_MGRNAME     = "Request for an unknown Manager"
CANDRV_ERROR        = "Low level can driver failure"
CONUNPK_ERROR       = "Error unpacking Control data structure"
BADCHAN_ERROR       = "Requested Channel unavailable for manager"
CANWRT_ERROR        = "Error writing to the CAN bus"
CANREAD_ERROR       = "Error reading from the CAB bus"
FLUSHED_ERROR       = "Operation was flushed by Server"
READ_PERM_DENIED    = "Permission to read from specified AmbManager Denied"
WRITE_PERM_DENIED   = "Permission to write to specified AmbManager Denied"
ACCESS_DENIED       = "The AmbManager denied read/write access"

ErrorCodes = {
    TIMEOUT_ERROR:     5002,
    REQUEST_ERROR:     5003,
    MONUNPK_ERROR:     5004,
    MANAGER_UNAVBLE:   5005,  
    ILLEGAL_MGRNAME:   5006,
    CANDRV_ERROR:      5007,
    CONUNPK_ERROR:     5008,
    BADCHAN_ERROR:     5009,
    CANWRT_ERROR:      5010,
    CANREAD_ERROR:     5011,
    FLUSHED_ERROR:     5012,
    READ_PERM_DENIED:  5013,
    WRITE_PERM_DENIED: 5014,
    ACCESS_DENIED:     5015,
    UNKNOWN_ERROR:     5999
    }

PERM_READ  = 0
PERM_WRITE = 1



def printHex(st):
    dataLen = len(st)
    up = struct.unpack("!%dB" % dataLen, st)
    strf = "0x"
    for i in up:
        strf += "%02x " % i 
    return strf

# ----------------- Helper Classes ------------------------------
class AmbException(Exception):
    '''
    Helper Class used internally to propagate exceptions.
    Also handles translation for CAMBExceptions to
    AMBSocketServer error codes
    '''
    def __init__(self, ErrorCode = 0, CAMBEx = None):
        if CAMBEx is not None:
            if int(CAMBEx.errorTrace.previousError[0].data[0].value.split()[2]) == 9:
                # Timeout error
                self.errorCode = ErrorCodes[TIMEOUT_ERROR]
            elif int(CAMBEx.errorTrace.previousError[0].data[0].value.split()[2]) == 3:
                # Bad Channel Error
                self.errorCode = ErrorCodes[BADCHAN_ERROR]
            elif int(CAMBEx.errorTrace.previousError[0].data[0].value.split()[2]) == 6:
                self.errorCode = ErrorCodes[CANWRT_ERROR]
            elif int(CAMBEx.errorTrace.previousError[0].data[0].value.split()[2]) == 7:
                self.errorCode = ErrorCodes[CANREAD_ERROR]
            elif int(CAMBEx.errorTrace.previousError[0].data[0].value.split()[2]) == 8:
                self.errorCode = ErrorCodes[FLUSHED_ERROR]
            else:
                self.errorCode = ErrorCodes[CANDRV_ERROR]
        else:
            self.errorCode = ErrorCode

    def __str__(self):
        for key in ErrorCodes.keys():
            if self.errorCode == ErrorCodes[key]:
                return key
        return UNKNOWN_ERROR



class ResourceManager:
    '''
    Global class which handles component references to the AMB Managers
    and access control
    '''
    def __init__(self, componentAccessor):
        self.componentAccessor = componentAccessor
        self._ambManagerList = []
        self._writeAccessList = []
        self._readAccessList = []
        self.maxRange = 108
        self.minRange = -5
        self.melco12m = 80
        self.melco7m = 84
        self.lab = 96

        for n in range(0, self.maxRange - self.minRange):
            self._ambManagerList.append(None)
            self._readAccessList.append(True)
            self._writeAccessList.append(True)
        self._numberOfClients = 0


    def getAmbManager(self, abm, permission):
        '''
        Return a reference to the AMB Manager for this component.

      1-40 Refer to DV01 - DV40
      41-80 Refer to DA41 - DA80
      81-84 Refer to 12m Melco antennas PM01 - PM04
      85-96 Refer to the 7m Melco antennas CM01 - CM12   
      97-108  Refers to the holography minirack LA01-LA11
       0     Refers to the ARTM
      -1    Refers to the DMC1
      -2    Refers to the DMC2
      -3    Refers to the DMC3
      -4    Refers to the DMC4
      -5    Refers to the LMC

        '''
        if abm < self.minRange or abm > self.maxRange:
            raise AmbException(ErrorCode = ErrorCodes[ILLEGAL_MGRNAME])

        if self._ambManagerList[abm - self.minRange] is None:
            try:
                if abm == -1:
                    # DMC
                    self._ambManagerList[abm - self.minRange] = self.componentAccessor.getComponent("CONTROL/DMC/AmbManager")
                elif abm == -2:
                    # DMC-2
                    self._ambManagerList[abm - self.minRange] = self.componentAccessor.getComponent("CONTROL/DMC2/AmbManager")
                elif abm == -3:
                    # DMC-3
                    self._ambManagerList[abm - self.minRange] = self.componentAccessor.getComponent("CONTROL/DMC3/AmbManager")
                elif abm == -4:
                    # DMC-4
                    self._ambManagerList[abm - self.minRange] = self.componentAccessor.getComponent("CONTROL/DMC4/AmbManager")
                elif abm == -5:
                    # LMC
                    self._ambManagerList[abm - self.minRange] = self.componentAccessor.getComponent("CONTROL/LMC/AmbManager")
                elif abm == 0:
                    #ARTM
                    self._ambManagerList[abm - self.minRange] = self.componentAccessor.getComponent("CONTROL/AOSTiming/AmbManager")
                elif abm < 41:
                    #Vertex Antenna
                    self._ambManagerList[abm - self.minRange] = self.componentAccessor.getComponent("CONTROL/DV%02d/AmbManager" % abm)
                elif abm < 81:
                    #Alcatel Antenna
                    self._ambManagerList[abm - self.minRange] = self.componentAccessor.getComponent("CONTROL/DA%02d/AmbManager" % abm)
                elif abm < 85:
                    #Melco 12m Antenna
                    abmtmp = abm-self.melco12m
                    self._ambManagerList[abm - self.minRange] = self.componentAccessor.getComponent("CONTROL/PM%02d/AmbManager" % abmtmp)
                elif abm < 97:
                    #Melco 7m Antenna96
                    abmtmp = abm-self.melco7m
                    self._ambManagerList[abm - self.minRange] = self.componentAccessor.getComponent("CONTROL/CM%02d/AmbManager" % abmtmp )
                elif abm < self.maxRange:
                    # LAB Antennas
                    abmtmp = abm-self.lab
                    self._ambManagerList[abm - self.minRange] = self.componentAccessor.getComponent("CONTROL/LA%02d/AmbManager" % abmtmp) 
            except:
                # Error getting the manager
                print "Error getting the manager"
                sys.stdout.flush()
                self._ambManagerList[abm - self.minRange] = None

        if self._ambManagerList[abm - self.minRange] is None:
            raise AmbException(ErrorCode = ErrorCodes[MANAGER_UNAVBLE])

        if permission == PERM_READ and not self._readAccessList[abm - self.minRange]:
            raise AmbException(ErrorCode = ErrorCodes[READ_PERM_DENIED])

        if permission == PERM_WRITE and not self._writeAccessList[abm - self.minRange]:
            raise AmbException(ErrorCode = ErrorCodes[WRITE_PERM_DENIED])
        
        return self._ambManagerList[abm - self.minRange]

    def clientConnect(self):
        '''
        Increments the counter on the number of clients connected
        '''
        self._numberOfClients += 1

    def clientDisconnect(self):
        '''
        Decrements the counter on the number of clients connected
        '''
        self._numberOfClients -= 1

    def getNumberOfClients(self):
        '''
        Returns the number of clients currently connected to the system
        '''
        return self._numberOfClients


    def resourceStringToInt(self, ResourceName):
        '''
        This method translates the string resource name into the correct
        numerical value for use in the resources class
        '''

        if ResourceName.upper() == "DMC":
            return -1
        elif ResourceName.upper() == "DMC2":
            return -2
        elif ResourceName.upper() == "DMC3":
            return -3
        elif ResourceName.upper() == "DMC4":
            return -4
        elif ResourceName.upper() == "LMC":
            return -5
        elif ResourceName.upper() == "ARTM":
            return 0
        # Vertex
        elif ResourceName.upper()[:2] == "DV":
            if len(ResourceName) != 4:
                #Fail
                raise IllegalResourceNameExImpl()
            try:
                antNo = int(ResourceName[2:4])
            except:
                # Not an integer
                raise IllegalResourceNameExImpl()
            if antNo < 1 or antNo > 40:
                # Illegal Antenna Number
                raise IllegalResourceNameExImpl()
            return antNo
        # Alcatel
        elif ResourceName.upper()[:2] == "DA":
            if len(ResourceName) != 4:
                #Fail
                raise IllegalResourceNameExImpl()
            try:
                antNo = int(ResourceName[2:4])
            except:
                # Not an integer
                raise IllegalResourceNameExImpl()
            if antNo < 41 or antNo > 80:
                # Illegal Antenna Number
                raise IllegalResourceNameExImpl()
            return antNo
        # Melco 12m
        elif ResourceName.upper()[:2] == "PM":
            if len(ResourceName) != 4:
                #Fail
                raise IllegalResourceNameExImpl()
            try:
                antNo = int(ResourceName[2:4])
            except:
                # Not an integer
                raise IllegalResourceNameExImpl()
            if antNo < 1 or antNo > 4:
                # Illegal Antenna Number
                raise IllegalResourceNameExImpl()
            return (antNo + self.melco12m)
        # Melco 7m
        elif ResourceName.upper()[:2] == "CM":
            if len(ResourceName) != 4:
                #Fail
                raise IllegalResourceNameExImpl()
            try:
                antNo = int(ResourceName[2:4])
            except:
                # Not an integer
                raise IllegalResourceNameExImpl()
            if antNo < 1 or antNo > 12:
                # Illegal Antenna Number
                raise IllegalResourceNameExImpl()
            return (antNo + self.melco7m)
        # LAB antennas
        elif ResourceName.upper()[:2] == "LA":
            if len(ResourceName) != 4:
                #Fail
                raise IllegalResourceNameExImpl()
            try:
                antNo = int(ResourceName[2:4])
            except:
                # Not an integer
                raise IllegalResourceNameExImpl()
            if antNo < 1 or antNo > 11:
                # Illegal Antenna Number
                raise IllegalResourceNameExImpl()
            return (antNo + self.lab)
        else:
            raise IllegalResourceNameExImpl()

    def enableReadWriteAccess(self, ResourceName):
        '''
        Gives full access control to a manager from the socket interface
        This is the default state
        '''
        try:
            self._readAccessList[self.resourceStringToInt(ResourceName) - self.minRange] = True
            self._writeAccessList[self.resourceStringToInt(ResourceName) - self.minRange] = True
        except IllegalResourceNameExImpl, e:
            raise IllegalResourceNameExImpl(exception=e)
        
    def enableReadOnlyAccess(self, ResourceName):
        self._readAccessList[self.resourceStringToInt(ResourceName) - self.minRange] = True
        self._writeAccessList[self.resourceStringToInt(ResourceName) - self.minRange] = False
                    
    def disableAccess(self, ResourceName):
        self._readAccessList[self.resourceStringToInt(ResourceName) - self.minRange] = False
        self._writeAccessList[self.resourceStringToInt(ResourceName) - self.minRange] = False
                    
    
class AmbMessage:
    global read_format
    read_length = struct.calcsize(read_format)
    _RCA_BITS = 18
    _RCA_MASK = (1 << _RCA_BITS) - 1

    _NODE_BITS = 11
    _NODE_MASK = (1 << _NODE_BITS) - 1
    _NODE_SHIFT = _RCA_BITS
    
    def __init__(self, rawMessage):
        try:
            self.abm, self.bus, canid, self.mode, canLength, canData = \
                      struct.unpack(read_format, rawMessage)
            self.node = int((canid >> self._NODE_SHIFT) & self._NODE_MASK) - 1
            self.rca = int(canid & self._RCA_MASK)
            self.canData = canData[:canLength]
        except Exception, e:
            raise AmbException(ErrorCode = ErrorCodes[REQUEST_ERROR])

    def __str__(self):
        return "ABM\t%d\nBus\t%d\nNode\t0x%x\nRCA\t0x%x\nData\t%s" % \
               (self.abm, self.bus, self.node, self.rca, self.canData)


# ----------------- Actual Classes That Do The Work

class MandCRequestHandler(StreamRequestHandler):
    '''
    TCP/IP Server for LabView

    The request handlers are instantiated for each client connection by a TCP
    Server, in this case a ThreadingTCPServer.

    The message is a standard format with a mode byte which controls which
    method is executed.
    '''
    global resources

    def setup(self):
        '''
        Call for each new client connection accepted

        This method is called by the StreamRequestHandler
        '''
        global write_format

        self.writeFormat = write_format
        self.connection = self.request
        self.resources = resources
        self.rfile = self.connection.makefile('rb', AmbMessage.read_length)
        self.wfile = self.connection.makefile('wb', 256)

        # Because of the ACS python issues, import the first ABM now!
        #self.resources.getAmbManager(1, PERM_READ)


    def _writeError(self, ErrorCode):
        # Put a log message out
        try:
            self.wfile.write(struct.pack(self.writeFormat, ErrorCode, 0, ''))
            self.wfile.flush()
        except:
            print "Exception reporting error"

    def handle(self):
        '''
        Handle each client connection for the connection lifetime

        This method is called by the StreamRequestHandler
        '''
        self.resources.clientConnect()
        while 1:
            # select on input or exception on socket, kill socket after 1 hour
            iwtd, owtd, ewtd = select([self.rfile], [], [self.rfile], 60. * 60.)

            if len(iwtd) == 0 and len(owtd) == 0:
                break

            # if exception then break out of loop (and return)
            if len(ewtd) != 0:
                break

            try:
                rawMsg = self.rfile.read(AmbMessage.read_length)
                if len(rawMsg) == 0:
                    # No Data to read but yet we're here.
                    # Assume socket closed and exit handler
                    break
                ambMsg = AmbMessage(rawMsg)
            except AmbException, e:
                self._writeError(e.errorCode)
                continue
            except Exception, e:
                self._writeError(ErrorCodes[UNKNOWN_ERROR])
                continue

            if ambMsg.mode == 0:
                self._monitor(ambMsg)
            elif ambMsg.mode == 1:
                self._control(ambMsg)
            elif ambMsg.mode == 2:
                self._list_nodes(ambMsg)
            elif ambMsg.mode == 3:
                self._reset_bus(ambMsg)
                
        self.resources.clientDisconnect()
        return

    def _monitor(self, ambMsg):
        '''
        Execute CAN monitor request

        usage: _monitor()

        RETURN:
        '''

        try:
            mgr = self.resources.getAmbManager(ambMsg.abm, PERM_READ)
        except AmbException, e:
            self._writeError(e.errorCode)
            return
        except omniORB.CORBA.TRANSIENT, e:
            self._writeError(ErrorCodes[MANAGER_UNAVBLE])
            return
        except Exception, e:
            self._writeError(ErrorCodes[UNKNOWN_ERROR])
            return

        # Filtering holography monitor point
        # 0x30004 which is not implemented at the current firmware.
        if ambMsg.bus == 3 and ambMsg.node == 29 and ambMsg.rca == 196612:
            data = "x00"
        else:
            try:
                (data, time) = mgr.monitor(ambMsg.bus, ambMsg.node, ambMsg.rca)
            except ControlExceptions.CAMBErrorEx, e:
                ex = AmbException(CAMBEx = e)
                self._writeError(ex.errorCode)
                return
            except ControlExceptions.InvalidRequestEx, e:
                ex = AmbException(ErrorCode = ErrorCodes[ACCESS_DENIED])
                self._writeError(ex.errorCode)
                return
            except Exception, e:
                self._writeError(ErrorCodes[UNKNOWN_ERROR])
                return

        # unpack answer into byte array, and pad with 0s
        try:
            format = '%ds' % len(data)
            can_data, = struct.unpack(format, data)
            for n in range(len(can_data), 8):
                can_data = can_data + '\x00'
        except Exception, e:
            # Error unpacking returned data
            self._writeError(ErrorCodes[MONUNPK_ERROR])
            return
        
        self.wfile.write(struct.pack(self.writeFormat, 0, len(data), can_data))
        self.wfile.flush()

    
    def _control(self, ambMsg):
        '''
        Execute CAN control request

        usage: _control(can_length, can_data)

        can_length is number of bytes in message
        can_data is some kind of integer type

        This unpacks the CAN data into a standard integer type, and if the
        type is an odd number of bytes, an error is generated.  The CAN
        control requires integer  passed to it.

        RETURN:
        '''
        try:
            mgr = self.resources.getAmbManager(ambMsg.abm, PERM_WRITE)
        except AmbException, e:
            self._writeError(e.errorCode)
            return
        except:
            self._writeError(ErrorCodes[UNKNOWN_ERROR])
            return

        try:
            mgr.command(ambMsg.bus, ambMsg.node, ambMsg.rca, ambMsg.canData)
        except ControlExceptions.CAMBErrorEx, e:
            ex = AmbException(CAMBEx = e)
            self._writeError(ex.errorCode)
            return
        except ControlExceptions.InvalidRequestEx, e:
            ex = AmbException(ErrorCode = ErrorCodes[ACCESS_DENIED])
            self._writeError(ex.errorCode)
            return
        except omniORB.CORBA.TRANSIENT, e:
            self._writeError(ErrorCodes[MANAGER_UNAVBLE])
            return
        except Exception, e:
            self._writeError(ErrorCodes[UNKNOWN_ERROR])
            return
        
        self.wfile.write(struct.pack(self.writeFormat, 0, 0, ''))
        self.wfile.flush()
            
    def _reset_bus(self, ambMsg):
        '''
        Execute CAN bus reset for this bus number

        usage: _reset_bus()

        RETURN:
        '''
        try:
            mgr = self.resources.getAmbManager(ambMsg.abm, PERM_WRITE)
        except AmbException, e:
            self._writeError(e.errorCode)
            return
        except:
            self._writeError(ErrorCodes[UNKNOWN_ERROR])
            return

        try:
            mgr.reset(ambMsg.bus)
        except ControlExceptions.CAMBErrorEx, e:
            ex = AmbException(CAMBEx = e)
            self._writeError(ex.errorCode)
            return
        except ControlExceptions.InvalidRequestEx, e:
            ex = AmbException(ErrorCode = ErrorCodes[ACCESS_DENIED])
            self._writeError(ex.errorCode)
            return
        except omniORB.CORBA.TRANSIENT, e:
            self._writeError(ErrorCodes[MANAGER_UNAVBLE])
            return
        except:
            self._writeError(ErrorCodes[UNKNOWN_ERROR])
            return

        self.wfile.write(struct.pack(self.writeFormat, 0, 0, ''))
        self.wfile.flush()
    
    def _list_nodes (self, ambMsg):
        '''
        List nodes for this bus

        usage: _list_nodes()

        RETURN:
        '''
        try:
            mgr = self.resources.getAmbManager(ambMsg.abm, PERM_READ)
        except AmbException, e:
            self._writeError(e.errorCode)
            return
        except:
            self._writeError(ErrorCodes[UNKNOWN_ERROR])
            return

        try:
            (nodes, time) = mgr.getNodes(ambMsg.bus)
        except ControlExceptions.CAMBErrorEx, e:
            ex = AmbException(CAMBEx = e)
            self._writeError(ex.errorCode)
            return
        except ControlExceptions.InvalidRequestEx, e:
            ex = AmbException(ErrorCode = ErrorCodes[ACCESS_DENIED])
            self._writeError(ex.errorCode)
            return
        except omniORB.CORBA.TRANSIENT, e:
            self._writeError(ErrorCodes[MANAGER_UNAVBLE])
            return
        except:
            self._writeError(ErrorCodes[UNKNOWN_ERROR])
            return

        self.wfile.write(struct.pack(self.writeFormat, 0, len(nodes), ''));
        for n in nodes:
            self.wfile.write(struct.pack('!L8s', n.node, n.sn))
        self.wfile.flush()
            
# ----------------- ACS Component Binding --------------------------
class AmbServerThread(threading.Thread, ThreadingTCPServer):
    def __init__(self, threadName, addr, handler):
        self.exitFlag = False
        self.pause = 10.0 #10 Second loop on shutting down
        threading.Thread.__init__(self, name = threadName)
        ThreadingTCPServer.__init__(self, addr, handler)
        
        
    def run(self):
        while not self.exitFlag:
            r, w, e = select([self.socket], [], [], self.pause)
            if r:
                self.handle_request()
        
    def exit(self):
        self.exitFlag = True
            

class AmbSocketServer(ControlSocketServer__POA.AmbSocketServer,
                      ACSComponent,
                      ContainerServices,
                      ComponentLifecycle):
    def __init__(self):
        ACSComponent.__init__(self)
        ContainerServices.__init__(self)
        
    def initialize(self):
        global resources
        resources = ResourceManager(self)
        ComponentLifecycle.initialize(self)
        try:
            self.server = AmbServerThread('AmbServerThread',
                                          (gethostbyname(gethostname()), 2000),
                                          MandCRequestHandler)
        except Exception, e:
            # Error binding to socket, log error and exit
            raise ComponentLifecycleException("Error binding to socket: "
                                              + e[1])

    def execute(self):
        #Because of the way lifecycle exceptions are handled in python
        # we need to check the error state here.
        if self._get_componentState() != COMPSTATE_ERROR:
            ComponentLifecycle.execute(self)
            self.server.start()

    def cleanUp(self):
        self.server.exit()
        self.server.join()
        ComponentLifecycle.cleanUp(self)

    def aboutToAbort(self):
        ComponentLifecycle.aboutToAbort(self)
        self.server.exit()
        self.server.join()

    def numberOfClients(self):
        global resources
        return resources.getNumberOfClients();

    def enableReadWriteAccess(self, ResourceName):
        global resources
        try:
            resources.enableReadWriteAccess(ResourceName)
        except IllegalResourceNameExImpl, e:
            raise IllegalResourceNameExImpl(exception = e)
        
    def enableReadOnlyAccess(self, ResourceName):
        global resources
        resources.enableReadOnlyAccess(ResourceName)
                    
        
    def disableAccess(self, ResourceName):
        global resources
        try:
            resources.disableAccess(ResourceName)
        except IllegalResourceNameEx, e:
            raise IllegalResourceNameEx(e)

        
# ----------------------- Stand Alone Version -------------------------
# The Server class spawns a new thread for each connection
class StandAloneServer (threading.Thread):
    '''
    Create a threading TCP server and run it as a thread
    '''
    def __init__ (self, myname, addr, handler):
        threading.Thread.__init__(self, name = myname)
        self.server = ThreadingTCPServer(addr, handler)

    def run(self):
        self.server.serve_forever()

def main():
    '''
    Run the CANSockServer as a task
    '''
    global resources
    import sys
    import getopt

    from Acspy.Clients.SimpleClient import PySimpleClient


    short_options = 'hp:'
    long_options = ['help', 'port=']
    port = 2000

    def usage():
        '''
    usage: CANSockServer [(-h | --help)] [{-p | --port} tcp port]

    -p or --port port number to use, defaults to %d
    -h or --help print this message

        '''
        print usage.__doc__ % (port, manager_name)

    try:
        opts, args = getopt.getopt(sys.argv [1:], short_options, long_options)
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    
    ip = gethostbyname(gethostname())
    
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        if o in ("-p", "--port"):
            port = int(a)
    
    print '''
    
        *** Debug Version April 3 2007 ***
        Run CAN server on %s, port %d.
    ''' % (ip, port)


    resources = ResourceManager(PySimpleClient())

#    _initCANServer ()
    server = StandAloneServer("Monitor", (ip, port), MandCRequestHandler)
    server.start()
    
    #
    # TODO wait for event.  when received, stop servers and exit.
    #
    # For now, just wait on a server which will never exit
    #
    server.join()
    
if __name__ == '__main__':
    main()
