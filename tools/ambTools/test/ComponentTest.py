#!/usr/bin/env python
import socket
import struct
import unittest
import time
from Acspy.Clients.SimpleClient import PySimpleClient
from AmbSocketServerExceptions import IllegalResourceNameEx

def getCanID(Node, RCA):
    _RCA_BITS = 18
    _RCA_MASK = (1 << _RCA_BITS) - 1
    _NODE_BITS = 11
    _NODE_MASK = (1 << _NODE_BITS) - 1
    _NODE_SHIFT = _RCA_BITS
    return int((((Node + 1) & _NODE_MASK) << _NODE_SHIFT) | (RCA & _RCA_MASK))

class clientConnection:
    def __init__(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((socket.gethostname(), 2000))
        time.sleep(0.1) # Allow time for server to acknowledge

    def __del__(self):
        self.socket.close()
        time.sleep(0.1) # Allow time for server to acknowledge

    def monitor(self, ABM, Bus, Node, RCA):
        Can_ID = getCanID(Node,RCA)
        if self.socket.send(struct.pack("!2HL2B8s",ABM, Bus,
                                        Can_ID, 0 , 0,'')) != 18:
            print "Error Writing to Socket"
        data = self.socket.recv(13)
        if len(data) != 13:
            print "Return length incorrect"
        (error, lenData, canData) = struct.unpack("!lB8s", data)
        canData = canData[:lenData]
        return (error, canData)

    def command(self, ABM, Bus, Node, RCA,Data):
        Can_ID = getCanID(Node,RCA)
        struct.pack("!2HL2B8s",ABM, Bus, Can_ID, 1 , len(Data),Data)
        if self.socket.send(struct.pack("!2HL2B8s",ABM, Bus,
                                        Can_ID,  1 , len(Data),Data)) != 18:
            print "Error Writing to Socket"
        data = self.socket.recv(13)
        if len(data) != 13:
            print "Return length incorrect"
        (error, lenData, canData) = struct.unpack("!lB8s", data)
        return error
        
class automated(unittest.TestCase):
    def __init__(self,  methodName="runTest"):
        unittest.TestCase.__init__(self,methodName)

    def setUp(self):
        global client
        self.ref = client.getComponent("CONTROL/AmbSocketServer")
        
    def tearDown(self):
        global client
        client.releaseComponent(self.ref._get_name())


    def test01(self):
        '''
        Test that the number of clients is correctly reported.  Also
        tests that at least 99 clients can be simultaneously connected.
        '''

        connection = []
        for i in range(0,99):
            numberOfClients = self.ref.numberOfClients()
            self.failIf(numberOfClients != len(connection),
                        "Incorrect number of clients reported "
                        "(%d reported should be %d)" % 
                        (numberOfClients,len(connection)))
            connection.append(clientConnection())

        while len(connection) > 0:
            numberOfClients = self.ref.numberOfClients()
            self.failIf(numberOfClients != len(connection),
                        "Incorrect number of clients reported"+
                        "(%d reported should be %d)" %
                        (numberOfClients,len(connection)))
            connection.pop()

        self.failIf(self.ref.numberOfClients() != 0,
                    "Incorrect number of clients reported (should be 0)")
            
    def test02(self):
        '''
        Test that the interpreting of resource names is done correctly
        '''
        goodNames = ["DMC", "ARTM"]
        for x in range(1,65):
            goodNames.append("ALMA%03d" % x)

        badNames = ["foo", "ALMA00001", "ALMAbar", "ALMA067", "ALMA000"]

        for resource in goodNames:
            try:
                self.ref.enableReadWriteAccess(resource)
            except IllegalResourceNameEx, e:
                self.fail("Good resource name \"" + resource +"\" failed.")

        for resource in badNames:
            self.failUnlessRaises(IllegalResourceNameEx,
                                  self.ref.enableReadWriteAccess,
                                  resource)


    def test03(self):
        '''
        Test that setting the permissions works as expected
        '''
        #Start with access enabled.
        self.ref.enableReadWriteAccess("ALMA001")

        connection = clientConnection()
        (error, data) = connection.monitor(1, 0 , 0x30,0x0)
        self.failIf(error != 0, "Error Monitoring ALMA001 (%d)" % error)
        error = connection.command(1, 0, 0x30, 0x81, "1")
        self.failIf(error != 0, "Error Commanding ALMA001 (%d)" % error)

        self.ref.enableReadOnlyAccess("ALMA001")
        (error, data) = connection.monitor(1, 0 , 0x30,0x0)
        self.failIf(error != 0, "Error Monitoring ALMA001 (%d)" % error)
        error = connection.command(1, 0, 0x30, 0x81, "1")
        self.failIf(error != 5014,
                    "Expected permission error not found commanding ALMA001")
        
        self.ref.disableAccess("ALMA001")
        (error, data) = connection.monitor(1, 0 , 0x30,0x0)
        self.failIf(error != 5013,
                    "Error permission error not found monitoring ALMA001")
        error = connection.command(1, 0, 0x30, 0x81, "1")
        self.failIf(error != 5014,
                    "Expected permission error not found commanding ALMA001")

        self.ref.enableReadWriteAccess("ALMA001")
        (error, data) = connection.monitor(1, 0 , 0x30,0x0)
        self.failIf(error != 0, "Error Monitoring ALMA001 (%d)" % error)
        error = connection.command(1, 0, 0x30, 0x81, "1")
        self.failIf(error != 0, "Error Commanding ALMA001 (%d)" % error)


if __name__ == '__main__':
    global client
    client = PySimpleClient()
    unittest.main()
    client.disconnect()
