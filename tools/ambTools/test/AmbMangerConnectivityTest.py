#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# tjuerges  Oct 6, 2008  created
#


import Acspy.Clients.SimpleClient
import Acspy.Common.QoS
import Acspy.Util.ACSCorba
import time


'''
AmbManagerConnectivity test

This test checks if ACS successfully revives CORBA references for objects which
went away for some time.

How to use it:
--------------
Just run this script when the DV01 antenna is operational, i.e. the CONTROL/DV01
component went through hwOperational successfully.
'''

Acspy.Common.QoS.setObjectTimeout(Acspy.Util.ACSCorba.getManager(), 20000)
sc = Acspy.Clients.SimpleClient.PySimpleClient.getInstance()

dv01String = "CONTROL/DV01"
try:
    print "Getting a non-sticky antenna reference for %s..." % dv01String
    dv01 = sc.getComponentNonSticky(dv01String)
    print "Got it."
except Exception, ex:
    print "Could not retrieve the antenna reference. Check if the antenna is \
    in operational state!"
    exit -1

try:
    print "Now let\'s check if the AmbManager is running by querying the \
    antenna component..."
    state = dv01.isAmbManagerRunning()
    print "AmbManager is ",
    if state == False:
        print "not ",
    print "running."
    if state == False:
        print "You want to start the AmbManager in the Antenna component now. \
        After the AmbManager is started, run this script again."
        exit -1
except Exception, ex:
    print "Could not determine the AmbManager state from the Antenna component \
    response.  Make sure that the Antenna component is operational!"
    exit -1

mgrString = "CONTROL/DV01/AmbManager"
try:
    print "Getting a non-sticky AmbManager reference for %s..." % mgrString
    mgr = sc.getComponentNonSticky(mgrString)
    print "Got it."
except Exception, ex:
    print "Could not retrieve the AmbManager reference. Check if the antenna \
    is in operational state and if the AmbManager has already been started \
    from the Antenna component!"

try:
    print "Now let\'s try the same test again but asking the AmbManager itself \
    ..."
    state2 = mgr.monitoringEnabled()
    print "AmbManager is ",
    if state == False:
        print "not ",
    print "running."
except Exception, ex:
    print "Could not determine the AmbManager state from the AmbManager \
    component response."
    exit -1

try:
    print "Now we turn the AmbManager off..."
    dv01.stopAmbManager()
    print "Okay, AmbManager is off.  Let\'s verify this..."
    state = dv01.isAmbManagerRunning()
    if state != False:
        print "The AmbManager did not stop.  There must be something wrong \
        with the Antenna component.  Please check!" 
        exit -1
    else:
        print "Okay.  AmbManager is really off."
except Exception, ex:
    print "Caught an exception while trying to turn the AmbManager off.  \
    Don\'t know what to do now because this should not happen."
    exit -1

print "Let's sleep one minute so that the ACS manager can discard all internal \
references..."
for i in range(60):
    time.sleep(1.0)
    print "Sleeping another %ds..." % (60 - i)
print "Okay."

try:
    print "Now let's execute something on the AmbManager.  A CORBA exception \
    should be thrown..."
    state = mgr.commandingEnabled()
    print "Strange.  No CORBA exception happened.  This should not be and \
    there is not much we can do now."
    exit -1
except Exception, ex:
    print "Okay. Looks good so far."

try:
    print "Now let's turn the AmbManager on again..."
    dv01.startAmbManager()
    print "Okay, AmbManager is off.  Let\'s verify this..."
    state = dv01.isAmbManagerRunning()
    if state != True:
        print "The AmbManager did not start.  There must be something wrong \
        with the Antenna component.  Please check!" 
        exit -1
    else:
        print "Okay.  AmbManager is up again."
except Exception, ex:
    print "Caught an exception while trying to turn the AmbManager on.  \
    Don\'t know what to do now because this should not happen."
    exit -1

try:
    print "ACS promises that the old reference is still valid. Let\'s try it..."
    state = mgr.monitoringEnabled()
    print "Great!  ACS works as expected!"
except Exception, ex:
    print "ACS failed to reactivate the same reference.  Please report this to \
    the ACS developers.  Send them this test script, too!"
