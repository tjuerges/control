#!/usr/bin/env python
import socket
import struct
import unittest


def getCanID(Node, RCA):
    _RCA_BITS = 18
    _RCA_MASK = (1 << _RCA_BITS) - 1
    _NODE_BITS = 11
    _NODE_MASK = (1 << _NODE_BITS) - 1
    _NODE_SHIFT = _RCA_BITS
    return int((((Node + 1) & _NODE_MASK) << _NODE_SHIFT) | (RCA & _RCA_MASK))

def packMonitor(ABM, Bus, Node, RCA):
    Can_ID = getCanID(Node,RCA)
    return struct.pack("!2HL2B8s",ABM, Bus, Can_ID, 0 , 0,'')

def packCommand(ABM, Bus, Node, RCA, Data):
    Can_ID = getCanID(Node,RCA)
    return struct.pack("!2HL2B8s",ABM, Bus, Can_ID, 1 , len(Data),Data)

def packList(ABM, Bus):
    return struct.pack("!2HL2B8s", ABM, Bus, 0, 2, 0, '')

def packReset(ABM, Bus):
    return struct.pack("!2HL2B8s", ABM, Bus, 0, 3, 0, '')

class automated(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        unittest.TestCase.__init__(self,methodName)

    def setUp(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((socket.gethostname(), 2000))
        
    def tearDown(self):
        self.socket.close()


    def test01(self):
        '''
        Simple test which just tests the ability to do a single monitor
        '''
        self.failIf(self.socket.send(packMonitor(1,0,0x30,0x0000)) != 18,
                    "Error writing to Socket")
        data = self.socket.recv(32)
        self.failIf(len(data) != 13, "Return length incorrect")
        (error, length, data) = struct.unpack('!lBQ',data)
        self.failIf(error != 0,
                    "Non-Zero Error code Returned (%d)" % error);
        self.failIf(length != 8, "Incorrect data length returned");

    def test02(self):
        '''
        Test to verify correct behavior of non-existant CAN node request
        '''
        #Monitor
        self.failIf(self.socket.send(packMonitor(1,0,0x128,0x0000)) != 18,
                    "Monitor: Error writing to Socket")
        data = self.socket.recv(32)
        self.failIf(len(data) != 13, "Monitor: Return length incorrect")
        (error, length, data) = struct.unpack('!lB8s',data)
        self.failIf(error != 5002,
                    "Monitor: Incorrect Error Code (%d)" % error)


    def test03(self):
        '''
        Test to verify correct errror handling when an illegal manager
        is requested
        '''
        # Monitor 
        self.failIf(self.socket.send(packMonitor(-2,0,0x30,0x0000)) != 18,
                    "Monitor: Error writing to Socket")
        data = self.socket.recv(32)
        self.failIf(len(data) != 13, "Monitor: Return length incorrect")
        (error, length, data) = struct.unpack('!lB8s',data)
        self.failIf(error != 5006,
                    "Monitor: Incorrect Error Code (%d)" % error)

        # Command
        self.failIf(self.socket.send(packCommand(-2,0,0x30,0x0000,"1")) != 18,
                    "Command: Error writing to Socket")
        data = self.socket.recv(32)
        self.failIf(len(data) != 13, "Command: Return length incorrect")
        (error, length, data) = struct.unpack('!lB8s',data)
        self.failIf(error != 5006,
                    "Command: Incorrect Error Code (%d)" % error)

        

    def test04(self):
        '''
        Test to verify correct error handling when the manager is
        unavailable
        '''
        # Monitor
        self.failIf(self.socket.send(packMonitor(28,0,0x30,0x0000)) != 18,
                    "Monitor: Error writing to Socket")
        data = self.socket.recv(32)
        self.failIf(len(data) != 13, "Monitor: Return length incorrect")
        (error, length, data) = struct.unpack('!lB8s',data)
        self.failIf(error != 5005,
                    "Monitor: Incorrect Error Code (%d)" % error)

        # Command
        self.failIf(self.socket.send(packCommand(28,0,0x30,0x0000,"1")) != 18,
                    "Command: Error writing to Socket")
        data = self.socket.recv(32)
        self.failIf(len(data) != 13, "Command: Return length incorrect")
        (error, length, data) = struct.unpack('!lB8s',data)
        self.failIf(error != 5005,
                    "Command: Incorrect Error Code (%d)" % error)


    def test05(self):
        '''
        Test to ensure that the data structure is correctly formed:
        in particular make sure that byte 5 really specifies how many
        vailid bytes there are
        '''
        self.failIf(self.socket.send(packMonitor(1,0,0x30,0x30002)) != 18,
                    "Monitor: Error writing to Socket")
        data = self.socket.recv(32)
        self.failIf(len(data) != 13, "Monitor: Return length incorrect")
        (error, length, data) = struct.unpack('!lB8s',data)
        self.failIf(error != 0, "Monitor: Error getting data from socket")
        self.failIf(length != 4, "Monitor: Incorrect data length returned")


    def test06(self):
        '''
        Test to ensure that we are able to maintain a good socket connection
        through errors
        '''
        self.test04() #Unavalible Manager
        self.test03() #Illegal Manager
        self.test02() #Illegal Node
        self.test01() #Valid Node
        
    def test07(self):
        '''
        Test the ability to do a simple command
        '''
        self.failIf(self.socket.send(packCommand(1,0,0x30,0x31000,"1")) != 18,
                    "Error writing to Socket")
        data = self.socket.recv(32)
        self.failIf(len(data) != 13, "Return length incorrect")
        (error, length, data) = struct.unpack('!lB8s',data)
        self.failIf(error != 0, "Error processing Command (%d)" % error)

    def test08(self):
        '''
        Test that various lengths of commands can be sent
        '''
        data = '\x00\x00\x00\x00\x00\x00\x00\x00'
        for i in range(8,0,-1):
            data = data[:i]
            self.failIf(self.socket.send(packCommand(1,0,0x30,0x1000,
                                                     data))!= 18,
                        "Error writing to Socket (Command lenght: %d)"
                        % len(data))
            resp = self.socket.recv(32)
            self.failIf(len(resp) != 13, "Return length incorrect")
            (error, length, retndata) = struct.unpack('!lB8s',resp)
            self.failIf(error != 0,
                        "Error processing command (error %d, lenght %d"
                        % (error,i))
            
    def test09(self):
        '''
        Test correct function of the Reset command
        '''
        # This one should work
        self.failIf(self.socket.send(packReset(1,0))!= 18,
                    "Error writing to Socket")
        resp = self.socket.recv(32)
        self.failIf(len(resp) != 13, "Return length incorrect")
        (error, length, retndata) = struct.unpack('!lB8s',resp)
        self.failIf(error != 0,
                    "Error sending reset command (error %d)" % error)

        # This one should not
        self.failIf(self.socket.send(packReset(1,11))!= 18,
                    "Error writing to Socket")
        resp = self.socket.recv(32)
        self.failIf(len(resp) != 13, "Return length incorrect")
        (error, length, retndata) = struct.unpack('!lB8s',resp)
        self.failIf(error != 5009,
                    "Incorrect Error Code returned (error %d)" % error)
        
    def test10(self):
        '''
        Test the correct functioning of the list nodes command
        '''
        self.failIf(self.socket.send(packList(1,0))!= 18,
                    "Error writing to Socket")
        resp = self.socket.recv(13)
        self.failIf(len(resp) != 13,
                    "Return length incorrect (%d bytes)" % len(resp))
        (error, length, retndata) = struct.unpack('!lB8s',resp)
        self.failIf(error != 0,
                    "Error sending list nodes command (error %d)" % error)
        nodeList = {}
        for x in range(0,length):
            resp = self.socket.recv(12)
            self.failIf(len(resp) != 12,
                        "Return length incorrect (length=%d)" % len(resp))
            (node,sn) = struct.unpack('!LQ',resp)
            nodeList[node] = sn            

if __name__ == '__main__':
    unittest.main()
