// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2006
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA


#include <AcceptanceMountInterface.h> 
#include <ace/Semaphore.h> // for sem_t
#include <ambDefs.h> // For AMBERR defs
#include <acstimeEpochHelper.h> // for EpochHelper
#include <acstimeDurationHelper.h> // for DurationHelper
#include <TETimeUtil.h> // for TETimeUtil
#include <unistd.h> // for usleep
#include <deque> // for deque
#include <iomanip> // for std::setprecision and std::fixed
#include <iostream> // for std::cout and std::endl
#include <ace/Get_Opt.h>
#include <sstream>
#include <stdlib.h>
#include <map>

using std::string;
using std::map;

const static unsigned short int defaultChannel(2U);
const static unsigned short int defaultNodeId(0U);
const static unsigned int defaultReplyTolerance(5);
const static AmbRelativeAddr minRCA(0x1);
const static AmbRelativeAddr maxRCA(0x7fffff);

unsigned short int channel(defaultChannel);
unsigned short int nodeId(defaultNodeId);
unsigned int replyTolerance(defaultReplyTolerance);
std::vector< AmbRelativeAddr > monitorRCA;
std::vector< string > monitorMnemonic;
std::vector< string > controlRCA;
std::vector< string > controlMnemonic;
bool exitProgram(false);
map<string,AmbRelativeAddr> monitorMap;
map<string,AmbRelativeAddr> controlMap;

void usage(char *argv0)
{
    std::cout << std::endl
	      << argv0
	      << ": usage:"
	      << std::endl
	      << "Options:"
	      << std::endl
	      << "[--help|h]:"
	      << std::endl
	      << "     You are reading it!"
	      << std::endl
	      << "[--channel=|-c<CHANNEL>]:"
	      << std::endl
	      << "      ABM channel. Range: 0-5. Default = "
	      << defaultChannel
	      << "."
	      << std::endl
	      << "[--nodeId=|-n<NODEID>]:"
	      << std::endl
	      << "      NodeId to be used. Range: 0-512. Default = "
	      << defaultNodeId
	      << "."
	      << std::endl
	      << "[--monitor=|-m<mnemonic>]:"
	      << std::endl
	      << "      Mnemonic of monitor request. See ICD for valid values."
	      << std::endl
	      << "[--monitor_rca=|-r<RCA>]:"
	      << std::endl
	      << "      RCA of monitor request. Min. = 0x1; max. = "
	      << "0x7fffff."
	      << std::endl
	      << "[--control=|-p<mnemonic>,<value>[|,<value>]]:"
	      << std::endl
	      << "      Mnemonic of control request. See ICD for valid values."
	      << std::endl
	      << "[--control_rca=|-x<RCA>]:"
	      << std::endl
	      << "      RCA of control request. Min. = 0x1; max. = "
	      << "0x7fffff."
	      << std::endl;
};

void parseOptions(ACE_Get_Opt &options)
{
    AmbRelativeAddr rca;

    int c(0);
    while((c = options()) != EOF)
	{
        switch(c)
	    {
	    case 'c':
	    {
	    if(options.opt_arg() != 0)
		{
		std::istringstream value;
		value.str(options.opt_arg());
		short int val(0);
		value >> val;
		if((val < 0) || (val > 5))
		    {
		    std::cout << "The channel number has to be in the range of"
			      << "(0; 5)."
			      << std::endl;
		    exitProgram = true;
		    }
		else
		    {
		    channel = val;
		    std::cout << "channel     : "
			      << channel
			      << std::endl;
		    }
		}
	    }
	    break;
            case 'n':
            {
	    if(options.opt_arg() != 0)
                {
		std::istringstream value;
		value.str(options.opt_arg());
		unsigned int val(0U);
		value >> val;
		if((val < 0) || (val > 0x200))
		    {
		    std::cout << "The node ID has to be in the range of (0; 512)." 
			      << std::endl;
		    exitProgram = true;
		    }
		else
		    {
		    nodeId = val;
		    std::cout << "node        : "
			      << nodeId
			      << std::endl;
		    }
                }
            }
	    break;
	    case 'm':
	    {
	    if(options.opt_arg() != 0)
		{
		string value;
		value = options.opt_arg();
		std::cout << "Monitor     : "
			  << value
			  << std::endl;
		monitorMnemonic.push_back(value); 
		}
	    }
	    break;
	    case 'r':
	    {
	    if(options.opt_arg() != 0)
		{
		string value;
		value = options.opt_arg();
		AmbRelativeAddr monitor = strtol(value.c_str(),NULL,0);
		if((monitor < minRCA) || (monitor > maxRCA))
		    {
		    std::cout << "The RCA has to be in the range "
			      << "(0x1; 0x7fffff)! The value will not be used." << std::endl;
		    }
		else
		    {           
		    rca = monitor;
		    std::cout << "Monitor RCA : 0x"
			      << std::hex
			      << rca
			      << std::dec
			      << std::endl;
		    monitorRCA.push_back(rca); 
		    }
		}
	    }
	    break;
	    case 'p':
	    {
	    if(options.opt_arg() != 0)
		{
		string value;
		value = options.opt_arg();
		std::cout << "Control     : "
			  << value
			  << std::endl;
		controlMnemonic.push_back(value); 
		}
	    }
	    break;
	    case 'x':
	    {
	    if(options.opt_arg() != 0)
		{
		string value;
		value = options.opt_arg();
		AmbRelativeAddr control = strtol(value.substr(0,value.find(',')).c_str(),NULL,0);
		if((control < minRCA) || (control > maxRCA))
		    {
		    std::cout << "The RCA has to be in the range "
			      << "(0x1; 0x7fffff)! The value will not be used." << std::endl;
		    }
		else
		    {
		    std::cout << "Control RCA : 0x"
			      << std::hex
			      << control
			      << std::dec
			      << std::endl;
		    controlRCA.push_back(value);
		    }
		}
	    }
	    break;
            case '?':
            {
	    std::cout << "Option -"
		      << options.opt_opt()
		      << "requires an argument!"
		      << std::endl;
            }
	    // Fall through...
            case 'h':
            default:    // Display help.
            {
	    exitProgram = true;
            }
	    }
	}
};

void initializeMonitorMap()
{
    monitorMap["BUS_IDENTIFICATION"]            = BUS_IDENTIFICATION;
    monitorMap["ACU_MODE_RSP"]            = ACU_MODE_RSP;
    monitorMap["ACU_TRK_MODE_RSP"]        = ACU_TRK_MODE_RSP;
    monitorMap["AZ_POSN_RSP"]             = AZ_POSN_RSP;
    monitorMap["EL_POSN_RSP"]             = EL_POSN_RSP;
    monitorMap["GET_ACU_ERROR"]           = GET_ACU_ERROR;
    monitorMap["GET_AZ_TRAJ_CMD"]         = GET_AZ_TRAJ_CMD;
    monitorMap["GET_AZ_BRAKE"]            = GET_AZ_BRAKE;
    monitorMap["GET_AZ_AUX_MODE"]         = GET_AZ_AUX_MODE;
    monitorMap["GET_AZ_RATEFDBK_MODE"]         = GET_AZ_RATEFDBK_MODE;
    monitorMap["GET_AZ_ENC_STATUS"]       = GET_AZ_ENC_STATUS;
    monitorMap["GET_AZ_ENC"]              = GET_AZ_ENC;
    monitorMap["GET_AZ_MOTOR_CURRENTS"]   = GET_AZ_MOTOR_CURRENTS;
    monitorMap["GET_AZ_MOTOR_TEMPS"]      = GET_AZ_MOTOR_TEMPS;
    monitorMap["GET_AZ_MOTOR_TORQUE"]     = GET_AZ_MOTOR_TORQUE;
    monitorMap["GET_AZ_SERVO_COEFF_0"]    = GET_AZ_SERVO_COEFF_0;
    monitorMap["GET_AZ_SERVO_COEFF_1"]    = GET_AZ_SERVO_COEFF_1;
    monitorMap["GET_AZ_SERVO_COEFF_2"]    = GET_AZ_SERVO_COEFF_2;
    monitorMap["GET_AZ_SERVO_COEFF_3"]    = GET_AZ_SERVO_COEFF_3;
    monitorMap["GET_AZ_SERVO_COEFF_4"]    = GET_AZ_SERVO_COEFF_4;
    monitorMap["GET_AZ_SERVO_COEFF_5"]    = GET_AZ_SERVO_COEFF_5;
    monitorMap["GET_AZ_SERVO_COEFF_6"]    = GET_AZ_SERVO_COEFF_6;
    monitorMap["GET_AZ_SERVO_COEFF_7"]    = GET_AZ_SERVO_COEFF_7;
    monitorMap["GET_AZ_SERVO_COEFF_8"]    = GET_AZ_SERVO_COEFF_8;
    monitorMap["GET_AZ_SERVO_COEFF_9"]    = GET_AZ_SERVO_COEFF_9;
    monitorMap["GET_AZ_SERVO_COEFF_10"]    = GET_AZ_SERVO_COEFF_10;
    monitorMap["GET_AZ_SERVO_COEFF_11"]    = GET_AZ_SERVO_COEFF_11;
    monitorMap["GET_AZ_SERVO_COEFF_12"]    = GET_AZ_SERVO_COEFF_12;
    monitorMap["GET_AZ_SERVO_COEFF_13"]    = GET_AZ_SERVO_COEFF_13;
    monitorMap["GET_AZ_SERVO_COEFF_14"]    = GET_AZ_SERVO_COEFF_14;
    monitorMap["GET_AZ_SERVO_COEFF_15"]    = GET_AZ_SERVO_COEFF_15;
    monitorMap["GET_AZ_STATUS"]           = GET_AZ_STATUS;
    monitorMap["GET_AZ_STATUS_2"]           = GET_AZ_STATUS_2;
    monitorMap["GET_AZ_ENCODER_OFFSET"]   = GET_AZ_ENCODER_OFFSET;
    monitorMap["GET_CAN_ERROR"]           = GET_CAN_ERROR;
    monitorMap["GET_EL_TRAJ_CMD"]         = GET_EL_TRAJ_CMD;
    monitorMap["GET_EL_BRAKE"]            = GET_EL_BRAKE;
    monitorMap["GET_EL_AUX_MODE"]         = GET_EL_AUX_MODE;
    monitorMap["GET_EL_RATEFDBK_MODE"]         = GET_EL_RATEFDBK_MODE;
    monitorMap["GET_EL_ENC_STATUS"]       = GET_EL_ENC_STATUS;
    monitorMap["GET_EL_ENC"]              = GET_EL_ENC;
    monitorMap["GET_EL_MOTOR_CURRENTS"]   = GET_EL_MOTOR_CURRENTS;
    monitorMap["GET_EL_MOTOR_TEMPS"]      = GET_EL_MOTOR_TEMPS;
    monitorMap["GET_EL_MOTOR_TORQUE"]     = GET_EL_MOTOR_TORQUE;
    monitorMap["GET_EL_SERVO_COEFF_0"]    = GET_EL_SERVO_COEFF_0;
    monitorMap["GET_EL_SERVO_COEFF_1"]    = GET_EL_SERVO_COEFF_1;
    monitorMap["GET_EL_SERVO_COEFF_2"]    = GET_EL_SERVO_COEFF_2;
    monitorMap["GET_EL_SERVO_COEFF_3"]    = GET_EL_SERVO_COEFF_3;
    monitorMap["GET_EL_SERVO_COEFF_4"]    = GET_EL_SERVO_COEFF_4;
    monitorMap["GET_EL_SERVO_COEFF_5"]    = GET_EL_SERVO_COEFF_5;
    monitorMap["GET_EL_SERVO_COEFF_6"]    = GET_EL_SERVO_COEFF_6;
    monitorMap["GET_EL_SERVO_COEFF_7"]    = GET_EL_SERVO_COEFF_7;
    monitorMap["GET_EL_SERVO_COEFF_8"]    = GET_EL_SERVO_COEFF_8;
    monitorMap["GET_EL_SERVO_COEFF_9"]    = GET_EL_SERVO_COEFF_9;
    monitorMap["GET_EL_SERVO_COEFF_10"]    = GET_EL_SERVO_COEFF_10;
    monitorMap["GET_EL_SERVO_COEFF_11"]    = GET_EL_SERVO_COEFF_11;
    monitorMap["GET_EL_SERVO_COEFF_12"]    = GET_EL_SERVO_COEFF_12;
    monitorMap["GET_EL_SERVO_COEFF_13"]    = GET_EL_SERVO_COEFF_13;
    monitorMap["GET_EL_SERVO_COEFF_14"]    = GET_EL_SERVO_COEFF_14;
    monitorMap["GET_EL_SERVO_COEFF_15"]    = GET_EL_SERVO_COEFF_15;
    monitorMap["GET_EL_STATUS"]           = GET_EL_STATUS;
    monitorMap["GET_EL_STATUS_2"]           = GET_EL_STATUS_2;
    monitorMap["GET_EL_ENCODER_OFFSET"]   = GET_EL_ENCODER_OFFSET;
    monitorMap["GET_SYSTEM_ID"]           = GET_SYSTEM_ID;
    monitorMap["GET_IDLE_STOW_TIME"]      = GET_IDLE_STOW_TIME;
    monitorMap["GET_IP_ADDRESS"]          = GET_IP_ADDRESS;
    monitorMap["GET_IP_GATEWAY"]          = GET_IP_GATEWAY;
    monitorMap["GET_NUM_TRANS"]           = GET_NUM_TRANS;
    monitorMap["GET_SYSTEM_STATUS"]       = GET_SYSTEM_STATUS;
    monitorMap["GET_SYSTEM_STATUS_2"]       = GET_SYSTEM_STATUS_2;
    monitorMap["GET_PT_MODEL_COEFF_0"]    = GET_PT_MODEL_COEFF_0;
    monitorMap["GET_PT_MODEL_COEFF_1"]    = GET_PT_MODEL_COEFF_1;
    monitorMap["GET_PT_MODEL_COEFF_2"]    = GET_PT_MODEL_COEFF_2;
    monitorMap["GET_PT_MODEL_COEFF_3"]    = GET_PT_MODEL_COEFF_3;
    monitorMap["GET_PT_MODEL_COEFF_4"]    = GET_PT_MODEL_COEFF_4;
    monitorMap["GET_PT_MODEL_COEFF_5"]    = GET_PT_MODEL_COEFF_5;
    monitorMap["GET_PT_MODEL_COEFF_6"]    = GET_PT_MODEL_COEFF_6;
    monitorMap["GET_PT_MODEL_COEFF_7"]    = GET_PT_MODEL_COEFF_7;
    monitorMap["GET_PT_MODEL_COEFF_8"]    = GET_PT_MODEL_COEFF_8;
    monitorMap["GET_PT_MODEL_COEFF_9"]    = GET_PT_MODEL_COEFF_9;
    monitorMap["GET_PT_MODEL_COEFF_10"]   = GET_PT_MODEL_COEFF_10;
    monitorMap["GET_PT_MODEL_COEFF_11"]   = GET_PT_MODEL_COEFF_11;
    monitorMap["GET_PT_MODEL_COEFF_12"]   = GET_PT_MODEL_COEFF_12;
    monitorMap["GET_PT_MODEL_COEFF_13"]   = GET_PT_MODEL_COEFF_13;
    monitorMap["GET_PT_MODEL_COEFF_14"]   = GET_PT_MODEL_COEFF_14;
    monitorMap["GET_PT_MODEL_COEFF_15"]   = GET_PT_MODEL_COEFF_15;
    monitorMap["GET_PT_MODEL_COEFF_16"]   = GET_PT_MODEL_COEFF_16;
    monitorMap["GET_PT_MODEL_COEFF_17"]   = GET_PT_MODEL_COEFF_17;
    monitorMap["GET_PT_MODEL_COEFF_18"]   = GET_PT_MODEL_COEFF_18;
    monitorMap["GET_PT_MODEL_COEFF_19"]   = GET_PT_MODEL_COEFF_19;
    monitorMap["GET_PT_MODEL_COEFF_20"]   = GET_PT_MODEL_COEFF_20;
    monitorMap["GET_PT_MODEL_COEFF_21"]   = GET_PT_MODEL_COEFF_21;
    monitorMap["GET_PT_MODEL_COEFF_22"]   = GET_PT_MODEL_COEFF_22;
    monitorMap["GET_PT_MODEL_COEFF_23"]   = GET_PT_MODEL_COEFF_23;
    monitorMap["GET_PT_MODEL_COEFF_24"]   = GET_PT_MODEL_COEFF_24;
    monitorMap["GET_PT_MODEL_COEFF_25"]   = GET_PT_MODEL_COEFF_25;
    monitorMap["GET_PT_MODEL_COEFF_26"]   = GET_PT_MODEL_COEFF_26;
    monitorMap["GET_PT_MODEL_COEFF_27"]   = GET_PT_MODEL_COEFF_27;
    monitorMap["GET_PT_MODEL_COEFF_28"]   = GET_PT_MODEL_COEFF_28;
    monitorMap["GET_PT_MODEL_COEFF_29"]   = GET_PT_MODEL_COEFF_29;
    monitorMap["GET_PT_MODEL_COEFF_30"]   = GET_PT_MODEL_COEFF_30;
    monitorMap["GET_PT_MODEL_COEFF_31"]   = GET_PT_MODEL_COEFF_31;
    monitorMap["GET_SHUTTER"]             = GET_SHUTTER;
    monitorMap["GET_STOW_PIN"]            = GET_STOW_PIN;
    monitorMap["SUBREF_MODE_RSP"]         = SUBREF_MODE_RSP;
    monitorMap["GET_SUBREF_ABS_POSN"]     = GET_SUBREF_ABS_POSN;
    monitorMap["GET_SUBREF_DELTA_POSN"]   = GET_SUBREF_DELTA_POSN;
    monitorMap["GET_SUBREF_LIMITS"]       = GET_SUBREF_LIMITS;
    monitorMap["GET_SUBREF_ROTATION"]     = GET_SUBREF_ROTATION;
    monitorMap["GET_SUBREF_STATUS"]       = GET_SUBREF_STATUS;
    monitorMap["GET_SUBREF_PT_COEFF_0"]   = GET_SUBREF_PT_COEFF_0;
    monitorMap["GET_SUBREF_PT_COEFF_1"]   = GET_SUBREF_PT_COEFF_1;
    monitorMap["GET_SUBREF_PT_COEFF_2"]   = GET_SUBREF_PT_COEFF_2;
    monitorMap["GET_SUBREF_PT_COEFF_3"]   = GET_SUBREF_PT_COEFF_3;
    monitorMap["GET_SUBREF_PT_COEFF_4"]   = GET_SUBREF_PT_COEFF_4;
    monitorMap["GET_SUBREF_PT_COEFF_5"]   = GET_SUBREF_PT_COEFF_5;
    monitorMap["GET_SUBREF_PT_COEFF_6"]   = GET_SUBREF_PT_COEFF_6;
    monitorMap["GET_SUBREF_PT_COEFF_7"]   = GET_SUBREF_PT_COEFF_7;
    monitorMap["GET_SUBREF_PT_COEFF_8"]   = GET_SUBREF_PT_COEFF_8;
    monitorMap["GET_SUBREF_PT_COEFF_9"]   = GET_SUBREF_PT_COEFF_9;
    monitorMap["GET_SUBREF_PT_COEFF_10"]   = GET_SUBREF_PT_COEFF_10;
    monitorMap["GET_SUBREF_PT_COEFF_11"]   = GET_SUBREF_PT_COEFF_11;
    monitorMap["GET_SUBREF_PT_COEFF_12"]   = GET_SUBREF_PT_COEFF_12;
    monitorMap["GET_SUBREF_PT_COEFF_13"]   = GET_SUBREF_PT_COEFF_13;
    monitorMap["GET_SUBREF_PT_COEFF_14"]   = GET_SUBREF_PT_COEFF_14;
    monitorMap["GET_SUBREF_PT_COEFF_15"]   = GET_SUBREF_PT_COEFF_15;
    monitorMap["GET_SUBREF_PT_COEFF_16"]   = GET_SUBREF_PT_COEFF_16;
    monitorMap["GET_SUBREF_PT_COEFF_17"]   = GET_SUBREF_PT_COEFF_17;
    monitorMap["GET_SUBREF_PT_COEFF_18"]   = GET_SUBREF_PT_COEFF_18;
    monitorMap["GET_SUBREF_PT_COEFF_19"]   = GET_SUBREF_PT_COEFF_19;
    monitorMap["GET_SUBREF_PT_COEFF_20"]   = GET_SUBREF_PT_COEFF_20;
    monitorMap["GET_SUBREF_PT_COEFF_21"]   = GET_SUBREF_PT_COEFF_21;
    monitorMap["GET_SUBREF_PT_COEFF_22"]   = GET_SUBREF_PT_COEFF_22;
    monitorMap["GET_SUBREF_PT_COEFF_23"]   = GET_SUBREF_PT_COEFF_23;
    monitorMap["GET_SUBREF_PT_COEFF_24"]   = GET_SUBREF_PT_COEFF_24;
    monitorMap["GET_SUBREF_PT_COEFF_25"]   = GET_SUBREF_PT_COEFF_25;
    monitorMap["GET_SUBREF_PT_COEFF_26"]   = GET_SUBREF_PT_COEFF_26;
    monitorMap["GET_SUBREF_PT_COEFF_27"]   = GET_SUBREF_PT_COEFF_27;
    monitorMap["GET_SUBREF_PT_COEFF_28"]   = GET_SUBREF_PT_COEFF_28;
    monitorMap["GET_SUBREF_PT_COEFF_29"]   = GET_SUBREF_PT_COEFF_29;
    monitorMap["GET_SUBREF_PT_COEFF_30"]   = GET_SUBREF_PT_COEFF_30;
    monitorMap["GET_SUBREF_PT_COEFF_31"]   = GET_SUBREF_PT_COEFF_31;
    monitorMap["GET_SUBREF_ENCODER_POSN_1"]   = GET_SUBREF_ENCODER_POSN_1;
    monitorMap["GET_SUBREF_ENCODER_POSN_2"]   = GET_SUBREF_ENCODER_POSN_2;
    monitorMap["GET_METR_MODE"]           = GET_METR_MODE;
    monitorMap["GET_METR_EQUIP_STATUS"]   = GET_METR_EQUIP_STATUS;
    monitorMap["GET_METR_DISPL_0"]        = GET_METR_DISPL_0;
    monitorMap["GET_METR_DISPL_1"]        = GET_METR_DISPL_1;
    monitorMap["GET_METR_DISPL_2"]        = GET_METR_DISPL_2;
    monitorMap["GET_METR_DISPL_3"]        = GET_METR_DISPL_3;
    monitorMap["GET_METR_DISPL_4"]        = GET_METR_DISPL_4;
    monitorMap["GET_METR_DISPL_5"]        = GET_METR_DISPL_5;
    monitorMap["GET_METR_DISPL_6"]        = GET_METR_DISPL_6;
    monitorMap["GET_METR_DISPL_7"]        = GET_METR_DISPL_7;
    monitorMap["GET_METR_DISPL_8"]        = GET_METR_DISPL_8;
    monitorMap["GET_METR_DISPL_9"]        = GET_METR_DISPL_9;
    monitorMap["GET_METR_DISPL_10"]        = GET_METR_DISPL_10;
    monitorMap["GET_METR_DISPL_11"]        = GET_METR_DISPL_11;
    monitorMap["GET_METR_DISPL_12"]        = GET_METR_DISPL_12;
    monitorMap["GET_METR_DISPL_13"]        = GET_METR_DISPL_13;
    monitorMap["GET_METR_DISPL_14"]        = GET_METR_DISPL_14;
    monitorMap["GET_METR_DISPL_15"]        = GET_METR_DISPL_15;
    monitorMap["GET_METR_DISPL_16"]        = GET_METR_DISPL_16;
    monitorMap["GET_METR_DISPL_17"]        = GET_METR_DISPL_17;
    monitorMap["GET_METR_DISPL_18"]        = GET_METR_DISPL_18;
    monitorMap["GET_METR_DISPL_19"]        = GET_METR_DISPL_19;
    monitorMap["GET_METR_DISPL_20"]        = GET_METR_DISPL_20;
    monitorMap["GET_METR_DISPL_21"]        = GET_METR_DISPL_21;
    monitorMap["GET_METR_DISPL_22"]        = GET_METR_DISPL_22;
    monitorMap["GET_METR_DISPL_23"]        = GET_METR_DISPL_23;
    monitorMap["GET_METR_TEMPS_0"]        = GET_METR_TEMPS_0;
    monitorMap["GET_METR_TEMPS_1"]        = GET_METR_TEMPS_1;
    monitorMap["GET_METR_TEMPS_2"]        = GET_METR_TEMPS_2;
    monitorMap["GET_METR_TEMPS_3"]        = GET_METR_TEMPS_3;
    monitorMap["GET_METR_TEMPS_4"]        = GET_METR_TEMPS_4;
    monitorMap["GET_METR_TEMPS_5"]        = GET_METR_TEMPS_5;
    monitorMap["GET_METR_TEMPS_6"]        = GET_METR_TEMPS_6;
    monitorMap["GET_METR_TEMPS_7"]        = GET_METR_TEMPS_7;
    monitorMap["GET_METR_TEMPS_8"]        = GET_METR_TEMPS_8;
    monitorMap["GET_METR_TEMPS_9"]        = GET_METR_TEMPS_9;
    monitorMap["GET_METR_TEMPS_10"]       = GET_METR_TEMPS_10;
    monitorMap["GET_METR_TEMPS_11"]       = GET_METR_TEMPS_11;
    monitorMap["GET_METR_TEMPS_12"]       = GET_METR_TEMPS_12;
    monitorMap["GET_METR_TEMPS_13"]       = GET_METR_TEMPS_13;
    monitorMap["GET_METR_TEMPS_14"]       = GET_METR_TEMPS_14;
    monitorMap["GET_METR_TEMPS_15"]       = GET_METR_TEMPS_15;
    monitorMap["GET_METR_TEMPS_16"]       = GET_METR_TEMPS_16;
    monitorMap["GET_METR_TEMPS_17"]       = GET_METR_TEMPS_17;
    monitorMap["GET_METR_TEMPS_18"]       = GET_METR_TEMPS_18;
    monitorMap["GET_METR_TEMPS_19"]       = GET_METR_TEMPS_19;
    monitorMap["GET_METR_TEMPS_20"]       = GET_METR_TEMPS_20;
    monitorMap["GET_METR_TEMPS_21"]       = GET_METR_TEMPS_21;
    monitorMap["GET_METR_TEMPS_22"]       = GET_METR_TEMPS_22;
    monitorMap["GET_METR_TEMPS_23"]       = GET_METR_TEMPS_23;
    monitorMap["GET_METR_TEMPS_24"]       = GET_METR_TEMPS_24;
    monitorMap["GET_METR_TEMPS_25"]       = GET_METR_TEMPS_25;
    monitorMap["GET_METR_TEMPS_26"]       = GET_METR_TEMPS_26;
    monitorMap["GET_METR_TEMPS_27"]       = GET_METR_TEMPS_27;
    monitorMap["GET_METR_TEMPS_28"]       = GET_METR_TEMPS_28;
    monitorMap["GET_METR_TEMPS_29"]       = GET_METR_TEMPS_29;
    monitorMap["GET_METR_TEMPS_30"]       = GET_METR_TEMPS_30;
    monitorMap["GET_METR_TEMPS_31"]       = GET_METR_TEMPS_31;
    monitorMap["GET_METR_TEMPS_32"]       = GET_METR_TEMPS_32;
    monitorMap["GET_METR_TEMPS_33"]       = GET_METR_TEMPS_33;
    monitorMap["GET_METR_TEMPS_34"]       = GET_METR_TEMPS_34;
    monitorMap["GET_METR_TEMPS_35"]       = GET_METR_TEMPS_35;
    monitorMap["GET_METR_TEMPS_36"]       = GET_METR_TEMPS_36;
    monitorMap["GET_METR_TEMPS_37"]       = GET_METR_TEMPS_37;
    monitorMap["GET_METR_TEMPS_38"]       = GET_METR_TEMPS_38;
    monitorMap["GET_METR_TEMPS_39"]       = GET_METR_TEMPS_39;
    monitorMap["GET_METR_TEMPS_40"]       = GET_METR_TEMPS_40;
    monitorMap["GET_METR_TEMPS_41"]       = GET_METR_TEMPS_41;
    monitorMap["GET_METR_TEMPS_42"]       = GET_METR_TEMPS_42;
    monitorMap["GET_METR_TEMPS_43"]       = GET_METR_TEMPS_43;
    monitorMap["GET_METR_TEMPS_44"]       = GET_METR_TEMPS_44;
    monitorMap["GET_METR_TEMPS_45"]       = GET_METR_TEMPS_45;
    monitorMap["GET_METR_TEMPS_46"]       = GET_METR_TEMPS_46;
    monitorMap["GET_METR_TEMPS_47"]       = GET_METR_TEMPS_47;
    monitorMap["GET_METR_TEMPS_48"]       = GET_METR_TEMPS_48;
    monitorMap["GET_METR_TEMPS_49"]       = GET_METR_TEMPS_49;
    monitorMap["GET_METR_TEMPS_50"]       = GET_METR_TEMPS_50;
    monitorMap["GET_METR_TEMPS_51"]       = GET_METR_TEMPS_51;
    monitorMap["GET_METR_TEMPS_52"]       = GET_METR_TEMPS_52;
    monitorMap["GET_METR_TEMPS_53"]       = GET_METR_TEMPS_53;
    monitorMap["GET_METR_TEMPS_54"]       = GET_METR_TEMPS_54;
    monitorMap["GET_METR_TEMPS_55"]       = GET_METR_TEMPS_55;
    monitorMap["GET_METR_TEMPS_56"]       = GET_METR_TEMPS_56;
    monitorMap["GET_METR_TEMPS_57"]       = GET_METR_TEMPS_57;
    monitorMap["GET_METR_TEMPS_58"]       = GET_METR_TEMPS_58;
    monitorMap["GET_METR_TEMPS_59"]       = GET_METR_TEMPS_59;
    monitorMap["GET_METR_TEMPS_60"]       = GET_METR_TEMPS_60;
    monitorMap["GET_METR_TEMPS_61"]       = GET_METR_TEMPS_61;
    monitorMap["GET_METR_TEMPS_62"]       = GET_METR_TEMPS_62;
    monitorMap["GET_METR_TEMPS_63"]       = GET_METR_TEMPS_63;
    monitorMap["GET_METR_TEMPS_64"]       = GET_METR_TEMPS_64;
    monitorMap["GET_METR_TEMPS_65"]       = GET_METR_TEMPS_65;
    monitorMap["GET_METR_TEMPS_66"]       = GET_METR_TEMPS_66;
    monitorMap["GET_METR_TEMPS_67"]       = GET_METR_TEMPS_67;
    monitorMap["GET_METR_TEMPS_68"]       = GET_METR_TEMPS_68;
    monitorMap["GET_METR_TEMPS_69"]       = GET_METR_TEMPS_69;
    monitorMap["GET_METR_TEMPS_70"]       = GET_METR_TEMPS_70;
    monitorMap["GET_METR_TEMPS_71"]       = GET_METR_TEMPS_71;
    monitorMap["GET_METR_TEMPS_72"]       = GET_METR_TEMPS_72;
    monitorMap["GET_METR_TEMPS_73"]       = GET_METR_TEMPS_73;
    monitorMap["GET_METR_TEMPS_74"]       = GET_METR_TEMPS_74;
    monitorMap["GET_METR_TEMPS_75"]       = GET_METR_TEMPS_75;
    monitorMap["GET_METR_TEMPS_76"]       = GET_METR_TEMPS_76;
    monitorMap["GET_METR_TEMPS_77"]       = GET_METR_TEMPS_77;
    monitorMap["GET_METR_TEMPS_78"]       = GET_METR_TEMPS_78;
    monitorMap["GET_METR_TEMPS_79"]       = GET_METR_TEMPS_79;
    monitorMap["GET_METR_TILT_0"]         = GET_METR_TILT_0;
    monitorMap["GET_METR_TILT_1"]         = GET_METR_TILT_1;
    monitorMap["GET_METR_TILT_2"]         = GET_METR_TILT_2;
    monitorMap["GET_METR_TILT_3"]         = GET_METR_TILT_3;
    monitorMap["GET_METR_TILT_4"]         = GET_METR_TILT_4;
    monitorMap["GET_METR_DELTAS"]         = GET_METR_DELTAS;
    monitorMap["GET_METR_COEFF_0"]        = GET_METR_COEFF_0;
    monitorMap["GET_METR_COEFF_1"]        = GET_METR_COEFF_1;
    monitorMap["GET_METR_COEFF_2"]        = GET_METR_COEFF_2;
    monitorMap["GET_METR_COEFF_3"]        = GET_METR_COEFF_3;
    monitorMap["GET_METR_COEFF_4"]        = GET_METR_COEFF_4;
    monitorMap["GET_METR_COEFF_5"]        = GET_METR_COEFF_5;
    monitorMap["GET_METR_COEFF_6"]        = GET_METR_COEFF_6;
    monitorMap["GET_METR_COEFF_7"]        = GET_METR_COEFF_7;
    monitorMap["GET_METR_COEFF_8"]        = GET_METR_COEFF_8;
    monitorMap["GET_METR_COEFF_9"]        = GET_METR_COEFF_9;
    monitorMap["GET_METR_COEFF_10"]       = GET_METR_COEFF_10;
    monitorMap["GET_METR_COEFF_11"]       = GET_METR_COEFF_11;
    monitorMap["GET_METR_COEFF_12"]       = GET_METR_COEFF_12;
    monitorMap["GET_METR_COEFF_13"]       = GET_METR_COEFF_13;
    monitorMap["GET_METR_COEFF_14"]       = GET_METR_COEFF_14;
    monitorMap["GET_METR_COEFF_15"]       = GET_METR_COEFF_15;
    monitorMap["GET_METR_COEFF_16"]       = GET_METR_COEFF_16;
    monitorMap["GET_METR_COEFF_17"]       = GET_METR_COEFF_17;
    monitorMap["GET_METR_COEFF_18"]       = GET_METR_COEFF_18;
    monitorMap["GET_METR_COEFF_19"]       = GET_METR_COEFF_19;
    monitorMap["GET_METR_COEFF_20"]       = GET_METR_COEFF_20;
    monitorMap["GET_METR_COEFF_21"]       = GET_METR_COEFF_21;
    monitorMap["GET_METR_COEFF_22"]       = GET_METR_COEFF_22;
    monitorMap["GET_METR_COEFF_23"]       = GET_METR_COEFF_23;
    monitorMap["GET_METR_COEFF_24"]       = GET_METR_COEFF_24;
    monitorMap["GET_METR_COEFF_25"]       = GET_METR_COEFF_25;
    monitorMap["GET_METR_COEFF_26"]       = GET_METR_COEFF_26;
    monitorMap["GET_METR_COEFF_27"]       = GET_METR_COEFF_27;
    monitorMap["GET_METR_COEFF_28"]       = GET_METR_COEFF_28;
    monitorMap["GET_METR_COEFF_29"]       = GET_METR_COEFF_29;
    monitorMap["GET_METR_COEFF_30"]       = GET_METR_COEFF_30;
    monitorMap["GET_METR_COEFF_31"]       = GET_METR_COEFF_31;
    monitorMap["GET_METR_DELTAPATH"]       = GET_METR_DELTAPATH;
    monitorMap["GET_POWER_STATUS"]        = GET_POWER_STATUS;
    monitorMap["GET_AC_STATUS"]           = GET_AC_STATUS;
    monitorMap["GET_FAN_STATUS"]           = GET_FAN_STATUS;
    monitorMap["GET_UPS_OUTPUT_VOLTS_1"]    = GET_UPS_OUTPUT_VOLTS_1;
    monitorMap["GET_UPS_OUTPUT_VOLTS_2"]    = GET_UPS_OUTPUT_VOLTS_2;
    monitorMap["GET_UPS_OUTPUT_CURRENT_1"]    = GET_UPS_OUTPUT_CURRENT_1;
    monitorMap["GET_UPS_OUTPUT_CURRENT_2"]    = GET_UPS_OUTPUT_CURRENT_2;
    monitorMap["GET_UPS_BATTERY_OUTPUT"]  = GET_UPS_BATTERY_OUTPUT;
//    monitorMap["GET_UPS_BATTERY_STATUS"]  = GET_UPS_BATTERY_STATUS;
    monitorMap["GET_UPS_BYPASS_VOLTS"]    = GET_UPS_BYPASS_VOLTS;
//    monitorMap["GET_UPS_FREQS"]           = GET_UPS_FREQS;
    monitorMap["GET_UPS_ALARMS"]          = GET_UPS_ALARMS;
    monitorMap["GET_UPS_INVERTER_VOLTS"]  = GET_UPS_INVERTER_VOLTS;
    monitorMap["GET_ANTENNA_TEMPS"]       = GET_ANTENNA_TEMPS;
    monitorMap["GET_SW_REV_LEVEL"]        = GET_SW_REV_LEVEL;
    monitorMap["GET_ALS_STATUS"]          = GET_ALS_STATUS;
    monitorMap["SELFTEST_RSP"]            = SELFTEST_RSP;
    monitorMap["SELFTEST_ERR"]            = SELFTEST_ERR;
}
void initializeControlMap()
{
    controlMap["ACU_MODE_CMD"]          = ACU_MODE_CMD;
    controlMap["ACU_TRK_MODE_CMD"]      = ACU_TRK_MODE_CMD;
    controlMap["AZ_TRAJ_CMD"]           = AZ_TRAJ_CMD;
    controlMap["EL_TRAJ_CMD"]           = EL_TRAJ_CMD;
    controlMap["CLEAR_FAULT_CMD"]       = CLEAR_FAULT_CMD;
    controlMap["RESET_ACU_CMD"]         = RESET_ACU_CMD;
    controlMap["SET_AZ_BRAKE"]          = SET_AZ_BRAKE;
    controlMap["SET_AZ_AUX_MODE"]       = SET_AZ_AUX_MODE;
    controlMap["SET_AZ_RATEFDBK_MODE"]       = SET_AZ_RATEFDBK_MODE;
    controlMap["SET_AZ_SERVO_COEFF_0"]  = SET_AZ_SERVO_COEFF_0;
    controlMap["SET_AZ_SERVO_COEFF_1"]  = SET_AZ_SERVO_COEFF_1;
    controlMap["SET_AZ_SERVO_COEFF_2"]  = SET_AZ_SERVO_COEFF_2;
    controlMap["SET_AZ_SERVO_COEFF_3"]  = SET_AZ_SERVO_COEFF_3;
    controlMap["SET_AZ_SERVO_COEFF_4"]  = SET_AZ_SERVO_COEFF_4;
    controlMap["SET_AZ_SERVO_COEFF_5"]  = SET_AZ_SERVO_COEFF_5;
    controlMap["SET_AZ_SERVO_COEFF_6"]  = SET_AZ_SERVO_COEFF_6;
    controlMap["SET_AZ_SERVO_COEFF_7"]  = SET_AZ_SERVO_COEFF_7;
    controlMap["SET_AZ_SERVO_COEFF_8"]  = SET_AZ_SERVO_COEFF_8;
    controlMap["SET_AZ_SERVO_COEFF_9"]  = SET_AZ_SERVO_COEFF_9;
    controlMap["SET_AZ_SERVO_COEFF_10"]  = SET_AZ_SERVO_COEFF_10;
    controlMap["SET_AZ_SERVO_COEFF_11"]  = SET_AZ_SERVO_COEFF_11;
    controlMap["SET_AZ_SERVO_COEFF_12"]  = SET_AZ_SERVO_COEFF_12;
    controlMap["SET_AZ_SERVO_COEFF_13"]  = SET_AZ_SERVO_COEFF_13;
    controlMap["SET_AZ_SERVO_COEFF_14"]  = SET_AZ_SERVO_COEFF_14;
    controlMap["SET_AZ_SERVO_COEFF_15"]  = SET_AZ_SERVO_COEFF_15;
    controlMap["SET_AZ_SERVO_DEFAULT"]  = SET_AZ_SERVO_DEFAULT;
    controlMap["SET_EL_BRAKE"]          = SET_EL_BRAKE;
    controlMap["SET_EL_AUX_MODE"]       = SET_EL_AUX_MODE;
    controlMap["SET_EL_RATEFDBK_MODE"]       = SET_EL_RATEFDBK_MODE;
    controlMap["SET_EL_SERVO_COEFF_0"]  = SET_EL_SERVO_COEFF_0;
    controlMap["SET_EL_SERVO_COEFF_1"]  = SET_EL_SERVO_COEFF_1;
    controlMap["SET_EL_SERVO_COEFF_2"]  = SET_EL_SERVO_COEFF_2;
    controlMap["SET_EL_SERVO_COEFF_3"]  = SET_EL_SERVO_COEFF_3;
    controlMap["SET_EL_SERVO_COEFF_4"]  = SET_EL_SERVO_COEFF_4;
    controlMap["SET_EL_SERVO_COEFF_5"]  = SET_EL_SERVO_COEFF_5;
    controlMap["SET_EL_SERVO_COEFF_6"]  = SET_EL_SERVO_COEFF_6;
    controlMap["SET_EL_SERVO_COEFF_7"]  = SET_EL_SERVO_COEFF_7;
    controlMap["SET_EL_SERVO_COEFF_8"]  = SET_EL_SERVO_COEFF_8;
    controlMap["SET_EL_SERVO_COEFF_9"]  = SET_EL_SERVO_COEFF_9;
    controlMap["SET_EL_SERVO_COEFF_10"]  = SET_EL_SERVO_COEFF_10;
    controlMap["SET_EL_SERVO_COEFF_11"]  = SET_EL_SERVO_COEFF_11;
    controlMap["SET_EL_SERVO_COEFF_12"]  = SET_EL_SERVO_COEFF_12;
    controlMap["SET_EL_SERVO_COEFF_13"]  = SET_EL_SERVO_COEFF_13;
    controlMap["SET_EL_SERVO_COEFF_14"]  = SET_EL_SERVO_COEFF_14;
    controlMap["SET_EL_SERVO_COEFF_15"]  = SET_EL_SERVO_COEFF_15;
    controlMap["SET_EL_SERVO_DEFAULT"]  = SET_EL_SERVO_DEFAULT;
    controlMap["SET_IDLE_STOW_TIME"]    = SET_IDLE_STOW_TIME;
    controlMap["SET_IP_ADDRESS"]        = SET_IP_ADDRESS;
    controlMap["SET_IP_GATEWAY"]        = SET_IP_GATEWAY;
    controlMap["SET_PT_MODEL_COEFF_0"]  = SET_PT_MODEL_COEFF_0;
    controlMap["SET_PT_MODEL_COEFF_1"]  = SET_PT_MODEL_COEFF_1;
    controlMap["SET_PT_MODEL_COEFF_2"]  = SET_PT_MODEL_COEFF_2;
    controlMap["SET_PT_MODEL_COEFF_3"]  = SET_PT_MODEL_COEFF_3;
    controlMap["SET_PT_MODEL_COEFF_4"]  = SET_PT_MODEL_COEFF_4;
    controlMap["SET_PT_MODEL_COEFF_5"]  = SET_PT_MODEL_COEFF_5;
    controlMap["SET_PT_MODEL_COEFF_6"]  = SET_PT_MODEL_COEFF_6;
    controlMap["SET_PT_MODEL_COEFF_7"]  = SET_PT_MODEL_COEFF_7;
    controlMap["SET_PT_MODEL_COEFF_8"]  = SET_PT_MODEL_COEFF_8;
    controlMap["SET_PT_MODEL_COEFF_9"]  = SET_PT_MODEL_COEFF_9;
    controlMap["SET_PT_MODEL_COEFF_10"] = SET_PT_MODEL_COEFF_10;
    controlMap["SET_PT_MODEL_COEFF_11"] = SET_PT_MODEL_COEFF_11;
    controlMap["SET_PT_MODEL_COEFF_12"] = SET_PT_MODEL_COEFF_12;
    controlMap["SET_PT_MODEL_COEFF_13"] = SET_PT_MODEL_COEFF_13;
    controlMap["SET_PT_MODEL_COEFF_14"] = SET_PT_MODEL_COEFF_14;
    controlMap["SET_PT_MODEL_COEFF_15"] = SET_PT_MODEL_COEFF_15;
    controlMap["SET_PT_MODEL_COEFF_16"] = SET_PT_MODEL_COEFF_16;
    controlMap["SET_PT_MODEL_COEFF_17"] = SET_PT_MODEL_COEFF_17;
    controlMap["SET_PT_MODEL_COEFF_18"] = SET_PT_MODEL_COEFF_18;
    controlMap["SET_PT_MODEL_COEFF_19"] = SET_PT_MODEL_COEFF_19;
    controlMap["SET_PT_MODEL_COEFF_20"] = SET_PT_MODEL_COEFF_20;
    controlMap["SET_PT_MODEL_COEFF_21"] = SET_PT_MODEL_COEFF_21;
    controlMap["SET_PT_MODEL_COEFF_22"] = SET_PT_MODEL_COEFF_22;
    controlMap["SET_PT_MODEL_COEFF_23"] = SET_PT_MODEL_COEFF_23;
    controlMap["SET_PT_MODEL_COEFF_24"] = SET_PT_MODEL_COEFF_24;
    controlMap["SET_PT_MODEL_COEFF_25"] = SET_PT_MODEL_COEFF_25;
    controlMap["SET_PT_MODEL_COEFF_26"] = SET_PT_MODEL_COEFF_26;
    controlMap["SET_PT_MODEL_COEFF_27"] = SET_PT_MODEL_COEFF_27;
    controlMap["SET_PT_MODEL_COEFF_28"] = SET_PT_MODEL_COEFF_28;
    controlMap["SET_PT_MODEL_COEFF_29"] = SET_PT_MODEL_COEFF_29;
    controlMap["SET_PT_MODEL_COEFF_30"] = SET_PT_MODEL_COEFF_30;
    controlMap["SET_PT_MODEL_COEFF_31"] = SET_PT_MODEL_COEFF_31;
    controlMap["SET_PT_DEFAULT"]        = SET_PT_DEFAULT;
    controlMap["SET_STOW_PIN"]          = SET_STOW_PIN;
    controlMap["SUBREF_MODE_CMD"]       = SUBREF_MODE_CMD;
    controlMap["SET_SUBREF_ABS_POSN"]   = SET_SUBREF_ABS_POSN;
    controlMap["SET_SUBREF_DELTA_POSN"] = SET_SUBREF_DELTA_POSN;
    controlMap["SUBREF_DELTA_ZERO_CMD"] = SUBREF_DELTA_ZERO_CMD;
    controlMap["SET_SUBREF_ROTATION"]   = SET_SUBREF_ROTATION;
    controlMap["SET_SUBREF_PT_COEFF_0"]  = SET_SUBREF_PT_COEFF_0;
    controlMap["SET_SUBREF_PT_COEFF_1"]  = SET_SUBREF_PT_COEFF_1;
    controlMap["SET_SUBREF_PT_COEFF_2"]  = SET_SUBREF_PT_COEFF_2;
    controlMap["SET_SUBREF_PT_COEFF_3"]  = SET_SUBREF_PT_COEFF_3;
    controlMap["SET_SUBREF_PT_COEFF_4"]  = SET_SUBREF_PT_COEFF_4;
    controlMap["SET_SUBREF_PT_COEFF_5"]  = SET_SUBREF_PT_COEFF_5;
    controlMap["SET_SUBREF_PT_COEFF_6"]  = SET_SUBREF_PT_COEFF_6;
    controlMap["SET_SUBREF_PT_COEFF_7"]  = SET_SUBREF_PT_COEFF_7;
    controlMap["SET_SUBREF_PT_COEFF_8"]  = SET_SUBREF_PT_COEFF_8;
    controlMap["SET_SUBREF_PT_COEFF_9"]  = SET_SUBREF_PT_COEFF_9;
    controlMap["SET_SUBREF_PT_COEFF_10"] = SET_SUBREF_PT_COEFF_10;
    controlMap["SET_SUBREF_PT_COEFF_11"] = SET_SUBREF_PT_COEFF_11;
    controlMap["SET_SUBREF_PT_COEFF_12"] = SET_SUBREF_PT_COEFF_12;
    controlMap["SET_SUBREF_PT_COEFF_13"] = SET_SUBREF_PT_COEFF_13;
    controlMap["SET_SUBREF_PT_COEFF_14"] = SET_SUBREF_PT_COEFF_14;
    controlMap["SET_SUBREF_PT_COEFF_15"] = SET_SUBREF_PT_COEFF_15;
    controlMap["SET_SUBREF_PT_COEFF_16"] = SET_SUBREF_PT_COEFF_16;
    controlMap["SET_SUBREF_PT_COEFF_17"] = SET_SUBREF_PT_COEFF_17;
    controlMap["SET_SUBREF_PT_COEFF_18"] = SET_SUBREF_PT_COEFF_18;
    controlMap["SET_SUBREF_PT_COEFF_19"] = SET_SUBREF_PT_COEFF_19;
    controlMap["SET_SUBREF_PT_COEFF_20"] = SET_SUBREF_PT_COEFF_20;
    controlMap["SET_SUBREF_PT_COEFF_21"] = SET_SUBREF_PT_COEFF_21;
    controlMap["SET_SUBREF_PT_COEFF_22"] = SET_SUBREF_PT_COEFF_22;
    controlMap["SET_SUBREF_PT_COEFF_23"] = SET_SUBREF_PT_COEFF_23;
    controlMap["SET_SUBREF_PT_COEFF_24"] = SET_SUBREF_PT_COEFF_24;
    controlMap["SET_SUBREF_PT_COEFF_25"] = SET_SUBREF_PT_COEFF_25;
    controlMap["SET_SUBREF_PT_COEFF_26"] = SET_SUBREF_PT_COEFF_26;
    controlMap["SET_SUBREF_PT_COEFF_27"] = SET_SUBREF_PT_COEFF_27;
    controlMap["SET_SUBREF_PT_COEFF_28"] = SET_SUBREF_PT_COEFF_28;
    controlMap["SET_SUBREF_PT_COEFF_29"] = SET_SUBREF_PT_COEFF_29;
    controlMap["SET_SUBREF_PT_COEFF_30"] = SET_SUBREF_PT_COEFF_30;
    controlMap["SET_SUBREF_PT_COEFF_31"] = SET_SUBREF_PT_COEFF_31;
    controlMap["SET_SUBREF_PT_DEFAULT"] = SET_SUBREF_PT_DEFAULT;
    controlMap["SET_METR_MODE"]         = SET_METR_MODE;
    controlMap["SET_METR_COEFF_0"]  = SET_METR_COEFF_0;
    controlMap["SET_METR_COEFF_1"]  = SET_METR_COEFF_1;
    controlMap["SET_METR_COEFF_2"]  = SET_METR_COEFF_2;
    controlMap["SET_METR_COEFF_3"]  = SET_METR_COEFF_3;
    controlMap["SET_METR_COEFF_4"]  = SET_METR_COEFF_4;
    controlMap["SET_METR_COEFF_5"]  = SET_METR_COEFF_5;
    controlMap["SET_METR_COEFF_6"]  = SET_METR_COEFF_6;
    controlMap["SET_METR_COEFF_7"]  = SET_METR_COEFF_7;
    controlMap["SET_METR_COEFF_8"]  = SET_METR_COEFF_8;
    controlMap["SET_METR_COEFF_9"]  = SET_METR_COEFF_9;
    controlMap["SET_METR_COEFF_10"] = SET_METR_COEFF_10;
    controlMap["SET_METR_COEFF_11"] = SET_METR_COEFF_11;
    controlMap["SET_METR_COEFF_12"] = SET_METR_COEFF_12;
    controlMap["SET_METR_COEFF_13"] = SET_METR_COEFF_13;
    controlMap["SET_METR_COEFF_14"] = SET_METR_COEFF_14;
    controlMap["SET_METR_COEFF_15"] = SET_METR_COEFF_15;
    controlMap["SET_METR_COEFF_16"] = SET_METR_COEFF_16;
    controlMap["SET_METR_COEFF_17"] = SET_METR_COEFF_17;
    controlMap["SET_METR_COEFF_18"] = SET_METR_COEFF_18;
    controlMap["SET_METR_COEFF_19"] = SET_METR_COEFF_19;
    controlMap["SET_METR_COEFF_20"] = SET_METR_COEFF_20;
    controlMap["SET_METR_COEFF_21"] = SET_METR_COEFF_21;
    controlMap["SET_METR_COEFF_22"] = SET_METR_COEFF_22;
    controlMap["SET_METR_COEFF_23"] = SET_METR_COEFF_23;
    controlMap["SET_METR_COEFF_24"] = SET_METR_COEFF_24;
    controlMap["SET_METR_COEFF_25"] = SET_METR_COEFF_25;
    controlMap["SET_METR_COEFF_26"] = SET_METR_COEFF_26;
    controlMap["SET_METR_COEFF_27"] = SET_METR_COEFF_27;
    controlMap["SET_METR_COEFF_28"] = SET_METR_COEFF_28;
    controlMap["SET_METR_COEFF_29"] = SET_METR_COEFF_29;
    controlMap["SET_METR_COEFF_30"] = SET_METR_COEFF_30;
    controlMap["SET_METR_COEFF_31"] = SET_METR_COEFF_31;
    controlMap["SET_METR_DEFAULT"] = SET_METR_DEFAULT;
    controlMap["SET_METR_CALIBRATION"] = SET_METR_CALIBRATION;
    controlMap["SET_AC_TEMP"] = SET_AC_TEMP;
    controlMap["SET_SHUTTER"]           = SET_SHUTTER;
    //controlMap["SET_ALS"]               = SET_ALS;
    //controlMap["SET_REC_CAB_TEMP"]      = SET_REC_CAB_TEMP;
    controlMap["SELFTEST_CMD"]          = SELFTEST_CMD;
}

void reportValue(BareAMBDeviceInt::ICDPoint point)
{
    std::cout << "MONITOR 0x" << std::hex << point.rca << std::dec << " OK. ";
    switch (point.rca)
	{
	case ACU_MODE_RSP            : 
	{
	unsigned short value[3];
	value[0] = (unsigned short) (point.data.Char[0] & 0x0F);
	value[1] = (unsigned short) ((point.data.Char[0] & 0xF0)>>4);
	value[2] = (unsigned short) point.data.Char[1];
	std::cout << "LENGTH: " << point.dataLength
		  << " VALUES: " 
		  << value[0] << " " 
		  << value[1] << " "
		  << value[2] 
		  << std::endl;
	}
	break;
	case ACU_TRK_MODE_RSP        : 
	case GET_AZ_BRAKE            : 
	case GET_EL_BRAKE            : 
	case GET_AZ_AUX_MODE         :
	case GET_EL_AUX_MODE         :
	case SUBREF_MODE_RSP         :
	{
	unsigned short value;
	value = (unsigned short) point.data.Char[0];
	std::cout << "LENGTH: " << point.dataLength 
		  << " VALUES: " 
		  << value 
		  << std::endl;
	}
	break;
	case AZ_POSN_RSP             : 
	case EL_POSN_RSP             : 
	case GET_AZ_TRAJ_CMD         : 
	case GET_EL_TRAJ_CMD         :
	{
	long value[2];
	// swap data
	char tmp = point.data.Char[0]; point.data.Char[0] = point.data.Char[3];
	point.data.Char[3] = tmp;
	tmp = point.data.Char[1]; point.data.Char[1] = point.data.Char[2];
	point.data.Char[2] = tmp;
	tmp = point.data.Char[4]; point.data.Char[4] = point.data.Char[7];
	point.data.Char[7] = tmp;
	tmp = point.data.Char[5]; point.data.Char[5] = point.data.Char[6];
	point.data.Char[6] = tmp;
	value[0] = point.data.Long[0];
	value[1] = point.data.Long[1];
	std::cout << "LENGTH: " << point.dataLength
		  << " VALUES: " 
		  << value[0] << " " 
		  << value[1] 
		  << std::endl;
	}
	break;
	case GET_ACU_ERROR           : 
	{
	std::cout << "LENGTH: " << point.dataLength;
	if(point.dataLength==0)
	    {
	    std::cout << " VALUES: (empty)"
		      << std::endl;
	    }
	else
	    {
	    unsigned short errorCode;
	    union
	    {
		char byte[4];
		unsigned long address;
	    } can;
	    errorCode = (unsigned short) point.data.Char[0];
	    for(unsigned short index=0; index<4; index++)
		can.byte[index] = point.data.Char[index+1];
	    // swap bytes
	    char tmp = can.byte[0]; can.byte[0] = can.byte[3]; can.byte[3] = tmp;
	    tmp = can.byte[1]; can.byte[1] = can.byte[2]; can.byte[2] = tmp;
	    
	    std::cout << " VALUES: "
		      << "Error Code : " << errorCode
		      << " CAN Address: " << std::hex << can.address << std::dec 
		      << std::endl;
	    }
	}
	break;
	case GET_AZ_ENC              : 
	case GET_EL_ENC              : 
	{
	long value;
	// swap data
	char tmp = point.data.Char[0]; point.data.Char[0] = point.data.Char[3];
	point.data.Char[3] = tmp;
	tmp = point.data.Char[1]; point.data.Char[1] = point.data.Char[2];
	point.data.Char[2] = tmp;
	value = point.data.Long[0];
	std::cout << "LENGTH: " << point.dataLength
		  << " VALUES: " 
		  << value
		  << std::endl;
	}
	break;
	case GET_EL_MOTOR_CURRENTS   : 
	case GET_EL_MOTOR_TEMPS      : 
	case GET_EL_MOTOR_TORQUE     : 
	{
	unsigned short value[2];
	for(unsigned short index=0; index<2; index++)
	    value[index] = (unsigned short) point.data.Char[index] & 0xff;
	std::cout << "LENGTH: " << point.dataLength
		  << " VALUES: " 
		  << value[0] << " "  
		  << value[1] 
		  << std::endl;
	}
	break;
	case GET_AZ_SERVO_COEFF_0    : 
	case GET_AZ_SERVO_COEFF_1    : 
	case GET_AZ_SERVO_COEFF_2    : 
	case GET_AZ_SERVO_COEFF_3    : 
	case GET_AZ_SERVO_COEFF_4    : 
	case GET_AZ_SERVO_COEFF_5    : 
	case GET_AZ_SERVO_COEFF_6    : 
	case GET_AZ_SERVO_COEFF_7    : 
	case GET_AZ_SERVO_COEFF_8    : 
	case GET_AZ_SERVO_COEFF_9    : 
	case GET_AZ_SERVO_COEFF_10   : 
	case GET_AZ_SERVO_COEFF_11    : 
	case GET_AZ_SERVO_COEFF_12    : 
	case GET_AZ_SERVO_COEFF_13    : 
	case GET_AZ_SERVO_COEFF_14    : 
	case GET_AZ_SERVO_COEFF_15    :
	case GET_EL_SERVO_COEFF_0    : 
	case GET_EL_SERVO_COEFF_1    : 
	case GET_EL_SERVO_COEFF_2    : 
	case GET_EL_SERVO_COEFF_3    : 
	case GET_EL_SERVO_COEFF_4    : 
	case GET_EL_SERVO_COEFF_5    : 
	case GET_EL_SERVO_COEFF_6    : 
	case GET_EL_SERVO_COEFF_7    : 
	case GET_EL_SERVO_COEFF_8    : 
	case GET_EL_SERVO_COEFF_9    : 
	case GET_EL_SERVO_COEFF_10    : 
	case GET_EL_SERVO_COEFF_11    : 
	case GET_EL_SERVO_COEFF_12    : 
	case GET_EL_SERVO_COEFF_13    : 
	case GET_EL_SERVO_COEFF_14    : 
	case GET_EL_SERVO_COEFF_15    : 
	case GET_PT_MODEL_COEFF_0    : 
	case GET_PT_MODEL_COEFF_1    : 
	case GET_PT_MODEL_COEFF_2    : 
	case GET_PT_MODEL_COEFF_3    : 
	case GET_PT_MODEL_COEFF_4    : 
	case GET_PT_MODEL_COEFF_5    : 
	case GET_PT_MODEL_COEFF_6    : 
	case GET_PT_MODEL_COEFF_7    : 
	case GET_PT_MODEL_COEFF_8    : 
	case GET_PT_MODEL_COEFF_9    : 
	case GET_PT_MODEL_COEFF_10   : 
	case GET_PT_MODEL_COEFF_11   : 
	case GET_PT_MODEL_COEFF_12   : 
	case GET_PT_MODEL_COEFF_13   : 
	case GET_PT_MODEL_COEFF_14   : 
	case GET_PT_MODEL_COEFF_15   : 
	case GET_PT_MODEL_COEFF_16   : 
	case GET_PT_MODEL_COEFF_17   : 
	case GET_PT_MODEL_COEFF_18   : 
	case GET_PT_MODEL_COEFF_19   : 
	case GET_PT_MODEL_COEFF_20    : 
	case GET_PT_MODEL_COEFF_21    : 
	case GET_PT_MODEL_COEFF_22    : 
	case GET_PT_MODEL_COEFF_23    : 
	case GET_PT_MODEL_COEFF_24    : 
	case GET_PT_MODEL_COEFF_25    : 
	case GET_PT_MODEL_COEFF_26    : 
	case GET_PT_MODEL_COEFF_27    : 
	case GET_PT_MODEL_COEFF_28    : 
	case GET_PT_MODEL_COEFF_29    : 
	case GET_PT_MODEL_COEFF_30   : 
	case GET_PT_MODEL_COEFF_31   : 
	case GET_SUBREF_PT_COEFF_0    : 
	case GET_SUBREF_PT_COEFF_1    : 
	case GET_SUBREF_PT_COEFF_2    : 
	case GET_SUBREF_PT_COEFF_3    : 
	case GET_SUBREF_PT_COEFF_4    : 
	case GET_SUBREF_PT_COEFF_5    : 
	case GET_SUBREF_PT_COEFF_6    : 
	case GET_SUBREF_PT_COEFF_7    : 
	case GET_SUBREF_PT_COEFF_8    : 
	case GET_SUBREF_PT_COEFF_9    : 
	case GET_SUBREF_PT_COEFF_10   : 
	case GET_SUBREF_PT_COEFF_11   : 
	case GET_SUBREF_PT_COEFF_12   : 
	case GET_SUBREF_PT_COEFF_13   : 
	case GET_SUBREF_PT_COEFF_14   : 
	case GET_SUBREF_PT_COEFF_15   : 
	case GET_SUBREF_PT_COEFF_16   : 
	case GET_SUBREF_PT_COEFF_17   : 
	case GET_SUBREF_PT_COEFF_18   : 
	case GET_SUBREF_PT_COEFF_19   : 
	case GET_SUBREF_PT_COEFF_20    : 
	case GET_SUBREF_PT_COEFF_21    : 
	case GET_SUBREF_PT_COEFF_22    : 
	case GET_SUBREF_PT_COEFF_23    : 
	case GET_SUBREF_PT_COEFF_24    : 
	case GET_SUBREF_PT_COEFF_25    : 
	case GET_SUBREF_PT_COEFF_26    : 
	case GET_SUBREF_PT_COEFF_27    : 
	case GET_SUBREF_PT_COEFF_28    : 
	case GET_SUBREF_PT_COEFF_29    : 
	case GET_SUBREF_PT_COEFF_30   : 
	case GET_SUBREF_PT_COEFF_31   : 
	case GET_METR_COEFF_0    : 
	case GET_METR_COEFF_1    : 
	case GET_METR_COEFF_2    : 
	case GET_METR_COEFF_3    : 
	case GET_METR_COEFF_4    : 
	case GET_METR_COEFF_5    : 
	case GET_METR_COEFF_6    : 
	case GET_METR_COEFF_7    : 
	case GET_METR_COEFF_8    : 
	case GET_METR_COEFF_9    : 
	case GET_METR_COEFF_10   : 
	case GET_METR_COEFF_11   : 
	case GET_METR_COEFF_12   : 
	case GET_METR_COEFF_13   : 
	case GET_METR_COEFF_14   : 
	case GET_METR_COEFF_15   : 
	case GET_METR_COEFF_16   : 
	case GET_METR_COEFF_17   : 
	case GET_METR_COEFF_18   : 
	case GET_METR_COEFF_19   : 
	case GET_METR_COEFF_20    : 
	case GET_METR_COEFF_21    : 
	case GET_METR_COEFF_22    : 
	case GET_METR_COEFF_23    : 
	case GET_METR_COEFF_24    : 
	case GET_METR_COEFF_25    : 
	case GET_METR_COEFF_26    : 
	case GET_METR_COEFF_27    : 
	case GET_METR_COEFF_28    : 
	case GET_METR_COEFF_29    : 
	case GET_METR_COEFF_30   : 
	case GET_METR_COEFF_31   : 
	{
	double value;
	// swap bytes
	char tmp = point.data.Char[0]; point.data.Char[0] = point.data.Char[7];
	point.data.Char[7] = tmp;
	tmp = point.data.Char[1]; point.data.Char[1] = point.data.Char[6];
	point.data.Char[6] = tmp;
	tmp = point.data.Char[2]; point.data.Char[2] = point.data.Char[5];
	point.data.Char[5] = tmp;
	tmp = point.data.Char[3]; point.data.Char[3] = point.data.Char[4];
	point.data.Char[4] = tmp;
	value = point.data.Double;
	std::cout << "LENGTH: " << point.dataLength
		  << " VALUES: " 
		  << value 
		  << std::endl;
	}
	break;
	case GET_AZ_STATUS           : 
	case GET_AZ_STATUS_2         : 
	case GET_EL_STATUS           : 
	case GET_EL_STATUS_2         : 
	case GET_AZ_ENC_STATUS       : 
	case GET_EL_ENC_STATUS       : 
	case GET_SYSTEM_STATUS       : 
	case GET_SYSTEM_STATUS_2     : 
	case GET_SHUTTER             : 
	case GET_STOW_PIN            : 
	case GET_SUBREF_LIMITS       : 
	case GET_SUBREF_STATUS       : 
	case GET_METR_MODE           : 
	case GET_METR_EQUIP_STATUS   : 
	case GET_POWER_STATUS        : 
	case GET_AC_STATUS           : 
	case GET_UPS_ALARMS          :
	case GET_ALS_STATUS          :
	case GET_FAN_STATUS          :
	case GET_AZ_RATEFDBK_MODE          :
	case GET_EL_RATEFDBK_MODE          :
	{
	std::cout << "LENGTH: " << point.dataLength
		  << " VALUES: " << std::endl;
	for(unsigned int index=0; index < point.dataLength; index++)
	    {
	      short val = point.data.Char[index] & 0xff;
	      std::cout << "byte " << index << ": " << val << std::endl;
	    for(unsigned int bit=0; bit <8; bit++)
		{
		std::cout << "    bit " << bit
			  << ": ";
		//		if((point.data.Char[index]>>bit)&0x01 == 0x0)
		short val1 = (val>>bit) & 0x1;
		if (val1 == 0x0)
		  std::cout << "false " << val1 << std::endl;
		else
		  std::cout << "true " << val1 << std::endl;
		}
	    }
	}
	break;
	case GET_AZ_ENCODER_OFFSET   : 
	case GET_EL_ENCODER_OFFSET   : 
	{
	long value;
	// swap data
	char tmp = point.data.Char[0]; point.data.Char[0] = point.data.Char[3];
	point.data.Char[3] = tmp;
	tmp = point.data.Char[1]; point.data.Char[1] = point.data.Char[2];
	point.data.Char[2] = tmp;
	value = point.data.Long[0];
	std::cout << "LENGTH: " << point.dataLength
		  << " VALUES: " 
		  << value 
		  << std::endl;
	}
	break;
	case GET_CAN_ERROR           : 
	{
	unsigned short value1, value2;
	// swap data
	char tmp = point.data.Char[0]; point.data.Char[0]= point.data.Char[1];
	point.data.Char[1] = tmp;
	value1 = point.data.UShort[0];
	value2 = (unsigned short) point.data.Char[3];
	std::cout << "LENGTH: " << point.dataLength
		  << " VALUES: " 
		  << value1 << " "
		  << value2 
		  << std::endl;
	}
	break;
	case GET_AZ_MOTOR_CURRENTS   : 
	case GET_AZ_MOTOR_TEMPS      : 
	case GET_AZ_MOTOR_TORQUE     : 
	{
	unsigned short value[4];
	for(unsigned short index=0; index<4; index++)
	    value[index] = (unsigned short) point.data.Char[index] & 0xff;
	std::cout << "LENGTH: " << point.dataLength
		  << " VALUES: " 
		  << value[0] << " "  
		  << value[1] << " "
	          << value[2] << " "
	          << value[3]
		  << std::endl;
//	unsigned short value;
//	value = (unsigned short) point.data.Char[0];
//	std::cout << "LENGTH: " << point.dataLength
//		  << " VALUES: " 
//		  << value 
//		  << std::endl;
	}
	break;
	case GET_SYSTEM_ID           : 
	case GET_SW_REV_LEVEL        : 
	case BUS_IDENTIFICATION      : 
	{
	unsigned short value[3];
	for(unsigned short index=0; index<3; index++)
	    value[index] = (unsigned short) point.data.Char[index] & 0xff;
	std::cout << "LENGTH: " << point.dataLength
		  << " VALUES: VERSION " 
		  << value[0] << "." 
		  << value[1] << "." 
		  << value[2]
		  << std::endl;
	}
	break;
	case GET_IDLE_STOW_TIME      : 
	{
	unsigned short value;
	// swap data
	char tmp = point.data.Char[0]; point.data.Char[0]= point.data.Char[1];
	point.data.Char[1] = tmp;
	value = point.data.UShort[0];
	std::cout << "LENGTH: " << point.dataLength
		  << " VALUES: " 
		  << value
		  << std::endl;
	}
	break;
	case GET_IP_ADDRESS          : 
	{
	unsigned short value[8];
	for(unsigned short index = 0; index < 8; index++)
	    value[index] = (unsigned short) point.data.Char[index] & 0xff;
	std::cout << "LENGTH: " << point.dataLength
		  << " VALUES: " << std::endl
		  << "    IP ADDRESS: "
		  << value[0] << "."
		  << value[1] << "."
		  << value[2] << "."
		  << value[3] << std::endl
		  << "    SUBNET MASK: " 
		  << value[4] << "."
		  << value[5] << "."
		  << value[6] << "."
		  << value[7]
		  << std::endl;
	}
	break;
	case GET_IP_GATEWAY          : 
	{
	unsigned short value[4];
	for(unsigned short index = 0; index < 4; index++)
	    value[index] = (unsigned short) point.data.Char[index] & 0xff;
	std::cout << "LENGTH: " << point.dataLength
		  << " VALUES: GATEWAY IP " 
		  << value[0] << "."
		  << value[1] << "."
		  << value[2] << "."
		  << value[3] 
		  << std::endl;
	}
	break;
	case GET_NUM_TRANS           : 
	{
	unsigned long value;
	// swap data
	char tmp = point.data.Char[0]; point.data.Char[0] = point.data.Char[3];
	point.data.Char[3] = tmp;
	tmp = point.data.Char[1]; point.data.Char[1] = point.data.Char[2];
	point.data.Char[2] = tmp;
	value = point.data.ULong[0];
	std::cout << "LENGTH: " << point.dataLength
		  << " VALUES: " 
		  << value
		  << std::endl;
	}
	break;
	case GET_SUBREF_ABS_POSN     : 
	case GET_SUBREF_DELTA_POSN   : 
	case GET_SUBREF_ROTATION     : 
	{
	short value[3];
	// swap data
	char tmp = point.data.Char[0]; point.data.Char[0]= point.data.Char[1];
	point.data.Char[1] = tmp;
	tmp = point.data.Char[2]; point.data.Char[2]= point.data.Char[3];
	point.data.Char[3] = tmp;
	tmp = point.data.Char[4]; point.data.Char[4]= point.data.Char[5];
	point.data.Char[5] = tmp;
	for(unsigned short index = 0; index < 3; index++)
	    value[index] = point.data.Short[index];
	std::cout << "LENGTH: " << point.dataLength
		  << " VALUES: " 
		  << value[0] << " "
		  << value[1] << " "
		  << value[2]
		  << std::endl;
	}
	break;
	case GET_METR_DISPL_0        : 
	case GET_METR_DISPL_1        : 
	case GET_METR_DISPL_2        : 
	case GET_METR_DISPL_3        : 
	case GET_METR_DISPL_4        : 
	case GET_METR_DISPL_5        : 
	case GET_METR_DISPL_6        : 
	case GET_METR_DISPL_7        : 
	case GET_METR_DISPL_8        : 
	case GET_METR_DISPL_9        : 
	case GET_METR_DISPL_10        : 
	case GET_METR_DISPL_11        : 
	case GET_METR_DISPL_12        : 
	case GET_METR_DISPL_13        : 
	case GET_METR_DISPL_14        : 
	case GET_METR_DISPL_15        : 
	case GET_METR_DISPL_16        : 
	case GET_METR_DISPL_17        : 
	case GET_METR_DISPL_18        : 
	case GET_METR_DISPL_19        : 
	case GET_METR_DISPL_20        : 
	case GET_METR_DISPL_21        : 
	case GET_METR_DISPL_22        : 
	case GET_METR_DISPL_23        : 
	case GET_METR_DELTAPATH        : 
	{
	long value;
	// swap data
	char tmp = point.data.Char[0]; point.data.Char[0] = point.data.Char[3];
	point.data.Char[3] = tmp;
	tmp = point.data.Char[1]; point.data.Char[1] = point.data.Char[2];
	point.data.Char[2] = tmp;
	value = point.data.Long[0];
	std::cout << "LENGTH: " << point.dataLength
		  << " VALUES: " 
		  << value 
		  << std::endl;
	}
	break;
	case GET_METR_TEMPS_0        : 
	case GET_METR_TEMPS_1        : 
	case GET_METR_TEMPS_2        : 
	case GET_METR_TEMPS_3        : 
	case GET_METR_TEMPS_4        : 
	case GET_METR_TEMPS_5        : 
	case GET_METR_TEMPS_6        : 
	case GET_METR_TEMPS_7        : 
	case GET_METR_TEMPS_8        : 
	case GET_METR_TEMPS_9        : 
	case GET_METR_TEMPS_10       : 
	case GET_METR_TEMPS_11       : 
	case GET_METR_TEMPS_12       : 
	case GET_METR_TEMPS_13       : 
	case GET_METR_TEMPS_14       : 
	case GET_METR_TEMPS_15       : 
	case GET_METR_TEMPS_16       : 
	case GET_METR_TEMPS_17       : 
	case GET_METR_TEMPS_18       :
	case GET_METR_TEMPS_19       :
	case GET_METR_TEMPS_20       : 
	case GET_METR_TEMPS_21       : 
	case GET_METR_TEMPS_22       : 
	case GET_METR_TEMPS_23       : 
	case GET_METR_TEMPS_24       : 
	case GET_METR_TEMPS_25       : 
	case GET_METR_TEMPS_26       : 
	case GET_METR_TEMPS_27       : 
	case GET_METR_TEMPS_28       :
	case GET_METR_TEMPS_29       :
	case GET_METR_TEMPS_30       : 
	case GET_METR_TEMPS_31       : 
	case GET_METR_TEMPS_32       : 
	case GET_METR_TEMPS_33       : 
	case GET_METR_TEMPS_34       : 
	case GET_METR_TEMPS_35       : 
	case GET_METR_TEMPS_36       : 
	case GET_METR_TEMPS_37       : 
	case GET_METR_TEMPS_38       :
	case GET_METR_TEMPS_39       :
	case GET_METR_TEMPS_40       : 
	case GET_METR_TEMPS_41       : 
	case GET_METR_TEMPS_42       : 
	case GET_METR_TEMPS_43       : 
	case GET_METR_TEMPS_44       : 
	case GET_METR_TEMPS_45       : 
	case GET_METR_TEMPS_46       : 
	case GET_METR_TEMPS_47       : 
	case GET_METR_TEMPS_48       :
	case GET_METR_TEMPS_49       :
	case GET_METR_TEMPS_50       : 
	case GET_METR_TEMPS_51       : 
	case GET_METR_TEMPS_52       : 
	case GET_METR_TEMPS_53       : 
	case GET_METR_TEMPS_54       : 
	case GET_METR_TEMPS_55       : 
	case GET_METR_TEMPS_56       : 
	case GET_METR_TEMPS_57       : 
	case GET_METR_TEMPS_58       :
	case GET_METR_TEMPS_59       :
	case GET_METR_TEMPS_60       : 
	case GET_METR_TEMPS_61       : 
	case GET_METR_TEMPS_62       : 
	case GET_METR_TEMPS_63       : 
	case GET_METR_TEMPS_64       : 
	case GET_METR_TEMPS_65       : 
	case GET_METR_TEMPS_66       : 
	case GET_METR_TEMPS_67       : 
	case GET_METR_TEMPS_68       :
	case GET_METR_TEMPS_69       :
	case GET_METR_TEMPS_70       : 
	case GET_METR_TEMPS_71       : 
	case GET_METR_TEMPS_72       : 
	case GET_METR_TEMPS_73       : 
	case GET_METR_TEMPS_74       : 
	case GET_METR_TEMPS_75       : 
	case GET_METR_TEMPS_76       : 
	case GET_METR_TEMPS_77       : 
	case GET_METR_TEMPS_78       :
	case GET_METR_TEMPS_79       :
 	{
	short value[4];
	// swap data
	char tmp = point.data.Char[0]; point.data.Char[0]= point.data.Char[1];
	point.data.Char[1] = tmp;
	tmp = point.data.Char[2]; point.data.Char[2]= point.data.Char[3];
	point.data.Char[3] = tmp;
	tmp = point.data.Char[4]; point.data.Char[4]= point.data.Char[5];
	point.data.Char[5] = tmp;
	tmp = point.data.Char[6]; point.data.Char[6]= point.data.Char[7];
	point.data.Char[7] = tmp;
	for(unsigned short index = 0; index < 4; index++)
	    value[index] = point.data.Short[index];
	std::cout << "LENGTH: " << point.dataLength
		  << " VALUES: " 
		  << value[0] << " "
		  << value[1] << " "
		  << value[2] << " "
		  << value[3]
		  << std::endl;
	}
	break;
	case GET_SUBREF_ENCODER_POSN_1         : 
	case GET_SUBREF_ENCODER_POSN_2         : 
	case GET_METR_TILT_0         : 
	case GET_METR_TILT_1         : 
	case GET_METR_TILT_2         : 
	case GET_METR_TILT_3         : 
	case GET_METR_TILT_4         : 
 	{
	short value[3];
	// swap data
	char tmp = point.data.Char[0]; point.data.Char[0]= point.data.Char[1];
	point.data.Char[1] = tmp;
	tmp = point.data.Char[2]; point.data.Char[2]= point.data.Char[3];
	point.data.Char[3] = tmp;
	tmp = point.data.Char[4]; point.data.Char[4]= point.data.Char[5];
	point.data.Char[5] = tmp;
	for(unsigned short index = 0; index < 3; index++)
	    value[index] = point.data.Short[index];
	std::cout << "LENGTH: " << point.dataLength
		  << " VALUES: " 
		  << value[0] << " "
		  << value[1] << " "
		  << value[2]
		  << std::endl;
	}
	break;
	case GET_UPS_OUTPUT_VOLTS_1         : 
	case GET_UPS_OUTPUT_VOLTS_2         : 
	case GET_UPS_OUTPUT_CURRENT_1         : 
	case GET_UPS_OUTPUT_CURRENT_2         : 
	case GET_UPS_BYPASS_VOLTS    : 
	case GET_UPS_INVERTER_VOLTS    : 
	{
	short value[3];
	// swap data
	char tmp = point.data.Char[0]; point.data.Char[0]= point.data.Char[1];
	point.data.Char[1] = tmp;
	tmp = point.data.Char[2]; point.data.Char[2]= point.data.Char[3];
	point.data.Char[3] = tmp;
	tmp = point.data.Char[4]; point.data.Char[4]= point.data.Char[5];
	point.data.Char[5] = tmp;
	for(unsigned short index = 0; index < 3; index++)
	    value[index] = point.data.Short[index];
	std::cout << "LENGTH: " << point.dataLength
		  << " VALUES: " 
		  << value[0] << " "
		  << value[1] << " "
		  << value[2]
		  << std::endl;
	}
	break;
	case GET_UPS_BATTERY_OUTPUT  :
//	case GET_UPS_BATTERY_STATUS  : 
//	case GET_UPS_FREQS           : 
	{
	short value[2];
	// swap data
	char tmp = point.data.Char[0]; point.data.Char[0]= point.data.Char[1];
	point.data.Char[1] = tmp;
	tmp = point.data.Char[2]; point.data.Char[2]= point.data.Char[3];
	point.data.Char[3] = tmp;
	for(unsigned short index = 0; index < 2; index++)
	    value[index] = point.data.Short[index];
	std::cout << "LENGTH: " << point.dataLength
		  << " VALUES: " 
		  << value[0] << " "
		  << value[1]
		  << std::endl;
	}
	break;
	case GET_METR_DELTAS         : 
	{
	long value[2];
	// swap data
	char tmp = point.data.Char[0]; point.data.Char[0] = point.data.Char[3];
	point.data.Char[3] = tmp;
	tmp = point.data.Char[1]; point.data.Char[1] = point.data.Char[2];
	point.data.Char[2] = tmp;
	tmp = point.data.Char[4]; point.data.Char[4] = point.data.Char[7];
	point.data.Char[7] = tmp;
	tmp = point.data.Char[5]; point.data.Char[5] = point.data.Char[6];
	point.data.Char[6] = tmp;
	for(unsigned short index = 0; index < 2; index++)
	    value[index] = point.data.Long[index];
	std::cout << "LENGTH: " << point.dataLength
		  << " VALUES: " 
		  << value[0] << " "
		  << value[1]
		  << std::endl;
	break;
	}
	case GET_ANTENNA_TEMPS       : 
	{
	short value[2];
	// swap data
	char tmp = point.data.Char[0]; point.data.Char[0]= point.data.Char[1];
	point.data.Char[1] = tmp;
	tmp = point.data.Char[2]; point.data.Char[2]= point.data.Char[3];
	point.data.Char[3] = tmp;
	for(unsigned short index = 0; index < 2; index++)
	    value[index] = point.data.Short[index];
	std::cout << "LENGTH: " << point.dataLength
		  << " VALUES: " 
		  << value[0] << " "
		  << value[1]
		  << std::endl;
	break;
	}
	case SELFTEST_RSP            : 
	{
	std::cout << "LENGTH: " << point.dataLength
		  << " VALUES: " << std::endl;
	std::cout << "byte 0: " << std::endl;
	for(unsigned int bit=0; bit <3; bit++)
	    {
	    std::cout << "    bit " << bit
		      << ": ";
	    if((point.data.Char[0]>>bit)&0x01 == 0x0)
		std::cout << "false ";
	    else
		std::cout << "true " << std::endl;
	    }
	union
	{
	    char Char[4];
	    short Short[2];
	} value;
	// swap data
	char tmp = point.data.Char[0]; point.data.Char[0]= point.data.Char[1];
	point.data.Char[1] = tmp;
	tmp = point.data.Char[2]; point.data.Char[2]= point.data.Char[3];
	point.data.Char[3] = tmp;
	for(unsigned short index=0; index < 4; index++)
	    value.Char[index] = point.data.Char[index+1];
	std::cout << "Failing tests   : " << value.Short[0] << ": " << std::endl;
	std::cout << "Number of errors: " << value.Short[1] << ": " << std::endl;
	}
	break;
	case SELFTEST_ERR            :
	{
	std::cout << "LENGTH: " << point.dataLength
		  << " VALUES: ";
	std::cout << "Test failed     : " << point.data.Short[0] << ": " << std::endl;
	union
	{
	    char Char[4];
	    float Float;
	} value;
	// swap data
	char tmp = point.data.Char[0]; point.data.Char[0] = point.data.Char[3];
	point.data.Char[3] = tmp;
	tmp = point.data.Char[1]; point.data.Char[1] = point.data.Char[2];
	point.data.Char[2] = tmp;
	for(unsigned short index=0; index < 4; index++)
	    value.Char[index] = point.data.Char[index+2];
	std::cout << "VALUE           : " << value.Float << ": " << std::endl;
	}
	break;
	default:
	    break;
	}
    std::cout << std::endl;
}

int main(int argc, char* argv[])
{
    ACE_Get_Opt options(argc, argv);
    options.long_option("help"       , 'h');
    options.long_option("channel"    , 'c', ACE_Get_Opt::ARG_OPTIONAL);
    options.long_option("nodeId"     , 'n', ACE_Get_Opt::ARG_OPTIONAL);
    options.long_option("monitor"    , 'm', ACE_Get_Opt::ARG_OPTIONAL);
    options.long_option("monitor_rca", 'r', ACE_Get_Opt::ARG_OPTIONAL);
    options.long_option("control"    , 'p', ACE_Get_Opt::ARG_OPTIONAL);
    options.long_option("control_rca", 'x', ACE_Get_Opt::ARG_OPTIONAL);
    parseOptions(options);

    if(exitProgram)
	{
	usage(argv[0]);
	return -ENODEV;
	}

    BareAMBDeviceInt mc;
    if (mc.initialize(channel, nodeId) == false)
	{
        return -ENODEV;
	}
    initializeMonitorMap();
    initializeControlMap();
    BareAMBDeviceInt::ICDPoint point;

    // Tranforming monitor mnemonics to rcas
    for(std::vector< string >::const_iterator mnemonic(monitorMnemonic.begin());
	mnemonic != monitorMnemonic.end(); ++mnemonic)
	{
	string element = *mnemonic;
	    if(monitorMap.find(element) == monitorMap.end())
		{
		std::cout << "WARNING    : " << element 
			  <<" is not a valid monitor mnemonic. Discarting it."<<std::endl;
		}
	    else
		{
		std::cout << "INFO       : " << element <<" transformed to 0x" 
 			  << std::hex << monitorMap[element] << std::dec << std::endl;
		monitorRCA.push_back(monitorMap[element]); 
		}
	}
    
    // Tranforming control mnemonics to rcas
    for(std::vector< string >::const_iterator mnemonic(controlMnemonic.begin());
	mnemonic != controlMnemonic.end(); ++mnemonic)
	{
	string element = (*mnemonic).substr(0,(*mnemonic).find(","));
	if(controlMap.find(element) == controlMap.end())
	    {
	    std::cout << "WARNING    : " << element 
		      <<" is not a valid control mnemonic. Discarting it."<<std::endl;
	    }
	else
	    {
	    std::cout << "INFO        : " << element <<" transformed to 0x" 
		      << std::hex << controlMap[element] << std::dec << std::endl;
	    std::stringstream rca;
	    rca << "0x" << std::hex << controlMap[element];
	    string command;
	    if((*mnemonic).find(",") != string::npos)
		command = rca.str()+(*mnemonic).substr((*mnemonic).find(","),(*mnemonic).length());
	    else
		command = rca.str();
	    controlRCA.push_back(command); 
	    }
	}

    if (monitorRCA.size() == 0 && controlRCA.size() == 0)
	{
	std::cout << "No valid monitor or control point has been specified." << std::endl;
	usage(argv[0]);
	return -ENODEV;
	}
    
    // Sending monitors
    for(std::vector< AmbRelativeAddr >::const_iterator rca(monitorRCA.begin());
	rca != monitorRCA.end(); ++rca)
	{
	point.rca = *rca;
	point.status = AMBERR_PENDING;

	mc.sendMonitor(point);
	if(point.status==0)
	    {
	    reportValue(point);
	    }
	else
	    std::cout << "MONITOR 0x" << std::hex << point.rca << std::dec 
		      <<" FAILED. Status: " << point.status << std::endl;
	}

    //Sending controls
    for(std::vector< string >::const_iterator control(controlRCA.begin());
	control != controlRCA.end(); ++control)
	{
	bool abortControl = false;
	AmbRelativeAddr RCA = strtol((*control).substr(0,(*control).find(",")).c_str(),NULL,0);
	point.rca = RCA;
	point.status = AMBERR_PENDING;

	string arguments;
	if((*control).find(",") != string::npos)
	    {
	    arguments = (*control).substr((*control).find(",")+1,(*control).length());
	    std::cout << "CONTROL RCA : 0x" << std::hex << RCA << std::dec << " received" << std::endl;
	    std::cout << "CONTROL ARG : " << arguments << " received" << std::endl;
	    }
	else
	    {
	    std::cout << "WARNING   : 0x" << std::hex << RCA << std::dec << " needs arguments. Discarting it." << std::endl;
	    point.rca = 0x00;
	    abortControl = true;
	    }
	switch(point.rca)
	    {
	    case ACU_MODE_CMD:
	    {
	    std::istringstream tmp1;
	    std::istringstream tmp2;
	    unsigned int position1 = arguments.find(",",0);
	    if(position1 != string::npos)
		{
		tmp1.str(arguments.substr(0,position1));
		unsigned int position2 = arguments.find(",",position1+1);
		if(position2 != string::npos)
		    {
		    string arguments1 = arguments.substr(position1+1,arguments.length());
		    tmp2.str(arguments1.substr(0,arguments1.find(",",0)));
		    }
		else
		    tmp2.str(arguments.substr(position1+1,arguments.length()));
		}
	    else
		{
		std::cout << "WARNING   : 0x" << std::hex << RCA << std::dec 
			  << " incomplete set of arguments. Discarting it." << std::endl;
		point.rca = 0x00;
		abortControl = true;
		}
	    if(!abortControl)
		{
		unsigned short value1(0U);
		unsigned short value2(0U);
		unsigned short value3(0U);
		tmp1 >> value1;
		tmp2 >> value2;
		char value(0);
		value3 = value1 + (value2<<4);
		value = (char) value3;
		point.dataLength = 1;
		point.data.Char[0] = value;
		}
	    }
	    if(point.rca!=0x00)
		std::cout << "CONTROL DAT : " << (unsigned short) point.data.Char[0] << " sent" << std::endl;
	    break;
	    case ACU_TRK_MODE_CMD:
	    case CLEAR_FAULT_CMD:
	    case SET_AZ_AUX_MODE:
	    case SET_AZ_RATEFDBK_MODE:
	    case SET_AZ_BRAKE:
	    case SET_AZ_SERVO_DEFAULT:
	    case SET_EL_AUX_MODE:
	    case SET_EL_RATEFDBK_MODE:
	    case SET_EL_BRAKE:
	    case SET_EL_SERVO_DEFAULT:
	    case SET_PT_DEFAULT:
	    case SET_SUBREF_PT_DEFAULT:
            case SET_METR_DEFAULT:
            case SET_METR_CALIBRATION:
	    case SUBREF_DELTA_ZERO_CMD:
	    case SET_SHUTTER:
	    case SUBREF_MODE_CMD:
	    case SET_AC_TEMP:
	    //case SET_ALS:
	    //case SET_REC_CAB_TEMP:
	    case SELFTEST_CMD:
	    {
	    std::istringstream tmp;
	    if(arguments.find(",",0) != string::npos)
		tmp.str(arguments.substr(0,arguments.find(",",0)));
	    else
		tmp.str(arguments);
	    unsigned short value1(0U);
	    tmp >> value1;
	    char value(0);
	    value = (char) value1;
	    point.dataLength = 1;
	    point.data.Char[0] = value;
	    }
	    std::cout << "CONTROL DAT : " << (unsigned short) point.data.Char[0] << " sent" << std::endl;
	    break;
	    case AZ_TRAJ_CMD:
	    case EL_TRAJ_CMD:
	    {
	    std::istringstream tmp1;
	    std::istringstream tmp2;
	    unsigned int position1 = arguments.find(",",0);
	    if(position1 != string::npos)
		{
		tmp1.str(arguments.substr(0,position1));
		string arguments1 = arguments.substr(position1+1,arguments.length());
		unsigned int position2 = arguments1.find(",",0);
		if(position2 != string::npos)
		    tmp2.str(arguments1.substr(0,position2));
		else
		    tmp2.str(arguments1);
		}
	    else
		{
		std::cout << "WARNING     : 0x" << std::hex << RCA << std::dec 
			  << " incomplete set of arguments. Discarting it." << std::endl;
		point.rca = 0x00;
		abortControl = true;
		}
	    if(!abortControl)
		{
		long value1(0U);
		long value2(0U);
		tmp1 >> value1;
		tmp2 >> value2;
		point.dataLength = 8;
		point.data.Long[0] = value1;
		point.data.Long[1] = value2;
		// swap data
		char tmp = point.data.Char[0]; 
		point.data.Char[0] = point.data.Char[3];
		point.data.Char[3] = tmp;
		tmp = point.data.Char[1]; 
		point.data.Char[1] = point.data.Char[2];
		point.data.Char[2] = tmp;
		tmp = point.data.Char[4]; 
		point.data.Char[4] = point.data.Char[7];
		point.data.Char[7] = tmp;
		tmp = point.data.Char[5]; 
		point.data.Char[5] = point.data.Char[6];
		point.data.Char[6] = tmp;
	    if(point.rca!=0x00)
                /*
		std::cout << "CONTROL DAT : " << (long) point.data.Long[0] << "," 
			  <<(long) point.data.Long[1] << " sent" << std::endl;
                */
		std::cout << "CONTROL DAT : " << value1 << "," 
			  << value2 << " sent" << std::endl;
		}
	    }
	    break;
	    case RESET_ACU_CMD:
	    {
	    std::istringstream tmp1;
	    std::istringstream tmp2;
	    std::istringstream tmp3;
	    unsigned int position1 = arguments.find(",",0);
	    if(position1 != string::npos)
		{
		tmp1.str(arguments.substr(0,position1));
		string arguments1 = arguments.substr(position1+1,arguments.length());
		unsigned int position2 = arguments1.find(",",0);
		if(position2 != string::npos)
		    {
		    tmp2.str(arguments1.substr(0,position2));
		    string arguments2 = arguments1.substr(position2+1,arguments1.length());
		    unsigned position3 = arguments2.find(",",0);
		    if(position3 != string::npos)
			tmp3.str(arguments2.substr(0,position3));
		    else
			tmp3.str(arguments2);
		    }
		else
		    {
		    std::cout << "WARNING     : 0x" << std::hex << RCA << std::dec 
			      << " incomplete set of arguments. Discarting it." << std::endl;
		    point.rca = 0x00;
		    abortControl = true;
		    }
		}
	    else
		{
		std::cout << "WARNING     : 0x" << std::hex << RCA << std::dec 
			  << " incomplete set of arguments. Discarting it." << std::endl;
		point.rca = 0x00;
		abortControl = true;
		}
	    if(!abortControl)
		{
		unsigned short value0(0U);
		unsigned short value1(0U);
		unsigned short value2(0U);
		unsigned short value3(0U);
		tmp1 >> value0;
		tmp2 >> value1;
		tmp3 >> value2;
		value3 = value0 
		    + (value1<<1) 
		    + (value2<<2);
		char value(0);
		value = (char) value3;
		point.dataLength = 1;
		point.data.Char[0] = value;
		}
	    if(point.rca!=0x00)
		std::cout << "CONTROL DAT : " << (unsigned short) point.data.Char[0] << "," 
			  << " sent" << std::endl;
	    }
	    break;
	    case SET_AZ_SERVO_COEFF_0:
	    case SET_AZ_SERVO_COEFF_1:
	    case SET_AZ_SERVO_COEFF_2:
	    case SET_AZ_SERVO_COEFF_3:
	    case SET_AZ_SERVO_COEFF_4:
	    case SET_AZ_SERVO_COEFF_5:
	    case SET_AZ_SERVO_COEFF_6:
	    case SET_AZ_SERVO_COEFF_7:
	    case SET_AZ_SERVO_COEFF_8:
	    case SET_AZ_SERVO_COEFF_9:
	    case SET_AZ_SERVO_COEFF_10:
	    case SET_AZ_SERVO_COEFF_11:
	    case SET_AZ_SERVO_COEFF_12:
	    case SET_AZ_SERVO_COEFF_13:
	    case SET_AZ_SERVO_COEFF_14:
	    case SET_AZ_SERVO_COEFF_15:
	    case SET_EL_SERVO_COEFF_0:
	    case SET_EL_SERVO_COEFF_1:
	    case SET_EL_SERVO_COEFF_2:
	    case SET_EL_SERVO_COEFF_3:
	    case SET_EL_SERVO_COEFF_4:
	    case SET_EL_SERVO_COEFF_5:
	    case SET_EL_SERVO_COEFF_6:
	    case SET_EL_SERVO_COEFF_7:
	    case SET_EL_SERVO_COEFF_8:
	    case SET_EL_SERVO_COEFF_9:
	    case SET_EL_SERVO_COEFF_10:
	    case SET_EL_SERVO_COEFF_11:
	    case SET_EL_SERVO_COEFF_12:
	    case SET_EL_SERVO_COEFF_13:
	    case SET_EL_SERVO_COEFF_14:
	    case SET_EL_SERVO_COEFF_15:
	    case SET_PT_MODEL_COEFF_0:
	    case SET_PT_MODEL_COEFF_1:
	    case SET_PT_MODEL_COEFF_2:
	    case SET_PT_MODEL_COEFF_3:
	    case SET_PT_MODEL_COEFF_4:
	    case SET_PT_MODEL_COEFF_5:
	    case SET_PT_MODEL_COEFF_6:
	    case SET_PT_MODEL_COEFF_7:
	    case SET_PT_MODEL_COEFF_8:
	    case SET_PT_MODEL_COEFF_9:
	    case SET_PT_MODEL_COEFF_10:
	    case SET_PT_MODEL_COEFF_11:
	    case SET_PT_MODEL_COEFF_12:
	    case SET_PT_MODEL_COEFF_13:
	    case SET_PT_MODEL_COEFF_14:
	    case SET_PT_MODEL_COEFF_15:
	    case SET_PT_MODEL_COEFF_16:
	    case SET_PT_MODEL_COEFF_17:
	    case SET_PT_MODEL_COEFF_18:
	    case SET_PT_MODEL_COEFF_19:
	    case SET_PT_MODEL_COEFF_20:
	    case SET_PT_MODEL_COEFF_21:
	    case SET_PT_MODEL_COEFF_22:
	    case SET_PT_MODEL_COEFF_23:
	    case SET_PT_MODEL_COEFF_24:
	    case SET_PT_MODEL_COEFF_25:
	    case SET_PT_MODEL_COEFF_26:
	    case SET_PT_MODEL_COEFF_27:
	    case SET_PT_MODEL_COEFF_28:
	    case SET_PT_MODEL_COEFF_29:
	    case SET_PT_MODEL_COEFF_30:
	    case SET_PT_MODEL_COEFF_31:
	    case SET_SUBREF_PT_COEFF_0:
	    case SET_SUBREF_PT_COEFF_1:
	    case SET_SUBREF_PT_COEFF_2:
	    case SET_SUBREF_PT_COEFF_3:
	    case SET_SUBREF_PT_COEFF_4:
	    case SET_SUBREF_PT_COEFF_5:
	    case SET_SUBREF_PT_COEFF_6:
	    case SET_SUBREF_PT_COEFF_7:
	    case SET_SUBREF_PT_COEFF_8:
	    case SET_SUBREF_PT_COEFF_9:
	    case SET_SUBREF_PT_COEFF_10:
	    case SET_SUBREF_PT_COEFF_11:
	    case SET_SUBREF_PT_COEFF_12:
	    case SET_SUBREF_PT_COEFF_13:
	    case SET_SUBREF_PT_COEFF_14:
	    case SET_SUBREF_PT_COEFF_15:
	    case SET_SUBREF_PT_COEFF_16:
	    case SET_SUBREF_PT_COEFF_17:
	    case SET_SUBREF_PT_COEFF_18:
	    case SET_SUBREF_PT_COEFF_19:
	    case SET_SUBREF_PT_COEFF_20:
	    case SET_SUBREF_PT_COEFF_21:
	    case SET_SUBREF_PT_COEFF_22:
	    case SET_SUBREF_PT_COEFF_23:
	    case SET_SUBREF_PT_COEFF_24:
	    case SET_SUBREF_PT_COEFF_25:
	    case SET_SUBREF_PT_COEFF_26:
	    case SET_SUBREF_PT_COEFF_27:
	    case SET_SUBREF_PT_COEFF_28:
	    case SET_SUBREF_PT_COEFF_29:
	    case SET_SUBREF_PT_COEFF_30:
	    case SET_SUBREF_PT_COEFF_31:
	    case SET_METR_COEFF_0:
	    case SET_METR_COEFF_1:
	    case SET_METR_COEFF_2:
	    case SET_METR_COEFF_3:
	    case SET_METR_COEFF_4:
	    case SET_METR_COEFF_5:
	    case SET_METR_COEFF_6:
	    case SET_METR_COEFF_7:
	    case SET_METR_COEFF_8:
	    case SET_METR_COEFF_9:
	    case SET_METR_COEFF_10:
	    case SET_METR_COEFF_11:
	    case SET_METR_COEFF_12:
	    case SET_METR_COEFF_13:
	    case SET_METR_COEFF_14:
	    case SET_METR_COEFF_15:
	    case SET_METR_COEFF_16:
	    case SET_METR_COEFF_17:
	    case SET_METR_COEFF_18:
	    case SET_METR_COEFF_19:
	    case SET_METR_COEFF_20:
	    case SET_METR_COEFF_21:
	    case SET_METR_COEFF_22:
	    case SET_METR_COEFF_23:
	    case SET_METR_COEFF_24:
	    case SET_METR_COEFF_25:
	    case SET_METR_COEFF_26:
	    case SET_METR_COEFF_27:
	    case SET_METR_COEFF_28:
	    case SET_METR_COEFF_29:
	    case SET_METR_COEFF_30:
	    case SET_METR_COEFF_31:
	    {
	    std::istringstream tmp;
	    if(arguments.find(",",0) != string::npos)
		tmp.str(arguments.substr(0,arguments.find(",",0)));
	    else
		tmp.str(arguments);
	    double value(0);
	    tmp >> value;
	    point.dataLength = sizeof(double);
	    point.data.Double = value;
	    // swap bytes
	    char tmp1 = point.data.Char[0]; 
	    point.data.Char[0] = point.data.Char[7];
	    point.data.Char[7] = tmp1;
	    tmp1 = point.data.Char[1]; point.data.Char[1] = point.data.Char[6];
	    point.data.Char[6] = tmp1;
	    tmp1 = point.data.Char[2]; point.data.Char[2] = point.data.Char[5];
	    point.data.Char[5] = tmp1;
	    tmp1 = point.data.Char[3]; point.data.Char[3] = point.data.Char[4];
	    point.data.Char[4] = tmp1;
            /*
	    std::cout << "CONTROL DAT : " << point.data.Double << "," 
		      << " sent" << std::endl;
            */
	    std::cout << "CONTROL DAT : " << value << "," 
		      << " sent" << std::endl;
	    }
	    break;
	    case SET_IDLE_STOW_TIME:
	    {
	    std::istringstream tmp;
	    if(arguments.find(",",0) != string::npos)
		tmp.str(arguments.substr(0,arguments.find(",",0)));
	    else
		tmp.str(arguments);
	    unsigned short int value(0);
	    tmp >> value;
	    point.dataLength = sizeof(unsigned short int);
	    point.data.UShort[0] = value;
	    // swap data
	    char tmp1 = point.data.Char[0]; 
	    point.data.Char[0]= point.data.Char[1];
	    point.data.Char[1] = tmp1;
	    std::cout << "CONTROL DAT : " << point.data.UShort[0] << "," 
		      << " sent" << std::endl;
	    }
	    break;
	    case SET_IP_ADDRESS:
	    {
	    string ip;
	    string netmask;
	    unsigned int position1 = arguments.find(",",0);
	    if(position1 != string::npos)
		{
		ip = arguments.substr(0,position1);
		unsigned int position2 = arguments.find(",",position1+1);
		if(position2 != string::npos)
		    {
		    string arguments1 = arguments.substr(position1+1,arguments.length());
		    netmask = arguments1.substr(0,arguments1.find(",",0));
		    }
		else
		    netmask = arguments.substr(position1+1,arguments.length());
		}
	    else
		{
		std::cout << "WARNING   : 0x" << std::hex << RCA << std::dec 
			  << " incomplete set of arguments. Discarting it." << std::endl;
		point.rca = 0x00;
		abortControl = true;
		}
	    
	    std::istringstream tmp[8];
	    unsigned int index(0U);
	    unsigned int position = ip.find(".",0);
	    while(position != string::npos && index < 3U)
		{
		position = ip.find(".",0);
		tmp[index].str(ip.substr(0,position));
		index++; ip = ip.substr(position+1,ip.length());
		}
	    position = ip.find(".",0);
	    if(position != string::npos)
		tmp[index].str(ip.substr(0,position));
	    else
		tmp[index].str(ip);
	    index++;
	    if(index != 4)
		{
		std::cout << "WARNING   : 0x" << std::hex << RCA << std::dec 
			  << " incomplete set of arguments. Discarting it." << std::endl;
		point.rca = 0x00;
		abortControl = true;
		}
	    position = netmask.find(".",0);
	    while(position!=string::npos && index < 8)
		{
		position = netmask.find(".",0);
		tmp[index].str(netmask.substr(0,position));
		index++; netmask = netmask.substr(position+1,netmask.length());
		}
	    position = netmask.find(".",0);
            /*
	    if(position != string::npos) 
		tmp[index].str(netmask.substr(0,position));
	    else 
		tmp[index].str(netmask);
	    index++;
            */
	    if(index != 8)
		{
		std::cout << "WARNING   : 0x" << std::hex << RCA << std::dec 
			  << " incomplete set of arguments. Discarting it." << std::endl;
		point.rca = 0x00;
		abortControl = true;
		}
	    if(!abortControl)
		{
		unsigned short value[8];
		for(unsigned short idx=0; idx<8; idx++) {
		    tmp[idx] >> value[idx];
                }
		point.dataLength = 8;
		for(unsigned short idx=0; idx<8; idx++)
		    point.data.Char[idx] = (char) value[idx];
	    std::cout << "CONTROL DAT : " 
		<< ((unsigned short) point.data.Char[0] & 0xff) << "." 
		<< ((unsigned short) point.data.Char[1] & 0xff) << "." 
		<< ((unsigned short) point.data.Char[2] & 0xff) << "." 
		<< ((unsigned short) point.data.Char[3] & 0xff) << ", " 
		<< ((unsigned short) point.data.Char[4] & 0xff) << "." 
		<< ((unsigned short) point.data.Char[5] & 0xff) << "." 
		<< ((unsigned short) point.data.Char[6] & 0xff) << "." 
		<< ((unsigned short) point.data.Char[7] & 0xff)  
		<< " sent" << std::endl;
		}
	    }	    
	    break;
	    case SET_IP_GATEWAY:
	    {
	    string gateway;
	    if(arguments.find(",",0) != string::npos)
		gateway = arguments.substr(0,arguments.find(",",0));
	    else
		gateway = arguments;

	      std::istringstream tmp[4];
	    unsigned int  index(0U);
	    unsigned int  position = gateway.find(".",0);
	    while(position!=string::npos && index < 3)
		{
		position = gateway.find(".",0);
		tmp[index].str(gateway.substr(0,position));
		index++; gateway = gateway.substr(position+1,gateway.length());
		}
	    position = gateway.find(".",0);
	    if(position != string::npos)
		tmp[index].str(gateway.substr(0,position));
	    else
		tmp[index].str(gateway);
	    index++;
	    if(index != 4)
		{
		std::cout << "WARNING   : 0x" << std::hex << RCA << std::dec 
			  << " incomplete set of arguments. Discarting it." << std::endl;
		point.rca = 0x00;
		abortControl = true;
		}
	    if(!abortControl)
		{
		unsigned short value[4];
		for(unsigned short idx=0; idx<4; idx++)
		    tmp[idx] >> value[idx];
		point.dataLength = 4;
		for(unsigned short idx=0; idx<4; idx++)
		    point.data.Char[idx] = (char) value[idx];
		std::cout << "CONTROL DAT : " 
		    << ((unsigned short) point.data.Char[0] & 0xff) << "." 
		    << ((unsigned short) point.data.Char[1] & 0xff) << "." 
		    << ((unsigned short) point.data.Char[2] & 0xff) << "." 
		    << ((unsigned short) point.data.Char[3] & 0xff)  
		    << " sent" << std::endl;
		}
	    }
	    break;
	    case SET_STOW_PIN:
	    {
	    std::istringstream tmp[2];
	    unsigned int position1 = arguments.find(",",0);
	    if(position1 != string::npos)
		{
		tmp[0].str(arguments.substr(0,position1));
		unsigned int position2 = arguments.find(",",position1+1);
		if(position2 != string::npos)
		    {
		    string arguments1 = arguments.substr(position1+1,arguments.length());
		    tmp[1].str(arguments1.substr(0,arguments1.find(",",0)));
		    }
		else
		    tmp[1].str(arguments.substr(position1+1,arguments.length()));
		}
	    else
		{
		std::cout << "WARNING   : 0x" << std::hex << RCA << std::dec 
			  << " incomplete set of arguments. Discarting it." << std::endl;
		point.rca = 0x00;
		abortControl = true;
		}
	    if(!abortControl)
		{
		unsigned short value1[2];
		for(unsigned short index=0; index<2; index++)
		    tmp[index] >> value1[index];
		union 
		{
		    char Char[2];
		    unsigned short Short;
		} value;
		for(unsigned short index=0; index<2; index++)
		    value.Char[index]=(char)value1[index];
		point.dataLength = 2;
		point.data.Short[0] = value.Short;
		}
	    
	    if(point.rca!=0x00)
		std::cout << "CONTROL DAT : " << (unsigned short) point.data.Char[0]  << " "
			  << (unsigned short) point.data.Char[1]
			  << " sent" << std::endl;
	    }
	    break;
	    case SET_SUBREF_ABS_POSN:
	    case SET_SUBREF_DELTA_POSN:
	    case SET_SUBREF_ROTATION:
	    {
	    string modes = arguments;
	    std::istringstream tmp[3];
	    unsigned int  index(0U);
	    unsigned int  position = modes.find(",",0);
	    while(position!=string::npos && index < 2)
		{
		position = modes.find(",",0);
		tmp[index].str(modes.substr(0,position));
		index++; modes = modes.substr(position+1,modes.length());
		}
	    position = modes.find(",",0);
	    if(position != string::npos)
		tmp[index].str(modes.substr(0,position));
	    else
		tmp[index].str(modes);
	    index++;
	    if(index != 3)
		{
		std::cout << "WARNING   : 0x" << std::hex << RCA << std::dec 
			  << " incomplete set of arguments. Discarting it." << std::endl;
		point.rca = 0x00;
		abortControl = true;
		}
	    if(!abortControl)
		{
		short value[3];
		for(unsigned short index=0; index < 3; index++)
		    tmp[index] >> value[index];
		point.dataLength = 6;
		point.data.Short[0] = value[0];
		point.data.Short[1] = value[1];
		point.data.Short[2] = value[2];
		// swap data
		char tmp1 = point.data.Char[0]; 
		point.data.Char[0]= point.data.Char[1];
		point.data.Char[1] = tmp1;
		tmp1 = point.data.Char[2]; 
		point.data.Char[2]= point.data.Char[3];
		point.data.Char[3] = tmp1;
		tmp1 = point.data.Char[4]; 
		point.data.Char[4]= point.data.Char[5];
		point.data.Char[5] = tmp1;

	    if(point.rca!=0x00)
                /*
		std::cout << "CONTROL DAT : " 
			  << point.data.Short[0]  << " "
			  << point.data.Short[1]  << " "
			  << point.data.Short[2]
			  << " sent" << std::endl;
                */
		std::cout << "CONTROL DAT : " 
			  << value[0]  << " "
			  << value[1]  << " "
			  << value[2]
			  << " sent" << std::endl;
		}
	    }
	    break;
	    case SET_METR_MODE:
	    {
	    string modes = arguments;
	    std::istringstream tmp[7];
	    unsigned int  index(0U);
	    unsigned int  position = modes.find(",",0);
	    while(position!=string::npos && index < 6)
		{
		position = modes.find(",",0);
		tmp[index].str(modes.substr(0,position));
		index++; modes = modes.substr(position+1,modes.length());
		}
	    position = modes.find(",",0);
	    if(position != string::npos)
		tmp[index].str(modes.substr(0,position));
	    else
		tmp[index].str(modes);
	    index++;
	    if(index != 7)
		{
		std::cout << "WARNING   : 0x" << std::hex << RCA << std::dec 
			  << " incomplete set of arguments. Discarting it." << std::endl;
		point.rca = 0x00;
		abortControl = true;
		}
	    if(!abortControl)
		{
		unsigned short value[8];
		for(unsigned short index=0; index < 7; index++)
		    {
		    tmp[index] >> value[index];
		    if(value[index] != 0)
			value[index]=1;
		    }
		unsigned short byte(0U);
		byte = value[0] 
		    + (value[1]<<1)
		    + (value[2]<<2)
		    + (value[3]<<3)
		    + (value[4]<<4)
		    + (value[5]<<5)
		    + (value[6]<<6);
		point.dataLength = 4;
		point.data.Char[0] = (char) byte;
		point.data.Char[1] = (char) 0U;
		point.data.Char[2] = (char) 0U;
		point.data.Char[3] = (char) 0U;
		}
	    if(point.rca!=0x00)
		std::cout << "CONTROL DAT : " 
			  << std::hex <<(unsigned short) point.data.Char[0] << std::dec
			  << " sent" << std::endl;
	    }
	    break;
	    default:
	    {
	    std::istringstream tmp[2];
	    unsigned int position1 = arguments.find(",",0);
	    if(position1 != string::npos)
		{
		tmp[0].str(arguments.substr(0,position1));
		unsigned int position2 = arguments.find(",",position1+1);
		if(position2 != string::npos)
		    {
		    string arguments1 = arguments.substr(position1+1,arguments.length());
		    tmp[1].str(arguments1.substr(0,arguments1.find(",",0)));
		    }
		else
		    tmp[1].str(arguments.substr(position1+1,arguments.length()));
		}
	    else
		{
		std::cout << "WARNING   : 0x" << std::hex << RCA << std::dec 
			  << " incomplete set of arguments. Discarting it." << std::endl;
		point.rca = 0x00;
		abortControl = true;
		}
	    tmp[0] >> point.dataLength;
	    string data;
	    unsigned short byte;
	    tmp[1] >> data;
	    for(unsigned short index=0; index <8; index++)
		{
		byte = atoi(data.substr(2+index,1).c_str());
		point.data.Char[index] = (char) byte;
		}
	    
	    std::cout << "CONTROL DAT : " 
		      << std::hex 
		      <<(unsigned short) point.data.Char[0] << " "
		      <<(unsigned short) point.data.Char[1] << " "
		      <<(unsigned short) point.data.Char[2] << " "
		      <<(unsigned short) point.data.Char[3] << " "
		      <<(unsigned short) point.data.Char[4] << " "
		      <<(unsigned short) point.data.Char[5] << " "
		      <<(unsigned short) point.data.Char[6] << " "
		      <<(unsigned short) point.data.Char[7] << " "
		      << std::dec
		      << " sent" << std::endl;
	    }
	    break;
	    }
	if(!abortControl)
	    mc.sendControl(point);
	if(point.status==0 && !abortControl)
	    std::cout << "CONTROL 0x" << std::hex << point.rca << std::dec << " OK." << std::endl;
	else
	    if(point.rca != 0x00)
		{
		std::cout << "CONTROL 0x" << std::hex << point.rca << std::dec 
			  << " FAILED. Status: " << point.status << std::endl;
		}
	}
}
