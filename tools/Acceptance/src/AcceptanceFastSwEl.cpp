// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2006
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA


#include <AcceptanceMountInterface.h> 
#include <ace/Semaphore.h> // for sem_t
#include <ambDefs.h> // For AMBERR defs
#include <acstimeEpochHelper.h> // for EpochHelper
#include <acstimeDurationHelper.h> // for DurationHelper
#include <TETimeUtil.h> // for TETimeUtil
#include <unistd.h> // for usleep
#include <deque> // for deque
#include <iomanip> // for std::setprecision and std::fixed
#include <iostream> // for std::cout and std::endl
#include <ace/Get_Opt.h>
#include <sstream>
#include <stdlib.h>
#include <map>
#include <math.h>

using std::string;
using std::map;

const static double minimumDuration(0.001);
const static double defaultDuration(10.0);
const static double defaultStepDuration(2.0);
const static unsigned short int defaultChannel(2U);
const static unsigned short int defaultNodeId(0U);
const static unsigned short int defaultNumberRequests(40U);
const static unsigned int defaultReplyTolerance(5);

double duration(defaultDuration);
double stepDuration(defaultStepDuration);
unsigned short int channel(defaultChannel);
unsigned short int nodeId(defaultNodeId);
unsigned short int requests(defaultNumberRequests);
unsigned int replyTolerance(defaultReplyTolerance);

std::vector< AmbRelativeAddr > monitorRCA;
std::vector< string > monitorMnemonic;
std::vector< string > controlRCA;
std::vector< string > controlMnemonic;
bool exitProgram(false);
map<string,AmbRelativeAddr> monitorMap;
map<string,AmbRelativeAddr> controlMap;

const ACS::TimeInterval loopTime(TETimeUtil::TE_PERIOD_DURATION.value);
// time format string.
const std::string timeFmt("%H:%M:%S.%3q");


#include "subs.h"

int main(int argc, char* argv[])
{

  if ((argc < 2) || (argc > 3))
	{
	std::cout << "Too few or too many arguments. "
                  << "Usage: "
                  << argv[0] << " "
                  << "pos,vel,acc,step "
                  << "[duration (seconds),step duration(seconds)]"
	          << std::endl;
	exit(-1);
	}

  float pos, vel, acc, step;
  sscanf(argv[1], "%f,%f,%f,%f", &pos, &vel, &acc, &step);
  if (argc == 3)
    {
      float dur, stepDur;
      sscanf(argv[2], "%f,%f", &dur, &stepDur);
      if (dur < minimumDuration)
	{
	  dur = minimumDuration;
	}
      if (stepDur < minimumDuration)
	{
	  stepDur = minimumDuration;
	}

      duration = dur;
      stepDuration = stepDur;
    }
  std::cout << "Track to "
	    << pos << " deg, velocity "
	    << vel << " deg/s, acceleration "
	    << acc << " deg/s*s, step "
            << step << " deg, duration "
	    << duration << " seconds, step duration "
            << stepDuration << "seconds"
	    << std::endl;
  
  BareAMBDeviceInt mc;
  if (mc.initialize(channel, nodeId) == false)
    {
      return -ENODEV;
    }

  BareAMBDeviceInt::ICDPoint point, monitorPoint;

  // Check axis mode
  monitorPoint.rca = 0x22;
  monitorPoint.status = AMBERR_PENDING;
  mc.sendMonitor(monitorPoint);
  if(monitorPoint.status!=0)
    {
      std::cout << "Monitor ACU_MODE_RSP" 
		<<" FAILED. Status: " << monitorPoint.status << std::endl;
    }

  if (monitorPoint.data.Char[1] != 0x2)
    {
      std::cout << "ACU not in remote mode, exiting" << std:: endl;
      exit(-1);
    }
  if ((monitorPoint.data.Char[0] & 0xf0) != 0x20)
    {
      std::cout << "Axis not in encoder mode" << std::endl;
      
      if ((monitorPoint.data.Char[0] & 0xf0) != 0x10)
	{
	  std::cout << "Setting axis to standby mode"  << std::endl;
	  
	  // Set standby mode
	  point.rca = 0x1022;
	  point.data.Char[0] = 0x10;
	  point.dataLength = 1;
	  mc.sendControl(point);
	  if (point.status != 0)
	    {
	      std::cout << "Failed to send ACU mode command, status: " 
			<< point.status << std:: endl;
	    }
	  sleep(1);

	  // Check axis mode
	  bool stateReached(false);
	  int loopCnt = 0;
	  do
	    {
	      monitorPoint.rca = 0x22;
	      monitorPoint.status = AMBERR_PENDING;
	      mc.sendMonitor(monitorPoint);
	      if(monitorPoint.status!=0)
		{
		  std::cout << "Monitor ACU_MODE_RSP" 
			    <<" FAILED. Status: " << monitorPoint.status 
			    << std::endl;
		}
	      
	      if (monitorPoint.data.Char[1] != 0x2)
		{
		  std::cout << "ACU not in remote mode, exiting" << std:: endl;
		  exit(-1);
		}
	      if ((monitorPoint.data.Char[0] & 0xf0) == 0x10)
		{
		  stateReached = true;
		}
	      loopCnt++;
	      if (loopCnt > 30)
		{
		  std::cout << "Failed to set ACU to standby mode, exiting" 
			    << std:: endl;
		  exit(-1);
		}
	      sleep(1);
	    }
	  while (stateReached == false);
	}

      // Set encoder mode
      point.rca = 0x1022;
      point.data.Char[0] = 0x20;
      point.dataLength = 1;
      mc.sendControl(point);
      if (point.status != 0)
	{
	  std::cout << "Failed to send ACU mode command, status: " 
		    << point.status << std:: endl;
	}
      sleep(1);
      
      // Check axis mode
      bool stateReached(false);
      int loopCnt = 0;
      do
	{
	  monitorPoint.rca = 0x22;
	  monitorPoint.status = AMBERR_PENDING;
	  mc.sendMonitor(monitorPoint);
	  if(monitorPoint.status!=0)
	    {
	      std::cout << "Monitor ACU_MODE_RSP" 
			<<" FAILED. Status: " << monitorPoint.status 
			<< std::endl;
	    }
	  
	  if (monitorPoint.data.Char[1] != 0x2)
	    {
	      std::cout << "ACU not in remote mode, exiting" << std:: endl;
	      exit(-1);
	    }
	  if ((monitorPoint.data.Char[0] & 0xf0) == 0x20)
	    {
	      stateReached = true;
	    }
	  loopCnt++;
	  if (loopCnt > 30)
	    {
	      std::cout << "Failed to set ACU to encoder mode, exiting" 
			<< std:: endl;
	      exit(-1);
	    }
	  sleep(1);
	}
      while (stateReached == false);
    }

  EpochHelper timeNow(TETimeUtil::ceilTE(
					 TimeUtil::ace2epoch(ACE_OS::gettimeofday())));
  
  EpochHelper lastScheduledTime(timeNow.value());
  lastScheduledTime.add(DurationHelper(loopTime * 2).value());
  
  EpochHelper stopTime(lastScheduledTime.value());
  stopTime.add(DurationHelper(static_cast< long double >(duration)).value());
  
  EpochHelper stepTime(timeNow.value());
  stepTime.add(DurationHelper(static_cast< long double >(stepDuration)).value());

  EpochHelper horizon(timeNow.value());
  horizon.add(DurationHelper(loopTime).value());
  bool doExit(false);
  bool stepActive(false);

  float posIncr = vel * 0.048;
  float velIncr;
  
  int countCmd = 0;
  int stepCount = 0;
  int errorVector[0x16];
  
  for (int i = 0; i < 0x16; i++)
    errorVector[i] = 0;

  // empty the error stack
  do
    {
      monitorPoint.rca = 0x2f;
      monitorPoint.status = AMBERR_PENDING;
      
      mc.sendMonitor(monitorPoint);
      if(monitorPoint.status!=0)
	{
	  std::cout << "Monitor GET_ACU_ERROR" 
		    << " FAILED. Status: " << monitorPoint.status 
		    << std::endl;
	}
    }
  while (monitorPoint.dataLength != 0);

#ifdef POSN_DUMP
  BareAMBDeviceInt::ICDPoint ElPosnPoint;
  ElPosnPoint.rca = 0x02;
#endif

  point.rca = 0x1002;
  point.dataLength = 8;
  monitorPoint.rca = 0x2f;
  do
    {	
      if((horizon >= stopTime.value()))
	{
	  doExit = true;
	}

      if ((horizon >= stepTime.value()))
	{
	  if (stepActive == false)
	    {
	      pos += step;
	      stepActive = true;
	    }
	  else
	    {
	      pos -= step;
	      stepActive = false;
	    }
	  stepTime.add(DurationHelper(static_cast< long double >(stepDuration)).value());
	  stepCount++;
	}

      point.data.Long[0] = (long)roundf((pos/360.0) * pow(2,31));
      point.data.Long[1] = (long)roundf((vel/360.0) * pow(2,31));
      posIncr = vel * 0.048;
      velIncr = acc * 0.048;

      // swap data
      myntohl(point.data.Long[0]);
      myntohl(point.data.Long[1]);

      ACS::Time TargetTime = lastScheduledTime.value().value;

      mc.sendControlTE(TargetTime, point);
      if (point.status != 0)
	{
	  std::cout << "Failed to send az trajectory command, status: " 
		    << point.status << std:: endl;
	}

      // Get the error stack
      monitorPoint.status = AMBERR_PENDING;

      mc.sendMonitorTE(TargetTime, monitorPoint);
      if(monitorPoint.status!=0)
	{
	  std::cout << "Monitor GET_ACU_ERROR" 
		    <<" FAILED. Status: " << monitorPoint.status << std::endl;
	}

      if (monitorPoint.dataLength > 0)
	{
	  int errorCode = monitorPoint.data.Char[0];
	  errorVector[errorCode]++;
	}

#ifdef POSN_DUMP
      ACS::Time TargetTimeM = TargetTime + TETimeUtil::TE_PERIOD_DURATION.value / 2;
      // Get El Position
      ElPosnPoint.status = AMBERR_PENDING;
      mc.sendMonitorTE(TargetTimeM, ElPosnPoint);
      if (ElPosnPoint.status)
       {
         std::cout << "Monitor EL_POSN_RSP"
                   <<" FAILED. Status: " << ElPosnPoint.status << std::endl;
       }

      std::cout << TargetTime << ": ";
      std::cout << "ELtraj " << TrajDump(&point.data) << ", ";
      std::cout << "ELposn " << PosnDump(&ElPosnPoint.data);
      std::cout << std::endl;
#endif

      horizon.value(
		    TETimeUtil::ceilTE(TimeUtil::ace2epoch(ACE_OS::gettimeofday())));
      horizon.add(DurationHelper(loopTime).value());
      //	usleep(static_cast<useconds_t>(DurationHelper(loopTime).toSeconds() * 1E6));
      lastScheduledTime.add(DurationHelper(loopTime).value());
      pos += posIncr;
      vel += velIncr;
      countCmd++;
    }
  while (doExit == false);

  // Send references with 0 speed
  ACS::Time TargetTime = lastScheduledTime.value().value;
  point.data.Long[1] = 0;
  mc.sendControlTE(TargetTime, point);
  if (point.status != 0)
    {
      std::cout << "Failed to send az trajectory command, status: " 
		<< point.status << std:: endl;
    }
  
  // Get the error stack
  monitorPoint.status = AMBERR_PENDING;

  mc.sendMonitorTE(TargetTime, monitorPoint);
  if(monitorPoint.status!=0)
    {
      std::cout << "Monitor GET_ACU_ERROR" 
		<<" FAILED. Status: " << monitorPoint.status << std::endl;
    }

  if (monitorPoint.dataLength > 0)
    {
      int errorCode = monitorPoint.data.Char[0];
      errorVector[errorCode]++;
    }

  std::cout << "Sent " << countCmd << " trajectory commands" << std::endl;
  std::cout << "Performed " << stepCount << " steps" << std::endl;
  for (int i = 0; i < 0x16; i++)
    if (errorVector[i] > 0)
      std::cout << "ACU error 0x" << std::hex << i << std::dec << " occured " 
		<< errorVector[i] << " times" << std::endl;
}


