// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2006
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA


#include <AcceptanceMountInterface.h> 
#include <ace/Semaphore.h> // for sem_t
#include <ambDefs.h> // For AMBERR defs
#include <acstimeEpochHelper.h> // for EpochHelper
#include <acstimeDurationHelper.h> // for DurationHelper
#include <TETimeUtil.h> // for TETimeUtil
#include <unistd.h> // for usleep
#include <deque> // for deque
#include <iomanip> // for std::setprecision and std::fixed
#include <iostream> // for std::cout and std::endl
#include <ace/Get_Opt.h>
#include <sstream>
#include <stdlib.h>
#include <map>
#include <math.h>

using std::string;
using std::map;

const static double minimumDuration(0.001);
const static double defaultDuration(10.0);
const static unsigned short int defaultChannel(2U);
const static unsigned short int defaultNodeId(0U);
const static unsigned short int defaultNumberRequests(40U);
const static unsigned int defaultReplyTolerance(5);

double duration(defaultDuration);
unsigned short int channel(defaultChannel);
unsigned short int nodeId(defaultNodeId);
unsigned short int requests(defaultNumberRequests);
unsigned int replyTolerance(defaultReplyTolerance);

std::vector< AmbRelativeAddr > monitorRCA;
std::vector< string > monitorMnemonic;
std::vector< string > controlRCA;
std::vector< string > controlMnemonic;
bool exitProgram(false);
map<string,AmbRelativeAddr> monitorMap;
map<string,AmbRelativeAddr> controlMap;

// This is how often the monitor loop runs (once every 20 TE's).
const ACS::TimeInterval loopTime(20 * TETimeUtil::TE_PERIOD_DURATION.value);
// time format string.
const std::string timeFmt("%H:%M:%S.%3q");

#include "subs.h"

int main(int argc, char* argv[])
{

  if ((argc < 3) || (argc > 4))
    {
      std::cout << "Too few or too many arguments. "
		<< "Usage: "
		<< argv[0] << " "
		<< "azPos,azVel,azAcc elPos,elVel,elAcc [duration (seconds)]"
		<< std::endl;
      exit(-1);
    }

  float azPos, azVel, azAcc;
  sscanf(argv[1], "%f,%f,%f", &azPos, &azVel, &azAcc);
  float elPos, elVel, elAcc;
  sscanf(argv[2], "%f,%f,%f", &elPos, &elVel, &elAcc);
  if (argc == 4)
    {
      float dur;
      sscanf(argv[3], "%f", &dur);
      if (dur < minimumDuration)
	{
	  dur = minimumDuration;
	}
      duration = dur;
    }
  std::cout << "Track to "
	    << azPos << "," << elPos << " deg, velocity "
	    << azVel << "," << elVel << " deg/s, acceleration "
	    << azAcc << "," << elAcc << " deg/s*s duration "
	    << duration << " seconds"
	    << std::endl;
  
  BareAMBDeviceInt mc;
  if (mc.initialize(channel, nodeId) == false)
    {
      return -ENODEV;
    }

  BareAMBDeviceInt::ICDPoint point, pointAz, pointEl, monitorPoint;

  // Check axis mode
  monitorPoint.rca = 0x22;
  monitorPoint.status = AMBERR_PENDING;
  mc.sendMonitor(monitorPoint);
  if(monitorPoint.status!=0)
    {
      std::cout << "Monitor ACU_MODE_RSP" 
		<<" FAILED. Status: " << monitorPoint.status << std::endl;
    }

  if (monitorPoint.data.Char[1] != 0x2)
    {
      std::cout << "ACU not in remote mode, exiting" << std:: endl;
      exit(-1);
    }

//
// Set the axes to encoder mode
//
  if (monitorPoint.data.Char[0] != 0x22)
    {
      std::cout << "Axis not in encoder mode" << std::endl;
      
      if (monitorPoint.data.Char[0] != 0x11)
	{
	  std::cout << "Setting axis to standby mode"  << std::endl;
	  
	  // Set standby mode
	  point.rca = 0x1022;
	  point.data.Char[0] = 0x11;
	  point.dataLength = 1;
	  mc.sendControl(point);
	  if (point.status != 0)
	    {
	      std::cout << "Failed to send ACU mode command, status: " 
			<< point.status << std:: endl;
	    }
	  sleep(1);
	  
	  // Check axis mode
	  bool stateReached(false);
	  int loopCnt = 0;
	  do
	    {
	      monitorPoint.rca = 0x22;
	      monitorPoint.status = AMBERR_PENDING;
	      mc.sendMonitor(monitorPoint);
	      if(monitorPoint.status!=0)
		{
		  std::cout << "Monitor ACU_MODE_RSP" 
			    <<" FAILED. Status: " << monitorPoint.status << std::endl;
		}
	      
	      if (monitorPoint.data.Char[1] != 0x2)
		{
		  std::cout << "ACU not in remote mode, exiting" << std:: endl;
		  exit(-1);
		}
	      if (monitorPoint.data.Char[0] == 0x11)
		{
		  stateReached = true;
		}
	      loopCnt++;
	      if (loopCnt > 30)
		{
		  std::cout << "Failed to set ACU to standby mode, exiting" 
			    << std:: endl;
		  exit(-1);
		}
	      sleep(1);
	    }
	  while (stateReached == false);

	}

      // Set encoder mode
      point.rca = 0x1022;
      point.data.Char[0] = 0x22;
      point.dataLength = 1;
      mc.sendControl(point);
      if (point.status != 0)
	{
	  std::cout << "Failed to send ACU mode command, status: " 
		    << point.status << std:: endl;
	}
      sleep(1);
      
      // Check axis mode
      bool stateReached(false);
      int loopCnt = 0;
      do
	{
	  monitorPoint.rca = 0x22;
	  monitorPoint.status = AMBERR_PENDING;
	  mc.sendMonitor(monitorPoint);
	  if(monitorPoint.status!=0)
	    {
	      std::cout << "Monitor ACU_MODE_RSP" 
			<<" FAILED. Status: " << monitorPoint.status 
			<< std::endl;
	    }

	  if (monitorPoint.data.Char[1] != 0x2)
	    {
	      std::cout << "ACU not in remote mode, exiting" << std:: endl;
	      exit(-1);
	    }
	  if (monitorPoint.data.Char[0] == 0x22)
	    {
	      stateReached = true;
	    }
	  loopCnt++;
	  if (loopCnt > 30)
	    {
	      std::cout << "Failed to set ACU to encoder mode, exiting" 
			<< std:: endl;
	      exit(-1);
	    }
	  sleep(1);
	}
      while (stateReached == false);
    }

  // Set track mode
  point.rca = 0x1020;
  if ((azVel == 0.0) && (azAcc == 0.0) && (elVel == 0.0) && (elAcc == 0.0))
      {
      point.data.Char[0] = 0x1;  // slewing
      }
  else
      {
      point.data.Char[0] = 0x0;  // continous sidereal tracking
      }
  point.data.Char[0] = 0x2;
  point.dataLength = 1;
  mc.sendControl(point);
  if (point.status != 0)
      {
      std::cout << "Failed to send ACU track mode command, status: " 
		<< point.status << std:: endl;
      }
  sleep(1);

  EpochHelper timeNow(TETimeUtil::ceilTE(
					 TimeUtil::ace2epoch(ACE_OS::gettimeofday())));
    
  EpochHelper lastScheduledTime(timeNow.value());
  lastScheduledTime.add(DurationHelper(loopTime).value());
  
  EpochHelper stopTime(lastScheduledTime.value());
  stopTime.add(DurationHelper(static_cast< long double >(duration)).value());
  
  EpochHelper horizon(timeNow.value());
  horizon.add(DurationHelper(loopTime * 2).value());
  bool doExit(false);
  std::deque< BareAMBDeviceInt::RequestStruct > monitorQueue;
  std::deque< BareAMBDeviceInt::RequestStruct > commandQueue;

  int failure(0);
  int late_reply(0);
  int success(0);
  int tooLate(0);
  int failure_cmd(0);
  int late_reply_cmd(0);
  int success_cmd(0);
  int tooLate_cmd(0);
  unsigned int failure_per_amberrorcode[13];
  for(int index=0;index<13;index++)
      failure_per_amberrorcode[index]=0;

  double minTime(1E30);
  double maxTime(-1E30);
  double minTime_cmd(1E30);
  double maxTime_cmd(-1E30);

  float azPosIncr = azVel * 0.048;
  float azVelIncr;
  float elPosIncr = elVel * 0.048;
  float elVelIncr;

  int countCmd = 0;
  int countMSent = 0;
  
  // empty the error stack
  do
    {
      monitorPoint.rca = 0x2f;
      monitorPoint.status = AMBERR_PENDING;
      
      mc.sendMonitor(monitorPoint);
      if(monitorPoint.status!=0)
	{
	  std::cout << "Monitor GET_ACU_ERROR" 
		    << " FAILED. Status: " << monitorPoint.status 
		    << std::endl;
	}
    }
  while (monitorPoint.dataLength != 0);
  
  do
      {	
      BareAMBDeviceInt::RequestStruct azRequest, elRequest;
      if((horizon >= stopTime.value()))
	  {
	  doExit = true;
	  }

      ACS::Time lastTime;
      if (doExit == true)
	  {
	  lastTime = stopTime.value().value;
	  }
      else
	  {
	  lastTime = horizon.value().value;
	  }

      azRequest.TargetTime = lastScheduledTime.value().value;
      while (azRequest.TargetTime < lastTime)
	  {
	  azRequest.data.Long[0] = (long)roundf((azPos/360.0) * pow(2,31));
	  azRequest.data.Long[1] = (long)roundf((azVel/360.0) * pow(2,31));
	  azPosIncr = azVel * 0.048;
	  azVelIncr = azAcc * 0.048;

	  // swap data
          myntohl(azRequest.data.Long[0]);
          myntohl(azRequest.data.Long[1]);

	  azRequest.RCA = 0x1012;
	  azRequest.DataLength = 8;
	  azRequest.Status  = AMBERR_PENDING;
	  azRequest.Timestamp = 0;

	  commandQueue.push_back(azRequest);
	  // As the push_back function (above) will do a copy this gets a
	  // reference to the copy and this is needed as the monitorTE function
	  // uses the addresses.
	  BareAMBDeviceInt::RequestStruct& nextAzRequest(commandQueue.back());
	  mc.commandTE(nextAzRequest.TargetTime, nextAzRequest.RCA,
		       nextAzRequest.DataLength, nextAzRequest.data.Raw, NULL,
		       &nextAzRequest.Timestamp, &nextAzRequest.Status);
//	  std::cout << "AZ traj cmd sent at " << nextAzRequest.TargetTime
//	            << std::endl;
	  countCmd++;
	  azPos += azPosIncr;
	  azVel += azVelIncr;

	  elRequest.data.Long[0] = (long)roundf((elPos/360.0) * pow(2,31));
	  elRequest.data.Long[1] = (long)roundf((elVel/360.0) * pow(2,31));
	  elPosIncr = elVel * 0.048;
	  elVelIncr = elAcc * 0.048;
	  elRequest.TargetTime = azRequest.TargetTime;

	  // swap data
          myntohl(elRequest.data.Long[0]);
          myntohl(elRequest.data.Long[1]);

	  elRequest.RCA = 0x1002;
	  elRequest.DataLength = 8;
	  elRequest.Status  = AMBERR_PENDING;
	  elRequest.Timestamp = 0;

	  commandQueue.push_back(elRequest);
	  // As the push_back function (above) will do a copy this gets a
	  // reference to the copy and this is needed as the monitorTE function
	  // uses the addresses.
	  BareAMBDeviceInt::RequestStruct& nextElRequest(commandQueue.back());
	  mc.commandTE(nextElRequest.TargetTime, nextElRequest.RCA,
		       nextElRequest.DataLength, nextElRequest.data.Raw, NULL,
		       &nextElRequest.Timestamp, &nextElRequest.Status);
//      	  std::cout << "EL traj cmd sent at " << nextElRequest.TargetTime
//	            << std::endl;
	  elPos += elPosIncr;
	  elVel += elVelIncr;
#ifdef POSN_DUMP
          std::cout << azRequest.TargetTime << ": "
                    << " Az traj " << TrajDump(&azRequest.data)
                    << std::endl;
          std::cout << elRequest.TargetTime << ": "
                    << " El traj " << TrajDump(&elRequest.data)
                    << std::endl;
#endif
	  azRequest.TargetTime += TETimeUtil::TE_PERIOD_DURATION.value;
	  }

      // Get the position and error stack
      BareAMBDeviceInt::RequestStruct azPosRequest, elPosRequest, errRequest;
      // Need to schedule the monitor requests 24ms after the TE
      azPosRequest.TargetTime = lastScheduledTime.value().value +
	  TETimeUtil::TE_PERIOD_DURATION.value / 2;
      azPosRequest.Status = AMBERR_PENDING;
      azPosRequest.DataLength = 8;
      azPosRequest.RCA = 0x12;
      azPosRequest.Timestamp = 0;
      elPosRequest.TargetTime = azPosRequest.TargetTime;
      elPosRequest.Status = AMBERR_PENDING;
      elPosRequest.DataLength = 8;
      elPosRequest.RCA = 0x02;
      elPosRequest.Timestamp = 0;
      errRequest.TargetTime = azPosRequest.TargetTime;
      errRequest.Status = AMBERR_PENDING;
      errRequest.DataLength = 8;
      errRequest.RCA = 0x2f;
      errRequest.Timestamp = 0;

      while (azPosRequest.TargetTime < lastTime)
	  {
	  monitorQueue.push_back(azPosRequest);
	  // As the push_back function (above) will do a copy this gets a
	  // reference to the copy and this is needed as the monitorTE function
	  // uses the addresses.
	  {
	  BareAMBDeviceInt::RequestStruct& nextRequest(monitorQueue.back());
	  mc.monitorTE(nextRequest.TargetTime, nextRequest.RCA,
		       nextRequest.DataLength, nextRequest.data.Raw, NULL,
		       &nextRequest.Timestamp, &nextRequest.Status);
	  countMSent++;
	  }
	  monitorQueue.push_back(elPosRequest);
	  // As the push_back function (above) will do a copy this gets a
	  // reference to the copy and this is needed as the monitorTE function
	  // uses the addresses.
	  {
	  BareAMBDeviceInt::RequestStruct& nextRequest(monitorQueue.back());
	  mc.monitorTE(nextRequest.TargetTime, nextRequest.RCA,
		       nextRequest.DataLength, nextRequest.data.Raw, NULL,
		       &nextRequest.Timestamp, &nextRequest.Status);
	  countMSent++;
	  }
	  for (int i = 0; i < 2; i++)
	      {
	      monitorQueue.push_back(errRequest);
	      // As the push_back function (above) will do a copy this gets a
	      // reference to the copy and this is needed as the monitorTE function
	      // uses the addresses.
	      BareAMBDeviceInt::RequestStruct& nextRequest(monitorQueue.back());
	      mc.monitorTE(nextRequest.TargetTime, nextRequest.RCA,
			   nextRequest.DataLength, nextRequest.data.Raw, NULL,
			   &nextRequest.Timestamp, &nextRequest.Status);
	      countMSent++;
	      }
	  azPosRequest.TargetTime += TETimeUtil::TE_PERIOD_DURATION.value;
	  elPosRequest.TargetTime = azPosRequest.TargetTime;
	  errRequest.TargetTime = azPosRequest.TargetTime;
	  }

      lastScheduledTime.value(horizon.value());
      usleep(static_cast<useconds_t>(DurationHelper(loopTime).toSeconds() * 1E6));

      bool processCommands(true);
      while ((processCommands == true) && (commandQueue.empty() == false))
	  {
	  BareAMBDeviceInt::RequestStruct& curCommand(commandQueue.front());
	    
	  if(curCommand.Status == AMBERR_PENDING)
	      {
	      // Stop processing when we hit the first pending command point.
	      processCommands = false;
    
	      // check that the pending command is less that 5TEs behind the
	      // current time
	      EpochHelper    now(TETimeUtil::ceilTE(
				     TimeUtil::ace2epoch(ACE_OS::gettimeofday())));
	      EpochHelper commandedTime(curCommand.TargetTime);
	      DurationHelper diff(now.difference(commandedTime.value()));
	      double executionTime(diff.toSeconds());
	      if((now.value().value) > (replyTolerance*TETimeUtil::TE_PERIOD_DURATION.value + curCommand.TargetTime))
		  {
		  std::cout << "*** WARNING: command reply pending more than "
			    << replyTolerance
			    << " TEs "
			    << " ( " << executionTime << " s ) "
			    << "after the commanded time."
			    << std::endl << std::endl;
		  ++late_reply_cmd;
		  }
	      }
	  else
	      {
//	      std::cout << "Reply arrived on command RCA " << curCommand.RCA
//		        << std::endl;

	      EpochHelper commandedTime(curCommand.TargetTime);
	      EpochHelper actualTime(curCommand.Timestamp);
	      DurationHelper diff(actualTime.difference(commandedTime.value()));
	      double executionTime(diff.toSeconds());
	      
	      if (executionTime > 0.020)
		  {
		  ++tooLate_cmd;
		  }
	      
	      if (executionTime < minTime_cmd)
		  {
		  minTime_cmd = executionTime;
		  }
	      
	      if (executionTime > maxTime_cmd)
		  {
		  maxTime_cmd = executionTime;
		  }

	      if(curCommand.Status == AMBERR_TIMEOUT)
		  {
		  std::cout << "*** TIMEOUT ***"
			    << std::endl
			    << "Commanded Time "
			    << commandedTime.toString(acstime::TSArray, timeFmt.c_str(), 0,0)
			    << " Actual Time " << actualTime.toString(acstime::TSArray, timeFmt.c_str(), 0,0)
			    << std::fixed << std::setprecision(6)
			    << " Difference " << executionTime
			    << " RCA: " << curCommand.RCA
			    << " Datalength: " << curCommand.DataLength
			    << " Status: " << curCommand.Status
			    << std::endl << std::endl;
		  }
	      
	      if(curCommand.Status != AMBERR_NOERR)
		  {
		  ++failure_cmd;
		  }
	      else
		  {
		  ++success_cmd;
		  }
	      failure_per_amberrorcode[curCommand.Status]++;
	      commandQueue.pop_front();
	      }
	  }

      bool processMonitors(true);
      while ((processMonitors == true) && (monitorQueue.empty() == false))
	  {
	  BareAMBDeviceInt::RequestStruct& curMonitor(monitorQueue.front());
	  
	  if(curMonitor.Status == AMBERR_PENDING)
	      {
	      // Stop processing when we hit the first pending monitor point.
	      processMonitors = false;
	      
	      // check that the pending monitor is less that 5TEs behind the
	      // current time
	      EpochHelper    now(TETimeUtil::ceilTE(
				     TimeUtil::ace2epoch(ACE_OS::gettimeofday())));
	      EpochHelper commandedTime(curMonitor.TargetTime);
	      DurationHelper diff(now.difference(commandedTime.value()));
	      double executionTime(diff.toSeconds());
	      if((now.value().value) > (replyTolerance*TETimeUtil::TE_PERIOD_DURATION.value + curMonitor.TargetTime))
		  {
		  std::cout << "*** WARNING: monitor reply pending more than "
			    << replyTolerance
			    << " TEs "
			    << " ( " << executionTime << " s ) "
			    << "after the commanded time " 
			    << 	curMonitor.TargetTime	         
			    << std::endl << std::endl;
		  ++late_reply;
		  }
	      }
	  else
	      {
	      EpochHelper commandedTime(curMonitor.TargetTime);
	      EpochHelper actualTime(curMonitor.Timestamp);
	      DurationHelper diff(actualTime.difference(commandedTime.value()));
//	      std::cout << "Got monitor reply on RCA " << curMonitor.RCA
//		        << " requested at time " << curMonitor.TargetTime
//		        << " excuted at time " << curMonitor.Timestamp
//		        << std::endl;
	      double executionTime(diff.toSeconds());

	      if (executionTime > 0.020)
		  {
		  ++tooLate;
		  }
	      
	      if (executionTime < minTime)
		  {
		  minTime = executionTime;
		  }
	      
	      if (executionTime > maxTime)
		  {
		  maxTime = executionTime;
		  }

	      if(curMonitor.Status == AMBERR_TIMEOUT)
		  {
		  std::cout << "*** TIMEOUT ***"
			    << std::endl
			    << "Commanded Time "
			    << commandedTime.toString(acstime::TSArray, timeFmt.c_str(), 0,0)
			    << " Actual Time " << actualTime.toString(acstime::TSArray, timeFmt.c_str(), 0,0)
			    << std::fixed << std::setprecision(6)
			    << " Difference " << executionTime
			    << " RCA: " << curMonitor.RCA
			    << " Datalength: " << curMonitor.DataLength
			    << " Status: " << curMonitor.Status
			    << std::endl << std::endl;
		  }
#ifdef POSN_DUMP
              else
                  {
                  switch (curMonitor.RCA)
                      {
                      case 0x12:    // az_posn_rsp
                          std::cout << roundTE(curMonitor.TargetTime) << ": "
                                    << " Az posn " << PosnDump(&curMonitor.data)
                                    << std::endl;
                          break;
                      case 0x02:    // el_posn_rsp
                          std::cout << roundTE(curMonitor.TargetTime) << ": "
                                    << " El posn " << PosnDump(&curMonitor.data)
                                    << std::endl;
                          break;
                      case 0x2f:    // get_acu_error
                          break;
                      }
                  }
#endif
	      
	      if(curMonitor.Status != AMBERR_NOERR)
		  {
		  ++failure;
		  }
	      else
		  {
		  ++success;
		  }
	      failure_per_amberrorcode[curMonitor.Status]++;
	      monitorQueue.pop_front();
	      }
	  }
	
      horizon.value(
		    TETimeUtil::ceilTE(TimeUtil::ace2epoch(ACE_OS::gettimeofday())));
      horizon.add(DurationHelper(loopTime * 2).value());

      // save last position
      pointAz.data.Long[0] = azRequest.data.Long[0];
      pointEl.data.Long[0] = elRequest.data.Long[0];
    }
  while (doExit == false);

  // Send references with 0 speed
  ACS::Time TargetTime = lastScheduledTime.value().value;
  pointAz.rca = 0x1012;
  pointAz.dataLength = 8;
  pointAz.data.Long[1] = 0;
  mc.sendControlTE(TargetTime, pointAz);
  if (pointAz.status != 0)
    {
      std::cout << "Failed to send az trajectory command, status: " 
		<< pointAz.status << std:: endl;
    }
  pointEl.rca = 0x1002;
  pointEl.dataLength = 8;
  pointEl.data.Long[1] = 0;
  mc.sendControlTE(TargetTime, pointEl);
  if (pointEl.status != 0)
    {
      std::cout << "Failed to send az trajectory command, status: " 
		<< pointEl.status << std:: endl;
    }

  // Get the error stack
  for (int i = 0; i < 2; i++)
    {
      monitorPoint.rca = 0x2f;
      monitorPoint.status = AMBERR_PENDING;

      mc.sendMonitorTE(TargetTime + TETimeUtil::TE_PERIOD_DURATION.value / 2,
		       monitorPoint);
      if(monitorPoint.status!=0)
	{
	  std::cout << "Monitor GET_ACU_ERROR" 
		    <<" FAILED. Status: " << monitorPoint.status << std::endl;
	}
    }
  
  EpochHelper    now(TETimeUtil::ceilTE(
			 TimeUtil::ace2epoch(ACE_OS::gettimeofday())));
    
  ACS::TimeInterval waitTime = 
      stopTime.value().value 
      - now.value().value 
      + replyTolerance*TETimeUtil::TE_PERIOD_DURATION.value;
    
  if (waitTime > 0)	
      {
      std::cout << "Waiting "
		<< DurationHelper(waitTime).toSeconds() 
		<< " seconds before checking the pending requests"
		<< std::endl << std::endl;
      
      usleep(static_cast< useconds_t >(DurationHelper(waitTime).toSeconds() * 1E6));
      }
    
  while(commandQueue.size() > 0)
      {
      BareAMBDeviceInt::RequestStruct& curCommand(commandQueue.front());
      EpochHelper commandedTime(curCommand.TargetTime);
      EpochHelper actualTime(curCommand.Timestamp);
      DurationHelper diff(actualTime.difference(commandedTime.value()));
      double executionTime(diff.toSeconds());

      if (executionTime < minTime_cmd)
	  {
	  minTime_cmd = executionTime;
	  }
      
      if (executionTime > maxTime_cmd)
	  {
	  maxTime_cmd = executionTime;
	  }
      
      if (executionTime > 0.020)
	  {
	  tooLate_cmd++;
	  }
      
      if(curCommand.Status == AMBERR_TIMEOUT)
	  {
	  std::cout << "*** TIMEOUT ***"
		    << std::endl
		    << "Commanded Time "
		    << commandedTime.toString(acstime::TSArray, timeFmt.c_str(), 0,0)
		    << " Actual Time " << actualTime.toString(acstime::TSArray, timeFmt.c_str(), 0,0)
		    << std::fixed << std::setprecision(6)
		    << " Difference " << executionTime
		    << " RCA: " << curCommand.RCA
		    << " Datalength: " << curCommand.DataLength
		    << " Status: " << curCommand.Status
		    << std::endl << std::endl;
	  }

      if(curCommand.Status == AMBERR_PENDING)
	  {
	    // All the monitor requests should be completed at this point
	    // because we were sleeping before analizing the last points
	  ++late_reply_cmd;
	  }
      
      if(curCommand.Status != AMBERR_NOERR)
	  {
	  ++failure_cmd;
	  }
      else
	  {
	  ++success_cmd;
	  }
      failure_per_amberrorcode[curCommand.Status]++;
      commandQueue.pop_front();
      }

  while(monitorQueue.size() > 0)
      {
      BareAMBDeviceInt::RequestStruct& curMonitor(monitorQueue.front());
      EpochHelper commandedTime(curMonitor.TargetTime);
      EpochHelper actualTime(curMonitor.Timestamp);
      DurationHelper diff(actualTime.difference(commandedTime.value()));
      double executionTime(diff.toSeconds());

      if (executionTime < minTime)
	  {
	  minTime = executionTime;
	  }
      
      if (executionTime > maxTime)
	  {
	  maxTime = executionTime;
	  }
      
      if (executionTime > 0.020)
	  {
	  tooLate++;
	  }
      
      if(curMonitor.Status == AMBERR_TIMEOUT)
	  {
	  std::cout << "*** TIMEOUT ***"
		    << std::endl
		    << "Commanded Time "
		    << commandedTime.toString(acstime::TSArray, timeFmt.c_str(), 0,0)
		    << " Actual Time " << actualTime.toString(acstime::TSArray, timeFmt.c_str(), 0,0)
		    << std::fixed << std::setprecision(6)
		    << " Difference " << executionTime
		    << " RCA: " << curMonitor.RCA
		    << " Datalength: " << curMonitor.DataLength
		    << " Status: " << curMonitor.Status
		    << std::endl << std::endl;
	  }
      
      if(curMonitor.Status == AMBERR_PENDING)
	  {
	    // All the monitor requests should be completed at this point
	    // because we were sleeping before analizing the last points
	  ++late_reply;
	  }
      
      if(curMonitor.Status != AMBERR_NOERR)
	  {
	  ++failure;
	  }
      else
	  {
	  ++success;
	  }
      failure_per_amberrorcode[curMonitor.Status]++;
      monitorQueue.pop_front();
      }

  std::cout << std::endl 
	    << std::endl
	    << "FINAL REPORT" << std::endl
	    << "Sent " << countCmd 
	    << " trajectory commands" << std::endl
	    << "Sent " << countMSent 
	    << " monitor points. Got " << success + failure 
	    << " monitor replies and " << success_cmd + failure_cmd
            << " command replies. Problems with " << failure 
	    << " of the monitor replies (" 
	    << 100.0 * failure / (success + failure) << "%) and " 
	    << failure_cmd << " of the command replies ("
	    << 100.0 * failure_cmd / (success_cmd + failure_cmd) << "%)."
	    << std::endl;

  std::cout << "Min/Max execution times for monitor points (milli-seconds). " 
	    << minTime * 1000 << "/"
	    << maxTime * 1000 << std::endl;

  std::cout << "Min/Max execution times for control points (milli-seconds). " 
	    << minTime_cmd * 1000 << "/"
	    << maxTime_cmd * 1000 << std::endl;
    
  std::cout << tooLate 
	    << " monitor points (" << 100.0 * tooLate / (success + failure)
	    << "%) where executed later than 20ms after the requested time."
	    << std::endl;

  std::cout << tooLate_cmd
	    << " control points (" 
            << 100.0 * tooLate_cmd / (success_cmd + failure_cmd)
	    << "%) where executed later than 20ms after the requested time."
	    << std::endl;
    
  std::cout << "In " << late_reply 
	    << " occasions the monitor request replies were still pending "
	    << replyTolerance
	    <<" TEs after the commanded time."
	    << std::endl;

  std::cout << "In " << late_reply_cmd 
	    << " occasions the control request replies were still pending "
	    << replyTolerance
	    <<" TEs after the commanded time."
	    << std::endl;
  
  std::cout << std::endl
	    << "Error summary:"
	    << std::endl;
  
  for(int index=1;index<13;index++)
      {
      if (failure_per_amberrorcode[index] > 0)
	  {
	  std::cout << "Monitor points with AmbErrorCode_t "
		    << index 
		    << ": " 
		    << failure_per_amberrorcode[index]
		    << std::endl;
	  }
      }
}





