Usage: AcceptanceScript [ItemListFile] [MonitorOnly=True/False]
  MonitorOnly: if 'True', all control commands in the ItemListFile are skipped


ItemListFile Format (Comma separated format):

  Monitor:
    1st column: 'M'
    2nd column: MonitorName
    3rd column: Monitor CAN ID (Not used in the program)
    4th column: Timeout (sec) (Not implemented)
    Sample:
      M, GET_ACU_ERROR, 0x2F, 5


  Control:
    1st column: 'C'
    2nd column: ControlName
    3rd column: Control CAN ID (Not used in the program)
    4th column: Timeout (sec) (Not implemented)
    5th column: 'Check:xxx', 'Wait:xxx', or 'NoCheck'
      Check: Monitor the result
      Wait: just wait for specified duration
      NoCheck: Only issue the command without any check-up

  To monitor the result:
    5th column: 'Check:MonitorName,Wait,NCheck'
      Wait: Wait (sec) before checking the Monitor
      NCheck: Number of Monitor check cycles
    6th column and after:
      Each column has a Control Parameter set to be executed
    Sample: 
      C, ACU_MODE_CMD, 0x1022, 30, 'Check:ACU_MODE_RSP,5,2', '0,0'
      C, ACU_MODE_CMD, 0x1022, 30, 'Check:ACU_MODE_RSP,5,3', '0,0', '1,1', '2,2'      C, SET_AZ_BRAKE, 0x1014, 30, 'Check:GET_AZ_BRAKE,2,5', 0, 1, 0

  To wait for specified duration:
    5th column: 'Wait:Duration'
      Duration: wait duration in sec
    6th column and after:
      Each column has a Control Parameter set to be executed
    Sample:
      C, AZ_TRAJ_CMD, 0x1012, 30, 'Wait:60', '0,0', '1073741824,0', '-1073741824,0'

  To issue the command without any check:
    5th column: 'NoCheck'
    6th column and after:
      Each column has a Control Parameter set to be executed
    Sample:
      C, SET_METR_CALIBRATION, 0x1043, 30, 'NoCheck', 1


  Special Cases:

  Reliability check:
    1st column: 'M'
    2nd column: 'AcceptanceReliaility'
    3rd column: Monitor CAN ID should be issued continuously
    4th column: Duration (sec)
  Sample:
  M, AcceptanceReliability, 0x22, 30

  Elevation Tracking Test:
    1st column: 'C'
    2nd column: 'AcceptanceTrackEl'
    3rd column: '0x00' (not used in the program)
    4th column and after:
      Each column has a Control Parameter set to be executed
      'StartPosition(deg),Velocity(deg/sec),Acceleration(deg/s^2) Duration(sec)'  Sample:
    C, AcceptanceTrackEl, 0x00, '32,0.1,0.0 10', '35,0.2,0.0 10'

  Azimuth Tracking Test:
    1st column: 'C'
    2nd column: 'AcceptanceTrackAz'
    3rd column: '0x00' (not used in the program)
    4th column and after:
      Each column has a Control Parameter set to be executed
      'StartPosition(deg),Velocity(deg/sec),Acceleration(deg/s^2) Duration(sec)'  Sample:
    C, AcceptanceTrackAz, 0x00, '180,0.1,0.0 30', '190,0.2,0.0 30', '210,0.3,0.0,30'

  Az/El Tracking Test:
    1st column: 'C'
    2nd column: 'AcceptanceTrack'
    3rd column: '0x00' (not used in the program)
    4th column and after:
      Each column has a Control Parameter set to be executed
      'AzStartPosition(deg),Velocity(deg/sec),Acceleration(deg/s^2) ElStartPosition(deg),Velocity(deg/sec),Acceleration(deg/s^2) Duration(sec)'
  Sample:
    C, AcceptanceTrack, 0x00, '180,0.1,0.0 32,0.1,0.0 30', '190,0.2,0.0 35,0.2,0.0 30'

  Elevation Fast Switching Test:
    1st column: 'C'
    2nd column: 'AcceptanceFastSwEl'
    3rd column: '0x00' (not used in the program)
    4th column and after:
      Each column has a Control Parameter set to be executed
      'StartPosition(deg),Velocity(deg/sec),Acceleration(deg/s^2),SwitchingWidth(deg) Wait(sec)?,Cycle?'
  Sample:
    C, AcceptanceFastSwEl, 0x00, '30,0.1,0.0,1.0 30,2'

  Azimuth Fast Switching Test:
    1st column: 'C'
    2nd column: 'AcceptanceFastSwAz'
    3rd column: '0x00' (not used in the program)
    4th column and after:
      Each column has a Control Parameter set to be executed
      'StartPosition(deg),Velocity(deg/sec),Acceleration(deg/s^2),SwitchingWidth(deg) Wait(sec)?,Cycle?'
  Sample:
    C, AcceptanceFastSwAz, 0x00, '150,0.2,0.0,1.0 30,2'

  Az/El Fast Switching Test:
    1st column: 'C'
    2nd column: 'AcceptanceFastSw'
    3rd column: '0x00' (not used in the program)
    4th column and after:
      Each column has a Control Parameter set to be executed
      'AzStartPosition(deg),Velocity(deg/sec),Acceleration(deg/s^2),SwitchingWidth(deg) ElStartPosition(deg),Velocity(deg/sec),Acceleration(deg/s^2),SwitchingWidth(deg) Wait(sec)?,Cycle?'
  Sample:
    C, AcceptanceFastSw, 0x00, '150,0.2,0.0,1.0 30,0.1,0.0,1.0 30,2'
