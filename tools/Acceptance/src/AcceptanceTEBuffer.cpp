// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2006
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA


/* The objective of this program is to verify the ALMA ACU communication timing
 * requirement via CAN bus for command and requests per time event (TE). The
 * internal ACU buffer should be able to manage at least 256 commands and
 * requests.
 *
 * To test this performance, a monitor point will be invoked as many times as
 * needed.
 *
 */


#include <AcceptanceMountInterface.h> 
#include <ambDefs.h> // For AMBERR defs
#include <TETimeUtil.h> // for TETimeUtil
#include <acstimeTimeUtil.h>
#include <iostream> // for std::cout and std::endl
#include <ace/Get_Opt.h>
#include <stdlib.h>
#include <math.h>

using std::string;

const static unsigned short int defaultChannel(0U);
const static unsigned short int defaultNodeId(0U);
const static unsigned int defaultReplyTolerance(5);
const static unsigned int defaultMessageSend(256);
const static AmbRelativeAddr defaultMonitorRCA(ACU_MODE_RSP);

unsigned short int channel(defaultChannel);
unsigned short int nodeId(defaultNodeId);
unsigned int replyTolerance(defaultReplyTolerance);
unsigned int messageSend(defaultMessageSend);
AmbRelativeAddr monitorRCA(defaultMonitorRCA);
const static AmbRelativeAddr minRCA(0x1);
const static AmbRelativeAddr maxRCA(0x7fffff);
bool exitProgram(false);

void usage(char *argv0)
{
    std::cout << std::endl
	      << argv0
	      << ": usage:"
	      << std::endl
	      << "Options:"
	      << std::endl
	      << "[--help|h]:"
	      << std::endl
	      << "     You are reading it!"
	      << std::endl
	      << "[--channel=|-c<CHANNEL>]:"
	      << std::endl
	      << "      ABM channel. Range: 0-5. Default = "
	      << defaultChannel
	      << "."
	      << std::endl
	      << "[--nodeId=|-n<NODEID>]:"
	      << std::endl
	      << "      NodeId to be used. Range: 0-512. Default = "
	      << defaultNodeId
	      << "."
	      << std::endl
	      << "[--monitor_rca=|-r<RCA>]:"
	      << std::endl
	      << "      RCA of monitor request. Min. = 0x1; max. = "
	      << "0x7fffff. Default = " << defaultMonitorRCA
	      << "."
	      << std::endl
	      << "[--messageSend=|-s<NUMBER>]:"
	      << std::endl
	      << "      Amount of requests to send. Default = "
	      << defaultMessageSend
	      << "."
	      << std::endl;
};

void parseOptions(ACE_Get_Opt &options)
{
    int c(0);
    while((c = options()) != EOF)
    {
        switch(c)
	{
	    case 'c':
	    {
	    if(options.opt_arg() != 0)
		{
		std::istringstream value;
		value.str(options.opt_arg());
		short int val(0);
		value >> val;
		if((val < 0) || (val > 5))
		    {
		    std::cout << "The channel number has to be in the range of"
			      << "(0; 5)."
			      << std::endl;
		    exitProgram = true;
		    }
		else
		    {
		    channel = val;
		    std::cout << "channel     : "
			      << channel
			      << std::endl;
		    }
		}
	    }
	    break;
            case 'n':
            {
	    if(options.opt_arg() != 0)
                {
		std::istringstream value;
		value.str(options.opt_arg());
		unsigned int val(0U);
		value >> val;
		if((val < 0) || (val > 0x200))
		    {
		    std::cout << "The node ID has to be in the range of (0; 512)." 
			      << std::endl;
		    exitProgram = true;
		    }
		else
		    {
		    nodeId = val;
		    std::cout << "node        : "
			      << nodeId
			      << std::endl;
		    }
                }
            }
	    break;
	    case 'r':
	    {
	    if(options.opt_arg() != 0)
		{
		string value;
		value = options.opt_arg();
		AmbRelativeAddr monitor = strtol(value.c_str(),NULL,0);
		if((monitor < minRCA) || (monitor > maxRCA))
		    {
		    std::cout << "The RCA has to be in the range "
			      << "(0x1; 0x7fffff)! The value will not be used." << std::endl;
		    }
		else
		    {           
		    std::cout << "Monitor RCA : 0x"
			      << std::hex
			      << monitor
			      << std::dec
			      << std::endl;
		    monitorRCA = monitor;
		    }
		}
	    }
	    break;
            case 's':
            {
	    if(options.opt_arg() != 0)
                {
		std::istringstream value;
		value.str(options.opt_arg());
		unsigned int val(0U);
		value >> val;
		if(val <= 0)
		    {
		    std::cout << "The amount of messages has to be greater than zero." 
			      << std::endl;
		    exitProgram = true;
		    }
		else
		    {
		    messageSend = val;
		    std::cout << "messageSend        : "
			      << messageSend
			      << std::endl;
		    }
                }
            }
	    break;
            case '?':
            {
	    std::cout << "Option -"
		      << options.opt_opt()
		      << "requires an argument!"
		      << std::endl;
            }
	    // Fall through...
            case 'h':
            default:    // Display help.
            {
	    exitProgram = true;
            }
	}
    }
};


int main(int argc, char* argv[])
{
	ACE_Get_Opt options(argc, argv);
	options.long_option("help"       , 'h');
	options.long_option("channel"    , 'c', ACE_Get_Opt::ARG_OPTIONAL);
	options.long_option("nodeId"     , 'n', ACE_Get_Opt::ARG_OPTIONAL);
	options.long_option("monitorRCA" , 'r', ACE_Get_Opt::ARG_OPTIONAL);
	options.long_option("messageSend", 's', ACE_Get_Opt::ARG_OPTIONAL);
	parseOptions(options);

	if(exitProgram) {
		usage(argv[0]);
		return -ENODEV;
	}

	BareAMBDeviceInt mc;
	if (mc.initialize(channel, nodeId) == false) {
		return -ENODEV;
	}

	// Sending monitors
	BareAMBDeviceInt::ICDPoint points[messageSend];
	unsigned int failed_count = 0U;
	unsigned int sent_count;
	
	acstime::Epoch startTime = TimeUtil::ace2epoch(ACE_OS::gettimeofday());
	//ACS::Time targetTime = TETimeUtil::plusTE(startTime, targetGap).value;
	
	for (sent_count=0U; sent_count < messageSend; sent_count++ ) {
		points[sent_count].rca = monitorRCA;
		points[sent_count].status = AMBERR_PENDING;
		//mc.sendMonitorTE(targetTime, points[sent_count]);
		mc.sendMonitor(points[sent_count]);
	}
	
	acstime::Epoch endTime = TimeUtil::ace2epoch(ACE_OS::gettimeofday());
	ACS::Time duration = endTime.value - startTime.value;
	double durationTE = ceil(double(duration) / double(TETimeUtil::TE_PERIOD_ACS));
	
	std::cout << "TE period: " << TETimeUtil::TE_PERIOD_ACS << std::endl;
	std::cout << "startTime: " << startTime.value << " endTime: " << endTime.value
		<< " duration: " << duration << std::endl << std::endl;

	// Wait until execution time is over (considering maximum duration of 4 requests per TE)
	while (TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value < (messageSend/100)*TETimeUtil::TE_PERIOD_ACS)
		sleep(1);

	// Print results
	unsigned int currentTE = 1U;
	unsigned int currentTE_req = 0U;
	for (sent_count=0U; sent_count < messageSend; sent_count++ ) {
		if(points[sent_count].status != 0 || points[sent_count].dataLength==0) {
			failed_count++;
			std::cout << " MONITOR " << sent_count
				<< " FAILED. Status: " << points[sent_count].status << " dataLength:  " << points[sent_count].dataLength << std::endl;
		} else {
			//double exTE = ceil(double(points[sent_count].timestamp - targetTime) / double(TETimeUtil::TE_PERIOD_ACS));
			double exTE = ceil(double(points[sent_count].timestamp - startTime.value) / double(TETimeUtil::TE_PERIOD_ACS));
			if (exTE != currentTE) {
				std::cout << "TOTAL of " << currentTE_req << " successful MONITOR requests during TE " << currentTE << std::endl;
				currentTE++;
				currentTE_req = 1U;
			}
			//std::cout << " MONITOR " << sent_count
			//	<< " executed in time event " << exTE << std::endl;

			currentTE_req++;
		}
	}
	std::cout << "TOTAL of " << currentTE_req << " successful MONITOR requests during TE " << currentTE << std::endl;

	std::cout << std::endl;
	std::cout << "Sent " << messageSend << " requests with " << failed_count
		<< " failures in " << durationTE << " time event(s)." << std::endl;

}

