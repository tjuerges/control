#include <BareAmbDeviceInt.h>

const double degreePerTurn = 180. / 0x40000000;

void myntohl(long &v)
{
    long a = 0;
    unsigned long uv = v;

    for (int i = 0; i < 4; i++) {
        a <<= 8;
        a |= uv & 0xff;
        uv >>= 8;
    }
    v = a;
}

char trajbuf[256], posnbuf[256];
const char *PosnDump(union BareAMBDeviceInt::holder *p)
{
    double te, mid;
    te  = ntohl(p->Long[0]) * degreePerTurn;
    mid = ntohl(p->Long[1]) * degreePerTurn;

    sprintf(posnbuf, "%11.6f %11.6f", mid, te);
    return posnbuf;
}
const char *TrajDump(union BareAMBDeviceInt::holder *p)
{
    double pos, vel;
    pos = ntohl(p->Long[0]) * degreePerTurn;
    vel = ntohl(p->Long[1]) * degreePerTurn;

    sprintf(trajbuf, "%11.6f %13.10f", pos, vel);
    return trajbuf;
}
ACS::Time roundTE(ACS::Time time)
{
    return time % TETimeUtil::TE_PERIOD_DURATION.value;
}
