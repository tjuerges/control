// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2006
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA


#include <BareAmbDeviceInt.h>
#include <ace/Semaphore.h> // for sem_t
#include <ambDefs.h> // For AMBERR defs
#include <acstimeEpochHelper.h> // for EpochHelper
#include <acstimeDurationHelper.h> // for DurationHelper
#include <TETimeUtil.h> // for TETimeUtil
#include <unistd.h> // for usleep
#include <deque> // for deque
#include <iomanip> // for std::setprecision and std::fixed
#include <iostream> // for std::cout and std::endl
#include <ace/Get_Opt.h>
#include <sstream>

const static double minimumDuration(0.001);
const static double defaultDuration(10.0);
const static unsigned short int defaultNodeId(0U);
const static unsigned short int defaultChannel(2U);
const static unsigned short int defaultNumberRequests(40U);
const static unsigned int defaultRca(0x30000U);
const static unsigned int defaultReplyTolerance(5);

double duration(defaultDuration);
unsigned short int nodeId(defaultNodeId);
unsigned short int channel(defaultChannel);
unsigned short int requests(defaultNumberRequests);
unsigned int rca(defaultRca);
unsigned int replyTolerance(defaultReplyTolerance);

// This is how often the monitor loop runs (once every 20 TE's).
const ACS::TimeInterval loopTime(20 * TETimeUtil::TE_PERIOD_DURATION.value);
// time format string.
const std::string timeFmt("%H:%M:%S.%3q");

void usage(char *argv0)
{
    std::cout << std::endl
	      << argv0
	      << "::usage:"
	      << std::endl
	      << "Options:"
	      << std::endl
	      << "[--help|h]:"
	      << std::endl
	      << "     You are reading it!"
	      << std::endl
	      << "[--duration=|-d<TIME in fractional s>]:"
	      << std::endl
	      << "      Duration of the test. Default = "
	      << defaultDuration
	      << "(minimum value = "
	      << minimumDuration
	      << "s)"
	      << std::endl
	      << "[--nodeId=|-n<NODEID>]:"
	      << std::endl
	      << "      NodeId to be used. Default = "
	      << defaultNodeId
	      << std::endl
	      << "[--channel=|-c<CHANNEL>]:"
	      << std::endl
	      << "      ABM channel. Default = "
	      << defaultChannel
	      << std::endl
	      << "[--monitor_rca=|-r<RCA>]:"
	      << std::endl
	      << "      RCA of monitor request to use per request. Min. = 0x1; max. = "
	      << "0x7fffff. Default = "
	      << defaultRca
	      << std::endl
	      << "[--reply_tolerance=|-t<NUMBER_OF_TEs>]:"
	      << std::endl
	      << "      Time tolerance when waiting for replies (in TEs)."
	      << " Default = "
	      << defaultReplyTolerance
	      << " TE"
	      << std::endl;
};

int main(int argc, char* argv[])
{
    ACE_Get_Opt options(argc, argv);

    options.long_option("help", 'h');
    options.long_option("duration", 'd', ACE_Get_Opt::ARG_OPTIONAL);
    options.long_option("nodeId", 'n', ACE_Get_Opt::ARG_OPTIONAL);
    options.long_option("channel", 'c', ACE_Get_Opt::ARG_OPTIONAL);
    options.long_option("control_rca", 'x', ACE_Get_Opt::ARG_OPTIONAL);
    options.long_option("monitor_rca", 'r', ACE_Get_Opt::ARG_OPTIONAL);
    options.long_option("reply_tolerance", 't', ACE_Get_Opt::ARG_OPTIONAL);

    std::vector< AmbRelativeAddr > rcaList;
    int c(0);
    while((c = options()) != EOF)
	{
        switch(c)
	    {
            case 'd':
            {
	    if(options.opt_arg() != 0)
                {
		std::istringstream value;
		value.str(options.opt_arg());
		double val(0);
		value >> val;
		if(val < minimumDuration)
		    {
		    std::cout << "The minimum duration is "
			      << minimumDuration
			      <<" s. The default value will "
			      << "be used instead."
			      << std::endl;
		    
		    }
		else
		    {
		    duration = val;
		    std::cout << "The test duration has been set to "
			      << duration
			      << "s."
			      << std::endl;
		    }
                }
            }
	    break;
	    
            case 'n':
            {
	    if(options.opt_arg() != 0)
                {
		std::istringstream value;
		value.str(options.opt_arg());
		unsigned int val(0U);
		value >> val;
		if((val < 0) || (val > 0x200))
		    {
		    std::cout << "The node ID has to be in the range of (0; 512). The "
			      << "value will not be used."
			      << std::endl;
		    }
		else
		    {
		    nodeId = val;
		    std::cout << "The node ID has been set to "
			      << nodeId
			      << "."
			      << std::endl;
		    }
                }
            }
	    break;
	    
	    case 'c':
	    {
	    if(options.opt_arg() != 0)
		{
		std::istringstream value;
		value.str(options.opt_arg());
		short int val(0);
		value >> val;
		if((val < 0) || (val > 5))
		    {
		    std::cout << "The channel number has to be in the range of"
			      << "(0; 5). The value will not be used."
			      << std::endl;
		    }
		else
		    {
		    channel = val;
		    std::cout << "The ABM channel has been set to "
			      << channel
			      << "."
			      << std::endl;
		    }
		}
	    }
	    break;
	    
	    case 'x':
	    {
	    if(options.opt_arg() != 0)
		{
		std::string control;
		std::string control_rca;
		std::string control_argument;
		control = options.opt_arg();
		control_rca      = control.substr(0,control.find(','));
		control_argument = control.substr(control.find(',')+1);
		
		unsigned int val1(0U);
		unsigned int val2(0U);
		
		val1 = ACE_OS::atoi(control_rca.c_str());
		val2 = ACE_OS::atoi(control_argument.c_str());
		
		std::cout << "control point received: "
			  << control
			  << " rca: "
			  << val1
			  << " val: "
			  << val2
			  << std::endl;
		
		control.find(',');
		}
	    }
	    break;
	    
	    case 'r':
	    {
	    if(options.opt_arg() != 0)
		{
//		std::istringstream value;
//		value.str(options.opt_arg());
//		unsigned int val(0U);
//		value >> val;
		std::string value;
		value = options.opt_arg();
		AmbRelativeAddr monitor = strtol(value.c_str(),NULL,0);
		if((monitor < 1) || (monitor > 0x7fffff))
		    {
		    std::cout << "The RCA has to be in the range "
			      << "(0x1; 0x7fffff)! The value will not be used." << std::endl;
		    }
		else
		    {
		    rca = monitor;
		    std::cout << "RCA 0x"
			      << std::hex
			      << rca
			      << std::dec
			      << " has been added to the monitor point list."
			      << std::endl;
		    rcaList.push_back(rca); // Slave rev. level
		    }
		}
	    }
	    break;
	    
	    case 't':
	    {
	    if(options.opt_arg() != 0)
		{
		std::istringstream value;
		value.str(options.opt_arg());
		unsigned int val(0);
		value >> val;
		if(val > defaultReplyTolerance)
                    {
		    replyTolerance = val;
		    std::cout << "The reply tolerance has been set to "
			      << replyTolerance
			      << " TEs."
			      << std::endl;
                    }
                }
            }
	    break;
	    
            case '?':
            {
	    std::cout << "Option -"
		      << options.opt_opt()
		      << "requires an argument!"
		      << std::endl;
            }
	    // Fall through...
            case 'h':
            default:    // Display help.
            {
	    usage(argv[0]);
	    return -1;
            }
	    }
	}

    BareAMBDeviceInt mc;
    if (mc.initialize(channel, nodeId) == false)
	{
        return -ENODEV;
	}
    
    if (rcaList.size() == 0)
	rcaList.push_back(rca); // Slave rev. level
    
    EpochHelper    timeNow(TETimeUtil::ceilTE(
			       TimeUtil::ace2epoch(ACE_OS::gettimeofday())));
    
    EpochHelper lastScheduledTime(timeNow.value());
    lastScheduledTime.add(DurationHelper(loopTime).value());
    
    EpochHelper stopTime(lastScheduledTime.value());
    stopTime.add(DurationHelper(static_cast< long double >(duration)).value());
    
    EpochHelper horizon(timeNow.value());
    horizon.add(DurationHelper(loopTime * 2).value());
    bool doExit(false);
    std::deque< BareAMBDeviceInt::RequestStruct > monitorQueue;
    
    int failure(0);
    int late_reply(0);
    int success(0);
    int tooLate(0);
    unsigned int failure_per_amberrorcode[13];
    for(int index=0;index<13;index++)
	failure_per_amberrorcode[index]=0;
    
    double minTime(1E30);
    double maxTime(-1E30);

    do
	{
        if((horizon >= stopTime.value()))
	    {
            horizon.value(stopTime.value());
            doExit = true;
	    }
	
        {
	DurationHelper queueTime(horizon.difference(lastScheduledTime.value()));
	const int numTEs(
	    queueTime.value().value /
	    TETimeUtil::TE_PERIOD_DURATION.value);
	int monitorsPerTE(rcaList.size());
	const int numRequests(numTEs * monitorsPerTE);
	std::cout << "Queueing " << numRequests << " monitor requests ("
		  << queueTime.toSeconds() << " seconds worth)" << std::endl;
        }
	
        BareAMBDeviceInt::RequestStruct thisRequest;
        // Need to schedule the monitor requests 24ms after the TE
        thisRequest.TargetTime = lastScheduledTime.value().value +
            TETimeUtil::TE_PERIOD_DURATION.value / 2;
        thisRequest.Status = AMBERR_PENDING;
        thisRequest.DataLength = 8;
        thisRequest.RCA = 0x01;
        thisRequest.Timestamp = 0;
        const ACS::Time lastTime(horizon.value().value);
	
        while (thisRequest.TargetTime < lastTime)
	    {
            for(std::vector< AmbRelativeAddr >::const_iterator i(rcaList.begin());
                i != rcaList.end(); ++i)
		{
                thisRequest.RCA = *i;
                monitorQueue.push_back(thisRequest);
                // As the push_back function (above) will do a copy this gets a
                // reference to the copy and this is needed as the monitorTE function
                // uses the addresses.
                BareAMBDeviceInt::RequestStruct& nextRequest(monitorQueue.back());
                mc.monitorTE(nextRequest.TargetTime, nextRequest.RCA,
			     nextRequest.DataLength, nextRequest.data.Raw, NULL,
			     &nextRequest.Timestamp, &nextRequest.Status);
		}
	    
            thisRequest.TargetTime += TETimeUtil::TE_PERIOD_DURATION.value;
	    }
	
        lastScheduledTime.value(horizon.value());
        usleep(static_cast<useconds_t>(DurationHelper(loopTime).toSeconds() * 1E6));
	
        bool processMonitors(true);
        while ((processMonitors == true) && (monitorQueue.empty() == false))
	    {
            BareAMBDeviceInt::RequestStruct& curMonitor(monitorQueue.front());
	    
            if(curMonitor.Status == AMBERR_PENDING)
		{
                // Stop processing when we hit the first pending monitor point.
                processMonitors = false;
		
		// check that the pending monitor is less that 5TEs behind the
		// current time
		EpochHelper    now(TETimeUtil::ceilTE(
				       TimeUtil::ace2epoch(ACE_OS::gettimeofday())));
                EpochHelper commandedTime(curMonitor.TargetTime);
                DurationHelper diff(now.difference(commandedTime.value()));
                double executionTime(diff.toSeconds());
		if((now.value().value) > (replyTolerance*TETimeUtil::TE_PERIOD_DURATION.value + curMonitor.TargetTime))
		    {
                    std::cout << "*** WARNING: reply pending more than "
			      << replyTolerance
			      << " TEs "
			      << " ( " << executionTime << " s ) "
			      << "after the commanded time."
			      << std::endl << std::endl;
		    ++late_reply;
		    }
		}
            else
		{
                EpochHelper commandedTime(curMonitor.TargetTime);
                EpochHelper actualTime(curMonitor.Timestamp);
                DurationHelper diff(actualTime.difference(commandedTime.value()));
                double executionTime(diff.toSeconds());
		
                if (executionTime > 0.020)
		    {
                    ++tooLate;
		    }
		
                if (executionTime < minTime)
		    {
                    minTime = executionTime;
		    }
		
                if (executionTime > maxTime)
		    {
                    maxTime = executionTime;
		    }
		
                if(curMonitor.Status == AMBERR_TIMEOUT)
		    {
                    std::cout << "*** TIMEOUT ***"
			      << std::endl
			      << "Commanded Time "
			      << commandedTime.toString(acstime::TSArray, timeFmt.c_str(), 0,0)
			      << " Actual Time " << actualTime.toString(acstime::TSArray, timeFmt.c_str(), 0,0)
			      << std::fixed << std::setprecision(6)
			      << " Difference " << executionTime
			      << " RCA: " << curMonitor.RCA
			      << " Datalength: " << curMonitor.DataLength
			      << " Status: " << curMonitor.Status
			      << std::endl << std::endl;
		    }
		
                if(curMonitor.Status != AMBERR_NOERR)
		    {
		    ++failure;
		    }
                else
		    {
                    ++success;
		    }
		failure_per_amberrorcode[curMonitor.Status]++;
                monitorQueue.pop_front();
		}
	    }
	
        horizon.value(
            TETimeUtil::ceilTE(TimeUtil::ace2epoch(ACE_OS::gettimeofday())));
        horizon.add(DurationHelper(loopTime * 2).value());
	}
    while (doExit == false);
    
    EpochHelper    now(TETimeUtil::ceilTE(
			   TimeUtil::ace2epoch(ACE_OS::gettimeofday())));
    
    ACS::TimeInterval waitTime = 
	stopTime.value().value 
	- now.value().value 
	+ replyTolerance*TETimeUtil::TE_PERIOD_DURATION.value;
    
    if (waitTime > 0)	
	{
	std::cout << "Waiting "
		  << DurationHelper(waitTime).toSeconds() 
		  << " seconds before checking the pending requests"
		  << std::endl << std::endl;
	
	usleep(static_cast< useconds_t >(DurationHelper(waitTime).toSeconds() * 1E6));
	}
    
    while(monitorQueue.size() > 0)
	{
        BareAMBDeviceInt::RequestStruct& curMonitor(monitorQueue.front());
        EpochHelper commandedTime(curMonitor.TargetTime);
        EpochHelper actualTime(curMonitor.Timestamp);
        DurationHelper diff(actualTime.difference(commandedTime.value()));
        double executionTime(diff.toSeconds());
	
        if (executionTime < minTime)
	    {
            minTime = executionTime;
	    }
	
        if (executionTime > maxTime)
	    {
            maxTime = executionTime;
	    }
	
        if (executionTime > 0.020)
	    {
            tooLate++;
	    }
	
        if(curMonitor.Status == AMBERR_TIMEOUT)
	    {
            std::cout << "*** TIMEOUT ***"
		      << std::endl
		      << "Commanded Time "
		      << commandedTime.toString(acstime::TSArray, timeFmt.c_str(), 0,0)
		      << " Actual Time " << actualTime.toString(acstime::TSArray, timeFmt.c_str(), 0,0)
		      << std::fixed << std::setprecision(6)
		      << " Difference " << executionTime
		      << " RCA: " << curMonitor.RCA
		      << " Datalength: " << curMonitor.DataLength
		      << " Status: " << curMonitor.Status
		      << std::endl << std::endl;
	    doExit = true;
	    }
	
	if(curMonitor.Status == AMBERR_PENDING)
	    {
	    // All the monitor requests should be completed at this point
	    // because we were sleeping before analizing the last points
	    ++late_reply;
	    }
	
        if(curMonitor.Status != AMBERR_NOERR)
	    {
            ++failure;
	    }
        else
	    {
            ++success;
	    }
	failure_per_amberrorcode[curMonitor.Status]++;
        monitorQueue.pop_front();
	}
    
    std::cout << std::endl 
	      << std::endl
	      << "FINAL REPORT" << std::endl
	      << "Sent " << success + failure 
	      << " monitor points. Problems with "
	      << failure 
	      << " of them (" << 100.0 * failure / (success + failure) << "%)." 
	      << std::endl;
    
    std::cout << "Min/Max execution times (milli-seconds). " 
	      << minTime * 1000 << "/"
	      << maxTime * 1000 << std::endl;
    
    std::cout << tooLate 
	      << " monitor points (" << 100.0 * tooLate / (success + failure)
	      << "%) where executed later than 20ms after the requested time."
	      << std::endl;
    
    std::cout << "In " << late_reply 
	      << " occasions the monitor request replies were still pending "
	      << replyTolerance
	      <<" TEs after the commanded time."
	      << std::endl;
    
    std::cout << std::endl
	      << "Error summary (code 0 means no error):"
	      << std::endl;
    
    for(int index=0;index<13;index++)
	{
	std::cout << "Monitor points with AmbErrorCode_t "
		  << index 
		  << ": " 
		  << failure_per_amberrorcode[index]
		  << std::endl;
	}
    
    return 0;
}
