#include "Sensor.h"
#include "GalilSensor.h"
#include <Galil.h>
#include <sstream>
#include "Opts.h"

GalilSensor::GalilSensor(Galil * board, int n) :
galil(board),
inputNumber(n)
{
    using namespace boost::posix_time;
    cacheTime = microsec_clock::universal_time() - seconds(1);
}

double GalilSensor::read()
{
    using namespace boost::posix_time;
    ptime t(microsec_clock::universal_time());

    if (t < cacheTime + seconds(0.1)) {
        return cached;
    }

    std::stringstream ss;
    ss << "MG @AN[" << inputNumber << "]";

    try {
        std::string result(galil -> command(ss.str()));

        ss.str(result);
        double output;
        ss >> output; 
        if (!ss.fail()) cached = output;
        else logFile << "Failed to read from analog " << inputNumber << "\n";
        
    } catch (std::string ex) {
        logFile << "Exception: " << ex << std::endl;
    }

    return cached;
}
