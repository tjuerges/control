#ifndef QUERYCOMMAND_H
#define QUERYCOMMAND_H

#include "Command.h"

/** A command that can be called by the syntax "Query [something]". These
 * commands may be executed by anyone by default. The QueryCommand class
 * does not represent a command in itself. Instead, query commands are
 * created by extending the QueryCommand class.
 */
class QueryCommand : public Command {
    public:
        /** Constructor. Registers a new query command with the specified
         * name.
         * @param name the name of the new command.
         */
        QueryCommand(const char * name);

        virtual bool permission(MessageParty sender, 
                                std::vector<std::string> command);
};

class QueryBias : QueryCommand {
    public:
        QueryBias() : QueryCommand("Bias") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QueryBias queryBias;

class QueryBlank : QueryCommand {
    public:
        QueryBlank() : QueryCommand("Blank") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QueryBlank queryBlank;

/** Command to query the CCD camera power state (on or off). */
class QueryCamPower : QueryCommand {
    public:
        /** Constructor. Registers the command as a query command
         * with the name "CamPower".
         */
        QueryCamPower() : QueryCommand("CamPower") {};
        std::string execute(MessageParty sender, 
                            std::vector<std::string> command);
};
extern QueryCamPower queryCamPower;

/** Command to query the camera status. The camera may be in a state of 
 * "Power Off", "Idle", or "Exposing". If the camera is exposing, the time
 * left in the current series is also reported.
 */
class QueryCamStat : QueryCommand {
    public:
        /** Constructor. Registers the command as a query command
         * with the name "CamStat".
         */
        QueryCamStat() : QueryCommand("CamStat") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QueryCamStat queryCamStat;

/** Command to report the current and target CCD temperature. */
class QueryCamTemp : QueryCommand {
    public:
        /** Constructor. Registers the command as a query command
         * with the name "CamTemp".
         */
        QueryCamTemp() : QueryCommand("CamTemp") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QueryCamTemp queryCamTemp;

/** Command to report the ID of the selected centroiding algorithm. */
class QueryCentAlg : QueryCommand {
    public:
        /** Constructor. Registers the command as a query command
         * with the name "CentAlg".
         */
        QueryCentAlg() : QueryCommand("CentAlg") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QueryCentAlg queryCentAlg;


/** Command to query the currently selected threshold for centroiding. */
class QueryCenThresh : QueryCommand {
    public:
        /** Constructor. Registers the command as a query command
         * with the name "CenThresh".
         */
        QueryCenThresh() : QueryCommand("CenThresh") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QueryCenThresh queryCenThresh;


/** Command to query the OPTS for engineering diagnostic data. */
class QueryEngStatus : QueryCommand {
    public:
        /** Constructor. Registers the command as a query command
         * with the name "EngStatus".
         */
        QueryEngStatus() : QueryCommand("EngStatus") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QueryEngStatus queryEngStatus;


/** Command to query the currently selected exposure time for the camera. */
class QueryExpTime : QueryCommand {
    public:
        /** Constructor. Registers the command as a query command
         * with the name "ExpTime".
         */
        QueryExpTime() : QueryCommand("ExpTime") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QueryExpTime queryExpTime;

/** Command to query the state of the fan. */
class QueryFan : QueryCommand {
    public:
        QueryFan() : QueryCommand("Fan") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QueryFan queryFan;

/** Command to query the position of the filter wheel. */
class QueryFiltPos : QueryCommand {
    public:
        /** Constructor. Registers the command as a query command
         * with the name "FiltPos".
         */
        QueryFiltPos() : QueryCommand("FiltPos") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QueryFiltPos queryFiltPos;


/** Command to query the flat field setting for data processing system. */
class QueryFlatField : QueryCommand {
    public:
        /** Constructor. Registers the command as a query command
         * with the name "FlatField".
         */
        QueryFlatField() : QueryCommand("FlatField") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QueryFlatField queryFlatField;


/** Command to initialize the focuser. */
class QueryFocusInit : QueryCommand {
    public:
        /** Constructor. Registers the command as a query command
         * with the name "FocusInit".
         */
        QueryFocusInit() : QueryCommand("FocusInit") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QueryFocusInit queryFocusInit;


/** Command to query the current position of the focuser. */
class QueryFocusPos : QueryCommand {
    public:
        /** Constructor. Registers the command as a query command
         * with the name "FocusPos".
         */
        QueryFocusPos() : QueryCommand("FocusPos") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QueryFocusPos queryFocusPos;


/** Command to list the currently set hot pixels. */
class QueryHotPix : QueryCommand {
    public:
        /** Constructor. Registers the command as a query command
         * with the name "HotPix".
         */
        QueryHotPix() : QueryCommand("HotPix") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QueryHotPix queryHotPix;


/** Command to query the camera ID. */
class QueryIDCamera : QueryCommand {
    public:
        /** Constructor. Registers the command as a query command
         * with the name "IDCamera".
         */
        QueryIDCamera() : QueryCommand("IDCamera") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QueryIDCamera queryIDCamera;


/** Command to query the TCM ID. */
class QueryIDTCM : QueryCommand {
    public:
        /** Constructor. Registers the command as a query command
         * with the name "IDTCM".
         */
        QueryIDTCM() : QueryCommand("IDTCM") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QueryIDTCM queryIDTCM;


/** Command to query for the name of the master mailbox. */
class QueryMasterMB : QueryCommand {
    public:
        /** Constructor. Registers the command as a query command
         * with the name "MasterMB".
         */
        QueryMasterMB() : QueryCommand("MasterMB") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QueryMasterMB queryMasterMB;


/** Command to query the power state of the system. */
class QueryPower : QueryCommand {
    public:
        /** Constructor. Registers the command as a query command
         * with the name "Power".
         */
        QueryPower() : QueryCommand("Power") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QueryPower queryPower;


/** Command to query for the sequence number that will apply to the next
 * exposure. 
 */
class QuerySeqNo : QueryCommand {
    public:
        /** Constructor. Registers the command as a query command
         * with the name "SeqNo".
         */
        QuerySeqNo() : QueryCommand("SeqNo") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QuerySeqNo querySeqNo;


/** Command to query for the state of the shutter. */
class QueryShutter : QueryCommand {
    public:
        /** Constructor. Registers the command as a query command
         * with the name "Shutter".
         */
        QueryShutter() : QueryCommand("Shutter") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QueryShutter queryShutter;


/** Command to query for the system status. */
class QueryStatus : QueryCommand {
    public:
        /** Constructor. Registers the command as a query command
         * with the name "Status".
         */
        QueryStatus() : QueryCommand("Status") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QueryStatus queryStatus; 


/** Command to query for the region of interest of future exposures. */
class QuerySubImage : QueryCommand {
    public:
        /** Constructor. Registers the command as a query command
         * with the name "SubImage".
         */
        QuerySubImage() : QueryCommand("SubImage") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QuerySubImage querySubImage;


/** Command to query for the state of the telescope heater. */
class QueryTelHeater : QueryCommand {
    public:
        /** Constructor. Registers the command as a query command
         * with the name "TelHeater".
         */
        QueryTelHeater() : QueryCommand("TelHeater") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QueryTelHeater queryTelHeater; 


/** Command query for the current telescope temperature. */
class QueryTelTemp : QueryCommand {
    public:
        /** Constructor. Registers the command as a query command
         * with the name "TelTemp".
         */
        QueryTelTemp() : QueryCommand("TelTemp") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QueryTelTemp queryTelTemp;


/** Commmand to query whether the telescope is slewing. */
class QuerySlew : QueryCommand {
    public:
        QuerySlew() : QueryCommand("Slew") {};
	std::string execute(MessageParty sender,
	                    std::vector<std::string> command);
};
extern QuerySlew querySlew;

/** Command to query for the current state of the sun sensor. */
class QuerySunSensor : QueryCommand {
    public:
        /** Constructor. Registers the command as a query command
         * with the name "SunSensor".
         */
        QuerySunSensor() : QueryCommand("SunSensor") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QuerySunSensor querySunSensor;


/** Command to query for the current sun sensor threshold. */
class QuerySunThresh : QueryCommand {
    public:
        /** Constructor. Registers the command as a query command
         * with the name "SunThresh".
         */
        QuerySunThresh() : QueryCommand("SunThresh") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QuerySunThresh querySunThresh;


/** Command to issue all query commands. This command skips the
 * "Query EngStatus" command, since the output from that command is very long,
 * not very readable, and not standardized in any way.
 */
class QueryAll : QueryCommand {
    public:
        /** Constructor. Registers the command as a query command
         * with the name "All".
         */
        QueryAll() : QueryCommand("All") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern QueryAll queryAll;

#endif // ! QUERYCOMMAND_H
