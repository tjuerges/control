#ifndef COMMAND_H
#define COMMAND_H

#include <string>
#include <vector>

#include "ClientInterface.h"

/** Base class for representing a client command.
 *
 * There must be one derived class for each command
 * in the command syntax described in the ICD. For instance,
 * there should be a derived class for "Query CamPower", 
 * one for "Query CamStat", and so forth. Each derived class
 * should implement <code>permission</code> and 
 * <code>execute</code> in the way prescribed by the
 * server's instrumentation API and the Interface Control 
 * Document.
 */
class Command {    
    public:
        /** Checks whether the sender has permission to
         * execute the command with the given parameters.
         *
         * Presently, the Do and Set commands block anyone other than
         * the master client, while the Query and Get commands allow
         * anyone. This behavior can be modified as needed by changing
         * the implementations in DoCommand or SetCommand, or by
         * implementing <code>permission</code> separately for an
         * individual command.
         *
         * @param sender The sender who wishes to execute the command.
         * @param command The command to be executed, broken into tokens.
         *
         * @returns true if the command should be allowed to execute.
         */
        virtual bool permission(const MessageParty sender,
                                const std::vector<std::string> command) = 0;

        /** Executes the command with the specified arguments.
         * 
         * The full command should be passed through in the
         * <code>command</code> argument, including the
         * name of the command.
         *
         * @param sender The party executing the command.
         * @param command 
         */
        virtual std::string execute(MessageParty sender,
                                    std::vector<std::string> command) = 0;

        /** Destructor. */
        virtual ~Command() {};

        /** Checks for the presence of a "/Notify" argument present in a
         * command string, and removes the /Notify argument if it is present.
         * "/N" is also accepted as a substitute for /Notify.
         * @returns true if "/Notify" was found at the end of the command,
         *          false otherwise.
         */
        bool checkNotify(MessageParty& sender,
                         std::vector<std::string>& command);

        /** Sends message to the specified message party indicating that
         * the command represented by this Command object has completed.
         */
        void doNotify(bool notify, MessageParty& sender);

    protected:
        /** The name of the command represented by this Command object. */
        std::string commandName;
};

#endif // ! COMMAND_H

