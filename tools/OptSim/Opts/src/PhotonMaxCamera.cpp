#include <boost/thread.hpp>
#include <boost/bind.hpp>

#include "Opts.h"
#include "PhotonMaxCamera.h"
#include "Image.h"

#include "Shutter.h"
#include "Relay.h"
#include "SunSensor.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H

PhotonMaxCamera::PhotonMaxCamera() 
{
    RegionOfInterest roi = getROI();
    roi.colLo = 0;
    roi.colHi = 1023;
    roi.rowLo = 0;
    roi.rowHi = 1023;

    hCam = 0;

    setFlat(0);
    setBlank(0);
    setBias(0);

    setROI(roi);
    setGeometry(roi);

    setNextSequenceNum(1);

    if (pl_pvcam_init() == FALSE) {
        setError();
        std::cout << "pl_pvcam_init failed." << std::endl;
    } else std::cout << "Camera initialized." << std::endl;

    char cam_name[CAM_NAME_LEN];
    if (pl_cam_get_name(0, cam_name) == FALSE) {
        std::cout << "pl_cam_get_name failed." << std::endl;
    }
    name = cam_name;
    pl_get_param(hCam, PARAM_HEAD_SER_NUM_ALPHA, ATTR_CURRENT, cam_name);
    name += " 0";

    // Set the target camera temperature.
    double targTemp = getTargetTemp();
    pl_ccd_set_tmp_setpoint(hCam, (int)(targTemp * 100));

    cacheTime = boost::posix_time::microsec_clock::universal_time();
    cacheTime -= boost::posix_time::seconds(10);

    thread = new boost::thread();
}

PhotonMaxCamera::~PhotonMaxCamera()
{
    pl_cam_close(hCam);
    pl_pvcam_uninit();
}

std::string PhotonMaxCamera::getName()
{
    return name;
}

void PhotonMaxCamera::setState(Camera::CameraState newState)
{
    rs_bool mult_enable = 1;
    int16 gain = 1;
    uns16 mult_factor = 2000;

    switch (newState) {
        // Switch camera off
        case CAM_POWEROFF:
            pl_cam_close(hCam);
            break;

        // Switch camera on
        case CAM_IDLE:
            char cam_name[CAM_NAME_LEN];
            if (pl_cam_get_name(0, cam_name) == FALSE) {
                std::cout << "pl_cam_get_name failed." << std::endl;
                return;
            }

            name = cam_name;
            if (pl_cam_open(cam_name, &hCam, OPEN_EXCLUSIVE) == FALSE) {
                setError();
                logFile << "pl_cam_open failed with error ";
                logFile << pl_error_code() << std::endl;
                return;
            }

            pl_get_param(hCam, PARAM_HEAD_SER_NUM_ALPHA, ATTR_CURRENT, cam_name);

//            pl_set_param(hCam, PARAM_GAIN_MULT_ENABLE, &mult_enable); 
            pl_set_param(hCam, PARAM_GAIN_INDEX, &gain); 
            pl_set_param(hCam, PARAM_GAIN_MULT_FACTOR, &mult_factor);
            setGain(6);

            name += ' ';
            name += cam_name;
            break;

        default:
            break;
    }

    Camera::setState(newState);
}

void PhotonMaxCamera::getEngStatus(std::stringstream& ss)
{
    ss << "Princeton Instrument PhotonMax Camera\n";

    if (getState() == Camera::CAM_EXPOSING) {
        ss << "Camera engineering status is not available when\n";
        ss << "the camera is exposing.";
        return;
    } 

    int errCode = pl_error_code();
    char errMessage[ERROR_MSG_LEN];
    pl_error_message(errCode, errMessage);

    ss << "Camera status: " << errMessage << std::endl;

    rs_bool pbool;
    int16 p16;
    uns16 pu16;
    int32 p32;
    uns32 pu32;
    flt64 p64;

#define SHOW_PARAM(name, type) \
    if (pl_get_param(hCam, (name), ATTR_CURRENT, &type)) \
        ss << #name << ' ' << type << '\n';

    SHOW_PARAM(PARAM_ACCUM_CAPABLE, pbool)
    SHOW_PARAM(PARAM_ADC_OFFSET, p16)
    SHOW_PARAM(PARAM_ANTI_BLOOMING, pu32)
    SHOW_PARAM(PARAM_BIT_DEPTH, p16)
    SHOW_PARAM(PARAM_CAM_FW_VERSION, pu16)
    SHOW_PARAM(PARAM_CCS_STATUS, p16)

    char * str = new char[CCD_NAME_LEN];
    if (pl_get_param(hCam, PARAM_CHIP_NAME, ATTR_CURRENT, str))
        ss << "PARAM_CHIP_NAME " << str << std::endl;
    delete [] str;

    SHOW_PARAM(PARAM_CLEAR_CYCLES, pu16)
    SHOW_PARAM(PARAM_CLEAR_MODE, pu32)
    SHOW_PARAM(PARAM_COLOR_MODE, pu32)
    SHOW_PARAM(PARAM_CONTROLLER_ALIVE, pbool)
    SHOW_PARAM(PARAM_COOLING_MODE, pu32)
    SHOW_PARAM(PARAM_EDGE_TRIGGER, pu32)
    SHOW_PARAM(PARAM_EXPOSURE_MODE, pu32)
    SHOW_PARAM(PARAM_FRAME_CAPABLE, pbool)
    SHOW_PARAM(PARAM_FWELL_CAPACITY, pu32)
    SHOW_PARAM(PARAM_GAIN_INDEX, p16)
    SHOW_PARAM(PARAM_GAIN_MULT_ENABLE, pbool)
    SHOW_PARAM(PARAM_GAIN_MULT_FACTOR, pu16)

    str = new char[MAX_ALPHA_SER_NUM_LEN];
    if (pl_get_param(hCam, PARAM_HEAD_SER_NUM_ALPHA, ATTR_CURRENT, str))
        ss << "PARAM_HEAD_SER_NUM_ALPHA " << str << std::endl;
    delete [] str;

    SHOW_PARAM(PARAM_INTENSIFIER_GAIN, p16)
    SHOW_PARAM(PARAM_IO_ADDR, pu16)
    SHOW_PARAM(PARAM_IO_BITDEPTH, pu16)
    SHOW_PARAM(PARAM_IO_DIRECTION, pu32)
    SHOW_PARAM(PARAM_IO_STATE, p64)
    SHOW_PARAM(PARAM_IO_TYPE, pu32)
    SHOW_PARAM(PARAM_LOGIC_OUTPUT, pu32)
    SHOW_PARAM(PARAM_MIN_BLOCK, p16)
    SHOW_PARAM(PARAM_MPP_CAPABLE, pu32)
    SHOW_PARAM(PARAM_NUM_MIN_BLOCK, p16)
    SHOW_PARAM(PARAM_NUM_OF_STRIPS_PER_CLR, p16)
    SHOW_PARAM(PARAM_PAR_SIZE, pu16 )
    SHOW_PARAM(PARAM_PCI_FW_VERSION, pu16)
    SHOW_PARAM(PARAM_PIX_PAR_DIST, pu16)
    SHOW_PARAM(PARAM_PIX_PAR_SIZE, pu16)
    SHOW_PARAM(PARAM_PIX_SER_DIST, pu16)
    SHOW_PARAM(PARAM_PIX_SER_SIZE, pu16)
    SHOW_PARAM(PARAM_PIX_TIME, pu16)
    SHOW_PARAM(PARAM_PMODE, pu32)
    SHOW_PARAM(PARAM_POSTMASK, pu16)
    SHOW_PARAM(PARAM_POSTSCAN, pu16)
    SHOW_PARAM(PARAM_PREAMP_DELAY, pu16)
    SHOW_PARAM(PARAM_PREAMP_OFF_CONTROL, pu32)
    SHOW_PARAM(PARAM_PREMASK, pu16)
    SHOW_PARAM(PARAM_PRESCAN, pu16)
    SHOW_PARAM(PARAM_READOUT_PORT, pu32)
    SHOW_PARAM(PARAM_READOUT_TIME, p64)
    SHOW_PARAM(PARAM_SER_SIZE, pu16)
    SHOW_PARAM(PARAM_SERIAL_NUM, pu16)
    SHOW_PARAM(PARAM_SHTR_GATE_MODE, pu32)
    SHOW_PARAM(PARAM_SHTR_CLOSE_DELAY, pu16)
    SHOW_PARAM(PARAM_SHTR_CLOSE_DELAY_UNIT, pu32)
    SHOW_PARAM(PARAM_SHTR_OPEN_DELAY, pu16)
    SHOW_PARAM(PARAM_SHTR_OPEN_MODE, pu32)
    SHOW_PARAM(PARAM_SHTR_STATUS, pu32)
    SHOW_PARAM(PARAM_SKIP_AT_ONCE_BLK, pu32)
    SHOW_PARAM(PARAM_SPDTAB_INDEX, p16)
    SHOW_PARAM(PARAM_SUMMING_WELL, pbool)
    SHOW_PARAM(PARAM_TEMP, p16)
    SHOW_PARAM(PARAM_TEMP_SETPOINT, p16)
}

double PhotonMaxCamera::getCurrTemp()
{
    if (getState() == Camera::CAM_EXPOSING) return currentTemperature;

    /* Don't query the camera for current temperature more than every 
     * three seconds. */
    using namespace boost::posix_time;
    if (microsec_clock::universal_time() - cacheTime < seconds(3)) {
        return currentTemperature;
    }

    cacheTime = microsec_clock::universal_time();

    int16 currTemp;
    if (pl_get_param(hCam, PARAM_TEMP, ATTR_CURRENT, &currTemp) == FALSE)
        return currentTemperature;
    currentTemperature = (double)currTemp / 100.;
    return currentTemperature;
}

void PhotonMaxCamera::setTargetTemp(double temp)
{
    int16 tempInt = (int)(temp * 100);
    pl_set_param(hCam, PARAM_TEMP_SETPOINT, (void *)&tempInt);
    Camera::setTargetTemp(temp);
}

bool PhotonMaxCamera::preExpose()
{
    int result;
    int retries = 3;

    do {
        result = pl_exp_init_seq();
        retries--;
    } while ((result == FALSE) && (retries > 0));

    if (result == FALSE) { 
        setError();
        logFile << "pl_exp_init_seq failed with error ";
        logFile << pl_error_code();
        logFile << std::endl; 
        return false;
    }

    RegionOfInterest roi = getROI();
    rgn_type region;
    region.p1 = roi.rowLo;
    region.p2 = roi.rowHi;
    region.s1 = roi.colLo;
    region.s2 = roi.colHi;
    region.pbin = region.sbin = 1;

    roiOffset = 0;
    if (region.p1 > 1) {
        region.p1 -= 2;
        roiOffset = 2;
    }
    if (region.p1 == 1) {
        region.p1 = 0;
        roiOffset = 1;
    }

    do {
        result = pl_exp_setup_seq(hCam, 1, 1, &region, TIMED_MODE,
                                  getExptime(), &bufferSize);
        retries--;
    } while ((result == FALSE) && (retries > 0));

    if (result == FALSE) {
        setError();
        logFile << "pl_exp_setup_seq failed with error ";
        logFile << pl_error_code();
        logFile << std::endl;
        return false;
    }

    return true;
}

bool PhotonMaxCamera::startExposure(signed short * imagePix)
{
    int result = 0;
    int retries = 4;

    do {
        result = pl_exp_start_seq(hCam, imagePix);
        retries--;
        if (!result) logFile << "pl_exp_start_seq failed!" << std::endl;
    } while (retries && (result == FALSE));

    if (result == FALSE) {
        setError();
        logFile << "pl_exp_start_seq failed with error ";
        logFile << pl_error_code();
        logFile << std::endl;
        return false;
    }

    copyTo = imagePix;
    nCopied = 0;

    return true;
}

Camera::CameraState PhotonMaxCamera::checkStatus()
{
    int16 status = 0;
    uns32 bytes = 0;

    pl_exp_check_status(hCam, &status, &bytes);

    if (status == READOUT_FAILED) {
        setError();
        std::stringstream ss;
        ss << "Error Expose Readout failed: error " << pl_error_code();
        ss << std::ends;
        logFile << "error: " << pl_error_code() << std::endl;
        theOPTS.getInterface() -> send(observer, ss.str());

        return CAM_FAILED;
    }

    if (status == READOUT_COMPLETE) {
        RegionOfInterest roi = getROI();
        if (roiOffset != 0) {
            memmove(copyTo, 
                    copyTo + roiOffset * (roi.colHi - roi.colLo + 1),
                    bytes);
            logFile << "Bytes: " << bytes << std::endl;
        }
        return CAM_COMPLETE;
    }
    return CAM_EXPOSING;
}


bool PhotonMaxCamera::postExpose()
{
    if (pl_exp_finish_seq(hCam, copyTo, 0) == false) {
        setError();
        logFile << "pl_exp_finish_seq failed." << std::endl;
        return false;
    }

    if (pl_exp_uninit_seq() == false) {
        setError();
        logFile << "pl_exp_uninit_seq failed." << std::endl;
        return false;
    }

    return true;
}

void PhotonMaxCamera::sendAbort()
{
    pl_exp_abort(hCam, CCS_NO_CHANGE);

    int16 status = 0;
    uns32 bytes = 0;

    do {
        pl_exp_check_status(hCam, &status, &bytes);
    } while (status != READOUT_NOT_ACTIVE);
    
}

signed short * PhotonMaxCamera::getPixels()
{
    return copyTo;
}
