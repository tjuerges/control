#include <limits>
#include <vector>
#include <cmath>

#include "Centroid.h"
#include "Camera.h"
#include "Opts.h"

Centroid::Centroid(Centroid& from)
{
    param1 = from.param1;
    param2 = from.param2;
    param3 = from.param3;
}

void Centroid::go()
{
    xctr = yctr = -1;
    std::pair<int, int> guess = findMaximum();

    xctr = guess.first;
    yctr = guess.second;

    if ((guess.first == -1) && (guess.second == -1)) {
        std::cout << "Centroid for image " << img -> getSequenceNumber();
        std::cout << " failed. Could not get initial guess." << std::endl;

        invalidate();
        return;
    }

    calculateCentroid(guess);
}

std::pair<int,int> Centroid::findMaximum()
{
    std::pair<int,int> max;
    int maxValue = -1000000;
    int i,j;
    RegionOfInterest roi = img -> getROI();

    const signed short * pixels = img -> getPixels();
    const signed short * ptr = pixels;

    max.first = max.second = -1;

    int columns = roi.colHi - roi.colLo + 1;
    int numPixels = columns;
    numPixels *= roi.rowHi - roi.rowLo + 1;

/**********
    for (i = 5; i < roi.rowHi - roi.rowLo - 4; i++) {
        for (j = 5; j < roi.colHi - roi.colLo - 4; j++) {
            if (pixels[columns * j + i] > maxValue) {
                maxValue = *ptr;
                max.first = j;
                max.second = i;
            }
        }
    }
********/
    for (i = 0; i < numPixels; i++) {
        if (*ptr > maxValue) {
            maxValue = *ptr;
            max.first = i % columns;
            max.second = i / columns;
        } 
        ptr++;
    }

    max.first += roi.colLo;
    max.second += roi.rowLo;

    return max;
}
    

void Centroid::calculateCentroid(std::pair<int, int> guess)
{
    int x = guess.first;
    int y = guess.second;

    int boxSize = (int)param1;
    double sigclip = param2;

    // Make sure boxSize is odd.
    if ((boxSize % 2) == 0) boxSize++; 
    param1 = boxSize;

    logFile << "Initial guess at " << guess.first << ", " << guess.second << std::endl;

    // Make sure the box is entirely inside the image.
    RegionOfInterest roi = img -> getROI();
    if ((guess.first <= boxSize / 2 + 1) ||
        (guess.second <= boxSize / 2 + 1) ||
        (guess.first >= roi.colHi - boxSize / 2 - 1) ||
        (guess.second >= roi.rowHi - boxSize / 2 - 1))
    {
        logFile << "Centroid for image " << img -> getSequenceNumber(); 
        logFile << " rejected. Initial guess too close to image edge."; 
        logFile << std::endl;

        invalidate();
        return;
    } 

    // Construct a box around the center pixel.
    double box[boxSize][boxSize];

    int i,j;
    for (i = 0; i < boxSize; i++) {
        for (j = 0; j < boxSize; j++) {
            box[j][i] = img -> getPixel(i + x - boxSize / 2, 
                                        j + y - boxSize / 2);
        }
    }

    // Compute the standard deviation of the perimeter pixels...
    double mean = 0;
    double sumSquare = 0;

    // Start with the top and bottom row
    for (i = 0; i < boxSize; i++) {
        mean += box[0][i];
        sumSquare += box[0][i] * box[0][i];
        mean += box[boxSize-1][i];
        sumSquare += box[boxSize-1][i] * box[boxSize-1][i];
    }

    // Then add the left and right sides
    for (i = 1; i < boxSize-1; i++) {
        mean += box[i][0];
        sumSquare += box[i][0] * box[i][0];
        mean += box[i][boxSize-1];
        sumSquare += box[i][boxSize-1] * box[i][boxSize-1];
    }

    mean /= 4 * (boxSize - 1);    // Convert the sum to an average.
    rms = std::sqrt(sumSquare / (4 * (boxSize - 1)) - mean * mean);
    double clippingThresh = mean + sigclip * rms;
    logFile << "Threshold established at " << clippingThresh << std::endl;

    // Get zeroeth, first, and second moments. We will use these to calculate
    // the centroid and the FWHM.
    int n = 0;
    double sum = 0;
    peak = 0;
    fwhm_x = fwhm_y = 0.;
    xctr = yctr = 0.;
    for (i = 0; i < boxSize; i++) {
        for (j = 0; j < boxSize; j++) {
            peak += box[j][i] - mean;
            if (box[j][i] < clippingThresh) continue;

            n++;

            sum += box[j][i] - mean;
            xctr += i * (box[j][i] - mean);
            yctr += j * (box[j][i] - mean); 
        }
    }

    if (sum > 0) {
        xctr /= sum;
        yctr /= sum;
    }

    if (n < 5) {
        logFile << "Centroid for image " << img -> getSequenceNumber();
        logFile << " rejected. Too few pixels above threshold." << std::endl;
        invalidate();

        xctr += x - (boxSize - 1) / 2;
        yctr += y - (boxSize - 1) / 2;

        return;
    }


    int scan = (int)yctr;
    double ix = 0;
    int sum_ix = 0;
    for (i = 0; i < boxSize; i++) {
        for (j = scan; j <= scan + 1; j++) {
            if (box[j][i] < clippingThresh) continue;
            fwhm_x += box[j][i] * (i - xctr) * (i - xctr);
            sum_ix += box[j][i];
        }
    }
    fwhm_x = 2.355 * sqrt(fwhm_x/sum_ix);

    scan = (int)xctr;
    ix = 0;
    sum_ix = 0;
    for (i = scan; i <= scan + 1; i++) {
        for (j = 0; j < boxSize; j++) {
            if (box[j][i] < clippingThresh) continue;
            fwhm_y += box[j][i] * (j - yctr) * (j - yctr);
            sum_ix += box[j][i];
        }
    }
    fwhm_y = 2.355 * sqrt(fwhm_y/sum_ix);

    if (isnan(fwhm_x)) fwhm_x = -1;
    if (isnan(fwhm_y)) fwhm_y = -1;

    xctr += x - (boxSize - 1) / 2;
    yctr += y - (boxSize - 1) / 2;

    sn = peak / (rms * boxSize);

    logFile << "Computed centroid at (" << xctr << "," << yctr << ")\n";
    logFile << "Signal to noise: " << sn << std::endl;
}

void Centroid::invalidate()
{
   peak = 0;
   fwhm_x = fwhm_y = -1;
   rms = sn = 0;
}
