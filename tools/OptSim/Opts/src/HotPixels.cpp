#include "HotPixels.h"

#include <vector>
#include <map>
#include <iostream>
#include <Opts.h>

void HotPixels::add(int x, int y)
{
    int n = 1024 * y + x;
    pixList[n] = 1;
}

void HotPixels::del(int x, int y)
{
    int n = 1024 * y + x;
    pixList.erase(n);
}

void HotPixels::deleteAll()
{
    pixList.clear();
}

bool HotPixels::includes(int x, int y)
{
    int n = 1024 * y + x;
    if (pixList.count(n) == 0) return false;
    return true;
}

std::vector<std::pair<int, int> > HotPixels::getPixels()
{
    std::vector<std::pair<int, int> > pix;

    std::map<int, int>::iterator it = pixList.begin();

    while (it != pixList.end()) {
        int n = (*it).first;
        int x = n % 1024;
        int y = n / 1024;

        pix.push_back(std::pair<int, int>(x, y));
        it++;
    }

    return pix;
}
