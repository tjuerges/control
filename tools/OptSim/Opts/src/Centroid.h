#ifndef CENTROID_H
#define CENTROID_H

#include "Image.h"

/** Contains the setup and results for a centroid calculation. The method by
 * which a centroid is computed can be changed by modifying this class or
 * by creating a derived class. The OPTS can offer multiple centroid algorithms
 * simultaneously. The user selects a centroid algorithm using the Set CentAlg
 * command. To allow the user to select between this centroid algorithm and a
 * new one, create a class derived from this one, and incorporate the new
 * class into the CentroidFactory.
 */
class Centroid {
    public:
        /** Constructor. */
        Centroid() {};

        /** Constructor. */
        Centroid(Centroid& from);

        /** Destructor. The destructor should be virtual, since the class
         * contains virtual methods.
         */
        virtual ~Centroid() {};

        /** Specifies the image that this Centroid object should operate on.
         * @param img the image for which a centroid should be calculated.
         */
        void setImage(Image * img) { this -> img = img; } 

        /** Starts the calculation of a centroid. A call to setImage() should
         * be made before this method is called. This method works by calling
         * findMaximum() to get an initial guess of the centroid position, and
         * then calling calculateCentroid using that initial guess to refine
         * the centroid and compute the flux, signal-to-noise, etc.
         */
        virtual void go();

        /** Finds an initial guess of the centroid position. It is difficult
         * and slow to find a robust initial guess that avoids hot pixels,
         * saturation, and so on. We are starting off with a naive algorithm
         * that finds the peak pixel and returns that as the initial guess.
         * This method relies on either the camera to not have any hot pixels,
         * or the user to identify them correctly.
         * @returns a std::pair object containing the pixel coordinates of
         *          the presumed peak of the PSF.
         */
        virtual std::pair<int,int> findMaximum();

        /** Performs a precision computation of the centroid using an initial 
         * guess as a starting point. This algorithm works by drawing a box 
         * centered on the initial guess, using the pixels in this box to
         * compute a zeroth, first, and second moment, and using these to
         * compute the FWHM, S/N, and so on. This method works well as long as
         * the box is large enough that the edges of the box are not
         * significantly brighter than the corners. A circular aperture with
         * annulus would probably work better, but this seems adequate for
         * just getting a centroid.
         *
         * A call to calculateCentroid() should leave xctr, yctr, fwhm_x,
         * fwhm_y, peak, and sn set.
         */
        virtual void calculateCentroid(std::pair<int,int> guess);

        /** Returns the intensity of the peak, i.e., the total flux from the
         * star minus the background.
         */
        double getPeak() const { return peak; };

        /** Returns the X-coordinate of the computed centroid. 
         * @returns the X-coordinate of the computed centroid. 
         */
        double getX() const { return xctr; };

        /** Returns the X-coordinate of the computed centroid. 
         * @returns the X-coordinate of the computed centroid. 
         */
        double getY() const { return yctr; };

        /** Returns the FWHM of the target in the X-coordinate. The value
         * is normalized so that, for a circular source object, the value
         * returned is the same as the FWHM.
         * @returns the FWHM of the source in the X-axis.
         */
        double getFwhmX() const { return fwhm_x; };

        /** Returns the FWHM of the target in the Y-coordinate. The value
         * is normalized so that, for a circular source object, the value
         * returned is the same as the FWHM.
         * @returns the FWHM of the source in the Y-axis.
         */
        double getFwhmY() const { return fwhm_y; };

        /** Returns the RMS noise of the sky background found near the
         * location of the centroid.
         * @returns the RMS noise of the sky background.
         */
        double getRMS() const { return rms; };

        /** Returns the threshold signal to noise level above which a
         * centroid is considered valid.
         * @returns the signal-to-noise acceptance threshold of this centroid.
         */
        double getThreshold() const { return threshold; }

        /** Sets the threshold signal-to-noise level above which the centroid
         * is considered acceptable.
         * @param thresh the new signal-to-noise threshold for this centroid.
         */
        void setThreshold(double thresh) { threshold = thresh; }

        /** Returns an integer representing this particular algorithm for the
         * user. The user selects which algorithm the software should use
         * by its algorithm ID. The ICD dictates that the algorithm ID be
         * one of 1, 2 or 3.
         * @returns the algorithm ID for this centroiding algorithm.
         */
        virtual int getAlgorithmID() const { return 1; }

        /** Sets centroiding parameter 1. The meaning of this parameter is
         * assigned by each centroiding algorithm. For the base algorithm,
         * it is the box size. 
         * @param p1 the new value for parameter 1.
         */
        void setParam1(double p1) { param1 = p1; };

        /** Returns centroiding parameter 1. The meaning of this parameter is
         * assigned by each centroiding algorithm. For the base algorithm,
         * it is the box size. 
         * @returns centroiding parameter 1.
         */
        double getParam1() const { return param1; };

        /** Sets centroiding parameter 2. The meaning of this parameter is
         * assigned by each centroiding algorithm. For the base algorithm,
         * it is the sigma value for the clipping threshold.
         * @param p2 the new value for parameter 2.
         */
        void setParam2(double p2) { param2 = p2; };

        /** Returns centroiding parameter 2. The meaning of this parameter is
         * assigned by each centroiding algorithm. For the base algorithm,
         * it is the sigma value for the clipping threshold.
         * @returns centroiding parameter 2.
         */
        double getParam2() const {return param2; };

        /** Sets centroiding parameter 3. The meaning of this parameter is
         * assigned by each centroiding algorithm. For the base algorithm,
         * it is unused.
         * @param p3 the new value for parameter 3.
         */
        void setParam3(double p3) { param3 = p3; };

        /** Returns centroiding parameter 3. The meaning of this parameter is
         * assigned by each centroiding algorithm. For the base algorithm,
         * it is unused.
         * @returns centroiding parameter 3.
         */
        double getParam3() const { return param3; };

        /** Returns the signal to noise ratio of the computed centroid.
         * @returns the signal to noise ratio of the computed centroid.
         */
        double getSignalToNoise() const { return sn; }

        /** Returns true if the centroid is considered value.
         * @returns a boolean indicating whether the centroid is valid.
         */
        bool getValid() const { return (getSignalToNoise() > threshold); }

    protected:
        /** Marks the centroid data for this image as invalid due to
         * poor signal to noise or lack of an acceptable PSF.
         */
        void invalidate();

    private:
        /** The image for which a centroid is computed. */
        Image * img;

        /** The peak intensity of the centroid star, in counts. */
        double peak;

        /** The X-coordinate of the centroid. */
        double xctr;
        /** The Y-coordinate of the centroid. */
        double yctr;

        /** The X-axis FWHM of the centroid. */
        double fwhm_x;
        /** The Y-axis FWHM of the centroid. */
        double fwhm_y;

        /** The RMS noise of the sky background in counts. */
        double rms;
        /** The signal to noise ratio for the centroid star. */
        double sn;

        /** The acceptance threshold of the centroid. If the signal-to-noise
         * ratio of the centroid is higher than this, the centroid is
         * considered "good".
         */
        double threshold;

        /** Centroiding parameter 1. The meaning of this parameter depends on
         * how it is assigned by the centroiding algorithm. For the base
         * algorithm, this is the box size.
         */
        double param1;

        /** Centroiding parameter 2. The meaning of this parameter depends on
         * how it is assigned by the centroiding algorithm. For the base
         * algorithm, this is the sigma for the clipping threshold.
         */
        double param2;

        /** Centroiding parameter 3. The meaning of this parameter depends on
         * how it is assigned by the centroiding algorithm. For the base
         * algorithm, this is unused.
         */
        double param3;
};

#endif // ! CENTROID_H
