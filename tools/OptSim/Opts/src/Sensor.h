#ifndef SENSOR_H
#define SENSOR_H

/** Provides an interface to an analog sensor in the instrument.
 */
class Sensor {
    public:
        /** Constructor. */
        Sensor() {};

        /** Destructor. */
        virtual ~Sensor() {};
 
        /** Gets a reading from the sensor.
         * @returns a double float representing the value read from the sensor.
         */
        virtual double read() { return 0.; }
};

#endif // !SENSOR_H
