#ifndef DIGITALINPUT_H
#define DIGITALINPUT_H

/** Represents an input device that can be in either an "on" or "off" state.
 * The digital input is used for the shutter limits.
 * @see GalilDigitalInput
 */
class DigitalInput {
    public:
        /** Constructor. */
        DigitalInput() : defaultState(false) {}
        /** Destructor. This must be virtual, since there are virtual methods
         * in this class. 
         */
        virtual ~DigitalInput() {}

        /** Sets the default value for the input. This value is used if
         * communication with the input fails for some reason. It is also
         * used when the software is run in test mode.
         * @param ds the default state of the input.
         */
        void setDefault(bool ds) { defaultState = ds; }

        /** Returns the default value of the input. This value will be used
         * if a real input result is unavailable for some reason.
         * @returns the default state of the input.
         */
        bool getDefault() { return defaultState; }

        /** Returns a reading from the digital input device.
         * @returns a reading from the digital input device.
         */ 
        virtual bool check() { return defaultState; }

    private:
        /** The default state of the device, used if communication with
         * the hardware device fails.
         */
        bool defaultState;
};

#endif // !DIGITALINPUT_H
