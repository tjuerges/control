#ifndef GALILSTEPPER_H
#define GALILSTEPPER_H

#include <Galil.h>
#include "Stepper.h"

class GalilStepper : public Stepper {
    public:
        GalilStepper(Galil * board, char axis);

        Stepper::State getState() const;

        int getPosition();
        void setPosition(int pos);

        bool begin();

        void on();
        void off();

        void setEncoderGear(double g);

        void setSpeed(int sp);
        void setAccel(int ac);
        void setDecel(int ac);

    protected:
        void goTo(int pos);
        void jog(int steps);
        void goHome();
        void findIndex(int direction);

        double sendQuery(const char * cmd) const;

    private:
        mutable Galil * galil;
        char axis;

        /** True if the stepper motor has a coupled encoder. */
        bool useEncoder;
};

#endif // GALILSTEPPER_H
