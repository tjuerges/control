#include <iostream>
#include <boost/thread.hpp>
#include <boost/bind.hpp>

#include <signal.h>
#include <unistd.h>

#if HAVE_CONFIG_H
#include <config.h>
#endif

#include "Opts.h"
#include "SunSensor.h"
#include "Image.h"
#include "FilterWheel.h"
#include "Focuser.h"

std::ofstream logFile;
extern Opts theOPTS;

int main (int argc, char * const argv[]) {
    signal(SIGPIPE, SIG_IGN);
    //    daemon(0,1);

    theOPTS.getFilterWheel() -> init();
    theOPTS.getFocuser() -> init();

    // Start the sun sensor monitor.
    boost::thread thread1(boost::bind(&SunSensor::monitor,
        theOPTS.getSunSensor1()));
    boost::thread thread2(boost::bind(&SunSensor::monitor,
        theOPTS.getSunSensor2()));
    boost::thread camMonitor(boost::bind(&Opts::monitorCamera,
        theOPTS.getOpts()));


    // Start the image processing thread.
    boost::thread imageProcess(boost::bind(&Image::process));


    // Open a log at /var/log/opts.log, or at the location
    // chosen by the user.
    char * logFileName = getenv("LOGFILE");
    if (logFileName == NULL) {
        logFile.open("/var/log/opts.log", std::ios::app);
    } else {
        logFile.open(logFileName, std::ios::app);
    }

    logFile << "Welcome to " PACKAGE_STRING << std::endl;

    // Listen for commands.
    try {
        DataSequencer::getInstance() -> listen();
    } catch(std::string ex) {
        logFile << "Exception: " << ex << std::endl;
    }

    return 0;
}

/** @mainpage Overview
 *
 * This documentation is intended for users who wish to modify the server code
 * for the ALMA Optical Pointing Telescope System. Users who wish to learn to
 * use the Optical Pointing Telescope should consult the User's Guide. If you
 * are building a third-party client for the OPTS, you might find the OPTS
 * Interface Control Document easier to read, and you should use this to
 * complement the ICD.
 *
 * <h2>About the OPTS</h2>
 * The ALMA OPTS is a small optical telescope mounted to the rear of the main
 * antenna. The purpose of the OPTS is to measure precisely the pointing error
 * of an ALMA Antenna. An OPTS contains a small refracting telescope with
 * shutter, filter wheel, imaging camera, and various miscellaneous sensors.
 *
 * The OPTS software consists of a server residing on the Telescope Control
 * Module, and a client which may run from any number of other machines.
 * The computer running the OPTS software is a VIA EPIA M-700 with 2 GB RAM
 * and a 4 GB flash drive. The server and client communicate via the ARO
 * Socket Library, written by Jeff Hagen, Bill Peters, and Tom Folkers, using
 * a simple plain-text command syntax. The server interprets the commands
 * and interfaces with the OPTS hardware.
 *
 * <h2>The OPTS Server Code</h2>
 * The OPTS server is built according to the ALMA C++ Coding Standards, and
 * has been carefully designed for high maintainability. The code base relies
 * heavily on object orientation and abstraction. Each implementation-specific
 * component in the OPTS server is wrapped inside a generic class. Any
 * migration of the OPTS to different hardware should be done by creating
 * new class to replace the existing hardware, and wrapping it in the same way.
 * This will ensure minimal effort when making changes in the OPTS.
 *
 * The OPTS server can be broken into three major parts: the client interface,
 * the command interpreter, and the hardware interface. Commands issued from
 * the client progress through the OPTS server as shown in the figure below.
 *
 * @image html img/opts_callstack.png
 *
 * The ClientInterface class handles communication between the server and
 * the client. This class is used by making calls to the <code>send</code>
 * or <code>receive</code> methods, or the <code>sendFile</code> and
 * <code>receiveFile</code> methods.
 *
 * The DataSequencer manages the user session, receiving commands from the
 * ClientInterface, parsing them, and passing them along to the appropriate
 * command handler.
 *
 * The Command class is an abstract class that defines the interface between
 * the data sequencer and the control code. Each individual command is derived
 * from the Command class. Command objects must register themselves with the
 * data sequencer so that they can be called.
 *
 * For each device the OPTS server controls, there is a class representing
 * that device. Each device has an API defined by this class, which the rest
 * of the server code can use to control that device. These general classes
 * should not have code specific to the hardware they control. Instead, they
 * should either have derived classes with instrument-specific code -- as is
 * done with the PhotonMax CCD Camera -- or they should make calls to a
 * separate control object -- as is done with the Galil controller.
 *
 * <h2>Adding a New Protocol Command</h2>
 * The protocol commands are implemented as classes derived from the Command
 * class. Four abstract classes derive from Command: GetCommand, QueryCommand,
 * SetCommand, and DoCommand. The easiest way to implement a new command is
 * to derive from one of these classes.
 *
 * The new Command class should override the execute method of the Command
 * class. The execute method will take two arguments. The first of these
 * is a MessageParty containing information about the client that issued the
 * command. The second argument is a vector of STL strings containing the
 * tokens of the command. For example, the command "Do Expose 1 1" will send
 * {"Do", "Expose", "1", "1"} as the second argument to the execute() method.
 *
 * If desired, you may override the default permissions behavior by overriding
 * the permission() method. This method should return true if the command will
 * be allowed, or false if the command should be rejected.
 *
 * Some commands take "/Notify" as an optional argument. If the new command
 * is to accept the "/Notify" syntax, it can do so by calling checkNotify()
 * at the beginning of the execute() method. This will store information about
 * whether to send a notification, and it will clean the "/Notify" argument
 * from the contents of the command. The checkNotify() method will return true
 * if a notification was requested. When the command is finished, call
 * doNotify() to send the notification.
 *
 * Exactly one instance of a command object should be created for each command.
 * This will automatically register the command in the data sequencer. This
 * is done by creating a global instance of the command.
 *
 * When creating a new command, keep in mind that the function that implements
 * the command should be re-entrant, so that the command can be executed from
 * multiple clients simultaneously.
 *
 * <h2>Adding a Centroiding Algorithm</h2>
 * Centroiding algorithms are represented by the Centroid object. One Centroid
 * object is created for each exposure. Centroids are created by the
 * CentroidFactory class.
 *
 * To create a new centroiding algorithm, create a class derived from the
 * Class. The derived class can override either or both of the findMaximum()
 * or calculateCentroid() methods. The findMaximum() method makes an initial
 * guess of the centroid position by finding the brightest pixel in the image.
 * The calculateCentroid() method refines the initial guess and fills in all
 * centroid data.
 *
 * These methods should fill in the correct values for the class member
 * variables. The variables to be filled in are peak, xctr, yctr, fwhm_x,
 * fwhm_y, rms, and sn. The parameter members and threshold will be set for
 * you.
 *
 * Finally, register the centroiding algorithm with an ID name by editing the
 * getCentroid() method of the CentroidFactory class. Users will be able to
 * select your algorithm using the ID number you specify.
 *
 * <h2>Overview of Instrument Classes</h2>
 * The OPT instrumentation is accessed through a hierarchy of classes that
 * abstract each hardware item. The classes are collected under a single class
 * named Opts. This class has members corresponding to the hardware on the
 * OPT.
 *
 * The PhotonMaxCamera class represents the PhotonMax camera used in the OPT.
 * This class derives from the Camera object, which defines imaging routines
 * that are not specific to the PhotonMax camera.
 *
 * The Focuser, Shutter, SunSensor, and ThermalSensor classes
 * represent the corresponding instruments on the OPT. These classes rely
 * on other classes that provide access the Galil motion controller.
 *
 * Motion control is represented by the DigitalInput and Relay classes, which
 * provide digital input and output, by the AnalogInput class, which provides
 * analog input, and the Stepper class, which controls a stepper motor. The
 * Stepper class cannot be used directly. Instead, a helper to the Stepper
 * class, StepperControl, should be used to moderate access to the stepper
 * motor. This ensures that access to the stepper motors occurs in a thread-
 * safe way.
 *
 * The FilterWheel class represents the filter wheel. The FilterWheel class
 * represents filter positions using the FilterWheelPosition class. The
 * FilterWheel class is abstract. The StepperFilterWheel class implements
 * the FilterWheel with control by a stepper motor, in which the filter wheel
 * moves through its positions by incrementing the stepper motor by a
 * pre-determined amount.
 *
 * Refer to the following chapters for a complete discussion of the behaviors
 * of these classes and their support classes.
 *
 * <h2>Threading Behavior</h2>
 * The OPT server breaks off new threads for a number of purposes. These threads
 * are:
 * - A thread to handle the camera. This thread sends commands to the camera,
 *   reads the image data from the camera, and passes the image to the image
 *   processing thread.
 * - A thread for image processing. This thread takes images from the camera,
 *   performs calibrations, computes the centroid, and builds a FITS image.
 * - One thread each for the sun sensors. These threads monitor the state of
 *   the sun sensors, and orders the OPT to enter its protection mode if
 *   its assigned sun sensor trips.
 * - "Get" commands break off a new thread when the command calls for an image
 *   that is not yet ready to be sent. The thread terminates once the command
 *   has finished executing.
 * - "Do" commands that require a notification break off a thread to perform
 *   their action. The thread terminates after completion of the command.
 *
 *
 */
