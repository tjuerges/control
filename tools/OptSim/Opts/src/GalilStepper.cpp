#include "GalilStepper.h"
#include "Opts.h"

#include <sstream>

#include <iostream>

#include <Galil.h>

GalilStepper::GalilStepper(Galil * board, char theAxis) : 
Stepper(),
axis(theAxis),
galil(board)
{
    useEncoder = false;

    try {
        // Tell the Galil board that this is a stepper motor
        // with active low step pulses.
        std::stringstream ss;
        ss << "MT" << axis <<  "=2";
        galil -> command(ss.str());   // MTx=2
    
        ss.str("");
        ss << "YA" << axis << "=16";
        galil -> command(ss.str());
    
        ss.str("");
        ss << "AC" << axis << "=128000";
        galil -> command(ss.str());
    
        ss.str("");
        ss << "DC" << axis << "=128000";
        galil -> command(ss.str());
    
        ss.str("");
        ss << "SP" << axis << "=128000";
        galil -> command(ss.str());
    
        galil -> command("OE 1,1"); 
        galil -> command("CN 1,1");

        setEncoderGear(1);
        on();
    } catch (std::string ex) {
        logFile << "Exception: " << ex << std::endl;
    }
} 

Stepper::State GalilStepper::getState() const
{ 
    if (Stepper::getState() == Stepper::STUCK) return Stepper::STUCK;

    try {
        if (sendQuery("MG _LR") < 0.01) return Stepper::LIMIT_DN;
        if (sendQuery("MG _LF") < 0.01) return Stepper::LIMIT_UP;
        if (sendQuery("MG _BG") < 0.01) return Stepper::STOPPED;
        if (sendQuery("TV ") > 0) return Stepper::MOVING_UP;
        return Stepper::MOVING_DN;
    } catch (std::string ex) {
        logFile << "Exception: " << ex << std::endl;
    }

    return Stepper::UNKNOWN;
}

int GalilStepper::getPosition()
{
    std::string cmd("TP");
    cmd += axis; 
    std::stringstream result(galil -> command(cmd));  // TPx

    // Parse the result and send it back.
    result >> position;
    return position / getEncoderGear() - getEncoderOffset();
}

void GalilStepper::setPosition(int pos)
{
    std::stringstream ss;

    ss << "DP" << axis << "=" << pos;
    galil -> command(ss.str());     // DPx=<pos>

    int encoderOffset = getEncoderOffset();
    encoderOffset += getPosition();
    setEncoderOffset(encoderOffset);
}

void GalilStepper::goTo(int pos)
{
    setAbortState(false);
    on();

    std::stringstream ss;
    ss << "SP" << axis << "=" << getSpeed();
    galil -> command(ss.str());

    // The command to send is PAx=nnn, "Position Absolute"
    ss.str("");
    ss << "PA" << axis << "=" << pos;
    galil -> command(ss.str());  // PAx=<pos>

    begin();

    if (useEncoder) {
        // Set a slower speed for the error correction.
        ss.str("");
        ss << "SP" << axis << "=" << getSpeed() / 10;
        galil -> command(ss.str());

        // Issue the positioning correction...
        ss.str("");
        ss << "YR" << axis << "=_QS" << axis << std::endl;
        galil -> command(ss.str()); // YRx=_QSx

        // Wait for motion to complete.
        ss.str("");
        ss << "MC" << axis;
        galil -> command(ss.str());

        // Reset speed to original.
        ss.str("");
        ss << "SP" << axis << "=" << getSpeed();
        galil -> command(ss.str()); 
    }
}

void GalilStepper::jog(int steps)
{
    setAbortState(false);

    // Set the motion speed.
    std::stringstream ss;
    ss << "JG" << axis << "=" << getSpeed();
    galil -> command(ss.str());  // JGx=<speed>

    // The command to send is PRx=nnn, "Position Relative"
    ss.str("");
    ss << "PR" << axis << "=" << steps;
    galil -> command(ss.str());  // PRx=<steps>

    begin();
}

void GalilStepper::goHome()
{
    setAbortState(false);

    // Send the motor backwards to the reverse limit.
    // We will send the motor backwards a bunch and hope it's enough.
    std::string cmd("PA");
    cmd += axis; 
    cmd += "=-21474836"; //58";
    galil -> command(cmd);     // PAx=-2147483658

    // Begin and wait for motion to complete.
    if (begin()) {
        // Motion failed or was aborted.
        logFile << "Home aborted." << std::endl;
        return;
    }
    logFile << "Found limit." << std::endl;

    std::string checkLim("MG _LR");
    checkLim += axis;
    double atLimit = 0;

    int retries = 10;

    do { 
        findIndex(1);
        std::stringstream result(galil -> command(checkLim));
        result >> atLimit;
    } while ((--retries) && (atLimit < 0.5));

    if (retries) logFile << "Found index." << std::endl;
    else {
        logFile << "Gave up while looking for the index pulse." << std::endl;
        setState(Stepper::STUCK);
    }
}

bool GalilStepper::begin()
{
    using namespace boost::posix_time;

    setAbortState(false);

    std::string begin("BG");
    begin += axis;
    try {
        galil -> command(begin);
    } catch (std::string ex) {
        return false;
    }

    std::string cmd("MG_BG");
    cmd += axis;
    
    ptime start(microsec_clock::universal_time());

    double motionStatus;
    do {
        try {
            std::stringstream result(galil -> command(cmd));
            result >> motionStatus; 

            if (result.fail()) {
                logFile << "Could not parse response from Galil: " << std::endl;
                logFile << "    \"" << result.str() << "\"" << std::endl;
                return true;
            }
        } catch (std::string ex) {
            logFile << "At BG: " << ex << std::endl;
        }

        // Check whether motion was aborted
        if (getAbortState()) {
            logFile << "Galil axis " << axis << " aborted." << std::endl;

            std::string stopCmd("ST");
            stopCmd += axis;
            galil -> command(stopCmd);

            setAbortState(false);
            return true;
        }

        // Check whether motion timed out
        if (microsec_clock::universal_time() - start > seconds(60)) {
            logFile << "Motion on axis " << axis << " timed out." << std::endl;

            std::string stopCmd("ST");
            stopCmd += axis;
            galil -> command(stopCmd);

            setState(Stepper::STUCK);

            return true;
        }
   } while(motionStatus > 0.1);

    return false;
}

void GalilStepper::on()
{
    std::stringstream ss;
    ss << "SH" << axis;
    galil -> command(ss.str());
}

void GalilStepper::off()
{
    std::stringstream ss;
    ss << "ST" << axis;
    galil -> command(ss.str());

    ss.str("");
    ss << "MO" << axis;
    galil -> command(ss.str());
}

void GalilStepper::findIndex(int direction)
{
   int jogSpeed = getSpeed() * direction / 10;
   std::stringstream cmd;

   // Set the direction to move
   cmd << "JG" << axis << "=" << jogSpeed;

   try {
       galil -> command(cmd.str());         // JGx=+100
   } catch (std::string ex) {
       logFile << "Exception: " << ex << std::endl;
   }

   // Set find index mode.
   cmd.str("");
   cmd << "FI" << axis;
   try {
       galil -> command(cmd.str());         // FIx
   } catch (std::string ex) {
       logFile << "Exception: " << ex << std::endl;
   }

   // Begin motion.
   if (begin()) throw std::string("Motion could not complete "
                                  "during home.");

   cmd.str("");
   cmd << "JG" << axis << "=" << getSpeed();
   try {
       galil -> command(cmd.str());         // JGx=+100
   } catch (std::string ex) {
       logFile << "Exception: " << ex << std::endl;
   }
}

void GalilStepper::setEncoderGear(double g)
{
    Stepper::setEncoderGear(g);

    std::stringstream ss;
    ss << "YC" << axis << "=10000";
    galil -> command(ss.str());

    ss.str("");
    ss << "YS" << axis << "=1";
    galil -> command(ss.str());

    useEncoder = true;
}

void GalilStepper::setSpeed(int sp)
{
    Stepper::setSpeed(sp);

    std::stringstream ss;
    ss << "SP" << axis << "=" << sp;
    galil -> command(ss.str());
}

void GalilStepper::setAccel(int ac)
{
    Stepper::setAccel(ac);

    std::stringstream ss;
    ss << "AC" << axis << "=" << ac;
    galil -> command(ss.str());
}

void GalilStepper::setDecel(int dc)
{
    Stepper::setDecel(dc);

    std::stringstream ss;
    ss << "DC" << axis << "=" << dc;
    galil -> command(ss.str());
}

double GalilStepper::sendQuery(const char * cmd) const
{
    std::string str(cmd);
    str += axis;

    double ret = 0;
    std::stringstream result(galil -> command(str));
    result >> ret;
    return ret;
}
