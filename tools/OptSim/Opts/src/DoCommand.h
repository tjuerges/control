#ifndef DOCOMMAND_H
#define DOCOMMAND_H

#include "Command.h"


/** Base class for "do" commands.
 * This class implements <tt>registerCommand()</tt>
 * so that instances of classes derived from this one
 * are listed in the "do" command map. By default,
 * do commands can be executed only by the Master.
 */
class DoCommand : public Command {
    public:
        /** Constructor.
         * To implement a Do command, call this constructor 
         * with the name of the command, e.g., DoCommand("Bias")
         *
         * @param name the name of the new command.
         */
        DoCommand(const char * name);

        virtual bool permission(MessageParty sender, 
                                std::vector<std::string> command);
};

/** Command to take a new Bias exposure or zero 
 * out the current one.
 *
 * (A Bias image is an image with an exposure time of
 * zero seconds.)
 * <pre>
 * Command:
 *    Do Bias [zero] [/Notify]
 * </pre>
 * If the word "zero" is present, no exposure is taken,
 * instead the OPTS will no longer subtract the Bias
 * image (or subtract zero) from new images. When
 * the Bias image is zero, the FITS header should have 0
 * for the value of BIASNUM. /Nofity is ignored for
 * <tt>Bias zero</tt>.
 *
 * <pre>
 * Response:
 *     Bias OK
 *  or  Bias zeroed
 *  or  Error Bias bad
 *  </pre>
 */
class DoBias : public DoCommand {
    public:
        /// Construct and register the command object.
        DoBias() : DoCommand("Bias") {};

        std::string execute(MessageParty sender, 
                            std::vector<std::string> command);    
        /// Wait for the bias to complete, then send a notification.
        void wait(bool notify, MessageParty sender);
};
extern DoBias doBias;


/** Command to take a new Blank exposure or zero out the current one.
 * <pre>
 * Command:
 *     Do Blank [zero] [/Notify]
 *
 * The default exposure time is used for the exposure.  It is assumed that the 
 * telescope is either looking at blank sky (or slewing) or the filter is in 
 * the Blank position.
 * 
 * If the word “zero” is present, no exposure is taken, instead the OPTS will 
 * no longer subtract the (scaled) blank image (or subtract zero) from new 
 * images.  When the Blank image is zero, the FITS header should have 0 for 
 * the value of BLANKNUM.  /Notify is ignored for Blank zero.
 * 
 * <pre>
 * Response:
 *     Blank OK
 * or  Blank zeroed
 * or  Error Blank bad
 */
class DoBlank : public DoCommand {
    public:
        /// Construct and register the command object.
        DoBlank() : DoCommand("Blank") {};
        /// Execute the command.
        std::string execute(MessageParty sender, 
                            std::vector<std::string> command);    
        void wait(bool notify, MessageParty sender);
};
extern DoBlank doBlank;

/** Command to turn on or off the CCD camera power.
 *  The power is turned on or off via a relay that
 * connects the CCD power supply in the receiver
 * cabin to the camera. The command syntax is:
 * <pre>
 *     Do CamPower {On|Off}
 * </pre>
 * Response is of the form:
 * <pre>
 *     CamPower {On|Off}
 * </pre>
 */
class DoCamPower : public DoCommand {
    public:
        /// Construct and register the command object.
        DoCamPower() : DoCommand("CamPower") {};
        /// Execute the command.
        std::string execute(MessageParty sender, 
                            std::vector<std::string> command);    
};
extern DoCamPower doCamPower;


/** Exits from the OPTS command environment.
 * The syntax for this command is:
 * <pre>
 *     Do Exit
 * </pre>
 * The command sends no response. It is the responsibility of the
 * OPTS software environment to reset the machine, if appropriate.
 */
class DoExit : public DoCommand {
    public:
        DoExit() : DoCommand("Exit") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern DoExit doExit;


/** Command to make or abort an exposure or series of exposures.
 * To start a new exposure, the previous exposure(s)
 * must be complete or one must first <tt>Do Expose 
 * abort</tt>. The syntax is:
 * <pre>
 *     Do Expose [<em>exp_time</em> [<em>number</em>]] [/Notify]
 * or  Do Expose abort
 * </pre>
 * If <em>number</em> is present, the specified number of
 * exposures are made, one after the other. Response will
 * be of the form:
 * <pre>
 *     Expose <em>exp_time</em> <em>number</em>
 * or  Expose aborted
 * or  Error Expose bad parameter
 * or  Error Expose busy
 * </pre>
 */
class DoExpose : public DoCommand {
    public:
        /// Construct and register the command object.
        DoExpose() : DoCommand("Expose") {};
        /// Execute the command.
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
        void wait(bool notify, MessageParty sender);
};
extern DoExpose doExpose;

/** Command to turn the fan on or off.
 * <pre>
 * Command:
 *     Do Fan {On|Off}
 * </pre>
 */
class DoFan : public DoCommand {
public:
    DoFan() : DoCommand("Fan") {};
    std::string execute(MessageParty sender, std::vector<std::string> command);
};
extern DoFan doFan;

/** Command to Move to the specified filter position
 * <pre>
 * Command:
 *      Do Filter <name> [/Notify]
 * or   Do Filter
 * </pre>
 * 
 * where <name> is the filter name: Clear, IR, Blank, or Other.  (If <name> is 
 * omitted, move to the filter being used when the last Slew command was 
 * given, clear the Slew flag, and report the <name> of that filter in the 
 * Response below.).
 *
 * <pre>
 * Response:
 *      Filter <name>
 * or   Error Filter bad name
 * </pre>
 */
class DoFilter : public DoCommand {
    public:
        /// Construct and register the command object.
        DoFilter() : DoCommand("Filter") {};
        /// Execute the command.
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
        void go(std::string filter, bool notify, MessageParty sender);
};
extern DoFilter doFilter;


/** Command to initialize the filter wheel. (Needed if the filter wheel has 
 * stopped at an Unknown position.)
 *
 * <pre>
 * Command:
 *     Do FilterInit [/Notify]
 * </pre>
 * 
 * <pre>
 * Response:
 *     FilterInit started
 * </pre>
 */
class DoFilterInit : public DoCommand {
    public:
        /// Construct and register the command object.
        DoFilterInit() : DoCommand("FilterInit") {};
        /// Execute the command.
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
        void doInit(bool notify, MessageParty sender);
};
extern DoFilterInit doFilterInit;


/** Command to load a Flat Field image into the ALMA flat field area.
 *
 * <pre>
 * Command:
 *     Do FlatLoad
 * </pre>
 * (followed by a FITS image of the flat field in a separate Ethernet message)
 *
 * <pre>
 * Response:
 *      FlatLoad OK
 * or   Error FlatLoad no FITS
 * or   Error FlatLoad bad FITS
 * or   Error FlatLoad wrong size
 * </pre>
 * 
 * The "no FITS" error occurs if there was a timeout waiting for the message 
 * containing the FITS image.  "bad FITS" means that the message received 
 * was not a FITS file.  "wrong size" means that a valid FITS image was 
 * found but it specified an image of a different size than the CCD image.
 */
class DoFlatLoad : public DoCommand {
    public:
        /// Construct and register the command object.
        DoFlatLoad() : DoCommand("FlatLoad") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern DoFlatLoad doFlatLoad;


/** Command to move to the specified focus position.
 * 
 * <pre>
 * Command:
 *     Do Focus xx.xx [/Notify]
 * </pre>
 * where xx.xx is the position of the focus mechanism in mm.  This overrides 
 * any previous Focus or FocusFind command in progress.
 *
 * <pre>
 * Response:
 *       Focus xx.xx
 * or    Error Focus bad position
 * </pre>
 */
class DoFocus : public DoCommand {
    public:
        /// Construct and register the command object.
        DoFocus() : DoCommand("Focus") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);

        /** Sends the command to the focuser. After the focus motion is
         * complete, the sender will receive a notification if notify is true.
         * @param notify whether to send a completion notification to the
         *               sender.
         * @param pos the position to which the focuser should move.
         * @param sender who is to receive the completion notification.
         */
        void go(bool notify, double pos, MessageParty sender);
};
extern DoFocus doFocus;

/** Command to initialize the (incremental) focus encoder.  This is done by 
 * first moving in a standard direction until the corresponding limit 
 * switch is tripped.  The focus is then moved in the opposite direction 
 * until a Z pulse is detected with the limit switch untripped.  (There are 
 * Z pulses every 1/16 of an inch.)  At this instant, the apparent focus 
 * position is recorded then set to zero.  The date and time of the Z pulse 
 * is also recorded.  The recorded data represents the error in the previous 
 * focus positions that was corrected by FocusInit procedure.  (The 
 * response to the Query FocusInit command gives this error and the time 
 * stamp.)
 * 
 * <pre>
 * Command:
 *     Do FocusInit [/Notify]
 * </pre>
 * 
 * <pre>
 * Response:
 *     FocusInit OK
 * </pre>
 */
class DoFocusInit : public DoCommand {
    public:
        /// Construct and register the command object.
        DoFocusInit() : DoCommand("FocusInit") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
        void doInit(bool notify, MessageParty sender);
};
extern DoFocusInit doFocusInit;


/** Command to open or close the telescope shutter.
 * <pre>
 * Command:
 *      Do Shutter <state> [/Notify]
 * </pre>
 * where <state> is Open or Close
 * 
 * <pre>
 * Response:
 *       Shutter OK
 * or    Error Shutter bad
 * </pre>
 */
class DoShutter : public DoCommand {
    public:
        /// Construct and register the command object.
        DoShutter() : DoCommand("Shutter") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
        /// Open the shutter
        void open(bool notify, MessageParty sender);
        /// Close the shutter
        void close(bool notify, MessageParty sender);
};
extern DoShutter doShutter;



/** Command to stop sending to the mailbox that sent this command, the 
 * multiple responses that Get commands can generate when they specify a 
 * sequence number range. 
 * 
 * <pre>
 * Command:
 *     Do ShutUp
 * </pre>
 * 
 * <pre>
 * Response:
 *     ShutUp OK
 * </pre>
 */
class DoShutUp : public DoCommand {
    public:
        /// Construct and register the command object.
        DoShutUp() : DoCommand("ShutUp") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern DoShutUp doShutUp;


/** Command to move the filter to the Blocked position and set the slew flag.  
 * (This should be done in prior to slewing in daylight in order to protect the 
 * camera from direct sunlight.  While the OPTS will do this automatically if 
 * the sun trips the SunSensor, this protects the camera in case this evasive 
 * action is tardy or the SunSensor fails.)  The previous filter position 
 * is saved. The filter will be automatically be returned to this position 
 * and the slew flag cleared upon the next Do Filter command with no parameter 
 * or the next Do Expose command.
 * 
 * <pre>
 * Command:
 *     Do Slew
 * </pre>
 *
 * <pre>
 * Response:
 *     Slew OK
 * </pre>
 */
class DoSlew : public DoCommand {
    public:
        /// Construct and register the command object.
        DoSlew() : DoCommand("Slew") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern DoSlew doSlew;


/** Command to abort any Do commands in progress
 * <pre>
 * Command:
 *     Do Stop
 * </pre>
 *
 * <pre>
 * Response:
 *      Stop aborted <name(s)>
 * or   Stop nothing to do
 * </pre>
 * where <name(s)> are the Do command(s) aborted.
 */
class DoStop : public DoCommand {
    public:
        /// Construct and register the command object.
        DoStop() : DoCommand("Stop") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern DoStop doStop;


/** Command to turn On, or Off the telescope Shutter heater
 * 
 * <pre>
 * Command:
 *     Do TelHeater <state>
 * </pre> 
 * where <state> is On, or Off.
 * 
 * <pre>
 * Response:
 *      TelHeater <state>
 * </pre>
 */
class DoTelHeater : public DoCommand {
    public:
        /// Construct and register the command object.
        DoTelHeater() : DoCommand("TelHeater") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern DoTelHeater doTelHeater;

class DoVideo : public DoCommand {
    public:
        /// Construct and register the command object.
        DoVideo() : DoCommand("Video") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern DoVideo doVideo;

#endif // ! DOCOMMAND_H
