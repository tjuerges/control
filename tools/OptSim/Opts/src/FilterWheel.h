#ifndef FILTERWHEEL_H
#define FILTERWHEEL_H

#include <boost/thread/mutex.hpp>
#include <string>
#include <vector>
#include <map>

class FilterPosition;

/** Represents a filter wheel or similar device.
 */
class FilterWheel {
    public:
        /** Describes the possible positions of the filter wheel. */
        enum State {
            FW_FILTERED,  /**< A filter is in position. */
            FW_BETWEEN,   /**< The filter wheel is stopped between filters. */
            FW_MOVING,    /**< The filter wheel is moving. */
            FW_STUCK,     /**< The filter wheel is stuck and must be reset. */
            FW_UNKNOWN    /**< The filter wheel status is unknown. */ 
        };

        /** Constructor. */
        FilterWheel();

        /** Destructor. */
        virtual ~FilterWheel();

        /** Adds a filter to the filter wheel. Filters are ordered by
         * the order in which they were added to the filter wheel using
         * addFilter(), and not by any ordering that might derive from the
         * information in FilterPosition. If you wish to use the functions
         * that refer to filters by a number, you must take care to make
         * the calls the addFilter() in the correct order.
         * @param name a unique name for the new filter.
         * @param position information about how to move to this filter.
         */
        void addFilter(const std::string name, 
                       FilterPosition * position);

        /** Specifies which position the filter wheel should consider to
         * be the blocked position. This call must be made after the
         * blocked position has been added using addFilter().
         * @param filterName the name assigned to the blocked position.
         */
        void setBlockedPosition(const std::string filterName);

        /** Specifies which position the filter wheel should consider to
         * be the blocked position. This call must be made after the
         * blocked position has been added using addFilter().
         * @param filterNum the filter number assigned to the blocked 
         *                  position.
         */
        void setBlockedPosition(int filterNum);

        /** Specifies which position the filter wheel should consider to
         * be the clear position. This call must be made after the
         * clear position has been added using addFilter().
         * @param filterName the name assigned to the clear position.
         */
        void setClearPosition(const std::string filterName);

        /** Specifies which position the filter wheel should consider to
         * be the clear position. This call must be made after the
         * clear position has been added using addFilter().
         * @param filterNum the filter number assigned to the clear 
         *                  position.
         */
        void setClearPosition(int filterNum);

        /** Places the filter wheel in an initialized state. Usually this
         * will mean finding the motor's home position. The init() method
         * must be called before using the filter wheel. After calling
         * this method, a user should call one of the go() functions to
         * move the filter wheel into one of the filter positions.
         */
        virtual void init();

        /** Returns the list of filters currently loaded in the filter wheel.
         * @returns the list of filters currently loaded in the filter wheel.
         */
        const std::vector<const std::string> getFilters();

        /** Returns the filter position of the given filter.
         * @param name the name of the filter to get.
         * @returns information about the position of the filter.
         */
        const FilterPosition * getPosition(const std::string name); 

        /** Returns the filter position of the given filter.
         * @param the filter's filter number.
         * @returns the information about the position of the filter.
         */
        const FilterPosition * getPosition(int num);

        /** Sends the filter wheel to the specified filter.
         * @param filterName the name of the filter to go to.
         */
        virtual void go(std::string filterName);

        /** Sends the filter wheel to the specified filter.
         * In this form, an integer is passed to go(). This is the number of
         * the filter to go to. The numbers are assigned according to the
         * order of calls to addFilter().
         * @param filterNumber the filter number to go to.
         */
        virtual void go(int filterNumber);

        /** Stops any movement currently in progress.
         */
        virtual void abort() {}

        /** Returns the filter number of the current filter.
         * @returns the filter number of the current filter.
         */
        virtual int getFilter() const;
        virtual int getPosition() const { return getFilter(); }

        /** Returns the filter number corresponding to the specified name.
         * @param name the name of the filter to find.
         * @returns the number of the specified filter.
         */
        int getFilterNum(std::string name) const;

        /** @returns the current status of the filter wheel.
         */
        virtual State getState() const { return state; }

        /** Returns the name of the filter in the specified position.
         * @param num the filter number to retrieve.
         * @returns the number of the given filter.
         */
        std::string getFilterName(int num) const;

        /** Returns the name of the filter that is currently in position.
         * @returns the number of the filter.
         */
        std::string getFilterName() const { return getFilterName(getFilter()); }

        /** Sets the index of the filter to which the filter wheel should move.
         * @param targ the index number of the target filter.
         */
        void setTarget(int targ) { target = targ; }     

        int getTarget() { return target; }

        int getNumFilters() { return filters.size(); }

        /** Returns true if an error has occurred in the filter wheel driver.
         */
        virtual bool getError() { return error; }

    protected:
        /** Sets the current filter. The filter number is determined by
         * the ordering of calls to addFilter(). Callers to this function
         * should first obtain a lock on the mutex.
         * @param pos the new filter wheel position.
         */
        void setPosition(int pos) { current = pos; }

        /** Sets the current state of the filter wheel. Callers to this
         * function should first obtain a lock on the mutex.
         * @param s the new filter wheel state
         */
        void setState(State s) { state = s; }

        /** A mutex to prevent multiple threads from accessing the
         * filter wheel at the same time.
         */
        boost::mutex mutex;

        /** Indicates that an error has occurred in the filter wheel driver.
         */
        void setError() { error = true; }

        /** Removes the error status flag from an error that occurred
         * previously.
         */
        void clearError() { error = false; }

    private:
        /** The current status of the filter wheel. */
        State state;

        /** The number of the filter currently in position. The name of
         * the currently loaded filter is given by filters[current].
         * If no filter is currently in position, this is set to -1.
         */
        int current;

        /** The filter number corresponding to a position that blocks
         * light from reaching the instrument. If no blocked position
         * has been set, this is set to -1.
         */
        int blockedPosition; 

        /** The filter number corresponding to a position that allows all
         * incoming light to pass to the instrument. If no blocked position
         * has been set, this is set to -1.
         */
        int clearPosition;

        /** The target the filter wheel should go to, if it is moving. */
        int target;

        /** A list of the filter positions. The order of this list
         * defines the order of the filter numbering.
         */
        std::vector<std::string> filters;

        /** A map providing information about how to move each
         * filter into position.
         * @see FilterPosition
         */ 
        std::map<const std::string, const FilterPosition *> filterPositions;

        /** True if an error has occurred in the filter wheel driver.
         */
        bool error;
};

/** Represents a valid position for a FilterWheel. A FilterPosition object
 * contains the information required for a filter wheel to move to that
 * position.
 * @see FilterWheel
 */
class FilterPosition {
public:
    /** The name of the filter. */
    std::string name;

    /** The filter's index number in the filter wheel list. */
    int index;
};

#endif // !FILTERWHEEL_H
