#include "StepperFilterWheel.h"
#include "Stepper.h"
#include "Opts.h"


FilterWheel::State StepperFilterWheel::getState() const
{
    if (moving) return FilterWheel::FW_MOVING;
    if (stuck) return FilterWheel::FW_STUCK;

    int currentPosition = getPosition();
    if (currentPosition == -1) return FilterWheel::FW_BETWEEN;
    return FW_FILTERED;
}

void StepperFilterWheel::go(std::string filterName)
{
    go(FilterWheel::getPosition(filterName) -> index);
}

void StepperFilterWheel::go(int filterNumber)
{
    int currentPosition = getPosition();
    int numFilters = getNumFilters();

    if (currentPosition == -1) {
        logFile << "Can't change filter wheel until initialized." << std::endl;
        return;
    }

    setState(FilterWheel::FW_MOVING);
    setTarget(filterNumber);

    // Find how many jumps are needed to reach the requested position.
    int nSteps = filterNumber - currentPosition;

    // Decide which direction to go
    if (nSteps == 0) return;
    if (nSteps > numFilters / 2) nSteps -= numFilters;
    if (nSteps < -numFilters / 2) nSteps += numFilters;

    int direction = 1;
    if (nSteps < 0) {
        direction = -1;
    }

    // Set the filter wheel in motion, stop on reaching the
    // desired position.
    for (int i = 0; i < numFilters * 2; i++) {
        step(direction);
        int pos = getPosition();
        setPosition(pos);
        
        if (pos == -1) {
            setState(FW_STUCK);
            return;
        }
 
        if (pos == filterNumber) {
            setState(FW_FILTERED);
            return;
        }
    }

    setState(FW_STUCK);
}

int StepperFilterWheel::getPosition() const
{
    int index = 0;

    if (posMarker -> check() != 0) {
        return -1;
    }

    if (di1 -> check() == 0) index += 1;
    if (di2 -> check() == 0) index += 2;

    return index;
}

void StepperFilterWheel::step(int direction)
{
    if ((direction != 1) && (direction != -1)) return;

    try {
        StepperControl sc(stepper);
        sc.on();
        sc.jog(direction * filterSeparation);
        sc.off();
    } catch (std::string ex) {
        logFile << "EXCEPTION: " << ex << std::endl;
    }
}

void StepperFilterWheel::init()
{
    int steps = 150;
    int retries = 4;
    try {
        StepperControl sc(stepper);
        sc.on();

        int speed = stepper -> getSpeed();
        stepper -> setSpeed(speed / 2);

        while ((posMarker -> check() == 0) && (--steps)) {
            sc.jog(filterSeparation / 100);
        }

        do {
            sc.jog(filterSeparation / 100);
 
            if (posMarker -> check() == 0) {
                sc.jog(detentSize);
                stepper -> setSpeed(speed);
                sc.off();
                return;
            }
        } while (--steps);

        logFile << "Filter init failed." << std::endl;

        stepper -> setSpeed(speed);

        sc.off();
    } catch (std::string ex) {
        logFile << "EXCEPTION: " << ex << std::endl;
        logFile << "Filter init failed due to exception." << std::endl;
    }
}

bool StepperFilterWheel::getError()
{
    if (FilterWheel::getError()) return true;

    if (stepper -> getState() == Stepper::STUCK) return true;

    return false;
}
