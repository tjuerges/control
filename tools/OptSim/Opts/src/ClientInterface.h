#ifndef CLIENTINTERFACE_H
#define CLIENTINTERFACE_H

#include <string>

#if HAVE_CONFIG_H
#   include <config.h>
#endif // HAVE_CONFIG_H

#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>

#include <boost/filesystem/operations.hpp>

#include "Image.h"

/** A participant in the client-server communication system.
 *
 * Describes an entity that has communicated with the server
 * by sending a message. It is possible to distinguish each
 * party from the others, and to send messages to specific
 * parties.
 *
 * It should be noted that identification in this system
 * is based entirely on the honor system. Matters of security
 * must be handled separately and independently of this
 * code.
 */
class MessageParty {
    public:
        /** Constructor. */
        MessageParty() : name("OPT_GUI"), handle(0) {}

        /** Sets the name identifier string for this message party.
         * For Socklib, this is the mailbox handle. 
         */
        void setName(std::string newName);

        /** Gets a string to identify this party. 
         *
         * @returns a string representing the name of the party.
         */
        std::string getName() const;

        /** Sets the handle data for the message party. The handle provides
         * the client interface with the information necessary to contact
         * the message party. For Socklib, the handle is a pointer to SOCK
         * structure.
         *
         * @param newHandle the handle to associate with the message party.
         */
        void setHandle(void * newHandle);

        /** Gets the handle for the party.
         * 
         * A handle is a data structure that can
         * be used to find the party and send a reply.
         *
         * @returns a handle describing the party.
         */
        void * getHandle();

    private:
        /** A string for identifying the message party. A name should be
         * unique to each party.
         */
        std::string name;

        /** The message party handle. This points to information that can be
         * used to contact the message party.
         */
        void * handle;
};

/** Used for passing a message and its sender together. */
typedef struct _message {
        std::string command;      //!< The message being received.
        MessageParty * sender;    //!< The sender of the message.
} Message;

/** A console-style interface to the user.
 *
 * This class handles communication between client and
 * server. The intended operating practice for the OPT
 * will be to use the NRAO socket library for this
 * interface. A simple text interface is supported through
 * this class, and the SockLibInterface provides the
 * socket library interface.
 */
class ClientInterface {
    public:
        /** Destructor. */
        virtual ~ClientInterface() {};

        /** Sends a message to the user.
         * 
         * @param recipient The intended recipient of the message.
         * @param message The text of the message to deliver.
         */
        virtual void send(MessageParty recipient, std::string message);

        /** Receives a new message.
         *
         * Listens for a new message coming from any
         * elligible party. If no message is waiting to
         * be received, this method blocks.
         *
         * @returns the text of the message and the identity of its sender.
         */
        virtual Message receive();

        /** Receives a FITS file from the client.
         *
         * TODO: This function should be modified so that it only looks for
         * a FITS file from a specific message party, and ignores FITS files
         * sent by any other message party.
         */
        virtual char * receiveFits();

        /** Send a FITS file.
         * @param recipient the message party to receive the FITS file.
         * @param file the path for the file that will be sent. 
         */
        virtual void sendImage(MessageParty recipient, Image * img);

        /** Receive a file from the client.
         * @param recipient the recipient to receive a file from. All other 
         *                  messages transmitted will be ignored. 
         * @returns a pointer to the contents of the file.
         */
        virtual char * readFile(MessageParty recipient);
        
        /** Sets the name of the master client.
         *
         * The master client has special privileges to
         * execute any command for the OPT. The client
         * identified by this string will become the
         * master client. Naturally, it should be called
         * only if there is no master already or if the
         * current master directs.
         *
         * @param newMaster a string identifying the new master client.
         */
        virtual void setMaster(std::string newMaster);

        /** Gets the name of the master client.
         *
         * @returns a string representing the master client, or an empty 
         *          string if there is no master.
         */
        virtual std::string getMaster();
        
    private:
        /** A name identifying the master client. The master
         * is allowed to use all commands, whereas most (all?)
         * Do and Set commands are blocked to ordinary clients.
         * If this string is left empty, all clients are allowed
         * to use the complete command set.
         */
        std::string master;
};


#if HAVE_LIBSOCKPP
class SockLibInterface : public ClientInterface {
    public:
        /** Constructor. */
        SockLibInterface();
        void sendImage(MessageParty recipient, Image * img);
        void send(MessageParty recipient, std::string message);
        Message receive();
        
    private:
        std::string mailboxName;
        std::vector<struct SOCK *> handles;

        boost::mutex mutex;
};

#endif // HAVE_LIBSOCKPP

#endif // ! CLIENTINTERFACE_H

