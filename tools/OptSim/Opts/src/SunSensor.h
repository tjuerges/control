#ifndef SUNSENSOR_H
#define SUNSENSOR_H

class Sensor;

/** Interface to a sensor that provides a warning when the sun comes too close
 * to the telescope's focal plane. By monitoring the sun sensor, the system
 * can detect the presence of the sun and take appropriate steps.
 */ 
class SunSensor {
    public:
        /** Constructor. Sets the threshold to the default and marks
         * the sun sensor as "not tripped".
         */
        SunSensor(int num);

        /** Monitors the sun sensor. This function polls the sun sensor
         * periodically. If the sensor measurement exceeds the threshold,
         * the shutter will close, and the filter wheel will move to the
         * blocked position. The camera is also notified that the sun sensor
         * is tripped. 
         */
        void monitor();

        /** Handles a signal from the sun sensor. This function closes the
         * shutter, blocks the filter wheel, and sends a notification to
         * the camera.
         */
        void trip();

        /** Reverses the effects of trip(). This function should be called
         * once the sun sensor readings drop back below the threshold.
         */
        void untrip();

        /** Queries the status of the sun sensor.
         * @returns true if the sun sensor is tripped, false otherwise.
         */
        bool getState() { return tripped; }

        /** Queries the current level of the sun sensor. */
        double getLevel();

        /** Sets the threshold above which the sun sensor is tripped.
         * @param thresh the new threshold reading for the sun sensor.
         */
        void setThreshold(double thresh) { threshold = thresh; }

        /** Gets the current threshold setting for the sun sensor.
         * @returns the voltage reading from the sun sensor that will cause
         *          safety measures to be taken.
         */
        double getThreshold() { return threshold; }

    private:
        /** The sensor device used to control the sun sensor. */
        Sensor * sensor;

        /** The threshold above which the sun sensor should be tripped */
        double threshold;

        /** Whether the sun sensor is in a tripped state. */
        bool tripped;
};

#endif // !SUNSENSOR_H
