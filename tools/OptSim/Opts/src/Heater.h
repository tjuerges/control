#ifndef HEATER_H
#define HEATER_H

class Relay;
class Shutter;

class Heater 
{
public:
    enum State {
        ON,   /**< The heater is on. */
        OFF   /**< The heater is off. */
    };

    Heater(Relay * r);

    State on();
    State off();
    State getState();

private:
    State state;
    Shutter * shutter;
    Relay * relay;
};

#endif
