#ifndef THERMALSENSOR_H
#define THERMALSENSOR_H

#include "Sensor.h"

/** Provides a translation from a generic Sensor object to a temperature
 * sensor. The ThermalSensor uses a mapping from a sensor reading to a
 * meaningful temperature. The base class uses the mapping for the
 * LM35 sensor, in which 10 mV corresponds to 1 degree C.
 */
class ThermalSensor {
    public:
        /** Constructor. */
        ThermalSensor(Sensor * s) : sensor(s) {}

        /** Destructor. */
        ~ThermalSensor() {}

        /** Gets a reading from the temperature sensor. 
         * 
         * @returns the temperature measured by the sensor, in Celsius.
         */
        virtual double getTemperature() { return sensor -> read() * 100./20.5; }

    private:
        /** The sensor object that provides a temperature reading. */
        Sensor * sensor;
};

#endif // !THERMALSENSOR_H
