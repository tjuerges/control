
#include "Focuser.h"
#include "Stepper.h"
//#include <boost/date_time/time_facet.hpp>
#include "Opts.h"

Focuser::Focuser(Stepper * s, double stepSize)
{
    stepper = s;
    this -> stepSize = stepSize;

    error = false;
    zeroError = zeroPosition = 0;
}

void Focuser::init()
{
    try { 
        StepperControl ctrl(stepper);

        if (getPosition() > 1.) goTo(1.); 

        ctrl.goHome(); 

        // Set the zero position, and record how much it will change.
        zeroError = zeroPosition;
        zeroPosition = ctrl.getPosition();
        zeroError -= zeroPosition;
        ctrl.setPosition(0);

        initTime = boost::posix_time::microsec_clock::local_time();
        clearError();
    } catch (std::string ex) {
        logFile << "Exception: " << ex << std::endl;
        setError();
    }
}

std::string Focuser::getInitTime()
{
    using namespace boost::posix_time;

    return to_iso_extended_string(initTime);

#if 0
    boost::posix_time::time_facet * f = new boost::posix_time::time_facet();
    f -> format("%Y-%m-%dT%H:%M:%s");

    std::stringstream ss;
    ss.imbue(std::locale(ss.getloc(), f));
    ss << initTime;

    return ss.str();
#endif
}

double Focuser::getZeroError()
{
    return zeroError * stepSize;
}

void Focuser::goTo(double pos)
{
    // Convert mm to steps.
    pos /= stepSize;
    logFile << "Sending focuser to " << pos << std::endl;
    try {
        StepperControl ctrl(stepper);
        ctrl.goTo((int)pos);
    } catch (std::string exception) {
        setError();
    }
}

double Focuser::getPosition()
{
    int pos = stepper -> getPosition();
    return pos * stepSize;
}

Focuser::State Focuser::getState()
{
    Stepper::State state = stepper -> getState();

    switch (state) {
        case Stepper::STOPPED:
            return Focuser::STOPPED;

        case Stepper::LIMIT_UP:
            return Focuser::LIMIT_UP;

        case Stepper::LIMIT_DN:
            return Focuser::LIMIT_DN;

        case Stepper::MOVING_UP:
            return Focuser::MOVING_UP;
  
        case Stepper::MOVING_DN:
            return Focuser::MOVING_DN;

        case Stepper::STUCK:
            return Focuser::STUCK;

        case Stepper::UNKNOWN:
        default:
            return Focuser::UNKNOWN;
    }
}

bool Focuser::getError()
{
    if (error) return true;

    if (stepper -> getState() == Stepper::STUCK) return true;

    return false;
}
