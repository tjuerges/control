#ifndef IMAGE_H
#define IMAGE_H

#if HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H

#if HAVE_CFITSIO_FITSIO_H
#include <cfitsio/fitsio.h>
#else
#include <fitsio.h>
#endif

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/condition.hpp>

#include <string>
#include "RegionOfInterest.h"
#include <iostream>

class Centroid;

class CalibrationImage;
class HotPixels;
typedef struct _region RegionOfInterest;

class MessageParty;

class Image {
    public:
        enum State {
            EXPOSING,
            PROCESSING,
            WRITING,
            ABORTED,
            READY
        };

        enum ImageType { IMG_STAR, IMG_BLANK, IMG_FLAT, IMG_BIAS };
        Image();
        Image(Image&);
        Image(const char * fits);
        ~Image();

        char * getFITS() { return fitsBuffer; }
        int getFitsSize() { return fitsSize; }

        State getState() { return state; }
        void setState(State s) { state = s; } 

        void buildHeader(fitsfile * fptr);

        void setSequenceNumber(const int num);
        int getSequenceNumber();

        void setDateObs(boost::posix_time::ptime t);

        void setROI(const RegionOfInterest newRoi);
        RegionOfInterest getROI();

        ImageType getType() { return type; }
        void setType(ImageType t) { type = t; }

        void setBits(signed short * bits);
        signed short * getBits();
        signed short getPixel(int x, int y);
        signed short * getPixels() const;
        int countPixels() const;

        const Centroid * getCentroid() { return centroid; } 

        static void dispatch(Image * img);
        static void process();

        void setHotPix(HotPixels * h) { hotpix = h; }
        void setBias(CalibrationImage * b) { bias = b; }
        void setBlank(CalibrationImage * b) { blank = b; }
        void setFlat(CalibrationImage * f) { flat = f; }

        void applyHotPix();
        void applyBias();
        void applyBlank();
        void applyFlat();

        void createFits();

        void setExpTime(double time) { expTime = time; }
        double getExpTime() { return expTime; }
        void setFilter(std::string filt) { filter = filt; }

        void setShutter(bool val) { shutter = val; }
        bool getShutter() { return shutter; }
        void setHeater(bool val) { heater = val; }
        bool getHeater() { return heater; }
        void setSunSensor(bool val) { sunSensor = val; }
        bool getSunSensor() { return sunSensor; }

        void setCcdTemp(double temp) { ccdTemp = temp; }
        double getCcdTemp() { return ccdTemp; }
        void setTubeTemp(double temp) { tubeTemp = temp; }
        double getTubeTemp() { return tubeTemp; }
        void setFilterTemp(double temp) { filterTemp = temp; }
        double getFilterTemp() { return filterTemp; }

        std::string getDateObs() { return dateObs; }


        void setSendTo(MessageParty * to);

    private:
        static boost::mutex processMutex;
        static boost::condition processCond;
        static boost::condition completeCond;
        static volatile bool idle;
        static Image * processImage;

        char * fitsBuffer;
        size_t fitsSize;

        ImageType type;
        State state;

        RegionOfInterest roi;
        signed short * pixels;

        double expTime;
        std::string filter;

        int sequenceNumber;
        std::string dateObs;

        HotPixels * hotpix;
        CalibrationImage * bias;
        CalibrationImage * blank;
        CalibrationImage * flat;

        bool shutter;
        bool heater;
        bool sunSensor;

        double tubeTemp;
        double filterTemp;
        double ccdTemp;

        MessageParty * sendTo;

        Centroid * centroid;
};

#endif // ! IMAGE_H

