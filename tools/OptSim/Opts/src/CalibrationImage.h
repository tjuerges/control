#ifndef CALIBRATIONIMAGE_H
#define CALIBRATIONIMAGE_H


#if HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H

#if HAVE_CFITSIO_FITSIO_H
#include <cfitsio/fitsio.h>
#else
#include <fitsio.h>
#endif

class Image;

static const int calibration_image_x = 1024;
static const int calibration_image_y = 1024;

/** CalibrationImage
 * This class represents an image that is used for calibrating images
 * that come off the OPTS. For instance, it might represent a flat field
 * image or a bias. This class has functionality by which it can combine 
 * multiple calibration images, but that functionality is not currently
 * used by the OPTS ICD. It would be natural to make this an extension of
 * the Image class. That is not done currently, but it might be a good idea
 * to change it to do that.
 */
class CalibrationImage {
    public:
        /** Identifies the source of a flat field image. */
        enum FlatSource {
            SRC_ALMA,        /**< The flat is provided by ALMA */
            SRC_FACTORY,     /**< The flat is provided by ACE */
            SRC_NONE         /**< There is no flat field being used. */
        };


        /** Constructor. This constructor creates the calibration image from
         * an Image object. 
         *
         * @param img convert this Image to a CalibrationImage.
         */
        CalibrationImage(Image& img);

        /** Constructor. This constructor creates a new image with all
         * pixels set to a constant value.
         *
         * @param val the initial value of the image pixels.
         */ 
        CalibrationImage(double val);

        /** Sets the image sequence number associated with this calibration
         * image.
         *
         * @param num the sequence number to be assigned to the next image.
         *            This number must be between 1 and Camera::MAX_IMAGES.
         */
        void setSequenceNumber(int num);

        /** Gets the image sequence number that was used to make this
         * calibration image.
         */
        int getSequenceNumber();

        RegionOfInterest getROI() { return roi; }

        /** Returns the original image. */
        Image * getImage() { return img; }

        void setImage(Image& i) { img = new Image(i); }

        /** Returns the number of images that were combined to create this
         * calibration image.
         */
        int getNumComponents();

        /** Multiplies all the pixel values in the image by a constant such
         * that the mean pixel value becomes 1.0.
         *
         * Flat field calibration images should be normalized before use so
         * that the values in flat-fielded images are comparable to the values
         * from the raw image.
         */
        void normalize();

        /** Combines two calibration images. The images are combined in a
         * weighted average.
         *
         * @param src the calibration image to copy into this one.
         */
        void combine(CalibrationImage& src);

        /** The pixel values for the image.
         */
        double pix[calibration_image_x][calibration_image_y];

        /** @returns the date and time of the observation, formatted
         * for use in a FITS header.
         */
        std::string getDateObs() { return dateObs; }

        /** Sets the source of the flat field image that this object
         * represents. This function should be called only on flat field
         * images.
         */
        void setFlatSource(FlatSource ft) { flatSource = ft; }

        /** @returns the source of this flat field image. The result
         * is only meaningful if the object represents a flat field
         * calibration image.
         */
        FlatSource getFlatSource() { return flatSource; }

    private:
        /** The image from which the calibration image was created. */
        Image * img;

        /** The number of raw images combined to create this calibration
         * image.
         */
        int numImages;        

        /** The image sequence number used to create this calibration image.
         */
        int sequenceNum;

        RegionOfInterest roi;

        /** The date at which this calibration image was exposed.
         */
        std::string dateObs;

        /** The source of the flat field calibration image. This can be
         * either SRC_FACTORY, to indicate that the source is a factory-
         * loaded flat field image, or SRC_ALMA, to indicate that the
         * flat field image is not provided with the OPT.
         */
        FlatSource flatSource;
};

#endif // !CALIBRATIONIMAGE_H
