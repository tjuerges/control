#ifndef HOTPIXELS_H
#define HOTPIXELS_H

#include <vector>
#include <map>

class HotPixels {
    public:
        HotPixels() : pixList() {}

        void add(int x, int y);
        void del(int x, int y);
        void deleteAll();

        bool includes(int x, int y);

        std::vector<std::pair<int,int> > getPixels();

    private:
        std::map<int,int> pixList;
};

#endif // HOTPIXELS_H
