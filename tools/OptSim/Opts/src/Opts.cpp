#include <string>
#include <vector>
#include <iostream>
// For std::logic_error
#include <stdexcept>
// For std::getenv
#include <cstdlib>

#include "Opts.h"
#include "ClientInterface.h"
#include "Camera.h"
#include "PhotonMaxCamera.h"
#include "FilterWheel.h"
#include "StepperFilterWheel.h"
#include "Focuser.h"
#include "Stepper.h"
#include "Relay.h"
#include "ThermalSensor.h"
#include "SunSensor.h"
#include "Shutter.h"
#include "Heater.h"

// For managing the factory flat image.
#include "Image.h"
#include "CalibrationImage.h"

#include <libconfig.h++>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef ENABLE_HARDWARE
#include <Galil.h>
#include "GalilStepper.h"
#include "GalilSensor.h"
#include "GalilRelay.h"
#include "GalilDigitalInput.h"
#endif // ENABLE_HARDWARE


Opts theOPTS;

Opts::Opts()
{
    identifier = "OPT";
    isProtected = false;

    configuration = new libconfig::Config;

    try
    {
        // Fill a vector with the possible configuration file paths.
        std::vector< std::string >paths;

        // Current directory.
        paths.push_back(".");

        // Allright, buckle up!  Here comes an odd one:
        // Assuming that the environment variable INTROOT is not defined, then
        // std::getenv returns a char* 0.  Assigning this to a std::string
        // throws a std::logical exception.  :-O  Constructing a std::string
        // from it throws a std::logical exception, too.  So just assign the
        // damn thing to a char*.
        char* fromEnvironment(0);
        std::string confPath;

        fromEnvironment = std::getenv("INTROOT");
        if(fromEnvironment != 0)
        {
            confPath = fromEnvironment;
            confPath += "/config";
            paths.push_back(confPath);
        }

        fromEnvironment = std::getenv("INTLIST");
        if(fromEnvironment != 0)
        {
            // Do the split up at ":" of the INTLIST and add each item
            // plus */config.
        }

        fromEnvironment = std::getenv("ACSROOT");
        if(fromEnvironment != 0)
        {
            confPath = fromEnvironment;
            confPath += "/config";
            paths.push_back(confPath);
        }

        // Build time configuration directory.  Normally unusable.
        paths.push_back(SYSCONFDIR);

        // System configuration directory.
        paths.push_back("/etc");

        std::vector< std::string >::const_iterator iter(paths.begin());
        for(; iter != paths.end(); ++iter)
        {
            confPath = *iter;
            std::cout << "Trying to read the configuration file from \""
                << confPath
                << "\".\n";
            try
            {
                confPath += "/opts.conf";
                configuration->readFile(confPath.c_str());
                break;
            }
            catch(const libconfig::FileIOException& ex)
            {
                std::cout << "Configuration file \""
                    << confPath
                    << "\" is not available.\n";
            }

            if(iter == paths.end())
            {
                std::cout << "A configuration is not available.  This is a "
                        "fatal error.\n";
                exit(EXIT_FAILURE);
            }
        }
    }
    catch(libconfig::ParseException& ex)
    {
        std::cout << "Could not parse configuration file." << std::endl;
        std::cout << "  " << ex.getLine() << ":" << ex.getError() << std::endl;
        exit(EXIT_FAILURE);
    }

    identifier = (const char *)configuration->lookup("opts.name");


    power = new Relay;
#ifdef ENABLE_HARDWARE
    try {
        galil = new Galil("/dev/ttyS0 115200");
    } catch (std::string ex) {
        std::cout << "Exception: " << ex << std::endl;
    }

    camera = new PhotonMaxCamera;

    // Set up the filter wheel.
    GalilStepper * filterStepper = 0;
    try {
        filterStepper = new GalilStepper(galil, 'A');
        galil->command("MTA=2");
        filterStepper->setSpeed(3200);
        filterStepper->setAccel(32000);
        filterStepper->setDecel(32000);
    } catch (std::string ex) {
        std::cout << "Exception: " << ex << std::endl;
        exit(EXIT_FAILURE);
    }

    StepperFilterWheel * sfw;
    galil->command("LDA=3");
    sfw = new StepperFilterWheel(filterStepper);

    sfw->setPositionMarker(new GalilDigitalInput(galil, 1));
    sfw->setInput1(new GalilDigitalInput(galil, 2));
    sfw->setInput2(new GalilDigitalInput(galil, 3));
    sfw->setFilterSeparation(3200);
    sfw->setDetentSize(160);
    wheel = sfw;

    GalilStepper * focuser_stepper = new GalilStepper(galil, 'B');
    try {
        galil->command("MTB=2");
    } catch (std::string ex) {
        std::cout << "Exception: " << ex << std::endl;
        exit(EXIT_FAILURE);
    }
    focuser_stepper->setEncoderGear(3.125);
//    galil->command("YSB=1");
    focuser_stepper->setSpeed(2560);
    focuser_stepper->setAccel(64000);
    focuser_stepper->setDecel(64000);

    focuser = new Focuser(focuser_stepper, 0.000390625);

    shutter = new Shutter;
    sunSensor1 = new SunSensor(3);
    sunSensor2 = new SunSensor(4);

    galil->command("AQ1,2");
    galil->command("AQ2,2");
    tubeSensor = new ThermalSensor(new GalilSensor(galil, 1));
    filterSensor = new ThermalSensor(new GalilSensor(galil, 2));

    cameraPower = new GalilRelay(galil, 4);
    heater = new Heater(new GalilRelay(galil, 2));
    fan = new GalilRelay(galil, 3);
    fan->on();
    pcbSunSensor = new GalilDigitalInput(galil, 6);

#else
    camera = new Camera;
    wheel = new FilterWheel;
    focuser = new Focuser(new Stepper, 0.00125);
    shutter = new Shutter;

    sunSensor1 = new SunSensor(0);
    sunSensor2 = new SunSensor(0);
    pcbSunSensor = new DigitalInput;

    tubeSensor = new ThermalSensor(new Sensor);
    filterSensor = new ThermalSensor(new Sensor);

    fan = new Relay;
    cameraPower = new Relay;
    heater = new Heater(new Relay);
#endif  // ENABLE_HARDWARE

    // Configure the filter wheel positions.
    try {
        for (int i = 0; i < 100; i++) {
            // Read the item in the configuration file.
            libconfig::Setting& setting =
                      (configuration->lookup("opts.filterwheel.filters"))[i];

            std::string filterName = (const char *)setting["name"];

            // Add the filter to the filter wheel.
            wheel->addFilter(filterName, new FilterPosition);
        }
    } catch (libconfig::SettingNotFoundException ex) { }

    // Specify which is the blocked position on the filter wheel
    // When the filter wheel needs to move to blocked, this is where it will go.
    const char * str = configuration->lookup("opts.filterwheel.blocked");
    std::string blocked(str);
    wheel->setBlockedPosition(blocked);

    // Specify which is the clear position.
    str = configuration->lookup("opts.filterwheel.clear");
    std::string clear(str);
    wheel->setClearPosition(clear);

    iface = new SockLibInterface;

    almaFlat = 0;
    if(getConfig()->exists("opts.factory_flat")) {
        const char * flatFile;
        flatFile = getConfig()->lookup("opts.factory_flat");
        Image img(flatFile);
        factoryFlat = new CalibrationImage(img);
        factoryFlat->setFlatSource(CalibrationImage::SRC_FACTORY);
        delete [] img.getPixels();
    } else {
        factoryFlat = 0;
    }
}

Opts::~Opts()
{
    delete power;
#ifdef ENABLE_HARDWARE
    delete camera;
    delete sfw;
    delete shutter;
    delete sunSenor1;
    delete sunSensor2;
    delete tubeSensor;
    delete filterSensor;
    delete cemraPower;
    delete heater;
    delete fan;
    delete pcbSunSensor;
#else
    delete camera;
    delete wheel;
    delete focuser;
    delete shutter;
    delete sunSensor1;
    delete sunSensor2;
    delete pcbSunSensor;
    delete tubeSensor;
    delete filterSensor;
    delete fan;
    delete cameraPower;
    delete heater;
#endif
    delete iface;
    delete factoryFlat;
}

Opts* Opts::getOpts()
{
    return this;
}

void Opts::setID(std::string newID)
{
    identifier = newID;
}

std::string Opts::getID()
{
    return identifier;
}

void Opts::setAlmaFlat(CalibrationImage * flat)
{
    if (almaFlat) delete almaFlat;
    almaFlat = flat;
}

void Opts::protect()
{
    if (!isProtected) {
        isProtected = true;
        slewFilter = getFilterWheel()->getFilter();
        getFilterWheel()->go("Blocked");
        getShutter()->close();
    }
}

void Opts::unprotect()
{
    if (isProtected) {
        isProtected = false;
        getFilterWheel()->go(slewFilter);
        if (shutterOpen) getShutter()->open();
    }
}

Relay * Opts::getPower()
{
    return power;
}

Camera * Opts::getCamera()
{
    return camera;
}

ClientInterface * Opts::getInterface()
{
    return iface;
}

void Opts::galilEngStatus(std::stringstream& ss)
{
#ifdef ENABLE_HARDWARE
    std::vector<char> r = galil->record();
    std::vector<std::string> vars = galil->sources();

    ss << std::setw(8) << "@AN[3]";
    ss << ' ' << std::setw(10) << galil->command("MG @AN[3]");
    ss << "Analog Input 3\n";

    ss << std::setw(8) << "@AN[4]";
    ss << ' ' << std::setw(10) << galil->command("MG @AN[4]");
    ss << "Analog Input 4\n";

    for (int i = 0; i < vars.size(); i++) {
        ss << std::setw(8) << vars[i];
        ss << ' ' << std::setw(10) << galil->sourceValue(r, vars[i]);
        ss << ' ' << galil->source("Description", vars[i]) << '\n';
    }
#endif // ENABLE_HARDWARE
}

void Opts::monitorCamera()
{
    bool shutdown = false;
    Camera * cam = getCamera();

    while (1) {
        while (cam->getState() != Camera::CAM_POWEROFF) {
            sleep(1);
            double t_filter = getFilterSensor()->getTemperature();

            if ((t_filter > 35) || (t_filter < -20)) {
                logFile << "Camera has overheated and must power down." << std::endl;
                cam->setState(Camera::CAM_POWEROFF);
                cameraPower->off();
                shutdown = false;
            }

            boost::thread::yield();
        }

        while (cam->getState() == Camera::CAM_POWEROFF) {
            sleep(1);
            boost::thread::yield();
        }
    }
}
