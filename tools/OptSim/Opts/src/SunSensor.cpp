#include "SunSensor.h"
#include "Opts.h"
#include "Sensor.h"

#include <unistd.h>

#include <iostream>

#if HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H

#ifdef ENABLE_HARDWARE
#include "GalilSensor.h"
#endif // ENABLE_HARDWARE

SunSensor::SunSensor(int num)
{
    if (theOPTS.getConfig() -> exists("opts.sunthresh")) {
        threshold = theOPTS.getConfig() -> lookup("opts.sunthresh");
    } else {
        threshold = 5.;
    }

    tripped = false;
#if ENABLE_HARDWARE
    sensor = new GalilSensor(theOPTS.getGalil(), num);
#else
    sensor = new Sensor();
#endif // ENABLE_HARDWARE
}

void SunSensor::monitor()
{
    while (true) {
        // Ralph Marson 2009-09-30. Chnage the sense of this comparison to
        // ensure the sun sensor is not continually tripped when we are
        // simulating.
        if (sensor -> read() > getThreshold()) {
            // If the sensor is not already tripped, trip it.
            if (!getState()) trip();
        } else {
            // If the sensor is not already cleared, clear it.
            if (getState()) untrip();
        }
        sleep(1);
    }
}

void SunSensor::trip()
{
    tripped = true;
    theOPTS.protect();
}

void SunSensor::untrip()
{
    theOPTS.unprotect();
    tripped = false;
}

double SunSensor::getLevel()
{
    return sensor -> read();
}
