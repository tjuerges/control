
#include "Image.h"
#include "CalibrationImage.h"
#include "CentroidFactory.h"
#include "Centroid.h"
#include "Opts.h"
#include "Camera.h"
#include "HotPixels.h"
#include "ClientInterface.h"
#include "Focuser.h"

#include <cstring>
#include <cmath>
#include <sstream>

#if HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H

#if HAVE_CFITSIO_FITSIO_H
#include <cfitsio/fitsio.h>
#else
#include <fitsio.h>
#endif

#include <boost/filesystem/operations.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/condition.hpp>

Image::Image()
{
    bias = blank = flat = 0;

    bias = theOPTS.getCamera() -> getBias();
    blank = theOPTS.getCamera() -> getBlank();
    flat = theOPTS.getCamera() -> getFlat();

    shutter = true;
    heater = false;
    sunSensor = false;
    centroid = 0;
    state = EXPOSING;
    type = Image::IMG_STAR;
    pixels = 0;
    fitsBuffer = 0;
}

Image::Image(Image& from)
{
    roi = from.getROI();

    type = from.type;

    sequenceNumber = from.getSequenceNumber();
    state = from.state; 

    expTime = from.expTime;
    filter = from.filter;
    sequenceNumber = from.sequenceNumber;
    dateObs = from.dateObs;

    hotpix = from.hotpix;

    bias = from.bias;
    blank = from.blank;
    flat = from.flat;

    centroid = 0;
    shutter = from.shutter;
    heater = from.heater;
    sunSensor = from.sunSensor;
    pixels = 0;

    type = from.type;

    fitsSize = from.fitsSize;
    if (from.fitsBuffer > 0) {
        fitsBuffer = new char[fitsSize];
        memcpy(fitsBuffer, from.fitsBuffer, fitsSize);
    }
}

Image::Image(const char * fits)
{
    fitsfile * fptr;
    int status = 0;
    long naxis[2];
    int subx, suby;

    char * fileName = new char[strlen(fits) + 1];
    strcpy(fileName, fits);
    fits_open_file(&fptr, fileName, READONLY, &status);

    // Use the FITS headers to compute a ROI.
    fits_get_img_size(fptr, 2, naxis, &status);
    fits_read_key(fptr, TINT, "SUBX", &subx, NULL, &status);
    fits_read_key(fptr, TINT, "SUBY", &suby, NULL, &status);

    roi.rowLo = suby - 1;
    roi.rowHi = naxis[1] + roi.rowLo - 1;
    roi.colLo = subx - 1;
    roi.colHi = naxis[0] + roi.colLo - 1;

    sequenceNumber = 0;

    long fpixel[2] = {1, 1};
    int anynul;
    pixels = new signed short[naxis[0] * naxis[1]];
    fits_read_pix(fptr, TSHORT, fpixel, naxis[0] * naxis[1],
                  NULL, pixels, &anynul, &status); 

    char date[40];
    fits_read_key(fptr, TSTRING, "DATE-OBS", date, NULL, &status); 
    dateObs = std::string(date);

    fits_close_file(fptr, &status);
    delete [] fileName;

    fitsBuffer = 0;
    centroid = 0;
}

Image::~Image()
{
    if (centroid) delete centroid;
    //if (pixels) delete [] pixels;
    if (fitsBuffer) delete [] fitsBuffer; 
}

void Image::setSequenceNumber(const int num)
{
    sequenceNumber = num;
}

int Image::getSequenceNumber()
{
    return sequenceNumber;
}

void Image::setDateObs(boost::posix_time::ptime t)
{
    using namespace boost::posix_time;

    dateObs = to_iso_extended_string(t);
    logFile << "DATE-OBS: " << dateObs << std::endl;

#if 0
    boost::posix_time::time_facet * f = new boost::posix_time::time_facet();
    f -> format("%Y-%m-%dT%H:%M:%s");

    std::stringstream ss;
    ss.imbue(std::locale(ss.getloc(), f));
    ss << boost::posix_time::microsec_clock::universal_time(); 
    dateObs = ss.str();

    delete f;
#endif
}


void Image::setROI(RegionOfInterest newRoi)
{
    roi = newRoi;
}

RegionOfInterest Image::getROI()
{
    return roi;
}

void Image::setBits(signed short * bits)
{
    pixels = bits;
}

signed short * Image::getBits()
{
    return pixels;    
}

signed short Image::getPixel(int x, int  y)
{
    int columns = (roi.colHi - roi.colLo + 1);
    int n = (y - roi.rowLo) * columns + (x - roi.colLo);
    return pixels[n];
}

signed short * Image::getPixels() const
{
    return pixels;
}

int Image::countPixels() const
{
    int n = roi.colHi - roi.colLo + 1;
    n *= roi.rowHi - roi.rowLo + 1;
    return n;
}

void Image::dispatch(Image * img)
{
    boost::mutex::scoped_lock lock(processMutex);

    if (img) logFile << "Dispatching image " << img -> getSequenceNumber() << std::endl;

    while (idle == false) completeCond.wait(lock);
    if (img) {
        processImage = img;
        idle = false;
        processCond.notify_all();
    }

    if (img) logFile << "Image " << img -> getSequenceNumber() << " dispatched." << std::endl;
}

void Image::process()
{
    using namespace boost::posix_time;
    boost::mutex::scoped_lock lock(processMutex);

    while (1) {
        // Wait for a new image to become available.
        while (idle) processCond.wait(lock);

	logFile << "Processing Image " << processImage -> getSequenceNumber() ;// << std::endl;
        logFile << " " << to_iso_extended_string(microsec_clock::universal_time()) << std::endl;

        processImage -> setState(PROCESSING);

        RegionOfInterest roi = processImage -> getROI();
        int n = roi.colHi - roi.colLo + 1;
        n *= roi.rowHi - roi.rowLo + 1;
        signed short * pixels = processImage -> getBits();
        for (int i = 0; i < n; i++) {
            pixels[i] = (unsigned short)(pixels[i]) / 2;
        }

        // Apply calibration images.
        processImage -> applyHotPix();
        processImage -> applyBias();
        processImage -> applyBlank();
        processImage -> applyFlat();

        logFile << "Computing centroid " << to_iso_extended_string(microsec_clock::universal_time()) << std::endl;
        // Create a new centroid object, calculate the centroid.
        processImage -> centroid = CentroidFactory::get() -> getCentroid();
        processImage -> centroid -> setImage(processImage);
        processImage -> centroid -> go();

        // Write to disk
        processImage -> setState(WRITING);
        logFile << "Generating FITS " << to_iso_extended_string(microsec_clock::universal_time()) << std::endl;
        processImage -> createFits();

        // Done.
        processImage -> setState(READY);

        if (processImage -> sendTo) {
            logFile << "Sending Image " << to_iso_extended_string(microsec_clock::universal_time()) << std::endl;
            theOPTS.getInterface() -> sendImage(*(processImage -> sendTo), 
                                                processImage);
            delete processImage -> sendTo;
            processImage -> sendTo = 0;
        }

        idle = true;

        logFile << "Finished processing image " << processImage -> getSequenceNumber() ;// << std::endl;
        logFile << " " << to_iso_extended_string(microsec_clock::universal_time()) << std::endl;
        completeCond.notify_all();

    }
}

#if 0
void Image::applyHotPix(CalibrationImage * hotpix)
{
    int i,j;

    this -> hotpix = hotpix;
    if (hotpix == 0) return;

    for (i = roi.rowLo + 1; i <= roi.rowHi - 1; i++) {
        for (j = roi.colLo + 1; j <= roi.colHi - 1; j++) {
            if (hotpix -> pix[j][i] > 0.5) {
                signed short neighbors[8];
                neighbors[0] = getPixel(i-1, j-1);
                neighbors[1] = getPixel(i-1, j);
                neighbors[2] = getPixel(i-1, j+1);
                neighbors[3] = getPixel(i, j-1);
                neighbors[4] = getPixel(i, j+1);
                neighbors[5] = getPixel(i+1, j-1);
                neighbors[6] = getPixel(i+1, j);
                neighbors[7] = getPixel(i+1, j+1);

                std::nth_element(neighbors, neighbors+4, neighbors+8);

                int columns = (roi.rowHi - roi.rowLo + 1);
                int n = (j - roi.colLo) * columns + (i - roi.rowLo);
                pixels[n] = (neighbors[3] + neighbors[4]) / 2;
            }
        }
    }
}
#endif

void Image::applyHotPix()
{
    if (hotpix == 0) return;

    std::vector<std::pair<int, int> > list = hotpix -> getPixels();

    RegionOfInterest roi = getROI();

    for (int i = 0; i < list.size(); i++) {
        int x = list[i].first;
        int y = list[i].second;

        if ((x < roi.rowLo + 1) || (x > roi.rowHi - 1)) {
            continue;
        }
        if ((y < roi.colLo + 1) || (y > roi.colHi - 1)) {
            continue;
        }

        signed short neighbors[8];
        neighbors[1] = getPixel(x-1, y);
        neighbors[2] = getPixel(x-1, y+1);
        neighbors[3] = getPixel(x, y-1);
        neighbors[4] = getPixel(x, y+1);
        neighbors[5] = getPixel(x+1, y-1);
        neighbors[6] = getPixel(x+1, y);
        neighbors[7] = getPixel(x+1, y+1);

        std::nth_element(neighbors, neighbors+4, neighbors+8);

        int columns = (roi.rowHi - roi.rowLo + 1);
        int n = (y - roi.colLo) * columns + (x - roi.rowLo);
        pixels[n] = (neighbors[3] + neighbors[4]) / 2;
    }
}
    

void Image::applyBias()
{
    if (bias == 0) return;

    if (getType() == IMG_BIAS) return;

    // Do not apply the calibration image if it only partially covers the
    // science image.
    RegionOfInterest imgroi = getROI();
    RegionOfInterest calroi = bias -> getROI();
    if (imgroi.colLo < calroi.colLo) return;
    if (imgroi.rowLo < calroi.rowLo) return;
    if (imgroi.colHi > calroi.colHi) return;
    if (imgroi.rowHi > calroi.rowHi) return;

    logFile << "Applying bias: " << bias -> getSequenceNumber() << std::endl;

    int i, j;
    signed short * ptr = pixels;

    for (i = roi.rowLo; i <= roi.rowHi; i ++) {
        for (j = roi.colLo; j <= roi.colHi; j ++) {
            *ptr++ -= (signed short)round(bias -> pix[j][i]);
        }
    }
}

void Image::applyBlank()
{
    if (blank == 0) return;

    ImageType type = getType();
    if (type == IMG_BIAS) return;
    if (type == IMG_BLANK) return;

    // Do not apply the calibration image if it only partially covers the
    // science image.
    RegionOfInterest imgroi = getROI();
    RegionOfInterest calroi = blank -> getROI();
    if (imgroi.colLo < calroi.colLo) return;
    if (imgroi.rowLo < calroi.rowLo) return;
    if (imgroi.colHi > calroi.colHi) return;
    if (imgroi.rowHi > calroi.colHi) return;

    int i, j;
    signed short * ptr = pixels;
    double scale = getExpTime();
    scale /= blank -> getImage() -> getExpTime(); 

    for (i = roi.rowLo; i <= roi.rowHi; i ++) {
        for (j = roi.colLo; j <= roi.colHi; j ++) {
            signed short val = *ptr;
            val -= (signed short)round(scale * (blank -> pix[j][i]));
            *ptr++ = val;
        }
    }
}

void Image::applyFlat()
{
    if (flat == 0) return;

    ImageType type = getType();
    if (type == IMG_BIAS) return;
    if (type == IMG_BLANK) return;
    if (type == IMG_FLAT) return;
   
    // Do not apply the calibration image if it only partially covers the
    // science image.
    RegionOfInterest imgroi = getROI();
    RegionOfInterest calroi = flat -> getROI();
    if (imgroi.colLo < calroi.colLo) return;
    if (imgroi.rowLo < calroi.rowLo) return;
    if (imgroi.colHi > calroi.colHi) return;
    if (imgroi.rowHi > calroi.colHi) return;

    int i, j;
    signed short * ptr = pixels;

    for (i = roi.rowLo; i <= roi.rowHi; i ++) {
        for (j = roi.colLo; j <= roi.colHi; j ++) {
            *ptr++ /= flat -> pix[j][i];
        }
    }
}


void Image::createFits()
{
    fitsfile * fptr;
    int status;
    long fpixel = 1, naxis = 2, nelements;

    long naxes[2] = { (roi.colHi - roi.colLo + 1),
                      (roi.rowHi - roi.rowLo + 1) };

    status = 0;    // initialize status before calling fitsio routines

    fitsSize = naxes[0] * naxes[1] * 2 + 2880 * 2;
    if (fitsSize % 2880) {
        fitsSize -= (fitsSize % 2880);
        fitsSize += 2880;
    }

    fitsBuffer = new char[fitsSize];
    fits_create_memfile(&fptr, (void **)&fitsBuffer, &fitsSize,
                        0, NULL, &status); 

    // Create the primary array image
    fits_create_img(fptr, SHORT_IMG, naxis, naxes, &status);
    nelements = naxes[0] * naxes[1];


    // Write the array of integers into the image
    fits_write_img(fptr, TSHORT, fpixel, nelements, pixels, &status);

    buildHeader(fptr);
    fits_set_bscale(fptr, 2, 0, &status);

    fits_close_file(fptr, &status);

    return;
}

void Image::buildHeader(fitsfile * fptr)
{
    char buffer[80];
    int status = 0;
    double tmp;
    int tempInt;

    std::stringstream ss;

    double bzero = 0.;
    fits_write_key(fptr, TDOUBLE, "BZERO", &bzero, "", &status);
    double bscale = 2.;
    fits_write_key(fptr, TDOUBLE, "BSCALE", &bscale, "", &status);


    // IMAGETYP: Image type - Star, Blank, Flat, or Bias
    ImageType type = getType();
    switch (type) {
        case (IMG_STAR):
            strcpy(buffer, "Star");
            fits_write_key(fptr, TSTRING, "IMAGETYP", buffer,
                           "Image type (Star, Blank, Flat or Bias)", &status);
            break;
        case (IMG_BLANK):
            strcpy(buffer, "Blank");
            fits_write_key(fptr, TSTRING, "IMAGETYP", buffer,
                           "Image type (Star, Blank, Flat or Bias)", &status);
            break;
        case (IMG_FLAT):
            strcpy(buffer, "Flat");
            fits_write_key(fptr, TSTRING, "IMAGETYP", buffer,
                           "Image type (Star, Blank, Flat or Bias)", &status);
            break;
        case (IMG_BIAS):
            strcpy(buffer, "Bias");
            fits_write_key(fptr, TSTRING, "IMAGETYP", buffer,
                           "Image type (Star, Blank, Flat or Bias)", &status);
            break;
        default:
            break;
    }

    // IMAGENUM: The image sequence number
    int seq = getSequenceNumber();
    fits_write_key(fptr, TINT, "IMAGENUM", &seq,
                   "The image sequence number", &status);

    // INSTRUME: The TCM electronics that made the image
    strncpy(buffer, theOPTS.getID().c_str(), 80);
    fits_write_key(fptr, TSTRING, "INSTRUME", buffer,
                   "The TCM electronics that made the image", &status);

    // CAMERA: The Photon Max Camera that took the image (married to the OPT)
    strncpy(buffer, theOPTS.getCamera() -> getName().c_str(), 80);
    fits_write_key(fptr, TSTRING, "CAMERA", buffer,
                   "The Photon Max Camera that took the image", &status); 

    // TIMESYS: UTC
    strncpy(buffer, "UTC", 80);
    fits_write_key(fptr, TSTRING, "TIMESYS", buffer,
                   "UTC", &status);

    // DATE-OBS: The universal date & time of the exposure to the nearest ms
    strncpy(buffer, dateObs.c_str(), 80);
    fits_write_key(fptr, TSTRING, "DATE-OBS", buffer,
                   "The universal date & time of the exposure", &status);

    // EXPTIME: The exposure time (seconds)
    fits_write_key(fptr, TDOUBLE, "EXPTIME", &expTime,
                   "The exposure time (seconds)", &status);

    // FILTER: The name of the filter being used.
    strncpy(buffer, filter.c_str(), 80);
    fits_write_key(fptr, TSTRING, "FILTER", buffer,
                   "The name of the filter being used", &status);

    // FOCUS: The current focus postion.
    double focus = theOPTS.getFocuser() -> getPosition();
    fits_write_key(fptr, TDOUBLE, "FOCUS", &focus,
                   "The focus position (mm)", &status);

    // BLANKNUM: Image sequence number of the blank sky (or dark image that
    // was subtracted from the image (0 if no subtraction)
    if (blank) {
        int blanknum = blank -> getSequenceNumber();
        fits_write_key(fptr, TINT, "BLANKNUM", &blanknum,
                       "Image sequence number of the blank", &status);
    } else {
        int zero = 0;
        fits_write_key(fptr, TINT, "BLANKNUM", &zero,
                       "Image sequence number of the blank", &status); 
    }

    // Set up the physical coordinate system as described in the FITS spec.
    int one = 1;
    int subImage = roi.colLo + 1;
    fits_write_key(fptr, TINT, "CRPIX1", &one, "", &status);
    fits_write_key(fptr, TINT, "CRVAL1", &subImage, "", &status);
    fits_write_key(fptr, TINT, "CDELT1", &one, "", &status);

    subImage = roi.rowLo + 1;
    fits_write_key(fptr, TINT, "CRPIX2", &one, "", &status);
    fits_write_key(fptr, TINT, "CRVAL2", &subImage, "", &status);
    fits_write_key(fptr, TINT, "CDELT2", &one, "", &status);

    // SUBX: Sub array absolute X (column) coordinate of the first FITS pixel
    subImage = roi.colLo + 1;
    fits_write_key(fptr, TINT, "SUBX", &subImage,
                   "Absolute X (column) coord of the first pixel", &status);

    // SUBY: Sub array absolute Y (row) coordinate of the first FITS pixel
    subImage = roi.rowLo + 1;
    fits_write_key(fptr, TINT, "SUBY", &subImage,
                   "Absolute Y (column) coord of the first pixel", &status);

    // PEAK: Centroid intensity (Sum of counts-minus-background of pixels)
    tmp = centroid -> getPeak();
    fits_write_key(fptr, TDOUBLE, "PEAK", &tmp,
                   "Centroid intensity (Sum of counts)", &status);

    // CENTX: Centroid position in absolute X (CCD coordinate of a full CCD
    // image)
    tmp = centroid -> getX() + 1;
    fits_write_key(fptr, TDOUBLE, "CENTX", &tmp,
                   "Centroid position in absolute X", &status);

    // CENTY: Centroid position in absolute Y
    tmp = centroid -> getY() + 1;
    fits_write_key(fptr, TDOUBLE, "CENTY", &tmp,
                   "Centroid position in absolute Y", &status);

    // WIDTHX: Centroid width in X
    tmp = centroid -> getFwhmX();
    fits_write_key(fptr, TDOUBLE, "WIDTHX", &tmp,
                   "Centroid width in X", &status);

    // WIDTHY: Centroid width in Y
    tmp = centroid -> getFwhmY();
    fits_write_key(fptr, TDOUBLE, "WIDTHY", &tmp,
                   "Centroid width in Y", &status);

    // RMS: Centroid RMS of its perimeter pixels
    tmp = centroid -> getRMS();
    fits_write_key(fptr, TDOUBLE, "RMS", &tmp,
                   "Centroid RMS of its perimeter pixels", &status);

    // THRESH: Centroid S/N threshold (brightest star peak must exceed this
    // noise)
    tmp = centroid -> getThreshold();
    fits_write_key(fptr, TDOUBLE, "THRESH", &tmp,
                   "Centroid S/N threshold", &status);

    // VALID: T or F (T = valid, F = invalid: no star was brighter than the
    // threshold)
    int isValid = (centroid -> getValid() ? 1 : 0);
    fits_write_key(fptr, TLOGICAL, "VALID", &isValid,
                   "whether the star is brighter than the threshold", &status);

    // ALGORITH: 1, 2, or 3 /Which of the 3 softare routines was used to 
    // compute centroid
    int algorith = centroid -> getAlgorithmID();
    fits_write_key(fptr, TINT, "ALGORITH", &algorith,
                   "1, 2, or 3 - which centroid algorithm was used", &status);

    // ALGORP1: Value of the algorithm's first parameter (if any [size of 
    // square, ...])
    tmp = centroid -> getParam1();
    fits_write_key(fptr, TDOUBLE, "ALGORP1", &tmp,
                   "Value of the algorithm's first parameter", &status);

    // ALGORP2: Value of the algorithm's second parameter (if any)
    tmp = centroid -> getParam2();
    fits_write_key(fptr, TDOUBLE, "ALGORP2", &tmp,
                   "Value of the algorithm's second parameter", &status);

    // ALGORP3: Value of the algorithm's third parameter (if any)
    tmp = centroid -> getParam3();
    fits_write_key(fptr, TDOUBLE, "ALGORP3", &tmp,
                   "Value of the algorithm's third parameter", &status);

    // SHUTTER: Shutter was open(T) or closed(F)
    int active = (getShutter() ? 1 : 0);
    fits_write_key(fptr, TLOGICAL, "SHUTTER", &active,
                   "Shutter was open(T) or closed(F)", &status);

    // HEATER: Heater was on(T) or off(F)
    active = (getHeater() ? 1 : 0);
    fits_write_key(fptr, TLOGICAL, "HEATER", &active,
                   "Heater was on(T) or off(F)", &status);
    // SUNSENSR: Sun detected(T) or not(F)
    active = (getSunSensor() ? 1 : 0);
    fits_write_key(fptr, TLOGICAL, "SUNSENSR", &active,
                   "Sun detected(T) or not(F)", &status);

    // TEMPTUBE: Temperature at optical tube
    tmp = getTubeTemp();
    fits_write_key(fptr, TDOUBLE, "TEMPTUBE", &tmp,
                   "Temperature at optical tube", &status);

    // TEMPFILT: Temperature at filter box
    tmp = getFilterTemp();
    fits_write_key(fptr, TDOUBLE, "TEMPFILT", &tmp,
                   "Temperature at filter box", &status);

    // CCDTEMP: Temperature of detector
    tmp = getCcdTemp();
    fits_write_key(fptr, TDOUBLE, "CCDTEMP", &tmp,
                   "Temperature of detector", &status);

    // CCDGAIN: Gain setting of detector
    tmp = theOPTS.getCamera() -> getGain();
    fits_write_key(fptr, TDOUBLE, "CCDGAIN", &tmp,
                   "Gain setting of detector", &status);

    // FLATID: Flat field ID: "None", "Factory", or Date-Time in Flat FITS
    // header
    CalibrationImage::FlatSource source;
    if (flat == 0) source = CalibrationImage::SRC_NONE;
    else source = flat -> getFlatSource();

    switch (source) {
        case (CalibrationImage::SRC_ALMA):
            strncpy(buffer, flat -> getDateObs().c_str(), 80);
            break;
        case (CalibrationImage::SRC_FACTORY):
            strcpy(buffer, "Factory");
            break;
        case (CalibrationImage::SRC_NONE):
        default:
            strcpy(buffer, "None");
    }
    fits_write_key(fptr, TSTRING, "FLATID", buffer,
                   "Flat field ID: None, Factory, or Date-Time", &status);
            

    // BIASNUM: Image sequence number of the Bias Image (0 if none)
    if (bias) {
        int biasNum = bias -> getSequenceNumber();
        fits_write_key(fptr, TINT, "BIASNUM", &biasNum,
                       "Image sequence number of the Bias Image", &status);
    } else {
        int zero = 0;
        fits_write_key(fptr, TINT, "BIASNUM", &zero,
                       "Image sequence number of the Bias Image", &status);
    }

}

void Image::setSendTo(MessageParty * to)
{
    if (to) sendTo = new MessageParty(*to);
    else sendTo = 0;
}

volatile bool Image::idle = true;
Image * Image::processImage = 0;
boost::mutex Image::processMutex;
boost::condition Image::processCond;
boost::condition Image::completeCond;
