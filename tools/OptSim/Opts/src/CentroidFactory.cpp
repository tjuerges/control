#include "CentroidFactory.h"
#include "Centroid.h"

CentroidFactory * CentroidFactory::get()
{
    if (instance == 0) instance = new CentroidFactory();
    return instance;
}

void CentroidFactory::select(int id)
{
    algorithm = id;
}

Centroid * CentroidFactory::getCentroid()
{
    Centroid * centroid;
    switch (algorithm) {
        case 1:
            centroid = new Centroid();
            break;
        case 2:
            // Example, to register a new centroid algorithm: 
            // centroid = new CustomMadeCentroidAlgorithm();
            // break;
        case 3:
        default:
            centroid = new Centroid();
            break;
    }

    centroid -> setParam1(param1);
    centroid -> setParam2(param2);
    centroid -> setParam3(param3);
    centroid -> setThreshold(thresh);

    return centroid;
}

CentroidFactory * CentroidFactory::instance = 0;
