#ifndef GALILSENSOR_H
#define GALILSENSOR_H

#include "Sensor.h"
#include <boost/date_time/posix_time/posix_time.hpp>

class Galil;

/** A sensor provided by a Galil controller. */
class GalilSensor : public Sensor {
    public:
        /** Constructor.
         * @param board the galil board that provides the sensor. 
         * @param n the analog input number connected to the sensor.
         */
        GalilSensor(Galil * board, int n);

        double read();

    private:
        double cached;
        boost::posix_time::ptime cacheTime;

        /** The Galil board that controls this sensor. */
        Galil * galil;

        /** The analog input number associated with the sensor. */
        int inputNumber;
};

#endif //!GALILSENSOR_H
