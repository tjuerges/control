#ifndef GETCOMMAND_H
#define GETCOMMAND_H

#include <map>

#include "Command.h"

class GetCommand : public Command {
    public:
        GetCommand(const char * name);
        virtual bool permission(MessageParty sender,
                                std::vector<std::string> command);

        static void beQuietPlease(MessageParty forMe); 
        static bool checkQuiet(void * handle);
        static void clearRequest(MessageParty forMe);

    private:
        static std::map<void *, bool> politeRequests;
};

class GetCentroid : public GetCommand {
    public:
        GetCentroid() : GetCommand("Centroid") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern GetCentroid getCentroid;

class GetImage : public GetCommand {
    public:
        GetImage() : GetCommand("Image") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern GetImage getImage;

class GetImageCen : public GetCommand {
    public:
        GetImageCen() : GetCommand("ImageCen") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern GetImageCen getImageCen;

std::string parseGetCommand(MessageParty& sender, 
                            std::vector<std::string>& command);
void getCommandSend(MessageParty& sender, int first, int last, int sendWhat);

#endif // !GETCOMMAND_H
