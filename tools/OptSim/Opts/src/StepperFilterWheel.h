#ifndef STEPPERFILTERWHEEL_H
#define STEPPERFILTERWHEEL_H

#include "FilterWheel.h"
#include "Stepper.h"
#include "DigitalInput.h"

/** A filter wheel controlled by a stepper motor, in which the stepper motor
 * turns a wheel containing filters. The filter wheel contains a set of
 * digital inputs. One input is used to indicate whether the filter wheel
 * is properly in position, or between filters. The other digital inputs
 * indicate which filter is in position. 
 * @see FilterWheel
 */
class StepperFilterWheel : public FilterWheel {
    public:
        /** Constructor. */
        StepperFilterWheel(Stepper * s) : FilterWheel(), stepper(s), moving(false), stuck(false) {}

        State getState() const;

        void setPositionMarker(DigitalInput * p) { posMarker = p; }
        void setInput1(DigitalInput * p) { di1 = p; }
        void setInput2(DigitalInput * p) { di2 = p; }

        void setFilterSeparation(int s) { filterSeparation = s; }

        void setDetentSize(int det) { detentSize = det; }

        int getPosition() const;
        int getFilter() const { return getPosition(); } 

        void go(std::string filterName);
        void go(int filterNumber);
        void abort() { stepper -> abort(); }

        void step(int direction);

        void init();


        bool getError();
    private:
        /** The stepper motor connected to this filter wheel. */
        Stepper * stepper;

        /** When this digital input is active, it indicates that
         * a filter is in position.
         */
        DigitalInput * posMarker;

        /*@{*/
        /** The state of these digital inputs indicate which filter
         * is in place.
         */
        DigitalInput * di1;
        DigitalInput * di2;
        /*@}*/

        /** The number of steps the motor must move to go from one
         * filter to an adjacent filter.
         */
        int filterSeparation;

        /** The number of motor steps from the detent edge to the center
         * of the detent.
         */
        int detentSize;

        /** True if the filter wheel is moving. */
        bool moving;

        /** True if the filter wheel appears to be stuck. */
        bool stuck;
};

#endif // !STEPPERFILTERWHEEL_H
