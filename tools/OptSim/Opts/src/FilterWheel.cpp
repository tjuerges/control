#include "FilterWheel.h"

#include <string>
#include <boost/thread/mutex.hpp>
#include <iostream>

FilterWheel::FilterWheel()
{
    current = -1;
    blockedPosition = -1;
    clearPosition = -1;
    state = FW_UNKNOWN;
    error = false;
}

FilterWheel::~FilterWheel()
{
    boost::mutex::scoped_lock lock(mutex);

    std::map<const std::string, const FilterPosition *>::iterator iter;
    for (iter = filterPositions.begin(); 
         iter != filterPositions.end(); 
         iter++)
    {
        delete iter -> second;
    }
}

void FilterWheel::addFilter(const std::string name, 
                            FilterPosition * position)
{
    boost::mutex::scoped_lock lock(mutex);

    position -> index = filters.size();
    position -> name = name;

    filters.push_back(name);
    filterPositions[name] = position;
}

void FilterWheel::setBlockedPosition(const std::string filterName)
{
    int bp = getFilterNum(filterName);
    boost::mutex::scoped_lock lock(mutex);
    blockedPosition = bp;
}

void FilterWheel::setBlockedPosition(int filterNum)
{
    boost::mutex::scoped_lock lock(mutex);
    blockedPosition = filterNum;
}

void FilterWheel::setClearPosition(const std::string filterName)
{
    int cp = getFilterNum(filterName);
    boost::mutex::scoped_lock lock(mutex);
    clearPosition = cp;
}

void FilterWheel::setClearPosition(int filterNum)
{
    boost::mutex::scoped_lock lock(mutex);
    clearPosition = filterNum;
}

void FilterWheel::init()
{

}

const FilterPosition * FilterWheel::getPosition(const std::string name)
{
    boost::mutex::scoped_lock lock(mutex);
    if (filterPositions.count(name) == 0) return 0;
    return filterPositions[name];
}

const FilterPosition * FilterWheel::getPosition(int num)
{
    return filterPositions[filters[num]];
}

void FilterWheel::go(std::string filterName)
{
    go(getFilterNum(filterName));
}

void FilterWheel::go(int filterNumber)
{
    boost::mutex::scoped_lock lock(mutex);
    current = filterNumber;
    // Ralph Marson 2009-09-30. This is to prevent the filter wheel always
    // being in an unknown state when we are simulating.
    state =  FW_FILTERED;
}

int FilterWheel::getFilter() const 
{
    return current;
}

int FilterWheel::getFilterNum(std::string name) const
{
    for (unsigned int i = 0; i < filters.size(); i++) {
        if (name == filters[i]) {
            return i;
        }
    }

    return -1;
}

std::string FilterWheel::getFilterName(int num) const
{
    if (num == -1) return "Unknown";
    return filters[num];
}
