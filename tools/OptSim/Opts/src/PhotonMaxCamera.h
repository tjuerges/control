#ifndef PHOTONMAXCAMERA_H
#define PHOTONMAXCAMERA_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H

#ifdef ENABLE_HARDWARE

#include "Camera.h"
#include "ClientInterface.h"

#include <boost/thread.hpp>

#include <sstream>

#ifdef WIN32
#  include <master.h>
#  include <pvcam.h>
#else // !WIN32
#  include <pvcam/master.h>
#  include <pvcam/pvcam.h>
#endif // WIN32

class PhotonMaxCamera : public Camera {
    public:
        PhotonMaxCamera();
        virtual ~PhotonMaxCamera();

        std::string getName();
        void setState(Camera::CameraState newState);

        void getEngStatus(std::stringstream& ss);

        double getCurrTemp();
        void setTargetTemp(double temp);

    protected:
        virtual bool preExpose();
        virtual bool startExposure(signed short * pix);
        virtual CameraState checkStatus();
        virtual void sendAbort();
        virtual signed short * getPixels();
        virtual bool postExpose();


    private:
        int16 hCam;

        MessageParty observer;

        boost::thread * thread;

        boost::posix_time::ptime cacheTime;

        signed short * copyTo;
        int nReady;
        int nCopied;
        
        uns16 * imageBuffer;
        uns32 bufferSize;
        bool abort;
        int roiOffset;

        std::string name;
};

#endif // ENABLE_HARDWARE
#endif // ! PHOTONMAXCAMERA_H
