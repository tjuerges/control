#ifndef DATASEQUENCER_H
#define DATASEQUENCER_H

#include <string>
#include <map>

#include "Command.h"

/** Command interpreter class.
 *
 * This sequencer class handles the interface between the
 * ClientInterface and the commands. It contains code to
 * manage the list of commands, listen for commands from the
 * user, evaluate them, and execute them.
 */
class DataSequencer {
    public:
        /** Returns the single allowed instance of the DataSequencer class.
         */
        static DataSequencer * getInstance();

        /** Obtains commands from the user and executes them.
         *
         * The <code>listen</code> function obtains a command
         * from the ClientInterface, breaks the command into
         * tokens, validates permission, and executes the command.
         * This method does not exit until the client ends the
         * session.
         */
        void listen();
        
        /** Registers a command in the command catalog.
         *
         * This method should be called before any user
         * sessions begin. It is recommended that a call
         * to this function be placed in the constructor
         * of each class derived from Command.
         *
         * @param command The command to be registered.
         * @param directive The command's directive, such as "Query".
         * @param name The command's name, such as "CamPower".
         */
        void registerCommand(Command* command, 
                              std::string directive,
                              std::string name);

    protected:        
        /** Defines the allowed command directives.
         *
         * The directive is the first word in a command.
         * Currently, the allowed directives are "Do",
         * "Get", "Query", and "Set". If additional directives
         * are added, they should be placed here.
         */
        DataSequencer();

        /** Parse and executes a command.
         *
         * Given a sender and a command in the form of a string,
         * this method breaks the command into whitespace-separated
         * tokens. It then resolves the string into a corresponding
         * Command object, and checks the command's permission using
         * the Command::<code>permission</code> method, and makes a 
         * call to Command::<code>execute</code>.
         *
         * @param sender The communications party who issued the command.
         * @param command The text of the command exactly as it was received.
         */
        void executeCommand(MessageParty sender, const std::string command);

    private:
        /** A map of maps connecting directive and command names 
         * to Command objects.
         *
         * The exterior map connects the strings "Do", "Set", "Get",
         * and "Query" each to an interior map. The interior map
         * connects a command name, such as "CamPower", to its
         * corresponding Command object. Thus, for example,
         * <code>commands["Query"]["CamPower"] == queryCamPower</code>.
         */
        std::map<const std::string, 
                 std::map<const std::string, Command*> > commands;

        /** The singleton instance of this class.
         */
        static DataSequencer * instance;
};

#endif // DATASEQUENCER_H

