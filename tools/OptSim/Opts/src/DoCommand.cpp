#include <sstream>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <stdlib.h>

#include "GetCommand.h"
#include "Opts.h"
#include "DoCommand.h"
#include "QueryCommand.h"
#include "Image.h"
#include "CalibrationImage.h"
#include "FilterWheel.h"
#include "Focuser.h"
#include "Relay.h"
#include "Shutter.h"
#include "Camera.h"
#include "SunSensor.h"
#include "Heater.h"

DoCommand::DoCommand(const char * name)
{
    DataSequencer::getInstance() -> registerCommand(this, "Do", name);
    commandName = std::string(name);
}

bool DoCommand::permission(MessageParty sender, 
                           std::vector<std::string> command)
{
    std::string master = theOPTS.getInterface() -> getMaster();
    std::string senderName = sender.getName();
    
    if (master == "") return true;
    if (senderName == master) return true;
    
    return false;
}


DoBias doBias;
std::string DoBias::execute(MessageParty sender,
                            std::vector<std::string> command)
{
    bool notify = checkNotify(sender, command);

    if (command.size() == 3) {
        if (command[2] == "zero") {
            theOPTS.getCamera() -> setBias(0);
            return "Bias zeroed";
        }
        
    } else if (command.size() == 2) {
    
        Camera::CameraState state = theOPTS.getCamera() -> getState(); 

        if (state == Camera::CAM_POWEROFF) {
            return "Error " + command[1] + " poweroff";
        }

        if (state != Camera::CAM_IDLE) {
            return "Error " + command[1] + " busy";
        }

        if (theOPTS.getFilterWheel() -> getFilter() != 0) {
            return "Error Bias filterwheel incorrect";
        }

        boost::thread thread(boost::bind(&DoBias::wait, this, notify, sender));
        return "Bias OK";
    }

    return "Error Bias syntax";
}

void DoBias::wait(bool notify, MessageParty sender)
{
    //theOPTS.getCamera() -> wait();
    theOPTS.getCamera() -> takeBias();
    doNotify(notify, sender);
}

DoBlank doBlank;
std::string DoBlank::execute(MessageParty sender,
                             std::vector<std::string> command)
{
    bool notify = checkNotify(sender, command);

    if (command.size() == 3) {
        if (command[2] == "zero") {
            theOPTS.getCamera() -> setBlank(0);
            return "Blank zeroed";
        }
    }

    if (command.size() != 2) {
        return "Error Blank syntax";
    }

    Camera::CameraState state = theOPTS.getCamera() -> getState(); 

    if (state == Camera::CAM_POWEROFF) {
        return "Error " + command[1] + " poweroff";
    }

    if (state != Camera::CAM_IDLE) {
        return "Error " + command[1] + " busy";
    }

    boost::thread thread(boost::bind(&DoBlank::wait, this, notify, sender));

    return "Blank OK";
}
    
void DoBlank::wait(bool notify, MessageParty sender)
{
    theOPTS.getCamera() -> takeBlank();
    doNotify(notify, sender);
}

DoCamPower doCamPower;
std::string DoCamPower::execute(MessageParty sender,
                                std::vector<std::string> command)
{
    if (command.size() == 3) {
        if (command[2] == "On") {
            theOPTS.getCameraPower() -> on();
            sleep(3);
            theOPTS.getCamera() -> setState(Camera::CAM_IDLE);
        }
        if (command[2] == "Off") {
            theOPTS.getCamera() -> setState(Camera::CAM_POWEROFF);
            theOPTS.getCameraPower() -> off();
        }
    }
    
    return queryCamPower.execute(sender, command);
}

DoExit doExit;
std::string DoExit::execute(MessageParty sender,
                            std::vector<std::string> command)
{
    exit(EXIT_SUCCESS);
    return "";
}

DoExpose doExpose;
std::string DoExpose::execute(MessageParty sender,
                              std::vector<std::string> command)
{
    double exposure;
    int expMilliseconds;
    int howMany = 1;
    MessageParty * sendTo = 0;

    bool notify = checkNotify(sender, command);

    int last = command.size() - 1;
    if (command[last] == "/Send") {
        command.pop_back();
        sendTo = new MessageParty(sender);
    }

    std::stringstream ss(std::stringstream::in | std::stringstream::out);

    if (command.size() > 2) {
        if (command[2] == "abort") {
            theOPTS.getCamera() -> abort();
            return command[1] + " aborted";
        }

        ss << command[2];
        ss >> exposure;
        if (ss.fail()) {
            return "Error " + command[1] + " bad parameter " + command[2];
        }

        expMilliseconds = (int)((0.0005 + exposure) * 1000);
        theOPTS.getCamera() -> setExptime(expMilliseconds);
        ss.clear(std::stringstream::goodbit);
        if (command.size() > 3) {
            ss << command[3];
            ss >> howMany;
            if (ss.fail()) {
                return "Error " + command[1] + " bad parameter " + command[3];
            }
        }
    } else {
        expMilliseconds = theOPTS.getCamera() -> getDefaultExptime();
        theOPTS.getCamera() -> setExptime(expMilliseconds);
    }

    // Check for the slew flag. If it's set, unset it and restore the filter.
    if (theOPTS.getSlewing()) {
        theOPTS.getFilterWheel() -> go(theOPTS.getSlewFilter());
        theOPTS.setSlewing(false);
    }

    std::cout << "Exposing " << howMany << std::endl;
    // Send the expose command to the camera.
    Camera::CameraState state = theOPTS.getCamera() -> expose(howMany, sendTo);

    
    if (state == Camera::CAM_POWEROFF) {
        return "Error " + command[1] + " poweroff";
    }

    if (state == Camera::CAM_BUSY) {
        return "Error " + command[1] + " busy";
    }

    boost::thread thread(boost::bind(&DoExpose::wait, this, notify, sender));

    exposure = expMilliseconds / 1000.;

    if (command[1] == "Video") return "Video OK";

    std::ostringstream response;
    response << command[1] << " " << exposure << " " << howMany << std::ends;
    return response.str();
}

void DoExpose::wait(bool notify, MessageParty sender)
{
    theOPTS.getCamera() -> wait();
    doNotify(notify, sender);
}


DoFan doFan;
std::string DoFan::execute(MessageParty sender,
                           std::vector<std::string> command)
{
    if (command.size() != 3) return "Error fan syntax";

    Relay * fan = theOPTS.getFan();

    if (command[2] == "On") {
        fan -> on();
        return "Fan OK";
    }

    if (command[2] == "Off") {
        fan -> off();
        return "Fan OK";
    }

    return "Error fan syntax";
}

DoFilter doFilter;
std::string DoFilter::execute(MessageParty sender,
                              std::vector<std::string> command)
{

    if (theOPTS.getSlewing()) {
        return "Error Filter slewing is on";
    }

    if ((theOPTS.getSunSensor1() -> getState()) ||
        (theOPTS.getSunSensor2() -> getState()))
    {
        return "Error Filter sun sensor is tripped";
    }

    bool notify = checkNotify(sender, command);

    if (command.size() != 3) return "Error Filter bad syntax";

    if ((command[2] != "Blocked") &&
        (command[2] != "Clear") &&
        (command[2] != "IR") &&
        (command[2] != "Blank") &&
        (command[2] != "Other"))
    {
        return "Error Filter bad name";
    }

    boost::thread thread(boost::bind(&DoFilter::go, this, 
                                     command[2], notify, sender));

    return "Filter " + command[2];
}

void DoFilter::go(std::string filter, bool notify, MessageParty sender)
{
    theOPTS.getFilterWheel() -> go(filter);
    doNotify(notify, sender);
}


DoFilterInit doFilterInit;
std::string DoFilterInit::execute(MessageParty sender,
                                  std::vector<std::string> command)
{
    bool notify = checkNotify(sender, command);
    boost::thread thread(boost::bind(&DoFilterInit::doInit, this,
                                     notify, sender));
    return "FilterInit started";
}

void DoFilterInit::doInit(bool notify, MessageParty sender)
{
    theOPTS.getFilterWheel() -> init();
    doNotify(notify, sender);
}

DoFlatLoad doFlatLoad;
std::string DoFlatLoad::execute(MessageParty sender,
                                std::vector<std::string> command)
{
    char * file = theOPTS.getInterface() -> receiveFits();
    if (file == 0) return "Error FlatLoad no FITS";

    Image img(file);
    unlink(file);
    RegionOfInterest roi = img.getROI();
    if (roi.rowHi == 0) {
        return "Error FlatLoad bad FITS";
    }

    if ((roi.rowHi > 1023) || (roi.colHi > 1023)) {
        return "Error FlatLoad wrong size";
    }

    CalibrationImage * flat = new CalibrationImage(img);
    flat -> normalize();

    delete [] img.getPixels();

    theOPTS.getCamera() -> setFlat(flat);
    theOPTS.setAlmaFlat(flat);
    flat -> setFlatSource(CalibrationImage::SRC_ALMA);

    return "FlatLoad OK";
}


DoFocus doFocus;
std::string DoFocus::execute(MessageParty sender,
                             std::vector<std::string> command)
{
    if (command.size() < 3) return "Error Focus syntax";

    bool notify = checkNotify(sender, command);

    std::stringstream ss(command[2]);
    double pos;
    ss >> pos;
    if (ss.fail()) {
        return "Error Focus bad position";
    }

    boost::thread thread(boost::bind(&DoFocus::go, this, notify, pos, sender));

    return "Focus " + command[2];
}

void DoFocus::go(bool notify, double pos, MessageParty sender)
{
    Focuser * focuser = theOPTS.getFocuser();
    focuser -> goTo(pos);

    doNotify(notify, sender);
}


DoFocusInit doFocusInit;
std::string DoFocusInit::execute(MessageParty sender,
                                 std::vector<std::string> command)
{
    bool notify = checkNotify(sender, command);

    boost::thread thread(boost::bind(&DoFocusInit::doInit, this, 
                                     notify, sender));

    return "FocusInit started";
}

void DoFocusInit::doInit(bool notify, MessageParty sender)
{
    theOPTS.getFocuser() -> init();
    doNotify(notify, sender);
}

DoShutter doShutter;
std::string DoShutter::execute(MessageParty sender,
                               std::vector<std::string> command)
{
    bool notify = checkNotify(sender, command);

    if (command.size() != 3) {
        doNotify(notify, sender);
        return "Error Shutter bad";
    }

    if (command[2] == "Open") {
        theOPTS.getHeater() -> off();
        boost::thread thread(boost::bind(&DoShutter::open, this, 
                                         notify, sender));
        return "Shutter OK";
    }

    if (command[2] == "Close") {
        boost::thread thread(boost::bind(&DoShutter::close, this,
                                         notify, sender));
        return "Shutter OK";
    }

    doNotify(notify, sender);
    return "Error Shutter bad";
}

void DoShutter::open(bool notify, MessageParty sender)
{
    theOPTS.setShutterOpen(true);
    theOPTS.getShutter() -> open();
    doNotify(notify, sender);
}

void DoShutter::close(bool notify, MessageParty sender)
{
    theOPTS.setShutterOpen(false);
    theOPTS.getShutter() -> close();
    doNotify(notify, sender);
}


DoShutUp doShutUp;
std::string DoShutUp::execute(MessageParty sender,
                              std::vector<std::string> command)
{
    GetCommand::beQuietPlease(sender);
    return "ShutUp OK";
}


DoSlew doSlew;
std::string DoSlew::execute(MessageParty sender,
                            std::vector<std::string> command)
{
    theOPTS.setSlewFilter(theOPTS.getFilterWheel() -> getFilter());
    theOPTS.getFilterWheel() -> go("Blocked");
    theOPTS.setSlewing(true);

    return "Slew OK";
}

DoStop doStop;
std::string DoStop::execute(MessageParty sender,
                            std::vector<std::string> command)
{
    /* We will set this up to provide the highest possible protection
     * to the telescope short of cutting the power entirely. The operations
     * are prioritized in such a way as to correct the most likely anticipated
     * problems as quickly as possible.
     */

    /* The focuser should stop first. This is a very quick operation,
     * high on the list of things likely to cause harm if something goes
     * wrong, and it has the side effect of also stopping all other motion
     * controlled on the Galil card.
     */
    theOPTS.getFocuser() -> abort();

    /* The shutter should close. This is an important safety measure and
     * also high on the list.
     */
    theOPTS.getShutter() -> close();

    /* Stop the filterwheel. This should already be done as long as the
     * filter wheel and focuser are both controlled by the Galil card.
     */
    theOPTS.getFilterWheel() -> abort();

    /* Turn off the telescope heater. */ 
    theOPTS.getHeater() -> off();

    /* Stop exposing on the camera. */
    theOPTS.getCamera() -> abort();

    return "Stop OK";
}


DoTelHeater doTelHeater;
std::string DoTelHeater::execute(MessageParty sender,
                                 std::vector<std::string> command)
{
    if (command.size() != 3) {
        return "Error TelHeater syntax";
    }

    if (command[2] == "On") {
        if (theOPTS.getShutter() -> getState() != Shutter::CLOSED) {
            return "Error TelHeater shutter is open";
        }
        theOPTS.getHeater() -> on();
        return queryTelHeater.execute(sender, command);
    }

    if (command[2] == "Off") {
        theOPTS.getHeater() -> off();
        return queryTelHeater.execute(sender, command);
    }

    return "Error TelHeater syntax";
}

DoVideo doVideo;
std::string DoVideo::execute(MessageParty sender,
                             std::vector<std::string> command)
{
    if (command.size() == 3) {
        if (command[2] == "Stop") {
            theOPTS.getCamera() -> setVideo(false);
            return "Video Stop";
        }
    }

    command.push_back("/Send");
    theOPTS.getCamera() -> setVideo();
    return doExpose.execute(sender, command);
}
