#include <iostream>
#include <string>
#include <sstream>
#include <ctime>

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>

#include "Opts.h"
#include "Camera.h"
#include "Image.h"
#include "CalibrationImage.h"
#include "ThermalSensor.h"
#include "FilterWheel.h"

#include "Shutter.h"
#include "Relay.h"
#include "Heater.h"
#include "SunSensor.h"


#define NOISE
#ifdef NOISE
#include <boost/random.hpp>
#include <boost/random/poisson_distribution.hpp>
#include <boost/random/variate_generator.hpp>
#endif // NOISE

#define GAIN 5.93 // electrons per ADU
#define READNOISE 42.5 // electrons

Camera::Camera()
{
    name = "Generic_Camera";
    lastSequenceNumber = 0;
    nextSequenceNumber = 1;
    state = CAM_POWEROFF;
    targetTemperature = -55;
    setType(Image::IMG_STAR);
    gain = 6;

    sendTo = 0;
    video = false;

    if (theOPTS.getConfig() -> exists("opts.default_exptime")) {
        double d = theOPTS.getConfig() -> lookup("opts.default_exptime");
        defaultExptime = (int)(1000 * d);  // Convert seconds to milliseconds.
        currentExptime = defaultExptime;
    } else {
        currentExptime = defaultExptime = 1000;
    }

    bias = 0;
    blank = 0;
    flat = 0;
    hotpix = 0;

    roi.colLo = 0;
    roi.colHi = 1023;
    roi.rowLo = 0;
    roi.rowHi = 1023;
    setGeometry(roi);

    thread = 0;
    error = 0;

    imageBits = new signed short * [MAX_IMAGES];

    for (int i = 0; i < MAX_IMAGES; i++) {
        imageBits[i] = new signed short[1024*1024];
    }
}

std::string Camera::getName()
{
    return name;
}

void Camera::setName(std::string newName)
{
    name = newName;
}

Camera::CameraState Camera::getState()
{
    return state;
}

void Camera::setState(Camera::CameraState newState)
{
    state = newState;
}

void Camera::setImage(Image * img, int seq)
{
    seq = (seq % MAX_IMAGES) - 1;
    if (images.count(seq) != 0) delete images[seq];
    images[seq] = img;
}

Image * Camera::getImage(int seq)
{
    seq = (seq % MAX_IMAGES) - 1;
    if (images.count(seq) == 0) return 0;
    return images[seq];
}

void Camera::setExposuresRemaining(int remain)
{
    exposuresRemaining = remain;
}

int Camera::getExposuresRemaining()
{
    return exposuresRemaining;
}

void Camera::decrExposuresRemaining()
{
    exposuresRemaining--;
}

void Camera::setNextSequenceNum(const int next)
{
    if ((next < 1) || (next > 999999)) {
        logFile << "nextSequenceNumber must be 1-999999" << std::endl;
        return;
    }
    nextSequenceNumber = next;
}

int Camera::getNextSequenceNum()
{
    return nextSequenceNumber;
}

void Camera::incrementSequenceNum()
{
    nextSequenceNumber++;
}

void Camera::setLastSequenceNum(const int last)
{
    lastSequenceNumber = last;
}

int Camera::getLastSequenceNum()
{
    return lastSequenceNumber;
}

double Camera::getCurrTemp()
{
    currentTemperature = -50.;
    return -50.0;
}

double Camera::getTargetTemp()
{
    return targetTemperature;
}

void Camera::setTargetTemp(double temp)
{
    targetTemperature = temp;
}

void Camera::setExptime(int milliseconds)
{
    currentExptime = milliseconds;
}

int Camera::getExptime()
{
    return currentExptime;
}

void Camera::setDefaultExptime(int milliseconds)
{
    defaultExptime = milliseconds;
}

int Camera::getDefaultExptime()
{
    return defaultExptime;
}

void Camera::setEndTime(int milliseconds)
{
    using namespace boost::posix_time;
    //endTime = microsec_clock::universal_time() + millisec(milliseconds);
    seqEndTime = microsec_clock::universal_time() + millisec(milliseconds);
}

void Camera::setExpEndTime(int milliseconds)
{
    using namespace boost::posix_time;
    expEndTime = microsec_clock::universal_time() + millisec(milliseconds);
}

int Camera::getEndTime()
{
    using namespace boost::posix_time;
    //ptime now = microsec_clock::universal_time();
    ptime now = microsec_clock::universal_time();

    time_duration td = milliseconds(0);
    if (now < expEndTime) td += expEndTime - now;

    td += milliseconds(getExposuresRemaining() * getExptime());

    return td.total_milliseconds();
}

void Camera::setROI(RegionOfInterest newROI)
{
    roi = newROI;
}

RegionOfInterest Camera::getROI()
{
    return roi;
}

void Camera::setGeometry(RegionOfInterest whole)
{
    wholeCCD = whole;
}

RegionOfInterest Camera::getGeometry()
{
    return wholeCCD;
}

Image * Camera::setupImage()
{
    using namespace boost::posix_time;

    Image * img = new Image();

    int seq = getNextSequenceNum();
    int exptime = getExptime();

    incrementSequenceNum();

    img -> setSequenceNumber(seq);
    setImage(img, seq);

    img -> setHotPix(hotpix);
    img -> setBias(bias);
    img -> setBlank(blank);
    img -> setFlat(flat);

    img -> setExpTime(exptime / 1000.);
    img -> setFilter(theOPTS.getFilterWheel() -> getFilterName());

    img -> setCcdTemp(getCurrTemp());
    img -> setTubeTemp(theOPTS.getTubeSensor() -> getTemperature());
    img -> setFilterTemp(theOPTS.getFilterSensor() -> getTemperature()); 

    img -> setBits(imageBits[seq % MAX_IMAGES]);
    img -> setType(getType());

    ptime now = microsec_clock::universal_time();

    img -> setDateObs(now);
    expEndTime = now + milliseconds(exptime);

    return img;
}

Camera::CameraState Camera::expose(int howMany, MessageParty * recipient)
{
    getCurrTemp();

    CameraState state = Camera::getState();
    if (state == CAM_POWEROFF) return CAM_POWEROFF;
    if (state == CAM_EXPOSING) return CAM_BUSY;
    Camera::setState(CAM_EXPOSING);

    sendTo = recipient;

    if (preExpose() == false) {
        setState(CAM_IDLE);
        return CAM_IDLE;
    }

    if (video) setExposuresRemaining(1);
    else setExposuresRemaining(howMany);

    setAbortState(false);

    if (video) setEndTime(0); 
    else setEndTime(getExptime() * howMany);

    if (thread != 0) delete thread;
    thread = new boost::thread(boost::bind(&Camera::acquire, this));

    return CAM_EXPOSING;
}

void Camera::acquire()
{
    using namespace boost::posix_time;

    int numImages = getExposuresRemaining();

    RegionOfInterest roi = getROI();
    int numPixels = roi.colHi - roi.colLo + 1;
    numPixels *= roi.rowHi - roi.rowLo + 1;

    boost::posix_time::ptime t(boost::posix_time::microsec_clock::universal_time());

    while (getExposuresRemaining() > 0 || video) {
        if (!video) decrExposuresRemaining();

        logFile << "Image Start Timestamp (1): " << to_iso_extended_string(microsec_clock::universal_time()) << std::endl;

        // Set up a new image.
        Image * current = setupImage();

        if (startExposure(current -> getBits()) == false) break;
        logFile << "Image Start Timestamp (2): " << to_iso_extended_string(microsec_clock::universal_time()) << std::endl;

        current -> setROI(roi);
        current -> setSendTo(sendTo);

        logFile << "Starting image " << current -> getSequenceNumber() << std::endl;


        CameraState state;
        do {

            // Check the status of the shutter.
            if (theOPTS.getShutter() -> getState() != Shutter::OPEN) {
                current -> setShutter(false);
            }

            // Check the status of the heater.
            Heater::State heaterState = theOPTS.getHeater() -> getState();
            if (heaterState == Heater::ON) {
                current -> setHeater(true);
            }

            // Check the status of the sun sensor.
            if (theOPTS.getSunSensor1() -> getState() ||
                theOPTS.getSunSensor2() -> getState()) 
            {
                current -> setSunSensor(true);
            }

            state = checkStatus();

            if (getAbortState() == true) { // Did the user request an abort?
                video = false;
                sendAbort();
                setAbortState(false);
                setExposuresRemaining(0);

                current -> setState(Image::ABORTED);

                acquireCleanup();
                logFile << "Camera thread was told to abort." << std::endl;
                return;
            }

            if (state == CAM_FAILED) {
                current -> setState(Image::ABORTED);

                setError();
                setExposuresRemaining(0);
                acquireCleanup();
                return;
            }

//            if (state != CAM_COMPLETE) boost::thread::yield();
        } while (state != CAM_COMPLETE);

        // Send the image to the image processing thread.
        Image::dispatch(current);

        setLastSequenceNum(current -> getSequenceNumber());
    }


    acquireCleanup();

    boost::posix_time::time_duration dur = 
        boost::posix_time::microsec_clock::universal_time() - t;
    logFile << "Completed in " << to_simple_string(dur) << " (";
    logFile << (numImages * 1.e6 / dur.total_microseconds()) << " Hz)";
    logFile << std::endl;
}

void Camera::wait()
{
    if (thread) thread -> join();
}

void Camera::abort()
{
    aborted = true;
}

void Camera::setAbortState(bool abort)
{
    aborted = abort;
}

bool Camera::getAbortState()
{
    return aborted;
}

void Camera::setBias(CalibrationImage * b)
{
    if (bias != 0) delete bias;
    bias = b;
}

bool Camera::takeBias()
{
    setExptime(0);

    setType(Image::IMG_BIAS);
    CameraState state = expose(1, 0);
    if (state != CAM_EXPOSING) {
        setType(Image::IMG_STAR);
        return false;
    }
    wait();

    setType(Image::IMG_STAR);

    Image * img = getImage(getLastSequenceNum());
    setBias(new CalibrationImage(*img));
    return true;
}

bool Camera::takeBlank()
{
    setExptime(getDefaultExptime());

    setType(Image::IMG_BLANK);
    CameraState state = expose(1, 0);
    if (state != CAM_EXPOSING) {
        setType(Image::IMG_STAR);
        return false;
    }
    wait();
    setType(Image::IMG_STAR);

    Image * img = getImage(getLastSequenceNum());
    setBlank(new CalibrationImage(*img));
    return true;
}

CalibrationImage * Camera::getBias()
{
    return bias;
}

void Camera::setBlank(CalibrationImage * b)
{
    if (blank != NULL) delete blank;
    blank = b;
}

CalibrationImage * Camera::getBlank()
{
    return blank;
}

void Camera::setFlat(CalibrationImage * f)
{
    flat = f;
}

CalibrationImage * Camera::getFlat()
{
    return flat;
}

HotPixels * Camera::getHotPix()
{
    return hotpix;
}

bool Camera::startExposure(signed short * pixels)
{
    signed short * fakeImage = getPixels();

    RegionOfInterest roi = getROI();
    int cols = roi.colHi - roi.colLo + 1;
    int rows = roi.rowHi - roi.rowLo + 1;
    int npix = cols * rows;

    memcpy(pixels, fakeImage, npix * 2);

    delete [] fakeImage;

    return true;
}

Camera::CameraState Camera::checkStatus()
{
    //if (endTime < boost::posix_time::microsec_clock::universal_time()) {
    if (expEndTime < boost::posix_time::microsec_clock::universal_time()) {
        return CAM_COMPLETE;
    } else return CAM_EXPOSING;
}

#define PLAYLIST
#ifdef PLAYLIST

#include <fstream>
#include <unistd.h>
#if HAVE_CFITSIO_FITSIO_H
#include <cfitsio/fitsio.h>
#else
#include <fitsio.h>
#endif

// Read the pixels from the specified FITS file
signed short * getFITSpix(std::string fitsName, int cols, int rows)
{
  int status = 0;
  long naxis[2];
  fitsfile * fptr;

  logFile << "Reading pixels from " + fitsName << std::endl;
  fits_open_file(&fptr, fitsName.c_str(), READWRITE, &status);
  fits_get_img_size(fptr, 2, naxis, &status);
  if (status) {
    logFile << "Error" << status << " reading FITS file" << std::endl;
    return NULL;
  } else {
    if (naxis[0] != cols || naxis[1] != rows) {
      logFile << "Wrong size image in FITS file: " << naxis[0] << " x " << 
	naxis[1] << " (Must be " << cols << " x "  << rows << ")\n";
      return NULL;
    }
  }
  long fpixel[2] = {1, 1};
  int anynul;
  signed short * pixels = new signed short[naxis[0] * naxis[1]];
  fits_read_pix(fptr, TUSHORT, fpixel, naxis[0] * naxis[1],
		NULL, pixels, &anynul, &status); 
  fits_close_file(fptr, &status);
  if (status) {
    logFile << "Error" << status << " reading pixels from FITS file" 
	    << std::endl;
    delete[] pixels;
    return NULL;
  }
  return pixels;
}
#endif // PLAYLIST

signed short * Camera::getPixels()
{
#ifdef NOISE
    using namespace boost;
    using namespace boost::random;
#endif // NOISE

    RegionOfInterest roi = getROI();
    int cols = roi.colHi - roi.colLo + 1;
    int rows = roi.rowHi - roi.rowLo + 1;
    int npix = cols * rows;

#define PLAYLIST
#ifdef PLAYLIST
    /*  Allows one to substitute pixels read from a FITS image file
	The file, imageList.txt, contains a list of images to be read.
	If it doesn't exist, or if the list is empty, or if one or 
	more files in the list don't exist, or if the FITS image doesn't
	match the current size of the ROI, getPixels() will revert 
	to generating a synthetic image.  (If only a few of the images
	in the list don't match or exist, synthetic images will be generated
	for only those files.)
	When the end of imageList.txt is reached, the file is closed and
	re-opened.  Thus, until imageList is changed, getPixels() will 
	repeat the same list of images indefinitely.
     */
    
    static std::string fitsName("");	// Name of the FITS image file
    static bool checkList = true;
    static std::ifstream fitsList;
    signed short * pixFITS = NULL;

    if (checkList) {
      checkList = false;
      fitsList.open("imageList.txt");
      if (! fitsList) {
	logFile << "Can't open imageList.txt\n";
      } else {
	if (! (fitsList >> fitsName)) {
	  logFile << "List is empty\n";
	  fitsList.close();
	  fitsName.clear();	// Make sure its empty
	} 
      }
    }
    if (! fitsName.empty()) {
      if (access(fitsName.c_str(), R_OK)) {
	logFile << "fitsName is " << fitsName << "\n but it can't be read\n";
      } else {
	pixFITS = getFITSpix(fitsName, cols, rows);
      }
      // Get next name
      if (! (fitsList >> fitsName)) {
	logFile << "End of list\n";
	fitsList.clear();
	fitsList.close();
	checkList = true;	// Next call will reopen the list
	fitsName.clear();	// Make sure its empty
      } 
    }
    if (pixFITS != NULL) {
      return pixFITS;
    }
    // Generate a synthetic image instead
#endif // PLAYLIST

    signed short * pixels = new signed short[npix];

    double xctr = cols / 2 + .2;
    double yctr = rows / 2 + .7;
#ifdef NOISE
    mt19937 rng(static_cast<unsigned> (std::time(0)));
    normal_distribution<double> dist(5000, 45);
    variate_generator<mt19937, normal_distribution<double> > bias(rng, dist);

    normal_distribution<double> xPosDist(xctr, 10.);
    normal_distribution<double> yPosDist(yctr, 10.);

    variate_generator<mt19937,normal_distribution<double> > xpos(rng, xPosDist);
    variate_generator<mt19937,normal_distribution<double> > ypos(rng, yPosDist);

    xctr = xpos();
    yctr = ypos();

#endif //NOISE
    logFile << "placing artificial star at ("; 
    logFile << xctr + roi.colLo << "," << yctr + roi.rowLo << ")\n";

    double sigma = 3.0 / 2.35;

    // Compute the height of the gaussian, based on a total
    // flux of 4320 photons per square centimeter per millisecond 
    // for a zero-magnitude star (unfiltered).
    double flux0 = 500 / (2 * 3.14159 * sigma * sigma) * currentExptime;

    // Multiply by the aperture of the OPT in cm^2
    flux0 *= 113;

    // Adjust to make an eighth magnitude star.
    flux0 /= 1584.9;    // 10^(8/2.5)

    logFile << "Peak at " << flux0 << std::endl;

    for (int i = 0; i < npix; i++) {
        int x = i % cols;
        int y = i / cols;

#ifdef NOISE
        pixels[i] = bias();
#else
        pixels[i] = 5000;
#endif // NOISE

        double x0 = (double)x - xctr;
        double y0 = (double)y - yctr;

        if ((x0 < -20) || (y0 < -20) || (x0 > 20) || (y0 > 20)) continue;

        double flux = flux0 * exp(-(x0*x0 + y0*y0)/(2 * sigma * sigma)); 

#ifdef NOISE
        if (flux < 1) continue;

        // Strictly speaking, we should simulate shot noise with a Poisson
        // distribution. However, generating variates for a Poisson
        // distribution is a hassle for large lambda. The shot noise only
        // matters when lambda is large anyway, so I'm just going to use
        // a Gaussian to approximate the Poisson distribution.
        normal_distribution<double> shotDist(flux, sqrt(flux));
        variate_generator<mt19937, normal_distribution<double> > 
                      shot(rng, shotDist);

        pixels[i] += shot() / GAIN;
#else
        pixels[i] += flux / GAIN;
#endif // NOISE
      
    }

#ifdef MAKEHOTPIX
    // Simulate some hot pixels (absolute row & column)
    int hotcol[] = {350, 350, 351, 351, 210};
    int hotrow[] = {400, 401, 400, 401, 190};
    int numhot = sizeof(hotcol)/sizeof(hotcol[0]);
    int n;

    for (int i=0; i<numhot; i++) {
      n = (hotcol[i] - 1  - roi.colLo) + cols * (hotrow[i] - 1 - roi.rowLo);
      if (n >= 0 && n < npix)
	pixels[n] = 60000;
    }
#endif // MAKEHOTPIX

    return pixels;
}

void Camera::acquireCleanup()
{
    postExpose();

    if (sendTo) delete sendTo;
    sendTo = 0;

    // Wait for image processing to complete.
    Image::dispatch(0);

    Camera::setState(CAM_IDLE);
}

const int Camera::MAX_IMAGES = 150;
