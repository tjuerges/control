#ifndef REGIONOFINTEREST_H
#define REGIONOFINTEREST_H

/** Describes a region of interest to use in camera readout. The camera
 * reads out only data within the region of interest.
 */
typedef struct _region {
    int rowLo;   /**< The x-value pixel that forms the right edge of the ROI */
    int colLo;   /**< The y-value pixel that forms the bottom edge of the ROI */

    int rowHi;   /**< The x-value pixel that forms the left edge of the ROI */
    int colHi;   /**< The y-value pixel that forms the top edge of the ROI */
} RegionOfInterest;

#endif // !REGIONOFINTEREST_H
