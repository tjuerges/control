#include "Heater.h"
#include "Relay.h"
#include "Shutter.h"
#include "Opts.h"

Heater::Heater(Relay * r)
{
    state = OFF;
    relay = r;

    shutter = theOPTS.getShutter();

    r -> off();
}

Heater::State Heater::on()
{
    if (shutter -> getState() != Shutter::CLOSED) return OFF;

    relay -> on();
    state = ON;

    return ON;
}

Heater::State Heater::off()
{
    relay -> off();
    state = OFF;
}

Heater::State Heater::getState()
{
    return state;
}
