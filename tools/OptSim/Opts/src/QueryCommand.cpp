#include <sstream>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H

#include "Opts.h"
#include "QueryCommand.h"
#include "Camera.h"
#include "CalibrationImage.h"
#include "HotPixels.h"
#include "FilterWheel.h"
#include "Focuser.h"
#include "Relay.h"
#include "ThermalSensor.h"
#include "Shutter.h"
#include "SunSensor.h"
#include "CentroidFactory.h"
#include "Heater.h"
#include "DigitalInput.h"

QueryCommand::QueryCommand(const char * name)
{
    DataSequencer::getInstance() -> registerCommand(this, "Query", name);
    commandName = std::string(name);
}

bool QueryCommand::permission(MessageParty sender,
                              std::vector<std::string> command)
{
    return true;
}

QueryBias queryBias;
std::string QueryBias::execute(MessageParty sender, 
                               std::vector<std::string> command)
{
    CalibrationImage * bias = theOPTS.getCamera() -> getBias();
    if (bias == 0) return "Bias zeroed";

    std::stringstream ss;
    ss << "Bias " << bias -> getSequenceNumber();
    ss << " " << bias -> getImage() -> getDateObs();
    return ss.str();
}

QueryBlank queryBlank;
std::string QueryBlank::execute(MessageParty sender,
                                std::vector<std::string> command)
{
    CalibrationImage * blank = theOPTS.getCamera() -> getBlank();
    if (blank == 0) return "Blank zeroed";

    std::stringstream ss;
    ss << "Blank " << blank -> getSequenceNumber();
    ss << " " << blank -> getImage() -> getDateObs();
    return ss.str();
}

QueryCamPower queryCamPower;
std::string QueryCamPower::execute(MessageParty sender,
                                   std::vector<std::string> command)
{
    switch (theOPTS.getCamera() -> getState()) {
        case Camera::CAM_POWEROFF:
            return "CamPower Off";
        case Camera::CAM_IDLE:
        case Camera::CAM_EXPOSING:
            return "CamPower On";
        default:
            return "Error CamPower status is invalid. Reset system.";
    }
}


QueryCamStat queryCamStat;
std::string QueryCamStat::execute(MessageParty sender,
                                  std::vector<std::string> command)
{
    Camera::CameraState state = theOPTS.getCamera() -> getState();

    if (state == Camera::CAM_POWEROFF) {
        return "CamStat Power Off";
    }

    if (state == Camera::CAM_IDLE) {
        return "CamStat Idle";
    }

    std::stringstream ss;
    ss << "CamStat Exposing ";
    ss << 0.001 * theOPTS.getCamera() -> getEndTime();
    ss << " ";
    ss << theOPTS.getCamera() -> getExposuresRemaining(); 
    ss << " ";
    ss << theOPTS.getCamera() -> getExptime() / 1000.;

    return ss.str();
}


QueryCamTemp queryCamTemp;
std::string QueryCamTemp::execute(MessageParty sender,
                                  std::vector<std::string> command)
{
    double temp = theOPTS.getCamera() -> getCurrTemp();
    double targTemp = theOPTS.getCamera() -> getTargetTemp();

    // round temperatures to the nearest instead of down
    temp -= 0.5;
    targTemp -= 0.5;

    std::stringstream response;

    response << "CamTemp " << (int)temp << " " << (int)targTemp;

    return response.str();
}


QueryCentAlg queryCentAlg;
std::string QueryCentAlg::execute(MessageParty sender,
                                  std::vector<std::string> command)
{
    CentroidFactory * cf = CentroidFactory::get();
    std::stringstream ss;

    int nparam = cf -> getNumParams();

    ss << "CentAlg ";
    ss << CentroidFactory::get() -> getSelectedID();

    if (nparam > 0) {
        ss << " " << cf -> getParam1();
    }
    if (nparam > 1) {
        ss << " " << cf -> getParam2();
    }
    if (nparam > 2) {
        ss << " " << cf -> getParam3();
    }

    return ss.str();
}

QueryCenThresh queryCenThresh;
std::string QueryCenThresh::execute(MessageParty sender,
                                    std::vector<std::string> command)
{
    std::stringstream ss;
    ss << "CenThresh ";
    ss << CentroidFactory::get() -> getThresh();
    return ss.str();
}


QueryEngStatus queryEngStatus;
std::string QueryEngStatus::execute(MessageParty sender,
                                    std::vector<std::string> command)
{
    std::stringstream ss;
    ss << "EngStatus\n";
#ifdef PACKAGE_STRING
    ss << PACKAGE_STRING << std::endl;
#endif // PACKAGE_STRING
    theOPTS.galilEngStatus(ss);
    theOPTS.getCamera() -> getEngStatus(ss);
    logFile << ss.str() << std::endl;
    return ss.str();
}

QueryExpTime queryExpTime;
std::string QueryExpTime::execute(MessageParty sender,
                                  std::vector<std::string> command)
{
    std::stringstream response;
    int milli = theOPTS.getCamera() -> getDefaultExptime();
    response << "ExpTime " << (double)milli / 1000.;

    return response.str();
}


QueryFan queryFan;
std::string QueryFan::execute(MessageParty sender,
                              std::vector<std::string> command)
{
    Relay::State state = theOPTS.getFan() -> getState();
    std::stringstream response;
    response << "Fan ";

    switch (state) {
        case Relay::ON:
            response << "On";
            break;
        case Relay::OFF:
            response << "Off";
            break;
        case Relay::OVER:
        case Relay::UNDER:
        case Relay::UNKNOWN:
            response << "Unknown";
            break;
    }

    return response.str();
}

QueryFiltPos queryFiltPos;
std::string QueryFiltPos::execute(MessageParty sender,
                                  std::vector<std::string> command)
{
    std::stringstream ss;
    ss << "FiltPos ";
    FilterWheel::State state = theOPTS.getFilterWheel() -> getState();
    int filter;
    switch (state) {
        case FilterWheel::FW_FILTERED:
            ss << "at ";
            filter = theOPTS.getFilterWheel() -> getFilter();
            break;

        case FilterWheel::FW_BETWEEN:
            ss << "between";
            return ss.str();
            break;

        case FilterWheel::FW_MOVING:
            ss << "moving to ";
            filter = theOPTS.getFilterWheel() -> getTarget();
            break;

        case FilterWheel::FW_STUCK:
        case FilterWheel::FW_UNKNOWN:
            ss << "Stuck ";
            filter = theOPTS.getFilterWheel() -> getFilter();
            break;
    }

    if (filter == -1) ss << "Unknown";
    else ss << theOPTS.getFilterWheel() -> getFilterName(filter);
   
    return ss.str();
}


QueryFlatField queryFlatField;
std::string QueryFlatField::execute(MessageParty sender,
                                    std::vector<std::string> command)
{
    CalibrationImage * source;
    source = theOPTS.getCamera() -> getFlat();

    if (source == 0) return "FlatField None"; 

    switch (source -> getFlatSource()) {
        case (CalibrationImage::SRC_ALMA):
            return "FlatField ALMA";
            break;
        case (CalibrationImage::SRC_FACTORY):
            return "FlatField Factory";
            break;
        case (CalibrationImage::SRC_NONE):
        default:
            return "FlatField None";
            break;
    }

    return "FlatField None";
}


QueryFocusInit queryFocusInit;
std::string QueryFocusInit::execute(MessageParty sender,
                                    std::vector<std::string> command)
{
    std::stringstream ss;
    ss << "FocusInit ";
    ss << theOPTS.getFocuser() -> getZeroError();
    ss << " ";
    ss << theOPTS.getFocuser() -> getInitTime();
    return ss.str();
}


QueryFocusPos queryFocusPos;
std::string QueryFocusPos::execute(MessageParty sender,
                                   std::vector<std::string> command)
{
    std::stringstream ss;

    ss << "FocusPos ";
    ss << theOPTS.getFocuser() -> getPosition();
    Focuser::State state = theOPTS.getFocuser() -> getState();

    switch (state) {
        case Focuser::STOPPED:
            ss << " Stopped";
            break;

        case Focuser::MOVING_UP:
            ss << " Moving +";
            break;

        case Focuser::MOVING_DN:
            ss << " Moving -";
            break;

        case Focuser::LIMIT_UP:
        case Focuser::LIMIT_DN:
            ss << " Limit";
            break;

        case Focuser::STUCK:
        case Focuser::UNKNOWN:
            ss << " Unknown";
            break;
    }

    return ss.str();
}

QueryHotPix queryHotPix;
std::string QueryHotPix::execute(MessageParty sender,
                                 std::vector<std::string> command)
{
    int i, j;
    HotPixels * hotpix = theOPTS.getCamera() -> getHotPix();
    if (hotpix == 0) return "HotPix (none)";

    std::ostringstream ss;

    ss << "HotPix";

    std::vector<std::pair<int, int> > list = hotpix -> getPixels();
    for (i = 0; i < list.size(); i++) {
        ss << " " << list[i].first + 1 << "," << list[i].second + 1;
    }

    if (list.size() == 0) ss << " (none)";
    return ss.str();
}

QueryIDCamera queryIDCamera;
std::string QueryIDCamera::execute(MessageParty sender,
                                   std::vector<std::string> command)
{
    std::string response = "IDCamera ";
    response += theOPTS.getCamera() -> getName();

    return response;
}

QueryIDTCM queryIDTCM;
std::string QueryIDTCM::execute(MessageParty sender,
                                std::vector<std::string> command)
{
    std::string response = "IDTCM ";
    response += theOPTS.getID();

    return response;
}

QueryMasterMB queryMasterMB;
std::string QueryMasterMB::execute(MessageParty sender,
                                   std::vector<std::string> command)
{
    std::string response = "MasterMB ";
    std::string master = theOPTS.getInterface() -> getMaster();
    if (master == "") {
        response += "none";
    } else {
        response += master;
    }

    return response;
}


QueryPower queryPower;
std::string QueryPower::execute(MessageParty sender,
                                std::vector<std::string> command)
{
    Relay::State state = theOPTS.getPower() -> getState();

    switch (state) {
        case Relay::ON:
            return "Power OK";
            break;

        case Relay::OFF:
        case Relay::UNDER:
        case Relay::UNKNOWN:
            return "Power OFF";
            break;

        case Relay::OVER:
            return "Power OVER";
            break;
    }
}



QuerySeqNo querySeqNo;
std::string QuerySeqNo::execute(MessageParty sender,
                                std::vector<std::string> command)
{
    std::stringstream response;
    int last = theOPTS.getCamera() -> getLastSequenceNum();
    response << "SeqNo " << last;

    return response.str();
}


QueryShutter queryShutter;
std::string QueryShutter::execute(MessageParty sender,
                                  std::vector<std::string> command)
{
    Shutter::State state = theOPTS.getShutter() -> getState();    
    bool fault = theOPTS.getShutter() -> getError();

    std::stringstream ss;

    ss << "Shutter ";

    switch (state) {
        case (Shutter::OPEN):
            ss << "Open";
            break;
        case (Shutter::CLOSED):
            ss << "Closed";
            break;
        case (Shutter::OPENING):
            ss << "Opening";
            break;
        case (Shutter::CLOSING):
            ss << "Closing";
            break;
        case (Shutter::UNKNOWN):
            ss << "Unknown";
            break;
    }

    if (fault) ss << " Fault";

    return ss.str();
}


QueryStatus queryStatus;
std::string QueryStatus::execute(MessageParty sender,
                                   std::vector<std::string> command)
{
    if (command.size() > 3) return "Error Status syntax";

    bool all = false;
    std::string keyword;
    if (command.size() == 3) keyword = command[2];
    else all = true;

    std::stringstream ss;
    ss << "Status ";

    if (all || keyword == "CCD") {
        ss << "CCD ";
        if (theOPTS.getCamera() -> getError()) ss << "Bad";
        else ss << "OK";
        if (all) ss << "\n";
    }

    if (all || keyword == "CamTemp") {
        double temperature = theOPTS.getCamera() -> getCurrTemp();
        // TODO: check that temperature is in range.
        ss << "CamTemp ";

        if (temperature < -20) ss << "OK";
        else ss << temperature;

        if (all) ss << "\n";
    }

    if (all || keyword == "FiltPos") {
        if (theOPTS.getFilterWheel() -> getError()) ss << "FiltPos Bad";
        else ss << "FiltPos OK";
        if (all) ss << "\n";
    }

    if (all || keyword == "FocusPos") {
        double focus = theOPTS.getFocuser() -> getPosition();

        if (theOPTS.getFocuser() -> getError()) ss << "FocusPos Bad";
        else if ((focus < -15) || (focus > 25)) ss << "FocusPos Bad";
        else ss << "FocusPos OK";
        if (all) ss << "\n";
    }

    if (all || keyword == "Power") {
        // TODO: Check for overvoltage/undervoltage/whatever in the Galil
        ss << "Power OK";
        if (all) ss << "\n";
    }

    if (all || keyword == "Shutter") {
        if (theOPTS.getShutter() -> getError()) ss << "Shutter Bad";
        else ss << "Shutter OK";
        if (all) ss << "\n";
    }

    if (all || keyword == "SunSensor") {
        ss << "SunSensor ";

        if (theOPTS.getSunSensor1() -> getState()) ss << "Tripped ";
        else ss << "OK ";

        if (theOPTS.getSunSensor2() -> getState()) ss << "Tripped";
        else ss << "OK";

        if (all) ss << "\n";
    }

    if (all || keyword == "TelTemp") {
        double temperature = theOPTS.getTubeSensor() -> getTemperature(); 
        // TODO: Check that the tube temperature is in range.
        ss << "TelTemp ";
        if ((temperature > -20) && (temperature < 40)) ss << "OK";
        else ss << temperature;
    }

    return ss.str();
}

QuerySubImage querySubImage;
std::string QuerySubImage::execute(MessageParty sender,
                                   std::vector<std::string> command)
{
    RegionOfInterest roi = theOPTS.getCamera() -> getROI();
    RegionOfInterest whole = theOPTS.getCamera() -> getGeometry();

    if ((roi.colHi == whole.colHi) &&
        (roi.colLo == whole.colLo) &&
        (roi.rowHi == whole.rowHi) &&
        (roi.rowLo == whole.rowLo))
    {
        return "SubImage All";
    }

    std::stringstream response;
    response << "SubImage ";
    response << roi.colLo+1 << "," << roi.rowLo+1 << " ";
    response << roi.colHi+1 << "," << roi.rowHi+1;

    return response.str();
}


QuerySunSensor querySunSensor;
std::string QuerySunSensor::execute(MessageParty sender,
                                    std::vector<std::string> command)
{
    SunSensor * ss1 = theOPTS.getSunSensor1();
    SunSensor * ss2 = theOPTS.getSunSensor2();

    bool tripped1 = ss1 -> getState();
    bool tripped2 = ss2 -> getState();

    std::stringstream ss;
    ss << "SunSensor ";
    if (tripped1) ss << "Tripped ";
    else ss << "Ready ";

    if (tripped2) ss << "Tripped ";
    else ss << "Ready ";

    ss << ss1 -> getLevel() << " " << ss2 -> getLevel();

    if (theOPTS.getPcbSunSensor() -> check()) {
        ss << " Ready";
    } else {
        ss << " Tripped";
    }

    return ss.str();
}


QuerySunThresh querySunThresh;
std::string QuerySunThresh::execute(MessageParty sender,
                                    std::vector<std::string> command)
{
    std::stringstream ss;
    double thresh1 = theOPTS.getSunSensor1() -> getThreshold();
    double thresh2 = theOPTS.getSunSensor2() -> getThreshold();
    ss << "SunThresh " << thresh1 << " " << thresh2;
    return ss.str();
}

QueryTelHeater queryTelHeater;
std::string QueryTelHeater::execute(MessageParty sender,
                                    std::vector<std::string> command)
{
    Heater::State state = theOPTS.getHeater() -> getState();

    switch(state) {
        case Heater::ON:
            return "TelHeater On";
            break;

        case Heater::OFF:
        default:
            return "TelHeater Off";
            break;
    }
}


QuerySlew querySlew;
std::string QuerySlew::execute(MessageParty sender,
                               std::vector<std::string> command)
{
    if (theOPTS.getSlewing()) return "Slew Yes";
    else return "Slew No";
}

QueryTelTemp queryTelTemp;
std::string QueryTelTemp::execute(MessageParty sender,
                                  std::vector<std::string> command)
{
    std::stringstream ss;

    ss << std::setprecision(0);
    ss << std::fixed;
    ss << "TelTemp ";

    ss << theOPTS.getTubeSensor() -> getTemperature(); 
    ss << " ";
    ss << theOPTS.getFilterSensor() -> getTemperature(); 

    return ss.str();
}

QueryAll queryAll;
std::string QueryAll::execute(MessageParty sender,
                              std::vector<std::string> command)
{
    std::stringstream ss;
    ss << queryBias.execute(sender, command) << std::endl;
    ss << queryBlank.execute(sender, command) << std::endl;
    ss << queryCamPower.execute(sender, command) << std::endl;
    ss << queryCamStat.execute(sender, command) << std::endl;
    ss << queryCamTemp.execute(sender, command) << std::endl; 
    ss << queryCentAlg.execute(sender, command) << std::endl; 
    ss << queryCenThresh.execute(sender, command) << std::endl; 
    ss << queryExpTime.execute(sender, command) << std::endl; 
    ss << queryFan.execute(sender, command) << std::endl;
    ss << queryFiltPos.execute(sender, command) << std::endl;
    ss << queryFlatField.execute(sender, command) << std::endl;
    ss << queryFocusInit.execute(sender, command) << std::endl;
    ss << queryFocusPos.execute(sender, command) << std::endl;
    ss << queryHotPix.execute(sender, command) << std::endl;
    ss << queryIDCamera.execute(sender, command) << std::endl; 
    ss << queryIDTCM.execute(sender, command) << std::endl; 
    ss << queryMasterMB.execute(sender, command) << std::endl; 
    ss << queryPower.execute(sender, command) << std::endl;
    ss << querySeqNo.execute(sender, command) << std::endl; 
    ss << queryShutter.execute(sender, command) << std::endl;
    ss << querySlew.execute(sender, command) << std::endl;
    ss << querySubImage.execute(sender, command) << std::endl;
    ss << querySunSensor.execute(sender, command) << std::endl;
    ss << querySunThresh.execute(sender, command) << std::endl;
    ss << queryTelHeater.execute(sender, command) << std::endl;
    ss << queryTelTemp.execute(sender, command);

    return ss.str();
}
