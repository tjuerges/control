#include "GetCommand.h"

#include "Opts.h"
#include "Image.h"
#include "CalibrationImage.h"
#include "Centroid.h"
#include "Camera.h"

#include <boost/thread/thread.hpp>
#include <boost/bind.hpp>

GetCommand::GetCommand(const char * name)
{
    DataSequencer::getInstance() -> registerCommand(this, "Get", name);
    commandName = std::string(name);
}

bool GetCommand::permission(MessageParty sender,
                            std::vector<std::string> command)
{
    return true;
}

void GetCommand::beQuietPlease(MessageParty forMe)
{
    politeRequests[forMe.getHandle()] = true;
}

bool GetCommand::checkQuiet(void * handle)
{
    return politeRequests[handle];
}

void GetCommand::clearRequest(MessageParty forMe)
{
    politeRequests[forMe.getHandle()] = false;
}

std::map<void *, bool> GetCommand::politeRequests;

GetCentroid getCentroid;
std::string GetCentroid::execute(MessageParty sender,
                                 std::vector<std::string> command)
{
    return parseGetCommand(sender, command);
}

GetImage getImage;
std::string GetImage::execute(MessageParty sender,
                              std::vector<std::string> command)
{
    if (command.size() == 3) {
        if (command[2] == "Bias") {
            CalibrationImage * bias = theOPTS.getCamera() -> getBias();
            if (bias == 0) return "Error Image bias not set";
            Image * img = bias -> getImage();
            if (img == 0) return "Error Image bias not set";
            theOPTS.getInterface() -> sendImage(sender, img);
            return "";
        }
        if (command[2] == "Blank") {
            CalibrationImage * blank = theOPTS.getCamera() -> getBlank();
            if (blank == 0) return "Error Image blank not set";
            Image * img = blank -> getImage();
            if (img == 0) return "Error Image blank not set";
            theOPTS.getInterface() -> sendImage(sender, img);
            return "";
        }
    }

    return parseGetCommand(sender, command);
}

GetImageCen getImageCen;
std::string GetImageCen::execute(MessageParty sender,
                                 std::vector<std::string> command)
{
    return parseGetCommand(sender, command);
}

std::string parseGetCommand(MessageParty& sender, 
                            std::vector<std::string>& command)
{
    int first, last;

    if (command.size() > 4) {
        return "Error " + command[1] + " syntax";
    }

    if (command.size() == 2) {
        first = theOPTS.getCamera() -> getLastSequenceNum();
    }

    if (command.size() > 2) {
        std::stringstream ss(command[2]);
        ss >> first;
        if (ss.fail()) {
            return "Error " + command[1] + " no such image " + command[2];
        }
    }

    if (command.size() == 4) {
        std::stringstream ss(command[3]);
        ss >> last;
        if (ss.fail()) { 
            return "Error " + command[1] + " no such image " + command[3];
        }
    } else {
        last = first;
    }

    // Since we might be waiting for new images to finish, we'll break off
    // a new thread to handle this.

    int sendWhat = 0;
    if (command[1] == "Centroid") sendWhat = 1;
    if (command[1] == "Image") sendWhat = 2;
    if (command[1] == "ImageCen") sendWhat = 3;

    // This sender has not yet politely asked the Get command
    // to shut up. 
    GetCommand::clearRequest(sender);

    boost::thread::thread thread(boost::bind(getCommandSend, sender,
                                             first, last, sendWhat)); 
    return ""; 
}

void getCommandSend(MessageParty& sender, int first, int last, int sendWhat)
{
    Camera * cam = theOPTS.getCamera();

    void * handle = sender.getHandle();

    for (int i = first; i <= last; i++) {
        Camera::CameraState camstate = cam -> getState();
        Image * img = cam -> getImage(i);
        int nextNum = cam -> getNextSequenceNum();
        int howMany = cam -> getExposuresRemaining(); 

        if (img == 0) {
            if ((camstate == Camera::CAM_EXPOSING) && 
                (nextNum + howMany >= i)) 
            {
                // If we are going to expose the image soon, wait for it.
                while ((img = cam -> getImage(i)) == 0) boost::thread::yield();

            } else {
                // If the image doesn't exist, send an error message.
                std::stringstream ss;
                ss << "Error ";
                if (sendWhat == 1) ss << "Centroid";
                if (sendWhat == 2) ss << "Image";
                if (sendWhat == 3) ss << "ImageCen";
                ss << " no such image " << i << std::ends;

                theOPTS.getInterface() -> send(sender, ss.str());
                continue;
            }
        }

        if (img -> getSequenceNumber() != i) {
            std::stringstream ss;
            ss << "Error ";
            if (sendWhat == 1) ss << "Centroid";
            if (sendWhat == 2) ss << "Image";
            if (sendWhat == 3) ss << "ImageCen";
            ss << " image " << i << " is no longer available.";

            theOPTS.getInterface() -> send(sender, ss.str());

            continue;
        }

        // Wait for the centroid calculation to complete.
        Image::State state = img -> getState();
        while ((state != Image::WRITING) && 
               (state != Image::READY) &&
               (state != Image::ABORTED)) 
        {
            state = img -> getState();


            if (GetCommand::checkQuiet(handle)) {
                return;
            }

            boost::thread::yield();
        }

        if (state == Image::ABORTED) {
            std::stringstream ss;
            ss << "Error ";
            if (sendWhat == 1) ss << "Centroid";
            if (sendWhat == 2) ss << "Image";
            if (sendWhat == 3) ss << "ImageCen";
            ss << " image " << i << " was aborted.";
            theOPTS.getInterface() -> send(sender, ss.str());

            continue;
        }

        if (sendWhat & 1) {
            // Compose a centroid report to send.
            std::stringstream ss;
            ss << "Centroid " << i << "\n";
            ss << "Peak " << img -> getCentroid() -> getPeak() << "\n";
            ss << "CentX " << img -> getCentroid() -> getX() + 1 << "\n";
            ss << "CentY " << img -> getCentroid() -> getY() + 1 << "\n";
            ss << "WidthX " << img -> getCentroid() -> getFwhmX() << "\n";
            ss << "WidthY " << img -> getCentroid() -> getFwhmY() << "\n";
            ss << "RMS " << img -> getCentroid() -> getRMS() << "\n";
            ss << "Valid " << img -> getCentroid() -> getValid() << "\n";
            ss << "ExpTime " << img -> getExpTime() << "\n";
            ss << "TimeStamp " << img -> getDateObs() << "\n";
            ss << std::ends;

            theOPTS.getInterface() -> send(sender, ss.str());
        }

        if (sendWhat & 2) {

            // Wait for writing to finish.
            while (state != Image::READY) {
                state = img -> getState();

                if (GetCommand::checkQuiet(handle)) {
                    return;
                }

                boost::thread::yield();
            }

            // Send off the file.
            theOPTS.getInterface() -> sendImage(sender, img);
        }
    }
}
