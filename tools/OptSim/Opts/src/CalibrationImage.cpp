#if HAVE_CONFIG_H
#include <config.h>
#endif

#if HAVE_CFITSIO_FITSIO_H
#include <cfitsio/fitsio.h>
#else
#include <fitsio.h>
#endif

#include <limits>

#include "Camera.h"
#include "Image.h"
#include "CalibrationImage.h"

CalibrationImage::CalibrationImage(Image& img) :
dateObs(img.getDateObs())
{
    roi = img.getROI();
    int i, j;
    int columns = roi.colHi - roi.colLo + 1;

    setSequenceNumber(img.getSequenceNumber());
    setImage(img);

    signed short * ptr = img.getBits();
    double val = std::numeric_limits<double>::quiet_NaN();

    for (i = 0; i < roi.rowLo; i++) {
        for (j = 0; j < calibration_image_y; j++) {
            pix[j][i] = val;
        }
    }

    for (i = roi.rowLo; i <= roi.rowHi; i++) {
        for (j = 0; j < roi.colLo; j++) {
            pix[j][i] = val;
        }

        for (j = roi.colLo; j <= roi.colHi; j++) {
            pix[j][i] = *ptr++;//ptr[(i - roi.rowLo) * columns +
                        //    (j - roi.colLo)];
        }

        for (j = roi.colHi + 1; j < calibration_image_y; j++) {
            pix[j][i] = val;
        }
    } 

    for (i = roi.rowHi + 1; i < calibration_image_x; i++) {
        for (j = 0; j < calibration_image_x; j++) {
            pix[j][i] = val;
        }
    }
}

CalibrationImage::CalibrationImage(double val)
{
    sequenceNum = 0;
    numImages = 0;

    roi.colLo = roi.rowLo = 0;
    roi.colHi = calibration_image_x;
    roi.rowHi = calibration_image_y;

    int i,j;
    for (i = 0; i < calibration_image_x; i++) {
        for (j = 0; j < calibration_image_y; j++) {
            pix[j][i] = val;
        }
    }
}

void CalibrationImage::setSequenceNumber(int num)
{
    sequenceNum = num;
}

int CalibrationImage::getSequenceNumber()
{
    return sequenceNum;
}

int CalibrationImage::getNumComponents()
{
    return numImages;
}

void CalibrationImage::normalize()
{
    int i, j; 
    double sum = 0;

    // Loop over the image to compute the sum.
    for (i = 0; i < calibration_image_x; i++) {
        for (j = 0; j < calibration_image_y; j++) {
            if (pix[j][i] == pix[j][i]) {
                sum += pix[j][i];
            }
        }
    }

    // Convert the sum into the mean.
    sum /= calibration_image_x * calibration_image_y;

    std::cout << "Flat field has average " << sum << std::endl;

    // Divide through by the mean.
    for (i = 0; i < calibration_image_x; i++) {
        for (j = 0; j < calibration_image_y; j++) {
            pix[j][i] /=  sum;
            if (pix[j][i] < 0.001) pix[j][i] = 1.e9;
        }
    }
}

void CalibrationImage::combine(CalibrationImage& src)
{
    int n = src.getNumComponents();

    for (int i = 0; i < calibration_image_x; i++) {
        for (int j = 0; j < calibration_image_y; j++) {
            pix[j][i] = pix[i][j] * numImages + src.pix[i][j] * n;
            pix[j][i] /= numImages + n;
        }
    }
}
