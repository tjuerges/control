#include <boost/thread.hpp>

#include <iostream>
#include <string>
#include <vector>
#include "Opts.h"
#include "DataSequencer.h"
#include "ClientInterface.h"

DataSequencer * DataSequencer::instance = 0;

DataSequencer * DataSequencer::getInstance()
{
    if (instance == 0) instance = new DataSequencer();
    return instance;
}

DataSequencer::DataSequencer()
{
    // Create the available directives.
    commands["Set"];
    commands["Get"];
    commands["Do"];
    commands["Query"];
}

void DataSequencer::listen()
{
    Message mesg;
    std::string response;
    while (true) {
        mesg = theOPTS.getInterface() -> receive();
        executeCommand(*(mesg.sender), mesg.command);
        delete mesg.sender;
    }
}

void DataSequencer::registerCommand(Command* command, 
                                    const std::string directive, 
                                    const std::string name)
{
 
    // Check that the new command does not already exist in the catalog.
    if (commands[directive].count(name)) {
        logFile << "attempt to register a pre-existing command name";
        logFile << std::endl;
    }

    // Add the command to the catalog.
    commands[directive][name] = command;
}

void DataSequencer::executeCommand(MessageParty sender, std::string command)
{
    // Tokenize the command.
    std::vector<std::string> tokens;
    std::vector<size_t> tokenPositions;
    size_t tokenBegin = 0;
    while (true) {
        // Parse through the whitespace to find each token.
        tokenBegin = command.find_first_not_of(" \t\n\r\v\f", tokenBegin);
        if (tokenBegin == std::string::npos) break;
        size_t tokenEnd = command.find_first_of(" \t\n\r\v\f", tokenBegin);
        
        tokenPositions.push_back(tokenBegin);
        tokens.push_back(command.substr(tokenBegin, tokenEnd - tokenBegin));

        tokenBegin = tokenEnd;
        if (tokenEnd == std::string::npos) break;        
    }

    if (tokens.size() == 0) return;
    if (tokens.size() == 1) {
        // Special case: "Query" is the same as "Query All".
        if (tokens[0] == "Query") { 
           std::string response;
           response = commands["Query"]["All"] -> execute(sender, tokens);
           theOPTS.getInterface() -> send(sender, response);
           return;
        }

        theOPTS.getInterface() -> send(sender, "Error bad command: " + 
            tokens[0]);

        return;
    }

    // Validate the directive in the command.
    if (commands.count(tokens[0]) > 0) {
		std::map<const std::string, Command*> commandSubset;
        commandSubset = commands[tokens[0]];
        
        // Validate the identifier.
        if (commandSubset.count(tokens[1]) > 0) {
            if (commandSubset[tokens[1]] -> permission(sender, tokens)) 
            {
                std::string response;
                response = commandSubset[tokens[1]] -> execute(sender, tokens);
                theOPTS.getInterface() -> send(sender, response);
            } else {
                // Error rejected Set/Do/whatever
                theOPTS.getInterface() -> send(sender, "Error rejected " +
                    tokens[0] + ": " + command.substr(tokenPositions[1]));
                                                       
            }
        } else {
            // Error bad Set/Do/whatever
            theOPTS.getInterface() -> send(sender, "Error bad " +
                tokens[0] + ": " + command.substr(tokenPositions[1]));
        }
    } else {
        theOPTS.getInterface() -> send(sender, "Error bad command: " +
                                               command);
    }
}

