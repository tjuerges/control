#include "GalilDigitalInput.h"
#include "Opts.h"
#include <Galil.h>
#include <sstream>
#include <boost/date_time/posix_time/posix_time.hpp>

GalilDigitalInput::GalilDigitalInput(Galil * board, int n) :
DigitalInput(),
galil(board),
inputNumber(n)
{
    using namespace boost::posix_time;
    cacheTime = microsec_clock::universal_time() - seconds(1);
}

bool GalilDigitalInput::check()
{
    using namespace boost::posix_time;
    ptime t(microsec_clock::universal_time());

    if (t < cacheTime + boost::posix_time::seconds(0.5)) 
    {
        return cached;   
    }

    cacheTime = t;

    std::stringstream ss;
    ss << "MG @IN[" << inputNumber << "]";
    try {
        std::string result = galil -> command(ss.str());

        double state;
        ss.str(result);
        ss >> state;
        if (state > 0.5) cached = true;
        else cached = false;
    } catch (std::string ex) {
        logFile << "Exception: " << ex << std::endl;
    }

    return cached;
}
