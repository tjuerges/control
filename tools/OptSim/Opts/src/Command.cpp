#include "Command.h"
#include "Opts.h"

bool Command::checkNotify(MessageParty& sender,
                          std::vector<std::string>& command)
{
    unsigned last = command.size() - 1;

    bool notify = false;
    if ((command[last] == "/Notify") || (command[last] == "/N")) {
        command.pop_back();
        notify = true;
    }

    return notify;
}

void Command::doNotify(bool notify, MessageParty& sender)
{
    if (notify) {
        theOPTS.getInterface() -> send(sender, commandName + " Done");
    }
}
