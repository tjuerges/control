#ifndef RELAY_H
#define RELAY_H

/** A relay or switch that turns an instrument on or off. This class controls,
 * for instance, the shutter motor and clutch, and the master power supply.
 */
class Relay {
    public:
        /** A description of the state of the relay. Some of these states
         * may not be used, depending on the functionality of the hardware.
         */
        enum State {
            ON,      /**< The relay is switched on. */
            OFF,     /**< The relay is switched off. */
            OVER,    /**< An overvoltage was detected. Power should be reset.*/
            UNDER,   /**< An undervoltage was detected. */
            UNKNOWN  /**< The state of the relay could not be determined. */
        };

        /** Constructor. Communication with the relay should be initialized
         * here, possibly by a call to init().
         */
        Relay() : state(OFF) {}

        /** Constructor. Communication with the relay should be initialized
         * here, possibly by a call to init().
         * @param s the initial state to place the relay in.
         */
        Relay(State s) : state(s) {} 

        /** Destructor. */
        virtual ~Relay() {}

        /** Switches the relay to the on position. */
        virtual void on() { state = Relay::ON; }

        /** Switches the relay to the off position. */
        virtual void off() { state = Relay::OFF; }

        /** Checks the current state of the relay. 
         * @returns the state of the relay.
         */
        virtual State getState() { return state; }

    protected:
        /** The current state of the relay (on or off) */
        State state;
}; 

#endif // !RELAY_H
