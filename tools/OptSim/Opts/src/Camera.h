/*
 *  Camera.h
 *
 */

#ifndef CAMERA_H
#define CAMERA_H
 
#include <string>
#include <map>

#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "Image.h"

class Image;
class CalibrationImage;

#include "RegionOfInterest.h"

/** Generic CCD Camera.
 *
 * This class contains generic code for controlling a 
 * CCD camera. All work done in this class is general 
 * to all CCD cameras. Code for communicating with and 
 * controlling a specific camera (such as the PhotonMax 
 * in this case) will go in a derived class.
 */
class Camera {
    public:
        /** Describes the power status of the camera. */
        enum CameraState { 
            CAM_POWEROFF,   /**< Camera is powered down or not initialized. */
            CAM_IDLE,       /**< Camera is powered up but is idle. */
            CAM_EXPOSING,   /**< Camera is exposing. */
            CAM_COMPLETE,   /**< Camera has finished exposing, but the results 
                             * are still being processed.
                             */
            CAM_FAILED,     /**< A previous exposure sequence was not completed
                             * successfully.
                             */
            CAM_BUSY        /**< Camera is not available for new exposures. 
                             * The camera state will never be set to this. If
                             * the camera is busy, it will have state
                             * CAM_EXPOSING or CAM_COMPLETE.
                             */
        };

        /** Types of images. */
        enum ImageType { 
            IMG_LIGHT,      /**< Image of stars or other science target. */
            IMG_BIAS,       /**< Bias frame or master. */
            IMG_FLAT        /**< Flat frame or master. */
        };

        /** The maximum number of images the OPTS will hold at a given time.
         * After this number is exceeded, the exposure counter will roll over
         * and begin at 1 again.
         */
        static const int MAX_IMAGES;

        signed short ** imageBits;

        /** Constructor.
         * This should leave the class in a valid state for imaging.
         */
        Camera();

        /** Destructor. The destructor should be virtual, since this
         * is an abstract class and therefore has virtual methods.
         */
        virtual ~Camera() {};

        /** Gets the name of this camera. */
        virtual std::string getName();

        /** Sets the name of the camera. Should this maybe be protected? 
         *
         * @param newName the new name to give the camera.
         */
        void setName(std::string newName);
        
        /** Gets the current power state of the camera. */
        virtual CameraState getState();

        /** Sets the power state of the camera. */
        virtual void setState(CameraState newState);

        /** Provides engineering diagnostic information about the camera.
         * The diagnostic information is written into the provided stream.
         *
         * @param ss A stream into which to write the diagnostic information.
         */
        virtual void getEngStatus(std::stringstream& ss) {ss << "Camera: OK\n";}

        /* Gets the image identified by the given sequence number.
         *
         *  @param seq The image sequence number to get.
         */
        virtual Image * getImage(int seq);

        /** Specify the number of exposures remaining
         * in the exposure sequence currently in progress.
         *
         * Setting this to zero will end the current sequence
         * after the current exposure finishes.
         *
         * @param remain How many exposures to take after the current one 
         *        finishes.
         */
        virtual void setExposuresRemaining(int remain);

        /** Returns the number of exposures remaining in this sequence. */
        virtual int getExposuresRemaining();

        /** Sets the exposure number that will be assigned
         * to the next image to be read out. 
         * 
         * If an image is in progress when this method is called, that
         * image will be assigned an exposure number of <em>next</em>.
         *
         * @param next The exposure number for the next image.
         */
        virtual void setNextSequenceNum(const int next);

        /** Gets the exposure number that will be assigned to the next image. 
         * Unless otherwise specified, this is the last exposure number plus
         * one, or zero if no exposures have been taken.
         */
        virtual int getNextSequenceNum();

        /** Gets the exposure number that was assigned to the most 
         * recent image. 
         */
        virtual int getLastSequenceNum();

        /** @returns the gain setting of the camera.
         */
        virtual double getGain() { return gain; }
        virtual void setGain(double g) { gain = g; }

        /** Gets the current camera temperature. */
        virtual double getCurrTemp();

        /** Gets the camera's thermostat setting. */
        virtual double getTargetTemp();

        /** Adjusts the camera's thermostat to a new target temperature.
         *
         * @param temp The new temperature setting.
         */
        virtual void setTargetTemp(double temp);

        /* Sets the exposure time to be used in the next sequence. This
         * is not the same as the default exposure time, which will
         * apply to all future sequences unless setExptime() is called
         * before the sequence. 
         *
         * @param milliseconds The exposure time to use in milliseconds.
         */
        virtual void setExptime(int milliseconds);

        /** Sets the image type of future images to be exposed.
         *
         * @param type the type of images to be taken with the camera.
         */
        void setType(Image::ImageType type) { currentType = type; }

        /** @returns the image type that will be assigned to any images
         * exposed with the camera. This image type will apply until
         * it is changed with setType().
         */
        Image::ImageType getType() { return currentType; }

        /// Gets the exposure time to be used in the next sequence.
        virtual int getExptime();

        /** Sets the default exposure time.
         * The default exposure time is used when the user does
         * not explicitly specify an exposure time.
         *
         * @param milliseconds The new default exposure in milliseconds.
         */
        virtual void setDefaultExptime(int milliseconds);

        /// Get the default exposure time.
        virtual int getDefaultExptime();

        /** Set the time at which the current exposure series should end.
         *
         * @param milliseconds The time until series end, in milliseconds.
         */
        void setEndTime(int milliseconds);

        /** Sets the time at which the current exposure should end.
         * 
         * @param milliseconds The time until exposure end, in milliseconds.
         */
        void setExpEndTime(int milliseconds);

        /** Get the time until the end of the exposure sequence.
         *
         * @returns the number of milliseconds until the end of the sequence.
         */
        int getEndTime();

        /** Apply a new region of interest.
         *
         * The specified region of interest will be passed to the
         * camera in future exposures. Only this region will be
         * read out from the camera. This setting applies until
         * changed by another call to setROI.
         *
         * @param newROI The new region of interest to use.
         */
        virtual void setROI(RegionOfInterest newROI);

        /// Get the current region of interest.
        virtual RegionOfInterest getROI();

        /** Get a description of the CCD geometry.
         *
         * @returns a description of the CCD in the form of a RegionOfInterest.
         */
        virtual RegionOfInterest getGeometry();


        /// Start exposing a new sequence.
        virtual CameraState expose(int howMany, MessageParty * recipient);


        /// Begin acquiring the images.
        virtual void acquire();

        /// Abort the current exposure sequence.
        virtual void abort();

        /** Set the abort status for the camera.
         * Setting the abort state to true will cause any in-progress 
         * exposure sequence to stop. The abort request will be acknowledged
         * by returning the abort state to false.
         *
         * @param abort The new abort state.
         */
        virtual void setAbortState(bool abort);

        /** Has the user requested abort?
         *
         * @returns true if the user has requested abort.
         */
        virtual bool getAbortState();

        /** Removes the existing bias and establishes a new one to use. 
         *
         * @param b the new bias image.
         */
        virtual void setBias(CalibrationImage * b);

        /** Exposes and records a single bias frame. */
        virtual bool takeBias();

        /** Exposes and records a single blank frame. */
        virtual bool takeBlank();

        /// Blocks until the current exposure sequence is finished.
        virtual void wait();

        /** Gets the master bias frame currently in effect. The image returned
         * by this function will be subtracted from all images taken.
         *
         * @returns a CalibrationImage representing the bias frame.
         */
        CalibrationImage * getBias();

        /** Sets a new blank to be used. The blank image, also known as a
         * dark, is an image taken with minimal light reaching the camera.
         * For the OPT, a blank is taken while the telescope is pointed at
         * blank sky. If a blank image is specified using setBlank(), this
         * image will be subtracted from all future exposures.
         */
        void setBlank(CalibrationImage * b);

        /** Gets the master blank (dark) image currently in effect.
         * @returns a CalibrationImage representing the dark image.
         */
        CalibrationImage * getBlank();

        /** Gets the master flat currently in effect.
         * @returns a CalibrationImage representing the flat field.
         */
        CalibrationImage * getFlat();

        /** Sets the master flat that will be applied to future images.
         * @param f the new flat image.
         */
        void setFlat(CalibrationImage * f);
        
        /** Gets the hot pixel mask currently in effect.
         * @returns a CalibrationImage representing the hot pixel mask.
         */
        HotPixels * getHotPix();

        /** Sets the hot pixels mask that should be applied to future images.
         * This hot pixel mask will specify which images should be ignored in
         * calculating the image centroid.
         */
        void setHotPix(HotPixels * hot) { hotpix = hot; }

        /** Indicates whether an error has occurred in the camera driver.
         * @returns true if an error occurred, or false otherwise.
         */
        bool getError() { return error; }

        /** Causes the camera to use video exposure mode for the next
         * exposure sequence. In video mode, the camera takes exposures
         * repeatedly, sending each one back to the observer.
         */
        void setVideo(bool vid = true) { video = vid; }

    protected:
        /** Prepares the camera to expose an image.
         */
        virtual bool preExpose() { return true; }

        /** Sends a command to the camera to start the exposure that
         * was requested.
         */
        virtual bool startExposure(short signed int *); // { return true; }

        /** Checks whether a requested exposure sequence is still in progress.
         */
        virtual CameraState checkStatus();

        /** Sends a command to the camera to abort an exposure sequence that is
         * in progress.
         */
        virtual void sendAbort() {}

        /** Obtains a pointer to the image that the camera has just exposed.
         */
        virtual signed short * getPixels();

        /** Cleans up resources used during an exposure sequence, and signals
         * to the camera the exposure sequence is finished.
         */
        virtual bool postExpose() { return true; }

        /** Associates an Image object with an exposure number.
         *
         * Future calls to getImage with the specified sequence
         * number will return img.
         * 
         * @param img The image to add to the image catalog.
         * @param seq The sequence number to be used with this image.
         */
        virtual void setImage(Image * img, int seq);

        /** Decrement the number of exposures remaining. */
        virtual void decrExposuresRemaining();

        /** Increment the exposure number. */
        virtual void incrementSequenceNum();

        /** Specifies the exposure number assigned to the 
         * last image taken.  Used after an exposure is taken.
         *
         * @param last The last exposure number.
         */
        virtual void setLastSequenceNum(const int last);

        /** Specifies the geometry of the CCD. 
         *
         * @param geom This ROI will be used to represent the CCD geometry.
         */
        virtual void setGeometry(RegionOfInterest geom);

        /** Constructs a new image object and fills it with the camera's status
         * information.
         */
        Image * setupImage();

        /** Camera's most recent temperature. */
        double currentTemperature;

        /** Signals that an error has occurred in the camera driver.
         */
        void setError() { error = true; }

        /** Removes an error condition that was signaled earlier using
         * setError().
         */
        void clearError() { error = false; }

    private:
        void acquireCleanup();

        /** A name that identifies this camera. */
        std::string name;
        /** The camera's current power state. */
        CameraState state;

        /** The gain of this camera. */
        double gain;

        /** The image that is currently in-progress, if there is one. */
//        Image * currentlyExposing;

        /** The master bias image. */
        CalibrationImage * bias;

        /** The master blank (dark) image. */
        CalibrationImage * blank;

        /** The master flat image. */
        CalibrationImage * flat;

        /** A hot pixel mask. */
        HotPixels * hotpix;

        /** Images from this camera that have been stored. */
        std::map<int, Image *> images;

        /** Number of exposures left to do in this series, 
         * after the current one ends.
         */
        int exposuresRemaining;
        /** Exposure number for the next exposure to be read out. */
        int nextSequenceNumber;
        /** Exposure number for the most recently read out exposure. */
        int lastSequenceNumber;    

        /** Camera's cooler setting. */
        double targetTemperature;

        /** Default exposure time in milliseconds. */
        int defaultExptime;
        /** Currently in-use exposure time in milliseconds. */
        int currentExptime;
        /** Exposure type for the next exposure. 
         */
        Image::ImageType currentType;
                

        /** Time at which the current exposure should end. */
        boost::posix_time::ptime expEndTime;

        /** Time at which the current exposure series should end. */
        boost::posix_time::ptime seqEndTime;

        /** The region of the CCD currently being used for imaging. */
        RegionOfInterest roi;
        /** A ROI describing the entire CCD. */
        RegionOfInterest wholeCCD;

        /** If true, the user has requested an abort. */
        bool aborted;

        /** If true, some kind of error has occurred. For example, this
         * might be set after a communication problem with the camera.
         */
        bool error;

        /** The thread that controls the camera's activity.
         */
        boost::thread * thread;

        /** A message party who is to receive copies of images created by
         * the camera.
         */
        MessageParty * sendTo;

        /** If true, the camera should expose in video mode. */
        bool video;
};

#endif // ! CAMERA_H
