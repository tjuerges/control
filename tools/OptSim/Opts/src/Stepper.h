#ifndef STEPPER_H
#define STEPPER_H

#include <boost/thread/mutex.hpp>

/** The Stepper class represents a stepper motor. There should be exactly one
 * stepper motor object per stepper motor in the system, but multiple stepper
 * motors may be controlled by a single control board. This class includes
 * a mutex object for thread safety. 
 *
 * The state of a stepper motor must not be changed by accessing the Stepper
 * class directly. Instead, a StepperControl object must be instantiated,
 * which can be used to cause the motor to move.
 *
 * @see StepperControl
 */ 
class Stepper {
    friend class StepperControl;
    public:
        /** Describes the state of the motor. */
        enum State {
            STOPPED,     /**< The motor is stopped. */
            LIMIT_UP,    /**< The motor is stopped at its upper limit. */
            LIMIT_DN,    /**< The motor is stopped at its lower limit. */
            MOVING_UP,   /**< The motor is moving up (increasing pos). */
            MOVING_DN,   /**< The motor is moving down (decreasing pos). */
            STUCK,       /**< The motor is stuck. */
            UNKNOWN      /**< The motor is improperly initialized. */
        };

        /** Constructor. A side effect of construction of a stepper
         * motor object should be to move the motor to its home position.
         */
        Stepper() : state(STOPPED), position(0), aborted(false)    { }

        /** Destructor. */
        virtual ~Stepper() {};

        void setSpeed(int sp) { speed = sp; }
        int getSpeed() { return speed; }

        void setAccel(int ac) { accel = ac; }
        int getAccel() { return accel; }
 
        void setDecel(int dc) { decel = dc; }
        int getDecel() { return decel; }

        /** Returns the state of the motor.
         * @returns a State representing the current state of the motor.
         */
        virtual Stepper::State getState() const            { return state; }

        virtual void on() {}
        virtual void off() {}

        /** Returns the current position of the motor.
         * @returns the current position of the motor.
         */
        virtual int getPosition()        { return position; }

        /** Defines the current position of the motor.
         * @param pos the current position of the motor.
         */
        virtual void setPosition(int pos) { position = pos; }

        /** Causes any in-progress commands to the stepper motor to stop
         * the motion they ordered. Returns immediately, without verifying
         * that the motion has stopped.
         */
        virtual void abort() { setAbortState(true); }

        virtual void setEncoderGear(double g) { encoderGear = g; }
        void setEncoderOffset(int o) { encoderOffset = o; }

    protected:
        /** Checks whether an abort of the stepper's motion has been requested.
         * @returns true if an abort has been requested.
         */
        bool getAbortState() { return aborted; }

        double getEncoderGear() { return encoderGear; }
        int getEncoderOffset() { return encoderOffset; }

        /** Requests an abort of any motion in the stepper. */
        void setAbortState(bool a) { aborted = a; }

        /** Sends the motor to the specified position.
         * This function does not return until the motor has completed
         * its slew.
         * @param pos position to move the motor to.
         */
        virtual void goTo(int pos)     { position = pos; }

        /** Manually adjusts the motor position, moving it by the specified
         * amount. This function does not return until the motor is finished
         * jogging.
         * @param steps number of steps to clock the motor through.
         */
        virtual void jog(int steps)    { position += steps; }

        /** Sends the motor to its home (zero) position.
         * This function blocks until the motor is finished moving.
         */
        virtual void goHome()     { homeOffset = position; position = 0; }

        /** Sends the motor forward or reverse in search of an encoder
         * index pulse. When the encoder pulse is emitted, the motor stops on
         * the edge of the index.
         * This function blocks until the motor is finished moving.
         *
         * @param direction If this parameter is 1, move the motor forward.
         *                  If the parameter is -1, move in reverse.
         */
        virtual void findIndex(int direction) {}

        /** Sets the state of the motor. */
        void setState(State s)    { state = s; }

        /** The motor's position reading when it was last initialized.
         * This can be used as an indicator of the variability in the
         * motor's home position.
         */
        int homeOffset;


        /** The current position of the motor. */
        int position;

        /** A mutex that guarantees that only one thread uses the motor
         * at any given time.
         */
        boost::mutex mutex;


    private:
        /** The current state of the motor. */
        State state;

        int speed;
        int accel;
        int decel;

        double encoderGear;
        int encoderOffset;

        /** True if an abort has been requested for the motor. */
        bool aborted;
};

/** An front-end interface to the Stepper class.
 * The StepperControl class provides access to the control functions
 * found in the stepper class. This wrapper-class provides built-in
 * thread safety transparently to the caller. It does this by using
 * a scoped mutex lock, which automatically locks the stepper motor
 * on construction and unlocks it on destruction.
 */
class StepperControl {
    public:
        /** Constructor for the StepperControl object.
         * Because this function blocks until the motor is not in
         * use by any other thread, construction should be delayed
         * until the motor is actually needed, and the object should
         * be destroyed as soon as the caller is finished with the
         * motor.
         */
        StepperControl(Stepper * s) : step(s), lock(s -> mutex) {};


        void on()              { step -> on(); }
        void off()             { step -> off(); }

        /** Sends the motor to the specified position.
         * This function does not return until the motor has completed
         * its slew.
         * @param pos position to move the motor to.
         */
        void goTo(int pos)     { step -> goTo(pos); }

        /** Manually adjusts the motor position, moving it by the specified
         * amount. This function does not return until the motor is finished
         * jogging.
         * @param steps number of steps to clock the motor through.
         */
        void jog(int steps)    { step -> jog(steps); }

        /** Sends the motor to its home (zero) position.
         * This function blocks until the motor is finished moving.
         */
        void goHome()          { step -> goHome(); }

        /** Causes any in-progress commands to the stepper motor to stop
         * the motion they ordered and return immediately.
         */
        void abort()           { step -> abort(); }


        /** Moves the focuser in the specified direction, until an index
         * pulse is generated by the encoder.
         * @param direction 1 to move forward in search of an index pulse.
         *                  -1 to move in reverse in search of an index pulse.
         */
        void findIndex(int direction)   { step -> findIndex(direction); }

        /** Returns the current position of the motor.
         * @returns the current position of the motor.
         */
        int getPosition()      { return step -> getPosition(); }

        /** Defines the current positions of the motor.
         * @param pos the new position value to be associated with the motor's
         *            current position.
         */
        void setPosition(int pos) { step -> setPosition(pos); }

    private:
        /** The stepper motor provided by this StepperControl object. */
        Stepper * step;

        int speed;
        int accel;
        int decel;

        /** A lock on the stepper motor object, to prevent anyone else
         * from using it as long as this object has it.
         */
        boost::mutex::scoped_lock lock; 
};

#endif // !STEPPER_H
