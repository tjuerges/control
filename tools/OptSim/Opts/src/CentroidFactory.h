#ifndef CENTROIDFACTORY_H
#define CENTROIDFACTORY_H

class Centroid;

/** Contains the information needed to map algorithm IDs to Centroid class
 * objects. The user may choose from a selection of centroiding algorithms,
 * by issuing a command requesting the desired centroiding algorithm by its
 * ID number. New centroid objects should be created through the 
 * CentroidFactory, rather than instantiated directly.
 * 
 * Programmers who wish to add new centroid algorithms should register their
 * new algorithm in this class. All you should need to do is add a new line
 * mapping your new centroid to a class (derived from the Centroid class) in
 * the get() method.
 */
class CentroidFactory {
    public:
        /** Obtain an instance of the CentroidFactory singleton. */
        static CentroidFactory * get();

        /** Creates a new centroid object, using the current values for the
         * algorithm ID and parameters.
         */
        /** Constructs a new centroiding algorithm object with the selected
         * settings.
         *
         * @returns a new centroid computation object.
         */
        Centroid * getCentroid();

        /** @returns the ID of the centroiding algorithm to be used.
         */
        int getSelectedID() const { return algorithm; }

        /** Select a new algorithm ID to be used for future centroids.
         * @param id the new algorithm ID.
         */
        void select(int id);

        /** Sets the number of parameters that are valid for the Centroid
         * object. This is not used in the computation of the centroid, but
         * it specifies how many parameters should be displayed in response
         * to Query CentAlg.
         * @param n the number of parameters used to specify the centroid
         *          object.
         */
        void setNumParams(int n) { nparams = n; }

        /** Sets parameter 1 of the centroid. The meaning of this parameter is
         * determined by the centroid algorithm.
         * @param p1 parameter 1 for new centroid objects.
         */
        void setParam1(double p1) { param1 = p1; }

        /** Sets parameter 2 of the centroid. The meaning of this parameter is
         * determined by the centroid algorithm.
         * @param p2 parameter 2 for new centroid objects.
         */
        void setParam2(double p2) { param2 = p2; }

        /** Sets parameter 3 of the centroid. The meaning of this parameter is
         * determined by the centroid algorithm.
         * @param p3 parameter 3 for new centroid objects.
         */
        void setParam3(double p3) { param3 = p3; }

        /** Sets the acceptance signal-to-noise threshold for new centroids.
         * Calculated centroids that have a signal-to-noise ratio higher
         * than this will be considered "good".
         */
        void setThresh(double t) { thresh = t; }

        /** Returns the number of parameters that are valid for the Centroid
         * object. This is not used in the computation of the centroid, but
         * it specifies how many parameters should be displayed in response
         * to Query CentAlg.
         * @returns the number of parameters new centroid objects will have.
         */
        int getNumParams() const { return nparams; }

        /** Returns centroiding parameter 1.
         * @returns centroiding parameter 1.
         */
        double getParam1() const { return param1; }

        /** Returns centroiding parameter 2.
         * @returns centroiding parameter 2.
         */
        double getParam2() const { return param2; }

        /** Returns centroiding parameter 3.
         * @returns centroiding parameter 3.
         */
        double getParam3() const { return param3; }

        /** Returns the signal-to-noise threshold of new centroids. Centroid
         * calculations with signal-to-noise higher than this should be
         * considered valid.
         * @returns the threshold S/N of new centroids.
         */
        double getThresh() const { return thresh; }
        
    private:
        /** Constructor. The constructor is private, since the CentroidFactory
         * is a singleton.
         */
        CentroidFactory() : algorithm(1), param1(21.), param2(3), param3(0), nparams(2), thresh(10) {}

        /** Copy Constructor. The constructor is private, since the
         * CentroidFactory is a singleton.
         */
        CentroidFactory(CentroidFactory& from) {};

        /** The algorithm ID that should be used for new centroid objects. */
        int algorithm;

        /** The number of parameters used to specify the centroid objects. */
        int nparams;

        /*{*/
        /** The centroid parameters. */
        double param1, param2, param3;
        /*}*/

        /** The threshold signal-to-noise for the acceptance of centroid
         * calculations. 
         */
        double thresh;

        /** The singleton instance for the Centroid factory. */
        static CentroidFactory * instance; 
};

#endif // !CENTROIDFACTORY_H
