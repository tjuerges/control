#ifndef GALILRELAY_H
#define GALILRELAY_H

#include "Relay.h"
#include <Galil.h>
#include <boost/date_time/posix_time/posix_time.hpp>

/** Represents a switch or relay controlled by a Galil controller. The Galil
 * board should be initialized before a GalilRelay object is used. This Galil
 * board is specified in the constructor.
 */
class GalilRelay : public Relay {
    public:
        /** Constructor.
         * @param board the galil board that controls the relay.
         * @param n the output number connected to the relay.
         */
        GalilRelay(Galil * board, int n);

        void on();
        void off();
        Relay::State getState();

    private:
        State cached;
        boost::posix_time::ptime cacheTime;

        /** The Galil board that controls this relay. */ 
        Galil * galil;

        /** The output pin number connected to the relay. */
        int outputNumber;
};

#endif // !GALILRELAY_H
