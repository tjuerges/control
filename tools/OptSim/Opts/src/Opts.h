#ifndef OPTS_H
#define OPTS_H

#include <string>
#include <sstream>
#include <libconfig.h++>
#include "DataSequencer.h"


#ifdef ENABLE_HARDWARE
#include <Galil.h>
#endif // ENABLE_HARDWARE
class Camera;
class ClientInterface;
class Focuser;
class FilterWheel;
class Relay;
class ThermalSensor;
class Shutter;
class SunSensor;
class Heater;
class DigitalInput;


class CalibrationImage;


/** An environment for the Optical Pointing Telescope System.
 *
 * The class contains all hardware and APIs needed
 * for the operation of the OPTS. It contains a
 * camera, control board, and a user interface.
 * Devices should be accessed through this class,
 * rather than independently.
 */
class Opts
{
    public:
    /** Constructs a new OPTS environment.
     *
     * Sets a default identifier string for
     * this OPTS, and configures all hardware
     * devices for use. It DOES NOT power up
     * any hardware.
     */
    Opts();

    /// Destructor.
    ~Opts();


    /// Return a pointer to this instance.
    Opts* getOpts();

    /** Assigns an identification string to this
     * OPT unit.
     *
     * This string is used in the Query IDTCM
     * command.
     *
     * @param newID A string that identifies this Telescope Control Module.
     */
    void setID(std::string newID);
    /** Retrieves the ID of this OPTS.
     *
     * @returns A string that identifies this Telescope Control Module.
     */
    std::string getID();

    libconfig::Config* getConfig()
    {
        return configuration;
    }

#ifdef ENABLE_HARDWARE
    Galil* getGalil()
    {   return galil;}
#endif // ENABLE_HARDWARE
    /** @returns An interface to the relay that controls the power
     * supply. */
    Relay* getPower();

    /** @returns An interface to the camera controlled by this OPTS. */
    Camera* getCamera();

    /** @returns An interfrace to the filter wheel controlled by this
     * OPTS. */
    FilterWheel* getFilterWheel()
    {
        return wheel;
    }

    /** @returns An interface to the focuser controlled by this OPTS. */
    Focuser* getFocuser()
    {
        return focuser;
    }

    ThermalSensor* getTubeSensor()
    {
        return tubeSensor;
    }
    ThermalSensor* getFilterSensor()
    {
        return filterSensor;
    }

    Relay* getCameraPower()
    {
        return cameraPower;
    }

    Heater* getHeater()
    {
        return heater;
    }
    Relay* getFan()
    {
        return fan;
    }
    Shutter* getShutter()
    {
        return shutter;
    }

    DigitalInput* getPcbSunSensor()
    {
        return pcbSunSensor;
    }

    SunSensor* getSunSensor1()
    {
        return sunSensor1;
    }
    SunSensor* getSunSensor2()
    {
        return sunSensor2;
    }

    /** @returns An interface for communicating with client programs. */
    ClientInterface* getInterface();

    CalibrationImage* getFactoryFlat()
    {
        return factoryFlat;
    }

    CalibrationImage* getAlmaFlat()
    {
        return almaFlat;
    }
    void setAlmaFlat(CalibrationImage* flat);

    void protect();
    void unprotect();
    void setShutterOpen(bool isOpen)
    {
        shutterOpen = isOpen;
    }

    int getSlewFilter()
    {
        return slewFilter;
    }
    void setSlewFilter(int sf)
    {
        slewFilter = sf;
    }

    bool getSlewing()
    {
        return slewing;
    }
    void setSlewing(bool s)
    {
        slewing = s;
    }

    void galilEngStatus(std::stringstream& ss);

    void monitorCamera();
    private:
    /** A string identifying this OPTS unit. */
    std::string identifier;

    libconfig::Config* configuration;

#ifdef ENABLE_HARDWARE
    Galil* galil;
#endif // ENABLE_HARDWARE
    /** Power supply for the sun sensor, the filter wheel, the focuser,
     * and the shutter.
     */
    Relay* power;

    /** The camera controlled by this OPTS. */
    Camera* camera;
    /** The filter wheel controlled by this OPTS. */
    FilterWheel* wheel;
    /** The focuser motor. */
    Focuser* focuser;
    /** The shutter. */
    Shutter* shutter;

    /** The sun sensors. */
    SunSensor* sunSensor1;
    SunSensor* sunSensor2;

    ThermalSensor* tubeSensor;
    ThermalSensor* filterSensor;

    Relay* cameraPower;
    Heater* heater;
    Relay* fan;

    DigitalInput* pcbSunSensor;

    /** The filter that was in place during the last slew.
     * After a Filter or Expose command after a slew,
     * this filter will be returned to position.
     */
    int slewFilter;

    /** Is a slew in progress? */
    bool slewing;

    bool isProtected;
    bool shutterOpen;

    /** A user interface setup. */
    ClientInterface* iface;

    CalibrationImage* factoryFlat;
    CalibrationImage* almaFlat;
};

extern Opts theOPTS;

#include <iostream>
#include <fstream>

extern std::ofstream logFile;

#endif // ! OPTS_H
