#ifndef FOCUSER_H
#define FOCUSER_H

#include <boost/date_time/posix_time/posix_time.hpp>

#include "Stepper.h"

/** Represents a focusing motor. The focuser has a range of valid focus
 * position. The focuser can be commanded to move to any position in the
 * range, to achieve best focus.
 */
class Focuser {
    public:
        /** The current position or activity of the focus motor. */
        enum State {
            STOPPED,    /**< The focuser is stopped. */
            MOVING_UP,  /**< The focuser is moving to an increased position. */
            MOVING_DN,  /**< The focuser is moving to a decreased position. */
            LIMIT_UP,   /**< The focuser is at its high limit. */
            LIMIT_DN,   /**< The focuser is at its low limit. */
            STUCK,      /**< The focuser is stuck or has otherwise failed to
                         *   reach its commanded position.
                         */
            UNKNOWN     /**< The current state of the focuser is unknown. */
        };

        /** Constructor. */
        Focuser(Stepper * s, double stepSize);

        /** Initialize the focuser. Initialization realigns the software
         * focuser position with a reference position built into the
         * instrument. The focuser position may (and probably will) change
         * after this function is called. This function blocks until the 
         * initialization is finished.
         */
        void init();

        /** Returns a string representing the time and date when the focuser
         * was last initialized.
         * @returns the time when the focuser was last initialized.
         */
        std::string getInitTime();

        /** Returns the discrepancy found between the expected focus zeropoint
         * and the actual measured zeropoint, when the focuser was last
         * initialized.
         * @returns the zero position of the focuser at the last initialization.
         */
        double getZeroError();

        /** Sends the focuser to the specified position.
         * @param pos the desired position for the focuser, in millimeters.
         */
        void goTo(double pos);

        /** Returns the current position of the focuser.
         * @returns the current position of the focuser.
         */
        double getPosition();

        /** Returns the current state of the focuser.
         * @returns the current state of the focuser.
         * @see Focuser::State
         */
        State getState();

        /** Forces all focuser motion to stop. As a side effect, other
         * unrelated motion in the system may stop.
         */
        void abort() { stepper -> abort(); }

        /** Checks the error flag of the focuser object.
         * @returns true if an error has occurred, false otherwise.
         */
        bool getError();

    protected:
        /** Sets the error flag for the focuser object. */
        void setError() { error = true; }

        /** Clears the error flag for the focuser object. */
        void clearError() { error = false; }

    private:
        /** Time at which the focus was initialized. */
        boost::posix_time::ptime initTime;

        /** Position of the motor when it was last initialized. */
        double zeroPosition;

        /** Difference between this and the last zero position. */
        double zeroError;

        /** The stepper motor used to control the focus. */
        Stepper * stepper;

        /** The size of a step of the stepper motor in 
         * millimeters. */
        double stepSize;

        /** Error flag for the focuser object. True if the focuser has 
         * encountered an error. False otherwise. 
         */
        bool error;
};

#endif // !FOCUSER_H
