#include "Shutter.h"
#include "Relay.h"
#include "DigitalInput.h"
#include "Opts.h"

#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#ifdef ENABLE_HARDWARE
#include "GalilRelay.h"
#include "GalilDigitalInput.h"
#endif

Shutter::Shutter()
{
    state = Shutter::CLOSED;
#ifdef ENABLE_HARDWARE
    openSignal = new GalilRelay(theOPTS.getGalil(), 1);
#else
    openSignal = new Relay();
#endif

#ifdef ENABLE_HARDWARE
    openLimit = new GalilDigitalInput(theOPTS.getGalil(), 4);
    closedLimit = new GalilDigitalInput(theOPTS.getGalil(), 5);
#else
    openLimit = new DigitalInput();
    closedLimit = new DigitalInput();
    closedLimit -> setDefault(true);
#endif

    abort = false;

    error = false;
    state = CLOSED;
}

bool Shutter::isOpen()
{
    return !(openLimit -> check());
}

bool Shutter::isClosed()
{
    return !(closedLimit -> check());
}

void Shutter::open()
{
    using namespace boost::posix_time;

    if (abort) {
       abort = false;
       return;
    }

    state = OPENING;
    clearError();

    // Start the motor and clutch.
    openSignal -> on();

    // Determine when the operation should time out.
    logFile << "Shutter open will timeout in " << openTimeout << " seconds." << std::endl;
    ptime timeout = microsec_clock::local_time() + seconds(openTimeout);

    do {   // Wait for the shutter open signal.
        boost::thread::yield();

        if (state != OPENING) {
            logFile << "Conflicting command received." << std::endl;
            return;
        }

        if (abort) {       // User requested abort
            logFile << "Shutter open aborted." << std::endl;
            setError();
            state = UNKNOWN;
            openSignal -> off();
            abort = false;
            return;
        }

        // Check for timeout
        if (timeout < microsec_clock::local_time()) {
            logFile << "Shutter open timed out." << std::endl;
            setError();
            state = UNKNOWN;
            openSignal -> off();
            return;
        }

#ifndef ENABLE_HARDWARE
        openLimit -> setDefault(true);
        closedLimit -> setDefault(false);
#endif
    } while (!isOpen());
    logFile << "Shutter open finished." << std::endl;
    state = OPEN;

    // The clutch remains engaged and holds the shutter open.
}

void Shutter::close()
{
    using namespace boost::posix_time;

    clearError();

    if (abort) {
       setError();
       abort = false;
       return;
    }

    state = CLOSING;

    try {
        openSignal -> off();
    } catch (std::string exception) {
        setError();
    }

    // Determine when the operation should time out.
    ptime timeout = microsec_clock::local_time() + seconds(openTimeout);

    do {   // Wait for the shutter closed signal.
        boost::thread::yield();

        if (state != CLOSING) return;

        if (abort) {       // User requested abort
            state = UNKNOWN;
            abort = false;
            setError();
            return;
        }

        // Check for timeout
        if (timeout < microsec_clock::local_time()) {
            state = UNKNOWN;
            setError();
            return;
        }

#ifndef ENABLE_HARDWARE
        openLimit -> setDefault(false);
        closedLimit -> setDefault(true);
#endif

    } while (!isClosed());
    state = CLOSED;
}

void Shutter::stop()
{
    if ((state == OPENING) || (state == CLOSING)) abort = true;
}

Shutter::State Shutter::getState()
{
    bool isOpen = Shutter::isOpen();
    bool isClosed = Shutter::isClosed();

    if (isOpen && isClosed) {
        state = UNKNOWN;
        return state;
    }

    if (isOpen) {
        state = OPEN;
        return state;
    }

    if (isClosed) {
        state = CLOSED;
        return state;
    }

    switch (state) {
        case OPENING: return OPENING;
        case CLOSING: return CLOSING;
        default: return UNKNOWN;
    }

    return UNKNOWN;
}
