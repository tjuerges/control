#include <Galil.h>
#include "Relay.h"
#include "GalilRelay.h"
#include <sstream>
#include "Opts.h"
#include <boost/date_time/posix_time/posix_time.hpp>

GalilRelay::GalilRelay(Galil * board, int n) :
galil(board),
outputNumber(n)
{
    using namespace boost::posix_time;
    cacheTime = microsec_clock::universal_time() - seconds(1);
}

void GalilRelay::on()
{
    std::stringstream ss;
    ss << "SB " << outputNumber;
    try {
        galil -> command(ss.str());
    } catch (std::string ex) {
        logFile << "Exception: " << ex << std::endl;
    }
}

void GalilRelay::off()
{
    std::stringstream ss;
    ss << "CB " << outputNumber;
    try {
        galil -> command(ss.str());
    } catch (std::string ex) {
        logFile << "Exception: " << ex << std::endl;
    }
}

Relay::State GalilRelay::getState()
{
    using namespace boost::posix_time;
    ptime t(microsec_clock::universal_time()); 

    if (t < cacheTime + seconds(0.1)) {
        return cached;
    }

    std::stringstream ss;
    ss << "MG @OUT[" << outputNumber << "]";

    try {
        std::string result = galil -> command(ss.str());

        double state;
        ss.str(result);
        ss >> state;
        if (state > 0.5) cached = Relay::ON;
        else cached = Relay::OFF; 

        cacheTime = t;
    } catch (std::string ex) {
        logFile << "Exception: " << ex << std::endl;
    }

    return cached;
}
