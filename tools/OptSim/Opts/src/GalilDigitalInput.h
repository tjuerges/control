#ifndef GALILDIGITALINPUT_H
#define GALILDIGITALINPUT_H

#include "DigitalInput.h"
#include <Galil.h>
#include <boost/date_time/posix_time/posix_time.hpp>

/** Represents a digital input provided by a Galil board. Communication
 * with the Galil board must be established before the GalilDigitalInput
 * object is instantiated. This Galil object is provided in the constructor.
 * @see DigitalInput
 */
class GalilDigitalInput : public DigitalInput {
    public:
        /** Constructor. 
         * @param board the Galil board that provides this input.
         * @param n the input number on the board.
         */
        GalilDigitalInput(Galil * board, int n);

        /** @returns true if the digital input is active, or false otherwise.
         */
        bool check();

    private:
        bool cached;
        boost::posix_time::ptime cacheTime;

        /** The Galil board that provides the input. */
        Galil * galil;

        /** The input number for this input. */
        int inputNumber;
};

#endif // !GALILDIGITALINPUT_H
