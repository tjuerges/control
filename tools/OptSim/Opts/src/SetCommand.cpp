#include <sstream>

#include "Opts.h"
#include "SetCommand.h"
#include "QueryCommand.h"
#include "Camera.h"        // Needed for definition of RegionOfInterest.
#include "CalibrationImage.h"
#include "HotPixels.h"
#include "SunSensor.h"
#include "CentroidFactory.h"

SetCommand::SetCommand(const char * name)
{
    DataSequencer::getInstance() -> registerCommand(this, "Set", name);
    commandName = std::string(name);
}

bool SetCommand::permission(MessageParty sender, 
                            std::vector<std::string> command)
{
    std::string master = theOPTS.getInterface() -> getMaster();
    std::string senderName = sender.getName();
    
    if (master == "") return true;
    if (senderName == master) return true;
    
    return false;
}


SetCamTemp setCamTemp;
std::string SetCamTemp::execute(MessageParty sender,
                                std::vector<std::string> command)
{
    double temperature;
    std::stringstream ss(std::stringstream::in | std::stringstream::out);

    ss << command[2];
    ss >> temperature;
    if (ss.fail()) {
        return "Error CamTemp bad temperature";
    }

    if (theOPTS.getCamera() -> getState() == Camera::CAM_BUSY) {
        return "Error CamTemp camera busy";
    }

    theOPTS.getCamera() -> setTargetTemp(temperature);

    return "CamTemp " + command[2];
}


SetCentAlg setCentAlg;
std::string SetCentAlg::execute(MessageParty sender,
                                std::vector<std::string> command)
{
    if (command.size() < 2) return "Error CentAlg syntax";
    if (command.size() > 6) return "Error CentAlg syntax";

    int algorithm;
    double p1=0, p2=0, p3=0;

    std::stringstream ss(command[2]);
    ss >> algorithm;
    if (ss.fail() || (algorithm < 1) || (algorithm > 3)) {
        return "Error CentAlg number: " + command[2];
    }

    for (int i = 3; i < command.size(); i++) {
        double param;
        std::stringstream ss(command[i]);
        ss >> param;
        if (ss.fail()) {
            return "Error CentAlg parameter: " + command[i];
        }

        switch (i) {
            case 3:
               p1 = param;
               break;
            case 4:
               p2 = param;
               break;
            case 5:
               p3 = param;
               break;
        }
    } 

    CentroidFactory * cf = CentroidFactory::get();
    cf -> setNumParams(command.size() - 3);
    cf -> select(algorithm);
    if (command.size() > 3) cf -> setParam1(p1);
    if (command.size() > 4) cf -> setParam2(p2);
    if (command.size() > 5) cf -> setParam3(p3);

    return queryCentAlg.execute(sender, command);
}

SetCenThresh setCenThresh;
std::string SetCenThresh::execute(MessageParty sender,
                                  std::vector<std::string> command)
{
    if (command.size() != 3) return "Error CenThresh syntax";

    std::stringstream ss(command[2]);
    double thresh;

    ss >> thresh;
    if (ss.fail() || thresh < 0) return "Error CenThresh bad threshold";

    CentroidFactory::get() -> setThresh(thresh);
    return queryCenThresh.execute(sender, command);
}

SetExpCount setExpCount;
std::string SetExpCount::execute(MessageParty sender,
                                 std::vector<std::string> command)
{
    if (command.size() == 3) {
        int newCount, oldCount;
        std::stringstream ss;
        ss << command[2];
        ss >> newCount;
        if (ss.fail() == false) {
            std::stringstream response;
            response << "ExpCount now " << newCount;
            oldCount = theOPTS.getCamera() -> getExposuresRemaining();
            theOPTS.getCamera() -> setExposuresRemaining(newCount);
            response << ", was " << oldCount << std::ends;

            return response.str();
        }
    }

    return "Error ExpCount bad count";
}


SetExpTime setExpTime;
std::string SetExpTime::execute(MessageParty sender,
                                std::vector<std::string> command)
{
    if (command.size() > 2) {
        int milliseconds;
        double seconds;

        std::stringstream ss(std::stringstream::in | std::stringstream::out);
        ss << command[2];
        ss >> seconds;
        
        if (ss.fail() == false) {
            milliseconds = (int)(seconds * 1000);
            if (milliseconds > 0) {
                theOPTS.getCamera() -> setDefaultExptime(milliseconds);

                std::ostringstream response;
                response << "Exptime " << seconds << std::ends;
                return response.str();
            }
        }
    }

    return "Error ExpTime bad time";
}


SetFlatField setFlatField;
std::string SetFlatField::execute(MessageParty sender,
                                  std::vector<std::string> command)
{
    if (command.size() != 3) return "Error FlatField syntax";

    if (command[2] == "None") {
        theOPTS.getCamera() -> setFlat(0);
        return queryFlatField.execute(sender, command);
    }

    if (command[2] == "Factory") {
        CalibrationImage * flat = theOPTS.getFactoryFlat();
        theOPTS.getCamera() -> setFlat(flat);
        return queryFlatField.execute(sender, command);
    }

    if (command[2] == "ALMA") {
        CalibrationImage * flat = theOPTS.getAlmaFlat();
        theOPTS.getCamera() -> setFlat(flat);
        return queryFlatField.execute(sender, command);
    }

    return "Error FlatField bad option: " + command[2];
}



SetHotPix setHotPix;
std::string SetHotPix::execute(MessageParty sender,
                               std::vector<std::string> command)
{
    unsigned int i, j;

    if (command.size() < 3) return "Error HotPix unknown Operation";
    if ((command[2] != "Add") && 
        (command[2] != "Remove") && 
        (command[2] != "None")) 
    {
        return "Error HotPix unknown Operation";
    }

    RegionOfInterest roi = theOPTS.getCamera() -> getGeometry();
    HotPixels * hotpix = theOPTS.getCamera() -> getHotPix();

    std::vector<std::pair<int, int> > pixels;
    std::vector<std::string> illegal;

    // Parse the command list.
    for (i = 3; i < command.size(); i++) {
        std::istringstream ss(command[i]);

        int x = -1, y = -1;
        char comma;

        ss >> x;
        ss >> comma;
        ss >> y;

        x--; y--;   // Change from one-indexed to zero-indexed.

        if ((ss.fail()) ||
            (comma != ',') || 
            (x < roi.rowLo) ||
            (x > roi.rowHi) ||
            (y < roi.colLo) ||
            (y > roi.colHi)) 
        {
            illegal.push_back(command[i]);
        } else {
            pixels.push_back(std::make_pair(x, y));
        }
    }

    // Check for any illegal pixels.
    if (illegal.size()) {
        std::ostringstream err;
        err << "Error HotPix ";
        err << command[2];
        err << " specifies illegal pixel position(s):";

        for (i = 0; i < illegal.size(); i++) err << " " << illegal[i];

        return err.str();
    }

    // None is easy. We just zero out the whole hot pixel mask.
    if (command[2] == "None") {
        if (hotpix) hotpix -> deleteAll();
        return queryHotPix.execute(sender, command);
    }

    if (!hotpix) hotpix = new HotPixels();
    theOPTS.getCamera() -> setHotPix(hotpix);

    if (command[2] == "Add") {
        for (i = 0; i < pixels.size(); i++) {
            hotpix -> add(pixels[i].first, pixels[i].second);
        }
        return queryHotPix.execute(sender, command);
    }

    if (command[2] == "Remove") {
        // Before proceeding, check that all the pixels are currently
        // marked.
        for (i = 0; i < pixels.size(); i++) {
            if (!(hotpix -> includes(pixels[i].first, pixels[i].second))) {
                // This pixel is not marked!
                return "Error HotPix Remove specifies pixels which aren't "
                       "in the list";
            }
        }

        // Now that we've checked them all, go ahead and unmark them.
        for (i = 0; i < pixels.size(); i++) {
            hotpix -> del(pixels[i].first, pixels[i].second);
        }
        return queryHotPix.execute(sender, command);
    }

    /*@notreached@*/
    return "Error HotPix unknown Operation";
}


SetMasterMB setMasterMB;
bool SetMasterMB::permission(MessageParty sender,
                             std::vector<std::string> command)
{
    return true;
}

std::string SetMasterMB::execute(MessageParty sender,
                                 std::vector<std::string> command)
{
    if (command.size() > 2) {
        if (command[2] == "Done") {
            std::string master = theOPTS.getInterface() -> getMaster();
            theOPTS.getInterface() -> setMaster("");
            return "MasterMB " + master + " Done";
        } 

        theOPTS.getInterface() -> setMaster(command[2]);
        return "MasterMB " + command[2];
    }

    if (command.size() == 2) {
        std::string master = sender.getName();
        theOPTS.getInterface() -> setMaster(master);
        return "MasterMB " + master;
    }

    return "Error MasterMB syntax";
}

SetNextSeqNo setNextSeqNo;
std::string SetNextSeqNo::execute(MessageParty sender,
                                  std::vector<std::string> command)
{
    if (command.size() > 2) {
        std::stringstream ss(std::stringstream::in | std::stringstream::out);
        ss << command[2];
        int nextSeqNo;
        ss >> nextSeqNo;

        if (ss.fail() == false) { // Does the string parse?
            if ((nextSeqNo >= 1) && (nextSeqNo <= 999999)) {
                // If we reach here, we have accepted the new number.
                theOPTS.getCamera() -> setNextSequenceNum(nextSeqNo);

                std::ostringstream response;
                response << "NextSeqNo " << nextSeqNo << std::ends;

                return response.str();
            }
        }

        return "Error NextSeqNo bad";
    }
    return "Error NextSeqNo syntax";
}


SetSubImage setSubImage;
std::string SetSubImage::execute(MessageParty sender,
                                 std::vector<std::string> command)
{
    if (theOPTS.getCamera() -> getState() == Camera::CAM_EXPOSING) {
        return "Error SubImage camera busy";
    }

    if (command.size() < 3) {
        return "Error SubImage syntax";
    }

    if (command[2] == "All") {
        RegionOfInterest whole = theOPTS.getCamera() -> getGeometry();
        theOPTS.getCamera() -> setROI(whole);
        return querySubImage.execute(sender, command);
    }

    if (command.size() < 4) {
        return "Error SubImage syntax";
    }

    std::stringstream ss(std::stringstream::in | std::stringstream::out);
    RegionOfInterest roi;
    ss << command[2].substr(0, command[2].find(","));
    ss >> roi.colLo;
    if (ss.fail()) {
        return "Error SubImage could not parse lo col";
    }

    ss.clear(std::stringstream::goodbit);
    ss << command[2].substr(command[2].find(",")+1);
    ss >> roi.rowLo;
    if (ss.fail()) {
        return "Error SubImage could not parse lo row";
    }

    ss.clear(std::stringstream::goodbit);
    ss << command[3].substr(0, command[3].find(","));
    ss >> roi.colHi;
    if (ss.fail()) {
        return "Error SubImage could not parse hi col";
    }

    ss.clear(std::stringstream::goodbit);
    ss << command[3].substr(command[3].find(",")+1);
    ss >> roi.rowHi;
    if (ss.fail()) {
        return "Error SubImage could not parse hi row";
    }

    roi.colLo--;
    roi.colHi--;
    roi.rowLo--;
    roi.rowHi--;

    RegionOfInterest whole = theOPTS.getCamera() -> getGeometry();

    if (roi.colLo >= roi.colHi) return "Error SubImage range invalid";
    if (roi.rowLo >= roi.rowHi) return "Error SubImage range invalid";
    if (roi.rowLo < 0) return "Error SubImage bad geometry";
    if (roi.colLo < 0) return "Error SubImage bad geometry";
    if (roi.rowHi > whole.rowHi) return "Error SubImage bad geometry";
    if (roi.colHi > whole.rowHi) return "Error SubImage bad geometry";

    theOPTS.getCamera() -> setROI(roi);
    return querySubImage.execute(sender, command);
}


SetSunThresh setSunThresh;
std::string SetSunThresh::execute(MessageParty sender,
                                  std::vector<std::string> command)
{
    if (command.size() != 4) return "Error SunThresh bad";

    double thresh1, thresh2;
    std::stringstream ss(command[2]);
    ss >> thresh1;
    if (ss.fail()) return "Error SunThresh bad";

    std::stringstream ss2(command[3]);
    ss2 >> thresh2;
    if (ss2.fail()) return "Error SunThresh bad";

    theOPTS.getSunSensor1() -> setThreshold(thresh1);
    theOPTS.getSunSensor2() -> setThreshold(thresh2);

    return querySunThresh.execute(sender, command);
}
