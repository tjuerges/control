#ifndef DOCOMMAND_H
#define DOCOMMAND_H

#include "Command.h"

class SetCommand : public Command {
    public:
        SetCommand(const char * name);
        virtual bool permission(MessageParty sender, 
                                std::vector<std::string> command);
};


class SetCamTemp : public SetCommand {
    public:
        SetCamTemp() : SetCommand("CamTemp") {};
        std::string execute(MessageParty sender, 
                            std::vector<std::string> command);    
};
extern SetCamTemp setCamTemp;

class SetCentAlg : public SetCommand {
    public:
        SetCentAlg() : SetCommand("CentAlg") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern SetCentAlg setCentAlg;


class SetCenThresh : public SetCommand {
    public:
        SetCenThresh() : SetCommand("CenThresh") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern SetCenThresh setCenThresh;


class SetExpCount : public SetCommand {
    public:
        SetExpCount() : SetCommand("ExpCount") {};
        std::string execute(MessageParty sender, 
                            std::vector<std::string> command);    
};
extern SetExpCount setExpCount;


class SetExpTime : public SetCommand {
    public:
        SetExpTime() : SetCommand("ExpTime") {};
        std::string execute(MessageParty sender, 
                            std::vector<std::string> command);    
};
extern SetExpTime setExpTime;


class SetFlatField : public SetCommand {
    public:
        SetFlatField() : SetCommand("FlatField") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern SetFlatField setFlatField;


class SetHotPix : public SetCommand {
    public:
        SetHotPix() : SetCommand("HotPix") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);

};
extern SetHotPix setHotPix;

class SetMasterMB : public SetCommand {
    public:
        SetMasterMB() : SetCommand("MasterMB") {};
        bool permission(MessageParty sender,
                        std::vector<std::string> command);
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern SetMasterMB setMasterMB;


class SetNextSeqNo : public SetCommand {
    public:
        SetNextSeqNo() : SetCommand("NextSeqNo") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern SetNextSeqNo setNextSeqNo;


class SetSubImage : public SetCommand {
    public:
        SetSubImage() : SetCommand("SubImage") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern SetSubImage setSubImage;


class SetSunThresh : public SetCommand {
    public:
        SetSunThresh() : SetCommand("SunThresh") {};
        std::string execute(MessageParty sender,
                            std::vector<std::string> command);
};
extern SetSunThresh setSunThresh;

#endif // ! DOCOMMAND_H
