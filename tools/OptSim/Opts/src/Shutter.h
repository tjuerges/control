#ifndef SHUTTER_H
#define SHUTTER_H

class Relay;
class DigitalInput;

/** Represents the shutter that protects the optical system when the telescope
 * is not in use, or when the sun sensor is tripped.
 */
class Shutter {
    public:
        /** Possible states for the shutter. */
        enum State {
            OPEN,    //< The open limit has been triggered.
            CLOSED,  //< The closed limit has been triggered.
            OPENING, //< The shutter is moving towards an open position.
            CLOSING, //< The shutter is moving towards a closed position.
            UNKNOWN  //< The shutter position cannot be determined.
        };


        /** Timeout for opening the shutter, in seconds. If the shutter has
         * not opened this many seconds after the command to open the shutter
         * is issued, the shutter will be stopped and open() will return.
         */ 
        static const unsigned int openTimeout = 6;

        /** Timeout for closeing the shutter, in seconds. If the shutter has
         * not closed this many seconds after the command to close the shutter
         * is issued, the shutter will be stopped and close() will return.
         */ 
        static const unsigned int closeTimeout = 1;

        /** Constructor. */
        Shutter();

        bool isOpen();
        bool isClosed();

        /** Open the shutter. This function blocks until the shutter is open
         * or has timed out.
         */ 
        void open();

        /** Close the shutter. This function blocks until the shutter is
         * closed or has timed out.
         */ 
        void close();

        /** Emergency stop. If this function is closed, the motor will be
         * stopped. The state of the clutch is not changed.
         */
        void stop();

        /** Checks the state of the shutter. 
         * @returns a State enum representing the state of the shutter.
         */
        State getState();

        /** Checks whether a timeout occurred during an open or close\
         * operation.
         * @returns true if a timeout has occurred, false otherwise.
         */
        bool getError() { return error; }

    protected:
        /** Sets the shutter's error flag, indicating that an error has
         * occurred.
         */
        void setError() { error = true; }
        /** Clears the shutter's error flag. */
        void clearError() { error = false; }

    private:
        /** State of the shutter when last checked. */
        State state;

        /** A signal that sends power to the motor and clutch. When the
         * shutter opens, a limit switch cuts power to the motor. The
         * clutch remains powered, holding the shutter open.
         */
        Relay * openSignal;

        /** A digital input that becomes active when the shutter is open. */
        DigitalInput * openLimit;

        /** A digital input that becomes active when the shutter is closed. */
        DigitalInput * closedLimit;

        /** Whether the user has requested an abort. */
        volatile bool abort;

        /** Whether an error has occurred while opening/closing the shutter. */
        bool error;
};

#endif // !SHUTTER_H
