#include <iostream>
#include <vector>

#if HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H

#if HAVE_CFITSIO_FITSIO_H
#include <cfitsio/fitsio.h>
#else
#include <fitsio.h>
#endif

#if HAVE_LIBSOCKPP
#include <sock.h>
#endif // HAVE_LIBSOCKPP

#include "Opts.h"
#include "Image.h"
#include "ClientInterface.h"

#include <boost/date_time/microsec_time_clock.hpp>

void MessageParty::setName(std::string newName)
{
    name = newName;
}

std::string MessageParty::getName() const
{
    return name;
}

void MessageParty::setHandle(void * newHandle)
{
    handle = newHandle;
}

void * MessageParty::getHandle()
{
    return handle;
}

void ClientInterface::send(MessageParty sender, std::string message)
{
    logFile << "TO " << sender.getName() << ": " << message << std::endl;
}

Message ClientInterface::receive()
{
    Message mesg;
    logFile << "> ";
    getline(std::cin, mesg.command);
    return mesg;
}

char * ClientInterface::receiveFits()
{
    char * messageBuf = new char[1024 * 1024 * 10];
    char * filename = new char[16];

    int messageLength;

    sock_bufct(1024 * 1024 * 10);
    int gotMessage = sock_sel(messageBuf, &messageLength, NULL, 0, 0, 0);
    sock_bufct(4096);

    if (gotMessage < 0) return 0;

    if (strncmp(messageBuf, "SIMPLE", 6) != 0) {
        delete [] messageBuf;
        delete [] filename;
        return 0;
    }

    strcpy(filename, "/tmp/flatXXXXXX");

    int fd = mkstemp(filename);
    if (fd == -1) {
        // Could not use mkstemp to create a temporary file.
        logFile << "mkstemp failed." << std::endl;
        delete [] messageBuf;
        delete [] filename;
        return 0;
    }

    // Store the received FITS file on disk.
    if (write(fd, messageBuf, messageLength) != messageLength) {
        logFile << "Could not write image to disk." << std::endl;
        close(fd);
        delete [] messageBuf;
        delete [] filename;
        return 0;
    }

    // Clean up.
    close(fd);
    delete [] messageBuf;

    return filename;
}

void ClientInterface::sendImage(MessageParty recipient, Image * img)
{
    using namespace boost::posix_time;
    logFile << "Instructed to send image";
    logFile << " to " << recipient.getName() << std::endl; 
}

char * ClientInterface::readFile(MessageParty recipient)
{
	return 0;
}

void ClientInterface::setMaster(std::string newMaster)
{
    master = newMaster;
}

std::string ClientInterface::getMaster()
{
    return master;
}

#if HAVE_LIBSOCKPP
SockLibInterface::SockLibInterface()
{
    if (theOPTS.getConfig() -> exists("opts.mailboxdefs")) {
        const char * mbd;
        mbd = theOPTS.getConfig() -> lookup("opts.mailboxdefs");
        setenv("MAILBOXDEFS", mbd, 0);
    }

#ifdef ENABLE_HARDWARE
    if (theOPTS.getConfig() -> exists("opts.mailbox")) {
        mailboxName = (const char *)theOPTS.getConfig()->lookup("opts.mailbox");
    } else {
        char host[10];
        if (gethostname(host, 10) == 0) {
            // Convert the hostname to upper case and use it as the mailbox.
            char * ptr = host;
            while (*ptr) {
                if (*ptr >= 'a' && *ptr <= 'z') *ptr += 'A' - 'a';
                ptr++;
            }
            mailboxName = std::string(host);
        } else {
            mailboxName = "OPT1";
        }
    }
#else
    mailboxName = "OPTSIM";
#endif

    std::cerr << "Binding to " << mailboxName << std::endl; 

    if (sock_bind(mailboxName.c_str()) == NULL) {
        std::cerr << "Could not bind to a mailbox." << std::endl;
        exit(EXIT_FAILURE);
    }
}

void SockLibInterface::sendImage(MessageParty recipient, Image * img)
{
    using namespace boost::posix_time;

    boost::mutex::scoped_lock lock(mutex);

    logFile << "Attempting to send an image." << std::endl;
    logFile << to_iso_extended_string(microsec_clock::universal_time()) << std::endl;
    struct SOCK * handle = (struct SOCK *)recipient.getHandle();
    sock_write(handle, (char *)(img -> getFITS()), img -> getFitsSize());
    logFile << "Attempted to send an image." << std::endl;
    logFile << to_iso_extended_string(microsec_clock::universal_time()) << std::endl;

    return;
}

void SockLibInterface::send(MessageParty recipient, std::string message)
{
    boost::mutex::scoped_lock lock(mutex);
    struct SOCK * handle = (struct SOCK *)recipient.getHandle();
    sock_send(handle, message.c_str());
}

Message SockLibInterface::receive()
{
    Message msg;
    char * messageBuf = new char[4096];
    int gotMessage, messageLength;
    
    while (true) {
        gotMessage = sock_sel(messageBuf, &messageLength, NULL, 0, 0, 0);
        if (strstr(messageBuf, "Query") == NULL) {
            logFile << "> " << messageBuf << std::endl;
        }

        if (gotMessage >= 0) {
            Message msg;
            struct SOCK * handle;

            msg.command = std::string(messageBuf);
            msg.sender = new MessageParty();

            handle = sock_last();
            msg.sender -> setHandle(handle);
            msg.sender -> setName(std::string(sock_name(handle)));

            delete [] messageBuf;
            return msg;
        }
    }
}

#endif // HAVE_LIBSOCKPP
