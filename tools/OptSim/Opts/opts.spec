Summary: The control program for the ALMA Optical Pointing Telescope System.
Name: opts-test
Version: 0.2.3
Release: 1
License: Commercial
Group: Applications/Engineering
Source0: opts-%{version}.tar.gz
BuildRoot: %{_tmppath}/opts-%{version}-%{release}-build

%description
OPTS is a software package for the control of the ALMA Optical Pointing
Telescope System. The behavior of OPTS is described by ALMA document
ALMA-36.01.00.00-70.35-40.00-A-ICD, the Interface Control Document. This
distribution of the OPTS software is designed for use as a testing
environment, and contains no actual hardware control code.

%prep
%setup -n opts-%{version} -b 0 

%build
./configure --prefix=/usr --disable-hardware RPM_OPT_FLAGS="$RPM_OPT_FLAGS"

%install
if [ "${RPM_BUILD_ROOT}x" != "/x" ]; then
    rm -rf $RPM_BUILD_ROOT
fi
make install DESTDIR=$RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc README COPYING ChangeLog INSTALL NEWS

/usr/bin/opts
/etc/opts.conf

%changelog
* Tue Dec 23 2008 Brian Brondel <bbrondel@astronomical.com>
- basic spec file
