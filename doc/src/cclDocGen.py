#!/usr/bin/env python
# The purpose of this script is to generate all of the required documentation
# for the CCL with some sort of order and to prevent the generation of
# redundant documentation.
#
# This is intended to generate a set of documentation that can be printed
# and used by our end users.  This is not intended for developer documentation.

# This is a hack suggested by Arne to work around a bug in ACS.  We should
# remove it when this bug gets fixed (ACS-8.0?)
#  Jeff Kern Sept 24 2008
import sys
sys.argv[0] = 'pydoc'

import CCL
import pydoc

TargetDir = "../doc/CCLReferenceManual"
import os
if not os.path.exists("../doc"):
	os.mkdir("../doc")
if not os.path.exists(TargetDir):
	os.mkdir(TargetDir)
		
object, name = pydoc.resolve(CCL,0)

# This is the list of all the devices the system knows about.  Any base class
# associated with these will be removed from the list as redundant in terms
# of the documentation
genDeviceList = ['FLOOG', 'CRD', 'ColdCart3', 'ColdCart4', 'ColdCart6',
		 'ColdCart7', 'ColdCart8', 'ColdCart9', 'ColdCart', 'Cryostat', 'DGCK',
		 'DRX', 'DTX', 'OpticalTelescope', 'IFProc',
		 'IFSwitch', 'LLC', 'LO2', 'LORR', 'LPR', 'Mount',
		 'PSA', 'PSD', 'PSLLC', 'PSSAS',
		 'PowerDist3', 'PowerDist4', 'PowerDist6', 'PowerDist7',
		 'PowerDist8', 'PowerDist9', 'PowerDist', 'WCA3', 'WCA4', 'WCA6',
		 'WCA8', 'WCA7', 'WCA9', 'WCA', 'PDA']

deviceList = ['CVR', 'GPS', 'FrontEnd', 
	      'HoloRx', 'HoloDSP']

# This is the list of observing modes that the system knows about
obsModeList =  ['OpticalPointingObservingMode', 'SingleFieldInterferometryObservingMode',
		'TotalPowerObservingMode', 'TowerHolographyObservingMode']

# This is the list of Antenna Controllers
antControllerList = ['AntLOController', 'MountController', 'TowerHolography',
		     'OpticalPointing']

# This is a list of classes which I think we should suppress and can get
# of.  These are mostly historic methods that are no longer needed
suppressList = ['ObservingMode', 'TestMC', 'TestOM', 'TrackBase',
		'FivePointsBase', 'Gaussian',  'SpiralBase',
		'DelayServerBase', 'AntModeController', 'MountACABase',
		'MountAEMBase','PSU']

introText ="""
Control Command Language Reference Manual

The Control Command Language (CCL) is a set of python classes which
are designed to provide access and control of the ALMA online system.
A description of the CCL and examples of use can be found in the CCL
Users Manual.  

The following modules are currently supported within the CCL:
"""

#------------------- End of Configuration Section -------------------

# modpkg is the set of all modules that the online system can find
# associated with the CCL
modpkg = []
for importer, modname, ispkg in pydoc.pkgutil.iter_modules(object.__path__):
	modpkg.append(modname)


# Now we want to remove all of the devices and the device base classes from
# the modpkg we will handle them seperatly
for device in genDeviceList:
	try:
		modpkg.remove(device)
		modpkg.remove(device + 'Base')
	except ValueError:
		print "Error: Generated Device %s was not handled correctly."%\
		      device

for device in deviceList:
	try:
		modpkg.remove(device)
	except ValueError:
		print "Error: Non Generated Device %s was not handled correctly" %\
		      device

for obsMode in obsModeList:
	try:
		modpkg.remove(obsMode)
	except ValueError:
		print "Error: Observing mode %s was not handled correctly" %\
		      obsMode

for modeCntl in antControllerList:
	try:
		modpkg.remove(modeCntl)
	except ValueError:
		print "Error: Mode Controller %s was not handled correctly" %\
		      obsMode

for pkg in suppressList:
	try:
		modpkg.remove(pkg)
	except ValueError:
		print "Notice: Suppressed package %s no longer is in system" %\
		      pkg
		
genList= [("ObservingModes",obsModeList),
	  ("ModeControllers",antControllerList),
	  ("Devices", genDeviceList),
	  ("Devices", deviceList),
	  ("Miscellaneous", modpkg)]

for dir, list in genList:
	if not os.path.exists(TargetDir+"/"+dir):
		os.mkdir(TargetDir+"/"+dir)
	list.sort()
	for mod in list:
		file = open(TargetDir +"/" + dir + "/" + mod +".txt",'w')
		mod = pydoc.safeimport("CCL." + mod)
		file.write(pydoc.text.document(mod))
		file.close()
	
# Now we want to build an overview / table of contents
# for this.

overview = open(TargetDir + "/CCLReferenceManual.txt","w")
overview.write(introText)

overview.write("\nOBSERVING MODES:\n")
for obsMode in obsModeList:
	overview.write("\t * " + obsMode+ "\n")

overview.write("\nMODE CONTROLLERS:\n")
for mc in antControllerList:
	overview.write("\t * " + mc + "\n")

allDevices = genDeviceList + deviceList
allDevices.sort()
overview.write("\nDEVICES:\n")
for device in allDevices:
	overview.write("\t * " + device + "\n")

overview.write("\nMISCELLANEOUS:\n")
for pkg in modpkg:
	overview.write("\t * " + pkg + "\n")

overview.close()
