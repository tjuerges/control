/* @(#) $Id: testRtFindKernelModules.cpp,v 1.4 2006/08/31 18:37:38 ramestic Exp
$
 *
 * Copyright (C) 2006
 * Associated Universities, Inc. Washington DC, USA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * Correspondence concerning ALMA Software should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */


#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestCaller.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#include <delayServerBaseImpl.h>
#include <TETimeUtil.h>
#include <sys/time.h>
#include <ControlExceptions.h>
#include <ASDM.h>
#include <CalWVRTable.h>
#include <CalWVRRow.h>
#include <maciSimpleClient.h>

class DelayServerBaseImplTest : public CppUnit::TestFixture,
                                public DelayServerBaseImpl
{
public:
  DelayServerBaseImplTest() : CppUnit::TestFixture(),
                              DelayServerBaseImpl(),
                              interceptGenerateDelayEvent(true) {


}
  ~DelayServerBaseImplTest(){}

  void setUp() {
    srand48(time(0));
  }

  void testinitializeServer(){
    const std::string arrayName("Array001");
    initializeServer(arrayName.c_str());
    strcmp(arrayName.c_str(), arrayName_m.c_str());
    CPPUNIT_ASSERT_MESSAGE("Array name incorrectly stored",
                           !strcmp(arrayName.c_str(), arrayName_m.c_str()));
    CPPUNIT_ASSERT_MESSAGE("Last Event Stop Time is not on a TE",
                           (lastEventStopTime_m.value % 480000L)==0);
    CPPUNIT_ASSERT_MESSAGE("Last Event Stop Time is (alot) in the past",
                           TETimeUtil::unix2epoch(time(0)).value-
                           lastEventStopTime_m.value < 1e6);
  }

  void testAddingAnAntenna(){    /* This should give us the default antenna*/
    addAntenna("DV01");
    CPPUNIT_ASSERT(antennaData_m.size() == 1);
    
    addAntenna("DV02");
    CPPUNIT_ASSERT(antennaData_m.size() == 2);

    {
      Control::AntennaSeq_var antSeq(getAntennas());
      CPPUNIT_ASSERT(antSeq->length() == 2);
      CPPUNIT_ASSERT(!strcmp((*antSeq)[0],"DV01"));
      CPPUNIT_ASSERT(!strcmp((*antSeq)[1],"DV02"));
    }

    removeAntenna("DV01");
    CPPUNIT_ASSERT(antennaData_m.size() == 1);
    {
      Control::AntennaSeq_var antSeq(getAntennas());
      CPPUNIT_ASSERT(antSeq->length() == 1);
      CPPUNIT_ASSERT(!strcmp((*antSeq)[0],"DV02"));
    }
  }

  void testPadLocationInterface(){
    double X;
    double Y;
    double Z;

    addAntenna("DV01");
    CPPUNIT_ASSERT_DOUBLES_EQUAL(1, antennaData_m["DV01"].padVector[0], 1E-12);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(1, antennaData_m["DV01"].padVector[1], 1E-12);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(1, antennaData_m["DV01"].padVector[2], 1E-12);
    
    getAntennaPadLocation("DV01", X, Y, Z);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(1, X, 1E-12);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(1, Y, 1E-12);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(1, Z, 1E-12);

    setAntennaPadLocation("DV01", 10101.1, 20202.2, -30303.3);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(10101.1, antennaData_m["DV01"].padVector[0], 
                                 1E-12);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(20202.2, antennaData_m["DV01"].padVector[1],
                                 1E-12);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(-30303.3, antennaData_m["DV01"].padVector[2],
                                 1E-12);
    
    getAntennaPadLocation("DV01", X, Y, Z);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(10101.1, X, 1E-12);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(20202.2, Y, 1E-12);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(-30303.3, Z, 1E-12);

    /* Check the Exception Cases */
    CPPUNIT_ASSERT_THROW(setAntennaPadLocation("DV02", X, Y, Z),
                         ControlExceptions::IllegalParameterErrorEx);

    CPPUNIT_ASSERT_THROW(getAntennaPadLocation("DV02", X, Y, Z),
                         ControlExceptions::IllegalParameterErrorEx);
   }

  void testAntennaVectorInterface(){
    double X;
    double Y;
    double Z;

    addAntenna("DV01");
    CPPUNIT_ASSERT_DOUBLES_EQUAL(0, antennaData_m["DV01"].antennaVector[0], 
                                 1E-12);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(0, antennaData_m["DV01"].antennaVector[1], 
                                 1E-12);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(0, antennaData_m["DV01"].antennaVector[2],
                                 1E-12);
    
    getAntennaVector("DV01", X, Y, Z);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(0, X, 1E-12);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(0, Y, 1E-12);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(0, Z, 1E-12);

    setAntennaVector("DV01", 2.5E-6, -1.5E-6, 12.5);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(2.5E-6, 
                                 antennaData_m["DV01"].antennaVector[0],1E-12);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(-1.5E-6,
                                 antennaData_m["DV01"].antennaVector[1],1E-12);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(12.5, 
                                 antennaData_m["DV01"].antennaVector[2],1E-12);
    
    getAntennaVector("DV01", X, Y, Z);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(2.5E-6, X, 1E-12);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(-1.5E-6, Y, 1E-12);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(12.5, Z, 1E-12);

    /* Check the Exception Cases */
    CPPUNIT_ASSERT_THROW(setAntennaVector("DV02", X, Y, Z),
                         ControlExceptions::IllegalParameterErrorEx);

    CPPUNIT_ASSERT_THROW(getAntennaVector("DV02", X, Y, Z),
                         ControlExceptions::IllegalParameterErrorEx);
   }

  void testKTermInterface(){
    addAntenna("DV01");
    CPPUNIT_ASSERT_DOUBLES_EQUAL(0, antennaData_m["DV01"].offsetVector[0], 
                                 1E-12);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(0, getAxisNonIntersectionParameter("DV01"),
                                 1E-12);

    setAxisNonIntersectionParameter("DV01", 1.4E-6);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(1.4E-6, 
                                 antennaData_m["DV01"].offsetVector[0], 1E-12);
    
    CPPUNIT_ASSERT_DOUBLES_EQUAL(1.4E-6,
                                 getAxisNonIntersectionParameter("DV01"),
                                 1E-12);

    /* Check the Exception Cases */
    CPPUNIT_ASSERT_THROW(setAxisNonIntersectionParameter("DV02", 1E-6),
                         ControlExceptions::IllegalParameterErrorEx);

    CPPUNIT_ASSERT_THROW(getAxisNonIntersectionParameter("DV02"),
                         ControlExceptions::IllegalParameterErrorEx);
  }



  void testSettingCableDelays(){
    addAntenna("DV01");
    CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0E-9, 
                                 antennaData_m["DV01"].antennaCableDelay,
                                 1E-12);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0E-9, getAntennaCableDelay("DV01"), 1E-12);
    
    setAntennaCableDelay("DV01",22.3E-9);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(22.3E-9,
                                 antennaData_m["DV01"].antennaCableDelay,
                                 1E-12);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(22.3E-9, getAntennaCableDelay("DV01"), 1E-12);

    /* Check the Exception Cases */
     CPPUNIT_ASSERT_THROW(setAntennaCableDelay("DV03",22.3E-9),
                          ControlExceptions::IllegalParameterErrorEx);

     CPPUNIT_ASSERT_THROW(getAntennaCableDelay("DV03"),
                           ControlExceptions::IllegalParameterErrorEx);
  }

  void testGenerateDelayEventExecution(){
     const unsigned int NUM_ANTENNA(4);
     const std::string ARRAY_NAME("Array001");

     initializeServer(ARRAY_NAME.c_str());
     interceptGenerateDelayEvent = false;
    
     for (unsigned int idx = 0; idx < NUM_ANTENNA; idx++) {
       char name[5];
       sprintf(name,"DV%02d", idx);
       addAntenna(name);
     }
     publishSingleDelayEvent();
  };


  void testRegenerateDelayEvent(){
  }

  void testCreateArrayDelayEvent(){
    const unsigned int MAX_NUM_ANTENNA(66);
    const std::string arrayName("Array001");
    const ACS::Time   startTime(134554839717600000LL);
    initializeServer(arrayName.c_str());

    for (unsigned int loop = 0; loop < MAX_NUM_ANTENNA; loop++) {
      eventSequenceNumber_m = loop;
      char name[5];
      sprintf(name,"DV%02d", loop);
      addAntenna(name);
      setAntennaCableDelay(name, drand48() * 60E-6);
      
      Control::ArrayDelayEvent ade = createArrayDelayEvent(startTime);
      
      CPPUNIT_ASSERT(!strcmp(ade.arrayName, arrayName.c_str()));
      CPPUNIT_ASSERT(ade.sequenceNumber == static_cast<int>(loop));
      CPPUNIT_ASSERT(ade.antennaDelayEvents.length() == loop + 1);

      for (unsigned int ant = 0; ant <= loop; ant++) {
        Control::AntennaDelayEvent antDE = ade.antennaDelayEvents[ant];
        CPPUNIT_ASSERT(antDE.startTime == startTime);
        CPPUNIT_ASSERT(antDE.stopTime == lastEventStopTime_m.value);
        CPPUNIT_ASSERT(antDE.delayTables.length() == 0);
        CPPUNIT_ASSERT(antDE.coarseDelays.length() == 0);
        CPPUNIT_ASSERT(antDE.fineDelays.length() == 0);
      }
    }
  }  

  void testApplyCableDelays(){
    const unsigned int MAX_NUM_ANTENNA(66);
    
    for (unsigned int loop = 0; loop < MAX_NUM_ANTENNA; loop++) {
      char name[5];
      sprintf(name,"DV%02d", loop);
      addAntenna(name);
      setAntennaCableDelay(name, drand48() * 60E-6);
      allocateDelayTable();

      /* Now zero the delay table */
      std::map<std::string, DelayServerData>::iterator iter;
      for (iter = antennaData_m.begin(); iter != antennaData_m.end(); iter++){
        for (unsigned int idx = 0; idx <=polynomialOrder_m; idx++){
          iter->second.geometricDelay[idx] = 0.0;
          iter->second.dryAtmDelay[idx] = 0.0;
          iter->second.wetAtmDelay[idx] = 0.0;
        }
        iter->second.calculateTotalDelay();
      }

      //applyNonGeometricDelays();
      for (iter = antennaData_m.begin(); iter != antennaData_m.end(); iter++){
        for (unsigned int idx = 0; idx <=polynomialOrder_m; idx++){
          CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE("Delay value incorrect",
                                               iter->second.antennaCableDelay,
                                               iter->second.totalDelay[idx],
                                               1E-12);
        }
      }
    }
  }

  void testPopulateTotalDelayEventOnce(){
    const unsigned int MAX_NUM_ANTENNA(66);
    const unsigned int MIN_POLYNOMIAL_ORDER(3);
    const unsigned int MAX_POLYNOMIAL_ORDER(7);
    const ACS::Time   startTime(134554839717600000LL);
    const ACS::Time   stopTime(134554840322400000LL);

   initializeServer("Array001");

   for (unsigned int loop = 0; loop < MAX_NUM_ANTENNA; loop++){
      char name[5];
      sprintf(name,"DV%02d", loop);
      addAntenna(name);
      setAntennaCableDelay(name, 0);

      for (unsigned int polyOrder = MIN_POLYNOMIAL_ORDER;
           polyOrder < MAX_POLYNOMIAL_ORDER; polyOrder++) {
        polynomialOrder_m = polyOrder;
        allocateDelayTable();

        for (unsigned int idx = 0; idx <=polynomialOrder_m; idx++){
          delayTime_m[idx] = startTime + (idx * delayEventDuration_m / 
                                          polynomialOrder_m);
        }

        unsigned int antIdx = 0;
        std::map<std::string, DelayServerData>::iterator iter;
        for (iter=antennaData_m.begin(); iter!=antennaData_m.end(); iter++){
          for (unsigned int idx = 0; idx <=polynomialOrder_m; idx++){
            iter->second.totalDelay[idx] = antIdx +(0.01 * idx);
          }
          antIdx++;
        }
        
        
        Control::ArrayDelayEvent ade = createArrayDelayEvent(startTime);
        populateTotalDelayEvent(ade, startTime, stopTime);
        
        for (unsigned int ant = 0; ant <= loop; ant++) {
          Control::AntennaDelayEvent antDE = ade.antennaDelayEvents[ant];
          CPPUNIT_ASSERT(antDE.delayTables.length() == 1);
          CPPUNIT_ASSERT(antDE.delayTables[0].startTime == startTime);
          CPPUNIT_ASSERT(antDE.delayTables[0].stopTime == stopTime);
          
          CPPUNIT_ASSERT(antDE.delayTables[0].delayValues.length() ==
                         polyOrder + 1);
          for (unsigned int idx = 0; idx < polyOrder + 1; idx++) {
            CPPUNIT_ASSERT(antDE.delayTables[0].delayValues[idx].time ==
                           delayTime_m[idx]);
            CPPUNIT_ASSERT_DOUBLES_EQUAL(ant + (0.01 * idx),
                 antDE.delayTables[0].delayValues[idx].totalDelay, 1E-12);
          }            
        }
      }
    }
  }

  void testPopulateTotalDelayEventRepeatedly(){
    const unsigned int NUM_DELAY_TABLES(4);
    const unsigned int NUM_ANTENNA(66);
    const ACS::Time   startTime(134554839717600000LL);
    const ACS::Time   stopTime(134554840322400000LL);
    
    ACS::Time interval = (stopTime - startTime)/ NUM_DELAY_TABLES;

    initializeServer("Array001");

    for (unsigned int loop = 0; loop < NUM_ANTENNA; loop++){
      char name[5];
      sprintf(name,"DV%02d", loop);
      addAntenna(name);
      setAntennaCableDelay(name, 0);
    }

    Control::ArrayDelayEvent ade = createArrayDelayEvent(startTime);

    for (unsigned int idx = 0; idx <= polynomialOrder_m; idx++){
      delayTime_m[idx] = startTime + (idx * delayEventDuration_m / 
                                      polynomialOrder_m);
    }

    for (unsigned int table = 0; table < NUM_DELAY_TABLES; table++) {
      unsigned int ant = 0;
      std::map<std::string, DelayServerData>::iterator iter;
      for (iter=antennaData_m.begin(); iter!=antennaData_m.end(); iter++){
        for (unsigned int idx = 0; idx <= polynomialOrder_m; idx++){
          iter->second.totalDelay[idx] = (100*table)+ant+(0.01 * idx);
        }
        ant++;
      }
      populateTotalDelayEvent(ade, startTime + (interval*table),
                              startTime + (interval* (table+1)));
    }

    unsigned int ant = 0;
    std::map<std::string, DelayServerData>::iterator iter;
    for (iter=antennaData_m.begin(); iter!=antennaData_m.end(); iter++){
      Control::AntennaDelayEvent antDE = ade.antennaDelayEvents[ant];
      CPPUNIT_ASSERT(antDE.delayTables.length() == NUM_DELAY_TABLES);
      for (unsigned int table = 0; table < NUM_DELAY_TABLES; table++) {
        CPPUNIT_ASSERT(antDE.delayTables[table].startTime == 
                       startTime + (interval*table));
        CPPUNIT_ASSERT(antDE.delayTables[table].stopTime == 
                       startTime + (interval * (table +1)));
          
        CPPUNIT_ASSERT(antDE.delayTables[table].delayValues.length() ==
                       polynomialOrder_m + 1);
        for (unsigned int idx = 0; idx < polynomialOrder_m + 1; idx++) {
          CPPUNIT_ASSERT(antDE.delayTables[table].delayValues[idx].time ==
                         delayTime_m[idx]);
          CPPUNIT_ASSERT_DOUBLES_EQUAL((100*table) + ant + (0.01 * idx),
                         antDE.delayTables[table].delayValues[idx].totalDelay,
                                    1E-13);

        }
      }
      ant++;
    }
  }

  void testPopulateQuantizedDelays1() {
    /* This test checks that given a delay table the course and fine
       delays are populated correctly. */
    const double coarseDelay(250E-12);

    initializeServer("Array001");

    addAntenna("DV01");
    setAntennaCableDelay("DV01",0.0);

    /* To make it simple use time near zero */
    ACS::Time startTime = 0;
    ACS::Time stopTime  = delayEventDuration_m; 
    
    //    allocateDelayTable();
    Control::ArrayDelayEvent ade = createArrayDelayEvent(startTime);

    for (unsigned int idx = 0; idx <= polynomialOrder_m; idx++){
      delayTime_m[idx] = startTime + (idx * delayEventDuration_m / 
                                      polynomialOrder_m);
    }

    /* Again making things simple use a delay ramp which corresponds
       to 6 coarse delay steps in one delayEventDuration */
    std::map<std::string, DelayServerData>::iterator iter;
    iter = antennaData_m.find("DV01");
    for (unsigned int idx = 0; idx <=polynomialOrder_m; idx++){
      iter->second.totalDelay[idx] =
        (3.0 * coarseDelay * delayTime_m[idx]/delayEventDuration_m);
    }
    causalityDelay_m = iter->second.totalDelay[polynomialOrder_m];

    populateQuantizedDelays(ade, startTime, startTime, stopTime );

    /* So we know that in this period the delay goes from 0 to
       exactly 6 coarse delay steps.  So there should be 97 fine
       delays and 7 coarse delays */
    Control::AntennaDelayEvent antDE = ade.antennaDelayEvents[0];
    CPPUNIT_ASSERT(antDE.fineDelays.length() == 49);
    CPPUNIT_ASSERT(antDE.coarseDelays.length() == 4);

    /* This is a linear ramp at a rate of 2.5E-14 sec/msec.
       Therefore we should get a trasition exactly every 1250 ms starting
       at 650.   The correction will count down starting at 48 (%8) samples

       Note: There is always the first value at relativeStartTime = 0
             In this case the fine delay should be (48%8=) 0.

       Jan 20 2010, Changing to 16th sample fine delay steps means that the
                    transitions should happen twice as fast. To avoid round
                    off problems I slow the rate to 1.25E-14 s/ms
    */
    CPPUNIT_ASSERT(antDE.fineDelays[0].relativeStartTime == 0);
    CPPUNIT_ASSERT(antDE.fineDelays[0].fineDelay == 0);

    for (unsigned int idx = 1; idx < antDE.fineDelays.length(); idx++) {
      CPPUNIT_ASSERT(antDE.fineDelays[idx].relativeStartTime ==
                     ((idx-1) * 1250) +625);
      CPPUNIT_ASSERT(antDE.fineDelays[idx].fineDelay == (48 - idx) % 16);
    }

    /* Now we need to check the coarse delays, the first one should be 
       at time 0, and have value 3.
       
       The fine delays start at 625 ms and happen every 1250 ms, so
       our first transition (3->2) should happen at 625 and then every
       16 * 1250 = 20000 ms after that
    */
    CPPUNIT_ASSERT(antDE.coarseDelays[0].relativeStartTime == 0);
    CPPUNIT_ASSERT(antDE.coarseDelays[0].coarseDelay == 3);

    for (unsigned int idx = 1; idx < antDE.coarseDelays.length(); idx++) {
      CPPUNIT_ASSERT(antDE.coarseDelays[idx].relativeStartTime ==
                     ((idx-1) * 20000) + 625);
      CPPUNIT_ASSERT(antDE.coarseDelays[idx].coarseDelay == 3 - idx);
    }
  }

  void testPopulateQuantizedDelays2() {
    /* Test correct behavior with multiple antennas */
    const double coarseDelay(250E-12);
    const unsigned int NUM_ANTENNA(66);

    initializeServer("Array001");
    
    for (unsigned int idx = 0; idx < NUM_ANTENNA; idx++) {
      char name[5];
      sprintf(name,"DV%02d", idx);
      addAntenna(name);
      setAntennaCableDelay(name, 10E-9 * idx);
    }

    /* To make it simple use time near zero */
    ACS::Time startTime = 0;
    ACS::Time stopTime  = delayEventDuration_m; 
    
    Control::ArrayDelayEvent ade = createArrayDelayEvent(startTime);

    
    for (unsigned int idx = 0; idx <= polynomialOrder_m; idx++){
      delayTime_m[idx] = startTime + (idx * delayEventDuration_m / 
                                      polynomialOrder_m);
    }
   

    /* Again making things simple use a delay ramp which corresponds
       to 6 coarse delay steps in one delayEventDuration */
    std::map<std::string, DelayServerData>::iterator iter;
    iter = antennaData_m.find("DV01");
    unsigned int ant = 0;
    for (iter = antennaData_m.begin(); iter != antennaData_m.end(); iter++) {
      for (unsigned int idx = 0; idx <=polynomialOrder_m; idx++){
        iter->second.totalDelay[idx] =
          (6.0 * ant * coarseDelay * delayTime_m[idx]/delayEventDuration_m);
      }
      ant++;
    }
            
    causalityDelay_m = 60E-6; /* Causality Delay at 60 us */

    populateQuantizedDelays(ade, startTime, startTime, stopTime);

    /* There should be exactly NUM_ANTENNAS antenna delay events
       in this event */
    CPPUNIT_ASSERT(ade.antennaDelayEvents.length() == NUM_ANTENNA);

    /* So we know that in this period the delay goes from 0 to
       exactly (6 * ant) coarse delay steps.  So there should be 
       (48 * ant) + 1 fine delays and (6 * ant) + 1 coarse delays 

       Jan 20, 2010 With the change to 16th sample steps this number of
                    fine delays should be (96 * ant) +1 
    */
    for (unsigned int ant = 0; ant < NUM_ANTENNA; ant++) {
      Control::AntennaDelayEvent antDE = ade.antennaDelayEvents[ant];
      CPPUNIT_ASSERT(antDE.fineDelays.length() == (96*ant) + 1);
      /* Once we are at or above 16 antennas we have trouble with two
         updates in the first TE */
      if (ant < 7) {
        CPPUNIT_ASSERT(antDE.coarseDelays.length() == (6 * ant) + 1);
      } else {
        CPPUNIT_ASSERT(antDE.coarseDelays.length() == (6 * ant));
      }

      /* This is a linear ramp at a rate of 2.5E-14 * ant sec/sec.

         The coarse delay accounts for a offset of 1920000 fine delays
         (240000 coarse).  
      
         Note: There is always the first value at relativeStartTime = 0
         In this case the fine delay should be (48%8=) 0.
      */
       CPPUNIT_ASSERT(antDE.fineDelays[0].relativeStartTime == 0);
       CPPUNIT_ASSERT(antDE.fineDelays[0].fineDelay == 0);
    
      if (ant < 7) {
        CPPUNIT_ASSERT(antDE.coarseDelays[0].relativeStartTime == 0);
        CPPUNIT_ASSERT(antDE.coarseDelays[0].coarseDelay == 240000);
      } else {
        CPPUNIT_ASSERT(antDE.coarseDelays[0].relativeStartTime != 0);
        CPPUNIT_ASSERT(antDE.coarseDelays[0].coarseDelay != 240000);
      }
    }
  }

  void testCatchingMultipleCoarseDelays() {
    /* This test ensures that we get at most one coarse delay event
       per 48ms */
    
    const double coarseDelay(250E-12);

    initializeServer("Array001");
    
    addAntenna("DV01");
    
    /* To make it simple use time near zero */
    ACS::Time startTime = 0;
    ACS::Time stopTime  = delayEventDuration_m; 
    
    Control::ArrayDelayEvent ade = createArrayDelayEvent(startTime);

    
    for (unsigned int idx = 0; idx <= polynomialOrder_m; idx++){
      delayTime_m[idx] = startTime + (idx * delayEventDuration_m / 
                                      polynomialOrder_m);
    }
   

    /* Use a ramp with a coarse delay step every 36 ms:
       250E-9 / 0.036
    */
    std::map<std::string, DelayServerData>::iterator iter;
    iter = antennaData_m.find("DV01");
    for (unsigned int idx = 0; idx <=polynomialOrder_m; idx++){
        iter->second.totalDelay[idx] =
          (coarseDelay * delayTime_m[idx] / 360000);
      }

    populateQuantizedDelays(ade, startTime, startTime, stopTime);

    /* There should be exactly 1 antenna delay events
       in this event */
    CPPUNIT_ASSERT(ade.antennaDelayEvents.length() == 1);

    Control::AntennaDelayEvent antDE = ade.antennaDelayEvents[0];

    /* We know that the first TE has two events check that we are in 
       fact taking the second one (not the first)*/
    CPPUNIT_ASSERT(antDE.coarseDelays[0].relativeStartTime != 0);

    for (unsigned int idx = 1; idx < antDE.coarseDelays.length(); idx++) {
      /* We want to fail if there are two coarse delays in the same timing
         event. */
      CPPUNIT_ASSERT(antDE.coarseDelays[idx].relativeStartTime/48 !=
                     antDE.coarseDelays[idx-1].relativeStartTime/48);
    }
  }

  void testPopulateQuantizedDelays3() {
    /* Test correct behavior with multiple delay times */
    


  }

  void timePopulateQuantizedDelays() {
    /* Test correct behavior with multiple antennas */
    const unsigned int NUM_ANTENNA(66);

    initializeServer("Array001");
    
    for (unsigned int idx = 0; idx < NUM_ANTENNA; idx++) {
      char name[5];
      sprintf(name,"DV%02d", idx);
      addAntenna(name);
      setAntennaCableDelay(name, 10E-9 * idx);
    }


    /* To make it simple use time near zero */
    ACS::Time startTime = 0;
    
    Control::ArrayDelayEvent ade = createArrayDelayEvent(startTime);

    causalityDelay_m = 60E-6; /* Causality Delay at 60 us */
  
  
    for (unsigned int idx = 0; idx <= polynomialOrder_m; idx++){
      delayTime_m[idx] = startTime + (idx * delayEventDuration_m / 
                                      polynomialOrder_m);
    }
   
    /* Here we want to make this as hard as possible, for a 15 km 
       baseline observing a maximally moving source the maximum rate 
       of delay is: (15km/c) * (omega_e) = 5E-5 * (2Pi/86400.0)
       Delay time is in ACS units so the 5E-5/1E7 = 5E-12
    */
    std::map<std::string, DelayServerData>::iterator iter;
    for (iter = antennaData_m.begin(); iter != antennaData_m.end(); iter++) {
      for (unsigned int idx = 0; idx <=polynomialOrder_m; idx++){
        iter->second.totalDelay[idx] =
          delayTime_m[idx] * 5E-12 * 2 * M_PI / 86400.0;
      }
    }
           
    std::cout << "@\n@Timing Test Results:" << std::endl;
    delayEventDuration_m = 100000000L;
    while (delayEventDuration_m <= 60E7) {
      ACS::Time stopTime  = delayEventDuration_m; 

      struct timeval start_tv;
      struct timeval stop_tv;
      gettimeofday(&start_tv, NULL);
      populateQuantizedDelays(ade, startTime, startTime, stopTime );
      gettimeofday(&stop_tv, NULL);

      double elapsed = (stop_tv.tv_sec - start_tv.tv_sec) +
        ((stop_tv.tv_usec - start_tv.tv_usec)*1E-6);

      std::cout << "@\tEvent duration: "  << (delayEventDuration_m/1.0E7) 
                <<" s\tElapsed Time: " << elapsed << std::endl;
      delayEventDuration_m += 100000000L;
    }
  }

   void testPublishSingleDelayEvent() {
    initializeServer("Array001");

    addAntenna("DV01");
    setAntennaCableDelay("DV01", 10E-9);
    
    ACS::Time delayStartTime = lastEventStopTime_m.value;
    publishSingleDelayEvent();
    ACS::Time delayEndTime = lastEventStopTime_m.value;

    CPPUNIT_ASSERT(delayStartTime % 480000 == 0);
    CPPUNIT_ASSERT(delayEndTime % 480000 == 0);
    CPPUNIT_ASSERT(lastEventPublished.antennaDelayEvents.length() == 1);
    Control::AntennaDelayEvent  ade = lastEventPublished.antennaDelayEvents[0];
    CPPUNIT_ASSERT(ade.startTime == delayStartTime);
    CPPUNIT_ASSERT(ade.stopTime == delayEndTime);
    CPPUNIT_ASSERT(!strncmp("DV01", ade.antennaName, 4));
    CPPUNIT_ASSERT(ade.delayTables.length() == 1);
    CPPUNIT_ASSERT(ade.fineDelays.length() == 1);
    CPPUNIT_ASSERT(ade.coarseDelays.length() == 1);    
  }
  
  void testAtmosphericModels() {
    const unsigned int NUM_ANTENNA(66);
    CPPUNIT_ASSERT_EQUAL(getAtmosphericDelayModelType(),
                         Control::ZERO_ATMOSPHERE_MODEL);

    for (unsigned int idx = 0; idx < NUM_ANTENNA; idx++) {
      char name[5];
      sprintf(name,"DV%02d", idx);
      addAntenna(name);
      setAntennaCableDelay(name,0);
    }

    /* Now Zero everything but the Atmospheric Terms */
    unsigned int antennaNumber = 0;
    std::map<std::string, DelayServerData>::iterator iter;
    for (iter = antennaData_m.begin(); iter != antennaData_m.end(); iter++) {
      antennaNumber++;
      for (unsigned int idx = 0; idx <=polynomialOrder_m; idx++){
        iter->second.geometricDelay[idx] = 0.0;
        iter->second.dryAtmDelay[idx] = (antennaNumber*100) + idx;
        iter->second.wetAtmDelay[idx] = (antennaNumber*.01) + (idx*0.0001);
      }
    }

    setAtmosphericDelayModelType(Control::CALC_WET_AND_DRY);
    CPPUNIT_ASSERT_EQUAL(getAtmosphericDelayModelType(),
                         Control::CALC_WET_AND_DRY);
    
    /* Apply the Model */
    atmDelayModel_p->calculateAtmosphericModel(antennaData_m);
    
    antennaNumber = 0;
    for (iter = antennaData_m.begin(); iter != antennaData_m.end(); iter++) {
      antennaNumber++;
      iter->second.calculateTotalDelay();
      for (unsigned int idx = 0; idx <=polynomialOrder_m; idx++){
        CPPUNIT_ASSERT_EQUAL(iter->second.geometricDelay[idx], 0.0);
        CPPUNIT_ASSERT_DOUBLES_EQUAL(iter->second.dryAtmDelay[idx],
                                     (antennaNumber*100) + idx, 1E-6);
        CPPUNIT_ASSERT_DOUBLES_EQUAL(iter->second.wetAtmDelay[idx],
                                     (antennaNumber*.01)+(idx*0.0001), 1E-6);
        CPPUNIT_ASSERT_DOUBLES_EQUAL(iter->second.totalDelay[idx],
                                     (antennaNumber*100.01)+(idx*1.0001),
                                     1E-6);
      }
    }

    setAtmosphericDelayModelType(Control::CALC_DRY_ONLY);
    CPPUNIT_ASSERT_EQUAL(getAtmosphericDelayModelType(),
                         Control::CALC_DRY_ONLY);
    
    /* Apply the Model */
    atmDelayModel_p->calculateAtmosphericModel(antennaData_m);
    
    antennaNumber = 0;
    for (iter = antennaData_m.begin(); iter != antennaData_m.end(); iter++) {
      antennaNumber++;
      iter->second.calculateTotalDelay();
      for (unsigned int idx = 0; idx <=polynomialOrder_m; idx++){
        CPPUNIT_ASSERT_EQUAL(iter->second.geometricDelay[idx], 0.0);
        CPPUNIT_ASSERT_DOUBLES_EQUAL(iter->second.dryAtmDelay[idx],
                                     (antennaNumber*100) + idx, 1E-6);
        CPPUNIT_ASSERT_EQUAL(iter->second.wetAtmDelay[idx], 0.0);
        CPPUNIT_ASSERT_DOUBLES_EQUAL(iter->second.totalDelay[idx],
                                     (antennaNumber*100)+idx, 1E-6);
      }
    }

    setAtmosphericDelayModelType(Control::ZERO_ATMOSPHERE_MODEL);
    CPPUNIT_ASSERT_EQUAL(getAtmosphericDelayModelType(),
                         Control::ZERO_ATMOSPHERE_MODEL);
    
    /* Apply the Model */
    atmDelayModel_p->calculateAtmosphericModel(antennaData_m);
    
    antennaNumber = 0;
    for (iter = antennaData_m.begin(); iter != antennaData_m.end(); iter++) {
      antennaNumber++;
      iter->second.calculateTotalDelay();
      for (unsigned int idx = 0; idx <=polynomialOrder_m; idx++){
        CPPUNIT_ASSERT_EQUAL(iter->second.geometricDelay[idx], 0.0);
        CPPUNIT_ASSERT_EQUAL(iter->second.dryAtmDelay[idx],0.0);
        CPPUNIT_ASSERT_EQUAL(iter->second.wetAtmDelay[idx], 0.0);
        CPPUNIT_ASSERT_EQUAL(iter->second.totalDelay[idx], 0.0);
      }
    }
  }

  void testSettingAtmosphericDelayModels() {
    // Test inserting a model for a nonexistent antenna
    {
      Control::DelayServerBase::AntennaDelayModelSeq modelSeq;
      modelSeq.length(2);
      modelSeq[0].antennaId = CORBA::string_dup("DV01");
      modelSeq[0].zenithDryDelay = 1.0;
      modelSeq[0].zenithWetDelay = 1.0;
      
      modelSeq[1].antennaId = CORBA::string_dup("PM03");
      modelSeq[1].zenithDryDelay = 1.0;
      modelSeq[1].zenithWetDelay = 1.0;
      
      setAtmosphericDelayModel(::getTimeStamp(), modelSeq);
    }

    // Test getting models even though none are set
    {
      Control::DelayServerBase::AntennaDelayModelSeq_var
        modelSeq(getCurrentAtmosphericDelayModel());
      CPPUNIT_ASSERT_EQUAL(static_cast<int>(modelSeq->length()), 0);
    }

    // Need to add some antennas
    addAntenna("DV01");
    addAntenna("DV02");
    addAntenna("PM03");
    addAntenna("DA41");
    
    // Test inserting 3 models (past, current, future)
    {
      ACS::Time currentTime = ::getTimeStamp();
      ACS::Time pastTime    = currentTime - static_cast<ACS::Time>(6E8);
      ACS::Time futureTime  = currentTime + static_cast<ACS::Time>(6E8);

      Control::DelayServerBase::AntennaDelayModelSeq modelSeq;
      modelSeq.length(2);
      modelSeq[0].antennaId = CORBA::string_dup("DV01");
      modelSeq[1].antennaId = CORBA::string_dup("DA41");

      modelSeq[0].zenithDryDelay = 1.0;
      modelSeq[0].zenithWetDelay = 1.0;
      modelSeq[1].zenithDryDelay = 1.75;
      modelSeq[1].zenithWetDelay = 1.76;
      setAtmosphericDelayModel(pastTime, modelSeq);

      modelSeq[1].antennaId = CORBA::string_dup("PM03");
      modelSeq[0].zenithDryDelay = 2.0;
      modelSeq[0].zenithWetDelay = 2.1;
      modelSeq[1].zenithDryDelay = 2.5;
      modelSeq[1].zenithWetDelay = 2.6;
      setAtmosphericDelayModel(currentTime, modelSeq);

      modelSeq[0].antennaId = CORBA::string_dup("DV02");
      modelSeq[0].zenithDryDelay = 3.0;
      modelSeq[0].zenithWetDelay = 3.0;
      modelSeq[1].zenithDryDelay = 3.5;
      modelSeq[1].zenithWetDelay = 3.5;
      setAtmosphericDelayModel(futureTime, modelSeq);

      Control::DelayServerBase::AntennaDelayModelSeq_var
        retSeq(getCurrentAtmosphericDelayModel());

      CPPUNIT_ASSERT(retSeq->length() == 3);
      for (unsigned int idx = 0; idx < retSeq->length(); idx++) {
        if (!strcmp(retSeq[idx].antennaId,"DV01")) {
          CPPUNIT_ASSERT_EQUAL(retSeq[idx].zenithDryDelay, 2.0);
          CPPUNIT_ASSERT_EQUAL(retSeq[idx].zenithWetDelay, 2.1);
        } else if (!strcmp(retSeq[idx].antennaId,"DA41")) {
          CPPUNIT_ASSERT_EQUAL(retSeq[idx].zenithDryDelay, 1.75);
          CPPUNIT_ASSERT_EQUAL(retSeq[idx].zenithWetDelay, 1.76);
        } else if (!strcmp(retSeq[idx].antennaId,"PM03")) {
          CPPUNIT_ASSERT_EQUAL(retSeq[idx].zenithDryDelay, 2.5);
          CPPUNIT_ASSERT_EQUAL(retSeq[idx].zenithWetDelay, 2.6);
        } else {
          CPPUNIT_FAIL("Unrecognized Antenna Returned");
        }
      }
    }
  }

  void testSettingAtmosphericDelayModelsASDM() {
    asdm::ASDM asdm;
    asdm::CalWVRTable& wvrTable = asdm.getCalWVR();
    
    addCalWVRRow(wvrTable, "DV01", 1.0, 2.0);
    addCalWVRRow(wvrTable, "DA41", 1.5, 2.5);

    // Need to add some antennas
    addAntenna("DV01");
    addAntenna("DA41");
    
    setAtmosphericDelayModelASDM(1000, *(wvrTable.toIDL()));
    
    Control::DelayServerBase::AntennaDelayModelSeq_var
      retSeq(getCurrentAtmosphericDelayModel());

    CPPUNIT_ASSERT(retSeq->length() == 2);
    for (unsigned int idx = 0; idx < retSeq->length(); idx++) {
      if (!strcmp(retSeq[idx].antennaId,"DV01")) {
        CPPUNIT_ASSERT_EQUAL(retSeq[idx].zenithDryDelay, 1.0);
        CPPUNIT_ASSERT_EQUAL(retSeq[idx].zenithWetDelay, 2.0);
      } else if (!strcmp(retSeq[idx].antennaId,"DA41")) {
        CPPUNIT_ASSERT_EQUAL(retSeq[idx].zenithDryDelay, 1.5);
        CPPUNIT_ASSERT_EQUAL(retSeq[idx].zenithWetDelay, 2.5);
      } else {
        CPPUNIT_FAIL("Unrecognized Antenna Returned");
      }
    }
  }

  void testPolynomialEvaluation() {
    std::vector<float> polynomial;

    polynomial.push_back(0.5);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(evaluatePolynomial(polynomial), 0.5, 1E-9);
    
    polynomial.push_back(0.25);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(evaluatePolynomial(polynomial), 0.5, 1E-9);

    polynomial.push_back(0.25);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(evaluatePolynomial(polynomial), 0.25, 1E-9);

    polynomial.push_back(0.125);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(evaluatePolynomial(polynomial), 0.25, 1E-9);

    polynomial.push_back(0.125);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(evaluatePolynomial(polynomial), 0.375, 1E-9);

  }

  void testNewSourceAdded(){};


  void testSetDelayEventDuration(){}
  //  void getDelayEventDuration();

  //void setPolynomialOrder(CORBA::Long newOrder);  
  //void getPolynomialOrder();

  /* Include testing the effect of the deformation model */
  // void enableAntennaModel(bool enableAntennaModel);
  // bool antennaModelEnabled() ;

     
  // void setCausalityDelay(CORBA::Double newDelay);
  //void getCausalityDelay();

  CPPUNIT_TEST_SUITE(DelayServerBaseImplTest);
  CPPUNIT_TEST(testinitializeServer);
  CPPUNIT_TEST(testAddingAnAntenna);
  CPPUNIT_TEST(testPadLocationInterface);
  CPPUNIT_TEST(testAntennaVectorInterface);
  CPPUNIT_TEST(testKTermInterface);
  CPPUNIT_TEST(testGenerateDelayEventExecution);
  CPPUNIT_TEST(testCreateArrayDelayEvent);
  CPPUNIT_TEST(testSettingCableDelays);
  CPPUNIT_TEST(testApplyCableDelays);
  CPPUNIT_TEST(testPopulateTotalDelayEventOnce);
  CPPUNIT_TEST(testPopulateTotalDelayEventRepeatedly);
  CPPUNIT_TEST(testPopulateQuantizedDelays1);
  CPPUNIT_TEST(testPopulateQuantizedDelays2);
  CPPUNIT_TEST(testCatchingMultipleCoarseDelays);
  CPPUNIT_TEST(timePopulateQuantizedDelays);
  CPPUNIT_TEST(testPublishSingleDelayEvent);
  CPPUNIT_TEST(testAtmosphericModels);
  CPPUNIT_TEST(testSettingAtmosphericDelayModels);
  CPPUNIT_TEST(testSettingAtmosphericDelayModelsASDM);
  CPPUNIT_TEST(testPolynomialEvaluation);
  CPPUNIT_TEST_SUITE_END();
 
  void publishDelayEvent(const Control::ArrayDelayEvent& event){
    lastEventPublished = event;
  }
  /* ---------------- Overridden functions ------------ */
  virtual ACS::Time fillDelayTable(ACS::Time startTime) {
    std::map<std::string, DelayServerData>::iterator iter;
    for (iter = antennaData_m.begin(); iter != antennaData_m.end(); iter++) {
      for (unsigned int idx = 0; idx < polynomialOrder_m+1; idx++){
        iter->second.geometricDelay[idx] = 0.0;
        iter->second.azDirection[idx] = 1.0;
        iter->second.elDirection[idx] = -1.0;
      }
    }
    return startTime + delayEventDuration_m;
  }

  virtual void generateDelayEvent() {
    if (!interceptGenerateDelayEvent) {
      DelayServerBaseImpl::generateDelayEvent();
    } 
  }

  virtual DelayServerData getAntennaData(const char* antennaId) {
    DelayServerData newData;
    newData.antennaName   = antennaId;
    newData.padName       = "PadId";

    newData.antennaVector[0] = 0.0;
    newData.antennaVector[1] = 0.0;
    newData.antennaVector[2] = 0.0;

    newData.padVector[0] = 1.0;
    newData.padVector[1] = 1.0;
    newData.padVector[2] = 1.0;

    newData.offsetVector[0] = 0.0;
    newData.offsetVector[1] = 0.0;
    newData.offsetVector[2] = 0.0;

    newData.antennaCableDelay = 1E-9;
    newData.pressure = 0.0;
    newData.setPolynomialOrder(polynomialOrder_m);
    return newData;
  }

private:
  /* ---------------- Utility Functions -------------------- */
  bool interceptGenerateDelayEvent;
  Control::ArrayDelayEvent   lastEventPublished;
  
  
  void addCalWVRRow(asdm::CalWVRTable& wvrTable, string antennaName, 
                    double dryDelay, double wetDelay){
    asdm::CalWVRRow* newRow = wvrTable.newRow();

    newRow->setWvrMethod(WVRMethodMod::ATM_MODEL);
    newRow->setAntennaName(antennaName);
    vector<float> dryPathVector; dryPathVector.push_back(dryDelay);
    vector<float> wetPathVector; wetPathVector.push_back(wetDelay);

    newRow->setDryPath(dryPathVector);
    newRow->setWetPath(wetPathVector);

    wvrTable.add(newRow);
  }; 

  void getWeatherData() {
    std::map<std::string, DelayServerData>::iterator iter;
    for (iter = antennaData_m.begin(); iter != antennaData_m.end(); iter++) {
      iter->second.pressure = 101325; // Standard Pressure
    }
  }

public:
  /* Because the POA defines them we need these functions even though 
     they are never actually used in the tests */
  virtual char* name() {
    return "Bogus Name";
  }

  virtual ACS::ComponentStates componentState() {
    return ACS::COMPSTATE_NEW;
  }
}; // End of Test Class

int main( int argc, char **argv)
{

  maci::SimpleClient m_client;
  m_client.init(argc,argv);
  m_client.login();
  ACSAlarmSystemInterfaceFactory::init(ACSAlarmSystemInterfaceFactory::getManager());
  CppUnit::TextUi::TestRunner runner;
  runner.addTest( DelayServerBaseImplTest::suite() );
  runner.run();
  m_client.logout();
  m_client.disconnect();

  return 0;
}
