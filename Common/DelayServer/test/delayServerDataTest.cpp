/* @(#) $Id: testRtFindKernelModules.cpp,v 1.4 2006/08/31 18:37:38 ramestic Exp
$
 *
 * Copyright (C) 2006
 * Associated Universities, Inc. Washington DC, USA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * Correspondence concerning ALMA Software should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */


#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestCaller.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#include <ControlExceptions.h>
#include <acstime.h>

#include <delayServerData.h>
class DelayServerDataTest : public CppUnit::TestFixture,
                        public DelayServerData
{
public:
  DelayServerDataTest() : CppUnit::TestFixture(),
                          DelayServerData(){}
  DelayServerDataTest( const DelayServerDataTest &toCopy){*this=toCopy;}
  DelayServerDataTest &operator=(const DelayServerDataTest&)
     {return *this;}
  ~DelayServerDataTest(){};

  //void setUp() {
  // }

  void testInstanciation() {
    /* Check that the default values are set correctly */
    CPPUNIT_ASSERT(antennaVector.size() == 3);
    CPPUNIT_ASSERT(padVector.size() == 3);
    CPPUNIT_ASSERT(offsetVector.size() == 3);
    CPPUNIT_ASSERT(geometricDelay.size() == 0);
    CPPUNIT_ASSERT(dryAtmDelay.size() == 0);
    CPPUNIT_ASSERT(wetAtmDelay.size() == 0);
    CPPUNIT_ASSERT(azDirection.size() == 0);
    CPPUNIT_ASSERT(elDirection.size() == 0);
    CPPUNIT_ASSERT(totalDelay.size() == 0);
    CPPUNIT_ASSERT(polynomialOrder_m == 0);
    CPPUNIT_ASSERT(atmosphericModel_m.size() == 0);
  }

  void testSettingPolynomialOrder() {
    setPolynomialOrder(4);
    CPPUNIT_ASSERT(polynomialOrder_m == 4);
    CPPUNIT_ASSERT(antennaVector.size() == 3);
    CPPUNIT_ASSERT(padVector.size() == 3);
    CPPUNIT_ASSERT(offsetVector.size() == 3);
    CPPUNIT_ASSERT(geometricDelay.size() == 5);
    CPPUNIT_ASSERT(dryAtmDelay.size() == 5);
    CPPUNIT_ASSERT(wetAtmDelay.size() == 5);
    CPPUNIT_ASSERT(azDirection.size() == 5);
    CPPUNIT_ASSERT(elDirection.size() == 5);
    CPPUNIT_ASSERT(totalDelay.size() == 5);
    CPPUNIT_ASSERT(atmosphericModel_m.size() == 0);
  }

  void testInsertingAtmosphericModels() {
    ACS::Time modelBaseTime = static_cast<ACS::Time>(::getTimeStamp() + 1E10);

    AtmosphericModel model1 = {modelBaseTime+1000, 1.0, 1.0};
    AtmosphericModel model2 = {modelBaseTime+2000, 2.0, 2.0};
    AtmosphericModel model3 = {modelBaseTime+1500, 1.5, 1.5};
    AtmosphericModel model4 = {modelBaseTime+ 500, 0.5, 0.5};
    AtmosphericModel model5 = {modelBaseTime+ 500, 2.5, 2.5};

    addAtmosphericModel(model1);
    CPPUNIT_ASSERT(atmosphericModel_m.size() == 1);
    CPPUNIT_ASSERT(modelsEqual(model1, atmosphericModel_m[0]));

    addAtmosphericModel(model2);
    CPPUNIT_ASSERT(atmosphericModel_m.size() == 2);
    CPPUNIT_ASSERT(modelsEqual(model1, atmosphericModel_m[0]));
    CPPUNIT_ASSERT(modelsEqual(model2, atmosphericModel_m[1]));

    addAtmosphericModel(model3);
    CPPUNIT_ASSERT(atmosphericModel_m.size() == 2);
    CPPUNIT_ASSERT(modelsEqual(model1, atmosphericModel_m[0]));
    CPPUNIT_ASSERT(modelsEqual(model3, atmosphericModel_m[1]));    

    addAtmosphericModel(model4);
    CPPUNIT_ASSERT(atmosphericModel_m.size() == 1);
    CPPUNIT_ASSERT(modelsEqual(model4, atmosphericModel_m[0]));

    addAtmosphericModel(model5);
    CPPUNIT_ASSERT(atmosphericModel_m.size() == 1);
    CPPUNIT_ASSERT(modelsEqual(model5, atmosphericModel_m[0]));
  }

  void testGettingModelIter() {
    ACS::Time modelBaseTime = static_cast<ACS::Time>(::getTimeStamp() + 1E10);

    CPPUNIT_ASSERT(getAtmosphericModelIter(500) == atmosphericModel_m.end());

    AtmosphericModel model1 = {modelBaseTime+1000, 1.0, 1.0};
    AtmosphericModel model2 = {modelBaseTime+2000, 2.0, 2.0};

    addAtmosphericModel(model1);
    addAtmosphericModel(model2);

    CPPUNIT_ASSERT(getAtmosphericModelIter(modelBaseTime+500) == 
                   atmosphericModel_m.end());
    CPPUNIT_ASSERT(modelsEqual(*getAtmosphericModelIter(modelBaseTime+1000),
                               model1));
    CPPUNIT_ASSERT(modelsEqual(*getAtmosphericModelIter(modelBaseTime+1500),
                               model1));
    CPPUNIT_ASSERT(modelsEqual(*getAtmosphericModelIter(modelBaseTime+2000),
                               model2));
    CPPUNIT_ASSERT(modelsEqual(*getAtmosphericModelIter(modelBaseTime+2500),
                               model2));
  }

  void testGettingAtmosphericModel(){
    ACS::Time modelBaseTime = static_cast<ACS::Time>(::getTimeStamp() + 1E10);

    CPPUNIT_ASSERT_THROW(getAtmosphericModel(1000),
                         ControlExceptions::InvalidRequestExImpl);

     AtmosphericModel model1 = {modelBaseTime+1000, 1.0, 1.0};
     AtmosphericModel model2 = {modelBaseTime+2000, 2.0, 2.0};
     addAtmosphericModel(model1);
     addAtmosphericModel(model2);

     CPPUNIT_ASSERT(modelsEqual(getAtmosphericModel(modelBaseTime+1000),
                                model1));
     CPPUNIT_ASSERT(modelsEqual(getAtmosphericModel(modelBaseTime+1500),
                                model1));
     CPPUNIT_ASSERT(modelsEqual(getAtmosphericModel(modelBaseTime+2000),
                                model2));
     CPPUNIT_ASSERT(modelsEqual(getAtmosphericModel(modelBaseTime+2500),
                                model2));    

     CPPUNIT_ASSERT_THROW(getAtmosphericModel(modelBaseTime+500),
                          ControlExceptions::InvalidRequestExImpl);
  }

  void testRemovalOfOldModels() {
    
    AtmosphericModel model1 = {1000, 1.0, 1.0};
    AtmosphericModel model2 = {2000, 2.0, 2.0};
    AtmosphericModel model3 = {::getTimeStamp(), 3.0, 3.0};
    AtmosphericModel model4 = {static_cast<ACS::Time>(::getTimeStamp()+1E8),
                               4.0, 4.0};

    addAtmosphericModel(model1);
    addAtmosphericModel(model2);

    /* These are way in the past, since we're using #2 it should stick 
       around but #1 should be gone */
    CPPUNIT_ASSERT(atmosphericModel_m.size() == 1);
    CPPUNIT_ASSERT(modelsEqual(model2, atmosphericModel_m[0]));

    /* Now add one for the current time */
    addAtmosphericModel(model3);
    CPPUNIT_ASSERT(atmosphericModel_m.size() == 2);
    CPPUNIT_ASSERT(modelsEqual(model2, atmosphericModel_m[0]));
    CPPUNIT_ASSERT(modelsEqual(model3, atmosphericModel_m[1]));

    addAtmosphericModel(model4);
    CPPUNIT_ASSERT(atmosphericModel_m.size() == 3);
    CPPUNIT_ASSERT(modelsEqual(model2, atmosphericModel_m[0]));
    CPPUNIT_ASSERT(modelsEqual(model3, atmosphericModel_m[1]));
    CPPUNIT_ASSERT(modelsEqual(model4, atmosphericModel_m[2]));
  }


  CPPUNIT_TEST_SUITE(DelayServerDataTest);
  CPPUNIT_TEST(testInstanciation);
  CPPUNIT_TEST(testSettingPolynomialOrder);
  CPPUNIT_TEST(testInsertingAtmosphericModels);
  CPPUNIT_TEST(testGettingModelIter);
  CPPUNIT_TEST(testGettingAtmosphericModel);
  CPPUNIT_TEST(testRemovalOfOldModels);
  CPPUNIT_TEST_SUITE_END();

private:
  /* ---------------- Utility Functions -------------------- */
  bool modelsEqual(AtmosphericModel ref, AtmosphericModel test) {
    if (ref.applicationTime != test.applicationTime) {
      std::cerr << "Application time mismatch:" << std::endl
                << "\tReference: " << ref.applicationTime << std::endl
                << "\tTest:      " << test.applicationTime << std::endl;
      return false;
    }

    if (fabs(ref.zenithDryDelay - test.zenithDryDelay) > 1E-12) {
      std::cerr << "Application time mismatch:" << std::endl
                << "\tReference: " << ref.zenithDryDelay << std::endl
                << "\tTest:      " << test.zenithDryDelay << std::endl;
      return false;
    }

    if (fabs(ref.zenithWetDelay - test.zenithWetDelay) > 1E-12) {
      std::cerr << "Application time mismatch:" << std::endl
                << "\tReference: " << ref.zenithWetDelay << std::endl
                << "\tTest:      " << test.zenithWetDelay << std::endl;
      return false;
    }
    return true;
  }

  void printModelDeque() {
    std::cout << "\nSize of Model Deque: " << atmosphericModel_m.size() 
              << std::endl;
    std::deque<AtmosphericModel>::iterator iter = atmosphericModel_m.begin();
    for (;iter != atmosphericModel_m.end(); iter++) {
      std::cout << "\t" << iter->applicationTime
                << "\t" << iter->zenithDryDelay
                << "\t" << iter->zenithWetDelay << std::endl;
    }
  }
}; // End of Test Class

int main( int argc, char **argv)
{
  CppUnit::TextUi::TestRunner runner;
  runner.addTest( DelayServerDataTest::suite() );
  runner.run();
  return 0;
}
