/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2006-05-24  created 
*/

/************************************************************************
*   NAME
*   
* 
*   SYNOPSIS
*
*   
*   PARENT CLASS
*
* 
*   DESCRIPTION
*
*
*   PUBLIC METHODS
*
*
*   PUBLIC DATA MEMBERS
*
*
*   PROTECTED METHODS
*
*
*   PROTECTED DATA MEMBERS
*
*
*   PRIVATE METHODS
*
*
*   PRIVATE DATA MEMBERS
*
*
*   FILES
*
*   ENVIRONMENT
*
*   COMMANDS
*
*   RETURN VALUES
*
*   CAUTIONS 
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS   
* 
*------------------------------------------------------------------------
*/


static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <delayReceiver.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <maciSimpleClient.h>

class DelayReceiverTest :  public DelayReceiver {
public:
  DelayReceiverTest(CORBA::ORB_ptr orbRef_p):
    DelayReceiver(orbRef_p){};
    
  virtual ~DelayReceiverTest(){};

  void eventHandler(Control::ArrayDelayEvent* eventIn) {
    fprintf(stdout,"Event Received");
  }
};

int main(int argc, char** argv) {
  struct stat buf;
  SimpleClient client;
  client.init(argc, argv);
  client.login();

  fprintf(stdout,"Now starting delayReceiverTest\n");
  DelayReceiverTest testReceiver(client.getORB());

  testReceiver.assignArray("array001");
  ACE_Time_Value tv(1);

  while (stat("exit.flag", &buf)) {
    client.run(tv)
  }

  client.disconnect();
  unlink("exit.flag");
}
/*___oOo___*/
