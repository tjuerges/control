/* @(#) $Id: testRtFindKernelModules.cpp,v 1.4 2006/08/31 18:37:38 ramestic Exp
$
 *
 * Copyright (C) 2006
 * Associated Universities, Inc. Washington DC, USA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * Correspondence concerning ALMA Software should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */

#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestCaller.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#include <calcDataManager.h>

// These two times must match
#define IERSMJD 55521
#define ACSTIMEREF 135095902801000000LL
//#define IERSMJD 55526
//#define ACSTIMEREF 135100222801000000LL


class CalcDataManagerTest : public CppUnit::TestFixture,
                            public CalcDataManager
{
public:
  CalcDataManagerTest() : CppUnit::TestFixture(),
                          CalcDataManager(){}
  CalcDataManagerTest( const CalcDataManagerTest &toCopy){*this=toCopy;}
  CalcDataManagerTest &operator=(const CalcDataManagerTest&)
     {return *this;}
  ~CalcDataManagerTest(){};

    void setUp() {
      setCalcEnvironment();
    }

  void testInstanciation() {
    /* Check that the default values are set correctly */
    CPPUNIT_ASSERT_MESSAGE("Default Reference incorrect",
                           calcArgs_m.b_x == 0 &&  calcArgs_m.b_y == 0 &&
                           calcArgs_m.b_z == 0 &&  calcArgs_m.axis_off_b == 0);
    CPPUNIT_ASSERT_MESSAGE("Polynomial Order is not correctly defaulted",
                           polynomialOrder_m == 4);
    CPPUNIT_ASSERT_MESSAGE("Default Pressures set Incorrectly",
                           calcArgs_m.pressure_a == 0.0 && 
                           calcArgs_m.pressure_b == 0.0);
    CPPUNIT_ASSERT_MESSAGE("Telescope Mount Type set Incorrectly",
                           strcmp(calcArgs_m.axis_type_a,"ALTZ") == 0 &&
                           strcmp(calcArgs_m.axis_type_b,"ALTZ") == 0);
    CPPUNIT_ASSERT_MESSAGE("Reference Frame set Incorrectly",
                           calcArgs_m.ref_frame == 0);
    CPPUNIT_ASSERT_MESSAGE("CALC Flags are incorrect",
                           calcArgs_m.kflags[0] == -1);
    for (int idx = 1; idx<64; idx++) {
      CPPUNIT_ASSERT_MESSAGE("CALC Flags are incorrect",
                             calcArgs_m.kflags[idx] == 0);
    }
  }

  void testSettingCalcEnvironment() {
    char* envValue;
    envValue = getenv("CALC_USER");
    CPPUNIT_ASSERT_MESSAGE("CALC_USER Environment not found",
                           envValue != NULL);
    CPPUNIT_ASSERT_MESSAGE("CALC_USER Environment not set correctly",
                           strcmp(envValue, "C") == 0);

    envValue = getenv("WET_ATM");
    CPPUNIT_ASSERT_MESSAGE("WET_ATM Environment not found",
                           envValue != NULL);
    CPPUNIT_ASSERT_MESSAGE("WET_ATM Environment not set correctly",
                           strcmp(envValue, "N") == 0);

    envValue = getenv("JPLEPH");
    CPPUNIT_ASSERT_MESSAGE("JPLEPH Environment not found",
                           envValue != NULL);

  }

  void testAddingAntenna() {
    const unsigned int MAX_ANTENNA = 99;
    CalcAntenna_t newAntenna[MAX_ANTENNA];

    for (unsigned int idx = 0; idx < MAX_ANTENNA; idx++) {
      char name[5];
      sprintf(name,"DV%02d",idx);
      createAntenna(newAntenna[idx], name , idx, 2*idx, 3*idx, 0.1*idx);
      addAntenna(newAntenna[idx]);

      CPPUNIT_ASSERT_MESSAGE("Antenna list is incorrect size",
                             antennaList_m.size() == idx+1);

      CPPUNIT_ASSERT_MESSAGE("Antenna was inserted incorrectly",
                             checkAntenna(newAntenna[idx]));

      for (unsigned int cidx = 0; cidx < idx; cidx++) {
        CPPUNIT_ASSERT_MESSAGE("Antenna was incorrectly modified",
                               checkAntenna(newAntenna[cidx]));
      }
    }
    checkTables();
  }

  void testUpdatingAnAntenna() {
    const unsigned int MAX_ANTENNA = 99;
    CalcAntenna_t newAntenna[MAX_ANTENNA];

    /* First populate the antenna list */
    for (unsigned int idx = 0; idx < MAX_ANTENNA; idx++) {
      char name[5];
      sprintf(name,"DV%02d",idx);
      createAntenna(newAntenna[idx], name , idx, 2*idx, 3*idx, 0.1*idx);
      addAntenna(newAntenna[idx]);
    }

    /* Now modify each antenna and ensure that it changes */
    for (unsigned int idx = 0; idx < MAX_ANTENNA; idx++) {
      char name[5];
      sprintf(name,"DV%02d",idx);
      createAntenna(newAntenna[idx], name , 0.1* idx, 0.2*idx, 0.3*idx, 1*idx);
      addAntenna(newAntenna[idx]);

      for (unsigned int tidx = 0; tidx < MAX_ANTENNA; tidx++) {
        CPPUNIT_ASSERT_MESSAGE("Antenna list is incorrect size",
                               antennaList_m.size() == MAX_ANTENNA);
        if (tidx == idx) {
          CPPUNIT_ASSERT_MESSAGE("Antenna was modified but with wrong values",
                                 checkAntenna(newAntenna[tidx]));
        } else {
          CPPUNIT_ASSERT_MESSAGE("Antenna was incorrectly modified",
                                 checkAntenna(newAntenna[tidx]));
        }
      }
    }
    checkTables();
  }

  void testFindingTAI2UTC() {
    CPPUNIT_ASSERT_MESSAGE("TAI to UTC value no longer 34.  (Has there been a leap second?)", sortaEqual(findTAI2UTC(),34.0, 1E-6));
  }

  void testGettingIERSData() {
    double mjdRes[5] = {55520.0,  55521.0, 55522.0, 55523.0, 55524.0};
    //double xpole[5]  = {0.20559,0.20512,0.20480,0.20417,0.20285};
    //double ypole[5]  = {0.25831,0.25611,0.25408,0.25224,0.25056};
    //double ut1utc[5] = {-0.111736,-0.112410,-0.112987,-0.113480,-0.113948};                                             
    double xpole[5]  = {0.205596,0.205074,0.204829,0.203984,0.202921};
    double ypole[5]  = {0.258421,0.256197,0.254244,0.252249,0.250712};
    double ut1utc[5] = {-0.1117369,-0.1124101,-0.1129861,-0.1134647,-0.1139535};                                             


    if (true)for (int i=55400; i <= 55700; i+=10) {
        std::ostringstream o;
        double mj = i*1.0;
        string stat;
        stat=getIERSData(mj)?"ERR":" OK";
        o << "@IERS range test: " << mj << "  " << stat ; 
        LOG_TO_DEVELOPER(LM_INFO, o.str()); 
    }

    std::ostringstream o;
    o << "Error getting IERS data for MJD " << IERSMJD;
    CPPUNIT_ASSERT_MESSAGE(o.str(), !getIERSData(IERSMJD));

    for (int idx = 0; idx < 5; idx++) {
      std::ostringstream o;
      o << "@PROGRAM/IERS data MJD=" << mjdRes[idx] << ":" 
        << setprecision(5) << std::fixed
        << " XPOLE=" << xpole[idx]  << "/" << calcArgs_m.xpole[idx]
        << " YPOLE=" << ypole[idx]  << "/" << calcArgs_m.ypole[idx]
        << setprecision(6)
        << " UT1-UTC=" << ut1utc[idx] << "/" << calcArgs_m.ut1_utc[idx];
      LOG_TO_DEVELOPER(LM_INFO, o.str()); 
    } 
 
    for (int idx = 0; idx < 5; idx++) {
      CPPUNIT_ASSERT_MESSAGE("MJD of IERS Data incorrect",
                             sortaEqual(calcArgs_m.EOP_time[idx],
                                        mjdRes[idx]));
      CPPUNIT_ASSERT_MESSAGE("TAI_UTC set incorrectly",
                             sortaEqual(calcArgs_m.tai_utc[idx],
                                        findTAI2UTC(), 1E-6));
      CPPUNIT_ASSERT_MESSAGE("UT1_UTC set incorrectly",
                             sortaEqual(calcArgs_m.ut1_utc[idx],
                                        ut1utc[idx], 1.0E-4));
      // Polar motion units are different than time, so error could be different
      CPPUNIT_ASSERT_MESSAGE("XPOLE set incorrectly",
                             sortaEqual(calcArgs_m.xpole[idx],
                                        xpole[idx], 1E-4));
      CPPUNIT_ASSERT_MESSAGE("YPOLE set incorrectly",
                             sortaEqual(calcArgs_m.ypole[idx],
                                        ypole[idx], 1E-4));
    }
  }

  void testFailureGettingIERSData() {

    /* The date 65833 is somewhere around 2040, if your still checking
       this test then... better update it. */
    CPPUNIT_ASSERT_MESSAGE("Incorrectly got IERS data",
                           getIERSData(65833.0));
  }

  void testSettingPolynomialOrder(){
    unsigned long currentPolynomialOrder = polynomialOrder_m;
    unsigned long currentDelayTableSize  = delayTableSize;
    unsigned long currentAzTableSize     = azTableSize;
    unsigned long currentElTableSize     = elTableSize;
    unsigned long currentDryAtmTableSize = dryAtmTableSize;
    unsigned long currentWetAtmTableSize = wetAtmTableSize;

    unsigned long newPolynomialOrder     = currentPolynomialOrder - 1;
    setPolynomialOrder(newPolynomialOrder);

    CPPUNIT_ASSERT_MESSAGE("Delay Table size did not change as expected",
                           delayTableSize == 
                           (currentDelayTableSize/ currentPolynomialOrder) * 
                           newPolynomialOrder);
    CPPUNIT_ASSERT_MESSAGE("Azimuth Table size did not change as expected",
                           azTableSize == 
                           (currentAzTableSize/ currentPolynomialOrder) * 
                           newPolynomialOrder);
    CPPUNIT_ASSERT_MESSAGE("Elevation Table size did not change as expected",
                           elTableSize == 
                           (currentElTableSize/ currentPolynomialOrder) * 
                           newPolynomialOrder);
    CPPUNIT_ASSERT_MESSAGE("Dry Atm. Table size did not change as expected",
                           dryAtmTableSize == 
                           (currentDryAtmTableSize/ currentPolynomialOrder) * 
                           newPolynomialOrder);
    CPPUNIT_ASSERT_MESSAGE("Wet Atm. Table size did not change as expected",
                           wetAtmTableSize == 
                           (currentWetAtmTableSize/ currentPolynomialOrder) * 
                           newPolynomialOrder);
  }

  void testSettingReference() {
    CalcAntenna_t newReference;
    createAntenna(newReference, "Ref", 1, 2, 3, 4);
    setReference(newReference);

    CPPUNIT_ASSERT_MESSAGE("Reference position not set correctly",
                           calcArgs_m.a_x == newReference.x &&
                           calcArgs_m.a_y == newReference.y &&
                           calcArgs_m.a_z == newReference.z &&
                           calcArgs_m.axis_off_a == newReference.axisOffset);
  }

  void testFillingSourceTable() {
    std::vector<CalcSource_t> newSourceTable;
    for (unsigned int idx = 0; idx <= polynomialOrder_m; idx++) {
      CalcSource_t newSource;
      newSource.RA = 0.1 * idx;
      newSource.Dec = -0.1 * idx;
      newSource.dRA = 0.01 * idx;
      newSource.dDec = -0.01 * idx;
      newSource.Epoch = 54000 + idx;
      newSource.Parallax = 0.0001 * idx;
      newSourceTable.push_back(newSource);
    }

    fillSourceTable(newSourceTable);
    for (unsigned int idx = 0; idx <= polynomialOrder_m; idx++) {
      CPPUNIT_ASSERT_MESSAGE("RA incorrectly set in source table",
                             sortaEqual(newSourceTable[idx].RA,
                                        sourceTable_m[idx].RA));
      CPPUNIT_ASSERT_MESSAGE("Dec incorrectly set in source table",
                             sortaEqual(newSourceTable[idx].Dec,
                                        sourceTable_m[idx].Dec));
      CPPUNIT_ASSERT_MESSAGE("dRA incorrectly set in source table",
                             sortaEqual(newSourceTable[idx].dRA,
                                        sourceTable_m[idx].dRA));
      CPPUNIT_ASSERT_MESSAGE("dDec incorrectly set in source table",
                             sortaEqual(newSourceTable[idx].dDec,
                                        sourceTable_m[idx].dDec));
      CPPUNIT_ASSERT_MESSAGE("Epoch incorrectly set in source table",
                             sortaEqual(newSourceTable[idx].Epoch,
                                        sourceTable_m[idx].Epoch));
      CPPUNIT_ASSERT_MESSAGE("Parallax incorrectly set in source table",
                             sortaEqual(newSourceTable[idx].Parallax,
                                        sourceTable_m[idx].Parallax));
    }

  }

  void testErrorFillingSourceTable() {
    /* First test too short of a table */
    {
      std::vector<CalcSource_t> newSourceTable;
      for (unsigned int idx = 0; idx < polynomialOrder_m; idx++) {
        CalcSource_t newSource;
        newSource.RA = 0.1 * idx;
        newSource.Dec = -0.1 * idx;
        newSource.dRA = 0.01 * idx;
        newSource.dDec = -0.01 * idx;
        newSource.Epoch = 54000 + idx;
        newSource.Parallax = 0.0001 * idx;
        newSourceTable.push_back(newSource);
      }
      CPPUNIT_ASSERT_THROW(fillSourceTable(newSourceTable),
                           DelayServerExceptions::TableSizeErrorExImpl);
    }

    /* Now test too long of a table */
    {
      std::vector<CalcSource_t> newSourceTable;
      for (unsigned int idx = 0; idx <= polynomialOrder_m+1 ; idx++) {
        CalcSource_t newSource;
        newSource.RA = 0.1 * idx;
        newSource.Dec = -0.1 * idx;
        newSource.dRA = 0.01 * idx;
        newSource.dDec = -0.01 * idx;
        newSource.Epoch = 54000 + idx;
        newSource.Parallax = 0.0001 * idx;
        newSourceTable.push_back(newSource);
      }
      CPPUNIT_ASSERT_THROW(fillSourceTable(newSourceTable),
                           DelayServerExceptions::TableSizeErrorExImpl);
    }
  }

  void testSettingSource() {
    CalcSource_t newSource;
    newSource.RA = 0.1;
    newSource.Dec = -0.1;
    newSource.dRA = 0.01;
    newSource.dDec = -0.01;
    newSource.Epoch = 54000;
    newSource.Parallax = 0.0001;
   
    setSource(newSource);
    
    for (unsigned int idx = 0; idx <= polynomialOrder_m; idx++) {
      CPPUNIT_ASSERT_MESSAGE("RA incorrectly set in source table",
                             sortaEqual(newSource.RA,
                                        sourceTable_m[idx].RA));
      CPPUNIT_ASSERT_MESSAGE("Dec incorrectly set in source table",
                             sortaEqual(newSource.Dec,
                                        sourceTable_m[idx].Dec));
      CPPUNIT_ASSERT_MESSAGE("dRA incorrectly set in source table",
                             sortaEqual(newSource.dRA,
                                        sourceTable_m[idx].dRA));
      CPPUNIT_ASSERT_MESSAGE("dDec incorrectly set in source table",
                             sortaEqual(newSource.dDec,
                                        sourceTable_m[idx].dDec));
      CPPUNIT_ASSERT_MESSAGE("Epoch incorrectly set in source table",
                             sortaEqual(newSource.Epoch,
                                        sourceTable_m[idx].Epoch));
      CPPUNIT_ASSERT_MESSAGE("Parallax incorrectly set in source table",
                             sortaEqual(newSource.Parallax,
                                        sourceTable_m[idx].Parallax));
    }
  }

  void testFillingDelayTableSimple() {
    /* This is a simple call to CALC with one antenna to make sure it 
       runs properly */
    CalcAntenna_t newReference;
    CalcAntenna_t newAntenna;
    CalcSource_t newSource;
    double delayResults[5] = {-8.78142e-08, -8.77314e-08, -8.76485e-08,
                              -8.75655e-08, -8.74824e-08};
    double azResults[5]    = {2.10534, 2.10621, 2.10708, 2.10795, 2.10882};
    double elResults[5]    = {0.503957, 0.504737, 0.505516, 0.506294,
                              0.507073};

    newSource.RA = 0.1;
    newSource.Dec = -0.1;
    newSource.dRA = 0.0;
    newSource.dDec = 0.0;
    newSource.Epoch = 54000;
    newSource.Parallax = 0.0;


    createAntenna(newReference, "Ref", -1601361.760577, -5042192.535329, 
                  3554531.519329, 0.0);
    createAntenna(newAntenna, "DV01", -1601328.438917, -5042203.194271,
                  3554532.360703, 0.0);
    

    setReference(newReference);
    addAntenna(newAntenna);
    setSource(newSource);
    getIERSData(IERSMJD);
    
    std::vector<ACS::Time> timeList;
    fillTimeList(timeList, 60, polynomialOrder_m +1);
    fillDelayTable(timeList);

    double* delayTable = getDelayTable();
    double* azTable = getAzTable();
    double* elTable = getElTable();

    for (unsigned int idx = 0; idx <= polynomialOrder_m; idx++) {
      CPPUNIT_ASSERT_MESSAGE("Delay result differs from expected.",
                             sortaEqual(delayResults[idx], delayTable[idx],
                                        1E-5));
      if (false)CPPUNIT_ASSERT_MESSAGE("Azimuth result differs from expected.",
                             sortaEqual(azResults[idx], azTable[idx],
                                        1E-4));
      if (false)CPPUNIT_ASSERT_MESSAGE("Elevation result differs from expected.",
                             sortaEqual(elResults[idx], elTable[idx],
                                        1E-4));
    }
  }

  void testFillingDelayTable() {
    /* Instead of doing a detailed value check on all of these
       results, I'm just checking to make sure they are plausable
    */
    srand(time(0));
    CalcAntenna_t newReference;
    CalcSource_t newSource;
    
    const double MAX_DELAY = 60E-6;
    const double MAX_DELAY_CHANGE = 0.2E-6;
    const double MAX_POINTING_CHANGE = M_PI / 720.0;
    const double MAX_POINTNG_DIFFERENCE = 0.0058177641733144318; //20 arcmin
    const double NUM_ANTENNA = 66;

    newSource.RA = 0.1;
    newSource.Dec = -0.1;
    newSource.dRA = 0.0;
    newSource.dDec = 0.0;
    newSource.Epoch = 54000;
    newSource.Parallax = 0.0;

    double refX = -1601361.760577;
    double refY = -5042192.535329;
    double refZ = 3554531.519329;

    createAntenna(newReference, "Ref", refX, refY, refZ, 0.0);
    setReference(newReference);
    setSource(newSource);
    getIERSData(IERSMJD);

    for (int idx = 0; idx < NUM_ANTENNA; idx++) {
      /* Create antennas within a 15 km circle of the reference and
         withing 500 m vertically the axis offset is less than 1 mm */
      CalcAntenna_t newAntenna;
      char name[5];
      sprintf(name,"DV%02d",idx);
      createAntenna(newAntenna, name, refX +(20000*(drand48()-0.5)),
                    refY + (20000*(drand48()-0.5)),refZ+(1000*(drand48()-0.5)),
                    (drand48()-0.5)*0.002);
      addAntenna(newAntenna);
    }
    
    std::vector<ACS::Time> timeList;
    fillTimeList(timeList, 60, polynomialOrder_m +1);
    
    fillDelayTable(timeList);

    double* delayResult = getDelayTable();
    
    for (unsigned int antIdx = 0; antIdx < NUM_ANTENNA; antIdx++) {
      double maxDelay = delayResult[calcDelayIndex(antIdx, 0)];
      double minDelay = delayResult[calcDelayIndex(antIdx, 0)];
      for (unsigned int tidx = 0; tidx <= polynomialOrder_m; tidx++) {
        CPPUNIT_ASSERT_MESSAGE("Delay value is greater tha 60 us",
                               MAX_DELAY > 
                               delayResult[calcDelayIndex(antIdx, tidx)]);
        if (delayResult[calcDelayIndex(antIdx, tidx)] > maxDelay) 
          maxDelay = delayResult[calcDelayIndex(antIdx, tidx)];
        if (delayResult[calcDelayIndex(antIdx, tidx)] < minDelay) 
          minDelay = delayResult[calcDelayIndex(antIdx, tidx)];
      }
      CPPUNIT_ASSERT_MESSAGE("Delay value does not seem to have changed",
                             maxDelay-minDelay > 0);
      CPPUNIT_ASSERT_MESSAGE("Delay value has changed too much",
                             maxDelay-minDelay < MAX_DELAY_CHANGE);
    }

    double* elResult = getElTable();
    double globalMinEl = elResult[calcDelayIndex(0, 0)];
    double globalMaxEl = elResult[calcDelayIndex(0, 0)];
    for (unsigned int antIdx = 0; antIdx < NUM_ANTENNA; antIdx++) {
      double maxEl = elResult[calcDelayIndex(antIdx, 0)];
      double minEl = elResult[calcDelayIndex(antIdx, 0)];
      for (unsigned int tidx = 0; tidx <= polynomialOrder_m; tidx++) {
        if (elResult[calcDelayIndex(antIdx, tidx)] > maxEl) {
          maxEl = elResult[calcDelayIndex(antIdx, tidx)];
          if (elResult[calcDelayIndex(antIdx, tidx)] > globalMaxEl) {
            globalMaxEl = elResult[calcDelayIndex(antIdx, tidx)];
          }
        }
        if (elResult[calcDelayIndex(antIdx, tidx)] < minEl) {
          minEl = elResult[calcDelayIndex(antIdx, tidx)];
          if (elResult[calcDelayIndex(antIdx, tidx)] < globalMinEl) {
            globalMinEl = elResult[calcDelayIndex(antIdx, tidx)];
          }
        }
      }
      double deltaEl = maxEl - minEl;
      CPPUNIT_ASSERT_MESSAGE("El value does not seem to have changed",
                             deltaEl > 0);
      CPPUNIT_ASSERT_MESSAGE("El value has changed to much",
                             maxEl-minEl < MAX_POINTING_CHANGE);
    }
    double deltaEl = globalMaxEl - globalMinEl;
    CPPUNIT_ASSERT_MESSAGE("Antennas are pointing too far from each other",
                           deltaEl < MAX_POINTNG_DIFFERENCE);
        
    double* azResult = getAzTable();
    double globalMinAz = azResult[calcDelayIndex(0, 0)];
    double globalMaxAz = azResult[calcDelayIndex(0, 0)];
    for (unsigned int antIdx = 0; antIdx < NUM_ANTENNA; antIdx++) {
      double maxAz = azResult[calcDelayIndex(antIdx, 0)];
      double minAz = azResult[calcDelayIndex(antIdx, 0)];
      for (unsigned int tidx = 0; tidx <= polynomialOrder_m; tidx++) {
        if (azResult[calcDelayIndex(antIdx, tidx)] > maxAz) {
          maxAz = azResult[calcDelayIndex(antIdx, tidx)];
          if (azResult[calcDelayIndex(antIdx, tidx)] > globalMaxAz) {
            globalMaxAz = azResult[calcDelayIndex(antIdx, tidx)];
          }
        }
        if (azResult[calcDelayIndex(antIdx, tidx)] < minAz) {
          minAz = azResult[calcDelayIndex(antIdx, tidx)];
          if (azResult[calcDelayIndex(antIdx, tidx)] < globalMinAz) {
            globalMinAz = azResult[calcDelayIndex(antIdx, tidx)];
          }
        }
      }
        
      double cosel = cos(globalMaxEl);
      double deltaAz = maxAz - minAz;
      if (deltaAz > M_PI) deltaAz = (2 * M_PI) - deltaAz;
      // We correct the delta az for the elevation to get an approx sky arc
      // which does not change with elevation (so that we can compare to a
      // fixed value).
      deltaAz *= cosel;
      if (false) cout << "AZ VALUES:" << deltaAz << "  "  
           << maxAz << "  "<< minAz << "  " << MAX_POINTING_CHANGE 
           << "  " << globalMaxEl << endl; 
      CPPUNIT_ASSERT_MESSAGE("Az value does not seem to have changed",
                             deltaAz > 0);
      CPPUNIT_ASSERT_MESSAGE("Az value has changed to much",
                             deltaAz < MAX_POINTING_CHANGE);
    }
    double deltaAz = globalMaxAz - globalMinAz;
    if (deltaAz > M_PI) deltaAz = (2 * M_PI) - deltaAz;
    CPPUNIT_ASSERT_MESSAGE("Antennas are pointing too far from each other",
                           deltaAz < MAX_POINTNG_DIFFERENCE);
  }


  void testFillingDelayTableErrors() {
    std::vector<ACS::Time> timeList;
    fillTimeList(timeList, 60, polynomialOrder_m);
    
    CPPUNIT_ASSERT_THROW(fillDelayTable(timeList),
                         DelayServerExceptions::TableSizeErrorExImpl);

    fillTimeList(timeList, 60, polynomialOrder_m + 2);
    CPPUNIT_ASSERT_THROW(fillDelayTable(timeList),
                         DelayServerExceptions::TableSizeErrorExImpl); 

    fillTimeList(timeList, 60, polynomialOrder_m + 1);
    CPPUNIT_ASSERT_THROW(fillDelayTable(timeList),
                         DelayServerExceptions::TableSizeErrorExImpl); 
  }

  void testAtmosphericDelay() {
    /* Instead of doing a detailed value check on all of these
       results, I'm just checking to make sure they are plausable
    */
    CalcAntenna_t newReference;
    CalcSource_t newSource;

    newSource.RA = 0.1;
    newSource.Dec = 1.57;
    newSource.dRA = 0.0;
    newSource.dDec = 0.0;
    newSource.Epoch = 54000;
    newSource.Parallax = 0.0;

    double refX = -1601361.760577;
    double refY = -5042192.535329;
    double refZ = 3554531.519329;

    createAntenna(newReference, "Ref", refX, refY, refZ, 0.0);
    setReference(newReference);
    setSource(newSource);
    getIERSData(IERSMJD);

    /* Test All antennas at same pressure, but different Altitudes */
    CalcAntenna_t newAntenna;
    
    std::vector<ACS::Time> timeList;
    fillTimeList(timeList, 60, polynomialOrder_m +1);   
    fillDelayTable(timeList);

    createAntenna(newAntenna, "DV01", refX + 100, refY + 100, refZ+100, 0);
    addAntenna(newAntenna);
    createAntenna(newAntenna, "DV02", refX + 100, refY + 100, refZ+100, 0);
    addAntenna(newAntenna);
    createAntenna(newAntenna, "DV03", refX + 100, refY + 100, refZ+100, 0);
    addAntenna(newAntenna);


     /* Now test different pressures at same altitude */
     setAntennaPressure(createPressureData("DV01", 101325.0 * 0.5));
     setAntennaPressure(createPressureData("DV02", 101325.0));
     setAntennaPressure(createPressureData("DV03", 101325.0 * 1.5));

    fillDelayTable(timeList);

    double* dryAtmResult = getDryAtmTable();

    for (unsigned int antIdx = 0; antIdx < 2; antIdx++) {
      /* The ones with more atmosphere should have larger delays; but
         since this appears to be negative for some reason */
      CPPUNIT_ASSERT(dryAtmResult[calcDelayIndex(antIdx,0)] <
                     dryAtmResult[calcDelayIndex(antIdx+1,0)]);
    }
  }


  void testTiming() {
    srand(time(0));

    struct timeval startTime;
    struct timeval endTime;
    
    CalcAntenna_t newReference;
    CalcSource_t newSource;

    const double NUM_ANTENNA = 66;

    newSource.RA = 0.1;
    newSource.Dec = -0.1;
    newSource.dRA = 0.0;
    newSource.dDec = 0.0;
    newSource.Epoch = 54000;
    newSource.Parallax = 0.0;

    double refX = -1601361.760577;
    double refY = -5042192.535329;
    double refZ = 3554531.519329;

    createAntenna(newReference, "Ref", refX, refY, refZ, 0.0);
    setReference(newReference);
    getIERSData(IERSMJD);

    std::cout << "\n\n@Fill Delay Table Timing Test Results:" << std::endl;
    std::cout << "@\t\t\t\tTime per CALC Call Per Antenna" << std::endl;
    std::cout << "@Num Ant.\t5\t\t\t10\t\t\t20\t\t\t30" << std::endl;
    for (int idx = 0; idx < NUM_ANTENNA; idx++) {
      /* Create antennas within a 15 km circle of the reference and
         within 500 m vertically the axis offset is less than 1 mm */
      CalcAntenna_t newAntenna;
      char name[5];
      sprintf(name,"DV%02d",idx);
      createAntenna(newAntenna, name, refX +(20000*(drand48()-0.5)),
                    refY + (20000*(drand48()-0.5)),refZ+(1000*(drand48()-0.5)),
                    (drand48()-0.5)*0.002);
      addAntenna(newAntenna);

      if (!(antennaList_m.size() % 6) ||(antennaList_m.size() == 1) ) {
        std::cout << "@"<<antennaList_m.size() << std::setw(3)
                  << std::scientific;
      
        setPolynomialOrder(4);
        setSource(newSource);
        while (polynomialOrder_m < 30) {
          std::vector<ACS::Time> timeList;
          fillTimeList(timeList, 60, polynomialOrder_m +1);
    
          gettimeofday(&startTime, NULL);
          fillDelayTable(timeList);
          gettimeofday(&endTime, NULL);

          double elapsedTime = (endTime.tv_sec - startTime.tv_sec) +
            (1E-6 * (endTime.tv_usec - startTime.tv_usec));
          
          std::cout << "\t"  << std::setprecision(1) 
                    << elapsedTime / ((polynomialOrder_m +1)
                                      *antennaList_m.size())
                    << " (" << elapsedTime << ")";
          if (polynomialOrder_m < 10) {
            setPolynomialOrder(polynomialOrder_m +5);
            setSource(newSource);
          } else {
            setPolynomialOrder(polynomialOrder_m +10);
            setSource(newSource);
          }
        }
        std::cout << std::endl;
      }
    }
  }

  CPPUNIT_TEST_SUITE(CalcDataManagerTest);
  CPPUNIT_TEST(testSettingCalcEnvironment);
  CPPUNIT_TEST(testInstanciation);
  CPPUNIT_TEST(testAddingAntenna);
  CPPUNIT_TEST(testUpdatingAnAntenna);
  CPPUNIT_TEST(testFindingTAI2UTC);
  CPPUNIT_TEST(testGettingIERSData);
  CPPUNIT_TEST(testFailureGettingIERSData);
  CPPUNIT_TEST(testSettingPolynomialOrder);
  CPPUNIT_TEST(testSettingReference);
  CPPUNIT_TEST(testFillingSourceTable);
  CPPUNIT_TEST(testErrorFillingSourceTable);
  CPPUNIT_TEST(testSettingSource);
  CPPUNIT_TEST(testFillingDelayTableSimple);
  CPPUNIT_TEST(testAtmosphericDelay);
  CPPUNIT_TEST(testFillingDelayTable);
  CPPUNIT_TEST(testFillingDelayTableErrors);
  CPPUNIT_TEST(testTiming);
  CPPUNIT_TEST_SUITE_END();

private:
  /* ---------------- Utility Functions -------------------- */

  bool checkAntenna(CalcAntenna_t& refAnt) {
    for (unsigned int antIdx = 0; antIdx < antennaList_m.size(); antIdx++) { 
      if (!strncmp(refAnt.antennaName, antennaList_m[antIdx].antennaName,4)){
        if (antennaList_m[antIdx].x != refAnt.x ||
            antennaList_m[antIdx].y != refAnt.y ||
            antennaList_m[antIdx].z != refAnt.z || 
            antennaList_m[antIdx].axisOffset != refAnt.axisOffset) {
          return false;
        } else {
          return true;
        } 
      }
    }
    return false;
  }

  void allocateDelayTable() {
    CalcDataManager::allocateDelayTable();
    
    delayTableSize = (polynomialOrder_m + 1) * antennaList_m.size();
    azTableSize    = (polynomialOrder_m + 1) * antennaList_m.size();
    elTableSize    = (polynomialOrder_m + 1) * antennaList_m.size();
    dryAtmTableSize= (polynomialOrder_m + 1) * antennaList_m.size();
    wetAtmTableSize= (polynomialOrder_m + 1) * antennaList_m.size();
    checkTables();
  }

  void checkTables() {
    CPPUNIT_ASSERT_MESSAGE("Delay Table is Unallocated",
                           delayTable_m != NULL);
    CPPUNIT_ASSERT_MESSAGE("Delay Table Size Incorrect",
                           delayTableSize ==
                           (polynomialOrder_m + 1) * antennaList_m.size());


    CPPUNIT_ASSERT_MESSAGE("Azimuth Table is Unallocated",
                           azTable_m != NULL);
    CPPUNIT_ASSERT_MESSAGE("Azimuth Table Size Incorrect",
                           azTableSize ==
                           (polynomialOrder_m + 1) * antennaList_m.size());

    CPPUNIT_ASSERT_MESSAGE("Elevation Table is Unallocated",
                           elTable_m != NULL);
    CPPUNIT_ASSERT_MESSAGE("Elevation Table Size Incorrect",
                           elTableSize ==
                           (polynomialOrder_m + 1) * antennaList_m.size());

    CPPUNIT_ASSERT_MESSAGE("Dry Atmosphere Delay Table is Unallocated",
                           dryAtmTable_m != NULL);
    CPPUNIT_ASSERT_MESSAGE("Dry Atmosphere Delay Table Size Incorrect",
                           dryAtmTableSize ==
                           (polynomialOrder_m + 1) * antennaList_m.size());

    CPPUNIT_ASSERT_MESSAGE("Wet Atmosphere Delay Table is Unallocated",
                           wetAtmTable_m != NULL);
    CPPUNIT_ASSERT_MESSAGE("Wet Atmosphere Delay Table Size Incorrect",
                           wetAtmTableSize ==
                           (polynomialOrder_m + 1) * antennaList_m.size());

    /* Now populate all the tables then read them back to 
       ensure we haven't had a problem */
    for (unsigned long idx = 0; idx < delayTableSize; idx++)
      *(reinterpret_cast<unsigned long long*>(&delayTable_m[idx])) =
        0xa5a5a5a5ULL;

     for (unsigned long idx = 0; idx < azTableSize; idx++)
       *(reinterpret_cast<unsigned long long*>(&azTable_m[idx])) =
         0x5a5a5a5aULL;

     for (unsigned long idx = 0; idx < elTableSize; idx++)
       *(reinterpret_cast<unsigned long long*>(&elTable_m[idx])) =
         0xb6b6b6b6ULL;

     for (unsigned long idx = 0; idx < dryAtmTableSize; idx++)
       *(reinterpret_cast<unsigned long long*>(&dryAtmTable_m[idx])) =
         0xc7c7c7c7ULL;

     for (unsigned long idx = 0; idx < wetAtmTableSize; idx++)
       *(reinterpret_cast<unsigned long long*>(&wetAtmTable_m[idx])) =
         0xd8d8d8d8ULL;

    for (unsigned long idx = 0; idx < delayTableSize; idx++)
      CPPUNIT_ASSERT_MESSAGE("Error in consistency check of Delay Table",
                             *(reinterpret_cast<unsigned long long*>
                               (&delayTable_m[idx])) ==  0xa5a5a5a5ULL);

    for (unsigned long idx = 0; idx < azTableSize; idx++) 
      CPPUNIT_ASSERT_MESSAGE("Error in consistency check of Azimuth Table",
                             *(reinterpret_cast<unsigned long long*>
                               (&azTable_m[idx])) ==  0x5a5a5a5aULL);

     for (unsigned long idx = 0; idx < elTableSize; idx++)
       CPPUNIT_ASSERT_MESSAGE("Error in consistency check of Elevation Table",
                             *(reinterpret_cast<unsigned long long*>
                               (&elTable_m[idx])) ==  0xb6b6b6b6ULL);

     for (unsigned long idx = 0; idx < dryAtmTableSize; idx++) 
      CPPUNIT_ASSERT_MESSAGE("Error in consistency check of Dry Atm. Table",
                             *(reinterpret_cast<unsigned long long*>
                               (&dryAtmTable_m[idx])) ==  0xc7c7c7c7ULL);

     for (unsigned long idx = 0; idx < wetAtmTableSize; idx++) 
      CPPUNIT_ASSERT_MESSAGE("Error in consistency check of Wet Atm. Table",
                             *(reinterpret_cast<unsigned long long*>
                               (&wetAtmTable_m[idx])) ==  0xd8d8d8d8ULL);
  }


   bool sortaEqual(double a, double b, double threshold = 1E-14) {
    if (fabs(a-b) < threshold)
      return true;
    std::cout << "Sorta Equal Method Difference is: " << fabs(a-b)<< std::endl;
    return false;
  }

  void fillTimeList(std::vector<ACS::Time>& timeList, double duration,
                    int numTimes) {
    timeList.clear();  
    //timeList.push_back(static_cast<ACS::Time>(134547910801920000LL));
    timeList.push_back(static_cast<ACS::Time>(ACSTIMEREF));
    ACS::Time increment = static_cast<ACS::Time>(duration * 1E7/(numTimes-1));
    for (int idx = 1; idx < numTimes; idx++) {
      timeList.push_back(timeList[idx-1] + increment);
    }
  }

  void createAntenna(CalcAntenna_t& newAnt, char* name, 
                     double x, double y, double z, double k){
    strncpy(newAnt.antennaName, name, 4);
    newAnt.x = x;
    newAnt.y = y;
    newAnt.z = z;
    newAnt.axisOffset = k;
  }

  CalcPressureData_t createPressureData(char* name, double pressure) {
    CalcPressureData_t newPressureData;
    strncpy(newPressureData.antennaName, name, 4);
    newPressureData.pressure = pressure;
    return newPressureData;
  }

  /* State Variables to assist in testing */
  unsigned long delayTableSize;
  unsigned long azTableSize;   
  unsigned long elTableSize;   
  unsigned long dryAtmTableSize;   
  unsigned long wetAtmTableSize;   
}; // End of Test Class

int main( int argc, char **argv)
{
  CppUnit::TextUi::TestRunner runner;
  runner.addTest( CalcDataManagerTest::suite() );
  runner.run();
  return 0;
}
