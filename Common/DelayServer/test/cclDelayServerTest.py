#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# jkern  2006-11-08  created
#

#************************************************************************
#   NAME
# 
#   SYNOPSIS
# 
#   DESCRIPTION
#
#   FILES
#
#   ENVIRONMENT
#
#   RETURN VALUES
#
#   CAUTIONS
#
#   EXAMPLES
#
#   SEE ALSO
#
#   BUGS     
#
#------------------------------------------------------------------------
#
import unittest

from CCL.DelayServerBase import DelayServerBase
from CCL.SkyDelayServer import SkyDelayServer
## from Acspy.Clients.SimpleClient import PySimpleClient
## from Acspy.Nc.Consumer import Consumer
import Control
import TmcdbErrType
## from time import sleep
## from time import time
## import TETimeUtil
## import asdmIDLTypes



ArrayName = 'TestArray002'

class DelayServerBaseTest ( unittest.TestCase):
    #def __init__(self, delayComponentReference):
    #    self.dsb = DelayServerBase(delayComponentReference)

    def _setDelayComponentReference(self, reference):
        self.dsb = DelayServerBase(reference)

    def testInstactiation(self):
        self.assertNotEqual(self.dsb._getReference(), None)
        pass
    
    def testAntennas(self):
        self.dsb.addAntenna("DA41")
        self.assertEqual(self.dsb.getAntennas(), ["DA41"])
        self.dsb.addAntenna("DV02")
        self.assertEqual(self.dsb.getAntennas(), ["DA41", "DV02"])
        self.dsb.addAntenna("DA41")
        self.assertEqual(self.dsb.getAntennas(), ["DA41", "DV02"])
        self.dsb.removeAntenna("DA41")
        self.assertEqual(self.dsb.getAntennas(), ["DV02"])

    def testSettingPadVector(self):
        self.dsb.addAntenna("DA41")
        self.dsb.setAntennaPadLocation("DA41", "111.1111 km",
                                       222222.2, -333333.3)
        rv = self.dsb.getAntennaPadLocation("DA41");
        self.assertEqual(len(rv), 3)
        self.assertAlmostEqual(rv[0], 111111.1, 9)
        self.assertAlmostEqual(rv[1], 222222.2, 9)
        self.assertAlmostEqual(rv[2], -333333.3, 9)

    def testSettingAntennaVector(self):
        self.dsb.addAntenna("DA41")
        self.dsb.setAntennaVector("DA41", "12.2 um", "-25 um", 12.5)
        rv = self.dsb.getAntennaVector("DA41");
        self.assertEqual(len(rv), 3)
        self.assertAlmostEqual(rv[0], 12.2E-6, 9)
        self.assertAlmostEqual(rv[1], -25E-6, 9)
        self.assertAlmostEqual(rv[2], 12.5, 9)

    def testSettingKTerm(self):
        self.dsb.addAntenna("DA41")
        self.dsb.setAxisNonIntersectionParameter("DA41", "32.5 um")
        self.assertAlmostEqual(self.dsb.getAxisNonIntersectionParameter("DA41"),
                               32.5E-6, 12)

    def testAntennaCableDelay(self):
        self.dsb.addAntenna("DA41")
        self.dsb.setAntennaCableDelay("DA41", "32.5 ns")
        self.assertAlmostEqual(self.dsb.getAntennaCableDelay("DA41"), 32.5E-9,
                                                             9)
        
    def testCausalityDelay(self):
        self.dsb.setCausalityDelay("45 us")
        self.assertAlmostEqual(self.dsb.getCausalityDelay(), 4.5E-5, 9)


    def testPolynomialOrder(self):
        self.dsb.setPolynomialOrder(7)
        self.assertEqual(self.dsb.getPolynomialOrder(),7)


    def testEventDuration(self):
        self.dsb.setDelayEventDuration("0.25 min")
        self.assertAlmostEqual(self.dsb.getDelayEventDuration(), 14.976, 9)

    def testAtmosphericDelayModel(self):
        self.dsb.setAtmosphericDelayModel("CALC_DRY_ONLY")
        self.assertEqual(self.dsb.getAtmosphericDelayModel(),
                         Control.CALC_DRY_ONLY)

        self.dsb.setAtmosphericDelayModel(Control.CALC_WET_AND_DRY)
        self.assertEqual(self.dsb.getAtmosphericDelayModel(),
                         Control.CALC_WET_AND_DRY)

class SkyDelayServerTest (DelayServerBaseTest):

    def setUp(self):
        self.sds = SkyDelayServer("Array001")
        self._setDelayComponentReference(self.sds._getReference())

    def testTMCDBAccess(self):
        self.sds.addAntenna("DA41")
        
        #for coord in self.sds.getAntennaPadLocation("DA41"):
        #     self.assertNotEqual(coord, 0.0)
            
        # Just test that these methods execute
        self.sds.getAntennaVector("DA41")
        self.sds.getAxisNonIntersectionParameter("DA41")

        #self.assertNotEqual(self.sds.getAntennaCableDelay("DA41"), 0);

        # Test that non existant antennas throw an exception
        #try:
        #   self.sds.addAntenna("NO15")
        #except TmcdbErrType.TmcdbErrorEx:
        #    pass
        #else:
        #    self.fail("Failed to throw exception")



## class EventCollector:
##     def __init__(self):
##         self.eventList = []
##         self.c = Consumer(Control.CHANNELNAME_CONTROLREALTIME)
##         self.c.addSubscription(Control.ArrayDelayEvent, self.eventIn)
##         self.c.consumerReady()
##         self.eventList = []
##     def __del__(self):
##         self.c.disconnect()
##     def eventIn(self,dataEvent):
##         self.eventList.append(dataEvent)
##     def numEvent(self):
##         return len(self.eventList)
##     def clear(self):
##         self.eventList = []


## def ServerInitializationTest(simpleDelayServer):
##     simpleDelayServer.setDelayEventDuration(10000000)
##     simpleDelayServer.initializeServer(ArrayName)

##     ec = EventCollector()
##     ec.clear()
##     maxIter = 30
##     while ec.numEvent() < 5 and maxIter > 0:
##         maxIter -= 1
##         sleep(1)
        
##     if maxIter == 0:
##         return (False,"Never recieved any events")

##     if ec.eventList[0].arrayName != ArrayName:
##         return (False,"Array name mismatch")
    

##     if len(ec.eventList[0].antennaDelayEvents) != 0:
##         return (False,"Recieved Antenna Delay event")

##     return (True, "")

## def BasicStructureTest(simpleDelayServer):

##     antChar = Control.AntennaCharacteristics('Alma01',
##                                     asdmIDLTypes.IDLLength(12.0),
##                                     asdmIDLTypes.IDLLength(0.0),
##                                     asdmIDLTypes.IDLLength(0.0),
##                                     asdmIDLTypes.IDLLength(0.0),
##                                     asdmIDLTypes.IDLLength(0.0),
##                                     asdmIDLTypes.IDLLength(0.0),
##                                     asdmIDLTypes.IDLLength(0.0),
##                                     1,
##                                     'Pad001',
##                                     asdmIDLTypes.IDLLength(-1601361.555455),
##                                     asdmIDLTypes.IDLLength(-5042191.805932),
##                                     asdmIDLTypes.IDLLength(3554531.803007),
##                                     600)

##     simpleDelayServer.addAntenna(antChar)
  
##     ec = EventCollector()
##     ec.clear()
##     maxIter = 30
##     while ec.numEvent() < 5 and maxIter > 0:
##         maxIter -= 1
##         sleep(1)

##     if maxIter == 0:
##         return (False,"Never recieved any events")

##     if len(ec.eventList[0].antennaDelayEvents) == 0:
##         return (False,"Did not recieve Antenna Delay event")

##     ade = ec.eventList[0].antennaDelayEvents[0]
    
##     if ade.antennaName != 'Alma01':
##         return (False, "Incorrect antenna name returned")
    
##     if ade.stopTime - ade.startTime != 10000000:
##         return (False, "Delay Event has incorrect duration")

##     if len(ade.delayTables) != 1:
##         return (False, "Incorrect number of delay tables")

##     if ade.delayTables[0].startTime != ade.startTime:
##         return (False, "StartTime mismatch")

##     if ade.delayTables[0].stopTime != ade.stopTime:
##         return (False, "StopTime mismatch")

##     if len(ade.delayTables[0].delayValues) != 5:
##         return (False, "Incorrect order polynomial in delay value")

##     return (True,"")

## def DelayTableTest(simpleDelayServer):
##     simpleDelayServer.setPolynomialOrder(3)
    
##     ec = EventCollector()
##     ec.clear()
##     maxIter = 30
##     while ec.numEvent() < 5 and maxIter > 0:
##         maxIter -= 1
##         sleep(1)

##     if maxIter == 0:
##         return (False,"Never recieved any events")

##     if len(ec.eventList[0].antennaDelayEvents) == 0:
##         return (False,"Did not recieve Antenna Delay event")
    
##     ade = ec.eventList[0].antennaDelayEvents[0]

##     if len(ade.delayTables[0].delayValues) != 4:
##         return (False, "Size of delay table does not match polynomial order")

##     for x in ade.delayTables[0].delayValues:
##         if x.totalDelay != 600.0:
##             return (False, "Delay value non equal to cable delay before initialzation")
        
##     startEpoch = TETimeUtil.unix2epoch(time())

##     polyCoef = [1E-6,3.0]
##     delayPoly = Control.SimpleDelayServer.DelayPolynomial(startEpoch.value,\
##                                                           polyCoef)
##     simpleDelayServer.setAntennaDelay('Alma01',delayPoly)
    
##     ec.clear()
##     sleep(3)
##     maxIter = 30
##     while ec.numEvent() < 5 and maxIter > 0:
##         maxIter -= 1
##         sleep(1)

##     if maxIter == 0:
##         return (False,"Never recieved any events")


##     ade = ec.eventList[0].antennaDelayEvents[0]
    
##     for x in ade.delayTables[0].delayValues:
##         model = 0.0
##         for coef in polyCoef:
##             model *= (x.time - startEpoch.value)/1E7
##             model += coef
##         model += 600 # Cable delay term

            
##         if model != x.totalDelay:
##             return (False, "Model does not match Total Delay Event Value")
    
##     return (True,"")
  
#    simpleDelayServer = \
#           client.getDynamicComponent("mySimpleDelayServer",
#                                      "IDL:alma/Control/SimpleDelayServer:1.0",
#                                      "SimpleDelayServer",
#                                      "CONTROL/ACC/cppContainer")

# **************************************************
# MAIN Program
# **************************************************
if __name__ == '__main__':
    suite = unittest.makeSuite(SkyDelayServerTest)
    unittest.TextTestRunner(verbosity=2).run(suite)


   
#
# ___oOo___
