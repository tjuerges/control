/* @(#) $Id: testRtFindKernelModules.cpp,v 1.4 2006/08/31 18:37:38 ramestic Exp
$
 *
 * Copyright (C) 2006
 * Associated Universities, Inc. Washington DC, USA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * Correspondence concerning ALMA Software should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */


#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestCaller.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#include <calcDataManager.h>
#include <calcPipeManager.h>

// These two times must match
#define IERSMJD 55526
#define ACSTIMEREF 135100222801000000LL

class CalcDataManagerProxy : public CalcDataManager {
public:
  CalcDataManagerProxy() : CalcDataManager(){}
  CalcDataManagerProxy( const CalcDataManagerProxy &toCopy) {*this = toCopy;}
  CalcDataManagerProxy &operator=(const CalcDataManagerProxy&){return *this;}


  bool getIERSData(double mjdToday) {
    iersRefMJD = mjdToday;
    return CalcDataManager::getIERSData(mjdToday);
  }
  
  double getIERSRefMJD() {
    return iersRefMJD;
  }
  
  CalcAntenna_t getAntenna(const char* antennaName) {
    for (unsigned int antIdx = 0; antIdx < antennaList_m.size(); antIdx++) { 
      if (!strncmp(antennaName,
                   antennaList_m[antIdx].antennaName,4)) {
        return antennaList_m[antIdx];
      }
    }
    CPPUNIT_FAIL("Unable to find the antenna just added");

    /* This is just to suppress the compiler warnings */
    CalcAntenna_t junk;
    strncpy(junk.antennaName,"Junk",4);
    junk.x = 0;
    junk.y = 0;
    junk.z = 0;
    junk.axisOffset = 0;
    return junk;
  }
  
  void getReference(double& x, double& y, double& z, double& axisOffset){
    x = calcArgs_m.a_x;
    y = calcArgs_m.a_y;
    z = calcArgs_m.a_z;
    axisOffset = calcArgs_m.axis_off_a;
  }

  double getPressureData(const char* antennaName) {
    for (unsigned int antIdx = 0; antIdx < antennaList_m.size(); antIdx++) { 
      if (!strncmp(antennaName,
                   antennaList_m[antIdx].antennaName,4)) {
        return pressure_m[antIdx];
      }
    }
    return -1;
  }

  unsigned int getPolynomialOrder() {
    return polynomialOrder_m;
  }

  std::vector<CalcSource_t>  getSourceTable() {
    return sourceTable_m;
  }

  void fillDelayTable(const std::vector<ACS::Time>& timeList) {
    CalcDataManager::fillDelayTable(timeList);
    gettimeofday(&endOfFillDelayTable, NULL);
  }
  struct timeval endOfFillDelayTable;

private:
  double iersRefMJD;

};
 
class CalcPipeManagerTest : public CppUnit::TestFixture,
                            public CalcPipeManager
{
public:
  CalcPipeManagerTest()
    : CppUnit::TestFixture(),
      CalcPipeManager(new CalcDataManagerProxy())
  {

    int   commOutputDescriptor[2];
    int   commInputDescriptor[2];

    if (pipe(commOutputDescriptor) || pipe(commInputDescriptor)) {
      std::cout << "Error opening the pipes" << std::endl;
    }

    inputFd_m  = commOutputDescriptor[0];
    outputFd_m = commInputDescriptor[1];

    cmdFd = commOutputDescriptor[1];
    rspFd = commInputDescriptor[0];
  }
  
//   CalcPipeManagerTest( const CalcPipeManagerTest &toCopy){*this=toCopy;}
//   CalcPipeManagerTest &operator=(const CalcPipeManagerTest&)
//      {return *this;}
  ~CalcPipeManagerTest(){};

    void setUp() {
    }

  //   void tearDown() {
  //     std::cout << "Teardown now running" << std::endl;
  //   }

  void testReadAll() {
    /* Test reads of between 4 an 4096 bytes */
    for (int idx = 1; idx < 1024; idx++) {
      int src[idx];
      int dst[idx];

      for (int i = 0; i < idx; i++) {
        src[i] = rand();
      }
      
      writeBuffer(src, sizeof(int)*idx);
      
      CPPUNIT_ASSERT_MESSAGE("Failed to read data from pipe",
                             !readAll(dst, sizeof(int)*idx));
      for (int i = 0; i < idx; i++) {
        CPPUNIT_ASSERT_MESSAGE("Data read does not match sent",
                               src[i] == dst[i]);
      }
    }
  }

  void testDelayedRead() {
    /* Test a read which has a break in it */
    const int NUM_INTEGERS = 4096;
    int src[NUM_INTEGERS];   
    for (int i = 0; i < NUM_INTEGERS; i++) {
      src[i] = rand();
    }

    pid_t pid = fork();
    if (pid == 0) {
      writeBuffer(src, sizeof(int)*NUM_INTEGERS/2); // write half the buffer
      sleep(5); // Now wait 5 sec
      // write second half the buffer
      writeBuffer(&src[NUM_INTEGERS/2], sizeof(int)*NUM_INTEGERS/2);
      exit(0);
    }

    int dst[NUM_INTEGERS]; 
    CPPUNIT_ASSERT_MESSAGE("Failed to read all data from pipe",
                           !readAll(dst, sizeof(int)*NUM_INTEGERS));
    /* Just for completeness check the data integrity too */
    for (int i = 0; i < NUM_INTEGERS; i++) {
      CPPUNIT_ASSERT_MESSAGE("Data read does not match sent",
                             src[i] == dst[i]);
    }
  }

  void testWriteAll() {
    /* Verify that we can write data back through the pipe */
    const int MAX_BUFFER_SIZE = 32768;
    for (int idx = 1; idx < MAX_BUFFER_SIZE; idx*=2) {
      int src[idx];
      int dst[idx];

      for (int i = 0; i < idx; i++) {
        src[i] = rand();
      }
      CPPUNIT_ASSERT_MESSAGE("Failed writing data to buffer",
                             !writeAll(src, sizeof(int)*idx));
      
      readBuffer(dst, sizeof(int)*idx);
      for (int i = 0; i < idx; i++) {
        CPPUNIT_ASSERT_MESSAGE("Data read does not match sent",
                               src[i] == dst[i]);
      }
    }
  }


  void testFailedWrite() {
    /* Verify that we can write data back through the pipe */
    const int BUFFER_SIZE = 32768;

    int src[BUFFER_SIZE];

    for (int i = 0; i < BUFFER_SIZE; i++) {
      src[i] = rand();
    }
    
    close(rspFd);
    CPPUNIT_ASSERT_MESSAGE("Error detecting closed pipe",
                           writeAll(src, sizeof(int)*BUFFER_SIZE));
      
  }

  void testReturnSuccess() {
    returnSuccess();
    CPPUNIT_ASSERT_MESSAGE("Incorrect State Returned",
                           getStateFromPipe() == Success);
  }

  void testReturnExit() {
    returnExit();
    CPPUNIT_ASSERT_MESSAGE("Incorrect State Returned",
                           getStateFromPipe() == Exit);
  }

  void testReturnError() {
    returnError();
    CPPUNIT_ASSERT_MESSAGE("Incorrect State Returned",
                           getStateFromPipe() == Error);
  }

  void testInitializeCalc() {
    double targetMJD = IERSMJD;
    
    writeBuffer(&targetMJD, sizeof(double));
    initializeCalc();
    CPPUNIT_ASSERT_MESSAGE("Incorrect State Returned",
                           getStateFromPipe() == Success);

    CPPUNIT_ASSERT_MESSAGE("Target MJD not correctly sent to Data Manager",
                           sortaEqual(targetMJD,
     dynamic_cast<CalcDataManagerProxy*>(dataManager_m)->getIERSRefMJD()));
  }

  void testInitializeCalcFail() {
    double targetMJD = 65832.6;
    writeBuffer(&targetMJD, sizeof(double));
    initializeCalc();
    CPPUNIT_ASSERT_MESSAGE("Incorrect State Returned",
                           getStateFromPipe() == Error);
  }


  void testAddAntenna() {
    CalcAntenna_t newAnt;
    createAntenna(newAnt,"DA41", 1000000, 2000000, -3000000, 1.2E-6);

    writeBuffer(&newAnt, sizeof(CalcAntenna_t));
    addAntenna();

    CPPUNIT_ASSERT_MESSAGE("Incorrect State Returned",
                           getStateFromPipe() == Success);
    
    CalcAntenna_t returnAnt =  
      dynamic_cast<CalcDataManagerProxy*>(dataManager_m)->getAntenna("DA41");

    CPPUNIT_ASSERT_MESSAGE("Antenna Names Don't Match",
                           !strncmp(newAnt.antennaName,
                                    returnAnt.antennaName,4));
    CPPUNIT_ASSERT_MESSAGE("X Positions do not match",
                           sortaEqual(newAnt.x, returnAnt.x));
    CPPUNIT_ASSERT_MESSAGE("Y Positions do not match",
                           sortaEqual(newAnt.y, returnAnt.y));
    CPPUNIT_ASSERT_MESSAGE("Z Positions do not match",
                           sortaEqual(newAnt.z, returnAnt.z));
    CPPUNIT_ASSERT_MESSAGE("Axis Offsets do not match",
                           sortaEqual(newAnt.axisOffset,
                                      returnAnt.axisOffset));
  }

  void testSetReference() {
    CalcAntenna_t refAnt;
    createAntenna(refAnt,"Ref",100,200,-300,1.1);
    writeBuffer(&refAnt, sizeof(CalcAntenna_t));
    setReference();
    CPPUNIT_ASSERT_MESSAGE("Incorrect State Returned",
                           getStateFromPipe() == Success);

    double x;
    double y;
    double z;
    double axisOffset;
    dynamic_cast<CalcDataManagerProxy*>(dataManager_m)->
      getReference(x, y, z, axisOffset);
    CPPUNIT_ASSERT_MESSAGE("X Positions do not match",
                           sortaEqual(refAnt.x, x));
    CPPUNIT_ASSERT_MESSAGE("Y Positions do not match",
                           sortaEqual(refAnt.y, y));
    CPPUNIT_ASSERT_MESSAGE("Z Positions do not match",
                           sortaEqual(refAnt.z, z));
    CPPUNIT_ASSERT_MESSAGE("Axis Offsets do not match",
                           sortaEqual(refAnt.axisOffset, axisOffset));
  }

  void testSetAntennaPressure() {
    CalcAntenna_t newAnt;
    createAntenna(newAnt,"DV01", 1000000, 2000000, -3000000, 1.2E-6);
    writeBuffer(&newAnt, sizeof(CalcAntenna_t));
    addAntenna();

    CalcPressureData_t pressureData;
    strncpy(pressureData.antennaName, "DV01", 4);
    pressureData.pressure = 666.0;

    writeBuffer(&pressureData, sizeof(CalcPressureData_t));
    setAntennaPressure();
    CPPUNIT_ASSERT_MESSAGE("Incorrect State Returned",
                           getStateFromPipe() == Success);

    double  pressureValue =  dynamic_cast<CalcDataManagerProxy*>
      (dataManager_m)->getPressureData("DV01");
    CPPUNIT_ASSERT_MESSAGE("Unable to store pressure correctly",
                           pressureValue = 666);
  }


  void testSetPolynomicalOrder() {
    unsigned int polynomialOrder = 696;
    
    writeBuffer(&polynomialOrder, sizeof(unsigned int ));
    setPolynomialOrder();
    CPPUNIT_ASSERT_MESSAGE("Incorrect State Returned",
                           getStateFromPipe() == Success);
    CPPUNIT_ASSERT_MESSAGE("Incorrect Polynomial Order Returned",
                           polynomialOrder == 
                           dynamic_cast<CalcDataManagerProxy*>(dataManager_m)->
                           getPolynomialOrder());
    
  }

  void testSetSource() {
    CalcDataManagerProxy* proxy = 
      dynamic_cast<CalcDataManagerProxy*>(dataManager_m);

    CalcSource_t newSource;
    unsigned int listSize = 1;

    newSource.RA = 0.1;
    newSource.Dec = -0.1;
    newSource.dRA = 0.01;
    newSource.dDec = -0.01;
    newSource.Epoch = 54000;
    newSource.Parallax = 0.0001;


    writeBuffer(&listSize, sizeof(unsigned int));
    writeBuffer(&newSource, sizeof(CalcSource_t));
    setSource();
    CPPUNIT_ASSERT_MESSAGE("Incorrect State Returned",
                           getStateFromPipe() == Success);

    // Check that the source table is of size polynomialOrder+1 
    std::vector<CalcSource_t> sourceTable = proxy->getSourceTable();
    unsigned int polynomialOrder = proxy->getPolynomialOrder();

    CPPUNIT_ASSERT_MESSAGE("Source table length incorrect",
                           sourceTable.size() == polynomialOrder +1 );

    for (unsigned int idx = 0; idx < sourceTable.size(); idx++) {
      CPPUNIT_ASSERT_MESSAGE("Source RA is not set correctly",
                             sortaEqual(sourceTable[idx].RA, newSource.RA));
      CPPUNIT_ASSERT_MESSAGE("Source Dec is not set correctly",
                             sortaEqual(sourceTable[idx].Dec, newSource.Dec));
      CPPUNIT_ASSERT_MESSAGE("Source dRA is not set correctly",
                             sortaEqual(sourceTable[idx].dRA, newSource.dRA));
      CPPUNIT_ASSERT_MESSAGE("Source dDec is not set correctly",
                             sortaEqual(sourceTable[idx].dDec,newSource.dDec));
      CPPUNIT_ASSERT_MESSAGE("Source Epoch is not set correctly",
                             sortaEqual(sourceTable[idx].Epoch, 
                                        newSource.Epoch));
      CPPUNIT_ASSERT_MESSAGE("Source Parallax is not set correctly",
                             sortaEqual(sourceTable[idx].Parallax,
                                        newSource.Parallax));
    }
  }

  void testSetSourceList() {
    CalcDataManagerProxy* proxy = 
      dynamic_cast<CalcDataManagerProxy*>(dataManager_m);

    unsigned int polynomialOrder = proxy->getPolynomialOrder();

    std::vector<CalcSource_t> sourceList;
    for (unsigned int idx = 0; idx <= polynomialOrder; idx++) {
      CalcSource_t newSource;
      newSource.RA = 0.1 * idx;
      newSource.Dec = -0.1 * idx;
      newSource.dRA = 0.01 * idx;
      newSource.dDec = -0.01 * idx;
      newSource.Epoch = 54000 + idx;
      newSource.Parallax = 0.0001 * idx;
      sourceList.push_back(newSource);
    }

    unsigned int listSize = sourceList.size();
    writeBuffer(&listSize, sizeof(unsigned int));
    
    for (unsigned int idx = 0; idx < listSize; idx++) {
      writeBuffer(&sourceList[idx], sizeof(CalcSource_t));
    }
    setSource();
    CPPUNIT_ASSERT_MESSAGE("Incorrect State Returned",
                           getStateFromPipe() == Success);


    std::vector<CalcSource_t> sourceTable = proxy->getSourceTable();
    CPPUNIT_ASSERT_MESSAGE("Source table length incorrect",
                           sourceTable.size() == polynomialOrder +1 );

    for (unsigned int idx = 0; idx <= polynomialOrder; idx++) {
      CPPUNIT_ASSERT_MESSAGE("RA incorrectly set in source table",
                             sortaEqual(sourceList[idx].RA,
                                        sourceTable[idx].RA));
      CPPUNIT_ASSERT_MESSAGE("Dec incorrectly set in source table",
                             sortaEqual(sourceList[idx].Dec,
                                        sourceTable[idx].Dec));
      CPPUNIT_ASSERT_MESSAGE("dRA incorrectly set in source table",
                             sortaEqual(sourceList[idx].dRA,
                                        sourceTable[idx].dRA));
      CPPUNIT_ASSERT_MESSAGE("dDec incorrectly set in source table",
                             sortaEqual(sourceList[idx].dDec,
                                        sourceTable[idx].dDec));
      CPPUNIT_ASSERT_MESSAGE("Epoch incorrectly set in source table",
                             sortaEqual(sourceList[idx].Epoch,
                                        sourceTable[idx].Epoch));
      CPPUNIT_ASSERT_MESSAGE("Parallax incorrectly set in source table",
                             sortaEqual(sourceList[idx].Parallax,
                                        sourceTable[idx].Parallax));
    }
  }

  void testSetSourceListError() {
    CalcDataManagerProxy* proxy = 
      dynamic_cast<CalcDataManagerProxy*>(dataManager_m);

    unsigned int polynomialOrder = proxy->getPolynomialOrder() +2;

    std::vector<CalcSource_t> sourceList;
    for (unsigned int idx = 0; idx <= polynomialOrder; idx++) {
      CalcSource_t newSource;
      newSource.RA = 0.1 * idx;
      newSource.Dec = -0.1 * idx;
      newSource.dRA = 0.01 * idx;
      newSource.dDec = -0.01 * idx;
      newSource.Epoch = 54000 + idx;
      newSource.Parallax = 0.0001 * idx;
      sourceList.push_back(newSource);
    }

    unsigned int listSize = sourceList.size();
    writeBuffer(&listSize, sizeof(unsigned int));
    
    for (unsigned int idx = 0; idx < listSize; idx++) {
      writeBuffer(&sourceList[idx], sizeof(CalcSource_t));
    }
    setSource();
    CPPUNIT_ASSERT_MESSAGE("Incorrect State Returned",
                           getStateFromPipe() == Error);

  }

  void testGetDelays() {
    CalcDataManager::setCalcEnvironment();
    CalcDataManagerProxy* proxy = 
      dynamic_cast<CalcDataManagerProxy*>(dataManager_m);
    const double MAX_NUM_ANTENNA = 66;
    srand(time(0));
    CalcSource_t newSource;
    CalcAntenna_t newRef;


    /* We need to set up the dataManager so we will get real results */
    newSource.RA = 0.1;
    newSource.Dec = -0.1;
    newSource.dRA = 0.00;
    newSource.dDec = 0.00;
    newSource.Epoch = 54000;
    newSource.Parallax = 0.0;
    
    double refX = -1601361.760577;
    double refY = -5042192.535329;
    double refZ = 3554531.519329;

    createAntenna(newRef, "Ref", refX, refY, refZ, 0.0);
    proxy->setReference(newRef);

    //double targetMJD = 54886.75;
    double targetMJD = IERSMJD;
    proxy->getIERSData(targetMJD);

    //for (int idx = 0; idx < NUM_ANTENNA; idx++) {
    /* Create antennas within a 15 km circle of the reference and
       withing 500 m vertically the axis offset is less than 1 mm */
    for (int antIdx = 0; antIdx < MAX_NUM_ANTENNA; antIdx++) {
      CalcAntenna_t newAntenna;
      char name[5];
      sprintf(name,"DV%02d",antIdx);
      createAntenna(newAntenna, name, refX +(20000*(drand48()-0.5)),
                    refY + (20000*(drand48()-0.5)),refZ+(1000*(drand48()-0.5)),
                    (drand48()-0.5)*0.002);
      proxy->addAntenna(newAntenna);

      for (int polyOrder = 3; polyOrder <7; polyOrder++) {
        proxy->setPolynomialOrder(polyOrder); 
        proxy ->setSource(newSource); // After changing polynomial reset source
        unsigned int listSize = polyOrder +1;
        std::vector<ACS::Time> timeList;
    
        fillTimeList(timeList, 60, listSize);

        writeBuffer(&listSize,sizeof(unsigned int));
        for (unsigned int idx = 0; idx < timeList.size(); idx++) {
          writeBuffer(&timeList[idx], sizeof(ACS::Time));
        }

        // The getDelays() will crash (not throw) if the MJD above is not right
        getDelays();
        CPPUNIT_ASSERT_MESSAGE("Incorrect State Returned",
                               getStateFromPipe() == Success);
        
        unsigned int returnedTableSize;
        readBuffer(&returnedTableSize, sizeof(unsigned int));
        CPPUNIT_ASSERT_MESSAGE("Delay table size returned is incorrect",
                               static_cast<int>(returnedTableSize) == 
                               proxy->getDelayTableSize());
        CPPUNIT_ASSERT_MESSAGE("Table Size calculation incorrect",
                               static_cast<int>(returnedTableSize) ==
                               (antIdx+1) * (polyOrder +1));

        /* Now read and check the delay table the az table and the eltable */
        double  delayResult[returnedTableSize];
        double* delayTable = proxy->getDelayTable();
        readBuffer(&delayResult, returnedTableSize*sizeof(double));
    
        for (unsigned int idx = 0; idx < returnedTableSize; idx++){
          CPPUNIT_ASSERT_MESSAGE("Delay Values do not match",
                                 sortaEqual(delayResult[idx],delayTable[idx]));
        }
    
        double  azResult[returnedTableSize];
        double* azTable = proxy->getAzTable();
        readBuffer(&azResult, returnedTableSize*sizeof(double));
        
        for (unsigned int idx = 0; idx < returnedTableSize; idx++){
          CPPUNIT_ASSERT_MESSAGE("Azimuth Values do not match",
                                 sortaEqual(azResult[idx],azTable[idx]));
        }
        
        double elResult[returnedTableSize];
        double* elTable = proxy->getElTable();
        readBuffer(&elResult, returnedTableSize * sizeof(double));
        
        for (unsigned int idx = 0; idx < returnedTableSize; idx++){
          CPPUNIT_ASSERT_MESSAGE("Elevation Values do not match",
                                 sortaEqual(elResult[idx],elTable[idx]));
        }

        double dryAtmResult[returnedTableSize];
        double* dryAtmTable = proxy->getDryAtmTable();
        readBuffer(&dryAtmResult, returnedTableSize * sizeof(double));
        
        for (unsigned int idx = 0; idx < returnedTableSize; idx++){
          CPPUNIT_ASSERT_MESSAGE("Dry Atmosphere Values do not match",
                                 sortaEqual(dryAtmResult[idx],
                                            dryAtmTable[idx]));
        }

        double wetAtmResult[returnedTableSize];
        double* wetAtmTable = proxy->getWetAtmTable();
        readBuffer(&wetAtmResult, returnedTableSize * sizeof(double));
        
        for (unsigned int idx = 0; idx < returnedTableSize; idx++){
          CPPUNIT_ASSERT_MESSAGE("Wet Atmosphere Values do not match",
                                 sortaEqual(wetAtmResult[idx],
                                            wetAtmTable[idx]));
        }
      }
    }
  }

  void testGetDelaysFailures() {
    CalcDataManagerProxy* proxy = 
      dynamic_cast<CalcDataManagerProxy*>(dataManager_m);
    unsigned int listSize = proxy->getPolynomialOrder() +1;
    std::vector<ACS::Time> timeList;
    
    fillTimeList(timeList, 60, listSize);

    writeBuffer(&listSize,sizeof(unsigned int));
    for (unsigned int idx = 0; idx < timeList.size(); idx++) {
      writeBuffer(&timeList[idx], sizeof(ACS::Time));
    }

    /* This will fail because we don't have any sources defined */
    getDelays();

    CPPUNIT_ASSERT_MESSAGE("Incorrect State Returned",
                           getStateFromPipe() == Error);
   }

  void testServiceLoop() {

  }
  
  void testTiming(){
    CalcDataManager::setCalcEnvironment();
    CalcDataManagerProxy* proxy = 
      dynamic_cast<CalcDataManagerProxy*>(dataManager_m);
    const double MAX_NUM_ANTENNA = 66;

    srand(time(0));
    CalcSource_t newSource;
    CalcAntenna_t newRef;


    /* We need to set up the dataManager so we will get real results */
    newSource.RA = 0.1;
    newSource.Dec = -0.1;
    newSource.dRA = 0.00;
    newSource.dDec = 0.00;
    newSource.Epoch = 54000;
    newSource.Parallax = 0.0;
    
    double refX = -1601361.760577;
    double refY = -5042192.535329;
    double refZ = 3554531.519329;

    createAntenna(newRef, "Ref", refX, refY, refZ, 0.0);
    proxy->setReference(newRef);

    double targetMJD = IERSMJD;
    proxy->getIERSData(targetMJD);


    std::cout << "\n\n@Delay Table Transfer Timing Test Results:" << std::endl;
    std::cout << "@\t\t\t\t\tPolynomial Order" << std::endl;
    std::cout << "@Num Ant.\t3\t\t4\t\t5\t\t6" << std::endl;


    //for (int idx = 0; idx < NUM_ANTENNA; idx++) {
    /* Create antennas within a 15 km circle of the reference and
       withing 500 m vertically the axis offset is less than 1 mm */
    for (int antIdx = 0; antIdx < MAX_NUM_ANTENNA; antIdx++) {
      CalcAntenna_t newAntenna;
      char name[5];
      sprintf(name,"DV%02d",antIdx);
      createAntenna(newAntenna, name, refX +(20000*(drand48()-0.5)),
                    refY + (20000*(drand48()-0.5)),refZ+(1000*(drand48()-0.5)),
                    (drand48()-0.5)*0.002);
      proxy->addAntenna(newAntenna);
      if ((antIdx % 6 ==5) ||(antIdx == 0)) {
        std::cout << "@" << antIdx +1 << "\t" <<std::setw(3)
                  << std::scientific;
        for (int polyOrder = 3; polyOrder <7; polyOrder++) {
          struct timeval tableDataReceived;

          proxy->setPolynomialOrder(polyOrder); 
          proxy ->setSource(newSource);//After changing polynomial reset source
          unsigned int listSize = polyOrder +1;
          std::vector<ACS::Time> timeList;
          fillTimeList(timeList, 60, listSize);
      
          writeBuffer(&listSize,sizeof(unsigned int));
          for (unsigned int idx = 0; idx < timeList.size(); idx++) {
            writeBuffer(&timeList[idx], sizeof(ACS::Time));
          }

          getDelays();
          CPPUNIT_ASSERT_MESSAGE("Incorrect State Returned",
                                 getStateFromPipe() == Success);
          unsigned int returnedTableSize;
          readBuffer(&returnedTableSize, sizeof(unsigned int));
          /* Now read and check the delay table the az table and the eltable */
          double delayResult[returnedTableSize];
          double azResult[returnedTableSize];
          double elResult[returnedTableSize];
          double dryAtmResult[returnedTableSize];
          double wetAtmResult[returnedTableSize];
          
          readBuffer(&delayResult, returnedTableSize*sizeof(double));
          readBuffer(&azResult, returnedTableSize*sizeof(double));
          readBuffer(&elResult, returnedTableSize * sizeof(double));
          readBuffer(&dryAtmResult, returnedTableSize * sizeof(double));
          readBuffer(&wetAtmResult, returnedTableSize * sizeof(double));
          gettimeofday(&tableDataReceived,NULL);
          double elapsedTime = 
            (tableDataReceived.tv_sec - proxy->endOfFillDelayTable.tv_sec) +
            ((tableDataReceived.tv_usec - proxy->endOfFillDelayTable.tv_usec)
             *1E-6);
          std::cout << "\t"  << std::setprecision(2)
                    << elapsedTime;
        }
        std::cout << std::endl;
      }
     
    }
  }

  CPPUNIT_TEST_SUITE(CalcPipeManagerTest);
  CPPUNIT_TEST(testReadAll);
  CPPUNIT_TEST(testDelayedRead);
  CPPUNIT_TEST(testWriteAll);
  CPPUNIT_TEST(testFailedWrite);
  CPPUNIT_TEST(testReturnSuccess);
  CPPUNIT_TEST(testReturnExit);
  CPPUNIT_TEST(testReturnError);
  CPPUNIT_TEST(testInitializeCalc);
  CPPUNIT_TEST(testInitializeCalcFail);
  CPPUNIT_TEST(testAddAntenna);
  CPPUNIT_TEST(testSetReference);
  CPPUNIT_TEST(testSetAntennaPressure);
  CPPUNIT_TEST(testSetPolynomicalOrder);
  CPPUNIT_TEST(testSetSource);
  CPPUNIT_TEST(testSetSourceList);
  CPPUNIT_TEST(testSetSourceListError);
  CPPUNIT_TEST(testGetDelays);
  CPPUNIT_TEST(testGetDelaysFailures);
  CPPUNIT_TEST(testTiming);
  CPPUNIT_TEST_SUITE_END();
  

private:
  /* Helper methods to make testing easier */
  void writeBuffer(void* buffer, size_t count) {
    int bytesToWrite =  count;
    while (bytesToWrite > 0) {
      int bytesWritten = write(cmdFd, buffer, bytesToWrite);
      if (bytesWritten == -1) {
        /* Error on the write */
        switch (errno){
        case EINTR:
          continue;
          break;
        default: //EPIPE or other error received 
          CPPUNIT_FAIL(strerror(errno));
        }
      } else {
        bytesToWrite -= bytesWritten;
        buffer       += bytesWritten;
      }
    }
  }

  void readBuffer(void* buffer, size_t count) {
    int bytesToRead =  count;

    while (bytesToRead > 0) {
      int bytesRead = read(rspFd, buffer, bytesToRead);
      if (bytesRead < 0) {
        /* Error on the read */
        switch (errno){
        case EINTR:
          continue;
          break;
        default: //EPIPE or other error received 
          CPPUNIT_FAIL(strerror(errno));
        }
      } else {
        bytesToRead -= bytesRead;
        buffer      += bytesRead;
      }
    }
  }

  CalcStatusType_t getStateFromPipe() {
    CalcStatusType_t status;
    readBuffer(reinterpret_cast<char*>(&status), sizeof(CalcStatusType_t));
    return status;
  } 


  void fillTimeList(std::vector<ACS::Time>& timeList, double duration,
                    int numTimes) {
    timeList.clear();  
    timeList.push_back(static_cast<ACS::Time>(ACSTIMEREF));
    ACS::Time increment = static_cast<ACS::Time>(duration * 1E7/(numTimes-1));
    for (int idx = 1; idx < numTimes; idx++) {
      timeList.push_back(timeList[idx-1] + increment);
    }
  }
  bool sortaEqual(double a, double b, double threshold = 1E-14) {
    if (fabs(a-b) < threshold)
      return true;
    std::cout << "Sorta Equal Method Difference is: " << fabs(a-b)<< std::endl;
    return false;
  }

  void createAntenna(CalcAntenna_t& newAnt, char* name, 
                     double x, double y, double z, double k){
    strncpy(newAnt.antennaName, name, 4);
    newAnt.x = x;
    newAnt.y = y;
    newAnt.z = z;
    newAnt.axisOffset = k;
  }

  /* ---------------- Variables for testing --------------- */
  int cmdFd; // File descriptor for writing commands into
  int rspFd; // File descriptor for reading commands from

  unsigned int polynomialOrder_m;  // Needed for calcDelayIndex
}; // End of Test Class

int main( int argc, char **argv)
{
  CppUnit::TextUi::TestRunner runner;
  runner.addTest(CalcPipeManagerTest::suite());
  runner.run();
  return 0;
}
