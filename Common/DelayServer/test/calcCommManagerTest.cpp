/* @(#) $Id: testRtFindKernelModules.cpp,v 1.4 2006/08/31 18:37:38 ramestic Exp
$
 *
 * Copyright (C) 2006
 * Associated Universities, Inc. Washington DC, USA.
 *
 * This library is free software; you can redistribute it and/or modify 
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * Correspondence concerning ALMA Software should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */


#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestCaller.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#include <calcCommManager.h>
#include <pthread.h>

/* The CalcCommManager is hard to test, it does 2 things, one is the
   IPC over the pipe and one is the management of the actual process
   The strategy here is to split the tests:
   
   * The CalcProcessManagementTest tests the startup, shutdown, cleanup
     etc of the child process

   * The CalcCommManagerTest tests the IPC

   At least thats the plan JSK 2009-02-26
*/

class CalcProcessManagementTest : public CppUnit::TestFixture,
                                  public CalcCommManager
{
public:
  CalcProcessManagementTest() : CppUnit::TestFixture(),
                                CalcCommManager("calcTestProcess"){}
  ~CalcProcessManagementTest(){}

  void testInstanciation() {
  }

  void setUp(){
    /* We need to add ../bin to our path */
    char* initialPath = getenv("PATH");
    char* localExecutables = strdup("../bin:");
    char newPath[strlen(initialPath) + strlen(localExecutables) +1];
    strcpy(newPath, localExecutables);
    strcat(newPath, initialPath);
    setenv("PATH", newPath, 1);
  }

  void testCheckCalcProcess(){
    CPPUNIT_ASSERT_MESSAGE("Child Process ID is not -1 at beginning of test",
                           childProcessID_m  == -1);

    childProcessID_m = fork();
    if(childProcessID_m == 0){
       /* This is the child process, hold it open for 2 seconds then close*/
       sleep(2);
       exit(0);
    }
    sleep(1);
    CPPUNIT_ASSERT_MESSAGE("Child process has exit early!",
                           !checkCalcProcess());
    sleep(2);
    CPPUNIT_ASSERT_MESSAGE("Child process failed to exit",
                           checkCalcProcess());
    childProcessID_m = -1;
  }

  void testStartCalcProcess() {
    CalcRequestType_t request;

    /* Ensure that the process is not already running */
    CPPUNIT_ASSERT_MESSAGE("Child process already running",
                           childProcessID_m == -1)   ;
    startCalcProcess();
    CPPUNIT_ASSERT_MESSAGE("Failed to start Calc Process",
                           !checkCalcProcess());

    /* Use the antenna case as a loopback to ensure that
       the child is really running */
    request = Antenna;
    writeAll(&request, sizeof(CalcRequestType_t));

    CPPUNIT_ASSERT_MESSAGE("Child Process Responded incorrectly",
                           getChildMessage() == Success);
   
    /* Send the exit command */
    request = ExitRequest;
    writeAll(&request, sizeof(CalcRequestType_t));
    
    CPPUNIT_ASSERT_MESSAGE("Child Process Responded incorrectly",
                           getChildMessage() == Exit);
    /* Give time for child to close.  I hate sleeps, but to keep this
     * test and the close test independent, I don't see another good
     * solution.
     */
    sleep(2);
    
    CPPUNIT_ASSERT_MESSAGE("Child process still running",
                           checkCalcProcess());
    childProcessID_m = -1;// To prevent issues at shutdown
    
  }
  
  void testCloseCalcProcess(){
    /* Test behavior when the child is not running */
    closeCalcProcess();
    CPPUNIT_ASSERT_MESSAGE("childProcessID is not -1",
                            childProcessID_m == -1);
    CPPUNIT_ASSERT_MESSAGE("output file descriptor is not -1",
                            outputFd_m == -1);
    CPPUNIT_ASSERT_MESSAGE("input file descriptor is not -1",
                            inputFd_m  == -1);
    
    startCalcProcess();
    CPPUNIT_ASSERT_MESSAGE("Failed to start CALC process",
                           !checkCalcProcess());
    CPPUNIT_ASSERT_MESSAGE("childProcessID is -1",
                            childProcessID_m != -1);
    CPPUNIT_ASSERT_MESSAGE("output file descriptor is -1",
                            outputFd_m != -1);
    CPPUNIT_ASSERT_MESSAGE("input file descriptor is -1",
                            inputFd_m  != -1);

    closeCalcProcess();
    CPPUNIT_ASSERT_MESSAGE("childProcessID is not -1",
                            childProcessID_m == -1);
    CPPUNIT_ASSERT_MESSAGE("output file descriptor is not -1",
                            outputFd_m == -1);
    CPPUNIT_ASSERT_MESSAGE("input file descriptor is not -1",
                            inputFd_m  == -1);
 }

  void testClosingDeadCalcProcess(){
    startCalcProcess();
    CPPUNIT_ASSERT_MESSAGE("Failed to start CALC process",
                           !checkCalcProcess());
    CPPUNIT_ASSERT_MESSAGE("childProcessID is -1",
                            childProcessID_m != -1);
    CPPUNIT_ASSERT_MESSAGE("output file descriptor is -1",
                            outputFd_m != -1);
    CPPUNIT_ASSERT_MESSAGE("input file descriptor is -1",
                            inputFd_m  != -1);

    CalcRequestType_t request = Reference;
    /* Use the Reference case to cause the CALC process to exit roughly
       this is intended to simulate a crash */
    writeAll(&request, sizeof(CalcRequestType_t));

    sleep(5); // For test ensure we have time for process to crash

    closeCalcProcess();
    CPPUNIT_ASSERT_MESSAGE("childProcessID is not -1",
                            childProcessID_m == -1);
    CPPUNIT_ASSERT_MESSAGE("output file descriptor is not -1",
                            outputFd_m == -1);
    CPPUNIT_ASSERT_MESSAGE("input file descriptor is not -1",
                            inputFd_m  == -1);
  }


  void testClosingStuckCalcProcess(){
    startCalcProcess();
    CPPUNIT_ASSERT_MESSAGE("Failed to start CALC process",
                           !checkCalcProcess());
    CPPUNIT_ASSERT_MESSAGE("childProcessID is -1",
                            childProcessID_m != -1);
    CPPUNIT_ASSERT_MESSAGE("output file descriptor is -1",
                            outputFd_m != -1);
    CPPUNIT_ASSERT_MESSAGE("input file descriptor is -1",
                           inputFd_m  != -1);

    CalcRequestType_t request = PolynomialOrder;
    writeAll(&request, sizeof(CalcRequestType_t));

    closeCalcProcess();
    CPPUNIT_ASSERT_MESSAGE("childProcessID is not -1",
                            childProcessID_m == -1);
    CPPUNIT_ASSERT_MESSAGE("output file descriptor is not -1",
                            outputFd_m == -1);
    CPPUNIT_ASSERT_MESSAGE("input file descriptor is not -1",
                            inputFd_m  == -1);
  }
 
  CPPUNIT_TEST_SUITE(CalcProcessManagementTest);
  CPPUNIT_TEST(testCheckCalcProcess);
  CPPUNIT_TEST(testStartCalcProcess);
  CPPUNIT_TEST(testCloseCalcProcess);
  CPPUNIT_TEST(testClosingDeadCalcProcess);
  CPPUNIT_TEST(testClosingStuckCalcProcess);
  CPPUNIT_TEST_SUITE_END();
 
};


class CalcProcessProxy {
public:
  CalcProcessProxy(int inputStream, int outputStream):
    returnState(Success),
    timeTable_m(NULL)
  {
    inputFd_m  = inputStream;
    outputFd_m = outputStream;
    returnSuccess();
  }

  ~CalcProcessProxy(){
    close(inputFd_m);
    close(outputFd_m);
  }

  void setReturnState(CalcStatusType_t newReturnState) {
    returnState = newReturnState;
  }

  static void* handleRequest(void* obj){
    CalcProcessProxy* object = static_cast<CalcProcessProxy*>(obj);
    object->handleRequest();
    pthread_exit(NULL);
  }

  void spawnHandler(){
    pthread_create(&childThread, NULL, &handleRequest,
                   static_cast<void*>(this));
  }

  void joinThread(){
    pthread_join(childThread, NULL);
  }

  void proxyAddAntenna() {
    readBuffer(&lastAntenna, sizeof(CalcAntenna_t));
    returnCurrentState();
  }

  CalcAntenna_t getLastAntenna() {
    return lastAntenna;
  }

  void proxySetReference() {
    readBuffer(&lastReference, sizeof(CalcAntenna_t));
    returnCurrentState();
  }

  CalcAntenna_t getLastReference(){
    return lastReference;
  }

  void proxySetPressure() {
    readBuffer(&lastPressure, sizeof(CalcPressureData_t));
    returnCurrentState();
  }

  CalcPressureData_t getLastPressureData(){
    return lastPressure;
  }

  void proxySetPolynomialOrder() {
    readBuffer(&polynomialOrder, sizeof(unsigned int));
    returnCurrentState();
  }

  unsigned int getLastPolynomialOrder(){
    return polynomialOrder;
  }

  void proxySetSource() {
    unsigned int      listSize;
    readBuffer(&listSize, sizeof(unsigned int));
    sourceList.clear();
    for (unsigned int idx = 0; idx < listSize; idx++){
      CalcSource_t newSource;
      readBuffer(&newSource, sizeof(CalcSource_t));
      sourceList.push_back(newSource);
    }
    returnCurrentState();
  }

  std::vector<CalcSource_t> getLastSourceList() {
    return sourceList;
  }

  void proxyInitializeCalc() {
    readBuffer(&lastMJD, sizeof(double));
    returnCurrentState();
  }

  double getLastMJD(){
    return lastMJD;
  }

  void setNumAntenna(unsigned int numAntenna) {
    numAntenna_m = numAntenna;
  }

  void proxyGetDelays(){
    readBuffer(&timeTableSize_m, sizeof(unsigned int));

    delete timeTable_m;
    timeTable_m = new ACS::Time[timeTableSize_m];
    readBuffer(timeTable_m, sizeof(ACS::Time)*timeTableSize_m);
    returnCurrentState();
    
    unsigned int delayTableSize = timeTableSize_m * numAntenna_m; 

    writeBuffer(&delayTableSize, sizeof(unsigned int));
    double delayTable[delayTableSize];
    double azTable[delayTableSize];
    double elTable[delayTableSize];
    double dryAtmTable[delayTableSize];
    double wetAtmTable[delayTableSize];

    for (unsigned int idx = 0; idx < delayTableSize; idx++){
      delayTable[idx] = idx;
      azTable[idx]    = 0.1*idx;
      elTable[idx]    = -0.1*idx;
      dryAtmTable[idx]    = 10.0*idx;
      wetAtmTable[idx]    = 11.0*idx;
    }

    writeBuffer(delayTable, delayTableSize*sizeof(double));
    writeBuffer(azTable, delayTableSize*sizeof(double));
    writeBuffer(elTable, delayTableSize*sizeof(double));
    writeBuffer(dryAtmTable, delayTableSize*sizeof(double));
    writeBuffer(wetAtmTable, delayTableSize*sizeof(double));
  }

  ACS::Time* getTimeTable(){
    return timeTable_m;
  }

  unsigned int getTimeTableSize() {
    return timeTableSize_m;
  }

  void handleRequest() {
    CalcRequestType_t requestType;
    readBuffer(&requestType, sizeof(CalcRequestType_t));
    switch (requestType) {
    case ExitRequest:
      return;
      break;
    case Antenna:
      proxyAddAntenna();
      break;
    case Reference:
      proxySetReference();
      break;
    case AntennaPressure:
      proxySetPressure();
      break;
    case PolynomialOrder:
      proxySetPolynomialOrder();
      break;
    case Source:
      proxySetSource();
      break;
    case Delays:
      proxyGetDelays();
      break;
    case Initialize:
      proxyInitializeCalc();
      break;
    default:
      return; // Receive unknown request.
    }
  }

private:
  void writeBuffer(void* buffer, size_t count) {

    int bytesToWrite =  count;
    while (bytesToWrite > 0) {
      int bytesWritten = write(outputFd_m, buffer, bytesToWrite);
      if (bytesWritten == -1) {
        /* Error on the write */
        switch (errno){
        case EINTR:
          continue;
          break;
        default: //EPIPE or other error received 
          CPPUNIT_FAIL(strerror(errno));
        }
      } else {
        bytesToWrite -= bytesWritten;
        buffer       += bytesWritten;
      }
    }
  }

  void readBuffer(void* buffer, size_t count) {
    int bytesToRead =  count;

    while (bytesToRead > 0) {
      int bytesRead = read(inputFd_m, buffer, bytesToRead);
      if (bytesRead < 0) {
        /* Error on the read */
        switch (errno){
        case EINTR:
          continue;
          break;
        default: //EPIPE or other error received 
          CPPUNIT_FAIL(strerror(errno));
        }
      } else {
        bytesToRead -= bytesRead;
        buffer      += bytesRead;
      }
    }
  }

  void returnCurrentState() {
    writeBuffer(&returnState, sizeof(CalcStatusType_t));
  }

  void returnSuccess() {
    /* This method just returns a SUCCESS Enumeration over the pipe */
    CalcStatusType_t state = Success;
    writeBuffer(&state, sizeof(CalcStatusType_t));
  }

  void returnExit() {
    /* This method just returns a EXIT Enumeration over the pipe */
    CalcStatusType_t state = Exit;
    writeBuffer(&state, sizeof(CalcStatusType_t));
  }

  void returnError() {
    CalcStatusType_t state = Error;
    writeBuffer(&state, sizeof(CalcStatusType_t));
  }

  int inputFd_m;
  int outputFd_m;
  pthread_t childThread;
  CalcStatusType_t returnState;

  CalcAntenna_t lastAntenna;
  CalcAntenna_t lastReference;
  CalcPressureData_t lastPressure;
  unsigned int  polynomialOrder;
  std::vector<CalcSource_t> sourceList;
  double        lastMJD;

  unsigned int numAntenna_m;
  unsigned int timeTableSize_m;
  ACS::Time*   timeTable_m;

};


/* Ok here we want to override the startCalcProcess and closeCalcProcess.
   (The checkCalcProcess method will never be used), and create a loopback
   pipe handler instead of an external process 
*/

class CalcCommManagerTest : public CppUnit::TestFixture,
                            public CalcCommManager
{
public:
  CalcCommManagerTest() : CppUnit::TestFixture(),
                          CalcCommManager("None"),
                          childProxy(NULL),
                          reinitializeCalled(false),
                          reinitializeSuccess(true){}
//   CalcCommManagerTest( const CalcCommManagerTest &toCopy){*this=toCopy;}
//   CalcCommManagerTest &operator=(const CalcCommManagerTest&)
//      {return *this;}
  ~CalcCommManagerTest(){};

  void setUp() {
    startCalcProcess();
    CPPUNIT_ASSERT_MESSAGE("Reinitialized Called variable in incorrect state",
                           !reinitializeCalled);
    CPPUNIT_ASSERT_MESSAGE("Reinitialized Success variable in incorrect state",
                           reinitializeSuccess);
  }

  void tearDown() {
    closeCalcProcess();
  }

  void testInstanciation() {
    /* This is really just a test on the testing framework */
  }

  void testWriteAntenna(){
      CalcAntenna_t newAntenna;
      createAntenna(newAntenna, "DV01" , 1,  2, 3, 0.1);
      
      childProxy->spawnHandler();
      writeAntenna(newAntenna);

      CPPUNIT_ASSERT_MESSAGE("Antenna sent to process incorrectly",
                             checkLastAntenna(newAntenna));
  }

  void testWriteAntennaFail(){
    CalcAntenna_t newAntenna;
    createAntenna(newAntenna, "DV01" , 1,  2, 3, 0.1);

    childProxy->setReturnState(Error);
    childProxy->spawnHandler();
    CPPUNIT_ASSERT_MESSAGE("Improper response writing new antenna",
                           writeAntenna(newAntenna));
    childProxy->joinThread();
  }

  void testWriteAntennaTimeOut(){
    CalcAntenna_t newAntenna;
    createAntenna(newAntenna, "DV01" , 1,  2, 3, 0.1);
    
    CPPUNIT_ASSERT_MESSAGE("Improper response writing new antenna",
                           writeAntenna(newAntenna));
  }

  void testWriteAntennaClosed() {
    delete childProxy;
    childProxy = NULL;
    
    CalcAntenna_t newAntenna;
    createAntenna(newAntenna, "DV01" , 1,  2, 3, 0.1);

    CPPUNIT_ASSERT_MESSAGE("Improper response writing new antenna",
                           writeAntenna(newAntenna));
  }


  void testWriteReference(){
    createAntenna(reference_m, "ref" , 1000.0, 2000.0, 3000.0, 0.1);

    childProxy->spawnHandler();
    CPPUNIT_ASSERT_MESSAGE("Error writing reference to process",
                           !writeReference());
    childProxy->joinThread();
        
    CalcAntenna_t processRef = childProxy->getLastReference();
    CPPUNIT_ASSERT_MESSAGE("Reference incorrectly set on process",
                           reference_m.x == processRef.x &&
                           reference_m.y == processRef.y &&
                           reference_m.z == processRef.z &&
                           reference_m.axisOffset == processRef.axisOffset);
  }

  void testWriteReferenceFail(){
    createAntenna(reference_m, "ref" , 1000.0, 2000.0, 3000.0, 0.1);

    childProxy->setReturnState(Error);
    childProxy->spawnHandler();
    CPPUNIT_ASSERT_MESSAGE("Improper response writing new antenna",
                           writeReference());
    childProxy->joinThread();
  }

  void testWriteReferenceTimeOut(){
     createAntenna(reference_m, "ref" , 1000.0, 2000.0, 3000.0, 0.1);
    
    CPPUNIT_ASSERT_MESSAGE("Improper response writing new antenna",
                           writeReference());
  }

  void testWriteReferenceClosed() {
    delete childProxy;
    childProxy = NULL;
    
    createAntenna(reference_m, "ref" , 1000.0, 2000.0, 3000.0, 0.1);

    CPPUNIT_ASSERT_MESSAGE("Improper response writing new antenna",
                           writeReference());
  }


  void testWritePolynomialOrder() {
    polynomialOrder_m = 8;

    childProxy->spawnHandler();
    CPPUNIT_ASSERT_MESSAGE("Failed writing polynomail order to process",
                           !writePolynomialOrder());
    childProxy->joinThread();
    
    unsigned int processPolyOrder = childProxy->getLastPolynomialOrder();
    CPPUNIT_ASSERT_MESSAGE("Polynomial Order incorrectly set on process",
                           8  == processPolyOrder);
  }

  void testWritePolynomialOrderFail(){
    polynomialOrder_m = 8;

    childProxy->setReturnState(Error);
    childProxy->spawnHandler();
    CPPUNIT_ASSERT_MESSAGE("Improper response writing new antenna",
                           writePolynomialOrder());
    childProxy->joinThread();
  }

  void testWritePolynomialOrderTimeOut(){
    polynomialOrder_m = 8;
    
    CPPUNIT_ASSERT_MESSAGE("Improper response writing new antenna",
                           writePolynomialOrder());
  }

  void testWritePolynomialOrderClosed() {
    delete childProxy;
    childProxy = NULL;
    
    polynomialOrder_m = 8;

    CPPUNIT_ASSERT_MESSAGE("Improper response writing new antenna",
                           writePolynomialOrder());
  }

  void testWriteAntennaPressure() {
    CalcPressureData_t newPressureData;
    strncpy(newPressureData.antennaName, "PM04", 4);
    newPressureData.pressure = 1234.56;
    childProxy->spawnHandler();


    CPPUNIT_ASSERT_MESSAGE("Failed to write the pressure data",
                           !writeAntennaPressure(newPressureData));

    CalcPressureData_t processPressureData = childProxy->getLastPressureData();
    CPPUNIT_ASSERT_MESSAGE("Antenna Name is incorrect",
                           !strncmp(processPressureData.antennaName,
                                    newPressureData.antennaName, 4));

    CPPUNIT_ASSERT_MESSAGE("Antenna pressure is incorrect",
                           processPressureData.pressure == 
                           newPressureData.pressure);
  }


  void testWriteSourceList(){
    const int MAX_SOURCELIST_SIZE=10;
    
    for (int listSize = 1; listSize < MAX_SOURCELIST_SIZE; listSize++) {
      sourceList_m.clear();
      for (int idx = 0; idx < listSize; idx++) {
        CalcSource_t newSource;
        newSource.RA = 0.1 * idx;
        newSource.Dec = -0.1 * idx;
        newSource.dRA = 0.01 * idx;
        newSource.dDec = -0.01 * idx;
        newSource.Epoch = 54000 + idx;
        newSource.Parallax = 0.0001 * idx;
        sourceList_m.push_back(newSource);
      }
      
      childProxy->spawnHandler();

      CPPUNIT_ASSERT_MESSAGE("Failed while trying to write source list",
                             !writeSourceList()); // True == Error
      childProxy->joinThread();

      std::vector<CalcSource_t>  procList= childProxy->getLastSourceList();
      CPPUNIT_ASSERT_MESSAGE("Source List Size on process incorrect",
                             sourceList_m.size() == procList.size());
      for (int idx = 0; idx < listSize; idx++) {
        CPPUNIT_ASSERT_MESSAGE("Source list not written to process correctly",
                               procList[idx].RA==sourceList_m[idx].RA &&
                               procList[idx].Dec==sourceList_m[idx].Dec &&
                               procList[idx].dRA==sourceList_m[idx].dRA &&
                               procList[idx].dDec==sourceList_m[idx].dDec &&
                               procList[idx].Epoch==sourceList_m[idx].Epoch &&
                               procList[idx].Parallax==
                               sourceList_m[idx].Parallax);
      }                               
    }
  }

  void testWriteSourceListFail(){
    sourceList_m.clear();
    {
      CalcSource_t newSource;
      newSource.RA = 0.1;
      newSource.Dec = -0.1;
      newSource.dRA = 0.01;
      newSource.dDec = -0.01;
      newSource.Epoch = 54000;
      newSource.Parallax = 0.0001;
      sourceList_m.push_back(newSource);
    }
    childProxy->setReturnState(Error);
    childProxy->spawnHandler();
    CPPUNIT_ASSERT_MESSAGE("Improper response writing source list",
                           writeSourceList());
    childProxy->joinThread();
  }

  void testWriteSourceListTimeOut(){
    sourceList_m.clear();
    {
      CalcSource_t newSource;
      newSource.RA = 0.1;
      newSource.Dec = -0.1;
      newSource.dRA = 0.01;
      newSource.dDec = -0.01;
      newSource.Epoch = 54000;
      newSource.Parallax = 0.0001;
      sourceList_m.push_back(newSource);
    }
    
    CPPUNIT_ASSERT_MESSAGE("Improper response writing source list",
                           writeSourceList());
  }

  void testWriteSourceListClosed() {
    delete childProxy;
    childProxy = NULL;
    
    sourceList_m.clear();
    {
      CalcSource_t newSource;
      newSource.RA = 0.1;
      newSource.Dec = -0.1;
      newSource.dRA = 0.01;
      newSource.dDec = -0.01;
      newSource.Epoch = 54000;
      newSource.Parallax = 0.0001;
      sourceList_m.push_back(newSource);
    }
    CPPUNIT_ASSERT_MESSAGE("Improper response writing source list",
                           writeSourceList());
  }

  void testInitializeCalcProcess(){
    childProxy->spawnHandler();
    
    CPPUNIT_ASSERT_MESSAGE("Failed to initialize Calc process",
                           !initializeCalcProcess());
    childProxy->joinThread();

    CPPUNIT_ASSERT_MESSAGE("Process initalized incorrectly",
                           childProxy->getLastMJD() == todayMJD_m);
  }

  void testInitializeCalcProcessFail(){
    childProxy->setReturnState(Error);
    childProxy->spawnHandler();
    CPPUNIT_ASSERT_MESSAGE("Improper response initializing Calc process",
                           initializeCalcProcess());
    childProxy->joinThread();
  }

  void testInitializeCalcProcessTimeOut(){
    CPPUNIT_ASSERT_MESSAGE("Improper response initializing Calc process",
                           initializeCalcProcess());
  }

  void testInitializeCalcProcessClosed() {
    delete childProxy;
    childProxy = NULL;
    CPPUNIT_ASSERT_MESSAGE("Improper response initializing Calc process",
                           initializeCalcProcess());
  }

  void testAddAntenna(){
    const unsigned int MAX_ANTENNA = 99;
    CalcAntenna_t newAntenna[MAX_ANTENNA];

    for (unsigned int idx = 0; idx < MAX_ANTENNA; idx++) {
      childProxy->spawnHandler();

      char name[5];
      sprintf(name,"DV%02d",idx);
      createAntenna(newAntenna[idx], name , idx, 2*idx, 3*idx, 0.1*idx);
      
      addAntenna(newAntenna[idx]);

      CPPUNIT_ASSERT_MESSAGE("Antenna sent to process incorrectly",
                             checkLastAntenna(newAntenna[idx]));

      CPPUNIT_ASSERT_MESSAGE("Antenna list is incorrect size",
                             antennaList_m.size() == idx+1);

      CPPUNIT_ASSERT_MESSAGE("Antenna was inserted incorrectly",
                             checkAntenna(newAntenna[idx]));

      for (unsigned int cidx = 0; cidx < idx; cidx++) {
        CPPUNIT_ASSERT_MESSAGE("Antenna was incorrectly modified",
                               checkAntenna(newAntenna[cidx]));
      }
    }
  }
  
  void testUpdateAntenna(){
    const unsigned int MAX_ANTENNA = 99;
    CalcAntenna_t newAntenna[MAX_ANTENNA];

    /* First populate the antenna list */
    for (unsigned int idx = 0; idx < MAX_ANTENNA; idx++) {
      childProxy->spawnHandler();

      char name[5];
      sprintf(name,"DV%02d",idx);
      createAntenna(newAntenna[idx], name , idx, 2*idx, 3*idx, 0.1*idx);
      addAntenna(newAntenna[idx]);
      childProxy->joinThread();
    }

    /* Now modify each antenna and ensure that it changes */
    for (unsigned int idx = 0; idx < MAX_ANTENNA; idx++) {
      childProxy->spawnHandler();

      char name[5];
      sprintf(name,"DV%02d",idx);
      createAntenna(newAntenna[idx], name , 0.1* idx, 0.2*idx, 0.3*idx, 1*idx);
      addAntenna(newAntenna[idx]);
      childProxy->joinThread();

      CPPUNIT_ASSERT_MESSAGE("Antenna sent to process incorrectly",
                             checkLastAntenna(newAntenna[idx]));

      for (unsigned int tidx = 0; tidx < MAX_ANTENNA; tidx++) {
        CPPUNIT_ASSERT_MESSAGE("Antenna list is incorrect size",
                               antennaList_m.size() == MAX_ANTENNA);
        if (tidx == idx) {
          CPPUNIT_ASSERT_MESSAGE("Antenna was modified but with wrong values",
                                 checkAntenna(newAntenna[tidx]));
        } else {
          CPPUNIT_ASSERT_MESSAGE("Antenna was incorrectly modified",
                                 checkAntenna(newAntenna[tidx]));
        }
      }
    }
  }

  void testAddAntennaFail(){
    childProxy->setReturnState(Error);
    childProxy->spawnHandler();

    CalcAntenna_t newAntenna;
    createAntenna(newAntenna, "PM01" , 0.1, 0.2, 0.3, 1);
    addAntenna(newAntenna);
    childProxy->joinThread();

    CPPUNIT_ASSERT_MESSAGE("FAILED to attempt reinitialization",
                           reinitializeCalled);
  }

  void testAddAntennaReinitFails(){
    reinitializeSuccess = false;
    childProxy->setReturnState(Error);
    childProxy->spawnHandler();

    CalcAntenna_t newAntenna;
    createAntenna(newAntenna, "PM01" , 0.1, 0.2, 0.3, 1);
    CPPUNIT_ASSERT_THROW(addAntenna(newAntenna),
                         DelayServerExceptions::ReinitializationErrorExImpl);
    childProxy->joinThread();
  }


  void testSetReference(){
    CalcAntenna_t reference;
    createAntenna(reference, "ref" , 1000.0, 2000.0, 3000.0, 0.1);

    childProxy->spawnHandler();
    setReference(reference);
    childProxy->joinThread();

    CalcAntenna_t processRef = childProxy->getLastReference();
    CPPUNIT_ASSERT_MESSAGE("Reference incorrectly set on process",
                           reference.x == processRef.x &&
                           reference.y == processRef.y &&
                           reference.z == processRef.z &&
                           reference.axisOffset == processRef.axisOffset);

    CPPUNIT_ASSERT_MESSAGE("Reference incorrectly cached",
                           reference.x == reference_m.x &&
                           reference.y == reference_m.y &&
                           reference.z == reference_m.z &&
                           reference.axisOffset == reference_m.axisOffset);

  }

  void testSetReferenceFail(){
    childProxy->setReturnState(Error);
    childProxy->spawnHandler();

    CalcAntenna_t reference;
    createAntenna(reference, "ref" , 1000.0, 2000.0, 3000.0, 0.1);

    setReference(reference);
    childProxy->joinThread();
    CPPUNIT_ASSERT_MESSAGE("FAILED to attempt reinitialization",
                           reinitializeCalled);

  }

  void testSetReferenceReinitFails(){
    reinitializeSuccess = false;
    childProxy->setReturnState(Error);
    childProxy->spawnHandler();

    CalcAntenna_t reference;
    createAntenna(reference, "ref" , 1000.0, 2000.0, 3000.0, 0.1);
    CPPUNIT_ASSERT_THROW(setReference(reference),
                         DelayServerExceptions::ReinitializationErrorExImpl);
    childProxy->joinThread();
  }

  void testSetPolynomialOrder(){
    unsigned int newPolyOrder = 8;

    childProxy->spawnHandler();
    setPolynomialOrder(newPolyOrder);
    childProxy->joinThread();

    unsigned int processPolyOrder = childProxy->getLastPolynomialOrder();
    CPPUNIT_ASSERT_MESSAGE("Polynomial Order incorrectly set on process",
                           newPolyOrder == processPolyOrder);

    CPPUNIT_ASSERT_MESSAGE("Polynomial Order incorrectly cached",
                           newPolyOrder == polynomialOrder_m);
  }

  void testSetPolynomialOrderFail(){
    childProxy->setReturnState(Error);
    childProxy->spawnHandler();

    unsigned int newPolyOrder = 8;
    setPolynomialOrder(newPolyOrder);
    childProxy->joinThread();
    CPPUNIT_ASSERT_MESSAGE("FAILED to attempt reinitialization",
                           reinitializeCalled);

  }

  void testSetPolynomialOrderReinitFails(){
    reinitializeSuccess = false;
    childProxy->setReturnState(Error);
    childProxy->spawnHandler();

    unsigned int newPolyOrder = 8;
    CPPUNIT_ASSERT_THROW(setPolynomialOrder(newPolyOrder),
                         DelayServerExceptions::ReinitializationErrorExImpl);
    childProxy->joinThread();
  }

  void testSetSource(){
    CalcSource_t newSource;
    newSource.RA = 1.1;
    newSource.Dec = -1.1;
    newSource.dRA = 1.01;
    newSource.dDec = -1.01;
    newSource.Epoch = 54000;
    newSource.Parallax = 1.0001;
      
    childProxy->spawnHandler();
    setSource(newSource);
    childProxy->joinThread();

    std::vector<CalcSource_t>  procList= childProxy->getLastSourceList();
    CPPUNIT_ASSERT_MESSAGE("Source List Size on process incorrect",
                           sourceList_m.size() == 1);
    CPPUNIT_ASSERT_MESSAGE("Source List Size on process incorrect",
                           procList.size() == 1);

    CPPUNIT_ASSERT_MESSAGE("Source list not written to process correctly",
                               procList[0].RA==newSource.RA &&
                               procList[0].Dec==newSource.Dec &&
                               procList[0].dRA==newSource.dRA &&
                               procList[0].dDec==newSource.dDec &&
                               procList[0].Epoch==newSource.Epoch &&
                               procList[0].Parallax== newSource.Parallax);

    CPPUNIT_ASSERT_MESSAGE("Source list not cached correctly",
                               procList[0].RA==sourceList_m[0].RA &&
                               procList[0].Dec==sourceList_m[0].Dec &&
                               procList[0].dRA==sourceList_m[0].dRA &&
                               procList[0].dDec==sourceList_m[0].dDec &&
                               procList[0].Epoch==sourceList_m[0].Epoch &&
                               procList[0].Parallax==sourceList_m[0].Parallax);
  }

  void testSetSourceFail(){
    childProxy->setReturnState(Error);
    childProxy->spawnHandler();
    
    CalcSource_t newSource;
    newSource.RA = 1.1;
    newSource.Dec = -1.1;
    newSource.dRA = 1.01;
    newSource.dDec = -1.01;
    newSource.Epoch = 54000;
    newSource.Parallax = 1.0001;
    
    setSource(newSource);
    childProxy->joinThread();
    CPPUNIT_ASSERT_MESSAGE("FAILED to attempt reinitialization",
                           reinitializeCalled);
  }

  void testSetSourceReinitFails(){
    reinitializeSuccess = false;
    childProxy->setReturnState(Error);
    childProxy->spawnHandler();

    CalcSource_t newSource;
    newSource.RA = 1.1;
    newSource.Dec = -1.1;
    newSource.dRA = 1.01;
    newSource.dDec = -1.01;
    newSource.Epoch = 54000;
    newSource.Parallax = 1.0001;

    CPPUNIT_ASSERT_THROW(setSource(newSource),
                         DelayServerExceptions::ReinitializationErrorExImpl);
    childProxy->joinThread();
  }

  void testSetSourceList(){
    const int MAX_SOURCELIST_SIZE=10;
    std::vector<CalcSource_t>  sourceList;
    
    for (int listSize = 1; listSize < MAX_SOURCELIST_SIZE; listSize++) {
      sourceList.clear();
      for (int idx = 0; idx < listSize; idx++) {
        CalcSource_t newSource;
        newSource.RA = 0.1 * idx;
        newSource.Dec = -0.1 * idx;
        newSource.dRA = 0.01 * idx;
        newSource.dDec = -0.01 * idx;
        newSource.Epoch = 54000 + idx;
        newSource.Parallax = 0.0001 * idx;
        sourceList.push_back(newSource);
      }
      
      childProxy->spawnHandler();
      setSource(sourceList);
      childProxy->joinThread();

      std::vector<CalcSource_t>  procList= childProxy->getLastSourceList();
      CPPUNIT_ASSERT_MESSAGE("Source List Size on process incorrect",
                             sourceList.size() == procList.size());
      CPPUNIT_ASSERT_MESSAGE("Source List Size on process incorrect",
                             sourceList.size() == sourceList_m.size());

      for (int idx = 0; idx < listSize; idx++) {
        CPPUNIT_ASSERT_MESSAGE("Source list not written to process correctly",
                               procList[idx].RA==sourceList[idx].RA &&
                               procList[idx].Dec==sourceList[idx].Dec &&
                               procList[idx].dRA==sourceList[idx].dRA &&
                               procList[idx].dDec==sourceList[idx].dDec &&
                               procList[idx].Epoch==sourceList[idx].Epoch &&
                               procList[idx].Parallax==
                               sourceList[idx].Parallax);

        CPPUNIT_ASSERT_MESSAGE("Source list not cacheds correctly",
                               sourceList[idx].RA==sourceList_m[idx].RA &&
                               sourceList[idx].Dec==sourceList_m[idx].Dec &&
                               sourceList[idx].dRA==sourceList_m[idx].dRA &&
                               sourceList[idx].dDec==sourceList_m[idx].dDec &&
                               sourceList[idx].Epoch==
                               sourceList_m[idx].Epoch &&
                               sourceList[idx].Parallax==
                               sourceList_m[idx].Parallax);

      }                               
    }
  }

  void testSetSourceListFail(){
    childProxy->setReturnState(Error);
    childProxy->spawnHandler();
    
    const int SOURCELIST_SIZE=10;
    std::vector<CalcSource_t>  sourceList;
    
    for (int idx = 0; idx < SOURCELIST_SIZE; idx++) {
      CalcSource_t newSource;
      newSource.RA = 0.1 * idx;
      newSource.Dec = -0.1 * idx;
      newSource.dRA = 0.01 * idx;
      newSource.dDec = -0.01 * idx;
      newSource.Epoch = 54000 + idx;
      newSource.Parallax = 0.0001 * idx;
      sourceList.push_back(newSource);
    }
    
    setSource(sourceList);
    childProxy->joinThread();
    CPPUNIT_ASSERT_MESSAGE("FAILED to attempt reinitialization",
                           reinitializeCalled);
  }

  void testSetSourceListReinitFails(){
    reinitializeSuccess = false;
    childProxy->setReturnState(Error);
    childProxy->spawnHandler();

    const int SOURCELIST_SIZE=10;
    std::vector<CalcSource_t>  sourceList;
    
    for (int idx = 0; idx < SOURCELIST_SIZE; idx++) {
      CalcSource_t newSource;
      newSource.RA = 0.1 * idx;
      newSource.Dec = -0.1 * idx;
      newSource.dRA = 0.01 * idx;
      newSource.dDec = -0.01 * idx;
      newSource.Epoch = 54000 + idx;
      newSource.Parallax = 0.0001 * idx;
      sourceList.push_back(newSource);
    }
    
    CPPUNIT_ASSERT_THROW(setSource(sourceList),
                         DelayServerExceptions::ReinitializationErrorExImpl);
    childProxy->joinThread();
  }

 
  void testReadCalcProcessResults(){
    ACS::Time  delayTimes[polynomialOrder_m+1];
    const unsigned int numAntenna = 66;

    for (unsigned int idx = 0; idx <= polynomialOrder_m; idx++) {
      delayTimes[idx] = idx;
    }
    
    childProxy->setNumAntenna(numAntenna);

    /* Reallocate the transfer tables directly */
    delete[] delayTable_m;
    delete[] azTable_m;
    delete[] elTable_m;
    delete[] dryAtmTable_m;
    delete[] wetAtmTable_m;

    unsigned int tableSize = numAntenna * (polynomialOrder_m+1);
    delayTable_m  = new double[tableSize];
    azTable_m     = new double[tableSize];
    elTable_m     = new double[tableSize];
    dryAtmTable_m = new double[tableSize];
    wetAtmTable_m = new double[tableSize];
    
    childProxy->spawnHandler();
    readCALCProcessResults(delayTimes);
    childProxy->joinThread();
    
    /* Check to make sure the delay times made it through */
    ACS::Time* procTime = childProxy->getTimeTable();
    unsigned int procTimeSize = childProxy->getTimeTableSize();

    CPPUNIT_ASSERT_MESSAGE("Time Table Sizes are not equal",
                           polynomialOrder_m+1 == procTimeSize);

    for (unsigned int idx = 0; idx < procTimeSize; idx++) {
      CPPUNIT_ASSERT_MESSAGE("Times are not same in tables",
                             delayTimes[idx]  == procTime[idx]);
    }
  
    /* Check the values of the return */
    for (unsigned int idx = 0; idx < procTimeSize * numAntenna; idx++) {
      CPPUNIT_ASSERT_MESSAGE("Delay value incorrect",
                             sortaEqual(delayTable_m[idx], idx));
      CPPUNIT_ASSERT_MESSAGE("Azimuth value incorrect",
                             sortaEqual(azTable_m[idx], 0.1*idx));
      CPPUNIT_ASSERT_MESSAGE("Elevation value incorrect",
                             sortaEqual(elTable_m[idx], -0.1*idx));
      CPPUNIT_ASSERT_MESSAGE("Dry Atmosphere delay value incorrect",
                             sortaEqual(dryAtmTable_m[idx], 10.0*idx));
      CPPUNIT_ASSERT_MESSAGE("Wet Atmosphere delay value incorrect",
                             sortaEqual(wetAtmTable_m[idx], 11.*idx));
   }
  }
  
  void testGetDelayTable(){}

  void testReinitializeCalcProcess(){}


  CPPUNIT_TEST_SUITE(CalcCommManagerTest);
  CPPUNIT_TEST(testInstanciation);
  /* -------- Low Level IO Tests ---------- */
  /* Write Antenna Tests */
  CPPUNIT_TEST(testWriteAntenna);
  CPPUNIT_TEST(testWriteAntennaFail);
  CPPUNIT_TEST(testWriteAntennaTimeOut);
  CPPUNIT_TEST(testWriteAntennaClosed);
  
  /* Write Reference Tests */
  CPPUNIT_TEST(testWriteReference);
  CPPUNIT_TEST(testWriteReferenceFail);
  CPPUNIT_TEST(testWriteReferenceTimeOut);
  CPPUNIT_TEST(testWriteReferenceClosed);

  /* Write Polynomial Order Tests*/
  CPPUNIT_TEST(testWritePolynomialOrder);
  CPPUNIT_TEST(testWritePolynomialOrderFail);
  CPPUNIT_TEST(testWritePolynomialOrderTimeOut);
  CPPUNIT_TEST(testWritePolynomialOrderClosed);

  /* Write Source List Tests*/
  CPPUNIT_TEST(testWriteSourceList);
  CPPUNIT_TEST(testWriteSourceListFail);
  CPPUNIT_TEST(testWriteSourceListTimeOut);
  CPPUNIT_TEST(testWriteSourceListClosed);

  CPPUNIT_TEST(testInitializeCalcProcess);
  CPPUNIT_TEST(testInitializeCalcProcessFail);
  CPPUNIT_TEST(testInitializeCalcProcessTimeOut);
  CPPUNIT_TEST(testInitializeCalcProcessClosed); 

  /* ---------- Class Manipulator Tests ---------- */
  /* Add Antenna Tests */
  CPPUNIT_TEST(testAddAntenna);
  CPPUNIT_TEST(testUpdateAntenna);
  CPPUNIT_TEST(testAddAntennaFail);
  CPPUNIT_TEST(testAddAntennaReinitFails);

   /* Set Reference Tests */
  CPPUNIT_TEST(testSetReference);
  CPPUNIT_TEST(testSetReferenceFail);
  CPPUNIT_TEST(testSetReferenceReinitFails);
  
  /* Set Antenna Pressure Tests */
  CPPUNIT_TEST(testWriteAntennaPressure);

  /* Set Polynomial Order Tests */
  CPPUNIT_TEST(testSetPolynomialOrder);
  CPPUNIT_TEST(testSetPolynomialOrderFail);
  CPPUNIT_TEST(testSetPolynomialOrderReinitFails);
  
  /* Set Source Tests */
  CPPUNIT_TEST(testSetSource);
  CPPUNIT_TEST(testSetSourceFail);
  CPPUNIT_TEST(testSetSourceReinitFails);
  CPPUNIT_TEST(testSetSourceList);
  CPPUNIT_TEST(testSetSourceListFail);
  CPPUNIT_TEST(testSetSourceListReinitFails);
 
  CPPUNIT_TEST(testReadCalcProcessResults);
  
  CPPUNIT_TEST_SUITE_END();

  /* ---------------- Utility Functions -------------------- */
  void reinitializeCalcProcess() {
    reinitializeCalled = true;
    if (!reinitializeSuccess)
      throw DelayServerExceptions::
        ReinitializationErrorExImpl(__FILE__,__LINE__, 
                                    "reinitializeCalcProcess");
  }


  void startCalcProcess() {
    if (childProxy != NULL) {
      closeCalcProcess();
    }

    /* Open the new file descriptors 
       Note: idx0 is for reading
       idx1 is for writing
    */
    int   commOutputDescriptor[2];
    int   commInputDescriptor[2];
    
    
    if (pipe(commOutputDescriptor) ||  pipe(commInputDescriptor)) {
      CPPUNIT_FAIL("Failed to open the pipes for communication");
    }
      
    childProxy = new CalcProcessProxy(commOutputDescriptor[0],
                                      commInputDescriptor[1]);

    inputFd_m  = commInputDescriptor[0];
    outputFd_m = commOutputDescriptor[1];

    CPPUNIT_ASSERT_MESSAGE("Failure getting the proxy process working",
                           getChildMessage() == Success);
  }

  void closeCalcProcess(){
    if (childProxy != NULL){
      delete childProxy;
      childProxy = NULL;
    }

    if (outputFd_m != -1) {   
      close(outputFd_m);
      outputFd_m = -1;
    }

    if (inputFd_m != -1) {
      close(inputFd_m);
      inputFd_m = -1;
    }
  }

  bool checkAntenna(CalcAntenna_t& refAnt) {
    for (unsigned int antIdx = 0; antIdx < antennaList_m.size(); antIdx++) {
      if (!strncmp(refAnt.antennaName, antennaList_m[antIdx].antennaName,4)){
        if (antennaList_m[antIdx].x != refAnt.x ||
            antennaList_m[antIdx].y != refAnt.y ||
            antennaList_m[antIdx].z != refAnt.z ||
            antennaList_m[antIdx].axisOffset != refAnt.axisOffset) {
          return false;
        } else {
          return true;
        }
      }
    }
    return false;
  }

  bool checkLastAntenna(CalcAntenna_t& refAnt) {
    CalcAntenna_t lastAnt = childProxy->getLastAntenna();
    //     std::cout << "Check last Antenna:" << std::endl
    //               << lastAnt.x << "\t" << refAnt.x << std::endl
    //               << lastAnt.y << "\t" << refAnt.y << std::endl
    //               << lastAnt.z << "\t" << refAnt.z << std::endl
    //               << lastAnt.axisOffset << "\t" << refAnt.axisOffset
    //               << std::endl;
    if (!strncmp(lastAnt.antennaName, refAnt.antennaName,4) &&
        lastAnt.x == refAnt.x &&
        lastAnt.y == refAnt.y &&
        lastAnt.z == refAnt.z &&
        lastAnt.axisOffset == refAnt.axisOffset) {
      return true;
    } 
    return false;
  }
  


  bool sortaEqual(double a, double b, double threshold = 1E-14) {
    if (fabs(a-b) < threshold)
      return true;
    std::cout << "Sorta Equal Method Difference is: " << fabs(a-b)<< std::endl;
    return false;
  }
  
  void createAntenna(CalcAntenna_t& newAnt, char* name,
                     double x, double y, double z, double k){
    strncpy(newAnt.antennaName, name, 4);
    newAnt.x = x;
    newAnt.y = y;
    newAnt.z = z;
    newAnt.axisOffset = k;
  }

private:
  CalcProcessProxy* childProxy;
  bool              reinitializeCalled;
  bool              reinitializeSuccess;
  
 
}; // End of Test Class

int main( int argc, char **argv)
{
  CppUnit::TextUi::TestRunner runner;
  runner.addTest( CalcCommManagerTest::suite() );
  //runner.addTest(CalcProcessManagementTest::suite());
  runner.run();
  return 0;
}
