#!/usr/bin/env python

from Acspy.Nc.Consumer import Consumer
import Control
import time
import pylab as pl
import TETimeUtil
from math import *
from Acspy.Common import TimeHelper
import acstime


class EventCollector:
    def __init__(self):
        self.antennaList = {}
        self.numEvents = 0
        self.helper = TimeHelper.TimeUtil()

    def timestamp(self, epoch):
        if isinstance(epoch, acstime.Epoch):
            epoch = epoch.value
        gt = time.gmtime(self.helper.epoch2py(epoch))
        return "%02d:%02d:%02d.%07d" % ( gt[3],gt[4],gt[5],
                                         epoch % 10000000)

    def newData(self,dataEvent):
        print "New Delay Event: ", self.timestamp(TimeHelper.getTimeStamp())
        self.numEvents += 1
        for antennaEvent in dataEvent.antennaDelayEvents:
            print "\tAntenna: ", antennaEvent.antennaName
            print "\tStart Time: ", self.timestamp(antennaEvent.startTime)
            print "\tStop Time:  ", self.timestamp(antennaEvent.stopTime)
            self.printCoarseDelays(antennaEvent.coarseDelays)
            self.printFineDelays(antennaEvent.fineDelays)
            self.printDelayTable(antennaEvent.delayTables)

    def printCoarseDelays(self, coarseDelaySeq):
        print "\tCoarse Delay: ", len(coarseDelaySeq)
        for cd in coarseDelaySeq:
            print "\t\tRealitiveStartTime: %d \t\t Delay: %d" % \
                  (cd.relativeStartTime, cd.coarseDelay)
            
    def printFineDelays(self, fineDelaySeq):
        print "\tFine Delay:   ", len(fineDelaySeq)
        for fd in fineDelaySeq:
            print "\t\tRealitiveStartTime: %d \t\t Delay: %d" % \
                  (fd.relativeStartTime, fd.fineDelay)           

    def printDelayTable(self, delayTableSeq):
        print "\t%d Delay Tables" % len(delayTableSeq)
        for dt in delayTableSeq:
            print"\t\t From ", self.timestamp(dt.startTime),\
                 " to ", self.timestamp(dt.stopTime)
            for dv in dt.delayValues:
                print "\t\t\t", self.timestamp(dv.time),"\t",dv.totalDelay

if __name__ == "__main__":
    collector = EventCollector()
    
    c = Consumer(Control.CHANNELNAME_CONTROLREALTIME)
    c.addSubscription(Control.ArrayDelayEvent, collector.newData)
    c.consumerReady()

    try:
        while(True):
            pass
    except:
        pass

    c.disconnect()
