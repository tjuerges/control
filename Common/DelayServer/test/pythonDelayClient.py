#!/usr/bin/env python

from Acspy.Nc.Consumer import Consumer
import Control
import time
import pylab as pl
#from numarray import array
#from numarray import zeros
#from ppgplot import *
from math import *

class AntennaPlotter:
    def __init__(self, name):
##        print "Creating Antenna: " + name
        self.__name = name

        self.__delayTime = []
        self.__delayValue= []
        
##        self.eventTimes = {}
##        self.eventTimes["startTime"] = []
##        self.eventTimes["stopTime"]  = []
##        self.delayValues = {}
##        self.delayValues["Time"] = []
##        self.delayValues["Delay"] = []
        self.__fineTime = []
        self.__fineDelay = []
        self.__coarseTime = []
        self.__coarseDelay = []
        self.__timeOffset = 0
        

    def newData(self, antennaEvent):
        print "New Data In antenna"
        if self.__timeOffset == 0:
            self.__timeOffset = antennaEvent.startTime

        for delayTable in antennaEvent.delayTables:
            for value in delayTable.delayValues:
                if (value.time >= delayTable.startTime and
                    value.time < delayTable.stopTime):
                    self.__delayTime.append((value.time -
                                             self.__timeOffset)/1E7)
                    self.__delayValue.append(value.totalDelay)


        for delay in antennaEvent.coarseDelays:
            self.__coarseTime.append(
                ((antennaEvent.startTime-self.__timeOffset)/ 1E7) +
                        (delay.relativeStartTime/1E3))
            self.__coarseDelay.append(delay.coarseDelay * 250E-12)



        coarseDelayIndex = 0
        for delay in antennaEvent.fineDelays:
            newTime = ((antennaEvent.startTime-self.__timeOffset) / 1E7) + \
                      (delay.relativeStartTime/1E3)
            print "Fine Delay time: ", newTime
            
            
            while coarseDelayIndex < (len(self.__coarseTime) -1) and \
                      newTime >= self.__coarseTime[coarseDelayIndex +1] :
                coarseDelayIndex += 1
                
            print "Using coarse delay from: ", \
                  self.__coarseTime[coarseDelayIndex]

            print "Using coarseDelay: ", coarseDelayIndex
            self.__fineTime.append(newTime)
            self.__fineDelay.append(self.__coarseDelay[coarseDelayIndex] +
                                    (delay.fineDelay*31.25E-12))



    def draw(self):
        pl.text(0.05, 0.9, self.__name,
                horizontalalignment='left',
                transform=pl.gca().transAxes)        

        pl.plot(self.__delayTime, self.__delayValue,'r+')
        pl.plot(self.__coarseTime, self.__coarseDelay, 'b:', linestyle="steps")
        pl.plot(self.__fineTime, self.__fineDelay, 'g-',linestyle="steps")
        

class ArrayPlotter:
    def __init__(self):
        self.antennaList = {}
        pl.figure(figsize=[10,10])

    def newData(self,dataEvent):
        for antennaEvent in dataEvent.antennaDelayEvents:
            if not self.antennaList.has_key(antennaEvent.antennaName):
                self.antennaList[antennaEvent.antennaName] = \
                              AntennaPlotter(antennaEvent.antennaName)
            self.antennaList[antennaEvent.antennaName].newData(antennaEvent)

        self.draw()

    def draw(self):
        pl.ioff()
        for antenna in self.antennaList.keys():
            if len(self.antennaList) >= 4:
                pl.subplot((len(self.antennaList)+1)/2, 2,
                           len(self.antennaList) -
                           self.antennaList.keys().index(antenna))
            else :
                print "Using: ", self.antennaList.keys().index(antenna)
                pl.subplot(len(self.antennaList), 1,
                           len(self.antennaList) -
                           self.antennaList.keys().index(antenna))
            self.antennaList[antenna].draw()
        pl.ion()
        pl.get_current_fig_manager().show()

if __name__ == "__main__":
    arrayPlotter = ArrayPlotter()
    
    c = Consumer(Control.CHANNELNAME_CONTROLREALTIME)
    c.addSubscription(Control.ArrayDelayEvent, arrayPlotter.newData)
    c.consumerReady()

    pl.show()
    c.disconnect()
