#!/usr/bin/env python

from Acspy.Nc.Consumer import Consumer
import Control
import time
from numarray import array
from numarray import zeros
from ppgplot import *
from math import *

class AntennaPlotter:
    def __init__(self, name):
        print "Creating Antenna: " + name
        self.__name = name
        self.eventTimes = {}
        self.u = []
        self.v = []

    def newData(self, antennaEvent):
        for uvwTable in antennaEvent.uvwTables:
            for entry in uvwTable.uvwEntries:
                if (entry.validTime >= uvwTable.startTime and
                    entry.validTime < uvwTable.stopTime):
                    self.u.append(entry.U)
                    self.v.append(entry.V)

    def getRange(self):
        rng = [min(self.u),
               max(self.u),
               min(self.v),
               max(self.v)]

        return tuple(rng)
        

    def draw(self):
        pgpt(array(self.u),array(self.v))
     
        

class ArrayPlotter:
    def __init__(self):
        self.antennaList = {}
        self.numEvents = 0

    def newData(self,dataEvent):
        self.numEvents += 1
        for antennaEvent in dataEvent.antennaUVWEvents:
            if not self.antennaList.has_key(antennaEvent.antennaName):
                self.antennaList[antennaEvent.antennaName] = \
                              AntennaPlotter(antennaEvent.antennaName)
            self.antennaList[antennaEvent.antennaName].newData(antennaEvent)

        self.draw()

    def draw(self):
        color = 2
        pgeras()
        rng = list(self.antennaList[self.antennaList.keys()[0]].getRange())
        for antenna in self.antennaList.keys():
            trng = self.antennaList[antenna].getRange()
            rng[0] = min(rng[0],trng[0])
            rng[1] = max(rng[1],trng[1])
            rng[2] = min(rng[2],trng[2])
            rng[3] = max(rng[3],trng[3])

        scale = 10**floor(log10(rng[1]-rng[0]))
        rng[0] = scale * floor(rng[0]/scale)
        rng[1] = scale * ceil(rng[1]/scale)

        scale = 10**floor(log10(rng[3]-rng[2]))
        rng[2] = scale * floor(rng[2]/scale)
        rng[3] = scale * ceil(rng[3]/scale)

        pgsvp(0.1,0.9,0.1,0.9)
        pgswin(*rng)

        for antenna in self.antennaList.keys():
            pgsci(color)
            self.antennaList[antenna].draw()
            pgmtxt("T",-color,0.05,0.0,antenna)
            color += 1

        pgsci(1)
        pgbox("BCNTS",0.0,0,"BCNTSV",0.0,0)


if __name__ == "__main__":
    pgbeg("/xw",1,1)
    arrayPlotter = ArrayPlotter()

    c = Consumer(Control.CHANNELNAME_CONTROLREALTIME)
    c.addSubscription(Control.ArrayUVWEvent, arrayPlotter.newData)
    c.consumerReady()

    while arrayPlotter.numEvents < 60:
        time.sleep(5)
    

    c.disconnect()
    pgend()
