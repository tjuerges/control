/* @(#) $Id: testRtFindKernelModules.cpp,v 1.4 2006/08/31 18:37:38 ramestic Exp
$
 *
 * Copyright (C) 2006
 * Associated Universities, Inc. Washington DC, USA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * Correspondence concerning ALMA Software should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */


#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestCaller.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#include <simpleDelayServerImpl.h>
#include <TETimeUtil.h>
#include <sys/time.h>
#include <ControlExceptions.h>
#include <ASDM.h>
#include <CalWVRTable.h>
#include <CalWVRRow.h>
#include <maciSimpleClient.h>

class SimpleDelayServerImplTest : public CppUnit::TestFixture,
                                  public SimpleDelayServerImpl
{
public:
  SimpleDelayServerImplTest() : CppUnit::TestFixture(),
                                SimpleDelayServerImpl(){}
  ~SimpleDelayServerImplTest(){}
  
  void setUp() {
    srand48(time(0));
  }

  void testinitializeServer(){
    const std::string arrayName("Array001");
    initializeServer(arrayName.c_str());

    CPPUNIT_ASSERT_MESSAGE("Array name incorrectly stored",
                           !strcmp(arrayName.c_str(), arrayName_m.c_str()));
    CPPUNIT_ASSERT_MESSAGE("Last Event Stop Time is not on a TE",
                           (lastEventStopTime_m.value % 480000L)==0);
    CPPUNIT_ASSERT_MESSAGE("Last Event Stop Time is (alot) in the past",
                           TETimeUtil::unix2epoch(time(0)).value-
                           lastEventStopTime_m.value < 1e6);
  }

  void testSettingAntennaDelay() {
    const std::string arrayName("Array101");
    initializeServer(arrayName.c_str());

    std::map<std::string, DelayServerData>::iterator iter;


    addAntenna("DV02");
    
    const double   delayValue = 0.123170E-6;
    unsigned int   coarseDelay = 399507;
    unsigned short fineDelay = 5;
    
    {
      /* Check that a constant is correctly handled */
      Control::SimpleDelayServer::DelayPolynomial delayPoly;
      delayPoly.referenceTime = 0;
      delayPoly.coeffs.length(1);
      delayPoly.coeffs[0] = delayValue;
      
      setAntennaDelay("DV02",delayPoly);
    }
     
     iter = antennaData_m.begin(); 

    for (unsigned int idx = 0; idx < polynomialOrder_m; idx++){
      CPPUNIT_ASSERT_DOUBLES_EQUAL(iter->second.geometricDelay[idx], 
                                   delayValue, 1E-12);
      CPPUNIT_ASSERT_DOUBLES_EQUAL(iter->second.dryAtmDelay[idx], 0, 1E-12);
      CPPUNIT_ASSERT_DOUBLES_EQUAL(iter->second.wetAtmDelay[idx], 0, 1E-12);
      CPPUNIT_ASSERT_DOUBLES_EQUAL(iter->second.totalDelay[idx], 
                                   delayValue, 1E-12);
    }

    /* Now we want to check the delay event */
    CPPUNIT_ASSERT(!strcmp(lastEventPublished.arrayName,arrayName.c_str()));
    CPPUNIT_ASSERT(lastEventPublished.antennaDelayEvents.length() == 1);
    
    Control::AntennaDelayEvent ade =lastEventPublished.antennaDelayEvents[0];
    
    /* Check that start and stop are on TE boundaries */
    CPPUNIT_ASSERT(ade.startTime % 480000 == 0);
    CPPUNIT_ASSERT(ade.stopTime % 480000 == 0);
    
    CPPUNIT_ASSERT(!strcmp(ade.antennaName, "DV02"));
    
    /* Check that there is only one value in each of the data fields */
    CPPUNIT_ASSERT(ade.coarseDelays.length() == 1);
    CPPUNIT_ASSERT(ade.fineDelays.length() == 1);
    CPPUNIT_ASSERT(ade.delayTables.length() == 1);
    
    CPPUNIT_ASSERT(ade.coarseDelays[0].relativeStartTime == 0);      
    CPPUNIT_ASSERT(ade.coarseDelays[0].coarseDelay == coarseDelay);
    


    CPPUNIT_ASSERT(ade.fineDelays[0].relativeStartTime == 0);
    CPPUNIT_ASSERT(ade.fineDelays[0].fineDelay == fineDelay);
    
    CPPUNIT_ASSERT(ade.delayTables[0].startTime == ade.startTime);
    CPPUNIT_ASSERT(ade.delayTables[0].stopTime == ade.stopTime);
    CPPUNIT_ASSERT(ade.delayTables[0].delayValues.length() ==
                   polynomialOrder_m +1);
    
    for (unsigned int idx = 0; idx < polynomialOrder_m + 1; idx ++) {
      CPPUNIT_ASSERT(ade.delayTables[0].delayValues[idx].totalDelay ==
                     delayValue);
    }

    /* Now do a test with a delay ramp and 2 antennas */
    addAntenna("DV03");
      
    const double   slope     = 200E-13;
    const double   intercept = 0.123170E-6;
    const ACS::Time refTime  = ::getTimeStamp();
      //       const double   delayValue = 0.123170E-6;
      //       unsigned int   coarseDelay = 3507;
      //       unsigned short fineDelay = 3;
      
    {
      Control::SimpleDelayServer::DelayPolynomial delayPoly;
      delayPoly.referenceTime = refTime;
      delayPoly.coeffs.length(2);
      delayPoly.coeffs[0] = slope;
      delayPoly.coeffs[1] = intercept;

      setAntennaDelay("DV03",delayPoly);
    }
    
    for (iter == antennaData_m.begin(); iter != antennaData_m.end(); iter++){
      for (unsigned int idx = 0; idx < polynomialOrder_m; idx++){
        if (!strcmp(iter->first.c_str(),"DV02")) {
          CPPUNIT_ASSERT_DOUBLES_EQUAL(iter->second.geometricDelay[idx], 
                                       delayValue, 1E-12);
          CPPUNIT_ASSERT_DOUBLES_EQUAL(iter->second.totalDelay[idx], 
                                       delayValue, 1E-12);
        } else if  (!strcmp(iter->first.c_str(),"DV03")) {
          double currentDelay = (delayTime_m[idx] - refTime) * 
            (slope * 1E-7) + intercept;
          CPPUNIT_ASSERT_DOUBLES_EQUAL(iter->second.geometricDelay[idx], 
                                       currentDelay, 1E-12);
          CPPUNIT_ASSERT_DOUBLES_EQUAL(iter->second.totalDelay[idx], 
                                       currentDelay, 1E-12);
        } else {
          CPPUNIT_FAIL("Unknown antenna found");
        }
        CPPUNIT_ASSERT_DOUBLES_EQUAL(iter->second.dryAtmDelay[idx], 0, 1E-12);
        CPPUNIT_ASSERT_DOUBLES_EQUAL(iter->second.wetAtmDelay[idx], 0, 1E-12);
      }
    }

    /* Now we want to check the delay event */
    CPPUNIT_ASSERT(!strcmp(lastEventPublished.arrayName,arrayName.c_str()));
    CPPUNIT_ASSERT(lastEventPublished.antennaDelayEvents.length() == 2);
    
    for (unsigned int idx = 0; 
         idx < lastEventPublished.antennaDelayEvents.length();
         idx ++) {
      Control::AntennaDelayEvent ade = 
        lastEventPublished.antennaDelayEvents[idx];
    
      /* Check that start and stop are on TE boundaries */
      CPPUNIT_ASSERT(ade.startTime % 480000 == 0);
      CPPUNIT_ASSERT(ade.stopTime % 480000 == 0);
    
      if (!strcmp(ade.antennaName, "DV02")) {
        /* Check that there is only one value in each of the data fields */
        CPPUNIT_ASSERT(ade.coarseDelays.length() == 1);
        CPPUNIT_ASSERT(ade.fineDelays.length() == 1);
        CPPUNIT_ASSERT(ade.delayTables.length() == 1);
    
        CPPUNIT_ASSERT(ade.coarseDelays[0].relativeStartTime == 0);
        CPPUNIT_ASSERT(ade.coarseDelays[0].coarseDelay == coarseDelay);
    
        CPPUNIT_ASSERT(ade.fineDelays[0].relativeStartTime == 0);
        CPPUNIT_ASSERT(ade.fineDelays[0].fineDelay == fineDelay);
    
        CPPUNIT_ASSERT(ade.delayTables[0].startTime == ade.startTime);
        CPPUNIT_ASSERT(ade.delayTables[0].stopTime == ade.stopTime);
        CPPUNIT_ASSERT(ade.delayTables[0].delayValues.length() ==
                       polynomialOrder_m +1);
    
        for (unsigned int idx = 0; idx < polynomialOrder_m + 1; idx ++) {
          CPPUNIT_ASSERT(ade.delayTables[0].delayValues[idx].totalDelay ==
                         delayValue);
        }
      }  else if (!strcmp(ade.antennaName, "DV03")) {
        /* Now checking DV03 */
        CPPUNIT_ASSERT(ade.coarseDelays.length() > 1);
        CPPUNIT_ASSERT(ade.fineDelays.length() > 1);
        CPPUNIT_ASSERT(ade.delayTables.length() == 1);

        CPPUNIT_ASSERT(ade.coarseDelays[0].relativeStartTime == 0);
        for (unsigned int idx = 1; idx < ade.coarseDelays.length(); idx++) {
          CPPUNIT_ASSERT(ade.coarseDelays[idx].coarseDelay == 
                         ade.coarseDelays[idx-1].coarseDelay -1);
        }

        CPPUNIT_ASSERT(ade.fineDelays[0].relativeStartTime == 0);
        for (unsigned int idx = 1; idx < ade.fineDelays.length(); idx++) {
          if (ade.fineDelays[idx].fineDelay == 15) {
            CPPUNIT_ASSERT(ade.fineDelays[idx-1].fineDelay == 0);
          } else {
            CPPUNIT_ASSERT(ade.fineDelays[idx].fineDelay == 
                           ade.fineDelays[idx-1].fineDelay -1);
          }
        }
      } else {
        CPPUNIT_FAIL("Unknown antenna found");
      }
    }
  }

  void testGetWeatherDataMethod() {
    addAntenna("DV02");
    addAntenna("DV03");

    getWeatherData();
    std::map<std::string, DelayServerData>::iterator iter;
    for (iter = antennaData_m.begin(); iter != antennaData_m.end(); iter++){
      CPPUNIT_ASSERT(iter->second.pressure == 101325);
    }
  }

  void testAddingAnAntenna() {
    addAntenna("DV02");
    CPPUNIT_ASSERT(antennaData_m.size() == 1);
    CPPUNIT_ASSERT(antennaData_m["DV02"].antennaCableDelay == 0.0);
  }

  CPPUNIT_TEST_SUITE(SimpleDelayServerImplTest);
  CPPUNIT_TEST(testinitializeServer);
  CPPUNIT_TEST(testAddingAnAntenna);
  CPPUNIT_TEST(testGetWeatherDataMethod);
  CPPUNIT_TEST(testSettingAntennaDelay);
  CPPUNIT_TEST_SUITE_END();
 
  void publishDelayEvent(const Control::ArrayDelayEvent& event){
    lastEventPublished = event;
  }
  /* ---------------- Overridden functions ------------ */

private:
  Control::ArrayDelayEvent   lastEventPublished;

public:
  /* Because the POA defines them we need these functions even though 
     they are never actually used in the tests */
  virtual char* name() {
    return "Bogus Name";
  }

  virtual ACS::ComponentStates componentState() {
    return ACS::COMPSTATE_NEW;
  }
}; // End of Test Class

int main( int argc, char **argv)
{
  maci::SimpleClient m_client;
  m_client.init(argc,argv);
  m_client.login();
  ACSAlarmSystemInterfaceFactory::init(ACSAlarmSystemInterfaceFactory::getManager());
  CppUnit::TextUi::TestRunner runner;
  runner.addTest(SimpleDelayServerImplTest::suite() );
  runner.run();
  m_client.logout();
  m_client.disconnect();

  return 0;
}
