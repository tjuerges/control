#ifndef CalcPipeManager_H
#define CalcPipeManager_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2007-10-15  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <calcDataManager.h>
#include <DelayServerExceptions.h>

class CalcPipeManager {
public:
  CalcPipeManager(int inputStream, int outputStream);
  virtual ~CalcPipeManager();

  void serviceLoop();

  void returnSuccess(); 
  void returnExit();
  void returnError();
  
 protected:
  CalcPipeManager(CalcDataManager*);

  CalcDataManager* dataManager_m;
  
  /* Data Management Calls */
  virtual void addAntenna();
  virtual void setReference();
  virtual void setAntennaPressure();
  virtual void setPolynomialOrder();
  virtual void setSource();
  virtual void initializeCalc();
  virtual void getDelays();
  

  /* This method attempts to read count bytes from the inputStream
     it handles all errors it can, and will return false if there is
     an error it can not handle
  */
  bool readAll(void* buf, size_t count);
  bool writeAll(void* buf, size_t count);
  
  /* The input and output file descriptors */
  int inputFd_m;
  int outputFd_m;
};

#endif /*!CalcPipeManager_H*/
