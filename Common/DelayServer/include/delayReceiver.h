#ifndef DELAYRECEIVER_H
#define DELAYRECEIVER_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2005 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2005-07-30  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <acstime.h>
#include <acsncConsumer.h>
#include <ControlInterfacesC.h>
#include <DelayClientS.h>

class DelayReceiver : public nc::Consumer {
 public:
  DelayReceiver();
  DelayReceiver(CORBA::ORB_ptr orbRef_p);
  
  virtual ~DelayReceiver();

  void assignArray(const char* arrayName);
  void unassignArray();

  /**
   * This is the overridden function which is executed whenever an event comes
   * in.  It gets the data structure from the event and passes it to
   * the handler.
   *
   * \exception CosEventComm::Disconnected
  */
  virtual void push_structured_event(const CosNotification::StructuredEvent 
				     &notification);

  unsigned long long getNumEvents();  

 protected:

  virtual void eventHandler(Control::ArrayDelayEvent&) = 0;

 private:
  std::string arrayName_m;
  unsigned long long numEvents_m;

};


class AntennaDelayDistributor : public DelayReceiver {
 public:
  AntennaDelayDistributor();
  ~AntennaDelayDistributor();


  void setAntennaName(const char* antennaName);

  void attachClient(std::string clientName, 
		    Control::DelayClient_ptr clientReference);

 protected:
  void eventHandler(Control::ArrayDelayEvent& eventHandler);

 private:
  std::string                         antennaName_m;

  std::map<std::string, Control::DelayClient_ptr> clients_m;
};
#endif /*!DELAYSERVERIMPL_H*/
