#ifndef DELAYSERVERBASEIMPL_H
#define DELAYSERVERBASEIMPL_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2005 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2005-07-30  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <DelayServerBaseS.h>
#include <ControlAntennaInterfacesS.h>

#include <acsncSimpleSupplier.h>
#include <acsThread.h>
#include <DelayServerExceptions.h>
#include <PolynomialInterpolatorImpl.h>
//#include <delayServerErrorManager.h>
#include <controlAlarmHelper.h>
#include <delayServerData.h>
#include <atmosphericDelayModel.h>
#include <delayServerData.h>

enum DelayServerErrorCodes {
  ErrEmptySourceList      = 0x01,
  ErrCommunicationFailure = 0x02,
  ErrNoAtmosphereModel    = 0x04,
  ErrAtmosphereModel      = 0x08,
  ErrWeatherData          = 0x10,
  ErrPublication          = 0x20
};

class DelayServerBaseImpl: public Control::AlarmHelper,
  public virtual POA_Control::DelayServerBase
{  
 public:
  DelayServerBaseImpl();
  virtual ~DelayServerBaseImpl();
  
  /* ------------- External Methods --------------- */
 /**
   * Method which initializes the server 
   *
   * \exception DelayServerExceptions::InitializeServerErrorEx
   */
  virtual void initializeServer(const char* arrayId);
  
  /**
   * Method to add and antenna to the delay server
   *
   * \exception DelayServerExceptions::AddAntennaErrorEx
   */
  virtual void addAntenna(const char* antennaId);

  /* See the IDL for documentation on this method */
  virtual Control::AntennaSeq* getAntennas();
  
  /* See the IDL for documentation on this method */
  virtual void removeAntenna(const char* antennaId);

  /* See the IDL for documentation on this method */
  virtual void setAntennaPadLocation(const char* antennaId,
                                     double x, double y, double z);

  /* See the IDL for documentation on this method */
  virtual void getAntennaPadLocation(const char* antennaId,
                                     double& x, double& y, double& z);

  /* See the IDL for documentation on this method */
  virtual void setAntennaVector(const char* antennaId, double xPos,
                                double yPos,  double zPos);

  /* See the IDL for documentation on this method */
  virtual void getAntennaVector(const char* antennaId, double& xPos,
                                double& yPos,  double& zPos);

  /* See the IDL for documentation on this method */
  virtual void setAxisNonIntersectionParameter(const char* antennaId, 
                                               double kTerm);

  /* See the IDL for documentation on this method */
  double getAxisNonIntersectionParameter(const char* antennaId);

    
  virtual void setCausalityDelay(CORBA::Double newDelay);
  
  virtual CORBA::Double getCausalityDelay();
  
  /**
   * Method to set the cable delay for an antenna
   *
   * \exception ControlExceptions::IllegalParameterErrorEx
   */
  virtual void setAntennaCableDelay(const char*   antennaName,
				    CORBA::Double cableDelay);

  /**
   * Method to get the cable delay for an antenna
   *
   * \exception ControlExceptions::IllegalParameterErrorEx
   */
  virtual CORBA::Double getAntennaCableDelay(const char* antennaName);
  
  /**
   * Method to set the Delay Event Duration
   *
   * \exception DelayServerExceptions::MaxEventDurationErrorEx
   */
  virtual void setDelayEventDuration(ACS::TimeInterval duration);
  
  virtual ACS::TimeInterval getDelayEventDuration();

  /**
   * Method to set the Polynomial Order
   *
   * \exception DelayServerExceptions::SetPolynomialOrderErrorEx
   */
  virtual void setPolynomialOrder(CORBA::Long newOrder);
  
  virtual CORBA::Long getPolynomialOrder();
 
  /**
   * Method to define the type of Atmospheric model to use 
   */
  virtual void setAtmosphericDelayModelType(Control::AtmosphericDelayModelType);

  /* See the IDL for documentation on this method */
  virtual Control::DelayServerBase::AntennaDelayModelSeq* 
    getCurrentAtmosphericDelayModel();

  /* See the IDL for documentation on this method */
  virtual void setAtmosphericDelayModel(ACS::Time applicationTime,
                 const Control::DelayServerBase::AntennaDelayModelSeq& model);

  /* See the IDL for documentation on this method */
  virtual void setAtmosphericDelayModelASDM(ACS::Time applicationTime,
                                  const asdmIDL::CalWVRTableIDL& atmModel);

  /**
   * Method to get the type of Atmospheric model currently in use
   */
  virtual Control::AtmosphericDelayModelType getAtmosphericDelayModelType();

  virtual void enableAntennaModel(bool enableAntennaModel);
 
  virtual bool antennaModelEnabled() ;
 
  /* This is the worker method which should be called 
     by the service thread.  It will cause a sequence of
     delay events to be generated */
  virtual void generateDelayEvent();
    
 protected:
  /* --------- controlAlarmHelper Overrides -------- */
  /* We probably should do something with these, but until we get
   * farther along in blanking and flagging it is unclear what
   */
  virtual void handleActivateAlarm(int code){};
  virtual void handleDeactivateAlarm(int code){};
  //void setError(std::string){};
  //void clearError(){};

  /* This method accesses the TMCDB and determines the information about
     the antenna from there */
  virtual DelayServerData getAntennaData(const char* antennaId) = 0;


  /* --- Pure Virtual methods that must be overridder ------ */
  /**
   * Method to fill the delay table with values
   *
   * \exception DelayServerExceptions::SetSourceErrorExImpl
   * \exception DelayServerExceptions::GetDelayTableErrorExImpl
   * \exception DelayServerExceptions::EmptySourceListExImpl
   */
  virtual ACS::Time fillDelayTable(ACS::Time startTime) = 0;
  
  /* This is the method which actually publishes the delay event
     in the BaseInterface class it is attached to the notification
     channel.  But for testing it is nice to be able to avoid that
  */
  virtual void publishDelayEvent(const Control::ArrayDelayEvent&) = 0;  

  /* This method populates the weather data in each DelayServerData
     object.  This is just an iteration through the antennaData_m map.
  */
  virtual void getWeatherData() =0;

  /* -----  End Abstract methods -------------*/
  
  inline unsigned int calcDelayIndex(unsigned int antennaIndex,
				     unsigned int timeIndex) const {
    return (antennaIndex * (polynomialOrder_m +1)) + timeIndex;
  }
    
  /* This is the worker thread class, although it is defined as
     part of the DelayServerBaseImpl, the actual activation is 
     delegated to the DelayServerWrapperClass
  */
  class ServiceLoop : public ACS::Thread { 
  public: 
    ServiceLoop(const ACE_CString&    name,
                DelayServerBaseImpl&  delayServer_p);
    ~ServiceLoop();
    void runLoop();
  private:
    DelayServerBaseImpl&  delayServer_m;
  };

  /* Member Variables */
  ServiceLoop*                 serviceThread_p; 

  
  virtual void newSourceAdded(ACS::Time newSourceStartTime);
  
  void allocateDelayTable();
      
  /*-- The following methods are used to generate delay events ---*/
  Control::ArrayDelayEvent createArrayDelayEvent(ACS::Time startTime); 
  virtual void publishSingleDelayEvent();
  virtual void regenerateDelayEvent();

  virtual void populateTotalDelayEvent(Control::ArrayDelayEvent&,
                                       ACS::Time delayStartTime, 
                                       ACS::Time delayStopTime);
  virtual void populateQuantizedDelays(Control::ArrayDelayEvent&,
                                       ACS::Time eventStartTime,
                                       ACS::Time delayStartTime, 
                                       ACS::Time delayStopTime);
  /*--------- End delay event generation methods. --------*/

  /* This routine evaluates the Chebyshev Polynomial which gives
     the wet and dry path delay.  Because we don't need to track
     the fequency dependence (here) we evaluate it at 0, which
     makes everyones life much easier.
  */
  double evaluatePolynomial(const std::vector<float> pathPolynomial);


  /* This method checks if the specified antenna is defined for the
     array and generates an IllegalParameterExImpl if it is not.
     If it is it returns an iterator pointing to the correct element
  */
  std::map<std::string, DelayServerData>::iterator findAntenna(const char*);


  void printDelayTable();

  /* This method goes through the table and zeros any row in which
     all values are > 1E20 or less than -1E20 */
  void checkForSingularResults();

  /* Protected Member Variables */
  std::map<std::string, DelayServerData>       antennaData_m;
  unsigned int                                 polynomialOrder_m;
  
  /**
   * Delay table is an array of polynomialOrder_m+1 by length of antennaList
   * it is protected by the delayTableMutex variable.  For speed I do pointer
   * arithemtic here rather that using a STL class. 
   * We will also need to know the antenna position for applying additional
   * corrections to the delayTable.
   */
  std::vector<ACS::Time>                       delayTime_m;

  AtmosphericDelayModel*                       atmDelayModel_p;

  /* This structure is used in the generation of the quatized values
     for the delay events */
  struct DelayPair {
    unsigned long  relativeStartTime;
    unsigned long  fineSampleDelay;
  };


  
  /* Parameters */
  const ACS::TimeInterval MAX_EVENT_DURATION;
  const long long         ACS_TIME_TO_MILLISEC;
  
  /* Member Variables */
  ACS::TimeInterval            delayEventDuration_m;
  acstime::Epoch               lastEventStopTime_m;
  ACE_Recursive_Thread_Mutex*  delayTableMutex_p;
  std::string                  arrayName_m;
   long                         eventSequenceNumber_m;
  double                       causalityDelay_m;
  /* DelayServerAntennaModel*     delayAntennaModel_p; */
  bool                         antennaModelEnabled_m;

  private:
  //This will be called when the delay server is created (constructor)
  // and when the delay server is initialized. 
  virtual std::vector<Control::AlarmInformation> createAlarmVector();
};

#endif /*!DELAYSERVERBASEIMPL_H*/
