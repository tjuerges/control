#ifndef CALCDATAMANAGER_H
#define CALCDATAMANAGER_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2007-10-06  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <vector>
#include <acstimeEpochHelper.h>
#include <structures.h>
#include <calcCommType.h>
#include <DelayServerExceptions.h>

class CalcDataManager {
public:
  CalcDataManager();
  virtual ~CalcDataManager();

  void addAntenna(const CalcAntenna_t& newAntenna);
 
  /** 
   * Static method which sets the runtime environment for the 
   * CALC process
   */
  static bool setCalcEnvironment();
  
  /**
   * Get the IERSData using casa
   */
  virtual bool getIERSData(double mjd);

  /* Set the location of the source reference */
  virtual void setReference(const CalcAntenna_t& newReference);

  /* Set the order of the polynomial interpolation */
  virtual void setPolynomialOrder(const unsigned int& polyorder);

  /* Debugging rountine which lets us see the arguments */
  void printCalcArgs();
  void printCalcData();
  
  /* Change the pressure associated with an antenna */
  void setAntennaPressure(const CalcPressureData_t& newPressure);


  /* Fill the Source Table with all the same object */
  virtual void setSource(const CalcSource_t& newSource);

  /* Fill Source Table */
  /**
   * Method which populates the source table
   * 
   * \exception DelayServerExceptions::TableSizeErrorExImpl
   */
  virtual void fillSourceTable(const std::vector<CalcSource_t>& sourceTable);
    
  /* Fill Delay Table */
  /**
   * Method which populates the Delay Table
   *
   * \exception DelayServerExceptions::TableSizeErrorExImpl
   */
  virtual void fillDelayTable(const std::vector<ACS::Time>& timeList);

  virtual double* getDelayTable();
  virtual double* getAzTable();
  virtual double* getElTable();
  virtual double* getDryAtmTable();
  virtual double* getWetAtmTable();
  virtual int     getDelayTableSize();

  /* Things we should be able to configure */
  /* pressure */
  /* kflags */



protected:
  /* These are the same methods found in delayServerBase */

  /* Protected Method */
  inline unsigned int calcDelayIndex(unsigned int antennaIndex,
                                     unsigned int timeIndex) const {
    return (antennaIndex * (polynomialOrder_m +1)) + timeIndex;
  }


  double*                                      delayTable_m;
  double*                                      azTable_m;
  double*                                      elTable_m;
  double*                                      dryAtmTable_m;
  double*                                      wetAtmTable_m;
  virtual void allocateDelayTable();

  /* Utility routine that finds the value of TAI-UTC */
  double findTAI2UTC();

  static getCALC_arg  calcArgs_m; // These are the arguments that are sent to calc


  std::vector<CalcAntenna_t> antennaList_m;
  std::vector<CalcSource_t>  sourceTable_m;
  std::vector<double>        pressure_m;

  unsigned int polynomialOrder_m; // The order of the polynomial interpolation
};
#endif /*!CALCDATAMANAGER_H*/
