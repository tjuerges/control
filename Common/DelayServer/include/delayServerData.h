#ifndef DELAYSERVERDATA_H
#define DELAYSERVERDATA_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2005 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2005-07-30  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <vector>
#include <string>
#include <deque>
#include <acstime.h>

class DelayServerData {
 public:
  
  typedef struct {
    ACS::Time applicationTime;
    double zenithDryDelay;
    double zenithWetDelay;
  } AtmosphericModel;

  DelayServerData();
  ~DelayServerData();

  /* This method resizes the delay tables to provide sufficient
     space for the values */
  void setPolynomialOrder(unsigned int newOrder);

  /* This method calculates the total delay based on the terms 
     in the structure.
     The values in the wet and dry Atm Delay are used, not the 
     values in the AtmosphericModel.
  */
  void calculateTotalDelay();

  /* Method to add a new atmospheric model to the deque.  Any model
     whose last valid time is over 5 minutes ago will be removed */
  void addAtmosphericModel(AtmosphericModel& model);
  
  /* This method returns the atmospheric model in effect for the specified
     time 
     
     @exception ControlExceptions::InvalidRequestExImpl if no model is
     available for the specified time.
  */
  AtmosphericModel getAtmosphericModel(ACS::Time targetTime);

  /* - These are the methods which actually calculate the module -*/
  void zeroDryComponent();
  void zeroWetComponent();

  /* This method uses the current time (it's a parameter to ensure
     all antennas use the same time) to find the atmospheric model
     and uses this and the mapping function to calculate both
     the wet and dry components.
  */
  void applyZenithDelayValue(ACS::Time currentTime);
  
  /* ------------ These are the fields in the structure */
  std::string antennaName;
  std::string padName;
  std::vector<double> antennaVector;
  std::vector<double> padVector;
  std::vector<double> offsetVector;
  
  std::vector<double> geometricDelay;
  std::vector<double> dryAtmDelay;
  std::vector<double> wetAtmDelay;
 
  std::vector<double> azDirection;
  std::vector<double> elDirection;
    
  double              antennaCableDelay;
  double              pressure;

  std::vector<double> totalDelay;

  

 protected:
  unsigned int                 polynomialOrder_m;
  std::deque<AtmosphericModel> atmosphericModel_m;

  /* This method returns an iterator to the atmospheric model in effect
     at the specified time.  Since the models are assumed to be good until
     replaced.  The only time no model is available will be if the list is
     empty or the time specified is before all other models in the list.
     In both of these cases, the atmosphericModel_m.end() will be returned.
  */

  std::deque<AtmosphericModel>::iterator getAtmosphericModelIter(ACS::Time);
  const static ACS::Time ExpirationTime;


  double almaDryMappingFunction(const double elevation);
  double almaWetMappingFunction(const double elevation);
  double mappingFunction(const double elevation, const double a,
                         const double b, const double c);
};

#endif //DELAYSERVERDATA_H
