// "@(#) $Id$"
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007 - 2010 
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#ifndef CALCSKYDELAYSERVERIMPL_H
#define CALCSKYDELAYSERVERIMPL_H

#include <delayServerBaseImpl.h>
#include <SkyDelayServerS.h>
#include <calcCommType.h> // for CalcAntenna_t & CalcSource_t
#include <acsComponentSmartPtr.h> // for maci::SmartPtr
#include <boost/shared_ptr.hpp> // for boost::shared_ptr
#include <list>
#include <vector>

// Forward Declarations
class CalcCommManager;
namespace Control {
    class CurrentWeather;
    class TrackableObject;
}
namespace TMCDB {
    class Access;
}

class CalcSkyDelayServerImpl : public virtual POA_Control::SkyDelayServer,
                               public DelayServerBaseImpl
{
public:
    CalcSkyDelayServerImpl();
  
    ~CalcSkyDelayServerImpl();

    /* ------------- External Methods --------------- */
    /**
     * Method which initializes the server 
     *
     * \exception DelayServerExceptions::InitializeServerErrorEx
     */
    virtual void initializeServer(const char* arrayId);
  
    /**
     * Method to add and antenna to the delay server
     *
     * \exception DelayServerExceptions::AddAntennaErrorEx
     */
    virtual void addAntenna(const char* antennId);
  
    /**
     * Method to set the Polynomial Order of the delay interpolation
     *
     * \exception DelayServerExceptions::SetPolynomialOrderErrorEx
     */
    virtual void setPolynomialOrder(CORBA::Long polynomialOrder);
  
    /**
     * Method to set the reference location
     *
     * \exception DelayServerExceptions::SetReferenceErrorEx
     *
     */
    virtual void setReference(double      xPosition,
                              double      yPosition,
                              double      zPosition);

    /**
     * Method to update the antenna position
     *
     * \exception DelayServerExceptions::SetAntennaPositionErrorEx
     */
    /* virtual void setAntennaPosition(const  */
    /*                                   Control::AntennaCharacteristics& antChar); */

    /// See the IDL file for a description of this function.
    virtual void addSource(const Control::DelaySource& newSource);

    /// See the IDL file for a description of this function.
    virtual void addSources(const Control::DelaySourceSeq& newSources);

protected:
    /**
     * Method to fill the delay table with values
     *
     * \exception DelayServerExceptions::SetSourceErrorExImpl
     * \exception DelayServerExceptions::GetDelayTableErrorExImpl
     * \exception DelayServerExceptions::EmptySourceListExImpl
     */
    virtual ACS::Time fillDelayTable(ACS::Time startTime);

    CalcAntenna_t getCalcAntenna(const DelayServerData&);

    /**
     * Method to fill the weather data in the DelayServerData structures
     */
    void getWeatherData();

    // A reference to a weather station component. By default its
    // Control::WeatherStation::_nil() and is initialized by the 
    // wrapper class as part of the initialization routines.
    maci::SmartPtr<Control::CurrentWeather> ws_m;


    /* This method accesses the TMCDB and determines the information about
       the antenna from there
     
       @exception TmcdbConnectionFailureEx
       @exception TmcdbErrType::TmcdbNoSuchRowEx
    */
    virtual DelayServerData getAntennaData(const char* antennaId);
    maci::SmartPtr<TMCDB::Access> tmcdb_m; 

private:
    std::list<Control::DelaySource> sourceList_m;

    /* Use a pointer so I can control the time of instanciation
       don't want to throw an exception in the component contstructor
    */
     
    CalcCommManager*   commManager_p;

    // the current array reference position
    std::vector<double> refPosition_m;
    
    // A function to convert a delay Source to a set of positions. If the
    // sources RA/Dec changes its position is sampled at the specified times.
    std::vector<CalcSource_t> delayToCalcSource
    (const Control::DelaySource& source,
     const std::vector<ACS::Time>& times);

    // A function to convert a delay Source to a trackable object. 
    boost::shared_ptr<Control::TrackableObject> delayToTrackableObject
    (const Control::DelaySource& source);
};

#endif /*!CALCSKYSERVERIMPL_H*/
