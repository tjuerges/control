#ifndef ATMOSPHERIC_DELAY_MODEL_H
#define ATMOSPHERIC_DELAY_MODEL_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* rsoto   2007-12-13  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <delayServerData.h>
#include <string>
#include <map>
#include <DelayServerBaseC.h>

/* This is an abstract interface which all of our AtmosphericDelay Models
   should implment.

   The job of this class is to populate the wet and dry atmospheric 
   delay vectors.  

   These vectors are all populated by CALC before being being called so
   it is important to zero any terms that you do not want.
*/

class AtmosphericDelayModel {
 protected:
  AtmosphericDelayModel(Control::AtmosphericDelayModelType);

 public:
  virtual ~AtmosphericDelayModel(){}

  virtual bool calculateAtmosphericModel
    (std::map<std::string, DelayServerData>& antennaData) = 0;

  const Control::AtmosphericDelayModelType modelType;
};

/* 
   This is the simplist model, it just zeros both the wet and dry values 
   from CALC.
*/
class AtmosphericZeroDelayModel: public AtmosphericDelayModel {
 public:
    AtmosphericZeroDelayModel();
    virtual ~AtmosphericZeroDelayModel(){}

    virtual bool calculateAtmosphericModel
      (std::map<std::string, DelayServerData>& antennaData);
};

/* 
   This model just set the wets portion of the model to zero and leaves
   the Dry portion untouched.  Thus we get the CALC dry model
*/
class CALCDryAtmosphereModel: public AtmosphericDelayModel {
 public:
    CALCDryAtmosphereModel();
    virtual ~CALCDryAtmosphereModel(){}

    virtual bool calculateAtmosphericModel
      (std::map<std::string, DelayServerData>& antennaData);
};

/* 
   This model does nothing to the  model and thus we get the CALC 
   wet and dry model
*/
class CALCWetAtmosphereModel: public AtmosphericDelayModel {
 public:
    CALCWetAtmosphereModel();
    virtual ~CALCWetAtmosphereModel(){}

    virtual bool calculateAtmosphericModel
      (std::map<std::string, DelayServerData>& antennaData);
};

/* 
  This just uses the Dry portion of the current alma model
*/
class ALMADryAtmosphereModel: public AtmosphericDelayModel {
 public:
    ALMADryAtmosphereModel();
    virtual ~ALMADryAtmosphereModel(){}

    virtual bool calculateAtmosphericModel
      (std::map<std::string, DelayServerData>& antennaData);
};

/* 
   This model uses the current ALMA Model
*/
class ALMAWetAtmosphereModel: public AtmosphericDelayModel {
 public:
    ALMAWetAtmosphereModel();
    virtual ~ALMAWetAtmosphereModel(){}

    virtual bool calculateAtmosphericModel
      (std::map<std::string, DelayServerData>& antennaData);
};


#endif /*!ATMOSPHERIC_DELAY_MODEL_H*/
