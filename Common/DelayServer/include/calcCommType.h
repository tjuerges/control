#ifndef CALCCOMMTYPE_H
#define CALCCOMMTYPE_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2007-10-09  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <string>

using std::string; 

/* CalcCommunication HeaderFile */

typedef enum {
  Antenna = 10,
  Reference,
  AntennaPressure,
  PolynomialOrder,
  Source,
  Delays,
  Initialize,
  ExitRequest
} CalcRequestType_t;

typedef enum {
  Success = 1,
  Exit,
  Timeout,
  Error
} CalcStatusType_t;
  

typedef struct {
  char   antennaName[4];
  double x;
  double y;
  double z;
  double axisOffset;
} CalcAntenna_t;

typedef struct {
  double RA;
  double Dec;
  double dRA;
  double dDec;
  double Epoch;
  double Parallax;
} CalcSource_t;

typedef struct {
  char   antennaName[4];
  double pressure;
} CalcPressureData_t;

#ifndef INFTIM
#define INFTIM -1
#endif

#define SIGCHLD_HANDLER(pid) (void (*)(int)) waitpid(pid, NULL, WNOHANG)

#endif /*!CALCCOMMTYPE_H*/
