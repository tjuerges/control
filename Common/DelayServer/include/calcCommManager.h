#ifndef CalcCommManager_H
#define CalcCommManager_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2007-10-15  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif
 
#include <calcCommType.h>
#include <poll.h>
#include <vector>
#include <acstimeEpochHelper.h>
#include <DelayServerExceptions.h>

#include <delayServerBaseImpl.h>

class CalcCommManager{
public:
  /**
   * Constructor:
   * 
   * \exception DelayServerExceptions::ReinitializationErrorExImpl
   */
  CalcCommManager();

  /**
   * Constructor taking in today's MJD useful for testing
   * 
   * \exception DelayServerExceptions::ReinitializationErrorExImpl
   */
  CalcCommManager(const double& todayMJD);

  virtual ~CalcCommManager();

  int calcChildHandler();

  /* Data Sending Methods */
  /**
   * Method to add and Antenna to the set of antennas in the
   * delay server
   *
   * \exception DelayServerExceptions::ReinitializationErrorExImpl
   */
  void addAntenna(CalcAntenna_t newAntenna);

  /**
   * Method to set the reference position.
   *
   * \exception DelayServerExceptions::ReinitializationErrorExImpl
   */
  void setReference(CalcAntenna_t newReference);

  /**
   * Method to set the order of polynomial used in the 
   * interpolation of position
   *
   * \exception DelayServerExceptions::ReinitializationErrorExImpl
   */
  void setPolynomialOrder(unsigned int polynomialOrder);

  
  /**
   * Method to set a single source in the delay model
   *
   * \exception DelayServerExceptions::ReinitializationErrorExImpl
   */
  void setSource(CalcSource_t newSource);

  /**
   * Method to set a list of sources in the delay model
   *
   * \exception DelayServerExceptions::ReinitializationErrorExImpl
   */
  void setSource(std::vector<CalcSource_t> newSourceList);

  /**
   * Method to send all pressure data for an array
   */
  void updatePressureData(std::map<std::string, DelayServerData>& antData);

  /**
   * Method to set the pressure at a given antenna
   */
  bool writeAntennaPressure(CalcPressureData_t antennaPressure);

  /* Data Retreival Method */
  /**
   * Method to get the resultant delay table back from the server
   *
   * \exception DelayServerExceptions::ReinitializationErrorExImpl
   */
  void getDelayTable(ACS::Time* delayTime, 
                     std::map<std::string, DelayServerData>& map);
/* 		     double* delayTable,  */
/* 		     double* azTable, */
/* 		     double* elTable, */
/* 		     double* dryAtmTable, */
/* 		     double* wetAtmTable); */

  /**
   * Method to do a full reinitilization of the Calc Process
   *
   * \exception DelayServerExceptions::ReinitializationErrorExImpl
   */
  virtual void reinitializeCalcProcess();

 protected:
  // This constructor exists for testing purposes only.
  CalcCommManager(string CalcExecutableName);

  /* Methods for managing the child process */
  /**
   * Method which starts the Calc Process 
   * \exception DelayServerExceptions::CommunicationChannelErrorExImpl
   * \exception DelayServerExceptions::StartCalcErrorExImpl)
   */
  virtual void startCalcProcess();
  virtual void closeCalcProcess();
  bool initializeCalcProcess();
  
  /* This method just reads the responses from the pipe into the transfer 
     buffers */
  void readCALCProcessResults(ACS::Time*);


  /* This method resizes the buffers used to hold return values from 
     the calc process */
  void reallocateTransferBuffers();

  /* Returns false if the child process appears to be running correctly */
  bool checkCalcProcess();
 
  bool writeAll(void* buf, size_t count);
  bool readAll(void* buf, size_t count);

  bool writeAntenna(CalcAntenna_t newAntenna);
  bool writeReference();
  bool writePolynomialOrder();
  bool writeSourceList();

  /* Method which listens for a response on the input stream */
  CalcStatusType_t getChildMessage();

  int   outputFd_m;
  int   inputFd_m;

  pid_t childProcessID_m;

  const string calcExecutableName_m;

  std::vector<CalcAntenna_t> antennaList_m;
  CalcAntenna_t              reference_m;
  unsigned int               polynomialOrder_m;
  std::vector<CalcSource_t>  sourceList_m;
  double                     todayMJD_m;

  /* These memory registers are used to store the data 
     as we read it back from the calc process */
  double* delayTable_m; 
  double* azTable_m; 
  double* elTable_m;
  double* dryAtmTable_m; 
  double* wetAtmTable_m;

  /* Mutex to prevent reallocation during use */
  ACE_Recursive_Thread_Mutex*  transferTableMutex_p;

};


#endif /*!CalcCommManager_H*/
