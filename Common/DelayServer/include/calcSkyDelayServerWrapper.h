#ifndef CALCSKYDELAYSERVERWRAPPER_H
#define CALCSKYDELAYSERVERWRAPPER_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2005 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2005-07-30  created
*/

/************************************************************************
 * The interface class is a class which delegates all of the real
 * work to the delayServerBaseImpl class.  These classes are split to
 * simplify more sophisticated testing.
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

class nc::SimpleSupplier;

class CalcSkyDelayServerWrapper: 
public acscomponent::ACSComponentImpl,
public CalcSkyDelayServerImpl
{
 public:
  CalcSkyDelayServerWrapper(const ACE_CString&             name,
                     maci::ContainerServices       *containerServices);
  
  ~CalcSkyDelayServerWrapper();
   
  /**
   * Method which initializes the server 
   *
   * \exception DelayServerExceptions::InitializeServerErrorEx
   */
  virtual void initializeServer(const char* arrayId);

  /**
   * Method to set the Delay Event Duration
   *
   * \exception DelayServerExceptions::MaxEventDurationErrorEx
   */
  virtual void setDelayEventDuration(ACS::TimeInterval duration);
    

  /* ------- ACS Component Lifecycle ------------- */
  /**
   * ACS Lifecycle method cleanUp
   *
   * \except acsErrTypeLifeCycle::LifeCycleExImpl
   */
  virtual void cleanUp();
    
 protected:
  /* Method which actually published the delay events on 
     a notification channel */
  virtual void publishDelayEvent(const Control::ArrayDelayEvent&);
  
  //DelayServerAlarms*                           delayAlarms_p;

 private:
  /* Classes */
  class DelayPublisher : public nc::SimpleSupplier { 
  public: 
    DelayPublisher(const char *channelName, 
 		   acscomponent::ACSComponentImpl *component) : 
      nc::SimpleSupplier(channelName,component){} 
  }; 

  
  /* Member Variables */
  DelayPublisher               delayPublisher_m; 
};

#endif /*!DELAYSERVERBASEINTERFACE_H*/
