#ifndef SIMPLEDELAYSERVERIMPL_H
#define SIMPLEDELAYSERVERIMPL_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2005 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2005-07-30  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <SimpleDelayServerS.h>
#include <delayServerBaseImpl.h>

class SimpleDelayServerImpl :
public virtual POA_Control::SimpleDelayServer,
  public DelayServerBaseImpl
{
 public:
  SimpleDelayServerImpl();
  ~SimpleDelayServerImpl();

  /**
   * Method to set the delay polynomial for an antenna
   */
  virtual void setAntennaDelay(const char* antennaId,
			       const Control::SimpleDelayServer::
			       DelayPolynomial& delayPoly);


 protected:
  /**
   * Method to get the weather data
   */
  virtual void getWeatherData();
  virtual DelayServerData getAntennaData(const char* antennaId);

  /**
   * Method to evaluate and populate the delay table
   *
   * \exception DelayServerExceptions::EmptySourceListExImpl
   */
  virtual ACS::Time fillDelayTable(ACS::Time startTime);

 private:
  std::map<std::string, Control::SimpleDelayServer::DelayPolynomial> polynomials_m;
};

#endif /*!DELAYSERVERIMPL_H*/
