#ifndef DELAY_SERVER_ANTENNA_MODEL_H
#define DELAY_SERVER_ANTENNA_MODEL_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* rsoto   2008-08-24  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <acscomponentImpl.h>
#include <string>
//#include <WeatherStationC.h> // for Control::WeatherStation
#include <acsComponentSmartPtr.h> // for maci::SmartPtr

class DelayServerAntennaModel : public acscomponent::ACSComponentImpl{
 public:
  DelayServerAntennaModel(const ACE_CString&  name,
			  maci::ContainerServices* containerServices);
  ~DelayServerAntennaModel();
  double getModel(double actEl);
 private:
  double tCoeff;
  double elCoeff;
  //maci::SmartPtr<Control::WeatherStation> ws_m;
};


#endif /*!DELAY_SERVER_ANTENNA_MODEL_H*/
