#ifndef DELAYSERVERIMPL_H
#define DELAYSERVERIMPL_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2005
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2005-07-30  created
*/

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif


#include <DelayServerS.h>
#include <acscomponentImpl.h>
#include <acsncSimpleSupplier.h>
#include <ControlInterfacesC.h>
#include <acsThread.h>
#include <sys/time.h>
#include <queue>
#include <vector>


class DelayServerImpl: public virtual POA_Control::DelayServer,
    public acscomponent::ACSComponentImpl
{
    public:
    DelayServerImpl(const ACE_CString& name,
        maci::ContainerServices* containerServices);

    virtual ~DelayServerImpl();

    virtual void calcInit(const Control::DelayServer::Antenna& refAntenna);

    virtual void addAntenna(const Control::DelayServer::Antenna& Antenna);

    virtual void addSource(const Control::DelayServer::Source& Source);


    virtual void updateLoop() const;

    class ServiceLoop: public ACS::Thread
    {
        public:
        ServiceLoop(const ACE_CString& name,
            const DelayServerImpl* delayServer);

        virtual ~ServiceLoop();

        virtual void runLoop();

        private:
        const DelayServerImpl* delayServer_m;
    };

  /**
   * This class must exist solely because of the poor way the simple supplier
   * was written
   */
    class DelaySupplier: public nc::SimpleSupplier
    {
        public:
        DelaySupplier(const char*channelName,
            acscomponent::ACSComponentImpl* component):
            nc::SimpleSupplier(channelName,component)
        {
        };

        virtual ~DelaySupplier()
        {
        };
    };

    private:
    const double delayOffset_m;
    const double coarseRes_m;
    const double fineRes_m;


    DelaySupplier* eventPublisher_m;
    ServiceLoop* svcThread_m;

    acstime::Duration lookAheadTime_m;
    acstime::Duration responseTime_m;
    acstime::Duration calcInterval_m;

    Control::DelayServer::Antenna refPosition_m;
    std::vector< Control::DelayServer::Antenna > antennaList_m;
    std::queue< Control::DelayServer::Source > sourceList_m;

    mutable acstime::Epoch evaluationTime_m;
    mutable unsigned long lastCoarseDelay_m;
    mutable unsigned short lastFineDelay_m;
};
#endif /*!DELAYSERVERIMPL_H*/
