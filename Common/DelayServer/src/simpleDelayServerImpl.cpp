/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2005 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2005-07-30  created 
*/

/************************************************************************
*   NAME
*   
* 
*   SYNOPSIS
*
*   
*   PARENT CLASS
*
* 
*   DESCRIPTION
*
*
*   PUBLIC METHODS
*
*
*   PUBLIC DATA MEMBERS
*
*
*   PROTECTED METHODS
*
*
*   PROTECTED DATA MEMBERS
*
*
*   PRIVATE METHODS
*
*
*   PRIVATE DATA MEMBERS
*
*
*   FILES
*
*   ENVIRONMENT
*
*   COMMANDS
*
*   RETURN VALUES
*
*   CAUTIONS 
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS   
* 
*------------------------------------------------------------------------
*/


static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <simpleDelayServerImpl.h>
#include <TETimeUtil.h>
#include <acstimeDurationHelper.h>

SimpleDelayServerImpl::SimpleDelayServerImpl():
  DelayServerBaseImpl()
{
  ACS_TRACE("SimpleDelayServerImpl::SimpleDelayServerImpl");
}

SimpleDelayServerImpl::~SimpleDelayServerImpl() {
  ACS_TRACE("SimpleDelayServerImpl::~SimpleDelayServerImpl");
}

void SimpleDelayServerImpl::setAntennaDelay(const char* antennaId,
					    const Control::SimpleDelayServer::
					    DelayPolynomial& delayPoly) {
  ACS_TRACE("SimpleDelayServerImpl::setAntennaDelay");
  polynomials_m[antennaId] = delayPoly;

  if (polynomials_m[antennaId].referenceTime == 0) {
    polynomials_m[antennaId].referenceTime= 
      TETimeUtil::unix2epoch(time(0)).value;
  }

  newSourceAdded(TETimeUtil::unix2epoch(time(0)).value);
}

void SimpleDelayServerImpl::getWeatherData() {
  std::map<std::string, DelayServerData>::iterator iter;
  for (iter = antennaData_m.begin(); iter != antennaData_m.end(); iter++) {
    iter->second.pressure = 101325; // Standard Pressure
  }
}

DelayServerData SimpleDelayServerImpl::getAntennaData(const char* antennaId){
  DelayServerData newAntenna;
  newAntenna.antennaName = antennaId;
  newAntenna.setPolynomialOrder(polynomialOrder_m);

  newAntenna.antennaCableDelay = 0.0;
  newAntenna.padName = "WhoCares";
  newAntenna.padVector[0] = 0.0;
  newAntenna.padVector[1] = 0.0;
  newAntenna.padVector[2] = 0.0;

  /* Now populate the information from the Antenna Info */
  newAntenna.antennaVector[0] = 0.0;
  newAntenna.antennaVector[1] = 0.0;
  newAntenna.antennaVector[2] = 0.0;

  newAntenna.offsetVector[0] = 0.0;
  newAntenna.offsetVector[1] = 0.0;
  newAntenna.offsetVector[2] = 0.0;

  return newAntenna;
}


ACS::Time SimpleDelayServerImpl::fillDelayTable(ACS::Time startTime) {
  ACS_TRACE("SimpleDelayServerImpl::fillDelayTable");
  
  std::map<std::string, DelayServerData>::iterator iter;
  for (iter = antennaData_m.begin(); iter != antennaData_m.end(); iter++) {
    std::map<std::string, 
      Control::SimpleDelayServer::DelayPolynomial>::const_iterator polyIter =
      polynomials_m.find(iter->first);

    /* Zero the Delay Table */
    for (unsigned int timeIdx = 0; timeIdx <= polynomialOrder_m; timeIdx++) {
      iter->second.geometricDelay[timeIdx] = 0.0;
      iter->second.dryAtmDelay[timeIdx] = 0.0;
      iter->second.wetAtmDelay[timeIdx] = 0.0;
    }

    if (polyIter == polynomials_m.end()) {
      /* No Polynomial found, enter zero's for all times */
      continue;
    }

    for (unsigned int timeIdx = 0; timeIdx <= polynomialOrder_m; timeIdx++) {
      ACS::TimeInterval timeOffset = 
        delayTime_m[timeIdx] - polyIter->second.referenceTime;
      DurationHelper deltaT(timeOffset);
      for (unsigned int term=0; term<polyIter->second.coeffs.length(); term++){
	iter->second.geometricDelay[timeIdx] *= deltaT.toSeconds();
        iter->second.geometricDelay[timeIdx] += polyIter->second.coeffs[term];
      }
    } 
  }
  return delayTime_m[polynomialOrder_m];
}

/* ---------------- This should be a template -------------- */
#include <simpleDelayServerWrapper.h>

#include <acsncSimpleSupplier.h>

SimpleDelayServerWrapper::SimpleDelayServerWrapper(const ACE_CString& name,
                                                  maci::ContainerServices *cs):
  ACSComponentImpl(name,cs),
  SimpleDelayServerImpl(),
  delayPublisher_m(Control::CHANNELNAME_CONTROLREALTIME, this)
{
  const std::string fnName =
    "SimpleDelayServerWrapper::SimpleDelayServerWrapper";
  ACS_TRACE(fnName);
  
  serviceThread_p = getContainerServices()->getThreadManager()->
    create<ServiceLoop, DelayServerBaseImpl>(name+"ServiceThread", *this);
   serviceThread_p -> setSleepTime(delayEventDuration_m/2);
}


SimpleDelayServerWrapper::~SimpleDelayServerWrapper() {
  delayTableMutex_p->acquire();

  if (serviceThread_p != NULL) {
    serviceThread_p->terminate();
    serviceThread_p = NULL;
  }
}

void SimpleDelayServerWrapper::cleanUp() {
  const std::string fnName = "SimpleDelayServer::cleanUp";
  ACS_TRACE(fnName);

  ACS::ThreadSyncGuard  guard(delayTableMutex_p, true);
  if (serviceThread_p != NULL) {
    serviceThread_p->terminate();
    serviceThread_p = NULL;
  }

  ACSComponentImpl::cleanUp();
}


void SimpleDelayServerWrapper::initializeServer(const char* arrayId) {
  const std::string fnName = "SimpleDelayServerWrapper::initializeServer";
  ACS_TRACE(fnName);

  SimpleDelayServerImpl::initializeServer(arrayId);

//   // Initialize the delay server alarms with the array name
//   delayAlarms_p = new DelayServerAlarms(arrayName_m);

  serviceThread_p->resume();
}

void SimpleDelayServerWrapper::
setDelayEventDuration(ACS::TimeInterval duration) {
  const std::string fnName = "SimpleDelayServerWrapper::setAntennaCableDelay";
  ACS_TRACE(fnName);

  SimpleDelayServerImpl::setDelayEventDuration(duration);
  serviceThread_p->setSleepTime(delayEventDuration_m/2);
}


void SimpleDelayServerWrapper::
publishDelayEvent(const Control::ArrayDelayEvent& ade) {
  const std::string fnName = "SimpleDelayServerWrapper::publishDelayEvent";
  try {
    delayPublisher_m.publishData<Control::ArrayDelayEvent>(ade);
  } catch (ACSErrTypeCommon::CORBAProblemEx& ex) {
    /* Since this is from a thread not much to do but log it and continue */
    ACS_LOG(LM_SOURCE_INFO,fnName.c_str(),
            (LM_WARNING,"Unable to publish delay event."));
  }
}


/////////////////////////////////////////////////
// MACI DLL support functions
/////////////////////////////////////////////////
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(SimpleDelayServerWrapper)
/*___oOo___*/
