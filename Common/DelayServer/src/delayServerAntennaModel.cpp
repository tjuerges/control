/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* rsoto  2008-08-25  created 
*/

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "delayServerAntennaModel.h"
#include <string>
using std::string;

DelayServerAntennaModel::DelayServerAntennaModel(const ACE_CString& name,
						 maci::ContainerServices* cs):
  ACSComponentImpl(name, cs)
  //ws_m(Control::WeatherStation::_nil())
{
  // Get reference to weather station
  //try {
  // const string wsComponentName = "CONTROL/WeatherStation";
  
  // Change to use a non-sticky reference when the Master starts the
  // weather station.
  //ws_m = getContainerServices()->getComponentSmartPtr<Control::WeatherStation>(wsComponentName.c_str());
  //} catch (maciErrType::CannotGetComponentExImpl& ex) {
  //  string errorMsg = "Cannot connect to the weather station";
  //  errorMsg += "Delay Server Refraction correction will not be available";
  //  errorMsg += " Detailed error trace follows.";
  //  ex.log(LM_WARNING);
  //  ws_m = Control::WeatherStation::_nil();
  //}
  
  
  // Here we should load the table models 
}


DelayServerAntennaModel::~DelayServerAntennaModel()
{ 
  // release the weather station. The smart pointer will call
  // releaseComponent if necessary.
  //ws_m = Control::WeatherStation::_nil();
} 

double DelayServerAntennaModel::getModel(double actEl) 
{
 
  const string fnName = "DelayServerAntennaModel::getModel";
  AUTO_TRACE(fnName);
  
  //   // Check the reference was created.
  //   if (CORBA::is_nil(&*ws_m)) 
  //     return 0.0;
  
  //   // Check the weather station component is really there (and has not been
  //   // shutdown or crashed
  //   try {
  //     if (ws_m->componentState() != ACS::COMPSTATE_OPERATIONAL) {
  //       const string msg("The weather station is not operational");
  //       LOG_TO_AUDIENCE(LM_ERROR, fnName, msg, log_audience::OPERATOR);
  //       return 0.0;
  //     }
  //   } catch (CORBA::SystemException& ex) {
  //     string msg("Cannot communicate with the weather station component.");
  //     msg += " Perhaps this component or its container have been shutdown.";
  //     msg += " Check that the control subsystem operational.";
  //     msg += " Will switch to using default weather parameters.";
  //     LOG_TO_AUDIENCE(LM_ERROR, fnName, msg, log_audience::OPERATOR);
  //     ws_m = Control::WeatherStation::_nil();
  //     return 0.0; 
  //   }
  
  // Some operation with tCoff and elCoeff 
  // For the moment I returned 0 as antenna model correction
  return 0.0;
}

