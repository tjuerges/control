/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2005 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2005-07-30  created 
*/

/************************************************************************
*   NAME
*
* 
*------------------------------------------------------------------------
*/


static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <delayServerBaseImpl.h>
#include <TETimeUtil.h>
#include <delayReceiver.h>
#include <limits.h>
#include <ControlExceptions.h>
#include <DelayServerExceptions.h>
#include <PolynomialInterpolatorImpl.h>
#include <LogToAudience.h>
#include <ASDM.h>
#include <CalWVRTable.h>
#include <CalWVRRow.h>

#include <queue>
#include <vector>

using namespace std;
//using Control::DelayServerBase::AntennaDelayModelSeq;
using Control::AtmosphericDelayModelType;
using Control::ZERO_ATMOSPHERE_MODEL;
using Control::CALC_DRY_ONLY;
using Control::CALC_WET_AND_DRY;
using Control::ALMA_DRY_ONLY;
using Control::ALMA_WET_AND_DRY;
using Control::AlarmInformation;

DelayServerBaseImpl::DelayServerBaseImpl():
  serviceThread_p(NULL),
  polynomialOrder_m(4),
  delayTime_m(polynomialOrder_m+1),
  atmDelayModel_p(NULL),
  MAX_EVENT_DURATION(600000000),
  ACS_TIME_TO_MILLISEC(10000),
  delayEventDuration_m(MAX_EVENT_DURATION),
  eventSequenceNumber_m(0),
  causalityDelay_m(100E-6),
  antennaModelEnabled_m(false)
{
  ACS_TRACE(__func__);
  
  delayTableMutex_p   = new ACE_Recursive_Thread_Mutex();
  atmDelayModel_p     = new AtmosphericZeroDelayModel();

  setPolynomialOrder(polynomialOrder_m);
 
  initializeAlarms("DelayServer", "Unkwon Array", createAlarmVector());
}

DelayServerBaseImpl::~DelayServerBaseImpl() {
  {
    ACS::ThreadSyncGuard  guard(delayTableMutex_p, true);
    antennaData_m.clear();
  }
  delete atmDelayModel_p;
  delete delayTableMutex_p;
}

void DelayServerBaseImpl::initializeServer(const char* arrayId) {
  ACS_TRACE(__func__);

  arrayName_m = arrayId;
  
 
  //DelayServerErrorManager::setArrayName(arrayId);
  //re-initialize the alarms with the correct array as a member.
  initializeAlarms("DelayServer", arrayId, createAlarmVector());
  
  lastEventStopTime_m.value = 
    TETimeUtil::floorTE(TETimeUtil::unix2epoch(time(0))).value;
}

void DelayServerBaseImpl::enableAntennaModel(bool enableAntennaModel) {
  antennaModelEnabled_m = enableAntennaModel;
  regenerateDelayEvent();
}

bool DelayServerBaseImpl::antennaModelEnabled() {
  return antennaModelEnabled_m;
}

void DelayServerBaseImpl::addAntenna(const char* antennaId) {
  ACS_TRACE(__func__);
  ACS::ThreadSyncGuard  guard(delayTableMutex_p, true);

  try{
    std::map<std::string, DelayServerData>::iterator iter = 
      findAntenna(antennaId);

    std::ostringstream output;
    output << "Antenna " << antennaId << " is already in delay server:";
    LOG_TO_OPERATOR(LM_INFO, output.str().c_str());
  } catch (ControlExceptions::IllegalParameterErrorExImpl& ex) {
    /* This is actually the good case add the antenna */
    DelayServerData newAntenna = getAntennaData(antennaId);

    std::ostringstream output;
    output << "Adding Antenna " << newAntenna.antennaName 
           << " on Pad: " << newAntenna.padName 
           << " to delay server:\n";
    output << "\tPad:     " << newAntenna.padVector[0] 
           << "\t" << newAntenna.padVector[1] 
           << "\t" << newAntenna.padVector[2] << "\n";
    output << "\tAntenna: " << newAntenna.antennaVector[0] 
           << "\t" << newAntenna.antennaVector[1] 
           << "\t" << newAntenna.antennaVector[2] << "\n";
    output << "\tOffset:  " << newAntenna.offsetVector[0] 
           << "\t" << newAntenna.offsetVector[1] 
           << "\t" << newAntenna.offsetVector[2] << "\n";
    LOG_TO_SCILOG(LM_INFO, output.str().c_str());
  
    antennaData_m[std::string(antennaId)] = newAntenna;
    regenerateDelayEvent();
  }
}

void DelayServerBaseImpl::removeAntenna(const char* antennaId) {
  ACS_TRACE(__func__);
  ACS::ThreadSyncGuard  guard(delayTableMutex_p, true);
  
  try{
    std::map<std::string, DelayServerData>::iterator iter = 
      findAntenna(antennaId);
    std::ostringstream output;
    output << "Removing Antenna " << antennaId << " from delay server";
    LOG_TO_OPERATOR(LM_INFO, output.str().c_str());
    antennaData_m.erase(iter);
  } catch (ControlExceptions::IllegalParameterErrorExImpl& ex) {
    /* Antenna is not defined, just return quietly */
    return;
  }
}    
      
Control::AntennaSeq* DelayServerBaseImpl::getAntennas() {
  ACS_TRACE(__func__);
  ACS::ThreadSyncGuard  guard(delayTableMutex_p, true);

  Control::AntennaSeq_var antSeq = new Control::AntennaSeq();
  antSeq->length(antennaData_m.size());
  unsigned int idx = 0;
  
  std::map<std::string, DelayServerData>::const_iterator iter;
  for (iter = antennaData_m.begin(); iter != antennaData_m.end(); iter++) {
    antSeq[idx] = CORBA::string_dup(iter->first.c_str());
    idx ++;
  }

  return antSeq._retn();
}

void DelayServerBaseImpl::setAntennaPadLocation(const char* antennaId,
                                                double x, double y, double z){
  ACS_TRACE(__func__);
  ACS::ThreadSyncGuard  guard(delayTableMutex_p, true);
  
  try {
    std::map<std::string, DelayServerData>::iterator iter =
      findAntenna(antennaId);

    iter->second.padVector[0] = x;
    iter->second.padVector[1] = y;
    iter->second.padVector[2] = z;

    std::ostringstream output;
    output << "Position of Pad " << iter->second.padName 
           << " corresponding to antenna " << iter->second.antennaName
           << " has been changed to:"
           << "\n\tX: "<< iter->second.padVector[0]
           << "\n\tY: "<< iter->second.padVector[1]
           << "\n\tZ: "<< iter->second.padVector[2];
    LOG_TO_OPERATOR(LM_INFO, output.str().c_str());
  } catch (ControlExceptions::IllegalParameterErrorExImpl& ex) {
    ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__, __LINE__, 
                                                       __func__);
    throw nex.getIllegalParameterErrorEx();
  }
}

void DelayServerBaseImpl::getAntennaPadLocation(const char* antennaId, 
                                                double& x, double& y, 
                                                double& z){
  ACS_TRACE(__func__);
  ACS::ThreadSyncGuard  guard(delayTableMutex_p, true);
  
  try {
    std::map<std::string, DelayServerData>::iterator iter =
      findAntenna(antennaId);
    x = iter->second.padVector[0];
    y = iter->second.padVector[1];
    z = iter->second.padVector[2];
  } catch (ControlExceptions::IllegalParameterErrorExImpl& ex) {
    ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__, __LINE__, 
                                                       __func__);
    throw nex.getIllegalParameterErrorEx();
  }
}

void DelayServerBaseImpl::setAntennaVector(const char* antennaId, double xPos,
                                           double yPos,  double zPos){
  ACS_TRACE(__func__);
  ACS::ThreadSyncGuard  guard(delayTableMutex_p, true);
  
  try {
    std::map<std::string, DelayServerData>::iterator iter =
      findAntenna(antennaId);

    iter->second.antennaVector[0] = xPos;
    iter->second.antennaVector[1] = yPos;
    iter->second.antennaVector[2] = zPos;

    std::ostringstream output;
    output << "Antenna " << iter->second.antennaName 
           << " has had it's relative position changed to:"
           << "\n\tX: "<< iter->second.antennaVector[0]
           << "\n\tY: "<< iter->second.antennaVector[1]
           << "\n\tZ: "<< iter->second.antennaVector[2];
    LOG_TO_OPERATOR(LM_INFO, output.str().c_str());
  } catch (ControlExceptions::IllegalParameterErrorExImpl& ex) {
    ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__, __LINE__, 
                                                       __func__);
    throw nex.getIllegalParameterErrorEx();
  }
}

void DelayServerBaseImpl::getAntennaVector(const char* antennaId, double& xPos,
                                           double& yPos,  double& zPos){
  ACS_TRACE(__func__);
  ACS::ThreadSyncGuard  guard(delayTableMutex_p, true);
  
  try {
    std::map<std::string, DelayServerData>::iterator iter =
      findAntenna(antennaId);
    xPos = iter->second.antennaVector[0];
    yPos = iter->second.antennaVector[1];
    zPos = iter->second.antennaVector[2];
  } catch (ControlExceptions::IllegalParameterErrorExImpl& ex) {
    ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__, __LINE__, 
                                                       __func__);
    throw nex.getIllegalParameterErrorEx();
  }

}

void DelayServerBaseImpl::
setAxisNonIntersectionParameter(const char* antennaId, double kTerm){
  ACS_TRACE(__func__);
  ACS::ThreadSyncGuard  guard(delayTableMutex_p, true);
  
  try {
    std::map<std::string, DelayServerData>::iterator iter =
      findAntenna(antennaId);

    iter->second.offsetVector[0] = kTerm;

    std::ostringstream output;
    output << "Antenna " << iter->second.antennaName 
           << " has had the axis non-intersection (K) term set to: "
           << iter->second.offsetVector[0];
    LOG_TO_OPERATOR(LM_INFO, output.str().c_str());
  } catch (ControlExceptions::IllegalParameterErrorExImpl& ex) {
    ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__, __LINE__, 
                                                       __func__);
    throw nex.getIllegalParameterErrorEx();
  }
}

double DelayServerBaseImpl::
getAxisNonIntersectionParameter(const char* antennaId){
 ACS_TRACE(__func__);
  ACS::ThreadSyncGuard  guard(delayTableMutex_p, true);
  
  try {
    std::map<std::string, DelayServerData>::iterator iter =
      findAntenna(antennaId);
    return iter->second.offsetVector[0];
  } catch (ControlExceptions::IllegalParameterErrorExImpl& ex) {
    ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__, __LINE__, 
                                                       __func__);
    throw nex.getIllegalParameterErrorEx();
  }
}

void DelayServerBaseImpl::setAntennaCableDelay(const char* antennaId,
                                               double newValue) {
  ACS_TRACE(__func__);
  std::ostringstream output;
  
  try {
    ACS::ThreadSyncGuard  guard(delayTableMutex_p, true);
    std::map<std::string, DelayServerData>::iterator iter =
      findAntenna(antennaId);

    if (iter->second.antennaCableDelay == newValue) return;

    iter->second.antennaCableDelay = newValue;
    regenerateDelayEvent();

    output << "Antenna " << iter->first << " cable delay set to "
           << iter->second.antennaCableDelay << " s";
  } catch (ControlExceptions::IllegalParameterErrorExImpl& ex) {
    ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__, __LINE__, 
                                                       __func__);
    throw nex.getIllegalParameterErrorEx();
  }
  LOG_TO_OPERATOR(LM_INFO, output.str().c_str());
}

CORBA::Double DelayServerBaseImpl::getAntennaCableDelay(const char* antennaId){
  ACS_TRACE(__func__);
  
  try {
    ACS::ThreadSyncGuard  guard(delayTableMutex_p, true);
    std::map<std::string, DelayServerData>::iterator iter =
      findAntenna(antennaId);
    return iter->second.antennaCableDelay;
  } catch (ControlExceptions::IllegalParameterErrorExImpl& ex) {
    ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__, __LINE__, 
                                                       __func__);
    throw nex.getIllegalParameterErrorEx();
  }
}



CORBA::Double DelayServerBaseImpl::getCausalityDelay() {
  ACS_TRACE(__func__);
  
  return causalityDelay_m;
}
 
void DelayServerBaseImpl::setCausalityDelay(double newDelayValue) {
  ACS_TRACE(__func__);
  
  if (newDelayValue == causalityDelay_m) return;


  ACS::ThreadSyncGuard  guard(delayTableMutex_p, true);
  causalityDelay_m = newDelayValue;


  regenerateDelayEvent();
  guard.release();

  std::ostringstream output;
  output << "Array causality delay set to: " << causalityDelay_m  << " s";
  LOG_TO_OPERATOR(LM_INFO, output.str().c_str());
}

void DelayServerBaseImpl::setDelayEventDuration(ACS::TimeInterval duration) {
  ACS_TRACE(__func__);

  if (duration > MAX_EVENT_DURATION) {
    throw DelayServerExceptions::MaxEventDurationErrorExImpl(__FILE__,__LINE__,
	   __func__).getMaxEventDurationErrorEx();
  }
  
  /* NAOJ requires that all events occur on TE boundaries.  This implies
     that the delayEventDuration must be an integer number of TEs */
  delayEventDuration_m = static_cast<int>(duration/480000.0) * 480000;
  ACS_LOG(LM_SOURCE_INFO, __func__,
          (LM_INFO, "Delay Event Duration now set to %d ms",
           delayEventDuration_m / 480000));
 
  std::ostringstream output;
  output << "Delay Event Duration now set to " 
         << delayEventDuration_m/1.0E7 << " s";
  LOG_TO_OPERATOR(LM_INFO, output.str().c_str());

}

ACS::TimeInterval DelayServerBaseImpl::getDelayEventDuration() {
  ACS_TRACE(__func__);

  return delayEventDuration_m;
} 

void DelayServerBaseImpl::setPolynomialOrder(CORBA::Long newOrder) {
  ACS_TRACE(__func__);
  
  if (polynomialOrder_m == static_cast<unsigned long>(newOrder)) return;
 
  ACS::ThreadSyncGuard  guard(delayTableMutex_p, true);
  polynomialOrder_m = newOrder;
  
  /* Allocate the Delay Table Space Here */
  allocateDelayTable();

  guard.release();

  std::ostringstream output;
  output << "Polynomial order has been changed to " << polynomialOrder_m;
  LOG_TO_OPERATOR(LM_INFO, output.str().c_str());
}

CORBA::Long DelayServerBaseImpl::getPolynomialOrder() {
  ACS_TRACE(__func__);
  return polynomialOrder_m;
}

void DelayServerBaseImpl::
setAtmosphericDelayModelType(AtmosphericDelayModelType type){
  if (type == atmDelayModel_p->modelType) return;
  ACS::ThreadSyncGuard  guard(delayTableMutex_p, true);

  ostringstream output;
  switch (type){
  case ZERO_ATMOSPHERE_MODEL:
    delete atmDelayModel_p;
    atmDelayModel_p = new AtmosphericZeroDelayModel();
    output << "Changing atmospheric delay model to type" 
           <<" \"ZERO_ATMOSPHERE_MODEL\"";
    break;
  case CALC_DRY_ONLY:
    delete atmDelayModel_p;
    atmDelayModel_p = new CALCDryAtmosphereModel();
    output << "Changing atmospheric delay model to type \"CALC_DRY_ONLY\"";
    break;
  case CALC_WET_AND_DRY:
    delete atmDelayModel_p;
    atmDelayModel_p = new CALCWetAtmosphereModel();
    output << "Changing atmospheric delay model to type \"CALC_WET_AND_DRY\"";
    break;
  case ALMA_DRY_ONLY:
    delete atmDelayModel_p;
    atmDelayModel_p = new ALMADryAtmosphereModel();
    output << "Changing atmospheric delay model to type \"ALMA_DRY_ONLY\"";
    break;
  case ALMA_WET_AND_DRY:
    delete atmDelayModel_p;
    atmDelayModel_p = new ALMAWetAtmosphereModel();
    output << "Changing atmospheric delay model to type \"ALMA_WET_AND_DRY\"";
    break;
  default:
    ControlExceptions::IllegalParameterErrorExImpl ex(__FILE__,__LINE__,
                                                      __func__);
    ex.log();
    throw ex.getIllegalParameterErrorEx();
  }
  regenerateDelayEvent();
  guard.release();
  LOG_TO_OPERATOR(LM_INFO,output.str().c_str()); 
}
 

AtmosphericDelayModelType DelayServerBaseImpl::
getAtmosphericDelayModelType(){
  return atmDelayModel_p->modelType;
}

Control::DelayServerBase::AntennaDelayModelSeq*
 DelayServerBaseImpl::getCurrentAtmosphericDelayModel(){
  ACS_TRACE(__func__);

  ACS::Time currentTime = ::getTimeStamp();
  Control::DelayServerBase::AntennaDelayModelSeq_var returnSeq
    = new Control::DelayServerBase::AntennaDelayModelSeq();
  returnSeq->length(antennaData_m.size());
  unsigned int idx = 0;

  std::map<std::string, DelayServerData>::iterator iter =
    antennaData_m.begin();
  
  while (iter != antennaData_m.end()) {
    try {
      DelayServerData::AtmosphericModel dsModel = 
        iter->second.getAtmosphericModel(currentTime);

      returnSeq[idx].antennaId = CORBA::string_dup(iter->first.c_str());
      returnSeq[idx].zenithDryDelay = dsModel.zenithDryDelay;
      returnSeq[idx].zenithWetDelay = dsModel.zenithWetDelay;
      idx++;
    } catch (ControlExceptions::InvalidRequestExImpl& ex) {
      ostringstream output;
      output << "Unable to get atmospheric model for " 
             << iter->first << " for the current time.";
      LOG_TO_OPERATOR(LM_ERROR, output.str().c_str());
    }
    iter++;
  }
  returnSeq->length(idx);
  return returnSeq._retn();
}

void DelayServerBaseImpl::setAtmosphericDelayModel(ACS::Time applicationTime,
                 const Control::DelayServerBase::AntennaDelayModelSeq& model){
  ACS_TRACE(__func__);
  ostringstream message;
  message << "New atmospheric model set to take effect at  "
          << applicationTime << ": " << std::endl;
  ACS::ThreadSyncGuard  guard(delayTableMutex_p, true);


  for (unsigned int idx = 0; idx < model.length(); idx++) {
    try{
      std::map<std::string, DelayServerData>::iterator iter = 
        findAntenna(model[idx].antennaId);
      DelayServerData::AtmosphericModel dsModel = {applicationTime,
                                                   model[idx].zenithDryDelay,
                                                   model[idx].zenithWetDelay};
      iter->second.addAtmosphericModel(dsModel); 
      message << "\tAntenna " << model[idx].antennaId
              << " Zenith Dry Delay: " << model[idx].zenithDryDelay
              << " Zenith Wet Delay: " << model[idx].zenithWetDelay 
              << std::endl;
    }catch (ControlExceptions::IllegalParameterErrorExImpl& ex) {
      /* Antenna is not in the list. Log the error but go on */
      ostringstream output;
      output << "Unable to set atmospheric model for " 
             << model[idx].antennaId 
             << " because antenna is not defined in delay server.";
      LOG_TO_OPERATOR(LM_ERROR, output.str().c_str());
    }
  }

  if (applicationTime < lastEventStopTime_m.value &&
      (atmDelayModel_p->modelType == ALMA_DRY_ONLY ||
       atmDelayModel_p->modelType == ALMA_WET_AND_DRY)) {
    regenerateDelayEvent();
  }
  LOG_TO_OPERATOR(LM_INFO, message.str().c_str());
}

void DelayServerBaseImpl::
setAtmosphericDelayModelASDM(ACS::Time applicationTime,
                             const asdmIDL::CalWVRTableIDL& atmModel){
  ACS_TRACE(__func__);
  asdm::ASDM inputModel;
  asdm::CalWVRTable& wvrTable = inputModel.getCalWVR();
  wvrTable.fromIDL(atmModel);
  
  vector<asdm::CalWVRRow*> row = wvrTable.get();
  Control::DelayServerBase::AntennaDelayModelSeq settingSeq;
  settingSeq.length(wvrTable.size());
  
  for (unsigned int idx = 0; idx < settingSeq.length(); idx++) {
    settingSeq[idx].antennaId =
      CORBA::string_dup(row[idx]->getAntennaName().c_str());
    settingSeq[idx].zenithDryDelay =evaluatePolynomial(row[idx]->getDryPath());
    settingSeq[idx].zenithWetDelay =evaluatePolynomial(row[idx]->getWetPath());
  }
  setAtmosphericDelayModel(applicationTime, settingSeq);
}

void DelayServerBaseImpl::newSourceAdded(ACS::Time newSourceStartTime) {
  ACS_TRACE(__func__);
  
  ACS::ThreadSyncGuard  guard(delayTableMutex_p, true);

  if (newSourceStartTime < lastEventStopTime_m.value ) {
    /* The new source replaces something already sent.  Send out delays
     * again.  NAOJ required that events be on 48 ms boundaries so force
     * ths new time to be a TE boundary.
     */
    acstime::Epoch startTimeEpoch;
    startTimeEpoch.value = newSourceStartTime;
    lastEventStopTime_m.value = 
      TETimeUtil::floorTE(startTimeEpoch).value;

    guard.release();
    generateDelayEvent();
  }

}


std::map<std::string, DelayServerData>::iterator
DelayServerBaseImpl::findAntenna(const char* antennaId){
  std::map<std::string, DelayServerData>::iterator iter = 
    antennaData_m.find(antennaId);

  if (iter == antennaData_m.end()) {
    /* Antenna was not found */
    ControlExceptions::IllegalParameterErrorExImpl ex(__FILE__, __LINE__, 
                                                      __func__);
    std::ostringstream msg;
    msg << "The antenna " << antennaId 
        << " was not specified in the delay server.";
    ex.addData("Cause", msg.str().c_str());
    throw ex;
  }

  return iter;
}
    

void DelayServerBaseImpl::allocateDelayTable(){
  ACS_TRACE(__func__);

  std::map<std::string, DelayServerData>::iterator iter;
  for (iter = antennaData_m.begin(); iter != antennaData_m.end(); iter++){
    iter->second.setPolynomialOrder(polynomialOrder_m);
  }
  delayTime_m.resize(polynomialOrder_m+1);
}

void DelayServerBaseImpl::printDelayTable(){

  std::ostringstream output;
  output << "Delay Table Output: \n"
         << antennaData_m.size() << " Antennas\n"
         << polynomialOrder_m + 1 << " Delay Values\n";

  std::map<std::string, DelayServerData>::iterator iter;
  for (iter = antennaData_m.begin(); iter != antennaData_m.end(); iter++) {
    output << iter->first << "\t";
    for (unsigned int idx = 0; idx < polynomialOrder_m+1; idx++){
      output << iter->second.geometricDelay[idx] << "\t";
    }
    output << "\n";
  }
  ACS_LOG(LM_SOURCE_INFO, __func__, (LM_DEBUG,output.str().c_str()));
}

void DelayServerBaseImpl::checkForSingularResults(){

  std::map<std::string, DelayServerData>::iterator iter;
  for (iter = antennaData_m.begin(); iter != antennaData_m.end(); iter++) {
    bool singular=true;
    for (unsigned int idx = 0; idx < polynomialOrder_m + 1; idx++) {
      if (iter->second.geometricDelay[idx] < 1E20 &&
	  iter->second.geometricDelay[idx] > -1E20) {
	singular = false;
      }
    }
    if (singular) {
      for (unsigned int idx = 0; idx < polynomialOrder_m + 1; idx++) {
	iter->second.geometricDelay[idx] = 0;
      }
      std::ostringstream output;
      output << "Singular Value detected for Antenna: " 
	     << iter->first << " delay value set to zero.";
      ACS_LOG(LM_SOURCE_INFO, __func__,(LM_WARNING, output.str().c_str()));
    }
  }
}

void DelayServerBaseImpl::regenerateDelayEvent() {
  ACS_TRACE(__func__);
  
  ACS::ThreadSyncGuard  guard(delayTableMutex_p, true);
  lastEventStopTime_m.value = 
    TETimeUtil::floorTE(TETimeUtil::unix2epoch(time(0))).value;
  generateDelayEvent();
}

void DelayServerBaseImpl::generateDelayEvent() {
  ACS_TRACE(__func__);

  while ( static_cast<long long>(lastEventStopTime_m.value - 
				 TETimeUtil::unix2epoch(time(0)).value)
	  < 2 * delayEventDuration_m ) {
    ACS::ThreadSyncGuard           guard(delayTableMutex_p, true);

    publishSingleDelayEvent();
    eventSequenceNumber_m++;
    if (eventSequenceNumber_m < 0) {
      eventSequenceNumber_m = 0;
    }
  }
}

void DelayServerBaseImpl::publishSingleDelayEvent() {
  ACS_TRACE(__func__);

  ACS::Time startTime = lastEventStopTime_m.value;
  lastEventStopTime_m.value += delayEventDuration_m;

  /* Fill out the header information for the array delay event */
  Control::ArrayDelayEvent arrayDelayEvent = createArrayDelayEvent(startTime);

  /* Now populate the time structure for the vaid data range */
  for (unsigned int idx = 0; idx <= polynomialOrder_m; idx++) {
    delayTime_m[idx] = startTime + 
      idx * delayEventDuration_m / polynomialOrder_m;
  }

  /* Fill the weather data, the pressure is used by CALC to do the Atmosphere
     so this must be dome before the call to fillDelayTable.
  */
  getWeatherData();

  ACS::Time delayStartTime = startTime;
  ACS::Time delayStopTime  = startTime;
    
  while (delayStartTime < lastEventStopTime_m.value) {
    try {
      delayStopTime = fillDelayTable(delayStartTime);
      updateAlarm(ErrEmptySourceList, false);
      updateAlarm(ErrCommunicationFailure, false);
    } catch (DelayServerExceptions::EmptySourceListExImpl& ex) {
      updateAlarm(ErrEmptySourceList, true);
      return;
    } catch (DelayServerExceptions::SetSourceErrorExImpl& ex) {
      ACS_LOG(LM_SOURCE_INFO, __func__,
              (LM_WARNING, "Failed setting a new source at the delay server"));
      updateAlarm(ErrCommunicationFailure, true);
      return;
    } catch (DelayServerExceptions::GetDelayTableErrorExImpl& ex) {
      ACS_LOG(LM_SOURCE_INFO, __func__,
              (LM_WARNING, "Failed getting delay table."));
      updateAlarm(ErrCommunicationFailure, true);
      return;
    } catch (ACSErr::ACSbaseExImpl &ex){
      // ACS error exception. Log the error detected
      ex.log(LM_WARNING);
      updateAlarm(ErrCommunicationFailure, true);
      return;
    } catch (const CORBA::Exception& ex) {
      ACS_LOG(LM_SOURCE_INFO, __func__,
              (LM_WARNING, 
               "Failed getting delay table. CORBA exception caught."));
      updateAlarm(ErrCommunicationFailure, true);
      return;
    } catch (...) {
      // Unexpected exception catched.
      ACS_LOG(LM_SOURCE_INFO, __func__,
              (LM_WARNING, 
               "Failed getting delay table. Unexpected exception caught."));
      updateAlarm(ErrCommunicationFailure, true);
      return;
    }
    
    /* Calculate the Atmospheric Delays */
    if (atmDelayModel_p != NULL) {
      updateAlarm(ErrNoAtmosphereModel, false);
      if (!atmDelayModel_p->calculateAtmosphericModel(antennaData_m)) {
        /* Error Applying Delay Values */
        LOG_TO_OPERATOR(LM_ERROR, 
                        "Error applying atmospheric delay model");
        updateAlarm(ErrAtmosphereModel, true);
      } else {
        updateAlarm(ErrAtmosphereModel, false);
      }
    } else {
      /* No Delay defined */
      LOG_TO_OPERATOR(LM_WARNING, 
                      "No atmospheric delay model has been defined");
      updateAlarm(ErrNoAtmosphereModel, true);
    }
    
    /* Now calculate the total delay based on the values 
       we have populated inside the DelayServerData Structures
    */
    std::map<std::string, DelayServerData>::iterator iter;
    for (iter = antennaData_m.begin(); iter != antennaData_m.end(); iter++){
      iter->second.calculateTotalDelay();
    }

    /* The Total Delays are now correct, populate the event with 
       the values and the valid time */
    populateTotalDelayEvent(arrayDelayEvent, delayStartTime, delayStopTime);

    populateQuantizedDelays(arrayDelayEvent, startTime,
                            delayStartTime, delayStopTime);

    delayStartTime = delayStopTime;
  }

  publishDelayEvent(arrayDelayEvent);
  std::ostringstream output;
  output << "Delay event " << eventSequenceNumber_m << " published: "
         << "\n\tPublish Time: " <<  TETimeUtil::unix2epoch(time(0)).value
         << "\n\tEvent Start Time: " << startTime
         << "\tEnd Time: " << lastEventStopTime_m.value
         << std::endl;
  ACS_LOG(LM_SOURCE_INFO, __func__, (LM_DEBUG,output.str().c_str()));
}

Control::ArrayDelayEvent 
DelayServerBaseImpl::createArrayDelayEvent(ACS::Time startTime) {
  ACS_TRACE(__func__);
  /* Fill out the header information for the array delay event */
  Control::ArrayDelayEvent       arrayDelayEvent;

  arrayDelayEvent.arrayName = CORBA::string_dup(arrayName_m.c_str());
  arrayDelayEvent.sequenceNumber = eventSequenceNumber_m;

  /* Allocate space for all of the antenna events */
  arrayDelayEvent.antennaDelayEvents.length(antennaData_m.size());

  std::map<std::string, DelayServerData>::iterator iter;
  unsigned int antIdx = 0;

  for (iter = antennaData_m.begin(); iter != antennaData_m.end(); iter++) {
    Control::AntennaDelayEvent  antennaDelayEvent;
    antennaDelayEvent.startTime   = startTime;
    antennaDelayEvent.stopTime    = lastEventStopTime_m.value;
    antennaDelayEvent.delayTables.length(0);
    antennaDelayEvent.coarseDelays.length(0);
    antennaDelayEvent.fineDelays.length(0);
    antennaDelayEvent.antennaName =
      CORBA::string_dup(iter->second.antennaName.c_str());

    /* Add the antenna event to the array event */
    arrayDelayEvent.antennaDelayEvents[antIdx] = antennaDelayEvent;
    antIdx++;
  }
  return arrayDelayEvent;
}

/* This method copies the Delay Table into the TotalDelayEvent portion
   of the ArrayDelayEvent */
void DelayServerBaseImpl::
populateTotalDelayEvent(Control::ArrayDelayEvent& arrayDelayEvent,
                        ACS::Time delayStartTime, 
                        ACS::Time delayStopTime) {

  std::map<std::string, DelayServerData>::iterator iter;

  for (unsigned int antIdx = 0; antIdx < antennaData_m.size(); antIdx++) {
    /* Create a space for the data in the delayTables and put int the
       range of validity */
    int delayTableIdx = arrayDelayEvent.antennaDelayEvents[antIdx].
      delayTables.length();
    arrayDelayEvent.antennaDelayEvents[antIdx].
      delayTables.length(delayTableIdx + 1);
    arrayDelayEvent.antennaDelayEvents[antIdx].
      delayTables[delayTableIdx].startTime = delayStartTime;
    arrayDelayEvent.antennaDelayEvents[antIdx].
      delayTables[delayTableIdx].stopTime = delayStopTime;

    /* Find the iterator for this antenna */
    iter = antennaData_m.find(&*arrayDelayEvent.antennaDelayEvents[antIdx].                              antennaName);

    /* Now put in the actual data */
    arrayDelayEvent.antennaDelayEvents[antIdx].
      delayTables[delayTableIdx].delayValues.length(polynomialOrder_m +1);

    for (unsigned int idx = 0; idx <= polynomialOrder_m; idx++) {
      arrayDelayEvent.antennaDelayEvents[antIdx].
        delayTables[delayTableIdx].delayValues[idx].time =delayTime_m[idx];
      arrayDelayEvent.antennaDelayEvents[antIdx].
        delayTables[delayTableIdx].delayValues[idx].totalDelay
        = iter->second.totalDelay[idx];
    }
  }
}

/* This method is the one that actually finds and fills the Coarse and
   Fine delay values.
*/

void DelayServerBaseImpl::
populateQuantizedDelays(Control::ArrayDelayEvent& arrayDelayEvent,
                        ACS::Time startTime,
                        ACS::Time delayStartTime, 
                        ACS::Time delayStopTime) {
  const double FINE_DELAY_SIZE(15.625E-12);
  const int    COARSE_DELAY_FACTOR(16);

  const double RoundingFactor(FINE_DELAY_SIZE/2.0);

  double delayCorrectionTable[polynomialOrder_m + 1];
  std::map<std::string, DelayServerData>::iterator iter;

  for (unsigned int antIdx = 0; antIdx < antennaData_m.size(); antIdx++) {
    /* Change the delays to delayCorrections and store the result for
       the interpolator */
    iter = antennaData_m.find(&*arrayDelayEvent.antennaDelayEvents[antIdx].
                              antennaName);
    for (unsigned int idx = 0; idx <= polynomialOrder_m; idx++ ) {
      delayCorrectionTable[idx] = causalityDelay_m - 
        iter->second.totalDelay[idx];
    }
    
    PolynomialInterpolator polyInt(polynomialOrder_m, &delayTime_m[0],
                                   delayCorrectionTable);
    
    /* For each of the defined times convert the delay into the NEAREST
       number of fine delay steps.  The evalTime is in absolute
       ACS::Time units, and is set to the middle of the ms boundaries
    */

    ACS::Time evalTime = delayStartTime+5000;
    DelayPair lastPair;
    std::queue<DelayPair> delayList;

    /* Get the first value */
    lastPair.relativeStartTime = evalTime - startTime;
    lastPair.fineSampleDelay = static_cast<unsigned long>
      ((polyInt.getDelay(evalTime) + RoundingFactor) / FINE_DELAY_SIZE);

    unsigned long currentDelay;
    while (evalTime < delayStopTime) {
      currentDelay = static_cast<unsigned long>
        ((polyInt.getDelay(evalTime) + RoundingFactor) / FINE_DELAY_SIZE);

      if (currentDelay != lastPair.fineSampleDelay) {
        delayList.push(lastPair);
        lastPair.fineSampleDelay = currentDelay;
        lastPair.relativeStartTime = evalTime - startTime;
      }
      evalTime += 10000; // Increment by 1 ms
    }
    delayList.push(lastPair);

    /* The Memory Allocation Strategy here is make both lists sufficently
       long to hold an additional delayList.size() elements.  Then reallocate
       it back to the corect size at the end */
    long fdIdx = 
      arrayDelayEvent.antennaDelayEvents[antIdx].fineDelays.length();
    long cdIdx =
      arrayDelayEvent.antennaDelayEvents[antIdx].coarseDelays.length();

    arrayDelayEvent.antennaDelayEvents[antIdx].fineDelays.
      length(fdIdx + delayList.size());
    arrayDelayEvent.antennaDelayEvents[antIdx].coarseDelays.
      length(cdIdx + delayList.size());

    /* Now transfer these into the event */
    Control::FineDelayValue   fdv;
    Control::CoarseDelayValue cdv;
    cdv.coarseDelay = 0;
    cdv.relativeStartTime = 0;
    bool firstTime = true;

    while (!delayList.empty()) {
      DelayPair dp = delayList.front();
      fdv.relativeStartTime = dp.relativeStartTime/ACS_TIME_TO_MILLISEC;
      fdv.fineDelay         = dp.fineSampleDelay % COARSE_DELAY_FACTOR;
      arrayDelayEvent.antennaDelayEvents[antIdx].fineDelays[fdIdx] = fdv;
      fdIdx++;

      if (dp.fineSampleDelay / COARSE_DELAY_FACTOR != cdv.coarseDelay ||
          firstTime) {
        cdv.coarseDelay       = dp.fineSampleDelay / COARSE_DELAY_FACTOR;
        cdv.relativeStartTime = dp.relativeStartTime/ACS_TIME_TO_MILLISEC;

        if (cdIdx > 0 && cdv.relativeStartTime/ 48 == 
            arrayDelayEvent.antennaDelayEvents[antIdx].coarseDelays[cdIdx-1].
            relativeStartTime / 48) {
          /* This is the case for 2 coarse delay events in the same
             TE. */
          arrayDelayEvent.antennaDelayEvents[antIdx].coarseDelays[cdIdx-1]=cdv;
        } else {
          arrayDelayEvent.antennaDelayEvents[antIdx].coarseDelays[cdIdx]=cdv;
          cdIdx++;
        }
        firstTime = false;
      }
      delayList.pop();
    }
    arrayDelayEvent.antennaDelayEvents[antIdx].fineDelays.length(fdIdx);
    arrayDelayEvent.antennaDelayEvents[antIdx].coarseDelays.length(cdIdx);
  }
}

double DelayServerBaseImpl::evaluatePolynomial(const vector<float> poly){
  double returnValue = 0.0;
  for (unsigned int idx = 0; idx < poly.size(); idx+=2) {
    if ((idx % 4) == 0) {
      // These are terms 0, 4, 8,...
      returnValue += poly[idx];
    } else {
      //These are terms 2, 6, 10,...
      returnValue -= poly[idx];
    }
  }
  return returnValue;
}
/*===========================================================*/

DelayServerBaseImpl::ServiceLoop::ServiceLoop(const ACE_CString& name,
                                          DelayServerBaseImpl& delayServer):
  ACS::Thread(name),
  delayServer_m(delayServer)
{
  ACS_TRACE(__func__);
}

DelayServerBaseImpl::ServiceLoop::~ServiceLoop() {
  ACS_TRACE(__func__);
}

void DelayServerBaseImpl::ServiceLoop::runLoop(){
  ACS_TRACE(__func__);
  delayServer_m.generateDelayEvent();
}


std::vector<AlarmInformation> DelayServerBaseImpl::createAlarmVector() 
{
    std::vector<AlarmInformation> aivector;
    {
        AlarmInformation ai;
        ai.alarmCode = ErrEmptySourceList;
        ai.alarmTerminateCount = 2;
        ai.alarmDescription = "No source defined in delay server";
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = ErrCommunicationFailure;
        ai.alarmTerminateCount = 4;
        ai.alarmDescription = "Error communicating with the CALC process";
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = ErrNoAtmosphereModel;
        ai.alarmTerminateCount = 1;
        ai.alarmDescription = "No Atmospheric model defined";
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = ErrAtmosphereModel;
        ai.alarmTerminateCount = 2;
        ai.alarmDescription = "Error in evaluating the atmospheric delay model";
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = ErrWeatherData;
        ai.alarmTerminateCount = 2;
        ai.alarmDescription = "Error in weather data";
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = ErrPublication;
        ai.alarmTerminateCount = 2;
        ai.alarmDescription = "Error publishing delay event";
        aivector.push_back(ai);
    }
    return aivector;
}
/*___oOo___*/
