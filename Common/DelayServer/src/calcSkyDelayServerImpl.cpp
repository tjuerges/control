// "@(#) $Id$"
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007 - 2010
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#include <calcSkyDelayServerImpl.h>
#include <calcCommManager.h>
#include <TrackableObject.h>
#include <PlanetaryObject.h>
#include <EquatorialObject.h>
#include <EphemerisObject.h>
#include <OffsetStroke.h>
#include <SourceAlarmTypes.h>
#include <controlAlarmSender.h>
#include <ControlExceptions.h>
#include <EphemerisInterpolator.h>
#include <TETimeUtil.h>
#include <TmcdbErrType.h>
#include <LogToAudience.h>
#include <TMCDBAccessIFC.h> // for TMCDB::Access
#include <TMCDBComponentC.h> // for TMCDB::AntennaDelays

#include <slalib.h> // for sla* functions
#include <casacore/casa/Quanta/Euler.h> 
#include <casacore/casa/Quanta/RotMatrix.h>
#include <casacore/measures/Measures/MPosition.h>
#include <casacore/casa/Quanta/MVPosition.h>
#include <casacore/casa/Quanta/Quantum.h>
#include <casacore/casa/Quanta/MVAngle.h> // for casa::MVAngle
#include <casacore/measures/Measures/MDirection.h> // for casa::MDirection

#include <boost/shared_ptr.hpp> // for boost::shared_ptr
#include <ostream>
#include <string>
#include <vector>
#include <cmath>

using TmcdbErrType::TmcdbConnectionFailureExImpl;
using ControlExceptions::IllegalParameterErrorExImpl;
using Control::EphemerisInterpolator;
using Control::DelaySource;
using Control::TrackableObject;
using Control::EquatorialObject;
using Control::PlanetaryObject;
using Control::EphemerisObject;

using casa::MVAngle;

using boost::shared_ptr;
using std::ostringstream;
using std::vector;
using std::abs;
using std::cos;
using std::string;
using std::list;
CalcSkyDelayServerImpl::CalcSkyDelayServerImpl():
    DelayServerBaseImpl(),
    commManager_p(NULL),
    refPosition_m(3, 0.0)
{  
    sourceList_m.clear();
}

CalcSkyDelayServerImpl::~CalcSkyDelayServerImpl() {
  const string fnName = "CalcSkyDelayServerImpl::~CalcSkyDelayServerImpl";
  ACS_TRACE(fnName);
  
  if (commManager_p != NULL) {
    delete commManager_p;
    commManager_p = NULL;
  }
}

void CalcSkyDelayServerImpl::initializeServer(const char* arrayId){
  const string fnName = "CalcSkyDelayServerImpl::initializeServer";
  ACS_TRACE(fnName);
  
  try {
    commManager_p = new CalcCommManager(); 
    updateAlarm(ErrCommunicationFailure, false);
  } catch (DelayServerExceptions::ReinitializationErrorExImpl& ex) {
    DelayServerExceptions::InitializeServerErrorExImpl 
      newEx(ex, __FILE__,__LINE__,fnName.c_str());
    newEx.addData("Detail","Failed creating communication manager");
    newEx.log();
    updateAlarm(ErrCommunicationFailure, true);
    throw newEx.getInitializeServerErrorEx();
  }
 
  DelayServerBaseImpl::initializeServer(arrayId);
}

void CalcSkyDelayServerImpl::setPolynomialOrder(CORBA::Long polynomialOrder){
  const string fnName = "CalcSkyDelayServerImpl::setPolynomialOrder";
  ACS_TRACE(fnName);

  // Tell the Communication Manager to initialize the polynomial
  try {
    commManager_p->
    setPolynomialOrder(static_cast<unsigned int>(polynomialOrder));
    updateAlarm(ErrCommunicationFailure, false);
  } catch (DelayServerExceptions::ReinitializationErrorExImpl& ex) {
    DelayServerExceptions::SetPolynomialOrderErrorExImpl newEx(ex, __FILE__,__LINE__,
                                                               fnName.c_str());
    newEx.addData("Detail","Failed setting polynomial order at communication manager"); 
    newEx.log();
    updateAlarm(ErrCommunicationFailure, true);
    throw newEx.getSetPolynomialOrderErrorEx();
  }

  DelayServerBaseImpl::setPolynomialOrder(polynomialOrder);
}


void CalcSkyDelayServerImpl::setReference(double xPosition,
                                          double yPosition,
                                          double zPosition){
    // The reference location is in the ITRF frame. Antenna name is not
    // required on this method.
    CalcAntenna_t refLocation = {"", xPosition, yPosition, zPosition, 0};

    std::ostringstream output;
    output << "Setting Delay Reference Location: "
           << " X: " << refLocation.x << "  "
           << " Y: " << refLocation.y << "  "
           << " Z: " << refLocation.z << " meters from centre of the earth";

    LOG_TO_SCILOG(LM_INFO, output.str().c_str());
    try {
        commManager_p->setReference(refLocation);
        updateAlarm(ErrCommunicationFailure, false);
    } catch (DelayServerExceptions::ReinitializationErrorExImpl& ex) {
        DelayServerExceptions::SetReferenceErrorExImpl 
            newEx(ex, __FILE__,__LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail","Failed setting an antenna reference at communication manager"); 
        newEx.log();
        updateAlarm(ErrCommunicationFailure, true);
        throw newEx.getSetReferenceErrorEx();
    }

    refPosition_m[0] = xPosition;
    refPosition_m[1] = yPosition;
    refPosition_m[2] = zPosition;

    regenerateDelayEvent();
}

void CalcSkyDelayServerImpl::addSource(const DelaySource& newSource){
    list<DelaySource>::iterator iter= sourceList_m.begin();
    
    const ACS::Time startTime = newSource.sourceStartTime;
    while (iter != sourceList_m.end() && 
           iter->sourceStartTime < startTime) {
        ++iter;
    }
    sourceList_m.insert(iter, newSource);
    newSourceAdded(startTime);
}

void CalcSkyDelayServerImpl::addSources(const Control::DelaySourceSeq& newSources){

    if (newSources.length() == 0) return;
    
    list<DelaySource>::iterator currentSource = sourceList_m.begin();

    for (unsigned int s = 0; s < newSources.length(); s++) {
        ACS::Time startTime = newSources[s].sourceStartTime;
        while (currentSource != sourceList_m.end() && 
               currentSource->sourceStartTime < startTime) {
            ++currentSource;
        }
        sourceList_m.insert(currentSource, newSources[s]);
    }
    const ACS::Time firstStartTime = newSources[0].sourceStartTime;
    newSourceAdded(firstStartTime);
}

void CalcSkyDelayServerImpl::getWeatherData() {
  std::map<std::string, DelayServerData>::iterator iter;
    
  if (CORBA::is_nil(&*ws_m)){
    //if (ws_m.isNil()) {
    /* No weather station use default values */
    for (iter = antennaData_m.begin(); iter != antennaData_m.end(); iter++) {
      // At the OSF we have about 75% of the atmosphere above us, 
      // use 53% for the AOS
      iter->second.pressure = 101325 * 0.75; 
    }
    updateAlarm(ErrWeatherData, true);
  } else {
    for (iter = antennaData_m.begin(); iter != antennaData_m.end(); iter++) {
      Control::CurrentWeather::Pressure pressure;
      try {
        pressure = ws_m->getPressureAtPad(iter->second.padName.c_str());
      } catch (ControlExceptions::IllegalParameterErrorEx& ex) {
        ostringstream msg;
        msg << "Illegal pad name detected for antenna " 
            << iter->first << " (" << iter->second.padName
            << ") please correct in the TMCDB";
        LOG_TO_OPERATOR(LM_ERROR, msg.str().c_str());
        pressure.valid = false;
      }
      /* Probably should put a time check on this as well */
      if (!pressure.valid) {
        updateAlarm(ErrWeatherData, true);
        // At the OSF we have about 75% of the atmosphere above us, 
        // use 53% for the AOS
        pressure.value = 101325 *0.75; 
      } else {
        updateAlarm(ErrWeatherData, false);
      }
      iter->second.pressure = pressure.value;
    }
  }
}

void CalcSkyDelayServerImpl::addAntenna(const char* antennaId){
  ACS_TRACE(__func__);

  DelayServerBaseImpl::addAntenna(antennaId);
  
  CalcAntenna_t antennaPosition = 
    getCalcAntenna(antennaData_m[std::string(antennaId)]);

  try {
    commManager_p->addAntenna(antennaPosition);
    updateAlarm(ErrCommunicationFailure, false);
  } catch (DelayServerExceptions::ReinitializationErrorExImpl& ex) {
    DelayServerExceptions::AddAntennaErrorExImpl 
      newEx(ex, __FILE__,__LINE__,__func__);
    newEx.addData("Detail",
                  "Failed adding antenna at the communication manager"); 
    newEx.log();
    updateAlarm(ErrCommunicationFailure, true);
    throw newEx.getAddAntennaErrorEx();
  }   
}

DelayServerData CalcSkyDelayServerImpl::getAntennaData(const char* antennaId){
  ACS_TRACE(__func__);

  if (tmcdb_m.isNil()) {
    /* Unable to add antenna, TMCDB unavailable */
    TmcdbConnectionFailureExImpl ex(__FILE__, __LINE__, __func__);
    ex.addData("ErrorMessage", 
               "Unable to add antenna, connection to TMCDB unavailable");
    ex.log();
    throw ex.getTmcdbConnectionFailureEx();
  }

  double antennaDelay = 0;
  double padDelay = 0;
  TMCDB_IDL::AntennaIDL_var ai;
  TMCDB_IDL::PadIDL_var pi;

  try {
    ai = tmcdb_m->getAntennaInfo(antennaId);
    pi = tmcdb_m->getCurrentAntennaPadInfo(antennaId);
    TMCDB::AntennaDelays_var delays = tmcdb_m->getCurrentAntennaDelays(antennaId);
    antennaDelay = delays->antennaDelay;
    padDelay = delays->padDelay;
  } catch (TmcdbErrType::TmcdbNoSuchRowEx& ex) {
    string msg = "Cannot retrieve the antenna location from the database.";
    TmcdbErrType::TmcdbErrorExImpl nEx(ex, __FILE__, __LINE__, __func__);
    nEx.addData("Detail", msg);
    nEx.log();
    throw nEx.getTmcdbErrorEx();
  }

  DelayServerData newAntenna;
  newAntenna.antennaName = antennaId;
  newAntenna.setPolynomialOrder(polynomialOrder_m);

  /* Populate the information from the Pad Info */
  newAntenna.padName = pi->PadName;
  newAntenna.padVector[0] = pi->XPosition.value;
  newAntenna.padVector[1] = pi->YPosition.value;
  newAntenna.padVector[2] = pi->ZPosition.value;

  /* Now populate the information from the Antenna Info */
  newAntenna.antennaVector[0] = ai->XPosition.value;
  newAntenna.antennaVector[1] = ai->YPosition.value;
  newAntenna.antennaVector[2] = ai->ZPosition.value;

  newAntenna.offsetVector[0] = ai->XOffset.value;
  newAntenna.offsetVector[1] = ai->YOffset.value;
  newAntenna.offsetVector[2] = ai->ZOffset.value;

  newAntenna.antennaCableDelay = antennaDelay + padDelay;

  return newAntenna;
}


CalcAntenna_t CalcSkyDelayServerImpl::
getCalcAntenna(const DelayServerData& antData) {
  ACS_TRACE(__func__);

  /* Note this methods conversions is based on the addPosition method in
     MountControllerImpl.  Any corrections should be made in both places
  */

  /* Here we need to change the co-ordinates of the Antenna into a
     calcAntenna_t.  We assume that the padPosition is in the ITRF 
     frame.
  */
  casa::MVPosition padLocation(antData.padVector[0],
                               antData.padVector[1],
                               antData.padVector[2]);

  double padLat(padLocation.getLat("rad").getValue());
  double padLong(padLocation.getLong("rad").getValue());

  casa::MVPosition antennaVect(-1*antData.antennaVector[0],
                               -1*antData.antennaVector[1],
                               antData.antennaVector[2]);
  antennaVect *= casa::RotMatrix(casa::Euler(M_PI_2 - padLat,
                                             0.0,
                                             M_PI_2 - padLong));

  // Now that both the pad and the antenna are in the same frame we can add
  // them
  antennaVect += padLocation;
  

  /* Note by convention we assume that the xOffset is the only interesting
     term for the Delay calculation.  Thus in the rotating frame of the 
     az platform, X is the axis non-intesection in the dirction of the source
  */
  
  
  CalcAntenna_t antennaPosition;
  strncpy(antennaPosition.antennaName, antData.antennaName.c_str(), 4);
  antennaPosition.x = antennaVect.getValue()[0];
  antennaPosition.y = antennaVect.getValue()[1];
  antennaPosition.z = antennaVect.getValue()[2];
  antennaPosition.axisOffset = antData.offsetVector[0];
  return antennaPosition;
}

ACS::Time CalcSkyDelayServerImpl::fillDelayTable(ACS::Time startTime) {
  
    if (sourceList_m.empty()) {
        updateAlarm(ErrEmptySourceList,true);
        throw DelayServerExceptions::
            EmptySourceListExImpl(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    } 

    // remove, from sourceList_m, all the sources that are no longer needed and
    // find the first source in this trimmed list.
    std::list<DelaySource>::iterator source = sourceList_m.end();
    source--;
    while (source != sourceList_m.begin()  &&
           startTime < source->sourceStartTime) {
        source--;
    }
    if (source != sourceList_m.begin()) {
        sourceList_m.erase(sourceList_m.begin(), source);
    }  

    if (delayTime_m[polynomialOrder_m] < source->sourceStartTime) {
        // The source list is not empty, but the first one does not start
        // during this delay event.  The list is effectivly empty.
        updateAlarm(ErrEmptySourceList,true);
        throw DelayServerExceptions::
            EmptySourceListExImpl(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    }
    updateAlarm(ErrEmptySourceList, false);

    vector<CalcSource_t> sourceList = delayToCalcSource(*source, delayTime_m);
    
    // Send the source vector to the Communication Manager
    try {
        commManager_p->setSource(sourceList);
        updateAlarm(ErrCommunicationFailure,false);
    } catch (DelayServerExceptions::ReinitializationErrorExImpl& ex) {
        updateAlarm(ErrCommunicationFailure,true);
        throw DelayServerExceptions::SetSourceErrorExImpl(ex,__FILE__,__LINE__,
                                                          __func__);
    }  

    /* Now populate the results fields of the calc data structures*/
    try {
        commManager_p->updatePressureData(antennaData_m);
        commManager_p->getDelayTable(&delayTime_m[0], antennaData_m);
        updateAlarm(ErrCommunicationFailure,false);
    } catch (DelayServerExceptions::ReinitializationErrorExImpl& ex) {
        updateAlarm(ErrCommunicationFailure, true);
        throw DelayServerExceptions::GetDelayTableErrorExImpl(ex, __FILE__,
                                                              __LINE__,__func__);
    }   
  
    checkForSingularResults();

    /* The returned delays are valid until either the next source becomes valid
       or we reach the end of the list of times. */
    source++;
    if (source != sourceList_m.end()) {
      if (source->sourceStartTime < delayTime_m[polynomialOrder_m]) {
        return source->sourceStartTime;
      }
    }
  
    return delayTime_m[polynomialOrder_m];
}

vector<CalcSource_t> CalcSkyDelayServerImpl::
delayToCalcSource(const DelaySource& source, const vector<ACS::Time>& times) {
    ostringstream msg;

    shared_ptr<TrackableObject> star(delayToTrackableObject(source));
    const double milliArcsec = M_PI/180/60/60/1000;
    double longOffset = source.offset.longitudeOffset;
    double latOffset = source.offset.latitudeOffset;
    if (abs(longOffset) > milliArcsec || abs(latOffset) > milliArcsec) {
        shared_ptr<Control::Stroke> 
            offset(new Control::OffsetStroke(longOffset, latOffset));
        if (source.offset.frame == Control::HORIZON) {
            star->horizonPattern().insert(offset);
            msg << " Horizon";
        } else if (source.offset.frame == Control::EQUATORIAL) {
            star->equatorialPattern().insert(offset);
            msg << " Equatorial";
        } else if (source.offset.frame == Control::GALACTIC) {
            longOffset = latOffset = 0.0;
            string warning =  "Offsets in Galactic coordinates are not yet supported";
            warning += " - Ignoring the offset.";
            LOG_TO_OPERATOR(LM_WARNING, warning);
            msg << " Galactic";
        }
        msg << " offset of (" << casa::Quantity(longOffset, "rad").getValue("arcsec")
            << ", " << casa::Quantity(latOffset, "rad").get("arcsec") << ") arcsec.";
     
    }
    if (abs( source.offset.longitudeVelocity) > milliArcsec || 
        abs(source.offset.latitudeVelocity) > milliArcsec) {
        string warning = "Time variable offsets cannot be supported in the delay calculations.";
        warning += " Ignoring the time variation.";
        LOG_TO_OPERATOR(LM_ERROR, warning);
    }
    
    const unsigned int numTimes = times.size();
    vector<CalcSource_t> sourceList(numTimes); 
    CalcSource_t thisSource;
    if (source.PlanetaryObject == false) {
        thisSource.dRA = source.DRA;
        thisSource.dDec = source.DDec;
        if (abs(source.Dec) < M_PI/2-M_PI/180/60/60/1000) {
            thisSource.dDec /= cos(source.Dec);
        }
        thisSource.Epoch = source.Epoch;
        thisSource.Parallax = source.Parallax;
    } else {
        thisSource.dRA = 0;
        thisSource.dDec = 0; 
        thisSource.Epoch = EpochHelper(times[0]).toUTCdate(0, 0);
        // The epoch is correct to within 60 seconds. So the error in the
        // precision will not be much.
        thisSource.Parallax = 0;  // This may be wrong
    }
    for (unsigned int i = 0; i < numTimes; i++)  {
        Control::EquatorialDirection target;
        Control::Offset equatorial;
        Control::EquatorialDirection equatorialRADec;
        Control::Offset horizon;
        Control::HorizonDirection horizonAzEl;
        Control::HorizonDirection commanded;
        Control::HorizonDirection commandedRate;
        const ACS::Time& t = times[i];
        star->getPointing(t, target, equatorial, equatorialRADec, horizon,
                         horizonAzEl, commanded, commandedRate);
        star->getActualRADec(commanded.az, commanded.el, t, 
                             thisSource.RA, thisSource.Dec);
        sourceList[i] = thisSource;

        msg << " RA/Dec at " << TETimeUtil::toTimeString(t) << " is (" 
            << casa::MVAngle(thisSource.RA).string(casa::MVAngle::TIME, 9)
            << ", " << casa::MVAngle(thisSource.Dec).string(casa::MVAngle::ANGLE, 9) << ")";
    }

    LOG_TO_DEVELOPER(LM_INFO, msg.str());
    return sourceList;
}

boost::shared_ptr<TrackableObject> 
CalcSkyDelayServerImpl::delayToTrackableObject(const DelaySource& source) {
    casa::MPosition antLocation(casa::MVPosition(refPosition_m[0],
                                                 refPosition_m[1],
                                                 refPosition_m[2]),
                                casa::MPosition::ITRF);
    boost::shared_ptr<Control::AlarmHelper> 
        alarmPtr(new Control::AlarmSender());
    {
        vector<Control::AlarmInformation> alarms(4);
        alarms[0].alarmCode = Control::NO_COMMS_TO_WS; 
        alarms[1].alarmCode = Control::WS_DISABLED;
        alarms[2].alarmCode = Control::OLD_WS_DATA;
        alarms[3].alarmCode = Control::NO_IERS_DATA;
        alarmPtr->initializeAlarms("DelayServer", "Array001", alarms);
    }
        
    ostringstream msg;
    shared_ptr<TrackableObject> toPtr;
    if (source.PlanetaryObject == false) { // its outside the solar system
        msg << "Equatorial source" << source.ObjectName 
            << " RA/Dec (" << casa::MVAngle(source.RA).string(casa::MVAngle::TIME, 9)
            << ", " << casa::MVAngle(source.Dec).string(casa::MVAngle::ANGLE, 9)
            << ") Epoch: " <<  slaEpj(source.Epoch);
        
        const double milliArcsec = M_PI/180/60/60/1000;
        if ((source.DRA > milliArcsec) || 
            (source.DDec > milliArcsec) || 
            (source.Parallax > milliArcsec)) {
            msg << " Proper motion (" << casa::Quantity(source.DRA, "rad/s").getValue("arcsec/yr")
                << ", " << casa::Quantity(source.DDec, "rad/s").getValue("arcsec/yr") << ") arcsec/yr."
                << " Parallax " << casa::Quantity(source.Parallax, "rad").get("arcsec");
        }
        
        toPtr.reset(new EquatorialObject(source.RA, source.Dec, 
                                         source.DRA, source.DDec, source.Parallax,
                                         antLocation, slaEpj(source.Epoch), 
                                         getLogger(), alarmPtr));
    } else { // Its a solar system object
        //  First convert string to upper case.
        string ucName(source.ObjectName);
        for (string::iterator p = ucName.begin(); p !=  ucName.end(); ++p) {
            *p = std::toupper(*p);
        }

        if (ucName != "EPHEMERIS") { // Its a "planet"
            msg << "Planet " << ucName;
            casa::MDirection::Types planet(PlanetaryObject::planetNameToEnum(ucName));
            toPtr.reset(new PlanetaryObject(planet, antLocation, getLogger(), alarmPtr));
        } else { // Its an "ephemeris" object
            msg << "Ephemeris ";    
            toPtr.reset(new EphemerisObject(source.positionList, antLocation, getLogger(), alarmPtr));
        }
    }
    LOG_TO_DEVELOPER(LM_INFO, msg.str());
    return toPtr;
}

/* ---------------- This should be a template -------------- */
#include <calcSkyDelayServerWrapper.h>

#include <acsncSimpleSupplier.h>

CalcSkyDelayServerWrapper::CalcSkyDelayServerWrapper(const ACE_CString& name,
                                                     maci::ContainerServices *cs):
  ACSComponentImpl(name,cs),
  CalcSkyDelayServerImpl(),
  delayPublisher_m(Control::CHANNELNAME_CONTROLREALTIME, this)
{
  const std::string fnName =
    "CalcSkyDelayServerWrapper::CalcSkyDelayServerWrapper";
  ACS_TRACE(fnName);
  
  serviceThread_p = getContainerServices()->getThreadManager()->
    create<ServiceLoop, DelayServerBaseImpl>(name+"ServiceThread", *this);
   serviceThread_p -> setSleepTime(delayEventDuration_m/2);
}


CalcSkyDelayServerWrapper::~CalcSkyDelayServerWrapper() {
  delayTableMutex_p->acquire();

  if (serviceThread_p != NULL) {
    serviceThread_p->terminate();
    serviceThread_p = NULL;
  }
}

void CalcSkyDelayServerWrapper::cleanUp() {
  const std::string fnName = "CalcSkyDelayServer::cleanUp";
  ACS_TRACE(fnName);

  ACS::ThreadSyncGuard  guard(delayTableMutex_p, true);
  if (serviceThread_p != NULL) {
    serviceThread_p->terminate();
    serviceThread_p = NULL;
  }

  ACSComponentImpl::cleanUp();
}


void CalcSkyDelayServerWrapper::initializeServer(const char* arrayId) {
  const std::string fnName = "CalcSkyDelayServerWrapper::initializeServer";
  ACS_TRACE(fnName);

  CalcSkyDelayServerImpl::initializeServer(arrayId);

  /* Here we want to get a reference to the Weather station which 
     will be used in the getWeatherData method */
  try {
    const string wsComponentName = "CONTROL/WeatherStationController";
    ws_m = getContainerServices()->
      getComponentNonStickySmartPtr<Control::CurrentWeather>
      (wsComponentName.c_str());
    updateAlarm(ErrWeatherData, false);
  } catch (maciErrType::CannotGetComponentExImpl& ex) {
    string msg = "Cannot connect to the weather station";
    msg += " - using default values.";
    msg += " Detailed error trace follows.";
    LOG_TO_OPERATOR(LM_WARNING, msg);
    ex.log(LM_WARNING);
    //ws_m.release();
    updateAlarm(ErrWeatherData, true);
  }

  try {
      tmcdb_m = getContainerServices()->getDefaultComponentSmartPtr<TMCDB::Access>
          ("IDL:alma/TMCDB/Access:1.0");
    /* Now that we have the TMCDB get the array reference location */
    TMCDB::ArrayReferenceLocation refLoc=tmcdb_m->getArrayReferenceLocation();
    setReference(refLoc.x, refLoc.y, refLoc.z);
  } catch (maciErrType::CannotGetComponentExImpl& ex) {
    maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,__func__);
    string msg = "Cannot connect to the TMCDB, this is a fatal error";
    nex.addData("Cause", msg);
    nex.log(LM_CRITICAL);
    //tmcdb_m.release();
    throw nex.getCannotGetComponentEx();
  }

  serviceThread_p->resume();
}

void CalcSkyDelayServerWrapper::
setDelayEventDuration(ACS::TimeInterval duration) {
  const std::string fnName = "CalcSkyDelayServerWrapper::setAntennaCableDelay";
  ACS_TRACE(fnName);

  CalcSkyDelayServerImpl::setDelayEventDuration(duration);
  serviceThread_p->setSleepTime(delayEventDuration_m/2);
}


void CalcSkyDelayServerWrapper::
publishDelayEvent(const Control::ArrayDelayEvent& ade) {
  const std::string fnName = "CalcSkyDelayServerWrapper::publishDelayEvent";
  try {
    delayPublisher_m.publishData<Control::ArrayDelayEvent>(ade);
    updateAlarm(ErrPublication, false);
  } catch (ACSErrTypeCommon::CORBAProblemEx& ex) {
    /* Since this is from a thread not much to do but log it and continue */
    updateAlarm(ErrPublication, true);
    ACS_LOG(LM_SOURCE_INFO,fnName.c_str(),
            (LM_WARNING,"Unable to publish delay event."));
  }
}

/////////////////////////////////////////////////
// MACI DLL support functions
/////////////////////////////////////////////////
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(CalcSkyDelayServerWrapper)
