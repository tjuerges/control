#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It defines the SkyDelayServer class.
"""
import Acspy.Clients.SimpleClient
import Acspy.Common.QoS
import Acspy.Util.ACSCorba
import math
import TETimeUtil

from CCL.DelayServerBase import DelayServerBase

from Control import DelaySource
from Control import AntennaCharacteristics

from asdmIDLTypes import IDLLength

from CCL.Container import getComponent
from CCL.Container import getDynamicComponent
from CCL.logging import getLogger

class SkyDelayServer(DelayServerBase):
    '''
    The SkyDelayServer object delivers array based delay events to all
    of the antennas in an array.  Usually this functionality is
    contained withing an observing mode, but for testing and debugging
    it is useful to have a Python CCL binding to it.
    
    EXAMPLE:
    '''

    def __init__(self, arrayName = None, componentName = None):
        '''
        The constructor creates a SkyDelayServer object.

        If the arrayName is defined then this constructor starts a
        dynamic component that implements the functions available in
        the SkyDelayServer object. The constructor also initialized the object
        to the default array center (currently 0,0,0)

        If the componentName is not none, then the object uses that component
        to implement the functions available in the SkyDelayServer
        It is assumed that the component was initialized by other means and
        references to the relavent hardware already exist.  This method is
        intended for internal use, and should only be used by experts.

        An exception is thrown if there is a problem creating
        this component, establishing the connection to the
        previously mentioned hardware components, or if either
        both or neither antennaName and componentName are specified.
        
        EXAMPLE:
        # Load the necessary defintions
        from CCL.SkyDelayServer import SkyDelayServer
        # create the object
        sds = SkyDelayServer("Array001")
        # destroy the component
        del(ds)
        '''
        if not((arrayName == None) ^ (componentName == None)):
            raise;

        self.__logger = getLogger()

        if componentName != None:
            # Specified previously existing component, just get reference
            self.__ds = getComponent(componentName)
            if (self.__ds == None):
                return None
            DelayServerBase.__init__(self,self.__ds)

        if arrayName != None:
            self.__ds = getDynamicComponent(None, 'IDL:alma/Control/SkyDelayServer:1.0', None, None)
            if (self.__ds == None):
                return None
            self.__ds.initializeServer(arrayName)

            DelayServerBase.__init__(self,self.__ds)
            self.setReference(0,0,0)
            

    def addSiderealSource(self, RA = 0.0, Dec = 0.0, DRA = 0.0, DDec = 0.0,
                          Parallax = 0.0, Epoch = 51544.5, startTime = None,
                          delaySource = None):
        '''
        This adds a source to the queue for calculation by the Delay Server
        if DelaySource in not None it is used in its entirety.  Otherwise
        a DelaySource object is built from the input variables.  The source
        object has the following fields:
        double      RA;              # Right Acsension (in radians)
        double      Dec;             # Declenation (in radians)
        double      DRA;             # Derivitive in RA (rad/s)
        double      DDec;            # Derivitive in Dec (rad/s)
        double      Parallax;        # Parallax of source       
        double      Epoch;           # Epoch of RA and Dec (mjd)
        ACS::Time   sourceStartTime; # Timestamp of beginning of observation 

        Note the default epoch corresponds to J2000.
        
        If the input \"startTime\" parameter is None, the system will set
        the source start time to be the current time.
        '''
        if delaySource == None:
            if startTime == None:
                startTime = TETimeUtil.unix2epoch(TETimeUtil.time.time()).value
            delaySource = DelaySource(float(RA),  float(Dec),
                                      float(DRA), float(DDec),
                                      float(Parallax), float(Epoch),
                                      False, '', [], startTime)
        self.__ds.addSource(delaySource)

    def addPlanetarySource(self, PlanetName, startTime=None):
        '''
        This adds a source to the queue for calculation by the Delay Server
        The current list of supported sources is:
           'MERCURY', 'VENUS', 'MARS', 'JUPITER', 'SATURN', 'URANUS',
           'NEPTUNE', 'PLUTO', 'SUN', 'MOON'

        If the input \"startTime\" parameter is None, the system will set
        the source start time to be the current time.
        '''

        if startTime == None:
            startTime = TETimeUtil.unix2epoch(TETimeUtil.time.time()).value
        delaySource = DelaySource(0,  0, 0, 0, 0, 0,
                                  True, PlanetName.upper(), [], startTime)
        self.__ds.addSource(delaySource)

    def setReference(self, xPosition, yPosition, zPosition):
        '''
        This method sets the reference position for the Array in 3 space.
        [x,y,z]Position is the Geocentric X,Y,Z of the reference point in
        meters.  The timeOffset is the delay imposed across the array to
        ensure that all delays are positive relative to the reference.
        
        EXAMPLE
        >>> from CCL.SkyDelayServer import SkyDelayServer
        >>> sds = SkyDelayServer("Array001")
        >>> sds.setReference(0,0,0)
        '''
        self.__ds.setReference(xPosition, yPosition, zPosition)

        
    
