#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It defines the DelayServerBase class.
"""
import Acspy.Clients.SimpleClient
import Acspy.Common.QoS
import Acspy.Util.ACSCorba
import math
import Control

from CCL.Container import getComponent
from CCL.Container import getDynamicComponent
from CCL.logging import getLogger
import CCL.SIConverter

class DelayServerBase:
    '''
    The DelayServerBase is a Python proxy for the base class of the
    Delay Server Components.  As the Base class it reflects is abstract
    this should never really be needed to be instaciated directly.
    '''
    def __init__(self, componentRef):
        self.__ds = componentRef

    def _getReference(self):
        '''
        Utility method which returns the reference to the component
        '''
        return self.__ds

    def addAntenna(self, antennaName):
        '''
        This method adds another antenna to the list of antennas associated
        with this Array.
        '''
        self.__ds.addAntenna(antennaName)

    def removeAntenna(self, antennaName):
        '''
        This method removes the specified antenna from the list of
        antennas supported by this delay server.

        If the antenna is not already defined in this array nothing is done
        '''
        self.__ds.removeAntenna(antennaName)

    def getAntennas(self):
        '''
        This method returns a list of the antennas currently specified
        on this delay server.
        '''
        return self.__ds.getAntennas()
        
    def setAntennaPadLocation(self, antennaName, X, Y, Z):
        '''
        This method (temporarily) changes the pad location for the
        currently specified antenna.  This does not affect the persistent
        version of the pad location stored in the data base.

        The X, Y, and Z coordinates are specified in the frame of the
        geocenter and are in units of meters.
        
        If the  antenna is not already specified for the delay server
        an IllegalParameterErrorEx is thrown.

        Example (assuming ds is a DelayServer reference):
        >>> ds.setAntennaPadLocations("DV01", "230.542 km", 26034.2, -35768.1)
        '''
        self.__ds.setAntennaPadLocation(antennaName,
                                        CCL.SIConverter.toMeters(X),
                                        CCL.SIConverter.toMeters(Y),
                                        CCL.SIConverter.toMeters(Z))

    def getAntennaPadLocation(self, antennaName):
        '''
        This method returns the current pad location for the
        currently specified antenna. 

        The X, Y, and Z coordinates are specified in the frame of the
        geocenter and are in units of meters.
        
        If the  antenna is not already specified for the delay server
        an IllegalParameterErrorEx is thrown.

        Example (assuming ds is a DelayServer reference):
        >>> padLocation = ds.getAntennaPadLocations("DV01")
        >>> padLocation
        (23054.2, 26034.2, -35768.1)
        '''
        return list(self.__ds.getAntennaPadLocation(antennaName))
            
    def setAntennaVector(self, antennaName, X, Y, Z):
        '''
        This method (temporarily) adjusts the vector pointing from the
        pad location, to the theoretical axis intersection for the antenna.

        Here X, Y, and Z are in the local horizonal frame, with Z pointing
        in the local up direction.
        
        If the  antenna is not already specified for the delay server
        an IllegalParameterErrorEx is thrown.

        Example (assuming ds is a DelayServer reference):
        >>> ds.setAntennaVector("DA41", "15 um", "-25 um", 12.5)
        '''
        self.__ds.setAntennaVector(antennaName,
                                   CCL.SIConverter.toMeters(X),
                                   CCL.SIConverter.toMeters(Y),
                                   CCL.SIConverter.toMeters(Z))


    def getAntennaVector(self, antennaName):
        '''
        This method returns the current relative antenna position
        (the vector from the pad to the axis intersection) for the
        specified antenna. 

        The X, Y, and Z coordinates are specified in the local horizontal
        frame and are in units of meters.
        
        If the  antenna is not already specified for the delay server
        an IllegalParameterErrorEx is thrown.
        '''
        return list(self.__ds.getAntennaVector(antennaName))


    def setAxisNonIntersectionParameter(self, antennaName, kTerm):
        '''
        This method [temporarily] adjusts the non interstection of the
        axis vector.  This is sometimes referred to as the k-Term.

        If the antenna is not already specified for the delay server
        an IllegalParameterErrorEx is thrown.

        Example (assuming ds is a DelayServer reference):
        >>> ds.setAxisNonIntersectionParameter("DA41", "1.2 um")
        or
        >>> ds.setAxisNonIntersectionParameter("DA41", 1.2E-6)
        '''
        self.__ds.setAxisNonIntersectionParameter(antennaName,
                                                  CCL.SIConverter.toMeters(kTerm))
        
    def getAxisNonIntersectionParameter(self, antennaName):
        '''
        This method adjusts the current parameter describing the
        non interstection of the antenna axis sometimes referred to
        as the k-Term.

        If the antenna is not already specified for the delay server
        an IllegalParameterErrorEx is thrown.

        Example (assuming ds is a DelayServer reference):
        >>> ds.getAxisNonIntersectionParameter("DA41")
        1.2E-6
        '''
        return self.__ds.getAxisNonIntersectionParameter(antennaName)


    def setAntennaCableDelay(self, antennaName, cableDelay):
        '''
        This method (temporarily) sets the cable delay associated with
        a particular antenna.

        If the antenna is not already specified for the delay server
        an IllegalParameterErrorEx is thrown.

        Example (assuming ds is a DelayServer reference):
        >>> ds.setAntennaCableDealy("DA41", "32.5 ns")
        '''
        self.__ds.setAntennaCableDelay(antennaName,
                                       CCL.SIConverter.toSeconds(cableDelay))

    def getAntennaCableDelay(self, antennaName):
        '''
        This method returns the current cable delay for the
        requested antenna.  The return value is in seconds.

        If the antenna is not already specified for the delay server
        an IllegalParameterErrorEx is thrown.

        Example (assuming ds is a DelayServer reference):
        >>> ds.getAntennaCableDelay("DA41")
        32.5E-9
        '''
        return self.__ds.getAntennaCableDelay(antennaName)

    def setCausalityDelay(self, newDelay):
        '''
        This method sets the global delay applied to all antennas to
        ensure that we do not have negative delays at any point in the array
        Current default of 200 ns is appropriate for ATF.

        Example (assuming ds is a DelayServer reference):
        >>> ds.setCauslityDelay("60 us") 
        '''
        self.__ds.setCausalityDelay(CCL.SIConverter.toSeconds(newDelay))

    def getCausalityDelay(self):
        '''
        This method returns the current value of the  global delay applied
        to all antenna to ensure that we do not have negative delays at any
        point in the array.  The return value is in seconds.

        Example (assuming ds is a DelayServer reference):
        >>> ds.getCausalityDelay()
        6.0E-5
        '''
        return self.__ds.getCausalityDelay()

        
    def setPolynomialOrder(self, polynomialOrder):
        '''
        This is here for flexability but should only be modified by
        _really_ expert users.  Adjusting this may reduce the accuracy of
        fringe and delay tracking.
        '''
        self.__ds.setPolynomialOrder(polynomialOrder)

    def getPolynomialOrder(self):
        '''
        Returns the current polynomial order being used
        '''
        return self.__ds.getPolynomialOrder()

        
    def setDelayEventDuration(self,delayDuration):
        '''
        This method sets the time over which a single delay event
        should be valid.  The two constraints are that the duration must be
        an integer number of 48 ms intervals and the maximum time is 1 Min.
        If the specified value is not an multiple of 48 ms, the system will
        determine the largest acceptable value less than the specified value.

        An exception will be thrown if the the duration is not in the
        valid range.

        Example (assuming ds is a DelayServer reference):
        >>> ds.setDelayEventDuration("0.5 min")
        '''
        newDuration = int(CCL.SIConverter.toSeconds(delayDuration) * 1E7)
        self.__ds.setDelayEventDuration(newDuration)

    def getDelayEventDuration(self):
        '''
        Return the current delay event duration (in seconds).

        Example (assuming ds is a DelayServer reference):
        >>> ds.getDelayEventDuration()
        30.0
        '''
        return self.__ds.getDelayEventDuration()/1.0E7

    def setAtmosphericDelayModel(self,modelType=Control.ZERO_ATMOSPHERE_MODEL):
        '''
        This method selects the Atmospheric Delay Model to be used.
        The option can be entered either as an enumerated value or
        as a string.

        Example:
        >>> import Control
        >>> ds = sfi.getDelayServer();
        >>> ds.setAtmosphericDelayModel(Control.ZERO_ATMOSPHERE_MODEL

        Vaild options are:
           * ZERO_ATMOSPHERE_MODEL: No atmospheric delay model used
           * CALC_DRY_ONLY: Only use the Dry portion of the model determined
                            by CALC.
           * CALC_WET_AND_DRY: Use both the wet atmosphere and dry atmosphere
                               corrections determined by CALC.
        '''
        enumMap= {'ZERO_ATMOSPHERE_MODEL': Control.ZERO_ATMOSPHERE_MODEL,
                  'CALC_DRY_ONLY': Control.CALC_DRY_ONLY,
                  'CALC_WET_AND_DRY': Control.CALC_WET_AND_DRY}
        if not isinstance(modelType, str):
            modelType = str(modelType)

        try:
            modelType = enumMap[modelType.upper()]
        except KeyError:
            raise ControlExceptions.IllegalParameterErrorEx(modelType.upper()+
                        'is not a member of the ' +
                        'Control::AtmosphericDelayModelType enumeration.')

        self.__ds.setAtmosphericDelayModelType(modelType)

    def getAtmosphericDelayModel(self):
        '''
        Return the current Atmospheric delay modle being used.  Set the
        setAtmosphericDelayModel for explanation of the return value.
        '''
        return self.__ds.getAtmosphericDelayModelType()

    def enableAntennaModel(self, enable):
        '''
        This method turn on/off the refraction correction on
        delay server.
        '''
        self.__ds.enableAntennaModel(enable)
        
    
    def antennaModelEnabled(self):
        '''
        This function returns the state of refraction correction.
        True value means correction is enabled.
        '''
        return self.__ds.antennaModelEnabled()   
    
