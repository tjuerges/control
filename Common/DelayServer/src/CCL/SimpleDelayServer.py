#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It defines the SkyDelayServer class.
"""
import Acspy.Clients.SimpleClient
import Acspy.Common.QoS
import Acspy.Util.ACSCorba
import math
import TETimeUtil

from CCL.DelayServerBase import DelayServerBase
from Control import SimpleDelayServer as SimpleDelayServerIDL


from CCL.Container import getComponent
from CCL.Container import getDynamicComponent
from CCL.logging import getLogger

class SimpleDelayServer(DelayServerBase):
    '''
    The SimpleDelayServer object delivers array based delay events to all
    of the antennas in an array, based on simple polynomial faux delay models.
    This component is used primarily for testing.
    
    EXAMPLE:
    '''

    def __init__(self, arrayName = None, componentName = None):
        '''
        The constructor creates a SimpleDelayServer object.

        If the arrayName is defined then this constructor starts a
        dynamic component that implements the functions available in
        the SimpleDelayServer object. The constructor also initialized the
        object to the default array center (currently 0,0,0)

        If the componentName is not none, then the object uses that component
        to implement the functions available in the SimpleDelayServer
        It is assumed that the component was initialized by other means and
        references to the relavent hardware already exist.  This method is
        intended for internal use, and should only be used by experts.

        An exception is thrown if there is a problem creating
        this component, establishing the connection to the
        previously mentioned hardware components, or if either
        both or neither antennaName and componentName are specified.
        
        EXAMPLE:
        # Load the necessary defintions
        from CCL.SkyDelayServer import SimpleDelayServer
        # create the object
        sds = SimpleDelayServer("Array001")
        # destroy the component
        del(ds)
        '''
        if not((arrayName == None) ^ (componentName == None)):
            raise;

        self.__logger = getLogger()

        if componentName != None:
            # Specified previously existing component, just get reference
            self.__ds = getComponent(componentName)
            if (self.__ds == None):
                return None

        if arrayName != None:
            self.__ds = getDynamicComponent(None, 'IDL:alma/Control/SimpleDelayServer:1.0', None, None)
            if (self.__ds == None):
                return None
            self.__ds.initializeServer(arrayName)

        DelayServerBase.__init__(self,self.__ds)

    def setAntennaDelay(self, antennaId, coefficients, referenceTime = None):
        '''
        This method defines the delay polynomial to be used for the
        specified antenna.

        The antennaID is the string identifing the target antenna which was
        already added using the addAntenna method.

        The coefficients are a python list of the desired delay polynomial
        in decreasing order.  For instance [a, b, c] would generate the
        polynomial a(t-t0)^2 + b(t-t0) + c. The polynomial order is set by
        the length of the input vector.

        The reference time specifies t0 for the polynomial, if no
        reference time is specified the the current time is used.  Times
        are specified in ACS units (100ns from Oct 15, 1582).

        EXAMPLE:
        >>> from CCL.SimpleDelayServer import SimpleDelayServer
        >>> sds = SimpleDelayServer(\'Array001\')
        >>> sds.addAntenna(\'DV01\')
        >>> sds.setAntennaDelay(\'DV01\',[1,0])
        '''

        if referenceTime == None:
            referenceTime = TETimeUtil.unix2epoch(TETimeUtil.time.time()).value

        delayPolynomial = SimpleDelayServerIDL.DelayPolynomial(referenceTime,
                                                               coefficients)
        self.__ds.setAntennaDelay(antennaId, delayPolynomial)


