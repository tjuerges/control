/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2005 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2005-07-30  created 
*/

/************************************************************************
*   NAME
*
* 
*------------------------------------------------------------------------
*/


static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <delayServerData.h>
#include <iostream>
#include <ControlExceptions.h>

const ACS::Time DelayServerData::ExpirationTime = 3000000000ULL;// 5 min

DelayServerData::DelayServerData():
  antennaVector(3),
  padVector(3),
  offsetVector(3),
  geometricDelay(0),
  dryAtmDelay(0),
  wetAtmDelay(0),
  azDirection(0),
  elDirection(0),
  antennaCableDelay(0),
  totalDelay(0),
  polynomialOrder_m(0)
{}

DelayServerData::~DelayServerData(){}

void DelayServerData::setPolynomialOrder(unsigned int order){
  polynomialOrder_m = order;
  geometricDelay.resize(polynomialOrder_m+1);
  dryAtmDelay.resize(polynomialOrder_m+1);
  wetAtmDelay.resize(polynomialOrder_m+1);
  azDirection.resize(polynomialOrder_m+1);
  elDirection.resize(polynomialOrder_m+1);
  totalDelay.resize(polynomialOrder_m+1);
}

void DelayServerData::calculateTotalDelay() {
  /* Nothing to do, return */
  if (polynomialOrder_m == 0) return;

  for (unsigned int idx = 0; idx < polynomialOrder_m+1; idx++) {
    totalDelay[idx] = geometricDelay[idx] + dryAtmDelay[idx] 
      + wetAtmDelay[idx] + antennaCableDelay;
    std::ostringstream msg;
    msg << " For antenna " << antennaName
        << " the total delay is " << totalDelay[idx]*1E9 << ""
        << "ns (geometric=" << geometricDelay[idx]*1E9 
        << " + wet atmosphere=" << wetAtmDelay[idx]*1E9
        << " + dry atmosphere=" << dryAtmDelay[idx]*1E9
        << " + cable=" << antennaCableDelay*1E9 << ")." << endl;
    LOG_TO_OPERATOR(LM_INFO, msg.str());
  }
}

void DelayServerData::addAtmosphericModel(AtmosphericModel& model){
  ACS_TRACE(__func__);
  const ACS::Time FIVE_MINUTES = 3000000000ULL;
  
  std::deque<AtmosphericModel>::iterator iter =
    getAtmosphericModelIter(model.applicationTime);

  if (iter == atmosphericModel_m.end()) {
    /* We need to insert at the beginning */
    iter = atmosphericModel_m.insert(atmosphericModel_m.begin(), model);
  } else if (iter->applicationTime == model.applicationTime) {
    iter = atmosphericModel_m.insert(iter, model);
  } else {
    iter = atmosphericModel_m.insert(++iter, model);
  }
  atmosphericModel_m.erase(++iter, atmosphericModel_m.end());
  
  /* Now get rid of anything that stopped being useful 5 min ago */
  iter = getAtmosphericModelIter(::getTimeStamp() - FIVE_MINUTES);
  if (iter != atmosphericModel_m.end() && iter != atmosphericModel_m.begin()) {
    atmosphericModel_m.erase(atmosphericModel_m.begin(), iter);
  }
}

DelayServerData::AtmosphericModel 
DelayServerData::getAtmosphericModel(ACS::Time targetTime) {
  ACS_TRACE(__func__);

  std::deque<AtmosphericModel>::iterator iter = 
    getAtmosphericModelIter(targetTime);

  if (iter == atmosphericModel_m.end()) {
    /* This means either we are empty or this time is before all 
       entries in the Deque */
    ControlExceptions::InvalidRequestExImpl ex(__FILE__,__LINE__,__func__);
    std::ostringstream msg;
    if (atmosphericModel_m.empty()) {
      msg << "Unable to find Antenna Atmospheric model for antenna "
          << antennaName << " no models have been entered";
    } else {
      msg << "The requested time ( "<< targetTime 
          << ") is before any application time in the queue. Earliest time is "
          << atmosphericModel_m[0].applicationTime;
    }
    ex.addData("ErrorMessage", msg.str().c_str());
    ex.log();
    throw ex;
  }
  
  return *iter;
}


std::deque<DelayServerData::AtmosphericModel>::iterator 
DelayServerData::getAtmosphericModelIter(ACS::Time targetTime) {
  ACS_TRACE(__func__);

  if (atmosphericModel_m.empty() ||
      targetTime < atmosphericModel_m.begin()->applicationTime) {
    return atmosphericModel_m.end();
  }

  std::deque<AtmosphericModel>::iterator iter = atmosphericModel_m.begin();

  do iter++; while ((iter != atmosphericModel_m.end()) && 
                   (targetTime >= iter->applicationTime));

  return --iter;
}

void DelayServerData::zeroDryComponent() {
  for (unsigned int idx = 0; idx < polynomialOrder_m+1; idx++) 
    dryAtmDelay[idx] = 0.0;
}

void DelayServerData::zeroWetComponent() {
  for (unsigned int idx = 0; idx < polynomialOrder_m+1; idx++) 
    wetAtmDelay[idx] = 0.0;
}

void DelayServerData::applyZenithDelayValue(ACS::Time currentTime){
  try {
    DelayServerData::AtmosphericModel model = getAtmosphericModel(currentTime);
    for (unsigned int idx = 0; idx < polynomialOrder_m+1; idx++) {
      dryAtmDelay[idx] =  model.zenithDryDelay *
        almaDryMappingFunction(elDirection[idx]);
      wetAtmDelay[idx] =  model.zenithWetDelay * 
        almaWetMappingFunction(elDirection[idx]);
    }
  } catch (ControlExceptions::InvalidRequestExImpl& ex) {
    std::ostringstream msg;
    msg <<  "No valid Antenna Atmospheric model for antenna " << antennaName;
    LOG_TO_OPERATOR(LM_ERROR, msg.str().c_str());
  }
}
  
double DelayServerData::almaDryMappingFunction(const double elevation){
  /* This is a simplified version of the mapping function from Mangum's
     memo, and Niell 1996 

     The coefficients used are the average values at 30 degrees latitude
  */
  const double a = 1.2683230E-3;
  const double b = 2.9152299E-3;
  const double c = 62.837393E-3;
  
  double uncorrected = mappingFunction(elevation, a, b, c);
  return uncorrected;
}

double DelayServerData::almaWetMappingFunction(const double elevation){
  /* This is a simplified version of the mapping function from Mangum's
     memo, and Niell 1996 

     The coefficients used are the wet values at 30 degrees latitude
  */
  const double a = 5.6794847E-4;
  const double b = 1.5138625E-3;
  const double c = 4.6729510E-2;
  
  double uncorrected = mappingFunction(elevation, a, b, c);
  return uncorrected;
}

double DelayServerData::mappingFunction(const double elevation,const double a,
                                        const double b, const double c) {
  double sel = sin(elevation);
 
  double num   = 1.0 + (a/(1.0 + (b/(1.0 +c))));
  double denom = sel + (a/(sel + (b/(sel +c))));

  return num/denom;
}
