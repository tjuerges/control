/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2005 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2005-07-30  created 
*/

/************************************************************************
*   NAME
*   
* 
*   SYNOPSIS
*
*   
*   PARENT CLASS
*
* 
*   DESCRIPTION
*
*
*   PUBLIC METHODS
*
*
*   PUBLIC DATA MEMBERS
*
*
*   PROTECTED METHODS
*
*
*   PROTECTED DATA MEMBERS
*
*
*   PRIVATE METHODS
*
*
*   PRIVATE DATA MEMBERS
*
*
*   FILES
*
*   ENVIRONMENT
*
*   COMMANDS
*
*   RETURN VALUES
*
*   CAUTIONS 
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS   
* 
*------------------------------------------------------------------------
*/


static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);


#include <math.h>
#include <delayReceiver.h>
#include <string.h>

#include <stdio.h>

#define ALL_EVENT_TYPE "*"

DelayReceiver::DelayReceiver():
  nc::Consumer(Control::CHANNELNAME_CONTROLREALTIME),
  numEvents_m(0)
{
  ACS_TRACE("DelayReceiver::DelayReceiver()")
  Consumer::init();
  Consumer::addSubscription<Control::ArrayDelayEvent>();
  Consumer::consumerReady();
  Consumer::suspend();
}

DelayReceiver::DelayReceiver(CORBA::ORB_ptr orbRef_p):
  nc::Consumer(Control::CHANNELNAME_CONTROLREALTIME,orbRef_p)
{
  ACS_TRACE("DelayReceiver::DelayReceiver(CORBA::ORB_ptr)");
  Consumer::init();
  Consumer::addSubscription<Control::ArrayDelayEvent>();
  Consumer::consumerReady();
  Consumer::suspend();
}

DelayReceiver::~DelayReceiver() {
  ACS_TRACE("DelayReceiver::~DelayReceiver");
  Consumer::resume();
  disconnect();
}

void DelayReceiver::assignArray(const char* arrayName){
  ACS_TRACE("DelayReceiver::assignArray");
  arrayName_m = arrayName;
  numEvents_m = 0;
  Consumer::resume();
}

void DelayReceiver::unassignArray(){
  ACS_TRACE("DelayReceiver::unassignArray");
  Consumer::suspend();
  arrayName_m = "";
}

unsigned long long DelayReceiver::getNumEvents(){
  ACS_TRACE("DelayReceiver::getNumEvents");
  return numEvents_m;
}

void 
DelayReceiver::push_structured_event(const CosNotification::StructuredEvent &
				     notification){
  // Update the internal counter
  numEvents_m++;

  //extract the data from the event
  Control::ArrayDelayEvent  delayEvent;
  Control::ArrayDelayEvent* delayEvent_p;
  delayEvent_p = &delayEvent;
  notification.filterable_data[0].value >>= delayEvent_p;
    
  //Now call the handler routine
  if (!strcmp(arrayName_m.c_str(),delayEvent_p->arrayName)) {
    eventHandler(*delayEvent_p);
  } 
}


/* ------------------------------------------------------ */


AntennaDelayDistributor::AntennaDelayDistributor() :
  DelayReceiver()
{
  ACS_TRACE("AntennaDelayDistributor::AntennaDelayDistributor");
}

AntennaDelayDistributor::~AntennaDelayDistributor() 
{
  ACS_TRACE("AntennaDelayDistributor::~AntennaDelayDistributor");
  std::multimap<std::string, Control::DelayClient_ptr>::iterator iter;

  /* Because these references come from a narrow we need to release them */
  for (iter=clients_m.begin(); iter!= clients_m.end(); iter++) {
    CORBA::release(iter->second);
  }
}

void AntennaDelayDistributor::eventHandler(Control::ArrayDelayEvent& event){
  ACS_TRACE("AntennaDelayDistributor::AntennaImpl::eventHandler");
  std::multimap<std::string, Control::DelayClient_ptr>::iterator iter;  

  for (unsigned int antIdx = 0; 
       antIdx < event.antennaDelayEvents.length();
       antIdx++){
    if (!strcmp(event.antennaDelayEvents[antIdx].antennaName,
		antennaName_m.c_str())) {
      for (iter=clients_m.begin(); iter!= clients_m.end(); iter++) {
	iter->second->newDelayEvent(event.antennaDelayEvents[antIdx]);
      }
      return;
    }
  }
}

void AntennaDelayDistributor::setAntennaName(const char* antennaName) {
  ACS_TRACE("AntennaDelayDistributor::setAntennaName");
  antennaName_m = antennaName;
}

void 
AntennaDelayDistributor::attachClient(std::string clientName,
				      Control::DelayClient_ptr client) {
  ACS_TRACE("AntennaDelayDistributor::attachClient");
  clients_m[clientName] = Control::DelayClient::_duplicate(client);
}

/*___oOo___*/
