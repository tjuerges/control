/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2005
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2005-07-30  created
*/

static char *rcsId="@(#) $Id$";
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <calcCommManager.h>
#include <math.h>
#include <poll.h>

CalcCommManager::CalcCommManager():
  outputFd_m(-1),
  inputFd_m(-1),
  childProcessID_m(-1),
  calcExecutableName_m("calcProcess"),
  polynomialOrder_m(4),
  delayTable_m(NULL),
  azTable_m(NULL),
  elTable_m(NULL),
  dryAtmTable_m(NULL),
  wetAtmTable_m(NULL)
{
  transferTableMutex_p   = new ACE_Recursive_Thread_Mutex();


  /* Find the current day as a MJD, round off.  The UTCDate is not precise
     here but is good enough for what we need
  */
  EpochHelper timeNow(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
  todayMJD_m = floor(timeNow.toUTCdate(0, 0) + 0.5);
  
  try { 
    reinitializeCalcProcess(); 
  } catch (DelayServerExceptions::ReinitializationErrorExImpl& ex) {
    throw DelayServerExceptions::ReinitializationErrorExImpl(ex,__FILE__,
                               __LINE__,__func__);
  }
}

CalcCommManager::CalcCommManager(const double& todayMJD):
  outputFd_m(-1),
  inputFd_m(-1),
  childProcessID_m(-1),
  calcExecutableName_m("calcProcess"),
  polynomialOrder_m(5),
  todayMJD_m(todayMJD),
  delayTable_m(NULL),
  azTable_m(NULL),
  elTable_m(NULL),
  dryAtmTable_m(NULL),
  wetAtmTable_m(NULL)
{
  transferTableMutex_p   = new ACE_Recursive_Thread_Mutex();
  try { 
    reinitializeCalcProcess(); 
  } catch (DelayServerExceptions::ReinitializationErrorExImpl& ex) {
    throw DelayServerExceptions::ReinitializationErrorExImpl(ex, __FILE__,__LINE__,__func__);
  }
}

CalcCommManager::CalcCommManager(string calcExecutableName):
  outputFd_m(-1),
  inputFd_m(-1),
  childProcessID_m(-1),
  calcExecutableName_m(calcExecutableName),
  polynomialOrder_m(4),
  delayTable_m(NULL),
  azTable_m(NULL),
  elTable_m(NULL),
  dryAtmTable_m(NULL),
  wetAtmTable_m(NULL) 
{/* Because this is for testing only, thats all were gonna do */
  transferTableMutex_p   = new ACE_Recursive_Thread_Mutex();
}

CalcCommManager::~CalcCommManager(){
  closeCalcProcess();
  delete transferTableMutex_p;

  delete[] delayTable_m; 
  delete[] azTable_m; 
  delete[] elTable_m;
  delete[] dryAtmTable_m; 
  delete[] wetAtmTable_m;
}


void CalcCommManager::startCalcProcess(){
  if (childProcessID_m != -1) {
    closeCalcProcess();
  }

  /* Open the new file descriptors 
     Note: idx0 is for reading
           idx1 is for writing
  */
  int   commOutputDescriptor[2];
  int   commInputDescriptor[2];
  
  if (pipe(commOutputDescriptor) ||  pipe(commInputDescriptor)) {
    throw DelayServerExceptions::CommunicationChannelErrorExImpl(__FILE__,
                                                                 __LINE__,
                                                                 __func__);
  }

  childProcessID_m = fork();
  if(childProcessID_m == 0){
    /* Store the file descriptors as strings so we can pas them to the
       childProcess.
       Note the names of these are from the perspective of the child
       so fdIn is the one the child should read from.
    */
    char  fdInChar[15];
    char  fdOutChar[15];
    sprintf(fdInChar, "%d", commOutputDescriptor[0]); 
    sprintf(fdOutChar,"%d", commInputDescriptor[1]); 
    close(commOutputDescriptor[1]);
    close(commInputDescriptor[0]);
    execlp(calcExecutableName_m.c_str(), calcExecutableName_m.c_str(),
           fdInChar, fdOutChar, (char*)0 );
  }
  
  SIGCHLD_HANDLER(childProcessID_m);
  close(commOutputDescriptor[0]);
  close(commInputDescriptor[1]);
  inputFd_m  = commInputDescriptor[0];
  outputFd_m = commOutputDescriptor[1];

  CalcStatusType_t returnStatus = getChildMessage();
  
  switch (returnStatus) {
  case Success:
    return;
  case Timeout:
    {
      DelayServerExceptions::StartCalcErrorExImpl ex(__FILE__,__LINE__,
                                                     __func__);
      ex.addData("ErrorMessage", "Timeout waiting for CALC process to start");
      throw ex;
    }
  default:
    {
      DelayServerExceptions::StartCalcErrorExImpl ex(__FILE__,__LINE__, 
                                                     __func__);
      ex.addData("ErrorMessage", "Error detected in starting CALC process");
      throw ex;
    }
  }
}

void CalcCommManager::closeCalcProcess(){
  if (childProcessID_m != -1 && !checkCalcProcess()) {
    /* Child process is running send it the shutdown command */
    CalcRequestType_t exitFlag = ExitRequest;
  
    writeAll(&exitFlag, sizeof(CalcRequestType_t));

    if (getChildMessage() == Exit) {
      /* Child seems to be shutting down properly */
      childProcessID_m = -1;
    }
  }
   
  /* Make sure all of the file handles are closed */
  if (outputFd_m != -1) {   
    close(outputFd_m);
    outputFd_m = -1;
  }

  if (inputFd_m != -1) {
    close(inputFd_m);
    inputFd_m = -1;
  }
    
  /* Make sure the process actually closed */
  if (childProcessID_m != -1 && !checkCalcProcess()) {
    /* Child Process has not exited */
    kill(childProcessID_m, SIGKILL);
  }
  childProcessID_m = -1;
}

bool CalcCommManager::checkCalcProcess() {
  if(waitpid(childProcessID_m, NULL, WNOHANG) == childProcessID_m) {
    /* Child Process has exited */
    return true;
  } 
  return false;
}

void CalcCommManager::reinitializeCalcProcess() {
  if (childProcessID_m != -1) {
    closeCalcProcess();
  }

  try {
    startCalcProcess();
  }catch(DelayServerExceptions::CommunicationChannelErrorExImpl& ex){
    throw DelayServerExceptions::ReinitializationErrorExImpl(ex,__FILE__,__LINE__,__func__);
  }catch(DelayServerExceptions::StartCalcErrorExImpl& ex){
    throw DelayServerExceptions::ReinitializationErrorExImpl(ex, __FILE__, __LINE__,__func__);
  }

  /* Send the polynomial order */
  if (writePolynomialOrder()) {
    DelayServerExceptions::ReinitializationErrorExImpl 
      newEx(__FILE__,__LINE__,__func__);	 
    newEx.addData("Details", "Failed sending polynomial order to calc process.");
    throw newEx;
  }

  /* Initilize Calc Process */
  if (initializeCalcProcess()) {
    DelayServerExceptions::ReinitializationErrorExImpl 
      newEx(__FILE__,__LINE__,__func__);
    newEx.addData("Details", "Failed initalizing calc process.");
    throw newEx;
  }

  /* If it is defined reload the reference */
  if (writeReference()){
    DelayServerExceptions::ReinitializationErrorExImpl 
      newEx(__FILE__,__LINE__,__func__);
    newEx.addData("Details", "Failed sending reference localtion to calc process.");
    throw newEx;
  }    

  if (sourceList_m.size() > 0) {
    if (writeSourceList()) {
      DelayServerExceptions::ReinitializationErrorExImpl 
	newEx(__FILE__,__LINE__,__func__);
      newEx.addData("Details", "Failed sending source list to calc process.");
      throw newEx;
    }
  }

  /* Reload all of the Antenna's */
  std::vector<CalcAntenna_t>::iterator antIter = antennaList_m.begin();
  for (;antIter != antennaList_m.end(); antIter++) {
    if (writeAntenna(*antIter)) {
      DelayServerExceptions::ReinitializationErrorExImpl 
	newEx(__FILE__,__LINE__,__func__);
      newEx.addData("Details", "Failed sending antenna list to calc processes.");
      throw newEx;
    }
  }
}


void CalcCommManager::addAntenna(CalcAntenna_t newAntenna) {
  bool antennaExist = false;

 
  /* Update position at the antenna list if exist*/
  for (unsigned int antIdx = 0; antIdx < antennaList_m.size(); antIdx++) { 
    if (!strncmp(newAntenna.antennaName,antennaList_m[antIdx].antennaName,4)) {
      antennaList_m[antIdx].x = newAntenna.x;
      antennaList_m[antIdx].y = newAntenna.y;
      antennaList_m[antIdx].z = newAntenna.z;
      antennaList_m[antIdx].axisOffset = newAntenna.axisOffset;
      antennaExist = true;
    }
  }
      
  /* Store this antenna to the list so we can reinitialize if necessary */
  if (!antennaExist) {
    antennaList_m.push_back(newAntenna);
    reallocateTransferBuffers();
  }

  if (writeAntenna(newAntenna)) {
    /* Error Adding the Antenna.. try to reinitialize */
    try { 
      reinitializeCalcProcess(); 
    } catch (DelayServerExceptions::ReinitializationErrorExImpl& ex) {
      /* Error reinitializing the Calc Process*/
      throw DelayServerExceptions::ReinitializationErrorExImpl(ex,
				  __FILE__,__LINE__,__func__);
    }
  }
}


void CalcCommManager::setReference(CalcAntenna_t newReference) {
  /* Store this antenna to the list so we can reinitialize if necessary */  
  reference_m = newReference;
  if (writeReference()) {
    /* Error setting the reference.. try to reinitialize */
   try { 
      reinitializeCalcProcess(); 
    } catch (DelayServerExceptions::ReinitializationErrorExImpl& ex) {
      /* Error reinitializing the Calc Process*/
      throw DelayServerExceptions::ReinitializationErrorExImpl (ex,
                                    __FILE__,__LINE__,__func__);
    }
  }
}

void CalcCommManager::setPolynomialOrder(unsigned int polyOrder) {
  if (polynomialOrder_m == polyOrder) {
    /* Nothing to do */
    return;
  }

  polynomialOrder_m = polyOrder;
  reallocateTransferBuffers();
  
  if (writePolynomialOrder()) {
    /* Error writing the polynomial order */
    try { 
      reinitializeCalcProcess(); 
    } catch (DelayServerExceptions::ReinitializationErrorExImpl& ex) {
      /* Error reinitializing the Calc Process*/
      throw DelayServerExceptions::ReinitializationErrorExImpl(ex,__FILE__,
                                                        __LINE__,__func__);
    }
  }
}

void CalcCommManager::reallocateTransferBuffers() {
  ACS::ThreadSyncGuard  guard(transferTableMutex_p, true);

  delete[] delayTable_m; 
  delete[] azTable_m; 
  delete[] elTable_m;
  delete[] dryAtmTable_m; 
  delete[] wetAtmTable_m;

  delayTable_m = new double[(polynomialOrder_m + 1) * antennaList_m.size()]; 
  azTable_m= new double[(polynomialOrder_m + 1) * antennaList_m.size()];  
  elTable_m= new double[(polynomialOrder_m + 1) * antennaList_m.size()]; 
  dryAtmTable_m= new double[(polynomialOrder_m + 1) * antennaList_m.size()];  
  wetAtmTable_m= new double[(polynomialOrder_m + 1) * antennaList_m.size()]; 
} 
  

void CalcCommManager::setSource(CalcSource_t newSource) {
  sourceList_m.resize(1);

  sourceList_m[0] = newSource;

  if (writeSourceList()) {
    /* Error writing a new source */
    try { 
      reinitializeCalcProcess(); 
    } catch (DelayServerExceptions::ReinitializationErrorExImpl& ex) {
      /* Error reinitializing the Calc Process*/
      throw DelayServerExceptions::ReinitializationErrorExImpl(ex, 
                                   __FILE__,__LINE__,__func__);
    }
  }
}

void CalcCommManager::setSource(std::vector<CalcSource_t> newSourceList) {
  sourceList_m = newSourceList;

  if (writeSourceList()) {
    /* Error writing a source list */
    try { 
      reinitializeCalcProcess(); 
    } catch (DelayServerExceptions::ReinitializationErrorExImpl& ex) {
      /* Error reinitializing the Calc Process*/
      throw DelayServerExceptions::ReinitializationErrorExImpl(ex,
                                   __FILE__,__LINE__,__func__);
    }
  }
}

void CalcCommManager::getDelayTable(ACS::Time* delayTime,
                                  std::map<std::string, DelayServerData>& map){
// double* delayTable, 
// 				    double* azTable,
// 				    double* elTable,
//                                     double* dryAtmTable,
//                                     double* wetAtmTable) {
  /* The Error handling in this section needs some work 
     Jeff 3/3/2009
  */
  
  ACS::ThreadSyncGuard  guard(transferTableMutex_p, true);
  readCALCProcessResults(delayTime);

  /* Now that we have the data, we need to put it into the map */
  std::map<std::string, DelayServerData>::iterator iter;

//   void* delayPtr  = delayTable_m;
//   void* azPtr     = azTable_m;
//   void* elPtr     = elTable_m;
//   void* dryAtmPtr = dryAtmTable_m;
//   void* wetAtmPtr = wetAtmTable_m;

  char antennaNameHolder[5];

  for (unsigned int ant = 0; ant < map.size(); ant++) {
    /* Because the antenna name is not null terminated we need to 
       copy it into a null terminated string befre calling the
       find method */
    memcpy(antennaNameHolder, antennaList_m[ant].antennaName, 4);
    antennaNameHolder[4] = '\0';
    iter = map.find(antennaNameHolder);


    /* Need to clean up this memory operation but for now do it the 
       dumb (but sure) way. 
       delayPtr =mempcpy(&(iter->second.geometricDelay[0]), delayPtr, 
         sizeof(double)*(polynomialOrder_m+1));
       dryAtmPtr = mempcpy(&(iter->second.dryAtmDelay[0]), dryAtmPtr,
         sizeof(double)*(polynomialOrder_m+1));
       wetAtmPtr = mempcpy(&(iter->second.wetAtmDelay[0]), wetAtmPtr,
         sizeof(double)*(polynomialOrder_m+1));
       azPtr = mempcpy(&(iter->second.azDirection[0]), azPtr,
         sizeof(double)*(polynomialOrder_m+1));
       elPtr = mempcpy(&(iter->second.elDirection[0]), elPtr,
         sizeof(double)*(polynomialOrder_m+1));
    */		      
    for (unsigned int idx = 0; idx < polynomialOrder_m+1; idx++) {
      iter->second.geometricDelay[idx] =
	delayTable_m[idx + ((polynomialOrder_m+1)*ant)];

      iter->second.dryAtmDelay[idx] =
	dryAtmTable_m[idx + ((polynomialOrder_m+1)*ant)];

      iter->second.wetAtmDelay[idx] =
	wetAtmTable_m[idx + ((polynomialOrder_m+1)*ant)];

      iter->second.azDirection[idx]=azTable_m[idx+((polynomialOrder_m+1)*ant)];
      iter->second.elDirection[idx]=elTable_m[idx+((polynomialOrder_m+1)*ant)];
    }
  }
}

void CalcCommManager::readCALCProcessResults(ACS::Time* delayTime) {
  ACS::ThreadSyncGuard  guard(transferTableMutex_p, true);

  CalcRequestType_t request = Delays;
  unsigned int      timeListSize = polynomialOrder_m+1;

  if (writeAll(&request, sizeof(CalcRequestType_t)) ||
      writeAll(&timeListSize, sizeof(unsigned int)) ||
      writeAll(delayTime, sizeof(ACS::Time) * timeListSize)) {
     try { 
      reinitializeCalcProcess(); 
     } catch (DelayServerExceptions::ReinitializationErrorExImpl& ex) {
       /* Error reinitializing the Calc Process*/
       throw DelayServerExceptions::ReinitializationErrorExImpl(ex, 
                                   __FILE__,__LINE__,__func__);
     }
  }
  
  if (getChildMessage() != Success) {
    try { 
      reinitializeCalcProcess(); 
    } catch (DelayServerExceptions::ReinitializationErrorExImpl& ex) {
      /* Error reinitializing the Calc Process*/
       throw DelayServerExceptions::ReinitializationErrorExImpl(ex,
                                    __FILE__,__LINE__,__func__);
     } 
  }

  /* Now read the data back */
  unsigned int tableSize;
  if (readAll(&tableSize, sizeof(unsigned int)) ||
      readAll(delayTable_m, tableSize*sizeof(double)) ||  
      readAll(azTable_m, tableSize*sizeof(double)) ||
      readAll(elTable_m, tableSize*sizeof(double)) ||
      readAll(dryAtmTable_m, tableSize*sizeof(double)) ||
      readAll(wetAtmTable_m, tableSize*sizeof(double))) {
    try { 
      reinitializeCalcProcess(); 
    } catch (DelayServerExceptions::ReinitializationErrorExImpl& ex) {
      /* Error reinitializing the Calc Process*/
      throw DelayServerExceptions::ReinitializationErrorExImpl(ex,
                                                   __FILE__,__LINE__,__func__);
    } 
  }
}


bool CalcCommManager::writeAntenna(CalcAntenna_t newAntenna) {
  CalcRequestType_t request = Antenna;
  
  if (writeAll(&request,sizeof(CalcRequestType_t)) ||
      writeAll(&newAntenna,sizeof(CalcAntenna_t))) {
    return true;
  }

  if (getChildMessage() != Success) {
    /* Error adding antenna */
    return true;
  }

  return false;
}

bool CalcCommManager::writeReference() {
  CalcRequestType_t request = Reference;
  
  if (writeAll(&request,sizeof(CalcRequestType_t)) ||
      writeAll(&reference_m,sizeof(CalcAntenna_t))) {
    return true;
  }

  if (getChildMessage() != Success) {
    /* Error writing reference */
    return true;
  }

  return false;
}

void CalcCommManager::updatePressureData
(std::map<std::string, DelayServerData>& antData) {
  CalcPressureData_t antennaPressure;

  std::map<std::string, DelayServerData>::iterator iter;
  for (iter=antData.begin(); iter != antData.end(); iter++) {
    strncpy(antennaPressure.antennaName, iter->first.c_str(), 4);
    antennaPressure.pressure = iter->second.pressure;
 
    if (writeAntennaPressure(antennaPressure)) {
      DelayServerExceptions::ReinitializationErrorExImpl ex(__FILE__,__LINE__,
                                                            __func__);
      throw ex;
    }
  }
}


bool CalcCommManager::writeAntennaPressure(CalcPressureData_t antennaPressure){
  CalcRequestType_t request = AntennaPressure;
  if (writeAll(&request,sizeof(CalcRequestType_t)) ||
      writeAll(&antennaPressure,sizeof(CalcPressureData_t))) {
    return true;
  }

  if (getChildMessage() != Success) {
    /* Error setting Antenna pressure */
    return true;
  }
  
  return false;
}

bool CalcCommManager::writePolynomialOrder() {
  CalcRequestType_t request = PolynomialOrder;

  if (writeAll(&request,sizeof(CalcRequestType_t)) ||
      writeAll(&polynomialOrder_m,sizeof(unsigned int))) {
    return true;
  }

  if (getChildMessage() != Success) {
    /* Error setting polynomial order */
    return true;
  }

  return false;
}

bool CalcCommManager::writeSourceList() {
  CalcRequestType_t request = Source;
  unsigned int      listSize = sourceList_m.size();

  if (writeAll(&request,sizeof(CalcRequestType_t)) ||
      writeAll(&listSize, sizeof(unsigned int)) ||
      writeAll(&sourceList_m[0],listSize * sizeof(CalcSource_t))) {
    return true;
  }


  if (getChildMessage() != Success) {
    /* Error writing source list */
    return true;
  }

  return false;
}

bool CalcCommManager::initializeCalcProcess() {
  CalcRequestType_t request = Initialize;
  if (writeAll(&request,sizeof(CalcRequestType_t)) ||
      writeAll(&todayMJD_m, sizeof(double))) {
    ACS_LOG(LM_SOURCE_INFO, __func__,
	    (LM_ERROR,"Communication failure initializing CALC Process"));
    return true;
  }

  if (getChildMessage() != Success) {
    /* Error initializing calc process */
    ACS_LOG(LM_SOURCE_INFO, __func__,
	    (LM_ERROR,"Error initializing CALC Process (usually IERS issues)"));
    
    return true;
  }
  return false;
}

bool CalcCommManager::writeAll(void* buf, size_t count){
  char* buffer = static_cast<char*>(buf);
  int   bytesToWrite = count;
  int   bytesWritten;
  int   ret;

  /* Ignore the SIGPIPE for getting the EPIPE error */
  signal(SIGPIPE, SIG_IGN);
  
  /* POLLOUT event must be captured */ 
  struct pollfd pfd[1];
  pfd[0].fd = outputFd_m;
  pfd[0].events = POLLOUT;
  
  while (bytesToWrite > 0) {
    /* POLL for checking that pipe is alive 
     * (8 sec for timeout) */
    ret = poll(pfd, 1, 8000);
    
    /* POLL returned error, timeout or a POLLHUP 
       event was captured.*/ 
    if (ret <= 0 || pfd[0].revents & (POLLHUP)) {
      switch (errno) {
      case EINTR:
	continue;
	break;
      default:
	return true; // Error
      }
    }

    /* Now it can be write safety */
    bytesWritten = write(outputFd_m, buffer, count);
    if (bytesWritten == -1) {
      /* Error on the write */
      switch (errno){
      case EINTR:
	continue;
	break;
      default: //EPIPE or other error received
	return true;
      }
    } else {
      bytesToWrite -= bytesWritten;
      buffer       += bytesWritten;
    }
  }
  return false;
}

bool CalcCommManager::readAll(void* buf, size_t count) {
  int    ret;
  int    bytesToRead = count;
  int    bytesRead;
  char*  buffer = static_cast<char*>(buf);

  /* POLLIN event must be captured */ 
  struct pollfd pfd[1];
  pfd[0].fd = inputFd_m;
  pfd[0].events = POLLIN;
  
  while (bytesToRead > 0) {
    /* POLL for checking that pipe is alive 
     * (8 sec for timeout) */
    ret = poll(pfd, 1, 8000);
    
    /* POLL returned error, timeout or a POLLHUP 
       event was captured.*/ 
    if (ret <= 0 || pfd[0].revents & (POLLHUP)) {
      switch (errno) {
      case EINTR:
	continue;
	break;
      default:
	return true; // Error
      }
    }
    
    /* Now it can be read safety */
    bytesRead = read(inputFd_m, buffer, bytesToRead); 
    if (bytesRead < 0) {
      /* Some sort of error */
      switch (errno) {
      case EINTR:
	continue;
	break;
      default:
	return true; // Error
      }
    } else {
      bytesToRead -= bytesRead;
      buffer += bytesRead;
    }
  }
  return false;
}


CalcStatusType_t CalcCommManager::getChildMessage(){
  CalcStatusType_t response;
  int              ret;

  /* POLLIN event must be captured */ 
  struct pollfd pfd[1];
  pfd[0].fd = inputFd_m;

  pfd[0].events = POLLIN | POLLRDHUP;
  
  while (true) {
    /* POLL for checking that pipe is alive 
     * (8 sec for timeout) */
    ret = poll(pfd, 1, 8000);
    if (ret == 0) {
      /* This is a timeout */
      return Timeout;
    }

    /* POLL returned error,  or a POLLHUP 
       event was captured.*/ 
    if (ret < 0 || pfd[0].revents & (POLLHUP)) {
      switch (errno) {
      case EINTR:
	break;
      default:
	return Error; // Error
      } 
    }
    
    /* Now it can be read safety */
    int bytesRead = read(inputFd_m, &response, sizeof(CalcStatusType_t));

    /* Read failed, check if it is EINTR problem */
    if (bytesRead < 0) {       
      switch (errno) {
      case EINTR:
	continue;
	break;
      default:
	return Error; // Error
      }
    }
    
    /* Everything was OK */
    break;
  }
  
  switch (response) {
  case Success:
    return Success;
    break;
  case Error:
    return Error;
    break;
  case Exit:
    return Exit;
    break;
  default:
    return Error;
  }
}
