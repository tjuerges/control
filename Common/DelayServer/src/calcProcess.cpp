/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2005
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* csmith  2007-06-30  created
* jkern   2007-10-04  Brought into operational system
*
*  This executable is the actual CALC process, it opens a Socket and 
*  is responsible for maintaing state.  It makes the actual calls to CALC
*  as necessary.
*/


static char *rcsId="@(#) $Id$";
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <calcPipeManager.h>

int main(int argc, char* argv[]) {
  if (argc != 3) {
    /* This is really bad, no way to even report the error */
    exit(-1);
  }

  int fdIn  = atoi(argv[1]);  // The communication streams are passed
  int fdOut = atoi(argv[2]);  // in as the first 2 arguments.

  CalcPipeManager pipeManager(fdIn, fdOut);
  
  if (CalcDataManager::setCalcEnvironment()) {    
    pipeManager.returnSuccess();
    pipeManager.serviceLoop();
  }

  pipeManager.returnExit();
  
  /* Now close the File Descriptors */
  close(fdIn);
  close(fdOut);
  exit(0);
}

