/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2007-10-15  created 
*/

/************************************************************************
*   NAME
*   
* 
*------------------------------------------------------------------------
*/


static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <calcPipeManager.h>
#include <calcCommType.h>



CalcPipeManager::CalcPipeManager(int inputStream, int outputStream) {
  inputFd_m  = inputStream;
  outputFd_m = outputStream;
  dataManager_m = new CalcDataManager();
}

/* This constructor is protected and only exists for testing purposes
   the derived class must define the input and output streams.
*/
CalcPipeManager::CalcPipeManager(CalcDataManager* dataMgr) {
  dataManager_m = dataMgr;
}
  
CalcPipeManager::~CalcPipeManager(){
  delete dataManager_m;
}

/* ----------- Data Management Calls --------------- */
void CalcPipeManager::initializeCalc() {
  double todayMJD;
  if (readAll(&todayMJD, sizeof(double))) {
    ACS_LOG(LM_SOURCE_INFO, __func__,
	    (LM_WARNING, "Failed reading todayMJD from communication manager"));
    returnError();
  } 
  
  if (dataManager_m->getIERSData(todayMJD)) {
    ACS_LOG(LM_SOURCE_INFO, __func__,
	    (LM_ERROR, "Failed to get IERS Data for specified day"));
    returnError();
  }
  returnSuccess();
}


void CalcPipeManager::addAntenna() {
  CalcAntenna_t newAntenna;

  if (readAll(&newAntenna, sizeof(CalcAntenna_t))) {
    ACS_LOG(LM_SOURCE_INFO, __func__,
	    (LM_WARNING, "Failed reading antenna from communication manager"));
    returnError();
  }
 
  //Try adding the antenna to the DataManager
  dataManager_m->addAntenna(newAntenna);

  returnSuccess();
}

void CalcPipeManager::setReference() {
  CalcAntenna_t newReference;

  if (readAll(&newReference, sizeof(CalcAntenna_t))) {
    ACS_LOG(LM_SOURCE_INFO, __func__,
	    (LM_WARNING, "Failed reading new reference from communication manager"));
    returnError();
  }
 
  //Try setting the reference to the DataManager
  dataManager_m->setReference(newReference);

  returnSuccess();
}

void CalcPipeManager::setAntennaPressure() {
  CalcPressureData_t newPressureData;

  if (readAll(&newPressureData, sizeof(CalcPressureData_t))) {
    ACS_LOG(LM_SOURCE_INFO, __func__,
	    (LM_WARNING, "Failed reading new reference from communication manager"));
    returnError();
  }
 
  //Try setting the new pressure on the DataManager
  dataManager_m->setAntennaPressure(newPressureData);

  returnSuccess();
}

void CalcPipeManager::setPolynomialOrder() {
  unsigned int newPolyOrder;

  if (readAll(&newPolyOrder, sizeof(unsigned int))) {
    ACS_LOG(LM_SOURCE_INFO, __func__,
	    (LM_WARNING, "Failed reading polynomial order from communication manager"));
    returnError();
  }
 
  //Try adding the antenna to the DataManager
  dataManager_m->setPolynomialOrder(newPolyOrder);
  
  returnSuccess();
}

void CalcPipeManager::setSource() {
  unsigned int  listSize;
  
  if (readAll(&listSize, sizeof(unsigned int))) {
    ACS_LOG(LM_SOURCE_INFO, __func__,
	    (LM_WARNING, "Failed reading source list size from communication manager"));
    returnError();
  }
  
  std::vector<CalcSource_t> sourceTable(listSize);
  
  if (readAll(&sourceTable[0], listSize * sizeof(CalcSource_t))) {
    ACS_LOG(LM_SOURCE_INFO, __func__,
	    (LM_WARNING, "Failed reading source list from communication manager"));
    returnError();
  }
  
  /* Try setting the table in the data manager */
  if (listSize == 1) {
    dataManager_m->setSource(sourceTable[0]);
  } else {
    try {
      dataManager_m->fillSourceTable(sourceTable);
    } catch (DelayServerExceptions::TableSizeErrorExImpl& ex) {
      ex.log();
      returnError();
    }  
  }
 
  returnSuccess();
}

void CalcPipeManager::getDelays() {
  unsigned int  listSize;

  if (readAll(&listSize, sizeof(unsigned int))) {
    ACS_LOG(LM_SOURCE_INFO, __func__,
	    (LM_WARNING, "Failed reading time list size from communication manager"));
    returnError();
  }
  
  std::vector<ACS::Time> timeList(listSize);
  
  if (readAll(&timeList[0], listSize * sizeof(ACS::Time))) {
    ACS_LOG(LM_SOURCE_INFO, __func__,
	    (LM_WARNING, "Failed reading time list from communication manager"));
    returnError();
  }

  // Fill Delay Table
  try {
    dataManager_m->fillDelayTable(timeList);
  } catch (DelayServerExceptions::TableSizeErrorExImpl& ex) {
    ex.log();
    returnError();
  }

  unsigned int delayTableSize = dataManager_m->getDelayTableSize();
  double*      delayTable     = dataManager_m->getDelayTable();
  double*      azTable        = dataManager_m->getAzTable();
  double*      elTable        = dataManager_m->getElTable();
  double*      dryAtmTable    = dataManager_m->getDryAtmTable();
  double*      wetAtmTable    = dataManager_m->getWetAtmTable();
  
  returnSuccess();
  
  if (writeAll(&delayTableSize, sizeof(unsigned int)) ||
      writeAll(delayTable, delayTableSize * sizeof(double)) ||
      writeAll(azTable, delayTableSize * sizeof(double)) || 
      writeAll(elTable, delayTableSize * sizeof(double))||
      writeAll(dryAtmTable, delayTableSize * sizeof(double)) ||
      writeAll(wetAtmTable, delayTableSize * sizeof(double))) {
    ACS_LOG(LM_SOURCE_INFO, __func__,
	    (LM_WARNING, "Failed sending delay table to communication manager"));
  }
}

/* --------------- Response Methods ------------------ */
void CalcPipeManager::returnSuccess() {
  
  /* This method just returns a SUCCESS Enumeration over the pipe */
  CalcStatusType_t state = Success;
  if (writeAll(reinterpret_cast<char*>(&state), sizeof(CalcStatusType_t))) {
    ACS_LOG(LM_SOURCE_INFO, __func__,
	    (LM_WARNING, "Failed writting Success state"));
  }
}

void CalcPipeManager::returnExit() {
  
  /* This method just returns a EXIT Enumeration over the pipe */
  CalcStatusType_t state = Exit;
  if (writeAll(reinterpret_cast<char*>(&state), sizeof(CalcStatusType_t))) {
    ACS_LOG(LM_SOURCE_INFO, __func__,
	    (LM_WARNING, "Failed writing Exit state"));
  }
}

void CalcPipeManager::returnError() {
  
  CalcStatusType_t state = Error;
  if (writeAll(reinterpret_cast<char*>(&state), sizeof(CalcStatusType_t))) {
    ACS_LOG(LM_SOURCE_INFO, __func__,
	    (LM_WARNING, "Failed writing Error state")); 
  }
}

/* ------------------ Data IO Methods ----------------- */
bool CalcPipeManager::writeAll(void* buf, size_t count) {
  char* buffer = static_cast<char*>(buf);
  int   bytesToWrite = count;
  int   bytesWritten;
  int   ret;
  
  /* Ignore the SIGPIPE for getting the EPIPE error */
  signal(SIGPIPE, SIG_IGN);
  
  /* POLLOUT event must be captured */ 
  struct pollfd pfd[1];
  pfd[0].fd = outputFd_m;
  pfd[0].events = POLLOUT;
  
  while (bytesToWrite > 0) {
    /* POLL for checking that pipe is alive 
     * (8 sec for timeout) */
    ret = poll(pfd, 1, 8000);
    
    /* POLL returned error, timeout or a POLLHUP 
       event was captured.*/ 
    if (ret <= 0 || pfd[0].revents & (POLLHUP)) {
      switch (errno) {
      case EINTR:
	continue;
	break;
      default:
	return true; // Error
      }
    }
    
    /* Now it can be write safety */
    bytesWritten = write(outputFd_m, buffer, count);
    if (bytesWritten == -1) {
      /* Error on the write */
      switch (errno){
      case EINTR:
	continue;
	break;
      default: //EPIPE or other error received 
	return true;
      }
    } else {
      bytesToWrite -= bytesWritten;
      buffer       += bytesWritten;
    }
  }
  return false;
}


/* This is the child side, if we timeout here rather than trying to
   resynch, just hang.  The parent should detect that we have hung
   and restart the child.
*/
bool CalcPipeManager::readAll(void* buf, size_t count) {
  int    ret;
  int    bytesToRead = count;
  int    bytesRead;
  char*  buffer = static_cast<char*>(buf);

  /* POLLIN event must be captured */ 
  struct pollfd pfd[1];
  pfd[0].fd = inputFd_m;
  pfd[0].events = POLLIN;
  
  while (bytesToRead > 0) {
    /* POLL for checking that pipe is alive 
     * For this case we have to wait for POLLIN
     * because the loop is always reading */
    ret = poll(pfd, 1, INFTIM);
    
    /* POLL returned error, POLLHUP 
       event was captured.*/ 
    if (ret < 0 || pfd[0].revents & (POLLHUP)) {
      switch (errno) {
      case EINTR:
	continue;
	break;
      default:
	return true; // Error
      }
    }
    
    /* Now it can be read safety */
    bytesRead = read(inputFd_m, buffer, bytesToRead); 
    if (bytesRead < 0) {
      /* Some sort of error */
      switch (errno) {
      case EINTR:
	continue;
	break;
      default:
	return true; // Error
      }
    } else {
      bytesToRead -= bytesRead;
      buffer += bytesRead;
    }
  }
  return false;
}


void CalcPipeManager::serviceLoop(){
  /* This is the main service loop of the Manager, it handles all incoming
     requests. */
  
  CalcRequestType_t requestType;

  while (!readAll(&requestType,sizeof(CalcRequestType_t))) {
    switch (requestType) {
    case ExitRequest:
      return;
      break;
    case Antenna:
      addAntenna();
      break;
    case Reference:
      setReference();
      break;
    case AntennaPressure:
      setAntennaPressure();
      break;
    case PolynomialOrder:
      setPolynomialOrder();
      break;
    case Source:
      setSource();
      break;
    case Delays:
      getDelays();
      break;
    case Initialize:
      initializeCalc();
      break;
    default:
      return; // Receive unknown request.
    }
  }
}

/*___oOo___*/
