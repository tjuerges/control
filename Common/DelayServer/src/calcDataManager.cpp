/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2007-10-06  created 
*/

/************************************************************************
*   NAME
* 
*------------------------------------------------------------------------
*/

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);


#include <calcDataManager.h>
#include <casacore/measures/Measures/MeasIERS.h>
#include <casacore/measures/Measures/MEpoch.h>
#include <casacore/measures/Measures/MeasConvert.h>
#include <casacore/measures/Measures/MCEpoch.h> // Needed to work around a bug in CASA-3.0.0
#include <acstimeEpochHelper.h>
#include <iomanip>
#include <acsutilFindFile.h>

// For some reason it appears that this works best as the last include
#include <calcExec.h>

using std::ostringstream;

getCALC_arg CalcDataManager::calcArgs_m;


CalcDataManager::CalcDataManager():
  delayTable_m(NULL),
  azTable_m(NULL),
  elTable_m(NULL),
  dryAtmTable_m(NULL),
  wetAtmTable_m(NULL),
  //uvwTable_m(NULL),
  polynomialOrder_m(4)
{
  /* Initialize the calcArgs with default behavior */

  /* Default Reference is the Center of the Earth */
  calcArgs_m.b_x = 0;
  calcArgs_m.b_y = 0;
  calcArgs_m.b_z = 0;
  calcArgs_m.axis_off_b = 0;


  //Fill out remaining CALC arguments
  calcArgs_m.pressure_a = 0.0;
  calcArgs_m.pressure_b = 0.0;
  calcArgs_m.station_a = strdup("Reference");
  calcArgs_m.station_b = strdup("Antenna");
  calcArgs_m.axis_type_a = strdup("ALTZ");
  calcArgs_m.axis_type_b = strdup("ALTZ");
  calcArgs_m.source = strdup("Source");
  calcArgs_m.ref_frame = 0;
  calcArgs_m.request_id = 0;
	
  calcArgs_m.kflags[0] = -1; // Use VLBA default setup for CALC.
  for(int i = 1; i < 64; i++){
    calcArgs_m.kflags[i] = 0;//Initialize the flags to zero
  }
}

CalcDataManager::~CalcDataManager(){

  if (delayTable_m != NULL) {
    delete[] delayTable_m;
    delayTable_m = NULL;
  }

  if (delayTable_m != NULL) {
    delete[] azTable_m;
    azTable_m = NULL;
  }

  if (elTable_m != NULL) {
    delete[] elTable_m;
    elTable_m = NULL;
  }

  if (dryAtmTable_m != NULL) {
    delete[] dryAtmTable_m;
    dryAtmTable_m = NULL;
  }

  if (wetAtmTable_m != NULL) {
    delete[] wetAtmTable_m;
    wetAtmTable_m = NULL;
  }

}

bool CalcDataManager::setCalcEnvironment() {
  const string fnName = "setCalcEnvironment()";
  
  /* Set Environment variable to default value for CALC */
   setenv("CALC_USER","C",1); // Set for Correlator usage
   setenv("WET_ATM","N",1);   // By default turn off the Wet Atmosphere

   // Set the environment for the JPL Ephemeris File
   {
     char filePath[512];
     char mode[128];
     int  size;
     char dirFlag;
    
     if (!acsFindFile("config/JPLEPH", filePath, mode, &size, &dirFlag)) {
       // Send some sort of error message and exit
       ACS_LOG(LM_SOURCE_INFO, fnName.c_str(),
	       (LM_ERROR, "Unable to find JPL Ephemeris File... Exiting"));
       return false;
     }
     ACS_LOG(LM_SOURCE_INFO, fnName.c_str(),
             (LM_INFO, "Setting the JPL Ephemeris file to: %s", filePath));
     setenv("JPLEPH",filePath,1);
   }

   return true;
}  

void CalcDataManager::addAntenna(const CalcAntenna_t& newAntenna){
  bool antennaExist = false;
  
  /* Search if antenna exists and update position */
  for (unsigned int antIdx = 0; antIdx < antennaList_m.size(); antIdx++) { 
    if (!strncmp(newAntenna.antennaName,antennaList_m[antIdx].antennaName,4)) {
      antennaList_m[antIdx].x = newAntenna.x;
      antennaList_m[antIdx].y = newAntenna.y;
      antennaList_m[antIdx].z = newAntenna.z;
      antennaList_m[antIdx].axisOffset = newAntenna.axisOffset;
      antennaExist = true;
    } 
  }
  
  if(!antennaExist) {
     antennaList_m.push_back(newAntenna);
     pressure_m.push_back(0);
  }
  allocateDelayTable();
}

void CalcDataManager::setAntennaPressure(const CalcPressureData_t& newPressure){
  /* The input pressure should be in Pascals, but Calc wants hPa (or millibars)
     so divide by 100 */
  for (unsigned int antIdx = 0; antIdx < antennaList_m.size(); antIdx++) { 
    if (!strncmp(newPressure.antennaName,antennaList_m[antIdx].antennaName,4)){
      pressure_m[antIdx] = newPressure.pressure/100.0;
      return;
    }
  }
}

void CalcDataManager::setPolynomialOrder(const unsigned int& newOrder){
  if (newOrder != polynomialOrder_m) {
    polynomialOrder_m = newOrder;
    allocateDelayTable();
  }
}

bool CalcDataManager::getIERSData(double mjdToday) {
  /* 
     What we want to do is get 5 EOP parameteters. And store them in
     the CalcArgs structure.  According to J. Benson we should make 
     today (the passed in argument) day-2 of 5.

     Method returns true when there is an error getting data
  */
  for (int idx = 0; idx < 5; idx++ ){
    /* For some reason CASA does not have a simple way to get tai_utc
       use the utility routine */
    calcArgs_m.tai_utc[idx] = findTAI2UTC();
    try {
      casa::Bool ok = casa::MeasIERS::get(calcArgs_m.EOP_time[idx],
                          casa::MeasIERS::PREDICTED, casa::MeasIERS::MJD,
                          mjdToday - 1.0 + idx);
      ok = ok && casa::MeasIERS::get(calcArgs_m.ut1_utc[idx], 
                          casa::MeasIERS::PREDICTED, casa::MeasIERS::dUT1,
                          calcArgs_m.EOP_time[idx]);
      ok = ok && casa::MeasIERS::get(calcArgs_m.xpole[idx], 
                          casa::MeasIERS::PREDICTED, casa::MeasIERS::X, 
                          calcArgs_m.EOP_time[idx]);
      ok = ok && casa::MeasIERS::get(calcArgs_m.ypole[idx],
                          casa::MeasIERS::PREDICTED, casa::MeasIERS::Y, 
                          calcArgs_m.EOP_time[idx]);
      if (!ok) throw casa::AipsError("IERS tables are out of date.");
    } catch (casa::AipsError ex) {
      for (int day = 0; day < 5; day++ ){
        calcArgs_m.EOP_time[day] = mjdToday - 1.0 + day;
        calcArgs_m.ut1_utc[day]  = 0.0;
        calcArgs_m.xpole[day]    = 0.0;
        calcArgs_m.ypole[day]    = 0.0;
      }
      std::ostringstream o;
      o << "Failed getting IERS Data for requested mjd:" << mjdToday;
      ACS_LOG(LM_SOURCE_INFO, "CalcDataManager::getIERSData",
                (LM_WARNING, o.str().c_str()));
      return true;
    }
  }
  return false;
}

double CalcDataManager::findTAI2UTC() {
  /* This is from Ralph, it's a work around for the fact that Casa doesn't
     have the routine.
  */
  const double mjd =
    EpochHelper(TimeUtil::ace2epoch(ACE_OS::gettimeofday())).toUTCdate(0,0);
  casa::MEpoch mep(casa::MVEpoch(casa::Quantity(mjd, "d")),
		   casa::MEpoch::TAI);
  const double inTAI = mep.getValue().getTime("s").getValue();
  mep = casa::MeasConvert<casa::MEpoch>(mep, casa::MEpoch::UTC)();
  const double inUTC = mep.getValue().getTime("s").getValue();
  return inTAI - inUTC;
}

void CalcDataManager::setReference(const CalcAntenna_t& newReference) {
  /* Station B is always the reference station*/
  calcArgs_m.a_x = newReference.x;
  calcArgs_m.a_y = newReference.y;
  calcArgs_m.a_z = newReference.z;
  calcArgs_m.axis_off_a = newReference.axisOffset;
}

void CalcDataManager::
fillSourceTable(const std::vector<CalcSource_t>& newSourceTable) {
  const string fnName = "CalcDataManager::fillSourceTable";

  if (newSourceTable.size() != polynomialOrder_m+1){
    ostringstream msg;
    DelayServerExceptions::TableSizeErrorExImpl 
      ex(__FILE__,__LINE__, fnName.c_str());
    msg <<"Source table size:" << newSourceTable.size()
	<<" is not equal to polynomial order + 1:" << polynomialOrder_m + 1; 
    ex.addData("Details", msg.str());
    throw ex;
  }
  sourceTable_m.clear();
  sourceTable_m = newSourceTable;
}

void CalcDataManager::setSource(const CalcSource_t& newSource){
  std::vector<CalcSource_t> newSourceTable;
  for (unsigned int idx = 0; idx <= polynomialOrder_m; idx++) {
    newSourceTable.push_back(newSource);
  }
  fillSourceTable(newSourceTable);
}

void CalcDataManager::allocateDelayTable() {
  delete[] delayTable_m;
  delete[] azTable_m;
  delete[] elTable_m;
  delete[] wetAtmTable_m;
  delete[] dryAtmTable_m;


  delayTable_m = new double[(polynomialOrder_m + 1) * antennaList_m.size()];
  azTable_m = new double[(polynomialOrder_m + 1) * antennaList_m.size()];
  elTable_m = new double[(polynomialOrder_m + 1) * antennaList_m.size()];
  wetAtmTable_m = new double[(polynomialOrder_m + 1) * antennaList_m.size()];
  dryAtmTable_m = new double[(polynomialOrder_m + 1) * antennaList_m.size()];
}

double* CalcDataManager::getDelayTable(){
  return delayTable_m;
}

double* CalcDataManager::getAzTable(){
  return azTable_m;
}

double* CalcDataManager::getElTable(){
  return elTable_m;
}

double* CalcDataManager::getDryAtmTable(){
  return dryAtmTable_m;
}

double* CalcDataManager::getWetAtmTable(){
  return wetAtmTable_m;
}

int CalcDataManager::getDelayTableSize(){
  return (polynomialOrder_m + 1) * antennaList_m.size();
}

void CalcDataManager::fillDelayTable(const std::vector<ACS::Time>& timeList){
  const string fnName = "CalcDataManager::fillDelayTable";
 
  if (timeList.size() != polynomialOrder_m + 1) {
    ostringstream msg;
    DelayServerExceptions::TableSizeErrorExImpl 
      ex(__FILE__,__LINE__, fnName.c_str());
    msg <<"Time list size:" << timeList.size()
	<<" is not equal to polynomial order + 1:" << polynomialOrder_m + 1;   
    ex.addData("Details", msg.str());
    throw ex;
  }


  if (sourceTable_m.size() != polynomialOrder_m + 1) {
    ostringstream msg;
    DelayServerExceptions::TableSizeErrorExImpl 
      ex(__FILE__,__LINE__,fnName.c_str());
    msg <<"Source table size:" << sourceTable_m.size()
	<<" is not equal to polynomial order + 1:" << polynomialOrder_m + 1; 
    ex.addData("Details", msg.str());
    ex.log();
    throw ex;
  }

  CALCRecord calcResult;

  for (unsigned int timeIdx = 0; timeIdx < timeList.size(); timeIdx++){

    /* Put the correct time in the Arguments */
    EpochHelper targetTime(timeList[timeIdx]);
    double mjd;
    calcArgs_m.time = modf(targetTime.toUTCdate(0,0), &mjd);
    calcArgs_m.date = static_cast<long>(mjd);
    
    /* Put in the correct source */
    calcArgs_m.ra = sourceTable_m[timeIdx].RA;
    calcArgs_m.dec = sourceTable_m[timeIdx].Dec;
    calcArgs_m.dra = sourceTable_m[timeIdx].dRA;
    calcArgs_m.ddec = sourceTable_m[timeIdx].dDec;
    calcArgs_m.depoch = sourceTable_m[timeIdx].Epoch;
    calcArgs_m.parallax = sourceTable_m[timeIdx].Parallax;

    /* Now loop over the antennas */
    for (unsigned int antIdx = 0; antIdx < antennaList_m.size(); antIdx++) {
      /* Put the antenna information in the calcArg structure */
      calcArgs_m.b_x = antennaList_m[antIdx].x;
      calcArgs_m.b_y = antennaList_m[antIdx].y;
      calcArgs_m.b_z = antennaList_m[antIdx].z;
      calcArgs_m.axis_off_b = antennaList_m[antIdx].axisOffset;
      calcArgs_m.pressure_b = pressure_m[antIdx];

      calcExec(&calcArgs_m, &calcResult);
      // Copy delay, az, el and uvw to delay tables
      delayTable_m[calcDelayIndex(antIdx,timeIdx)] = 
        calcResult.delay[0] - calcResult.dry_atmos[1]; ;
      azTable_m[calcDelayIndex(antIdx,timeIdx)] = calcResult.az[0];
      elTable_m[calcDelayIndex(antIdx,timeIdx)] = calcResult.el[0];
      dryAtmTable_m[calcDelayIndex(antIdx,timeIdx)] = calcResult.dry_atmos[1];
      wetAtmTable_m[calcDelayIndex(antIdx,timeIdx)] = calcResult.wet_atmos[1];
    } // End Antenna Loop
  } // End Time Loop 
}

void CalcDataManager::printCalcData(){
  
  /* Print the IERS data Portion */
  std::cout << "IERS Data:" << std::endl;
  std::cout << "\t MJD        tai_utc   ut1_utc       xpole       ypole" << std::endl;
  for (int idx = 0; idx < 5; idx++) {
    std::cout << "\t" 
	      << std::setw(5)  << calcArgs_m.EOP_time[idx] << "    "
	      << std::setw(7)  << calcArgs_m.tai_utc[idx] << "     " 
	      << std::setw(8) << calcArgs_m.ut1_utc[idx] << "    "
	      << std::setw(8) << calcArgs_m.xpole[idx] << "    " 
	      << std::setw(8) << calcArgs_m.ypole[idx] << std::endl;
  }
  std::cout << "\nReference Location: " 
	    "X: " << calcArgs_m.b_x << "  " 
	    "Y: " << calcArgs_m.b_y << "  "
	    "Z: " << calcArgs_m.b_z << "  "
	    "K: " << calcArgs_m.axis_off_b << std::endl;

  std::cout << antennaList_m.size() << " Antennas found:" << std::endl;
  std::vector<CalcAntenna_t>::iterator antIter;
  for (antIter = antennaList_m.begin(); antIter != antennaList_m.end();
       antIter++) {
    std::cout << "\t" 
	      << "X: " << antIter->x << "  "
	      << "Y: " << antIter->y << "  "
	      << "Z: " << antIter->z << "  "
	      << "K: " << antIter->axisOffset << std::endl;
  }

  /* Print out the Source Table */
  std::cout << "\n" << "Source Table: (" << sourceTable_m.size() 
	    << " elements)" << std::endl;
  std::cout << "\t" << "  RA   " << "    Dec    " 
	    << "    dRA    " << "    dDec    " 
	    << "    Epoch  " <<  "  Parallax  " << std::endl;

  std::vector<CalcSource_t>::iterator sourceIter;
  for (sourceIter = sourceTable_m.begin(); 
       sourceIter != sourceTable_m.end();
       sourceIter++) {
    std::cout << "\t " 
	      << std::setw(6)  << sourceIter->RA << "  "
	      << std::setw(6)  << sourceIter->Dec << "  "
	      << std::setw(10) << sourceIter->dRA << "   "
	      << std::setw(10) << sourceIter->dDec << "  "
	      << std::setw(8) << sourceIter->Epoch << "  "
	      << std::setw(8) << sourceIter->Parallax << std::endl;
  }
}

void CalcDataManager::printCalcArgs() {
  std::cout << "Calc Arguments: " << std::endl;
  std::cout << "\trequest_id: " << calcArgs_m.request_id << std::endl;
  std::cout << "\tdate: " << calcArgs_m.date << std::endl;
  std::cout << "\tref_frame: " << calcArgs_m.ref_frame << std::endl;
  //std::cout << "\tdummy: " << calcArgs_m.dummy << std::endl;
  //std::cout << "\tint kflags[64]: " << calcArgs_m.int kflags[64] << std::endl;

  std::cout << "\ttime: " << calcArgs_m.time << std::endl;

  std::cout << "\ta_x: " << calcArgs_m.a_x << std::endl;
  std::cout << "\ta_y: " << calcArgs_m.a_y << std::endl;
  std::cout << "\ta_z: " << calcArgs_m.a_z << std::endl;
  std::cout << "\taxis_off_a: " << calcArgs_m.axis_off_a << std::endl;
  std::cout << "\tb_x: " << calcArgs_m.b_x << std::endl;
  std::cout << "\tb_y: " << calcArgs_m.b_y << std::endl;
  std::cout << "\tb_z: " << calcArgs_m.b_z << std::endl;
  std::cout << "\taxis_off_b: " << calcArgs_m.axis_off_b << std::endl;
  std::cout << "\tra: " << calcArgs_m.ra << std::endl;
  std::cout << "\tdec: " << calcArgs_m.dec << std::endl;
  std::cout << "\tdra: " << calcArgs_m.dra << std::endl;
  std::cout << "\tddec: " << calcArgs_m.ddec << std::endl;
  std::cout << "\tdepoch: " << calcArgs_m.depoch << std::endl;
  std::cout << "\tparallax: " << calcArgs_m.parallax << std::endl;

  std::cout << "\tIERS Data:" << std::endl;
  std::cout << "\t\t MJD        tai_utc   ut1_utc       xpole       ypole" << std::endl;
  for (int idx = 0; idx < 5; idx++) {
    std::cout << "\t\t" 
	      << std::setw(5)  << calcArgs_m.EOP_time[idx] << "    "
	      << std::setw(7)  << calcArgs_m.tai_utc[idx] << "     " 
	      << std::setw(8) << calcArgs_m.ut1_utc[idx] << "    "
	      << std::setw(8) << calcArgs_m.xpole[idx] << "    " 
	      << std::setw(8) << calcArgs_m.ypole[idx] << std::endl;
  }

  std::cout << "\tpressure_a: " << calcArgs_m.pressure_a << std::endl;
  std::cout << "\tpressure_b: " << calcArgs_m.pressure_b << std::endl;
  std::cout << "\tstation_a: " << calcArgs_m.station_a << std::endl;
  std::cout << "\taxis_type_a: " << calcArgs_m.axis_type_a << std::endl;
  std::cout << "\tstation_b: " << calcArgs_m.station_b << std::endl;
  std::cout << "\taxis_type_b: " << calcArgs_m.axis_type_b << std::endl;
  std::cout << "\tsource: " << calcArgs_m.source << std::endl;
}
