/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* rsoto  2008-08-25  created 
*/

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <atmosphericDelayModel.h>
#include <logging.h>
using Control::AtmosphericDelayModelType;
using Control::ZERO_ATMOSPHERE_MODEL;
using Control::CALC_DRY_ONLY;
using Control::CALC_WET_AND_DRY;
using Control::ALMA_DRY_ONLY;
using Control::ALMA_WET_AND_DRY;

AtmosphericDelayModel::AtmosphericDelayModel(AtmosphericDelayModelType type):
  modelType(type){};


AtmosphericZeroDelayModel::AtmosphericZeroDelayModel():
  AtmosphericDelayModel(ZERO_ATMOSPHERE_MODEL){}

bool AtmosphericZeroDelayModel::calculateAtmosphericModel
      (std::map<std::string, DelayServerData>& antennaData) {

  std::map<std::string, DelayServerData>::iterator iter;
  
  for (iter = antennaData.begin(); iter != antennaData.end(); iter++) {
    iter->second.zeroDryComponent();
    iter->second.zeroWetComponent();
  }
  return true;
}

/*-----------------------------------------------------*/
CALCDryAtmosphereModel::CALCDryAtmosphereModel():
  AtmosphericDelayModel(CALC_DRY_ONLY){}

bool CALCDryAtmosphereModel::calculateAtmosphericModel
(std::map<std::string, DelayServerData>& antennaData) {

  std::map<std::string, DelayServerData>::iterator iter;
  for (iter = antennaData.begin(); iter != antennaData.end(); iter++) {
    iter->second.zeroWetComponent();
  }
  return true;
}

/*-----------------------------------------------------*/
CALCWetAtmosphereModel::CALCWetAtmosphereModel():
  AtmosphericDelayModel(CALC_WET_AND_DRY) {}

bool CALCWetAtmosphereModel::calculateAtmosphericModel
(std::map<std::string, DelayServerData>& antennaData) {
  return true;
}

/*-----------------------------------------------------*/
ALMADryAtmosphereModel::ALMADryAtmosphereModel():
  AtmosphericDelayModel(ALMA_DRY_ONLY){}

bool ALMADryAtmosphereModel::calculateAtmosphericModel
(std::map<std::string, DelayServerData>& antennaData) {

  const ACS::Time currentTime = ::getTimeStamp();

  std::map<std::string, DelayServerData>::iterator iter;
  for (iter = antennaData.begin(); iter != antennaData.end(); iter++) {
    iter->second.applyZenithDelayValue(currentTime);
    iter->second.zeroWetComponent();
  }
  return true;
}

/*-----------------------------------------------------*/
ALMAWetAtmosphereModel::ALMAWetAtmosphereModel():
  AtmosphericDelayModel(ALMA_WET_AND_DRY){}

bool ALMAWetAtmosphereModel::calculateAtmosphericModel
(std::map<std::string, DelayServerData>& antennaData) {

  const ACS::Time currentTime = ::getTimeStamp();

  std::map<std::string, DelayServerData>::iterator iter;
  for (iter = antennaData.begin(); iter != antennaData.end(); iter++) {
    iter->second.applyZenithDelayValue(currentTime);
  }
  return true;
}

/*-----------------------------------------------------*/
