/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2006-05-04  created
*/

/************************************************************************
 * The simple delay server is designed to facilite testing in the PSIL
 * and perhaps at the ATF.  It allows for simple control of the delays
 * going to each antenna.
 *----------------------------------------------------------------------
 */

#ifndef SIMPLEDELAYSERVER_IDL
#define SIMPLEDELAYSERVER_IDL

#include <DelayServerBase.idl>
#pragma prefix "alma"


module Control {
  interface SimpleDelayServer : Control::DelayServerBase {

    /**
       This is a squence of the polynomial coeficents in decending order
       that is the sequence [a1, a2, a3] coresponds to the polynomial
       a1*t^2 + a2*t + a3
    */
    typedef sequence<double>  Coefficients; 
    struct DelayPolynomial {
      ACS::Time    referenceTime; /*!< The zero point of the polynomial time */
      Coefficients coeffs;
    }; 

    void setAntennaDelay(in Control::AntennaId antennaIdentifier,
			 in DelayPolynomial    antDelayPolynomial);


  };
};




#endif //SIMPLEDELAYSERVER_IDL
