#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef BareAmbDeviceInt_h
#define BareAmbDeviceInt_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges May 11, 2009  created
//


#include <ambDeviceInt.h>
#include <ambInterface.h>


class BareAMBDeviceInt: public AmbDeviceInt
{
    public:
    union holder
    {
        char Char[8];
        unsigned short UShort[4];
        short Short[4];
        unsigned int UInt[2];
        int Int[2];
        unsigned long ULong[2];
        long Long[2];
        unsigned long long ULLong;
        long long LLong;
        float Float[2];
        double Double;
        AmbDataMem_t Raw[8];
    };

    struct ICDPoint
    {
        AmbRelativeAddr rca;
        AmbDataLength_t dataLength;
        union holder data;
        ACS::Time timestamp;
        AmbErrorCode_t status;
    };

    struct RequestStruct
    {
        AmbRelativeAddr RCA;
        ACS::Time TargetTime;
        AmbDataLength_t DataLength;
        union holder data;
        ACS::Time Timestamp;
        AmbErrorCode_t Status;
    };

    BareAMBDeviceInt():
        AmbDeviceInt()
    {
    };

    virtual ~BareAMBDeviceInt()
    {
        AmbInterface::deleteInstance();
    };

    virtual bool initialize()
    {
        try
        {
            AmbDeviceInt::interface_mp = AmbInterface::getInstance();
        }
        catch(const ControlExceptions::CAMBErrorExImpl& ex)
        {
            std::cerr << "Unable to get an instance of AmbInterface.\n"
                << "Did you load the kernel modules?\n";
            return false;
        }

        return true;
    };

    virtual bool initialize(AmbChannel AMBChannel, AmbNodeAddr nodeID)
    {
        AmbDeviceInt::channel_m = AMBChannel;
        AmbDeviceInt::nodeAddress_m = nodeID;

        return BareAMBDeviceInt::initialize();
    };

    void setChannel(AmbChannel AMBChannel)
    {
        AmbDeviceInt::channel_m = AMBChannel;
    };

    void setNodeAddress(AmbNodeAddr nodeID)
    {
        AmbDeviceInt::nodeAddress_m = nodeID;
    };

    void sendMonitor(ICDPoint& point)
    {
        sem_t synch;

        sem_init(&synch, 0, 0);
        monitor(point.rca, point.dataLength, point.data.Raw, &synch,
            &point.timestamp, &point.status);
        sem_wait(&synch);
        sem_destroy(&synch);
    };

    void sendMonitorTE(ACS::Time TimeEvent, ICDPoint& point)
    {
        sem_t synch;

        sem_init(&synch, 0, 0);
        monitorTE(TimeEvent, point.rca, point.dataLength, point.data.Raw,
            &synch, &point.timestamp, &point.status);
        sem_wait(&synch);
        sem_destroy(&synch);
    };

    void sendMonitorNextTE(ICDPoint& point)
    {
        sem_t synch;

        sem_init(&synch, 0, 0);
        monitorNextTE(point.rca, point.dataLength, point.data.Raw, &synch,
            &point.timestamp, &point.status);
        sem_wait(&synch);
        sem_destroy(&synch);
    };

    void sendControl(ICDPoint& point)
    {
        sem_t synch;

        sem_init(&synch, 0, 0);
        command(point.rca, point.dataLength, point.data.Raw, &synch,
            &point.timestamp, &point.status);
        sem_wait(&synch);
        sem_destroy(&synch);
    };

    void sendControlTE(ACS::Time TimeEvent, ICDPoint& point)
    {
        sem_t synch;

        sem_init(&synch, 0, 0);
        commandTE(TimeEvent, point.rca, point.dataLength, point.data.Raw,
            &synch, &point.timestamp, &point.status);
        sem_wait(&synch);
        sem_destroy(&synch);
    };

    void sendControlNextTE(ICDPoint& point)
    {
        sem_t synch;

        sem_init(&synch, 0, 0);
        commandNextTE(point.rca, point.dataLength, point.data.Raw, &synch,
            &point.timestamp, &point.status);
        sem_wait(&synch);
        sem_destroy(&synch);
    };

    /// The AmbDevieInt class does not allow to send "raw" messages. So
    /// I circumvent this limitation be declaring the sendMessage method
    /// here.
    /// \exception ControlExceptions::CAMBErrorExImpl
    void sendMessage(const AmbMessage_t& msg) const
    {
        AmbDeviceInt::interface_mp->sendMessage(msg);
    };
};
#endif
