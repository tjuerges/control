#ifndef AMBDEVICEINT_H
#define AMBDEVICEINT_H
/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * $Id$
 *
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

/************************************************************************
 * This is the baseclass for device drivers.  It defines the class AmbDeviceInt
 * which provides methods for monitor and control interactions with the Amb
 * as well as providing the interface which AmbDevIO uses.  This is a pure
 * virtual class, as there is no way to specify the channel and address
 * thus the constructor is protected.
 *----------------------------------------------------------------------
 */


#include "ambDeviceS.h"
#include <ambDefs.h>
#include <ambServerC.h>


// Forward declaration.
class AmbInterface;


class AmbDeviceInt
{
    public:
    virtual void monitor(AmbRelativeAddr RCA, AmbDataLength_t& dataLength,
        AmbDataMem_t* data, sem_t* synchLock, Time* timestamp,
        AmbErrorCode_t* status);

    virtual void command(AmbRelativeAddr RCA, AmbDataLength_t dataLength,
        const AmbDataMem_t* data, sem_t* synchLock, Time* timestamp,
        AmbErrorCode_t* status);

    virtual void monitorTE(ACS::Time TimeEvent, AmbRelativeAddr RCA,
        AmbDataLength_t& dataLength, AmbDataMem_t* data, sem_t* synchLock,
        Time* timestamp, AmbErrorCode_t* status);

    virtual void commandTE(ACS::Time TimeEvent, AmbRelativeAddr RCA,
        AmbDataLength_t dataLength, const AmbDataMem_t* data, sem_t* synchLock,
        Time* timestamp, AmbErrorCode_t* status);

    virtual void monitorNextTE(AmbRelativeAddr RCA,
        AmbDataLength_t& dataLength, AmbDataMem_t* data, sem_t* synchLock,
        Time* timestamp, AmbErrorCode_t* status);

    virtual void commandNextTE(AMBSystem::AmbRelativeAddr RCA,
        AmbDataLength_t dataLength, const AmbDataMem_t* data, sem_t* synchLock,
        ACS::Time* timestamp, AmbErrorCode_t* status);

    virtual void flushNode(ACS::Time TimeEvent, ACS::Time* timestamp,
        AmbErrorCode_t* status);

    virtual void flushRCA(ACS::Time TimeEvent, AmbRelativeAddr RCA,
        ACS::Time* timestamp, AmbErrorCode_t* status);


    protected:
    AmbDeviceInt();
    virtual ~AmbDeviceInt();

    AmbChannel channel_m;
    AmbNodeAddr nodeAddress_m;

    const AmbInterface* interface_mp;
};
#endif
