#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef ambDeviceImpl_h
#define ambDeviceImpl_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <hardwareDeviceImpl.h>
#include <ambCDBAccess.h>
#include <ambDeviceInt.h>
#include <ControlExceptions.h>
// For ACS::Time
#include <acscommonC.h>
// For the AmbInterface class.
#include <ambInterface.h>


namespace maci
{
    class ContainerServies;
};


/// This is the base class for device drivers.  It defines the class AmbDevice
/// which provides methods for monitor and control interactions with the Amb
/// as well as providing the interface which AmbDevIO uses, and the default
/// set of properties and methods for every device.
///
/// \note the CharacteristicComponentImpl and ControlXmlParser here should
/// really be virtual, but in order to ensure correct instanciation I have
/// ignored that fact.
class AmbDeviceImpl: public Control::HardwareDeviceImpl,
    public AmbCDBAccessor,
    public AmbDeviceInt
{
    public:
    /// Constructor used by all components.
    AmbDeviceImpl(const ACE_CString& name,
        maci::ContainerServices* containerServices);

    /// Destructor.
    virtual ~AmbDeviceImpl();

    // --------------------- Lifecycle Interface -------------------------
    /// ACS Lifecycle method
    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void initialize();

    /// ACS Lifecycle method
    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void cleanUp();

    /// CORBA wrapper for \ref getChannelNumber.
    virtual CORBA::ULong GET_CHANNEL_NUMBER();

    /// CORBA wrapper for \ref getNodeAddress.
    virtual CORBA::ULong GET_NODE_ADDRESS();

    /// CORBA wrapper for \ref getBaseAddress.
    virtual CORBA::ULong GET_BASE_ADDRESS();

    /// CORBA wrapper for \ref setChannelNumber.
    virtual void SET_CHANNEL_NUMBER(CORBA::ULong);

    /// CORBA wrapper for \ref setNodeAddress.
    virtual void SET_NODE_ADDRESS(CORBA::ULong);

    /// CORBA wrapper for \ref setBaseAddress.
    virtual void SET_BASE_ADDRESS(CORBA::ULong);


    protected:
    /// TimeThread keeps track of the current time for monitor and control
    /// requests.  This replaces the many many gettimeofday calls which would
    /// have been made to check every incoming control or monitor request for
    /// being still in the same TE.  gettimeofday is per se not too expensive
    /// anymore since the advent of rdtsc on Intel/AMD architectures but it is
    /// still about 30 times slower than on PowerPC CPUs.  On a Pentium-M the
    /// call takes about 5-6 �s and the amount of incoming requests adds up.
    /// The \ref runLoop function will be executed every TE and updates
    /// \ref current which is a reference that is passed in during construction
    /// and is stored in the parent class \ref AmbDeviceImpl.
    class TimeThread: public ACS::Thread
    {
        public:
        /// Constructor.
        TimeThread(const ACE_CString& name, ACS::Time& time);

        /// Destructor.
        virtual ~TimeThread();

        /// Time keeper function.  Executed every TE.  Executes gettimeofday and
        /// stores the value in \ref current.
        virtual void runLoop();

        protected:
        /// Reference to the real variable in \ref AmbDeviceImpl.
        ACS::Time& current;
    };

    /// Control life cycle method.
    virtual void hwStartAction();

    /// Control life cycle method.
    virtual void hwConfigureAction();

    /// Control life cycle method.
    virtual void hwInitializeAction();

    /// Control life cycle method.
    virtual void hwOperationalAction();

    /// Control life cycle method.
    virtual void hwStopAction();

    /// Instanciates an \ref AmbInterface, which is the underlying class that
    /// handles the hardware communication.
    virtual void getAmbInterfaceInstance();

    /// Method which returns the unique Id of the device
    /// this works by accessing the CAN Address at 0x0 on the device
    /// \exception ControlExceptions::CAMBErrorExImpl
    virtual void getDeviceUniqueId(std::string& deviceID);

    /// Method which returns the unique Id of the device
    /// this works by using the broadcast ID response of the AMB.
    /// Work around for old AMBSI-1s which don't support the ID reponse
    /// \exception ControlExceptions::CAMBErrorExImpl
    virtual void broadcastBasedDeviceUniqueID(std::string& deviceID);

    /// \exception ControlExceptions::CAMBErrorExImpl
    virtual void monitorEnc(ACS::Time* timestamp, const AmbRelativeAddr& rca,
        AmbDataLength_t& length, AmbDataMem_t* rawBytes);

    /// \exception ControlExceptions::CAMBErrorExImpl)
    virtual void commandEnc(const ACS::Time& cmdTime,
        const AmbRelativeAddr& rca, const AmbDataLength_t& length,
        const AmbDataMem_t* data);

    /// Set the channel number for the AMB-device.
    virtual void setChannelNumber(const unsigned long arg);

    /// Set the node address for the AMB-device.
    virtual void setNodeAddress(const unsigned long arg);

    /// Set the base address for the AMB-device.
    virtual void setBaseAddress(const unsigned long arg);

    /// Get the channel number of the AMB-device.
    virtual unsigned long getChannelNumber() const;

    /// Get the node address of the AMB-device.
    virtual unsigned long getNodeAddress() const;

    /// Get the base address of the AMB-device.
    virtual unsigned long getBaseAddress() const;

    /// Convert a temperature vector which has been read from a Dallas DS1820.
    /// This is called from \ref rawToWorldAmbientTemperature.
    virtual float ds1820Temp(const u_int8_t temp, const int8_t sign,
        const u_int8_t countRem, const u_int8_t countPerC) const;

    /// Do the raw conversion from bytes to Kelvin.
    virtual float rawToWorldAmbientTemperature(
        const std::vector< unsigned char >& raw);

    /// Storage for the base address.
    unsigned long baseAddress_m;

    /// Time keeper thread.
    TimeThread* timeThread_p;

    /// Contains the time stamp of the current TE.
    ACS::Time currentTime;
};
#endif
