/* @(#) $Id$
 *
 * Copyright (C) 2002
 * Associated Universities, Inc. Washington DC, USA.
 *
 * Produced for the ALMA project
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
 *
 * Correspondence concerning ALMA should be addressed as follows:
 *
 *        Internet email: alma-sw-admin@alma.org
 */

#ifndef AMBDEVIOTEST_H
#define AMBDEVIOTEST_H

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

//Control base device
//#include <ambDeviceImpl.h>
#include <oldAmbDeviceImpl.h>
#include "ambDevIOTestS.h"

#include <baciSmartPropertyPointer.h>

#include <acsBool.h> // This is needed to define the ROAcsBool/RWAcsBool types
// These are headers which should go away
#include <baciROdouble.h>
#include <baciRWdouble.h>
#include <baciROdoubleSeq.h>
#include <baciRWdoubleSeq.h>
#include <baciROlong.h>
#include <baciRWlong.h>
#include <baciROlongLong.h>
#include <baciRWlongLong.h>
#include <baciROuLongLong.h>
#include <baciRWuLongLong.h>
#include <acsEnum.h>

AMB_ENUM_DEVIO(Control, EnumTestSet);

class AmbDevIOTestImpl: public virtual OldAmbDeviceImpl,
    public virtual POA_Control::ambDevIOTest
{
    public:
    /**
     * Constructor
     * @param poa Poa which will activate this and also all other components.
     * @param name component's name. This is also the name that will be used
     * to find the configuration data for the component in the Configuration
     *  Database.
     *  @exceptionControlExceptions::CDBErrorExImpl
     */
    AmbDevIOTestImpl(const ACE_CString& name,
        maci::ContainerServices *containerServices);

    /**
     * Destructor
     */
    virtual ~AmbDevIOTestImpl();

    /* Device Implementation test methods */
    virtual ACS::Time flush(ACS::Time flushTime);

    virtual ACS::Time flushAddr(ACS::Time flushTime, int RCA);

    /* --------------------- [ CORBA interface ] ----------------------*/
    /* Boolean Properties for testing */
    virtual ACS::ROBool_ptr roBoolProp();

    virtual ACS::RWBool_ptr rwBoolProp();

    virtual ACS::ROdouble_ptr roDoubleLinear();

    virtual ACS::RWdouble_ptr rwDoubleLinear();

    virtual ACS::ROdouble_ptr roSignedDoubleLinear();

    virtual ACS::RWdouble_ptr rwSignedDoubleLinear();

    virtual ACS::ROdoubleSeq_ptr roDoubleSeqLinear();

    virtual ACS::RWdoubleSeq_ptr rwDoubleSeqLinear();

    virtual ACS::ROlong_ptr roLongLinear();

    virtual ACS::RWlong_ptr rwLongLinear();

    virtual ACS::ROlongLong_ptr roLongLongLinear();

    virtual ACS::RWlongLong_ptr rwLongLongLinear();

    virtual ACS::ROlong_ptr roLongParametric();

    virtual ACS::RWlong_ptr rwLongParametric();

    virtual ACS::ROuLongLong_ptr roULongLongExponential();

    virtual ACS::RWuLongLong_ptr rwULongLongExponential();

    virtual Control::ROEnumTestSet_ptr roEnum();

    virtual Control::RWEnumTestSet_ptr rwEnum();

    /* -------------------- [Sub Lifecycle Methods ] ------------------*/
    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void startInitialize();

    private:
    /**
     The copy constructor is made private to prevent a compiler generated
     one from being used. It is not implemented.
     */
    AmbDevIOTestImpl(const AmbDevIOTestImpl& other);

    // Properties
    baci::SmartPropertyPointer< ROAcsBool > roBoolProp_m;
    baci::SmartPropertyPointer< RWAcsBool > rwBoolProp_m;

    baci::SmartPropertyPointer< baci::ROdouble > roDoubleLinear_m;
    baci::SmartPropertyPointer< baci::RWdouble > rwDoubleLinear_m;

    baci::SmartPropertyPointer< baci::ROdouble > roSignedDoubleLinear_m;
    baci::SmartPropertyPointer< baci::RWdouble > rwSignedDoubleLinear_m;

    baci::SmartPropertyPointer< baci::ROdoubleSeq > roDoubleSeqLinear_m;
    baci::SmartPropertyPointer< baci::RWdoubleSeq > rwDoubleSeqLinear_m;

    baci::SmartPropertyPointer< baci::ROlong > roLongLinear_m;
    baci::SmartPropertyPointer< baci::RWlong > rwLongLinear_m;

    baci::SmartPropertyPointer< baci::ROlongLong > roLongLongLinear_m;
    baci::SmartPropertyPointer< baci::RWlongLong > rwLongLongLinear_m;

    baci::SmartPropertyPointer< baci::ROlong > roLongParametric_m;
    baci::SmartPropertyPointer< baci::RWlong > rwLongParametric_m;

    baci::SmartPropertyPointer< baci::ROuLongLong > roULongLongExponential_m;
    baci::SmartPropertyPointer< baci::RWuLongLong > rwULongLongExponential_m;

    baci::SmartPropertyPointer< Control::ROEnumTestSetImpl > roEnum_m;
    baci::SmartPropertyPointer< Control::RWEnumTestSetImpl > rwEnum_m;
};

#endif /* AMBDEVIOTEST_H */
