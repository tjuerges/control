s|Queueing [0-9]\+ monitor requests ([0-9]\+\.[0-9]\+ seconds worth)|Queueing N monitor requests (N\.NN seconds worth)|g
s|Min/Max execution times (milli-seconds)\. [0-9]\+\.*[0-9]*/[0-9]\+\.*[0-9]*|Min/Max execution times (milli-seconds)\. N\.NN/N\.NN|g
s|.* Local file logger: Cache saved to .*||g

