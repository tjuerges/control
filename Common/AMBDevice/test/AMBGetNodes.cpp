//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Dec 28, 2006  created
//


/**
 * AMBGetNodes:
 * ------------
 *
 * Lists all devices on all CAN bus channels without a running ACS instance.
 *
 *
 * Prerequisites:
 * --------------
 *
 * - loadLkmModule foo.lkm
 * - In case of CAMBServerSim.lkm: the AMBLBSimulator must be running.
 */


#include <BareAmbDeviceInt.h>
#include <ControlExceptions.h>
#include <semaphore.h>
#include <ambDefs.h>
#include <iomanip>
#include <iostream>


int main(int argc, char* argv[])
{
    std::string myName(argv[0]);
    const std::string::size_type pos(myName.find_last_of('/'));
    if(pos != std::string::npos)
    {
        myName.erase(0, pos + 1);
    }

    std::cout << "\n"
        << myName
        << ":\nLists all the devices and their serial numbers on all CAN bus "
        << "channels.\nPlease stand by while querying the bus...\n";

    BareAMBDeviceInt mc;
    if(mc.initialize() == false)
    {
        std::cout << "Could not initialise the AMB interface.\n";
        return -ENODEV;
    }

    mc.setNodeAddress(0);

    for(unsigned short int channel(0U); channel < 6U; ++channel)
    {
        mc.setChannel(channel);

        AmbCompletion_t* completion(new AmbCompletion_t);
        AmbMessage_t message;

        AmbChannel channel_resp;
        AmbAddr node;
        AmbDataLength_t dataLength;
        AmbDataMem_t data[8];
        ACS::Time timestamp;
        sem_t synchLock;
        sem_t contLock;
        AmbErrorCode_t status;

        sem_init(&synchLock, 0, 0);
        sem_init(&contLock, 0, 1);

        // Build the completion block
        completion->dataLength_p = &dataLength;
        completion->data_p = data;
        completion->channel_p = &channel_resp;
        completion->address_p = &node;
        completion->timestamp_p = &timestamp;
        completion->status_p = &status;
        completion->synchLock_p = &synchLock;
        completion->contLock_p = &contLock;
        completion->type_p = 0;

        // Build the message block
        message.requestType = AMB_GET_NODES;
        message.channel = channel;
        message.address = 0;
        message.dataLen = 0;
        message.completion_p = completion;
        message.targetTE = 0;

        std::cout << "Querying nodes on channel "
            << channel
            << "...\n";

        try
        {
            mc.sendMessage(message);

            sem_wait(&synchLock);
            while(status == AMBERR_CONTINUE)
            {
                if(dataLength != 8)
                {
                    std::cout << "Node 0x"
                    << std::hex
                    << node
                    << std::dec
                    << "reported incorrect length serial number.\n";
                }
                else
                {
                    std::cout << "Node 0x"
                        << std::hex
                        << node
                        << " ("
                        << std::dec
                        << node
                        << "), serial number 0x";

                    for(unsigned int idx(0U); idx < 8U; ++idx)
                    {
                        std::cout << std::hex
                            << std::setw(2)
                            << std::setfill('0')
                            << static_cast< unsigned short >(data[idx]);
                    }

                    std::cout << std::dec
                        << "\n";
                }

                sem_post(&contLock);
                sem_wait(&synchLock);
            }

            sem_destroy(&synchLock);
            sem_destroy(&contLock);
        }
        catch(const ControlExceptions::CAMBErrorExImpl& ex)
        {
            sem_destroy(&synchLock);
            sem_destroy(&contLock);

            ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            nex.addData("Detail", "Ouch! Sending the broadcast resulted in an "
                "exception.");
            nex.log();
        }
        catch(...)
        {
            sem_destroy(&synchLock);
            sem_destroy(&contLock);

            ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            ex.addData("Detail", "Ouch! Sending the broadcast resulted in an "
                "unexpected exception.");
            ex.log();
        }
    }

    return 0;
}
