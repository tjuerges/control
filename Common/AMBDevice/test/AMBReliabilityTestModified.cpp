//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2006
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
// @(#) $Id$
//


#include <sstream> // for std::[i|o]stringstream
#include <deque> // for deque
#include <vector> // for std::vector
#include <iomanip> // for std::setprecision and std::fixed
#include <iostream> // for std::cout and std::endl

#include <Semaphore.h> // for sem_t
#include <Time_Value.h> // For ACE_Time_Value
#include <Get_Opt.h> // For the parameter parsing
#include <OS_NS_sys_select.h> // For ACE_OS::select

#include <TETimeUtil.h> // for TETimeUtil
#include <BareAmbDeviceInt.h> // Base class for BareAMBDeviceInt
#include <ambDefs.h> // For AMBERR defs
#include <acstimeEpochHelper.h> // for EpochHelper
#include <acstimeDurationHelper.h> // for DurationHelper


const static double minimumDuration(0.001);
const static double defaultDuration(10.0);
const static unsigned short int defaultNodeId(0x20U);
const static unsigned short int defaultChannel(0U);
const static unsigned short int defaultNumberRequests(40U);
const static unsigned int defaultRca(0x30000U);
const static unsigned int defaultReplyTolerance(5U);

double duration(defaultDuration);
unsigned short int nodeId(defaultNodeId);
unsigned short int channel(defaultChannel);
unsigned short int requests(defaultNumberRequests);
unsigned int rca(defaultRca);
unsigned int replyTolerance(defaultReplyTolerance);

// This is how often the monitor loop runs (once every 20 TE's).
const ACS::TimeInterval loopTime(20 * TETimeUtil::TE_PERIOD_DURATION.value);
// time format string.
const std::string timeFmt("%H:%M:%S.%3q");


void usage(char* argv0)
{
    std::cout << "\n"
        << argv0
        << "::usage:\n"
            "Options:\n"
            "[--help|h]:\n"
            "     You are reading it!\n"
            "[--duration=|-d<TIME in fractional s>]:\n"
            "      Duration of the test. Default = "
        << defaultDuration
        << "(minimum value = "
        << minimumDuration
        << "s)\n"
        << "[--nodeId=|-n<NODEID>]:\n"
            "      NodeId (hex., dec. or oct. base) to be used. Default = "
        << defaultNodeId
        << "\n"
            "[--channel=|-c<CHANNEL>]:\n"
            "      ABM channel (hex., dec. or oct. base). Default = "
        << defaultChannel
        << "\n"
            "[--monitor_rca=|-r<RCA>]:\n"
            "      RCA (hex., dec. or oct. base) of monitor request to use "
            "per request. Min. = 0x1; max. = 0x7fffff. Default = "
        << defaultRca
        << "\n"
            "[--reply_tolerance=|-t<NUMBER_OF_TEs>]:\n"
            "      Time tolerance when waiting for replies (in TEs w. hex., "
            "dec. or oct. base)). Default = "
        << defaultReplyTolerance
        << " TE.\n";
};

void checkBase(std::istringstream& input)
{
    // Check if the first two letters are 0x or 0X and set
    // the decoding of the std::istringstream accordingly.
    std::string subString(input.str().substr(0, 2));
    if((subString == "0x") || (subString == "0X"))
    {
        input.setf(std::ios::hex, std::ios::basefield);
        return;
    }

    // Now check if they are octal, e.g. the first letter is 0.
    subString = input.str().substr(0, 1);
    if(subString == "0")
    {
        input.setf(std::ios::oct, std::ios::basefield);
        return;
    }
}

struct RequestStruct
{
    AmbRelativeAddr RCA;
    ACS::Time TargetTime;
    AmbDataLength_t DataLength;
    AmbDataMem_t Data[8];
    ACS::Time Timestamp;
    AmbErrorCode_t Status;
};


int main(int argc, char* argv[])
{
    ACE_Get_Opt options(argc, argv);

    options.long_option("help", 'h');
    options.long_option("duration", 'd', ACE_Get_Opt::ARG_OPTIONAL);
    options.long_option("nodeId", 'n', ACE_Get_Opt::ARG_OPTIONAL);
    options.long_option("channel", 'c', ACE_Get_Opt::ARG_OPTIONAL);
    options.long_option("monitor_rca", 'r', ACE_Get_Opt::ARG_OPTIONAL);
    options.long_option("reply_tolerance", 't', ACE_Get_Opt::ARG_OPTIONAL);

    std::vector< AmbRelativeAddr > rcaList;
    int c(0);
    while((c = options()) != EOF)
    {
        switch(c)
        {
            case 'd':
            {
                if(options.opt_arg() != 0)
                {
                    std::istringstream value;
                    value.str(options.opt_arg());
                    double val(0);
                    value >> val;
                    if(val < minimumDuration)
                    {
                        std::cout << "The minimum duration is "
                            << minimumDuration
                            <<" s. The default value will be used instead of "
                            << val
                            << "s.\n";
                    }
                    else
                    {
                        duration = val;
                        std::cout << "The test duration has been set to "
                            << duration
                            << "s.\n";
                    }
                }
            }
            break;

            case 'n':
            {
                if(options.opt_arg() != 0)
                {
                    std::istringstream value;
                    value.str(options.opt_arg());
                    unsigned int val(0U);
                    checkBase(value);
                    value >> val;
                    if((val < 0) || (val > 0x200))
                    {
                        std::cout << "The node ID has to be in the range of "
                            "[0; 512].  The value (0x"
                            << std::hex
                            << val
                            << ") will not be used.\n";
                    }
                    else
                    {
                        nodeId = val;
                        std::cout << "The node ID has been set to 0x"
                            << std::hex
                            << nodeId
                            << ".\n";
                    }
                }
            }
            break;

            case 'c':
            {
                if(options.opt_arg() != 0)
                {
                    std::istringstream value;
                    value.str(options.opt_arg());
                    short int val(0);
                    checkBase(value);
                    value >> val;
                    if((val < 0) || (val > 5))
                    {
                        std::cout << "The channel number ("
                            << val
                            << "has to be in the range of [0; 5].  The value "
                               "will not be used.\n";
                    }
                    else
                    {
                        channel = val;
                        std::cout << "The ABM channel has been set to "
                            << channel
                            << ".\n";
                    }
                }
            }
            break;

            case 'r':
            {
                if(options.opt_arg() != 0)
                {
                    std::istringstream value;
                    value.str(options.opt_arg());
                    unsigned int val(0U);
                    checkBase(value);
                    value >> val;
                    if((val < 1) || (val > 0x7fffff))
                    {
                        std::cout << "The RCA (0x"
                            << std::hex
                            << val
                            << ") has to be in the range of [0x1; 0x7fffff].  "
                                "The value will not be used.\n";
                    }
                    else
                    {
                        rca = val;
                        std::cout << "RCA 0x"
                            << std::hex
                            << rca
                            << " has been added to the monitor point list.\n";

                        rcaList.push_back(rca); // Slave rev. level
                    }
                }
            }
            break;

            case 't':
            {
                if(options.opt_arg() != 0)
                {
                    std::istringstream value;
                    value.str(options.opt_arg());
                    unsigned int val(0);
                    checkBase(value);
                    value >> val;
                    if(val > defaultReplyTolerance)
                    {
                        replyTolerance = val;
                        std::cout << "The reply tolerance has been set to "
                            << replyTolerance
                            << " TEs.\n";
                    }
                }
            }
            break;

            case '?':
            {
                std::cout << "Option -"
                    << options.opt_opt()
                    << "requires an argument!\n";
            }

            // Fall through...
            case 'h':
            default:    // Display help.
            {
                usage(argv[0]);
                return -1;
            }
        }
    }

    BareAMBDeviceInt* mc(new BareAMBDeviceInt);
    if(mc->initialize(channel, nodeId) == false)
    {
        return -ENODEV;
    }

    if(rcaList.size() == 0)
    rcaList.push_back(rca); // Slave rev. level

    EpochHelper timeNow(TETimeUtil::ceilTE(
        TimeUtil::ace2epoch(ACE_OS::gettimeofday())));

    EpochHelper lastScheduledTime(timeNow.value());
    lastScheduledTime.add(DurationHelper(loopTime).value());

    EpochHelper stopTime(lastScheduledTime.value());
    stopTime.add(DurationHelper(static_cast< long double >(duration)).value());

    EpochHelper horizon(timeNow.value());
    horizon.add(DurationHelper(loopTime * 2).value());
    bool doExit(false);
    std::deque< RequestStruct > monitorQueue;

    int failure(0);
    int late_reply(0);
    int success(0);
    int tooLate(0);
    std::vector< unsigned int > failure_per_amberrorcode(13);

    double minTime(1E30);
    double maxTime(-1E30);
    ACE_Time_Value waitFor;

    do
    {
        if((horizon >= stopTime.value()))
        {
            horizon.value(stopTime.value());
            doExit = true;
        }

        {
            DurationHelper queueTime(horizon.difference(lastScheduledTime.value()));
            const int numTEs(queueTime.value().value /
                TETimeUtil::TE_PERIOD_DURATION.value);
            int monitorsPerTE(rcaList.size());
            const int numRequests(numTEs * monitorsPerTE);
            std::cout << "Queueing "
                << numRequests
                << " monitor requests ("
                << queueTime.toSeconds()
                << " seconds worth)...\n";
        }

        RequestStruct thisRequest;
        // Need to schedule the monitor requests 24ms after the TE
        thisRequest.TargetTime = lastScheduledTime.value().value +
            TETimeUtil::TE_PERIOD_DURATION.value / 2ULL;
        thisRequest.Status = AMBERR_PENDING;
        thisRequest.DataLength = 8U;
        thisRequest.RCA = 0x01U;
        thisRequest.Timestamp = 0ULL;
        const ACS::Time lastTime(horizon.value().value);

        while (thisRequest.TargetTime < lastTime)
        {
            for(std::vector< AmbRelativeAddr >::const_iterator i(
                rcaList.begin()); i != rcaList.end(); ++i)
            {
                thisRequest.RCA = *i;
                monitorQueue.push_back(thisRequest);
                // As the push_back function (above) will do a copy this gets a
                // reference to the copy and this is needed as the monitorTE
                // function uses the addresses.
                RequestStruct& nextRequest(monitorQueue.back());
                mc->monitorTE(nextRequest.TargetTime, nextRequest.RCA,
                nextRequest.DataLength, nextRequest.Data, NULL,
                &nextRequest.Timestamp, &nextRequest.Status);
            }

            thisRequest.TargetTime += TETimeUtil::TE_PERIOD_DURATION.value;
        }

        lastScheduledTime.value(horizon.value());
        waitFor.set(DurationHelper(loopTime).toSeconds());
        ACE_OS::select(0, 0, 0, 0, waitFor);

        bool processMonitors(true);
        while((processMonitors == true) && (monitorQueue.empty() == false))
        {
            RequestStruct& curMonitor(monitorQueue.front());

            if(curMonitor.Status == AMBERR_PENDING)
            {
                // Stop processing when we hit the first pending monitor point.
                processMonitors = false;

                // check that the pending monitor is less that 5TEs behind the
                // current time
                EpochHelper now(TETimeUtil::ceilTE(
                    TimeUtil::ace2epoch(ACE_OS::gettimeofday())));
                EpochHelper commandedTime(curMonitor.TargetTime);
                DurationHelper diff(now.difference(commandedTime.value()));
                const double executionTime(diff.toSeconds());
                if((now.value().value) > (
                    replyTolerance * TETimeUtil::TE_PERIOD_DURATION.value
                        + curMonitor.TargetTime))
                {
                    std::cout << "*** WARNING: reply pending more than "
                    << replyTolerance
                    << " TEs "
                    << " ( "
                    << executionTime
                    << " s ) after the commanded time.\n\n";
                    ++late_reply;
                }
            }
            else
            {
                EpochHelper commandedTime(curMonitor.TargetTime);
                EpochHelper actualTime(curMonitor.Timestamp);
                DurationHelper diff(actualTime.difference(
                    commandedTime.value()));
                const double executionTime(diff.toSeconds());

                if(executionTime > 0.020)
                {
                    ++tooLate;
                }

                if(executionTime < minTime)
                {
                    minTime = executionTime;
                }

                if(executionTime > maxTime)
                {
                    maxTime = executionTime;
                }

                if(curMonitor.Status == AMBERR_TIMEOUT)
                {
                    std::cout << "*** TIMEOUT ***\n"
                        << "Commanded Time "
                        << commandedTime.toString(
                            acstime::TSArray, timeFmt.c_str(), 0,0)
                        << " Actual Time "
                        << actualTime.toString(
                            acstime::TSArray, timeFmt.c_str(), 0,0)
                        << " Difference "
                        << std::fixed << std::setprecision(6)
                        << executionTime
                        << " RCA: "
                        << curMonitor.RCA
                        << " Datalength: "
                        << curMonitor.DataLength
                        << " Status: "
                        << curMonitor.Status
                        << "\n\n";
                }

                if(curMonitor.Status != AMBERR_NOERR)
                {
                    ++failure;
                }
                else
                {
                    ++success;
                }

                ++(failure_per_amberrorcode[curMonitor.Status]);
                monitorQueue.pop_front();
            }
        }

        horizon.value(
            TETimeUtil::ceilTE(TimeUtil::ace2epoch(ACE_OS::gettimeofday())));
        horizon.add(DurationHelper(loopTime * 2).value());
    }
    while(doExit == false);

    EpochHelper now(TETimeUtil::ceilTE(
        TimeUtil::ace2epoch(ACE_OS::gettimeofday())));

    ACS::TimeInterval waitTime = stopTime.value().value - now.value().value
        + replyTolerance*TETimeUtil::TE_PERIOD_DURATION.value;
    if(waitTime > 0)
    {
        std::cout << "Waiting "
            << DurationHelper(waitTime).toSeconds()
            << " seconds before checking the pending requests.\n\n";

        waitFor.set(DurationHelper(waitTime).toSeconds());
        ACE_OS::select(0, 0, 0, 0, waitFor);
    }

    while(monitorQueue.empty() == false)
    {
        RequestStruct& curMonitor(monitorQueue.front());
        EpochHelper commandedTime(curMonitor.TargetTime);
        EpochHelper actualTime(curMonitor.Timestamp);
        DurationHelper diff(actualTime.difference(commandedTime.value()));
        const double executionTime(diff.toSeconds());

        if(executionTime < minTime)
        {
            minTime = executionTime;
        }

        if(executionTime > maxTime)
        {
            maxTime = executionTime;
        }

        if(executionTime > 0.020)
        {
            ++tooLate;
        }

        if(curMonitor.Status == AMBERR_TIMEOUT)
        {
            std::cout << "*** TIMEOUT ***\n"
                << "Commanded Time "
                << commandedTime.toString(
                    acstime::TSArray, timeFmt.c_str(), 0,0)
                << " Actual Time "
                << actualTime.toString(acstime::TSArray, timeFmt.c_str(), 0,0)
                << " Difference "
                << std::fixed << std::setprecision(6)
                << executionTime
                << " RCA: "
                << curMonitor.RCA
                << " Datalength: "
                << curMonitor.DataLength
                << " Status: "
                << curMonitor.Status
                << "\n\n";

            doExit = true;
        }

        if(curMonitor.Status == AMBERR_PENDING)
        {
            // All the monitor requests should be completed at this point
            // because we were sleeping before analizing the last points
            ++late_reply;
        }

        if(curMonitor.Status != AMBERR_NOERR)
        {
            ++failure;
        }
        else
        {
            ++success;
        }

        failure_per_amberrorcode[curMonitor.Status]++;
        monitorQueue.pop_front();
    }

    std::cout << "\n\nFINAL REPORT\n"
        "Sent "
        << success + failure
        << " monitor points. Problems with "
        << failure
        << " of them ("
        << 100.0 * failure / (success + failure)
        << "%).\n"
            "Min/Max execution times (milli-seconds). "
        << minTime * 1000.0
        << "/"
        << maxTime * 1000.0
        << "\n"
        << tooLate
        << " monitor points ("
        << 100.0 * tooLate / (success + failure)
        << "%) where executed later than 20ms after the requested time.\n"
            "In "
        << late_reply
        << " occasions the monitor request replies were still pending "
        << replyTolerance
        <<" TEs after the commanded time.\n\n"
            "Error summary (code 0 means no error):";

    for(unsigned int index(0U); index <= 13; ++index)
    {
        std::cout << "\nMonitor points with AmbErrorCode_t "
            << index
            << ": "
            << failure_per_amberrorcode[index];
    }

    std::cout << "\n\n";

    try
    {
        delete mc;
        mc = 0;
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__, "main");
        nex.log();
    }

    return 0;
}
