/* @(#) $Id$
 *
 * Copyright (C) 2002
 * Associated Universities, Inc. Washington DC, USA.
 *
 * Produced for the ALMA project
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
 *
 * Correspondence concerning ALMA should be addressed as follows:
 *
 *        Internet email: alma-sw-admin@nrao.edu
 */

#include <ambDevIO.h>
#include "ambDevIOTestImpl.h"

// Debugging
#include <unistd.h>

AmbDevIOTestImpl::AmbDevIOTestImpl(const ACE_CString& name,
    maci::ContainerServices* containerServices):
    OldAmbDeviceImpl(name, containerServices),
    roBoolProp_m(this),
    rwBoolProp_m(this),
    roDoubleLinear_m(this),
    rwDoubleLinear_m(this),
    roSignedDoubleLinear_m(this),
    rwSignedDoubleLinear_m(this),
    roDoubleSeqLinear_m(this),
    rwDoubleSeqLinear_m(this),
    roLongLinear_m(this),
    rwLongLinear_m(this),
    roLongLongLinear_m(this),
    rwLongLongLinear_m(this),
    roLongParametric_m(this),
    rwLongParametric_m(this),
    roULongLongExponential_m(this),
    rwULongLongExponential_m(this),
    roEnum_m(this),
    rwEnum_m(this)
{
    ACS_TRACE("AmbDevIOTestImpl::AmbDevIOTestImpl");
}

AmbDevIOTestImpl::~AmbDevIOTestImpl()
{
    // delete threads
    if(getComponent())
        getComponent()->stopAllThreads();
}

/* -------------------[ Lifecycle Sub Method Interface ] -------------------*/
void AmbDevIOTestImpl::startInitialize()
{
    ACS_TRACE("AmbDevIOTestImpl::startInitialize");
    OldAmbDeviceImpl::startInitialize();

    // create Properties
    {
        const ACE_CString propName = cdbName_m + ":roBoolProp";
        roBoolProp_m = new ROAcsBool(propName, getComponent(), new AmbRODevIO<
            ACS::Bool, AcsBool> (getElement("roBoolProp"), this), true);
    }

    {
        const ACE_CString propName = cdbName_m + ":rwBoolProp";
        rwBoolProp_m = new RWAcsBool(propName, getComponent(), new AmbRWDevIO<
            ACS::Bool, AcsBool> (getElement("rwBoolProp"), this), true);
    }

    {
        const ACE_CString propName = cdbName_m + ":roDoubleLinear";
        roDoubleLinear_m = new baci::ROdouble(propName, getComponent(),
            new AmbRODevIO<CORBA::Double, Linear> (
                getElement("roDoubleLinear"), this), true);
    }

    {
        const ACE_CString propName = cdbName_m + ":rwDoubleLinear";
        rwDoubleLinear_m = new baci::RWdouble(propName, getComponent(),
            new AmbRWDevIO<CORBA::Double, Linear> (
                getElement("rwDoubleLinear"), this), true);
    }

    {
        const ACE_CString propName = cdbName_m + ":roSignedDoubleLinear";
        roSignedDoubleLinear_m = new baci::ROdouble(propName, getComponent(),
            new AmbRODevIO<CORBA::Double, Linear> (getElement(
                "roSignedDoubleLinear"), this), true);
    }
    {
        const ACE_CString propName = cdbName_m + ":rwSignedDoubleLinear";
        rwSignedDoubleLinear_m = new baci::RWdouble(propName, getComponent(),
            new AmbRWDevIO<CORBA::Double, Linear> (getElement(
                "rwSignedDoubleLinear"), this), true);
    }
    {
        const ACE_CString propName = cdbName_m + ":roDoubleSeqLinear";
        roDoubleSeqLinear_m = new baci::ROdoubleSeq(propName, getComponent(),
            new AmbROSeqDevIO<ACS::doubleSeq, Linear, CORBA::Double> (
                getElement("roDoubleSeqLinear"), this), true);
    }
    {
        const ACE_CString propName = cdbName_m + ":rwDoubleSeqLinear";
        rwDoubleSeqLinear_m = new baci::RWdoubleSeq(propName, getComponent(),
            new AmbRWSeqDevIO<ACS::doubleSeq, Linear, CORBA::Double> (
                getElement("rwDoubleSeqLinear"), this), true);
    }

    {
        const ACE_CString propName = cdbName_m + ":roLongLinear";
        roLongLinear_m = new baci::ROlong(propName, getComponent(), new AmbRODevIO<
            CORBA::Long, Linear> (getElement("roLongLinear"), this), true);
    }
    {
        const ACE_CString propName = cdbName_m + ":rwLongLinear";
        rwLongLinear_m = new baci::RWlong(propName, getComponent(), new AmbRWDevIO<
            CORBA::Long, Linear> (getElement("rwLongLinear"), this), true);
    }
    {
        const ACE_CString propName = cdbName_m + ":roLongLongLinear";
        roLongLongLinear_m = new baci::ROlongLong(propName, getComponent(),
            new AmbRODevIO<CORBA::LongLong, Linear> (getElement(
                "roLongLongLinear"), this), true);
    }
    {
        const ACE_CString propName = cdbName_m + ":rwLongLongLinear";
        rwLongLongLinear_m = new baci::RWlongLong(propName, getComponent(),
            new AmbRWDevIO<CORBA::LongLong, Linear> (getElement(
                "rwLongLongLinear"), this), true);
    }
    {
        const ACE_CString propName = cdbName_m + ":roLongParametric";
        roLongParametric_m = new baci::ROlong(propName, getComponent(),
            new AmbRODevIO<CORBA::Long, Parametric> (getElement(
                "roLongParametric"), this), true);
    }
    {
        const ACE_CString propName = cdbName_m + ":rwLongParametric";
        rwLongParametric_m = new baci::RWlong(propName, getComponent(),
            new AmbRWDevIO<CORBA::Long, Parametric> (getElement(
                "rwLongParametric"), this), true);
    }
    {
        const ACE_CString propName = cdbName_m + ":roULongLongExponential";
        roULongLongExponential_m = new baci::ROuLongLong(propName, getComponent(),
            new AmbRODevIO<CORBA::ULongLong, Exponential> (getElement(
                "roULongLongExponential"), this), true);
    }
    {
        const ACE_CString propName = cdbName_m + ":rwULongLongExponential";
        rwULongLongExponential_m = new baci::RWuLongLong(propName, getComponent(),
            new AmbRWDevIO<CORBA::ULongLong, Exponential> (getElement(
                "rwULongLongExponential"), this), true);
    }
    {
        const ACE_CString propName = cdbName_m + ":roEnum";
        roEnum_m = new Control::ROEnumTestSetImpl(propName, getComponent(),
            new AmbRODevIO<Control::EnumTestSet, AcsEnum> (getElement("roEnum"), this),
            true);
    }
    {
        const ACE_CString propName = cdbName_m + ":rwEnum";
        rwEnum_m = new Control::RWEnumTestSetImpl(propName, getComponent(),
            new AmbRWDevIO<Control::EnumTestSet, AcsEnum> (getElement("rwEnum"), this),
            true);
    }
}

//-----------------------------------------------------------------------------
// Device Implementation tests
//----------------------------------------------------------------------------
ACS::Time AmbDevIOTestImpl::flush(ACS::Time flushTime)
{
    ACS_TRACE("AmbDevIOTestImpl::flushTest");
    ACS::Time timestamp;
    AmbErrorCode_t status;

    flushNode(flushTime, & timestamp, & status);

    return timestamp;
}

ACS::Time AmbDevIOTestImpl::flushAddr(ACS::Time flushTime, int RCA)
{
    ACS_TRACE("AmbDevIOTestImpl::flushRCA");
    ACS::Time timestamp;
    AmbErrorCode_t status;

    flushRCA(flushTime, static_cast<AmbRelativeAddr> (RCA), & timestamp,
        & status);

    return timestamp;
}

//-----------------------------------------------------------------------------
// Property interface
//-----------------------------------------------------------------------------

ACS::ROBool_ptr AmbDevIOTestImpl::roBoolProp()
{
    ACS::ROBool_var prop = ACS::ROBool::_narrow(
        roBoolProp_m->getCORBAReference());
    return prop._retn();
}

ACS::RWBool_ptr AmbDevIOTestImpl::rwBoolProp()
{
    ACS::RWBool_var prop = ACS::RWBool::_narrow(
        rwBoolProp_m->getCORBAReference());
    return prop._retn();
}

ACS::ROdouble_ptr AmbDevIOTestImpl::roDoubleLinear()
{
    ACS::ROdouble_var prop = ACS::ROdouble::_narrow(
        roDoubleLinear_m->getCORBAReference());
    return prop._retn();
}

ACS::RWdouble_ptr AmbDevIOTestImpl::rwDoubleLinear()
{
    ACS::RWdouble_var prop = ACS::RWdouble::_narrow(
        rwDoubleLinear_m->getCORBAReference());
    return prop._retn();
}

ACS::ROdouble_ptr AmbDevIOTestImpl::roSignedDoubleLinear()
{
    ACS::ROdouble_var prop = ACS::ROdouble::_narrow(
        roSignedDoubleLinear_m->getCORBAReference());
    return prop._retn();
}

ACS::RWdouble_ptr AmbDevIOTestImpl::rwSignedDoubleLinear()
{
    ACS::RWdouble_var prop = ACS::RWdouble::_narrow(
        rwSignedDoubleLinear_m->getCORBAReference());
    return prop._retn();
}

ACS::ROdoubleSeq_ptr AmbDevIOTestImpl::roDoubleSeqLinear()
{
    ACS::ROdoubleSeq_var prop = ACS::ROdoubleSeq::_narrow(
        roDoubleSeqLinear_m->getCORBAReference());
    return prop._retn();
}

ACS::RWdoubleSeq_ptr AmbDevIOTestImpl::rwDoubleSeqLinear()
{
    ACS::RWdoubleSeq_var prop = ACS::RWdoubleSeq::_narrow(
        rwDoubleSeqLinear_m->getCORBAReference());
    return prop._retn();
}

ACS::ROlong_ptr AmbDevIOTestImpl::roLongLinear()
{
    ACS::ROlong_var prop = ACS::ROlong::_narrow(
        roLongLinear_m->getCORBAReference());
    return prop._retn();
}

ACS::RWlong_ptr AmbDevIOTestImpl::rwLongLinear()
{
    ACS::RWlong_var prop = ACS::RWlong::_narrow(
        rwLongLinear_m->getCORBAReference());
    return prop._retn();
}

ACS::ROlongLong_ptr AmbDevIOTestImpl::roLongLongLinear()
{
    ACS::ROlongLong_var prop = ACS::ROlongLong::_narrow(
        roLongLongLinear_m->getCORBAReference());
    return prop._retn();
}

ACS::RWlongLong_ptr AmbDevIOTestImpl::rwLongLongLinear()
{
    ACS::RWlongLong_var prop = ACS::RWlongLong::_narrow(
        rwLongLongLinear_m->getCORBAReference());
    return prop._retn();
}

ACS::ROlong_ptr AmbDevIOTestImpl::roLongParametric()
{
    ACS::ROlong_var prop = ACS::ROlong::_narrow(
        roLongParametric_m->getCORBAReference());
    return prop._retn();
}

ACS::RWlong_ptr AmbDevIOTestImpl::rwLongParametric()
{
    ACS::RWlong_var prop = ACS::RWlong::_narrow(
        rwLongParametric_m->getCORBAReference());
    return prop._retn();
}

ACS::ROuLongLong_ptr AmbDevIOTestImpl::roULongLongExponential()
{
    ACS::ROuLongLong_var prop = ACS::ROuLongLong::_narrow(
        roULongLongExponential_m->getCORBAReference());
    return prop._retn();
}

ACS::RWuLongLong_ptr AmbDevIOTestImpl::rwULongLongExponential()
{
    ACS::RWuLongLong_var prop = ACS::RWuLongLong::_narrow(
        rwULongLongExponential_m->getCORBAReference());
    return prop._retn();
}

Control::ROEnumTestSet_ptr AmbDevIOTestImpl::roEnum()
{
    Control::ROEnumTestSet_var prop = Control::ROEnumTestSet::_narrow(
        roEnum_m->getCORBAReference());
    return prop._retn();
}

Control::RWEnumTestSet_ptr AmbDevIOTestImpl::rwEnum()
{
    Control::RWEnumTestSet_var prop = Control::RWEnumTestSet::_narrow(
        rwEnum_m->getCORBAReference());
    return prop._retn();
}

/////////////////////////////////////////////////
// MACI DLL support functions
/////////////////////////////////////////////////
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(AmbDevIOTestImpl)
