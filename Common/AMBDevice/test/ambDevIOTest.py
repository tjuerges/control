#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$

from ACS import acsFALSE
from ACS import acsTRUE

# This is necessary to prevent the test from picking up the local version
# of the Control
import sys
import os
origPath = sys.path
cwd = os.getcwd().split('/')
path = []
for x in sys.path:
    sx = x.split('/')
    if sx[0:(len(cwd)-1)] != cwd[0:(len(cwd)-1)]:
        path.append(x)

sys.path = path

import Control
import Control__POA

# Now set the path back for the rest of this
sys.path = origPath

import ambDevIOTest_idl
import unittest
import struct
import ControlExceptions
from CCL.AmbManager import *
from TETimeUtil import *
from time import time, sleep

from Acspy.Clients.SimpleClient import PySimpleClient


class ambDevIOTest(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        self._client=PySimpleClient.getInstance()
        unittest.TestCase.__init__(self,methodName)

    def __del__(self):
        self._client.disconnect()


    def setUp(self):
        self._ref = self._client.getComponent("CONTROL/AmbDevIOTest/Node0x02/Generic")

    def tearDown(self):
        self._client.releaseComponent(self._ref._get_name())

    def propTest(self,roProp,rwProp, initValue, returnValue):
        try:
            returnVal = rwProp.set_sync(initValue)
        except :
            self.fail("Error writing to AMB")

        returnVal = rwProp.get_sync()
        self.failIf(returnVal[0] != initValue,
                    "Read Write Property Returned wrong value")
        
        try:
            returnVal = roProp.get_sync()
        except :
            self.fail("Error reading from AMB")
        
        self.failIf(returnVal[0] == initValue,
                    "Read Only Property returned same as Read Write Property")
        self.failIf(returnVal[0] != returnValue,
                    "Read Only Property returned incorrect value.")

    # --------------------------------------------------
    # Test Definitions
    def test01(self):
        '''
        Test the Basic framework with <Double, Linear>
        '''
        roProp = self._ref._get_roDoubleLinear()
        rwProp = self._ref._get_rwDoubleLinear()
        self.propTest(roProp,rwProp,5.5,5.25);

    def test02(self):
        '''
        Test the Signed version of the basic test
        '''
        roProp = self._ref._get_roSignedDoubleLinear()
        rwProp = self._ref._get_rwSignedDoubleLinear()
        self.propTest(roProp,rwProp,-5.5,-5.25);

    def test03(self):
        '''
        Test the Sequence properties
        '''
        roProp = self._ref._get_roDoubleSeqLinear()
        rwProp = self._ref._get_rwDoubleSeqLinear()
        self.propTest(roProp,rwProp,[201.5, 202.5, 203.5],
                      [201.25, 201.875, 203.125]);

    def test04(self):
        '''
        Test the interger version
        '''
        roProp = self._ref._get_roLongLinear()
        rwProp = self._ref._get_rwLongLinear()
        self.propTest(roProp, rwProp, 100, 16484);

    def test05(self):
        '''
        Test Long Long interger mapping
        '''
        roProp = self._ref._get_roLongLongLinear()
        rwProp = self._ref._get_rwLongLongLinear()
        self.propTest(roProp, rwProp, 12345, 12346);

    def test06(self):
        '''
        Test the boolean converter
        '''
        roProp = self._ref._get_roBoolProp()
        rwProp = self._ref._get_rwBoolProp()
        self.propTest(roProp, rwProp, acsTRUE, acsFALSE)
    
    def test07(self):
        '''
        Test the parameteric converter
        '''
        roProp = self._ref._get_roLongParametric()
        rwProp = self._ref._get_rwLongParametric()
        self.propTest(roProp, rwProp, 32, 2016);
        
    def test08(self):
        '''
        Test the LongLong Exponential converter
        '''
        roProp = self._ref._get_roULongLongExponential()
        rwProp = self._ref._get_rwULongLongExponential()
        self.propTest(roProp, rwProp, 8, 27);

    def test09(self):
        '''
        Test the temperature converter
        '''
        roProp = self._ref._get_ambTemperature()

        returnVal = roProp.get_sync()
        self.failIf(returnVal[0] != 22.39,
                    "Read Only Property returned incorrect value.")

    def test10(self):
        '''
        Test the Enumerated converter type.
        '''
        roProp = self._ref._get_roEnum()
        rwProp = self._ref._get_rwEnum()
        self.propTest(roProp, rwProp, Control.ENUM_VALUE_B,
                      Control.ENUM_VALUE_C)

    def test11(self):
        '''
        Test the flush capabilities of the system
        '''

# This test is commented out for MONTHLY-2005-08 integration as there
# is a problem in the real-time code. Re-enable this test once the
# problem is fixed.

#         mgr = AmbManager("Test")

#         data01 = TEData()
#         data02 = TEData()
#         data03 = TEData()

#         startTime = plusTE(unix2epoch(time()),10)
        
#         for x in range(0,100):
#             mgr.monitorTEAsynch(0,0x02,0x30001,
#                                 plusTE(startTime,x).value,data01)
#             mgr.monitorTEAsynch(0,0x02,0x30002,
#                                 plusTE(startTime,x).value,data02)
#             mgr.monitorTEAsynch(0,0x03,0x30002,
#                                 plusTE(startTime,x).value,data03)

#         self._ref.flush(plusTE(startTime,50).value)
#         self._ref.flushAddr(plusTE(startTime,25).value,0x30002)

#         sleep(10) # Allow everything to execute

#         # The ranges are necessary because this is not a real time interface
#         self.failIf(len(data01.errorList) > 51 or len(data01.errorList) < 49 ,
#                     "Node 2 RCA 0x30001 Incorrect number of flushes: %d" %
#                     len(data01.errorList) )
#         self.failIf(len(data02.errorList) > 76 or len(data02.errorList)< 74,
#                     "Node 2 RCA 0x30002 Incorrect number of flushes: %d" %
#                     len(data01.errorList) )
#         self.failIf(len(data03.errorList) != 0,
#                     "Node 3 RCA 0x30002 Incorrect number of flushes: %d" %
#                     len(data01.errorList) )
#         del(mgr)
        
# **************************************************
# MAIN Program
if __name__ == '__main__':
    # For some reason ACS has problems unless I do this
    client = PySimpleClient.getInstance()
 
    unittest.main()
