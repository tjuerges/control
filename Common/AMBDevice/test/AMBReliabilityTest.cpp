//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2006
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
// @(#) $Id$
//


#include <BareAmbDeviceInt.h> // Base class for BareAMBDeviceInt
#include <ace/Semaphore.h> // for sem_t
#include <ambDefs.h> // For AMBERR defs
#include <acstimeEpochHelper.h> // for EpochHelper
#include <acstimeDurationHelper.h> // for DurationHelper
#include <TETimeUtil.h> // for TETimeUtil
#include <ControlExceptions.h>
#include <unistd.h> // for usleep
#include <deque> // for std::deque
#include <iomanip> // for std::setprecision and std::fixed
#include <iostream> // for std::cerr and endl
#include <vector> // for std::vector


const int channel(0);
const int nodeId(0x0);

// This is how often the monitor loop runs (once every 20 TE's).
const ACS::TimeInterval loopTime(20 * TETimeUtil::TE_PERIOD_DURATION.value);
const double duration(60.0);



struct RequestStruct
{
    AmbRelativeAddr RCA;
    ACS::Time TargetTime;
    AmbDataLength_t DataLength;
    AmbDataMem_t Data[8];
    ACS::Time Timestamp;
    AmbErrorCode_t Status;
};


int main(int argc, char* argv[])
{
    BareAMBDeviceInt mc;
    // Todo parse input

    if(mc.initialize(channel, nodeId) == false)
    {
        return 1;
    }

    EpochHelper timeNow(TETimeUtil::ceilTE(
        TimeUtil::ace2epoch( ACE_OS::gettimeofday())));

    EpochHelper lastScheduledTime(timeNow.value());
    lastScheduledTime.add(DurationHelper(loopTime).value());

    EpochHelper stopTime(lastScheduledTime.value());
    stopTime.add(DurationHelper(static_cast< long double > (duration)).value());

    EpochHelper horizon(timeNow.value());
    horizon.add(DurationHelper(loopTime * 2).value());
    bool doExit(false);
    std::deque< RequestStruct > monitorQueue;

    // This is a list of all the RCA's that need to be monitored every TE.
    std::vector< AmbRelativeAddr > rcaList;
    rcaList.push_back(0x22);
    rcaList.push_back(0x02);
    rcaList.push_back(0x12);
    rcaList.push_back(0x22);
    rcaList.push_back(0x17);
    rcaList.push_back(0x07);
    rcaList.push_back(0x22);
    rcaList.push_back(0x1B);
    rcaList.push_back(0x0B);
    rcaList.push_back(0x23);

    rcaList.push_back(0x22);
    rcaList.push_back(0x02);
    rcaList.push_back(0x12);
    rcaList.push_back(0x22);
    rcaList.push_back(0x17);
    rcaList.push_back(0x07);
    rcaList.push_back(0x22);
    rcaList.push_back(0x1B);
    rcaList.push_back(0x0B);
    rcaList.push_back(0x23);

    rcaList.push_back(0x22);
    rcaList.push_back(0x02);
    rcaList.push_back(0x12);
    rcaList.push_back(0x22);
    rcaList.push_back(0x17);
    rcaList.push_back(0x07);
    rcaList.push_back(0x22);
    rcaList.push_back(0x1B);
    rcaList.push_back(0x0B);
    rcaList.push_back(0x23);

    int failure(0);
    int success(0);
    int tooLate(0);
    double minTime(1E30);
    double maxTime(-1E30);

    do
    {
        if((horizon >= stopTime.value()))
        {
            horizon.value(stopTime.value());
            doExit = true;
        }

        {
            DurationHelper queueTime(horizon.difference(
                lastScheduledTime.value()));
            const int numTEs(queueTime.value().value
                / TETimeUtil::TE_PERIOD_DURATION.value);
            int monitorsPerTE(rcaList.size());
            const int numRequests(numTEs * monitorsPerTE);
            std::cerr << "Queueing "
                << numRequests
                << " monitor requests ("
                << queueTime.toSeconds()
                << " seconds worth)\n";
        }

        RequestStruct thisRequest;
        // Need to schedule the monitor requests 24ms after the TE
        thisRequest.TargetTime = lastScheduledTime.value().value
            + TETimeUtil::TE_PERIOD_DURATION.value / 2ULL;
        thisRequest.Status = AMBERR_PENDING;
        thisRequest.DataLength = 8U;
        thisRequest.RCA = 0x01U;
        thisRequest.Timestamp = 0ULL;

        const ACS::Time lastTime(horizon.value().value);
        while(thisRequest.TargetTime < lastTime)
        {
            for(unsigned int i(0U); i < rcaList.size(); ++i)
            {
                thisRequest.RCA = rcaList[i];
                monitorQueue.push_back(thisRequest);
                // As the push_back function (above) will do a copy this gets a
                // reference to the copy and this is needed as the monitorTE
                // function uses the addresses.
                RequestStruct& nextRequest(monitorQueue.back());
                mc.monitorTE(nextRequest.TargetTime, nextRequest.RCA,
                    nextRequest.DataLength, nextRequest.Data, NULL,
                    &nextRequest.Timestamp, &nextRequest.Status);
            }

            thisRequest.TargetTime += TETimeUtil::TE_PERIOD_DURATION.value;
        }

        lastScheduledTime.value(horizon.value());

        usleep(static_cast< useconds_t >(DurationHelper(loopTime).toSeconds()
            * 1E6));

        bool processMonitors(true);
        while((processMonitors == true) && (monitorQueue.empty() == false))
        {
            RequestStruct& curMonitor(monitorQueue.front());
            if(curMonitor.Status == AMBERR_PENDING)
            {
                // Stop processing when we hit the first pending monitor point.
                processMonitors = false;
            }
            else
            {
                EpochHelper commandedTime(curMonitor.TargetTime);
                EpochHelper actualTime(curMonitor.Timestamp);
                DurationHelper diff(
                    actualTime.difference(commandedTime.value()));

                const double executionTime(diff.toSeconds());
                if(executionTime > 0.020)
                {
                    ++tooLate;
                }

                if(executionTime < minTime)
                {
                    minTime = executionTime;
                }

                if(executionTime > maxTime)
                {
                    maxTime = executionTime;
                }

                if(curMonitor.Status != AMBERR_NOERR)
                {
                    std::cerr << "Commanded Time "
                        << TETimeUtil::toTimeString(commandedTime)
                    << " Actual Time "
                    << TETimeUtil::toTimeString(actualTime)
                    << " Difference "
                    << std::fixed << std::setprecision(6)
                    << executionTime
                    << " RCA: "
                    << curMonitor.RCA
                    << " Datalength: "
                    << curMonitor.DataLength
                    << " Status: "
                    << curMonitor.Status
                    << "\n";

                    ++failure;
                }
                else
                {
                    ++success;
                }

                monitorQueue.pop_front();
            }
        }

        horizon. value(TETimeUtil::ceilTE(TimeUtil::ace2epoch(
            ACE_OS::gettimeofday())));
        horizon.add(DurationHelper(loopTime * 2).value());
    }
    while(doExit == false);

    usleep(static_cast< useconds_t > (DurationHelper(loopTime * 5).toSeconds()
        * 1E6));

    while(monitorQueue.empty() == false)
    {
        RequestStruct& curMonitor(monitorQueue.front());
        EpochHelper commandedTime(curMonitor.TargetTime);
        EpochHelper actualTime(curMonitor.Timestamp);
        DurationHelper diff(actualTime.difference(commandedTime.value()));

        const double executionTime(diff.toSeconds());
        if(executionTime < minTime)
        {
            minTime = executionTime;
        }

        if(executionTime > maxTime)
        {
            maxTime = executionTime;
        }

        if(executionTime > 0.020)
        {
            ++tooLate;
        }

        if(curMonitor.Status != AMBERR_NOERR)
        {
            std::cerr << "Commanded Time "
                << TETimeUtil::toTimeString(commandedTime)
                << " Actual Time "
                << TETimeUtil::toTimeString(actualTime)
                << " Difference "
                << std::fixed << std::setprecision(6)
                << diff.toSeconds()
                << " RCA: "
                << curMonitor.RCA
                << " Datalength: "
                << curMonitor.DataLength
                << " Status: "
                << curMonitor.Status
                << "\n";

            ++failure;
        }
        else
        {
            ++success;
        }

        monitorQueue.pop_front();
    }

    std::cerr << "Sent "
        << success + failure
        << " monitor points. Problems with "
        << failure
        << " of them ("
        << 100.0 * failure / (success + failure)
        << "%).\n"
        << "Min/Max execution times (milli-seconds). "
        << minTime * 1000
        << "/"
        << maxTime * 1000
        << "\n"
        << tooLate
        << " monitor points ("
        << 100.0 * tooLate / (success + failure)
        << "%) where executed later than 20ms after the requested time.\n";
}
