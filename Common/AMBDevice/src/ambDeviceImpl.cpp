//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


//------------------------------------------------------------------------
//    This is the baseclass for all AMB devices, monitor and control
// facilities are implemented through the AmbDeviceInt.
//------------------------------------------------------------------------


#include <iomanip>
#include <string>
#include <sstream>
#include <cerrno>
#include <cstring>
#include <ControlExceptions.h>
// For the AmbInterface class.
#include <ambInterface.h>
#include <ambDefs.h>
#include <ambDeviceImpl.h>
#include <hardwareDeviceImpl.h>
#include <ambInterface.h>
#include <maciContainerServices.h>
#include <acsErrTypeLifeCycle.h>
#include <xercesc/dom/DOM.hpp>
#include <acsutilTimeStamp.h>
#include <TETimeUtil.h>


AmbDeviceImpl::AmbDeviceImpl(const ACE_CString& name,
    maci::ContainerServices* cs):
    Control::HardwareDeviceImpl(name, cs),
    AmbCDBAccessor(cs),
    AmbDeviceInt(),
    baseAddress_m(0UL),
    timeThread_p(0),
    currentTime(0ULL)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

AmbDeviceImpl::~AmbDeviceImpl()
{
    if(timeThread_p != 0)
    {
        getContainerServices()->getThreadManager()->stop(
            timeThread_p->getName());
        getContainerServices()->getThreadManager()->terminate(
            timeThread_p->getName());
        delete timeThread_p;
    }
}

// ----------------------- [ Lifecycle Interface ] ------------------
void AmbDeviceImpl::hwStartAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

}

void AmbDeviceImpl::getAmbInterfaceInstance()
{
    interface_mp = AmbInterface::getInstance();
}

void AmbDeviceImpl::hwConfigureAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(interface_mp == 0)
    {
        try
        {
            getAmbInterfaceInstance();
            //interface_mp = AmbInterface::getInstance();
        }
        catch(const ControlExceptions::CAMBErrorExImpl& ex)
        {
            const std::string msg("Unable to get instance of of AmbInterface");
            setError(msg);

            ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            nex.addData("Detail", msg);
            throw nex;
        }
    }

    std::string deviceID;
    try
    {
        getDeviceUniqueId(deviceID);
        setSerialNumber(deviceID);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        const std::string msg("Device failed to respond to request for unique "
            "ID");
        setError(msg);

        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", msg);
        throw nex;
    }

    // What should happen here is that we contact the T-CDB and get
    // the instance specific data and update the T-CDB if necessary
}

void AmbDeviceImpl::hwInitializeAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

}

void AmbDeviceImpl::hwOperationalAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

void AmbDeviceImpl::hwStopAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(interface_mp != 0)
    {
        interface_mp->deleteInstance();
        interface_mp = 0;
    }
}

// ------------------------- [IMPLEMENTATION ]-------------------------
void AmbDeviceImpl::initialize()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        Control::HardwareDeviceImpl::initialize();

        std::auto_ptr< const ControlXmlParser > address(
            ControlXmlParser::getElement("Address"));
        AmbDeviceInt::channel_m = address->getLongAttribute(
            "ChannelNumber");
        AmbDeviceInt::nodeAddress_m = address->getLongAttribute(
            "NodeNumber");

        // Create the time keeper thread.
        CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());
        const std::string compName(tmpName.in());
        ACS::Time updateTime(TETimeUtil::TE_PERIOD_ACS);
        timeThread_p = getContainerServices()->getThreadManager()->
            create< AmbDeviceImpl::TimeThread, ACS::Time >(
                std::string(compName + "TimerThread").c_str(), currentTime);
        timeThread_p->setSleepTime(updateTime);
    }
    catch(const ControlExceptions::CDBErrorExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getLifeCycleEx();
    }
}

void AmbDeviceImpl::cleanUp()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(timeThread_p != 0)
    {
        getContainerServices()->getThreadManager()->stop(
            timeThread_p->getName());
        getContainerServices()->getThreadManager()->terminate(
            timeThread_p->getName());

        delete timeThread_p;
        timeThread_p = 0;
    }

    Control::HardwareDeviceImpl::cleanUp();
}

float AmbDeviceImpl::ds1820Temp(const u_int8_t temp, const int8_t sign,
    const u_int8_t countRem, const u_int8_t countPerC) const
{
    // The DS1820 data sheet states that count per c is always 16.
    // If it is not then something is very broken
    if(countPerC != 16)
    {
        return -9999.99;
    }

    short tempVal(temp);              // Copy the temp to a 16-bit
    tempVal |= (sign << 8U);          // Put the sign byte in the upper bytes
                                      // Do this before the divide so that the
                                      // sign is handled properly.
    tempVal = tempVal >> 1;           // As per the ds1850data sheet, we need
                                      // to divide by 2 AND drop the last bit.
    float floatTemp(tempVal);         // Switch to floating point for decimal
                                      // calculations
    return (floatTemp - 0.25 + (      // Use the equation in the data sheet
        (static_cast< float >(countPerC) - static_cast< float >(countRem))
            / static_cast< float >(countPerC)));
}

float AmbDeviceImpl::rawToWorldAmbientTemperature(
    const std::vector< unsigned char >& raw)
{
    // This calculate the AMBSI temperature.
    // Return the temperature in Kelvin.
    return(ds1820Temp(raw[0], raw[1], raw[2],
    raw[3]) + 273.15);
}

void AmbDeviceImpl::getDeviceUniqueId(std::string& deviceID)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    unsigned char tmpSN[8];
    Time timestamp(0ULL);

    try
    {
        interface_mp->findSN(tmpSN, channel_m, nodeAddress_m,
            timestamp);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }

    std::ostringstream uniqueId;
    uniqueId << "0x"
        << std::hex;
    for(unsigned int idx(0); idx < 8U; ++idx)
    {
        uniqueId << std::setw(2)
            << std::setfill('0')
            << static_cast< unsigned short >(tmpSN[idx]);
    }

    deviceID = uniqueId.str();
}

void AmbDeviceImpl::broadcastBasedDeviceUniqueID(std::string& deviceID)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    unsigned char tmpSN[8];
    Time timestamp(0ULL);

    try
    {
        interface_mp->findSNUsingBroadcast(tmpSN, channel_m, nodeAddress_m,
            timestamp);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }

    std::ostringstream uniqueId;
    uniqueId << "0x"
        << std::hex;
    for(unsigned int idx(0); idx < 8U; ++idx)
    {
        uniqueId << std::setw(2)
            << std::setfill('0')
            << static_cast< unsigned short >(tmpSN[idx]);
    }

    deviceID = uniqueId.str();
}
AmbDeviceImpl::TimeThread::TimeThread(const ACE_CString& name, ACS::Time& time):
    ACS::Thread(name),
    current(time)
{
    STATIC_LOG_TO_DEVELOPER(LM_DEBUG,"");
}

void AmbDeviceImpl::TimeThread::runLoop()
{
    // Get the current time and round it down to the begin of the current TE.
    current = (::getTimeStamp() / 48000ULL * 48000ULL);
}

AmbDeviceImpl::TimeThread::~TimeThread()
{
}

void AmbDeviceImpl::monitorEnc(ACS::Time* timestamp, const AmbRelativeAddr& rca,
    AmbDataLength_t& length, AmbDataMem_t* rawBytes)
{
    sem_t synchLock;
    AmbErrorCode_t status(AMBERR_PENDING);
    if(sem_init(&synchLock, 0,0) == -1)
    {
        ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream msg;
        msg << "Failed to init the synchLock semaphore.  Reason: "
            << errno
            << " ("
            << std::strerror(errno)
            << ").";
        ex.addData("Detail", msg.str());
        throw ex;
    }

    try
    {
        ///
        /// TODO
        /// Thomas, Apr 4, 2009
        ///
        /// Use a time stamp for TE related requests.
        monitor(rca, length, rawBytes, &synchLock, timestamp, &status);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        sem_destroy(&synchLock);

        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream msg;
        msg << "Failed to init the synchLock semaphore.  Reason: "
            << errno
            << " ("
            << std::strerror(errno)
            << ").";
        ex.addData("Detail", msg.str());
        throw ex;
    }

    if(sem_wait(&synchLock) == -1)
    {
        sem_destroy(&synchLock);

        ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream msg;
        msg << "Failed to wait for the synchLock semaphore.  Reason: "
            << errno
            << " ("
            << std::strerror(errno)
            << "), AMB status = "
            << status
            << ".";
        ex.addData("Detail", msg.str());
        ex.log();
    }

    if(sem_destroy(&synchLock) == -1)
    {
        ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream msg;
        msg << "Failed to destroy the synchLock semaphore.  Reason: "
            << errno
            << " ("
            << std::strerror(errno)
            << "), AMB status = "
            << status
            << ".";
        ex.addData("Detail", msg.str());
        ex.log();
    }

    if(status != AMBERR_NOERR)
    {
        switch(status)
        {
            case AMBERR_BADCMD:
            case AMBERR_BADCHAN:
            case AMBERR_UNKNOWNDEV:
            case AMBERR_INITFAILED:
            case AMBERR_WRITEERR:
            case AMBERR_READERR:
            case AMBERR_TIMEOUT:
            case AMBERR_ADDRERR:
            {
                Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
            }
                break;

            default:
            {
                Control::HardwareDeviceImpl::clearDeviceErrorCounter();
            }
                break;
        };

        ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream msg;
        msg << "The monitor request returned an error.  AMB status = "
            << status
            << ", channel = "
            << getChannelNumber()
            << ", node number = 0x"
            << std::hex
            << std::setfill('0')
            << getNodeAddress()
            << ", RCA = 0x"
            << rca
            << ".";
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex;
    }
    else
    {
        clearDeviceErrorCounter();
    }
}

void AmbDeviceImpl::commandEnc(const ACS::Time& cmdTime,
    const AmbRelativeAddr& rca, const AmbDataLength_t& length,
    const AmbDataMem_t* data)
{
    ACS::Time timestamp(0ULL);
    sem_t synchLock;
    AmbErrorCode_t status(AMBERR_PENDING);

    if(sem_init(&synchLock, 0, 0) == -1)
    {
        ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream msg;
        msg << "Failed to init the synchLock semaphore.  Reason: "
            << errno
            << " ("
            << std::strerror(errno)
            << ").";
        ex.log();
        throw ex;
    }

    try
    {
        if(cmdTime != 0ULL)
        {
            commandTE(cmdTime, rca, length, data, &synchLock, &timestamp,
            &status);
        }
        else
        {
            command(rca, length, data, &synchLock, &timestamp, &status);
        }
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        sem_destroy(&synchLock);

        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw ex;
    }

    if(sem_wait(&synchLock) == -1)
    {
        sem_destroy(&synchLock);

        ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream msg;
        msg << "Failed to wait for the synchLock semaphore.  Reason: "
            << errno
            << " ("
            << std::strerror(errno)
            << "), AMB status = "
            << status
            << ".";
        ex.addData("Detail", msg.str());
        ex.log();
    }

    if(sem_destroy(&synchLock) == -1)
    {
        ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream msg;
        msg << "Failed to destroy the synchLock semaphore.  Reason: "
            << errno
            << " ("
            << std::strerror(errno)
            << "), AMB status = "
            << status
            << ".";
        ex.addData("Detail", msg.str());
        ex.log();
    }

    if(status != AMBERR_NOERR)
    {
        switch(status)
        {
            case AMBERR_BADCMD:
            case AMBERR_BADCHAN:
            case AMBERR_UNKNOWNDEV:
            case AMBERR_INITFAILED:
            case AMBERR_WRITEERR:
            case AMBERR_READERR:
            case AMBERR_TIMEOUT:
            case AMBERR_ADDRERR:
            {
                Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
            }
                break;

            default:
            {
                Control::HardwareDeviceImpl::clearDeviceErrorCounter();
            }
                break;
        };

        ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream msg;
        msg << "The control request returned an error.  AMB status = "
            << status
            << ", channel = "
            << getChannelNumber()
            << ", node number = 0x"
            << std::hex
            << std::setfill('0')
            << getNodeAddress()
            << ", RCA = 0x"
            << rca
            << ".";
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex;
    }
    else
    {
        Control::HardwareDeviceImpl::clearDeviceErrorCounter();
    }
}

/**
 * CORBA method for getting the channel number for this device.
 */
inline CORBA::ULong AmbDeviceImpl::GET_CHANNEL_NUMBER()
{
    return getChannelNumber();
}
/**
 * CORBA method for Setting the channel number for this device.
 */
inline void AmbDeviceImpl::SET_CHANNEL_NUMBER(CORBA::ULong channelNumber)
{
    setChannelNumber(channelNumber);
}
/**
 * Override the channel number for this device.
 */
inline void AmbDeviceImpl::setChannelNumber(const unsigned long arg)
{
    AmbDeviceInt::channel_m = arg;
}
/**
 * Get the channel number for this device.
 */
inline unsigned long AmbDeviceImpl::getChannelNumber() const
{
    return AmbDeviceInt::channel_m;
}
/**
 * CORBA method for getting the node address for this device.
 */
inline CORBA::ULong AmbDeviceImpl::GET_NODE_ADDRESS()
{
    return getNodeAddress();
}
/**
 * CORBA method for setting the node address for this device.
 */
inline void AmbDeviceImpl::SET_NODE_ADDRESS(CORBA::ULong nodeAddress)
{
    setNodeAddress(nodeAddress);
}
/**
 * Get the node address for this device.
 */
inline unsigned long AmbDeviceImpl::getNodeAddress() const
{
    return AmbDeviceInt::nodeAddress_m;
}

/**
 * Override the node address for this device.
 */
inline void AmbDeviceImpl::setNodeAddress(const unsigned long arg)
{
    AmbDeviceInt::nodeAddress_m = arg;
}

/**
 * CORBA method for getting the base address for this device.
 */
inline CORBA::ULong AmbDeviceImpl::GET_BASE_ADDRESS()
{
    return getBaseAddress();
}
/**
 * CORBA method for setting the base address for this device.
 */
inline void AmbDeviceImpl::SET_BASE_ADDRESS(CORBA::ULong baseAddress)
{
    setBaseAddress(baseAddress);
}
/**
 * Override the base address for this device.
 */
inline void AmbDeviceImpl::setBaseAddress(const unsigned long arg)
{
    baseAddress_m = arg;
}

/**
 * Get the base address for this device.
 */
inline unsigned long AmbDeviceImpl::getBaseAddress() const
{
    return baseAddress_m;
}

#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(AmbDeviceImpl)
