/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * $Id$
 *
 */

/************************************************************************
 *    This is the baseclass for all AMB devices, the AMBDevIO class also
 * makes use of this class.  This abstraction is the layer which interfaces
 * directly to the Interface
 *------------------------------------------------------------------------
 */


#include "ambDeviceInt.h"
#include <ControlExceptions.h>
#include <ambDefs.h>
#include <ambInterface.h>
#include <ambServerC.h>


AmbDeviceInt::AmbDeviceInt():
    channel_m(0ULL),
    nodeAddress_m(0ULL),
    interface_mp(0)
{
}

AmbDeviceInt::~AmbDeviceInt()
{
}

void AmbDeviceInt::monitor(AmbRelativeAddr RCA, AmbDataLength_t& dataLength,
    AmbDataMem_t* data, sem_t* synchLock, Time* timestamp,
    AmbErrorCode_t* status)
{
    return (monitorTE(0, RCA, dataLength, data, synchLock, timestamp, status));
}

void AmbDeviceInt::command(AmbRelativeAddr RCA, AmbDataLength_t dataLength,
    const AmbDataMem_t* data, sem_t* synchLock, Time* timestamp,
    AmbErrorCode_t* status)
{
    return (commandTE(0, RCA, dataLength, data, synchLock, timestamp, status));
}

void AmbDeviceInt::monitorTE(ACS::Time TimeEvent, AmbRelativeAddr RCA,
    AmbDataLength_t& dataLength, AmbDataMem_t* data, sem_t* synchLock,
    Time* timestamp, AmbErrorCode_t* status)
{
    if(interface_mp == 0)
    {
        *status = AMBERR_WRITEERR;
        sem_post(synchLock);
        return;
    }

    // Call to get a monitor point at a specific time
    AmbCompletion_t* completion(new AmbCompletion_t);
    std::memset(completion, 0, sizeof(AmbCompletion_t));
    // Build the completion block
    completion->dataLength_p = &dataLength;
    completion->data_p = data;
    completion->timestamp_p = timestamp;
    completion->status_p = status;
    completion->synchLock_p = synchLock;

    AmbMessage_t message;
    // Build the message block
    message.requestType = AMB_MONITOR;
    message.channel = channel_m;
    try
    {
        message.address = createAMBAddr(nodeAddress_m, RCA);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        // Bad address
        if(completion->status_p != 0)
        {
            *completion->status_p = AMBERR_ADDRERR;
        }

        if(completion->timestamp_p != 0)
        {
            *completion->timestamp_p = 0;
        }

        if(completion->synchLock_p != 0)
        {
            sem_post(completion->synchLock_p);
        }

        delete completion;
        return;
    }

    message.dataLen = 0;
    message.completion_p = completion;
    message.targetTE = TimeEvent;

    // Send the message and wait for a return
    interface_mp->sendMessage(message);
}

void AmbDeviceInt::commandTE(ACS::Time TimeEvent, AmbRelativeAddr RCA,
    AmbDataLength_t dataLength, const AmbDataMem_t* data, sem_t* synchLock,
    Time* timestamp, AmbErrorCode_t* status)
{

    if(interface_mp == 0)
    {
        *status = AMBERR_WRITEERR;
        sem_post(synchLock);
        return;
    }

    AmbCompletion_t* completion(new AmbCompletion_t);
    std::memset(completion, 0, sizeof(AmbCompletion_t));
    // Build the completion block
    completion->timestamp_p = timestamp;
    completion->status_p = status;
    completion->synchLock_p = synchLock;

    AmbMessage_t message;
    // Build the message block
    message.requestType = AMB_CONTROL;
    message.channel = channel_m;

    try
    {
        message.address = createAMBAddr(nodeAddress_m, RCA);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        // Bad address
        if(completion->status_p != 0)
        {
            *completion->status_p = AMBERR_ADDRERR;
        }

        if(completion->timestamp_p != 0)
        {
            *completion->timestamp_p = 0;
        }

        if(completion->synchLock_p != 0)
        {
            sem_post(completion->synchLock_p);
        }

        delete completion;
        return;
    }

    message.dataLen = dataLength;
    for(unsigned int idx(0U); idx < dataLength; ++idx)
    {
        message.data[idx] = data[idx];
    }

    message.completion_p = completion;
    message.targetTE = TimeEvent;

    // Send the message and wait for a return
    interface_mp->sendMessage(message);
}

void AmbDeviceInt::monitorNextTE(AmbRelativeAddr RCA,
    AmbDataLength_t& dataLength, AmbDataMem_t* data, sem_t* synchLock,
    Time* timestamp, AmbErrorCode_t* status)
{
    if(interface_mp == 0)
    {
        *status = AMBERR_WRITEERR;
        sem_post(synchLock);
        return;
    }

    // Call to get a monitor point at a specific time
    AmbCompletion_t* completion = new AmbCompletion_t;
    std::memset(completion, 0, sizeof(AmbCompletion_t));
    // Build the completion block
    completion->dataLength_p = &dataLength;
    completion->data_p = data;
    completion->timestamp_p = timestamp;
    completion->status_p = status;
    completion->synchLock_p = synchLock;

    AmbMessage_t message;
    // Build the message block
    message.requestType = AMB_MONITOR_NEXT;
    message.channel = channel_m;
    try
    {
        message.address = createAMBAddr(nodeAddress_m, RCA);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        // Bad address
        if(completion->status_p != 0)
        {
            *completion->status_p = AMBERR_ADDRERR;
        }

        if(completion->timestamp_p != 0)
        {
            *completion->timestamp_p = 0;
        }

        if(completion->synchLock_p != 0)
        {
            sem_post(completion->synchLock_p);
        }

        delete completion;
        return;
    }

    message.dataLen = 0;
    message.completion_p = completion;
    message.targetTE = 0;

    // Send the message and wait for a return
    interface_mp->sendMessage(message);
}

void AmbDeviceInt::commandNextTE(AMBSystem::AmbRelativeAddr RCA,
    AmbDataLength_t dataLength, const AmbDataMem_t* data, sem_t* synchLock,
    ACS::Time* timestamp, AmbErrorCode_t* status)
{
    if(interface_mp == 0)
    {
        *status = AMBERR_WRITEERR;
        sem_post(synchLock);
        return;
    }

    AmbCompletion_t* completion(new AmbCompletion_t);
    std::memset(completion, 0, sizeof(AmbCompletion_t));
    // Build the completion block
    completion->timestamp_p = timestamp;
    completion->status_p = status;
    completion->synchLock_p = synchLock;

    AmbMessage_t message;
    // Build the message block
    message.requestType = AMB_CONTROL_NEXT;
    message.channel = channel_m;
    try
    {
        message.address = createAMBAddr(nodeAddress_m, RCA);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        // Bad address
        if(completion->status_p != 0)
        {
            *completion->status_p = AMBERR_ADDRERR;
        }

        if(completion->timestamp_p != 0)
        {
            *completion->timestamp_p = 0;
        }

        if(completion->synchLock_p != 0)
        {
            sem_post(completion->synchLock_p);
        }

        delete completion;
        return;
    }

    message.dataLen = dataLength;
    for(unsigned int idx(0U); idx < dataLength; ++idx)
    {
        message.data[idx] = data[idx];
    }

    message.completion_p = completion;
    message.targetTE = 0;

    // Send the message and wait for a return
    interface_mp->sendMessage(message);
}

void AmbDeviceInt::flushNode(ACS::Time TimeEvent, ACS::Time* timestamp,
    AmbErrorCode_t* status)
{
    if(interface_mp == 0)
    {
        *status = AMBERR_WRITEERR;
        return;
    }

    // Send the message and wait for a return
    try
    {
        *status = interface_mp->flush(channel_m, nodeAddress_m, TimeEvent,
            *timestamp);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        if(status != 0)
        {
            *status = AMBERR_ADDRERR;
        }
    }
}

void AmbDeviceInt::flushRCA(ACS::Time TimeEvent, AmbRelativeAddr RCA,
    ACS::Time* timestamp, AmbErrorCode_t* status)
{
    if(interface_mp == 0)
    {
        *status = AMBERR_WRITEERR;
        return;
    }

    // Send the message and wait for a return
    try
    {
        *status = interface_mp->flush(channel_m, nodeAddress_m, RCA,
            TimeEvent, *timestamp);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        if(status != 0)
        {
            *status = AMBERR_ADDRERR;
        }
    }
}
