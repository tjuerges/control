#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2009 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# jkern  2009-08-06  created
#

#************************************************************************
#   NAME
# 
#   SYNOPSIS
# 
#   DESCRIPTION
#
#   FILES
#
#   ENVIRONMENT
#
#   RETURN VALUES
#
#   CAUTIONS
#
#   EXAMPLES
#
#   SEE ALSO
#
#   BUGS     
#
#------------------------------------------------------------------------
#

import unittest
import Acspy.Clients.SimpleClient
import ControlExceptionsImpl
import ControlCommonExceptionsImpl
from ControlCommonCallbacks.SimpleCallbackImpl import SimpleCallbackImpl

class PySimpleCallbackImplTest(unittest.TestCase):

    def setUp(self):
        self.client = Acspy.Clients.SimpleClient.PySimpleClient()
        self.target = self.client.getComponent("CallbackTestTarget")

    def tearDown(self):
        self.client.releaseComponent(self.target._get_name())
        self.client.disconnect()
        
    def testCallbackConstruction(self):
        cb = SimpleCallbackImpl(self.client)

    def testSuccess(self):
        cb = SimpleCallbackImpl(self.client)
        self.target.simpleReturnOk(cb.getExternalInterface())
        cb.waitForCompletion(1.0)

    def testFailure(self):
        cb = SimpleCallbackImpl(self.client)
        self.target.simpleReturnBad(cb.getExternalInterface())
        try:
            cb.waitForCompletion(1.0)
        except ControlCommonExceptionsImpl.AsynchronousFailureExImpl:
            pass
        else:
            self.fail("Did not receive AsynchronousError exception")
            
    def testClientTimeout(self):
        cb = SimpleCallbackImpl(self.client)
        self.target.simpleClientTimeout(cb.getExternalInterface())
        try:
            cb.waitForCompletion(1.0)
        except ControlExceptionsImpl.TimeoutExImpl:
            pass
        else:
            self.fail("Did not receive Timeout exception")

    def testContainerImplementation(self):
        cb = SimpleCallbackImpl()
        self.target.simpleReturnOk(cb.getExternalInterface())
        cb.waitForCompletion(1.0)

  
        
if __name__ == '__main__':
    suite = unittest.makeSuite(PySimpleCallbackImplTest)
    unittest.TextTestRunner(verbosity=2).run(suite)


    
#
# ___oOo___
