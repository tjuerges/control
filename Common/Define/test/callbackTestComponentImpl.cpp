/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * $Id$
 *
 * who       when      what
 * --------  --------  ----------------------------------------------
 * jkern  2009-08-03  created
 */


#include <ControlCommonCallbacksTestS.h>
#include <acscomponentImpl.h>
#include <simpleCallbackImpl.h>
// for maci::SmartPtr
#include <acsComponentSmartPtr.h>
#include <ControlExceptions.h>
#include <ControlCommonExceptions.h>


class CallbackTestComponentImpl: public POA_ControlTest::CallbackTestComponent,
    public acscomponent::ACSComponentImpl
{
    public:
    CallbackTestComponentImpl(const ACE_CString& name,
        maci::ContainerServices *containerServices):
        acscomponent::ACSComponentImpl(name, containerServices)
    {
        ACS_TRACE(__PRETTY_FUNCTION__);

        target_m.release();
        target_m = containerServices-> getComponentSmartPtr<
            ControlTest::CallbackTestTarget > ("CallbackTestTarget");
    };

    virtual ~CallbackTestComponentImpl()
    {
        ACS_TRACE(__PRETTY_FUNCTION__);
    }

    // ----------- Test Cases -------------------
    bool testInstanciation()
    {
        SimpleCallbackImpl cb(getContainerServices());

        return true;
    };

    bool testSuccess()
    {
        SimpleCallbackImpl cb(getContainerServices());

        target_m->simpleReturnOk(cb.getExternalReference());
        cb.waitForCompletion(static_cast< ACS::Time >(1E6));

        return true;
    };

    bool testFailure()
    {
        SimpleCallbackImpl cb(getContainerServices());

        try
        {
            target_m->simpleReturnBad(cb.getExternalReference());
            cb.waitForCompletion(static_cast< ACS::Time >(1E6));

            return false;
        }
        catch(const ControlCommonExceptions::AsynchronousFailureExImpl& ex)
        {
            return true;
        }
    };

    bool testClientTimeout()
    {
        try
        {
            SimpleCallbackImpl cb(getContainerServices());
            target_m->simpleClientTimeout(cb.getExternalReference());
            cb.waitForCompletion(static_cast< ACS::Time >(1E6));

            return false;
        }
        catch(const ControlExceptions::TimeoutExImpl& ex)
        {
            // This is ok, we expected a timeout.
            return true;
        }
    };


    protected:
    maci::SmartPtr< ControlTest::CallbackTestTarget > target_m;
};


/////////////////////////////////////////////////
// MACI DLL support functions
/////////////////////////////////////////////////
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(CallbackTestComponentImpl)

