s/corbaloc::.*\//corbaloc::<corbaloc>/
s/Time: .*/Time: <test exec time>/
s/client handle .*/client handle <client handle>/
s/handle '.*' obtained/handle <handle> obtained/
s/MANAGER_COMPUTER_NAME=[1-9][0-9]*.[1-9][0-9]*.[1-9][0-9]*.[1-9][0-9]*/MANAGER_COMPUTER_NAME=<ip number>/g
s/Connected to [1-9][0-9]*.[1-9][0-9]*.[1-9][0-9]*.[1-9][0-9]*:[1-9][0-9]*/Connected to <ip number>/g
s/java\.endorsed\.dirs=.*/<java endorsed list path>/
s/tests in [0-9]*.[0-9]*s/tests in x.xxxs/g
