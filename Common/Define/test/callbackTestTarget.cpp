/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2009 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2009-08-03  created 
*/

/************************************************************************
*   NAME
*   
*------------------------------------------------------------------------
*/

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <ControlCommonCallbacksTestS.h>
#include <acscomponentImpl.h>
#include <ACSErrTypeOK.h>
#include <ControlExceptions.h>

class CallbackTestTargetImpl: public POA_ControlTest::CallbackTestTarget,
                                 public acscomponent::ACSComponentImpl
{
public:
  CallbackTestTargetImpl(const ACE_CString&             name,
                         maci::ContainerServices       *containerServices):
    acscomponent::ACSComponentImpl(name, containerServices),
    antennaName("Test")
  {
    ACS_TRACE(__func__);
  }

  ~CallbackTestTargetImpl(){
    ACS_TRACE(__func__);
  }
  
  /* ----------- SimpleCallback Test cases ---------------- */
  void simpleReturnOk(Control::SimpleCallback* cb) {
    /* Just return ok */
    cb->report(ACSErrTypeOK::ACSErrOKCompletion());
  }

  void simpleReturnBad(Control::SimpleCallback* cb) {
    /* Return a completion which is not ok */
    ControlExceptions::InvalidRequestCompletion c(__FILE__,__LINE__,__func__);
    cb->report(c);
  }
      
  void simpleClientTimeout(Control::SimpleCallback* cb) {
    /* This just never returns and ensures that we timeout correctly
       on the client side */
  }

  /* ----------- AntennaCallback Test cases ---------------- */
  void returnOk(Control::AntennaCallback* cb) {
    /* Just return ok */
    cb->report(antennaName.c_str(), ACSErrTypeOK::ACSErrOKCompletion());
  }

  void returnBad(Control::AntennaCallback* cb) {
    /* Return a completion which is not ok */
    ControlExceptions::InvalidRequestCompletion c(__FILE__,__LINE__,__func__);
    cb->report(antennaName.c_str(), c);
  }
      
  void clientTimeout(Control::AntennaCallback* cb) {
    /* This just never returns and ensures that we timeout correctly
       on the client side */
  }

  /* ----------- BooleanCallback Test cases ---------------- */
  void returnOkTrue(Control::BooleanCallback* cb) {
    /* Just return ok */
    cb->report(antennaName.c_str(), true, ACSErrTypeOK::ACSErrOKCompletion());
  }

  void returnOkFalse(Control::BooleanCallback* cb) {
    /* Just return ok */
    cb->report(antennaName.c_str(), false, ACSErrTypeOK::ACSErrOKCompletion());
  }

  void returnBadBoolean(Control::BooleanCallback* cb) {
    /* Return a completion which is not ok */
    ControlExceptions::InvalidRequestCompletion c(__FILE__,__LINE__,__func__);
    cb->report(antennaName.c_str(), true, c);
  }
      
  void clientTimeoutBoolean(Control::BooleanCallback* cb) {
    /* This just never returns and ensures that we timeout correctly
       on the client side */
  }

  void setAntennaName(const char* name) {
    antennaName = name;
  }

private:
  std::string antennaName;
  

};



/////////////////////////////////////////////////
// MACI DLL support functions
/////////////////////////////////////////////////
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(CallbackTestTargetImpl)
/*___oOo___*/
