/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2007-07-18  created
*/

/************************************************************************
*   NAME
*   
* 
*   SYNOPSIS
*   
* 
*   DESCRIPTION
*
*   FILES
*
*   ENVIRONMENT
*
*   COMMANDS
*
*   RETURN VALUES
*
*   CAUTIONS 
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS   
* 
*------------------------------------------------------------------------
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <walshFunction.h>
#include <iostream>

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

int walshProduct(const WalshFunction& a, const WalshFunction& b) {
  int product = 0;
  for  (int idx = 0; idx < 128; idx++) {
    if (a[idx] == b[idx]) {
      product += 1;
    } else {
      product -= 1;
    }
  }
  return product;
}

int main(int argc, char *argv[])
{
  /* This test does a couple of things, it uses the TAT framework to check
     that the WalshFunctions output by the generator class do not change and
     it checks that the functions all have the correct orthogonality behavior
     to ensure no typos creep in
  */
  fprintf(stdout,"Test access and orthogonality:\n");
  for (int idx = 0; idx < 128; idx++) {
    WalshFunction a = WalshGenerator::getWalshFunction(idx);

    fprintf(stdout,"\tWal Sequence No: %3d  Integer Encoding: 0x%016llx %016llx\n",
	    idx, a.firstInteger(), a.secondInteger());

    std::vector<unsigned char> vect = a.asVector();
    fprintf(stdout,"\t                       Vector Encoding: 0x");
    for (int idx3 = 0; idx3 < 2; idx3++) {
      for (int idx2 = 0; idx2 < 8; idx2++)
	fprintf(stdout,"%02x",vect[(8*idx3)+idx2]);
      fprintf(stdout," ");
    }

    std::cout << "\n\t\t" << a << std::endl;

    fprintf(stdout,"\t\tOrthogonality Test: ");
    bool passed = true;	    
    for (int idx2 = 0; idx2 < idx; idx2++) {
      if (walshProduct(a,WalshGenerator::getWalshFunction(idx2)) != 0) {
	if (passed) {
	  fprintf(stdout," FAILED\n");
	  passed = false;
	}
	fprintf(stdout,"\t\t\tWalsh Function WAL(%d) is not orthogonal\n",idx2);
      }
    }
    if (passed) {
      fprintf(stdout, "PASSED\n");
    }
  }

  /* Now test that exceptions are thrown when we have an illegal request */
  fprintf(stdout,"\nTest error handling: ");
  bool passed = true;
  try {
    WalshFunction a = WalshGenerator::getWalshFunction(-1);
    fprintf(stdout,"FAILED\n\tReceived Walsh Function for WAL number -1\n");
    passed = false;
  } catch (std::out_of_range& e) {}

  try {
    WalshFunction a = WalshGenerator::getWalshFunction(128);
    if (passed) {
      fprintf(stdout,"FAILED\n\tReceived Walsh Function for WAL number 128\n");
      passed = false;
    } else {
      fprintf(stdout,"\tReceived Walsh Function for WAL number 128\n");
    }
  } catch (std::out_of_range& e) {}
  if (passed) {
    fprintf(stdout,"PASSED\n");
  }
   
  return 0;
}








