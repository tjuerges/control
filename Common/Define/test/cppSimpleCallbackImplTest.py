#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2009 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# jkern  2009-08-03  created
#

import unittest
import Acspy.Clients.SimpleClient

class CppSimpleCallbackImplTest(unittest.TestCase):

    def setUp(self):
        self.ref = client.getComponent("CallbackTestComponent")

    def testInstanciation(self):
        self.assertTrue(self.ref.testInstanciation())

    def testSuccess(self):
        self.assertTrue(self.ref.testSuccess())

    def testFailure(self):
        self.assertTrue(self.ref.testFailure())

    def testClientTimeout(self):
        self.assertTrue(self.ref.testClientTimeout())

    
if __name__ == '__main__':
    client = Acspy.Clients.SimpleClient.PySimpleClient()
    suite = unittest.makeSuite(CppSimpleCallbackImplTest)
    unittest.TextTestRunner(verbosity=2).run(suite)

#
# ___oOo___
