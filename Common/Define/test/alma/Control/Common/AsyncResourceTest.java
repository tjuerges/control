/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * $Id$
 */
package alma.Control.Common;

import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;

import alma.ACSSim.Simulator;
import alma.ACSSim.SimulatorHelper;
import alma.Control.Antenna;
import alma.Control.AntennaState;
import alma.Control.AntennaStateEvent;
import alma.Control.AntennaSubstate;
import alma.ResourceExceptions.wrappers.AcsJNotYetAcquiredEx;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.container.ContainerServices;

public class AsyncResourceTest extends ComponentClientTestCase {

    private Logger logger;
    private ContainerServices container;
    private Simulator simulator;
    private ResourceState resourceState;
    
	public AsyncResourceTest() throws Exception {
        super(AsyncResourceTest.class.getName());

	}

	protected void setUp() throws Exception {
		super.setUp();		
        container = getContainerServices();
		logger = getContainerServices().getLogger();
        this.simulator = 
            SimulatorHelper.narrow(container.getDefaultComponent("IDL:alma/ACSSim/Simulator:1.0"));
        resourceState = ResourceState.UNINITIALIZED;
	}

	protected void tearDown() throws Exception {
        container.releaseComponent(simulator.name());
		super.tearDown();
	}

	public void testAsyncResourceAcquisition() throws Exception {
        
        String code = "from Control import AntennaStateEvent\n" +
                      "import Control\n" +
                      "state = Control.AntennaOperational\n" +
                      "substate = Control.AntennaNoError\n" +
                      "AntennaStateEvent('CONTROL/ALMA01', 0L, state, substate)";
        simulator.setMethod("CONTROL/ALMA01", "getAntennaState", code, 0);

		// Create first a static resource that will get an Antenna component
    	StaticResource<Antenna> res =
            new StaticResource<Antenna>(getContainerServices(),
                                        "CONTROL/ALMA01",
                                        "alma.Control.AntennaHelper");
    	// Wrap the static resource inside an asynchronous resource decorator.
    	// It is needed to implement the two abstract methods, getUpAsync() and
    	// getDownAsync().
    	AsyncResource2<Antenna> ares =
    	    new AsyncResource2<Antenna>(res, 
                                        getContainerServices(),
                                        2000,
                                        logger) {
    	    
			@Override
			public ResourceState getUpAsync() throws Exception {
			    logger.finest("Entering getUpAsync");
			    acquire();
				getComponent().controllerOperational();
				AntennaStateEvent state = getComponent().getAntennaState();
				if (state.newState == AntennaState.AntennaOperational && 
					state.newSubstate == AntennaSubstate.AntennaNoError) {
				    logger.finest("Leaving getUpAsync state is OPERATIONAL");
					return ResourceState.OPERATIONAL;
				} else {
                    logger.finest("Leaving getUpAsync state is ERROR");
					return ResourceState.ERROR;
				}
			}

			@Override
			public ResourceState getDownAsync() throws Exception {
                getComponent().controllerShutdown();
                release();
                return ResourceState.UNINITIALIZED;
			}            
    	};
		
    	// Add an observer
    	ares.addObserver(new Observer() {
            public void update(Observable o, Object arg) {
                resourceState = (ResourceState) arg; 
                logger.info("Notification received. State is "+resourceState);
            }
    	});
        
    	ares.up();

        long timeout = 5000;
        long startTime = System.currentTimeMillis();
        while(resourceState != ResourceState.OPERATIONAL) {
            long span = System.currentTimeMillis() - startTime;
            if (span > timeout) fail("Timeout");
        }
    	
        ares.down();
        while(resourceState != ResourceState.UNINITIALIZED) {
            long span = System.currentTimeMillis() - startTime;
            if (span > timeout) fail("Timeout");
        }        
	}
}
