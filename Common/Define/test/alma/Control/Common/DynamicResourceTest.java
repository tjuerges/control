/**
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2005 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * $Id$
 */

package alma.Control.Common;

// Java imports
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;

import alma.ResourceExceptions.wrappers.AcsJNotYetAcquiredEx;
import alma.acs.component.client.ComponentClientTestCase;
import alma.offline.DataCapturer;

public class DynamicResourceTest extends ComponentClientTestCase {

    private Logger logger;

    public DynamicResourceTest() throws Exception {
        super(DynamicResourceTest.class.getName());
    }

    protected void setUp() throws Exception {

        super.setUp();

        // Get the Logger.
        logger = getContainerServices().getLogger();

    }

    protected void tearDown() throws Exception {
    }

    public void testResourceAcquisition() throws Exception {
        logger.info("Creating resource...");
        DynamicResource<DataCapturer> resource = 
            new DynamicResource<DataCapturer>(getContainerServices(),
                                              "DATACAPTURE001",
                                              "IDL:alma/offline/DataCapturer:1.0");
        logger.info("Testing resource acquisition...");
        resource.acquire();
        
        DataCapturer conn = resource.getComponent();
        String compName = conn.name();
        assertEquals("DATACAPTURE001", compName);
        
        resource.release();        
    }

    public void testResourceMonitoring() throws Exception {

        DynamicResource<DataCapturer> res = 
            new DynamicResource<DataCapturer>(getContainerServices(),
                                              "DATACAPTURE001",
                                              "IDL:alma/offline/DataCapturer:1.0");        
        ObservableResource<DataCapturer> mres = new ObservableResource<DataCapturer>(res, logger);
        mres.addObserver(new Observer() {
            public void update(Observable o, Object arg) {
                ResourceState status = (ResourceState) arg;
                System.out.println("> Resource has been " + status.toString());
                if (status != ResourceState.RELEASING) {
                    Resource<DataCapturer> res = (Resource<DataCapturer>) o;
                    try {
                        System.out.println("> DataCapturer state is " + 
                                res.getComponent().componentState());
                    } catch (AcsJNotYetAcquiredEx e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        });
        mres.acquire();
        
        DataCapturer dc = mres.getComponent();
        String compName = dc.name();
        assertEquals("DATACAPTURE001", compName);
        
        mres.release();
    }
    
    public static void main(String[] args) {
        junit.textui.TestRunner.run(DynamicResourceTest.class);
    }

}
