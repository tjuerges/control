/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * $Id$
 */
package alma.Control.Common;

import java.util.logging.Logger;

import junit.framework.Test;
import junit.framework.TestSuite;
import alma.ACSSim.Simulator;
import alma.ACSSim.SimulatorHelper;
import alma.Control.Antenna;
import alma.Control.AntennaState;
import alma.Control.AntennaStateEvent;
import alma.Control.AntennaSubstate;
import alma.ResourceExceptions.wrappers.AcsJNotYetAcquiredEx;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.container.ContainerServices;
import alma.xmlstore.ArchiveConnection;

public class ResourceManagerTest extends ComponentClientTestCase {

    private ContainerServices container;
	private Logger logger;	
	private ResourceManager resMng;
    private Simulator simulator;
	
    ///////////////////////////////////////////
    // Constructors
    ///////////////////////////////////////////
    
    public ResourceManagerTest(String test) throws Exception {
        super(test);
    }
    
	public ResourceManagerTest() throws Exception {
		super(ResourceManagerTest.class.getName());
	}

    ///////////////////////////////////////////
    // Test Framework
    ///////////////////////////////////////////
    
    public static Test suite() throws Exception {
        TestSuite suite = new TestSuite();
        String testSuite = System.getProperty("suite");
        if (testSuite == null || testSuite.equals("ALL")) {
            suite.addTest(new ResourceManagerTest("testAddResource"));
            suite.addTest(new ResourceManagerTest("testWrongAndAsyncResource"));
        } else {
            if (testSuite.equals("1")) {
                suite.addTest(new ResourceManagerTest("testAddResource"));
            } else if (testSuite.equals("2")) {
                suite.addTest(new ResourceManagerTest("testWrongAndAsyncResource"));
            }
        }
        return suite;
    }
    
    
	protected void setUp() throws Exception {
		super.setUp();
        container = getContainerServices();
        logger = container.getLogger();
		resMng = ResourceManager.getInstance(container);
        this.simulator = 
            SimulatorHelper.narrow(container.getDefaultComponent("IDL:alma/ACSSim/Simulator:1.0"));
	}

	protected void tearDown() throws Exception {
        container.releaseComponent(simulator.name());
		super.tearDown();
	}

    ///////////////////////////////////////////
    // Tests
    ///////////////////////////////////////////
    
	public void testAddResource() throws Exception {
		        
		resMng.addResource(new StaticResource<ArchiveConnection>(container,
                "ARCHIVE_CONNECTION", "alma.xmlstore.ArchiveConnectionHelper"));
        resMng.addResource(new StaticResource<Antenna>(container, "CONTROL/ALMA01",
                "alma.Control.AntennaHelper"));
		resMng.acquireResources();
		
		Resource<ArchiveConnection> archRes = resMng.getResource("ARCHIVE_CONNECTION");
		ArchiveConnection archConn = archRes.getComponent();
		assertEquals("ARCHIVE_CONNECTION", archConn.name());
		
        Resource<Antenna> antRes = resMng.getResource("CONTROL/ALMA01");
        logger.info("Antenna Status = "+antRes.status());
        Antenna antenna = antRes.getComponent();
        assertEquals("CONTROL/ALMA01", antenna.name());
        
		resMng.releaseResources();
		
	}
    
    public void testWrongAndAsyncResource() throws Exception {
        
        String code = "from Control import AntennaStateEvent\n" +
                      "import Control\n" +
                      "state = Control.AntennaOperational\n" +
                      "substate = Control.AntennaError\n" +
                      "AntennaStateEvent('CONTROL/ALMA01', 0L, state, substate)";
        simulator.setMethod("CONTROL/ALMA01", "getAntennaState", code, 0);
        
        resMng.addResource(new StaticResource<ArchiveConnection>(container,
            "ARCHIVE_CONNECTION", "alma.xmlstore.ArchiveConnectionHelper"));
        resMng.addResource(new StaticResource<Antenna>(container, "BAD_RESOURCE",
            "alma.Bad.Resource"));
        resMng.addResource(new StaticResource<Antenna>(container, "BAD_ANTENNA",
            "alma.Control.AntennaHelper"));
        logger.info("Acquiring static resources...");
        resMng.acquireResources();
        
        StaticResource<Antenna> res =
            new StaticResource<Antenna>(getContainerServices(),
                    "CONTROL/ALMA01", "alma.Control.AntennaHelper");
        AsyncResource2<Antenna> asyncres = new AsyncResource2<Antenna>(res, 
                getContainerServices(), 2000, logger) {
            
            @Override
            public ResourceState getUpAsync() throws Exception {
                logger.finest("Entering getUpAsync");
                acquire();
                getComponent().controllerOperational();
                AntennaStateEvent state = getComponent().getAntennaState();
                if (state.newState == AntennaState.AntennaOperational && 
                    state.newSubstate == AntennaSubstate.AntennaNoError) {
                    logger.finest("Leaving getUpAsync state is OPERATIONAL");
                    return ResourceState.OPERATIONAL;
                } else {
                    logger.finest("Leaving getUpAsync state is ERROR");
                    return ResourceState.ERROR;
                }
            }

            @Override
            public ResourceState getDownAsync() throws Exception {
                getComponent().controllerShutdown();
                release();
                return ResourceState.UNINITIALIZED;
            }
        };
        resMng.addResource(asyncres);
        logger.info("Acquiring asynchronous resources...");
        resMng.acquireResources(3000);
        
        resMng.releaseResources();
        
    }
}

// __oOo__