/*
ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2009 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*/
/** 
 * @author  jkern
 * @version $Id$
 * @since    
 */

package alma.Control.Common;

import alma.acs.component.ComponentQueryDescriptor;

import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.container.ContainerServices;

import alma.Control.CommonCallbacks.SimpleCallbackImpl;

import alma.ControlTest.CallbackTestTarget;
import alma.ControlTest.CallbackTestTargetHelper;

import alma.ControlCommonExceptions.wrappers.AcsJAsynchronousFailureEx;
import alma.ControlExceptions.wrappers.AcsJTimeoutEx;


public class SimpleCallbackImplTest extends ComponentClientTestCase {

    public SimpleCallbackImplTest(String test) throws Exception {
        super(test);
    }

    public SimpleCallbackImplTest() throws Exception {
        super(SimpleCallbackImplTest.class.getName());
    }

    private ContainerServices cs;

    CallbackTestTarget target = null;

    ////////////////////////////////////////////////////////////////////
    // Test fixture methods
    ////////////////////////////////////////////////////////////////////

    protected void setUp() throws Exception {
        super.setUp();
        ComponentQueryDescriptor spec = new ComponentQueryDescriptor();
        spec.setComponentType("IDL:alma/ControlTest/CallbackTestTarget:1.0");
        this.cs = getContainerServices();
        this.target = CallbackTestTargetHelper.
            narrow(cs.getDynamicComponent(spec, false));
    }

    protected void tearDown() throws Exception {
        cs.releaseComponent(target.name());
        super.tearDown();
    }

    ////////////////////////////////////////////////////////////////////
    // Test cases
    ////////////////////////////////////////////////////////////////////
    public void testTrivialCase() throws Exception {
        /* This simple case just creates a Callback Object and waits
           without actually requiring any responses */
        SimpleCallbackImpl cb = new SimpleCallbackImpl(cs);
    }

    public void testSuccess() throws Exception {
        SimpleCallbackImpl cb = new SimpleCallbackImpl(cs);
        target.simpleReturnOk(cb.getExternalInterface());
        cb.waitForCompletion(1);
    }

    public void testFailure() throws Exception {
        SimpleCallbackImpl cb = new SimpleCallbackImpl(cs);
        target.simpleReturnBad(cb.getExternalInterface());
        try {
            cb.waitForCompletion(1);
            fail("Did not recieve expected AysnchronousFailureEx");
        } catch (AcsJAsynchronousFailureEx ex) {
            /* This is expected */
            return;
        }
    }

    public void testClientTimeout() throws Exception {
        SimpleCallbackImpl cb = new SimpleCallbackImpl(cs);
        target.simpleClientTimeout(cb.getExternalInterface());
        try {
            cb.waitForCompletion(1);
            fail("Did not recieve expected TimeoutEx");
        } catch (AcsJTimeoutEx ex) {
            /* This is expected */
            return;
        }
    }
}
