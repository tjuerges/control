/*
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2005 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * $Id$
 */
package alma.Control.Common;

import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;

import alma.Control.Antenna;
import alma.ResourceExceptions.wrappers.AcsJNotYetAcquiredEx;
import alma.acs.component.client.ComponentClientTestCase;
import alma.xmlstore.ArchiveConnection;

public class StaticResourceTest extends ComponentClientTestCase {

    private Logger logger;

    public StaticResourceTest() throws Exception {
        super(StaticResourceTest.class.getName());
    }

    protected void setUp() throws Exception {
        super.setUp();
        logger = getContainerServices().getLogger();
    }

    protected void tearDown() throws Exception {}

    public void testResourceAcquisition() throws Exception {
    	logger.info("Creating resource ARCHIVE_CONNECTION");
    	StaticResource<ArchiveConnection> resource = 
            new StaticResource<ArchiveConnection>(getContainerServices(),
                    "ARCHIVE_CONNECTION", "alma.xmlstore.ArchiveConnectionHelper");
        assertEquals(ResourceState.UNINITIALIZED, resource.status());
    	resource.acquire();
        assertEquals(ResourceState.INITIALIZED, resource.status());
        resource.startup();
        assertEquals(ResourceState.OPERATIONAL, resource.status());
    	ArchiveConnection conn = resource.getComponent();
    	String compName = conn.name();
    	assertEquals("ARCHIVE_CONNECTION", compName);
        resource.shutdown();
        assertEquals(ResourceState.INITIALIZED, resource.status());        
    	resource.release();
        assertEquals(ResourceState.UNINITIALIZED, resource.status());
        
        logger.info("Creating resource CONTROL/ALMA01");
        StaticResource<Antenna> res2 = 
            new StaticResource<Antenna>(getContainerServices(),
                "CONTROL/ALMA01", "alma.Control.AntennaHelper");
        res2.acquire();
        res2.release();
    }

    public void testResourceMonitoring() throws Exception {
    	
    	StaticResource<ArchiveConnection> res =
            new StaticResource<ArchiveConnection>(getContainerServices(),
    			"ARCHIVE_CONNECTION", "alma.xmlstore.ArchiveConnectionHelper");
    	ObservableResource<ArchiveConnection> mres =
            new ObservableResource<ArchiveConnection>(res, logger);
    	mres.addObserver(new Observer() {
            private int count = 0;
            private ResourceState[] changes = {
                    ResourceState.INITIALIZED,
                    ResourceState.OPERATIONAL,
                    ResourceState.INITIALIZED,
                    ResourceState.UNINITIALIZED
            };
    		public void update(Observable o, Object arg) {
    			ResourceState status = (ResourceState) arg;
                assertEquals(changes[count++], status);
    			System.out.println(">>> Resource is " + status.toString());
    			if (status == ResourceState.OPERATIONAL) {
    				Resource<ArchiveConnection> res = (Resource<ArchiveConnection>) o;
    				try {
                        System.out.println(">>> Archive state is " + 
                                res.getComponent().componentState().toString());
                    } catch (AcsJNotYetAcquiredEx ex) {
                        ex.printStackTrace();
                    }
    			}
    		}
    	});
    	mres.acquire();
        mres.startup();
    	
    	ArchiveConnection conn = mres.getComponent();
    	String compName = conn.name();
    	assertEquals("ARCHIVE_CONNECTION", compName);
    	
        mres.shutdown();
    	mres.release();
    }
        
    public static void main(String[] args) {
    	junit.textui.TestRunner.run(StaticResourceTest.class);
    }

}
