/*
ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2009 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*/
/** 
 * @author  jkern
 * @version $Id$
 * @since    
 */

package alma.Control.Common;

import java.util.HashMap;
import java.util.Iterator;

import alma.acs.component.ComponentQueryDescriptor;

import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.container.ContainerServices;

import alma.Control.CommonCallbacks.BooleanCallbackImpl;

import alma.ControlTest.CallbackTestTarget;
import alma.ControlTest.CallbackTestTargetHelper;

import alma.ControlCommonExceptions.wrappers.AcsJAsynchronousFailureEx;
import alma.ControlExceptions.wrappers.AcsJTimeoutEx;


public class BooleanCallbackImplTest extends ComponentClientTestCase {

    public BooleanCallbackImplTest(String test) throws Exception {
        super(test);
    }

    public BooleanCallbackImplTest() throws Exception {
        super(BooleanCallbackImplTest.class.getName());
    }

    private ContainerServices cs;

    private HashMap<String, CallbackTestTarget> targets = 
        new HashMap<String, CallbackTestTarget>();


    ////////////////////////////////////////////////////////////////////
    // Test fixture methods
    ////////////////////////////////////////////////////////////////////

    protected void setUp() throws Exception {
        super.setUp();
        ComponentQueryDescriptor spec = new ComponentQueryDescriptor();
        spec.setComponentType("IDL:alma/ControlTest/CallbackTestTarget:1.0");
        this.cs = getContainerServices();
        this.targets.put("DV01", CallbackTestTargetHelper.
                         narrow(cs.getDynamicComponent(spec, false)));
        this.targets.put("DV02", CallbackTestTargetHelper.
                         narrow(cs.getDynamicComponent(spec, false)));
        this.targets.put("PM01", CallbackTestTargetHelper.
                         narrow(cs.getDynamicComponent(spec, false)));
        this.targets.put("PM03", CallbackTestTargetHelper.
                         narrow(cs.getDynamicComponent(spec, false)));
        this.targets.put("DA41", CallbackTestTargetHelper.
                         narrow(cs.getDynamicComponent(spec, false)));
        for (Iterator<String> iter = targets.keySet().iterator();
             iter.hasNext();) {
            final String name = iter.next();
            targets.get(name).setAntennaName(name);
        }
    }

    protected void tearDown() throws Exception {
        for (Iterator<String> iter = targets.keySet().iterator();
             iter.hasNext();) {
            cs.releaseComponent(targets.get(iter.next()).name());
        }
        super.tearDown();
    }

    ////////////////////////////////////////////////////////////////////
    // Test cases
    ////////////////////////////////////////////////////////////////////
    public void testTrivialCase() throws Exception {
        /* This simple case just creates a Callback Object and waits
           without actually requiring any responses */
        BooleanCallbackImpl cb = new BooleanCallbackImpl(cs);
        cb.waitForCompletion(1);
    }

    public void testSingleAntennaSuccessTrue() throws Exception {
        BooleanCallbackImpl cb = new BooleanCallbackImpl(cs);
        cb.addExpectedResponse("DV01");
        targets.get("DV01").returnOkTrue(cb.getExternalInterface());
        cb.waitForCompletion(1);
        assert(cb.allTrue());
        assert(!cb.allFalse());
    }

    public void testSingleAntennaSuccessFalse() throws Exception {
        BooleanCallbackImpl cb = new BooleanCallbackImpl(cs);
        cb.addExpectedResponse("DV01");
        targets.get("DV01").returnOkFalse(cb.getExternalInterface());
        cb.waitForCompletion(1);
        assert(cb.allFalse());
        assert(!cb.allTrue());
    }

    public void testSingleAntennaFailure() throws Exception {
        BooleanCallbackImpl cb = new BooleanCallbackImpl(cs);
        cb.addExpectedResponse("DV01");
        targets.get("DV01").returnBadBoolean(cb.getExternalInterface());
        try {
            cb.waitForCompletion(1);
            fail("Did not recieve expected AysnchronousFailureEx");
        } catch (AcsJAsynchronousFailureEx ex) {
            /* This is expected */
            return;
        }
    }

    public void testSingleClientTimeout() throws Exception {
        BooleanCallbackImpl cb = new BooleanCallbackImpl(cs);
        cb.addExpectedResponse("DV01");
        targets.get("DV01").clientTimeoutBoolean(cb.getExternalInterface());
        try {
            cb.waitForCompletion(1);
            fail("Did not recieve expected TimeoutEx");
        } catch (AcsJTimeoutEx ex) {
            /* This is expected */
            return;
        }
    }

    public void testMultipleSuccess() throws Exception {
        BooleanCallbackImpl cb = new BooleanCallbackImpl(cs);
        for (Iterator<String> iter = targets.keySet().iterator();
             iter.hasNext();) {
            final String name = iter.next();
            cb.addExpectedResponse(name);
            targets.get(name).returnOkTrue(cb.getExternalInterface());
        }
        cb.waitForCompletion(1);
        assert(cb.allTrue());
        assert(!cb.allFalse());
    }

    public void testSomeTimeouts() throws Exception {
        BooleanCallbackImpl cb = new BooleanCallbackImpl(cs);
        cb.setAllowableFailures(1);
        for (Iterator<String> iter = targets.keySet().iterator();
             iter.hasNext();) {
            final String name = iter.next();
            cb.addExpectedResponse(name);
            if (name == "PM01" || name == "PM03") {
                targets.get(name).clientTimeoutBoolean(cb.getExternalInterface());
            } else {
                targets.get(name).returnOkTrue(cb.getExternalInterface());
            }
        }
        try {
            cb.waitForCompletion(1);
            fail("Did not recieve expected TimeoutEx");
        } catch (AcsJTimeoutEx ex) {
            /* This is expected */
            return;
        }
    }

    public void testPermissableTimeouts() throws Exception {
        BooleanCallbackImpl cb = new BooleanCallbackImpl(cs);
        cb.setAllowableFailures(1);
        for (Iterator<String> iter = targets.keySet().iterator();
             iter.hasNext();) {
            final String name = iter.next();
            cb.addExpectedResponse(name);
            if (name == "PM01") {
                targets.get(name).clientTimeoutBoolean(cb.getExternalInterface());
            } else {
                targets.get(name).returnOkTrue(cb.getExternalInterface());
            }
        }
        cb.waitForCompletion(1);
    }

    public void testSomeFailures() throws Exception {
        BooleanCallbackImpl cb = new BooleanCallbackImpl(cs);
        cb.setAllowableFailures(1);
        for (Iterator<String> iter = targets.keySet().iterator();
             iter.hasNext();) {
            final String name = iter.next();
            cb.addExpectedResponse(name);
            if (name == "PM01" || name == "PM03") {
                targets.get(name).returnBadBoolean(cb.getExternalInterface());
            } else {
                targets.get(name).returnOkTrue(cb.getExternalInterface());
            }
        }
        try {
            cb.waitForCompletion(1);
            fail("Did not recieve expected AysnchronousFailureEx");
        } catch (AcsJAsynchronousFailureEx ex) {
            /* This is expected */
            return;
        }
    }

    public void testPermissableFailures() throws Exception {
        BooleanCallbackImpl cb = new BooleanCallbackImpl(cs);
        cb.setAllowableFailures(1);
        for (Iterator<String> iter = targets.keySet().iterator();
             iter.hasNext();) {
            final String name = iter.next();
            cb.addExpectedResponse(name);
            if (name == "PM01") {
                targets.get(name).returnBadBoolean(cb.getExternalInterface());
            } else {
                targets.get(name).returnOkTrue(cb.getExternalInterface());
            }
        }
        cb.waitForCompletion(1);
    }

    public void testPermissibleTrue() throws Exception {
        BooleanCallbackImpl cb = new BooleanCallbackImpl(cs);
        cb.setAllowableFailures(1);
        for (Iterator<String> iter = targets.keySet().iterator();
             iter.hasNext();) {
            final String name = iter.next();
            cb.addExpectedResponse(name);
            if (name == "PM01") {
                targets.get(name).returnOkFalse(cb.getExternalInterface());
            } else {
                targets.get(name).returnOkTrue(cb.getExternalInterface());
            }
        }
        cb.waitForCompletion(1);
        assert(!cb.allTrue());
        assert(!cb.allFalse());
        assert(cb.allowableTrue());
    }
    
    public void testPermissibleFalse() throws Exception {
        BooleanCallbackImpl cb = new BooleanCallbackImpl(cs);
        cb.setAllowableFailures(1);
        for (Iterator<String> iter = targets.keySet().iterator();
             iter.hasNext();) {
            final String name = iter.next();
            cb.addExpectedResponse(name);
            if (name != "PM01") {
                targets.get(name).returnOkFalse(cb.getExternalInterface());
            } else {
                targets.get(name).returnOkTrue(cb.getExternalInterface());
            }
        }
        cb.waitForCompletion(1);
        assert(!cb.allTrue());
        assert(!cb.allFalse());
        assert(cb.allowableFalse());
    }
}
