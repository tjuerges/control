// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

#ifndef PROGRESSCALLBACKIMPL_H
#define PROGRESSCALLBACKIMPL_H

#include <ControlCommonCallbacksS.h>
#include <Semaphore.h>
#include <acscommonC.h>
#include <acserr.h>
#include <deque>

namespace maci
{
    class ContainerServices;
};

namespace Control {
    class ProgressCallbackImpl: public POA_Control::ProgressCallback
    {
    public:
        ProgressCallbackImpl(maci::ContainerServices* cs);
        virtual ~ProgressCallbackImpl();
        
        /// --------- Corba Interface ------------
        virtual void report(const ACSErr::Completion&);
        
        /// ---------  Local Interface --------------
        Control::ProgressCallback_ptr getExternalReference();
        
        /// This method waits until the report method is called or the timeout
        /// period is reached. The timeout can be in seconds or 100ns ticks
        /// (depending on which version you use). It returns the completion
        /// provided when the client called report function.
        /// \exception ControlExceptions::TimeoutEx will be thrown if there
        /// is a timeout
        /// \exception ControlCommonExceptions::OSErrorEx will be thrown if
        /// there is a system error while waiting
        CompletionImpl waitForCompletion(double timeout);
        CompletionImpl waitForCompletion(ACS::TimeInterval timeout);
              
        /// Deactivate the CORBA object. Once this is called the report
        /// function can no longer be called.
        void deactivateCallback();
        
    private:
        /// No default ctor.
        ProgressCallbackImpl();
        
        /// No copy ctor.
        ProgressCallbackImpl(const ProgressCallbackImpl&);
        
        /// No assignment operator.
        ProgressCallbackImpl& operator=(const ProgressCallbackImpl&);
        
        /// Member method containing the COBRA reference to this object
        Control::ProgressCallback_var externalReference_m;
        
        /// Cached reference to containerServices, only non-Null if the
        /// container service constructor was used.
        maci::ContainerServices* containerServices_m;
        
        /// Semaphore used to signal when a response comes in
        ACE_Semaphore completionSemaphore_m;
        
        /// Location used to store the returned completion object
        std::deque<ACSErr::Completion> completion_m;
    };
};
#endif // PROGRESSCALLBACKIMPL_H
