#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef simpleCallbackImpl_h
#define simpleCallbackImpl_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


/************************************************************************
 * This class encapsulates a Callback implementing the
 * Control::AntennaCallback interface.  This class should be used only once
 * and for a single responding object.
 *----------------------------------------------------------------------
 */


#include <ControlCommonCallbacksS.h>
#include <Semaphore.h>
#include <acstime.h>

namespace maci
{
    class ContainerServices;
};

class SimpleCallbackImpl: public POA_Control::SimpleCallback
{
    public:
    SimpleCallbackImpl(maci::ContainerServices* cs);
    virtual ~SimpleCallbackImpl();

    /// --------- Corba Interface ------------
    virtual void report(const ACSErr::Completion&);

    /// ---------  Local Interface --------------
    Control::SimpleCallback_ptr getExternalReference();

    /// This method waits until the report method is called or the
    /// timeout period is reached.
    /// \exception ControlExceptions::TimeoutExImpl will be thrown if there
    /// is a timeout
    /// \exception ControlException::OSErrorExImpl will be thrown if there
    /// is a system error while waiting
    void waitForCompletion(ACS::Time timeout);


    private:
    /// No default ctor.
    SimpleCallbackImpl();

    /// No copy ctor.
    SimpleCallbackImpl(const SimpleCallbackImpl&);

    /// No assignment operator.
    SimpleCallbackImpl& operator=(const SimpleCallbackImpl&);

    /// Private method responsible for removing the CORBA reference to the
    /// Callback object
    void deactivateCallback();

    /// Member method containing the COBRA reference to this object
    Control::SimpleCallback_var externalReference_m;

    /// Cached reference to containerServices, only non-Null if the
    /// container service constructor was used.
    maci::ContainerServices* containerServices_m;

    /// Semaphore used to signal when a response comes in
    ACE_Semaphore completionSemaphore_m;

    /// Location used to store the returned completion object
    ACSErr::Completion completion_m;
};
#endif
