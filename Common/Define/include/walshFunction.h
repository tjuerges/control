#ifndef WALSHFUNCTION_H
#define WALSHFUNCTION_H
/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 *
 * who       when      what
 * --------  --------  ----------------------------------------------
 * jkern  2007-07-18  created
 */


#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif


#include <bitset>
#include <string>
#include <stdexcept>
#include <vector>

/**
 * This is a convenience class for holding the walsh function and translating
 * it into unsigned long long integers.
 */
class WalshFunction: public std::bitset< 128 >
{
    public:
    WalshFunction();
    WalshFunction(const WalshFunction&);

    template< class Ch, class Tr, class A >
    WalshFunction(const std::basic_string< Ch, Tr, A >& str);

    /**
     * These methods are provided to allow simple conversion to unsigned long
     * long values (as used by the hardware). The function firstInteger
     * returns the First 64 states of the Walsh function encoded as an
     * unsigned long long integer, similarly the secondInteger method
     * returns the remaining 64 states.
     */
    unsigned long long firstInteger() const;
    unsigned long long secondInteger() const;

    /** Another useful interface is to have the walsh function as
     * a vector of unsigned bytes.
     */
    std::vector< unsigned char > asVector() const;
    std::vector< unsigned char > firstVector() const;
    std::vector< unsigned char > secondVector() const;

};

/**
 * This class provides a standard lookup table for all walsh functions, this
 * should be the only place that walsh functions are created.
 */
class WalshGenerator
{
    public:
    /**
     * This method returns the Walsh function taken from the set, ordered in
     * WAL order.  Valid values are zero through 127 inclusive.
     * \exception std::out_of_range
     */
    static const WalshFunction& getWalshFunction(const int walNumber);

    private:
    WalshGenerator();

    /**
     * This array holds all of the walsh functions.  Following ALMA Memo 565
     * these are "A set of Walsh functions with positive phasing arranged in
     * WAL order".
     */
    static const WalshFunction walshFunctions[];
};

#endif /*!WALSHFUNCTION_H*/
