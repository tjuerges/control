/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ControlCommonInterfaces.idl
 */

#ifndef _CONTROLCOMMONCALLBACKSTEST_IDL_
#define _CONTROLCOMMONCALLBACKSTEST_IDL_

#pragma prefix "alma"

#include <acscomponent.idl>
#include <acserr.idl>
#include <ControlCommonCallbacks.idl>

/**
 * These IDL definitions and structures are implemented by Control
 * and are used internally within Control.  They have no relevance
 * to any subsystem other than Control.
 */
module ControlTest {

  interface CallbackTestComponent : ACS::ACSComponent {
    
    boolean testInstanciation();
    boolean testSuccess();
    boolean testFailure();
    boolean testClientTimeout();
  };

  interface CallbackTestTarget : ACS::ACSComponent {
    /* --------- Simple Callback Tests ------------- */
    oneway void simpleReturnOk(in Control::SimpleCallback cb);
    oneway void simpleReturnBad(in Control::SimpleCallback cb);
    oneway void simpleClientTimeout(in Control::SimpleCallback cb);


    /* --------- Antenna Callback Tests ------------- */
    oneway void returnOk(in Control::AntennaCallback cb);
    oneway void returnBad(in Control::AntennaCallback cb);
    oneway void clientTimeout(in Control::AntennaCallback cb);

    /* --------- Boolean Callback Test -------------- */
    oneway void returnOkTrue(in Control::BooleanCallback cb);
    oneway void returnOkFalse(in Control::BooleanCallback cb);
    oneway void returnBadBoolean(in Control::BooleanCallback cb);
    oneway void clientTimeoutBoolean(in Control::BooleanCallback cb);

    void setAntennaName(in string name);
  };
};

#endif // !CONTROLCOMMONCALLBACKS
