#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2009 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# jkern  2009-08-06  created
#

#************************************************************************
#   NAME
# 
#   SYNOPSIS
# 
#   DESCRIPTION
#
#   FILES
#
#   ENVIRONMENT
#
#   RETURN VALUES
#
#   CAUTIONS
#
#   EXAMPLES
#
#   SEE ALSO
#
#   BUGS     
#
#------------------------------------------------------------------------
#
import Control__POA
from threading import Event
from ControlExceptionsImpl import TimeoutExImpl
from ControlCommonExceptionsImpl import AsynchronousFailureExImpl
from ControlCommonExceptionsImpl import AsynchronousFailureCompletionImpl
import Acspy.Common.Err 
import Acspy.Clients.SimpleClient

from CCL.Container import activateOffShoot

class SimpleCallbackImpl(Control__POA.SimpleCallback):
    def __init__(self, corbaServices = None):
        '''
        The argument here (corbaServices) can be any class which
        provides an activateOffShoot method.
        '''
        self._event = Event()
        self._completion = None

        if corbaServices is not None:
            self._externalInterface = corbaServices.activateOffShoot(self)
        else:
            self._externalInterface = activateOffShoot(self)

    def report(self, completion):
        '''
        This is the external interface to this Callback.  The result
        is passed back as part of the completion.
        '''
        self._completion = completion
        self._event.set()

    def getExternalInterface(self):
        '''
        This method returns the external CORBA interface for this object.
        '''
        return self._externalInterface

    def waitForCompletion(self, timeout):
        '''
        Method will wait for the completion to be returned, timeout is in
        seconds and floating point values are allowed.

        This method will throw ControlExceptionsImpl::TimeoutExImpl and
        ControlCommonExceptionsImpl::AsynchronousErrorExImpl in case of error
        '''
        self._event.wait(timeout)

        if self._completion is None:
            #We must have timed out
            ex = TimeoutExImpl()
            raise ex

        Acspy.Common.Err.addComplHelperMethods(self._completion)
        if not self._completion.isErrorFree():
            ex = AsynchronousFailureExImpl(
                AsynchronousFailureCompletionImpl(self._completion))
            raise ex

    
#
# ___oOo___
