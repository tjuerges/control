/*ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

/** 
 * @author  jkern
 * @version $Id$
 * @since    
 */

package alma.Control.CommonCallbacks;

import alma.ACSErr.Completion;

import alma.acs.container.ContainerServices;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;

import alma.Control.AntennaCallback;
import alma.Control.AntennaCallbackPOATie;
import alma.Control.AntennaCallbackOperations;
import alma.Control.AntennaCallbackHelper;

public class AntennaCallbackImpl 
    extends AntennaCallbackBase<Object ,AntennaCallback, AntennaCallbackPOATie>
    implements AntennaCallbackOperations {
    
    public AntennaCallbackImpl(ContainerServices cs)
        throws AcsJContainerServicesEx {
        super(cs);
        tieClass = new AntennaCallbackPOATie(this);
        externalInterface =  AntennaCallbackHelper.
            narrow(containerServices.activateOffShoot(tieClass));
    }

    protected void deactivateOffshoot() {
        try {
            containerServices.deactivateOffShoot(tieClass);
        } catch  (AcsJContainerServicesEx ex) {
            logger.warning("Failed to deactive Callback object");
            ex.log(logger);
        }
    }

   /* -------------- CORBA Interface ------------------ */
    /* This is a one-way void call so we cannot throw exceptions*/
    public void report(String antennaId, Completion status) {
        report(antennaId,  new ResponseStructure(null,status));
    }
}
