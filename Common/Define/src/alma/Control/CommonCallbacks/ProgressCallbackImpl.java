// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

package alma.Control.CommonCallbacks;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import alma.ACSErr.Completion;
import alma.Control.ProgressCallback;
import alma.Control.ProgressCallbackHelper;
import alma.Control.ProgressCallbackPOA;
import alma.ControlExceptions.wrappers.AcsJTimeoutEx;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.acs.container.ContainerServices;
import alma.acs.exceptions.AcsJCompletion;

public class ProgressCallbackImpl extends ProgressCallbackPOA {

    private ContainerServices cs;

    private ProgressCallback externalInterface = null;

    private Semaphore semaphore = null;

    private List<AcsJCompletion> responseCompletion = null;

    public ProgressCallbackImpl(ContainerServices cs)
            throws AcsJContainerServicesEx {
        this.cs = cs;
        semaphore = new Semaphore(0);
        externalInterface = ProgressCallbackHelper.narrow(cs.activateOffShoot(this));
        responseCompletion = new ArrayList<AcsJCompletion>();
    }

    /* -------------- CORBA Interface ------------------ */
    /* This is a one-way void call so we cannot throw exceptions */
    public void report(Completion returnedCompletion) {
        responseCompletion.add(AcsJCompletion.fromCorbaCompletion(returnedCompletion));
        semaphore.release();
    }

    /* --------------- Java Interface ----------------- */
    public ProgressCallback getExternalInterface() {
        return externalInterface;
    }

    public void reset() {
    	int permits = semaphore.drainPermits();
    	cs.getLogger().fine("When resetting callback, it darined: "+permits+" permits.");
    }
    
    /* The timeout is specified in seconds, not ACS::TimeInterval units */
    public AcsJCompletion waitForCompletion(double timeout)
            throws AcsJTimeoutEx {
        boolean status;
        String msg = "Timed out";
        try {
            status = semaphore.tryAcquire((long) (timeout * 1E6),
                    TimeUnit.MICROSECONDS);
        } catch (InterruptedException ex) {
            status = false;
            msg = "Interruped while";
        }

        if (status == false) {
            AcsJTimeoutEx nEx = new AcsJTimeoutEx();
            nEx.setProperty("Detail", msg + " waiting for a response."
                    + " (Timeout= " + timeout + " s).");
            nEx.log(cs.getLogger());
            throw nEx;
        }

        AcsJCompletion retVal = responseCompletion.remove(0); 
        return retVal;
    }

    public void deactivateOffshoot() {
        try {
            cs.deactivateOffShoot(this);
        } catch (AcsJContainerServicesEx ex) {
            ex.setProperty("Detail", "Failed to deactive Callback object");
            ex.log(cs.getLogger());
        }
    }
}
