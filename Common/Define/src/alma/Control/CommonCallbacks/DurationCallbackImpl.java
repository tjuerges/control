/*ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

/** 
 * @author  jkern
 * @version $Id$
 * @since    
 */

package alma.Control.CommonCallbacks;

import alma.ACSErr.Completion;

import alma.acs.container.ContainerServices;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;

import alma.Control.DurationCallback;
import alma.Control.DurationCallbackPOATie;
import alma.Control.DurationCallbackOperations;
import alma.Control.DurationCallbackHelper;

import java.util.Iterator;


/**
 * Insert a Class/Interface comment.
 * 
 */
public class DurationCallbackImpl 
    extends AntennaCallbackBase<Long, DurationCallback, 
            DurationCallbackPOATie>
    implements DurationCallbackOperations {
    
    public DurationCallbackImpl(ContainerServices cs)
        throws AcsJContainerServicesEx {
        super(cs);
        tieClass = new DurationCallbackPOATie(this);
        externalInterface =  DurationCallbackHelper.
            narrow(containerServices.activateOffShoot(tieClass));
    }

    protected void deactivateOffshoot() {
        try {
            containerServices.deactivateOffShoot(tieClass);
        } catch  (AcsJContainerServicesEx ex) {
            logger.warning("Failed to deactive Callback object");
            ex.log(logger);
        }
    }
    
    /* -------------- CORBA Interface ------------------ */
    /* This is a one-way void call so we cannot throw exceptions*/
    public void report(String antennaId, long value, Completion status) {
        report(antennaId,  new ResponseStructure(value,status));
    }

     /* --------------- Java Interface ----------------- */
     /* This method will return the smallest of all of the responses which 
        were without error*/
    public long getMinimumTime() {
        Long minimum = null;

        for (Iterator<String> iter = responses.keySet().iterator();
             iter.hasNext();) {
            ResponseStructure response = responses.get(iter.next());
            if (response != null && !response.getCompletion().isError()) {
                if ((minimum == null) || (minimum > response.getValue())) {
                    minimum = response.getValue();
                }
            }
        }
        return minimum;
    }
    
    public long getMaximumTime() {
        Long maximum = null;

        for (Iterator<String> iter = responses.keySet().iterator();
             iter.hasNext();) {
            ResponseStructure response = responses.get(iter.next());
            if (response != null && !response.getCompletion().isError()) {
                if ((maximum == null) || (maximum < response.getValue())) {
                    maximum = response.getValue();
                }
            }
        }
        return maximum;
    }
}
