/*ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

/** 
 * @author  jkern
 * @version $Id$
 * @since    
 */

package alma.Control.CommonCallbacks;

import alma.ACSErr.Completion;

import alma.acs.container.ContainerServices;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;

import alma.Control.BooleanCallback;
import alma.Control.BooleanCallbackPOATie;
import alma.Control.BooleanCallbackOperations;
import alma.Control.BooleanCallbackHelper;

import java.util.Iterator;


/**
 * Insert a Class/Interface comment.
 * 
 */
public class BooleanCallbackImpl 
    extends AntennaCallbackBase<Boolean,BooleanCallback, BooleanCallbackPOATie>
    implements BooleanCallbackOperations {
    
    public BooleanCallbackImpl(ContainerServices cs)
        throws AcsJContainerServicesEx {
        super(cs);
        tieClass = new BooleanCallbackPOATie(this);
        externalInterface =  BooleanCallbackHelper.
            narrow(containerServices.activateOffShoot(tieClass));
    }

    protected void deactivateOffshoot() {
        try {
            containerServices.deactivateOffShoot(tieClass);
        } catch  (AcsJContainerServicesEx ex) {
            logger.warning("Failed to deactive Callback object");
            ex.log(logger);
        }
    }
    
    /* -------------- CORBA Interface ------------------ */
    /* This is a one-way void call so we cannot throw exceptions*/
    public void report(String antennaId, boolean value, Completion status) {
        report(antennaId,  new ResponseStructure(value,status));
    }

     /* --------------- Java Interface ----------------- */
     /* This method will return true if all of the responses which 
        were without error returned true */
    public boolean allTrue() {
        int numberFalse = 0;
        for (Iterator<String> iter = responses.keySet().iterator();
             iter.hasNext();) {
            final String antName = iter.next();
            ResponseStructure response = responses.get(antName);
            if (response != null && !response.getCompletion().isError()
                && !response.getValue()) {
                numberFalse++;
            }
        }
        return numberFalse == 0;
    }

    /* This method will return true if at most allowable failures of
       the responses which were without error returned false */
    public boolean allowableTrue(){
        int numberFalse = 0;
        for (Iterator<String> iter = responses.keySet().iterator();
             iter.hasNext();) {
            final String antName = iter.next();
            ResponseStructure response = responses.get(antName);
            if (response != null &&  !response.getCompletion().isError()
                && !response.getValue()) {
                numberFalse++;
            }
        }
        return numberFalse <= getAllowableFailures();
    }

    /* This method will return true if all of the responses which 
       were without error returned false */
    public boolean allFalse(){
        int numberTrue = 0;
        for (Iterator<String> iter = responses.keySet().iterator();
             iter.hasNext();) {
            final String antName = iter.next();
            ResponseStructure response = responses.get(antName);
            if (response != null  &&  !response.getCompletion().isError()
                && response.getValue()) {
                numberTrue++;
            }
        }
        return numberTrue == 0;
    }

    /* This method will return true if at most allowable failures of
       the responses which were without error returned true */
    public boolean allowableFalse(){
        int numberTrue = 0;
        for (Iterator<String> iter = responses.keySet().iterator();
             iter.hasNext();) {
            final String antName = iter.next();
            ResponseStructure response = responses.get(antName);
            if (response != null  &&  !response.getCompletion().isError()
                && response.getValue()) {
                numberTrue++;
            }
        }
        return numberTrue < getAllowableFailures();
    }


}
