/*ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

/** 
 * @author  jkern
 * @version $Id$
 * @since    
 */

package alma.Control.CommonCallbacks;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;

import alma.ACSErr.Completion;
import alma.acs.exceptions.AcsJCompletion;

import alma.acs.container.ContainerServices;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;

import alma.Control.SimpleCallback;
import alma.Control.SimpleCallbackPOA;
import alma.Control.SimpleCallbackHelper;


import alma.ControlExceptions.wrappers.AcsJTimeoutEx;
import alma.ControlCommonExceptions.wrappers.AcsJAsynchronousFailureEx;

/**
 * Insert a Class/Interface comment.
 * 
 */
public class SimpleCallbackImpl extends SimpleCallbackPOA {
    
    private ContainerServices containerServices;
    
    private SimpleCallback externalInterface = null;
    
    private Semaphore semaphore;
    
    private AcsJCompletion responseCompletion = null;
    
    protected Logger logger;
    
    public SimpleCallbackImpl(ContainerServices cs)
        throws AcsJContainerServicesEx {
        containerServices = cs;
        logger = cs.getLogger();

        semaphore = new Semaphore(1);
        semaphore.drainPermits();
        externalInterface =  SimpleCallbackHelper.
            narrow(containerServices.activateOffShoot(this));
    }

    /* -------------- CORBA Interface ------------------ */
    /* This is a one-way void call so we cannot throw exceptions*/
    public void report(Completion returnedCompletion) {
        responseCompletion = 
            AcsJCompletion.fromCorbaCompletion(returnedCompletion);
        semaphore.release();
    }

    /* --------------- Java Interface ----------------- */
    public SimpleCallback getExternalInterface() 
    {
        return this.externalInterface;
    }


    /* Here the timeout is specified in seconds, not ACS::Time units */
    public void waitForCompletion(long timeout)
        throws AcsJAsynchronousFailureEx, AcsJTimeoutEx
    {
        boolean status;
        try {
            status = semaphore.tryAcquire(timeout, TimeUnit.SECONDS);
            deactivateOffshoot();
        } catch (InterruptedException ex) {
            AcsJTimeoutEx nEx = new AcsJTimeoutEx();
            nEx.setProperty("Error Message", "Interrupted while waiting"
                            + " for response.).");
            deactivateOffshoot();
            nEx.log(logger);
            throw nEx;
        }
        
        if (!status) {
            AcsJTimeoutEx nEx = new AcsJTimeoutEx();
            nEx.setProperty("Error Message", "Timed out waiting for response."
                            +" (Timeout= "  + timeout + " s).");
            nEx.log(logger);
            throw nEx;
        }

        /* Ok at this point we have the completion */
        if (responseCompletion.isError()) {
            AcsJAsynchronousFailureEx ex = new AcsJAsynchronousFailureEx
                (responseCompletion.getAcsJException()); 
            ex.setProperty("Message", "Failure detected in" 
                                     + " Asynchronous invocation.");
            ex.log(logger);
            throw ex;
        }
    }

    private void deactivateOffshoot() {
        try {
            containerServices.deactivateOffShoot(this);
        } catch  (AcsJContainerServicesEx ex) {
            logger.warning("Failed to deactive Callback object");
            ex.log(logger);
        }
    }
}
