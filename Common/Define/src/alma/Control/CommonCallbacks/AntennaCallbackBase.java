/*ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

/** 
 * @author  jkern
 * @version $Id$
 * @since    
 */

package alma.Control.CommonCallbacks;

import java.util.HashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;
import java.util.Iterator;
import java.util.ArrayList;

import alma.ACSErr.Completion;
import alma.acs.exceptions.AcsJCompletion;

import alma.acs.container.ContainerServices;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;

import alma.ControlExceptions.wrappers.AcsJTimeoutEx;
import alma.ControlExceptions.wrappers.AcsJInvalidRequestEx;
import alma.ControlCommonExceptions.wrappers.AcsJAsynchronousFailureEx;

import alma.Control.Common.Util;


/**
 * Insert a Class/Interface comment.
 * 
 */
public abstract class AntennaCallbackBase<Value_t, External_t, TieClass_t> {

    protected class ResponseStructure {
        Value_t   value;
        Completion  status;

        public ResponseStructure(Value_t  value,
                                 Completion status) {
            this.value  = value;
            this.status = status;
        }

        public AcsJCompletion getCompletion(){
            return AcsJCompletion.fromCorbaCompletion(status);
        }
        
        public Value_t         getValue(){return value;}
        
    };
    
    final private int MAX_EXPECTED_RESPONSES = 128;
    
    protected ContainerServices containerServices;
    
    protected External_t externalInterface = null;
    protected TieClass_t tieClass = null;

    private Semaphore semaphore;
    
    private int pendingResponses;
    
    /* This is the number of allowed failures in the callback
       all failures are logged, but no exeption is thrown unless
       the number of failures exceeds this value */
    private int allowableFailures = 0;

    protected HashMap<String, ResponseStructure> responses;    
    protected Logger logger;
    
    public AntennaCallbackBase(ContainerServices cs)
        throws AcsJContainerServicesEx {
        containerServices = cs;
        logger = cs.getLogger();

        responses = new HashMap<String, ResponseStructure>();
        semaphore = new Semaphore(MAX_EXPECTED_RESPONSES);
        semaphore.drainPermits();
        pendingResponses = 0;
    }

    /* -------------- Report Method  ------------------ */
    /* This is a one-way void call so we cannot throw exceptions*/
    protected void report(String antennaId, ResponseStructure rspStruct) {
        if (responses.containsKey(antennaId)) {
            responses.put(antennaId, rspStruct);
            semaphore.release();
        } else {
            logger.warning("The antenna " + antennaId 
                           + " has reported to the callback,"
                           + " although it was not expected");
        }
    }

    /* --------------- Java Interface ----------------- */
    public External_t getExternalInterface() 
    {
        return this.externalInterface;
    }

    public void addExpectedResponse(String antennaId)
        throws AcsJInvalidRequestEx
    {
        if (responses.containsKey(antennaId)) {
            AcsJInvalidRequestEx ex = new AcsJInvalidRequestEx();
            ex.setProperty("Message", "The antenna " + antennaId +
                           "is already registered with this callback");
            ex.log(logger);
            throw ex;
        }
        
        pendingResponses++;
        if (pendingResponses > MAX_EXPECTED_RESPONSES) {
            // Too many requests, throw an exception
            AcsJInvalidRequestEx ex = new AcsJInvalidRequestEx();
            ex.setProperty("Message", "Unable to add an more expected" 
                           + " responses.  Limit exceeded (Limit = "
                           + MAX_EXPECTED_RESPONSES + ")");
            ex.log(logger);
            throw ex;
        }
        responses.put(antennaId, null);
    }

    public int getAllowableFailures() {
        return allowableFailures;
    }

    public void setAllowableFailures(int AllowableFailures) {
        allowableFailures = AllowableFailures;
    }

    /* Here the timeout is specified in seconds, not ACS::Time units */
    public void waitForCompletion(long timeout)
        throws AcsJAsynchronousFailureEx, AcsJTimeoutEx
    {
        waitForAllResponses(timeout);
        deactivateOffshoot();
        checkCompletions();
    }


    /* This method returns true if all responses have been recieved
       if not then it will return false */
    private void waitForAllResponses(long timeOut)
    {
        boolean status;
        try {
            semaphore.tryAcquire(pendingResponses, timeOut, TimeUnit.SECONDS);
        } catch (InterruptedException ex) {
            logger.fine("Interruption detected while waiting for responses");
        }
    }

    private void checkCompletions() 
        throws AcsJAsynchronousFailureEx, AcsJTimeoutEx {
        ArrayList<String> noCompletion = new ArrayList<String>();
        HashMap<String, AcsJCompletion> errorCompletion =
            new HashMap<String, AcsJCompletion>();

        for (Iterator<String> iter = responses.keySet().iterator();
             iter.hasNext();) {
            final String antName = iter.next();
            ResponseStructure response = responses.get(antName);
            if (response == null) {
                noCompletion.add(antName);
            } else {
                if (response.getCompletion().isError()) {
                    errorCompletion.put(antName, response.getCompletion());
                }
            }
        }

        // Log all bad or missing completions
        final int numBadAntennas = noCompletion.size()+errorCompletion.size();
        if (numBadAntennas > 0) {
            /* Deal with the case of not responding first */
            AcsJTimeoutEx jTimeEx = new AcsJTimeoutEx();
            if (noCompletion.size() > 0 ){
                String msg = "Unable to determining if antenna(s) ";
                for (Iterator<String> iter = noCompletion.iterator();
                     iter.hasNext();) {
                    String antName = iter.next();
                    msg += antName + " ";
                }
                msg += "have completed the command " 
                    + "(as their completion is null).";
                jTimeEx.setProperty("Message", msg);
                jTimeEx.log(logger);
            }


            /* Now handle the case of non-success */
            AcsJAsynchronousFailureEx jAsychFailEx =
                new AcsJAsynchronousFailureEx(); 
            if (errorCompletion.size() > 0) {
                String msg = "Errors reported by antenna(s) ";
                for (Iterator<String> iter =
                         errorCompletion.keySet().iterator(); iter.hasNext();){
                    final String antName = iter.next();
                    msg += antName + " ";
                    jAsychFailEx.setProperty(antName, 
                                             Util.getNiceErrorTraceString(errorCompletion.get(antName).getAcsJException().getErrorTrace()));
                }
                msg += " in asynchronous invocation.";
                jAsychFailEx.setProperty("Message", msg);
                jAsychFailEx.log(logger);
            }
            
            // If enough antennas are bad, throw an exception.
            if (numBadAntennas > allowableFailures) {
                // Throw a timeout if any timed out
                if (errorCompletion.size() > allowableFailures) {
                    throw jAsychFailEx;
                } else {
                    throw jTimeEx;
                }
            }
        }
    }

    protected abstract void deactivateOffshoot();
}
