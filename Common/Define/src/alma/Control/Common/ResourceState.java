/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * $Id$
 */
package alma.Control.Common;

public class ResourceState {
        public static ResourceState UNINITIALIZED = new ResourceState("UNINITIALIZED");
        public static ResourceState INITIALIZED = new ResourceState("INITIALIZED");
        public static ResourceState OPERATIONAL = new ResourceState("OPERATIONAL");
        public static ResourceState ERROR = new ResourceState("ERROR");
        public static ResourceState ACQUIRING = new ResourceState("ACQUIRING");
        public static ResourceState RELEASING = new ResourceState("RELEASING");
        public static ResourceState STARTING_UP = new ResourceState("STARTING_UP");
        public static ResourceState SHUTTING_DOWN = new ResourceState("SHUTTING_DOWN");
        
        private String state;
        public ResourceState(String state) { this.state = state; }
        public String toString() { return state; }
}
