/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File Name.java
 */
package alma.Control.Common;

/**
 * The Name class is a collection of static constants,
 * mostly strings, that are used throughout Control.
 * 
 * @version 1.1 Nov 14, 2005
 * @author Allen Farris
 */
public class Name {

    public static final String ACSManager					= "ACS.manager";
    public static final String ControlMasterComponent		= "CONTROL_Master_COMP";
    public static final String ArchiveComponent				= "ARCHIVE_CONNECTION";
    public static final String ArchiveIdentifierComponent	= "ARCHIVE_IDENTIFIER";
    public static final String ArchiveQuery					= "/*";
    public static final String TelConfigSchema				= "TelConfig";
    public static final String Master						= "CONTROL/MASTER";
    public static final String NCExternal					= "CONTROL_SYSTEM";
    public static final String NCInternal					= "CONTROL_REALTIME";
    public static final String OperatorComponent			= "CONTROL/Operator";
    public static final String DeviceMonitorComponent		= "CONTROL/DeviceMonitoring";
    public static final String MasterClockComponent			= "CONTROL/TIMESOURCE";
    public static final String CorrelatorMasterComponent	= "CORR_MASTER_COMP";
    public static final String CorrelatorObsComponent		= "CORR/OBSERVATION_CONTROL";
    public static final String WeatherStationComponent		= "CONTROL/WeatherStationMC";
    
    public static final String AntennaComponentType  		= "IDL:alma/Control/Antenna:1.0";
    public static final String AntennaComponentCode			= "alma.Control.Device.Antenna.AntennaImplCreator";
    public static final String AntennaComponentContainer	= "ACC/javaContainer";
    
    public static final String AutomaticArrayComponentType  = "IDL:alma/Control/AutomaticArray:1.0";
    public static final String AutomaticArrayComponentCode  = "alma.Control.Array.AutomaticArrayImplCreator";
    public static final String AutomaticArrayComponentContainer = "ACC/javaContainer";

    public static final String DataCaptureComponentType 	= "IDL:alma/offline/DataCapturer:1.0";
    public static final String DataCaptureComponentCode		= "alma.offline.DataCapturerImpl.DataCapturerHelper";
    public static final String DataCaptureComponentContainer= "ACC/javaContainer";

    public static final String ScriptExecutorComponentType 	= "IDL:alma/Control/ScriptExecutor:1.0";
    public static final String ScriptExecutorComponentCode	= "alma.Control.Script.ScriptExecutorImplCreator";
    public static final String ScriptExecutorComponentContainer= "ACC/javaContainer";

    public static final String ExecutionStateComponentType 	= "IDL:alma/Control/ExecutionState:1.0";
    public static final String ExecutionStateComponentCode	= "alma.Control.ExecState.ExecutionStateImplCreator";
    public static final String ExecutionStateComponentContainer= "ACC/javaContainer";

    public static final String DelayServerComponentType 	= "IDL:alma/Control/SkyDelayServer:1.0";
    public static final String DelayServerComponentCode		= "alma.Control.DelayServer.DelayServerHelper";
    public static final String DelayServerComponentContainer= "ACC/javaContainer";

    public static final long SleeptimeBetweenAntennaCreate	= 1000L;
    
    // The following are prefixes for generating 
    // various ALMA names.
    public static final String ControlPrefix				= "CONTROL/";
	public static final String MainAntennaPrefix 			= "ALMA";
	public static final String ACAAntennaPrefix 			= "ACA";
	public static final String WeatherStationPrefix			= "Weather";
	public static final String ArrayPrefix					= "Array";
	public static final String PadPrefix					= "Pad";
	public static final String ComputerPrefix				= "Computer";
	public static final String DataCapturePrefix			= ControlPrefix + "DataCapture";
	public static final String ScriptExecutorPrefix			= ControlPrefix + "ScriptExecutor";
	public static final String ExecutionStatePrefix			= ControlPrefix + "ExecutionState";
	public static final String DelayServerPrefix			= ControlPrefix + "DelayServer";
	public static final String AutomaticArrayPrefix			= ControlPrefix + "AutomaticArray";
	public static final String ExecutePrefix				= ControlPrefix + "Execute";

    // Counters
    private static int mainAntennaNumber = 0;
    private static int acaAntennaNumber = 0;
    private static int wsNumber = 0;
    private static int arrayNumber = 0;
    private static int padNumber = 0;
    private static int computerNumber = 0;
    private static int dataCaptureNumber = 0;
	
	private static final String gen3digit(String prefix, int n) {
	    if (n < 10)
	        return prefix + "00" + Integer.toString(n);
	    else if (n < 100)
	        return prefix + "0" + Integer.toString(n);
	    else
	        return prefix + Integer.toString(n);
	}
	private static final String gen2digit(String prefix, int n) {
	    if (n < 10)
	        return prefix + "0" + Integer.toString(n);
	    else
	        return prefix + Integer.toString(n);
	}
	
	/**
	 * Generate a name for a Main antenna.
	 * @return The name for a Main antenna.
	 */
	public static final String genMainAntennaName() {
	    ++mainAntennaNumber;
	    return gen3digit(MainAntennaPrefix,mainAntennaNumber);
	}
	public static void setMainAntennaCounter(int n) {
	    mainAntennaNumber = n;
    }
    
	/**
	 * Generate a name for an ACA antenna.
	 * @return The name for an ACA antenna.
	 */
	public static final String genACAAntennaName() {
	    ++acaAntennaNumber;
	    return gen2digit(ACAAntennaPrefix,acaAntennaNumber);
	}
    public static void setACAAntennaCounter(int n) {
        acaAntennaNumber = n;
    }

	/**
	 * Generate a name for a weather station.
	 * @return The name for a weather station.
	 */
	public static final String genWSName() {
	    ++wsNumber;
	    return gen2digit(WeatherStationPrefix,wsNumber);
	}
    public static void setWeatherStationCounter(int n) {
        wsNumber = n;
    }

	/**
	 * Generate a name for an ALMA array.
	 * @return The name for an ALMA array.
	 */
	public static final synchronized String genArrayName() {
	    ++arrayNumber;
	    if (arrayNumber == 1000)
	        arrayNumber = 1;
	    return gen3digit(ArrayPrefix,arrayNumber);
	}
    public static void setArrayCounter(int n) {
        arrayNumber = n;
    }
	
	/**
	 * Generate a name for an ALMA pad.
	 * @return The name for an ALMA pad.
	 */
	public static final String genPadName() {
	    ++padNumber;
	    return gen3digit(PadPrefix,padNumber);
	}
    public static void setPadCounter(int n) {
        padNumber = n;
    }

	/**
	 * Generate a name for an ALMA computer.  A computer is a device
	 * that operates under the control of a simgle instance of an
	 * operating system.
	 * @return The name for an ALMA computer.
	 */
	public static final String genComputerName() {
	    ++computerNumber;
	    return gen3digit(ComputerPrefix,computerNumber);
	}
    public static void setComputerCounter(int n) {
        computerNumber = n;
    }
	
	/**
	 * Generate a name for a DataCapture component.
	 * @return The name for a DataCapture component.
	 */
	public static final String genDataCaptureName() {
	    ++dataCaptureNumber;
	    if (dataCaptureNumber == 1000)
	        dataCaptureNumber = 1;
	    return gen3digit(DataCapturePrefix,dataCaptureNumber);
	}
    public static void setDataCaptureCounter(int n) {
        dataCaptureNumber = n;
    }

	/**
	 * Generate a name for a Script Executor.
	 * @return The name for a Script Executor.
	 */
	public static final String genScriptExecutorName() {
	    return gen3digit(ScriptExecutorPrefix,arrayNumber);
	}

	/**
	 * Generate a name for an ExecutionState component.
	 * @return The name for an ExecutionState component.
	 */
	public static final String genExecutionStateName() {
	    return gen3digit(ExecutionStatePrefix,arrayNumber);
	}

	/**
	 * Generate a name for a Delay Server component.
	 * @return The name for a Delay Server component.
	 */
	public static final String genDelayServerName() {
	    return gen3digit(DelayServerPrefix,arrayNumber);
	}

	/**
	 * Generate a name for an Execute thread.
	 * @return The name for an Execute thread.
	 */
	public static final String genExecuteName() {
	    return gen3digit(ExecutePrefix,dataCaptureNumber);
	}

}
