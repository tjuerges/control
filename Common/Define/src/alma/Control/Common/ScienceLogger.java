// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

package alma.Control.Common;

import alma.acs.logging.AcsLogger;
import alma.log_audience.SCILOG;

public class ScienceLogger extends AudienceLogger {

    public ScienceLogger(AcsLogger logger) {
        super(logger, SCILOG.value);
    }
    
    public static void severe(String msg, AcsLogger logger) {
        AudienceLogger.severe(msg, SCILOG.value, logger);
    }

    public static void warning(String msg, AcsLogger logger) {
        AudienceLogger.warning(msg, SCILOG.value, logger);
    }

    public static void info(String msg, AcsLogger logger) {
        AudienceLogger.info(msg, SCILOG.value, logger);
    }

    public static void config(String msg, AcsLogger logger) {
        AudienceLogger.config(msg, SCILOG.value, logger);
    }

    public static void fine(String msg, AcsLogger logger) {
        AudienceLogger.fine(msg, SCILOG.value, logger);
    }

    public static void finer(String msg, AcsLogger logger) {
        AudienceLogger.finer(msg, SCILOG.value, logger);
    }

    public static void finest(String msg, AcsLogger logger) {
        AudienceLogger.finest(msg, SCILOG.value, logger);
    }
}
