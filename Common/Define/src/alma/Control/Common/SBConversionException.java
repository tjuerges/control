/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File SBConversionException.java
 */
package alma.Control.Common;

/**
 * Description
 *
 * @version 1.0 Sep 18, 2005
 * @author Allen Farris
 */
public class SBConversionException extends Exception {

    /**
     * The UID of the APDM scheduling block.
     */
    private String sbId;
    /**
     * The name of the field in the APDM scheduling block in error.
     */
    private String fieldName;
    /**
     * A message indicating the error.
     */
    private String errMessage;
    
    /**
     * An exception denoting an error in converting a
     * scheduling block from APDM format into Control format.
     */
    public SBConversionException(String sbId, String fieldName,String errMessage ) {
        super("Error in APDM scheduling block " + sbId + ", field " + 
                fieldName + ": " + errMessage);
        this.sbId = sbId;
        this.fieldName = fieldName;
        this.errMessage = errMessage;
    }
    
    /**
     * @return Returns the errMessage.
     */
    public String getErrMessage() {
        return errMessage;
    }

    /**
     * @return Returns the fieldName.
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * @return Returns the sbId.
     */
    public String getSbId() {
        return sbId;
    }

    
}
