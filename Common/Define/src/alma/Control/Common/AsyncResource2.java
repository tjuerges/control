/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * $Id$
 */
package alma.Control.Common;

import java.util.logging.Logger;

import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.acs.container.ContainerServices;

/**
 * Asynchronous Resource Decorator.
 * 
 * This class is a decorator for another Resource type. It adds the capability
 * of specifying asynchronous operations. In the same way that the ObservableResource
 * class, it will use Java Observer/Observable mechanism to notify clients when the
 * asynchronous operation has completed.
 * 
 * This class is abstract. Children classes need to implement the startupAsync(), shutdownAsync()
 * and status() operations. Only the startupAsync() operation is asynchronous, the shutdownAsync()
 * operation is synchronous (this could change in the future, although currently I don't
 * have a use case to justify this.)
 * 
 * @author rhiriart@nrao.edu
 *
 * @param <C> Component class this Resource is wrapping.
 */
public abstract class AsyncResource2<C> extends ObservableResource<C> {

    /**
     * This thread (Monitoring Thread) monitors another thread (the Executing Thread)
     * that is in charge of executing an asynchronous operation. When the Executing Thread
     * finish the execution of the aynchronous operation, it invokes the finish() operation in
     * the Monitoring thread. The Monitoring thread will then notify the Observers of
     * this Resource. The Monitoring thread will wait for the Executing Thread to finish
     * before a given timeout. If the timeout is reached, the Executing Thread will be
     * interrupted. 
     * 
     * @author rhiriart@nrao.edu
     *
     */
    private class AsyncMonitor implements Runnable {

        Thread monitoredThread;
        long timeout;
        ResourceState newState = null;
        VerboseLogger vlogger;
        
        public AsyncMonitor(long timeout) {
            this.timeout = timeout;
            this.vlogger = new VerboseLogger(logger.getInternalLogger(),
                                             "AsyncMonitor"); 
        }
        
        /**
         * Sets the monitored Execution Thread. It can't be passed in the constructor
         * because of a circular problem (an instance of this class needs to be passed
         * in the constructor of the Execution Thread.)
         * @param monitoredThread Monitored Execution Thread.
         */
        public void setMonitoredThread(Thread monitoredThread) {
            this.monitoredThread = monitoredThread;            
        }
        
        /**
         * This function is called when this thread starts.
         */
        public synchronized void run() {
            
            long startTime = System.currentTimeMillis();
            while (newState == null) {
                try {
                    wait(timeout);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
                long timeSpan = System.currentTimeMillis() - startTime;
                vlogger.finer("run", "timeSpan="+timeSpan);
                if (timeSpan >= timeout) {
                    monitoredThread.interrupt();
                    newState = ResourceState.ERROR;
                    resource.setState(newState);
                    break;
                }
            }
            vlogger.finer(getName(), "run", "Notifying observers with state="+newState);
            setChanged();
            notifyObservers(newState);
        }

        /**
         * This function is called by the Executing thread when it has finished
         * the asynchonous operation.
         * @param state New Resource state.
         */
        public synchronized void finish(ResourceState state) {
            vlogger.finer(getName(), "finish", "Notifying state "+state.toString());
            newState = state;
            notify();
        }
    }
    
    /**
     * This thread (an Execution thread) will execute the startup operation 
     * asynchonously.
     * @author rhiriart@nrao.edu
     * 
     */
    private class AsyncStartupController implements Runnable {
        AsyncMonitor monitor;
        
        public AsyncStartupController(AsyncMonitor monitor) {
            this.monitor = monitor;
        }
        
        public void run() {
            VerboseLogger vlogger =
                new VerboseLogger(logger.getInternalLogger(),
                                  "AsyncStartupController");
            try {
                vlogger.fine("Starting async operation");
                resource.setState(ResourceState.STARTING_UP);
                ResourceState state = getUpAsync();
                resource.setState(state);
            } catch (Exception ex) {
                logger.fine("run", "Error starting up resource "+getName()+
                                   ". Exception has been set.");
                setException(ex);
                resource.setState(ResourceState.ERROR);
            }
            vlogger.finer("run", "state="+resource.status().toString());
            monitor.finish(resource.status());
        }        
    }

    /**
     * This thread (an Execution thread) will execute the shutdown operation 
     * asynchonously.
     * @author rhiriart@nrao.edu
     * 
     */
    private class AsyncShutdownController implements Runnable {
        AsyncMonitor monitor;
        
        public AsyncShutdownController(AsyncMonitor monitor) {
            this.monitor = monitor;
        }
        
        public void run() {
            VerboseLogger vlogger =
                new VerboseLogger(logger.getInternalLogger(),
                                  "AsyncShutdownController");
            try {
                resource.setState(ResourceState.SHUTTING_DOWN);
                ResourceState state = getDownAsync();
                resource.setState(state);
            } catch (Exception ex) {
                logger.fine("run", "Error shutting down resource "+getName()+
                                   ". Exception has been set.");
                setException(ex);
                resource.setState(ResourceState.ERROR);
            }
            vlogger.finer("run", "state="+resource.status().toString());
            monitor.finish(resource.status());
        }        
    }
    
    /** ACS Container Services */
    private ContainerServices container;
    
    /** Timeout (ms) for the execution of asynchronous operations */
	private long timeout;
    
    /** 
     * The Monitor Thread. It will wait with a timeout for the asynchronous
     * operation executed by the Execution thread to finish.
     */
    private Thread monitorThread;
    
    /**
     * The Execution Thread. It will execute an operation asynchronous and inform
     * the Monitor Thread when done.
     */
    private Thread executionThread;
    
    /**
     * The logger, either a common Java Logger (in test cases) or the ACS Logger
     * in production.
     */
    private VerboseLogger logger;

	/**
     * Main constructor.
     * 
	 * @param resource 
	 */
	public AsyncResource2(InternalResource<C> resource,
	                      ContainerServices container,
                          long timeout,
                          Logger logger) {
		super(resource, logger);
        this.container = container;
        if (logger == null)
            this.logger = new VerboseLogger(container.getLogger(),
                                            "AsyncResource2");
        else
            this.logger = new VerboseLogger(logger, "AsyncResource2");
		this.timeout = timeout;
        
	}

    /**
     * Acquire the Resource (i.e., get the underlying ACS Component).
     */
    @Override
	public void acquire() throws AcsJResourceErrorEx {		
		// This will perform an ACS getComponent()
		super.acquire();
	}

    /**
     * Release the Resource
     * (which will release the underlying ACS Component.)
     */
    @Override
    public void release() {
        super.release(); // This will release the underlying ACS component.
    }       

    /**
     * Startup the Resource. This will change the state of the Resource from
     * INITIALIZED to OPERTIONAL, or ERROR, depending on the outcome of the
     * startup operation.
     * 
     * In the case of asynchronous resources, startup() will be called, followed
     * by startupAsync() that will be called asynchronously. The invoker will
     * be notified of the outcome of the operation by means of Java
     * Observer/Observable mechanism, i.e., its update() method will be called
     * when the Resource changes state.
     */
    @Override
    public void up() throws AcsJResourceErrorEx {
        AsyncMonitor startupMonitor = new AsyncMonitor(timeout);
        monitorThread = container.getThreadFactory().newThread((startupMonitor));
        AsyncStartupController startupController =
            new AsyncStartupController(startupMonitor);
        executionThread = container.getThreadFactory().newThread(startupController);
        startupMonitor.setMonitoredThread(executionThread);
        
        monitorThread.start();
        executionThread.start();        
    }

    /**
     * Shuts down the Resource. This will change the state of the Resource from
     * OPERATIONAL to INITIALIZED, or ERROR, depending on the outcome of the
     * shutdown operation(s). This method will also release the component,
     * right after shutdown has finished. This behavior deviates from the
     * general interface, but it is in most cases what is needed and the
     * alternatives are quite complex. The fundamental problem with an
     * asynchronous shutdown is that the caller needs to be notified in some
     * way when the shutdown has finished, in order to call release(). It can't
     * call release() right away, because then the component will be deactivated
     * in the middle of shutdown operations. 
     * 
     * In the case of asynchronous resources, shutdown() will be called, followed
     * by shutdownAsync() that will be called asynchronously. The invoker will
     * be notified of the outcome of the operation by means of Java
     * Observer/Observable mechanism, i.e., its update() method will be called
     * when the Resource changes state.
     */
    @Override
    public void down() throws AcsJResourceErrorEx {        
        AsyncMonitor shutdownMonitor = new AsyncMonitor(timeout);
        monitorThread = container.getThreadFactory().newThread((shutdownMonitor));
        AsyncShutdownController shutdownController =
            new AsyncShutdownController(shutdownMonitor);
        executionThread = container.getThreadFactory().newThread(shutdownController);
        shutdownMonitor.setMonitoredThread(executionThread);
        
        monitorThread.start();
        executionThread.start();        
    }
    
    /**
     * Cancels the executing startup operation. If this thread is not
     * running this method does nothing. 
     *
     */
	public void cancel() {
        if (executionThread != null && executionThread.isAlive()) {
            executionThread.interrupt();
            if (monitorThread != null && monitorThread.isAlive())
                monitorThread.interrupt();
        
            setChanged();
            notifyObservers(ResourceState.ERROR);
        }
	}

    /**
     * Startup operation. It will be executed asynchronously. Needs to be
     * implemented in child classes.
     */
	public abstract ResourceState getUpAsync() throws Exception;

	/**
	 * Shutdown operation. It will be executed asynchronously. Needs to be
	 * implemented in child classes.
	 */
	public abstract ResourceState getDownAsync() throws Exception;
	
}

// __oOo__