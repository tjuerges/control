package alma.Control.Common;

/**
 * An internal or private interface for a Resource. This interface is used
 * by the ResourceManager and decorators. Clients should use Resource instead.
 */
public interface InternalResource<C> extends Resource<C> {

    /**
     * Sets the resource state.
     * 
     * @param state Resource state.
     */
    void setState(ResourceState state);
    
}
