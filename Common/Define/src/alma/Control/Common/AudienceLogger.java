// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2010, 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

package alma.Control.Common;

import alma.acs.logging.AcsLogger;
import alma.acs.logging.AcsLogLevel;
import java.util.logging.Level;

public class AudienceLogger {

    // The internal logger must be an AcsLogger
    private AcsLogger logger;
    
    // Audience name
    private String audience;
    
    public AudienceLogger(AcsLogger logger, String audience) {
        this.logger = logger;
        this.audience = audience;
    }

    public AcsLogger getLogger() {
        return logger;
    }

    // These functions use ACS log levels
    public void emergency(String msg) {
        logger.logToAudience(AcsLogLevel.EMERGENCY, msg, audience);
    }
 
    public void alert(String msg) {
        logger.logToAudience(AcsLogLevel.ALERT, msg, audience);
    }
 
    public void critical(String msg) {
        logger.logToAudience(AcsLogLevel.CRITICAL, msg, audience);
    }
 
    public void error(String msg) {
        logger.logToAudience(AcsLogLevel.ERROR, msg, audience);
    }
 
    public void warning(String msg) {
        logger.logToAudience(AcsLogLevel.WARNING, msg, audience);
    }
 
    public void notice(String msg) {
        logger.logToAudience(AcsLogLevel.NOTICE, msg, audience);
    }
 
    public void info(String msg) {
        logger.logToAudience(AcsLogLevel.INFO, msg, audience);
    }
 
    public void debug(String msg) {
        logger.logToAudience(AcsLogLevel.DEBUG, msg, audience);
    }
 
    public void delouse(String msg) {
        logger.logToAudience(AcsLogLevel.DELOUSE, msg, audience);
    }
 
    public void trace(String msg) {
        logger.logToAudience(AcsLogLevel.TRACE, msg, audience);
    }
 
    // The following functions use Java log levels
    public void severe(String msg) {
        logger.logToAudience(Level.SEVERE, msg, audience);
    }
 
    public void config(String msg) {
        logger.logToAudience(Level.CONFIG, msg, audience);
    }
    
    public void fine(String msg) {
        logger.logToAudience(Level.FINE, msg, audience);
    }
    
    public void finer(String msg) {
        logger.logToAudience(Level.FINER, msg, audience);
    }
    
    public void finest(String msg) {
        logger.logToAudience(Level.FINEST, msg, audience);
    }

    // Static versions of all the abovementioned functions
    public static void emergency(String msg, String audience, AcsLogger logger) {
        logger.logToAudience(AcsLogLevel.EMERGENCY, msg, audience);
    }

    public static void alert(String msg, String audience, AcsLogger logger) {
        logger.logToAudience(AcsLogLevel.ALERT, msg, audience);
    }

    public static void critical(String msg, String audience, AcsLogger logger) {
        logger.logToAudience(AcsLogLevel.CRITICAL, msg, audience);
    }

    public static void error(String msg, String audience, AcsLogger logger) {
        logger.logToAudience(AcsLogLevel.ERROR, msg, audience);
    }

    public static void warning(String msg, String audience, AcsLogger logger) {
        logger.logToAudience(AcsLogLevel.WARNING, msg, audience);
    }

    public static void info(String msg, String audience, AcsLogger logger) {
        logger.logToAudience(AcsLogLevel.INFO, msg, audience);
    }

    public static void debug(String msg, String audience, AcsLogger logger) {
        logger.logToAudience(AcsLogLevel.DEBUG, msg, audience);
    }

    public static void delouse(String msg, String audience, AcsLogger logger) {
        logger.logToAudience(AcsLogLevel.DELOUSE, msg, audience);
    }

    public static void trace(String msg, String audience, AcsLogger logger) {
        logger.logToAudience(AcsLogLevel.TRACE, msg, audience);
    }

    public static void severe(String msg, String audience, AcsLogger logger) {
        logger.logToAudience(Level.SEVERE, msg, audience);
    }
    
    public static void config(String msg, String audience, AcsLogger logger) {
        logger.logToAudience(Level.CONFIG, msg, audience);
    }
    
    public static void fine(String msg, String audience, AcsLogger logger) {
        logger.logToAudience(Level.FINE, msg, audience);
    }

    public static void finer(String msg, String audience, AcsLogger logger) {
        logger.logToAudience(Level.FINER, msg, audience);
    }

    public static void finest(String msg, String audience, AcsLogger logger) {
        logger.logToAudience(Level.FINEST, msg, audience);
    }

}
