package alma.Control.Common;

/**
 * The class <code>ResCreationException</code> indicates error conditions
 * when a <code>Resource</code> object was being created.
 * 
 * @author  Rafael Hiriart
 * @version $Id$
 * @see     alma.Control.Common.Resource
 */
public class ResCreationException extends Exception {
    
    private static final long serialVersionUID = 1L;

    public ResCreationException() {
        super();
    }

    public ResCreationException(String message) {
        super(message);
    }

    public ResCreationException(Throwable cause) {
        super(cause);
    }

    public ResCreationException(String message, Throwable cause) {
        super(message, cause);
    }    
}
