/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * $Id$
 */
package alma.Control.Common;


import java.util.Observable;
import java.util.logging.Logger;

import alma.ResourceExceptions.wrappers.AcsJNotYetAcquiredEx;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;

/**
 * A decorator for a Resource, adding the capability to be observed. This
 * means that this class is an Observable in Java Observer/Observable mechanism.
 * @param C Type of the wrapped component.
 * @author rhiriart@nrao.edu
 *
 */
public class ObservableResource<C>
    extends Observable implements InternalResource<C> {
    
	/** The concrete Resource */
	protected InternalResource<C> resource;

	/** The Logger. VerboseLogger adds some functions to log class name,
	 *  instance name and method to the normal Logger.
	 */
    private VerboseLogger logger;
	
	public ObservableResource(InternalResource<C> resource, Logger logger) {
        this.logger = new VerboseLogger(logger, "ObservableResource");
		if (resource != null) 
			this.resource = resource;
		else
			throw new NullPointerException("Null resource passed to constructor.");
	}
	
	public void acquire(boolean force) throws AcsJResourceErrorEx {
	    acquire();
	}
	
	public void acquire() throws AcsJResourceErrorEx {
		resource.acquire();
		setChanged();
		notifyObservers(resource.status());        
	}

	public void release() {
		resource.release();
		setChanged();
		notifyObservers(resource.status());
	}	
	
    public void shutdown() throws AcsJResourceErrorEx {
        logger.finest("shutdown", "ENTRY");
        
        resource.shutdown();
        setChanged();
        notifyObservers(resource.status());
    }

    public void startup() throws AcsJResourceErrorEx {
        resource.startup();
        setChanged();
        notifyObservers(resource.status());
    }

    public C getComponent() throws AcsJNotYetAcquiredEx {
		return resource.getComponent();
	}

	public String getName() {
		return resource.getName();
	}

	public ResourceState status() {
		return resource.status();
	}

    public boolean isCritical() {
        return resource.isCritical();
    }

    public void clearException() {
        resource.clearException();
    }

    public Throwable getLastException() {
        return resource.getLastException();
    }

    public void setException(Throwable error) {
        resource.setException(error);
    }
    
    @Override
    public Resource<C> getConcreteResource() {
        return resource;
    }    

    public void up() throws AcsJResourceErrorEx {
        acquire();
        startup();
    }

    public void down() throws AcsJResourceErrorEx {
        shutdown();
        release();
    }

    @Override
    public void setState(ResourceState state) {
        resource.setState(state);
    }
}

// __oOo__