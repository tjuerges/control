// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2010, 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

package alma.Control.Common;

import alma.acs.logging.AcsLogger;
import alma.log_audience.DEVELOPER;

public class DeveloperLogger extends AudienceLogger {

    public DeveloperLogger(AcsLogger logger) {
        super(logger, DEVELOPER.value);
    }

    public static void emergency(String msg, AcsLogger logger) {
        AudienceLogger.emergency(msg, DEVELOPER.value, logger);
    }

    public static void alert(String msg, AcsLogger logger) {
        AudienceLogger.alert(msg, DEVELOPER.value, logger);
    }

    public static void critical(String msg, AcsLogger logger) {
        AudienceLogger.critical(msg, DEVELOPER.value, logger);
    }
    public static void error(String msg, AcsLogger logger) {
        AudienceLogger.error(msg, DEVELOPER.value, logger);
    }

    public static void warning(String msg, AcsLogger logger) {
        AudienceLogger.warning(msg, DEVELOPER.value, logger);
    }

    public static void info(String msg, AcsLogger logger) {
        AudienceLogger.info(msg, DEVELOPER.value, logger);
    }

    public static void debug(String msg, AcsLogger logger) {
        AudienceLogger.debug(msg, DEVELOPER.value, logger);
    }

    public static void delouse(String msg, AcsLogger logger) {
        AudienceLogger.delouse(msg, DEVELOPER.value, logger);
    }

    public static void trace(String msg, AcsLogger logger) {
        AudienceLogger.trace(msg, DEVELOPER.value, logger);
    }

    public static void severe(String msg, AcsLogger logger) {
        AudienceLogger.severe(msg, DEVELOPER.value, logger);
    }

    public static void config(String msg, AcsLogger logger) {
        AudienceLogger.config(msg, DEVELOPER.value, logger);
    }

    public static void fine(String msg, AcsLogger logger) {
        AudienceLogger.fine(msg, DEVELOPER.value, logger);
    }

    public static void finer(String msg, AcsLogger logger) {
        AudienceLogger.finer(msg, DEVELOPER.value, logger);
    }

    public static void finest(String msg, AcsLogger logger) {
        AudienceLogger.finest(msg, DEVELOPER.value, logger);
    }

}
