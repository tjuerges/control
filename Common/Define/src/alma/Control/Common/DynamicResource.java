/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * $Id$
 */

package alma.Control.Common;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Logger;

import alma.ACS.ACSComponent;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.acs.component.ComponentQueryDescriptor;
import alma.acs.container.ContainerServices;

public class DynamicResource<C extends ACSComponent> 
    implements InternalResource<C> {

    private ContainerServices container;
    private C component;
    private String name;
    private String type;
    private String helperClassName;
    private boolean isCritical;
    private Logger logger;
    private Throwable error;
    private ResourceState state = ResourceState.UNINITIALIZED;

    public DynamicResource(ContainerServices container, String name, String type) {
        this(container, name, type, false);
    }
    
    public DynamicResource(ContainerServices container, String name, String type,
                           boolean isCritical) {
        this.container = container;
        this.logger = container.getLogger();
        this.name = name;
        this.type = type;
        this.isCritical = isCritical;
        this.helperClassName = getHelperClassName();        
    }

    public DynamicResource(ContainerServices container, String name, String type,
            String hlpClsName) {
        this(container, name, type, hlpClsName, false);
    }
    
    public DynamicResource(ContainerServices container, String name, String type,
                           String hlpClsName, boolean isCritical) {
        this.container = container;
        this.name = name;
        this.type = type;
        this.helperClassName = hlpClsName;
        this.isCritical = isCritical;
        this.logger = container.getLogger();
    }
    
    public String getName() {
        return name;
    }
    
    public ResourceState status() {
        return state;
    }
     
    public void acquire(boolean force) throws AcsJResourceErrorEx {
        acquire();
    }
    
    public void acquire() throws AcsJResourceErrorEx {
        
        if (component != null) return;
        
        Class helperClass = null;
        try {
            helperClass = Class.forName(helperClassName);
        } catch (ClassNotFoundException ex) {
            String msg = "The helper class '" + helperClassName + "' was not found. ";
            msg = msg + "Check that this class is *really* installed in your ACSROOT/INTROOT/INTLIST.";
            AcsJResourceErrorEx resEx = new AcsJResourceErrorEx(ex);
            resEx.setProperty("HelperClassName", helperClassName);
            resEx.setProperty("Message", msg);
            throw resEx;
        }
        Class[] paramTypes = new Class[1];
        paramTypes[0] = org.omg.CORBA.Object.class;
        Method narrowMethod = null;
        try {
            narrowMethod = helperClass.getMethod("narrow", paramTypes);
        } catch (SecurityException ex) {
            String msg = "Security exception when trying to get the 'narrow' ";
            msg = msg + "method from the helper class '" + helperClassName + "'. ";
            msg = msg + "This is a system error. Contact your administrator.";
            AcsJResourceErrorEx resEx = new AcsJResourceErrorEx(ex);
            resEx.setProperty("HelperClassName", helperClassName);
            resEx.setProperty("Message", msg);
            throw resEx;
        } catch (NoSuchMethodException ex) {
            String msg = "The helper class '" + helperClassName + "' doesn't seem ";
            msg = msg + "to have the 'narrow()' method. ";
            msg = msg + "This is a system error. Contact your administrator.";
            AcsJResourceErrorEx resEx = new AcsJResourceErrorEx(ex);
            resEx.setProperty("HelperClassName", helperClassName);
            resEx.setProperty("Message", msg);
            throw resEx;
        }
        
        Object[] args = new Object[1];
        try {
            
            ComponentQueryDescriptor spec = new ComponentQueryDescriptor();
            spec.setComponentName(name);
            spec.setComponentType(type);            
            logger.fine("Getting dynamic component of type '" + type + "'");
            final long startTime = System.currentTimeMillis();
            args[0] = container.getDynamicComponent(spec, false);
            final long stopTime = System.currentTimeMillis();
            logger.fine("Took " + (stopTime - startTime)/1000.0 + 
                        " secs to get a reference to this component.");
        } catch (AcsJContainerServicesEx ex) {
            AcsJResourceErrorEx resEx = new AcsJResourceErrorEx(ex);
            String msg = "Error getting reference to component '" + name + "'.";
            resEx.setProperty("ComponentName", name);
            resEx.setProperty("Message", msg);
            throw resEx;
        }
        try {
            // Invoke the narrow (static) method in the helper class
            component = (C) narrowMethod.invoke(helperClass, args);
        } catch (IllegalArgumentException ex) {
            String msg = "Illegal argument exception when invoking the 'narrow' ";
            msg = msg + "method in the helper class '" + helperClassName + "'. ";
            msg = msg + "This is a system error. Contact your administrator.";
            AcsJResourceErrorEx resEx = new AcsJResourceErrorEx(ex);
            resEx.setProperty("HelperClassName", helperClassName);
            resEx.setProperty("Message", msg);
            throw resEx;
        } catch (IllegalAccessException ex) {
            String msg = "Illegal access exception when invoking the 'narrow' ";
            msg = msg + "method in the helper class '" + helperClassName + "'. ";
            msg = msg + "This is a system error. Contact your administrator.";
            AcsJResourceErrorEx resEx = new AcsJResourceErrorEx(ex);
            resEx.setProperty("HelperClassName", helperClassName);
            resEx.setProperty("Message", msg);
            throw resEx;
        } catch (InvocationTargetException ex) {
            String msg = "Invocation target exception when invoking the 'narrow' ";
            msg = msg + "method in the helper class '" + helperClassName + "'. ";
            msg = msg + "This is a system error. Contact your administrator.";
            AcsJResourceErrorEx resEx = new AcsJResourceErrorEx(ex);
            resEx.setProperty("HelperClassName", helperClassName);
            resEx.setProperty("Message", msg);
            throw resEx;
        }
        
        setState(ResourceState.INITIALIZED);
    }
    
    public void release() {
        if (component ==  null) return;
        container.releaseComponent(name);
        component = null;
        setState(ResourceState.UNINITIALIZED);
    }

    public void shutdown() throws AcsJResourceErrorEx {
        setState(ResourceState.INITIALIZED);
    }

    public void startup() throws AcsJResourceErrorEx {
        setState(ResourceState.OPERATIONAL);
    }
    
    public void setState(ResourceState state) {
        this.state = state;
    }
        
    public C getComponent() {
        return component;
    }

    public boolean isCritical() {
        return isCritical;
    }
    
    public void clearException() {
        error = null;
    }

    public Throwable getLastException() {
        return error;
    }

    public void setException(Throwable error) {
        this.error = error;
    }
    
    private String getHelperClassName() {

        String hn = type.substring(4);
        hn = hn.substring(0, hn.length()-4);
        hn = hn.replaceAll("/", ".");
        hn = hn + "Helper";
        return hn;
    }
    
    @Override
    public Resource<C> getConcreteResource() {
        return null;
    }    

    public void up() throws AcsJResourceErrorEx {
        acquire();
        startup();
    }

    public void down() throws AcsJResourceErrorEx {
        shutdown();
        release();
    }
}
