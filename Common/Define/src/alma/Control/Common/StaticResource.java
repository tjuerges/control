/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * $Id$
 */
package alma.Control.Common;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Logger;

import alma.ACS.ACSComponent;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.ResourceExceptions.wrappers.AcsJNotYetAcquiredEx;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.acs.container.ContainerServices;

public class StaticResource<C extends ACSComponent> 
	implements InternalResource<C> {

	protected ContainerServices container;
    protected Logger logger;
    
	private C component;
	private String name;
	private String helperClassName;
    private boolean isCritical;
    private boolean isDefault;
    private Throwable error;
    private ResourceState state = ResourceState.UNINITIALIZED;

    public StaticResource(ContainerServices container, String name, 
                          String helperClassName) {
        this(container, name, helperClassName, false, false, null);
    }

    public StaticResource(ContainerServices container,
                          String name, 
                          String helperClassName,
                          boolean isCritical) {
        this(container, name, helperClassName, isCritical, false, null);
    }

    public StaticResource(ContainerServices container,
                          String name, 
                          String helperClassName,
                          boolean isCritical, 
                          boolean isDefault,
                          Logger logger) {
        
        if (container != null)
            this.container = container;
        else
            throw new NullPointerException("Null ContainerServices passed to constructor.");
        this.name = name;
        this.helperClassName = helperClassName;
        this.isCritical = isCritical;
        this.isDefault = isDefault;
        
        if (logger == null)
            this.logger = container.getLogger();
        else
            this.logger = logger;
        
        setState(ResourceState.UNINITIALIZED);
    }
    
    @Override
	public String getName() {
		return name;
	}

	@Override
	public ResourceState status() {
		return state;
	}	

	public void acquire(boolean force) throws AcsJResourceErrorEx {
	    component = null;
	    acquire();
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public void acquire() throws AcsJResourceErrorEx {				
	
        if (component != null) return;
        
        setState(ResourceState.ACQUIRING);
        
		Class helperClass = null;
		try {
			helperClass = Class.forName(helperClassName);
		} catch (ClassNotFoundException ex) {
			String msg = "The helper class '" + helperClassName + "' was not found. ";
            msg = msg + "Check that this class is *really* installed in your ACSROOT/INTROOT/INTLIST.";
            AcsJResourceErrorEx resEx = new AcsJResourceErrorEx(ex);
            resEx.setProperty("HelperClassName", helperClassName);
            resEx.setProperty("Message", msg);
            setState(ResourceState.ERROR);
            throw resEx;
		}
		Class[] paramTypes = new Class[1];
		paramTypes[0] = org.omg.CORBA.Object.class;
		Method narrowMethod = null;
		try {
			narrowMethod = helperClass.getMethod("narrow", paramTypes);
		} catch (SecurityException ex) {
            String msg = "Security exception when trying to get the 'narrow' ";
            msg = msg + "method from the helper class '" + helperClassName + "'. ";
            msg = msg + "This is a system error. Contact your administrator.";
            AcsJResourceErrorEx resEx = new AcsJResourceErrorEx(ex);
            resEx.setProperty("HelperClassName", helperClassName);
            resEx.setProperty("Message", msg);
            setState(ResourceState.ERROR);
            throw resEx;
		} catch (NoSuchMethodException ex) {
            String msg = "The helper class '" + helperClassName + "' doesn't seem ";
            msg = msg + "to have the 'narrow()' method. ";
            msg = msg + "This is a system error. Contact your administrator.";
            AcsJResourceErrorEx resEx = new AcsJResourceErrorEx(ex);
            resEx.setProperty("HelperClassName", helperClassName);
            resEx.setProperty("Message", msg);
            setState(ResourceState.ERROR);
            throw resEx;
		}
		
		Object[] args = new Object[1];
		try {
                    final long startTime = System.currentTimeMillis();
                    if (isDefault) {                
                        logger.fine("Getting default component for interface '" + name + "'");
                        args[0] = container.getDefaultComponent(name);
                    } else {
                        logger.fine("Getting component '" + name + "'");
                        args[0] = container.getComponent(name);
                    }                
                    final long stopTime = System.currentTimeMillis();
                    logger.fine("Took " + (stopTime - startTime)/1000.0 + 
                                " secs to get a reference to this component.");
        } catch (AcsJContainerServicesEx ex) {
            AcsJResourceErrorEx resEx = new AcsJResourceErrorEx(ex);
            String msg = "Error getting reference to component '" + name + "'.";
            resEx.setProperty("ComponentName", name);
            resEx.setProperty("Message", msg);
            setState(ResourceState.ERROR);
            throw resEx;
        }
            
		try {
			// Invoke the narrow (static) method in the helper class
			component =	(C) narrowMethod.invoke(helperClass, args);
		} catch (IllegalArgumentException ex) {
            String msg = "Illegal argument exception when invoking the 'narrow' ";
            msg = msg + "method in the helper class '" + helperClassName + "'. ";
            msg = msg + "This is a system error. Contact your administrator.";
            AcsJResourceErrorEx resEx = new AcsJResourceErrorEx(ex);
            resEx.setProperty("HelperClassName", helperClassName);
            resEx.setProperty("Message", msg);
            setState(ResourceState.ERROR);
            throw resEx;
		} catch (IllegalAccessException ex) {
            String msg = "Illegal access exception when invoking the 'narrow' ";
            msg = msg + "method in the helper class '" + helperClassName + "'. ";
            msg = msg + "This is a system error. Contact your administrator.";
            AcsJResourceErrorEx resEx = new AcsJResourceErrorEx(ex);
            resEx.setProperty("HelperClassName", helperClassName);
            resEx.setProperty("Message", msg);
            setState(ResourceState.ERROR);
            throw resEx;
		} catch (InvocationTargetException ex) {
            String msg = "Invocation target exception when invoking the 'narrow' ";
            msg = msg + "method in the helper class '" + helperClassName + "'. ";
            msg = msg + "This is a system error. Contact your administrator.";
            AcsJResourceErrorEx resEx = new AcsJResourceErrorEx(ex);
            resEx.setProperty("HelperClassName", helperClassName);
            resEx.setProperty("Message", msg);
            setState(ResourceState.ERROR);
            throw resEx;
		}
        
        setState(ResourceState.INITIALIZED);
    }

	@Override
	public void release() {
        setState(ResourceState.RELEASING);
        if (component == null) {
            setState(ResourceState.UNINITIALIZED);
            return;
        }
		container.releaseComponent(name);
        component = null;
        setState(ResourceState.UNINITIALIZED);
	}	
	
	@Override
    public void shutdown() throws AcsJResourceErrorEx {
        // If shutting down operations take some time, then
        // setState(ResourceState.SHUTTING_DOWN) should be called. This should
	    // be done in children of this class.
        setState(ResourceState.INITIALIZED);        
    }

    @Override
    public void startup() throws AcsJResourceErrorEx {
        // If start operations take some time, then
        // setState(ResourceState.STARTING_UP) should be called. This should
        // be done in children of this class.
        setState(ResourceState.OPERATIONAL);        
    }

    @Override
    public void up() throws AcsJResourceErrorEx {
        acquire();
        startup();
    }

    @Override
    public void down() throws AcsJResourceErrorEx {
        shutdown();
        release();
    }
    
    @Override
	public C getComponent() throws AcsJNotYetAcquiredEx {
        if (component == null) {
            String msg = "This resource has not yet been properly acquired, so ";
            msg = msg + "its internal component is not usable (null).";
            AcsJNotYetAcquiredEx ex = new AcsJNotYetAcquiredEx();
            ex.setProperty("ResourceName", name);
            ex.setProperty("Message", msg);
            throw ex;
        }
		return component;
	}

	@Override
    public boolean isCritical() {
        return isCritical;
    }
    
    @Override
    public void clearException() {
        error = null;
    }

    @Override
    public Throwable getLastException() {
        return error;
    }

    @Override
    public void setException(Throwable error) {
        this.error = error;
    }

    @Override
    public void setState(ResourceState state) {
        this.state = state;
    }

    @Override
    public Resource<C> getConcreteResource() {
        return null;
    }    
}
