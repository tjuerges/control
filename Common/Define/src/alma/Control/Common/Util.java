/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File Util.java
 */
package alma.Control.Common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import alma.hla.runtime.asdm.types.*;

/**
 * Description
 *
 * @version 1.0 Apr 30, 2005
 * This version is from the Optical Pointing branch.
 * @author Allen Farris
 */
public class Util {

    /**
     * Convert ACS time to ArrayTime
     * 
     * ACS: hundreds of nanoseconds since 15 October 1582 00:00:00 UTC
     * ArrayTime: nanoseconds since 17 November 1858 00:00:00 UTC
     * base difference in days: JD(17 November 1858 00:00:00 UTC) minus
     *                          JD(15 October 1582 00:00:00 UTC)
     * ArrayTime = (ACS.time * 100) minus (baseDifference * 86400000000000)
     * ArrayTime = 100*ACS.time - (2400000.5 - 2299160.5) * 86400000000000
     * ArrayTime = 100*ACS.time - (100840) * 86400000000000
     * ArrayTime = 100*ACS.time - 8712576000000000000
     *  
     * @param acsTime The ACS time.
     * @return the ArrayTime as a long.
     */
    public static long acsTimeToArrayTime(long acsTime) {
        return 100L * acsTime - 8712576000000000000L;
    }
    
    /**
     * Convert ACS time to ArrayTime
     * 
     * @param arrayTime
     * @return
     */
    public static long arrayTimeToACSTime(long arrayTime) {
        return arrayTime / 100L + 87125760000000000L;
    }
    
    /**
     * Convert an ACS::Time to a string of the format HH:MM:SS.sss
     */
    public static String acsTimeToString(long acsTime) {
        int[] unit = new ArrayTime(acsTimeToArrayTime(acsTime)).getDateTime();
        int hr = unit[3];
        int min = unit[4];
        int sec = unit[5];
        int nsec = unit[6];
        int msec = (int) Math.round(nsec/1E6);
        if (msec == 1000) {
            msec = 0;
            sec += 1;
        }
        if (sec == 60) {
            sec = 0;
            min += 1;
        }
        if (min == 60) {
            min = 0;
            hr += 1;
        }
        if (hr == 24) {
            hr = 0;
        }
        return String.format("%02d:%02d:%02d.%03d", hr, min, sec, msec);
   }
    
    public static String arrayTimeToString(long arrayTime) {
        return new ArrayTime(arrayTime).toFITS();
    }

    
    private static long systemTimeBase = new ArrayTime(1970,1,1,0,0,0.0).get();
    
    /**
     * The getArrayTime method returns the ArrayTime 
     * using the current system time.
     * @return
     */
    public static ArrayTime getArrayTime() {
        // The difference, measured in milliseconds, between the 
        // current time and midnight, January 1, 1970 UTC.
        long currentSysTime = System.currentTimeMillis();
        
        return new ArrayTime(currentSysTime * 1000000L + systemTimeBase);
    }
    
    /**
     * returns a time duration as an ACS::TimeInterval object (where time
     * intervals are in units of 100ns).
     * 
     * @param timeInSeconds: time interval in seconds.
     * @return
     */
    public static long toTimeInterval(double timeInSeconds) {
        return Math.round(timeInSeconds / 100E-9);
    }
    
    private static long TEInterval = 480000;

    /**
     * Returns the time, in ACS Time units, of the TE nearest to the specified
     * time.
     */
    public static long roundTE(long ACSTime) {
        return Math.round((double) ACSTime / (double) TEInterval) * TEInterval;
    }

    /**
     * Returns the time, in ACS Time units, of the TE thats nearest to but less
     * or equal to the specified time.
     */
    public static long floorTE(long ACSTime) {
        return Math.round(Math.floor((double) ACSTime / (double) TEInterval)) * TEInterval;
    }

    /**
     * Returns the time, in ACS Time units, of the TE thats nearest to but
     * greater than or equal to the specified time.
     */
    public static long ceilTE(long ACSTime) {
        return Math.round(Math.ceil((double) ACSTime / (double) TEInterval)) * TEInterval;
    }
   
    /**
     * Return the full path of the file specified in target.  The directory 
     * search is from $INTROOT/bin, to each "directory/bin" in $INTLIST and 
     * finally to $ACSROOT/bin.  If the file was not found, null is returned.  
     * If there are invalid directory entries in the list of search directories,
     * they are skipped.
     * @param target
     * @return The full path of the file specified in target.
     */
    public static String findFile(String target) {
    	return findFile(getDirList(),target);
    }
    
    public static String findFile(String[] dirList, String target) {
    	String result = null;
    	for (int i = 0; i < dirList.length; ++i) {
    		File dir = new File (dirList[i]);
    		if (dir.isDirectory()) {
    			result = findFile(dir,target);
    			if (result != null)
    				return result;
    		}
    	}
    	return null;
    }
    private static String findFile(File dir, String target) {
    	File[] f = dir.listFiles();
    	String result = null;
    	for (int i = 0; i < f.length; ++i) {
    		if (f[i].isDirectory()) {
    			result = findFile(f[i],target);
    			if (result != null)
    				return result;
    		} else if (f[i].isFile()) {
    			if (f[i].getName().equals(target))
    				return f[i].getAbsolutePath();
    		}
    	}
    	return null;
    }
    
    private static final String fileSeparator = System.getProperty("file.separator");
    private static final String pathSeparator = System.getProperty("path.separator");
    private static final String lineSeparator = System.getProperty("line.separator");
    private static String[] getDirList() {
    	String introot = System.getenv("INTROOT");
    	String intlist = System.getenv("INTLIST");
    	String acsroot = System.getenv("ACSROOT");
    	ArrayList<String> list = new ArrayList<String> ();
    	if (introot != null)
    		list.add(introot + fileSeparator + "bin");
    	if (intlist != null) {
    		int beg = 0;
    		int end = 0;
    		do {
    			end = intlist.indexOf(pathSeparator,beg);
    			if (end != -1) {
    				list.add(intlist.substring(beg,end) + fileSeparator + "bin");
    				beg = end + 1;
    			}
    		} while (end != -1);
    		list.add(intlist.substring(beg) + fileSeparator + "bin");
    	}
    	if (acsroot != null)
    		list.add(acsroot + fileSeparator + "bin");
    	String[] result = new String [list.size()];
    	result = list.toArray(result);
    	return result;
    }

    /**
     * Return the text of a file as a string.
     * @param fullPathName The full path name of the file to be read.
     * @return The text of a file as a string. An IllegalArgumentException is thrown if any error is encountered.
     */
    public static String readFile(String fullPathName)  {
    	BufferedReader in = null;
		File file = new File(fullPathName);
		if (!file.exists()) {
			throw new IllegalArgumentException ("There is no such file as " + fullPathName);
		}
		try {
			in = new BufferedReader(new FileReader(file));
		} catch (IOException err) {
			throw new IllegalArgumentException ("Error opening file " + fullPathName + ": " + err.toString());
		}
		StringBuffer out = new StringBuffer ();
		try {
			String line = in.readLine();
			while (line != null) {
				out.append(line + lineSeparator);
				line = in.readLine();
			}
			in.close();
		} catch (IOException err) {
			throw new IllegalArgumentException ("Error reading file " + fullPathName + ": " + err.toString());
		}
    	return out.toString();
    }
    
    /**
     * Get the complete error trace information as a String, including the
     * nested error traces.
     * 
     * @param et Error trace.
     * @return String representation of an error trace. 
     */
    public static String getNiceErrorTraceString(alma.ACSErr.ErrorTrace et) {
        return getNiceErrorTraceString(et, 0);
    }
    
    /**
     * Gets the complete error trace information as a String, including the
     * nested error traces. This is a recursive function. Invoke this function
     * with level = 0, or even better, use getNiceErrorTraceString(ErrorTrace)
     * instead.
     * @param et Error trace. This is an IDL structure that contains exception information
     * and allows exception "chaining".
     * @param level Level of the error trace. This is used internally to indent nested
     * error traces recursively. Use level = 0 if calling this function directly.
     * @return String representation of an error trace.
     * @see getNiceErrorTraceString(alma.ACSErr.errorTrace)
     */
    private static String getNiceErrorTraceString(alma.ACSErr.ErrorTrace et, int level) {

        String ws = "    ";
        for (int i=0; i<level; i++) ws += "    ";
        
        String s = ws + "ERROR TRACE " + level + "\n" +
                   ws + "Code = " + et.errorCode + "\n" +
                   ws + "Type = " + et.errorType + "\n" +
                   ws + "File = '" + et.file + "'\n" +
                   ws + "Host = '" + et.host + "'\n" +
                   ws + "Line Number = " + et.lineNum + "\n" +
                   ws + "Process = '" + et.process + "'\n" +
                   ws + "Routine = '" + et.routine + "'\n" +
                   ws + "Short Description = '" + et.shortDescription + "'\n" +
                   ws + "Source Object = '" + et.sourceObject + "'\n" +
                   ws + "Thread = '" + et.thread + "'\n";
        s += ws + "Additional Data " + "\n";
        for (int di=0; di<et.data.length; di++) {
            s += ws + "Name = '" + et.data[di].name + "'; Value = '" + et.data[di].value + "'\n";
        }
        
        for (int err=0; err<et.previousError.length; err++)
            s += getNiceErrorTraceString(et.previousError[err], ++level);
            
        return s;
    }
    
    /**
     * Get the complete error trace information as an HTML String, including the
     * nested error traces.
     * 
     * @param et Error trace.
     * @return String representation of an error trace. 
     */
    public static String getNiceErrorTraceHTMLString(alma.ACSErr.ErrorTrace et) {
        return getNiceErrorTraceHTMLString(et, 0);
    }
    
    /**
     * Gets the complete error trace information as an HTML String, including the
     * nested error traces. This is a recursive function. Invoke this function
     * with level = 0, or even better, use getNiceErrorTraceString(ErrorTrace)
     * instead.
     * @param et Error trace. This is an IDL structure that contains exception information
     * and allows exception "chaining".
     * @param level Level of the error trace. This is used internally to indent nested
     * error traces recursively. Use level = 0 if calling this function directly.
     * @return String representation of an error trace.
     * @see getNiceErrorTraceHTMLString(alma.ACSErr.errorTrace)
     */
    private static String getNiceErrorTraceHTMLString(alma.ACSErr.ErrorTrace et, int level) {
    	String singleSpacer= "&nbsp;&nbsp;";
    	String spacer=singleSpacer;
    	StringBuilder ret = new StringBuilder();
		for (int i = 0; i < level; i++) {
			spacer+=singleSpacer;
		}
		ret.append(spacer + "<B>ERROR TRACE " + level + "</B><BR>");
		ret.append(spacer + "Code = '<EM>"+ et.errorCode + "</EM>'<BR>");
		ret.append(spacer + "Type = '<EM>" + et.errorType + "</EM>'<BR>");
		ret.append(spacer + "File = '<EM>" + et.file + "</EM>'<BR>");
		ret.append(spacer + "Host = '<EM>"+ et.host + "</EM>'<BR>");
		ret.append(spacer + "Line Number = '<EM>" + et.lineNum+ "</EM>'<BR>");
		ret.append(spacer + "Process = '<EM>" + et.process + "</EM>'<BR>");
		ret.append(spacer + "Routine = '<EM>" + et.routine + "</EM>'<BR>");
		ret.append(spacer + "Short Description = '<EM>" + et.shortDescription+ "</EM>'<BR>");
		ret.append(spacer + "Source Object = '<EM>" + et.sourceObject + "</EM>'<BR>");
		ret.append(spacer + "Thread = '<EM>" + et.thread + "</EM>'<BR>");
		if (et.data.length>0) {
			ret.append(spacer + "Additional Data " + "<BR>");
			for (int di = 0; di < et.data.length; di++) {
				ret.append(spacer + "&nbsp;- Name = '" + et.data[di].name + "'; Value = '"+ et.data[di].value + "'<BR>");
			}
		}

		for (int err = 0; err < et.previousError.length; err++) {
			ret.append(getNiceErrorTraceHTMLString(et.previousError[err], ++level));
		}
		return ret.toString();
	}

}
