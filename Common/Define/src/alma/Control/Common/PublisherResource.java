package alma.Control.Common;

import java.util.logging.Logger;

import alma.ResourceExceptions.wrappers.AcsJNotYetAcquiredEx;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.acs.container.ContainerServices;
import alma.acs.exceptions.AcsJException;
import alma.acs.nc.SimpleSupplier;

public class PublisherResource implements InternalResource<SimpleSupplier> {

    private ContainerServices container;
    private String name;
    private SimpleSupplier publisher;
    private boolean isCritical;
    private Logger logger;
    private Throwable error;
    private ResourceState state;
    
    public PublisherResource(ContainerServices container,
                             String name,
                             boolean isCritical) {
        this(container, name, isCritical, null);
    }
    
    public PublisherResource(ContainerServices container, String name,
                             boolean isCritical, Logger logger) {
        this.container = container;
        this.name = name;
        this.isCritical = isCritical;
        if (logger == null)
            this.logger = container.getLogger();
        else
            this.logger = logger;
        this.state = ResourceState.UNINITIALIZED;
    }

    public void acquire(boolean force) throws AcsJResourceErrorEx {
        acquire();
    }
    
    @Override
    public void acquire() throws AcsJResourceErrorEx {
        if (publisher != null) return;
        state = ResourceState.ACQUIRING;
        try {
            logger.finer("Getting '" + name + "' PublisherResource.");
            publisher = new SimpleSupplier(name, container);
            state = ResourceState.OPERATIONAL;
        } catch (AcsJException ex) {
            String msg = "Error when creating SimpleSupplier instance.";
            AcsJResourceErrorEx resEx = new AcsJResourceErrorEx(ex);
            resEx.setProperty("Message", msg);
            state = ResourceState.ERROR;
            throw resEx;
        }
    }

    @Override
    public void release() {
        logger.finer("Releasing '" + name + "' PublisherResource.");
        if (publisher != null)
            publisher.disconnect();
        publisher = null;
        state = ResourceState.UNINITIALIZED;
    }
    
    @Override
    public SimpleSupplier getComponent() throws AcsJNotYetAcquiredEx {
        if (publisher == null) {
            String msg = "This resource has not yet been properly acquired, so ";
            msg = msg + "its internal component not usable (null).";
            AcsJNotYetAcquiredEx ex = new AcsJNotYetAcquiredEx();
            ex.setProperty("ResourceName", name);
            ex.setProperty("Message", msg);
            logger.fine(msg);
            throw ex;
        }
        return publisher;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isCritical() {
        return isCritical;
    }

    @Override
    public ResourceState status() {
        return state;
    }

    @Override
    public void clearException() {
        error = null;
    }

    @Override
    public Throwable getLastException() {
        return error;
    }

    @Override
    public void setException(Throwable error) {
        this.error = error;
    }

    @Override
    public void shutdown() throws AcsJResourceErrorEx {}

    @Override
    public void startup() throws AcsJResourceErrorEx {}

    @Override
    public Resource<SimpleSupplier> getConcreteResource() {
        return null;
    }    

    @Override
    public void up() throws AcsJResourceErrorEx {
        acquire();
        startup();
    }

    @Override
    public void down() throws AcsJResourceErrorEx {
        shutdown();
        release();
    }

    @Override
    public void setState(ResourceState state) {
        this.state = state;
    }
}
