/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * $Id$
 */
package alma.Control.Common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

import alma.ResourceExceptions.wrappers.AcsJBadResourceEx;
import alma.ResourceExceptions.wrappers.AcsJNotYetAcquiredEx;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.ResourceExceptions.wrappers.AcsJUnknownResourceEx;
import alma.acs.container.ContainerServices;
import alma.acs.logging.AcsLogLevel;

/**
 * The ResourceManager manages a collection of resources. Once the resources
 * has been added to the ResourceManager, they can be collectively acquired,
 * released, and monitored.
 * 
 * @author rhiriart
 *
 */
public class ResourceManager extends Observable implements Observer {
    
    /** The state of the collection of resources managed by this class. */
    public enum ResourceCollectionState {
        /** None of the Resource components have been activated */
        UNINITIALIZED,
        /** All the Resource components have been activated */
        INITIALIZED,
        /**
         *  Some of the Resources are in the middle of operations either
         *  to start up or shut down. This is a transitional state.
         */
        WORKING,
        /** Some non-critical Resources are in error */
        DEGRADED,
        /** One or more critical Resources are in error */
        ERROR,
        /** All the Resources are operational */
        OPERATIONAL };
    
    /**
     * ResourceManager instances. The ResourceManager is a singleton. 
     */
    private static Map<String, ResourceManager> instances = 
        new HashMap<String, ResourceManager>();

    /** ACS container services */
    private ContainerServices container;

    /** 
     *  ResourceManager instance name. It's the same name as the component
     *  that created an instance. 
     */
    private String name;
    
    /** The logger */
    private VerboseLogger logger;
    
    /** The map with all resources managed by this ResourceManager */
	private Map<String, Resource<?>> resources;

	/** 
	 * Previous Resource collection state. It is used to decide whether to
	 * send a notification when the resource collection changes state.
	 */
    private ResourceCollectionState previousState =
        ResourceCollectionState.UNINITIALIZED;

    /**
     * Lock, Condition Variable, and list of resources to wait when
     * the collection or a subset of the collection is going up 
     * (components activated and initialized).
     */
    private Lock upLock = new ReentrantLock();
    private Condition upCV = upLock.newCondition();
    private String[] upWaitedResources;

    /**
     * Lock, Condition Variable, and list of resources to wait when
     * the collection or a subset of the collection is going down
     * (components shutdown and deactivated).
     */
    private Lock downLock = new ReentrantLock();
    private Condition downCV = downLock.newCondition();
    private String[] downWaitedResources;

    /**
     * Get an instance of the ResourceManager.
     * 
     * @param cont ACS ContainerServices
     * @return ResourceManager instance
     */
    public static ResourceManager getInstance(ContainerServices cont) {
        return getInstance(cont, null);
    }
    
    /**
     * Method to get the ResourceManager singleton instance.
     * @param cont ACS ContainerServices.
     * @return ResourceManager singleton instance.
     */
    public synchronized static ResourceManager 
        getInstance(ContainerServices cont, Logger logger) {
        
        String compName = cont.getName();
        if (!instances.containsKey(compName)) {
            if (logger == null)
                cont.getLogger().log(AcsLogLevel.DEBUG, "Creating new ResourceManager instance for '" +
                                      compName + "'");
            else
                logger.log(AcsLogLevel.DEBUG, "Creating new instance for '" + compName + "'");
            instances.put(compName, new ResourceManager(cont, logger));
        }
        return instances.get(compName);
    }

    /**
     * Frees an instance of the ResourceManager.
     * This method should be called as part of the component cleanup
     * operation.
     * @param cont ACS Container Services.
     */
    public synchronized static void freeInstance(ContainerServices cont) {
        instances.remove(cont.getName());
    }
    
    /**
     * Creates an empty ResourceManager, containing no resources.
     * This constructor is not meant to be used directly by a client. (That's
     * why it's private.) Clients should use getInstance() instead.
     * @param container ACS container services
     */
	private ResourceManager(ContainerServices cont, Logger logger) {		
		container = cont;
        name = container.getName();
        if (logger == null)
            this.logger = new VerboseLogger(container.getLogger(), getClass().getName());
        else
            this.logger = new VerboseLogger(logger, getClass().getName());
		resources = new HashMap<String, Resource<?>>();
        
        this.logger.fine(name,
                         "ResourceManager",
                         "Created ResourceManager for component '" +
                             container.getName() + "'");
	}

    /**
     * Get Resource collection state.
     * 
     * @return Resource collection state
     */
	public ResourceCollectionState getResourceCollState() {
	    return getResourceCollState(null);
	}
	
	/**
	 * Get Resource collection state.
	 * 
	 * @param resourceNames
	 *     Subset of resources to consider. If null, all resources are used.
	 * 
	 * @return Resource collection state
	 */
    public ResourceCollectionState
        getResourceCollState(String[] resourceNames) {
                
        List<String> resCriticalInERROR = new ArrayList<String>();
        List<String> resNonCriticalInERROR = new ArrayList<String>();
        List<String> resInUNINITIALIZED = new ArrayList<String>();
        List<String> resInINITIALIZED = new ArrayList<String>();
        List<String> resInOPERATIONAL = new ArrayList<String>();
        List<String> resInTRANSITION = new ArrayList<String>();
        
        String msg = "\nResource (Subset) States:\n";
        String[] keys = resourceNames;
        if (resourceNames == null) {
            msg = "\nResource States:\n";
            keys = resources.keySet().toArray(new String[0]);
        }
        
        // First classify the resources according to their states
        for (int i = 0; i < keys.length; i++) {
            String resName = keys[i];
            Resource<?> res = resources.get(resName);
            ResourceState state = res.status();
            msg += String.format("%-40s%s\n", resName, state);
            if (state == ResourceState.UNINITIALIZED)
                resInUNINITIALIZED.add(resName);
            else if (state == ResourceState.ERROR && res.isCritical())
                resCriticalInERROR.add(resName);
            else if (state == ResourceState.ERROR && !res.isCritical())
                resNonCriticalInERROR.add(resName);
            else if (state == ResourceState.OPERATIONAL)
                resInOPERATIONAL.add(resName);
            else if (state == ResourceState.INITIALIZED)
                resInINITIALIZED.add(resName);
            else
                resInTRANSITION.add(resName);
        }
        
        // Then apply rules to get the state of the whole resource
        // collection.
        ResourceCollectionState returnValue;
        if (resCriticalInERROR.size() > 0)
            // If any critical resource is in error, then the whole collection
            // is in error,
            returnValue =  ResourceCollectionState.ERROR;
        else if (resInTRANSITION.size() > 0)
            // then if any resource is in process of changing the state,
            // the collection is working,
            returnValue = ResourceCollectionState.WORKING;
        else if (resNonCriticalInERROR.size() > 0)
            // then if any non critical resource is in error, the collection
            // is degraded,
            returnValue = ResourceCollectionState.DEGRADED;
        else if (resInUNINITIALIZED.size() == keys.length)
            // then if all the resources are unitialized, the collection is
            // uninitialized,
            returnValue = ResourceCollectionState.UNINITIALIZED;
        else if (resInINITIALIZED.size() == keys.length)
            // then if all the resources are itialized, the collection is
            // initialized,
            returnValue = ResourceCollectionState.INITIALIZED;
        else if (resInOPERATIONAL.size() == keys.length)
            // then if all resources are operational, the collection is
            // operational,
            returnValue = ResourceCollectionState.OPERATIONAL;
        else
            // else the collection is working. This happens during shutdown
            // when not all resources are unitialized yet.
            returnValue = ResourceCollectionState.WORKING;

        msg += String.format("%-40s=> %s\n", "Resource Collection State",
                             returnValue);
        logger.finest(name, "getResourceCollState", msg);
        return returnValue;
    }
    
    /**
     * Adds a resource to the collection of resources managed by this
     * ResourceManager.
     * 
     * @param resource Resource to add. Can't be null.
     */
	public synchronized void addResource(Resource<?> resource) {

	    if (resource == null)
	        throw new NullPointerException("Resource is null");
                        
        if (resources.containsKey(resource.getName())) {
            logger.fine(name,
                        "addResource",
                        "Attempting to add resource '" + resource.getName() +
                        "' twice. Resource won't be added.");
            return;
        }
        
        logger.finer(name,
                     "addResouce",
                     "Adding resource '" + resource.getName() + "'.");

		resources.put(resource.getName(), resource);
                
        // If the resource needs to be observed, the ResourceManager adds
        // itself to the list of Observers.
        if (resource instanceof Observable) {
            ObservableResource<?> mr = (ObservableResource<?>) resource;
            mr.addObserver(this);            
        }
        
        logger.finest(name, "addResource", toString());
	}

    /**
     * Removes a resource from the collection managed by the ResourceManager.
     * @param resName Resource name
     */
    public synchronized void freeResource(String resName) {
        Resource<?> resource = resources.get(resName);
        resource.release();
        resources.remove(resName);
    }
    
    /**
     * Removes all Resources from the collection managed by the ResourceManager.
     * This function should be called at the end of the component lifecycle.
     */
    public synchronized void freeAllResources() {
        // Make sure all components have been released
        for (Iterator<String> it = resources.keySet().iterator(); it.hasNext(); )
            resources.get(it.next()).release();
        resources.clear();
    }
    
    /**
     * Get all managened Resource names.
     * 
     * @return All Resource names.
     */
    public String[] getResourceNames() {
        return resources.keySet().toArray(new String[0]);
    }
    
    /**
     * Gets a Resource.
     * 
     * @param resName Resource name
     * @return The Resource
     * @throws AcsJUnknownResourceEx
     *      If there is not a Resource in the collection with such a name
     * @throws AcsJBadResourceEx
     *      If the resouce is not OPERATIONAL
     */
	@SuppressWarnings("unchecked")
	public <T> Resource<T> getResource(String resName)
	    throws AcsJUnknownResourceEx, AcsJBadResourceEx {
        
        if (!resources.containsKey(resName)) {
            AcsJUnknownResourceEx ex = new AcsJUnknownResourceEx();
            ex.setProperty("UnknownResourceName", resName);
            throw ex;
        }
        Resource<T> resource = (Resource<T>) resources.get(resName);
        if (resource.status() != ResourceState.OPERATIONAL) {
            AcsJBadResourceEx ex = new AcsJBadResourceEx();
            ex.setProperty("Resource Name", resName);
            ex.setProperty("Resource State", resource.status().toString());
            throw ex;
        }
		return resource;
	}
        
    /**
     * Gets a Resource, even if the Resource is not OPERATIONAL.
     * 
     * @param resName Resource name
     * @return The Resource
     * @throws AcsJUnknownResourceEx
     *      If there is not a Resource in the collection with such a name
     */
    @SuppressWarnings("unchecked")
    public <T> Resource<T> getResourceEvenIfBad(String resName)
        throws AcsJUnknownResourceEx {
        
        if (!resources.containsKey(resName))
            throw new AcsJUnknownResourceEx();
        return (Resource<T>) resources.get(resName);
    }
	
    /**
     * Acquires and starts all resources in the collection.
     * @throws AcsJResourceErrorEx
     */    
	public synchronized void acquireResources() throws AcsJResourceErrorEx {
		
		Set<String> keys = resources.keySet();
		Iterator<String> iter = keys.iterator();
		
		while(iter.hasNext()) {
            String resName = iter.next();
			Resource<?> res = resources.get(resName);
			acquireAndStartup(res);            
		}
		// force an update
		update(null, null);
	}
    
    /**
     * Acquires and starts selected resources.
     * @param resourceNames Resources to initialize
     * @throws AcsJResourceErrorEx
     */
    public synchronized void acquireResources(String[] resourceNames)
        throws AcsJResourceErrorEx {
        for (String rn : resourceNames) {
            Resource<?> res = resources.get(rn);
            acquireAndStartup(res);                        
        }
        // force an update
        update(null, null);
    }
    
    /**
     * Acquires and starts a single resource.
     * @param resName Resource name.
     * @throws AcsJResourceErrorEx
     */
    public synchronized void acquireResource(String resName)
        throws AcsJResourceErrorEx {
       Resource<?> res = resources.get(resName);
       acquireAndStartup(res);
    }

    
    /**
     * Acquires and starts a single Resource. If the Resource hasn't been
     * added to the collection yet, then it is added as well.
     *  
     * @param resource Resource
     * @throws AcsJResourceErrorEx
     */
    public synchronized void acquireResource(Resource<?> resource)
        throws AcsJResourceErrorEx {
        if ( ! resources.containsKey(resource.getName()) )
            addResource(resource);
        acquireAndStartup(resource);
    }
    
    /**
     * Acquire resources specifying a timeout.
     * 
     * This function is useful when the collection contains asynchronous
     * resources. It will acquire & start all resources and then wait with a
     * timeout for the collection to go to any of its terminal states:
     * OPERATIONAL, DEGRADED or ERROR.
     * 
     * When asynchronous resources are started (asynchronously), they
     * will notify changes in their state by means of the update() function
     * (using Java Observer/Observable framework). This function blocks
     * waiting for the Resources to go to the terminal states or times out.
     * 
     * @param timeout Timeout [ms]
     * @return False if the wait timed out, True otherwise
     */
    public boolean acquireResources(int timeout) 
        throws AcsJResourceErrorEx, InterruptedException {
        
        acquireResources();
        
        try {
            upLock.lock();
            upWaitedResources = resources.keySet().toArray(new String[0]);
            ResourceCollectionState resCollState =
                getResourceCollState(upWaitedResources);
            logger.finer(name,
                         "acquireResources",
                         "Resource collection state: " + resCollState);
            while (resCollState != ResourceCollectionState.OPERATIONAL &&
                   resCollState != ResourceCollectionState.ERROR &&
                   resCollState != ResourceCollectionState.DEGRADED) {
                boolean ret = upCV.await(timeout, TimeUnit.MILLISECONDS);
                if (!ret) {
                    logger.fine(name,
                                "acquireResources",
                                "Timeout when waiting for all resources.");
                    // Cancel remaining working threads
                    cancelWorkingThreads(upWaitedResources);
                    upWaitedResources = null;
                    return false;
                }
                resCollState = getResourceCollState(upWaitedResources);
                logger.finer(name,
                             "acquireResources",
                             "Resource collection state: "+resCollState);
            }
        } finally {
            upWaitedResources = null;
            upLock.unlock();
        }
        return true;
    }

    /**
     * Acquire selected resources specifying a timeout.
     * 
     * This function is useful when the collection contains asynchronous
     * resources. It will acquire & start all resources and then wait with a
     * timeout for the collection to go to any of its terminal states:
     * OPERATIONAL, DEGRADED or ERROR.
     * 
     * When asynchronous resources are started (an asynchronous operation), they
     * will notify changes in their state by means of the update() function
     * (Java Observer/Observable framework). This function will check the
     * collection state and if it is in any of the terminal states it will
     * wakeup the ResourceManager monitor, finishing the wait. 
     * 
     * @param timeout Timeout [ms]
     * @param resources Resources names to start.
     * @return False if the wait timed out, True otherwise
     */
    public boolean acquireResources(int timeout, String[] resources)
        throws AcsJResourceErrorEx, InterruptedException {
        logger.finest(name,
                      "acquireResources(int, String[])",
                      "Entering");
        
        acquireResources(resources);
        
        try {
            upLock.lock();
            upWaitedResources = resources;
            ResourceCollectionState resCollState =
                getResourceCollState(upWaitedResources);
            logger.finer(name,
                         "acquireResources",
                         "Resource collection state: "+resCollState);
            while (resCollState != ResourceCollectionState.OPERATIONAL &&
                   resCollState != ResourceCollectionState.ERROR &&
                   resCollState != ResourceCollectionState.DEGRADED) {
                boolean ret = upCV.await(timeout, TimeUnit.MILLISECONDS);
                if (!ret) {
                    logger.fine(name, "acquireResources",
                                "Timeout when waiting for all resources.");
                    // Cancel remaining working threads
                    cancelWorkingThreads(upWaitedResources);
                    upWaitedResources = null;
                    return false;
                }
                resCollState = getResourceCollState(upWaitedResources);
                logger.finer(name, "acquireResources",
                             "Resource collection state: "+resCollState);
            }
        } finally {
            upWaitedResources = null;
            upLock.unlock();
        }
        return true;        
    }

    /**
     * Releases all Resources.
     */
	public synchronized void releaseResources() {
		
		Set<String> keys = resources.keySet();
		Iterator<String> iter = keys.iterator();
		
		while(iter.hasNext()) {
            String resName = iter.next();
			Resource<?> res = resources.get(resName);
            logger.finer(name, "releaseResources",
                         "Releasing resource '" + resName + "'");
            shutdownAndRelease(res);
        }        
        update(null, null);
        // resources.clear();
        logger.finer(name, "releaseResources", "Resources released");
	}

    /**
     * Releases one Resource.
     */
    public synchronized void releaseResource(String resourceName) {
        //throws AcsJResourceErrorEx {
        Resource<?> res = resources.get(resourceName);
        shutdownAndRelease(res);
        // resources.remove(resourceName);
    }

    /**
     * Shut down and release selected resources.
     * @param resourceNames Resources to shutdown
     * @throws AcsJResourceErrorEx
     */
    public synchronized void releaseResources(String[] resourceNames)
        throws AcsJResourceErrorEx {
        for (String rn : resourceNames) {
            Resource<?> res = resources.get(rn);
            shutdownAndRelease(res);               
        }
        update(null, null);
    }

    /**
     * Releases all Resources, blocking until all of them have reached the
     * UNINITIALIZED state.
     * 
     * @param timeout Timeout, in milliseconds
     * @return False if the wait timed out, True otherwise.
     */
    public boolean releaseResources(int timeout) {
    
        releaseResources();
    
        try {
            downLock.lock();
            downWaitedResources = resources.keySet().toArray(new String[0]);
            ResourceCollectionState resCollState =
                getResourceCollState(downWaitedResources);
            logger.finer(name,
                         "releaseResources",
                         "Resource collection state: "+resCollState);
            while (resCollState != ResourceCollectionState.UNINITIALIZED) {
                boolean ret = downCV.await(timeout, TimeUnit.MILLISECONDS);
                if (!ret) {
                    logger.fine(name,
                                "releaseResources",
                                "Timeout when waiting for resources to be released.");
                    // Cancel remaining working threads
                    cancelWorkingThreads(downWaitedResources);
                    downWaitedResources = null;
                    return false;
                }
                resCollState = getResourceCollState(downWaitedResources);
                logger.finer(name,
                             "releaseResources",
                             "Resource collection state: "+resCollState);
            }
        } catch (InterruptedException ex) { // nothing sensible to do
        } finally {
            downWaitedResources = null;
            downLock.unlock();
        }
        return true;
    }

    /**
     * Releases selected Resources, blocking until all of them have reached the
     * UNINITIALIZED state.
     * 
     * @param timeout Timeout, in milliseconds
     * @param Resources to release
     * @return False if the wait timed out, True otherwise.
     */
    public boolean releaseResources(int timeout, String[] resources) 
        throws AcsJResourceErrorEx {

        releaseResources(resources);

        try {
            downLock.lock();
            downWaitedResources = resources;
            ResourceCollectionState resCollState =
                getResourceCollState(downWaitedResources);
            logger.finer(name,
                         "releaseResources",
                         "Resource collection state: "+resCollState);
            while (resCollState != ResourceCollectionState.UNINITIALIZED) {
                boolean ret = downCV.await(timeout, TimeUnit.MILLISECONDS);
                if (!ret) {
                    logger.fine(name,
                                "releaseResources",
                                "Timeout when waiting for resources to be released");
                    // Cancel remaining working threads
                    cancelWorkingThreads(downWaitedResources);
                    downWaitedResources = null;
                    return false;
                }
                resCollState = getResourceCollState(downWaitedResources);
                logger.finer(name,
                             "releaseResources",
                             "Resource collection state: "+resCollState);
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace(); // Nothing sensible to do
        } finally {
            downWaitedResources = null;
            downLock.unlock();
        }
        return true;
    }
    
    public synchronized boolean refreshResource(String resourceName, int timeout)
        throws AcsJResourceErrorEx, AcsJUnknownResourceEx {
        
        Resource<?> res = resources.get(resourceName);
        
        if (res == null) {
            AcsJUnknownResourceEx ex = new AcsJUnknownResourceEx();
            ex.setProperty("Details", "Unknown Resource: " + resourceName);
            throw ex;
        }
        
        // Inhibit the asynchronous behaviour in case if asynchronous
        // resources.
        if (res instanceof AsyncResource2)
            res = res.getConcreteResource();
        
        res.shutdown();
        res.release();
        res.acquire();
        res.startup();
        
        long startTime = System.currentTimeMillis();
        while (res.status() != ResourceState.OPERATIONAL) {
            try {
                wait(timeout);
            } catch (InterruptedException ex) {
                // Nothing sensible to do. One way this could happen is
                // if the Container is killed.
                ex.printStackTrace();
            }
            long timeSpan = System.currentTimeMillis() - startTime;
            if (timeSpan >= timeout) {
                logger.fine("Timeout when waiting for resource " +
                            res.getName());
                return false;
            }
        }
        return true;
    }

//    public synchronized void forgetResource(String name) {
//        container.forgetComponent(name);
//        resources.remove(name);
//    }
    
    /**
     * Gets a Resource underlyng component.
     * 
     * @param compName Component name, it should be the same as the Resource name
     * @return ACS Component
     */
	@SuppressWarnings("unchecked")
	public synchronized <T> T getComponent(String compName) 
        throws AcsJNotYetAcquiredEx, AcsJUnknownResourceEx, AcsJBadResourceEx {
		return (T) getResource(compName).getComponent();
	}

    /**
     * Gets a Resource underlyng component, even if the Resource is in a bad state.
     * 
     * @param compName Component name, it should be the same as the Resource name
     * @return ACS Component
     */
    @SuppressWarnings("unchecked")
    public synchronized <T> T getComponentEvenIfBad(String compName) 
        throws AcsJNotYetAcquiredEx, AcsJUnknownResourceEx {
        return (T) getResourceEvenIfBad(compName).getComponent();
    }    
    
    /**
     * Receives a notification from an Observable, in this case
     * an ObservableResource, when its state change.
     * 
     * @see java.util.Observable
     * @see java.util.Observer
     */
    public void update(Observable o, Object arg) {
        logger.finest(name, "update", "Entering");
        
        if (o != null && arg != null) {
            ResourceState rc = (ResourceState) arg;
            ObservableResource<?> res = (ObservableResource<?>) o;
            logger.finer(name,
                         "update",
                         "Receiving notification from " + res.getName() +
                             " , state = " + rc);
        }
        
        ResourceCollectionState newState = getResourceCollState();
        logger.finer(name, "update", "Collection state is " + newState);
        
        if (newState != previousState) {
            setChanged();
            notifyObservers(newState);
        }
        
        try {
            upLock.lock();
            if (upWaitedResources != null) {
                newState = getResourceCollState(upWaitedResources);
                if (newState == ResourceCollectionState.OPERATIONAL ||
                    newState == ResourceCollectionState.ERROR ||
                    newState == ResourceCollectionState.DEGRADED) {
                    logger.finer("Collection state is " + newState + ". Notifying...");
                    upCV.signal();
                }
            }
        } finally {
            upLock.unlock();
        }
        
        try {
            downLock.lock();
            if (downWaitedResources != null) {
                newState = getResourceCollState(downWaitedResources);        
                if (newState == ResourceCollectionState.UNINITIALIZED) {
                    logger.finer("Collection state is " + newState + ". Notifying...");
                    downCV.signal();
                }
            }
        } finally {
            downLock.unlock();
        }
    }
	
    /**
     * Get list of Resources in any transitional state, i.e., states that are
     * not UNINITIALIZED, ERROR, or OPERATIONAL.
     * 
     * @return List of Resources in transitional states.
     */
    public String[] getWorkingResources() {
        List<String> resInTRANSITION = new ArrayList<String>();
        
        Set<String> keys = resources.keySet();
        Iterator<String> iter = keys.iterator();
        
        while(iter.hasNext()) {
            String resName = iter.next();
            Resource<?> res = resources.get(resName);
            ResourceState state = res.status();
            if (state != ResourceState.UNINITIALIZED &&
                state != ResourceState.ERROR &&
                state != ResourceState.OPERATIONAL)
                resInTRANSITION.add(resName);
        }
        return resInTRANSITION.toArray(new String[0]);
    }

    /**
     * Get list of critical Resources in ERROR state.
     * 
     * @return List of critical Resources in ERROR state.
     */
    public String[] getErroneousResources() {
        List<String> erroneousResources = new ArrayList<String>();
        
        Set<String> keys = resources.keySet();
        Iterator<String> iter = keys.iterator();
        
        while(iter.hasNext()) {
            String resName = iter.next();
            Resource<?> res = resources.get(resName);
            ResourceState state = res.status();
            if (state == ResourceState.ERROR)
                erroneousResources.add(resName);
        }
        return erroneousResources.toArray(new String[0]);
    }

    /**
     * Get list of non-critical Resources in ERROR state.
     * 
     * @return List of non-critical Resources in ERROR state.
     */
    public String[] getDegradedResources() {
        List<String> degradedResources = new ArrayList<String>();
        
        Set<String> keys = resources.keySet();
        Iterator<String> iter = keys.iterator();
        
        while(iter.hasNext()) {
            String resName = iter.next();
            Resource<?> res = resources.get(resName);
            ResourceState state = res.status();
            if (state == ResourceState.ERROR && !res.isCritical())
                degradedResources.add(resName);
        }
        return degradedResources.toArray(new String[0]);
    }    
    
    /**
     * Get this ResourceManager String representation.
     */
    public String toString() {
        String rep = "ResourceManager\n";
        rep += String.format("%-40s%s\n", "Name", name);
        rep += "Resources:\n";
        for (Iterator<String> iter = resources.keySet().iterator();
             iter.hasNext(); ) {
            Resource<?> r = resources.get(iter.next());
            rep += String.format("%-40s%s\n", r.getName(), r.status());
        }
        return rep;
    }

    ////////////////////////////////////////////////////////////////////
    // Private methods
    ////////////////////////////////////////////////////////////////////

    private synchronized void acquireAndStartup(Resource<?> resource) 
        throws AcsJResourceErrorEx {
    
        logger.fine(name,
                    "acquireAndStartup",
                    resource.getName() + " state: " + resource.status());
        if (resource.status() == ResourceState.OPERATIONAL ||
            resource.status() == ResourceState.ERROR)
            return;
    
        try {
            resource.up();
        } catch (AcsJResourceErrorEx ex) {
            if (!resource.isCritical()) {
                logger.fine(name,
                            "acquireAndStartup",
                            "Error acquiring resource '" + resource.getName() +
                            "':\n"+
                            Util.getNiceErrorTraceString(ex.getErrorTrace()));
                resource.setException(ex);
                return;
            } else {
                throw ex;
            }
        }
    }

    private synchronized void shutdownAndRelease(Resource<?> resource) {
        logger.fine(name, "shutdownAndRelease", resource.getName() + " state: "
                + resource.status());
        if (resource.status() == ResourceState.UNINITIALIZED)
            return;

        try {
            resource.down();
        } catch (AcsJResourceErrorEx ex) {
            logger.severe("Error releasing resource '"
                                + resource.getName()
                                + "':\n"
                                + Util.getNiceErrorTraceString(ex
                                        .getErrorTrace()));
            resource.setException(ex);
        }
    }
    
    private synchronized void cancelWorkingThreads(String[] resources) {
        String[] resourcesToCancel; 
        if (resources == null)
            resourcesToCancel = this.resources.keySet().toArray(new String[0]);
        else
            resourcesToCancel = resources;
        for (int i = 0; i < resourcesToCancel.length; i++) {
            Resource<?> res = this.resources.get(resourcesToCancel[i]);
            if (res instanceof AsyncResource2) {
                ((AsyncResource2<?>)res).cancel();
            }                
        }
    }
}
