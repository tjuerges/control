package alma.Control.Common;

/**
 * The class <code>ResAcqException</code> indicates error conditions
 * when a <code>Resource</code> object is being acquired.
 * 
 * @author  Rafael Hiriart
 * @version $Id$
 * @see     alma.Control.Common.Resource
 */
public class ResAcqException extends Exception {
    
    private static final long serialVersionUID = 1L;

    public ResAcqException() {
        super();
    }

    public ResAcqException(String message) {
        super(message);
    }

    public ResAcqException(Throwable cause) {
        super(cause);
    }

    public ResAcqException(String message, Throwable cause) {
        super(message, cause);
    }    
}
