/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * $Id$
 */
package alma.Control.Common;

import alma.ResourceExceptions.wrappers.AcsJNotYetAcquiredEx;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;

/**
 * The <code>Resource</code> interface abstracts objects that a component 
 * needs for its operation. These mainly consists in other ACS components.
 * 
 * This interface allows to deal with the common operations involved in
 * acquiring and releasing resources in a uniform way.
 * 
 * This interface is used by the <code>ResourceManager</code>, which provides a
 * way for components to manage its resources.
 * 
 * @author  Rafael Hiriart (rhiriart@nrao.edu)
 * @see     alma.Control.Common.ResourceManager
 */
public interface Resource<C> {

    /**
     * Gets the resource name.
     * 
     * @return Resource name.
     */
	String getName();
	
    /**
     * Gets the resource status.
     * 
     * @return Resource status.
     */
	ResourceState status();
	
    /**
     * Gets a reference to the resource underlying component.
     * 
     * @throws ResAcqException
     * @throws AcsJContainerServicesEx
     */
	void acquire() throws AcsJResourceErrorEx;

	void acquire(boolean force) throws AcsJResourceErrorEx;
	
    /**
     * Releases the underlying component reference.
     */
	void release();

	/**
	 * Performs any required operations for the resource to be operational.
	 * 
	 * @throws AcsJResourceErrorEx
	 */
    void startup() throws AcsJResourceErrorEx;
    
    /**
     * Performs any required operations to clean up the resource.
     * 
     * @throws AcsJResourceErrorEx
     */
    void shutdown() throws AcsJResourceErrorEx;

    /**
     * Acquires and starts up the Resource.
     * 
     * @throws AcsJResourceErrorEx
     */
    void up() throws AcsJResourceErrorEx;
    
    /**
     * Shuts down and releases the Resource.
     * 
     * @throws AcsJResourceErrorEx
     */
    void down() throws AcsJResourceErrorEx;
    
    /**
     * Get the underlying component.
     * @return An instance of the class defined by the generic parameter.
     * @throws AcsJNotYetAcquiredEx 
     */
	C getComponent() throws AcsJNotYetAcquiredEx;

    /**
     * If a resource is critical, then if an exception is thrown when the resource
     * is being acquired then the ResourceManager re-throws the exception and stops
     * the acquisition of the remaining resources. This allows the component using
     * the ResourceManager to stop its initialization, as a critical resource for its
     * operation will be missing.
     * 
     * If a resource is not critical, then in the case of an exception being thrown
     * during the resource acquisition, the ResourceManager will flag the resouce as
     * bad and continue acquiring the remaining resources. The component should go to
     * some sort of degraded mode. 
     */
    boolean isCritical();
    
    /**
     * Set exception.
     * For some cases a Resource will need to keep a memory of its last exception.
     * This is the case for non-critical resources and asynchronous resources.
     * @param error Exception.
     */
    void setException(Throwable error);
    
    /**
     * Get last exception.
     * For some cases a Resource will need to keep a memory of its last exception.
     * This is the case for non-critical resources and asynchronous resources.
     * @return Last exception or null if there is not one.
     */
    Throwable getLastException();
    
    /**
     * Clear last exception.
     * For some cases a Resource will need to keep a memory of its last exception.
     * This is the case for non-critical resources and asynchronous resources.
     */
    void clearException();
    
    Resource<C> getConcreteResource();
    
}

// __oOo__
