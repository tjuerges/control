// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#include "progressCallbackImpl.h"
#include <acsContainerServices.h>
#include <ControlExceptions.h>
#include <ControlCommonExceptions.h>
#include <Time_Value.h>
#include <sstream>
#include <cstring>
#include <Semaphore.h>


using Control::ProgressCallbackImpl;
using ControlExceptions::TimeoutExImpl;
using ControlCommonExceptions::OSErrorExImpl;
using std::ostringstream;


ProgressCallbackImpl::ProgressCallbackImpl(maci::ContainerServices* cs):
    containerServices_m(cs),
    completionSemaphore_m(0, USYNC_PROCESS)
{
    externalReference_m = Control::ProgressCallback::_narrow(
        containerServices_m->activateOffShoot(this));
}

ProgressCallbackImpl::~ProgressCallbackImpl()
{
    deactivateCallback();
    completionSemaphore_m.remove();
}
void ProgressCallbackImpl::report(const ACSErr::Completion& completion)
{
    completion_m.push_back(completion);
    completionSemaphore_m.release();
}

CompletionImpl ProgressCallbackImpl::waitForCompletion(double timeout)
{
    ACE_Time_Value to;
    to.set(timeout);
    to += ACE_OS::gettimeofday();

    const int response(completionSemaphore_m.acquire(to));
    const int myErrno(errno);
    if(response == -1)
    {
        // These are the error cases
        if(myErrno == ETIME)
        {
            TimeoutExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
            ostringstream msg;
            msg << "Timed out waiting for response from target object. "
                << "Timeout is "
                << timeout
                << " secs";
            ex.addData("Detail", msg.str());
            ex.log();
            throw ex.getTimeoutEx();
        }
        else
        {
            OSErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
            ostringstream msg;
            msg << "An operating system error was encountered, Error String: "
                << std::strerror(myErrno);
            ex.addData("Detail", msg.str());
            ex.log();
            throw ex.getOSErrorEx();
        }
    }

    // We got a response now return the completion
    ACSErr::CompletionImpl cmplImpl(completion_m.front());
    completion_m.pop_front();
    return cmplImpl;
}

CompletionImpl ProgressCallbackImpl::waitForCompletion(
    ACS::TimeInterval timeout)
{
    return waitForCompletion(timeout * 100e-9);
}

Control::ProgressCallback_ptr ProgressCallbackImpl::getExternalReference()
{
    return externalReference_m.in();
}

void ProgressCallbackImpl::deactivateCallback()
{
    if(CORBA::is_nil(externalReference_m.in()))
    {
        return;
    }

    containerServices_m->deactivateOffShoot(this);
    externalReference_m = Control::ProgressCallback::_nil();
}
