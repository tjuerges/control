//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <simpleCallbackImpl.h>
#include <sstream>
#include <loggingACEMACROS.h>
#include <acsContainerServices.h>
#include <ctime>
#include <cstdio>
#include <ControlExceptions.h>
#include <ControlCommonExceptions.h>

SimpleCallbackImpl::SimpleCallbackImpl(maci::ContainerServices* cs):
    containerServices_m(cs),
    completionSemaphore_m(0, USYNC_PROCESS)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    externalReference_m = Control::SimpleCallback::_narrow(
        containerServices_m->activateOffShoot(this));
}

SimpleCallbackImpl::~SimpleCallbackImpl()
{
    deactivateCallback();
    completionSemaphore_m.remove();
}

void SimpleCallbackImpl::report(const ACSErr::Completion& completion)
{
    completion_m = completion;
    completionSemaphore_m.release();
}

void SimpleCallbackImpl::waitForCompletion(ACS::Time timeout)
{
    ACE_Time_Value t;
    t.set(timeout / 1e7);

    ACE_Time_Value tv = ACE_OS::gettimeofday();
    tv += t;

    const int response(completionSemaphore_m.acquire(tv));
    const int myErrno(errno);
    if(response == -1)
    {
        // These are the error cases
        if(myErrno == ETIME)
        {
            ControlExceptions::TimeoutExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            std::ostringstream msg;
            msg << "Timed out waiting for response from target object. "
                << " Timeout is "
                << timeout / 1.0E7
                << " s";
            ex.addData("Detail", msg.str());
            ex.log();
            throw ex;
        }
        else
        {
            ControlCommonExceptions::OSErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            std::ostringstream msg;
            msg << "An operating system error was encountered, Error String: "
                << std::strerror(myErrno);
            ex.addData("Detail", msg.str());
            ex.log();
            throw ex;
        }

        deactivateCallback();
    }

    // We got a response now check the completion
    ACSErr::CompletionImpl cmplImpl(completion_m);
    if(cmplImpl.isErrorFree() == false)
    {
        // For now just throw a generic ControlExceptions, we might clean
        // this up as part of the exception handling refactor
        ControlCommonExceptions::AsynchronousFailureExImpl ex(__FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", cmplImpl.getErrorTraceHelper()->toString());
        throw ex;
    }
}

Control::SimpleCallback_ptr SimpleCallbackImpl::getExternalReference()
{
    return externalReference_m.in();
}

void SimpleCallbackImpl::deactivateCallback()
{
    if(CORBA::is_nil(externalReference_m.in()))
    {
        // Already deactivated
        return;
    }

    externalReference_m = Control::SimpleCallback::_nil();

    if(containerServices_m != NULL)
    {
        containerServices_m->deactivateOffShoot(this);
    }
}
