/*
 * Copyright (c) 2004 by Cosylab d.o.o.
 *
 * The full license specifying the redistribution, modification, usage and other
 * rights and obligations is included with the distribution of this project in
 * the file license.html. If the license is not included you may find a copy at
 * http://www.cosylab.com/legal/abeans_license.htm or may write to Cosylab, d.o.o.
 *
 * THIS SOFTWARE IS PROVIDED AS-IS WITHOUT WARRANTY OF ANY KIND, NOT EVEN THE
 * IMPLIED WARRANTY OF MERCHANTABILITY. THE AUTHOR OF THIS SOFTWARE, ASSUMES
 * _NO_ RESPONSIBILITY FOR ANY CONSEQUENCE RESULTING FROM THE USE, MODIFICATION,
 * OR REDISTRIBUTION OF THIS SOFTWARE.
 */

package alma.tics.gui.util;

/**
 * Converts from ACS time to UTC and back.
 *
 * @author <a href="mailto:igor.kriznar@cosylab.com">Igor Kriznar</a>
 */
public final class TimeUtilities
{
	/**
			 *
			 */
	private TimeUtilities()
	{
		super();
	}

	/**
	 * Converts ACS time in ms to UTC.
	 *
	 * @param acsTime ACS time in ms
	 *
	 * @return UTC time
	 */
	public static long toUTC(long acsTime)
	{
		return acsTime - 40587L;
	}

	/**
	 * Converrts UTC time to ACS in ms.
	 *
	 * @param utcTime UTC time
	 *
	 * @return ACS timein ms
	 */
	public static long toACS(long utcTime)
	{
		return utcTime + 40587L;
	}

	/**
	 * Converts ACS time in hours to UTC.
	 *
	 * @param acsHours ACS time in hours
	 *
	 * @return UTC time
	 */
	public static long toUTC(double acsHours)
	{
		return (long)((acsHours - 40587.0) * 86400000.0);
	}

	/**
	 * Converts UTC time to ACS time in hours
	 *
	 * @param utcTime UTC time
	 *
	 * @return ACS time in hours
	 */
	public static double toACSHours(long utcTime)
	{
		return utcTime / 86400000.0 + 40587.0;
	}

	/**
	 * Test
	 *
	 * @param args ignored
	 */
	public static void main(String[] args)
	{
		long t = System.currentTimeMillis();
		System.out.println("NOW " + t);

		double acsH = toACSHours(t);
		System.out.println("ACS " + acsH);

		long acsT = toACS(t);
		System.out.println("ACS " + acsT);
		System.out.println("NOW " + toUTC(acsH));
		System.out.println("NOW " + toUTC(acsT));
	}
}

/* __oOo__ */
