#ifndef ErrorStateHelper_H
#define ErrorStateHelper_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2007-08-07  created
*/

/************************************************************************
 * This is a helper class designed to support devices which have multiple
 * error states that need to be tracked.  The class is based on an integer
 * which contains the bit pattern for errors, and an enumeration which
 * describes the various error conditions.
 * Note the enumeration should be set up so each value maps to a unique bit
 * (i.e. enum Errors { ErrorType1 = 0x01, ErrorType2=0x02, ErrorType3=0x04} )
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <iostream>

template <typename E> class ErrorStateHelper {
 public:
    virtual ~ErrorStateHelper();

 protected:
  /* Simple Default Contstructer, resets all error flags */
  ErrorStateHelper();

  /*
    Method to set a new error specified by the input enumeration
    if this bit was previously unset then the handleError method is
    called 
  */
  void setErrorFlag(E newError);

  /* 
     Method to reset the bit associated with associated error state
     enumeration value.  If the error was previously set, clear error
     is called.
  */
  void resetErrorFlag(E clearedError);

  /*
    Reset all error flags associated with this device, if any flags were
    previously set, clearError is called 
  */
  void resetErrorFlags();

  /*
    Returns true if _any_ error state bit is set 
  */
  bool errorFlagSet();

  /* 
     Returns true if the specified error bit is set
  */
  bool errorFlagSet(E errorState);

  /*
    Abstract methods wich specify how the device should
     respond to various error states.
  */
  virtual void handleSetErrorFlag(E newError) = 0;
  virtual void handleResetErrorFlag(E newError) = 0;

 private:
  /* The bit pattern representing the current error state */
  long long errorStateRegister_m;

};

template<typename E>
ErrorStateHelper<E>::ErrorStateHelper():
  errorStateRegister_m(0)
{
  ACS_TRACE("ErrorStateHelper::ErrorStateHelper");
}

template< typename E >
ErrorStateHelper<E>::~ErrorStateHelper()
{
    ACS_TRACE("ErrorStateHelper::~ErrorStateHelper");
}

template< typename E>
void ErrorStateHelper<E>::resetErrorFlags() {
  if (errorStateRegister_m != 0) {

    for (unsigned int bit = 0; bit < 8*sizeof(E); bit++) {
      E bitPattern = static_cast<E>(0x01LL << bit);
      if (errorFlagSet(bitPattern)) {
	resetErrorFlag(bitPattern);
      }
    }
    errorStateRegister_m = 0;
  }
}

template<typename E>
bool ErrorStateHelper<E>::errorFlagSet() {
  return errorStateRegister_m != 0;
}

template<typename E>
void ErrorStateHelper<E>::setErrorFlag(E newError) {
  if (!(errorStateRegister_m & newError)){
    errorStateRegister_m |= newError;
    handleSetErrorFlag(newError);
  }
}

template<typename E>
void ErrorStateHelper<E>::resetErrorFlag(E clearedError){
  if (errorStateRegister_m & clearedError) {
    errorStateRegister_m &= ~clearedError;
    handleResetErrorFlag(clearedError);
  }
}

template<typename E>
bool ErrorStateHelper<E>::errorFlagSet(E errorType) {
  return errorStateRegister_m & errorType;
}
#endif //ErrorStateHelper
