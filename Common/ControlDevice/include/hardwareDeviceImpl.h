#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef hardwareDeviceImpl_h
#define hardwareDeviceImpl_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <HardwareDeviceS.h>
#include <string>
#include <controlDeviceImpl.h>
#include <controlAlarmSender.h>
#include <MonitorCollectorC.h>
#include <MonitorCollectorErr.h>
// Protection Mutex for the error counter on GCC < 4 and for the
// hardware lifecycle methods.
#include <Mutex.h>


namespace Control
{
    class HardwareDeviceImpl: public virtual POA_Control::HardwareDevice,
        public Control::CharacteristicControlDeviceImpl
    {
        public:
        HardwareDeviceImpl(const ACE_CString& name,
            maci::ContainerServices *containerServices);

        virtual ~HardwareDeviceImpl();

        /// Hardware Lifecycle Methods: hwStart
        /// \exception  ControlDeviceExceptions::HwLifecycleEx
        virtual void hwStart();

        /// Hardware Lifecycle Methods: hwConfigure
        /// \exception  ControlDeviceExceptions::HwLifecycleEx
        virtual void hwConfigure();

        /// Hardware Lifecycle Methods: hwInitialize
        /// \exception  ControlDeviceExceptions::HwLifecycleEx
        virtual void hwInitialize();

        /// Hardware Lifecycle Methods: hwOperational
        /// \exception  ControlDeviceExceptions::HwLifecycleEx
        virtual void hwOperational();

        /// Hardware Lifecycle Methods: hwDiagnostic
        /// \exception  ControlDeviceExceptions::HwLifecycleEx
        virtual void hwDiagnostic();

        /// Hardware Lifecycle Methods: hwSimulation
        /// \exception  ControlDeviceExceptions::HwLifecycleEx
        virtual void hwSimulation();

        /// Hardware Lifecycle Methods: hwStop
        virtual void hwStop();

        /// Method to turn the monitoring of the hardware on.
        /// \exception  ControlDeviceExceptions::HwLifecycleEx
        virtual void monitoringOn();

        /// Method to turn off  monitoring of the assembly.
        /// \exception  ControlDeviceExceptions::HwLifecycleEx
         virtual void monitoringOff();

        /// Method to Register with Monitor Collector Component
        /// \exception
        virtual void registerWithCollector();

        /// Method to Deregister  with Monitor Collector Component
        /// \exception
        virtual void deregisterWithCollector();

        /// Method to get state of current monitoring status
        /// \exception
        virtual bool isMonitoring();

        /// Method to set the serial number of an assembly
        /// \exception
        void setSerialNumber(const std::string& serialNumber);

        /// Method to get the serial number of an assembly.  The serial number
        /// is available as soon as \ref hwConfigure() has been called.
        /// \exception ControlExceptions::INACTErrorEx is thrown if the device
        /// state is not Control::HardwareDevice::Configure,
        /// Control::HardwareDevice::Initialize,
        /// Control::HardwareDevice::Operational,
        /// Control::HardwareDevice::Simulation or
        /// Control::HardwareDevice::Diagnostic.
        virtual char* getSerialNumber();


        // ACS Component Lifecycle Methods

        /// ACS Lifecycle Method
        /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
        virtual void initialize();

        /// ACS Lifecycle Method
        /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
        virtual void cleanUp();

        /// Return the current hardware state, i.e. the state from the CONTROL
        /// subsystem point of view.
        /// \return The hardware state.
        virtual Control::HardwareDevice::HwState getHwState();

        /// Typefdef for the members of Control::HardwareDeviceImpl which test
        /// the current hardware state.  Used in \ref checkHwStateOrThrow.
        typedef bool (Control::HardwareDeviceImpl::*HwStateFunction)();

        /// Check if the hardware component is in hwOperational state.  For
        /// this the return value of \ref isReady() compared with false.
        /// true -> throw ControlExceptions::INACTErrorExImpl.  This method
        /// avoids code duplication in autogenerated code.
        /// \exception ControlExceptions::INACTErrorExImpl
        void checkHwStateOrThrow(HwStateFunction ptr, const std::string& file,
            unsigned int line, const std::string& function);

        /// This method returns true if we are in the hwOperational
        /// hwSimulation or hwDiagnostic modes.
        /// \return True if in hwOperational, hwSimulation, hwDiagnostic.
        virtual bool isReady();

        /// Check if the device is in a state past hwStart.
        /// \return True if state is hwConfigure, hwOperational, hwSimulation
        /// or hwDiagnostic.
        virtual bool isStartup();

        /// Return if the device is in hwDiagnostic.
        /// \return True if state is hwDiagnostic.
        virtual bool isDiagnostic();

        /// Hierarchy error reporting.
        virtual void setSubdeviceError(const char* subdeviceName);

        /// Clear hierarchy error.
        virtual void clearSubdeviceError(const char* subdeviceName);

        /// Report the current hwState as std::string.
        /// \return Current hwState.
        static std::string stateToString(
            Control::HardwareDevice::HwState state);

        /// Log a transition from one hwState to another hwState.
        /// \param oldState Current hwState.
        /// \param newState New hwState.
        void logNewState(Control::HardwareDevice::HwState oldState,
            Control::HardwareDevice::HwState newState);

        /// Retrieve the value of the device communication error counter.
        virtual CORBA::ULong getDeviceCommunicationErrorCounter();

        /// Increments the error counter for device communication errors by
        /// one.  If the counter reaches \ref MAX_ERROR_COUNTER, \ref hwStop()
        /// will be called.
        /// \return Current value of the error counter.
        unsigned int incrementDeviceErrorCounter();

        /// Clears the counter for device communication errors.
        void clearDeviceErrorCounter();

        /// clear device communication error latched alarm.
        void clearDeviceCommunicationErrorAlarm();


        protected:
        std::string antennaLoc_m;

        bool diagnosticModeEnabled;
        bool simulationModeEnabled;

        bool monitoring;

        TMCDB::MonitorCollector_ptr monitorCollector_m;

        virtual void hwStartAction() = 0;
        virtual void hwConfigureAction() = 0;
        virtual void hwInitializeAction() = 0;
        virtual void hwOperationalAction() = 0;
        virtual void hwStopAction() = 0;

        /// These two are defined here for the cases where they are disabled.
        virtual void hwDiagnosticAction() {};
        virtual void hwSimulationAction() {};

        virtual bool checkMonitor();

        #if __GNUC__ < 4
        /// Protection Mutex for the error counter.  Only used on if the
        /// software was compiled with a GCC version < 4.
        ACE_Mutex m_ErrorCounterMutex;
        #endif

        /// Protection Mutex for hardware lifecycle methods.
        ACE_Mutex hwLifecycleMutex;

        /// Maximum number of allowed communication errors.
        const unsigned int MAX_ERROR_COUNTER;

        /// Error counter alarm code for the alarm system.
        const unsigned int ERROR_COUNTER_ALARM_CODE;

        /// Error counter.  Defined as volatile because multiple threads may
        /// access it at the same time.  The volatile keyword prevents the
        /// compiler from keeping this variable in a register or optimising
        /// read/write accesses.
        volatile unsigned int errorCounter;

        /// Boolean which tells the error counter handler if the component is
        /// currently executing hwStop or is already in hwStop state.  If that
        /// is true, the error handler will not report any errors and not
        /// increment the \ref errorCounter because once the \ref errorCounter
        /// reaches \ref MAX_ERROR_COUNTER, the handler would call hwStop and
        /// trigger a possibly endless recursion.
        volatile bool isInHwStop;


        private:
        /// No default ctor.
        HardwareDeviceImpl();

        /// No copy-ctor.
        HardwareDeviceImpl(const HardwareDeviceImpl&);

        /// No assignment optor.
        HardwareDeviceImpl& operator=(const HardwareDeviceImpl&);

        /// This method does the real work for \ref hwStop().
        /// \param calledFromHwStop is true per default which signals that
        /// a call to hwStopAction should be made.  If for some reason an
        /// internal call is made to hwStopDoWork, hwStopAction is not called.
        void hwStopDoWork(bool calledFromHwStop = true);

        /// This method currently does nothing
        void archiveStateChange(Control::HardwareDevice::HwState newState);

        /// This is the state of the device.*/
        Control::HardwareDevice::HwState deviceState_m;

        /// Alarm sender for the communication error latched alarm.
        Control::AlarmSender alarmSender_m;

        /// Hexadecimal serial number of the device represented as string.
        std::string serialNumber_m;
    };
};
#endif
