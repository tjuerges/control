#ifndef MonitorHelper_H
#define MonitorHelper_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2007-08-07  created
*/

/************************************************************************
 * This Class is designed to help with devices which need the following
 * pattern:
 * A command or monitor is queued to executed in the future, and a
 * thread is needed to ensure that the request executed correctly, and
 * take action in case of an error.
 *
 * The basic pattern is the following:
 * 1) The device gets a AMBRequestStruct by using the "getRequestStruct" method
 * 2) The pertanent information is filled into the structure
 *     (i.e. RCA time etc) NOTE: The SynchLock and Status have already
 *      been correctly initialized.
 * 3) The device calls monitorTE, commandTE as necessary to queue the
 *    request for execution by the real time system
 *    the device calls queueRequest() with the pointer to the structure.
 *    At this point the Structure "Belongs" to the Monitor helper class and
 *    should not be used by the Device class anymore.
 *
 *  When the request has been executed by the Real Time Layers, the abstract
 * method processRequestResponse() is called and the device should inspect
 * the AMBRequestStruct to verify the correct execution of the request and
 * any other error handling as needed.
 *
 * Basic lifecycle concepts:
 *   The initialize method should be called in the initialize method of the
 *  derived class, in most cases the code generated base class initializer
 *  should be called first then the initializer for this class.
 *
 *  The cleanUp method should similarly be called in the cleanUp method,
 *    after the cleanUp method of the generated base class.
 *
 *  The resume method simply activates the monitoring thread, in general I
 *     suggest calling it in the hwInitializeAction method of the derived class
 *
 *  The suspend method deactivates the monitoring thread, and is usally called
 *     in the hwStopAction method.  To avoid having "stuck" requests that
 *     never get get processed, I suggest suspending any writer threads,
 *     then flushing the Real Time Queues for this device before calling
 *     the suspend method.
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif


#include <ambDefs.h>

// For the device name:
#include <string>


namespace maci
{
    class ContainerServices;
};


class MonitorHelper
{
    public:
    /// Structure describing the request, the SynchLock and Status
    /// fields will already be initialized when the structure is
    /// returned from the getRequestStruct() method.
    ///
    /// Timestamp will be filled by the real time system with the
    /// actual exection time of the request
    struct AMBRequestStruct
    {
        AMBRequestStruct(sem_t* synchLock);

        AmbRelativeAddr RCA;
        ACS::Time TargetTime;
        AmbDataLength_t DataLength;
        AmbDataMem_t Data[8];
        sem_t* SynchLock;
        ACS::Time Timestamp;
        AmbErrorCode_t Status;
    };


    protected:
    /// The actual worker thread
    class MonitorThread: public ACS::Thread
    {
        public:
        MonitorThread(const ACE_CString& name, const MonitorHelper* helper);
        virtual ~MonitorThread();
        virtual void runLoop();

        protected:
        MonitorHelper* monitorHelper_p;

        private:
        std::string myName;
    };


    friend class MonitorHelper::MonitorThread;

    /// Constructor.
    /// \attention Deprecated.  Developers should use the
    /// \ref MonitorHelper(const std::string&) constructor instead and pass
    /// the device name as std::string parameter to it.
    MonitorHelper();

    /// Constructor which allows the creator to pass its component name for
    /// logging purposes.
    /// @parm deviceName: component name of the creating component.
    MonitorHelper(const std::string& deviceName);

    /// Destructor.
    virtual ~MonitorHelper();

    /// Component Lifecycle Method
    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    void initialize(ACE_CString compName, maci::ContainerServices* cs);

    /// Component Lifecycle Method
    void cleanUp();

    /// Enable the active monitoring
    void resume();

    /// Disable the active monitoring
    void suspend();

    /// Get an AMBRequestStruct to use, the SynchLock and Status
    /// fields will already be initialized when the structure is
    /// returned.  This structure either must passed back to the
    /// class with a call to queueRequest, or deleted by the
    /// device implementation.
    MonitorHelper::AMBRequestStruct* getRequestStruct();

    /// Inform the MonitorHelper class of a new request which state
    /// should be monitored.  One this method is called structure
    /// pointed to by the newRequest belongs to the Monitor Helper
    /// class, which is reponsible for deleting it once it is no longer
    /// in use.
    void queueRequest(MonitorHelper::AMBRequestStruct* newRequest);

    /// This is the method called by the thread which will handle any requests
    /// which have been executed by the real time system, in general the user
    /// should not need to call this method.
    void processPendingQueue();

    /// Abstract method called for every executed request.  This is where the
    /// device specific error handeling should be done.
    virtual void processRequestResponse(
        const MonitorHelper::AMBRequestStruct& response) = 0;


    private:
    /// No copy constructor.
    MonitorHelper(const MonitorHelper&);

    /// No assignment operator.
    MonitorHelper& operator=(const MonitorHelper&);

    /// Semaphore for the avalible request deque
    sem_t availableRequestSem_m;

    /// Semaphore for the pending request list
    sem_t pendingRequestSem_m;

    /// Semaphore used to synchronize with RT layer
    sem_t synchronizationSem_m;

    /// Deque which holds currently unused requests
    std::deque< MonitorHelper::AMBRequestStruct* > availableRequests_m;

    /// List holding all pending requests
    std::list< MonitorHelper::AMBRequestStruct* > pendingRequests_m;

    /// The thread which drives the processing
    MonitorThread* monitorThread_p;

    /// This variable contains the device name plus ::MonitorHelper::.
    std::string myName;
};


#endif /*!MonitorHelper_H*/
