#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

#ifndef hardwareControllerImpl_h
#define hardwareControllerImpl_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// jkern  2006-06-06  created
//


#include <controlDeviceImpl.h>
#include <ControlBasicInterfacesC.h>
#include <HardwareDeviceC.h>
#include <HardwareControllerS.h>
#include <vector>
#include <string>
#include <Recursive_Thread_Mutex.h>
// Protection Mutex the controller lifecycle methods.
#include <Mutex.h>


namespace Control
{
    class HardwareControllerImpl: public Control::CharacteristicControlDeviceImpl,
        public virtual POA_Control::HardwareController
    {
        public:
        HardwareControllerImpl(const ACE_CString& name,
            maci::ContainerServices *containerServices);

        virtual ~HardwareControllerImpl();

        /// Called when the component is shut down. Makes certain that the
        /// controllerShutdown method has been executed.
        virtual void cleanUp();

        virtual Control::HardwareController::ControllerState getState();

        virtual void controllerOperational();

        virtual void controllerShutdown();

        virtual Control::BadResourceSeq* getBadSubdevices();

        virtual void setSubdeviceError(const char* subdeviceName);

        virtual void clearSubdeviceError(const char* subdeviceName);

        /// \return A formatted string containing the LRU name and the error
        /// message for that LRU.  Example output:
        /// DTXBBpr0: Yo, I am broke.
        /// FrontEnd/Stuff: Aw man, I am broke, too!
        virtual char* getErrorMessage();

        /// Start or stop the flagging of the data.
        /// \param startStop: true = start, false = stop the flagging of data.
        /// \param timeStamp: The begin or end of the data flagging.
        /// \param componentName: Something like "CONTROL/Willy/IFProc0" or
        /// "CONTROL/Foo/DRXBBPr3".
        /// \param description: This is a free format string which contains the
        /// reason for the flagging of the data.
        virtual void flagData(bool startStop, ACS::Time timeStamp,
            const char* componentName, const char* description);


        protected:
        /// Protection Mutex for the controller lifecycle methods.
        ACE_Mutex controllerLifecycleMutex;

        /// Tries to bring a device to hwOperational state.
        /// \param hwDevice is a pointer to a device reference.  It is checked
        /// if it is non-nil and valid.
        /// \return false if the device is a Control::HardwareController and
        /// not a Control::HardwareDevice.
        bool setDeviceOperational(std::map<
            std::string, Control::ControlDevice_ptr >::iterator& iter);

        /// Checks the local error stack and sets the errors in the parent
        /// accordingly.
        void setParentError();

        virtual void setControllerState(
            Control::HardwareController::ControllerState newState);

        Control::HardwareController::ControllerState currentState_m;

        /// Mutex to protect the badSubdeviceList.
        ACE_Recursive_Thread_Mutex badDeviceMutex_m;

        /// A list of subdevices that could not be initialised or somehow reported
        /// an error otherwise.
        std::vector< std::string > badSubdeviceList_m;
    };
};
#endif
