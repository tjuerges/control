#ifndef TESTCONTROLDEVICEIMPL_H
#define TESTCONTROLDEVICEIMPL_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2005 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2005-07-30  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <ControlDeviceS.h>
#include <controlDeviceImpl.h>
//#include <acscomponentImpl.h>
//#include <ControlCommonInterfacesC.h>

class TestControlDeviceImpl: public virtual POA_Control::ControlDevice,
			     public Control::CharacteristicControlDeviceImpl
{
  public:
    TestControlDeviceImpl(const ACE_CString& name,
			  maci::ContainerServices *containerServices);
    
    ~TestControlDeviceImpl();

    virtual void setSubdeviceError(const char* subdeviceName);

    virtual void clearSubdeviceError(const char* subdeviceName);
    
};

#endif //!TESTCONTROLDEVICEIMPL_H
