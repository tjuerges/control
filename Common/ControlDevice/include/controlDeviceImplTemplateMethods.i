#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef controlDeviceImplTemplateMthods_i
#define controlDeviceImplTemplateMthods_i
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Feb 28, 2011  created
//


#include <controlDeviceImpl.h>
#include <sstream>
#include <string>
#include <map>
#include <ControlExceptions.h>
#include <ControlDeviceExceptions.h>


namespace Control
{
    template< class componentInterfaceType, class acscomponentImpl >
    template< typename SubdeviceReference >
    SubdeviceReference* ControlDeviceImpl<componentInterfaceType, acscomponentImpl>::getSubdeviceReference(
            const std::string& lruType)
    {
        std::map< std::string, Control::ControlDevice_ptr >::const_iterator iter(
            findLruTypeAndCheckIfNil(lruType));
        SubdeviceReference* ptr(SubdeviceReference::_narrow((*iter).second));
        if(CORBA::is_nil(ptr) == true)
        {
            // Narrowing the subdevice reference to a SubdeviceReference reference
            // failed.  This is bad.
            ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);

            std::ostringstream output;
            output << "Could not properly narrow the Control::ControlDevice* "
                << lruType
                << " reference.";
            LOG_TO_DEVELOPER(LM_ERROR, output.str());
            ex.addData("Detail", output.str());
            throw ex;
        }

        return ptr;
    }

    template< class componentInterfaceType, class acscomponentImpl >
    void ControlDeviceImpl< componentInterfaceType, acscomponentImpl >::createSubdevices(
        const DeviceConfig& config, const char* parentName)
    {
        ACS_TRACE(__PRETTY_FUNCTION__);

        setParent(parentName);
        setDeviceReferenceName(config);

        // Go through the sequence of subdevices getting references
        for(unsigned int idx(0U); idx < config.subdevice.length(); ++idx)
        {
            try
            {
                getSubdeviceReference(config.subdevice[idx]);
            }
            catch(const ControlDeviceExceptions::IllegalConfigurationExImpl& ex)
            {
                throw ControlDeviceExceptions::IllegalConfigurationExImpl(
                    ex, __FILE__, __LINE__,
                    __PRETTY_FUNCTION__).getIllegalConfigurationEx();
            }
        }
    }
};
#endif
