#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef controlAlarmHelper_h
#define controlAlarmHelper_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <string>
#include <memory>
#include <vector>
#include <map>
#include <ACSAlarmSystemInterfaceFactory.h>
#include <AlarmSystemInterface.h>
#include <FaultState.h>
#include <faultStateConstants.h>
#include <acscommonC.h>


namespace Control
{

    /// Alarm Information necessary to initialise the AlarmHelper.
    struct AlarmInformation
    {
        /// ctor.
        AlarmInformation();

        /// dtor.
        ~AlarmInformation();

        ///< int. This contains the alarm code, which should be unique within
        /// each device.
        int alarmCode;

        /// < int. How many time the alarm must be deactivated until it is
        /// actually cleared.
        unsigned int alarmTerminateCount;

        /// < int. How many time the alarm must be activated until it is
        /// actually triggered.
        unsigned int alarmActivateCount;

        /// < std:string. Optional description for the alarm. This is NOT
        /// used by the ALARM SYSTEM
        std::string alarmDescription;
    };

    /// Alarm helper class.
    /// This class is the intended replacement for the erroHelper which is
    /// extended by some control devices. This class should be extended in its
    /// replacement. 2 method must be implemented since this is an abstract
    /// class.
    /// handleActivateAlarm: what should be done when an alarm goes off.
    /// handleDeactivateAlarm: what should be done when an alarm goes on.
    class AlarmHelper
    {
        public:
        /// ctor.
        AlarmHelper();

        /// dtor.
        virtual ~AlarmHelper();

        /// Manual implementation of the copy constructor, since
        /// the default copy constructor does a shallow copy.
        /// The guilty party is the ACSAlarmSystemInterfaceFactory
        ///
        AlarmHelper(const AlarmHelper &original);

        /// Check if an alarm code has been configured.
        /// If code is not configured an error log is sent.
        /// @param code. The alarm code to check.
        /// @return True if the alarm code is configured. False if not.
        virtual bool findAlarm(const int code);

        /// Revert the action done by initializeAlarms
        /// @see initializeAlarms()
        virtual void resetAlarms();

        /// Initialise the alarm list. This method can be called multiple
        /// times. If alarm codes are repeated the latest setting will take
        /// precedence. It is possible that an alarm that is already active
        /// gets reinitialised with a different fault member name. In this
        /// case the control alarm helper will clear the old alarm.
        /// @param family a string which contains the family of the alarms in
        /// alarmInformation.
        /// @param member a string which identifies the meber of the alarms in
        /// alarmInformation.
        /// @param alarmInformation a vector of AlarmInformation
        /// @see AlarmInformation()
        virtual void initializeAlarms(const std::string family,
            const std::string member,
            const std::vector< AlarmInformation > alarmInformation);

        /// Activate an alarm.  Calling this method does not guarantee that
        /// the alarm will be triggered. If the activateCommandCounter for the
        /// specified alarm has not reached the alarmActivateCount, it will
        /// only increment the alarCount but will not be published, nor the
        /// action handler called.
        /// @param code as int.
        virtual void activateAlarm(const int code);

        /// Deactivate an alarm.  Calling this method does not guarantee that
        /// the alarm will be turned off.  If the terminateCommandCounter for
        /// the specified alarm has not reached the alarmDeactivateCount, it
        /// will only increment the terminateCommandCounter but will not be
        /// published, nor the action handler called.
        /// @param code as int.
        virtual void deactivateAlarm(const int code);

        /// Update an alarm which is triggered by a boolean value.
        /// This is a helper function that will mimic activateAlarm if the
        /// state is TRUE and deactivateAlarm if the state is FALSE.
        /// @param code as int
        /// @param state as boolean
        virtual void updateAlarm(const int code, const bool state);

        /// \return true or false depending if the alarm identified by code is
        /// set.
        /// @see activateAlarm(int code)
        /// @see deactivateAlarm(int code)
        virtual bool isAlarmSet();

        /// \return true or false depending if the alarm identified by code is
        /// set.
        /// @see activateAlarm(int code)
        /// @see deactivateAlarm(int code)
        virtual bool isAlarmSet(const int code);

        /// Inhibit alarms for a period of time.  During this period of not
        /// alarms will be published, unless forceTerminateAllAlarms() is
        /// called.
        /// @param duration as an ACS::Time.
        /// @see forceTerminateAllAlarms()
        virtual void inhibitAlarms(const ACS::Time duration);

        /// Reset the alarm inhibition time.  This clears the inhibition time,
        /// even if it has not been reach yet.
        virtual void resetAlarmsInhibitionTime();

        /// Disable activation or clearing of alarms which are not forced.
        virtual void disableAlarms();

        /// Enable all alarms.
        virtual void enableAlarms();

        /// Terminates all active alarms.
        virtual void terminateAllAlarms();

        /// Forcibly terminates all alarms. This will terminate alarms
        /// regardless of the inhibition time and/or if the alarm is active.
        /// Usually this is used on startup to make sure that not alarms are
        /// active in the AlarmSystem and stayed that way due to a container
        /// crash.
        virtual void forceTerminateAllAlarms();

        /// \return the alam description.  This description is NOT used by the
        /// alarm system.
        virtual std::string getAlarmDescription(const int code);

        /// \return a composite string of all the active alarm descriptions.
        /// This description is NOT used by the alarm system.
        virtual std::string createErrorMessage();

        /// Member that gets called when an alarm is effectively activated.
        virtual void handleActivateAlarm(int code) = 0;

        /// Member that gets called when an alarm is effectively deactivated.
        virtual void handleDeactivateAlarm(int code) = 0;


        protected:
        /// Member that actually sends the alarm to the alarm system and calls
        /// the handler.
        void send(const int code, const bool activate, bool force = false);

        std::vector< int > alarmCodes_m;


        private:
        acsalarm::AlarmSystemInterface* alarmSource_m;
        std::map< int, acsalarm::FaultState* > alarms_m;
        std::map< int, bool > activated_m;
        std::map< int, unsigned int > terminateCommandCounter_m;
        std::map< int, unsigned int > activateCommandCounter_m;
        std::map< int, Control::AlarmInformation > alarmInformationMap_m;
        ACS::Time inhibitionTime_m;
        bool disable_m;
    };
};
#endif
