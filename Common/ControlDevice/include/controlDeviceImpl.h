#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef controlDeviceImpl_h
#define controlDeviceImpl_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who       when        what
// jkern     2005-07-30  created
// rhiriart  2005-09-15  Modified ControlDeviceImpl::getSubDeviceName()
//                       to return a sequence of strings.
//                       Changed subDevice to be a multimap instead of
//                       map.


#include <string>
#include <stack>
#include <ControlDeviceS.h>
#include <baciCharacteristicComponentImpl.h>
// For maci::SmartPtr
#include <acsComponentSmartPtr.h>


namespace maci
{
    class ContainerServices;
};


// This class is a base class for all control devices.
namespace Control
{
    template< class componentInterfaceType, class acscomponentImpl >
        class ControlDeviceImpl: public acscomponentImpl,
            public virtual componentInterfaceType
    {
        public:
        /// Standard constructor for Control Devices.
        ControlDeviceImpl(const ACE_CString& name,
            maci::ContainerServices* containerServices);

        /// Destructor for Control Devices.
        virtual ~ControlDeviceImpl();

        /// See the documentation in the IDL file for a description of this
        /// function.
        /// \exception ControlDeviceExceptions::IllegalConfigurationEx)
        virtual void createSubdevices(
            const Control::DeviceConfig& configuration,
            const char* parentName);

        /// See the documentation in the IDL file for a description of this
        /// function.
        virtual void releaseSubdevices();

        /// See the documentation in the IDL file for a description of this
        /// function.
        virtual DeviceNameList* getSubdeviceList();

        /// See the documentation in the IDL file for a description of this
        /// function.
        /// \exception ControlExceptions::IllegalParameterErrorEx;
        virtual char* getSubdeviceName(const char* refName);

        /// Try to retrieve the reference for the \ref lruType component which
        /// has to be a member of the \ref subdevice_m container..
        /// \param lruType will contain an LRU type name like "WCA6".
        /// \return A valid reference for the sub device will be returned.
        /// \exception ControlExceptions::INACTErrorExImpl is thrown if the
        /// LRU cannot be found in the \ref subdevice_m container or if a check
        /// of it against CORBA::is_nil() returned true or if the reference
        /// cannot be narrowed.
        template< typename SubdeviceReference >
        SubdeviceReference* getSubdeviceReference(const std::string& lruType);

        ///
        virtual char* getDeviceReferenceName();

        ///
        void setDeviceReferenceName(const DeviceConfig& config);

        /// See the documentation in the IDL file for a description of this
        /// function.
        virtual CORBA::Boolean inErrorState();

        /// See the documentation in the IDL file for a description of this
        /// function.
        virtual char* getErrorMessage();

        /// ACS life cycle method
        /// Overwrites the cleanUp parent method to release the references to the
        /// subdevices before calling the class destructor.
        virtual void cleanUp();

        virtual void setSubdeviceError(const char* subdeviceName) = 0;

        virtual void clearSubdeviceError(const char* subdeviceName) = 0;

        /// Deduce the antenna name from the component name.
        /// Given a component name, of the form
        /// "CONTROL/<ANTENNA-NAME>/<DEVICE-TYPE>", this function returns the
        /// <ANTENNA-NAME>. This function searches for the "/" characters and
        /// returns the string between the first and second occurrence of these
        /// characters. If the supplied string is not of this form this function
        /// will return "Unknown".
        static std::string componentToAntennaName(const std::string& compName);


        protected:
        /// Returns the name of the alarm system family. The family is something
        /// like:
        /// - "ML" if the device itself is
        ///   "CONTROL/CentalLO/ML".
        /// - "CRD" if the device itself is
        ///   "CONTROL/CentralLO/PhotonicReference3/CRD".
        /// - "DTXBbPr2" if the device itself is
        ///   "CONTROL/DV01/DTXBbPr2".
        /// - "WCA7" if the device itself is
        ///   "CONTROL/DA41/FrontEnd/WCA7".
        /// \param baseName: the name of the base device like DTX. This is
        /// necessary in order to firgure out the proper instance name, e.g.
        /// DTXBbPr3.
        /// \return Name of the alarm system family.
        const std::string getAlarmFamilyName(const std::string& baseName);

        /// Returns the name of the alarm system member. The member is something
        /// like:
        /// - "CentralLO" if the device itself is
        ///   "CONTROL/CentalLO/ML".
        /// - "PhotonicReference3" if the device itself is
        ///   "CONTROL/CentralLO/PhotonicReference3/CRD".
        /// - "DV01" if the device itself is
        ///   "CONTROL/DV01/LORR".
        /// - "DA41" if the device itself is
        ///   "CONTROL/DA41/FrontEnd/WCA7".
        /// \return Name of the alarm system member.
        const std::string getAlarmMemberName();

        /// This function acquires a non-sticky reference to the parent
        /// and pushes it onto the parrent stack.
        ///
        /// \exception: maciErrType::CannotGetComponentEx if the component
        /// reference cannot be acquired
        /// \exception: IllegalParameterErrorEx: If the specified name is null
        /// or has zero length.
        virtual void setParent(const char* parentName);

        /// This method returns a reference to the CORBA proxy for the parent
        /// if the parentStack is empty then a nil reference is returned.
        maci::SmartPtr< Control::ControlDevice > getParentReference();

        /// This function pops the top of the parent stack. If the stack is
        /// empty, nothing is done.
        virtual void releaseParent();

        /// This stack holds the full name of the parent component, only needs
        /// to be directly manipulated occasionally.
        std::stack< maci::SmartPtr< Control::ControlDevice > > parentStack_m;

        /// This method returns a reference to the CORBA proxy for the parent if
        /// the parentStack is empty then a nil reference is returned.
        ///
        /// ToDo: Change the return type to a smart pointer when ACS makes them
        /// available
        ///
        /// Note: You _must_ call releaseParentReference when you have completed
        ///       your use of the pointer.  Do not hold this reference open any
        ///       longer than necessary
        //virtual Control::ControlDevice_ptr getParentReference();
        //virtual void releaseParentReference();

        /// \exception ControlDeviceExceptions::SubstateExceptionExImpl
        virtual void setError(const std::string& errorDescription);

        /// \exception ControlDeviceExceptions::SubstateExceptionExImpl
        virtual void clearError();

        /// Substate mapping
        bool inErrorState_m;

        /// std::string representation of the current error.  Empty if no error
        /// is present.
        std::string errorMessage_m;

        /// This map associates a name (like LORR) with a object reference. It may
        /// contain null object references. Only parent components have an data in
        /// this map.
        std::map< std::string, Control::ControlDevice_ptr > subdevice_m;


        private:
        std::string m_deviceReferenceName;

        /// Try to find an LRU in the \ref subdevice_m container and return an
        /// access iterator.  Called by
        /// \ref getSubdeviceReference(const std::string& lruType).
        /// \return access iterator to the LRU component from the subdevice
        /// container.
        /// \exception ControlExceptions::INACTErrorExImpl is thrown if there
        /// is no such LRU or if the reference test against CORBA::is_nil()
        /// returns true.
        std::map< std::string, Control::ControlDevice_ptr >::const_iterator
            findLruTypeAndCheckIfNil(const std::string& lruType);

        /// This function gets a static component reference to the component
        /// specified by the configuration and stores this reference in the
        /// subdevice_m data member (along with the reference name). If the
        /// component cannot be instanciated a CORBA::nil will be inserted in the
        /// map
        ///
        /// \exception ControlDeviceExceptions::IllegalConfigurationExImpl
        void getSubdeviceReference(const Control::DeviceConfig& configuration);
    };

    typedef Control::ControlDeviceImpl< POA_Control::ControlDevice,
        baci::CharacteristicComponentImpl > CharacteristicControlDeviceImpl;

/*   typedef ControlDeviceImpl<acscomponent::ACSComponentImpl, */
/*                             POA_Control::ComponentControlDevice>  */
/*     ComponentControlDeviceImpl;  */
}
#include <controlDeviceImplTemplateMethods.i>
#endif
