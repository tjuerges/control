#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$
#------------------------------------------------------------------------------

'''
ControlDevice PyU class
'''
import unittest
from PyUCommonTestCases import *
import Control
from ControlExceptions import IllegalParameterErrorEx
from ControlDeviceExceptions import IllegalConfigurationEx

class TestCase(PyUCommonTestCase):
    def __init__(self, methodName='runTest',
                 componentName="CONTROL/TEST_HWCONTROLLER"):

        self.hwcstates = {
            'Shutdown'   : Control.HardwareController.ControllerState._item(0),
            'Operational': Control.HardwareController.ControllerState._item(1),
            'Degraded'   : Control.HardwareController.ControllerState._item(2)
                          }
        self.name=componentName        
        PyUCommonTestCase.__init__(self,methodName,self.name)

    def check_no_subdevices(self):
        '''
        '''
        cd_config = Control.DeviceConfig("TEST_HWCONTROLLER",
                                               [])
        self.configuration = cd_config
        self.reference.createSubdevices(self.configuration,"")
        self.assertEqual(self.reference.getSubdeviceList(),[],"Subdevice list is not empty")
        self.assertEqual(self.reference.getState(),self.hwcstates['Shutdown'],
                         "Incorrect initial state. Expected Shutdown, received "+
                         str(self.reference.getState()))
        self.reference.controllerOperational()
        self.assertEqual(self.reference.getState(),self.hwcstates['Operational'],
                         "Incorrect final state. Expected Operational, received "+
                         str(self.reference.getState()))
        self.reference.controllerShutdown()
        self.assertEqual(self.reference.getState(),self.hwcstates['Shutdown'],
                         "Incorrect final state. Expected Shutdown, received "+
                         str(self.reference.getState()))       
        self.reference.releaseSubdevices()
        self.assertEqual(self.reference.getSubdeviceList(),[],"Subdevice list is not empty")

    def check_bogus_subdevice(self):
        '''
        '''
        cd1_config = Control.DeviceConfig("BAD_SUBDEVICE",
                                                [])
        cd_config = Control.DeviceConfig("TEST_HWCONTROLLER",
                                               [cd1_config])
        self.configuration = cd_config
        self.reference.createSubdevices(self.configuration,"")

        subdevices = self.reference.getSubdeviceList()
        self.assertNotEqual(subdevices,[],"Subdevice list is empty")
        self.assertEqual(len(subdevices),1,
                         "Incorrect number of subdevices. Expected 1, received "+str(len(subdevices)))
        self.assertEqual(subdevices[0].ReferenceName,
                         "BAD_SUBDEVICE","Incorrect subdevice name. Expected BAD_SUBDEVICE, received "+subdevices[0].ReferenceName)
        self.assertEqual(subdevices[0].FullName,
                         "","Incorrect subdevice full name. Expected empty string, received "+subdevices[0].FullName)
        subdevice = self.reference.getSubdeviceName("BAD_SUBDEVICE")
        self.assertEqual(subdevice,"","Incorrect subdevice full name. It should be empty for a bogus subdevice")
        self.assertEqual(self.reference.getState(),self.hwcstates['Shutdown'],
                         "Incorrect initial state. Expected Shutdown, received "+
                         str(self.reference.getState()))
        self.reference.controllerOperational()
        self.assertEqual(self.reference.getState(),self.hwcstates['Degraded'],
                         "Incorrect final state. Expected Operational, received "+
                         str(self.reference.getState()))
        self.reference.controllerShutdown()
        self.assertEqual(self.reference.getState(),self.hwcstates['Shutdown'],
                         "Incorrect final state. Expected Shutdown, received "+
                         str(self.reference.getState()))       
        self.reference.releaseSubdevices()
        self.assertEqual(self.reference.getSubdeviceList(),[],"Subdevice list is not empty")

    def check_one_subdevice(self):
        '''
        '''
        cd1_config = Control.DeviceConfig("HWSUBDEVICE1",
                                                [])
        cd_config = Control.DeviceConfig("TEST_HWCONTROLLER",
                                               [cd1_config])
        
        self.configuration = cd_config
        self.reference.createSubdevices(self.configuration,"")
        subdevices = self.reference.getSubdeviceList()
        self.assertNotEqual(subdevices,[],"Subdevice list is empty")
        self.assertEqual(len(subdevices),1,
                         "Incorrect number of subdevices. Expected 1, received "+
                         str(len(subdevices)))
        self.assertEqual(self.reference.getState(),self.hwcstates['Shutdown'],
                         "Incorrect initial state. Expected Shutdown, received "+
                         str(self.reference.getState()))
        self.reference.controllerOperational()
        self.assertEqual(self.reference.getState(),self.hwcstates['Operational'],
                         "Incorrect final state. Expected Operational, received "+
                         str(self.reference.getState()))
        self.reference.controllerShutdown()
        self.assertEqual(self.reference.getState(),self.hwcstates['Shutdown'],
                         "Incorrect final state. Expected Shutdown, received "+
                         str(self.reference.getState()))       
        self.reference.releaseSubdevices()
        self.assertEqual(self.reference.getSubdeviceList(),[],"Subdevice list is not empty")
        
    def check_two_subdevices(self):
        '''
        '''
        cd1_config = Control.DeviceConfig("HWSUBDEVICE1",
                                                [])
        cd2_config = Control.DeviceConfig("HWSUBDEVICE2",
                                                [])
        cd_config = Control.DeviceConfig("TEST_HWCONTROLLER",
                                               [cd1_config,cd2_config])
        
        self.configuration = cd_config

        self.reference.createSubdevices(self.configuration,"")
        subdevices = self.reference.getSubdeviceList()
        self.assertNotEqual(subdevices,[],"Subdevice list is empty")
        self.assertEqual(len(subdevices),2,
                         "Incorrect number of subdevices. Expected 2, received "+
                         str(len(subdevices)))
        self.assertEqual(self.reference.getState(),self.hwcstates['Shutdown'],
                         "Incorrect initial state. Expected Shutdown, received "+
                         str(self.reference.getState()))
        self.reference.controllerOperational()
        self.assertEqual(self.reference.getState(),self.hwcstates['Operational'],
                         "Incorrect final state. Expected Operational, received "+
                         str(self.reference.getState()))
        self.reference.controllerShutdown()
        self.assertEqual(self.reference.getState(),self.hwcstates['Shutdown'],
                         "Incorrect final state. Expected Shutdown, received "+
                         str(self.reference.getState()))       
        self.reference.releaseSubdevices()
        self.assertEqual(self.reference.getSubdeviceList(),[],"Subdevice list is not empty")
        
    def test_subdevices(self):
        '''
        '''
        self.check_reference()

        # 0 subdevice
        self.check_no_subdevices()
        # bogus subdevice
        self.check_bogus_subdevice()
        # 1 subdevice
        self.check_one_subdevice()
        # 2 subdevices
        self.check_two_subdevices()

if __name__ == '__main__':
    unittest.main()            
