
#include<controlAlarmSender.h>
#include <maciSimpleClient.h>

using Control::AlarmSender;
using Control::AlarmInformation;

int main(int argc, char* argv[])
{
   ///---- begin standalone specific code --------//
   maci::SimpleClient m_client;
   m_client.init(argc,argv);
   m_client.login();
   ACSAlarmSystemInterfaceFactory::init(ACSAlarmSystemInterfaceFactory::getManager());
   ///---- end standalone specific code --------//
   
   //Simple example of the usage as follows.
   {
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "--------------TEST 1 --------------------"));
       //Create an alarm wrapper
       AlarmSender as;

       //Create a vector that has the alarm information.
       std::vector<AlarmInformation> aivector;
       for(int i =0; i< 10; i++) {
           AlarmInformation ai;
           //Alarm Code. This MUST be set.
           ai.alarmCode= i;
           //Alarm termination Count. This is how many times an alarm must be deativated before it really clears. Default 1.
           //ai.alarmTerminateCount = 3;
           //Alarm description. Internal description of the alarm. Default ""
           //ai.alarmDescription = 3;
           aivector.push_back(ai);
       }   
       //Initialize the alarms with:
       // FaultFamily
       // FaultMember
       // Vector with the AlarmInformation.
       as.initializeAlarms("TestAlarmSender", "TAS", aivector);
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "activate Alarms 1"));
       for(int i =0; i< 10; i++) {
           as.activateAlarm(i);
       }
    
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Deactivate Alarms 1"));
       for(int i =0; i< 10; i++) {
           as.deactivateAlarm(i);
       }
    
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "inhibitAlarms"));
       //Inhibit alarms
       as.inhibitAlarms(3000000000ULL);
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "activate Alarms 1"));
       for(int i =0; i< 10; i++) {
           //These alarms wont be trigered.
           as.activateAlarm(i);
       }
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Reset inhibitAlarms"));
       //Reenable alarms before inhibition time expires.
       as.resetAlarmsInhibitionTime();
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "activate Alarms 1"));
       for(int i =0; i< 10; i++) {
           as.activateAlarm(i);
       }
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "activate Alarms 2"));
       for(int i =0; i< 10; i++) {
           as.activateAlarm(i);
       }
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "activate Alarms 3"));
       for(int i =0; i< 10; i++) {
           as.activateAlarm(i);
       }
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "activate Alarms 4"));
       for(int i =0; i< 10; i++) {
           as.activateAlarm(i);
       }
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Deactivate Alarms"));
       for(int i =0; i< 10; i++) {
           as.deactivateAlarm(i);
       }
   }

   {
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "--------------TEST 2 --------------------"));
       AlarmSender as;
       std::vector<AlarmInformation> aivector;
       for(int i =0; i< 10; i++) {
           AlarmInformation ai;
           ai.alarmCode = i;
           ai.alarmTerminateCount = 3;
           aivector.push_back(ai);
       }   
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Activating Alarms"));
       as.initializeAlarms("TestAlarmSender2", "TAS2", aivector);
       for(int i =0; i< 10; i++) {
           as.activateAlarm(i);
       }
    
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Deativate Alarms 1"));
       for(int i =0; i< 10; i++) {
           as.deactivateAlarm(i);
       }
    
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Deativate Alarms 2"));
       for(int i =0; i< 10; i++) {
           as.deactivateAlarm(i);
       }

       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Deativate Alarms 3"));
       for(int i =0; i< 10; i++) {
           as.deactivateAlarm(i);
       }
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Deativate Alarms 4"));
       for(int i =0; i< 10; i++) {
           as.deactivateAlarm(i);
       }
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Deativate Alarms 5"));
       for(int i =0; i< 10; i++) {
           as.deactivateAlarm(i);
       }
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Deativate Alarms 6"));
       for(int i =0; i< 10; i++) {
           as.deactivateAlarm(i);
       }
   }

   {
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "--------------TEST 3 --------------------"));
       AlarmSender as;
       std::vector<AlarmInformation> aivector;
       for(int i =0; i< 10; i++) {
           AlarmInformation ai;
           ai.alarmCode = i;
           ai.alarmActivateCount = 3;
           aivector.push_back(ai);
       }   
       as.initializeAlarms("TestAlarmSender3", "TAS3", aivector);
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Activating Alarms 1"));
       for(int i =0; i< 10; i++) {
           as.activateAlarm(i);
       }
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Activating Alarms 2"));
       for(int i =0; i< 10; i++) {
           as.activateAlarm(i);
       }

       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Activating Alarms 3"));
       for(int i =0; i< 10; i++) {
           as.activateAlarm(i);
       }

       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Activating Alarms 4"));
       for(int i =0; i< 10; i++) {
           as.activateAlarm(i);
       }

       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Activating Alarms 5"));
       for(int i =0; i< 10; i++) {
           as.activateAlarm(i);
       }
    
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Deativate Alarms 1"));
       for(int i =0; i< 10; i++) {
           as.deactivateAlarm(i);
       }
    
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Deativate Alarms 2"));
       for(int i =0; i< 10; i++) {
           as.deactivateAlarm(i);
       }
   }
   {
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "--------------TEST 4 --------------------"));
       AlarmSender as;
       std::vector<AlarmInformation> aivector;
       for(int i =0; i< 10; i++) {
           AlarmInformation ai;
           ai.alarmCode = i;
           aivector.push_back(ai);
       }   
       as.initializeAlarms("TestAlarmSender4", "TAS4", aivector);
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Activating Alarms 1"));
       for(int i =0; i< 10; i++) {
           as.activateAlarm(i);
       }
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Deactivating Alarms 1"));
       for(int i =0; i< 10; i++) {
           as.deactivateAlarm(i);
       }
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Deactivating Alarms 2"));
       for(int i =0; i< 10; i++) {
           as.deactivateAlarm(i);
       }
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "ForceTermanate all alarms"));
       as.forceTerminateAllAlarms();

   }
   { //TEST the copy constructor.
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "--------------TEST 5 --------------------"));
       AlarmSender as;
       std::vector<AlarmInformation> aivector;
       for(int i =0; i< 10; i++) {
           AlarmInformation ai;
           ai.alarmCode = i;
           aivector.push_back(ai);
       }   
       as.initializeAlarms("TestAlarmSender4", "TAS4", aivector);
       AlarmSender as2(as);
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Activating Alarms 1"));
       for(int i =0; i< 10; i++) {
           as2.activateAlarm(i);
       }
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Deactivating Alarms 1"));
       for(int i =0; i< 10; i++) {
           as2.deactivateAlarm(i);
       }
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Deactivating Alarms 2"));
       for(int i =0; i< 10; i++) {
           as2.deactivateAlarm(i);
       }
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "ForceTermanate all alarms"));
       as2.forceTerminateAllAlarms();

   }
   { //TEST assigment contructor
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "--------------TEST 6 --------------------"));
       AlarmSender as;
       std::vector<AlarmInformation> aivector;
       for(int i =0; i< 10; i++) {
           AlarmInformation ai;
           ai.alarmCode = i;
           aivector.push_back(ai);
       }   
       as.initializeAlarms("TestAlarmSender4", "TAS4", aivector);
       AlarmSender as2 = as;
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Activating Alarms 1"));
       for(int i =0; i< 10; i++) {
           as2.activateAlarm(i);
       }
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Deactivating Alarms 1"));
       for(int i =0; i< 10; i++) {
           as2.deactivateAlarm(i);
       }
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Deactivating Alarms 2"));
       for(int i =0; i< 10; i++) {
           as2.deactivateAlarm(i);
       }
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "ForceTermanate all alarms"));
       as2.forceTerminateAllAlarms();

   }
   { //TEST assigment contructor
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "--------------TEST 7 --------------------"));
       AlarmSender as;
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Activating Alarms 1 non innitialize class"));
       for(int i =0; i< 10; i++) {
           as.activateAlarm(i);
       }
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Deactivating Alarms 1 non innitialize class"));
       for(int i =0; i< 10; i++) {
           as.deactivateAlarm(i);
       }
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "ForceTermanate all alarms non innitialize class"));
       as.forceTerminateAllAlarms();

   }
   { //TEST copy reset an initialize
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "--------------TEST 8 --------------------"));
       AlarmSender as;
       std::vector<AlarmInformation> aivector;
       for(int i =0; i< 10; i++) {
           AlarmInformation ai;
           ai.alarmCode = i;
           aivector.push_back(ai);
       }   
       as.initializeAlarms("TestAlarmSender4", "TAS4", aivector);
       AlarmSender as2 = as;
       as2.resetAlarms();
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Activating Alarms 1"));
       for(int i =0; i< 10; i++) {
           as2.activateAlarm(i);
       }
       as2.initializeAlarms("TestAlarmSender4", "TAS4", aivector);
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Activate Alarms 1"));
       for(int i =0; i< 10; i++) {
           as2.activateAlarm(i);
       }
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "Deactivating Alarms 2"));
       for(int i =0; i< 10; i++) {
           as2.deactivateAlarm(i);
       }
       ACS_LOG(LM_SOURCE_INFO, "Main", (LM_ALERT, "ForceTermanate all alarms"));
       as2.forceTerminateAllAlarms();

   }
   


   m_client.logout();
   m_client.disconnect();   

   
}
