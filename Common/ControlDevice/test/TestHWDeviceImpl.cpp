// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2007, 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <TestHWDeviceImpl.h>

TestHWDeviceImpl::TestHWDeviceImpl(const ACE_CString& name,
                                   maci::ContainerServices* cs)
    : Control::HardwareDeviceImpl(name, cs)
{
  ACS_TRACE("TestHWDeviceImpl::TestHWDeviceImpl");
}

TestHWDeviceImpl::~TestHWDeviceImpl() {
  ACS_TRACE("TestHWDeviceImpl::~TestHWDeviceImpl");
}

/* ----------------------- [ HW Lifecycle Interface ] ------------------ */
void TestHWDeviceImpl::hwStartAction(){
  ACS_TRACE("TestHWDeviceImpl::hwStartAction");
}

void TestHWDeviceImpl::hwConfigureAction(){
  ACS_TRACE("TestHWDeviceImpl::hwConfigureAction");
}

void TestHWDeviceImpl::hwInitializeAction(){
  ACS_TRACE("TestHWDeviceImpl::hwInitializeAction");
}

void TestHWDeviceImpl::hwOperationalAction(){
  ACS_TRACE("TestHWDeviceImpl::hwOperationalAction");
}

void TestHWDeviceImpl::hwStopAction(){
  ACS_TRACE("TestHWDeviceImpl::hwStopAction");
}

/////////////////////////////////////////////////
// MACI DLL support functions
/////////////////////////////////////////////////
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(TestHWDeviceImpl)
