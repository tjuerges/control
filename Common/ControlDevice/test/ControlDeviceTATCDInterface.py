#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$
#------------------------------------------------------------------------------

'''
ControlDevice PyU class
'''
import unittest
from PyUCommonTestCases import *
import Control
from ControlExceptions import IllegalParameterErrorEx
from ControlDeviceExceptions import IllegalConfigurationEx

class TestCase(PyUCommonTestCase):
    def __init__(self, methodName='runTest',
                 componentName="CONTROL/TEST_CONTROLDEVICE"):

        self.name=componentName        
        cd_config = Control.DeviceConfig("TEST_CONTROLDEVICE",
                                               [])
        self.configuration = cd_config
        PyUCommonTestCase.__init__(self,methodName,self.name,self.configuration)

    def check_no_subdevices(self):
        '''
        '''
        cd_config = Control.DeviceConfig("TEST_CONTROLDEVICE",
                                               [])
        self.configuration = cd_config
        self.reference.createSubdevices(self.configuration,"")

        self.assertEqual(self.reference.getSubdeviceList(),[],"Subdevice list is not empty")
        self.referenceName = self.reference.getDeviceReferenceName()
        self.assertEqual(self.referenceName,"TEST_CONTROLDEVICE","Incorrect device reference name. Got "+self.referenceName)
        try:
            self.reference.getSubdeviceName("SUBDEVICE1")
        except IllegalParameterErrorEx:
            pass
        else:
            self.fail("IllegalParameterErrorEx exception was not thrown")
        self.assertEqual(self.reference.inErrorState(),False,"Incorrect error state flag. It should be False")
        error_message = self.reference.getErrorMessage()
        self.assertEqual(error_message,"","Error message should be empty. Received "+error_message)
        self.reference.releaseSubdevices()
        self.assertEqual(self.reference.getSubdeviceList(),[],"Subdevice list is not empty")

    def check_bogus_subdevice(self):
        '''
        '''
        cd1_config = Control.DeviceConfig("BAD_SUBDEVICE",
                                                [])
        cd_config = Control.DeviceConfig("TEST_CONTROLDEVICE",
                                               [cd1_config])
        self.configuration = cd_config
        self.reference.createSubdevices(self.configuration,"")
        self.referenceName = self.reference.getDeviceReferenceName()
        self.assertEqual(self.referenceName,"TEST_CONTROLDEVICE","Incorrect device reference name. Got "+self.referenceName)

        subdevices = self.reference.getSubdeviceList()
        self.assertNotEqual(subdevices,[],"Subdevice list is empty")
        self.assertEqual(len(subdevices),1,
                         "Incorrect number of subdevices. Expected 1, received "+str(len(subdevices)))
        self.assertEqual(subdevices[0].ReferenceName,
                         "BAD_SUBDEVICE","Incorrect subdevice name. Expected BAD_SUBDEVICE, received "+subdevices[0].ReferenceName)
        self.assertEqual(subdevices[0].FullName,
                         "","Incorrect subdevice full name. Expected empty string, received "+subdevices[0].FullName)
        subdevice = self.reference.getSubdeviceName("BAD_SUBDEVICE")
        self.assertEqual(subdevice,"","Incorrect subdevice full name. It should be empty for a bogus subdevice")

        self.reference.releaseSubdevices()
        self.assertEqual(self.reference.getSubdeviceList(),[],"Subdevice list is not empty")

    def check_one_subdevice(self):
        '''
        '''
        cd1_config = Control.DeviceConfig("SUBDEVICE1",
                                                [])
        cd_config = Control.DeviceConfig("TEST_CONTROLDEVICE",
                                               [cd1_config])
        
        self.configuration = cd_config
        self.reference.createSubdevices(self.configuration,"")
        self.referenceName = self.reference.getDeviceReferenceName()
        self.assertEqual(self.referenceName,"TEST_CONTROLDEVICE","Incorrect device reference name. Got "+self.referenceName)
        subdevices = self.reference.getSubdeviceList()
        self.assertNotEqual(subdevices,[],"Subdevice list is empty")
        self.assertEqual(len(subdevices),1,
                         "Incorrect number of subdevices. Expected 1, received "+
                         str(len(subdevices)))
        self.assertEqual(subdevices[0].ReferenceName,"SUBDEVICE1",
                         "Incorrect subdevice name. Expected SUBDEVICE1, received "+
                         subdevices[0].ReferenceName)
        self.assertEqual(subdevices[0].FullName,
                         "CONTROL/TEST_CONTROLDEVICE/SUBDEVICE1",
                         "Incorrect full subdevice name. Expected CONTROL/TEST_CONTROLDEVICE/SUBDEVICE1, received "+
                         subdevices[0].FullName)
        subdevice = self.reference.getSubdeviceName("SUBDEVICE1")
        self.assertEqual(subdevice,
                         "CONTROL/TEST_CONTROLDEVICE/SUBDEVICE1",
                         "Incorrect subdevice name. Expected CONTROL/TEST_CONTROLDEVICE/SUBDEVICE1, received "+
                         subdevice)       
        self.reference.releaseSubdevices()
        self.assertEqual(self.reference.getSubdeviceList(),[],"Subdevice list is not empty")
        
    def check_two_subdevices(self):
        '''
        '''
        cd1_config = Control.DeviceConfig("SUBDEVICE1",
                                                [])
        cd2_config = Control.DeviceConfig("SUBDEVICE2",
                                                [])
        cd_config = Control.DeviceConfig("TEST_CONTROLDEVICE",
                                               [cd1_config,cd2_config])
        
        self.configuration = cd_config

        self.reference.createSubdevices(self.configuration,"")
        self.referenceName = self.reference.getDeviceReferenceName()
        self.assertEqual(self.referenceName,"TEST_CONTROLDEVICE","Incorrect device reference name. Got "+self.referenceName)
        subdevices = self.reference.getSubdeviceList()
        self.assertNotEqual(subdevices,[],"Subdevice list is empty")
        self.assertEqual(len(subdevices),2,
                         "Incorrect number of subdevices. Expected 2, received "+
                         str(len(subdevices)))
        self.assertEqual(subdevices[0].ReferenceName,"SUBDEVICE1",
                         "Incorrect subdevice name. Expected SUBDEVICE1, received "+
                         subdevices[0].ReferenceName)
        self.assertEqual(subdevices[0].FullName,
                         "CONTROL/TEST_CONTROLDEVICE/SUBDEVICE1",
                         "Incorrect subdevice full name. Expected CONTROL/TEST_CONTROLDEVICE/SUBDEVICE1, received "+
                         subdevices[0].FullName)
        self.assertEqual(subdevices[1].ReferenceName,"SUBDEVICE2",
                         "Incorrect subdevice name. Expected SUBDEVICE2, received "+
                         subdevices[1].ReferenceName)
        self.assertEqual(subdevices[1].FullName,
                         "CONTROL/TEST_CONTROLDEVICE/SUBDEVICE2",
                         "Incorrect subdevice full name: "+
                         subdevices[1].FullName)
        
        subdevice = self.reference.getSubdeviceName("SUBDEVICE1")
        self.assertEqual(subdevice,
                         "CONTROL/TEST_CONTROLDEVICE/SUBDEVICE1",
                         "Incorrect subdevice full name. Expected CONTROL/TEST_CONTROLDEVICE/SUBDEVICE1, received "+
                         subdevice)
        subdevice = self.reference.getSubdeviceName("SUBDEVICE2")
        self.assertEqual(subdevice,
                         "CONTROL/TEST_CONTROLDEVICE/SUBDEVICE2",
                         "Incorrect subdevice full name. Expected CONTROL/TEST_CONTROLDEVICE/SUBDEVICE1, received "+
                         subdevice)
        self.reference.releaseSubdevices()
        self.assertEqual(self.reference.getSubdeviceList(),[],"Subdevice list is not empty")
        
    def check_duplicated_keys(self):
        '''
        '''
        cd1_config = Control.DeviceConfig("SUBDEVICE1",
                                                [])
        cd2_config = Control.DeviceConfig("SUBDEVICE1",
                                                [])
        cd_config = Control.DeviceConfig("TEST_CONTROLDEVICE",
                                               [cd1_config, cd2_config])        
        self.configuration = cd_config

        try:
            self.reference.createSubdevices(self.configuration,"")
        except IllegalConfigurationEx:
            pass
        else:
            self.fail("IllegalConfigurationEx exception was not thrown")

        subdevices = self.reference.getSubdeviceList()
        self.assertNotEqual(subdevices,[],"Subdevice list is empty")
        self.assertEqual(len(subdevices),1,
                         "Incorrect number of subdevices. Expected 1, received "+
                         str(len(subdevices)))
        self.assertEqual(subdevices[0].ReferenceName,"SUBDEVICE1",
                         "Incorrect subdevice name. Expected SUBDEVICE1, received "+
                         subdevices[0].ReferenceName)
        self.assertEqual(subdevices[0].FullName,
                         "CONTROL/TEST_CONTROLDEVICE/SUBDEVICE1",
                         "Incorrect full subdevice name. Expected CONTROL/TEST_CONTROLDEVICE/SUBDEVICE1, received "+
                         subdevices[0].FullName)
        subdevice = self.reference.getSubdeviceName("SUBDEVICE1")
        self.assertEqual(subdevice,
                         "CONTROL/TEST_CONTROLDEVICE/SUBDEVICE1",
                         "Incorrect subdevice name. Expected CONTROL/TEST_CONTROLDEVICE/SUBDEVICE1, received "+
                         subdevice)       

        self.reference.releaseSubdevices()
        self.assertEqual(self.reference.getSubdeviceList(),[],"Subdevice list is not empty")
            
    def check_threesubdevices_lastBogus(self):
        '''
        '''
        cd21_config = Control.DeviceConfig("SUBSUBDEVICE11",
                                                [])
        cd22_config = Control.DeviceConfig("SUBSUBDEVICE11",
                                                [])
        cd1_config = Control.DeviceConfig("SUBDEVICE1",
                                                [cd21_config,cd22_config])
        cd_config = Control.DeviceConfig("TEST_CONTROLDEVICE",
                                               [cd1_config])        
        self.configuration = cd_config

        try:
            self.reference.createSubdevices(self.configuration,"")
        except IllegalConfigurationEx:
            pass
        else:
            self.fail("IllegalConfigurationEx exception was not thrown")

        subdevices = self.reference.getSubdeviceList()
        self.assertNotEqual(subdevices,[],"Subdevice list is empty")
        self.assertEqual(len(subdevices),1,
                         "Incorrect number of subdevices. Expected 1, received "+
                         str(len(subdevices)))
        self.assertEqual(subdevices[0].ReferenceName,"SUBDEVICE1",
                         "Incorrect subdevice name. Expected SUBDEVICE1, received "+
                         subdevices[0].ReferenceName)
        self.assertEqual(subdevices[0].FullName,
                         "CONTROL/TEST_CONTROLDEVICE/SUBDEVICE1",
                         "Incorrect full subdevice name. Expected CONTROL/TEST_CONTROLDEVICE/SUBDEVICE1, received "+
                         subdevices[0].FullName)
        subdevice = self.reference.getSubdeviceName("SUBDEVICE1")
        self.assertEqual(subdevice,
                         "CONTROL/TEST_CONTROLDEVICE/SUBDEVICE1",
                         "Incorrect subdevice name. Expected CONTROL/TEST_CONTROLDEVICE/SUBDEVICE1, received "+
                         subdevice)       

        self.reference.releaseSubdevices()
        self.assertEqual(self.reference.getSubdeviceList(),[],"Subdevice list is not empty")
            
    def test_subdevices(self):
        '''
        '''
        self.check_reference()

        # 0 subdevice
        self.check_no_subdevices()
        # bogus subdevice
        self.check_bogus_subdevice()
        # 1 subdevice
        self.check_one_subdevice()
        # 2 subdevices
        self.check_two_subdevices()
        # 2 subdevices, duplicated keys
        self.check_duplicated_keys()
        # 3 subdevices
        self.check_threesubdevices_lastBogus()
        
if __name__ == '__main__':
    unittest.main()            
