//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// jkern  2006-06-06  created
//


#include <hardwareControllerImpl.h>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <HardwareDeviceC.h>
// for ControlDeviceExceptions::HwLifecycleEx
#include <ControlDeviceExceptions.h>


Control::HardwareControllerImpl::HardwareControllerImpl(const ACE_CString& name,
    maci::ContainerServices* containerServices):
    Control::CharacteristicControlDeviceImpl(name, containerServices),
    currentState_m(Control::HardwareController::Shutdown)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    badSubdeviceList_m.clear();
}

Control::HardwareControllerImpl::~HardwareControllerImpl()
{
}

void Control::HardwareControllerImpl::cleanUp()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    controllerShutdown();
}

bool Control::HardwareControllerImpl::setDeviceOperational(std::map<
    std::string, Control::ControlDevice_ptr >::iterator& iter)
{
    bool ret(true);

    Control::HardwareDevice_var hwDevice(
        Control::HardwareDevice::_narrow((*iter).second));

    if(CORBA::is_nil(hwDevice) == false)
    {
        try
        {
            // Check the current state of the subdevice before the
            // transition is tried.  This allows to bring a stopped
            // device back to operational if hardware controller had
            // stopped all subdevices by executing
            // controllerOperational.
            if(hwDevice->getHwState() == Control::HardwareDevice::Stop)
            {
                hwDevice->hwStart();
            }

            if(hwDevice->getHwState() == Control::HardwareDevice::Start)
            {
                hwDevice->hwConfigure();
            }

            if(hwDevice->getHwState()
                == Control::HardwareDevice::Configure)
            {
                hwDevice->hwInitialize();
            }

            if(hwDevice->getHwState()
                == Control::HardwareDevice::Initialize)
            {
                hwDevice->hwOperational();
            }

            if(hwDevice->getHwState()
                == Control::HardwareDevice::Operational)
            {
                hwDevice->monitoringOn();
            }
        }
        catch(ControlDeviceExceptions::HwLifecycleExImpl& ex)
        {
            const std::string detail(ex.getData("Detail").c_str());
            if(detail.empty() == false)
            {
                // Control::ControlDeviceImpl::setError does not compile
                // because it is a template method.
                setError(detail);
            }

            // Add this device to the degraded list and go to degraded
            // state
            setSubdeviceError((*iter).first.c_str());
        }
        catch(ControlDeviceExceptions::HwLifecycleEx& ex)
        {
            // Add this device to the degraded list and go to degraded
            // state
            setSubdeviceError((*iter).first.c_str());
        }
        catch(...)
        {
            // Add this device to the degraded list and go to degraded
            // state.
            setSubdeviceError((*iter).first.c_str());
        }
    }
    else
    {
        ret = false;
    }

    return ret;
}

void Control::HardwareControllerImpl::setParentError()
{
    maci::SmartPtr< Control::ControlDevice > parentRef(getParentReference());
    CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());
    if(badSubdeviceList_m.empty() == false)
    {
        setControllerState(Control::HardwareController::Degraded);
        if(parentRef.isNil() == false)
        {
            parentRef->setSubdeviceError(tmpName.in());
        }
    }
    else
    {
        setControllerState(Control::HardwareController::Operational);
        if(parentRef.isNil() == false)
        {
            parentRef->clearSubdeviceError(tmpName.in());
        }
    }
}

void Control::HardwareControllerImpl::controllerOperational()
{
    // Protect the controller lifecycle methods from being called synchronous.
    ACE_Guard< ACE_Mutex > guard(controllerLifecycleMutex);

    for(std::map< std::string, Control::ControlDevice_ptr >::iterator iter(
        subdevice_m.begin()); iter != subdevice_m.end(); ++iter)
    {
        if(CORBA::is_nil((*iter).second) == true)
        {
            // Add this device to the degraded list and go to degraded state.
            setSubdeviceError((*iter).first.c_str());
        }
        else
        {
            if(setDeviceOperational(iter) == false)
            {
                Control::HardwareController_var hwController(
                    Control::HardwareController::_narrow((*iter).second));
                if(CORBA::is_nil(hwController) == false)
                {
                    try
                    {
                        hwController->controllerOperational();
                    }
                    catch(...)
                    {
                        // Add this device to the degraded list and go to degraded
                        // state
                        setSubdeviceError((*iter).first.c_str());
                    }
                }
            }
        }
    }

    setParentError();
}

void Control::HardwareControllerImpl::controllerShutdown()
{
    // Protect the controller lifecycle methods from being called synchronous.
    ACE_Guard< ACE_Mutex > guard(controllerLifecycleMutex);

    AUTO_TRACE(__PRETTY_FUNCTION__);

    for(std::map< std::string, Control::ControlDevice_ptr >::iterator iter(
        subdevice_m.begin()); iter != subdevice_m.end(); ++iter)
    {
        if(CORBA::is_nil((*iter).second) == false)
        {
            Control::HardwareDevice_var hwDevice(
                Control::HardwareDevice::_narrow((*iter).second));
            if(CORBA::is_nil(hwDevice) == false)
            {
                try
                {
                    hwDevice->hwStop();
                }
                catch(...)
                {
                    // Add this device to the degraded list and go to degraded
                    // state
                    setSubdeviceError((*iter).first.c_str());
                }
            }

            Control::HardwareController_var hwController(
                Control::HardwareController::_narrow((*iter).second));
            if(CORBA::is_nil(hwController) == false)
            {
                try
                {
                    hwController->controllerShutdown();
                }
                catch(...)
                {
                    // Add this device to the degraded list and go to degraded
                    // state
                    setSubdeviceError((*iter).first.c_str());
                }
            }
        }
        else
        {
            // No connection so subdevice, remove it from the error list
            clearSubdeviceError((*iter).first.c_str());
        }
    }

    maci::SmartPtr < Control::ControlDevice > parentRef(getParentReference());
    CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());
    if(badSubdeviceList_m.empty() == false)
    {
        setControllerState(Control::HardwareController::Degraded);
        if(parentRef.isNil() == false)
        {
            parentRef->setSubdeviceError(tmpName.in());
        }
    }
    else
    {
        setControllerState(Control::HardwareController::Shutdown);
        if(parentRef.isNil() == false)
        {
            parentRef->clearSubdeviceError(tmpName.in());
        }
    }
}

Control::HardwareController::ControllerState Control::HardwareControllerImpl::getState()
{
    return currentState_m;
}

void Control::HardwareControllerImpl::setSubdeviceError(
    const char* subdeviceName)
{
    std::string lruName(subdeviceName);
    const std::size_t pos(lruName.rfind("/"));
    if((pos < std::string::npos) && ((pos + 1) < std::string::npos))
    {
        lruName = lruName.substr(pos + 1);
    }

    ACS::ThreadSyncGuard guard(&badDeviceMutex_m);

    // std::vector does not provide a find member, so use the STL find.
    if(std::find(badSubdeviceList_m.begin(), badSubdeviceList_m.end(),
        lruName) != badSubdeviceList_m.end())
    {
        // Device is already in the vector, just return.
        return;
    }

    // Device is not in the vector, add it.
    badSubdeviceList_m.push_back(lruName);

    if(currentState_m == Control::HardwareController::Operational)
    {
        setControllerState(Control::HardwareController::Degraded);
        maci::SmartPtr < Control::ControlDevice > parentRef(
            getParentReference());
        if(parentRef.isNil() == false)
        {
            CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());
            parentRef->setSubdeviceError(tmpName.in());
        }
    }
}

void Control::HardwareControllerImpl::clearSubdeviceError(const char* subdeviceName)
{
    std::string lruName(subdeviceName);
    const std::size_t pos(lruName.rfind("/"));
    if((pos < std::string::npos) && ((pos + 1) < std::string::npos))
    {
        lruName = lruName.substr(pos + 1);
    }

    ACS::ThreadSyncGuard guard(&badDeviceMutex_m);

    // std::vector does not provide a find member, so use the STL find.
    std::vector< std::string >::iterator iter(
        std::find(badSubdeviceList_m.begin(), badSubdeviceList_m.end(),
            lruName));
    if(iter == badSubdeviceList_m.end())
    {
        // Device is not in the vector, just return.
        std::ostringstream msg;
        msg << "Device "
            << lruName
            << " reported clearing error, but no error previously reported.";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    }
    else
    {
        // We found the device remove it
        badSubdeviceList_m.erase(iter);

        if((currentState_m == Control::HardwareController::Degraded)
        && (badSubdeviceList_m.empty() == true))
        {
            setControllerState(Control::HardwareController::Operational);
            maci::SmartPtr < Control::ControlDevice > parentRef(
                getParentReference());
            if(parentRef.isNil() == false)
            {
                CORBA::String_var tmpName(
                    acscomponent::ACSComponentImpl::name());
                parentRef->clearSubdeviceError(tmpName.in());
            }
        }
    }
}

char* Control::HardwareControllerImpl::getErrorMessage()
{
    // This dude will contain all error messages nicely formatted.
    std::string errorMessage;

    // First off add the controller error message if there is one.
    if(inErrorState() == true)
    {
        CORBA::String_var myName(getDeviceReferenceName());
        errorMessage += myName.in();
        errorMessage += ": ";
        // Call the parent class!  Otherwise it will be an endless loop.
        CORBA::String_var myErrorMessage(
            CharacteristicControlDeviceImpl::getErrorMessage());
        errorMessage += myErrorMessage.in();
        errorMessage += "\n";
    }

    // Now add the error messages for each subdevice in the badsubdevices
    // vector.
    for(std::vector< std::string >::const_iterator iter(
        badSubdeviceList_m.begin()); iter != badSubdeviceList_m.end(); ++iter)
    {
        try
        {
            // Get a reference to the bad subdevice.
            Control::ControlDevice_ptr ref(
                getSubdeviceReference< Control::ControlDevice >(*iter));
            // Now build a string that looks like
            // "LRUName: I am feeling sick.\n"
            // This is the LRUName part:
            errorMessage += (*iter);
            errorMessage += ": ";
            // Now add the error message which I retrieve from the subdevice.
            CORBA::String_var tmpErrorMassage(ref->getErrorMessage());
            errorMessage += tmpErrorMassage.in();
            errorMessage += "\n";
        }
        catch(const ControlExceptions::INACTErrorExImpl& ex)
        {
            // Damn.  I could not get a reference to the bad subdevice.  Tell
            // in the error message.
            errorMessage += (*iter);
            errorMessage += ": Could not connect to the device.  This may be "
                "an indicator for serious trouble!\n";
        }
        catch(...)
        {
            // Woah.  This exception should really not be thrown at all.
            errorMessage += (*iter);
            errorMessage += ": Caught an unexpected exception while trying to "
                "connecto to the device.  This may be an indicator for "
                "serious trouble!\n";
        }
    }

    // Done.  Create a CORBA::String_var, copy the string over and return it
    // while transferring ownership to the caller.
    CORBA::String_var ret(errorMessage.c_str());
    return ret._retn();
}

void Control::HardwareControllerImpl::flagData(bool startStop, ACS::Time startTimeStamp,
    const char* componentName, const char* description)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

void Control::HardwareControllerImpl::setControllerState(
    Control::HardwareController::ControllerState newState)
{
    currentState_m = newState;
}

Control::BadResourceSeq* Control::HardwareControllerImpl::getBadSubdevices()
{
    ACS::ThreadSyncGuard guard(&badDeviceMutex_m);

    Control::BadResourceSeq_var badResource(new Control::BadResourceSeq);
    badResource->length(badSubdeviceList_m.size());
    for(unsigned int idx(0U); idx < badSubdeviceList_m.size(); ++idx)
    {
        (*badResource)[idx]
            = CORBA::string_dup(badSubdeviceList_m[idx].c_str());
    }

    return badResource._retn();
}
