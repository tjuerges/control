//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <hardwareDeviceImpl.h>
// For acscomponentImpl::ACSComponentImpl::getContainerServices
#include <acscomponentImpl.h>
// For std::string
#include <string>
// For std::vector
#include <vector>
// For audience logs
#include <sstream>
#include <LogToAudience.h>
// For to_lower
#include <boost/algorithm/string.hpp>
// for ControlExceptions::INACTError
#include <ControlExceptions.h>
// for ControlDeviceExceptions::HwLifecycleEx
#include <ControlDeviceExceptions.h>
// For ACE_Guard
#include <Guard_T.h>


Control::HardwareDeviceImpl::HardwareDeviceImpl(const ACE_CString& name,
    maci::ContainerServices* containerServices):
    Control::CharacteristicControlDeviceImpl(name, containerServices),
    diagnosticModeEnabled(false),
    simulationModeEnabled(false),
    monitoring(false),
    monitorCollector_m(TMCDB::MonitorCollector::_nil()),
    MAX_ERROR_COUNTER(15U),
    ERROR_COUNTER_ALARM_CODE(1U),
    errorCounter(0U),
    isInHwStop(false),
    deviceState_m(Control::HardwareDevice::Undefined),
    alarmSender_m(),
    serialNumber_m("na")
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());
    std::vector< Control::AlarmInformation::AlarmInformation > aivector;
    {
        Control::AlarmInformation::AlarmInformation ai;
        ai.alarmCode = ERROR_COUNTER_ALARM_CODE;
        aivector.push_back(ai);
    }

    alarmSender_m.initializeAlarms("HardwareDevice", tmpName.in(), aivector);
}

Control::HardwareDeviceImpl::~HardwareDeviceImpl()
{
}

void Control::HardwareDeviceImpl::setSerialNumber(const std::string& _deviceID)
{
    //Try and check whether the serialNumber is prefixed with 0x.
    // If yes, strip it off.
    if(boost::algorithm::to_lower_copy(_deviceID.substr(0, 2)) == "0x")
    {
        serialNumber_m = _deviceID.substr(2);
    }
    else
    {
        serialNumber_m = _deviceID;
    }

    CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());

    std::ostringstream msg;
    msg << "SerialNumber for device "
        << tmpName.in()
        << " has been set to 0x"
        << serialNumber_m
        << ".";
    LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
}

char* Control::HardwareDeviceImpl::getSerialNumber()
{
    if((deviceState_m != Control::HardwareDevice::Configure)
    && (deviceState_m != Control::HardwareDevice::Initialize)
    && (deviceState_m != Control::HardwareDevice::Operational)
    && (deviceState_m != Control::HardwareDevice::Simulation)
    && (deviceState_m != Control::HardwareDevice::Diagnostic))
    {
        std::ostringstream message;
        message << "The device has not been configured, i.e. "
            "HardwareDeviceImpl::hwConfigure() has not been called so far. "
            "Therefore a serial number is not available yet. The device is "
            "currently in the "
            << stateToString(deviceState_m)
            << " state.";

        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", message.str());
        ex.log();
        throw ex.getINACTErrorEx();
    }

    return CORBA::string_dup(serialNumber_m.c_str());
}

void Control::HardwareDeviceImpl::hwStart()
{
    // Protect the hwLifecycle methods from being called synchronous.
    ACE_Guard< ACE_Mutex > guard(hwLifecycleMutex);

    ACS_TRACE(__PRETTY_FUNCTION__);

    if(deviceState_m == Control::HardwareDevice::Start)
    {
        return;
    }

    CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());

    if((deviceState_m != Control::HardwareDevice::Undefined)
    && (deviceState_m != Control::HardwareDevice::Stop))
    {
        std::ostringstream message;
        message << "Cannot put the component called "
            << tmpName.in()
            << " into the start state.\nThis component must be in the "
                "undefined or stop states before it can change to the start "
                "state. Currently it is in the "
            << stateToString(deviceState_m)
            << " state.";

        ControlDeviceExceptions::HwLifecycleExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", message.str());
        ex.log();
        throw ex.getHwLifecycleEx();
    }
    else
    {
        // Set this boolean to false which will allow
        // HardwareDeviceImpl::incrementDeviceErrorCounter to increment
        // the communication error counter in case of a communication error.
        isInHwStop = false;
    }

    monitoringOff();
    clearError();
    clearDeviceErrorCounter();
    hwStartAction();

    if(inErrorState() == true)
    {
        // If an exception was thrown, call hwStopDoWork.  A call to
        // hwStopAction will not be made.
        hwStopDoWork(false);

        throw ControlDeviceExceptions::HwLifecycleExImpl(__FILE__, __LINE__,
            __PRETTY_FUNCTION__).getHwLifecycleEx();
    }

    logNewState(deviceState_m, Control::HardwareDevice::Start);
    deviceState_m = Control::HardwareDevice::Start;
    // Store the state change in the archive somehow
    archiveStateChange(deviceState_m);
}

void Control::HardwareDeviceImpl::hwConfigure()
{
    // Protect the hwLifecycle methods from being called synchronous.
    ACE_Guard< ACE_Mutex > guard(hwLifecycleMutex);

    ACS_TRACE(__PRETTY_FUNCTION__);

    if(deviceState_m == Control::HardwareDevice::Configure)
    {
        return;
    }

    CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());

    if(deviceState_m != Control::HardwareDevice::Start)
    {
        std::ostringstream message;
        message << "Cannot put the component called "
            << tmpName.in()
            << " into the configure state.\nThis component must be in the "
                "start state before it can change to the configure state. "
                "Currently it is in the "
            << stateToString(deviceState_m)
            << " state.";

        ControlDeviceExceptions::HwLifecycleExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", message.str());
        ex.log();
        throw ex.getHwLifecycleEx();
    }

    // After this call, serialNumber_m should be set
    try
    {
        hwConfigureAction();
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        // If an exception was thrown, call hwStopDoWork.  A call to
        // hwStopAction will not be made.
        hwStopDoWork(false);

        throw ControlDeviceExceptions::HwLifecycleExImpl(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__).getHwLifecycleEx();
    }

    logNewState(deviceState_m, Control::HardwareDevice::Configure);
    deviceState_m = Control::HardwareDevice::Configure;
    archiveStateChange(deviceState_m);
}

void Control::HardwareDeviceImpl::hwInitialize()
{
    // Protect the hwLifecycle methods from being called synchronous.
    ACE_Guard< ACE_Mutex > guard(hwLifecycleMutex);

    ACS_TRACE(__PRETTY_FUNCTION__);

    if(deviceState_m == Control::HardwareDevice::Initialize)
    {
        return;
    }

    CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());

    if((deviceState_m != Control::HardwareDevice::Configure)
    && (deviceState_m != Control::HardwareDevice::Operational)
    && (deviceState_m != Control::HardwareDevice::Simulation)
    && (deviceState_m != Control::HardwareDevice::Diagnostic))
    {
        std::ostringstream message;
        message << "Cannot put the component called "
            << tmpName.in()
            << " into the initialize state.\nThis component must be in the "
                "configure, operational simulation or diagnostic states before "
                "it can change to the initialize state. Currently it is in the "
            << stateToString(deviceState_m)
            << " state.";

        ControlDeviceExceptions::HwLifecycleExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", message.str());
        ex.log();
        throw ex.getHwLifecycleEx();
    }

    hwInitializeAction();

    if(inErrorState() == true)
    {
        // If an exception was thrown, call hwStopDoWork.  A call to
        // hwStopAction will not be made.
        hwStopDoWork(false);

        throw ControlDeviceExceptions::HwLifecycleExImpl(__FILE__, __LINE__,
            __PRETTY_FUNCTION__).getHwLifecycleEx();
    }

    registerWithCollector();
    logNewState(deviceState_m, Control::HardwareDevice::Initialize);
    deviceState_m = Control::HardwareDevice::Initialize;
    archiveStateChange(deviceState_m);
}

void Control::HardwareDeviceImpl::hwOperational()
{
    // Protect the hwLifecycle methods from being called synchronous.
    ACE_Guard< ACE_Mutex > guard(hwLifecycleMutex);

    ACS_TRACE(__PRETTY_FUNCTION__);

    if(deviceState_m == Control::HardwareDevice::Operational)
    {
        return;
    }

    CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());

    if((deviceState_m != Control::HardwareDevice::Initialize)
    && (deviceState_m != Control::HardwareDevice::Simulation)
    && (deviceState_m != Control::HardwareDevice::Diagnostic))
    {
        std::ostringstream message;
        message << "Cannot put the component called "
            << tmpName.in()
            << " into the operational state.\nThis component must be in the "
                "initialize, simulation or diagnostic states before it can "
                "change to the operational state. Currently it is in the "
            << stateToString(deviceState_m)
            << " state.";

        ControlDeviceExceptions::HwLifecycleExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", message.str());
        ex.log();
        throw ex.getHwLifecycleEx();
    }

    // Thomas, 2010-06-21
    // Log the transition from initialised to operational before(!) the device
    // state is set to operational.  I know that this could be called
    // cheating but the rationale behind this is to avoid a message like the
    // one that Victor noticed:
    // "2010-06-16T23:21:46.369 Info CONTROL/DV03/WVR Switched hardware state
    // of component CONTROL/DV03/WVR: operational -> operational"
    // So by logging the transition first, then making the transition, and then
    // allowing MonitorHelper threads to do their stuff, makes everybody
    // happy and the world a nicer place.
    logNewState(deviceState_m, Control::HardwareDevice::Operational);

    // Please see the comment just above, too!
    // Set the current hw-state to operational.  This allows for control
    // and monitor points to be executed in the hwOperationalAction method.
    deviceState_m = Control::HardwareDevice::Operational;

    hwOperationalAction();

    if(inErrorState() == true)
    {
        // If an exception was thrown, call hwStopDoWork.  A call to
        // hwStopAction will not be made.
        hwStopDoWork(false);

        throw ControlDeviceExceptions::HwLifecycleExImpl(__FILE__, __LINE__,
            __PRETTY_FUNCTION__).getHwLifecycleEx();
    }

    archiveStateChange(deviceState_m);
    monitoringOn();
}

void Control::HardwareDeviceImpl::hwDiagnostic()
{
    // Protect the hwLifecycle methods from being called synchronous.
    ACE_Guard< ACE_Mutex > guard(hwLifecycleMutex);

    ACS_TRACE(__PRETTY_FUNCTION__);

    if(deviceState_m == Control::HardwareDevice::Diagnostic)
    {
        return;
    }

    CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());

    if((deviceState_m != Control::HardwareDevice::Initialize)
    && (deviceState_m != Control::HardwareDevice::Simulation)
    && (deviceState_m != Control::HardwareDevice::Operational))
    {
        std::ostringstream message;
        message << "Cannot put the component called "
            << tmpName.in()
            << " into the diagnostic state.\nThis component must be in the "
                "initialize, operational or simulation state before it can "
                "change to the diagnostic state. Currently it is in the "
            << stateToString(deviceState_m)
            << " state.";

        ControlDeviceExceptions::HwLifecycleExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", message.str());
        ex.log();
        throw ex.getHwLifecycleEx();
    }

    if(diagnosticModeEnabled == false)
    {
        throw ControlDeviceExceptions::HwLifecycleExImpl(__FILE__, __LINE__,
            __PRETTY_FUNCTION__).getHwLifecycleEx();
    }

    hwDiagnosticAction();

    if(inErrorState() == true)
    {
        // If an exception was thrown, call hwStopDoWork.  A call to
        // hwStopAction will not be made.
        hwStopDoWork(false);

        throw ControlDeviceExceptions::HwLifecycleExImpl(__FILE__, __LINE__,
            __PRETTY_FUNCTION__).getHwLifecycleEx();
    }

    monitoringOn();
    logNewState(deviceState_m, Control::HardwareDevice::Diagnostic);
    deviceState_m = Control::HardwareDevice::Diagnostic;
    archiveStateChange(deviceState_m);
}

void Control::HardwareDeviceImpl::hwSimulation()
{
    // Protect the hwLifecycle methods from being called synchronous.
    ACE_Guard< ACE_Mutex > guard(hwLifecycleMutex);

    ACS_TRACE(__PRETTY_FUNCTION__);

    if(deviceState_m == Control::HardwareDevice::Simulation)
    {
        return;
    }

    CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());

    if((deviceState_m != Control::HardwareDevice::Initialize)
    && (deviceState_m != Control::HardwareDevice::Diagnostic)
    && (deviceState_m != Control::HardwareDevice::Operational))
    {
        std::ostringstream message;
        message << "Cannot put the component called "
            << tmpName.in()
            << " into the simulation state.\n";

        ControlDeviceExceptions::HwLifecycleExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", message.str());
        ex.log();
        throw ex.getHwLifecycleEx();
    }

    if(simulationModeEnabled == false)
    {
        throw ControlDeviceExceptions::HwLifecycleExImpl(__FILE__, __LINE__,
            __PRETTY_FUNCTION__).getHwLifecycleEx();
    }

    hwSimulationAction();

    if(inErrorState() == true)
    {
        // If an exception was thrown, call hwStopDoWork.  A call to
        // hwStopAction will not be made.
        hwStopDoWork(false);

        throw ControlDeviceExceptions::HwLifecycleExImpl(__FILE__, __LINE__,
            __PRETTY_FUNCTION__).getHwLifecycleEx();
    }

    monitoringOn();
    logNewState(deviceState_m, Control::HardwareDevice::Simulation);
    deviceState_m = Control::HardwareDevice::Simulation;
    archiveStateChange(deviceState_m);
}

void Control::HardwareDeviceImpl::hwStop()
{
    // Protect the hwLifecycle methods from being called synchronous.
    ACE_Guard< ACE_Mutex > guard(hwLifecycleMutex);

    ACS_TRACE(__PRETTY_FUNCTION__);

    hwStopDoWork();
}

void Control::HardwareDeviceImpl::hwStopDoWork(bool calledFromHwStop)
{
    if(deviceState_m == Control::HardwareDevice::Stop)
    {
        return;
    }
    else
    {
        // Set this boolean to true which will prevent
        // HardwareDeviceImpl::incrementDeviceErrorCounter from incrementing
        // the communication error counter in case of a communication error.
        // Remember that once the error counter reaches MAX_ERROR_COUNTER,
        // incrementDeviceErrorCounter would call hwStop, i.e. creating an
        // endless recursion.
        isInHwStop = true;
    }

    if((deviceState_m == Control::HardwareDevice::Diagnostic)
    || (deviceState_m == Control::HardwareDevice::Simulation)
    || (deviceState_m == Control::HardwareDevice::Operational))
    {
        // Turn off monitoring.
        monitoringOff();
        // Deregister with monitor collector
        deregisterWithCollector();
    }

    if(calledFromHwStop == true)
    {
        // Carry out the action.
        hwStopAction();
    }

    logNewState(deviceState_m, Control::HardwareDevice::Stop);
    deviceState_m = Control::HardwareDevice::Stop;
    archiveStateChange(deviceState_m);
}

void Control::HardwareDeviceImpl::checkHwStateOrThrow(HwStateFunction ptr,
    const std::string& file, unsigned int line,
    const std::string& function)
{
    // Call the HwStateFunction which is a member of this class and check
    // the returned boolean result:
    if(((*this).*(ptr))() == false)
    {
        ControlExceptions::INACTErrorExImpl ex(file.c_str(), line,
            function.c_str());
        ex.addData("Detail", "Cannot execute monitor or control request.  "
            "Device is inactive.");
        ex.log();
        throw ex;
    }
}

void Control::HardwareDeviceImpl::registerWithCollector()
{
    monitoring = false;

    if((CORBA::is_nil(monitorCollector_m) == true)
    && (getSerialNumber() != "na"))
    {
        CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());

        try
        {
            const std::string compName(tmpName.in());
            const std::string::size_type position(compName.find('/'));

            maci::ComponentInfo compInfo;
            compInfo = acscomponent::ACSComponentImpl::getContainerServices()->
                getComponentDescriptor(compName.c_str());

            const std::string contName(compInfo.container_name);

            if(position != std::string::npos)
            {
                const std::string::size_type position2(contName.find('/',
                    position + 1));
                if(position2 != std::string::npos)
                {
                    antennaLoc_m = contName.substr(0, position2 + 1);
                }
                else
                {
                    antennaLoc_m = "";
                }
            }
            else
            {
                antennaLoc_m = "";
            }

            const std::string monitorCollectorName(antennaLoc_m
                + "MONITOR_COLLECTOR");

            maci::ComponentSpec collectorSpec;
            collectorSpec.component_name = monitorCollectorName.c_str();
            collectorSpec.component_type = "IDL:alma/TMCDB/MonitorCollector:1.0";
            collectorSpec.component_code = "MonitorCollector";
            //collectorSpec.container_name = compInfo.container_name;
            collectorSpec.container_name = "*";

            monitorCollector_m =
                    acscomponent::ACSComponentImpl::getContainerServices()->
                        getCollocatedComponent< TMCDB::MonitorCollector >
                            (collectorSpec, false, compName.c_str());

            monitorCollector_m->registerMonitoredDevice(compName.c_str(),
                getSerialNumber());
            const std::string msg(compName
                + " registered succesfully with its MONITOR_COLLECTOR");
            LOG_TO_DEVELOPER(LM_DEBUG, msg);
        }
        catch(const maciErrType::CannotGetComponentExImpl& ex)
        {
            monitorCollector_m = TMCDB::MonitorCollector::_nil();

            const std::string msg(std::string(tmpName.in()) + std::string(
                " component cannot get a reference to MONITOR_COLLECTOR"));
            LOG_TO_DEVELOPER(LM_WARNING, msg);

            MonitorCollectorErr::RegisteringDeviceProblemExImpl newEx(ex,
                __FILE__, __LINE__, __PRETTY_FUNCTION__);
            newEx.addData("Detail", msg);
            newEx.log();
        }
        catch(const maciErrType::IncompleteComponentSpecExImpl &ex)
        {
            monitorCollector_m = TMCDB::MonitorCollector::_nil();

            const std::string msg(std::string(tmpName.in()) + std::string(
                " Incomplete Component Spec "));
            LOG_TO_DEVELOPER(LM_WARNING, msg);

            MonitorCollectorErr::RegisteringDeviceProblemExImpl newEx(ex,
                __FILE__, __LINE__, __PRETTY_FUNCTION__);
            newEx.addData("Detail", msg);
            newEx.log();
        }
        catch(const maciErrType::InvalidComponentSpecExImpl &ex)
        {
            monitorCollector_m = TMCDB::MonitorCollector::_nil();

            const std::string msg(std::string(tmpName.in()) + std::string(
                    "Invalid Component Spec"));
            LOG_TO_DEVELOPER(LM_WARNING, msg);

            MonitorCollectorErr::RegisteringDeviceProblemExImpl newEx(ex,
                __FILE__, __LINE__, __PRETTY_FUNCTION__);
            newEx.addData("Detail", msg);
            newEx.log();
        }
        catch(const maciErrType::ComponentSpecIncompatibleWithActiveComponentExImpl &ex)
        {
            monitorCollector_m = TMCDB::MonitorCollector::_nil();

            const std::string msg(std::string(tmpName.in()) + std::string(
                    "Component Spec Incompatible With Active Component"));
            LOG_TO_DEVELOPER(LM_WARNING, msg);

            MonitorCollectorErr::RegisteringDeviceProblemExImpl newEx(ex,
                __FILE__, __LINE__, __PRETTY_FUNCTION__);
            newEx.addData("Detail", msg);
            newEx.log();
        }
        catch(const MonitorCollectorErr::DeviceAlreadyRegistredEx &ex)
        {
            std::ostringstream msg;
            msg << "Device "
                << tmpName.in()
                << " already registered";
            LOG_TO_DEVELOPER(LM_WARNING, msg.str());
        }
        catch(const MonitorCollectorErr::RegisteringDeviceProblemEx &ex)
        {
            ///
            /// TODO
            /// Thomas, Jun 24, 2009
            /// What should happen with the TMCDB::MonitorCollector reference?
            ///
            std::ostringstream msg;
            msg << "Registering device "
                << tmpName.in()
                << " did NOT succeded";

            MonitorCollectorErr::RegisteringDeviceProblemExImpl newEx(ex,
                __FILE__, __LINE__, __PRETTY_FUNCTION__);
            newEx.addData("Detail", msg.str());
            newEx.log();
        }
    }
}

void Control::HardwareDeviceImpl::monitoringOn()
{
    CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());

    if(CORBA::is_nil(monitorCollector_m) == true)
    {
        monitoring = false;

        std::ostringstream msg;
        msg << "Component "
            << tmpName.in()
            << " is not registered with its MONITOR_COLLECTOR component";
        LOG_TO_DEVELOPER(LM_WARNING, msg.str());
    }
    else
    {
        try
        {
            startPropertiesMonitoring();
            monitorCollector_m->startMonitoring(tmpName.in());
            monitoring = true;

            std::ostringstream msg;
            msg << "Component "
                << tmpName.in()
                << " has started to send monitoring information to Collector";
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }
        catch(const MonitorCollectorErr::StartMonitoringProblemEx& ex)
        {
            stopPropertiesMonitoring();
            monitoring = false;

            std::ostringstream msg;
            msg << "Start monitoring on device "
                << tmpName.in()
                << " did NOT succeded";
            MonitorCollectorErr::StartMonitoringProblemExImpl newEx(ex,
                __FILE__, __LINE__, __PRETTY_FUNCTION__);
            newEx.addData("Detail", msg.str());
            newEx.log();
        }
    }
}

void Control::HardwareDeviceImpl::monitoringOff()
{
    CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());

    if(CORBA::is_nil(monitorCollector_m) == true)
    {
        std::ostringstream msg;
        msg << "Component "
            << tmpName.in()
            << " is not registered with its MONITOR_COLLECTOR component";
        LOG_TO_DEVELOPER(LM_WARNING, msg.str());

    }
    else
    {
        try
        {
            stopPropertiesMonitoring();
            monitorCollector_m->stopMonitoring(tmpName.in());
            monitoring = false;

            std::ostringstream msg;
            msg << "Component "
                << tmpName.in()
                << " stop succesfully its monitoring process";
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }
        catch(const MonitorCollectorErr::StopMonitoringProblemEx& ex)
        {
            stopPropertiesMonitoring();
            monitoring = false;

            std::ostringstream msg;
            msg << "Stop monitoring on device "
                << tmpName.in()
                << " did NOT succeded";
            MonitorCollectorErr::StopMonitoringProblemExImpl newEx(ex,
                __FILE__, __LINE__, __PRETTY_FUNCTION__);
            newEx.addData("Detail", msg.str());
            newEx.log();
        }
    }
}

void Control::HardwareDeviceImpl::deregisterWithCollector()
{
    CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());

    if(CORBA::is_nil(monitorCollector_m) == true)
    {
        std::ostringstream message;
        message << "Component "
            << tmpName.in()
            << " is not registered with its MONITOR_COLLECTOR component";
        LOG_TO_DEVELOPER(LM_DEBUG, message.str());
    }
    else
    {
        try
        {
            monitorCollector_m->deregisterMonitoredDevice(tmpName.in());

            const std::string compName(tmpName.in());
            const std::string monitorCollectorName(antennaLoc_m
                + "MONITOR_COLLECTOR");
            acscomponent::ACSComponentImpl::getContainerServices()->
                releaseComponent(monitorCollectorName.c_str());

            monitorCollector_m = TMCDB::MonitorCollector::_nil();

            const std::string msg("Deregistering device" + compName
                + " succeded");
            LOG_TO_DEVELOPER(LM_DEBUG, msg);
        }
        catch(const MonitorCollectorErr::DeviceNotRegistredEx& ex)
        {
            monitorCollector_m = TMCDB::MonitorCollector::_nil();
            std::ostringstream msg;
            msg << "Deregistering from collector on device "
                << tmpName.in()
                << " did NOT succeded";
            MonitorCollectorErr::DeviceNotRegistredExImpl newEx(ex, __FILE__,
                __LINE__, __PRETTY_FUNCTION__);
            newEx.addData("Detail", msg.str());
            newEx.log();
        }
        catch(const maciErrType::CannotReleaseComponentExImpl& ex)
        {
            monitorCollector_m = TMCDB::MonitorCollector::_nil();
            std::ostringstream msg;
            msg << "Could not release collector refference on device "
                << tmpName.in()
                << " did NOT succeded";
            MonitorCollectorErr::DeviceNotRegistredExImpl newEx(ex, __FILE__,
                __LINE__, __PRETTY_FUNCTION__);
            newEx.addData("Detail", msg.str());
            newEx.log();
        }
    }
}


bool Control::HardwareDeviceImpl::isMonitoring()
{
    return monitoring;
}


bool Control::HardwareDeviceImpl::checkMonitor()
{
    if(CORBA::is_nil(monitorCollector_m) == true)
    {
        return false;
    }

    return true;
}


Control::HardwareDevice::HwState Control::HardwareDeviceImpl::getHwState()
{
    return deviceState_m;
}


void Control::HardwareDeviceImpl::archiveStateChange(
    Control::HardwareDevice::HwState newState)
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}


void Control::HardwareDeviceImpl::initialize()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        hwStart();
    }
    catch(const ControlDeviceExceptions::HwLifecycleEx& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
}


void Control::HardwareDeviceImpl::cleanUp()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    if(deviceState_m != Control::HardwareDevice::Stop)
    {
        try
        {
            hwStop();
        }
        catch(const ControlDeviceExceptions::HwLifecycleEx& ex)
        {
            LOG_TO_OPERATOR(LM_WARNING, "Caught a hardware lifecycle exception "
                "during the transition to the hwStop state.  Will continue "
                "anyway but keep in mind that the hardware might not go to "
                "the operational state.");
        }
        catch(...)
        {
            LOG_TO_OPERATOR(LM_WARNING, "Caught an undefined exception during "
                "the transition to the hwStop state.  Will continue "
                "anyway but keep in mind that the hardware might not go to "
                "the operational state.");
        }
    }

    CharacteristicControlDeviceImpl::cleanUp();
}


bool Control::HardwareDeviceImpl::isReady()
{
    return ((deviceState_m == Control::HardwareDevice::Simulation)
        || (deviceState_m == Control::HardwareDevice::Diagnostic)
        || (deviceState_m == Control::HardwareDevice::Operational));
}


bool Control::HardwareDeviceImpl::isStartup()
{
    return ((deviceState_m == Control::HardwareDevice::Configure)
        || (deviceState_m == Control::HardwareDevice::Initialize)
        || (deviceState_m == Control::HardwareDevice::Simulation)
        || (deviceState_m == Control::HardwareDevice::Diagnostic)
        || (deviceState_m == Control::HardwareDevice::Operational));
}


bool Control::HardwareDeviceImpl::isDiagnostic()
{
    return (deviceState_m == Control::HardwareDevice::Diagnostic);
}


/* The following methods should never be called.  They are here just
 to enable Control Devices to have these methods
 */
void Control::HardwareDeviceImpl::setSubdeviceError(const char* subdeviceName)
{
    setError("Hardware device claimed to have subdevice");
}


void Control::HardwareDeviceImpl::clearSubdeviceError(const char* subdeviceName)
{
    setError("Hardware device claimed to have subdevice");
}


std::string Control::HardwareDeviceImpl::stateToString(
    Control::HardwareDevice::HwState state)
{
    std::string stateString;
    switch(state)
    {
        case Control::HardwareDevice::Undefined:
        {
            stateString = "undefined";
        }
            break;

        case Control::HardwareDevice::Start:
        {
            stateString = "start";
        }
            break;

        case Control::HardwareDevice::Configure:
        {
            stateString = "configure";
        }
            break;

        case Control::HardwareDevice::Initialize:
        {
            stateString = "initialize";
        }
            break;

        case Control::HardwareDevice::Operational:
        {
            stateString = "operational";
        }
            break;

        case Control::HardwareDevice::Simulation:
        {
            stateString = "simulation";
        }
            break;

        case Control::HardwareDevice::Diagnostic:
        {
            stateString = "diagnostic";
        }
            break;

        case Control::HardwareDevice::Stop:
        {
            stateString = "stop";
        }
            break;

        default:
        {
            stateString = "";
        }
    }

    return stateString;
}

void Control::HardwareDeviceImpl::logNewState(
    Control::HardwareDevice::HwState oldState,
    Control::HardwareDevice::HwState newState)
{
    CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());
    std::ostringstream msg;
    msg << "Switched hardware state of component "
        << tmpName.in()
        << ": "
        << stateToString(oldState)
        << " -> "
        << stateToString(newState);

    LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
}

CORBA::ULong Control::HardwareDeviceImpl::getDeviceCommunicationErrorCounter()
{
#if __GNUC__ >= 4
    const unsigned int counter(__sync_fetch_and_add(&errorCounter, 0U));
#else
    m_ErrorCounterMutex.acquire();
    const unsigned int counter(errorCounter);
    m_ErrorCounterMutex.release();
#endif

    return static_cast< CORBA::ULong >(counter);
}

unsigned int Control::HardwareDeviceImpl::incrementDeviceErrorCounter()
{
    // Do not report any communication errors when the component is already in
    // hwStop.  Some devices need to communicate with the hardware during hwStop
    // and excessive communication errors would lead to repeated calls to hwStop
    // from here.
    if(isInHwStop == true)
    {
#if __GNUC__ >= 4
        // Do not add anything, just get the current value.
        const unsigned int counter(__sync_fetch_and_add(&errorCounter, 0U));
#else
        m_ErrorCounterMutex.acquire();
        const unsigned int counter(errorCounter);
        m_ErrorCounterMutex.release();
#endif
        return counter;
    }

    // A communication error happened, increment the error counter.  Keep a copy
    // for log messages and return it to the caller.  Do it atomically for GCC
    // compiler version >= 4 which has those operations built-in.
#if __GNUC__ >= 4
    const unsigned int counter(__sync_add_and_fetch(&errorCounter, 1U));
#else
    m_ErrorCounterMutex.acquire();
    const unsigned int counter(++errorCounter);
    m_ErrorCounterMutex.release();
#endif

    // If the error counter equals MAX_ERROR_COUNTER, stop the device.
    // Check for greater/equals to make sure that parallel access, even
    // if it is protected by the mutex, cannot invalidate the check.
    if(counter >= MAX_ERROR_COUNTER)
    {
        std::ostringstream msg;
        msg << "Maximum number of communication errors reached.  Stopping "
            "this hardware device.";
        LOG_TO_OPERATOR(LM_ERROR, msg.str());

        try
        {
            this->hwStop();
            alarmSender_m.activateAlarm(ERROR_COUNTER_ALARM_CODE);
        }
        catch(...)
        {
            std::ostringstream msg;
            msg << "hwStop() call failed!  This means that the device will "
                "probably continue to fail and there is now way to shut it "
                "down.";
            LOG_TO_OPERATOR(LM_ERROR, msg.str());
        }
    }

    return counter;
}


void Control::HardwareDeviceImpl::clearDeviceErrorCounter()
{
#if __GNUC__ >= 4
    unsigned int foo __attribute__((unused)) = __sync_and_and_fetch(
        &errorCounter, 0U);
#else
    m_ErrorCounterMutex.acquire();
    errorCounter = 0U;
    m_ErrorCounterMutex.release();
#endif
}

void Control::HardwareDeviceImpl::clearDeviceCommunicationErrorAlarm()
{
    alarmSender_m.deactivateAlarm(ERROR_COUNTER_ALARM_CODE);
}
