//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <controlAlarmHelper.h>
#include <map>
#include <memory>
#include <loggingMACROS.h>
#include <acsutilTimeStamp.h>


Control::AlarmInformation::AlarmInformation():
    alarmCode(0),
    alarmTerminateCount(1),
    alarmActivateCount(1)
{
}

Control::AlarmInformation::~AlarmInformation()
{
}


Control::AlarmHelper::AlarmHelper():
    alarmSource_m(0),
    inhibitionTime_m(0),
    disable_m(false)
{
    try
    {
        alarmSource_m = ACSAlarmSystemInterfaceFactory::createSource(
            "ALARM_SYSTEM_SOURCES");
    }
    catch(acsErrTypeAlarmSourceFactory::ACSASFactoryNotInitedExImpl &ex)
    {
        ex.log();
        LOG_TO_OPERATOR(LM_ERROR, "ACSAlarmSystemInterfaceFactory could not "
            "be initialised. Alarm Sending will be disabled.");
    }

}

Control::AlarmHelper::~AlarmHelper()
{
    std::map< int, AlarmInformation >::iterator iter(
        alarmInformationMap_m.begin());
    for(; iter != alarmInformationMap_m.end(); ++iter)
    {
        delete alarms_m[(*iter).first];
    }
    // FIXME We cannot delete the reference to the interface
    // It crashed in flames, this is probably an error in the
    // alarm interface. Since it is a singleton
    // harm of not deleting it is minimised.
    //  delete alarmSource_m;
    //  alarmSource_m = 0;//Just for kicks.
}

Control::AlarmHelper::AlarmHelper(const AlarmHelper& original):
    alarmCodes_m(original.alarmCodes_m),
    alarmSource_m(0),
    activated_m(original.activated_m),
    terminateCommandCounter_m(original.terminateCommandCounter_m),
    activateCommandCounter_m(original.activateCommandCounter_m),
    alarmInformationMap_m(original.alarmInformationMap_m),
    inhibitionTime_m(original.inhibitionTime_m),
    disable_m(original.disable_m)
{
    try
    {
        alarmSource_m = ACSAlarmSystemInterfaceFactory::createSource(
            "ALARM_SYSTEM_SOURCES");
    }
    catch(acsErrTypeAlarmSourceFactory::ACSASFactoryNotInitedExImpl &ex)
    {
        ex.log();
        LOG_TO_OPERATOR(LM_ERROR, "ACSAlarmSystemInterfaceFactory could not "
            "be initialised. Alarm Sending will be disabled.");

        return;
    }

    //Have to make a temporal copy, otherwise gcc chokes.
    std::map< int, acsalarm::FaultState* > Talarms(original.alarms_m);
    for(std::map< int, acsalarm::FaultState* >::iterator iter(Talarms.begin());
        iter != Talarms.end(); ++iter)
    {
        std::auto_ptr< acsalarm::FaultState > alarm(
            ACSAlarmSystemInterfaceFactory::createFaultState(
                (*iter).second->getFamily(), (*iter).second->getMember(),
                (*iter).second->getCode()));
        alarms_m[(*iter).second->getCode()] = alarm.release();
    }
}

void Control::AlarmHelper::resetAlarms()
{
    for(std::map< int, AlarmInformation >::iterator iter(
        alarmInformationMap_m.begin()); iter != alarmInformationMap_m.end();
        ++iter)
    {
        delete alarms_m[(*iter).first];
    }

    alarmCodes_m.clear();
    alarms_m.clear();
    activated_m.clear();
    terminateCommandCounter_m.clear();
    activateCommandCounter_m.clear();
    alarmInformationMap_m.clear();
    inhibitionTime_m = 0;
    disable_m = false;
}

void Control::AlarmHelper::initializeAlarms(const std::string family,
    const std::string member,
    const std::vector< AlarmInformation > alarmInformation)
{
    if(alarmSource_m == 0)
    {
        LOG_TO_OPERATOR(LM_ERROR, "ACSAlarmSystemInterfaceFactory could not "
            "be initialised. Alarm Sending will be disabled.");

        return;
    }

    for(std::vector< AlarmInformation >::const_iterator iter(
        alarmInformation.begin()); iter != alarmInformation.end(); ++iter)
    {
        // It is possible that an alarm gets that is already active gets
        // initialised with a different member name. In this case
        // the control alarm helper will clear the old alarm.
        if(isAlarmSet((*iter).alarmCode) == true)
        {
            deactivateAlarm((*iter).alarmCode);
        }

        std::auto_ptr< acsalarm::FaultState > alarm(
            ACSAlarmSystemInterfaceFactory::createFaultState(family, member,
                (*iter).alarmCode));
        alarms_m[(*iter).alarmCode] = alarm.release();
        activated_m[(*iter).alarmCode] = false;
        terminateCommandCounter_m[(*iter).alarmCode] = 0;//alarmInformation[index].alarmTerminateCount;
        activateCommandCounter_m[(*iter).alarmCode] = 0;//alarmInformation[index].alarmActivateCount;
        alarmInformationMap_m[(*iter).alarmCode] = (*iter);
    }
}

bool Control::AlarmHelper::findAlarm(const int code)
{
    if(alarms_m.find(code) != alarms_m.end())
    {
        return true;
    }

    return false;
}

void Control::AlarmHelper::activateAlarm(const int code)
{
    send(code, true);
}

void Control::AlarmHelper::deactivateAlarm(const int code)
{
    send(code, false);
}

void Control::AlarmHelper::updateAlarm(const int code, const bool state)
{
    send(code, state);
}

bool Control::AlarmHelper::isAlarmSet()
{
    bool alarm = false;
    for(std::map< int, bool >::iterator it(activated_m.begin());
        it != activated_m.end(); ++it)
    {
        if((*it).second == true)
        {
            alarm = true;
            break;
        }
    }

    return alarm;
}

bool Control::AlarmHelper::isAlarmSet(const int code)
{
    if(findAlarm(code) == false)
    {
        return false;
    }

    return activated_m[code];
}

void Control::AlarmHelper::inhibitAlarms(const ACS::Time duration)
{
    inhibitionTime_m = std::max(inhibitionTime_m, ::getTimeStamp() + duration);
}

void Control::AlarmHelper::resetAlarmsInhibitionTime()
{
    inhibitionTime_m = 0;
}

void Control::AlarmHelper::disableAlarms()
{
    disable_m = true;
}

void Control::AlarmHelper::enableAlarms()
{
    disable_m = false;
}

void Control::AlarmHelper::terminateAllAlarms()
{
    for(std::map< int, AlarmInformation >::iterator iter(
        alarmInformationMap_m.begin()); iter != alarmInformationMap_m.end();
        ++iter)
    {
        send((*iter).first, false);
    }
}

void Control::AlarmHelper::forceTerminateAllAlarms()
{
    for(std::map< int, AlarmInformation >::iterator iter(
        alarmInformationMap_m.begin()); iter
        != alarmInformationMap_m.end(); ++iter)
    {
        send((*iter).first, false, true);
    }
}

std::string Control::AlarmHelper::getAlarmDescription(const int code)
{
    std::string ret;
    if(findAlarm(code) == true)
    {
        ret = alarmInformationMap_m[code].alarmDescription;
    }

    return ret;
}

std::string Control::AlarmHelper::createErrorMessage()
{
    std::ostringstream errMsg;
    bool firstMessage = true;

    for(std::map< int, bool >::iterator it(activated_m.begin());
        it != activated_m.end(); ++it)
    {
        if((*it).second == true)
        {
            if(firstMessage)
            {
                firstMessage = false;
            }
            else
            {
                errMsg << "\n";
            }

            errMsg << getAlarmDescription((*it).first);
        }
    }

    return errMsg.str();
}

void Control::AlarmHelper::send(const int code, const bool activate, bool force)
{
    if(alarmSource_m == 0)
    {
        LOG_TO_DEVELOPER(LM_WARNING, "AlarmSystemInterface not initialised. "
            "Not publishing alarm");

        return;
    }
    else if(findAlarm(code) == false)
    {
        std::ostringstream msg;
        msg << "Trying to operate over alarm code "
            << code
            << "which does not exist.";
        LOG_TO_DEVELOPER(LM_WARNING, msg.str());

        return;
    }

    std::string state;
    if((force == false)&& (disable_m == false))
    {
        // Do not activate  if alredy activated
        if(((activate == true) && (activated_m[code] == true)))
        {
            return;
        }
        // Do not deactivate if not activated before
        else if(((activate == false) && (activated_m[code] == false)))
        {
            return;
        }
        else if((inhibitionTime_m != 0)
        && (::getTimeStamp() < inhibitionTime_m))
        {
            // No alarms during inhibition time
            return;
        }
        else
        {
            // Set inhibition time to zero so we avoid unnecessary call to
            // getTimeStamp()
            inhibitionTime_m = 0;
        }

        if(activate == true)
        {
            state = faultState::ACTIVE_STRING;
            ++(activateCommandCounter_m[code]);

            if(activateCommandCounter_m[code]
                < alarmInformationMap_m[code].alarmActivateCount)
            {
                return;
            }

            // reset the terminate counter
            terminateCommandCounter_m[code] = 0;
        }
        else
        {
            state = faultState::TERMINATE_STRING;
            ++(terminateCommandCounter_m[code]);

            if(terminateCommandCounter_m[code]
                < alarmInformationMap_m[code].alarmTerminateCount)
            {
                return;
            }

            activateCommandCounter_m[code] = 0;
        }
    }
    else
    {
        state = faultState::TERMINATE_STRING;
        terminateCommandCounter_m[code] = 0;
        activateCommandCounter_m[code] = 0;
    }

    activated_m[code] = activate;
    alarms_m[code]->setDescriptor(state);

    // create a Timestamp and use it to configure the FaultState
    std::auto_ptr< acsalarm::Timestamp > tstampAutoPtr(
        new acsalarm::Timestamp);
    alarms_m[code]->setUserTimestamp(tstampAutoPtr);

    // push the FaultState using the AlarmSystemInterface previously created
    //acsalarm::FaultState stateToPush(*fltstate);
    alarmSource_m->push(*alarms_m[code]);
    if(activate == true)
    {
        handleActivateAlarm(code);
    }
    else
    {
        handleDeactivateAlarm(code);
    }
}
