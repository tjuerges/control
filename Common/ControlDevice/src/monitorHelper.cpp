/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * $Id$
 *
 */


#include <monitorHelper.h>
// For ACE_OS::select
#include <OS_NS_sys_select.h>
// For ACE_Time_Value
#include <Time_Value.h>
// For ControlExceptions
#include <ControlExceptions.h>
// For maci::ContainerServices
#include <acsContainerServices.h>
// For std::memset and strerror
#include <cstring>
// For errno & friends
#include <cerrno>


MonitorHelper::MonitorHelper():
    monitorThread_p(0),
    myName(std::string("::MonitorHelper::"))
{
    const std::string fnName(myName + "MonitorHelper");
    AUTO_TRACE(fnName);

    availableRequests_m.clear();
    pendingRequests_m.clear();
}

MonitorHelper::MonitorHelper(const std::string& deviceName):
    monitorThread_p(0),
    myName(deviceName + std::string("::MonitorHelper::"))
{
    const std::string fnName(myName + "MonitorHelper");
    AUTO_TRACE(fnName);

    availableRequests_m.clear();
    pendingRequests_m.clear();
}

MonitorHelper::~MonitorHelper()
{
    const std::string fnName(myName + "~MonitorHelper");
    AUTO_TRACE(fnName);
}

void MonitorHelper::initialize(ACE_CString compName,
    maci::ContainerServices* cs)
{
    // If the name of the owner has not been set yet, set it now.
    if((myName.empty() == true)
    || (myName == "::MonitorHelper::"))
    {
        myName = compName.c_str() + std::string("::MonitorHelper::");
    }

    const std::string fnName(myName + "initialize");
    AUTO_TRACE(fnName);

    // Prepare the threads for use
    monitorThread_p = cs->getThreadManager()->
        create< MonitorThread, MonitorHelper* const >(
            compName + "MonitorThread", this);

    // Set the thread sleep time to 0.2s.
    monitorThread_p->setSleepTime(2000000);

    // Initialize semaphores
    if(sem_init(&availableRequestSem_m, 0, 1)
    || sem_init(&pendingRequestSem_m, 0, 1)
    || sem_init(&synchronizationSem_m, 0, 0))
    {
        acsErrTypeLifeCycle::LifeCycleExImpl ex(__FILE__, __LINE__,
            fnName.c_str());
        ex.addData("Detail", "Could not init one of the semaphores: "
            "availableRequestSem_m, pendingRequestSem_m, synchronizationSem_m");
        ex.log();
        throw ex;
    }
}

void MonitorHelper::cleanUp()
{
    const std::string fnName(myName + "cleanUp");
    AUTO_TRACE(fnName);

    // Shutdown the threads, the threads were suspended by the
    // baseclass call to hwStopAction so we just need to terminate them
    // here.
    monitorThread_p->terminate();

    // Sleep for 5s.  That should guarantee that the monitoring thread has
    // been terminated by then.
    ACE_Time_Value waitFor(5ULL);
    // Use ACE_OS::select to sleep.  This is an interruptible call
    // and other threads/processes can continue.
    ACE_OS::select(0, 0, 0, 0, waitFor);

    processPendingQueue();

    if(sem_wait(&availableRequestSem_m) != 0)
    {
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            fnName.c_str());
        std::ostringstream msg;
        msg << "Acquisition of the availableRequestSem_m semaphore failed. "
            "Error code = "
            << std::strerror(errno)
            << ". Continuing anyway.";
        ex.addData("Detail", msg.str());
        ex.log();
    }

    // Clean up all of the memory allocated to the queues
    while(availableRequests_m.empty() == false)
    {
        delete availableRequests_m.front();
        availableRequests_m.pop_front();
    }

    if(sem_wait(&pendingRequestSem_m) != 0)
    {
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            fnName.c_str());
        std::ostringstream msg;
        msg << "Acquisition of the pendingRequestSem_m semaphore failed. "
            "Error code = "
            << std::strerror(errno)
            << ". Continuing anyway.";
        ex.addData("Detail", msg.str());
        ex.log();
    }

    while(pendingRequests_m.empty() == false)
    {
        delete pendingRequests_m.front();
        pendingRequests_m.pop_front();
    }

    sem_destroy(&availableRequestSem_m);
    sem_destroy(&pendingRequestSem_m);
    sem_destroy(&synchronizationSem_m);
}

void MonitorHelper::resume()
{
    const std::string fnName(myName + "resume");
    AUTO_TRACE(fnName);

    monitorThread_p->resume();
}


void MonitorHelper::suspend()
{
    const std::string fnName(myName + "suspend");
    AUTO_TRACE(fnName);

    // Wait for up to 5 seconds for the queue to clear
    int waitTime(5);

    // Now wait for the queues to clear
    ACE_Time_Value waitFor(1ULL);
    while((pendingRequests_m.empty() == false)
    && (waitTime > 0))
    {
        // Use ACE_OS::select to sleep.  This is an interruptible call
        // and other threads/processes can continue.
        ACE_OS::select(0, 0, 0, 0, waitFor);
        // The Linux select call may modify the timeout value.  Reset it.
        waitFor.set(1.0);
        --waitTime;
    }

  monitorThread_p->suspend();
}

void MonitorHelper::processPendingQueue()
{
    std::list< MonitorHelper::AMBRequestStruct* >::iterator iter;

    while(sem_trywait(&synchronizationSem_m) == 0)
    {
        if(sem_wait(&pendingRequestSem_m) != 0)
        {
            const std::string fnName(myName + "::processPendingQueue");
            ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
                fnName.c_str());
            std::ostringstream msg;
            msg << "Acquisition of the pendingRequestSem_m semaphore failed. "
                "Error code = "
                << std::strerror(errno)
                << ". Continuing anyway.";
            ex.addData("Detail", msg.str());
            ex.log();
        }

        iter = pendingRequests_m.begin();
        while((iter != pendingRequests_m.end())
        && ((*iter)->Status == AMBERR_PENDING))
        {
            ++iter;
        }

        if(iter == pendingRequests_m.end())
        {
            const std::string fnName(myName + "::processPendingQueue");
            ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
                fnName.c_str());
            std::ostringstream msg;
            msg << "Error condition: iterator is invalid!  Continuing anyway.";
            ex.addData("Detail", msg.str());
            ex.log();
            sem_post(&pendingRequestSem_m);
            continue;
        }

        // At this point iter points to a structure that has reached the end
        // point do something about it
        MonitorHelper::AMBRequestStruct* procMessage = *iter;
        pendingRequests_m.erase(iter);
        if(sem_post(&pendingRequestSem_m) != 0)
        {
            const std::string fnName(myName + "::processPendingQueue");
            ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
                fnName.c_str());
            std::ostringstream msg;
            msg << "Release of the pendingRequestSem_m semaphore failed. "
                "Error code = "
                << std::strerror(errno)
                << ". Continuing anyway.";
            ex.addData("Detail", msg.str());
            ex.log();
        }

        // Here is where we actually process
        processRequestResponse(*procMessage);

        if(sem_wait(&availableRequestSem_m) != 0)
        {
            const std::string fnName(myName + "::processPendingQueue");
            ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
                fnName.c_str());
            std::ostringstream msg;
            msg << "Acquisition of the availableRequestSem_m semaphore failed. "
                "Error code = "
                << std::strerror(errno)
                << ". Continuing anyway.";
            ex.addData("Detail", msg.str());
            ex.log();
        }

        availableRequests_m.push_back(procMessage);

        if(sem_post(&availableRequestSem_m) != 0)
        {
            const std::string fnName(myName + "::processPendingQueue");
            ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
                fnName.c_str());
            std::ostringstream msg;
            msg << "Release of the availableRequestSem_m semaphore failed. "
                "Error code = "
                << std::strerror(errno)
                << ". Continuing anyway.";
            ex.addData("Detail", msg.str());
            ex.log();
        }
    }
}

MonitorHelper::AMBRequestStruct* MonitorHelper::getRequestStruct()
{
    MonitorHelper::AMBRequestStruct* newRequest;
    if(sem_wait(&availableRequestSem_m) != 0)
    {
        const std::string fnName(myName + "::getRequestStruct");
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            fnName.c_str());
        std::ostringstream msg;
        msg << "Acquisition of the availableRequestSem_m semaphore failed. "
            "Error code = "
            << std::strerror(errno)
            << ". Continuing anyway.";
        ex.addData("Detail", msg.str());
        ex.log();
    }

    if(availableRequests_m.empty())
    {
        newRequest = new MonitorHelper::AMBRequestStruct(&synchronizationSem_m);
    }
    else
    {
        newRequest = availableRequests_m.front();
        availableRequests_m.pop_front();
        newRequest->Status = AMBERR_PENDING;
    }

    if(sem_post(&availableRequestSem_m) != 0)
    {
        const std::string fnName(myName + "::getRequestStruct");
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            fnName.c_str());
        std::ostringstream msg;
        msg << "Release of the availableRequestSem_m semaphore failed. "
            "Error code = "
            << std::strerror(errno)
            << ". Continuing anyway.";
        ex.addData("Detail", msg.str());
        ex.log();
    }

    return newRequest;
}

void MonitorHelper::queueRequest(MonitorHelper::AMBRequestStruct* newRequest)
{
    if(sem_wait(&pendingRequestSem_m) != 0)
    {
        const std::string fnName(myName + "::queueRequest");
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            fnName.c_str());
        std::ostringstream msg;
        msg << "Acquisition of the pendingRequestSem_m semaphore failed. "
            "Error code = "
            << std::strerror(errno)
            << ". Continuing anyway.";
        ex.addData("Detail", msg.str());
        ex.log();
    }

    pendingRequests_m.push_back(newRequest);

    if(sem_post(&pendingRequestSem_m) != 0)
    {
        const std::string fnName(myName + "::queueRequest");
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            fnName.c_str());
        std::ostringstream msg;
        msg << "Release of the pendingRequestSem_m semaphore failed. "
            "Error code = "
            << std::strerror(errno)
            << ". Continuing anyway.";
        ex.addData("Detail", msg.str());
        ex.log();
    }
}


MonitorHelper::AMBRequestStruct::AMBRequestStruct(sem_t* synchLock):
        RCA(0UL),
        TargetTime(0ULL),
        DataLength(0U),
        SynchLock(synchLock),
        Timestamp(0ULL),
        Status(AMBERR_PENDING)
{
    std::memset(Data, 0, sizeof(Data));
}


MonitorHelper::MonitorThread::MonitorThread(const ACE_CString& name,
    const MonitorHelper* helper):
    ACS::Thread(name),
    monitorHelper_p(const_cast< MonitorHelper* >(helper)),
    myName("MonitorHelper::MonitorThread::")
{
    myName += name.c_str();
    AUTO_TRACE(myName + "::MonitorThread");
}

MonitorHelper::MonitorThread::~MonitorThread()
{
    AUTO_TRACE(myName + "::~MonitorThread");
}

void MonitorHelper::MonitorThread::runLoop()
{
    monitorHelper_p->processPendingQueue();
}
