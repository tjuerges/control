#! /usr/bin/env python
#
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# $Id$
#


import CCL.Container
import Acspy.Common.QoS
import Acspy.Util.ACSCorba
import math
import Acspy.Common.Err


class HardwareDevice:
    def __init__(self, componentName, stickyFlag = False):
        '''

        The HardwareDevice class is a python proxy to functions in the
        HardwareDevice interface. The component must already be
        running (usually started by the control master component)
        before creating this proxy.

        EXAMPLE:
        from HardwareDevice import *
        hw = HardwareDevice("CONTROL/DV01/HoloRx")
        state = hw.getHWState();
        hw.hwStart();
        hw.hwConfigure();
        hw.hwInitialize();
        hw.hwOperational();
        hw.hwSimulate();
        hw.hwStop();
        del(hw)
        '''
        self.__stickyFlag = stickyFlag
        self.__componentName = componentName
        if self.__stickyFlag:
            self.__hw = CCL.Container.getComponent(self.__componentName)
        else:
            self.__hw = CCL.Container.getComponentNonSticky(self.__componentName)

    def __del__(self):
        '''
        Destructor
        '''
        if self.__stickyFlag:
            CCL.Container.releaseComponent(self.__componentName)

    def getHwState(self):
        '''
        Returns the state of the hardware.
        '''
        return self.__hw.getHwState();

    def hwStart(self):
        '''
        Puts the hardware into the start state. This can only be done
        if the hardware is in the undefined or stop state and if it is
        in any other state a HwLifecycleEx exception will be
        thrown. The initialize component lifecycle method will call
        hwStart so that the hardware should end up in the start state
        once the component lifecycle has completed.
        '''
        return self.__hw.hwStart();

    def hwConfigure(self):
        '''
        Puts the hardware into the configure state. This can only be
        done if the hardware is in the start state and if it is in any
        other state a HwLifecycleEx exception will be thrown. The
        hwConfigure method will communicate with the hardware to get
        identification information like the node and serial number. It
        will use this to retrieve device specific data from the
        telescope configuration data base. It will not configure the
        hardware as that is done by the hwInitialize method.  This
        method will not change state and throw an <as-yet-undefined>
        exception if it cannot get the identification information from
        the hardware.

        '''
        return self.__hw.hwConfigure();

    def hwInitialize(self):
        '''
        Puts the hardware into the initialize state. This can only be
        done if the hardware is in the configure, operational,
        diagnostic or simulation states and if it is in any other
        state a HwLifecycleEx exception will be thrown. The
        hwInitialize method will initialize the hardware and leave it
        in a state it can safely maintain for long periods of time. In
        this state it will apply any configuration information &
        initialize any parts of the hardware that must be done before
        it can be used for observing. For example: Framegrabber - take
        a dark frame; ACU - initialize encoders & check it is in
        remote mode but leave it in the shutdown state; Receivers & LO
        - tune to a nominal value and check phase locked loops are
        locked.  This function will also enable monitoring. This
        method will not change state and throw an <as-yet-undefined>
        exception if it cannot fully initialize the hardware.
        '''
        return self.__hw.hwInitialize();

    def hwOperational(self):
        '''
        Puts the hardware into the operational state. This can only be
        done if the hardware is in the initialize, diagnostic or
        simulation states and if it is in any other state a
        HwLifecycleEx exception will be thrown. The hwOperational
        method will normally not do anything when switching from the
        initialize state. However it will disable simulation when
        switching from simulation mode and disable access to some
        capabilities when switching from diagnostic mode.
        '''
        return self.__hw.hwOperational();

    def hwDiagnostic(self):
        '''
        Puts the hardware into the diagnostic state. This can only be
        done if the hardware is in the initialize, operational or
        simulation states and if it is in any other state a
        HwLifecycleEx exception will be thrown. The hwDiagnostic
        method may enable access to additional, potentially dangerous,
        functions on the hardware. Normal observing should not be done
        in this state. This function will throw an HwLifecycleEx
        exception if the device does not support a diagnostic mode.
        '''
        return self.__hw.hwDiagnostic();

    def hwSimulation(self):
        '''
        Puts the hardware into the simulation state. This can only be
        done if the hardware is in the initialize, operational or
        diagnostic states and if it is in any other state a
        HwLifecycleEx exception will be thrown. The hwSimulation
        method will substitute internally generated data for data
        normally generated by the hardware. However in this state
        there may still be some communication across the hardware
        interfaces (AMB, Ethernet etc.) for data that can normally be
        handled by a simulator. For example fake holography data might
        be generated internally when in this state but more routine
        monitor point data may be obtained through the AMB interfaces
        though the responses may come from the AMBLBSimulator. This
        function will throw an HwLifecycleEx exception if the device
        does not support a simulation mode.
        '''
        return self.__hw.hwSimulation();

    def hwStop(self):
        '''
        Puts the hardware into the stop state. This can be done
        irrespective of the state of the hardware and will not throw a
        HwLifecycleEx exception. The stop state will, if transitioning
        from the initialize, operational, diagnostic or simulation
        states attempt to put the hardware into a state where the
        power can be removed. It will also disable monitoring. If it
        cannot do any of this it will log an error message but
        continue through to the stop state. Once in the stop state
        there will be no communications with the hardware.
        '''
        return self.__hw.hwStop();

    def getDeviceCommunicationErrorCounter(self):
        '''
        Returns the number of failed CAN-bus communication attempts with the
        hardware.  Every successful communication attempt will reset the
        counter, every failed attempt will increment the counter until a maximum
        of 15 attempts is reached.  The device will then be put into the hwStop
        state.
        The maximum number of failed communication attempts can change during
        the development of this software.  Check the constructor in
        CONTROL/Common/ControlDevice/src/hardwareDeviceImpl.cpp for the current
        value.
        '''
        return self.__hw.getDeviceCommunicationErrorCounter()

    def clearDeviceCommunicationErrorAlarm(self):
        '''
        When the maximum number of failed CAN-bus communication attempts is reached
        a latched alarm will be raised. Indicating the component that was put in
        hwStop for this reason. The only way to clear this alarm is by a Full
        System Restart or by calling this method.

        Calling this method should be done with the understanding that the underlying
        cause that tripped the alarm in the first place has been addressed. 
        '''
        return self.__hw.clearDeviceCommunicationErrorAlarm()

    def inErrorState(self):
        '''
        Returns True if the device is in error state. Use getErrorMessage() to see
        more details.
        '''
        return self.__hw.inErrorState()

    def getErrorMessage(self):
        '''
        Returns the current error state message.
        '''
        return self.__hw.getErrorMessage()

    def isMonitoring(self):
        '''
        Returns True if the device properties are being monitored by the system.
        '''
        return self.__hw.isMonitoring()

