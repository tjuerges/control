#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# nbarriga  2008-01-29  created
#

def format(text, indent=2, fullwidth=70):
    width = fullwidth - indent
    out = []
    stack = [word for word in text.replace("\n", " ").split(" ") if word]
    while stack:
        line = ""
        while stack:
            if len(line) + len(" " + stack[0]) > width:
                if len(" " + stack[0])<= width:
                    break
            if line: line += " "
            line += stack.pop(0)
        out.append(" "*indent + line + " "*(fullwidth-len(line)-indent))
    return out

class ValueUnit:
    def __init__(self, value, unit = None, label = None):
        self.value = str(value)
        self.unit = str(unit)
        self.label = str(label)
    
    def out(self):
        output = ""
        if self.label != "None":
            output += self.label + ": "
        output += str(self.value)
        if self.unit != "None":
            output += "[" + self.unit + "]"
        return output
        
class Line:
    def __init__(self, label, val = None):# val should be a list of ValueUnit
        self.label = str(label)
        if val != None:
            self.val = val
        else:
            self.val = []
        

    def add(self, value):
        self.val.append(value)
        
    def out(self, width):
        output = self.label + " "*(width/3-len(self.label))
        #width2 = 2*width/3
        width2 = width - len(output)
        for i in range(len(self.val)):
            output += self.val[i].out() + " "*(width2/(len(self.val))-len(self.val[i].out()))
            #output += self.val[i].out() + " "*(width2/(len(self.val)+1)-len(self.label))
        
        return [output.ljust(width)]    

class Group:
    def __init__(self, label, children = None):#children should be a list of Line
        self.delimiter = " |_"
        self.label = str(label)
        if children != None:
            self.children =  children
        else:
            self.children = []

    def add(self, child):
        self.children.append(child)
    
    def out(self, width):
        output = [self.label.ljust(width)]
        for i in range(len(self.children)):
            childStr = self.children[i].out(width - 3)
            for j in range(len(childStr)):
                output.append(self.delimiter + childStr[j])
        return output

class Separator:
    def __init__(self, label = None): # The label is optional
        self.delimiter = "="
        self.label = str(label)

    def out(self, width):
        if self.label == None:
            output = [self.delimiter*width]
        else:
            half = (width - self.label.__len__() - 2 )/2
            output = [self.delimiter*half +' '+ self.label +' '+ self.delimiter*(width - half - self.label.__len__() - 2)]
        return output

class Area:
    def __init__(self, label, children = None):#children should be a list of Line and Group
        self.delimiter = ":"
        self.label = str(label)
        if children != None:
            self.children =  children
        else:
            self.children = []

    def add(self, child):
        self.children.append(child)

    def out(self, width):
        output = [self.delimiter*width]
        label = self.label.center(width-5)
        output.append((self.delimiter*4 + " " + label))

        output.append(self.delimiter*width)
            
        for i in range(len(self.children)):
            childStr = self.children[i].out(width - 5)
            for j in range(len(childStr)):
                output.append(self.delimiter*4 + " " + childStr[j])
        
        output.append(self.delimiter*width)
        return output

class Frame:
    def __init__(self, label, children = None):
        self.label = str(label)
        self.delimiter = "/"
        self.screen = 80
        if children != None:
            self.children =  children
        else:
            self.children = []

    def add(self, child):
        self.children.append(child)


    def out(self):
        output = [self.delimiter*self.screen]
        label = self.label.center(self.screen - 3)
        output.append(self.delimiter + " " + label + self.delimiter)

        output.append(self.delimiter*self.screen)
            
        for i in range(len(self.children)):
            if isinstance(self.children[i],Area):
                childStr = self.children[i].out(self.screen - 2)
                for j in range(len(childStr)):
                    output.append(self.delimiter + childStr[j] + self.delimiter)
            else:
                childStr = self.children[i].out(self.screen - 3)
                for j in range(len(childStr)):
                    output.append(self.delimiter + " " + childStr[j] + self.delimiter)

        output.append(self.delimiter*self.screen)
        return output
        
    def printScreen(self):
        text = self.out()
        for i in range(len(text)):
            print text[i]

class StatusHelper:
    def __init__(self):
        self.frame = "#"
        self.entries = []
        self.screen = 80
        self.title = ""
        
    def printStatus(self):
        if self.title != "":
            print self.frame*self.screen
            line = self.center(self.title, self.screen -2)
            for i in range(len(line)):
                print self.frame + line[i] + self.frame

        print self.frame*self.screen  #print top frame
        for i in range(len(self.entries)):
            fields = len(self.entries[i])
            text = []
            maxLen = 0
            for k in range(fields):
                if self.entries[i][k] == "":
                    self.entries[i][k] = "NULL"
                text.append(self.format(self.entries[i][k], 0, (self.screen - 2)/fields - 3))
                if len(text[k])>maxLen:
                    maxLen = len(text[k])
            for j in range(maxLen):
                line = self.frame + " "
                for k in range(fields):
                    if j < len(text[k]):
                        line += " " + text[k][j] + " " + self.frame
                    else:
                        line += " "*((self.screen - 2)/fields - 2) + self.frame
                print line
        print self.frame*self.screen     #print bottom frame       

    def setTitle(self, text):
        self.title = text
       
    def addOne(self, text):
        self.entries.append([str(text)])
    
    def addTwo(self, text, value):
        self.entries.append([str(text), str(value)])
    
    def addThree(self, text, value1, value2):
        self.entries.append([str(text), str(value1), str(value2)])
    
    def addFour(self, text, value1, value2, value3):
        self.entries.append([str(text), str(value1), str(value2), str(value3)])
    
    def addFive(self, text, value1, value2, value3, value4):
        self.entries.append([str(text),str(value1), str(value2), str(value3), str(value4)])
    
    def addTuple(self, tuple):
        strTuple = []
        for i in range(len(tuple)):
            strTuple.append(str(tuple[i]))
        self.entries.append(strTuple)
        
    def format(self, text, indent=2, fullwidth=70):
        '''
        Format a text block.

        This function formats a block of text. The text is broken into
        tokens. (Whitespace is NOT preserved.) The tokens are reassembled
        at the specified level of indentation and line width.  A tuple of
        strings is returned.

        Arguments:
        `text`   -- the string to be reformatted.
        `indent` -- the integer number of spaces to indent by.
        `width`  -- the maximum width of formatted text (including indent).
        '''
        width = fullwidth - indent
        out = []
        stack = [word for word in text.replace("\n", " ").split(" ") if word]
        while stack:
            line = ""
            while stack:
                if len(line) + len(" " + stack[0]) > width:
                    if len(" " + stack[0])<= width:
                        break
                if line: line += " "
                line += stack.pop(0)
            out.append(" "*indent + line + " "*(fullwidth-len(line)-indent))
        return out

    def center(self, text, width):
        '''
        Format a text block.

        This function formats a block of text. The text is broken into
        tokens. (Whitespace is NOT preserved.) The tokens are reassembled
        by reformating to line width and centering.  A tuple of
        strings is returned.

        Arguments:
        `text`   -- the string to be reformatted.
        `indent` -- the integer number of spaces to indent by.
        `width`  -- the maximum width of formatted text (including indent).
        '''
        out = []
        stack = [word for word in text.replace("\n", " ").split(" ") if word]
        while stack:
            line = ""
            while stack:
                if len(line) + len(" " + stack[0]) > width:
                    if len(" " + stack[0])<= width:
                        break
                if line: line += " "
                line += stack.pop(0)
            indent = (width - len(line))/2
            out.append(" "*indent + line + " "*(width-len(line)-indent))
        return out
        
#
# ___oOo___
