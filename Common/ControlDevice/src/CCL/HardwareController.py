#! /usr/bin/env python
#
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# $Id$
#


import CCL.Container
import Acspy.Common.QoS
import Acspy.Util.ACSCorba
import math
import Acspy.Common.Err


class HardwareController:
    def __init__(self, componentName, stickyFlag = False):
        '''
        The HardwareController class is a Python proxy to functions in the
        HardwareController interface. The component must already be
        running (usually started by the control master component)
        before creating this proxy.

        EXAMPLE:
        from HardwareController import *
        controller = HardwareController("CONTROL/DV01")
        state = controller.getState();
        controller.controllerOperational();
        controller.controllerShutdown();
        badSubdevices = controller.getBadSubdevices()
        del controller
        '''
        self.__stickyFlag = stickyFlag
        self.__componentName = componentName
        if self.__stickyFlag:
            self.__controller = CCL.Container.getComponent(self.__componentName)
        else:
            self.__controller = CCL.Container.getComponentNonSticky(self.__componentName)

    def __del__(self):
        '''
        Destructor
        '''
        if self.__stickyFlag:
            CCL.Container.releaseComponent(self.__componentName)

    def getState(self):
        '''
        Returns the state of the controller.
        '''
        return self.__controller.getState()

    def getErrorMessage(self):
        '''
        Returns the current error message.
        '''
        return self.__controller.getErrorMessage()

    def controllerOperational(self):
        '''
        Puts the controller into the operational state.
        '''
        self.__controller.controllerOperational()

    def controllerShutdown(self):
        '''
        Puts the controller into the stop state.
        '''
        self.__controller.controllerShutdown()

    def getBadSubdevices(self):
        '''
        Returns a list of subdevices which are not operational or in error state.
        Please refer to the device(s)'s getErrorMessage() method for further
        details.
        '''
        return self.__controller.getBadSubdevices()

    def clearSubdeviceError(self, device):
        '''
        Manually clear subdevice reported by getBadSubdevices().

        Calling this method should be done with the understanding that the underlying
        cause that tripped the error state in the first place has been addressed.
        This will NOT clear the device's error state.
        '''
        return self.__controller.clearSubdeviceError(device)

