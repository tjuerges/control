//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
//
// jkern     2005-07-30  created
// rhiriart  2005-09-15  Modified ControlDeviceImpl::getSubDeviceName()
//                       to return a sequence of strings.
//                       Modified functions to work with subDevice as
//                       a multimap.
//


#include <controlDeviceImpl.h>
#include <string>
#include <map>
#include <acscomponentImpl.h>
#include <baciCharacteristicComponentImpl.h>
#include <acsContainerServices.h>
#include <ControlExceptions.h>
#include <ControlDeviceExceptions.h>
#include <acsComponentSmartPtr.h>
#include <loggingMACROS.h>


// ToDO: Once we have the T-CDB a device could connect during initialization
//       and get the list of subdevices so the get subdevices method could
//       become private
namespace Control
{
    template< class componentInterfaceType, class acscomponentImpl >
    ControlDeviceImpl< componentInterfaceType, acscomponentImpl >::ControlDeviceImpl(
        const ACE_CString& name,
        maci::ContainerServices* containerServices):
        acscomponentImpl(name, containerServices),
        inErrorState_m(false)
    {
        ACS_TRACE(__PRETTY_FUNCTION__);
    }

    template< >
    ControlDeviceImpl< POA_Control::ControlDevice,
        baci::CharacteristicComponentImpl >::ControlDeviceImpl(
        const ACE_CString& name,
        maci::ContainerServices* containerServices):
        CharacteristicComponentImpl(name, containerServices, false),
        inErrorState_m(false)
    {
        ACS_TRACE(__PRETTY_FUNCTION__);
    }

    template< class componentInterfaceType, class acscomponentImpl >
    ControlDeviceImpl< componentInterfaceType, acscomponentImpl >::~ControlDeviceImpl()
    {
        ACS_TRACE(__PRETTY_FUNCTION__);
    }

    template< class componentInterfaceType, class acscomponentImpl >
    void ControlDeviceImpl< componentInterfaceType, acscomponentImpl >::cleanUp()
    {
        ACS_TRACE(__PRETTY_FUNCTION__);

        // release all subdevices
        releaseSubdevices();

        // call parent class cleanUp
        acscomponentImpl::cleanUp();
    }

    template< class componentInterfaceType, class acscomponentImpl >
    void ControlDeviceImpl< componentInterfaceType, acscomponentImpl >::setDeviceReferenceName(
        const DeviceConfig& config)
    {
        m_deviceReferenceName = config.Name;
    }

    template< class componentInterfaceType, class acscomponentImpl >
    char* ControlDeviceImpl< componentInterfaceType, acscomponentImpl >::getDeviceReferenceName()
    {
        std::ostringstream msg;
        msg << "Device reference name: "
            << m_deviceReferenceName;
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());

        return CORBA::string_dup(m_deviceReferenceName.c_str());
    }


    template< class componentInterfaceType, class acscomponentImpl >
    std::map< std::string, Control::ControlDevice_ptr >::const_iterator
    ControlDeviceImpl< componentInterfaceType, acscomponentImpl >::findLruTypeAndCheckIfNil(
        const std::string& lruType)
    {
        const std::map< std::string, Control::ControlDevice* >::const_iterator
            iter(subdevice_m.find(lruType));

        if(iter == subdevice_m.end())
        {
            // Could not find an "lruType" in the list of subdevices.  This is
            // not good and reason enough to throw an exception.
            ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);

            std::ostringstream output;
            output << "A request for the LRU type "
                << lruType
                << " reference cannot be fulfilled because there is no "
                    "the list of configured subdevices.";
            LOG_TO_DEVELOPER(LM_ERROR, output.str());
            ex.addData("Detail", output.str());
            throw ex;
        }

        if(CORBA::is_nil((*iter).second) == true)
        {
            // The "lruType" reference is invalid.  Bad again, throw an
            // exception and let others deal with the mess.
            ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);

            std::ostringstream output;
            output << "The subdevice which is configured as "
                << lruType
                << " has a nil reference.";
            LOG_TO_DEVELOPER(LM_ERROR, output.str());
            ex.addData("Detail", output.str());
            throw ex;
        }

        return iter;
    }

    template< class componentInterfaceType, class acscomponentImpl >
    void ControlDeviceImpl< componentInterfaceType, acscomponentImpl >::releaseSubdevices()
    {
        ACS_TRACE(__PRETTY_FUNCTION__);

        for(std::map< std::string, Control::ControlDevice_ptr >::iterator iter(
            subdevice_m.begin()); iter != subdevice_m.end(); ++iter)
        {
            if(CORBA::is_nil((*iter).second) == false)
            {
                try
                {
                    (*iter).second->releaseSubdevices();
                    CORBA::String_var tmpName((*iter).second->name());
                    acscomponent::ACSComponentImpl::getContainerServices()->
                        releaseComponent(tmpName.in());
                }
                catch(...)
                {
                    CORBA::String_var tmpName((*iter).second->name());
                    std::ostringstream msg;
                    msg << "Error narrowing component \""
                        << tmpName.in()
                        << "\" to ACSComponent.";
                    LOG_TO_DEVELOPER(LM_ERROR, msg.str());
                }
            }
        }

        subdevice_m.clear();
    }

    template< class componentInterfaceType, class acscomponentImpl >
    void ControlDeviceImpl< componentInterfaceType, acscomponentImpl >::setParent(
        const char* parentName)
    {
        if(parentName == NULL || std::strlen(parentName) == 0)
        {
            const std::string msg(
                "Invalid parent name (either NULL or Zero Length).");
            ControlExceptions::IllegalParameterErrorExImpl newEx(
                __FILE__, __LINE__, __func__);
            newEx.addData("Detail", msg);
            LOG_TO_DEVELOPER(LM_ERROR, msg);
            // newEx.log();
            //    throw newEx.getIllegalParameterErrorEx();
            return;
        }

        try
        {
            maci::SmartPtr< Control::ControlDevice > parentRef(
                acscomponent::ACSComponentImpl::getContainerServices()->
                    getComponentNonStickySmartPtr< Control::ControlDevice >(
                        parentName));
            parentStack_m.push(parentRef);
        }
        catch(const maciErrType::CannotGetComponentExImpl& ex)
        {
            const std::string msg("Parent reference is null. Is your "
                "configuration correct?");
            maciErrType::CannotGetComponentExImpl newEx(ex, __FILE__, __LINE__,
                __func__);
            newEx.addData("Detail", msg);
            newEx.log();
            throw newEx.getCannotGetComponentEx();
        }
    }

    template< class componentInterfaceType, class acscomponentImpl >
    maci::SmartPtr< Control::ControlDevice > ControlDeviceImpl<
        componentInterfaceType, acscomponentImpl >::getParentReference()
    {
        if(parentStack_m.empty())
        {
            maci::SmartPtr< Control::ControlDevice > nilReference;
            return nilReference;
        }
        else
        {
            return parentStack_m.top();
        }
    }

    template< class componentInterfaceType, class acscomponentImpl >
    void ControlDeviceImpl< componentInterfaceType, acscomponentImpl >::releaseParent()
    {
        if(parentStack_m.empty() == false)
        {
            parentStack_m.pop();
        }
    }

    template< class componentInterfaceType, class acscomponentImpl >
    void ControlDeviceImpl< componentInterfaceType, acscomponentImpl >::getSubdeviceReference(
        const Control::DeviceConfig& configuration)
    {
        ACS_TRACE(__PRETTY_FUNCTION__);

        if(subdevice_m.count(configuration.Name.in()))
        {
            ControlDeviceExceptions::IllegalConfigurationExImpl ex(__FILE__,
                __LINE__, __PRETTY_FUNCTION__);
            std::ostringstream msg;
            msg << "Duplicated key detected ("
                << std::string(configuration.Name)
                << ").";
            ex.addData("Reason", msg.str());
            ex.log();
            throw(ex);
        }

        // Build the full name for the new device.
        const std::string configurationName(configuration.Name);
        CORBA::String_var tmpName(acscomponentImpl::name());
        const std::string fullName(std::string(tmpName.in()) + "/"
            + configurationName);

        try
        {
            subdevice_m[configurationName] =
                acscomponent::ACSComponentImpl::getContainerServices()->
                    getComponent< Control::ControlDevice >(fullName.c_str());
        }
        catch(const maciErrType::CannotGetComponentExImpl& _ex)
        {
            std::ostringstream msg;
            msg << "Failed to get the reference to "
                << fullName
                << "component";
            LOG_TO_DEVELOPER(LM_ERROR, msg.str());

            subdevice_m[configurationName] = Control::ControlDevice::_nil();
            return;
        }

        if(CORBA::is_nil(subdevice_m[configurationName]) == true)
        {
            std::ostringstream msg;
            msg << "Component called "
                << fullName
                << "does not implement the ControlDevice interface.";
            LOG_TO_DEVELOPER(LM_ERROR, msg.str());
            return;
        }

        std::ostringstream msg;
        msg << "Reference to "
            << fullName
            << "component succesfully retrieved.";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());

        // Recurse to the next level
        try
        {
            subdevice_m[configurationName]->createSubdevices(configuration,
                tmpName.in());
        }
        catch(const ControlDeviceExceptions::IllegalConfigurationEx& ex)
        {
            throw ControlDeviceExceptions::IllegalConfigurationExImpl(ex,
                __FILE__, __LINE__, __PRETTY_FUNCTION__);
        }
    }

    template< class componentInterfaceType, class acscomponentImpl >
    DeviceNameList* ControlDeviceImpl< componentInterfaceType, acscomponentImpl >::getSubdeviceList()
    {
        ACS_TRACE(__PRETTY_FUNCTION__);

        typedef std::map<
            std::string, Control::ControlDevice* >::const_iterator CI;

        DeviceNameList_var nameList(new DeviceNameList(subdevice_m.size()));
        nameList->length(subdevice_m.size());

        unsigned int idx(0U);
        for(CI iter(subdevice_m.begin()); iter != subdevice_m.end(); ++iter)
        {
            nameList[idx].ReferenceName = (*iter).first.c_str();
            if(CORBA::is_nil((*iter).second) == true)
            {
                nameList[idx].FullName = "";
            }
            else
            {
                try
                {
                    ACS::ACSComponent_var acsComp(ACS::ACSComponent::_narrow(
                        (*iter).second));
                    if(CORBA::is_nil(acsComp) == false)
                    {
                        CORBA::String_var tmpName(acsComp->name());
                        nameList[idx].FullName = tmpName.in();
                    }
                }
                catch(...)
                {
                    std::ostringstream msg;
                    msg << "Error narrowing component \""
                        << (*iter).second
                        << "\" to ACSComponent.";
                    LOG_TO_DEVELOPER(LM_ERROR, msg.str());
                }
            }

            ++idx;
        }

        return nameList._retn();
    }

    template< class componentInterfaceType, class acscomponentImpl >
    char* ControlDeviceImpl< componentInterfaceType, acscomponentImpl >::getSubdeviceName(
        const char* refName)
    {
        ACS_TRACE(__PRETTY_FUNCTION__);

        std::map< std::string, Control::ControlDevice* >::iterator iter(
            subdevice_m.find(std::string(refName)));

        if(iter == subdevice_m.end())
        {
            // Name was not found
            ControlExceptions::IllegalParameterErrorExImpl ex(__FILE__,
                __LINE__, __PRETTY_FUNCTION__);
            ex.addData("Detail", refName);
            throw ex.getIllegalParameterErrorEx();
        }

        if(CORBA::is_nil((*iter).second) == true)
        {
            CORBA::String_var compName(CORBA::string_dup(""));
            return compName._retn();
        }

        CORBA::String_var compName;
        try
        {
            ACS::ACSComponent_var acsComp(ACS::ACSComponent::_narrow(
                (*iter).second));
            if(CORBA::is_nil(acsComp.in()) == false)
            {
                CORBA::String_var tmpName(acsComp->name());
                compName = tmpName.in();
            }
        }
        catch (...)
        {
            std::ostringstream msg;
            msg << "Error narrowing component \""
                << (*iter).second
                << "\" to ACSComponent.";
            LOG_TO_DEVELOPER(LM_ERROR, msg.str());
            compName = CORBA::string_dup("");
        }

        return compName._retn();
    }

    template< class componentInterfaceType, class acscomponentImpl >
    CORBA::Boolean ControlDeviceImpl< componentInterfaceType, acscomponentImpl >::inErrorState()
    {
        return (inErrorState_m);
    }

    template< class componentInterfaceType, class acscomponentImpl >
    char* ControlDeviceImpl< componentInterfaceType, acscomponentImpl >::getErrorMessage()
    {
        CORBA::String_var rv(CORBA::string_dup(errorMessage_m.c_str()));
        return rv._retn();
    }

    template< class componentInterfaceType, class acscomponentImpl >
    void ControlDeviceImpl< componentInterfaceType, acscomponentImpl >::setError(
        const std::string& errorDescription)
    {
        ACS_TRACE(__PRETTY_FUNCTION__);

        if(inErrorState_m == false)
        {
            inErrorState_m = true;
            errorMessage_m = errorDescription;

            LOG_TO_DEVELOPER(LM_WARNING, errorDescription);

            // Report up the stack
            maci::SmartPtr< Control::ControlDevice > parentRef =
                getParentReference();
            if(parentRef.isNil() == false)
            {
                CORBA::String_var tmpName(acscomponentImpl::name());
                parentRef->setSubdeviceError(tmpName.in());
            }
        }
        else
        {
            // we save the last errorDescritpion
            errorMessage_m = errorDescription;
        }
    }

    template< class componentInterfaceType, class acscomponentImpl >
    void ControlDeviceImpl< componentInterfaceType, acscomponentImpl >::clearError()
    {
        ACS_TRACE(__PRETTY_FUNCTION__);

        errorMessage_m.clear();
        inErrorState_m = false;

        // Report it up the stack
        maci::SmartPtr< Control::ControlDevice > parentRef(
            getParentReference());
        if(parentRef.isNil() == false)
        {
            CORBA::String_var tmpName(acscomponentImpl::name());
            parentRef->clearSubdeviceError(tmpName.in());
        }
    }

    template< class componentInterfaceType, class acscomponentImpl >
    std::string ControlDeviceImpl< componentInterfaceType, acscomponentImpl >::componentToAntennaName(
        const std::string& compName)
    {
        std::string antName("Unknown");
        std::string::size_type startPos(compName.find("/"));
        if(startPos != std::string::npos)
        {
            ++startPos;
            if(startPos < compName.size())
            {
                const std::string::size_type endPos(
                    compName.find("/", startPos));
                if(endPos != std::string::npos)
                {
                    antName = compName.substr(startPos, endPos - startPos);
                }
            }
        }

        return antName;
    }

    template< class componentInterfaceType, class acscomponentImpl >
    const std::string ControlDeviceImpl<
        componentInterfaceType, acscomponentImpl >::getAlarmFamilyName(
            const std::string& baseName)
    {
        CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());
        std::string alarmFamilyName(tmpName.in());
        //
        // The assumption is here that the device instance name, i.e. the alarm
        // family name, is always the remaining string after the last '/' in
        // the component name.
        //
        // The alarm family name string looks like:
        // - "ML" if the device itself is
        //   "CONTROL/CentalLO/ML".
        // - "CRD" if the device itself is
        //   "CONTROL/CentralLO/PhotonicReference3/CRD".
        // - "DTXBBPr2" if the device itself is
        //   "CONTROL/DV01/DTXBBPr2".
        // - "WCA7" if the device itself is
        //   "CONTROL/DA41/FrontEnd/WCA7".
        //
        // Real world example:
        // baseName = DTX
        // tmpName = CONTROL/DA41/DTXBBpr1
        // -> alarmFamilyName = DTXBBpr1

        const std::string searchFor("/");
        const std::string::size_type pos(
            alarmFamilyName.rfind(searchFor));
        if(pos == std::string::npos)
        {
            // Here is something really wrong but there is nothing I can do
            // about it.  Log it as exception, perhaps somebody pays attention.
            ControlDeviceExceptions::IllegalConfigurationExImpl ex(__FILE__,
                __LINE__, __PRETTY_FUNCTION__);
            std::ostringstream msg;
            msg << "Could not identify the alarm system family name.  The "
                "provided base name ("
                << baseName
                << ") could not be found in the component name ("
                << alarmFamilyName
                << ").  Assuming the base name as the alarm system family "
                    "name.";
            ex.addData("Detail", msg.str());
            ex.log();

            return baseName;
        }
        else
        {
            // The instance name is everything from pos + 1 until the end
            // of the component name.
            alarmFamilyName = alarmFamilyName.substr(pos + 1);
        }

        return alarmFamilyName;
    }

    template< class componentInterfaceType, class acscomponentImpl >
    const std::string ControlDeviceImpl<
        componentInterfaceType, acscomponentImpl >::getAlarmMemberName()
    {
        CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());
        std::string alarmMemberName(tmpName.in());
        // The alarm member name string looks like:
        // - "CentralLO" if the device itself is
        //   "CONTROL/CentalLO/ML".
        // - "PhotonicReference3" if the device itself is
        //   "CONTROL/CentralLO/PhotonicReference3/CRD".
        // - "DV01" if the device itself is
        //   "CONTROL/DV01/LORR".
        // - "DA41" if the device itself is
        //   "CONTROL/DA41/FrontEnd/WCA7".
        //
        // Real world example:
        // tmpName = CONTROL/DA41/DTXBBpr1
        // -> alarmMemberName = DA41

        // Remove "CONTROL/" from the front.
        std::string searchFor("CONTROL/");
        std::string::size_type pos(alarmMemberName.find_first_of(searchFor));
        if(pos == 0)
        {
            alarmMemberName = alarmMemberName.erase(pos, searchFor.size());
        }

        // If the name contains more than one "/", then check for special
        // cases below.
        if(std::count(
            alarmMemberName.begin(), alarmMemberName.end(), '/') == 1)
        {
            // Handle here the case with only a lonely "/".  The member name is
            // the string preceeding the "/". This is covers for:
            // - "CONTROL/CentralLO" -> "CentalLO/ML"
            //   "CONTROL/DV01/LORR" -> "DV01"
            searchFor = "/";
            pos = alarmMemberName.find(searchFor);
            alarmMemberName = alarmMemberName.erase(pos);

            // That was quick.  Done!
            return alarmMemberName;
        }

        // Cases remaining with multiple occurences of "/":
        // - "CentralLO/PhotonicReference3/CRD" -> "PhotonicReference3"
        // - "DA41/FrontEnd/WCA7" -> "DA41"

        // Check if it is a FrontEnd device.
        searchFor = "/FrontEnd/";
        pos = alarmMemberName.find(searchFor);
            if(pos != std::string::npos)
            {
            // This is a FrontEnd subdevice. The Parent String is the one
            // in front of the "/FrontEnd/" string.
            // - "DA41/FrontEnd/WCA7" -> "DA41"
            alarmMemberName = alarmMemberName.erase(pos);

            // That was quick, too.  Done.
            return alarmMemberName;
        }

        // Cases remaining with multiple occurences of "/":
        // - "CentralLO/PhotonicReference3/CRD" -> "PhotonicReference3"
        // The parent is the one between the two "/"s.
        searchFor = "/";
        pos = alarmMemberName.find_first_of(searchFor) + 1;
        alarmMemberName = alarmMemberName.substr(pos,
                alarmMemberName.find_last_of(searchFor) - pos);

        return alarmMemberName;
    }


    template class ControlDeviceImpl< POA_Control::ControlDevice,
        baci::CharacteristicComponentImpl >;
};
