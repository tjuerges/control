// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#if !defined(COMPRESSOR_H)
#define COMPRESSOR_H

#include "AMBDevice.h"
#include "CANError.h"
#include <string>

namespace AMB {

    // Valid for draft Compressor ICD dated 2000-NOV-05
    // TODO - simulate some errors, time-variable values.
    class Compressor : public AMB::Device
    {
    public:
	// After construction, the compressor and refrigerator are in the off
	// state.
	Compressor(node_t node, const std::vector<CAN::byte_t> &serialNumber);
	virtual ~Compressor();

	virtual node_t node() const;
	virtual std::vector<CAN::byte_t> serialNumber() const;

	virtual std::vector<CAN::byte_t> monitor(rca_t rca) const;
	virtual void control(rca_t rca, const std::vector<CAN::byte_t> &data);

	// CAN Generic MC points.

	/// Take the default implementation = return 0,0,0,0
	virtual std::vector<CAN::byte_t>  get_can_error() const;

	/// Take the default implementation = return 0,0,0
	virtual std::vector<CAN::byte_t>  get_protocol_rev_level() const;

	/// Return the number of transactions
	virtual unsigned int get_trans_num() const;

	/// Temperature on M&C interface board. 18.0 constant.
	float get_ambient_temperature() const;

	// Compressor specific monitor points.

	/// Return the current state of the compressor (1=running, 0=off).
	CAN::byte_t get_comp_state() const;

	/// 40 if the compressor is on, 20 if off.
	float get_control_box_temp() const;

	/// 2.5Amps if the fridge is on, 0.0 otherwise.
	float get_crosshead_current() const;

	/// 41 if the compressor is on, 21 if off.
	float get_elec_cage_temp() const;

	/// 42 if the compressor is on, 22 if off.
	float get_fan_air_temp() const;

	/// 43 if the compressor is on, 23 if off.
	float get_fan_motor_temp() const;

	/// Return current fridge state (1=running, 0=off).
	CAN::byte_t get_fridge_drive_state() const;

	/// 44 if the compressor is on, 24 if off.
	float get_gm_head_temp() const;

	/// 45 if the compressor is on, 25 if off.
	float get_gm_htex_temp() const;

	/// 46 if the compressor is on, 26 if off.
	float get_gm_oil_temp() const;

	/// 47 if the compressor is on, 27 if off.
	float get_gm_oilhex_temp() const;

	/// 280 if on, 200 if off.
	float get_gm_return_pressure() const;

	/// 280 if on, 200 if off.
	float get_gm_supply_pressure() const;

	/// 48 if the compressor is on, 28 if off.
	float get_jt_head_temp() const;

	/// 49 if the compressor is on, 29 if off.
	float get_jt_oil_temp() const;

	/// 50 if the compressor is on, 30 if off.
	float get_jt_oilhex_temp() const;

	/// -10.5 if on, 100 if off.
	float get_jt_return_pressure() const;

	/// 120 if on, 180 if off.
	float get_res_tank_pressure() const;

	/// Status of the various switches in the compressor.
	std::vector<CAN::byte_t> get_status() const;

	// Controls

	/// Turn on (state=1) or off (state=0) the compressor.
	void set_comp_state(CAN::byte_t state);

	/// Turn on (state=1) or off (state=0) the fridge.
	void set_fridge_state(CAN::byte_t state);

	// Throw this if something bad happens.
	class CompressorError : public CAN::Error {
	public:
	    CompressorError(const std::string &error) : CAN::Error(error) {;}
	};

	enum Monitor_t {
	    // Generic monitor points
	    GET_CAN_ERROR           = AMB::Device::GET_CAN_ERROR,
	    GET_PROTOCOL_REV_LEVEL  = AMB::Device::GET_PROTOCOL_REV_LEVEL,
	    GET_TRANS_NUM           = AMB::Device::GET_TRANS_NUM,
	    GET_AMBIENT_TEMPERATURE = AMB::Device::GET_AMBIENT_TEMPERATURE,

	    // Monitor points peculiar to the helium compressor (from the
	    // ICD).
	    GET_COMP_STATE          = 0x00000006,
	    GET_CONTROL_BOX_TEMP    = 0x00000013,
	    GET_CROSSHEAD_CURRENT   = 0x00000008,
	    GET_ELEC_CAGE_TEMP      = 0x00000012,
	    GET_FAN_AIR_TEMP        = 0x00000010,
	    GET_FAN_MOTOR_TEMP      = 0x00000011,
	    GET_FRIDGE_DRIVE_STATE  = 0x00000007,
	    GET_GM_HEAD_TEMP        = 0x00000009,
	    GET_GM_HTEX_TEMP        = 0x0000000A,
	    GET_GM_OIL_TEMP         = 0x0000000B,
	    GET_GM_OILHEX_TEMP      = 0x0000000F,
	    GET_GM_RETURN_PRESSURE  = 0x00000002,
	    GET_GM_SUPPLY_PRESSURE  = 0x00000003,
	    GET_JT_HEAD_TEMP        = 0x0000000C,
	    GET_JT_OIL_TEMP         = 0x0000000D,
	    GET_JT_OILHEX_TEMP      = 0x0000000E,
	    GET_JT_RETURN_PRESSURE  = 0x00000001,
	    GET_RES_TANK_PRESSURE   = 0x00000004,
	    GET_STATUS              = 0x00000005
	};

	enum Controls_t {
	    SET_COMP_STATE          = 0x00001001,
	    SET_FRIDGE_STATE        = 0x00001002
	};

    private:
	// Undefined and inaccessible - copying does not make sense.
	Compressor(const Compressor &);
	Compressor &operator=(const Compressor &);

	/// Bus location (node number).
	node_t node_m;
	/// Serial number of this node.
	std::vector<CAN::byte_t> sn_m;

	/// State of the compressor.
	CAN::byte_t comp_state_m;

	/// State of the fridge.
	CAN::byte_t fridge_state_m;

	/// Number of transactions.
	mutable unsigned int trans_num_m;

    };  // class Compressor

}  // namespace AMB

#endif  // COMPRESSOR_H
