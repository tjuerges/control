// @(#) $Id$
//
// Copyright (C) 2002, 2004
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#if !defined(DIGICLOCK_H)
#define DIGICLOCK_H

//#define WRITE_LOG


#include "AMBDevice.h"
#include "CANError.h"
#include <string>
#include <stdio.h>

namespace AMB
{
  /** The LORR class is a simulator for the local oscillator receiver reference 
   */
  class DigiClock : public AMB::Device
  {
  public:
    /** Constructor
     ** @param node Node ID of this device
     ** @param serialNumber S/N of this device
     */
    DigiClock(node_t node, const std::vector<CAN::byte_t>& serialNumber);

    /// Destructor
    virtual ~DigiClock();
    
    /// Return the device's node ID
    virtual node_t node() const;

    /// Return the device's S/N
    virtual std::vector<CAN::byte_t> serialNumber() const;
    
    /** Monitor interface
     ** @param CAN node RCA
     ** @return std::vector of bytes containing monitor info. Each response is
     ** sized to what's specified in the ICD.
     */
    virtual std::vector<CAN::byte_t> monitor(rca_t rca) const;
    
    /** Control Interface ('set' functions)
     ** @param rca CAN node RCA
     ** @param data CAN message data required by device
     */
    virtual void control(rca_t rca, const std::vector<CAN::byte_t>& data);
    
    /// Monitor points for the LORR
    enum Monitor_t {
      /// Generic monitor points
      //@{
      GET_SERIAL_NUMBER       = 0x00000,
      GET_CAN_ERROR           = AMB::Device::GET_CAN_ERROR,
      GET_PROTOCOL_REV_LEVEL  = AMB::Device::GET_PROTOCOL_REV_LEVEL,
      GET_TRANS_NUM           = AMB::Device::GET_TRANS_NUM,
      GET_AMBIENT_TEMPERATURE = AMB::Device::GET_AMBIENT_TEMPERATURE,
      //@}
      
      /// Specific DigiClock data point monitors
      //@{
      LAST_PHASE_COMMAND     = 0x00061,
      CURRENT_PHASE          = 0x00062,
      DGCK_STATUS            = 0x00063,
      MISSED_COMMAND         = 0x00064,
      LOCK_INDICATOR_VOLTAGE = 0x00070,
      PS_VOLTAGE_CLOCK       = 0x00071
      //@}
    };

    /// Control Functions for the DigiClock
    enum Controls_t {
      PHASE_COMMAND                = 0x00081,
      PLL_LOCK_RESET               = 0x00083,
      MISSED_COMMAND_RESET         = 0x00084
    };
    
    // Throw this if something bad happens.
    class DigiClckError : public CAN::Error {
    public:
      DigiClckError(const std::string &error) : CAN::Error(error) {;}
    };
    
    /// Get functions for unit testing
    //@{
      CAN::byte_t* getLAST_PHASE_COMMAND();
      CAN::byte_t* getCURRENT_PHASE();
      CAN::byte_t  getDGCK_STATUS() const;
      CAN::byte_t  getMISSED_COMMAND() const;
      double getLOCK_INDICATOR_VOLTAGE() const;
      double getPS_VOLTAGE_CLOCK() const;
    
  private:
    /// Take the default implementation = return 0,0,0,0
    virtual std::vector<CAN::byte_t>  get_can_error() const;
    
    /// Take the default implementation = return 0,0,0
    virtual std::vector<CAN::byte_t>  get_protocol_rev_level() const;
    
    /// Return the number of transactions
    virtual unsigned int get_trans_num() const;
    
    /// Temperature on M&C interface board.
    double get_ambient_temperature() const;
    
    // LORR Measurement Equipment specific monitor points.    
    std::vector<CAN::byte_t> CAN_LAST_PHASE_COMMAND() const;
    std::vector<CAN::byte_t> CAN_CURRENT_PHASE() const;
    std::vector<CAN::byte_t> CAN_DGCK_STATUS() const;
    std::vector<CAN::byte_t> CAN_MISSED_COMMAND() const;
    std::vector<CAN::byte_t> CAN_LOCK_INDICATOR_VOLTAGE() const;
    std::vector<CAN::byte_t> CAN_PS_VOLTAGE_CLOCK() const;
    
    /** Helper function for 'set' commands to check that data size
     ** is valid, if not throw exception
     ** @param data Vector of bytes containing data to check
     ** @param size Expected number of bytes in data std::vector
     ** @param pExceptionMsg String identifying CAN msg that is
     ** associated with failed size check.
     */
    void checkSize( const std::vector<CAN::byte_t> &data, int size,
		    const char *pExceptionMsg);
    
    /// \name Undefined and inaccessible - copying does not make sense.
    //@{
    DigiClock (const DigiClock &);
    DigiClock &operator=(const DigiClock &);
    //@}
    
    /// Bus location (node number).
    node_t m_node;

    /// Serial number of this node.
    std::vector<CAN::byte_t> m_sn;
    
    /// Number of transactions. Needs to be mutable as its incremented by the
    /// monitor function, which is const.
    mutable unsigned int m_trans_num;
    // values for all the monitor points
    CAN::byte_t m_errorCode;

    // This is the last phase command sent
    CAN::byte_t LAST_PHASE_COMMAND_m[8];

    CAN::byte_t DGCK_STATUS_m;
    CAN::byte_t MISSED_COMMAND_m;
    double      LOCK_INDICATOR_VOLTAGE_m;;
    double      PS_VOLTAGE_CLOCK_m;

    FILE*       outputFile;
  };  // class DigiClock
}  // namespace AMB

#endif  // DIGICLOCK_H
