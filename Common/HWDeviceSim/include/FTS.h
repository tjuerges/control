// @(#) $Id$
//
// Copyright (C) 2004
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Library General Public License
// as published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Library General Public License for more details.
//
// You should have received a copy of the GNU Library General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA. 
// Correspondence concerning ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#if !defined(FTS_H)
#define FTS_H

#include "AMBDevice.h"
#include "CANError.h"
#include <string>

//#define CREATE_LOG_FILE

namespace AMB
{
  /** The FTS class is a simulator for the fine tuning synthesizer device.
   */
  class FTS : public AMB::Device
  {
  public:
    /** Constructor
     ** @param node Node ID of this device
     ** @param serialNumber S/N of this device
     */
    FTS(node_t node, const std::vector<CAN::byte_t>& serialNumber,
        rca_t offset = 0);

    /// Destructor
    virtual ~FTS();
    
    /// Return the device's node ID
    virtual node_t node() const;

    /// Return the device's S/N
    virtual std::vector<CAN::byte_t> serialNumber() const;
    
    /** Monitor interface
     ** @param CAN node RCA
     ** @return std::vector of bytes containing monitor info. Each response is
     ** sized to what's specified in the ICD.
     */
    virtual std::vector<CAN::byte_t> monitor(rca_t rca) const;
    
    /** Control Interface ('set' functions)
     ** @param rca CAN node RCA
     ** @param data CAN message data required by device
     */
    virtual void control(rca_t rca, const std::vector<CAN::byte_t>& data);
    
    /// Monitor points for the FTS
    enum Monitor_t {
      /// Generic monitor points
      //@{
      GET_SERIAL_NUMBER       = 0x00000,
      GET_CAN_ERROR           = AMB::Device::GET_CAN_ERROR,
      GET_PROTOCOL_REV_LEVEL  = AMB::Device::GET_PROTOCOL_REV_LEVEL,
      GET_TRANS_NUM           = AMB::Device::GET_TRANS_NUM,
      GET_AMBIENT_TEMPERATURE = AMB::Device::GET_AMBIENT_TEMPERATURE,
      //@}
      
      /// Specific FTS data point monitors
      //@{
      GET_FREQ = 0x1,
      GET_PHASE_VALS = 0x2,
      GET_PHASE_SEQ1 = 0x3,
      GET_PHASE_SEQ2 = 0X4,
      GET_VOLTAGE_STATUS = 0x6,
      GET_PHASE_OFFSET = 0x7
      //@}
    };

    /// Control points for the FTS
    enum Controls_t {
      SET_FREQ = 0x81,
      SET_PHASE_VALS = 0x82,
      SET_PHASE_SEQ1 = 0x83,
      SET_PHASE_SEQ2 = 0x84,
      RESTART = 0x85,
      RESET_VOLTAGE_STATUS = 0x86,
      SET_PHASE_OFFSET = 0x87
    };
    
    // Throw this if something bad happens.
    class FTSError : public CAN::Error {
    public:
      FTSError(const std::string &error) : CAN::Error(error) {;}
    };
    
  protected:
    /// Take the default implementation = return 0,0,0,0
    virtual std::vector<CAN::byte_t>  get_can_error() const;
    
    /// Take the default implementation = return 0,0,0
    virtual std::vector<CAN::byte_t>  get_protocol_rev_level() const;
    
    /// Return the number of transactions
    virtual unsigned int get_trans_num() const;
    
    /// Temperature on M&C interface board.
    double get_ambient_temperature() const;
    
    // FTS specific monitor points.    
    std::vector<CAN::byte_t> CAN_GET_FREQ() const;
    std::vector<CAN::byte_t> CAN_GET_PHASE_VALS() const;
    std::vector<CAN::byte_t> CAN_GET_PHASE_SEQ1() const;
    std::vector<CAN::byte_t> CAN_GET_PHASE_SEQ2() const;
    std::vector<CAN::byte_t> CAN_GET_VOLTAGE_STATUS() const;
    std::vector<CAN::byte_t> CAN_GET_PHASE_OFFSET() const;

    // FTS specific control points
    void CAN_SET_FREQ(const std::vector<CAN::byte_t>& data);
    void CAN_SET_PHASE_VALS(const std::vector<CAN::byte_t>& data);
    void CAN_SET_PHASE_SEQ1(const std::vector<CAN::byte_t>& data);
    void CAN_SET_PHASE_SEQ2(const std::vector<CAN::byte_t>& data);
    void CAN_RESTART(const std::vector<CAN::byte_t>& data);
    void CAN_RESET_VOLTAGE_STATUS(const std::vector<CAN::byte_t>& data);
    void CAN_SET_PHASE_OFFSET(const std::vector<CAN::byte_t>& data);

    /** Helper function for 'set' commands to check that data size
     ** is valid, if not throw exception
     ** @param data Vector of bytes containing data to check
     ** @param size Expected number of bytes in data std::vector
     ** @param pExceptionMsg String identifying CAN msg that is
     ** associated with failed size check.
     */
    void checkSize( const std::vector<CAN::byte_t> &data, int size,
		    const char *pExceptionMsg);
    
    /// \name Undefined and inaccessible - copying does not make sense.
    //@{
    FTS (const FTS &);
    FTS &operator=(const FTS &);
    //@}
    
    /// Bus location (node number).
    node_t m_node;

    /// Serial number of this node.
    std::vector<CAN::byte_t> m_sn;

    /// Offset for RCA's (used by SecondLO)
    rca_t offset_m;

    /// Number of transactions. Needs to be mutable as its incremented by the
    /// monitor function, which is const.
    mutable unsigned int m_trans_num;
    // values for all the monitor points
    CAN::byte_t m_errorCode;
    std::vector<CAN::byte_t> FREQ_m;
    std::vector<CAN::byte_t> PHASE_VALS_m;
    unsigned long long       PHASE_SEQ1_m;
    unsigned long long       PHASE_SEQ2_m;
    unsigned char            VOLTAGE_STATUS_m;
    std::vector<CAN::byte_t> PHASE_OFFSET_m;

    FILE* logFile;
  };  // class FTS
}  // namespace AMB

#endif  // FTS_H
