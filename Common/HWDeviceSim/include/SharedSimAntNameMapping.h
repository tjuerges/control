#ifndef SHAREDSIMANTNAMEMAPPING_H
#define SHAREDSIMANTNAMEMAPPING_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2005 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* rsoto   2005-09-20  created
*/

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <string>

/**
 * This class is just a wrapper for converting 
 * from component name to the corresponding antenna 
 * number
 */

class SharedSimAntNameMapping {
 public:
  SharedSimAntNameMapping();
  ~SharedSimAntNameMapping();

  int getAntennaID(const std::string &compName);
  
};

#endif /*!SHAREDSIMANTNAMEMAPPING_H*/
