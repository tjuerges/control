// @(#) @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#if !defined(AMB_DEVICE_H)
#define AMB_DEVICE_H

#include <vector>
#include "CANTypes.h"

#include <SharedSimInterface.h>

namespace AMB {

    /** Device is the base class from which all AMB simulators are derived.

	Each simulated device must provide a node and serial number. Typically 
	they will merely be constructor parameters that are returned 
	thereafter. The node number must be less than or equal to 2031, and 
	the serial number must be 8 (binary) characters.

	The fundamental methods are monitor() and control(). Both of these 
	functions takes a RCA (Relative Can Address) which should be thought 
	of as the "id" of the actual monitor or control	point the caller is 
	interested in. Typically the monitor/control method will have a switch 
	statement that calls the actual method that does the simulation. A 
	useful way to partition the functionality is for the monitor/control
	function to unpack/pack the data (into a CAN::vector<byte_t>), and for 
	the simulator function to operate in "natural" units units for the 
	simulator. For example, the simulated method might return a 
	temperature in degrees celsius, and the monitor method might pack that 
	into a 10 bit integre scaled between 0 and 100 degrees Celsium. 
	Several utility functions for packing/unpacking the data are available 
	(See AMBUtil.h).

	For a canonical example of how this all works in practice see the 
	Compressor simulated device. For details on the AMB protocol (not 
	necessary to create a simulated device), see ALMA Computing Memo #0007.

	The mandatory monitor points (given in "Generic Monitor and Control
	Points" by Brooks are defined as abstract functions with a default
	implementation. The optional monitors and control are not defined
	similarly since many devices will not need them. Note that you still
	need to manually dispatch to the mandatory points in your monitor and
	control functions. Note that this gives you an opportinity to ignore
	the mandatory points if you truly have a peculiar situation.
    */

    class Device
    {
    public:
    
    /**
     * Constructor.
     * Initialize the pointer to the SharedSimulator.
     */
    Device() : sharedSim_p(NULL) {};
    
	/// Destructor is a no-op.
	virtual ~Device();

	/// Return the node number (must be in the range 0-231).
	virtual node_t node() const = 0;

	/// Return the serial number (must have 8 (binary) characters).
	virtual std::vector<CAN::byte_t> serialNumber() const = 0;

	/** Perform a monitor transaction.
	    A monitor is defined to be a transaction that, given a
	    RCA, returns 0-8 bytes of data. Typically the monitor
	    function will have a switch statement that calls the real
	    simulation function, and will then pack the result into
	    the output vector. */
	virtual std::vector<CAN::byte_t> monitor(rca_t rca) const = 0;
	
	/** Perform a control transaction.
	    A control is defined to be a transaction that, takes a RCA 
	    and 1-8 (not zero!) bytes of data. No value is returned to 
	    the caller, although of course the result of the control
	    is presumably visible via a subsequent monitor request.
	    Typically the control function will switch on RCA, unpack
	    the data from the input vector, and call the actual
	    simulation function in "physical" units, e.g. floating
	    point degrees Celsius. */
	virtual void control(rca_t rca, const std::vector<CAN::byte_t> &data) = 0;

	enum GenericMCPointRCA_t {
				  GET_SERIAL_NUM          = 0x00000000,
				  GET_PROTOCOL_REV_LEVEL  = 0x00030000,
				  GET_CAN_ERROR           = 0x00030001,
				  GET_TRANS_NUM           = 0x00030002,
				  GET_AMBIENT_TEMPERATURE = 0x00030003,
				  GET_SW_REV_LEVEL        = 0x00030004,
				  GET_HW_REV_LEVEL        = 0x00030005,
				  RESET_AMBSI             = 0x00031000,
				  RESET_DEVICE            = 0x00031001};

	/* Generic monitor point. Default implementation returns 0,0,0,0. and
           does not increase the transaction count */
	virtual std::vector<CAN::byte_t> get_can_error() const;

	/** Generic monitor point. Default implementation returns 0,0,0 and
            increases the transaction count. */
	virtual std::vector<CAN::byte_t> get_protocol_rev_level() const;

	/** Generic monitor point. Default implementation returns three bytes
	 * ('F','o','o') and increases the transaction count.
	 * RCA=GET_SW_REV_LEVEL=0x30004
	 */
	virtual std::vector<CAN::byte_t> get_software_rev_level() const;

	/** Generic monitor point. Default implementation returns three bytes
	 * ('B','a','r') and increases the transaction count.
	 * RCA=GET_HW_REV_LEVEL=0x30005
	 */
	virtual std::vector<CAN::byte_t> get_hardware_rev_level() const;

	/** Generic monitor point. No implementation is provided since it
	    is easier to just increment a counter in the derived class
	    monitor() and control() functions. */
	virtual unsigned int get_trans_num() const = 0;

	/** Set the SharedSimulator member pointer */
	virtual void setSharedSimulator(SharedSimInterface* ssim) {sharedSim_p = ssim;}

    protected:

	/** Pointer to the SharedSimulator component */
	SharedSimInterface* sharedSim_p;

    };

} // namespace AMB

#endif /* AMB_DEVICE_H */
