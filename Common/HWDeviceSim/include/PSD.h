#ifndef PSD_H_
#define PSD_H_
// @(#) $Id$
//
// Copyright (C) 2005
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Library General Public License
// as published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Library General Public License for more details.
//
// You should have received a copy of the GNU Library General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
//
// Correspondence concerning ALMA should be addressed as follows:
//
//        Internet email: alma-sw-admin@alma.org
//

#include "AMBDevice.h"
#include "CANError.h"
#include <string>

namespace AMB {
  class PSD: public AMB::Device {
  public:
    /** Constructor
     ** @param node Node ID of this device
     ** @param serialNumber S/N of this device
     */
    PSD(node_t node, 
		      const std::vector<CAN::byte_t>& serialNumber);
    
    /// Destructor
    virtual ~PSD();

    /// Return the device's node ID
    virtual node_t node() const;

    /// Return the device's S/N
    virtual std::vector<CAN::byte_t> serialNumber() const;

    /** Monitor interface
     ** @param CAN node RCA
     ** @return std::vector of bytes containing monitor info. Each response is
     ** sized to what's specified in the ICD.
     */
    virtual std::vector<CAN::byte_t> monitor(rca_t rca) const;

    /** Control Interface ('set' functions)
     ** @param rca CAN node RCA
     ** @param data CAN message data required by device
     */
    virtual void control(rca_t rca, const std::vector<CAN::byte_t> &data);

    /// Return the number of transactions
    virtual unsigned int get_trans_num() const;

     /// Temperature on M&C interface board. 18.0 constant.
    std::vector<CAN::byte_t> get_ambient_temperature() const;
    /// Error and Alarms monitor point (0x00001)
    std::vector<CAN::byte_t> error_and_alarms() const;
    /// Instantaneous measured voltage on the +24 Volt A converter output
    std::vector<CAN::byte_t> voltage_24v_a() const;

    /// Instantaneous measured current drawn on the +24 Volt A converter output
    std::vector<CAN::byte_t> current_24v_a() const;

    /// Instantaneous measured voltage on the +24 Volt B converter output
    std::vector<CAN::byte_t> voltage_24v_b() const;

    /// Instantaneous measured current drawn on the +24 Volt B converter output
    std::vector<CAN::byte_t> current_24v_b() const;
    /// Error and Alarms monitor point (0x00001)
    std::vector<CAN::byte_t> max_min_24v_a() const;
    /// Error and Alarms monitor point (0x00001)
    std::vector<CAN::byte_t> max_min_24v_b() const;
    // shutdown the output
    void shutdown_com(const std::vector<CAN::byte_t>& command);

    // Throw this if something bad happens.
    class PSDError : 
      public CAN::Error {
    public:
      PSDError(const std::string& error) : CAN::Error(error) {;}
    };

    ////////////////////////////////////////////////////////////////////
    enum Monitor_t {
      // Generic monitor points
      GET_SERIAL_NUMBER       = 0x00000,
      GET_CAN_ERROR           = AMB::Device::GET_CAN_ERROR,
      GET_PROTOCOL_REV_LEVEL  = AMB::Device::GET_PROTOCOL_REV_LEVEL,
      GET_TRANS_NUM           = AMB::Device::GET_TRANS_NUM,
      GET_AMBIENT_TEMPERATURE = AMB::Device::GET_AMBIENT_TEMPERATURE,
       		
      
      // Monitor points specific to PSD Power supply (from the ICD).
      ERROR_AND_ALARMS	  = 0x00001,
      VOLTAGE_24V_A	      = 0x00009,
      CURRENT_24V_A	      = 0x0000a,
      VOLTAGE_24V_B	      = 0x0000d,
      CURRENT_24V_B	      = 0x0000e,
      MAX_MIN_24V_A		  = 0x00022,
      MAX_MIN_24V_B	 	  = 0x00024,
    };
    
    enum Controls_t {
      SHUTDOWN_COM	   = 0X00000081,
      };
    
   enum Test_t{
    	TEST_VOLTAGE_24V_A	=	0x0f001,
    	TEST_CURRENT_24V_A	=	0x0f002,
    	TEST_VOLTAGE_24V_B	=	0x0f003,
    	TEST_CURRENT_24V_B	=	0x0f004,
    	TEST_ERROR_AND_ALARMS = 0x0f005,
    	TEST_AMBIENT_TEMPERATURE=0x0f006,
    	TEST_MAXMIN24VA = 		0x0f007,
    	TEST_MAXMIN24VB=		0x0f008
    };

  private:
    // Undefined and inaccessible - copying does not make sense. See 
    // "Effective C++ Second edition" item 45 for details on what 
    // functions the compiler generates for you.
    PSD(const PSD&);
    PSD& operator=(const PSD&);
    
    /// Bus location (node number).
    node_t node_m;
    
    /// Serial number of this node.
    std::vector<CAN::byte_t> sn_m;
    
    /// Number of transactions. Needs to be mutable as its incremented by the
    /// monitor function, which is const.
    mutable unsigned int trans_num_m;
    ///checkSize
    void checkSize( const std::vector<CAN::byte_t> &data, int size,
    			const char *pExceptionMsg);
    // values for all the monitor points
    CAN::byte_t m_errorCode;
    bool shutdown_m;
    unsigned long int error_and_alarms_m;
    unsigned char max_min_24v_a_m[6]; //6 bytes according to ICD
    unsigned char max_min_24v_b_m[6];//6 bytes according to ICD
    double voltage_24v_a_m;
    double current_24v_a_m;
    double voltage_24v_b_m;
    double current_24v_b_m;
    unsigned long long int ambient_temperature_m;
  }; //end class
}//end namespace


#endif /*PSD_H_*/
