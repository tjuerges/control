// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#if !defined(NUTATOR_H)
#define NUTATOR_H

#include "AMBDevice.h"
#include "CANError.h"
#include <string>

#include <sys/time.h>
#include <unistd.h>

namespace AMB
{

/**
 * A concrete class that implements a CAN bus simulator for ATF Prototype 
 * Nutator.  This class contains all interface routines to access the ACU 
 * simulator through the CAN bus.
 */
class Nutator : public AMB::Device
{
  public:
    const static int MAX_POINTS_IN_PROGRAM = 6;
 
    struct ProgramPoint
    {
	ProgramPoint():  position(0), transition_time(0), dwell_time(0) {};
	unsigned short int position;	// TBD units
	unsigned short int transition_time;  // millisec
	unsigned short int dwell_time;	// millisec, 65535 => indefinite
    };

    struct Program
    {
	Program():  program_time(0) {};
	ProgramPoint points[MAX_POINTS_IN_PROGRAM];
	ProgramPoint current_point;
	unsigned short int program_time; // OK short int?
    };

    // TODO - have the behavior (position) vary with time.

    // After construction, the nutator is in the off state.
    Nutator(node_t node, const std::vector<CAN::byte_t>& serialNumber);
    virtual ~Nutator();
    
    // AMB::Device functions
    virtual node_t node() const;
    virtual std::vector<CAN::byte_t> serialNumber() const;
    virtual std::vector<CAN::byte_t> monitor(rca_t rca) const;
    virtual void control(rca_t rca, const std::vector<CAN::byte_t>& data);
    unsigned int get_trans_num() const;

    // ---- Nutator-Specific Monitor Points  
    unsigned char get_status() const;
    unsigned short int  get_position() const;
    std::vector<CAN::byte_t> get_active_program_point(int pointIndex) const;
    std::vector<CAN::byte_t> get_standby_program_point(int pointIndex) const;
    unsigned char get_program_validity() const;
    unsigned  int get_apex_ps_status() const;
    unsigned  int get_linamp_ps_status() const;
    unsigned  int get_controller_ps_status() const;
    unsigned short get_selftest_result() const;
    float get_temperature_x() const;
    
    // ---- Standard Monitor Points
    float get_control_box_temp() const;
    std::vector<CAN::byte_t> get_sw_rev_level() const;

    // ---- Nutator-Specific Control Points
    void set_standby_program_point(unsigned int pointIndex, std::vector<CAN::byte_t>& data);
    void clear_standby_program(unsigned char dummy);
    void reset_status(unsigned char dummy);
    void start_program(unsigned char dummy);
    void stop_program(unsigned char prog_point);
    void abort_program(unsigned char dummy);
    void stow(unsigned char dummy);
    void load_standby_program(unsigned char dummy);
    void do_selftest(unsigned char dummy);

    // ---- Standard Control Points
    void reset_device(unsigned char dummy);

    // Throw these errors if something bad happens
    class NutatorError : public CAN::Error
    {
      public:
	NutatorError(const std::string& error) : CAN::Error(error) {;}
    };

    /*
     * CAN Addresses for Nutator M&C Points.
     * These are used in the monitor() and control() switch statements.
     */
    enum Monitor_t
    {
	// Generic monitor points
	GET_CAN_ERROR           = AMB::Device::GET_CAN_ERROR,
	GET_PROTOCOL_REV_LEVEL  = AMB::Device::GET_PROTOCOL_REV_LEVEL,
	GET_TRANS_NUM           = AMB::Device::GET_TRANS_NUM,
	GET_AMBIENT_TEMPERATURE = AMB::Device::GET_AMBIENT_TEMPERATURE,

	GET_STATUS                = 0x00001,
	GET_POSITION              = 0x00002,
	GET_PROGRAM_VALIDITY      = 0x00005,
	GET_APEX_PS_STATUS        = 0x0000C,
	GET_LINAMP_PS_STATUS      = 0x0000D,
	GET_CONTROLLER_PS_STATUS  = 0x0000E,
	GET_SELFTEST_RESULT       = 0x0000F,
	GET_TEMPERATURE_0         = 0x00010,
	GET_TEMPERATURE_1         = 0x00011,
	GET_TEMPERATURE_2         = 0x00012,
	GET_TEMPERATURE_3         = 0x00013,
	GET_TEMPERATURE_4         = 0x00014,
	GET_TEMPERATURE_5         = 0x00015,
	GET_TEMPERATURE_6         = 0x00016,
	GET_TEMPERATURE_7         = 0x00017,
	GET_TEMPERATURE_8         = 0x00018,
	GET_TEMPERATURE_9         = 0x00019,
	GET_TEMPERATURE_A         = 0x0001A,
	GET_TEMPERATURE_B         = 0x0001B,
	GET_TEMPERATURE_C         = 0x0001C,
	GET_TEMPERATURE_D         = 0x0001D,
	GET_TEMPERATURE_E         = 0x0001E,
	GET_TEMPERATURE_F         = 0x0001F,
	GET_AD1			  = 0x00020,
	GET_AD2			  = 0x00021,
	GET_ADC_ERR1		  = 0x00022,
	GET_AD3			  = 0x00023,
	GET_AD4			  = 0x00024,
	GET_ADC_ERR2		  = 0x00025,
	GET_LOOP1_P               = 0x00026,
	GET_LOOP1_I               = 0x00027,
	GET_LOOP1_D               = 0x00028,
	GET_LOOP2_P               = 0x00029,
	GET_LOOP2_I               = 0x0002A,
	GET_LOOP2_D               = 0x0002B,
	GET_LOOP1_MIRROR          = 0x0002C,
	GET_LOOP2_MIRROR          = 0x0002D,
	GET_LOOP1_GOAL            = 0x00031,
	GET_LOOP1_PID             = 0x00032,
	GET_LOOP2_GOAL            = 0x00033,
	GET_LOOP2_PID             = 0x00034,
	GET_ERROR_BITS            = 0x00035,
	GET_ADC1_MIN              = 0x00036,
	GET_ADC1_MAX              = 0x00037,
	GET_ADC3_MIN              = 0x00038,
	GET_ADC3_MAX              = 0x00039,
	GET_BOARD_ID              = 0x0003B,
	GET_SAFETY_STATUS         = 0x0003C,
	GET_ACTIVE_PROG_SEG_0     = 0x00040,
	GET_ACTIVE_PROG_SEG_1     = 0x00041,
	GET_ACTIVE_PROG_SEG_2     = 0x00042,
	GET_ACTIVE_PROG_SEG_3     = 0x00043,
	GET_ACTIVE_PROG_SEG_4     = 0x00044,
	GET_ACTIVE_PROG_SEG_5     = 0x00045,
	GET_ACTIVE_PROG_SEG_I     = 0x0004F,
	GET_STANDBY_PROG_SEG_0    = 0x00080,
	GET_STANDBY_PROG_SEG_1    = 0x00081,
	GET_STANDBY_PROG_SEG_2    = 0x00082,
	GET_STANDBY_PROG_SEG_3    = 0x00083,
	GET_STANDBY_PROG_SEG_4    = 0x00084,
	GET_STANDBY_PROG_SEG_5    = 0x00085,
	GET_STANDBY_PROG_SEG_I    = 0x0008F
    };
    enum Controls_t
    {

	CLEAR_STANDBY_PROGRAM     = 0x01002,
	RESET_STATUS              = 0x01003,
	START_PROGRAM             = 0x01004,
	STOP_PROGRAM              = 0x01005,
	ABORT_PROGRAM             = 0x01006,
	STOW                      = 0x01007,
	DO_SELFTEST               = 0x0100F,
	SET_ADC_ERR1              = 0x01022,
	SET_ADC_ERR2              = 0x01025,
	SET_LOOP1_P               = 0x01026,
	SET_LOOP1_I               = 0x01027,
	SET_LOOP1_D               = 0x01028,
	SET_LOOP2_P               = 0x01029,
	SET_LOOP2_I               = 0x0102a,
	SET_LOOP2_D               = 0x0102b,
	SET_LOOP1_MIRROR          = 0x0102c,
	SET_LOOP2_MIRROR          = 0x0102d,
	SET_ADC1_MIN              = 0x01036,
	SET_ADC1_MAX              = 0x01037,
	SET_ADC3_MIN              = 0x01038,
	SET_ADC3_MAX              = 0x01039,
	SET_AMPEN_CLR             = 0x0103b,
	SET_STANDBY_PROG_SEG_0    = 0x01080,
	SET_STANDBY_PROG_SEG_1    = 0x01081,
	SET_STANDBY_PROG_SEG_2    = 0x01082,
	SET_STANDBY_PROG_SEG_3    = 0x01083,
	SET_STANDBY_PROG_SEG_4    = 0x01084,
	SET_STANDBY_PROG_SEG_5    = 0x01085,
	DEBUG_NOP                 = 0x00000,
	DEBUG_ADC_STATS_0c        = 0x01101,
	DEBUG_ADC_STATS_0e        = 0x01102,
	DEBUG_INFO                = 0x01103,
	DEBUG_GET_ERR             = 0x01104,
	RESET_DEVICE              = 0x31001
    };

  private:

    node_t node_m;
    std::vector<CAN::byte_t> sn_m;
    
    mutable unsigned char status_m;
    unsigned short int selftest_m;
    mutable unsigned int trans_num_m;

    mutable Program programs_m[2]; 
    mutable Program* active_program_m;
    mutable Program* standby_program_m;
    
    mutable long int stop_time_m;
    struct timeval start_time_m;

    // control loop coefficients
    float loop_m[2][3];

    // Undefined and inaccessible
    Nutator(const Nutator&);
    Nutator& operator=(const Nutator&);
};

} // namespace AMB

#endif
