// @(#) $Id$


#ifndef ACUACA_H
#define ACUACA_H

#include <string>
#include "AMBDevice.h"
#include "AMBUtil.h"
#include "CANError.h"
#include "ACUACASim.h"

namespace AMB
{

/**
 * This class is derived from the AMB::Device class and is a concrete class 
 * that implements a CAN bus simulator for the ACA ACU computer.
 *
 */
class ACUACA : public AMB::Device
{
  public:

    ACUACA();
    ACUACA(node_t node,
	const std::vector<CAN::byte_t>& serialNumber);

    ~ACUACA();

    //
    // Generic monitor points
    //
    std::vector<CAN::byte_t> get_can_error() const;
    std::vector<CAN::byte_t> get_protocol_rev_level() const;
    std::vector<CAN::byte_t> get_sw_rev_level() const;
    unsigned int get_trans_num() const;

    //
    // AMB::Device functions used by the AMBSimulator
    //
    node_t node() const;
    std::vector<CAN::byte_t> serialNumber() const;

    std::vector<CAN::byte_t> monitor(rca_t rca) const;
    void control(rca_t rca,const std::vector<CAN::byte_t>& data);

    //
    // ACU specializations
    //
    // throw these errors if something bad happens
    class ACUError : public CAN::Error
    {
      public:
	ACUError(const std::string& error) : CAN::Error(error) {;}
    };

  private:

    ACAacu::ACUSim sim_m;

    //
    // CAN Node stuff
    //
    node_t node_m;		// bus location (node number)
    std::vector<CAN::byte_t> sn_m;	// serial number of this CAN node
    mutable unsigned int trans_num_m;  // number of transactions

    // undefined and inaccessible
    ACUACA(const ACUACA&);
    ACUACA& operator = (const ACUACA&);

};  // class ACUACA

} // namespace AMB
#endif // ACUACA_H
