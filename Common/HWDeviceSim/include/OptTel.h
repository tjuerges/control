// @(#) $Id$
//
// Copyright (C) 2001, 2005
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#if !defined(OPTTEL_H)
#define OPTTEL_H

#include "AMBDevice.h"
#include "CANError.h"
#include <string>

namespace AMB {

  /// 
  /// Based on the Optical telescope ICD dated 2004-07-27 with corrections from
  /// the optical telescope firmware 1.14 and checked into CVS on 2004-03-09
  /// (for main.c).
  ///

  class OptTel: public AMB::Device
  {
  public:

    /// 
    /// After construction, the shutter and solar filter are closed, the IR
    /// filter is out, the power and heater are off
    ///
    OptTel(node_t node, const std::vector<CAN::byte_t>& serialNumber);
    virtual ~OptTel();

    ///
    /// Returns the node number. This should usually be 0x1F
    ///
    virtual node_t node() const;

    ///
    /// Returns the serial number. This is an arbitrary but unique 64 bit sequence.
    ///
    virtual std::vector<CAN::byte_t> serialNumber() const;

    ///
    /// Performs monitor request of the specified relative CAN address
    /// (RCA). An excpetion is thrown if the RCA is not one of the expected
    /// ones.
    /// 
    virtual std::vector<CAN::byte_t> monitor(rca_t rca) const;

    ///
    /// Performs control request of the specified relative CAN address (RCA),
    /// using teh supplied data. An excpetion is thrown if the RCA is not one
    /// of the expected ones or if the supplied data is not of the correct length.
    /// 
    virtual void control(rca_t rca, const std::vector<CAN::byte_t>& data);

    ///
    /// CAN Generic MC points. Some of these are implemented in the base class
    /// are are not overridden here.
    ///

    ///
    /// Return the number of transactions. This counts bothe monitor are
    /// control transactions.
    ///
    virtual unsigned int get_trans_num() const;

    ///
    /// Temperature on M&C interface board in dgrees C. This returns 18.0
    /// constant (if only it was always like that).
    ///
    float getAmbientTemperature() const;

    ///
    /// Optical Telescope specific monitor points.
    ///

    ///
    /// Returns the current state of the shutters, filters, heaters, power etc. 
    /// See the ICD for details.
    ///
    CAN::byte_t getStatus() const;

    ///
    /// returns the amount of light measured by the photodetector. This is used
    /// to automatically close the the shutter if there is too much light.
    /// The default threshold is 2.5 Volts but this can be chnaged over the
    /// AMB. Currently this returns a constant value (0x100 or 1.25
    /// Volts). TODO implement a variable value and have the shutter
    /// automatically close when it is above the threshold.
    ///
    short getPhotodetector() const;

    ///
    /// The temperature at the shutter and solar filter. This is fixed at 15.0 C.
    ///
    float getTubeTemp() const;

    ///
    /// The temperature on the CCD. This is fixed at set at about 25.0 degrees C (0x0080)
    ///
    short getCCDTemp() const;

    ///
    /// A temperature inside the electonics box. Set to a constant 20.0 degrees C
    ///
    float getElectronicTemp1() const;
    
    ///
    /// Another temperature inside the electonics box. Set to a constant 30.0 degrees C
    /// 
    float getElectronicTemp2() const;

    ///
    /// The threshold used to decide when to automatically close the
    /// shutter. This returns the value last set using the SET_PHOTO_THRESHOLD
    /// control point and defaults to 2.5 Volts on power up.
    /// 
    float getPhotoThreshold() const;

    ///
    /// The status of the limit switches on the focus mechanism. I suspect
    /// these indicate when the focus mechanism has reached either end of its
    /// travel range. Currently returns 0x00. TODO link this with the focusing
    /// commands so that they are set when you move tadjust focus outside the
    /// allowed range.
    /// 
    CAN::byte_t getLimitSwitchStatus() const;

    ///
    /// Optical Telescope specific control points.
    ///

    ///
    /// Open or close the shutter on the optical telescope. This shutter
    /// protects the telescope aperture from the weather. This shutter cannot
    /// be closed unless the solar filer is in place.
    ///
    void setShutter(CAN::byte_t state);

    ///
    /// Returns the last command sent to open or close the shutter on the
    /// optical telescope. Ideally this corresponds to the actual position of
    /// the shutter. The getStatus command should be used, in preference to
    /// this function, to determine the actual position. See the setShutter
    /// function for the purpose of the shutter.
    ///
    CAN::byte_t getShutter() const;

    ///
    /// Open or close the solar filter on the optical telescope. This protects
    /// the CCD from bright light, in particular the sun. It is automatically
    /// inserted if the photodetector senses too much light. This filter cannot
    /// be removed unless the shutter is open.
    ///
    void setSolarFilter(CAN::byte_t state);

    ///
    /// Returns the last command sent to open or close the solar filter on the
    /// optical telescope. Ideally this corresponds to the actual position of
    /// the solar filter. The getStatus command should be used, in preference to
    /// this function, to determine the actual position. See the setSolarFilter
    /// function for the purpose of the filter.
    ///
    CAN::byte_t getSolarFilter() const;

    ///
    /// Open or close the infra-read filter on the optical telescope. This protects
    /// the CCD from bright light, in particular, the sun. This filter cannot
    /// be removed unless the shutter is open.
    ///
    void setIRFilter(CAN::byte_t state);

    ///
    /// Returns the last command sent to open or close the IR filter on the
    /// optical telescope. Ideally this corresponds to the actual position of
    /// the IR filter. The getStatus command should be used, in preference to
    /// this function, to determine the actual position. See the setIRFilter
    /// function for the purpose of the filter.
    ///
    CAN::byte_t getIRFilter() const;

    ///
    /// Enable power to the system. Its unclear if this is power to the motor
    /// or to the CCD and video transmitter (or both).
    ///
    void setPower(CAN::byte_t state);

    ///
    /// readback of the setPower control point
    ///
    CAN::byte_t getPower() const;

    ///
    /// Enable the telecope tube heater. This is used to de-ice the shutter and
    /// related mechanical parts. Its unclear if this can also de-fog the
    /// optics.
    ///
    void setHeater(CAN::byte_t state);

    ///
    /// readback of the setHeater control point
    ///
    CAN::byte_t getHeater() const;

    ///
    /// Set the photo-detector threshold. When the detected incident light
    /// exceeds this threshold the firmware will automatically close the
    /// shutter.
    ///
    void setPhotoThreshold(float threshold);

    ///
    /// Adjust the focus of the telescope. The distance is in micrometers and
    /// can be any value between +/-30,000.0. The magnitude controls how far to
    /// move the focus and the sign is the direction. The focus moves at a
    /// constant speed of 30micrometers/second so big changes can take up to
    /// 1000 seconds (over 15 minutes). Focus commands are queued in the
    /// firmware. TODO:
    /// 1. Keep track of the focus and when the focus is moved outside the
    /// allowed range trip the limit switches. 
    /// 2. Queue the focus commands an implement the amount of time it takes to
    /// move the focus.
    ///
    void moveFocus(float distance);
    float getFocus() const;

    ///
    /// Throw this if anything bad happens.
    ///
    class OptTelError : public CAN::Error {
    public:
      OptTelError(const std::string& error) : CAN::Error(error) {}
    };
    
    enum Monitor_t {
      ///
      /// Generic monitor points for an AMBSI-1
      ///
      GET_SERIAL_NUM          = AMB::Device::GET_SERIAL_NUM,
      GET_CAN_ERROR           = AMB::Device::GET_CAN_ERROR,
      GET_PROTOCOL_REV_LEVEL  = AMB::Device::GET_PROTOCOL_REV_LEVEL,
      GET_TRANS_NUM           = AMB::Device::GET_TRANS_NUM,
      GET_AMBIENT_TEMPERATURE = AMB::Device::GET_AMBIENT_TEMPERATURE,
      GET_SW_REV_LEVEL        = AMB::Device::GET_SW_REV_LEVEL,
      GET_HW_REV_LEVEL        = AMB::Device::GET_HW_REV_LEVEL,

      ///
      /// Monitor points specific to the optical telescope (from the ICD).
      ///
      GET_STATUS                = 0x00001,
      GET_PHOTODETECTOR         = 0x00002,
      GET_TUBE_TEMPERATURE      = 0x00003,
      GET_CCD_TEMPERATURE       = 0x00004,
      GET_ELECTRONIC_TEMP1      = 0x00005,
      GET_ELECTRONIC_TEMP2      = 0x00006,
      GET_PHOTO_THRESHOLD       = 0x00007,
      GET_LIMIT_SWITCHES_STATUS = 0x00008,

      /// The optical telescope can also readback, as a monitor point, any
      /// control point (except reset device).
      GET_SET_SHUTTER_POSITION      = 0x01001,
      GET_SET_SOLAR_FILTER_POSITION = 0x01002,
      GET_SET_IR_FILTER_POSITION    = 0x01003,
      GET_SET_ENABLE_12V            = 0x01004,
      GET_SET_HEATER                = 0x01005,
      GET_SET_PHOTO_THRESHOLD       = 0x01006,
      GET_SET_MOVE_FOCUS            = 0x01007
    };

    enum Controls_t {
      ///
      /// Generic control points for an AMBSI-1
      ///
      RESET_DEVICE              = AMB::Device::RESET_DEVICE,

      ///
      /// Control points specific to the optical telescope (from the ICD).
      ///
      SET_SHUTTER_POSITION      = 0x01001,
      SET_SOLAR_FILTER_POSITION = 0x01002,
      SET_IR_FILTER_POSITION    = 0x01003,
      SET_ENABLE_12V            = 0x01004,
      SET_HEATER                = 0x01005,
      SET_PHOTO_THRESHOLD       = 0x01006,
      SET_MOVE_FOCUS            = 0x01007
    };

  private:
    // Undefined and inaccessible - copying does not make sense.
    OptTel(const OptTel&);
    OptTel& operator=(const OptTel&);

    /// Bus location (node number).
    node_t node_m;
    
    ///
    /// Serial number of this node.
    ///
    std::vector<CAN::byte_t> sn_m;

    ///
    /// State of the compressor.
    ///
    CAN::byte_t state_m;

    ///
    /// State of the fridge.
    ///
    CAN::byte_t fridge_state_m;

    ///
    /// Photodetector threshold.
    ///
    float threshold_m;

    ///
    /// Photodetector threshold.
    ///
    float lastFocusCmd_m;

    ///
    /// Number of transactions. It needs to be mutable because the monitor
    /// function, which is a const function, updates this number.
    ///
    mutable unsigned int transNum_m;

  };  // class OptTel

}  // namespace AMB

#endif  // OPTTEL_H
