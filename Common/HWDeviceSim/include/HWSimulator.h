#ifndef HWSIMULATOR_H
#define HWSIMULATOR_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2003-12-30  created
*/

/************************************************************************
 * This is the baseclass for device drivers.  It defines the class AmbDeviceInt
 * which provides methods for monitor and control interactions with the Amb
 * as well as providing the interface which AmbDevIO uses.  This is a pure
 * virtual class, as there is no way to specify the channel and address
 * thus the constructor is protected.
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <queue>
#include <pthread.h>
#include <acstime.h>
#include <ambDefs.h>
#include <ControlExceptions.h>
#include "AMBDevice.h"

class HWSimulator
{
  public:
    
    HWSimulator();
    
    virtual ~HWSimulator();

    virtual void setDevice(AMB::Device* device) throw();
    virtual bool isSimulating() throw() {return simulating_m; };
    virtual void start() throw (ControlExceptions::CAMBErrorExImpl);
    virtual void stop();
   
    virtual void insert(AmbTransaction_t requestType,
			ACS::Time        TimeEvent,
			AmbRelativeAddr  RCA,
			AmbDataLength_t& dataLength, 
			AmbDataMem_t*    data,
			sem_t*           synchLock,
			Time*            timestamp,
			AmbErrorCode_t*  status)
	throw (ControlExceptions::CAMBErrorExImpl);
    
    virtual void flush(ACS::Time         TimeEvent,
		       ACS::Time*        timestamp,
		       AmbErrorCode_t*   status);
    
    virtual void flush(ACS::Time         TimeEvent,
		       AmbRelativeAddr   RCA,
		       ACS::Time*        timestamp,
		       AmbErrorCode_t*   status);
    class AmbCommand
    {
      public: 
	AmbCommand()
	    {	    
		requestType_m = AMB_MONITOR;
		TimeEvent_m = 0;
		rca_m = 0;
		length_m = NULL;
		data_m = NULL;
		synchLock_m = NULL;
		timestamp_m = NULL;
		status_m = NULL;
		counter_m = 0ULL;
	    };
	
	void set(AmbTransaction_t requestType,
		 ACS::Time        TimeEvent,
		 AmbRelativeAddr  rca,
		 AmbDataLength_t* length,
		 AmbDataMem_t*    data,
		 sem_t*           synchLock,
		 ACS::Time*       timestamp,
		 AmbErrorCode_t*  status,
		 unsigned long long counter)
	    {
		requestType_m = requestType;
		TimeEvent_m   = TimeEvent;
		rca_m         = rca;
		length_m      = length; 
		data_m        = data;
		synchLock_m   = synchLock; 
		timestamp_m   = timestamp;
		status_m      = status;
		counter_m     = counter;
	    };
	
	void clear()
	    {	    
		requestType_m = AMB_MONITOR;
		TimeEvent_m = 0;
		rca_m = 0;
		length_m = NULL;
		data_m = NULL;
		synchLock_m = NULL;
		timestamp_m = NULL;
		status_m = NULL;
		counter_m = 0ULL;
	    };

	AmbTransaction_t requestType_m;
	ACS::Time        TimeEvent_m;
	AmbRelativeAddr  rca_m;
	AmbDataLength_t* length_m;
	AmbDataMem_t*    data_m;
	sem_t*           synchLock_m;
	ACS::Time*       timestamp_m;
	AmbErrorCode_t*  status_m;
	unsigned long long counter_m;
    };

  protected:
    bool simulating_m;

  private:
    static const int TE_PERIOD_ACS_IN_US;
    static const unsigned int DEFAULT_AVAILABLE_SIZE;
    
    class AmbCommandPCompare
    {
      public:
	bool operator() (const AmbCommand* command1, const AmbCommand* command2) const
	    {
		bool comparison = false;
		if (command1->TimeEvent_m >command2->TimeEvent_m)
		    comparison = true;
		else
		    if ((command1->TimeEvent_m == command2->TimeEvent_m) && ( command1->counter_m> command2->counter_m))
			comparison = true;
		return comparison;

		// return (command1->TimeEvent_m > command2->TimeEvent_m);
	    }
    };
    
    AmbCommand* getSlot();
    
    AmbCommand* getNextCommand();

    static void* run(void* data);

    void simulation();
    
    void simulate(AmbCommand* command);

    void monitor(AmbCommand* command);
    
    void control(AmbCommand* command);
    
    AMB::Device* device_m;
    std::priority_queue<AmbCommand*,std::vector<AmbCommand*>,AmbCommandPCompare> pending_m;
    ACE_Recursive_Thread_Mutex pendingMutex_m;
    std::queue<AmbCommand*> available_m;
    ACE_Recursive_Thread_Mutex availableMutex_m;
    std::queue<AmbCommand*> flushed_m;
    ACE_Recursive_Thread_Mutex flushedMutex_m;
    pthread_t simulationTID_m;
    unsigned long long counter_m;


    };
#endif /*!HWSIMULATOR_H*/
