// @(#) $Id$
//
// Copyright (C) 2002, 2004
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#ifndef AMBDEVIOTESTDEVICE_H
#define AMBDEVIOTESTDEVICE_H

//#define WRITE_LOG


#include "AMBDevice.h"
#include "CANError.h"
#include <string>
#include <stdio.h>

namespace AMB
{
  /** The LORR class is a simulator for the local oscillator receiver reference 
   */
  class AmbDevIOTestDevice : public AMB::Device
  {
  public:
    /** Constructor
     ** @param node Node ID of this device
     ** @param serialNumber S/N of this device
     */
    AmbDevIOTestDevice(node_t node, 
		       const std::vector<CAN::byte_t>& serialNumber);

    /// Destructor
    virtual ~AmbDevIOTestDevice();
    
    /// Return the device's node ID
    virtual node_t node() const;

    /// Return the device's S/N
    virtual std::vector<CAN::byte_t> serialNumber() const;
    
    /** Monitor interface
     ** @param CAN node RCA
     ** @return std::vector of bytes containing monitor info. Each response is
     ** sized to what's specified in the ICD.
     */
    virtual std::vector<CAN::byte_t> monitor(rca_t rca) const;
    
    /** Control Interface ('set' functions)
     ** @param rca CAN node RCA
     ** @param data CAN message data required by device
     */
    virtual void control(rca_t rca, const std::vector<CAN::byte_t>& data);
    
    /// Monitor points for the LORR
    enum Monitor_t {
      /// Generic monitor points
      //@{
      GET_SERIAL_NUMBER       = 0x00000,
      GET_CAN_ERROR           = AMB::Device::GET_CAN_ERROR,
      GET_PROTOCOL_REV_LEVEL  = AMB::Device::GET_PROTOCOL_REV_LEVEL,
      GET_TRANS_NUM           = AMB::Device::GET_TRANS_NUM,
      GET_AMBIENT_TEMPERATURE = AMB::Device::GET_AMBIENT_TEMPERATURE,
      //@}
      
      /// Specific DigiClock data point monitors
      //@{
      BOOL_PROP_MON               = 0x00001,
      DOUBLE_PROP_MON             = 0x00002,
      SIGNED_DOUBLE_PROP_MON      = 0x00003,
      DOUBLE_SEQ_PROP_MON1        = 0x00004,
      DOUBLE_SEQ_PROP_MON2        = 0x00005,
      SIGNED_DOUBLE_SEQ_PROP_MON1 = 0x00006,
      SIGNED_DOUBLE_SEQ_PROP_MON2 = 0x00007,
      LONG_PROP_MON               = 0x00008,
      LONGLONG_PROP_MON           = 0x00009,
      LONG_PARAMETRIC_MON         = 0x0000A,
      ULONGLONG_EXP_MON           = 0x0000B,
      ENUM_PROP_MON               = 0x0000C
      //@}
    };

    /// Control Functions for the DigiClock
    enum Controls_t {
      BOOL_PROP_CMD               = 0x00081,
      DOUBLE_PROP_CMD             = 0x00082,
      SIGNED_DOUBLE_PROP_CMD      = 0x00083,
      DOUBLE_SEQ_PROP_CMD1        = 0x00084,
      DOUBLE_SEQ_PROP_CMD2        = 0x00085,
      SIGNED_DOUBLE_SEQ_PROP_CMD1 = 0x00086,
      SIGNED_DOUBLE_SEQ_PROP_CMD2 = 0x00087,
      LONG_PROP_CMD               = 0x00088,
      LONGLONG_PROP_CMD           = 0x00089,
      LONG_PARAMETRIC_CMD         = 0x0008A,
      ULONGLONG_EXP_CMD           = 0x0008B,
      ENUM_PROP_CMD               = 0x0008C
    };
    
    // Throw this if something bad happens.
    class AmbDevIOTestDeviceError : public CAN::Error {
    public:
      AmbDevIOTestDeviceError(const std::string &error) : CAN::Error(error) {;}
    };
    
    /// Get functions for unit testing
    //@{
      CAN::byte_t* getLAST_PHASE_COMMAND();
      CAN::byte_t* getCURRENT_PHASE();
      CAN::byte_t  getDGCK_STATUS() const;
      CAN::byte_t  getMISSED_COMMAND() const;
      double getLOCK_INDICATOR_VOLTAGE() const;
      double getPS_VOLTAGE_CLOCK() const;
    
  private:
    /// Take the default implementation = return 0,0,0,0
    virtual std::vector<CAN::byte_t>  get_can_error() const;
    
    /// Take the default implementation = return 0,0,0
    virtual std::vector<CAN::byte_t>  get_protocol_rev_level() const;
    
    /// Return the number of transactions
    virtual unsigned int get_trans_num() const;
    
    /// Temperature on M&C interface board.
    double get_ambient_temperature() const;
    
    
    /** Helper function for 'set' commands to check that data size
     ** is valid, if not throw exception
     ** @param data Vector of bytes containing data to check
     ** @param size Expected number of bytes in data std::vector
     ** @param pExceptionMsg String identifying CAN msg that is
     ** associated with failed size check.
     */
    void checkSize( const std::vector<CAN::byte_t> &data, int size,
		    const char *pExceptionMsg);
    
    /// \name Undefined and inaccessible - copying does not make sense.
    //@{
    AmbDevIOTestDevice (const AmbDevIOTestDevice &);
    AmbDevIOTestDevice &operator=(const AmbDevIOTestDevice &);
    //@}
    
    /// Bus location (node number).
    node_t m_node;

    /// Serial number of this node.
    std::vector<CAN::byte_t> m_sn;
    
    /// Number of transactions. Needs to be mutable as its incremented by the
    /// monitor function, which is const.
    mutable unsigned int m_trans_num;
    // values for all the monitor points
    CAN::byte_t m_errorCode;

    // This is the last phase command sent
    CAN::byte_t LAST_PHASE_COMMAND_m[8];

  
    std::vector<CAN::byte_t> BOOL_PROP_m;
    std::vector<CAN::byte_t> DOUBLE_PROP_m;
    std::vector<CAN::byte_t> SIGNED_DOUBLE_PROP_m;
    std::vector<CAN::byte_t> DOUBLE_SEQ_PROP1_m;
    std::vector<CAN::byte_t> DOUBLE_SEQ_PROP2_m;
    std::vector<CAN::byte_t> SIGNED_DOUBLE_SEQ_PROP1_m;
    std::vector<CAN::byte_t> SIGNED_DOUBLE_SEQ_PROP2_m;
    std::vector<CAN::byte_t> LONG_PROP_m;
    std::vector<CAN::byte_t> LONGLONG_PROP_m;
    std::vector<CAN::byte_t> LONG_PARAMETRIC_m;
    std::vector<CAN::byte_t> ULONGLONG_EXP_m;
    std::vector<CAN::byte_t> ENUMPROP_m;
  };  // class AmbDevIOTestDevice
}  // namespace AMB

#endif  // AMBDEVIOTESTDEVICE_H
