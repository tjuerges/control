#ifndef SHAREDSIMINTERFACE_H
#define SHAREDSIMINTERFACE_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2005 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* rhiriart  2005-09-20  created
*/

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <maciSimpleClient.h>
#include <SharedSimulatorC.h>
#include "SharedSimWrapper.h"
#include <iostream>
#include <sstream>
#include "AMBUtil.h"

/**
 * \brief Adapter class to interface AMBLBSimulator with TELCAL
 * SharedSimulator.
 *
 * AMBLBSimulator works by dispatching CAN messages to several device
 * simulators. A pointer to a SharedSimInterface is passed to the
 * device simulators (see AMBDevice.h), which will invoke
 * SharedSimInterface functions when receiving selected monitoring or
 * control CAN messages.
 */
class SharedSimInterface : public SharedSimWrapper {
 public:

  /**
   * \brief Constructor.
   *
   * The constructor expects to receive the CLI parameters passed to
   * AMBLBSimulator. The constructor recognizes two CLI paramenters:
   * \li \c -sharedsim Use the SharedSimulator
   * \li <tt>-antenna number</tt> Sets up the antenna number
   *
   * If the \c sharedsim argument is passed, the SharedSimulator
   * component will be contacted using \c maci::SimpleClient and 
   * turned on.
   *
   * If no antenna number is passed, it will default to 0.
   */
  SharedSimInterface(int argc, char *argv[]);

  /**
   * \brief Destructor.
   * 
   * The destructor will free the reference to TELCAL SharedSimulator.
   */
   ~SharedSimInterface();

 private:

  /** A SimpleClient object to access ACS services. */
  maci::SimpleClient client;

  /** Print a message to stdout showing the CLI parameters accepted. */
  void show_usage() const;
};

#endif /*!SHAREDSIMINTERFACE_H*/
