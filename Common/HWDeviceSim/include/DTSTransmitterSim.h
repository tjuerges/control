#ifndef DTSTransmitter_h
#define DTSTransmitter_h
// @(#) $Id$
//
// Copyright (C) 2005
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include "AMBDevice.h"
#include "CANError.h"
#include <vector>
#include <string>
#include <bitset>

namespace AMB
{
	/** The DTSTransmitter class is a simulator for the DTS transmitter module.
	 */
	class DTSTransmitter: public AMB::Device
	{
		public:
			/** Constructor
			 ** @param node Node ID of this device
			 ** @param serialNumber S/N of this device
			 */
			DTSTransmitter(node_t node, const std::vector< CAN::byte_t >& serialNumber);

			/// Destructor
			virtual ~DTSTransmitter();

			/// Return the device's node ID
			virtual node_t node() const;

			/// Return the device's S/N
			virtual std::vector< CAN::byte_t > serialNumber() const;

			/** Monitor interface
			 ** @param CAN node RCA
			 ** @return std::vector of bytes containing monitor info. Each response is
			 ** sized to what's specified in the ICD.
			 */
			virtual std::vector< CAN::byte_t > monitor(const rca_t rca) const;

			/** Control Interface ('set' functions)
			 ** @param rca CAN node RCA
			 ** @param data CAN message data required by device
			 */
			virtual void control(const rca_t rca, const std::vector< CAN::byte_t >& data);

			/// Monitor points for the DTS transmitter
			enum Monitor_t
			{
				/// Specific DTSTransmitter data point monitors
				//@{
				GET_DG_3_3_V = 0x02501,
				GET_DG_5_V = 0x02502,
				GET_DG_FW_VER = 0x02505,
				GET_DG_MODE = 0x02504,
				GET_DG_TEMP = 0x02503,
				GET_FR_1_5_V = 0x01600,
				GET_FR_1_8_V = 0x01601,
				GET_FR_48_V = 0x04003,
				GET_FR_BOARD_VOLTAGE = 0x01602,
				GET_FR_CW_CH1 = 0x01001,
				GET_FR_CW_CH2 = 0x02001,
				GET_FR_CW_CH3 = 0x03001,
				GET_FR_RNG_CH1 = 0x0100a,
				GET_FR_RNG_CH2 = 0x0200a,
				GET_FR_RNG_CH3 = 0x0300a,
				GET_FR_FPGA_FW_VER_CH1 = 0x01004,
				GET_FR_FPGA_FW_VER_CH2 = 0x02004,
				GET_FR_FPGA_FW_VER_CH3 = 0x03004,
				GET_FR_INPUT_TEST_CH1 = 0x0100b,
				GET_FR_INPUT_TEST_CH2 = 0x0200b,
				GET_FR_INPUT_TEST_CH3 = 0x0300b,
				GET_FR_LASER_BIAS = 0x01605,
				GET_FR_LASER_PWR = 0x01604,
				GET_FR_LRU_CIN = 0x07fff,
				GET_FR_PAYLOAD1 = 0x01005,
				GET_FR_PAYLOAD2 = 0x02005,
				GET_FR_PAYLOAD3 = 0x03005,
				GET_FR_PAYLOAD_STATUS = 0x02006,
				GET_FR_PHASE_OFFSET = 0x01009,
				GET_FR_PHASE_SEQ_A = 0x01007,
				GET_FR_PHASE_SEQ_B = 0x01008,
				GET_FR_STATUS = 0x02000,
				GET_FR_TE_STATUS = 0x02002,
				GET_FR_TMP = 0x01603,
				GET_TTX_ALARM_STATUS = 0x02401,
				GET_TTX_LASER_BIAS_CH1 = 0x02101,
				GET_TTX_LASER_BIAS_CH2 = 0x02201,
				GET_TTX_LASER_BIAS_CH3 = 0x02301,
				GET_TTX_LASER_ENABLED = 0x02405,
				GET_TTX_LASER_PWR_CH1 = 0x02102,
				GET_TTX_LASER_PWR_CH2 = 0x02202,
				GET_TTX_LASER_PWR_CH3 = 0x02302,
				GET_TTX_LASER_TMP_CH1 = 0x02103,
				GET_TTX_LASER_TMP_CH2 = 0x02203,
				GET_TTX_LASER_TMP_CH3 = 0x02303,
				GET_TTX_REG_DATA_CH1 = 0x0210f,
				GET_TTX_REG_DATA_CH2 = 0x0220f,
				GET_TTX_REG_DATA_CH3 = 0x0230f,
				//@}
			};

			/// Control Functions for the DTSTransmitter
			enum Controls_t
			{
				DG_LOAD_TEST_PAT = 0x0a504,
				FR_TE_RESET = 0x0a002,
				FR_RESET_CH1 = 0x09000,
				FR_RESET_CH2 = 0x0a000,
				FR_RESET_CH3 = 0x0b000,
				FR_RESET_ALL = 0x0c000,
				SET_FR_CW_CH1 = 0x09001,
				SET_FR_CW_CH2 = 0x0a001,
				SET_FR_CW_CH3 = 0x0b001,
				SET_FR_CW_ALL = 0x0c001,
				SET_FR_INPUT_TEST_CH1 = 0x0900b,
				SET_FR_INPUT_TEST_CH2 = 0x0a00b,
				SET_FR_INPUT_TEST_CH3 = 0x0b00b,
				SET_FR_INPUT_TEST_ALL = 0x0c00b,
				SET_FR_RNG_CH1 = 0x0900a,
				SET_FR_RNG_CH2 = 0x0a00a,
				SET_FR_RNG_CH3 = 0x0b00a,
				SET_FR_RNG_CHALL = 0x0c00a,
				SET_FR_PHASE_SEQ_A = 0x09007,
				SET_FR_PHASE_SEQ_B = 0x09008,
				SET_FR_PHASE_OFFSET = 0x09009,
				SET_FR_48_V = 0x0c003,
				FR_RESET = 0x0c004,
				FR_CAPTURE_PAYLOAD = 0x0c005,
				TTX_RESET = 0x0a400,
				TTX_CLR_ALARMS = 0x0a401,
				TTX_RESET_FIFO = 0x0a404,
				TTX_LASER_ENABLE = 0x0a405,
				TTX_REG_ACCESS_CH1 = 0x0a10f,
				TTX_REG_ACCESS_CH2 = 0x0a20f,
				TTX_REG_ACCESS_CH3 = 0x0a30f,
				TTX_REG_ACCESS_ALL = 0x0a40f};

			// Throw this if something bad happens.
			class DTSTransmitterError:public CAN::Error
			{
				public:
					DTSTransmitterError(const std::string& error):CAN::Error (error){};
			};

		private:
			/**
			 * The DTX has three different firmware strings. They are different
			 *  in bytes 6 and 7.
			 */  
			static const unsigned char FirmwareRevision_BitD[8];
			static const unsigned char FirmwareRevision_BitC[8];
			static const unsigned char FirmwareRevision_BitB[8];

			/// Return the number of transactions
			virtual unsigned int get_trans_num() const;

			// DTSTransmitter specific monitor points.
			std::vector< CAN::byte_t > CAN_GET_DG_3_3_V() const;
			std::vector< CAN::byte_t > CAN_GET_DG_5_V() const;
			std::vector< CAN::byte_t > CAN_GET_DG_TEMP() const;
			std::vector< CAN::byte_t > CAN_GET_DG_MODE() const;
			std::vector< CAN::byte_t > CAN_GET_DG_FW_VER() const;
			std::vector< CAN::byte_t > CAN_GET_FR_TE_STATUS() const;
			std::vector< CAN::byte_t > CAN_GET_FR_STATUS() const;
			std::vector< CAN::byte_t > CAN_GET_FR_INPUT_TEST(const int channel) const;
			std::vector< CAN::byte_t > CAN_GET_FR_1_5_V() const;
			std::vector< CAN::byte_t > CAN_GET_FR_1_8_V() const;
			std::vector< CAN::byte_t > CAN_GET_FR_BOARD_VOLTAGE() const;
			std::vector< CAN::byte_t > CAN_GET_FR_TMP() const;
			std::vector< CAN::byte_t > CAN_GET_FR_LASER_PWR() const;
			std::vector< CAN::byte_t > CAN_GET_FR_LASER_BIAS() const;
			std::vector< CAN::byte_t > CAN_GET_FR_CW(const int channel) const;
			std::vector< CAN::byte_t > CAN_GET_FR_RNG(const int channel) const;
			std::vector< CAN::byte_t > CAN_GET_FR_48_V() const;
			std::vector< CAN::byte_t > CAN_GET_FR_FPGA_FW_VER(const int channel) const;
			std::vector< CAN::byte_t > CAN_GET_FR_PHASE_SEQ(const bool lower_half) const;
			std::vector< CAN::byte_t > CAN_GET_FR_PHASE_OFFSET() const;
			std::vector< CAN::byte_t > CAN_GET_FR_PAYLOAD(const int channel) const;
			std::vector< CAN::byte_t > CAN_GET_FR_PAYLOAD_STATUS() const;
			std::vector< CAN::byte_t > CAN_GET_FR_LRU_CIN() const;
			std::vector< CAN::byte_t > CAN_GET_TTX_ALARM_STATUS() const;
			std::vector< CAN::byte_t > CAN_GET_TTX_LASER_BIAS(const int channel) const;
			std::vector< CAN::byte_t > CAN_GET_TTX_LASER_PWR(const int channel) const;
			std::vector< CAN::byte_t > CAN_GET_TTX_LASER_TMP(const int channel) const;
			std::vector< CAN::byte_t > CAN_GET_TTX_LASER_ENABLED() const;
			std::vector< CAN::byte_t > CAN_GET_TTX_REG_DATA(const int channel) const;

			/// DTS Transmitter specific controls.
			void CAN_SET_DG_LOAD_TEST_PAT(const std::vector< CAN::byte_t >& data);
			void CAN_SET_FR_CW(const std::vector< CAN::byte_t >& data, const int channel);
			void CAN_SET_FR_PHASE_SEQ(const std::vector< CAN::byte_t >& data, const bool lower_half);
			void CAN_SET_FR_PHASE_OFFSET(const std::vector< CAN::byte_t >& data);
			void CAN_SET_FR_INPUT_TEST(const std::vector< CAN::byte_t >& data, const int channel);
			void CAN_SET_FR_48_V(const std::vector< CAN::byte_t >& data);
			void CAN_SET_FR_TE_RESET(const std::vector< CAN::byte_t >& data);
			void CAN_SET_FR_RESET_CH(const std::vector< CAN::byte_t >& data, const int channel);
			void CAN_SET_FR_RNG(const std::vector< CAN::byte_t >& data, const int channel);
			void CAN_SET_FR_RESET(const std::vector< CAN::byte_t >& data);
			void CAN_SET_FR_CAPTURE_PAYLOAD(const std::vector< CAN::byte_t >& data);
			void CAN_SET_TTX_RESET(const std::vector< CAN::byte_t >& data);
			void CAN_SET_TTX_CLR_ALARMS(const std::vector< CAN::byte_t >& data);
			void CAN_SET_TTX_RESET_FIFO(const std::vector< CAN::byte_t >& data);
			void CAN_SET_TTX_LASER_ENABLE(const std::vector< CAN::byte_t >& data);
			void CAN_SET_TTX_REG_ACCESS(const std::vector< CAN::byte_t >& data, const int channel);

			/** Helper function for 'set' commands to check that data size
			 ** is valid, if not throw exception
			 ** @param data Vector of bytes containing data to check
			 ** @param size Expected number of bytes in data std::vector
			 ** @param pExceptionMsg String identifying CAN msg that is
			 ** associated with failed size check.
			 */
			void checkSize(const std::vector< CAN::byte_t >& data,int size,const char* pExceptionMsg);

			/** Helper method which returns a random number in the range [-std::abs(range/2)..std::abs(range/2)]
			 */
			double noise(const double range) const;

			/** Helper method which returns a random number in the range [0..std::abs(range)]
			 */
			double noise_positive(const double range) const;

			/** Helper method which transforms any data to a Can message. The vector has to
			 * have a preallocated size which determines the message length and the number
			 * of bytes in data which are taken into account.
			 */
			//void rawDataToCanMsg(std::vector< CAN::byte_t >& bytes,void* data) const;

			/// \name Undefined and inaccessible - copying does not make sense.
			//@{
			DTSTransmitter(const DTSTransmitter&);
			DTSTransmitter& operator=(const DTSTransmitter&);
			//@}

			/** Values needed for all simulated devices.
			 */
			//@{
			/// Bus location (node number).
			node_t node_m;
			/// Serial number of this node.
			std::vector< CAN::byte_t > sn_m;
			/// Number of transactions. Needs to be mutable as its incremented by the
			/// monitor function, which is const.
			mutable unsigned int trans_num_m;
			/// Number of errors.
			CAN::byte_t errorCode_m;
			//@}

			/** Simulated properties of the DTS Transmitter.
			 */
			//@{
			typedef struct FR_BOARD_VOLTAGE_t
			{
				std::vector<double> VOLTAGE;
				std::bitset<1> MINUS_5_2_PRESENT;
			};

			mutable double DG_3_3_V_m;
			mutable double DG_5_V_m;
			mutable double DG_TEMP_m;
			std::bitset<1> DG_MODE_m;
			unsigned char DG_FW_VER_m;
			std::vector< std::bitset<8> > FR_TE_STATUS_m;
			std::bitset<16> FR_STATUS_m;
			mutable std::vector<double> FR_1_5_V_m;
			mutable std::vector<double> FR_1_8_V_m;
			mutable FR_BOARD_VOLTAGE_t FR_BOARD_VOLTAGE_m;
			mutable std::vector<double> FR_TMP_m;
			mutable std::vector<double> FR_LASER_PWR_m;
			mutable std::vector<double> FR_LASER_BIAS_m;
			mutable std::vector< std::bitset<4> > FR_INPUT_TEST_m;
			std::vector< std::bitset<8> > FR_CW_m;
			std::vector< std::bitset<2> > FR_RNG_m;
			mutable std::bitset<2> FR_48_V_m;
			std::vector<unsigned long long> FR_PHASE_SEQ_m;
			unsigned int FR_PHASE_OFFSET_m;
			std::bitset<6> FR_PAYLOAD_STATUS_m;
			std::vector< unsigned long long > FR_PAYLOAD_m;
			mutable std::vector< std::bitset<16> > TTX_ALARM_STATUS_m;
			mutable std::vector<double> TTX_LASER_BIAS_m;
			mutable std::vector<double> TTX_LASER_PWR_m;
			mutable std::vector<double> TTX_LASER_TMP_m;
			std::bitset<3> TTX_LASER_ENABLED_m;
			std::vector< std::vector<unsigned char> > TTX_REG_DATA_m;
			//@}
	};				// class DTSTransmitter
}				// namespace AMB

#endif //DTSTransmitter_h
