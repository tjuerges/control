#if !defined(LORR_H)
#define LORR_H
// @(#) $Id$
//
// Copyright (C) 2002, 2004
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif


#include "AMBDevice.h"
#include "CANError.h"
#include <string>
#include <vector>


namespace AMB
{
    /** The LORR class is a simulator for the local oscillator receiver reference
     */
    class LORR: public AMB::Device
    {
        public:
        /** Constructor
         ** @param node Node ID of this device
         ** @param serialNumber S/N of this device
         */
        LORR(node_t node, const std::vector< CAN::byte_t >& serialNumber);

        /// Destructor
        virtual ~LORR();

        /// Return the device's node ID
        virtual node_t node() const;

        /// Return the device's S/N
        virtual std::vector< CAN::byte_t > serialNumber() const;

        /** Monitor interface
         ** @param CAN node RCA
         ** @return std::vector of bytes containing monitor info. Each response
         ** is sized to what's specified in the ICD.
         */
        virtual std::vector< CAN::byte_t > monitor(rca_t rca) const;

        /** Control Interface ('set' functions)
         ** @param rca CAN node RCA
         ** @param data CAN message data required by device
         */
        virtual void control(rca_t rca, const std::vector< CAN::byte_t >& data);

        /// Monitor points for the LORR
        enum Monitor_t
        {
            /// Generic monitor points
            //@{
            GET_SERIAL_NUMBER = 0x00000,
            GET_CAN_ERROR = AMB::Device::GET_CAN_ERROR,
            GET_PROTOCOL_REV_LEVEL = AMB::Device::GET_PROTOCOL_REV_LEVEL,
            GET_TRANS_NUM = AMB::Device::GET_TRANS_NUM,
            GET_AMBIENT_TEMPERATURE = AMB::Device::GET_AMBIENT_TEMPERATURE,
            //@}

            /// Specific LORR data point monitors
            //@{
            STATUS = 0x00001,
            EFC_25_MHZ = 0x00002,
            EFC_2_GHZ = 0x00003,
            PWR_25_MHZ = 0x00004,
            PWR_125_MHZ = 0x00005,
            PWR_2_GHZ = 0x00006,
            RX_OPT_PWR = 0x00007,
            VDC_7 = 0x00008,
            VDC_12 = 0x00009,
            VDC_15 = 0x0000A,
            VDC_MINUS_7 = 0x0000B,
            TE_LENGTH = 0x00011,
            TE_OFFSET_COUNTER = 0x00012,
            READ_MODULE_CODES = 0x00079
        //@}
        };

        /// Only one control for the LORR - reset
        enum Controls_t
        {
            XILINX_PROM_RESET = 0x31001
        };

        enum Test_t
        {
            TEST_STATUS = 0x0F001,
            TEST_EFC_25_MHZ = 0x0F002,
            TEST_EFC_2_GHZ = 0x0F003,
            TEST_PWR_25_MHZ = 0x0F004,
            TEST_PWR_125_MHZ = 0x0F005,
            TEST_PWR_2_GHZ = 0x0F006,
            TEST_RX_OPT_PWR = 0x0F007,
            TEST_VDC_7 = 0x0F008,
            TEST_VDC_12 = 0x0F009,
            TEST_VDC_15 = 0x0F00A,
            TEST_VDC_MINUS_7 = 0x0F00B,
            TEST_TE_LENGTH = 0x00011,
            TEST_TE_OFFSET_COUNTER = 0x00012,
            TEST_EAD_MODULE_CODES = 0x00079
        };

        // Throw this if something bad happens.
        class LORRError: public CAN::Error
        {
            public:
            LORRError(const std::string &error): CAN::Error(error)
            {
            };
        };

        /// Get functions for unit testing
        //@{
        unsigned short getSTATUS() const;
        double getEFC_25_MHZ() const;
        double getEFC_2_GHZ() const;
        double getPWR_25_MHZ() const;
        double getPWR_125_MHZ() const;
        double getPWR_2_GHZ() const;
        double getRX_OPT_PWR() const;
        double getVDC_7() const;
        double getVDC_12() const;
        double getVDC_15() const;
        double getVDC_MINUS_7() const;
        unsigned int getTeLength() const;
        unsigned int getTeOffsetCounter() const;
        std::vector< CAN::byte_t > getReadModuleCodes() const;

        private:
        /// Take the default implementation = return 0,0,0,0
        virtual std::vector< CAN::byte_t > get_can_error() const;

        /// Take the default implementation = return 0,0,0
        virtual std::vector< CAN::byte_t > get_protocol_rev_level() const;

        /// Return the number of transactions
        virtual unsigned int get_trans_num() const;

        /// Temperature on M&C interface board.
        double get_ambient_temperature() const;

        // LORR Measurement Equipment specific monitor points.
        std::vector< CAN::byte_t > CAN_STATUS() const;
        std::vector< CAN::byte_t > CAN_EFC_25_MHZ() const;
        std::vector< CAN::byte_t > CAN_EFC_2_GHZ() const;
        std::vector< CAN::byte_t > CAN_PWR_25_MHZ() const;
        std::vector< CAN::byte_t > CAN_PWR_125_MHZ() const;
        std::vector< CAN::byte_t > CAN_PWR_2_GHZ() const;
        std::vector< CAN::byte_t > CAN_RX_OPT_PWR() const;
        std::vector< CAN::byte_t > CAN_VDC_7() const;
        std::vector< CAN::byte_t > CAN_VDC_12() const;
        std::vector< CAN::byte_t > CAN_VDC_15() const;
        std::vector< CAN::byte_t > CAN_VDC_MINUS_7() const;
        std::vector< CAN::byte_t > CAN_TE_LENGTH() const;
        std::vector< CAN::byte_t > CAN_TE_OFFSET_COUNTER() const;
        std::vector< CAN::byte_t > CAN_READ_MODULE_CODES() const;

        /** Helper function for 'set' commands to check that data size
         ** is valid, if not throw exception
         ** @param data Vector of bytes containing data to check
         ** @param size Expected number of bytes in data std::vector
         ** @param pExceptionMsg String identifying CAN msg that is
         ** associated with failed size check.
         */
        void checkSize(const std::vector< CAN::byte_t > &data, int size,
            const char *pExceptionMsg);

        void toDouble(const std::vector< CAN::byte_t >& data, int size,
            double& var);

        /// \name Undefined and inaccessible - copying does not make sense.
        //@{
        LORR(const LORR &);
        LORR& operator=(const LORR&);
        //@}

        /// Bus location (node number).
        node_t m_node;

        /// Serial number of this node.
        std::vector< CAN::byte_t > m_sn;

        /// Number of transactions. Needs to be mutable as its incremented by the
        /// monitor function, which is const.
        mutable unsigned int m_trans_num;
        // values for all the monitor points
        CAN::byte_t m_errorCode;
        unsigned short STATUS_m;
        double EFC_25_MHZ_m;
        double EFC_2_GHZ_m;
        double PWR_25_MHZ_m;
        double PWR_125_MHZ_m;
        double PWR_2_GHZ_m;
        double RX_OPT_PWR_m;
        double VDC_7_m;
        double VDC_12_m;
        double VDC_15_m;
        double VDC_MINUS_7_m;
        unsigned int TE_LENGTH_m;
        unsigned int TE_OFFSET_COUNTER_m;
        std::vector< CAN::byte_t > READ_MODULE_CODES_m;
    }; // class LORR
} // namespace AMB

#endif  // LORR_H
