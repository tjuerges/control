#ifndef SHAREDSIMWRAPPER_H
#define SHAREDSIMWRAPPER_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2005 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* rhiriart  2005-09-20  created
*/

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <SharedSimulatorC.h>
#include <iostream>
#include <sstream>
#include "AMBUtil.h"


// SharedSimulator Antenna position update period [s]
const double SS_ANTPOS_PERIOD_ = 0.5;

// SharedSimulator update period [s]
const double SS_UPDATE_PERIOD_ = 1.0;

/**
 * \brief Adapter class to interface AMBLBSimulator with TELCAL
 * SharedSimulator.
 *
 * AMBLBSimulator works by dispatching CAN messages to several device
 * simulators. A pointer to a SharedSimWrapper is passed to the
 * device simulators (see AMBDevice.h), which will invoke
 * SharedSimWrapper functions when receiving selected monitoring or
 * control CAN messages.
 */
class SharedSimWrapper {
 public:

  /**
   * \brief Constructor.
   *
   * The constructor expects to receive the CLI parameters passed to
   * AMBLBSimulator. The constructor recognizes two CLI paramenters:
   * \li \c -sharedsim Use the SharedSimulator
   * \li <tt>-antenna number</tt> Sets up the antenna number
   *
   * If the \c sharedsim argument is passed, the SharedSimulator
   * component will be contacted using \c maci::SimpleClient and 
   * turned on.
   *
   * If no antenna number is passed, it will default to 0.
   */
  SharedSimWrapper() {};
  SharedSimWrapper(int antenna, SHARED_SIMULATOR::SharedSimulator_var ss);

  /**
   * \brief Destructor.
   * 
   * The destructor will free the reference to TELCAL SharedSimulator.
   */
  ~SharedSimWrapper();

  /**
   * \brief Get down converter powers from the SharedSimulator.
   * 
   * The SharedSimulator seems to give the power from the downconverters
   * in Kelvin. In order to transform this value to the output from
   * the IFProc board, the square law detector transfer function would be
   * required. Right now we're assuming this transformation to be
   * just the unity.
   * \param pol Polarization
   * \return A vector of bytes encoded in way required by the CAN
   * bus. Two shorts are packed in this vector, containing
   * the sideband 0 and sideband 1 powers, for the given polarization.
   */
  std::vector<CAN::byte_t> getDownConvPowers(short pol);

  /**
   * \brief Get baseband powers from the SharedSimulator.
   * 
   * The SharedSimulator seems to give the baseband powers
   * in Kelvin. In order to transform this value to the output from
   * the IFProc board, the square law detector transfer function would be
   * required. Right now we're assuming this transformation to be
   * just the unity.
   * \param pol Polarization
   * \return A vector of bytes encoded in way required by the CAN
   * bus. Four shorts are packed in this vector, containing
   * the detected powers for channe A, B, C, and D respectively, 
   * for the given polarization.
   */
  std::vector<CAN::byte_t> getBaseBandPowers(short pol);

  /**
   * \brief Get downconverter and baseband attenuations from
   * the SharedSimulator.
   *
   * \param pol Polarization
   * \return A vector of bytes encoded in the way required by the
   * CAN bus. Six bytes are packed into the vector, which are, for
   * the given polarization:
   * \li Sideband 0 attenuation
   * \li Sideband 1 attenuation
   * \li Channel A attenuation
   * \li Channel B attenuation
   * \li Channel C attenuation
   * \li Channel D attenuation
   */
  std::vector<CAN::byte_t> getGains(short pol);

  /**
   * \brief Set IF Processor gains in SharedSimulator.
   * \param pol Polarization
   * \param data Vector of bytes in the format used by the CAN bus.
   * It should contain six bytes:
   * \li Sideband 0 gain
   * \li Sideband 1 gain
   * \li Baseband channel A gain
   * \li Baseband channel B gain
   * \li Baseband channel C gain
   * \li Baseband channel D gain
   */
  void setGains(short pol, const std::vector<CAN::byte_t>& data);

  /**
   * \brief Set the antenna position in the SharedSimulator.
   * \param az Antenna azimuth
   * \param el Antenna elevation
   */
  void setAntPosn(double az, double el);

  /**
   * \brief Set the coarse 2nd LO frequency in the SharedSimulator.
   *
   * \param baseband Which baseband, 0 or 1.
   * \param freq Frequency [MHz], range is 8000 MHz - 14000 MHz.
   */
  void setSecondLOFreq(short baseband, double freq);

  /**
   * \brief Set downcoverter switches.
   * 
   * \param pol Polarization.
   * \param data Vector of bytes encoded in the way specified by the
   * IFProc/Control ICD.
   */
  void setSignalPaths(short pol, const std::vector<CAN::byte_t>& data);

  /** Do we have an ACS reference to TELCAL SharedSimulator component? */
  bool haveSimulatorRef();

 protected:
  /** An ACS reference to TELCAL SharedSimulator. */
  SHARED_SIMULATOR::SharedSimulator_var simulator;
	
  /** The antenna number. */
  int antennaNumber;

  /** Sets up several values in the SharedSimulator in order to run
   * a simulation. */
  void setDefaults();

};

#endif /*!SHAREDSIMWRAPPER_H*/
