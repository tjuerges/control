/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2002 
*
* This code is under GNU General Public Licence (GPL). 
* Correspondence concerning ALMA Software should be addressed to:
* alma-sw-admin@nrao.edu
*
* Copyright (C) 2001
* Associated Universities, Inc. Washington DC, USA.
*
* Produced for the ALMA project
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Library General Public License
* as published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Library General Public License for more details.
*
* You should have received a copy of the GNU Library General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* dfugate  16/12/02  created 
*/

#ifndef DOWNCONVERTER_H
#define DOWNCONVERTER_H

#include "AMBDevice.h"
#include "CANError.h"
#include <string>

namespace AMB 
{
    // Valid for draft DownConverter ICD ALMA-50.01.00.00-70.35.16.00-A
    class DownConverter : public AMB::Device
    {
      public:
	////////////////////////////////////////////////////////////////////
	DownConverter(node_t node, const std::vector<CAN::byte_t> &serialNumber);

	virtual ~DownConverter();


	////////////////////////////////////////////////////////////////////
	virtual node_t 
	node() const;

	virtual std::vector<CAN::byte_t> 
	serialNumber() const;

	////////////////////////////////////////////////////////////////////
	virtual std::vector<CAN::byte_t> 
	monitor(rca_t rca) const;

	virtual void 
	control(rca_t rca, const std::vector<CAN::byte_t> &data);

	////////////////////////////////////////////////////////////////////
	/// Take the default implementation = return 0,0,0,0
	virtual std::vector<CAN::byte_t>  
	get_can_error() const;

	/// Take the default implementation = return 0,0,0
	virtual std::vector<CAN::byte_t>  
	get_protocol_rev_level() const;

	/// Return the number of transactions
	virtual unsigned int 
	get_trans_num() const;

	/// Temperature on M&C interface board. 18.0 constant.
	float 
	get_ambient_temperature() const;

	////////////////////////////////////////////////////////////////////
   	/// Monitor control lines for each of 6 digital attenuators.  Only the
	/// the first six bytes are relavent.  They are initialized to 0, but 
	/// change after a set_att_* command.
	std::vector<CAN::byte_t> 
	read_attenuators() const;

	/// Monitor control lines for each of 6 SP2T switches.  Only the
	/// the first six bits of the first byte  are relavent.  They are 
	/// initialized to 0, but change after a set_tpd_switches command.
	std::vector<CAN::byte_t> 
	read_tpd_switches() const;

	/// Monitor control lines for the 2x2 matrix switches.  Only the
	/// the first six bits are relevent.  They are initialized to 0, but 
	/// change after a set_matrix_switches command.
	std::vector<CAN::byte_t> 
	read_matrix_switches() const;

	/// Indicates PIC CPU error occured.  Only the first 3 bits of 
	/// the first byte is relevent.  Always 0!
	std::vector<CAN::byte_t> 
	pic_error_code() const;

	/// Returns a std::vector consisting of four integers.
	/// All four integers are 127
	std::vector<CAN::byte_t> 
	read_4_shorts() const;

	/// Returns a std::vector consisting of three integers.
	/// All three integers are 127
	std::vector<CAN::byte_t> 
	read_3_shorts() const;

	////////////////////////////////////////////////////////////////////
	/// Set control lines to SP2T switches - actual simulation here
	void 
	set_tpd_switches(CAN::byte_t state);

	/// Set control lines to 2x2 matrix switches - actual simulation here
	void 
	set_matrix_switches(CAN::byte_t state);

	/// Set the input power level to the IF-U total power detectors
	void 
	set_att_u(CAN::byte_t state);

	/// Set the input power level to the IF-L total power detectors
	void 
	set_att_l(CAN::byte_t state);

	/// Set the input power level to the BB-A total power detectors
	void 
	set_att_a(CAN::byte_t state);

	/// Set the input power level to the BB-B total power detectors
	void 
	set_att_b(CAN::byte_t state);

	/// Set the input power level to the BB-C total power detectors
	void 
	set_att_c(CAN::byte_t state);

	/// Set the input power level to the BB-D total power detectors
	void 
	set_att_d(CAN::byte_t state);

	////////////////////////////////////////////////////////////////////
	// Throw this if something bad happens.
	class DownConverterError : 
	    public CAN::Error 
	{
	  public:
	    DownConverterError(const std::string &error) : CAN::Error(error) {;}
	};

	////////////////////////////////////////////////////////////////////
	enum Monitor_t 
	{
	    // Generic monitor points
	    GET_CAN_ERROR           = AMB::Device::GET_CAN_ERROR,
	    GET_PROTOCOL_REV_LEVEL  = AMB::Device::GET_PROTOCOL_REV_LEVEL,
	    GET_TRANS_NUM           = AMB::Device::GET_TRANS_NUM,
	    GET_AMBIENT_TEMPERATURE = AMB::Device::GET_AMBIENT_TEMPERATURE,

	    // Monitor points specific to the DC (from the ICD).
	    READ_ATTENUATORS           = 0x00000001,
	    READ_TPD_SWITCHES          = 0x00000002,
	    READ_MATRIX_SWITCHES       = 0x00000003,
	    PIC_ERROR_CODE             = 0x00000004,
	    READ_TEMPS_IF              = 0x00000005,
	    READ_TEMPS_BB              = 0x00000006,
	    READ_TEMPS_MC              = 0x00000007,
	    READ_VR_15_AND_15SEQ_IF    = 0x00000009,
	    READ_VR_5_AND_7_AND_15H_IF = 0x0000000A,
	    READ_VR_5_AND_16_BB        = 0x0000000B,
	    READ_VR_5_AND_16_MC        = 0x0000000C
	};

	enum Controls_t 
	{
	    SET_TPD_SWITCHES       = 0x0000000E,
	    SET_MATRIX_SWITCHES    = 0x0000000F,
	    SET_ATT_U              = 0x00000010,
	    SET_ATT_L              = 0x00000011,
	    SET_ATT_A              = 0x00000012,
	    SET_ATT_B              = 0x00000013,
	    SET_ATT_C              = 0x00000014,
	    SET_ATT_D              = 0x00000015,
	};

	////////////////////////////////////////////////////////////////////
      private:

	// Undefined and inaccessible - copying does not make sense.
	DownConverter(const DownConverter &);

	DownConverter &operator=(const DownConverter &);


	////////////////////////////////////////////////////////////////////
	/// Bus location (node number).
	node_t node_m;

	/// Serial number of this node.
	std::vector<CAN::byte_t> sn_m;
	
	/// Number of transactions.
	mutable unsigned int trans_num_m;

	/// Specific to DC
	mutable unsigned char attenuators_m[6];
	mutable unsigned char tpd_switches_m;
	mutable unsigned char matrix_switches_m;
	
    };

} 

#endif  // DOWNCONVERTER_H
