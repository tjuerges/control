// @(#) $Id$
//
// Copyright (C) 2002, 2006
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#if !defined(HOLOGRAPHY_RECEIVERH)
#define HOLOGRAPHY_RECEIVERH

#include "AMBDevice.h"
#include "CANError.h"
#include <string>

namespace AMB {
  /** The HolographyReceiver class is a simulator for the node 2, the receiver module
   ** of the Holography Measurement Equipment device. 
   ** There are 2 nodes to this device, one to obtain the integrated holography
   ** data -- covered by the HolographyDSP class  & node 2 which is the M&C for the
   ** holography receiver -- covered here.
   ** See test/testHolography.cpp or src/AMBSimulator.cpp for usage guidance.
   */
  class HolographyReceiver: public AMB::Device {
  public:
    /// Constructor with node and serial number
    HolographyReceiver(node_t node, const std::vector<CAN::byte_t> &serialNumber);
    /// Destructor
    virtual ~HolographyReceiver();
    
    /// Return this device's node ID
    virtual node_t node() const;

    /// Return this device's serial number
    virtual std::vector<CAN::byte_t> serialNumber() const;
        
    /** Monitor interface
     ** @param CAN node RCA
     ** @return std::vector of bytes containing monitor info. Each response is
     ** sized to what's specified in the ICD.
     */
    virtual std::vector<CAN::byte_t> monitor(rca_t rca) const;

    /** Control Interface ('set' functions)
     ** @param rca CAN node RCA
     ** @param data CAN message data required by device
     */
    virtual void control(rca_t rca, const std::vector<CAN::byte_t> &data);

    enum Monitor_t {
      GET_CAN_ERROR           = AMB::Device::GET_CAN_ERROR,
      GET_PROTOCOL_REV_LEVEL  = AMB::Device::GET_PROTOCOL_REV_LEVEL,
      GET_TRANS_NUM           = AMB::Device::GET_TRANS_NUM,
      GET_AMBIENT_TEMPERATURE = AMB::Device::GET_AMBIENT_TEMPERATURE,
      
      GET_PLL_STATUS          = 0x00000001,
      GET_GUNN_H_VOLTAGE      = 0x00000002,
      GET_GUNN_L_VOLTAGE      = 0x00000003,
      GET_LO_DET_OUT          = 0x00000004,
      GET_REF_DET_OUT         = 0x00000005,
      GET_SIG_DET             = 0x00000006,
      GET_REF_SENSE_I         = 0x00000007,
      GET_REF_SENSE_Q         = 0x00000008,
      GET_SIG_SENSE_I         = 0x00000009,
      GET_SIG_SENSE_Q         = 0x0000000A,
      GET_SUPPLY_CURRENT      = 0x0000000B,
      GET_TEMP_POWER_SUPPLY   = 0x0000000C,
      GET_TEMP_SIG_MIX        = 0x0000000D,
      GET_TEMP_REF_MIX        = 0x0000000E,
      GET_TEMP_29MHZ_OCXO     = 0x0000000F,
      GET_TEMP_95MHZ_OCXO     = 0x00000010,
      GET_TEMP_LOCK_BOX       = 0x00000011,
      GET_SIG_ATTENUATION     = 0x00001001,
      GET_REF_ATTENUATION     = 0x00001002,
      GET_SYNTH_FREQUENCY     = 0x00001003,
      GET_DRIVE_ENABLE        = 0x00001004,
      GET_LOOP_ENABLE         = 0x00001005,
      GET_NOMINAL_VOLTAGE     = 0x00001006,
      GET_GUNN_LOOP_GAIN      = 0x00001007,
      GET_GUNN_TUNE_RANGE     = 0x00001008,
      GET_GUNN_SELECT         = 0x00001009
    };

    enum Controls_t {
      SET_SIG_ATTENUATION     = 0x00001001,
      SET_REF_ATTENUATION     = 0x00001002,
      SET_SYNTH_FREQUENCY     = 0x00001003,
      SET_DRIVE_ENABLE        = 0x00001004,
      SET_LOOP_ENABLE         = 0x00001005,
      SET_NOMINAL_VOLTAGE     = 0x00001006,
      SET_GUNN_LOOP_GAIN      = 0x00001007,
      SET_GUNN_TUNE_RANGE     = 0x00001008,
      SET_GUNN_SELECT         = 0x00001009,
      RESET_PLL_STATUS        = 0x0000100A
    };

    /**\name Get Functions
     ** These get functions retrieve data directly & can be used
     ** to compare with the gets via the monitor() function.
     */
    //@{
    short getPLL_STATUS() const {
      return PLLStatus_m;
    }
    
    float getGUNN_H_VOLTAGE() const {
      return gunnHVoltage_m;
    }
    
    float getGUNN_L_VOLTAGE() const {
      return gunnLVoltage_m;
    }
        
    float getLO_DET_OUT() const {
      return LOLevel_m;
    }

    float getREF_DET_OUT() const {
      return RefIFLevel_m;
    }
        
    float getSIG_DET() const {
      return SigIFLevel_m;
    }
        
    float getREF_SENSE_I() const {
      return RefIChannelRMS_m;
    }
    
    float getREF_SENSE_Q() const {
      return RefQChannelRMS_m;
    }
    
    float getSIG_SENSE_I() const {
      return SigIChannelRMS_m;
    }
        
    float getSIG_SENSE_Q() const {
      return SigQChannelRMS_m;
    }
        
    float getSUPPLY_CURRENT() const {
      return supplyCurrent_m;
    }
        
    float getTEMP_POWER_SUPPLY() const {
      return tempPS_m;
    }
        
    float getTEMP_SIG_MIX() const {
      return tempSignalChannelMixer_m;
    }
    
    float getTEMP_REF_MIX() const {
      return tempReferenceChannelMixer_m;
    }
    
    float getTEMP_29MHZ_OCXO() const {
      return temp29MHzOCXO_m;
    }
    
    float getTEMP_95MHZ_OCXO() const {
      return temp95MHzOCXO_m;
    }
    
    float getTEMP_LOCK_BOX() const {
      return tempLockBox_m;
    }

    //@}

    // Throw this if something bad happens.
    class HolographyReceiverError : public CAN::Error {
    public:
      HolographyReceiverError(const std::string &error) : CAN::Error(error) {}
    };

  private:
    /// Take the default implementation = return 0,0,0,0
    virtual std::vector<CAN::byte_t>  get_can_error() const;
    
    /// Take the default implementation = return 0,0,0
    virtual std::vector<CAN::byte_t>  get_protocol_rev_level() const;
    
    /// Return the number of transactions
    virtual unsigned int get_trans_num() const;
    
    /// Temperature on M&C interface board. 22.0 constant.
    float get_ambient_temperature() const;
    
    /// Status of phased lock loop
    std::vector<CAN::byte_t> getPLL_Status() const;
    
    /// Get tuning voltage of high band Gunn Oscillator
    std::vector<CAN::byte_t> getGunnH_Voltage() const;
    
    /// Get tuning voltage of low band Gunn Oscillator
    std::vector<CAN::byte_t> getGunnL_Voltage() const;
    
    /// Get local oscillator (LO) level
    std::vector<CAN::byte_t> getLODetOut() const;
    
    /// Get reference intermediate frequency (IF) level
    std::vector<CAN::byte_t> getRefDetOut() const;
    
    /// Get signal intermediate frequency (IF) level
    std::vector<CAN::byte_t> getSigDet() const;
    
    /** Get the RMS voltage of the Ref. I channel at the Anti-Aliasing
     ** module output
     */
    std::vector<CAN::byte_t> getRefSenseI() const;
    
    /** Get the RMS voltage of the Ref. Q channel at the Anti-Aliasing
     ** module output
     */
    std::vector<CAN::byte_t> getRefSenseQ() const;
    
    /** Get the RMS voltage of the Signal I channel at the Anti-Aliasing 
     ** module output
     */
    std::vector<CAN::byte_t> getSigSenseI() const;
    
    /** Get the RMS voltage of the Signal Q  channel at the Anti-Aliasing
     ** module output
     */
    std::vector<CAN::byte_t> getSigSenseQ() const;
    
    /// Get the supply current in A for the entire holography receiver
    std::vector<CAN::byte_t> getSupplyCurrent() const;
    
    /* Get the temperature reading in ?C from temperature sensor by 
    ** power supply
    */
    std::vector<CAN::byte_t> getTempPowerSupply() const;
    
    /** Get the temperature reading in C from temperature sensor by mixer
     ** in signal channel
     */
    std::vector<CAN::byte_t> getTempSigMix() const;
    
    /** Get the temperature reading in ?C from temperature sensor by 
     ** mixer in reference channel
     */
    std::vector<CAN::byte_t> getTempRefMix() const;
    
    /** Get the temperature reading in ?C from temperature sensor by 29
     **  MHz oven-controlled crystal oscillator
     */
    std::vector<CAN::byte_t> getTemp29MHzOCXO() const;
    
    /** Get the temperature reading in ?C from temperature sensor by 95
     ** MHz oven-controlled crystal oscillator
     */
    std::vector<CAN::byte_t> getTemp95MHzOCXO() const;
    
    /** Get the temperature reading in ?C from temperature sensor by lock
     ** box
     */
    std::vector<CAN::byte_t> getTempLockBox() const;
    
    /** Get attenuation of signal channel
     */
    std::vector<CAN::byte_t> getSigAttenuation() const;
    
    /** Get attenuation of reference channel
     */
    std::vector<CAN::byte_t> getRefAttenuation() const;

    /** Get holography synthesizer frequency.
     */
    std::vector<CAN::byte_t> getSynthFrequency() const;

    /** Returns whether the Gunn oscillator bias has been turned on/off.
     */
    std::vector<CAN::byte_t> getDriveEnable() const;

    /** Returns whether the Gunn oscillator loop has been opened or closed.
     */
    std::vector<CAN::byte_t> getLoopEnable() const;

    /** Returns the last commanded value to the DAC that determines the bias
	voltage to the Gunn oscillator.
     */
    std::vector<CAN::byte_t> getNominalVoltage() const;

    /** Returns the last commanded value to the loop gain of the phase locked
	Gunn oscillator.
     */
    std::vector<CAN::byte_t> getGunnLoopGain() const;

    /** Returns the last commanded value to the tunming rtange of the Gunn
	oscillator.
     */
    std::vector<CAN::byte_t> getGunnTuneRange() const;

    /** Returns whether the high or low band Gunn has been selected.
     */
    std::vector<CAN::byte_t> getGunnSelect() const;

    /** \name Controls
     ** Set an internal value for a given monitor control point. Each call
     ** checks that the CAN msg contains the expected number of bytes and
     ** that it passes a basic range check.
     ** @param data CAN message as a std::vector<bytes>
     ** @param size Expected number of bytes for the CAN msg
     ** @param pMsg defines an identifier to be printed on error.
     */
    //@{
    /** Set attenuation of signal channel
     */
    void setSigAttenuation( const std::vector<CAN::byte_t> &data, int size,
                            const char *pMsg);
    /** Set attenuation of reference channel.
     */
    void setRefAttenuation( const std::vector<CAN::byte_t> &data, int size,
                            const char *pMsg);
    /** Set holography synthesizer frequency.
     */
    void setSynthFrequency(const std::vector<CAN::byte_t> &data, int size,
                           const char *pMsg);
    /** Turn the Gunn oscillator bias on/off.
     ** Affects oscillator lock bit in PLL_STATUS
     */
    void setDriveEnable(const std::vector<CAN::byte_t> &data, int size,
                        const char *pMsg);
    /** To open or close the phased lock loop to the Gunn oscillator.
     ** Affects Synthesizer lock bit of PLL_STATUS
     */
    void setLoopEnable(const std::vector<CAN::byte_t> &data, int size,
                       const char *pMsg);
    /** To set the output voltage of the 12-bit multiplying DAC (LTC8043),
     ** which in turn will determine the bias voltage to the Gunn oscillator.
     */
    void setNominalVoltage(const std::vector<CAN::byte_t>& data,
                           const char *pMsg);

    /** Set the loop gain of the phased locked Gunn oscillator
     */
    void setGunnLoopGain(const std::vector<CAN::byte_t> &data,
                         const char *pMsg);
    /** Set tuning range of Gunn oscillator
     */
    void setGunnTuneRange(const std::vector<CAN::byte_t> &data,
                          const char *pMsg);
    /** Select high-band Gunn (GUNN_H) or low-band Gunn (GUNN_L)
     */
    void setGunnSelect(const std::vector<CAN::byte_t> &data, int size,
                       const char *pMsg);

    /** Reset Gunn lock status bits &/or Synthesizer lock status bit
     */
    void resetPLLStatus(const std::vector<CAN::byte_t> &data, int size,
                        const char *pMsg);
    //@}
    
    /** Helper function for 'set' commands to check that data size
     ** is valid, if not throw exception
     ** @param data Vector of bytes containing data to check
     ** @param size Expected number of bytes in data std::vector
     ** @param pExceptionMsg String identifying CAN msg that is
     ** associated with failed size check.
     */
    void checkSize( const std::vector<CAN::byte_t> &data, int size,
                    const char *pExceptionMsg);
    
    /** Helper function for set commands to check range of value
     ** w/ byte args
     ** @param value Byte value to check
     ** @param maxValue Maximum value that 'value' can be
     ** @param pMsg String identifying CAN message for this check
     */
    void checkRange( CAN::byte_t value, CAN::byte_t maxValue,
                     const char *pMsg) const;
    
    /** Helper function for set commands to check range of value
     ** w/ short args
     ** @param value Short integer value to check
     ** @param maxValue Maximum value that 'value' can be
     ** @param pMsg String identifying CAN message for this check
     */
    void checkRange( short value, short maxValue,
                     const char *pMsg) const;
    
    /** Helper function for set commands to check range of value
     ** w/ float args
     ** @param value float value to check
     ** @param minValue Minimum value that 'value' can be
     ** @param maxValue Maximum value that 'value' can be
     ** @param pMsg String identifying CAN message for this check
     */
    void checkRange( float value, float minValue, float maxValue,
                     const char *pMsg) const;
    
    /// \name Undefined and inaccessible - copying does not make sense.
    //@{
    HolographyReceiver (const HolographyReceiver &);
    HolographyReceiver &operator=(const HolographyReceiver &);
    //@}
    
    /// Bus location (node number).
    node_t node_m;
    
    /// Serial number of this node.
    std::vector<CAN::byte_t> sn_m;
    
    /// Number of transactions.
    mutable unsigned int trans_num_m;
    
    // PLL status byte
    CAN::byte_t PLLStatus_m;
    
    /// Gunn High band voltage
    float gunnHVoltage_m;
    
    /// Gunn Low band voltage
    float gunnLVoltage_m;
    
    /// LO level
    float LOLevel_m;
    
    /// Reference IF level
    float RefIFLevel_m;
    
    /// Signal IF level
    float SigIFLevel_m;
    
    /// RMS voltage of Ref I channel
    float RefIChannelRMS_m;

    /// RMS voltage of Ref Q channel
    float RefQChannelRMS_m;
    
    /// RMS voltage of Signal Q channel
    float SigIChannelRMS_m;
    
    /// RMS voltage of Signal Q channel
    float SigQChannelRMS_m;
    
    /// Supply current A
    float supplyCurrent_m;
    
    /// Temp in C of PS
    float tempPS_m;
    
    /// Temp in C of Signal Channel Mixer
    float tempSignalChannelMixer_m;
    
    /// Temp in C of Reference Channel Mixer
    float tempReferenceChannelMixer_m;
    
    /// Temp in C of 29 MHz oven-controlled crystal oscillator
    float temp29MHzOCXO_m;
    
    /// Temp in C of 29 MHz oven-controlled crystal oscillator
    float temp95MHzOCXO_m;
    
    /// Temp in C of lock box
    float tempLockBox_m;
    
    /// Attenuation of the signal channel
    short sigAttenuation_m;

    /// Attenuation of the reference channel
    short refAttenuation_m;

    /// Synthesiser frequency
    short synthFrequency_m;

    /// Bias on=true
    bool driveEnable_m;

    /// Loop closed=true
    bool loopEnable_m;

    /// Gunn voltage
    float nominalVoltage_m;

    /// loop gain
    float gunnLoopGain_m;

    /// tune range
    float gunnTuneRange_m;

    /// gunn select high band=true
    bool gunnSelect_m;

  };  // class HolographyReceiver
}  // namespace AMB

#endif  // HOLOGRAPHY_RECEIVER_H
