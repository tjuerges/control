#ifndef DCTODCPOWERSUPPLY_H
#define DCTODCPOWERSUPPLY_H
// @(#) $Id$
//
// Copyright (C) 2005
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Library General Public License
// as published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Library General Public License for more details.
//
// You should have received a copy of the GNU Library General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
//
// Correspondence concerning ALMA should be addressed as follows:
//
//        Internet email: alma-sw-admin@alma.org
//

#include "AMBDevice.h"
#include "CANError.h"
#include <string>

namespace AMB {
  class DCtoDCPowerSupply: public AMB::Device {
  public:
    /** Constructor
     ** @param node Node ID of this device
     ** @param serialNumber S/N of this device
     */
    DCtoDCPowerSupply(node_t node, 
		      const std::vector<CAN::byte_t>& serialNumber);
    
    /// Destructor
    virtual ~DCtoDCPowerSupply();

    /// Return the device's node ID
    virtual node_t node() const;

    /// Return the device's S/N
    virtual std::vector<CAN::byte_t> serialNumber() const;

    /** Monitor interface
     ** @param CAN node RCA
     ** @return std::vector of bytes containing monitor info. Each response is
     ** sized to what's specified in the ICD.
     */
    virtual std::vector<CAN::byte_t> monitor(rca_t rca) const;

    /** Control Interface ('set' functions)
     ** @param rca CAN node RCA
     ** @param data CAN message data required by device
     */
    virtual void control(rca_t rca, const std::vector<CAN::byte_t> &data);

    /// Return the number of transactions
    virtual unsigned int get_trans_num() const;

     /// Temperature on M&C interface board. 18.0 constant.
    float get_ambient_temperature() const;
    /// Error and Alarms monitor point (0x00001)
    std::vector<CAN::byte_t> error_and_alarms() const;
    /// Instantaneous measured voltage on the +7 Volt converter output
    std::vector<CAN::byte_t> voltage_7v() const;

    /// Instantaneous measured current drawn on the +7 Volt converter output
    std::vector<CAN::byte_t> current_7v() const;

    /// Instantaneous measured voltage on the -7 Volt converter output
    std::vector<CAN::byte_t> voltage_minus_7v() const;

    /// Instantaneous measured current drawn on the -7 Volt converter output
    std::vector<CAN::byte_t> current_minus_7v() const;

    /// Instantaneous measured voltage on the +17 Volt converter output
    std::vector<CAN::byte_t> voltage_17v() const;

    /// Instantaneous measured current drawn on the +17 Volt converter output
    std::vector<CAN::byte_t> current_17v() const;

    /// Instantaneous measured voltage on the -17 Volt converter output
    std::vector<CAN::byte_t> voltage_minus_17v() const;

    /// Instantaneous measured current drawn on the -17 Volt converter output
    std::vector<CAN::byte_t> current_minus_17v() const;
    
    std::vector<CAN::byte_t> max_min_7v() const;
    std::vector<CAN::byte_t> max_min_minus_7v() const;
    std::vector<CAN::byte_t> max_min_17v_a() const;
    std::vector<CAN::byte_t> max_min_minus_17v() const;
    std::vector<CAN::byte_t> max_min_17v_b() const;
    
    // shutdown the output
    void shutdown_com(const std::vector<CAN::byte_t>& command);

    // Throw this if something bad happens.
    class DCtoDCPowerSupplyError : 
      public CAN::Error {
    public:
      DCtoDCPowerSupplyError(const std::string& error) : CAN::Error(error) {;}
    };

    ////////////////////////////////////////////////////////////////////
    enum Monitor_t {
      // Generic monitor points
      GET_SERIAL_NUMBER       = 0x00000,
      GET_CAN_ERROR           = AMB::Device::GET_CAN_ERROR,
      GET_PROTOCOL_REV_LEVEL  = AMB::Device::GET_PROTOCOL_REV_LEVEL,
      GET_TRANS_NUM           = AMB::Device::GET_TRANS_NUM,
      GET_AMBIENT_TEMPERATURE = AMB::Device::GET_AMBIENT_TEMPERATURE,
      
      // Monitor points specific to the DC to DC Power supply (from the ICD).
      ERROR_AND_ALARMS		= 0x00001,
      VOLTAGE_7V	      = 0x00005,
      CURRENT_7V	      = 0x00006,
      VOLTAGE_MINUS_7V	      = 0x00007,
      CURRENT_MINUS_7V	      = 0x00008,
      VOLTAGE_17V	      = 0x00009,
      CURRENT_17V	      = 0x0000A,
      VOLTAGE_MINUS_17V	      = 0x0000B,
      CURRENT_MINUS_17V	      = 0x0000C,
      MAX_MIN_7V			=0x00020,
      MAX_MIN_MINUS_7V		=0x00021,
      MAX_MIN_17V_A			=0x00022,
      MAX_MIN_MINUS_17V		=0x00023,
      MAX_MIN_17V_B			=0x00024
    };
    
    enum Controls_t {
      SHUTDOWN_COM	   = 0X00081
    };

  private:
    // Undefined and inaccessible - copying does not make sense. See 
    // "Effective C++ Second edition" item 45 for details on what 
    // functions the compiler generates for you.
    DCtoDCPowerSupply(const DCtoDCPowerSupply&);
    DCtoDCPowerSupply& operator=(const DCtoDCPowerSupply&);
    
    /// Bus location (node number).
    node_t node_m;
    
    /// Serial number of this node.
    std::vector<CAN::byte_t> sn_m;
    
    /// Number of transactions. Needs to be mutable as its incremented by the
    /// monitor function, which is const.
    mutable unsigned int trans_num_m;

    // values for all the monitor points
    CAN::byte_t m_errorCode;
    bool shutdown_m;
    unsigned long int error_and_alarms_m;
    unsigned char max_min_7v_m[6]; //6 bytes according to ICD
    unsigned char max_min_minus_7v_m[6]; //6 bytes according to ICD
    unsigned char max_min_17v_a_m[6]; //6 bytes according to ICD
    unsigned char max_min_minus_17v_m[6]; //6 bytes according to ICD
    unsigned char max_min_17v_b_m[6]; //6 bytes according to ICD
    double voltage_7v_m;
    double current_7v_m;
    double voltage_minus_7v_m;
    double current_minus_7v_m;
    double voltage_17v_m;
    double current_17v_m;
    double voltage_minus_17v_m;
    double current_minus_17v_m;
  };
}
#endif  // DCTODCPOWERSUPPLY_H
