// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#if !defined(PTC_H)
#define PTC_H

#include <string>
#include "AMBDevice.h"
#include "AMBUtil.h"
#include "CANError.h"
#include <vector>

namespace AMB
{

/**
 * A concrete class that implements a CAN bus simulator for the Vertex 
 * PoinTing Computer (PTC).  This class contains all interface routines to 
 * access the PTC simulator through the CAN bus.
 *
 * This class is derived from the PTC class (see AMBSimPTC.h) to be the
 * equivalent of the PTChw class (ticsAntVA module) that is also derived from
 * the PTC class.  This class accesses the PTC simulator, while the PTChw
 * class accesses the PTC computer hardware.
 *
 * This class is also derived from the AMB::Device class.
 */
class PTC : public AMB::Device
{
  public:

    PTC(node_t node,
	const std::vector<CAN::byte_t>& serialNumber);

    ~PTC();

    //
    // Monitor Requests
    //

    //
    // Control Commands
    //

    //
    // AMB::Device functions
    //
    node_t node() const;
    std::vector<CAN::byte_t> serialNumber() const;
    std::vector<CAN::byte_t> monitor(rca_t rca) const;
    void control(rca_t rca,const std::vector<CAN::byte_t>& data);
    unsigned int get_trans_num() const;

    //
    // PTC specializations
    //
    // throw these errors if something bad happens
    class PTCError : public CAN::Error
    {
      public:
	PTCError(const std::string& error) : CAN::Error(error) {;}
    };
        
    /*
     * CAN Addresses for PTC M&C Points.
     * These are used in the monitor() and control() switch statements.
     */
    enum Monitor_t
    {
	GET_AC_STATUS           = 0x00002,
//	GET_CAN_ERROR           = 0x30001,
        GET_IP_ADDRESS          = 0x00014,
        GET_METR_EQUIP_STATUS   = 0x00010,
        GET_METR_MODE           = 0x00011,
        GET_METR_TEMPS_1        = 0x04000,
        GET_METR_TEMPS_2        = 0x04001,
        GET_METR_TEMPS_3        = 0x04002,
        GET_METR_TEMPS_4        = 0x04003,
        GET_METR_TEMPS_5        = 0x04004,
        GET_METR_TEMPS_6        = 0x04005,
        GET_METR_TEMPS_7        = 0x04006,
        GET_METR_TEMPS_8        = 0x04007,
        GET_METR_TEMPS_9        = 0x04008,
        GET_METR_TEMPS_10       = 0x04009,
        GET_METR_TEMPS_11       = 0x0400A,
        GET_METR_TEMPS_12       = 0x0400B,
        GET_METR_TEMPS_13       = 0x0400C,
        GET_METR_TEMPS_14       = 0x0400D,
        GET_METR_TEMPS_15       = 0x0400E,
        GET_METR_TEMPS_16       = 0x0400F,
        GET_METR_TEMPS_17       = 0x04010,
        GET_METR_TEMPS_18       = 0x04011,
        GET_METR_TEMPS_19       = 0x04012,
        GET_METR_TEMPS_20       = 0x04013,
        GET_METR_TEMPS_21       = 0x04014,
        GET_METR_TEMPS_22       = 0x04015,
        GET_METR_TEMPS_23       = 0x04016,
        GET_METR_TEMPS_24       = 0x04017,
        GET_METR_TEMPS_25       = 0x04018,
        GET_METR_TILT_0         = 0x05000,
        GET_METR_TILT_1         = 0x05001,
        GET_METR_TILT_2         = 0x05002,
        GET_METR_TILT_3         = 0x05003,
        GET_METR_TILT_4         = 0x05004,
        GET_METR_DISPL_1        = 0x06000,
        GET_METR_DISPL_2        = 0x06001,
//	GET_NUM_TRANS           = 0x30002,
        GET_OTHER_STATUS        = 0x0003A,
        GET_POWER_STATUS        = 0x00003,
        GET_PT_MODEL_COEFF_0    = 0x03000,
        GET_PTC_ERROR           = 0x00015,
        GET_SUBREF_ABS_POSN     = 0x00020,
        GET_SUBREF_DELTA_POSN   = 0x00021,
        GET_SUBREF_LIMITS       = 0x00022,
        GET_SUBREF_ROTATION     = 0x00024,
        GET_SUBREF_STATUS       = 0x00023,
//	GET_SW_REV_LEVEL        = 0x30000,  // same as GET_PROTOCOL_REV_LEVEL
//	GET_SYSTEM_ID           = 0x30004,  // same as GET_SW_REV_LEVEL
        GET_UPS_ALARMS_1          = 0x00030,
        GET_UPS_BATTERY_OUTPUT_1  = 0x00031,
        GET_UPS_BATTERY_STATUS_1  = 0x00032,
        GET_UPS_BYPASS_VOLTS_1    = 0x00033,
        GET_UPS_FREQS_1           = 0x00034,
        GET_UPS_INVERTER_SW_1     = 0x00035,
        GET_UPS_INVERTER_VOLTS_1  = 0x00036,
        GET_UPS_OUTPUT_CURRENT_1  = 0x00037,
        GET_UPS_OUTPUT_VOLTS_1    = 0x00038,
        GET_UPS_STATUS_1          = 0x00039,
        GET_UPS_ALARMS_2          = 0x00040,
        GET_UPS_BATTERY_OUTPUT_2  = 0x00041,
        GET_UPS_BATTERY_STATUS_2  = 0x00042,
        GET_UPS_BYPASS_VOLTS_2    = 0x00043,
        GET_UPS_FREQS_2           = 0x00044,
        GET_UPS_INVERTER_SW_2     = 0x00045,
        GET_UPS_INVERTER_VOLTS_2  = 0x00046,
        GET_UPS_OUTPUT_CURRENT_2  = 0x00047,
        GET_UPS_OUTPUT_VOLTS_2    = 0x00048,
        GET_UPS_STATUS_2          = 0x00049
    };

    enum Controls_t
    {
        CLEAR_FAULT_CMD          = 0x1003,
        INIT_SUBREF_CMD          = 0x1023,
//	RESET_PTC_CMD            = 0x1001,  // same as RESET_DEVICE
        RESET_UPS_CMD_1          = 0x1002,
        RESET_UPS_CMD_2          = 0x1012,
        SET_METR_MODE            = 0x1011,
        SET_PT_MODEL_COEFF_0     = 0x2000,
        SET_SUBREF_ABS_POSN      = 0x1020,
        SET_SUBREF_DELTA_POSN    = 0x1021,
        SET_SUBREF_ROTATION      = 0x1024,
//      SET_UPS_INVERTER_SW      = 0x1030,
        SUBREF_DELTA_ZERO_CMD    = 0x1022,
        UPS_BATTERY_TEST_CMD_1   = 0x1031,
        UPS_BATTERY_TEST_CMD_2   = 0x1041,
	// SIMULATION ENTRIES BEGIN HERE
	SIM_AC_STATUS            = 0x8001,
        SIM_METR_EQUIP_STATUS    = 0x8002,
        SIM_METR_TEMPS           = 0x8003,
        SIM_METR_TILT            = 0x8004,
        SIM_METR_DISPL           = 0x8005,
        SIM_OTHER_STATUS         = 0x8006,
        SIM_POWER_STATUS         = 0x8007,
        SIM_PTC_ERROR            = 0x8008,
        SIM_SUBREF_LIMITS        = 0x8009,
        SIM_SUBREF_STATUS        = 0x800A,
        SIM_UPS_ALARMS_1         = 0x800B,
        SIM_UPS_BATTERY_OUTPUT_1 = 0x800C,
        SIM_UPS_BATTERY_STATUS_1 = 0x800D,
        SIM_UPS_BYPASS_VOLTS_1   = 0x800E,
        SIM_UPS_FREQS_1          = 0x800F,
        SIM_UPS_INVERTER_SW_1    = 0x8010,
        SIM_UPS_INVERTER_VOLTS_1 = 0x8011,
        SIM_UPS_OUTPUT_CURRENT_1 = 0x8012,
        SIM_UPS_OUTPUT_VOLTS_1   = 0x8013,
        SIM_UPS_STATUS_1         = 0x8014,
        SIM_UPS_ALARMS_2         = 0x8015,
        SIM_UPS_BATTERY_OUTPUT_2 = 0x8016,
        SIM_UPS_BATTERY_STATUS_2 = 0x8017,
        SIM_UPS_BYPASS_VOLTS_2   = 0x8018,
        SIM_UPS_FREQS_2          = 0x8019,
        SIM_UPS_INVERTER_SW_2    = 0x801A,
        SIM_UPS_INVERTER_VOLTS_2 = 0x801B,
        SIM_UPS_OUTPUT_CURRENT_2 = 0x801C,
        SIM_UPS_OUTPUT_VOLTS_2   = 0x801D,
        SIM_UPS_STATUS_2         = 0x801E
    };

  private:

  std::vector<CAN::byte_t> tempData(const int firstTemp) const;
  std::vector<CAN::byte_t> displData(const int whichOne) const;
  std::vector<CAN::byte_t> tiltData(const int whichOne) const;

  // All 100 temperatures. Values are in degrees C.
  std::vector<double> temp_m;

  // Just two displacement sensors. Values are in meters.
  std::vector<double> displ_m;

  struct tiltValue {
    double x; // tilts are in arcsecs
    double y; 
    double temp; // temperature is in degrees C
  };

  // Five tilt sensors.
  std::vector<tiltValue> tilt_m;


    // pointing model coefficients
    double m_IA;
    double m_CA;
    double m_NPAE;
    double m_AN;
    double m_AW;
    double m_IE;
    double m_ECEC;
    double m_C_GR;
    double m_D_GR;
    double m_RESERVED;

    // metrology mode
    char m_metrMode;
    char m_ac_status;

    // absolute position
    short m_Xposition;
    short m_Yposition;
    short m_Zposition;

    // delta position
    short m_deltaXposition;
    short m_deltaYposition;
    short m_deltaZposition;

    // rotation
    short m_Xrotation;
    short m_Yrotation;
    short m_Zrotation;

    //
    // CAN Node stuff
    //
    node_t m_node;		// bus location (node number)
    std::vector<CAN::byte_t> m_sn;	// serial number of this CAN node
    mutable unsigned int m_trans_num;  // number of transactions
    
    // undefined and inaccessible
    PTC(const PTC&);
    PTC& operator = (const PTC&);

}; // class PTC

} // namespace AMB

#endif // ! PTC_H
