// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#ifndef ACU_INCLUDE_H
#define ACU_INCLUDE_H

#include <vector>

/*
 * Commonly used enums to describe the ACU
 */

// monitor values as part of get_acu_error() response
enum ACU_Error_t
{
    ACU_TIMEOUT             = 1,
    INVALID_MODE_CHANGE     = 2,
    POSITION_CMD_RANGE_ERR  = 3,
    VELOCITY_CMD_RANGE_ERR  = 4,
    LOCAL_MODE              = 5,
    TRANSFER_ERROR          = 6,
    MOUNT_IMMOBILE          = 7,
    MOUNT_RUNAWAY           = 8,
    MOUNT_REVERSED          = 9,
    INVALID_BRAKE_CMD       = 10
};
  
// used with get_acu_mode_rsp()
enum AZEL_Mode_t
{
    SHUTDOWN                = 0x0,
    STANDBY                 = 0x1,
    ENCODER                 = 0x2,
    AUTONOMOUS              = 0x3,
    SURVIVAL_STOW           = 0x4,
    MAINTENANCE_STOW        = 0x5,
    ACU_LOCAL               = 0x1,
    ACU_REMOTE              = 0x2
};
  
// used with get/set_az_brake(), get/set_el_brake()
enum AZEL_Brake_t
{
    BRAKE_OFF = 0,
    BRAKE_ON = 1
};
  
/*
 * AZ/EL status bits.  Setting bits to these values enables
 * the condition or the fault.  The status bits cover two
 * integer words, so Status 1, is low word, and Status2 is
 * high word in status replies.
 */
enum AZEL_Status1_t
{
    PRELIM_UP               = 0x00000001,
    PRELIM_DOWN             = 0x00000002,
    LIMIT_UP                = 0x00000004,
    LIMIT_DOWN              = 0x00000008,
    EMERG_PRELIM_UP         = 0x00000010,
    EMERG_PRELIM_DOWN       = 0x00000020,
    SERVO_FAILURE           = 0x00000100,
    OVER_SPEED              = 0x00000200,
    NO_MOTION               = 0x00000400,
    ZERO_SPEED              = 0x00000800,
    STOW_POSITION           = 0x00001000,
    ENCODER_FAILURE         = 0x00002000,
    CRAZY_VEL_FEEDBACK      = 0x00004000,
    BRAKE1_FAILURE          = 0x00010000,
    BRAKE2_FAILURE          = 0x00020000,
    BRAKE3_FAILURE          = 0x00040000,
    BRAKE4_FAILURE          = 0x00080000,
    AMP1_FAILURE            = 0x01000000,
    AMP2_FAILURE            = 0x02000000,
    AMP3_FAILURE            = 0x04000000,
    AMP4_FAILURE            = 0x08000000
};
  
enum AZEL_Status2_t
{
    MOTOR1_OVER_TEMP        = 0x00000001,
    MOTOR2_OVER_TEMP        = 0x00000002,
    MOTOR3_OVER_TEMP        = 0x00000004,
    MOTOR4_OVER_TEMP        = 0x00000008,
    AUX_MOTOR1_OFF          = 0x00010000,
    AUX_MOTOR2_OFF          = 0x00020000,
    AUX_MOTOR4_OFF          = 0x00040000,
    AUX_MOTOR8_OFF          = 0x00080000,
    COMPUTER_DISABLED       = 0x01000000,
    AXIS_DISABLED           = 0x02000000,
    HANDHELD_CU_OPERATION   = 0x04000000,
    AXIS_IN_STOP            = 0x08000000,
    ELEVATION_GT_90         = 0x10000000
};
  
// used with get_az_enc_status(), get_el_enc_status()
enum AZEL_Encoder_Status_t
{
    READ_HEAD_ERROR         = 0x00000001,
    EEU_CPU_FAILURE         = 0x00000002,
    EEU_NO_RESPONSE         = 0x00000008,
    EEU_DATA_ERROR          = 0x00000010,
    BAD_POSITION_READ       = 0x00000020
};
  
// used with get_shutter(), set_shutter()
enum Shutter_Bits_t
{
    SHUTTER_CLOSED          = 0,
    SHUTTER_OPEN            = 1,
    SHUTTER_DISABLED        = 2,
};
  
// used with set_stow_pin() and get_stow_pin()
enum Stow_Pin_t
{
    AZ_IN                   = 0x0100,
    AZ_OUT                  = 0x0200,
    EL_IN                   = 0x0001,
    EL_OUT                  = 0x0002,
    PIN_IN                  = 0x0001,
    PIN_OUT                 = 0x0002
};
  
// used with get_system_status()
enum System_Status_t
{
    SAFE_SWITCH             = 0x00000001,
    POWER_FAILURE           = 0x00000002,
    V24_FAILURE             = 0x00000004,
    BREAKER_FAILURE         = 0x00000008,
    COMM_ERROR_PTC          = 0x00000010,
    COMM_ERROR_PLC          = 0x00000020,
    CABINET_OVER_TEMP       = 0x00000040,
    CABINET_DOOR_OPEN       = 0x00000080,
    PLATFORM2_RAILS_UP      = 0x00000100,
    RX_CABIN_RAMP_ERROR     = 0x00000200,
    HOIST_INTERLOCK         = 0x00000400,
    PLATFORM2_ACCESS_OPEN   = 0x00000800,
    PLATFORM1_DOOR_OPEN     = 0x00001000,
    RX_CABIN_DOOR_OPEN      = 0x00002000,
    CABINET_STOP            = 0x00010000,
    EQUIPMENT_RACK_STOP     = 0x00020000,
    ANTENNA_BASE_STOP       = 0x00040000,
    PLATFORM_STOP           = 0x00080000,
    STAIRWAY_STOP           = 0x00100000,
    RX_CABIN_STOP           = 0x00200000
};
  
// used with get/set_az_aux_mode(), get/set_el_aux_mode()
enum AUX_Mode_t
{
    AUX_DISABLED            = 0x00,
    AUX1_MODE               = 0x01,
    AUX2_MODE               = 0x02,
    AUX12_MODE              = 0x03
};

/**
 * An Abstract Base Class (ABC) that defines CAN bus accesses on the Vertex 
 * Antenna Control Unit (ACU) computer.  All members of this class must be 
 * over-ridden.
 *
 * The class definitions are 1-to-1 with the monitor and control points 
 * defined in ALMA ICD No. 9, "Antenna / Monitor and Control Interface".
 *
 * There are two classes derived from this class:
 * 1) ACUSim, a simulator (see ACUSim.h in this module), and
 * 2) ACUhw, an interface to the CAN hardware (part of the ticsAntVA module).
 *
 * For a discussion of this design, see ticsAntVA/lcu/include/ACUhw.h.
 */
class ACUbase
{
  public:
    // After construction, the ACU is in the STANDBY state.
    ACUbase() {};
    virtual ~ACUbase() {};
      
    //
    // Generic monitor points
    //
    // RCA 0x30000, aka generic GET_PROTOCOL_REV_LEVEL
    virtual std::vector<char> get_sw_rev_level(
	int& errcode,
	unsigned long long& timestamp) const = 0;

    // RCA 0x30001
    virtual unsigned long get_can_error(
	int& errcode,
	unsigned long long& timestamp) const = 0;

    // RCA 0x30002
    virtual unsigned long get_num_trans(
	int& errcode,
	unsigned long long& timestamp) const = 0;

    // RCA 0x30004, aka generic GET_SW_REV_LEVEL
    virtual std::vector<char> get_system_id(
	int& errcode,
	unsigned long long& timestamp) const = 0;

    //
    // Monitor Requests
    //
      
    // get_acu_mode_rsp returns 2 bytes.  The azimuth mode is encoded in the
    // low nibble of byte 0, and elevation mode in high nibble of same.  The 
    // access mode is encoded in byte 1.
    // Uses ACU_Mode_t and AXIS_Mode_t enums.
    virtual std::vector<char> get_acu_mode_rsp(
	int& errcode,
	unsigned long long& timestamp) const = 0;
      
    // get_acu_error() returns 5 bytes.  The error byte is in byte 0, and 
    // is defined in enum ACU_Error_t.  The upper 4 bytes are the command
    // RCA that caused the error in host byte order.
    virtual unsigned long long get_acu_error(
	int& errcode,
	unsigned long long& timestamp) const = 0;
      
    // get_az_posn_rsp(), get_el_posn_rsp() return 2 positions:
    // on last Timing Event and 24 ms before
    // Positions are in binary parts of a circle, a.k.a BPC.  The BPC here is
    // a signed 32 bit number with +180 to -180 at 0x40000000.  These are long
    // long because there are two 32 bit values returned.
    virtual unsigned long long get_az_posn_rsp(	// BPC units
	int& errcode,
	unsigned long long& timestamp) const = 0;
    virtual unsigned long long get_el_posn_rsp(	// BPC units
	int& errcode,
	unsigned long long& timestamp) const = 0;
#if 0  /* !!! NOT USED !!! */
    virtual vector<double> get_az_posn_rsp(	// natural units
	int& errcode,
	double& timestamp) const = 0;
    virtual vector<double> get_el_posn_rsp(	// natural units
	int& errcode,
	double& timestamp) const = 0;
#endif /* not used */

    // get_az_enc(), get_el_enc() return positions on last time tick
    virtual long get_az_enc(			// encoder units
	int& errcode,
	unsigned long long& timestamp) const = 0;
    virtual void get_el_enc(			// encoder units
	std::vector<long>& positions,
	int& errcode,
	unsigned long long& timestamp) const = 0;
#if 0  /* !!! NOT USED !!! */
    virtual double get_az_enc(			// natural units
	int& errcode,
	double& timestamp) const = 0;
    virtual double get_el_enc(			// natural units
	int& errcode,
	double& timestamp) const = 0;
#endif /* not used */

    // get_az_enc_status(), get_el_enc_status().  There is 1 az encoder,
    // and 3 el encoders.  The bytes are right adjusted in the word.
    // Uses enum AZEL_Encoder_Status
    virtual unsigned char get_az_enc_status(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    virtual std::vector<char> get_el_enc_status(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    
    // get_az_aux_mode(), get_el_aux_mode() returns az_aux_mode, 
    // el_aux_mode status.  See AUX_Mode_t enum
    virtual unsigned char get_az_aux_mode(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    virtual unsigned char get_el_aux_mode(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    
    // az has two motors.
    // get_az_motor_currents() - each motor current is one byte, 0 to 255 A.
    virtual std::vector<unsigned char> get_az_motor_currents(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    
    // get_az_motor_temps() - each motor temp is one byte, -50 to 205 C
    virtual std::vector<unsigned char> get_az_motor_temps(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    
    // get_az_motor_torque() - each motor torque is one byte, 0 to 255 Nm
    virtual unsigned long get_az_motor_torques(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    
    // el has four motors.  the units are the same as az, but twice the data
    virtual unsigned long get_el_motor_currents(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    virtual unsigned long get_el_motor_temps(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    virtual unsigned long get_el_motor_torques(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    
    // get_az_brake(), get_el_brake() returns az/el brake status
    // Uses enum AZEL_Brake_t
    virtual unsigned char get_az_brake(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    virtual unsigned char get_el_brake(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    
    // get_az_servo_coeff_n, get_el_servo_coeff_n is implemented as an
    // array that is simply stuffed with the set values.  no conversion is
    // done.
    virtual double get_az_servo_coeff(
	long n,
	int& errcode,
	unsigned long long& timestamp) const = 0;
    virtual double get_el_servo_coeff(
	long n,
	int& errcode,
	unsigned long long& timestamp) const = 0;
   
    // get_az_status(), get_el_status() returns a multi-byte status with
    // mult-bit fields.  Uses enums AZEL_Status1, AZEL_Status2
    virtual unsigned long long get_az_status(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    virtual unsigned long long get_el_status(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    
    // get_idle_stow_time() seconds to wait for comm or tix and then stow.
    virtual unsigned short get_idle_stow_time(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    
    // get_shutter, uses enum Shutter_Bits_t
    virtual unsigned char get_shutter(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    
    // get_stow_pin, short word with lower byte az, upper byte el pin status
    // use Stow_Pin_t enum
    virtual unsigned short get_stow_pin(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    
    // get_system_status() - get antenna related system status.
    // Uses enum System_Status_t
    virtual unsigned long get_system_status(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    
    // get_ip_address() - get ACU IP address
    virtual unsigned long get_ip_address(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    
    //
    // Control Commands
    //
    
    // set_reset_acu_cmd()  The underlying control point takes an argument
    // with only one valid value, so I am assuming the ACU will not
    // implement other values over time.
    virtual void reset_acu_cmd(
	int& errcode,
	unsigned long long& timestamp) = 0;
    
    // acu modes are defined in the AZEL_Mode_t enum
    // This sets the az/el axes modes.  The byte's low nibble is az, and
    // the high nibble is el.
    virtual void acu_mode_cmd(
	const unsigned char mode,
	int& errcode,
	unsigned long long& timestamp) = 0;
    
    // clear_fault_cmd() clear all faults.
    virtual void clear_fault_cmd(
	int& errcode,
	unsigned long long& timestamp) = 0;
    
    // Hardware interface for az_traj_cmd and el_traj_cmd are two integers 
    // with position and velocity at the second timing event after the 
    // command is sent.
    virtual void az_traj_cmd(		// encoder units
	const long position,
	const long velocity,
	int& errcode,
	unsigned long long& timestamp) = 0;
    virtual void el_traj_cmd(		// encoder units
	const long position,
	const long velocity,
	int& errcode,
	unsigned long long& timestamp) = 0;
#if 0  /* !!! NOT USED !!! */
    virtual void az_traj_cmd(		// natural units
	const double position,
	const double velocity,
	int& errcode,
	double& timestamp) = 0;
    virtual void el_traj_cmd(		// natural units
	const double position,
	const double velocity,
	int& errcode,
	double& timestamp) = 0;
#endif /* not used */

    // set_az_aux_mode(), set_el_aux_mode(), see AUX_Mode_t enum
    virtual void set_az_aux_mode(
	const unsigned char mode,
	int& errcode,
	unsigned long long& timestamp) = 0;
    virtual void set_el_aux_mode(
	const unsigned char mode,
	int& errcode,
	unsigned long long& timestamp) = 0;
    
    // set_az_brake(), set_el_brake(), uses Brake_Mode_t enum
    virtual void set_az_brake(
	const unsigned char mode,
	int& errcode,
	unsigned long long& timestamp) = 0;
    virtual void set_el_brake(
	const unsigned char mode,
	int& errcode,
	unsigned long long& timestamp) = 0;
    
    // AZ/EL servo commands
    virtual void set_az_servo_coeff(
	const long index,
	double value,
	int& errcode,
	unsigned long long& timestamp) = 0;
    virtual void set_el_servo_coeff(
	const long index,
	double value,
	int& errcode,
	unsigned long long& timestamp) = 0;
    virtual void set_az_servo_default(
	int& errcode,
	unsigned long long& timestamp) = 0;
    virtual void set_el_servo_default(
	int& errcode,
	unsigned long long& timestamp) = 0;
    
    // miscellaneous ACU commands
    virtual void set_idle_stow_time(
	const unsigned short seconds,
	int& errcode,
	unsigned long long& timestamp) = 0;
    virtual void set_shutter(
	const unsigned char bits,
	int& errcode,
	unsigned long long& timestamp) = 0;
    
    // byte 0 is AZ, byte 1 is EL.  see Stow_Pin_t enum
    virtual void set_stow_pin(
	const unsigned short bits,
	int& errcode,
	unsigned long long& timestamp) = 0;
    
    /*
     * CAN Addresses for ACU M&C Points.
     * These are used in the monitor() and control() switch statements.
     */
    enum Monitor_t
    {
	ACU_MODE_RSP            = 0x00022,
        AZ_POSN_RSP             = 0x00012,
        EL_POSN_RSP             = 0x00002,
        GET_ACU_ERROR           = 0x0002F,
        GET_AZ_AUX_MODE         = 0x00016,
        GET_AZ_BRAKE            = 0x00014,
	GET_AZ_ENC              = 0x00017,
        GET_AZ_ENC_STATUS       = 0x00018,
        GET_AZ_MOTOR_CURRENTS   = 0x00019,
        GET_AZ_MOTOR_TEMPS      = 0x0001A,
        GET_AZ_MOTOR_TORQUE     = 0x00015,
        GET_AZ_SERVO_COEFF_0    = 0x03020,
        GET_AZ_STATUS           = 0x0001B,
	GET_CAN_ERROR           = 0x30001,
        GET_EL_AUX_MODE         = 0x00006,
        GET_EL_BRAKE            = 0x00004,
        GET_EL_ENC              = 0x00007,
        GET_EL_ENC_STATUS       = 0x00008,
        GET_EL_MOTOR_CURRENTS   = 0x00009,
        GET_EL_MOTOR_TEMPS      = 0x0000A,
        GET_EL_MOTOR_TORQUE     = 0x00005,
        GET_EL_SERVO_COEFF_0    = 0x03010,
        GET_EL_STATUS           = 0x0000B,
        GET_IDLE_STOW_TIME      = 0x00025,
        GET_IP_ADDRESS          = 0x0002D,
	GET_NUM_TRANS           = 0x30002,
        GET_SHUTTER             = 0x0002E,
        GET_STOW_PIN            = 0x00024,
	GET_SW_REV_LEVEL        = 0x30000,  // same as GET_PROTOCOL_REV_LEVEL
	GET_SYSTEM_ID           = 0x30004,  // same as GET_SW_REV_LEVEL
        GET_SYSTEM_STATUS       = 0x00023 
    };

    enum Controls_t
    {
	SIM_SYSTEM_STATUS       = 0x10023,
        ACU_MODE_CMD            = 0x01022,
        AZ_TRAJ_CMD             = 0x01012,
        CLEAR_FAULT_CMD         = 0x01021,
        EL_TRAJ_CMD             = 0x01002,
        RESET_ACU_CMD           = 0x0102F,
        SET_AZ_AUX_MODE         = 0x01016,
        SET_AZ_BRAKE            = 0x01014,
        SET_AZ_SERVO_COEFF_0    = 0x02020,
        SET_AZ_SERVO_DEFAULT    = 0x01017,
        SET_EL_AUX_MODE         = 0x01006,
        SET_EL_BRAKE            = 0x01004,
        SET_EL_SERVO_COEFF_0    = 0x02010,
        SET_EL_SERVO_DEFAULT    = 0x01007,
        SET_IDLE_STOW_TIME      = 0x01025,
        SET_SHUTTER             = 0x0102E,
        SET_STOW_PIN            = 0x0102D,
    };

};  // class ACUbase

#endif  // ACU_INCLUDE_H
