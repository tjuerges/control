// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2007
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#if !defined(CTNLL_H)
#define CTNLL_H

#include "AMBDevice.h"
#include "CANError.h"
#include <string>

namespace AMB {

  ///
  /// Based on the Laser Synthesiser ICD dated 2006-02-16. This class
  /// simulates the Calibrated Tunable Narrow Line-width Laser (CTNLL) part.
  ///
  class CTNLL: public AMB::Device
  {
  public:

    /// 
    /// After construction, the initial state is <define-it>
    ///
    CTNLL(node_t node, const std::vector<CAN::byte_t>& serialNumber);
    virtual ~CTNLL();

    ///
    /// Returns the node number. This should usually be 0x39 and other allowed
    /// values are 0x3b, 0x3d or 0x3f
    ///
    virtual node_t node() const;

    ///
    /// Returns the serial number. This is an arbitrary but unique 64 bit
    /// sequence.
    ///
    virtual std::vector<CAN::byte_t> serialNumber() const;

    ///
    /// Performs monitor request of the specified relative CAN address
    /// (RCA). An excpetion is thrown if the RCA is not one of the expected
    /// ones.
    /// 
    virtual std::vector<CAN::byte_t> monitor(rca_t rca) const;

    ///
    /// Performs control request of the specified relative CAN address (RCA),
    /// using the supplied data. An exception is thrown if the RCA is not one
    /// of the expected ones or if the supplied data is not of the correct
    /// length.
    ///
    virtual void control(rca_t rca, const std::vector<CAN::byte_t>& data);

    ///
    /// CAN Generic MC points. Some of these are implemented in the base class
    /// are are not overridden here.
    ///

    ///
    /// Return the number of transactions. This counts both monitor and
    /// control transactions.
    ///
    virtual unsigned int get_trans_num() const;

    ///
    /// Temperature on M&C interface board in dgrees C. This returns 18.0
    /// constant (if only it was always like that).
    ///
    float getAmbientTemperature() const;


    std::vector<CAN::byte_t> CAN_CTNLL_STATUS () const;
    std::vector<CAN::byte_t> CAN_LASER_FREQUENCY () const;
    std::vector<CAN::byte_t> CAN_LASER_CURRENT () const;
    std::vector<CAN::byte_t> CAN_LASER_POWER () const;
    std::vector<CAN::byte_t> CAN_LASER_TEMPERATURE () const;


   /** Helper function for 'set' commands to check that data size
    ** is valid, if not throw exception
    ** @param data Vector of bytes containing data to check
    ** @param size Expected number of bytes in data std::vector
    ** @param pExceptionMsg String identifying CAN msg that is
    ** associated with failed size check.
    */
    void checkSize( const std::vector<CAN::byte_t> &data, int size,
                        const char *pExceptionMsg);


    ///
    /// CTNLL specific monitor points.
    ///

    enum Monitor_t {
      ///
      /// Generic monitor points for an AMBSI-2
      ///
      GET_SERIAL_NUM          = AMB::Device::GET_SERIAL_NUM,
      GET_CAN_ERROR           = AMB::Device::GET_CAN_ERROR,
      GET_PROTOCOL_REV_LEVEL  = AMB::Device::GET_PROTOCOL_REV_LEVEL,
      GET_TRANS_NUM           = AMB::Device::GET_TRANS_NUM,
      GET_AMBIENT_TEMPERATURE = AMB::Device::GET_AMBIENT_TEMPERATURE,
      GET_SW_REV_LEVEL        = AMB::Device::GET_SW_REV_LEVEL,
      GET_HW_REV_LEVEL        = AMB::Device::GET_HW_REV_LEVEL,
      GET_CTNLL_STATUS        = 0x00100,
      GET_LASER_FREQUENCY     = 0x00300,
      GET_LASER_CURRENT       = 0x00301,
      GET_LASER_POWER         = 0x00302,
      GET_LASER_TEMPERATURE   = 0x00303

      ///
      /// Monitor points specific to the CTNLL (from the ICD).
      ///
    };

    enum Controls_t {
      ///
      /// Generic control points for an AMBSI-1
      ///
      RESET_DEVICE              = AMB::Device::RESET_DEVICE,

      ///
      /// Control points specific to the CTNLL (from the ICD).
      ///
      SET_LASER_FREQUENCY       = 0x00200,
      SET_CTNLL_COMMAND         = 0x00400

    };

  private:
    // Undefined and inaccessible - copying does not make sense.
    CTNLL(const CTNLL&);
    CTNLL& operator=(const CTNLL&);

    ///
    /// Node number.
    ///
    node_t node_m;
    
    ///
    /// Serial number of this node.
    ///
    std::vector<CAN::byte_t> sn_m;

    ///
    /// Number of transactions. It needs to be mutable because the monitor
    /// function, which is a const function, updates this number.
    ///
    mutable unsigned int transNum_m;

    double CTNLL_STATUS_m;
    double LASER_FREQUENCY_m;
    double LASER_CURRENT_m;
    double LASER_POWER_m;
    double LASER_TEMPERATURE_m;

    enum CTNLL_Commands {

       CALIBRATE = 0x0001,
       PZT_PLUS  = 0x0002,
       PZT_MINUS = 0x0004

    };

  };  // class CTNLL
}  // namespace AMB
#endif  // CTNLL_H
