// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2007
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#if !defined(OPLL_H)
#define OPLL_H

#include "AMBDevice.h"
#include "CANError.h"
#include <string>

namespace AMB {

  ///
  /// Based on the Laser Synthesiser ICD dated 2006-02-16. This class
  /// simulates the Opto-electronic Phase Locked Loop (OPLL) part.
  ///
  class OPLL: public AMB::Device
  {
  public:

    /// 
    /// After construction, the initial state is <define-it>
    ///
    OPLL(node_t node, const std::vector<CAN::byte_t>& serialNumber);
    virtual ~OPLL();

    ///
    /// Returns the node number. This should usually be 0x39 and other allowed
    /// values are 0x3b, 0x3d or 0x3f
    ///
    virtual node_t node() const;

    ///
    /// Returns the serial number. This is an arbitrary but unique 64 bit
    /// sequence.
    ///
    virtual std::vector<CAN::byte_t> serialNumber() const;

    ///
    /// Performs monitor request of the specified relative CAN address
    /// (RCA). An excpetion is thrown if the RCA is not one of the expected
    /// ones.
    /// 
    virtual std::vector<CAN::byte_t> monitor(rca_t rca) const;

    ///
    /// Performs control request of the specified relative CAN address (RCA),
    /// using the supplied data. An exception is thrown if the RCA is not one
    /// of the expected ones or if the supplied data is not of the correct
    /// length.
    ///
    virtual void control(rca_t rca, const std::vector<CAN::byte_t>& data);

    ///
    /// CAN Generic MC points. Some of these are implemented in the base class
    /// are are not overridden here.
    ///

    ///
    /// Return the number of transactions. This counts both monitor and
    /// control transactions.
    ///
    virtual unsigned int get_trans_num() const;

    std::vector<CAN::byte_t> CAN_BAND_STATUS () const;
    std::vector<CAN::byte_t> CAN_LOCK_STATUS () const;
    std::vector<CAN::byte_t> CAN_CTNLL_READY () const;
    std::vector<CAN::byte_t> CAN_PM_V_BAND_A () const;
    std::vector<CAN::byte_t> CAN_PM_I_BAND_A () const;
    std::vector<CAN::byte_t> CAN_PM_V_BAND_B () const;
    std::vector<CAN::byte_t> CAN_PM_I_BAND_B () const;
    std::vector<CAN::byte_t> CAN_PM_V_BAND_C () const;
    std::vector<CAN::byte_t> CAN_PM_I_BAND_C () const;
    std::vector<CAN::byte_t> CAN_PM_V_BAND_D () const;
    std::vector<CAN::byte_t> CAN_PM_I_BAND_D () const;
    std::vector<CAN::byte_t> CAN_IF_LEVEL () const;
    std::vector<CAN::byte_t> CAN_REF_LEVEL_125MHZ () const;
    std::vector<CAN::byte_t> CAN_PIEZO_VOLTAGE () const;
    std::vector<CAN::byte_t> CAN_VCO_TUNING_V () const;
    std::vector<CAN::byte_t> CAN_IF_VCTRL () const;

/*
*/

   /** Helper function for 'set' commands to check that data size
    ** is valid, if not throw exception
    ** @param data Vector of bytes containing data to check
    ** @param size Expected number of bytes in data std::vector
    ** @param pExceptionMsg String identifying CAN msg that is
    ** associated with failed size check.
    */
    void checkSize( const std::vector<CAN::byte_t> &data, int size,
                        const char *pExceptionMsg);



    ///
    /// Temperature on M&C interface board in dgrees C. This returns 18.0
    /// constant (if only it was always like that).
    ///
    float getAmbientTemperature() const;

    ///
    /// OPLL specific monitor points.
    ///

    enum Monitor_t {
      ///
      /// Generic monitor points for an AMBSI-2
      ///
      GET_SERIAL_NUM          = AMB::Device::GET_SERIAL_NUM,
      GET_CAN_ERROR           = AMB::Device::GET_CAN_ERROR,
      GET_PROTOCOL_REV_LEVEL  = AMB::Device::GET_PROTOCOL_REV_LEVEL,
      GET_TRANS_NUM           = AMB::Device::GET_TRANS_NUM,
      GET_AMBIENT_TEMPERATURE = AMB::Device::GET_AMBIENT_TEMPERATURE,
      GET_SW_REV_LEVEL        = AMB::Device::GET_SW_REV_LEVEL,
      GET_HW_REV_LEVEL        = AMB::Device::GET_HW_REV_LEVEL,

      ///
      /// Monitor points specific to the OPLL (from the ICD).
      ///

      GET_BAND_STATUS         = 0x00005,
      GET_LOCK_STATUS         = 0x00006,
      GET_CTNLL_READY         = 0x00007,
      GET_PM_V_BAND_A         = 0x00010,
      GET_PM_I_BAND_A         = 0x00011,
      GET_PM_V_BAND_B         = 0x00012,
      GET_PM_I_BAND_B         = 0x00013,
      GET_PM_V_BAND_C         = 0x00014,
      GET_PM_I_BAND_C         = 0x00015,
      GET_PM_V_BAND_D         = 0x00016,
      GET_PM_I_BAND_D         = 0x00017,
      GET_IF_LEVEL            = 0x00018,
      GET_REF_LEVEL_125MHZ    = 0x00019,
      GET_PIEZO_VOLTAGE       = 0x0001A,
      GET_VCO_TUNING_V        = 0x0001B,
      GET_IF_VCTRL            = 0x0001C,
      GET_PORT2_DATA          = 0x00080,
      GET_PORT7_DATA          = 0x00081,
      GET_PORT8_DATA          = 0x00082,

    };

    enum Controls_t {
      ///
      /// Generic control points for an AMBSI-1
      ///
      RESET_DEVICE              = AMB::Device::RESET_DEVICE,

      ///
      /// Control points specific to the OPLL (from the ICD).
      ///
      SET_BAND                  = 0x00100,
      SET_IF_VCTRL              = 0x00101,
      LOCK_STATUS_RESET         = 0x00102,
      SET_OPTSW_STATE           = 0x01080,
      SET_RFSW_STATE            = 0x01081,
      SET_IFSW_STATE            = 0x01082
    };

    enum Test_t {
       TEST_BAND_STATUS      = 0x0F005, 
       TEST_LOCK_STATUS      = 0x0F006, 
       TEST_CTNLL_READY      = 0x0F007, 
       TEST_PM_V_BAND_A      = 0x0F010, 
       TEST_PM_I_BAND_A      = 0x0F011, 
       TEST_PM_V_BAND_B      = 0x0F012, 
       TEST_PM_I_BAND_B      = 0x0F013, 
       TEST_PM_V_BAND_C      = 0x0F014, 
       TEST_PM_I_BAND_C      = 0x0F015, 
       TEST_PM_V_BAND_D      = 0x0F016, 
       TEST_PM_I_BAND_D      = 0x0F017, 
       TEST_IF_LEVEL         = 0x0F018, 
       TEST_REF_LEVEL_125MHZ = 0x0F019, 
       TEST_PIEZO_VOLTAGE    = 0x0F01A, 
       TEST_VCO_TUNING_V     = 0x0F01B, 
       TEST_IF_VCTRL         = 0x0F01C 
    };


  private:
    // Undefined and inaccessible - copying does not make sense.
    OPLL(const OPLL&);
    OPLL& operator=(const OPLL&);

    ///
    /// Node number.
    ///
    node_t node_m;
    
    ///
    /// Serial number of this node.
    ///
    std::vector<CAN::byte_t> sn_m;
    mutable unsigned int transNum_m;

    //short int BAND_STATUS_m;
    double BAND_STATUS_m;
    double LOCK_STATUS_m;
    double CTNLL_READY_m;
    double PM_V_BAND_A_m;
    double PM_I_BAND_A_m;
    double PM_V_BAND_B_m;
    double PM_I_BAND_B_m;
    double PM_V_BAND_C_m;
    double PM_I_BAND_C_m;
    double PM_V_BAND_D_m;
    double PM_I_BAND_D_m;
    double IF_LEVEL_m;
    double REF_LEVEL_125MHZ_m;
    double PIEZO_VOLTAGE_m;
    double VCO_TUNING_V_m;
    double IF_VCTRL_m;
    /// Number of transactions. It needs to be mutable because the monitor
    /// function, which is a const function, updates this number.
    ///

      //double VCO_TUNING_V_m;
/*
*/
    /*mutable unsigned int transNum_m;*/
    ///
  };  // class OPLL
}  // namespace AMB
#endif  // OPLL_H
