// @(#) $Id$
//
// Copyright (C) 2002, 2004
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#if !defined(WVR_H)
#define WVR_H

#include "AMBDevice.h"
#include "CANError.h"
#include <string>

namespace AMB
{
    /** The WVR class is a simulator for the water vapor radiomenter
     */
    class WVR : public AMB::Device
    {
      public:
	/** Constructor
	 ** @param node Node ID of this device
	 ** @param serialNumber S/N of this device
	 */
	WVR(node_t node, const std::vector<CAN::byte_t>& serialNumber);

	/// Destructor
	virtual ~WVR();
    
	/// Return the device's node ID
	virtual node_t node() const;

	/// Return the device's S/N
	virtual std::vector<CAN::byte_t> serialNumber() const;
    
	/** Monitor interface
	 ** @param CAN node RCA
	 ** @return std::vector of bytes containing monitor info. Each response
	 ** is sized to what's specified in the ICD.
	 */
	virtual std::vector<CAN::byte_t> monitor(rca_t rca) const;
    
	/** Control Interface ('set' functions)
	 ** @param rca CAN node RCA
	 ** @param data CAN message data required by device
	 */
	virtual void control(rca_t rca, const std::vector<CAN::byte_t>& data);
    
	/// Monitor points for the WVR
	enum Monitor_t {
	    /// Generic monitor points
	    //@{
	    GET_SERIAL_NUMBER       = 0x00000,
	    GET_CAN_ERROR           = AMB::Device::GET_CAN_ERROR,
	    GET_PROTOCOL_REV_LEVEL  = AMB::Device::GET_PROTOCOL_REV_LEVEL,
	    GET_TRANS_NUM           = AMB::Device::GET_TRANS_NUM,
	    GET_AMBIENT_TEMPERATURE = AMB::Device::GET_AMBIENT_TEMPERATURE,
	    //@}
      
	    /// Specific WVR data point monitors
	    //@{
	    GET_WVR_STATE          = 0x00001,
	    GET_INT_TIME           = 0x00002,
	    GET_BLK_TIME           = 0x00003,
            GET_CYC_IN_RES         = 0x00004,
            GET_CYC_IN_CAL         = 0x00005,
            GET_CAL_PER            = 0x00006,
	    GET_LO_STATE           = 0x00007,
	    GET_PHASE_TIME         = 0x00008,
	    GET_IHL_DATA           = 0x00009,
	    GET_ICL_DATA           = 0x0000a,
	    GET_HTR0_DATA          = 0x0000b,
	    GET_HTR1_DATA          = 0x0000c,
	    GET_PELT_SENS          = 0x0000e,
	    GET_PELT_THRESH        = 0x0000f,
	    GET_DDS_DATA0          = 0x00010,
            GET_DDS_DATA1          = 0x00011,
	    GET_DIAG_CONF          = 0x00025,
	    GET_VANE_POS           = 0x00027,
	    GET_WVR_CONF           = 0x00028,
	    GET_PLL_LOCK           = 0x00029,
	    GET_YIG_LOCK           = 0x0002a,
	    GET_IF_PWROK           = 0x0002b,
	    GET_REF_OK             = 0x0002c,
	    GET_CHOP_LOCK          = 0x0002d,
	    GET_CHOP_SENS          = 0x0002e,
	    GET_CHOP_STATE         = 0x0002f,
	    GET_VANE_TMOUT         = 0x00030,
	    GET_CHOP_TMOUT         = 0x00032,
            GET_ICL_STATE          = 0x00034,
            GET_HTR0_STATE         = 0x00035,
            GET_HTR1_STATE         = 0x00036,
            GET_VANE_STATE         = 0x00038,
	    GET_TEMP_LO0           = 0x00040,
	    GET_TEMP_LO1           = 0x00041,
	    GET_TEMP_LO2           = 0x00042,
	    GET_TEMP_LO3           = 0x00043,
	    GET_TEMP_LO4           = 0x00044,
	    GET_TEMP_LO5           = 0x00045,
	    GET_TEMP_LO6           = 0x00046,
	    GET_TEMP_LO7           = 0x00047,
	    GET_PSUV_LO0           = 0x00048,
	    GET_PSUV_LO1           = 0x00049,
	    GET_PSUV_LO2           = 0x0004a,
	    GET_PSUV_LO3           = 0x0004b,
	    GET_PSUV_LO4           = 0x0004c,
	    GET_PSUV_LO5           = 0x0004d,
	    GET_PSUV_LO6           = 0x0004e,
	    GET_PSUV_LO7           = 0x0004f,
	    GET_GUNNV0_LO          = 0x00050,
	    GET_IFPWR0_LO          = 0x00051,
	    GET_GUNNV1_LO          = 0x00052,
	    GET_IFPWR1_LO          = 0x00053,
	    GET_TEMP_HI0           = 0x00060,
	    GET_TEMP_HI1           = 0x00061,
	    GET_TEMP_HI2           = 0x00062,
	    GET_TEMP_HI3           = 0x00063,
	    GET_TEMP_HI4           = 0x00064,
	    GET_TEMP_HI5           = 0x00065,
	    GET_TEMP_HI6           = 0x00066,
	    GET_TEMP_HI7           = 0x00067,
	    GET_PSUV_HI0           = 0x00068,
	    GET_PSUV_HI1           = 0x00069,
	    GET_PSUV_HI2           = 0x0006a,
	    GET_PSUV_HI3           = 0x0006b,
	    GET_PSUV_HI4           = 0x0006c,
	    GET_PSUV_HI5           = 0x0006d,
	    GET_PSUV_HI6           = 0x0006e,
	    GET_PSUV_HI7           = 0x0006f,
	    GET_GUNNV0_HI          = 0x00070,
	    GET_IFPWR0_HI          = 0x00071,
	    GET_GUNNV1_HI          = 0x00072,
	    GET_IFPWR1_HI          = 0x00073,
	    GET_TEMP0              = 0x00080,
	    GET_TEMP1              = 0x00081,
	    GET_TEMP2              = 0x00082,
	    GET_TEMP3              = 0x00083,
	    GET_TEMP4              = 0x00084,
	    GET_TEMP5              = 0x00085,
	    GET_TEMP6              = 0x00086,
	    GET_TEMP7              = 0x00087,
	    GET_PSUV0              = 0x00088,
	    GET_PSUV1              = 0x00089,
	    GET_PSUV2              = 0x0008a,
	    GET_PSUV3              = 0x0008b,
	    GET_PSUV4              = 0x0008c,
	    GET_PSUV5              = 0x0008d,
	    GET_PSUV6              = 0x0008e,
	    GET_PSUV7              = 0x0008f,
            GET_GUNNV0             = 0x00090,
            GET_IFPWR0             = 0x00091,
            GET_GUNNV1             = 0x00092,
            GET_IFPWR1             = 0x00093,
            GET_ALM_DIS0           = 0x000a0,
            GET_ALM_DIS1           = 0x000a1,
            GET_ALM_DIS2           = 0x000a2,
            GET_ALM_DIS3           = 0x000a3,
            GET_ALM_QT0            = 0x000a4,
            GET_ALM_QT1            = 0x000a5,
            GET_ALM_QT2            = 0x000a6,
            GET_ALM_QT3            = 0x000a7,
            GET_ALM_STATE0         = 0x000ac,
            GET_ALM_STATE1         = 0x000ad,
            GET_ALM_STATE2         = 0x000ae,
            GET_ALM_STATE3         = 0x000af,
	    GET_RAW_TSKY0          = 0x000c0,
	    GET_RAW_TSKY1          = 0x000c1,
	    GET_RAW_TSKY2          = 0x000c2,
	    GET_RAW_TSKY3          = 0x000c3,
	    GET_RAW_TSKY4          = 0x000c4,
	    GET_RAW_TSKY5          = 0x000c5,
	    GET_RAW_TSKY6          = 0x000c6,
	    GET_RAW_TSKY7          = 0x000c7,
	    GET_TSKY0              = 0x00100,
	    GET_TSKY1              = 0x00101,
	    GET_TSKY2              = 0x00102,
	    GET_TSKY3              = 0x00103,
	    GET_TSKY4              = 0x00104,
	    GET_TSKY5              = 0x00105,
	    GET_TSKY6              = 0x00106,
	    GET_TSKY7              = 0x00107,
	    GET_CALG_LO            = 0x00180,
	    GET_CALG_HI            = 0x00181,
	    GET_CALTRX_LO          = 0x00182,
	    GET_CALTRX_HI          = 0x00183,
	    GET_TRX_SM0            = 0x00190,
	    GET_TRX_SM1            = 0x00191,
	    GET_TSKYSTAMP          = 0x001a0,
            GET_CALG0              = 0x001c0,
            GET_CALG1              = 0x001c1,
            GET_CALG2              = 0x001c2,
            GET_CALG3              = 0x001c3,
            GET_CALG4              = 0x001c4,
            GET_CALG5              = 0x001c5,
            GET_CALG6              = 0x001c6,
            GET_CALG7              = 0x001c7,
            GET_CALG8              = 0x001c8,
            GET_CALG9              = 0x001c9,
            GET_CALG10             = 0x001ca,
            GET_CALG11             = 0x001cb,
            GET_CALG12             = 0x001cc,
            GET_CALG13             = 0x001cd,
            GET_CALG14             = 0x001ce,
            GET_CALG15             = 0x001cf,
            GET_CALTRX0            = 0x001d0,
            GET_CALTRX1            = 0x001d1,
            GET_CALTRX2            = 0x001d2,
            GET_CALTRX3            = 0x001d3,
            GET_CALTRX4            = 0x001d4,
            GET_CALTRX5            = 0x001d5,
            GET_CALTRX6            = 0x001d6,
            GET_CALTRX7            = 0x001d7,
            GET_CALTRX8            = 0x001d8,
            GET_CALTRX9            = 0x001d9,
            GET_CALTRX10           = 0x001da,
            GET_CALTRX11           = 0x001db,
            GET_CALTRX12           = 0x001dc,
            GET_CALTRX13           = 0x001dd,
            GET_CALTRX14           = 0x001de,
            GET_CALTRX15           = 0x001df,
            GET_T0_AMB0            = 0x001e0,
            GET_T0_AMB1            = 0x001e1,
            GET_T0_AMB2            = 0x001e2,
            GET_T0_AMB3            = 0x001e3,
            GET_T0_AMB4            = 0x001e4,
            GET_T0_AMB5            = 0x001e5,
            GET_T0_AMB6            = 0x001e6,
            GET_T0_AMB7            = 0x001e7,
            GET_T0_COLD0           = 0x001e8,
            GET_T0_COLD1           = 0x001e9,
            GET_T0_COLD2           = 0x001ea,
            GET_T0_COLD3           = 0x001eb,
            GET_T0_COLD4           = 0x001ec,
            GET_T0_COLD5           = 0x001ed,
            GET_T0_COLD6           = 0x001ee,
            GET_T0_COLD7           = 0x001ef,
            GET_RAW_AD0            = 0x001f0,
            GET_RAW_AD1            = 0x001f1,
            GET_RAW_AD2            = 0x001f2,
            GET_RAW_AD3            = 0x001f3,
            GET_RAW_AD4            = 0x001f4,
            GET_RAW_AD5            = 0x001f5,
            GET_RAW_AD6            = 0x001f6,
            GET_RAW_AD7            = 0x001f7,
            GET_RAW_AD8            = 0x001f8,
            GET_RAW_AD9            = 0x001f9,
            GET_RAW_AD10           = 0x001fa,
            GET_RAW_AD11           = 0x001fb,
            GET_RAW_AD12           = 0x001fc,
            GET_RAW_AD13           = 0x001fd,
            GET_RAW_AD14           = 0x001fe,
            GET_RAW_AD15           = 0x001ff
	    //@}
	};

	/// Control Functions for the WVR
	enum Controls_t {
	    SET_WVR_STATE          = 0x00001,
	    SET_INT_TIME           = 0x00002,
            SET_BLK_TIME           = 0x00003,
	    SET_CYC_IN_RES         = 0x00004,
            SET_CYC_IN_CAL         = 0x00005,
            SET_CAL_PER            = 0x00006,
	    SET_LO_STATE           = 0x00007,
            SET_PHASE_TIME         = 0x00008,
	    SET_IHL_DATA           = 0x00009,
	    SET_ICL_DATA           = 0x0000a,
	    SET_HTR0_DATA          = 0x0000b,
	    SET_HTR1_DATA          = 0x0000c,
	    SET_PELT_SENS          = 0x0000e,
	    SET_PELT_THRESH        = 0x0000f,
	    SET_DDS_DATA0          = 0x00010,
	    SET_DDS_DATA1          = 0x00011,
	    SET_NV_STORE           = 0x00020,
	    SET_NV_LOAD            = 0x00021,
	    SET_FORCE_CAL          = 0x00022,
	    SET_FORCE_INT          = 0x00023,
	    SET_FORCE_CYC          = 0x00024,
            SET_DIAG_CONF          = 0x00025,
            SET_VANE_POS           = 0x00027,
	    SET_WVR_CONF           = 0x00028,
            SET_VANE_TMOUT         = 0x00030,
            SET_CHOP_TMOUT         = 0x00032,
            SET_TEMP_LO0           = 0x00040,
            SET_TEMP_LO1           = 0x00041,
            SET_TEMP_LO2           = 0x00042,
            SET_TEMP_LO3           = 0x00043,
            SET_TEMP_LO4           = 0x00044,
            SET_TEMP_LO5           = 0x00045,
            SET_TEMP_LO6           = 0x00046,
            SET_TEMP_LO7           = 0x00047,
            SET_PSUV_LO0           = 0x00048,
            SET_PSUV_LO1           = 0x00049,
            SET_PSUV_LO2           = 0x0004a,
            SET_PSUV_LO3           = 0x0004b,
            SET_PSUV_LO4           = 0x0004c,
            SET_PSUV_LO5           = 0x0004d,
            SET_PSUV_LO6           = 0x0004e,
            SET_PSUV_LO7           = 0x0004f,
            SET_GUNNV0_LO          = 0x00050,
            SET_IFPWR0_LO          = 0x00051,
            SET_GUNNV1_LO          = 0x00052,
            SET_IFPWR1_LO          = 0x00053,
            SET_TEMP_HI0           = 0x00060,
            SET_TEMP_HI1           = 0x00061,
            SET_TEMP_HI2           = 0x00062,
            SET_TEMP_HI3           = 0x00063,
            SET_TEMP_HI4           = 0x00064,
            SET_TEMP_HI5           = 0x00065,
            SET_TEMP_HI6           = 0x00066,
            SET_TEMP_HI7           = 0x00067,
            SET_PSUV_HI0           = 0x00068,
            SET_PSUV_HI1           = 0x00069,
            SET_PSUV_HI2           = 0x0006a,
            SET_PSUV_HI3           = 0x0006b,
            SET_PSUV_HI4           = 0x0006c,
            SET_PSUV_HI5           = 0x0006d,
            SET_PSUV_HI6           = 0x0006e,
            SET_PSUV_HI7           = 0x0006f,
            SET_GUNNV0_HI          = 0x00070,
            SET_IFPWR0_HI          = 0x00071,
            SET_GUNNV1_HI          = 0x00072,
            SET_IFPWR1_HI          = 0x00073,
            SET_ALM_DIS0           = 0x000a0,
            SET_ALM_DIS1           = 0x000a1,
            SET_ALM_DIS2           = 0x000a2,
            SET_ALM_DIS3           = 0x000a3,
            SET_ALM_QT0            = 0x000a4,
            SET_ALM_QT1            = 0x000a5,
            SET_ALM_QT2            = 0x000a6,
            SET_ALM_QT3            = 0x000a7,
            SET_ALM_CLR            = 0x000a8,
            SET_CALG_LO            = 0x00180,
            SET_CALG_HI            = 0x00181,
            SET_CALTRX_LO          = 0x00182,
            SET_CALTRX_HI          = 0x00183,
            SET_TRX_SM0            = 0x00190,
            SET_TRX_SM1            = 0x00191,
            SET_T0_AMB0            = 0x001e0,
            SET_T0_AMB1            = 0x001e1,
            SET_T0_AMB2            = 0x001e2,
            SET_T0_AMB3            = 0x001e3,
            SET_T0_AMB4            = 0x001e4,
            SET_T0_AMB5            = 0x001e5,
            SET_T0_AMB6            = 0x001e6,
            SET_T0_AMB7            = 0x001e7,
            SET_T0_COLD0           = 0x001e8,
            SET_T0_COLD1           = 0x001e9,
            SET_T0_COLD2           = 0x001ea,
            SET_T0_COLD3           = 0x001eb,
            SET_T0_COLD4           = 0x001ec,
            SET_T0_COLD5           = 0x001ed,
            SET_T0_COLD6           = 0x001ee,
            SET_T0_COLD7           = 0x001ef
	};
    
	// Throw this if something bad happens.
	class WVRError : public CAN::Error {
	  public:
	    WVRError(const std::string &error) : CAN::Error(error) {;}
	};
	
	/// Get functions for unit testing
	//@{
	unsigned long getSTATE();

    
      private:
	/// Take the default implementation = return 0,0,0,0
	virtual std::vector<CAN::byte_t>  get_can_error() const;
    
	/// Take the default implementation = return 0,0,0
	virtual std::vector<CAN::byte_t>  get_protocol_rev_level() const;
    
	/// Return the number of transactions
	virtual unsigned int get_trans_num() const;
    
	/// Temperature on M&C interface board.
	double get_ambient_temperature() const;
    
	// WVR specific monitor points.    
	std::vector<CAN::byte_t> CAN_WVR_STATE() const;
	std::vector<CAN::byte_t> CAN_INT_TIME() const;
	std::vector<CAN::byte_t> CAN_BLK_TIME() const;
	std::vector<CAN::byte_t> CAN_CYC_IN_RES() const;
	std::vector<CAN::byte_t> CAN_CYC_IN_CAL() const;
	std::vector<CAN::byte_t> CAN_CAL_PER() const;
	std::vector<CAN::byte_t> CAN_LO_STATE() const;
	std::vector<CAN::byte_t> CAN_PHASE_TIME() const;
	std::vector<CAN::byte_t> CAN_IHL_DATA() const;
	std::vector<CAN::byte_t> CAN_ICL_DATA() const;
	std::vector<CAN::byte_t> CAN_HTR0_DATA() const;
	std::vector<CAN::byte_t> CAN_HTR1_DATA() const;
	std::vector<CAN::byte_t> CAN_PELT_SENS() const;
	std::vector<CAN::byte_t> CAN_PELT_THRESH() const;
	std::vector<CAN::byte_t> CAN_DDS_DATA0() const;
	std::vector<CAN::byte_t> CAN_DDS_DATA1() const;
	std::vector<CAN::byte_t> CAN_DIAG_CONF() const;
	std::vector<CAN::byte_t> CAN_VANE_POS() const;
	std::vector<CAN::byte_t> CAN_WVR_CONF() const;
	std::vector<CAN::byte_t> CAN_PLL_LOCK() const;
	std::vector<CAN::byte_t> CAN_YIG_LOCK() const;
	std::vector<CAN::byte_t> CAN_IF_PWROK() const;
	std::vector<CAN::byte_t> CAN_REF_OK() const;
	std::vector<CAN::byte_t> CAN_CHOP_LOCK() const;
	std::vector<CAN::byte_t> CAN_CHOP_SENS() const;
	std::vector<CAN::byte_t> CAN_CHOP_STATE() const;
	std::vector<CAN::byte_t> CAN_VANE_TMOUT() const;
	std::vector<CAN::byte_t> CAN_CHOP_TMOUT() const;
	std::vector<CAN::byte_t> CAN_ICL_STATE() const;
	std::vector<CAN::byte_t> CAN_HTR0_STATE() const;
	std::vector<CAN::byte_t> CAN_HTR1_STATE() const;
	std::vector<CAN::byte_t> CAN_VANE_STATE() const;
	std::vector<CAN::byte_t> CAN_TEMP_LO0() const;
	std::vector<CAN::byte_t> CAN_TEMP_LO1() const;
	std::vector<CAN::byte_t> CAN_TEMP_LO2() const;
	std::vector<CAN::byte_t> CAN_TEMP_LO3() const;
	std::vector<CAN::byte_t> CAN_TEMP_LO4() const;
	std::vector<CAN::byte_t> CAN_TEMP_LO5() const;
	std::vector<CAN::byte_t> CAN_TEMP_LO6() const;
	std::vector<CAN::byte_t> CAN_TEMP_LO7() const;
	std::vector<CAN::byte_t> CAN_PSUV_LO0() const;
	std::vector<CAN::byte_t> CAN_PSUV_LO1() const;
	std::vector<CAN::byte_t> CAN_PSUV_LO2() const;
	std::vector<CAN::byte_t> CAN_PSUV_LO3() const;
	std::vector<CAN::byte_t> CAN_PSUV_LO4() const;
	std::vector<CAN::byte_t> CAN_PSUV_LO5() const;
	std::vector<CAN::byte_t> CAN_PSUV_LO6() const;
	std::vector<CAN::byte_t> CAN_PSUV_LO7() const;
	std::vector<CAN::byte_t> CAN_GUNNV0_LO() const;
	std::vector<CAN::byte_t> CAN_IFPWR0_LO() const;
	std::vector<CAN::byte_t> CAN_GUNNV1_LO() const;
	std::vector<CAN::byte_t> CAN_IFPWR1_LO() const;
	std::vector<CAN::byte_t> CAN_TEMP_HI0() const;
	std::vector<CAN::byte_t> CAN_TEMP_HI1() const;
	std::vector<CAN::byte_t> CAN_TEMP_HI2() const;
	std::vector<CAN::byte_t> CAN_TEMP_HI3() const;
	std::vector<CAN::byte_t> CAN_TEMP_HI4() const;
	std::vector<CAN::byte_t> CAN_TEMP_HI5() const;
	std::vector<CAN::byte_t> CAN_TEMP_HI6() const;
	std::vector<CAN::byte_t> CAN_TEMP_HI7() const;
	std::vector<CAN::byte_t> CAN_PSUV_HI0() const;
	std::vector<CAN::byte_t> CAN_PSUV_HI1() const;
	std::vector<CAN::byte_t> CAN_PSUV_HI2() const;
	std::vector<CAN::byte_t> CAN_PSUV_HI3() const;
	std::vector<CAN::byte_t> CAN_PSUV_HI4() const;
	std::vector<CAN::byte_t> CAN_PSUV_HI5() const;
	std::vector<CAN::byte_t> CAN_PSUV_HI6() const;
	std::vector<CAN::byte_t> CAN_PSUV_HI7() const;
	std::vector<CAN::byte_t> CAN_GUNNV0_HI() const;
	std::vector<CAN::byte_t> CAN_IFPWR0_HI() const;
	std::vector<CAN::byte_t> CAN_GUNNV1_HI() const;
	std::vector<CAN::byte_t> CAN_IFPWR1_HI() const;
	std::vector<CAN::byte_t> CAN_TEMP0() const;
	std::vector<CAN::byte_t> CAN_TEMP1() const;
	std::vector<CAN::byte_t> CAN_TEMP2() const;
	std::vector<CAN::byte_t> CAN_TEMP3() const;
	std::vector<CAN::byte_t> CAN_TEMP4() const;
	std::vector<CAN::byte_t> CAN_TEMP5() const;
	std::vector<CAN::byte_t> CAN_TEMP6() const;
	std::vector<CAN::byte_t> CAN_TEMP7() const;
	std::vector<CAN::byte_t> CAN_PSUV0() const;
	std::vector<CAN::byte_t> CAN_PSUV1() const;
	std::vector<CAN::byte_t> CAN_PSUV2() const;
	std::vector<CAN::byte_t> CAN_PSUV3() const;
	std::vector<CAN::byte_t> CAN_PSUV4() const;
	std::vector<CAN::byte_t> CAN_PSUV5() const;
	std::vector<CAN::byte_t> CAN_PSUV6() const;
	std::vector<CAN::byte_t> CAN_PSUV7() const;
	std::vector<CAN::byte_t> CAN_GUNNV0() const;
	std::vector<CAN::byte_t> CAN_IFPWR0() const;
	std::vector<CAN::byte_t> CAN_GUNNV1() const;
	std::vector<CAN::byte_t> CAN_IFPWR1() const;
	std::vector<CAN::byte_t> CAN_ALM_DIS0() const;
	std::vector<CAN::byte_t> CAN_ALM_DIS1() const;
	std::vector<CAN::byte_t> CAN_ALM_DIS2() const;
	std::vector<CAN::byte_t> CAN_ALM_DIS3() const;
	std::vector<CAN::byte_t> CAN_ALM_QT0() const;
	std::vector<CAN::byte_t> CAN_ALM_QT1() const;
	std::vector<CAN::byte_t> CAN_ALM_QT2() const;
	std::vector<CAN::byte_t> CAN_ALM_QT3() const;
	std::vector<CAN::byte_t> CAN_ALM_STATE0() const;
	std::vector<CAN::byte_t> CAN_ALM_STATE1() const;
	std::vector<CAN::byte_t> CAN_ALM_STATE2() const;
	std::vector<CAN::byte_t> CAN_ALM_STATE3() const;
	std::vector<CAN::byte_t> CAN_RAW_TSKY0() const;
	std::vector<CAN::byte_t> CAN_RAW_TSKY1() const;
	std::vector<CAN::byte_t> CAN_RAW_TSKY2() const;
	std::vector<CAN::byte_t> CAN_RAW_TSKY3() const;
	std::vector<CAN::byte_t> CAN_RAW_TSKY4() const;
	std::vector<CAN::byte_t> CAN_RAW_TSKY5() const;
	std::vector<CAN::byte_t> CAN_RAW_TSKY6() const;
	std::vector<CAN::byte_t> CAN_RAW_TSKY7() const;
	std::vector<CAN::byte_t> CAN_TSKY0() const;
	std::vector<CAN::byte_t> CAN_TSKY1() const;
	std::vector<CAN::byte_t> CAN_TSKY2() const;
	std::vector<CAN::byte_t> CAN_TSKY3() const;
	std::vector<CAN::byte_t> CAN_TSKY4() const;
	std::vector<CAN::byte_t> CAN_TSKY5() const;
	std::vector<CAN::byte_t> CAN_TSKY6() const;
	std::vector<CAN::byte_t> CAN_TSKY7() const;
	std::vector<CAN::byte_t> CAN_CALG_LO() const;
	std::vector<CAN::byte_t> CAN_CALG_HI() const;
	std::vector<CAN::byte_t> CAN_CALTRX_LO() const;
	std::vector<CAN::byte_t> CAN_CALTRX_HI() const;
	std::vector<CAN::byte_t> CAN_TRX_SM0() const;
	std::vector<CAN::byte_t> CAN_TRX_SM1() const;
	std::vector<CAN::byte_t> CAN_TSKYSTAMP() const;
	std::vector<CAN::byte_t> CAN_CALG0() const;
	std::vector<CAN::byte_t> CAN_CALG1() const;
	std::vector<CAN::byte_t> CAN_CALG2() const;
	std::vector<CAN::byte_t> CAN_CALG3() const;
	std::vector<CAN::byte_t> CAN_CALG4() const;
	std::vector<CAN::byte_t> CAN_CALG5() const;
	std::vector<CAN::byte_t> CAN_CALG6() const;
	std::vector<CAN::byte_t> CAN_CALG7() const;
	std::vector<CAN::byte_t> CAN_CALG8() const;
	std::vector<CAN::byte_t> CAN_CALG9() const;
	std::vector<CAN::byte_t> CAN_CALG10() const;
	std::vector<CAN::byte_t> CAN_CALG11() const;
	std::vector<CAN::byte_t> CAN_CALG12() const;
	std::vector<CAN::byte_t> CAN_CALG13() const;
	std::vector<CAN::byte_t> CAN_CALG14() const;
	std::vector<CAN::byte_t> CAN_CALG15() const;
	std::vector<CAN::byte_t> CAN_CALTRX0() const;
	std::vector<CAN::byte_t> CAN_CALTRX1() const;
	std::vector<CAN::byte_t> CAN_CALTRX2() const;
	std::vector<CAN::byte_t> CAN_CALTRX3() const;
	std::vector<CAN::byte_t> CAN_CALTRX4() const;
	std::vector<CAN::byte_t> CAN_CALTRX5() const;
	std::vector<CAN::byte_t> CAN_CALTRX6() const;
	std::vector<CAN::byte_t> CAN_CALTRX7() const;
	std::vector<CAN::byte_t> CAN_CALTRX8() const;
	std::vector<CAN::byte_t> CAN_CALTRX9() const;
	std::vector<CAN::byte_t> CAN_CALTRX10() const;
	std::vector<CAN::byte_t> CAN_CALTRX11() const;
	std::vector<CAN::byte_t> CAN_CALTRX12() const;
	std::vector<CAN::byte_t> CAN_CALTRX13() const;
	std::vector<CAN::byte_t> CAN_CALTRX14() const;
	std::vector<CAN::byte_t> CAN_CALTRX15() const;
	std::vector<CAN::byte_t> CAN_T0_AMB0() const;
	std::vector<CAN::byte_t> CAN_T0_AMB1() const;
	std::vector<CAN::byte_t> CAN_T0_AMB2() const;
	std::vector<CAN::byte_t> CAN_T0_AMB3() const;
	std::vector<CAN::byte_t> CAN_T0_AMB4() const;
	std::vector<CAN::byte_t> CAN_T0_AMB5() const;
	std::vector<CAN::byte_t> CAN_T0_AMB6() const;
	std::vector<CAN::byte_t> CAN_T0_AMB7() const;
	std::vector<CAN::byte_t> CAN_T0_COLD0() const;
	std::vector<CAN::byte_t> CAN_T0_COLD1() const;
	std::vector<CAN::byte_t> CAN_T0_COLD2() const;
	std::vector<CAN::byte_t> CAN_T0_COLD3() const;
	std::vector<CAN::byte_t> CAN_T0_COLD4() const;
	std::vector<CAN::byte_t> CAN_T0_COLD5() const;
	std::vector<CAN::byte_t> CAN_T0_COLD6() const;
	std::vector<CAN::byte_t> CAN_T0_COLD7() const;
	std::vector<CAN::byte_t> CAN_RAW_AD0() const;
 	std::vector<CAN::byte_t> CAN_RAW_AD1() const;
 	std::vector<CAN::byte_t> CAN_RAW_AD2() const;
 	std::vector<CAN::byte_t> CAN_RAW_AD3() const;
 	std::vector<CAN::byte_t> CAN_RAW_AD4() const;
	std::vector<CAN::byte_t> CAN_RAW_AD5() const;
	std::vector<CAN::byte_t> CAN_RAW_AD6() const;
	std::vector<CAN::byte_t> CAN_RAW_AD7() const;
	std::vector<CAN::byte_t> CAN_RAW_AD8() const;
	std::vector<CAN::byte_t> CAN_RAW_AD9() const;
	std::vector<CAN::byte_t> CAN_RAW_AD10() const;
	std::vector<CAN::byte_t> CAN_RAW_AD11() const;
	std::vector<CAN::byte_t> CAN_RAW_AD12() const;
	std::vector<CAN::byte_t> CAN_RAW_AD13() const;
	std::vector<CAN::byte_t> CAN_RAW_AD14() const;
	std::vector<CAN::byte_t> CAN_RAW_AD15() const;

	/** Helper function for 'set' commands to check that data size
	 ** is valid, if not throw exception
	 ** @param data Vector of bytes containing data to check
	 ** @param size Expected number of bytes in data std::vector
	 ** @param pExceptionMsg String identifying CAN msg that is
	 ** associated with failed size check.
	 */
	void checkSize( const std::vector<CAN::byte_t> &data, int size,
			const char *pExceptionMsg);
    
	/// \name Undefined and inaccessible - copying does not make sense.
	//@{
	WVR (const WVR &);
	WVR &operator=(const WVR &);
	//@}
    
	/// Bus location (node number).
	node_t m_node;
	
	/// Serial number of this node.
	std::vector<CAN::byte_t> m_sn;
	
	/// Number of transactions. Needs to be mutable as its incremented by the
	/// monitor function, which is const.
	mutable unsigned int m_trans_num;
	// values for all the monitor points
	CAN::byte_t m_errorCode;

	// start time
	timeval start_m;

	// values for all the monitor points
	unsigned short     STATE_m;
	unsigned char      INT_TIME_m;
        unsigned char      BLK_TIME_m;
	unsigned short     CYC_IN_RES_m;
        unsigned char      CYC_IN_CAL_m;
	unsigned short     CAL_PER_m;
	unsigned char      LO_STATE_m;
	unsigned char      PHASE_TIME_m;
	unsigned long long IHL_DATA_m;
	unsigned long long ICL_DATA_m;
        unsigned short     HTR0_DATA_m;
        unsigned short     HTR1_DATA_m;
        unsigned char      PELT_SENS_m;
        unsigned long      PELT_THRESH_m;
	unsigned long long DDS_DATA0_m;
	unsigned long long DDS_DATA1_m;
        unsigned long long DIAG_CONF_m;
        unsigned char      VANE_POS_m;
	unsigned long long WVR_CONF_m;
        unsigned short     VANE_TMOUT_m;
        unsigned char      CHOP_TMOUT_m;
        unsigned short     TEMP_LO0_m;
        unsigned short     TEMP_LO1_m;
        unsigned short     TEMP_LO2_m;
        unsigned short     TEMP_LO3_m;
        unsigned short     TEMP_LO4_m;
        unsigned short     TEMP_LO5_m;
        unsigned short     TEMP_LO6_m;
        unsigned short     TEMP_LO7_m;
        unsigned short     PSUV_LO0_m;
        unsigned short     PSUV_LO1_m;
        unsigned short     PSUV_LO2_m;
        unsigned short     PSUV_LO3_m;
        unsigned short     PSUV_LO4_m;
        unsigned short     PSUV_LO5_m;
        unsigned short     PSUV_LO6_m;
        unsigned short     PSUV_LO7_m;
        unsigned short     GUNNV0_LO_m;
        unsigned short     IFPWR0_LO_m;
        unsigned short     GUNNV1_LO_m;
        unsigned short     IFPWR1_LO_m;
        unsigned short     TEMP_HI0_m;
        unsigned short     TEMP_HI1_m;
        unsigned short     TEMP_HI2_m;
        unsigned short     TEMP_HI3_m;
        unsigned short     TEMP_HI4_m;
        unsigned short     TEMP_HI5_m;
        unsigned short     TEMP_HI6_m;
        unsigned short     TEMP_HI7_m;
        unsigned short     PSUV_HI0_m;
        unsigned short     PSUV_HI1_m;
        unsigned short     PSUV_HI2_m;
        unsigned short     PSUV_HI3_m;
        unsigned short     PSUV_HI4_m;
        unsigned short     PSUV_HI5_m;
        unsigned short     PSUV_HI6_m;
        unsigned short     PSUV_HI7_m;
        unsigned short     GUNNV0_HI_m;
        unsigned short     IFPWR0_HI_m;
        unsigned short     GUNNV1_HI_m;
        unsigned short     IFPWR1_HI_m;
        unsigned long long ALM_DIS0_m;
        unsigned long long ALM_DIS1_m;
        unsigned long long ALM_DIS2_m;
        unsigned long long ALM_DIS3_m;
        unsigned long long ALM_QT0_m;
        unsigned long long ALM_QT1_m;
        unsigned long long ALM_QT2_m;
        unsigned long long ALM_QT3_m;
	unsigned long      CALG_LO_m;
	unsigned long      CALG_HI_m;
	unsigned long      CALTRX_LO_m;
	unsigned long      CALTRX_HI_m;
	unsigned long      TRX_SM0_m;
	unsigned long      TRX_SM1_m;
	unsigned long      T0_AMB0_m;
	unsigned long      T0_AMB1_m;
	unsigned long      T0_AMB2_m;
	unsigned long      T0_AMB3_m;
	unsigned long      T0_AMB4_m;
	unsigned long      T0_AMB5_m;
	unsigned long      T0_AMB6_m;
	unsigned long      T0_AMB7_m;
	unsigned long      T0_COLD0_m;
	unsigned long      T0_COLD1_m;
	unsigned long      T0_COLD2_m;
	unsigned long      T0_COLD3_m;
	unsigned long      T0_COLD4_m;
	unsigned long      T0_COLD5_m;
	unsigned long      T0_COLD6_m;
	unsigned long      T0_COLD7_m;

    };  // class WVR
}  // namespace AMB

#endif  // WVR_H
