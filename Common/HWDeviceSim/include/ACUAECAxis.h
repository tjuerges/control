// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#ifndef ACU_AXIS_H
#define ACU_AXIS_H

#include "AMBSimACUAECbase.h"

// time between calls to tick()
#define TIME_UNIT   0.020

/*
 * ACUAxis Class - track an axis motion
 */
class ACUAECAxis
{
  public:

    // just hold these bits for a get
#define N_COEFFS    16      // number of servo coefficients
    double servo_coefficients[N_COEFFS];

#define PT_N_COEFFS 10      // number of pointing model coefficients
    double pt_model_coefficients[PT_N_COEFFS];

    // limit bits
    unsigned char limit;

    int debug_level;

    ACUAECAxis();

    // clock tick to update axis state
    void tick();
      
    // perform a state change
    void transition();

    // evaluate if mode change is valid
    // This is a transition table.
    AEC_AZEL_Mode_t evaluate(AEC_AZEL_Mode_t m);

    // set default servo coefficients
    void set_servo_defaults();

    // check for zero speed condition
    bool driving() const;

    // set operation mode, returns 0 (OK) ! 1 (ERROR)
    int set_mode(AEC_AZEL_Mode_t m);

    AEC_AZEL_Mode_t get_mode() const;

    AEC_AZEL_Brake_t get_brake() const;

    int set_brake(AEC_AZEL_Brake_t b);

    int get_stow_pin() const;

    void set_stow_pin(AEC_Stow_Pin_t arg);

    void get_cur(double& tp,double& tv,double& ta) const;

    void get_track(double &tp,double& tv,double& ta) const;

    void servo();

    bool inrange(double actual,double delta,double compare);

    void inc_track();

    void inc_cur();

    // look at error and calculate state, acceleration, drive, etc.
    void drive();

    int set(double x,double vel);

    void limits(double lower,double upper);

    void stows(double maint,double survive);

  private:
    /*
     * DRIVE_MODE - the axis motion goes through different phases
     * to drive to a point.  To drive in the shortest time, the
     * motion should accelerate at maximum acceleration and maximum
     * velocity if needed.
     */
    enum DRIVE_MODE { ACCELERATE, DECELERATE, SLEW, SERVO, STOP };
    enum ACU_Axis_Status_t
    {
	PRELIM_UP               = 0x00000001,
	LIMIT_UP                = 0x00000002,
	PRELIM_DOWN             = 0x00000010,
	LIMIT_DOWN              = 0x00000020
    };

    // current drive position, etc.
    double cur_pos;
    double cur_vel;
    double cur_acc;

    // offset from track position to decelerate
    double decel_offset, decel_vel;

    DRIVE_MODE drive_state;

    // current tracked position
    double track_pos;
    double track_vel;
    double delta_track;

    double lower_limit;
    double upper_limit;

    // stow positions
    double mstow;
    double sstow;

    // clock tick
    int count;

    AEC_AZEL_Mode_t newmode;
    AEC_AZEL_Mode_t mode;
    AEC_AZEL_Brake_t brake;
    int pin;

    // position change flag for timer loop
    bool newpos;
};

#endif /* ACU_AXIS_H */
