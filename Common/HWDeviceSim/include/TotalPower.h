/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2002 
*
* This code is under GNU General Public Licence (GPL). 
* Correspondence concerning ALMA Software should be addressed to:
* alma-sw-admin@nrao.edu
*
* Copyright (C) 2001
* Associated Universities, Inc. Washington DC, USA.
*
* Produced for the ALMA project
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Library General Public License
* as published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Library General Public License for more details.
*
* You should have received a copy of the GNU Library General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* dfugate  16/12/02  created 
*/

#ifndef TOTALPOWER_H
#define TOTALPOWER_H

#include "AMBDevice.h"
#include "CANError.h"
#include <string>

namespace AMB 
{
    // Valid for draft TotalPower ICD ALMA-50.01.00.00-70.35.16.00-A
    class TotalPower : public AMB::Device
    {
      public:
	////////////////////////////////////////////////////////////////////
	TotalPower(node_t node, const std::vector<CAN::byte_t> &serialNumber);
	virtual ~TotalPower();
	////////////////////////////////////////////////////////////////////
	virtual node_t 
	node() const;

	virtual std::vector<CAN::byte_t> 
	serialNumber() const;
	////////////////////////////////////////////////////////////////////
	virtual std::vector<CAN::byte_t> 
	monitor(rca_t rca) const;

	virtual void 
	control(rca_t rca, const std::vector<CAN::byte_t> &data);
	////////////////////////////////////////////////////////////////////
	/// Take the default implementation = return 0,0,0,0
	virtual std::vector<CAN::byte_t>  
	get_can_error() const;

	/// Take the default implementation = return 0,0,0
	virtual std::vector<CAN::byte_t>  
	get_protocol_rev_level() const;

	/// Return the number of transactions
	virtual unsigned int 
	get_trans_num() const;

	/// Temperature on M&C interface board. 18.0 constant.
	float 
	get_ambient_temperature() const;
	////////////////////////////////////////////////////////////////////
	/// Returns a std::vector consisting of four shorts that will change 
	/// every time this command is executed within one timing event
	/// IFF start_continuous_conversion_mode has been enabled.  If 
	/// start_continuous_conversion_mode is disabled, returns four 0's.
	std::vector<CAN::byte_t> 
	read_continuous_conversion_data_block() const;

	/// Returns a std::vector consisting of six bytes.
	/// At present, all six bytes are set to 0.
	std::vector<CAN::byte_t> 
	read_digitizer_status_registers() const;

	/// Returns a std::vector consisting of two 16-bit integers.
	/// Corresponds to fifo_data_m[0-1].
	std::vector<CAN::byte_t> 
	read_single_conversion_data_block_ul() const;

	/// Returns a std::vector consisting of two 16-bit integers.
	/// Corresponds to fifo_data_m[2-3].
	std::vector<CAN::byte_t> 
	read_single_conversion_data_block_ab() const;
	
	/// Returns a std::vector consisting of two 16-bit integers.
	/// Corresponds to fifo_data_m[4-5].
	std::vector<CAN::byte_t> 
	read_single_conversion_data_block_cd() const;

	/// Returns a std::vector consisting of two 16-bit integers.
	/// Both ints are 5 IFF not in start_continuous_conversion_mode. Else 0.
	std::vector<CAN::byte_t> 
	read_two_shorts() const;

	/// Returns a std::vector consisting of two 32-bit integers.
	/// Both ints are 5 IFF not in start_continuous_conversion_mode AND 
	/// a reset_digitizers command has been done.  Else 0.
	std::vector<CAN::byte_t> 
	read_two_ints() const;

	/// Returns a std::vector consisting of one byte.
	/// Bits 0-1 -> Data read status:
	///        00:  Read Complete
	///        01:  Read Incomplete
	///        10:  Read past the end of the buffer
	///        11:  Not used
	/// Bit 2    -> In START_CONTINUOUS_CONVERSION_MODE
	/// Bit 3    -> Control command busy
	std::vector<CAN::byte_t> 
	read_digitizer_status() const;

	////////////////////////////////////////////////////////////////////
	/// Initializes digitizers -- only resets the FIFO.
	/// Bits 0-5 are associated with U-D and if a bit is set, the associated
	/// digitizer will be initialized.  In reality, this doesn't do anything
	/// other than: reset the fifo, set bit 3 of read_digitizer_status, 
	//  sleep for 125ms, unset bit 3 of read_digitizer_status.
	void 
	reset_digitizers(CAN::byte_t state);

	/// Starts continuous conversion mode for all digitizers (U-D).  
	/// According to the ICD, certain digitizers can be in this 
	/// mode but this does not work with the monitor points.
	/// For now the byte (and more specifically bits 0-5) are completely
	/// ignored.
	void 
	start_continuous_conversion_mode(CAN::byte_t state);

	/// Stops continuous conversion mode for all digitizers (U-D).  
	/// According to the ICD, certain digitizers can be in this 
	/// mode but this does not work with the monitor points.
	/// For now the byte (and more specifically bits 0-5) are completely
	/// ignored.
	void 
	stop_continuous_conversion_mode(CAN::byte_t state);

	/// Turns on the burnout currents -- doesn't really do anything
	/// For now the byte (and more specifically bits 0-5) are completely
	/// ignored.
	void 
	activate_digitizer_burnout_currents(CAN::byte_t state);

	/// Turns off the burnout currents -- doesn't really do anything
	/// For now the byte (and more specifically bits 0-5) are completely
	/// ignored.
	void 
	de_activate_digitizer_burnout_currents(CAN::byte_t state);


	////////////////////////////////////////////////////////////////////
	// Throw this if something bad happens.
	class TotalPowerError : 
	    public CAN::Error 
	{
	  public:
	    TotalPowerError(const std::string &error) : CAN::Error(error) {;}
	};

	////////////////////////////////////////////////////////////////////
	enum Monitor_t 
	{
	    // Generic monitor points
	    GET_CAN_ERROR           = AMB::Device::GET_CAN_ERROR,
	    GET_PROTOCOL_REV_LEVEL  = AMB::Device::GET_PROTOCOL_REV_LEVEL,
	    GET_TRANS_NUM           = AMB::Device::GET_TRANS_NUM,
	    GET_AMBIENT_TEMPERATURE = AMB::Device::GET_AMBIENT_TEMPERATURE,

	    // Monitor points specific to the DC (from the ICD).
	    READ_CONTINUOUS_CONVERSION_DATA_BLOCK = 0x00000001,
	    READ_DIGITIZER_STATUS_REGISTERS       = 0x00000002,
	    READ_SINGLE_CONVERSION_DATA_BLOCK_UL  = 0x00000003,
	    READ_SINGLE_CONVERSION_DATA_BLOCK_AB  = 0x00000004,
	    READ_SINGLE_CONVERSION_DATA_BLOCK_CD  = 0x00000005,
	    READ_DIGITIZER_UL_MODE_REGISTERS      = 0x00000006,
	    READ_DIGITIZER_AB_MODE_REGISTERS      = 0x00000007,
	    READ_DIGITIZER_CD_MODE_REGISTERS      = 0x00000008,
	    READ_DIGITIZER_UL_FILTER_REGISTERS    = 0x00000009,
	    READ_DIGITIZER_AB_FILTER_REGISTERS    = 0x0000000A,
	    READ_DIGITIZER_CD_FILTER_REGISTERS    = 0x0000000B,
	    READ_DIGITIZER_UL_OFFSET_REGISTERS    = 0x0000000C,
	    READ_DIGITIZER_AB_OFFSET_REGISTERS    = 0x0000000D,
	    READ_DIGITIZER_CD_OFFSET_REGISTERS    = 0x0000000E,
	    READ_DIGITIZER_UL_GAIN_REGISTERS      = 0x0000000F,
	    READ_DIGITIZER_AB_GAIN_REGISTERS      = 0x00000010,
	    READ_DIGITIZER_CD_GAIN_REGISTERS      = 0x00000011,
	    READ_DIGITIZER_STATUS                 = 0x00000012
	};

	enum Controls_t 
	{
	    RESET_DIGITIZERS                       = 0x00000001,
	    START_CONTINUOUS_CONVERSION_MODE       = 0x00000002,
	    STOP_CONTINUOUS_CONVERSION_MODE        = 0x00000003,
	    ACTIVATE_DIGITIZER_BURNOUT_CURRENTS    = 0x00000004,
	    DE_ACTIVATE_DIGITIZER_BURNOUT_CURRENTS = 0x00000005,
	};
	
	////////////////////////////////////////////////////////////////////
      private:

	// Undefined and inaccessible - copying does not make sense.
	TotalPower(const TotalPower &);
	TotalPower &operator=(const TotalPower &);
	
	/// Thread ID
	pthread_t tid;

	/// This updates all monitor points associated with the 48ms timing
	/// event.
	void update();

	/// This is where update is called from.
	static void * 
	timingEvent(void* data);

	////////////////////////////////////////////////////////////////////
	/// Bus location (node number).
	node_t node_m;

	/// Serial number of this node.
	std::vector<CAN::byte_t> sn_m;
	
	/// Number of transactions.
	mutable unsigned int trans_num_m;
	////////////////////////////////////////////////////////////////////
	/// Specific to TP
	mutable bool start_continuous_conversion_mode_m;
	mutable bool reset_digitizers_m;
	mutable bool control_command_busy_m;

	/// Every 48ms, this needs to be reset (i.e., fifo_m=fifo_data_m).
	/// After every "read_continuous_conversion_data_block", pop 8 bytes
	///  off of this pseudo-stack.  This simplification assumes that >36
	///  reads will never be performed within a 48ms timing event.
	mutable unsigned short *fifo_m;
	/// Number of times the fifo has been accessed within one 48ms timing
	///  event.
	mutable int fifo_count_m;
	static const int FIFO_SIZE=144;
	/// fifo_data_m[i]=i
	unsigned short fifo_data_m[FIFO_SIZE];

	/// For now, registers status will always be 0 until something is 
	///  defined within the ICD...
	mutable unsigned char read_digitizer_status_registers_m[6];
	mutable unsigned char read_digitizer_status_m;
	////////////////////////////////////////////////////////////////////
    };

} 

#endif  // TOTALPOWER_H
