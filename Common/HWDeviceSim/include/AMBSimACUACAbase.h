// @(#) $Id$


#ifndef ACUACA_INCLUDE_H
#define ACUACA_INCLUDE_H

#include <vector>

namespace ACAacu {

/*
 * Commonly used enums to describe the ACU
 */

// get_acu_error()
enum ACU_Error_t
{
    INVALID_DATA            = 1,
    COMMAND_NOT_ALLOWED     = 2
};

// acu_mode_rsp()
enum AZEL_Mode_t
{
    SHUTDOWN               = 0x0,
    STANDBY                = 0x1,
    ENCODER                = 0x2,
    AUTONOMOUS             = 0x3,
    SURVIVAL_STOW          = 0x4,
    MAINTENANCE_STOW       = 0x5,
    VELOCITY               = 0x6,
    SELFTEST               = 0x7,
    ACU_LOCAL              = 0x1,
    ACU_REMOTE             = 0x2
};

// get/set_az_brake(), get/set_el_brake()
enum AZEL_Brake_t
{
    BRAKE_OFF = 0,
    BRAKE_ON  = 1
};

// get_shutter()
enum Shutter_Status_Bits_t
{
    SHUTTER_OPENED          = 1,
    SHUTTER_CLOSED          = 2,
    SHUTTER_MOTOR_ON        = 4,
    SHUTTER_SYSTEM_ERROR    = 8
};

// set_shutter()
enum Shutter_Cmd_Bits_t
{
    SHUTTER_CLOSE           = 0,
    SHUTTER_OPEN            = 1
};
 
// set_stow_pin() and get_stow_pin()
enum Stow_Pin_t
{
    AZ_IN                  = 0x0001,
    AZ_OUT                 = 0x0002,
    EL_IN                  = 0x0100,
    EL_OUT                 = 0x0200,
};


struct az_motor_st {
	uint8_t AZ_L1, AZ_L2, AZ_R1, AZ_R2;
};
struct el_motor_st {
	uint8_t EL_L, EL_R;
};
struct antenna_temps {
	uint16_t	receivercabin;
	uint16_t	pedestal;;
};

struct metr_tilt {
	int16_t x;	// tilt x
	int16_t y;	// tilt y
	int16_t t;	// tiltmeter temperature
};
struct selftest_rsp_param {
	uint16_t	failed_test_latest;
	uint16_t	num_of_errors;
	uint8_t		status;
};
struct selftest_err_param {
	union {
		float		param;
		uint32_t	iparam;
	};
	uint16_t	failed_test_index;
};



/*
 * CAN Addresses for ACU M&C Points.
 */
enum Monitor_t		// 2007-06-08fri - MELCO version H.
{
        ACU_MODE_RSP            = 0x00022,
        ACU_TRK_MODE_RSP        = 0x00020,
        AZ_POSN_RSP             = 0x00012,
        EL_POSN_RSP             = 0x00002,
        GET_ACU_ERROR           = 0x0002F,

        GET_AZ_TRAJ_CMD         = 0x00013,
        GET_AZ_BRAKE            = 0x00014,
        GET_AZ_ENC              = 0x00017,
        GET_AZ_MOTOR_CURRENTS   = 0x00019,
        GET_AZ_MOTOR_TEMPS      = 0x0001A,
        GET_AZ_MOTOR_TORQUE     = 0x00015,
        GET_AZ_SERVO_COEFF_0    = 0x03020,
        GET_AZ_STATUS           = 0x0001B,
        GET_AZ_STATUS_2         = 0x00056,
        GET_AZ_ENCODER_OFFSET   = 0x0001C,
        GET_AZ_AUX_MODE         = 0x00016,
        GET_AZ_RATEFDBK_MODE    = 0x0004D,

        GET_CAN_ERROR           = 0x30001,

        GET_EL_TRAJ_CMD         = 0x00003,
        GET_EL_BRAKE            = 0x00004,
        GET_EL_ENC              = 0x00007,
        GET_EL_MOTOR_CURRENTS   = 0x00009,
        GET_EL_MOTOR_TEMPS      = 0x0000A,
        GET_EL_MOTOR_TORQUE     = 0x00005,
        GET_EL_SERVO_COEFF_0    = 0x03010,
        GET_EL_STATUS           = 0x0000B,
        GET_EL_STATUS_2         = 0x00057,
        GET_EL_ENCODER_OFFSET   = 0x0000C,
        GET_EL_AUX_MODE         = 0x00006,
        GET_EL_RATEFDBK_MODE    = 0x0004F,

        GET_SYSTEM_ID           = 0x30004,
        GET_IDLE_STOW_TIME      = 0x00025,
        GET_IP_ADDRESS          = 0x0002D,
        GET_IP_GATEWAY          = 0x00038,
        GET_NUM_TRANS           = 0x30002,
        GET_SYSTEM_STATUS       = 0x00023,
        GET_SYSTEM_STATUS_2     = 0x00055,
        GET_PT_MODEL_COEFF_0    = 0x03040,
        GET_SHUTTER             = 0x0002E,
        GET_STOW_PIN            = 0x00024,

        SUBREF_MODE_RSP         = 0x00042,
        GET_SUBREF_ABS_POSN     = 0x00026,
        GET_SUBREF_DELTA_POSN   = 0x00027,
        GET_SUBREF_LIMITS       = 0x00028,
        GET_SUBREF_ROTATION     = 0x0002A,
        GET_SUBREF_STATUS       = 0x00029,
        GET_SUBREF_PT_COEFF_0   = 0x03060,
        GET_SUBREF_ENCODER_POSN_1 = 0x00051,
        GET_SUBREF_ENCODER_POSN_2 = 0x00052,

        GET_METR_MODE           = 0x00031,
        GET_METR_EQUIP_STATUS   = 0x00032,
        GET_METR_DISPL_0        = 0x06000,
        GET_METR_TEMPS_0        = 0x04000,
        GET_METR_TILT_0         = 0x05000,
        GET_METR_DELTAS         = 0x00034,
        GET_METR_COEFF_0        = 0x03080,
        GET_METR_DELTAPATH      = 0x00053,

        GET_POWER_STATUS        = 0x00030,
        GET_AC_STATUS           = 0x0002C,
        GET_FAN_STATUS          = 0x00054,
        GET_UPS_OUTPUT_VOLTS_1  = 0x00035,
        GET_UPS_OUTPUT_VOLTS_2  = 0x0003B,
        GET_UPS_OUTPUT_CURRENT_1 = 0x00036,
        GET_UPS_OUTPUT_CURRENT_2 = 0x00039,
        GET_ANTENNA_TEMPS       = 0x00037,
        GET_SW_REV_LEVEL        = 0x30000,
        SELFTEST_RSP            = 0x00040,
        SELFTEST_ERR            = 0x00041
};

enum Controls_t
{
        ACU_MODE_CMD            = 0x01022,
        ACU_TRK_MODE_CMD        = 0x01020,
        AZ_TRAJ_CMD             = 0x01012,
        EL_TRAJ_CMD             = 0x01002,
        CLEAR_FAULT_CMD         = 0x01021,
        RESET_ACU_CMD           = 0x0102F,

        SET_AZ_BRAKE            = 0x01014,
        SET_AZ_SERVO_COEFF_0    = 0x02020,
        SET_AZ_SERVO_DEFAULT    = 0x01017,
        SET_AZ_AUX_MODE         = 0x01016,
        SET_AZ_RATEFDBK_MODE    = 0x0104D,

        SET_EL_BRAKE            = 0x01004,
        SET_EL_SERVO_COEFF_0    = 0x02010,
        SET_EL_SERVO_DEFAULT    = 0x01007,
        SET_EL_AUX_MODE         = 0x01006,
        SET_EL_RATEFDBK_MODE    = 0x0104F,

        SET_IDLE_STOW_TIME      = 0x01025,
        SET_IP_ADDRESS          = 0x01024,
        SET_IP_GATEWAY          = 0x01038,

        SET_PT_MODEL_COEFF_0    = 0x02040,
        SET_PT_DEFAULT          = 0x01044,
        SET_SHUTTER             = 0x0102E,
        SET_STOW_PIN            = 0x0102D,

        SUBREF_MODE_CMD         = 0x01042,
        SET_SUBREF_ABS_POSN     = 0x01029,
        SET_SUBREF_DELTA_POSN   = 0x0102A,
        SUBREF_DELTA_ZERO_CMD   = 0x0102B,
        SET_SUBREF_ROTATION     = 0x01028,
        SET_SUBREF_PT_COEFF_0   = 0x02060,
        SET_SUBREF_PT_DEFAULT   = 0x01045,

        SET_METR_MODE           = 0x01026,
        SET_METR_COEFF_0        = 0x02080,
        SET_METR_DEFAULT        = 0x01046,
        SET_METR_CALIBRATION    = 0x01043,

        SET_AC_TEMP             = 0x0102C,
        SELFTEST_CMD            = 0x01030
};


}; // namespace ACAacu
#endif  // ACUACA_INCLUDE_H
