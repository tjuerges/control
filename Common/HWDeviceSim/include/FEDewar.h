/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2002 
*
* This code is under GNU General Public Licence (GPL). 
* Correspondence concerning ALMA Software should be addressed to:
* alma-sw-admin@nrao.edu
*
* Copyright (C) 2001
* Associated Universities, Inc. Washington DC, USA.
*
* Produced for the ALMA project
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Library General Public License
* as published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Library General Public License for more details.
*
* You should have received a copy of the GNU Library General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* aperrigo  30/11/02  created 
* aperrigo  03/12/02  modification due to new ICD
*/

#if !defined(FEDEWAR_H)
#define FEDEWAR_H

#include "AMBDevice.h"
#include "CANError.h"
#include <string>

namespace AMB {

    // Valid for draft Dewar ICD ALMA-41.05.01.00-70.35.00.00-A-ICD
    class FEDewar : public AMB::Device
    {
    public:
	FEDewar(node_t node, const std::vector<CAN::byte_t> &serialNumber);
	virtual ~FEDewar();

	virtual node_t node() const;
	virtual std::vector<CAN::byte_t> serialNumber() const;

	virtual std::vector<CAN::byte_t> monitor(rca_t rca) const;
	virtual void control(rca_t rca, const std::vector<CAN::byte_t> &data);

	// CAN Generic MC points.

	/// Take the default implementation = return 0,0,0,0
	virtual std::vector<CAN::byte_t>  get_can_error() const;

	/// Take the default implementation = return 0,0,0
	virtual std::vector<CAN::byte_t>  get_protocol_rev_level() const;

	/// Return the number of transactions
	virtual unsigned int get_trans_num() const;

	/// Temperature on M&C interface board. 18.0 constant.
	float get_ambient_temperature() const;

	// FE dewar specific monitor points.

	/// Helium supply pressure to the Gifford-McMahon refrigerator.
	/// Range [0, 400psi]  set to 19. psi.
	float get_gm_supply_pressure() const;

	/// Helium return pressure from the Gifford-McMahon refrigerator.
	/// Range [0, 300psi]  set to 20. psi.
	float get_gm_return_pressure() const;

	/// Helium supply pressure to the Joule-Thompson system.
	/// Range [0, 300psi]  set to 21. psi.
	float get_jt_supply_pressure() const;

	/// Helium return pressure from the Joule-Thompson system.
	/// Range [-30, 0 inHG] & [0, 300 psi]  set to 22. psi.
	float get_jt_return_pressure() const;
	
	/// Helium return flow rate from the Joule-Thompson system.
	/// Range [0, 56.64 l/mn]  set to 23. l/mn.
	float get_jt_return_flow() const;

	/// Status of the Varian electro-magnetic valve which is betwen the 
	/// pumping system and the dewar.
	/// 0 - Valve Close, 1 - Valve Open  set to 1.
	std::vector<CAN::byte_t> get_dewar_valve_state() const;

	/// Temperature of the Gifford-McMahon 1st stage.
	/// Set to 24.
	float get_gm_1st_stage() const;

	/// Temperature of the Gifford-McMahon 2nd stage.
	/// Set to 25.
	float get_gm_2nd_stage() const;

	/// Temperature of the 4K stage.
	/// Set to 26.
	float get_4k_stage() const;

	/// Temperature of the top shield.
	/// Set to 27.
	float get_top_shield_temp() const;

	/// T1 for band 3.
	/// Set to 28.
	float get_band3_t1() const;

	/// T2 for band 3.
	/// Set to 29.
	float get_band3_t2() const;

	/// T1 for band 6.
	/// Set to 30.
	float get_band6_t1() const;

	/// T2 for band 6.
	/// Set to 31.
	float get_band6_t2() const;

	/// Dewar vacuum pressure.
	/// Set to 32.
	float get_dwr_vacuum() const;

	/// Pump port vacuum.
	/// Set to 33.
	float get_pump_port_vacuum() const;

	/// Last error occured.
	/// 8 bytes. The last one is the error number set to 0.
	std::vector<CAN::byte_t> get_interface_status() const;


	// Controls

	/// Clear the communication interface of Lake Shore 218 temperature
	/// monitor.
	void set_clr_intrf_ls218(CAN::byte_t state);

	// Throw this if something bad happens.
	class FEDewarError : public CAN::Error {
	public:
	    FEDewarError(const std::string &error) : CAN::Error(error) {;}
	};

	enum Monitor_t {
	    // Generic monitor points
	    GET_CAN_ERROR           = AMB::Device::GET_CAN_ERROR,
	    GET_PROTOCOL_REV_LEVEL  = AMB::Device::GET_PROTOCOL_REV_LEVEL,
	    GET_TRANS_NUM           = AMB::Device::GET_TRANS_NUM,
	    GET_AMBIENT_TEMPERATURE = AMB::Device::GET_AMBIENT_TEMPERATURE,

	    // Monitor points peculiar to the FE dewar (from the ICD).
	    GET_GM_SUPPLY_PRESSURE = 0x00000009,
	    GET_GM_RETURN_PRESSURE = 0x0000000B,
	    GET_JT_SUPPLY_PRESSURE = 0x0000000D,
	    GET_JT_RETURN_PRESSURE = 0x0000000F,
	    GET_JT_RETURN_FLOW     = 0x00000011,
	    GET_DEWAR_VALVE_STATE  = 0x00000013,
	    GET_GM_1ST_STAGE       = 0x00000100,
	    GET_GM_2ND_STAGE       = 0x00000101,
	    GET_4K_STAGE           = 0x00000102,
	    GET_TOP_SHIELD_TEMP    = 0x00000103,
	    GET_BAND3_T1           = 0x00000104,
	    GET_BAND3_T2           = 0x00000105,
	    GET_BAND6_T1           = 0x00000106,
	    GET_BAND6_T2           = 0x00000107,
	    GET_DWR_VACCUM         = 0x00000200,
	    GET_PUMP_PORT_VACUUM   = 0x00000201,
	    GET_INTERFACE_STATUS   = 0x00002000
	};

	enum Controls_t {
	    SET_CLR_INTRF_LS218    = 0x00001100
	};

	/// IEEE 754-1990 format
	std::vector<CAN::byte_t> AMB::FEDewar::IEEEFloatToData(float value) const;

    private:
	// Undefined and inaccessible - copying does not make sense.
	FEDewar(const FEDewar &);
	FEDewar &operator=(const FEDewar &);

	/// Bus location (node number).
	node_t node_m;
	/// Serial number of this node.
	std::vector<CAN::byte_t> sn_m;

	/// Number of transactions.
	mutable unsigned int trans_num_m;

    };  // class FEDewar

}  // namespace AMB

#endif  // FEDEWAR_H
