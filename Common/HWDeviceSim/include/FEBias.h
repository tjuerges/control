// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#if !defined(FE_BIAS_H)
#define FE_BIAS_H

#include "AMBDevice.h"
#include "CANError.h"
#include <string>

namespace AMB {

    // Valid for draft Pending Front End Bias and Control Module ICD dated 2002-08-14
    // TODO - simulate some errors, time-variable values.
    class FEBias : public AMB::Device
    {
    public:
	// ---------------------------------------------------------------------------
	// After construction, all LNAs are disabled, all LNA drain voltages and currents as well
	// as gate voltages, all Schottky bias voltages and junction currents, all SIS mixer bias
	// and junction voltages as well as offsets, and all SIS magnet currents are set to 0.
	// ---------------------------------------------------------------------------
	
	FEBias(node_t node, std::vector<CAN::byte_t> &serialNumber);
	virtual ~FEBias();

	// ---------------------------------------------------------------------------
	// Monitor point RCAs
	// ---------------------------------------------------------------------------
	enum Monitor_t 
	{
	    // Generic monitor points
	    GET_CAN_ERROR           = AMB::Device::GET_CAN_ERROR,
	    GET_PROTOCOL_REV_LEVEL  = AMB::Device::GET_PROTOCOL_REV_LEVEL,
	    GET_TRANS_NUM           = AMB::Device::GET_TRANS_NUM,
	    GET_AMBIENT_TEMPERATURE = AMB::Device::GET_AMBIENT_TEMPERATURE,

	    // Monitor points peculiar to the Fron End Bias & Control Module (from the
	    // ICD).
	    LNA_STATE_MONITOR_START    = 0x00000010,
	    GET_B3_LNA1_STATE          = 0x00000010,
	    GET_B3_LNA2_STATE          = 0x00000011,
	    GET_B6_LNA1_STATE          = 0x00000012,
	    GET_B6_LNA2_STATE          = 0x00000013,
	    LNA_MONITOR_START	       = 0x00000014,
	    GET_B3_LNA1_S1_DRAIN_V     = 0x00000014,
	    GET_B3_LNA1_S1_DRAIN_C     = 0x00000015,
	    GET_B3_LNA1_S1_GATE_V      = 0x00000016,
	    GET_B3_LNA1_S2_DRAIN_V     = 0x00000017,
	    GET_B3_LNA1_S2_DRAIN_C     = 0x00000018,
	    GET_B3_LNA1_S2_GATE_V      = 0x00000019,
	    GET_B3_LNA1_S3_DRAIN_V     = 0x0000001a,
	    GET_B3_LNA1_S3_DRAIN_C     = 0x0000001b,
	    GET_B3_LNA1_S3_GATE_V      = 0x0000001c,
	    GET_B3_LNA1_S4_DRAIN_V     = 0x0000001d,
	    GET_B3_LNA1_S4_DRAIN_C     = 0x0000001e,
	    GET_B3_LNA1_S4_GATE_V      = 0x0000001f,
	    GET_B3_LNA1_S5_DRAIN_V     = 0x00000020,
	    GET_B3_LNA1_S5_DRAIN_C     = 0x00000021,
	    GET_B3_LNA1_S5_GATE_V      = 0x00000022,
	    GET_B3_LNA2_S1_DRAIN_V     = 0x00000023,
	    GET_B3_LNA2_S1_DRAIN_C     = 0x00000024,
	    GET_B3_LNA2_S1_GATE_V      = 0x00000025,
	    GET_B3_LNA2_S2_DRAIN_V     = 0x00000026,
	    GET_B3_LNA2_S2_DRAIN_C     = 0x00000027,
	    GET_B3_LNA2_S2_GATE_V      = 0x00000028,
	    GET_B3_LNA2_S3_DRAIN_V     = 0x00000029,
	    GET_B3_LNA2_S3_DRAIN_C     = 0x0000002a,
	    GET_B3_LNA2_S3_GATE_V      = 0x0000002b,
	    GET_B3_LNA2_S4_DRAIN_V     = 0x0000002c,
	    GET_B3_LNA2_S4_DRAIN_C     = 0x0000002d,
	    GET_B3_LNA2_S4_GATE_V      = 0x0000002e,
	    GET_B3_LNA2_S5_DRAIN_V     = 0x0000002f,
	    GET_B3_LNA2_S5_DRAIN_C     = 0x00000030,
	    GET_B3_LNA2_S5_GATE_V      = 0x00000031,
	    GET_B6_LNA1_S1_DRAIN_V     = 0x00000032,
	    GET_B6_LNA1_S1_DRAIN_C     = 0x00000033,
	    GET_B6_LNA1_S1_GATE_V      = 0x00000034,
	    GET_B6_LNA1_S2_DRAIN_V     = 0x00000035,
	    GET_B6_LNA1_S2_DRAIN_C     = 0x00000036,
	    GET_B6_LNA1_S2_GATE_V      = 0x00000037,
	    GET_B6_LNA1_S3_DRAIN_V     = 0x00000038,
	    GET_B6_LNA1_S3_DRAIN_C     = 0x00000039,
	    GET_B6_LNA1_S3_GATE_V      = 0x0000003a,
	    GET_B6_LNA2_S1_DRAIN_V     = 0x0000003b,
	    GET_B6_LNA2_S1_DRAIN_C     = 0x0000003c,
	    GET_B6_LNA2_S1_GATE_V      = 0x0000003d,
	    GET_B6_LNA2_S2_DRAIN_V     = 0x0000003e,
	    GET_B6_LNA2_S2_DRAIN_C     = 0x0000003f,
	    GET_B6_LNA2_S2_GATE_V      = 0x00000040,
	    GET_B6_LNA2_S3_DRAIN_V     = 0x00000041,
	    GET_B6_LNA2_S3_DRAIN_C     = 0x00000042,
	    GET_B6_LNA2_S3_GATE_V      = 0x00000043,
	    SCHOTTKY_MONITOR_START     = 0x00000044,
	    GET_B3_SCHOTTKY1_PC	       = 0x00000044,
	    GET_B3_SCHOTTKY1_NC	       = 0x00000045,
	    GET_B3_SCHOTTKY2_PC	       = 0x00000046,
	    GET_B3_SCHOTTKY2_NC	       = 0x00000047,
	    SIS_MIXER_MONITOR_START    = 0x00000048,
	    GET_B6_SIS1_VJ	       = 0x00000048,
	    GET_B6_SIS1_IJ	       = 0x00000049,
	    GET_B6_SIS2_VJ	       = 0x0000004a,
	    GET_B6_SIS2_IJ	       = 0x0000004b,
	    SIS_MAGNET_MONITOR_START   = 0x0000004c,
	    GET_B6_MAGNET1	       = 0x0000004c,
	    GET_B6_MAGNET2	       = 0x0000004d,
	    INVALID_MONITOR_RCA,
	    GET_INTERFACE_STATUS       = 0x00002000
	};

	// ---------------------------------------------------------------------------
	// Control point RCAs
	// ---------------------------------------------------------------------------
	enum Controls_t 
	{
	    LNA_STATE_CONTROL_START    = 0x00001010,
	    SET_B3_LNA1_STATE          = 0x00001010,
	    SET_B3_LNA2_STATE          = 0x00001011,
	    SET_B6_LNA1_STATE          = 0x00001012,
	    SET_B6_LNA2_STATE          = 0x00001013,
	    LNA_CONTROL_START	       = 0x00001014,
	    SET_B3_LNA1_S1_DRAIN_V     = 0x00001014,
	    SET_B3_LNA1_S1_DRAIN_C     = 0x00001015,
	    SET_B3_LNA1_S2_DRAIN_V     = 0x00001016,
	    SET_B3_LNA1_S2_DRAIN_C     = 0x00001017,
	    SET_B3_LNA1_S3_DRAIN_V     = 0x00001018,
	    SET_B3_LNA1_S3_DRAIN_C     = 0x00001019,
	    SET_B3_LNA1_S4_DRAIN_V     = 0x0000101a,
	    SET_B3_LNA1_S4_DRAIN_C     = 0x0000101b,
	    SET_B3_LNA1_S5_DRAIN_V     = 0x0000101c,
	    SET_B3_LNA1_S5_DRAIN_C     = 0x0000101d,
	    SET_B3_LNA2_S1_DRAIN_V     = 0x0000101e,
	    SET_B3_LNA2_S1_DRAIN_C     = 0x0000101f,
	    SET_B3_LNA2_S2_DRAIN_V     = 0x00001020,
	    SET_B3_LNA2_S2_DRAIN_C     = 0x00001021,
	    SET_B3_LNA2_S3_DRAIN_V     = 0x00001022,
	    SET_B3_LNA2_S3_DRAIN_C     = 0x00001023,
	    SET_B3_LNA2_S4_DRAIN_V     = 0x00001024,
	    SET_B3_LNA2_S4_DRAIN_C     = 0x00001025,
	    SET_B3_LNA2_S5_DRAIN_V     = 0x00001026,
	    SET_B3_LNA2_S5_DRAIN_C     = 0x00001027,
	    SET_B6_LNA1_S1_DRAIN_V     = 0x00001028,
	    SET_B6_LNA1_S1_DRAIN_C     = 0x00001029,
	    SET_B6_LNA1_S2_DRAIN_V     = 0x0000102a,
	    SET_B6_LNA1_S2_DRAIN_C     = 0x0000102b,
	    SET_B6_LNA1_S3_DRAIN_V     = 0x0000102c,
	    SET_B6_LNA1_S3_DRAIN_C     = 0x0000102d,
	    SET_B6_LNA2_S1_DRAIN_V     = 0x0000102e,
	    SET_B6_LNA2_S1_DRAIN_C     = 0x0000102f,
	    SET_B6_LNA2_S2_DRAIN_V     = 0x00001030,
	    SET_B6_LNA2_S2_DRAIN_C     = 0x00001031,
	    SET_B6_LNA2_S3_DRAIN_V     = 0x00001032,
	    SET_B6_LNA2_S3_DRAIN_C     = 0x00001033,
	    SCHOTTKY_CONTROL_START     = 0x00001034,
	    SET_B3_SCHOTTKY1	       = 0x00001034,
	    SET_B3_SCHOTTKY2	       = 0x00001035,
	    SIS_MIXER_CONTROL_START    = 0x00001036,
	    SET_B6_SIS1		       = 0x00001036,
	    SET_B6_SIS2		       = 0x00001037,
	    SIS_MIXER_OFFSET_START     = 0x00001038,
	    SET_B6_SIS1_BIAS_CAL_VJ    = 0x00001038,
	    SET_B6_SIS1_BIAS_CAL_IJ    = 0x00001039,
	    SET_B6_SIS1_BIAS_CAL_VB    = 0x0000103a,
	    SET_B6_SIS2_BIAS_CAL_VJ    = 0x0000103b,
	    SET_B6_SIS2_BIAS_CAL_IJ    = 0x0000103c,
	    SET_B6_SIS2_BIAS_CAL_VB    = 0x0000103d,
	    SIS_MAGNET_CONTROL_START   = 0x0000103e,
	    SET_B6_MAGNET1	       = 0x0000103e,
	    SET_B6_MAGNET2	       = 0x0000103f,
	    INVALID_CONTROL_RCA
	};

	// ---------------------------------------------------------------------------
	// Front end bias and control module constants
	// ---------------------------------------------------------------------------

	static const int BANDS = 2;		// B3 and B6
	static const int CHANNELS = 2;
	static const int STAGES_FOR_B3 = 5;	// LNA stages per channel for B3
	static const int STAGES_FOR_B6 = 3;	// LNA stages per channel for B6

	// LNA constants
	static const int LNA_MONITORS_PER_STAGE = 3;
	static const int LNA_CONTROLS_PER_STAGE = 2;
	static const int NUMBER_OF_LNAS = BANDS * CHANNELS;
	static const int TOTAL_STAGES_FOR_B3 = STAGES_FOR_B3 * CHANNELS;
	static const int TOTAL_STAGES_FOR_B6 = STAGES_FOR_B6 * CHANNELS;
	static const int LNA_STAGES_PER_CHANNEL = STAGES_FOR_B3 + STAGES_FOR_B6;
	static const int NUMBER_OF_LNA_STAGES = LNA_STAGES_PER_CHANNEL * CHANNELS;
	static int STAGES_PER_LNA[NUMBER_OF_LNAS];
	static const float LNAEnabledDrainVoltage = 2.0; // 2 volts
	static const float LNAEnabledDrainCurrent = 5.0; // 5 mA
	static const float LNAEnabledGateVoltage = 500.0; // 500 mV

	// Schottky constants
	static const int SCHOTTKY_MONITORS_PER_CHANNEL = 2;
	static const int SCHOTTKY_MONITORS = SCHOTTKY_MONITORS_PER_CHANNEL * CHANNELS;
	static const int SCHOTTKY_CONTROLS_PER_CHANNEL = 1;
	static const int SCHOTTKY_CONTROLS = SCHOTTKY_CONTROLS_PER_CHANNEL * CHANNELS;

	// SIS mixer constants
	static const int SIS_MONITORS_PER_CHANNEL = 2;
	static const int SIS_MONITORS = SIS_MONITORS_PER_CHANNEL * CHANNELS;
	static const int SIS_CONTROLS_PER_CHANNEL = 1;
	static const int SIS_CONTROLS = SIS_CONTROLS_PER_CHANNEL * CHANNELS;
	static const int SIS_OFFSETS_PER_CHANNEL = 3;
	static const int SIS_OFFSETS = SIS_OFFSETS_PER_CHANNEL * CHANNELS;


	// ---------------------------------------------------------------------------
	// CAN properties
	// ---------------------------------------------------------------------------

	virtual node_t node() const;
	virtual std::vector<CAN::byte_t> serialNumber() const;

	// ---------------------------------------------------------------------------
	// Monitor and control functions
	// ---------------------------------------------------------------------------
	virtual std::vector<CAN::byte_t> monitor(rca_t rca) const;
	virtual void control(rca_t rca, const std::vector<CAN::byte_t> &data);

	// ---------------------------------------------------------------------------
	// CAN Generic MC points.
	// ---------------------------------------------------------------------------

	/// Take the default implementation = return 0,0,0,0
	virtual std::vector<CAN::byte_t>  get_can_error() const;

	/// Take the default implementation = return 0,0,0
	virtual std::vector<CAN::byte_t>  get_protocol_rev_level() const;

	/// Return the number of transactions
	virtual unsigned int get_trans_num() const;

	/// Temperature on M&C interface board. 18.0 constant.
	float get_ambient_temperature() const;

	// ---------------------------------------------------------------------------
	// FEBias specific monitor points.  All monitor points are specified by RCAs.
	// ---------------------------------------------------------------------------

	/// Return the specified current LNA state (1=enabled, 0=disabled).
	CAN::byte_t getLNAState(Monitor_t state) const;

	/// Return the specified LNA drain voltage
	std::vector<CAN::byte_t> getLNADrainVoltage(Monitor_t LNADrainVoltage) const;

	/// Return the specified LNA drain current 
	std::vector<CAN::byte_t> getLNADrainCurrent(Monitor_t LNADrainCurrent) const;

	/// Return the specified LNA gate voltage
	std::vector<CAN::byte_t> getLNAGateVoltage(Monitor_t LNAGateVoltage) const;

	/// Return the specified Schottky mixer positive junction current
	std::vector<CAN::byte_t> getSchottkyPosCurrent(Monitor_t schottkyPosCurrent) const;

	/// Return the specified Schottky mixer negative junction current
	std::vector<CAN::byte_t> getSchottkyNegCurrent(Monitor_t schottkyNegCurrent) const;

	/// Return the specified SIS mixer bias voltage
	std::vector<CAN::byte_t> getSISVoltage(Monitor_t SISVoltage) const;

	/// Return the specified SIS mixer bias current
	std::vector<CAN::byte_t> getSISCurrent(Monitor_t SISCurrent) const;

	/// Return the specified SIS magnet coil current
	std::vector<CAN::byte_t> getMagnetCurrent(Monitor_t magnetCurrent) const;

	/// Return interface status
	std::vector<CAN::byte_t> getInterfaceStatus() const;

	// ---------------------------------------------------------------------------
	// FEBias specific controls.  All control points are specified by RCAs. 
	// ---------------------------------------------------------------------------

	/// Set the specified LNA state to the passed-in value
	/// If value = 0 (disabled) the drain voltage, drain current and gate voltage
	/// are all set to 0.  For this simulator, if value = 1 (enabled), drain voltage 
	/// is arbitrarily set to 2 V, drain current to 5 mA, and gate voltage to 500 mV. 
	void setLNAState(Controls_t LNAstate, CAN::byte_t value);

	/// Set the specified LNA drain voltage to the passed-in value (0 - 3 V)
	void setLNADrainVoltage(Controls_t LNADrainVoltage,float value );

	/// Set the specified LNA drain current to the passed-in value (0 - 10 mA)
	void setLNADrainCurrent(Controls_t LNADrainCurrent,float value );

	/// Set the specified Schottky bias voltage to the passed-in value (0 - 15 V)
	/// Setting the Schottky bias voltage impacts the Schottky mixer junction currents.
	/// For this simulator a simple linear conversion between bias voltage (0-15V) and
	/// junction currents (0 - 5 mA, -5 - 0 mA) is made.
	void setSchottkyBiasVoltage(Controls_t schottkyBiasVoltage,float value );

	/// Set the specified SIS bias voltage to the passed-in value (0  - 20 mV)
	/// Setting this voltage affects the SIS mixer junction voltage and current.
	/// For this simulator a simple linear conversion between bias voltage (0-10mV) and
	/// junction voltage (0 - 20 mV) and current (0 - 400 uA) is made making use of the
	/// SIS bias offset.
	void setSISBiasVoltage(Controls_t SISBiasVoltage,float value );

	/// Set the specified SIS mixer zero-bias error offset bias voltage to the passed-in 
	/// value (-1 - +1 mV).
	void setSISBiasVoltageOffset(Controls_t SISBiasVoltageOffset,float value );

	/// Set the specified SIS mixer zero-bias error offset voltage to the passed-in 
	/// value (-1 - +1 mV).
	void setSISVoltageOffset(Controls_t SISVoltageOffset,float value );

	/// Set the specified SIS mixer zero-bias error offset current to the passed-in 
	/// value (-10 uA - 10 uA).
	void setSISCurrentOffset(Controls_t SISCurrentOffset,float value );

	/// Set the specified magnet current to the passed-in value (0 - 300 mA).
	void setMagnetCurrent(Controls_t magnetCurrent,float value );

	// Throw this if something bad happens.
	class FEBiasError : public CAN::Error {
	public:
	    FEBiasError(const std::string &error) : CAN::Error(error) {;}
	};

	// ---------------------------------------------------------------------------
	// Utility functions
	// ---------------------------------------------------------------------------
	/// Convert monitor state RCAs to LNA states index
	int stateRCAToIndex(Monitor_t stateRCA) const
		{return stateRCA - LNA_STATE_MONITOR_START;}
	/// Convert control point RCAs to LNA states index
	int stateControlRCAToIndex(Controls_t stateControlRCA)
	    {return stateControlRCA - LNA_STATE_CONTROL_START;}
	/// Convert monitor RCAs to LNA monitors index
	int LNAMonitorRCAToIndex(Monitor_t LNAMonitorRCA) const 
		{return ((LNAMonitorRCA - LNA_MONITOR_START) / LNA_MONITORS_PER_STAGE) % 
									NUMBER_OF_LNA_STAGES;}
	/// Convert control point RCAs to LNA monitors index
	int LNAControlRCAToIndex(Controls_t LNAControlRCA)
		{ return ((LNAControlRCA - LNA_CONTROL_START) / LNA_CONTROLS_PER_STAGE)  % 
									NUMBER_OF_LNA_STAGES;}

	/// Get state for LNA associated with the specified monitor point
	int LNAMonitorToLNAState(int monitorIndex);
	/// Get index to beginning of monitor block for a given LNA 
	int LNAStateToLNAMonitor (int LNAIndex);
	Monitor_t LNAMonitorIndexToMonitorRCA(int LNAMonitorIndex)
	    {return (static_cast<Monitor_t>((LNAMonitorIndex % NUMBER_OF_LNA_STAGES) * 
							LNA_MONITORS_PER_STAGE + 
							LNA_MONITOR_START));}

	/// Convert monitor RCAs to Schottky monitors index 
	int schottkyMonitorRCAToIndex(Monitor_t schottkyMonitorRCA ) const 
		{return ((schottkyMonitorRCA  - SCHOTTKY_MONITOR_START) % SCHOTTKY_MONITORS) /
								SCHOTTKY_MONITORS_PER_CHANNEL;}
	/// Convert control point RCAs to Schottky monitors index 
	int schottkyControlRCAToIndex(Controls_t schottkyControlRCA)
	    {return ((schottkyControlRCA - SCHOTTKY_CONTROL_START)  % SCHOTTKY_MONITORS) / 
							SCHOTTKY_CONTROLS_PER_CHANNEL;}
	/// Convert Schottky monitors index to start of monitor RCAs
	Monitor_t schottkyIndexToMonitorRCA(int schottkyIndex) const 
		{return (static_cast<Monitor_t>((schottkyIndex % CHANNELS)  * 
							SCHOTTKY_MONITORS_PER_CHANNEL + 
							SCHOTTKY_MONITOR_START));}

	/// Convert monitor RCAs to SIS mixer monitors index  
	int SISMixerMonitorRCAToIndex(Monitor_t SISMixerMonitorRCA) const 
		{return ((SISMixerMonitorRCA - SIS_MIXER_MONITOR_START) % SIS_MONITORS) / 
							SIS_MONITORS_PER_CHANNEL;}
	/// Convert control point RCAs to SIS mixer monitors index 
	int SISMixerControlRCAToIndex(Controls_t SISMixerControlRCA)
	    {return ((SISMixerControlRCA - SIS_MIXER_CONTROL_START) % SIS_MONITORS) / 
							SIS_CONTROLS_PER_CHANNEL;}
	/// Convert SIS mixer monitors index to start of monitor RCAs
	Monitor_t SISIndexToMonitorRCA(int sisIndex) const 
		{return (static_cast<Monitor_t>((sisIndex % CHANNELS)  * 
							SIS_MONITORS_PER_CHANNEL + 
							SIS_MIXER_MONITOR_START));}
	/// Convert control point RCAs to SIS offsets index 
	int  SISMixerOffsetRCAToIndex(Controls_t SISMixerOffsetRCA) const
	    {return ((SISMixerOffsetRCA - SIS_MIXER_OFFSET_START) % SIS_OFFSETS) / 
							SIS_OFFSETS_PER_CHANNEL;}

	/// Convert monitor RCAs to SIS magnet currents index
	int SISMagnetMonitorRCAToIndex(Monitor_t SISMagnetMonitorRCA) const 
		{return (SISMagnetMonitorRCA - SIS_MAGNET_MONITOR_START) % CHANNELS;}
	/// Convert SIS magnet currents index to monitor RCAs
	Monitor_t SISMagnetIndexToMonitorRCA(int SISMagnetIndex) const 
		{return (static_cast<Monitor_t>((SISMagnetIndex % CHANNELS) + 
							SIS_MAGNET_MONITOR_START));}
	/// Convert control point RCAs to SIS magnet monitors index
	int SISMagnetControlRCAToIndex(Controls_t SISMagnetControlRCA)
		{return (SISMagnetControlRCA - SIS_MAGNET_CONTROL_START) % CHANNELS;}

	/// Converts the bytes in data to a float.  If successful, returns true and value
	/// contains float.  Otherwise returns false.
	bool validFloat(std::vector<CAN::byte_t> data, float &value);

	/// Converts the float to data bytes.  Unlike AMB::floatToData this function uses
	/// IEEE 754-1990 format
	std::vector<CAN::byte_t> AMB::FEBias::IEEEFloatToData(float value) const;

    private:
	// Undefined and inaccessible - copying does not make sense.
	FEBias(const FEBias &);
	FEBias &operator=(const FEBias &);

	/// Bus location (node number).
	node_t m_Node;
	/// Serial number of this node.
	std::vector<CAN::byte_t> m_SN;
	/// Number of transactions.
	mutable unsigned int m_TransNum;

	/// LNA states properties: enabled/disabled
	CAN::byte_t m_LNAStates[NUMBER_OF_LNAS];
	/// LNA monitors and control points: drain currents, drain voltages, and gate voltages
	float m_LNADrainVoltages[NUMBER_OF_LNA_STAGES];
	float m_LNADrainCurrents[NUMBER_OF_LNA_STAGES];
	float m_LNAGateVoltages[NUMBER_OF_LNA_STAGES];

	/// Schottky mixer bias monitors: negative and positive junction currents
	float m_SchottkyPosCurrents[CHANNELS];
	float m_SchottkyNegCurrents[CHANNELS];
	/// Schottky mixer bias control voltages
	float m_SchottkyBiasVoltages[CHANNELS];

        /// SIS mixer bias monitors: junction voltages and currents
	float m_SISJunctionVoltages[CHANNELS];
	float m_SISJunctionCurrents[CHANNELS];
	/// The following values are used as offsets that are subtracted from SIS mixer monitored 
	/// data. The values themselves are not monitored data, only control points.
	float m_SISBiasVoltages[CHANNELS];
	float m_SIS0BiasVoltages[CHANNELS];
	float m_SIS0BiasCurrents[CHANNELS];
	float m_SIS0BiasBiasVoltages[CHANNELS];

	/// SIS magnet bias monitors: magnet coil currents
	float m_SISMagnetCurrents[CHANNELS];

    };  // class FEBias

}  // namespace AMB

#endif  // FE_BIAS_H
