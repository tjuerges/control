// @(#) @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#ifndef CommonHWSim_H
#define CommonHWSim_H

#include <stdio.h>
#include <vector>
#include <map>

//#include <SharedSimInterface.h>
#include "CANTypes.h"
#include "CANError.h"
#include "TypeConversion.h"
#include <AMBDevice.h>

namespace AMB
{
  /**
  *
  */
  class CommonHWSim : public Device
  {
  public:
	/// Constructor
	CommonHWSim();
 
	/// Destructor
	virtual ~CommonHWSim();

	/// AMB::Device abstract methods (not used)
	virtual node_t node() const;
	virtual std::vector<CAN::byte_t> serialNumber() const;
	virtual unsigned int get_trans_num() const;

	/** Monitor and Control Points creation (state_m map entries)
	    and value initialization. Has to be called from the extending
	    class. */
	virtual void initialize();

	/** Perform a monitor transaction.
	    A monitor is defined to be a transaction that, given a
	    RCA, returns 0-8 bytes of data. Typically the monitor
	    function will have a switch statement that calls the real
	    simulation function, and will then pack the result into
	    the output vector. */
	virtual std::vector<CAN::byte_t> monitor(rca_t rca) const;
	
	/** Control points getter ('get' functions for control points) */
	virtual std::vector<CAN::byte_t> controlGetter(rca_t rca) const;

	/** Perform a control transaction.
	    A control is defined to be a transaction that, takes a RCA 
	    and 1-8 (not zero!) bytes of data. No value is returned to 
	    the caller, although of course the result of the control
	    is presumably visible via a subsequent monitor request.
	    Typically the control function will switch on RCA, unpack
	    the data from the input vector, and call the actual
	    simulation function in "physical" units, e.g. floating
	    point degrees Celsius. */
	virtual void control(rca_t rca, const std::vector<CAN::byte_t>& data);

	/** Monitor points setter ('set' functions for monitor points) */
	virtual void monitorSetter(rca_t rca, const std::vector<CAN::byte_t>& data);

	/// Generic monitor point RCAs
	static const rca_t GET_SERIAL_NUMBER;
	static const rca_t GET_PROTOCOL_REV_LEVEL;
	static const rca_t GET_CAN_ERROR;
	static const rca_t GET_TRANS_NUM;
	static const rca_t GET_AMBIENT_TEMPERATURE;  //optional
	static const rca_t GET_SW_REV_LEVEL;
	static const rca_t GET_HW_REV_LEVEL;

	/// Generic control point RCAs
	static const rca_t RESET_AMBSI;
	static const rca_t RESET_DEVICE;  //optional


  protected:
	/// Values for monitor and control points
	std::map< rca_t , std::vector<CAN::byte_t>* > state_m;

	/// Device name
	std::string devname_m;

	/// Bus location (node number).
	node_t node_m;
	
	/// Generic monitor get helper functions
	virtual std::vector<CAN::byte_t> getSerialNumber() const;
	virtual std::vector<CAN::byte_t> getProtocolRevLevel() const;
	virtual std::vector<CAN::byte_t> getCanError() const;
	virtual std::vector<CAN::byte_t> getTransNum() const;
	virtual std::vector<CAN::byte_t> getAmbientTemperature() const;
	virtual std::vector<CAN::byte_t> getSwRevLevel() const;
	virtual std::vector<CAN::byte_t> getHwRevLevel() const;

	/// Generic control get helper functions
	virtual std::vector<CAN::byte_t> getResetAmbsi() const;
	virtual std::vector<CAN::byte_t> getResetDevice() const;

	/// Generic control set helper functions
	virtual void setResetAmbsi(const std::vector<CAN::byte_t>& data);
	virtual void setResetDevice(const std::vector<CAN::byte_t>& data);

	/// Generic monitor set helper functions
	virtual void setSerialNumber(const std::vector<CAN::byte_t>& data);
	virtual void setProtocolRevLevel(const std::vector<CAN::byte_t>& data);
	virtual void setCanError(const std::vector<CAN::byte_t>& data);
	virtual void setTransNum(const std::vector<CAN::byte_t>& data);
	virtual void setSwRevLevel(const std::vector<CAN::byte_t>& data);
	virtual void setHwRevLevel(const std::vector<CAN::byte_t>& data);
	virtual void setAmbientTemperature(const std::vector<CAN::byte_t>& data);
	
	/** Helper function for 'set' commands to check that data size
	 ** is valid, if not throw exception.
	 ** @param data Vector of bytes containing data to check
	 ** @param size Expected number of bytes in data std::vector
	 ** @param pExceptionMsg String identifying CAN msg that is
	 ** associated with failed size check
	 */
	virtual void checkSize(const std::vector<CAN::byte_t>& data, const int size, const char *pExceptionMsg);
  }; // class CommonHWSim

} // namespace AMB

#endif /* CommonHWSim_H */
