// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#if !defined(ACUAEC_H)
#define ACUAEC_H

#include <string>
#include "AMBDevice.h"
#include "AMBUtil.h"
#include "CANError.h"
#include "ACUAECSim.h"
#include "ACUAECDrive.h"

namespace AMB
{

/**
 * This class is derived from the AMB::Device class and is a concrete class 
 * that implements a CAN bus simulator for the Vertex ACU computer.
 *
 */
class ACUAEC : public AMB::Device
{
  public:

    ACUAEC();
    ACUAEC(node_t node,
	const std::vector<CAN::byte_t>& serialNumber);

    ~ACUAEC();

    //
    // Generic monitor points
    //
    std::vector<CAN::byte_t> get_can_error() const;
    std::vector<CAN::byte_t> get_protocol_rev_level() const;
    std::vector<CAN::byte_t> get_sw_rev_level() const;
    unsigned int get_trans_num() const;

    //
    // AMB::Device functions used by the AMBSimulator
    //
    node_t node() const;
    std::vector<CAN::byte_t> serialNumber() const;
    
    std::vector<CAN::byte_t> monitor(rca_t rca) const;
    void control(rca_t rca,const std::vector<CAN::byte_t>& data);

    //
    // ACU specializations
    //
    // throw these errors if something bad happens
    class ACUError : public CAN::Error
    {
      public:
	ACUError(const std::string& error) : CAN::Error(error) {;}
    };
        
  private:

    std::vector<CAN::byte_t> tempData(const int firstTemp) const;
    std::vector<double> temp_m;

    std::vector<CAN::byte_t> tiltData(const int whichOne) const;
    struct tiltValue {
	double x; // tilts are in arcsecs
	double y; 
	double temp; // temperature is in degrees C
    };
    std::vector<tiltValue> tilt_m;
    
    ACUAECSim sim_m;
    
    //
    // ACU stuff
    //
    long az_enc_m;     // az, el encoder member values
    long el_enc_m;
    unsigned char az_mode_m; // defaults to STANDBY
    unsigned char el_mode_m;
    unsigned char acu_mode_m;   // LOCAL or REMOTE, defaults to REMOTE
    short az_motion_limit_m[2];
    short el_motion_limit_m[2];
    unsigned char az_motor_m;   // defaults to off, disabled
    unsigned char el_motor_m;
    double az_servo_coeff_n[8]; // 8 values for set/get
    double el_servo_coeff_n[8]; // 8 values for set/get
    unsigned short idle_stow_time_m;  // default to 5 minutes.
    unsigned char shutter_m;	// 0- shutter closed, 1- open
    double pt_model_coeff_n[10]; // 10 values for set/get
    short subref_abs_posn_m[3];
    short subref_delta_posn_m[3];
    unsigned char metr_mode_m;  // defaults to ready, disabled 

    //
    // CAN Node stuff
    //
    node_t node_m;		// bus location (node number)
    std::vector<CAN::byte_t> sn_m;	// serial number of this CAN node
    mutable unsigned int trans_num_m;  // number of transactions
    
    // undefined and inaccessible
    ACUAEC(const ACUAEC&);
    ACUAEC& operator = (const ACUAEC&);

};  // class ACU

} // namespace AMB

#endif // ACUAEC_H
