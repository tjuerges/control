// @(#) $Id$
//
// Copyright (C) 2002, 2004
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Library General Public License
// as published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Library General Public License for more details.
//
// You should have received a copy of the GNU Library General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA. 
// Correspondence concerning ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#if !defined(SECOND_LO_H)
#define SECOND_LO_H

#include "AMBDevice.h"
#include "CANError.h"
#include "FTS.h"
#include <string>

namespace AMB
{
  /** The SecondLO class is a simulator for the second local oscillator device.
   */
  class SecondLO : public AMB::FTS
  {
  public:
    /** Constructor
     ** @param node Node ID of this device
     ** @param serialNumber S/N of this device
     ** @param baseband Corresponding Baseband
     */
    SecondLO(node_t node, const std::vector<CAN::byte_t>& serialNumber, const short baseband);

    /// Destructor
    virtual ~SecondLO();
    
    /** Monitor interface
     ** @param CAN node RCA
     ** @return std::vector of bytes containing monitor info. Each response is
     ** sized to what's specified in the ICD.
     */
    virtual std::vector<CAN::byte_t> monitor(rca_t rca) const;
    
    /** Control Interface ('set' functions)
     ** @param rca CAN node RCA
     ** @param data CAN message data required by device
     */
    virtual void control(rca_t rca, const std::vector<CAN::byte_t>& data);

    /// RCA offset for FTS M&C points in Second LO
    static const rca_t FTS_RCA_OFFSET = 0x18000;

    /// Monitor points for the SecondLO
    enum Monitor_t {
        /// Generic monitor points
        //@{
/*         GET_SERIAL_NUMBER       = 0x00000, */
/*         GET_CAN_ERROR           = AMB::Device::GET_CAN_ERROR, */
/*         GET_PROTOCOL_REV_LEVEL  = AMB::Device::GET_PROTOCOL_REV_LEVEL, */
/*         GET_TRANS_NUM           = AMB::Device::GET_TRANS_NUM, */
/*         GET_AMBIENT_TEMPERATURE = AMB::Device::GET_AMBIENT_TEMPERATURE, */
        //@}
      
        /// Specific SecondLO data point monitors
        //@{
        GET_MODULE_STATUS = 0x1,
	GET_COARSE_FREQ = 0x2,
        GET_POWER_SUPPLY_1_VALUE = 0x3,
        GET_POWER_SUPPLY_2_VALUE = 0x4,
        GET_POWER_SUPPLY_3_VALUE = 0x5,
        GET_POWER_SUPPLY_4_VALUE = 0x6,
        GET_DETECTED_RF_POWER    = 0x7,
        GET_DETECTED_IF_POWER    = 0x8,
        GET_DETECTED_FTS_POWER   = 0x9,
        GET_FM_COIL_VOLTAGE      = 0xa
        //@}

        /// FTS monitor points in Second LO CAN address space
        //@{
/*         GET_FREQ = 0x1 + FTS_RCA_OFFSET, */
/*         GET_PHASE_VALS = 0x2 + FTS_RCA_OFFSET, */
/*         GET_PHASE_SEQ1 = 0x3 + FTS_RCA_OFFSET, */
/*         GET_PHASE_SEQ2 = 0X4 + FTS_RCA_OFFSET, */
/*         GET_VOLTAGE_STATUS = 0x6 + FTS_RCA_OFFSET, */
/*         GET_PHASE_OFFSET = 0x7 + FTS_RCA_OFFSET */
        //@}
    };

    /// Control Functions for the SecondLO
    enum Controls_t {
        /// Specific SecondLO control points
        //@{
        SET_COARSE_FREQ = 0x00181,
        SET_LOCK = 0x00182,
        SET_RANGE = 0x00183
        //@}

        /// FTS control points in Second LO CAN address space
        //@{
/*         SET_FREQ = 0x81 + FTS_RCA_OFFSET, */
/*         SET_PHASE_VALS = 0x82 + FTS_RCA_OFFSET, */
/*         SET_PHASE_SEQ1 = 0x83 + FTS_RCA_OFFSET, */
/*         SET_PHASE_SEQ2 = 0x84 + FTS_RCA_OFFSET, */
/*         RESTART = 0x85 + FTS_RCA_OFFSET, */
/*         RESET_VOLTAGE_STATUS = 0x86 + FTS_RCA_OFFSET, */
/*         SET_PHASE_OFFSET = 0x87 + FTS_RCA_OFFSET */
        //@}
    };

    // Throw this if something bad happens.
    class SecondLOError : public CAN::Error {
    public:
      SecondLOError(const std::string &error) : CAN::Error(error) {;}
    };
    
/*     /// Get functions for unit testing */
/*     //@{ */
/*       CAN::byte_t* getLAST_PHASE_COMMAND(); */
/*       CAN::byte_t* getCURRENT_PHASE(); */
/*       CAN::byte_t  getDGCK_STATUS() const; */
/*       CAN::byte_t  getMISSED_COMMAND() const; */
/*       double getLOCK_INDICATOR_VOLTAGE() const; */
/*       double getPS_VOLTAGE_CLOCK() const; */
    
  private:
    // SecondLO specific monitor points.    
    std::vector<CAN::byte_t> CAN_GET_MODULE_STATUS() const;
    std::vector<CAN::byte_t> CAN_GET_COARSE_FREQ() const;
    std::vector<CAN::byte_t> CAN_GET_POWER_SUPPLY_1_VALUE() const;
    std::vector<CAN::byte_t> CAN_GET_POWER_SUPPLY_2_VALUE() const;
    std::vector<CAN::byte_t> CAN_GET_POWER_SUPPLY_3_VALUE() const;
    std::vector<CAN::byte_t> CAN_GET_POWER_SUPPLY_4_VALUE() const;
    std::vector<CAN::byte_t> CAN_GET_DETECTED_RF_POWER() const;
    std::vector<CAN::byte_t> CAN_GET_DETECTED_IF_POWER() const;
    std::vector<CAN::byte_t> CAN_GET_DETECTED_FTS_POWER() const;
    std::vector<CAN::byte_t> CAN_GET_FM_COIL_VOLTAGE() const;

    // Second LO specific control points.
    void CAN_SET_COARSE_FREQ(const std::vector<CAN::byte_t>& data);
    void CAN_SET_LOCK(const std::vector<CAN::byte_t>& data);
    void CAN_SET_RANGE(const std::vector<CAN::byte_t>& data);

    /// \name Undefined and inaccessible - copying does not make sense.
    //@{
    SecondLO (const SecondLO &);
    SecondLO &operator=(const SecondLO &);
    //@}

    static const unsigned char TUNE_POSITION = (1 << 0);
    static const unsigned char HIGH_POSITION = (1 << 1);
    static const unsigned char PLL_LOCKED = (1 << 2);

    unsigned char MODULE_STATUS_m;
    double POWER_SUPPLY_1_VALUE_m;
    double POWER_SUPPLY_2_VALUE_m;
    double POWER_SUPPLY_3_VALUE_m;
    double POWER_SUPPLY_4_VALUE_m;
    double DETECTED_RF_POWER_m;
    double FM_COIL_VOLTAGE_m;
    double COARSE_FREQ_m;

    // to avoid implementing a lookup table, just keep the raw "hardware"
    // values for IF and FTS power
    signed short DETECTED_IF_POWER_m;
    signed short DETECTED_FTS_POWER_m;

    // corresponding baseband
    short baseband_m;

  };  // class SecondLO
}  // namespace AMB

#endif  // SECOND_LO_H
