// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu
#if !defined(AMB_UTIL_H)
#define AMB_UTIL_H

#include <CANTypes.h>
#include <vector>
#include <string>
#include <netinet/in.h>

namespace AMB
    {
    using CAN::id_t;

    /** If the node number is valid, return true, otherwise return false and 
	put a helpful error message into error. */
    bool validateNodeNumber(
	std::string& error,
	node_t node);

    /** Pad SN to 8 to characters if necessary by appending binary 0's.
	@exception CAN::Error is serialNumber has more than 8 characters. */
    // TODO - Maybe it would be better to not throw an exception, but to leave
    // it unchanged on the assumption that validateSerialNumber will be 
    // called later.
    void padSerialNumber(
	std::vector<CAN::byte_t>& serialNumber);

    /** Return true if the serial number is valid, otherwise false and set 
	error to contain a helpful message. */
    bool validateSerialNumber(
	std::string& error, 
	const std::vector<CAN::byte_t>& serialNumber);

    /** Convert an AMB node number and RCA to a raw CAN ID as described in
	ALMA Computing Memo #7. */
    id_t toID(
	node_t node,
	rca_t rca);

    /** Convert a raw CAN ID to an AMB node number and RCA as described in
	ALMA Computing Memo #7. */
    void fromID(
	node_t& node,
	rca_t& rca,
	id_t id);

    /** Calculate the time from start to now (the present time).
	@param start time at which the "stopwatch" started. Timeval is defined
	in <sys/time.h>.
	@return the duration "now - start" in seconds. */ 
    double difftime(
	const timeval& start);

    /** Insert a double (8-byte) value into a CAN message
	@return floating point value packed into a CAN message. 
        @param value double value to be copied to the output. */
    std::vector<CAN::byte_t> doubleToData(
	double value);

    /** Insert a decimal value (up to 8-bytes) into the given CAN message.
        @return decimal value packed into given CAN message.
        @param dataIn is first copied to the output so a message can be built
	       up. That is, the output of a previous *toData function is
	       intended to be the input to the next *toData function.
        @param decimal value to be copied to the output.
        @param nbyte number of bytes to pack into CAN message. */
    std::vector<CAN::byte_t> doubleToData(
	const std::vector<CAN::byte_t>& dataIn,
    double value,
    int nbytes);

    /** Insert a float (4-byte) value into a CAN message
	@return floating point value packed into a CAN message. 
        @param value float value to be copied to the output. */
    std::vector<CAN::byte_t> floatToData(float value);

    /** Insert a floating-point value into the given CAN message as a scaled 
	integer.  The input double value is in "natural" units (e.g. volts), 
	and on return, the CAN message contains the value scaled to an integer 
	of the given number of bits.  Note, however, that 1, 2, or 4 bytes are 
	appended to the CAN message with the "extra" upper bits set to zero.  
	So, if nbits is 10 then 2 bytes would be returned with the top 6 bits 
	all zero.
	N.B. Only positive values and ranges are handled properly.
	@return value packed into given CAN message.
        @param dataIn is first copied to the output so a message can be built
	       up.  That is, the output of a previous *toData function is
	       intended to be the input to the next *toData function.
        @param value floating point value to be copied to the output.
        @param lowRange the low end of the range, used for scaling. That is,
	       if value == lowRange then all bits zero is returned.
        @param highRange the high end of the range. That is, if 
	       value == highRange then all bits one is returned.
	@param nbits number of output bits to use. */
    std::vector<CAN::byte_t> floatToData(
	const std::vector<CAN::byte_t>& dataIn, 
	double value,
	double lowRange,
	double highRange, 
	unsigned int nbits);

    /** Insert a Dallas Semiconductor 1820 temperature value into the given 
	CAN message.  The value occupies 4 CAN message bytes.
	@return value packed into given CAN message.
        @param dataIn is first copied to the output so a message can be built
	       up.  That is, the output of a previous *toData function is
	       intended to be the input to the next *toData function.
       @param value temperature to be place in CAN message.  Message format is 
               defined in the DS1820 data sheet, page 4. */
    std::vector<CAN::byte_t> floatDS1820ToData(
	const std::vector<CAN::byte_t>& dataIn, 
	double value);

    /** Insert an integer value (up to 8-bytes) into the given CAN message.
        @return integer value packed into given CAN message.
        @param dataIn is first copied to the output so a message can be built
	       up.  That is, the output of a previous *toData function is
	       intended to be the input to the next *toData function.
        @param integer value to be copied to the output.
        @param nbyte number of bytes to pack into CAN message. */
    std::vector<CAN::byte_t> longlongToData(
	const std::vector<CAN::byte_t>& dataIn,
	unsigned long long value,
	int nbyte);

    /** Insert a long integer (4-byte) value into the given CAN message.
	@return integer value packed into given CAN message.
        @param dataIn is first copied to the output so a message can be built
	       up.  That is, the output of a previous *toData function is
	       intended to be the input to the next *toData function.
        @param long (4-byte) value to be copied to the output.*/
    std::vector<CAN::byte_t> longToData(
	const std::vector<CAN::byte_t>& dataIn,
	long value);

    /** Insert a short integer (2-byte) value into the given CAN message.
	@return short value packed into given CAN message.
        @param dataIn is first copied to the output so a message can be built
	       up.  That is, the output of a previous *toData function is
	       intended to be the input to the next *toData function.
        @param short (2-byte) value to be copied to the output.*/
    std::vector<CAN::byte_t> shortToData(
	const std::vector<CAN::byte_t>& dataIn,
	short value);

    /** Insert a char integer (1-byte) value into the given CAN message.
	@return char value packed into given CAN message.
        @param dataIn is first copied to the output so a message can be built
	       up.  That is, the output of a previous *toData function is
	       intended to be the input to the next *toData function.
        @param value char (1-byte) to be copied to the output.*/
    std::vector<CAN::byte_t> charToData(
	const std::vector<CAN::byte_t>& dataIn, 
	unsigned char value);

    /** Remove double floating point (8-byte) value from the given CAN message.
	@return extracted double value.
        @param dataInOut contains the CAN message bytes. */
    double dataToDouble(
	std::vector<CAN::byte_t>& dataInOut);

    /** Remove and scale a floating point value from the given CAN message.   
	An integer value of the given number of bits is removed from the CAN 
	message, and a double value scaled to "natural" units (e.g. volts) is 
	returned.  Note however that 1, 2, or 4 bytes are taken out of 
	dataInOut.  So, if nbits is 10 then 2 bytes would be removed with the 
	top 6 bits ignored. 
	N.B. Only positive values and ranges are handled properly.
	@return extracted floating pointing value.
        @param dataInOut contains the CAN message bytes. The bytes from the 
	       floating point are removed from dataInOut but any additional 
	       bytes are left for extraction in another dataTo* function.
        @param lowRange the low end of the range, used for scaling. That is, if 
	       all bits are zero than lowRange is returned.
        @param highRange the high end of the range. That is, if 
	       all bits are set then highRange is returned.
        @param nbits number of bits in the message integer. */
    double dataToFloat(
	std::vector<CAN::byte_t>& dataInOut,
	double lowRange, 
	double highRange, 
	unsigned int nbits);

    /** Remove a floating point value from the given CAN message. The vector
	must be at least 4 bytes long and it must contain an IEEE floating
	point value. No scaling is done. After calling this the length of teh
	suppkied vector is reduced by 4.*/
    float dataToFloat(
	std::vector<CAN::byte_t>& dataInOut);

    /** Remove a Dallas Semiconductor 1820 temperature from the given CAN 
	message.
	@return extracted temperature according to DS1820 data sheet (p. 4).
        @param dataInOut contains the CAN message bytes.  The four bytes of 
	       the temperature are removed from dataInOut, but any additional 
	       bytes are left for extraction in another dataTo* function. */
    double dataToFloatDS1820(
	std::vector<CAN::byte_t>& dataInOut);

    /** Remove integer value (up to 8-bytes) from the given CAN message.
	@return extracted 8-byte unsigned long long integer.
        @param dataInOut contains the CAN message bytes. The bytes from the 
	       long long integer are removed from dataInOut but any additional 
	       bytes are left for extraction in another dataTo* function.
        @param nbyte number of bytes to remove from message. */
    unsigned long long dataToLonglong(
	std::vector<CAN::byte_t>& dataInOut, 
        const int nbyte);

    /** Remove long (4-byte) value from the given CAN message.
	@return extracted 4-byte long integer.
        @param dataInOut contains the CAN message bytes. The bytes from the 
	       long integer are removed from dataInOut but any additional 
	       bytes are left for extraction in another dataTo* function. */
    long dataToLong(
	std::vector<CAN::byte_t>& dataInOut);

    /** Remove short (2-byte) value from the given CAN message.
	@return extracted 2-byte short integer.
        @param dataInOut contains the CAN message bytes. The bytes from the 
	       short integer are removed from dataInOut but any additional 
	       bytes are left for extraction in another dataTo* function. */
    short dataToShort(
	std::vector<CAN::byte_t>& dataInOut);

    /** Remove char (1-byte) value from the given CAN message.
	@return extracted 1-byte char integer.
        @param dataInOut contains the CAN message bytes. The bytes from the 
	       char integer are removed from dataInOut but any additional 
	       bytes are left for extraction in another dataTo* function. */
    unsigned char dataToChar(
	std::vector<CAN::byte_t>& dataInOut);

} // namespace AMB

#endif  /* AMB_UTIL_H */
