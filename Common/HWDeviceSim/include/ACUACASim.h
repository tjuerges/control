// @(#) $Id$


#ifndef ACUACA_SIM_H
#define ACUACA_SIM_H

#include "AMBSimACUACAbase.h"

namespace ACAacu {

class ACUDrive;

struct ACUSimStatus
{
    int errcode;
    uint64_t timestamp;
};

/**
 * A concrete class that implements a CAN bus simulator for the ACA ACU 
 * computer.  This class contains all interface routines to access the ACU
 * simulator through the CAN bus.
 *
 * This class is derived from the ACU class (see AMBSimACU.h) to be the
 * equivalent of the ACUhw class (ticsAntVA module) that is also derived from
 * the ACU class.  This class accesses the ACU simulator, while the ACUhw
 * class accesses the ACU computer hardware.
 *
 * For a discussion of this design, see ticsAntVA/lcu/include/ACUhw.h.
 */
class ACUSim
{
  public:

    ACUSim();
    ~ACUSim();

    //
    // Monitor Points
    //
    std::vector<uint8_t> get_acu_mode_rsp() const;
    uint8_t acu_trk_mode_rsp() const;

    void get_acu_error(uint8_t &error, uint32_t &rca) const;

    void get_az_posn_rsp(int32_t&, int32_t&) const;
    void get_el_posn_rsp(int32_t&, int32_t&) const;
    uint32_t get_az_enc() const;
    uint32_t get_el_enc() const;

    void get_az_traj_cmd(int32_t&, int32_t&) const;
    void get_el_traj_cmd(int32_t&, int32_t&) const;
    void get_az_motor_currents(az_motor_st&) const;
    void get_el_motor_currents(el_motor_st&) const;
    void get_az_motor_temps(az_motor_st&) const;
    void get_el_motor_temps(el_motor_st&) const;
    void get_az_motor_torque(az_motor_st&) const;
    void get_el_motor_torque(el_motor_st&) const;

    uint8_t get_az_brake() const;
    uint8_t get_el_brake() const;
    double get_az_servo_coeff(const int) const;
    double get_el_servo_coeff(const int) const;
    std::vector<uint8_t> get_az_status() const;
    std::vector<uint8_t> get_az_status_2() const;
    std::vector<uint8_t> get_el_status() const;
    std::vector<uint8_t> get_el_status_2() const;
    uint32_t get_az_encoder_offset() const;
    uint32_t get_el_encoder_offset() const;
    uint8_t get_az_aux() const;
    uint8_t get_el_aux() const;
    uint8_t get_az_ratefdbk_mode() const;
    uint8_t get_el_ratefdbk_mode() const;

    uint8_t get_shutter() const;
    void get_stow_pin(uint8_t&az, uint8_t&el) const;
    double get_pt_model_coeff(const int) const;
    std::vector<uint8_t> get_system_status() const;
    std::vector<uint8_t> get_system_status_2() const;

    uint8_t subref_mode_rsp() const;
    std::vector<int16_t> get_subref_abs_posn() const;
    std::vector<int16_t> get_subref_delta_posn() const;
    std::vector<int16_t> get_subref_rotation() const;
    std::vector<int16_t> get_sr_encoder_posn_1() const;
    std::vector<int16_t> get_sr_encoder_posn_2() const;
    std::vector<uint8_t> get_subref_limits() const;
    std::vector<uint8_t> get_subref_limits_2() const;
    std::vector<uint8_t> get_subref_status() const;
    double get_sr_pt_coeff(const int) const;
    double get_sr_servo_coeff(const int) const;

    std::vector<uint8_t> get_metr_mode() const;
    std::vector<uint8_t> get_metr_equip_status() const;
    double get_metr_coeff(const int) const;
    int32_t get_metr_displ(const int) const;
    std::vector<int16_t> get_metr_temps(const int) const;
    void get_metr_deltas(int32_t &, int32_t &) const;
    int32_t get_metr_deltapath() const;
    void get_metr_tilt(const int, metr_tilt &) const;

    void get_ip_address(uint32_t&,uint32_t&) const;
    void get_ip_gateway(uint32_t&) const;
    uint16_t get_idle_stow_time() const;

    std::vector<uint8_t> get_power_status() const;
    uint8_t get_ac_status() const;
    std::vector<uint8_t> get_fan_status() const;
    std::vector<uint16_t> get_ups_output_volts_1() const;
    std::vector<uint16_t> get_ups_output_volts_2() const;
    std::vector<uint16_t> get_ups_output_current_1() const;
    std::vector<uint16_t> get_ups_output_current_2() const;
    void get_antenna_temps(antenna_temps&) const;
    uint8_t get_ac_temp() const;
    void selftest_rsp(selftest_rsp_param &) const;
    void selftest_err(selftest_err_param &) const;

    //
    // Control Points
    //
    void reset_acu_cmd();
    void acu_mode_cmd(const uint8_t);
    void acu_trk_mode_cmd(const uint8_t);
    void clear_fault_cmd();
    void az_traj_cmd(const int32_t, const int32_t);
    void el_traj_cmd(const int32_t, const int32_t);

    void set_az_brake(int);
    void set_el_brake(int);

    void set_az_servo_coeff(const int, const double);
    void set_el_servo_coeff(const int, const double);
    void set_az_servo_default(); 
    void set_el_servo_default();
    void set_az_aux(uint8_t);
    void set_el_aux(uint8_t);
    void set_az_ratefdbk_mode(uint8_t);
    void set_el_ratefdbk_mode(uint8_t);

    void set_shutter(const uint8_t);
    void set_stow_pin(const uint8_t az, const uint8_t el);
    void set_pt_model_coeff(const int, const double);
    void set_pt_default();

    void subref_mode_cmd(const uint8_t);
    void set_subref_abs_posn(const std::vector<int16_t>);
    void set_subref_delta_posn(const std::vector<int16_t>);
    void set_subref_rotation(const std::vector<int16_t>);
    void subref_delta_zero_cmd(const uint8_t);
    void set_sr_pt_coeff(const int, const double);
    void set_sr_pt_default();
    void set_sr_servo_coeff(const int, const double);
    void set_sr_servo_default();

    void set_metr_mode(const std::vector<uint8_t>);
    void set_metr_coeff(const int, const double);
    void set_metr_default();
    void set_metr_calibration();

    void set_ip_address(const uint32_t, const uint32_t);
    void set_ip_gateway(const uint32_t);
    void set_idle_stow_time(const uint16_t);
    void set_ac_temp(uint8_t);
    void selftest_cmd(uint8_t);


    //
    // Generic monitor points
    //
    std::vector<uint8_t> get_sw_rev_level() const;
    uint32_t get_can_error() const;
    uint32_t get_num_trans() const;
    std::vector<int8_t> get_system_id() const;

    void set_errcode(int) const;
    int  get_errcode() const;
    void set_timestamp() const;
    uint64_t get_timestamp() const;

  private:

    ACUSimStatus *status_m;

    mutable unsigned int trans_num_m;  // number of transactions

    ACUDrive* drive_m;

    uint8_t acu_mode_m;   // LOCAL or REMOTE, defaults to REMOTE
    uint8_t az_mode_m; // defaults to STANDBY
    uint8_t el_mode_m;
    uint8_t acu_trk_mode_m;

    int32_t az_traj_cmd_m[2];
    int32_t el_traj_cmd_m[2];

    uint32_t az_enc_m;     // az, el encoder member values
    uint32_t el_enc_m;
    uint32_t az_encoder_offset_m;
    uint32_t el_encoder_offset_m;
    uint8_t az_aux_m;
    uint8_t el_aux_m;
    uint8_t az_ratefdbk_mode_m;
    uint8_t el_ratefdbk_mode_m;

    uint16_t idle_stow_time_m;  // default to 5 minutes.
    uint8_t shutter_m;	// 0- shutter closed, 1- open

    double pt_model_coeff_m[32]; // 32 values for set/get

    uint8_t subref_mode_m;
    int16_t subref_abs_posn_m[3];
    int16_t subref_delta_posn_m[3];
    int16_t subref_rotation_m[3];
    int16_t subref_encoder_posn_m[6];
    double subref_pt_coeff_m[32];    // 32 values for set/get

    uint8_t metr_mode_m[4];  // defaults to ready, disabled 
    uint8_t metr_equip_status_m[8];
    double metr_coeff_m[32];    // 32 values for set/get
    int8_t metr_displ_m[24];
    int8_t metr_temps_m[320];
    int32_t metr_deltapath_m;
    antenna_temps antenna_temps_m;
    metr_tilt metr_tilt_m[5];

    uint32_t ip_address;
    uint32_t ip_netmask;
    uint32_t ip_gateway;
    uint8_t ac_temperature_m;

    // Return current time in ACS format (uint64_t in 100 nsec).
//    uint64_t getTimeStamp() const;

    // undefined and inaccessible
    ACUSim(const ACUSim&);
    ACUSim& operator = (const ACUSim&);

};  // class ACUSim

}; // namespace ACAacu
#endif // ACUACA_SIM_H
