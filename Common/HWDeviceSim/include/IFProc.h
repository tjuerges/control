// @(#) $Id$
//
// Copyright (C) 2002, 2004
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#if !defined(IFPROC_H)
#define IFPROC_H

//#define WRITE_LOG


#include "AMBDevice.h"
#include "CANError.h"
#include <string>
#include <stdio.h>
#include <sys/time.h>

namespace AMB
{
  /** The IFPRoc class is a simulator for the IFProcessor and TP detector
   */
  class IFProc : public AMB::Device
    {
    public:
      /** Constructor
       ** @param node Node ID of this device
       ** @param serialNumber S/N of this device
       ** @param pol Polarization (there's one IFProc device per polarization)
       */
      IFProc(node_t node, const std::vector<CAN::byte_t>& serialNumber, short pol);

      // Destructor
      virtual ~IFProc();

      // Return the device's node ID
      virtual node_t node() const;

      // Return the device's S/N
      virtual std::vector<CAN::byte_t> serialNumber() const;

      /** Monitor interface
       ** @param CAN node RCA
       ** @return std::vector of bytes containing monitor info. Each response
       ** is sized to what's specified in the ICD.
       */
      virtual std::vector<CAN::byte_t> monitor(rca_t rca) const;

      /** Control Interface ('set' functions)
       ** @param rca CAN node RCA
       ** @param data CAN message data required by device
       */
      virtual void control(rca_t rca, const std::vector<CAN::byte_t>& data);

      /// Monitor points for the LORR
      enum Monitor_t {

        /// Generic monitor points
        GET_SERIAL_NUMBER       = 0x00000,
        GET_CAN_ERROR           = AMB::Device::GET_CAN_ERROR,
        GET_PROTOCOL_REV_LEVEL  = AMB::Device::GET_PROTOCOL_REV_LEVEL,
        GET_TRANS_NUM           = AMB::Device::GET_TRANS_NUM,
        GET_AMBIENT_TEMPERATURE = AMB::Device::GET_AMBIENT_TEMPERATURE,

        /// Specific IFProc data point monitors
        READ_STATUS             = 0x00001,
        READ_TIMING_ERROR_FLAG  = 0x00002,
        READ_DATA_MONITOR_1     = 0x00003,
        READ_DATA_MONITOR_2     = 0x00004,
        READ_FIFO_DEPTHS        = 0x00005,
        READ_DATA_REMAP         = 0x00006,
        READ_MODULE_IP_ADDR     = 0x00007,
        READ_DEST_IP_ADDR       = 0x00008,
        READ_ETHERNET_MAC       = 0x00009,
        READ_TCP_PORT           = 0x0000A,
        READ_BDB_PER_PACKET     = 0x0000B,
        READ_ALMA_TIME          = 0x0000C,
        READ_48MS_LENGTH        = 0x0000D,
        READ_ETHERNET_TIMES     = 0x0000E,
        READ_AVERAGE_LENGTH     = 0x0000F,
        READ_IFDC_SPI_STATUS    = 0x00010,
        READ_VOLTAGES_1         = 0x00011,
        READ_VOLTAGES_2         = 0x00012,
        READ_AMB_CMD_COUNTER    = 0x00013,
        READ_AMB_INVALID_CMD    = 0x00014,
        READ_SPI_ERRORS         = 0x00015,
        GATEWAY_IP_ADDR		= 0x00016,
        NETMASK			= 0x00017,
        READ_IFTP_MODULE_CODES  = 0x00079,
        READ_GAINS              = 0x00101,
        READ_SIGNAL_PATHS       = 0x00103,
        READ_TEMPS_1            = 0x00104,
        READ_TEMPS_2            = 0x00105,
        READ_TEMPS_3            = 0x00106,
        READ_10V_CURRENTS       = 0x00107,
        READ_65V_CURRENTS       = 0x00108,
        READ_8V_CURRENTS        = 0x00109,
        READ_IFDC_MODULE_CODES  = 0x00179
      };

      /// Control Functions for the IFProc
    enum Controls_t {
      RESET_TPD_BOARD       = 0x00081,
      SET_DATA_REMAP        = 0x00082,
      SET_MODULE_IP_ADDR    = 0x00083,
      SET_DEST_IP_ADDR      = 0x00084,
      SET_TCP_PORT          = 0x00085,
      SET_BDB_PER_PACKET    = 0x00086,
      SET_ALMA_TIME         = 0x00087,
      INIT_TCP_CONN         = 0x00088,
      START_DATA            = 0x00089,
      STOP_DATA             = 0x0008A,
      START_COMM_TEST       = 0x0008B,
      CLEAR_TIMING_ERROR    = 0x0008C,
      SET_AVERAGE_LENGTH    = 0x0008D,
      RESET_ETHERNET_TIMES  = 0x0008E,
      RESET_AMB_INVALIED_CMD= 0x0008F,
      RESET_AMB_CMD_COUNTER = 0x00090,
      SET_GATEWAY_IP_ADDR   = 0x00093,
      SET_NETMASK           = 0x00094,
      SET_GAINS             = 0x00181,
      //SET_SB_PATHS          = 0x00182
      SET_SIGNAl_PATHS      = 0x00182
    };

      // Throw this if something bad happens.
      class IFProcError : public CAN::Error {
      public:
        IFProcError(const std::string &error) : CAN::Error(error) {}
      };

    private:
      // Reset all values to nominal
      void resetIFProc();

      // Take the default implementation = return 0,0,0,0
      virtual std::vector<CAN::byte_t>  get_can_error() const;

      // Take the default implementation = return 0,0,0
      virtual std::vector<CAN::byte_t>  get_protocol_rev_level() const;

      // Return the number of transactions
      virtual unsigned int get_trans_num() const;

      // Temperature on M&C interface board.
      double get_ambient_temperature() const;

      /** Helper function for 'set' commands to check that data size
       ** is valid, if not throw exception
       ** @param data Vector of bytes containing data to check
       ** @param size Expected number of bytes in data std::vector
       ** @param pExceptionMsg String identifying CAN msg that is
       ** associated with failed size check.
       */
      void checkSize(const std::vector<CAN::byte_t> &data, int size,
             const char *pExceptionMsg);

      // \name Undefined and inaccessible - copying does not make sense.
      //@{
      IFProc (const IFProc &);
      IFProc &operator=(const IFProc &);
      //@}

      // Bus location (node number).
      node_t m_node;

      // Serial number of this node.
      std::vector<CAN::byte_t> m_sn;

      // Number of transactions. Needs to be mutable as its incremented by the
      // monitor function, which is const.
      unsigned int m_trans_num;

      // values for all the monitor points
      CAN::byte_t m_errorCode;


      std::vector<CAN::byte_t> dataMap_m;   // Data_REMAP
      std::vector<CAN::byte_t> moduleIP_m;  // Module IP Address
      std::vector<CAN::byte_t> destIP_m;    // Destination IP Address
      std::vector<CAN::byte_t> gatewayIP_m; // Module IP Address
      std::vector<CAN::byte_t> netmask_m;   // Module IP Address
      std::vector<CAN::byte_t> deviceMAC_m; // MAC Address of device
      std::vector<CAN::byte_t> status_m;    // Current status
      std::vector<CAN::byte_t> invalidCmd_m;
      unsigned short           tcpPort_m;  // TCP Port to connect to
      unsigned char            bdbPerPacket_m; //BDB per IP Packet

      struct
      {
        unsigned long long almaTime;
        struct timeval   tv;
      } timeRef_m;

      unsigned char    dataAvgLen_m; // Average Data length
      unsigned char    signalPaths_m;


      unsigned short ambCmdCounter_m;
      short polarization_m;

      float    gain_m[4];
      char timingError_m;

      unsigned long fifoReadTime_m; // Time it took to read the fifo
      unsigned long ipTrxTime_m;    // Time it took to transmit the data

    };  // class IFProc
}  // namespace AMB

#endif  // IFProc_H

