// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#ifndef ACUDRIVE_INCLUDE_H
#define ACUDRIVE_INCLUDE_H

#include "ACUAxis.h"
#include <SharedSimInterface.h>

// definitions that are configurable
#define AZ_LOWER_LIMIT  -270.
#define AZ_UPPER_LIMIT   270.
#define EL_LOWER_LIMIT   -20.
#define EL_UPPER_LIMIT   120.
#define AZ_MAINT_STOW     90.
#define AZ_SURVL_STOW     90.
#define EL_MAINT_STOW     90.
#define EL_SURVL_STOW     15.

/*
 * ACUDrive Class
 */
class ACUDrive
{
  public:

    // two public axes
    ACUAxis Az;
    ACUAxis El;

    ACUDrive();

    ~ACUDrive();

    int setAzPsn(
	double position,
	double velocity);

    int setElPsn(
	double position,
	double velocity);

    void getAzPsn(
	double& position,
	double& velocity) const;

    void getElPsn(
	double& position,
	double& velocity) const;

    void getAzStatus(
	unsigned long long& status) const;

    void getElStatus(
	unsigned long long& status) const;

#if 0  /* DISCARD!! */
    int setAzMode(
	AZEL_Mode_t m);

    int setElMode(
	AZEL_Mode_t m);

    void getAzMode(
	AZEL_Mode_t& m) const;

    void getElMode(
	AZEL_Mode_t& m) const;
#endif

    std::vector<char> getACUMode() const;

    int setACUMode(
	unsigned char mode);

    void setSharedSimulator(SharedSimInterface* ssim) {sharedSim_p = ssim;}

  protected:
    // ACU clock tick handler
    void handler();

    // updateLoop - thread to run the ACU simulator timer loop
    static void* updateLoop(void* data);


    void sharedSimHandler();
    static void* sharedSimUpdateLoop(void* data);

    bool active_m;
    bool ssActive_m;

  private:
    pthread_t tid;
    pthread_t sharedSimThreadId;

    /** pointer to TELCAL SharedSimulator */
    SharedSimInterface* sharedSim_p;
    
    int ticCount;

};

#endif /* ACUDRIVE_INCLUDE_H */
