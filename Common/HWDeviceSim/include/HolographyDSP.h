// @(#) $Id$
//
// Copyright (C) 2002
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#if !defined(HOLOGRAPHY_DSP_H)
#define HOLOGRAPHY_DSP_H

#include "AMBDevice.h"
#include "CANError.h"
#include <string>

namespace AMB
{
    /** The HolographyDSP class is a simulator for the node 1, the DSP module of
     ** the Holography Measurement Equipment device. 
     ** There are 2 nodes to this device, one to obtain the integrated holography
     ** data -- represented by this class & node 2 which is the M&C for the
     ** holography receiver & covered by the class HolographyReceiver.
     ** See test/testHolography.cpp or src/AMBSimulator.cpp for usage guidance.
     */
    class HolographyDSP : public AMB::Device
    {
    public:
	/** Constructor
	 ** @param node Node ID of this device
	 ** @param serialNumber S/N of this device
	 */
	HolographyDSP(node_t node, const std::vector<CAN::byte_t> &serialNumber);
	/// Destructor
	virtual ~HolographyDSP();

	/// Return the device's node ID
	virtual node_t node() const;
	/// Return the device's S/N
	virtual std::vector<CAN::byte_t> serialNumber() const;

	/** Monitor interface
	 ** @param CAN node RCA
	 ** @return std::vector of bytes containing monitor info. Each response is
	 ** sized to what's specified in the ICD.
	 */
	virtual std::vector<CAN::byte_t> monitor(rca_t rca) const;

	/** Control Interface ('set' functions)
	 ** @param rca CAN node RCA
	 ** @param data CAN message data required by device
	 */
	virtual void control(rca_t rca, const std::vector<CAN::byte_t> &data);

	// Throw this if something bad happens.
	class HolographyDSPError : public CAN::Error {
	public:
	    HolographyDSPError(const std::string &error) : CAN::Error(error) {;}
	};

	/// Monitor points for the Holography DSP module
	enum Monitor_t {
	    /// Generic monitor points
	    //@{
	    GET_CAN_ERROR           = AMB::Device::GET_CAN_ERROR,
	    GET_PROTOCOL_REV_LEVEL  = AMB::Device::GET_PROTOCOL_REV_LEVEL,
	    GET_TRANS_NUM           = AMB::Device::GET_TRANS_NUM,
	    GET_AMBIENT_TEMPERATURE = AMB::Device::GET_AMBIENT_TEMPERATURE,
	    //@}

	    /// Specific Holography DSP data point monitors
	    //@{
	    GET_DATA_QQ_0           = 0x00000100,
	    GET_DATA_QR_0           = 0x00000101,
	    GET_DATA_QS_0           = 0x00000102,
	    GET_DATA_RR_0           = 0x00000103,
	    GET_DATA_RS_0           = 0x00000104,
	    GET_DATA_SS_0           = 0x00000105,

	    GET_DATA_QQ_1           = 0x00000106,
	    GET_DATA_QR_1           = 0x00000107,
	    GET_DATA_QS_1           = 0x00000108,
	    GET_DATA_RR_1           = 0x00000109,
	    GET_DATA_RS_1           = 0x0000010A,
	    GET_DATA_SS_1           = 0x0000010B,

	    GET_DATA_QQ_2           = 0x0000010C,
	    GET_DATA_QR_2           = 0x0000010D,
	    GET_DATA_QS_2           = 0x0000010E,
	    GET_DATA_RR_2           = 0x0000010F,
	    GET_DATA_RS_2           = 0x00000110,
	    GET_DATA_SS_2           = 0x00000111,

	    GET_DATA_QQ_3           = 0x00000112,
	    GET_DATA_QR_3           = 0x00000113,
	    GET_DATA_QS_3           = 0x00000114,
	    GET_DATA_RR_3           = 0x00000115,
	    GET_DATA_RS_3           = 0x00000116,
	    GET_DATA_SS_3           = 0x00000117,
	    //@}

	    /// Get the number of samples
	    GET_NUM_SAMPLES         = 0x00000118,
	    GET_BAD_FRAMES_COUNT    = 0x00000119
	};

	/// Only one control for the DSP -- reset
	enum Controls_t {
	    RESET_DSP               = 0x00001001
	};

	/// Number of samples for each 12ms integration interval
	const int NUM_SAMPLES_PER_INTERVAL;

    private:
	/// Take the default implementation = return 0,0,0,0
	virtual std::vector<CAN::byte_t>  get_can_error() const;

	/// Take the default implementation = return 0,0,0
	virtual std::vector<CAN::byte_t>  get_protocol_rev_level() const;

	/// Return the number of transactions
	virtual unsigned int get_trans_num() const;

	/// Temperature on M&C interface board. 18.0 constant.
	float get_ambient_temperature() const;

	// HolographyDSP Measurement Equipment specific monitor points.

	/// Enums for the  6 data products 
	enum dataProduct_t
	{
	    dataProduct_QQ = 0,
	    dataProduct_QR,
	    dataProduct_QS,
	    dataProduct_RR,
	    dataProduct_RS,
	    dataProduct_SS
	};

	/** Get a data point given a data product (QQ, QR, QS, RR, RS, SS)
	 ** and a 12ms interval (1-4)
	 ** These are a sequence of digits 1 - 2^40 (1,099,511,627,776) which
	 ** increment for each data product & each time interval : 
	 ** <PRE>
	 **    QQ  QR  QS  RR  RS  SS 
	 **     1   2   3   4   5   6   (time interval 1)
	 **     7   8   9  10  11  12   (time interval 2)
	 **   ...
	 ** </PRE>
	 ** @param dp Data product of interest
	 ** @return 6-byte CAN msg containing the next data product in the 
	 ** sequence.
	 */
	std::vector<CAN::byte_t> getDataPoint( dataProduct_t dp) const;

	/** Return the number of samples in each interval. Each value is 
	 ** equal to NUM_SAMPLES_PER_INTERVAL
	 ** @return sampleVector contains samples for 4 intervals
	 */
	std::vector<short> getNumberSamples() const;

	/** Returns the number of bad frames
	 ** @return always zero
	 */
        short getBadFramesCount() const;

	// Controls
	/** Send reset pulse to digital signal processor
	 ** @param data CAN message as a std::vector<bytes>
	 ** @param size Expected number of bytes for the CAN msg
	 ** @param pMsg defines an identifier to be printed on error.
	 */
	void resetDSP(const std::vector<CAN::byte_t> &data, int size,
		      const char *pMsg);

	/** Helper function for 'set' commands to check that data size
	 ** is valid, if not throw exception
	 ** @param data Vector of bytes containing data to check
	 ** @param size Expected number of bytes in data std::vector
	 ** @param pExceptionMsg String identifying CAN msg that is
	 ** associated with failed size check.
	 */
	void checkSize( const std::vector<CAN::byte_t> &data, int size,
			const char *pExceptionMsg);

	/// \name Undefined and inaccessible - copying does not make sense.
	//@{
	HolographyDSP (const HolographyDSP &);
	HolographyDSP &operator=(const HolographyDSP &);
	//@}

	/// Bus location (node number).
	node_t m_node;
	/// Serial number of this node.
	std::vector<CAN::byte_t> m_sn;

	/// Number of transactions.
	mutable unsigned int m_trans_num;

	/// These are used to generate data points in sequential order.
	//@{
	/// current data product value
	mutable long long m_dataProductBaseValue;

	/// current 12ms interval (0-3)
	mutable int m_interval;
	//@}
    };  // class HolographyDSP
}  // namespace AMB

#endif  // HOLOGRAPHY_DSP_H
