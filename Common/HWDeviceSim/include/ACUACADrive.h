// @(#) $Id$


#ifndef ACUACA_DRIVE_H
#define ACUACA_DRIVE_H

#include "ACUACAAxis.h"

// definitions that are configurable
#define AZ_LOWER_LIMIT  -270.
#define AZ_UPPER_LIMIT   270.
#define EL_LOWER_LIMIT   -20.
#define EL_UPPER_LIMIT   120.
#define AZ_MAINT_STOW     90.
#define AZ_SURVL_STOW     90.
#define EL_MAINT_STOW     90.
#define EL_SURVL_STOW     15.

namespace ACAacu {

/*
 * ACUDrive Class
 */
class ACUDrive
{
  public:

    // two public axes
    ACUAxis Az;
    ACUAxis El;

    ACUDrive();

    ~ACUDrive();

    int setAzPsn(
	double position,
	double velocity);

    int setElPsn(
	double position,
	double velocity);

    void getAzPsn(
	double& position,
	double& velocity) const;

    void getElPsn(
	double& position,
	double& velocity) const;

    std::vector<uint8_t> getAzStatus() const;
    std::vector<uint8_t> getAzStatus2() const;
    std::vector<uint8_t> getElStatus() const;
    std::vector<uint8_t> getElStatus2() const;

    std::vector<uint8_t> getACUMode() const;

    int setACUMode(
	unsigned char mode);

  protected:
    // ACU clock tick handler
    void handler();

    // updateLoop - thread to run the ACU simulator timer loop
    static void* updateLoop(void* data);
    bool active_m;

  private:
    pthread_t tid;
};

}; // namespace ACAacu
#endif /* ACUACA_DRIVE_H */
