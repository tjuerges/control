#if !defined(ECHO_DEVICE_H)
#define ECHO_DEVICE_H

// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include <map>
#include "AMBDevice.h"

namespace AMB {

    class EchoDevice : public AMB::Device
    {
    public:
	EchoDevice(node_t node, const std::vector<CAN::byte_t> &serialNumber);
	~EchoDevice();

	// For space reasons (controls, monitors) keep the max RCA low
	// rather than using ~ 2^18*16 = 4MB or more of memory.
	enum {MAX_RCA = 1024};

	virtual node_t node() const;
	virtual std::vector<CAN::byte_t> serialNumber() const;

	virtual std::vector<CAN::byte_t> monitor(rca_t rca) const;
	virtual void control(rca_t rca, const std::vector<CAN::byte_t> &data);

	/* Generic monitor point. Default implementation returns 0,0,0,0. and
           increases the transaction count */
	virtual std::vector<CAN::byte_t>  get_can_error() const;
	/** Generic monitor point. Default implementation returns 0,0,0 and
            increases the transaction count. */
	virtual std::vector<CAN::byte_t>  get_protocol_rev_level() const;
	/** Generic monitor point. No implementation is provided since it
	    is easier to just increment a counter in the derived class
	    monitor() and control() functions. */
	virtual unsigned int get_trans_num() const;
	

    private:
	// Undefined and inaccessible
	EchoDevice(const EchoDevice &);
	EchoDevice& operator=(const EchoDevice &);

	node_t node_m;
	std::vector<CAN::byte_t> serialNumber_m;

	// mutable so that [] will work in monitor.
	mutable std::map<rca_t, std::vector<CAN::byte_t> > commanded_values_m;

	mutable unsigned int trans_num_m;
    };

} // namespace AMB

#endif
