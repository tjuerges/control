// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#if !defined(ACU_SIM_H)
#define ACU_SIM_H

#include "AMBSimACUbase.h"
#include <SharedSimInterface.h>

class ACUDrive;

/**
 * A concrete class that implements a CAN bus simulator for the Vertex ACU 
 * computer.  This class contains all interface routines to access the ACU
 * simulator through the CAN bus.
 *
 * This class is derived from the ACU class (see AMBSimACU.h) to be the
 * equivalent of the ACUhw class (ticsAntVA module) that is also derived from
 * the ACU class.  This class accesses the ACU simulator, while the ACUhw
 * class accesses the ACU computer hardware.
 *
 * For a discussion of this design, see ticsAntVA/lcu/include/ACUhw.h.
 */
class ACUSim : public ACUbase
{
  public:

    ACUSim();
    ~ACUSim();

    //
    // Monitor Points
    //
    std::vector<char> get_acu_mode_rsp(int&,unsigned long long&) const;

    unsigned long long get_acu_error(int&,unsigned long long&) const;

    unsigned long long get_az_posn_rsp(int&,unsigned long long&) const;
    unsigned long long get_el_posn_rsp(int&,unsigned long long&) const;
#if 0  /* !!! not used !!! */
    vector <double> get_az_posn_rsp(int&,double&) const;
    vector <double> get_el_posn_rsp(int&,double&) const;
#endif /* not used */

    long get_az_enc(int&,unsigned long long&) const;
    void get_el_enc(std::vector<long>&,int&,unsigned long long&) const;
#if 0  /* !!! not used !!! */
    double get_az_enc(int&,double&) const;
    double get_el_enc(int&,double&) const;
#endif /* not used */

    unsigned char get_az_enc_status(int&,unsigned long long&) const;
    std::vector<char> get_el_enc_status(int&,unsigned long long&) const;

    unsigned char get_az_aux_mode(int&,unsigned long long&) const;
    unsigned char get_el_aux_mode(int&,unsigned long long&) const;

    std::vector<unsigned char> get_az_motor_currents(int&,unsigned long long&) const;
    std::vector<unsigned char> get_az_motor_temps(int&,unsigned long long&) const;
    unsigned long get_az_motor_torques(int&,unsigned long long&) const;

    unsigned long get_el_motor_currents(int&,unsigned long long&) const;
    unsigned long get_el_motor_temps(int&,unsigned long long&) const;
    unsigned long get_el_motor_torques(int&,unsigned long long&) const;

    unsigned char get_az_brake(int&,unsigned long long&) const;
    unsigned char get_el_brake(int&,unsigned long long&) const;

    double get_az_servo_coeff(long n,int&,unsigned long long&) const;
    double get_el_servo_coeff(long n,int&,unsigned long long&) const;

    unsigned long long get_az_status(int&,unsigned long long&) const;
    unsigned long long get_el_status(int&,unsigned long long&) const;

    unsigned short get_idle_stow_time(int&,unsigned long long&) const;

    unsigned char get_shutter(int&,unsigned long long&) const;

    unsigned short get_stow_pin(int&,unsigned long long&) const;

    unsigned long get_system_status(int&,unsigned long long&) const;

    unsigned long get_ip_address(int&,unsigned long long int&) const;

    //
    // Control Points
    //
    void sim_system_status(const unsigned long,int&,unsigned long long&);

    void reset_acu_cmd(int&,unsigned long long&);

    void acu_mode_cmd(const unsigned char,int&,unsigned long long&);

    void clear_fault_cmd(int&,unsigned long long&);

    void az_traj_cmd(const long,const long,int&,unsigned long long&);
    void el_traj_cmd(const long,const long,int&,unsigned long long&);
#if 0  /* !!! not used !!! */
    void az_traj_cmd(const double,double,int&,double&);
    void el_traj_cmd(const double,double,int&,double&);
#endif /* not used */

    void set_az_aux_mode(const unsigned char,int&,unsigned long long&);
    void set_el_aux_mode(const unsigned char,int&,unsigned long long&);

    void set_az_brake(const unsigned char,int&,unsigned long long&);
    void set_el_brake(const unsigned char,int&,unsigned long long&);

    void set_az_servo_coeff(const long,const double,int&,unsigned long long&);
    void set_el_servo_coeff(const long,const double,int&,unsigned long long&);
    void set_az_servo_default(int&,unsigned long long&); 
    void set_el_servo_default(int&,unsigned long long&);

    void set_idle_stow_time(const unsigned short,int&,unsigned long long&);
    void set_shutter(const unsigned char,int&,unsigned long long&);

    void set_stow_pin(const unsigned short,int&,unsigned long long&);
    
    //
    // Generic monitor points
    //
    virtual std::vector<char> get_sw_rev_level(
	int& errcode,
	unsigned long long& timestamp) const;
    virtual unsigned long get_can_error(
	int& errcode,
	unsigned long long& timestamp) const;
    virtual unsigned long get_num_trans(
	int& errcode,
	unsigned long long& timestamp) const;
    std::vector<char> get_system_id(
	int& errcode,
	unsigned long long& timestamp) const;

    // Set the ACUDrive SharedSimulator
    void setSharedSimulator(SharedSimInterface* ssim);

  private:

    unsigned long system_status_m;

    ACUDrive* drive_m;
    
    long az_enc_m;     // az, el encoder member values
    long el_enc_m;
    unsigned char az_mode_m; // defaults to STANDBY
    unsigned char el_mode_m;
    unsigned char acu_mode_m;   // LOCAL or REMOTE, defaults to REMOTE
    double az_servo_coeff_n[8]; // 8 values for set/get
    double el_servo_coeff_n[8]; // 8 values for set/get
    unsigned short idle_stow_time_m;  // default to 5 minutes.
    unsigned char shutter_m;	// 0- shutter closed, 1- open, 2- handcrank
    mutable unsigned int trans_num_m;  // number of transactions
    
    // Return current time in ACS format (unsigned long long in 100 nsec).
    unsigned long long getTimeStamp() const;

    // undefined and inaccessible
    ACUSim(const ACUSim&);
    ACUSim& operator = (const ACUSim&);


};  // class ACUSim

#endif // ACU_SIM_H
