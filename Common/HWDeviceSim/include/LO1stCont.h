/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2002 
*
* This code is under GNU General Public Licence (GPL). 
* Correspondence concerning ALMA Software should be addressed to:
* alma-sw-admin@nrao.edu
*
* Copyright (C) 2001
* Associated Universities, Inc. Washington DC, USA.
*
* Produced for the ALMA project
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Library General Public License
* as published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Library General Public License for more details.
*
* You should have received a copy of the GNU Library General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* aperrigo  08/12/02  created 
*/

#if !defined(LO1stCont_H)
#define LO1stCont_H

#include "AMBDevice.h"
#include "CANError.h"
#include <string>

namespace AMB {

    class LO1stCont : public AMB::Device
    {
    public:
	LO1stCont(node_t node, const std::vector<CAN::byte_t> &serialNumber);
	virtual ~LO1stCont();

	virtual node_t node() const;
	virtual std::vector<CAN::byte_t> serialNumber() const;

	virtual std::vector<CAN::byte_t> monitor(rca_t rca) const;
	virtual void control(rca_t rca, const std::vector<CAN::byte_t> &data);

	// CAN Generic MC points.

	/// Take the default implementation = return 0,0,0,0.
	virtual std::vector<CAN::byte_t>  get_can_error() const;

	/// Take the default implementation = return 0,0,0.
	virtual std::vector<CAN::byte_t>  get_protocol_rev_level() const;

	/// Return the number of transactions.
	virtual unsigned int get_trans_num() const;

	/// Temperature on M&C interface board. 18.0 constant.
	float get_ambient_temperature() const;

	// FE 1st LO Controller specific monitor points.

	/// Status of the phased locked loop. Set to Ox55
	/// bit 0: Gunn Oscillator lock (0:locked; 1:unlocked)
	/// bit 1: DRO lock (0:locked; 1:unlocked)
	/// bit 2: REF. Status (0:fault condition; 1:sufficient power)
	/// bit 3: IF Status (0:fault condition; 1:sufficient power)
	/// bit 4: CH 1 (0:open; 1:closed)
	/// bit 5: CH 2 (0:open; 1:closed)
	/// bit 6: 48ms pulse received (0:no; 1:yes)
	/// bit 7: unused
	std::vector<CAN::byte_t> get_lo_control_status() const;

	/// Get tuning voltage of Band 3 Gunn Oscillator.
	/// float, IEEE 754-1990 format. Set to 10.
	float get_gunn_voltage_b3() const;

	/// Get tuning voltage of Band 6 Gunn Oscillator.
	/// float, IEEE 754-1990 format. Set to 11.
	float get_gunn_voltage_b6() const;

	/// Get number of counts of Band 3 Gunn Tune motor position.
	/// Range [0x0000, 0xFFFF].
	unsigned short get_gunn_tune_counts_b3() const;

	/// Get number of counts of Band 3 Gunn Backshort motor position.
	/// Range [0x0000, 0xFFFF].
	unsigned short get_gunn_backshort_counts_b3() const; 

	/// Get number of counts of Band 6 Gunn Tune motor position.
	/// Range [0x0000, 0xFFFF].
	unsigned short get_gunn_tune_counts_b6() const;

	/// Get number of counts of Band 6 Gunn Backshort motor position.
	/// Range [0x0000, 0xFFFF].
	unsigned short get_gunn_backshort_counts_b6() const; 

	/// Get voltage corresponding to detected IF power.
	/// float, IEEE 754-1990 format. Set to 12.
	float get_if_power() const;

	/// Get the temperature reading in C on Band 3 Gunn block.
	/// float, IEEE 754-1990 format. Set to 13.
	float get_gunn_temp_b3() const;

	/// Get the temperature reading in C on Band 6 Gunn block.
	/// float, IEEE 754-1990 format. Set to 14.
	float get_gunn_temp_b6() const;

	/// Get current drawn by the photomixer for band 3 mmm-wave reference.
	/// float, IEEE 754-1990 format. Set to 15.
	float get_photo_current_b3() const;

	/// Get bias voltage to photomixer for band 3 mmm-wave reference.
	/// float, IEEE 754-1990 format. Set to 16.
	float get_photo_voltage_b3() const;

	/// Get current drawn by the photomixer for band 6 mmm-wave reference.
	/// float, IEEE 754-1990 format. Set to 17.
	float get_photo_current_b6() const;

	/// Get bias voltage to photomixer for band 6 mmm-wave reference.
	/// float, IEEE 754-1990 format. Set to 18.
	float get_photo_voltage_b6() const;

	// Get tuning voltage for the 26GHz DRO in IF Converter/Mux module.
	/// float, IEEE 754-1990 format. Set to 19.
	float get_dro_mon_voltage() const;

	// Controls

	/// Set Band 3 Gunn Oscillator Tune motor position.
	/// Range [0x0000, 0xFFFF]
	void set_gunn_tune_counts_b3(std::vector<CAN::byte_t> data);

	/// Set Band 3 Gunn Oscillator Backshort motor position.
	/// Range [0x0000, 0xFFFF]
	void set_gunn_backshort_counts_b3(std::vector<CAN::byte_t> data);

	/// Set Band 6 Gunn Oscillator Tune motor position.
	/// Range [0x0000, 0xFFFF]
	void set_gunn_tune_counts_b6(std::vector<CAN::byte_t> data);

	/// Set Band 6 Gunn Oscillator Backshort motor position.
	/// Range [0x0000, 0xFFFF]
	void set_gunn_backshort_counts_b6(std::vector<CAN::byte_t> data);

	/// Set LO power into cartridge.
	/// Range [0x0000, 0x0FFF]
	void set_lo_power_b6(std::vector<CAN::byte_t> data);

	/// Control loop which adjusts ferrite modulator with SIS mixer. 
	/// current.
	/// bit 0: loop (0:open; 1:closed).
	/// bits 1-7: unused
	void set_modulator_servo(CAN::byte_t cmd);

	/// Turn the Gunn Oscillator bias on/off.
	/// bit 0: Gun oscillator (0:off; 1:on).
	/// bits 1-7: unused
	void set_drive_enable(CAN::byte_t cmd);

	/// To open or close the phased lock loop to the Gunn oscillator.
	/// bit 0: loop (0:open; 1:closed)
	/// bits 1-7: unused
	void set_loop_enable(CAN::byte_t cmd);

	/// Set the bias voltage to the Gunn oscillator.
	/// float, IEEE 754-1990 format.
	void set_nominal_voltage(float value);

	/// Set the loop gain of the phased locked Gunn oscillator.
	/// float, IEEE 754-1990 format.
	void set_gunn_loop_gain(float value);
	
	/// Set tuning range of Gunn oscillator.
	/// float, IEEE 754-1990 format.
	void set_gun_tune_range(float value);

	/// Select either Band 3 or Band 6 Gunn Oscillator as active device.
	/// bit 0: (0:Band 3; 1:Band 6)
	/// bits 1-7: unused
	void set_gunn_select(CAN::byte_t cmd);

	/// Set the Fine Tune Synthesizer(DDS) to the required frequency.
	/// float, IEEE 754-1990 format.
	void set_fts_frequency(float value);

	/// Set the bias to the photomixer on or off for Band 3.
	/// bit 0: (0:off; 1:on);
	/// bits 1-7: unused
	void set_photomixer_b3(CAN::byte_t cmd);

	/// Set the bias to the photomixer on or off for Band 6.
	/// bit 0: (0:off; 1:on:);
	/// bits 1-7: unused
	void set_photomixer_b6(CAN::byte_t cmd);

	/// Select the band of operation in the IF Mux.
	/// bit 0: (0:Band 3; 1:Band 6);
	/// bits 1-7: unused
	void set_band_select(CAN::byte_t cmd);

	// Throw this if something bad happens.
	class LO1stContError : public CAN::Error {
	public:
	    LO1stContError(const std::string &error) : CAN::Error(error) {;}
	};

	enum Monitor_t {
	    // Generic monitor points
	    GET_CAN_ERROR           = AMB::Device::GET_CAN_ERROR,
	    GET_PROTOCOL_REV_LEVEL  = AMB::Device::GET_PROTOCOL_REV_LEVEL,
	    GET_TRANS_NUM           = AMB::Device::GET_TRANS_NUM,
	    GET_AMBIENT_TEMPERATURE = AMB::Device::GET_AMBIENT_TEMPERATURE,

	    // Monitor points peculiar to the FE 1st LO Controller
	    GET_LO_CONTROL_STATUS        = 0x00000001,
	    GET_GUNN_VOLTAGE_B3          = 0x00000002,
	    GET_GUNN_VOLTAGE_B6          = 0x00000003,
	    GET_GUNN_TUNE_COUNTS_B3      = 0x00000004,
	    GET_GUNN_BACKSHORT_COUNTS_B3 = 0x00000005,
	    GET_GUNN_TUNE_COUNTS_B6      = 0x00000006,
	    GET_GUNN_BACKSHORT_COUNTS_B6 = 0x00000007,
	    GET_IF_POWER                 = 0x00000008,
	    GET_GUNN_TEMP_B3             = 0x00000009,
	    GET_GUNN_TEMP_B6             = 0x0000000A,
	    GET_PHOTO_CURRENT_B3         = 0x0000000B,
	    GET_PHOTO_VOLTAGE_B3         = 0x0000000C,
	    GET_PHOTO_CURRENT_B6         = 0x0000000D,
	    GET_PHOTO_VOLTAGE_B6         = 0x0000000E,
	    GET_DRO_MON_VOLTAGE          = 0x0000000F
	};

	enum Controls_t {
	    SET_GUNN_TUNE_COUNTS_B3      = 0x00001001,
	    SET_GUNN_BACKSHORT_COUNTS_B3 = 0x00001002,
	    SET_GUNN_TUNE_COUNTS_B6      = 0x00001003,
	    SET_GUNN_BACKSHORT_COUNTS_B6 = 0x00001004,
	    SET_LO_POWER_B6              = 0x00001005,
	    SET_MODULATOR_SERVO          = 0x00001006,
	    SET_DRIVE_ENABLE             = 0x00001007,
	    SET_LOOP_ENABLE              = 0x00001008,
	    SET_NOMINAL_VOLTAGE          = 0x00001009,
	    SET_GUNN_LOOP_GAIN           = 0x0000100A,
	    SET_GUNN_TUNE_RANGE          = 0x0000100B,
	    SET_GUNN_SELECT              = 0x0000100C,
	    SET_FTS_FREQUENCY            = 0x0000100D,
	    SET_PHOTOMIXER_B3            = 0x0000100E,
	    SET_PHOTOMIXER_B6            = 0x0000100F,
	    SET_BAND_SELECT              = 0x00001010
	};

	/// IEEE 754-1990 format to float.
	/// If successful, returns true and value contains float. 
	///Otherwise returns false.
	bool AMB::LO1stCont::validIEEEFloat(std::vector<CAN::byte_t> data, 
					      float &value);
 
	/// float to IEEE 754-1990 format
	std::vector<CAN::byte_t> AMB::LO1stCont::IEEEFloatToData(float value) const;

     private:
	// Undefined and inaccessible - copying does not make sense.
	LO1stCont(const LO1stCont &);
	LO1stCont &operator=(const LO1stCont &);

	/// Bus location (node number).
	node_t node_m;
	/// Serial number of this node.
	std::vector<CAN::byte_t> sn_m;

	/// Number of transactions.
	mutable unsigned int trans_num_m;

	unsigned short gunn_tune_counts_b3_m;
	unsigned short gunn_backshort_counts_b3_m;
	unsigned short gunn_tune_counts_b6_m;
	unsigned short gunn_backshort_counts_b6_m;

    };  // class LO1stCont

}  // namespace AMB

#endif  // LO1stVCont_H
