// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#ifndef ACU_AEC_INCLUDE_H
#define ACU_AEC_INCLUDE_H

#include <vector>

/*
 * Commonly used enums to describe the ACU
 */

// monitor values as part of get_acu_error() response
enum ACU_AEC_Error_t
{
    INVALID_DATA            = 1,
    COMMAND_NOT_ALLOWED     = 2
};
  
// used with get_acu_mode_rsp()
enum AEC_AZEL_Mode_t
{
    SHUTDWN                = 0x0,
    STANDB                 = 0x1,
    ENCODR                 = 0x2,
    AUTONOM                = 0x3,
    SURVIVAL_STW           = 0x4,
    MAINTENANCE_STW        = 0x5,
    ACU_LOCL               = 0x1,
    ACU_REMOT              = 0x2
};
  
// used with get/set_az_brake(), get/set_el_brake()
enum AEC_AZEL_Brake_t
{
    BRKE_OFF = 0,
    BRKE_ON  = 1
};

// used with get_az_motor(), get_el_motor()
enum AEC_AZEL_Motor_t
{
    MOTOR_POWER   = 1,
    MOTOR_ENABLED = 2
};

  
/*
 * AZ/EL status bits.  Setting bits to these values enables
 * the condition or the fault.  The status bits cover two
 * integer words, so Status 1, is low word, and Status2 is
 * high word in status replies.
 */
enum AEC_AZEL_Status1_t
{
    SW_CW_PRELIM            = 0x00000001,
    HW_CW_PRELIM            = 0x00000002,
    CW_FINAL_LIMIT          = 0x00000004,
    CW_SHUTDOWN_ON_LIMIT    = 0x00000008,
    SW_CCW_LIMIT            = 0x00000010,
    HW_CCW_LIMIT            = 0x00000020,
    CCW_FINAL_LIMIT         = 0x00000040,
    CCW_SHUTDOWN_ON_LIMIY   = 0x00000080,
    OVER_SPD                = 0x00000100,
    MOTOR1_OVER_CURRENT     = 0x00000200,
    MOTOR1_OVER_HEATING     = 0x00000400,
    MOTOR2_OVER_CURRENT     = 0x00000800,
    MOTOR2_OVER_HEATING     = 0x00001000,
    ENABLE_FAULT            = 0x00010000,
    DRIVER1_FAULT           = 0x00020000,
    DRIVER2_FAULT           = 0x00040000,
    DRIVER3_FAULT           = 0x00080000,
    DRIVER4_FAULT           = 0x00100000,
    STOWPIN1_POS_ERROR      = 0x01000000,
    MOTOR1_THERMAL_PROT     = 0x02000000
};
  
enum AEC_AZEL_Status2_t
{
    BREAK_POS_ERROR         = 0x00000001,
    HYDR_MOTOR_THERMAL_PROT = 0x00000002,
    BREAK_WEAR              = 0x00000004,
    ENCODER_VALUE_FAULT     = 0x00000100,
    ENC_POS_NOT_AVAILABLE   = 0x00000200,
    ENCODER_HEAD1_FAULT     = 0x00000400,
    ENCODER_HEAD2_FAULT     = 0x00000800,
    ENCODER_HEAD3_FAULT     = 0x00001000,
    ENCODER_HEAD4_FAULT     = 0x00002000,
    ENCODER_VALUE_NOT_VALID = 0x00004000,
    SERVO_OSCILLATION       = 0x00008000     
};

// used with get_shutter()
enum AEC_Shutter_Status_Bits_t
{
    SHUTTER_OPENED          = 1,
    SHUTTER_CLOSE           = 2,
    SHUTTER_MOTOR_ON        = 4,
    SHUTTER_SYSTEM_ERROR    = 8
};

 // used with set_shutter()
enum AEC_Shutter_Cmd_Bits_t
{
    SHUTTER_CLOS            = 0,
    SHUTTER_OP              = 1
};
 
// used with set_stow_pin() and get_stow_pin()
enum AEC_Stow_Pin_t
{
    AZM_IN                  = 0x0001,
    AZM_OUT                 = 0x0002,
    EL1_IN                  = 0x0100,
    EL1_OUT                 = 0x0200,
    EL2_IN                  = 0x0400,
    EL2_OUT                 = 0x0800
};
  
// used with get_system_status()
enum AEC_System_Status_t
{
    EMERGENCY_STOP          = 0x00000001,
    STAIRWAY_INTERLOCK      = 0x00000002,
    HANDLING_INTERLOCK      = 0x00000004,
    SMOKE_ALARM             = 0x00000008,
    ACU_BOOT_FAILURE        = 0x00000010,
    SURVIVAL_STOW_NO_CMD    = 0x00000020,
    SURVIVAL_STOW_NO_TIME   = 0x00000040,
    PCU_BASEMENT            = 0x00000100,
    PCU_CONTROL_CABINET     = 0x00000200,
    PCU_RECEIVER_CABIN      = 0x00000300,
    LOCAL_TERMINAL_CONTROL  = 0x00000400
};
  
// used with get_subref_limits()
enum AEC_Subref_Limits_t
{
    X_UPPER_SW_LIMIT        = 0x000001,
    X_LOWER_SW_LIMIT        = 0x000002,
    X_UPPER_LIMIT           = 0x000004,
    X_LOWER_LIMIT           = 0x000008,
    Y_UPPER_SW_LIMIT        = 0x000100,
    Y_LOWER_SW_LIMIT        = 0x000200,
    Y_UPPER_LIMIT           = 0x000400,
    Y_LOWER_LIMIT           = 0x000800,
    Z_UPPER_SW_LIMIT        = 0x010000,
    Z_LOWER_SW_LIMIT        = 0x020000,
    Z_UPPER_LIMIT           = 0x040000,
    Z_LOWER_LIMIT           = 0x080000
};

// used with get_subref_status()
enum AEC_Subref_Status_t
{
    LOCAL_CNTR_NOT_READY    = 0x00000001,
    LOCAL_CNTR_FAILURE      = 0x00000002,
    X_AMPL_FAILURE          = 0x00000100,
    X_FOLLOWING_ERROR       = 0x00000200,
    X_POSITION_REACHED      = 0x00000400,
    Y_AMPL_FAILURE          = 0x00010000,
    Y_FOLLOWING_ERROR       = 0x00020000,
    Y_POSITION_REACHED      = 0x00040000,
    Z_AMPL_FAILURE          = 0x01000000,
    Z_FOLLOWING_ERROR       = 0x02000000,
    Z_POSITION_REACHED      = 0x04000000
};

// used with get_metr_mode()
enum AEC_Metr_Mode_t
{
    METROLOGY_SYSTEM_READY  = 0x00000001,
    METROLOGY_CORR_ENABLED  = 0x00000002
};

// used with set_metr_mode()
enum AEC_Metr_Cmd_Mode_t
{
    METROLOGY_CORR_ENABLE  = 0x00000001
};

// used with get_metr_status()
enum AEC_Metr_Status_t
{
    SENSOR1_FAULT    = 0x00000001,
    SENSOR2_FAULT    = 0x00000002,
    SENSOR3_FAULT    = 0x00000004,
    SENSOR4_FAULT    = 0x00000008,
    SENSOR5_FAULT    = 0x00000010,
    SENSOR6_FAULT    = 0x00000020,
    SENSOR7_FAULT    = 0x00000040,
    SENSOR8_FAULT    = 0x00000080,
    SENSOR9_FAULT    = 0x00000100,
    SENSOR10_FAULT   = 0x00000200

};

// used with get_power_status()
enum AEC_Power_Status_t
{
    POWER_MONITOR_ALARM    = 0x01,
    POWER_FROM_TRANSPORTER = 0x02,
    EXTERNAL_POWER_FAULT   = 0x04,
    SYSTEM_SUPPLY          = 0x08,
    UPS_LOW_BATTERY        = 0x10
};

// used with get_ac_status()
enum AEC_AC_Status_t
{
    CHILLER_FUNCTIONING         = 0x01,
    CHILLER_FAULT               = 0x02,
    PUMP_FAULT                  = 0x04,
    CHILLER_FAN_FAULT           = 0x08,
    ATU_FAN_FAULT               = 0x10,
    SET_TEMP_OUT_OF_RANGE       = 0x20,
    WARN_AIR_TEMP_OUT_OF_RANGE  = 0x40,
    ERROR_AIR_TEMP_OUT_OF_RANGE = 0x80
};

/**
 * An Abstract Base Class (ABC) that defines CAN bus accesses on the AEC 
 * Antenna Control Unit (ACU) computer.  All members of this class must be 
 * over-ridden.
 *
 * The class definitions are 1-to-1 with the monitor and control points 
 * defined in ALMA ICD No. 9, "Antenna / Monitor and Control Interface".
 *
 * There are two classes derived from this class:
 * 1) ACUSim, a simulator (see ACUSim.h in this module), and
 * 2) ACUhw, an interface to the CAN hardware (part of the ticsAntVA module).
 *
 * For a discussion of this design, see ticsAntVA/lcu/include/ACUhw.h.
 */
class ACUAECbase
{
  public:
    // After construction, the ACU is in the STANDBY state.
    ACUAECbase() {};
    virtual ~ACUAECbase() {};
      
    //
    // Generic monitor points
    //
    // RCA 0x30000, aka generic GET_PROTOCOL_REV_LEVEL
    virtual std::vector<char> get_sw_rev_level(
	int& errcode,
	unsigned long long& timestamp) const = 0;

    // RCA 0x30001
    virtual unsigned long get_can_error(
	int& errcode,
	unsigned long long& timestamp) const = 0;

    // RCA 0x30002
    virtual unsigned long get_num_trans(
	int& errcode,
	unsigned long long& timestamp) const = 0;

    // RCA 0x30004, aka generic GET_SW_REV_LEVEL
    virtual std::vector<char> get_system_id(
	int& errcode,
	unsigned long long& timestamp) const = 0;

    //
    // Monitor Requests
    //
      
    // get_acu_mode_rsp returns 2 bytes.  The azimuth mode is encoded in the
    // low nibble of byte 0, and elevation mode in high nibble of same.  The 
    // access mode is encoded in byte 1.
    // Uses ACU_Mode_t and AXIS_Mode_t enums.
    virtual std::vector<char> get_acu_mode_rsp(
	int& errcode,
	unsigned long long& timestamp) const = 0;
      
    // get_acu_error() returns 5 bytes.  The error byte is in byte 0, and 
    // is defined in enum ACU_Error_t.  The upper 4 bytes are the command
    // RCA that caused the error in host byte order.
    virtual unsigned long long get_acu_error(
	int& errcode,
	unsigned long long& timestamp) const = 0;
      
    // get_az_posn_rsp(), get_el_posn_rsp() return 2 positions:
    // on last Timing Event and 24 ms before
    // Positions are in binary parts of a circle, a.k.a BPC.  The BPC here is
    // a signed 32 bit number with +180 to -180 at 0x40000000.  These are long
    // long because there are two 32 bit values returned.
    virtual unsigned long long get_az_posn_rsp(	// BPC units
	int& errcode,
	unsigned long long& timestamp) const = 0;
    virtual unsigned long long get_el_posn_rsp(	// BPC units
	int& errcode,
	unsigned long long& timestamp) const = 0;

    // get_az_enc(), get_el_enc() return positions on last time tick
    virtual long get_az_enc(			// encoder units
	int& errcode,
	unsigned long long& timestamp) const = 0;
    virtual void get_el_enc(			// encoder units
	std::vector<long>& positions,
	int& errcode,
	unsigned long long& timestamp) const = 0;

    // az has two motors.
    // get_az_motor_currents() - each motor current is two bytes, -255 to 255 A.
    virtual std::vector<short> get_az_motor_currents(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    
    // get_az_motor_temps() - each motor temp is two bytes, -50 to 205 C
    virtual std::vector<short> get_az_motor_temps(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    
    // get_az_motor_torque() - each motor torque is two bytes, -32768 to 32768 dNm
    virtual std::vector<short> get_az_motor_torques(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    
    // el has one motor.  the units are the same as az, but half the data
    virtual short get_el_motor_currents(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    virtual short get_el_motor_temps(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    virtual short get_el_motor_torques(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    
    // get_az_brake(), get_el_brake() returns az/el brake status
    // Uses enum AZEL_Brake_t
    virtual unsigned char get_az_brake(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    virtual unsigned char get_el_brake(
	int& errcode,
	unsigned long long& timestamp) const = 0;

    // get_az_motor(), get_el_motor() returns az/el motor status
    // Uses enum AZEL_Motor_t
    virtual unsigned char get_az_motor(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    virtual unsigned char get_el_motor(
	int& errcode,
	unsigned long long& timestamp) const = 0;

    // get_az_motion_lim(), get_el_motion_lim() returns az/el motion limits
    // Two bytes for velocity and two bytes for acceleration
    virtual unsigned long get_az_motion_lim(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    virtual unsigned long get_el_motion_lim(
	int& errcode,
	unsigned long long& timestamp) const = 0;

    // get_az_servo_coeff_n, get_el_servo_coeff_n is implemented as an
    // array that is simply stuffed with the set values.  no conversion is
    // done.
    virtual double get_az_servo_coeff(
	long n,
	int& errcode,
	unsigned long long& timestamp) const = 0;
    virtual double get_el_servo_coeff(
	long n,
	int& errcode,
	unsigned long long& timestamp) const = 0;
   
    // get_az_status(), get_el_status() returns a multi-byte status with
    // mult-bit fields.  Uses enums AZEL_Status1, AZEL_Status2
    virtual unsigned long long get_az_status(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    virtual unsigned long long get_el_status(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    
    // get_idle_stow_time() seconds to wait for comm or tix and then stow.
    virtual unsigned short get_idle_stow_time(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    
    // get_shutter, uses enum Shutter_Bits_t
    virtual unsigned char get_shutter(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    
    // get_stow_pin, short word with lower byte az, upper byte el pin status
    // use Stow_Pin_t enum
    virtual unsigned short get_stow_pin(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    
    // get_system_status() - get antenna related system status.
    // Uses enum System_Status_t
    virtual unsigned long get_system_status(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    
    // get_ip_address() - get ACU IP address
    virtual unsigned long get_ip_address(
	int& errcode,
	unsigned long long& timestamp) const = 0;

    // get_pt_model_coeff_n is implemented as an
    // array that is simply stuffed with the set values.  no conversion is
    // done.
    virtual double get_pt_model_coeff(
	long n,
	int& errcode,
	unsigned long long& timestamp) const = 0;

    // get_subref_abs_posn(),returns subreflector absolute position.
    // Two bytes for each axis, x, y, z.
    // array that is simply stuffed with the set values.  no conversion is
    // done.
    virtual std::vector<short> get_subref_abs_posn(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    
    // get_subref_delta_posn(),returns subreflector delta position.
    // Two bytes for each axis, x, y, z.
    // array that is simply stuffed with the set values.  no conversion is
    // done.
    virtual std::vector<short> get_subref_delta_posn(
	int& errcode,
	unsigned long long& timestamp) const = 0;

    // get_subref_limits(), returns a multi-byte limit status with
    // mult-bit fields.  Uses enums Subref_Limits_t
    virtual unsigned long get_subref_limits(
	int& errcode,
	unsigned long long& timestamp) const = 0;
    
    // get_subref_status(), returns a multi-byte status with
    // mult-bit fields.  Uses enums Subref_Status_t
    virtual unsigned long get_subref_status(
	int& errcode,
	unsigned long long& timestamp) const = 0;

    // get_metr_mode(), returns a one byte metrology mode with
    // mult-bit fields.  Uses enums Metr_Mode_t
    virtual unsigned char get_metr_mode(
	int& errcode,
	unsigned long long& timestamp) const = 0;

    // get_metr_equip_status(), returns a two byte metrology status with
    // mult-bit fields.  Uses enums Metr_Status_t
    virtual unsigned short get_metr_equip_status(
	int& errcode,
	unsigned long long& timestamp) const = 0;

    // get_metr_values_n is implemented as an array
    virtual long get_metr_values(
	long n,
	int& errcode,
	unsigned long long& timestamp) const = 0;

    // get_metr_deltas return az and el deltas each 4 bytes
    virtual std::vector<long> get_metr_deltas(
	int& errcode,
	unsigned long long& timestamp) const = 0;

    // get_power_status(), returns one byte power status with
    // mult-bit fields.  Uses enums Power_Status_t
    virtual unsigned short get_power_status(
	int& errcode,
	unsigned long long& timestamp) const = 0;

    // get_ac_status(), returns one byte power status with
    // mult-bit fields.  Uses enums AC_Status_t
    virtual unsigned char get_ac_status(
	int& errcode,
	unsigned long long& timestamp) const = 0;

    //
    // Control Commands
    //
    
    // set_reset_acu_cmd()  The underlying control point takes an argument
    // with only one valid value, so I am assuming the ACU will not
    // implement other values over time.
    virtual void reset_acu_cmd(
	int& errcode,
	unsigned long long& timestamp) = 0;
    
    // acu modes are defined in the AZEL_Mode_t enum
    // This sets the az/el axes modes.  The byte's low nibble is az, and
    // the high nibble is el.
    virtual void acu_mode_cmd(
	const unsigned char mode,
	int& errcode,
	unsigned long long& timestamp) = 0;
    
    // clear_fault_cmd() clear all faults.
    virtual void clear_fault_cmd(
	int& errcode,
	unsigned long long& timestamp) = 0;
    
    // Hardware interface for az_traj_cmd and el_traj_cmd are two integers 
    // with position and velocity at the second timing event after the 
    // command is sent.
    virtual void az_traj_cmd(		// encoder units
	const long position,
	const long velocity,
	int& errcode,
	unsigned long long& timestamp) = 0;
    virtual void el_traj_cmd(		// encoder units
	const long position,
	const long velocity,
	int& errcode,
	unsigned long long& timestamp) = 0;

    // set_az_brake(), set_el_brake(), uses Brake_Mode_t enum
    virtual void set_az_brake(
	const unsigned char mode,
	int& errcode,
	unsigned long long& timestamp) = 0;
    virtual void set_el_brake(
	const unsigned char mode,
	int& errcode,
	unsigned long long& timestamp) = 0;

    // set_az_motor_power(), set_el_motor_power()
    virtual void set_az_motor_power(
	const unsigned char mode,
	int& errcode,
	unsigned long long& timestamp) = 0;
    virtual void set_el_motor_power(
	const unsigned char mode,
	int& errcode,
	unsigned long long& timestamp) = 0;

    // set_az_motor_driver(), set_el_motor_driver()
    virtual void set_az_motor_driver(
	const unsigned char mode,
	int& errcode,
	unsigned long long& timestamp) = 0;
    virtual void set_el_motor_driver(
	const unsigned char mode,
	int& errcode,
	unsigned long long& timestamp) = 0;

    // set_az_motion_lim(), set_el_motion_lim()
    virtual void set_az_motion_lim(
	const long limit,
	int& errcode,
	unsigned long long& timestamp) = 0;
    virtual void set_el_motion_lim(
	const long limit,
	int& errcode,
	unsigned long long& timestamp) = 0;

    // init_az_enc_abs_pos(), init_el_enc_abs_pos()
    virtual void init_az_enc_abs_pos(
	const unsigned char mode,
	int& errcode,
	unsigned long long& timestamp) = 0;
    virtual void init_el_enc_abs_pos(
	const unsigned char mode,
	int& errcode,
	unsigned long long& timestamp) = 0;

    // AZ/EL servo commands
    virtual void set_az_servo_coeff(
	const long index,
	double value,
	int& errcode,
	unsigned long long& timestamp) = 0;
    virtual void set_el_servo_coeff(
	const long index,
	double value,
	int& errcode,
	unsigned long long& timestamp) = 0;
    virtual void set_az_servo_default(
	int& errcode,
	unsigned long long& timestamp) = 0;
    virtual void set_el_servo_default(
	int& errcode,
	unsigned long long& timestamp) = 0;
    
    // miscellaneous ACU commands
    virtual void set_idle_stow_time(
	const unsigned short seconds,
	int& errcode,
	unsigned long long& timestamp) = 0;
    virtual void set_shutter(
	const unsigned char bits,
	int& errcode,
	unsigned long long& timestamp) = 0;
    
    // byte 0 is AZ, byte 1 is EL.  see Stow_Pin_t enum
    virtual void set_stow_pin(
	const unsigned short bits,
	int& errcode,
	unsigned long long& timestamp) = 0;

    virtual void set_ip_address(
	const unsigned long address,
	int& errcode,
	unsigned long long& timestamp) = 0;

    virtual void set_pt_model_coeff(
	const long index,
	double value,
	int& errcode,
	unsigned long long& timestamp) = 0;

    virtual void set_subref_abs_posn(
	const long long value,
	int& errcode,
	unsigned long long& timestamp) = 0;

    virtual void set_subref_delta_posn(
	const long long value,
	int& errcode,
	unsigned long long& timestamp) = 0;

    virtual void set_metr_mode(
	const unsigned char mode,
	int& errcode,
	unsigned long long& timestamp) = 0;

    /*
     * CAN Addresses for ACU M&C Points.
     * These are used in the monitor() and control() switch statements.
     */
    enum AEC_Monitor_t
    {
	ACU_MODE_RSP            = 0x00022,
        AZ_POSN_RSP             = 0x00012,
        EL_POSN_RSP             = 0x00002,
        GET_ACU_ERROR           = 0x0002F,
        GET_AZ_BRAKE            = 0x00014,
	GET_AZ_ENC              = 0x00017,
        GET_AZ_MOTOR            = 0x00018,
        GET_AZ_MOTOR_CURRENTS   = 0x00019,
        GET_AZ_MOTOR_TEMPS      = 0x0001A,
        GET_AZ_MOTOR_TORQUE     = 0x00015,
        GET_AZ_SERVO_COEFF_0    = 0x03020,
        GET_AZ_MOTION_LIMIT     = 0x00016,
        GET_AZ_STATUS           = 0x0001B,
	GET_CAN_ERROR           = 0x30001,
        GET_EL_BRAKE            = 0x00004,
        GET_EL_ENC              = 0x00007,
        GET_EL_MOTOR            = 0x00008,
        GET_EL_MOTOR_CURRENTS   = 0x00009,
        GET_EL_MOTOR_TEMPS      = 0x0000A,
        GET_EL_MOTOR_TORQUE     = 0x00005,
        GET_EL_SERVO_COEFF_0    = 0x03010,
        GET_EL_MOTION_LIMIT     = 0x00006,
        GET_EL_STATUS           = 0x0000B,
        GET_IDLE_STOW_TIME      = 0x00025,
        GET_IP_ADDRESS          = 0x0002D,
	GET_NUM_TRANS           = 0x30002,
        GET_PT_MODEL_COEFF_0    = 0x03000,
        GET_SHUTTER             = 0x0002E,
        GET_STOW_PIN            = 0x00024,
        GET_SUBREF_ABS_POSN     = 0x00026,
        GET_SUBREF_DELTA_POSN   = 0x00027,
        GET_SUBREF_LIMITS       = 0x00028,
        GET_SUBREF_STATUS       = 0x00029,
        GET_METR_MODE           = 0x00031,
        GET_METR_TILT_1         = 0x00032,
        GET_METR_TILT_2         = 0x00033,
        GET_METR_TEMPS_0        = 0x03030,
        GET_METR_DELTAS         = 0x00034,
        GET_POWER_STATUS        = 0x00030,
        GET_AC_STATUS           = 0x0002C,
	GET_SW_REV_LEVEL        = 0x30000,  // same as GET_PROTOCOL_REV_LEVEL
	GET_SYSTEM_ID           = 0x30004,  // same as GET_SW_REV_LEVEL
        GET_SYSTEM_STATUS       = 0x00023 
    };

    enum AEC_Controls_t
    {
        ACU_MODE_CMD            = 0x01022,
        AZ_TRAJ_CMD             = 0x01012,
        CLEAR_FAULT_CMD         = 0x01021,
        EL_TRAJ_CMD             = 0x01002,
        RESET_ACU_CMD           = 0x0102F,
        SET_AZ_BRAKE            = 0x01014,
        SET_AZ_MOTOR_POWER      = 0x01015,
        SET_AZ_MOTOR_DRIVER     = 0x01016,
        SET_AZ_SERVO_COEFF_0    = 0x02020,
        SET_AZ_SERVO_DEFAULT    = 0x01017,
        SET_AZ_MOTION_LIM       = 0x01013,
        INIT_AZ_ENC_ABS_POS     = 0x01018,
        SET_EL_BRAKE            = 0x01004,
        SET_EL_MOTOR_POWER      = 0x01005,
        SET_EL_MOTOR_DRIVER     = 0x01006,
        SET_EL_SERVO_COEFF_0    = 0x02010,
        SET_EL_SERVO_DEFAULT    = 0x01007,
        SET_EL_MOTION_LIM       = 0x01003,
        INIT_EL_ENC_ABS_POS     = 0x01008,
        SET_IDLE_STOW_TIME      = 0x01025,
        SET_IP_ADDRESS          = 0x01024,
        SET_SHUTTER             = 0x0102E,
        SET_STOW_PIN            = 0x0102D,
        SET_PT_MODEL_COEFF_0    = 0x02000,
        SET_SUBREF_ABS_POSN     = 0x01029,
        SET_SUBREF_DELTA_POSN   = 0x0102A,
        SUBREF_DELTA_ZERO_CMD   = 0x0102B,
        SET_METR_MODE           = 0x01026,
	SET_AIR_CONDITIONING    = 0x01027
    };

};  // class ACUAECbase

#endif  // ACU_AEC_INCLUDE_H
