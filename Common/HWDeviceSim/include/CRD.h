// @(#) $Id$
//
// Copyright (C) 2002, 2004
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#if !defined(CRD_H)
#define CRD_H

#include "AMBDevice.h"
#include "CANError.h"
#include <string>

namespace AMB
{
  /** The CRD class is a simulator for the central reference distributor
   */
  class CRD : public AMB::Device
  {
  public:
    /** Constructor
     ** @param node Node ID of this device
     ** @param serialNumber S/N of this device
     */
    CRD(node_t node, const std::vector<CAN::byte_t>& serialNumber);

    /// Destructor
    virtual ~CRD();
    
    /// Return the device's node ID
    virtual node_t node() const;

    /// Return the device's S/N
    virtual std::vector<CAN::byte_t> serialNumber() const;
    
    /** Monitor interface
     ** @param CAN node RCA
     ** @return std::vector of bytes containing monitor info. Each response is
     ** sized to what's specified in the ICD.
     */
    virtual std::vector<CAN::byte_t> monitor(rca_t rca) const;
    
    /** Control Interface ('set' functions)
     ** @param rca CAN node RCA
     ** @param data CAN message data required by device
     */
    virtual void control(rca_t rca, const std::vector<CAN::byte_t>& data);
    
    /// Monitor points for the LORR
    enum Monitor_t {
      /// Generic monitor points
      //@{
      GET_SERIAL_NUMBER       = 0x00000,
      GET_CAN_ERROR           = AMB::Device::GET_CAN_ERROR,
      GET_PROTOCOL_REV_LEVEL  = AMB::Device::GET_PROTOCOL_REV_LEVEL,
      GET_TRANS_NUM           = AMB::Device::GET_TRANS_NUM,
      GET_AMBIENT_TEMPERATURE = AMB::Device::GET_AMBIENT_TEMPERATURE,
      //@}
      
      /// Specific CRD data point monitors
      //@{
      GET_RESET_TIME         = 0x00001,
      MASER_VS_GPS_COUNTER   = 0x00004,
      MASER_COUNTER          = 0x00005,
      EFC_5_MHZ              = 0x00006,
      PWR_5_MHZ              = 0x00007,
      PWR_25_MHZ             = 0x00008,
      PWR_125_MHZ            = 0x00009,
      PWR_2_GHZ              = 0x0000A,
      LASER_CURRENT          = 0x0000C,
      VDC_MINUS_5            = 0x0000D,
      VDC_15                 = 0x0000E,
      STATUS                 = 0x0000F
      //@}
    };

    /// Control Functions for the CRD
    enum Controls_t {
      SET_RESET_TIME               = 0x00081,
      XILINX_PROM_RESET            = 0x31001,
      MASTER_COUNTER_RESET         = 0x00083
    };
    
    // Throw this if something bad happens.
    class CRDError : public CAN::Error {
    public:
      CRDError(const std::string &error) : CAN::Error(error) {;}
    };
    
    /// Get functions for unit testing
    //@{
      unsigned long long getRESET_TIME();
      unsigned long long getMASER_VS_GPS_COUNTER();
      unsigned long long getMASER_COUNTER();
      short              getEFC_5_MHZ();
      short              getPWR_5_MHZ();
      short              getPWR_25_MHZ();
      short              getPWR_125_MHZ();
      short              getPWR_2_GHZ();
      short              getLASER_CURRENT();
      short              getVDC_MINUS_5();
      short              getVDC_15();
      CAN::byte_t        getSTATUS() const;
    
  private:
    /// Take the default implementation = return 0,0,0,0
    virtual std::vector<CAN::byte_t>  get_can_error() const;
    
    /// Take the default implementation = return 0,0,0
    virtual std::vector<CAN::byte_t>  get_protocol_rev_level() const;
    
    /// Return the number of transactions
    virtual unsigned int get_trans_num() const;
    
    /// Temperature on M&C interface board.
    double get_ambient_temperature() const;
    
    // CRD specific monitor points.    
    std::vector<CAN::byte_t> CAN_GET_RESET_TIME() const;
    std::vector<CAN::byte_t> CAN_MASER_VS_GPS_COUNTER() const;
    std::vector<CAN::byte_t> CAN_MASER_COUNTER() const;
    std::vector<CAN::byte_t> CAN_EFC_5_MHZ() const;
    std::vector<CAN::byte_t> CAN_PWR_5_MHZ() const;
    std::vector<CAN::byte_t> CAN_PWR_25_MHZ() const;
    std::vector<CAN::byte_t> CAN_PWR_125_MHZ() const;
    std::vector<CAN::byte_t> CAN_PWR_2_GHZ() const;
    std::vector<CAN::byte_t> CAN_LASER_CURRENT() const;
    std::vector<CAN::byte_t> CAN_VDC_MINUS_5() const;
    std::vector<CAN::byte_t> CAN_VDC_15() const;
    std::vector<CAN::byte_t> CAN_STATUS() const;
    
    /** Helper function for 'set' commands to check that data size
     ** is valid, if not throw exception
     ** @param data Vector of bytes containing data to check
     ** @param size Expected number of bytes in data std::vector
     ** @param pExceptionMsg String identifying CAN msg that is
     ** associated with failed size check.
     */
    void checkSize( const std::vector<CAN::byte_t> &data, int size,
		    const char *pExceptionMsg);
    
    /// \name Undefined and inaccessible - copying does not make sense.
    //@{
    CRD (const CRD &);
    CRD &operator=(const CRD &);
    //@}
    
    /// Bus location (node number).
    node_t m_node;

    /// Serial number of this node.
    std::vector<CAN::byte_t> m_sn;
    
    /// Number of transactions. Needs to be mutable as its incremented by the
    /// monitor function, which is const.
    mutable unsigned int m_trans_num;
    // values for all the monitor points
    CAN::byte_t m_errorCode;

    // start time
    timeval start_m;

    // values for all the monitor points
    unsigned long long RESET_TIME_m;
    double             EFC_5_MHZ_m;
    double             PWR_5_MHZ_m;
    double             PWR_25_MHZ_m;
    double             PWR_125_MHZ_m;
    double             PWR_2_GHZ_m;
    double             LASER_CURRENT_m;
    double             VDC_MINUS_5_m;
    double             VDC_15_m;
    CAN::byte_t        STATUS_m;
  };  // class CRD
}  // namespace AMB

#endif  // CRD_H
