// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include <unistd.h>
#include "ACUAECDrive.h"

//------------------------------------------------------------------------------

ACUAECDrive::ACUAECDrive() :
    active_m(false)
{

    azStatus_m.raw[0] = Az.limit;
    azStatus_m.raw[1] = 0;
    azStatus_m.raw[2] = 0;
    azStatus_m.raw[3] = 0;
    azStatus_m.raw[4] = 0;
    azStatus_m.raw[5] = static_cast<unsigned char>(0x02);
    azStatus_m.raw[6] = 0;
    azStatus_m.raw[7] = 0;

    elStatus_m.raw[0] = El.limit;
    elStatus_m.raw[1] = 0;
    elStatus_m.raw[2] = 0;
    elStatus_m.raw[3] = 0;
    elStatus_m.raw[4] = 0;
    elStatus_m.raw[5] = static_cast<unsigned char>(0x02);
    elStatus_m.raw[6] = 0;
    elStatus_m.raw[7] = 0;

    Az.debug_level = 0;
    El.debug_level = 0;

    Az.limits(AZ_LOWER_LIMIT,AZ_UPPER_LIMIT);
    El.limits(EL_LOWER_LIMIT,EL_UPPER_LIMIT);

    Az.stows(AZ_MAINT_STOW,AZ_SURVL_STOW);
    El.stows(EL_MAINT_STOW,EL_SURVL_STOW);

    // Start thread to read CAN messages and stick them in queue.
    active_m = true;
    if(pthread_create(&tid,
		      NULL,
		      &ACUAECDrive::updateLoop,
		      static_cast<void*>(this))!=0)
	{
	active_m = false;
	}
}

//------------------------------------------------------------------------------

ACUAECDrive::~ACUAECDrive() 
{     
    if(active_m)
	{
	active_m = false;
	pthread_join(tid, NULL); 
	}
}

//------------------------------------------------------------------------------

int ACUAECDrive::setAzPsn(double position,double velocity)
{
    return Az.set(position,velocity);
}

//------------------------------------------------------------------------------

int ACUAECDrive::setElPsn(double position,double velocity)
{
    return El.set(position,velocity);
}

//------------------------------------------------------------------------------

void ACUAECDrive::getAzPsn(double& position,double& velocity) const
{
    double accel;
    Az.get_cur(position,velocity,accel);
}

//------------------------------------------------------------------------------

void ACUAECDrive::getElPsn(double& position,double& velocity) const
{
    double accel;
    El.get_cur(position,velocity,accel);
}

//------------------------------------------------------------------------------

void ACUAECDrive::setAzStatus(unsigned char status)
{
    azStatus_m.raw[5] &= status;
}
//------------------------------------------------------------------------------

void ACUAECDrive::getAzStatus(unsigned long long& status) const
{

    status = azStatus_m.Long;
#if 0
    unsigned char* ptr = (unsigned char*)&status;
    
    ptr[0] = Az.limit;
    ptr[1] = 0;
    ptr[2] = 0;
    ptr[3] = 0;
    ptr[4] = 0;
    ptr[5] = 0;
    ptr[6] = 0;
    ptr[7] = 0;
#endif
}

//------------------------------------------------------------------------------

void ACUAECDrive::setElStatus(unsigned char status)
{
    elStatus_m.raw[5] &= status;
}

void ACUAECDrive::getElStatus(unsigned long long& status) const
{
    status = elStatus_m.Long;
#if 0
    unsigned char* ptr = (unsigned char*)&status;
    
    ptr[0] = El.limit;
    ptr[1] = 0;
    ptr[2] = 0;
    ptr[3] = 0;
    ptr[4] = 0;
    ptr[5] = 0;
    ptr[6] = 0;
    ptr[7] = 0;
#endif
}

//------------------------------------------------------------------------------

std::vector<char> ACUAECDrive::getACUMode() const
{
    std::vector<char> rtnVal(2);
    rtnVal[0] = (char)El.get_mode();
    rtnVal[0] <<= 4;
    rtnVal[0] |= (char)Az.get_mode();

    rtnVal[1] = ACU_REMOT;  // always remote
    return rtnVal;
}

//------------------------------------------------------------------------------

int ACUAECDrive::setACUMode(unsigned char mode)
{
    int rc = Az.set_mode((AEC_AZEL_Mode_t)(mode & 0xF));
    mode >>= 4;
    rc |= El.set_mode((AEC_AZEL_Mode_t)(mode & 0xF));
    return rc;	// 0-OK, 1-ERROR
}

//------------------------------------------------------------------------------

void ACUAECDrive::handler()
{
    Az.tick();
    El.tick();
}

//------------------------------------------------------------------------------

// This function is static to be the pthread_create() argument.  A pointer to 
// the class object is passed to allow access to its variables and methods().
void* ACUAECDrive::updateLoop(void* data)
{
    ACUAECDrive* This = static_cast<ACUAECDrive*>(data);
    while(This->active_m)
	{
	usleep((int)(1000000 * TIME_UNIT));
	This->handler();
	}
    pthread_exit(NULL);
}
