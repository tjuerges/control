// @(#) $Id$


#include <queue>
#include <iomanip>
#include <sys/time.h>
#include <stdio.h>
#include "ACUACASim.h"
#include "ACUACADrive.h"
#include <libiberty.h>

using namespace ACAacu;

const double bitsPerDegree = ((double)0x40000000 / 180.0);
const double degreesPerBit = (180.0 / (double)0x40000000);
const uint16_t IST_DEFAULT = 5 * 60;    // idle_stow_time default


/*
 * ACU error stack (duplicates operation of ACU monitor function)
 */
class ACUErrorStack
{
  public:
    // structure to hold error code to be assembled into the byte sequence
    typedef struct error_code {
        uint8_t error;
        uint32_t rca;
    } ERROR_CODE;

    void push_error(uint8_t error,uint32_t address)
    {
        /*
         * byte 0 is the error status, and then 4 bytes for the RCA
         * 5 bytes total
         */
        ERROR_CODE ec;
        ec.error = error;
        ec.rca = address;
        m_es.push(ec);
    }
    ERROR_CODE pop_error()
    {
        ERROR_CODE ec = { 0, 0 };
        if (m_es.empty())
            return ec;
    
        ec = m_es.front();
        m_es.pop();
        return ec;
    }
private:
    std::queue<ERROR_CODE> m_es;
};

static ACUErrorStack acu_errs;

// -----------------------------------------------------------------------------

ACUSim::ACUSim()
    : idle_stow_time_m(IST_DEFAULT)
{
    drive_m = new ACUDrive();
    status_m = new ACUSimStatus();
}

// -----------------------------------------------------------------------------

ACUSim::~ACUSim()
{
    if (drive_m)
	delete drive_m;
    if (status_m)
	delete status_m;
}


////////////////////////////////////////////////////////////////////////////////
////                                                                        ////
////                            Monitor Points                              ////
////                                                                        ////
////////////////////////////////////////////////////////////////////////////////
      
// -----------------------------------------------------------------------------

std::vector<uint8_t> ACUSim::get_acu_mode_rsp() const
{
    return drive_m->getACUMode();
}
uint8_t ACUSim::acu_trk_mode_rsp() const
{
    return acu_trk_mode_m;
}

// -----------------------------------------------------------------------------

void ACUSim::get_az_posn_rsp(
	int32_t &pos_at_TE,
	int32_t &pos_at_intm) const
{
    double pos, vel;
    long position;

    // the acu simulator does not have the position for the time between
    // timing events.
    drive_m->getAzPsn(pos,vel);
    position = (long)(pos * bitsPerDegree);
    pos_at_TE = position;
    pos_at_intm = position;
}
void ACUSim::get_el_posn_rsp(
	int32_t &pos_at_TE,
	int32_t &pos_at_intm) const
{
    double pos, vel;
    long position;

    // the acu simulator does not have the position for the time between
    // timing events.
    drive_m->getElPsn(pos,vel);
    position = (long)(pos * bitsPerDegree);
    pos_at_TE = position;
    pos_at_intm = position;
}

// -----------------------------------------------------------------------------

void ACUSim::get_acu_error(uint8_t &error, uint32_t &rca) const
{
    ACUErrorStack::ERROR_CODE ec = acu_errs.pop_error();
    error = ec.error;
    rca   = ec.rca;
}

// == Az El ================================================================

void ACUSim::get_az_traj_cmd(int32_t &pos, int32_t &vel) const
{
    pos = az_traj_cmd_m[0];
    vel = az_traj_cmd_m[1];
}
void ACUSim::get_el_traj_cmd(int32_t &pos, int32_t &vel) const
{
    pos = el_traj_cmd_m[0];
    vel = el_traj_cmd_m[1];
}

// -----------------------------------------------------------------------------

uint8_t ACUSim::get_az_brake() const
{
    return (uint8_t)drive_m->Az.get_brake();
}
uint8_t ACUSim::get_el_brake() const
{
    return (uint8_t)drive_m->El.get_brake();
}

// -----------------------------------------------------------------------------

uint32_t ACUSim::get_az_enc() const
{
//    return get_az_posn_rsp();
    return 0;
}
uint32_t ACUSim::get_el_enc() const
{
//    return get_el_posn_rsp();
    return 0;
}

// -----------------------------------------------------------------------------

void ACUSim::get_az_motor_currents(az_motor_st& st) const
{
    st.AZ_L1 = 0; st.AZ_L2 = 0;
    st.AZ_R1 = 0; st.AZ_R2 = 0;
}
void ACUSim::get_az_motor_temps(az_motor_st& st) const
{
    st.AZ_L1 = 255; st.AZ_L2 = 255;
    st.AZ_R1 = 255; st.AZ_R2 = 255;
}
void ACUSim::get_az_motor_torque(az_motor_st& st) const
{
    st.AZ_L1 = 0; st.AZ_L2 = 0;
    st.AZ_R1 = 0; st.AZ_R2 = 0;
}

void ACUSim::get_el_motor_currents(struct el_motor_st& st) const
{
    st.EL_L = 0;
    st.EL_R = 0;
}
void ACUSim::get_el_motor_temps(struct el_motor_st& st) const
{
    st.EL_L = 255;
    st.EL_R = 255;
}
void ACUSim::get_el_motor_torque(struct el_motor_st& st) const
{
    st.EL_L = 0;
    st.EL_R = 0;
}

// -----------------------------------------------------------------------------

#define check_range(i, max) \
	if (i < 0 || (int)max <= i) { printf("check_range.\n"); exit(1); }

double ACUSim::get_az_servo_coeff(int n) const
{
    check_range(n, 16)
    return drive_m->Az.servo_coefficients[n];
}
double ACUSim::get_el_servo_coeff(int n) const
{
    check_range(n, 16)
    return drive_m->El.servo_coefficients[n];
}

// -----------------------------------------------------------------------------

std::vector<uint8_t> ACUSim::get_az_status() const
{
    std::vector<uint8_t> status = drive_m->getAzStatus();
    return status;
}
std::vector<uint8_t> ACUSim::get_az_status_2() const
{
    std::vector<uint8_t> status = drive_m->getAzStatus2();
    return status;
}

std::vector<uint8_t> ACUSim::get_el_status() const
{
    std::vector<uint8_t> status = drive_m->getElStatus();
    return status;
}
std::vector<uint8_t> ACUSim::get_el_status_2() const
{
    std::vector<uint8_t> status = drive_m->getElStatus2();
    return status;
}

// -----------------------------------------------------------------------------

uint32_t ACUSim::get_az_encoder_offset() const
{
    return az_encoder_offset_m;
}
uint32_t ACUSim::get_el_encoder_offset() const
{
    return el_encoder_offset_m;
}

// -----------------------------------------------------------------------------

uint8_t ACUSim::get_az_aux() const
{
    return az_aux_m;
}
uint8_t ACUSim::get_el_aux() const
{
    return el_aux_m;
}

uint8_t ACUSim::get_az_ratefdbk_mode() const
{
    return az_ratefdbk_mode_m;
}
uint8_t ACUSim::get_el_ratefdbk_mode() const
{
    return el_ratefdbk_mode_m;
}

// == Pointing, ACU config , status ======================================================

void ACUSim::get_ip_address(uint32_t& addr, uint32_t& mask) const
{
    addr = ip_address;
    mask = ip_netmask;
}
void ACUSim::get_ip_gateway(uint32_t& gateway) const
{
    gateway = ip_gateway;
}

// -----------------------------------------------------------------------------

std::vector<uint8_t> ACUSim::get_system_status() const
{
    std::vector<uint8_t> status(8);
    return status;
}
std::vector<uint8_t> ACUSim::get_system_status_2() const
{
    std::vector<uint8_t> status(8);
    return status;
}

// -----------------------------------------------------------------------------

double ACUSim::get_pt_model_coeff(const int n) const
{
    check_range(n, ARRAY_SIZE(pt_model_coeff_m))
    return pt_model_coeff_m[n];
}

// -----------------------------------------------------------------------------

uint16_t ACUSim::get_idle_stow_time() const
{
    return idle_stow_time_m;
}
uint8_t ACUSim::get_shutter() const
{
    return shutter_m;
}
void ACUSim::get_stow_pin(uint8_t &az, uint8_t &el) const
{
    az = drive_m->Az.get_stow_pin();
    el = drive_m->El.get_stow_pin();
}

// == SUBREF ===================================================================

uint8_t ACUSim::subref_mode_rsp() const
{
    return subref_mode_m;
}

// -----------------------------------------------------------------------------

std::vector<int16_t> ACUSim::get_subref_abs_posn() const
{
    std::vector<int16_t> temp(3);
    temp[0] = subref_abs_posn_m[0];
    temp[1] = subref_abs_posn_m[1];
    temp[2] = subref_abs_posn_m[2];
    return temp;
}
std::vector<int16_t> ACUSim::get_subref_delta_posn() const
{
    std::vector<int16_t> temp(3);
    temp[0] = subref_delta_posn_m[0];
    temp[1] = subref_delta_posn_m[1];
    temp[2] = subref_delta_posn_m[2];
    return temp;
}
std::vector<int16_t> ACUSim::get_subref_rotation() const
{
    std::vector<int16_t> temp(3);
    temp[0] = subref_rotation_m[0];
    temp[1] = subref_rotation_m[1];
    temp[2] = subref_rotation_m[2];
    return temp;
}
std::vector<int16_t> ACUSim::get_sr_encoder_posn_1() const
{
    std::vector<int16_t> temp(3);
    temp[0] = subref_encoder_posn_m[0];
    temp[1] = subref_encoder_posn_m[1];
    temp[2] = subref_encoder_posn_m[2];
    return temp;
}
std::vector<int16_t> ACUSim::get_sr_encoder_posn_2() const
{
    std::vector<int16_t> temp(3);
    temp[0] = subref_encoder_posn_m[3];
    temp[1] = subref_encoder_posn_m[4];
    temp[2] = subref_encoder_posn_m[5];
    return temp;
}

// -----------------------------------------------------------------------------

std::vector<uint8_t> ACUSim::get_subref_limits() const
{
    std::vector<uint8_t> limits(8);
    return limits;
}

// -----------------------------------------------------------------------------

std::vector<uint8_t> ACUSim::get_subref_status() const
{
    std::vector<uint8_t> status(8);
    return status;
}

// -----------------------------------------------------------------------------

double ACUSim::get_sr_pt_coeff(const int n) const
{
    check_range(n, ARRAY_SIZE(subref_pt_coeff_m))
    return subref_pt_coeff_m[n];
}

// == Metrology etc. ===========================================================

std::vector<uint8_t> ACUSim::get_metr_mode() const
{
    std::vector<uint8_t> temp(4);
    for (unsigned int i = 0; i < ARRAY_SIZE(metr_mode_m); i++) {
        temp[i] = metr_mode_m[i];
    }
    return temp;
}

std::vector<uint8_t> ACUSim::get_metr_equip_status() const
{
    std::vector<uint8_t> temp(8);
    for (unsigned int i = 0; i < ARRAY_SIZE(metr_equip_status_m); i++) {
        temp[i] = metr_equip_status_m[i];
    }
    return temp;
}

// -----------------------------------------------------------------------------

int32_t ACUSim::get_metr_displ(const int n) const
{
    return metr_displ_m[n];
}

std::vector<int16_t> ACUSim::get_metr_temps(const int n) const
{
    std::vector<int16_t> temp(4);
    temp[0] = metr_temps_m[n*4];
    temp[1] = metr_temps_m[n*4 + 1];
    temp[2] = metr_temps_m[n*4 + 2];
    temp[3] = metr_temps_m[n*4 + 3];
    return temp;
}

void ACUSim::get_metr_tilt(const int n, metr_tilt &tp) const
{
    tp.x = metr_tilt_m[n].x;
    tp.y = metr_tilt_m[n].y;
    tp.t = metr_tilt_m[n].t;
}

// -----------------------------------------------------------------------------

double ACUSim::get_metr_coeff(const int n) const
{
    check_range(n, ARRAY_SIZE(metr_coeff_m))
    return metr_coeff_m[n];
}

// -----------------------------------------------------------------------------

void ACUSim::get_metr_deltas(int32_t& az, int32_t& el) const
{
    az = 1;
    el = 2;
}

int32_t ACUSim::get_metr_deltapath() const
{
    return metr_deltapath_m;
}

// == antenna status etc. ======================================================

std::vector<uint8_t> ACUSim::get_power_status() const
{
    std::vector<uint8_t> status(3);
    return status;
}
uint8_t ACUSim::get_ac_status() const
{
    return 0x00;
}
std::vector<uint8_t> ACUSim::get_fan_status() const
{
    std::vector<uint8_t> temp(6);
    return temp;
}
uint8_t ACUSim::get_ac_temp() const
{
    return ac_temperature_m;
}
void ACUSim::get_antenna_temps(antenna_temps &temps) const
{
    temps.receivercabin = antenna_temps_m.receivercabin;
    temps.pedestal      = antenna_temps_m.pedestal;
}

// -----------------------------------------------------------------------------

std::vector<uint16_t> ACUSim::get_ups_output_volts_1() const
{
    std::vector<uint16_t> data(3);
    data[0] = 11;
    data[1] = 12;
    data[2] = 13;
    return data;
}
std::vector<uint16_t> ACUSim::get_ups_output_volts_2() const
{
    std::vector<uint16_t> data(3);
    data[0] = 21;
    data[1] = 22;
    data[2] = 23;
    return data;
}

std::vector<uint16_t> ACUSim::get_ups_output_current_1() const
{
    std::vector<uint16_t> data(3);
    data[0] = 11;
    data[1] = 12;
    data[2] = 13;
    return data;
}
std::vector<uint16_t> ACUSim::get_ups_output_current_2() const
{
    std::vector<uint16_t> data(3);
    data[0] = 21;
    data[1] = 22;
    data[2] = 23;
    return data;
}

// -----------------------------------------------------------------------------

void	ACUSim::selftest_rsp(selftest_rsp_param &data) const
{
    data.status = 0x0;
    data.failed_test_latest = 1115;
    data.num_of_errors = 1;
}
void	ACUSim::selftest_err(selftest_err_param &data) const
{
    data.failed_test_index = 1115;
    data.param = 111.5f;
}


////////////////////////////////////////////////////////////////////////////////
////                                                                        ////
////                              Control Points                            ////
////                                                                        ////
////////////////////////////////////////////////////////////////////////////////

// -----------------------------------------------------------------------------

void ACUSim::acu_mode_cmd(const uint8_t mode)
{
    if (drive_m->setACUMode(mode) != 0)
	acu_errs.push_error(COMMAND_NOT_ALLOWED, ACAacu::ACU_MODE_CMD);
}
void ACUSim::acu_trk_mode_cmd(const uint8_t mode)
{
    acu_trk_mode_m = mode;
}

// -----------------------------------------------------------------------------

void ACUSim::az_traj_cmd(
	const int32_t position,
	const int32_t velocity
	)
{
    // need to remember the time this command came in to later
    // predict az position.  the equations are kept in binary parts
    // of a circle.
    //
    // later, az encoder = x0 + v * dt + a * dt * dt / 2 where dt is
    // time since az traj last commanded
    // this is all integer math.  to more accurately represent the encoders,
    // the az encoder value should be masked to produce the upper 26 bits,
    // since the encoders are 26 bits for a 720 degree range.
    //
    // the maximum velocity is 6 deg/sec, and the maximum acceleration is 12
    // deg/sec/sec.
    //

    double pos = (double)(position) * degreesPerBit;
    double vel = (double)(velocity) * degreesPerBit;
    uint8_t err = 0;

    switch (drive_m->setAzPsn(pos,vel)) {
    case 1:
	err = COMMAND_NOT_ALLOWED;
	break;
    case 2:
	err = COMMAND_NOT_ALLOWED;
	break;
    case 3:
	err = COMMAND_NOT_ALLOWED;
	break;
    case 4:
	err = COMMAND_NOT_ALLOWED;
	break;
    default:
	az_traj_cmd_m[0] = position;
	az_traj_cmd_m[1] = velocity;
	break;
    }
    if (err) {
	acu_errs.push_error(err, ACAacu::AZ_TRAJ_CMD);
    }
}

// -----------------------------------------------------------------------------

void ACUSim::el_traj_cmd(
	const int32_t position,
	const int32_t velocity
	)
{
    double pos = (double)(position) * degreesPerBit;
    double vel = (double)(velocity) * degreesPerBit;
    uint8_t err = 0;

    switch (drive_m->setElPsn(pos,vel)) {
    case 1:
	err = COMMAND_NOT_ALLOWED;
	break;
    case 2:
	err = COMMAND_NOT_ALLOWED;
	break;
    case 3:
	err = COMMAND_NOT_ALLOWED;
	break;
    case 4:
	err = COMMAND_NOT_ALLOWED;
	break;
    default:
	el_traj_cmd_m[0] = position;
	el_traj_cmd_m[1] = velocity;
	break;
    }
    if (err) {
	acu_errs.push_error(err, ACAacu::EL_TRAJ_CMD);
    }
}

// -----------------------------------------------------------------------------

void ACUSim::clear_fault_cmd()
{
}

void ACUSim::reset_acu_cmd()
{
}

// == Az El control ================================================================

void ACUSim::set_az_brake(int mode)
{
    if (drive_m->Az.set_brake((AZEL_Brake_t)mode))
        acu_errs.push_error(COMMAND_NOT_ALLOWED, ACAacu::SET_AZ_BRAKE);
}
void ACUSim::set_el_brake(int mode)
{
    if (drive_m->El.set_brake((AZEL_Brake_t)mode))
        acu_errs.push_error(COMMAND_NOT_ALLOWED, ACAacu::SET_EL_BRAKE);
}

// -----------------------------------------------------------------------------

void ACUSim::set_az_servo_coeff(const int n, const double value)
{
    check_range(n, 16)
    drive_m->Az.servo_coefficients[n] = value;
}
void ACUSim::set_el_servo_coeff(const int n, const double value)
{
    check_range(n, 16)
    drive_m->El.servo_coefficients[n] = value;
}

void ACUSim::set_az_servo_default()
{
    drive_m->Az.set_servo_defaults();
}
void ACUSim::set_el_servo_default()
{
    drive_m->El.set_servo_defaults();
}

// -----------------------------------------------------------------------------

void ACUSim::set_az_aux(const uint8_t mode)
{
    az_aux_m = mode;
}
void ACUSim::set_el_aux(const uint8_t mode)
{
    el_aux_m = mode;
}

void ACUSim::set_az_ratefdbk_mode(const uint8_t mode)
{
    az_ratefdbk_mode_m = mode;
}
void ACUSim::set_el_ratefdbk_mode(const uint8_t mode)
{
    el_ratefdbk_mode_m = mode;
}

// == Pointing =============================================================

void ACUSim::set_pt_model_coeff(const int n, const double value)
{
    check_range(n, ARRAY_SIZE(pt_model_coeff_m))
    pt_model_coeff_m[n] = value;
}

void ACUSim::set_pt_default()
{
    for (unsigned int i = 0; i < ARRAY_SIZE(pt_model_coeff_m); i++) {
        pt_model_coeff_m[i] = 0;
    }
}

// == SUBREF ===================================================================

void ACUSim::subref_mode_cmd(const uint8_t mode)
{
    subref_mode_m = mode;
}

// -----------------------------------------------------------------------------

void ACUSim::set_subref_abs_posn(const std::vector<int16_t> pos)
{
    subref_abs_posn_m[0] = pos[0];
    subref_abs_posn_m[1] = pos[1];
    subref_abs_posn_m[2] = pos[2];
}
void ACUSim::set_subref_delta_posn(const std::vector<int16_t> pos)
{
    subref_delta_posn_m[0] += pos[0];
    subref_delta_posn_m[1] += pos[1];
    subref_delta_posn_m[2] += pos[2];
}
void ACUSim::subref_delta_zero_cmd(const uint8_t value)
{
    subref_abs_posn_m[0] = 0;
    subref_abs_posn_m[1] = 0;
    subref_abs_posn_m[2] = 0;
    subref_delta_posn_m[0] = 0;
    subref_delta_posn_m[1] = 0;
    subref_delta_posn_m[2] = 0;
}
void ACUSim::set_subref_rotation(const std::vector<int16_t> pos)
{
    subref_rotation_m[0] = pos[0];
    subref_rotation_m[1] = pos[1];
    subref_rotation_m[2] = pos[2];
}

// -----------------------------------------------------------------------------

void ACUSim::set_sr_pt_coeff(const int n, const double value)
{
    check_range(n, ARRAY_SIZE(subref_pt_coeff_m))
    subref_pt_coeff_m[n] = value;
}
void ACUSim::set_sr_pt_default()
{
    for (unsigned int i = 0; i < ARRAY_SIZE(subref_pt_coeff_m); i++) {
	subref_pt_coeff_m[i] = 0;
    }
}

// == Metrology etc. ===========================================================

void ACUSim::set_metr_mode(const std::vector<uint8_t> mode)
{
    metr_mode_m[0] = mode[0];
}

// -----------------------------------------------------------------------------

void ACUSim::set_metr_coeff(const int n, const double value)
{
    check_range(n, ARRAY_SIZE(metr_coeff_m))
    metr_coeff_m[n] = value;
}
void ACUSim::set_metr_default()
{
    for (int i = 0; i < 32; i++) {
        metr_coeff_m[i] = 0;
    }
}

// -----------------------------------------------------------------------------

void ACUSim::set_metr_calibration()
{
}

// == ACU & Antenna config etc. ==========================================================

void ACUSim::set_ip_address(const uint32_t addr, const uint32_t mask)
{
    ip_address = addr;
    ip_netmask = mask;
}
void ACUSim::set_ip_gateway(const uint32_t gateway)
{
    ip_gateway = gateway;
}

// -----------------------------------------------------------------------------

void ACUSim::set_idle_stow_time(const uint16_t seconds)
{
    idle_stow_time_m = seconds;
}
void ACUSim::set_shutter(const uint8_t value)
{
    shutter_m = value;
}
void ACUSim::set_stow_pin(const uint8_t az, const uint8_t el)
{
    drive_m->Az.set_stow_pin((Stow_Pin_t)az);
    drive_m->El.set_stow_pin((Stow_Pin_t)el);
}

// -----------------------------------------------------------------------------

void ACUSim::set_ac_temp(const uint8_t temperature)
{
    // 18..22 degC
    ac_temperature_m = temperature;
}

// -----------------------------------------------------------------------------

void	ACUSim::selftest_cmd(const uint8_t mode)
{
}

////////////////////////////////////////////////////////////////////////////////
////                                                                        ////
////                         Generic Monitor Points                         ////
////                                                                        ////
////////////////////////////////////////////////////////////////////////////////
      
// -----------------------------------------------------------------------------

std::vector<uint8_t> ACUSim::get_sw_rev_level() const
{
    std::vector<uint8_t> tmp(3);
    tmp[0] = 1;	// major
    tmp[1] = 2;	// minor
    tmp[2] = 3;	// patch
    return tmp;
}

// -----------------------------------------------------------------------------

uint32_t ACUSim::get_can_error() const
{
    return 0;
}

// -----------------------------------------------------------------------------

uint32_t ACUSim::get_num_trans() const
{
    return 0;
}

// -----------------------------------------------------------------------------

std::vector<int8_t> ACUSim::get_system_id() const
{
    // Notice ACU version has extra status byte compared to mandatory version.
    std::vector<int8_t> tmp(3);
    tmp[0] = 0xAB;      // major
    tmp[1] = 0xCD;      // minor
    tmp[2] = 0xEF;      // release
//    tmp[3] = 0x87;      // status byte
    /*
     * Byte 3 -
     * bit 0: encoder interface board detected    
     * bit 1: CAN bus interface board (to servo amplifiers) detected  
     * bit 2: dig/ana I/O board detected    
     * bit 3-6:    
     * bit 7: Ethernet interface (on CPU) detected
     */
    return tmp;
}

// -----------------------------------------------------------------------------

void	ACUSim::set_errcode(int code) const
{
    status_m->errcode = code;
}
int	ACUSim::get_errcode() const
{
    return status_m->errcode;
}
void	ACUSim::set_timestamp() const
{
    const uint64_t timeFor1970 = 122192928000000000ULL;
    struct timeval tv;
    gettimeofday(&tv, 0);
    status_m->timestamp = timeFor1970 + tv.tv_sec * 10000000LL + tv.tv_usec * 10LL;
}
uint64_t ACUSim::get_timestamp() const
{
    return status_m->timestamp;
}

// -----------------------------------------------------------------------------
