// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include <queue>
#include <iomanip>
#include "AMBUtil.h"
#include "ACUSim.h"
#include "CANError.h"
#include "ACU.h"

using AMB::node_t;
using AMB::rca_t;

// -----------------------------------------------------------------------------

AMB::ACU::ACU(
        node_t node,
	const std::vector<CAN::byte_t>& serialNumber)
    : node_m(node),sn_m(serialNumber)
{
    std::cout << "Creating node " << node << ", s/n 0x";
    for (int i = 0; i < 8; i++)
	std::cout << std::hex << std::setw(2) << std::setfill('0') << (int)serialNumber[i];
    std::cout << ", ACU device" << std::endl;

    std::string error;
    if ( ! validateNodeNumber(error,this->node()))
	{
	    throw ACUError("Unknown RCA in monitor command");
//	throw CAN::Error(error);
	}

    if ( ! validateSerialNumber(error,this->serialNumber()))
	{
	    throw ACUError("Unknown RCA in monitor command");
//	throw CAN::Error(error);
	}
}

// -----------------------------------------------------------------------------

AMB::ACU::~ACU()
{
    // Nothing
}

// -----------------------------------------------------------------------------

node_t AMB::ACU::node() const
{
    return node_m;
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::ACU::serialNumber() const
{
    return sn_m;
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::ACU::monitor(
    rca_t rca) const
{
    // empty CAN message
    const std::vector<CAN::byte_t> nostring;

    // these are dummies to allow calling ICD methods
    int errcode;
    unsigned long long timestamp;

    // bump transaction count
    trans_num_m++;

    switch (rca & 0x3FFFF)      // rca is first 18 bits
	{
	
	// mandatory standard generic monitor points
	
	case GET_PROTOCOL_REV_LEVEL:  // aka GET_SW_REV_LEVEL in ICD 9
	    return get_protocol_rev_level();
	    
	case AMB::Device::GET_HW_REV_LEVEL:
	    return get_hardware_rev_level();
	    
	case GET_CAN_ERROR:
	    return get_can_error();
	    
	case GET_TRANS_NUM:
	    return longToData(nostring,get_trans_num());
	    
	case GET_SW_REV_LEVEL:  // aka GET_SYSTEM_ID in ICD 9
	    return get_sw_rev_level();
	    
	    // ACU specific monitor points (order from AMBSimACU.h)
	    
	case ACUbase::ACUbase::ACU_MODE_RSP:
	{
	std::vector<char> mode = sim_m.get_acu_mode_rsp(errcode,timestamp);
	std::vector<CAN::byte_t> tmps = charToData(nostring,mode[0]);
	tmps = charToData(tmps,mode[1]);
	return tmps;
	}
	
        case ACUbase::AZ_POSN_RSP:
	{
	unsigned long long rsp = sim_m.get_az_posn_rsp(errcode,timestamp);
	// actually two ints, so keep int order, but convert to network
	// order the two ints.
	unsigned long* ptr = (unsigned long*)&rsp;
	std::vector<CAN::byte_t> tmps = longToData(nostring,ptr[0]);
	return longToData(tmps,ptr[1]);
	}
	
	case ACUbase::EL_POSN_RSP:
	{
	unsigned long long rsp = sim_m.get_el_posn_rsp(errcode,timestamp);
	// actually two ints, so keep int order, but convert to network
	// order the two ints.
	unsigned long* ptr = (unsigned long*)&rsp;
	std::vector<CAN::byte_t> tmps = longToData(nostring,ptr[0]);
	return longToData(tmps,ptr[1]);
	}
	
        case ACUbase::GET_ACU_ERROR:
	{
	// 0 bytes implies no error
	unsigned long long rsp = sim_m.get_acu_error(errcode,timestamp);
	
	if(rsp == 0L)
	    {
	    // 0 bytes sent implies no error
	    std::vector<CAN::byte_t> tmps(0);
	    return tmps;
	    }
	else
	    {
	    // stuff error code into byte 0
	    std::vector<CAN::byte_t> tmps = longlongToData(nostring,rsp,1);
	    
	    // extract the next 4 bytes as an integer in host byte order
	    unsigned char* ptr = (unsigned char*)&rsp;
	    unsigned long rca;
	    memcpy(&rca,ptr+1,4);
	    
	    // now convert host byte order to network order, pack it into the
	    // buffer, and return the buffer.
	    return longToData(tmps,rca);
	    }
	}
	
	case ACUbase::GET_AZ_AUX_MODE:
	    return longlongToData(nostring,
				  sim_m.get_az_aux_mode(errcode,timestamp),1);
	    
	case ACUbase::GET_AZ_BRAKE:
	    return longlongToData(nostring,
				  sim_m.get_az_brake(errcode,timestamp),1);
	    
	case ACUbase::GET_AZ_ENC:
	    return longToData(nostring,sim_m.get_az_enc(errcode,timestamp));
	    
	case ACUbase::GET_AZ_ENC_STATUS:
            return charToData(nostring,
			      sim_m.get_az_enc_status(errcode,timestamp));
	    
	case ACUbase::GET_AZ_MOTOR_CURRENTS:                         
	    // m1, m2
	    // 48, 64 Amps
// !!! these are flipped !!!
	    return longlongToData(nostring,0x4030,2);     
	    
	case ACUbase::GET_AZ_MOTOR_TEMPS:
	    // m1, m2
	    // -34, -18 C
// !!! these are flipped !!!
	    return longlongToData(nostring,0x2010,2);     
	    
	case ACUbase::GET_AZ_MOTOR_TORQUE:
	    // m1, m2
	    // 80, 96 Nm
// !!! these are flipped !!!
	    return longlongToData(nostring,0x6050,2);     
	    
	case ACUbase::GET_AZ_SERVO_COEFF_0:
	case ACUbase::GET_AZ_SERVO_COEFF_0 + 1:
	case ACUbase::GET_AZ_SERVO_COEFF_0 + 2:
	case ACUbase::GET_AZ_SERVO_COEFF_0 + 3:
	case ACUbase::GET_AZ_SERVO_COEFF_0 + 4:
	case ACUbase::GET_AZ_SERVO_COEFF_0 + 5:
	case ACUbase::GET_AZ_SERVO_COEFF_0 + 6:
	case ACUbase::GET_AZ_SERVO_COEFF_0 + 7:
	case ACUbase::GET_AZ_SERVO_COEFF_0 + 8:
	case ACUbase::GET_AZ_SERVO_COEFF_0 + 9:
	case ACUbase::GET_AZ_SERVO_COEFF_0 + 10:
	case ACUbase::GET_AZ_SERVO_COEFF_0 + 11:
	case ACUbase::GET_AZ_SERVO_COEFF_0 + 12:
            return doubleToData(
		sim_m.get_az_servo_coeff(
		    rca - ACUbase::GET_AZ_SERVO_COEFF_0,errcode,timestamp));
	    
	case ACUbase::GET_AZ_STATUS:
	{
	unsigned long long stat = sim_m.get_az_status(errcode,timestamp);
	unsigned char* stat_p = (unsigned char*)&stat;
	std::vector<CAN::byte_t> tmps = charToData(nostring,stat_p[0]);
	for (int i = 1; i < 8; i++)
	    tmps = charToData(tmps,stat_p[i]);
	return tmps;
	}
	
	case ACUbase::GET_EL_AUX_MODE:
	    return longlongToData(nostring,
				  sim_m.get_el_aux_mode(errcode,timestamp),1);

	case ACUbase::GET_EL_BRAKE:
	    return longlongToData(nostring,
				  sim_m.get_el_brake(errcode,timestamp),1);
	    
	case ACUbase::GET_EL_ENC:
	{
	std::vector<long> positions(2);
	sim_m.get_el_enc(positions,errcode,timestamp);
	std::vector<CAN::byte_t> tmps = longToData(nostring,positions[0]);
	return longToData(tmps,positions[1]);
	}
	
	case ACUbase::GET_EL_ENC_STATUS:
	{
	std::vector<char> status = sim_m.get_el_enc_status(errcode,timestamp);
	std::vector<CAN::byte_t> tmps = charToData(nostring,status[0]);
	tmps = charToData(tmps,status[1]);
	return charToData(tmps,status[2]);
	}
	
	case ACUbase::GET_EL_MOTOR_CURRENTS:                         
	    // m1, m2, m3, m4
	    // 176, 192, 208, 224 Amps 
// !!! these are flipped !!!
	    return longlongToData(nostring,0xE0D0C0B0,4);
	    
	case ACUbase::GET_EL_MOTOR_TEMPS:                            
	    // m1, m2, m3, m4
	    // 62, 78, 94, 110 C
// !!! these are flipped !!!
	    return longlongToData(nostring,0xA0908070,4); 
	    
	case ACUbase::GET_EL_MOTOR_TORQUE:
	    // m1, m2, m3, m4
	    // 240, 16, 32, 48 Nm
// !!! these are flipped !!!
	    return longlongToData(nostring,0x302010F0,4);
	    
	case ACUbase::GET_EL_SERVO_COEFF_0:
	case ACUbase::GET_EL_SERVO_COEFF_0 + 1:
	case ACUbase::GET_EL_SERVO_COEFF_0 + 2:
	case ACUbase::GET_EL_SERVO_COEFF_0 + 3:
	case ACUbase::GET_EL_SERVO_COEFF_0 + 4:
	case ACUbase::GET_EL_SERVO_COEFF_0 + 5:
	case ACUbase::GET_EL_SERVO_COEFF_0 + 6:
	case ACUbase::GET_EL_SERVO_COEFF_0 + 7:
	case ACUbase::GET_EL_SERVO_COEFF_0 + 8:
	case ACUbase::GET_EL_SERVO_COEFF_0 + 9:
	case ACUbase::GET_EL_SERVO_COEFF_0 + 10:
	case ACUbase::GET_EL_SERVO_COEFF_0 + 11:
	case ACUbase::GET_EL_SERVO_COEFF_0 + 12:
            return doubleToData(sim_m.get_el_servo_coeff(
		rca - ACUbase::GET_EL_SERVO_COEFF_0,errcode,timestamp));
	    
	case ACUbase::GET_EL_STATUS:
	{
	unsigned long long stat = sim_m.get_el_status(errcode,timestamp);
	unsigned char* stat_p = (unsigned char*)&stat;
	std::vector<CAN::byte_t> tmps = charToData(nostring,stat_p[0]);
	for (int i = 1; i < 8; i++)
	    tmps = charToData(tmps,stat_p[i]);
	return tmps;
	}

	case ACUbase::GET_IDLE_STOW_TIME:
	    return shortToData(nostring,
			       sim_m.get_idle_stow_time(errcode,timestamp));
	    
	case ACUbase::GET_IP_ADDRESS:
	    return longToData(nostring,sim_m.get_ip_address(errcode,timestamp));

	case ACUbase::GET_SHUTTER:
	    return charToData(nostring,sim_m.get_shutter(errcode,timestamp));
	    
	case ACUbase::GET_STOW_PIN:
	    return longlongToData(nostring,
				  sim_m.get_stow_pin(errcode,timestamp),2);
	    
	case ACUbase::GET_SYSTEM_STATUS:
	    return longlongToData(nostring,
				  sim_m.get_system_status(errcode,timestamp),3);
	    
	default:
	    std::cerr << "Switch does not match ACU any case" << std::endl;
	    throw ACUError("Unknown RCA in monitor command");
	}
}

// -----------------------------------------------------------------------------

void AMB::ACU::control(
    rca_t rca,
    const std::vector<CAN::byte_t>& data)
{
    // these are dummies to allow calling ICD methods
    int errcode;
    unsigned long long timestamp;

    // bump transaction count
    trans_num_m++;

    switch (rca & 0x3FFFF)      // rca is first 18 bits
	{
	case ACUbase::SIM_SYSTEM_STATUS:
	{
	if (data.size() != static_cast<size_t>(3))
	    throw ACUError("ACU_MODE_CMD data length must be 3");
	std::vector<CAN::byte_t> temp(data);
	unsigned long value;
	value = static_cast<unsigned long>(dataToLonglong(temp,3));
	sim_m.sim_system_status(value,errcode,timestamp);
	}
	break;
	case ACUbase::ACU_MODE_CMD:
	    {
	    if (data.size() != 1)
		{
		throw ACUError("ACU_MODE_CMD data length must be 1");
		}
	    std::vector<CAN::byte_t> temp(data);
	    unsigned char mode;
	    mode = dataToChar(temp);
	    sim_m.acu_mode_cmd(mode,errcode,timestamp);
	    }
	    break;

	case ACUbase::AZ_TRAJ_CMD:
	    {
	    if (data.size() != 8)
		{
		throw ACUError("AZ_TRAJ_CMD data length must be 8");
		}
	    std::vector<CAN::byte_t> temp(data);
	    long position = dataToLong(temp);
	    long velocity = dataToLong(temp);
	    sim_m.az_traj_cmd(position,velocity,errcode,timestamp);
	    }
	    break;

	case ACUbase::CLEAR_FAULT_CMD:
	    {
	    if (data.size() != 1)
		{
		throw ACUError("CLEAR_FAULT_CMD data length must be 1");
		}
	    }
	    break;

	case ACUbase::EL_TRAJ_CMD:
	    {
            if (data.size() != 8)
		{
                throw ACUError("EL_TRAJ_CMD data length must be 8");
		}
            std::vector<CAN::byte_t> temp(data);
            long position = dataToLong(temp);
            long velocity = dataToLong(temp);
            sim_m.el_traj_cmd(position,velocity,errcode,timestamp);
	    }
            break;

	case ACUbase::RESET_ACU_CMD:
	    {
	    if (data.size() != 1)
		{
		throw ACUError("RESET_ACU_CMD data length must be 1");
		}
	    }
	    break;

	case ACUbase::SET_SHUTTER:
	    {
            if (data.size() != 1)
		{
                throw ACUError("ACU_MODE_CMD data length must be 1");
		}
            sim_m.set_shutter(data[0],errcode,timestamp);
	    }
            break;

	case ACUbase::SET_STOW_PIN:
	    {
            /*
* LOOK AT ME
             * The bits are 0xAAEE in network order, where AA is the Az, and
             * EE is the El.  It is listed as byte 0 for Az, and byte 1 for El.
             */
            if (data.size() != 2)
		{
                throw ACUError("SET_STOW_IN data length must be 2");
		}
            unsigned short pins;
            unsigned char* ptr = (unsigned char*)&pins;
            ptr[0] = data[0];
            ptr[1] = data[1];
            sim_m.set_stow_pin(pins,errcode,timestamp);
	    }
            break;

	case ACUbase::SET_IDLE_STOW_TIME:
	    {
            if (data.size() != 2)
		{
                throw ACUError("ACU_MODE_CMD data length must be 2");
		}
            std::vector<CAN::byte_t> temp(data);
            unsigned short idle_time = dataToShort(temp);
            sim_m.set_idle_stow_time(idle_time,errcode,timestamp);
	    }
            break;

	case ACUbase::SET_AZ_AUX_MODE:
	    {
            if (data.size() != 1)
		{
                throw ACUError("SET_AZ_AUX_MODE data length must be 1");
		}
            unsigned char mode = data[0];
            sim_m.set_az_aux_mode(mode,errcode,timestamp);
	    }
            break;
	    
	case ACUbase::SET_EL_AUX_MODE:
	    {
            if (data.size() != 1)
		{
                throw ACUError("SET_EL_AUX_MODE data length must be 1");
		}
            unsigned char mode = data[0];
            sim_m.set_el_aux_mode(mode,errcode,timestamp);
	    }
            break;
	    
	case ACUbase::SET_AZ_BRAKE:
	    {
            if (data.size() != 1)
		{
                throw ACUError("SET_AZ_BRAKE data length must be 1");
		}
            unsigned char mode = data[0];
            sim_m.set_az_brake(mode,errcode,timestamp);
	    }
            break;
	    
	case ACUbase::SET_EL_BRAKE:
	    {
            if (data.size() != 1)
		{
                throw ACUError("SET_EL_BRAKE data length must be 1");
		}
            unsigned char mode = data[0];
            sim_m.set_el_brake(mode,errcode,timestamp);
	    }
            break;
	    
	case ACUbase::SET_AZ_SERVO_COEFF_0:
	case ACUbase::SET_AZ_SERVO_COEFF_0 + 1:
	case ACUbase::SET_AZ_SERVO_COEFF_0 + 2:
	case ACUbase::SET_AZ_SERVO_COEFF_0 + 3:
	case ACUbase::SET_AZ_SERVO_COEFF_0 + 4:
	case ACUbase::SET_AZ_SERVO_COEFF_0 + 5:
	case ACUbase::SET_AZ_SERVO_COEFF_0 + 6:
	case ACUbase::SET_AZ_SERVO_COEFF_0 + 7:
	case ACUbase::SET_AZ_SERVO_COEFF_0 + 8:
	case ACUbase::SET_AZ_SERVO_COEFF_0 + 9:
	case ACUbase::SET_AZ_SERVO_COEFF_0 + 10:
	case ACUbase::SET_AZ_SERVO_COEFF_0 + 11:
	case ACUbase::SET_AZ_SERVO_COEFF_0 + 12:
	    {
	    std::vector<CAN::byte_t> temp(data);
            sim_m.set_az_servo_coeff(
		rca - ACUbase::SET_AZ_SERVO_COEFF_0,dataToDouble(temp),
		errcode,timestamp);
	    }
	    break;

	case ACUbase::SET_EL_SERVO_COEFF_0:
	case ACUbase::SET_EL_SERVO_COEFF_0 + 1:
	case ACUbase::SET_EL_SERVO_COEFF_0 + 2:
	case ACUbase::SET_EL_SERVO_COEFF_0 + 3:
	case ACUbase::SET_EL_SERVO_COEFF_0 + 4:
	case ACUbase::SET_EL_SERVO_COEFF_0 + 5:
	case ACUbase::SET_EL_SERVO_COEFF_0 + 6:
	case ACUbase::SET_EL_SERVO_COEFF_0 + 7:
	case ACUbase::SET_EL_SERVO_COEFF_0 + 8:
	case ACUbase::SET_EL_SERVO_COEFF_0 + 9:
	case ACUbase::SET_EL_SERVO_COEFF_0 + 10:
	case ACUbase::SET_EL_SERVO_COEFF_0 + 11:
	case ACUbase::SET_EL_SERVO_COEFF_0 + 12:
	    {
	    std::vector<CAN::byte_t> temp(data);
            sim_m.set_el_servo_coeff(
		rca - ACUbase::SET_EL_SERVO_COEFF_0,dataToDouble(temp),
		errcode,timestamp);
	    }
	    break;

	case ACUbase::SET_AZ_SERVO_DEFAULT:
	    sim_m.set_az_servo_default(errcode,timestamp);
	    break;

	case ACUbase::SET_EL_SERVO_DEFAULT:
	    sim_m.set_el_servo_default(errcode,timestamp);
	    break;

	default:
	    throw CAN::Error("rca NOT Implemented");
	}
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::ACU::get_can_error() const
{
    int errcode;
    unsigned long long timestamp;
    unsigned long level = sim_m.get_can_error(errcode,timestamp);
    const std::vector<CAN::byte_t> nostring;
    return longToData(nostring,level);
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::ACU::get_protocol_rev_level() const
{
    int errcode;
    unsigned long long timestamp;
    std::vector<char> level = sim_m.get_sw_rev_level(errcode,timestamp);
    const std::vector<CAN::byte_t> nostring;
    std::vector<CAN::byte_t> tmps = charToData(nostring,level[0]);
    tmps = charToData(tmps,level[1]);
    return charToData(tmps,level[2]);
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::ACU::get_sw_rev_level() const
{
    int errcode;
    unsigned long long timestamp;
    std::vector<char> level = sim_m.get_system_id(errcode,timestamp);
    const std::vector<CAN::byte_t> nostring;
    std::vector<CAN::byte_t> tmps = charToData(nostring,level[0]);
    tmps = charToData(tmps,level[1]);
    tmps = charToData(tmps,level[2]);
    tmps = charToData(tmps,level[3]);
    return tmps;
}
	
// -----------------------------------------------------------------------------

unsigned int AMB::ACU::get_trans_num() const
{
    // sim_m.get_num_trans(errcode,timestamp); is NOT used
    return trans_num_m;
}

void AMB::ACU::setSharedSimulator(SharedSimInterface* ssim) {

  AMB::Device::setSharedSimulator(ssim);
  sim_m.setSharedSimulator(ssim);

}
