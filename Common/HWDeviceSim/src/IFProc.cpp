// @(#) $Id$
//
// Copyright (C) 2002
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include <iomanip>
#include "IFProc.h"
#include "AMBUtil.h"
#include <cmath>
#include <sstream>

// ----------------------------------------------------------------------------
AMB::IFProc::IFProc(node_t node,
            const std::vector<CAN::byte_t> &serialNumber,
            short pol)
  : m_node(node),
    m_sn(serialNumber),
    m_trans_num(0),
    m_errorCode(1),
    dataMap_m(0),   
    moduleIP_m(4),  
    destIP_m(4),
    gatewayIP_m(4), 
    netmask_m(4),   
    deviceMAC_m(0),
    status_m(0),
    invalidCmd_m(0),
    tcpPort_m(0),
    bdbPerPacket_m(0),
    dataAvgLen_m(0), 
    signalPaths_m(0),
    ambCmdCounter_m(0),
    polarization_m(pol),
    fifoReadTime_m(0),
    ipTrxTime_m(0)
{
  std::cout << "Creating node " << node << ",s/n 0x";
   for (int i = 0; i < 8; i++) {
     std::cout << std::hex << std::setw(2) << std::setfill('0')
           << static_cast<int>(serialNumber[i]);
   }
   std::cout << ", IFPRoc device" << std::endl;
   resetIFProc();
}

AMB::IFProc::~IFProc() {
}

AMB::node_t AMB::IFProc::node() const {
  return m_node;
}

std::vector<CAN::byte_t> AMB::IFProc::serialNumber() const {
  return m_sn;
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::IFProc::monitor(rca_t rca) const {
//   m_trans_num++;
   std::vector<CAN::byte_t> tmp;
   //   std::vector<CAN::byte_t> retVal;
   switch (rca) {
     // Mandatory generic points
   case GET_SERIAL_NUMBER:
     return serialNumber();
     break;
   case GET_CAN_ERROR:
     return get_can_error();
     break;
   case GET_PROTOCOL_REV_LEVEL:
     return get_protocol_rev_level();
     break;
    case AMB::Device::GET_SW_REV_LEVEL:
        return get_software_rev_level();
        break;
    case AMB::Device::GET_HW_REV_LEVEL:
        return get_hardware_rev_level();
        break;
//   case GET_TRANS_NUM:
//     return longToData(tmp, get_trans_num());
//     break;
   case READ_AMB_CMD_COUNTER:
     return shortToData(tmp, ambCmdCounter_m);
     break;
   // Optional generic point
   case GET_AMBIENT_TEMPERATURE:
     return floatDS1820ToData(tmp, get_ambient_temperature());
     break;

   // IFProc specific points
   case READ_STATUS:
     return status_m;
     break;
   case READ_TIMING_ERROR_FLAG:
     return charToData(tmp, timingError_m);
     break;
   case READ_DATA_MONITOR_1:
     if (sharedSim_p->haveSimulatorRef())
     {
       return sharedSim_p->getDownConvPowers(polarization_m);
     }
     else {
       std::cout << "Monitor 1 no-sharedsim"<<std::endl;
       unsigned short TPS[2];
       TPS[0] = 26214;
       TPS[1] = 52428;
//       TPS[0] = static_cast<unsigned short>( (random() / static_cast<double>(RAND_MAX))* 52428.8 ); // random number between 0 and 52428.8
//       TPS[1] = static_cast<unsigned short>( (random() / static_cast<double>(RAND_MAX))* 52428.8 ); // random number between 0 and 52428.8
 
       for(int idx=0 ; idx < 2 ; idx++){
          unsigned char* byteptr = reinterpret_cast< unsigned char* >(&TPS[idx]);
          for (int i=1 ; i>=0; i--)
             tmp.push_back(byteptr[i]);
       }
       return tmp;
     }
     break;
   case READ_DATA_MONITOR_2:
     if (sharedSim_p->haveSimulatorRef())
       return sharedSim_p->getBaseBandPowers(polarization_m);
     else {
       unsigned short TP[4];

       for (int i=0;i<4;i++)
          TP[i] = static_cast<unsigned short>( pow(10,gain_m[i]*-0.05+0.39794)*26214.4); // random number between 0 and 52428.8
                // (655.36 * pow(10, gain_m[idx]/5.0)/2.5));
//       TP[0] = 13107;
//       TP[1] = 26214;
//       TP[2] = 39321;
//       TP[3] = 52428;

       //pushing the uint16 values into a data vector 
       for(int idx=0 ; idx < 4 ; idx++){
          unsigned char* byteptr = reinterpret_cast< unsigned char* >(&TP[idx]);
          for (int i=1 ; i>=0; i--)
             tmp.push_back(byteptr[i]);
       }
       return tmp;
     }
     break;
   case READ_FIFO_DEPTHS:
     unsigned short FIFO[2];
     FIFO[0] = static_cast<unsigned short>( (random() / static_cast<double>(RAND_MAX))* 2047  ); // random number between 0 and 2047
     FIFO[1] = static_cast<unsigned short>( (random() / static_cast<double>(RAND_MAX))* 43689 ); // random number between 0 and 43689

     //pushing the uint16 values into a data vector 
     for(int idx=0 ; idx < 2 ; idx++){
        unsigned char* byteptr = reinterpret_cast< unsigned char* >(&FIFO[idx]);
        for (int i=1 ; i>=0; i--)
           tmp.push_back(byteptr[i]);
     }
     return tmp;
     break;
   case READ_DATA_REMAP:
     return dataMap_m;
     break;
   case READ_MODULE_IP_ADDR:
     return moduleIP_m;
     break;
   case READ_DEST_IP_ADDR:
     return destIP_m;
     break;
   case READ_ETHERNET_MAC:
     return deviceMAC_m;
     break;
   case READ_TCP_PORT:
     return shortToData(tmp, tcpPort_m);
     break;
   case READ_BDB_PER_PACKET:
     return charToData(tmp, bdbPerPacket_m);
     break;
   case READ_ALMA_TIME:
     {
       struct timeval tvNow;
       gettimeofday(&tvNow, NULL);
       unsigned long long elapsedTime =
     static_cast<unsigned long long>((tvNow.tv_sec -
                      timeRef_m.tv.tv_sec)*1E7) +
     static_cast<unsigned long long>((tvNow.tv_usec -
                      timeRef_m.tv.tv_usec)*10);
       return longlongToData(tmp,timeRef_m.almaTime + elapsedTime,8);
     }
     break;
   case READ_48MS_LENGTH:
     if (random() > 0.9*RAND_MAX) {
       return longToData(tmp,static_cast<long>(6e6));
     } else {
       return longToData(tmp,static_cast<long>(6e6)-1);
     }
     break;
   case READ_ETHERNET_TIMES:
     tmp = longToData(tmp,fifoReadTime_m);
     tmp = longToData(tmp,ipTrxTime_m);
     printf("fifoReadTime: %li\n", fifoReadTime_m); 
     printf("ipTrxTime: %li\n", ipTrxTime_m); 

//     // Maximum time to read data for a single packet
//     fifoReadTime_m = static_cast< unsigned int >( 2E6 + 
//         ((random() / static_cast< double >(RAND_MAX)) - 0.5) * 2E3);
//
//     // Maximum Ethernet transimt plus handling 
//     ipTrxTime_m = static_cast< unsigned int >( 3E6 + 
//         ((random() / static_cast< double >(RAND_MAX)) - 0.5) * 2E3);

     return tmp;
     break;


   case READ_AVERAGE_LENGTH:
     return charToData(tmp,dataAvgLen_m);
     break;
   case READ_IFDC_SPI_STATUS:
     tmp.push_back(0x01); // DC found, fixed length transfers
     tmp.push_back(0x88); // Address Bytes = 0; FixedLen = 2; varLen = 4
     return tmp;
     break;
   case READ_VOLTAGES_1:
     tmp = shortToData(tmp, static_cast<unsigned short>((1.2 + (((random()/static_cast<double>(RAND_MAX))-0.5)*0.05 ))*204.8));
     tmp = shortToData(tmp, static_cast<unsigned short>((2.5 + (((random()/static_cast<double>(RAND_MAX))-0.5)*0.075))*204.8));
     tmp = shortToData(tmp, static_cast<unsigned short>((3.3 + (((random()/static_cast<double>(RAND_MAX))-0.5)*0.125))*204.8));
     tmp = shortToData(tmp, static_cast<unsigned short>((3.3 + (((random()/static_cast<double>(RAND_MAX))-0.5)*0.125))*204.8));
     return tmp;
     break;
   case READ_VOLTAGES_2:
     tmp = shortToData(tmp, static_cast<unsigned short>((5.0 + (((random()/static_cast<double>(RAND_MAX))-0.5)*0.25))*204.8));
     tmp = shortToData(tmp, static_cast<unsigned short>((5.0 + (((random()/static_cast<double>(RAND_MAX))-0.5)*0.25))*204.8));
     tmp = shortToData(tmp, static_cast<unsigned short>((5.0 + (((random()/static_cast<double>(RAND_MAX))-0.5)*0.25))*204.8));
     tmp = shortToData(tmp, static_cast<unsigned short>((7.0 + (((random()/static_cast<double>(RAND_MAX))-0.5)*0.25))*204.8));
     return tmp;
     break;
   case READ_AMB_INVALID_CMD:
     return(invalidCmd_m);
     break;
   case READ_SPI_ERRORS:
     tmp.push_back(0x0);
     tmp.push_back(0x0);
     tmp.push_back(0x0);
     tmp.push_back(0x0);
     tmp = shortToData(tmp, 0x0);
     return tmp;
     break;

   case GATEWAY_IP_ADDR:
     return gatewayIP_m;
     break;
   case NETMASK:
     return netmask_m;
     break;
   case READ_IFTP_MODULE_CODES:
     tmp.push_back(0x42); // BACK End Code
     tmp.push_back(0x0A); // IPMC-C Code?
     tmp.push_back(0x15); // Firmware revision 1.5
     tmp.push_back(0x0C); // Day: 12
     tmp.push_back(0x05); // Month: 5
     tmp.push_back(0x05); // Year: 2005
     tmp.push_back(0x00); // 
     tmp.push_back(0x6E); // 110
     return tmp;
     break;
   case READ_GAINS:
     if (sharedSim_p->haveSimulatorRef())
       return sharedSim_p->getGains(polarization_m);
     else {
       for (int idx = 0; idx < 4; idx++) {
         tmp.push_back(static_cast<CAN::byte_t>(gain_m[idx]*8));
       }
       return tmp;
     }
     break;
   case READ_SIGNAL_PATHS:
     tmp = charToData(tmp,signalPaths_m);
     return tmp;
     break;
   case READ_TEMPS_1:
   case READ_TEMPS_2:
     for (int idx = 0; idx < 4; idx++) {
       tmp = shortToData(tmp, 187 + static_cast<short>(
             ((random()/static_cast<double>(RAND_MAX))-0.5)*13));
     }
     return tmp;
     break;
    case READ_TEMPS_3:
    {
        tmp = shortToData(tmp, 187 + static_cast< short >(
             ((::random() / static_cast< double >(RAND_MAX)) - 0.5) * 13));
    }
    return tmp;
        break;
    case READ_10V_CURRENTS:
    {
        for (std::size_t idx(0); idx < 4; ++idx)
        {
            tmp = shortToData(tmp, 95 + static_cast< short >(
                ((::random() / static_cast< double >(RAND_MAX)) - 0.5) * 20));
        }
    }
    return tmp;
        break;
    case READ_65V_CURRENTS:
    {
        for (std::size_t idx(0); idx < 4; ++idx)
        {
            tmp = shortToData(tmp, 165 + static_cast< short >(
                ((::random() / static_cast< double >(RAND_MAX)) - 0.5) * 10));
        }
    }
    return tmp;
        break;
    case READ_8V_CURRENTS:
    {
        for (std::size_t idx(0); idx < 4; ++idx)
        {
            tmp = shortToData(tmp, 45 + static_cast< short >(
                ((::random() / static_cast< double >(RAND_MAX)) - 0.5) * 10));
        }
    }
    return tmp;
        break;
   case READ_IFDC_MODULE_CODES:
     tmp.push_back(0x42); // BACK End Code
     tmp.push_back(0x0B); // IFDC Code?
     tmp.push_back(0x18); // Firmware revision 1.8
     tmp.push_back(0x0C); // Day: 12
     tmp.push_back(0x05); // Month: 5
     tmp.push_back(0x05); // Year: 2005
     tmp.push_back(0xFF); // Unavailible SN
     tmp.push_back(0xFF);
     return tmp;
     break;
   default:
    {
        std::ostringstream s;
        s << "Unknown RCA (0x"
            << std::hex << rca << std::dec
            << ") in monitor command";
        throw IFProcError(s.str());
    }
   }
   // Never executed but makes compiler happy
   return tmp;
}

// ----------------------------------------------------------------------------
void AMB::IFProc::control(rca_t rca, const std::vector<CAN::byte_t> &data) {
   m_trans_num++;
   std::vector<CAN::byte_t> localData(data);
   ambCmdCounter_m++;
   try{
     switch (rca) {
     case RESET_TPD_BOARD:
       if (data[0] & 0x01){
     resetIFProc();
       }
       break;
     case SET_DATA_REMAP:
       checkSize(data, 6, "Illegal Data Size");
       dataMap_m.clear();
       for (unsigned int idx = 0; idx < data.size(); idx++)
     dataMap_m.push_back(data[idx]);
       break;
     case SET_MODULE_IP_ADDR:
       checkSize(data, 4, "Illegal Data Size");
       moduleIP_m.clear();
       for (unsigned int idx = 0; idx < data.size(); idx++)
     moduleIP_m.push_back(data[idx]);
       break;
     case SET_DEST_IP_ADDR:
       checkSize(data, 4, "Illegal Data Size");
       destIP_m.clear();
       for (unsigned int idx = 0; idx < data.size(); idx++)
     destIP_m.push_back(data[idx]);
       break;
     case SET_TCP_PORT:
       checkSize(data, 2, "Illegal Data Size");
       tcpPort_m = dataToShort(localData);
       break;
     case SET_BDB_PER_PACKET:
       checkSize(data, 1, "Illegal Data Size");
       bdbPerPacket_m = dataToChar(localData);
       break;
     case SET_ALMA_TIME:
       ////////////////////
       checkSize(data, 8, "Illegal Data Size");
       break;
     case INIT_TCP_CONN:
       ///////////////////////
     break;
     case START_DATA:
       /////////////////
       break;
     case STOP_DATA:
       ///////////////////////
       break;
     case START_COMM_TEST:
       checkSize(data, 1, "Illegal Data Size");
       status_m[3] = data[0];
       /////////////////////
     break;
     case CLEAR_TIMING_ERROR:
       timingError_m = 0;
       break;
     case SET_AVERAGE_LENGTH:
       checkSize(data, 1, "Illegal Data Size");
       dataAvgLen_m = dataToChar(localData);
       break;
     case RESET_ETHERNET_TIMES:
       checkSize(data, 1, "Illegal Data Size");
       if (data[0] & 0x01){
     fifoReadTime_m = 0;
     ipTrxTime_m = 0;
       }
       break;
     case RESET_AMB_INVALIED_CMD:
       checkSize(data, 1, "Illegal Data Size");
       if (data[0] & 0x01){
     invalidCmd_m.clear();
     invalidCmd_m =charToData(invalidCmd_m, 0);
     invalidCmd_m=charToData(invalidCmd_m, 0);
     invalidCmd_m=shortToData(invalidCmd_m, 0);
       }
       break;
     case RESET_AMB_CMD_COUNTER:
       checkSize(data, 1, "Illegal Data Size");
       if (data[0] & 0x01){
     ambCmdCounter_m= 0;
       }
       break;
     case SET_GATEWAY_IP_ADDR:
       checkSize(data, 4, "Illegal Data Size");
       gatewayIP_m.clear();
       for (unsigned int idx = 0; idx < data.size(); idx++)
         gatewayIP_m.push_back(data[idx]);
       break;
     case SET_NETMASK:
       checkSize(data, 4, "Illegal Data Size");
       netmask_m.clear();
       for (unsigned int idx = 0; idx < data.size(); idx++)
         netmask_m.push_back(data[idx]);
       break;
     case SET_GAINS:
       if (sharedSim_p->haveSimulatorRef())
         sharedSim_p->setGains(polarization_m, data);
       else {
         checkSize(data, 4, "Illegal Data Size");
         for (int idx = 0; idx < 4; idx++) {
             gain_m[idx] = data[idx] / 8.0;
         }
       }
       break;
     case SET_SIGNAl_PATHS:
       checkSize(data, 1, "Illegal Data Size");
       signalPaths_m = dataToChar(localData);
       if (sharedSim_p->haveSimulatorRef())
         sharedSim_p->setSignalPaths(polarization_m, data);
       break;
    default:
    {
        std::ostringstream s;
        s << "IFProc: unknown RCA (0x"
            << std::hex << rca << std::dec
            << ") in control command.";
        throw IFProcError(s.str());
    }
     }
   }
   catch (const AMB::IFProc::IFProcError& ex) {
     invalidCmd_m.clear();
     invalidCmd_m =charToData(invalidCmd_m,
                  static_cast<unsigned char>(rca & 0xFF));
     invalidCmd_m=charToData(invalidCmd_m,
                 static_cast<unsigned char>(data[0]));
     invalidCmd_m=shortToData(invalidCmd_m, ambCmdCounter_m);
     throw ex;
   }
 }

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::IFProc::get_can_error() const {
  return AMB::Device::get_can_error();
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::IFProc::get_protocol_rev_level() const {
   return AMB::Device::get_protocol_rev_level();
 }

// ----------------------------------------------------------------------------
unsigned int AMB::IFProc::get_trans_num() const {
  return m_trans_num;
}

// ----------------------------------------------------------------------------
double AMB::IFProc::get_ambient_temperature() const {
   return 22.4 + (5 *((random()/static_cast<double>(RAND_MAX))-0.5));
}

// ----------------------------------------------------------------------------
void AMB::IFProc::checkSize(const std::vector<CAN::byte_t> &data, int size,
                const char *pExceptionMsg) {
  if( static_cast<int>(data.size()) != size ) {
    char exceptionMsg[200];
    sprintf(exceptionMsg,"%s data length is %d must be %d",pExceptionMsg,
         data.size(), size);
    throw IFProcError(exceptionMsg);
  }
}

void AMB::IFProc::resetIFProc(){
  /* Set the default data map */

  dataMap_m.clear();
  deviceMAC_m.clear();
  for (int idx = 0; idx < 6; idx++){
    dataMap_m.push_back(idx);
    deviceMAC_m.push_back(idx);
  }

  destIP_m.clear();
  moduleIP_m.clear();
  gatewayIP_m.clear();
  netmask_m.clear();
  invalidCmd_m.clear();

  for (int idx = 0; idx < 4; idx++) {
    gain_m[idx]    = 0.0; // Note Gains are in db
    destIP_m.push_back(0);
    moduleIP_m.push_back(0);
    gatewayIP_m.push_back(0);
    netmask_m.push_back(0);
    invalidCmd_m.push_back(0);
  }

  tcpPort_m = 1433;
  bdbPerPacket_m = 32;

  timeRef_m.almaTime   = 0;
  timeRef_m.tv.tv_sec  = 0;
  timeRef_m.tv.tv_usec = 0;
  dataAvgLen_m = 0;
  timingError_m = 1;
  ambCmdCounter_m = 0;

  status_m.push_back(0);
  status_m.push_back(0x3F);
  status_m.push_back(0x3F);
  status_m.push_back(0xab);

}
