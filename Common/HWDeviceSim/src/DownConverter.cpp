/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2002 
*
* This code is under GNU General Public Licence (GPL). 
* Correspondence concerning ALMA Software should be addressed to:
* alma-sw-admin@nrao.edu
*
* Copyright (C) 2001
* Associated Universities, Inc. Washington DC, USA.
*
* Produced for the ALMA project
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Library General Public License
* as published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Library General Public License for more details.
*
* You should have received a copy of the GNU Library General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* dfugate  16/12/02  created 
*/

#include <iomanip>
#include "DownConverter.h"
#include "AMBUtil.h"

// -----------------------------------------------------------------------------

AMB::DownConverter::DownConverter(node_t node,
				  const std::vector<CAN::byte_t> &serialNumber) : 
    node_m(node), 
    sn_m(serialNumber), 
    trans_num_m(0)
{
    std::cout << "Creating node " << node << ", s/n 0x";
    for (int i = 0; i < 8; i++)
	{
	std::cout << std::hex << std::setw(2) << std::setfill('0') << (int)serialNumber[i];
	}
    std::cout << ", DownConverter device" << std::endl;


    /// Now for data related specifically to the downconverter device
    memset(attenuators_m, 0, 6);
    tpd_switches_m    = 0;
    matrix_switches_m = 0;
}

// -----------------------------------------------------------------------------
AMB::DownConverter::~DownConverter()
{
    // Nothing
}
// -----------------------------------------------------------------------------
AMB::node_t 
AMB::DownConverter::node() const
{
    return node_m;
}
// -----------------------------------------------------------------------------
std::vector<CAN::byte_t> 
AMB::DownConverter::serialNumber() const
{
    return sn_m;
}
// -----------------------------------------------------------------------------
std::vector<CAN::byte_t> 
AMB::DownConverter::monitor(rca_t rca) const
{
    trans_num_m++;
    const std::vector<CAN::byte_t> tmp;
    
    switch (rca) 
	{
	// Mandatory generic points
	case GET_CAN_ERROR:
	    return get_can_error();
	    break;
	case GET_PROTOCOL_REV_LEVEL:
	    return get_protocol_rev_level();
	    break;
	case AMB::Device::GET_SW_REV_LEVEL:
		return get_software_rev_level();
		break;
	case AMB::Device::GET_HW_REV_LEVEL:
		return get_hardware_rev_level();
		break;
	case GET_TRANS_NUM:
	    return longToData(tmp, get_trans_num());
	    break;
	    // Optional generic point
	case GET_AMBIENT_TEMPERATURE:
	    return floatDS1820ToData(tmp, get_ambient_temperature());
	    break;
	    
	case READ_ATTENUATORS:
	    return read_attenuators();
	    break;          
	case READ_TPD_SWITCHES:
	    return read_tpd_switches();
	    break;          
	case READ_MATRIX_SWITCHES:
	    return read_matrix_switches();
	    break;      
	case PIC_ERROR_CODE:
	    return pic_error_code();
	    break;             
	case READ_TEMPS_IF:
	    return read_4_shorts();
	    break;            
	case READ_TEMPS_BB:
	    return read_4_shorts();
	    break;              
	case READ_TEMPS_MC:
	    return read_4_shorts();
	    break;              
	case READ_VR_15_AND_15SEQ_IF:
	    return read_4_shorts();
	    break;    
	case READ_VR_5_AND_7_AND_15H_IF:
	    return read_4_shorts();
	    break;
	case READ_VR_5_AND_16_BB:
	    return read_3_shorts();
	    break;        
	case READ_VR_5_AND_16_MC:
	    return read_3_shorts();
	    break;        
	    
	default:
	    std::cerr << "Switch does not match any case" << std::endl;
	    throw DownConverterError("Unknown RCA in monitor command");
	}
}

// -----------------------------------------------------------------------------
void 
AMB::DownConverter::control(rca_t rca, const std::vector<CAN::byte_t> &data)
{
    trans_num_m++;
    if (data.size() != 1) 
	{
	std::cout << "All DC set commands are of size 1!\n";
	}
    
    switch (rca) 
	{
	case SET_TPD_SWITCHES:
	    set_tpd_switches(data[0]);
	    break;
	case SET_MATRIX_SWITCHES:
	    set_matrix_switches(data[0]);
	    break;           
	case SET_ATT_U:
	    set_att_u(data[0]);
	    break;                      
	case SET_ATT_L:
	    set_att_l(data[0]);
	    break;                      
	case SET_ATT_A:
	    set_att_a(data[0]);
	    break;                      
	case SET_ATT_B:
	    set_att_b(data[0]);
	    break;                      
	case SET_ATT_C:
	    set_att_c(data[0]);
	    break;                      
	case SET_ATT_D:
	    set_att_d(data[0]);
	    break;                      
	    
	default:
	    throw DownConverterError("Unknown RCA in control command");
	    break;
	}
}

// -----------------------------------------------------------------------------
std::vector<CAN::byte_t> 
AMB::DownConverter::get_can_error() const
{
    return AMB::Device::get_can_error();
}
// -----------------------------------------------------------------------------
std::vector<CAN::byte_t> 
AMB::DownConverter::get_protocol_rev_level() const
{
    return AMB::Device::get_protocol_rev_level();
}
// -----------------------------------------------------------------------------
unsigned int 
AMB::DownConverter::get_trans_num() const
{
    return trans_num_m;
}
// -----------------------------------------------------------------------------
float 
AMB::DownConverter::get_ambient_temperature() const
{
    return 18.0;
}
// -----------------------------------------------------------------------------
std::vector<CAN::byte_t> 
AMB::DownConverter::read_attenuators() const
{
    std::vector<CAN::byte_t> tmp(7);
    int i;

    for (i=0; i<7; i++)
	{
	tmp[i]=attenuators_m[i];
	}
    return tmp;
}
// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> 
AMB::DownConverter::read_tpd_switches() const
{
    std::vector<CAN::byte_t> tmp(1);
    tmp[0] = tpd_switches_m;
    return tmp;
}
// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> 
AMB::DownConverter::read_matrix_switches() const
{
    std::vector<CAN::byte_t> tmp(1);
    tmp[0] = matrix_switches_m;
    return tmp;
}
// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> 
AMB::DownConverter::pic_error_code() const
{
   std::vector<CAN::byte_t> tmp(1);
    tmp[0] = 0;
    return tmp; 
}
// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> 
AMB::DownConverter::read_4_shorts() const
{
    std::vector<CAN::byte_t> tmp(8);
    
    // 0x7F = 127d
    tmp[0] = 0x00;
    tmp[1] = 0x7F;
    tmp[2] = 0x00;
    tmp[3] = 0x7F;
    tmp[4] = 0x00;
    tmp[5] = 0x7F;
    tmp[6] = 0x00;
    tmp[7] = 0x7F;
    return tmp;
}
// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> 
AMB::DownConverter::read_3_shorts() const
{
    std::vector<CAN::byte_t> tmp(6);
    
    // 0x7F = 127d
    tmp[0] = 0x00;
    tmp[1] = 0x7F;
    tmp[2] = 0x00;
    tmp[3] = 0x7F;
    tmp[4] = 0x00;
    tmp[5] = 0x7F;
    return tmp;
}
// ----------------------------------------------------------------------------
void 
AMB::DownConverter::set_tpd_switches(CAN::byte_t state)
{
    tpd_switches_m=state;
}
// ----------------------------------------------------------------------------
void 
AMB::DownConverter::set_matrix_switches(CAN::byte_t state)
{
    matrix_switches_m=state;
}
// ----------------------------------------------------------------------------
void 
AMB::DownConverter::set_att_u(CAN::byte_t state)
{
    attenuators_m[0]=state;
}
// ----------------------------------------------------------------------------
void 
AMB::DownConverter::set_att_l(CAN::byte_t state)
{
    attenuators_m[1]=state;
}
// ----------------------------------------------------------------------------
void 
AMB::DownConverter::set_att_a(CAN::byte_t state)
{
    attenuators_m[2]=state;
}
// ----------------------------------------------------------------------------
void 
AMB::DownConverter::set_att_b(CAN::byte_t state)
{
    attenuators_m[3]=state;
}
// ----------------------------------------------------------------------------
void 
AMB::DownConverter::set_att_c(CAN::byte_t state)
{
    attenuators_m[4]=state;
}
// ----------------------------------------------------------------------------
void 
AMB::DownConverter::set_att_d(CAN::byte_t state)
{
    attenuators_m[5]=state;
}
// ----------------------------------------------------------------------------
