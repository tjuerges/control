//// @(#) $Id$
//
// Copyright (C) 2002
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include <iomanip>
#include "HolographyDSP.h"
#include "AMBUtil.h"

#include <stdio.h>

// -----------------------------------------------------------------------------
AMB::HolographyDSP::HolographyDSP(
	node_t node,
	const std::vector<CAN::byte_t>& serialNumber)
    : NUM_SAMPLES_PER_INTERVAL(600), m_node(node), m_sn(serialNumber),
      m_trans_num(0), m_dataProductBaseValue(1LL), m_interval(0)

{
    std::cout << "Creating node " << node << ", s/n 0x";
    for (int i = 0; i < 8; i++)
	std::cout << std::hex << std::setw(2) << std::setfill('0') << (int)serialNumber[i];
    std::cout << ", Holography DSP device" << std::endl;

    // TODO - validate node & sn
}

// -----------------------------------------------------------------------------
AMB::HolographyDSP::~HolographyDSP()
{
    // Nothing
}

// -----------------------------------------------------------------------------
AMB::node_t AMB::HolographyDSP::node() const
{
    return m_node;
}

// -----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::HolographyDSP::serialNumber() const
{
    return m_sn;
}

// -----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::HolographyDSP::monitor(
    rca_t rca) const
{
    m_trans_num++;
    const std::vector<CAN::byte_t> tmp;

    switch (rca)
    {
	// Mandatory generic points
    case GET_CAN_ERROR:
	return get_can_error();
	break;
    case GET_PROTOCOL_REV_LEVEL:
	return get_protocol_rev_level();
	break;
	case AMB::Device::GET_SW_REV_LEVEL:
		return get_software_rev_level();
		break;
	case AMB::Device::GET_HW_REV_LEVEL:
		return get_hardware_rev_level();
		break;
    case GET_TRANS_NUM:
	return longToData(tmp, get_trans_num());
	break;
	// Optional generic point
    case GET_AMBIENT_TEMPERATURE:
	return floatDS1820ToData(tmp, get_ambient_temperature());
	break;
	
	// HolographyDSP specific points
    case GET_DATA_QQ_0:
    case GET_DATA_QQ_1:
    case GET_DATA_QQ_2:
    case GET_DATA_QQ_3:
	return getDataPoint(dataProduct_QQ);
	break;
    case GET_DATA_QR_0:
    case GET_DATA_QR_1:
    case GET_DATA_QR_2:
    case GET_DATA_QR_3:
	return getDataPoint(dataProduct_QR);
	break;
    case GET_DATA_QS_0:
    case GET_DATA_QS_1:
    case GET_DATA_QS_2:
    case GET_DATA_QS_3:
	return getDataPoint(dataProduct_QS);
	break;
    case GET_DATA_RR_0:
    case GET_DATA_RR_1:
    case GET_DATA_RR_2:
    case GET_DATA_RR_3:
	return getDataPoint(dataProduct_RR);
	break;
    case GET_DATA_RS_0:
    case GET_DATA_RS_1:
    case GET_DATA_RS_2:
    case GET_DATA_RS_3:
	return getDataPoint(dataProduct_RS);
	break;
    case GET_DATA_SS_0:
    case GET_DATA_SS_1:
    case GET_DATA_SS_2:
    case GET_DATA_SS_3:
	return getDataPoint(dataProduct_SS);
	break;

    case GET_NUM_SAMPLES:
    {
	std::vector<short> sampleVector = getNumberSamples();

	std::vector<CAN::byte_t> returnBytes;
	for (int i = 0; i < 4; i++) {
	  returnBytes = shortToData(returnBytes, sampleVector[i]);
	}
	return returnBytes;
	break;
    }

    case GET_BAD_FRAMES_COUNT:
    {
      std::vector<CAN::byte_t> returnBytes;
      returnBytes = shortToData(returnBytes, getBadFramesCount());
      return returnBytes;
      break;
    }

    default:
	std::cerr << "Switch does not match any HolographyDSP case" << std::endl;
	throw HolographyDSPError("Unknown RCA in monitor command");
    }
}

// -----------------------------------------------------------------------------
void AMB::HolographyDSP::control(
    rca_t rca, 
    const std::vector<CAN::byte_t>& data)
{
    m_trans_num++;
    switch (rca) 
    {
    case RESET_DSP:
	resetDSP(data,1,"RESET_DSP");
	break;

    default:
	throw HolographyDSPError("Unknown RCA in control command");
    }
}

// -----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::HolographyDSP::getDataPoint(
    dataProduct_t dataProduct) const
{
    std::vector<CAN::byte_t> tmp;

    int dpIndex = int(dataProduct);
    
    long long retVal = m_dataProductBaseValue + dpIndex + (m_interval * 6LL);

    // Each time we request the 4-th interval of product SS, then jump to 
    // the next baseValue for the 24 points (6 products x 4 intervals)
    if ((dataProduct == dataProduct_SS) && (m_interval == 3))
	m_dataProductBaseValue += 24LL;

    // Check if we need to move to the next interval (modulo 4)
    if (dataProduct == dataProduct_SS)
    {
	m_interval++;
	m_interval = m_interval % 4;
    }
    return longlongToData(tmp,retVal,6);
}

// ----------------------------------------------------------------------------
std::vector<short> AMB::HolographyDSP::getNumberSamples() const
{
  std::vector<short> sampleVector(4);

  for (int i = 0; i < 4; i++) {
    sampleVector[i] = NUM_SAMPLES_PER_INTERVAL;
  }
  return  sampleVector;
}

// ----------------------------------------------------------------------------
short AMB::HolographyDSP::getBadFramesCount() const {
  return 0;
}

// -----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::HolographyDSP::get_can_error() const
{
    return AMB::Device::get_can_error();
}

// -----------------------------------------------------------------------------
 std::vector<CAN::byte_t> AMB::HolographyDSP::get_protocol_rev_level() const
{
    return AMB::Device::get_protocol_rev_level();
}

// -----------------------------------------------------------------------------
unsigned int AMB::HolographyDSP::get_trans_num() const
{
    return m_trans_num;
}

// -----------------------------------------------------------------------------
float AMB::HolographyDSP::get_ambient_temperature() const
{
    return 22.0;
}

// -----------------------------------------------------------------------------
void AMB::HolographyDSP::resetDSP(
    const std::vector<CAN::byte_t>& data,
    int size,
    const char* pMsg)
{
    checkSize(data,1,"RESET_DSP");
    m_dataProductBaseValue = 1LL;
    m_interval = 0; 
}

// -----------------------------------------------------------------------------
void AMB::HolographyDSP::checkSize(
    const std::vector<CAN::byte_t>& data,
    int size,
    const char* pExceptionMsg)
{
    if ((int)data.size() != size)
    {
	char exceptionMsg[200];
	sprintf(exceptionMsg,"%s data length is %d must be %d",pExceptionMsg,
		data.size(), size);
	throw HolographyDSPError(exceptionMsg);
    }
}

