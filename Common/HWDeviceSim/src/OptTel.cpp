// @(#) $Id$
//
// Copyright (C) 2001, 2005
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include "OptTel.h"
#include "AMBUtil.h"
#include <iomanip>
#include <iostream>
#include <iomanip>
#include <sstream>
using namespace std;

// ----------------------------------------------------------------------------
AMB::OptTel::OptTel(node_t node,
		    const vector<CAN::byte_t>& serialNumber)
  :node_m(node),
   sn_m(serialNumber),
   state_m(0x15), // heater off, motor off, ir used (in), 
                  // solar filter used (in), shutter closed (in)
   threshold_m(2.5),
   lastFocusCmd_m(0.0),
   transNum_m(0)
{
  cout << "Creating node 0x" << node << ", s/n 0x";
  for (int i = 0; i < 8; i++) {
    cout << hex << setw(2) << setfill('0')<< static_cast<int>(serialNumber[i]);
  }
  cout << ", Optical Telescope device" << endl;

  if (node != 0x1F) {
    ostringstream errMessage;
    errMessage << "Bad node number. It should be 0x1F but 0x"
	       << hex << node << " was specified." << endl;
    throw OptTelError(errMessage.str());
  }
}

// ----------------------------------------------------------------------------
AMB::OptTel::~OptTel() {
  // Nothing
}

// ----------------------------------------------------------------------------
AMB::node_t AMB::OptTel::node() const {
  return node_m;
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> AMB::OptTel::serialNumber() const {
    return sn_m;
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> AMB::OptTel::monitor(rca_t rca) const {
  transNum_m++;
  const vector<CAN::byte_t> tmp;
  
  switch (rca) {
    // Mandatory generic points
  case GET_SERIAL_NUM:
    return serialNumber();
    break;
  case GET_CAN_ERROR:
    return get_can_error();
    break;
  case GET_PROTOCOL_REV_LEVEL:
    return get_protocol_rev_level();
    break;
  case GET_SW_REV_LEVEL:
    return get_software_rev_level();
    break;
  case GET_HW_REV_LEVEL:
    return get_hardware_rev_level();
    break;
  case GET_TRANS_NUM:
    return longToData(tmp, get_trans_num());
    break;
    // Optional generic point
  case GET_AMBIENT_TEMPERATURE:
    return floatDS1820ToData(tmp, getAmbientTemperature());
    break;
    // OptTel specific points
  case GET_STATUS:
    return charToData(tmp, getStatus());
    break;
  case GET_PHOTODETECTOR:
    return shortToData(tmp, getPhotodetector());
    break;
  case GET_TUBE_TEMPERATURE:
    { // TODO: Move this conversion into AMBUtil.
      unsigned long *junk;
      float value = getTubeTemp();
      junk = reinterpret_cast<unsigned long *> (&value);
      return longToData(tmp, *junk);
    }
    break;
  case GET_CCD_TEMPERATURE:
    return shortToData(tmp, getCCDTemp());
    break;
  case GET_ELECTRONIC_TEMP1:
    {
      unsigned long *junk;
      float value = getElectronicTemp1();
      junk = reinterpret_cast<unsigned long *> (&value);
      return longToData(tmp, *junk);
    }
    break;
  case GET_ELECTRONIC_TEMP2:
    {
      unsigned long *junk;
      float value = getElectronicTemp2();
      junk = reinterpret_cast<unsigned long *> (&value);
      return longToData(tmp, *junk);
    }
    break;
  case GET_PHOTO_THRESHOLD:
    {
      unsigned long *junk;
      float value = getPhotoThreshold();
      junk = reinterpret_cast<unsigned long *> (&value);
      return longToData(tmp, *junk);
    }
    break;
  case GET_LIMIT_SWITCHES_STATUS:
    return charToData(tmp, getLimitSwitchStatus());
    break;
  case GET_SET_SHUTTER_POSITION:
    return charToData(tmp, getShutter());
    break;
  case GET_SET_SOLAR_FILTER_POSITION:
    return charToData(tmp, getSolarFilter());
    break;
  case GET_SET_IR_FILTER_POSITION:
    return charToData(tmp, getIRFilter());
    break;
  case GET_SET_ENABLE_12V:
    return charToData(tmp, getPower());
    break;
  case GET_SET_HEATER:
    return charToData(tmp, getHeater());
    break;
  case GET_SET_PHOTO_THRESHOLD:
    {
      unsigned long *junk;
      float value = getPhotoThreshold();
      junk = reinterpret_cast<unsigned long *> (&value);
      return longToData(tmp, *junk);
    }
    break;
  case GET_SET_MOVE_FOCUS:
    {
      unsigned long *junk;
      float value = getFocus();
      junk = reinterpret_cast<unsigned long *> (&value);
      return longToData(tmp, *junk);
    }
    break;
  default:
    ostringstream errMessage;
    errMessage << "Unknown RCA (0x" << hex << rca << ") in monitor command." << endl;
    throw OptTelError(errMessage.str());
  }
  // Should never get here
  return tmp;
}

// ----------------------------------------------------------------------------
void AMB::OptTel::control(rca_t rca, const vector<CAN::byte_t>& data) {
  transNum_m++;
  switch (rca) {
  case RESET_DEVICE:
    // TODO find out what the hardware does when hit with this command
    break;
  case SET_SHUTTER_POSITION:
    if (data.size() != 1) {
      ostringstream errMessage;
      errMessage << "SET_SHUTTER_POSITION data length must be 1 but " 
		 << data.size() << " bytes were received" << endl;
      throw OptTelError(errMessage.str());
    }
    setShutter(data[0]);
    break;
  case SET_SOLAR_FILTER_POSITION:
    if (data.size() != 1) {
      ostringstream errMessage;
      errMessage << "SET_SOLAR_FILTER_POSITION data length must be 1 but " 
		 << data.size() << " bytes were received" << endl;
    }
    setSolarFilter(data[0]);
    break;
  case SET_IR_FILTER_POSITION:
    if (data.size() != 1) {
      ostringstream errMessage;
      errMessage << "SET_IR_FILTER_POSITION data length must be 1 but " 
		 << data.size() << " bytes were received" << endl;
      throw OptTelError(errMessage.str());
    }
    setIRFilter(data[0]);
    break;
  case SET_ENABLE_12V:
    if (data.size() != 1) {
      ostringstream errMessage;
      errMessage << "SET_ENABLE_12V data length must be 1 but " 
		 << data.size() << " bytes were received" << endl;
      throw OptTelError(errMessage.str());
    }
    setPower(data[0]);
    break;
  case SET_HEATER:
    if (data.size() != 1) {
      ostringstream errMessage;
      errMessage << "SET_HEATER data length must be 1 but " 
		 << data.size() << " bytes were received" << endl;
      throw OptTelError(errMessage.str());
    }
    setHeater(data[0]);
    break;
  case SET_PHOTO_THRESHOLD:
    if (data.size() != 4) {
      ostringstream errMessage;
      errMessage << "SET_PHOTO_THRESHOLD data length must be 4 but " 
		 << data.size() << " bytes were received" << endl;
      throw OptTelError(errMessage.str());
    }
    {
      // TODO: Move this conversion into AMBUtil.
      vector<CAN::byte_t> dataCopy = data;
      long junk = dataToLong(dataCopy);
      float* asFloat =  reinterpret_cast<float *>(&junk);
      setPhotoThreshold(*asFloat);
    }
    break;
  case SET_MOVE_FOCUS:
    if (data.size() != 4) {
      ostringstream errMessage;
      errMessage << "SET_MOVE_FOCUS data length must be 4 but " 
		 << data.size() << " bytes were received" << endl;
      throw OptTelError(errMessage.str());
    }
    {
      // TODO: Move this conversion into AMBUtil.
      vector<CAN::byte_t> dataCopy = data;
      long junk = dataToLong(dataCopy);
      float* asFloat =  reinterpret_cast<float *>(&junk);
      moveFocus(*asFloat);
    }
    break;
  default:
      ostringstream errMessage;
      errMessage << "Unknown RCA (0x" << hex << rca 
		 << ") in control command." << endl;
    throw OptTelError(errMessage.str());
  }
}

// ----------------------------------------------------------------------------
unsigned int AMB::OptTel::get_trans_num() const {
  return transNum_m;
}

// ----------------------------------------------------------------------------
float AMB::OptTel::getAmbientTemperature() const {
  return 18.0;
}

// ----------------------------------------------------------------------------
CAN::byte_t AMB::OptTel::getStatus () const {
  return state_m;
}

// ----------------------------------------------------------------------------
short AMB::OptTel::getPhotodetector () const {
  return 0x100;
}

// ----------------------------------------------------------------------------
float AMB::OptTel::getTubeTemp() const {
  return 15.0;
}

// ----------------------------------------------------------------------------
short AMB::OptTel::getCCDTemp() const {
    return 0x0080;
}

// ----------------------------------------------------------------------------
float AMB::OptTel::getElectronicTemp1() const {
    return 20.0;
}

// ----------------------------------------------------------------------------
float AMB::OptTel::getElectronicTemp2() const {
    return 30.0;
}

// ----------------------------------------------------------------------------
float AMB::OptTel::getPhotoThreshold() const {
    return threshold_m;
}

// ----------------------------------------------------------------------------
CAN::byte_t AMB::OptTel::getLimitSwitchStatus() const {
    return 0x00;
}

// ----------------------------------------------------------------------------
void AMB::OptTel::setShutter (CAN::byte_t cmd) {
  const unsigned int value = static_cast<unsigned int>(cmd);
  if (value > 1) {
    throw OptTelError("OptTel::setShutter illegal cmd, must be in [0..1]");
  }
  if (value == 0) {
    state_m = state_m & ~0x2 | 0x1;       // shutter closed
  } else {
    state_m = state_m & ~0x1 | 0x2;       // shutter open
  }
}

// ----------------------------------------------------------------------------
CAN::byte_t AMB::OptTel::getShutter() const {
  if (  ((state_m & 0x02) != 0 ) && ((state_m & 0x01) == 0 )) {
    return 0x01;
  } if (((state_m & 0x02) == 0 ) && ((state_m & 0x01) != 0 )) {
    return 0x00;
  } 
  return 0x03;
}

// ----------------------------------------------------------------------------
void AMB::OptTel::setSolarFilter (CAN::byte_t cmd) {
  const unsigned int value = static_cast<unsigned int>(cmd);
  if (value > 1) {
    throw OptTelError("OptTel::setSolarFilter illegal cmd, must be in [0..1]");
  }
  if (value == 1) {
    state_m = state_m & ~0x4 | 0x8;       // filter open (out)
  } else {
    state_m = state_m & ~0x8 | 0x4;       // filter closed (in)
  }
}

// ----------------------------------------------------------------------------
CAN::byte_t AMB::OptTel::getSolarFilter() const {
  if (  ((state_m & 0x04) != 0 ) && ((state_m & 0x08) == 0 )) {
    return 0x00;
  } if (((state_m & 0x04) == 0 ) && ((state_m & 0x08) != 0 )) {
    return 0x01;
  } 
  return 0x03;
}

// ----------------------------------------------------------------------------
void AMB::OptTel::setIRFilter (CAN::byte_t cmd) {
  const unsigned int value = static_cast<unsigned int>(cmd);
  if (value > 1) {
    throw OptTelError("OptTel::setIRFilter illegal cmd, must be in [0..1]");
  }
  if (value == 0) {
    state_m = state_m & ~0x10 | 0x20;       // filter out
  } else {
    state_m = state_m & ~0x20 | 0x10;       // filter in
  }
}

// ----------------------------------------------------------------------------
CAN::byte_t AMB::OptTel::getIRFilter() const {
  if (  ((state_m & 0x10) != 0 ) && ((state_m & 0x20) == 0 )) {
    return 0x01;
  } if (((state_m & 0x10) == 0 ) && ((state_m & 0x20) != 0 )) {
    return 0x00;
  } 
  return 0x03;
}

// ----------------------------------------------------------------------------
void AMB::OptTel::setPower (CAN::byte_t cmd) {
  const unsigned int value = static_cast<unsigned int>(cmd);
  if (value > 1) {
    throw OptTelError("setPower illegal cmd, must be in [0..1]");
  }
  if (value == 0) {
    state_m = state_m & ~0x40;        // power off
  } else {
    state_m = state_m | 0x40;         // power on
  }
}

// ----------------------------------------------------------------------------
CAN::byte_t AMB::OptTel::getPower() const {
  if ((state_m & 0x40) != 0 ) {
    return 0x01;
  } else {
    return 0x00;
  }
}

// ----------------------------------------------------------------------------
void AMB::OptTel::setHeater (CAN::byte_t cmd) {
  const unsigned int value = static_cast<unsigned int>(cmd);
  if (value > 1) {
    throw OptTelError("setHeater illegal cmd, must be in [0..1]");
  }
  if (cmd == 0) {
    state_m = state_m & ~0x80;        // filter in, closed
  } else {
    state_m = state_m | 0x80;         // filter out, open
  }
}

// ----------------------------------------------------------------------------
CAN::byte_t AMB::OptTel::getHeater() const {
  if ((state_m & 0x80) != 0 ) {
    return 0x01;
  } else {
    return 0x00;
  }
}

// ----------------------------------------------------------------------------
void AMB::OptTel::setPhotoThreshold(float threshold) {
  if (threshold > 2.5) {
    threshold_m = 2.5;
  } else {
    threshold_m = threshold;
  }
}

// ----------------------------------------------------------------------------
void AMB::OptTel::moveFocus(float distance) {
  lastFocusCmd_m = distance;
}

// ----------------------------------------------------------------------------
float AMB::OptTel::getFocus() const {
  return lastFocusCmd_m;
}
