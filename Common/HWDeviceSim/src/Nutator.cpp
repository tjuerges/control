// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include <iomanip>
#include "Nutator.h"
#include "AMBUtil.h"

// -----------------------------------------------------------------------------

AMB::Nutator::Nutator(
	node_t node,
	const std::vector<CAN::byte_t> &serialNumber)
  : node_m(node),sn_m(serialNumber),
    status_m(0),selftest_m(0),
    trans_num_m(0),stop_time_m(0)
{
    std::cout << "Creating node " << node << ", s/n 0x";
    for (int i = 0; i < 8; i++)
	std::cout << std::hex << std::setw(2) << std::setfill('0') << (int)serialNumber[i];
    std::cout << ", Nutator device" << std::endl;

    // initialize control loop coefficients
    loop_m[0][0] = 0;
    loop_m[0][1] = 0;
    loop_m[0][2] = 0;
    loop_m[1][0] = 0;
    loop_m[1][1] = 0;
    loop_m[1][2] = 0;
    
    // initialize program points
    for (int i = 0; i < 2; i++)
    {
	for (int j = 0; j < MAX_POINTS_IN_PROGRAM; j++)
	{
	    programs_m[i].points[j].position = 0;
	    programs_m[i].points[j].transition_time = 0;
	    programs_m[i].points[j].dwell_time = 0;
	}
    }
    active_program_m  = &programs_m[0];
    standby_program_m = &programs_m[1];

    // TODO - validate node & sn
}

// -----------------------------------------------------------------------------

AMB::Nutator::~Nutator()
{
    // Nothing
}

// -----------------------------------------------------------------------------

AMB::node_t AMB::Nutator::node() const
{
    return node_m;
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::Nutator::serialNumber() const
{
    return sn_m;
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::Nutator::monitor(
	rca_t rca) const
{
    // empty CAN message
    const std::vector<CAN::byte_t> nostring;

    // bump transaction count
    trans_num_m++;

    switch (rca)
	{

	// mandatory standard generic monitor points
	    
	case GET_PROTOCOL_REV_LEVEL:
	    return Device::get_protocol_rev_level();
	    
	case AMB::Device::GET_HW_REV_LEVEL:
		return get_hardware_rev_level();
		break;

	case GET_CAN_ERROR:
	    return Device::get_can_error();

	case GET_TRANS_NUM:
	    return AMB::longToData(nostring,get_trans_num());

	case GET_AMBIENT_TEMPERATURE:
	    return AMB::floatDS1820ToData(nostring,get_control_box_temp());

	case GET_SW_REV_LEVEL:
	    return get_sw_rev_level();

	// Nutator specific monitor points (order from ICD)

	case GET_STATUS:
	    return AMB::charToData(nostring,get_status());

	case GET_POSITION:
        {
	    return AMB::shortToData(nostring,get_position()); 
	}

	case GET_ACTIVE_PROG_SEG_0:
	case GET_ACTIVE_PROG_SEG_1:
	case GET_ACTIVE_PROG_SEG_2:
	case GET_ACTIVE_PROG_SEG_3:
	case GET_ACTIVE_PROG_SEG_4:
	case GET_ACTIVE_PROG_SEG_5:
	case GET_ACTIVE_PROG_SEG_I:
        {
            return get_active_program_point(rca - GET_ACTIVE_PROG_SEG_0);
	}

	case GET_STANDBY_PROG_SEG_0:
	case GET_STANDBY_PROG_SEG_1:
	case GET_STANDBY_PROG_SEG_2:
	case GET_STANDBY_PROG_SEG_3:
	case GET_STANDBY_PROG_SEG_4:
	case GET_STANDBY_PROG_SEG_5:
	case GET_STANDBY_PROG_SEG_I:
        {
 	    return get_standby_program_point(rca - GET_STANDBY_PROG_SEG_0);
	}

	case GET_PROGRAM_VALIDITY:
	    return AMB::charToData(nostring,get_program_validity());

	case GET_LOOP1_P:
	    return AMB::floatToData(nostring,loop_m[0][0],
				    -100000.0,100000.0,32);

	case GET_LOOP1_I:
	    return AMB::floatToData(nostring,loop_m[0][1],
				    -100000.0,100000.0,32);

	case GET_LOOP1_D:
	    return AMB::floatToData(nostring,loop_m[0][2],
				    -100000.0,100000.0,32);

	case GET_LOOP2_P:
	    return AMB::floatToData(nostring,loop_m[1][0],
				-100000.0,100000.0,32);

	case GET_LOOP2_I:
	    return AMB::floatToData(nostring,loop_m[1][1],
				    -100000.0,100000.0,32);

	case GET_LOOP2_D:
	    return AMB::floatToData(nostring,loop_m[1][2],
				    -100000.0,100000.0,32);

	case GET_APEX_PS_STATUS:
	    return AMB::longToData(nostring,get_apex_ps_status());
	    
	case GET_LINAMP_PS_STATUS:
	    return AMB::longToData(nostring,get_linamp_ps_status());

	case GET_CONTROLLER_PS_STATUS:
	    return AMB::longToData(nostring,get_controller_ps_status());

	case GET_SELFTEST_RESULT:
        {
	    return shortToData(nostring,get_selftest_result());
	}
	    
	case GET_TEMPERATURE_0:
	case GET_TEMPERATURE_1:
	case GET_TEMPERATURE_2:
	case GET_TEMPERATURE_3:
	case GET_TEMPERATURE_4:
	case GET_TEMPERATURE_5:
	case GET_TEMPERATURE_6:
	case GET_TEMPERATURE_7:
	case GET_TEMPERATURE_8:
	case GET_TEMPERATURE_9:
	case GET_TEMPERATURE_A:
	case GET_TEMPERATURE_B:
	case GET_TEMPERATURE_C:
	case GET_TEMPERATURE_D:
	case GET_TEMPERATURE_E:
	case GET_TEMPERATURE_F:
  	    return AMB::floatToData(nostring,get_temperature_x(),
  				    -100000.0,100000.0,32);

	default:
	    std::cerr << "Switch does not match any case" << std::endl;
	    throw NutatorError("Unknown RCA in monitor command");
	}
}

// -----------------------------------------------------------------------------

void AMB::Nutator::control(rca_t rca,const std::vector<CAN::byte_t> &data)
{
    // bump transaction count
    trans_num_m++;

    switch (rca)
	{
	// mandatory standard generic control point
	    
	case RESET_DEVICE:
	    {
	    if (data.size() != 1)
		{
		throw NutatorError("RESET_DEVICE data length "
				      "must be 1");
		}
	    reset_device(data[0]);
	    }
	    break;

	// Nutator specific control points (order from ICD)

	case CLEAR_STANDBY_PROGRAM:
	    clear_standby_program(0);
	    break;

	case RESET_STATUS:
	    reset_status(0);
	    break;

	case START_PROGRAM:
	    start_program(0);
	    break;

	case STOP_PROGRAM:
	{
	    std::vector<CAN::byte_t> temp(data);
	    stop_program(dataToChar(temp));
	}
	break;

	case ABORT_PROGRAM:
	    abort_program(0);
	    break;

	case DO_SELFTEST:
            {
		do_selftest(0);
	    }
	    break;

	case SET_LOOP1_P:
            {
	    std::vector<CAN::byte_t> temp(data);
	    loop_m[0][0] = dataToFloat(temp,-100000.0,100000.0,32);
	    }
	    break;

	case SET_LOOP1_I:
            {
	    std::vector<CAN::byte_t> temp(data);
	    loop_m[0][1] = dataToFloat(temp,-100000.0,100000.0,32);
	    }
	    break;

	case SET_LOOP1_D:
            {
	    std::vector<CAN::byte_t> temp(data);
	    loop_m[0][2] = dataToFloat(temp,-100000.0,100000.0,32);
	    }
	    break;

	case SET_LOOP2_P:
            {
	    std::vector<CAN::byte_t> temp(data);
	    loop_m[1][0] = dataToFloat(temp,-100000.0,100000.0,32);
	    }
	    break;

	case SET_LOOP2_I:
            {
	    std::vector<CAN::byte_t> temp(data);
	    loop_m[1][1] = dataToFloat(temp,-100000.0,100000.0,32);
	    }
	    break;

	case SET_LOOP2_D:
            {
	    std::vector<CAN::byte_t> temp(data);
	    loop_m[1][2] = dataToFloat(temp,-100000.0,100000.0,32);
	    }
	    break;

	case SET_STANDBY_PROG_SEG_0:
	case SET_STANDBY_PROG_SEG_1:
	case SET_STANDBY_PROG_SEG_2:
	case SET_STANDBY_PROG_SEG_3:
	case SET_STANDBY_PROG_SEG_4:
	case SET_STANDBY_PROG_SEG_5:
	{
	    std::vector<CAN::byte_t> temp(data);
	    set_standby_program_point(rca - SET_STANDBY_PROG_SEG_0, temp);
	}
	break;

	default:
	    throw NutatorError("Unknown RCA in control command");
	}
}

// - MONITOR points ------------------------------------------------------------

// -----------------------------------------------------------------------------
// Returns the status byte.  Zero-th bit indicates running condition.  In this simulator 
// the status is determined only when a call is made to this function.  This is done 
// by getting the current system time and then figuring out what the status should be 
// based on any pending stop program commands.
unsigned char AMB::Nutator::get_status() const
{
    struct timeval  now_time;
    struct timezone tz;

    gettimeofday(&now_time,&tz);
    long int elapsed_msec = (now_time.tv_sec  - start_time_m.tv_sec)  * 1000
	                  + (now_time.tv_usec - start_time_m.tv_usec) / 1000;

    // Running status may have changed since last due to a pending stop program command
    if ((stop_time_m != 0) && (elapsed_msec > stop_time_m))
    {
	stop_time_m = 0;
	status_m = status_m & 0xfe;
    }
    return status_m;
}

// -----------------------------------------------------------------------------
// Returns the nutator position at current time.  In this simulator the current 
// position is determined only when a call is made to this function.  This is done 
// by getting the current system time and then figuring out which point should have 
// been reached.  The nutator keeps on cycling through the active program until a 
// stop/abort command is received.
unsigned short int  AMB::Nutator::get_position() const
{
    struct timeval  now_time;
    struct timezone tz;

    unsigned short int position = 0;

    gettimeofday(&now_time,&tz);
    long int elapsed_msec = (now_time.tv_sec  - start_time_m.tv_sec)  * 1000
	                  + (now_time.tv_usec - start_time_m.tv_usec) / 1000;

    // If program is not running return last reached position
    if ((status_m & 1) == 0)
	return active_program_m->current_point.position;

    int rem_time = 0; // time into current program cycle
    if ((stop_time_m != 0) && (elapsed_msec > stop_time_m))
    { // There is a pending stop program command -- use stop time for time into current cycle
      // and update status.
	stop_time_m = 0;
	status_m = status_m & 0xfe;
	if (active_program_m->program_time != 0)
	    rem_time = stop_time_m % active_program_m->program_time;
    }
    else
    {
	if (active_program_m->program_time != 0)
	    rem_time = elapsed_msec % active_program_m->program_time;
    }

    // Use previous position if we are still in first transition 
    if (rem_time < active_program_m->points[0].transition_time)
	return active_program_m->current_point.position;

    int counter = 0; // index to point that nutator should have reached at current time

    while (rem_time >= 0)
    {
	position = active_program_m->points[counter].position;
	rem_time -= (active_program_m->points[counter].transition_time
	          + active_program_m->points[counter].dwell_time);
	counter++;
    }
    // We need to update the current position
    active_program_m->current_point.position = position;

    return position;
}

// -----------------------------------------------------------------------------
// Return active program point specified by pointIndex
std::vector<CAN::byte_t> AMB::Nutator::get_active_program_point(int pointIndex) const
{
    AMB::Nutator::ProgramPoint *pPoint = NULL;
    std::vector<CAN::byte_t> tmp;

    if (MAX_POINTS_IN_PROGRAM > pointIndex)
    {
	pPoint = &active_program_m->points[pointIndex];
	if (pPoint != NULL)
	{
	    tmp = shortToData(tmp, pPoint->position);
	    tmp = shortToData(tmp, pPoint->transition_time);
	    tmp = shortToData(tmp, pPoint->dwell_time);
	    active_program_m->current_point = *pPoint;
	}
    }
    else
    {
	tmp = shortToData(tmp, active_program_m->current_point.position);
	tmp = shortToData(tmp, active_program_m->current_point.transition_time);
	tmp = shortToData(tmp, active_program_m->current_point.dwell_time);
    }
  
    return tmp;
}

// -----------------------------------------------------------------------------
// Return standby program point specified by pointIndex

std::vector<CAN::byte_t> AMB::Nutator::get_standby_program_point(int pointIndex) const
{
    AMB::Nutator::ProgramPoint *pPoint = NULL;
    std::vector<CAN::byte_t> tmp;

    if (MAX_POINTS_IN_PROGRAM > pointIndex)
    {
	pPoint = &standby_program_m->points[pointIndex];
	if (pPoint != NULL)
	{
	    tmp = shortToData(tmp, pPoint->position);
	    tmp = shortToData(tmp, pPoint->transition_time);
	    tmp = shortToData(tmp, pPoint->dwell_time);
	}
    }
  
    return tmp;
}

// -----------------------------------------------------------------------------
// Program validity is always 1 for this simulator
unsigned char AMB::Nutator::get_program_validity() const
{
    return 1;
}

// -----------------------------------------------------------------------------
// Apex power supply status is always 0 for this simulator

unsigned  int AMB::Nutator::get_apex_ps_status() const
{
    return 0;
}

// -----------------------------------------------------------------------------
// Linear amplifier power supply status is always 0 for this simulator

unsigned  int AMB::Nutator::get_linamp_ps_status() const
{
    return 0;
}

// -----------------------------------------------------------------------------
// Controller power supply is always 0 for this simulator

unsigned  int AMB::Nutator::get_controller_ps_status() const
{
    return 0;
}

// -----------------------------------------------------------------------------
// Return results of self test.  For this simulator it is either 0 - no selftest
// commanded or  2 - self test completed
unsigned short AMB::Nutator::get_selftest_result() const
{
    return selftest_m;
}

// -----------------------------------------------------------------------------
// Temperature is always 18 C  for this simulator

float AMB::Nutator::get_temperature_x() const
{
    return 18.0;
}

// --- Standard monitor points-------------------------------------------------

//-----------------------------------------------------------------------------

unsigned int AMB::Nutator::get_trans_num() const
{
    return trans_num_m;
}

// -----------------------------------------------------------------------------
//  is always  for this simulator

float AMB::Nutator::get_control_box_temp() const
{
    return 18.0;
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::Nutator::get_sw_rev_level() const
{
    std::vector<CAN::byte_t> tmp(3);
    tmp[0] = tmp[1] = tmp[2] = 0; // 0.0.0
    return tmp;
}

// - CONTROL points ------------------------------------------------------------

// -----------------------------------------------------------------------------
// Set the specified standby program point to the values passed in data.  Update the
// standby program time accordingly.
void AMB::Nutator::set_standby_program_point(unsigned int pointIndex, std::vector<CAN::byte_t>& data)
{
    standby_program_m->program_time -= 
	(standby_program_m->points[pointIndex].transition_time + 
		standby_program_m->points[pointIndex].dwell_time);
    standby_program_m->points[pointIndex].position = dataToShort(data);
    standby_program_m->points[pointIndex].transition_time = dataToShort(data);
    standby_program_m->points[pointIndex].dwell_time = dataToShort(data);
    standby_program_m->program_time += 
	(standby_program_m->points[pointIndex].transition_time + 
		standby_program_m->points[pointIndex].dwell_time);

}

// -----------------------------------------------------------------------------
// Reset all standby program point values and program time to 0 
void AMB::Nutator::clear_standby_program(unsigned char dummy)
{
    for (int j = 0; j < MAX_POINTS_IN_PROGRAM; j++)
    {
	standby_program_m->points[j].position = 0;
	standby_program_m->points[j].transition_time = 0;
	standby_program_m->points[j].dwell_time = 0;
    }
    standby_program_m->program_time = 0;
}

// -----------------------------------------------------------------------------
// Reset status byte 
void AMB::Nutator::reset_status(unsigned char dummy)
{
    status_m = 0;
}

// -----------------------------------------------------------------------------
// Load standby program, set status to running, and set program start time
void AMB::Nutator::start_program(unsigned char dummy)
{
    struct timezone tz;

    status_m = status_m | 1;
    load_standby_program(dummy);
    gettimeofday(&start_time_m,&tz);
}

// -----------------------------------------------------------------------------
// Stop active program when nutator reaches position indicated by prog_point.  To
// do this we just set the time at which the program should stop.  The actual stop
// will pend until the system time matches or surpasses the pending time.  
void AMB::Nutator::stop_program(unsigned char prog_point)
{
    struct timeval  now_time;
    struct timezone tz;

    gettimeofday(&now_time,&tz);
    long int time_into_program = (now_time.tv_sec  - start_time_m.tv_sec)  * 1000
	                  + (now_time.tv_usec - start_time_m.tv_usec) / 1000;

    int rem_time = 0; // time into current program cycle
    if (active_program_m->program_time != 0)
	rem_time = time_into_program % active_program_m->program_time;
    int current_point = 0;

    // Find point that nutator is at currently
    while (rem_time >= 0)
    {
	rem_time -= (active_program_m->points[current_point].transition_time
	          + active_program_m->points[current_point].dwell_time);
	current_point++;
    }
    stop_time_m = time_into_program;

    if  (current_point < prog_point)
    {// The point to stop at is in this program cycle
	for (int i = current_point; i < prog_point; i++)
	{
	    stop_time_m += (active_program_m->points[i].transition_time + 
					active_program_m->points[i].dwell_time);
	}
    }
    else
    {// The point to stop at is in the next program cycle
	prog_point += MAX_POINTS_IN_PROGRAM;
	for (int i = current_point; i < prog_point; i++)
	{
	    stop_time_m += (active_program_m->points[i % MAX_POINTS_IN_PROGRAM].transition_time + 
				 active_program_m->points[i % MAX_POINTS_IN_PROGRAM].dwell_time);
	}
    }
}

// -----------------------------------------------------------------------------
// Stops active program immediately
void AMB::Nutator::abort_program(unsigned char dummy)
{
    status_m = status_m & 0xfe;
    stop_time_m = 0;
}

// -----------------------------------------------------------------------------
// For this simulator, the same as abort
void AMB::Nutator::stow(unsigned char dummy)
{
    abort_program(0);
}

// -----------------------------------------------------------------------------
// Copies the standby program to the active program and clears the standby program
void AMB::Nutator::load_standby_program(unsigned char dummy)
{
    for (int j = 0; j < MAX_POINTS_IN_PROGRAM; j++)
    {
	active_program_m->points[j].position = standby_program_m->points[j].position;
	active_program_m->points[j].transition_time = standby_program_m->points[j].transition_time;
	active_program_m->points[j].dwell_time = standby_program_m->points[j].dwell_time;
    }
    active_program_m->program_time = standby_program_m->program_time;
    clear_standby_program(0);
}

// -----------------------------------------------------------------------------
// This just sets selftest_m to 2 -- completed
void AMB::Nutator::do_selftest(unsigned char dummy)
{
    selftest_m = 2; // Completed. Value
}

// - standard control points ---------------------------------------------------

void AMB::Nutator::reset_device(unsigned char)
{
    abort_program(0);
}
