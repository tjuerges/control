// @(#) $Id$
//
// Copyright (C) 2002
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include <iomanip>
#include "CRD.h"
#include "AMBUtil.h"

#include <stdio.h>
#include <sys/time.h>

// ----------------------------------------------------------------------------
AMB::CRD::CRD(node_t node, const std::vector<CAN::byte_t> &serialNumber)
  : m_node(node),
    m_sn(serialNumber),
    m_trans_num(0),
    m_errorCode(1),
    RESET_TIME_m(0),
    EFC_5_MHZ_m(1.27),
    PWR_5_MHZ_m(0.683),
    PWR_25_MHZ_m(1.362),
    PWR_125_MHZ_m(0.351),
    PWR_2_GHZ_m(1.83),
    LASER_CURRENT_m(57),
    VDC_MINUS_5_m(-5.3),
    VDC_15_m(15.2),
    STATUS_m(1)
{
  std::cout << "Creating node " << node << ",s/n 0x";
  for (int i = 0; i < 8; i++) {
    std::cout << std::hex << std::setw(2) << std::setfill('0')
          << static_cast<int>(serialNumber[i]);
  }
  std::cout << ", CRD device" << std::endl;

  // get the start time
  gettimeofday(&start_m, 0);
}

// ----------------------------------------------------------------------------
AMB::CRD::~CRD() {
  // Nothing
}

// ----------------------------------------------------------------------------
AMB::node_t AMB::CRD::node() const {
  return m_node;
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::CRD::serialNumber() const {
  return m_sn;
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::CRD::monitor(rca_t rca) const {
  m_trans_num++;
  const std::vector<CAN::byte_t> tmp;
  std::vector<CAN::byte_t> retVal;

  switch (rca) {
    // Mandatory generic points
  case GET_SERIAL_NUMBER:
    return serialNumber();
    break;
  case GET_CAN_ERROR:
    return get_can_error();
    break;
  case GET_PROTOCOL_REV_LEVEL:
    return get_protocol_rev_level();
    break;
    case AMB::Device::GET_SW_REV_LEVEL:
        return get_software_rev_level();
        break;
    case AMB::Device::GET_HW_REV_LEVEL:
        return get_hardware_rev_level();
        break;
  case GET_TRANS_NUM:
    return longToData(tmp, get_trans_num());
    break;
    // Optional generic point
  case GET_AMBIENT_TEMPERATURE:
    return floatDS1820ToData(tmp, get_ambient_temperature());
    break;

    // CRD specific points
  case GET_RESET_TIME:
    return CAN_GET_RESET_TIME();
    break;
  case MASER_VS_GPS_COUNTER:
    return CAN_MASER_VS_GPS_COUNTER();
    break;
  case MASER_COUNTER:
    return CAN_MASER_COUNTER();
    break;
  case EFC_5_MHZ:
    return CAN_EFC_5_MHZ();
    break;
  case PWR_5_MHZ:
    return CAN_PWR_5_MHZ();
    break;
  case PWR_25_MHZ:
    return CAN_PWR_25_MHZ();
    break;
  case PWR_125_MHZ:
    return CAN_PWR_125_MHZ();
    break;
  case PWR_2_GHZ:
    return CAN_PWR_2_GHZ();
    break;
  case LASER_CURRENT:
    return CAN_LASER_CURRENT();
    break;
  case VDC_MINUS_5:
    return CAN_VDC_MINUS_5();
    break;
  case VDC_15:
    return CAN_VDC_15();
    break;
  case STATUS:
    return CAN_STATUS();
    break;
  default:
    std::cerr << "Switch '" << static_cast<int>(rca)
          << "' does not match any CRD case." << std::endl;
    throw CRDError("Unknown RCA in monitor command");
  }
}

// ----------------------------------------------------------------------------
void AMB::CRD::control(rca_t rca, const std::vector<CAN::byte_t> &data) {
    std::vector<CAN::byte_t> temp(data);
    m_trans_num++;
//    printf("AMBSimulator:CRD received control command 0x%x\n", rca);
    switch (rca) {
    case SET_RESET_TIME:
    checkSize(data, 8, "Set Reset Time");
    RESET_TIME_m = dataToLonglong(temp, 8);
    break;
    case XILINX_PROM_RESET:
    checkSize(data, 1, "Xilinx Prom Reset");
    STATUS_m = 0;
    break;
    case MASTER_COUNTER_RESET:
    checkSize(data, 1, "Master Counter Reset");
    if ((data[0] && 1) == 1)
        {
        gettimeofday(&start_m, 0);
        if (start_m.tv_usec != 0)
        {
        start_m.tv_sec++;
        start_m.tv_usec = 0;
        }
//        unsigned long long seconds = 12219292800;
//        seconds += start_m.tv_sec;
//        int sec1 = seconds/1000000;
//        int sec2 = seconds%1000000;
//        printf ("Master counter reset at time %i%i seconds\n", sec1, sec2);
        }
    break;
    default:
    throw CRDError("Unknown RCA in control command module CRD");
    }
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::CRD::CAN_GET_RESET_TIME() const {
  std::vector<CAN::byte_t> tmp;
  return longlongToData(tmp, (long long)RESET_TIME_m, 8);
}

std::vector<CAN::byte_t> AMB::CRD::CAN_MASER_VS_GPS_COUNTER() const{
  std::vector<CAN::byte_t> tmp;
  timeval timenow;
  unsigned long long maserCount;

  gettimeofday(&timenow, 0);
//  printf ("Simulator:CRD::CAN_MASER_VS_GPS_COUNTER: start time %i.%i, act time %i.%i\n",
//          start_m.tv_sec, start_m.tv_usec, timenow.tv_sec, timenow.tv_usec);

  maserCount = static_cast< unsigned long long >(
      timenow.tv_sec - start_m.tv_sec) * 125000000ULL;
//  printf ("Simulator:CRD::CAN_MASER_VS_GPS_COUNTER: maserCount case 1 %llu\n", maserCount);

  return longlongToData(tmp, (long long)maserCount, 8);
}

std::vector<CAN::byte_t> AMB::CRD::CAN_MASER_COUNTER() const
{
  std::vector<CAN::byte_t> tmp;
  timeval timenow;
//  timeval timediff;
  unsigned long long maserCount;

  gettimeofday(&timenow, 0);
//  timediff.tv_sec = timenow.tv_sec - start_m.tv_sec;
//  timediff.tv_usec = timenow.tv_sec;
//  printf ("Simulator:CRD::CAN_MASER_COUNTER: start time %i.%i, act time %i.%i\n",
//          start_m.tv_sec, start_m.tv_usec, timenow.tv_sec, timenow.tv_usec);
//  printf("imulator:CRD::CAN_MASER_COUNTER: time diff = %i.%i\n",
//     timediff.tv_sec, timediff.tv_usec);
  unsigned long long secs(static_cast< unsigned long long >(
      timenow.tv_sec - start_m.tv_sec));
  long usecs(timenow.tv_usec - start_m.tv_usec);
  if(usecs < 0L)
  {
      usecs *= -1L;
      --secs;
  }

  maserCount = (secs * 1000000ULL + static_cast< unsigned long long >(usecs))
      * 125ULL;
//  printf ("Simulator:CRD::CAN_MASER_COUNTER: maserCount case 1 %llu\n", maserCount);

  return longlongToData(tmp, static_cast< long long >(maserCount), 8);
}

std::vector<CAN::byte_t> AMB::CRD::CAN_EFC_5_MHZ() const {
  std::vector<CAN::byte_t> tmp;
  const short int roundedValue =
    static_cast<short int>(EFC_5_MHZ_m*4095.0/20.0);
  return shortToData(tmp, roundedValue);
}

std::vector<CAN::byte_t> AMB::CRD::CAN_PWR_5_MHZ() const {
  std::vector<CAN::byte_t> tmp;
  const short int roundedValue =
    static_cast<short int>(PWR_5_MHZ_m*4095.0/20.0);
  return shortToData(tmp, roundedValue);
}

std::vector<CAN::byte_t> AMB::CRD::CAN_PWR_25_MHZ() const {
  std::vector<CAN::byte_t> tmp;
  const short int roundedValue =
    static_cast<short int>(PWR_25_MHZ_m*4095.0/20.0);
  return shortToData(tmp, roundedValue);
}

std::vector<CAN::byte_t> AMB::CRD::CAN_PWR_125_MHZ() const {
  std::vector<CAN::byte_t> tmp;
  const short int roundedValue =
    static_cast<short int>(PWR_125_MHZ_m*4095.0/20.0);
  return shortToData(tmp, roundedValue);
}

std::vector<CAN::byte_t> AMB::CRD::CAN_PWR_2_GHZ() const {
  std::vector<CAN::byte_t> tmp;
  const short int roundedValue =
    static_cast<short int>(PWR_2_GHZ_m*4095.0/20.0);
  return shortToData(tmp, roundedValue);
}

std::vector<CAN::byte_t> AMB::CRD::CAN_LASER_CURRENT() const {
  std::vector<CAN::byte_t> tmp;
  const short int roundedValue =
    static_cast<short int>(LASER_CURRENT_m*4095.0/2000.0);
  return shortToData(tmp, roundedValue);
}

std::vector<CAN::byte_t> AMB::CRD::CAN_VDC_MINUS_5() const {
  std::vector<CAN::byte_t> tmp;
  const short int roundedValue =
    static_cast<short int>(VDC_MINUS_5_m*4095.0/40.0);
  return shortToData(tmp, roundedValue);
}

std::vector<CAN::byte_t> AMB::CRD::CAN_VDC_15() const {
  std::vector<CAN::byte_t> tmp;
  const short int roundedValue =
    static_cast<short int>(VDC_15_m*4095.0/40.0);
  return shortToData(tmp, roundedValue);
}

std::vector<CAN::byte_t> AMB::CRD::CAN_STATUS() const{
  std::vector<CAN::byte_t> tmp;
  tmp.push_back(0x1);
  return(tmp);
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::CRD::get_can_error() const {
  return AMB::Device::get_can_error();
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::CRD::get_protocol_rev_level() const {
  return AMB::Device::get_protocol_rev_level();
}

// ----------------------------------------------------------------------------
unsigned int AMB::CRD::get_trans_num() const {
  return m_trans_num;
}

// ----------------------------------------------------------------------------
double AMB::CRD::get_ambient_temperature() const {
  return 22.4;
}

// ----------------------------------------------------------------------------
void AMB::CRD::checkSize(const std::vector<CAN::byte_t> &data, int size,
              const char *pExceptionMsg) {
  if( static_cast<int>(data.size()) != size ) {
    char exceptionMsg[200];
    sprintf(exceptionMsg,"%s data length is %d must be %d",pExceptionMsg,
        data.size(), size);
    throw CRDError(exceptionMsg);
  }
}

/// Get functions for unit testing
// ----------------------------------------------------------------------------
unsigned long long AMB::CRD::getRESET_TIME() {
  return RESET_TIME_m;
}

unsigned long long AMB::CRD::getMASER_VS_GPS_COUNTER() {
  timeval timenow;
  unsigned long long maserCount;

  gettimeofday(&timenow, 0);
//  printf ("Simulator:CRD::CAN_MASER_VS_GPS_COUNTER: start time %i.%i, act time %i.%i\n",
//          start_m.tv_sec, start_m.tv_usec, timenow.tv_sec, timenow.tv_usec);
  maserCount = (unsigned long long)(timenow.tv_sec - start_m.tv_sec)
      * 125000000ULL;
//  printf ("Simulator:CRD::CAN_MASER_VS_GPS_COUNTER: maserCount case 1 %llu\n", maserCount);

  return maserCount;
}

unsigned long long AMB::CRD::getMASER_COUNTER() {
  timeval timenow;
  unsigned long long maserCount;

  gettimeofday(&timenow, 0);
//  printf ("Simulator:CRD::CAN_MASER_COUNTER: start time %i.%i, act time %i.%i\n",
//          start_m.tv_sec, start_m.tv_usec, timenow.tv_sec, timenow.tv_usec);
  unsigned long long secs(static_cast< unsigned long long >(
      timenow.tv_sec - start_m.tv_sec));
  long usecs(timenow.tv_usec - start_m.tv_usec);
  if(usecs < 0L)
  {
      usecs *= -1L;
      --secs;
  }

  maserCount = (secs * 1000000ULL + static_cast< unsigned long long >(usecs))
      * 125ULL;
//  printf ("Simulator:CRD::CAN_MASER_COUNTER: maserCount case 1 %llu\n", maserCount);

  return maserCount;
}

short AMB::CRD::getEFC_5_MHZ() {
  return static_cast<short int>(EFC_5_MHZ_m*4095.0/20.0);
}

short AMB::CRD::getPWR_5_MHZ() {
  return static_cast<short int>(PWR_5_MHZ_m*4095.0/20.0);
}

short AMB::CRD::getPWR_25_MHZ() {
  return static_cast<short int>(PWR_25_MHZ_m*4095.0/20.0);
}

short AMB::CRD::getPWR_125_MHZ() {
  return static_cast<short int>(PWR_125_MHZ_m*4095.0/20.0);
}

short AMB::CRD::getPWR_2_GHZ() {
  return static_cast<short int>(PWR_2_GHZ_m*4095.0/20.0);
}

short AMB::CRD::getLASER_CURRENT() {
  return static_cast<short int>(LASER_CURRENT_m*4095.0/2000.0);
}

short AMB::CRD::getVDC_MINUS_5() {
  return static_cast<short int>(VDC_MINUS_5_m*4095.0/40.0);
}

short AMB::CRD::getVDC_15() {
  return static_cast<short int>(VDC_15_m*4095.0/40.0);
}

CAN::byte_t  AMB::CRD::getSTATUS() const{
  return 0x1;
}













