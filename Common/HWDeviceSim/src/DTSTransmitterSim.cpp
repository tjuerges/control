
// @(#) $Id$
//
// Copyright (C) 2002
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include "DTSTransmitterSim.h"
#include "AMBUtil.h"
#include <iomanip>
#include <iterator>
#include <iostream>
#include <sstream>
#include <cmath>


/**
 * Define ADD_NOISE for adding noise to some values. This will in case of too much
 * noise trigger alarms.
#define ADD_NOISE
 */

/**
 * The FPGA firmware revisions are different for each of the three channels.
 * From the DTX-ICD (rev. 2006-06-16):
Byte 6
Ch 1 - Bit D - Module serial number
Ch 2 - Bit C - zero
Ch 3 - Bit B -
Byte 7
Ch 1 - Bit D - Module serial number
Ch 2 - Bit C - Module Can Address (0x50 - 0x53)
Ch 3 - Bit B -
 */

const unsigned char AMB::DTSTransmitter::
    FirmwareRevision_BitD[8] = {'\xbe', '\x40', '\x02', '\x06', '\x06', '\x06', '\xde', '\xad'};
const unsigned char AMB::DTSTransmitter::
    FirmwareRevision_BitC[8] = {'\xbe', '\x40', '\x02', '\x06', '\x06', '\x06', '\x00', '\x50'};
const unsigned char AMB::DTSTransmitter::
    FirmwareRevision_BitB[8] = {'\xbe', '\x40', '\x02', '\x06', '\x06', '\x06', '\xbe', '\xef'};

// ----------------------------------------------------------------------------
AMB::DTSTransmitter::DTSTransmitter(node_t node,
                    const std::vector< CAN::byte_t >& serialNumber):
    node_m(node),
    sn_m(serialNumber),
    trans_num_m(0),
    errorCode_m(1),
    DG_3_3_V_m(3.3),
    DG_5_V_m(5.0),
    DG_TEMP_m(25.0),
    DG_FW_VER_m(0x11),
    FR_PHASE_OFFSET_m(0U)
{
    FR_STATUS_m.set();
    FR_STATUS_m.reset(8);
    FR_STATUS_m.reset(9);
    FR_STATUS_m.reset(10);
    FR_TE_STATUS_m.assign(3, 0);
    FR_TE_STATUS_m.insert(FR_TE_STATUS_m.begin(), 0xf0);
    FR_1_5_V_m.assign(3, 1.5);
    FR_1_8_V_m.assign(3, 1.8);
    FR_BOARD_VOLTAGE_m.VOLTAGE.push_back(3.3);
    FR_BOARD_VOLTAGE_m.VOLTAGE.push_back(12.0);
    FR_BOARD_VOLTAGE_m.VOLTAGE.push_back(5.0);
    FR_BOARD_VOLTAGE_m.MINUS_5_2_PRESENT.set();
    FR_TMP_m.assign(3, 0.0);
    FR_TMP_m.insert(FR_TMP_m.begin(), 25.0);
    FR_LASER_PWR_m.assign(3, 0.5);
    FR_LASER_BIAS_m.assign(3, 70.0e-3);
    FR_INPUT_TEST_m.assign(3, 0);
    FR_CW_m.assign(3, 0);
    FR_RNG_m.assign(3, 0);
    FR_48_V_m.set(0);
    FR_PHASE_SEQ_m.assign(2, 0ULL);
    FR_PAYLOAD_m.assign(3, 0ULL);
    FR_PAYLOAD_STATUS_m.set(0);
    FR_PAYLOAD_STATUS_m.set(2);
    FR_PAYLOAD_STATUS_m.set(4);

    std::bitset< 16 > dummy;
    dummy.set();
    TTX_ALARM_STATUS_m.assign(3, dummy);
    TTX_LASER_BIAS_m.assign(3, 0);
    TTX_LASER_PWR_m.assign(3, std::pow(10.0, 0.5) * 1.0e-3 * 0.5);
    TTX_LASER_TMP_m.assign(3, 0.0);
    TTX_REG_DATA_m.assign(3, std::vector< unsigned char >(2, 0x0));

    std::cout << "Creating node 0x" << node << ", s/n 0x";
    for(int i(0); i < 8; i++)
    {
        std::cout << std::hex << std::setw(2) << std::
        setfill('0') << static_cast< int >(serialNumber[i]);
    }

    std::cout << ", DTX device BBpr" << node - 0x50
        << std::dec << std::endl;
}

// ----------------------------------------------------------------------------
AMB::DTSTransmitter::~DTSTransmitter()
{
}

// ----------------------------------------------------------------------------
AMB::node_t AMB::DTSTransmitter::node() const
{
    return node_m;
}

// ----------------------------------------------------------------------------
std::vector< CAN::byte_t > AMB::DTSTransmitter::serialNumber() const
{
    return sn_m;
}

// ----------------------------------------------------------------------------
unsigned int AMB::DTSTransmitter::get_trans_num() const
{
    return trans_num_m;
}

// ----------------------------------------------------------------------------
void AMB::DTSTransmitter::checkSize(const std::vector< CAN::byte_t >& data,
                    int size, const char *pExceptionMsg)
{
    if(static_cast< int >(data.size()) != size)
    {
        std::ostringstream exceptionsMsg;

        exceptionsMsg << pExceptionMsg << " data length is "
            << data.size() << ", must be " << size << "!" << std::ends;
        throw DTSTransmitterError(exceptionsMsg.str());
    }
}

double AMB::DTSTransmitter::noise(const double range) const
{
#ifdef ADD_NOISE
    const double n(std::rand() / static_cast< double >(RAND_MAX));
    const double dummy(n * std::abs(range) * 2.0 - std::abs(range));

    return dummy;
#else
    return 0.0;
#endif
}

double AMB::DTSTransmitter::noise_positive(const double range) const
{
#ifdef ADD_NOISE
    const double n(std::rand() / static_cast< double >(RAND_MAX));
    const double dummy(n * std::abs(range));

    return dummy;
#else
    return 0.0;
#endif
}

// ----------------------------------------------------------------------------
std::vector< CAN::byte_t > AMB::DTSTransmitter::monitor(const rca_t rca) const
{
    ++trans_num_m;

    switch (rca)
    {
        // Mandatory generic points
        case AMB::Device::GET_SERIAL_NUM:
            return serialNumber();
            break;
        case AMB::Device::GET_CAN_ERROR:
            return get_can_error();
            break;
        case AMB::Device::GET_PROTOCOL_REV_LEVEL:
            return get_protocol_rev_level();
            break;
        case AMB::Device::GET_SW_REV_LEVEL:
            return get_software_rev_level();
            break;
        case AMB::Device::GET_HW_REV_LEVEL:
            return get_hardware_rev_level();
            break;
        case AMB::Device::GET_TRANS_NUM:
        {
            const std::vector< CAN::byte_t > no_data;

            return longToData(no_data, get_trans_num());
        }
            break;

        // Optional generic point
        case AMB::Device::GET_AMBIENT_TEMPERATURE:
        {
            const std::vector< CAN::byte_t > no_data;

            return floatDS1820ToData(no_data,
                         22.0 + 1.0 +
                         (5.0 * std::rand() / (RAND_MAX + 1.0)));
        }
            break;

        // DTSTransmitter specific points
        case GET_DG_3_3_V:
            return CAN_GET_DG_3_3_V();
            break;
        case GET_DG_5_V:
            return CAN_GET_DG_5_V();
            break;
        case GET_DG_TEMP:
            return CAN_GET_DG_TEMP();
            break;
        case GET_DG_MODE:
            return CAN_GET_DG_MODE();
            break;
        case GET_DG_FW_VER:
            return CAN_GET_DG_FW_VER();
            break;
        case GET_FR_TE_STATUS:
            return CAN_GET_FR_TE_STATUS();
            break;
        case GET_FR_STATUS:
            return CAN_GET_FR_STATUS();
            break;
        case GET_FR_1_5_V:
            return CAN_GET_FR_1_5_V();
            break;
        case GET_FR_1_8_V:
            return CAN_GET_FR_1_8_V();
            break;
        case GET_FR_BOARD_VOLTAGE:
            return CAN_GET_FR_BOARD_VOLTAGE();
            break;
        case GET_FR_TMP:
            return CAN_GET_FR_TMP();
            break;
        case GET_FR_LASER_PWR:
            return CAN_GET_FR_LASER_PWR();
            break;
        case GET_FR_LASER_BIAS:
            return CAN_GET_FR_LASER_BIAS();
            break;
        case GET_FR_INPUT_TEST_CH1:
            return CAN_GET_FR_INPUT_TEST(0);
            break;
        case GET_FR_INPUT_TEST_CH2:
            return CAN_GET_FR_INPUT_TEST(1);
            break;
        case GET_FR_INPUT_TEST_CH3:
            return CAN_GET_FR_INPUT_TEST(2);
            break;
        case GET_FR_CW_CH1:
            return CAN_GET_FR_CW(0);
            break;
        case GET_FR_CW_CH2:
            return CAN_GET_FR_CW(1);
            break;
        case GET_FR_CW_CH3:
            return CAN_GET_FR_CW(2);
            break;
        case GET_FR_RNG_CH1:
            return CAN_GET_FR_RNG(0);
            break;
        case GET_FR_RNG_CH2:
            return CAN_GET_FR_RNG(1);
            break;
        case GET_FR_RNG_CH3:
            return CAN_GET_FR_RNG(2);
            break;
        case GET_FR_48_V:
            return CAN_GET_FR_48_V();
            break;
        case GET_FR_FPGA_FW_VER_CH1:
            return CAN_GET_FR_FPGA_FW_VER(0);
            break;
        case GET_FR_FPGA_FW_VER_CH2:
            return CAN_GET_FR_FPGA_FW_VER(1);
            break;
        case GET_FR_FPGA_FW_VER_CH3:
            return CAN_GET_FR_FPGA_FW_VER(2);
            break;
        case GET_FR_PHASE_SEQ_A:
            return CAN_GET_FR_PHASE_SEQ(true);
            break;
        case GET_FR_PHASE_SEQ_B:
            return CAN_GET_FR_PHASE_SEQ(false);
            break;
        case GET_FR_PHASE_OFFSET:
            return CAN_GET_FR_PHASE_OFFSET();
            break;
        case GET_FR_PAYLOAD1:
            return CAN_GET_FR_PAYLOAD(0);
            break;
        case GET_FR_PAYLOAD2:
            return CAN_GET_FR_PAYLOAD(1);
            break;
        case GET_FR_PAYLOAD3:
            return CAN_GET_FR_PAYLOAD(2);
            break;
        case GET_FR_PAYLOAD_STATUS:
            return CAN_GET_FR_PAYLOAD_STATUS();
            break;
        case GET_FR_LRU_CIN:
            return CAN_GET_FR_LRU_CIN();
            break;
        case GET_TTX_ALARM_STATUS:
            return CAN_GET_TTX_ALARM_STATUS();
            break;
        case GET_TTX_LASER_BIAS_CH1:
            return CAN_GET_TTX_LASER_BIAS(0);
            break;
        case GET_TTX_LASER_BIAS_CH2:
            return CAN_GET_TTX_LASER_BIAS(1);
            break;
        case GET_TTX_LASER_BIAS_CH3:
            return CAN_GET_TTX_LASER_BIAS(2);
            break;
        case GET_TTX_LASER_PWR_CH1:
            return CAN_GET_TTX_LASER_PWR(0);
            break;
        case GET_TTX_LASER_PWR_CH2:
            return CAN_GET_TTX_LASER_PWR(1);
            break;
        case GET_TTX_LASER_PWR_CH3:
            return CAN_GET_TTX_LASER_PWR(2);
            break;
        case GET_TTX_LASER_TMP_CH1:
            return CAN_GET_TTX_LASER_TMP(0);
            break;
        case GET_TTX_LASER_TMP_CH2:
            return CAN_GET_TTX_LASER_TMP(1);
            break;
        case GET_TTX_LASER_TMP_CH3:
            return CAN_GET_TTX_LASER_TMP(2);
            break;
        case GET_TTX_LASER_ENABLED:
            return CAN_GET_TTX_LASER_ENABLED();
            break;
        case GET_TTX_REG_DATA_CH1:
            return CAN_GET_TTX_REG_DATA(0);
            break;
        case GET_TTX_REG_DATA_CH2:
            return CAN_GET_TTX_REG_DATA(1);
            break;
        case GET_TTX_REG_DATA_CH3:
            return CAN_GET_TTX_REG_DATA(2);
            break;
        default:
            std::ostringstream msg;

            msg << "File: " << __FILE__ << ", Line " << __LINE__ <<
                ": Exception because of an unhandled monitor point. RCA=" <<
                std::setbase(16) << setiosflags(std::ios::showbase) <<
                rca << std::ends;
            throw DTSTransmitterError(msg.str());
    }
}

// ----------------------------------------------------------------------------
void AMB::DTSTransmitter::control(rca_t rca,
                  const std::vector< CAN::byte_t >& data)
{
    ++trans_num_m;

    switch (rca)
    {
        case DG_LOAD_TEST_PAT:
            checkSize(data, 1, "DG_LOAD_TEST_PAT");
            CAN_SET_DG_LOAD_TEST_PAT(data);
            break;
        case FR_TE_RESET:
            checkSize(data, 1, "FR_TE_RESET");
            CAN_SET_FR_TE_RESET(data);
            break;
        case FR_RESET:
            checkSize(data, 1, "FR_RESET");
            CAN_SET_FR_RESET(data);
            break;
        case FR_RESET_CH1:
            checkSize(data, 1, "FR_RESET_CH1");
            CAN_SET_FR_RESET_CH(data, 0);
            break;
        case FR_RESET_CH2:
            checkSize(data, 1, "FR_RESET_CH2");
            CAN_SET_FR_RESET_CH(data, 1);
            break;
        case FR_RESET_CH3:
            checkSize(data, 1, "FR_RESET_CH3");
            CAN_SET_FR_RESET_CH(data, 2);
            break;
        case FR_RESET_ALL:
            checkSize(data, 1, "FR_RESET_ALL");
            CAN_SET_FR_RESET_CH(data, 3);
            break;
        case SET_FR_INPUT_TEST_CH1:
            checkSize(data, 1, "SET_FR_INPUT_TEST_CH1");
            CAN_SET_FR_INPUT_TEST(data, 0);
            break;
        case SET_FR_INPUT_TEST_CH2:
            checkSize(data, 1, "SET_FR_INPUT_TEST_CH2");
            CAN_SET_FR_INPUT_TEST(data, 1);
            break;
        case SET_FR_INPUT_TEST_CH3:
            checkSize(data, 1, "SET_FR_INPUT_TEST_CH3");
            CAN_SET_FR_INPUT_TEST(data, 2);
            break;
        case SET_FR_INPUT_TEST_ALL:
            checkSize(data, 1, "SET_FR_INPUT_TEST_ALL");
            CAN_SET_FR_INPUT_TEST(data, 3);
            break;
        case SET_FR_CW_CH1:
            checkSize(data, 1, "SET_FR_CW_CH1");
            CAN_SET_FR_CW(data, 0);
            break;
        case SET_FR_CW_CH2:
            checkSize(data, 1, "SET_FR_CW_CH2");
            CAN_SET_FR_CW(data, 1);
            break;
        case SET_FR_CW_CH3:
            checkSize(data, 1, "SET_FR_CW_CH3");
            CAN_SET_FR_CW(data, 2);
            break;
        case SET_FR_CW_ALL:
            checkSize(data, 1, "SET_FR_CW_ALL");
            CAN_SET_FR_CW(data, 3);
            break;
        case SET_FR_RNG_CH1:
            checkSize(data, 1, "SET_FR_RNG_CH1");
            CAN_SET_FR_RNG(data, 0);
            break;
        case SET_FR_RNG_CH2:
            checkSize(data, 1, "SET_FR_RNG_CH2");
            CAN_SET_FR_RNG(data, 1);
            break;
        case SET_FR_RNG_CH3:
            checkSize(data, 1, "SET_FR_RNG_CH3");
            CAN_SET_FR_RNG(data, 2);
            break;
        case SET_FR_RNG_CHALL:
            checkSize(data, 1, "SET_FR_RNG_ALL");
            CAN_SET_FR_RNG(data, 3);
            break;
        case SET_FR_48_V:
            checkSize(data, 1, "SET_FR_48_V");
            CAN_SET_FR_48_V(data);
            break;
        case SET_FR_PHASE_SEQ_A:
            checkSize(data, 8, "SET_FR_PHASE_SEQ_A");
            CAN_SET_FR_PHASE_SEQ(data, true);
            break;
        case SET_FR_PHASE_SEQ_B:
            checkSize(data, 8, "SET_FR_PHASE_SEQ_B");
            CAN_SET_FR_PHASE_SEQ(data, false);
            break;
        case SET_FR_PHASE_OFFSET:
            checkSize(data, 3, "SET_FR_PHASE_OFFSET");
            CAN_SET_FR_PHASE_OFFSET(data);
            break;
        case FR_CAPTURE_PAYLOAD:
            checkSize(data, 1, "FR_CAPTURE_PAYLOAD");
            CAN_SET_FR_CAPTURE_PAYLOAD(data);
            break;
        case TTX_RESET:
            checkSize(data, 1, "TTX_RESET");
            CAN_SET_TTX_RESET(data);
            break;
        case TTX_CLR_ALARMS:
            checkSize(data, 1, "TTX_CLR_ALARMS");
            CAN_SET_TTX_CLR_ALARMS(data);
            break;
        case TTX_RESET_FIFO:
            checkSize(data, 1, "TTX_RESET_FIFO");
            CAN_SET_TTX_RESET_FIFO(data);
            break;
        case TTX_LASER_ENABLE:
            checkSize(data, 1, "TTX_LASER_ENABLE");
            CAN_SET_TTX_LASER_ENABLE(data);
            break;
        case TTX_REG_ACCESS_CH1:
            checkSize(data, 4, "TTX_REG_ACCESS_CH1");
            CAN_SET_TTX_REG_ACCESS(data, 0);
            break;
        case TTX_REG_ACCESS_CH2:
            checkSize(data, 4, "TTX_REG_ACCESS_CH2");
            CAN_SET_TTX_REG_ACCESS(data, 1);
            break;
        case TTX_REG_ACCESS_CH3:
            checkSize(data, 4, "TTX_REG_ACCESS_CH3");
            CAN_SET_TTX_REG_ACCESS(data, 2);
            break;
        case TTX_REG_ACCESS_ALL:
            checkSize(data, 4, "TTX_REG_ACCESS_ALL");
            CAN_SET_TTX_REG_ACCESS(data, 3);
            break;
        default:
            std::ostringstream msg;

            msg << "File: " << __FILE__ << ", Line " << __LINE__ <<
                ": Exception because of an unhandled control command. RCA=" <<
                std::setbase(16) << setiosflags(std::ios::showbase) <<
                rca << std::ends;
            throw DTSTransmitterError(msg.str());
    }
}

// ----------------------------------------------------------------------------
std::vector< CAN::byte_t > AMB::DTSTransmitter::CAN_GET_DG_3_3_V() const
{
    double dummy(3.3 + noise(0.21));

    DG_3_3_V_m = dummy;
    const std::vector< CAN::byte_t > no_data;

    return charToData(no_data,
              static_cast< unsigned char >(std::floor(dummy / 0.0161 + 0.5)));
}

std::vector< CAN::byte_t > AMB::DTSTransmitter::CAN_GET_DG_5_V() const
{
    double dummy(5.0 + noise(0.21));

    DG_5_V_m = dummy;
    const std::vector< CAN::byte_t > no_data;

    return charToData(no_data,
              static_cast< unsigned char >(std::floor(dummy / 0.0244 + 0.5)));
}

std::vector< CAN::byte_t > AMB::DTSTransmitter::CAN_GET_DG_TEMP() const
{
    double dummy(25.0 + noise(15.1));

    DG_TEMP_m = dummy;
    const std::vector< CAN::byte_t > no_data;

    return charToData(no_data,
              static_cast< unsigned char >(std::floor(dummy / 0.240 + 0.5)));
}

std::vector< CAN::byte_t > AMB::DTSTransmitter::CAN_GET_DG_MODE() const
{
    const std::vector< CAN::byte_t > no_data;

    return charToData(no_data,
        static_cast< unsigned char >(DG_MODE_m.to_ulong()));
}

std::vector< CAN::byte_t > AMB::DTSTransmitter::CAN_GET_DG_FW_VER() const
{
    const std::vector< CAN::byte_t > no_data;

    return charToData(no_data, DG_FW_VER_m);
}

std::vector< CAN::byte_t > AMB::DTSTransmitter::CAN_GET_FR_STATUS() const
{
    const std::vector< CAN::byte_t > no_data;

    return shortToData(no_data,
               static_cast< unsigned short >(FR_STATUS_m.to_ulong()));
}

std::vector< CAN::byte_t > AMB::DTSTransmitter::CAN_GET_FR_1_5_V() const
{
    std::vector< CAN::byte_t > tmp;
    double dummy;

    for(std::vector<double>::iterator i(FR_1_5_V_m.begin());
        i < FR_1_5_V_m.end(); ++i)
    {
        dummy = 1.5 + noise(0.076);
        *i = dummy;
        tmp = shortToData(tmp,
            static_cast< unsigned short >(std::floor(dummy / 2.44e-3 + 0.5)));
    }

    return tmp;
}

std::vector< CAN::byte_t > AMB::DTSTransmitter::CAN_GET_FR_1_8_V() const
{
    std::vector< CAN::byte_t > tmp;
    double dummy;

    for(std::vector<double>::iterator i(FR_1_8_V_m.begin());
        i < FR_1_8_V_m.end(); ++i)
    {
        dummy = 1.8 + noise(0.1);
        *i = dummy;
        tmp = shortToData(tmp,
            static_cast< unsigned short >(std::floor(dummy / 2.44e-3 + 0.5)));
    }

    return tmp;
}

std::vector< CAN::byte_t > AMB::DTSTransmitter::CAN_GET_FR_BOARD_VOLTAGE() const
{
    std::vector< CAN::byte_t > tmp;

    double dummy(3.3 + noise(0.18));

    FR_BOARD_VOLTAGE_m.VOLTAGE[0] = dummy;
    tmp = shortToData(tmp,
        static_cast< unsigned short >(std::floor(dummy / 4.6115e-3 + 0.5)));

    dummy = 15.0 + noise(1.6);
    FR_BOARD_VOLTAGE_m.VOLTAGE[1] = dummy;
    tmp = shortToData(tmp,
        static_cast< unsigned short >(std::floor(dummy / 26.855e-3 + 0.5)));

    dummy = 5.0 + noise(0.6);
    FR_BOARD_VOLTAGE_m.VOLTAGE[2] = dummy;
    tmp = shortToData(tmp,
        static_cast< unsigned short >(std::floor(dummy / 8.1055e-3 + 0.5)));

    tmp = shortToData(tmp,
        static_cast< unsigned short >(
            FR_BOARD_VOLTAGE_m.MINUS_5_2_PRESENT.to_ulong()));

    return tmp;
}

std::vector< CAN::byte_t > AMB::DTSTransmitter::CAN_GET_FR_TE_STATUS() const
{
    std::vector< CAN::byte_t > tmp;

    for(std::vector< std::bitset<8> >::const_iterator i(FR_TE_STATUS_m.begin());
        i < FR_TE_STATUS_m.end(); ++ i)
    {
        tmp = charToData(tmp,
            static_cast< unsigned char >(i->to_ulong()));
    }

    return tmp;
}


std::vector< CAN::byte_t > AMB::DTSTransmitter::CAN_GET_FR_TMP() const
{
    std::vector< CAN::byte_t > tmp;

    double dummy(25.0 + noise(31.0));

    FR_TMP_m[0] = dummy;
    tmp = shortToData(tmp,
        static_cast< unsigned short >(std::floor(dummy / 0.244 + 0.5)));

    std::vector< std::bitset<16> >::iterator ttx_alarm(
        TTX_ALARM_STATUS_m.begin());
    for(std::vector<double>::iterator i(FR_TMP_m.begin() + 1);
        i < FR_TMP_m.end(); ++i, ++ttx_alarm)
    {
        dummy = noise(1.1);
        *i = dummy;

        if((dummy < -1.0) || (dummy > 1.0))
        {
            ttx_alarm->reset(10);
        }

        tmp = shortToData(tmp,
            static_cast< unsigned short >(std::floor(0x400 + (dummy / 97.6e-3) + 0.5)));
    }

    return tmp;
}

std::vector< CAN::byte_t > AMB::DTSTransmitter::CAN_GET_FR_LASER_PWR() const
{
    std::vector< CAN::byte_t > tmp;
    double dummy;

    std::vector< std::bitset<16> >::iterator ttx_alarm(
        TTX_ALARM_STATUS_m.begin());
    for(std::vector<double>::iterator i(FR_LASER_PWR_m.begin());
        i < FR_LASER_PWR_m.end(); ++i, ++ttx_alarm)
    {
        dummy = 0.5 + noise(0.26);
        *i = dummy;

        if(dummy < 0.25)
        {
            ttx_alarm->reset(13);
        }

        tmp = shortToData(tmp,
            static_cast< unsigned short >(std::floor((dummy / 2.44e-3) + 0.5)));
    }

    return tmp;
}

std::vector< CAN::byte_t > AMB::DTSTransmitter::CAN_GET_FR_LASER_BIAS() const
{
    std::vector< CAN::byte_t > tmp;
    double dummy;

    std::vector< std::bitset<16> >::iterator ttx_alarm(
        TTX_ALARM_STATUS_m.begin());
    for(std::vector<double>::iterator i(FR_LASER_BIAS_m.begin());
        i < FR_LASER_BIAS_m.end(); ++i, ++ttx_alarm)
    {
        dummy = 70.0e-3 + noise(36.0e-3);
        *i = dummy;

        if(dummy > 105.0e-3)
        {
            ttx_alarm->reset(9);
        }

        tmp = shortToData(tmp,
            static_cast< unsigned short >(std::floor((dummy / 122.0e-6) + 0.5)));
    }

    return tmp;
}

std::vector< CAN::byte_t >
    AMB::DTSTransmitter::CAN_GET_FR_INPUT_TEST(const int channel) const
{
    const std::vector< CAN::byte_t > no_data;

    return charToData(no_data,
        static_cast< unsigned char >(FR_INPUT_TEST_m[channel].to_ulong()));
}

std::vector< CAN::byte_t >
    AMB::DTSTransmitter::CAN_GET_FR_CW(const int channel) const
{
    const std::vector< CAN::byte_t > no_data;

    return charToData(no_data,
        static_cast< unsigned char >(FR_CW_m[channel].to_ulong()));
}

std::vector< CAN::byte_t >
    AMB::DTSTransmitter::CAN_GET_FR_RNG(const int channel) const
{
    const std::vector< CAN::byte_t > no_data;

    return charToData(no_data,
        static_cast< unsigned char >(FR_RNG_m[channel].to_ulong()));
}

std::vector< CAN::byte_t > AMB::DTSTransmitter::CAN_GET_FR_48_V() const
{
    const std::vector< CAN::byte_t > no_data;

    return charToData(no_data, static_cast< unsigned char >(FR_48_V_m.to_ulong()));
}

std::vector< CAN::byte_t >
    AMB::DTSTransmitter::CAN_GET_FR_FPGA_FW_VER(const int channel) const
{
    const unsigned char* firmwareString = 0;
    std::vector< CAN::byte_t > tmp;

    switch(channel)
    {
        case 0:
        {
            firmwareString = FirmwareRevision_BitD;
        }
            break;

        case 1:
        {
            firmwareString = FirmwareRevision_BitC;
        }
            break;

        case 2:
        {
            firmwareString = FirmwareRevision_BitB;
        }
            break;

        default:
        {
            return tmp;
        }
    }

    unsigned long long* foo(static_cast< unsigned long long* >(
        static_cast< void* >(const_cast< unsigned char* >(firmwareString))));
    tmp = longlongToData(tmp, *foo, 8);

    return tmp;
}

std::vector< CAN::byte_t >
    AMB::DTSTransmitter::CAN_GET_FR_PHASE_SEQ(const bool lower_half) const
{
    std::vector< unsigned long long >::size_type index(0);
    if(lower_half == false)
    {
        index = 1;
    }

    const std::vector< CAN::byte_t > no_data;

    return longlongToData(no_data, FR_PHASE_SEQ_m[index], 8);
}

std::vector< CAN::byte_t > AMB::DTSTransmitter::CAN_GET_FR_PHASE_OFFSET() const
{
    unsigned long long dummy(FR_PHASE_OFFSET_m);
    std::vector< CAN::byte_t > no_content;

    return longlongToData(no_content, dummy, 3);
}

std::vector< CAN::byte_t >
    AMB::DTSTransmitter::CAN_GET_FR_PAYLOAD(const int channel) const
{
    const std::vector< CAN::byte_t > no_data;

    return longlongToData(no_data, FR_PAYLOAD_m[channel], 8);
}

std::vector< CAN::byte_t > AMB::DTSTransmitter::CAN_GET_FR_LRU_CIN() const
{
    std::vector< CAN::byte_t > tmp(8);

    tmp[4] = 0x01;        // Serial number (two bytes)
    tmp[5] = 0x00;
    tmp[6] = 0x01;        // LRU revision

    return tmp;
}

std::vector< CAN::byte_t > AMB::DTSTransmitter::CAN_GET_FR_PAYLOAD_STATUS() const
{
    const std::vector< CAN::byte_t > no_data;

    return charToData(no_data,
        static_cast<unsigned char>(FR_PAYLOAD_STATUS_m.to_ulong()));
}

std::vector< CAN::byte_t > AMB::DTSTransmitter::CAN_GET_TTX_ALARM_STATUS() const
{
    std::vector< CAN::byte_t > tmp;

    for(std::vector< std::bitset<16> >::iterator i(TTX_ALARM_STATUS_m.begin());
        i < TTX_ALARM_STATUS_m.end(); ++i)
    {
        tmp = shortToData(tmp,
            static_cast< unsigned short >(i->to_ulong()));
        i->set();
    }

    return tmp;
}

std::vector< CAN::byte_t > AMB::DTSTransmitter::CAN_GET_TTX_LASER_BIAS(const int channel) const
{
    double dummy(70.0e-3 + noise(35.1e-3));

    TTX_LASER_BIAS_m[channel] = dummy;

    if(dummy > (105.0e-3))
    {
        TTX_ALARM_STATUS_m[channel].reset(9);
    }

    dummy = std::floor((dummy * 1.0e6) + 0.5);

    const std::vector< CAN::byte_t > no_data;

    return longlongToData(no_data, static_cast< long long >(dummy), 3);
}

std::vector< CAN::byte_t > AMB::DTSTransmitter::CAN_GET_TTX_LASER_PWR(const int channel) const
{
    double dummy(std::pow(10.0, 0.5) * 1.0e-3 * 0.5 +
        noise(std::pow(10.0, 0.5) * 1.0e-3 * 0.5 * 0.51));

    TTX_LASER_PWR_m[channel] = dummy;

    if(dummy < std::pow(10.0, 0.5) * 1.0e-3 * 0.5 * 0.5)
    {
        TTX_ALARM_STATUS_m[channel].reset(13);
    }

    dummy = std::floor(dummy * 1.0e6 + 0.5);

    const std::vector< CAN::byte_t > no_data;

    return longlongToData(no_data, static_cast< long long >(dummy), 3);
}

std::vector< CAN::byte_t > AMB::DTSTransmitter::CAN_GET_TTX_LASER_TMP(const int channel) const
{
    double dummy(noise(1.1));

    TTX_LASER_TMP_m[channel] = dummy;

    if((dummy < -1.0) || (dummy > 1.0))
    {
        TTX_ALARM_STATUS_m[channel].reset(10);
    }

    dummy = std::floor(dummy * 1.0e3 + 0.5);

    const std::vector< CAN::byte_t > no_data;

    return longlongToData(no_data, static_cast< long long >(dummy), 3);
}

std::vector< CAN::byte_t > AMB::DTSTransmitter::CAN_GET_TTX_LASER_ENABLED() const
{
    const std::vector< CAN::byte_t > no_data;

    return charToData(no_data,
        static_cast< unsigned char >(TTX_LASER_ENABLED_m.to_ulong()));
}

std::vector< CAN::byte_t >
    AMB::DTSTransmitter::CAN_GET_TTX_REG_DATA(const int channel) const
{
    std::vector< CAN::byte_t > tmp;

    for(std::vector<unsigned char>::const_iterator i(TTX_REG_DATA_m[channel].begin());
        i < TTX_REG_DATA_m[channel].end(); ++i)
    {
        tmp = charToData(tmp, *i);
    }

    return tmp;
}

// ----------------------------------------------------------------------------

void AMB::DTSTransmitter::CAN_SET_DG_LOAD_TEST_PAT(const std::vector< CAN::byte_t >& data)
{
    std::bitset<1> dummy(static_cast< std::bitset<1> >(
        static_cast< unsigned long >(data[0] & 0x01)));

    DG_MODE_m.set(0, dummy.test(0));
}

void AMB::DTSTransmitter::CAN_SET_FR_RESET_CH(const std::vector< CAN::byte_t >& data,
    const int channel)
{
    std::bitset<8> dummy(static_cast< std::bitset<8> >(
        static_cast< unsigned long >(data[0] & 0xf1)));

    switch(channel)
    {
        case 0:
        case 1:
        case 2:
        {
            if(dummy.test(0) == true)
            {
                FR_TE_STATUS_m.assign(3, 0);
                FR_TE_STATUS_m.insert(FR_TE_STATUS_m.begin(), 0xf0);
                FR_STATUS_m.set();
                FR_STATUS_m.set(8, TTX_LASER_ENABLED_m.test(0));
                FR_STATUS_m.set(9, TTX_LASER_ENABLED_m.test(1));
                FR_STATUS_m.set(10, TTX_LASER_ENABLED_m.test(2));
            }

            if((dummy.to_ulong() && 0xf0) == 0xf0)
            {
                FR_1_5_V_m[channel] = 1.5;
                FR_1_8_V_m[channel] = 1.8;
                FR_TMP_m[0] = 25.0;
                FR_TMP_m[channel] = 0.0;
                FR_LASER_PWR_m[channel] = 0.5;
                FR_LASER_BIAS_m[channel] = 70.0e-3;
                FR_INPUT_TEST_m[channel] = 0;
                FR_CW_m[channel] = 0;
                FR_RNG_m[channel] = 0;
                FR_PAYLOAD_m[channel] = 0ULL;
                FR_PAYLOAD_STATUS_m.set(0);
                FR_PAYLOAD_STATUS_m.set(2);
                FR_PAYLOAD_STATUS_m.set(4);
                FR_PAYLOAD_STATUS_m.reset(1);
                FR_PAYLOAD_STATUS_m.reset(3);
                FR_PAYLOAD_STATUS_m.reset(5);

                if(channel == 2)
                {
                    FR_PHASE_OFFSET_m = 0U;
                    FR_BOARD_VOLTAGE_m.VOLTAGE.clear();
                    FR_BOARD_VOLTAGE_m.VOLTAGE.push_back(3.3);
                    FR_BOARD_VOLTAGE_m.VOLTAGE.push_back(12.0);
                    FR_BOARD_VOLTAGE_m.VOLTAGE.push_back(5.0);
                    FR_BOARD_VOLTAGE_m.MINUS_5_2_PRESENT.set();
                    FR_48_V_m.set(0);
                    FR_PHASE_SEQ_m.assign(2, 0ULL);
                }
            }
        }
        break;

        case 3:
        {
            if(dummy.test(0) == true)
            {
                FR_TE_STATUS_m.assign(3, 0);
                FR_TE_STATUS_m.insert(FR_TE_STATUS_m.begin(), 0xf0);
                FR_STATUS_m.set();
                FR_STATUS_m.set(8, TTX_LASER_ENABLED_m.test(0));
                FR_STATUS_m.set(9, TTX_LASER_ENABLED_m.test(1));
                FR_STATUS_m.set(10, TTX_LASER_ENABLED_m.test(2));
            }

            if((dummy.to_ulong() && 0xf0) == 0xf0)
            {
                FR_PHASE_OFFSET_m = 0U;
                FR_1_5_V_m.assign(3, 1.5);
                FR_1_8_V_m.assign(3, 1.8);
                FR_BOARD_VOLTAGE_m.VOLTAGE.clear();
                FR_BOARD_VOLTAGE_m.VOLTAGE.push_back(3.3);
                FR_BOARD_VOLTAGE_m.VOLTAGE.push_back(12.0);
                FR_BOARD_VOLTAGE_m.VOLTAGE.push_back(5.0);
                FR_BOARD_VOLTAGE_m.MINUS_5_2_PRESENT.set();
                FR_TMP_m.assign(3, 0.0);
                FR_TMP_m.insert(FR_TMP_m.begin(), 25.0);
                FR_LASER_PWR_m.assign(3, 0.5);
                FR_LASER_BIAS_m.assign(3, 70.0e-3);
                FR_INPUT_TEST_m.assign(3, 0);
                FR_CW_m.assign(3, 0);
                FR_RNG_m.assign(3, 0);
                FR_48_V_m.set(0);
                FR_PHASE_SEQ_m.assign(2, 0ULL);
                FR_PAYLOAD_m.assign(3, 0ULL);
                FR_PAYLOAD_STATUS_m.set(0);
                FR_PAYLOAD_STATUS_m.set(2);
                FR_PAYLOAD_STATUS_m.set(4);
                FR_PAYLOAD_STATUS_m.reset(1);
                FR_PAYLOAD_STATUS_m.reset(3);
                FR_PAYLOAD_STATUS_m.reset(5);

                FR_PHASE_OFFSET_m = 0U;
                FR_BOARD_VOLTAGE_m.VOLTAGE.clear();
                FR_BOARD_VOLTAGE_m.VOLTAGE.push_back(3.3);
                FR_BOARD_VOLTAGE_m.VOLTAGE.push_back(12.0);
                FR_BOARD_VOLTAGE_m.VOLTAGE.push_back(5.0);
                FR_BOARD_VOLTAGE_m.MINUS_5_2_PRESENT.set();
                FR_48_V_m.set(0);
                FR_PHASE_SEQ_m.assign(2, 0ULL);
            }
        }
        break;

        default:
        {
            std::ostringstream msg;

            msg << "File: " << __FILE__ << ", Line " << __LINE__ <<
                ": Exception because of an illegal channel in SET_FR_RESET_CHi. Channel=" <<
                channel << std::ends;
            throw DTSTransmitterError(msg.str());

            throw(msg.str());
        }
    }
}

void AMB::DTSTransmitter::CAN_SET_FR_INPUT_TEST(const std::vector< CAN::byte_t >& data,
    const int channel)
{
    std::bitset<4> dummy(static_cast< std::bitset<4> >(
        static_cast< unsigned long >(data[0] & 0x0f)));

    switch (channel)
    {
        case 0:
        case 1:
        case 2:
            FR_INPUT_TEST_m[channel] = dummy;
            break;
        case 3:
            FR_INPUT_TEST_m[0] = FR_INPUT_TEST_m[1] = FR_INPUT_TEST_m[2] = dummy;
            break;
        default:
            std::ostringstream msg;

            msg << "File: " << __FILE__ << ", Line " << __LINE__ <<
                ": Exception because of an illegal channel in SET_FR_INPUT_TEST_CHi. Channel=" <<
                channel << std::ends;
            throw DTSTransmitterError(msg.str());

            throw(msg.str());
    }
}

void AMB::DTSTransmitter::CAN_SET_FR_CW(const std::vector< CAN::byte_t >& data,
    const int channel)
{
    std::bitset< 8 > dummy(static_cast< std::bitset< 8 > >(
        static_cast< unsigned long >(data[0] & 0xfc)));

    switch (channel)
    {
        case 0:
        case 1:
        case 2:
            FR_CW_m[channel] = dummy;
            break;
        case 3:
            FR_CW_m[0] = FR_CW_m[1] = FR_CW_m[2] = dummy;
            break;
        default:
            std::ostringstream msg;

            msg << "File: " << __FILE__ << ", Line " << __LINE__ <<
                ": Exception because of an illegal channel in SET_FR_CWi. Channel=" <<
                channel << std::ends;
            throw DTSTransmitterError(msg.str());

            throw(msg.str());
    }
}

void AMB::DTSTransmitter::CAN_SET_FR_RNG(const std::vector< CAN::byte_t >& data,
    const int channel)
{
    std::bitset< 2 > dummy(static_cast< std::bitset< 2 > >(
        static_cast< unsigned long >(data[0] & 0x03)));

    switch(channel)
    {
        case 0:
        case 1:
        case 2:
        {
            FR_RNG_m[channel] = dummy;
        }
            break;

        case 3:
        {
            FR_RNG_m[0] = FR_RNG_m[1] = FR_RNG_m[2] = dummy;
        }
            break;

        default:
        {
            std::ostringstream msg;

            msg << "File: " << __FILE__ << ", Line " << __LINE__ <<
                ": Exception because of an illegal channel in SET_FR_RNGi. Channel=" <<
                channel << std::ends;
            throw DTSTransmitterError(msg.str());

            throw(msg.str());
        }
    }
}

void AMB::DTSTransmitter::CAN_SET_FR_48_V(const std::vector< CAN::byte_t >& data)
{
    std::bitset< 2 > dummy(static_cast< std::bitset< 2 > >(
        static_cast< unsigned long >(data[0] & 0x1)));

    FR_48_V_m.set(0, (dummy.test(0) == true ? 1 : 0));
}

void AMB::DTSTransmitter::CAN_SET_FR_PHASE_SEQ(const std::vector< CAN::byte_t >& data,
    const bool lower_half)
{
    std::vector< CAN::byte_t > dummy(data);
    std::vector< unsigned long long >::size_type index(0);
    if(lower_half == false)
    {
        index = 1;
    }

    FR_PHASE_SEQ_m[index] = dataToLonglong(dummy, 8);
}

void AMB::DTSTransmitter::CAN_SET_FR_PHASE_OFFSET(const std::vector< CAN::byte_t >& data)
{
    std::vector< CAN::byte_t > tmp(data);
    unsigned long dummy(dataToLonglong(tmp, 3));

    FR_PHASE_OFFSET_m = dummy;
}

void AMB::DTSTransmitter::CAN_SET_FR_RESET(const std::vector< CAN::byte_t >& data)
{
    std::vector< CAN::byte_t > dummy(1, 0xf1);

    CAN_SET_FR_RESET_CH(dummy, 3);
}

void AMB::DTSTransmitter::CAN_SET_FR_TE_RESET(const std::vector< CAN::byte_t >& data)
{
        std::bitset< 3 > dummy(static_cast< std::bitset< 3 > >(
            static_cast< unsigned long >(data[0] & 0x7)));

        FR_TE_STATUS_m[0].set(1, dummy.test(1));

        if((dummy.test(0) == true) || (dummy.test(2) == true))
        {
            for(std::vector< std::bitset<8> >::iterator i(FR_TE_STATUS_m.begin() + 1);
                i < FR_TE_STATUS_m.end(); ++i)
            {
                *i = 0;
            }
        }
}

void AMB::DTSTransmitter::CAN_SET_FR_CAPTURE_PAYLOAD(const std::vector< CAN::byte_t >& data)
{
    if(DG_MODE_m.test(0) == true)
    {
        FR_PAYLOAD_m.assign(3, 0x1020304050607080ULL);
    }
    else
    {
        FR_PAYLOAD_m.assign(3, 0ULL);
    }

    if((data[0] & 0x01) == 1)
    {
        FR_PAYLOAD_STATUS_m.set(1);
        FR_PAYLOAD_STATUS_m.set(3);
        FR_PAYLOAD_STATUS_m.set(5);
        FR_PAYLOAD_STATUS_m.reset(0);
        FR_PAYLOAD_STATUS_m.reset(2);
        FR_PAYLOAD_STATUS_m.reset(4);
    }
    else
    {
        FR_PAYLOAD_STATUS_m.set(0);
        FR_PAYLOAD_STATUS_m.set(2);
        FR_PAYLOAD_STATUS_m.set(4);
        FR_PAYLOAD_STATUS_m.reset(1);
        FR_PAYLOAD_STATUS_m.reset(3);
        FR_PAYLOAD_STATUS_m.reset(5);
    }
}

void AMB::DTSTransmitter::CAN_SET_TTX_RESET(const std::vector< CAN::byte_t >& data)
{
    // Do nothing.
}

void AMB::DTSTransmitter::CAN_SET_TTX_CLR_ALARMS(const std::vector< CAN::byte_t >& data)
{
    std::bitset< 16 > dummy;
    dummy.set();

    TTX_ALARM_STATUS_m.assign(3, dummy);
}

void AMB::DTSTransmitter::CAN_SET_TTX_RESET_FIFO(const std::vector< CAN::byte_t >& data)
{
    // Do nothing.
}

void AMB::DTSTransmitter::CAN_SET_TTX_LASER_ENABLE(const std::vector< CAN::byte_t >& data)
{
    if(data[0] & 1)
    {
        TTX_LASER_ENABLED_m.set(0);
        FR_STATUS_m.set(8);
    }
    else
    {
        TTX_LASER_ENABLED_m.reset(0);
        FR_STATUS_m.reset(8);
    }

    if(data[0] & 2)
    {
        TTX_LASER_ENABLED_m.set(1);
        FR_STATUS_m.set(9);
    }
    else
    {
        TTX_LASER_ENABLED_m.reset(1);
        FR_STATUS_m.reset(9);
    }

    if(data[0] & 4)
    {
        TTX_LASER_ENABLED_m.set(2);
        FR_STATUS_m.set(10);
    }
    else
    {
        TTX_LASER_ENABLED_m.reset(2);
        FR_STATUS_m.reset(10);
    }
}

void AMB::DTSTransmitter::CAN_SET_TTX_REG_ACCESS(const std::vector< CAN::byte_t >& data,
    const int channel)
{
    // Do nothing.
}
