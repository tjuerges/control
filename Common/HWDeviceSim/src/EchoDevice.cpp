// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include <iomanip>
#include "EchoDevice.h"
#include "CANError.h"
#include "AMBUtil.h"

#include <stdio.h>

using AMB::node_t;
using AMB::rca_t;

// -----------------------------------------------------------------------------

AMB::EchoDevice::EchoDevice(
		       AMB::node_t node, 
		       const std::vector<CAN::byte_t> &serialNumber)
    : node_m(node), serialNumber_m(serialNumber), trans_num_m(0)
{
    std::cout << "Creating node " << node << ", s/n 0x";
    for (int i = 0; i < 8; i++)
	std::cout << std::hex << std::setw(2) << std::setfill('0') << (int)serialNumber[i];
    std::cout << ", Echo device" << std::endl;

    std::string error;
    if (! AMB::validateNodeNumber(error, this->node())) {
	throw CAN::Error(error);
    }

    if (! AMB::validateSerialNumber(error, this->serialNumber())) {
	throw CAN::Error(error);
    }
}

// -----------------------------------------------------------------------------

AMB::EchoDevice::~EchoDevice()
{
    // Nothing
}

// -----------------------------------------------------------------------------

AMB::node_t AMB::EchoDevice::node() const
{
    return node_m;
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::EchoDevice::serialNumber() const
{
    return serialNumber_m;
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::EchoDevice::monitor(rca_t rca) const
{
    trans_num_m++;
    switch (rca) {
    case GET_CAN_ERROR:
	return get_can_error();
	break;
    case GET_PROTOCOL_REV_LEVEL:
	return get_protocol_rev_level();
	break;

	case AMB::Device::GET_SW_REV_LEVEL:
		return get_software_rev_level();
		break;
	case AMB::Device::GET_HW_REV_LEVEL:
		return get_hardware_rev_level();
		break;

    case GET_TRANS_NUM:
	{
	    std::vector <CAN::byte_t> tmp;
	    return longToData(tmp, get_trans_num());
	}
	break;
    default:
	if (rca > MAX_RCA) {
	    trans_num_m--; // Do we really want to correct for illegal points?
	    throw CAN::Error("rca is greater than MAX_RCA");
	}
	// If not already defined via a control, returns the empty std::string.
	return commanded_values_m[rca];
    }
}

// -----------------------------------------------------------------------------

void AMB::EchoDevice::control(rca_t rca, const std::vector<CAN::byte_t> &data)
{
    if (rca > MAX_RCA) {
	throw CAN::Error("rca is greater than MAX_RCA");
    }
    trans_num_m++;
    commanded_values_m[rca] = data;
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::EchoDevice::get_can_error() const
{
    return AMB::Device::get_can_error();
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::EchoDevice::get_protocol_rev_level() const
{
    return AMB::Device::get_protocol_rev_level();
}

// -----------------------------------------------------------------------------

unsigned int AMB::EchoDevice::get_trans_num() const
{
    return trans_num_m;
}
