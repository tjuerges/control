// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2007
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "CTNLL.h"
#include "CANTypes.h"
#include "AMBUtil.h"
#include <iostream>
#include <iomanip>
#include <vector>
#include <sstream>
using std::vector;
using std::cout;
using std::endl;
using std::hex;
using std::setw;
using std::setfill;
using std::ostringstream;
using AMB::CTNLL;

// ----------------------------------------------------------------------------
CTNLL::CTNLL(node_t node, const vector<CAN::byte_t>& serialNumber)
  :node_m(node),
   sn_m(serialNumber),
   transNum_m(0),
   //CTNLL_STATUS_m(134217728), // 134217728 --> 0x08 00 00 00
   LASER_FREQUENCY_m(75729),  // 75729 --> 192556.77GHz
   LASER_CURRENT_m(75729),    // 75729 300mA
   LASER_POWER_m(75729),      // 75729 50mW
   LASER_TEMPERATURE_m(75729) // 75729 Celcius

{
  cout << "Creating node 0x" << node << ", s/n 0x";
  for (int i = 0; i < 8; i++) {
    cout << hex << setw(2) << setfill('0')<< static_cast<int>(serialNumber[i]);
  }
  cout << ", CTNLL device" << endl;

  if (node != 0x39 && 
      node != 0x3B &&
      node != 0x3D &&
      node != 0x3F) {
    ostringstream errMessage;
    errMessage << "Bad node number. It should be 0x0x3{9,B,D,F} but 0x"
	       << hex << node << " was specified." << endl;
    throw CAN::Error(errMessage.str());
  }
}

// ----------------------------------------------------------------------------
CTNLL::~CTNLL() {
  // Nothing
}

// ----------------------------------------------------------------------------
AMB::node_t CTNLL::node() const {
  return node_m;
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> CTNLL::serialNumber() const {
    return sn_m;
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> CTNLL::monitor(rca_t rca) const {
  transNum_m++;
  const vector<CAN::byte_t> tmp;
  
  switch (rca) {
    // Mandatory generic points
  case GET_SERIAL_NUM:
    return serialNumber();
    break;
  case GET_CAN_ERROR:
    return get_can_error();
    break;
  case GET_PROTOCOL_REV_LEVEL:
    return get_protocol_rev_level();
    break;
  case GET_SW_REV_LEVEL:
    return get_software_rev_level();
    break;
  case GET_HW_REV_LEVEL:
    return get_hardware_rev_level();
    break;
  case GET_TRANS_NUM:
    return longToData(tmp, get_trans_num());
    break;
    // Optional generic point
  case GET_AMBIENT_TEMPERATURE:
    return floatDS1820ToData(tmp, getAmbientTemperature());
    break;
  case GET_CTNLL_STATUS:
    return CAN_CTNLL_STATUS ();
    break;
  case GET_LASER_FREQUENCY:
    return CAN_LASER_FREQUENCY ();
    break;
  case GET_LASER_CURRENT:
    return CAN_LASER_CURRENT ();
    break;
  case GET_LASER_POWER:
    return CAN_LASER_POWER ();
    break;
  case GET_LASER_TEMPERATURE:
    return CAN_LASER_TEMPERATURE ();
    break;
  default:
    ostringstream errMessage;
    errMessage << "Unknown RCA (0x" << hex << rca 
               << ") in monitor command." << endl;
    throw CAN::Error(errMessage.str());
  }
  // Should never get here
  return tmp;
}

// ----------------------------------------------------------------------------
void CTNLL::control(rca_t rca, const vector<CAN::byte_t>& data) {
  transNum_m++;
  switch (rca) {
  case RESET_DEVICE:
    // TODO find out what the hardware does when hit with this command
    break;
  case SET_LASER_FREQUENCY:
    checkSize(data,4,"SET_LASER_FREQUENCY");
    LASER_FREQUENCY_m = 
         dataToDouble(const_cast<std::vector<CAN::byte_t> &>(data));
    break;
  case SET_CTNLL_COMMAND:
    {
    checkSize(data,4,"SET_CTNLL_COMMAND");
    const CTNLL_Commands cmd_key =
      static_cast<AMB::CTNLL::CTNLL_Commands>( 
                                              dataToShort(const_cast<std::vector<CAN::byte_t> &>(data))& 0x0007
                                              );
    
    if ( static_cast<short int>(CTNLL_STATUS_m)&0x0008 )
    {
       switch(cmd_key){
       case CALIBRATE:
          break;
       case PZT_PLUS:
          break;
       case PZT_MINUS:
          break;
       default:
          break;
       }
    }
    } 
    break;

  default:
    ostringstream errMessage;
    errMessage << "Unknown RCA (0x" << hex << rca 
               << ") in control command." << endl;
    throw CAN::Error(errMessage.str());
  }
}


void AMB::CTNLL::checkSize(const std::vector<CAN::byte_t> &data, int size,
                          const char *pExceptionMsg) {
  if( static_cast<int>(data.size()) != size ) {
        char exceptionMsg[200];
        sprintf(exceptionMsg,"%s data length is %d must be %d",pExceptionMsg,
                data.size(), size);
        throw CAN::Error(exceptionMsg);
  }
}


// ----------------------------------------------------------------------------
unsigned int CTNLL::get_trans_num() const {
  return transNum_m;
}

// ----------------------------------------------------------------------------
float CTNLL::getAmbientTemperature() const {
  return 18.0;
}

std::vector<CAN::byte_t> CTNLL::CAN_CTNLL_STATUS() const
{
   std::vector<CAN::byte_t> tmp (3,0x0);
   tmp.push_back(0x08);
   return tmp;
}

std::vector<CAN::byte_t> CTNLL::CAN_LASER_FREQUENCY() const
{
  const std::vector<CAN::byte_t> tmp;
  const long int intValue =
    static_cast<long int>(LASER_FREQUENCY_m) & 0x0003FFFF;
  return longToData(tmp, intValue);
}

std::vector<CAN::byte_t> CTNLL::CAN_LASER_CURRENT() const
{
  const std::vector<CAN::byte_t> tmp;
  const long int intValue =
    static_cast<long int>(LASER_CURRENT_m) & 0x0003FFFF;
  return longToData(tmp, intValue);
}

std::vector<CAN::byte_t> CTNLL::CAN_LASER_POWER() const
{
  const std::vector<CAN::byte_t> tmp;
  const long int intValue =
    static_cast<long int>(LASER_POWER_m) & 0x0003FFFF;
  return longToData(tmp, intValue);
}

std::vector<CAN::byte_t> CTNLL::CAN_LASER_TEMPERATURE() const
{
  const std::vector<CAN::byte_t> tmp;
  const long int intValue =
    static_cast<long int>(LASER_TEMPERATURE_m) & 0x0003FFFF;
  return longToData(tmp, intValue);
}
