// License along with this library; if not, write to the Free Software
// Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
//
// Correspondence concerning ALMA should be addressed as follows:
//
//        Internet email: alma-sw-admin@nrao.edu
//

#include <iomanip>
#include <iostream>
#include <sstream>
#include "PSD.h"
#include "AMBUtil.h"


// ---------------------------------------------------------------------------

AMB::PSD::
PSD(node_t node,
		  const std::vector<CAN::byte_t> &serialNumber)
  :node_m(node), 
   sn_m(serialNumber), 
   trans_num_m(0),
   shutdown_m(false),
   error_and_alarms_m(0x0003DF), //all normal MSBLSB
   voltage_24v_a_m(24),	//24V	(operating range)
   current_24v_a_m(8),	//8AMP	(operating range)
   voltage_24v_b_m(24),	//24V	(operating range)
   current_24v_b_m(8),	//8AMP	(operating range)
   ambient_temperature_m(0x2C001010) // 22 celsius
 {
  std::cout << "Creating node " << node << ",s/n 0x";
  for (int i = 0; i < 8; i++) {
    std::cout << std::hex << std::setw(2) << std::setfill('0') 
	      << static_cast<int>(serialNumber[i]);
  }
  std::cout << ", PSD Power Supply Digital" << std::endl;
  //initialize max_min_24v_a_m[]   0x02d202950170
  max_min_24v_a_m[0]=0x02;
  max_min_24v_a_m[1]=0xd2;
  max_min_24v_a_m[2]=0x02;
  max_min_24v_a_m[3]=0x95;
  max_min_24v_a_m[4]=0x01;
  max_min_24v_a_m[5]=0x70;
  /*for(int i=0;i<6;i++){
  			std::cerr << "max_min_24v_a_m["<<i<<"]=" <<  std::hex <<static_cast<int>(max_min_24v_a_m[i])<< std::endl;
  					}*/
   //initialize max_min_24v_b_m[]
        
    max_min_24v_b_m[0]=0x02;
    max_min_24v_b_m[1]=0xd2;
    max_min_24v_b_m[2]=0x02;
    max_min_24v_b_m[3]=0x95;
    max_min_24v_b_m[4]=0x01;
    max_min_24v_b_m[5]=0x70;
    /*for(int i=0;i<6;i++){
      			std::cerr << "max_min_24v_b_m["<<i<<"]=" <<  std::hex << static_cast<int>(max_min_24v_b_m[i])<< std::endl;
      					}*/
}

AMB::PSD::~PSD() {
  // Nothing
}

AMB::node_t AMB::PSD::node() const {
    return node_m;
}

std::vector<CAN::byte_t> AMB::PSD::serialNumber() const {
  return sn_m;
}

std::vector<CAN::byte_t> AMB::PSD::monitor(rca_t rca) const {
  trans_num_m++;
  const std::vector<CAN::byte_t> tmp;
  std::vector<CAN::byte_t> retVal;
  
  switch (rca) {
    // Mandatory generic points
  case GET_SERIAL_NUMBER:
    return serialNumber();
    break;
  case GET_CAN_ERROR:
    return get_can_error();
    break;
  case GET_PROTOCOL_REV_LEVEL:
    return get_protocol_rev_level();
    break;
	case AMB::Device::GET_SW_REV_LEVEL:
		return get_software_rev_level();
		break;
	case AMB::Device::GET_HW_REV_LEVEL:
		return get_hardware_rev_level();
		break;
  case GET_TRANS_NUM:
    return longToData(tmp, get_trans_num());
    break;
    // Optional generic point
  case GET_AMBIENT_TEMPERATURE:
    return get_ambient_temperature();
    break;
    // PSD specific points
  case ERROR_AND_ALARMS:
	  return error_and_alarms();
	  break;
  case VOLTAGE_24V_A:
    return voltage_24v_a();
    break;          
  case CURRENT_24V_A:
    return current_24v_a();
    break;          
  case VOLTAGE_24V_B:
    return voltage_24v_b();
    break;          
  case CURRENT_24V_B:
    return current_24v_b();
    break;
  case MAX_MIN_24V_A:
      return max_min_24v_a();
      break;
  case MAX_MIN_24V_B:
      return max_min_24v_b();
      break;
  default:
    std::cerr << "Switch does not match any case" << std::endl;
    throw PSDError("Unknown RCA in monitor command");
  }
}

void AMB::PSD::control(rca_t rca, const std::vector<CAN::byte_t>& data) {
	trans_num_m++;
	ACS_DEBUG("PSD::control",
					"Processing RCA. Control Point Received");
		std::cerr << "Processing RCA. Control Point Received" << std::endl;
	switch (rca) {
	
	case SHUTDOWN_COM:
		shutdown_com(data);
		break;
		//Test Control Points 
	case TEST_VOLTAGE_24V_A:
		//checkSize(data, 8, "TEST_VOLTAGE_24V_A");
		ACS_DEBUG("PSD::control",
						"Processing RCA. TEST_VOLTAGE_24V_A Received");
			std::cerr << "Processing RCA. TEST_VOLTAGE_24V_A Received" << std::endl;
		voltage_24v_a_m
				=dataToDouble(const_cast<std::vector<CAN::byte_t> &>(data));
		break;
	case TEST_CURRENT_24V_A:
		checkSize(data, 8, "TEST_CURRENT_24V_A");
		current_24v_a_m
				=dataToDouble(const_cast<std::vector<CAN::byte_t> &>(data));
		break;
	case TEST_VOLTAGE_24V_B:
		checkSize(data, 8, "TEST_VOLTAGE_24V_B");
		voltage_24v_b_m
				=dataToDouble(const_cast<std::vector<CAN::byte_t> &>(data));
		break;
	case TEST_CURRENT_24V_B:
		checkSize(data, 8, "TEST_CURRENT_24V_B");
		current_24v_b_m
				=dataToDouble(const_cast<std::vector<CAN::byte_t> &>(data));
		break;
	case TEST_ERROR_AND_ALARMS:
		std::cerr << "Processing RCA. TEST_ERROR_AND_ALARMS Received" << std::endl;
			checkSize(data, 4, "TEST_ERROR_AND_ALARMS");
			error_and_alarms_m
					=dataToLong(const_cast<std::vector<CAN::byte_t> &>(data));
			std::cerr << "error_and_alarms_m=" << error_and_alarms_m<< std::endl;
			break;
	case TEST_AMBIENT_TEMPERATURE:
			std::cerr << "Processing RCA. TEST_AMBIENT_TEMPERATURE Received" << std::endl;
				checkSize(data, 4, "TEST_AMBIENT_TEMPERATURE");
				ambient_temperature_m
						=dataToLong(const_cast<std::vector<CAN::byte_t> &>(data));
				std::cerr << "ambient_temperature_m" << ambient_temperature_m<< std::endl;
				break;
	case TEST_MAXMIN24VA:
				std::cerr << "Processing RCA. TEST_MAXMIN24VA" << std::endl;
					//checkSize(data, 6, "TEST_MAXMIN24VA");
				//std::vector<CAN::byte_t> tmp;
				//tmp=const_cast<std::vector<CAN::byte_t>& >(data)
					for(int i=5;i>=0;i--){
						max_min_24v_a_m[i]=data[i];
						std::cerr << "max_min_24v_a_m["<<i<<"]=" << std::hex << static_cast<int>(max_min_24v_a_m[i])<< std::endl;
					}
					break;
	case TEST_MAXMIN24VB:
			std::cerr << "Processing RCA. TEST_MAXMIN24VB" << std::endl;
			
				//checkSize(data, 6, "TEST_MAXMIN24VB");
				for(int i=5;i>=0;i--){
					max_min_24v_b_m[i]=data[i];
					std::cerr << "max_min_24v_b_m["<<i<<"]=" << std::hex << static_cast<int>(max_min_24v_b_m[i])<< std::endl;
					}
				break;
	default:
		ACS_DEBUG("PSD::control",
								"Unknown  RCA.");
					std::cerr << "Unknown RCA. (int)RCA= "<<rca << std::endl;
		throw PSDError("Unknown RCA  in control command");
	}
}

unsigned int AMB::PSD::get_trans_num() const {
  return trans_num_m;
}

std::vector<CAN::byte_t> AMB::PSD::get_ambient_temperature() const {
	std::vector<CAN::byte_t> tmp;
	return longlongToData(tmp,ambient_temperature_m,4);
}
std::vector<CAN::byte_t> AMB::PSD::error_and_alarms() const {
  std::vector<CAN::byte_t> tmp(3);
  unsigned long int aux=error_and_alarms_m;
  for (int i=2;i>=0;i--){
	  tmp[i]= aux & 0x0000FF;
	 // std::cerr << "AMB::PSD::error_and_alarms(). tmp["<<i<<"]="<< static_cast<int>(tmp[i]) << std::endl;
	  aux>>=8;
  }
  //const std::vector<CAN::byte_t> tmp;
  //  return longToData(tmp, error_and_alarms_m);
  return tmp;
}

std::vector<CAN::byte_t> AMB::PSD::max_min_24v_a() const {
  std::vector<CAN::byte_t> tmp(6);
      tmp[0] = (unsigned char)max_min_24v_a_m[0];
      tmp[1] = (unsigned char)max_min_24v_a_m[1];
      tmp[2] = (unsigned char)max_min_24v_a_m[2];
      tmp[3] = (unsigned char)max_min_24v_a_m[3];
      tmp[4] = (unsigned char)max_min_24v_a_m[4];
      tmp[5] = (unsigned char)max_min_24v_a_m[5];
      /*for(int i=5;i>=0;i--){
      		std::cerr << "max_min_24v_a_m["<<i<<"]=" << std::hex << tmp[i]<< std::endl;
      					}*/
      return tmp;
}
std::vector<CAN::byte_t> AMB::PSD::max_min_24v_b() const {
  std::vector<CAN::byte_t> tmp(6);
        tmp[0] = (unsigned char)max_min_24v_b_m[0];
        tmp[1] = (unsigned char)max_min_24v_b_m[1];
        tmp[2] = (unsigned char)max_min_24v_b_m[2];
        tmp[3] = (unsigned char)max_min_24v_b_m[3];
        tmp[4] = (unsigned char)max_min_24v_b_m[4];
        tmp[5] = (unsigned char)max_min_24v_b_m[5];
        /*for(int i=5;i>=0;i--){
              	std::cerr << "max_min_24v_a_m["<<i<<"]=" << std::hex << tmp[i]<< std::endl;
              					}*/
        return tmp;
}
std::vector<CAN::byte_t> AMB::PSD::voltage_24v_a() const {
  const std::vector<CAN::byte_t> tmp;
  const short int roundedValue = shutdown_m ? 0 :
    static_cast<short int>(voltage_24v_a_m*30.088) & 0x0FFF;
  return shortToData(tmp, roundedValue);
}

std::vector<CAN::byte_t> AMB::PSD::current_24v_a() const {
  const std::vector<CAN::byte_t> tmp;
  const short int roundedValue = shutdown_m ? 0 :
    static_cast<short int>(current_24v_a_m*46.035) & 0x0FFF;
  return shortToData(tmp, roundedValue);
}

std::vector<CAN::byte_t> AMB::PSD::voltage_24v_b() const {
  const std::vector<CAN::byte_t> tmp;
  const short int roundedValue = shutdown_m ? 0 :
    static_cast<short int>(voltage_24v_b_m*30.088) & 0x0FFF;
  return shortToData(tmp, roundedValue);
}

std::vector<CAN::byte_t> AMB::PSD::current_24v_b() const {
  const std::vector<CAN::byte_t> tmp;
  const short int roundedValue = shutdown_m ? 0 :
    static_cast<short int>(current_24v_b_m*46.035) & 0x0FFF;
  return shortToData(tmp, roundedValue);
}

void AMB::PSD::
shutdown_com(const std::vector<CAN::byte_t>& command) {
  if (command.size() != 1) {
    std::ostringstream errMessage;
    errMessage << "SHUTDOWN_COM command is incorrect length." << std::endl;
    errMessage << "Should be 1 and the actual value is  " 
	       <<  command.size() << std::endl;
    throw PSDError(errMessage.str());
  }
  if ((command[0] & 0xFE) != 0x00) {
    std::ostringstream errMessage;
    errMessage << "SHUTDOWN_COM command has unexpected bits set." << std::endl;
    errMessage << "The top 7-bits should be zero but 0x" 
	       << std::hex << std::setw(2) << std::setfill('0')
	       << static_cast<int>(command[0]) << " was received."
	       << std::endl;
    throw PSDError(errMessage.str());
  }
  if ((command[0] & 0x01) == 0x01) {
    shutdown_m = true;
  } else {
    shutdown_m = false;
  }
}

void AMB::PSD::checkSize(const std::vector<CAN::byte_t> &data, int size,
			  const char *pExceptionMsg) {
  if( static_cast<int>(data.size()) != size ) {
	char exceptionMsg[200];
	sprintf(exceptionMsg,"%s data length is %d must be %d",pExceptionMsg,
		data.size(), size);
	throw PSDError(exceptionMsg);
  }
}

