// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include <queue>
#include <iomanip>
#include <sys/time.h>
#include "ACUSim.h"
#include "ACUDrive.h"

const double bitsPerDegree = ((double)0x40000000 / 180.0);
const double degreesPerBit = (180.0 / (double)0x40000000);
const unsigned short IST_DEFAULT = 5 * 60;    // idle_stow_time default


/*
 * ACU error stack (duplicates operation of ACU monitor function)
 */
class ACUErrorStack
{
  public:
    // structure to hold error code to be assembled into the byte sequence
    typedef struct error_code {
        unsigned char error;
        unsigned long rca;
    } ERROR_CODE;

    void push_error(unsigned char error,unsigned long address)
    {
        /*
         * byte 0 is the error status, and then 4 bytes for the RCA
         * 5 bytes total
         */
        ERROR_CODE ec;
        ec.error = error;
        ec.rca = address;
        m_es.push(ec);
    }
    ERROR_CODE pop_error()
    {
        ERROR_CODE ec = { 0, 0 };
        if (m_es.empty())
            return ec;
    
        ec = m_es.front();
        m_es.pop();
        return ec;
    }
private:
    std::queue<ERROR_CODE> m_es;
};

ACUErrorStack acu_es;
int acu_errcode = 0;

// -----------------------------------------------------------------------------

ACUSim::ACUSim()
    : ACUbase(),
      system_status_m(0),
      idle_stow_time_m(IST_DEFAULT),
      shutter_m(0)
{
    drive_m = new ACUDrive();
}

// -----------------------------------------------------------------------------

ACUSim::~ACUSim()
{
    if (drive_m)
	delete drive_m;
}

// -----------------------------------------------------------------------------

unsigned long long ACUSim::getTimeStamp() const
{
const unsigned long long int timeFor1970 = 122192928000000000ULL;
    struct timeval tv;
    gettimeofday(&tv,0);
    return timeFor1970 + tv.tv_sec * 10000000LL + tv.tv_usec * 10LL;
}

////////////////////////////////////////////////////////////////////////////////
////                                                                        ////
////                            Monitor Points                              ////
////                                                                        ////
////////////////////////////////////////////////////////////////////////////////
      
// -----------------------------------------------------------------------------

std::vector<char> ACUSim::get_acu_mode_rsp(
	int& errcode,
	unsigned long long& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStamp();
    return drive_m->getACUMode();
}

// -----------------------------------------------------------------------------

unsigned long long ACUSim::get_acu_error(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStamp();
    /*
     * This is a funny value, so arrange the data in the first five bytes
     * with byte 0 as the error, and bytes 1-4 as the rca.  The rca is in
     * host byte order.
     */
    unsigned long long tmp;
    ACUErrorStack::ERROR_CODE ec = acu_es.pop_error();
    tmp = ec.error;
    unsigned char* ptr = (unsigned char*)&tmp;
    memcpy(ptr + 1,&ec.rca,4);
    return tmp;
}

// -----------------------------------------------------------------------------

unsigned long long ACUSim::get_az_posn_rsp(
	int& errcode,
	unsigned long long& timestamp) const
{
    double pos;
    double vel;
    long position;

    errcode = 0;

    // the acu simulator does not have the position for the time between
    // timing events.
    drive_m->getAzPsn(pos,vel);
    position = (long)(pos * (double)bitsPerDegree);
    unsigned long long rc = (unsigned long long)position;
    rc = rc << 32 | ((unsigned long long)position & 0xffffffff);
    timestamp = getTimeStamp();
    return rc;
}

// -----------------------------------------------------------------------------

unsigned long long ACUSim::get_el_posn_rsp(
	int& errcode,
	unsigned long long& timestamp) const
{
    double pos;
    double vel;
    long position;

    errcode = 0;

    // the acu simulator does not have the position for the time between
    // timing events.
    drive_m->getElPsn(pos,vel);
    position = (long)(pos * bitsPerDegree);
    unsigned long long rc = (unsigned long long) position;
    rc = rc << 32 | ((unsigned long long)position & 0xffffffff);
    timestamp = getTimeStamp();
    return rc;
}

#if 0  /* !!! NOT USED !!! */
// -----------------------------------------------------------------------------

std::vector <double> ACUSim::get_az_posn_rsp(
	int& errcode,
	double& timestamp) const
{
    std::vector <double> v;
    errcode = 0;
    timestamp = getTimeStamp();
    return v;
}

// -----------------------------------------------------------------------------

std::vector <double> ACUSim::get_el_posn_rsp(
	int& errcode,
	double& timestamp) const
{
    std::vector <double> v;
    errcode = 0;
    timestamp = getTimeStamp();
    return v;
}
#endif /* not used */

// -----------------------------------------------------------------------------

long ACUSim::get_az_enc(
	int& errcode,
	unsigned long long& timestamp) const
{
  // should remove pointing model from _RSP if in autonomous mode
  // unless Brian is wrong and they are the same
  return (get_az_posn_rsp(errcode,timestamp)
          // Encoder offset for the Vertex prototype antenna
          + static_cast<long>(184.54259 * bitsPerDegree))
    // scale correctly (for the Vertex prototype antenna
    /16 & 0x7ffFFFF;
}

// -----------------------------------------------------------------------------

void ACUSim::get_el_enc(
	std::vector<long>& positions,
	int& errcode,
	unsigned long long& timestamp) const
{
    double pos;
    double vel;
    long position;

    drive_m->getElPsn(pos,vel);
    position = static_cast<long>(bitsPerDegree * (pos - 84.6421990)) /16 & 0x7ffFFFF;
    timestamp = getTimeStamp();
    errcode = 0;
    positions[0] = position;
    positions[1] = positions[0];
}

#if 0  /* !!! NOT USED !!! */
// -----------------------------------------------------------------------------

double ACUSim::get_az_enc(
	int& errcode,
	double& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStamp();
    return 0;
}

// -----------------------------------------------------------------------------

double ACUSim::get_el_enc(
	int& errcode,
	double& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStamp();
    return 0;
}
#endif /* not used */

// -----------------------------------------------------------------------------

unsigned char ACUSim::get_az_enc_status(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStamp();
    return 0;
}

// -----------------------------------------------------------------------------

std::vector<char> ACUSim::get_el_enc_status(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStamp();
    std::vector<char> status(3);
    status[0] = 1;
    status[1] = 2;
    status[2] = 3;
    return status;
}

// -----------------------------------------------------------------------------

unsigned char ACUSim::get_az_aux_mode(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStamp();
    return(unsigned char) drive_m->Az.get_aux_mode();
}

// -----------------------------------------------------------------------------

unsigned char ACUSim::get_el_aux_mode(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStamp();
    return(unsigned char) drive_m->El.get_aux_mode();
}

// -----------------------------------------------------------------------------

std::vector<unsigned char> ACUSim::get_az_motor_currents(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStamp();
    std::vector<unsigned char> current(2);
    current[0] = 0x10;
    current[1] = 0x20;
    return current;
}

// -----------------------------------------------------------------------------

std::vector<unsigned char> ACUSim::get_az_motor_temps(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStamp();
    std::vector<unsigned char> temp(2);
    temp[0] = 0x30;
    temp[1] = 0x40;
    return temp;
}

// -----------------------------------------------------------------------------

unsigned long ACUSim::get_az_motor_torques(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStamp();
    return 0x4050;
}

// -----------------------------------------------------------------------------

unsigned long ACUSim::get_el_motor_currents(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStamp();
    return 0x40506070;
}

// -----------------------------------------------------------------------------

unsigned long ACUSim::get_el_motor_temps(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStamp();
    return 0x50607080;
}

// -----------------------------------------------------------------------------

unsigned long ACUSim::get_el_motor_torques(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStamp();
    return 0x60708090;
}

// -----------------------------------------------------------------------------

unsigned char ACUSim::get_az_brake(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStamp();
    return (unsigned char)drive_m->Az.get_brake();
}

// -----------------------------------------------------------------------------

unsigned char ACUSim::get_el_brake(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStamp();
    return (unsigned char)drive_m->El.get_brake();
}

// -----------------------------------------------------------------------------

double ACUSim::get_az_servo_coeff(
	long n,
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStamp();
    return drive_m->Az.servo_coefficients[n];
}

// -----------------------------------------------------------------------------

double ACUSim::get_el_servo_coeff(
	long n,
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStamp();
    return drive_m->El.servo_coefficients[n];
}

// -----------------------------------------------------------------------------

unsigned long long ACUSim::get_az_status(
	int& errcode,
	unsigned long long int& timestamp) const
{
    unsigned long long status;
    errcode = 0;
    drive_m->getAzStatus(status);
    timestamp = getTimeStamp();
    return status;
}

// -----------------------------------------------------------------------------

unsigned long long ACUSim::get_el_status(
	int& errcode,
	unsigned long long int& timestamp) const
{
    unsigned long long status;
    errcode = 0;
    drive_m->getElStatus(status);
    timestamp = getTimeStamp();
    return status;
}

// -----------------------------------------------------------------------------

unsigned short ACUSim::get_idle_stow_time(
	int& errcode,
	unsigned long long& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStamp();
    return idle_stow_time_m;
}

// -----------------------------------------------------------------------------

unsigned char ACUSim::get_shutter(
	int& errcode,
	unsigned long long& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStamp();
    return shutter_m;
}

// -----------------------------------------------------------------------------

unsigned short ACUSim::get_stow_pin(
	int& errcode,
	unsigned long long int& timestamp) const
{
    unsigned short pins;
    unsigned char* ptr = (unsigned char*)&pins;
    errcode = 0;
    timestamp = getTimeStamp();
    ptr[0] = drive_m->Az.get_stow_pin();
    ptr[1] = drive_m->El.get_stow_pin();
    return pins;
}

// -----------------------------------------------------------------------------

unsigned long ACUSim::get_system_status(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStamp();
    return system_status_m;
}

// -----------------------------------------------------------------------------

void ACUSim::sim_system_status(const unsigned long value,
			       int& errcode,
			       unsigned long long& timestamp)
{
    errcode = 0;
    timestamp = getTimeStamp();
    system_status_m = value; 
}

// -----------------------------------------------------------------------------

unsigned long ACUSim::get_ip_address(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStamp();
    return 0x01020304;  // should be 1:2:3:4
}

////////////////////////////////////////////////////////////////////////////////
////                                                                        ////
////                              Control Points                            ////
////                                                                        ////
////////////////////////////////////////////////////////////////////////////////
      
// -----------------------------------------------------------------------------

void ACUSim::reset_acu_cmd(
	int& errcode,
	unsigned long long int& timestamp)
{
    errcode = 0;
    timestamp = getTimeStamp();
}

// -----------------------------------------------------------------------------

void ACUSim::acu_mode_cmd(
	const unsigned char mode,
	int& errcode,
	unsigned long long& timestamp)
{
    errcode = 0;
    timestamp = getTimeStamp();

    if (drive_m->setACUMode(mode) != 0)
        acu_errcode = INVALID_MODE_CHANGE;
    else
        acu_errcode = 0;
}

// -----------------------------------------------------------------------------

void ACUSim::clear_fault_cmd(
	int& errcode,
	unsigned long long int& timestamp)
{
    errcode = 0;
    timestamp = getTimeStamp();
}

// -----------------------------------------------------------------------------

void ACUSim::az_traj_cmd(
	const long position,
	const long velocity,
	int& errcode,
	unsigned long long& timestamp)
{
    // need to remember the time this command came in to later
    // predict az position.  the equations are kept in binary parts
    // of a circle.
    //
    // later, az encoder = x0 + v * dt + a * dt * dt / 2 where dt is
    // time since az traj last commanded
    // this is all integer math.  to more accurately represent the encoders,
    // the az encoder value should be masked to produce the upper 26 bits,
    // since the encoders are 26 bits for a 720 degree range.
    //
    // the maximum velocity is 6 deg/sec, and the maximum acceleration is 12
    // deg/sec/sec.
    //
    errcode = 0;

    double pos = (double)(position) * degreesPerBit;
    double vel = (double)(velocity) * degreesPerBit;

    switch (drive_m->setAzPsn(pos,vel))
	{
	case 1:
	    acu_errcode = INVALID_BRAKE_CMD;
	    break;

	case 2:
	    acu_errcode = POSITION_CMD_RANGE_ERR;
	    break;

	case 3:
	    acu_errcode = VELOCITY_CMD_RANGE_ERR;
	    break;

	case 4:
	    acu_errcode = INVALID_MODE_CHANGE;
	    break;

	default:
	    acu_errcode = 0;
	    break;
	}

    timestamp = getTimeStamp();
}

// -----------------------------------------------------------------------------

void ACUSim::el_traj_cmd(
	const long position,
	const long velocity,
	int& errcode,
	unsigned long long& timestamp)
{
    errcode = 0;

    double pos = (double)(position) * degreesPerBit;
    double vel = (double)(velocity) * degreesPerBit;

    switch (drive_m->setElPsn(pos,vel))
	{
	case 1:
	    acu_errcode = INVALID_BRAKE_CMD;
	    break;

	case 2:
	    acu_errcode = POSITION_CMD_RANGE_ERR;
	    break;

	case 3:
	    acu_errcode = VELOCITY_CMD_RANGE_ERR;
	    break;

	case 4:
	    acu_errcode = INVALID_MODE_CHANGE;
	    break;

	default:
	    acu_errcode = 0;
	    break;
    }

    timestamp = getTimeStamp();
}

#if 0  /* !!! NOT USED !!! */
// -----------------------------------------------------------------------------

void ACUSim::az_traj_cmd(
	const double position,
	const double velocity,
	int& errcode,
	double& timestamp)
{
    errcode = 0;
    timestamp = getTimeStamp();
}

// -----------------------------------------------------------------------------

void ACUSim::el_traj_cmd(
	const double position,
	const double velocity,
	int& errcode,
	double& timestamp)
{
    errcode = 0;
    timestamp = getTimeStamp();
}
#endif /* not used */

// -----------------------------------------------------------------------------

void ACUSim::set_az_aux_mode(
	const unsigned char mode,
	int& errcode,
	unsigned long long int& timestamp)
{
    errcode = 0;
    drive_m->Az.set_aux_mode((AUX_Mode_t)mode);
    timestamp = getTimeStamp();
}

// -----------------------------------------------------------------------------

void ACUSim::set_el_aux_mode(
	const unsigned char mode,
	int& errcode,
	unsigned long long int& timestamp)
{
    errcode = 0;
    drive_m->El.set_aux_mode((AUX_Mode_t)mode);
    timestamp = getTimeStamp();
}

// -----------------------------------------------------------------------------

void ACUSim::set_az_brake(
	const unsigned char mode,
	int& errcode,
	unsigned long long int& timestamp)
{
    errcode = 0;
    if (drive_m->Az.set_brake((AZEL_Brake_t)mode) != 0)
        acu_errcode = INVALID_BRAKE_CMD;
    else
        acu_errcode = 0;
    timestamp = getTimeStamp();
}

// -----------------------------------------------------------------------------

void ACUSim::set_el_brake(
	const unsigned char mode,
	int& errcode,
	unsigned long long int& timestamp)
{
    errcode = 0;
    if (drive_m->El.set_brake((AZEL_Brake_t)mode))
        acu_errcode = INVALID_BRAKE_CMD;
    else
        acu_errcode = 0;
    timestamp = getTimeStamp();
}

// -----------------------------------------------------------------------------

void ACUSim::set_az_servo_coeff(
	const long index,
	const double value,
	int& errcode,
	unsigned long long int& timestamp)
{
    drive_m->Az.servo_coefficients[index] = value;
    errcode = 0;
    timestamp = getTimeStamp();
}

// -----------------------------------------------------------------------------

void ACUSim::set_el_servo_coeff(
	const long index,
	const double value,
	int& errcode,
	unsigned long long int& timestamp)
{
    drive_m->El.servo_coefficients[index] = value;
    errcode = 0;
    timestamp = getTimeStamp();
}

// -----------------------------------------------------------------------------

void ACUSim::set_az_servo_default(
	int& errcode,
	unsigned long long int& timestamp)
{
    errcode = 0;
    timestamp = getTimeStamp();
    drive_m->Az.set_servo_defaults();
}

// -----------------------------------------------------------------------------

void ACUSim::set_el_servo_default(
	int& errcode,
	unsigned long long int& timestamp)
{
    errcode = 0;
    timestamp = getTimeStamp();
    drive_m->El.set_servo_defaults();
}

// -----------------------------------------------------------------------------

void ACUSim::set_idle_stow_time(
	const unsigned short seconds,
	int& errcode,
	unsigned long long& timestamp)
{
    errcode = 0;
    idle_stow_time_m = seconds;
    timestamp = getTimeStamp();
}

// -----------------------------------------------------------------------------

void ACUSim::set_shutter(
	const unsigned char value,
	int& errcode,
	unsigned long long& timestamp)
{
    errcode = 0;
    timestamp = getTimeStamp();
    shutter_m = value;
}

// -----------------------------------------------------------------------------

void ACUSim::set_stow_pin(
	const unsigned short bits,
	int& errcode,
	unsigned long long int& timestamp)
{
    unsigned char* ptr = (unsigned char*)&bits;
    drive_m->Az.set_stow_pin((Stow_Pin_t)(ptr[0]));
    drive_m->El.set_stow_pin((Stow_Pin_t)(ptr[1]));
    timestamp = getTimeStamp();
}

////////////////////////////////////////////////////////////////////////////////
////                                                                        ////
////                         Generic Monitor Points                         ////
////                                                                        ////
////////////////////////////////////////////////////////////////////////////////
      
// -----------------------------------------------------------------------------

std::vector<char> ACUSim::get_sw_rev_level(
    int& errcode,
    unsigned long long& timestamp) const
{
    std::vector<char> tmp(3);
    tmp[0] = 1;	// major
    tmp[1] = 2;	// minor
    tmp[2] = 3;	// patch
    return tmp;
}

// -----------------------------------------------------------------------------

unsigned long ACUSim::get_can_error(
    int& errcode,
    unsigned long long& timestamp) const
{
    return 0;
}

// -----------------------------------------------------------------------------

unsigned long ACUSim::get_num_trans(
    int& errcode,
    unsigned long long& timestamp) const
{
    return 0;
}

// -----------------------------------------------------------------------------

std::vector<char> ACUSim::get_system_id(
    int& errcode,
    unsigned long long& timestamp) const
{
    // Notice ACU version has extra status byte compared to mandatory version.
    std::vector<char> tmp(4);
    tmp[0] = 0xAB;      // major
    tmp[1] = 0xCD;      // minor
    tmp[2] = 0xEF;      // release
    tmp[3] = 0x87;      // status byte
    /*
     * Byte 3 -
     * bit 0: encoder interface board detected    
     * bit 1: CAN bus interface board (to servo amplifiers) detected  
     * bit 2: dig/ana I/O board detected    
     * bit 3-6:    
     * bit 7: Ethernet interface (on CPU) detected
     */
    return tmp;
}

void ACUSim::setSharedSimulator(SharedSimInterface* ssim) {
  if (drive_m) drive_m->setSharedSimulator(ssim);
}
