// @(#) $Id$
//
// Copyright (C) 2002
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include <iomanip>
#include <math.h>
#include "WVR.h"
#include "AMBUtil.h"

#include <stdio.h>
#include <sys/time.h>

#undef debug

// ----------------------------------------------------------------------------
AMB::WVR::WVR(node_t node, const std::vector<CAN::byte_t> &serialNumber)
  : m_node(node), 
    m_sn(serialNumber),
    m_trans_num(0),
    m_errorCode(1),
    STATE_m(0)
{
  std::cout << "Creating node " << node << ",s/n 0x";
  for (int i = 0; i < 8; i++) {
    std::cout << std::hex << std::setw(2) << std::setfill('0') 
	      << static_cast<int>(serialNumber[i]);
  }
  std::cout << ", WVR device" << std::endl;

  // get the start time
  gettimeofday(&start_m, 0);
}

// ----------------------------------------------------------------------------
AMB::WVR::~WVR() {
  // Nothing
}

// ----------------------------------------------------------------------------
AMB::node_t AMB::WVR::node() const {
  return m_node;
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::WVR::serialNumber() const {
  return m_sn;
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::WVR::monitor(rca_t rca) const {
  m_trans_num++;
  const std::vector<CAN::byte_t> tmp;
  std::vector<CAN::byte_t> retVal;

  switch (rca) {
    // Mandatory generic points
  case GET_SERIAL_NUMBER:
    return serialNumber();
    break;
  case GET_CAN_ERROR:
    return get_can_error();
    break;
  case GET_PROTOCOL_REV_LEVEL:
    return get_protocol_rev_level();
    break;
	case AMB::Device::GET_SW_REV_LEVEL:
		return get_software_rev_level();
		break;
	case AMB::Device::GET_HW_REV_LEVEL:
		return get_hardware_rev_level();
		break;
  case GET_TRANS_NUM:
    return longToData(tmp, get_trans_num());
    break;
    // Optional generic point
  case GET_AMBIENT_TEMPERATURE:
    return floatDS1820ToData(tmp, get_ambient_temperature());
    break;
    
    // WVR specific points
  case GET_WVR_STATE:
    return CAN_WVR_STATE();
    break;
  case GET_INT_TIME:
    return CAN_INT_TIME();
    break;
  case GET_BLK_TIME:
    return CAN_BLK_TIME();
    break;
  case GET_CYC_IN_RES:
    return CAN_CYC_IN_RES();
    break;
  case GET_CYC_IN_CAL:
    return CAN_CYC_IN_CAL();
    break;
  case GET_CAL_PER:
    return CAN_CAL_PER();
    break;
  case GET_LO_STATE:
    return CAN_LO_STATE();
    break;
  case GET_PHASE_TIME:
    return CAN_PHASE_TIME();
    break;
  case GET_IHL_DATA:
    return CAN_IHL_DATA();
    break;
  case GET_ICL_DATA:
    return CAN_ICL_DATA();
    break;
  case GET_HTR0_DATA:
    return CAN_HTR0_DATA();
    break;
  case GET_HTR1_DATA:
    return CAN_HTR1_DATA();
    break;
  case GET_PELT_SENS:
    return CAN_PELT_SENS();
    break;
  case GET_PELT_THRESH:
    return CAN_PELT_THRESH();
    break;
  case GET_DDS_DATA0:
    return CAN_DDS_DATA0();
    break;
  case GET_DDS_DATA1:
    return CAN_DDS_DATA1();
    break;
  case GET_DIAG_CONF:
    return CAN_DIAG_CONF();
    break;
  case GET_VANE_POS:
    return CAN_VANE_POS();
    break;
  case GET_WVR_CONF:
    return CAN_WVR_CONF();
    break;
  case GET_PLL_LOCK:
    return CAN_PLL_LOCK();
    break;
  case GET_YIG_LOCK:
    return CAN_YIG_LOCK();
    break;
  case GET_IF_PWROK:
    return CAN_IF_PWROK();
    break;
  case GET_REF_OK:
    return CAN_REF_OK();
    break;
  case GET_CHOP_LOCK:
    return CAN_CHOP_LOCK();
    break;
  case GET_CHOP_SENS:
    return CAN_CHOP_SENS();
    break;
  case GET_CHOP_STATE:
    return CAN_CHOP_STATE();
    break;
  case GET_VANE_TMOUT:
    return CAN_VANE_TMOUT();
    break;
  case GET_CHOP_TMOUT:
    return CAN_CHOP_TMOUT();
    break;
  case GET_ICL_STATE:
    return CAN_ICL_STATE();
    break;
  case GET_HTR0_STATE:
    return CAN_HTR0_STATE();
    break;
  case GET_HTR1_STATE:
    return CAN_HTR1_STATE();
    break;
  case GET_VANE_STATE:
    return CAN_VANE_STATE();
    break;
  case GET_TEMP_LO0:
    return CAN_TEMP_LO0();
    break;
  case GET_TEMP_LO1:
    return CAN_TEMP_LO1();
    break;
  case GET_TEMP_LO2:
    return CAN_TEMP_LO2();
    break;
  case GET_TEMP_LO3:
    return CAN_TEMP_LO3();
    break;
  case GET_TEMP_LO4:
    return CAN_TEMP_LO4();
    break;
  case GET_TEMP_LO5:
    return CAN_TEMP_LO5();
    break;
  case GET_TEMP_LO6:
    return CAN_TEMP_LO6();
    break;
  case GET_TEMP_LO7:
    return CAN_TEMP_LO7();
    break;
  case GET_PSUV_LO0:
    return CAN_PSUV_LO0();
    break;
  case GET_PSUV_LO1:
    return CAN_PSUV_LO1();
    break;
  case GET_PSUV_LO2:
    return CAN_PSUV_LO2();
    break;
  case GET_PSUV_LO3:
    return CAN_PSUV_LO3();
    break;
  case GET_PSUV_LO4:
    return CAN_PSUV_LO4();
    break;
  case GET_PSUV_LO5:
    return CAN_PSUV_LO5();
    break;
  case GET_PSUV_LO6:
    return CAN_PSUV_LO6();
    break;
  case GET_PSUV_LO7:
    return CAN_PSUV_LO7();
    break;
  case GET_GUNNV0_LO:
    return CAN_GUNNV0_LO();
    break;
  case GET_IFPWR0_LO:
    return CAN_IFPWR0_LO();
    break;
  case GET_GUNNV1_LO:
    return CAN_GUNNV1_LO();
    break;
  case GET_IFPWR1_LO:
    return CAN_IFPWR1_LO();
    break;
  case GET_TEMP_HI0:
    return CAN_TEMP_HI0();
    break;
  case GET_TEMP_HI1:
    return CAN_TEMP_HI1();
    break;
  case GET_TEMP_HI2:
    return CAN_TEMP_HI2();
    break;
  case GET_TEMP_HI3:
    return CAN_TEMP_HI3();
    break;
  case GET_TEMP_HI4:
    return CAN_TEMP_HI4();
    break;
  case GET_TEMP_HI5:
    return CAN_TEMP_HI5();
    break;
  case GET_TEMP_HI6:
    return CAN_TEMP_HI6();
    break;
  case GET_TEMP_HI7:
    return CAN_TEMP_HI7();
    break;
  case GET_PSUV_HI0:
    return CAN_PSUV_HI0();
    break;
  case GET_PSUV_HI1:
    return CAN_PSUV_HI1();
    break;
  case GET_PSUV_HI2:
    return CAN_PSUV_HI2();
    break;
  case GET_PSUV_HI3:
    return CAN_PSUV_HI3();
    break;
  case GET_PSUV_HI4:
    return CAN_PSUV_HI4();
    break;
  case GET_PSUV_HI5:
    return CAN_PSUV_HI5();
    break;
  case GET_PSUV_HI6:
    return CAN_PSUV_HI6();
    break;
  case GET_PSUV_HI7:
    return CAN_PSUV_HI7();
    break;
  case GET_GUNNV0_HI:
    return CAN_GUNNV0_HI();
    break;
  case GET_IFPWR0_HI:
    return CAN_IFPWR0_HI();
    break;
  case GET_GUNNV1_HI:
    return CAN_GUNNV1_HI();
    break;
  case GET_IFPWR1_HI:
    return CAN_IFPWR1_HI();
    break;
  case GET_TEMP0:
    return CAN_TEMP0();
    break;
  case GET_TEMP1:
    return CAN_TEMP1();
    break;
  case GET_TEMP2:
    return CAN_TEMP2();
    break;
  case GET_TEMP3:
    return CAN_TEMP3();
    break;
  case GET_TEMP4:
    return CAN_TEMP4();
    break;
  case GET_TEMP5:
    return CAN_TEMP5();
    break;
  case GET_TEMP6:
    return CAN_TEMP6();
    break;
  case GET_TEMP7:
    return CAN_TEMP7();
    break;
  case GET_PSUV0:
    return CAN_PSUV0();
    break;
  case GET_PSUV1:
    return CAN_PSUV1();
    break;
  case GET_PSUV2:
    return CAN_PSUV2();
    break;
  case GET_PSUV3:
    return CAN_PSUV3();
    break;
  case GET_PSUV4:
    return CAN_PSUV4();
    break;
  case GET_PSUV5:
    return CAN_PSUV5();
    break;
  case GET_PSUV6:
    return CAN_PSUV6();
    break;
  case GET_PSUV7:
    return CAN_PSUV7();
    break;
  case GET_GUNNV0:
    return CAN_GUNNV0();
    break;
  case GET_IFPWR0:
    return CAN_IFPWR0();
    break;
  case GET_GUNNV1:
    return CAN_GUNNV1();
    break;
  case GET_IFPWR1:
    return CAN_IFPWR1();
    break;
  case GET_ALM_DIS0:
    return CAN_ALM_DIS0();
    break;
  case GET_ALM_DIS1:
    return CAN_ALM_DIS1();
    break;
  case GET_ALM_DIS2:
    return CAN_ALM_DIS2();
    break;
  case GET_ALM_DIS3:
    return CAN_ALM_DIS3();
    break;
  case GET_ALM_QT0:
    return CAN_ALM_QT0();
    break;
  case GET_ALM_QT1:
    return CAN_ALM_QT1();
    break;
  case GET_ALM_QT2:
    return CAN_ALM_QT2();
    break;
  case GET_ALM_QT3:
    return CAN_ALM_QT3();
    break;
  case GET_ALM_STATE0:
    return CAN_ALM_STATE0();
    break;
  case GET_ALM_STATE1:
    return CAN_ALM_STATE1();
    break;
  case GET_ALM_STATE2:
    return CAN_ALM_STATE2();
    break;
  case GET_ALM_STATE3:
    return CAN_ALM_STATE3();
    break;
  case GET_RAW_TSKY0:
    return CAN_RAW_TSKY0();
    break;
  case GET_RAW_TSKY1:
    return CAN_RAW_TSKY1();
    break;
  case GET_RAW_TSKY2:
    return CAN_RAW_TSKY2();
    break;
  case GET_RAW_TSKY3:
    return CAN_RAW_TSKY3();
    break;
  case GET_RAW_TSKY4:
    return CAN_RAW_TSKY4();
    break;
  case GET_RAW_TSKY5:
    return CAN_RAW_TSKY5();
    break;
  case GET_RAW_TSKY6:
    return CAN_RAW_TSKY6();
    break;
  case GET_RAW_TSKY7:
    return CAN_RAW_TSKY7();
    break;
  case GET_TSKY0:
    return CAN_TSKY0();
    break;
  case GET_TSKY1:
    return CAN_TSKY1();
    break;
  case GET_TSKY2:
    return CAN_TSKY2();
    break;
  case GET_TSKY3:
    return CAN_TSKY3();
    break;
  case GET_TSKY4:
    return CAN_TSKY4();
    break;
  case GET_TSKY5:
    return CAN_TSKY5();
    break;
  case GET_TSKY6:
    return CAN_TSKY6();
    break;
  case GET_TSKY7:
    return CAN_TSKY7();
    break;
  case GET_CALG_LO:
    return CAN_CALG_LO();
    break;
  case GET_CALG_HI:
    return CAN_CALG_HI();
    break;
  case GET_CALTRX_LO:
    return CAN_CALTRX_LO();
    break;
  case GET_CALTRX_HI:
    return CAN_CALTRX_HI();
    break;
  case GET_TRX_SM0:
    return CAN_TRX_SM0();
    break;
  case GET_TRX_SM1:
    return CAN_TRX_SM1();
    break;
  case GET_TSKYSTAMP:
    return CAN_TSKYSTAMP();
    break;
  case GET_CALG0:
    return CAN_CALG0();
    break;
  case GET_CALG1:
    return CAN_CALG1();
    break;
  case GET_CALG2:
    return CAN_CALG2();
    break;
  case GET_CALG3:
    return CAN_CALG3();
    break;
  case GET_CALG4:
    return CAN_CALG4();
    break;
  case GET_CALG5:
    return CAN_CALG5();
    break;
  case GET_CALG6:
    return CAN_CALG6();
    break;
  case GET_CALG7:
    return CAN_CALG7();
    break;
  case GET_CALG8:
    return CAN_CALG8();
    break;
  case GET_CALG9:
    return CAN_CALG9();
    break;
  case GET_CALG10:
    return CAN_CALG10();
    break;
  case GET_CALG11:
    return CAN_CALG11();
    break;
  case GET_CALG12:
    return CAN_CALG12();
    break;
  case GET_CALG13:
    return CAN_CALG13();
    break;
  case GET_CALG14:
    return CAN_CALG14();
    break;
  case GET_CALG15:
    return CAN_CALG15();
    break;
  case GET_CALTRX0:
    return CAN_CALTRX0();
    break;
  case GET_CALTRX1:
    return CAN_CALTRX1();
    break;
  case GET_CALTRX2:
    return CAN_CALTRX2();
    break;
  case GET_CALTRX3:
    return CAN_CALTRX3();
    break;
  case GET_CALTRX4:
    return CAN_CALTRX4();
    break;
  case GET_CALTRX5:
    return CAN_CALTRX5();
    break;
  case GET_CALTRX6:
    return CAN_CALTRX6();
    break;
  case GET_CALTRX7:
    return CAN_CALTRX7();
    break;
  case GET_CALTRX8:
    return CAN_CALTRX8();
    break;
  case GET_CALTRX9:
    return CAN_CALTRX9();
    break;
  case GET_CALTRX10:
    return CAN_CALTRX10();
    break;
  case GET_CALTRX11:
    return CAN_CALTRX11();
    break;
  case GET_CALTRX12:
    return CAN_CALTRX12();
    break;
  case GET_CALTRX13:
    return CAN_CALTRX13();
    break;
  case GET_CALTRX14:
    return CAN_CALTRX14();
    break;
  case GET_CALTRX15:
    return CAN_CALTRX15();
    break;
  case GET_T0_AMB0:
    return CAN_T0_AMB0();
    break;
  case GET_T0_AMB1:
    return CAN_T0_AMB1();
    break;
  case GET_T0_AMB2:
    return CAN_T0_AMB2();
    break;
  case GET_T0_AMB3:
    return CAN_T0_AMB3();
    break;
  case GET_T0_AMB4:
    return CAN_T0_AMB4();
    break;
  case GET_T0_AMB5:
    return CAN_T0_AMB5();
    break;
  case GET_T0_AMB6:
    return CAN_T0_AMB6();
    break;
  case GET_T0_AMB7:
    return CAN_T0_AMB7();
    break;
  case GET_T0_COLD0:
    return CAN_T0_COLD0();
    break;
  case GET_T0_COLD1:
    return CAN_T0_COLD1();
    break;
  case GET_T0_COLD2:
    return CAN_T0_COLD2();
    break;
  case GET_T0_COLD3:
    return CAN_T0_COLD3();
    break;
  case GET_T0_COLD4:
    return CAN_T0_COLD4();
    break;
  case GET_T0_COLD5:
    return CAN_T0_COLD5();
    break;
  case GET_T0_COLD6:
    return CAN_T0_COLD6();
    break;
  case GET_T0_COLD7:
    return CAN_T0_COLD7();
  case GET_RAW_AD0:
    return CAN_RAW_AD0();
  case GET_RAW_AD1:
    return CAN_RAW_AD1();
  case GET_RAW_AD2:
    return CAN_RAW_AD2();
  case GET_RAW_AD3:
    return CAN_RAW_AD3();
  case GET_RAW_AD4:
    return CAN_RAW_AD4();
  case GET_RAW_AD5:
    return CAN_RAW_AD5();
  case GET_RAW_AD6:
    return CAN_RAW_AD6();
  case GET_RAW_AD7:
    return CAN_RAW_AD7();
  case GET_RAW_AD8:
    return CAN_RAW_AD8();
  case GET_RAW_AD9:
    return CAN_RAW_AD9();
  case GET_RAW_AD10:
    return CAN_RAW_AD10();
  case GET_RAW_AD11:
    return CAN_RAW_AD11();
  case GET_RAW_AD12:
    return CAN_RAW_AD12();
  case GET_RAW_AD13:
    return CAN_RAW_AD13();
  case GET_RAW_AD14:
    return CAN_RAW_AD14();
  case GET_RAW_AD15:
    return CAN_RAW_AD15();
    break;

  default:
    std::cerr << "Switch '" << static_cast<int>(rca) 
	      << "' does not match any WVR case." << std::endl;
    throw WVRError("Unknown RCA in monitor command");
  }
}

// ----------------------------------------------------------------------------
void AMB::WVR::control(rca_t rca, const std::vector<CAN::byte_t> &data) {
    std::vector<CAN::byte_t> temp(data);
#ifdef debug
    float tmpVal;
#endif
    unsigned char tmpCh;

    m_trans_num++;

    switch (rca) {
    case SET_WVR_STATE:
	checkSize(data, 2, "Set WVR State");
	STATE_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: state set to %i\n", STATE_m);
#endif
	break;
    case SET_INT_TIME:
	checkSize(data, 1, "Set WVR Integration Time");
	INT_TIME_m = dataToChar(temp);
#ifdef debug
	printf("AMB::WVR::control: state integration time to %i\n", INT_TIME_m);
#endif
	break;
    case SET_BLK_TIME:
	checkSize(data, 1, "Set WVR Block Time");
	BLK_TIME_m = dataToChar(temp);
#ifdef debug
	printf("AMB::WVR::control: state block time to %i\n", BLK_TIME_m);
#endif
	break;
    case SET_CYC_IN_RES:
	checkSize(data, 2, "Set WVR CYC_IN_RES");
	CYC_IN_RES_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: CYC_IN_RES to %i\n", CYC_IN_RES_m);
#endif
	break;
    case SET_CYC_IN_CAL:
	checkSize(data, 1, "Set WVR CYC_IN_CAL");
	CYC_IN_CAL_m = dataToChar(temp);
#ifdef debug
	printf("AMB::WVR::control: CYC_IN_CAL to %i\n", CYC_IN_CAL_m);
#endif
	break;
    case SET_CAL_PER:
	checkSize(data, 2, "Set WVR CAL_PER");
	CAL_PER_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: CAL_PER to %i\n", CAL_PER_m);
#endif
	break;
    case SET_LO_STATE:
	checkSize(data, 1, "Set WVR LO_STATE");
	LO_STATE_m = dataToChar(temp);
#ifdef debug
	printf("AMB::WVR::control: LO_STATE to %i\n", LO_STATE_m);
#endif
	break;
    case SET_PHASE_TIME:
	checkSize(data, 1, "Set WVR PHASE_TIME");
	PHASE_TIME_m = dataToChar(temp);
#ifdef debug
	printf("AMB::WVR::control: PHASE_TIME to %i\n", PHASE_TIME_m);
#endif
	break;
    case SET_IHL_DATA:
	checkSize(data, 8, "Set WVR IHL_DATA");
	IHL_DATA_m = dataToLonglong(temp, 8);
#ifdef debug
	printf("AMB::WVR::control: IHL_DATA to %llu\n", IHL_DATA_m);
#endif
    case SET_ICL_DATA:
      {
	checkSize(data, 3, "Set WVR ICL_DATA");
#ifdef debug
	printf("AMB::WVR::control: ICL_DATA: ");
	for (int i=0; i<3; i++)
	    printf(" %#x", data[i]);
	printf("\n");
#endif
	unsigned char * pi = (unsigned char *)&ICL_DATA_m;
	pi[2] = dataToChar(temp);
	pi[1] = dataToChar(temp);
	pi[0] = dataToChar(temp);
      }
	break;
    case SET_HTR0_DATA:
	checkSize(data, 2, "Set WVR HTR0_DATA");
	HTR0_DATA_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: HTR0_DATA to %lu\n", HTR0_DATA_m);
#endif
	break;
    case SET_HTR1_DATA:
	checkSize(data, 2, "Set WVR HTR1_DATA");
	HTR1_DATA_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: HTR1_DATA to %lu\n", HTR1_DATA_m);
#endif
	break;
    case SET_PELT_SENS:
	checkSize(data, 1, "Set WVR PELT_SENS");
	PELT_SENS_m = dataToChar(temp);
#ifdef debug
	printf("AMB::WVR::control: PELT_SENS to %#x\n", PELT_SENS_m);
#endif
	break;
    case SET_PELT_THRESH:
	checkSize(data, 4, "Set WVR PELT_THRESH");
	PELT_THRESH_m = dataToLong(temp);
#ifdef debug
	printf("AMB::WVR::control: PELT_THRESH to %lu\n", PELT_THRESH_m);
#endif
	break;
    case SET_DDS_DATA0:
	checkSize(data, 8, "Set WVR DDS_DATA0");
#ifdef debug
	printf("AMB::WVR::control: DDS_DATA0: ");
	for (int i=0; i<8; i++)
	    printf(" %#x", data[i]);
	printf("\n");
#endif
	DDS_DATA0_m = dataToLonglong(temp, 8);
#ifdef debug
	long tmpLong1 = DDS_DATA0_m & 0xffff;
	long tmpLong2 = (DDS_DATA0_m >> 32) & 0xffff;
	printf("AMB::WVR::control: DDS_DATA0 to %llu,(%lu + %lu)\n", 
	       DDS_DATA0_m, tmpLong2, tmpLong1); 
#endif
	break;
    case SET_DDS_DATA1:
	checkSize(data, 8, "Set WVR DDS_DATA1");
#ifdef debug
	printf("AMB::WVR::control: DDS_DATA1: ");
	for (int i=0; i<8; i++)
	    printf(" %#x", data[i]);
	printf("\n");
#endif
	DDS_DATA1_m = dataToLonglong(temp, 8);
#ifdef debug
	unsigned char * p = (unsigned char *)&DDS_DATA1_m;
	printf("AMB::WVR::control: DDS_DATA1 to %llu, ( ", 
	       DDS_DATA1_m);
	for (int i = 0; i<8; i++)
	    printf("%i ",p[i]);
	printf(" )\n");
#endif
	break;
    case SET_NV_STORE:
	checkSize(data, 8, "Set WVR NV_STORE");
#ifdef debug
	unsigned long long nvStore = dataToLonglong(temp, 8);
	printf("AMB::WVR::control: NV_STORE to %llu\n", nvStore);
#endif
	break;
    case SET_NV_LOAD:
	checkSize(data, 1, "Set WVR NV_LOAD");
	tmpCh = dataToChar(temp);
#ifdef debug
	printf("AMB::WVR::control: NV_LOAD to %i\n", tmpCh);
#endif
	break;
    case SET_FORCE_CAL:
	checkSize(data, 1, "Set WVR FORCE_CAL");
	tmpCh = dataToChar(temp);
#ifdef debug
	printf("AMB::WVR::control: FORCE_CAL to %i\n", tmpCh);
#endif
	break;
    case SET_FORCE_INT:
	checkSize(data, 1, "Set WVR FORCE_INT");
	tmpCh = dataToChar(temp);
#ifdef debug
	printf("AMB::WVR::control: FORCE_INT to %i\n", tmpCh);
#endif
	break;
    case SET_FORCE_CYC:
	checkSize(data, 1, "Set WVR FORCE_CYC");
	tmpCh = dataToChar(temp);
#ifdef debug
	printf("AMB::WVR::control: FORCE_CYC to %i\n", tmpCh);
#endif
	break;
    case SET_DIAG_CONF:
	checkSize(data, 8, "Set WVR DIAG_CONF");
	DIAG_CONF_m = dataToLonglong(temp, 8);
#ifdef debug
	printf("AMB::WVR::control: DIAG_CONF to %llu\n", DIAG_CONF_m);
#endif
	break;
    case SET_VANE_POS:
	checkSize(data, 1, "Set WVR VANE_POS");
	VANE_POS_m = dataToChar(temp);
#ifdef debug
	printf("AMB::WVR::control: VANE_POS to %i\n", VANE_POS_m);
#endif
	break;
    case SET_WVR_CONF:
	checkSize(data, 8, "Set WVR WVR_CONF");
	WVR_CONF_m = dataToLonglong(temp, 8);
#ifdef debug
	printf("AMB::WVR::control: WVR_CONF to %llu\n", WVR_CONF_m); 
#endif
	break;
    case SET_VANE_TMOUT:
	checkSize(data, 2, "Set WVR VANE_TMOUT");
	VANE_TMOUT_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: VANE_TMOUT to %i\n", VANE_TMOUT_m);
#endif
	break;
    case SET_CHOP_TMOUT:
	checkSize(data, 1, "Set WVR CHOP_TMOUT");
	CHOP_TMOUT_m = dataToChar(temp);
#ifdef debug
	printf("AMB::WVR::control: CHOP_TMOUT to %i\n", CHOP_TMOUT_m);
#endif
	break;
    case SET_TEMP_LO0:
	checkSize(data, 2, "Set TEMP_LO0");
	TEMP_LO0_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: TEMP_LO0 to %i\n", TEMP_LO0_m);
#endif
	break;
    case SET_TEMP_LO1:
	checkSize(data, 2, "Set TEMP_LO1");
	TEMP_LO1_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: TEMP_LO1 to %i\n", TEMP_LO1_m);
#endif
	break;
    case SET_TEMP_LO2:
	checkSize(data, 2, "Set TEMP_LO2");
	TEMP_LO2_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: TEMP_LO2 to %i\n", TEMP_LO2_m);
#endif
	break;
    case SET_TEMP_LO3:
	checkSize(data, 2, "Set TEMP_LO3");
	TEMP_LO3_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: TEMP_LO3 to %i\n", TEMP_LO3_m);
#endif
	break;
    case SET_TEMP_LO4:
	checkSize(data, 2, "Set TEMP_LO4");
	TEMP_LO4_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: TEMP_LO4 to %i\n", TEMP_LO4_m);
#endif
	break;
    case SET_TEMP_LO5:
	checkSize(data, 2, "Set TEMP_LO5");
	TEMP_LO5_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: TEMP_LO5 to %i\n", TEMP_LO5_m);
#endif
	break;
    case SET_TEMP_LO6:
	checkSize(data, 2, "Set TEMP_LO6");
	TEMP_LO6_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: TEMP_LO6 to %i\n", TEMP_LO6_m);
#endif
	break;
    case SET_TEMP_LO7:
	checkSize(data, 2, "Set TEMP_LO7");
	TEMP_LO7_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: TEMP_LO7 to %i\n", TEMP_LO7_m);
#endif
	break;
    case SET_PSUV_LO0:
	checkSize(data, 2, "Set PSUV_LO0");
	PSUV_LO0_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: PSUV_LO0 to %i\n", PSUV_LO0_m);
#endif
	break;
    case SET_PSUV_LO1:
	checkSize(data, 2, "Set PSUV_LO1");
	PSUV_LO1_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: PSUV_LO1 to %i\n", PSUV_LO1_m);
#endif
	break;
    case SET_PSUV_LO2:
	checkSize(data, 2, "Set PSUV_LO2");
	PSUV_LO2_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: PSUV_LO2 to %i\n", PSUV_LO2_m);
#endif
	break;
    case SET_PSUV_LO3:
	checkSize(data, 2, "Set PSUV_LO3");
	PSUV_LO3_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: PSUV_LO3 to %i\n", PSUV_LO3_m);
#endif
	break;
    case SET_PSUV_LO4:
	checkSize(data, 2, "Set PSUV_LO4");
	PSUV_LO4_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: PSUV_LO4 to %i\n", PSUV_LO4_m);
#endif
	break;
    case SET_PSUV_LO5:
	checkSize(data, 2, "Set PSUV_LO5");
	PSUV_LO5_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: PSUV_LO5 to %i\n", PSUV_LO5_m);
#endif
	break;
    case SET_PSUV_LO6:
	checkSize(data, 2, "Set PSUV_LO6");
	PSUV_LO6_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: PSUV_LO6 to %i\n", PSUV_LO6_m);
#endif
	break;
    case SET_PSUV_LO7:
	checkSize(data, 2, "Set PSUV_LO7");
	PSUV_LO7_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: PSUV_LO7 to %i\n", PSUV_LO7_m);
#endif
	break;
    case SET_GUNNV0_LO:
	checkSize(data, 2, "Set GUNNV0_LO");
	GUNNV0_LO_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: GUNNV0_LO to %i\n", GUNNV0_LO_m);
#endif
	break;
    case SET_IFPWR0_LO:
	checkSize(data, 2, "Set IFPWR0_LO");
	IFPWR0_LO_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: IFPWR0_LO to %i\n", IFPWR0_LO_m);
#endif
	break;
    case SET_GUNNV1_LO:
	checkSize(data, 2, "Set GUNNV1_LO");
	GUNNV1_LO_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: GUNNV1_LO to %i\n", GUNNV1_LO_m);
#endif
	break;
    case SET_IFPWR1_LO:
	checkSize(data, 2, "Set IFPWR1_LO");
	IFPWR1_LO_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: IFPWR1_LO to %i\n", IFPWR1_LO_m);
#endif
	break;
    case SET_TEMP_HI0:
	checkSize(data, 2, "Set TEMP_HI0");
	TEMP_HI0_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: TEMP_HI0 to %i\n", TEMP_HI0_m);
#endif
	break;
    case SET_TEMP_HI1:
	checkSize(data, 2, "Set TEMP_HI1");
	TEMP_HI1_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: TEMP_HI1 to %i\n", TEMP_HI1_m);
#endif
	break;
    case SET_TEMP_HI2:
	checkSize(data, 2, "Set TEMP_HI2");
	TEMP_HI2_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: TEMP_HI2 to %i\n", TEMP_HI2_m);
#endif
	break;
    case SET_TEMP_HI3:
	checkSize(data, 2, "Set TEMP_HI3");
	TEMP_HI3_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: TEMP_HI3 to %i\n", TEMP_HI3_m);
#endif
	break;
    case SET_TEMP_HI4:
	checkSize(data, 2, "Set TEMP_HI4");
	TEMP_HI4_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: TEMP_HI4 to %i\n", TEMP_HI4_m);
#endif
	break;
    case SET_TEMP_HI5:
	checkSize(data, 2, "Set TEMP_HI5");
	TEMP_HI5_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: TEMP_HI5 to %i\n", TEMP_HI5_m);
#endif
	break;
    case SET_TEMP_HI6:
	checkSize(data, 2, "Set TEMP_HI6");
	TEMP_HI6_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: TEMP_HI6 to %i\n", TEMP_HI6_m);
#endif
	break;
    case SET_TEMP_HI7:
	checkSize(data, 2, "Set TEMP_HI7");
	TEMP_HI7_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: TEMP_HI7 to %i\n", TEMP_HI7_m);
#endif
	break;
    case SET_PSUV_HI0:
	checkSize(data, 2, "Set PSUV_HI0");
	PSUV_HI0_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: PSUV_HI0 to %i\n", PSUV_HI0_m);
#endif
	break;
    case SET_PSUV_HI1:
	checkSize(data, 2, "Set PSUV_HI1");
	PSUV_HI1_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: PSUV_HI1 to %i\n", PSUV_HI1_m);
#endif
	break;
    case SET_PSUV_HI2:
	checkSize(data, 2, "Set PSUV_HI2");
	PSUV_HI2_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: PSUV_HI2 to %i\n", PSUV_HI2_m);
#endif
	break;
    case SET_PSUV_HI3:
	checkSize(data, 2, "Set PSUV_HI3");
	PSUV_HI3_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: PSUV_HI3 to %i\n", PSUV_HI3_m);
#endif
	break;
    case SET_PSUV_HI4:
	checkSize(data, 2, "Set PSUV_HI4");
	PSUV_HI4_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: PSUV_HI4 to %i\n", PSUV_HI4_m);
#endif
	break;
    case SET_PSUV_HI5:
	checkSize(data, 2, "Set PSUV_HI5");
	PSUV_HI5_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: PSUV_HI5 to %i\n", PSUV_HI5_m);
#endif
	break;
    case SET_PSUV_HI6:
	checkSize(data, 2, "Set PSUV_HI6");
	PSUV_HI6_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: PSUV_HI6 to %i\n", PSUV_HI6_m);
#endif
	break;
    case SET_PSUV_HI7:
	checkSize(data, 2, "Set PSUV_HI7");
	PSUV_HI7_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: PSUV_HI7 to %i\n", PSUV_HI7_m);
#endif
	break;
    case SET_GUNNV0_HI:
	checkSize(data, 2, "Set GUNNV0_HI");
	GUNNV0_HI_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: GUNNV0_HI to %i\n", GUNNV0_HI_m);
#endif
	break;
    case SET_IFPWR0_HI:
	checkSize(data, 2, "Set IFPWR0_HI");
	IFPWR0_HI_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: IFPWR0_HI to %i\n", IFPWR0_HI_m);
#endif
	break;
    case SET_GUNNV1_HI:
	checkSize(data, 2, "Set GUNNV1_HI");
	GUNNV1_HI_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: GUNNV1_HI to %i\n", GUNNV1_HI_m);
#endif
	break;
    case SET_IFPWR1_HI:
	checkSize(data, 2, "Set IFPWR1_HI");
	IFPWR1_HI_m = dataToShort(temp);
#ifdef debug
	printf("AMB::WVR::control: IFPWR1_HI to %i\n", IFPWR1_HI_m);
#endif
	break;
    case SET_ALM_DIS0:
	checkSize(data, 8, "Set WVR ALM_DIS0");
  	ALM_DIS0_m = dataToLonglong(temp, 8);
#ifdef debug
	printf("AMB::WVR::control: ALM_DIS0 to %llu\n", ALM_DIS0_m);
#endif
	break;
    case SET_ALM_DIS1:
	checkSize(data, 8, "Set WVR ALM_DIS1");
 	ALM_DIS1_m = dataToLonglong(temp, 8);
#ifdef debug
	printf("AMB::WVR::control: ALM_DIS1 to %llu\n", ALM_DIS1_m);
#endif
	break;
    case SET_ALM_DIS2:
	checkSize(data, 8, "Set WVR ALM_DIS2");
 	ALM_DIS2_m = dataToLonglong(temp, 8);
#ifdef debug
	printf("AMB::WVR::control: ALM_DIS2 to %llu\n", ALM_DIS2_m);
#endif
	break;
    case SET_ALM_DIS3:
	checkSize(data, 8, "Set WVR ALM_DIS3");
 	ALM_DIS3_m = dataToLonglong(temp, 8);
#ifdef debug
	printf("AMB::WVR::control: ALM_DIS3 to %llu\n", ALM_DIS3_m);
#endif
	break;
    case SET_ALM_QT0:
	checkSize(data, 8, "Set WVR ALM_QT0");
	ALM_QT0_m = dataToLonglong(temp, 8);
#ifdef debug
	printf("AMB::WVR::control: ALM_QT0 to %llu\n", ALM_QT0_m);
#endif
	break;
    case SET_ALM_QT1:
	checkSize(data, 8, "Set WVR ALM_QT1");
	ALM_QT1_m = dataToLonglong(temp, 8);
#ifdef debug
	printf("AMB::WVR::control: ALM_QT1 to %llu\n", ALM_QT1_m);
#endif
	break;
    case SET_ALM_QT2:
	checkSize(data, 8, "Set WVR ALM_QT2");
	ALM_QT2_m = dataToLonglong(temp, 8);
#ifdef debug
	printf("AMB::WVR::control: ALM_QT2 to %llu\n", ALM_QT2_m);
#endif
	break;
    case SET_ALM_QT3:
	checkSize(data, 8, "Set WVR ALM_QT3");
	ALM_QT3_m = dataToLonglong(temp, 8);
#ifdef debug
	printf("AMB::WVR::control: ALM_QT3 to %llu\n", ALM_QT3_m);
#endif
	break;
    case SET_ALM_CLR:
	checkSize(data, 1, "Set WVR ALM_CLR");
	tmpCh = data[0];
#ifdef debug
	printf("AMB::WVR::control: ALM_CLR to %i\n", tmpCh);
#endif
	break;
    case SET_CALG_LO:
	checkSize(data, 4, "Set CALG_LO");
	CALG_LO_m = dataToLong(temp);
#ifdef debug
	memcpy(&tmpVal, &CALG_LO_m, 4);
	printf("AMB::WVR::control: CALG_LO to %f\n", tmpVal);
#endif
	break;
    case SET_CALG_HI:
	checkSize(data, 4, "Set CALG_HI");
	CALG_HI_m = dataToLong(temp);
#ifdef debug
	memcpy(&tmpVal, &CALG_HI_m, 4);
	printf("AMB::WVR::control: CALG_HI to %f\n", tmpVal);
#endif
	break;
    case SET_CALTRX_LO:
	checkSize(data, 4, "Set CALTRX_LO");
	CALTRX_LO_m = dataToLong(temp);
#ifdef debug
	memcpy(&tmpVal, &CALTRX_LO_m, 4);
	printf("AMB::WVR::control: CALTRX_LO to %f\n", tmpVal);
#endif
	break;
    case SET_CALTRX_HI:
	checkSize(data, 4, "Set CALTRX_HI");
	CALTRX_HI_m = dataToLong(temp);
#ifdef debug
	memcpy(&tmpVal, &CALTRX_HI_m, 4);
	printf("AMB::WVR::control: CALTRX_HI to %f\n", tmpVal);
#endif
	break;
    case SET_TRX_SM0:
	checkSize(data, 4, "Set TRX_SM0");
	TRX_SM0_m = dataToLong(temp);
#ifdef debug
	memcpy(&tmpVal, &TRX_SM0_m, 4);
	printf("AMB::WVR::control: TRX_SM1 to %f\n", tmpVal);
#endif
	break;
    case SET_TRX_SM1:
	checkSize(data, 4, "Set TRX_SM1");
	TRX_SM1_m = dataToLong(temp);
#ifdef debug
	memcpy(&tmpVal, &TRX_SM1_m, 4);
	printf("AMB::WVR::control: TRX_SM1 to %f\n", tmpVal);
#endif
	break;
    case SET_T0_AMB0:
	checkSize(data, 4, "Set T0_AMB0");
	T0_AMB0_m = dataToLong(temp);
#ifdef debug
	memcpy(&tmpVal, &T0_AMB0_m, 4);
	printf("AMB::WVR::control: T0_AMB0 to %f\n", tmpVal);
#endif
	break;
    case SET_T0_AMB1:
	checkSize(data, 4, "Set T0_AMB1");
	T0_AMB1_m = dataToLong(temp);
#ifdef debug
	memcpy(&tmpVal, &T0_AMB1_m, 4);
	printf("AMB::WVR::control: T0_AMB1 to %f\n", tmpVal);
#endif
	break;
    case SET_T0_AMB2:
	checkSize(data, 4, "Set T0_AMB2");
	T0_AMB2_m = dataToLong(temp);
#ifdef debug
	memcpy(&tmpVal, &T0_AMB2_m, 4);
	printf("AMB::WVR::control: T0_AMB2 to %f\n", tmpVal);
#endif
	break;
    case SET_T0_AMB3:
	checkSize(data, 4, "Set T0_AMB3");
	T0_AMB3_m = dataToLong(temp);
#ifdef debug
	memcpy(&tmpVal, &T0_AMB3_m, 4);
	printf("AMB::WVR::control: T0_AMB3 to %f\n", tmpVal);
#endif
	break;
    case SET_T0_AMB4:
	checkSize(data, 4, "Set T0_AMB4");
	T0_AMB4_m = dataToLong(temp);
#ifdef debug
	memcpy(&tmpVal, &T0_AMB4_m, 4);
	printf("AMB::WVR::control: T0_AMB4 to %f\n", tmpVal);
#endif
	break;
    case SET_T0_AMB5:
	checkSize(data, 4, "Set T0_AMB5");
	T0_AMB5_m = dataToLong(temp);
#ifdef debug
	memcpy(&tmpVal, &T0_AMB5_m, 4);
	printf("AMB::WVR::control: T0_AMB5 to %f\n", tmpVal);
#endif
	break;
    case SET_T0_AMB6:
	checkSize(data, 4, "Set T0_AMB6");
	T0_AMB6_m = dataToLong(temp);
#ifdef debug
	memcpy(&tmpVal, &T0_AMB6_m, 4);
	printf("AMB::WVR::control: T0_AMB6 to %f\n", tmpVal);
#endif
	break;
    case SET_T0_AMB7:
	checkSize(data, 4, "Set T0_AMB7");
	T0_AMB7_m = dataToLong(temp);
#ifdef debug
	memcpy(&tmpVal, &T0_AMB7_m, 4);
	printf("AMB::WVR::control: T0_AMB7 to %f\n", tmpVal);
#endif
	break;
    case SET_T0_COLD0:
	checkSize(data, 4, "Set T0_COLD0");
	T0_COLD0_m = dataToLong(temp);
#ifdef debug
	memcpy(&tmpVal, &T0_COLD0_m, 4);
	printf("AMB::WVR::control: T0_COLD0 to %f\n", tmpVal);
#endif
	break;
    case SET_T0_COLD1:
	checkSize(data, 4, "Set T0_COLD1");
	T0_COLD1_m = dataToLong(temp);
#ifdef debug
	memcpy(&tmpVal, &T0_COLD1_m, 4);
	printf("AMB::WVR::control: T0_COLD1 to %f\n", tmpVal);
#endif
	break;
    case SET_T0_COLD2:
	checkSize(data, 4, "Set T0_COLD2");
	T0_COLD2_m = dataToLong(temp);
#ifdef debug
	memcpy(&tmpVal, &T0_COLD2_m, 4);
	printf("AMB::WVR::control: T0_COLD2 to %f\n", tmpVal);
#endif
	break;
    case SET_T0_COLD3:
	checkSize(data, 4, "Set T0_COLD3");
	T0_COLD3_m = dataToLong(temp);
#ifdef debug
	memcpy(&tmpVal, &T0_COLD3_m, 4);
	printf("AMB::WVR::control: T0_COLD3 to %f\n", tmpVal);
#endif
	break;
    case SET_T0_COLD4:
	checkSize(data, 4, "Set T0_COLD4");
	T0_COLD4_m = dataToLong(temp);
#ifdef debug
	memcpy(&tmpVal, &T0_COLD4_m, 4);
	printf("AMB::WVR::control: T0_COLD4 to %f\n", tmpVal);
#endif
	break;
    case SET_T0_COLD5:
	checkSize(data, 4, "Set T0_COLD5");
	T0_COLD5_m = dataToLong(temp);
#ifdef debug
	memcpy(&tmpVal, &T0_COLD5_m, 4);
	printf("AMB::WVR::control: T0_COLD5 to %f\n", tmpVal);
#endif
	break;
    case SET_T0_COLD6:
	checkSize(data, 4, "Set T0_COLD6");
	T0_COLD6_m = dataToLong(temp);
#ifdef debug
	memcpy(&tmpVal, &T0_COLD6_m, 4);
	printf("AMB::WVR::control: T0_COLD6 to %f\n", tmpVal);
#endif
	break;
    case SET_T0_COLD7:
	checkSize(data, 4, "Set T0_COLD7");
	T0_COLD7_m = dataToLong(temp);
#ifdef debug
	memcpy(&tmpVal, &T0_COLD7_m, 4);
	printf("AMB::WVR::control: T0_COLD7 to %f\n", tmpVal);
#endif
	break;
    default:
	throw WVRError("Unknown RCA in control command module WVR");
    }
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::WVR::CAN_WVR_STATE() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, STATE_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_INT_TIME() const{
  std::vector<CAN::byte_t> tmp;
  tmp.push_back(INT_TIME_m);
  return(tmp);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_BLK_TIME() const{
  std::vector<CAN::byte_t> tmp;
  tmp.push_back(BLK_TIME_m);
  return(tmp);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CYC_IN_RES() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, CYC_IN_RES_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CYC_IN_CAL() const{
  std::vector<CAN::byte_t> tmp;
  tmp.push_back(CYC_IN_CAL_m);
  return(tmp);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CAL_PER() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, CAL_PER_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_LO_STATE() const{
  std::vector<CAN::byte_t> tmp;
  tmp.push_back(LO_STATE_m);
  return(tmp);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PHASE_TIME() const{
  std::vector<CAN::byte_t> tmp;
  tmp.push_back(PHASE_TIME_m);
  return(tmp);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_IHL_DATA() const {
  const std::vector<CAN::byte_t> tmp;
  return longlongToData(tmp, IHL_DATA_m, 8);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_ICL_DATA() const {
  const std::vector<CAN::byte_t> tmp;
  return longlongToData(tmp, ICL_DATA_m, 3);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_HTR0_DATA() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, HTR0_DATA_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_HTR1_DATA() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, HTR1_DATA_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PELT_SENS() const{
  std::vector<CAN::byte_t> tmp;
  tmp.push_back(PELT_SENS_m);
  return(tmp);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PELT_THRESH() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, PELT_THRESH_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_DDS_DATA0() const {
  const std::vector<CAN::byte_t> tmp;
  return longlongToData(tmp, DDS_DATA0_m, 8);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_DDS_DATA1() const {
  const std::vector<CAN::byte_t> tmp;
  return longlongToData(tmp, DDS_DATA1_m, 8);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_DIAG_CONF() const {
  const std::vector<CAN::byte_t> tmp;
  return longlongToData(tmp, DIAG_CONF_m, 8);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_VANE_POS() const{
  std::vector<CAN::byte_t> tmp;
  tmp.push_back(VANE_POS_m);
  return(tmp);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_WVR_CONF() const {
  const std::vector<CAN::byte_t> tmp;
  return longlongToData(tmp, WVR_CONF_m, 8);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PLL_LOCK() const{
  std::vector<CAN::byte_t> tmp;
  tmp.push_back(67);
  return(tmp);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_YIG_LOCK() const{
  std::vector<CAN::byte_t> tmp;
  tmp.push_back(49);
  return(tmp);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_IF_PWROK() const{
  std::vector<CAN::byte_t> tmp;
  tmp.push_back(13);
  return(tmp);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_REF_OK() const{
  std::vector<CAN::byte_t> tmp;
  tmp.push_back(25);
  return(tmp);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CHOP_LOCK() const{
  std::vector<CAN::byte_t> tmp;
  tmp.push_back(31);
  return(tmp);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CHOP_SENS() const{
  std::vector<CAN::byte_t> tmp;
  tmp.push_back(42);
  return(tmp);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CHOP_STATE() const{
  std::vector<CAN::byte_t> tmp;
  tmp.push_back(36);
  return(tmp);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_VANE_TMOUT() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, VANE_TMOUT_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CHOP_TMOUT() const{
  std::vector<CAN::byte_t> tmp;
  tmp.push_back(CHOP_TMOUT_m);
  return(tmp);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_HTR0_STATE() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 12345);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_HTR1_STATE() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 23456);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_VANE_STATE() const{
  std::vector<CAN::byte_t> tmp;
  tmp.push_back(123);
  return(tmp);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TEMP_LO0() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, TEMP_LO0_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TEMP_LO1() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, TEMP_LO1_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TEMP_LO2() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, TEMP_LO2_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TEMP_LO3() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, TEMP_LO3_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TEMP_LO4() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, TEMP_LO4_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TEMP_LO5() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, TEMP_LO5_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TEMP_LO6() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, TEMP_LO6_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TEMP_LO7() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, TEMP_LO7_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PSUV_LO0() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, PSUV_LO0_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PSUV_LO1() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, PSUV_LO1_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PSUV_LO2() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, PSUV_LO2_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PSUV_LO3() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, PSUV_LO3_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PSUV_LO4() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, PSUV_LO4_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PSUV_LO5() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, PSUV_LO5_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PSUV_LO6() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, PSUV_LO6_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PSUV_LO7() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, PSUV_LO7_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_GUNNV0_LO() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, GUNNV0_LO_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_IFPWR0_LO() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, IFPWR0_LO_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_GUNNV1_LO() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, GUNNV1_LO_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_IFPWR1_LO() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, IFPWR1_LO_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TEMP_HI0() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, TEMP_HI0_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TEMP_HI1() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, TEMP_HI1_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TEMP_HI2() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, TEMP_HI2_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TEMP_HI3() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, TEMP_HI3_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TEMP_HI4() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, TEMP_HI4_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TEMP_HI5() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, TEMP_HI5_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TEMP_HI6() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, TEMP_HI6_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TEMP_HI7() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, TEMP_HI7_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PSUV_HI0() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, PSUV_HI0_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PSUV_HI1() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, PSUV_HI1_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PSUV_HI2() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, PSUV_HI2_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PSUV_HI3() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, PSUV_HI3_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PSUV_HI4() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, PSUV_HI4_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PSUV_HI5() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, PSUV_HI5_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PSUV_HI6() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, PSUV_HI6_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PSUV_HI7() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, PSUV_HI7_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_GUNNV0_HI() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, GUNNV0_HI_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_IFPWR0_HI() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, IFPWR0_HI_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_GUNNV1_HI() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, GUNNV1_HI_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_IFPWR1_HI() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, IFPWR1_HI_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_ICL_STATE() const {
  const std::vector<CAN::byte_t> tmp;
  return longlongToData(tmp, 0x1234134514561567LLU, 8);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TEMP0() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 11);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TEMP1() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 22);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TEMP2() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 33);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TEMP3() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 404);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TEMP4() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 550);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TEMP5() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 66);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TEMP6() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 707);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TEMP7() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 780);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PSUV0() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 101);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PSUV1() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 202);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PSUV2() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 303);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PSUV3() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 404);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PSUV4() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 505);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PSUV5() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 606);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PSUV6() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 780);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_PSUV7() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 790);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_GUNNV0() const{
  std::vector<CAN::byte_t> tmp;
  float tmp1 = (6.75/12.81)*4095;
  short tmp2 = (short)roundf(tmp1);
  return shortToData(tmp, tmp2);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_IFPWR0() const{
  std::vector<CAN::byte_t> tmp;
  float tmp1 = ((3.28+5.01)/10.01)*4095;
  short tmp2 = (short)roundf(tmp1);
  return shortToData(tmp, tmp2);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_GUNNV1() const{
  std::vector<CAN::byte_t> tmp;
  float tmp1 = (9.62/12.81)*4095;
  short tmp2 = (short)roundf(tmp1);
  return shortToData(tmp, tmp2);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_IFPWR1() const{
  std::vector<CAN::byte_t> tmp;
  float tmp1 = ((-3.28+5.01)/10.01)*4095;
  short tmp2 = (short)roundf(tmp1);
  return shortToData(tmp, tmp2);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_ALM_DIS0() const {
  const std::vector<CAN::byte_t> tmp;
  return longlongToData(tmp, ALM_DIS0_m, 8);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_ALM_DIS1() const {
  const std::vector<CAN::byte_t> tmp;
  return longlongToData(tmp, ALM_DIS1_m, 8);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_ALM_DIS2() const {
  const std::vector<CAN::byte_t> tmp;
  return longlongToData(tmp, ALM_DIS2_m, 8);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_ALM_DIS3() const {
  const std::vector<CAN::byte_t> tmp;
  return longlongToData(tmp, ALM_DIS3_m, 8);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_ALM_QT0() const {
  const std::vector<CAN::byte_t> tmp;
  return longlongToData(tmp, ALM_QT0_m, 8);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_ALM_QT1() const {
  const std::vector<CAN::byte_t> tmp;
  return longlongToData(tmp, ALM_QT1_m, 8);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_ALM_QT2() const {
  const std::vector<CAN::byte_t> tmp;
  return longlongToData(tmp, ALM_QT2_m, 8);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_ALM_QT3() const {
  const std::vector<CAN::byte_t> tmp;
  return longlongToData(tmp, ALM_QT3_m, 8);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_ALM_STATE0() const {
  const std::vector<CAN::byte_t> tmp;
  return longlongToData(tmp, 0x12345678901234LLU, 8);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_ALM_STATE1() const {
  const std::vector<CAN::byte_t> tmp;
  return longlongToData(tmp, 0x23456789012345LLU, 8);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_ALM_STATE2() const {
  const std::vector<CAN::byte_t> tmp;
  return longlongToData(tmp, 0x34567890123456LLU, 8);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_ALM_STATE3() const {
  const std::vector<CAN::byte_t> tmp;
  return longlongToData(tmp, 0x45678901234567LLU, 8);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_RAW_TSKY0() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 11);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_RAW_TSKY1() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 22);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_RAW_TSKY2() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 303);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_RAW_TSKY3() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 444);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_RAW_TSKY4() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 550);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_RAW_TSKY5() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 606);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_RAW_TSKY6() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 7707);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_RAW_TSKY7() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 88);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TSKY0() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 10);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TSKY1() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 202);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TSKY2() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 33);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TSKY3() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 404);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TSKY4() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 5005);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TSKY5() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 6666);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TSKY6() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 77);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TSKY7() const{
  std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, 567);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALG_LO() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, CALG_LO_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALG_HI() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, CALG_HI_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALTRX_LO() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, CALTRX_LO_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALTRX_HI() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, CALTRX_HI_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TRX_SM0() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, TRX_SM0_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TRX_SM1() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, TRX_SM1_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_TSKYSTAMP() const {
  const std::vector<CAN::byte_t> tmp;
  return longlongToData(tmp, 23456ULL, 3);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALG0() const{
  return floatToData(0.1234);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALG1() const{
  return floatToData(1.2345);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALG2() const{
  return floatToData(2.3456);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALG3() const{
  return floatToData(3.4567);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALG4() const{
  return floatToData(4.5678);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALG5() const{
  return floatToData(5.6789);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALG6() const{
  return floatToData(6.7890);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALG7() const{
  return floatToData(7.8901);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALG8() const{
  return floatToData(8.9012);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALG9() const{
  return floatToData(9.0123);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALG10() const{
  return floatToData(10.123);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALG11() const{
  return floatToData(11.234);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALG12() const{
  return floatToData(12.345);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALG13() const{
  return floatToData(13.456);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALG14() const{
  return floatToData(14.567);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALG15() const{
  return floatToData(15.678);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALTRX0() const{
  return floatToData(0.2345);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALTRX1() const{
  return floatToData(1.3456);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALTRX2() const{
  return floatToData(2.4567);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALTRX3() const{
  return floatToData(3.5678);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALTRX4() const{
  return floatToData(4.6789);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALTRX5() const{
  return floatToData(5.7890);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALTRX6() const{
  return floatToData(6.8901);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALTRX7() const{
  return floatToData(7.9012);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALTRX8() const{
  return floatToData(8.0123);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALTRX9() const{
  return floatToData(9.1234);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALTRX10() const{
  return floatToData(10.234);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALTRX11() const{
  return floatToData(11.345);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALTRX12() const{
  return floatToData(12.456);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALTRX13() const{
  return floatToData(13.567);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALTRX14() const{
  return floatToData(14.678);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_CALTRX15() const{
  return floatToData(15.789);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_T0_AMB0() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, T0_AMB0_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_T0_AMB1() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, T0_AMB1_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_T0_AMB2() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, T0_AMB2_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_T0_AMB3() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, T0_AMB3_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_T0_AMB4() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, T0_AMB4_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_T0_AMB5() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, T0_AMB5_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_T0_AMB6() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, T0_AMB6_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_T0_AMB7() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, T0_AMB7_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_T0_COLD0() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, T0_COLD0_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_T0_COLD1() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, T0_COLD1_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_T0_COLD2() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, T0_COLD2_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_T0_COLD3() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, T0_COLD3_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_T0_COLD4() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, T0_COLD4_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_T0_COLD5() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, T0_COLD5_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_T0_COLD6() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, T0_COLD6_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_T0_COLD7() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, T0_COLD7_m);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_RAW_AD0() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, 0x12345678);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_RAW_AD1() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, 0x23456789);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_RAW_AD2() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, 0x3456789a);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_RAW_AD3() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, 0x456789ab);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_RAW_AD4() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, 0x56789abc);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_RAW_AD5() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, 0x6789abcd);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_RAW_AD6() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, 0x789abcde);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_RAW_AD7() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, 0x89abcdef);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_RAW_AD8() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, 0x9abcdef0);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_RAW_AD9() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, 0xabcdef01);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_RAW_AD10() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, 0xbcdef012);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_RAW_AD11() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, 0xcdef0123);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_RAW_AD12() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, 0xdef01234);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_RAW_AD13() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, 0xef012345);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_RAW_AD14() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, 0xf0123456);
}

std::vector<CAN::byte_t> AMB::WVR::CAN_RAW_AD15() const{
  std::vector<CAN::byte_t> tmp;
  return longToData(tmp, 0x01234567);
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::WVR::get_can_error() const {
  return AMB::Device::get_can_error();
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::WVR::get_protocol_rev_level() const {
  return AMB::Device::get_protocol_rev_level();
}

// ----------------------------------------------------------------------------
unsigned int AMB::WVR::get_trans_num() const {
  return m_trans_num;
}

// ----------------------------------------------------------------------------
double AMB::WVR::get_ambient_temperature() const {
  return 22.4;
}

// ----------------------------------------------------------------------------
void AMB::WVR::checkSize(const std::vector<CAN::byte_t> &data, int size,
			  const char *pExceptionMsg) {
  if( static_cast<int>(data.size()) != size ) {
    char exceptionMsg[200];
    sprintf(exceptionMsg,"%s data length is %d must be %d",pExceptionMsg,
	    data.size(), size);
    throw WVRError(exceptionMsg);
  }
}

/// Get functions for unit testing
// ----------------------------------------------------------------------------













