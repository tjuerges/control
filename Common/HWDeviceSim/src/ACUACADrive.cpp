// @(#) $Id$


#include <unistd.h>
#include "ACUACADrive.h"

using namespace ACAacu;

//------------------------------------------------------------------------------

ACUDrive::ACUDrive() :
    active_m(false)
{
    Az.debug_level = 0;
    El.debug_level = 0;

    Az.limits(AZ_LOWER_LIMIT,AZ_UPPER_LIMIT);
    El.limits(EL_LOWER_LIMIT,EL_UPPER_LIMIT);

    Az.stows(AZ_MAINT_STOW,AZ_SURVL_STOW);
    El.stows(EL_MAINT_STOW,EL_SURVL_STOW);

    // Start thread to read CAN messages and stick them in queue.
    active_m = true;
    if(pthread_create(&tid,
		      NULL,
		      &ACUDrive::updateLoop,
		      static_cast<void*>(this))!=0)
	{
	active_m = false;
	}
    //pthread_create(&tid,NULL,&ACUDrive::updateLoop,(void*)this);
}

//------------------------------------------------------------------------------

ACUDrive::~ACUDrive() {     
  if(active_m)
    {
      active_m = false;
      pthread_join(tid, NULL); 
    }
}

//------------------------------------------------------------------------------

int ACUDrive::setAzPsn(double position,double velocity)
{
    return Az.set(position,velocity);
}

//------------------------------------------------------------------------------

int ACUDrive::setElPsn(double position,double velocity)
{
    return El.set(position,velocity);
}

//------------------------------------------------------------------------------

void ACUDrive::getAzPsn(double& position,double& velocity) const
{
    double accel;
    Az.get_cur(position,velocity,accel);
}

//------------------------------------------------------------------------------

void ACUDrive::getElPsn(double& position,double& velocity) const
{
    double accel;
    El.get_cur(position,velocity,accel);
}

//------------------------------------------------------------------------------

std::vector<uint8_t> ACUDrive::getAzStatus() const
{
    std::vector<uint8_t> status(8);

    status[0] = Az.limit;
    status[1] = 0;
    status[2] = 0;
    status[3] = 0;
    status[4] = 0;
    status[5] = 0;
    status[6] = 0;
    status[7] = 0;
    return status;
}
std::vector<uint8_t> ACUDrive::getAzStatus2() const
{
    std::vector<uint8_t> status(8);

    status[0] = 0;
    status[1] = 0;
    status[2] = 0;
    status[3] = 0;
    status[4] = 0;
    status[5] = 0;
    status[6] = 0;
    status[7] = 0;
    return status;
}

//------------------------------------------------------------------------------

std::vector<uint8_t> ACUDrive::getElStatus() const
{
    std::vector<uint8_t> status(8);

    status[0] = El.limit;
    status[1] = 0;
    status[2] = 0;
    status[3] = 0;
    status[4] = 0;
    status[5] = 0;
    status[6] = 0;
    status[7] = 0;
    return status;
}
std::vector<uint8_t> ACUDrive::getElStatus2() const
{
    std::vector<uint8_t> status(8);

    status[0] = 0;
    status[1] = 0;
    status[2] = 0;
    status[3] = 0;
    status[4] = 0;
    status[5] = 0;
    status[6] = 0;
    status[7] = 0;
    return status;
}

//------------------------------------------------------------------------------

std::vector<uint8_t> ACUDrive::getACUMode() const
{
    std::vector<uint8_t> mode(2);
    mode[0] = ((El.get_mode() & 0xf) << 4) | (Az.get_mode() & 0xf);
    mode[1] = ACU_REMOTE;  // always remote
    return mode;
}

//------------------------------------------------------------------------------

int ACUDrive::setACUMode(uint8_t mode)
{
    int az_mode = mode & 0xf;
    int el_mode = (mode >> 4) & 0xf;

    int error = 0;
    error =  Az.set_mode((AZEL_Mode_t)(az_mode));
    error |= El.set_mode((AZEL_Mode_t)(el_mode));
    return error;	// 0-OK, 1-ERROR
}

//------------------------------------------------------------------------------

void ACUDrive::handler()
{
    Az.tick();
    El.tick();
}

//------------------------------------------------------------------------------

// This function is static to be the pthread_create() argument.  A pointer to 
// the class object is passed to allow access to its variables and methods().
void* ACUDrive::updateLoop(void* data)
{
    ACUDrive* This = static_cast<ACUDrive*>(data);
    while(This->active_m)
	{
	usleep((int)(1000000 * TIME_UNIT));
	This->handler();
	}
    pthread_exit(NULL);
}

//------------------------------------------------------------------------------
