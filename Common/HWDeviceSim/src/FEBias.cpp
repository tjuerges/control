// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include <iomanip>
#include "FEBias.h"
#include "AMBUtil.h"

int AMB::FEBias::STAGES_PER_LNA[AMB::FEBias::NUMBER_OF_LNAS] =
    { AMB::FEBias::STAGES_FOR_B3,
      AMB::FEBias::STAGES_FOR_B3,
      AMB::FEBias::STAGES_FOR_B6,
      AMB::FEBias::STAGES_FOR_B6 };

// -----------------------------------------------------------------------------

AMB::FEBias::FEBias(
	node_t node,
	std::vector<CAN::byte_t> &serialNumber)
    : m_Node(node), m_SN(serialNumber), m_TransNum(0) 
{
    std::cout << "Creating node " << node << ", s/n 0x";
    for (int i = 0; i < 8; i++)
	std::cout << std::hex << std::setw(2) << std::setfill('0') << (int)serialNumber[i];
    std::cout << ", FEBias device" << std::endl;

    for (int i = 0; i < AMB::FEBias::NUMBER_OF_LNAS; i++)
    {
	m_LNAStates[i] = 0;
    }

    for (int i = 0; i < AMB::FEBias::NUMBER_OF_LNA_STAGES; i++)
    {
	m_LNADrainVoltages[i] = 0.0;
	m_LNADrainCurrents[i] = 0.0;
	m_LNAGateVoltages[i] = 0.0;
    }

    for (int i = 0; i < AMB::FEBias::CHANNELS; i++)
    {
	m_SchottkyPosCurrents[i] = 0.0;
	m_SchottkyNegCurrents[i] = 0.0;
	m_SchottkyBiasVoltages[i] = 0.0;
	m_SISJunctionVoltages[i] = 0.0;
	m_SISJunctionCurrents[i] = 0.0;
	m_SISBiasVoltages[i] = 0.0;
	m_SIS0BiasVoltages[i] = 0.0;
	m_SIS0BiasCurrents[i] = 0.0;
	m_SIS0BiasBiasVoltages[i] = 0.0;
	m_SISMagnetCurrents[i] = 0.0;
    }

   // TODO - validate node & sn
}

// -----------------------------------------------------------------------------

AMB::FEBias::~FEBias()
{
    // Nothing
}

// -----------------------------------------------------------------------------

AMB::node_t AMB::FEBias::node() const
{
    return m_Node;
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::FEBias::serialNumber() const
{
    return m_SN;
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::FEBias::monitor(rca_t rca) const
{
    m_TransNum++;
    const std::vector<CAN::byte_t> tmp;
    Monitor_t monitorRCA = static_cast<Monitor_t>(rca);

    switch (monitorRCA) 
    {
    // Mandatory generic points
    case GET_CAN_ERROR:
    {
	return get_can_error();
	break;
    }
    case GET_PROTOCOL_REV_LEVEL:
    {
	return get_protocol_rev_level();
	break;
    }

	case AMB::Device::GET_SW_REV_LEVEL:
		return get_software_rev_level();
		break;

	case AMB::Device::GET_HW_REV_LEVEL:
		return get_hardware_rev_level();
		break;

    case GET_TRANS_NUM:
    {
	return longToData(tmp, get_trans_num());
	break;
    }
    // Optional generic point
    case GET_AMBIENT_TEMPERATURE:
    {
	return floatDS1820ToData(tmp, get_ambient_temperature());
	break;
    }

    // FEBias specific points
    case GET_B3_LNA1_STATE:
    case GET_B3_LNA2_STATE:
    case GET_B6_LNA1_STATE:
    case GET_B6_LNA2_STATE:
    {
	return charToData(tmp, getLNAState(monitorRCA));
	break;
    }
    case GET_B3_LNA1_S1_DRAIN_V:
    case GET_B3_LNA1_S2_DRAIN_V:
    case GET_B3_LNA1_S3_DRAIN_V:
    case GET_B3_LNA1_S4_DRAIN_V:
    case GET_B3_LNA1_S5_DRAIN_V:
    case GET_B3_LNA2_S1_DRAIN_V:
    case GET_B3_LNA2_S2_DRAIN_V:
    case GET_B3_LNA2_S3_DRAIN_V:
    case GET_B3_LNA2_S4_DRAIN_V:
    case GET_B3_LNA2_S5_DRAIN_V:
    case GET_B6_LNA1_S1_DRAIN_V:
    case GET_B6_LNA1_S2_DRAIN_V:
    case GET_B6_LNA1_S3_DRAIN_V:
    case GET_B6_LNA2_S1_DRAIN_V:
    case GET_B6_LNA2_S2_DRAIN_V:
    case GET_B6_LNA2_S3_DRAIN_V:
    {
	return getLNADrainVoltage(monitorRCA);
	break;
    }
    case GET_B3_LNA1_S1_DRAIN_C:
    case GET_B3_LNA1_S2_DRAIN_C:
    case GET_B3_LNA1_S3_DRAIN_C:
    case GET_B3_LNA1_S4_DRAIN_C:
    case GET_B3_LNA1_S5_DRAIN_C:
    case GET_B3_LNA2_S1_DRAIN_C:
    case GET_B3_LNA2_S2_DRAIN_C:
    case GET_B3_LNA2_S3_DRAIN_C:
    case GET_B3_LNA2_S4_DRAIN_C:
    case GET_B3_LNA2_S5_DRAIN_C:
    case GET_B6_LNA1_S1_DRAIN_C:
    case GET_B6_LNA1_S2_DRAIN_C:
    case GET_B6_LNA1_S3_DRAIN_C:
    case GET_B6_LNA2_S1_DRAIN_C:
    case GET_B6_LNA2_S2_DRAIN_C:
    case GET_B6_LNA2_S3_DRAIN_C:
    {
	return getLNADrainCurrent(monitorRCA);
	break;
    }
    case GET_B3_LNA1_S1_GATE_V:
    case GET_B3_LNA1_S2_GATE_V:
    case GET_B3_LNA1_S3_GATE_V:
    case GET_B3_LNA1_S4_GATE_V:
    case GET_B3_LNA1_S5_GATE_V:
    case GET_B3_LNA2_S1_GATE_V:
    case GET_B3_LNA2_S2_GATE_V:
    case GET_B3_LNA2_S3_GATE_V:
    case GET_B3_LNA2_S4_GATE_V:
    case GET_B3_LNA2_S5_GATE_V:
    case GET_B6_LNA1_S1_GATE_V:
    case GET_B6_LNA1_S2_GATE_V:
    case GET_B6_LNA1_S3_GATE_V:
    case GET_B6_LNA2_S1_GATE_V:
    case GET_B6_LNA2_S2_GATE_V:
    case GET_B6_LNA2_S3_GATE_V:
    {
	return getLNAGateVoltage(monitorRCA);
	break;
    }
    case GET_B3_SCHOTTKY1_PC :
    case GET_B3_SCHOTTKY2_PC:
    {
	return getSchottkyPosCurrent(monitorRCA);
	break;
    }
    case GET_B3_SCHOTTKY1_NC :
    case GET_B3_SCHOTTKY2_NC:
    {
	return getSchottkyNegCurrent(monitorRCA);
	break;
    }
    case GET_B6_SIS1_VJ:
    case GET_B6_SIS2_VJ:
    {
	return getSISVoltage(monitorRCA);
	break;
    }
    case GET_B6_SIS1_IJ:
    case GET_B6_SIS2_IJ:
    {
	return getSISCurrent(monitorRCA);
	break;
    }
    case GET_B6_MAGNET1:
    case GET_B6_MAGNET2:
    {
	return getMagnetCurrent(monitorRCA);
	break;
    }
    case GET_INTERFACE_STATUS:
	return getInterfaceStatus();
	break;
    default:
	throw FEBiasError("Unknown RCA in monitor command");
    }
}

// -----------------------------------------------------------------------------

void AMB::FEBias::control(rca_t rca, const std::vector<CAN::byte_t> &data)
{
    m_TransNum++;
    Controls_t controlRCA = static_cast<Controls_t>(rca);

    switch (controlRCA) 
    {
    case SET_B3_LNA1_STATE:
    case SET_B3_LNA2_STATE:
    case SET_B6_LNA1_STATE:
    case SET_B6_LNA2_STATE:
    {
	if (data.size() != 1) 
	{
	    throw FEBiasError("data length must be 1");
	}
	setLNAState(controlRCA, data[0]);
	break;
    }
    case SET_B3_LNA1_S1_DRAIN_V:
    case SET_B3_LNA1_S2_DRAIN_V:
    case SET_B3_LNA1_S3_DRAIN_V:
    case SET_B3_LNA1_S4_DRAIN_V:
    case SET_B3_LNA1_S5_DRAIN_V:
    case SET_B3_LNA2_S1_DRAIN_V:
    case SET_B3_LNA2_S2_DRAIN_V:
    case SET_B3_LNA2_S3_DRAIN_V:
    case SET_B3_LNA2_S4_DRAIN_V:
    case SET_B3_LNA2_S5_DRAIN_V:
    case SET_B6_LNA1_S1_DRAIN_V:
    case SET_B6_LNA1_S2_DRAIN_V:
    case SET_B6_LNA1_S3_DRAIN_V:
    case SET_B6_LNA2_S1_DRAIN_V:
    case SET_B6_LNA2_S2_DRAIN_V:
    case SET_B6_LNA2_S3_DRAIN_V:
    {
	float value = 0.0;
	if (validFloat(data, value))
	{
	    setLNADrainVoltage(controlRCA, value);
	}
	break;
    }
    case SET_B3_LNA1_S1_DRAIN_C:
    case SET_B3_LNA1_S2_DRAIN_C:
    case SET_B3_LNA1_S3_DRAIN_C:
    case SET_B3_LNA1_S4_DRAIN_C:
    case SET_B3_LNA1_S5_DRAIN_C:
    case SET_B3_LNA2_S1_DRAIN_C:
    case SET_B3_LNA2_S2_DRAIN_C:
    case SET_B3_LNA2_S3_DRAIN_C:
    case SET_B3_LNA2_S4_DRAIN_C:
    case SET_B3_LNA2_S5_DRAIN_C:
    case SET_B6_LNA1_S1_DRAIN_C:
    case SET_B6_LNA1_S2_DRAIN_C:
    case SET_B6_LNA1_S3_DRAIN_C:
    case SET_B6_LNA2_S1_DRAIN_C:
    case SET_B6_LNA2_S2_DRAIN_C:
    case SET_B6_LNA2_S3_DRAIN_C:
    {
	float value = 0.0;
	if (validFloat(data, value))
	{
	    setLNADrainCurrent(controlRCA, value);
	}
	break;
    }
    case SET_B3_SCHOTTKY1:
    case SET_B3_SCHOTTKY2:
    {
	float value = 0.0;
	if (validFloat(data, value))
	{
	    setSchottkyBiasVoltage(controlRCA, value);
	}
	break;
    }
    case SET_B6_SIS1:
    case SET_B6_SIS2:
    {
	float value = 0.0;
	if (validFloat(data, value))
	{
	    setSISBiasVoltage(controlRCA, value);
	}
	break;
    }
    case SET_B6_SIS1_BIAS_CAL_VJ:
    case SET_B6_SIS2_BIAS_CAL_VJ:
    {
	float value = 0.0;
	if (validFloat(data, value))
	{
	    setSISVoltageOffset(controlRCA, value);
	}
	break;
    }
    case SET_B6_SIS1_BIAS_CAL_IJ:
    case SET_B6_SIS2_BIAS_CAL_IJ:
    {
	float value = 0.0;
	if (validFloat(data, value))
	{
	    setSISCurrentOffset(controlRCA, value);
	}
	break;
    }
    case SET_B6_SIS1_BIAS_CAL_VB:
    case SET_B6_SIS2_BIAS_CAL_VB:
    {
	float value = 0.0;
	if (validFloat(data, value))
	{
	    setSISBiasVoltageOffset(controlRCA, value);
	}
	break;
    }
    case SET_B6_MAGNET1:
    case SET_B6_MAGNET2:
    {
	float value = 0.0;
	if (validFloat(data, value))
	{
	    setMagnetCurrent(controlRCA, value);
	}
	break;
    }
    default:
	throw FEBiasError("Unknown RCA in control command");
    }
}

// -----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::FEBias::get_can_error() const
{
    return AMB::Device::get_can_error();
}

// -----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::FEBias::get_protocol_rev_level() const
{
    return AMB::Device::get_protocol_rev_level();
}

// -----------------------------------------------------------------------------
unsigned int AMB::FEBias::get_trans_num() const
{
    return m_TransNum;
}

// -----------------------------------------------------------------------------
float AMB::FEBias::get_ambient_temperature() const
{
    return 18.0;
}

// -----------------------------------------------------------------------------
// FEBias specific monitors.  All monitors are specified by RCAs.
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
/// Return the specified current LNA state (1=enabled, 0=disabled).
CAN::byte_t AMB::FEBias::getLNAState(Monitor_t state) const
{
    return m_LNAStates[stateRCAToIndex(state)];
}

// -----------------------------------------------------------------------------
/// Return the specified LNA drain voltage
std::vector<CAN::byte_t> AMB::FEBias::getLNADrainVoltage(
    Monitor_t LNADrainVoltage) const
{
    return IEEEFloatToData(m_LNADrainVoltages[LNAMonitorRCAToIndex(
	LNADrainVoltage)]);
}

// -----------------------------------------------------------------------------
/// Return the specified LNA drain current 
std::vector<CAN::byte_t> AMB::FEBias::getLNADrainCurrent(
    Monitor_t LNADrainCurrent) const
{
    return IEEEFloatToData(m_LNADrainCurrents[LNAMonitorRCAToIndex(
	LNADrainCurrent)]);
}

// -----------------------------------------------------------------------------
/// Return the specified LNA gate voltage
std::vector<CAN::byte_t> AMB::FEBias::getLNAGateVoltage(
    Monitor_t LNAGateVoltage) const
{
    return IEEEFloatToData(m_LNAGateVoltages[LNAMonitorRCAToIndex(
	LNAGateVoltage)]);
}

// -----------------------------------------------------------------------------
/// Return the specified Schottky mixer positive junction current
std::vector<CAN::byte_t> AMB::FEBias::getSchottkyPosCurrent(
    Monitor_t schottkyPosCurrent) const
{
    return IEEEFloatToData(m_SchottkyPosCurrents[schottkyMonitorRCAToIndex(
	schottkyPosCurrent)]);
}

// -----------------------------------------------------------------------------
/// Return the specified Schottky mixer negative junction current
std::vector<CAN::byte_t> AMB::FEBias::getSchottkyNegCurrent(
    Monitor_t schottkyNegCurrent) const
{
    return IEEEFloatToData(m_SchottkyNegCurrents[schottkyMonitorRCAToIndex(
	schottkyNegCurrent)]);
}

// -----------------------------------------------------------------------------
/// Return the specified SIS mixer junction voltage with zero-bias error corrections
std::vector<CAN::byte_t> AMB::FEBias::getSISVoltage(
    Monitor_t SISVoltage) const
{
    int SISMixerIndex = SISMixerMonitorRCAToIndex(SISVoltage);
    float value = m_SISJunctionVoltages[SISMixerIndex]
	- m_SIS0BiasVoltages[SISMixerIndex];
    return IEEEFloatToData(value);
}

// -----------------------------------------------------------------------------
/// Return specified SIS mixer junction current with zero-bias error corrections
std::vector<CAN::byte_t> AMB::FEBias::getSISCurrent(
    Monitor_t SISCurrent) const
{
    int SISMixerIndex = SISMixerMonitorRCAToIndex(SISCurrent);
    float value =  m_SISJunctionCurrents[SISMixerIndex]
	- m_SIS0BiasCurrents[SISMixerIndex];
    return IEEEFloatToData(value);
}

// -----------------------------------------------------------------------------
/// Return specified SIS magnet coil current
std::vector<CAN::byte_t> AMB::FEBias::getMagnetCurrent(
    Monitor_t magnetCurrent) const
{
    return IEEEFloatToData(m_SISMagnetCurrents[SISMagnetMonitorRCAToIndex(
	magnetCurrent)]);
}

// -----------------------------------------------------------------------------
/// Return interface status
std::vector<CAN::byte_t> AMB::FEBias::getInterfaceStatus() const
{
    std::vector<CAN::byte_t> tmp(8);
    tmp[0] = 'E';
    tmp[1] = 'r';
    tmp[2] = 'r';
    tmp[3] = 'N';
    tmp[4] = 'o';
    tmp[5] = 'E';
    tmp[6] = 'r';
    tmp[7] = 0;
    return tmp;
}

// -----------------------------------------------------------------------------
// FEBias specific controls.  All control points are specified by RCAs. 
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
/// Set the specified LNA state to the passed-in value
/// If value = 0 (disabled) the drain voltage, drain current and gate voltage
/// are all set to 0 for all stages of the specified LNA.  For this simulator,
/// if value = 1 (enabled), drain voltage is arbitrarily set to 2 V, drain 
/// current to 5 mA, and gate voltage to 500 mV.   
void AMB::FEBias::setLNAState(Controls_t LNAState, CAN::byte_t value)
{
    if (value > 1) 
    {
	throw FEBiasError("setLNAState illegal value, must be in [0..1]");
    }
    int LNAIndex = stateControlRCAToIndex(LNAState);
    int numberOfStages = STAGES_PER_LNA[LNAIndex];
    int monitorStartIndex = LNAStateToLNAMonitor(LNAIndex);
    int monitorStopIndex = monitorStartIndex + numberOfStages;
    m_LNAStates[LNAIndex] = value;
    if (value == 0) // disabled
    {
	// Set all monitors for this LNA to 0
	for (int i = monitorStartIndex; i < monitorStopIndex; i++)
	{
	    m_LNADrainVoltages[i] = 0.0;
	    m_LNADrainCurrents[i] = 0.0;
	    m_LNAGateVoltages[i] = 0.0;
	}
    }
    else // enabled
    {
	// Set all monitors for this LNA to 0
	for (int i = monitorStartIndex; i < monitorStopIndex; i++)
	{
	    m_LNADrainVoltages[i] = LNAEnabledDrainVoltage;
	    m_LNADrainCurrents[i] = LNAEnabledDrainCurrent;
	    m_LNAGateVoltages[i] = LNAEnabledGateVoltage;
	}
    }
}

// -----------------------------------------------------------------------------
/// Set the specified LNA drain voltage to the passed-in value (0 - 3 V)
void AMB::FEBias::setLNADrainVoltage(Controls_t LNADrainVoltage,float value )
{
    if ((value < 0.0 ) || (value > 3.0))
    {
	throw FEBiasError(
	    "setLNADrainVoltage illegal value, must be 0.0 to 3.0");
    }
    m_LNADrainVoltages[LNAControlRCAToIndex(LNADrainVoltage)] = value;
}

// -----------------------------------------------------------------------------
/// Set the specified LNA drain current to the passed-in value (0 - 10 mA)
void AMB::FEBias::setLNADrainCurrent(Controls_t LNADrainCurrent,float value )
{
    if ((value < 0.0 ) || (value > 10.0))
    {
	throw FEBiasError(
	    "setLNADrainCurrent illegal value, must be 0.0 to 10.0");
    }
    m_LNADrainCurrents[LNAControlRCAToIndex(LNADrainCurrent)] = value;
}

// -----------------------------------------------------------------------------
/// Set the specified Schottky bias voltage to the passed-in value (0 - 15 V)
/// Setting the Schottky bias voltage impacts the Schottky mixer junction 
/// currents.  For this simulator a simple linear conversion between bias 
/// voltage (0-15V) and
/// junction currents (0 - 5 mA, -5 - 0 mA) is made.
void AMB::FEBias::setSchottkyBiasVoltage(
    Controls_t schottkyBiasVoltage,float value )
{
    if ((value < 0.0 ) || (value > 15.0))
    {
	throw FEBiasError(
	    "setSchottkyBiasVoltage illegal value, must be 0.0 to 15.0");
    }
    int schottkyIndex = schottkyControlRCAToIndex(schottkyBiasVoltage);
    float scaledValue = 5.0 * value / 15.0;
    m_SchottkyPosCurrents[schottkyIndex] = scaledValue;
    m_SchottkyNegCurrents[schottkyIndex] = -scaledValue;
}

// -----------------------------------------------------------------------------
/// Set the specified SIS mixer bias voltage to the passed-in value (0  - 20 mV)
/// Setting this voltage affects the SIS mixer junction voltage and current. 
/// In this simulator, the passed-in value is simply scaled to the junction
/// voltage (0 - 20 mV) and current (0 - 400 uA) with zero-bias error offset
/// bias voltage subtracted before scaling.  Zero-bias error offset for
/// junction voltage and current are subtracted after scaling. 
void AMB::FEBias::setSISBiasVoltage(Controls_t SISBiasVoltage,float value )
{
    if ((value < 0.0 ) || (value > 20.0))
    {
	throw FEBiasError(
	    "setSISBiasVoltage illegal value, must be 0.0 to 20.0");
    }
    int SISMixerIndex = SISMixerControlRCAToIndex(SISBiasVoltage);
    m_SISBiasVoltages[SISMixerIndex] = value
	- m_SIS0BiasBiasVoltages[SISMixerIndex];
    m_SISJunctionVoltages[SISMixerIndex] = m_SISBiasVoltages[SISMixerIndex];
    m_SISJunctionCurrents[SISMixerIndex]
	= m_SISBiasVoltages[SISMixerIndex] * 20.0; 
}

// -----------------------------------------------------------------------------
/// Set the specified SIS mixer zero-bias error voltage to the passed-in
/// value (-1  - 1 mV).  Setting this voltage affects the SIS mixer bias
/// voltage.  It is subtracted from the junction voltage after scaling.
void AMB::FEBias::setSISVoltageOffset(Controls_t SISVoltageOffset,float value )
{
    if ((value < -1.0 ) || (value > 1.0))
    {
	throw FEBiasError(
	    "setSISVoltageOffset illegal value, must be -1.0 to +1.0");
    }
    int SISMixerIndex = SISMixerOffsetRCAToIndex(SISVoltageOffset);
    m_SIS0BiasVoltages[SISMixerIndex] =  value;
}

// -----------------------------------------------------------------------------
/// Set the specified SIS mixer zero-bias error offset current to the passed-in 
/// value (-10 uA - 10 uA).  This value is subtracted from the SIS mixer
/// junction current after scaling.
void AMB::FEBias::setSISCurrentOffset(Controls_t SISCurrentOffset,float value)
{
    if ((value < -10.0 ) || (value > 10.0))
    {
	throw FEBiasError(
	    "setSISCurrentOffset illegal value, must be -10.0 to +10.0");
    }
    int SISMixerIndex = SISMixerOffsetRCAToIndex(SISCurrentOffset);
    m_SIS0BiasCurrents[SISMixerIndex] =  value;
}

// -----------------------------------------------------------------------------
/// Set the specified SIS mixer zero-bias error offset bias voltage to the
/// passed-in value (-1 mV - 1 mV). This value is subtracted from the bias
/// voltage before scaling. 
void AMB::FEBias::setSISBiasVoltageOffset(
    Controls_t SISBiasVoltage,float value)
{
    if ((value < -1.0 ) || (value > 1.0))
    {
	throw FEBiasError(
	    "setSISBiasVoltageOffset illegal value, must be -1.0 to +1.0");
    }
    int SISMixerIndex = SISMixerOffsetRCAToIndex(SISBiasVoltage);
    m_SIS0BiasBiasVoltages[SISMixerIndex] =  value;
    m_SISBiasVoltages[SISMixerIndex] -= value;
    m_SISJunctionVoltages[SISMixerIndex] = m_SISBiasVoltages[SISMixerIndex];
    m_SISJunctionCurrents[SISMixerIndex]
	= m_SISBiasVoltages[SISMixerIndex] * 20.0; 
}

// -----------------------------------------------------------------------------
/// Set the specified magnet current to the passed-in value (0 - 300 mA).
void AMB::FEBias::setMagnetCurrent(Controls_t magnetCurrent,float value )
{
    if ((value < 0.0 ) || (value > 300.0))
    {
	throw FEBiasError(
	    "setMagnetCurrent illegal value, must be 0.0 to 300.0");
    }
    m_SISMagnetCurrents[SISMagnetControlRCAToIndex(magnetCurrent)] = value;
}

// -----------------------------------------------------------------------------
// Conversion utilities
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
/// Get state for LNA associated with the specified monitor point 
int AMB::FEBias::LNAMonitorToLNAState(int monitorIndex)
{
    if (monitorIndex >= NUMBER_OF_LNA_STAGES)
	throw FEBiasError("LNAMonitorToLNAState monitorIndex out of range");
    if (monitorIndex >= TOTAL_STAGES_FOR_B3)
    {
	return (monitorIndex - TOTAL_STAGES_FOR_B3) / STAGES_FOR_B6;
    }
    else
    {
	return monitorIndex / STAGES_FOR_B3;
    }
}

// ----------------------------------------------------------------------------
/// Get index to monitor for a given LNA 
int AMB::FEBias::LNAStateToLNAMonitor (int LNAIndex)
{
    int monitorIndex = 0;
    for (int i = 0; i < LNAIndex; i++)
    {
	monitorIndex += STAGES_PER_LNA[i];
    }
    return monitorIndex;
}

// ----------------------------------------------------------------------------
/// Converts the bytes in data to a float.  If successful, returns true and
/// value contains float.  Otherwise returns false.
bool AMB::FEBias::validFloat(std::vector<CAN::byte_t> data, float &value) 
{
    if (data.size() == 4)
    {
	std::vector<CAN::byte_t> tmp(data);
	long tmpVal =  dataToLong(tmp);
	float *junk = reinterpret_cast<float *> (&tmpVal);
	value = *junk;
	return true;
    }
    else
    { 
	return false;
    }
}

// ----------------------------------------------------------------------------
/// Converts the float to data bytes.  Unlike AMB::floatToData this function
/// uses IEEE 754-1990 format
std::vector<CAN::byte_t> AMB::FEBias::IEEEFloatToData(float value) const 
{
    const std::vector<CAN::byte_t> tmp;
    unsigned long *tmpLong;
    float tmpFloat = value;
    tmpLong = reinterpret_cast<unsigned long *> (&tmpFloat);
    return longToData(tmp, *tmpLong);
}
