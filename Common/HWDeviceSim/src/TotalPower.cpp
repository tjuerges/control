/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2002 
*
* This code is under GNU General Public Licence (GPL). 
* Correspondence concerning ALMA Software should be addressed to:
* alma-sw-admin@nrao.edu
*
* Copyright (C) 2001
* Associated Universities, Inc. Washington DC, USA.
*
* Produced for the ALMA project
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Library General Public License
* as published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Library General Public License for more details.
*
* You should have received a copy of the GNU Library General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* dfugate  16/12/02  created 
*/

#include <iomanip>
#include <unistd.h>

#include "TotalPower.h"
#include "AMBUtil.h"

// -----------------------------------------------------------------------------

AMB::TotalPower::TotalPower(node_t node,
			    const std::vector<CAN::byte_t> &serialNumber) : 
    node_m(node), 
    sn_m(serialNumber), 
    trans_num_m(0)
{
    std::cout << "Creating node " << node << ", s/n 0x";
    for (int i = 0; i < 8; i++)
	{
	std::cout << std::hex << std::setw(2) << std::setfill('0') << (int)serialNumber[i];
	}
    std::cout << ", TotalPower device" << std::endl;


    /// Now for data related specifically to the totalpower device
    start_continuous_conversion_mode_m=false;
    reset_digitizers_m=false;
    control_command_busy_m=false;

    /// Simply set the fifo at position i to be i.
    for(int i=0; i<FIFO_SIZE; i++)
	{
	fifo_data_m[i]=i;
	}
    fifo_m=fifo_data_m;
    
    /// No fifo reads have occured yet.
    fifo_count_m=0;
    
    /// For now, registers status will always be 0 until something is 
    ///  defined within the ICD...
    memset(read_digitizer_status_registers_m, 0, 6);

    read_digitizer_status_m=0;

    // Start thread to simulate a 48ms pulse
    pthread_create(&tid, NULL, &TotalPower::timingEvent, (void*)this);

}

// -----------------------------------------------------------------------------
AMB::TotalPower::~TotalPower()
{
    // Nothing
}
// -----------------------------------------------------------------------------
AMB::node_t 
AMB::TotalPower::node() const
{
    return node_m;
}
// -----------------------------------------------------------------------------
std::vector<CAN::byte_t> 
AMB::TotalPower::serialNumber() const
{
    return sn_m;
}
// -----------------------------------------------------------------------------
std::vector<CAN::byte_t> 
AMB::TotalPower::monitor(rca_t rca) const
{
    trans_num_m++;
    const std::vector<CAN::byte_t> tmp;
    
    switch (rca) 
	{
	// Mandatory generic points
	case GET_CAN_ERROR:
	    return get_can_error();
	    break;
	case GET_PROTOCOL_REV_LEVEL:
	    return get_protocol_rev_level();
	    break;
	case AMB::Device::GET_SW_REV_LEVEL:
			return get_software_rev_level();
			break;
	case AMB::Device::GET_HW_REV_LEVEL:
			return get_hardware_rev_level();
			break;
	case GET_TRANS_NUM:
	    return longToData(tmp, get_trans_num());
	    break;
	    // Optional generic point
	case GET_AMBIENT_TEMPERATURE:
	    return floatDS1820ToData(tmp, get_ambient_temperature());
	    break;
	    
	// Monitor points specific to the TP (from the ICD).
	case READ_CONTINUOUS_CONVERSION_DATA_BLOCK:
	    return read_continuous_conversion_data_block();
	    break;
	case READ_DIGITIZER_STATUS_REGISTERS:
	    return read_digitizer_status_registers();
	    break;
	case READ_SINGLE_CONVERSION_DATA_BLOCK_UL:  
	    return read_single_conversion_data_block_ul();
	    break;
	case READ_SINGLE_CONVERSION_DATA_BLOCK_AB:  
	    return read_single_conversion_data_block_ab();
	    break;
	case READ_SINGLE_CONVERSION_DATA_BLOCK_CD:  
	    return read_single_conversion_data_block_cd();
	    break;
	case READ_DIGITIZER_UL_MODE_REGISTERS:
	    return read_two_shorts();
	    break;
	case READ_DIGITIZER_AB_MODE_REGISTERS:      
	    return read_two_shorts();
	    break;
	case READ_DIGITIZER_CD_MODE_REGISTERS:     
	    return read_two_shorts();
	    break;
	case READ_DIGITIZER_UL_FILTER_REGISTERS:   
	    return read_two_shorts();
	    break;
	case READ_DIGITIZER_AB_FILTER_REGISTERS:   
	    return read_two_shorts();
	    break;
	case READ_DIGITIZER_CD_FILTER_REGISTERS:   
	    return read_two_shorts();
	    break;
	case READ_DIGITIZER_UL_OFFSET_REGISTERS:   
	    return read_two_ints();
	    break;
	case READ_DIGITIZER_AB_OFFSET_REGISTERS:   
	    return read_two_ints();
	    break;
	case READ_DIGITIZER_CD_OFFSET_REGISTERS:   
	    return read_two_ints();
	    break;
	case READ_DIGITIZER_UL_GAIN_REGISTERS:     
	    return read_two_ints();
	    break;
	case READ_DIGITIZER_AB_GAIN_REGISTERS:     
	    return read_two_ints();
	    break;
	case READ_DIGITIZER_CD_GAIN_REGISTERS:      
	    return read_two_ints();
	    break;
	case READ_DIGITIZER_STATUS:                
	    return read_digitizer_status();
	    break;
	default:
	    std::cerr << "Switch does not match any case" << std::endl;
	    throw TotalPowerError("Unknown RCA in monitor command");
	}
}

// -----------------------------------------------------------------------------
void 
AMB::TotalPower::control(rca_t rca, const std::vector<CAN::byte_t> &data)
{
    trans_num_m++;
    if (data.size() != 1) 
	{
	std::cout << "All TP set commands are of size 1!\n";
	}
    
    switch (rca) 
	{
	case RESET_DIGITIZERS:
	    reset_digitizers(data[0]);
	    break;
	case START_CONTINUOUS_CONVERSION_MODE:    
	    start_continuous_conversion_mode(data[0]);
	    break;
	case STOP_CONTINUOUS_CONVERSION_MODE:  
	    stop_continuous_conversion_mode(data[0]);
	    break;
	case ACTIVATE_DIGITIZER_BURNOUT_CURRENTS:  
	    activate_digitizer_burnout_currents(data[0]);
	    break;
	case DE_ACTIVATE_DIGITIZER_BURNOUT_CURRENTS: 
	    de_activate_digitizer_burnout_currents(data[0]);
	    break;
	    
	default:
	    throw TotalPowerError("Unknown RCA in control command");
	    break;
	}
}

// -----------------------------------------------------------------------------
std::vector<CAN::byte_t> 
AMB::TotalPower::get_can_error() const
{
    return AMB::Device::get_can_error();
}
// -----------------------------------------------------------------------------
std::vector<CAN::byte_t> 
AMB::TotalPower::get_protocol_rev_level() const
{
    return AMB::Device::get_protocol_rev_level();
}
// -----------------------------------------------------------------------------
unsigned int 
AMB::TotalPower::get_trans_num() const
{
    return trans_num_m;
}
// -----------------------------------------------------------------------------
float 
AMB::TotalPower::get_ambient_temperature() const
{
    return 18.0;
}


// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> 
AMB::TotalPower::read_continuous_conversion_data_block() const
{
    std::vector<CAN::byte_t> tmp(8);
    
    if(start_continuous_conversion_mode_m && (fifo_count_m < 36))
	{
	/// Take advantage of the fact that fifo_m[0-4] will always be < 256.
	tmp[0] = 0x00;
	tmp[1] = fifo_m[0];
	tmp[2] = 0x00;
	tmp[3] = fifo_m[1];
	tmp[4] = 0x00;
	tmp[5] = fifo_m[2];
	tmp[6] = 0x00;
	tmp[7] = fifo_m[3];
	
	/// Pop four elements off the fifo stack.
	fifo_m = fifo_m + 4;
	
	fifo_count_m++;
	}
    else
	{
	for(int i=0; i<8; i++)
	    {
	    tmp[i] = 0x00;
	    }
	}
            
    return tmp;
}
// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> 
AMB::TotalPower::read_digitizer_status_registers() const
{
    std::vector<CAN::byte_t> tmp(6);
    
    for(int i=0; i<6; i++)
	{
	tmp[i] = read_digitizer_status_registers_m[i];
	}
    return tmp;
}
// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> 
AMB::TotalPower::read_single_conversion_data_block_ul() const
{
    std::vector<CAN::byte_t> tmp(4);
    
    tmp[0] = 0x00;
    tmp[1] = fifo_data_m[0];
    tmp[2] = 0x00;
    tmp[3] = fifo_data_m[1];

    return tmp;
}
// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> 
AMB::TotalPower::read_single_conversion_data_block_ab() const
{
    std::vector<CAN::byte_t> tmp(4);
    
    tmp[0] = 0x00;
    tmp[1] = fifo_data_m[2];
    tmp[2] = 0x00;
    tmp[3] = fifo_data_m[3];

    return tmp;    
}
// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> 
AMB::TotalPower::read_single_conversion_data_block_cd() const
{
    std::vector<CAN::byte_t> tmp(4);
    
    tmp[0] = 0x00;
    tmp[1] = fifo_data_m[4];
    tmp[2] = 0x00;
    tmp[3] = fifo_data_m[5];

    return tmp;
}
// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> 
AMB::TotalPower::read_two_shorts() const
{
    std::vector<CAN::byte_t> tmp(4);
    
    char joe = 0x05;
    if(start_continuous_conversion_mode_m)
	{
	joe = 0x00;
	}
    
    tmp[0] = 0x00;
    tmp[1] = joe;

    tmp[2] = 0x00;
    tmp[3] = joe;
    
    return tmp; 
}
// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> 
AMB::TotalPower::read_two_ints() const
{
    std::vector<CAN::byte_t> tmp(8);
    
    char joe = 0x05;
    if(start_continuous_conversion_mode_m || !reset_digitizers_m)
	{
	joe = 0x00;
	}
    
    tmp[0] = 0x00;
    tmp[1] = 0x00;
    tmp[2] = 0x00;
    tmp[3] = joe;

    tmp[4] = 0x00;
    tmp[5] = 0x00;
    tmp[6] = 0x00;
    tmp[7] = joe;
    
    return tmp; 
}
// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> 
AMB::TotalPower::read_digitizer_status() const
{
    std::vector<CAN::byte_t> tmp(1);
    
    /// Initialize
    tmp[0]=0;
    /// Set bits 1,0: since these commands are not asynchronous for the simulator,
    ///  only two values will be allowed: read complete and read past end of buffer.
    if(fifo_count_m>36)
	{
	tmp[0] = tmp[0] | 0x02;
	}
    /// Set bit 2:
    if(start_continuous_conversion_mode_m)
	{
	tmp[0] = tmp[0] | 0x04;
	}
    /// Set bit 3:
    if(control_command_busy_m)
	{
	tmp[0] = tmp[0] | 0x08;
	}

    return tmp; 
}
// ----------------------------------------------------------------------------
void 
AMB::TotalPower::reset_digitizers(CAN::byte_t state)
{
    reset_digitizers_m = true;
    control_command_busy_m = true;

    usleep(125);
    fifo_m=fifo_data_m;
    fifo_count_m=0;

    control_command_busy_m = false;
}
// ----------------------------------------------------------------------------
void 
AMB::TotalPower::start_continuous_conversion_mode(CAN::byte_t state)
{
    start_continuous_conversion_mode_m=true;
}
// ----------------------------------------------------------------------------
void 
AMB::TotalPower::stop_continuous_conversion_mode(CAN::byte_t state)
{
    start_continuous_conversion_mode_m=false;
}
// ----------------------------------------------------------------------------
void 
AMB::TotalPower::activate_digitizer_burnout_currents(CAN::byte_t state)
{
    /// ???
}
// ----------------------------------------------------------------------------
void 
AMB::TotalPower::de_activate_digitizer_burnout_currents(CAN::byte_t state)
{
  /// ???  
}
// ----------------------------------------------------------------------------
/// This updates all monitor points associated with the 48ms timing
/// event.
void AMB::TotalPower::update()
{
    fifo_m=fifo_data_m;
    fifo_count_m=0;
}
// ----------------------------------------------------------------------------
void * 
AMB::TotalPower::timingEvent(void* data)
{
    TotalPower* This = (TotalPower *)data;
    while(1)
	{
	/// sleep for 48 milliseconds then update the properties 
	usleep(48);
	This->update();
	}
 
}
// ----------------------------------------------------------------------------
