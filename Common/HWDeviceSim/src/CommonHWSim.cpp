// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include "CommonHWSim.h"

using namespace AMB;


/// Generic monitor point RCA values
const rca_t CommonHWSim::GET_SERIAL_NUMBER 			= 0x00000000U;
const rca_t CommonHWSim::GET_PROTOCOL_REV_LEVEL 	= 0x00030000U;
const rca_t CommonHWSim::GET_CAN_ERROR 				= 0x00030001U;
const rca_t CommonHWSim::GET_TRANS_NUM 				= 0x00030002U;
const rca_t CommonHWSim::GET_AMBIENT_TEMPERATURE 	= 0x00030003U;
const rca_t CommonHWSim::GET_SW_REV_LEVEL 			= 0x00030004U;
const rca_t CommonHWSim::GET_HW_REV_LEVEL 			= 0x00030005U;

/// Generic control point RCA values
const rca_t CommonHWSim::RESET_AMBSI 				= 0x00031000U;
const rca_t CommonHWSim::RESET_DEVICE 				= 0x00031001U;


// Constructor
// -----------------------------------------------------------------------------
CommonHWSim::CommonHWSim()
{
	/// Set fake device name for unit test
	devname_m = "CommonHWSim";
	
	/// Initialize monitor and control points
	initialize();
}

// Destructor
// -----------------------------------------------------------------------------
CommonHWSim::~CommonHWSim()
{
	std::map< rca_t , std::vector<CAN::byte_t>* >::iterator it;   //, aux;
	
	/// Delete all vectors pointed by state_m, considering some pointers
	/// to the same vector (associated getter-setter)
	for (it = state_m.begin(); it != state_m.end(); it++) {
		if (it->second != NULL)
			delete it->second;
	}
}

// AMB::Device abstract methods (not used)
// ----------------------------------------------------------------------------
node_t CommonHWSim::node() const {
	return node_m;
}
std::vector<CAN::byte_t> CommonHWSim::serialNumber() const {
	return getSerialNumber();
}
unsigned int CommonHWSim::get_trans_num() const {
	unsigned int value;
	
	std::vector<CAN::byte_t> vvalue = getTransNum();
	TypeConversion::dataToValue(vvalue, value);
	
	return value;
}

// Monitor and Control Points creation (state_m map entries)
// and value initialization
// ----------------------------------------------------------------------------
void CommonHWSim::initialize()
{
	/// Monitor points
	state_m.insert(std::make_pair(GET_SERIAL_NUMBER, new std::vector<CAN::byte_t>(8,0x00)));
	state_m.insert(std::make_pair(GET_PROTOCOL_REV_LEVEL, new std::vector<CAN::byte_t>(3,0x00)));
	state_m.insert(std::make_pair(GET_CAN_ERROR, new std::vector<CAN::byte_t>(4,0x00)));
	state_m.insert(std::make_pair(GET_TRANS_NUM, new std::vector<CAN::byte_t>(4,0x00)));
	state_m.insert(std::make_pair(GET_AMBIENT_TEMPERATURE, new std::vector<CAN::byte_t>(4,0x00)));
	state_m.insert(std::make_pair(GET_SW_REV_LEVEL, new std::vector<CAN::byte_t>(3,0x00)));
	state_m.insert(std::make_pair(GET_HW_REV_LEVEL, new std::vector<CAN::byte_t>(3,0x00)));

	/// Control points
	state_m.insert(std::make_pair(RESET_AMBSI, new std::vector<CAN::byte_t>(1,0x00)));
	state_m.insert(std::make_pair(RESET_DEVICE, new std::vector<CAN::byte_t>(1,0x00)));
}


// Monitor and Control Points getters
// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> CommonHWSim::monitor(rca_t rca) const
{
	switch (rca) {
	case GET_SERIAL_NUMBER:
		return getSerialNumber();
		break;
	case GET_PROTOCOL_REV_LEVEL:
		return getProtocolRevLevel();
		break;
	case GET_CAN_ERROR:
		return getCanError();
		break;
	case GET_TRANS_NUM:
		return getTransNum();
		break;
	case GET_AMBIENT_TEMPERATURE:
		return getAmbientTemperature();
		break;
	case GET_SW_REV_LEVEL:
		return getSwRevLevel();
		break;
	case GET_HW_REV_LEVEL:
		return getHwRevLevel();
		break;
	default:
		return controlGetter(rca);
	}
}

std::vector<CAN::byte_t> CommonHWSim::controlGetter(rca_t rca) const
{
	switch (rca) {
	case RESET_AMBSI:
		return getResetAmbsi();
		break;
	case RESET_DEVICE:
		return getResetDevice();
		break;
	default:
		std::ostringstream error;
		error << "Unknown RCA in get command for device " << devname_m
			<< ". Switch " << std::hex << rca << " does not match any case.";
		throw CAN::Error(error.str());
	}
}

// Control and Monitor Points setters
// ----------------------------------------------------------------------------
void CommonHWSim::control(rca_t rca, const std::vector<CAN::byte_t>& data)
{
	switch (rca) {
	case RESET_AMBSI:
		setResetAmbsi(data);
		break;
	case RESET_DEVICE:
		setResetDevice(data);
		break;
	default:
		monitorSetter(rca, data);
	}
}

void CommonHWSim::monitorSetter(rca_t rca, const std::vector<CAN::byte_t>& data)
{
	switch (rca) {
	case GET_SERIAL_NUMBER:
		setSerialNumber(data);
		break;
	case GET_PROTOCOL_REV_LEVEL:
		setProtocolRevLevel(data);
		break;
	case GET_CAN_ERROR:
		setCanError(data);
		break;
	case GET_TRANS_NUM:
		setTransNum(data);
		break;
	case GET_AMBIENT_TEMPERATURE:
		setAmbientTemperature(data);
		break;
	case GET_SW_REV_LEVEL:
		setSwRevLevel(data);
		break;
	case GET_HW_REV_LEVEL:
		setHwRevLevel(data);
		break;
	default:
		std::ostringstream error;
		error << "Unknown RCA in set command for device " << devname_m
			<< ". Switch " << std::hex << rca << " does not match any case.";
		throw CAN::Error(error.str());
	}
}

// Monitor get helpers
// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> CommonHWSim::getSerialNumber() const {
	return *(state_m.find(GET_SERIAL_NUMBER)->second);
}
std::vector<CAN::byte_t> CommonHWSim::getProtocolRevLevel() const {
	return *(state_m.find(GET_PROTOCOL_REV_LEVEL)->second);
}
std::vector<CAN::byte_t> CommonHWSim::getCanError() const {
	return *(state_m.find(GET_CAN_ERROR)->second);
}
std::vector<CAN::byte_t> CommonHWSim::getTransNum() const {
	return *(state_m.find(GET_TRANS_NUM)->second);
}
std::vector<CAN::byte_t> CommonHWSim::getAmbientTemperature() const {
	return *(state_m.find(GET_AMBIENT_TEMPERATURE)->second);
}
std::vector<CAN::byte_t> CommonHWSim::getSwRevLevel() const {
	return *(state_m.find(GET_SW_REV_LEVEL)->second);
}
std::vector<CAN::byte_t> CommonHWSim::getHwRevLevel() const {
	return *(state_m.find(GET_HW_REV_LEVEL)->second);
}

// Control get helpers
// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> CommonHWSim::getResetAmbsi() const {
	return *(state_m.find(RESET_AMBSI)->second);
}
std::vector<CAN::byte_t> CommonHWSim::getResetDevice() const {
	return *(state_m.find(RESET_DEVICE)->second);
}

// Control set helpers
// ----------------------------------------------------------------------------
void CommonHWSim::setResetAmbsi(const std::vector<CAN::byte_t>& data)  {
	checkSize(data,1,"RESET_AMBSI");
	if (state_m.find(RESET_AMBSI) != state_m.end())
		*(state_m.find(RESET_AMBSI)->second) = data;
	else
		throw CAN::Error("Trying to set RESET_AMBSI. Member not found.");
}
void CommonHWSim::setResetDevice(const std::vector<CAN::byte_t>& data)  {
	checkSize(data,1,"RESET_DEVICE");
	if (state_m.find(RESET_DEVICE) != state_m.end())
		*(state_m.find(RESET_DEVICE)->second) = data;
	else
		throw CAN::Error("Trying to set RESET_DEVICE. Member not found.");
}

// Monitor set helpers
// ----------------------------------------------------------------------------
void CommonHWSim::setSerialNumber(const std::vector<CAN::byte_t>& data) {
	checkSize(data,8,"GET_SERIAL_NUMBER");
	if (state_m.find(GET_SERIAL_NUMBER) != state_m.end())
		*(state_m.find(GET_SERIAL_NUMBER)->second) = data;
	else
		throw CAN::Error("Trying to set GET_SERIAL_NUMBER. Member not found.");
}
void CommonHWSim::setProtocolRevLevel(const std::vector<CAN::byte_t>& data) {
	checkSize(data,3,"GET_PROTOCOL_REV_LEVEL");
	if (state_m.find(GET_PROTOCOL_REV_LEVEL) != state_m.end())
		*(state_m.find(GET_PROTOCOL_REV_LEVEL)->second) = data;
	else
		throw CAN::Error("Trying to set GET_PROTOCOL_REV_LEVEL. Member not found.");
}
void CommonHWSim::setCanError(const std::vector<CAN::byte_t>& data) {
	checkSize(data,4,"GET_CAN_ERROR");
	if (state_m.find(GET_CAN_ERROR) != state_m.end())
		*(state_m.find(GET_CAN_ERROR)->second) = data;
	else
		throw CAN::Error("Trying to set GET_CAN_ERROR. Member not found.");
}
void CommonHWSim::setTransNum(const std::vector<CAN::byte_t>& data) {
	checkSize(data,4,"GET_TRANS_NUM");
	if (state_m.find(GET_TRANS_NUM) != state_m.end())
		*(state_m.find(GET_TRANS_NUM)->second) = data;
	else
		throw CAN::Error("Trying to set GET_TRANS_NUM. Member not found.");
}
void CommonHWSim::setAmbientTemperature(const std::vector<CAN::byte_t>& data) {
	checkSize(data,4,"GET_AMBIENT_TEMPERATURE");
	if (state_m.find(GET_AMBIENT_TEMPERATURE) != state_m.end())
		*(state_m.find(GET_AMBIENT_TEMPERATURE)->second) = data;
	else
		throw CAN::Error("Trying to set GET_AMBIENT_TEMPERATURE. Member not found.");
}
void CommonHWSim::setSwRevLevel(const std::vector<CAN::byte_t>& data) {
	checkSize(data,3,"GET_SW_REV_LEVEL");
	if (state_m.find(GET_SW_REV_LEVEL) != state_m.end())
		*(state_m.find(GET_SW_REV_LEVEL)->second) = data;
	else
		throw CAN::Error("Trying to set GET_SW_REV_LEVEL. Member not found.");
}
void CommonHWSim::setHwRevLevel(const std::vector<CAN::byte_t>& data) {
	checkSize(data,3,"GET_HW_REV_LEVEL");
	if (state_m.find(GET_HW_REV_LEVEL) != state_m.end())
		*(state_m.find(GET_HW_REV_LEVEL)->second) = data;
	else
		throw CAN::Error("Trying to set GET_HW_REV_LEVEL. Member not found.");
}

// Check data size
// ----------------------------------------------------------------------------
void CommonHWSim::checkSize(const std::vector<CAN::byte_t>& data, const int size, const char *pExceptionMsg)
{
	if( static_cast<int>(data.size()) != size ) {
		char exceptionMsg[200];
		sprintf(exceptionMsg,"%s data length is %d, must be %d",pExceptionMsg, data.size(), size);
		throw CAN::Error(exceptionMsg);
	}
}

