// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2007
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "OPLL.h"
#include "CANTypes.h"
#include "AMBUtil.h"
#include <iostream>
#include <iomanip>
#include <vector>
#include <sstream>
using std::vector;
using std::cout;
using std::endl;
using std::hex;
using std::setw;
using std::setfill;
using std::ostringstream;
using AMB::OPLL;

// ----------------------------------------------------------------------------
OPLL::OPLL(node_t node, 
           const vector<CAN::byte_t>& serialNumber)
  :node_m(node),
   sn_m(serialNumber),
   transNum_m(0),
   BAND_STATUS_m(42.0),// 42 --> 0x2A BandC
   LOCK_STATUS_m(1.0), // Not Locked
   CTNLL_READY_m(1.0),
   PM_V_BAND_A_m (288.0),// 288 --> 1.4V - 0 - 1024 range  
   PM_I_BAND_A_m (277.0),// 
   PM_V_BAND_B_m (419.0),// 419 --> 2.04V
   PM_I_BAND_B_m (0.0),
   PM_V_BAND_C_m (416.0),// 416 --> 2.03V
   PM_I_BAND_C_m (305.0),// 305 --> 1.4mA
   PM_V_BAND_D_m (238.0),// 238 --> 1.16V
   PM_I_BAND_D_m (239.0),// 239 --> 1.16mA
   IF_LEVEL_m(890.0),   //  890 --> 1.71V
   REF_LEVEL_125MHZ_m(131.0),// 131 --> 0.639
   PIEZO_VOLTAGE_m(507.0),// 507 -->11.88
   VCO_TUNING_V_m(539.0),//  539 -->2.63
   IF_VCTRL_m(1.0)      // 1 --> 0.004882812
/*
*/
{
  cout << "Creating node 0x" << node << ", s/n 0x";
  for (int i = 0; i < 8; i++) {
    cout << hex << setw(2) << setfill('0')
         << static_cast<int>(serialNumber[i]);
  }
  cout << ", OPLL device" << endl;

  if (node != 0x38 && 
      node != 0x3A &&
      node != 0x3C &&
      node != 0x3E) {
    ostringstream errMessage;
    errMessage << "Bad node number. It should be 0x0x3{8,A,C,E} but 0x"
	       << hex << node << " was specified." << endl;
    throw CAN::Error(errMessage.str());
  }
}

// ----------------------------------------------------------------------------
OPLL::~OPLL() {
  // Nothing
}

// ----------------------------------------------------------------------------
AMB::node_t OPLL::node() const {
  return node_m;
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> OPLL::serialNumber() const {
    return sn_m;
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> OPLL::monitor(rca_t rca) const {
  transNum_m++;
  vector<CAN::byte_t> tmp;
  
  switch (rca) {
    // Mandatory generic points
  case GET_SERIAL_NUM:
    return serialNumber();
    break;
  case GET_CAN_ERROR:
    return get_can_error();
    break;
  case GET_PROTOCOL_REV_LEVEL:
    return get_protocol_rev_level();
    break;
  case GET_SW_REV_LEVEL:
    return get_software_rev_level();
    break;
  case GET_HW_REV_LEVEL:
    return get_hardware_rev_level();
    break;
  case GET_TRANS_NUM:
    return longToData(tmp, get_trans_num());
    break;
    // Optional generic point
  case GET_AMBIENT_TEMPERATURE:
    return floatDS1820ToData(tmp, getAmbientTemperature());
    break;
  case GET_BAND_STATUS:
    return CAN_BAND_STATUS();
    break;
  case GET_LOCK_STATUS:
    return CAN_LOCK_STATUS();
    break;
  case GET_CTNLL_READY:
    return CAN_CTNLL_READY();
    break;
  case GET_PM_V_BAND_A:
    return CAN_PM_V_BAND_A();
    break;
  case GET_PM_I_BAND_A:
    return CAN_PM_I_BAND_A();
    break;
  case GET_PM_V_BAND_B:
    return CAN_PM_V_BAND_B();
    break;
  case GET_PM_I_BAND_B:
    return CAN_PM_I_BAND_B();
    break;
  case GET_PM_V_BAND_C:
    return CAN_PM_V_BAND_C();
    break;
  case GET_PM_I_BAND_C:
    return CAN_PM_I_BAND_C();
    break;
  case GET_PM_V_BAND_D:
    return CAN_PM_V_BAND_D();
    break;
  case GET_PM_I_BAND_D:
    return CAN_PM_I_BAND_D();
    break;
  case GET_IF_LEVEL:
    return CAN_IF_LEVEL();
    break;
  case GET_REF_LEVEL_125MHZ:
    return CAN_REF_LEVEL_125MHZ();
    break;
  case GET_PIEZO_VOLTAGE:
    return CAN_PIEZO_VOLTAGE();
    break;
  case GET_VCO_TUNING_V:
    return CAN_VCO_TUNING_V();
    break;
  case GET_IF_VCTRL:
    return CAN_IF_VCTRL();
    break;
  case GET_PORT2_DATA:
     tmp.push_back(0);
     tmp.push_back(0);
     return tmp;
    break;
  case GET_PORT7_DATA:
     tmp.push_back(0);
     return tmp;
    break;
  case GET_PORT8_DATA:
     tmp.push_back(0);
     return tmp;
    break;
/*
*/
  default:
    ostringstream errMessage;
    errMessage << "Unknown RCA (0x" << hex << rca 
               << ") in monitor command." << endl;
    throw CAN::Error(errMessage.str());
  }
  // Should never get here
  return tmp;
}

// ----------------------------------------------------------------------------
void OPLL::control(rca_t rca, const vector<CAN::byte_t>& data) {
  transNum_m++;
  short int fakeVal;

  switch (rca) {
  case RESET_DEVICE:
    // TODO find out what the hardware does when hit with this command
    break;

  case SET_BAND:
    checkSize(data,1,"SET_BAND");
    BAND_STATUS_m = dataToDouble(const_cast<std::vector<CAN::byte_t> &>(data));
    break;
  case SET_IF_VCTRL:
    checkSize(data,2,"SET_IF_VCTRL");
    IF_VCTRL_m = dataToDouble(const_cast<std::vector<CAN::byte_t> &>(data));
    break;

  case LOCK_STATUS_RESET:
     //checkSize(data,8,"LOCK_STATUS_RESET");
    //LOCK_STATUS_m = dataToDouble(const_cast<std::vector<CAN::byte_t> &>(data));
     LOCK_STATUS_m=0.0;
     break;

  case SET_OPTSW_STATE:
    {
    checkSize(data,1,"SET_OPTSW_STATE");

    const unsigned char chValOPTSW =
       dataToChar( const_cast<std::vector<CAN::byte_t> &>(data))& 0x03;
    const unsigned char chOldValOPTSW = 
       static_cast<unsigned char>(BAND_STATUS_m) & 0xFC;

    BAND_STATUS_m = static_cast<double>(chOldValOPTSW | chValOPTSW);
    }
    break;

  case SET_RFSW_STATE:
    {
      checkSize(data,1,"SET_RFSW_STATE");

    const unsigned char chValRFSW =
       dataToChar( const_cast<std::vector<CAN::byte_t> &>(data))& 0x0C;
    const unsigned char chOldValRFSW = 
       static_cast<unsigned char>(BAND_STATUS_m) & 0xF3;

    BAND_STATUS_m = static_cast<double>(chOldValRFSW | chValRFSW);
    }
    break;

  case SET_IFSW_STATE:
    {
    checkSize(data,1,"SET_IFSW_STATE");

    const unsigned char chValIFSW =
       dataToChar( const_cast<std::vector<CAN::byte_t> &>(data))& 0x30;
    const unsigned char chOldValIFSW = 
       static_cast<unsigned char>(BAND_STATUS_m) & 0xCF;

    BAND_STATUS_m = static_cast<double>(chOldValIFSW | chValIFSW);
    }
    break;



    //
    // FAKE control point added for testing alarms.
    //
  case TEST_BAND_STATUS:
     checkSize(data,8,"BAND_STATUS");
     BAND_STATUS_m =
dataToDouble(const_cast<std::vector<CAN::byte_t> &>(data));
    break;
  case TEST_LOCK_STATUS:
     checkSize(data,8,"LOCK_STATUS");
     LOCK_STATUS_m =dataToDouble(const_cast<std::vector<CAN::byte_t> &>(data));
    break;
  case TEST_CTNLL_READY:
     checkSize(data,8,"CTNLL_READY");
     CTNLL_READY_m =dataToDouble(const_cast<std::vector<CAN::byte_t> &>(data));
    break;
  case TEST_PM_V_BAND_A:
     checkSize(data,8,"TEST_PM_V_BAND_A");
     fakeVal = static_cast<short int>(
        dataToDouble( const_cast<std::vector<CAN::byte_t> &>(data)) * 204.8);
     PM_V_BAND_A_m =static_cast<double>(fakeVal);
     break;
  case TEST_PM_I_BAND_A:
     checkSize(data,8,"TEST_PM_I_BAND_A");
     fakeVal = static_cast<short int>(
        dataToDouble( const_cast<std::vector<CAN::byte_t> &>(data)) * 204.8);
     PM_I_BAND_A_m =static_cast<double>(fakeVal);
     break;
  case TEST_PM_V_BAND_B:
     checkSize(data,8,"TEST_PM_V_BAND_B");
     fakeVal = static_cast<short int>(
        dataToDouble( const_cast<std::vector<CAN::byte_t> &>(data)) * 204.8);
     PM_V_BAND_B_m =static_cast<double>(fakeVal);
     break;
  case TEST_PM_I_BAND_B:
     checkSize(data,8,"TEST_PM_I_BAND_B");
     fakeVal = static_cast<short int>(
        dataToDouble( const_cast<std::vector<CAN::byte_t> &>(data)) * 204.8);
     PM_I_BAND_B_m =static_cast<double>(fakeVal);
     break;
  case TEST_PM_V_BAND_C:
     checkSize(data,8,"TEST_PM_V_BAND_C");
     fakeVal = static_cast<short int>(
        dataToDouble( const_cast<std::vector<CAN::byte_t> &>(data)) * 204.8);
     PM_V_BAND_C_m =static_cast<double>(fakeVal);
     break;
  case TEST_PM_I_BAND_C:
     checkSize(data,8,"TEST_PM_I_BAND_C");
     fakeVal = static_cast<short int>(
        dataToDouble( const_cast<std::vector<CAN::byte_t> &>(data)) * 204.8);
     PM_I_BAND_C_m =static_cast<double>(fakeVal);
     break;
  case TEST_PM_V_BAND_D:
     checkSize(data,8,"TEST_PM_V_BAND_D");
     fakeVal = static_cast<short int>(
        dataToDouble( const_cast<std::vector<CAN::byte_t> &>(data)) * 204.8);
     PM_V_BAND_D_m =static_cast<double>(fakeVal);
     break;
  case TEST_PM_I_BAND_D:
     checkSize(data,8,"TEST_PM_I_BAND_D");
     fakeVal = static_cast<short int>(
        dataToDouble( const_cast<std::vector<CAN::byte_t> &>(data)) * 204.8);
     PM_I_BAND_D_m =static_cast<double>(fakeVal);
     break;
  case TEST_IF_LEVEL:
     checkSize(data,8,"TEST_IF_LEVEL");
     fakeVal = static_cast<short int>(
        dataToDouble( const_cast<std::vector<CAN::byte_t> &>(data)) * 1228.8);
     IF_LEVEL_m =static_cast<double>(fakeVal);
     break;
  case TEST_REF_LEVEL_125MHZ:
     checkSize(data,8,"TEST_125MHZ_REF_LEVEL");
     fakeVal = static_cast<short int>(
        dataToDouble( const_cast<std::vector<CAN::byte_t> &>(data)) * 204.8);
     REF_LEVEL_125MHZ_m =static_cast<double>(fakeVal);
     break;
  case TEST_PIEZO_VOLTAGE:
     checkSize(data,8,"TEST_PIEZO_VOLTAGE");
     fakeVal = static_cast<short int>(
        dataToDouble( const_cast<std::vector<CAN::byte_t> &>(data)) * 42.67);
     PIEZO_VOLTAGE_m =static_cast<double>(fakeVal);
     break;
  case TEST_IF_VCTRL:
     checkSize(data,8,"TEST_IF_VCTRL");
     fakeVal = static_cast<short int>(
        dataToDouble( const_cast<std::vector<CAN::byte_t> &>(data)) * 204.8);
     IF_VCTRL_m =static_cast<double>(fakeVal);
     break;
  default:
    ostringstream errMessage;
    errMessage << "Unknown RCA (0x" << hex << rca 
               << ") in control command." << endl;
    throw CAN::Error(errMessage.str());
  }
}

void AMB::OPLL::checkSize(const std::vector<CAN::byte_t> &data, int size,
                          const char *pExceptionMsg) {
  if( static_cast<int>(data.size()) != size ) {
        char exceptionMsg[200];
        sprintf(exceptionMsg,"%s data length is %d must be %d",pExceptionMsg,
                data.size(), size);
        throw CAN::Error(exceptionMsg);
  }
}


// ----------------------------------------------------------------------------
unsigned int OPLL::get_trans_num() const {
  return transNum_m;
}

// ----------------------------------------------------------------------------
float OPLL::getAmbientTemperature() const {
  return 18.0;
}

std::vector<CAN::byte_t> OPLL::CAN_BAND_STATUS() const
{
  const std::vector<CAN::byte_t> tmp;
  const unsigned char charValue =
    static_cast<unsigned char>(BAND_STATUS_m) & 0x3F;
  return charToData(tmp, charValue);
}

std::vector<CAN::byte_t> OPLL::CAN_LOCK_STATUS() const
{
  const std::vector<CAN::byte_t> tmp;
  const unsigned char charValue =
    static_cast<unsigned char>(LOCK_STATUS_m) & 0x01;
  return charToData(tmp, charValue);
}

std::vector<CAN::byte_t> OPLL::CAN_CTNLL_READY() const
{
  const std::vector<CAN::byte_t> tmp;
  const unsigned char charValue =
    static_cast<unsigned char>(CTNLL_READY_m) & 0x01;
  return charToData(tmp, charValue);
}


std::vector<CAN::byte_t> OPLL::CAN_PM_V_BAND_A() const
{
  const std::vector<CAN::byte_t> tmp;
  const short int intValue =
     static_cast<short int>(PM_V_BAND_A_m) & 0x03FF;
  return shortToData(tmp, intValue);
}

std::vector<CAN::byte_t> OPLL::CAN_PM_I_BAND_A() const
{
  const std::vector<CAN::byte_t> tmp;
  const short int intValue =
    static_cast<short int>(PM_I_BAND_A_m) & 0x03FF;
  return shortToData(tmp, intValue);
}
std::vector<CAN::byte_t> OPLL::CAN_PM_V_BAND_B() const
{
  const std::vector<CAN::byte_t> tmp;
  const short int intValue =
    static_cast<short int>(PM_V_BAND_B_m) & 0x03FF;
  return shortToData(tmp, intValue);
}
std::vector<CAN::byte_t> OPLL::CAN_PM_I_BAND_B() const
{
  const std::vector<CAN::byte_t> tmp;
  const short int intValue =
    static_cast<short int>(PM_I_BAND_B_m) & 0x03FF;
  return shortToData(tmp, intValue);
}
std::vector<CAN::byte_t> OPLL::CAN_PM_V_BAND_C() const
{
  const std::vector<CAN::byte_t> tmp;
  const short int intValue =
    static_cast<short int>(PM_V_BAND_C_m) & 0x03FF;
  return shortToData(tmp, intValue);
}
std::vector<CAN::byte_t> OPLL::CAN_PM_I_BAND_C() const
{
  const std::vector<CAN::byte_t> tmp;
  const short int intValue =
    static_cast<short int>(PM_I_BAND_C_m) & 0x03FF;
  return shortToData(tmp, intValue);
}
std::vector<CAN::byte_t> OPLL::CAN_PM_V_BAND_D() const
{
  const std::vector<CAN::byte_t> tmp;
  const short int intValue =
    static_cast<short int>(PM_V_BAND_D_m) & 0x03FF;
  return shortToData(tmp, intValue);
}
std::vector<CAN::byte_t> OPLL::CAN_PM_I_BAND_D() const
{
  const std::vector<CAN::byte_t> tmp;
  const short int intValue =
    static_cast<short int>(PM_I_BAND_D_m) & 0x03FF;
  return shortToData(tmp, intValue);
}


std::vector<CAN::byte_t> OPLL::CAN_IF_LEVEL() const
{
  const std::vector<CAN::byte_t> tmp;
  const short int intValue =
//    static_cast<short int>(IF_LEVEL_m) & 0x03FF;
    static_cast<short int>(IF_LEVEL_m) & 0xFFFF;
  return shortToData(tmp, intValue);
}

std::vector<CAN::byte_t> OPLL::CAN_REF_LEVEL_125MHZ() const
{
  const std::vector<CAN::byte_t> tmp;
  const short int intValue =
    static_cast<short int>(REF_LEVEL_125MHZ_m) & 0x03FF;
  return shortToData(tmp, intValue);
}

std::vector<CAN::byte_t> OPLL::CAN_PIEZO_VOLTAGE() const
{
  const std::vector<CAN::byte_t> tmp;
  const short int intValue =
    static_cast<short int>(PIEZO_VOLTAGE_m) & 0x03FF;
  return shortToData(tmp, intValue);
}


std::vector<CAN::byte_t> OPLL::CAN_VCO_TUNING_V() const
{
  const std::vector<CAN::byte_t> tmp;
  const short int intValue =
    static_cast<short int>(VCO_TUNING_V_m) & 0x03FF;
  return shortToData(tmp, intValue);
}

std::vector<CAN::byte_t> OPLL::CAN_IF_VCTRL() const
{
  const std::vector<CAN::byte_t> tmp;
  const short int intValue =
    static_cast<short int>(IF_VCTRL_m) & 0x03FF;
  return shortToData(tmp, intValue);
}

/*
*/

