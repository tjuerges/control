// @(#) $Id$


#include <queue>
#include <iomanip>
#include <stdio.h>
#include "AMBUtil.h"
#include "ACUACASim.h"
#include "CANError.h"
#include "ACUACA.h"

using AMB::node_t;
using AMB::rca_t;
using namespace ACAacu;

std::vector<uint8_t> serial_number(8);

// -----------------------------------------------------------------------------

AMB::ACUACA::ACUACA(
	node_t node,
	const std::vector<CAN::byte_t>& serialNumber)
    : node_m(node),sn_m(serialNumber)
{
    std::cout << "Creating node " << node << ", s/n 0x";
    for (int i = 0; i < 8; i++)
    {
	std::cout << std::hex << std::setw(2) << std::setfill('0') << (int)serialNumber[i];
	serial_number[i] = serialNumber[i];
    }
    std::cout << ", ACA ACU device" << std::endl;

    std::string error;
    if ( ! validateNodeNumber(error,this->node())) {
	    throw ACUError("Unknown RCA in monitor command");
//	throw CAN::Error(error);
    }

    if ( ! validateSerialNumber(error,this->serialNumber())) {
	    throw ACUError("Unknown RCA in monitor command");
//	throw CAN::Error(error);
    }
}

// -----------------------------------------------------------------------------

AMB::ACUACA::~ACUACA()
{
    // Nothing
}

// -----------------------------------------------------------------------------

node_t AMB::ACUACA::node() const
{
    return node_m;
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::ACUACA::serialNumber() const
{
    return sn_m;
}

// =============================================================================
// -- int8 --------------------
static void
i8_to_data(std::vector<CAN::byte_t> &data, unsigned int pos, uint8_t v)
{
    if (data.size() <= pos && pos >= 8u) {
	throw CAN::Error("Data length error");
    }
    data[pos] = v;
}
static std::vector<CAN::byte_t>
i8_to_data(uint8_t v1, uint8_t v2, uint8_t v3, uint8_t v4)
{
    std::vector<CAN::byte_t> data(4);
    data[0] = v1; data[1] = v2; data[2] = v3; data[3] = v4;
    return data;
}
static std::vector<CAN::byte_t>
i8_to_data(uint8_t v1, uint8_t v2, uint8_t v3)
{
    std::vector<CAN::byte_t> data(3);
    data[0] = v1; data[1] = v2; data[2] = v3;
    return data;
}
static std::vector<CAN::byte_t>
i8_to_data(uint8_t v1, uint8_t v2)
{
    std::vector<CAN::byte_t> data(2);
    data[0] = v1; data[1] = v2;
    return data;
}
static std::vector<CAN::byte_t>
i8_to_data(uint8_t v)
{
    std::vector<CAN::byte_t> data(1);
    data[0] = v;
    return data;
}
// -- int16 --------------------
static void i16_to_data(std::vector<CAN::byte_t> &data, int pos, uint16_t v)
{
    if (data.size() < pos + 2u && pos + 2u >= 8u) {
	throw CAN::Error("Data length error");
    }
    for (int i = 1; i >= 0; v >>= 8) { data[pos + i--] = (CAN::byte_t) v; }
}
static std::vector<CAN::byte_t>
i16_to_data(uint16_t v1, uint16_t v2, uint16_t v3, uint16_t v4)
{
    std::vector<CAN::byte_t> data(8);
    for (int i = 1; i >= 0; v1 >>= 8) { data[i--] = (CAN::byte_t) v1; }
    for (int i = 3; i >= 2; v2 >>= 8) { data[i--] = (CAN::byte_t) v2; }
    for (int i = 5; i >= 4; v3 >>= 8) { data[i--] = (CAN::byte_t) v3; }
    for (int i = 7; i >= 6; v4 >>= 8) { data[i--] = (CAN::byte_t) v4; }
    return data;
}
static std::vector<CAN::byte_t>
i16_to_data(uint16_t v1, uint16_t v2, uint16_t v3)
{
    std::vector<CAN::byte_t> data(6);
    for (int i = 1; i >= 0; v1 >>= 8) { data[i--] = (CAN::byte_t) v1; }
    for (int i = 3; i >= 2; v2 >>= 8) { data[i--] = (CAN::byte_t) v2; }
    for (int i = 5; i >= 4; v3 >>= 8) { data[i--] = (CAN::byte_t) v3; }
    return data;
}
static std::vector<CAN::byte_t>
i16_to_data(uint16_t v1, uint16_t v2)
{
    std::vector<CAN::byte_t> data(4);
    for (int i = 1; i >= 0; v1 >>= 8) { data[i--] = (CAN::byte_t) v1; }
    for (int i = 3; i >= 2; v2 >>= 8) { data[i--] = (CAN::byte_t) v2; }
    return data;
}
static std::vector<CAN::byte_t> i16_to_data(uint16_t v1)
{
    std::vector<CAN::byte_t> data(2);
    for (int i = 1; i >= 0; v1 >>= 8) { data[i--] = (CAN::byte_t) v1; }
    return data;
}
// -- int32 --------------------
static void i32_to_data(std::vector<CAN::byte_t> &data, int pos, uint32_t v)
{
    if (data.size() < pos + 4u && pos + 4u >= 8u) {
	throw CAN::Error("Data length error");
    }
    for (int i = 3; i >= 0; v >>= 8) { data[pos + i--] = (CAN::byte_t) v; }
}
static std::vector<CAN::byte_t> i32_to_data(uint32_t v1, uint32_t v2)
{
    std::vector<CAN::byte_t> data(8);
    for (int i = 3; i >= 0; v1 >>= 8) { data[i--] = (CAN::byte_t) v1; }
    for (int i = 7; i >= 4; v2 >>= 8) { data[i--] = (CAN::byte_t) v2; }
    return data;
}
static std::vector<CAN::byte_t> i32_to_data(uint32_t v1)
{
    std::vector<CAN::byte_t> data(4);
    for (int i = 3; i >= 0; v1 >>= 8) { data[i--] = (CAN::byte_t) v1; }
    return data;
}
// -- double --------------------
static std::vector<CAN::byte_t> f64_to_data(double val)
{
    union {
	uint64_t v;
	double d;
    };
    d = val;
    std::vector<CAN::byte_t> data(8);

    // native little-endian double to CAN bytes
    for (int i = 7; i >= 0; v >>= 8) { data[i--] = (CAN::byte_t) v; }

    return data;
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::ACUACA::monitor(
    rca_t rca) const
{
    // empty CAN message
    const std::vector<CAN::byte_t> nostring;

    // these are dummies to allow calling ICD methods
    sim_m.set_errcode(0);
    sim_m.set_timestamp();

    // bump transaction count
    trans_num_m++;

    {
//    rca_t myrca = rca & 0x3FFFF;
//    if ((myrca != ACAacu::ACU_MODE_RSP) && 
//	(myrca != ACAacu::AZ_POSN_RSP) && 
//	(myrca != ACAacu::EL_POSN_RSP) && 
//	(myrca != ACAacu::GET_AZ_ENC) &&
//	(myrca != ACAacu::GET_EL_ENC))
//	printf("Received monitor command 0x%x\n", myrca);
    }

    rca = rca & 0x3ffff;    // rca is lower 18 bits

    switch (rca) {

    case 0:
	{
	std::vector<CAN::byte_t> tmps(8);
	for (int i = 0; i < 8; i++)
		tmps[i] = serial_number[i];
	return tmps;
	}

    case ACAacu::ACU_MODE_RSP:
	return sim_m.get_acu_mode_rsp();
    case ACAacu::ACU_TRK_MODE_RSP:
	{
	std::vector<CAN::byte_t> tmp(1);
	tmp[0] = sim_m.acu_trk_mode_rsp();
	return tmp;
	}
    case ACAacu::AZ_POSN_RSP:
	{
	int32_t pos_at_TE, pos_at_intm;
	sim_m.get_az_posn_rsp(pos_at_TE, pos_at_intm);
	return i32_to_data(pos_at_TE, pos_at_intm);
	}
    case ACAacu::EL_POSN_RSP:
	{
	int32_t pos_at_TE, pos_at_intm;
	sim_m.get_el_posn_rsp(pos_at_TE, pos_at_intm);
	return i32_to_data(pos_at_TE, pos_at_intm);
	}
    case ACAacu::GET_ACU_ERROR:
	{
	uint8_t  error;
	uint32_t rca;
	sim_m.get_acu_error(error, rca);

	if (error == 0) {
		std::vector<CAN::byte_t> tmps;
		return tmps;
	}
	std::vector<CAN::byte_t> tmps(5);
	i8_to_data(tmps, 0, error);
	i32_to_data(tmps,1, rca);
	return tmps;
	}

    case ACAacu::GET_AZ_TRAJ_CMD:
	{
	int32_t pos, vel;
	sim_m.get_az_traj_cmd(pos, vel);
	return i32_to_data(pos, vel);
	}
    case ACAacu::GET_AZ_BRAKE:
	return i8_to_data(sim_m.get_az_brake());
    case ACAacu::GET_AZ_ENC:
	return i32_to_data(sim_m.get_az_enc());
    case ACAacu::GET_AZ_MOTOR_CURRENTS:                         
	{
	struct az_motor_st st;
	sim_m.get_az_motor_currents(st);
	return i8_to_data(st.AZ_L1, st.AZ_L2, st.AZ_R1, st.AZ_R2);
	}
    case ACAacu::GET_AZ_MOTOR_TEMPS:
	{
	struct az_motor_st st;
	sim_m.get_az_motor_temps(st);
	return i8_to_data(st.AZ_L1, st.AZ_L2, st.AZ_R1, st.AZ_R2);
	}
    case ACAacu::GET_AZ_MOTOR_TORQUE:
	{
	struct az_motor_st st;
	sim_m.get_az_motor_torque(st);
	return i8_to_data(st.AZ_L1, st.AZ_L2, st.AZ_R1, st.AZ_R2);
	}

    case ACAacu::GET_AZ_SERVO_COEFF_0:
#define xxx(n) case ACAacu::GET_AZ_SERVO_COEFF_0 + n:
    xxx(1) xxx(2) xxx(3) xxx(4) xxx(5) xxx(6) xxx(7) xxx(8) xxx(9)
    xxx(10) xxx(11) xxx(12) xxx(13) xxx(14) xxx(15)
#undef xxx
	{
	int n = rca - ACAacu::GET_AZ_SERVO_COEFF_0;
	return f64_to_data(sim_m.get_az_servo_coeff(n));
	}

    case ACAacu::GET_AZ_STATUS:
	return sim_m.get_az_status();
    case ACAacu::GET_AZ_STATUS_2:
	return sim_m.get_az_status_2();
    case ACAacu::GET_AZ_ENCODER_OFFSET:
	return i32_to_data(sim_m.get_az_encoder_offset());
    case ACAacu::GET_AZ_AUX_MODE:
	return i8_to_data(sim_m.get_az_aux());
    case ACAacu::GET_AZ_RATEFDBK_MODE:
	return i8_to_data(sim_m.get_az_ratefdbk_mode());


    case ACAacu::GET_EL_TRAJ_CMD:
	{
	int32_t pos, vel;
	sim_m.get_el_traj_cmd(pos, vel);
	return i32_to_data(pos, vel);
	}
    case ACAacu::GET_EL_BRAKE:
	return i8_to_data(sim_m.get_el_brake());
    case ACAacu::GET_EL_ENC:
	return i32_to_data(sim_m.get_el_enc());
    case ACAacu::GET_EL_MOTOR_CURRENTS:                         
	{
	struct el_motor_st st;
	sim_m.get_el_motor_currents(st);
	return i8_to_data(st.EL_L, st.EL_R);
	}
    case ACAacu::GET_EL_MOTOR_TEMPS:                            
	{
	struct el_motor_st st;
	sim_m.get_el_motor_temps(st);
	return i8_to_data(st.EL_L, st.EL_R);
	}
    case ACAacu::GET_EL_MOTOR_TORQUE:
	{
	struct el_motor_st st;
	sim_m.get_el_motor_torque(st);
	return i8_to_data(st.EL_L, st.EL_R);
	}

    case ACAacu::GET_EL_SERVO_COEFF_0:
#define xxx(n) case ACAacu::GET_EL_SERVO_COEFF_0 + n:
    xxx(1) xxx(2) xxx(3) xxx(4) xxx(5) xxx(6) xxx(7) xxx(8) xxx(9)
    xxx(10) xxx(11) xxx(12) xxx(13) xxx(14) xxx(15)
#undef xxx
	{
	int n = rca - ACAacu::GET_EL_SERVO_COEFF_0;
	return f64_to_data(sim_m.get_el_servo_coeff(n));
	}

    case ACAacu::GET_EL_STATUS:
	return sim_m.get_el_status();
    case ACAacu::GET_EL_STATUS_2:
	return sim_m.get_el_status_2();
    case ACAacu::GET_EL_ENCODER_OFFSET:
	return i32_to_data(sim_m.get_el_encoder_offset());
    case ACAacu::GET_EL_AUX_MODE:
	return i8_to_data(sim_m.get_el_aux());
    case ACAacu::GET_EL_RATEFDBK_MODE:
	return i8_to_data(sim_m.get_el_ratefdbk_mode());

    case ACAacu::GET_SYSTEM_STATUS:
	return sim_m.get_system_status();
    case ACAacu::GET_SYSTEM_STATUS_2:
	return sim_m.get_system_status_2();

    case ACAacu::GET_PT_MODEL_COEFF_0:
#define xxx(n) case ACAacu::GET_PT_MODEL_COEFF_0 + n:
    xxx(1) xxx(2) xxx(3) xxx(4) xxx(5) xxx(6) xxx(7) xxx(8) xxx(9)
    xxx(10) xxx(11) xxx(12) xxx(13) xxx(14) xxx(15) xxx(16) xxx(17) xxx(18) xxx(19)
    xxx(20) xxx(21) xxx(22) xxx(23) xxx(24) xxx(25) xxx(26) xxx(27) xxx(28) xxx(29)
    xxx(30) xxx(31)
#undef xxx
	{
	int n = rca - ACAacu::GET_PT_MODEL_COEFF_0;
	return f64_to_data(sim_m.get_pt_model_coeff(n));
	}

    case ACAacu::GET_SHUTTER:
	return i8_to_data(sim_m.get_shutter());
    case ACAacu::GET_STOW_PIN:
	{
	uint8_t az, el;
	sim_m.get_stow_pin(az,el);
	return i8_to_data(az, el);
	}


    case ACAacu::SUBREF_MODE_RSP:
	return i8_to_data(sim_m.subref_mode_rsp());     
    case ACAacu::GET_SUBREF_ABS_POSN:
	{
	std::vector<int16_t> pos = sim_m.get_subref_abs_posn();
	return i16_to_data(pos[0], pos[1], pos[2]);
	}
    case ACAacu::GET_SUBREF_DELTA_POSN:
	{
	std::vector<int16_t> pos = sim_m.get_subref_delta_posn();
	return i16_to_data(pos[0], pos[1], pos[2]);
	}
    case ACAacu::GET_SUBREF_ROTATION:
	{
	std::vector<int16_t> pos = sim_m.get_subref_rotation();
	return i16_to_data(pos[0], pos[1], pos[2]);
	}
    case ACAacu::GET_SUBREF_ENCODER_POSN_1:
	{
	std::vector<int16_t> pos = sim_m.get_sr_encoder_posn_1();
	return i16_to_data(pos[0], pos[1], pos[2]);
	}
    case ACAacu::GET_SUBREF_ENCODER_POSN_2:
	{
	std::vector<int16_t> pos = sim_m.get_sr_encoder_posn_2();
	return i16_to_data(pos[0], pos[1], pos[2]);
	}
    case ACAacu::GET_SUBREF_LIMITS:
	return sim_m.get_subref_limits();
    case ACAacu::GET_SUBREF_STATUS:
	return sim_m.get_subref_status();

    case ACAacu::GET_SUBREF_PT_COEFF_0:
#define xxx(n) case ACAacu::GET_SUBREF_PT_COEFF_0 + n:
    xxx(1) xxx(2) xxx(3) xxx(4) xxx(5) xxx(6) xxx(7) xxx(8) xxx(9)
    xxx(10) xxx(11) xxx(12) xxx(13) xxx(14) xxx(15) xxx(16) xxx(17) xxx(18) xxx(19)
    xxx(20) xxx(21) xxx(22) xxx(23) xxx(24) xxx(25) xxx(26) xxx(27) xxx(28) xxx(29)
    xxx(30) xxx(31)
#undef xxx
	{
	int n = rca - ACAacu::GET_SUBREF_PT_COEFF_0;
	return f64_to_data(sim_m.get_sr_pt_coeff(n));
	}

    case ACAacu::GET_METR_MODE:
	return sim_m.get_metr_mode();
    case ACAacu::GET_METR_EQUIP_STATUS:
	return sim_m.get_metr_equip_status();     

    case ACAacu::GET_METR_DISPL_0:
#define xxx(n) case ACAacu::GET_METR_DISPL_0 + n:
    xxx(1) xxx(2) xxx(3) xxx(4) xxx(5) xxx(6) xxx(7) xxx(8) xxx(9)
    xxx(10) xxx(11) xxx(12) xxx(13) xxx(14) xxx(15) xxx(16)
    xxx(17) xxx(18) xxx(19) xxx(20) xxx(21) xxx(22) xxx(23)
#undef xxx
	{
	int n = rca - ACAacu::GET_METR_DISPL_0;
	return i32_to_data(sim_m.get_metr_displ(n));
	}

    case ACAacu::GET_METR_TEMPS_0:
#define xxx(n) case ACAacu::GET_METR_TEMPS_0 + n:
    xxx(1) xxx(2) xxx(3) xxx(4) xxx(5) xxx(6) xxx(7) xxx(8) xxx(9)
    xxx(10) xxx(11) xxx(12) xxx(13) xxx(14) xxx(15) xxx(16) xxx(17) xxx(18) xxx(19)
    xxx(20) xxx(21) xxx(22) xxx(23) xxx(24) xxx(25) xxx(26) xxx(27) xxx(28) xxx(29)
    xxx(30) xxx(31) xxx(32) xxx(33) xxx(34) xxx(35) xxx(36) xxx(37) xxx(38) xxx(39)
    xxx(40) xxx(41) xxx(42) xxx(43) xxx(44) xxx(45) xxx(46) xxx(47) xxx(48) xxx(49)
    xxx(50) xxx(51) xxx(52) xxx(53) xxx(54) xxx(55) xxx(56) xxx(57) xxx(58) xxx(59)
    xxx(60) xxx(61) xxx(62) xxx(63) xxx(64) xxx(65) xxx(66) xxx(67) xxx(68) xxx(69)
    xxx(70) xxx(71) xxx(72) xxx(73) xxx(74) xxx(75) xxx(76) xxx(77) xxx(78) xxx(79)
#undef xxx
	{
	int n = rca - ACAacu::GET_METR_TEMPS_0;
	std::vector<int16_t> val = sim_m.get_metr_temps(n);
	return i16_to_data(val[0], val[1], val[2], val[3]);
	}

    case ACAacu::GET_METR_TILT_0:
    case ACAacu::GET_METR_TILT_0 + 1:
    case ACAacu::GET_METR_TILT_0 + 2:
    case ACAacu::GET_METR_TILT_0 + 3:
    case ACAacu::GET_METR_TILT_0 + 4:
	{
	int n = rca - ACAacu::GET_METR_TILT_0;
	metr_tilt val;
	sim_m.get_metr_tilt(n, val);
	return i16_to_data(val.x, val.y, val.t);
	}

    case ACAacu::GET_METR_COEFF_0:
#define xxx(n) case ACAacu::GET_METR_COEFF_0 + n:
    xxx(1) xxx(2) xxx(3) xxx(4) xxx(5) xxx(6) xxx(7) xxx(8) xxx(9)
    xxx(10) xxx(11) xxx(12) xxx(13) xxx(14) xxx(15) xxx(16) xxx(17) xxx(18) xxx(19)
    xxx(20) xxx(21) xxx(22) xxx(23) xxx(24) xxx(25) xxx(26) xxx(27) xxx(28) xxx(29)
    xxx(30) xxx(31)
#undef xxx
	{
	int n = rca - ACAacu::GET_METR_COEFF_0;
	return f64_to_data(sim_m.get_metr_coeff(n));
	}

    case ACAacu::GET_METR_DELTAS:
	{
	int32_t az, el;
	sim_m.get_metr_deltas(az, el);
	return i32_to_data(az, el);
	}
    case ACAacu::GET_METR_DELTAPATH:
	return i32_to_data(sim_m.get_metr_deltapath());

    case ACAacu::GET_SYSTEM_ID:
	return get_sw_rev_level();
    case ACAacu::GET_SW_REV_LEVEL:
	return get_sw_rev_level();

    case ACAacu::GET_IP_ADDRESS:
	{
	uint32_t address, netmask;
	sim_m.get_ip_address(address,netmask);
	return i32_to_data(address,netmask);
	}
    case ACAacu::GET_IP_GATEWAY:
	{
	uint32_t gateway;
	sim_m.get_ip_gateway(gateway);
	return i32_to_data(gateway);
	}
    case ACAacu::GET_IDLE_STOW_TIME:
	return i16_to_data(sim_m.get_idle_stow_time());
    case ACAacu::GET_CAN_ERROR:
	return get_can_error();
    case ACAacu::GET_NUM_TRANS:
	return i32_to_data(AMB::ACUACA::get_trans_num());
    case ACAacu::GET_POWER_STATUS:                            
	return sim_m.get_power_status();
    case ACAacu::GET_AC_STATUS:                            
	return i8_to_data(sim_m.get_ac_status());
    case ACAacu::GET_FAN_STATUS:                            
	return sim_m.get_fan_status();
    case ACAacu::GET_UPS_OUTPUT_VOLTS_1:                            
	{
	std::vector<uint16_t> val = sim_m.get_ups_output_volts_1();
	return i16_to_data(val[0], val[1], val[2]);
	}
    case ACAacu::GET_UPS_OUTPUT_VOLTS_2:                            
	{
	std::vector<uint16_t> val = sim_m.get_ups_output_volts_2();
	return i16_to_data(val[0], val[1], val[2]);
	}
    case ACAacu::GET_UPS_OUTPUT_CURRENT_1:                            
	{
	std::vector<uint16_t> val = sim_m.get_ups_output_current_1();
	return i16_to_data(val[0], val[1], val[2]);
	}
    case ACAacu::GET_UPS_OUTPUT_CURRENT_2:                            
	{
	std::vector<uint16_t> val = sim_m.get_ups_output_current_2();
	return i16_to_data(val[0], val[1], val[2]);
	}
    case ACAacu::GET_ANTENNA_TEMPS:                            
	{
	antenna_temps val;
	sim_m.get_antenna_temps(val);
	return i16_to_data(val.receivercabin, val.pedestal);
	}

    case ACAacu::SELFTEST_RSP:                            
	{
	selftest_rsp_param val;
	sim_m.selftest_rsp(val);
	std::vector<CAN::byte_t> tmps(5);
	i8_to_data(tmps, 0, val.status);
	i16_to_data(tmps, 1, val.failed_test_latest);
	i16_to_data(tmps, 3, val.num_of_errors);
	return tmps;
	}
    case ACAacu::SELFTEST_ERR:                            
	{
	selftest_err_param val;
	sim_m.selftest_err(val);
	std::vector<CAN::byte_t> tmps(6);
	i16_to_data(tmps, 0, val.failed_test_index);
	i16_to_data(tmps, 2, val.iparam);
	return tmps;
	}

    default:
	std::cerr << "Switch does not match ACU any case" << std::endl;
	fprintf(stderr, "RCA: 0x%05x\n", rca);
	throw ACUError("Unknown RCA in monitor command");
    }
}

// =============================================================================

static
uint16_t data_to_i16(const std::vector<CAN::byte_t> &data, int pos)
{
    if (data.size() < pos+2u) {
	throw CAN::Error("Data wrong length");
    }
    uint16_t v = (uint8_t) data[pos++];
    v = (v << 8) | (uint8_t) data[pos];
    return v;
}
static
uint16_t data_to_i32(const std::vector<CAN::byte_t> &data, int pos)
{
    if (data.size() < pos+4u) {
	throw CAN::Error("Data wrong length");
    }
    uint32_t v = 0;
    for (int i = 0; i < 4; v <<= 8) {
	v |= (uint8_t) data[pos + i++];
    }
    return v;
}
static
double data_to_f64(const std::vector<CAN::byte_t> &data)
{
    if (data.size() < 8u) {
	throw CAN::Error("Data wrong length");
    }
    union {
	uint8_t v[8];
	double  d;
    };

    // CAN bytes to native little-endian double
    for (int i = 0; i < 8; i++) { v[7-i] = (uint8_t) data[i]; }

    return d;
}

// common error checking macros
#define check_datasize(nm,sz) \
    if (data.size() != sz) { throw ACUError(#nm " data length must be " #sz); }
#define check_1(nm) \
    if (data[0] != 1) { throw ACUError(#nm " data must be 0x01"); }
#define check_0or1(nm) \
    if (data[0] != 0 && data[0] != 1) { throw ACUError(#nm " data must be 0x00 or 0x01"); }

// -----------------------------------------------------------------------------

void AMB::ACUACA::control(
	rca_t rca,
	const std::vector<CAN::byte_t>& data)
{
    sim_m.set_errcode(0);
    sim_m.set_timestamp();


    // bump transaction count
    trans_num_m++;

    {
//    rca_t myrca = rca & 0x3FFFF;
//    if ((myrca != ACAacu::AZ_TRAJ_CMD) && 
//	(myrca != ACAacu::EL_TRAJ_CMD))
//	printf("Received control command 0x%x\n", myrca);
    }

    rca = rca & 0x3ffff;    // rca is lower 18 bits

    switch (rca) {

    case 0:
        break;

    case ACAacu::ACU_MODE_CMD:
	check_datasize(ACU_MODE_CMD, 1);
	sim_m.acu_mode_cmd(data[0]);
	break;
    case ACAacu::ACU_TRK_MODE_CMD:
	check_datasize(ACU_TRK_MODE_CMD, 1);
	sim_m.acu_trk_mode_cmd(data[0]);
	break;
    case ACAacu::AZ_TRAJ_CMD:
	{
	check_datasize(AZ_TRAJ_CMD, 8);
	int32_t position = data_to_i32(data, 0);
	int32_t velocity = data_to_i32(data, 2);
	sim_m.az_traj_cmd(position,velocity);
	}
	break;
    case ACAacu::EL_TRAJ_CMD:
	{
	check_datasize(EL_TRAJ_CMD, 8);
	int32_t position = data_to_i32(data, 0);
	int32_t velocity = data_to_i32(data, 2);
	sim_m.el_traj_cmd(position,velocity);
	}
	break;
    case ACAacu::CLEAR_FAULT_CMD:
	check_datasize(CLEAR_FAULT_CMD, 1);
	sim_m.clear_fault_cmd();
	break;
    case ACAacu::RESET_ACU_CMD:
	check_datasize(RESET_ACU_CMD, 1);
	sim_m.reset_acu_cmd();
	break;

    case ACAacu::SET_AZ_BRAKE:
	check_datasize(SET_AZ_BRAKE, 1);
	sim_m.set_az_brake(data[0]);
	break;

    case ACAacu::SET_AZ_SERVO_COEFF_0:
#define xxx(n) case ACAacu::SET_AZ_SERVO_COEFF_0 + n:
    xxx(1) xxx(2) xxx(3) xxx(4) xxx(5) xxx(6) xxx(7) xxx(8) xxx(9)
    xxx(10) xxx(11) xxx(12) xxx(13) xxx(14) xxx(15)
#undef xxx
	{
	check_datasize(SET_AZ_SERVO_COEFF_n, 8);
	int n = rca - ACAacu::SET_AZ_SERVO_COEFF_0;
	sim_m.set_az_servo_coeff(n, data_to_f64(data));
	}
	break;

    case ACAacu::SET_AZ_SERVO_DEFAULT:
	check_datasize(SET_AZ_SERVO_DEFAULT, 1);
	// data[0];
	sim_m.set_az_servo_default();
	break;

    case ACAacu::SET_AZ_AUX_MODE:
	check_datasize(SET_AZ_AUX_MODE, 1);
	sim_m.set_az_aux(data[0]);
	break;
    case ACAacu::SET_AZ_RATEFDBK_MODE:
	check_datasize(SET_AZ_RATEFDBK_MODE, 1);
	sim_m.set_az_ratefdbk_mode(data[0]);
	break;

    case ACAacu::SET_EL_BRAKE:
	check_datasize(SET_EL_BRAKE, 1);
	sim_m.set_el_brake(data[0]);
	break;	

    case ACAacu::SET_EL_SERVO_COEFF_0:
#define xxx(n) case ACAacu::SET_EL_SERVO_COEFF_0 + n:
    xxx(1) xxx(2) xxx(3) xxx(4) xxx(5) xxx(6) xxx(7) xxx(8) xxx(9)
    xxx(10) xxx(11) xxx(12) xxx(13) xxx(14) xxx(15)
#undef xxx
	{
	check_datasize(SET_EL_SERVO_COEFF_n, 8);
	int n = rca - ACAacu::SET_EL_SERVO_COEFF_0;
	sim_m.set_el_servo_coeff(n, data_to_f64(data));
	}
	break;

    case ACAacu::SET_EL_SERVO_DEFAULT:
	check_datasize(SET_EL_SERVO_DEFAULT, 1);
	    // data[0];
	sim_m.set_el_servo_default();
	break;

    case ACAacu::SET_EL_AUX_MODE:
	check_datasize(SET_EL_AUX_MODE, 1);
	sim_m.set_el_aux(data[0]);
	break;
    case ACAacu::SET_EL_RATEFDBK_MODE:
	check_datasize(SET_EL_RATEFDBK_MODE, 1);
	sim_m.set_el_ratefdbk_mode(data[0]);
	break;

    case ACAacu::SET_PT_MODEL_COEFF_0:
#define xxx(n) case ACAacu::SET_PT_MODEL_COEFF_0 + n:
    xxx(1) xxx(2) xxx(3) xxx(4) xxx(5) xxx(6) xxx(7) xxx(8) xxx(9)
    xxx(10) xxx(11) xxx(12) xxx(13) xxx(14) xxx(15) xxx(16) xxx(17) xxx(18) xxx(19)
    xxx(20) xxx(21) xxx(22) xxx(23) xxx(24) xxx(25) xxx(26) xxx(27) xxx(28) xxx(29)
    xxx(30) xxx(31)
#undef xxx
	{
	check_datasize(SET_PT_MODEL_COEFF_n, 8);
	int n = rca - ACAacu::SET_PT_MODEL_COEFF_0;
	sim_m.set_pt_model_coeff(n, data_to_f64(data));
	}
	break;

    case ACAacu::SET_PT_DEFAULT:
	check_datasize(SET_PT_DEFAULT, 1);
	// data[0];
	sim_m.set_pt_default();
	break;

    case ACAacu::SET_SHUTTER:
	check_datasize(SET_SHUTTER, 1);
	sim_m.set_shutter(data[0]);
	break;
    case ACAacu::SET_STOW_PIN:
	check_datasize(SET_STOW_PIN, 2);
	sim_m.set_stow_pin(data[0],data[1]);
	break;

    case ACAacu::SUBREF_MODE_CMD:
	check_datasize(SUBREF_MODE_CMD, 1);
	sim_m.subref_mode_cmd(data[0]);
	break;
    case ACAacu::SET_SUBREF_ABS_POSN:
	{
	check_datasize(SET_SUBREF_ABS_POSN, 6);
	std::vector<int16_t> posn(3);
	posn[0] = data_to_i16(data, 0);
	posn[1] = data_to_i16(data, 2);
	posn[2] = data_to_i16(data, 4);
	sim_m.set_subref_abs_posn(posn);
	}
	break;
    case ACAacu::SET_SUBREF_DELTA_POSN:
	{
	check_datasize(SET_SUBREF_DELTA_POSN, 6);
	std::vector<int16_t> posn(3);
	posn[0] = data_to_i16(data, 0);
	posn[1] = data_to_i16(data, 2);
	posn[2] = data_to_i16(data, 4);
	sim_m.set_subref_delta_posn(posn);
	}
	break;
    case ACAacu::SUBREF_DELTA_ZERO_CMD:
	check_datasize(SUBREF_DELTA_ZERO_CMD, 1);
	sim_m.subref_delta_zero_cmd(data[0]);
	break;
    case ACAacu::SET_SUBREF_ROTATION:
	{
	check_datasize(SET_SUBREF_ROTATION, 6);
	std::vector<int16_t> posn(3);
	posn[0] = data_to_i16(data, 0);
	posn[1] = data_to_i16(data, 2);
	posn[2] = data_to_i16(data, 4);
	sim_m.set_subref_rotation(posn);
	}
	break;

    case ACAacu::SET_SUBREF_PT_COEFF_0:
#define xxx(n) case ACAacu::SET_SUBREF_PT_COEFF_0 + n:
    xxx(1) xxx(2) xxx(3) xxx(4) xxx(5) xxx(6) xxx(7) xxx(8) xxx(9)
    xxx(10) xxx(11) xxx(12) xxx(13) xxx(14) xxx(15) xxx(16) xxx(17) xxx(18) xxx(19)
    xxx(20) xxx(21) xxx(22) xxx(23) xxx(24) xxx(25) xxx(26) xxx(27) xxx(28) xxx(29)
    xxx(30) xxx(31)
#undef xxx
	{
	check_datasize(SET_SUBREF_PT_COEFF_n, 8);
	int n = rca - ACAacu::SET_SUBREF_PT_COEFF_0;
	sim_m.set_sr_pt_coeff(n, data_to_f64(data));
	}
	break;

    case ACAacu::SET_SUBREF_PT_DEFAULT:
	check_datasize(SET_SUBREF_PT_DEFAULT, 1);
	// data[0];
	sim_m.set_sr_pt_default();
	break;

    case ACAacu::SET_METR_MODE:
	check_datasize(SET_METR_MODE, 4);
	sim_m.set_metr_mode(data);
	break;
    case ACAacu::SET_METR_COEFF_0:
#define xxx(n) case ACAacu::SET_METR_COEFF_0 + n:
    xxx(1) xxx(2) xxx(3) xxx(4) xxx(5) xxx(6) xxx(7) xxx(8) xxx(9)
    xxx(10) xxx(11) xxx(12) xxx(13) xxx(14) xxx(15) xxx(16) xxx(17) xxx(18) xxx(19)
    xxx(20) xxx(21) xxx(22) xxx(23) xxx(24) xxx(25) xxx(26) xxx(27) xxx(28) xxx(29)
    xxx(30) xxx(31)
#undef xxx
	{
	check_datasize(SET_METR_COEFF_n, 8);
	int n = rca - ACAacu::SET_METR_COEFF_0;
	sim_m.set_metr_coeff(n, data_to_f64(data));
	}
	break;

    case ACAacu::SET_METR_DEFAULT:
	check_datasize(SET_METR_DEFAULT, 1);
	// data[0];
	sim_m.set_metr_default();
	break;

    case ACAacu::SET_METR_CALIBRATION:
	check_datasize(SET_METR_CALIBRATION, 1);
	sim_m.set_metr_calibration();
	break;

    case ACAacu::SET_IP_ADDRESS:
	{
	check_datasize(SET_IP_ADDRESS, 8);
	uint32_t address = data_to_i32(data, 0);
	uint32_t netmask = data_to_i32(data, 2);
	sim_m.set_ip_address(address,netmask);
	}
	break;
    case ACAacu::SET_IP_GATEWAY:
	{
	check_datasize(SET_IP_GATEWAY, 4);
	uint32_t gateway = data_to_i32(data, 0);
	sim_m.set_ip_gateway(gateway);
	}
	break;
    case ACAacu::SET_IDLE_STOW_TIME:
	check_datasize(SET_IDLE_STOW_TIME, 2);
	sim_m.set_idle_stow_time(data_to_i16(data, 0));
	break;
    case ACAacu::SET_AC_TEMP:
	check_datasize(SET_AC_TEMP, 1);
	sim_m.set_ac_temp(data[0]);
	break;
    case ACAacu::SELFTEST_CMD:
	check_datasize(SELFTEST_CMD, 1);
	sim_m.selftest_cmd(data[0]);
	break;

    default:
	std::cerr << "Switch does not match ACU any case" << std::endl;
	fprintf(stderr, "RCA: 0x%05x\n", rca);
	throw ACUError("Unknown RCA in control command");
//	throw CAN::Error("rca NOT Implemented");
    }
}

#undef check_datasize

// =============================================================================

std::vector<CAN::byte_t> AMB::ACUACA::get_can_error() const
{
//    sim_m.set_errcode(0);
//    sim_m.set_timestamp();
    uint32_t level = sim_m.get_can_error();
    return i32_to_data(level);
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::ACUACA::get_protocol_rev_level() const
{
//    sim_m.set_errcode(0);
//    sim_m.set_timestamp();
    std::vector<uint8_t> level = sim_m.get_sw_rev_level();
    return i8_to_data(level[0], level[1], level[2]);
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::ACUACA::get_sw_rev_level() const
{
//    sim_m.set_errcode(0);
//    sim_m.set_timestamp();
    std::vector<int8_t> level = sim_m.get_system_id();
    return i8_to_data(level[0], level[1], level[2]);
}

// -----------------------------------------------------------------------------

uint32_t AMB::ACUACA::get_trans_num() const
{
    // sim_m.get_num_trans(); is NOT used
    return trans_num_m;
}

// -----------------------------------------------------------------------------
