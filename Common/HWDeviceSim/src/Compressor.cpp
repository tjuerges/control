// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include <iomanip>
#include "Compressor.h"
#include "AMBUtil.h"

// -----------------------------------------------------------------------------

AMB::Compressor::Compressor(
	node_t node,
	const std::vector<CAN::byte_t> &serialNumber)
    : node_m(node), sn_m(serialNumber), comp_state_m(0), fridge_state_m(0),
      trans_num_m(0)
{
    std::cout << "Creating node " << node << ", s/n 0x";
    for (int i = 0; i < 8; i++)
	std::cout << std::hex << std::setw(2) << std::setfill('0') << (int)serialNumber[i];
    std::cout << ", Compressor device" << std::endl;

    // TODO - validate node & sn
}

// -----------------------------------------------------------------------------

AMB::Compressor::~Compressor()
{
    // Nothing
}

// -----------------------------------------------------------------------------

AMB::node_t AMB::Compressor::node() const
{
    return node_m;
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::Compressor::serialNumber() const
{
    return sn_m;
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::Compressor::monitor(rca_t rca) const
{
    trans_num_m++;
    const std::vector<CAN::byte_t> tmp;

    switch (rca) {
    // Mandatory generic points
    case GET_CAN_ERROR:
	return get_can_error();
	break;
    case GET_PROTOCOL_REV_LEVEL:
	return get_protocol_rev_level();
	break;
 
 	case AMB::Device::GET_SW_REV_LEVEL:
		return get_software_rev_level();
		break;

	case AMB::Device::GET_HW_REV_LEVEL:
		return get_hardware_rev_level();
		break;

    case GET_TRANS_NUM:
	return longToData(tmp, get_trans_num());
	break;
    // Optional generic point
    case GET_AMBIENT_TEMPERATURE:
	return floatDS1820ToData(tmp, get_ambient_temperature());
	break;

    // Compressor specific points
    case GET_COMP_STATE:
	return charToData(tmp, get_comp_state());
	break;
    case GET_CONTROL_BOX_TEMP:
	return floatToData(tmp, get_control_box_temp(), 0.0, 500.0, 12);
	break;
    case GET_CROSSHEAD_CURRENT:
	return floatToData(tmp, get_crosshead_current(), 0.0, 5.0, 12);
	break;
    case GET_ELEC_CAGE_TEMP:
	return floatToData(tmp, get_elec_cage_temp(), 0.0, 500.0, 12);
	break;
    case GET_FAN_AIR_TEMP:
	return floatToData(tmp, get_fan_air_temp(), 0.0, 500.0, 12);
	break;
    case GET_FAN_MOTOR_TEMP:
	return floatToData(tmp, get_fan_motor_temp(), 0.0, 500.0, 12);
	break;
    case GET_FRIDGE_DRIVE_STATE:
	return charToData(tmp, get_fridge_drive_state());
	break;
    case GET_GM_HEAD_TEMP:
	return floatToData(tmp, get_gm_head_temp(), 0.0, 500.0, 12);
	break;
    case GET_GM_HTEX_TEMP:
	return floatToData(tmp, get_gm_htex_temp(), 0.0, 500.0, 12);
	break;
    case GET_GM_OIL_TEMP:
	return floatToData(tmp, get_gm_oil_temp(), 0.0, 500.0, 12);
	break;
    case GET_GM_OILHEX_TEMP:
	return floatToData(tmp, get_gm_oilhex_temp(), 0.0, 500.0, 12);
	break;
    case GET_GM_RETURN_PRESSURE:
	return floatToData(tmp, get_gm_return_pressure(), 0.0, 500.0, 12);
	break;
    case GET_GM_SUPPLY_PRESSURE:
	return floatToData(tmp, get_gm_supply_pressure(), 0.0, 500.0, 12);
	break;
    case GET_JT_HEAD_TEMP:
	return floatToData(tmp, get_jt_head_temp(), 0.0, 500.0, 12);
	break;
    case GET_JT_OIL_TEMP:
	return floatToData(tmp, get_jt_oil_temp(), 0.0, 500.0, 12);
	break;
    case GET_JT_OILHEX_TEMP:
	return floatToData(tmp, get_jt_oilhex_temp(), 0.0, 500.0, 12);
	break;
    case GET_JT_RETURN_PRESSURE:
	return floatToData(tmp, get_jt_return_pressure(), -14.6, 128.65, 12);
	break;
    case GET_RES_TANK_PRESSURE:
	return floatToData(tmp, get_res_tank_pressure(), 0, 500, 12);
	break;
    case GET_STATUS:
	return get_status();
	break;
    default:
	std::cerr << "Switch does not match any Compressor case" << std::endl;
	throw CompressorError("Unknown RCA in monitor command");
    }
}

// -----------------------------------------------------------------------------

void AMB::Compressor::control(rca_t rca, const std::vector<CAN::byte_t> &data)
{
    trans_num_m++;
    switch (rca) {
    case SET_COMP_STATE:
	{
	    if (data.size() != 1) {
		throw CompressorError("SET_COMP_STATE data length must be 1");
	    }
	    set_comp_state(data[0]);
	}
	break;
    case SET_FRIDGE_STATE:
	{
	    if (data.size() != 1) {
		throw CompressorError("SET_FRIDGE_DRIVER_STATE data length "
				      "must be 1");
	    }
	    set_fridge_state(data[0]);
	}
	break;
    default:
	throw CompressorError("Unknown RCA in control command");
    }
}

// -----------------------------------------------------------------------------

void AMB::Compressor::set_comp_state(CAN::byte_t cmd)
{
    if (cmd > 1) {
	throw CompressorError("set_comp_state illegal cmd, must be in [0..1]");
    }
    comp_state_m = cmd;
}

// -----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::Compressor::get_can_error() const
{
    return AMB::Device::get_can_error();
}

// -----------------------------------------------------------------------------
 std::vector<CAN::byte_t> AMB::Compressor::get_protocol_rev_level() const
{
    return AMB::Device::get_protocol_rev_level();
}

// -----------------------------------------------------------------------------
unsigned int AMB::Compressor::get_trans_num() const
{
    return trans_num_m;
}

// -----------------------------------------------------------------------------
float AMB::Compressor::get_ambient_temperature() const
{
    return 18.0;
}

// -----------------------------------------------------------------------------
CAN::byte_t AMB::Compressor::get_comp_state() const
{
    return comp_state_m;
}

// -----------------------------------------------------------------------------
float AMB::Compressor::get_control_box_temp() const
{
    return get_comp_state() ? 40.0 : 20.0;
}

// -----------------------------------------------------------------------------
float AMB::Compressor::get_crosshead_current() const
{
    if (get_fridge_drive_state()) {
	return 2.5;
    } else {
	return 0.0;
    }
}

// -----------------------------------------------------------------------------
float AMB::Compressor::get_elec_cage_temp() const
{
    return get_comp_state() ? 41.0 : 21.0;
}

// -----------------------------------------------------------------------------
float AMB::Compressor::get_fan_air_temp() const
{
    return get_comp_state() ? 42.0 : 22.0;
}

// -----------------------------------------------------------------------------
float AMB::Compressor::get_fan_motor_temp() const
{
    return get_comp_state() ? 43.0 : 23.0;
}

// -----------------------------------------------------------------------------
CAN::byte_t AMB::Compressor::get_fridge_drive_state() const
{
    return fridge_state_m;
}

// -----------------------------------------------------------------------------
float AMB::Compressor::get_gm_head_temp() const
{
    return get_comp_state() ? 44.0 : 24.0;
}

// -----------------------------------------------------------------------------
float AMB::Compressor::get_gm_htex_temp() const
{
    return get_comp_state() ? 45.0 : 25.0;
}

// -----------------------------------------------------------------------------
float AMB::Compressor::get_gm_oil_temp() const
{
    return get_comp_state() ? 46.0 : 26.0;
}

// -----------------------------------------------------------------------------
float AMB::Compressor::get_gm_oilhex_temp() const
{
    return get_comp_state() ? 47.0 : 27.0;
}

// -----------------------------------------------------------------------------
float AMB::Compressor::get_gm_return_pressure() const
{
    return get_comp_state() ? 280.0 : 200.0;
}

// -----------------------------------------------------------------------------
float AMB::Compressor::get_gm_supply_pressure() const
{
    return get_comp_state() ? 280.0 : 200.0;
}

// -----------------------------------------------------------------------------
float AMB::Compressor::get_jt_head_temp() const
{
    return get_comp_state() ? 48.0 : 28.0;
}

// -----------------------------------------------------------------------------
float AMB::Compressor::get_jt_oil_temp() const
{
    return get_comp_state() ? 49.0 : 29.0;
}

// -----------------------------------------------------------------------------
float AMB::Compressor::get_jt_oilhex_temp() const
{
    return get_comp_state() ? 50.0 : 30.0;
}

// -----------------------------------------------------------------------------
float AMB::Compressor::get_jt_return_pressure() const
{
    return get_comp_state() ? -10.5 : 100.0;
}

// -----------------------------------------------------------------------------
float AMB::Compressor::get_res_tank_pressure() const
{
    return get_comp_state() ? 120.0 : 80.0;
}

// -----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::Compressor::get_status() const
{
    CAN::byte_t byte0 = 0;
    CAN::byte_t byte1 = 0;

    if (get_comp_state()) {
	// Compressor on
	byte1 |= 0x01;
	byte1 |= 0x02;
    }
    if (get_fridge_drive_state()) {
	// Fridge on
	byte1 |= 0x04;
    }

    std::vector<CAN::byte_t> tmp(2);
    tmp[0] = byte0;
    tmp[1] = byte1;
    return tmp;
}

// -----------------------------------------------------------------------------

void AMB::Compressor::set_fridge_state(CAN::byte_t cmd)
{
    if (cmd > 1) {
	throw CompressorError("set_comp_state illegal cmd, must be in [0..1]");
    }
    fridge_state_m = cmd;
}
