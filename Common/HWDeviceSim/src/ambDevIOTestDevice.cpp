// @(#) $Id$
//
// Copyright (C) 2002
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include <iomanip>
#include "ambDevIOTestDevice.h"
#include "AMBUtil.h"
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>

// ----------------------------------------------------------------------------
AMB::AmbDevIOTestDevice::AmbDevIOTestDevice(node_t node, 
					    const std::vector<CAN::byte_t> &serialNumber)
  : m_node(node), 
    m_sn(serialNumber),
    m_trans_num(0),
    m_errorCode(1),
    BOOL_PROP_m(0),
    DOUBLE_PROP_m(0),
    DOUBLE_SEQ_PROP1_m(0),
    DOUBLE_SEQ_PROP2_m(0),
    SIGNED_DOUBLE_SEQ_PROP1_m(0),
    SIGNED_DOUBLE_SEQ_PROP2_m(0),
    LONG_PROP_m(0),
    LONGLONG_PROP_m(0),
    LONG_PARAMETRIC_m(0),
    ULONGLONG_EXP_m(0),
    ENUMPROP_m(0)
{
  std::cout << "Creating node " << node << " ,s/n 0x";
  for (int i = 0; i < 8; i++) {
    std::cout << std::hex << std::setw(2) << std::setfill('0') 
	      << static_cast<int>(serialNumber[i]);
  }
  std::cout << ", AmbDevIOTestDevice device" << std::endl;
  for (int idx = 0; idx < 8; idx ++) {
    LAST_PHASE_COMMAND_m[idx] = 0;
  }

  BOOL_PROP_m.reserve(1);
  BOOL_PROP_m[0] = 1;
  
  ENUMPROP_m.reserve(1);
  ENUMPROP_m[0] = 1;


  DOUBLE_PROP_m.reserve(2);
  DOUBLE_PROP_m[0] = 1;
  DOUBLE_PROP_m[1] = 1;

  DOUBLE_SEQ_PROP1_m.reserve(8);
  DOUBLE_SEQ_PROP1_m[0] =0; DOUBLE_SEQ_PROP1_m[1] =0;
  DOUBLE_SEQ_PROP1_m[2] =0; DOUBLE_SEQ_PROP1_m[3] =0;
  DOUBLE_SEQ_PROP1_m[4] =0; DOUBLE_SEQ_PROP1_m[5] =0;
  DOUBLE_SEQ_PROP1_m[6] =0; DOUBLE_SEQ_PROP1_m[7] =0;

  DOUBLE_SEQ_PROP2_m.reserve(6);
  DOUBLE_SEQ_PROP2_m[0] =0; DOUBLE_SEQ_PROP2_m[1] =0;
  DOUBLE_SEQ_PROP2_m[2] =0; DOUBLE_SEQ_PROP2_m[3] =0;
  DOUBLE_SEQ_PROP2_m[4] =0; DOUBLE_SEQ_PROP2_m[5] =0;

  SIGNED_DOUBLE_SEQ_PROP1_m.reserve(8);
  SIGNED_DOUBLE_SEQ_PROP1_m[0] =0; SIGNED_DOUBLE_SEQ_PROP1_m[1] =0;
  SIGNED_DOUBLE_SEQ_PROP1_m[2] =0; SIGNED_DOUBLE_SEQ_PROP1_m[3] =0;
  SIGNED_DOUBLE_SEQ_PROP1_m[4] =0; SIGNED_DOUBLE_SEQ_PROP1_m[5] =0;
  SIGNED_DOUBLE_SEQ_PROP1_m[6] =0; SIGNED_DOUBLE_SEQ_PROP1_m[7] =0;
  
  SIGNED_DOUBLE_SEQ_PROP2_m.reserve(6);
  SIGNED_DOUBLE_SEQ_PROP2_m[0] =0; SIGNED_DOUBLE_SEQ_PROP2_m[1] =0;
  SIGNED_DOUBLE_SEQ_PROP2_m[2] =0; SIGNED_DOUBLE_SEQ_PROP2_m[3] =0;
  SIGNED_DOUBLE_SEQ_PROP2_m[4] =0; SIGNED_DOUBLE_SEQ_PROP2_m[5] =0;
  
  LONG_PROP_m.reserve(3);
  LONG_PROP_m[0] = 1;
  LONG_PROP_m[1] = 1;
  LONG_PROP_m[2] = 1;

  LONGLONG_PROP_m.reserve(8);
  LONGLONG_PROP_m[0] = 0; LONGLONG_PROP_m[1] = 0;
  LONGLONG_PROP_m[2] = 0; LONGLONG_PROP_m[3] = 0;
  LONGLONG_PROP_m[4] = 0; LONGLONG_PROP_m[5] = 0;
  LONGLONG_PROP_m[6] = 0; LONGLONG_PROP_m[7] = 0;
  
  LONG_PARAMETRIC_m.reserve(4);
  LONG_PARAMETRIC_m[0] = 0; LONG_PARAMETRIC_m[1] = 0;
  LONG_PARAMETRIC_m[2] = 0; LONG_PARAMETRIC_m[3] = 0;

  ULONGLONG_EXP_m.reserve(1);
  ULONGLONG_EXP_m[0] = 1;
}

// ----------------------------------------------------------------------------
AMB::AmbDevIOTestDevice::~AmbDevIOTestDevice() {}

// ----------------------------------------------------------------------------
AMB::node_t AMB::AmbDevIOTestDevice::node() const {
  return m_node;
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::AmbDevIOTestDevice::serialNumber() const {
  return m_sn;
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::AmbDevIOTestDevice::monitor(rca_t rca) const {
  m_trans_num++;
  const std::vector<CAN::byte_t> tmp;
  //std::vector<CAN::byte_t> retVal;

  switch (rca) {
    // Mandatory generic points
  case GET_SERIAL_NUMBER:
    return serialNumber();
    break;
  case GET_CAN_ERROR:
    return get_can_error();
    break;
  case GET_PROTOCOL_REV_LEVEL:
    return get_protocol_rev_level();
    break;
	case AMB::Device::GET_SW_REV_LEVEL:
		return get_software_rev_level();
		break;
	case AMB::Device::GET_HW_REV_LEVEL:
		return get_hardware_rev_level();
		break;
  case GET_TRANS_NUM:
    return longToData(tmp, get_trans_num());
    break;
    // Optional generic point
  case GET_AMBIENT_TEMPERATURE:
    return floatDS1820ToData(tmp, get_ambient_temperature());
    break;
    
    // AmbDevIOTestDevice specific points
  case BOOL_PROP_MON:
    return BOOL_PROP_m;
    break;
  case DOUBLE_PROP_MON:
    return DOUBLE_PROP_m;
    break;
  case SIGNED_DOUBLE_PROP_MON:
    return SIGNED_DOUBLE_PROP_m;
    break;
  case DOUBLE_SEQ_PROP_MON1:
    return DOUBLE_SEQ_PROP1_m;
    break;
  case DOUBLE_SEQ_PROP_MON2:        
    return  DOUBLE_SEQ_PROP2_m;
    break;
  case SIGNED_DOUBLE_SEQ_PROP_MON1:
    return  SIGNED_DOUBLE_SEQ_PROP1_m;
    break;
  case SIGNED_DOUBLE_SEQ_PROP_MON2:
    return  SIGNED_DOUBLE_SEQ_PROP2_m;
    break;
  case LONG_PROP_MON:
    return LONG_PROP_m;
    break;
  case LONGLONG_PROP_MON:
    return LONGLONG_PROP_m;
    break;
  case LONG_PARAMETRIC_MON:
    return  LONG_PARAMETRIC_m;
    break;
  case ULONGLONG_EXP_MON:
    return  ULONGLONG_EXP_m;
    break;
  case ENUM_PROP_MON:
    return  ENUMPROP_m;
  default:
    throw AmbDevIOTestDeviceError("Unknown RCA in monitor command");
  }
}

// ----------------------------------------------------------------------------
void AMB::AmbDevIOTestDevice::control(rca_t rca, const std::vector<CAN::byte_t> &data) {
  m_trans_num++;
  switch (rca) {
  case BOOL_PROP_CMD:
    checkSize(data, 1, "Bool Prop");
    BOOL_PROP_m = data;
    /* In order to test that the rw is perfoming correctly the written
       value must be different from the read value
    */
    if (BOOL_PROP_m[0]) 
      BOOL_PROP_m[0] = 0;
    else
      BOOL_PROP_m[0] = 1;
    break;
  case DOUBLE_PROP_CMD:
    checkSize(data,2, "Double Prop");
    DOUBLE_PROP_m = data;
    break;
  case SIGNED_DOUBLE_PROP_CMD:
    checkSize(data,4, "Double Prop");
    SIGNED_DOUBLE_PROP_m = data;
    break;
  case DOUBLE_SEQ_PROP_CMD1:
    checkSize(data,8, "Double Sequence Prop1");
     DOUBLE_SEQ_PROP1_m=data;
    break;
  case DOUBLE_SEQ_PROP_CMD2:
    checkSize(data,6, "Double Sequence Prop2");
    DOUBLE_SEQ_PROP2_m=data;
    break;
  case SIGNED_DOUBLE_SEQ_PROP_CMD1:
    checkSize(data,8, "Signed Double Sequence Prop1");
    SIGNED_DOUBLE_SEQ_PROP1_m=data;
    break;
  case SIGNED_DOUBLE_SEQ_PROP_CMD2:
    checkSize(data,6, "Signed Double Sequence Prop2");
    SIGNED_DOUBLE_SEQ_PROP2_m=data;
    break;
  case LONG_PROP_CMD:
    checkSize(data, 3, "Long Property");
    LONG_PROP_m=data;
    LONG_PROP_m[0]++;
    break;
  case LONGLONG_PROP_CMD:
    checkSize(data, 8, "Long Property");
    LONGLONG_PROP_m=data;
    LONGLONG_PROP_m[7]+=0x1F;
    break;
  case LONG_PARAMETRIC_CMD:
    checkSize(data,4, "Long Parametric Property");
    LONG_PARAMETRIC_m=data;
    break;
  case ULONGLONG_EXP_CMD:
    checkSize(data,1, "Unsigned Long Long Exponential Property");
    ULONGLONG_EXP_m=data;
    break;
  case ENUM_PROP_CMD:
    checkSize(data,1,"Enumerated Property CMD");
    ENUMPROP_m=data;
    ENUMPROP_m[0] += 2;
    if (ENUMPROP_m[0] > 5)
      ENUMPROP_m[0] = 1;
    break;
  default:
    throw AmbDevIOTestDeviceError("Unknown RCA in control command module AmbDevIOTestDevice");
  }
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::AmbDevIOTestDevice::get_can_error() const {
  return AMB::Device::get_can_error();
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::AmbDevIOTestDevice::get_protocol_rev_level() const {
  return AMB::Device::get_protocol_rev_level();
}

// ----------------------------------------------------------------------------
unsigned int AMB::AmbDevIOTestDevice::get_trans_num() const {
  return m_trans_num;
}

// ----------------------------------------------------------------------------
double AMB::AmbDevIOTestDevice::get_ambient_temperature() const {
  return 22.4;
}

// ----------------------------------------------------------------------------
void AMB::AmbDevIOTestDevice::checkSize(const std::vector<CAN::byte_t> &data, int size,
			  const char *pExceptionMsg) {
  if( static_cast<int>(data.size()) != size ) {
    char exceptionMsg[200];
    sprintf(exceptionMsg,"%s data length is %d must be %d",pExceptionMsg,
	    data.size(), size);
    throw AmbDevIOTestDeviceError(exceptionMsg);
  }
}

