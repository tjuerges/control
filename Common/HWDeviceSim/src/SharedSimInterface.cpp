/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2005 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* rhiriart  2005-09-20  created 
*/


#include <acstimeTimeUtil.h>
#include <SharedSimInterface.h>

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#define PI 3.1415926535897931

/*----------------------------------------------------------------------
 * Public interface
 *----------------------------------------------------------------------*/

/**
 * Constructor.
 */
SharedSimInterface::SharedSimInterface(int argc, char *argv[])
{
  bool useSharedSimulator = false;
  int antenna = -1;

  // check command line arguments
  int i = 1;
  while (i < argc) {
    std::string arg = argv[i];
    if (arg == "-sharedsim") {
      useSharedSimulator = true;
      ACS_SHORT_LOG((LM_INFO, "Using TELCAL SharedSimulator."));
    }
    else if (arg == "-antenna") {
      if (i == argc) {
	std::cout << "Error, missing antenna number" << std::endl;
	show_usage();
	break;
      }
      arg = argv[++i];
      antenna = atoi(arg.c_str());

      char tmp[80];
      sprintf(tmp, "Antenna number: %d", antennaNumber);
      ACS_SHORT_LOG((LM_INFO, tmp));
    }
    else {
      std::cout << "Unrecognized parameter: " << arg << std::endl;
      show_usage();
    }
    i++;
  }
  if (antenna < 0)
    antenna = 0;
  if (!useSharedSimulator)
    return;

  // initialize the ACS client interface
  if (client.init(argc, argv) == 0) {
    ACS_SHORT_LOG((LM_INFO, "Cannot init client"));
    return;
  }
  else {
    client.login();
  }

  SHARED_SIMULATOR::SharedSimulator_var ss;
  ss = SHARED_SIMULATOR::SharedSimulator::_nil();
  try {
    // Get a reference to the Shared Simulator component
    ACS_SHORT_LOG((LM_INFO, "Getting component: SharedSimulator"));
    ss = client.get_object<SHARED_SIMULATOR::SharedSimulator>("SIMULATOR", 0, true);
    if (CORBA::is_nil(simulator.in()) == true) {
      ACS_SHORT_LOG((LM_INFO, "Failed to get reference to SharedSimulator component."));
      return;
    }
  }
  catch(...) {
    ACS_SHORT_LOG((LM_ERROR, "Fatal error in SharedSimInterface constructor!"));
    return;
  }

  SharedSimWrapper(antenna, ss);
}

/**
 * Destructor.
 */
SharedSimInterface::~SharedSimInterface() {
  try {

    if (!CORBA::is_nil(simulator.in())) {
    //   simulator->off();

      // Release the Shared Simulator component
      ACS_SHORT_LOG((LM_INFO, "Releasing SharedSimulator component."));
      client.manager()->release_component(client.handle(), "SIMULATOR");
      client.logout();
    }
  }
  catch(...) {
    ACS_SHORT_LOG((LM_ERROR, "Fatal error in SharedSimInterface destructor!"));   
  }

}

/** 
 * Print a message to stdout showing the CLI parameters accepted. 
 */
void SharedSimInterface::show_usage() const {
  std::cout << "Command Line Arguments:" << std::endl;
  std::cout << "-sharedsim\tUse SharedSimulator" << std::endl;
  std::cout << "-antenna <number>\tAntenna number" << std::endl;
}

/*___oOo___*/
