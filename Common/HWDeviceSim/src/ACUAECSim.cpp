// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include <queue>
#include <iomanip>
#include <sys/time.h>
#include <stdio.h>
#include "ACUAECSim.h"
#include "ACUAECDrive.h"

const double bitsPerDegree = ((double)0x40000000 / 180.0);
const double degreesPerBit = (180.0 / (double)0x40000000);
const unsigned short IST_DEFAULT = 5 * 60;    // idle_stow_time default


/*
 * ACU error stack (duplicates operation of ACU monitor function)
 */
class ACUErrorStack
{
  public:
    // structure to hold error code to be assembled into the byte sequence
    typedef struct error_code {
        unsigned char error;
        unsigned long rca;
    } ERROR_CODE;

    void push_error(unsigned char error,unsigned long address)
    {
        /*
         * byte 0 is the error status, and then 4 bytes for the RCA
         * 5 bytes total
         */
        ERROR_CODE ec;
        ec.error = error;
        ec.rca = address;
        m_es.push(ec);
    }
    ERROR_CODE pop_error()
    {
        ERROR_CODE ec = { 0, 0 };
        if (m_es.empty())
            return ec;
    
        ec = m_es.front();
        m_es.pop();
        return ec;
    }
private:
    std::queue<ERROR_CODE> m_es;
};

ACUErrorStack acu_errs;
int acu_errorcode = 0;

// -----------------------------------------------------------------------------

ACUAECSim::ACUAECSim()
    : ACUAECbase(),
      idle_stow_time_m(IST_DEFAULT),
      shutter_m(2)
{
    drive_m = new ACUAECDrive();
}

// -----------------------------------------------------------------------------

ACUAECSim::~ACUAECSim()
{
    if (drive_m)
	delete drive_m;
}

// -----------------------------------------------------------------------------

unsigned long long ACUAECSim::getTimeStmp() const
{
const unsigned long long int timeFor1970 = 122192928000000000ULL;
    struct timeval tv;
    gettimeofday(&tv,0);
    return timeFor1970 + tv.tv_sec * 10000000LL + tv.tv_usec * 10LL;
}

////////////////////////////////////////////////////////////////////////////////
////                                                                        ////
////                            Monitor Points                              ////
////                                                                        ////
////////////////////////////////////////////////////////////////////////////////
      
// -----------------------------------------------------------------------------

std::vector<char> ACUAECSim::get_acu_mode_rsp(
	int& errcode,
	unsigned long long& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    return drive_m->getACUMode();
}

// -----------------------------------------------------------------------------

unsigned long long ACUAECSim::get_acu_error(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    /*
     * This is a funny value, so arrange the data in the first five bytes
     * with byte 0 as the error, and bytes 1-4 as the rca.  The rca is in
     * host byte order.
     */
    unsigned long long tmp;
    ACUErrorStack::ERROR_CODE ec = acu_errs.pop_error();
    tmp = ec.error;
    unsigned char* ptr = (unsigned char*)&tmp;
    memcpy(ptr + 1,&ec.rca,4);
    return tmp;
}

// -----------------------------------------------------------------------------

unsigned long long ACUAECSim::get_az_posn_rsp(
	int& errcode,
	unsigned long long& timestamp) const
{
    double pos;
    double vel;
    long position;

    errcode = 0;

    // the acu simulator does not have the position for the time between
    // timing events.
    drive_m->getAzPsn(pos,vel);
    position = (long)(pos * (double)bitsPerDegree);
    unsigned long long rc = (unsigned long long)position;
    rc = rc << 32 | ((unsigned long long)position & 0xffffffff);
    timestamp = getTimeStmp();
    return rc;
}

// -----------------------------------------------------------------------------

unsigned long long ACUAECSim::get_el_posn_rsp(
	int& errcode,
	unsigned long long& timestamp) const
{
    double pos;
    double vel;
    long position;

    errcode = 0;

    // the acu simulator does not have the position for the time between
    // timing events.
    drive_m->getElPsn(pos,vel);
    position = (long)(pos * bitsPerDegree);
    unsigned long long rc = (unsigned long long) position;
    rc = rc << 32 | ((unsigned long long)position & 0xffffffff);
    timestamp = getTimeStmp();
    return rc;
}

// -----------------------------------------------------------------------------

long ACUAECSim::get_az_enc(
	int& errcode,
	unsigned long long& timestamp) const
{
    // should remove pointing model from _RSP if in autonomous mode
    // unless Brian is wrong and they are the same
    return get_az_posn_rsp(errcode,timestamp);  // if Brian is wrong!
}

// -----------------------------------------------------------------------------

void ACUAECSim::get_el_enc(
	std::vector<long>& positions,
	int& errcode,
	unsigned long long& timestamp) const
{
    positions[0] = get_el_posn_rsp(errcode,timestamp);
    positions[1] = get_el_posn_rsp(errcode,timestamp);
}

// -----------------------------------------------------------------------------

std::vector<short> ACUAECSim::get_az_motor_currents(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    std::vector<short> current(2);
    current[0] = 0x10;
    current[1] = 0x20;
    return current;
}

// -----------------------------------------------------------------------------

std::vector<short> ACUAECSim::get_az_motor_temps(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    std::vector<short> temp(2);
    temp[0] = 0x30;
    temp[1] = 0x40;
    return temp;
}

// -----------------------------------------------------------------------------

std::vector<short> ACUAECSim::get_az_motor_torques(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    std::vector<short> temp(2);
    temp[0] = 0x40;
    temp[1] = 0x50;
    return temp;
}

// -----------------------------------------------------------------------------

short ACUAECSim::get_el_motor_currents(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    return 0x6070;
}

// -----------------------------------------------------------------------------

short ACUAECSim::get_el_motor_temps(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    return 0x7080;
}

// -----------------------------------------------------------------------------

short ACUAECSim::get_el_motor_torques(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    return 0x8090;
}

// -----------------------------------------------------------------------------

unsigned char ACUAECSim::get_az_brake(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    return (unsigned char)drive_m->Az.get_brake();
}

// -----------------------------------------------------------------------------

unsigned char ACUAECSim::get_el_brake(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    return (unsigned char)drive_m->El.get_brake();
}


// -----------------------------------------------------------------------------

unsigned char ACUAECSim::get_az_motor(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    return az_motor_m;
}

// -----------------------------------------------------------------------------

unsigned char ACUAECSim::get_el_motor(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    return el_motor_m;
}

// -----------------------------------------------------------------------------

double ACUAECSim::get_az_servo_coeff(
	long n,
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    return drive_m->Az.servo_coefficients[n];
}

// -----------------------------------------------------------------------------

double ACUAECSim::get_el_servo_coeff(
	long n,
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    return drive_m->El.servo_coefficients[n];
}

// -----------------------------------------------------------------------------

unsigned long ACUAECSim::get_az_motion_lim(
	int& errcode,
	unsigned long long int& timestamp) const
{
    unsigned long status;
    errcode = 0;
    status = az_motion_limit_m[0] + (az_motion_limit_m[1] << 16);
    timestamp = getTimeStmp();
    return status;
}

// -----------------------------------------------------------------------------

unsigned long ACUAECSim::get_el_motion_lim(
	int& errcode,
	unsigned long long int& timestamp) const
{
    unsigned long status;
    errcode = 0;
    status = el_motion_limit_m[0] + (el_motion_limit_m[1] << 16);
    timestamp = getTimeStmp();
    return status;
}

// -----------------------------------------------------------------------------

unsigned long long ACUAECSim::get_az_status(
	int& errcode,
	unsigned long long int& timestamp) const
{
    unsigned long long status;
    errcode = 0;
    drive_m->getAzStatus(status);
    timestamp = getTimeStmp();
    return status;
}

// -----------------------------------------------------------------------------

unsigned long long ACUAECSim::get_el_status(
	int& errcode,
	unsigned long long int& timestamp) const
{
    unsigned long long status;
    errcode = 0;
    drive_m->getElStatus(status);
    timestamp = getTimeStmp();
    return status;
}

// -----------------------------------------------------------------------------

unsigned short ACUAECSim::get_idle_stow_time(
	int& errcode,
	unsigned long long& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    return idle_stow_time_m;
}

// -----------------------------------------------------------------------------

unsigned char ACUAECSim::get_shutter(
	int& errcode,
	unsigned long long& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    return shutter_m;
}

// -----------------------------------------------------------------------------

unsigned short ACUAECSim::get_stow_pin(
	int& errcode,
	unsigned long long int& timestamp) const
{
    unsigned short pins;
    unsigned char* ptr = (unsigned char*)&pins;
    errcode = 0;
    timestamp = getTimeStmp();
    ptr[0] = drive_m->Az.get_stow_pin();
    ptr[1] = drive_m->El.get_stow_pin();
    return pins;
}

// -----------------------------------------------------------------------------

unsigned long ACUAECSim::get_system_status(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    return 0;       // all clear is good
}

// -----------------------------------------------------------------------------

unsigned long ACUAECSim::get_ip_address(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    return 0x01020304;  // should be 1:2:3:4
}

// -----------------------------------------------------------------------------

double ACUAECSim::get_pt_model_coeff(
	long n,
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    return pt_model_coeff_n[n];
}

// -----------------------------------------------------------------------------

std::vector<short> ACUAECSim::get_subref_abs_posn(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    std::vector<short> temp(3);
    temp[0] = subref_abs_posn_m[0];
    temp[1] = subref_abs_posn_m[1];
    temp[2] = subref_abs_posn_m[2];
    return temp;
}

// -----------------------------------------------------------------------------

std::vector<short> ACUAECSim::get_subref_delta_posn(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    std::vector<short> temp(3);
    temp[0] = subref_delta_posn_m[0];
    temp[1] = subref_delta_posn_m[1];
    temp[2] = subref_delta_posn_m[2];
    return temp;
}

// -----------------------------------------------------------------------------

unsigned long ACUAECSim::get_subref_limits(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    return 0x01020304;  // should be 1:2:3:4
}

// -----------------------------------------------------------------------------

unsigned long ACUAECSim::get_subref_status(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    return 0x05060708;
}

// -----------------------------------------------------------------------------

unsigned char ACUAECSim::get_metr_mode(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    return metr_mode_m;
}

// -----------------------------------------------------------------------------

unsigned short ACUAECSim::get_metr_equip_status(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    return 0x0102;
}

// -----------------------------------------------------------------------------

long ACUAECSim::get_metr_values(
	long n,
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    return n;
}

// -----------------------------------------------------------------------------

std::vector<long> ACUAECSim::get_metr_deltas(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    std::vector<long> temp(2);
    temp[0] = 0x01020304;
    temp[1] = 0x05060708;
    return temp;
}

// -----------------------------------------------------------------------------

unsigned short ACUAECSim::get_power_status(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    return 0x01;
}

// -----------------------------------------------------------------------------

unsigned char ACUAECSim::get_ac_status(
	int& errcode,
	unsigned long long int& timestamp) const
{
    errcode = 0;
    timestamp = getTimeStmp();
    return 0x02;
}

////////////////////////////////////////////////////////////////////////////////
////                                                                        ////
////                              Control Points                            ////
////                                                                        ////
////////////////////////////////////////////////////////////////////////////////
      
// -----------------------------------------------------------------------------

void ACUAECSim::reset_acu_cmd(
	int& errcode,
	unsigned long long int& timestamp)
{
    errcode = 0;
    timestamp = getTimeStmp();
}

// -----------------------------------------------------------------------------

void ACUAECSim::acu_mode_cmd(
	const unsigned char mode,
	int& errcode,
	unsigned long long& timestamp)
{
    errcode = 0;
    timestamp = getTimeStmp();

    if (drive_m->setACUMode(mode) != 0)
        acu_errorcode = COMMAND_NOT_ALLOWED;
    else
        acu_errorcode = 0;
}

// -----------------------------------------------------------------------------

void ACUAECSim::clear_fault_cmd(
	int& errcode,
	unsigned long long int& timestamp)
{
    errcode = 0;
    timestamp = getTimeStmp();
}

// -----------------------------------------------------------------------------

void ACUAECSim::az_traj_cmd(
	const long position,
	const long velocity,
	int& errcode,
	unsigned long long& timestamp)
{
    // need to remember the time this command came in to later
    // predict az position.  the equations are kept in binary parts
    // of a circle.
    //
    // later, az encoder = x0 + v * dt + a * dt * dt / 2 where dt is
    // time since az traj last commanded
    // this is all integer math.  to more accurately represent the encoders,
    // the az encoder value should be masked to produce the upper 26 bits,
    // since the encoders are 26 bits for a 720 degree range.
    //
    // the maximum velocity is 6 deg/sec, and the maximum acceleration is 12
    // deg/sec/sec.
    //
    errcode = 0;

    double pos = (double)(position) * degreesPerBit;
    double vel = (double)(velocity) * degreesPerBit;

    switch (drive_m->setAzPsn(pos,vel))
	{
	case 1:
	    acu_errorcode = COMMAND_NOT_ALLOWED;
	    break;

	case 2:
	    acu_errorcode = COMMAND_NOT_ALLOWED;
	    break;

	case 3:
	    acu_errorcode = COMMAND_NOT_ALLOWED;
	    break;

	case 4:
	    acu_errorcode = COMMAND_NOT_ALLOWED;
	    break;

	default:
	    acu_errorcode = 0;
	    break;
	}

    timestamp = getTimeStmp();
}

// -----------------------------------------------------------------------------

void ACUAECSim::el_traj_cmd(
	const long position,
	const long velocity,
	int& errcode,
	unsigned long long& timestamp)
{
    errcode = 0;

    double pos = (double)(position) * degreesPerBit;
    double vel = (double)(velocity) * degreesPerBit;

    switch (drive_m->setElPsn(pos,vel))
	{
	case 1:
	    acu_errorcode = COMMAND_NOT_ALLOWED;
	    break;

	case 2:
	    acu_errorcode = COMMAND_NOT_ALLOWED;
	    break;

	case 3:
	    acu_errorcode = COMMAND_NOT_ALLOWED;
	    break;

	case 4:
	    acu_errorcode = COMMAND_NOT_ALLOWED;
	    break;

	default:
	    acu_errorcode = 0;
	    break;
    }

    timestamp = getTimeStmp();
}

// -----------------------------------------------------------------------------

void ACUAECSim::set_az_brake(
	const unsigned char mode,
	int& errcode,
	unsigned long long int& timestamp)
{
    errcode = 0;
    if (drive_m->Az.set_brake((AEC_AZEL_Brake_t)mode) != 0)
        acu_errorcode = COMMAND_NOT_ALLOWED;
    else
        acu_errorcode = 0;
    timestamp = getTimeStmp();
}

// -----------------------------------------------------------------------------

void ACUAECSim::set_el_brake(
	const unsigned char mode,
	int& errcode,
	unsigned long long int& timestamp)
{
    errcode = 0;
    if (drive_m->El.set_brake((AEC_AZEL_Brake_t)mode))
        acu_errorcode = COMMAND_NOT_ALLOWED;
    else
        acu_errorcode = 0;
    timestamp = getTimeStmp();
}


// -----------------------------------------------------------------------------

void ACUAECSim::set_az_motor_power(
	const unsigned char mode,
	int& errcode,
	unsigned long long int& timestamp)
{
    if (mode == 1)
	{
	az_motor_m |= 1;
	}
    else if (mode == 0)
	{
	az_motor_m &= 2;
	}
}

// -----------------------------------------------------------------------------

void ACUAECSim::set_el_motor_power(
	const unsigned char mode,
	int& errcode,
	unsigned long long int& timestamp)
{
    if (mode == 1)
	{
	el_motor_m |= 1;
	}
    else if (mode == 0)
	{
	el_motor_m &= 2;
	}
}

// -----------------------------------------------------------------------------

void ACUAECSim::set_az_motor_driver(
	const unsigned char mode,
	int& errcode,
	unsigned long long int& timestamp)
{
    if (mode == 1)
	{
	az_motor_m |= 2;
	}
    else if (mode == 0)
	{
	az_motor_m &= 1;
	}
}

// -----------------------------------------------------------------------------

void ACUAECSim::set_el_motor_driver(
	const unsigned char mode,
	int& errcode,
	unsigned long long int& timestamp)
{
    if (mode == 1)
	{
	el_motor_m |= 2;
	}
    else if (mode == 0)
	{
	el_motor_m &= 1;
	}
}

// -----------------------------------------------------------------------------

void ACUAECSim::set_az_servo_coeff(
	const long index,
	const double value,
	int& errcode,
	unsigned long long int& timestamp)
{
    drive_m->Az.servo_coefficients[index] = value;
    errcode = 0;
    timestamp = getTimeStmp();
}

// -----------------------------------------------------------------------------

void ACUAECSim::set_el_servo_coeff(
	const long index,
	const double value,
	int& errcode,
	unsigned long long int& timestamp)
{
    drive_m->El.servo_coefficients[index] = value;
    errcode = 0;
    timestamp = getTimeStmp();
}

// -----------------------------------------------------------------------------

void ACUAECSim::set_az_servo_default(
	int& errcode,
	unsigned long long int& timestamp)
{
    errcode = 0;
    timestamp = getTimeStmp();
    drive_m->Az.set_servo_defaults();
}

// -----------------------------------------------------------------------------

void ACUAECSim::set_el_servo_default(
	int& errcode,
	unsigned long long int& timestamp)
{
    errcode = 0;
    timestamp = getTimeStmp();
    drive_m->El.set_servo_defaults();
}

// -----------------------------------------------------------------------------

void ACUAECSim::set_az_motion_lim(
	const long limits,
	int& errcode,
	unsigned long long& timestamp)
{
    errcode = 0;
    az_motion_limit_m[0] = limits & 0xffff;
    az_motion_limit_m[1] = (limits >> 16) & 0xffff;
    timestamp = getTimeStmp();
}

// -----------------------------------------------------------------------------

void ACUAECSim::set_el_motion_lim(
	const long limits,
	int& errcode,
	unsigned long long& timestamp)
{
    errcode = 0;
    el_motion_limit_m[0] = limits & 0xffff;
    el_motion_limit_m[1] = (limits >> 16) & 0xffff;
    timestamp = getTimeStmp();
}

// -----------------------------------------------------------------------------

void ACUAECSim::init_az_enc_abs_pos(
	const unsigned char mode,
	int& errcode,
	unsigned long long& timestamp)
{
    errcode = 0;
    timestamp = getTimeStmp();

    unsigned char status = static_cast<unsigned char>(0xFD);
    drive_m->setAzStatus(status);
}

// -----------------------------------------------------------------------------

void ACUAECSim::init_el_enc_abs_pos(
	const unsigned char mode,
	int& errcode,
	unsigned long long& timestamp)
{
    errcode = 0;
    timestamp = getTimeStmp();
    unsigned char status = static_cast<unsigned char>(0xFD);
    drive_m->setElStatus(status);
}

// -----------------------------------------------------------------------------

void ACUAECSim::set_idle_stow_time(
	const unsigned short seconds,
	int& errcode,
	unsigned long long& timestamp)
{
    errcode = 0;
    idle_stow_time_m = seconds;
    timestamp = getTimeStmp();
}

// -----------------------------------------------------------------------------

void ACUAECSim::set_shutter(
	const unsigned char value,
	int& errcode,
	unsigned long long& timestamp)
{
    errcode = 0;
    timestamp = getTimeStmp();
    if(value == 0x0)
	shutter_m = 0x2;
    else{
    if(value == 0x1)
	shutter_m = value;
    // TBD: report error in error stack 
    // for illegal values
    }
}

// -----------------------------------------------------------------------------

void ACUAECSim::set_stow_pin(
	const unsigned short bits,
	int& errcode,
	unsigned long long int& timestamp)
{
    unsigned char* ptr = (unsigned char*)&bits;
    drive_m->Az.set_stow_pin((AEC_Stow_Pin_t)(ptr[0]));
    drive_m->El.set_stow_pin((AEC_Stow_Pin_t)(ptr[1]));
    timestamp = getTimeStmp();
}

// -----------------------------------------------------------------------------

void ACUAECSim::set_pt_model_coeff(
	const long index,
	const double value,
	int& errcode,
	unsigned long long int& timestamp)
{
    pt_model_coeff_n[index] = value;
    errcode = 0;
    timestamp = getTimeStmp();
}

// -----------------------------------------------------------------------------

void ACUAECSim::set_ip_address(
	const unsigned long address,
	int& errcode,
	unsigned long long& timestamp)
{
    errcode = 0;
    timestamp = getTimeStmp();
}

// -----------------------------------------------------------------------------

void ACUAECSim::set_subref_abs_posn(
	const long long pos,
	int& errcode,
	unsigned long long& timestamp)
{
    errcode = 0;
    subref_abs_posn_m[0] = pos & 0xffff;
    subref_abs_posn_m[1] = (pos >> 16) & 0xffff;
    subref_abs_posn_m[2] = (pos >> 32) & 0xffff;
    timestamp = getTimeStmp();
}

// -----------------------------------------------------------------------------

void ACUAECSim::set_subref_delta_posn(
	const long long pos,
	int& errcode,
	unsigned long long& timestamp)
{
    errcode = 0;
    subref_delta_posn_m[0] = pos & 0xffff;
    subref_delta_posn_m[1] = (pos >> 16) & 0xffff;
    subref_delta_posn_m[2] = (pos >> 32) & 0xffff;
    timestamp = getTimeStmp();
}

// -----------------------------------------------------------------------------

void ACUAECSim::subref_delta_zero_cmd(
	const unsigned char value,
	int& errcode,
	unsigned long long& timestamp)
{
    errcode = 0;
    timestamp = getTimeStmp();
    subref_abs_posn_m[0] = 0;
    subref_abs_posn_m[1] = 0;
    subref_abs_posn_m[2] = 0;
}


// -----------------------------------------------------------------------------

void ACUAECSim::set_metr_mode(
	const unsigned char mode,
	int& errcode,
	unsigned long long& timestamp)
{
    errcode = 0;
    timestamp = getTimeStmp();
    if (mode == 1)
	{
	metr_mode_m |= 2;
	}
    else if (mode == 0)
	{
	metr_mode_m &= 0x1;
	}
}

////////////////////////////////////////////////////////////////////////////////
////                                                                        ////
////                         Generic Monitor Points                         ////
////                                                                        ////
////////////////////////////////////////////////////////////////////////////////
      
// -----------------------------------------------------------------------------

std::vector<char> ACUAECSim::get_sw_rev_level(
    int& errcode,
    unsigned long long& timestamp) const
{
    std::vector<char> tmp(3);
    tmp[0] = 1;	// major
    tmp[1] = 2;	// minor
    tmp[2] = 3;	// patch
    return tmp;
}

// -----------------------------------------------------------------------------

unsigned long ACUAECSim::get_can_error(
    int& errcode,
    unsigned long long& timestamp) const
{
    return 0;
}

// -----------------------------------------------------------------------------

unsigned long ACUAECSim::get_num_trans(
    int& errcode,
    unsigned long long& timestamp) const
{
    return 0;
}

// -----------------------------------------------------------------------------

std::vector<char> ACUAECSim::get_system_id(
    int& errcode,
    unsigned long long& timestamp) const
{
    // Notice ACU version has extra status byte compared to mandatory version.
    std::vector<char> tmp(4);
    tmp[0] = 0xAB;      // major
    tmp[1] = 0xCD;      // minor
    tmp[2] = 0xEF;      // release
    tmp[3] = 0x87;      // status byte
    /*
     * Byte 3 -
     * bit 0: encoder interface board detected    
     * bit 1: CAN bus interface board (to servo amplifiers) detected  
     * bit 2: dig/ana I/O board detected    
     * bit 3-6:    
     * bit 7: Ethernet interface (on CPU) detected
     */
    return tmp;
}
