// @(#) $Id$
//
// Copyright (C) 2002
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include "HolographyReceiver.h"
#include "AMBUtil.h"
#include <iomanip>

#include <stdio.h>

// shortcuts to avoid having to clutter the code with namespace qualifiers
using std::vector;
using std::endl;
using std::cerr;
using std::cout;
using AMB::HolographyReceiver;

// ----------------------------------------------------------------------------
HolographyReceiver::
HolographyReceiver(node_t node, const vector<CAN::byte_t>& serialNumber)
  :node_m(node),
   sn_m(serialNumber),
   trans_num_m(0),
   PLLStatus_m(0x0E),
   gunnHVoltage_m(9.0),
   gunnLVoltage_m(8.0),
   LOLevel_m(4.9),
   RefIFLevel_m(3.0),
   SigIFLevel_m(2.0),
   RefIChannelRMS_m(1.0),
   RefQChannelRMS_m(.5), 
   SigIChannelRMS_m(.25),
   SigQChannelRMS_m(.125),
   supplyCurrent_m(4.0),
   tempPS_m(28.0),
   tempSignalChannelMixer_m(23.0),
   tempReferenceChannelMixer_m(22.0),
   temp29MHzOCXO_m(29.0),
   temp95MHzOCXO_m(35.0),
   tempLockBox_m(30.0),
   sigAttenuation_m(10),
   refAttenuation_m(20),
   synthFrequency_m(0),
   driveEnable_m(false),
   loopEnable_m(false),
   nominalVoltage_m(4.0),
   gunnLoopGain_m(6000.0),
   gunnTuneRange_m(10E3),
   gunnSelect_m(true)
{
  cout << "Creating node 0x" << node << ", s/n 0x";
  for (int i = 0; i < 8; i++)
    cout << std::hex << std::setw(2) << std::setfill('0') 
	 << static_cast<int>(serialNumber[i]);
  cout << ", Holography Receiver device" << endl;
}

// ----------------------------------------------------------------------------
HolographyReceiver::~HolographyReceiver()
{}

// ----------------------------------------------------------------------------
AMB::node_t HolographyReceiver::node() const {
  return node_m;
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::serialNumber() const {
  return sn_m;
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::monitor(rca_t rca) const {
  trans_num_m++;
  const vector<CAN::byte_t> tmp;
  
  switch (rca) {
    // Mandatory generic points
  case GET_CAN_ERROR:
    return get_can_error();
    break;
  case GET_PROTOCOL_REV_LEVEL:
    return get_protocol_rev_level();
    break;
  case AMB::Device::GET_SW_REV_LEVEL:
    return get_software_rev_level();
    break;
  case AMB::Device::GET_HW_REV_LEVEL:
    return get_hardware_rev_level();
    break;
  case GET_TRANS_NUM:
    return longToData(tmp, get_trans_num());
    break;
    // Optional generic point
  case GET_AMBIENT_TEMPERATURE:
    return floatDS1820ToData(tmp, get_ambient_temperature());
    break;
    
    // Monitor points for node 2, HolographyReceiver receiver
  case GET_PLL_STATUS:
    return getPLL_Status();
    break;
  case GET_GUNN_H_VOLTAGE:
    return getGunnH_Voltage();
    break;
  case GET_GUNN_L_VOLTAGE:
    return getGunnL_Voltage();
    break;
  case GET_LO_DET_OUT:
    return getLODetOut();
    break;
  case GET_REF_DET_OUT:
    return getRefDetOut();
    break;
  case GET_SIG_DET:
    return getSigDet();
    break;
  case GET_REF_SENSE_I:
    return getRefSenseI();
    break;
  case GET_REF_SENSE_Q:
    return getRefSenseQ();
    break;
  case GET_SIG_SENSE_I:
    return getSigSenseI();
    break;
  case GET_SIG_SENSE_Q:
    return getSigSenseQ();
    break;
  case GET_SUPPLY_CURRENT:
    return getSupplyCurrent();
    break;
  case GET_TEMP_POWER_SUPPLY:
    return getTempPowerSupply();
    break;
  case GET_TEMP_SIG_MIX:
    return getTempSigMix();
    break;
  case GET_TEMP_REF_MIX:
    return getTempRefMix();
    break;
  case GET_TEMP_29MHZ_OCXO:
    return getTemp29MHzOCXO();
    break;
  case GET_TEMP_95MHZ_OCXO:
    return getTemp95MHzOCXO();
    break;
  case GET_TEMP_LOCK_BOX:
    return getTempLockBox();
    break;
  case GET_SIG_ATTENUATION:
    return getSigAttenuation();
    break;
  case GET_REF_ATTENUATION:
    return getRefAttenuation();
    break;
  case GET_SYNTH_FREQUENCY:
    return getSynthFrequency();
    break;
  case GET_DRIVE_ENABLE:
    return getDriveEnable();
    break;
  case GET_LOOP_ENABLE:
    return getLoopEnable();
    break;
  case GET_NOMINAL_VOLTAGE:
    return getNominalVoltage();
    break;
  case GET_GUNN_LOOP_GAIN:
    return getGunnLoopGain();
    break;
  case GET_GUNN_TUNE_RANGE:
    return getGunnTuneRange();
    break;
  case GET_GUNN_SELECT:
    return getGunnSelect();
    break;
  default:
    cerr << "Switch does not match any HolographyReceiver case. " 
	 << "The unhandled RCA is " << static_cast<int>(rca)
	 << endl;
    throw HolographyReceiverError("Unknown RCA in monitor command");
  }
}

// ----------------------------------------------------------------------------
void HolographyReceiver::control(rca_t rca, const vector<CAN::byte_t>& data) {
  trans_num_m++;
  switch (rca) {
  case SET_SIG_ATTENUATION:
    setSigAttenuation(data,1,"SET_SIG_ATTENUATION");
    break;
    
  case SET_REF_ATTENUATION:
    setRefAttenuation(data,1,"SET_REF_ATTENUATION");
    break;
    
  case SET_SYNTH_FREQUENCY:
    setSynthFrequency(data,2,"SET_SYNTH_FREQUENCY");
    break;
    
  case SET_DRIVE_ENABLE:
    setDriveEnable(data,1,"SET_DRIVE_ENABLE");
    break;
    
  case SET_LOOP_ENABLE:
    setLoopEnable(data,1,"SET_LOOP_ENABLE");
    break;
    
  case SET_NOMINAL_VOLTAGE:
    setNominalVoltage(data, "SET_NOMINAL_VOLTAGE");
    break;
    
  case SET_GUNN_LOOP_GAIN:
    setGunnLoopGain(data, "SET_GUNN_LOOP_GAIN");
    break;
    
  case SET_GUNN_TUNE_RANGE:
    setGunnTuneRange(data, "SET_GUNN_TUNE_RANGE");
    break;
    
  case SET_GUNN_SELECT:
    setGunnSelect(data,1,"SET_GUNN_SELECT");
    break;
    
  case RESET_PLL_STATUS:
    resetPLLStatus(data,1,"RESET_PLL_STATUS");
    break;
    
  default:
    throw HolographyReceiverError("Unknown RCA in control command");
  }
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::getPLL_Status() const {
  vector<CAN::byte_t> retVal = vector<CAN::byte_t>(1);
  retVal[0] = PLLStatus_m;
  return retVal;
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::getGunnH_Voltage() const {
  return AMB::floatToData(gunnHVoltage_m);
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::getGunnL_Voltage() const {
  return AMB::floatToData(gunnLVoltage_m);
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::getLODetOut() const {
  return AMB::floatToData(LOLevel_m);
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::getRefDetOut() const {
  return AMB::floatToData(RefIFLevel_m);
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::getSigDet() const {
  return AMB::floatToData(SigIFLevel_m);
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::getRefSenseI() const {
  return AMB::floatToData(RefIChannelRMS_m);
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::getRefSenseQ() const {
  return AMB::floatToData(RefQChannelRMS_m);
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::getSigSenseI() const {
  return AMB::floatToData(SigIChannelRMS_m);
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::getSigSenseQ() const {
  return AMB::floatToData(SigQChannelRMS_m);
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::getSupplyCurrent() const {
  return AMB::floatToData(supplyCurrent_m);
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::getTempPowerSupply() const {
  return AMB::floatToData(tempPS_m);
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::getTempSigMix() const {
  return AMB::floatToData(tempSignalChannelMixer_m);
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::getTempRefMix() const {
  return AMB::floatToData(tempReferenceChannelMixer_m);
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::getTemp29MHzOCXO() const {
  return AMB::floatToData(temp29MHzOCXO_m);
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::getTemp95MHzOCXO() const {
  return AMB::floatToData(temp95MHzOCXO_m);
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::getTempLockBox() const {
  return AMB::floatToData(tempLockBox_m);
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::getSigAttenuation() const {
  vector<CAN::byte_t> data;
  return AMB::charToData(data, static_cast<unsigned char>(sigAttenuation_m));
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::getRefAttenuation() const {
  vector<CAN::byte_t> data;
  return AMB::charToData(data, static_cast<unsigned char>(refAttenuation_m));
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::getSynthFrequency() const {
  vector<CAN::byte_t> data;
  return AMB::shortToData(data, synthFrequency_m);
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::getDriveEnable() const {
  vector<CAN::byte_t> data;
  unsigned char retVal = 0;
  if (driveEnable_m) retVal = 0x01;
  return AMB::charToData(data, retVal);
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::getLoopEnable() const {
  vector<CAN::byte_t> data;
  unsigned char retVal = 0;
  if (loopEnable_m) retVal = 0x01;
  return AMB::charToData(data, retVal);
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::getNominalVoltage() const {
  return AMB::floatToData(nominalVoltage_m);
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::getGunnLoopGain() const {
  return AMB::floatToData(gunnLoopGain_m);
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::getGunnTuneRange() const {
  return AMB::floatToData(gunnTuneRange_m);
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::getGunnSelect() const {
  vector<CAN::byte_t> data;
  unsigned char retVal = 0;
  if (gunnSelect_m) retVal = 0x01;
  return AMB::charToData(data, retVal);
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::get_can_error() const {
  return AMB::Device::get_can_error();
}

// ----------------------------------------------------------------------------
vector<CAN::byte_t> HolographyReceiver::get_protocol_rev_level() const {
  return AMB::Device::get_protocol_rev_level();
}

// ----------------------------------------------------------------------------
unsigned int HolographyReceiver::get_trans_num() const {
  return trans_num_m;
}

// ----------------------------------------------------------------------------
float HolographyReceiver::get_ambient_temperature() const {
  return 22.0;
}

// ----------------------------------------------------------------------------
void HolographyReceiver::
setSigAttenuation(const vector<CAN::byte_t>& data, int size, const char* pMsg){
  checkSize(data, size, pMsg);
  checkRange(data[0], 63, pMsg);
  sigAttenuation_m = static_cast<short>(data[0]);
}

// ----------------------------------------------------------------------------
void HolographyReceiver::
setRefAttenuation(const vector<CAN::byte_t>& data, int size, const char* pMsg){
  checkSize(data, size, pMsg);
  checkRange(data[0], 63, pMsg);
  refAttenuation_m = static_cast<short>(data[0]);
}

// ----------------------------------------------------------------------------
void HolographyReceiver::
setSynthFrequency(const vector<CAN::byte_t>& data, int size, const char* pMsg){
  checkSize(data, size, pMsg);
  vector<CAN::byte_t> tmp(data);
  short tmpVal =  dataToShort(tmp);
  checkRange(tmpVal, 5960, pMsg);
  synthFrequency_m = tmpVal; 
}

// ----------------------------------------------------------------------------
void HolographyReceiver::
setDriveEnable(const vector<CAN::byte_t>& data, int size, const char* pMsg) {
  checkSize(data, size, pMsg);
  checkRange(data[0], 1, pMsg);
  if (data[0] > 0) {
    driveEnable_m = true;
    PLLStatus_m |= 0x01;
  } else {
    PLLStatus_m &= 0xFE;
    driveEnable_m = false;
  }
}

// ----------------------------------------------------------------------------
void HolographyReceiver::
setLoopEnable(const vector<CAN::byte_t>& data, int size, const char* pMsg) {
  checkSize(data, size, pMsg);
  checkRange(data[0], 1, pMsg);
  PLLStatus_m |= (data[0] << 1);
  if (data[0] > 0) {
    loopEnable_m = true;
  } else {
    loopEnable_m = false;
  }
}

// ----------------------------------------------------------------------------
void HolographyReceiver::
setNominalVoltage(const vector<CAN::byte_t>& data, const char* pMsg){
  checkSize(data, 4, pMsg);
  vector<CAN::byte_t> tmp(data);
  float tmpVal = dataToFloat(tmp);
  checkRange(tmpVal, 0.0, 10., pMsg);
  nominalVoltage_m = tmpVal;
}

// ----------------------------------------------------------------------------
void HolographyReceiver::
setGunnLoopGain(const vector<CAN::byte_t>& data, const char* pMsg) {
  checkSize(data, 4, pMsg);
  vector<CAN::byte_t> tmp(data);
  float tmpVal = dataToFloat(tmp);
  checkRange(tmpVal, 0.0, 10.E3, pMsg);
  gunnLoopGain_m = tmpVal;
}

// ----------------------------------------------------------------------------
void HolographyReceiver::
setGunnTuneRange(const vector<CAN::byte_t>& data, const char* pMsg) {
  checkSize(data, 4, pMsg);
  vector<CAN::byte_t> tmp(data);
  float tmpVal = dataToFloat(tmp);
  checkRange(tmpVal, 0.0, 50.E3, pMsg);
  gunnTuneRange_m = tmpVal;
}

// ----------------------------------------------------------------------------
void HolographyReceiver::
setGunnSelect(const vector<CAN::byte_t>& data, int size, const char* pMsg) {
  checkSize(data, size, pMsg);
  checkRange(data[0], 1, pMsg);
  if (data[0] > 0) {
    gunnSelect_m = true;
  } else {
    gunnSelect_m = false;
  }
}

// ----------------------------------------------------------------------------
void HolographyReceiver::
resetPLLStatus(const vector<CAN::byte_t>& data, int size, const char* pMsg) {
  checkSize(data, size, pMsg);
  checkRange(data[0], 0x03, pMsg);
  PLLStatus_m = 0;
}

// ----------------------------------------------------------------------------
void HolographyReceiver::
checkSize(const vector<CAN::byte_t>& data, int size, 
	  const char* pExceptionMsg){
  if (static_cast<int>(data.size()) != size) {
    char exceptionMsg[200];
    sprintf(exceptionMsg,"%s data length is %d must be %d",pExceptionMsg,
	    data.size(), size);
    throw HolographyReceiverError(exceptionMsg);
  }
}

// ----------------------------------------------------------------------------
void HolographyReceiver::checkRange(CAN::byte_t value, CAN::byte_t maxValue,
				    const char* pMsg) const {
  if (value > maxValue) {
    char buf[200];
    sprintf(buf,"%s out of range: %d max is %d",pMsg, value, maxValue);
    throw HolographyReceiverError(buf);
  }
}

// ----------------------------------------------------------------------------
void HolographyReceiver::checkRange(short value, short maxValue,
				    const char* pMsg) const {
  if (value > maxValue) {
    char buf[200];
    sprintf(buf,"%s out of range: %d max is %d",pMsg, value, maxValue);
    throw HolographyReceiverError(buf);
  }
}

// ----------------------------------------------------------------------------
void HolographyReceiver::checkRange(float value, float minValue,
				    float maxValue, const char* pMsg) const {
  if (value > maxValue || value < minValue) {
    char buf[200];
    sprintf(buf,"%s out of range: %f (min is %f max is %f)", pMsg, value, 
	    minValue, maxValue);
    throw HolographyReceiverError(buf);
  }
}
