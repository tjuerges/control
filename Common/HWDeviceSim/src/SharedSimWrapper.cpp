/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2005
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* rhiriart  2005-09-20  created
*/


#include <SharedSimWrapper.h>
#include <cmath>
#include <vector>
#include <CANTypes.h>
#include <acstimeTimeUtil.h>


/*----------------------------------------------------------------------
 * Public interface
 *----------------------------------------------------------------------*/
/**
 * Constructor.
 */

SharedSimWrapper::SharedSimWrapper(int antenna, SHARED_SIMULATOR::SharedSimulator_var ss) :
  simulator(ss),
  antennaNumber(antenna)
{
  bool useSharedSimulator = true;
  SHARED_SIMULATOR::SharedSimulator_var tempRef(
      SHARED_SIMULATOR::SharedSimulator::_narrow(simulator));
  if(CORBA::is_nil(tempRef) == true)
  {
    useSharedSimulator = false;
  }

  if (!useSharedSimulator)
  {
    return;
  }

  // Set up some initial parameters in the simulator
  // setDefaults();

  // Turn on the simulator
  // simulator->on(SS_UPDATE_PERIOD);
}

/**
 * Destructor.
 */
SharedSimWrapper::~SharedSimWrapper() {
}

/**
 * Get down converter powers from the SharedSimulator.
 */
std::vector<CAN::byte_t> SharedSimWrapper::getDownConvPowers(short polarization) {

  std::vector<CAN::byte_t> tmp;

  if (!haveSimulatorRef()) {
    tmp.assign(4, 0);
    return tmp;
  }

  SHARED_SIMULATOR::DownConvPowers dcp;
  dcp = simulator->getDownConvPowers(antennaNumber);
  //double scaleFactor = 3.8147e-05;

  if (polarization == 0) {
    tmp = AMB::shortToData(tmp, dcp.S0P0);
    tmp = AMB::shortToData(tmp, dcp.S1P0);
  }
  else if (polarization == 1) {
    tmp = AMB::shortToData(tmp, dcp.S0P1);
    tmp = AMB::shortToData(tmp, dcp.S1P1);
  }

  return tmp;
}

/**
 * Get baseband powers from the SharedSimulator.
 */
std::vector<CAN::byte_t> SharedSimWrapper::getBaseBandPowers(short polarization) {

  std::vector<CAN::byte_t> tmp;

  if (!haveSimulatorRef()) {
    tmp.assign(8, 0);
    return tmp;
  }

  SHARED_SIMULATOR::BaseBandPowers bbp;
  bbp = simulator->getBaseBandPowers(antennaNumber);

  if (polarization == 0) {
    tmp = AMB::shortToData(tmp, bbp.AP0);
    tmp = AMB::shortToData(tmp, bbp.BP0);
    tmp = AMB::shortToData(tmp, bbp.CP0);
    tmp = AMB::shortToData(tmp, bbp.DP0);
  }
  else if (polarization == 1) {
    tmp = AMB::shortToData(tmp, bbp.AP1);
    tmp = AMB::shortToData(tmp, bbp.BP1);
    tmp = AMB::shortToData(tmp, bbp.CP1);
    tmp = AMB::shortToData(tmp, bbp.DP1);
  }

  return tmp;
}

/**
 * Get downconverter and baseband attenuations from the
 * SharedSimulator.
 */
std::vector<CAN::byte_t> SharedSimWrapper::getGains(short polarization) {

  std::vector<CAN::byte_t> tmp;

  if (!haveSimulatorRef()) {
    tmp.assign(6, 0);
    return tmp;
  }

  // Down converter attenuators
  SHARED_SIMULATOR::DownConvAttenuators dca;
  dca = simulator->getDownConvAttenuators(antennaNumber);

  if (polarization == 0) {
    tmp = AMB::charToData(tmp, static_cast<char>(dca.S0P0 * 8.0));
    tmp = AMB::charToData(tmp, static_cast<char>(dca.S1P0 * 8.0));
  }
  else if (polarization == 1) {
    tmp = AMB::charToData(tmp, static_cast<char>(dca.S0P1 * 8.0));
    tmp = AMB::charToData(tmp, static_cast<char>(dca.S1P1 * 8.0));
  }

  // Baseband attenuators
  SHARED_SIMULATOR::BaseBandAttenuators bba;
  bba = simulator->getBaseBandAttenuators(antennaNumber);

  if (polarization == 0) {
    tmp = AMB::charToData(tmp, static_cast<char>(bba.AP0 * 8.0));
    tmp = AMB::charToData(tmp, static_cast<char>(bba.BP0 * 8.0));
    tmp = AMB::charToData(tmp, static_cast<char>(bba.CP0 * 8.0));
    tmp = AMB::charToData(tmp, static_cast<char>(bba.DP0 * 8.0));
  }
  else if (polarization == 1) {
    tmp = AMB::charToData(tmp, static_cast<char>(bba.AP1 * 8.0));
    tmp = AMB::charToData(tmp, static_cast<char>(bba.BP1 * 8.0));
    tmp = AMB::charToData(tmp, static_cast<char>(bba.CP1 * 8.0));
    tmp = AMB::charToData(tmp, static_cast<char>(bba.DP1 * 8.0));
  }

  return tmp;
}

/**
 * Get downconverter and baseband attenuations from the
 * SharedSimulator.
 */
void SharedSimWrapper::setGains(short polarization, const std::vector<CAN::byte_t>& data) {

  if (!haveSimulatorRef())
    return;

  if (data.size() != 6) {
    std::cerr << "[SharedSimWrapper::setGains] ERROR: incorrect data length, it's "
	      << data.size() << " and should be 6." << std::endl;
    return;
  }

  double GS0 = static_cast<double>(data[0]) / 8.0;
  double GS1 = static_cast<double>(data[1]) / 8.0;
  double GA  = static_cast<double>(data[2]) / 8.0;
  double GB  = static_cast<double>(data[3]) / 8.0;
  double GC  = static_cast<double>(data[4]) / 8.0;
  double GD  = static_cast<double>(data[5]) / 8.0;

  SHARED_SIMULATOR::DownConvAttenuators dca;
  // get first the values that are already in the Simulator
  dca = simulator->getDownConvAttenuators(antennaNumber);

  // modify the values for this IFProc's polarization
  if (polarization == 0) {
    dca.S0P0 = GS0;
    dca.S1P0 = GS1;
  }
  else if (polarization == 1) {
    dca.S0P1 = GS0;
    dca.S1P1 = GS1;
  }

  // Baseband Attenuators
  SHARED_SIMULATOR::BaseBandAttenuators bba;

  bba = simulator->getBaseBandAttenuators(antennaNumber);

  if (polarization == 0) {
    bba.AP0 = GA;
    bba.BP0 = GB;
    bba.CP0 = GC;
    bba.DP0 = GD;
  }
  else if (polarization == 1) {
    bba.AP1 = GA;
    bba.BP1 = GB;
    bba.CP1 = GC;
    bba.DP1 = GD;
  }

  // Turn off the simulator
  // simulator->off();

  // Make the changes
  simulator->setDownConvAttenuators(antennaNumber, dca);
  simulator->setBaseBandAttenuators(antennaNumber, bba);

  // Turn on the simulator
  // simulator->on(SS_UPDATE_PERIOD);

}

/**
 * Set the antenna position in the SharedSimulator.
 */
void SharedSimWrapper::setAntPosn(double az, double el) {

  if (!haveSimulatorRef())
    return;

  // get the current time
  acstime::Epoch timestamp = TimeUtil::ace2epoch(ACE_OS::gettimeofday());

  SHARED_SIMULATOR::AntAzEl azel;
  azel.timeStamp = timestamp.value;
  azel.antennaNum = antennaNumber;
  azel.antAz = az * M_PI / 180.0; // SharedSimulator expects radians
  azel.antEl = el * M_PI / 180.0; // SharedSimulator expects radians

  // Turn off the simulator
  // simulator->off();

  std::cout << "Moving antenna, az = " << azel.antAz
	    << " el = " << azel.antEl << std::endl;

  // Setup new position
  simulator->setAntAzEl(azel);

  // Turn on the simulator
  // simulator->on(SS_UPDATE_PERIOD);

}

void SharedSimWrapper::setSecondLOFreq(short baseband, double freq) {

  if (!haveSimulatorRef())
    return;

  // Turn off the simulator
  // simulator->off();

  // Set the coarse frequency
  switch(baseband) {
  case 0:
    simulator->setLO_Frequency(SHARED_SIMULATOR::SECOND,
			       SHARED_SIMULATOR::ZERO,
			       freq);
    break;
  case 1:
    simulator->setLO_Frequency(SHARED_SIMULATOR::SECOND,
			       SHARED_SIMULATOR::ONE,
			       freq);
    break;
  case 2:
    simulator->setLO_Frequency(SHARED_SIMULATOR::SECOND,
			       SHARED_SIMULATOR::TWO,
			       freq);
    break;
  case 3:
    simulator->setLO_Frequency(SHARED_SIMULATOR::SECOND,
			       SHARED_SIMULATOR::THREE,
			       freq);
    break;
  }

  // Turn on the simulator
  // simulator->on(SS_UPDATE_PERIOD);

}

void SharedSimWrapper::setSignalPaths(short pol, const std::vector<CAN::byte_t>& data) {

  if (!haveSimulatorRef())
    return;

  unsigned short tmp = static_cast<unsigned short>(data[0]);
  bool channelA_HighFilter = tmp & 0x01;
  bool channelB_HighFilter = tmp & 0x02;
  bool channelC_HighFilter = tmp & 0x04;
  bool channelD_HighFilter = tmp & 0x08;
  bool channelsAB_S1       = tmp & 0x10;
  bool channelsCD_S1       = tmp & 0x20;
  bool SW0                 = tmp & 0x40;
  bool SW1                 = tmp & 0x80;

  std::cout << "Setting Downconverter signal paths for polarization " << pol << std::endl;
  std::cout << "Channel A - High Lowpass Filter switch: " <<
    (channelA_HighFilter? "on" : "off") << std::endl;
  std::cout << "Channel B - High Lowpass Filter switch: " <<
    (channelB_HighFilter? "on" : "off") << std::endl;
  std::cout << "Channel C - High Lowpass Filter switch: " <<
    (channelC_HighFilter? "on" : "off") << std::endl;
  std::cout << "Channel D - High Lowpass Filter switch: " <<
    (channelD_HighFilter? "on" : "off") << std::endl;
  std::cout << "Channels A and B - Sideband 1 switch: " <<
    (channelsAB_S1 ? "on" : "off") << std::endl;
  std::cout << "Channels C and D - Sideband 1 switch: " <<
    (channelsCD_S1 ? "on" : "off") << std::endl;
  std::cout << "Sideband 0 switch: " <<
    (SW0 ? "on" : "off") << std::endl;
  std::cout << "Sideband 1 switch: " <<
    (SW1 ? "on" : "off") << std::endl;

  SHARED_SIMULATOR::DownConvSwitches dcsw;

  dcsw = simulator->getDownConvSwitches();

  switch(pol) {
  case 0:
    dcsw.ABP0 = channelsAB_S1;
    dcsw.CDP0 = channelsCD_S1;
    break;
  case 1:
    dcsw.ABP1 = channelsAB_S1;
    dcsw.CDP1 = channelsCD_S1;
    break;
  }

  // Turn off the simulator
  // simulator->off();

  simulator->setDownConvSwitches(dcsw);

  // Turn on the simulator
  // simulator->on(SS_UPDATE_PERIOD);

}

/**
 * Do we have an ACS reference to TELCAL SharedSimulator component?
 */
bool SharedSimWrapper::haveSimulatorRef() {
  return !CORBA::is_nil(simulator.in());
}

/*----------------------------------------------------------------------
 * Private interface
 *----------------------------------------------------------------------*/

/** Sets up several values in the SharedSimulator in order to run
 * a simulation.
 */
void SharedSimWrapper::setDefaults() {

  if (!haveSimulatorRef())
    return;

  // Get the current time
  acstime::Epoch timestamp = TimeUtil::ace2epoch(ACE_OS::gettimeofday());


  // Check first for the reference to the simulator
  if (CORBA::is_nil(simulator.in()))
    return;

  simulator->off();

  // Set the simulator time. A zero value sets up the simulator
  // with the current time.
  simulator->setTime(0);

  // Set the simulator internal mode parameters.
  SHARED_SIMULATOR::InternalMode im;
  im.antenna = 0;   // don't use simulated antenna pointing directions
  im.subref = 1;    // use simulated subreflector positions
  im.delay = 1;     // use simulated total delay values
  im.phase = 1;     // use simulated LO phase values
  im.phaseRate = 1; // use simulated LO phase rate values
  simulator->setInternalMode(im);

  // Set the simulator weather
  simulator->setWeather(293.0,     // temperature
			101300.0,  // pressure
			0.5);      // humidity

  // Set up the subarray in the simulator
  SHARED_SIMULATOR::Antenna antenna;
  antenna.hwAnt = antennaNumber;
  antenna.hwCorrInput = antennaNumber;
  SHARED_SIMULATOR::SubArray subArray;
  subArray.antennas.length(1);
  subArray.antennas[0] = antenna;
  simulator->setArray(subArray);

  // Set up source parameters

  // RA = 22:02:43.2916, DEC = +42:16:39.977
  simulator->setSourcePosition((22*3600.0 + 2*60.0 + 43.2916)/43200.0 * M_PI,
			       (42*3600.0 + 16*60.0 + 39.977)/3600.0/180.0*M_PI);
  simulator->setSourceFlux(15.0);

  // Set frequencies
  simulator->setLO_Frequency(SHARED_SIMULATOR::FIRST,
			     SHARED_SIMULATOR::ZERO,
			     100.0e9);
  simulator->setLO_Frequency(SHARED_SIMULATOR::SECOND,
			     SHARED_SIMULATOR::ZERO,
			     8.0e9);
  simulator->setLO_Frequency(SHARED_SIMULATOR::SECOND,
			     SHARED_SIMULATOR::ONE,
			     10.0e9);
  simulator->setLO_Frequency(SHARED_SIMULATOR::SECOND,
			     SHARED_SIMULATOR::TWO,
			     12.0e9);
  simulator->setLO_Frequency(SHARED_SIMULATOR::SECOND,
			     SHARED_SIMULATOR::THREE,
			     14.0e9);

  // Set switches
  SHARED_SIMULATOR::DownConvSwitches dcs;
  dcs.ABP0 = 0;
  dcs.ABP1 = 0;
  dcs.CDP0 = 1;
  dcs.CDP1 = 1;
  simulator->setDownConvSwitches(dcs);

  // Set receiver temperatures and gains
  SHARED_SIMULATOR::ReceiverTemps temp;
  temp.S0P0 = 100.0;
  temp.S0P1 = 110.0;
  temp.S1P0 = 120.0;
  temp.S1P1 = 130.0;
  simulator->setReceiverTemps(antennaNumber, temp);

  SHARED_SIMULATOR::ReceiverGains gains;
  gains.S0P0 = 2000.0;
  gains.S0P1 = 2100.0;
  gains.S1P0 = 2200.0;
  gains.S1P1 = 2300.0;
  simulator->setReceiverGains(antennaNumber, gains);

  // set location
  SHARED_SIMULATOR::Location loc;
  loc.x = 100.0;
  loc.y = 1.0;
  loc.z = 200.0;
  simulator->setAntennaLocation(antennaNumber, loc);
  // set axes offsets
  simulator->setAxesOffset(3, 0.001);

  // variable attenuators
  SHARED_SIMULATOR::DownConvAttenuators dcatt;
  dcatt.S0P0 = 10.0;
  dcatt.S0P1 = 12.0;
  dcatt.S1P0 = 14.0;
  dcatt.S1P1 = 16.0;
  simulator->setDownConvAttenuators(antennaNumber, dcatt);

  SHARED_SIMULATOR::BaseBandAttenuators bbatt;
  bbatt.AP0 = 3.0;
  bbatt.AP1 = 3.1;
  bbatt.BP0 = 3.2;
  bbatt.BP1 = 3.3;
  bbatt.CP0 = 3.4;
  bbatt.CP1 = 3.5;
  bbatt.DP0 = 3.6;
  bbatt.DP1 = 3.7;
  simulator->setBaseBandAttenuators(antennaNumber, bbatt);

  // DDS parameters
  SHARED_SIMULATOR::DDS_Parameters ddspars;
  ddspars.timeStamp = timestamp.value;      // Time Stamp for this position
  ddspars.antennaNum = antennaNumber;           // Antenna number (hardware)
  ddspars.initialPhase = antennaNumber*M_PI/2.0;  // Initial phase (in radians)
  ddspars.phaseRate = 2*M_PI*antennaNumber;       // Phase rate (in radians / s)
  simulator->setDDS_Parameters(SHARED_SIMULATOR::FIRST,
			       SHARED_SIMULATOR::ZERO, ddspars);
  ddspars.timeStamp = timestamp.value;      // Time Stamp for this position
  ddspars.antennaNum = antennaNumber;           // Antenna number (hardware)
  ddspars.initialPhase = 0;  // Initial phase (in radians)
  ddspars.phaseRate = 2*M_PI/10.0*antennaNumber;       // Phase rate (in radians / s)
  simulator->setDDS_Parameters(SHARED_SIMULATOR::SECOND,
			       SHARED_SIMULATOR::ZERO, ddspars);

  // Set up the antenna directions
  SHARED_SIMULATOR::AntAzEl azel;
  azel.timeStamp = timestamp.value;   // Time Stamp for this position
  azel.antennaNum = antennaNumber; // Antenna number (hardware)
  azel.antAz = 95.0*M_PI/180.0;  // Azimuth in radians
  azel.antEl = 20.0*M_PI/180.0; // Elevation in radians
  simulator->setAntAzEl(azel);

  // Set up sub reflector positions
  SHARED_SIMULATOR::SubreflectorPosition srp;
  srp.timeStamp = timestamp.value;
  srp.antennaNum = antennaNumber;
  srp.x = 0.0;
  srp.y = 0.0;
  srp.z = 0.001;
  simulator->setSubrefPos(srp);

  // Set up the delay
  SHARED_SIMULATOR::Delay delay;
  delay.timeStamp = timestamp.value;
  delay.totalDelay = 100.e-9*antennaNumber;
}

/*___oOo___*/
