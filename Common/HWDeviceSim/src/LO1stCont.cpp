/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2002 
*
* This code is under GNU General Public Licence (GPL). 
* Correspondence concerning ALMA Software should be addressed to:
* alma-sw-admin@nrao.edu
*
* Copyright (C) 2001
* Associated Universities, Inc. Washington DC, USA.
*
* Produced for the ALMA project
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Library General Public License
* as published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Library General Public License for more details.
*
* You should have received a copy of the GNU Library General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* aperrigo  08/12/02  created 
*/

#include <iomanip>
#include "LO1stCont.h"
#include "AMBUtil.h"

// -----------------------------------------------------------------------------

AMB::LO1stCont::LO1stCont(
	node_t node,
	const std::vector<CAN::byte_t> &serialNumber)
    : node_m(node), sn_m(serialNumber), trans_num_m(0)
{
    std::cout << "Creating node " << node << ", s/n 0x";
    for (int i = 0; i < 8; i++)
	std::cout << std::hex << std::setw(2) << std::setfill('0') << (int)serialNumber[i];
    std::cout << ", LO1stCont device" << std::endl;
}

// -----------------------------------------------------------------------------

AMB::LO1stCont::~LO1stCont()
{
    // Nothing
}

// -----------------------------------------------------------------------------

AMB::node_t AMB::LO1stCont::node() const
{
    return node_m;
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::LO1stCont::serialNumber() const
{
    return sn_m;
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::LO1stCont::monitor(rca_t rca) const
{
    trans_num_m++;
    const std::vector<CAN::byte_t> tmp;

    switch (rca) {
    // Mandatory generic points
    case GET_CAN_ERROR:
	return get_can_error();
	break;
    case GET_PROTOCOL_REV_LEVEL:
	return get_protocol_rev_level();
	break;
	case AMB::Device::GET_SW_REV_LEVEL:
		return get_software_rev_level();
		break;
	case AMB::Device::GET_HW_REV_LEVEL:
		return get_hardware_rev_level();
		break;
    case GET_TRANS_NUM:
	return longToData(tmp, get_trans_num());
	break;
    // Optional generic point
    case GET_AMBIENT_TEMPERATURE:
	return floatDS1820ToData(tmp, get_ambient_temperature());
	break;

    // FE 1st LO control specific points
    case GET_LO_CONTROL_STATUS:
	return get_lo_control_status();
	break;
    case GET_GUNN_VOLTAGE_B3:
	return IEEEFloatToData(get_gunn_voltage_b3());
	break;
    case GET_GUNN_VOLTAGE_B6:
	return IEEEFloatToData(get_gunn_voltage_b6());
	break;
    case GET_GUNN_TUNE_COUNTS_B3:
	return shortToData(tmp, get_gunn_tune_counts_b3());
	    break;
    case GET_GUNN_BACKSHORT_COUNTS_B3:
	return shortToData(tmp, get_gunn_backshort_counts_b3());
	    break;
    case GET_GUNN_TUNE_COUNTS_B6:
	return shortToData(tmp, get_gunn_tune_counts_b6());
	    break;
    case GET_GUNN_BACKSHORT_COUNTS_B6:
	return shortToData(tmp, get_gunn_backshort_counts_b6());
	    break;
    case GET_IF_POWER:
	return IEEEFloatToData(get_if_power());
	break;
    case GET_GUNN_TEMP_B3:
	return IEEEFloatToData(get_gunn_temp_b3());
	break;
    case GET_GUNN_TEMP_B6:
	return IEEEFloatToData(get_gunn_temp_b6());
	break;
    case GET_PHOTO_CURRENT_B3:
	return IEEEFloatToData(get_photo_current_b3());
	break;
    case GET_PHOTO_VOLTAGE_B3:
	return IEEEFloatToData(get_photo_voltage_b3());
	break;
    case GET_PHOTO_CURRENT_B6:
	return IEEEFloatToData(get_photo_current_b6());
	break;
    case GET_PHOTO_VOLTAGE_B6:
	return IEEEFloatToData(get_photo_voltage_b6());
	break;
    case GET_DRO_MON_VOLTAGE:
	return IEEEFloatToData(get_dro_mon_voltage());
	break;
    default:
	std::cerr << "Switch does not match any case" << std::endl;
	throw LO1stContError("Unknown RCA in monitor command");
    }
}

// -----------------------------------------------------------------------------

void AMB::LO1stCont::control(rca_t rca, const std::vector<CAN::byte_t> &data)
{
    trans_num_m++;
    switch (rca) {
    case SET_GUNN_TUNE_COUNTS_B3:
    {
    if (data.size() != 2) 
	{
	throw LO1stContError(
	    "SET_GUNN_TUNE_COUNTS_B3 data length must be 2");
	}
    set_gunn_tune_counts_b3(data);
    }
    break;
    case SET_GUNN_BACKSHORT_COUNTS_B3:
    {
    if (data.size() != 2) 
	{
	throw LO1stContError(
	    "SET_GUNN_BACKSHORT_COUNTS_B3 data length must be 2");
	}
    set_gunn_backshort_counts_b3(data);
    }
    break;
    case SET_GUNN_TUNE_COUNTS_B6:
    {
    if (data.size() != 2) 
	{
	throw LO1stContError(
	    "SET_GUNN_TUNE_COUNTS_B6 data length must be 1");
	}
    set_gunn_tune_counts_b6(data);
    }
    break;
    case SET_GUNN_BACKSHORT_COUNTS_B6:
    {
    if (data.size() != 2) 
	{
	throw LO1stContError(
	    "SET_GUNN_BACKSHORT_COUNTS_B6 data length must be 2");
	}
    set_gunn_backshort_counts_b6(data);
    }
    break;
    case SET_LO_POWER_B6:
    {
    if (data.size() != 2) 
	{
	throw LO1stContError("SET_LO_POWER_B6 data length must be 2");
	}
    set_lo_power_b6(data);
    }
    break;
    case SET_MODULATOR_SERVO:
    {
    if (data.size() != 1)
	{
	throw LO1stContError("SET_MODULATOR_SERVO data length must be 1");
	}
    set_modulator_servo(data[0]);
    }
    break;
    case SET_DRIVE_ENABLE:
    {
    if (data.size() != 1) 
	{
	throw LO1stContError("SET_DRIVE_ENABLE data length must be 1");
	}
    set_drive_enable(data[0]);
    }
    break;
    case SET_LOOP_ENABLE:
    {
    if (data.size() != 1) 
	{
	throw LO1stContError("SET_LOOP_ENABLE data length must be 1");
	}
    set_loop_enable(data[0]);
    }
    break;
    case SET_NOMINAL_VOLTAGE:
    {
    float value = 0.0;
    if (!validIEEEFloat(data, value)) 
	{
	throw LO1stContError("SET_NOMINAL_VOLTAGE invalid IEEE Float");
	}
    set_nominal_voltage(value);
    }
    break;
    case SET_GUNN_LOOP_GAIN:
    {
    float value = 0.0;
    if (!validIEEEFloat(data, value)) 
	{
	throw LO1stContError("SET_GUNN_LOOP_GAIN invalid IEEE Float");
	}
    set_gunn_loop_gain(value);
    }
    break;
    case SET_GUNN_TUNE_RANGE:
    {
    float value = 0.0;
    if (!validIEEEFloat(data, value)) 
	{
	throw LO1stContError("SET_GUNN_TUNE_RANGE invalid IEEE Float");
	}
    set_gun_tune_range(value);
    }
    break;
    case SET_GUNN_SELECT:
    {
    if (data.size() != 1) 
	{
	throw LO1stContError("SET_GUNN_SELECT data length must be 1");
	}
    set_gunn_select(data[0]);
    }
    break;
    case SET_FTS_FREQUENCY:
    {
    float value = 0.0;
    if (!validIEEEFloat(data, value)) 
	{
	throw LO1stContError("SET_FTS_FREQUENCY invalid IEEE Float");
	}
    set_fts_frequency(value);
    }
    break;
    case SET_PHOTOMIXER_B3:
    {
    if (data.size() != 1) 
	{
	throw LO1stContError("SET_PHOTOMIXER_B3 data length must be 1");
	}
    set_photomixer_b3(data[0]);
    }
    break;
    case SET_PHOTOMIXER_B6:
    {
    if (data.size() != 1) 
	{
	throw LO1stContError("SET_PHOTOMIXER_B6 data length must be 1");
	}
    set_photomixer_b6(data[0]);
    }
    break;
    case SET_BAND_SELECT:
    {
    if (data.size() != 1) 
	{
	throw LO1stContError("SET_BAND_SELECT data length must be 1");
	}
    set_band_select(data[0]);
    }
    break;
    default:
	throw LO1stContError("Unknown RCA in control command");
    }
}

// -----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::LO1stCont::get_can_error() const
{
    return AMB::Device::get_can_error();
}

// -----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::LO1stCont::get_protocol_rev_level() const
{
    return AMB::Device::get_protocol_rev_level();
}

// -----------------------------------------------------------------------------
unsigned int AMB::LO1stCont::get_trans_num() const
{
    return trans_num_m;
}

// -----------------------------------------------------------------------------
float AMB::LO1stCont::get_ambient_temperature() const
{
    return 18.0;
}

// -----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::LO1stCont:: get_lo_control_status() const
{
    std::vector<CAN::byte_t> tmp(1);
    tmp[0] = 0x55;
    return tmp;
}

// -----------------------------------------------------------------------------
float AMB::LO1stCont:: get_gunn_voltage_b3() const
{
    return 10.;
}

// -----------------------------------------------------------------------------
float AMB::LO1stCont:: get_gunn_voltage_b6() const
{
    return 11.;
}

// -----------------------------------------------------------------------------
unsigned short AMB::LO1stCont::get_gunn_tune_counts_b3() const
{
    return gunn_tune_counts_b3_m;
}

// -----------------------------------------------------------------------------
unsigned short AMB::LO1stCont::get_gunn_backshort_counts_b3() const
{
    return gunn_backshort_counts_b3_m;
}

// -----------------------------------------------------------------------------
unsigned short AMB::LO1stCont::get_gunn_tune_counts_b6() const
{
    return gunn_tune_counts_b6_m;
}

// -----------------------------------------------------------------------------
unsigned short AMB::LO1stCont::get_gunn_backshort_counts_b6() const
{
    return gunn_backshort_counts_b6_m;
}

// -----------------------------------------------------------------------------
float AMB::LO1stCont::get_if_power() const
{
    return 12.;
}

// -----------------------------------------------------------------------------
float AMB::LO1stCont::get_gunn_temp_b3() const
{
    return 13.;
}

// -----------------------------------------------------------------------------
float AMB::LO1stCont::get_gunn_temp_b6() const
{
    return 14.;
}

// -----------------------------------------------------------------------------
float AMB::LO1stCont::get_photo_current_b3() const
{
    return 15.;
}

// -----------------------------------------------------------------------------
float AMB::LO1stCont::get_photo_voltage_b3() const
{
    return 16.;
}

// -----------------------------------------------------------------------------
float AMB::LO1stCont::get_photo_current_b6() const
{
    return 17.;
}

// -----------------------------------------------------------------------------
float AMB::LO1stCont::get_photo_voltage_b6() const
{
    return 18.;
}

// -----------------------------------------------------------------------------
float AMB::LO1stCont::get_dro_mon_voltage() const
{
    return 19.;
}

// -----------------------------------------------------------------------------

void AMB::LO1stCont::set_gunn_tune_counts_b3(std::vector<CAN::byte_t> data)
{
    gunn_tune_counts_b3_m = dataToShort(data);
}

void AMB::LO1stCont::set_gunn_backshort_counts_b3(std::vector<CAN::byte_t> data)
{
    gunn_backshort_counts_b3_m = dataToShort(data);
}

void AMB::LO1stCont::set_gunn_tune_counts_b6(std::vector<CAN::byte_t> data)
{
    gunn_tune_counts_b6_m = dataToShort(data);
}

void AMB::LO1stCont::set_gunn_backshort_counts_b6(std::vector<CAN::byte_t> data)
{
    gunn_backshort_counts_b6_m = dataToShort(data);
}

void AMB::LO1stCont::set_lo_power_b6(std::vector<CAN::byte_t> data)
{
    unsigned short value;
    value = dataToShort(data);
}

void AMB::LO1stCont::set_modulator_servo(CAN::byte_t cmd)
{
}

void AMB::LO1stCont::set_drive_enable(CAN::byte_t cmd)
{
}

void AMB::LO1stCont::set_loop_enable(CAN::byte_t cmd)
{
}

void AMB::LO1stCont::set_nominal_voltage(float value)
{
}

void AMB::LO1stCont::set_gunn_loop_gain(float value)
{
}

void AMB::LO1stCont::set_gun_tune_range(float value)
{
}

void AMB::LO1stCont::set_gunn_select(CAN::byte_t cmd)
{
}

void AMB::LO1stCont::set_fts_frequency(float value)
{
}

void AMB::LO1stCont::set_photomixer_b3(CAN::byte_t cmd)
{
}

void AMB::LO1stCont::set_photomixer_b6(CAN::byte_t cmd)
{
}

void AMB::LO1stCont::set_band_select(CAN::byte_t cmd)
{
}

// ----------------------------------------------------------------------------
/// Converts the bytes in data to a float.  If successful, returns true and
/// value contains float.  Otherwise returns false.
bool AMB::LO1stCont::validIEEEFloat(std::vector<CAN::byte_t> data, float &value) 
{
    if (data.size() == 4)
    {
	std::vector<CAN::byte_t> tmp(data);
	long tmpVal =  dataToLong(tmp);
	float *junk = reinterpret_cast<float *> (&tmpVal);
	value = *junk;
	return true;
    }
    else
    { 
	return false;
    }
}

// ----------------------------------------------------------------------------
/// Converts the float to data bytes.  Unlike AMB::floatToData this function
/// uses IEEE 754-1990 format
std::vector<CAN::byte_t> AMB::LO1stCont::IEEEFloatToData(float value) const 
{
    const std::vector<CAN::byte_t> tmp;
    unsigned long *tmpLong;
    float tmpFloat = value;
    tmpLong = reinterpret_cast<unsigned long *> (&tmpFloat);
    return longToData(tmp, *tmpLong);
}
