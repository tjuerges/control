// @(#) $Id$
//
// Copyright (C) 2005
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Library General Public License
// as published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Library General Public License for more details.
//
// You should have received a copy of the GNU Library General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
//
// Correspondence concerning ALMA should be addressed as follows:
//
//        Internet email: alma-sw-admin@nrao.edu
//

#include <iomanip>
#include <iostream>
#include <sstream>
#include "DCtoDCPowerSupply.h"
#include "AMBUtil.h"


// ---------------------------------------------------------------------------

AMB::DCtoDCPowerSupply::DCtoDCPowerSupply(node_t node,
		const std::vector<CAN::byte_t> &serialNumber) :
	node_m(node),
			sn_m(serialNumber),
			trans_num_m(0),
			shutdown_m(false),
			error_and_alarms_m(0xFB0000), //all error MSBLSB
			voltage_7v_m(7.1), current_7v_m(3.56), voltage_minus_7v_m(-6.9),
			current_minus_7v_m(4.14), voltage_17v_m(17.3),
			current_17v_m(13.56), voltage_minus_17v_m(-16.4),
			current_minus_17v_m(11.51) {
	std::cout << "Creating node " << node << ",s/n 0x";
	for (int i = 0; i < 8; i++) {
		std::cout << std::hex << std::setw(2) << std::setfill('0')
				<< static_cast<int>(serialNumber[i]);
	}
	std::cout << ", PSA Power Supply" << std::endl;
	
	//initialize max_min_7v_m[]   0x02d202950170
	max_min_7v_m[0]=0x02;
	max_min_7v_m[1]=0xd2;
	max_min_7v_m[2]=0x02;
	max_min_7v_m[3]=0x95;
	max_min_7v_m[4]=0x01;
	max_min_7v_m[5]=0x70;

	max_min_minus_7v_m[0]=0x02;
	max_min_minus_7v_m[1]=0xd2;
	max_min_minus_7v_m[2]=0x02;
	max_min_minus_7v_m[3]=0x95;
	max_min_minus_7v_m[4]=0x01;
	max_min_minus_7v_m[5]=0x70;

	max_min_17v_a_m[0]=0x02;
	max_min_17v_a_m[1]=0xd2;
	max_min_17v_a_m[2]=0x02;
	max_min_17v_a_m[3]=0x95;
	max_min_17v_a_m[4]=0x01;
	max_min_17v_a_m[5]=0x70;

	max_min_minus_17v_m[0]=0x02;
	max_min_minus_17v_m[1]=0xd2;
	max_min_minus_17v_m[2]=0x02;
	max_min_minus_17v_m[3]=0x95;
	max_min_minus_17v_m[4]=0x01;
	max_min_minus_17v_m[5]=0x70;

	max_min_17v_b_m[0]=0x02;
	max_min_17v_b_m[1]=0xd2;
	max_min_17v_b_m[2]=0x02;
	max_min_17v_b_m[3]=0x95;
	max_min_17v_b_m[4]=0x01;
	max_min_17v_b_m[5]=0x70;

}

AMB::DCtoDCPowerSupply::~DCtoDCPowerSupply() {
  // Nothing
}

AMB::node_t AMB::DCtoDCPowerSupply::node() const {
    return node_m;
}

std::vector<CAN::byte_t> AMB::DCtoDCPowerSupply::serialNumber() const {
  return sn_m;
}

std::vector<CAN::byte_t> AMB::DCtoDCPowerSupply::monitor(rca_t rca) const {
  trans_num_m++;
  const std::vector<CAN::byte_t> tmp;
  std::vector<CAN::byte_t> retVal;
  
  switch (rca) {
    // Mandatory generic points
  case GET_SERIAL_NUMBER:
    return serialNumber();
    break;
  case GET_CAN_ERROR:
    return get_can_error();
    break;
  case GET_PROTOCOL_REV_LEVEL:
    return get_protocol_rev_level();
    break;
	case AMB::Device::GET_SW_REV_LEVEL:
		return get_software_rev_level();
		break;
	case AMB::Device::GET_HW_REV_LEVEL:
		return get_hardware_rev_level();
		break;
  case GET_TRANS_NUM:
    return longToData(tmp, get_trans_num());
    break;
    // Optional generic point
  case GET_AMBIENT_TEMPERATURE:
    return floatDS1820ToData(tmp, get_ambient_temperature());
    break;
    // DCtoDCPowerSupply specific points
  case ERROR_AND_ALARMS:
  	  return error_and_alarms();
  	  break;
  case VOLTAGE_7V:
    return voltage_7v();
    break;          
  case CURRENT_7V:
    return current_7v();
    break;          
  case VOLTAGE_MINUS_7V:
    return voltage_minus_7v();
    break;          
  case CURRENT_MINUS_7V:
    return current_minus_7v();
    break;          
  case VOLTAGE_17V:
    return voltage_17v();
    break;          
  case CURRENT_17V:
    return current_17v();
    break;          
  case VOLTAGE_MINUS_17V:
    return voltage_minus_17v();
    break;          
  case CURRENT_MINUS_17V:
    return current_minus_17v();
    break;
  case MAX_MIN_7V:
	  return max_min_7v();
	  break;
  
  case MAX_MIN_MINUS_7V:
	  return max_min_minus_7v();
	  break;
	  
  case MAX_MIN_17V_A:
	  return max_min_17v_a();
	  break;
  
  case MAX_MIN_MINUS_17V:
	  return max_min_minus_17v();
	  break;
  
  case MAX_MIN_17V_B:
	  return max_min_17v_b();
	  break;
      
  default:
    std::cerr << "Switch does not match any case" << std::endl;
    throw DCtoDCPowerSupplyError("Unknown RCA in monitor command");
  }
}

void AMB::DCtoDCPowerSupply::control(rca_t rca,
				     const std::vector<CAN::byte_t>& data) {
  trans_num_m++;
  switch (rca) {
  case SHUTDOWN_COM:
    shutdown_com(data);
    break;
  default:
    throw DCtoDCPowerSupplyError("Unknown RCA in control command");
  }
}

unsigned int AMB::DCtoDCPowerSupply::get_trans_num() const {
  return trans_num_m;
}

float AMB::DCtoDCPowerSupply::get_ambient_temperature() const {
  return 18.0;
}

std::vector<CAN::byte_t> AMB::DCtoDCPowerSupply::error_and_alarms() const {
  std::vector<CAN::byte_t> tmp(3);
  unsigned long int aux=error_and_alarms_m;
  for (int i=2;i>=0;i--){
	  tmp[i]= aux & 0x0000FF;
	 // std::cerr << "AMB::PSD::error_and_alarms(). tmp["<<i<<"]="<< static_cast<int>(tmp[i]) << std::endl;
	  aux>>=8;
  }
  return tmp;
}
std::vector<CAN::byte_t> AMB::DCtoDCPowerSupply::voltage_7v() const {
  const std::vector<CAN::byte_t> tmp;
  const short int roundedValue = shutdown_m ? 0 :
    static_cast<short int>(voltage_7v_m*4095.0/20.0) & 0x0FFF;
  return shortToData(tmp, roundedValue);
}

std::vector<CAN::byte_t> AMB::DCtoDCPowerSupply::current_7v() const {
  const std::vector<CAN::byte_t> tmp;
  const short int roundedValue = shutdown_m ? 0 :
    static_cast<short int>(current_7v_m*4095.0/20.0/6.0) & 0x0FFF;
  return shortToData(tmp, roundedValue);
}

std::vector<CAN::byte_t> AMB::DCtoDCPowerSupply::voltage_minus_7v() const {
  const std::vector<CAN::byte_t> tmp;
  const short int roundedValue = shutdown_m ? 0 :
    static_cast<short int>(voltage_minus_7v_m*4095.0/20.0) & 0x0FFF;
  return shortToData(tmp, roundedValue);
}

std::vector<CAN::byte_t> AMB::DCtoDCPowerSupply::current_minus_7v() const {
  const std::vector<CAN::byte_t> tmp;
  const short int roundedValue = shutdown_m ? 0 :
    static_cast<short int>(current_minus_7v_m*4095.0/20.0/6.0) & 0x0FFF;
  return shortToData(tmp, roundedValue);
}

std::vector<CAN::byte_t> AMB::DCtoDCPowerSupply::voltage_17v() const {
  const std::vector<CAN::byte_t> tmp;
  const short int roundedValue = shutdown_m ? 0 :
    static_cast<short int>(voltage_17v_m*4095.0/20.0) & 0x0FFF;
  return shortToData(tmp, roundedValue);
}

std::vector<CAN::byte_t> AMB::DCtoDCPowerSupply::current_17v() const {
  const std::vector<CAN::byte_t> tmp;
  const short int roundedValue = shutdown_m ? 0 :
    static_cast<short int>(current_17v_m*4095.0/20.0/6.0)  & 0x0FFF;
  return shortToData(tmp, roundedValue);
}

std::vector<CAN::byte_t> AMB::DCtoDCPowerSupply::voltage_minus_17v() const {
  const std::vector<CAN::byte_t> tmp;
  const short int roundedValue = shutdown_m ? 0 :
    static_cast<short int>(voltage_minus_17v_m*4095.0/20.0)  & 0x0FFF;
  return shortToData(tmp, roundedValue);
}

std::vector<CAN::byte_t> AMB::DCtoDCPowerSupply::current_minus_17v() const {
  const std::vector<CAN::byte_t> tmp;
  const short int roundedValue = shutdown_m ? 0 :
    static_cast<short int>(current_minus_17v_m*4095.0/20.0/6.0) & 0x0FFF;
  return shortToData(tmp, roundedValue);
}
std::vector<CAN::byte_t> AMB::DCtoDCPowerSupply::max_min_7v() const {
  std::vector<CAN::byte_t> tmp(6);
      tmp[0] = (unsigned char)max_min_7v_m[0];
      tmp[1] = (unsigned char)max_min_7v_m[1];
      tmp[2] = (unsigned char)max_min_7v_m[2];
      tmp[3] = (unsigned char)max_min_7v_m[3];
      tmp[4] = (unsigned char)max_min_7v_m[4];
      tmp[5] = (unsigned char)max_min_7v_m[5];
      return tmp;
}

std::vector<CAN::byte_t> AMB::DCtoDCPowerSupply::max_min_minus_7v() const {
  std::vector<CAN::byte_t> tmp(6);
      tmp[0] = (unsigned char)max_min_7v_m[0];
      tmp[1] = (unsigned char)max_min_7v_m[1];
      tmp[2] = (unsigned char)max_min_7v_m[2];
      tmp[3] = (unsigned char)max_min_7v_m[3];
      tmp[4] = (unsigned char)max_min_7v_m[4];
      tmp[5] = (unsigned char)max_min_7v_m[5];
      return tmp;
}

std::vector<CAN::byte_t> AMB::DCtoDCPowerSupply::max_min_17v_a() const {
  std::vector<CAN::byte_t> tmp(6);
      tmp[0] = (unsigned char)max_min_7v_m[0];
      tmp[1] = (unsigned char)max_min_7v_m[1];
      tmp[2] = (unsigned char)max_min_7v_m[2];
      tmp[3] = (unsigned char)max_min_7v_m[3];
      tmp[4] = (unsigned char)max_min_7v_m[4];
      tmp[5] = (unsigned char)max_min_7v_m[5];
      return tmp;
}

std::vector<CAN::byte_t> AMB::DCtoDCPowerSupply::max_min_minus_17v() const {
  std::vector<CAN::byte_t> tmp(6);
      tmp[0] = (unsigned char)max_min_7v_m[0];
      tmp[1] = (unsigned char)max_min_7v_m[1];
      tmp[2] = (unsigned char)max_min_7v_m[2];
      tmp[3] = (unsigned char)max_min_7v_m[3];
      tmp[4] = (unsigned char)max_min_7v_m[4];
      tmp[5] = (unsigned char)max_min_7v_m[5];
      return tmp;
}

std::vector<CAN::byte_t> AMB::DCtoDCPowerSupply::max_min_17v_b() const {
  std::vector<CAN::byte_t> tmp(6);
      tmp[0] = (unsigned char)max_min_7v_m[0];
      tmp[1] = (unsigned char)max_min_7v_m[1];
      tmp[2] = (unsigned char)max_min_7v_m[2];
      tmp[3] = (unsigned char)max_min_7v_m[3];
      tmp[4] = (unsigned char)max_min_7v_m[4];
      tmp[5] = (unsigned char)max_min_7v_m[5];
      return tmp;
}

void AMB::DCtoDCPowerSupply::
shutdown_com(const std::vector<CAN::byte_t>& command) {
  if (command.size() != 1) {
    std::ostringstream errMessage;
    errMessage << "SHUTDOWN_COM command is incorrect length." << std::endl;
    errMessage << "Should be 1 and the actual value is " 
	       <<  command.size() << std::endl;
    throw DCtoDCPowerSupplyError(errMessage.str());
  }
  if ((command[0] & 0xFE) != 0x00) {
    std::ostringstream errMessage;
    errMessage << "SHUTDOWN_COM command has unexpected bits set." << std::endl;
    errMessage << "The top 7-bits should be zero but 0x" 
	       << std::hex << std::setw(2) << std::setfill('0')
	       << static_cast<int>(command[0]) << " was received."
	       << std::endl;
    throw DCtoDCPowerSupplyError(errMessage.str());
  }
  if ((command[0] & 0x01) == 0x01) {
    shutdown_m = true;
  } else {
    shutdown_m = false;
  }
}

