// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include "AMBUtil.h"
#include "CANError.h"

#include <math.h>
#include <sys/time.h>
#include <unistd.h>
#include <endian.h>

// -----------------------------------------------------------------------------

bool AMB::validateNodeNumber(
    std::string& error,
    node_t node)
{
    bool ok =  node <= MAX_AMB_NODE_NUMBER;
    if ( ! ok)
	{
	error = "node must be in the range [0..MAX_AMB_NODE_NUMBER]";
	}
    return ok;
}

// -----------------------------------------------------------------------------

void AMB::padSerialNumber(
    std::vector<CAN::byte_t>& serialNumber)
{
    const int size = serialNumber.size();
    if (size > 8)
	{
	throw CAN::Error("Serial number must not be greater than 8 bytes");
	}
    serialNumber.insert(serialNumber.end(),8 - size,'\0');
}

// -----------------------------------------------------------------------------

bool AMB::validateSerialNumber(
    std::string& error,
    const std::vector<CAN::byte_t> &serialNumber)
{
    bool ok =  serialNumber.size() == 8;
    if ( ! ok)
	{
	error = "Serial Number must be 8 character long exactly";
	}
    return ok;
}

// -----------------------------------------------------------------------------

CAN::id_t AMB::toID(
    node_t node,
    rca_t rca)
{
    // 1 << 18 == 2^18
    return (node + 1) * (1 << 18) + rca;
}

// -----------------------------------------------------------------------------

void AMB::fromID(
    node_t& node,
    rca_t& rca,
    id_t id)
{
    node = (id >> 18) - 1;      // node 0 = 0x40000
    rca = id & 0x3ffff;         // RCA is first 18 bits
}

// -----------------------------------------------------------------------------

double AMB::difftime(
    const timeval &start)
{
    timeval tmp;
    gettimeofday(&tmp,0);
    double interval = 1.0 * (tmp.tv_sec - start.tv_sec)
	              + 1.0E-6 * (tmp.tv_usec - start.tv_usec);
    return interval;
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::doubleToData(
    double value)
{
    std::vector<CAN::byte_t> rtnVal;
    char* byteptr = (char*)(&value);
#if __BYTE_ORDER == __LITTLE_ENDIAN
    for (int i = 7; i >= 0; i--)
#else
    for (int i = 0; i < 8; i++)
#endif
	{
        rtnVal.push_back(byteptr[i]);
	}

    return rtnVal;
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::doubleToData(
    const std::vector<CAN::byte_t>& dataIn,
    double value,
    int nbytes)
{
    if (dataIn.size() + nbytes > 8)
	{
        throw CAN::Error("CAN data length must be <= 8");
	}
    std::vector<CAN::byte_t> rtnVal = dataIn;
    char* byteptr = (char*)&value;
#if __BYTE_ORDER == __LITTLE_ENDIAN
    for (int i = nbytes - 1; i >= 0; i--)
#else
    for (int i = 0; i < nbytes; i++)
#endif
	{
        rtnVal.push_back(byteptr[i]);
	}
    return rtnVal;
}

// ----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::floatToData(float value) {
  std::vector<CAN::byte_t> rtnVal;
  char* byteptr = reinterpret_cast<char*>(&value);
#if __BYTE_ORDER == __LITTLE_ENDIAN
  for (int i = 3; i >= 0; i--) {
#else
  for (int i = 0; i < 4; i++) {
#endif
    rtnVal.push_back(byteptr[i]);
  }  
  return rtnVal;
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::floatToData(
    const std::vector<CAN::byte_t>& dataIn,
    double value,
    double lowRange,
    double highRange,
    unsigned int nbits)
{
    // check arguments
    if (nbits > 32)
	{
	throw CAN::Error("Can only encode floats in <= 32 bits");
	}
    if (lowRange >= highRange) 
	{
	throw CAN::Error("Illegal range: low >= high");
	}

    // clamp value
    if (value < lowRange)
	{
	value = lowRange;
	}
    if (value > highRange)
	{
	value = highRange;
	}

    // calc number of bytes to insert
    int nbytes;		// char, short or int(32)
    if (nbits >= 17)
	{
	nbytes = 4;
	}
    else if (nbits >= 9)
	{
	nbytes = 2;
	}
    else
	{
	nbytes = 1;
	}

    // zero all CAN message bits to be written during this call
    std::vector<CAN::byte_t> tmp = dataIn; 
    tmp.insert(tmp.end(),nbytes,'\0');	// null pad with nbytes 0's.

    // check there is enough empty bytes in message
    if (tmp.size() > 8)
	{
	throw CAN::Error("CAN data length must be <= 8");
	}

    // scale value and convert to integer of "nbits" length
    double fraction = (value - lowRange) / (highRange - lowRange);
    unsigned long bits;
    double maxint = (long long)1 << nbits;
    bits = (unsigned long)(fraction * maxint);

    // insert integer value into CAN message
    char* byteptr = (char*)(&bits);
#if __BYTE_ORDER == __LITTLE_ENDIAN
    for (int i = 0; i <  nbytes; i++)
#else
    for (int i = nbytes - 1; i >= 0; i--)
#endif
	{
	// BUG:  AMB originally had statement tmp[i] instead of finding
	// the size of input data array and starting at that point.  This
	// effectively wiped out all data points except the last one input
	// into the message.
	tmp[((int)tmp.size() - nbytes) + i] = byteptr[nbytes - 1 - i];
	}

    return tmp;
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::floatDS1820ToData(
    const std::vector<CAN::byte_t>& dataIn,
    double value)
{
    double frac_part;
    char temp;

    // where our temperature will begin in CAN message
    int offset = dataIn.size();

    // zero all CAN message bits to be written during this call
    std::vector<CAN::byte_t> tmp = dataIn;
    tmp.insert(tmp.end(),4,'\0');
    if (tmp.size() > 8)
	{
	throw CAN::Error("CAN data length must be <= 8");
	}

    // set count per C to 100
    tmp[offset+3] = 100; // countPerC

    // get the integer part of the temperature
    temp = (char)value;

    // get fractional part
    frac_part = value - (double)temp;

    // calculate countRemain
    frac_part *= 100.0;
    tmp[offset+2] = 75 - (char)frac_part;

    // set sign byte
    if (value < 0.0)
	{
	tmp[offset+0] = 0xff;
	}
    else
	{
	tmp[offset+0] = 0x00;
	}

    // remove sign bit
    tmp[offset+1] = temp << 1; // This had better set bit zero to 0!

    return tmp;
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::longlongToData(
    const std::vector<CAN::byte_t>& dataIn,
    unsigned long long value,
    int nbytes)
{
    if (dataIn.size() + nbytes > 8)
	{
        throw CAN::Error("CAN data length must be <= 8");
	}

    std::vector<CAN::byte_t> rtnVal = dataIn;
    char* byteptr = (char*)&value;
#if __BYTE_ORDER == __LITTLE_ENDIAN
    for (int i = nbytes - 1; i >= 0; i--)
#else
    for (int i = 0; i < nbytes; i++)
#endif
	{
        rtnVal.push_back(byteptr[i]);
	}
    return rtnVal;
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::longToData(
    const std::vector<CAN::byte_t>& dataIn,
    long value)
{
    return AMB::longlongToData(dataIn,(long long)value,4);
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::shortToData(
    const std::vector<CAN::byte_t>& dataIn,
    short value)
{
    return AMB::longlongToData(dataIn,(long long)value,2);
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::charToData(
    const std::vector<CAN::byte_t>& dataIn,
    unsigned char value)
{
    return AMB::longlongToData(dataIn,(long long)value,1);
}

// -----------------------------------------------------------------------------

double AMB::dataToDouble(
    std::vector<CAN::byte_t>& dataInOut)
{
    if (dataInOut.size() != 8)
	{
	throw CAN::Error("CAN data length wrong");
	}

    double value;
    char* byteptr = (char*)(&value);
#if __BYTE_ORDER == __LITTLE_ENDIAN
    for (int i = 7; i >= 0; i--)
#else
    for (int i = 0; i < 8; i++)
#endif
	{
	byteptr[i] = dataInOut.front();
	dataInOut.erase(dataInOut.begin());
	}

    return value;
}

// ----------------------------------------------------------------------------

double AMB::dataToFloat(
    std::vector<CAN::byte_t>& dataInOut,
    double lowRange,
    double highRange,
    unsigned int nbits)
{
    // check arguments
    if (nbits > 32)
	{
	throw CAN::Error("Can only encode doubles in <= 32 bits");
	}
    if (lowRange >= highRange) 
	{
	throw CAN::Error("Illegal range: low >= high");
	}

    // determine number of bytes to remove
    int nbytes; // char,short or int(32)
    if (nbits >= 17)
	{
	nbytes = 4;
	}
    else if (nbits >= 9)
	{
	nbytes = 2;
	}
    else
	{
	nbytes = 1;
	}
    
    // check there is enough bytes in CAN message
    if (dataInOut.size() < (unsigned int)nbytes)
	{
	throw CAN::Error("CAN data length too short");
	}

    // remove bytes from CAN message
    unsigned long bits = 0;
    char* byteptr = (char*)(&bits);
#if __BYTE_ORDER == __LITTLE_ENDIAN
    for (int i = 0; i < nbytes; i++)
#else
    for (int i = nbytes - 1; i >= 0; i--)
#endif
	{
	byteptr[nbytes - 1 - i] = dataInOut[i];
	}
    dataInOut.erase(dataInOut.begin(),dataInOut.begin() + nbytes);

    // convert removed bytes to output double value
    double maxint = (long long)1 << nbits;
    return bits / maxint * (highRange - lowRange) + lowRange;
}

// ----------------------------------------------------------------------------

float AMB::dataToFloat( std::vector<CAN::byte_t>& dataInOut) {
  // Always remove 4 bytes.
  const int nbytes = 4; 
  
  // check there is enough bytes in CAN message
  if (dataInOut.size() < static_cast<unsigned int>(nbytes)) {
    throw CAN::Error("CAN data length too short");
  }

  // remove bytes from CAN message
  float bits = 0;
  char* byteptr = reinterpret_cast<char*>(&bits);
#if __BYTE_ORDER == __LITTLE_ENDIAN
  for (int i = 0; i < nbytes; i++)
#else
  for (int i = nbytes - 1; i >= 0; i--)
#endif
    {
      byteptr[nbytes - 1 - i] = dataInOut[i];
    }
  dataInOut.erase(dataInOut.begin(), dataInOut.begin() + nbytes);

  return bits;
}

// -----------------------------------------------------------------------------

double AMB::dataToFloatDS1820(
    std::vector<CAN::byte_t>& dataInOut)
{
    if (dataInOut.size() < 4)
	{
	throw CAN::Error("DS1820 temperature requires 4 bytes");
	}
    unsigned char msb = dataInOut[0];
    unsigned char lsb = dataInOut[1];

    float result = 0.0;
    char temp;

    const unsigned char countRemain = dataInOut[2];
    const unsigned char countPerC = dataInOut[3];

    dataInOut.erase(dataInOut.begin(),dataInOut.begin()+4);

    // shift the byte to remove the least significant bit
    temp = lsb >> 1;

    // Most significant byte indicates sign
    if (msb)
	{
	temp |= 0x80;
	}

    // Convert from two's complement signed 8 bit value to float
    result = (float)temp;

    // Calculation from p4 of the DS1820 Data Sheet
    if (fabs((double) countPerC) > 1e-9)
	{
	result += ((float)countPerC -
		   (float)countRemain) / ((float)countPerC) - 0.25;
	}

    return result;
}

// -----------------------------------------------------------------------------

unsigned long long AMB::dataToLonglong(
    std::vector<CAN::byte_t>& dataInOut,
    const int nbytes)
{
    if ((int)dataInOut.size() < nbytes)
	{
	throw CAN::Error("Data wrong length");
	}
    unsigned long long value = 0;
    char* byteptr = (char*)(&value);
#if __BYTE_ORDER == __LITTLE_ENDIAN
    for (int i = nbytes - 1; i >= 0; i--)
#else
    for (int i = 0; i < nbytes; i++)
#endif
	{
	byteptr[i] = dataInOut.front();
	dataInOut.erase(dataInOut.begin());
	}
    return value;
}

// -----------------------------------------------------------------------------

long AMB::dataToLong(
    std::vector<CAN::byte_t>& dataInOut)
{
    return (long)dataToLonglong(dataInOut,4);
}

// -----------------------------------------------------------------------------

short AMB::dataToShort(
    std::vector<CAN::byte_t>& dataInOut)
{
    return (short)dataToLonglong(dataInOut,2);
}

// -----------------------------------------------------------------------------

unsigned char AMB::dataToChar(
    std::vector<CAN::byte_t>& dataInOut)
{
    return (unsigned char)dataToLonglong(dataInOut,1);
}
