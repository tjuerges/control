// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include <iostream>
#include <iomanip>
#include "PTC.h"

using AMB::node_t;
using AMB::rca_t;

using namespace std;

// -----------------------------------------------------------------------------
AMB::PTC::PTC( node_t node,
	       const vector<CAN::byte_t>& serialNumber)
    :temp_m(100),
     displ_m(2),
     tilt_m(5),
     m_IA(0),m_CA(0),m_NPAE(0),m_AN(0),m_AW(0),m_IE(0),m_ECEC(0),m_C_GR(0), m_D_GR(0),m_RESERVED(0),
     m_deltaXposition(0), m_deltaYposition(0), m_deltaZposition(0),
     m_node(node),m_sn(serialNumber),m_trans_num(0)
{
    for (int i = 0; i < 100; i++) { // 20 to 30 degrees in steps of 0.1 degree
    temp_m[i] = 20.0 + static_cast<double>(i)/10.0;
    }
    for (int i = 0; i < 2; i++) { // Values are 1 and 1.1 um
    displ_m[i] = (1.0 + static_cast<double>(i)/10.0) * 1.0E-6;
    }
    
    for (int i = 0; i < 5; i++) { 
    tilt_m[i].x = (1.0 + static_cast<double>(i));
    tilt_m[i].y = (-1.0 - static_cast<double>(i));
    tilt_m[i].temp = (20.0 + static_cast<double>(i));
    }
    
    cout << "Creating node " << node << ", s/n 0x";
    for (int i = 0; i < 8; i++)
	cout << hex << setw(2) << setfill('0') << (int)serialNumber[i];
    cout << ", PTC device" << endl;
    
    string error;
    if ( ! validateNodeNumber(error,this->node()))
	{
	throw CAN::Error(error);
	}
    
    if ( ! validateSerialNumber(error,this->serialNumber()))
	{
	throw CAN::Error(error);
	}
}

// -----------------------------------------------------------------------------

AMB::PTC::~PTC()
{
    // Nothing
}

// -----------------------------------------------------------------------------

node_t AMB::PTC::node() const
{
    return m_node;
}

// -----------------------------------------------------------------------------

vector<CAN::byte_t> AMB::PTC::serialNumber() const
{
    return m_sn;
}

// -----------------------------------------------------------------------------

vector<CAN::byte_t> AMB::PTC::monitor(
    rca_t rca) const
{
    // empty CAN message
    const vector<CAN::byte_t> nostring;
    
    // bump transaction count
    m_trans_num++;
    
    switch (rca & 0X3FFFF)      // rca is first 18 bits
	{
	
	// mandatory standard generic monitor points
	
	case GET_PROTOCOL_REV_LEVEL:  // aka GET_SW_REV_LEVEL in ICD 9
	    return Device::get_protocol_rev_level();
	    
	case AMB::Device::GET_HW_REV_LEVEL:
	    return get_hardware_rev_level();
	    break;
	    
	case GET_CAN_ERROR:
	    return Device::get_can_error();
	    
	case GET_TRANS_NUM:
	    return longToData(nostring,get_trans_num());
	    
	case GET_SW_REV_LEVEL:  // aka GET_SYSTEM_ID in ICD 9
	{
	vector<CAN::byte_t> tmps = charToData(nostring,0xAB);  // major
	tmps = charToData(tmps,0xCD);      // minor
	return charToData(tmps,0xEF);      // release
	}
	
	// PTC specific monitor points (order from ICD)
	
	case GET_AC_STATUS:
	{
	return charToData(nostring,m_ac_status); 
	}
	    
	case GET_IP_ADDRESS:
	    return longToData(nostring,0x01020304);  // 1:2:3:4
	    
        case GET_METR_DISPL_1:
	    return displData(0);
	    
        case GET_METR_DISPL_2:
	    return displData(1);
	    
        case GET_METR_EQUIP_STATUS:
	    return shortToData(nostring,0x0);
	    
        case GET_METR_MODE:
	{
	vector<CAN::byte_t> tmps = charToData(nostring,m_metrMode); 
	return charToData(tmps,0x0); 
	}
	
        case GET_METR_TEMPS_1:
	    return tempData(0);
	    
        case GET_METR_TEMPS_2:
	    return tempData(4);
	    
        case GET_METR_TEMPS_3:
	    return tempData(8);
	    
        case GET_METR_TEMPS_4:
	    return tempData(12);
	    
        case GET_METR_TEMPS_5:
	    return tempData(16);
	    
        case GET_METR_TEMPS_6:
	    return tempData(20);
	    
        case GET_METR_TEMPS_7:
	    return tempData(24);
	    
        case GET_METR_TEMPS_8:
	    return tempData(28);
	    
        case GET_METR_TEMPS_9:
	    return tempData(32);
	    
        case GET_METR_TEMPS_10:
	    return tempData(36);
	    
        case GET_METR_TEMPS_11:
	    return tempData(40);
	    
        case GET_METR_TEMPS_12:
	    return tempData(44);
	    
        case GET_METR_TEMPS_13:
	    return tempData(48);
	    
        case GET_METR_TEMPS_14:
	    return tempData(52);
	    
        case GET_METR_TEMPS_15:
	    return tempData(56);
	    
        case GET_METR_TEMPS_16:
	    return tempData(60);
	    
        case GET_METR_TEMPS_17:
	    return tempData(64);
	    
        case GET_METR_TEMPS_18:
	    return tempData(68);
	    
        case GET_METR_TEMPS_19:
	    return tempData(72);
	    
        case GET_METR_TEMPS_20:
	    return tempData(76);
	    
        case GET_METR_TEMPS_21:
	    return tempData(80);
	    
        case GET_METR_TEMPS_22:
	    return tempData(84);
	    
        case GET_METR_TEMPS_23:
	    return tempData(88);
	    
        case GET_METR_TEMPS_24:
	    return tempData(92);
	    
        case GET_METR_TEMPS_25:
	    return tempData(96);
	    
        case GET_METR_TILT_0:
	    return tiltData(0);
	    
        case GET_METR_TILT_1:
	    return tiltData(1);
	    
        case GET_METR_TILT_2:
	    return tiltData(2);
	    
        case GET_METR_TILT_3:
	    return tiltData(3);
	    
        case GET_METR_TILT_4:
	    return tiltData(4);
	    
        case GET_OTHER_STATUS:
	    return charToData(nostring,0x0);
        case GET_POWER_STATUS:
	    return shortToData(nostring,0x0);
	    
        case GET_PT_MODEL_COEFF_0:
	    return doubleToData(m_IA);
	    
        case GET_PT_MODEL_COEFF_0 + 1:
	    return doubleToData(m_CA);
	    
        case GET_PT_MODEL_COEFF_0 + 2:
	    return doubleToData(m_NPAE);

        case GET_PT_MODEL_COEFF_0 + 3:
	    return doubleToData(m_AN);
	    
        case GET_PT_MODEL_COEFF_0 + 4:
	    return doubleToData(m_AW);
	    
        case GET_PT_MODEL_COEFF_0 + 5:
	    return doubleToData(m_IE);
	    
        case GET_PT_MODEL_COEFF_0 + 6:
	    return doubleToData(m_ECEC);
	    
        case GET_PT_MODEL_COEFF_0 + 7:
	    return doubleToData(m_C_GR);
	    
        case GET_PT_MODEL_COEFF_0 + 8:
	    return doubleToData(m_D_GR);
	    
        case GET_PT_MODEL_COEFF_0 + 9:
	    return doubleToData(m_RESERVED);
	    
        case GET_PTC_ERROR:
	{
	// return longlongToData(nostring,0,5);      
	// 0 bytes sent means no error
	std::vector<CAN::byte_t> tmps(0);
	return tmps;
	}
        case GET_SUBREF_ABS_POSN:
	{    
	vector<CAN::byte_t> tmps = shortToData(nostring,m_Xposition);
	tmps = shortToData(tmps,m_Yposition);
	return shortToData(tmps,m_Zposition);
	}
	
        case GET_SUBREF_DELTA_POSN:
	{    
	vector<CAN::byte_t> tmps = shortToData(nostring,m_deltaXposition);
	tmps = shortToData(tmps,m_deltaYposition);
	return shortToData(tmps,m_deltaZposition);
	}
	
        case GET_SUBREF_LIMITS:
            return longlongToData(nostring,0,8);      // no error
	    
        case GET_SUBREF_ROTATION:
	{
	vector<CAN::byte_t> tmps = shortToData(nostring,m_Xrotation);
	tmps = shortToData(tmps,m_Yrotation);
	return shortToData(tmps,m_Zrotation);
	}
	
        case GET_SUBREF_STATUS:
            return longlongToData(nostring,0,5);      // no error
	    
        case GET_UPS_ALARMS_1:
	    return longlongToData(nostring,0,4);
	    
	case GET_UPS_BATTERY_OUTPUT_1:
	    return longlongToData(nostring,0,4);
	    
        case GET_UPS_BATTERY_STATUS_1:
	    return longlongToData(nostring,0,4);
	    
        case GET_UPS_BYPASS_VOLTS_1:
	    return longlongToData(nostring,0,6);
	    
        case GET_UPS_FREQS_1:
	    return longlongToData(nostring,0,4);
	    
        case GET_UPS_INVERTER_SW_1:
	    return longlongToData(nostring,0,1);
	    
	case GET_UPS_INVERTER_VOLTS_1:
	    return longlongToData(nostring,0,6);
	    
        case GET_UPS_OUTPUT_CURRENT_1:
	    return longlongToData(nostring,0,6);
	    
        case GET_UPS_OUTPUT_VOLTS_1:
	    return longlongToData(nostring,0,6);
	    
        case GET_UPS_STATUS_1:
	    return longlongToData(nostring,0,1);
	    
        case GET_UPS_ALARMS_2:
	    return longlongToData(nostring,0,4);
	    
        case GET_UPS_BATTERY_OUTPUT_2:
	    return longlongToData(nostring,0,4);
	    
        case GET_UPS_BATTERY_STATUS_2:
	    return longlongToData(nostring,0,4);
	    
        case GET_UPS_BYPASS_VOLTS_2:
	    return longlongToData(nostring,0,6);
	    
        case GET_UPS_FREQS_2:
	    return longlongToData(nostring,0,4);
	    
        case GET_UPS_INVERTER_SW_2:
	    return longlongToData(nostring,0,1);
	    
	case GET_UPS_INVERTER_VOLTS_2:
	    return longlongToData(nostring,0,6);
	    
        case GET_UPS_OUTPUT_CURRENT_2:
	    return longlongToData(nostring,0,6);
	    
        case GET_UPS_OUTPUT_VOLTS_2:
	    return longlongToData(nostring,0,6);
	    
        case GET_UPS_STATUS_2:
	    return longlongToData(nostring,0,1);
	    
	default:
	    int mrca = (rca & 0X3FFFF);
	    cerr << "RCA=" << mrca << " (0x" << hex << mrca << ")" 
		 << dec << endl;
	    cerr << "Switch does not match any PTC case" << endl;
	    throw PTCError("Unknown RCA in monitor command");
	}
}

// -----------------------------------------------------------------------------

void AMB::PTC::control(
    rca_t rca,
	const vector<CAN::byte_t>& data)
{
    // bump transaction count
    m_trans_num++;
    
    switch (rca & 0x3FFFF)      // RCA is first 18 bits
	{
	// mandatory standard generic control point
	
	case RESET_DEVICE:
	{
	if (data.size() != 1)
	    {
	    throw PTCError("SET_RESET_DEVICE data length must be 8");
	    }
	}
	break;
	
	// PTC specific control points (order from ICD)
	
        case CLEAR_FAULT_CMD:
            break;
	    
        case INIT_SUBREF_CMD:
	    break;
	    
        case RESET_UPS_CMD_1:
        case RESET_UPS_CMD_2:
	    break;
	    
        case SET_METR_MODE:
	{
	if (data.size() != 2)
	    {
	    throw PTCError("SET_METR_MODE data length must be 1");
	    }
	m_metrMode = data[0];
	}
	break;
	
        case SET_PT_MODEL_COEFF_0:
	{
	if (data.size() != 8)
	    {
	    throw PTCError("SET_PT_MODEL_COEFF_0 data length must be 8");
	    }
	vector<CAN::byte_t> temp(data);
	m_IA = dataToDouble(temp);
	}
	break;
	
        case SET_PT_MODEL_COEFF_0 + 1:
	{
	if (data.size() != 8)
	    {
	    throw PTCError("SET_PT_MODEL_COEFF_1 data length must be 8");
	    }
	vector<CAN::byte_t> temp(data);
	m_CA = dataToDouble(temp);
	}
	break;
	
        case SET_PT_MODEL_COEFF_0 + 2:
	{
	if (data.size() != 8)
	    {
		throw PTCError("SET_PT_MODEL_COEFF_2 data length must be 8");
	    }
	vector<CAN::byte_t> temp(data);
	m_NPAE = dataToDouble(temp);
	}
	break;
	
        case SET_PT_MODEL_COEFF_0 + 3:
	{
	if (data.size() != 8)
		{
		throw PTCError("SET_PT_MODEL_COEFF_3 data length must be 8");
		}
	vector<CAN::byte_t> temp(data);
	m_AN = dataToDouble(temp);
	}
	break;
	
        case SET_PT_MODEL_COEFF_0 + 4:
	{
	if (data.size() != 8)
	    {
	    throw PTCError("SET_PT_MODEL_COEFF_4 data length must be 8");
	    }
            vector<CAN::byte_t> temp(data);
	    m_AW = dataToDouble(temp);
	}
	break;
	
        case SET_PT_MODEL_COEFF_0 + 5:
	{
	if (data.size() != 8)
	    {
	    throw PTCError("SET_PT_MODEL_COEFF_5 data length must be 8");
	    }
	vector<CAN::byte_t> temp(data);
	m_IE = dataToDouble(temp);
	}
	break;
	
        case SET_PT_MODEL_COEFF_0 + 6:
	{
	if (data.size() != 8)
	    {
	    throw PTCError("SET_PT_MODEL_COEFF_6 data length must be 8");
	    }
	vector<CAN::byte_t> temp(data);
	m_ECEC = dataToDouble(temp);
	}
	break;
	
        case SET_PT_MODEL_COEFF_0 + 7:
	{
	if (data.size() != 8)
	    {
	    throw PTCError("SET_PT_MODEL_COEFF_7 data length must be 8");
	    }
	vector<CAN::byte_t> temp(data);
	m_C_GR = dataToDouble(temp);
	}
	break;
	
        case SET_PT_MODEL_COEFF_0 + 8:
	{
	if (data.size() != 8)
	    {
	    throw PTCError("SET_PT_MODEL_COEFF_8 data length must be 8");
	    }
	vector<CAN::byte_t> temp(data);
	m_D_GR = dataToDouble(temp);
	}
	break;
	
        case SET_PT_MODEL_COEFF_0 + 9:
	{
	if (data.size() != 8)
	    {
	    throw PTCError("SET_PT_MODEL_COEFF_9 data length must be 8");
	    }
	vector<CAN::byte_t> temp(data);
	m_RESERVED = dataToDouble(temp);
	}
	break;
	
        case SET_SUBREF_ABS_POSN:
	{    
	if (data.size() != 6)
	    {
	    throw PTCError("SET_SUBREF_ABS_POSN data length must be 6");
	    }
	vector<CAN::byte_t> temp(data);
	m_Xposition = dataToShort(temp);
	m_Yposition = dataToShort(temp);
	m_Zposition = dataToShort(temp);
	}
	break;
	
        case SET_SUBREF_DELTA_POSN:
	{    
	if (data.size() != 6)
	    {
	    throw PTCError("SET_SUBREF_DELTA_POSN data length must be 6");
	    }
	vector<CAN::byte_t> temp(data);
	m_deltaXposition = dataToShort(temp);
	m_deltaYposition = dataToShort(temp);
	m_deltaZposition = dataToShort(temp);
	}
	break;
	    
        case SET_SUBREF_ROTATION:
	{    
	if (data.size() != 6)
	    {
	    throw PTCError("SET_SUBREF_ROTATION data length must be 6");
	    }
            vector<CAN::byte_t> temp(data);
	    m_Xrotation = dataToShort(temp);
	    m_Yrotation = dataToShort(temp);
            m_Zrotation = dataToShort(temp);
	}
	break;
	
        case SUBREF_DELTA_ZERO_CMD:
        case UPS_BATTERY_TEST_CMD_1:
        case UPS_BATTERY_TEST_CMD_2:
	    break;

	// The remaining commands are only available for simulation
	// purposes
        case SIM_AC_STATUS:
	{
	if (data.size() != 1)
	    {
	    throw PTCError("SIM_AC_STATUS data length must be 1");
	    }
	m_ac_status = data[0];
	}
	break;
	default:
	    cerr << "PTC.control(): switch does not match any case" << endl;
	    throw CAN::Error("rca NOT Implemented");
	}
}

////////////////////////////////////////////////////////////////////////////////
////                                                                        ////
////                            Monitor Requests                            ////
////                                                                        ////
////////////////////////////////////////////////////////////////////////////////
      
unsigned int AMB::PTC::get_trans_num() const
{
    return m_trans_num;
}

vector<CAN::byte_t> AMB::PTC::tempData(const int firstTemp) const {
    vector<CAN::byte_t> retVal(0);
    for (int i = firstTemp; i < firstTemp + 4; i++) {
    const short int data = static_cast<const short int>(temp_m[i]*10.0);
    retVal = shortToData(retVal, data);
  }
    return retVal;
}

vector<CAN::byte_t> AMB::PTC::displData(const int whichOne) const {
    vector<CAN::byte_t> retVal(0);
    const short int data = static_cast<const short int>(displ_m[whichOne]*1.E7);
    return shortToData(retVal, data);
}

vector<CAN::byte_t> AMB::PTC::tiltData(const int whichOne) const {
    vector<CAN::byte_t> retVal(0);
    short int data = static_cast<short int>(tilt_m[whichOne].temp/.1 + 0.5);
    retVal = shortToData(retVal, data);
    data = static_cast<short int>(tilt_m[whichOne].y/.01 + 0.5);
    retVal = shortToData(retVal, data);
    data = static_cast<short int>(tilt_m[whichOne].x/.01 + 0.5);
    return shortToData(retVal, data);
}

////////////////////////////////////////////////////////////////////////////////
////                                                                        ////
////                            Control Commands                            ////
////                                                                        ////
////////////////////////////////////////////////////////////////////////////////

