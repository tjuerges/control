//// @(#) $Id$
//
// Copyright (C) 2002
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include <iomanip>
#include "SecondLO.h"
#include "AMBUtil.h"

#include <stdio.h>

// ----------------------------------------------------------------------------
AMB::SecondLO::SecondLO(node_t node,
                        const std::vector<CAN::byte_t> &serialNumber,
			const short baseband)
  : AMB::FTS(node, serialNumber, AMB::SecondLO::FTS_RCA_OFFSET),
    MODULE_STATUS_m(TUNE_POSITION),
    POWER_SUPPLY_1_VALUE_m(5.15),
    POWER_SUPPLY_2_VALUE_m(15.02),
    POWER_SUPPLY_3_VALUE_m(-14.8),
    POWER_SUPPLY_4_VALUE_m(14.99),
    DETECTED_RF_POWER_m(1164.0e-6),
    FM_COIL_VOLTAGE_m(-3.63),
    COARSE_FREQ_m(10000.0e6),
    DETECTED_IF_POWER_m(102),
    DETECTED_FTS_POWER_m(85),
    baseband_m(baseband)
{
  std::cout << "Creating node 0x" << std::hex << node << ", s/n 0x";
  for (int i = 0; i < 8; i++) {
    std::cout << std::hex << std::setw(2) << std::setfill('0') 
	      << static_cast<int>(serialNumber[i]);
  }
  std::cout << ", LO2 device" << std::endl;
}

// ----------------------------------------------------------------------------
AMB::SecondLO::~SecondLO() {
  // Nothing
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::SecondLO::monitor(rca_t rca) const {
  const std::vector<CAN::byte_t> tmp;
  try {
    return AMB::FTS::monitor(rca);
  }
  catch (FTSError e) {
    switch (rca) {
          
    // Mandatory generic points
    // These don't get processed in call to AMB::FTS::monitor since their
    // RCAs are not offset in the second LO.
    case GET_SERIAL_NUMBER:
        return serialNumber();
        break;
    case GET_CAN_ERROR:
        return get_can_error();
        break;
    case GET_PROTOCOL_REV_LEVEL:
        return get_protocol_rev_level();
        break;
		case AMB::Device::GET_SW_REV_LEVEL:
				return get_software_rev_level();
				break;
		case AMB::Device::GET_HW_REV_LEVEL:
				return get_hardware_rev_level();
				break;
    case GET_TRANS_NUM:
        return longToData(tmp, get_trans_num());
        break;
        // Optional generic point
    case GET_AMBIENT_TEMPERATURE:
        return floatDS1820ToData(tmp, get_ambient_temperature());
        break;

    // SecondLO monitor points
    case GET_MODULE_STATUS:
      return CAN_GET_MODULE_STATUS();
      break;
    case GET_COARSE_FREQ:
      return CAN_GET_COARSE_FREQ();
      break;
    case GET_POWER_SUPPLY_1_VALUE:
      return CAN_GET_POWER_SUPPLY_1_VALUE();
      break;
    case GET_POWER_SUPPLY_2_VALUE:
      return CAN_GET_POWER_SUPPLY_2_VALUE();
      break;
    case GET_POWER_SUPPLY_3_VALUE:
      return CAN_GET_POWER_SUPPLY_3_VALUE();
      break;
    case GET_POWER_SUPPLY_4_VALUE:
      return CAN_GET_POWER_SUPPLY_4_VALUE();
      break;
    case GET_DETECTED_RF_POWER:
      return CAN_GET_DETECTED_RF_POWER();
      break;
    case GET_DETECTED_IF_POWER:
      return CAN_GET_DETECTED_IF_POWER();
      break;
    case GET_DETECTED_FTS_POWER:
      return CAN_GET_DETECTED_FTS_POWER();
      break;
    case GET_FM_COIL_VOLTAGE:
      return CAN_GET_FM_COIL_VOLTAGE();
      break;
    default:
      std::cerr << "Switch '" << static_cast<int>(rca) 
                << "' does not match any SecondLO case." << std::endl;
      throw SecondLOError("Unknown RCA in monitor command");
    }
  }
}

// ----------------------------------------------------------------------------
void AMB::SecondLO::control(rca_t rca, const std::vector<CAN::byte_t> &data) {
  try {
    if (rca == 0x18000) {
      /* Ensure that the FTS does not respond to broadcast */
      throw FTSError("Unknown RCA in monitor command");
    }
    AMB::FTS::control(rca, data);
  }
  catch (FTSError e) {
      switch (rca) {
      case SET_COARSE_FREQ:
        checkSize(data, 2, "SET_COARSE_FREQ");
        CAN_SET_COARSE_FREQ(data);
        break;
      case SET_LOCK:
        checkSize(data, 1, "SET_LOCK");
        CAN_SET_LOCK(data);
        break;
      case SET_RANGE:
        checkSize(data, 1, "SET_RANGE");
        CAN_SET_RANGE(data);
        break;
      default:
        std::ostringstream msg;
        msg << "Unknown RCA 0x" << std::hex << static_cast<int>(rca) 
            << " in SecondLO command." << std::endl;
        throw SecondLOError(msg.str());
      }
  }
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::SecondLO::CAN_GET_MODULE_STATUS() const 
{
  std::vector<CAN::byte_t> tmp;
  tmp.push_back(MODULE_STATUS_m);
  return tmp;
}

std::vector<CAN::byte_t> AMB::SecondLO::CAN_GET_COARSE_FREQ() const 
{
  std::vector<CAN::byte_t> tmp;
  const short int intValue =
    static_cast<short int>((COARSE_FREQ_m - 8.0e9) * 0xfff / 6.0e9 + 0.5) & 0xFFF;
  return shortToData(tmp, intValue);
}

std::vector<CAN::byte_t> AMB::SecondLO::CAN_GET_POWER_SUPPLY_1_VALUE() const
{
  const std::vector<CAN::byte_t> tmp;
  const short int intValue =
    static_cast<short int>(POWER_SUPPLY_1_VALUE_m * 0x7FF / 10.0) & 0xFFF;
  return shortToData(tmp, intValue);
}

std::vector<CAN::byte_t> AMB::SecondLO::CAN_GET_POWER_SUPPLY_2_VALUE() const
{
  const std::vector<CAN::byte_t> tmp;
  const short int intValue =
    static_cast<short int>(POWER_SUPPLY_2_VALUE_m * 0x7FF / 20.0) & 0xFFF;
  return shortToData(tmp, intValue);
}

std::vector<CAN::byte_t> AMB::SecondLO::CAN_GET_POWER_SUPPLY_3_VALUE() const
{
  const std::vector<CAN::byte_t> tmp;
  const short int intValue =
    static_cast<short int>(POWER_SUPPLY_3_VALUE_m * 0x7FF / 20.0) & 0xFFF;
  return shortToData(tmp, intValue);
}

std::vector<CAN::byte_t> AMB::SecondLO::CAN_GET_POWER_SUPPLY_4_VALUE() const
{
  const std::vector<CAN::byte_t> tmp;
  const short int intValue =
    static_cast<short int>(POWER_SUPPLY_4_VALUE_m * 0x7FF / 20.0) & 0xFFF;
  return shortToData(tmp, intValue);
}

std::vector<CAN::byte_t> AMB::SecondLO::CAN_GET_DETECTED_RF_POWER() const
{
  const std::vector<CAN::byte_t> tmp;
  const short int intValue =
    static_cast<short int>(DETECTED_RF_POWER_m * 0x7FF / -20.0E-3) & 0xFFF;
  return shortToData(tmp, intValue);
}

std::vector<CAN::byte_t> AMB::SecondLO::CAN_GET_DETECTED_IF_POWER() const
{
  const std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, DETECTED_IF_POWER_m);
}

std::vector<CAN::byte_t> AMB::SecondLO::CAN_GET_DETECTED_FTS_POWER() const
{
  const std::vector<CAN::byte_t> tmp;
  return shortToData(tmp, DETECTED_FTS_POWER_m);
}

std::vector<CAN::byte_t> AMB::SecondLO::CAN_GET_FM_COIL_VOLTAGE() const
{
  const std::vector<CAN::byte_t> tmp;
  const short int intValue =
    static_cast<short int>(FM_COIL_VOLTAGE_m * 0x7FF / 10.0) & 0xFFF;
  return shortToData(tmp, intValue);
}

void AMB::SecondLO::CAN_SET_COARSE_FREQ(const std::vector<CAN::byte_t>& data)
{
  std::vector<CAN::byte_t> tmp = data;

  // Mmmm. This is not working. 
  // TODO find out why
  //COARSE_FREQ_m = dataToFloat(tmp, 8000.0e6, 14000.0e6, 12);

  int freq = (tmp[0] << 8) + tmp[1];
  COARSE_FREQ_m = 6.0e9 * freq / 0xfff + 8.0e9;

  std::cout << "Setting second LO frequency for baseband " << baseband_m << std::endl;
  std::cout << "frequency: " << COARSE_FREQ_m << std::endl;

  // set TELCAL SharedSimulator coarse frequency
  sharedSim_p->setSecondLOFreq(baseband_m, COARSE_FREQ_m);
}

void AMB::SecondLO::CAN_SET_LOCK(const std::vector<CAN::byte_t>& data)
{
  const unsigned char charValue = static_cast<unsigned char>(data[0]);
  if ((charValue & 1) > 0) {
    MODULE_STATUS_m |= TUNE_POSITION;
  }
  else {
    MODULE_STATUS_m &= ~TUNE_POSITION;
  }
}

void AMB::SecondLO::CAN_SET_RANGE(const std::vector<CAN::byte_t>& data)
{
  const unsigned char charValue = static_cast<unsigned char>(data[0]);
  if ((charValue & 1) > 0) {
    MODULE_STATUS_m |= HIGH_POSITION;
  }
  else {
    MODULE_STATUS_m &= ~HIGH_POSITION;
  }
}
