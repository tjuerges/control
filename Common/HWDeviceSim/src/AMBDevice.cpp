// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include "AMBDevice.h"

// -----------------------------------------------------------------------------

AMB::Device::~Device()
{
    // Nothing
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t>  AMB::Device::get_can_error() const
{
    std::vector<CAN::byte_t> tmp(4);
    tmp[0] = tmp[1] = tmp[2] = CAN::byte_t(0);
    tmp[3] = 0x18; 
    return tmp;
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::Device::get_protocol_rev_level() const
{
    std::vector<CAN::byte_t> tmp(3);
    tmp[0] = tmp[1] = tmp[2] = CAN::byte_t(0);
    return tmp;
}

std::vector<CAN::byte_t> AMB::Device::get_software_rev_level() const
{
	std::vector<CAN::byte_t> tmp;
	tmp.push_back('F');
	tmp.push_back('o');
	tmp.push_back('o');
	return tmp;
}

std::vector<CAN::byte_t> AMB::Device::get_hardware_rev_level() const
{
	std::vector<CAN::byte_t> tmp;
	tmp.push_back('B');
	tmp.push_back('a');
	tmp.push_back('r');
	return tmp;
}
