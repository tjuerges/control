// @(#) $Id$
//
// Copyright (C) 2002
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu


#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include "LORR.h"
#include "AMBUtil.h"


// ----------------------------------------------------------------------------
AMB::LORR::LORR(node_t node, const std::vector< CAN::byte_t >& serialNumber):
    m_node(node),
    m_sn(serialNumber),
    m_trans_num(0),
    m_errorCode(1),
    STATUS_m(0x3100),
    EFC_25_MHZ_m(4.5),
    EFC_2_GHZ_m(5.1),
    PWR_25_MHZ_m(1.9),
    PWR_125_MHZ_m(2.5),
    PWR_2_GHZ_m(3.1),
    RX_OPT_PWR_m(0.512),
    VDC_7_m(6.8),
    VDC_12_m(11.29), // This is nominally 11V (and NOT 12V)
    VDC_15_m(13.9), // This needs is nominally 13.5V (and not 15V)
    VDC_MINUS_7_m(-6.1),
    TE_LENGTH_m(5999999U),
    TE_OFFSET_COUNTER_m(2U)
// The ICD needs to be updated to reflect these new nominal voltages.
{
    // Byte 0: 0x55 (Product Tree Digit 1: Product Tree Digit 2)
    // Byte 1: 0x40 (Product Tree Digit 4: Product Tree Digit 6)
    // Byte 2: Firmware Revision 0.0 to 15.15
    // Byte 3: Firmware Compile Date (Day)
    // Byte 4: Firmware Compile Date (Month)
    // Byte 5: Firmware Compile Date (Year, 2000 => 0x00)
    // Bytes 6,7: Module Serial Number (0xFFFF if not available)
    READ_MODULE_CODES_m.push_back(0x55);
    READ_MODULE_CODES_m.push_back(0x40);
    READ_MODULE_CODES_m.push_back(0x00);
    READ_MODULE_CODES_m.push_back(0x02);
    READ_MODULE_CODES_m.push_back(0x02);
    READ_MODULE_CODES_m.push_back(0x02);
    READ_MODULE_CODES_m.push_back(0x12);
    READ_MODULE_CODES_m.push_back(0x34);


    std::ios::fmtflags oldFlags(std::cout.flags());
    std::cout.setf(std::ios::hex, std::ios::basefield);
    std::cout << "Creating node 0x"
        << node
        << ", s/n 0x";
    for(unsigned int i(0U); i < 8U; ++i)
    {
        std::cout << std::setw(2)
        << std::setfill('0')
        << static_cast< int >(serialNumber[i]);
    }

    std::cout.setf(oldFlags);
    std::cout << ", LORR device\n";
}

// ----------------------------------------------------------------------------
AMB::LORR::~LORR()
{
    // Nothing
}

// ----------------------------------------------------------------------------
AMB::node_t AMB::LORR::node() const
{
    return m_node;
}

// ----------------------------------------------------------------------------
std::vector< CAN::byte_t > AMB::LORR::serialNumber() const
{
    return m_sn;
}

// ----------------------------------------------------------------------------
std::vector< CAN::byte_t > AMB::LORR::monitor(rca_t rca) const
{
    ++m_trans_num;

    switch(rca)
    {
        // Mandatory generic points
        case GET_SERIAL_NUMBER:
            return serialNumber();
            break;
        case GET_CAN_ERROR:
            return get_can_error();
            break;
        case GET_PROTOCOL_REV_LEVEL:
            return get_protocol_rev_level();
            break;
        case AMB::Device::GET_SW_REV_LEVEL:
            return get_software_rev_level();
            break;
        case AMB::Device::GET_HW_REV_LEVEL:
            return get_hardware_rev_level();
            break;
        case GET_TRANS_NUM:
        {
            std::vector< CAN::byte_t > tmp;
            return longToData(tmp, get_trans_num());
        }
            break;
            // Optional generic point
        case GET_AMBIENT_TEMPERATURE:
        {
            const std::vector< CAN::byte_t > tmp;
            return floatDS1820ToData(tmp, get_ambient_temperature());
        }
            break;

            // LORR specific points
        case STATUS:
            return CAN_STATUS();
            break;
        case EFC_25_MHZ:
            return CAN_EFC_25_MHZ();
            break;
        case EFC_2_GHZ:
            return CAN_EFC_2_GHZ();
            break;
        case PWR_25_MHZ:
            return CAN_PWR_25_MHZ();
            break;
        case PWR_125_MHZ:
            return CAN_PWR_125_MHZ();
            break;
        case PWR_2_GHZ:
            return CAN_PWR_2_GHZ();
            break;
        case RX_OPT_PWR:
            return CAN_RX_OPT_PWR();
            break;
        case VDC_7:
            return CAN_VDC_7();
            break;
        case VDC_12:
            return CAN_VDC_12();
            break;
        case VDC_15:
            return CAN_VDC_15();
            break;
        case VDC_MINUS_7:
            return CAN_VDC_MINUS_7();
            break;
        case TE_LENGTH:
            return CAN_TE_LENGTH();
            break;
        case TE_OFFSET_COUNTER:
            return CAN_TE_OFFSET_COUNTER();
            break;
        case READ_MODULE_CODES:
            return CAN_READ_MODULE_CODES();
            break;

        default:
            std::ostringstream msg;
            msg << "AMB::LORR::monitor: switch '"
                << static_cast< int >(rca)
                << "' does not match any case.\n";
            const std::string out(msg.str());
            std::cout << out;
            throw LORRError(out);
    }
}

// ----------------------------------------------------------------------------
void AMB::LORR::control(rca_t rca, const std::vector< CAN::byte_t > &data)
{
    ++m_trans_num;

    switch(rca)
    {
        case XILINX_PROM_RESET:
            checkSize(data, 1, "XILINX_PROM_RESET");
            break;

            //** Test cases
        case TEST_STATUS:
            checkSize(data, 2, "TEST_STATUS");
            STATUS_m = dataToShort(
                const_cast< std::vector< CAN::byte_t >& >(data));
            break;

        case TEST_EFC_25_MHZ:
            checkSize(data, 8, "TEST_EFC_25_MHZ");
            EFC_25_MHZ_m = dataToDouble(
                const_cast< std::vector< CAN::byte_t >& >(data));
            break;

        case TEST_EFC_2_GHZ:
            checkSize(data, 8, "TEST_EFC_2_GHZ");
            EFC_2_GHZ_m = dataToDouble(
                const_cast< std::vector< CAN::byte_t >& >(data));
            break;

        case TEST_PWR_25_MHZ:
            checkSize(data, 8, "TEST_PWR_25_MHZ");
            PWR_25_MHZ_m = dataToDouble(
                const_cast< std::vector< CAN::byte_t >& >(data));
            break;

        case TEST_PWR_125_MHZ:
            checkSize(data, 8, "TEST_PWR_125_MHZ");
            PWR_125_MHZ_m = dataToDouble(
                const_cast< std::vector< CAN::byte_t >& >(data));
            break;

        case TEST_PWR_2_GHZ:
            checkSize(data, 8, "TEST_PWR_2_GHZ");
            PWR_2_GHZ_m = dataToDouble(
                const_cast< std::vector< CAN::byte_t >& >(data));
            break;

        case TEST_RX_OPT_PWR:
            checkSize(data, 8, "TEST_RX_OPT_PWR");
            RX_OPT_PWR_m = dataToDouble(
                const_cast< std::vector< CAN::byte_t >& >(data));
            break;

        case TEST_VDC_7:
            checkSize(data, 8, "TEST_VDC_7");
            VDC_7_m = dataToDouble(
                const_cast< std::vector< CAN::byte_t >& >(data));
            break;

        case TEST_VDC_12:
            checkSize(data, 8, "TEST_VDC_12");
            VDC_12_m = dataToDouble(
                const_cast< std::vector< CAN::byte_t >& >(data));
            break;

        case TEST_VDC_15:
            checkSize(data, 8, "TEST_VDC_15");
            VDC_15_m = dataToDouble(
                const_cast< std::vector< CAN::byte_t >& >(data));
            break;

        case TEST_VDC_MINUS_7:
            checkSize(data, 8, "TEST_VDC_MINUS_7");
            VDC_MINUS_7_m = dataToDouble(
                const_cast< std::vector< CAN::byte_t >& >(data));
            break;

        default:
            throw LORRError("Unknown RCA in control command");
    }

}

// ----------------------------------------------------------------------------
std::vector< CAN::byte_t > AMB::LORR::CAN_STATUS() const
{
    const std::vector< CAN::byte_t > tmp;
    return shortToData(tmp, STATUS_m);
}

// ----------------------------------------------------------------------------
std::vector< CAN::byte_t > AMB::LORR::CAN_EFC_25_MHZ() const
{
    const std::vector< CAN::byte_t > tmp;
    const short int roundedValue(static_cast< short int >(EFC_25_MHZ_m
        * 4095.0 / 20.0));
    return shortToData(tmp, roundedValue);
}

// ----------------------------------------------------------------------------
std::vector< CAN::byte_t > AMB::LORR::CAN_EFC_2_GHZ() const
{
    const std::vector< CAN::byte_t > tmp;
    const short int roundedValue(static_cast< short int >(EFC_2_GHZ_m
        * 4095.0 / 20.0));
    return shortToData(tmp, roundedValue);
}

// ----------------------------------------------------------------------------
std::vector< CAN::byte_t > AMB::LORR::CAN_PWR_25_MHZ() const
{
    const std::vector< CAN::byte_t > tmp;
    const short int roundedValue(static_cast< short int >(PWR_25_MHZ_m
        * 4095.0 / 20.0));
    return shortToData(tmp, roundedValue);
}

// ----------------------------------------------------------------------------
std::vector< CAN::byte_t > AMB::LORR::CAN_PWR_125_MHZ() const
{
    const std::vector< CAN::byte_t > tmp;
    const short int roundedValue(static_cast< short int >(PWR_125_MHZ_m
        * 4095.0 / 20.0));
    return shortToData(tmp, roundedValue);
}

// ----------------------------------------------------------------------------
std::vector< CAN::byte_t > AMB::LORR::CAN_PWR_2_GHZ() const
{
    const std::vector< CAN::byte_t > tmp;
    const short int roundedValue(static_cast< short int >(PWR_2_GHZ_m
        * 4095.0 / 20.0));
    return shortToData(tmp, roundedValue);
}

// ----------------------------------------------------------------------------
std::vector< CAN::byte_t > AMB::LORR::CAN_RX_OPT_PWR() const
{
    const std::vector< CAN::byte_t > tmp;
    const short int roundedValue(static_cast< short int >(RX_OPT_PWR_m
        * 4095.0 / 20.0));
    return shortToData(tmp, roundedValue);
}

// ----------------------------------------------------------------------------
std::vector< CAN::byte_t > AMB::LORR::CAN_VDC_7() const
{
    std::vector< CAN::byte_t > tmp;
    const short int roundedValue(static_cast< short int >(VDC_7_m * 4095.0
        / 20.0));
    return shortToData(tmp, roundedValue);
}

// ----------------------------------------------------------------------------
std::vector< CAN::byte_t > AMB::LORR::CAN_VDC_12() const
{
    std::vector< CAN::byte_t > tmp;
    const short int roundedValue(static_cast< short int >(VDC_12_m * 4095.0
        / 40.0));
    return shortToData(tmp, roundedValue);
}

// ----------------------------------------------------------------------------
std::vector< CAN::byte_t > AMB::LORR::CAN_VDC_15() const
{
    std::vector< CAN::byte_t > tmp;
    const short int roundedValue(static_cast< short int >(VDC_15_m * 4095.0
        / 40.0));
    return shortToData(tmp, roundedValue);
}

// ----------------------------------------------------------------------------
std::vector< CAN::byte_t > AMB::LORR::CAN_VDC_MINUS_7() const
{
    std::vector< CAN::byte_t > tmp;
    const short int roundedValue(static_cast< short int >(VDC_MINUS_7_m
        * 4095.0 / 20.0));
    return shortToData(tmp, roundedValue);
}

// ----------------------------------------------------------------------------
std::vector< CAN::byte_t > AMB::LORR::CAN_TE_LENGTH() const
{
    std::vector< CAN::byte_t > tmp;
    return AMB::longlongToData(tmp , TE_LENGTH_m, 3);
}

// ----------------------------------------------------------------------------
std::vector< CAN::byte_t > AMB::LORR::CAN_TE_OFFSET_COUNTER() const
{
    std::vector< CAN::byte_t > tmp;
    return AMB::longlongToData(tmp , TE_OFFSET_COUNTER_m, 3);
}

// ----------------------------------------------------------------------------
std::vector< CAN::byte_t > AMB::LORR::CAN_READ_MODULE_CODES() const
{
    return READ_MODULE_CODES_m;
}

// ----------------------------------------------------------------------------
std::vector< CAN::byte_t > AMB::LORR::get_can_error() const
{
    return AMB::Device::get_can_error();
}

// ----------------------------------------------------------------------------
std::vector< CAN::byte_t > AMB::LORR::get_protocol_rev_level() const
{
    return AMB::Device::get_protocol_rev_level();
}

// ----------------------------------------------------------------------------
unsigned int AMB::LORR::get_trans_num() const
{
    return m_trans_num;
}

// ----------------------------------------------------------------------------
double AMB::LORR::get_ambient_temperature() const
{
    return 22.4;
}

// ----------------------------------------------------------------------------
void AMB::LORR::checkSize(const std::vector< CAN::byte_t >& data, int size,
    const char* pExceptionMsg)
{
    if(data.size() != static_cast< unsigned int >(size))
    {
        std::ostringstream msg;
        msg << pExceptionMsg
            << " data length is "
            << data.size()
            << " must be "
            << size;
        const std::string out(msg.str());
        std::cout << out;
        throw LORRError(out);
    }
}

// Converts a CAN Data value to a 'double' data type
//(This function must be used until the one provided by AMBUtil 'dataToDouble' is fixed)
// ----------------------------------------------------------------------------
//void AMB::LORR::toDouble(const std::vector<CAN::byte_t> &data, int size,
//                          double& var) {
//
//   unsigned char* ptr;
//   ptr = (unsigned char*) (& var);
//
//   for(int i=0;i < size;i++){
//      ptr[i]=data[i];
//   }
//
//}


/// Get functions for unit testing
// ----------------------------------------------------------------------------
unsigned short AMB::LORR::getSTATUS() const
{
    return STATUS_m;
}

double AMB::LORR::getEFC_25_MHZ() const
{
    return EFC_25_MHZ_m;
}
double AMB::LORR::getEFC_2_GHZ() const
{
    return EFC_2_GHZ_m;
}
double AMB::LORR::getPWR_25_MHZ() const
{
    return PWR_25_MHZ_m;
}
double AMB::LORR::getPWR_125_MHZ() const
{
    return PWR_125_MHZ_m;
}
double AMB::LORR::getPWR_2_GHZ() const
{
    return PWR_2_GHZ_m;
}
double AMB::LORR::getRX_OPT_PWR() const
{
    return RX_OPT_PWR_m;
}
double AMB::LORR::getVDC_7() const
{
    return VDC_7_m;
}
double AMB::LORR::getVDC_12() const
{
    return VDC_12_m;
}
double AMB::LORR::getVDC_15() const
{
    return VDC_12_m;
}
double AMB::LORR::getVDC_MINUS_7() const
{
    return VDC_MINUS_7_m;
}

unsigned int AMB::LORR::getTeLength() const
{
    return TE_LENGTH_m;
}

unsigned int AMB::LORR::getTeOffsetCounter() const
{
    return TE_OFFSET_COUNTER_m;
}

std::vector< CAN::byte_t > AMB::LORR::getReadModuleCodes() const
{
    return READ_MODULE_CODES_m;
}
