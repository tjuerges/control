/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
*/

#include <unistd.h>
#include <TETimeUtil.h>
#include <ControlExceptions.h>
#include "CANError.h"
#include "HWSimulator.h"

using ACS::ThreadSyncGuard;
using std::queue;

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

const int HWSimulator::TE_PERIOD_ACS_IN_US = static_cast<int>(TETimeUtil::TE_PERIOD_ACS*1E-1);
const unsigned int HWSimulator::DEFAULT_AVAILABLE_SIZE = 100;

HWSimulator::HWSimulator():
    simulating_m(false),
    device_m(NULL)
{
    // const char* __METHOD__="HWSimulator::HWSimulator";
    // std:: cout << __METHOD__ << std::endl;
    {
    ThreadSyncGuard guard(&availableMutex_m);
    for (unsigned int idx = 0; idx < DEFAULT_AVAILABLE_SIZE; idx++) 
	available_m.push(new AmbCommand);
    }
    counter_m = 0ULL;
}

HWSimulator::~HWSimulator()
{
    AmbCommand* command;
    stop();

    {
    ThreadSyncGuard guard(&availableMutex_m);
    while(!available_m.empty())
	{
	command = available_m.front();
	available_m.pop();
	delete command;
	}
    }
}

void HWSimulator::setDevice(AMB::Device* device) throw()
{
    device_m = device; 
}

void HWSimulator::start() 
    throw (ControlExceptions::CAMBErrorExImpl)
{
    const char* __METHOD__ = "HWSimulator::start";
    // std:: cout << __METHOD__ << std::endl;
    if(device_m == NULL)
	{
	//std:: cout << __METHOD__ << " simulation object NULL" << std::endl;
	ControlExceptions::CAMBErrorExImpl ex =
	    ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,__METHOD__);
	ex.addData("Detail","No simulation object provided");
	throw ex;
	}
    //std:: cout << __METHOD__ << " simulation object OK" << std::endl;
    simulating_m = true;
    if(pthread_create(&simulationTID_m, 
		      NULL,
		      &HWSimulator::run,
		      static_cast<void*>(this))!= 0)
	{
	simulating_m = false;
	ControlExceptions::CAMBErrorExImpl ex =
	    ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,__METHOD__);
	//std:: cout << __METHOD__ << " Unable to start simulation thread" << std::endl;
	ex.addData("Detail","Unable to start simulation thread");
	throw ex;
	}
    //std:: cout << __METHOD__ << " start ready" << std::endl;
}

void HWSimulator::stop()
{
    if(simulating_m)
	{
	simulating_m = false;
	pthread_join(simulationTID_m, NULL); 
	}
    
    AmbCommand* command;
    
    {
    ThreadSyncGuard guard(&pendingMutex_m);
    ThreadSyncGuard guard2(&availableMutex_m);
    while(!pending_m.empty())
	{
	command = pending_m.top();
	if(command->requestType_m == AMB_CONTROL)
	    {
	    delete command->length_m;
            command->length_m = NULL;
	    delete[] command->data_m;
            command->data_m = NULL;
	    }
	*(command->status_m) = AMBERR_WRITEERR;
	if(command->synchLock_m != NULL)
	    sem_post(command->synchLock_m);
	pending_m.pop();
	available_m.push(command);
	}
    }
    
    {
    ThreadSyncGuard guard(&flushedMutex_m);
    ThreadSyncGuard guard2(&availableMutex_m);
    while(!flushed_m.empty())
	{
	command = flushed_m.front();
	if(command->requestType_m == AMB_CONTROL)
	    {
	    delete command->length_m;
            command->length_m = NULL;
	    delete[] command->data_m;
            command->data_m = NULL;
	    }
	if(command->synchLock_m != NULL)
	    sem_post(command->synchLock_m);
	flushed_m.pop();
	available_m.push(command);
	}
    }
}

HWSimulator::AmbCommand* HWSimulator::getSlot()
{
    ThreadSyncGuard guard(&availableMutex_m);
    if (available_m.empty()){
    return new AmbCommand;
    }
    
    AmbCommand* slot = available_m.front();
    slot->clear();
    available_m.pop();
    return slot;
}

HWSimulator::AmbCommand* HWSimulator::getNextCommand()
{
    ACS::Time currentTime;
    ThreadSyncGuard guard(&pendingMutex_m);
    if (!pending_m.empty())
	{
	AmbCommand* command = pending_m.top();
	currentTime = TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value;
	if(command->TimeEvent_m <= currentTime)
	    {
	    pending_m.pop();
	    return command;
	    }
	}
    return NULL;
}

void HWSimulator::insert(AmbTransaction_t requestType,
			 ACS::Time        TimeEvent,
			 AmbRelativeAddr  RCA,
			 AmbDataLength_t& dataLength, 
			 AmbDataMem_t*    data,
			 sem_t*           synchLock,
			 Time*            timestamp,
			 AmbErrorCode_t*  status)
    throw (ControlExceptions::CAMBErrorExImpl)
{
    const char* __METHOD__ = "HWSimulator::insert";

    counter_m++;
    if(!simulating_m)
	{
	ControlExceptions::CAMBErrorExImpl ex =
	    ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,__METHOD__);
	ex.addData("Detail","Simulation thread unavailable");
	throw ex;
	}
    
    AmbCommand* command;
    switch(requestType)
	{
	case AMB_MONITOR:
	{
	ThreadSyncGuard guard(&pendingMutex_m);
	command = getSlot();
	command->set(requestType,
		     TimeEvent,
		     RCA,
		     &dataLength,
		     data,
		     synchLock,
		     timestamp,
		     status,
		     counter_m);
	if(*(command->status_m) != AMBERR_PENDING)
	    *(command->status_m) = AMBERR_PENDING;
	}
	break;
	case AMB_CONTROL:
	{
	ThreadSyncGuard guard(&pendingMutex_m);
	command = getSlot();
	AmbDataLength_t* controlLength = new AmbDataLength_t;
	*controlLength = dataLength;
	AmbDataMem_t* controlData = new AmbDataMem_t[*controlLength];
	memcpy(controlData, data, *controlLength);
	command->set(requestType,
		     TimeEvent,
		     RCA,
		     controlLength,
		     controlData,
		     synchLock,
		     timestamp,
		     status,
		     counter_m);
	if(*(command->status_m) != AMBERR_PENDING)
	    *(command->status_m) = AMBERR_PENDING;
	}
	break;
	default:
	    return;
	}
    ThreadSyncGuard guard(&pendingMutex_m);
    pending_m.push(command);
}  

void HWSimulator::flush(ACS::Time         TimeEvent,
			ACS::Time*        timestamp,
			AmbErrorCode_t*   status)
{
    AmbCommand* command;
    queue<AmbCommand*> no_flushed;

    {
    ThreadSyncGuard guard(&pendingMutex_m);
    ThreadSyncGuard guard2(&flushedMutex_m);
    while(!pending_m.empty())
	{
	command = pending_m.top();
	pending_m.pop();
	if(command->TimeEvent_m >= TimeEvent)
	    {
	    ThreadSyncGuard guard(&flushedMutex_m);
	    *(command->timestamp_m) = TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value;
	    *(command->status_m) = AMBERR_FLUSHED;
	    flushed_m.push(command);
	    }
	else 
	    {
	    no_flushed.push(command);
	    }
	}
    
    while(!no_flushed.empty())
	{
	command = no_flushed.front();
	no_flushed.pop();
	pending_m.push(command);
	}
    }

    {
    ThreadSyncGuard guard(&flushedMutex_m);
    ThreadSyncGuard guard2(&availableMutex_m);
    while(!flushed_m.empty())
	{
	command = flushed_m.front();
	flushed_m.pop();
	if(command->requestType_m == AMB_CONTROL)
	    {
	    delete command->length_m;
            command->length_m = NULL;
	    delete[] command->data_m;
            command->data_m = NULL;
	    }
	if(command->synchLock_m != NULL)
	    sem_post(command->synchLock_m);
	available_m.push(command);
	}
    }
    *timestamp = TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value;
    *status = AMBERR_NOERR;
}

void HWSimulator::flush(ACS::Time         TimeEvent,
			AmbRelativeAddr   RCA,
			ACS::Time*        timestamp,
			AmbErrorCode_t*   status)
{
    AmbCommand* command;
    queue<AmbCommand*> no_flushed;

    {
    ThreadSyncGuard guard(&pendingMutex_m);
    ThreadSyncGuard guard2(&flushedMutex_m);
    while(!pending_m.empty())
	{
	command = pending_m.top();
	pending_m.pop();
	if(command->rca_m == RCA && command->TimeEvent_m >= TimeEvent)
	    {
	    ThreadSyncGuard guard(&flushedMutex_m);
	    *(command->timestamp_m) = TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value;
	    *(command->status_m) = AMBERR_FLUSHED;
	    flushed_m.push(command);
	    }
	else 
	    {
	    no_flushed.push(command);
	    }
	}
    
    while(!no_flushed.empty())
	{
	command = no_flushed.front();
	no_flushed.pop();
	pending_m.push(command);
	}
    }

    {
    ThreadSyncGuard guard(&flushedMutex_m);
    ThreadSyncGuard guard2(&availableMutex_m);
    while(!flushed_m.empty())
	{
	command = flushed_m.front();
	flushed_m.pop();
	if(command->requestType_m == AMB_CONTROL)
	    {
	    delete command->length_m;
            command->length_m = NULL;
	    delete[] command->data_m;
            command->data_m = NULL;
	    }
	if(command->synchLock_m != NULL)
	    sem_post(command->synchLock_m);
	available_m.push(command);
	}
    }
    *timestamp = TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value;
    *status = AMBERR_NOERR;
}

void* HWSimulator::run(void* data)
{
    HWSimulator* simulator = static_cast<HWSimulator*>(data);
    while(simulator->simulating_m)
	{
	simulator->simulation();
	}
    pthread_exit(NULL);
}
void HWSimulator::simulation()
{
    AmbCommand* command;
    command = getNextCommand();
    if(command !=NULL)
	simulate(command);
    else
	usleep(TE_PERIOD_ACS_IN_US);
}

void HWSimulator::simulate(AmbCommand* command)
{
    switch(command->requestType_m)
	{
	case AMB_MONITOR:
	    monitor(command);
	    break;
	case AMB_CONTROL:
	    control(command);
	    delete command->length_m;
            command->length_m = NULL;
	    delete[] command->data_m;
            command->data_m = NULL;
	    break;
	default:
	    break;
	}
    ThreadSyncGuard guard(&availableMutex_m);
    available_m.push(command);
}


void HWSimulator::monitor(AmbCommand* command)
{
    std::vector<CAN::byte_t> value;
    
    try{
    *(command->timestamp_m) = command->TimeEvent_m; 
    value = device_m->monitor(command->rca_m);
    *(command->status_m)  = AMBERR_NOERR;
    } catch (CAN::Error &ex) {
    std::cout << ex.errorMsg.c_str() << std::endl;
    *(command->status_m) = AMBERR_BADCMD;
    value.clear();
    }
    *(command->length_m)  = value.size();
    memcpy(command->data_m,&value[0],value.size());
    if(command->synchLock_m != NULL)
	sem_post(command->synchLock_m);
}

void HWSimulator::control(AmbCommand* command)
{
    const char* data = reinterpret_cast<char *>(command->data_m);
    try{
    *(command->timestamp_m) = command->TimeEvent_m;
    device_m->control(command->rca_m, 
		      std::vector<CAN::byte_t>(data, data + *(command->length_m)));
    *(command->status_m) = AMBERR_NOERR;
    } catch (CAN::Error &ex) {
    std::cout << ex.errorMsg.c_str() << std::endl;
    *(command->status_m) = AMBERR_BADCMD;
    }
    if(command->synchLock_m != NULL)
	sem_post(command->synchLock_m);
}

/*___oOo___*/
