/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2002 
*
* This code is under GNU General Public Licence (GPL). 
* Correspondence concerning ALMA Software should be addressed to:
* alma-sw-admin@nrao.edu
*
* Copyright (C) 2001
* Associated Universities, Inc. Washington DC, USA.
*
* Produced for the ALMA project
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Library General Public License
* as published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Library General Public License for more details.
*
* You should have received a copy of the GNU Library General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* aperrigo  30/11/02  created 
* aperrigo  03/12/02  modification due to new ICD
*/

#include <iomanip>
#include "FEDewar.h"
#include "AMBUtil.h"

// -----------------------------------------------------------------------------

AMB::FEDewar::FEDewar(
	node_t node,
	const std::vector<CAN::byte_t> &serialNumber)
    : node_m(node), sn_m(serialNumber), trans_num_m(0)
{
    std::cout << "Creating node " << node << ", s/n 0x";
    for (int i = 0; i < 8; i++)
	std::cout << std::hex << std::setw(2) << std::setfill('0') << (int)serialNumber[i];
    std::cout << ", FEDewar device" << std::endl;
}

// -----------------------------------------------------------------------------

AMB::FEDewar::~FEDewar()
{
    // Nothing
}

// -----------------------------------------------------------------------------

AMB::node_t AMB::FEDewar::node() const
{
    return node_m;
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::FEDewar::serialNumber() const
{
    return sn_m;
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::FEDewar::monitor(rca_t rca) const
{
    trans_num_m++;
    const std::vector<CAN::byte_t> tmp;

    switch (rca) {
    // Mandatory generic points
    case GET_CAN_ERROR:
	return get_can_error();
	break;
    case GET_PROTOCOL_REV_LEVEL:
	return get_protocol_rev_level();
	break;

	case AMB::Device::GET_SW_REV_LEVEL:
		return get_software_rev_level();
		break;

	case AMB::Device::GET_HW_REV_LEVEL:
		return get_hardware_rev_level();
		break;

    case GET_TRANS_NUM:
	return longToData(tmp, get_trans_num());
	break;
    // Optional generic point
    case GET_AMBIENT_TEMPERATURE:
	return floatDS1820ToData(tmp, get_ambient_temperature());
	break;

    // FEDewar specific points
    case GET_GM_SUPPLY_PRESSURE:
	return IEEEFloatToData(get_gm_supply_pressure());
	break;
    case GET_GM_RETURN_PRESSURE:
	return IEEEFloatToData(get_gm_return_pressure());
	break;
    case GET_JT_SUPPLY_PRESSURE:
	return IEEEFloatToData(get_jt_supply_pressure());
	break;
    case GET_JT_RETURN_PRESSURE:
	return IEEEFloatToData(get_jt_return_pressure());
	break;
    case GET_JT_RETURN_FLOW:
	return IEEEFloatToData(get_jt_return_flow());
	break;
    case GET_DEWAR_VALVE_STATE:
	return get_dewar_valve_state();
	break;
    case GET_GM_1ST_STAGE:
	return IEEEFloatToData(get_gm_1st_stage());
	break;
    case GET_GM_2ND_STAGE:
	return IEEEFloatToData(get_gm_2nd_stage());
	break;
    case GET_4K_STAGE:
	return IEEEFloatToData(get_4k_stage());
	break;
    case GET_TOP_SHIELD_TEMP:
	return IEEEFloatToData(get_top_shield_temp());
	break;
    case GET_BAND3_T1:
	return IEEEFloatToData(get_band3_t1());
	break;
    case GET_BAND3_T2:
	return IEEEFloatToData(get_band3_t2());
	break;
    case GET_BAND6_T1:
	return IEEEFloatToData(get_band6_t1());
	break;
    case GET_BAND6_T2:
	return IEEEFloatToData(get_band6_t2());
	break;
    case GET_DWR_VACCUM:
	return IEEEFloatToData(get_dwr_vacuum());
	break;
    case GET_PUMP_PORT_VACUUM:
	return IEEEFloatToData(get_pump_port_vacuum());
	break;
    case GET_INTERFACE_STATUS:
	return get_interface_status();
	break;
    default:
	std::cerr << "Switch does not match any case" << std::endl;
	throw FEDewarError("Unknown RCA in monitor command");
    }
}

// -----------------------------------------------------------------------------

void AMB::FEDewar::control(rca_t rca, const std::vector<CAN::byte_t> &data)
{
    trans_num_m++;
    switch (rca) {
    case SET_CLR_INTRF_LS218:
	{
	    if (data.size() != 1) {
		throw FEDewarError("SET_CLR_INTRF_LS218 data length must be 1");
	    }
	    set_clr_intrf_ls218(data[0]);
	}
	break;
    default:
	throw FEDewarError("Unknown RCA in control command");
    }
}

// -----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::FEDewar::get_can_error() const
{
    return AMB::Device::get_can_error();
}

// -----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::FEDewar::get_protocol_rev_level() const
{
    return AMB::Device::get_protocol_rev_level();
}


// -----------------------------------------------------------------------------
unsigned int AMB::FEDewar::get_trans_num() const
{
    return trans_num_m;
}

// -----------------------------------------------------------------------------
float AMB::FEDewar::get_ambient_temperature() const
{
    return 18.0;
}

// -----------------------------------------------------------------------------
float AMB::FEDewar::get_gm_supply_pressure() const
{
    return 19.0;
}

// -----------------------------------------------------------------------------
float AMB::FEDewar::get_gm_return_pressure() const
{
    return 20.0;
}

// -----------------------------------------------------------------------------
float AMB::FEDewar::get_jt_supply_pressure() const
{
    return 21.0;
}

// -----------------------------------------------------------------------------
float AMB::FEDewar::get_jt_return_pressure() const
{
    return 22.0;
}

// -----------------------------------------------------------------------------
float AMB::FEDewar::get_jt_return_flow() const
{
    return 23.0;
}

// -----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::FEDewar::get_dewar_valve_state() const
{
    std::vector<CAN::byte_t> tmp(1);
    tmp[0] = 1;
    return tmp;
}

// -----------------------------------------------------------------------------
float AMB::FEDewar::get_gm_1st_stage() const
{
    return 24.0;
}

// -----------------------------------------------------------------------------
float AMB::FEDewar::get_gm_2nd_stage() const
{
    return 25.0;
}

// -----------------------------------------------------------------------------
float AMB::FEDewar::get_4k_stage() const
{
    return 26.0;
}

// -----------------------------------------------------------------------------
float AMB::FEDewar::get_top_shield_temp() const
{
    return 27.0;
}

// -----------------------------------------------------------------------------
float AMB::FEDewar::get_band3_t1() const
{
    return 28.0;
}

// -----------------------------------------------------------------------------
float AMB::FEDewar::get_band3_t2() const
{
    return 29.0;
}

// -----------------------------------------------------------------------------
float AMB::FEDewar::get_band6_t1() const
{
    return 30.0;
}

// -----------------------------------------------------------------------------
float AMB::FEDewar::get_band6_t2() const
{
    return 31.0;
}

// -----------------------------------------------------------------------------
float AMB::FEDewar::get_dwr_vacuum() const
{
    return 32.0;
}

// -----------------------------------------------------------------------------
float AMB::FEDewar::get_pump_port_vacuum() const
{
    return 33.0;
}

// -----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::FEDewar::get_interface_status() const
{
    std::vector<CAN::byte_t> tmp(8);
    tmp[7] = 0;
    // In principle if the error number tmp[7] is null, there is no ASCII
    // description of the error. Here it's just for test purpose
    tmp[0] = 'N';
    tmp[1] = 'o';
    tmp[2] = 'E';
    tmp[3] = 'r';
    tmp[4] = 'r';
    tmp[5] = 'o';
    tmp[6] = 'r';
    return tmp;
}

// -----------------------------------------------------------------------------

void AMB::FEDewar::set_clr_intrf_ls218(CAN::byte_t cmd)
{
    //    nothing
}

// ----------------------------------------------------------------------------
/// Converts the float to data bytes.  Unlike AMB::floatToData this function
/// uses IEEE 754-1990 format
std::vector<CAN::byte_t> AMB::FEDewar::IEEEFloatToData(float value) const 
{
    const std::vector<CAN::byte_t> tmp;
    unsigned long *tmpLong;
    float tmpFloat = value;
    tmpLong = reinterpret_cast<unsigned long *> (&tmpFloat);
    return longToData(tmp, *tmpLong);
}
