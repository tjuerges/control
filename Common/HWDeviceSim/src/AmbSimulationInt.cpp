/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
*/

#include <TETimeUtil.h>
#include "AmbSimulationInt.h"


static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

AmbSimulationInt::AmbSimulationInt():
    AmbDeviceInt()
{
}

AmbSimulationInt::AmbSimulationInt(AMB::Device* device):
    AmbDeviceInt(),
    device_m(device)
{
    setSimObj(device_m);
}

AmbSimulationInt::~AmbSimulationInt()
{
    simulator_m.stop();
}

void AmbSimulationInt::setSimObj(AMB::Device* device)
{
    simulator_m.setDevice(device);
    simulator_m.start();
}

void AmbSimulationInt::monitor(AmbRelativeAddr  RCA,
			       AmbDataLength_t& dataLength,
			       AmbDataMem_t*    data,
			       sem_t*           synchLock,
			       Time*            timestamp,
			       AmbErrorCode_t*  status) {
    monitorNextTE(RCA, dataLength, data, synchLock, timestamp, status);
}

void AmbSimulationInt::command(AmbRelativeAddr      RCA,
			       AmbDataLength_t      dataLength,
			       const AmbDataMem_t*  data,
			       sem_t*               synchLock,
			       Time*                timestamp,
			       AmbErrorCode_t*      status) {
    commandNextTE(RCA, dataLength, data, synchLock, timestamp, status);
}

void AmbSimulationInt::monitorTE(ACS::Time        TimeEvent,
				 AmbRelativeAddr  RCA,
				 AmbDataLength_t& dataLength, 
				 AmbDataMem_t*    data,
				 sem_t*           synchLock,
				 Time*            timestamp,
				 AmbErrorCode_t*  status)
{
    simulator_m.insert(AMB_MONITOR, TimeEvent, RCA, dataLength, data, synchLock, timestamp, status);
}  

void AmbSimulationInt::commandTE(ACS::Time           TimeEvent,
				 AmbRelativeAddr     RCA,
				 AmbDataLength_t     dataLength, 
				 const AmbDataMem_t* data,
				 sem_t*              synchLock,
				 Time*               timestamp,
				 AmbErrorCode_t*     status)
{
    simulator_m.insert(AMB_CONTROL, TimeEvent, RCA, dataLength, const_cast<AmbDataMem_t*>(data), synchLock, timestamp, status);
}

void AmbSimulationInt::monitorNextTE(AmbRelativeAddr     RCA,
				     AmbDataLength_t&    dataLength, 
				     AmbDataMem_t*       data,
				     sem_t*              synchLock,
				     Time*               timestamp,
				     AmbErrorCode_t*     status)
{
  ACS::Time requestedTime = TETimeUtil::nearestTE(TimeUtil::ace2epoch(ACE_OS::gettimeofday())).value+TETimeUtil::TE_PERIOD_ACS/2;
  monitorTE(requestedTime, RCA, dataLength, data, synchLock, timestamp, status);
}

void  AmbSimulationInt::commandNextTE(AMBSystem::AmbRelativeAddr RCA,
				      AmbDataLength_t            dataLength, 
				      const AmbDataMem_t*        data,
				      sem_t*                     synchLock,
				      ACS::Time*                 timestamp,
				      AmbErrorCode_t*            status)
{
  ACS::Time requestedTime = TETimeUtil::nearestTE(TimeUtil::ace2epoch(ACE_OS::gettimeofday())).value;
  commandTE(requestedTime, RCA, dataLength, data, synchLock, timestamp, status);
}

void AmbSimulationInt::flushNode(ACS::Time                      TimeEvent,
				 ACS::Time*                     timestamp,
				 AmbErrorCode_t*                status)
{
    simulator_m.flush(TimeEvent, timestamp, status);
}

void AmbSimulationInt::flushRCA(ACS::Time                      TimeEvent,
				AmbRelativeAddr                RCA,
				ACS::Time*                     timestamp,
				AmbErrorCode_t*                status)
{
    simulator_m.flush(TimeEvent, RCA, timestamp, status);
}


/*___oOo___*/
