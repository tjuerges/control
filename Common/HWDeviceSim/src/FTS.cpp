//// @(#) $Id$
//
// Copyright (C) 2004
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Library General Public License
// as published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Library General Public License for more details.
//
// You should have received a copy of the GNU Library General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA. 
// Correspondence concerning ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include <iomanip>
#include "FTS.h"
#include "AMBUtil.h"
#include <sys/time.h>

#include <stdio.h>

// ----------------------------------------------------------------------------
AMB::FTS::FTS(node_t node, const std::vector<CAN::byte_t> &serialNumber,
              rca_t offset)
  : m_node(node), 
    m_sn(serialNumber),
    offset_m(offset),
    m_trans_num(0),
    m_errorCode(1),
    FREQ_m(),
    PHASE_VALS_m(),
    PHASE_SEQ1_m(0ULL),
    PHASE_SEQ2_m(0ULL),
    VOLTAGE_STATUS_m(0),
    PHASE_OFFSET_m(0)
{
  if (offset_m == 0 ) {
    std::cout << "Creating node " << node << ",s/n 0x";
    for (int i = 0; i < 8; i++) {
      std::cout << std::hex << std::setw(2) << std::setfill('0') 
		<< static_cast<int>(serialNumber[i]);
    }
    std::cout << ", FTS device" << std::endl;
  }

  for (int idx = 0; idx < 6; idx++)
    FREQ_m.push_back(0x0);

  for (int idx = 0; idx < 8; idx++)
    PHASE_VALS_m.push_back(0x0);

  /* Set the Default value for the Phase Delay */
  PHASE_OFFSET_m.push_back(0x07);
  PHASE_OFFSET_m.push_back(0xa1);
  PHASE_OFFSET_m.push_back(0x20);




#ifdef CREATE_LOG_FILE
  if (offset_m == 0) {
    char fileName[256];
    sprintf(fileName,"logFTS%x.%05d",node,getpid());
    
    logFile = fopen(fileName,"w+");
    if (logFile == NULL ) {
      std::cout << "\tERROR opening output file, Frequency Changes not logged." 
		<< std::endl;
    } else {
      std::cout << "\tLogging updates to: " << fileName << std::endl;
    }
  } else {
    lofFile = NULL;
  }
#else
    logFile = NULL;
#endif

}

// ----------------------------------------------------------------------------
AMB::FTS::~FTS() {
  if (logFile != NULL)
    fclose(logFile);
  // Nothing
}

// ----------------------------------------------------------------------------
AMB::node_t AMB::FTS::node() const {
  return m_node;
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::FTS::serialNumber() const {
  return m_sn;
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::FTS::monitor(rca_t rca) const {
  m_trans_num++;
  const std::vector<CAN::byte_t> tmp;
  switch (rca - offset_m) {
    // Mandatory generic points
  case GET_SERIAL_NUMBER:
    return serialNumber();
    break;
  case GET_CAN_ERROR:
    return get_can_error();
    break;
  case GET_PROTOCOL_REV_LEVEL:
    return get_protocol_rev_level();
    break;
	case AMB::Device::GET_SW_REV_LEVEL:
		return get_software_rev_level();
		break;
	case AMB::Device::GET_HW_REV_LEVEL:
		return get_hardware_rev_level();
		break;
  case GET_TRANS_NUM:
    return longToData(tmp, get_trans_num());
    break;
    // Optional generic point
  case GET_AMBIENT_TEMPERATURE:
    return floatDS1820ToData(tmp, get_ambient_temperature());
    break;
    
    // FTS specific points
  case GET_FREQ:
    return CAN_GET_FREQ();
    break;
  case GET_PHASE_VALS:
    return CAN_GET_PHASE_VALS();
    break;
  case GET_PHASE_SEQ1:
    return CAN_GET_PHASE_SEQ1();
    break;
  case GET_PHASE_SEQ2:
    return CAN_GET_PHASE_SEQ2();
    break;
  case GET_VOLTAGE_STATUS:
    return CAN_GET_VOLTAGE_STATUS();
    break;
  case GET_PHASE_OFFSET:
    return CAN_GET_PHASE_OFFSET();
    break;
  default:
    std::ostringstream msg;
    msg << "Unknown RCA '" << static_cast<int>(rca) 
        << "' does not match any FTS case." << std::endl;
    throw FTSError(msg.str());
  }
}

// ----------------------------------------------------------------------------
void AMB::FTS::control(rca_t rca, const std::vector<CAN::byte_t> &data) {
  m_trans_num++;
  switch (rca - offset_m) {
  case SET_FREQ:
      checkSize(data, 6, "SET_FREQ");
      CAN_SET_FREQ(data);
      break;
  case SET_PHASE_VALS:
      checkSize(data, 8, "SET_PHASE_VALS");
      CAN_SET_PHASE_VALS(data);
      break;
  case SET_PHASE_SEQ1:
      checkSize(data, 8, "SET_PHASE_SEQ1");
      CAN_SET_PHASE_SEQ1(data);
      break;
  case SET_PHASE_SEQ2:
      checkSize(data, 8, "SET_PHASE_SEQ2");
      CAN_SET_PHASE_SEQ2(data);
      break;
  case RESTART:
      checkSize(data, 1, "RESTART");
      CAN_RESTART(data);
      break;
  case RESET_VOLTAGE_STATUS:
      checkSize(data, 1, "RESET_VOLTAGE_STATUS");
      CAN_RESET_VOLTAGE_STATUS(data);
      break;
  case SET_PHASE_OFFSET:
      checkSize(data, 3, "SET_PHASE_OFFSET");
      CAN_SET_PHASE_OFFSET(data);
      break;
//   case XILINX_PROM_RESET:
//     checkSize(data, 1, "XILINX_PROM_RESET");
//     break;
  default:
    std::ostringstream msg;
    msg << "Unknown RCA 0x" << std::hex << static_cast<int>(rca) 
        << " in FTS command." << std::endl;
    throw FTSError(msg.str());
  }
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::FTS::CAN_GET_FREQ() const {
  return (FREQ_m);
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::FTS::CAN_GET_PHASE_VALS() const {
  return PHASE_VALS_m;
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::FTS::CAN_GET_PHASE_SEQ1() const {
  const std::vector<CAN::byte_t> tmp;
  return longlongToData(tmp, PHASE_SEQ1_m, 8);
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::FTS::CAN_GET_PHASE_SEQ2() const {
  const std::vector<CAN::byte_t> tmp;
  return longlongToData(tmp, PHASE_SEQ2_m, 8);
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::FTS::CAN_GET_VOLTAGE_STATUS() const {
  std::vector<CAN::byte_t> tmp;
  tmp.push_back(VOLTAGE_STATUS_m);
  return tmp;
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::FTS::CAN_GET_PHASE_OFFSET() const {
  return PHASE_OFFSET_m;
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::FTS::get_can_error() const {
  return AMB::Device::get_can_error();
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::FTS::get_protocol_rev_level() const {
  return AMB::Device::get_protocol_rev_level();
}

// ----------------------------------------------------------------------------
unsigned int AMB::FTS::get_trans_num() const {
  return m_trans_num;
}

// ----------------------------------------------------------------------------
double AMB::FTS::get_ambient_temperature() const {
  return 22.4;
}

// ----------------------------------------------------------------------------
void AMB::FTS::checkSize(const std::vector<CAN::byte_t> &data, int size,
			  const char *pExceptionMsg) {
  if( static_cast<int>(data.size()) != size ) {
    char exceptionMsg[200];
    sprintf(exceptionMsg,"%s data length is %d must be %d",pExceptionMsg,
	    data.size(), size);
    throw FTSError(exceptionMsg);
  }
}

// ----------------------------------------------------------------------------
void AMB::FTS::CAN_SET_FREQ(const std::vector<CAN::byte_t>& data)
{
  FREQ_m = data;
  if (logFile != NULL) {
      struct timeval tv;
      gettimeofday(&tv,NULL);
      fprintf(logFile,"FREQ: 0x");
      for (int idx = 0; idx < 6; idx++) 
	fprintf(logFile,"%02x",FREQ_m[idx]);
      fprintf(logFile,"\t Time:%ld.%06ld\n",tv.tv_sec, tv.tv_usec);
  } 
}

// ----------------------------------------------------------------------------
void AMB::FTS::CAN_SET_PHASE_VALS(const std::vector<CAN::byte_t>& data)
{
  PHASE_VALS_m = data;
}

// ----------------------------------------------------------------------------
void AMB::FTS::CAN_SET_PHASE_SEQ1(const std::vector<CAN::byte_t>& data)
{
    std::vector<CAN::byte_t> tmp = data;
    PHASE_SEQ1_m = dataToLonglong(tmp, 8);
}

// ----------------------------------------------------------------------------
void AMB::FTS::CAN_SET_PHASE_SEQ2(const std::vector<CAN::byte_t>& data)
{
    std::vector<CAN::byte_t> tmp = data;
    PHASE_SEQ2_m = dataToLonglong(tmp, 8);
}

// ----------------------------------------------------------------------------
void AMB::FTS::CAN_RESTART(const std::vector<CAN::byte_t>& data)
{
  if (logFile != NULL) {
      struct timeval tv;
      gettimeofday(&tv,NULL);
      fprintf(logFile,"RESTART\t Time:%ld.%06ld\n",tv.tv_sec, tv.tv_usec);
  } 
  return;
}

// ----------------------------------------------------------------------------
void AMB::FTS::CAN_RESET_VOLTAGE_STATUS(const std::vector<CAN::byte_t>& data)
{
    VOLTAGE_STATUS_m = 0;
}

// ----------------------------------------------------------------------------
void AMB::FTS::CAN_SET_PHASE_OFFSET(const std::vector<CAN::byte_t>& data)
{
  PHASE_OFFSET_m = data;
}
