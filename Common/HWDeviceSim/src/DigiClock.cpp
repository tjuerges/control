// @(#) $Id$
//
// Copyright (C) 2002
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include <iomanip>
#include "DigiClock.h"
#include "AMBUtil.h"
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>

// ----------------------------------------------------------------------------
AMB::DigiClock::DigiClock(node_t node, 
			  const std::vector<CAN::byte_t> &serialNumber)
  : m_node(node), 
    m_sn(serialNumber),
    m_trans_num(0),
    m_errorCode(1),
    DGCK_STATUS_m(1),
    MISSED_COMMAND_m(1),
    LOCK_INDICATOR_VOLTAGE_m(2.5),
    PS_VOLTAGE_CLOCK_m(6.5)
{
  std::cout << "Creating node " << node << ",s/n 0x";
  for (int i = 0; i < 8; i++) {
    std::cout << std::hex << std::setw(2) << std::setfill('0') 
	      << static_cast<int>(serialNumber[i]);
  }
  std::cout << ", DigiClock device" << std::endl;
  for (int idx = 0; idx < 8; idx ++) {
    LAST_PHASE_COMMAND_m[idx] = 0;
  }

#ifdef WRITE_LOG
  char fileName[256];
  sprintf(fileName,"logDigiClock.%05d",getpid());

  outputFile = fopen(fileName,"w+");
  if (outputFile == NULL ) {
    std::cout << "\tERROR opening output file, delay updates not logged." 
	      << std::endl;
  } else {
    std::cout << "\tLogging Delay updates to: " << fileName << std::endl;
  }
#else
  outputFile = NULL;
#endif
}

// ----------------------------------------------------------------------------
AMB::DigiClock::~DigiClock() {
  if (outputFile != NULL) {
    fclose(outputFile);
  }
}

// ----------------------------------------------------------------------------
AMB::node_t AMB::DigiClock::node() const {
  return m_node;
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::DigiClock::serialNumber() const {
  return m_sn;
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::DigiClock::monitor(rca_t rca) const {
  m_trans_num++;
  const std::vector<CAN::byte_t> tmp;
  std::vector<CAN::byte_t> retVal;
  
  switch (rca) {
    // Mandatory generic points
  case GET_SERIAL_NUMBER:
    return serialNumber();
    break;
  case GET_CAN_ERROR:
    return get_can_error();
    break;
  case GET_PROTOCOL_REV_LEVEL:
    return get_protocol_rev_level();
    break;
	case AMB::Device::GET_SW_REV_LEVEL:
		return get_software_rev_level();
		break;
	case AMB::Device::GET_HW_REV_LEVEL:
		return get_hardware_rev_level();
		break;
  case GET_TRANS_NUM:
    return longToData(tmp, get_trans_num());
    break;
    // Optional generic point
  case GET_AMBIENT_TEMPERATURE:
    return floatDS1820ToData(tmp, get_ambient_temperature());
    break;
    
    // DigiClock specific points
  case LAST_PHASE_COMMAND:
    return CAN_LAST_PHASE_COMMAND();
    break;
  case CURRENT_PHASE:
    return CAN_CURRENT_PHASE();
    break;
  case DGCK_STATUS:
    return CAN_DGCK_STATUS();
    break;
  case MISSED_COMMAND:
    return CAN_MISSED_COMMAND();
    break;
  case LOCK_INDICATOR_VOLTAGE:
    return CAN_LOCK_INDICATOR_VOLTAGE();
    break;
  case PS_VOLTAGE_CLOCK:
    return CAN_PS_VOLTAGE_CLOCK();
    break;
  default:
    throw DigiClckError("Unknown RCA in monitor command");
  }
}

// ----------------------------------------------------------------------------
void AMB::DigiClock::control(rca_t rca, const std::vector<CAN::byte_t> &data) {
  m_trans_num++;
  switch (rca) {
  case PHASE_COMMAND:
    if (outputFile != NULL) {
      struct timeval tv;
      gettimeofday(&tv,NULL);
      fprintf(outputFile,"Phase Command in %ld.%06ld ",tv.tv_sec, tv.tv_usec);
      fprintf(outputFile,"Data: 0x");
      for (int idx = 0; idx < 8; idx++)
	fprintf(outputFile,"%02x ",data[idx]);
      fprintf(outputFile,"\n");
      fflush(outputFile);
    }
    checkSize(data, 8, "Phase Command");
    for (int idx = 0; idx < 8; idx++)
      LAST_PHASE_COMMAND_m[idx]=data[idx];
    break;
  case PLL_LOCK_RESET:
    checkSize(data, 1, "PLL Lock Reset");
    DGCK_STATUS_m = 0;
    break;
  case MISSED_COMMAND_RESET:
    checkSize(data, 1, "Missed Command Reset");
    std::cout << "Reseting Missed Command" << std::endl;
    MISSED_COMMAND_m=0;
    break;
  default:
    throw DigiClckError("Unknown RCA in control command module DigiClock");
  }
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::DigiClock::CAN_LAST_PHASE_COMMAND() const {
  std::vector<CAN::byte_t> tmp;
  for (int idx = 0; idx < 8; idx++){
    tmp.push_back(LAST_PHASE_COMMAND_m[idx]);
  }
  return(tmp);
}

std::vector<CAN::byte_t> AMB::DigiClock::CAN_CURRENT_PHASE() const{
  std::vector<CAN::byte_t> tmp;
  for (int idx = 0; idx < 8; idx++){
    tmp.push_back(LAST_PHASE_COMMAND_m[idx]);
  }
  return(tmp);
}

std::vector<CAN::byte_t> AMB::DigiClock::CAN_DGCK_STATUS() const{
  std::vector<CAN::byte_t> tmp;
  tmp.push_back(DGCK_STATUS_m);
  return(tmp);

}
std::vector<CAN::byte_t> AMB::DigiClock::CAN_MISSED_COMMAND() const{
  std::vector<CAN::byte_t> tmp;
  tmp.push_back(getMISSED_COMMAND());
  return(tmp);
}
std::vector<CAN::byte_t> AMB::DigiClock::CAN_LOCK_INDICATOR_VOLTAGE() const{
  std::vector<CAN::byte_t> tmp;
  const short int roundedValue = 
    static_cast<short int>(LOCK_INDICATOR_VOLTAGE_m*1023.0/5.0);
  return shortToData(tmp, roundedValue);
}

std::vector<CAN::byte_t> AMB::DigiClock::CAN_PS_VOLTAGE_CLOCK() const {
  std::vector<CAN::byte_t> tmp;
  const short int roundedValue = 
    static_cast<short int>(PS_VOLTAGE_CLOCK_m*1023.0/10.0);
  
  return shortToData(tmp, roundedValue);
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::DigiClock::get_can_error() const {
  return AMB::Device::get_can_error();
}

// ----------------------------------------------------------------------------
std::vector<CAN::byte_t> AMB::DigiClock::get_protocol_rev_level() const {
  return AMB::Device::get_protocol_rev_level();
}

// ----------------------------------------------------------------------------
unsigned int AMB::DigiClock::get_trans_num() const {
  return m_trans_num;
}

// ----------------------------------------------------------------------------
double AMB::DigiClock::get_ambient_temperature() const {
  return 22.4;
}

// ----------------------------------------------------------------------------
void AMB::DigiClock::checkSize(const std::vector<CAN::byte_t> &data, int size,
			  const char *pExceptionMsg) {
  if( static_cast<int>(data.size()) != size ) {
    char exceptionMsg[200];
    sprintf(exceptionMsg,"%s data length is %d must be %d",pExceptionMsg,
	    data.size(), size);
    throw DigiClckError(exceptionMsg);
  }
}

/// Get functions for unit testing
// ----------------------------------------------------------------------------
CAN::byte_t* AMB::DigiClock::getLAST_PHASE_COMMAND() {
  return LAST_PHASE_COMMAND_m;
}

CAN::byte_t* AMB::DigiClock::getCURRENT_PHASE() {
  return LAST_PHASE_COMMAND_m;
}

CAN::byte_t  AMB::DigiClock::getDGCK_STATUS() const{
  return DGCK_STATUS_m;
}

CAN::byte_t  AMB::DigiClock::getMISSED_COMMAND() const {
  return MISSED_COMMAND_m;
}

double AMB::DigiClock::getLOCK_INDICATOR_VOLTAGE() const{
  return LOCK_INDICATOR_VOLTAGE_m;
}

double AMB::DigiClock::getPS_VOLTAGE_CLOCK() const{
  return PS_VOLTAGE_CLOCK_m;
}

