// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include <unistd.h>
#include "ACUDrive.h"

#undef DEBUG

using namespace std;
//------------------------------------------------------------------------------

ACUDrive::ACUDrive() : 
    active_m(false),
    ssActive_m(false),
    sharedSim_p(0)
{
    Az.debug_level = 0;
    El.debug_level = 0;

    Az.limits(AZ_LOWER_LIMIT,AZ_UPPER_LIMIT);
    El.limits(EL_LOWER_LIMIT,EL_UPPER_LIMIT);

    Az.stows(AZ_MAINT_STOW,AZ_SURVL_STOW);
    El.stows(EL_MAINT_STOW,EL_SURVL_STOW);

    // Start thread to read CAN messages and stick them in queue.
    active_m = true;
    if(pthread_create(&tid,
		      NULL,
		      &ACUDrive::updateLoop,
		      static_cast<void*>(this))!=0)
	{
	active_m = false;
	}

    // Start a thread to move the antenna in TELCAL SharedSimulator
    ssActive_m=true;
    if(pthread_create(&sharedSimThreadId, 
		      NULL, 
		      &ACUDrive::sharedSimUpdateLoop, 
		      static_cast<void*>(this))!=0)
	{
	ssActive_m = false;
	}
}

//------------------------------------------------------------------------------

ACUDrive::~ACUDrive() 
{ 
    if(active_m)
	{
	active_m = false;
	pthread_join(tid, NULL); 
	}
    if(ssActive_m)
	{
	ssActive_m = false;
	pthread_join(sharedSimThreadId, NULL); 
	}

}

//------------------------------------------------------------------------------

int ACUDrive::setAzPsn(double position,double velocity)
{
    return Az.set(position,velocity);
}

//------------------------------------------------------------------------------

int ACUDrive::setElPsn(double position,double velocity)
{
    return El.set(position,velocity);
}

//------------------------------------------------------------------------------

void ACUDrive::getAzPsn(double& position,double& velocity) const
{
    double accel;
    Az.get_cur(position,velocity,accel);
}

//------------------------------------------------------------------------------

void ACUDrive::getElPsn(double& position,double& velocity) const
{
    double accel;
    El.get_cur(position,velocity,accel);
}

//------------------------------------------------------------------------------

void ACUDrive::getAzStatus(unsigned long long& status) const
{
    unsigned int stow = Az.get_stow_pin() == PIN_IN ? 1 : 0;
    unsigned int zerospeed = Az.driving() ? 1 : 0;
    unsigned long auxmode = (unsigned long)Az.get_aux_mode();
    unsigned char* ptr = (unsigned char*)&status;
    auxmode = auxmode ^ 3;
    
    ptr[0] = Az.limit;
    ptr[1] = zerospeed << 3 | stow << 4;
    ptr[2] = 0;
    ptr[3] = 0;
    ptr[4] = 0;
    ptr[5] = 0;
    ptr[6] = auxmode;
    ptr[7] = 0;
}

//------------------------------------------------------------------------------

void ACUDrive::getElStatus(unsigned long long& status) const
{
  unsigned int stow = El.get_stow_pin() == PIN_IN ? 1 : 0;

  unsigned int zerospeed = El.driving() ? 1 : 0;
  double p, v, a;
  unsigned char* ptr = (unsigned char*)&status;
  int mode = (int)El.get_aux_mode();

  //int mode = static_cast<int>(El.get_aux_mode());
  // Mode is a weird large number!!!
  //mode = 0;

  int lut[4] = {0, 3, 12, 15};
  //unsigned long lut[4] = {0, 3, 12, 15};
  unsigned long auxmode = (unsigned long long)lut[mode];
  //unsigned long auxmode = static_cast<unsigned long>(lut[mode]);
  
  unsigned long overtop = 0;
  
  auxmode = auxmode ^ 0xF;
  //unsigned long mask = 0xfUL;
  //auxmode ^= 0xF;
  //auxmode ^= mask;
  El.get_cur(p,v,a);
  if (p > 90.)
      overtop = 0x10;
  
  ptr[0] = El.limit;
  ptr[1] = zerospeed << 3 | stow << 4;
  //ptr[1] = 0;
  ptr[2] = 0;
  ptr[3] = 0;
  ptr[4] = 0;
  ptr[5] = 0;
  ptr[6] = auxmode;
  ptr[7] = overtop;
  //ptr[6] = 0;
  //ptr[7] = 0;
}

#if 0  /* DISCARD!! */
//------------------------------------------------------------------------------

int ACUDrive::setAzMode(AZEL_Mode_t m)
{
    return Az.set_mode(m);
}

//------------------------------------------------------------------------------

int ACUDrive::setElMode(AZEL_Mode_t m)
{
    return El.set_mode(m);
}

//------------------------------------------------------------------------------

void ACUDrive::getAzMode(AZEL_Mode_t& m) const
{
    m = Az.get_mode();
}

//------------------------------------------------------------------------------

void ACUDrive::getElMode(AZEL_Mode_t& m) const
{
    m = El.get_mode();
}
#endif

//------------------------------------------------------------------------------

std::vector<char> ACUDrive::getACUMode() const
{
#ifdef DEBUG
    cout << "ACUDrive::getACUMode" << endl;
#endif
    std::vector<char> rtnVal(2);
#ifdef DEBUG
    cout << "======================================" << endl;
    cout << "ACUDrive::getACUMode: getting EL mode " << endl;
#endif
    int elmode = (int)El.get_mode();
    rtnVal[0] = (char)elmode;
#ifdef DEBUG
    cout << "ACUDrive::getACUMode: got     EL mode " << elmode << endl;
    cout << "======================================" << endl;
#endif
    rtnVal[0] <<= 4;
#ifdef DEBUG
    cout << "ACUDrive::getACUMode: getting AZ mode " << endl;
#endif
    int azmode = (int)Az.get_mode();
    rtnVal[0] |= (char)azmode;
#ifdef DEBUG
    cout << "ACUDrive::getACUMode: got     AZ mode " << azmode << endl;
    cout << "======================================" << endl;
#endif
    rtnVal[1] = ACU_REMOTE;  // always remote
    return rtnVal;
}

//------------------------------------------------------------------------------
int ACUDrive::setACUMode(unsigned char mode)
{
#ifdef DEBUG
    cout << "ACUDrive::setACUMode" << endl;
    cout << "ACUDrive::setACUMode: received mode " << (int) mode << endl;
    cout << "ACUDrive::setACUMode: about to send Az.set_mode" << endl;
#endif
    int requested_mode = (int) mode & 0xF;
    int rc;
#ifdef DEBUG
    cout << "ACUDrive::setACUMode: received az mode " << requested_mode << endl;
#endif
    if (requested_mode<6)
	{
	rc = Az.set_mode((AZEL_Mode_t)(requested_mode));
#ifdef DEBUG
	cout << "ACUDrive::setACUMode: az mode set" << endl;
#endif
	}
    else
	{
	rc =1;
#ifdef DEBUG
	cout << "ACUDrive::setACUMode: az mode not set. Wrong mode requested" << endl;
#endif
	}
    
    // right shift to get requested el mode
    mode >>= 4;

#ifdef DEBUG
    cout << "ACUDrive::setACUMode: about to send El.set_mode" << endl;
#endif
    requested_mode = (int) mode & 0xF;
#ifdef DEBUG
    cout << "ACUDrive::setACUMode: received el mode " << requested_mode << endl;
#endif
    if (requested_mode<6)
	{   
	rc |= El.set_mode((AZEL_Mode_t)(mode & 0xF));
#ifdef DEBUG
	cout << "ACUDrive::setACUMode: el mode set" << endl;
#endif
	}
    else
	{
	rc |= 1;
#ifdef DEBUG
	cout << "ACUDrive::setACUMode: el mode not set. Wrong mode requested" << endl;
#endif
	}
#ifdef DEBUG
    cout << "ACUDrive::setACUMode: return code " << (int) rc << endl;
#endif
    return rc;	// 0-OK, 1-ERROR
}

//------------------------------------------------------------------------------

void ACUDrive::handler()
{
    Az.tick();
    El.tick();

}

//------------------------------------------------------------------------------

// This function is static to be the pthread_create() argument.  A pointer to 
// the class object is passed to allow access to its variables and methods().
void* ACUDrive::updateLoop(void* data)
{
    ACUDrive* This = static_cast<ACUDrive*>(data);
    while(This->active_m)
	{
	usleep((int)(1000000 * TIME_UNIT));
	This->handler();
	}
    pthread_exit(NULL);
}

//------------------------------------------------------------------------------

void ACUDrive::sharedSimHandler() {
    
    // set azimuth and elevation in TELCAL SharedSimulator
    if (sharedSim_p && sharedSim_p->haveSimulatorRef()) {
    double az, azvel, azacc;
    double el, elvel, elacc;
    // getAzPsn(az, azvel);
    // getElPsn(el, elvel);
    
    Az.get_track(az, azvel, azacc);
    El.get_track(el, elvel, elacc);
    
    sharedSim_p->setAntPosn(az, el);
    }
}

//------------------------------------------------------------------------------

void* ACUDrive::sharedSimUpdateLoop(void * data) {

  ACUDrive* This = static_cast<ACUDrive*>(data);

  while(This->ssActive_m) 
      {
      usleep((int) (1e6 * SS_ANTPOS_PERIOD_));
      This->sharedSimHandler();
      }
  pthread_exit(NULL);
}
