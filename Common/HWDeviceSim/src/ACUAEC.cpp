// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include <queue>
#include <iomanip>
#include <stdio.h>
#include "AMBUtil.h"
#include "ACUAECSim.h"
#include "CANError.h"
#include "ACUAEC.h"

using AMB::node_t;
using AMB::rca_t;

// -----------------------------------------------------------------------------

AMB::ACUAEC::ACUAEC(
        node_t node,
	const std::vector<CAN::byte_t>& serialNumber)
    :temp_m(104),
     tilt_m(2),
     node_m(node),sn_m(serialNumber)
{
    for (int i = 0; i < 104; i++) { 
    temp_m[i] = 20.0 + static_cast<double>(i)/10.0;
    }
    
    for (int i = 0; i < 2; i++) { 
    tilt_m[i].x = (1.0 + static_cast<double>(i));
    tilt_m[i].y = (-1.0 - static_cast<double>(i));
    tilt_m[i].temp = (20.0 + static_cast<double>(i));
    }
    
    std::cout << "Creating node " << node << ", s/n 0x";
    for (int i = 0; i < 8; i++)
	std::cout << std::hex << std::setw(2) << std::setfill('0') << (int)serialNumber[i];
    std::cout << ", AEC ACU device" << std::endl;

    std::string error;
    if ( ! validateNodeNumber(error,this->node()))
	{
	    throw ACUError("Unknown RCA in monitor command");
//	throw CAN::Error(error);
	}

    if ( ! validateSerialNumber(error,this->serialNumber()))
	{
	    throw ACUError("Unknown RCA in monitor command");
//	throw CAN::Error(error);
	}
}

// -----------------------------------------------------------------------------

AMB::ACUAEC::~ACUAEC()
{
    // Nothing
}

// -----------------------------------------------------------------------------

node_t AMB::ACUAEC::node() const
{
    return node_m;
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::ACUAEC::serialNumber() const
{
    return sn_m;
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::ACUAEC::monitor(
    rca_t rca) const
{
    // empty CAN message
    const std::vector<CAN::byte_t> nostring;

    // these are dummies to allow calling ICD methods
    int errcode;
    unsigned long long timestamp;

    // bump transaction count
    trans_num_m++;

/*    rca_t myrca = rca & 0x3FFFF;

    if ((myrca != ACUAECbase::ACU_MODE_RSP) && 
	(myrca != ACUAECbase::AZ_POSN_RSP) && 
        (myrca != ACUAECbase::EL_POSN_RSP) && 
	(myrca != ACUAECbase::GET_AZ_ENC) &&
        (myrca != ACUAECbase::GET_EL_ENC))
        printf ("Received monitor command 0x%x\n", myrca); */

    switch (rca & 0x3FFFF)      // rca is first 18 bits
	{

	// mandatory standard generic monitor points

	case AMB::Device::GET_HW_REV_LEVEL:
		return get_hardware_rev_level();
		break;

	case GET_PROTOCOL_REV_LEVEL:  // aka GET_SW_REV_LEVEL in ICD 9
	    return get_protocol_rev_level();

	case GET_CAN_ERROR:
	    return get_can_error();

	case GET_TRANS_NUM:
           return longToData(nostring,get_trans_num());
	   
	case GET_SW_REV_LEVEL:  // aka GET_SYSTEM_ID in ICD 9
	    return get_sw_rev_level();
    
	// ACU specific monitor points (order from AMBSimACU.h)

	case ACUAECbase::ACUAECbase::ACU_MODE_RSP:
	    {
	    std::vector<char> mode = sim_m.get_acu_mode_rsp(errcode,timestamp);
            std::vector<CAN::byte_t> tmps = charToData(nostring,mode[0]);
            tmps = charToData(tmps,mode[1]);
            return tmps;
	    }

        case ACUAECbase::AZ_POSN_RSP:
            {
            unsigned long long rsp = sim_m.get_az_posn_rsp(errcode,timestamp);
            // actually two ints, so keep int order, but convert to network
            // order the two ints.
            unsigned long* ptr = (unsigned long*)&rsp;
            std::vector<CAN::byte_t> tmps = longToData(nostring,ptr[0]);
            return longToData(tmps,ptr[1]);
	    }

	case ACUAECbase::EL_POSN_RSP:
            {
            unsigned long long rsp = sim_m.get_el_posn_rsp(errcode,timestamp);
            // actually two ints, so keep int order, but convert to network
            // order the two ints.
            unsigned long* ptr = (unsigned long*)&rsp;
            std::vector<CAN::byte_t> tmps = longToData(nostring,ptr[0]);
            return longToData(tmps,ptr[1]);
	    }

        case ACUAECbase::GET_ACU_ERROR:
            {
            unsigned long long rsp = sim_m.get_acu_error(errcode,timestamp);

	    if(rsp == 0L)
		{
		// 0 bytes sent implies no error
		std::vector<CAN::byte_t> tmps(0);
		return tmps;
		}
	    else
		{
		// stuff error code into byte 0
		std::vector<CAN::byte_t> tmps = longlongToData(nostring,rsp,1);
		
		// extract the next 4 bytes as an integer in host byte order
		unsigned char* ptr = (unsigned char*)&rsp;
		unsigned long rca;
		memcpy(&rca,ptr+1,4);
		
		// now convert host byte order to network order, pack it into the
		// buffer, and return the buffer.
		return longToData(tmps,rca);
		}
	    }

	case ACUAECbase::GET_AZ_BRAKE:
	    return longlongToData(nostring,
				  sim_m.get_az_brake(errcode,timestamp),1);

	case ACUAECbase::GET_AZ_ENC:
	    return longToData(nostring,sim_m.get_az_enc(errcode,timestamp));

        case ACUAECbase::GET_AZ_MOTOR:
            return longlongToData(nostring,
                                  sim_m.get_az_motor(errcode,timestamp),1);

	case ACUAECbase::GET_AZ_MOTOR_CURRENTS:                         
	    // m1, m2
	    // 48, 64 Amps
// !!! these are flipped !!!
	    return longlongToData(nostring,0x400030,4);     

	case ACUAECbase::GET_AZ_MOTOR_TEMPS:
	    // m1, m2
	    // -34, -18 C
// !!! these are flipped !!!
	    return longlongToData(nostring,0x200010,4);     
	    
	case ACUAECbase::GET_AZ_MOTOR_TORQUE:
	    // m1, m2
	    // 80, 96 Nm
// !!! these are flipped !!!
	    return longlongToData(nostring,0x3C00320,4);     
        
	case ACUAECbase::GET_AZ_SERVO_COEFF_0:
	case ACUAECbase::GET_AZ_SERVO_COEFF_0 + 1:
	case ACUAECbase::GET_AZ_SERVO_COEFF_0 + 2:
	case ACUAECbase::GET_AZ_SERVO_COEFF_0 + 3:
	case ACUAECbase::GET_AZ_SERVO_COEFF_0 + 4:
	case ACUAECbase::GET_AZ_SERVO_COEFF_0 + 5:
	case ACUAECbase::GET_AZ_SERVO_COEFF_0 + 6:
	case ACUAECbase::GET_AZ_SERVO_COEFF_0 + 7:
	case ACUAECbase::GET_AZ_SERVO_COEFF_0 + 8:
	case ACUAECbase::GET_AZ_SERVO_COEFF_0 + 9:
	case ACUAECbase::GET_AZ_SERVO_COEFF_0 + 10:
	case ACUAECbase::GET_AZ_SERVO_COEFF_0 + 11:
	case ACUAECbase::GET_AZ_SERVO_COEFF_0 + 12:
	case ACUAECbase::GET_AZ_SERVO_COEFF_0 + 13:
	case ACUAECbase::GET_AZ_SERVO_COEFF_0 + 14:
	case ACUAECbase::GET_AZ_SERVO_COEFF_0 + 15:
            return doubleToData(sim_m.get_az_servo_coeff(
		rca - ACUAECbase::GET_AZ_SERVO_COEFF_0,errcode,timestamp));
	    
	case ACUAECbase::GET_AZ_MOTION_LIMIT:
	    return longlongToData(nostring,
				  sim_m.get_az_motion_lim(errcode,timestamp),4);
	
	case ACUAECbase::GET_AZ_STATUS:
	    {
	    unsigned long long stat = sim_m.get_az_status(errcode,timestamp);
	    unsigned char* stat_p = (unsigned char*)&stat;
            std::vector<CAN::byte_t> tmps = charToData(nostring,stat_p[0]);
	    for (int i = 1; i < 8; i++)
		tmps = charToData(tmps,stat_p[i]);
            return tmps;
	    }

	case ACUAECbase::GET_EL_BRAKE:
	    return longlongToData(nostring,
				  sim_m.get_el_brake(errcode,timestamp),1);

	case ACUAECbase::GET_EL_ENC:
	    {
	    std::vector<long> positions(2);
	    sim_m.get_el_enc(positions,errcode,timestamp);
            std::vector<CAN::byte_t> tmps = longToData(nostring,positions[0]);
	    return longToData(tmps,positions[1]);
	    }

        case ACUAECbase::GET_EL_MOTOR:
            return longlongToData(nostring,
                                  sim_m.get_el_motor(errcode,timestamp),1);

	case ACUAECbase::GET_EL_MOTOR_CURRENTS:                         
	    // 176 Amps 
	    return longlongToData(nostring,0xB0,2);
	    
	case ACUAECbase::GET_EL_MOTOR_TEMPS:                            
	    // 62 C
	    return longlongToData(nostring,0x70,2); 
	    
	case ACUAECbase::GET_EL_MOTOR_TORQUE:
	    // 240 Nm
	    return longlongToData(nostring,0x960,2);
	    
	case ACUAECbase::GET_EL_SERVO_COEFF_0:
	case ACUAECbase::GET_EL_SERVO_COEFF_0 + 1:
	case ACUAECbase::GET_EL_SERVO_COEFF_0 + 2:
	case ACUAECbase::GET_EL_SERVO_COEFF_0 + 3:
	case ACUAECbase::GET_EL_SERVO_COEFF_0 + 4:
	case ACUAECbase::GET_EL_SERVO_COEFF_0 + 5:
	case ACUAECbase::GET_EL_SERVO_COEFF_0 + 6:
	case ACUAECbase::GET_EL_SERVO_COEFF_0 + 7:
	case ACUAECbase::GET_EL_SERVO_COEFF_0 + 8:
	case ACUAECbase::GET_EL_SERVO_COEFF_0 + 9:
	case ACUAECbase::GET_EL_SERVO_COEFF_0 + 10:
	case ACUAECbase::GET_EL_SERVO_COEFF_0 + 11:
	case ACUAECbase::GET_EL_SERVO_COEFF_0 + 12:
	case ACUAECbase::GET_EL_SERVO_COEFF_0 + 13:
	case ACUAECbase::GET_EL_SERVO_COEFF_0 + 14:
	case ACUAECbase::GET_EL_SERVO_COEFF_0 + 15:
            return doubleToData(sim_m.get_el_servo_coeff(
		rca - ACUAECbase::GET_EL_SERVO_COEFF_0,errcode,timestamp));

    	case ACUAECbase::GET_EL_MOTION_LIMIT:
	    return longlongToData(nostring,
				  sim_m.get_el_motion_lim(errcode,timestamp),4);
    
	case ACUAECbase::GET_EL_STATUS:
	    {
	    unsigned long long stat = sim_m.get_el_status(errcode,timestamp);
	    unsigned char* stat_p = (unsigned char*)&stat;
            std::vector<CAN::byte_t> tmps = charToData(nostring,stat_p[0]);
	    for (int i = 1; i < 8; i++)
		tmps = charToData(tmps,stat_p[i]);
            return tmps;
	    }

	case ACUAECbase::GET_IDLE_STOW_TIME:
	    return shortToData(nostring,
			       sim_m.get_idle_stow_time(errcode,timestamp));
	    
	case ACUAECbase::GET_IP_ADDRESS:
	    return longToData(nostring,sim_m.get_ip_address(errcode,timestamp));

	case ACUAECbase::GET_SYSTEM_STATUS:
	    return longlongToData(nostring,
				  sim_m.get_system_status(errcode,timestamp),4);

	case ACUAECbase::GET_PT_MODEL_COEFF_0:
	case ACUAECbase::GET_PT_MODEL_COEFF_0 + 1:
	case ACUAECbase::GET_PT_MODEL_COEFF_0 + 2:
	case ACUAECbase::GET_PT_MODEL_COEFF_0 + 3:
	case ACUAECbase::GET_PT_MODEL_COEFF_0 + 4:
	case ACUAECbase::GET_PT_MODEL_COEFF_0 + 5:
	case ACUAECbase::GET_PT_MODEL_COEFF_0 + 6:
	case ACUAECbase::GET_PT_MODEL_COEFF_0 + 7:
	case ACUAECbase::GET_PT_MODEL_COEFF_0 + 8:
	case ACUAECbase::GET_PT_MODEL_COEFF_0 + 9:
            return doubleToData(sim_m.get_pt_model_coeff(
		rca - ACUAECbase::GET_PT_MODEL_COEFF_0,errcode,timestamp));
	
	case ACUAECbase::GET_SHUTTER:
	    return charToData(nostring,sim_m.get_shutter(errcode,timestamp));
	    
	case ACUAECbase::GET_STOW_PIN:
	    return longlongToData(nostring,
				  sim_m.get_stow_pin(errcode,timestamp),2);

	case ACUAECbase::GET_SUBREF_ABS_POSN:
	    // X, Y and Z
	    // 16, 32 and 48
// !!! these are flipped !!!
	    return longlongToData(nostring,0x3000200010ULL,6);     
	    	
	case ACUAECbase::GET_SUBREF_DELTA_POSN:
	    // X, Y and Z
	    // 64, 80, 96
// !!! these are flipped !!!
	    return longlongToData(nostring,0x6000500040ULL,6);     
	    	    	
	case ACUAECbase::GET_SUBREF_LIMITS:
	    return longlongToData(nostring,0x0,3);     
   	    	
	case ACUAECbase::GET_SUBREF_STATUS:
	    return longlongToData(nostring,0x04040400,4);     
	   	    	
	case ACUAECbase::GET_METR_MODE:
	    return longlongToData(nostring,0x3,1);     
	 	   	    	
        case ACUAECbase::GET_METR_TILT_1:
	  return tiltData(0);

        case ACUAECbase::GET_METR_TILT_2:
	  return tiltData(1);

        case ACUAECbase::GET_METR_TEMPS_0:
	  return tempData(0);

        case ACUAECbase::GET_METR_TEMPS_0+1:
	  return tempData(4);

        case ACUAECbase::GET_METR_TEMPS_0+2:
	  return tempData(8);

        case ACUAECbase::GET_METR_TEMPS_0+3:
	  return tempData(12);

        case ACUAECbase::GET_METR_TEMPS_0+4:
	  return tempData(16);

        case ACUAECbase::GET_METR_TEMPS_0+5:
	  return tempData(20);

        case ACUAECbase::GET_METR_TEMPS_0+6:
	  return tempData(24);

        case ACUAECbase::GET_METR_TEMPS_0+7:
	  return tempData(28);

        case ACUAECbase::GET_METR_TEMPS_0+8:
	  return tempData(32);

        case ACUAECbase::GET_METR_TEMPS_0+9:
	  return tempData(36);

        case ACUAECbase::GET_METR_TEMPS_0+10:
	  return tempData(40);

        case ACUAECbase::GET_METR_TEMPS_0+11:
	  return tempData(44);

        case ACUAECbase::GET_METR_TEMPS_0+12:
	  return tempData(48);

        case ACUAECbase::GET_METR_TEMPS_0+13:
	  return tempData(52);

        case ACUAECbase::GET_METR_TEMPS_0+14:
	  return tempData(56);

        case ACUAECbase::GET_METR_TEMPS_0+15:
	  return tempData(60);

        case ACUAECbase::GET_METR_TEMPS_0+16:
	  return tempData(64);

        case ACUAECbase::GET_METR_TEMPS_0+17:
	  return tempData(68);

	case ACUAECbase::GET_METR_TEMPS_0+18:
	  return tempData(72);

	case ACUAECbase::GET_METR_TEMPS_0+19:
	  return tempData(76);

	case ACUAECbase::GET_METR_TEMPS_0+20:
	  return tempData(80);

	case ACUAECbase::GET_METR_TEMPS_0+21:
	  return tempData(84);

	case ACUAECbase::GET_METR_TEMPS_0+22:
	  return tempData(88);

	case ACUAECbase::GET_METR_TEMPS_0+23:
	  return tempData(92);

	case ACUAECbase::GET_METR_TEMPS_0+24:
	  return tempData(96);

	case ACUAECbase::GET_METR_TEMPS_0+25:
	  return tempData(100);

	case ACUAECbase::GET_METR_DELTAS:
	{
	    std::vector<CAN::byte_t> tmps = longToData(nostring,0x20);
	    return longToData(tmps,0x20);
	}
      
	case ACUAECbase::GET_POWER_STATUS:                            
	    return longlongToData(nostring,0x8,2);
	         
	case ACUAECbase::GET_AC_STATUS:                            
	    return longlongToData(nostring,0x1,1); 

	default:
	    std::cerr << "Switch does not match ACU any case" << std::endl;
	    throw ACUError("Unknown RCA in monitor command");
	}
}

// -----------------------------------------------------------------------------

void AMB::ACUAEC::control(
	rca_t rca,
	const std::vector<CAN::byte_t>& data)
{
    // these are dummies to allow calling ICD methods
    int errcode;
    unsigned long long timestamp;

    // bump transaction count
    trans_num_m++;

/*    rca_t myrca = rca & 0x3FFFF;

    if ((myrca != ACUAECbase::AZ_TRAJ_CMD) && 
	(myrca != ACUAECbase::EL_TRAJ_CMD))
        printf ("Received control command 0x%x\n", myrca); */

    switch (rca & 0x3FFFF)      // rca is first 18 bits
	{
	case ACUAECbase::ACU_MODE_CMD:
	    {
	    if (data.size() != 1)
		{
		throw ACUError("ACU_MODE_CMD data length must be 1");
		}
	    std::vector<CAN::byte_t> temp(data);
	    unsigned char mode;
	    mode = dataToChar(temp);
	    sim_m.acu_mode_cmd(mode,errcode,timestamp);
	    }
	    break;

	case ACUAECbase::AZ_TRAJ_CMD:
	    {
	    if (data.size() != 8)
		{
		throw ACUError("AZ_TRAJ_CMD data length must be 8");
		}
	    std::vector<CAN::byte_t> temp(data);
	    long position = dataToLong(temp);
	    long velocity = dataToLong(temp);
	    sim_m.az_traj_cmd(position,velocity,errcode,timestamp);
	    }
	    break;

	case ACUAECbase::EL_TRAJ_CMD:
	    {
            if (data.size() != 8)
		{
                throw ACUError("EL_TRAJ_CMD data length must be 8");
		}
            std::vector<CAN::byte_t> temp(data);
            long position = dataToLong(temp);
            long velocity = dataToLong(temp);
            sim_m.el_traj_cmd(position,velocity,errcode,timestamp);
	    }
            break;

	case ACUAECbase::CLEAR_FAULT_CMD:
	    {
            if (data.size() != 1)
		{
                throw ACUError("CLEAR_FAULT_CMD data length must be 1");
		}
            sim_m.clear_fault_cmd(errcode,timestamp);
	    }
            break;
	
	case ACUAECbase::RESET_ACU_CMD:
	    {
            if (data.size() != 1)
		{
                throw ACUError("RESET_ACU_CMD data length must be 1");
		}
            sim_m.reset_acu_cmd(errcode,timestamp);
	    }
            break;
	
	case ACUAECbase::SET_SHUTTER:
	    {
            if (data.size() != 1)
		{
                throw ACUError("ACU_MODE_CMD data length must be 1");
		}
            sim_m.set_shutter(data[0],errcode,timestamp);
	    }
            break;

	case ACUAECbase::SET_STOW_PIN:
	    {
            /*
             * LOOK AT ME
             * The bits are 0xAAEE in network order, where AA is the Az, and
             * EE is the El.  It is listed as byte 0 for Az, and byte 1 for El.
             */
            if (data.size() != 2)
		{
                throw ACUError("SET_STOW_IN data length must be 2");
		}
            unsigned short pins;
            unsigned char* ptr = (unsigned char*)&pins;
            ptr[0] = data[0];
            ptr[1] = data[1];
            sim_m.set_stow_pin(pins,errcode,timestamp);
	    }
            break;

	case ACUAECbase::SET_IDLE_STOW_TIME:
	    {
            if (data.size() != 2)
		{
                throw ACUError("ACU_MODE_CMD data length must be 2");
		}
            std::vector<CAN::byte_t> temp(data);
            unsigned short idle_time = dataToShort(temp);
            sim_m.set_idle_stow_time(idle_time,errcode,timestamp);
	    }
            break;

	case ACUAECbase::SET_AZ_BRAKE:
	    {
            if (data.size() != 1)
		{
                throw ACUError("SET_AZ_BRAKE data length must be 1");
		}
            unsigned char mode = data[0];
            sim_m.set_az_brake(mode,errcode,timestamp);
	    }
            break;
	    
	case ACUAECbase::SET_EL_BRAKE:
	    {
            if (data.size() != 1)
		{
                throw ACUError("SET_EL_BRAKE data length must be 1");
		}
            unsigned char mode = data[0];
            sim_m.set_el_brake(mode,errcode,timestamp);
	    }
            break;	

	case ACUAECbase::SET_AZ_MOTOR_POWER:
	    {
            if (data.size() != 1)
		{
                throw ACUError("SET_AZ_MOTOR_POWER data length must be 1");
		}
            unsigned char mode = data[0];
            sim_m.set_az_motor_power(mode,errcode,timestamp);
	    }
            break;
	    
	case ACUAECbase::SET_EL_MOTOR_POWER:
	    {
            if (data.size() != 1)
		{
                throw ACUError("SET_EL_MOTOR_POWER data length must be 1");
		}
            unsigned char mode = data[0];
            sim_m.set_el_motor_power(mode,errcode,timestamp);
	    }
            break;
	  
	case ACUAECbase::SET_AZ_MOTOR_DRIVER:
	    {
            if (data.size() != 1)
		{
                throw ACUError("SET_AZ_MOTOR_DRIVER data length must be 1");
		}
            unsigned char mode = data[0];
            sim_m.set_az_motor_driver(mode,errcode,timestamp);
	    }
            break;
	 	  
	case ACUAECbase::SET_EL_MOTOR_DRIVER:
	    {
            if (data.size() != 1)
		{
                throw ACUError("SET_EL_MOTOR_DRIVER data length must be 1");
		}
            unsigned char mode = data[0];
            sim_m.set_el_motor_driver(mode,errcode,timestamp);
	    }
            break;
	 
	case ACUAECbase::SET_AZ_SERVO_COEFF_0:
	case ACUAECbase::SET_AZ_SERVO_COEFF_0 + 1:
	case ACUAECbase::SET_AZ_SERVO_COEFF_0 + 2:
	case ACUAECbase::SET_AZ_SERVO_COEFF_0 + 3:
	case ACUAECbase::SET_AZ_SERVO_COEFF_0 + 4:
	case ACUAECbase::SET_AZ_SERVO_COEFF_0 + 5:
	case ACUAECbase::SET_AZ_SERVO_COEFF_0 + 6:
	case ACUAECbase::SET_AZ_SERVO_COEFF_0 + 7:
	case ACUAECbase::SET_AZ_SERVO_COEFF_0 + 8:
	case ACUAECbase::SET_AZ_SERVO_COEFF_0 + 9:
	case ACUAECbase::SET_AZ_SERVO_COEFF_0 + 10:
	case ACUAECbase::SET_AZ_SERVO_COEFF_0 + 11:
	case ACUAECbase::SET_AZ_SERVO_COEFF_0 + 12:
	    {
	    std::vector<CAN::byte_t> temp(data);
            sim_m.set_az_servo_coeff(
		rca - ACUAECbase::SET_AZ_SERVO_COEFF_0,dataToDouble(temp),
		errcode,timestamp);
	    }
	    break;

	case ACUAECbase::SET_EL_SERVO_COEFF_0:
	case ACUAECbase::SET_EL_SERVO_COEFF_0 + 1:
	case ACUAECbase::SET_EL_SERVO_COEFF_0 + 2:
	case ACUAECbase::SET_EL_SERVO_COEFF_0 + 3:
	case ACUAECbase::SET_EL_SERVO_COEFF_0 + 4:
	case ACUAECbase::SET_EL_SERVO_COEFF_0 + 5:
	case ACUAECbase::SET_EL_SERVO_COEFF_0 + 6:
	case ACUAECbase::SET_EL_SERVO_COEFF_0 + 7:
	case ACUAECbase::SET_EL_SERVO_COEFF_0 + 8:
	case ACUAECbase::SET_EL_SERVO_COEFF_0 + 9:
	case ACUAECbase::SET_EL_SERVO_COEFF_0 + 10:
	case ACUAECbase::SET_EL_SERVO_COEFF_0 + 11:
	case ACUAECbase::SET_EL_SERVO_COEFF_0 + 12:
	    {
	    std::vector<CAN::byte_t> temp(data);
            sim_m.set_el_servo_coeff(
		rca - ACUAECbase::SET_EL_SERVO_COEFF_0,dataToDouble(temp),
		errcode,timestamp);
	    }
	    break;

	case ACUAECbase::SET_AZ_SERVO_DEFAULT:
	    sim_m.set_az_servo_default(errcode,timestamp);
	    break;

	case ACUAECbase::SET_EL_SERVO_DEFAULT:
	    sim_m.set_el_servo_default(errcode,timestamp);
	    break;

	case ACUAECbase::SET_AZ_MOTION_LIM:
	    {
            if (data.size() != 4)
		{
                throw ACUError("SET_AZ_MOTION_LIM data length must be 4");
		}
            std::vector<CAN::byte_t> temp(data);
            long limits = dataToLong(temp);
            sim_m.set_az_motion_lim(limits,errcode,timestamp);
	    }
	    break;

	case ACUAECbase::SET_EL_MOTION_LIM:
	    {
            if (data.size() != 4)
		{
                throw ACUError("SET_EL_MOTION_LIM data length must be 4");
		}
            std::vector<CAN::byte_t> temp(data);
            long limits = dataToLong(temp);
            sim_m.set_el_motion_lim(limits,errcode,timestamp);
	    }
	    break;
	 	  
	case ACUAECbase::INIT_AZ_ENC_ABS_POS:
	    {
            if (data.size() != 1)
		{
                throw ACUError("INIT_AZ_ENC_ABS_POS data length must be 1");
		}
            unsigned char mode = data[0];
            sim_m.init_az_enc_abs_pos(mode,errcode,timestamp);
	    }
            break;
		 	  
	case ACUAECbase::INIT_EL_ENC_ABS_POS:
	    {
            if (data.size() != 1)
		{
                throw ACUError("INIT_EL_ENC_ABS_POS data length must be 1");
		}
            unsigned char mode = data[0];
            sim_m.init_el_enc_abs_pos(mode,errcode,timestamp);
	    }
            break;
	
	case ACUAECbase::SET_IP_ADDRESS:
	    {
            if (data.size() != 4)
		{
                throw ACUError("SET_IP_ADDRESS data length must be 4");
		}
            std::vector<CAN::byte_t> temp(data);
            long address = dataToLong(temp);
            sim_m.set_ip_address(address,errcode,timestamp);
	    }
	    break;

	case ACUAECbase::SET_PT_MODEL_COEFF_0:
	case ACUAECbase::SET_PT_MODEL_COEFF_0 + 1:
	case ACUAECbase::SET_PT_MODEL_COEFF_0 + 2:
	case ACUAECbase::SET_PT_MODEL_COEFF_0 + 3:
	case ACUAECbase::SET_PT_MODEL_COEFF_0 + 4:
	case ACUAECbase::SET_PT_MODEL_COEFF_0 + 5:
	case ACUAECbase::SET_PT_MODEL_COEFF_0 + 6:
	case ACUAECbase::SET_PT_MODEL_COEFF_0 + 7:
	case ACUAECbase::SET_PT_MODEL_COEFF_0 + 8:
	case ACUAECbase::SET_PT_MODEL_COEFF_0 + 9:
	    {
	    std::vector<CAN::byte_t> temp(data);
            sim_m.set_pt_model_coeff(
		rca - ACUAECbase::SET_PT_MODEL_COEFF_0,dataToDouble(temp),
		errcode,timestamp);
	    }
	    break;
	
	case ACUAECbase::SET_SUBREF_ABS_POSN:
	    {
            if (data.size() != 6)
		{
                throw ACUError("SET_SUBREF_ABS_POSN data length must be 8");
		}
            std::vector<CAN::byte_t> temp(data);
            long long position = dataToLonglong(temp,6);
            sim_m.set_subref_abs_posn(position,errcode,timestamp);
	    }
	    break;
	
	case ACUAECbase::SET_SUBREF_DELTA_POSN:
	    {
            if (data.size() != 6)
		{
                throw ACUError("SET_SUBREF_DELTA_POSN data length must be 8");
		}
            std::vector<CAN::byte_t> temp(data);
            long long position = dataToLonglong(temp,6);
            sim_m.set_subref_delta_posn(position,errcode,timestamp);
	    }
	    break;
		 	  
	case ACUAECbase::SUBREF_DELTA_ZERO_CMD:
	    {
            if (data.size() != 1)
		{
                throw ACUError("SUBREF_DELTA_ZERO_CMD data length must be 1");
		}
            unsigned char mode = data[0];
            sim_m.subref_delta_zero_cmd(mode,errcode,timestamp);
	    }
            break;
			 	  
	case ACUAECbase::SET_METR_MODE:
	    {
            if (data.size() != 1)
		{
                throw ACUError("SET_METR_MODE data length must be 1");
		}
            unsigned char mode = data[0];
            sim_m.set_metr_mode(mode,errcode,timestamp);
	    }
            break;
	
	case ACUAECbase::SET_AIR_CONDITIONING:
	    {
            if (data.size() != 1)
		{
                throw ACUError("SET_AIR_CONDITIONING data length must be 1");
		}
	    }
	    // TBD add proper command handling
	    break;
	default:
	    throw CAN::Error("rca NOT Implemented");
	}
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::ACUAEC::get_can_error() const
{
    int errcode;
    unsigned long long timestamp;
    unsigned long level = sim_m.get_can_error(errcode,timestamp);
    const std::vector<CAN::byte_t> nostring;
    return longToData(nostring,level);
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::ACUAEC::get_protocol_rev_level() const
{
    int errcode;
    unsigned long long timestamp;
    std::vector<char> level = sim_m.get_sw_rev_level(errcode,timestamp);
    const std::vector<CAN::byte_t> nostring;
    std::vector<CAN::byte_t> tmps = charToData(nostring,level[0]);
    tmps = charToData(tmps,level[1]);
    return charToData(tmps,level[2]);
}

// -----------------------------------------------------------------------------

std::vector<CAN::byte_t> AMB::ACUAEC::get_sw_rev_level() const
{
    int errcode;
    unsigned long long timestamp;
    std::vector<char> level = sim_m.get_system_id(errcode,timestamp);
    const std::vector<CAN::byte_t> nostring;
    std::vector<CAN::byte_t> tmps = charToData(nostring,level[0]);
    tmps = charToData(tmps,level[1]);
    tmps = charToData(tmps,level[2]);
    tmps = charToData(tmps,level[3]);
    return tmps;
}
	
// -----------------------------------------------------------------------------

unsigned int AMB::ACUAEC::get_trans_num() const
{
    // sim_m.get_num_trans(errcode,timestamp); is NOT used
    return trans_num_m;
}

std::vector<CAN::byte_t> AMB::ACUAEC::tempData(const int firstTemp) const {
  std::vector<CAN::byte_t> retVal(0);
  for (int i = firstTemp; i < firstTemp + 4; i++) {
    const short int data = static_cast<const short int>(temp_m[i]*10.0);
    retVal = shortToData(retVal, data);
  }
  return retVal;
}

std::vector<CAN::byte_t> AMB::ACUAEC::tiltData(const int whichOne) const {
  std::vector<CAN::byte_t> retVal(0);
  short int data = static_cast<short int>(tilt_m[whichOne].temp/.1 + 0.5);
  retVal = shortToData(retVal, data);
  data = static_cast<short int>(tilt_m[whichOne].y/.01 + 0.5);
  retVal = shortToData(retVal, data);
  data = static_cast<short int>(tilt_m[whichOne].x/.01 + 0.5);
  return shortToData(retVal, data);
}
