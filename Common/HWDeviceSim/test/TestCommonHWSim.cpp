/*******************************************************************************
*    ALMA - Atacama Large Millimiter Array
*    (c) European Southern Observatory, 2002
*    Copyright by ESO (in the framework of the ALMA collaboration)
*    and Cosylab 2002, All rights reserved
*
*    This library is free software; you can redistribute it and/or
*    modify it under the terms of the GNU Lesser General Public
*    License as published by the Free Software Foundation; either
*    version 2.1 of the License, or (at your option) any later version.
*
*    This library is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    Lesser General Public License for more details.
*
*    You should have received a copy of the GNU Lesser General Public
*    License along with this library; if not, write to the Free Software
*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*/

#include <cppunit/extensions/HelperMacros.h>
#include "CommonHWSim.h"

/*
 * A test case for the CommonHWSim class
 *
 */
class CommonHWSimTestCase : public CPPUNIT_NS::TestFixture
{
	CPPUNIT_TEST_SUITE( CommonHWSimTestCase );
	CPPUNIT_TEST( test_simulation );
	CPPUNIT_TEST_SUITE_END();

  public:
	void setUp();
	void tearDown();

  protected:
	void test_simulation();
	std::vector<CAN::byte_t> createVector(int size);
	
	AMB::CommonHWSim* sim_m;
};

CPPUNIT_TEST_SUITE_REGISTRATION( CommonHWSimTestCase );

void CommonHWSimTestCase::setUp()
{
	sim_m = new AMB::CommonHWSim();
}

void CommonHWSimTestCase::tearDown()
{
	delete sim_m;
}


void CommonHWSimTestCase::test_simulation()
{
	std::vector<CAN::byte_t> received;
	int i, size;

	/// Initial createVector test
	CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(8), createVector(8).size());

	/// Testing monitor points
	///  GET_SERIAL_NUMBER
	CPPUNIT_ASSERT_EQUAL(static_cast<AMB::rca_t>(0x00000000), AMB::CommonHWSim::GET_SERIAL_NUMBER);
	size = 8;
	CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::CommonHWSim::GET_SERIAL_NUMBER, createVector(size)));
	CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::CommonHWSim::GET_SERIAL_NUMBER));
	received.clear();
	received = sim_m->monitor(AMB::CommonHWSim::GET_SERIAL_NUMBER);
	CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
	for (i=0; i<size; i++)
		CPPUNIT_ASSERT_EQUAL(static_cast<CAN::byte_t>(i+1), received[i]);
	
	///  GET_PROTOCOL_REV_LEVEL
	CPPUNIT_ASSERT_EQUAL(static_cast<AMB::rca_t>(0x00030000), AMB::CommonHWSim::GET_PROTOCOL_REV_LEVEL);
	size = 3;
	CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::CommonHWSim::GET_PROTOCOL_REV_LEVEL, createVector(size)));
	CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::CommonHWSim::GET_PROTOCOL_REV_LEVEL));
	received.clear();
	received = sim_m->monitor(AMB::CommonHWSim::GET_PROTOCOL_REV_LEVEL);
	CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
	for (i=0; i<size; i++)
		CPPUNIT_ASSERT_EQUAL(static_cast<CAN::byte_t>(i+1), received[i]);
	
	///  GET_CAN_ERROR
	CPPUNIT_ASSERT_EQUAL(static_cast<AMB::rca_t>(0x00030001), AMB::CommonHWSim::GET_CAN_ERROR);
	size = 4;
	CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::CommonHWSim::GET_CAN_ERROR, createVector(size)));
	CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::CommonHWSim::GET_CAN_ERROR));
	received.clear();
	received = sim_m->monitor(AMB::CommonHWSim::GET_CAN_ERROR);
	CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
	for (i=0; i<size; i++)
		CPPUNIT_ASSERT_EQUAL(static_cast<CAN::byte_t>(i+1), received[i]);
	
	///  GET_TRANS_NUM
	CPPUNIT_ASSERT_EQUAL(static_cast<AMB::rca_t>(0x00030002), AMB::CommonHWSim::GET_TRANS_NUM);
	size = 4;
	CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::CommonHWSim::GET_TRANS_NUM, createVector(size)));
	CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::CommonHWSim::GET_TRANS_NUM));
	received.clear();
	received = sim_m->monitor(AMB::CommonHWSim::GET_TRANS_NUM);
	CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
	for (i=0; i<size; i++)
		CPPUNIT_ASSERT_EQUAL(static_cast<CAN::byte_t>(i+1), received[i]);

	///  GET_AMBIENT_TEMPERATURE
	CPPUNIT_ASSERT_EQUAL(static_cast<AMB::rca_t>(0x00030003), AMB::CommonHWSim::GET_AMBIENT_TEMPERATURE);
	size = 4;
	CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::CommonHWSim::GET_AMBIENT_TEMPERATURE, createVector(size)));
	CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::CommonHWSim::GET_AMBIENT_TEMPERATURE));
	received.clear();
	received = sim_m->monitor(AMB::CommonHWSim::GET_AMBIENT_TEMPERATURE);
	CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
	for (i=0; i<size; i++)
		CPPUNIT_ASSERT_EQUAL(static_cast<CAN::byte_t>(i+1), received[i]);
	
	///  GET_SW_REV_LEVEL
	CPPUNIT_ASSERT_EQUAL(static_cast<AMB::rca_t>(0x00030004), AMB::CommonHWSim::GET_SW_REV_LEVEL);
	size = 3;
	CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::CommonHWSim::GET_SW_REV_LEVEL, createVector(size)));
	CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::CommonHWSim::GET_SW_REV_LEVEL));
	received.clear();
	received = sim_m->monitor(AMB::CommonHWSim::GET_SW_REV_LEVEL);
	CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
	for (i=0; i<size; i++)
		CPPUNIT_ASSERT_EQUAL(static_cast<CAN::byte_t>(i+1), received[i]);
	
	///  GET_HW_REV_LEVEL
	CPPUNIT_ASSERT_EQUAL(static_cast<AMB::rca_t>(0x00030005), AMB::CommonHWSim::GET_HW_REV_LEVEL);
	size = 3;
	CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::CommonHWSim::GET_HW_REV_LEVEL, createVector(size)));
	CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::CommonHWSim::GET_HW_REV_LEVEL));
	received.clear();
	received = sim_m->monitor(AMB::CommonHWSim::GET_HW_REV_LEVEL);
	CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
	for (i=0; i<size; i++)
		CPPUNIT_ASSERT_EQUAL(static_cast<CAN::byte_t>(i+1), received[i]);

	/// Testing control points
	///  RESET_AMBSI
	CPPUNIT_ASSERT_EQUAL(static_cast<AMB::rca_t>(0x00031000), AMB::CommonHWSim::RESET_AMBSI);
	CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::CommonHWSim::RESET_AMBSI, createVector(1)));
	CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::CommonHWSim::RESET_AMBSI));
	received.clear();
	received = sim_m->monitor(AMB::CommonHWSim::RESET_AMBSI);
	CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(1), received.size());
	CPPUNIT_ASSERT_EQUAL(static_cast<CAN::byte_t>(1), received[0]);
	
	///  RESET_DEVICE
	CPPUNIT_ASSERT_EQUAL(static_cast<AMB::rca_t>(0x00031001), AMB::CommonHWSim::RESET_DEVICE);
	CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::CommonHWSim::RESET_DEVICE, createVector(1)));
	CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::CommonHWSim::RESET_DEVICE));
	received.clear();
	received = sim_m->monitor(AMB::CommonHWSim::RESET_DEVICE);
	CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(1), received.size());
	CPPUNIT_ASSERT_EQUAL(static_cast<CAN::byte_t>(1), received[0]);

	/// Testing error cases
	CPPUNIT_ASSERT_THROW(sim_m->control(static_cast<AMB::rca_t>(0x00032000), createVector(8)), CAN::Error);
	CPPUNIT_ASSERT_THROW(sim_m->control(static_cast<AMB::rca_t>(0x00031002), createVector(8)), CAN::Error);
	CPPUNIT_ASSERT_THROW(sim_m->control(static_cast<AMB::rca_t>(0x00030006), createVector(8)), CAN::Error);
	
	CPPUNIT_ASSERT_THROW(sim_m->control(static_cast<AMB::rca_t>(0x00000000), createVector(4)), CAN::Error);
	CPPUNIT_ASSERT_THROW(sim_m->control(static_cast<AMB::rca_t>(0x00030005), createVector(1)), CAN::Error);
	CPPUNIT_ASSERT_THROW(sim_m->control(static_cast<AMB::rca_t>(0x00030005), createVector(4)), CAN::Error);
	CPPUNIT_ASSERT_THROW(sim_m->control(static_cast<AMB::rca_t>(0x00031000), createVector(3)), CAN::Error);
	
	CPPUNIT_ASSERT_THROW(sim_m->monitor(static_cast<AMB::rca_t>(0x00031002)), CAN::Error);
	CPPUNIT_ASSERT_THROW(sim_m->monitor(static_cast<AMB::rca_t>(0x00030006)), CAN::Error);
}

std::vector<CAN::byte_t> CommonHWSimTestCase::createVector(int size)
{
	std::vector<CAN::byte_t> data;
	int count;

	for (count=1; count<=size; count++)
		data.push_back(static_cast<CAN::byte_t>(count));

	return data;
}


/*
 * Main function running the tests
 */
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>

int main( int argc, char* argv[] )
{
	// Create the event manager and test controller
	CPPUNIT_NS::TestResult controller;
	
	// Add a listener that colllects test result
	CPPUNIT_NS::TestResultCollector result;
	controller.addListener( &result );
	
	// Add a listener that print dots as test run.
	CPPUNIT_NS::BriefTestProgressListener progress;
	controller.addListener( &progress );
	
	// Add the top suite to the test runner
	CPPUNIT_NS::TestRunner runner;
	runner.addTest( CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest() );
	runner.run( controller );
	
	// Print test in a compiler compatible format.
	CPPUNIT_NS::CompilerOutputter outputter( &result, std::cerr );
	outputter.write();
	
	return result.wasSuccessful() ? 0 : 1;
}

