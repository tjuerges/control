/*******************************************************************************
*    ALMA - Atacama Large Millimiter Array
*    (c) European Southern Observatory, 2002
*    Copyright by ESO (in the framework of the ALMA collaboration)
*    and Cosylab 2002, All rights reserved
*
*    This library is free software; you can redistribute it and/or
*    modify it under the terms of the GNU Lesser General Public
*    License as published by the Free Software Foundation; either
*    version 2.1 of the License, or (at your option) any later version.
*
*    This library is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    Lesser General Public License for more details.
*
*    You should have received a copy of the GNU Lesser General Public
*    License along with this library; if not, write to the Free Software
*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*/

#include <cppunit/extensions/HelperMacros.h>
#include "TypeConversion.h"

/*
 * A test case for the TypeConversion class
 *
 */
class TypeConversionTestCase : public CPPUNIT_NS::TestFixture
{
	CPPUNIT_TEST_SUITE( TypeConversionTestCase );
	CPPUNIT_TEST( test_simulation );
	CPPUNIT_TEST_SUITE_END();

  protected:
	void test_simulation();
	std::vector<CAN::byte_t> createVector(int size);
};

CPPUNIT_TEST_SUITE_REGISTRATION( TypeConversionTestCase );


void TypeConversionTestCase::test_simulation()
{
	/* Testing allowed Types */
	std::vector<CAN::byte_t> data;

	/// bit
	char new_value_bit = 0, value_bit = 1;
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, value_bit));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, new_value_bit));
	CPPUNIT_ASSERT_EQUAL(value_bit, new_value_bit);
	
	std::vector<char> vvalue_bit(8/sizeof(char),1);
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, vvalue_bit));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, vvalue_bit));
	
	vvalue_bit.push_back(1);
	CPPUNIT_ASSERT_THROW(AMB::TypeConversion::valueToData(data,vvalue_bit), CAN::Error);

	/// unsigned byte
	unsigned char new_value_ubyte = 0, value_ubyte = 1;
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, value_ubyte));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, new_value_ubyte));
	CPPUNIT_ASSERT_EQUAL(value_ubyte, new_value_ubyte);
	
	std::vector<unsigned char> vvalue_ubyte(8/sizeof(unsigned char),1);
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, vvalue_ubyte));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, vvalue_ubyte));
	
	vvalue_ubyte.push_back(1);
	CPPUNIT_ASSERT_THROW(AMB::TypeConversion::valueToData(data,vvalue_ubyte), CAN::Error);

	/// short
	short new_value_short = 0, value_short = 1;
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, value_short));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, new_value_short));
	CPPUNIT_ASSERT_EQUAL(value_short, new_value_short);
	
	std::vector<short> vvalue_short(8/sizeof(short),1);
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, vvalue_short));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, vvalue_short));
	
	vvalue_short.push_back(1);
	CPPUNIT_ASSERT_THROW(AMB::TypeConversion::valueToData(data,vvalue_short), CAN::Error);

	/// unsigned short
	unsigned short new_value_ushort = 0, value_ushort = 1;
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, value_ushort));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, new_value_ushort));
	CPPUNIT_ASSERT_EQUAL(value_ushort, new_value_ushort);
	
	std::vector<unsigned short> vvalue_ushort(8/sizeof(unsigned short),1);
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, vvalue_ushort));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, vvalue_ushort));
	
	vvalue_ushort.push_back(1);
	CPPUNIT_ASSERT_THROW(AMB::TypeConversion::valueToData(data,vvalue_ushort), CAN::Error);

	/// int
	int new_value_int = 0, value_int = 1;
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, value_int));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, new_value_int));
	CPPUNIT_ASSERT_EQUAL(value_int, new_value_int);
	
	std::vector<int> vvalue_int(8/sizeof(int),1);
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, vvalue_int));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, vvalue_int));
	
	vvalue_int.push_back(1);
	CPPUNIT_ASSERT_THROW(AMB::TypeConversion::valueToData(data,vvalue_int), CAN::Error);

	/// unsigned int
	unsigned int new_value_uint = 0, value_uint = 1;
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, value_uint));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, new_value_uint));
	CPPUNIT_ASSERT_EQUAL(value_uint, new_value_uint);
	
	std::vector<unsigned int> vvalue_uint(8/sizeof(unsigned int),1);
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, vvalue_uint));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, vvalue_uint));
	
	vvalue_uint.push_back(1);
	CPPUNIT_ASSERT_THROW(AMB::TypeConversion::valueToData(data,vvalue_uint), CAN::Error);

	/// long
	long new_value_long = 0, value_long = 1;
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, value_long));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, new_value_long));
	CPPUNIT_ASSERT_EQUAL(value_long, new_value_long);
	
	std::vector<long> vvalue_long(8/sizeof(long),1);
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, vvalue_long));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, vvalue_long));
	
	vvalue_long.push_back(1);
	CPPUNIT_ASSERT_THROW(AMB::TypeConversion::valueToData(data,vvalue_long), CAN::Error);

	/// unsigned long
	unsigned long new_value_ulong = 0, value_ulong = 1;
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, value_ulong));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, new_value_ulong));
	CPPUNIT_ASSERT_EQUAL(value_ulong, new_value_ulong);
	
	std::vector<unsigned long> vvalue_ulong(8/sizeof(unsigned long),1);
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, vvalue_ulong));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, vvalue_ulong));
	
	vvalue_ulong.push_back(1);
	CPPUNIT_ASSERT_THROW(AMB::TypeConversion::valueToData(data,vvalue_ulong), CAN::Error);

	/// long long
	long long new_value_llong = 0, value_llong = 1;
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, value_llong));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, new_value_llong));
	CPPUNIT_ASSERT_EQUAL(value_llong, new_value_llong);
	
	std::vector<long long> vvalue_llong(8/sizeof(long long),1);
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, vvalue_llong));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, vvalue_llong));
	
	vvalue_llong.push_back(1);
	CPPUNIT_ASSERT_THROW(AMB::TypeConversion::valueToData(data,vvalue_llong), CAN::Error);

	/// unsigned long long
	unsigned long long new_value_ullong = 0, value_ullong = 1;
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, value_ullong));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, new_value_ullong));
	CPPUNIT_ASSERT_EQUAL(value_ullong, new_value_ullong);
	
	std::vector<unsigned long long> vvalue_ullong(8/sizeof(unsigned long long),1);
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, vvalue_ullong));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, vvalue_ullong));
	
	vvalue_ullong.push_back(1);
	CPPUNIT_ASSERT_THROW(AMB::TypeConversion::valueToData(data,vvalue_ullong), CAN::Error);

	/// float
	float new_value_float = 0, value_float = 1;
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, value_float));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, new_value_float));
	CPPUNIT_ASSERT_EQUAL(value_float, new_value_float);
	
	std::vector<float> vvalue_float(8/sizeof(float),1);
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, vvalue_float));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, vvalue_float));
	
	vvalue_float.push_back(1);
	CPPUNIT_ASSERT_THROW(AMB::TypeConversion::valueToData(data,vvalue_float), CAN::Error);

	/// double
	double new_value_double = 0, value_double = 1;
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, value_double));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, new_value_double));
	CPPUNIT_ASSERT_EQUAL(value_double, new_value_double);
	
	std::vector<double> vvalue_double(8/sizeof(double),1);
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, vvalue_double));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, vvalue_double));
	
	vvalue_double.push_back(1);
	CPPUNIT_ASSERT_THROW(AMB::TypeConversion::valueToData(data,vvalue_double), CAN::Error);

	/// bool
	bool new_value_bool = 0, value_bool = 1;
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, value_bool));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, new_value_bool));
	CPPUNIT_ASSERT_EQUAL(value_bool, new_value_bool);
	
	std::vector<bool> vvalue_bool(8/sizeof(bool),1);
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, vvalue_bool));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, vvalue_bool));
	
	vvalue_bool.push_back(1);
	CPPUNIT_ASSERT_THROW(AMB::TypeConversion::valueToData(data,vvalue_bool), CAN::Error);

	/// ACS::Time
	ACS::Time new_value_acstime = 0, value_acstime = 1;
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, value_acstime));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, new_value_acstime));
	CPPUNIT_ASSERT_EQUAL(value_acstime, new_value_acstime);
	
	std::vector<ACS::Time> vvalue_acstime(8/sizeof(ACS::Time),1);
	data.clear();
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::valueToData(data, vvalue_acstime));
	CPPUNIT_ASSERT_NO_THROW(AMB::TypeConversion::dataToValue(data, vvalue_acstime));
	
	vvalue_acstime.push_back(1);
	CPPUNIT_ASSERT_THROW(AMB::TypeConversion::valueToData(data,vvalue_acstime), CAN::Error);
}


/*
 * Main function running the tests
 */
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>

int main( int argc, char* argv[] )
{
	// Create the event manager and test controller
	CPPUNIT_NS::TestResult controller;
	
	// Add a listener that colllects test result
	CPPUNIT_NS::TestResultCollector result;
	controller.addListener( &result );
	
	// Add a listener that print dots as test run.
	CPPUNIT_NS::BriefTestProgressListener progress;
	controller.addListener( &progress );
	
	// Add the top suite to the test runner
	CPPUNIT_NS::TestRunner runner;
	runner.addTest( CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest() );
	runner.run( controller );
	
	// Print test in a compiler compatible format.
	CPPUNIT_NS::CompilerOutputter outputter( &result, std::cerr );
	outputter.write();
	
	return result.wasSuccessful() ? 0 : 1;
}

