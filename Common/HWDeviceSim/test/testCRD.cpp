// @(#) $Id$
//
// Copyright (C) 2002
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

/** This tests the function calls of the CRD
 ** classes which represent the CRD Central Reference Distributor device
 ** simulator.
 ** This test displays 'Tests Successful' if all tests pass, otherwise
 ** error messages appear ending with 'Tests Failed'.
 */

#include <CRD.h>

#include <AMBUtil.h>
#include <iostream>
#include <string>

using namespace std;
// Forward declaration for checking value for CRD object
bool doCRDTests();

template <class T>
 bool checkExpectedCRDValue(const vector<CAN::byte_t>& monitorBytes, 
 			    T expectedValue,
 			    unsigned int size, 
 			    const string& pMonitorName);
void printCANMsg(const vector<CAN::byte_t>& CANMsg, const string& pMsg);

bool gTestsSuccessful = true;

int main() {
  // Construct 2 CAN nodes
  if( !doCRDTests( ) )
    gTestsSuccessful = false;
  
  if( gTestsSuccessful )
    cout << "Tests Successful" << endl;
  else
    cout << "Tests Failed" << endl;
}

//-----------------------------------------------------------------------------
bool doCRDTests() {
  bool bTestsSuccessful = true;
  AMB::node_t CRDNode = 0x22;

  vector<CAN::byte_t> CRDSn(8);

  // CRD S/N looks like 10660000
  CRDSn[0] = 0x01;
  CRDSn[1] = 0x02;
  CRDSn[2] = 0x04;
  CRDSn[3] = 0x08;
  CRDSn[4] = 0x10;
  CRDSn[5] = 0x20;
  CRDSn[6] = 0x40;
  CRDSn[7] = 0x80;
  
  AMB::CRD CRD(CRDNode, CRDSn );
  vector<CAN::byte_t> monitorBytes;
  
  // To check the # of CAN transactions monitor points, keep track of the
  // # of times we call monitor & control points
  int expectedNumberCANTransactions = 0;
  
  vector<CAN::byte_t> sn = CRD.serialNumber();
  for( int i = 0; i < 8; i++ ) {
    if( sn[i] != CRDSn[i] ) {
      bTestsSuccessful = false;
      cerr << "CRD: S/N check failed on digit " << i << endl;
    }
  }
  if (gTestsSuccessful)
      cout << "CRD: S/N check " << "tests were successful. " << endl;

  if( CRDNode != CRD.node() ) {
    bTestsSuccessful = false;
    cerr << "CRD: node check failed: " << CRD.node() << " should be: "
 	 << CRDNode << endl;
  }
  if (gTestsSuccessful)
      cout << "CRD: mode check " << "tests were successful. " << endl;

  // Monitor points specific to CRD
  monitorBytes = CRD.monitor(AMB::CRD::GET_RESET_TIME);
  checkExpectedCRDValue(monitorBytes, CRD.getRESET_TIME(), 8, "GET_RESET_TIME");
  expectedNumberCANTransactions++;

  monitorBytes = CRD.monitor(AMB::CRD::MASER_VS_GPS_COUNTER);
  checkExpectedCRDValue(monitorBytes, CRD.getMASER_VS_GPS_COUNTER(), 8, "MASER_VS_GPS_COUNTER");
  expectedNumberCANTransactions++;

  monitorBytes = CRD.monitor(AMB::CRD::MASER_COUNTER);
  checkExpectedCRDValue(monitorBytes, CRD.getMASER_COUNTER(), 8, "MASER_COUNTER");
  expectedNumberCANTransactions++;

  monitorBytes = CRD.monitor(AMB::CRD::EFC_5_MHZ);
  checkExpectedCRDValue(monitorBytes, CRD.getEFC_5_MHZ(), 2, "EFC_5_MHZ");
  expectedNumberCANTransactions++;

  monitorBytes = CRD.monitor(AMB::CRD::PWR_5_MHZ);
  checkExpectedCRDValue(monitorBytes, CRD.getPWR_5_MHZ(), 2, "PWR_5_MHZ");
  expectedNumberCANTransactions++;
  
  monitorBytes = CRD.monitor(AMB::CRD::PWR_25_MHZ);
  checkExpectedCRDValue(monitorBytes, CRD.getPWR_25_MHZ(), 2, "PWR_25_MHZ");
  expectedNumberCANTransactions++;

  monitorBytes = CRD.monitor(AMB::CRD::PWR_125_MHZ);
  checkExpectedCRDValue(monitorBytes, CRD.getPWR_125_MHZ(), 2, 
			 "PWR_125_MHZ");
  expectedNumberCANTransactions++;

  monitorBytes = CRD.monitor(AMB::CRD::PWR_2_GHZ);
  checkExpectedCRDValue(monitorBytes, CRD.getPWR_2_GHZ(), 2, "PWR_2_GHZ");
  expectedNumberCANTransactions++;

  monitorBytes = CRD.monitor(AMB::CRD::LASER_CURRENT);
  checkExpectedCRDValue(monitorBytes, CRD.getLASER_CURRENT(), 2, "LASER_CURRENT");
  expectedNumberCANTransactions++;
  
  monitorBytes = CRD.monitor(AMB::CRD::VDC_MINUS_5);
  checkExpectedCRDValue(monitorBytes, CRD.getVDC_MINUS_5(), 2, "VDC_MINUS_5");
  expectedNumberCANTransactions++;
  
  monitorBytes = CRD.monitor(AMB::CRD::VDC_15);
  checkExpectedCRDValue(monitorBytes, CRD.getVDC_15(), 2, "VDC_15");
  expectedNumberCANTransactions++;

  monitorBytes = CRD.monitor(AMB::CRD::STATUS);
  checkExpectedCRDValue(monitorBytes, CRD.getSTATUS(), 1, "STATUS");
  expectedNumberCANTransactions++;
  
  // Do control functions
  vector<CAN::byte_t> data1(1), data2(2);
  data1[0] = 0;
  CRD.control(AMB::CRD::XILINX_PROM_RESET, data1);
  expectedNumberCANTransactions++;
  // Now check that XILINX_Reset works
  
  // Xilinx status should be 0 after successful reset/software load
  monitorBytes = CRD.monitor( AMB::CRD::STATUS);
  expectedNumberCANTransactions++;
  if ( (monitorBytes[0] && 0xfe) != 0 ) {
  cerr << "Didn't set reset Xilinx status correctly: " << (int)monitorBytes[0] <<
      " should be 0\n";
  bTestsSuccessful = false;
  }
  if (gTestsSuccessful)
      cout << "XILINX_PROM_RESET " << "tests were successful. " << endl;

  data1[0] = 1;
  CRD.control(AMB::CRD::MASTER_COUNTER_RESET, data1);
  expectedNumberCANTransactions++;
  // Now check that MASTER_COUNTER_Reset works

  // RESET_TIME should be zero after reset
  monitorBytes = CRD.monitor(AMB::CRD::MASER_COUNTER);
  expectedNumberCANTransactions++;
  {
    const unsigned long long maserCount = AMB::dataToLonglong (monitorBytes, 8);
    if ( maserCount > 50 ) {
      cerr << "Didn't set reset master counter correctly: " << maserCount <<
	  " should be 0\n";
      bTestsSuccessful = false;
    }
    if (gTestsSuccessful)
	cout << "MASTER_COUNTER_RESET " << "tests were successful. " << endl;
  } 

  // Check basic CAN functions
  monitorBytes = CRD.monitor(AMB::CRD::GET_CAN_ERROR );
  expectedNumberCANTransactions++;
  {
    const short shortVal= AMB::dataToShort(monitorBytes);
    if (shortVal != 0) {
      cerr << "CAN Error msg: " << shortVal << " expected 0" << endl;
      bTestsSuccessful = false;
    }
    if (gTestsSuccessful)
	cout << "GET_CAN_ERROR " << "tests were successful. " << endl;
  }
      
  monitorBytes = CRD.monitor( AMB::CRD::GET_PROTOCOL_REV_LEVEL );
  expectedNumberCANTransactions++;
  {
    const short shortVal= AMB::dataToShort(monitorBytes);
    if (shortVal != 0 ) {
      cerr << "Protocol Rev. Level Error: " << shortVal 
	   << " expected 0" << endl;
      bTestsSuccessful = false;
    }
    if (gTestsSuccessful)
	cout << "GET_PROTOCOL_REV_LEVEL " << "tests were successful. " << endl;
  }
  
  monitorBytes = CRD.monitor( AMB::CRD::GET_AMBIENT_TEMPERATURE );
  expectedNumberCANTransactions++;
  {
    const short shortVal= AMB::dataToShort(monitorBytes);
    if ( shortVal != 0x2C ) {
      cerr << "Ambient Temperature error : " << shortVal 
	   << " expected 0x2C" << endl;
      bTestsSuccessful = false;
    }
    if (gTestsSuccessful)
	cout << "GET_AMBIENT_TEMPERATURE " << "tests were successful. " << endl;
  }
  
  monitorBytes = CRD.monitor( AMB::CRD::GET_TRANS_NUM );
  expectedNumberCANTransactions++;
  {
    const long longVal = AMB::dataToLong(monitorBytes);
    if (longVal != expectedNumberCANTransactions ) {
      cerr << "Number of CAN Transactions Error: " << longVal << " expected " 
	   << expectedNumberCANTransactions << endl;
      bTestsSuccessful = false;
    }
    if (gTestsSuccessful)
	cout << "GET_TRANS_NUM " << "tests were successful. " << endl;
    return bTestsSuccessful;
  }
}

//-----------------------------------------------------------------------------
template <class T> 
bool checkExpectedCRDValue( const vector<CAN::byte_t>& monitorBytes,
			     T expectedValue, unsigned int size,
			     const string& pMonitorName) {
  if (monitorBytes.size() != size ) {
    cerr << "Incorrect CRD size: " 
	      << monitorBytes.size() << " expected " << size
	      << " for " << pMonitorName << endl;
    gTestsSuccessful = false;
    return gTestsSuccessful;
  }

  vector<CAN::byte_t> tmp(monitorBytes);
  short actualValue = 0;
  unsigned long long actualLonglongValue = 0;
  if( size == 1 ) {
    actualValue = static_cast<short>(monitorBytes[0]);
  } else if ( size == 2 ) {
    actualValue = AMB::dataToShort( tmp );
  } else if( size == 8 ) {
    actualLonglongValue = AMB::dataToLonglong (tmp, 8);
  }

// Commented out for now! FIX THIS! Once a better philosophy for handing unit
// tests of simulators is established.

  if ( size <= 2) {
    if (actualValue != (short)expectedValue ) {
      cerr << "CRD value: " << actualValue 
	   << " != expected value: " << expectedValue 
	   << " for " << pMonitorName << endl;
      gTestsSuccessful = false;
    }
  }
  else if ( size == 8 ) {
  if (actualLonglongValue != (unsigned long long)expectedValue)
      {
      if (actualLonglongValue < ((unsigned long long)expectedValue - 1000L))
	  {
	  cerr << "CRD value: " << actualLonglongValue 
	       << " != expected value: " << expectedValue 
	       << " for " << pMonitorName << endl;
	  gTestsSuccessful = false;
	  }
    }
  }

  if (gTestsSuccessful)
      cout << pMonitorName << " tests were successful. " << endl;
  
  return gTestsSuccessful;
}

//-----------------------------------------------------------------------------
void printCANMsg(const vector<CAN::byte_t> &CANMsg, const string& pMsg ) {
  cout << pMsg << ' ';
  for (int i = 0; i < 8; i++ ) {
    cout << static_cast<int>(CANMsg[i]) << ' ';
  }
  cout << endl;
}

