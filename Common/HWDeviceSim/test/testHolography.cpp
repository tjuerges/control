// @(#) $Id$
//
// Copyright (C) 2002
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

/** This tests the function calls of the HolographyReceiver & HolographyDSP
 ** classes which represent the Holography Measurement Equipment device
 ** simulator.
 ** This test displays 'Tests Successful' if all tests pass, otherwise
 ** error messages appear ending with 'Tests Failed'.
 */

#include <HolographyDSP.h>
#include <HolographyReceiver.h>

#include <AMBUtil.h>
#include <iostream>

using namespace std;

// Forward declaration for checking value for HolographyDSP object
bool doHolographyDSPTests();
bool doHolographyReceiverTests();
void checkExpectedDataPointValue( const vector<CAN::byte_t> &monitorBytes,
				  long long &expectedValue, unsigned int size);
template <class T>
void checkExpectedReceiverValue( const vector<CAN::byte_t> &monitorBytes, T expectedValue,
				 unsigned int size, const char *pMonitorName = "");
void printCANMsg( const vector<CAN::byte_t> &CANMsg, const char *pMsg = " ");

bool gTestsSuccessful = true;

int main()
{
    // Construct 2 CAN nodes
    if( !doHolographyDSPTests( ) )
	gTestsSuccessful = false;

    if( !doHolographyReceiverTests() )
	gTestsSuccessful = false;
    
    if( gTestsSuccessful )
	cout << "Tests Successful" << endl;
    else
        cout << "Tests Failed" << endl;
}

//------------------------------------------------------------------------------
/* This tests the HolographyDSP class. It basically tests the get data point 
** monitor points & the reset DSP control point.
*/
bool doHolographyDSPTests()
{
    bool bTestsSuccessful = true;

    AMB::node_t holoDSPNode = 1234;

    vector<CAN::byte_t> holoDSPSn(8);

    // Holo Recevier S/N looks like 6010AD2F
    holoDSPSn[0] = 0x06;
    holoDSPSn[1] = 0x00;
    holoDSPSn[2] = 0x01;
    holoDSPSn[3] = 0x00;
    holoDSPSn[4] = 0x0A;
    holoDSPSn[5] = 0x0D;
    holoDSPSn[6] = 0x02;
    holoDSPSn[7] = 0x0F;

    AMB::HolographyDSP holoDSP( holoDSPNode, holoDSPSn );

    // To check the # of CAN transactions monitor points, keep track of the
    // # of times we call monitor & control points
    int expectedNumberCANTransactions = 0;

    //* Constructor checks
    // Serial # check
    vector<CAN::byte_t> sn = holoDSP.serialNumber();
    for( int i = 0; i < 8; i++ )
    {
	if( sn[i] != holoDSPSn[i] )
	{
	    bTestsSuccessful = false;
	    cout << "HolographyDSP: S/N check failed on digit " << i << endl;
	}
    }
    
    // Node # check
    if( holoDSPNode != holoDSP.node() )
    {
	bTestsSuccessful = false;
	cout << "HolographyDSP: node check failed: " << holoDSP.node() << " should be: "
	     << holoDSPNode << endl;
    }
    
    vector<CAN::byte_t> monitorBytes;
    // Do monitor checks on HolographyReceiver

    // check 100 iterations of data points. We expect the holo. dsp data points
    // to increase from 1 on for each monitor call. If the retrieved values don't
    // match the expected value then we have an error.
    // Retrieve 100 sets of data points
    long long expectedValue = 1;
    for( int i = 0; i < 100; i++ )
    {
	monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_DATA_QQ_0 );
	checkExpectedDataPointValue( monitorBytes, expectedValue, 6);
	monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_DATA_QR_0 );
	checkExpectedDataPointValue( monitorBytes, expectedValue, 6);
	monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_DATA_QS_0 );
	checkExpectedDataPointValue( monitorBytes, expectedValue, 6);
	monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_DATA_RR_0 );
	checkExpectedDataPointValue( monitorBytes, expectedValue, 6);
	monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_DATA_RS_0 );
	checkExpectedDataPointValue( monitorBytes, expectedValue, 6);
	monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_DATA_SS_0 );
	checkExpectedDataPointValue( monitorBytes, expectedValue, 6);

	monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_DATA_QQ_1 );
	checkExpectedDataPointValue( monitorBytes, expectedValue, 6);
	monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_DATA_QR_1 );
	checkExpectedDataPointValue( monitorBytes, expectedValue, 6);
	monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_DATA_QS_1 );
	checkExpectedDataPointValue( monitorBytes, expectedValue, 6);
	monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_DATA_RR_1 );
	checkExpectedDataPointValue( monitorBytes, expectedValue, 6);
	monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_DATA_RS_1 );
	checkExpectedDataPointValue( monitorBytes, expectedValue, 6);
	monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_DATA_SS_1 );
	checkExpectedDataPointValue( monitorBytes, expectedValue, 6);

	monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_DATA_QQ_2 );
	checkExpectedDataPointValue( monitorBytes, expectedValue, 6);
	monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_DATA_QR_2 );
	checkExpectedDataPointValue( monitorBytes, expectedValue, 6);
	monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_DATA_QS_2 );
	checkExpectedDataPointValue( monitorBytes, expectedValue, 6);
	monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_DATA_RR_2 );
	checkExpectedDataPointValue( monitorBytes, expectedValue, 6);
	monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_DATA_RS_2 );
	checkExpectedDataPointValue( monitorBytes, expectedValue, 6);
	monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_DATA_SS_2 );
	checkExpectedDataPointValue( monitorBytes, expectedValue, 6);

	monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_DATA_QQ_3 );
	checkExpectedDataPointValue( monitorBytes, expectedValue, 6);
	monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_DATA_QR_3 );
	checkExpectedDataPointValue( monitorBytes, expectedValue, 6);
	monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_DATA_QS_3 );
	checkExpectedDataPointValue( monitorBytes, expectedValue, 6);
	monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_DATA_RR_3 );
	checkExpectedDataPointValue( monitorBytes, expectedValue, 6);
	monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_DATA_RS_3 );
	checkExpectedDataPointValue( monitorBytes, expectedValue, 6);
	monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_DATA_SS_3 );
	checkExpectedDataPointValue( monitorBytes, expectedValue, 6);

	expectedNumberCANTransactions += 24;
    }
    // Now check that all 4 intervals have the same # of samples
    monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_NUM_SAMPLES );
    expectedNumberCANTransactions++;
    int k = 0;
    for(int i = 0; i < 4; i++ )
    {
	vector<CAN::byte_t> tmp(2);
	for( int j = 0; j < 2; j++ )
	    tmp[j] = monitorBytes[k++];

	short val = AMB::dataToShort(tmp);

	if( val != holoDSP.NUM_SAMPLES_PER_INTERVAL )
	{
	    cout << "Number of samples / interval error: " << val <<
		" expected " << holoDSP.NUM_SAMPLES_PER_INTERVAL << 
		" for interval " << i << endl;
	    bTestsSuccessful = false;
	}
    }

    // Only control point is reset
    vector<CAN::byte_t> data(1);
    try {
	holoDSP.control( AMB::HolographyDSP::RESET_DSP, data);
	expectedNumberCANTransactions++;
    }
    catch ( AMB::HolographyDSP::HolographyDSPError e)
    {
	cout << e << endl;
	bTestsSuccessful = false;
    }
    /// This should generate an error which is caught & no output
    data = vector<CAN::byte_t>(2);
    try {
	expectedNumberCANTransactions++;
	holoDSP.control( AMB::HolographyDSP::RESET_DSP, data);
	bTestsSuccessful = false; // if exception not caught, then we execute this
    }
    catch ( AMB::HolographyDSP::HolographyDSPError e)
    {
    }
    
    // Check basic CAN functions
    monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_CAN_ERROR );
    expectedNumberCANTransactions++;

    short shortVal;
    if( (shortVal = AMB::dataToShort(monitorBytes)) != 0 )
    {
	cout << "CAN Error msg: " << shortVal << " expected 0" << endl;
	bTestsSuccessful = false;
    }
    
    monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_PROTOCOL_REV_LEVEL );
    expectedNumberCANTransactions++;
    if( (shortVal = AMB::dataToShort(monitorBytes)) != 0 )
    {
	cout << "Protocol Rev. Level Error: " << shortVal << " expected 0" << endl;
	bTestsSuccessful = false;
    }

    monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_AMBIENT_TEMPERATURE );
    expectedNumberCANTransactions++;
    if( (shortVal = AMB::dataToShort(monitorBytes)) != 0x2C )
    {
	cout << "Ambient Temperature error : " << shortVal << " expected 0x2C" << endl;
	bTestsSuccessful = false;
    }

    monitorBytes = holoDSP.monitor( AMB::HolographyDSP::GET_TRANS_NUM );
    expectedNumberCANTransactions++;

    long longVal;
    if( (longVal = AMB::dataToLong(monitorBytes)) != expectedNumberCANTransactions )
    {
	cout << "Number of CAN Transactions Error: " << longVal << " expected " 
	     << expectedNumberCANTransactions << endl;
	bTestsSuccessful = false;
    }
    return bTestsSuccessful;
}

//------------------------------------------------------------------------------
bool doHolographyReceiverTests()
{
    bool bTestsSuccessful = true;
    AMB::node_t holoRcvrNode = 1233;

    vector<CAN::byte_t> holoRcvrSn(8);

    // Holo Recevier S/N looks like 60108cer
    holoRcvrSn[0] = 0x06;
    holoRcvrSn[1] = 0x00;
    holoRcvrSn[2] = 0x01;
    holoRcvrSn[3] = 0x00;
    holoRcvrSn[4] = 0x08;
    holoRcvrSn[5] = 0x0C;
    holoRcvrSn[6] = 0x0E;
    holoRcvrSn[7] = 0x02;

    AMB::HolographyReceiver holoRcvr( holoRcvrNode, holoRcvrSn );
    vector<CAN::byte_t> monitorBytes;

    // To check the # of CAN transactions monitor points, keep track of the
    // # of times we call monitor & control points
    int expectedNumberCANTransactions = 0;

    vector<CAN::byte_t> sn = holoRcvr.serialNumber();
    for( int i = 0; i < 8; i++ )
    {
	if( sn[i] != holoRcvrSn[i] )
	{
	    bTestsSuccessful = false;
	    cout << "HolographyReceiver: S/N check failed on digit " << i << endl;
	}
    }
    if( holoRcvrNode != holoRcvr.node() )
    {
	bTestsSuccessful = false;
	cout << "HolographyRcvr: node check failed: " << holoRcvr.node() << " should be: "
	     << holoRcvrNode << endl;
    }

    // Monitor points specific to Holo. Receiver
    monitorBytes = holoRcvr.monitor( AMB::HolographyReceiver::GET_PLL_STATUS);
    checkExpectedReceiverValue(monitorBytes, holoRcvr.getPLL_STATUS(), 1, "PLL Status");
    expectedNumberCANTransactions++;

    monitorBytes = holoRcvr.monitor( AMB::HolographyReceiver::GET_GUNN_H_VOLTAGE);
    checkExpectedReceiverValue(monitorBytes, holoRcvr.getGUNN_H_VOLTAGE(), 4, 
			       "GET_GUNN_H_VOLTAGE");
    expectedNumberCANTransactions++;

    monitorBytes = holoRcvr.monitor( AMB::HolographyReceiver::GET_GUNN_L_VOLTAGE);
    checkExpectedReceiverValue(monitorBytes, holoRcvr.getGUNN_L_VOLTAGE(), 4,
			       "GET_GUNN_L_VOLTAGE");
    expectedNumberCANTransactions++;

    monitorBytes = holoRcvr.monitor( AMB::HolographyReceiver::GET_LO_DET_OUT);
    checkExpectedReceiverValue(monitorBytes, holoRcvr.getLO_DET_OUT(), 4, "GET_LO_DET_OUT");
    expectedNumberCANTransactions++;

    monitorBytes = holoRcvr.monitor( AMB::HolographyReceiver::GET_REF_DET_OUT);
    checkExpectedReceiverValue(monitorBytes, holoRcvr.getREF_DET_OUT(), 4, "GET_REF_DET_OUT");
    expectedNumberCANTransactions++;

    monitorBytes = holoRcvr.monitor( AMB::HolographyReceiver::GET_SIG_DET);
    checkExpectedReceiverValue(monitorBytes, holoRcvr.getSIG_DET(), 4, "GET_SIG_DET");
    expectedNumberCANTransactions++;

    monitorBytes = holoRcvr.monitor( AMB::HolographyReceiver::GET_REF_SENSE_I);
    checkExpectedReceiverValue(monitorBytes, holoRcvr.getREF_SENSE_I(), 4, "GET_REF_SENSE_I");
    expectedNumberCANTransactions++;

    monitorBytes = holoRcvr.monitor( AMB::HolographyReceiver::GET_REF_SENSE_Q);
    checkExpectedReceiverValue(monitorBytes,holoRcvr.getREF_SENSE_Q(), 4,"GN_REF_SENSE_Q");
    expectedNumberCANTransactions++;

    monitorBytes = holoRcvr.monitor( AMB::HolographyReceiver::GET_SIG_SENSE_I);
    checkExpectedReceiverValue(monitorBytes, holoRcvr.getSIG_SENSE_I(), 4, "GET_SIG_SENSE_I");
    expectedNumberCANTransactions++;

    monitorBytes = holoRcvr.monitor( AMB::HolographyReceiver::GET_SIG_SENSE_Q);
    checkExpectedReceiverValue(monitorBytes, holoRcvr.getSIG_SENSE_Q(), 4, "GET_SIG_SENSE_Q");
    expectedNumberCANTransactions++;

    monitorBytes = holoRcvr.monitor( AMB::HolographyReceiver::GET_SUPPLY_CURRENT);
    checkExpectedReceiverValue(monitorBytes, holoRcvr.getSUPPLY_CURRENT(), 4,
			       "GET_SUPPLY_CURRENT");
    expectedNumberCANTransactions++;

    monitorBytes = holoRcvr.monitor( AMB::HolographyReceiver::GET_TEMP_POWER_SUPPLY);
    checkExpectedReceiverValue(monitorBytes, holoRcvr.getTEMP_POWER_SUPPLY(), 4,
			       "GET_TEMP_POWER_SUPPLY");
    expectedNumberCANTransactions++;

    monitorBytes = holoRcvr.monitor( AMB::HolographyReceiver::GET_TEMP_SIG_MIX);
    checkExpectedReceiverValue(monitorBytes,  holoRcvr.getTEMP_SIG_MIX(), 4,"GET_TEMP_SIG_MIX");
    expectedNumberCANTransactions++;

    monitorBytes = holoRcvr.monitor( AMB::HolographyReceiver::GET_TEMP_REF_MIX);
    checkExpectedReceiverValue(monitorBytes, holoRcvr.getTEMP_REF_MIX(), 4, "GET_TEMP_REF_MIX");
    expectedNumberCANTransactions++;

    monitorBytes = holoRcvr.monitor( AMB::HolographyReceiver::GET_TEMP_29MHZ_OCXO);
    checkExpectedReceiverValue(monitorBytes, holoRcvr.getTEMP_29MHZ_OCXO(), 4,
			       "GET_TEMP_29MHZ_OCXO");
    expectedNumberCANTransactions++;

    monitorBytes = holoRcvr.monitor( AMB::HolographyReceiver::GET_TEMP_95MHZ_OCXO);
    checkExpectedReceiverValue(monitorBytes, holoRcvr.getTEMP_95MHZ_OCXO(), 4,
			       "GET_TEMP_95MHZ_OCXO");
    expectedNumberCANTransactions++;

    monitorBytes = holoRcvr.monitor( AMB::HolographyReceiver::GET_TEMP_LOCK_BOX);
    checkExpectedReceiverValue(monitorBytes, holoRcvr.getTEMP_LOCK_BOX(), 4,
			       "GET_TEMP_LOCK_BOX");
    expectedNumberCANTransactions++;

    // Do control functions
    vector<CAN::byte_t> data1(1), data2(2), data4(4);

    holoRcvr.control(AMB::HolographyReceiver::SET_SIG_ATTENUATION, data1);
    expectedNumberCANTransactions++;
    holoRcvr.control(AMB::HolographyReceiver::SET_REF_ATTENUATION, data1);
    expectedNumberCANTransactions++;
    holoRcvr.control(AMB::HolographyReceiver::SET_SYNTH_FREQUENCY, data2);
    expectedNumberCANTransactions++;
    holoRcvr.control(AMB::HolographyReceiver::SET_DRIVE_ENABLE, data1);
    expectedNumberCANTransactions++;
    holoRcvr.control(AMB::HolographyReceiver::SET_LOOP_ENABLE, data1);
    expectedNumberCANTransactions++;
    holoRcvr.control(AMB::HolographyReceiver::SET_NOMINAL_VOLTAGE, data4);
    expectedNumberCANTransactions++;
    holoRcvr.control(AMB::HolographyReceiver::SET_GUNN_LOOP_GAIN, data4);
    expectedNumberCANTransactions++;
    holoRcvr.control(AMB::HolographyReceiver::SET_GUNN_TUNE_RANGE, data4);
    expectedNumberCANTransactions++;
    holoRcvr.control(AMB::HolographyReceiver::SET_GUNN_SELECT, data1);
    expectedNumberCANTransactions++;
    holoRcvr.control(AMB::HolographyReceiver::RESET_PLL_STATUS, data1);
    expectedNumberCANTransactions++;

    // Now check that PLL_LOCKing works 
    data1[0] = 1;
    holoRcvr.control(AMB::HolographyReceiver::SET_DRIVE_ENABLE, data1);
    expectedNumberCANTransactions++;
    holoRcvr.control(AMB::HolographyReceiver::SET_LOOP_ENABLE, data1);
    expectedNumberCANTransactions++;

    // PLL status should be 0x03 to reflect 2 bits set
    monitorBytes = holoRcvr.monitor( AMB::HolographyReceiver::GET_PLL_STATUS);
    expectedNumberCANTransactions++;
    if( monitorBytes[0] != 0x03 )
    {
	cout << "Didn't set PLL status correctly: " << (int)monitorBytes[0] <<
	    " should be 3\n";
	bTestsSuccessful = false;
    }
    
    // Now reset PLL & read it to be 0
    holoRcvr.control(AMB::HolographyReceiver::RESET_PLL_STATUS, data1);
    expectedNumberCANTransactions++;
    monitorBytes = holoRcvr.monitor( AMB::HolographyReceiver::GET_PLL_STATUS);
    expectedNumberCANTransactions++;
    if( monitorBytes[0] != 0x00 )
    {
	cout << "Didn't reset PLL status correctly: " << (int)monitorBytes[0] <<
	    " should be 0\n";
	bTestsSuccessful = false;
    }

    // Check basic CAN functions
    monitorBytes = holoRcvr.monitor( AMB::HolographyDSP::GET_CAN_ERROR );
    expectedNumberCANTransactions++;

    short shortVal;
    if( (shortVal = AMB::dataToShort(monitorBytes)) != 0 )
    {
	cout << "CAN Error msg: " << shortVal << " expected 0" << endl;
	bTestsSuccessful = false;
    }
    
    monitorBytes = holoRcvr.monitor( AMB::HolographyReceiver::GET_PROTOCOL_REV_LEVEL );
    expectedNumberCANTransactions++;
    if( (shortVal = AMB::dataToShort(monitorBytes)) != 0 )
    {
	cout << "Protocol Rev. Level Error: " << shortVal << " expected 0" << endl;
	bTestsSuccessful = false;
    }

    monitorBytes = holoRcvr.monitor( AMB::HolographyReceiver::GET_AMBIENT_TEMPERATURE );
    expectedNumberCANTransactions++;
    if( (shortVal = AMB::dataToShort(monitorBytes)) != 0x2C )
    {
	cout << "Ambient Temperature error : " << shortVal << " expected 0x2C" << endl;
	bTestsSuccessful = false;
    }

    monitorBytes = holoRcvr.monitor( AMB::HolographyReceiver::GET_TRANS_NUM );
    expectedNumberCANTransactions++;

    long longVal;
    if( (longVal = AMB::dataToLong(monitorBytes)) != expectedNumberCANTransactions )
    {
	cout << "Number of CAN Transactions Error: " << longVal << " expected " 
	     << expectedNumberCANTransactions << endl;
	bTestsSuccessful = false;
    }
    return bTestsSuccessful;
}


//------------------------------------------------------------------------------
void checkExpectedDataPointValue( const vector<CAN::byte_t> &monitorBytes,
				  long long &expectedValue, unsigned int size)
{
    if( monitorBytes.size() != size )
    {
	cout << "Incorrect data point size: " << monitorBytes.size() << " expected " << size << endl;
	gTestsSuccessful = false;
	return;
    }

    vector<CAN::byte_t> tmp(monitorBytes);
    long long dataPoint = AMB::dataToLonglong( tmp, size );
    if( dataPoint != expectedValue )
    {
	cout << "Data point value: " << dataPoint << " doesn't match expected value: " << expectedValue << endl;
	gTestsSuccessful = false;
    }
    expectedValue++;
}

//------------------------------------------------------------------------------
template <class T> 
void checkExpectedReceiverValue( const vector<CAN::byte_t> &monitorBytes,
				 T expectedValue, unsigned int size, const char *pMonitorName)
{
    if( monitorBytes.size() != size )
    {
	cout << "Incorrect holo. receiver size: " << monitorBytes.size() << " expected " << size
	     << " for " << pMonitorName << endl;
	gTestsSuccessful = false;
	return;
    }

    vector<CAN::byte_t> tmp(monitorBytes);
    short actualValue;
    if( size == 1 )
	actualValue = (short) monitorBytes[0];
    else if( size == 2 )
	actualValue = AMB::dataToShort( tmp );
    else //if( size == 4 )
	return;

    if( actualValue != expectedValue )
    {
	cout << "Holo. Receiver value: " << actualValue << " != expected value: " << expectedValue 
	     << " for " << pMonitorName << endl;
	gTestsSuccessful = false;
    }
}

//------------------------------------------------------------------------------
void printCANMsg( const vector<CAN::byte_t> &CANMsg, const char *pMsg )
{
    cout << pMsg << ' ';
    for( int i = 0; i < 8; i++ )
	cout << (int)CANMsg[i] << ' ';

    cout << endl;
}
