/*******************************************************************************
*    ALMA - Atacama Large Millimiter Array
*    (c) European Southern Observatory, 2002
*    Copyright by ESO (in the framework of the ALMA collaboration)
*    and Cosylab 2002, All rights reserved
*
*    This library is free software; you can redistribute it and/or
*    modify it under the terms of the GNU Lesser General Public
*    License as published by the Free Software Foundation; either
*    version 2.1 of the License, or (at your option) any later version.
*
*    This library is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    Lesser General Public License for more details.
*
*    You should have received a copy of the GNU Lesser General Public
*    License along with this library; if not, write to the Free Software
*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*/

#include "vltPort.h"
#include <math.h>

#include <CANTypes.h>
#include <AMBDevice.h>
#include <ACU.h>

static char *rcsId="@(#) $Id:"; 
static void *use_rcsId = ((void)(void)&use_rcsId,(void *) &rcsId);

#include <cppunit/extensions/HelperMacros.h>

/* 
 * A test case for the HWSimulator class
 *
 */

class ACUTestCase : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE( ACUTestCase );
    CPPUNIT_TEST( test_system_status );
    CPPUNIT_TEST_SUITE_END();
    
  public:
    void setUp();
    void tearDown();

  protected:
    void test_system_status();

    AMB::Device* device_m;
};

CPPUNIT_TEST_SUITE_REGISTRATION( ACUTestCase );

void ACUTestCase::setUp()
{
    std::vector<CAN::byte_t> sn(8, '0');
    device_m = new AMB::ACU(0x0, sn);
    
}

void ACUTestCase::tearDown()
{
    delete device_m;    
}

void ACUTestCase::test_system_status()
{
    std::vector<CAN::byte_t> reply;
    std::vector<CAN::byte_t> message;
    reply = device_m->monitor(ACUbase::GET_SYSTEM_STATUS);//0x00023);
    CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(3), reply.size());
    CPPUNIT_ASSERT_EQUAL(static_cast<CAN::byte_t>(0), reply[0]);
    CPPUNIT_ASSERT_EQUAL(static_cast<CAN::byte_t>(0), reply[1]);
    CPPUNIT_ASSERT_EQUAL(static_cast<CAN::byte_t>(0), reply[2]);

    message.push_back(static_cast<CAN::byte_t>(1));
    message.push_back(static_cast<CAN::byte_t>(2));
    message.push_back(static_cast<CAN::byte_t>(3));
    
    device_m->control(static_cast<AMB::rca_t>(ACUbase::SIM_SYSTEM_STATUS /*0x10023*/), message);
    reply = device_m->monitor(ACUbase::GET_SYSTEM_STATUS/*0x00023*/);
    CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(3), reply.size());
    CPPUNIT_ASSERT_EQUAL(static_cast<CAN::byte_t>(1), reply[0]);
    CPPUNIT_ASSERT_EQUAL(static_cast<CAN::byte_t>(2), reply[1]);
    CPPUNIT_ASSERT_EQUAL(static_cast<CAN::byte_t>(3), reply[2]);

}

/*
 * Main function running the tests
 */ 
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>

int
main( int argc, char* argv[] )
{
  // Create the event manager and test controller
  CPPUNIT_NS::TestResult controller;

  // Add a listener that colllects test result
  CPPUNIT_NS::TestResultCollector result;
  controller.addListener( &result );        

  // Add a listener that print dots as test run.
  CPPUNIT_NS::BriefTestProgressListener progress;
  controller.addListener( &progress );      

  // Add the top suite to the test runner
  CPPUNIT_NS::TestRunner runner;
  runner.addTest( CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest() );
  runner.run( controller );

  // Print test in a compiler compatible format.
  CPPUNIT_NS::CompilerOutputter outputter( &result, std::cerr );
  outputter.write(); 

  return result.wasSuccessful() ? 0 : 1;
}

