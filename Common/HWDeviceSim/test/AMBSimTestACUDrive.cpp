// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it
// under
// the terms of the GNU Library General Public License as published by the
// Free
// Software Foundation; either version 2 of the License, or (at your option)
// any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for
// more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation,
// Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include "ACUDrive.h"
#include <unistd.h>
#include <string>
using namespace std;

void help()
{
    cout << "q to quit" << endl;
    cout << "a to set current axis" << endl;
    cout << "s to set" << endl;
    cout << "g to get" << endl;
    cout << "m to set mode, 0 shutdown, 1 standby, 2 encoder, ";
    cout << "3 autonomous, 4 maintenance stow, 5 survival" << endl;
    cout << "z for acu/axis status" << endl;
    cout << "? for this message" << endl;
}

int main(int argc,char** argv)
{
    double velocity, position;
    char ch;
    string azString("az"), elString("el"), axis("az");
    ACUDrive acu;
    unsigned char newmodeAz = (unsigned char) 0;
    unsigned char newmodeEl = (unsigned char) 0;
    unsigned char newmode   = (unsigned char) 0;

    help();

    do
	{
        switch (ch = getchar())
	    {
	    case 'a':
                {
		string tmp;
		cout << "enter az, el, or ?" << endl;
                cin >> tmp;
                if (tmp == "?")
                    cout << "axis set to " << axis << endl;
                else
		    {
                    if (tmp != azString && tmp != elString)
			{
                        cout << "bad value for axis" << endl;
			}
                    else
                        axis = tmp;
		    }
		}
		break;

	    case 'q':
		return 0;

	    case 's':
		cout << "enter position, velocity" << endl;
		cin >> position >> velocity;
		if (axis == azString)
		    acu.setAzPsn(position, velocity);
		else
		    acu.setElPsn(position, velocity);
		break;

	    case 'g':
		if (axis == azString)
		    acu.getAzPsn(position, velocity);
		else
		    acu.getElPsn(position, velocity);
		cout << "current: p " << position << ", v " << 
		    velocity << endl;
		break;

	    case 'z':
                {
                unsigned long long status;
                std::vector<char> tmp(2);
                tmp = acu.getACUMode();
                cout << "ACU Mode "  << (int) tmp[0] << endl;
                acu.getAzStatus (status);
                cout << "AZ status " << hex << status << endl;
                acu.getElStatus (status);
                cout << "EL status " << hex << status << endl;
		}
		break;

	    case 'm':
                {
		int m;
		cout << "enter mode" << endl;
                cin >> m;
                if (axis == azString)
		    {
		    newmodeAz = (unsigned char) m;
		    }
                else
		    {
		    newmodeEl = ((unsigned char) m) << 4;
		    }
		newmode = newmodeAz | newmodeEl;
		cout << "New mode is " << (int) newmode << endl;
		if(!acu.setACUMode(newmode))
		    {
		    cout << "New mode set " << endl;
		    }
		else
		    {
		    cout << "New mode not set (invalid value) " << endl;
		    }
		}
		break;

	    case '?':
                {
		help();
		}
		break;

	    default:
		break;

	    }
	} while (ch != 'q');
    
    return 0;
}
