// @(#) $Id$
//
// Copyright (C) 2002
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

/** This tests the function calls of the Nutator class which represents the nutator device
 ** simulator.
 ** This test displays 'Tests Successful' if all tests pass, otherwise
 ** error messages appear ending with 'Tests Failed'.
 */

#include "Nutator.h"
#include "AMBUtil.h"
#include <iostream>
#include <math.h>

using namespace std;

// Forward declarations
bool printFunctionConclusion(int expected, int actual,const char *pMonitorName, bool success); 
bool doGenericTests(AMB::Nutator &nutator);
bool doPositionTests(AMB::Nutator &nutator);
bool doPIDLoopTests(AMB::Nutator &nutator);
bool doProgramPointsTests(AMB::Nutator &nutator);
int  initStandbyProgram(AMB::Nutator &nutator);
vector<CAN::byte_t> IEEEFloatToData(float value); 
bool monitorMatchesIEEEFloat( const vector<CAN::byte_t> &monitorBytes,
				float IEEEFloat,  const char *pMonitorName);


//------------------------------------------------------------------------------
/* This tests the Nutator class. It basically tests the get data point 
** monitor and control points.
*/

//------------------------------------------------------------------------------
// Perform tests of generic monitors included in the front-end bias & control module simulator. 
bool doGenericTests(AMB::Nutator &nutator)
{
    bool bTestsSuccessful = true;
    vector<CAN::byte_t> monitorBytes;
    int expectedNumberCANTransactions = 0;

    // Do monitor checks on FEBias generic monitor points
    // CAN errors should be 0
    monitorBytes = nutator.monitor( nutator.GET_CAN_ERROR );
    expectedNumberCANTransactions++;
    short shortTmp;
    if( (shortTmp = AMB::dataToShort(monitorBytes)) != 0 )
    {
	cerr << "CAN Error msg: " << shortTmp << " expected 0" << endl;
	bTestsSuccessful = false;
    }
    
    // Protocol rev level should be 0
    monitorBytes = nutator.monitor( nutator.GET_PROTOCOL_REV_LEVEL );
    expectedNumberCANTransactions++;
    if( (shortTmp = AMB::dataToShort(monitorBytes)) != 0 )
    {
	cerr << "Protocol Rev. Level Error: " << shortTmp << " expected 0" << endl;
	bTestsSuccessful = false;
    }

    // Ambient temperature is always 18.0
    float floatTmp;
    monitorBytes = nutator.monitor( nutator.GET_AMBIENT_TEMPERATURE );
    expectedNumberCANTransactions++;
    floatTmp = fabs(AMB::dataToFloatDS1820(monitorBytes));
    if( (floatTmp - 18.0) > 0.01)
    {
	cerr << "Ambient Temperature error : " << floatTmp << " expected 18.0 C" << endl;
	bTestsSuccessful = false;
    }

    monitorBytes = nutator.monitor( nutator.GET_TRANS_NUM );
    expectedNumberCANTransactions++;

    int intTmp = static_cast<int>(AMB::dataToLong(monitorBytes));
    bTestsSuccessful = printFunctionConclusion(expectedNumberCANTransactions, intTmp, 
					       "Generic monitor ", bTestsSuccessful);
   return bTestsSuccessful;
}

//------------------------------------------------------------------------------
// Perform tests of nutator position simulator 
bool doPositionTests(AMB::Nutator &nutator)
{
    bool bTestsSuccessful = true;
    vector<CAN::byte_t> monitorBytes, controlBytes, startPositionBytes;
    // Time between position and status checks in usec.  This test will take approximately 93 sec.
    int sleep_times[19] = {500000,2500000,5000000,16000000,3000000,5000000,2000000,4000000,
			   5000000,5000000,5000000,5000000,5000000,5000000,5000000,5000000,
			   5000000,5000000,5000000};
    short int lastPosition = 0;
    short int currentPosition = 0;
    unsigned char status = 0;
    // To check the # of CAN transactions monitor points, keep track of the
    // # of times we call monitor & control points
    int expectedNumberCANTransactions = 0;
    int startNumberCANTransactions = nutator.get_trans_num();

    cout << "Position tests in progress.  These tests will take between 1.5 and 2.0 minutes.  Please wait..." << endl;
    try
    {
	expectedNumberCANTransactions = initStandbyProgram(nutator);
	expectedNumberCANTransactions++;
	startPositionBytes = nutator.monitor(nutator.GET_POSITION);
	// start the program
	expectedNumberCANTransactions++;
	nutator.control(nutator.START_PROGRAM,controlBytes);
	// Do a position check before first position has been reached
	usleep(sleep_times[0]);
	expectedNumberCANTransactions++;
	monitorBytes = nutator.monitor(nutator.GET_POSITION);
    	if (!monitorBytes.empty() &&
    	    (AMB::dataToShort(monitorBytes) != AMB::dataToShort(startPositionBytes)))
    	    throw AMB::Nutator::NutatorError("The nutator should still be in the start position.");
	// Check status -- should be running
	monitorBytes.clear();
	expectedNumberCANTransactions++;
	monitorBytes = nutator.monitor(nutator.GET_STATUS);
	status = AMB::dataToChar(monitorBytes);
	if (status & 1 == 0)
	    throw AMB::Nutator::NutatorError("First cycle of program is running but status say it is not.");
	// The next 7 positions should all be in the first cycle and should have the same
	// or increasing values.  The status should be RUNNING - 1.
	for (int i = 1; i < 8; i++)
	{
	    monitorBytes.clear();
	    usleep(sleep_times[i]);
	    expectedNumberCANTransactions++;
	    monitorBytes = nutator.monitor(nutator.GET_POSITION);
	    currentPosition = AMB::dataToShort(monitorBytes);
	    if (currentPosition < lastPosition)
		throw AMB::Nutator::NutatorError("Improper nutator position advance in first cycle.");
	    lastPosition = currentPosition;
	    monitorBytes.clear();
	    expectedNumberCANTransactions++;
	    monitorBytes = nutator.monitor(nutator.GET_STATUS);
	    status = AMB::dataToChar(monitorBytes);
	    if (status & 1 == 0)
		throw AMB::Nutator::NutatorError("First cycle of program is running but status say it is not.");
	}
	// Starting new cycle -- current position should be now smaller than last and status
	// should be RUNNING - 1.
	monitorBytes.clear();
	usleep(sleep_times[8]);
	expectedNumberCANTransactions++;
	monitorBytes = nutator.monitor(nutator.GET_POSITION);
        currentPosition = AMB::dataToShort(monitorBytes);
	if (currentPosition >= lastPosition)
	    throw AMB::Nutator::NutatorError("Improper nutator position advance to second cycle.");
	lastPosition = currentPosition;
	monitorBytes.clear();
	expectedNumberCANTransactions++;
	monitorBytes = nutator.monitor(nutator.GET_STATUS);
	status = AMB::dataToChar(monitorBytes);
	if (status & 1 == 0)
	    throw AMB::Nutator::NutatorError("Program is running from first cycle to second but status say it is not.");
	// Command to stop when 4th program point in second cycle is reached -- the fourth 
	// program point should be reached in 70 secs.
	controlBytes.clear();
	controlBytes = AMB::charToData(controlBytes,4);
	expectedNumberCANTransactions++;
	nutator.control(nutator.STOP_PROGRAM,controlBytes);
	// The following four positions should be advancing in the second cycle and status should
	// be RUNNING - 1
	for (int i = 9; i < 13; i++)
	{
	    monitorBytes.clear();
	    usleep(sleep_times[i]);
	    expectedNumberCANTransactions++;
	    monitorBytes = nutator.monitor(nutator.GET_POSITION);
	    currentPosition = AMB::dataToShort(monitorBytes);
	    if (currentPosition < lastPosition)
		throw AMB::Nutator::NutatorError("Improper nutator position advance in second cycle.");
	    lastPosition = currentPosition;
	    monitorBytes.clear();
	    expectedNumberCANTransactions++;
	    monitorBytes = nutator.monitor(nutator.GET_STATUS);
	    status = AMB::dataToChar(monitorBytes);
	    if (status & 1 == 0)
		throw AMB::Nutator::NutatorError("Second cycle of program is running but status say it is not.");
	}
	// The following six positions should be the same and the status should be STOPPED - 0
	for (int i = 13; i < 19; i++)
	{
	    monitorBytes.clear();
	    usleep(sleep_times[i]);
	    expectedNumberCANTransactions++;
	    monitorBytes = nutator.monitor(nutator.GET_POSITION);
	    currentPosition = AMB::dataToShort(monitorBytes);
	    if (currentPosition < lastPosition)
		throw AMB::Nutator::NutatorError("Nutator position should not be advancing since it is stopped.");
	    lastPosition = currentPosition;
	    monitorBytes.clear();
	    expectedNumberCANTransactions++;
	    monitorBytes = nutator.monitor(nutator.GET_STATUS);
	    status = AMB::dataToChar(monitorBytes);
	    if (status & 1 == 1)
		throw AMB::Nutator::NutatorError("Program is stopped but status say it is running.");
	}
    }
    catch(AMB::Nutator::NutatorError e)
    {
	cout << e << endl;
	bTestsSuccessful = false;
    }


    bTestsSuccessful = printFunctionConclusion(expectedNumberCANTransactions, 
					       nutator.get_trans_num() - 
					       startNumberCANTransactions, 
					       "Nutator position ", bTestsSuccessful);
    return bTestsSuccessful;
}


//------------------------------------------------------------------------------
// This function tests setting and monitoring standby and active program points
bool doProgramPointsTests(AMB::Nutator &nutator)
{
    bool bTestsSuccessful = true;
    vector<CAN::byte_t> monitorBytes, controlBytes;
    vector<vector<CAN::byte_t> > tmpStandbyProgram;
    // To check the # of CAN transactions monitor points, keep track of the
    // # of times we call monitor & control points
    int expectedNumberCANTransactions = 0;
    int startNumberCANTransactions = nutator.get_trans_num();

    try
    {
	expectedNumberCANTransactions = initStandbyProgram(nutator);
	// Set program positions, transition and dwell times
	// Total program time is 42 sec. per program cycle
	for (int i = 0; i < nutator.MAX_POINTS_IN_PROGRAM; i++)
	{
	    monitorBytes.clear();
	    expectedNumberCANTransactions++;
	    monitorBytes = nutator.monitor(nutator.GET_STANDBY_PROG_SEG_0 + i);
	    // save standby program for later comparisons
	    tmpStandbyProgram.push_back(monitorBytes); 
	}
	// start the program
	expectedNumberCANTransactions++;
	nutator.control(nutator.START_PROGRAM,controlBytes);
	// compare the saved standby program to the active program -- they should be the same
	for (int i = 0; i < nutator.MAX_POINTS_IN_PROGRAM; i++)
	{
	    monitorBytes.clear();
	    expectedNumberCANTransactions++;
	    monitorBytes = nutator.monitor(nutator.GET_ACTIVE_PROG_SEG_0 + i);
	    if (tmpStandbyProgram[i] != monitorBytes)
		throw AMB::Nutator::NutatorError("Program point does not match standby point."); 
	}
	// abort the program
	expectedNumberCANTransactions++;
	nutator.control(nutator.ABORT_PROGRAM,controlBytes);
    }
    catch(AMB::Nutator::NutatorError e)
    {
	cout << e << endl;
	bTestsSuccessful = false;
    }


    bTestsSuccessful = printFunctionConclusion(expectedNumberCANTransactions, 
					       nutator.get_trans_num() - 
					       startNumberCANTransactions, 
					       "Nutator program points ", bTestsSuccessful);
    return bTestsSuccessful;
}

//------------------------------------------------------------------------------
// This function sets and then reads all the PID loop coefficients.
bool doPIDLoopTests(AMB::Nutator &nutator)
{
    bool bTestsSuccessful = true;
    vector<CAN::byte_t> monitorBytes, controlBytes;
    // To check the # of CAN transactions monitor points, keep track of the
    // # of times we call monitor & control points
    int expectedNumberCANTransactions = 0;
    int startNumberCANTransactions = nutator.get_trans_num();

    try
    {
	// Set loop coefficients
	int loopIndex = 0;
	float loopCoeff = 0.0;
	for (int i = 0; i < 2; i++)
	{
	    for (int j = 0; j < 3; j++)
	    {
		controlBytes.clear();
		expectedNumberCANTransactions++;
		nutator.control(nutator.SET_LOOP1_P + loopIndex,AMB::floatToData(controlBytes,
									       loopCoeff,-100000.0,
									       100000.0, 32));
		loopIndex++;
		loopCoeff += 1.0;
	    } 
	}
	// get loop coefficients -- are they what is expected?
	loopIndex = 0;
	loopCoeff = 0.0;
	float tmpFloat = 0.0;
	for (int i = 0; i < 2; i++)
	{
	    for (int j = 0; j < 3; j++)
	    {
		monitorBytes.clear();
		expectedNumberCANTransactions++;
		monitorBytes = nutator.monitor(nutator.GET_LOOP1_P + loopIndex);
		tmpFloat = AMB::dataToFloat(monitorBytes,  -100000.0, 100000.0, 32);
		if (fabs(tmpFloat - loopCoeff) > 0.01)
		    throw AMB::Nutator::NutatorError("Loop coefficient not what was expected.");
		loopIndex++;
		loopCoeff += 1.0;
	    }
	}
    }
    catch(AMB::Nutator::NutatorError e)
    {
	cout << e << endl;
	bTestsSuccessful = false;
    }


    bTestsSuccessful = printFunctionConclusion(expectedNumberCANTransactions, 
					       nutator.get_trans_num() - 
					       startNumberCANTransactions, 
					       "PID Loops ", bTestsSuccessful);
    return bTestsSuccessful;
}

//------------------------------------------------------------------------------
// This function monitors various simple nutator properties
bool doGetMiscellaneousProperties(AMB::Nutator &nutator)
{
    bool bTestsSuccessful = true;
    vector<CAN::byte_t> monitorBytes, controlBytes;
    // To check the # of CAN transactions monitor points, keep track of the
    // # of times we call monitor & control points
    int expectedNumberCANTransactions = 0;
    int startNumberCANTransactions = nutator.get_trans_num();

    try
    {
	monitorBytes.clear();
	expectedNumberCANTransactions++;
	monitorBytes = nutator.monitor(nutator.GET_PROGRAM_VALIDITY);
	if (AMB::dataToChar(monitorBytes) != 1)
	    throw AMB::Nutator::NutatorError("Validity should be 1.");
	monitorBytes.clear();
	expectedNumberCANTransactions++;
	monitorBytes = nutator.monitor(nutator.GET_APEX_PS_STATUS);
	if (AMB::dataToLong(monitorBytes) != 0)
	    throw AMB::Nutator::NutatorError("Apex PS status should be 0");
	monitorBytes.clear();
	expectedNumberCANTransactions++;
	monitorBytes = nutator.monitor(nutator.GET_LINAMP_PS_STATUS);
	if (AMB::dataToLong(monitorBytes) != 0)
	    throw AMB::Nutator::NutatorError("Linear amplifier PS status should be 0");
	monitorBytes.clear();
	expectedNumberCANTransactions++;
	monitorBytes = nutator.monitor(nutator.GET_CONTROLLER_PS_STATUS);
	if (AMB::dataToLong(monitorBytes) != 0)
	    throw AMB::Nutator::NutatorError("Controller PS status should be 0");
	monitorBytes.clear();
	expectedNumberCANTransactions++;
	monitorBytes = nutator.monitor(nutator.GET_SELFTEST_RESULT);
	if (AMB::dataToShort(monitorBytes) != 0)
	    throw AMB::Nutator::NutatorError("Self test results should be 0 before test is done.");
	// Simulate doing a self test.
	controlBytes.clear();
	expectedNumberCANTransactions++;
	nutator.control(nutator.DO_SELFTEST, controlBytes);
	// Get self test results after self test is performed.
	monitorBytes.clear();
	expectedNumberCANTransactions++;
	monitorBytes = nutator.monitor(nutator.GET_SELFTEST_RESULT);
	if (AMB::dataToShort(monitorBytes) != 2)
	    throw AMB::Nutator::NutatorError("Self test results should be 2 after test is done.");
	// Read all 16 temperatures.
	float tmpFloat;
	for (int i = 0; i <= 0x0f; i++)
	{
	    monitorBytes.clear();
	    expectedNumberCANTransactions++;
	    monitorBytes = nutator.monitor(nutator.GET_TEMPERATURE_0 + i);
	    tmpFloat = AMB::dataToFloat(monitorBytes,  -100000.0, 100000.0, 32);
	    if ((tmpFloat - 18.0) > 0.01)
		throw AMB::Nutator::NutatorError("Temperature should be 18.0."); 
	}
    }
    catch(AMB::Nutator::NutatorError e)
    {
	cout << e << endl;
	bTestsSuccessful = false;
    }


    bTestsSuccessful = printFunctionConclusion(expectedNumberCANTransactions, 
					       nutator.get_trans_num() - 
					       startNumberCANTransactions, 
					       "Miscellaneous properties ", bTestsSuccessful);
    return bTestsSuccessful;
}

//------------------------------------------------------------------------------
// This funtion set the program position, dwell and transition time for the standby program
int  initStandbyProgram(AMB::Nutator &nutator)
{
    vector<CAN::byte_t> monitorBytes, controlBytes;
    // To check the # of CAN transactions monitor points, keep track of the
    // # of times we call monitor & control points
    int expectedNumberCANTransactions = 0;

    // Set program positions, transition and dwell times
    // Total program time is 42 sec. per program cycle
    for (int i = 0; i < nutator.MAX_POINTS_IN_PROGRAM; i++)
    {
	controlBytes.clear();
	// positions are powers of 2 starting with 2 and ending with 64
	controlBytes = AMB::shortToData(controlBytes,(2 << i));
	// transition time are from 1 - 6 sec. in increasing duration
	controlBytes = AMB::shortToData(controlBytes,(i + 1) * 1000);
	// dwell times are from 1 - 6 sec. in decreasing duration
	controlBytes = AMB::shortToData(controlBytes,(nutator.MAX_POINTS_IN_PROGRAM-i) * 1000);
	// set the standby program points
	expectedNumberCANTransactions++;
	nutator.control(static_cast<AMB::Nutator::Controls_t>(
	    nutator.SET_STANDBY_PROG_SEG_0 + i),controlBytes);
	// get and compare the standby program points to what they were set to
	expectedNumberCANTransactions++;
	nutator.control(static_cast<AMB::Nutator::Controls_t>(
	    nutator.SET_STANDBY_PROG_SEG_0 + i),controlBytes);
    }
    return expectedNumberCANTransactions;
}

//------------------------------------------------------------------------------
// Utility function that prints the concluding remarks to each test function
bool printFunctionConclusion(int expected, int actual,const char *pMonitorName, bool success)
{
    bool bTestsSuccessful = success;
    if (actual != expected )
    {
	cerr << pMonitorName << " tests: Number of CAN Transactions Error: " << actual 
	     << " expected "  << expected << endl;
	if (bTestsSuccessful)
	    bTestsSuccessful = false;
    }

    if (bTestsSuccessful)
	cout << pMonitorName << "tests were successful. ";
    else
	cerr << pMonitorName << "tests failed. ";
    cout << "Number of transactions : " << dec << expected << endl;
    return bTestsSuccessful;
}
 
//------------------------------------------------------------------------------
int main()
{
    bool bTestsSuccessful = true;

    AMB::node_t nutatorNode = 0x13;

    vector<CAN::byte_t> nutatorSn(8);

    //  Nutator S/N looks like 247a70f0
    nutatorSn[0] = 0x02;
    nutatorSn[1] = 0x04;
    nutatorSn[2] = 0x07;
    nutatorSn[3] = 0x0a;
    nutatorSn[4] = 0x07;
    nutatorSn[5] = 0x00;
    nutatorSn[6] = 0x0f;
    nutatorSn[7] = 0x00;

    AMB::Nutator nutator( nutatorNode, nutatorSn ); 

    //* Constructor checks
    // Serial # check
    vector<CAN::byte_t> sn = nutator.serialNumber();
    for( int i = 0; i < 8; i++ )
    {
	if( sn[i] != nutatorSn[i] )
	{
	    bTestsSuccessful = false;
	    cerr << "Nutator: S/N check failed on digit " << i << endl;
	}
    }
    
    // Node # check
    if( nutatorNode != nutator.node() )
    {
	bTestsSuccessful = false;
	cerr << "nutator: node check failed: " << nutator.node() << " should be: "
	     << nutatorNode << endl;
    }
    
    if (!doGenericTests(nutator))
	bTestsSuccessful = false;
    
    if (!doGetMiscellaneousProperties(nutator))
	bTestsSuccessful = false;

    if( !doPIDLoopTests(nutator))
	bTestsSuccessful = false;

    if( !doProgramPointsTests(nutator))
	bTestsSuccessful = false;

    if( !doPositionTests(nutator))
	bTestsSuccessful = false;

    if( bTestsSuccessful )
	cout << "Tests Successful.  " ; 
    else
        cerr << "Tests Failed.  " ;
    cout << "Total number of transactions: " << nutator.get_trans_num() << endl;
}

