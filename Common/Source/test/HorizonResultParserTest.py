#! /usr/bin/env python
# "@(#) $Id$"
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2009, 2010
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import unittest
from CCL.HorizonResultParser import *
from CCL.SIConverter import SIConverter

class HorizonResultParserTest ( unittest.TestCase):

    # -------------- Parser Tests --------------------------
    def testUTParserHM(self):
        parser = UTParserHM()
        parser.setDataRange(5,22)
                                
        self.assertEqual(parser.parse("     2009-Oct-30 23:30"),
                                      134762382000000000)


    def testUTParserHMS(self):
        parser = UTParserHMS()
        parser.setDataRange(5,25)
                                
        self.assertEqual(parser.parse("     2009-Oct-30 23:30:15"),
                                      134762382150000000)

    def testUTParserHMSF(self):
        parser = UTParserHMSF()
        parser.setDataRange(5,29)
        self.assertEqual(parser.parse("     2009-Oct-30 23:30:15.123"),
                         134762382151230000)

    def testJDParser(self):
        parser = JDParser()
        parser.setDataRange(0,17)
        self.assertAlmostEqual(parser.parse("2455125.506944444"),
                               134753766000000000L, -3)

    def testSexagesimalPositionParser(self):
        testData = []
        testData.append(("21 20 54.76 -16 35 05.4",
                         5.5890358659584889, -0.28945994756117288))
        testData.append(("21 20 54.76  16 35 05.4",
                         5.5890358659584889, 0.28945994756117288))
        parser = SexagesimalPositionParser()
        parser.setDataRange(0,23)
 
        for entry in testData:
            self.assertAlmostEqual(parser.parse(entry[0])[0],entry[1],12)
            self.assertAlmostEqual(parser.parse(entry[0])[1],entry[2],12)

    def testDecimalPositionParser(self):
        testData = []
        testData.append(("320.22816 -16.58483",
                         5.5890357496032053, -0.28945988938353118))
        testData.append(("320.22816  16.58483",
                         5.5890357496032053, 0.28945988938353118))

        parser = DecimalPositionParser()
        parser.setDataRange(0,19)
        for entry in testData:
            self.assertAlmostEqual(parser.parse(entry[0])[0],entry[1],12)
            self.assertAlmostEqual(parser.parse(entry[0])[1],entry[2],12)

    def testDeltaParser(self):
        # First test using AU, given different values for the AU this
        # is only sorta precise
        testData = []
        testData.append(("4.73420699627461", 708227286053.0))
        testData.append(("1.00000000000000", 149597870691.0))

        parser = DeltaParser()
        parser.setDataRange(0,17)
        parser.setUnit("AU")
        for entry in testData:
            self.assertAlmostEqual(parser.parse(entry[0]),entry[1],-3)
        
        # Now test using km
        parser.setUnit("km")
        testData = []
        testData.append(("1.000000000000E6", 1E9))
        testData.append(("1.00000000000000", 1000))
        for entry in testData:
            self.assertAlmostEqual(parser.parse(entry[0]),entry[1],5)

    # ------ Test Parsing of example files ----------------
    def testParsingOfExampleFile1(self):
        # Test File 1 uses UT HR:MN and Sexagesimal Positions
        # Delta is expressed in AU
        parser = HorizonResultParser('./testHorizonOutputFiles/testFile1.txt')
        ephemeris = parser.getEphemeris()
        self.assertEqual(len(ephemeris), 25)
        string = parser.getEphemerisString()
        self.assertEqual(ephemeris[0][0], 134752896000000000L)
        self.assertAlmostEqual(ephemeris[0][1], 5.5831686507897009, 6)
        self.assertAlmostEqual(ephemeris[0][2], -0.29158730999388155, 6)
        self.assertAlmostEqual(ephemeris[0][3], 685622519002.66895, -3)

    def testParsingOfExampleFile2(self):
        #Test File 2 uses HR:MN:SS and Decimal Positions
        #Delta is expressed in km
        parser = HorizonResultParser('./testHorizonOutputFiles/testFile2.txt')
        ephemeris = parser.getEphemeris()
        self.assertEqual(len(ephemeris), 25)
        string = parser.getEphemerisString()
        self.assertEqual(ephemeris[0][0], 134752896000000000L)
        self.assertAlmostEqual(ephemeris[0][1], 5.5831686507897009, 6)
        self.assertAlmostEqual(ephemeris[0][2], -0.29158730999388155, 6)
        self.assertAlmostEqual(ephemeris[0][3], 685622519002.66895, -3)

    def testParsingOfExampleFile3(self):
        # Test File 3 uses JD
        parser = HorizonResultParser('./testHorizonOutputFiles/testFile3.txt')
        ephemeris = parser.getEphemeris()
        self.assertEqual(len(ephemeris), 25)
        string = parser.getEphemerisString()
        self.assertEqual(ephemeris[0][0], 134752896000000000L)
        self.assertAlmostEqual(ephemeris[0][1], 5.5831686507897009, 6)
        self.assertAlmostEqual(ephemeris[0][2], -0.29158730999388155, 6)
        self.assertAlmostEqual(ephemeris[0][3], 685622519002.66895, -3)

    def testParsingOfExampleFile4(self):
        #Test File 4 Uses UT HR:MS:SC.fff
        parser = HorizonResultParser('./testHorizonOutputFiles/testFile4.txt')
        ephemeris = parser.getEphemeris()
        self.assertEqual(len(ephemeris), 25)
        string = parser.getEphemerisString()
        self.assertEqual(ephemeris[0][0], 134752896000000000L)
        self.assertAlmostEqual(ephemeris[0][1], 5.5831686507897009, 6)
        self.assertAlmostEqual(ephemeris[0][2], -0.29158730999388155, 6)
        self.assertAlmostEqual(ephemeris[0][3], 685622519002.66895, -3)

    # ------- Now deal with large files ----------------------
    def testSelectingSubsetOfData(self):
        parser = HorizonResultParser('../config/HorizonsEphemerisIo.txt')
        ephemeris = parser.getEphemeris("1 d")
        self.assertEqual(len(ephemeris), 58)
        
# **************************************************
# MAIN Program
# **************************************************
if __name__ == '__main__':
    suite = unittest.makeSuite(HorizonResultParserTest)
    unittest.TextTestRunner(verbosity=2).run(suite)
