// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#include <HorizonObject.h> // for Control::EquatorialObject
#include <SourceAlarmTypes.h>
#include <controlAlarmSender.h> // for Control::AlarmSender
#include <loggingGetLogger.h> // for getLogger()
#include <ACSAlarmSystemInterfaceFactory.h> // for ACSAlarmSystemInterfaceFactory
#include <maciSimpleClient.h> // for maci::SimpleClient
#include <boost/shared_ptr.hpp> // for boost::shared_ptt
#include <vector>

using namespace std;

int main(int argc, char *argv[]) {
    maci::SimpleClient client;
    client.init(argc,argv);
    client.login();
    ACSAlarmSystemInterfaceFactory::init(ACSAlarmSystemInterfaceFactory::getManager());
 
    boost::shared_ptr<Control::AlarmHelper> alarmPtr(new Control::AlarmSender());
    {
        vector<Control::AlarmInformation> alarms(4);
        alarms[0].alarmCode = Control::NO_COMMS_TO_WS; 
        alarms[1].alarmCode = Control::WS_DISABLED;
        alarms[2].alarmCode = Control::OLD_WS_DATA;
        alarms[3].alarmCode = Control::NO_IERS_DATA;
        alarmPtr->initializeAlarms("MountController", "DV01", alarms);
    }

    ACS::Time t = ::getTimeStamp();
    Control::HorizonObject star(t, 0, 1,
                                getLogger(), alarmPtr);
    

    double az;
    double el;
    star.getAzEl(t, az, el);
    cerr << "az: " << az << " el: " << el << endl; 

    client.logout();
    client.disconnect();

}
