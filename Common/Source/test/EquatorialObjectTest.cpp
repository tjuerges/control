// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#include <EquatorialObject.h> // for Control::EquatorialObject
#include <Pattern.h> // for Control::Pattern
#include <OffsetStroke.h> // for Control::OffsetStroke
#include <SourceAlarmTypes.h> // for Control::NO_IERS_DATA
#include <controlAlarmSender.h> // for Control::AlarmSender
#include <WeatherStationC.h> // for Control::WeatherStation
#include <TETimeUtil.h> // for TETimeUtil

#include <casa/Quanta/MVAngle.h> // for casa::MVAngle
#include <casa/Quanta/MVPosition.h> // for casa::MVPosition
#include <measures/Measures/MPosition.h> // for casa::MPosition

#include <acscommonC.h> // for ACS::Time
#include <loggingGetLogger.h> // for getLogger()
#include <ACSAlarmSystemInterfaceFactory.h> // for ACSAlarmSystemInterfaceFactory
#include <maciSimpleClient.h> // for maci::SimpleClient

#include <boost/shared_ptr.hpp> // for boost::shared_ptr
#include <vector> // for std::vector

using namespace std;
using casa::MVAngle;

int main(int argc, char *argv[]) {
    maci::SimpleClient client;
    client.init(argc,argv);
    client.login();
    ACSAlarmSystemInterfaceFactory::init
        (ACSAlarmSystemInterfaceFactory::getManager());
 
    boost::shared_ptr<Control::AlarmHelper> 
        alarmPtr(new Control::AlarmSender());
    {
        vector<Control::AlarmInformation> alarms(4);
        alarms[0].alarmCode = Control::NO_COMMS_TO_WS; 
        alarms[1].alarmCode = Control::WS_DISABLED;
        alarms[2].alarmCode = Control::OLD_WS_DATA;
        alarms[3].alarmCode = Control::NO_IERS_DATA;
        alarmPtr->initializeAlarms("MountController", "DV01", alarms);
    }

    // A position on the earth (the OSF).
    casa::MPosition antLocation(casa::MVPosition(2202229.615,
                                                 -5445184.762, 
                                                 -2485382.11),
                                casa::MPosition::ITRF);

    // The weather station component to use to get the weather parameters. As
    // its set to nil the refraction correction uses default values.
    Control::CurrentWeather_ptr ws = Control::CurrentWeather::_nil();

    // The observing frequency (in Hz).
    const double obsFreq = 100E9;

    // The antenna Name (only used for logging so any string will do)
    const string antennaName="DV01";


    Control::EquatorialDirection target;
    Control::Offset equatorial;
    Control::EquatorialDirection equatorialRADec;
    Control::Offset horizon;
    Control::HorizonDirection horizonAzEl;
    Control::HorizonDirection commanded;
    Control::HorizonDirection commandedRate;
    double computedRA, computedDec;
    {
        // The star to track. Pick a circumpolar one so that it nevers
        // sets. This avoids all the problems with refraction corrections at
        // low elevation
        const double ra = M_PI;  
        const double dec = -80./180*M_PI;
        const double pmRA = 0.0;
        const double pmDec = 0.0;
        const double parallax = 0.0;
        const double epoch = 2000.0;
        ACS::Time t = ::getTimeStamp();

        Control::EquatorialObject star(t, ra, dec, pmRA, pmDec, parallax,
                                       antLocation, "DV01", "A1", ws, obsFreq, 
                                       epoch, getLogger(), alarmPtr, true);
        for (int i = 0; i < 24;  i++ ) {
            t += static_cast<ACS::Time>(3600/100E-9);
            star.getPointing(t, target, equatorial, equatorialRADec, horizon,
                             horizonAzEl, commanded, commandedRate);
            star.getActualRADec(commanded.az, commanded.el, t, 
                                computedRA, computedDec);
            if ((abs(target.ra - computedRA) > M_PI/180/60/60) || 
                (abs(target.dec - computedDec) > M_PI/180/60/60)) {
                cout << TETimeUtil::toTimeString(t)
                     << ", " << MVAngle(commanded.az).string(MVAngle::ANGLE, 9)
                     << ", " << MVAngle(commanded.el).string(MVAngle::ANGLE, 9)
                     << ", " << MVAngle(computedRA).string(MVAngle::TIME, 9)
                     << ", " << MVAngle(computedDec).string(MVAngle::ANGLE, 9)
                     << ", " << MVAngle(target.ra).string(MVAngle::TIME, 9) 
                     << ", " << MVAngle(target.dec).string(MVAngle::ANGLE, 9)
                     << endl;
            }
        }
    }

    {
        // The star to track. Pick one with a zero ra and dec and then offset
        // (to the pole). This makes the computation of the predicted ra/dec
        // simpler
        const double ra = 0.0;  
        const double dec = 0.0;
        const double pmRA = 0.0;
        const double pmDec = 0.0;
        const double parallax = 0.0;
        const double epoch = 2000.0;
        ACS::Time t = ::getTimeStamp();

        Control::EquatorialObject star(t, ra, dec, pmRA, pmDec, parallax,
                                       antLocation, "DV01", "A1", ws, obsFreq, 
                                       epoch, getLogger(), alarmPtr, true);
        boost::shared_ptr<Control::Stroke> 
            offset(new Control::OffsetStroke(M_PI/12, -80*M_PI/180));
        star.equatorialPattern().insert(offset);
        
        for (int i = 0; i < 24;  i++ ) {
            t += static_cast<ACS::Time>(3600/100E-9);
            star.getPointing(t, target, equatorial, equatorialRADec, horizon,
                             horizonAzEl, commanded, commandedRate);
            star.getActualRADec(commanded.az, commanded.el, t,
                                computedRA, computedDec);
            if ((abs(target.ra+equatorial.lng-computedRA) > M_PI/180/60/60) ||
                (abs(target.dec+equatorial.lat-computedDec) > M_PI/180/60/60)){
                cout << TETimeUtil::toTimeString(t)
                     << ", " << MVAngle(commanded.az).string(MVAngle::ANGLE, 9)
                     << ", " << MVAngle(commanded.el).string(MVAngle::ANGLE, 9)
                     << ", " << MVAngle(computedRA).string(MVAngle::TIME, 9) 
                     << ", " << MVAngle(computedDec).string(MVAngle::ANGLE, 9)
                     << ", " << MVAngle(target.ra + equatorial.lng)
                    .string(MVAngle::TIME, 9) 
                     << ", " << MVAngle(target.dec + equatorial.lat)
                    .string(MVAngle::ANGLE, 9)
                     << endl;
            }
        }
    }

    {
        // The star to track. Pick a circumpolar one so that it nevers
        // sets. This avoids all the problems with refraction corrections at
        // low elevation
        const double ra = M_PI;  
        const double dec = -80./180*M_PI;
        const double pmRA = 0.0;
        const double pmDec = 0.0;
        const double parallax = 0.0;
        const double epoch = 2000.0;
        ACS::Time t = ::getTimeStamp();

        Control::EquatorialObject star(t, ra, dec, pmRA, pmDec, parallax,
                                       antLocation, "DV01", "A1", ws, obsFreq, 
                                       epoch, getLogger(), alarmPtr, true);
        boost::shared_ptr<Control::Stroke> 
            offset(new Control::OffsetStroke(-2*M_PI/180, M_PI/180));
        star.horizonPattern().insert(offset);
        
        for (int i = 0; i < 24;  i++ ) {
            t += static_cast<ACS::Time>(3600/100E-9);
            star.getPointing(t, target, equatorial, equatorialRADec, horizon,
                             horizonAzEl, commanded, commandedRate);
            star.getActualRADec(commanded.az - horizonAzEl.az, 
                                commanded.el - horizonAzEl.el, t, 
                                computedRA, computedDec);
            if ((abs(target.ra+equatorial.lng-computedRA) > M_PI/180/60/60) || 
                (abs(target.dec+equatorial.lat-computedDec) > M_PI/180/60/60)){
                cout << TETimeUtil::toTimeString(t)
                     << ", " << MVAngle(commanded.az).string(MVAngle::ANGLE, 9)
                     << ", " << MVAngle(commanded.el).string(MVAngle::ANGLE, 9)
                     << ", " <<MVAngle(horizonAzEl.az).string(MVAngle::ANGLE,9)
                     << ", " <<MVAngle(horizonAzEl.el).string(MVAngle::ANGLE,9)
                     << ", " << MVAngle(computedRA).string(MVAngle::TIME, 9) 
                     << ", " << MVAngle(computedDec).string(MVAngle::ANGLE, 9)
                     << ", " << MVAngle(target.ra).string(MVAngle::TIME, 9) 
                     << ", " << MVAngle(target.dec).string(MVAngle::ANGLE, 9)
                     << endl;
            }
        }
    }

    client.logout();
    client.disconnect();
}
