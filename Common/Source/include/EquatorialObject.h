// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007 - 2010
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#ifndef EQUATORIALOBJECT_H
#define EQUATORIALOBJECT_H

// Base class(es)
#include "TrackableObject.h" // for Control::TrackableObject

// Forward declarations for classes that this component uses

//includes for data members

namespace Control {
    class EquatorialObject: public TrackableObject {
    public:
        // ------------------- Constructor & Destructor -------------------

        /// Use the specified RA & Dec. The start time is "Now" 
        EquatorialObject(const double& ra, const double& dec, 
                         const double& pmRA, const double& pmDec,
                         const double& parallax,
                         const casa::MPosition& antennaLocation,
                         const std::string& antennaName,
                         const std::string& padName,
                         const Control::CurrentWeather_ptr& ws,
                         const double& obsFreq,
                         const double& epoch,
                         Logging::Logger::LoggerSmartPtr logger,
                         boost::shared_ptr<AlarmHelper>& alarms,
                         bool verbose);

        /// Use the specified RA & Dec at the specified start time.
        EquatorialObject(const ACS::Time& startTime, 
                         const double& ra, const double& dec,
                         const double& pmRA, const double& pmDec,
                         const double& parallax,
                         const casa::MPosition& antennaLocation,
                         const std::string& antennaName,
                         const std::string& padName,
                         const Control::CurrentWeather_ptr& ws,
                         const double& obsFreq,
                         const double& epoch,
                         Logging::Logger::LoggerSmartPtr logger,
                         boost::shared_ptr<AlarmHelper>& alarms,
                         bool verbose);

        /// Use the specified RA & Dec, a start time of now and default weather
        /// parameters. This is primarily for computations where a precise
        /// Az/El is not required.
        EquatorialObject(const double& ra, const double& dec,
                         const double& pmRA, const double& pmDec,
                         const double& parallax,
                         const casa::MPosition& antennaLocation,
                         const double& epoch,
                         Logging::Logger::LoggerSmartPtr logger,
                         boost::shared_ptr<AlarmHelper>& alarm);

        /// Copy constructor (copy semantics).
        EquatorialObject(const EquatorialObject& other);
 
        /// Assignment (copy semantics).
        EquatorialObject& operator=(const EquatorialObject& other);

        /// The destructor must be virtual
        virtual ~EquatorialObject();

        // --------------------- C++ public functions ---------------------
        /// See the documentation for the TrackableObject base class.
        virtual void getPointing(const ACS::Time& time,
                                 Control::EquatorialDirection& target,
                                 Control::Offset& equatorial,
                                 Control::EquatorialDirection& equatorialRADec,
                                 Control::Offset& horizon,
                                 Control::HorizonDirection& horizonAzEl,
                                 Control::HorizonDirection& commanded,
                                 Control::HorizonDirection& commandedRate);

        /// Returns the RA and Dec associated with the supplied Az & El. The
        /// supplied az and el, at the specified time should be obtained from
        /// the ACU and the inverse transformation done.
        virtual void getActualRADec(double az, double el, ACS::Time time,
                                    double& ra, double& dec);

        /// Returns the commanded RA and Dec. This just returns the internal
        /// values and the az, el and time arguments are ignored.
        virtual void getCommandedRADec(double az, double el, ACS::Time time,
                                       double& ra, double& dec);

        /// returns the refraction coefficients used by this object.
        virtual void getRefractionCoefficients(double& a, double& b);

        /// Returns the time, in seconds, before this object will rise. Returns
        /// a negative number if this object never rises and zero if this
        /// source is already up. The elevation limit, in radians, of the mount
        /// must be specified.
        virtual double timeToRise(const double& elLimit);

        /// Returns the time, in seconds, before this object will set. Returns
        /// a negative number if this object is already set and a large
        /// positive number if this object never sets. The elevation limit, in
        /// radians, of the mount must be specified.
        virtual double timeToSet(const double& elLimit);

    private:
        // Move the source to a epoch *and* equinox of 2000.0. This adjusts the
        // position to allow for both precession/nutation and proper motion. It
        // does not do the FK4 to FK5 transformation.
        void moveTo2000(const double& epoch);

        double ra_m;
        double dec_m;
        double pmRA_m;
        double pmDec_m;
        double parallax_m;
        double ra2000_m;
        double dec2000_m;
    }; // end EquatorialObject class
} // end Control namespace

#endif // EQUATORIALOBJECT_H
