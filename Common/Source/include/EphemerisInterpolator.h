// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2009, 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#ifndef EPHEMERISINTERPOLATOR_H
#define EPHEMERISINTERPOLATOR_H

// Base class(es)

//CORBA servant header

//includes for data members
#include <SourceC.h> // for Control::Ephemeris
#include <acscommonC.h> // for ACS::Time
#include <vector>

namespace Control {
    class EphemerisInterpolator {
    public:

        /// The default constructor create an object that is basically
        /// useless. An exception will always be thrown if you call getPosition
        /// on an object created using this constructor. However you can use
        /// the assignment operator on an object created using this
        /// constructor.
        EphemerisInterpolator();

        /// Construct an interpolator using the specified ephemeris.  The
        /// ephemeris must have at least two rows and if there are more than
        /// two rows the all rows must be equally spaced in time. The ra and
        /// dec in this ephemeris must be in J2000 coordinates and in
        /// radians. The distance (rho) must be in meters. If there are any
        /// problems with the supplied Ephemeris an IllegalParameterEx
        /// exception is thrown.
        EphemerisInterpolator(const Control::Ephemeris& ephemeris);

	// Copy constructor (copy semantics).
	EphemerisInterpolator(const EphemerisInterpolator& other);
	
	/// Assignment (copy semantics).
	EphemerisInterpolator& operator=(const EphemerisInterpolator& other);
	
    /// Get the RA and Dec of the source in J2000 coordinates at the
    /// specified time. If the time is too far (1 hr) outside the range of
    /// the ephemeris, or if no ephemeris has been loaded an
    /// IllegalParameterEx exception is thrown.
    void getPosition(const ACS::Time& time, double& ra, double& dec);
	
    /// Get the RA, Dec and distance of the source in J2000 coordinates at the
    /// specified time. If the time is too far (1 hr) outside the range of
    /// the ephemeris, or if no ephemeris has been loaded an
    /// IllegalParameterEx exception is thrown.
    void getPosition(const ACS::Time& time, double& ra, double& dec, 
        double& rho);

    protected:

    class CompareRows {
    public:
        int operator()(const Control::EphemerisRow& row,
                       const ACS::Time& time) const {
            return(row.time < time);
        }

        int operator()(const ACS::Time& time,
                       const Control::EphemerisRow& row) const {
            return(time < row.time);
        }
    };
    

    private:
        std::vector<Control::EphemerisRow> eph_m;
    }; // end EphemerisInterpolator class
} // end Control namespace

#endif // EPHEMERISINTERPOLATOR
