#ifndef PATTERN_H
#define PATTERN_H
// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2007, 2008
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

// Base class(es)

// Forward declarations for classes that this component uses
namespace Control {
  class Stroke;
}

namespace boost {
  template<class T> class shared_ptr;
}

//includes for data members
#include <acscommonC.h> // for ACS::Time
#include <deque>
#include <ace/Recursive_Thread_Mutex.h>
#include <casacore/casa/Quanta/MVDirection.h> // for casa::MVDirection

namespace Control {
  class Pattern { 
  public:
    // ------------------- Constructor & Destructor -------------------
    // Creates an empty list. Use the insert function to add strokes to
    // the list.
    Pattern();

    // Copy constructor (copy semantics).
    Pattern(const Pattern& other);
    
    // Assignment (copy semantics).
    Pattern& operator=(const Pattern& other);

    // The destructor cleans up memory
    ~Pattern();

    // --------------------- C++ public functions ---------------------
    
    // Insert a stroke into the list. The memory for the Stroke pointer should
    // be created using new and this pointer will be taken over and deleted by
    // this class.
    void insert(boost::shared_ptr<Stroke> stroke);

    // Clear all the strokes in the list
    void clear();

    // Modify the input position offseting the latitude and longitude by a
    // fixed amount. Uses the current stroke in the list (and not all
    // strokes). This function also removes old strokes from teh start of the
    // list.
    void applyPattern(const ACS::Time& time, 
                      double currentLong, double currentLat, 
                      double currentLongVel, double currentLatVel, 
                      double& newLong, double& newLat,
                      double& newLongVel, double& newLatVel);

    // Return the instananious offset that would be applied by the current
    // stroke at the specified time.
    void getOffset(const ACS::Time& time, double& longOffset, 
                   double& latOffset) const;

    // set the reference direction
    void setRefDirection(const casa::MVDirection& refDirection);

    // get the reference direction. Only meaningful if isRefDirectionSet is true;
    casa::MVDirection getRefDirection() const;
    
    // returns true if the reference direction has been set by the user.
    bool isRefDirectionSet() const;

  private:
    // this contains all the active strokes
    std::deque<boost::shared_ptr<Control::Stroke> > pattern_m;

    // This mutex prevents simultaneous access to the pattern_m data member
    // when the functions in this class are called by either the
    // MountController component or the TrajectoryPlanner thread
    // Its mutable because even const functions need to acquire this mutex.
    mutable ACE_Recursive_Thread_Mutex patternMutex_m;

    // The reference direction for all new strokes in this pattern. This does
    // not apply to strokes that are already defined.
    casa::MVDirection refDirection_m;

    // Indicates whether the pattern reference direction should be used i.e, if
    // it should be set in each newly defined stroke
    bool useRefDirection_m;
    
  }; // end Pattern class
  
} // end Control namespace

#endif // PATTERN_H
