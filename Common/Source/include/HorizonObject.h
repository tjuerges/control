//  @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007 - 2010
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#ifndef HORIZONOBJECT_H
#define HORIZONOBJECT_H

// Base class(es)
#include "TrackableObject.h" // for Control::TrackableObject

// Forward declarations for classes that this component uses

//includes for data members

namespace Control {
    class HorizonObject: public TrackableObject {
    public:
        // ------------------- Constructor & Destructor -------------------
        
        /// Use the specified az & el. The start time is "Now" 
        HorizonObject(const double& az, const double& el,
                      Logging::Logger::LoggerSmartPtr logger,
                      boost::shared_ptr<AlarmHelper>& alarms);
        
        /// Use the specified az & el at the specified start time.
        HorizonObject(const ACS::Time& startTime, 
                      const double& az, const double& el,
                      Logging::Logger::LoggerSmartPtr logger,
                      boost::shared_ptr<AlarmHelper>& alarms);
        
        /// Copy constructor (copy semantics).
        HorizonObject(const HorizonObject& other);
        
        /// Assignment (copy semantics).
        HorizonObject& operator=(const HorizonObject& other);
        
        /// The destructor must be virtual
        virtual ~HorizonObject();
        
        // --------------------- C++ public functions ---------------------
        
        /// See the documentation for the TrackableObject base class.
        virtual void getPointing(const ACS::Time& time,
                                 Control::EquatorialDirection& target,
                                 Control::Offset& equatorial,
                                 Control::EquatorialDirection& equatorialRADec,
                                 Control::Offset& horizon,
                                 Control::HorizonDirection& horizonAzEl,
                                 Control::HorizonDirection& commanded,
                                 Control::HorizonDirection& commandedRate);
        
        /// Returns the RA and Dec associated with the supplied Az & El. The
        /// supplied az and el is obtained from the ACU. If necessary this will
        /// initialize the SLA parameters necessary to convert from az/el to
        /// RA/Dec.
        virtual void getActualRADec(double az, double el, ACS::Time time,
                                    double& ra, double& dec);
        
        /// Returns the RA and Dec associated with the supplied Az & El. The
        /// supplied az and el is ignored and replaced with the az/el within
        /// this class. If necessary this will initialize the SLA parameters
        /// required to convert from Az/El to RA/Dec.
        virtual void getCommandedRADec(double az, double el, ACS::Time time,
                                       double& ra, double& dec);
        
        /// Always returns zeros.
        void getRefractionCoefficients(double& a, double& b);
        
        /// See TrackableObject.h for a description of this function.
        virtual double timeToRise(const double& elLimit);

        /// See TrackableObject.h for a description of this function.
        virtual double timeToSet(const double& elLimit);

    private:
        double az_m;
        double el_m;
        
    }; // end HorizonObject class
} // end Control namespace
#endif // HORIZONOBJECT_H
