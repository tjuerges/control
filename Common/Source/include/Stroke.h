// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2010
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

#ifndef STROKE_H
#define STROKE_H

// Base class(es)

// Forward declarations for classes that this component uses
namespace boost {
  template<class T> class shared_ptr;
}

//includes for data members
#include <acscommonC.h> // for ACS::Time
#include <casacore/casa/Quanta/MVDirection.h> // for casa::MVDirection

namespace Control {
  class Stroke { 
  public:
    // ------------------- Constructor & Destructor -------------------
    // The start time is "Now" 
    Stroke();

    // this defines a start time as specified by the user
    Stroke(const ACS::Time& startTime);
    
    // Copy constructor (copy semantics).
    Stroke(const Stroke& other);
    
    // Assignment (copy semantics).
    Stroke& operator=(const Stroke& other);

    // The destructor must be virtual
    virtual ~Stroke();

    // --------------------- C++ public functions ---------------------
    
    // Modify the input position depending on the stroke parameters
    virtual void 
    applyPattern(const ACS::Time& time, 
                 const double& currentLong, const double& currentLat, 
                 const double& currentLongVel, const double& currentLatVel, 
                 double& newLong, double& newLat,
                 double& newLongVel, double& newLatVel) const = 0;

    // Return the start time of this stroke
    ACS::Time getStartTime() const;

    // set the reference direction
    void setRefDirection(const casa::MVDirection& refDirection);

    // get the reference direction. Only meaningful if isRefDirectionSet is true;
    casa::MVDirection getRefDirection() const;
    
    // returns true if the reference direction has been set by the user.
    bool isRefDirectionSet() const;

    static double separation(const double long1, 
                             const double lat1, 
                             const double long2, 
                             const double lat2);

    // Returns true if the startTime of the first Stroke is before the
    // second. This is used by the Pattern class to sort a queue of strokes.
    static bool compareTime(const boost::shared_ptr<Control::Stroke> x, 
                            const boost::shared_ptr<Control::Stroke> y);

  private:
    ACS::Time startTime_m;

    // The reference direction for this stroke
    casa::MVDirection refDirection_m;
    // Indicates whether the direction should be used i.e, if its been set
    bool useRefDirection_m;
  }; // end Stroke class
} // end Control namespace

#endif // STROKE_H
