// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007 - 2010
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#ifndef EPHEMERISOBJECT_H
#define EPHEMERISOBJECT_H

// Forward declarations for classes that this component uses
namespace casa {
  class MPosition;
  class Table;
}

// Base class(es)
#include "TrackableObject.h" // for Control::TrackableObject

//includes for data members
#include <SourceC.h> // for Control::Ephemeris
#include <EphemerisInterpolator.h>

namespace Control {
    class EphemerisObject: public TrackableObject {
    public:
        // ------------------- Constructor & Destructor -------------------

        /// Use the specified ephemeris. The start time is "Now"
        EphemerisObject(const Control::Ephemeris& userEphemeris,
                        const casa::MPosition& antennaLocation,
                        const std::string& antennaName,
                        const std::string& padName,
                        const Control::CurrentWeather_ptr& ws,
                        const double& obsFreq,
                        Logging::Logger::LoggerSmartPtr logger,
                        boost::shared_ptr<AlarmHelper>& alarms);

        /// Use the specified ephemeris at the specified start time.
        EphemerisObject(const ACS::Time& startTime, 
                        const Control::Ephemeris& userEphemeris,
                        const casa::MPosition& antennaLocation,
                        const std::string& antennaName,
                        const std::string& padName,
                        const Control::CurrentWeather_ptr& ws,
                        const double& obsFreq,
                        Logging::Logger::LoggerSmartPtr logger,
                        boost::shared_ptr<AlarmHelper>& alarms);

        /// Use the specified ephemeris, a start time of now and default
        /// weather parameters. This is primarily for computations where a
        /// precise Az/El is not required.
        EphemerisObject(const Control::Ephemeris& userEphemeris,
                        const casa::MPosition& antennaLocation,
                        Logging::Logger::LoggerSmartPtr logger,
                        boost::shared_ptr<AlarmHelper>& alarms);

        /// Copy constructor (copy semantics).
        EphemerisObject(const EphemerisObject& other);
 
        /// Assignment (copy semantics).
        EphemerisObject& operator=(const EphemerisObject& other);

        /// The destructor must be virtual
        virtual ~EphemerisObject();

        // --------------------- C++ public functions ---------------------
 
        /// See the documentation for the TrackableObject base class.
        virtual void getPointing(const ACS::Time& time,
                                 Control::EquatorialDirection& target,
                                 Control::Offset& equatorial,
                                 Control::EquatorialDirection& equatorialRADec,
                                 Control::Offset& horizon,
                                 Control::HorizonDirection& horizonAzEl,
                                 Control::HorizonDirection& commanded,
                                 Control::HorizonDirection& commandedRate);

        /// Returns the RA and Dec associated with the supplied Az & El. The
        /// supplied az and el, at the specified time should be obtained from
        /// the ACU and the inverse transformation done.
        virtual void getActualRADec(double az, double el, ACS::Time time,
                                    double& ra, double& dec);

        /// Returns the commanded RA and Dec. This just returns the latest
        /// internal values and the az, el and time arguments are ignored.
        virtual void getCommandedRADec(double az, double el, ACS::Time time,
                                       double& ra, double& dec);

        /// returns the refraction coefficients used by this object.
        virtual void getRefractionCoefficients(double& a, double& b);

        /// See TrackableObject.h for a description of this function.
        virtual double timeToRise(const double& elLimit);

        /// See TrackableObject.h for a description of this function.
        virtual double timeToSet(const double& elLimit);

    private:
        
        EphemerisInterpolator ei_m;
    }; // end EphemerisObject class
} // end Control namespace
#endif // EPHEMERISOBJECT_H
