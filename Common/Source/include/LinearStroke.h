#ifndef LINEARSTROKE_H
#define LINEARSTROKE_H
// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2007
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

// Base class(es)
#include <OffsetStroke.h>

// Forward declarations for classes that this component uses

//includes for data members
#include <acscommonC.h> // for ACS::Time

namespace Control {
  class LinearStroke: public OffsetStroke
  {
  public:
    // ------------------- Constructor & Destructor -------------------
    // 
    LinearStroke(const ACS::Time& startTime, 
                 const double& longOffset, const double& latOffset,
                 const double& longVel, const double& latVel);

    // Copy constructor (copy semantics).
    LinearStroke(const LinearStroke& other);
    
    // Assignment (copy semantics).
    LinearStroke& operator=(const LinearStroke& other);

    // The destructor must be virtual
    virtual ~LinearStroke();

    // --------------------- C++ public functions ---------------------
    
    // Modify the input position offseting the latitude and longitude by a
    // fixed amount
    virtual void 
    applyPattern(const ACS::Time& time, 
                 const double& currentLong, const double& currentLat, 
                 const double& currentLongVel, const double& currentLatVel, 
                 double& newLong, double& newLat,
                 double& newLongVel, double& newLatVel) const;

    // Returns the instantaneous offset at the specified time
    virtual void getOffset(const ACS::Time time, 
                           double& longOffset, double& latOffset) const;

    static void offsetVelocity(double curLong, double curLat, 
                               double curLongVel, double curLatVel, 
                               double xOff, double yOff, double zOff,
                               double& xVel, double& yVel, double& zVel); 

  private:
    double longVel_m;
    double latVel_m;
  }; // end LinearStroke class
} // end Control namespace
#endif // LINEARSTROKE_H
