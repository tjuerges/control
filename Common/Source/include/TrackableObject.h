// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007 - 2010
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

#ifndef TRACKABLEOBJECT_H
#define TRACKABLEOBJECT_H
// Base class(es)
#include <loggingLoggable.h>

// Forward declarations for classes that this component uses

//includes for data members
#include <acscommonC.h> // for ACS::Time
#include <vector> // for std::vector
#include <CurrentWeatherC.h> // for Control::CurrentWeather
#include <casacore/measures/Measures/MPosition.h> // for casa::MPosition
#include <boost/shared_ptr.hpp> // for boost::shared_ptr
#include <Pattern.h>
#include <SourceS.h> // for Control::Offset
#include <ControlDataInterfacesS.h> // for Control::HorizonDirection

namespace Control {
    class AlarmHelper;
}

class EpochHelper;

namespace Control {
    class TrackableObject : public Logging::Loggable { 
    public:
	// ------------------- Constructor & Destructor -------------------
	
	/// The start time is "Now" 
        TrackableObject(Logging::Logger::LoggerSmartPtr logger,
                        boost::shared_ptr<AlarmHelper>& alarms);
	
	/// this defines a start time as specified by the user
	TrackableObject(const ACS::Time& startTime,
			Logging::Logger::LoggerSmartPtr logger,
                        boost::shared_ptr<AlarmHelper>& alarms);
	
	// Copy constructor (copy semantics).
	TrackableObject(const TrackableObject& other);
	
	/// Assignment (copy semantics).
	TrackableObject& operator=(const TrackableObject& other);
	
	/// The destructor must be virtual
	virtual ~TrackableObject();
	
	// --------------------- C++ public functions ---------------------
	
	/// Returns the commanded posistions & offsets and rates at the
	/// specified time. This is used by the TrajectoryPlanner thread to get
	/// the data it needs to send to the mount component and the command
	/// information it needs to provide to the mount GUI and accumulate in
	/// the ASDM. See the CCL documentation for getPointingData function
	/// more precise meanings of eavh field.
	virtual void getPointing(const ACS::Time& time,
				 Control::EquatorialDirection& target,
				 Control::Offset& equatorial,
				 Control::EquatorialDirection& equatorialRADec,
				 Control::Offset& horizon,
				 Control::HorizonDirection& horizonAzEl,
				 Control::HorizonDirection& commanded,
				 Control::HorizonDirection& commandedRate) = 0;
	
	/// Returns the apparent azimuth and elevation of the object at the
	/// specified time.
	void getAzEl(const ACS::Time& time, double& az, double& el);
	
	/// Returns the RA and Dec associated with the supplied Az & El. The
	/// supplied az and el should be values obtained from the ACU.
	virtual void getActualRADec(double az, double el, ACS::Time time,
				    double& ra, double& dec) = 0;
	
	/// The supplied az and el should be commanded values but may not be
	/// used if the subclass already knows the commanded RA and Dec e.g.,
	/// EquatorialObject
	virtual void getCommandedRADec(double az, double el, ACS::Time time,
				       double& ra, double& dec) = 0;
	
	
	/// When the mount controller should switch to this object.
	virtual ACS::Time getStartTime() const;
	
	/// Returns true if the start time is immediately (now)
	virtual bool startNow() const;
	
	/// Assigns a weather station that may be used by this class. Note the
	/// caller of this function is responsible for ensuring the supplied
	/// reference is valid over the lifecycle of this object. If no weather
	/// station is supplied then default weather parameters will be used.
	virtual void setWeatherStation(const Control::CurrentWeather_ptr& ws);
	
	// returns true is this class has a weather station it can use.
	virtual bool hasWeatherStation() const;
	
 	/// Sets the antenna parameters that will be used by this class. If no
	/// antenna parameters are supplied then a default value will be
	/// used. The default is fine (because it is not used) if you are only
	/// using Az/El positions. The pad name is used to get the most
	/// relevant weather data.
        void setAntenna(const casa::MPosition& antennaLocation, 
                        const std::string& padName="A1");

	/// returns the refraction coefficients used by this object.
	virtual void getRefractionCoefficients(double& a, double& b) = 0;
	
	/// A function that initializes the SLA parameters (amprms, aoprms and
	/// azCorrection). This uses the weather data, antenna location data,
	/// the IERS data and the current time. The amprms *must* be of length
	/// 21 and the aoprms *must* be of length 14. Returns false if the
	/// parameters cannot be initialized. If verbose is false then some log
	/// messages are suppressed. Use this for transient trackable objects.
	virtual bool initializeSlaParams(bool verbose = true);
	
	/// returns True if the SLA parameters are not initialized.
	virtual bool isSlaUnInitialized() const;
	
	/// Get a reference to the equatorial pattern object. If you use this
	/// you *must* ensure that the returned Pattern is not used after this
	/// TrackableObject is destroyed.
	Pattern& equatorialPattern();
	
	/// Get a reference to the horizon pattern object. If you use this you
	/// *must* ensure that the returned Pattern is not used after this
	/// TrackableObject is destroyed.
	Pattern& horizonPattern();
	
        /// Returns the time, in seconds, before this object will rise. Returns
        /// a negative number if this object never rises and zero if this
        /// source is already up. The elevation limit, in radians, of the mount
        /// must be specified.
        virtual double timeToRise(const double& elLimit) = 0;

        /// Returns the time, in seconds, before this object will set. Returns
        /// a negative number if this object is already set and a large
        /// positive number if this object never sets. The elevation limit, in
        /// radians, of the mount must be specified.
        virtual double timeToSet(const double& elLimit) = 0;

    protected:
	// These protected data members are the SLALIB parameters. These
	// vectors are zero length until they are initialized using the
	// initializeSlaParams functions. As part of this initialisation the
	// weather station and antenna position data members (below) are used.
	
	// These are the source dependent SLALIB terms.
	std::vector<double> amprms_m;
	
	// These are the source independent SLALIB terms
	std::vector<double> aoprms_m; 
	
	// Polar motion correction to add to azimuth. This is also used by
	// SLALIB
	double azCorrection_m; 
	
	// The observing frequency for this object. Used when the SLA
	// parameters are initialized and is necessary to properly correct for
	// refraction.
	double obsFreq_m;
	
	// Two alternative inplementations, for use by sub-classes, of the
	// getRefractionCoefficients function.
	virtual void getNullRefractionCoefficients(double& a, double& b);
	virtual void getRealRefractionCoefficients(double& a, double& b);
	
	// Calculate the azimuth/elevation and velocities given the observed
	// RA/Dec, RA/Dec velocities, siderial time & geodetic latitude. All
	// angles are in radians and velocities are in radianbs/.sec. No range
	// checking is done.
	void altAzVelocities(double ra, double dec, 
			     double raRate, double decRate, 
			     double siderealTime, double latitude, 
			     double& az, double& el, 
			     double& azRate, double& elRate);
	
        // Calculate the hour angle limits for this source given the elevation
        // limits and its declination.
        double calcHALimit(const double& elLimit, const double& dec);

        // Calculate the observed RA/Dec given the J2000 position and the
        // current time. This includes any equatorial offsets.
        void observedRADec(ACS::Time time, 
                           double ra2000, double dec2000,
                           double pmRA, double pmDec, double parrallax,
                           double& raObs, double& decObs,
                           double& raRate, double& decRate, 
                           double& raOffset, double& decOffset);

    private:
	// When this object becomes "active".
	ACS::Time startTime_m;
	
	// A sequence of offsets or strokes in equatorial coordinates that are
	// added to the position.
	Pattern equatorial_m;
	
	// A sequence of offsets or strokes in horizon coordinates that are
	// added to the position.
	Pattern horizon_m;
	
	// A reference to a weather station component. By default its
	// Control::WeatherStation::_nil()
	Control::CurrentWeather_ptr ws_m;
	
	// The position of the antenna
	casa::MPosition antLoc_m;
	
	// The padName this antenna is on antenna. Used to get the most
	// relevant data weather data.
	std::string padName_m;
	
        // An object to facilitate the sending of alarms
        boost::shared_ptr<AlarmHelper> alarms_m;
        
	// This functions warns the user if a weather parameter is out of
	// date. It logs a message, rasies and alarms, and returns false if it
	// is too old.
	bool checkAgeIsOK(const ACS::Time& measurementTime, 
                          EpochHelper& timeNow, const string& dataName);

    }; // end TrackableObject class
} // end Control namespace

#endif // TRACKABLEOBJECT_H
