// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007 - 2010
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#ifndef PLANETARYOBJECT_H
#define PLANETARYOBJECT_H

// Base class(es)
#include "TrackableObject.h" // for Control::TrackableObject

// Forward declarations for classes that this component uses

//includes for data members
#include <casacore/measures/Measures/MDirection.h> // for casa::MDirection
#include <casacore/measures/Measures/MeasConvert.h> // for casa::MeasConvert
#include <casacore/measures/Measures/MeasFrame.h> // for casa::MeasFrame

namespace Control {
    class PlanetaryObject: public TrackableObject {
    public:
        // ------------------- Constructor & Destructor -------------------

        /// Use the specified RA & Dec. The start time is "Now" 
        PlanetaryObject(const casa::MDirection::Types& planet,
                        const casa::MPosition& antennaLocation,
                        const std::string& antennaName,
                        const std::string& padName,
                        const Control::CurrentWeather_ptr& ws,
                        const double& obsFreq,
                        Logging::Logger::LoggerSmartPtr logger,
                        boost::shared_ptr<AlarmHelper>& alarms,
                        bool verbose);

        /// Use the specified RA & Dec at the specified start time.
        PlanetaryObject(const ACS::Time& startTime, 
                        const casa::MDirection::Types& planet,
                        const casa::MPosition& antennaLocation,
                        const std::string& antennaName,
                        const std::string& padName,
                        const Control::CurrentWeather_ptr& ws,
                        const double& obsFreq,
                        Logging::Logger::LoggerSmartPtr logger,
                        boost::shared_ptr<AlarmHelper>& alarms,
                        bool verbose);

        /// Use the specified planet, a start time of now and default weather
        /// parameters. This is primarily for computations where a precise
        /// Az/El is not required.
        PlanetaryObject(const casa::MDirection::Types& planet,
                        const casa::MPosition& antennaLocation,
                        Logging::Logger::LoggerSmartPtr logger,
                        boost::shared_ptr<AlarmHelper>& alarms);

        /// Copy constructor (copy semantics).
        PlanetaryObject(const PlanetaryObject& other);
 
        /// Assignment (copy semantics).
        PlanetaryObject& operator=(const PlanetaryObject& other);

        /// The destructor must be virtual
        virtual ~PlanetaryObject();

        // --------------------- C++ public functions ---------------------
 
        /// See the documentation for the TrackableObject base class.
        virtual void getPointing(const ACS::Time& time,
                                 Control::EquatorialDirection& target,
                                 Control::Offset& equatorial,
                                 Control::EquatorialDirection& equatorialRADec,
                                 Control::Offset& horizon,
                                 Control::HorizonDirection& horizonAzEl,
                                 Control::HorizonDirection& commanded,
                                 Control::HorizonDirection& commandedRate);

        /// Returns the RA and Dec associated with the supplied Az & El. The
        /// supplied az and el, at the specified time should be obtained from
        /// the ACU and the inverse transformation done.
        virtual void getActualRADec(double az, double el, ACS::Time time,
                                    double& ra, double& dec);

        /// Returns the commanded RA and Dec. This just returns the latest
        /// internal values and the az, el and time arguments are ignored.
        virtual void getCommandedRADec(double az, double el, ACS::Time time,
                                       double& ra, double& dec);

        /// returns the refraction coefficients used by this object.
        virtual void getRefractionCoefficients(double& a, double& b);

        /// Returns the time, in seconds, before this object will rise. Returns
        /// a negative number if this object never rises and zero if this
        /// source is already up. The elevation limit, in radians, of the mount
        /// must be specified.
        virtual double timeToRise(const double& elLimit);

        /// Returns the time, in seconds, before this object will set. Returns
        /// a negative number if this object is already set and a large
        /// positive number if this object never sets. The elevation limit, in
        /// radians, of the mount must be specified.
        virtual double timeToSet(const double& elLimit);

        /// Convert a planet name (as a string) to the relevant CASA enum. The
        /// string is case-insensitive. Throw an IllegalParameterErrorEx
        /// excpetion if the supplied string is not a planet name. For this
        /// application, the Earth is *not* a planet and the Sun, Moon and
        /// Pluto are!
        /// \exception ControlExceptions::IllegalParameterErrorEx
        static casa::MDirection::Types 
        planetNameToEnum(const string& planetName);

    private:

        // Returns the "observed" RA/Dec (plus rates) at the specified
        // time. Equatorial offsets and the apparent position are also returned.
        // TODO. See if we can merge this function with a similar function in
        // the base class
        void getObservedPosition(ACS::Time time, double& raOb, double& decOb, 
                                 double& raObRate, double& decObRate, 
                                 double& raApp, double& decApp, 
                                 double& lngOffset, double& latOffset,
                                 double& raOffset, double& decOffset);
        casa::MeasFrame mf_m;
        casa::MeasConvert<casa::MDirection> mc_m;
        casa::MDirection md_m;
    }; // end PlanetaryObject class
} // end Control namespace

#endif // PLANETARYOBJECT_H
