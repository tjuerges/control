// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2007 - 2009
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#ifndef OFFSETSTROKE_H
#define OFFSETSTROKE_H

// Base class(es)
#include "Stroke.h" // for Stroke

// Forward declarations for classes that this component uses

//includes for data members
#include <casacore/casa/Quanta/MVDirection.h> // for casa::MVDirection

namespace Control {
    class OffsetStroke: public Stroke {
    public:
	// ------------------- Constructor & Destructor -------------------
	// 
	OffsetStroke(const double& longOffset, const double& latOffset);
	
	OffsetStroke(const ACS::Time& startTime, 
		     const double& longOffset, const double& latOffset);
	
	// Copy constructor (copy semantics).
	OffsetStroke(const OffsetStroke& other);
	
	// Assignment (copy semantics).
	OffsetStroke& operator=(const OffsetStroke& other);
	
	// The destructor must be virtual
	virtual ~OffsetStroke();
	
	// --------------------- C++ public functions ---------------------
	
	// Modify the input position offseting the latitude and longitude by a
	// fixed amount
	virtual void 
	applyPattern(const ACS::Time& time, 
		     const double& currentLong, const double& currentLat, 
		     const double& currentLongVel, const double& currentLatVel,
		     double& newLong, double& newLat,
		     double& newLongVel, double& newLatVel) const;
	
	// Returns the instantaneous offset at the specified time.
	virtual void getOffset(const ACS::Time& time,
			       double& longOffset, double& latOffset) const;
	
	// Specify a value for the elevation reference. this value is
	// subtracted from the latitude offset in the getOffset function. Its
	// only non-zero when doing AzElOffsets.
	virtual void setElRef(double elRef);
	
	// Get the elevation reference. 
	virtual double getElRef() const;
	
    private:
	casa::MVDirection offset_m;
	double elRef_m;
    }; // end OffsetStroke class
} // end Control namespace
#endif // OFFSETSTROKE_H
