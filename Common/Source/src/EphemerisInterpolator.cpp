// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2009, 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "EphemerisInterpolator.h"
#include <acstimeTimeUtil.h> // for getTimeStamp
#include <ControlExceptions.h> 
#include <TETimeUtil.h> 
#include <string> 
#include <iostream> 
#include <iomanip> 

// shortcuts to avoid having to clutter the code with namespace qualifiers
using Control::EphemerisInterpolator;
using std::string;
using std::ostringstream;
using Control::EphemerisRow;
using std::vector;

EphemerisInterpolator::EphemerisInterpolator()
    :eph_m()
{  
}

EphemerisInterpolator::EphemerisInterpolator(const Control::Ephemeris& ephemeris)
    :eph_m(ephemeris.length())
{
    const unsigned int nRow = ephemeris.length();
    for (unsigned int i = 0; i < nRow; i++) {
        eph_m[i] = ephemeris[i];
    }
    // TODO Sort the Ephemeris and check for rows with duplicate times
}

EphemerisInterpolator::EphemerisInterpolator(const EphemerisInterpolator& other)
    :eph_m(other.eph_m)
{  
}

EphemerisInterpolator& EphemerisInterpolator::operator=(const EphemerisInterpolator& other) {
    if (this != &other) {
        eph_m = other.eph_m;
    }
    return *this;
}

void EphemerisInterpolator::getPosition(const ACS::Time& time, 
        double& ra, double& dec) {
    double rho;
    getPosition(time, ra, dec, rho);
}

void EphemerisInterpolator::getPosition(const ACS::Time& time, 
        double& ra, double& dec, double& rho) {
   
    if (eph_m.size() == 0) {
        ostringstream message;
        string msg = "No ephemeris has been loaded.";
        ControlExceptions::IllegalParameterErrorExImpl
            ex(__FILE__, __LINE__, __func__);
        ex.addData("Detail", msg);
        ex.log();
        throw ex;
    }
    if (eph_m.size() == 1) {
        ra = eph_m[1].ra;
        dec = eph_m[1].dec;
        rho = eph_m[1].rho;
        return;
    }

    // TODO check is data is too far off the end of the ephemeris. Too far is one hour
    const ACS::Time firstTime = eph_m[0].time;
    const double tooFarInSecs = 60*60;
    if (firstTime > time) {
        if ((firstTime - time)*100E-9 > tooFarInSecs) {
            ostringstream msg;
            msg << "Requested a position at time "
                << TETimeUtil::toTimeString(time)
                << " that is more than " << tooFarInSecs 
                << " seconds before the start of the ephemeris (which begins at ";
            msg << TETimeUtil::toTimeString(firstTime) << ").";
            ControlExceptions::IllegalParameterErrorExImpl
                ex(__FILE__, __LINE__, __func__);
            ex.addData("Detail", msg.str());
            ex.log();
            throw ex.getIllegalParameterErrorEx();
        }
    }
    const ACS::Time lastTime = eph_m[eph_m.size()-1].time;
    if (time > lastTime) {
        if ((time - lastTime)*100E-9 > tooFarInSecs) {
            ostringstream msg;
            msg << "Requested a position at time "
                << TETimeUtil::toTimeString(time)
                << " that is more than " << tooFarInSecs 
                << " seconds after the end of the ephemeris (which ends at ";
            msg << TETimeUtil::toTimeString(lastTime) << ").";
            ControlExceptions::IllegalParameterErrorExImpl
                ex(__FILE__, __LINE__, __func__);
            ex.addData("Detail", msg.str());
            ex.log();
            throw ex.getIllegalParameterErrorEx();
        }
    }
    // The lwrBnd variable points to the rowof the ephemeris that has the same
    // timestamp as the requested timestamp. If there is no element that has
    // the requested timestamp it points to the element with the next larger
    // timestamp.
    vector<EphemerisRow>::const_iterator lwrBnd =
        lower_bound(eph_m.begin(), eph_m.end(), time,
                    EphemerisInterpolator::CompareRows());

    // Get the data, at the endpoints, that will be used for the linear
    // interpolation
    double  ra0,  ra1;
    double  dec0, dec1;
    double  rho0, rho1;
    ACS::Time t0, t1;

    // Check if the requested timestamp is out of bounds.
    if (lwrBnd == eph_m.end()) {
        lwrBnd--;
        ra1  = lwrBnd->ra;
        dec1 = lwrBnd->dec;
        rho1 = lwrBnd->rho;
        t1   = lwrBnd->time;
        // We know there are at least two row in the ephemeris so this always
        // works
        lwrBnd--;
        ra0  = lwrBnd->ra;
        dec0 = lwrBnd->dec;
        rho0 = lwrBnd->rho;
        t0   = lwrBnd->time;
    }
 
    if (lwrBnd == eph_m.begin()) {
        ra0  = lwrBnd->ra;
        dec0 = lwrBnd->dec;
        rho0 = lwrBnd->rho;
        t0   = lwrBnd->time;
        // We know there are at least two row in the ephemeris so this always
        // works
        lwrBnd++;
        ra1  = lwrBnd->ra;
        dec1 = lwrBnd->dec;
        rho1 = lwrBnd->rho;
        t1   = lwrBnd->time;
    } else {
        ra1  = lwrBnd->ra;
        dec1 = lwrBnd->dec;
        rho1 = lwrBnd->rho;
        t1   = lwrBnd->time;
        // We know the iterator is not at the beginning (because we tested for
        // this condition earlier) so this always works.
        lwrBnd--;
        ra0  = lwrBnd->ra;
        dec0 = lwrBnd->dec;
        rho0 = lwrBnd->rho;
        t0   = lwrBnd->time;
    }
    // Linear interpolate between the two nearest positions
 
    // I use long doubles to avoid precision loss when subtracting two nearly
    // equal big numbers (like the timestamps).
    const long double dt = static_cast<long double>(t1 - time)/
        static_cast<long double>(t1 - t0);
 
    ra  = (ra0  - ra1)  * dt + ra1;
    dec = (dec0 - dec1) * dt + dec1;
    rho = (rho0 - rho1) * dt + rho1;
}

