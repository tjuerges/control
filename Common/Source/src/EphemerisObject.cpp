// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007 - 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#include "EphemerisObject.h"
#include <slalib.h> // for sla* functions
#include <acstimeEpochHelper.h> // for EpochHelper

// shortcuts to avoid having to clutter the code with namespace qualifiers
using Control::EphemerisObject;
using Control::AlarmHelper;
using Logging::Logger;
using boost::shared_ptr;
using std::abs;

// TODO. Get real values for all of these
const long array2TAI = 0; // from the CRD

EphemerisObject::
EphemerisObject(const Control::Ephemeris& userEphemeris,
                const casa::MPosition& antennaLocation,
                const string& antennaName, const string& padName,
                const Control::CurrentWeather_ptr& ws,
                const double& obsFreq,
                Logger::LoggerSmartPtr logger,
                shared_ptr<AlarmHelper>& alarms)
    :TrackableObject(logger, alarms),
     ei_m(userEphemeris)
{
    setAntenna(antennaLocation, padName);
    setWeatherStation(ws);
    obsFreq_m = obsFreq;
    initializeSlaParams();
}

EphemerisObject::
EphemerisObject(const ACS::Time& startTime, 
                const Control::Ephemeris& userEphemeris,
                const casa::MPosition& antennaLocation,
                const string& antennaName, const string& padName,
                const Control::CurrentWeather_ptr& ws,
                const double& obsFreq,
                Logger::LoggerSmartPtr logger,
                shared_ptr<AlarmHelper>& alarms)
    :TrackableObject(startTime, logger, alarms),
     ei_m(userEphemeris)
{
    setAntenna(antennaLocation, padName);
    setWeatherStation(ws);
    obsFreq_m = obsFreq;
    initializeSlaParams();
}

EphemerisObject::
EphemerisObject(const Control::Ephemeris& userEphemeris,
                const casa::MPosition& antennaLocation,
                Logger::LoggerSmartPtr logger,
                shared_ptr<AlarmHelper>& alarms)
    :TrackableObject(logger, alarms),
     ei_m(userEphemeris)
{
    setAntenna(antennaLocation);
    initializeSlaParams(false);
}

EphemerisObject::EphemerisObject(const EphemerisObject& other)
    :TrackableObject(other),
     ei_m(other.ei_m)
{
}

EphemerisObject& EphemerisObject::operator=(const EphemerisObject& other) {
    TrackableObject::operator=(other);
    if (this != &other) {
        ei_m = other.ei_m;
    }
    return *this;
}

EphemerisObject::~EphemerisObject()
{
}

void EphemerisObject::
getPointing(const ACS::Time& time,
            Control::EquatorialDirection& target,
            Control::Offset& equatorial,
            Control::EquatorialDirection& equatorialRADec,
            Control::Offset& horizon,
            Control::HorizonDirection& horizonAzEl,
            Control::HorizonDirection& commanded,
            Control::HorizonDirection& commandedRate) {

    // Get the J2000 position
    ei_m.getPosition(time, target.ra, target.dec);
    
    // Calculate the observed position
    equatorialPattern().getOffset(time, equatorial.lng, equatorial.lat);
    double ra, dec, raRatePattern, decRatePattern;
    observedRADec(time, target.ra, target.dec, 0.0, 0.0, 0.0,
                  ra, dec, raRatePattern, decRatePattern, 
                  equatorialRADec.ra, equatorialRADec.dec);

    // Calculate the az/el and the az/el velocities.
    // TODO What follows assumes that the specified object is moving with the
    // stars. This is not true, particularly for nearby objects and
    // satellites. It remains to be determined if this additional velocity is
    // significant.
    double az, el, azRate, elRate;
    altAzVelocities(ra, dec, raRatePattern, decRatePattern, 
                    aoprms_m[13], aoprms_m[0], 
                    az, el, azRate, elRate);

    az += azCorrection_m;

    // Put the azimuth between +/-180 to prevent problems calculating the slew
    // time. The AzCableWrap class will adjust this if necessary.
    az = slaDrange(az);

    // Now apply the horizon pattern
    horizonPattern().applyPattern(time, az, el, azRate, elRate, 
                                  commanded.az, commanded.el, 
                                  commandedRate.az, commandedRate.el);

    horizonPattern().getOffset(time, horizon.lng, horizon.lat);
    horizonAzEl.az = slaDrange(commanded.az - az);
    horizonAzEl.el = slaDrange(commanded.el - el);
}

void EphemerisObject::
getActualRADec(double az, double el, ACS::Time time,
               double& ra, double& dec) {
    const double mjd = EpochHelper(time).toUTCdate(array2TAI, 0);
    slaAoppat(mjd, &aoprms_m[0]);
    double apparentRA, apparentDec;

    slaOapqk("A", az, M_PI_2 - el, &aoprms_m[0], &apparentRA, &apparentDec);
    slaAmpqk(apparentRA, apparentDec, &amprms_m[0], &ra, &dec);
}

void EphemerisObject::
getCommandedRADec(double az, double el, ACS::Time time, 
                  double& ra, double& dec) {
    ei_m.getPosition(time, ra, dec);
}

void EphemerisObject::getRefractionCoefficients(double& a, double& b) {
    TrackableObject::getRealRefractionCoefficients(a, b);
}

double EphemerisObject::timeToRise(const double& elLimit) {
    // The current time in UTC.
    const ACS::Time now = ::getTimeStamp() - array2TAI;
    // Get the J2000 position
    double ra2000, dec2000;
    ei_m.getPosition(now, ra2000, dec2000);
    
    // Calculate the observed position
    double ra, dec;
    // These variables are not used here
    double raRate, decRate, raOffset, decOffset;
    observedRADec(now, ra2000, dec2000, 0.0, 0.0, 0.0,
                  ra, dec, raRate, decRate, raOffset, decOffset);

    // Now that we know the observed declination check if its outside the range
    // of sources which can rise or set. This is essential as the trigonometric
    // calculation that follows will return NaN if we have a source whose
    // declination outside these limits.
    if (dec > M_PI_2 + aoprms_m[0] - elLimit) {
        return -1; // never rises
    }
    if (dec < -M_PI_2 - aoprms_m[0] - elLimit) {
        return 0.0; // never sets
    }

    const double haLimit = calcHALimit(elLimit, dec);

    // bring HA into the range [-pi to +pi]
    double ha = slaDrange(aoprms_m[13] - ra);
    if (abs(ha) < haLimit) return 0.0; // source is up
    // bring ha into the range [-2pi to 0]
    if (ha > 0) ha -= 2 * M_PI;
    return (-haLimit - ha)/(2*M_PI)* 24*60*60;
}

double EphemerisObject::timeToSet(const double& elLimit) {
    // The current time in UTC.
    const ACS::Time now = ::getTimeStamp() - array2TAI;
    // Get the J2000 position
    double ra2000, dec2000;
    ei_m.getPosition(now, ra2000, dec2000);
    
    // Calculate the observed position
    double ra, dec;
    // These variables are not used here
    double raRate, decRate, raOffset, decOffset;
    observedRADec(now, ra2000, dec2000, 0.0, 0.0, 0.0,
                  ra, dec, raRate, decRate, raOffset, decOffset);

    // Now that we know the observed declination check if its outside the range
    // of sources which can rise or set. This is essential as the trigonometric
    // calculation that follows will return NaN if we have a source whose
    // declination outside these limits.
    if (dec > M_PI_2 + aoprms_m[0] - elLimit) {
        return -1; // never rises
    }
    if (dec < -M_PI_2 - aoprms_m[0] - elLimit) {
        return std::numeric_limits<double>::max(); // never sets
    }

    const double haLimit = calcHALimit(elLimit, dec);

    // bring HA into the range [-pi to +pi]
    const double ha = slaDrange(aoprms_m[13] - ra);
    if (abs(ha) > haLimit) return -1; // source has set

    return (haLimit - ha)/(2*M_PI)* 24*60*60;
}

