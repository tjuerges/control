# @(#) $Id$
#
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2010
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#
# additional include and library search paths
USER_INC = -I$(CASA_ROOT)/include 
USER_INC += -I$(CASA_ROOT)/include/casacore # Needed to work around a bug in CASA-3

# Needed to suppress error messages from acsMakeCheckUnresolvedSymbols
export LD_LIBRARY_PATH := $(CASA_ROOT)/lib:$(LD_LIBRARY_PATH)

#
# Includes (.h) files (public only)
# ---------------------------------
INCLUDES = HorizonObject.h TrackableObject.h EquatorialObject.h \
           PlanetaryObject.h EphemerisObject.h EphemerisInterpolator.h \
           Stroke.h OffsetStroke.h LinearStroke.h Pattern.h \
           SourceAlarmTypes.h

#
# Libraries (public and local)
# ----------------------------
LIBRARIES = Source

Source_OBJECTS = TrackableObject HorizonObject EquatorialObject \
                 PlanetaryObject EphemerisObject EphemerisInterpolator \
                 Stroke OffsetStroke LinearStroke Pattern

Source_LIBS = TETimeUtil ControlExceptions sla
Source_LDFLAGS = -L$(CASA_ROOT)/lib \
		 -Xlinker --rpath -Xlinker $(CASA_ROOT)/lib \
                 -lcasacore -lg2c

#
# Python stuff (public and local)
# ----------------------------
PY_PACKAGES        =  CCL

# 
# IDL Files and flags
# 
IDL_FILES = Source
SourceStubs_LIBS = acstimeStubs

#
# other files to be installed
#----------------------------
INSTALL_FILES = ../config/HorizonsEphemerisIo.txt \
                ../config/HorizonsEphemerisEuropa.txt \
                ../config/HorizonsEphemerisGanymede.txt \
                ../config/HorizonsEphemerisCallisto.txt \
		../config/HorizonsEphemerisJuno.txt \
		../config/HorizonsEphemerisCeres.txt \
		../config/HorizonsEphemerisPallas.txt \
		../config/HorizonsEphemerisTitan.txt \
		../config/HorizonsEphemerisVesta.txt

#
# list of all possible C-sources (used to create automatic dependencies)
# ------------------------------
CSOURCENAMES = \
	$(foreach exe, $(EXECUTABLES) $(EXECUTABLES_L), $($(exe)_OBJECTS)) \
	$(foreach rtos, $(RTAI_MODULES) , $($(rtos)_OBJECTS)) \
	$(foreach lib, $(LIBRARIES) $(LIBRARIES_L), $($(lib)_OBJECTS))

#
#>>>>> END OF standard rules

#
# INCLUDE STANDARDS
# -----------------

MAKEDIRTMP := $(shell searchFile include/acsMakefile)
ifneq ($(MAKEDIRTMP),\#error\#)
   MAKEDIR := $(MAKEDIRTMP)/include
   include $(MAKEDIR)/acsMakefile
endif

#
# TARGETS
# -------
all:	do_all
	@echo " . . . 'all' done" 

clean : clean_all 
	@echo " . . . clean done"

clean_dist : clean_all clean_dist_all 
	@echo " . . . clean_dist done"

man   : do_man 
	@echo " . . . man page(s) done"

install : install_all
	@echo " . . . installation done"
