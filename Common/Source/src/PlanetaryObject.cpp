// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007 - 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#include "PlanetaryObject.h"
#include <ControlExceptions.h> // for IllegalParameterErrorExImpl
#include <LogToAudience.h> // for LOG_TO_{DEVELOPER,OPERATOR}
#include <acstimeEpochHelper.h> // for EpochHelper
#include <acstimeTimeUtil.h> // for TimeUtil
#include <slalib.h> // for sla* functions
#include <casacore/measures/Measures/MEpoch.h> // for casa::MEpoch
#include <casacore/casa/Exceptions/Error.h> // for casa::AipsError
#include <casacore/measures/Measures/MCDirection.h> // Needed to work around a bug in CASA-3.1
#include <cmath> // for log, sin, cos etc.

// shortcuts to avoid having to clutter the code with namespace qualifiers
using Control::PlanetaryObject;
using Control::AlarmHelper;
using Logging::Logger;
using std::abs;
using boost::shared_ptr;

// TODO. Get these values from a central place 
const long array2TAI = 0; // from the CRD

PlanetaryObject::PlanetaryObject(const casa::MDirection::Types& planet,
                                 const casa::MPosition& antennaLocation,
                                 const string& antennaName,
                                 const string& padName,
                                 const Control::CurrentWeather_ptr& ws,
                                 const double& obsFreq,
                                 Logger::LoggerSmartPtr logger,
                                 shared_ptr<AlarmHelper>& alarms,
                                 bool verbose)
    :TrackableObject(logger, alarms),
     mf_m(antennaLocation),
     mc_m(),
     md_m()
{
    setAntenna(antennaLocation, padName);
    setWeatherStation(ws);
    obsFreq_m = obsFreq;
    initializeSlaParams(verbose);
    // Finish setting up the reference frame by setting an epoch
    const double mjd = EpochHelper(::getTimeStamp()).toUTCdate(0, 0);
    mf_m.set(casa::MEpoch(casa::MVEpoch(mjd), casa::MEpoch::UTC));
    
    // Now create a conversion object for the specified planet to J2000
    mc_m = casa::MeasConvert<casa::MDirection>
        (planet,casa::MeasRef<casa::MDirection>(casa::MDirection::TOPO, mf_m));

    // Do an initial unnecessary conversion as the first one takes 50-500 times
    // longer that the rest.
    try {
        md_m = mc_m();
    } catch (casa::AipsError& ex) { // This should be an alarm
        string msg = 
            "Cannot determine the coordinates (RA,Dec) of the planet.";
        msg += " Underlying error message is:\n";
        msg += ex.what();
        LOG_TO_DEVELOPER(LM_ERROR, msg);
    }
}

PlanetaryObject::PlanetaryObject(const ACS::Time& startTime, 
                                 const casa::MDirection::Types& planet,
                                 const casa::MPosition& antennaLocation,
                                 const string& antennaName,
                                 const string& padName,
                                 const Control::CurrentWeather_ptr& ws,
                                 const double& obsFreq,
                                 Logger::LoggerSmartPtr logger,
                                 shared_ptr<AlarmHelper>& alarms,
                                 bool verbose)
    :TrackableObject(startTime, logger, alarms),
     mf_m(antennaLocation),
     mc_m(),
     md_m()
{
    setAntenna(antennaLocation, padName);
    setWeatherStation(ws);
    obsFreq_m = obsFreq;
    initializeSlaParams(verbose);

    // Finish setting up the reference frame by setting an epoch
    const double mjd = EpochHelper(::getTimeStamp()).toUTCdate(0, 0);
    mf_m.set(casa::MEpoch(casa::MVEpoch(mjd), casa::MEpoch::UTC));
    
    // Now create a conversion object for the specified planet to J2000
    mc_m = casa::MeasConvert<casa::MDirection>
        (planet,casa::MeasRef<casa::MDirection>(casa::MDirection::TOPO, mf_m));

    // Do an initial unnecessary conversion as the first one takes 50-500 times
    // longer that the rest.
    try {
        md_m = mc_m(); 
    } catch (casa::AipsError& ex) { // This should be an alarm
        string msg = 
            "Cannot determine the coordinates (RA,Dec) of the planet.";
        msg += " Underlying error message is:\n";
        msg += ex.what();
        LOG_TO_DEVELOPER(LM_ERROR, msg);
    }
}

PlanetaryObject::PlanetaryObject(const casa::MDirection::Types& planet,
                                 const casa::MPosition& antennaLocation,
                                 Logger::LoggerSmartPtr logger,
                                 shared_ptr<AlarmHelper>& alarms)
    :TrackableObject(logger, alarms),
     mf_m(antennaLocation),
     mc_m(),
     md_m()
{
    setAntenna(antennaLocation);
    initializeSlaParams(false);
    // Finish setting up the reference frame by setting an epoch
    const double mjd = EpochHelper(::getTimeStamp()).toUTCdate(0, 0);
    mf_m.set(casa::MEpoch(casa::MVEpoch(mjd), casa::MEpoch::UTC));
    
    // Now create a conversion object for the specified planet to J2000
    mc_m = casa::MeasConvert<casa::MDirection>
        (planet,casa::MeasRef<casa::MDirection>(casa::MDirection::TOPO, mf_m));

    // Do an initial unnecessary conversion as the first one takes 50-500 times
    // longer that the rest.
    try {
        md_m = mc_m();
    } catch (casa::AipsError& ex) { // This should be an alarm
        string msg = 
            "Cannot determine the coordinates (RA,Dec) of the planet.";
        msg += " Underlying error message is:\n";
        msg += ex.what();
        LOG_TO_DEVELOPER(LM_ERROR, msg);
    }
}

PlanetaryObject::PlanetaryObject(const PlanetaryObject& other)
    :TrackableObject(other),
     mf_m(other.mf_m),
     mc_m(other.mc_m),
     md_m(other.md_m) 
{
}

PlanetaryObject& PlanetaryObject::operator=(const PlanetaryObject& other) {
    TrackableObject::operator=(other);
    if (this != &other) {
        mf_m = other.mf_m;
        mc_m = other.mc_m;
        md_m = other.md_m;
    }
    return *this;
}

PlanetaryObject::~PlanetaryObject() {
}

void PlanetaryObject::
getPointing(const ACS::Time& time,
            Control::EquatorialDirection& target,
            Control::Offset& equatorial,
            Control::EquatorialDirection& equatorialRADec,
            Control::Offset& horizon,
            Control::HorizonDirection& horizonAzEl,
            Control::HorizonDirection& commanded,
            Control::HorizonDirection& commandedRate) {
    double raOb, decOb, raObRate, decObRate;
    getObservedPosition(time, raOb, decOb, raObRate, decObRate, 
                        target.ra, target.dec,
                        equatorial.lng, equatorial.lat,  
                        equatorialRADec.ra, equatorialRADec.dec);

    // Calculate the az/el and the az/el velocities.
    // TODO What follows assumes that the planet is moving with the stars. This
    // is not true, particularly for the sun, moon and nearby planets. It
    // remains to be determined if this additional velocity is significant.
    double az, el, azRate, elRate;
    altAzVelocities(raOb, decOb, raObRate, decObRate, aoprms_m[13], aoprms_m[0], 
                    az, el, azRate, elRate);

    az += azCorrection_m;

    // Put the azimuth between +/-180 to prevent problems calculating the slew
    // time. The AzCableWrap class will adjust this if necessary.
    az = slaDrange(az);

    // Now apply the horizon pattern
    horizonPattern().applyPattern(time, az, el, azRate, elRate, 
                                  commanded.az, commanded.el, 
                                  commandedRate.az, commandedRate.el);

    horizonPattern().getOffset(time, horizon.lng, horizon.lat);
    horizonAzEl.az = slaDrange(commanded.az - az);
    horizonAzEl.el = slaDrange(commanded.el - el);
}

void PlanetaryObject::
getActualRADec(double az, double el, ACS::Time time,
               double& ra, double& dec) {
    const double mjd = EpochHelper(time).toUTCdate(array2TAI, 0);
    slaAoppat(mjd, &aoprms_m[0]);
    double apparentRA, apparentDec;

    az -= azCorrection_m;
    slaOapqk("A", az, M_PI_2 - el, &aoprms_m[0], &apparentRA, &apparentDec);
    slaAmpqk(apparentRA, apparentDec, &amprms_m[0], &ra, &dec);
}

void PlanetaryObject::
getCommandedRADec(double az, double el, ACS::Time time, 
                  double& ra, double& dec) {
    // Sometimes measures returns a negative RA (longitude). The GUI does not
    // like this so normalise it first.
    ra = slaDranrm(md_m.getValue().getLong());
    dec = md_m.getValue().getLat();
}

void PlanetaryObject::getRefractionCoefficients(double& a, double& b) {
    TrackableObject::getRealRefractionCoefficients(a, b);
}

double PlanetaryObject::timeToRise(const double& elLimit) {
    // Calculate the local apparent siderial time
    const ACS::Time time = ::getTimeStamp();
    double ra, dec;
    // the following doubles are not used.
    double raRate, decRate, raApp, decApp;
    double lngOffset, latOffset, raOffset, decOffset;
    getObservedPosition(time, ra, dec, raRate, decRate, raApp, decApp,
                        lngOffset, latOffset, raOffset, decOffset);

    // Now that we know the observed declination check if its outside the range
    // of sources which can rise or set. This is essential as the trigonometric
    // calculation that follows will return NaN if we have a source whose
    // declination outside these limits.
    if (dec > M_PI_2 + aoprms_m[0] - elLimit) {
        return -1; // never rises
    }
    if (dec < -M_PI_2 - aoprms_m[0] - elLimit) {
        return 0.0; // never sets
    }

    const double haLimit = calcHALimit(elLimit, dec);

    // bring HA into the range [-pi to +pi]
    double ha = slaDrange(aoprms_m[13] - ra);
    if (abs(ha) < haLimit) return 0.0; // source is up
    // bring ha into the range [-2pi to 0]
    if (ha > 0) ha -= 2 * M_PI;
    return (-haLimit - ha)/(2*M_PI)* 24*60*60;
}

double PlanetaryObject::timeToSet(const double& elLimit) {
    // Calculate the local apparent siderial time
    const ACS::Time time = ::getTimeStamp();
    double ra, dec;
    // the following doubles are not used.
    double raRate, decRate, raApp, decApp;
    double lngOffset, latOffset, raOffset, decOffset;
    getObservedPosition(time, ra, dec, raRate, decRate, raApp, decApp,
                        lngOffset, latOffset, raOffset, decOffset);

    // Now that we know the observed declination check if its outside the range
    // of sources which can rise or set. This is essential as the trigonometric
    // calculation that follows will return NaN if we have a source whose
    // declination outside these limits.
    if (dec > M_PI_2 + aoprms_m[0] - elLimit) {
        return -1; // never rises
    }
    if (dec < -M_PI_2 - aoprms_m[0] - elLimit) {
        return std::numeric_limits<double>::max(); // never sets
    }

    const double haLimit = calcHALimit(elLimit, dec);

    // bring HA into the range [-pi to +pi]
    const double ha = slaDrange(aoprms_m[13] - ra);
    if (abs(ha) > haLimit) return -1; // source has set

    return (haLimit - ha)/(2*M_PI)* 24*60*60;
}

void PlanetaryObject::
getObservedPosition(ACS::Time time, double& raOb, double& decOb, 
                    double& raObRate, double& decObRate, 
                    double& raApp, double& decApp, 
                    double& lngOffset, double& latOffset,
                    double& raOffset, double& decOffset) {

    // Convert the ACS time to the modified Julian date
    const double mjd = EpochHelper(time).toUTCdate(array2TAI, 0);
    // Set the new time in the frame object and do the conversion
    mf_m.resetEpoch(mjd);
    try {
        md_m = mc_m(); 
    } catch (casa::AipsError& ex) { // This should be an alarm
        string msg = 
            "Cannot determine the coordinates (RA,Dec) of the planet.";
        msg += " Underlying error message is:\n";
        msg += ex.what();
        LOG_TO_DEVELOPER(LM_ERROR, msg);
    }
    raApp = md_m.getValue().getLong();
    decApp = md_m.getValue().getLat();

    double raPattern, decPattern;
    equatorialPattern().getOffset(time, lngOffset, latOffset);
    equatorialPattern().applyPattern(time, raApp, decApp, 0.0, 0.0, 
                                     raPattern, decPattern, 
                                     raObRate, decObRate);
    raOffset = slaDrange(raPattern - raApp);
    decOffset = slaDrange(decPattern - decApp);

    // Calculate the local apparent siderial time
    slaAoppat(mjd, &aoprms_m[0]);

    // The topocentric position produced by measures is precessed to the
    // current epoch. So no need to call the SLA library function that does the
    // J2000 to apparent conversion.

    // Apparent to observed (both Ra/Dec and Az/El).
    double az, hob, zob;  // These are not used anywhere
    slaAopqk(raPattern, decPattern, &aoprms_m[0], &az, &zob, &hob, &decOb, &raOb);
}

casa::MDirection::Types 
PlanetaryObject::planetNameToEnum(const string& planetName) {
    // The default value should never be returned!
    casa::MDirection::Types planet(casa::MDirection::DEFAULT);

    // First convert string to upper case.
    string ucName(planetName);
    for (string::iterator p = ucName.begin(); p !=  ucName.end(); ++p) {
        *p = std::toupper(*p);
    }
  
    // now check against the list of "planets"
    if (ucName == "MERCURY") {
        planet = casa::MDirection::MERCURY;
    } else if (ucName == "VENUS") {
        planet = casa::MDirection::VENUS;
    } else if (ucName == "MARS") {
        planet = casa::MDirection::MARS;
    } else if (ucName == "JUPITER") {
        planet = casa::MDirection::JUPITER;
    } else if (ucName == "SATURN") {
        planet = casa::MDirection::SATURN;
    } else if (ucName == "URANUS") {
        planet = casa::MDirection::URANUS;
    } else if (ucName == "NEPTUNE") {
        planet = casa::MDirection::NEPTUNE;
    } else if (ucName == "PLUTO") {
        planet = casa::MDirection::PLUTO;
    } else if (ucName == "SUN") {
        planet = casa::MDirection::SUN;
    } else if (ucName == "MOON") {
        planet = casa::MDirection::MOON;
    } else {
        ControlExceptions::IllegalParameterErrorExImpl 
            ex( __FILE__,__LINE__,__PRETTY_FUNCTION__);
        ex.addData("ParameterName", "planetName");
        ex.addData("Value", planetName);
        ex.addData("ValidRange", "Mercury, Venus, Mars, Jupiter, Saturn, Uranus, Neptune, Pluto, Sun, Moon");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    return planet;
}
