// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007 - 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#include "TrackableObject.h"
#include "SourceAlarmTypes.h"
#include <ControlExceptions.h>
#include <iomanip> // for setprecision
#include <slalib.h> // for sla* functions
#include <acstimeEpochHelper.h> // for EpochHelper
#include <acstimeTimeUtil.h> // for TimeUtil
#include <LogToAudience.h> // for LOG_TO_{DEVELOPER,OPERATOR}
#include <baciROdouble.h> // for RODouble::get_sync in the weather station.
#include <casacore/measures/Measures/MeasIERS.h> // for casa::MeasIERS
#include <casacore/measures/Measures/MeasConvert.h> // for casa::MeasConvert
#include <casacore/measures/Measures/MCPosition.h> // Needed to work around a bug in CASA-3.0.0
#include <casacore/casa/Quanta/Euler.h>
#include <casacore/casa/Quanta/MVPosition.h> // for casa::MVPosition
#include <casacore/casa/Quanta/RotMatrix.h>
#include <casacore/casa/Exceptions/Error.h> // for casa::AipsError
#include <casacore/casa/BasicSL/Constants.h> // for casa::C::c
#include <controlAlarmHelper.h> // for Control::AlarmHelper
#include <TETimeUtil.h>

// shortcuts to avoid having to clutter the code with namespace qualifiers
using Control::TrackableObject;
using Control::AlarmHelper;
using std::ostringstream;
using std::string;
using std::endl;
using std::abs;
using Logging::Logger;
using boost::shared_ptr;
using std::setprecision;


// The now time could be anything (but not a plausible observation time)
const ACS::Time nowTime = static_cast<ACS::Time>(1);

// TODO. Get real values for all of these
const long array2TAI = 0; // from the CRD
const long TAI2UTC = 0; // From CASA
const double epoch = 2000.0; // From e

// The required sizes of the SLA parameter blocks
const int aoSize = 14;
const int amSize = 21;

// The default observing frequency used for refraction correction. This value
// is usually replaced by the value in the MountController
const double defaultObsFreq = 86E9;

// If the weather data is more than this many seconds old then warn the user.
const double maxAgeOfWeatherData = 5*60.0; // 5 mins

TrackableObject::TrackableObject(Logger::LoggerSmartPtr logger,
                                 shared_ptr<AlarmHelper>& alarms)
    :Logging::Loggable(logger),
     amprms_m(0),
     aoprms_m(0),
     azCorrection_m(0),
     obsFreq_m(defaultObsFreq),
     startTime_m(nowTime),
     equatorial_m(),
     horizon_m(),
     ws_m(Control::CurrentWeather::_nil()),
     antLoc_m(),
     padName_m("Unknown"),
     alarms_m(alarms)
{  
}

TrackableObject::TrackableObject(const ACS::Time& startTime,
                                 Logger::LoggerSmartPtr logger,
                                 shared_ptr<AlarmHelper>& alarms)
    :Logging::Loggable(logger),
     amprms_m(0),
     aoprms_m(0),
     azCorrection_m(0),
     obsFreq_m(defaultObsFreq),
     startTime_m(startTime),
     equatorial_m(),
     horizon_m(),
     ws_m(Control::CurrentWeather::_nil()),
     antLoc_m(),
     padName_m("Unknown"),
     alarms_m(alarms)
{
}

TrackableObject::TrackableObject(const TrackableObject& other)
    :Logging::Loggable(other),
     amprms_m(other.amprms_m),
     aoprms_m(other.aoprms_m),
     azCorrection_m(other.azCorrection_m),
     obsFreq_m(other.obsFreq_m),
     startTime_m(other.startTime_m),
     equatorial_m(other.equatorial_m),
     horizon_m(other.horizon_m),
     ws_m(other.ws_m),
     antLoc_m(other.antLoc_m),
     padName_m(other.padName_m),
     alarms_m(other.alarms_m)
{
}

TrackableObject& TrackableObject::operator=(const TrackableObject& other) {
    if (this != &other) {
        // The Logger does not have an operator= function
        //        Logger::operator=(other);
        amprms_m = other.amprms_m;
        aoprms_m = other.aoprms_m;
        azCorrection_m = other.azCorrection_m;
        obsFreq_m = other.obsFreq_m;
        startTime_m = other.startTime_m;
        equatorial_m = other.equatorial_m;
        horizon_m = other.horizon_m;
        ws_m = other.ws_m;
        antLoc_m = other.antLoc_m;
        padName_m = other.padName_m;
        alarms_m = other.alarms_m;
    }
    return *this;
}

TrackableObject::~TrackableObject() {
}

ACS::Time TrackableObject::getStartTime() const {
    return startTime_m;
}

bool TrackableObject::startNow() const {
    if (startTime_m == nowTime) {
        return true;
    } else {
        return false;
    }
}

void TrackableObject::getAzEl(const ACS::Time& time, double& az, double& el) {
    Control::EquatorialDirection target;
    Control::Offset equatorial;
    Control::EquatorialDirection equatorialRADec;
    Control::Offset horizon;
    Control::HorizonDirection horizonAzEl;
    Control::HorizonDirection commanded;
    Control::HorizonDirection commandedRate;
    getPointing(time, target, equatorial, equatorialRADec, 
                horizon, horizonAzEl, commanded, commandedRate);
    az = commanded.az;
    el = commanded.el;
}

void TrackableObject::setWeatherStation(const Control::CurrentWeather_ptr& ws){
    ws_m = ws;
}

bool TrackableObject::hasWeatherStation() const {
    if (CORBA::is_nil(ws_m)) {
        return false;
    } else {
        return true;
    }
}

void TrackableObject::
setAntenna(const casa::MPosition& antennaLocation,
           const string& padName) {
    antLoc_m = antennaLocation;
    padName_m = padName;
}

void TrackableObject::
getNullRefractionCoefficients(double& a, double& b) {
    a = 0;
    b = 0;
}

void TrackableObject::
getRealRefractionCoefficients(double& a, double& b) {
    if (isSlaUnInitialized()) {
        string msg = "SLA parameters are not initialized. Real";
        msg += " refraction coefficients are not available (returning zero).";
        LOG_TO_OPERATOR(LM_WARNING, msg);
        a = 0;
        b = 0;
    } else {
        a = aoprms_m[10];
        b = aoprms_m[11];
    }
}

bool TrackableObject::initializeSlaParams(bool verbose) {
    // Ensure the vectors are the right length.
    amprms_m.resize(amSize);
    aoprms_m.resize(aoSize);

    // Get the time now in modified Julian days.
    EpochHelper timeNow;
    if (startNow()) {
        timeNow.value(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
    } else {
        timeNow.value(getStartTime());
    }
    const double mjd(timeNow.toUTCdate(array2TAI, TAI2UTC));

    // Converting a vector to a C-style array should be OK
    slaMappa(epoch, mjd, &amprms_m[0]);

    // Get the antenna location in latitude (radians), longitude (radians),
    // altitude (meters). These default values are an antenna at the OSF. These
    // default values should never be used.
    double longitude = -1.18647;
    double latitude = -0.40271;
    double altitude = 2984.8;
    {
        const casa::MVPosition loc =
            casa::MeasConvert<casa::MPosition>
            (antLoc_m, casa::MPosition::WGS84)().getValue();
        latitude = loc.getLat("rad").getValue();
        longitude = loc.getLong("rad").getValue();
        altitude = loc.getLength("m").getValue();
    }

    // These default values are used if we cannot get real values;
    double dUT = 0.0; // seconds
    double polarMotionX = 0.0; // arc-secs
    double polarMotionY = 0.0;  // arc-secs
    try {
        // Now get the real values
        casa::Bool ok = casa::MeasIERS::get(dUT, casa::MeasIERS::PREDICTED,
                                            casa::MeasIERS::dUT1, mjd);
        ok = ok && casa::MeasIERS::get(polarMotionX, casa::MeasIERS::PREDICTED,
                                       casa::MeasIERS::X, mjd);
        ok = ok && casa::MeasIERS::get(polarMotionY, casa::MeasIERS::PREDICTED,
                                       casa::MeasIERS::Y, mjd);
        // This exception is caught immediately below.
        if (!ok) throw casa::AipsError("IERS tables are out of date.");
        alarms_m->deactivateAlarm(NO_IERS_DATA);
    } catch (casa::AipsError& ex) {
        string msg("Cannot get the latest earth rotation (IERS) parameters.");
        msg += " Using default values for the polar motion and UT1 - UTC.";
        msg += " Underlying error message is: " + ex.getMesg();
        LOG_TO_OPERATOR(LM_ERROR, msg);
        alarms_m->activateAlarm(NO_IERS_DATA);
    }
    if (verbose) {
        ostringstream msg;
        msg << "Current Earth orientation (IERS) parameters."
            << " Polar motion (x, y): (" 
            << setprecision(3) << std::fixed
            << polarMotionX << ", " << polarMotionY << ") arc-seconds."
            << " UT1 - UTC: " << dUT 
            << setprecision(3) << std::fixed
            << " seconds";
        LOG_TO_OPERATOR(LM_DEBUG, msg.str());
    }
    {
        const double arcsec2rad = M_PI/60/60/180;
        polarMotionX *= arcsec2rad;
        polarMotionY *= arcsec2rad;
    }

    const double waveLengthInUm = casa::C::c/obsFreq_m*1E6;
    // These default values are used if we cannot communicate with the weather
    // station controller;
    double tempInC = 0.0;
    double tempInK = 273.15 + tempInC;
    double pressureInhPa = 500.0;
    double relativeHumidity = 0.05;
    // I'm told, by Jeff Mangum, you need a balloon borne instrument to measure
    // the lapse rate. So we use a constant value.
    const double lapseRateKperM = 0.0065; 

    if (!CORBA::is_nil(ws_m)) {
        const ACS::Time startTime = ::getTimeStamp();
        bool wsDataOK = true;
        try {
            { // 1. get the temperature
                const Control::CurrentWeather::Temperature temp = 
                    ws_m->getTemperatureAtPad(padName_m.c_str());
                tempInC = temp.value;
                tempInK = 273.15 + tempInC;
                
                if (temp.valid) { // check the timestamp
                    wsDataOK = wsDataOK && 
                        checkAgeIsOK(temp.timestamp,timeNow,"Air temperature");
                } else {
                    ostringstream msg;
                    msg << "Cannot get a measured air temperature from the"
                        << "weather station. Using a simulated value of " 
                        << temp.value  << " deg. C.";
                    LOG_TO_OPERATOR(LM_ERROR, msg.str());
                    wsDataOK = false;
                    alarms_m->activateAlarm(WS_DISABLED);
                }
            }
            { // 2. get the pressure
                const Control::CurrentWeather::Pressure pressure = 
                    ws_m->getPressureAtPad(padName_m.c_str());
                pressureInhPa = pressure.value/100.0;
                
                if (pressure.valid) { // check the timestamp
                    wsDataOK = wsDataOK && 
                        checkAgeIsOK(pressure.timestamp, timeNow, "Pressure");
                } else {
                    ostringstream msg;
                    msg <<"Cannot get a measured atmospheric pressure from the"
                        << " weather station. Using a simulated value of " 
                        << pressureInhPa << " hPa.";
                    LOG_TO_OPERATOR(LM_ERROR, msg.str());
                    wsDataOK = false;
                    alarms_m->activateAlarm(WS_DISABLED);
                }
            }
            { // 3. get the humidity
                const Control::CurrentWeather::Humidity humidity = 
                    ws_m->getHumidityAtPad(padName_m.c_str());
                relativeHumidity = humidity.value;
                
                if (humidity.valid) { // check the timestamp
                    wsDataOK = wsDataOK && 
                        checkAgeIsOK(humidity.timestamp, timeNow, "Dew point");
                } else {
                    ostringstream msg;
                    msg << "Cannot get a measured relative humidity from the"
                        << " weather station. Using a simulated value of " 
                        << relativeHumidity*100 << "%.";
                    LOG_TO_OPERATOR(LM_ERROR, msg.str());
                    wsDataOK = false;
                    alarms_m->activateAlarm(WS_DISABLED);
                }
            }
            const ACS::Time endTime = ::getTimeStamp();
            {
                string msg = "Took " + TETimeUtil::toSeconds(endTime-startTime)
                    + " seconds to get the weather data";
                double elapsed = static_cast<double>(endTime-startTime)*100E-9;
                ACE_Log_Priority priority = LM_DEBUG;
                if (elapsed > .2) priority = LM_INFO;
                if (elapsed > 1) priority = LM_NOTICE;
                if (elapsed > 5) priority = LM_WARNING;
                if (elapsed > 20) priority = LM_ERROR;
                LOG_TO_DEVELOPER(priority, msg);
            }
        } catch (ControlExceptions::IllegalParameterErrorEx& ex) {
            string msg("Pad name supplied by the TMCDB is not correct.");
            msg += " Please ask computing support to correct this.";
            msg += " Using default weather parameters.";
            LOG_TO_OPERATOR(LM_ERROR, msg);
            alarms_m->activateAlarm(WS_DISABLED);
            ControlExceptions::IllegalParameterErrorExImpl newEx(ex);
            newEx.addData("Detail", msg);
            newEx.log();
            wsDataOK = false;
        }

        if (verbose) {
            ostringstream msg;
            msg << "Parameters used for refraction correction:"
                << " Temperature " << setprecision(3) << tempInC << " deg. C"
                << ", Pressure " << setprecision(3) << pressureInhPa << " hPa"
                << ", Humidity " << setprecision(3) <<relativeHumidity*100<<"%"
                << ", Observing frequency " << setprecision(4) 
                << obsFreq_m/1E9 << "GHz";
            LOG_TO_OPERATOR(LM_INFO, msg.str());
        }
        if (wsDataOK) {
            alarms_m->deactivateAlarm(WS_DISABLED);
            alarms_m->deactivateAlarm(OLD_WS_DATA);
        }
    }
  
    slaAoppa(mjd, dUT, longitude, latitude, altitude, 
             polarMotionX, polarMotionY, tempInK, 
             pressureInhPa, relativeHumidity, waveLengthInUm, lapseRateKperM, 
             &aoprms_m[0]);

    double trueLat, trueLong; // These are not used anywhere
    slaPolmo(longitude, latitude, 
             polarMotionX, polarMotionY, &trueLong, &trueLat, &azCorrection_m);

    if (verbose) {
        ostringstream msg;
        msg << "Azimuth correction " <<setprecision(3) 
            << azCorrection_m*180/M_PI*60*60 << " arc-sec";
        LOG_TO_OPERATOR(LM_DEBUG, msg.str());
    }
    return true;
}

bool TrackableObject::isSlaUnInitialized() const {
    if (amprms_m.size() == 0 || aoprms_m.size() == 0) {
        return true;
    } else {
        return false;
    }
}

bool TrackableObject::checkAgeIsOK(const ACS::Time& measurementTime, 
                                   EpochHelper& timeNow, 
                                   const string& dataName) {
    acstime::Epoch ep; 
    ep.value = measurementTime;
    const double dataAge = 
        abs(DurationHelper(timeNow.difference(ep)).toSeconds());
    if (dataAge > maxAgeOfWeatherData) {
        ostringstream msg;
        msg << dataName << " returned by the weather station is "
            << dataAge << " seconds old."
            << " Check the weather station is working."; 
        LOG_TO_OPERATOR(LM_ERROR, msg.str());
        alarms_m->activateAlarm(OLD_WS_DATA);
        return false;
    } else {
        return true;
    }
}

Control::Pattern& TrackableObject::horizonPattern() {
    return horizon_m;
}

Control::Pattern& TrackableObject::equatorialPattern() {
    return equatorial_m;
}

// The transformation from RA/Dec to Az/El involves a rotation about two
// angles, a rotation through the sidereal time and then a rotation through the
// co-latitude. Be aware that these rotations do not commute so that you cannot
// rotate through the co-latitude and then rotate through the sidereal time.

// These rotations are best done by first converting the RA/Dec to Cartesian
// coordinates, multiplying them by a rotation matrix and then converting back
// to a pair of angles (this time azimuth and elevation).
// The conversion from RA/Dec to Cartesian coordinates is done using:
//   x = cos(dec) * cos(RA)
//   y = cos(dec) * sin(RA)
//   z = sin(dec)
// And, as explained in more detail in the documentation for the
// LinearStroke::applyPattern function, the velocities in Cartesian
// coordinates are:
//   dx/dt = -sin(dec)*  cos(RA)*decRate + 
//            cos(dec)* -sin(RA)*raRate
//   dy/dt = -sin(dec)*  sin(RA)*decRate 
//            cos(dec)*  cos(RA)*raRate
//   dz/dt =  cos(dec)*decRate

// The rotation from RA/Dec to Az/El coordinates is done by
// 1. Rotating by the sidereal time around the z axis.
// 2. Rotating by the co-latitude (90deg - latitude) around the y axis
// This can be expressed in vector/matrix notation by defining.

// Pr: The RA/Dec position, in Cartestian coordinates
//     i.e., Pr = [Prx, Pry, Prz]'
// Pa: The Az/El position, in Cartestian coordinates
//     i.e., Pa = [Pax, Pay, Paz]'
// R: A rotation matrix (a rotation about the z and then the y axis)
//    i.e., R = (cos(coLat) 0 -sin(coLat)) ( cos(ST) sin(ST) 0) 
//              (0          1  0         ) (-sin(ST) cos(ST) 0) 
//              (sin(coLat) 0  cos(coLat)) ( 0       0       1) 
//            = ( cos(coLat)*cos(ST) cos(coLat)*sin(ST) -sin(coLat))
//              (-sin(ST)            cos(ST)             0         )
//              ( sin(coLat)*cos(ST) sin(coLat)*sin(ST)  cos(coLat))
// Where ST & coLat are the sidereal-time & co-latitude
//
// Then Pa = R*Pr 
// The velocity in azimuth and elevation (Va), is then given by
// Va = dPa/dt = dR/dt*Pr + R*dPr/dt = dR/dt*Pr + R*Vr
// If rates in RA and Dec are zero ie., we are not doing a linear equatorial
// stroke, then Vr == 0 and Va = dR/dt*Pr

// The calculation of dR/dt can be done in the same way as described in the
// LinearStroke::applyPattern function. However in this function we do not need
// to do this explicitly as the SLALIB function, slaAltAz, calculates both
// Az/El and dR/dt*Pr (in angular coordinates) for us. However this function
// does not calculate the other component of the velocity (R*Vr) for us. 
// This is done in part B of this function using CASA functions.
 
void TrackableObject::altAzVelocities(double ra, double dec, 
                                      double raRate, double decRate, 
                                      double siderealTime, double latitude, 
                                      double& az, double& el, 
                                      double& azRate, double& elRate)
{
    // Part A. Calculate the az/el and corresponding velocities. These
    // velocities only include the contribution from the rotation of the Earth
    // and do not include the contribution from the equatorial stroke (raRate &
    // decRate).
    {
        // The position angles and accelerations are not used anywhere
        double azAccel, elAccel, pa, paRate, paAccel;
        const double ha = siderealTime - ra; 
        slaAltaz(ha, dec, latitude, 
                 &az, &azRate, &azAccel, 
                 &el, &elRate, &elAccel, 
                 &pa, &paRate, &paAccel);
        // The velocities are in radians/radian of HA. Here I convert them to
        // rad/sec
        const double scale(2*M_PI/86400);
        azRate *= scale;
        elRate *= scale;
    }

    // Part B. Now add the velocity contribution from the equatorial stroke

    // 1. Get the stroke velocity in Cartesian coordinates (in the Equatorial
    // frame). ie., calculate Vr (and Pr which we do not need)
    casa::MVPosition eqStrokeVel;
    {
        vector<double> raDecxyz(6);
        slaDs2c6(ra, dec, 1.0, raRate, decRate, 0.0, &raDecxyz[0]);
        eqStrokeVel = casa::MVPosition(raDecxyz[3], raDecxyz[4], raDecxyz[5]);
    }
    // 2. Rotate to Horizon frame i.e., calculate R*Vr
    {
        const casa::RotMatrix 
            eqToHo(casa::Euler(siderealTime, 3u, M_PI_2-latitude, 2u));
        eqStrokeVel *= eqToHo;
        // flip the x-axis (because Az/El is a left handed coordinate system)
        eqStrokeVel(0) *= -1;
    }
  
    // 3. Get the celestial velocity vector in Cartesian coordinates (in the
    // Horizon frame). i.e., calculate dR/dt*Pr (and Pa which we do not need)
    casa::MVPosition starVel;
    vector<double> azElxyz(6);
    slaDs2c6(az, el, 1.0, azRate, elRate, 0.0, &azElxyz[0]);
    starVel = casa::MVPosition(azElxyz[3],  azElxyz[4], azElxyz[5]);
  
    // 4. Add the stroke velocity i.e, calculate dR/dt*Pr + R*Vr
    starVel += eqStrokeVel;
  
    // 5. Convert back to spherical coordinates
    for (int i = 0; i < 3; i++) {
        azElxyz[i+3] = starVel.getValue()(i);
    }
    double rad, radVel;
    double newLong, newLat;
    slaDc62s(&azElxyz[0], &newLong, &newLat, &rad, &azRate, &elRate, &radVel);
}

double TrackableObject::calcHALimit(const double& elLimit, const double& dec) {
    const double cosHALimit = (cos(M_PI_2-elLimit) - sin(dec)*aoprms_m[1])/
        (cos(dec)*aoprms_m[2]);
    return acos(cosHALimit);
}

void TrackableObject::observedRADec(ACS::Time time, 
                                    double ra2000, double dec2000,
                                    double pmRA, double pmDec, double parallax,
                                    double& raObs, double& decObs,
                                    double& raRate, double& decRate, 
                                    double& raOffset, double& decOffset) {
    // Calculate the LST
    const double mjd = EpochHelper(time).toUTCdate(0, 0);
    slaAoppat(mjd, &aoprms_m[0]);

    // Apply the equatorial pattern
    double raPattern, decPattern;
    equatorialPattern().applyPattern(time, ra2000, dec2000, 0.0, 0.0, 
                                     raPattern, decPattern, 
                                     raRate, decRate);
    raOffset = slaDrange(raPattern - ra2000);
    decOffset = slaDrange(decPattern - dec2000);

    // Mean J2000 to apparent
    double apparentRA, apparentDec;
    slaMapqk(raPattern, decPattern, pmRA, pmDec, parallax, 0.0, 
             &amprms_m[0], &apparentRA, &apparentDec);

    // Apparent to observed (both Ra/Dec and Az/El).
    // The following variables are not used anywhere
    double hob, zob, az;
    slaAopqk(apparentRA, apparentDec, &aoprms_m[0], 
             &az, &zob, &hob, &decObs, &raObs);
}
