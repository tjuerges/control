// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2007 - 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "OffsetStroke.h"
#include <loggingMACROS.h> // for AUTO_TRACE
#include <casacore/casa/aips.h>
#include <casacore/casa/Quanta/Euler.h>
#include <casacore/casa/Quanta/RotMatrix.h>

// shortcuts to avoid having to clutter the code with namespace qualifiers
using Control::OffsetStroke;

OffsetStroke::OffsetStroke(const double& longOffset, const double& latOffset)
    :Stroke(),
     offset_m(longOffset, latOffset),
     elRef_m(0.0)
{
    AUTO_TRACE(__func__);
}

OffsetStroke::OffsetStroke(const ACS::Time& startTime, 
                           const double& longOffset, const double& latOffset)
    :Stroke(startTime),
     offset_m(longOffset, latOffset),
     elRef_m(0.0)
{
    AUTO_TRACE(__func__);
}

OffsetStroke::OffsetStroke(const OffsetStroke& other)
    :Stroke(other),
     offset_m(other.offset_m),
     elRef_m(other.elRef_m)
{
    AUTO_TRACE(__func__);
}

OffsetStroke& OffsetStroke::operator=(const OffsetStroke& other) {
    AUTO_TRACE(__func__);
    Stroke::operator=(other);
    if (this != &other) {
	offset_m = other.offset_m;
	elRef_m = other.elRef_m;
    }
    return *this;
}

OffsetStroke::~OffsetStroke()
{
    AUTO_TRACE(__func__);
}

void OffsetStroke::
applyPattern(const ACS::Time& time, 
             const double& currentLong, const double& currentLat, 
             const double& currentLongVel, const double& currentLatVel, 
             double& newLong, double& newLat,
             double& newLongVel, double& newLatVel) const {
    // Commented out the following line because this function is called by the
    // trajectory planner thread numerous times and the tracing was having a
    // significant impact (10's of ms) on the thread execution time.
    // AUTO_TRACE(__func__);
    
    // The offset stroke should not change the velocities
    // TODO This is not right!
    newLongVel = currentLongVel;
    newLatVel = currentLatVel;
    
    // If time is before the start then do not change the values.
    if (time < getStartTime()) {
	newLong = currentLong;
	newLat = currentLat;
	newLongVel = currentLongVel;
	newLatVel = currentLatVel;
	return;
    }
    
    // 1. Rotate to the sky frame 
    casa::MVDirection skyOffset(offset_m);
    double refLat = currentLat;
    double refLong = currentLong;
    if (isRefDirectionSet() == true) {
	const casa::MVDirection refDirection = getRefDirection();
	refLat = refDirection.getLat();
	refLong = refDirection.getLong();
    }
    // The minus sign on the longitude is because Az/El is a left-handed
    // coordinate system
    skyOffset *= casa::RotMatrix(casa::Euler(0.0, refLat, -refLong));
    
    // 2. Convert to angles (lat/long)
    newLong = skyOffset.getLong();
    newLat = skyOffset.getLat();    
}

void OffsetStroke::
getOffset(const ACS::Time& time, double& longOffset, double& latOffset) const {
    // Commented out the following line because this function is called by the
    // trajectory planner thread numerous times and the tracing was having a
    // significant impact (10's of ms) on the thread execution time.
    //  AUTO_TRACE(__func__);
    if (time < getStartTime()) {
	longOffset = latOffset = 0.0;
    } else {
	longOffset = offset_m.getLong();
	latOffset = offset_m.getLat() - elRef_m;
    }
}

void OffsetStroke::setElRef(double elRef) {
    AUTO_TRACE(__func__);
    elRef_m = elRef;
}

double OffsetStroke::getElRef() const {
    AUTO_TRACE(__func__);
    return elRef_m;
}
