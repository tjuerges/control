// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007 - 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#include "HorizonObject.h"
#include <slalib.h> // for sla* functions
#include <acstimeEpochHelper.h> // for EpochHelper

// shortcuts to avoid having to clutter the code with namespace qualifiers
using Control::HorizonObject;
using Control::AlarmHelper;
using Logging::Logger;
using boost::shared_ptr;

// TODO. Get these values from a central place (like the base class)
const long array2TAI = 0; // from the CRD
const long TAI2UTC = 0;

HorizonObject::HorizonObject(const double& az, const double& el,
                             Logger::LoggerSmartPtr logger,
                             shared_ptr<AlarmHelper>& alarms)
    :TrackableObject(logger, alarms),
     az_m(az),
     el_m(el)
{ 
}

HorizonObject::HorizonObject(const ACS::Time& startTime, 
                             const double& az, const double& el,
                             Logger::LoggerSmartPtr logger,
                             shared_ptr<AlarmHelper>& alarms)
    :TrackableObject(startTime, logger, alarms),
     az_m(az),
     el_m(el)
{
}

HorizonObject::HorizonObject(const HorizonObject& other)
    :TrackableObject(other),
     az_m(other.az_m),
     el_m(other.el_m)
{
}

HorizonObject& HorizonObject::operator=(const HorizonObject& other) {
    TrackableObject::operator=(other);
    if (this != &other) {
        az_m = other.az_m;
        el_m = other.el_m;
    }
    return *this;
}

HorizonObject::~HorizonObject() {
}

void HorizonObject::
getPointing(const ACS::Time& time,
            Control::EquatorialDirection& target,
            Control::Offset& equatorial,
            Control::EquatorialDirection& equatorialRADec,
            Control::Offset& horizon,
            Control::HorizonDirection& horizonAzEl,
            Control::HorizonDirection& commanded,
            Control::HorizonDirection& commandedRate) {
    getCommandedRADec(az_m, el_m, time, target.ra, target.dec);

    // Cannot offset in RA/Dec for Horizon objects.
    equatorial.lng = equatorial.lat = 0.0;
    equatorialRADec.ra = equatorialRADec.dec = 0.0;

    // Now apply the horizon pattern
    horizonPattern().applyPattern(time, az_m, el_m, 0.0, 0.0, 
                                  commanded.az, commanded.el, 
                                  commandedRate.az, commandedRate.el);

    horizonPattern().getOffset(time, horizon.lng, horizon.lat);
    horizonAzEl.az = slaDrange(commanded.az - az_m);
    horizonAzEl.el = slaDrange(commanded.el - el_m);
}

void HorizonObject::getActualRADec(double az, double el, ACS::Time time,
                                   double& ra, double& dec) {
    if (isSlaUnInitialized()) initializeSlaParams();

    const double mjd = EpochHelper(time).toUTCdate(array2TAI, TAI2UTC);
    slaAoppat(mjd, &aoprms_m[0]);

    double apparentRA, apparentDec;
    slaOapqk("A", az, M_PI_2 - el, &aoprms_m[0], &apparentRA, &apparentDec);
    slaAmpqk(apparentRA, apparentDec, &amprms_m[0], &ra, &dec);
}

void HorizonObject::getCommandedRADec(double az, double el, ACS::Time time,
                                      double& ra, double& dec) {
    getActualRADec(az, el, time, ra, dec);
}

void HorizonObject::getRefractionCoefficients(double& a, double& b) {
    TrackableObject::getNullRefractionCoefficients(a, b);
}

double HorizonObject::timeToSet(const double& elLimit) {
    if (el_m >= elLimit) {
        return std::numeric_limits<double>::max();
    } else {
        return 0.0;
    }
}

double HorizonObject::timeToRise(const double& elLimit) {
    if (el_m >= elLimit) {
        return 0.0;
    } else {
        return std::numeric_limits<double>::max();
    }
}
