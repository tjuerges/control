// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2007, 2008
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "Pattern.h"
#include <loggingGetLogger.h> // Remove this line when COMP-1313 if fixed.
#include <loggingMACROS.h> // for AUTO_TRACE
#include <string> // for string
#include <Stroke.h> // For Stroke classes
#include <OffsetStroke.h> // For OffsetStroke classes
#include <LinearStroke.h> // For LinearStroke classes
#include <acstimeTimeUtil.h> // for TimeUtil
#include <boost/shared_ptr.hpp>

// shortcuts to avoid having to clutter the code with namespace qualifiers
using Control::Stroke;
using Control::Pattern;
using std::string;

Pattern::Pattern()
  :pattern_m(),
   patternMutex_m(),
   refDirection_m(),
   useRefDirection_m(false)
{
  const string fnName = "Pattern::Pattern()";
  AUTO_TRACE(fnName);
}

Pattern::Pattern(const Pattern& other)
  :pattern_m(),
   patternMutex_m(),
   refDirection_m(other.refDirection_m),
   useRefDirection_m(other.useRefDirection_m)
{
  const string fnName = "Pattern::Pattern(const Pattern&)";
  AUTO_TRACE(fnName);
  ACS::ThreadSyncGuard guard(&patternMutex_m); 
  ACS::ThreadSyncGuard otherGuard(&other.patternMutex_m); 
  pattern_m = other.pattern_m;
}

Pattern& Pattern::operator=(const Pattern& other) {
  const string fnName = "Pattern::operator=";
  AUTO_TRACE(fnName);

  if (this != &other) {
    {
      ACS::ThreadSyncGuard guard(&patternMutex_m); 
      ACS::ThreadSyncGuard otherGuard(&other.patternMutex_m); 
      pattern_m = other.pattern_m;
    }
    refDirection_m = other.refDirection_m;
    useRefDirection_m = other.useRefDirection_m;
  }
  return *this;
}

Pattern::~Pattern() {
  const string fnName = "Pattern::~Pattern";
  AUTO_TRACE(fnName);
}

void Pattern::insert(boost::shared_ptr<Stroke> stroke) {
  const string fnName = "Pattern::insert";
  AUTO_TRACE(fnName);

  if (useRefDirection_m) {
    stroke->setRefDirection(refDirection_m);
  }
  ACS::ThreadSyncGuard guard(&patternMutex_m); 
  // put the latest stroke at the back of the queue
  pattern_m.push_back(stroke);

  // Now sort it and remove all strokes that have elapsed. By using a stable
  // sort we ensure, if we have two strokes with the same time that the most
  // recently added one is used.
  const ACS::Time time = TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value;
  if (pattern_m.size() > 1) {
    std::stable_sort(pattern_m.begin(), pattern_m.end(), Stroke::compareTime);
    while (pattern_m.size() > 1 && pattern_m[1]->getStartTime() <= time) {
      pattern_m.pop_front();
    }
  }
}

void Pattern::clear() {
  const string fnName = "Pattern::clear";
  AUTO_TRACE(fnName);
  ACS::ThreadSyncGuard guard(&patternMutex_m); 
  pattern_m.clear();
  useRefDirection_m = false;
}

void Pattern::
applyPattern(const ACS::Time& time, 
             double currentLong, double currentLat, 
             double currentLongVel, double currentLatVel, 
             double& newLong, double& newLat,
             double& newLongVel, double& newLatVel) {
  const string fnName = "Pattern::applyPattern";
  // Commented out the following line because this function is called by the
  // trajectory planner thread numerous times and the tracing was having a
  // significant impact (10's of ms) on the thread execution time.
  // AUTO_TRACE(fnName);
  ACS::ThreadSyncGuard guard(&patternMutex_m); 
  if (pattern_m.empty()) {
    newLat =  currentLat;
    newLong = currentLong;
    newLongVel = currentLongVel;
    newLatVel = currentLatVel;
    return;
  }
  while (pattern_m.size() > 1 && pattern_m[1]->getStartTime() <= time) {
    pattern_m.pop_front();
  }
  pattern_m.front()->applyPattern(time, currentLong, currentLat,
                                  currentLongVel, currentLatVel,
                                  newLong, newLat, newLongVel, newLatVel);
}

void Pattern::getOffset(const ACS::Time& time, 
                        double& longOffset, double& latOffset) const {
  const string fnName = "Pattern::getOffset";
  //  AUTO_TRACE(fnName);
  // return zero if we cannot get a better value;
  longOffset = 0.0;
  latOffset = 0.0;
  ACS::ThreadSyncGuard guard(&patternMutex_m); 
  if (pattern_m.empty()) return;

  LinearStroke* lstrokePtr = 
    dynamic_cast<LinearStroke*>(pattern_m.front().get());
  if (lstrokePtr != 0) {
      return lstrokePtr->getOffset(time, longOffset, latOffset);
  }
  OffsetStroke* strokePtr = 
    dynamic_cast<OffsetStroke*>(pattern_m.front().get());
  if (strokePtr != 0) {
      return strokePtr->getOffset(time, longOffset, latOffset);
  }
}

void Pattern::setRefDirection(const casa::MVDirection& refDirection) {
  const string fnName = "Pattern:::setRefDirection";
  AUTO_TRACE(fnName);
  refDirection_m = refDirection;
  useRefDirection_m = true;
}

casa::MVDirection Pattern::getRefDirection() const {
  const string fnName = "Pattern::getRefDirection";
  AUTO_TRACE(fnName);
  return refDirection_m;
}

bool Pattern::isRefDirectionSet() const {
  const string fnName = "Pattern::isRefDirectionSet";
  AUTO_TRACE(fnName);
  return useRefDirection_m;
}
