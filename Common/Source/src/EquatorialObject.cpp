// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007 - 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#include "EquatorialObject.h"
#include <slalib.h> // for sla* functions
#include <acstimeEpochHelper.h> // for EpochHelper
#include <cmath> // for log, sin, cos etc.

// shortcuts to avoid having to clutter the code with namespace qualifiers
using Control::EquatorialObject;
using Control::EquatorialDirection;
using Control::AlarmHelper;
using Logging::Logger;
using std::acos;
using std::cos;
using std::sin;
using std::abs;
using boost::shared_ptr;

// TODO. Get this values from a central place (like the CRD)
const long array2TAI = 0;

// Always set the radial velocity to zero. Perhaps the parallax should also be
// eliminated.
const double radVel = 0.0;

// This constant is used in by the slaPrecess function in the moveTo2000
// function.
char* fk5 = "FK5";

EquatorialObject::EquatorialObject(const double& ra, const double& dec,
                                   const double& pmRA, const double& pmDec,
                                   const double& parallax,
                                   const casa::MPosition& antennaLocation,
                                   const string& antennaName,
                                   const string& padName,
                                   const Control::CurrentWeather_ptr& ws,
                                   const double& obsFreq,
                                   const double& epoch,
                                   Logger::LoggerSmartPtr logger,
                                   shared_ptr<AlarmHelper>& alarms,
                                   bool verbose)
    :TrackableObject(logger, alarms),
     ra_m(ra),
     dec_m(dec),
     pmRA_m(0.0),
     pmDec_m(0.0),
     parallax_m(0.0),
     ra2000_m(ra),
     dec2000_m(dec)
{
    pmRA_m = casa::Quantity(pmRA, "rad/s").getValue("rad/yr");
    // Convert from an angle, on the sky to a rate of change in the RA
    // coordinate.  The angle on the sky is what astronomers expect to provide
    // and is used by most catalogs. The rate of change in the RA coordinate is
    // what SLALIB wants.

    // There is a singularity at the poles. But we can handle things when they
    // are 1 milli-arcsec away. A declination of more than +/- 90 degrees
    // should be caught earlier and never get through to this function.
    if (abs(dec) < M_PI/2-M_PI/180/60/60/1000) {
        pmRA_m = pmRA_m/cos(dec);
    }
    pmDec_m = casa::Quantity(pmDec, "rad/s").getValue("rad/yr");
    parallax_m = casa::Quantity(parallax, "rad").getValue("arcsec");
    setAntenna(antennaLocation, padName);
    setWeatherStation(ws);
    obsFreq_m = obsFreq;
    initializeSlaParams(verbose);
    moveTo2000(epoch);
}

EquatorialObject::EquatorialObject(const ACS::Time& startTime, 
                                   const double& ra, const double& dec,
                                   const double& pmRA, const double& pmDec,
                                   const double& parallax,
                                   const casa::MPosition& antennaLocation,
                                   const string& antennaName,
                                   const string& padName,
                                   const Control::CurrentWeather_ptr& ws,
                                   const double& obsFreq,
                                   const double& epoch,
                                   Logger::LoggerSmartPtr logger,
                                   shared_ptr<AlarmHelper>& alarms,
                                   bool verbose)
    :TrackableObject(startTime, logger, alarms),
     ra_m(ra),
     dec_m(dec),
     pmRA_m(0.0),
     pmDec_m(0.0),
     parallax_m(0.0),
     ra2000_m(ra),
     dec2000_m(dec)
{
    pmRA_m = casa::Quantity(pmRA, "rad/s").getValue("rad/yr");
    // Convert from an angle, on the sky to a rate of change in the RA
    // coordinate.  The angle on the sky is what astronomers expect to provide
    // and is used by most catalogs. The rate of change in the RA coordinate is
    // what SLALIB wants.

    // There is a singularity at the poles. But we can handle things when they
    // are 1 milli-arcsec away. A declination of more than +/- 90 degrees
    // should be caught earlier and never get through to this function.
    if (abs(dec) < M_PI/2-M_PI/180/60/60/1000) {
        pmRA_m = pmRA_m/cos(dec);
    }
    pmDec_m = casa::Quantity(pmDec, "rad/s").getValue("rad/yr");
    parallax_m = casa::Quantity(parallax, "rad").getValue("arcsec");
    setAntenna(antennaLocation, padName);
    setWeatherStation(ws);
    obsFreq_m = obsFreq;
    initializeSlaParams(verbose);
    moveTo2000(epoch);
}

EquatorialObject::EquatorialObject(const double& ra, const double& dec,
                                   const double& pmRA, const double& pmDec,
                                   const double& parallax,
                                   const casa::MPosition& antennaLocation,
                                   const double& epoch,
                                   Logger::LoggerSmartPtr logger,
                                   shared_ptr<AlarmHelper>& alarms)
    :TrackableObject(logger, alarms),
     ra_m(ra),
     dec_m(dec),
     pmRA_m(0.0),
     pmDec_m(0.0),
     parallax_m(0.0),
     ra2000_m(ra),
     dec2000_m(dec)
{
    pmRA_m = casa::Quantity(pmRA, "rad/s").getValue("rad/yr");
    // Convert from an angle, on the sky to a rate of change in the RA
    // coordinate.  The angle on the sky is what astronomers expect to provide
    // and is used by most catalogs. The rate of change in the RA coordinate is
    // what SLALIB wants.

    // There is a singularity at the poles. But we can handle things when they
    // are 1 milli-arcsec away. A declination of more than +/- 90 degrees
    // should be caught earlier and never get through to this function.
    if (abs(dec) < M_PI/2-M_PI/180/60/60/1000) {
        pmRA_m = pmRA_m/cos(dec);
    }
    pmDec_m = casa::Quantity(pmDec, "rad/s").getValue("rad/yr");
    parallax_m = casa::Quantity(parallax, "rad").getValue("arcsec");
    setAntenna(antennaLocation);
    initializeSlaParams(false);
    moveTo2000(epoch);
}

EquatorialObject::EquatorialObject(const EquatorialObject& other)
    :TrackableObject(other),
     ra_m(other.ra_m),
     dec_m(other.dec_m),
     pmRA_m(other.pmRA_m),
     pmDec_m(other.pmDec_m),
     parallax_m(other.parallax_m),
     ra2000_m(other.ra2000_m),
     dec2000_m(other.dec2000_m)
{
}

EquatorialObject& EquatorialObject::operator=(const EquatorialObject& other) {
    TrackableObject::operator=(other);
    if (this != &other) {
        ra_m = other.ra_m;
        dec_m = other.dec_m;
        pmRA_m = other.pmRA_m;
        pmDec_m = other.pmDec_m;
        parallax_m = other.parallax_m;
        ra2000_m = other.ra2000_m;
        dec2000_m = other.dec2000_m;
    }
    return *this;
}

EquatorialObject::~EquatorialObject(){
}

void EquatorialObject::
getPointing(const ACS::Time& time,
            Control::EquatorialDirection& target,
            Control::Offset& equatorial,
            Control::EquatorialDirection& equatorialRADec,
            Control::Offset& horizon,
            Control::HorizonDirection& horizonAzEl,
            Control::HorizonDirection& commanded,
            Control::HorizonDirection& commandedRate) {
    target.ra = ra2000_m;
    target.dec = dec2000_m;
    equatorialPattern().getOffset(time, equatorial.lng, equatorial.lat);

    // Calculate the observed position
    double ra, dec, raRate, decRate;
    observedRADec(time, 
                  ra2000_m, dec2000_m, pmRA_m, pmDec_m, parallax_m,
                  ra, dec, raRate, decRate, 
                  equatorialRADec.ra, equatorialRADec.dec);

    // Calculate the az/el and the az/el velocities.
    double az, el, azRate, elRate;
    altAzVelocities(ra, dec, raRate, decRate, 
                    aoprms_m[13], aoprms_m[0], 
                    az, el, azRate, elRate);

    az += azCorrection_m;

    // Put the azimuth between +/-180 to prevent problems calculating the slew
    // time. The AzCableWrap class will adjust this if necessary.
    az = slaDrange(az);

    // Now apply the horizon pattern
    horizonPattern().applyPattern(time, az, el, azRate, elRate, 
                                  commanded.az, commanded.el, 
                                  commandedRate.az, commandedRate.el);
    horizonPattern().getOffset(time, horizon.lng, horizon.lat);
    horizonAzEl.az = slaDrange(commanded.az - az);
    horizonAzEl.el = slaDrange(commanded.el - el);
}

double EquatorialObject::timeToRise(const double& elLimit) {
    // The current time in UTC.
    const ACS::Time time = ::getTimeStamp() - array2TAI;

    // Calculate the observed position
    double ra, dec;
    // These variables are not used
    double raRate, decRate, raOffset, decOffset;
    observedRADec(time, 
                  ra2000_m, dec2000_m, pmRA_m, pmDec_m, parallax_m,
                  ra, dec, raRate, decRate, 
                  raOffset, decOffset);

    // Now that we know the observed declination check if its outside the range
    // of sources which can rise or set. This is essential as the trigonometric
    // calculation that follows will return NaN if we have a source whose
    // declination outside these limits.
    if (dec > M_PI_2 + aoprms_m[0] - elLimit) {
        return -1; // never rises
    }
    if (dec < -M_PI_2 - aoprms_m[0] - elLimit) {
        return 0.0; // never sets
    }

    const double haLimit = calcHALimit(elLimit, dec);

    // bring HA into the range [-pi to +pi]
    double ha = slaDrange(aoprms_m[13] - ra);
    if (abs(ha) < haLimit) return 0.0; // source is up
    // bring ha into the range [-2pi to 0]
    if (ha > 0) ha -= 2 * M_PI;
    return (-haLimit - ha)/(2*M_PI)* 24*60*60;
}

double EquatorialObject::timeToSet(const double& elLimit) {
    // The current time in UTC.
    const ACS::Time time = ::getTimeStamp() - array2TAI;

    // Calculate the observed position
    double ra, dec;
    // These variables are not used
    double raRate, decRate, raOffset, decOffset;
    observedRADec(time, 
                  ra2000_m, dec2000_m, pmRA_m, pmDec_m, parallax_m,
                  ra, dec, raRate, decRate, 
                  raOffset, decOffset);

    // Now that we know the observed declination check if its outside the range
    // of sources which can rise or set. This is essential as the trigonometric
    // calculation that follows will return NaN if we have a source whose
    // declination outside these limits.
    if (dec > M_PI_2 + aoprms_m[0] - elLimit) {
        return -1; // never rises
    }
    if (dec < -M_PI_2 - aoprms_m[0] - elLimit) {
        return std::numeric_limits<double>::max(); // never sets
    }

    const double haLimit = calcHALimit(elLimit, dec);

    // bring HA into the range [-pi to +pi]
    const double ha = slaDrange(aoprms_m[13] - ra);
    if (abs(ha) > haLimit) return -1; // source has set

    return (haLimit - ha)/(2*M_PI)* 24*60*60;
}

void EquatorialObject::
getActualRADec(double az, double el, ACS::Time time,
               double& ra, double& dec) {
    EpochHelper timeStamp(time);
    const double mjd = timeStamp.toUTCdate(array2TAI, 0);
    slaAoppat(mjd, &aoprms_m[0]);
    double apparentRA, apparentDec;

    az -= azCorrection_m;
    slaOapqk("A", az, M_PI_2 - el, &aoprms_m[0], &apparentRA, &apparentDec);
    slaAmpqk(apparentRA, apparentDec, &amprms_m[0], &ra, &dec);
}

void EquatorialObject::
getCommandedRADec(double az, double el, ACS::Time time, 
                  double& ra, double& dec) {
    ra = ra_m;
    dec = dec_m;
}

void EquatorialObject::getRefractionCoefficients(double& a, double& b) {
    TrackableObject::getRealRefractionCoefficients(a, b);
}

void EquatorialObject::moveTo2000(const double& epoch) {
    // Do the proper-motion correction
    slaPm(ra_m, dec_m, pmRA_m, pmDec_m, parallax_m, radVel, 
          epoch, 2000.0, &ra2000_m, &dec2000_m);
    // Do the precession/nutation correction
    slaPreces(fk5, epoch, 2000.0, &ra2000_m, &dec2000_m);
}
