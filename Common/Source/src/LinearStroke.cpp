// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2007 - 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "LinearStroke.h"
#include <cmath>
#include <string>
#include <acstimeDurationHelper.h>
#include <loggingMACROS.h> // for AUTO_TRACE
#include <casacore/casa/aips.h>
#include <casacore/casa/Quanta/Euler.h>
#include <casacore/casa/Quanta/RotMatrix.h>
#include <casacore/casa/Quanta/MVDirection.h>
#include <casacore/casa/Arrays/Vector.h>
#include <casacore/casa/Arrays/Matrix.h>
#include <casacore/casa/Arrays/MatrixMath.h>
#include <casacore/casa/Arrays/ArrayMath.h>
#include <slalib.h>

// shortcuts to avoid having to clutter the code with namespace qualifiers
using Control::LinearStroke;
using std::cos;
using std::sin;
using std::fabs;

LinearStroke::LinearStroke(const ACS::Time& startTime, 
                           const double& longOffset, const double& latOffset,
                           const double& longVel, const double& latVel)
    :OffsetStroke(startTime, longOffset, latOffset),
     longVel_m(longVel),
     latVel_m(latVel)
{
    AUTO_TRACE(__func__);
}

LinearStroke::LinearStroke(const LinearStroke& other)
    :OffsetStroke(other),
     longVel_m(other.longVel_m),
     latVel_m(other.latVel_m)
{
    AUTO_TRACE(__func__);
}

LinearStroke& LinearStroke::operator=(const LinearStroke& other) {
    AUTO_TRACE(__func__);
    
    OffsetStroke::operator=(other);
    if (this != &other) {
	longVel_m = other.longVel_m;
	latVel_m = other.latVel_m;
    }
    return *this;
}

LinearStroke::~LinearStroke()
{
    AUTO_TRACE(__func__);
}

void LinearStroke::
applyPattern(const ACS::Time& time, 
             const double& currentLong, const double& currentLat, 
             const double& currentLongVel, const double& currentLatVel, 
             double& newLong, double& newLat,
             double& newLongVel, double& newLatVel) const {
    // Commented out the following line because this function is called by the
    // trajectory planner thread numerous times and the tracing was having a
    // significant impact (10's of ms) on the thread execution time.
    // AUTO_TRACE(__func__);
    
    // This function calculates the instantaneous position and velocity of the
    // stroke in a spherical coordinate system where the reference position is
    // coincident with the origin i.e., (lat, long) = (0, 0). I'll call this
    // the local frame. It then rotates these positions (and velocities) to
    // another coordinate system where the reference position is as specified
    // by the user. I'll call this the sky frame.
    
    // A linear stroke is defined as having a position and velocity in both
    // longitude and latitude. The terms longitude and latitude are used
    // because they are, in general, not identical to azimuth and elevation (or
    // right ascension & declination). However longitude and latitude are
    // related to azimuth and elevation through a rotation.
    
    // A linear stroke on each axis (longitude and latitude) is defined as
    // having a constant angular offset e.g., lat0, and a constant angular
    // velocity e.g., lat1, in the local reference frame. The instantaneous
    // position is then a unit vector with its longitude and latitude given by:
    //   long = long0 + long1 * (t - t0)
    //   lat  = lat0  + lat1  * (t - t0)
    // here t0 is the start time of the stroke. At times before the start time
    // this function does not change the input position and velocity.
    
    // This function converts this unit vector to Cartesian coordinates using 
    //   x = cos(lat) * cos(long)
    //   y = cos(lat) * sin(long)
    //   z = sin(lat)
    // This is done in a SLALIB routine (slaDs2c6)
    //
    // It then rotates this vector to the sky frame by:
    // 1. Rotating by the latitude of the reference position ie., about the y
    // axis.
    // 2. Rotating by the longitude of the reference position ie., about the z
    // axis
    // This is done using casa functions
    //
    // The resultant unit vector (still in Cartesian coordinates) is then
    // converted to a pair of angles e.g., an azimuth and elevation, and
    // returned. The conversion from (x, y, z) to angles is:
    //   long = atan2(y, x);
    //   lat  = asin(z);
    // This is done in a SLALIB routine (slaDc62s)
    
    // Conceptually the same procedure is used for the velocities but its a bit
    // more complicated. The velocity, in the local reference frame and in
    // Cartesian coordinates is given by:
    //   dx/dt = d(cos(lat) * cos(long))/dt
    //         = d(cos(lat0+lat1*(t-t0)))/dt * cos(long) + 
    //            cos(lat)                   * d(cos(long0+long1*(t-t0)))/dt 
    //         = -sin(lat)*d(lat0+lat1*(t-t0))/dt *  cos(long) + 
    //            cos(lat)                        * -sin(long)*d(long0+long1*(t-t0))/dt
    //         = -sin(lat)*lat1 *  cos(long) + 
    //            cos(lat)      * -sin(long)*long1
    //   dy/dt = -sin(lat)*sin(long)*lat1
    //            cos(lat)*cos(long)*long1
    //   dz/dt =  cos(lat)*lat1
    // This is done in a SLALIB routine (slaDs2c6)
    // Note that the velocity is not a unit vector.
    // 

    // This velocity vector is then rotated, to the sky frame, in the same way
    // as the position is (but see below the additional terms if the reference
    // position is changing).
    //
    // Converting this velocity vector back to an angular velocity in latitude
    // and longitude uses the inverse of the previous equation.
    // This is done in a SLALIB routine (slaDc62s)
    
    // The rotation from local to sky coordinates can be expressed in
    // vector/matrix notation by defining
    // Pl: The position, in Cartestian coordinates, in the local frame.
    //     i.e., Pl = [Plx, Ply, Plz]'
    // Ps: The position, in Cartestian coordinates, in the sky frame.
    //     i.e., Ps = [Psx, Psy, Psz]'
    // R: A rotation matrix (a rotation about the y and then the z axis)
    //    i.e., R = ( cos(rLong) sin(rLong) 0) (cos(rLat) 0 -sin(rLat))
    //              (-sin(rLong) cos(rLong) 0) (0         1  0        )
    //              ( 0          0          1) (sin(rLat) 0  cos(rLat))
    //            = ( cos(rLong)*cos(rLat) sin(rLong) -cos(rLong)*sin(rLat))
    //              (-sin(rLong)*cos(rLat) cos(rLong)  sin(rLong)*sin(rLat))
    //              ( sin(rLat)            0           cos(rLat)           )
    // Where rLong & rLat are the longitude & latitude of the reference
    // position.
    //
    // Then Ps = R*Pl 
    // The velocity in the sky frame (Vs), is then given by
    // Vs = dPs/dt = dR/dt*Pl + R*dPl/dt
    // and if the reference position does not change then dR/dt is zero and
    // Vs = R*Vl (Vl is the velocity in the local frame).
    
    // The reference position does change with time if either of the input
    // parameters (currentLongVel or currentLatVel) are non-zero and the
    // reference position has not been explicitly set. This commonly happens
    // when doing linear strokes in horizon coordinates while tracking a
    // celestial source. The derivative is:
    // 
    // dR/dt = dR/d(rLong)*d(rLong)/dt + dR/d(rLat)*d(rLat)/dt
    //
    // where the derivative of the rotation matrix is another matrix thats
    // determined by taking the derivative of each element.
    // Specifically
    // dR/d(rLong) = (-sin(rLong)*cos(rLat)  cos(rLong)  sin(rLong)*sin(rLat))
    //               (-cos(rLong)*cos(rLat) -sin(rLong)  cos(rLong)*sin(rLat))
    //               (0                      0           0                   )
    // dR/d(rLat)  = (-cos(rLong)*sin(rLat) 0 -cos(rLong)*cos(rLat))
    //               ( sin(rLong)*sin(rLat) 0  sin(rLong)*cos(rLat))
    //               ( cos(rLat)            0 -sin(rLat)           )
    // and 
    // d(rLong)/dt = currentLongVel
    // d(rLat)/dt = currentLatVel
    //
    // The calculation of dR/dt*Pl is done, by hand, in the offsetVelocity
    // function, as I could not find any functions in SLALIB or casa that did
    // this for me.
    
    const ACS::Time startTime = getStartTime();
    if (time < startTime) {
	newLong = currentLong;
	newLat = currentLat;
	newLongVel = currentLongVel;
	newLatVel = currentLatVel;
	return;
    }
    
    // 
    // 1. get the current instantaneous positions
    // 
    double iLong, iLat;
    getOffset(time, iLong, iLat);
    iLat += getElRef();
    
    //
    // 2. Convert this to rectangular coordinates (using SLALIB)
    //
    vector<double> xyzVxyz(6);
    slaDs2c6(iLong, iLat, 1.0, longVel_m, latVel_m, 0.0, &xyzVxyz[0]);
    casa::MVDirection pos(xyzVxyz[0], xyzVxyz[1], xyzVxyz[2]);
    // Do *not* use an MVDirection object here as this is not a unit vector!
    casa::MVPosition vel(xyzVxyz[3], xyzVxyz[4], xyzVxyz[5]);
    
    //
    // 3. Get the reference position
    // 
    // Unless explicitly set I default the reference position to the current
    // position.
    double refLong = currentLong;
    double refLat = currentLat;
    double refLongVel = currentLongVel;
    double refLatVel = currentLatVel;
    if (isRefDirectionSet()) {
	const casa::MVDirection refDirection = getRefDirection();
	refLong = refDirection.getLong();
	refLat = refDirection.getLat();
	refLongVel = 0.0;
	refLatVel = 0.0;
    }
    
    //
    // 4. Rotate to the sky frame
    //
    // Do the rotation for both position and velocity. The minus sign on the
    // longitude is because Az/El is a left-handed coordinate system
    const casa::RotMatrix localToSky(casa::Euler(0.0, refLat, -refLong));
    pos *= localToSky;
    vel *= localToSky;
    
    //
    // 5. Add, to the velocity, the additional components due to
    // any the motion of the source on the sky.
    //
    {
	double xv, yv, zv;
	//  The minus sign on the longitude is because Az/El is a left-handed
	//  coordinate system
	offsetVelocity(-refLong, refLat, -refLongVel, refLatVel,
		       xyzVxyz[0], xyzVxyz[1], xyzVxyz[2],
		       xv, yv, zv);
	vel += casa::MVPosition(xv, yv, zv);
    }
    
    //
    // 6. Convert back to angles (using SLALIB)
    //
    for (int i = 0; i < 3; i++) {
	xyzVxyz[i] = pos.getValue()(i);
	xyzVxyz[i+3] = vel.getValue()(i);
    }
    double rad, radVel;
    slaDc62s(&xyzVxyz[0], 
	     &newLong, &newLat, &rad, 
	     &newLongVel, &newLatVel, &radVel);
}

void LinearStroke::offsetVelocity(double refLong, double refLat, 
                                  double refLongVel, double refLatVel, 
                                  double xOff, double yOff, double zOff,
                                  double& xVel, double& yVel, double& zVel) {
    
    casa::Vector<casa::Double> pl(3);
    pl(0) = xOff;
    pl(1) = yOff;
    pl(2) = zOff;
    
    const double sinLong = sin(refLong);
    const double cosLong = cos(refLong);
    const double sinLat  = sin(refLat);
    const double cosLat  = cos(refLat);
    casa::Matrix<casa::Double> dLong(3,3);
    
    dLong(0,0) = -sinLong*cosLat;
    dLong(0,1) = cosLong;
    dLong(0,2) = sinLong*sinLat;
    dLong(1,0) = -cosLong*cosLat;
    dLong(1,1) = -sinLong;
    dLong(1,2) = cosLong*sinLat;
    dLong(2,0) = 0.0;
    dLong(2,1) = 0.0;
    dLong(2,2) = 0.0; 
    
    casa::Matrix<casa::Double> dLat(3,3);
    dLat(0,0) = -cosLong*sinLat;
    dLat(0,1) = 0.0;
    dLat(0,2) = -cosLong*cosLat;
    dLat(1,0) = sinLong*sinLat;
    dLat(1,1) = 0.0;
    dLat(1,2) = sinLong*cosLat;
    dLat(2,0) = cosLat;
    dLat(2,1) = 0.0;
    dLat(2,2) = -sinLat; 
    
    const casa::Vector<casa::Double> vel = 
	casa::product(dLong, pl)*refLongVel + 
	casa::product(dLat, pl)*refLatVel;
    
    xVel = vel(0);
    yVel = vel(1);
    zVel = vel(2);
}

void LinearStroke::
getOffset(const ACS::Time time, double& longOffset, double& latOffset) const {
    // Commented out the following line because this function is called by the
    // trajectory planner thread numerous times and the tracing was having a
    // significant impact (10's of ms) on the thread execution time.
    //  AUTO_TRACE(__func__);
    
    OffsetStroke::getOffset(time, longOffset, latOffset);
    const ACS::Time startTime = getStartTime();
    if (time < startTime) return;
    
    const double elapsedTime = 
	DurationHelper(static_cast<ACS::TimeInterval>(time - startTime))
	.toSeconds();
    longOffset += longVel_m*elapsedTime;
    latOffset += latVel_m*elapsedTime;
}
