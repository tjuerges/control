#!/usr/bin/env python
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2009
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# jkern  2009-10-22  created
#


#************************************************************************
#   NAME HorizonResultParser.py
#
#   SYNOPSIS
#
#------------------------------------------------------------------------
#

from math import radians
import time
import calendar
import TETimeUtil
import CCL.SIConverter as SIConverter

class ParserErrorEx(Exception):
    def __init__(self, message):
        Exception.__init__(self, message)

class Parser:
    def __init__(self):
        self._startIndex = None
        self._endIndex   = None

    def setDataRange(self, startIndex, endIndex):
        self._startIndex = startIndex
        self._endIndex   = endIndex

    def parse(self, line):
        # This method must be overridden in the inheriting classes
        raise ParserErrorEx("Parser class without actual parser method")

class UTParserBase(Parser):
    def __init__(self):
        Parser.__init__(self)
        self.__acsTimeOffset = 12219292800
        self.__acsTimeScale  = 1.0E7;

    def parse(self, line):
        data = line[self._startIndex:self._endIndex]
        utcSec = calendar.timegm(time.strptime(data,self._format))
        return long((utcSec +self.__acsTimeOffset)*self.__acsTimeScale)

class UTParserHM(UTParserBase):
    def __init__(self):
        UTParserBase.__init__(self)
        self._format = "%Y-%b-%d %H:%M"

class UTParserHMS(UTParserBase):
    def __init__(self):
        UTParserBase.__init__(self)
        self._format = "%Y-%b-%d %H:%M:%S"
    
class UTParserHMSF(UTParserBase):
    def __init__(self):
        UTParserBase.__init__(self)
        self._format = "%Y-%b-%d %H:%M:%S"

    def setDataRange(self, startIndex, endIndex):
        # The fractional part is always the last 4 characters
        self._startIndex = startIndex
        self._endIndex   = endIndex - 4

    def parse(self, line):
        return  UTParserBase.parse(self, line) +\
               int(line[self._endIndex+1:self._endIndex+4])*1E4

class JDParser(Parser):
    def __init__(self):
        Parser.__init__(self)
        self._jdOffset = 2299160.5
        
    def parse(self, line):
        jd = float(line[self._startIndex:self._endIndex])
        return long((jd - self._jdOffset) * 86400E7)

class SexagesimalPositionParser(Parser):
    def parse(self, line):
        splitData = line[self._startIndex:self._endIndex].split()
        ra = "%s:%s:%s" % tuple(splitData[0:3])
        dec = "%s.%s.%s" % tuple(splitData[3:6])
        return (SIConverter.toRadians(ra), SIConverter.toRadians(dec))

class DecimalPositionParser(Parser):
    def parse(self, line):
        splitData = line[self._startIndex:self._endIndex].split()
        return (radians(float(splitData[0])),radians(float(splitData[1])))

class DeltaParser(Parser):

    def __init__(self):
        Parser.__init__(self)
        self._unit = None

    def setUnit(self, unit):
        self._unit = unit

    def parse(self, line):
        data = line[self._startIndex:self._endIndex]
        return SIConverter.toMeters("%s %s" % (data,self._unit))

class HorizonResultParser:
    def __init__(self, filename=None):
        self.__dateParser     = None
        self.__positionParser = None
        self.__deltaParser    = None
        if filename is not None:
            self.loadEphemerisFile(filename)
        
    def getEphemeris(self, duration = None):
        if duration is None:
            start = 0
            end = len(self.ephemeris)
        else:
            (start, end) = self._findEphemerisRange(SIConverter.toSeconds\
                                                    (duration))

        parsedEphemeris = []
        for line in self.ephemeris[start:end]:
            ephemerisEntry = []
            ephemerisEntry.append(self.__dateParser.parse(line))
            ephemerisEntry += self.__positionParser.parse(line)
            ephemerisEntry.append(self.__deltaParser.parse(line))
            parsedEphemeris.append(ephemerisEntry)

        return parsedEphemeris

    def _findEphemerisRange(self, duration):
      now = TETimeUtil.unix2epoch(time.time()).value

      #We know that the Ephemeris Files must be uniformly spaced so:
      fileStart = self.__dateParser.parse(self.ephemeris[0])
      fileStep  = self.__dateParser.parse(self.ephemeris[1])-fileStart

      #Just to be safe give a couple of extra lines
      start = max((now - fileStart)/fileStep - 5, 0)
      end   = min(start + int(duration*1E7/fileStep) + 10, len(self.ephemeris))
      return (start, end)        

    def getEphemerisString(self, duration = None):
        ephemerisString = ""
        for line in self.getEphemeris(duration):
            ephemerisString += ("%s,%f,%f,%f;" % tuple(line))

        return ephemerisString

    def __createEphemerisString(self):
        self.__ephemerisString = ""
        for line in self.__parsedEphemeris:
            self.__ephemerisString += ("%s,%f,%f,%f;" % tuple(line))
        
    def loadEphemerisFile(self, filename):
        self.__parsedEphemeris= None
        self.__ephemerisString= None
        self._readFile(filename)
        (dateHeader, positionHeader, deltaHeader) = self._findHeaders()
        if dateHeader is None:
            raise ParserErrorEx("Could not determine Date Column(s)")
        if positionHeader is None:
            raise ParserErrorEx("Could not determine Position Columns")
        if deltaHeader is None:
            raise ParserErrorEx("Could not determine Delta Column")
        self._createPositionParser(positionHeader)
        self._createDeltaParser(deltaHeader)
        self._createDateParser(dateHeader)
        
    def _readFile(self, filename):
        file = open(filename,'r')
        lines = file.readlines()
        file.close()
        self.header = lines[lines.index('$$SOE\n')-2]
        self.ephemeris = lines[lines.index('$$SOE\n')+1:lines.index('$$EOE\n')]

    def _findHeaders(self):
        dateTitle     = None
        positionTitle = None
        deltaTitle    = None
        for title in self.header.split():
            if title[0:4] == 'Date' and dateTitle is None:
                dateTitle = title
            if title[0:4] == 'R.A.' and positionTitle is None:
                positionTitle = title
                if (positionTitle.split('_')[1] != '(ICRF/J2000.0)') and \
                       (positionTitle.split('_')[1] != '(J2000.0)'):
                    positionTitle = None
            if title == 'delta':
                deltaTitle = title
        return (dateTitle, positionTitle, deltaTitle)
        
    def _createPositionParser(self, positionHeader):
        if positionHeader == 'R.A._(ICRF/J2000.0)_DEC':
            self.__positionParser = SexagesimalPositionParser()
        elif positionHeader == 'R.A._(J2000.0)_DEC.':
            self.__positionParser = DecimalPositionParser()
        else:
            raise ParserErrorEx("Unrecognized position type: "+ positionHeader)

        dataStart = self.header.find(positionHeader)
        dataEnd   = dataStart + len(positionHeader)
        self.__positionParser.setDataRange(dataStart, dataEnd)
                                           
    def _createDeltaParser(self, deltaHeader):
        dataEnd   = self.header.find(deltaHeader) + len(deltaHeader)
        dataStart = dataEnd - 16
        self.__deltaParser = DeltaParser()
        self.__deltaParser.setDataRange(dataStart, dataEnd)

        # Horizons allows delta to be in AU or in km, make a guess at
        # which one it is.  Basically anything over 1E5 must be be in km
        if float(self.ephemeris[0][dataStart:dataEnd]) > 1E5:
            self.__deltaParser.setUnit("km")
        else:
            self.__deltaParser.setUnit("AU")

    def _createDateParser(self, dateHeader):
        if dateHeader == 'Date__(UT)__HR:MN':
            self.__dateParser = UTParserHM()
        elif dateHeader == 'Date__(UT)__HR:MN:SS':
            self.__dateParser = UTParserHMS()
        elif dateHeader == 'Date__(UT)__HR:MN:SC.fff':
            self.__dateParser = UTParserHMSF()
        elif dateHeader == 'Date_________JDUT':
            self.__dateParser = JDParser()
        else:
            raise ParserErrorEx("Unrecognized date type: "+ dateHeader)


        dataStart = self.header.find(dateHeader)
        dataEnd   = dataStart + len(dateHeader)
        self.__dateParser.setDataRange(dataStart, dataEnd)
