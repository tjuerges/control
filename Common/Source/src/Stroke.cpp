// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "Stroke.h"
#include <boost/shared_ptr.hpp>

// shortcuts to avoid having to clutter the code with namespace qualifiers
using Control::Stroke;

// The now time could be anything (but not a plausible observation time)
const ACS::Time nowTime = static_cast<ACS::Time>(0);

Stroke::Stroke()
  :startTime_m(nowTime),
   refDirection_m(),
   useRefDirection_m(false)
{}

Stroke::Stroke(const ACS::Time& startTime)
  :startTime_m(startTime),
   refDirection_m(),
   useRefDirection_m(false)
{}

Stroke::Stroke(const Stroke& other)
  :startTime_m(other.startTime_m),
   refDirection_m(other.refDirection_m),
   useRefDirection_m(other.useRefDirection_m)
{}

Stroke& Stroke::operator=(const Stroke& other) {
  if (this != &other) {
    startTime_m = other.startTime_m;
    refDirection_m = other.refDirection_m;
    useRefDirection_m = other.useRefDirection_m;
  }
  return *this;
}

Stroke::~Stroke()
{}

ACS::Time Stroke::getStartTime() const {
  return startTime_m;
}

bool Stroke::compareTime(const boost::shared_ptr<Stroke> x, 
                         const boost::shared_ptr<Stroke> y) {
  if (x->getStartTime() < y->getStartTime()) {
    return true;
  } else {
    return false;
  }
}

void Stroke::setRefDirection(const casa::MVDirection& refDirection) {
  refDirection_m = refDirection;
  useRefDirection_m = true;
}

casa::MVDirection Stroke::getRefDirection() const {
  return refDirection_m;
}

bool Stroke::isRefDirectionSet() const {
  return useRefDirection_m;
}

