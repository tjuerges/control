#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# sscott    2010-06-15  created
#
import Acspy.Clients.SimpleClient
import Acspy.Common.QoS
import Acspy.Util.ACSCorba
import math
import Control

from CCL.Container import getComponent
from CCL.logging import getLogger


class DopplerServer:
    """Doppler server
    """

    def __init__(self):
        """Constructor.
        """
        self.interrupted = False
        self.__ds = getComponent('CONTROL/DopplerServer')
        if self.__ds == None:
            print "Could not get DopplerServer component"
    
    def helloDoppler(self):
        return self.__ds.helloDoppler()    
    
    def status(self):
        return self.__ds.status()    

    def skyFreq(restFreq, velocity, ra, idec, epoch):
        """Takes a rest frequency and doppler shift it to a sky frequency
        """
        return self.__ds.skyFreq(restFreq, velocity, ra, idec, epoch)
    
    def interrupt(self):
        """Interrupts the doppler server.
        This function should be overriden by an asynchronous operation in
        child classes.
        """
        self.interrupted = True

    def isInterrupted(self):
        """Has this object been interrupted?
        If needed, this method can be overriden by child classes.
        """
        return self.interrupted

#
# __oOo__
