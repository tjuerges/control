// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2006, 2007, 2008, 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char* rcsId="@(#) $Id$"; 
static void* use_rcsId = ((void)&use_rcsId,(void *) &rcsId);


#include "dopplerServerImpl.h"

#include <casacore/casa/aips.h>                         // for casa::Double
#include <casacore/measures/Measures.h>    // 
#include <casacore/measures/Measures/MBaseline.h> 
#include <casacore/measures/Measures/MCBaseline.h> 
#include <casacore/measures/Measures/MDirection.h>      // for casa::MDirection
#include <casacore/measures/Measures/MCDirection.h>      // for casa::MDirection
#include <casacore/measures/Measures/MCFrequency.h>     //
#include <casacore/measures/Measures/MDoppler.h>        // for casa::MDoppler
#include <casacore/measures/Measures/MEpoch.h>          // for casa::MEpoch
#include <casacore/measures/Measures/MFrequency.h>      //
#include <casacore/measures/Measures/MPosition.h>       //
#include <casacore/measures/Measures/MeasJPL.h> // 

#include "acstimeEpochHelper.h"
#include <acsutilTimeStamp.h>  // For ::getTimeStamp().
#include <ACSErrTypeOK.h>
#include <CDopplerReferenceCode.h>
#include <ControlExceptions.h> // for IllegalParameterErrorExImpl
#include <CRadialVelocityReferenceCode.h>
#include <EphemerisInterpolator.h>
#include <TMCDBAccessIFC.h> // for TMCDB::Access


using std::string;
using std::ostringstream;
using Logging::BaseLog;
using casa::Bool;
using casa::Double;
using casa::MeasFrame;
using casa::MDirection;
using casa::MDoppler;
using casa::MEpoch;
using casa::MFrequency;
using casa::MPosition;
using casa::MVDirection;
using casa::MVEpoch;
using casa::MVFrequency;
using casa::MVPosition;
using casa::Quantity;
using casa::Unit;
using casa::Vector;
using casa::MeasJPL;
using casa::MBaseline;
using casa::MVBaseline;
using casa::Quantum;
using Control::DopplerServerImpl;
using Control::EphemerisInterpolator;


DopplerServerImpl::DopplerServerImpl(const ACE_CString& name,
		          maci::ContainerServices* containerServices):
    ACSComponentImpl(name, containerServices),
    resourceMutex_m(),
    C(299792.458)
{
    const string fnName = "DopplerServerImpl::DopplerServerImpl";
    AUTO_TRACE(fnName);
}

DopplerServerImpl::~DopplerServerImpl() {
    const string fnName = "DopplerServerImpl::~DopplerServerImpl";
    AUTO_TRACE(fnName);
}

// Helper function
MFrequency::Types convertRadialVelRefCodeToMType(
        RadialVelocityReferenceCodeMod::RadialVelocityReferenceCode vmode) {
    if (vmode == RadialVelocityReferenceCodeMod::LSRK) {
        return MFrequency::LSRK;
    }    
    else if (vmode == RadialVelocityReferenceCodeMod::LSRK) {
        return MFrequency::LSRK;
    }
    else if (vmode == RadialVelocityReferenceCodeMod::LSRD) {
        return MFrequency::LSRD;
    }
    else if (vmode == RadialVelocityReferenceCodeMod::GALACTO) {
        return MFrequency::GALACTO;
    }
    else if (vmode == RadialVelocityReferenceCodeMod::BARY) {
        return MFrequency::BARY;
    }
    else if (vmode == RadialVelocityReferenceCodeMod::GEO) {
        return MFrequency::GEO;
    }
    else if (vmode == RadialVelocityReferenceCodeMod::TOPO) {
        return MFrequency::TOPO;
    }
    // No match, gotta throw...
    ostringstream o;
    o << "Unable to convert RadialVelocityReferenceCodeMod::"
      << CRadialVelocityReferenceCode::name(vmode) << " to an MFrequency::Type";
    ControlExceptions::IllegalParameterErrorExImpl ex(
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
    ex.addData("Detail", o.str());
    ex.log();
    throw ex.getIllegalParameterErrorEx();
}

// Helper function
MDoppler::Types convertDopplerRefCodeToMType(
        DopplerReferenceCodeMod::DopplerReferenceCode calcMode) {
    if (calcMode == DopplerReferenceCodeMod::OPTICAL) {
        return MDoppler::OPTICAL;
    }    
    else if (calcMode == DopplerReferenceCodeMod::RADIO) {
        return MDoppler::RADIO;
    }
    else if (calcMode == DopplerReferenceCodeMod::RELATIVISTIC) {
        return MDoppler::RELATIVISTIC;
    }
    // No match, gotta throw...
    ostringstream o;
    o << "Unable to convert DopplerReferenceCodeMod::"
      << CDopplerReferenceCode::name(calcMode) << " to an MDoppler::Type";
    ControlExceptions::IllegalParameterErrorExImpl ex(
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
    ex.addData("Detail", o.str());
    ex.log();
    throw ex.getIllegalParameterErrorEx();
}

casa::MDirection::Types planetNameToEnum(const string& planetName) {
    // The default value should never be returned!
    casa::MDirection::Types planet(casa::MDirection::DEFAULT);

    // First convert string to upper case.
    string ucName(planetName);
    for (string::iterator p = ucName.begin(); p !=  ucName.end(); ++p) {
        *p = std::toupper(*p);
    }

    // now check against the list of "planets"
    if (ucName == "MERCURY") {
        planet = casa::MDirection::MERCURY;
    } else if (ucName == "VENUS") {
        planet = casa::MDirection::VENUS;
    } else if (ucName == "MARS") {
        planet = casa::MDirection::MARS;
    } else if (ucName == "JUPITER") {
        planet = casa::MDirection::JUPITER;
    } else if (ucName == "SATURN") {
        planet = casa::MDirection::SATURN;
    } else if (ucName == "URANUS") {
        planet = casa::MDirection::URANUS;
    } else if (ucName == "NEPTUNE") {
        planet = casa::MDirection::NEPTUNE;
    } else if (ucName == "PLUTO") {
        planet = casa::MDirection::PLUTO;
    } else if (ucName == "SUN") {
        planet = casa::MDirection::SUN;
    } else if (ucName == "MOON") {
        planet = casa::MDirection::MOON;
    } else {
        ControlExceptions::IllegalParameterErrorExImpl
            ex( __FILE__,__LINE__,__PRETTY_FUNCTION__);
        string plist = "Mercury, Venus, Mars, Jupiter, Saturn, Uranus,";
        plist += " Neptune,Pluto, Sun, Moon";
        ex.addData("ParameterName", "planetName");
        ex.addData("Value", planetName);
        ex.addData("ValidRange", plist);
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    return planet;
}


// Converts to the enum used in the JPL ephemeris code
casa::MeasJPL::Types planetNameToJPLenum(const string& planetName) {
    // The default value should never be returned!
    casa::MeasJPL::Types planet(casa::MeasJPL::EARTH);

    // First convert string to upper case.
    string ucName(planetName);
    for (string::iterator p = ucName.begin(); p !=  ucName.end(); ++p) {
        *p = std::toupper(*p);
    }

    // now check against the list of "planets"
    if (ucName == "MERCURY") {
        planet = casa::MeasJPL::MERCURY;
    } else if (ucName == "VENUS") {
        planet = casa::MeasJPL::VENUS;
    } else if (ucName == "MARS") {
        planet = casa::MeasJPL::MARS;
    } else if (ucName == "JUPITER") {
        planet = casa::MeasJPL::JUPITER;
    } else if (ucName == "SATURN") {
        planet = casa::MeasJPL::SATURN;
    } else if (ucName == "URANUS") {
        planet = casa::MeasJPL::URANUS;
    } else if (ucName == "NEPTUNE") {
        planet = casa::MeasJPL::NEPTUNE;
    } else if (ucName == "PLUTO") {
        planet = casa::MeasJPL::PLUTO;
    } else if (ucName == "SUN") {
        planet = casa::MeasJPL::SUN;
    } else if (ucName == "MOON") {
        planet = casa::MeasJPL::MOON;
    } else {
        ControlExceptions::IllegalParameterErrorExImpl
            ex( __FILE__,__LINE__,__PRETTY_FUNCTION__);
        string plist = "Mercury, Venus, Mars, Jupiter, Saturn, Uranus,";
        plist += " Neptune,Pluto, Sun, Moon";
        ex.addData("ParameterName", "planetName");
        ex.addData("Value", planetName);
        ex.addData("ValidRange", plist); 
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    return planet;
}


char* DopplerServerImpl::helloDoppler() {
    return CORBA::string_dup("I'm a shifty fellow!");
}

TMCDB::ArrayReferenceLocation DopplerServerImpl::getArrayLoc() {
    try {
        const char* tmcdbType = "IDL:alma/TMCDB/Access:1.0";
        //TMCDB::TMCDBComponent* tmcdb = getContainerServices()->
        //   getDefaultComponent<TMCDB::TMCDBComponent>(tmcdbType);
        maci::SmartPtr<TMCDB::Access> tmcdb = getContainerServices()->
                    getDefaultComponentSmartPtr<TMCDB::Access> (tmcdbType);

        /* Now that we have the TMCDB get the array reference location */
        return tmcdb->getArrayReferenceLocation();
    } catch (maciErrType::CannotGetComponentExImpl& ex) {
        maciErrType::CannotGetComponentExImpl nex(ex, 
            __FILE__, __LINE__,__func__);
        string msg = "Cannot connect to the TMCDB, this is a fatal error";
        nex.addData("Cause", msg);
        nex.log(LM_CRITICAL);
        //tmcdb_m.release();
        throw nex.getCannotGetComponentEx();
    }
}

double DopplerServerImpl::dopFactorEquatorial(ACS::Time epoch, double vel,
            RadialVelocityReferenceCodeMod::RadialVelocityReferenceCode vmode, 
            DopplerReferenceCodeMod::DopplerReferenceCode calcMode, 
            double ra, double dec, 
            double pmra, double pmdec, 
            double parallax) 
try {
    const bool dbug = false;
    TMCDB::ArrayReferenceLocation refLoc=getArrayLoc();
    MPosition obsloc(MVPosition(refLoc.x, refLoc.y, refLoc.z), MPosition::ITRF);
    cout << std::fixed;
    ostringstream o;
    o << std::fixed;
    o <<"ArrayRef: " << obsloc;
    LOG_TO_DEVELOPER(LM_DEBUG, o.str());

    // Log the input values
    Quantity epochMJD(EpochHelper(epoch).toMJDseconds()/86400.0, "d");
    MEpoch casaEpoch(epochMJD, MEpoch::UTC);
    o.str("");
    o << "Input: epoch=" << setprecision(0) << casaEpoch;
    o << " vel=" << setprecision(4) << vel ;
    o <<" vmode="<< CRadialVelocityReferenceCode::name(vmode);
    o << " calcMode=" << CDopplerReferenceCode::name(calcMode) ;
    o << "\n  ra=" << setw(9) << setprecision(6) << ra << " dec=" << dec;
    LOG_TO_DEVELOPER(LM_INFO, o.str());

    // Debugging
    if (false) cout << "DOPFAC-casaepoch:" << setprecision(7) << std::fixed
         << epoch << "  " << (epoch/(1e7*86400)) << "  "
         << epochMJD.getValue() << "  " 
         << EpochHelper(epoch).value().value << "   "
         << EpochHelper(epoch).toMJDseconds() << "   "
         << EpochHelper(epoch).toMJDseconds()/86400.0 << endl;

    MDirection dir(MVDirection(ra, dec), MDirection::J2000);
    MeasFrame mframe(obsloc, casaEpoch, dir); // Measures frame=>loc,time,src

    MDoppler::Types dopCode=convertDopplerRefCodeToMType(calcMode);
    MDoppler invel(Quantity(Double(vel),"km/s"), dopCode);
    MFrequency::Types inFrame=convertRadialVelRefCodeToMType(vmode);
    Double reffreq(100e9); // The reference freq is arbitrary (Hz)
    MVFrequency mreffreq(Quantity(reffreq,"Hz"));
    // Convert the rest freq to the requested frame, using the calcMode
    MFrequency stepOneFreq=MFrequency::fromDoppler(invel,mreffreq,inFrame); 
    if(dbug)cout << "DOPFAC-stepOneFreq: " << stepOneFreq << endl;   
    MFrequency::Types outFrame=MFrequency::TOPO;  // the telescope frame
    MFrequency::Ref outDir(outFrame, mframe);
    if(dbug)cout << "DOPFAC-outDir: " << outDir << endl;   
    MFrequency::Convert freqConv(inFrame, outDir);
    MFrequency outFreq = freqConv(stepOneFreq);
    Double f = outFreq.getValue().getValue(); 
    o.str("");
    o <<"DOPFAC-Final freq:" << fixed << setprecision(0)
      << f << " refFreq:" << reffreq
      << "  dopfac:" << setprecision(6) <<f/reffreq << endl; 
    LOG_TO_DEVELOPER(LM_DEBUG, o.str());
    if(dbug)cout << o.str() << endl; 
    double dopfac = f/reffreq;

    return dopfac;
} catch(std::exception& e) {
    ostringstream o;
    o << "Caught exception in dopFactorEquatorial():" << e.what() ;
    LOG_TO_OPERATOR(LM_ERROR, o.str())
    return 1.0;
} catch(...) {
    ostringstream o;
    o<< "Caught unknown exception in dopFactorEquatorial()" ;
    LOG_TO_OPERATOR(LM_ERROR, o.str())
    return 1.0;
}
// Get the length of vector
double getVectorLen(const double xyz[3]) {
    double x = xyz[0];
    double y = xyz[1];
    double z = xyz[2];
    return sqrt(x*x+y*y+z*z);
}
// Get the length of vector(used for the topocentric distance to planet)
double getVectorLength(const Vector<Double>& p) {
    double xyz[3];
    xyz[0] = p[0];
    xyz[1] = p[1];
    xyz[2] = p[2];
    return getVectorLen(xyz);
}
// Convert a geocentric object position vector (doubles,km) with a celestial
// reference to a topocentric vector with an earth ITRF reference, and then
// get its length. 
// First the geocentric vector is converted to the ITRF frame,
// then the obsveratory position vector (also in ITRF) is subtacted, 
// and then the vector length is computed.
double getTopoDistanceKm(double xyz[3], MeasFrame frame) {
    const bool debug = false;
    MBaseline::Ref ref(MBaseline::APP, frame);
    MBaseline b(MVBaseline(Double(xyz[0]),Double(xyz[1]),Double(xyz[2])), ref);

    MBaseline::Convert bconv(b, MBaseline::ITRF);
    MBaseline srcITRF = bconv(b);
    MPosition obsloc = (MPosition)frame.position();
    Vector<Double> obsvec = obsloc.get("km").getValue();
    // Get as meters because it defaulted to m when it went in
    Vector<Double> srcITRFvec  = srcITRF.get("m").getValue();
    Vector<Double> topovec = srcITRFvec - obsvec;
    if (debug) {
        cout << setprecision(3) << std::fixed;
        cout << "DOPFAC-xyz: " << setprecision(0) 
             << xyz[0] << "  " << xyz[1]   << "  " << xyz[2]  << endl;
        cout << "DOPFAC-frame: " << frame << endl;
        cout << setprecision(0) << "DOPFAC-srcGEO: " << b << endl;
        cout << setprecision(0) << "DOPFAC-srcGEOdist: " 
             << getVectorLen(xyz) << endl;
        cout << setprecision(0) << "DOPFAC-sourceITRF: " << srcITRF << endl;
        cout<<setprecision(0)<< "DOPFAC-srcITRF:"<<srcITRFvec << endl;
        cout<<setprecision(0)<< "DOPFAC-obsloc:"<<obsvec << endl;
        cout<<setprecision(0)<< "DOPFAC-topovec:"<<topovec << endl;
    }
    return getVectorLength(topovec);
}

// Take a Vector<Double> AU and convert to an array of doubles km.
// Also return the vector length in case you want it.
double convertDistanceAU2KM(Vector<Double> pos, double xyz[3]) {
    Quantity qx(pos[0], "AU");
    Quantity qy(pos[1], "AU");
    Quantity qz(pos[2], "AU");
    qx.convert("km"); qy.convert("km"); qz.convert("km");
    xyz[0] = qx.getValue();
    xyz[1] = qy.getValue();
    xyz[2] = qz.getValue();
    return getVectorLen(xyz);
}
// Take a geocentric planet position in AU and get its topocentric distance
double getTopoDistanceAU(Vector<Double> geopos, MeasFrame frame) {
    double xyz[3];
    convertDistanceAU2KM(geopos, xyz);
    return getTopoDistanceKm(xyz, frame);
}

Vector<Double> getJPLgeoPos(string planetName, double t0) {
    const double C(299792.458);
    const bool debug = false;
    MeasJPL::Types jplPlanet(planetNameToJPLenum(planetName));   
    // results of ephem lookup for pla & earth; x,y,z,vx,vy,vz
    Vector<Double> val(6), valE(6); 
    MEpoch epoch = MEpoch(Quantity(t0/86400.0, "d"), MEpoch::UTC);
    MVEpoch ep = epoch.getValue();
    // Get position and velocity of planet (barycentric) into val
    if (!MeasJPL::get(val, MeasJPL::DE405, jplPlanet, ep)) {
        ostringstream o;
        o << "Error getting planet position from ephemeris";
        LOG_TO_OPERATOR(LM_ERROR, o.str())
        ControlExceptions::IllegalParameterErrorExImpl
                ex(__FILE__, __LINE__, __func__);
        ex.addData("Detail", o.str());
        throw ex.getIllegalParameterErrorEx();
    } 
    // Get Earth position and velocity (barycentric) into valE
    if (!MeasJPL::get(valE, MeasJPL::DE405, MeasJPL::EARTH, ep)) {
        ostringstream o;
        o << "Error getting Earth position from ephemeris";
        LOG_TO_OPERATOR(LM_ERROR, o.str())
        ControlExceptions::IllegalParameterErrorExImpl
                ex(__FILE__, __LINE__, __func__);
        ex.addData("Detail", o.str());
        throw ex.getIllegalParameterErrorEx();
    } 
    // Compute light travel time from Obj to Earth
    double xyz[3];
    double ltObjEarth  = convertDistanceAU2KM(val-valE, xyz)/C;
    // Correct time of ephemeris data for the light travel time
    epoch = MEpoch(Quantity((t0-ltObjEarth)/86400.0, "d"), MEpoch::UTC);
    ep = epoch.getValue();
    // Get ephem data again for corrected time
    if (!MeasJPL::get(val, MeasJPL::DE405, jplPlanet, ep)) {
        ostringstream o;
        o << "Error getting light travel time adjusted planet position "
          << "from ephemeris";
        LOG_TO_OPERATOR(LM_ERROR, o.str())
        ControlExceptions::IllegalParameterErrorExImpl
                ex(__FILE__, __LINE__, __func__);
        ex.addData("Detail", o.str());
        throw ex.getIllegalParameterErrorEx();
    } 
    if (!MeasJPL::get(valE, MeasJPL::DE405, MeasJPL::EARTH, ep)) {
        ostringstream o;
        o << "Error getting light travel time corrected Earth position"
          << " from ephemeris";
        LOG_TO_OPERATOR(LM_ERROR, o.str())
        ControlExceptions::IllegalParameterErrorExImpl
                ex(__FILE__, __LINE__, __func__);
        ex.addData("Detail", o.str());
        throw ex.getIllegalParameterErrorEx();
    }
    if (debug) {    
        //cout<<fixed<<setprecision(9)<< "DOPFAC-jplSource:"<< val << endl;
        //cout<<fixed<<setprecision(9)<< "DOPFAC-jplEarth:"<< valE << endl;
        //cout<<fixed<<setprecision(9)<< "DOPFAC-jplSrc-Erth:"<< val-valE << endl;
        cout<<setprecision(0)<< "DOPFAC-jplSourceR:"
            << convertDistanceAU2KM(val, xyz) << endl;
        cout<<setprecision(0)<< "DOPFAC-jplEarthR:"
            << convertDistanceAU2KM(valE, xyz) << endl;
        cout<<setprecision(0)<< "DOPFAC-jplSource-EarthR:"
            << convertDistanceAU2KM(val-valE, xyz) << endl;
    }
    // Return difference between object and earth
    return val-valE;
}    
double DopplerServerImpl::dopFactorPlanet(ACS::Time epoch, 
            const char* planetName) 
try {
    const bool dbug = true;
    TMCDB::ArrayReferenceLocation refLoc=getArrayLoc();
    MPosition obsloc(MVPosition(refLoc.x, refLoc.y, refLoc.z), MPosition::ITRF);
    cout << std::fixed;
    ostringstream o;
    o << std::fixed;
    o <<"ArrayRef: " << obsloc;
    LOG_TO_DEVELOPER(LM_DEBUG, o.str());

    // Log the input values
    Quantity epochMJD(EpochHelper(epoch).toMJDseconds()/86400.0, "d");
    MEpoch casaEpoch(epochMJD, MEpoch::UTC);
    o.str("");
    o << "Input: epoch=" << setprecision(0) << casaEpoch;
    o << " planet=" << planetName;
    LOG_TO_DEVELOPER(LM_INFO, o.str());

    // The velocity of the planet is calculated by computing the topocentric
    // distance at two closely spaced epochs and taking the difference.
    // Here we set up the two epochs and frames needed for the computation.

    double deltaTime = 60.0; // Time diff from nominal for the epochs (sec)
    double t0 = EpochHelper(epoch).toMJDseconds(); // Nominal time
    double t1 = t0-deltaTime;
    double t2 = t0+deltaTime;
    MEpoch   epoch1(Quantity(t1/86400.0, "d"), MEpoch::UTC);
    MEpoch   epoch2(Quantity(t2/86400.0, "d"), MEpoch::UTC);
    MDirection dir(planetNameToEnum(planetName));
    MeasFrame frame1(obsloc, epoch1, dir);  // Measures frame=>loc,time,src
    MeasFrame frame2(obsloc, epoch2, dir);  // Measures frame=>loc,time,src

    double r1 = getTopoDistanceAU(getJPLgeoPos(planetName, t1), frame1);
    double r2 = getTopoDistanceAU(getJPLgeoPos(planetName, t2), frame2);
    double vel = (r2-r1)/(deltaTime*2);

    const double beta = vel/C;

    double dopfac = sqrt((1-beta)/(1+beta));
    o.str("");
    o <<"DOPFAC-Final: "  << fixed << setprecision(4) 
      << "vel:" << vel 
      << setprecision(6) << "   dopfac:" << dopfac; 
    LOG_TO_DEVELOPER(LM_DEBUG, o.str())
    if (dbug) cout << o.str() << endl;
    return dopfac;
} catch(std::exception& e) {
    ostringstream o;
    o << "Caught std::exception in dopFactorPlanet():" << e.what() ;
    LOG_TO_OPERATOR(LM_ERROR, o.str())
    return 1.0;
} catch(...) {
    ostringstream o;
    o<< "Caught unknown exception in dopFactorPlanet()" ;
    LOG_TO_OPERATOR(LM_ERROR, o.str())
    return 1.0;
}

// Take ephemeris interpolator and produce topocentric distance in KM
double getTopoEphemDistanceKm(MPosition& obsloc, 
        EphemerisInterpolator ephemInterpolator, ACS::Time t)
try {
    const bool debug = false;
    double ra;
    double dec;
    double rho; // In m
    double xyz[3];
      
    double tmjdsec = EpochHelper(t).toMJDseconds(); // Nominal time
    MEpoch epoch(Quantity(tmjdsec/86400.0, "d"), MEpoch::UTC);
    ephemInterpolator.getPosition(t, ra, dec, rho);
    MDirection dirin(Quantity(ra,"rad"),Quantity(dec,"rad"), MDirection::J2000);
    MeasFrame frame(obsloc, epoch, dirin);  // MeasuresFrame=>loc,time,src
    // Convert directions to apparent geocentric 
    MDirection::Ref ref(MDirection::J2000, frame);
    MDirection::Convert mdconv(ref, MDirection::Ref(MDirection::APP, frame));
    MDirection dir = mdconv(dirin);

    // Get geocentric unit vector (and they are unit vecs, I checked)
    Vector<Double> XYZ = dir.getValue().getValue();
    // Multiply directions by the distance to get geocentric vector
    // and convert from meters to km
    for (int i=0; i<3; i++) xyz[i] = rho/1000.0*XYZ[i]; 
    // Convert geocentric pos to a topocentric distance (still in km)
    double r = getTopoDistanceKm(xyz, frame);
    if (debug) {
        cout << "DOPFAC-epoch: " << epoch << endl;
        cout << "DOPFAC-ra: " << setprecision(7) << ra << " dec: " << dec 
            << setprecision(0)<< " rho: " << rho << endl;
        cout <<setprecision(8) << "DOPFAC-srcJ2000: " << dirin << endl;
        cout << "DOPFAC-ref: " << ref << endl;
        cout << "DOPFAC-conv: " << mdconv << endl;
        cout <<setprecision(8) << "DOPFAC-srcApp: " << dir << endl;
        cout << "DOPFAC-XYZ: " << setprecision(8) << XYZ << endl;
        cout << setprecision(0)<< "DOPFAC-x: "  << xyz[0]
             << " y=" << xyz[1] << " z=" << xyz[2] << endl;
        cout << setprecision(0)<< "DOPFAC-Rgeo: "  << getVectorLen(xyz) << endl;
        cout << setprecision(0)<< "DOPFAC-Rtopo: "  << r << endl;
    }
    return r;
} catch(std::exception& e) {
    ostringstream o;
    o << "Caught std::exception in getEphemDistanceKm():" << e.what() ;
    LOG_TO_OPERATOR(LM_ERROR, o.str())
    return 1.0;
} catch(...) {
    ostringstream o;
    o<< "Caught unknown exception in getEphemDistanceKm()" ;
    LOG_TO_OPERATOR(LM_ERROR, o.str())
    return 1.0;
}
  
double DopplerServerImpl::dopFactorEphemeris(ACS::Time epoch, 
        const Control::Ephemeris& ephemeris) 
try {
    const bool dbug = false;
    TMCDB::ArrayReferenceLocation refLoc=getArrayLoc();
    MPosition obsloc(MVPosition(refLoc.x, refLoc.y, refLoc.z), MPosition::ITRF);
    cout << std::fixed;
    ostringstream o;
    o << std::fixed;
    o <<"ArrayRef: " << obsloc;
    LOG_TO_DEVELOPER(LM_DEBUG, o.str());

    // Log the input values
    Quantity epochMJD(EpochHelper(epoch).toMJDseconds()/86400.0, "d");
    MEpoch casaEpoch(epochMJD, MEpoch::UTC);
    o.str("");
    o << "Input: epoch=" << setprecision(0) << casaEpoch;
    o << "  ephemeris(not shown)" ;
    LOG_TO_DEVELOPER(LM_INFO, o.str());
    
    EphemerisInterpolator ephemInterpolator(ephemeris);

    double deltaTime = 60.0; // Time diff from nominal for the epochs (sec)
    long double t0 = EpochHelper(epoch).toMJDseconds(); // Nom time in MJD secs
    long double tmjdsecs = t0-deltaTime;
    EpochHelper ephelper(tmjdsecs); 
    ACS::Time time1 = ephelper.value().value;
    tmjdsecs = t0+deltaTime;
    ephelper.value(tmjdsecs); 
    ACS::Time time2 = ephelper.value().value;
    //long double t1 = EpochHelper(time1).toMJDseconds(); 
    //long double t2 = EpochHelper(time2).toMJDseconds(); 
    //cout << "DOPFAC-epoch: "    << epoch << "  t0: " << t0 << endl;
    //cout << "DOPFAC-acstime1: " << time1 << "  t1: " << t1 << endl;
    //cout << "DOPFAC-acstime2: " << time2 << "  t2: " << t2 << endl;

    double r1 = getTopoEphemDistanceKm(obsloc, ephemInterpolator, time1);
    double r2 = getTopoEphemDistanceKm(obsloc, ephemInterpolator, time2);
    double vel = (r2-r1)/(deltaTime*2);

    const double beta = vel/C;

    double dopfac = sqrt((1-beta)/(1+beta));
    o.str("");
    o <<"DOPFAC-Final: "  <<  fixed << setprecision(4)  << "vel:" << vel 
      << setprecision(6) << "   dopfac:" << dopfac ; 
    LOG_TO_DEVELOPER(LM_DEBUG, o.str());
    if(dbug)cout << o.str() << endl; 
    return dopfac;
} catch(std::exception& e) {
    ostringstream o;
    o << "Caught exception in dopFactorEphemeris():" << e.what() ;
    LOG_TO_OPERATOR(LM_ERROR, o.str())
    return 1.0;
} catch(...) {
    ostringstream o;
    o<< "Caught unknown exception in dopFactorEphemeris()" ;
    LOG_TO_OPERATOR(LM_ERROR, o.str())
    return 1.0;
}
 
char* DopplerServerImpl::status() 
try {
    ostringstream o;
    o << std::fixed;
    TMCDB::ArrayReferenceLocation refLoc=getArrayLoc();
    o << "Loc: " << setprecision(3) 
      << setw(12) << refLoc.x << ", "
                  << refLoc.y << ", "
                  << refLoc.z;
    return CORBA::string_dup(o.str().c_str()); 
} catch(std::exception& e) {
    ostringstream o;
    o << "Caught exception in DopplerServerImpl::status():" << e.what() ;
    LOG_TO_OPERATOR(LM_ERROR, o.str())
    return CORBA::string_dup("FAILED!!");
} catch(...) {
    ostringstream o;
    o<< "Caught unknown exception in DopplerServerImpl::status()" ;
    LOG_TO_OPERATOR(LM_ERROR, o.str())
    return CORBA::string_dup("FAILED!!");
}

void DopplerServerImpl::cleanUp() {
    const string fnName = "DopplerServerImpl::cleanUp";
    AUTO_TRACE(fnName);
    //releaseReferences();
    acscomponent::ACSComponentImpl::cleanUp();
}

/////////////////////////////////////////////////
// MACI DLL support functions
/////////////////////////////////////////////////
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(DopplerServerImpl)
