#! /usr/bin/env python
#
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2009
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# $Id$
#

import sys
import CCL.DopplerServer
from CCL.logging import getLogger
from Acspy.Clients.SimpleClient import PySimpleClient

compName = "IDL:alma/Control/DopplerServer:1.0"
client   = None
dopserve = None

def setUp():
    global client, dopserve
    client = PySimpleClient()
    dopserve = client.getDynamicComponent("MyDop", compName, None, None)

def cleanUp():
    client.releaseComponent(compName)

def test1() :
    s = dopserve.helloDoppler()
    match = (s == "I'm a shifty fellow!")
    #print s, match
    return match
    
def runtests() :
    setUp()
    f = test1()
    cleanUp()
    if f == True:      
        print "PASSED"
    else :
        print "FAILED"
    if __name__ == '__main__':
        if f == True:
            sys.exit(0)
        else:
            sys.exit(-1)
    else :
        return f        

                
if __name__ == '__main__':
    pass
    runtests()

    
