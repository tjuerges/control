#ifndef DOPPLERSERVERIMPL_H
#define DOPPLERSERVERIMPL_H
// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2005, 2006, 2007, 2008, 2009
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

// Base class(es)
#include <acscomponentImpl.h>

//CORBA servant header
#include <DopplerServerS.h>

//
#include <acsComponentSmartPtr.h> // for maci::SmartPtr
#include <TMCDBComponentC.h>

// Forward declarations for classes that this component uses
class maci::ContainerServices;


namespace Control { 
    class DopplerServerImpl: 
        public virtual POA_Control::DopplerServer,
        public acscomponent::ACSComponentImpl
    {
    public:
        /// The constructor for any ACS C++ component must have this signature.
        DopplerServerImpl(const ACE_CString& name, maci::ContainerServices* cs);

        /// The destructor ensures that the references to the hardware
        /// components are released.
        virtual ~DopplerServerImpl();

        /// This component lifecycle method ensures that the references to the
        /// hardware components have been released.
        virtual void cleanUp();

        /// See idl for docs
        double dopFactorEquatorial(ACS::Time epoch, double vel,
            RadialVelocityReferenceCodeMod::RadialVelocityReferenceCode vmode, 
            DopplerReferenceCodeMod::DopplerReferenceCode calcMode, 
            double ra, double dec, 
            double pmra, double pmdec, 
            double parallax);

        /// See idl for docs
        double dopFactorPlanet(ACS::Time epoch, const char* planetName);

        /// See idl for docs
        double dopFactorEphemeris(ACS::Time epoch,  
            const Control::Ephemeris&);

        // Testing
        char* helloDoppler();
        char* status();

    private:
        // The copy constructor is made private to prevent a compiler generated
        // one from being used. It is not implemented.
        DopplerServerImpl(const DopplerServerImpl& other);

        // This mutex serialises access to the allocate and deallocate
        // functions. It ensures that these functions are run to completion
        // before any other client can run them.
        ACE_Thread_Mutex resourceMutex_m;
        // Speed of light
        const double C;

        TMCDB::ArrayReferenceLocation getArrayLoc();

    };
} // end Control namespace

#endif // DopplerServerImpl_H
