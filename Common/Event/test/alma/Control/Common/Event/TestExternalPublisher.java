/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File TestEvent.java
 * 
 * @author Steve Harrington
 */
package alma.Control.Common.Event;

import junit.framework.AssertionFailedError;
import alma.acs.nc.Consumer;
import alma.acs.component.client.ComponentClientTestCase;


import alma.Control.AlmaCorrelatorOperationalModeEvent;
import alma.Control.AlmaCorrelatorTestModeEvent;
import alma.Control.AntennaOfflineEvent;
import alma.Control.AntennaOnlineEvent;
import alma.Control.Completion;
import alma.Control.ControlSystemChangeOfStateEvent;
import alma.Control.CreatedAutomaticArrayEvent;
import alma.Control.CreatedManualArrayEvent;
import alma.Control.DestroyedAutomaticArrayEvent;
import alma.Control.DestroyedManualArrayEvent;
import alma.Control.ExecBlockEndedEvent;
import alma.Control.ExecBlockStartedEvent;
import alma.Control.ScanEndedEvent;
import alma.Control.ScanPurpose;
import alma.Control.ScanStartedEvent;
import alma.Control.SubscanEndedEvent;
import alma.Control.SubscanStartedEvent;
import alma.Control.Common.ExternalPublisher;
import alma.Control.Common.Name;
import alma.Control.Common.Result;
import alma.Control.Common.State;
import alma.Control.ResultAction;
import alma.asdmIDLTypes.IDLEntityRef;

/**
 * TestExternalPublisher class tests the functionality of the alma.Control.Common.ExternalPublisher class.
 * This class functions as an ACS CORBA client and also a junit test case (because it extends ComponentClientTestCase).
 */
public class TestExternalPublisher extends ComponentClientTestCase 
{
	// all of the 'receive' methods function as callbacks 
	// which are called when an event of the proper type is received
	public void receive(ControlSystemChangeOfStateEvent e) {
		controlSystemChangeOfStateEventsReceived++;
		m_logger.info("Got ControlSystemChangeOfStateEvent.");
    }
    public void receive(CreatedAutomaticArrayEvent e) {
    	createdAutomaticArrayEventsReceived++;
    	m_logger.info("Got CreatedAutomaticArrayEvent.");
    }
    public void receive(DestroyedAutomaticArrayEvent e) {
    	destroyedAutomaticArrayEventsReceived++;
    	m_logger.info("Got DestroyedAutomaticArrayEvent.");
    }
    public void receive(CreatedManualArrayEvent e) {
    	createdManualArrayEventsReceived++;
    	m_logger.info("Got CreatedManualArrayEvent.");
    }
    public void receive(DestroyedManualArrayEvent e) {
    	destroyedManualArrayEventsReceived++;
    	m_logger.info("Got DestroyedManualArrayEvent.");
    }
    public void receive(AntennaOfflineEvent e) {
    	antennaOfflineEventsReceived++;
    	m_logger.info("Got AntennaOfflineEvent.");
    }
    public void receive(AntennaOnlineEvent e) {
    	antennaOnlineEventsReceived++;
    	m_logger.info("Got AntennaOnlineEvent.");
    }
    public void receive(AlmaCorrelatorTestModeEvent e) {
    	almaCorrelatorTestModeEventsReceived++;
    	m_logger.info("Got AlmaCorrelatorTestModeEvent.");
    }
    public void receive(AlmaCorrelatorOperationalModeEvent e) {
    	almaCorrelatorOperationalModeEventsReceived++;
    	m_logger.info("Got AlmaCorrelatorOperationalModeEvent.");
    }
    public void receive(ExecBlockStartedEvent e) {
    	execBlockStartedEventsReceived++;
    	m_logger.info("Got ExecBlockStartedEvent.");
    }
    public void receive(ExecBlockEndedEvent e) {
    	execBlockEndedEventsReceived++;
    	m_logger.info("Got ExecBlockEndedEvent.");
    }
    public void receive(ScanStartedEvent e) {
    	scanStartedEventsReceived++;
    	m_logger.info("Got ScanStartedEvent.");
    }
    public void receive(ScanEndedEvent e) {
    	scanEndedEventsReceived++;
    	m_logger.info("Got ScanEndedEvent.");
    }
    public void receive(SubscanStartedEvent e) {
    	subscanStartedEventsReceived++;
    	m_logger.info("Got SubscanStartedEvent.");
    }
    public void receive(SubscanEndedEvent e) {
    	subscanEndedEventsReceived++;
        m_logger.info("Got SubscanEndedEvent.");
    }
    
	/**
	 * Constructor.
	 */
	public TestExternalPublisher() throws Exception 
	{
		super(TestExternalPublisher.class.getName());

		// in order to keep track of the number of each type of
		// event that has been received, we will keep a counters for each,
		// which we initialize here to zero.
		controlSystemChangeOfStateEventsReceived = 0;
		createdAutomaticArrayEventsReceived = 0;
		destroyedAutomaticArrayEventsReceived = 0;
		createdManualArrayEventsReceived = 0;
		destroyedManualArrayEventsReceived = 0;
		antennaOfflineEventsReceived = 0;
		antennaOnlineEventsReceived = 0;
		almaCorrelatorTestModeEventsReceived = 0;
		almaCorrelatorOperationalModeEventsReceived = 0;
		execBlockStartedEventsReceived = 0;
		execBlockEndedEventsReceived = 0;
		scanStartedEventsReceived = 0;
		scanEndedEventsReceived = 0;
		subscanStartedEventsReceived = 0;
		subscanEndedEventsReceived = 0;
	}

	/**
	 * Required method of ComponentClientTestCase for setting up before junit test(s) run(s).
	 */
	protected void setUp() throws Exception
	{
		// call the superclass setUp method
		super.setUp();
		
		// create and configure the ExternalPublisher
        myExternalPublisher = new ExternalPublisher (getContainerServices());
        m_logger.info("External Control notification channel constructed.");
		
		// create the Consumer
		myConsumer = new Consumer(Name.NCExternal, getContainerServices());

		// configure the Consumer; add subscriptions for all the events we wish to receive (and for which
		// we have implemented a 'receive' method to handle the events - the receive methods are in this class 
		// (which is why we pass 'this' as the second argument in the Consumer.addSubscription method).
		myConsumer.addSubscription(alma.Control.AlmaCorrelatorOperationalModeEvent.class, this);
		myConsumer.addSubscription(alma.Control.AlmaCorrelatorTestModeEvent.class, this);
		myConsumer.addSubscription(alma.Control.AntennaOfflineEvent.class, this);
		myConsumer.addSubscription(alma.Control.AntennaOnlineEvent.class, this);
		myConsumer.addSubscription(alma.Control.ControlSystemChangeOfStateEvent.class, this);
		myConsumer.addSubscription(alma.Control.CreatedAutomaticArrayEvent.class, this);
		myConsumer.addSubscription(alma.Control.CreatedManualArrayEvent.class, this);
		myConsumer.addSubscription(alma.Control.DestroyedAutomaticArrayEvent.class, this);
		myConsumer.addSubscription(alma.Control.DestroyedManualArrayEvent.class, this);
		myConsumer.addSubscription(alma.Control.ExecBlockEndedEvent.class, this);
		myConsumer.addSubscription(alma.Control.ExecBlockStartedEvent.class, this);
		myConsumer.addSubscription(alma.Control.ScanEndedEvent.class, this);
		myConsumer.addSubscription(alma.Control.ScanStartedEvent.class, this);
		myConsumer.addSubscription(alma.Control.SubscanEndedEvent.class, this);
		myConsumer.addSubscription(alma.Control.SubscanStartedEvent.class, this);
		myConsumer.consumerReady();
	}

	/**
	 * Required method of ComponentClientTestCase for tearing down after junit test(s) run(s).
	 */
	protected void tearDown() throws Exception
	{
		myExternalPublisher.disconnect();
		myConsumer.disconnect();
		super.tearDown();
	}
	
	/**
	 * Junit test method; Junit automatically runs all methods with the word 'test' prepended to them.
	 */
	public void testEventPublishing() throws Exception
	{
		// publish each event type NUM_OF_EACH_EVENT_TYPE_TO_PUBLISH times, by looping
		for (int i = 0; i < NUM_OF_EACH_EVENT_TYPE_TO_PUBLISH; i++)
		{
			// publish events.   
			State state = new State (State.Operational_NoError);
			myExternalPublisher.publishStateChange(state, false, 0L);
	       
			String[] list = new String [3];
			list[0] = "ALMA001"; list[1] = "ALMA002"; list [2] = "ALMA003";
			myExternalPublisher.publishCreatedAutomaticArray("array", "dcname", list, 0L);
	       
			myExternalPublisher.publishDestroyedAutomaticArray("array", 0L);
	       
			myExternalPublisher.publishCreatedManualArray("array", "me", list, 0L);
	       
			myExternalPublisher.publishDestroyedManualArray("array", 0L);
	       
			myExternalPublisher.publishAntennaOffline("ALMA001", 0L);
	   
			myExternalPublisher.publishAntennaOnline("ALMA001", 0L);
	   
			myExternalPublisher.publishAlmaCorrelatorTestMode(true, true,  true,  true, 0L);
	
			myExternalPublisher.publishAlmaCorrelatorOperationalMode(true, true,  true,  true, 0L);

			IDLEntityRef execId = new IDLEntityRef();
			execId.entityId = "execId";
			execId.entityTypeName = "";
			execId.instanceVersion = "1.0";
			execId.partId = "";

			IDLEntityRef sbId = new IDLEntityRef();
			sbId.entityId = "execId";
			sbId.entityTypeName = "";
			sbId.instanceVersion = "1.0";
			sbId.partId = "";			

			IDLEntityRef sessionId = new IDLEntityRef();
			sessionId.entityId = "execId";
			sessionId.entityTypeName = "";
			sessionId.instanceVersion = "1.0";
			sessionId.partId = "";						
			
			myExternalPublisher.publishExecStarted(execId, sbId, sessionId, "array", 0L);
	   
			Result result = new Result (Completion.SUCCESS, ResultAction.CONTINUE);
			myExternalPublisher.publishExecEnded(execId, sbId, sessionId, "array", result, 0L);
	
			ScanPurpose[] purpose = new ScanPurpose [2];
			purpose[0] = ScanPurpose.FOCUS; purpose[1] = ScanPurpose.TARGET;
			myExternalPublisher.publishScanStarted(execId, "array", 3, purpose, 0L);
	   
			myExternalPublisher.publishScanEnded(execId, "array", 3, result, 0L);
	   
			myExternalPublisher.publishSubscanStarted(execId, "array", 3, 2, 0L);
	   
			myExternalPublisher.publishSubscanEnded(execId, "array", 3, 2, result, 0L);        
		}
		
		// now that all the events have been published, we need to verify that we can/have receive/received them all.
		verifyEventsWereReceived(NUM_OF_EACH_EVENT_TYPE_TO_PUBLISH);
	}
	
	/**
	 * Private method to make sure we received all the events we expected to receive.
	 * @param numExpected the total number (of each type of event) that we expect to receive
	 * @throws AssertionFailedError if we do not receive the expected events in the allotted time.
	 */
	private void verifyEventsWereReceived(int numExpected) throws AssertionFailedError
	{
		boolean success = false;

		// set the timeout to 10 seconds - this should be sufficient unless we 
		// raise NUM_OF_EACH_EVENT_TYPE_TO_PUBLISH to something very large
		int loopTimeout = 10000;

		// loop until either we have accounted for all the events we expect to receive OR
		// we timeout (and fail), whichever happens first.
		while(loopTimeout >= 0 && false == success)
		{
			// events are processed asynchronously so this is why we must wait and keep checking for the events to arrive
			try {
				// sleep 100 mseconds 
				Thread.sleep(100);
				loopTimeout -= 100;
				
				// assert that things are ok
				assertEquals("Number of AlmaCorrelatorOperationalModeEvents received not correct.",	
						numExpected, almaCorrelatorOperationalModeEventsReceived);
				assertEquals("Number of AlmaCorrelatorTestModeEvents received not correct.",	
						numExpected, almaCorrelatorTestModeEventsReceived);
				assertEquals("Number of AntennaOfflineEvents received not correct.",	
						numExpected, antennaOfflineEventsReceived);
				assertEquals("Number of AntennaOnlineEvents received not correct.",	
						numExpected, antennaOnlineEventsReceived);
				assertEquals("Number of ControlSystemChangeOfStateEvents received not correct.",	
						numExpected, controlSystemChangeOfStateEventsReceived);
				assertEquals("Number of CreatedAutomaticArrayEvents received not correct.",	
						numExpected, createdAutomaticArrayEventsReceived);
				assertEquals("Number of CreatedManualArrayEvents received not correct.",	
						numExpected, createdManualArrayEventsReceived);
				assertEquals("Number of DestroyedAutomaticArrayEvents received not correct.",	
						numExpected, destroyedAutomaticArrayEventsReceived);
				assertEquals("Number of DestroyedManualArrayEvents received not correct.",	
						numExpected, destroyedManualArrayEventsReceived);
				assertEquals("Number of ExecBlockStartedEvents received not correct.",	
						numExpected, execBlockStartedEventsReceived);
				assertEquals("Number of ExecBlockEndedEvents received not correct.",	
						numExpected, execBlockEndedEventsReceived);
				assertEquals("Number of ScanStartedEvents received not correct.",	
						numExpected, scanStartedEventsReceived);
				assertEquals("Number of ScanEndedEvents received not correct.",	
						numExpected, scanEndedEventsReceived);
				assertEquals("Number of subscanStartedEvents received not correct.",	
						numExpected, subscanStartedEventsReceived);
				assertEquals("Number of subscanEndedEvents received not correct.",	
						numExpected, subscanEndedEventsReceived);
				
				// if we reached this point w/o getting an AssertionFailedError.
				// set a flag indicating we can break out of loop, as all events are accounted for
				success = true;
			}
			catch(InterruptedException e)
			{
				m_logger.info("Sleep was interrupted for some reason.");
			}
			catch(AssertionFailedError ex)
			{
				if(loopTimeout == 0) {
					// we have timed out & also failed to get all the events; this is an exception 
					// so we rethrow to force our junit test to fail w/ an error.
					throw ex;
				}
				// we have failed to account for all the events, but we haven't yet timed out, so
				// we will keep checking (continuing the loop) until either we account for all events or
				// we finally timeout and fail
				m_logger.info("timeout not yet reached, and events not fully accounted for... continuing.");
			}
		}
		m_logger.info("successfully accounted for all events after: " + (10000 - loopTimeout) + " milliseconds");
	}

	// this constant dictates how many events (of each type) we will publish
	private final static int NUM_OF_EACH_EVENT_TYPE_TO_PUBLISH = 500;

	// the ExternalPublisher is the actual class that we are testing in this test case
	private ExternalPublisher myExternalPublisher = null;

	// we use the Consumer class to receive the events that the ExternalPublisher publishes
	private Consumer myConsumer = null;

	// for each type of event, we use a counter to track how many we actually have received/accounted for
	private int controlSystemChangeOfStateEventsReceived;
	private int createdAutomaticArrayEventsReceived;
	private int destroyedAutomaticArrayEventsReceived;
	private int createdManualArrayEventsReceived;
	private int destroyedManualArrayEventsReceived;
	private int antennaOfflineEventsReceived;
	private int antennaOnlineEventsReceived;
	private int almaCorrelatorTestModeEventsReceived;
	private int almaCorrelatorOperationalModeEventsReceived; 
	private int execBlockStartedEventsReceived;
	private int execBlockEndedEventsReceived;
	private int scanStartedEventsReceived;
	private int scanEndedEventsReceived;
	private int subscanStartedEventsReceived;
	private int subscanEndedEventsReceived;
}
