/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ExternalPublisher.java
 */
package alma.Control.Common;

// Control imports.
import alma.Control.ScanPurpose;
import alma.Control.ControlSystemChangeOfStateEvent;
import alma.Control.CreatedAutomaticArrayEvent;
import alma.Control.DestroyedAutomaticArrayEvent;
import alma.Control.CreatedManualArrayEvent;
import alma.Control.DestroyedManualArrayEvent;
import alma.Control.AntennaOfflineEvent;
import alma.Control.AntennaOnlineEvent;
import alma.Control.AlmaCorrelatorTestModeEvent;
import alma.Control.AlmaCorrelatorOperationalModeEvent;
import alma.Control.ExecBlockStartedEvent;
import alma.Control.ExecBlockEndedEvent;
import alma.Control.ScanStartedEvent;
import alma.Control.ScanEndedEvent;
import alma.Control.SubscanStartedEvent;
import alma.Control.SubscanEndedEvent;

// HLA imports
import alma.asdmIDLTypes.IDLEntityRef;

// ACS imports.
import alma.acs.container.ContainerServices;
//import alma.acs.nc.AbstractNotificationChannel;
import alma.acs.nc.SimpleSupplier;

// Java imports.
import java.util.logging.Logger;

/**
 * The External Publisher class is a general class that handles Control's external
 * notification channel.  Its methods include creating and disconnecting from the
 * notification channel, as well as method for publishing all external events.  
 *
 * @version 1.0 Mar 26, 2005
 * @author Allen Farris
 */
public class ExternalPublisher {
    
    //protected AbstractNotificationChannel nc;
    protected SimpleSupplier nc;
    protected Logger logger;

    /**
     * Create a Notification channel that publishes events outside Control.
     */
    public ExternalPublisher(ContainerServices cs) throws PublishEventException {
        try {
		    this.logger = cs.getLogger();
		    //this.nc = AbstractNotificationChannel.createNotificationChannel(
		    //     AbstractNotificationChannel.CORBA, Name.NCExternal, cs);
            this.nc = new SimpleSupplier(Name.NCExternal, cs);
		    logger.info("CONTROL: Created " + Name.NCExternal + " notification channel.");
        } catch (Exception err) {
            String s = "CONTROL: Error creating " + Name.NCExternal + " notification channel. " + err.toString();
            logger.severe(s);
            throw new PublishEventException(s);
        }
    }
    
    /**
     * Disconnect this external notification channel.
     */
	public void disconnect() throws PublishEventException {
    	try {
        	logger.info("CONTROL: Disconnecting from " + Name.NCExternal + " notification channel." );
         	//nc.deactivate();
            nc.disconnect();
    	} catch (Exception err) { 
    	    String s = "CONTROL: Error Disconnecting from " + Name.NCExternal + " notification channel." + err.toString();
        	logger.severe(s);
        	throw new PublishEventException(s);
    	}
	}
	
	private void error(String phrase, Exception err) throws PublishEventException {
	    String s = "CONTROL: Error publishing " + phrase + " event on " + Name.NCExternal + " notification channel. " + err.toString();
        logger.severe(s);
        throw new PublishEventException(s);            	    
	}

	/**
     * Publish an event denoting a state change in the Control system.
	 * A change of state event is published whenever there is a change in Control's
	 * state or substate.  The time this event occurred is included, as well as
	 * the new state and substate and an indication of whether this was the 
	 * result of an error.
     * @param state The state of the Control system.
     * @param acsTime The time at which this change of state occurred.
     * @throws PublishEventException If there is any error in publishing the event.
     */
    public void publishStateChange(State state, boolean error, long acsTime) throws PublishEventException {
        try {
            // The ControlSystemChangeOfStateEvent IDL structure was changed
            // from using enumerations to longs because of an error in 
            // the CORBA to Java mapping in ACS4.1.1.
            ControlSystemChangeOfStateEvent e = new ControlSystemChangeOfStateEvent ();
            e.currentState = state.getStateValue();
            e.currentSubstate = state.getSubstateValue();
            e.previousState = state.getPreviousStateValue();
            e.previousSubstate = state.getPreviousSubstateValue();
            nc.publishEvent(e);
            logger.info("CONTROL: Published state change event on " + Name.NCExternal + " notification channel.");
        } catch (Exception err) {
            error("state change",err);
        }
    }
    
    /**
     * Whenever an automatic array is created, a CreateAutomaticArray event is published.
     * @param arrayName The name of the automatic array that was created.
     * @param dcName The name of the newly created data capture component that is 
	 * attached to this array.
     * @param antennaName The list of the names of the antennas that belong to this array.
     * @param acsTime The time at which this array was created.
     * @throws PublishEventException If there is any error in publishing the event.
     */
    public void publishCreatedAutomaticArray(String arrayName, String dcName, String[] antennaList, long acsTime) throws PublishEventException {
        try {
           CreatedAutomaticArrayEvent e = new CreatedAutomaticArrayEvent();
           e.arrayName = arrayName;
           e.dataCaptureName = dcName;
           e.antennaList = antennaList;
           e.creationTime = acsTime;
           nc.publishEvent(e);
           logger.info("CONTROL: Published create automatic array" + arrayName + " event on " + Name.NCExternal + " notification channel.");
        } catch (Exception err) {
            error("create automatic array " + arrayName, err);
        }
    }

    /**
     * Whenever an automatic array is destroyed, a DestroyAutomaticArray event is published.
     * @param arrayName The name of the automatic array that was destroyed.
     * @param acsTime The time at which this array was destroyed.
     * @throws PublishEventException If there is any error in publishing the event.
     */
    public void publishDestroyedAutomaticArray(String arrayName, long acsTime) throws PublishEventException {
        try {
            DestroyedAutomaticArrayEvent e = new DestroyedAutomaticArrayEvent ();
            e.arrayName = arrayName;
            e.destructionTime = acsTime;
            nc.publishEvent(e);
            logger.info("CONTROL: Published destroy automatic array" + arrayName + " event on " + Name.NCExternal + " notification channel.");
        } catch (Exception err) {
            error("destroy automatic array " + arrayName, err);
        }
    }

    /**
     * Whenever a manual array is created, a CreateManualArray event is published.
     * @param arrayName The name of the manual array that was created.
     * @param staffName Return the id of the staff member who is responsible for operating
	 * this manual array.
     * @param antennaList The list of the names of the antennas that belong to this array. 
     * @param acsTime The time this array was created.
     * @throws PublishEventException If there is any error in publishing the event.
     */
    public void publishCreatedManualArray(String arrayName, String staffName, String[] antennaList, long acsTime) throws PublishEventException {
        try {
            CreatedManualArrayEvent e = new CreatedManualArrayEvent ();
            e.arrayName = arrayName;
            e.antennaList = antennaList;
            e.staffName = staffName;
            e.creationTime = acsTime;
            nc.publishEvent(e);
            logger.info("CONTROL: Published create manual array " + arrayName + " event on " + Name.NCExternal + " notification channel.");
        } catch (Exception err) {
            error("create manual array " + arrayName, err);
        }
    }

    /**
     * Whenever a manual array is destroyed, a DestroyManualArray event is published.
     * @param arrayName The name of the manual array that was destroyed.
     * @param acsTime The time at which this array was destroyed.
     * @throws PublishEventException If there is any error in publishing the event.
     */
    public void publishDestroyedManualArray(String arrayName, long acsTime) throws PublishEventException {
        try {
            DestroyedManualArrayEvent e = new DestroyedManualArrayEvent ();
            e.arrayName = arrayName;
            e.destructionTime = acsTime;
            nc.publishEvent(e);
            logger.info("CONTROL: Published destroy manual array " + arrayName + " event on " + Name.NCExternal + " notification channel.");
           
        } catch (Exception err) {
            error("destroy manual array " + arrayName,err);
        }
    }

    /**
     * Whenever an antennas has been taken offline, an AntennaOffline event is published.
     * @param antennaName The name of the antenna that was taken offline.
     * @param acsTime The time at which this antenna was taken offline.
     * @throws PublishEventException If there is any error in publishing the event.
     */
    public void publishAntennaOffline(String antennaName, long acsTime) throws PublishEventException {
        try {
            AntennaOfflineEvent e = new AntennaOfflineEvent ();
            e.antennaName = antennaName;
            e.offlineTime = acsTime;
            nc.publishEvent(e);
            logger.info("CONTROL: Published antenna " + antennaName + " offline event on " + Name.NCExternal + " notification channel.");
        } catch (Exception err) {
            error("antenna " + antennaName + " offline ",err);
        }
    }

    /**
     * Whenever an antenna has been placed online, an AntennaOnline event is published.
     * @param antennaName The name of the antenna that was placed online.
     * @param acsTime The time at which this antenna was placed online.
     * @throws PublishEventException If there is any error in publishing the event.
     */
    public void publishAntennaOnline(String antennaName, long acsTime) throws PublishEventException {
        try {
            AntennaOnlineEvent e = new AntennaOnlineEvent ();
            e.antennaName = antennaName;
            e.onlineTime = acsTime;
            nc.publishEvent(e);
            logger.info("CONTROL: Published antenna " + antennaName + " online event on " + Name.NCExternal + " notification channel.");
        } catch (Exception err) {
            error("antenna " + antennaName + " online ",err);
        }
    }

    /**
     * Whenever one or more quadrants of the ALMA correlator have been 
	 * placed in test mode, an AlmaCorrelatorTestMode event is published.
     * @param quad0 True, if and only if quadrant 0 is in test mode.
     * @param quad1 True, if and only if quadrant 1 is in test mode.
     * @param quad2 True, if and only if quadrant 2 is in test mode.
     * @param quad3 True, if and only if quadrant 3 is in test mode.
     * @param acsTime The time at which one or more quadrants of the ALMA correlator were
	 * placed in test mode.
     * @throws PublishEventException If there is any error in publishing the event.
     */
    public void publishAlmaCorrelatorTestMode(boolean quad0, boolean quad1,  boolean quad2,  boolean quad3, long acsTime) throws PublishEventException {
        try {
            AlmaCorrelatorTestModeEvent e = new AlmaCorrelatorTestModeEvent ();
            e.quad0 = quad0;
            e.quad1 = quad1;
            e.quad2 = quad2;
            e.quad3 = quad3;
            e.testTime = acsTime;
            nc.publishEvent(e);
            logger.info("CONTROL: Published ALMA Correlator test mode event on " + Name.NCExternal + " notification channel.");
        } catch (Exception err) {
             error("ALMA Correlator test mode",err);
        }       
    }

    /**
     * Whenever one or more quadrants of the ALMA correlator have been 
	 * placed in operational mode, an AlmaCorrelatorOperationalMode event is published.
     * @param quad0 True, if and only if quadrant 0 is in operational mode.
     * @param quad1 True, if and only if quadrant 1 is in operational mode.
     * @param quad2 True, if and only if quadrant 2 is in operational mode.
     * @param quad3 True, if and only if quadrant 3 is in operational mode.
     * @param acsTime The time at which one or more quadrants of the ALMA correlator were
	 * placed in operational mode.
     * @throws PublishEventException If there is any error in publishing the event.
     */
    public void publishAlmaCorrelatorOperationalMode(boolean quad0, boolean quad1,  boolean quad2,  boolean quad3, long acsTime) throws PublishEventException {
        try {
            AlmaCorrelatorOperationalModeEvent e = new AlmaCorrelatorOperationalModeEvent ();
            e.quad0 = quad0;
            e.quad1 = quad1;
            e.quad2 = quad2;
            e.quad3 = quad3;
            e.operationalTime = acsTime;
            nc.publishEvent(e);
            logger.info("CONTROL: Published ALMA Correlator operational mode event on " + Name.NCExternal + " notification channel.");
         } catch (Exception err) {
             error("ALMA Correlator operational mode",err);
         }       
    }
    
    /**
     * Whenever a scheduling block has begun execution, an ExecBlockStart event is published.
     * @param execId The UID of the exec block that was created as the result of executing
	 * this scheduling block.
     * @param sbId The UID of the scheduling block being executed.
     * @param sessionId The UID of the session to which the execution of this scheduling block belongs.
     * @param arrayName The name of the array on which this scheduling block is being executed.
     * @param acsTime The time at this execution stated.
     * @throws PublishEventException If there is any error in publishing the event.
     */
    public void publishExecStarted(IDLEntityRef execId, IDLEntityRef sbId, IDLEntityRef sessionId, String arrayName, long acsTime) throws PublishEventException {
        try {
            ExecBlockStartedEvent e = new ExecBlockStartedEvent ();
            e.execId = execId;
            e.sbId = sbId;
            e.sessionId = sessionId;
            e.arrayName = arrayName;
            e.startTime = acsTime;
            nc.publishEvent(e);
            logger.info("CONTROL: Published Exec Block " + execId + " start event on " + Name.NCExternal + " notification channel.");
        } catch (Exception err) {
            error("Exec Block " + execId + " start",err);
        }
    }

//    /**
//     * 
//     * @param execId The UID of the exec block that has been completed.
//     * @param sbId The UID of the scheduling block that has ceased execution.
//     * @param sessionId The UID of the session to which the execution of this scheduling block belongs.
//     * @param arrayName The name of the array on which this scheduling block was executed.
//     * @param result The result of this execution.
//     * @param acsTime The time this execution ended.
//     * @throws PublishEventException If there is any error in publishing the event.
//     */
//    public void publishExecEnded(IDLEntityRef execId, IDLEntityRef sbId, IDLEntityRef sessionId, String arrayName, Result result, long acsTime) throws PublishEventException {
//        try {
//            ExecBlockEndedEvent e = new ExecBlockEndedEvent ();
//            e.execId = execId;
//            e.sbId = sbId;
//            e.sessionId = sessionId;
//            e.arrayName = arrayName;
//            e.endTime = acsTime;
//            e.status = result.toIDL().condition;
//            nc.publishEvent(e);
//            logger.info("CONTROL: Published Exec Block " + execId + " end event on " + Name.NCExternal + " notification channel.");           
//        } catch (Exception err) {
//            error("Exec Block " + execId + " end",err);
//        }
//    }

    /**
     * Whenever a scan is started as the result of executing a script in the
	 * process of exeucting a scheduling block, a ScanStart event is published.
     * @param execId The UID of the exec block to which this scan belongs.
     * @param arrayName The name of the array on which this scan is being executed.
     * @param scanNumber The scan number that has been assigned to this scan.  Each scan
	 * has a unique, sequential number within this exec block.
     * @param purpose The purposes of this scan.
     * @param acsTime The time this scan was started.
     * @throws PublishEventException If there is any error in publishing the event.
     */
    public void publishScanStarted(IDLEntityRef execId, String arrayName, int scanNumber, ScanPurpose[] purpose, long acsTime) throws PublishEventException {
        try {
            ScanStartedEvent e = new ScanStartedEvent ();
            e.execId = execId;
            e.arrayName = arrayName;
            e.scanNumber = scanNumber;
            // Comment out the following line because it was stopping
            // this module compiling (and without this module
            // CONTROL/MASTER wilk not compile).  Rafael and/or Allen
            // are to descide what needs to be done here e.scanType =
            // purpose;
            e.startTime = acsTime;
            nc.publishEvent(e);
            logger.info("CONTROL: Published scan " + execId + "|" + scanNumber + " start event on " + Name.NCExternal + " notification channel.");           
        } catch (Exception err) {
            error("scan " + execId + "|" + scanNumber + " start",err);
        }
    }

//    /**
//     * Whenever a scan has ended as the result of executing a script in the
//	 * process of exeucting a scheduling block, a ScanEnd event is published.
//     * @param execIdThe UID of the exec block to which this scan belongs.
//     * @param arrayName The name of the array on which this scan was executed.
//     * @param scanNumber The scan number of the scan that has ended.
//     * @param result The resulting completion status of this execution.
//     * @param acsTime The time this scan ended.
//     * @throws PublishEventException If there is any error in publishing the event.
//     */
//    public void publishScanEnded(IDLEntityRef execId, String arrayName, int scanNumber, Result result, long acsTime) throws PublishEventException {
//        try {
//            ScanEndedEvent e = new ScanEndedEvent ();
//            e.execId = execId;
//            e.arrayName = arrayName;
//            e.scanNumber = scanNumber;
//            e.endTime = acsTime;
//            e.status = result.toIDL().condition;
//            nc.publishEvent(e);
//            logger.info("CONTROL: Published scan " + execId + "|" + scanNumber + " end event on " + Name.NCExternal + " notification channel.");           
//        } catch (Exception err) {
//            error("scan " + execId + "|" + scanNumber + " end",err);
//        }
//    }

    /**
     * Whenever a subscan is started as the result of executing a script in the
	 * process of exeucting a scheduling block, a SubscanStart event is published.
     * @param execId The UID of the exec block to which this subscan belongs.
     * @param arrayName The name of the array on which this subscan is being executed.
     * @param scanNumber The number of the scan to which this subscan belongs.
     * @param subscanNumber The number of the subscan.  Subscan are sequentially number within
	 * a given scan.
     * @param acsTime The time this subscan was started.
     * @throws PublishEventException If there is any error in publishing the event.
     */
    public void publishSubscanStarted(IDLEntityRef execId, String arrayName, int scanNumber, int subscanNumber, long acsTime) throws PublishEventException {
        try {
	        SubscanStartedEvent e = new SubscanStartedEvent ();
	        e.execId = execId;
	        e.arrayName = arrayName;
	        e.scanNumber = scanNumber;
	        e.subscanNumber = subscanNumber;
	        e.startTime = acsTime;
	        nc.publishEvent(e);
	        logger.info("CONTROL: Published subscan " + execId + "|" + scanNumber + "|" + subscanNumber + " start event on " + Name.NCExternal + " notification channel.");           
	    } catch (Exception err) {
	        error("scan " + execId + "|" + scanNumber + "|" + subscanNumber  + " start",err);
	    }
    }

//    /**
//     * Whenever a subscan has ended as the result of executing a script in the
//	 * process of exeucting a scheduling block, a SubscanEnd event is published.
//     * @param execId The UID of the exec block to which this scan belongs.
//     * @param arrayName The name of the array on which this scan was executed.
//     * @param scanNumber The number of the scan to which this subscan belongs.
//     * @param subscanNumber The number of the subscan.
//     * @param result The resulting completion status of this execution.
//     * @param acsTime The time this subscan ended.
//     * @throws PublishEventException If there is any error in publishing the event.
//     */
//    public void publishSubscanEnded(IDLEntityRef execId, String arrayName, int scanNumber, int subscanNumber, Result result, long acsTime) throws PublishEventException {
//        try {
//            SubscanEndedEvent e = new SubscanEndedEvent ();
//            e.execId = execId;
//            e.arrayName = arrayName;
//            e.scanNumber = scanNumber;
//            e.subscanNumber = subscanNumber;
//            e.endTime = acsTime;
//            e.status = result.toIDL().condition;
//            nc.publishEvent(e);
//            logger.info("CONTROL: Published subscan " + execId + "|" + scanNumber + "|" + subscanNumber + " end event on " + Name.NCExternal + " notification channel.");           
//        } catch (Exception err) {
//            error("subscan " + execId + "|" + scanNumber + "|" + subscanNumber  + " end",err);
//        }
//    }

}
