#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$
#------------------------------------------------------------------------------

'''
General CONTROL component test cases.
'''
import time
import math
from unittest import TestCase
import maciErrType
from Acspy.Clients.SimpleClient import PySimpleClient
import Control
import Control

# Global counter variable
testCounter = 0

# Count up
def testCountup():
    global testCounter
    testCounter +=1
    return testCounter

class PyUCommonTestCase(TestCase):
    def __init__(self, methodName='runTest',
                 componentName=None,
                 deviceConfig=Control.DeviceConfig("",[]),
                 verbose=0):
        '''
        '''
        self.client=PySimpleClient()
        self.verbose = verbose
        self.name = componentName
        self.configuration=deviceConfig

        if self.name:
            try:
                self.reference=self.client.getComponent(self.name)
            except maciErrType.maciErrTypeEx:
                self.fail("Unable to get reference to "+self.name)
        else:
            self.reference=None

        TestCase.__init__(self,methodName)

    def __del__(self):
        '''
        '''
        # Release component
        if self.name:
            try:
                self.client.releaseComponent(self.name)
            except maciErrType.maciErrTypeEx:
                pass
        self.client.disconnect()

    def dummy(self):
        '''
        Just a dummy method
        '''
        pass
    
    def check_reference(self):
        '''
        Checks that the component reference was correctly received
        '''
        self.assertNotEqual(self.reference, None, "Failed to get component reference")

    def check_ACSLifeCycle(self, name=None, iterations=10, sleepTime=0):
	'''
	Exercises a full ACS life cycle loop
		getComponent
		releaseComponent
	'''
        if self.name:
            self.fail("Internal self.name should be None to use this test")
        if not name:
            self.fail("No name specified")
        self.name = name
        if self.verbose:
            print "Component name:", self.name
        for i in range(0,iterations):
	    if self.verbose:
 		print "Iteration:", i
            try:
                self.reference=self.client.getComponent(self.name)
            except maciErrType.maciErrTypeEx:
                self.fail("Failure getting component reference")
                self.reference = None
            self.check_reference()
	    if sleepTime:
		time.sleep(sleepTime)
            try:
                self.client.releaseComponent(self.name)
            except maciErrType.maciErrTypeEx:
                pass
        self.name=None

    def check_createSubdevices(self):
        '''
        '''
        self.check_reference()
        self.reference.createSubdevices(self.configuration,
                                        "")
        subdevice_list = self.configuration.subdevice
        expected_subdevice_number = len(subdevice_list)
        received_subdevice_list = self.reference.getSubdeviceList()
        received_subdevice_number = len(received_subdevice_list)
        self.assertEqual(received_subdevice_number,
                         expected_subdevice_number,
                         "Incorrect number of subdevices. Expected "+
                         str(expected_subdevice_number)+
                         "received "+
                         str(received_subdevice_number))
        subdevice_refname_list = []
        for subdevice in received_subdevice_list:
            subdevice_refname_list.append(subdevice.ReferenceName)
        for subdevice in subdevice_list:
            subdevice_membership_test = subdevice.Name in subdevice_refname_list
            self.assertEqual(subdevice_membership_test,True,
                             "Subdevice "+subdevice.Name+" is not in received subdevice list")
        
    def check_releaseSubdevices(self):
        '''
        '''
        self.check_reference()
        self.reference.releaseSubdevices()
        self.assertEqual(self.reference.getSubdeviceList(),[],"Subdevice list is not empty")
        
    def check_CONTROLLifeCycle(self, name=None, iterations=10, sleepTime=0):
	'''
	Exercises a full CONTROL life cycle loop
		getComponent
		createSubdevices
		releaseComponent
	'''
        if self.name:
            self.fail("Internal self.name should be None to use this test")
        if not name:
            self.fail("No name specified")
        self.name = name
        if self.verbose:
            print "Component name:", self.name
        for i in range(0,iterations):
	    if self.verbose:
 		print "Iteration:", i
            self.reference=self.client.getComponent(self.name)
            self.check_reference()
            self.check_createSubdevices()
            #self.reference.createSubdevices(self.configuration,
            #                                "")
	    if sleepTime:
		time.sleep(sleepTime)
            # Release is done also in the cleanUp method now
            self.check_releaseSubdevices()
            #self.reference.releaseSubdevices()
            self.client.releaseComponent(self.name)
        self.name=None
        
    def check_property_definition(self,propertyName="dummy"):
        if propertyName=="dummy":
            pass
        else:
            if self.verbose:
                print ""
                print str(testCountup())+": "+propertyName
                print "    checking definition"
            try:
                propertyReference = eval("self.reference._get_"+propertyName+"()")
            except:
                propertyReference = None
            self.assertNotEqual(propertyReference, None, propertyName+": not defined")
       
    def check_property_existence(self,propertyName="dummy"):
        if propertyName=="dummy":
            pass
        else:
            self.check_property_definition(propertyName)
            propertyReference = eval("self.reference._get_"+propertyName+"()")
            try:
                propertyValue = propertyReference.get_sync()
            except:
                propertyValue = None
            if self.verbose:
                print "    checking result not null"
            self.assertNotEqual(propertyValue, None, propertyName+": no value received")
            if self.verbose:
                print "    checking read error in RO property"
            self.assertEqual(propertyValue[1].type, 0, propertyName+": read error (completion not ok)")

    def check_property_enum_range(self,propertyName="dummy",enumItems=()):
        if propertyName=="dummy":
            pass
        else: 
            self.check_property_existence(propertyName)
            propertyReference = eval("self.reference._get_"+propertyName+"()")
            propertyValue = propertyReference.get_sync()
            if self.verbose:
                print "    checking property value range"
            self.assertEqual(propertyValue[0] in enumItems, True, propertyName+": Invalid range")

    def check_property_enum_value(self,propertyName="dummy",enumItems=(),value=None):
        if propertyName=="dummy":
            pass
        else: 
            self.check_property_enum_range(propertyName,enumItems)
            propertyReference = eval("self.reference._get_"+propertyName+"()")
            propertyValue = propertyReference.get_sync()
            if self.verbose:
                print "    checking property value"
            self.assertEqual(propertyValue[0], value, propertyName+": incorrent value ("+str(propertyValue[0])+")!=("+str(value)+")")

    def check_property_pair(self,propertiesName=[]):
        if propertiesName==[]:
            pass
        else:
            property1 = eval("self.reference._get_"+propertiesName[0]+"()")
            (value1, completion1) = property1.get_sync()
            if self.verbose:
                print "    checking read error in RO property"
            self.assertEqual(completion1.type, 0, propertiesName[0]+": read error (completion not ok)")
            property2 = eval("self.reference._get_"+propertiesName[1]+"()")
            try:
                completion2 = property2.set_sync(value1)
            except:
                completion2 = None
            if self.verbose:
                print "    checking set_sync existence in RW property"
            self.assertNotEqual(completion2, None, propertiesName[1]+": no set_sync method")
            if self.verbose:
                print "    checking sucessful write in RW property"
            self.assertEqual(completion2.type, 0, propertiesName[1]+": write error (completion not ok)")
            (value2, completion2) = property2.get_sync()
            if self.verbose:
                print "    checking sucessful read in RW property"
            self.assertEqual(completion2.type, 0, propertiesName[1]+": read error (completion not ok)")
            if self.verbose:
                print "    checking match between written and read value in RW property"
            self.assertEqual(value2, value1, propertiesName[1]+": read value different from written value")
        
    def check_property_type(self,propertyName="dummy",propertyType=None):
        if propertyName=="dummy":
            pass
        else: 
            self.check_property_existence(propertyName)
            propertyReference = eval("self.reference._get_"+propertyName+"()")
            propertyValue = propertyReference.get_sync()
            if self.verbose:
                print "    checking property value type"
            self.assertEqual(type(propertyValue[0]) is propertyType, True, propertyName+": invalid type")

    def check_property_value(self,propertyName="dummy", propertyType =None, Value=None):
        if propertyName=="dummy":
            pass
        else: 
            self.check_property_type(propertyName,propertyType)
            propertyReference = eval("self.reference._get_"+propertyName+"()")
            propertyValue = propertyReference.get_sync()
            if self.verbose:
                print "    checking property value"
            self.assertEqual(propertyValue[0], Value, propertyName+": invalid value ->"+str(Value))
