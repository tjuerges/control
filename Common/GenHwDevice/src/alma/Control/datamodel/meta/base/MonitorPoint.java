/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File MonitorPoint.java
 */
package alma.Control.datamodel.meta.base;

import org.openarchitectureware.core.meta.core.ElementSet;
import org.openarchitectureware.core.meta.core.Element;
import org.openarchitectureware.core.meta.core.Model;
import org.openarchitectureware.core.constraint.DesignError;

/**
 * Description
 *
 * @version 1.0 Jul 13, 2005
 * @author Allen Farris
 */
public class MonitorPoint extends MandCBase {

	public MonitorPoint(String[] row, Element parent) {
		super(row, parent);
	}
	public String Default(){
		return row[Table.getColNum(sheet, "Default")];
	}
	public String ErrorCondition(){
		return row[Table.getColNum(sheet, "Error Condition")];
	}
	public String ErrorSeverity(){
		return row[Table.getColNum(sheet, "Error Severity")];
	}
	public String ErrorAction(){
		return row[Table.getColNum(sheet, "Error Action")];
	}

	// The following items are from the Archive spreadsheet.
	public String APName(){
		return ((ArchiveProperty)archive).Name();
	}
	public String RefersTo(){
		return ((ArchiveProperty)archive).RefersTo();
	}
	public String Interval(){
		return ((ArchiveProperty)archive).Interval();
	}
	public boolean OnlyOnChange(){
		return ((ArchiveProperty)archive).OnlyOnChange();
	}
	public String DisplayUnits(){
		return ((ArchiveProperty)archive).DisplayUnits();
	}
	public String GraphMin(){
		return ((ArchiveProperty)archive).GraphMin();
	}
	public String GraphMax(){
		return ((ArchiveProperty)archive).GraphMax();
	}
	public String Format(){
		return ((ArchiveProperty)archive).Format();
	}
	public String Title(){
		return ((ArchiveProperty)archive).Title();
	}
	public String MPName() {
		return PName();
	}
	public String AltMPName() {
		return AltPName();
	}
}
