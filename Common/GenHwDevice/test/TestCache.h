#ifndef TestCache_H
#define TestCache_H

#include "Utils.h"
#include <TETimeUtil.h>

class CacheAssemblyBase
{
	public:
	CacheAssemblyBase(double duration);
	~CacheAssemblyBase();
	void show();
	int getA();
	double getB();
	protected:
	void getP(ACS::Time&);
	private:
	int valueA;
	double valueB;
	ACS::Time refA;
	ACS::Time refB;
	ACS::Time refP;
	ACS::Time currentTime;
	//AMB::TimeThread* timeThread_p;
	pthread_t t;
	AMB::Cache<CacheAssemblyBase, void> *cacheParent;
};

void *loop(void* time)
{
	int i = 0;
	while(i < 1000){
		ACS::Time* current = (ACS::Time *)time;
		ACE_VERSIONED_NAMESPACE_NAME::ACE_Time_Value t(ACE_OS::gettimeofday());
		*current = (ACS::Time)t.sec()*10000000ULL + (ACS::Time)t.usec()*10ULL;
		i++;
		usleep(10000);
	}
	pthread_exit(NULL);
}

CacheAssemblyBase::CacheAssemblyBase(double duration)
{
	pthread_create(&t,NULL,loop,(void*)&currentTime);
	cacheParent = new AMB::Cache<CacheAssemblyBase, void>(this, &CacheAssemblyBase::getP, duration, currentTime);
	valueA = 0;
	valueB = 0.0;
	refA = refB = refP = 0;
}

CacheAssemblyBase::~CacheAssemblyBase()
{
	delete cacheParent;
}

void CacheAssemblyBase::show()
{
	std::cout << valueA << " " << valueB << std::endl;
}

int CacheAssemblyBase::getA()
{
	cacheParent->getVal(refA);
	return valueA;
}

double CacheAssemblyBase::getB()
{
	cacheParent->getVal(refB);
	return valueB;
}

void CacheAssemblyBase::getP(ACS::Time& timestamp)
{
	valueA++;
	valueB+=1.2;
	refP = timestamp;
	refA = refP;
	refB = refP;
}

#endif
