#ifndef EthernetDevIO_h
#define EthernetDevIO_h
/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * $Id$
 *
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif


#include <baciDevIO.h>
#include <acsutilTimeStamp.h>


class EthernetDevice;


/// Base class for all Read Only DevIO classes
/// The class is templated by the type of the value T.
template< class T > class EthernetRODevIO: public DevIO< T >
{
    public:
    /// Constructor.
    /// \param address Logical address of the monitor point in the Ethernet
    /// device.  This is the equivalent of an RCA in an AMBSI based device.
    /// \param ethDevice A pointer to the Ethernet Device component for which
    /// this DevIO is part of.
    EthernetRODevIO(int address, EthernetDevice* ethDevice);

    /// Destructor.
    virtual ~EthernetRODevIO();

    /// Method to retrieve the value from the Ethernet device.
    /// This overrides the DevIO< T >::read.
    /// \exception ControlExceptions::INACTErrorExImpl Thrown if the device is
    /// not in operational or simulation mode.
    /// \exception EthernetDeviceExceptions::SocketOperationFailedExImpl
    /// Accessing the socket failed.
    /// \exception EthernetDeviceExceptions::IllegalParameterExImpl Everything
    /// that can go wrong with values being read from the hardware (wrong value,
    /// unexpected type) or being sent to the hardware (wrong value, wrong
    /// type).
    virtual T read(ACS::Time& timestamp);


    protected:
    /// Pointer to the Ethernet Device component implementation.
    EthernetDevice* ethernetDevice_mp;

    /// Address of the DevIO.
    int address_m;
};

/// Base class for all Read Write DevIO Classes
/// The class is templated by the type of the value T.
template< class T > class EthernetRWDevIO: public DevIO< T >
{
    public:
    /// Constructor.
    /// \param address Logical address of the monitor point in the Ethernet
    /// device.  This is the equivalent of an RCA in an AMBSI based device.
    /// \param ethDevice A pointer to the Ethernet Device component for which
    /// this DevIO is part of.
    EthernetRWDevIO(int address, EthernetDevice* ethDevice);

    /// Destructor
    virtual ~EthernetRWDevIO();

    /// Method to retrieve the last value written to the Ethernet device.
    /// This overrides the DevIO< T >::read.
    /// \param timestamp Time& - Location to store the current time.
    virtual T read(ACS::Time& timestamp);

    /// Method to write the value(s) to the Ethernet.
    /// This overrides the DevIO<T> call write().
    /// \exception ControlExceptions::INACTErrorExImpl Thrown if the device is
    /// not in operational or simulation mode.
    /// \exception EthernetDeviceExceptions::SocketOperationFailedExImpl
    /// Accessing the socket failed.
    /// \exception EthernetDeviceExceptions::IllegalParameterExImpl Everything
    /// that can go wrong with values being read from the hardware (wrong value,
    /// unexpected type) or being sent to the hardware (wrong value, wrong
    /// type).
    virtual void write(const T& value, ACS::Time& timestamp);


    protected:
    /// This stores the last value(s) written to the Ethernet device.
    T lastWrittenValue_m;

    /// Pointer to the Ethernet Device component implementation.
    EthernetDevice* ethernetDevice_mp;

    /// Address of the DevIO.
    int address_m;
};
#endif
