#ifndef EthernetDeviceInterface_h
#define EthernetDeviceInterface_h
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
//  $Id$
//

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif


#include <string>
#include <map>
#include <vector>


/// Base class for all exceptions that Ethernet devices are allowed to throw.
class EthernetDeviceException
{
    public:
    /// Constructor.
    /// \param errorMessage Description of the problem.
    EthernetDeviceException(const std::string& _errorMessage):
        errorMessage(_errorMessage)
    {
    };

    /// Return the error message for the exception instance.
    /// \return Error message as std::string.
    std::string getErrorMessage() const
    {
        return errorMessage;
    };


    private:
    /// Exceptions without an error description are not allowed.
    EthernetDeviceException();

    std::string errorMessage;
};


/// Exception class for Ethernet device problems that are related to socket or
/// network problems.
class SocketOperationFailedException: public EthernetDeviceException
{
    public:
    /// Constructor.
    /// \param errorMessage Description of the problem.
    SocketOperationFailedException(const std::string& errorMessage):
        EthernetDeviceException(errorMessage)
    {
    };


    private:
    /// Exceptions without an error description are not allowed.
    SocketOperationFailedException();
};

/// Exception class for Ethernet device problems that are related to problems
/// with monitor or control parameters.
class IllegalParameterException: public EthernetDeviceException
{
    public:
    /// Constructor.
    /// \param errorMessage Description of the problem.
    IllegalParameterException(const std::string& errorMessage):
        EthernetDeviceException(errorMessage)
    {
    };


    private:
    /// Exceptions without an error description are not allowed.
    IllegalParameterException();
};


class EthernetDeviceInterface
{
    public:
    /// Empty virtual destructor since there is nothing implemented in this
    /// class.
    virtual ~EthernetDeviceInterface()
    {
    };

    /// Method which opens a new connection.
    ///
    /// \param hostname Contains the hostname (as DNS or IP address) to which
    /// a connection shall be established.  No empty string allowed!  It is up
    /// to the implementation to resolve the name to a valid IP4 address!
    ///
    /// \param port Port on the target computer, i.e. the one with the hardware
    /// device, that is used for the connection.  It is up to the implementation
    /// if the port is used only for communication establishment or for the data
    /// transport, too.
    ///
    /// \param timeout Tx/Rx timeout in s.
    ///
    /// \param lingerTime Time span in s before the OS closes the socket after
    /// the application has actively closed the socket.  This is from
    /// man 7 socket:
    ///
    /// "When enabled, a close(2) or shutdown(2) will not return until all
    /// queued messages for the socket have been successfully sent or the linger
    /// timeout has been reached.  Otherwise, the call returns immediately and
    /// the closing is done in the background.  When the socket is closed as
    /// part of exit(2), it always lingers in the background."
    ///
    /// \param retries Number of allowed retries after a communication failure.
    /// The initial first communication attempt shall not be counted against the
    /// retries.  If the number of retries has been reached, an exception will
    /// be thrown.
    ///
    /// \param parameters A std::multimap of const std::string pairs that
    /// specify implementation specific parameters.  The multimap consists of
    /// std::pair< std::string, std::string > key-value pairs.  Since it is a
    /// std::multimap, it can contain multiple entries for the same key.
    /// Parsing of the parameters is up to the implementation.
    ///
    /// \exception SocketOperationFailedException If the socket connection
    /// fails.
    virtual void open(const std::string& hostname, unsigned short port,
        double timeout, unsigned int lingerTime, unsigned short retries,
        const std::multimap< std::string, std::string >& parameters) = 0;

    /// Method which closes an existing connection.
    /// \return E.g. value that is returned by ::close(int fileDescriptor).  The
    /// device component implementation shall check this value and handle
    /// errors appropriately.  A value of 0 means no error, negative error
    /// numbers must match the errno definitions, positive return values are not
    /// allowed.
    virtual int close() = 0;

    /// Monitor Methods with a fundamental type parameter.
    ///
    /// These take two parameters.  One of which is the assigned address for the
    /// respective monitor point in the Ethernet hardware.  The other serves as
    /// return value replacement that will contain the correctly parsed / cast
    /// type on success.
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    virtual void monitor(int address, bool& value) = 0;

    /// See \ref virtual void monitor(int address, bool& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    virtual void monitor(int address, char& value) = 0;

    /// See \ref virtual void monitor(int address, bool& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    virtual void monitor(int address, unsigned char& value) = 0;

    /// See \ref virtual void monitor(int address, bool& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    virtual void monitor(int address, unsigned short& value) = 0;

    /// See \ref virtual void monitor(int address, bool& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    virtual void monitor(int address, short& value) = 0;

    /// See \ref virtual void monitor(int address, bool& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    virtual void monitor(int address, unsigned int& value) = 0;

    /// See \ref virtual void monitor(int address, bool& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    virtual void monitor(int address, int& value) = 0;

    /// See \ref virtual void monitor(int address, bool& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    virtual void monitor(int address, unsigned long int& value) = 0;

    /// See \ref virtual void monitor(int address, bool& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    virtual void monitor(int address, long int& value) = 0;

    /// See \ref virtual void monitor(int address, bool& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    virtual void monitor(int address, unsigned long long& value) = 0;

    /// See \ref virtual void monitor(int address, bool& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    virtual void monitor(int address, long long& value) = 0;

    /// See \ref virtual void monitor(int address, bool& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    virtual void monitor(int address, float& value) = 0;

    /// See \ref virtual void monitor(int address, bool& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    virtual void monitor(int address, double& value) = 0;

    /// See \ref virtual void monitor(int address, bool& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    virtual void monitor(int address, long double& value) = 0;

    /// See \ref virtual void monitor(int address, bool& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    virtual void monitor(int address, std::string& value) = 0;

    /// Monitor Methods with std::vector parameter.
    ///
    /// These take two parameters.  One of which is the assigned address for the
    /// respective monitor point in the Ethernet hardware.  The other serves as
    /// return value replacement that will contain the correctly parsed / cast
    /// type on success.  It is mandatory that the function implementation
    /// checks the size of the std::vector and throws an
    /// IllegalParameterException in the case of mismatch.
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    /// \exception IllegalParameterException If the length of the std::vector
    /// for the result does not match the number of values that would be
    /// returned.
    virtual void monitor(int address, std::vector< bool >& value) = 0;

    /// See \ref virtual void monitor(int address, std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    /// \exception IllegalParameterException If the length of the std::vector
    /// for the result does not match the number of values that would be
    /// returned.
    virtual void monitor(int address, std::vector< unsigned char >& value) = 0;

    /// See \ref virtual void monitor(int address, std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    /// \exception IllegalParameterException If the length of the std::vector
    /// for the result does not match the number of values that would be
    /// returned.
    virtual void monitor(int address, std::vector< char >& value) = 0;

    /// See \ref virtual void monitor(int address, std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    /// \exception IllegalParameterException If the length of the std::vector
    /// for the result does not match the number of values that would be
    /// returned.
    virtual void monitor(int address, std::vector< unsigned short >& value) = 0;

    /// See \ref virtual void monitor(int address, std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    /// \exception IllegalParameterException If the length of the std::vector
    /// for the result does not match the number of values that would be
    /// returned.
    virtual void monitor(int address, std::vector< short >& value) = 0;

    /// See \ref virtual void monitor(int address, std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    /// \exception IllegalParameterException If the length of the std::vector
    /// for the result does not match the number of values that would be
    /// returned.
    virtual void monitor(int address, std::vector< unsigned int >& value) = 0;

    /// See \ref virtual void monitor(int address, std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    /// \exception IllegalParameterException If the length of the std::vector
    /// for the result does not match the number of values that would be
    /// returned.
    virtual void monitor(int address, std::vector< int >& value) = 0;

    /// See \ref virtual void monitor(int address, std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    /// \exception IllegalParameterException If the length of the std::vector
    /// for the result does not match the number of values that would be
    /// returned.
    virtual void monitor(int address, std::vector< unsigned long >& value) = 0;

    /// See \ref virtual void monitor(int address, std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    /// \exception IllegalParameterException If the length of the std::vector
    /// for the result does not match the number of values that would be
    /// returned.
    virtual void monitor(int address, std::vector< long >& value) = 0;

    /// See \ref virtual void monitor(int address, std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    /// \exception IllegalParameterException If the length of the std::vector
    /// for the result does not match the number of values that would be
    /// returned.
    virtual void monitor(int address,
        std::vector< unsigned long long >& value) = 0;

    /// See \ref virtual void monitor(int address, std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    /// \exception IllegalParameterException If the length of the std::vector
    /// for the result does not match the number of values that would be
    /// returned.
    virtual void monitor(int address, std::vector< long long >& value) = 0;

    /// See \ref virtual void monitor(int address, std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    /// \exception IllegalParameterException If the length of the std::vector
    /// for the result does not match the number of values that would be
    /// returned.
    virtual void monitor(int address, std::vector< float >& value) = 0;

    /// See \ref virtual void monitor(int address, std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    /// \exception IllegalParameterException If the length of the std::vector
    /// for the result does not match the number of values that would be
    /// returned.
    virtual void monitor(int address, std::vector< double >& value) = 0;

    /// See \ref virtual void monitor(int address, std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    /// \exception IllegalParameterException If the length of the std::vector
    /// for the result does not match the number of values that would be
    /// returned.
    virtual void monitor(int address, std::vector< long double >& value) = 0;

    /// See \ref virtual void monitor(int address, std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware returned value and the value parameter) or
    /// invalid values that could fit in the parameter type.
    /// \exception IllegalParameterException If the length of the std::vector
    /// for the result does not match the number of values that would be
    /// returned.
    virtual void monitor(int address, std::vector< std::string >& value) = 0;


    /// Control Methods with a fundamental type parameter.
    ///
    /// These take either a single or two parameters.  One is always the
    /// assigned address for the respective control point in the Ethernet
    /// hardware.  The other serves as value that will be sent to the
    /// \param address.
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    virtual void command(int address) = 0;

    /// See \ref virtual void command(int address) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    virtual void command(int address, bool value) = 0;

    /// See \ref virtual void command(int address) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    virtual void command(int address, unsigned char value) = 0;

    /// See \ref virtual void command(int address) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    virtual void command(int address, char value) = 0;

    /// See \ref virtual void command(int address) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    virtual void command(int address, unsigned short value) = 0;

    /// See \ref virtual void command(int address) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    virtual void command(int address, short value) = 0;

    /// See \ref virtual void command(int address) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    virtual void command(int address, unsigned int value) = 0;

    /// See \ref virtual void command(int address) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    virtual void command(int address, int value) = 0;

    /// See \ref virtual void command(int address) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    virtual void command(int address, unsigned long value) = 0;

    /// See \ref virtual void command(int address) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    virtual void command(int address, long value) = 0;

    /// See \ref virtual void command(int address) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    virtual void command(int address, unsigned long long value) = 0;

    /// See \ref virtual void command(int address) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    virtual void command(int address, long long value) = 0;

    /// See \ref virtual void command(int address) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    virtual void command(int address, float value) = 0;

    /// See \ref virtual void command(int address) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    virtual void command(int address, double value) = 0;

    /// See \ref virtual void command(int address) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    virtual void command(int address, long double value) = 0;

    /// See \ref virtual void command(int address) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    virtual void command(int address, const std::string& value) = 0;

    /// Control Methods with std::vector parameter.
    ///
    /// These take either a single or two parameters.  One is always the
    /// assigned address for the respective control point in the Ethernet
    /// hardware.  The other serves as value that will be sent to the
    /// \param address.  It is mandatory that the function implementation
    /// checks the size of the std::vector and throws an
    /// IllegalParameterException in the case of mismatch.
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    /// \exception IllegalParameterException If the length of the input
    /// std::vector for the command values does not match the number of values
    /// expected by the device.
    virtual void command(int address, const std::vector< bool >& value) = 0;

    /// See \ref virtual void command(int address, const std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    /// \exception IllegalParameterException If the length of the input
    /// std::vector for the command values does not match the number of values
    /// expected by the device.
    virtual void command(int address,
        const std::vector< unsigned char >& value) = 0;

    /// See \ref virtual void command(int address, const std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    /// \exception IllegalParameterException If the length of the input
    /// std::vector for the command values does not match the number of values
    /// expected by the device.
    virtual void command(int address, const std::vector< char >& value) = 0;

    /// See \ref virtual void command(int address, const std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    /// \exception IllegalParameterException If the length of the input
    /// std::vector for the command values does not match the number of values
    /// expected by the device.
    virtual void command(int address,
        const std::vector< unsigned short >& value) = 0;

    /// See \ref virtual void command(int address, const std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    /// \exception IllegalParameterException If the length of the input
    /// std::vector for the command values does not match the number of values
    /// expected by the device.
    virtual void command(int address, const std::vector< short >& value) = 0;

    /// See \ref virtual void command(int address, const std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    /// \exception IllegalParameterException If the length of the input
    /// std::vector for the command values does not match the number of values
    /// expected by the device.
    virtual void command(int address,
        const std::vector< unsigned int >& value) = 0;

    /// See \ref virtual void command(int address, const std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    /// \exception IllegalParameterException If the length of the input
    /// std::vector for the command values does not match the number of values
    /// expected by the device.
    virtual void command(int address, const std::vector< int >& value) = 0;

    /// See \ref virtual void command(int address, const std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    /// \exception IllegalParameterException If the length of the input
    /// std::vector for the command values does not match the number of values
    /// expected by the device.
    virtual void command(int address,
        const std::vector< unsigned long >& value) = 0;

    /// See \ref virtual void command(int address, const std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    /// \exception IllegalParameterException If the length of the input
    /// std::vector for the command values does not match the number of values
    /// expected by the device.
    virtual void command(int address, const std::vector< long >& value) = 0;

    /// See \ref virtual void command(int address, const std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    /// \exception IllegalParameterException If the length of the input
    /// std::vector for the command values does not match the number of values
    /// expected by the device.
    virtual void command(int address,
        const std::vector< unsigned long long >& value) = 0;

    /// See \ref virtual void command(int address, const std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    /// \exception IllegalParameterException If the length of the input
    /// std::vector for the command values does not match the number of values
    /// expected by the device.
    virtual void command(int address, const std::vector< long long >& value) = 0;

    /// See \ref virtual void command(int address, const std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    /// \exception IllegalParameterException If the length of the input
    /// std::vector for the command values does not match the number of values
    /// expected by the device.
    virtual void command(int address, const std::vector< float >& value) = 0;

    /// See \ref virtual void command(int address, const std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    /// \exception IllegalParameterException If the length of the input
    /// std::vector for the command values does not match the number of values
    /// expected by the device.
    virtual void command(int address, const std::vector< double >& value) = 0;

    /// See \ref virtual void command(int address, const std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    /// \exception IllegalParameterException If the length of the input
    /// std::vector for the command values does not match the number of values
    /// expected by the device.
    virtual void command(int address,
        const std::vector< long double >& value) = 0;

    /// See \ref virtual void command(int address, const std::vector< bool >& value) = 0;
    /// \exception SocketOperationFailedException If the operation cannot be
    /// executed because of network / socket problems.
    /// \exception IllegalParameterException If the operation failed because of
    /// incompatible types (hardware cannot accept the value type because it
    /// does not fit) or incompatible values (too big or too small).
    /// \exception IllegalParameterException If the length of the input
    /// std::vector for the command values does not match the number of values
    /// expected by the device.
    virtual void command(int address,
        const std::vector< std::string >& value) = 0;
};
#endif
