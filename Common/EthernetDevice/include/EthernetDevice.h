#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef EthernetDevice_h
#define EthernetDevice_h
/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * $Id$
 * $Source$
 *
 */


#include <string>
#include <vector>
#include <boost/shared_ptr.hpp>
#include <ambCDBAccess.h>
#include <hardwareDeviceImpl.h>
#include <acscommonC.h>


namespace maci
{
    class ContainerServices;
};

namespace Control
{
    class AlarmSender;
};

class EthernetDeviceInterface;


/// This is the base class for device drivers.  It defines the class
/// EthernetDevice which provides methods for monitor and control interactions
/// with the Ethernet as well as providing the interface which EthernetDevIO
/// uses, and the default set of proporties and methods for every device.
/// \note The CharacteristicComponentImpl and ControlXmlParser should really be
/// virtual.  But in order to ensure correct instanciation the fact is ignored.
class EthernetDevice: public Control::HardwareDeviceImpl, public AmbCDBAccessor
{
    public:
    /// Constructor.
    EthernetDevice(const ACE_CString& name,
       maci::ContainerServices* containerServices);

    /// Destructor.
    virtual ~EthernetDevice();

    /// Query the hardware device ID.  This can be a MAC address or something
    /// implementation dependent.  But it must be guaranteed that the ID is
    /// unique among all hardware devices, AMB and ETH!
    /// \param deviceID will contain the unique hardware device ID.
    virtual void getDeviceUniqueId(std::string& deviceID);

    /// ACS Lifecycle methods
    virtual void initialize();

    /// ACS Lifecycle methods
    virtual void cleanUp();

    /// Control's hardware lifecycle methods
    virtual void hwStartAction();
    virtual void hwConfigureAction();
    virtual void hwInitializeAction();
    virtual void hwOperationalAction();
    virtual void hwStopAction();

    ///
    /// Monitor Method:
    /// These take an integer (enumeration) and return the correctly
    /// parsed / cast type.
    /// When a sequence of values needs to be returned it will always be
    /// be returned as a std::vector of the underlying type, e.g.
    /// std::vector< float >
    /// \exception EthernetDeviceExceptions::SocketOperationFailedExImpl Accessing
    /// the socket failed.
    /// \exception EthernetDeviceExceptions::IllegalParameterExImpl Everything
    /// that can go wrong with values being read from the hardware (wrong value,
    /// unexpected type) or being sent to the hardware (wrong value, wrong
    /// type).
    /// \exception EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl Thrown
    /// when an unexpected exception was caught or if an EthernetDeviceException was
    /// caught.
    template< class T > void monitor(int address, T& value,
        ACS::Time& timestamp);

    /// \exception EthernetDeviceExceptions::SocketOperationFailedExImpl Accessing
    /// the socket failed.
    /// \exception EthernetDeviceExceptions::IllegalParameterExImpl Everything
    /// that can go wrong with values being read from the hardware (wrong value,
    /// unexpected type) or being sent to the hardware (wrong value, wrong
    /// type).
    /// \exception EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl Thrown
    /// when an unexpected exception was caught or if an EthernetDeviceException was
    /// caught.
    template< class T > void monitor(int address, const std::vector< T >& value,
        ACS::Time& timestamp);

    /// Control Method
    /// These methods take an integer (enumeration) and a the value to send to
    /// the hardware.
    /// When a sequence of values needs to be sent it will always be
    /// be sent as a vector of the underlying type, e.g. std::vector< float >.
    /// \exception EthernetDeviceExceptions::SocketOperationFailedExImpl Accessing
    /// the socket failed.
    /// \exception EthernetDeviceExceptions::IllegalParameterExImpl Everything
    /// that can go wrong with values being read from the hardware (wrong value,
    /// unexpected type) or being sent to the hardware (wrong value, wrong
    /// type).
    /// \exception EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl Thrown
    /// when an unexpected exception was caught or if an EthernetDeviceException was
    /// caught.
    void command(int address, ACS::Time& timestamp);

    /// \exception EthernetDeviceExceptions::SocketOperationFailedExImpl Accessing
    /// the socket failed.
    /// \exception EthernetDeviceExceptions::IllegalParameterExImpl Everything
    /// that can go wrong with values being read from the hardware (wrong value,
    /// unexpected type) or being sent to the hardware (wrong value, wrong
    /// type).
    /// \exception EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl Thrown
    /// when an unexpected exception was caught or if an EthernetDeviceException was
    /// caught.
    template< class T > void command(int address, T& value,
        ACS::Time& timestamp);

    /// \exception EthernetDeviceExceptions::SocketOperationFailedExImpl Accessing
    /// the socket failed.
    /// \exception EthernetDeviceExceptions::IllegalParameterExImpl Everything
    /// that can go wrong with values being read from the hardware (wrong value,
    /// unexpected type) or being sent to the hardware (wrong value, wrong
    /// type).
    /// \exception EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl Thrown
    /// when an unexpected exception was caught or if an EthernetDeviceException was
    /// caught.
    template< class T > void command(int address, const std::vector< T >& value,
        ACS::Time& timestamp);


    protected:
    /// Name of the host for the current connection.  Empty if not connected.
    std::string hostname_m;

    /// Port on the host \ref hostname_m that shall be used for the TCP/IP
    /// connection.
    unsigned short port_m;

    /// Tx/Rx time out in s.
    double timeout_m;

    /// Linger time for the connection in s.
    unsigned int lingerTime_m;

    /// Number of allowed retries for every transaction.
    unsigned short retries_m;

    /// Parameter map for Ethernet device implementation dependent parameters.
    std::multimap< std::string, std::string > parameters_m;

    /// Smart pointer to the real Ethernet device implementation.
    boost::shared_ptr< EthernetDeviceInterface > ethernetDeviceIF_mp;

    ///Alarm Sender
    Control::AlarmSender* alarmSender_m;

    private:
    static const int NOCONNECT = 1;
};

/// Include the template implementations.
#include <EthernetDevice.i>
#endif
