/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * $Id$
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif


#include <EthernetDevIO.h>
#include <EthernetDevice.h>
#include <string>
#include <acscommonC.h>
#include <acsutilTimeStamp.h>


template< class T >
EthernetRODevIO< T >::EthernetRODevIO(int address, EthernetDevice* ethDevice):
    DevIO< T >(),
    ethernetDevice_mp(ethDevice),
    address_m(address)
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}

template< class T >
EthernetRODevIO< T >::~EthernetRODevIO()
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}

template< class T >
T EthernetRODevIO< T >::read(ACS::Time& timestamp)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    if(ethernetDevice_mp->isReady() == false)
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);

        const std::string msg(
            "Cannot execute monitor request.  Device is inactive.");
        ex.addData("Detail", msg);
        ex.log();
        throw ex;
    }

    T returnValue;
    try
    {
        // Call to EthernetDevice->
        ethernetDevice_mp->monitor(address_m, returnValue, timestamp);
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedExImpl& ex)
    {
        EthernetDeviceExceptions::SocketOperationFailedExImpl acsEx(ex,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw acsEx;
    }
    catch(const EthernetDeviceExceptions::IllegalParameterExImpl& ex)
    {
        EthernetDeviceExceptions::IllegalParameterExImpl acsEx(ex,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw acsEx;
    }

    return returnValue;
}

template< class T >
EthernetRWDevIO< T >::EthernetRWDevIO(int address, EthernetDevice* ethDevice):
    DevIO< T >(),
    ethernetDevice_mp(ethDevice),
    address_m(address)
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}

template< class T >
EthernetRWDevIO< T >::~EthernetRWDevIO()
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}

template< class T >
T EthernetRWDevIO< T >::read(ACS::Time& timestamp)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    timestamp = ::getTimeStamp();
    return lastWrittenValue_m;
}

template< class T >
void EthernetRWDevIO< T >::write(const T& value, ACS::Time& timestamp)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    if(ethernetDevice_mp->isReady() == false)
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);

        const std::string msg(
            "Cannot execute monitor request.  Device is inactive.");
        ex.addData("Detail", msg);
        ex.log();
        throw ex;
    }

    try
    {
        ethernetDevice_mp->command(address_m, value, timestamp);
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedExImpl& ex)
    {
        EthernetDeviceExceptions::SocketOperationFailedExImpl acsEx(ex,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw acsEx;
    }
    catch(const EthernetDeviceExceptions::IllegalParameterExImpl& ex)
    {
        EthernetDeviceExceptions::IllegalParameterExImpl acsEx(ex,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw acsEx;
    }

    lastWrittenValue_m = value;
}
