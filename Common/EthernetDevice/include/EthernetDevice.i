#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * $Id$
 * $Source$
 *
 */


#include <string>
#include <vector>
#include <ControlExceptions.h>
#include <EthernetDeviceInterface.h>
#include <EthernetDeviceExceptions.h>
#include <acsutilTimeStamp.h>
#include <acscommonC.h>


template< typename T > void EthernetDevice::monitor(int address, T& value,
    ACS::Time& timestamp)
{
    try
    {
        ethernetDeviceIF_mp->monitor(address, value);
        timestamp = ::getTimeStamp();
    }
    catch(const SocketOperationFailedException& ex)
    {
        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", ex.getErrorMessage());
        throw nex;
    }
    catch(const IllegalParameterException& ex)
    {
        EthernetDeviceExceptions::IllegalParameterExImpl nex(__FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", ex.getErrorMessage());
        throw nex;
    }
    catch(const EthernetDeviceException& ex)
    {
        EthernetDeviceExceptions::EthernetErrorExImpl nex(__FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", ex.getErrorMessage());
        throw nex;
    }
    catch(...)
    {
        EthernetDeviceExceptions::EthernetErrorExImpl ex(__FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", "Unexpected exception caught.");
        ex.log();
        throw ex;
    }
}

template< typename T > void EthernetDevice::monitor(int address,
    const std::vector< T >& value, ACS::Time& timestamp)
{
    try
    {
        ethernetDeviceIF_mp->monitor(address, value);
        timestamp = ::getTimeStamp();
    }
    catch(const SocketOperationFailedException& ex)
    {
        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", ex.getErrorMessage());
        throw nex;
    }
    catch(const IllegalParameterException& ex)
    {
        EthernetDeviceExceptions::IllegalParameterExImpl nex(__FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", ex.getErrorMessage());
        throw nex;
    }
    catch(const EthernetDeviceException& ex)
    {
        EthernetDeviceExceptions::EthernetErrorExImpl nex(__FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", ex.getErrorMessage());
        throw nex;
    }
    catch(...)
    {
        EthernetDeviceExceptions::EthernetErrorExImpl ex(__FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", "Unexpected exception caught.");
        ex.log();
        throw ex;
    }
}

template< typename T > void EthernetDevice::command(int address, T& value,
    ACS::Time& timestamp)
{
    try
    {
        ethernetDeviceIF_mp->command(address, value);
        timestamp = ::getTimeStamp();
    }
    catch(const SocketOperationFailedException& ex)
    {
        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", ex.getErrorMessage());
        throw nex;
    }
    catch(const IllegalParameterException& ex)
    {
        EthernetDeviceExceptions::IllegalParameterExImpl nex(__FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", ex.getErrorMessage());
        throw nex;
    }
    catch(const EthernetDeviceException& ex)
    {
        EthernetDeviceExceptions::EthernetErrorExImpl nex(__FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", ex.getErrorMessage());
        throw nex;
    }
    catch(...)
    {
        EthernetDeviceExceptions::EthernetErrorExImpl ex(__FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", "Unexpected exception caught.");
        ex.log();
        throw ex;
    }
}

template< typename T > void EthernetDevice::command(int address,
    const std::vector< T >& value, ACS::Time& timestamp)
{
    try
    {
        ethernetDeviceIF_mp->command(address, value);
        timestamp = ::getTimeStamp();
    }
    catch(const SocketOperationFailedException& ex)
    {
        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", ex.getErrorMessage());
        throw nex;
    }
    catch(const IllegalParameterException& ex)
    {
        EthernetDeviceExceptions::IllegalParameterExImpl nex(__FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", ex.getErrorMessage());
        throw nex;
    }
    catch(const EthernetDeviceException& ex)
    {
        EthernetDeviceExceptions::EthernetErrorExImpl nex(__FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", ex.getErrorMessage());
        throw nex;
    }
    catch(...)
    {
        EthernetDeviceExceptions::EthernetErrorExImpl ex(__FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", "Unexpected exception caught.");
        ex.log();
        throw ex;
    }
}
