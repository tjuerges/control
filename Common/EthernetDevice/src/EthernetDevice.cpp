/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * $Id$
 *
 */


#include <EthernetDevice.h>
#include <memory>
#include <vector>
#include <string>
#include <sstream>
#include <map>
#include <controlAlarmSender.h>
#include <ambCDBAccess.h>
#include <hardwareDeviceImpl.h>
#include <maciContainerServices.h>


EthernetDevice::EthernetDevice(const ACE_CString& name,
    maci::ContainerServices* containerServices):
    Control::HardwareDeviceImpl(name, containerServices),
    AmbCDBAccessor(containerServices)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    ethernetDeviceIF_mp.reset();
}

EthernetDevice::~EthernetDevice()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    ethernetDeviceIF_mp.reset();
}

void EthernetDevice::getDeviceUniqueId(std::string& deviceID)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        std::auto_ptr< const ControlXmlParser > ethConfig(getElement(
            "EthernetConfig"));
        deviceID = ethConfig->getStringAttribute("macAddress");
    }
    catch(const ControlExceptions::XmlParserErrorExImpl& ex)
    {
        deviceID = "00:00:00:00:00:00";
        const std::string fnName(__PRETTY_FUNCTION__);
        const std::string error(": Unable to read deviceID from the CDB");
        setError(fnName + error);
    }
}

/* ----------------------- [ Lifecycle Interface ] ------------------ */
void EthernetDevice::hwStartAction()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    if(ethernetDeviceIF_mp.get() != 0)
    {
        ethernetDeviceIF_mp->close();
    }
}

void EthernetDevice::hwConfigureAction()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        // Opening the socket
        ethernetDeviceIF_mp->open(hostname_m, port_m, timeout_m, lingerTime_m,
            retries_m, parameters_m);
    }
    catch(const SocketOperationFailedException& ex)
    {
        setError(ex.getErrorMessage());

        if(alarmSender_m->isAlarmSet(NOCONNECT) == false)
        {
            alarmSender_m->activateAlarm(NOCONNECT);
        }

        return;
    }


    if(alarmSender_m->isAlarmSet(NOCONNECT) == true)
    {
        alarmSender_m->deactivateAlarm(NOCONNECT);
    }
}

void EthernetDevice::cleanUp()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    alarmSender_m->forceTerminateAllAlarms();
    delete alarmSender_m;
    alarmSender_m = 0;

    HardwareDeviceImpl::cleanUp();
}

void EthernetDevice::initialize()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    HardwareDeviceImpl::initialize();
    try
    {
        std::auto_ptr< const ControlXmlParser > ethConfig(getElement(
            "EthernetConfig"));

        hostname_m = ethConfig->getStringAttribute("hostname");
        port_m = ethConfig->getLongAttribute("port");
        timeout_m = ethConfig->getDoubleAttribute("timeoutRxTx");
        lingerTime_m = ethConfig->getLongAttribute("lingerTime");
        retries_m = ethConfig->getLongAttribute("retries");

        std::auto_ptr< std::vector< ControlXmlParser > > parameters(
            ethConfig->getElements("parameters"));
        ///
        /// TODO
        /// Thomas, Apr 27, 2009
        ///
        /// It's not clear yet how or where to implement this in the XML/XSDs.
         for(std::vector< ControlXmlParser >::iterator iter(parameters->begin());
            iter != parameters->end(); ++iter)
        {
            parameters_m.insert(
                std::make_pair("param", iter->getStringAttribute("param")));
        }

        // Initialize alarms.
        alarmSender_m =  new Control::AlarmSender;
        std::vector< Control::AlarmInformation > aivector;
        {
            Control::AlarmInformation ai;
            ai.alarmCode = NOCONNECT;
            aivector.push_back(ai);
        }

        CORBA::String_var compName(acscomponent::ACSComponentImpl::name());
        alarmSender_m->initializeAlarms("EthernetDevice", compName.in(),
            aivector);
        alarmSender_m->forceTerminateAllAlarms();

    }
    catch(const ControlExceptions::CDBErrorExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getLifeCycleEx();
    }
    catch(const ControlExceptions::XmlParserErrorExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getLifeCycleEx();
    }

}

void EthernetDevice::hwInitializeAction()
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}

void EthernetDevice::hwOperationalAction()
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}

void EthernetDevice::hwStopAction()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    if(ethernetDeviceIF_mp.get() != 0)
    {
        ethernetDeviceIF_mp->close();
    }
}

void EthernetDevice::command(int address, ACS::Time& timestamp)
{
    try
    {
        ethernetDeviceIF_mp->command(address);
        timestamp = ::getTimeStamp();
    }
    catch(const SocketOperationFailedException& ex)
    {
        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", ex.getErrorMessage());
        throw nex;
    }
    catch(const IllegalParameterException& ex)
    {
        EthernetDeviceExceptions::IllegalParameterExImpl nex(__FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", ex.getErrorMessage());
        throw nex;
    }
    catch(const EthernetDeviceException& ex)
    {
        EthernetDeviceExceptions::EthernetErrorExImpl nex(__FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", ex.getErrorMessage());
        throw nex;
    }
    catch(...)
    {
        EthernetDeviceExceptions::EthernetErrorExImpl ex(__FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", "Unexpected exception caught.");
        ex.log();
        throw ex;
    }
}
