/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 *
 * who       when      what
 * --------  --------  ----------------------------------------------
 * ntroncos  2008-05-07  created 
 */


#include <configDataAccess.h>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/framework/Wrapper4InputSource.hpp>
#include <xercesc/dom/DOMException.hpp>
#include <xercesc/validators/common/Grammar.hpp>



ConfigDataAccessor::ConfigDataAccessor(const std::string &xml, const std::string &xsd):
        ControlXmlParser()
{
    const char* __METHOD__= "ConfigDataAccessor::ConfigDataAccessor";
    ACS_TRACE(__METHOD__);

    /* Use Xerces to parse this XML */
    try{


    parser.setErrorHandler((ErrorHandler *)&errorHandler);
    

    parser.setDoValidation(true);
    parser.setDoNamespaces(true);    // optional
    parser.setDoSchema(true);
    parser.useCachedGrammarInParse(true);
    //parser.setExternalNoNamespaceSchemaLocation("");
    parser.setValidationScheme(AbstractDOMParser::Val_Always);


    MemBufInputSource xsdSource((XMLByte *)xsd.c_str(), (unsigned
int)xsd.size(), "membuffer.xsd");
    parser.loadGrammar(xsdSource, Grammar::SchemaGrammarType, true);
    //parser->setExternalNoNamespaceSchemaLocation("file:///myxml.xsd");
    MemBufInputSource source((XMLByte *)xml.c_str(), (unsigned
int)xml.size(), "membuffer.xml");
    //source.setEncoding(XML("UTF-8")); // Not that this actually makes any difference...
    parser.parse(source);
    } catch (DOMException &ex) {
      ControlExceptions::XmlParserErrorExImpl nex(__FILE__,__LINE__,__METHOD__);
      nex.addData("DOMException", XMLString::transcode(ex.getMessage()));
      throw nex;
        
    //SAXException 	Any SAX exception, possibly wrapping another exception. 
    } catch (SAXException &ex) {
      ControlExceptions::XmlParserErrorExImpl nex(__FILE__,__LINE__,__METHOD__);
      nex.addData("SAXException", XMLString::transcode(ex.getMessage()));
      throw nex;
    //XMLException 	An exception from the parser or client handler code. 
    } catch (XMLException &ex) {
      ControlExceptions::XmlParserErrorExImpl nex(__FILE__,__LINE__,__METHOD__);
      nex.addData("XMLException", XMLString::transcode(ex.getMessage()));
      throw nex;
    } catch (...) {
      ControlExceptions::XmlParserErrorExImpl nex(__FILE__,__LINE__,__METHOD__);
      nex.addData("additional","Unknown exception");
      throw nex;
    }
    //If there where parse errors they will be stored in the error handler.
    //create an exception and bail out.
    if (errorHandler.getSawErrors()){
      ControlExceptions::XmlParserErrorExImpl nex(__FILE__,__LINE__,__METHOD__);
      nex.addData("Parsing","Error while parsing");
      nex.addData("Xerces-",(errorHandler.getErrorString()).c_str());
      throw nex;
      std::cout<<errorHandler.getSawErrors()<<std::endl;
    }
    elementReference = (parser.getDocument())->getDocumentElement();

}

    ConfigDataAccessor::~ConfigDataAccessor() {
        ACS_TRACE("ConfigDataAccessor::ConfigDataAccessor");

    }

    /*___oOo___*/
