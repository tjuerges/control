/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2005 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 *
 */

#include <xercesc/dom/DOM.hpp>
#include <xercesc/dom/DOMException.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/framework/Wrapper4InputSource.hpp>
#include <xercesc/validators/common/Grammar.hpp>

#include <ambTMCDBAccess.h>
#include <TMCDBComponentC.h>
#include <TmcdbErrType.h>

AmbTMCDBAccessor::AmbTMCDBAccessor(maci::ContainerServices* containerServices, 
    const char* tmcdbComponentName) : ControlXmlParser() {

    const char* __METHOD__= "AmbTMCDBAccessor::AmbTMCDBAccessor";
    AUTO_TRACE(__METHOD__);

    std::string compName_m = containerServices->getName().c_str();
    ACS_SHORT_LOG((LM_INFO, "Looking XML config data for component %s", compName_m.c_str()));

    cdbName_m = "alma/";
    cdbName_m += compName_m.c_str();

    try {
        // Get a reference to the TMCDBComponent component.    
        maci::SmartPtr<TMCDB::TMCDBComponent> tmcdb;
        tmcdb = containerServices->getComponentSmartPtr<TMCDB::TMCDBComponent>(tmcdbComponentName);
        
        // Get XML document and schema from the TMCDBComponent.
        TMCDB::AssemblyConfigXMLData_var xmlData;
        xmlData = tmcdb->getComponentConfigData(compName_m.c_str());

        // Set the root element reference in the ControlXmlParser base class.     
        elementReference = parse(std::string(xmlData->xmlDoc), std::string(xmlData->schema));
                
    } catch (...) {
        ACS_SHORT_LOG((LM_INFO, "Error getting XML data from TMCDB, trying with the CDB now"));
        elementReference = parse(getXMLFromCDB(containerServices));
        return;    
    }
}

AmbTMCDBAccessor::~AmbTMCDBAccessor() {
    AUTO_TRACE("AmbTMCDBAccessor::~AmbTMCDBAccessor");
}

DOMElement* AmbTMCDBAccessor::parse(std::string xml, std::string xsd) {
    const char* __METHOD__= "AmbTMCDBAccessor::parse";
    AUTO_TRACE(__METHOD__);
    
    // Parse the XML document along with its Schema.
    try {
        parser_m.setErrorHandler((ErrorHandler*) &errorHandler_m);
        parser_m.setDoValidation(true);
        parser_m.setDoNamespaces(true); // optional
        parser_m.setDoSchema(true);
        parser_m.useCachedGrammarInParse(true);
        parser_m.setValidationScheme(AbstractDOMParser::Val_Always);

        MemBufInputSource xsdSource((XMLByte*) xsd.c_str(), (unsigned int) xsd.size(), "membuffer.xsd");
        parser_m.loadGrammar(xsdSource, Grammar::SchemaGrammarType, true);
        
        MemBufInputSource source((XMLByte*) xml.c_str(), (unsigned int) xml.size(), "membuffer.xml");        
        parser_m.parse(source);
        
    } catch (DOMException& ex) {
        ControlExceptions::CDBErrorExImpl nex(__FILE__,__LINE__,__METHOD__);
        nex.addData("DOMException", XMLString::transcode(ex.getMessage()));
        throw nex;
    } catch (SAXException& ex) {
        ControlExceptions::CDBErrorExImpl nex(__FILE__,__LINE__,__METHOD__);
        nex.addData("SAXException", XMLString::transcode(ex.getMessage()));
        throw nex;
    } catch (XMLException& ex) {
        ControlExceptions::CDBErrorExImpl nex(__FILE__,__LINE__,__METHOD__);
        nex.addData("XMLException", XMLString::transcode(ex.getMessage()));
        throw nex;
    } catch (...) {
        ControlExceptions::CDBErrorExImpl nex(__FILE__,__LINE__,__METHOD__);
        nex.addData("additional","Unknown exception");
        throw nex;
    }

    // If there where parse errors they will be stored in the error handler.
    // create an exception and bail out.
    if (errorHandler_m.getSawErrors()){
        ControlExceptions::XmlParserErrorExImpl nex(__FILE__,__LINE__,__METHOD__);
        nex.addData("Parsing", "Error while parsing");
        nex.addData("Xerces-", (errorHandler_m.getErrorString()).c_str());
        throw nex;
    }
    
    return (parser_m.getDocument())->getDocumentElement();
}

DOMElement* AmbTMCDBAccessor::parse(std::string xml) {
    const char* __METHOD__= "AmbTMCDBAccessor::parse";
    AUTO_TRACE(__METHOD__);
    
    const char* dao = xml.c_str();
    
    parser_m.setDoNamespaces(true);
    parser_m.setDoSchema(false);
    parser_m.setDoValidation(false);
    parser_m.setErrorHandler(&errorHandler_m);
    
    try {
        MemBufInputSource memInput(reinterpret_cast<const XMLByte*>(dao), strlen(dao), "FakeSystemId", false);
        parser_m.resetDocumentPool();
        parser_m.parse(memInput);
    } catch (...) {
        throw ControlExceptions::CDBErrorExImpl(__FILE__, __LINE__, __METHOD__);
    }
    return (parser_m.getDocument())->getDocumentElement();
}

std::string AmbTMCDBAccessor::getXMLFromCDB(maci::ContainerServices* containerServices) {
    const char* __METHOD__= "AmbTMCDBAccessor::getXMLFromCDB";
    AUTO_TRACE(__METHOD__);
    
    CDB::DAL_ptr dal_p;
    try {
        dal_p = containerServices->getCDB();
    } catch(acsErrTypeContainerServices::CanNotGetCDBExImpl& ex) {
        throw ControlExceptions::CDBErrorExImpl(ex, __FILE__, __LINE__, __METHOD__);
    }

    std::string cdbName = "alma/";
    cdbName += containerServices->getName().c_str();

    ACS_SHORT_LOG((LM_INFO, "Looking for CDB Entry %s", cdbName.c_str()));  

    std::string dao;
    try {
        dao = dal_p->get_DAO(cdbName.c_str());
    } catch(cdbErrType::CDBXMLErrorEx& ex) {
        throw ControlExceptions::CDBErrorExImpl(ex, __FILE__, __LINE__, __METHOD__);
    } catch(cdbErrType::CDBRecordDoesNotExistEx& ex) {
        throw ControlExceptions::CDBErrorExImpl(ex, __FILE__, __LINE__, __METHOD__);
    }
    
    return dao;
}
