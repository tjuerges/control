
#include<phonyXmlTmcdbComponent.h>
#include<fstream>
#include<sstream>
#include<iostream>
#include <cstdlib>

//...

std::string  File2String::readFile(const std::string file) {
    std::ifstream inFile(file.c_str());
    std::string str;
    std::string buf;
    if( !inFile ) {
        std::cerr << "Couldn´t open input file" << file<<std::endl;
    }
    while(std::getline(inFile, buf)){
        str += buf;
        str += "\n";
    }
    return str;
}

PhonyXmlTmcdbComponent::PhonyXmlTmcdbComponent(){}

PhonyXmlTmcdbComponent::~PhonyXmlTmcdbComponent(){}

std::string PhonyXmlTmcdbComponent::getConfigXml(const std::string &deviceId)
{
    std::string retVal;
    std::stringstream fileLoc;
    const char *appdata = std::getenv ( "ACS_CDB" );
    fileLoc<<appdata;
    fileLoc<<"/TMCDB_DATA/"<<deviceId<<".xml";
    try{
        retVal = File2String::readFile(fileLoc.str());
    }catch(...){

    }
    return retVal;
}

std::string PhonyXmlTmcdbComponent::getConfigXsd(const std::string &deviceId)
{
    std::string retVal;
    std::stringstream fileLoc;
    const char *appdata = std::getenv ( "ACS_CDB" );
    fileLoc<<appdata;
    fileLoc<<"/TMCDB_DATA/"<<deviceId<<".xsd";
    try{
        retVal = File2String::readFile(fileLoc.str());
    }catch(...){

    }
    return retVal;
}
