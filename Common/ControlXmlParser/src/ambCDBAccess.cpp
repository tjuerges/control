/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2005 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 *
 * who       when      what
 * --------  --------  ----------------------------------------------
 * jkern  2005-06-22  created 
 */

//static char *rcsId="@(#) $Id$"; 
//static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <ambCDBAccess.h>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/dom/DOM.hpp>

AmbCDBAccessor::AmbCDBAccessor(maci::ContainerServices* containerServices):
        ControlXmlParser()
{
    const char* __METHOD__= "AmbCDBAccessor::AmbCDBAccessor";
    AUTO_TRACE(__METHOD__);
    char*              dao      = NULL;

    /* Configure the Parser */
    parser.setDoNamespaces(true);
    parser.setDoSchema(false);
    parser.setDoValidation(false);
    parser.setErrorHandler(&errorHandler);

    /* Access the CDB and get all the relavent information */
    CDB::DAL_ptr dal_p;
    try{
        dal_p = containerServices->getCDB();
    }
    catch(acsErrTypeContainerServices::CanNotGetCDBExImpl &_ex){
        //if (dal_p==NULL) {
        throw ControlExceptions::CDBErrorExImpl(_ex,__FILE__,__LINE__,__METHOD__);
    }

    /* Make a mutable copy of the passed in name append an alma: to the
     * beginning and replace the colons with slashes.
     */
    std::string cdbName = "alma/";
    cdbName += containerServices->getName().c_str();

    ACS_SHORT_LOG((LM_INFO, "Looking for CDB Entry %s", cdbName.c_str()));  

    try {
        dao = dal_p->get_DAO(cdbName.c_str());
    }
    catch(cdbErrType::CDBXMLErrorEx& _ex) {
        // In this case the XML file exists in the CDB, but it contains errors.
        cdbErrType::CDBXMLErrorExImpl ex(_ex);
        ACS_SHORT_LOG((LM_ERROR, "XML error in CDB entry %s: %s", ex.getFilename().c_str(), ex.getErrorString().c_str()));
        throw ControlExceptions::CDBErrorExImpl(_ex,__FILE__,__LINE__,__METHOD__);
    } catch(cdbErrType::CDBRecordDoesNotExistEx) {
        /* Assume that this means the requested component does not exist
         * use the default instead
         */
        ACS_SHORT_LOG((LM_WARNING, "CDB Entry not found for %s using Generic",
                    cdbName.c_str()));

        cdbName.erase(cdbName.rfind("/")+1);
        cdbName += "Generic";

        try {
            dao = dal_p->get_DAO(cdbName.c_str());
        }
        catch(cdbErrType::cdbErrTypeEx& _ex) {
            throw ControlExceptions::CDBErrorExImpl(_ex,__FILE__,__LINE__,__METHOD__);
        }
    }

    //if (dao == NULL) {
    // throw ControlExceptions::CDBErrorExImpl(__FILE__,__LINE__,__METHOD__);
    //}

    /* Use Xerces to parse this XML */
    try {
        /* Load the data into an input source */
        MemBufInputSource memInput((const XMLByte *)dao, strlen(dao),
                cdbName.c_str(),false);
        /* and Parse */
        parser.resetDocumentPool();
        parser.parse(memInput);
    }
    catch (...) {
        throw ControlExceptions::CDBErrorExImpl(__FILE__,__LINE__,__METHOD__);
    }
    elementReference = (parser.getDocument())->getDocumentElement();


    /* Now set the member variable for use in the derived classes */
    cdbName_m = cdbName.erase(0,strlen("alma/")).c_str();

    delete[] dao;
    }

    AmbCDBAccessor::~AmbCDBAccessor(){
        AUTO_TRACE("AmbCDBAccessor::AmbCDBAccessor");
    }

    /*___oOo___*/
