#include <controlSAXErrorHandler.h>
#include <xercesc/util/XMLString.hpp>

ControlSAXErrorHandler::ControlSAXErrorHandler() :
    sawErrors_m(false),
    errorStream_m()
{
}

ControlSAXErrorHandler::~ControlSAXErrorHandler()
{
}

void ControlSAXErrorHandler::warning (const SAXParseException &exc)
{
    errorStream_m << "\nWarning at file ";
    handleError(exc);
    
}
void ControlSAXErrorHandler::error (const SAXParseException &exc)
{
        errorStream_m << "\nError at file ";
    handleError(exc);

}
void ControlSAXErrorHandler::fatalError (const SAXParseException &exc)
{
        errorStream_m << "\nFatal Error at file ";
    handleError(exc);
    
}
// ---------------------------------------------------------------------------
//  DOMCountHandlers: Overrides of the DOM ErrorHandler interface
// ---------------------------------------------------------------------------
bool ControlSAXErrorHandler::handleError(const SAXParseException &exc)
{
    sawErrors_m = true;
    errorStream_m << XMLString::transcode(exc.getSystemId())
         << ", line " << exc.getLineNumber()
         << ", char " << exc.getColumnNumber()
         << "\n  Message: " << XMLString::transcode(exc.getMessage()) << std::endl;

    return true;
}

void ControlSAXErrorHandler::resetErrors()
{
    sawErrors_m = false;
    errorStream_m.str("");
}

bool ControlSAXErrorHandler::getSawErrors() const {
   return sawErrors_m;
}

std::string ControlSAXErrorHandler::getErrorString() const {
    return errorStream_m.str();
}

