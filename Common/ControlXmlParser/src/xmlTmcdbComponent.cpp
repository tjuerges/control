/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 *
 */

static char *rcsId="@(#) $Id$";
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <xmlTmcdbComponent.h>
#include <TmcdbErrType.h>

//THIS CONTRUCTOR IS DEPRECATED>
XmlTmcdbComponent::XmlTmcdbComponent(maci::ContainerServices* containerServices,
    const char *tmcdbCompName) : tmcdbComponentName_m(tmcdbCompName), currentDeviceId_m("NODEVICE") {
    
    tmdcbOk_m = false;
    phonyXmlTmcdb_m = new PhonyXmlTmcdbComponent();
    cs_m = containerServices;
    try {
        tmcdb_m = containerServices->getDefaultComponentSmartPtr<TMCDB::Access>
            ("IDL:alma/TMCDB/Access:1.0");
        tmdcbOk_m = true;
    } catch (maciErrType::CannotGetComponentExImpl ex) {
        ACS_SHORT_LOG((LM_WARNING, "Error getting TMCDB component"));
    }
}

XmlTmcdbComponent::XmlTmcdbComponent(maci::ContainerServices* containerServices) : 
    tmcdbComponentName_m(), currentDeviceId_m("NODEVICE") {

    tmdcbOk_m = false;
    phonyXmlTmcdb_m = new PhonyXmlTmcdbComponent();
    cs_m = containerServices;
    try {
        tmcdb_m = containerServices->getDefaultComponentSmartPtr<TMCDB::Access>
            ("IDL:alma/TMCDB/Access:1.0");
        tmdcbOk_m = true;
    } catch (maciErrType::CannotGetComponentExImpl ex) {
        ACS_SHORT_LOG((LM_WARNING, "Error getting TMCDB component"));
    }
}

XmlTmcdbComponent::~XmlTmcdbComponent() {
    delete phonyXmlTmcdb_m;
    //The smart pointer deals with releasing the reference.
}


std::string XmlTmcdbComponent::getConfigXml(const std::string &deviceId) {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_INFO, "Looking configuration data for S/N %s", deviceId.c_str()));
    ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_INFO,
    		"Current device S/N is %s", currentDeviceId_m.c_str()));
    if (deviceId != currentDeviceId_m) {
        if (tmdcbOk_m) {
            try {
            	ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
            			(LM_INFO, "GETTING XML conf. data from TMCDB for %s", deviceId.c_str()));
                currentXmlData_m = tmcdb_m->getAssemblyConfigData(deviceId.c_str());
            	ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
            			(LM_INFO, "GOT XML conf. data from TMCDB for %s", deviceId.c_str()));
                currentDeviceId_m = deviceId;
            } catch (TmcdbErrType::TmcdbNoSuchRowEx& ex) {
            	ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
            			(LM_WARNING, "Conf. XML data not found in TMCDB(TmcdbNoSuchRowEx), trying to get it from filesystem"));
                return phonyXmlTmcdb_m->getConfigXml(deviceId);
            } catch (TmcdbErrType::TmcdbSqlEx& ex) {
            	ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
            			(LM_WARNING, "Conf. XML data not found in TMCDB(TmcdbSqlEx), trying to get it from filesystem"));
                return phonyXmlTmcdb_m->getConfigXml(deviceId);
            } catch(...) {
            	ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
            			(LM_WARNING, "UNKOWN EXCEPTION!!, trying to get XML from filesystem"));
                return phonyXmlTmcdb_m->getConfigXml(deviceId);
            }
        } else {
            ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
           		(LM_WARNING, "Tmcdb access is _nil, trying to get data from filesystem"));
            return phonyXmlTmcdb_m->getConfigXml(deviceId);
        }
    }
    return std::string(currentXmlData_m->xmlDoc);
}

std::string XmlTmcdbComponent::getConfigXsd(const std::string &deviceId) {

    if (deviceId != currentDeviceId_m) {
        if (tmdcbOk_m) {
            try {
            	ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
            			(LM_DEBUG, "GETTING XSD conf. data from TMCDB"));
                currentXmlData_m = tmcdb_m->getAssemblyConfigData(deviceId.c_str());
            	ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
            			(LM_DEBUG, "GOT XSD conf. data from TMCDB"));
                currentDeviceId_m = deviceId;
            } catch (TmcdbErrType::TmcdbNoSuchRowEx& ex) {
            	ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
            			(LM_WARNING, "Conf. XSD data not found in TMCDB(TmcdbNoSuchRowEx), trying to get it from filesystem"));
                return phonyXmlTmcdb_m->getConfigXsd(deviceId);
            } catch (TmcdbErrType::TmcdbSqlEx& ex) {
            	ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
            			(LM_WARNING, "Conf. XSD data not found in TMCDB(TmcdbSqlEx), trying to get it from filesystem"));
                return phonyXmlTmcdb_m->getConfigXsd(deviceId);
            } catch(...) {
            	ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
            			(LM_WARNING, "UNKOWN EXCEPTION!!, trying to get XSD from filesystem"));
                return phonyXmlTmcdb_m->getConfigXsd(deviceId);
            }
        } else {
            ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
           		(LM_WARNING, "Tmcdb access is _nil, trying to get data from filesystem"));
            return phonyXmlTmcdb_m->getConfigXsd(deviceId);
        }
    }
    return std::string(currentXmlData_m->schema);
}
