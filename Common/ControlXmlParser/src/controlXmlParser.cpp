/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2005 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 *
 * who       when      what
 * --------  --------  ----------------------------------------------
 * jkern  2005-06-22  created 
 */

//static char *rcsId="@(#) $Id$"; 
//static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <controlXmlParser.h>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/dom/DOM.hpp>

unsigned long    ControlXmlParser::xercesReferenceCount = 0;
bool             ControlXmlParser::xercesInitialized = false;
pthread_mutex_t  ControlXmlParser::xercesMutex = PTHREAD_ERRORCHECK_MUTEX_INITIALIZER_NP;

/* ------------- ControlXmlParser -------------------------------*/
ControlXmlParser::ControlXmlParser() {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    incrementInstance();
}

ControlXmlParser::ControlXmlParser(const DOMElement*  Element){
    AUTO_TRACE(__PRETTY_FUNCTION__);
    incrementInstance();
    elementReference = Element;
}

ControlXmlParser::ControlXmlParser(const ControlXmlParser& src){
    AUTO_TRACE(__PRETTY_FUNCTION__);
    elementReference = src.elementReference;
    incrementInstance();
}

ControlXmlParser::~ControlXmlParser(){
    AUTO_TRACE(__PRETTY_FUNCTION__);
    decrementInstance();
}

void ControlXmlParser::incrementInstance(){
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&ControlXmlParser::xercesMutex);
    if (!ControlXmlParser::xercesInitialized) {
        // Initialize the Platform Utilities
        ACS_TRACE("Initializing Xerces");
        XMLPlatformUtils::Initialize();
        ControlXmlParser::xercesInitialized = true;
    }
    ControlXmlParser::xercesReferenceCount++;
    pthread_mutex_unlock(&ControlXmlParser::xercesMutex);
}

void ControlXmlParser::decrementInstance(){
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&ControlXmlParser::xercesMutex);
    ControlXmlParser::xercesReferenceCount--;
    if (ControlXmlParser::xercesReferenceCount == 0) {
        // Initialize the Platform Utilities
        ACS_TRACE("Terminating Xerces");
        XMLPlatformUtils::Terminate();
        ControlXmlParser::xercesInitialized = false;
    }
    pthread_mutex_unlock(&ControlXmlParser::xercesMutex);
}

const long 
    ControlXmlParser::getLongAttribute(const char* attributeName) const
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    XMLCh* xmlAttributeName = XMLString::transcode(attributeName);

    /* Ensure that the attribute exists */
    if (!elementReference->hasAttribute(xmlAttributeName)) {
        ACS_SHORT_LOG((LM_ERROR,
                    "Attribute \"%s\" undefined for element of type %s.",
                    attributeName,
                    XMLString::transcode(elementReference->getTagName())));
        XMLString::release(&xmlAttributeName);
        throw ControlExceptions::XmlParserErrorExImpl(__FILE__,__LINE__,
                __PRETTY_FUNCTION__);
    }

    const XMLCh* xmlAttributeValue = 
        elementReference->getAttribute(xmlAttributeName);
    if (xmlAttributeValue == NULL) {
        ACS_SHORT_LOG((LM_ERROR,"Error retrieving attribute \"%s\".",
                    attributeName));
        throw ControlExceptions::XmlParserErrorExImpl(__FILE__,__LINE__,
                __PRETTY_FUNCTION__);
    }

    char* valueString = XMLString::transcode(xmlAttributeValue) ;
    long returnValue = atol(valueString);
    XMLString::release(&valueString);
    return returnValue;
}

const double 
ControlXmlParser::getDoubleAttribute(const char* attributeName) const
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    XMLCh* xmlAttributeName = XMLString::transcode(attributeName);

    if (!elementReference->hasAttribute(xmlAttributeName)) {
        XMLString::release(&xmlAttributeName);
        ACS_SHORT_LOG((LM_ERROR,
                    "Attribute \"%s\" undefined for element of type %s.",
                    attributeName,
                    XMLString::transcode(elementReference->getTagName())));
        XMLString::release(&xmlAttributeName);
        throw ControlExceptions::XmlParserErrorExImpl(__FILE__,__LINE__,
                __PRETTY_FUNCTION__);
    }


    const XMLCh* xmlAttributeValue = 
        elementReference->getAttribute(xmlAttributeName);
    XMLString::release(&xmlAttributeName);
    if (xmlAttributeValue == NULL) {
        ACS_SHORT_LOG((LM_ERROR,"Error retrieving attribute \"%s\".",
                    attributeName));
        throw ControlExceptions::XmlParserErrorExImpl(__FILE__,__LINE__,
                __PRETTY_FUNCTION__);
    }

    char* valueString = XMLString::transcode(xmlAttributeValue) ;
    double returnValue = atof(valueString);
    XMLString::release(&valueString);
    return returnValue;
}

const bool   
ControlXmlParser::getBoolAttribute(const char* attributeName) const
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    XMLCh* xmlAttributeName = XMLString::transcode(attributeName);

    if (!elementReference->hasAttribute(xmlAttributeName)) {
        ACS_SHORT_LOG((LM_ERROR,
                    "Attribute \"%s\" undefined for element of type %s.",
                    attributeName,
                    XMLString::transcode(elementReference->getTagName())));
        XMLString::release(&xmlAttributeName);
        throw ControlExceptions::XmlParserErrorExImpl(__FILE__,__LINE__,
                __PRETTY_FUNCTION__);
    }

    const XMLCh* xmlAttributeValue = 
        elementReference->getAttribute(xmlAttributeName);
    XMLString::release(&xmlAttributeName);
    if (xmlAttributeValue == NULL) {
        ACS_SHORT_LOG((LM_ERROR,"Error retrieving attribute \"%s\".",
                    attributeName));
        throw ControlExceptions::XmlParserErrorExImpl(__FILE__,__LINE__,
                __PRETTY_FUNCTION__);
    }

    XMLCh* xmlCompareValue = XMLString::transcode("true");
    bool returnValue = XMLString::equals(xmlAttributeValue,xmlCompareValue);
    XMLString::release(&xmlCompareValue);
    return returnValue;
}


const std::string   
ControlXmlParser::getStringAttribute(const char* attributeName) const
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    XMLCh* xmlAttributeName = XMLString::transcode(attributeName);

    if (!elementReference->hasAttribute(xmlAttributeName)) {
        ACS_SHORT_LOG((LM_ERROR,
                    "Attribute \"%s\" undefined for element of type %s.",
                    attributeName,
                    XMLString::transcode(elementReference->getTagName())));
        XMLString::release(&xmlAttributeName);
        throw ControlExceptions::XmlParserErrorExImpl(__FILE__,__LINE__,
                __PRETTY_FUNCTION__);
    }

    const XMLCh* xmlAttributeValue = 
        elementReference->getAttribute(xmlAttributeName);
    XMLString::release(&xmlAttributeName);
    if (xmlAttributeValue == NULL) {
        ACS_SHORT_LOG((LM_ERROR,"Error retrieving attribute \"%s\".",
                    attributeName));
        throw ControlExceptions::XmlParserErrorExImpl(__FILE__,__LINE__,
                __PRETTY_FUNCTION__);
    }

    std::string returnValue = XMLString::transcode(xmlAttributeValue);
    return returnValue;
}

const std::string   
ControlXmlParser::getStringData() const
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    const XMLCh* xmlDataValue = ((DOMCharacterData *)elementReference->getFirstChild())->getData();
    std::string returnValue = XMLString::transcode(xmlDataValue);
    //XMLString::release(&xmlDataValue);
    return returnValue;
}

std::auto_ptr<const ControlXmlParser> 
ControlXmlParser::getElement(const char*  elementName) const
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    XMLCh* xmlPropName = XMLString::transcode(elementName);
    DOMNodeList* dnl = elementReference->getChildNodes();
    if (dnl == NULL) {
        ACS_SHORT_LOG((LM_ERROR,"No child nodes defined for elememt \"%s\".",
                    XMLString::transcode(elementReference->getTagName())));
        throw ControlExceptions::XmlParserErrorExImpl(__FILE__,__LINE__,
                __PRETTY_FUNCTION__);
    }

    for (unsigned int idx = 0; idx < dnl->getLength(); idx++) {
        const DOMNode* node = dnl->item(idx);
        if (XMLString::equals(node->getNodeName(), xmlPropName)) {
            if (node->getNodeType() == DOMNode::ELEMENT_NODE) {
                XMLString::release(&xmlPropName);
                // really this should be a dynamic_cast. However this does not always
                // work due to problems with the xerces library (as distributed with
                // ACS-4.0.2). The problem occurs when two or more components (in the
                // same container) are loaded and unloaded more than once. The dynamic
                // cast works the first time and fails the second time. This looks to
                // be similar to the bug described in
                // http://issues.apache.org/bugzilla/show_bug.cgi?id=17320 and the the
                // solution is to not use a dynamic cast.
                return std::auto_ptr<const ControlXmlParser>
                    (new ControlXmlParser(dynamic_cast<const DOMElement*>(node)));
            }
        }
    }
    XMLString::release(&xmlPropName);

    /* If we reach this point it is an error */
    ACS_SHORT_LOG((LM_ERROR,"Node \"%s\" not found for element of type \"%s\".",
                elementName, 
                XMLString::transcode(elementReference->getTagName())));
    throw ControlExceptions::XmlParserErrorExImpl(__FILE__,__LINE__,
            __PRETTY_FUNCTION__);
}

std::auto_ptr<std::vector<ControlXmlParser> >
ControlXmlParser::getElements(const char* elementName) const
{
    AUTO_TRACE(__PRETTY_FUNCTION__);


    auto_ptr<std::vector<ControlXmlParser> >
        elementList(new std::vector<ControlXmlParser>);

    XMLCh* xmlPropName = XMLString::transcode(elementName);

    try {
        DOMNodeList* dnl = elementReference->getChildNodes();
        for (unsigned int idx = 0; idx < dnl->getLength(); idx++) {
            const DOMNode* node = dnl->item(idx);
            if (XMLString::equals(node->getNodeName(), xmlPropName)) {
                if (node->getNodeType() == DOMNode::ELEMENT_NODE) {
                    elementList->
                        push_back(ControlXmlParser(dynamic_cast<const DOMElement*>(node)));
                }
            }
        }
    }
    catch (...) {
        throw ControlExceptions::XmlParserErrorExImpl(__FILE__,__LINE__,
                __PRETTY_FUNCTION__);
    }
    XMLString::release(&xmlPropName);

    return elementList;
}


/* These are the ACE_CString versions of the calls */
const long
    ControlXmlParser::getLongAttribute(const ACE_CString& attributeName) const
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    return getLongAttribute(attributeName.c_str());
}

const double 
    ControlXmlParser::getDoubleAttribute(const ACE_CString& attributeName) const
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    return getDoubleAttribute(attributeName.c_str());
}

const bool
    ControlXmlParser::getBoolAttribute(const ACE_CString& attributeName) const
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    return getBoolAttribute(attributeName.c_str());
}


const std::string
    ControlXmlParser::getStringAttribute(const ACE_CString& attributeName) const
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    return getStringAttribute(attributeName.c_str());
}


std::auto_ptr<const ControlXmlParser> 
ControlXmlParser::getElement(const ACE_CString& elementName) const
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    return getElement(elementName.c_str());
}

std::auto_ptr<std::vector<ControlXmlParser> >
ControlXmlParser::getElements(const ACE_CString& elementName) const
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    try {
        return getElements(elementName.c_str());
    }
    catch (ControlExceptions::XmlParserErrorExImpl& ex) {
        throw ControlExceptions::XmlParserErrorExImpl(ex,__FILE__,__LINE__,
                __PRETTY_FUNCTION__);
    }
}
/*___oOo___*/
