/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */

static char *rcsId="@(#) $Id$";
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <sstream>

#include <XmlConfigDataTesterImpl.h>
#include <xmlTmcdbComponent.h>
#include <phonyXmlTmcdbComponent.h>
#include <configDataAccess.h>
#include <ControlDeviceExceptions.h>

XmlConfigDataTesterImpl::XmlConfigDataTesterImpl(const ACE_CString &name,
                 maci::ContainerServices* containerServices) :
                 AmbTMCDBAccessor(containerServices, "TMCDB"),
                 acscomponent::ACSComponentImpl(name, containerServices) {

    ACS_TRACE("XmlConfigDataTesterImpl constructor");
    ACS_TRACE(name.c_str());
}

XmlConfigDataTesterImpl::~XmlConfigDataTesterImpl() {
    ACS_TRACE("::XmlConfigDataTesterImpl::~XmlConfigDataTesterImpl");
}


void XmlConfigDataTesterImpl::initialize()
{
    ACS_TRACE("XmlConfigDataTesterImpl initialize");
}

void XmlConfigDataTesterImpl::cleanUp() {
    ACS_TRACE("XmlConfigDataTesterImpl cleanUp");
}

CORBA::Boolean XmlConfigDataTesterImpl::testXmlTmcdbComponent() {

    ACS_LOG(LM_SOURCE_INFO, "XmlConfigDataTesterImpl::testXmlTmcdbComponent",
        (LM_INFO, "testXmlTmcdbComponent called"));

    XmlTmcdbComponent tmcdb(getContainerServices(), "TMCDB");
    std::string xml = tmcdb.getConfigXml("goodxml");
    std::string xsd = tmcdb.getConfigXsd("goodxml");

    ACS_SHORT_LOG((LM_INFO, "XML = %s", xml.c_str()));
    ACS_SHORT_LOG((LM_INFO, "XSD = %s", xsd.c_str()));

    ConfigDataAccessor* configData = NULL;
    bool success = true;
    try {
        configData = new ConfigDataAccessor(xml, xsd);
        success = assertEquals(889923L, configData->getLongAttribute("orderid"));
        success = assertEquals(std::string("John Smith"), configData->getElement("orderperson")->getStringData());
        std::auto_ptr<std::vector<ControlXmlParser> > dataArray;
        dataArray = configData->getElements("item");
        success = assertEquals(std::string("Special Edition"), (*dataArray)[0].getElement("note")->getStringData());
        // ...
    } catch (ControlExceptions::XmlParserErrorExImpl &ex) {
        ACS_LOG(LM_SOURCE_INFO, "XmlConfigDataTesterImpl::testXmlTmcdbComponent", (LM_INFO, "Could not access config data"));
        delete configData;
        return false;
    }

    delete configData;
    return success;
}

CORBA::Boolean XmlConfigDataTesterImpl::testXmlTmcdbComponentNoConfiguration() {

    ACS_LOG(LM_SOURCE_INFO, "XmlConfigDataTesterImpl::testXmlTmcdbComponent",
        (LM_INFO, "testXmlTmcdbComponent called"));

    XmlTmcdbComponent tmcdb(getContainerServices(), "TMCDB");
    std::string xml = tmcdb.getConfigXml("ThisDoesntExist");
    std::string xsd = tmcdb.getConfigXsd("ThisDoesntExist");

    ACS_SHORT_LOG((LM_INFO, "XML = %s", xml.c_str()));
    ACS_SHORT_LOG((LM_INFO, "XSD = %s", xsd.c_str()));

	if (xml.length() == 0 || xsd.length() == 0) {
		ACS_SHORT_LOG((LM_INFO, "XML or XSD data has length zero"));
		return false;
	} else {
		return true;
	}
}

CORBA::Boolean XmlConfigDataTesterImpl::testXmlTmcdbComponentPhonyDelegation() {

    XmlTmcdbComponent tmcdb(getContainerServices(), "TMCDB");
    std::string xml = tmcdb.getConfigXml("goodxml");
    std::string xsd = tmcdb.getConfigXsd("goodxml");

    xml = tmcdb.getConfigXml("123456");
    xsd = tmcdb.getConfigXsd("123456");

    ACS_SHORT_LOG((LM_INFO, "XML from Phony = %s", xml.c_str()));
    ACS_SHORT_LOG((LM_INFO, "XSD from Phony = %s", xsd.c_str()));

    ConfigDataAccessor* configData = NULL;
    try {
        configData = new ConfigDataAccessor(xml, xsd);
    } catch (ControlExceptions::XmlParserErrorExImpl &ex) {
        ACS_LOG(LM_SOURCE_INFO, "XmlConfigDataTesterImpl::testXmlTmcdbComponent", (LM_INFO, "Could not access config data"));
        delete configData;
        return false;
    }

    delete configData;
    return true;
}

CORBA::Boolean XmlConfigDataTesterImpl::testPhonyXmlTmcdbComponent() {

    ACS_LOG(LM_SOURCE_INFO, "XmlConfigDataTesterImpl::testPhonyXmlTmcdbComponent",
        (LM_INFO, "testPhonyXmlTmcdbComponent called"));

    PhonyXmlTmcdbComponent tmcdb;
    std::string xml = tmcdb.getConfigXml("goodxml");
    std::string xsd = tmcdb.getConfigXsd("goodxml");

    ACS_SHORT_LOG((LM_INFO, "XML = %s", xml.c_str()));
    ACS_SHORT_LOG((LM_INFO, "XSD = %s", xsd.c_str()));

    ConfigDataAccessor* configData = NULL;
    try {
        ConfigDataAccessor* configData = new ConfigDataAccessor(xml, xsd);
    } catch (ControlExceptions::XmlParserErrorExImpl &ex) {
        ACS_LOG(LM_SOURCE_INFO, "XmlConfigDataTesterImpl::testXmlTmcdbComponent", (LM_INFO, "Could not access config data"));
        delete configData;
        return false;
    }

    delete configData;
    return true;
}

CORBA::Boolean XmlConfigDataTesterImpl::testAmbTMCDBAccessor() {
    bool success = true;
    try {
        success = assertEquals(889923L, getLongAttribute("orderid"));
        success = assertEquals(std::string("John Smith"), getElement("orderperson")->getStringData());
        std::auto_ptr<std::vector<ControlXmlParser> > dataArray;
        dataArray = getElements("item");
        success = assertEquals(std::string("Special Edition"), (*dataArray)[0].getElement("note")->getStringData());
    } catch (ControlExceptions::XmlParserErrorExImpl &ex) {
        ACS_LOG(LM_SOURCE_INFO, "XmlConfigDataTesterImpl::testXmlTmcdbComponent", (LM_INFO, "Could not access config data"));
        return false;
    }
    return success;
}

CORBA::Boolean XmlConfigDataTesterImpl::testAmbTMCDBAccessorDelegation() {
    bool success = true;
    try {
        success = assertEquals(0.0, getElement("brightness")->getDoubleAttribute("min_value"));
    } catch (ControlExceptions::XmlParserErrorExImpl &ex) {
        ACS_LOG(LM_SOURCE_INFO, "XmlConfigDataTesterImpl::testXmlTmcdbComponent", (LM_INFO, "Could not access config data"));
        return false;
    }
    return success;
}


template<class T> bool XmlConfigDataTesterImpl::assertEquals(T expected, T obtained) {
    std::ostringstream ost;

    if (expected != obtained) {
        ost << "Assertion failed, expected = " << expected << "; obtained = " << obtained;
        ACS_LOG(LM_SOURCE_INFO, "XmlConfigDataTesterImpl::assertEquals",
            (LM_INFO, ost.str().c_str()));
        return false;
    }
    ost << "Fine, obtained = " << obtained;
    ACS_LOG(LM_SOURCE_INFO, "XmlConfigDataTesterImpl::assertEquals",
         (LM_INFO, ost.str().c_str()));
    return true;
}


/* --------------- [ MACI DLL support functions ] -----------------*/
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(XmlConfigDataTesterImpl)
/* ----------------------------------------------------------------*/
