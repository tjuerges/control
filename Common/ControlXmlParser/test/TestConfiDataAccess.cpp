#include <cppunit/extensions/HelperMacros.h>
#include <configDataAccess.h>
#include <string>
#include <fstream>

class ConfigDataAccessTestCase : public CPPUNIT_NS::TestFixture
{
	CPPUNIT_TEST_SUITE( ConfigDataAccessTestCase );
	CPPUNIT_TEST( test_goodxml );
	CPPUNIT_TEST_SUITE_END();

  public:
	void setUp();
	void tearDown();

  protected:
	void test_goodxml();
};

class File2String
{
   public:
   static std::string readFile(char *);
};

std::string  File2String::readFile(char *file) {
	std::ifstream inFile(file);
	std::string str;
        std::string buf;
	if( !inFile ) {
		std::cerr << "Couldn´t open input file" << file<<std::endl;
	}
        while(std::getline(inFile, buf)){
            str += buf;
            str += "\n";
        }
	return str;
}

CPPUNIT_TEST_SUITE_REGISTRATION( ConfigDataAccessTestCase );

void ConfigDataAccessTestCase::setUp()
{

}

void ConfigDataAccessTestCase::tearDown()
{
}

void ConfigDataAccessTestCase::test_goodxml() 
{
   std::string stringtmp;
   bool booltmp=false;
   long longtmp=0;
   double doubletmp=0.0;
   std::auto_ptr<const ControlXmlParser> data;
   std::auto_ptr<std::vector<ControlXmlParser> > dataArray;
   ConfigDataAccessor *datum=NULL;
   try{
   datum = new ConfigDataAccessor((File2String::readFile("goodxml.xml")), File2String::readFile("goodxml.xsd"));
   }catch(ControlExceptions::XmlParserErrorExImpl &ex){
    std::cout<<ex.toString();
    CPPUNIT_ASSERT_EQUAL(false,true);

  }

   CPPUNIT_ASSERT_NO_THROW(longtmp = (*datum).getLongAttribute("orderid"));
   CPPUNIT_ASSERT_EQUAL((long)889923, longtmp);


   CPPUNIT_ASSERT_NO_THROW(data = datum->getElement("orderperson"));
   CPPUNIT_ASSERT_NO_THROW(stringtmp = (*data).getStringData());
   CPPUNIT_ASSERT_EQUAL(std::string("John Smith"),stringtmp);

   CPPUNIT_ASSERT_NO_THROW(dataArray = datum->getElements("item"));
   CPPUNIT_ASSERT_NO_THROW(data = (*dataArray)[0].getElement("note"));
   CPPUNIT_ASSERT_NO_THROW(stringtmp = (*data).getStringData());
   CPPUNIT_ASSERT_EQUAL(std::string("Special Edition"),stringtmp);
   CPPUNIT_ASSERT_NO_THROW(data = (*dataArray)[0].getElement("price"));
   CPPUNIT_ASSERT_NO_THROW(stringtmp = (*data).getStringAttribute("type"));
   CPPUNIT_ASSERT_EQUAL(std::string("pound"),stringtmp);
   CPPUNIT_ASSERT_NO_THROW(stringtmp = (*data).getStringData());
   CPPUNIT_ASSERT_EQUAL(std::string("10.90"),stringtmp);


   CPPUNIT_ASSERT_NO_THROW(data = (*dataArray)[1].getElement("title"));
   CPPUNIT_ASSERT_NO_THROW(stringtmp = (*data).getStringData());
   CPPUNIT_ASSERT_EQUAL(std::string("Hide your heart"),stringtmp);
   CPPUNIT_ASSERT_NO_THROW(data = (*dataArray)[1].getElement("price"));
   CPPUNIT_ASSERT_NO_THROW(stringtmp = (*data).getStringAttribute("type"));
   CPPUNIT_ASSERT_EQUAL(std::string("dollar"),stringtmp);
   CPPUNIT_ASSERT_NO_THROW(stringtmp = (*data).getStringData());
   CPPUNIT_ASSERT_EQUAL(std::string("9.90"),stringtmp);

}


/*
 * Main function running the tests
 */
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>

int main( int argc, char* argv[] )
{
	// Create the event manager and test controller
	CPPUNIT_NS::TestResult controller;

	// Add a listener that colllects test result
	CPPUNIT_NS::TestResultCollector result;
	controller.addListener( &result );

	// Add a listener that print dots as test run.
	CPPUNIT_NS::BriefTestProgressListener progress;
	controller.addListener( &progress );

	// Add the top suite to the test runner
	CPPUNIT_NS::TestRunner runner;
	runner.addTest( CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest() );
	runner.run( controller );

	// Print test in a compiler compatible format.
	CPPUNIT_NS::CompilerOutputter outputter( &result, std::cerr );
	outputter.write();

	return result.wasSuccessful() ? 0 : 1;
}

