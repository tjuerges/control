s/Ran .* tests in .*/Ran x tests in xxx/
s/Ran 1 test in .*/Ran 1 test in xxx/
s/name service error resolving .*//
s/.* CosNaming.NotFound .*//
s/.* CONTROL_SYSTEM-Consumer addSubscription - ExecBlockEndedEvent//
s/.* CONTROL_SYSTEM-Consumer initCORBA - Created new channel.//
s/.* CONTROL_SYSTEM-Consumer addSubscription - ExecBlockEndedEvent//
s/.*T.* //
s/Line=.*/Line=<line>/
s/Process=.*/Proces=<process>/
s/Thread=.*/Thread=<thread>/
s/Host=.*/Host=<host>/
s/Type=.*/Type=<type>/
s/Code=.*/Code=<code>/
s/File=.*/File=<file>/
