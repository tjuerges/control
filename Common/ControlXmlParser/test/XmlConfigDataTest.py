#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#

import unittest

from Acspy.Clients.SimpleClient import PySimpleClient

class XMLConfigDataTest(unittest.TestCase):
    
    def setUp(self):
        self._client = PySimpleClient()
        self._simulator = self._client.getDefaultComponent("IDL:alma/ACSSim/Simulator:1.0")
        
    def tearDown(self):
        self._client.releaseComponent(self._simulator._get_name())

    def testXmlTmcdbComponent(self):
        tester = self._client.getComponent("XML_CONFIG_DATA_TESTER")
        self.assertTrue(tester.testXmlTmcdbComponent())
        self._client.releaseComponent(tester._get_name())

    def testPhonyXmlTmcdbComponent(self):
        tester = self._client.getComponent("XML_CONFIG_DATA_TESTER")
        self.assertTrue(tester.testPhonyXmlTmcdbComponent())
        self._client.releaseComponent(tester._get_name())

    def testXmlTmcdbComponentPhonyDelegation(self):
        tester = self._client.getComponent("XML_CONFIG_DATA_TESTER")
        self.assertTrue(tester.testXmlTmcdbComponentPhonyDelegation())
        self._client.releaseComponent(tester._get_name())
        
    def testAmbTMCDBAccessor(self):
        tester = self._client.getComponent("XML_CONFIG_DATA_TESTER")
        self.assertTrue(tester.testAmbTMCDBAccessor())
        self._client.releaseComponent(tester._get_name())

    def testAmbTMCDBAccessorDelegation(self):
        code = """import TmcdbErrTypeImpl
raise TmcdbErrTypeImpl.TmcdbNoSuchRowExImpl()"""
        self._simulator.setMethod("TMCDB", "getComponentConfigData", code, 0)
        tester = self._client.getComponent("XML_CONFIG_DATA_TESTER")
        self.assertTrue(tester.testAmbTMCDBAccessorDelegation())
        self._client.releaseComponent(tester._get_name())
        self._simulator.removeMethod("TMCDB", "getComponentConfigData")

    def testXmlTmcdbComponentNoConfiguration(self):
        tester = self._client.getComponent("XML_CONFIG_DATA_TESTER")
        self.assertFalse(tester.testXmlTmcdbComponentNoConfiguration())

def suite():
    suite = unittest.TestSuite()
    suite.addTest(XMLConfigDataTest("testXmlTmcdbComponent"))
    suite.addTest(XMLConfigDataTest("testPhonyXmlTmcdbComponent"))
    suite.addTest(XMLConfigDataTest("testXmlTmcdbComponentPhonyDelegation"))
    suite.addTest(XMLConfigDataTest("testAmbTMCDBAccessor"))
    suite.addTest(XMLConfigDataTest("testAmbTMCDBAccessorDelegation"))
    suite.addTest(XMLConfigDataTest("testXmlTmcdbComponentNoConfiguration"))
    return suite

if __name__ == '__main__':
    unittest.main(defaultTest='suite')
