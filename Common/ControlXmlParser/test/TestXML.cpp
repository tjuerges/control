#include <configDataAccess.h>
#include <string>
#include <fstream>


class File2String
{
   public:
   static std::string readFile(char *);
};

std::string  File2String::readFile(char *file) {
	std::ifstream inFile(file);
	std::string str;
        std::string buf;
	if( !inFile ) {
		std::cerr << "Couldn´t open input file" << file<<std::endl;
	}
        while(std::getline(inFile, buf)){
            str += buf;
            str += "\n";
        }
	return str;
}



int main( int argc, char* argv[] )
{
    if(argc!=3){
        std::cerr<<"Usage "<<argv[0]<<" file.xml file.xsd"<<endl;
        exit(-1);
    }
    std::string stringtmp;
    std::auto_ptr<const ControlXmlParser> data;
    std::auto_ptr<std::vector<ControlXmlParser> > dataArray;
    ConfigDataAccessor *datum;
    try{
        datum = new ConfigDataAccessor((File2String::readFile(argv[1])), File2String::readFile(argv[2]));
    }catch(ControlExceptions::XmlParserErrorExImpl &ex){
        std::cout<<ex.toString();
        exit(-1);
    }
    std::cout<<"OK"<<std::endl;
    exit(0);
}
    

