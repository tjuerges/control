#ifndef XMLCONFIGDATATESTERIMPL_H
#define XMLCONFIGDATATESTERIMPL_H
/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 *
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <acscomponentImpl.h>
#include <XmlConfigDataTesterS.h>
#include <ambTMCDBAccess.h>

/**
 * This is a "tester" component to test the classes in this module that
 * get XML configuration data either from files or from the TMCDB database.
 *
 * See idl/XmlConfigDataTester.idl for details.
 */
class XmlConfigDataTesterImpl : public virtual AmbTMCDBAccessor,
                public virtual acscomponent::ACSComponentImpl,
                public virtual POA_test::XmlConfigDataTester {

    public:

        XmlConfigDataTesterImpl(const ACE_CString& name,
            maci::ContainerServices* containerServices);

        virtual ~XmlConfigDataTesterImpl();
        /**
         * initialize the component lifeCycle
         * @param void
         * @return void
         * @exception acsErrTypeLifeCycle::LifeCycleExImpl - if there if a problem during initialization.
         */
        virtual void initialize();

        virtual void cleanUp();

        /**
         * Test the XmlTmcdbComponent class.
         *
         * See idl/XmlConfigDataTester.idl for details.
         */
        CORBA::Boolean testXmlTmcdbComponent();

        /**
         * Test the PhonyXmlTmcdbComponent class.
         *
         * See idl/XmlConfigDataTester.idl for details.
         */
        CORBA::Boolean testPhonyXmlTmcdbComponent();

        /**
         * Test the XmlTmcdbComponent delegation to the PhonyXmlTmcdbComponent.
         *
         * See idl/XmlConfigDataTester.idl for details.
         */
        CORBA::Boolean testXmlTmcdbComponentPhonyDelegation();

        /**
         * Test getting XML configuration data using the AmbTMCDBAccessor base class.
         *
         * See idl/XmlConfigDataTester.idl for details.
         */
        CORBA::Boolean testAmbTMCDBAccessor();

        CORBA::Boolean testAmbTMCDBAccessorDelegation();

        /**
         * Test what happens when the XmlTmcdbComponent is queried for a serial
         * number that doesn't have configuration.
         *
         * See idl/XmlConfigDataTester.idl for details.
         */
        CORBA::Boolean testXmlTmcdbComponentNoConfiguration();

    private:

        /**
         * Little utility template function to assert equality and log the expected and
         * obtained values.
         */
        template<class T> bool XmlConfigDataTesterImpl::assertEquals(T expected, T value);

        /**
         * ALMA C++ coding standards state copy operators should be disabled.
         */
        void operator=(const XmlConfigDataTesterImpl&);

};

#endif /*!XMLCONFIGDATATESTERIMPL_H*/

