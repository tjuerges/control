#ifndef CONFIG_DATA_ACCESSOR_H
#define CONFIG_DATA_ACCESSOR_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2004 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* ntroncos
*/

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <maciContainerServices.h>
#include <cdbErrType.h>
#include <ControlExceptions.h>
#include <controlSAXErrorHandler.h>
#include <controlXmlParser.h>
#include <xercesc/parsers/XercesDOMParser.hpp>

#include <memory>
#include <vector>
#include <string>

XERCES_CPP_NAMESPACE_USE
/**
   The Accessor class is used to get the instance XML data from the
   CDB. 
*/
class ConfigDataAccessor : public ControlXmlParser {
 public:
  /** 
   * The constructor handles obtaining the correct path for the CDB and 
   * retreiving the instance xml from the CDB.
   *
   * @param containerServices maci::ContainerServices* Pointer to container 
   * services for access to CDB
   * @exception ControlExceptions::CDBErrorExImpl Thrown whenever an error is 
   * encountered accessing or parsing the CDB.
   */
  ConfigDataAccessor( const std::string &xml, const std::string &xsd);

  virtual ~ConfigDataAccessor();

 private: 

  /**
   * This is a default error handler which is invoked whenever the parser
   * encounters an error.
   */
  ControlSAXErrorHandler errorHandler;

  /** 
   * This is the parser which holds the DOM model.  When the ConfigDataAccessor is
   * deleted all ControlXmlParsers referencing this parser are obsolete.
   */
  XercesDOMParser        parser;
};

#endif /*!AMB_CDB_ACCESS_H*/
