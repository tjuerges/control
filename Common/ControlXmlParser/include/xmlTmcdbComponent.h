#ifndef XML_TMCDB_COMPONENT_H
#define XML_TMCDB_COMPONENT_H
/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 *
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <acsContainerServices.h>
#include <TMCDBAccessIFC.h>
#include <phonyXmlTmcdbComponent.h>
#include <controlTmcdbComponentWrapper.h>

#include <string>

class XmlTmcdbComponent : ControlTmcdbComponentWrapper {
    public:
        XmlTmcdbComponent(maci::ContainerServices* containerServices, const char *tmcdbCompName);
        XmlTmcdbComponent(maci::ContainerServices* containerServices);
        virtual ~XmlTmcdbComponent();
        std::string getConfigXml(const std::string &deviceId);
        std::string getConfigXsd(const std::string &deviceId);

    private:
        maci::ContainerServices* cs_m;
        std::string tmcdbComponentName_m;
        maci::SmartPtr<TMCDB::Access> tmcdb_m;
        PhonyXmlTmcdbComponent* phonyXmlTmcdb_m;
        std::string currentDeviceId_m;
        bool tmdcbOk_m;
        TMCDB::AssemblyConfigXMLData_var currentXmlData_m;
};
#endif // XML_TMCDB_COMPONENT_H
