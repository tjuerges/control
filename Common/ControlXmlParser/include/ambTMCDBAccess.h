#ifndef AMB_TMCDB_ACCESS_H
#define AMB_TMCDB_ACCESS_H
/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2004 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <controlXmlParser.h>
#include <controlSAXErrorHandler.h>
#include <maciContainerServices.h>
#include <cdbErrType.h>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <ControlExceptions.h>
#include <memory>
#include <vector>

XERCES_CPP_NAMESPACE_USE

class AmbTMCDBAccessor : public ControlXmlParser {
    
    public:
        AmbTMCDBAccessor(maci::ContainerServices* containerServices, const char* tmcdbCompName);

        virtual ~AmbTMCDBAccessor();

    protected:
    
        DOMElement* parse(std::string xml, std::string xsd);
        DOMElement* parse(std::string xml);
        std::string getXMLFromCDB(maci::ContainerServices* containerServices);
        
        std::string compName_m;

        /**
         * This is the name of the file in the CDB/alma directory that is used.
         * This member variable is defined here to be backward compatible with
         * the OldAmbDeviceImpl class.
         */
        ACE_CString              cdbName_m;

    private:
        ControlSAXErrorHandler errorHandler_m;
        XercesDOMParser parser_m;
};

#endif /*! AMB_TMCDB_ACCESS_H */
