#ifndef CONTROL_XML_PARSER_H
#define CONTROL_XML_PARSER_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2004
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2004-12-02  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <maciContainerServices.h>
#include <cdbErrType.h>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <ControlExceptions.h>
#include <memory>
#include <vector>

XERCES_CPP_NAMESPACE_USE


/**
    The ControlXmlParser class provides a simple interface for accessing the CDB.
    It is based on the C++ xerces library and uses a DOM type parser.

    A static reference counting scheme is used to minimize the memory footprint
    of Xercies.  When the first ControlXmlParser is instanciated the Xercies
    platform initialize method is called, and the destructor is called when
    the reference count falls to zero.
*/
class ControlXmlParser {
 public:
  /**
     Constructor for instanciating from a raw DOM Element input.

     @param elementReference: const DOMElement* - The element to be further
     parsed by the ControlXmlParser.

     @note This method calls incrementInstance()
  */

  ControlXmlParser(const DOMElement*   elementReference);

  /**
     Copy Constructor
     The copy constructor is necessary becuse we use STL containers.

     @param src: const ControlXmlParser& - Source ControlXmlParser to be copied

     @note This method calls incrementInstance()
  */
  ControlXmlParser(const ControlXmlParser& src);

  /**
     Destructor

     @note This method calls decrementInstance()
  */
  virtual ~ControlXmlParser();




  /** @name Attribute accessor methods.
   */
  /*@{*/

  /**
     Accessor returns value of long attribute with given name.

     @param attributeName const char* - Name of attribute to be returned
     @return const long - Attribute value interpreted as long integer
     @exception ControlExceptions::XmlParserErrorExImpl - Thrown if the DOMElement is
     unable to be parsed, or the attribute name is not found.
   */
  virtual const long
    getLongAttribute(const char* attributeName) const;

  /**
     Accessor returns value of double attribute with given name.

     @param attributeName const char* - Name of attribute to be returned
     @return const double - Attribute value interpreted as double
     @exception ControlExceptions::XmlParserErrorExImpl - Thrown if the DOMElement is
     unable to be parsed, or the attribute name is not found.
   */
  virtual const double
    getDoubleAttribute(const char* attributeName) const;

  /**
     Accessor returns value of boolean attribute with given name.

     @param attributeName const char* - Name of attribute to be returned
     @return const bool - Attribute value interpreted as boolean
     @exception ControlExceptions::XmlParserErrorExImpl - Thrown if the DOMElement is
     unable to be parsed, or the attribute name is not found.
   */
  virtual const bool
    getBoolAttribute(const char* attributeName) const;

  /**   Accessor returns value of string attribute with given name.

     @param attributeName const char* - Name of attribute to be returned
     @return const string - Attribute value interpreted as boolean
     @exception ControlExceptions::XmlParserErrorExImpl - Thrown if the DOMElement is
     unable to be parsed, or the attribute name is not found.
   */
  virtual const std::string
    getStringAttribute(const char* attributeName) const;



  /**
    Accessor returns value of long attribute with given name.

    @param attributeName const ACE_CString& - Name of attribute to be returned
    @return const long - Attribute value interpreted as long integer
    @exception ControlExceptions::XmlParserErrorExImpl - Thrown if the DOMElement is
    unable to be parsed, or the attribute name is not found.
    @note This method calls ControlXmlParser::getLongAttribute(const char*)
   */
  virtual const long
    getLongAttribute(const ACE_CString& attributeName) const;

  /**
     Accessor returns value of double attribute with given name.

     @param attributeName const ACE_CString& - Name of attribute to be returned
     @return const double - Attribute value interpreted as double
     @exception ControlExceptions::XmlParserErrorExImpl - Thrown if the DOMElement is
     unable to be parsed, or the attribute name is not found.
     @note This method calls ControlXmlParser::getDoubleAttribute(const char*)
  */
  virtual const double
    getDoubleAttribute(const ACE_CString& attributeName) const;

  /**
     Accessor returns value of boolean attribute with given name.

     @param attributeName const ACE_CString& - Name of attribute to be returned
     @return const bool - Attribute value interpreted as boolean
     @exception ControlExceptions::XmlParserErrorExImpl - Thrown if the DOMElement is
     unable to be parsed, or the attribute name is not found.
     @note This method calls ControlXmlParser::getboolAttribute(const char*)
  */
  virtual const bool
    getBoolAttribute(const ACE_CString& attributeName) const;

  /**
     Accessor returns value of string attribute with given name.

     @param attributeName const ACE_CString& - Name of attribute to be returned
     @return const string - Attribute value interpreted as boolean
     @exception ControlExceptions::XmlParserErrorExImpl - Thrown if the DOMElement is
     unable to be parsed, or the attribute name is not found.
     @note This method calls ControlXmlParser::getboolAttribute(const char*)
  */
  virtual const std::string
    getStringAttribute(const ACE_CString& attributeName) const;

  /**
     Accessor returns value of the data within a given element.

     @param None
     @return const string - Data containes within the element.
     @exception ControlExceptions::XmlParserErrorExImpl - Thrown if the DOMElement data could not be retieved or transcoded.
  */
  virtual const std::string getStringData() const;
  /*@}*/

  /** @name Heirarchy navigation methods. */

  /*@{*/
  /**
     Method returns the FIRST sub-element with given name

     @param elementName const char* - Name of sub element to be returned
     @return std::auto_ptr<const ControlXmlParser> - A ControlXmlParser containing
     the DOM model of the sub-element.
     @exception ControlExceptions::XmlParserErrorExImpl - Thrown if the DOMElement is
     unable to be parsed, or the attribute name is not found.

   */
  virtual std::auto_ptr<const ControlXmlParser>
    getElement(const char* elementName) const;

  /**
     Method returns the FIRST sub-element with given name

     @param elementName const ACE_CString& - Name of attribute to be returned
     @return std::auto_ptr<const ControlXmlParser> - A ControlXmlParser containing
     the DOM model of the sub-element.
     @exception ControlExceptions::XmlParserErrorExImpl - Thrown if the DOMElement is
     unable to be parsed, or the attribute name is not found.
     @note This method calls ControlXmlParser::getElement(const char*)
   */
  virtual std::auto_ptr<const ControlXmlParser>
    getElement(const ACE_CString& elementName) const;

  /**
     Method returns ALL sub-elements with given name

     @param elementName const char* - Name of sub element to be returned
     @return std::auto_ptr<<std::vector<ControlXmlParser> > - A vector containing
     the one ControlXmlParser for each instance of \a elementName in the DOM model.
     @exception ControlExceptions::XmlParserErrorExImpl - Thrown if the DOMElement is
     unable to be parsed.

     @warning No exception is thrown if no elements are found, thus it is
     necessary to check for the 0 lenght in the returned vector.
   */
  virtual std::auto_ptr<std::vector<ControlXmlParser> >
    getElements(const char* elementName) const;


  /**
     Method returns ALL sub-elements with given name

     @param elementName const ACE_CString& - Name of sub element to be returned
     @return std::auto_ptr<<std::vector<ControlXmlParser> > - A vector containing
     the one ControlXmlParser for each instance of \a elementName in the DOM model.
     @exception ControlExceptions::XmlParserErrorExImpl - Thrown if the DOMElement is
     unable to be parsed.

     @warning No exception is thrown if no elements are found, thus it is
     necessary to check for the 0 lenght in the returned vector.
     @note This method calls ControlXmlParser::getElements(const char*)
   */
    virtual std::auto_ptr<std::vector<ControlXmlParser> >
    getElements(const ACE_CString& elementName) const;
  /*@}*/

 protected:
  /**
      Default Constructor
      @note This method calls incrementInstance()
  */
  ControlXmlParser();

  /**
   * This is the top level DOMElement for this instance, memory is allocated
   * and maintained external to the AmbCDBAccess class. */
  const DOMElement*   elementReference;

 private:
  /**
   * Method to increment the reference count, intializing Xerces resources if
   * necessary.
   */
  void incrementInstance();

  /**
   * Method to decrement reference count, freeing Xerces resources if
   * necessary.
   */
  void decrementInstance();

  /**
   * Static member keeping the reference count.  When this falls to zero
   * the Xercies resources are deallocated.
   */
  static unsigned long    xercesReferenceCount;

  /**
   * This keeps track of if the Xercies resources are initialized or not
   */
  static bool             xercesInitialized;

  /**
   * Mutex to synchronize access to xercesReferenceCount and xercesInitialized.
   */
  static pthread_mutex_t  xercesMutex;
};



#endif /*!CONTROL_XML_PARSER_H*/
