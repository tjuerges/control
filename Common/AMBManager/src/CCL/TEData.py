#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# tjuerges  Jul 13, 2008  created
#
#************************************************************************
#   NAME
#     TEData.py
#   SYNOPSIS
#     This class is used by the ambManager to effect TE related monitoring
#
#------------------------------------------------------------------------
#


import Control__POA


class TEData(Control__POA.TEData): 
    '''
    The TEData class is a data receptacle for asynchronous monitor/ command
    requests.  For examples of how to use it with the AmbManager class see
    the documentation for AmbManager.

    ACS provides classes to help manipulate the time from ACS units to
    the standard Unix units and back.  These can be loaded from Acspy.Common
    via:
    "from Acspy.Common.EpochHelper import *"
    "from Acspy.Common.TimeHelper import *"

    These classes provide conversion utilities between various methods of
    expressing time.  The python documentation is lacking and points to:
    the C++ documentation  which can be found at:
    http://www.eso.org/~gchiozzi/AlmaAcs/#OnlineDocumentation

    This class provides very bare bones utility for handling data from
    asynchronous calls.  Other classes which inherit from AMBMGR__POA.TEData
    and implement the putData method will probably be needed for
    special purpose applications.
    '''

    def __init__(self):
        '''
        Simple constructor, just does initialisation
        '''
        self.errorFlag = 0
        self.errorList = []
        self.dataList  = []
        self.myRef = self._this

    def __del__(self):
        pass

    def putData(self, dataPt):
        '''
        This function is used by the asynchrous command/monitor functions of
        AmbManager.  The user should NEVER execute this command directly.
        '''
        if(len(dataPt.ErrorString) != 0):
            self.errorFlag = 1
            self.errorList.append(len(self.dataList))
        self.dataList.append(dataPt)

    def getDataVector(self):
        '''
        This function returns the accumulated data as a tuple.  The data is
        returned in order based on the time of execution.
        '''
        dataVector = []
        for pt in self.dataList:
            dataVector.append(pt.data)
        return tuple(dataVector)

    def getTimeVector(self):
        '''
        This function returns a tuple containing all of the timestamps
        in the order they returned.

        PARAMETER: None
        RETURN: Tuple of ACS:Time timestamp values.
        '''
        timeVector = []
        for pt in self.dataList:
            timeVector.append(pt.timestamp)
        return tuple(timeVector)
