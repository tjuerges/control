#!/usr/bin/env python
# Copyright (C) 2001
# Associated Universities, Inc. Washington DC, USA.
#
# Produced for the ALMA project
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU Library General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for
# more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
# Correspondence concerning ALMA should be addressed as follows:
# Internet email: alma-sw-admin@nrao.edu
#
# $Id$
#


from Acspy.Clients.SimpleClient import PySimpleClient
from Acspy.Common.QoS import setObjectTimeout
import struct
from CCL.TEData import TEData
from CCL.Container import getComponent
from CCL.Container import getDynamicComponent
from CCL.logging import getLogger
import ControlExceptions
import CCL.Antenna
import CCL.AOSTiming


ex = "Exception:"


class AmbManagerError(Exception):
    '''
    General AmbManager Exceptions
    '''
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg


class AmbManager:
    '''
    The AmbManager class provides low level communication to the AMB Bus.
    This class uses the PySimpleClient method to communicate with the
    specified AmbManager component object.  An effort has been made to simplify
    the interface hiding much of the details of ACS from the user.

    Some General Notes:
    


    Notes on the TE interface:

      ACS provides classes to help manipulate the time from ACS units to
      the standard unix units and back.  These can be loaded from Acspy.Common
      via:
      "from Acspy.Common.EpochHelper import *"
      "from Acspy.Common.TimeHelper import *"

      These classes provide conversion utilities between various methods of
      expressing time.  The Python documentation is lacking and points to:
      the C++ documentation  which can be found at:
      http://www.eso.org/~gchiozzi/AlmaAcs/#OnlineDocumentation
      
      The TE interface provides three types of interactions:
         1) Synchronous Specified TE interaction: Using either the monitorTE
            or controlTE method with a targetTE specified will cause the
            request to be executed during the correct TE window (see caution
            below).

         2) Synchronous Next TE interaction: Using either the monitorTE or
            controlTE method without a targetTE specified (or targetTE = 0)
            will cause the request to be executed during the next monitor
            or command window respectively.

         3) Asynchronous TE interaction: Using either of the Asynch methods
           will schedule a request for the correct TE window (again see
           caution below), but will immediately return.  When the request
           completes the data will be recorded in the TEData object specified.
           Deleting a TEData object before all requests associated with it
           have completed will produce unspecified (and probably unwanted)
           results.

      CAUTION:  Any time will be accepted by this routine, and the system will
                attempt to execute it at that time.  A request for execution
                time t0 will be executed during the command window (first 24
                ms) of TE at t1 provided t1 - 24 ms < t0 <= t1 and will
                execute in the monitor widow provided t1 < t0 <= t1 + 24 ms.

    CAUTION:    An execution time which has been calculated on one computer
                (client) can be differ significantly from the "real" execution
                time of that computer on which the AmbManager component actually
                runs.  This can be the case when the client is not synchronised
                with the clock of the ARTM, i.e. no ArrayTime component is
                running on the client or no NTP client program is running on
                the client computer which uses ARTM as NTP server.

    Revision History:
    Version 1.1: Original version
    Version 1.1.4: Minor changes for robustness
    Version 1.1.4: 2004/06/17 Added temperature monitoring method
    Version 1.3: 2004/06/28 Ported to ACS3.1.1 major changes include:
      Modified output structure to use Tuples.
      Added exceptions for errors from AMB.
      Removed need to type AmbManager:<name> as constructor now just takes
      <name>
      Removed Synch from method invocations as it was unclear what this meant
         in the context of the overall system.  Name changes were only made
         by removing the "Synch" portion of the name.
    Version 1.5: 2004/07/27 Added to documentation and corrected small errors
    Version 1.15: 2008-07-14: Added access states. Moved to CCL.
***** Moved to CCL directory. *****    
    Version 1.2: 2008-11-07: Changed ambManager to AmbManager in the example
        code.
        Added comment about the fact that the AmbManager can now be shut down
        although clients are still using it.


    Release Notes for Version 1.2
    One of the largest changes in the new version is the use of exceptions to
    replace error codes.  In the examples below error handling is omitted for
    clarity here an example of the error handling is presented.

    NOTE: This is a very simple example, more complicated error handling will
    be supported in upcoming releases.

    EXAMPLE:
    >>> try:
    ...     (data, time) = mgr.monitor(1,2,0)
    ... except Exception, ex:
    ...     print 'Error attempting to monitor Node 2 on channel 1'
    ...     print 'Error at line %d of module %s' % (ex.errorTrace.lineNum, ex.errorTrace.routine)
    Error attempting to monitor Node 2 on channel 1
    Error at line 124 of module AmbManagerImpl::monitorTE


    Release Notes for Version 1.15 2008-07-14
    The AmbManager has been refactored.  It will now be started and stopped
    from the Antenna component.  Whenever clients are trying to retrieve an
    AmbManager reference, they should first bring the Antenna component up and
    to operational mode, and then check if Antenna.isAmbManagerRunning() returns
    false.  If false is returned, call Antenna.startAmbManager(), check that 
    Antenna.isAmbManagerRunning() returns true and then get a non-sticky(!!!)
    reference.  In the case that true is returned, somebody did already start
    the AMBManager and no further action is necessary.  But be aware that the
    AmbManager access can be disabled at any time!

    Additionally the AmbManager now allows to control the access mode. Initially
    the AmbManager in every Antenna will start in read-write mode, i.e. monitor
    and control requests can be executed. As soon as a request does not match
    with the current operations mode, a ControlExceptions::InvalidRequestEx
    will be thrown.
    '''

    
    def __init__(self, antenna = None):
        '''
        This is the simple constructor, it takes as the only argument
        the name of the antenna name (or DMC or ARTM).  Only objects
        defined in the CDB can be used here.  This routine initialises
        an PySimpleClient unless one has already been defined.

        Example:
        >>> from CCL.AmbManager import *
        >>> mgr = AmbManager("DV01")

        Raises:
            AmbManagerError exception if manager not found or component 
            name not found.
        '''
        self.__mgrRef = 0
 
        if antenna == None:
            self.__mgrName = None
            msg = "You must provide an antenna name or something equivalent."
            raise AmbManagerError(msg)
        self.__logger = getLogger()
        self.__client = PySimpleClient.getInstance()

        try:
            self.__getManagerReference(antenna)
        except AmbManagerError, ex:
            # Probably the case that the manager is not yet running
            # try to start it.
            self.__activateManager(antenna)
            self.__getManagerReference(antenna)
            
        if self.__mgrRef == None:
            msg = 'Component %s not found.' % (antenna)
            raise AmbManagerError(msg)

    def __getManagerReference(self, antenna):
        '''
        This method actually acquires the manager reference
        '''

        self.__mgrName = "CONTROL/" + antenna + "/AmbManager"
        try:
            self.__mgrRef = self.__client.getComponentNonSticky(self.__mgrName)
            setObjectTimeout(self.__mgrRef, 5000)
        except:
            msg = 'Manager reference failed for %s.' % (self.__mgrName)
            raise AmbManagerError(msg)

    def __activateManager(self, antenna):

        aosControllerList = ['DMC', 'DMC2', 'DMC3', 'DMC4', 'LMC', 'AOSTiming', 'ARTM']
        try:
            if aosControllerList.count(antenna) > 0 :
                print "Attempting to use AOSTiming."
                cntlRef = CCL.AOSTiming.AOSTiming()
                tmpAntenna = antenna
                if tmpAntenna == "AOSTiming":
                    tmpAntenna = "ARTM"
                if not cntlRef.isAmbManagerRunning(tmpAntenna):
                    cntlRef.startAmbManager(tmpAntenna)
            else:
                cntlRef = CCL.Antenna.Antenna(antenna)
                if not cntlRef.isAmbManagerRunning():
                    cntlRef.startAmbManager()
        except Exception, ex:
            msg = 'Unable to acquire reference to Antenna'
            raise AmbManagerError(msg)
        
    def __del__(self):
        '''
        Simple destructor used to release the manager.  Most importantly
        this cleanly terminates the connection to the ACS Manager by
        disconnecting the PySimpleClient

        EXAMPLE:
        >>> from CCL.AmbManager import *
        >>> mgr = AmbManager("DV01")
        >>> del(mgr)
        2004-07-01T16:49:08.000 Python Client disconnect - Shutdown called for client
        '''
        if self.__mgrRef != 0:
            self.__mgrRef = 0
        self.__client.disconnect()

    def commandingEnabled(self):
        return self.__mgrRef.commandingEnabled()

    def monitoringEnabled(self):
        return self.__mgrRef.monitoringEnabled()

    def disableAccess(self):
        self.__mgrRef.disableAccess()

    def enableReadOnlyAccess(self):
        self.__mgrRef.enableReadOnlyAccess()

    def enableReadWriteAccess(self):
        self.__mgrRef.enableReadWriteAccess()

    def command(self, channel, nodeAddress, RCA, data):
        '''
        Control method for asynchronous manipulation of a control point.
        PARAMETERS:
        channel : AMBSystem::AmbChannel - Channel target node is connected to.
        nodeAddress : AMBSystem::AmbNodeAddr - Base address of target node.
        RCA : AMBSystem::AmbRelativeAddr - Relative CAN address of target
             control point.
        data :

        RETURNS:
        (timestamp)
        timestamp : ACS::Time - The timestamp of the monitor points execution.

        RAISES:
        ControlExceptions::CAMBErrorEx

        EXAMPLE:
        >>> from CCL.AmbManager import *
        >>> import struct
        >>> mgr = AmbManager("DA41")
        >>> timestamp = command(0,1,15,struct.pack("4B",42,36,43,54))
        >>> print timestamp
        >>> 
        '''
        return self.__mgrRef.command(channel, nodeAddress, RCA, data)
    
    def commandTE(self, channel, nodeAddress, RCA, data, time = 0):
        '''
        Command method for synchronous command over AMB to a
        specific CAN address.
        
        PARAMETERS:
        channel : AMBSystem::AmbChannel - Channel target node is connected to.
        nodeAddress : AMBSystem::AmbNodeAddr - Base address of target node.
        RCA : AMBSystem::AmbRelativeAddr - Relative CAN address of target
            monitor point.
        data: AMBMGR::ambData - Data to be sent over can bus.
        Time: ACS::Time- Time to execute the command.

        RETURNS:
        (timestamp)
        timestamp: ACS::Time - The timestamp of the monitor points execution

        RAISES:
        ControlExceptions::CAMBErrorEx

        EXAMPLE:
        '''
        if time == 0:
            return self.__mgrRef.commandNextTE(channel, nodeAddress, RCA, data)
        else:
            return self.__mgrRef.commandTE(channel, nodeAddress, RCA, data,
                                           time)
        
    def commandTEAsynch(self, channel, nodeAddress, RCA, data, time, teData):
        '''
        Command method for asynchronous command of at
        a specific CAN address at a specific time.  This method uses a
        callback to the teData object to return the status of the request.

        PARAMETERS:
        channel : AMBSystem::AmbChannel - Channel target node is connected to.
        nodeAddress : AMBSystem::AmbNodeAddr - Base address of target node.
        RCA : AMBSystem::AmbRelativeAddr - Relative CAN address of target
          monitor point.
        data: AMBMGR::ambData - Data to be sent over can bus.\
        time : ACS:Time - Target time of execution, if no value is specified
          the monitor is executed during the next monitor window
        teData : TEData object - The callback object to use when monitor
          completes.

        RETURNS: NONE

        EXAMPLE:
        '''
        self.__mgrRef.commandTEAsynch(channel, nodeAddress, RCA, data,
                                      time, teData.myRef())

    def findSN(self, channel, nodeAddress):
        '''
        This method the serial number of the specified node on the requested
        channel.
        PARAMETERS:
        channel : AMBSystem::AmbChannel - Channel to be reset
        nodeAddress : AMBSystem::AmbNodeAddr - the node to be queried.

        RETURNS:
        (SerialNumber, timestamp)
        SerialNumber: 64 Bit serial number, this has been "unpacked"
        timestamp : ACS::Time - The timestamp of the monitor points execution.

        RAISES:
        ControlExceptions::CAMBErrorEx

        EXAMPLE:
        >>> from CCL.AmbManager import *
        >>> mgr = AmbManager("ARTM")
        >>> (sn, time) = mgr.findSN(1,17)
        >>> print 'At %d node 17 reported S/N 0x%x' % (time,sn)
        At 133080039678937930 node 17 reported S/N 0x3030303030303041
        '''
        (sn, timestamp) = self.__mgrRef.findSN(channel, nodeAddress)
        (sn,) = struct.unpack("!Q", sn)
        return (sn, timestamp)
            
    def getNodes(self, channel):
        '''
        This method executes a broadcast inquiry and returns all nodes on the
        specified channel.

        PARAMETERS:
        channel : AMBSystem::AmbChannel - Channel to be queried

        RETURNS:
        (Nodes, timestamp)
        Nodes: AMBSystem::AmbNodeSeq Sequence of all nodes responding to
        broadcast request.
        timestamp: ACS::Time The timestamp of the request execution

        RAISES:
        ControlExceptions::CAMBErrorEx

        EXAMPLE:
        >>> from CCL.AmbManager import *
        >>> mgr = AmbManager("DMC")
        >>> (nodes, time) = mgr.getNodes(1)
        >>> print "%d Nodes found (Time: %d)" % (len(nodes),time)
        18 Nodes found (Time: 133079930115020180)
        >>> for node in nodes:
        ...     print 'Node 0x%x has serial number 0x%x' % (node.node, node.sn)
        ...
        Node 0x0 has serial number 0x3030303030303030
        Node 0x1 has serial number 0x3030303030303031
        Node 0x2 has serial number 0x3030303030303032
        Node 0x3 has serial number 0x3030303030303033
        Node 0x4 has serial number 0x3030303030303034
        Node 0x5 has serial number 0x3030303030303035
        Node 0x6 has serial number 0x3030303030303036
        Node 0x7 has serial number 0x3030303030303037
        Node 0x8 has serial number 0x3030303030303038
        Node 0x9 has serial number 0x3030303030303039
        Node 0xa has serial number 0x303030303030303a
        Node 0xb has serial number 0x303030303030303b
        Node 0xc has serial number 0x303030303030303c
        Node 0xd has serial number 0x303030303030303d
        Node 0xe has serial number 0x303030303030303e
        Node 0xf has serial number 0x303030303030303f
        Node 0x10 has serial number 0x3030303030303040
        Node 0x11 has serial number 0x3030303030303041
        '''
        (nodes, time) = self.__mgrRef.getNodes(channel)
        for node in nodes:
            (node.sn,) = struct.unpack("!Q", node.sn)
        return (nodes, time)

    
    def getAllNodes(self):
        '''
        This method executes a broadcast inquiry and returns all nodes on
        all the channels.

        RETURNS:
        (nodes, channels)
        nodes: AMBSystem::AmbNodeSeq Sequence of all nodes responding to
        broadcast request.
        channels: a sequence of integers, with the same length as the
        nodes sequence, that indicates which channel the corresponding
        node is on.

        RAISES:
        ControlExceptions::CAMBErrorEx

        EXAMPLE:
        >>> import CCL.AmbManager
        >>> mgr = CCL.AmbManager.AmbManager("DV01")
        >>> (nodes, channels) = mgr.getAllNodes()
        >>> for i in range(len(nodes)):
        ...     print 'Node 0x%x is on channel %d' \
        ...            % (nodes[i].node, channels[i])
        Node 0x0 is on channel 0
        Node 0x1 is on channel 1
        Node 0x2 is on channel 1
        '''
        (numChan, ts) = self.getNumberOfChannels();
        allNodes = [];
        channels = [];
        for channel in range(numChan):
            (nodes, time) = self.getNodes(channel)
            for i in range(len(nodes)):
                 allNodes.append(nodes[i])
                 channels.append(channel)
        return (allNodes, channels)

    def getNumberOfChannels(self):
        '''
        This method returns the number of channels defined on the AMB.

        PARAMETERS:
        None

        RETURNS:
        (NoChannels, timestamp)
        NoChannels: AMBSystem::AmbChannel The number of channels
        timestamp : ACS::Time - The timestamp of the request execution.

        RAISES:
        ControlExceptions::CAMBErrorEx

        EXAMPLE:
        >>> from CCL.AmbManager import *
        >>> mgr = AmbManager("DA41")
        >>> (noChan, time) = mgr.getNumberOfChannels()
        >>> print "Found %d channels (Time: %d)" % (noChan,time)
        Found 2 channels (Time: 133079928454074010)
        '''
        return self.__mgrRef.getNumberOfChannels()
    
    def monitor(self, channel, nodeAddress, RCA):
        '''
        Monitor method for synchronous retrieval of monitor data from a
        specific CAN address.
        
        PARAMETERS:
        channel : AMBSystem::AmbChannel - Channel target node is connected to.
        nodeAddress : AMBSystem::AmbNodeAddr - Base address of target node.
        RCA : AMBSystem::AmbRelativeAddr - Relative CAN address of target
            monitor point.

        RETURNS:
        (AmbData, timestamp)
        AmbData:  The data returned from the monitor point
        timestamp: ACS::Time - The timestamp of the monitor points execution

        RAISES:
        ControlExceptions::CAMBErrorEx

        EXAMPLE:
        >>> from CCL.AmbManager import *
        >>> import struct
        >>> mgr = AmbManager("ARTM")
        >>> (data, time) = mgr.monitor(1,17,0)
        >>> print len(data)
        8
        >>asBytes = struct.unpack('8B',data)
        >>> print asBytes
        (48, 48, 48, 48, 48, 48, 48, 65)
        >>> (asLongLongInt,) = struct.unpack('!Q',data)
        >>> print asLongLongInt
        3472328296227680321
        '''

        return self.__mgrRef.monitor(channel, nodeAddress, RCA)

    def monitorTE(self, channel, nodeAddress, RCA, time = 0):
        '''
        Monitor method for synchronous retrieval of monitor data from
        a specific CAN address at a specific time.

        PARAMETERS:
        channel : AMBSystem::AmbChannel - Channel target node is connected to.
        nodeAddress : AMBSystem::AmbNodeAddr - Base address of target node.
        RCA : AMBSystem::AmbRelativeAddr - Relative CAN address of target
          monitor point.
        time : ACS:Time - Target time of execution, if no value is specified
          the monitor is executed during the next monitor window.

        RETURNS:
        (AmbData, timestamp)
        AmbData:  The data returned from the monitor point
        timestamp: ACS::Time - The timestamp of the monitor points execution

        EXAMPLE:
        >>> from CCL.AmbManager import *
        >>> import struct
        >>> mgr = AmbManager("DMC")
        >>> // Start off by finding the last TE we passed
        >>> import time                           // Native Python time support
        >>> from Acspy.Common.TimeHelper import * // ACS time support
        >>> timeUtil = TimeUtil()
        >>>
        >>> // Create an epoch object containing the current time
        >>> epoch = timeUtil.py2epoch(time.time())
        >>>
        >>> // Find the last TE
        >>> epoch.value -= (epoch.value % 480000) 
        >>>
        >>> // Now set our target TE in the monitor window of the fifth te
        >>> targetTE = epoch.value + (480000 * 5)  + 240000;
        >>> (data, timestamp) = mgr.monitorTE(1, 17, 0x30003, targetTE)
        
        
        '''
        if time == 0:
            return self.__mgrRef.monitorNextTE(channel, nodeAddress, RCA)
        else:
            return self.__mgrRef.monitorTE(channel, nodeAddress, RCA, time)

    def monitorTEAsynch(self, channel, nodeAddress, RCA, time, teData):
        '''
        Monitor method for asynchronous retrieval of monitor data from
        a specific CAN address at a specific time.  This method uses a
        callback to the teData object to return the data.

        PARAMETERS:
        channel : AMBSystem::AmbChannel - Channel target node is connected to.
        nodeAddress : AMBSystem::AmbNodeAddr - Base address of target node.
        RCA : AMBSystem::AmbRelativeAddr - Relative CAN address of target
          monitor point.
        time : ACS:Time - Target time of execution, if no value is specified
          the monitor is executed during the next monitor window
        teData : TEData object - The callback object to use when monitor
          completes.

        RETURNS: NONE

        EXAMPLE:
        >>> from CCL.AmbManager import *
        >>> import struct
        >>> from Acspy.Common.TimeHelper import * // ACS time support
        >>> mgr = AmbManager("DV01")
        >>> teData = TEData()
        >>> timeUtil = TimeUtil()
        >>>
        >>> //Get the current time in ACS units
        >>> epoch = getTimeStamp()

        >>> // Find the last TE
        >>> epoch.value -= (epoch.value % 480000) 

        >>> // Now set our target TE in the monitor window of 10 TEs starting
        >>> // 5 TEs in the future
        >>> for x in range (5,16):
        ...    targetTE = epoch.value + (480000 * x)  + 240000;
        ...    mgr.monitorTEAsynch(1, 17, 0x30003, targetTE, teData);
        >>> // Wait for request to execute
        >>> // teData now contains the return value.
        >>> 
        '''
        self.__mgrRef.monitorTEAsynch(channel, nodeAddress, RCA,
                                      time, teData.myRef())
    
    def reset(self, channel):
        '''
        This sends a 1.5 ms reset pulse on the reset line for each channel.
        PARAMETERS:
        channel: AMBSystem::AmbChannel - Channel to be reset.

        RETURN:
        timestamp:
        timestamp: ACS::Time - The timestamp of the monitor points execution

        RAISES:
        ControlExceptions::CAMBErrorEx

        EXAMPLE
        >>> from CCL.AmbManager import *
        >>> mgr = AmbManager("DA41")
        >>> time = mgr.reset(0)
        '''
        return self.__mgrRef.reset(channel)
        
    def flush(self, channel, flushTime = 0):
        '''
        This sends a flush command to the AmbManager on channel "channel".
        PARAMETERS:
        channel: AMBSystem::AmbChannel - Channel to be flushed.
        flushTime: ACS::Time - time stamp in the future when the flush shall
            be executed.

        RETURN:
        timestamp:
        timestamp: ACS::Time - The time stamp of the flush execution.

        RAISES:
        ControlExceptions::CAMBErrorEx

        EXAMPLE
        >>> from CCL.AmbManager import *
        >>> mgr = AmbManager("DA41")
        >>> time = mgr.flush(0) # Flushes at the next TE.
        >>> time = mgr.flush(0, myTimeStamp) # Flushes at myTimeStamp.
        '''
        return self.__mgrRef.flush(channel, flushTime)
        
    def getTemperature(self, channel, nodeAddress):
        '''
        Method to monitor the temperature returned by an AMBSI.  This method
        takes care of any byte ordering issues which need to be resolved.  The
        full range of the DS18S20 is used to provide maximum resolution.

        PARAMETERS:
        channel: AMBSystem::AmbChannel - Channel target node is connected to.
        nodeAddress : AMBSystem::AmbNodeAddr - Base address of target node.

        RETURNS:
        (temperature, timestamp)
        temperature: The reported temperature in degrees C.
        timestamp: ACS::Time - The timestamp of the monitor points execution

        RAISES:
        ControlExceptions::CAMBErrorEx

        EXAMPLE:
        >>> from CCL.AmbManager import *
        >>> (temp, time) = mgr.getTemperature(1,17)
        >>> print 'At %d Node 17 reported a temperature of %d C' %(time,temp)
        At 133079963790321910 Node 17 reported a temperature of 18 C
        '''

        try:
            (resp, time) = self.__mgrRef.monitor(channel, nodeAddress, 0x30003)
        except:
            # Error getting the temperature from ambNode, continue error trace
            print error
            
        byte = struct.unpack("BBBB", resp)
        if byte[0] == 0xFF or byte[0] == 0x00:
            # Byte Major Order
            (temp, cntRmn, cntPC) = struct.unpack(">hBB", resp)
        else:
            # Byte Minor Order or degenerate case
            (temp, cntRmn, cntPC) = struct.unpack("<hBB", resp)
        # Here we calculate the temperature as discussed in DS18S20 docs.
        resp = (temp >> 1) - 0.25 + ((cntPC - cntRmn) / float(cntPC))
        return (resp, time)
