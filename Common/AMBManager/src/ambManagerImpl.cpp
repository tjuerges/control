/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2003
 *
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 *
 * who       when      what
 * --------  --------  ----------------------------------------------
 * jkern  2003-12-19  created
 */

/************************************************************************
 *   This is the AmbManager class, this provides an interface which
 *   can be used to control the AMB.
 *------------------------------------------------------------------------
 */


#include "ambManagerImpl.h"
#include <pthread.h>
#include <sstream>
#include <iostream>

// For the mutex which protects all access related methods.
#include <Guard_T.h>


AmbManagerImpl::AmbManagerImpl(const ACE_CString& name,
    maci::ContainerServices* cs):
    ACSComponentImpl(name, cs),
    interface_mp(0),
    readAccess(true),
    writeAccess(true),
    containerService(cs)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    monitorThreadName = name.c_str();
    monitorThreadName.append("MonitorThread");
}

AmbManagerImpl::~AmbManagerImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    destroyAmbInterface();
}

/* ----------------------- [ Lifecycle Interface ] ------------------ */
void AmbManagerImpl::initialize()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        interface_mp = AmbInterface::getInstance();
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }

    // Initialize the monitor thread
    AmbManagerImpl* selfPtr(this);
    containerService->getThreadManager()->create<
        AmbManagerThread, AmbManagerImpl* >(monitorThreadName.c_str(), selfPtr);

    getContainerServices()->getThreadManager()->resume(
        monitorThreadName.c_str());
}

void AmbManagerImpl::cleanUp(void)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    destroyAmbInterface();

    containerService->getThreadManager()->suspend(monitorThreadName.c_str());
    containerService->getThreadManager()->stop(monitorThreadName.c_str());

    ACSComponentImpl::cleanUp();
}

void AmbManagerImpl::aboutToAbort(void)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    destroyAmbInterface();
    ACSComponentImpl::aboutToAbort();
}

void AmbManagerImpl::destroyAmbInterface()
{
    if(interface_mp != 0)
    {
        interface_mp->deleteInstance();
        interface_mp = 0;
    }
}

/* ----------------------- [ Implementation ] ------------------------- */
CORBA::Boolean AmbManagerImpl::commandingEnabled()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    return writeAccess;
}

CORBA::Boolean AmbManagerImpl::monitoringEnabled()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    return readAccess;
}

void AmbManagerImpl::disableAccess()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    ACE_Guard< ACE_Mutex > guard(accessMutex_m);
    readAccess = false;
    writeAccess = false;
}

void AmbManagerImpl::enableReadOnlyAccess()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    ACE_Guard< ACE_Mutex > guard(accessMutex_m);
    readAccess = true;
    writeAccess = false;
}

void AmbManagerImpl::enableReadWriteAccess()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    ACE_Guard< ACE_Mutex > guard(accessMutex_m);
    readAccess = true;
    writeAccess = true;
}

AmbChannel AmbManagerImpl::getNumberOfChannels(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(readAccess == false)
    {
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Read access is disabled.");
        ex.log();
        throw ex.getInvalidRequestEx();
    }

    /* Synchronous call to get the number of channels */
    AmbCompletion_t* completion(new AmbCompletion_t);
    AmbMessage_t message;

    AmbDataLength_t dataLength;
    AmbDataMem_t data[8];
    sem_t synchLock;
    AmbErrorCode_t status(AMBERR_PENDING);
    sem_init(&synchLock, 0, 0);

    /* Build the completion block */
    completion->dataLength_p = &dataLength;
    completion->data_p = data;
    completion->channel_p = 0;
    completion->address_p = 0;
    completion->timestamp_p = &timestamp;
    completion->status_p = &status;
    completion->synchLock_p = &synchLock;
    completion->contLock_p = 0;
    completion->type_p = 0;

    /* Build the message block */
    message.requestType = AMB_GET_NO_CHANNELS;
    message.channel = 0;
    message.address = 0;
    message.dataLen = 0;
    message.completion_p = completion;
    message.targetTE = 0;

    /* Send the message and wait for a return */
    try
    {
        interface_mp->sendMessage(message);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        sem_destroy(&synchLock);
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getCAMBErrorEx();
    }

    sem_wait(&synchLock);
    sem_destroy(&synchLock);

    switch(status)
    {
        case AMBERR_NOERR:
            break;
        default:
            ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            std::ostringstream msg;
            msg << "ErrorCode = "
                << status;
            ex.addData("Detail", msg.str().c_str());
            ex.log();
            throw ex.getCAMBErrorEx();
    }

    if(dataLength != sizeof(AmbChannel))
    {
        std::ostringstream message;
        message << "Number of channels should contain "
            << sizeof(AmbChannel)
            << " bytes but "
            << dataLength
            << " were returned."
            << std::endl;

        ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", message.str().c_str());
        ex.log();
        throw ex.getCAMBErrorEx();
    }

    return(*((AmbChannel*)data));
}

void AmbManagerImpl::reset(AMBSystem::AmbChannel channel, ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(writeAccess == false)
    {
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Write access is disabled.");
        ex.log();
        throw ex.getInvalidRequestEx();
    }

    /* Synchronous call to reset a channel */
    AmbCompletion_t* completion(new AmbCompletion_t);
    AmbMessage_t message;

    sem_t synchLock;
    AmbErrorCode_t status(AMBERR_PENDING);
    sem_init(&synchLock, 0, 0);

    /* Build the completion block */
    completion->dataLength_p = 0;
    completion->data_p = 0;
    completion->channel_p = 0;
    completion->address_p = 0;
    completion->timestamp_p = &timestamp;
    completion->status_p = &status;
    completion->synchLock_p = &synchLock;
    completion->contLock_p = 0;
    completion->type_p = 0;

    /* Build the message block */
    message.requestType = AMB_RESET;
    message.channel = channel;
    message.address = 0;
    message.dataLen = 0;
    message.completion_p = completion;
    message.targetTE = 0;

    /* Send the message and wait for a return */
    try
    {
        interface_mp->sendMessage(message);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        sem_destroy(&synchLock);
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getCAMBErrorEx();
    }

    sem_wait(&synchLock);
    sem_destroy(&synchLock);

    switch(status)
    {
        case AMBERR_NOERR:
            break;
        default:
            ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            std::ostringstream msg;
            msg << "ErrorCode = "
                << status;
            ex.addData("Detail", msg.str().c_str());
            ex.log();
            throw ex.getCAMBErrorEx();
    }
}

AmbSerialNumber* AmbManagerImpl::findSN(AmbChannel channel,
    AmbNodeAddr nodeAddress, ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(readAccess == false)
    {
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Read access is disabled.");
        throw ex.getInvalidRequestEx();
    }

    AmbSerialNumber_var rv(new AmbSerialNumber);
    unsigned char tmpSN[8];

    try
    {
        interface_mp->findSN(tmpSN, channel, nodeAddress, timestamp);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getCAMBErrorEx();
    }

    /* Copy the serial number into the return value */
    rv->length(8);
    for(unsigned short idx(0U); idx < 8U; ++idx)
    {
        rv[idx] = tmpSN[idx];
    }
    return rv._retn();
}

void AmbManagerImpl::flush(AmbChannel channel, ACS::Time flushTime,
    ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(writeAccess == false)
    {
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Write access is disabled.");
        ex.log();
        throw ex.getInvalidRequestEx();
    }

    try
    {
        interface_mp->flush(channel, flushTime, timestamp);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getCAMBErrorEx();
    }

    return;
}

AmbNodeSeq* AmbManagerImpl::getNodes(AmbChannel channel,
    ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(readAccess == false)
    {
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Read access is disabled.");
        ex.log();
        throw ex.getInvalidRequestEx();
    }

    /* Synchronous call to get the nodes on a given channel */
    AmbNodeSeq_var rv(new AmbNodeSeq);
    unsigned short numNodes(0U);

    AmbCompletion_t* completion(new AmbCompletion_t);
    AmbMessage_t message;

    AmbChannel channel_resp;
    AmbAddr node;
    AmbDataLength_t dataLength;
    AmbDataMem_t data[8];
    sem_t synchLock;
    sem_t contLock;
    AmbErrorCode_t status(AMBERR_PENDING);
    sem_init(&synchLock, 0, 0);
    sem_init(&contLock, 0, 1);

    /* Build the completion block */
    completion->dataLength_p = &dataLength;
    completion->data_p = data;
    completion->channel_p = &channel_resp;
    completion->address_p = &node;
    completion->timestamp_p = &timestamp;
    completion->status_p = &status;
    completion->synchLock_p = &synchLock;
    completion->contLock_p = &contLock;
    completion->type_p = 0;

    /* Build the message block */
    message.requestType = AMB_GET_NODES;
    message.channel = channel;
    message.address = 0;
    message.dataLen = 0;
    message.completion_p = completion;
    message.targetTE = 0;

    /* Initialize the return with zero length */
    rv->length(0);

    /* Send the message and wait for a return */
    try
    {
        interface_mp->sendMessage(message);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        sem_destroy(&synchLock);
        sem_destroy(&contLock);
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getCAMBErrorEx();
    }
    sem_wait(&synchLock);

    while(status == AMBERR_CONTINUE)
    {
        AmbNode tmpNode;

        tmpNode.channel = channel_resp;
        tmpNode.node = node;
        if(dataLength != 8)
        {
            ACS_SHORT_LOG(
                (LM_ERROR, "Node Reported incorrect length Serial Number"));
        }
        else
        {
            tmpNode.sn.length(8);
            for(unsigned short idx(0U); idx < 8U; ++idx)
            {
                tmpNode.sn[idx] = data[idx];
            }

            rv->length(numNodes + 1U);
            (*rv)[numNodes] = tmpNode;
            ++numNodes;
        }
        sem_post(&contLock);
        sem_wait(&synchLock);
    }

    sem_destroy(&synchLock);
    sem_destroy(&contLock);

    /* Check For errors */
    switch(status)
    {
        case AMBERR_NOERR:
            break;
        default:
            ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            std::ostringstream msg;
            msg << "ErrorCode = "
                << status;
            ex.addData("Detail", msg.str().c_str());
            ex.log();
            throw ex.getCAMBErrorEx();
    }
    return rv._retn();
}

/* Because IDL does not support optional or defaulted parameters, these are
 seperate calls */
Control::AmbData* AmbManagerImpl::monitor(AMBSystem::AmbChannel channel,
    AMBSystem::AmbNodeAddr nodeAddress, AMBSystem::AmbRelativeAddr RCA,
    ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(readAccess == false)
    {
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Read access is disabled.");
        ex.log();
        throw ex.getInvalidRequestEx();
    }

    try
    {
        return monitorTE(channel, nodeAddress, RCA, timestamp, 0);
    }
    catch(const ControlExceptions::CAMBErrorEx& ex)
    {
        ControlExceptions::CAMBErrorExImpl traceEx(ex, __FILE__, __LINE__,
            "AmbManagerImpl::monitor");
        throw traceEx.getCAMBErrorEx();
    }
}

Control::AmbData* AmbManagerImpl::monitorTE(AMBSystem::AmbChannel channel,
    AMBSystem::AmbNodeAddr nodeAddress, AMBSystem::AmbRelativeAddr RCA,
    ACS::Time& timestamp, ACS::Time te)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(readAccess == false)
    {
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Read access is disabled.");
        ex.log();
        throw ex.getInvalidRequestEx();
    }

    /* Synchronous call to get a monitor point */

    Control::AmbData_var rv(new Control::AmbData);

    AmbCompletion_t* completion(new AmbCompletion_t);
    std::memset(completion, 0, sizeof(AmbCompletion_t));

    AmbMessage_t message;

    AmbDataLength_t dataLength;
    AmbDataMem_t data[8];
    sem_t synchLock;
    AmbErrorCode_t status(AMBERR_PENDING);

    sem_init(&synchLock, 0, 0);
    completion->data_p = data;
    completion->timestamp_p = &timestamp;
    completion->status_p = &status;
    completion->synchLock_p = &synchLock;
    completion->dataLength_p = &dataLength;

    /* Build the message block */
    message.requestType = AMB_MONITOR;
    message.channel = channel;
    message.dataLen = 0;
    message.completion_p = completion;
    message.targetTE = te;

    /* Send the message and wait for a return */
    try
    {
        message.address = createAMBAddr(nodeAddress, RCA);
        interface_mp->sendMessage(message);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        sem_destroy(&synchLock);
        delete completion;
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            "AmbManagerImpl::monitorTE").getCAMBErrorEx();
    }

    /* Wait for the results */
    sem_wait(&synchLock);
    sem_destroy(&synchLock);

    /* Check for error conditions */
    switch(status)
    {
        case AMBERR_NOERR:
            break;
        default:
            ControlExceptions::CAMBErrorExImpl ex =
                ControlExceptions::CAMBErrorExImpl(__FILE__, __LINE__,
                    "AmbManagerImpl::monitorTE");
            std::ostringstream msg;
            msg << "ErrorCode = "
                << status;
            ex.addData("Detail", msg.str().c_str());
            ex.log();
            throw ex.getCAMBErrorEx();
    }

    /* Load the data into the output structure */
    rv->length(dataLength);
    for(unsigned short idx(0U); idx < dataLength; ++idx)
    {
        rv[idx] = data[idx];
    }
    return rv._retn();
}

Control::AmbData* AmbManagerImpl::monitorNextTE(AMBSystem::AmbChannel channel,
    AMBSystem::AmbNodeAddr nodeAddress, AMBSystem::AmbRelativeAddr RCA,
    ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(readAccess == false)
    {
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Read access is disabled.");
        ex.log();
        throw ex.getInvalidRequestEx();
    }

    /* Synchronous call to get a monitor point */

    Control::AmbData_var rv(new Control::AmbData);

    AmbCompletion_t* completion(new AmbCompletion_t);
    AmbMessage_t message;

    AmbDataLength_t dataLength;
    AmbDataMem_t data[8];
    sem_t synchLock;
    AmbErrorCode_t status(AMBERR_PENDING);

    sem_init(&synchLock, 0, 0);
    completion->data_p = data;
    completion->timestamp_p = &timestamp;
    completion->status_p = &status;
    completion->synchLock_p = &synchLock;
    completion->dataLength_p = &dataLength;
    completion->channel_p = 0;
    completion->address_p = 0;
    completion->contLock_p = 0;
    completion->type_p = 0;

    /* Build the message block */
    message.requestType = AMB_MONITOR_NEXT;
    message.channel = channel;
    message.dataLen = 0;
    message.completion_p = completion;
    message.targetTE = 0;

    /* Send the message and wait for a return */
    try
    {
        message.address = createAMBAddr(nodeAddress, RCA);
        interface_mp->sendMessage(message);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        sem_destroy(&synchLock);
        delete completion;
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            "AmbManagerImpl::monitorNextTE").getCAMBErrorEx();
    }

    /* Wait for the results */
    sem_wait(&synchLock);
    sem_destroy(&synchLock);

    /* Check for error conditions */
    switch(status)
    {
        case AMBERR_NOERR:
            /* Load the data into the output structure */
            rv->length(dataLength);
            for(unsigned short idx(0U); idx < dataLength; ++idx)
            {
                rv[idx] = data[idx];
            }
            break;
        default:
            ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__,
                    "AmbManagerImpl::monitorNextTE");
            std::ostringstream msg;
            msg << "ErrorCode = "
                << status;
            ex.addData("Detail", msg.str().c_str());
            ex.log();
            throw ex.getCAMBErrorEx();
    }
    return rv._retn();
}

void AmbManagerImpl::monitorTEAsynch(AMBSystem::AmbChannel channel,
    AMBSystem::AmbNodeAddr nodeAddress, AMBSystem::AmbRelativeAddr RCA,
    ACS::Time te, Control::TEData* teData)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(readAccess == false)
    {
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Read access is disabled.");
        ex.log();

        // CORBA oneway methods cannot throw exceptions. - Well they could,
        // but nobody will listen. Therefore just return.
        return;
    }

    /* Build a structure to hold the responses */
    Response_t* tmpResponse(new Response_t);
    std::memset(tmpResponse, 0, sizeof(Response_t));

    AmbCompletion_t* completion(new AmbCompletion_t);
    std::memset(completion, 0, sizeof(AmbCompletion_t));

    AmbMessage_t message;

    sem_init(&tmpResponse->synchLock, 0, 0);
    tmpResponse->data = new AmbDataMem_t[8];
    tmpResponse->teData = Control::TEData::_duplicate(teData);
    tmpResponse->targetTE = te;
    tmpResponse->status = AMBERR_PENDING;

    // Send out the request
    completion->data_p = tmpResponse->data;
    completion->timestamp_p = &tmpResponse->timestamp;
    completion->status_p = &tmpResponse->status;
    completion->synchLock_p = &tmpResponse->synchLock;
    completion->dataLength_p = &tmpResponse->dataLength;

    /* Build the message block */
    message.requestType = AMB_MONITOR;
    message.channel = channel;
    message.dataLen = 0;
    message.completion_p = completion;
    message.targetTE = te;

    /* Send the message and wait for a return */
    try
    {
        message.address = createAMBAddr(nodeAddress, RCA);
        interface_mp->sendMessage(message);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        sem_destroy(&tmpResponse->synchLock);
        delete[] tmpResponse->data;
        CORBA::release(tmpResponse->teData);
        delete tmpResponse;
        delete completion;
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            "AmbManagerImpl::monirotTEAsynch").getCAMBErrorEx();
    }

    /* Add this to the list to be handled by the responseQueue */
    responseMutex_m.acquire();
    responseQueue_m.push_back(tmpResponse);
    responseMutex_m.release();
}

void AmbManagerImpl::command(AMBSystem::AmbChannel channel,
    AMBSystem::AmbNodeAddr nodeAddress, AMBSystem::AmbRelativeAddr RCA,
    const Control::AmbData& data, ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(writeAccess == false)
    {
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Write access is disabled.");
        ex.log();
        throw ex.getInvalidRequestEx();
    }

    try
    {
        commandTE(channel, nodeAddress, RCA, data, timestamp, 0);
    }
    catch(const ControlExceptions::CAMBErrorEx& ex)
    {
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            "AmbManagerImpl::command").getCAMBErrorEx();
    }
}

void AmbManagerImpl::commandTE(AMBSystem::AmbChannel channel,
    AMBSystem::AmbNodeAddr nodeAddress, AMBSystem::AmbRelativeAddr RCA,
    const Control::AmbData& data, ACS::Time& timestamp, ACS::Time te)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(writeAccess == false)
    {
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Write access is disabled.");
        ex.log();
        throw ex.getInvalidRequestEx();
    }

    AmbCompletion_t* completion(new AmbCompletion_t);
    AmbMessage_t message;

    AmbErrorCode_t status(AMBERR_PENDING);
    sem_t synchLock;

    sem_init(&synchLock, 0, 0);
    completion->synchLock_p = &synchLock;
    completion->timestamp_p = &timestamp;
    completion->status_p = &status;
    completion->dataLength_p = 0;
    completion->data_p = 0;
    completion->channel_p = 0;
    completion->address_p = 0;
    completion->contLock_p = 0;
    completion->type_p = 0;

    /* Build the message block */
    message.requestType = AMB_CONTROL;
    message.channel = channel;
    message.dataLen = data.length();
    for(unsigned short idx(0U); idx < message.dataLen; ++idx)
    {
        message.data[idx] = data[idx];
    }
    message.completion_p = completion;
    message.targetTE = te;

    /* Send the message and wait for a return */
    try
    {
        message.address = createAMBAddr(nodeAddress, RCA);
        interface_mp->sendMessage(message);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        sem_destroy(&synchLock);
        delete completion;
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            "AmbManagerImpl::commandTE").getCAMBErrorEx();
    }

    sem_wait(&synchLock);
    sem_destroy(&synchLock);

    /* Look for errors */
    /* Check for error conditions */
    switch(status)
    {
        case AMBERR_NOERR:
            break;
        default:
            ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__,
                "AmbManagerImpl::commandTE");
            std::ostringstream msg;
            msg << "ErrorCode = "
                << status;
            ex.addData("Detail", msg.str().c_str());
            ex.log();
            throw ex.getCAMBErrorEx();
    }
    return;
}

void AmbManagerImpl::commandNextTE(AMBSystem::AmbChannel channel,
    AMBSystem::AmbNodeAddr nodeAddress, AMBSystem::AmbRelativeAddr RCA,
    const Control::AmbData& data, ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(writeAccess == false)
    {
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Write access is disabled.");
        ex.log();
        throw ex.getInvalidRequestEx();
    }

    AmbCompletion_t* completion(new AmbCompletion_t);
    AmbMessage_t message;

    AmbErrorCode_t status(AMBERR_PENDING);
    sem_t synchLock;

    sem_init(&synchLock, 0, 0);
    completion->synchLock_p = &synchLock;
    completion->timestamp_p = &timestamp;
    completion->status_p = &status;
    completion->dataLength_p = 0;
    completion->data_p = 0;
    completion->channel_p = 0;
    completion->address_p = 0;
    completion->contLock_p = 0;
    completion->type_p = 0;

    /* Build the message block */
    message.requestType = AMB_CONTROL_NEXT;
    message.channel = channel;
    message.dataLen = data.length();
    for(unsigned short idx(0U); idx < message.dataLen; ++idx)
    {
        message.data[idx] = data[idx];
    }
    message.completion_p = completion;
    message.targetTE = 0;

    /* Send the message and wait for a return */
    try
    {
        message.address = createAMBAddr(nodeAddress, RCA);
        interface_mp->sendMessage(message);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        sem_destroy(&synchLock);
        delete completion;
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            "AmbManagerImpl::commandNextTE").getCAMBErrorEx();
    }
    sem_wait(&synchLock);
    sem_destroy(&synchLock);

    /* Look for errors */
    /* Check for error conditions */
    switch(status)
    {
        case AMBERR_NOERR:
            break;
        default:
            ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__,
                "AmbManagerImpl::commandNextTE");
            std::ostringstream msg;
            msg << "ErrorCode = "
                << status;
            ex.addData("Detail", msg.str().c_str());
            ex.log();
            throw ex.getCAMBErrorEx();
    }
    return;
}

void AmbManagerImpl::commandTEAsynch(AMBSystem::AmbChannel channel,
    AMBSystem::AmbNodeAddr nodeAddress, AMBSystem::AmbRelativeAddr RCA,
    const Control::AmbData& data, ACS::Time te, Control::TEData* teData)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(writeAccess == false)
    {
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Write access is disabled.");
        ex.log();

        // CORBA oneway methods cannot throw exceptions. - Well they could,
        // but nobody will listen. Therefore just return.
        return;
    }

    /* Build a structure to hold the responses */
    Response_t* tmpResponse(new Response_t);
    std::memset(tmpResponse, 0, sizeof(Response_t));

    AmbCompletion_t* completion(new AmbCompletion_t);
    std::memset(completion, 0, sizeof(AmbCompletion_t));

    AmbMessage_t message;

    sem_init(&tmpResponse->synchLock, 0, 0);
    tmpResponse->teData = Control::TEData::_duplicate(teData);
    tmpResponse->targetTE = te;
    tmpResponse->status = AMBERR_PENDING;

    completion->synchLock_p = &tmpResponse->synchLock;
    completion->timestamp_p = &tmpResponse->timestamp;
    completion->status_p = &tmpResponse->status;

    /* Build the message block */
    message.requestType = AMB_CONTROL;
    message.channel = channel;
    message.dataLen = data.length();
    for(unsigned short idx(0U); idx < message.dataLen; ++idx)
    {
        message.data[idx] = data[idx];
    }
    message.completion_p = completion;
    message.targetTE = te;

    /* Send the message and wait for a return */
    try
    {
        message.address = createAMBAddr(nodeAddress, RCA);
        interface_mp->sendMessage(message);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        sem_destroy(&tmpResponse->synchLock);
        CORBA::release(tmpResponse->teData);
        delete tmpResponse;
        delete completion;
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            "AmbManagerImpl::commandTEAsynch").getCAMBErrorEx();
    }

    /* Add this to the list to be handled by the responseQueue */
    responseMutex_m.acquire();
    responseQueue_m.push_back(tmpResponse);
    responseMutex_m.release();
}

/* -------------------------- [ Thread Interface ] ----------------------*/
void AmbManagerImpl::ambManagerMonitor()
{
    // Only deal with responses if there are any.
    if(responseQueue_m.empty())
    {
        return;
    }

    // Do a quick copy of the response queue so that we do not delay too much.
    std::deque< Response_t* > tmpResponseQueue;

    responseMutex_m.acquire();
    tmpResponseQueue.swap(responseQueue_m);
    responseMutex_m.release();

    // Now is time enough to handle the responses.
    Response_t* tmpResponse(0);
    Control::DataPt returnData;

    while(tmpResponseQueue.empty() == false)
    {
        /* There is something in the queue wait for it and process */
        tmpResponse = tmpResponseQueue.front();
        tmpResponseQueue.pop_front();

        sem_wait(&tmpResponse->synchLock);
        switch(tmpResponse->status)
        {
            case AMBERR_NOERR:
                returnData.ErrorString = CORBA::string_dup("");
                returnData.targetTE = tmpResponse->targetTE;
                returnData.timestamp = tmpResponse->timestamp;
                if(tmpResponse->data == 0)
                {
                    returnData.data.length(0);
                }
                else
                {
                    returnData.data.length(tmpResponse->dataLength);
                    for(unsigned short idx(0U); idx < tmpResponse->dataLength;
                        ++idx)
                    {
                        returnData.data[idx] = tmpResponse->data[idx];
                    }
                }
                break;
            default:
                /* Send out a notification */
                returnData.ErrorString = CORBA::string_dup(
                    "Error executing AMB command");
                returnData.targetTE = tmpResponse->targetTE;
                returnData.timestamp = tmpResponse->timestamp;
                returnData.data.length(0);
        }

        /* Send the response back the the teData object */
        try
        {
            (tmpResponse->teData)->putData(returnData);
            CORBA::release(tmpResponse->teData);
        }
        catch(...)
        {
            /* This is usually because the callback object has ceased to exist.
             * I don't know what to do about that except throw the data away.
             */
            ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__,
                "AmbManagerImpl::ambManagerMonitor");
            ex.addData("Detail", "Error returning Asynchronous Data!");
            ex.log();
        }

        /* Clean up resources */
        if(tmpResponse->data != 0)
        {
            /* This is a monitor delete the data memory */
            delete[] tmpResponse->data;
        }

        sem_destroy(&tmpResponse->synchLock);
        delete tmpResponse;
    }
}

#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(AmbManagerImpl)
