#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef ambManagerImpl_h
#define ambManagerImpl_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// jkern  2003-12-17  created
//


/// This is the implementation header for the CAN manager engineering interface


#include <acscomponentImpl.h>
#include <acsThread.h>

#include <ambManagerS.h>
#include <ambInterface.h>
#include <deque>

// For the mutex which protects all access related methods and the other one
// which synchronises access to the responses.
#include <Mutex.h>


namespace maci
{
    class ContainerServices;
};


typedef struct
{
    ACS::Time timestamp;
    ACS::Time targetTE;
    AmbDataLength_t dataLength;
    AmbErrorCode_t status;
    AmbDataMem_t* data;
    sem_t synchLock;
    Control::TEData* teData;
} Response_t;


#ifndef AmbManagerThread
// Forward Declaration
class AmbManagerThread;
#endif


class AmbManagerImpl:
    public acscomponent::ACSComponentImpl,
    public virtual POA_Control::AmbManager
{
    public:
    /// Allow \ref AmbManagerSimImpl access to private members.
    friend class AmbManagerSimImpl;

    /// Constructor.
    AmbManagerImpl(const ACE_CString& name,
        maci::ContainerServices* containerServices);

    /// Destructor.
    virtual ~AmbManagerImpl();

    // Lifecycle Interface

    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void initialize();

    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void cleanUp();

    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void aboutToAbort();

    // Implementation
    /// Returns true if the AmbManager is in read and write mode. Control
    /// and monitor requests are allowed and the AmbManager is obviously not
    /// disabled.
    virtual CORBA::Boolean commandingEnabled();

    /// Returns true if the AmbManager is in read only mode. Only monitor
    /// requests are allowed and the AmbManager is obviously not disabled.
    virtual CORBA::Boolean monitoringEnabled();

    /// Turn all monitoring and commanding off. After this method has been
    /// called, commandingEnabled() and monitoringEnabled() will return
    /// false. To enable access again, call either enableReadOnlyAccess() or
    /// enableReadWriteAccess().
    virtual void disableAccess();

    /// Turn monitoring on. No control requests are allowed.
    virtual void enableReadOnlyAccess();

    /// Turn monitoring and commanding on. Monitor and control requests are
    /// allowed.
    virtual void enableReadWriteAccess();

    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::InvalidRequestEx
    virtual AmbChannel getNumberOfChannels(ACS::Time& timestamp);

    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::InvalidRequestEx
    virtual void reset(AMBSystem::AmbChannel channel, ACS::Time& timestamp);

    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::InvalidRequestEx
    virtual AmbSerialNumber* findSN(AmbChannel channel,
        AmbNodeAddr nodeAddress, ACS::Time& timestamp);

    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::InvalidRequestEx
    virtual AmbNodeSeq* getNodes(AmbChannel channel, ACS::Time& timestamp);

    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::InvalidRequestEx
    virtual void flush(AmbChannel channel, ACS::Time flushTime,
        ACS::Time& timestamp);

    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::InvalidRequestEx
    virtual Control::AmbData* monitor(AMBSystem::AmbChannel channel,
        AMBSystem::AmbNodeAddr nodeAddress, AMBSystem::AmbRelativeAddr RCA,
        ACS::Time& timestamp);

    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::InvalidRequestEx
    virtual Control::AmbData* monitorTE(AMBSystem::AmbChannel channel,
        AMBSystem::AmbNodeAddr nodeAddress, AMBSystem::AmbRelativeAddr RCA,
        ACS::Time& timestamp, ACS::Time te);

    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::InvalidRequestEx
    virtual Control::AmbData* monitorNextTE(AMBSystem::AmbChannel channel,
        AMBSystem::AmbNodeAddr nodeAddress, AMBSystem::AmbRelativeAddr RCA,
        ACS::Time& timestamp);

    virtual void monitorTEAsynch(AMBSystem::AmbChannel channel,
        AMBSystem::AmbNodeAddr nodeAddress, AMBSystem::AmbRelativeAddr RCA,
        ACS::Time te, Control::TEData* teData);

    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::InvalidRequestEx
    virtual void command(AMBSystem::AmbChannel channel,
        AMBSystem::AmbNodeAddr nodeAddress, AMBSystem::AmbRelativeAddr RCA,
        const Control::AmbData& data, ACS::Time& timestamp);

    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::InvalidRequestEx
    virtual void commandTE(AMBSystem::AmbChannel channel,
        AMBSystem::AmbNodeAddr nodeAddress, AMBSystem::AmbRelativeAddr RCA,
        const Control::AmbData& data, ACS::Time& timestamp, ACS::Time te);

    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::InvalidRequestEx
    virtual void commandNextTE(AMBSystem::AmbChannel channel,
        AMBSystem::AmbNodeAddr nodeAddress, AMBSystem::AmbRelativeAddr RCA,
        const Control::AmbData& data, ACS::Time& timestamp);

    virtual void commandTEAsynch(AMBSystem::AmbChannel channel,
        AMBSystem::AmbNodeAddr nodeAddress, AMBSystem::AmbRelativeAddr RCA,
        const Control::AmbData& data, ACS::Time targetTE,
        Control::TEData* callBack);

    /// Monitor method which handles the asychronous returns.
    void ambManagerMonitor();


    private:
    /// No default constructor.
    AmbManagerImpl();

    /// No copy constructor.
    AmbManagerImpl(const AmbManagerImpl&);

    /// No assignment operator.
    AmbManagerImpl& operator=(const AmbManagerImpl&);

    /// Get rid of the hardware interface.
    void destroyAmbInterface();

    const AmbInterface* interface_mp;
    ACE_Mutex accessMutex_m;
    ACE_Mutex responseMutex_m;
    bool readAccess;
    bool writeAccess;
    maci::ContainerServices* containerService;
    std::deque< Response_t* > responseQueue_m;
    std::string monitorThreadName;
};


class AmbManagerThread: public ACS::Thread
{
    public:
    AmbManagerThread(const ACE_CString &name, AmbManagerImpl* mgr_ptr,
        const ACS::TimeInterval& responseTime =
            ACS::ThreadBase::defaultResponseTime,
        const ACS::TimeInterval& sleepTime = static_cast< ACS::TimeInterval >(
            ACS::ThreadBase::defaultSleepTime * 0.1),
        const bool del = true):
        ACS::Thread(name, responseTime, sleepTime, del),
        ambManager_p(mgr_ptr)
    {
        ACS_TRACE("AmbManagerThread::AmbManagerThread");
    }

    virtual ~AmbManagerThread()
    {
        terminate();
    };

    virtual void runLoop()
    {
        ambManager_p->ambManagerMonitor();
    }

    private:
    AmbManagerImpl* ambManager_p;
};
#endif /*!AMBMANAGERIMPL_H*/
