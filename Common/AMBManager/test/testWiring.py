#!/usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston,
# MA 02111-1307  USA
#

import sys
import unittest
import struct
import ControlExceptions
from CCL.AmbManager import *
from time import sleep

from Acspy.Common.QoS import setObjectTimeout

import Acspy.Util

class automated( unittest.TestCase ):
    def __init__( self, methodName="runTest" ):
        unittest.TestCase.__init__( self, methodName )
        setObjectTimeout(Acspy.Util.ACSCorba.getManager(), 20000)
        self.manager = AmbManager( antenna )
        setObjectTimeout(self.manager._AmbManager__mgrRef, 20000)
        self.numChannels = 6
        self.out = ""
        
        
    def __del__( self ):
        if self.out != None:
            print self.out
        del( self.manager )


    def startTestLog( self, testName ):
        self.log( "\n" )
        self.log( "-------------------------------------------------------" )
        self.log( "Begin "+testName )

    def endTestLog( self, testname ):
        self.log( "End" + testname )
        self.log( "-------------------------------------------------------" )

    def log( self, msg ):
        self.out += msg + "\n"

	# --------------------------------------------------
	# Test Definitions (currently none)


class interactive( automated ):
    def __init__( self, methodName="runTest" ):
        automated.__init__( self, methodName )

    def tearDown( self ):
        self.out = None

    def log( self, msg ):
        print msg

	# --------------------------------------------------
	# Test Definitions


    def test01( self ):
        '''
        Assert the reset signals on all the buses. 
        '''
        self.startTestLog( "Test 1: Reset test" )
        channel = 0;
        while (channel < self.numChannels):
            try:
                self.log ("Asserting the reset signal every 48ms on AMB%d. Hit ^C to test the next channel" % (channel+1))
                while (True):
                    try:
                        t = self.manager.reset(channel)
                    except:
                        pass
                    sleep(0.048)
            except KeyboardInterrupt:
                pass
            channel += 1
        self.endTestLog( "Test 1: Reset test" )	

    def test02( self ):
        '''
        Send CAN traffic out on each buse. 
        '''
        self.startTestLog( "Test 2: CAN test" )
        channel = 0;
        while (channel < self.numChannels):
            try:
                self.log ("Sending CAN commands out every 48ms on AMB%d. Hit ^C to test the next channel" % (channel+1))
                while (True):
                    try:
                        t = self.manager.command(channel, 0, 0, struct.pack("8B",00,0xFF,00,0xFF,0xAA,0xAA,0xAA,0xAA))
                    except:
                        pass
                    sleep(0.048)
            except KeyboardInterrupt:
                pass
            channel += 1
        self.endTestLog( "Test 2: CAN test" )	

# **************************************************
# MAIN Program
# usage: testWiring interactive DV01
if __name__ == '__main__':
	if len( sys.argv ) == 1:
		sys.argv.append( "interactive" )

	if len( sys.argv ) == 2:
		sys.argv.append( "DV01" )

	antenna = sys.argv[2]
	sys.argv.remove( sys.argv[2] )

	unittest.main()
