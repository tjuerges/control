#! /usr/bin/env python
#
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# $Id$
#


'''
Define a test class for the CAMBManager
'''


# This is necessary to prevent the test from picking up the local version
# of the Control
import sys
import os
cwd = os.getcwd().split('/')
path = []
for x in sys.path:
    sx = x.split('/')
    if sx[0:(len(cwd) - 1)] != cwd[0:(len(cwd) - 1)]:
        path.append(x)

sys.path = path


import Acspy.Clients.SimpleClient
import Control__POA
import ControlExceptions
import unittest
import struct
import time

from Acspy.Common.QoS import setObjectTimeout

import Acspy.Util

import CCL.Antenna


class asynchData(Control__POA.TEData):
    def __init__(self):
        self.errorFlag = 0
        self.errorList = []
        self.dataList = []
        self.myRef = self._this

    def __del__(self):
        pass

    def putData(self, dataPt):
        '''
        This function is used by the asynchrous command/monitor functions of
        AmbManager.  The user should NEVER execute this command directly.
        '''
        if(len(dataPt.ErrorString) != 0):
            self.errorFlag = 1
            self.errorList.append(len(self.dataList))
        self.dataList.append(dataPt)


class cambManagerTestCase(unittest.TestCase):
    def __init__(self, methodName = 'runTest'):
        self._out = ""
        unittest.TestCase.__init__(self, methodName)

    def __del__(self):
        _ref.flush(0, 0)
        print self._out

    #-------------------------------------------------
    # Now we start the various tests
    # I use the DigiClock simulator for a device to test against
    #-------------------------------------------------
    def test1getNumberOfChannels(self):
        (resp, time) = _ref.getNumberOfChannels()
        self.failIf(resp != 1, "Incorrect number of channels specified")

    def test2getNodes(self):
        (resp, time) = _ref.getNodes(0)
        self.failIf(len(resp) == 0, "No Nodes Responded to broadcast")
    
    def test3findSN(self):
        (resp, time) = _ref.findSN(0, 0x30)
        (sn,) = struct.unpack("!Q", resp)
        self.failIf(sn != 0xc73dde7d7c83903e,
            "Incorrect Serial number returned "\
            "(expected = 0x%x, received = 0x%x)." % (0xc73dde7d7c83903e, sn))

    def test4flush(self):
        resp = _ref.flush(0, 0)

    def test5reset(self):
        resp = _ref.reset(0)

    def test6monitor(self):
        # Test the cycle time
        resp = []
        delta = []
        for i in range(0, 10):
            resp.append(_ref.monitor(0, 0x30, 0x61))
        for i in range(1, 10):
            delta.append(resp[i][1] - resp[i - 1][1])
        self._out += ("maximum monitor loop time is: " + str(max(delta) /
                                                             10000.0) + " ms")
        self.failIf(max(delta) > 150000,
                    "Monitor loop time exceeded 15 ms");

    def test7command(self):
        # Test the cycle time
        resp = []
        delta = []
        for i in range(0, 10):
            resp.append(_ref.command(0, 0x30, 0x81, struct.pack("!Q", 0)))
        for i in range(1, 10):
            delta.append(resp[i] - resp[i - 1])
        self._out += ("maximum command loop time is: " + str(max(delta) /
                                                             10000.0) + " ms")
        self.failIf(max(delta) > 150000,
                    "Command loop time exceeded 15 ms");
    
    def test8monitorNextTE(self):
        # Check for the accuracy of the timestamp
        resp = []
        delta = []
        for i in range(0, 10):
            resp.append(_ref.monitorNextTE(0, 0x30, 0x61))
        for i in range(0, 10):
            delta.append(resp[i][1] % 480000)
        self._out += ("miximum time to monitor point is: " + str(max(delta) /
                                                                 10000.0))
        self._out += (" ms\nminimum time to monitor point is: " +
                      str(min(delta) / 10000.0) + " ms\n")
        self.failIf(max(delta) > 260000,
                    "te monitor over 2 ms late")
        self.failIf(min(delta) < 239000,
                    "te monitor over 100 us early")


    def test9commandNextTE(self):
        # Check for the accuracy of the timestamp
        resp = []
        delta = []
        for i in range(0, 10):
            resp.append(_ref.commandNextTE(0, 0x30, 0x81, \
                                                struct.pack("!Q", 0)))
        for i in range(0, 10):
            delta.append(resp[i] % 480000)
        self._out += ("maximum time to command point is: " + str(max(delta) /
                                                                 10000.0))
        self._out += (" ms\nminimum time to command point is: " +
                      str(min(delta) / 10000.0) + " ms\n")
        self.failIf(max(delta) > 20000,
                    "te command over 2 ms late")
        self.failIf(min(delta) < 50,
                    "te command within 5 us of TE")


    def test10monitorTE(self):
        # Check the latency of the timestamp
        avg = 0
        (junk, targetTime) = _ref.monitorNextTE(0, 0x30, 0x61)
        targetTime += 240000 - (targetTime % 480000)
        for  i in range(0, 10):
            targetTime += 480000
            (junk, time) = _ref.monitorTE(0, 0x30, 0x61, targetTime)
            self.failIf(time - targetTime > 20000,
                        "monitor TE executed over 2 ms late")
            avg += time - targetTime
        self._out += ("mean latency for monitor is: " + str(avg / 100000.0) +
                      " ms\n")


    def test11commandTE(self):
        # Check the latency of the timestamp
        avg = 0
        targetTime = _ref.commandNextTE(0, 0x30, 0x81, struct.pack("!Q", 0))
        targetTime -= targetTime % 480000
        for  i in range(0, 10):
            targetTime += 480000
            time = _ref.commandTE(0, 0x30, 0x81, struct.pack("!Q", 0),
                                       targetTime)
            self.failIf(time - targetTime > 20000,
                        "command TE executed over 2 ms late")
            avg += time - targetTime
        self._out += ("mean latency for command is: " + str(avg / 100000.0) +
                      " ms\n")
            
    def test12monitorTEAsynch(self):
        # Check for the time taken for a single execution
        # Wait for maximum 120s.
        spentTime = 120
        data = asynchData()
        (junk, targetTime) = _ref.monitorNextTE(0, 0x30, 0x61)
        targetTime += 240000 - (targetTime % 480000)
        for  i in range(0, 10):
            targetTime += 480000
            _ref.monitorTEAsynch(0, 0x30, 0x61, targetTime, data.myRef())
        while (len(data.dataList) != 10) and (spentTime > 0):
            spentTime -= 1
            time.sleep(1)
        self.failIf(data.errorFlag != 0, "Error in Asynchronous TE Monitor")
        for resp in data.dataList:
            self.failIf(abs(resp.targetTE - resp.timestamp) > 20000,
                        "execution over 2 ms from target")

    def test13commandTEAsynch(self):
        # Check for the time taken for a single execution
        # Wait for maximum 120s.
        spentTime = 120
        data = asynchData()
        targetTime = _ref.commandNextTE(0, 0x30, 0x81,
                                             struct.pack("!Q", 0))
        targetTime -= targetTime % 480000
        for  i in range(0, 10):
            targetTime += 480000
            _ref.commandTEAsynch(0, 0x30, 0x81, struct.pack("!Q", 0),
                                      targetTime, data.myRef())
        while (len(data.dataList) != 10) and (spentTime > 0):
            spentTime -= 1
            time.sleep(1)
        self.failIf(data.errorFlag != 0, "Error in Asynchronous TE Command")
        for resp in data.dataList:
            self.failIf(abs(resp.targetTE - resp.timestamp) > 20000,
                        "execution over 2 ms from target")

if __name__ == '__main__':
    _Antenna = "DV01"
    _client = Acspy.Clients.SimpleClient.PySimpleClient.getInstance()
    setObjectTimeout(Acspy.Util.ACSCorba.getManager(), 20000)

    _AntennaRef = CCL.Antenna.Antenna(_Antenna)
    _AntennaRef.controllerOperational()

    if _AntennaRef.isAmbManagerRunning() == False:
        _AntennaRef.startAmbManager()

    _ref = _client.getComponentNonSticky("CONTROL/"
        + _Antenna + "/AmbManager")

    setObjectTimeout(_ref, 20000)
    unittest.main()

    _AntennaRef.controllerShutdown()
    _client.disconnect()
