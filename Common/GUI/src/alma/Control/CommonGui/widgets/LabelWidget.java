/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.CommonGui.widgets;

import alma.Control.CommonGui.properties.models.IPropertyModelChangeListener;

import javax.swing.JLabel;
import javax.swing.SwingUtilities;

/**
 * <code>LabelWidget</code>s are used in ALMA CONTROL GUIs to display values with optional units. 
 * These labels support conventions for drawing operator's attention to values as needed.
 * <p>
 * The logic for deciding <strong>when</strong> to draw attention to values is elsewhere.
 * <p>
 * After creating an instance, call the <code>initialize()</code> method <strong>before</strong> 
 * calling any other methods. This enables code for drawing values with units and appropriate 
 * emphasis. This is disabled by default to allow this class to work in a GUI builder.
 * <p>
 * TODO: review this design decision.  GUI builders have not proved to be as useful as hoped.  
 * For now, the <code>initialize()</code> method is called in the constructor.
 * <p>
 * This class is designed to be driven by code on a Java user thread.  It will defer work to the 
 * Swing event-dispatching thread as needed.
 * <p>
 * Set emphasis using <code>setEmphasis(LabelWidgetEmphasis)</code>.
 * <p>
 * Set units using <code>setUnits(String)</code>.
 * <p>
 * Set values using <code>setValue(String)</code>.
 */
public class LabelWidget extends JLabel implements IPropertyModelChangeListener {

    /**
     * TODO: Set a non-default serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    private LabelWidgetEmphasis emphasis;
    private boolean initialized;
    private String units = "";
    private String value = "";

    /**
     * Create a new blank LabelWidget with default settings.
     */
    public LabelWidget() {
        super("", null, LEADING);
        initialize();
    }

    /**
     * Create a new LabelWidget with default settings and the given value.
     * 
     * @param newValue
     *            an initial value to display.
     */
    public LabelWidget(String newValue) {
        super("", null, LEADING);
        initialize();
        setValue(newValue);
    }

    /**
     * Create a new LabelWidget with default settings and the given value and alignment.
     * 
     * @param newValue
     *            an initial value to display.
     * @param horizontalAlignment
     *            an initial horizontal alignment.
     */
    public LabelWidget(String newValue, int horizontalAlignment) {
        super("", null, horizontalAlignment);
        initialize();
        setValue(newValue);
    }

    /**
     * Create a new LabelWidget with default settings and the given value and units.
     * 
     * @param newValue
     *            an initial value to display.
     * @param newUnits
     *            initial units to display with the initial value.
     */
    public LabelWidget(String newValue, String newUnits) {
        super("", null, LEADING);
        initialize();
        setValue(newValue);
        setUnits(newUnits);
    }

    /**
     * Create a new LabelWidget with default settings and the given value, units, and alignment.
     * 
     * @param newValue
     *            an initial value to display.
     * @param newUnits
     *            initial units to display with the initial value.
     * @param horizontalAlignment
     *            an initial horizontal alignment.
     */
    public LabelWidget(String newValue, String newUnits, int horizontalAlignment) {
        super("", null, horizontalAlignment);
        initialize();
        setValue(newValue);
        setUnits(newUnits);
    }

    /**
     * Get the emphasis property.  This is intended for testing and debugging.
     * 
     * @return current emphasis enumeration.
     */
    LabelWidgetEmphasis getEmphasis() {
        return emphasis;
    }

    /**
     * Get the units property.  This is intended for testing and debugging.
     * 
     * @return the units string.
     */
    String getUnits() {
        return units;
    }

    /**
     * Get the value property.  This is intended for testing and debugging.
     * 
     * @return the value string.
     */
    String getValue() {
        return value;
    }

    /**
     * Call <code>initialize()</code> after creating an instance.
     * <p>
     * This method initializes the instance remove restrictions that allow it to
     * function in a GUI builder.
     * <p>
     * TODO: this method is left over from trying to use CGLabels in GUI builders.
     * For now, just call it in the constructor.  Review and decide if this should
     * be refactored out. 
     */
    public void initialize() {
        
        emphasis = LabelWidgetEmphasis.DEGRADED;
        initialized = true;
        
        // We can't predict when initialize may be called.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                updateContents();
            }
        });
    }

    /**
     * Get the initialized property.  This is intended for testing and debugging.
     * 
     * @return the initialized boolean.
     */
    boolean isInitialized() {
        return initialized;
    }

    /**
     * Get the current emphasis setting for this instance.
     * 
     * @param newEmph new value for emphasis.
     */
    public void setEmphasis(LabelWidgetEmphasis newEmph) {
        
        this.emphasis = newEmph;
        
        // We can't predict when setEmphasis may be called.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                updateContents();
            }
        });
    }

    /**
     * The superclass provides a setText() method. The superclass setText() method must not be 
     * called directly. It is mapped to setValue(String) to ensure consistent appearance and 
     * behavior.
     * <p>
     * 
     * @param newText
     *            the new text string.
     */
    public void setText(String newText) {
        setValue(newText);
    }

    /**
     * Set the optional units for this label.
     * <p>
     * 
     * @param newUnits
     *            the new units to display with the value.
     */
    public void setUnits(String newUnits) {

        this.units = newUnits;
        
        // We can't predict when setUnits may be called.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                updateContents();
            }
        });
    }

    /**
     * Set a new value for this label.
     * <p>
     * 
     * @param newValue
     *            the new value to display with the optional units.
     */
    public void setValue(String newValue) {
        
        this.value = newValue;
        
        // We can't predict when setValue may be called.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                updateContents();
            }
        });
    }

    private void updateContents() {
        // @TODO This method creates problems in the Netbeans GUI builder when
        // called by set*(). Debug!
        
        if (!isInitialized())
            super.setText(value);
        else {
            String htmlText = "<html>";
            if (emphasis.getEmphasize())
                htmlText += "<i>";
            htmlText += "<font color=\"" + emphasis.getFgHtmlColor() + "\"";
            if (emphasis.getSizeModifier() > 0)
                htmlText += " size=\"+" + emphasis.getSizeModifier() + "\"";
            else if (emphasis.getSizeModifier() < 0)
                htmlText += " size=\"" + emphasis.getSizeModifier() + "\"";
            htmlText += ">";
            htmlText += value;
            htmlText += "</font>";
            if (emphasis.getEmphasize())
                htmlText += "</i>";

            if (units.equalsIgnoreCase(""))
                htmlText += "</html>";
            else
                htmlText += " " + units + "</html>";
            super.setText(htmlText);
        }
    }
}

//
// O_o
