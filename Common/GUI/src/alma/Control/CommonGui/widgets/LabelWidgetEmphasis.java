/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.CommonGui.widgets;

import alma.acs.gui.standards.StandardColors;

import java.awt.Color;

/**
 * <code>LabelWidgetEmphasis</code> is used to identify the type of emphasis that should be used 
 * for values in <code>LabelWidget</code>s.
 * <p>
 * Each enum value contains the text modifiers to be applied to emphasize text on the display.
 */
public enum LabelWidgetEmphasis {
    // Notes on colors:
    //
    // ACS StandardColors assume we will use the same color for all text, and update the text 
    // background to draw attention to text.  This is inconsistent with CONTROL plans.  We will 
    // use the ACS StandardColors to set text colors, leaving the background color at the standard 
    // OK color.
    //
    // Based on testing, font size modifier of 0 matches the Java default text size.
    //
    DEGRADED(StandardColors.STATUS_WARNING_BG.color, 0, true), 
    ERROR(StandardColors.STATUS_ERROR_BG.color, 1, true),
    GOOD(StandardColors.STATUS_OKAY_BG.color, 0, false), 
    NONE(StandardColors.TEXT_FG.color, 0, false), 
    WARNING(StandardColors.STATUS_WARNING_BG.color, 1, false);
        
    /**
     * emphasize indicates if text is to be drawn in an emphasized type face.
     */
    private boolean emphasize;
    
    /**
     * fgHmtlColor is an HTML color string.
     */
    private String fgHtmlColor;
    
    /**
     * sizeModifier is a modifier to increase or decrease text size relative to the current type 
     * face size.
     */
    private int sizeModifier;

    private LabelWidgetEmphasis(Color fgC, int sizeMod, boolean emph) {
        int red = fgC.getRed();
        int green = fgC.getGreen();
        int blue = fgC.getBlue();

        String redStr = Integer.toHexString(red);
        String greenStr = Integer.toHexString(green);
        String blueStr = Integer.toHexString(blue);

        fgHtmlColor = "#" + redStr + greenStr + blueStr;
        sizeModifier = sizeMod;
        emphasize = emph;
    }

    public boolean getEmphasize() { return emphasize; }

    public String getFgHtmlColor() { return fgHtmlColor; }
    
    public int getSizeModifier() { return sizeModifier; }
    
}

//
//O_o

