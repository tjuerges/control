/*
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.CommonGui.environment;

import java.util.logging.Logger;

import alma.acs.container.ContainerServices;

// import alma.exec.extension.subsystemplugin.PluginContainerServices;

/**
 * The write only interface for building up a class that implements the IGuiEnvironment interface.
 * <p>
 * This interface is only used by the class that builds up an IGuiEnvironment.  It is not used 
 * by IGuiEnvironment clients.
 * <p>
 * The builder interface is separated from the read-only interface to prevent clients from changing 
 * the contents of a GuiEnvironment.
 */

public interface IRuntimeEnvironmentBuilder {
    
    /**
     * Set ACS ContainerServices.
     * 
     * @param cs ACS ContainerServices
     */
    public void setContainerServices(ContainerServices cs);
    
    /**
     * Store the Array this GUI monitors.
     */
    public void setArrayName(String array);
    
    /**
     * Store the logger to log to.
     */
    public void setLogger(Logger logger);
    
    /**
     * Set the GUI to read only mode.
     */
    public void setRunRestricted(boolean restricted);
    
    /**
     * Store the OMC PluginContainerServices reference.
     */
//    public void setServices(PluginContainerServices services);

}

//
// O_o
