/*
 * ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.CommonGui.environment;

import java.util.logging.Logger;

import alma.acs.container.ContainerServices;

/**
 * A bag of data containing information from the run time environment of a Control GUI.
 * <p>
 * This exists to pass the environment around within the GUI while minimizing coupling between 
 * parts.  It also massages some data from raw sources to reduce duplicated code in Control GUIs.
 * <p>
 * Clients of this class should keep a reference to a IRuntimeEnvironment, not this class.  This 
 * prevents buggy clients from changing the contents of this class.  Only the code that creates 
 * an instance of this class should change the contents using the IRuntimeEnvironmentBuilder 
 * interface.
 * <p>
 * TODO: Look for ways assign responsibilities to more appropriate classes.
 */

public class CommonEnvironment implements IRuntimeEnvironmentBuilder, IRuntimeEnvironment {
    
    private String arrayName = "Unknown Array Name";
    private Logger logger;
    private boolean restricted;
    private ContainerServices container;
    
//    
//    @SuppressWarnings("unused")
//    private PluginContainerServices services;
    
    public CommonEnvironment () {
    }
    
    /**
     * Implement the IRuntimeEnvironment interface.  See the interface for method header comments.
     */
//    @Override
//    public String getArray() {
//        return array;
//    }

    @Override
    public String getArrayName() {
        return arrayName;
    }

    @Override
    public Logger getLogger() {
        return logger;
    }
    
    @Override
    public boolean runRestricted() {
        return restricted;
    }

    /**
     * Implement the IRuntimeEnvironmentBuilder interface.  See the interface for method header 
     * comments.
     */
    @Override
    public void setArrayName(String array) {
        // this.array = array;
        this.arrayName = array;
    }
    
    @Override
    public void setLogger(Logger logger) {
        this.logger = logger;
    }
    
//    @Override
//    public void setProperties(Map<String, Object> guiProperties) {
//        this.guiModels = new HashMap<String, Object>(guiProperties);
//    }
    
    @Override
    public void setRunRestricted(boolean restricted) {
        this.restricted = restricted;
    }

    @Override
    public ContainerServices getContainerServices() {
        return container;
    }

    @Override
    public void setContainerServices(ContainerServices cs) {
        this.container = cs;
    }
    
//    @Override
//    public void setServices(PluginContainerServices services) {
//        this.services = services;
//    }

} // CommonEnvironment

// 
// O_o
