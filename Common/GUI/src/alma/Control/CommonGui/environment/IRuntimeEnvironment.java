/*
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.CommonGui.environment;

import java.util.logging.Logger;

import alma.acs.container.ContainerServices;

/**
 * The read only interface for getting data from other parts of a GUI, the OMC, UNIX shell 
 * guiEnvironment, etc.
 * <p>
 * Clients of a class that implements this interface should keep a reference to a this interface, 
 * not the implementing class.  This prevents modification of the the contents of the implementing 
 * class.  Only the code that creates an instance of the implementing class should change the 
 * contents.
 */

public interface IRuntimeEnvironment {
    
    /**
     * Get ACS ContainerServices.
     * 
     * @return ACS ContainerServices
     */
    public ContainerServices getContainerServices();
    
    /**
     * Get an Array object modeling an array in CONTROL.
     * <p>
     * TODO: For debugging, the first version will return a string.  Later versions will return a 
     * reference. 
     * 
     * @return an array.
     */
//    public String getArray();
    
    /**
     * Get the text string describing this array.
     * 
     * @return a text string containing an array name.
     */
    public String getArrayName();
    
    /**
     * Get the logger to log to.
     * <p>
     * TODO: For debugging, the first version will return a string.  Later versions will return a 
     * reference. 
     * 
     * @return a logger.
     */
    public Logger getLogger();
    
    /**
     * Is this GUI to run read only?
     * 
     * @return true if this GUI only displays data.
     */
    public boolean runRestricted();

}

//
// O_o

