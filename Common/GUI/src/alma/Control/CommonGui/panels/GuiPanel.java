/*
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.CommonGui.panels;

import alma.Control.CommonGui.environment.IRuntimeEnvironment;
import alma.Control.CommonGui.properties.IPropertyCollection;

import javax.swing.JPanel;

/**
 * Common behavior for Control GUI panels.
 */
public abstract class GuiPanel extends JPanel {
    
    private static final long serialVersionUID = 1L;

    protected IRuntimeEnvironment runtimeEnv;
    protected IPropertyCollection properties;

    public GuiPanel(IRuntimeEnvironment runtimeEnv, IPropertyCollection properties) {
        super();
        this.runtimeEnv = runtimeEnv;
        this.properties = properties;
        addChildren();
    }
    
    /**
     * Create and add all components to be displayed in this panel.
     */
    abstract protected void addChildren();

}

//
// O_o