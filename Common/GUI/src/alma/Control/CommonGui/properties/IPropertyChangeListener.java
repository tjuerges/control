/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.CommonGui.properties;

/**
 * <code>IPropertyChangeListener</code>s receives updates from CONTROL sources.  
 */
public interface IPropertyChangeListener<T> {

    /**
     * Handle a change to a property.
     * 
     * Note: it is necessary to identify the property and the new value as some listeners may 
     * listen for different properties to change from different sources.
     *  
     * @param name of property to set.
     * @param value of property to set.
     */
    public void propertyChanged(IPropertyNames name, T value);

}

//
//O_o
