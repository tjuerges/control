/*
 * ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.CommonGui.properties.sources;

import alma.Control.CommonGui.environment.IRuntimeEnvironment;
import alma.Control.CommonGui.properties.IPropertyChangeListener;
import alma.Control.CommonGui.properties.IPropertyNames;
import alma.Control.CommonGui.properties.PropertyChangeMonitor;

import java.util.Date;
import java.util.HashMap;
import java.util.Set;

/**
 * Read properties from some part of ALMA and pass values to PropertyChangeMonitors.  If necessary, 
 * split compound data out into single properties.  PropertyChangeMonitor will notify listeners of 
 * changes.
 * <p>
 * PropertyChangeMonitors are kept in the HashMap.  This allows allows implementing classes to 
 * contain what ever types of PropertyChangeMonitors are needed to handle the data they read.  
 */
public class PropertySource {
    
    protected IRuntimeEnvironment runtimeEnv;
    
    private HashMap<IPropertyNames, PropertyChangeMonitor> properties = 
                new HashMap<IPropertyNames, PropertyChangeMonitor>(); 
        
    public PropertySource(IRuntimeEnvironment runtimeEnv) {
        this.runtimeEnv = runtimeEnv;        
    }
    
    /**
     * Add a new property to this data source.
     * 
     * @param name
     * @param propertyClass
     */
    //  Several generic types can not be parameterized here.
    protected void addProperty(IPropertyNames name, Class propertyClass, Object initialValue) {
        
        final String METHOD_NAME = getClass().getName() + ".addProperty()";
        
        PropertyChangeMonitor newProperty;
        if (Date.class == propertyClass) {
            newProperty = new PropertyChangeMonitor <Date> (name, (Date)initialValue);
        } else if (Long.class == propertyClass) {
            newProperty = new PropertyChangeMonitor <Long> (name, (Long)initialValue);
        } else if (String.class == propertyClass) {
            newProperty = new PropertyChangeMonitor <String> (name, (String)initialValue);
        } else {
            throw new IllegalArgumentException(
                METHOD_NAME + ": no code to handle propertyClass = " + propertyClass.toString()); 
        }
        properties.put(name, newProperty);
    }
    
    /**
     * Provide a list of properties supplied by this source.
     * 
     * @return a set of all properties supplied by this source.
     */
    public Set<IPropertyNames> propertiesSupplied() {
        return properties.keySet();
    }
    
    /** 
     * Register the given listener for changes to the given property as read by this data source.
     * 
     * @param name of property to listen for changes to.
     * @param listener to report changes to.
     */
    //  Several generic types can not be parameterized here.
    public void registerListenerForProperty(IPropertyNames name, IPropertyChangeListener listener) {

        final String METHOD_NAME = getClass().getName() + ".registerListenerForProperty()";

        if (properties.containsKey(name)) {
            properties.get(name).addListener(listener);
        } else {
            throw new IllegalArgumentException(
                METHOD_NAME + ": property name: " + name + " was not found for this property source.");
            
        }
    }
    
    protected void setProperty(IPropertyNames name, Object value) {
        properties.get(name).setValue(value);
    }
    
} // class PropertySource

//
// O_o
