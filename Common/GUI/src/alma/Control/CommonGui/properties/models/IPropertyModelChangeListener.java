/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.CommonGui.properties.models;

import alma.Control.CommonGui.widgets.LabelWidgetEmphasis;

/**
 * <code>IPropertyModelChangeListener</code>s receive updates from CONTROL GUI Models.  
 * <p>
 * If the listener is a GUI widget, it must address any threading issues within these methods. 
 */
public interface IPropertyModelChangeListener {
    
    /**
     * 
     * @param emphasis
     */
    public void setEmphasis(LabelWidgetEmphasis emphasis);
    
    /**
     * 
     * @param units
     */
    public void setUnits(String units);
    
    /**
     * 
     * @param value
     */
    public void setValue(String value);
    
}

//
//O_o
