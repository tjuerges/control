/*
 * ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.CommonGui.properties.models;

import alma.Control.CommonGui.environment.IRuntimeEnvironment;
import alma.Control.CommonGui.properties.IPropertyNames;
import alma.Control.CommonGui.widgets.IDisplayWidgetBuilder;
import alma.Control.CommonGui.widgets.LabelWidgetEmphasis;

import java.util.HashSet;

import javax.swing.JComponent;

/**
 * Implement the behavior common to all CONTROL GUI Models.
 */
public abstract class PropertyModel implements IDisplayWidgetBuilder {
    
    protected LabelWidgetEmphasis emphasis = LabelWidgetEmphasis.WARNING;
    protected IRuntimeEnvironment runtimeEnv;
    protected HashSet<IPropertyModelChangeListener> listeners = new HashSet<IPropertyModelChangeListener>();
    
    /*
     * Typically, a model will only track one property.
     */
    protected IPropertyNames name;
    
    /*
     * First cut, store all values as strings.
     */
    protected String value = "unknown";
    
    public PropertyModel(IRuntimeEnvironment runtimeEnv, IPropertyNames name) {
        this.runtimeEnv = runtimeEnv;
        this.name = name;
    }

    /* (non-Javadoc)
     * @see alma.Control.CommonGui.properties.IDisplayWidgetBuilder#getDisplayWidget()
     */
    @Override
    public JComponent getDisplayWidget() {
        JComponent widget = makeDisplayWidget();
        // The following conversion will be safe if the subclass implementation of makeDisplayWidget()
        // returns a JComponent that implements IPropertyModelChangeListener.  Don't yet know how to enforce 
        // this in code.
        IPropertyModelChangeListener listener = (IPropertyModelChangeListener) widget;
        listeners.add(listener);
        return widget;       
    }
    
    /**
     * Make the appropriate display widget for this class.  This may be different for every 
     * subclass, but the resulting widget must be a subclass of JComponent and must implement 
     * IPropertyModelChangeListener.
     * 
     * @return new display widget.
     */
    protected abstract JComponent makeDisplayWidget();
    
    /**
     * TODO: decide how to handle a subset of this logic in descendants.
     */
    public void notifyListeners() {
        for (IPropertyModelChangeListener listener: listeners) {
            listener.setValue(value);
            listener.setEmphasis(emphasis);
        }
    }
}

//
// O_o
