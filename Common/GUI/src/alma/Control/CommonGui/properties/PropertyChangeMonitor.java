/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.CommonGui.properties;

import java.util.HashSet;

/**
 * Monitor a single property for changes.  If a change occurs, notify listeners.
 * <p>
 * Internally track which property is being monitored.  This is necessary because 
 * a listener may listen to several different properties.
 *
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 8.0.0
 */

public class PropertyChangeMonitor <T> {
    
	private HashSet<IPropertyChangeListener <T>> listeners = 
                new HashSet<IPropertyChangeListener <T>>();    
    private T oldValue;
    private IPropertyNames propertyName;
    private T value;
    
    /**
     * Create a new instance.
     * 
     * @param propertyName
     * @param initialValue
     */
    public PropertyChangeMonitor(IPropertyNames propertyName, T initialValue) {
	    this.propertyName = propertyName;
		setValue(initialValue);
	}
	
    /**
     * Add a listener.
     * 
     * @param listener
     */
	public void addListener(IPropertyChangeListener<T> listener) {
	    listeners.add(listener);
	}

	/**
	 * Notify all listeners.
	 */
    @SuppressWarnings("unchecked") //  Several generic types can not be parameterized here.
	public void notifyListeners() {
		for (IPropertyChangeListener listener: listeners) {
		    listener.propertyChanged(propertyName, value);
		}
	}
	
	/**
	 * Set a possibly new value.
	 * 
	 * @param newValue
	 */
	public void setValue(T newValue) {
		if (newValue != oldValue) {
			value = newValue;
			notifyListeners();
		}
	}
	
} // class PropertyChangeMonitor

//
// O_o
