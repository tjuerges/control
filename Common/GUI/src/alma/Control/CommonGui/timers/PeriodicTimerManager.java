/*
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.CommonGui.timers;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Manage a set of periodic timers.  Each timer notifies a separate list of TimerListeners when a 
 * time period expires.
 * <p>
 * Clients ask to be notified by calling:
 * <p>
 *      <code>ptm.notifyEvery(ms, client);</code>
 * <p>     
 * on an instance of a PeriodicTimerManager, where ms is the time period in milliseconds.  This 
 * sets an upper bound on the time between notifications.  <strong>THERE ARE NO REAL TIME 
 * GUARENTEES</strong>.  On average, notifications will arrive a little later than the given time 
 * period, due to the overhead of notifying TimerListeners.
 * <p>
 * ptm.notifyEvery(ms, client) is used even if the TimerListener has previously asked for 
 * notification on a different time period.  This allows a listener to change the time period of 
 * notifications without having to unregister with other timers.
 * <p>
 * Clients ask for notifications to stop by calling:
 * <p>
 *      <code>ptm.doNotNotify(client);</code>
 * <p>
 * TimerListeners may be added at any time.  Their first notification may arrive in more or less 
 * than the specified time if they register for a time period with an existing timer.
 * <p>
 * TimerListeners may removed at any time.  They will be removed from the timer they are registered 
 * with immediately, but they may receive a final notification.
 * <p>
 * In both cases above, this is due to timers running in separate threads.
 * <p>
 * Timers are started when created.  There is no need for a "start()" method.
 * <p>
 * All timers may be stopped at once by calling:
 * <p>
 *      <code>ptm.stopAllTimers();</code>
 * <p>     
 * This may be done as part of an orderly shutdown. 
 * <p>
 * Implementation:
 * <p>
 * This class wraps java.util.Timer to provide an interface that meets the typical needs of 
 * TimerListeners.
 */
public class PeriodicTimerManager implements IPeriodicTimerManager {
  
    /**
     * Manage a list of listeners to be notified as a group.
     * <p>
     * The timer that calls the run method on this class is managed internally.
     */
    private class PeriodicNotificationTask extends TimerTask {

        /**
         * TimerListeners to be notified.  This will not be re-initialized during the life of this 
         * object.
         */
        private volatile Set<ITimerListener> listeners = new HashSet<ITimerListener>();
        
        private long period;
        
        /**
         * Flag to stop this task gracefully.  Checked in run() to see if it should shut down the 
         * timer before notifying listeners.
         * <p>
         * This must be set to true before the timer below is scheduled. 
         */        
        private volatile Boolean running = true;
        
        /**
         * The timer that tells this task to when to notify its listeners.  This is created as a 
         * daemon to prevent it from holding a process open.
         */
        private final Timer timer = new Timer(true);

        /**
         * Create a new task that will notify listeners with the given period.
         * <p>
         * Schedule this task with the associated timer.
         * 
         * @param msBetweenNotifications in milliseconds for internal timer.
         */
        public PeriodicNotificationTask(long msBetweenNotifications) {
            this.period = msBetweenNotifications;
            this.timer.schedule(this, 0, period);            
        }
        
        /**
         * Add a listener for this task.
         * 
         * @param listener to notify.
         */
        public void addListener(ITimerListener listener) {
            
            final String METHOD_NAME =  getClass().getName() + 
                ".addListener() period: " + period;
            //
            // Ensure listener is not null and is not already in the list.  This allows us to 
            // change the type of listeners more easily if necessary.
            if (null == listener) {    
                final String msg = METHOD_NAME + ": argument 'listener' may not be null!";
                throw new IllegalArgumentException(msg);
            } else { // Parameters are OK.
                if (!listeners.contains(listener)) {
                    synchronized (listeners) {
                        listeners.add(listener);
                    }
                }
            }            
        }
        
        /**
         * Does this task have a listener?
         * 
         * @return true if the given listener is registered for this task.
         */
        public boolean hasListener(ITimerListener listener) {
            return listeners.contains(listener);
        }
        
        /**
         * Does this task have any listeners?
         * 
         * @return true if list of listeners is not empty.
         */
        public boolean hasListeners() {
            return !listeners.isEmpty();
        }
        
        /**
         * Remove a listener from this task.
         * 
         * @param listener to remove.
         */
        public void removeListener(ITimerListener listener) {

            final String METHOD_NAME =  getClass().getName() + ".removeListener()";

            if (null == listener) {
                final String msg = METHOD_NAME + ": argument 'listener' may not be null!";
                throw new IllegalArgumentException(msg);
            } else { // Parameters are OK.
                if (listeners.contains(listener)) {
                    synchronized (listeners) {
                        listeners.remove(listener);
                    }
                }
                if (listeners.isEmpty()) {
                    // Prepare to be garbage collected.
                    timer.cancel();
                }
            }
        }

        /**
         * Notify all listeners that the time period has expired.  This will be called by an 
         * internal timer.
         */
        @Override
        public void run() {
            if (!running) {
                // This task is shutting down.  Do not notify listeners. Stop the timer.  Empty 
                // the listeners list. 
                timer.cancel();
                listeners.clear();
            } else {
                for (ITimerListener listener : listeners) {
                    listener.timerExpired();
                }
            }
        }
        
        /**
         * Stop the associated timer.  Call this to start a clean shutdown of a 
         * PeriodicNotificationTask.
         */
        public void stop() {
            synchronized (running) {
                running = false;
            }
        }

    } // PeriodicNotificationTask
    
    /**
     * PeriodicTimerManagerErrors indicate serious internal errors in the PeriodicTimerManager.  
     * Applications should not try to catch them.  There is nothing an application can do to 
     * recover.  
     */
    public class PeriodicTimerManagerError extends Error {
        
        private static final long serialVersionUID = 1L;

        public PeriodicTimerManagerError(String message) {
            super(message);
        }
        
    } // PeriodicTimerManagerError
    
    public static final long PERIOD_LOWER_BOUND = 100 * MILLISECONDS; // Initial guess.

    /**
     * Track timer tasks by the period they measure.  This allows adding TimerListeners to existing 
     * timer tasks easily.
     * <p>
     * This list will not be re-initialized during the lifetime of an instance of this class.
     */
    private final Map<Long, PeriodicNotificationTask> tasks = new HashMap<Long, PeriodicNotificationTask>();

    /**
     * Debugging constructor.
     */
    public PeriodicTimerManager() {
        
        @SuppressWarnings("unused")
        final String METHOD_NAME =  getClass().getName() + ".PeriodicTimerManager()";
        
        // Debugging only. Comment out before release.
        // System.out.println("\n" + METHOD_NAME + ": begin.");        
        
        // Debugging only. Comment out before release.
        // System.out.println(METHOD_NAME + ": end.");        
    }
    
    /**
     * Ensure a timer task exists with the given period, then add the listener to the notification 
     * list for that task.
     * 
     * @param period of the timer task to find or create.
     * @param listener to notify.
     */
    private void addListenerToNotificationTask(long period, ITimerListener listener) {

        if (!tasks.containsKey(period)) {
            tasks.put(period, new PeriodicNotificationTask(period)); 
        }        
        tasks.get(period).addListener(listener);        
    }

    /**
     * Remove all tasks with empty listener lists.
     * <p>
     * Do not merge this code with the removeListenerFromAllNotificationTasks().  We want to keep 
     * a clear separation between removing listeners from tasks and removing tasks.
     */
    private void cleanupEmptyTasks() {
        for (long period : tasks.keySet()) {
            PeriodicNotificationTask task = tasks.get(period);
            if (!task.hasListeners()) {
                tasks.remove(period);
            }
        }
    }
    
    /**
     * Return a count of existing timers.  This is intended for testing and debugging.
     * 
     * @return a count of timers.
     */
    long getTaskCount() {
        return tasks.size();
    }
    
    /**
     * Check if a listener is already registered for a NotificationTask.
     * 
     * @param period to check for.
     * @param listener to check for.
     * @return true if the listener is already registered with a NotificationTask with the given 
     *         period.
     */
    private boolean listenerAlreadyRegistered(long period, ITimerListener listener) {
        if (tasks.containsKey(period)) {
            boolean result = tasks.get(period).hasListener(listener);
            return result;
        } else {
            return false;
        }
    }
    
    /**
     * Notify a TimerListener every 'period' milliseconds.
     * 
     * @param period to notify the listener.
     * @param listener to notify.
     */
    @Override
    public void notifyEvery(long period, ITimerListener listener) {
        
        final String METHOD_NAME =  getClass().getName() + ".notifyEvery()";
        
        if (period < PERIOD_LOWER_BOUND) {
            String msg = METHOD_NAME + ": argument 'period' may not be less than " + PERIOD_LOWER_BOUND + "!";
            throw new IllegalArgumentException(msg);            
        } else if (null == listener) {
            String msg = METHOD_NAME + ": argument 'listener' may not be null!";
            throw new IllegalArgumentException(msg);
        } else { // Parameters are OK.
            if (!listenerAlreadyRegistered(period, listener)) {
                removeListenerFromAllNotificationTasks(listener);
                addListenerToNotificationTask(period, listener);
                cleanupEmptyTasks();
            }
        }
    }
    
    /**
     * Remove a TimerListener from all timer tasks.
     * 
     * @param listener to remove.
     */
    private void removeListenerFromAllNotificationTasks(ITimerListener listener) {

        for (long period : tasks.keySet()) {
            PeriodicNotificationTask task = tasks.get(period);
            task.removeListener(listener);
        }
    }

    /**
     * Stop all timers to prepare for an orderly shutdown.
     */
    public void stopAllNotifications() {
        PeriodicNotificationTask task;
        for (long period : tasks.keySet()) {
            task = tasks.get(period);
            task.stop();                
        }      
        
        // All tasks are stopped.  Now empty the list so they may be garbage collected.
        tasks.clear();
    }
    
    /**
     * Stop notifying a TimerListener.
     * 
     * @param listener to stop notifying.
     */
    @Override
    public void stopNotifying(ITimerListener listener) {
        removeListenerFromAllNotificationTasks(listener);
    }
}

//
// O_o
