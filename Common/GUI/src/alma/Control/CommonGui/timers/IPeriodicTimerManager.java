/*
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.CommonGui.timers;

/**
 * The interface that must be implemented by classes that manage sets of periodic timers.
 */
public interface IPeriodicTimerManager {
    
    /**
     * These constants are common to all classes that implement this interface. 
     */
    public static final long MILLISECONDS = 1;
    public static final long SECONDS = 1000 * MILLISECONDS;
    
    /**
     * Notify an ITimerListener every 'period' milliseconds that the time period has passed.
     * 
     * @param period to be notified, in milliseconds.
     * @param listener to notify.
     */
    public void notifyEvery(long period, ITimerListener listener);

    /**
     * Stop notifying an ITimerListener.
     * 
     * @param listener to stop notifying.
     */
    public void stopNotifying(ITimerListener listener);
}

//
//O_o
