/*
ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2010 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets.util.DtsData;

import java.util.ArrayList;

/** 
 * @author  dhunter
 * @version $Id$
 * @since    
 */

/**
 * This class stores a set of data captured from a DTX or DRX module.
 * 
 * It then provides that data broken out into 2 polarities.
 * 
 * Input data:
 *   TODO: Do we want this set by a method or a constructor?
 *         And should it be an array or individual elements?
 *         Should the data capture gizmo get it, or vice-versa?
 *   -Raw data from devices
 * 
 * Outputs data:
 *   -Raw data from devices TODO: should this be an array or a single element?
 *      -For Ch D, C, & B
 *   -Formatted data for BBPrs (0 and 1)
 *      -Provided as alma values, ranging from -4 to +3
 */

//NOTE: Throughout this package, lower bits are older data & upper are newer!!!
public class DataPacket {
    public DataPacket(){

        rawDataBLower = new ArrayList<Long>(100);
        rawDataBUpper = new ArrayList<Long>(100);
        rawDataCLower = new ArrayList<Long>(100);
        rawDataCUpper = new ArrayList<Long>(100);
        rawDataDLower = new ArrayList<Long>(100);
        rawDataDUpper = new ArrayList<Long>(100);
    }
    
    /**
     * Add a set of new raw data. Sets of raw data should be entered in order, with the oldest data
     * first, and newest data last. Out of order data entry will result in difficult to detect errors
     * in the almaValueBBPr0 & 1 arrays.
     * @param newDataBLower
     * @param newDataBUpper
     * @param newDataCLower
     * @param newDataCUpper
     * @param newDataDLower
     * @param newDataDUpper
     */
    public void addRawData(long newDataDLower, long newDataDUpper,
                           long newDataCLower, long newDataCUpper,
                           long newDataBLower, long newDataBUpper){
        rawDataBLower.add(newDataBLower);
        rawDataBUpper.add(newDataBUpper);
        rawDataCLower.add(newDataCLower);
        rawDataCUpper.add(newDataCUpper);
        rawDataDLower.add(newDataDLower);
        rawDataDUpper.add(newDataDUpper);
    }
    
/**
 * This function returns the alma values captured for BBPr0.
 * They will be stored in an Byte 2-d array of the proper size.
 * The second dimension will be 16, for the 16 channels in the DTX.
 * And the first dimension will be equal to the number of raw data samples stored.
 * The alma values range from -4 to +3.
 * @return A 2-d byte array of alma values
 * @throws InsufficientDataException    Thrown if no data is stored
 */
    public byte[][] getAlmaValuesPol0() throws InsufficientDataException{
        if (almaValuesPol0 == null)
            try{
            calcPols();
            } catch (InsufficientDataException eeek){
                throw (eeek);
            }
        return  java.util.Arrays.copyOf(almaValuesPol0, almaValuesPol0.length);
    }
    
    /**
     * This function returns the alma values captured for BBPr0.
     * They will be stored in order, in a 1-d byte array of the proper size.
     * The alma values range from -4 to +3.
     * @return A 2-d byte array of alma values
     * @throws InsufficientDataException    Thrown if no data is stored
     */
        public byte[] getAlmaValues1DPol0() throws InsufficientDataException{
            if (almaValuesPol0 == null)
                try{
                calcPols();
                } catch (InsufficientDataException eeek){
                    throw (eeek);
                }
            byte[] returnValue = new byte[almaValuesPol0.length*16];
            for (int c=0; c<almaValuesPol0.length; c++)
                for (int q=0; q<16; q++)
                    returnValue[c*16+q]=almaValuesPol0[c][q];
            return  returnValue;
        }
    
    /**
     * This function returns the alma values captured for BBPr1.
     * They will be stored in an Byte 2-d array of the proper size.
     * The second dimension will be 16, for the 16 channels in the DTX.
     * And the first dimension will be equal to the number of raw data samples stored.
     * The alma values range from -4 to +3.
     * @return A 2-d byte array of alma values
     * @throws InsufficientDataException    Thrown if no data is stored
     */
    public byte[][] getAlmaValuesPol1() throws InsufficientDataException{
        if (almaValuesPol1 == null)
            try{
                calcPols();
            } catch (InsufficientDataException eeek){
                throw (eeek);
            }
        return java.util.Arrays.copyOf(almaValuesPol1, almaValuesPol1.length);
    }
    
    /**
     * This function returns the alma values captured for BBPr0.
     * They will be stored in order, in a 1-d byte array of the proper size.
     * The alma values range from -4 to +3.
     * @return A 2-d byte array of alma values
     * @throws InsufficientDataException    Thrown if no data is stored
     */
        public byte[] getAlmaValues1DPol1() throws InsufficientDataException{
            if (almaValuesPol1 == null)
                try{
                calcPols();
                } catch (InsufficientDataException eeek){
                    throw (eeek);
                }
            byte[] returnValue = new byte[almaValuesPol1.length*16];
            for (int c=0; c<almaValuesPol1.length; c++)
                for (int q=0; q<16; q++)
                    returnValue[c*16+q]=almaValuesPol1[c][q];
            return  returnValue;
        }
    
    /**
     * @return The entire array of raw data for Ch B, lower bits
     * @throws InsufficientDataException 
     */
    public Long[] getRawDataBLower() throws InsufficientDataException{
        checkRawData();
        Long[] a = (Long[]) rawDataBLower.toArray(new Long[rawDataBLower.size()]);
        return (Long[])a;
    }
    /**
     * @param index
     * @return the raw data from Ch B, lower bits, at [index]
     * @throws InsufficientDataException 
     */
    public long getRawDataBLower(int index) throws InsufficientDataException{
        checkRawData(index);
        return rawDataBLower.get(index);
    }
    
    /**
     * @return The entire array of raw data for Ch B, upper bits
     * @throws InsufficientDataException 
     */
    public Long[] getRawDataBUpper() throws InsufficientDataException{
        checkRawData();
        return (Long[]) rawDataBUpper.toArray();
    }
    /**
     * @param index
     * @return the raw data from Ch B, upper bits, at [index]
     * @throws InsufficientDataException 
     */
    public long getRawDataBUpper(int index) throws InsufficientDataException{
        checkRawData(index);
        return rawDataBUpper.get(index);
    }
    
    /**
     * @return The entire array of raw data for Ch C, lower bits
     * @throws InsufficientDataException 
     */
    public Long[] getRawDataCLower() throws InsufficientDataException{
        checkRawData();
        return (Long[]) rawDataCLower.toArray();
    }
    /**
     * @param index
     * @return the raw data from Ch C, lower bits, at [index]
     * @throws InsufficientDataException 
     */
    public long getRawDataCLower(int index) throws InsufficientDataException{
        checkRawData(index);
        return rawDataCLower.get(index);
    }
    
    /**
     * @return The entire array of raw data for Ch C, upper bits
     * @throws InsufficientDataException 
     */
    public Long[] getRawDataCUpper() throws InsufficientDataException{
        checkRawData();
        return (Long[]) rawDataCUpper.toArray();
    }
    /**
     * @param index
     * @return the raw data from Ch C, upper bits, at [index]
     */
    public long getRawDataCUpper(int index) throws InsufficientDataException{
        checkRawData(index);
        return rawDataCUpper.get(index);
    }
    
    
    /**
     * @return The entire array of raw data for Ch D, lower bits
     * @throws InsufficientDataException 
     */
    public Long[] getRawDataDLower() throws InsufficientDataException{
        checkRawData();
        return (Long[]) rawDataDLower.toArray();
    }
    /**
     * @param index
     * @return the raw data from Ch D, lower bits, at [index]
     * @throws InsufficientDataException 
     */
    public long getRawDataDLower(int index) throws InsufficientDataException{
        checkRawData(index);
        return rawDataDLower.get(index);
    }
    /**
     * @return The entire array of raw data for Ch D, upper bits
     * @throws InsufficientDataException 
     */
    public Long[] getRawDataDUpper() throws InsufficientDataException{
        checkRawData();
        return (Long[]) rawDataDUpper.toArray();
    }
    /**
     * @param index
     * @return the raw data from Ch D, upper bits, at [index]
     * @throws InsufficientDataException 
     */
    public long getRawDataDUpper(int index) throws InsufficientDataException{
        checkRawData(index);
        return rawDataDUpper.get(index);
    }
    
    protected byte[][] convertSingleValue(long dBits, long cBits, long bBits){
        byte[][] returnBytes = new byte[4][16];
        for (int c=0; c<16; c++)
            returnBytes[0][c] = valueFromGrayCode(getBit(dBits, c), getBit(cBits, c), getBit(bBits, c));
        for (int c=16; c<32; c++)
            returnBytes[1][c%16] = valueFromGrayCode(getBit(dBits, c), getBit(cBits, c), getBit(bBits, c));
        for (int c=32; c<48; c++)
            returnBytes[2][c%16] = valueFromGrayCode(getBit(dBits, c), getBit(cBits, c), getBit(bBits, c));
        for (int c=48; c<64; c++)
            returnBytes[3][c%16] = valueFromGrayCode(getBit(dBits, c), getBit(cBits, c), getBit(bBits, c));
        return returnBytes;
    }

    /**
     * Return just the requested bit from the given int. true=1, false=0.
     * This class is for internal use only. It is protected because JUnit tests need to access it.
     * @param aLong
     * @param bitToGet  What bit you want to get. Should be between 0 and 31.
     * @return
     * 
     * TODO: Should we do a <0 or >63 check?
     */
    protected boolean getBit(long aLong, int bitToGet){
        //We reeeeeeeeeeeeeeeealy want 1L here, otherwise it uses an int and goofs everything up!
        aLong = aLong & (1L<<bitToGet);
        if (aLong != 0)
            return true;
        else
            return false;
    }

    /**
     * Converts a set of alma gray code bits into an alma value.
     * Uses conversion defined in BEND-50.00.00.00-306-A-MAN
     * @param chD
     * @param chC
     * @param chB
     * @return
     */
    protected byte valueFromGrayCode(boolean chD, boolean chC, boolean chB){
        if (!chD && !chC && !chB)
            return -4;
        if (!chD && !chC && chB)
            return -3;
        if (!chD && chC && chB)
            return -2;
        if (!chD && chC && !chB)
            return -1;
        if (chD && chC && !chB)
            return 0;
        if (chD && chC && chB)
            return 1;
        if (chD && !chC && chB)
            return 2;
        return 3;
    }

    /**
     * This function is used to split the data into 2 polarities.
     * It should be called only once the data arrays are filled.
     * @throws InsufficientDataException Thrown is there is no data.
     */
    private void calcPols() throws InsufficientDataException{
        //The addRawData function adds equally to all data sets, so they should always be the same size.
        //TODO: Is 16 really the minimum number of samples?
        if (rawDataBLower == null || rawDataBLower.size()<1)
            throw new InsufficientDataException("DataPacket, calcPols: there is not enough data to" +
                                                "calculate the Pol1. A minimum of 16 sample sets is" +
                                                "required.");
        int length = rawDataBLower.size();
        almaValuesPol0 = new byte[length*4][16];
        almaValuesPol1 = new byte[length*4][16];
        byte newData[][];
        for (int c=0; c<length; c++){
            newData=convertSingleValue(rawDataDLower.get(c), rawDataCLower.get(c), rawDataBLower.get(c));
            almaValuesPol0[c*4] = java.util.Arrays.copyOf(newData[0], 16);
            almaValuesPol1[c*4] = java.util.Arrays.copyOf(newData[1], 16);
            almaValuesPol0[c*4+1] = java.util.Arrays.copyOf(newData[2], 16);
            almaValuesPol1[c*4+1] = java.util.Arrays.copyOf(newData[3], 16);
            newData=convertSingleValue(rawDataDUpper.get(c), rawDataCUpper.get(c), rawDataBUpper.get(c));
            almaValuesPol0[c*4+2] = java.util.Arrays.copyOf(newData[0], 16);
            almaValuesPol1[c*4+2] = java.util.Arrays.copyOf(newData[1], 16);
            almaValuesPol0[c*4+3] = java.util.Arrays.copyOf(newData[2], 16);
            almaValuesPol1[c*4+3] = java.util.Arrays.copyOf(newData[3], 16);
        }
    }

    /**
     * Used to make sure that we actually have raw data at the given index.
     * @param index
     * @throws InsufficientDataException
     */
    private void checkRawData(int index) throws InsufficientDataException{
        checkRawData();
        if (rawDataBUpper.size()<=index | index<0){
            throw new InsufficientDataException("Index out of range.");
        }
        if (rawDataBLower.size()<=index){
            throw new InsufficientDataException("Index out of range.");
        }
        if (rawDataCLower.size()<=index){
            throw new InsufficientDataException("Index out of range.");
        }
        if (rawDataCUpper.size()<=index){
            throw new InsufficientDataException("Index out of range.");
        }
        if (rawDataDLower.size()<=index){
            throw new InsufficientDataException("Index out of range.");
        }
        if (rawDataDUpper.size()<=index){
            throw new InsufficientDataException("Index out of range.");
        }
    }
    
    /**
     * Used to make sure that we have some data before attempting to return it.
     * @throws InsufficientDataException
     */
    private void checkRawData() throws InsufficientDataException{
        if (rawDataBLower.size()==0 || rawDataBLower==null){
            throw new InsufficientDataException("No stored data to return.");
        }
        if (rawDataBUpper.size()==0 || rawDataBUpper==null){
            throw new InsufficientDataException("No stored data to return.");
        }
        if (rawDataCLower.size()==0 || rawDataCLower==null){
            throw new InsufficientDataException("No stored data to return.");
        }
        if (rawDataCUpper.size()==0 || rawDataCUpper==null){
            throw new InsufficientDataException("No stored data to return.");
        }
        if (rawDataDLower.size()==0 || rawDataDLower==null){
            throw new InsufficientDataException("No stored data to return.");
        }
        if (rawDataDUpper.size()==0 || rawDataDUpper==null){
            throw new InsufficientDataException("No stored data to return.");
        }
    }
    
    //These will contain the alma state values for each base band pair.
    //There are 16 data channels in the DTX, and 32 in the DRX. The only time we
    //care about the channels is in the DTX when tuning the digitizer. So we store
    //the data in arrays of 16 channels.
    private byte[][] almaValuesPol0;
    private byte[][] almaValuesPol1;
    
    //The raw data as captured from the device.
    //Comes in 2 groups of 64 bits per channel.
    //This data is unsigned, but since we only look at the individual bits
    //it does not matter that type long is singed.
    private ArrayList<Long> rawDataBLower;
    private ArrayList<Long> rawDataBUpper;
    private ArrayList<Long> rawDataCLower;
    private ArrayList<Long> rawDataCUpper;
    private ArrayList<Long> rawDataDLower;
    private ArrayList<Long> rawDataDUpper;
}
