/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;

import java.util.logging.Logger;

/**
 * <code>RangeOkIndicatorWidget</code> displays two user defined images to indicate
 * true or false values from its associated monitor point.
 * 
 * @author  David Hunter  dhunter@nrao.edu
 * @author  Scott Rankin  srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 5.0.4
 * 
 * TODO: Document
 * TODO: Factor out common behavior with other widgets to a superclass.
 */
public class RangeOkIndicatorWidget extends MonitorPointWidget {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private Status falseStatus;
    private String falseToolTipText;
    private StatusIndicator statusIndicator;
    private Status trueStatus;
    private String trueToolTipText;
    private IMonitorPoint monitorPoint;

    /**
     * <code>RangeOkIndicatorWidget</code> is the constructor.
     * 
     * @param logger logger to be used for logging
     * @param model monitor point to listen for
     * @param falseStatus status to be used to indicate a false value read from the monitor point
     * @param falseToolTipText tool tip text to be used to indicate a false value was read
     * from the monitor point
     * @param trueStatus status to be used to indicate a true value was read from the monitor point
     * @param trueToolTipText tool tip text to be used to indicate a true value was read
     * from the monitor point
     */
    public RangeOkIndicatorWidget (
            Logger logger,
            MonitorPointPresentationModel<Boolean> monitorPointModel,
            Status falseStatus,
            String falseToolTipText,
            Status trueStatus,
            String trueToolTipText
    ) {
        super(logger, true);
        this.falseStatus = falseStatus;
        this.falseToolTipText = falseToolTipText;
        this.trueStatus = trueStatus;
        this.trueToolTipText = trueToolTipText;
        monitorPoint = addMonitorPointPM(monitorPointModel, Boolean.FALSE);
        buildWidget();
    }

    /**
     * buildWidget is a private method which is used to construct the entire widget
     * to be displayed on a panel
     */
    private void buildWidget() {		
        statusIndicator = new StatusIndicator(falseStatus, falseToolTipText);
        add(statusIndicator);
        addPopupMenuToComponent(statusIndicator);
    }

    @Override
    //Set the widget display to reflect the new range status
    protected void updateAttention(Boolean value) {
        if (value) {
            statusIndicator.setStatus(falseStatus, falseToolTipText);
        } else {
            statusIndicator.setStatus(trueStatus, trueToolTipText);
        }
    }

    @Override
    protected void updateValue(IMonitorPoint aMonitorPoint) {
        //Nothing to do here since we only care about the range
    }  
}

//
// O_o

