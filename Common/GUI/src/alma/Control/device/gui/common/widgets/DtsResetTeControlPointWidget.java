/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.ControlPointPresentationModel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

/**
 * <code>DtsResetTeControlPointWidget.java</code> presents the user with an interface for using the 
 * RESET_TE_ERRS on the DRX and DTX. This control provides access to the 3 features given in 
 * that control point: Reset the registers, Select the clock edge, and Resync the TE.
 * 
 * @author  David Hunter  dhunter@nrao.edu
 * @author  Scott Rankin  srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 7.0.0
 * 
 * TODO: Document
 * TODO: Factor out common behavior with other widgets to a superclass.
 */

public class DtsResetTeControlPointWidget extends JPanel implements ActionListener {
    
    /**Construct a new DtsResetTeControlPointWidget
     * 
     * @param logger the logger that all logs should be directed to.
     * @param controlPointModel The control point for RESET_TE_ERRS.
     */
    public DtsResetTeControlPointWidget( Logger logger,
            ControlPointPresentationModel<Integer> controlPointModel) {
        this.logger = logger;
        this.controlPointModel = controlPointModel;
        changeTeButton=new JButton(buttonLabel);                               //Setup the Change TE button
        changeTeButton.addActionListener(this);
        controlPointModel.getContainingDevicePM().addControl(changeTeButton);
        
        resyncTeSwitch=new JCheckBox("Resync TE");  //Setup the reset and resync check boxes
        controlPointModel.getContainingDevicePM().addControl(resyncTeSwitch);
        resetRegistersSwitch=new JCheckBox("Reset Registers");
        controlPointModel.getContainingDevicePM().addControl(resetRegistersSwitch);
        
        optionMenu = new JComboBox();                              //Setup the optionMenu
        optionMenu.setEditable(false);
        for (String s: optionList) optionMenu.addItem(s);
        controlPointModel.getContainingDevicePM().addControl(optionMenu);
        buildWidget();
    }
    
    /** When an actionEvent occurs (e.g the control button is pressed) check the value in the
     *  menu. Find the corresponding code and send it to the control point.
     */
    public void actionPerformed(ActionEvent event) {
        int count=0;
        int optionCode=0;
        int resyncTeCode=0;
        int resetCode=0;
        int outCode;
        
        String newValue=(String)optionMenu.getSelectedItem();
        try{
            while (newValue != optionList[count])
                count++;
            optionCode=(optionCodes[count]);
        }
        catch (ArrayIndexOutOfBoundsException Eeeek){
            logger.severe("ERROR: Lower bit text given by user not found in optionList!");
        }        
        
        if (resyncTeSwitch.isSelected())
            resyncTeCode=0x4;
        if (resetRegistersSwitch.isSelected())
            resetCode=0x1;
        
        outCode=optionCode | resyncTeCode | resetCode;
        controlPointModel.setValue(outCode);
    }
    
    /** Just add the option menu and control button to this panel.
     * @param buttonText The text to put on the control button.
     */
    private void buildWidget() {
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Reset TE"),
                BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        c.weightx=100;
        c.weighty=100;
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth=3;
        add(optionMenu, c);
        c.gridx+=3;
        c.gridwidth=1;
        add(resyncTeSwitch, c);
        c.gridy++;
        c.gridx=0;
        c.gridwidth=2;
        add(changeTeButton, c);
        c.gridx+=2;
        add(resetRegistersSwitch, c);
    }
    
    private static final long serialVersionUID = 1L;
    private String buttonLabel="Change TE";
    private JButton changeTeButton;
    private ControlPointPresentationModel<Integer> controlPointModel;
    private Logger logger;
    private int[] optionCodes={0x0, 0x2};
    private String[] optionList={"Clock to positive edge", "Clock to negative edge"};
    private JComboBox optionMenu;
    private JCheckBox resetRegistersSwitch;
    private JCheckBox resyncTeSwitch;
}
