/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.ControlPointPresentationModel;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JLabel;

/**
 * <code>BooleanControlPointWidget.java</code> is used to send a single pre-set boolean value to
 * a control point.
 * 
 * @author  Scott Rankin  srankin@nrao.edu
 * @author  David Hunter  dhunter@nrao.edu
 * @version $Id$
 * @since   ALMA 5.0.4
 * 
 * TODO: Document
 * TODO: Factor out common behavior with other widgets to a superclass.
 */

public class BooleanControlPointWidget extends JButton implements ActionListener {
    
    /**
     * Build a new boolean control point widget. This widget has just one button.
     * Since no control value is given a value will not be sent to the control point
     * when the button is pressed, but the control point method will be invoked with
     * no parameters. This is should be used on control points that do not require or
     * accept any parameters, such as some reset points.
     * @param l The logger to send all logs to.
     * @param cpPM The control point to attach the widget to.
     * @param bText The test to put on the button.
     */
    public BooleanControlPointWidget(
            Logger l, 
            ControlPointPresentationModel<Boolean> cpPM,
            String bText) {
        this(l, cpPM, bText, false);
        isThereAValue = false;
    }
    
    /**
     * Build a new boolean control point widget. This widget has just one button.
     * The given control value will be sent to the control point when the button is
     * pressed. If you are dealing with a control point that does not accept any
     * parameters then remove controlValue; this will cause the widget not to send
     * a value.
     * @param logger The logger to send all logs to.
     * @param controlPointModel The control point to attach the widget to.
     * @param buttonText The test to put on the button.
     * @param controlValue The boolean value to send to the control point.
     *        true for 1, false for 0.
     */
    public BooleanControlPointWidget(
            Logger l,
            ControlPointPresentationModel<Boolean> cpPM,
            String bText, 
            boolean cValue) {
        this.logger = l;
        this.controlPointModel = cpPM;
        this.controlValue = cValue;
        this.isThereAValue = true;
        controlPointModel.getContainingDevicePM().addControl(this);
        addActionListener(this);
        buildWidget(bText);
    }
    
    /**
     * Build a new boolean control point widget. This widget has just one button.
     * The given control value will be sent to the control point when the button is
     * pressed. If you are dealing with a control point that does not accept any
     * parameters then remove controlValue; this will cause the widget not to send
     * a value.
     * @param logger The logger to send all logs to.
     * @param controlPointModel The control point to attach the widget to.
     * @param buttonText The test to put on the button.
     * @param controlValue The byte value to send to the control point. The control will
     *        always send this same value.
     *
     * A few control points use this value, for example DG_250MHZ_DELAY
     */
    public BooleanControlPointWidget(
            Logger l,
            ControlPointPresentationModel<Integer> cpPM,
            String bText, 
            int cValue) {
        this.logger = l;
        this.intControlPointModel = cpPM;
        this.intControlValue = cValue;
        this.isInt = true;
        intControlPointModel.getContainingDevicePM().addControl(this);
        addActionListener(this);
        buildWidget(bText);
    }

    public void actionPerformed(ActionEvent event) {
        if (isThereAValue) {
            controlPointModel.setValue(controlValue);
        } else if (isInt){
            intControlPointModel.setValue(intControlValue);
        }
        else{
            controlPointModel.setValue();
        }
    }
    
    public void buildWidget(String buttonText) {
        setText(buttonText);
        //This minimizes the button size (while not squeezing in the text too much).
        setMargin(new Insets(0, 2, 0, 2)); 
    }


    private ControlPointPresentationModel<Boolean> controlPointModel;
    private ControlPointPresentationModel<Integer> intControlPointModel;
    private int intControlValue;
    private boolean controlValue;
    private boolean isThereAValue;
    private boolean isInt=false;
    private Logger logger;
    private static final int HEIGHT = 20;
    private static final long serialVersionUID = 1L;
}
