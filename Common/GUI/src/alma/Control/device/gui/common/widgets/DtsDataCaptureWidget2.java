/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2011
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.DevicePresentationModel;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingWorker;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * {@link DtsDataCaptureWidget2} provides a widget that reads from the DTX, DRX
 * and DTSR interface using the GET_DFR_PAYLOAD as specified in the ICD for said
 * devices. This is new version of {@link DtsDataCaptureWidget} to be able to be
 * shared among DTX, DRX, DTSR.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Kohji Nakamura k.nakamura@nao.ac.jp
 * @version $Id$
 * @since ALMA 8.1.0
 */

public class DtsDataCaptureWidget2<T> extends JPanel {
	/**
	 * Bit identity.
	 */
	public static enum Bit {
		D, C, B;
	}

	/**
	 * This interface provides an abstract interface to handle payload data.
	 */
	public static interface IPayloadHandler<T> {
		public String format(T payload, Bit bit);

		/**
		 * Capture the requested amount of data from the device and put it in
		 * the provided {@code capturedData}. If not all the requested data can
		 * be captured the return value will the percent that actually was
		 * captured. The partial set of data will be in the {@code capturedData}
		 * , and can be used.
		 * 
		 * Proper managing of the CAN bus and checking of the FIFO is the job of
		 * the implementing class.
		 * 
		 * @param capturedData
		 *            the DataPacket to put all the captured data in.
		 * @param dataRequested
		 *            the number of captures to do.
		 * @return the percentage of the requested data that was actually
		 *         captured.
		 */
		public int captureData(List<T> capturedData, int dataRequested);

		/**
		 * This allows the user to provide a progress bar. If one is given, it
		 * will be updated with the % complete when
		 * {@link #captureData(List, int)} is running. If a new one is given it
		 * will replace the old one. If null is given then no progress bar will
		 * be used. Note that when the constructor runs the progress bar
		 * defaults to none.
		 * 
		 * The progress bar will be updated via EventQueue.invokeLater, so it
		 * should be thread safe.
		 * 
		 * @param newProgressBar
		 *            The progress bar to update when data is being captures.
		 */
		public void setProgressBar(JProgressBar newProgressBar);

		/**
		 * The process of capturing data can take a long time, so there is a
		 * possibility that it will need to be aborted. Upon call of the
		 * shutdown command the {@link IPayloadHandler} will promptly cease data
		 * capture, clean up, and return the data that it already has.
		 * 
		 * Note: sending the shutdown command to a {@link IPayloadHandler} that
		 * is not currently grabbing data is guaranteed not to be harmful.
		 * 
		 * Caution: This command does not guaranty that no further data capture
		 * transactions will occur, meaning that a few more CAN transactions may
		 * be sent. The number of transactions sent should be less than 10.
		 */
		public void shutdown();
	}

	/**
	 * Creates a new DTS data capture widget.
	 * 
	 * @param logger
	 * @param dPM
	 *            The presentation model for the device.
	 * @param dataSource
	 *            A ready to go DTS DataSource
	 */
	public DtsDataCaptureWidget2(Logger l, DevicePresentationModel dPM,
			IPayloadHandler<T> dataSource) {
		logger = l;
		output = new JLabel[Bit.values().length];
		// Setup D bit display
		output[Bit.D.ordinal()] = new JLabel("<No data>");
		output[Bit.D.ordinal()].setPreferredSize(new Dimension(320, 14));

		// Setup C bit display
		output[Bit.C.ordinal()] = new JLabel("<No Data>");
		output[Bit.C.ordinal()].setPreferredSize(new Dimension(320, 14));

		// Setup B bit display
		output[Bit.B.ordinal()] = new JLabel("<No Data>");
		output[Bit.B.ordinal()].setPreferredSize(new Dimension(320, 14));

		// Setup the rest.
		payloadHandler = dataSource;
		devicePresentationModel = dPM;
		captureDataButton = new JButton(captureText);
		captureDataButton.addActionListener(new captureDataAction());
		devicePresentationModel.addControl(captureDataButton);
		sampleSize = new JSpinner(new SpinnerNumberModel(3, 1, 4096, 1));
		devicePresentationModel.addControl(sampleSize);
		sampleNumber = new JSpinner(new SpinnerNumberModel(1, 0, 4096, 1));
		sampleNumber.setEnabled(false);
		sampleNumber.addChangeListener(new updateDisplayAction());
		// This button is not added to the disable controls stuff because it
		// does not go to a control point.
		// devicePresentationModel.addControl(sampleNumber);
		captureProgressBar = new JProgressBar(0, 100);
		captureProgressBar.setEnabled(false);
		captureProgressBar.setStringPainted(true);
		captureProgressBar.setMaximumSize(new Dimension(320, 20));
		captureStatusReport = new JLabel("Ready to capture...");
		payloadHandler.setProgressBar(captureProgressBar);
		devicePresentationModel.addShutdownAction(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				payloadHandler.shutdown();
			}
		});
		buildWidget();
	}

	/*
	 * This function sets up the widget panel. It should only be called by the
	 * constructor.
	 */
	protected void buildWidget() {
		this.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder("Capture Payload Data"),
				BorderFactory.createEmptyBorder(1, 1, 1, 1)));
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		// Setup the readouts
		c.gridy = 0;
		c.gridx = 1;
		c.weightx = 100;
		c.weighty = 0;
		c.fill = GridBagConstraints.BOTH;
		addLeftLabel("D Bits: ", c);
		add(output[Bit.D.ordinal()], c);
		c.gridy++;
		addLeftLabel("C Bits: ", c);
		add(output[Bit.C.ordinal()], c);
		c.gridy++;
		addLeftLabel("B Bits: ", c);
		add(output[Bit.B.ordinal()], c);
		c.gridy++;
		// Setup the controls
		c.weightx = 0;
		c.weighty = 0;
		c.fill = GridBagConstraints.NONE;
		add(new JLabel("View Sample #"), c);
		c.gridy++;
		add(sampleNumber, c);
		c.gridy++;
		add(new JLabel("# of samples"), c);
		c.gridy++;
		add(sampleSize, c);
		c.gridy++;
		add(captureDataButton, c);
		c.gridy++;
		c.fill = GridBagConstraints.BOTH;
		add(captureProgressBar, c);
		c.gridy++;
		add(captureStatusReport, c);
	}

	// A simple helper function.
	private void addLeftLabel(String s, GridBagConstraints c) {
		c.gridx--;
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.EAST;
		add(new JLabel(s), c);
		c.anchor = GridBagConstraints.WEST;
		c.fill = GridBagConstraints.BOTH;
		c.gridx++;
	}

	/**
	 * Just writes the output strings to the displays. Should be called after
	 * captureData, logs an error if it is not.
	 */

	private void updateDisplay() {
		// Updating the display during a capture can cause all sorts of trouble,
		// so we make sure it does not happen.
		if (!captureLock) {
			int index = (Integer) sampleNumber.getValue();
			try {
				for (Bit bit : Bit.values()) {
					output[bit.ordinal()].setText(payloadHandler.format(
							theData.get(index), bit));
				}
			} catch (IndexOutOfBoundsException e) {
				logger.severe(this.getClass().getName() + ", updateDisplay:"
						+ "Error: InsurricientDataException: %s"
						+ e.getMessage());
				for (Bit bit : Bit.values()) {
					output[bit.ordinal()].setText("Error! Please try again.");
				}
			}
		}
	}

	/**
	 * The action listener for the Capture Data button. Spawns a SwingWorker
	 * thread that will go and capture the data. Being a slow process, we don't
	 * want to do this on the swing update thread...
	 */
	private class captureDataAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (!captureLock) {
				captureDataButton.setText(stopText);
				if ((Integer) (sampleNumber.getValue()) >= ((Integer) sampleSize
						.getValue()))
					sampleNumber.setValue(Integer.valueOf(0));
				SwingWorker<Object, Object> myWorker = new captureDataWorker();
				myWorker.execute();
			} else {
				captureCanceled = true;
				payloadHandler.shutdown();
			}
		}
	}

	/**
	 * A swing worker thread to capture the data. Sets up the progress bar, then
	 * calls captureData(). When the data is captured it then writes the data to
	 * the display. It also keeps the report string up to date.
	 */
	private class captureDataWorker extends SwingWorker<Object, Object> {
		@Override
		protected Object doInBackground() throws Exception {
			captureLock = true;
			captureCanceled = false;
			final int size = (Integer) sampleSize.getValue();
			EventQueue.invokeLater(new Runnable() {
				@Override
				public void run() {
					captureProgressBar.setEnabled(true);
					captureProgressBar.setValue(0);
					captureStatusReport.setText("Capturing data...");
				}
			});
			theData = new ArrayList<T>();
			final int Q = payloadHandler.captureData(theData, size);
			final int precentFinished = Q;
			EventQueue.invokeLater(new Runnable() {
				@Override
				public void run() {
					captureProgressBar.setValue(precentFinished);
					try {
						((SpinnerNumberModel) sampleNumber.getModel())
								.setMaximum(theData.size() - 1);
					} catch (IndexOutOfBoundsException e) {
						logger.severe(this.getClass().getName()
								+ ", captureDataWorker, doInBackground:"
								+ "Error: InsurricientDataException: %s"
								+ e.getMessage());
						((SpinnerNumberModel) sampleNumber.getModel())
								.setMaximum(0);
					}
					sampleNumber.setEnabled(true);
					if (precentFinished < 100)
						if (captureCanceled)
							captureStatusReport.setText("Capture Canceled...");
						else
							captureStatusReport.setText("Fifo Empty...");
					else
						captureStatusReport.setText("Done!");
					captureProgressBar.setEnabled(false);
					captureLock = false;
					updateDisplay();
				}
			});
			captureDataButton.setText(captureText);
			return null;
		}
	}

	private class updateDisplayAction implements ChangeListener {
		@Override
		public void stateChanged(ChangeEvent e) {
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					updateDisplay();
				}
			});
		}
	}

	// All sorts of variables, alphabetized by last name.
	private JButton captureDataButton;
	private JProgressBar captureProgressBar;
	private boolean captureLock = false;
	private JLabel captureStatusReport;
	private static final String captureText = "Capture Data";
	private boolean captureCanceled;
	private IPayloadHandler<T> payloadHandler;
	DevicePresentationModel devicePresentationModel; // We need this for the
														// enable/disable button
														// to work.
	private JLabel[] output;
	private JSpinner sampleNumber;
	private JSpinner sampleSize;
	private List<T> theData;
	private static final String stopText = "Cancel";
	private final Logger logger;
	// TODO - serial version UID
	private static final long serialVersionUID = 1L;
}

//
// O_o
