/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;

/**
 * Common behavior and properties for all Monitor Point Presentation Models.
 * 
 * @author  Scott Rankin  srankin@nrao.edu   
 * @version $Id$
 * @since   ALMA 5.1.1
 */
public abstract class MonitorPointPresentationModel<T> extends PresentationModel<T> {

    /**
     * All Presentation Models need to know which logger to use and which device contains this model.
     * All Monitor Point presentation models need to know which monitor point they model. 
     * 
     * @param aLogger the logger to log to.
     * @param aDevicePM the device presentation model containing this monitor point. 
     * @param aMP the monitor point this presentation model models.
     */
    public MonitorPointPresentationModel(Logger aLogger,
            DevicePresentationModel aDevicePM, IMonitorPoint aMP) {
        super(aLogger, aDevicePM);
        this.monitorPoint = aMP;
        this.listeners = new ArrayList<PresentationModelChangeListener>();
        createCommonEvents();
    }

    /**
     * Presentation models notify other objects of changes using events.
     * 
     * @param listener an object to notify when this model changes.
     */
    public void addListener(PresentationModelChangeListener listener) {
        listeners.add(listener);
    }

    /**
     * Override this method with the implementation to read a new value from a hardware device.
     */
    public abstract void checkDeviceForUpdates();

    /**
     * Report if monitor point is being polled fast or slow.
     * 
     * @return true for fast, false for slow.
     */
    public boolean fastPolling() {
        return fastPolling;
    }

    /**
     * Get the monitor point this model models.
     * 
     * @return the monitor point.
     */
    public IMonitorPoint getMonitorPoint() {
        return monitorPoint;
    }

    /**
     * Get the units for the value in this model.
     * 
     * @return the monitor point units.
     */
    public String getUnits() {
        return monitorPoint.getUnits();
    }
    
    /**
     * If it is in the list, remove the given change listener from the listener list.
     * Should be used when a widget is removed but the entire panel is not.
     * E.g. In the graphing sub-system.
     * @param listener
     */
    public void removeListener(PresentationModelChangeListener listener){
        listeners.remove(listener);
    }

    /**
     * Set the monitor point polling rate to fast or slow.
     * 
     * @param fast true indicates fast polling. False indicates slow polling.
     */
    public void setFastPolling(boolean fast) {
        fastPolling = fast;
        if (fastPolling)
            containingDevicePM.moveMonitorPointToFastGroup(monitorPoint);
        else
            containingDevicePM.moveMonitorPointToSlowGroup(monitorPoint);
    }

    /**
     * Create events that this PresentationModel will send later.
     * TODO: is setting the timestamp to 0 here OK?
     */
    protected void createCommonEvents() {
        this.commAttention = new PresentationModelChangeEvent<Boolean>(
                PresentationModelChangeEvents.COMMUNICATION_CHANGE, Boolean.TRUE,
                this.monitorPoint);
        this.commOK = new PresentationModelChangeEvent<Boolean>(
                PresentationModelChangeEvents.COMMUNICATION_CHANGE, Boolean.FALSE,
                this.monitorPoint);
        this.rangeAttention = new PresentationModelChangeEvent<Boolean>(
                PresentationModelChangeEvents.ATTENTION_CHANGE, Boolean.TRUE,
                this.monitorPoint);
        this.rangeOK = new PresentationModelChangeEvent<Boolean>(
                PresentationModelChangeEvents.ATTENTION_CHANGE, Boolean.FALSE,
                this.monitorPoint);
    }

    /**
     * Create a new MonitorPointPresentationModelChangeEvent.
     * 
     * @param eventType the type of change that has occurred
     * @param eventValue any value associated with the event
     * @return a new MonitorPointPresentationModelChangeEvent
     */
    @SuppressWarnings("unchecked")
    // PresentationModelChangeEvent can not be parameterized at this point.
    protected PresentationModelChangeEvent newChangeEvent(PresentationModelChangeEvents eventType,
                                                          T eventValue, Date timeStamp)
    {
        return new PresentationModelChangeEvent(eventType, eventValue, timeStamp, this.monitorPoint);
    }

    /*
     * Presentation models notify other objects of changes using events.
     */
    @SuppressWarnings("unchecked")
    // PresentationModelChangeEvent can not be parameterized at this point.
    protected void notifyListeners(ArrayList<PresentationModelChangeEvent> events) {
        if (null != listeners) {
            for (PresentationModelChangeListener listener : listeners) {
                for (PresentationModelChangeEvent event : events) {
                    listener.handlePresentationModelChangeEvent(event);
                }
            }
        }
    }

    /**
     * At runtime, find the method to call on a Hardware Device to read data 
     * from a monitor point or write data to a control point. 
     */
    @Override
    protected abstract void setUpdateMethod();

    protected boolean fastPolling;
    protected IMonitorPoint monitorPoint;

    // The following events may be sent by all monitor points presentation
    // models.
    protected PresentationModelChangeEvent<Boolean> commAttention;
    protected PresentationModelChangeEvent<Boolean> commOK;
    protected PresentationModelChangeEvent<Boolean> rangeAttention;
    protected PresentationModelChangeEvent<Boolean> rangeOK;
    
    private ArrayList<PresentationModelChangeListener> listeners;
}

//
// O_o
