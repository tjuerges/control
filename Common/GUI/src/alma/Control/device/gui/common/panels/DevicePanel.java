/* ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.panels;

/**
 * An Superclass for GUI panels used in OMC panels for code generated devices.
 * 
 * @author  Scott Rankin  srankin@nrao.edu  
 * @version $Id$
 * @since   5.x 
 */

import alma.Control.device.gui.common.DevicePresentationModel;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public abstract class DevicePanel extends JPanel {
	
    /**
     * Create an instance of a DevicePanel.
     * 
     * @param aLogger the logger we log to
     * @param aDevicePM the device presentation model for this panel. 
     */
    public DevicePanel(Logger aLogger, DevicePresentationModel aDevicePM) {
        this.logger = aLogger;
        this.devicePM = aDevicePM;
    }

    /**
     * Create an instance of a DevicePanel.
     * 
     * TODO: Remove this method. 
     * This method must remain while we remove the <device>Panel classes from all devices.
     * 
     * @param aLogger the logger we log to
     * 
     * @deprecated - Replaced by two arg constructor.
     */
    public DevicePanel(Logger aLogger) {
        this.logger = aLogger;
    }
    
    /**
     * Enable or disable all buttons in this panel and it's children.
     * 
     * @param enabled True to enable, false to disable.
     */
    public void setButtonsEnabled(boolean enabled) 
    {
    	for (Component comp: this.getComponents()) {
    		if (comp instanceof DevicePanel) {
    			((DevicePanel) comp).setButtonsEnabled(enabled);    			
    		}
    		if (comp instanceof JButton) {
    			comp.setEnabled(enabled);    			
    		}
    	}
    }
    
    /*
     * 2 simple helper functions that allow you to add a label to the left or right
     * of the current location with grid bag layout.
     */
    protected void addLeftLabel(String s, GridBagConstraints c) {
        c.gridx--;
        add(new JLabel(s), c);
        c.gridx++;
    }

    protected void addRightLabel(String s, GridBagConstraints c) {
        c.gridx++;
        add(new JLabel(s), c);
        c.gridx--;
    }
    
    /**
     * A variant of addLeftLabel that sets the label to stick to the right-hand
     * side of the area assigned to it. c.anchor will be changed and then restored to its
     * original configuration.
     * @param label The string to use for making the label
     * @param c The GridBagConstraints to position the label.
     */
    protected void addLeftEastLabel(String label, GridBagConstraints c){
        int q = c.anchor;
        c.anchor=GridBagConstraints.EAST;
        addLeftLabel(label, c);
        c.anchor=q;
    }

    /**
     * Build and draw this panel.
     * 
     * This must be called at the END of the subclass constructor method for non-abstract subclasses.  This 
     * is necessary because some subclasses must create objects in their own constructors.
     */
    protected abstract void buildPanel();
    
    protected DevicePresentationModel devicePM = null;
    protected Logger logger;

    // TODO: serial version UID
    private static final long serialVersionUID = 1L;
}
