/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common;

/** 
 * An interface to be implemented by any class that will be polled using the PollingManager class.
 * 
 * @deprecated  Replaced by alma.Control.CommonGui.timers.PeriodicTimerManager.
 * 
 * @author  Scott Rankin  srankin@nrao.edu
 * @author  Steve Harrington  sharring@nrao.edu
 * @version $Id$
 * @since   ALMA 5.0.4
 */
public interface Pollable {
	
	/**
	 * Poll all fast monitor points as required for this device.
	 */
	public void pollFastMonitorPoints();

	/**
	 * Poll all slow monitor points as required for this device.
	 */
	public void pollSlowMonitorPoints();

}

//
// O_o
