/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common;

import alma.Control.HardwareDevice;
import alma.Control.HardwareDevicePackage.HwState;
import alma.exec.extension.subsystemplugin.PluginContainerServices;
import alma.common.log.ExcLog;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

import javax.swing.JComponent;

/** 
 * Model the details of a device to be displayed in a user interface.  Collect all device 
 * Monitor and Control points, and manage polling of all device Monitor points.
 *
 * @author  Scott Rankin  srankin@nrao.edu
 * @author  David Hunter  dhunter@nrao.edu
 * @version $Id$
 * @since ALMA 5.1.1    
 * 
 * TODO: Add public methods to get list of monitor points in fast and slow polling groups 
 * TODO: Add public methods to tell if a monitor point is in a fast or slow polling group
 * TODO: Add ExcLog to all logged exceptions.
 */

public abstract class DevicePresentationModel implements PresentationModelChangeListener, Pollable {

    public DevicePresentationModel(String antennaName, PluginContainerServices services) {
        this(antennaName, 0, services);
    }
    
    @SuppressWarnings("unchecked") // ControlPointPresentationModel, MonitorPointPresentationModel can not be parameterized at this point.
    public DevicePresentationModel(String antennaName, int instance, PluginContainerServices services) {
        setServices(services);        
        this.antennaName = antennaName;
        this.instance = instance;
        shutdownListeners = new ArrayList<ActionListener>(4);

        controlPointPMs = new HashMap<IControlPoint, ControlPointPresentationModel>();       
        createControlPointPresentationModels();
        
        monitorPointPMs = new HashMap<IMonitorPoint, MonitorPointPresentationModel>();
        slowMonitorPoints = new ConcurrentHashMap<IMonitorPoint, MonitorPointPresentationModel>();
        slowDiagnosticMonitorPoints = new ConcurrentHashMap<IMonitorPoint, MonitorPointPresentationModel>();
        fastMonitorPoints = new ConcurrentHashMap<IMonitorPoint, MonitorPointPresentationModel>();
        fastDiagnosticMonitorPoints = new ConcurrentHashMap<IMonitorPoint, MonitorPointPresentationModel>();
        idleMonitorPoints = new ConcurrentHashMap<IMonitorPoint, MonitorPointPresentationModel>();
        idleDiagnosticMonitorPoints = new ConcurrentHashMap<IMonitorPoint, MonitorPointPresentationModel>();
        createMonitorPointPresentationModels();

        addMonitorPointsToDefaultPollingGroups();
        setDeviceComponent();
        role = pluginContainerServices.getSessionProperties().getUserRole();
        if (role==null)
            role="None";
        disableableControls = new DisableableControlList();    
        pollingManager = new PollingManager(this);
        graphingManager = new GraphManager(logger, this);
    }
    
    public void addControl(JComponent b) { disableableControls.addControl(b); }
    
    private void addMonitorPointsToDefaultPollingGroups() {
        
        //
        // With a few exceptions, all monitor points default to the slow group.
        for (IMonitorPoint monitorPoint: monitorPointPMs.keySet()) {
            if (monitorPoint.getOperatingMode().equals(HwState.Diagnostic)) {
                if (null != monitorPointPMs.get(monitorPoint))
                    slowDiagnosticMonitorPoints.put(monitorPoint, monitorPointPMs.get(monitorPoint));
            } else {
                if (null != monitorPointPMs.get(monitorPoint))
                    slowMonitorPoints.put(monitorPoint, monitorPointPMs.get(monitorPoint));
            }
        }
        
        //
        // Move the few exceptions to the fast group.
        moveMonitorPointToFastGroup(CommonIdlMonitorPoints.getHwState);
        moveMonitorPointToFastGroup(CommonIdlMonitorPoints.inErrorState);
        moveMonitorPointToFastGroup(CommonIdlMonitorPoints.getErrorMessage);
    }
    
    /**
     * Allow for a shutdown action to be registered. These actions should be used to perform
     * anything that needs to be done on the shutdown of the GUI for this device. For example,
     * clean up after a Chart2D or shutdown an extra thread.
     * 
     * If an action becomes unnecessary (e.g the thread exited on its own) be sure to remove the
     * corresponding action from the list with removeShutdownAction(ActionListener).
     * 
     * @param newListener       The listener to notify.
     */
    public void addShutdownAction(ActionListener newListener) {
        shutdownListeners.add(newListener);
    }

    protected void createCommonControlPointPresentationModels() {
        // TODO: flesh out
    }
    
    private void createCommonMonitorPointPresentationModels() {
        // These presentation models are common to all devices
        monitorPointPMs.put(
            CommonIdlMonitorPoints.getHwState,
            new HwStateMonitorPointPresentationModel(logger, this, CommonIdlMonitorPoints.getHwState)
        );
        monitorPointPMs.put(
            CommonIdlMonitorPoints.inErrorState,
            new HwErrorStateMonitorPointPresentationModel(logger, this, CommonIdlMonitorPoints.inErrorState)
        );
        monitorPointPMs.put(
            CommonIdlMonitorPoints.getErrorMessage,
            new HwErrorMessageMonitorPointPresentationModel(logger, this, CommonIdlMonitorPoints.getErrorMessage)
        );
    }
    
    protected void createControlPointPresentationModels() {
        createCommonControlPointPresentationModels();
        createIcdControlPointPresentationModels();
        createIdlControlPointPresentationModels();
    }
    
    protected abstract void createIcdControlPointPresentationModels();
    
    protected abstract void createIcdMonitorPointPresentationModels();
    
    protected abstract void createIdlControlPointPresentationModels();
    
    protected abstract void createIdlMonitorPointPresentationModels();
    
    private void createMonitorPointPresentationModels() {
        createCommonMonitorPointPresentationModels();
        createIcdMonitorPointPresentationModels();
        createIdlMonitorPointPresentationModels();
    }
    
    /**
     * Check if the current user is allowed to use the controls or now.
     * 
     * TODO: Does isAntennaAllowed actually properly tell us if the user should
     * be allowed to use the controls or not?
     * @return
     */
    public boolean allowControles(){
        return pluginContainerServices.getSessionProperties().isAntennaAllowed();
    }

    public String getAntenna() { return antennaName; }

    /**
     * Get the control point presentation model for a control point.
     *  
     * @param cp the control point we are looking for
     * @return the control point presentation model we requested.
     */
    @SuppressWarnings("unchecked") // ControlPointPresentationModel can not be parameterized at this point.
    public ControlPointPresentationModel getControlPointPM(IControlPoint cp) {
        if (controlPointPMs.containsKey(cp)) {
            return controlPointPMs.get(cp);
        }
        // Log presentation models not found.     	
        logger.severe(
                "DevicePresentationModel.getControlPointPM() - presentation model not found for:" +
                cp
        );
        logger.severe("getControlPointPM() - failed to find: " + cp);
        return null;
    }
    
    public boolean getControlsEnabled() { return disableableControls.getControlsEnabled(); }
    
    public abstract String getDeviceDescription();

    public GraphManager getGraphingManager() {return graphingManager; }
    
    public int getInstance() { return instance; }
    
    public Logger getLogger() { return logger; }
    
    /**
     * Get the monitor point presentation model for a monitor point.
     *  
     * @param mp the monitor point we are looking for
     * @return the monitor point presentation model we requested.
     */
    @SuppressWarnings("unchecked") // MonitorPointPresentationModel can not be parameterized at this point.
    public MonitorPointPresentationModel getMonitorPointPM(IMonitorPoint mp) {
        if (monitorPointPMs.containsKey(mp)) {
            return monitorPointPMs.get(mp);
        }
        // Log monitor point presentation models not found.     	
        logger.severe(
                "DevicePresentationModel.getMonitorPointPM() - presentation model not found for:" +
                mp
        );
        return null;
    }
    
    /**
     * @return The PluginContainerServices for the parent plugin. 
     */
    public PluginContainerServices getPluginContainerServices(){
        if (pluginContainerServices == null)
            logger.severe("DevicePresentationModel:getPluginContainerServices - ERROR: attempted to get" +
                          "pluginContainerServices without first initilizing them.");
        return pluginContainerServices;
    }
    
    public boolean getRunRestricted() { return disableableControls.getRunRestricted(); }

    /**
     * We need to track the hardware state so we don't poll monitor points inappropriately.
     */
    public void handlePresentationModelChangeEvent(PresentationModelChangeEvent<?> event) {
        if (event.getSource() == CommonIdlMonitorPoints.getHwState) {
            if (event.getEventType() == PresentationModelChangeEvents.VALUE_CHANGE) {
                hwState = (HwState)event.getValue();
            }
        }
    }
    
    /**
     * Return the role as was provided by the OMC at setup time.
     * @return the role.
     */
    public String getRole(){
        return role;
    }
    
    public void manuallyPollIdleMonitorPoint(IMonitorPoint pointToPoll){
        try {
            @SuppressWarnings("unchecked") //MonitorPointPresentationModel can not be parameterized at this point.
            MonitorPointPresentationModel mp;
            mp=idleMonitorPoints.get(pointToPoll);
            if (mp==null)
                mp=idleDiagnosticMonitorPoints.get(pointToPoll);
            if (mp==null)
                logger.severe("DevicePresentationmodel.manuallyPollIdleMonitorPoint() Error: unable to find" +
                              "requested monitor point " + pointToPoll);
            else
                mp.checkDeviceForUpdates();
        } catch(Exception e) {
            logger.severe(
                    "DevicePresentationModel.manuallyPollIdleMonitorPoint() caught an exception calling" +
                    "checkDeviceForUpdates() on: " +pointToPoll + ExcLog.details(e));}
    }
    
    public void moveMonitorPointToFastGroup(IMonitorPoint monitorPoint) {
        if (! fastDiagnosticMonitorPoints.containsKey(monitorPoint)
                && ! fastMonitorPoints.containsKey(monitorPoint)) {
            if (monitorPoint.getOperatingMode().equals(HwState.Diagnostic.toString())) {
                if (slowDiagnosticMonitorPoints.containsKey(monitorPoint)) {
                    fastDiagnosticMonitorPoints.put(monitorPoint, slowDiagnosticMonitorPoints.get(monitorPoint));
                    slowDiagnosticMonitorPoints.remove(monitorPoint);
                } else if(idleDiagnosticMonitorPoints.containsKey(monitorPoint)){
                    fastDiagnosticMonitorPoints.put(monitorPoint, idleDiagnosticMonitorPoints.get(monitorPoint));
                    idleDiagnosticMonitorPoints.remove(monitorPoint);
                } else {
                    logger.severe("DevicePresentationModel.moveMonitorPointToFastGroup() - MonitorPoint model "
                            + monitorPoint + " not found in slowDiagnosticMonitorPoints or in"
                            + "idleDiagnosticMonitorPoints!");
                }
            } else {
                if (slowMonitorPoints.containsKey(monitorPoint)) {
                    fastMonitorPoints.put(monitorPoint, slowMonitorPoints.get(monitorPoint));
                    slowMonitorPoints.remove(monitorPoint);
                } else if(idleMonitorPoints.containsKey(monitorPoint)){
                    fastMonitorPoints.put(monitorPoint, idleMonitorPoints.get(monitorPoint));
                    idleMonitorPoints.remove(monitorPoint);
                } else {
                    logger.severe("DevicePresentationModel.moveMonitorPointToFastGroup() - MonitorPoint model "+
                            monitorPoint + "not found in slowMonitorPoints or idleMonitorPoints!");
                }
            }
        }
    }
    
    public void moveMonitorPointToIdleGroup(IMonitorPoint monitorPoint){
        if (! idleDiagnosticMonitorPoints.containsKey(monitorPoint)
                && ! idleMonitorPoints.containsKey(monitorPoint)) {
            if (monitorPoint.getOperatingMode().equals(HwState.Diagnostic.toString())) {
                if (fastDiagnosticMonitorPoints.containsKey(monitorPoint)) {
                    idleDiagnosticMonitorPoints.put(monitorPoint, fastDiagnosticMonitorPoints
                            .get(monitorPoint));
                    fastDiagnosticMonitorPoints.remove(monitorPoint);
                } else if (slowDiagnosticMonitorPoints.containsKey(monitorPoint)) {
                    idleDiagnosticMonitorPoints.put(monitorPoint, slowDiagnosticMonitorPoints
                            .get(monitorPoint));
                    slowDiagnosticMonitorPoints.remove(monitorPoint);
                } else {
                    logger
                            .severe("DevicePresentationModel.moveMonitorPointToIdleGroup() - MonitorPoint model for "
                                    + monitorPoint
                                    + " not found!");
                }
            } else {
                if (slowMonitorPoints.containsKey(monitorPoint)) {
                    idleMonitorPoints.put(monitorPoint, slowMonitorPoints.get(monitorPoint));
                    slowMonitorPoints.remove(monitorPoint);
                } else if (fastMonitorPoints.containsKey(monitorPoint)) {
                    idleMonitorPoints.put(monitorPoint, fastMonitorPoints.get(monitorPoint));
                    fastMonitorPoints.remove(monitorPoint);
                } else {
                    logger
                            .severe("DevicePresentationModel.moveMonitorPointToIdleGroup() - MonitorPoint model "
                                    + monitorPoint + "not found!");
                }
            }
        }
    }
    
    public void moveMonitorPointToSlowGroup(IMonitorPoint monitorPoint) {
        if (! slowDiagnosticMonitorPoints.containsKey(monitorPoint)
                && ! slowMonitorPoints.containsKey(monitorPoint)) {
            if (monitorPoint.getOperatingMode().equals(HwState.Diagnostic.toString())) {
                if (fastDiagnosticMonitorPoints.containsKey(monitorPoint)) {
                    slowDiagnosticMonitorPoints.put(monitorPoint, fastDiagnosticMonitorPoints.get(monitorPoint));
                    fastDiagnosticMonitorPoints.remove(monitorPoint);
                } else if (idleDiagnosticMonitorPoints.containsKey(monitorPoint)){
                    slowDiagnosticMonitorPoints.put(monitorPoint, idleDiagnosticMonitorPoints.get(monitorPoint));
                    idleDiagnosticMonitorPoints.remove(monitorPoint);
                }else {
                    logger.severe("DevicePresentationModel.moveMonitorPointToFastGroup() - MonitorPoint model for "
                            + monitorPoint + " not found!");
                }
            } else {
                if (fastMonitorPoints.containsKey(monitorPoint)) {
                    slowMonitorPoints.put(monitorPoint, fastMonitorPoints.get(monitorPoint));
                    fastMonitorPoints.remove(monitorPoint);
                } else if (idleMonitorPoints.containsKey(monitorPoint)){
                    slowMonitorPoints.put(monitorPoint, idleMonitorPoints.get(monitorPoint));
                    idleMonitorPoints.remove(monitorPoint);
                } else {
                    logger.severe("DevicePresentationModel.moveMonitorPointToFastGroup() - MonitorPoint model for "
                            + monitorPoint + " not found!");
                }
            }
        }
    }

    /**
     * Poll all fast monitor points as required for this device.
     */
    public void pollFastMonitorPoints() {
        for (IMonitorPoint monitorPoint: fastMonitorPoints.keySet()) {
            try {
                fastMonitorPoints.get(monitorPoint).checkDeviceForUpdates();
            } catch(Exception e) {
                logger.severe(
                        "DevicePresentationModel.pollFastMonitorPoints() caught an exception calling checkDeviceForUpdates() on: " +
                        monitorPoint +
                        ExcLog.details(e)
                );
            }
        }
        
        if (HwState.Diagnostic == hwState) {
            for (IMonitorPoint monitorPoint: fastDiagnosticMonitorPoints.keySet()) {
                try {
                    fastDiagnosticMonitorPoints.get(monitorPoint).checkDeviceForUpdates();
                } catch(Exception e) {
                    logger.severe(
                            "DevicePresentationModel.pollFastMonitorPoints() caught an exception calling checkDeviceForUpdates() on: " +
                            monitorPoint +
                            ExcLog.details(e)
                    );
                }
            }
        }
    }
    
    /**
     * Poll all slow monitor points as required for this device.
     */
    public void pollSlowMonitorPoints() {
        for (IMonitorPoint monitorPoint: slowMonitorPoints.keySet()) {
            try {
                slowMonitorPoints.get(monitorPoint).checkDeviceForUpdates();
            } catch(Exception e) {
                logger.severe(
                        "DevicePresentationModel.pollSlowMonitorPoints() caught an exception calling checkDeviceForUpdates() on: " +
                        monitorPoint +
                        ExcLog.details(e)
                );
            }
        }

        if (HwState.Diagnostic == hwState) {
            for (IMonitorPoint monitorPoint: slowDiagnosticMonitorPoints.keySet()) {
                try {
                    slowDiagnosticMonitorPoints.get(monitorPoint).checkDeviceForUpdates();
                } catch(Exception e) {
                    logger.severe(
                            "DevicePresentationModel.pollSlowMonitorPoints() caught an exception calling checkDeviceForUpdates() on: " +
                            monitorPoint +
                            ExcLog.details(e)
                    );
                }
            }
        }
    }
        
    protected void releaseDeviceComponent() {
        deviceComponent = null;
        if (null != deviceComponent) {
            deviceComponent = null;
            componentName = null;
        }
    }
    
    /**
     * Remove a previously registered shutdown action. Be sure to use this if your shutdown action
     * becomes unnecessary. For example, if the action shutdowns an extra thread, it should be
     * removed if that thread closes via other means.
     * 
     * Note: If the device is shutting down at the time this function is called the remove request
     * will be ignored. This is done because removing an item during shutdown could adversely 
     * effect the calling of cleanup actions. In addition, once the shutdown is complete, all the
     * shutdownListeners should be discarded.
     * 
     * @param newListener       The listener to notify.
     */
    public void removeShutdownAction(ActionListener newListener) {
        if (!shuttingDown)
            shutdownListeners.remove(newListener);
    }
    
    protected abstract void setComponentName();
    
    public void setControlsEnabled(boolean enabled) { disableableControls.setControlsEnabled(enabled); }
    
    protected abstract void setDeviceComponent();
    
    public void setRunRestricted(boolean restricted) { disableableControls.setRunRestricted(restricted); }
    
    /**
     * Set Plugin Container Services for later use in getting a logger, getting components, etc.
     *
     * @param services
     */
    public void setServices(PluginContainerServices services) {
        pluginContainerServices = services;
        
        logger = (pluginContainerServices == null) ? 
            Logger.getLogger(DEFAULT_LOGGER) :
            pluginContainerServices.getLogger();
    }

    /**
     * Start polling the hardware device for changes.
     */
    public void start() {
        pollingManager.startPolling();
    } 
    
    /**
     * Perform all actions needed to clean up on GUI shutdown.
     * Currently:
     *   -Stop polling the hardware device for changes.
     *   -Call all registered shutdown actions.
     */
    public void stop() {
        shuttingDown=true;
        pollingManager.stopPolling();
        for (int c=0; c<shutdownListeners.size(); c++)
            shutdownListeners.get(c).actionPerformed
                                     (new ActionEvent(this, 1, "DevicePMShutdownEvent"));
    }

    public static String DEVICE_NAME;
    
    protected String antennaName = null;
    protected String componentName = null;
    @SuppressWarnings("unchecked") // ControlPointPresentationModel can not be parameterized at this point.
    protected HashMap<IControlPoint, ControlPointPresentationModel> controlPointPMs = null;
    protected HardwareDevice deviceComponent = null;
    protected int instance;
    protected Logger logger = null;
    @SuppressWarnings("unchecked") // MonitorPointPresentationModel can not be parameterized at this point.
    protected HashMap<IMonitorPoint, MonitorPointPresentationModel> monitorPointPMs = null;
    protected PluginContainerServices pluginContainerServices;

    private final static String DEFAULT_LOGGER = "DevicePresentationModelDefaultLogger";
    private DisableableControlList disableableControls;
    @SuppressWarnings("unchecked") // MonitorPointPresentationModel can not be parameterized at this point.
    private ConcurrentHashMap<IMonitorPoint, MonitorPointPresentationModel> fastDiagnosticMonitorPoints = null;
    @SuppressWarnings("unchecked") // MonitorPointPresentationModel can not be parameterized at this point.
    private ConcurrentHashMap<IMonitorPoint, MonitorPointPresentationModel> fastMonitorPoints = null;
    private GraphManager graphingManager;
    private HwState hwState = HwState.Undefined;
    @SuppressWarnings("unchecked") //MonitorPointPresentationModel can not be parameterized at this point.
    private ConcurrentHashMap<IMonitorPoint, MonitorPointPresentationModel> idleDiagnosticMonitorPoints = null;
    @SuppressWarnings("unchecked") //MonitorPointPresentationModel can not be parameterized at this point.
    private ConcurrentHashMap<IMonitorPoint, MonitorPointPresentationModel> idleMonitorPoints = null;
    private PollingManager pollingManager;
    private String role;
    private ArrayList<ActionListener> shutdownListeners = null;
    private boolean shuttingDown=false;
    @SuppressWarnings("unchecked") // MonitorPointPresentationModel can not be parameterized at this point.
    private ConcurrentHashMap<IMonitorPoint, MonitorPointPresentationModel> slowDiagnosticMonitorPoints = null;
    @SuppressWarnings("unchecked") // MonitorPointPresentationModel can not be parameterized at this point.
    private ConcurrentHashMap<IMonitorPoint, MonitorPointPresentationModel> slowMonitorPoints = null;
}

//
//O_o
