/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;

import java.util.logging.Logger;

import javax.swing.JLabel;

/**
 * <code>BooleanTextMonitorPointWidget</code> displays two user defined text
 * labels to indicate true or false values from its associated monitor point.
 * 
 * @author  Scott Rankin  srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 5.0.4
 * 
 *        TODO: Document
 */
public class BooleanTextMonitorPointWidget extends MonitorPointWidget {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private String falseText;
    private String trueText;
    private JLabel valueLabel;
    private IMonitorPoint monitorPoint;

    public BooleanTextMonitorPointWidget(Logger logger,
            MonitorPointPresentationModel<Boolean> monitorPointModel,
            String falseText, String trueText) {
        super(logger, true);
        this.falseText = falseText;
        this.trueText = trueText;
        monitorPoint = addMonitorPointPM(monitorPointModel, Boolean.FALSE);
        buildWidget();
    }

    /**
     * buildWidget is a private method which is used to construct the entire
     * widget to be displayed on a panel
     */
    private void buildWidget() {
        valueLabel = new JLabel(falseText);
        add(valueLabel);
        addPopupMenuToComponent(valueLabel);
    }

    @Override
    protected void updateAttention(Boolean value) {
        // TODO: remove
    }

    @Override
    protected void updateValue(IMonitorPoint aMonitorPoint) {
        if ((Boolean) getCachedValue(aMonitorPoint)) {
            valueLabel.setText(trueText);
        } else {
            valueLabel.setText(falseText);
        }
    }
}

//
// O_o

