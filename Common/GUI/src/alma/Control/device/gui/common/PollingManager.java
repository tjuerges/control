/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common;

import java.util.logging.Logger;

import alma.common.log.ExcLog;

/** 
 * Manage polling for any class that implements the Pollable interface.  This may be any code we want to 
 * poll at a fast and a slow polling rate.  The slow polling period may be any integer multiple of the 
 * fast polling period.  See code for defaults.
 * 
 * @deprecated  Replaced by alma.Control.CommonGui.timers.PeriodicTimerManager.
 * 
 * @author  Scott Rankin  srankin@nrao.edu
 * @author  Steve Harrington  sharring@nrao.edu
 * @version $Id$
 * @since   ALMA 5.0.4
 */
public class PollingManager implements Runnable {
	
	private final static String DEFAULT_LOGGER = "PollingManagerDefaultLogger";
	private final static int DEFAULT_SLOW_POLLING_PERIOD_MULTIPLIER = 3;
	private final static int FAST_POLLING_PERIOD_IN_SECONDS = 1;
	private final static int MILLISECONDS_PER_SECOND = 1000;
	private Pollable deviceToPoll = null;
	protected Logger logger = null;
	private volatile boolean nowPolling = false;
	private Thread pollingThread;
	private int slowPollingPeriodMultiplier = DEFAULT_SLOW_POLLING_PERIOD_MULTIPLIER;
	
	public PollingManager(Pollable deviceToPoll) {
		this.logger = Logger.getLogger(DEFAULT_LOGGER);		
		this.deviceToPoll = deviceToPoll;
	}
	
	/**
	 * How long is the fast polling period, in seconds.
	 * 
	 * @return the length of the Fast Polling Period in seconds.
	 */
	public int getFastPollingPeriod() {
		return FAST_POLLING_PERIOD_IN_SECONDS;
	}
	
	/**
	 * How long is the slow polling period, in multiples of the fast polling period.
	 * 
	 * @return the length of the slow polling period in seconds.
	 */
	public int getSlowPollingPeriod() {
		return slowPollingPeriodMultiplier;
	}
	
	/**
	 * Is the polling manager actively polling the device?
	 * 
	 * @return true if actively polling, false if not.
	 */
	public boolean nowPolling() {
		return nowPolling;
	}
	
	/**
	 * Poll data as scheduled.
	 */
	public void run() {
		try {
    		deviceToPoll.pollFastMonitorPoints();
	    	deviceToPoll.pollSlowMonitorPoints();
		} catch (Exception e) {
			logger.severe(
			    "PollingManager.run() - Exception occurred while collecting initial sample:" + 
			    ExcLog.details(e)
			);
		}
		int fastPeriodLoopCount = 0;
		while (nowPolling) {
			if (sleepInSeconds(FAST_POLLING_PERIOD_IN_SECONDS)) {
				fastPeriodLoopCount++;
				try {
					deviceToPoll.pollFastMonitorPoints();
				} catch (Exception e) {
					logger.severe(
					    "PollingManager.run() - Exception during call to deviceToPoll.pollFastPeriodMonitorPoints(): "  + 
					    ExcLog.details(e)
					);
				}
			} else {
				// The thread must have been interrupted, so prepare to exit cleanly.
				logger.severe("PollingManager.run() - did not sleep for full FAST_POLLING_PERIOD_IN_SECONDS." +
						      "Preparing to exit polling loop.");
				stopPolling();
			}
			if (fastPeriodLoopCount >= slowPollingPeriodMultiplier) {
				try {
					deviceToPoll.pollSlowMonitorPoints();
				} catch (Exception e) {
					logger.severe(
					    "PollingManager.run() - Exception during call to deviceToPoll.pollSlowMonitorPoints()" +
					    ExcLog.details(e)
					);
				}				
				fastPeriodLoopCount = 0;
			}
		}
	}
	
	/**
	 * Set a system logger to replace the default logger.
	 * 
	 * @param newLogger is any Java Logger.  In typical use, this will be a Logger associated with the ACS
	 *        logging system.
	 */
	public void setLogger(Logger newLogger) {
		this.logger = newLogger;
	}
	
	/**
	 * Set the slow polling period as needed.
	 * 
	 * @param newSlowPollingPeriod is the slow polling period in multiples of the fast polling period.
	 */
	public void setSlowPollingPeriod(int newSlowPollingPeriod) {
		this.slowPollingPeriodMultiplier = newSlowPollingPeriod;
	}
		
	/**
	 * Sleep for the given number of seconds.  Indicate if the given time expired.
	 * 
	 * @param secondsToSleep is the desired number of seconds to sleep
	 * @return true if slept the full duration, false if interrupted.
	 */
	private boolean sleepInSeconds(int secondsToSleep) {
	    try {
			Thread.sleep(secondsToSleep * MILLISECONDS_PER_SECOND);
		} catch (InterruptedException e) {
			logger.severe("PollingManager.sleepNseconds() was interrupted:" + ExcLog.details(e));
			return false;
		}
		return true;
	}	
	
	/**
	 * Start polling.
	 */
	public void startPolling() {
		if (nowPolling) {
		} else {			
			pollingThread = new Thread(this);
			pollingThread.setDaemon(true);
			pollingThread.start();
			nowPolling = true;			
		}
	}
	
	/**
	 * Stop polling.
	 */
	public void stopPolling() {
		nowPolling = false;
		pollingThread = null;
	}
}

//
// O_o
