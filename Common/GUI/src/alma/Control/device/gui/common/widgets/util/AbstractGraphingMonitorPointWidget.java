/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets.util;

import info.monitorenter.gui.chart.ZoomableChart;
import info.monitorenter.gui.chart.IAxis.AxisTitle;
import info.monitorenter.gui.chart.labelformatters.LabelFormatterDate;
import info.monitorenter.gui.chart.traces.Trace2DLtd;
import info.monitorenter.gui.chart.rangepolicies.RangePolicyFixedViewport;
import info.monitorenter.gui.chart.rangepolicies.RangePolicyMinimumViewport;
import info.monitorenter.util.Range;

import java.awt.Color;
import java.awt.BorderLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.swing.BorderFactory;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.widgets.MonitorPointWidget;


/**
 * An abstract class that covers most of the functionality of needed for graphing monitor points. This class
 * should be extended to make graph widgets for specific monitor point types. All the type specific settings
 * should go in the child classes. All the common stuff, which is about all of it, should go in this class.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since ALMA 7.1.0
 * 
 *        TODO: Add indication of out of range data.
 */
public abstract class AbstractGraphingMonitorPointWidget extends MonitorPointWidget {

    public AbstractGraphingMonitorPointWidget(Logger logger) {
        super(logger, true);
        listenerList = new ArrayList<ActionListener>();
    }
    
    /**
     * This constructor allows child classes to reset the default number of points. Some graphs might need
     * to have more points in order to match the normal graphs. For example, the Boolean graphs use twice as
     * many points due to the need to appear as a square wave.
     * @param logger
     * @param newNumberOfPoints
     */
    public AbstractGraphingMonitorPointWidget(Logger logger, int newNumberOfPoints) {
        this(logger);
        numberOfPoints=newNumberOfPoints;
    }

    /**
     * Adds an <code>ActionListener</code> to the graph widget.
     * @param l the <code>ActionListener</code> to be added
     * 
     * The action will be triggered each time the graph is updated. This is generally used if you want to
     * synchronize several graphs.
     */
    public void addActionListener(ActionListener l) {
        listenerList.add(l);
    }
    
    /**
     * Pause the updates of the graph.
     * @param setPause True for pause, false for resume
     * 
     * TODO: insure that the pause function works properly.
     */
    public void pause(boolean setPause){
        isPaused=setPause;
    }
    
    public void setAutoYRange(){
        chart.getAxisY().setRangePolicy(new RangePolicyMinimumViewport(new Range(-0.1, 1.1)));
    }
    
    /**
     * Sets the range of the graph based on the given upper and lower bounds.
     * Each bound a long containing the time in ms since midnight, January 1, 1970 UTC.
     * @param newLower The new lower X bound
     * @param newUpper The new upper X bound
     */
    public void setXRange(long newLower, long newUpper){
        if (!isPaused)
            chart.getAxisX().setRangePolicy(new RangePolicyFixedViewport(new Range(newLower, newUpper)));
    }

    //The monitor point must be set when this is called.
    protected void buildWidget() {
        //Child classes can override the y label. Used to allow the boolean widget to properly label things.
        if (yLabel.equals(""))
            yLabel=monitorPoint.getDisplayUnits();
        this.graphTitle=monitorPoint.getDisplayName();
        xLabel="Time, UTC (hh:mm:ss)";
        
        //Set the unit offset and scale based on monitor point settings (e.g covert �K to �C, or w to nw)
        //Customizations should be set by the GUI for the given device.
        scaleFactor = monitorPoint.getDisplayScale();
        offsetFactor= monitorPoint.getDisplayOffset();
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(graphTitle),
                BorderFactory.createEmptyBorder(1,1,1,1)));
        setLayout(new BorderLayout());
        
        chart=new ZoomableChart();
        data=new Trace2DLtd(numberOfPoints);
        data.setColor(DEFAULT_COLOR);
        chart.addTrace(data);
        chart.setPaintLabels(false);
        chart.getAxisX().setAxisTitle(new AxisTitle(xLabel));
        chart.getAxisY().setAxisTitle(new AxisTitle(yLabel));
        //Note: right now we graph with the local computer time, not the timestamp form the data.
        chart.getAxisX().setFormatter(new LabelFormatterDate(new SimpleDateFormat("kk:mm:ss")));
        chart.getAxisY().setRangePolicy(new RangePolicyMinimumViewport(new Range(0.0, 2.0)));
        add(chart);
    }
    
    /**
     * Call the destroy function of the chart2d. This is necessary to properly free up
     * the resources that the chart uses. The device GUI will not properly close if this is
     * not called.
     * 
     * It also removes this monitor point from the MonitorPointPresentationModel listener list.
     * 
     * CAUTION: Calling this before the chart is removed from the graphing panel may cause problems..
     */
    protected void destroy(){
        this.removeListeners();
        chart.destroy();
    }
    
    protected void fireStateChanged() {
        for (int c=0; c<listenerList.size(); c++)
            listenerList.get(c).actionPerformed(new ActionEvent(this, 1, "UpdateGraphMenuEvent"));
    }
    
    //Required by parent class, but we never use it.
    @Override
    protected void updateAttention(Boolean q) {}

    //This function needs to be sure to call fireStateChanged so that the graph updates.
    @Override
    protected abstract void updateValue(IMonitorPoint source);
    
    protected ZoomableChart chart;
    //These values will be used by the child classes of this class.
    protected Trace2DLtd data;
    protected String graphTitle;
    protected boolean isPaused=false;
    private ArrayList<ActionListener> listenerList;
    protected IMonitorPoint monitorPoint;
    protected double offsetFactor;
    protected double scaleFactor;
    protected String yLabel = "";
    private Color DEFAULT_COLOR = Color.BLUE;
    private int numberOfPoints=90000;          //25 hours @ 1 sample a second
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private String xLabel;
}

//
// O_o
