/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;

import java.util.logging.Logger;

/**
 * <code>BooleanMonitorPointWidget</code> displays two user defined images to indicate
 * true or false values from its associated monitor point.
 * 
 * @author  Scott Rankin  srankin@nrao.edu
 * @author  Steve Harrington  sharring@nrao.edu
 * @version $Id$
 * @since   ALMA 5.0.4
 * 
 * TODO: Document
 * TODO: Factor out common behavior with other widgets to a superclass.
 */
public class BooleanMonitorPointWidget extends MonitorPointWidget {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private Status falseStatus;
    private String falseToolTipText;
    private StatusIndicator statusIndicator;
    private Status trueStatus;
    private String trueToolTipText;
    private IMonitorPoint monitorPoint;

    /**
     * <code>BooleanMonitorPointWidget</code> is the constructor.
     * 
     * @param logger logger to be used for logging
     * @param model monitor point to listen for
     * @param falseStatus status to be used to indicate a false value read from the monitor point
     * @param falseToolTipText tool tip text to be used to indicate a false value was read
     * from the monitor point
     * @param trueStatus status to be used to indicate a true value was read from the monitor point
     * @param trueToolTipText tool tip text to be used to indicate a true value was read
     * from the monitor point
     */
    public BooleanMonitorPointWidget(
            Logger logger,
            MonitorPointPresentationModel<Boolean> monitorPointModel,
            Status falseStatus,
            String falseToolTipText,
            Status trueStatus,
            String trueToolTipText
    ) {
        super(logger, true, true);
        this.falseStatus = falseStatus;
        this.falseToolTipText = falseToolTipText;
        this.trueStatus = trueStatus;
        this.trueToolTipText = trueToolTipText;

        monitorPoint = addMonitorPointPM(monitorPointModel, Boolean.FALSE);
        
        //The BooleanGraphingMonitorPointWidget will use these to properly figure out which way on the
        //graph is up, and then to properly label the graph.
        monitorPoint.setLowerGraphYLabel(this.falseStatus.toString());
        monitorPoint.setUpperGraphYLabel(this.trueStatus.toString());
        
        buildWidget();
    }

    /**
     * buildWidget is a private method which is used to construct the entire widget
     * to be displayed on a panel
     */
    private void buildWidget() {
        statusIndicator = new StatusIndicator(falseStatus, falseToolTipText);
        add(statusIndicator);

        addPopupMenuToComponent(statusIndicator);
    }

    @Override
    protected void updateAttention(Boolean value) {
        // TODO: remove
    }

    @Override
    protected void updateValue(IMonitorPoint aMonitorPoint) {
        if ((Boolean)getCachedValue(aMonitorPoint)) {
            statusIndicator.setStatus(trueStatus, trueToolTipText);
        } else {
            statusIndicator.setStatus(falseStatus, falseToolTipText);
        }
    }  
}

//
// O_o

