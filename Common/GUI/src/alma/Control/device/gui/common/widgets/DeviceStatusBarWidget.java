/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.HardwareDevicePackage.HwState;
import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;

import java.awt.Color;
import java.awt.Dimension;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

public class DeviceStatusBarWidget extends MonitorPointWidget {
	
	private static final long serialVersionUID = 1L;
	private IMonitorPoint errorStateMonitorPoint;
	private IMonitorPoint hardwareStateMonitorPoint;
	private JLabel valueLabel;
	
	public DeviceStatusBarWidget(
			Logger logger,
			MonitorPointPresentationModel<HwState> hardwareStateModel,
			MonitorPointPresentationModel<Boolean> errorStateModel
			) {
		super(logger);
		hardwareStateMonitorPoint = addMonitorPointPM(hardwareStateModel, HwState.Undefined);
		errorStateMonitorPoint = addMonitorPointPM(errorStateModel, Boolean.FALSE);
		
		buildWidget();
	}
	
	private void buildWidget() {
		setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, Color.BLACK));
		setBackground(StatusColor.FAILURE.getColor());
		setPreferredSize(new Dimension(150, 25));
		
		valueLabel = new JLabel("Start");
		add(valueLabel);
	}
	
	@Override
	protected void updateAttention(Boolean value) {
		// Nothing to do in this class.
	}
		
	@Override
	protected void updateValue(IMonitorPoint source) {
		HwState hwState = getCachedValue(hardwareStateMonitorPoint);
		Boolean errorState = getCachedValue(errorStateMonitorPoint);
		
		if (source == hardwareStateMonitorPoint) {			
			//HwState newHwState = (HwState)value;
			//if (!hwState.equals(newHwState)) {
			//	hwState = newHwState;
				valueLabel.setText(hwState.toString());
			//}
		} else if (source == errorStateMonitorPoint) {
			//Boolean newErrorState = (Boolean)value;
			//if (!errorState.equals(newErrorState)) {
			//	errorState = newErrorState;
			//}
		}	
		
		// Background color must be set based on both current hwState and current errorState.
		// Only one changes per event, so both must be cached.
		if (!hwState.equals(HwState.Operational)) {
			setBackground(StatusColor.FAILURE.getColor());
		} else {
			if (errorState.booleanValue()) {
				setBackground(StatusColor.WARNING.getColor());
			} else { 
				setBackground(StatusColor.GOOD.getColor());
			}
		}
	}
}