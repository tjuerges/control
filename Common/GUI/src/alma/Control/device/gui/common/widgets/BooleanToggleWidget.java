/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.ControlPointPresentationModel;
import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.JButton;

/**
 * <code>BooleanToggleWidget</code> displays a button whose text is dependent upon
 * the current value of its associated monitor point.
 * 
 * @author  Scott Rankin  srankin@nrao.edu
 * @author  Justin Kenworthy jkenwort@nrao.edu
 * @version $Id$
 * @since   ALMA 5.0.4
 * 
 * TODO: Document
 * TODO: Factor out common behavior with other widgets to a superclass.
 */
public class BooleanToggleWidget
extends MonitorPointWidget
implements ActionListener {

    private static final long serialVersionUID = 1L;
    private boolean commandIssued;
    private ControlPointPresentationModel<Boolean> controlPointModel;
    private String falseButtonLabel;
    private MonitorPointPresentationModel<Boolean> monitorPointModel;
    private JButton toggleButton;
    private String trueButtonLabel;
    private IMonitorPoint monitorPoint;

    /**
     * <code>BooleanToggleWidget</code> is the constructor for this class
     * 
     * @param logger logger to handle logging
     * @param monitorPointModel monitor point to listen for
     * @param controlPointModel control point to set values for
     * @param falseButtonLabel character string to display when a false value is read from the
     * associated monitor point
     * @param trueButtonLabel character string to display when a true value is read from the
     * associated monitor point
     */
    public BooleanToggleWidget(
            Logger logger,
            MonitorPointPresentationModel<Boolean> monitorPointModel,
            ControlPointPresentationModel<Boolean> controlPointModel,
            String falseButtonLabel,
            String trueButtonLabel) {
        super(logger);
        this.logger = logger;
        this.monitorPointModel = monitorPointModel;
        this.controlPointModel = controlPointModel;
        this.falseButtonLabel = falseButtonLabel;
        this.trueButtonLabel = trueButtonLabel;

        commandIssued = false;
        toggleButton = new JButton();
        toggleButton.addActionListener(this);
        monitorPoint = addMonitorPointPM(monitorPointModel, Boolean.FALSE);
        buildWidget();
        this.controlPointModel.getContainingDevicePM().addControl(toggleButton);
    }

    /**
     * <code>actionPerformed</code> implements the <code>ActionListener</code> interface.
     * It listens for a button press on the widget's JButton and toggles the control points's value
     * based on the current value of the monitor point
     */
    public void actionPerformed(ActionEvent e) {
        controlPointModel.setValue(!monitorPointModel.getValue());
        toggleButton.setEnabled(false);
        commandIssued = true;
    }

    /**
     * buildWidget is a private method which is used to construct the entire widget
     * to be displayed on a panel
     */
    protected void buildWidget() {
        toggleButton.setText(falseButtonLabel);
        add(toggleButton);
    }

    @Override
    protected void updateAttention(Boolean value) {
        // TODO: remove
    }

    /**
     * <code>setValue</code> alters the value of this widget to be either 
     * true or false.
     * 
     * @param q value to be set for this widget
     */
    @Override
    public void updateValue(IMonitorPoint source) {
        if ((Boolean)getCachedValue(monitorPoint)) {
            toggleButton.setText(trueButtonLabel);
        } else {
            toggleButton.setText(falseButtonLabel);
        }

        if (commandIssued) {
            if (controlPointModel.getContainingDevicePM().getControlsEnabled()) {
                toggleButton.setEnabled(true);
            }
            commandIssued = false;
        }
    }
}