/* ALMA - Atacama Large Millimeter Array
* (c) Associated Universities Inc., 2008 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*/

package alma.Control.device.gui.common;

import java.util.Date;

/**
 * Events generated by a Presentation Model to inform UI components of changes to the Presentation Model.
 *
 * @author  Scott Rankin  srankin@nrao.edu   
 * @version $Id$
 * @since   ALMA 5.1.1
 */

public class PresentationModelChangeEvent<T> {
    
    /**
     * All PresentationModelChangeEvents need to know: 
     * 
     * @param anEventType the type of the event.
     * @param aValue the value of any data associated with the event.
     * @param newTimeStamp the alma time stamp associated with aValue.
     * @param aSource the source of the event.
     */
    public PresentationModelChangeEvent(PresentationModelChangeEvents anEventType, T aValue, 
                                        Date newTimeStamp, IMonitorPoint aSource) {
        this.eventType = anEventType;
        this.value = aValue;
        this.source = aSource;
        this.timeStamp = new Date(newTimeStamp.getTime());
    }
    
    /**
     * All PresentationModelChangeEvents need to know: 
     * 
     * @param anEventType the type of the event.
     * @param aValue the value of any data associated with the event.
     * @param aSource the source of the event.
     */
    public PresentationModelChangeEvent(PresentationModelChangeEvents anEventType, T aValue, 
            IMonitorPoint aSource) {
        this.eventType = anEventType;
        this.value = aValue;
        this.source = aSource;
        this.timeStamp=new Date(0);
    }
    
    /**
     * @return the eventType for this event.
     */
    public PresentationModelChangeEvents getEventType() {
        return this.eventType;
    }
    
    /**
     * Get the alma time associated with the value.
     * A return of 0 indicates that the time is not known.
     * @return The time stamp
     */
    public Date getTimeStamp(){
        //Return a new date in case they decide to change the value...
        return new Date(this.timeStamp.getTime());
    }
    
    /**
     * @return the value for this event.
     */
    public T getValue() {
        return this.value;
    }
    
    /**
     * @return the source for this event.
     */
    public IMonitorPoint getSource() {
        return this.source;
    }
    
    /**
     * Set the time stamp associated with this event. To be used when an event object is generated
     * but the time of the event is not know at construction time.
     * @param newTimeStamp
     */
    public void setTimeStamp(Date newTimeStamp){
        this.timeStamp.setTime(newTimeStamp.getTime());
    }
    
    private PresentationModelChangeEvents eventType;
    private IMonitorPoint source;
    private Date timeStamp;
    private T value;
}

//
// O_o
