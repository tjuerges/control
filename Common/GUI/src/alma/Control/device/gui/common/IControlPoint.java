/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common;

/**
 * Define the interface for all ControlPoint enums.
 * 
 * @author  Scott Rankin  srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 5.0.3
 */
public interface IControlPoint {
			
	/**
	 * Return the number of elements in a control point array.
	 * 
	 * @return the number of elements in an array.  0 indicates this control point does not contain an array.
	 */
    public int getArrayLength();

	/**
	 * Return the operating mode for this control point.  This identifies when this control point may
	 * be written.
	 * 
	 * @return either "diagnostic", "operational", or "startup".
	 */
	public String getOperatingMode();
	
	/** What is the minimum valid value to set for this control point?
	 * 
	 * @return the integer lower bound for this control point.
	 */
	public Double getRangeLowerBound();
	
	/** What is the maximum valid value to set for this control point?
	 * 
	 * @return the integer upper bound for this control point.
	 */
	public Double getRangeUpperBound();
	
	/**
	 * Identify the class type for this control point.
	 * 
	 * @return the class of this control point.
	 */
	public Class<?> getTypeClass();
	
	/**
	 * Return the world units for this control point.
	 * 
	 * @ return a String containing the world units for this control point.
	 */
	public String getUnits();
	
	/**
	 * Return true if this control point supports range checking.
	 * 
	 * @ return true if this control point supports range checking.
	 */
	public boolean supportsRangeChecking();
	
	/**
	 * Return true if this control point has units.
	 * 
	 * @ return true if this control point has units.
	 */
	public boolean supportsUnits();
}
//
// O_o
