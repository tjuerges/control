/* ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets.util;

import java.text.ParseException;

import javax.swing.text.DefaultFormatter;

/**
 * A formatter for hexadecimal, decimal, and octal input.
 *
 * This formatter limits a JFormattedTextField to valid hex, decimal, and octal
 * numbers. It also provides for range checking if desired.
 *
 * The valid input formats are: Hex: [0x|0X|#][0-9|a-f|A-F]* Dec: [1-9][0-9]*
 * OCt: 0[0-7]*
 *
 * Use of this formatter will cause the JFormattedTextField to reject all other
 * input. See the documentation on JFormattedTextField for the various ways it
 * reacts to invalid input.
 *
 * Sample usage:
 *
 * //Text field to input one byte
 * JFormattedTextField hexField = new JFormattedTextField(new HexFormatter(0, 255));
 *
 * Note that because this formatter used type int internally the max value it
 * can handle is 0x7FFFFFFF.
 *
 * @author David Hunter dhunter@nrao.edu
 * @version $Id$
 * @since ALMA 7.0.0
 */
public class HexFormatter extends DefaultFormatter {

    /**
     * Create and instance of this class with defaults.
     */
    public HexFormatter() {
        super();
    }

    /**
     * Create an instance of this class with the given range.
     *
     * @param min
     *            the closed lower limit of the range interval.
     * @param max
     *            the closed upper limit of the range interval.
     */
    public HexFormatter(final int min, final int max) {
        super();
        upperBound = max;
        lowerBound = min;
    }

    /**
     * Convert a string to an integer.
     *
     * Ensure the integer is in range for this instance.
     *
     * @param text
     *            the string to convert.
     * @return the integer value of the string.
     * @throws ParseException
     *            if the string does not contain a valid integer.
     */
    @Override
    public Object stringToValue(final String text) throws ParseException {
        int result;
        try {
            // If the input is valid then it will decode properly
            result = Integer.decode(text);
        } catch (NumberFormatException n) {
            throw new ParseException("Not a valid hex, dec, or octal input", 0);
        }
        if (result > upperBound || result < lowerBound) {
            throw new ParseException("Value out of bounds", 0);
        }
        return result;
    }

    /**
     * Convert an integer to a String.
     *
     * Ensure the integer is in range for this instance.
     *
     * @param value
     *            the integer to convert to a String
     * @return a String representing the integer value.
     * @throws ParseException
     *             if value is not an integer or if the value is out of range.
     */
    @Override
    public String valueToString(final Object value) throws ParseException {
        if (!(value instanceof Integer)) {
            throw new ParseException("Not a Integer", 0);
        }

        // We do this because < and > don't work on Objects.
        int checkValue = (Integer) value;
        if (checkValue > upperBound || checkValue < lowerBound) {
            throw new ParseException("Value out of bounds", 0);
        }
        return "0x" + (String.format("%x", value)).toUpperCase();
    }

    /**
     * The closed lower limit of the range interval.
     */
    private int lowerBound = Integer.MIN_VALUE;

    /**
     * The closed upper limit of the range interval.
     */
    private int upperBound = Integer.MAX_VALUE;

    /**
     * TODO: Correct handling of <code>serialVersionUID</code>.
     */
    private static final long serialVersionUID = 1L;
}
