/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;

import java.awt.BorderLayout;
import java.util.logging.Logger;

import javax.swing.JLabel;

public class MessageStatusBarWidget extends MonitorPointWidget {

    private static final long serialVersionUID = 1L;
    private JLabel valueLabel;
    private IMonitorPoint monitorPoint;

    public MessageStatusBarWidget(Logger logger, MonitorPointPresentationModel<String> monitorPointPM) {
        super(logger);
        monitorPoint = addMonitorPointPM(monitorPointPM, "");
        buildWidget();
    }

    public void buildWidget() {
        setLayout(new BorderLayout());
        valueLabel = new JLabel();
//        setPreferredSize(new Dimension(0xFFFF, 25));
        add(valueLabel, BorderLayout.WEST);
    }

    @Override
    protected void updateAttention(Boolean value) {
        // Nothing to do in this class.
    }

    @Override
    protected void updateValue(IMonitorPoint source) {
        String s = getCachedValue(monitorPoint);
        String msg = s.split("\\.")[0] + '.';
        if (msg.length()<s.length())
            msg=msg+"..";
        //Add some spaces after each . to make it easer to read the seperate messages.
        //I would rather have each on its own line, but java does not want to do this.
        //As this should be replaced in the next release I am not currently going to
        //take the time to find a way to beat java into submission.
        String toolTip = s.replaceAll("\\.", ".  ");
        valueLabel.setText(msg);
        valueLabel.setToolTipText(toolTip);
    }
}