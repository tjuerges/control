/*
ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2010 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*/
/** 
 * @author  dhunter
 * @version $Id$
 * @since    
 */

package alma.Control.device.gui.common.widgets.util.DtsData;

import javax.swing.JProgressBar;

/**
 * This interface provides for checking the status of a test data FIFO on the DTX or DRX.
 * The monitor points on those modules give FIFO indications in different ways. So we use
 * this interface to standardize the check.
 */

public interface IDataGrabber{

    /**
     * Capture the requested amount of data from the device and put it in the provided DataPacket.
     * Captures are done in cycles of 6 can transactions, which provides 128 bits of data. If not all the
     * requested data can be captured the return value will the percent that actually was captured. The
     * partial set of data will be in the DataPacket, and can be used.
     * 
     * Capture location: There are a few different locations that the data can be captured from.
     *      For the DTX:
     *          0 to capture data directly from the input pins
     *          All other values to capture formatted data
     *      For the DRX:
     *          0 to capture deformatted data before the transfer FIFO
     *          1 to capture data directly from the input pins
     *          All other values to capture deformatted data going to the output.
     * 
     * 
     * Proper managing of the can bus and checking of the FIFO is the job of the implementing class.
     * @param bagOData  the DataPacket to put all the captured data in.
     * @param dataRequested the number of captures to do.
     * @param captureLocation the location to capture the data from (see above)
     * @return the percentage of the requested data that was actually captured.
     */
    public int captureData(DataPacket bagOData, int dataRequested, int captureLocation);
    
    /**
     * This allows the user to provide a progress bar. If one is given, it will be updated with the % complete
     * when captureData(..) is running. If a new one is given it will replace the old one. If null is given
     * then no progress bar will be used. Note that when the constructor runs the progress bar defaults to
     * none.
     * 
     * The progress bar will be updated via EventQueue.invokeLater, so it should be thread safe.
     * 
     * @param newProgressBar The progress bar to update when data is being captures.
     */
    public void setProgressBar(JProgressBar newProgressBar);
    
    /**
     * The process of capturing data can take a long time, so there is a possibility that it will
     * need to be aborted. Upon call of the shutdown command the DataGrabber will promptly cease
     * data capture, clean up, and return the data that it already has.
     * 
     * Note: sending the shutdown command to a DataGrabber that is not currently grabbing data is
     * guaranteed not to be harmful.
     * 
     * Caution: This command does not guaranty that no further data capture transactions will
     * occur, meaning that a few more can transactions may be sent. The number of transactions sent
     * thould be less than 10.
     */
    public void shutdown();
}