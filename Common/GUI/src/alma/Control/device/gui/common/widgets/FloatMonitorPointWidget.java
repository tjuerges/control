/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;

import java.util.logging.Logger;

import javax.swing.JLabel;

/**
 * @author Scott Rankin srankin@nrao.edu
 * @author David Hunter dhunter@nrao.edu
 * @version $Id$
 * @since ALMA 5.0.4
 * 
 *        TODO: Document
 *        TODO: Factor out common behavior with BooleanMonitorPointWidget to a superclass.
 *        TODO: Add indication of out of range data.
 */
public class FloatMonitorPointWidget extends MonitorPointWidget {

    /**Construct a new FloatMonitorPointWidget with the following options:
     * 
     * @param logger The logger to send all logging messages to.
     * @param monitorPointModel The monitor point to display. Must be a point of type float.
     */
    public FloatMonitorPointWidget(Logger logger,
            MonitorPointPresentationModel<Float> model) {
        this(logger, model, false, 4);
    }
    
    /**Construct a new FloatMonitorPointWidget with the following options:
     * 
     * @param logger The logger to send all logging messages to.
     * @param monitorPointModel The monitor point to display. Must be a point of type float.
     * @param includeUnits True to display the units given by the monitor point.
     */
    public FloatMonitorPointWidget(Logger logger,
            MonitorPointPresentationModel<Float> monitorPointModel, boolean includeUnits) {
        this(logger, monitorPointModel, includeUnits, 4);
    }
    
    /**Construct a new FloatMonitorPointWidget with the following options:
     * 
     * @param logger The logger to send all logging messages to.
     * @param monitorPointModel The monitor point to display. Must be a point of type float.
     * @param setPrecision Set the number of decimal places to display.
     */
    public FloatMonitorPointWidget(Logger logger,
            MonitorPointPresentationModel<Float> monitorPointModel, int setPrecision) {
        this(logger, monitorPointModel, false, setPrecision);
    }
    
    /**Construct a new FloatMonitorPointWidget with the following options:
     * 
     * @param logger The logger to send all logging messages to.
     * @param monitorPointModel The monitor point to display. Must be a point of type float.
     * @param includeUnits True to display the units given by the monitor point.
     * @param setPrecision Set the number of decimal places to display.
     */
    public FloatMonitorPointWidget(Logger logger,
            MonitorPointPresentationModel<Float> monitorPointModel, boolean includeUnits, int setPrecision) {    
        super(logger, true, true);
        precision = setPrecision;
        this.includeUnits = includeUnits;
        monitorPoint = addMonitorPointPM(monitorPointModel, Float.valueOf(0.0F));
        toolTipText = getOperatingRangeToolTipText(monitorPointModel);

        buildWidget();
    }

    protected void buildWidget() {
        widgetLabel = new JLabel();
        widgetLabel.setText("0.0");
        if (monitorPoint.supportsRangeChecking())
            widgetLabel.setForeground(StatusColor.FAILURE.getColor());
        else
            widgetLabel.setForeground(StatusColor.NORMAL.getColor());
        widgetLabel.setToolTipText(toolTipText);
        add(widgetLabel);

        addPopupMenuToComponent(widgetLabel);
    }

    public int getPrecision() {
        return precision;
    }

    public void setPrecision(int precision) {
        this.precision = precision;
    }

    @Override
    protected void updateAttention(Boolean q) {
        if (q.booleanValue()) {
            widgetLabel.setForeground(StatusColor.FAILURE.getColor());
        } else {
            widgetLabel.setForeground(StatusColor.NORMAL.getColor());
        }
    }

    @Override
    protected void updateValue(IMonitorPoint source) {
        //We update the scale factor here in case the user does not set it at the time the widget is built.
        scaleFactor = monitorPoint.getDisplayScale();
        offsetFactor = monitorPoint.getDisplayOffset();
        String newOutput="";
        String units = "";
        String formatString="%."+precision+"f";
        float newValue=(Float) getCachedValue(monitorPoint);
        newValue*=scaleFactor;
        newValue+=offsetFactor;
        if (includeUnits == true && source.supportsUnits())
            units = " " + source.getDisplayUnits();
        newOutput=String.format(formatString, newValue);
        widgetLabel.setText(newOutput+units);
    }

    private boolean includeUnits;
    private IMonitorPoint monitorPoint;
    private double offsetFactor;
    private int precision;
    private double scaleFactor;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private String toolTipText;
    private JLabel widgetLabel;
}

//
// O_o
