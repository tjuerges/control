/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2011
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.ControlPointPresentationModel;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;
import javax.swing.JButton;

/**
 * <code>FixedValueControlPointWidget</code> is used to send a single pre-set
 * value or nothing to a control point. This class is type-safe version of
 * {@link BooleanControlPointWidget} and should replace
 * {@link BooleanControlPointWidget}.This widget has just one button.
 * 
 * @author Kohji Nakamura k.nakamura@nao.ac.jp
 * @version $Id$
 * @since ALMA 8.1.0
 */

public class FixedValueControlPointWidget extends JButton implements
		ActionListener {
	private static abstract class ControlKicker<T> {
		final ControlPointPresentationModel<T> model;

		ControlKicker(ControlPointPresentationModel<T> model) {
			this.model = model;
		}

		abstract void kick();
	}

	private static class VoidControlKicker extends ControlKicker<Void> {
		VoidControlKicker(ControlPointPresentationModel<Void> model) {
			super(model);
		}

		void kick() {
			model.setValue();
		}
	}

	private static class ControlKickerWithValue<T> extends ControlKicker<T> {
		final T value;

		ControlKickerWithValue(ControlPointPresentationModel<T> model, T value) {
			super(model);
			this.value = value;
		}

		void kick() {
			model.setValue(value);
		}
	}

	private static final long serialVersionUID = 1L;
	private ControlKicker<?> controlKicker;
	@SuppressWarnings("unused")
	private final Logger logger;

	FixedValueControlPointWidget(Logger logger, String label) {
		this.logger = logger;
		addActionListener(this);
		setText(label);
		// This minimizes the button size (while not squeezing in the text too
		// much).
		setMargin(new Insets(0, 2, 0, 2));
	}

	/**
	 * Use this constructor in case where the control point does not require or
	 * accept any parameters, such as some reset points.
	 * 
	 * @param logger
	 *            The logger to send all logs to.
	 * @param cpPM
	 *            The control point attached to the widget.
	 * @param label
	 *            The text to put on the button.
	 */
	public FixedValueControlPointWidget(Logger logger,
			ControlPointPresentationModel<Void> cpPM, String label) {
		this(logger, label);
		controlKicker = new VoidControlKicker(cpPM);
		cpPM.getContainingDevicePM().addControl(this);
	}

	/**
	 * Use this constructor in case where the given control value should be sent
	 * to the control point when the button is pressed.
	 * 
	 * @param logger
	 *            The logger to send all logs to.
	 * @param cpPM
	 *            The control point attached to the widget.
	 * @param label
	 *            The text to put on the button.
	 * @param value
	 *            The value to be sent to the control point.
	 */
	public <T> FixedValueControlPointWidget(Logger logger,
			ControlPointPresentationModel<T> cpPM, String label, T value) {
		this(logger, label);
		if (value == null) {
			throw new IllegalArgumentException(
					"The 'value' parameter must not be null.");
		}
		controlKicker = new ControlKickerWithValue<T>(cpPM, value);
		cpPM.getContainingDevicePM().addControl(this);
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		controlKicker.kick();
	}
}
