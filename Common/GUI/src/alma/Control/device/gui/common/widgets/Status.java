/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import alma.acs.gui.standards.StandardIcons;

/**
 * List all states available to be indicated.
 * 
 * TODO: Replace with status indicators from ACS.
 * 
 * @author  Scott Rankin  srankin@nrao.edu
 * @author  David Hunter  dhunter@nrao.edu
 * @version $Id$
 */

public enum Status {
	
//	ATYPICAL("/resources/atypical_small.png"),
//	COMM_CONNECTED("/resources/comm_connected.png"),
//	COMM_DISCONNECTED("/resources/comm_disconnected.png"),
//	/* Available statuses */
//	FAILURE("/resources/failure_small.png"),
//	FAILURE_ATTENTION("/resources/failure_attention_small.gif"),
//	GOOD("/resources/good_small.png"),
//	OFF("/resources/off_small.png"),
//	WARNING("/resources/warning_small.png"),
//	WARNING_ATTENTION("/resources/warning_attention_small.gif");

    /* Available statuses */
    ATYPICAL(StandardIcons.STATUS_NOTEWORTHY.icon),
    COMM_CONNECTED("/resources/comm_connected.png"),
    COMM_DISCONNECTED("/resources/comm_disconnected.png"),
    FAILURE(StandardIcons.STATUS_ERROR.icon),
//    FAILURE_ATTENTION("/resources/failure_attention_small.gif"),
    GOOD(StandardIcons.STATUS_OKAY.icon),
    ON("/resources/good_small.png"),
    OFF("/resources/off_small.png"),
    WARNING(StandardIcons.STATUS_WARNING.icon),
//    WARNING_ATTENTION("/resources/warning_attention_small.gif")
    ;

    /* Stores the image to be rendered for this status */
    private ImageIcon indicator;

    /**
     * Status constructor retrieves images for the Status enumeration
     * from the archive this package is located
     * 
     * @param iconPath path to the image for the given instantiation
     */	
    Status(String iconPath) {
        URL iconURL = this.getClass().getResource(iconPath);

        if (null != iconURL) {
            indicator = new ImageIcon(iconURL);
        } else {
            indicator = null;
        }
    }
    
    Status(ImageIcon icon){
        indicator = icon;
    }

    /**
     * getIcon is used to retrieve the image to be displayed for
     * this status
     * 
     * @return returns this status's image indicator
     */
    public Icon getIcon() {		
        return indicator;
    }
}
