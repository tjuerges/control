/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common;

import alma.Control.HardwareDevicePackage.HwState;
import alma.common.log.ExcLog;

import java.util.Date;
import java.util.logging.Logger;

/**
 * Track a read only property of a hardware device to be displayed in a UI.  Isolate the user interface 
 * from the implementation details of a hardware device in the Control system.
 * 
 * @author  Scott Rankin  srankin@nrao.edu   
 * @version $Id$
 * @since   ALMA 5.1.1
 */
public class HwStateMonitorPointPresentationModel extends IdlMonitorPointPresentationModel<HwState> {
    
    public HwStateMonitorPointPresentationModel(Logger logger, DevicePresentationModel container,
                                                IMonitorPoint monitorPoint)
    {super(logger, container, monitorPoint);}
    
    /**
     * Get the current value from the hardware device property this presentation model represents.
     * Note: for the timeStamp we just return the current local time because we don't have a good way to
     *       get the exact time the error occurred. Should a way be found this function should be updated
     *       to return the exact time.
     * @param timeStamp This will be set to the alma time stamp.
     * @return the current value held by the hardware device.
     */
    @Override
    protected HwState getValueFromDevice(Date timeStamp) {
        HwState res = HwState.Undefined;
        Date theTime = new Date();
        try {
            res = deviceComponent.getHwState();
            timeStamp.setTime(theTime.getTime());
        } catch (IllegalArgumentException e) {
            logger.severe("HwStateMonitorPointPresentationModel.getValueFromDevice()" +
                          " - illegal arguments for method" + ExcLog.details(e));
            e.printStackTrace();
        }
        return res;
    }

    @Override
    protected HwState getValueFromDevice() {
        return getValueFromDevice(new Date());
    }
}

//
// O_o
