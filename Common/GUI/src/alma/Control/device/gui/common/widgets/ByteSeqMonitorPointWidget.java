/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;

import java.util.logging.Logger;
import javax.swing.JLabel;

/**
 * @author David Hunter     dhunter@nrao.edu
 * @version $Id$
 * @since ALMA 5.0.4
 * 
 * ByteSeqMonitorPointWidget offers a JPanel containing a textual representation of an byte sequence from an
 * array of bytes.
 *        
 *Usage Notes:
 *    -You can specify that the bytes be printed in decimal or hex format.
 *    
 *    -You can provide any simple string as a separator. For example, if you wanted to display an IP address
 *     you might give a separator of ".", or to display hex data in the format 0xFF BB CD 7F you would give a
 *     separator of " ". If no separator is given the bytes will be printed without any spaces between them.
 *     
 *    -Combining the int format with no separator will give nonsensical results, as each byte is treated as
 *     a separate number. This widget will NOT display the sum of all the bytes as an integer.
 *     
 *    -When dealing with a monitor point that should be an array of bytes, Control software is providing
 *     us with an array of integers. As such, this monitor point uses integers internally, even though it
 *     is intended for use with bytes.
 *     
 *     -You can specify the byte display order (byte 0 on the left or right). The default is byte 0 on the
 *      right.
 */
public class ByteSeqMonitorPointWidget extends MonitorPointWidget {

    private boolean firstByteOnRight=true;
    private boolean isHex = false;
    private IMonitorPoint monitorPoint;
    private String separator;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private JLabel widgetLabel;
    
    /**Construct a new byte sequence monitor point widget
     * 
     * @param logger The logger to send all the logs to.
     * @param monitorPointModel The monitor point to get data from.
     * @param newSeparator The separator to put between each byte.
     * @param displayInHex True to display in hex, false for decimal.
     */
    public ByteSeqMonitorPointWidget(Logger logger, MonitorPointPresentationModel<int[]> monitorPointModel,
            String newSeparator, boolean displayInHex, boolean byteZeroOnRight){
        super(logger, true);
        firstByteOnRight=byteZeroOnRight;
        monitorPoint = addMonitorPointPM(monitorPointModel, new int[8]);
        separator = newSeparator;
        isHex = displayInHex;
        buildWidget();
    }
    
    /**Construct a new byte sequence monitor point widget
     * 
     * @param logger The logger to send all the logs to.
     * @param monitorPointModel The monitor point to get data from.
     * @param newSeparator The separator to put between each byte.
     * @param displayInHex True to display in hex, false for decimal.
     */
    public ByteSeqMonitorPointWidget(Logger logger, MonitorPointPresentationModel<int[]> monitorPointModel,
            String newSeparator, boolean displayInHex){
        this(logger, monitorPointModel, newSeparator, displayInHex, true);
    }

    //Default to hex display if no format given.
    /**Construct a new byte sequence monitor point widget
     * 
     * @param logger The logger to send all the logs to.
     * @param monitorPointModel The monitor point to get data from.
     * @param newSeparator The separator to put between each byte.
     */
    public ByteSeqMonitorPointWidget(Logger logger, MonitorPointPresentationModel<int[]> monitorPointModel,
            String newSeparator){
        this(logger, monitorPointModel, newSeparator, true);
    }
    //Default to no separator if none given
    /**Construct a new byte sequence monitor point widget
     * 
     * @param logger The logger to send all the logs to.
     * @param monitorPointModel The monitor point to get data from.
     * @param displayInHex True to display in hex, false for decimal.
     */
    public ByteSeqMonitorPointWidget(Logger logger, MonitorPointPresentationModel<int[]> monitorPointModel,
            boolean displayInHex){
        this(logger, monitorPointModel, "", displayInHex);
    }
    //Default to no separator and hex format, if neither given
    /**Construct a new byte sequence monitor point widget
     * 
     * @param logger The logger to send all the logs to.
     * @param monitorPointModel The monitor point to get data from.
     */
    public ByteSeqMonitorPointWidget(Logger logger, MonitorPointPresentationModel<int[]> monitorPointModel){
        this(logger, monitorPointModel, "", true);
    }

    protected void buildWidget() {
        widgetLabel = new JLabel();
        widgetLabel.setForeground(StatusColor.NORMAL.getColor());
        add(widgetLabel);
        addPopupMenuToComponent(widgetLabel);
    }
    
    @Override
    public void updateAttention(Boolean value) {
        widgetLabel.setForeground(StatusColor.NORMAL.getColor());
    }

    @Override
    public void updateValue(IMonitorPoint source) {
        int[] currentValue=getCachedValue(monitorPoint);
        int s;
        String newText = new String();
        if (isHex)
            newText+="0x";
        boolean start=true;
        
        //Build up the string, making sure that we put the first byte in the correct position.
        //First case for the first byte going on the right of the display string, and the
        //second case for it going on the left.
        if (firstByteOnRight)
            for (int count=currentValue.length - 1; count>=0; count--){
                s=currentValue[count];
                if (start)
                    start=false;
                else
                    newText += separator;
                if (isHex)
                    newText += String.format("%02X", s);
                else
                    newText += String.format("%d", s);
            }
        else
            for (int count=0; count<=currentValue.length - 1; count++){
                s=currentValue[count];
                if (start)
                    start=false;
                else
                    newText += separator;
                if (isHex)
                    newText += String.format("%02X", s);
                else
                    newText += String.format("%d", s);
            }
        widgetLabel.setText(newText);
    }
    
    public void setSeparator(String newSeperator){
        separator=newSeperator;
    }
    public void setDisplayToHex(){
        isHex=true;
    }
    public void setDisplayToDecimal(){
        isHex=false;
    }
}

//
// O_o
