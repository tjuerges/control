/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common;

/**
 * Define the interface for all MonitorPoint enums.
 * 
 * @author  Scott Rankin  srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 5.0.3
 */
public interface IMonitorPoint {
    
    /**
     * Return the number of elements in a monitor point array.
     * 
     * @return the number of elements in an array.  0 indicates this monitor point does not contain an array.
     */
    public int getArrayLength();
    
    /** Get the common name.
     *  The common name is the name that should normally be used when displaying the monitor point to users.
     *  Should default to the name given in the device spreadsheet, but can be reset by
     *  setDisplayNamed() as needed.
     */
    public String getDisplayName();
    
    /**Get the current display offset for this monitor point.
     * Intended to be used in conjunction with getDisplayScale() and getDisplayUnits() to
     * provide for user friendly unit displays. E.g. �C instead of �K, or nW instead of watts.
     * @return  Returns the current display offset.
     */
    public double getDisplayOffset();
    
    /**Get the current display scale factor for this monitor point.
     * Intended to be used in conjunction with getDisplayOffset() and getDisplayUnits() to
     * provide for user friendly unit displays. E.g. �C instead of �K, or nW instead of watts.
     * @return  Returns the current display scale factor.
     */
    public double getDisplayScale();
    
    /**Get the units associated with the monitor point.
     * Should default to the units given by getUnits() if the display units have not been set.
     * Should return an empty string for monitor points that have no units.
     * Intended to be used in conjunction with setDisplayOffset() and setDisplayScale() to
     * provide for user friendly unit displays. E.g. �C instead of �K, or nW instead of watts.
     * @return
     */
    public String getDisplayUnits();
    
    /**
     * For Boolean monitor points, what is the non-error value expected. 
     * 
     * @return
     */
    public Boolean getExpectedBooleanValue();
    
    /**
     * Get the lower label for the Y axis of graphs of this monitor point.
     * Used to handle properly labeling graphs of boolean monitor points.
     * Defaults to and empty string.
     * @return lower Y-axis label of graphs for this monitor point.
     */
    public String getLowerGraphYLabel();
    
	/**
     * Return the operating mode for this monitor point.  This identifies when this monitor point may
     * be read.
     * 
     * @return either "diagnostic", "operational", or "startup".
     */
    public String getOperatingMode();
    
    /** What is the minimum valid value to set for this monitor point?
     * 
     * @return the integer lower bound for this control point.
     */
    public Double getRangeLowerBound();
    
    /** What is the maximum valid value to set for this monitor point?
     * 
     * @return the integer upper bound for this control point.
     */
    public Double getRangeUpperBound();
    
    /**
     * Identify the class type for this monitor point.
     * 
     * @return the class of this monitor point.
     */
    public Class<?> getTypeClass();
    
    /**
     * Return the world units for this monitor point.
     * 
     * @ return a String containing the world units for this monitor point.
     */
    public String getUnits();
    
    /**
     * Get the upper label for the Y axis of graphs of this monitor point.
     * Used to handle properly labeling graphs of boolean monitor points.
     * Defaults to and empty string.
     * @return upper Y-axis label of graphs for this monitor point.
     */
    public String getUpperGraphYLabel();
    
    /** Overwrite the common name.
     *  The common name is the name that should normally be used when displaying the monitor point to users.
     */
    public void setDisplayName(String newCommonName);
    
    /**
     * Set the new display offset for this monitor point.
     * Will be ignored on boolean points.
     * Intended to be used in conjunction with setDisplayUnits() and setDisplayScale() to
     * provide for user friendly unit displays. E.g. �C instead of �K, or nW instead of watts.
     * @param newOffset  The new offset for this point.
     */
    public void setDisplayOffset(double newOffset);
    
    /**
     * The scale factor for use when displaying this monitor point.
     * Will be ignored on boolean points.
     * Intended to be used in conjunction with setDisplayOffset() and setDisplayUnits() to
     * provide for user friendly unit displays. E.g. �C instead of �K, or nW instead of watts.
     * @param newDisplayScale  The value to set the new scale factor to.
     */
    public void setDisplayScale(double newDisplayScale);
    
    /**
     * The display units default to the spreadsheet units (as provided by getUnits())
     * This function overrides them with a user supplied string.
     * Intended to be used in conjunction with setDisplayOffset() and setDisplayScale() to
     * provide for user friendly unit displays. E.g. �C instead of �K, or nW instead of watts.
     * @param newDisplayUnits  The new units to use when displaying this value.
     */
    public void setDisplayUnits(String newDisplayUnits);
    
    /**
     * Set the lower label for the Y axis of graphs of this monitor point.
     * Used to handle properly labeling graphs of boolean monitor points.
     */
    public void setLowerGraphYLabel(String newLowerGraphYLabel);
    
    /** Override the minimum valid value to set for this monitor point.
     * 
     * @return nothing.
     */
    public void setRangeLowerBound(Double newRangeLowerBound);
    
    /** Override is the maximum valid value to set for this monitor point.
     * 
     * @return nothing.
     */
    public void setRangeUpperBound(Double newRangeUpperBound);
    
    /**
     * Set the upper label for the Y axis of graphs of this monitor point.
     * Used to handle properly labeling graphs of boolean monitor points.
     */
    public void setUpperGraphYLabel(String newUpperGraphYLabel);
    
    /**
     * Return true if this monitor point supports range checking.
     * 
     * @ return true if this monitor point supports range checking.
     */
    public boolean supportsRangeChecking();
    
    /**
     * Return true if this monitor point has units.
     * 
     * @ return true if this monitor point has units.
     */
    public boolean supportsUnits();
}

//
// O_o
