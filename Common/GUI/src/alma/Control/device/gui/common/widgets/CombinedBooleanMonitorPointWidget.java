/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;

import java.util.logging.Logger;

/**
 * <code>CombinedBooleanMonitorPointWidget</code> displays two user defined images to indicate
 * true or false values from its associated monitor point.
 * 
 * @author  David Hunter  dhunter@nrao.edu
 * @author  Scott Rankin  srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 5.0.4
 * 
 * TODO: Document
 * TODO: Factor out common behavior with other widgets to a superclass.
 */
public class CombinedBooleanMonitorPointWidget extends MonitorPointWidget {
    /**
     * <code>CombinedBooleanMonitorPointWidget</code> is the constructor.
     * 
     * @param logger logger to be used for logging
     * @param model monitor point to listen for
     * @param falseStatus status to be used to indicate a false value read from the monitor point
     * @param falseToolTipText tool tip text to be used to indicate a false value was read
     * from the monitor point
     * @param trueStatus status to be used to indicate a true value was read from the monitor point
     * @param trueToolTipText tool tip text to be used to indicate a true value was read
     * from the monitor point
     */
    public CombinedBooleanMonitorPointWidget (
            Logger logger,
            MonitorPointPresentationModel<Boolean>[] monitorPointModels,
            boolean[] theGoodValues,
            Status falseStatus,
            String falseToolTipText,
            Status trueStatus,
            String trueToolTipText
    ) {
        super(logger, true);
        this.falseStatus = falseStatus;
        this.falseToolTipText = falseToolTipText;
        this.trueStatus = trueStatus;
        this.trueToolTipText = trueToolTipText;
        if (monitorPointModels.length != theGoodValues.length)
            logger.severe("CombinedBooleanMonitorPointWidget::CombinedBooleanMonitorPointWidget" +
                    "Error: theGoodValues[] is not the same length as monitorPointModels[]");
        monitorPoints = new IMonitorPoint[monitorPointModels.length];
        for (int c=0; c<monitorPointModels.length; c++)
            monitorPoints[c] = addMonitorPointPM(monitorPointModels[c], Boolean.FALSE);
        goodValues = theGoodValues.clone();
        buildWidget();
    }

    public CombinedBooleanMonitorPointWidget (
            Logger logger,
            MonitorPointPresentationModel<Boolean> monitorPointModel0,
            MonitorPointPresentationModel<Boolean> monitorPointModel1,
            Status falseStatus,
            String falseToolTipText,
            Status trueStatus,
            String trueToolTipText
    ) {
        super(logger, true);
        this.falseStatus = falseStatus;
        this.falseToolTipText = falseToolTipText;
        this.trueStatus = trueStatus;
        this.trueToolTipText = trueToolTipText;
        monitorPoints = new IMonitorPoint[2];
        monitorPoints[0] = addMonitorPointPM(monitorPointModel0, Boolean.FALSE);
        monitorPoints[1] = addMonitorPointPM(monitorPointModel1, Boolean.FALSE);
        buildWidget();
    }

    public CombinedBooleanMonitorPointWidget (
            Logger logger,
            MonitorPointPresentationModel<Boolean> monitorPointModel0,
            MonitorPointPresentationModel<Boolean> monitorPointModel1,
            MonitorPointPresentationModel<Boolean> monitorPointModel2,
            Status falseStatus,
            String falseToolTipText,
            Status trueStatus,
            String trueToolTipText
    ) {
        super(logger, true);
        this.falseStatus = falseStatus;
        this.falseToolTipText = falseToolTipText;
        this.trueStatus = trueStatus;
        this.trueToolTipText = trueToolTipText;
        monitorPoints = new IMonitorPoint[3];
        monitorPoints[0] = addMonitorPointPM(monitorPointModel0, Boolean.FALSE);
        monitorPoints[1] = addMonitorPointPM(monitorPointModel1, Boolean.FALSE);
        monitorPoints[2] = addMonitorPointPM(monitorPointModel2, Boolean.FALSE);
        buildWidget();
    }

    /**
     * buildWidget is a private method which is used to construct the entire widget
     * to be displayed on a panel
     */
    private void buildWidget() {
        statusIndicator = new StatusIndicator(falseStatus, falseToolTipText);
        add(statusIndicator);
        addPopupMenuToComponent(statusIndicator);
    }

    @Override
    protected void updateAttention(Boolean value) {
        // TODO: remove
    }

    @Override
    protected void updateValue(IMonitorPoint aMonitorPoint) {
        boolean newStatus = true;
        //Check the value of all the monitor points. If any value is an unexpected value then
        //we are status false. Otherwise we are status true.
        for (int c=0; c<monitorPoints.length; c++)
            if (goodValues[c] != (Boolean)getCachedValue(monitorPoints[c]))
                newStatus=false;

        //Take the proper action
        if (newStatus) {
            statusIndicator.setStatus(trueStatus, trueToolTipText);
        } else {
            statusIndicator.setStatus(falseStatus, falseToolTipText);
        }
        
    }
    

    private Status falseStatus;
    private String falseToolTipText;
    private boolean[] goodValues = {false, false, false};
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private StatusIndicator statusIndicator;
    private Status trueStatus;
    private String trueToolTipText;
    private IMonitorPoint[] monitorPoints;
}

//
// O_o

