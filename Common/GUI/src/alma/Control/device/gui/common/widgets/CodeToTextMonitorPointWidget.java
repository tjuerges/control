/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;

import java.util.logging.Logger;

import javax.swing.JLabel;

/**
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since ALMA 7.0.0
 * 
 *        CodeToTextMonitorPointWidget offers a means of displaying the proper text message
 *        for an error code. For example, if a monitor point reads 0x2 the widget could
*         display "Form error."
 */
public class CodeToTextMonitorPointWidget extends MonitorPointWidget {

    /**Construct a new CodeToTextMonitorPointWidget
     * 
     * @param logger  The logger that all log messages should be written to
     * @param monitorPointModel  The monitor point that this widget should be attached to
     * @param newTextOptions  The list of codes that should attach to the monitor point
     * 
     * When the default text is not given it is assumed to be " "
     */
    public CodeToTextMonitorPointWidget(Logger logger,
            MonitorPointPresentationModel<Integer> monitorPointModel,
            String[] newTextOptions) {
        this(logger, monitorPointModel, newTextOptions, " ");
    }

    /**Construct a new CodeToTextMonitorPointWidget
     * 
     * @param logger  The logger that all log messages should be written to
     * @param monitorPointModel  The monitor point that this widget should be attached to
     * @param newTextOptions  The list of codes that should attach to the monitor point
     * @param newDefaultText  The default for unknown codes
     */
    public CodeToTextMonitorPointWidget(Logger logger,
            MonitorPointPresentationModel<Integer> monitorPointModel,
            String[] newTextOptions, String newDefaultText) {
        this(logger, monitorPointModel, newTextOptions, newDefaultText, -1, null); //-1 is all 1s in binary 2's complement
    }
    
    /**Construct a new CodeToTextMonitorPointWidget
     * 
     * @param logger  The logger that all log messages should be written to
     * @param monitorPointModel  The monitor point that this widget should be attached to
     * @param newTextOptions  The list of codes that should attach to the monitor point
     * @param newDefaultText  The default for unknown codes
     * @param mask A bit mask to mask out parts of the monitor point that should be ignored.
     * @param newCodes The list of codes that correspond to the given text.
     * 
     * For each bit in the mask, a 0 will mask out the corresponding bit in the monitor point when
     * comparing the monitor value to the codes. A 1 will not.
     * 
     * Giving a value of null for the newCodes defaults the codes to 0, 1, 2, ...
     */
    public CodeToTextMonitorPointWidget(Logger logger,
            MonitorPointPresentationModel<Integer> monitorPointModel,
            String[] newTextOptions, String newDefaultText, int newMask, int[] newCodes) {
        super(logger, true);
        intCodes = newCodes;
        monitorPoint = addMonitorPointPM(monitorPointModel, Integer.valueOf(0));
        textOptions=newTextOptions;
        defaultText=newDefaultText;
        mask=newMask;
        
        if (intCodes == null){          //If the codes are not given then assume they are sequential
            int s=textOptions.length;   //and start with 0.
            int newIntCodes[] = new int[s]; 
            for (int c=0; c<s; c++)
                newIntCodes[c]=c;
            intCodes=newIntCodes;
        }

        buildWidget();
    }

    protected void buildWidget() {
        widgetLabel = new JLabel();
        widgetLabel.setText(defaultText);
        widgetLabel.setForeground(StatusColor.NORMAL.getColor());
        widgetLabel.setVisible(true);
        add(widgetLabel);
        addPopupMenuToComponent(widgetLabel);
    }

    @Override
    protected void updateAttention(Boolean q) {
    }

    @Override
    //TODO: Check if there is a better way to look up values. Maybe a hash?
    protected void updateValue(IMonitorPoint source) {
        int monitorValue=(Integer)getCachedValue(monitorPoint);
        int addressOfText=-1;
        try{
            int count=0;
            while ((monitorValue&mask)!=intCodes[count])
                count++;
            addressOfText=count;
        }
        catch (ArrayIndexOutOfBoundsException Eeeek){
            //Leave value at -1, setting the value to default.       
        }
        try {
          widgetLabel.setText(textOptions[addressOfText]);
        } catch (ArrayIndexOutOfBoundsException eeek) {
          widgetLabel.setText(defaultText);
        }
    }

    private String defaultText;
    private String[] textOptions;
    private int[] intCodes;
    private int mask;
    private IMonitorPoint monitorPoint;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private JLabel widgetLabel;
}

//
// O_o
