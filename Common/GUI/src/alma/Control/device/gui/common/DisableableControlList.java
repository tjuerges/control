/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common;

import java.util.ArrayList;

import javax.swing.JComponent;

/**
 * Collect together a set of GUI controls so they may be enabled or disabled as a group.
 * 
 * @author  Scott Rankin  srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 5.1
 * 
 * TODO: Document
 */
public class DisableableControlList {
	
	private ArrayList<JComponent> controls;
	private boolean enabled = false;
	private boolean runRestricted = false;
	
	public DisableableControlList() {
		controls = new ArrayList<JComponent>();
	}
	
	public void addControl(JComponent control) {
		control.setEnabled(enabled);
		controls.add(control);
	}
	
	public boolean getRunRestricted() { return runRestricted; }
	
	public void setControlsEnabled(boolean enabled) {

		// If runRestricted, disable all controls.  Otherwise, set as commanded
		this.enabled = (runRestricted ? false : enabled);
		
        for (JComponent b: controls)
		    b.setEnabled(this.enabled);
	}
	
	public void setRunRestricted(boolean restricted) {
		this.runRestricted = restricted;
	}

	public boolean getControlsEnabled() {
		// @todo Auto-generated method stub
		return enabled;
	}

}

//
//O_o
