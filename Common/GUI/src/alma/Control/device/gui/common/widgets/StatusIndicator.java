/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import javax.swing.JLabel;

/**
 * StatusIndicator displays a text label accompanied by a graphic to indicate
 * one of four statuses visually
 * 
 * @author Justin Kenworthy jkenwort@nrao.edu
 * @version $Id$
 */
public class StatusIndicator extends JLabel {

    private static final long serialVersionUID = 1L;
    private Status currentStatus;
    private String toolTipText;

    public StatusIndicator() {
        currentStatus = Status.OFF;
        toolTipText = "";
    }

    /**
     * StatusIndicator constructor
     * 
     * @param currentStatus
     *            status to place the indicator in
     * @param toolTipText
     *            tool tip text to be rendered on mouse hover
     */
    public StatusIndicator(Status currentStatus, String toolTipText) {
        setStatus(currentStatus, toolTipText);
        update();
    }

    /**
     * setStatus changes and updates the status of this indicator as well as the
     * tool tip text to be used
     * 
     * @param currentStatus
     *            status to change to
     * @param toolTipText
     *            new tool tip text to be rendered on mouse hover
     */
    public void setStatus(Status currentStatus, String toolTipText) {
        this.currentStatus = currentStatus;
        this.toolTipText = toolTipText;

        update();
    }

    /**
     * update is used internally to change its associated image and tool tip
     * text
     */
    private void update() {
        if (null != currentStatus.getIcon()) {
            setIcon(currentStatus.getIcon());
        } else {
            setText("<NO IMAGE>");
        }

        setToolTipText(toolTipText);
    }
}
