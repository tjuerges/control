/* ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets.util;

import java.text.ParseException;

import javax.swing.text.DefaultFormatter;

/**
 * A formatter for inputing even powers of two
 *
 * This formatter limits a JFormattedTextField to a integer power of two. In addition, it allows for
 * optional lower and upper bounds to be given. If they are, the value will also be limited to 
 * those bounds. When an unacceptable number is entered it is rounded to the nearest acceptable
 * value.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @version 
 * @since ALMA 8.0.0
 */
public class PowerOfTwoFormatter extends DefaultFormatter {

    /**
     * Create and instance of this class with defaults.
     */
    public PowerOfTwoFormatter() {
        super();
    }

    /**
     * Create an instance of this class with the given range.
     *
     * @param min
     *            the closed lower limit of the range interval. Must be at least 2; will be set
     *            to 2 if the given value is less than 2.
     * @param max
     *            the closed upper limit of the range interval.
     */
    public PowerOfTwoFormatter(final int min, final int max) {
        super();
        if (upperBound>2)
            upperBound = max;
        else
            upperBound=2;
        if (lowerBound>1)
            lowerBound = min;
    }

    /**
     * Convert a string to an integer.
     *
     * Ensure the integer is in range for this instance.
     *
     * @param text
     *            the string to convert.
     * @return the integer value of the string.
     * @throws ParseException
     *            if the string does not contain a valid integer.
     */
    @Override
    public Object stringToValue(final String text) throws ParseException {
        int result;
        try {
            // If the input is valid then it will decode properly
            result = Integer.decode(text);
        } catch (NumberFormatException n) {
            throw new ParseException("Not a valid hex, dec, or octal input", 0);
        }
        
        if (result > upperBound)
            result=upperBound;
        else if (result < lowerBound)
            result=lowerBound;
        
        //The algorithm below won't work with a 1.
        if (result==1)
            return 1;
        
        //We need to round to the nearest power of 2. We could use a base 2 log, but that would
        //not give us the nearest power of 2.
        //Do bit-shift loop to find highest bit. In theory this should take less processing power
        //than a log. Not that anyone cares. Knowing what place the highest bit is in will tell us 
        //what the nearest power of 2 less than or equal to our number is
        int tmp=result;
        int c=0;
        //This will set C to the nearest power of 2 less than or equal to our number.
        while (tmp >1){
            tmp=tmp>>1;
            c++;
        }
        //Check 2nd highest bit of the number. It tells us if we should round up or not.
        //This won't work if the initial value is 1, but we catch that case above.
        tmp=result;
        //Trim off the lower bits
        for (int count=1; count<c; count++)
            tmp=tmp>>1;
        //Trim off the top bit, which we know is set
        tmp = tmp & 0x01;
        //Round up to the next power of 2 if needed.
        if (tmp!=0)
            c++;
        result=1;
        for (int count=0; count<c; count++)
            result=result<<1;
        //Re-check bounds, bring within them. What about 0?
        if (result>upperBound)
            result=result>>1;
        else if (result<lowerBound)
            result=result<<1;
        if (result==0)
            result=1;
        return result;
    }

    /**
     * Convert an integer to a String.
     *
     * Ensure the integer is in range for this instance.
     *
     * @param value
     *            the integer to convert to a String
     * @return a String representing the integer value.
     * @throws ParseException
     *             if value is not an integer or if the value is out of range.
     */
    @Override
    public String valueToString(final Object value) throws ParseException {
        if (value==null) {
            throw new ParseException("Null value", 0);
        }

        // We do this because < and > don't work on Objects.
        int checkValue = (Integer) value;
        if (checkValue > upperBound || checkValue < lowerBound) {
            throw new ParseException("Value out of bounds", 0);
        }
        return String.format("%d", value);
    }

    /**
     * The closed lower limit of the range interval. Defaults to 1, since this formatter will
     * only produce integers - no floats and no complex numbers.
     */
    private int lowerBound = 1;

    /**
     * The closed upper limit of the range interval.
     */
    private int upperBound = Integer.MAX_VALUE;

    /**
     * TODO: Correct handling of <code>serialVersionUID</code>.
     */
    private static final long serialVersionUID = 1L;
}
