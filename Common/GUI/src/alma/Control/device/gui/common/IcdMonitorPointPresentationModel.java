/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common;

import org.omg.CORBA.LongHolder;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;
import alma.acs.time.TimeHelper;

/**
 * Common behavior and properties for all Monitor Point Presentation Models that can be code generated from the ICD.
 * 
 * @param <T> the type of the property.  This must be a Java Class type, not a primitive type.
 * 
 * @author  Scott Rankin  srankin@nrao.edu   
 * @version $Id$
 * @since   ALMA 5.0.3
 * 
 * TODO: Add ExcLog to logged exceptions.
 */
public abstract class IcdMonitorPointPresentationModel<T> extends MonitorPointPresentationModel<T> {

    private String monitorPointName;
    protected Method monitorPointUpdateMethod;
    protected String monitorPointUpdateMethodName;

    public IcdMonitorPointPresentationModel(Logger logger, DevicePresentationModel container, 
                                            IMonitorPoint monitorPoint)
    {
        super(logger, container, monitorPoint);
        this.monitorPointName = monitorPoint.toString();
        this.monitorPointUpdateMethodName = "GET_" + monitorPointName;
    }

    @SuppressWarnings({ "unchecked", "boxing" }) // PresentationModelChangeEvent can not be parameterized at this point. 
    @Override
    public void checkDeviceForUpdates() {
        T newValue;
        Date timeStamp = new Date();
        ArrayList<PresentationModelChangeEvent> events = 
            new ArrayList<PresentationModelChangeEvent>();

        if (null != deviceComponent) {
            newValue = getValueFromDevice(timeStamp);

            if (null == newValue) {
                logger.warning("IcdMonitorPointPresentationModel.queryForStateChanges()" +
                               "lost communication with device");
            } else {
                if (value != newValue) {
                    value = newValue;
                    events.add(newChangeEvent(PresentationModelChangeEvents.VALUE_CHANGE,
                            value, timeStamp));
                    if (monitorPoint.supportsRangeChecking()) {
                        if (value instanceof java.lang.Number) {
                            if (((Number)value).doubleValue() < monitorPoint.getRangeLowerBound() || 
                                    ((Number)value).doubleValue() > monitorPoint.getRangeUpperBound()) {
                                events.add(rangeAttention);
                            } else {
                                events.add(rangeOK);
                            }
                        }
                        if (value instanceof java.lang.Boolean) {
                            if ((Boolean)value != monitorPoint.getExpectedBooleanValue()) 
                                events.add(rangeAttention);
                            else
                                events.add(rangeOK);
                        }
                    }
                } else {
                    // No change, nothing to do.
                }
            }
        }

        notifyListeners(events);
    }

    /**
     * Get the current value from the device property this presentation model represents.
     * @return the current value held by the hardware device.
     */
    @SuppressWarnings("unchecked")
    public T getValueFromDevice() {
        return getValueFromDevice(new Date());
    }

    /**
     * Get the current value from the device property this presentation model represents.
     * @return the current value held by the hardware device.
     */
    @SuppressWarnings("unchecked")
    public T getValueFromDevice(Date timeStamp) {
        
        if (deviceComponent == null) {
            System.out.println("IcdMonitorPointPresentationModel.getValueFromDevice(): deviceComponent == null");
        }
        
        LongHolder l = new LongHolder(0);
        
        Object o = null;
        T res;
        try {
            o = this.monitorPointUpdateMethod.invoke(deviceComponent, l);
        } catch (IllegalArgumentException e) {
            logger.severe(
                    "IcdMonitorPointPresentationModel.getValueFromDevice()" +
                    " - illegal arguments for method: " + 
                    monitorPointName);
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            logger.severe(
                    "IcdMonitorPointPresentationModel.getValueFromDevice()" +
                    " - illegal access to method: " + 
                    monitorPointName);
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            logger.severe(
                    "IcdMonitorPointPresentationModel.getValueFromDevice()" +
                    " - illegal access to object when calling: " + 
                    monitorPointName);
            e.printStackTrace();
        }
        // This unsafe cast is the only way I can find to return the correct type value.
        // TODO: Find a better way to return the correct type value.
        res = (T)o;
        timeStamp.setTime(TimeHelper.utcOmgToJava(l.value));
        return res;
    }
}

//
// O_o
