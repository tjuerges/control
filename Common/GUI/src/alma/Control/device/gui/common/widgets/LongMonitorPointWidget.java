/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;

import java.util.logging.Logger;

import javax.swing.JLabel;

/**
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id: LongMonitorPointWidget.java,v 1.14 2009/03/06 01:00:06
 *          srankin Exp $
 * @since ALMA 5.0.4
 * 
 *        LongMonitorPointWidget offers a JPanel containing a textual
 *        representation of an long integer from a monitor point with range and
 *        communication verification/alarms
 * 
 *        TODO: Document TODO: Factor out common behavior with
 *        BooleanMonitorPointWidget to a superclass. TODO: Add indication of out
 *        of range data.
 */
public class LongMonitorPointWidget extends MonitorPointWidget {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    boolean includeUnits;
    private String toolTipText;
    private JLabel widgetLabel;
    private IMonitorPoint monitorPoint;

    public LongMonitorPointWidget(Logger logger,
            MonitorPointPresentationModel<Long> monitorPointModel) {
        this(logger, monitorPointModel, false);
    }

    public LongMonitorPointWidget(Logger logger,
            MonitorPointPresentationModel<Long> monitorPointModel,
            boolean includeUnits) {
        super(logger, true);
        monitorPoint = addMonitorPointPM(monitorPointModel, Long.valueOf(0));
        toolTipText = getOperatingRangeToolTipText(monitorPointModel);
        this.includeUnits = includeUnits;
        buildWidget();
    }

    protected void buildWidget() {
        widgetLabel = new JLabel();
        widgetLabel.setText("0");
        if (monitorPoint.supportsRangeChecking())
            widgetLabel.setForeground(StatusColor.FAILURE.getColor());
        else
            widgetLabel.setForeground(StatusColor.NORMAL.getColor());
        widgetLabel.setToolTipText(toolTipText);
        add(widgetLabel);
        addPopupMenuToComponent(widgetLabel);
    }

    @Override
    protected void updateAttention(Boolean q) {
        if (q.booleanValue()) {
            widgetLabel.setForeground(StatusColor.FAILURE.getColor());
        } else {
            widgetLabel.setForeground(StatusColor.NORMAL.getColor());
        }
    }

    @Override
    protected void updateValue(IMonitorPoint source) {
        logger.entering("LongMonitorPointWidget", "updateValue");
        String units = "";
        if (includeUnits && source.supportsUnits()) {
            units = " " + source.getUnits();
        }
        widgetLabel.setText(Long.toString((Long) getCachedValue(monitorPoint))+units);
    }
}

//
// O_o
