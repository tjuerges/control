/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;
import alma.Control.device.gui.common.PresentationModelChangeEvent;
import alma.Control.device.gui.common.PresentationModelChangeListener;

import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * @author Scott Rankin srankin@nrao.edu
 * @author David Hunter shunter@nrao.edu
 * @version $Id$
 * @since ALMA 5.0.4
 * 
 *        Base class for all Monitor Point Widgets.
 * 
 */
public abstract class MonitorPointWidget extends JPanel implements
        PresentationModelChangeListener {

    /**
     * Construct a new monitor point widget.
     * @param <T>  The data type for this monitor point
     * @param logger  The logger to send logs to.
     */
    public <T> MonitorPointWidget(Logger logger) {
        this(logger, false);
    }
    
    /**
     * Construct a new monitor point widget.
     * @param <T>  The data type for this monitor point
     * @param logger  The logger to send logs to.
     * @param generatePopupMenu  True if there should be a pop-up menu for this monitor point.
     */
    public <T> MonitorPointWidget(Logger logger, boolean generatePopupMenu){
        this(logger, generatePopupMenu, false);
    }

    /**
     * Construct a new monitor point widget.
     * @param <T>  The data type for this monitor point
     * @param logger  The logger to send logs to.
     * @param generatePopupMenu  True if there should be a pop-up menu for this monitor point.
     * @param isGraphable  True if this monitor point should have a graph option in the pop-up menu.
     */
    public <T> MonitorPointWidget(Logger logger, boolean generatePopupMenu, boolean isGraphable) {
        this.logger = logger;

        cacheMap = new HashMap<IMonitorPoint, CachedValue<?>>();
        if (generatePopupMenu) {
            popupMenu = new WidgetPopupMenu(logger, isGraphable);
        } else {
            popupMenu = null;
        }
        monitorPointList = new ArrayList<MonitorPointPresentationModel>(2);
        //Using the grid bag layout helps cut down on the extra empty space around the widget!
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createEmptyBorder(1, 2, 0, 1));
    }
    
    public void handlePresentationModelChangeEvent(
            PresentationModelChangeEvent<?> event) {

        switch (event.getEventType()) {
        case ATTENTION_CHANGE:
            if (event.getValue() instanceof Boolean) {
                updateAttention((Boolean) event.getValue());
            } else {
                logger.warning("MonitorPointWidget.handleMonitorPointChangeEvent(): " +
                               "received a non-boolean value with an ATTENTION_CHANGE event");
            }
            break;
        case VALUE_CHANGE:
            if (true) {
                CachedValue<?> cache = cacheMap.get(event.getSource());
                if (cache != null) {
                    cache.setValue(event.getValue(), event.getTimeStamp());
                    updateValue(event.getSource());
                }
            } else {
                logger.warning("MonitorPointWidget.handleMonitorPointChangeEvent(): " +
                               "received a wrong type with an VALUE_CHANGE event");
            }
            break;
        }

    }
    
    
    protected <T> IMonitorPoint addMonitorPointPM(
            MonitorPointPresentationModel<T> monitorPointPM, T init_value) {
        IMonitorPoint monitorPoint = monitorPointPM.getMonitorPoint();
        CachedValue<T> cache = new CachedValue<T>(init_value, new Date(0));

        cacheMap.put(monitorPoint, cache);
        monitorPointPM.addListener(this);
        if (popupMenu != null) {
            popupMenu.addMonitorPointPM(monitorPointPM);
        }
        monitorPointList.add(monitorPointPM);
        return monitorPoint;
    }

    protected void addPopupMenuToComponent(JComponent component) {
        if (popupMenu != null) {
            component.addMouseListener(popupMenu);
        }
    }

    /**
     * Retrieve the latest cached monitor value for the given monitor point.
     * @param <T>  The data type of this monitor point
     * @param key  The monitor point to get the value of
     * @return  The latest cached value
     */
    protected <T> T getCachedValue(IMonitorPoint key) {
        CachedValue<T> cache = (CachedValue<T>) cacheMap.get(key);
        if (cache != null) {
            return cache.getValue();
        }
        return null;
    }
    
    /**
     * Retrieve the latest cached monitor value for the given monitor point.
     * @param <T>  The data type of this monitor point
     * @param key  The monitor point to get the value of
     * @param timeStamp This Long will be set to the associated time stamp. 
     * @return  The latest cached value
     * 
     * Note: If the associated time is not known, timeStamp will be set to 0
     */
    protected <T> T getCachedValue(IMonitorPoint key, Date timeStamp) {
        CachedValue<T> cache = (CachedValue<T>) cacheMap.get(key);
        if (cache != null) {
            timeStamp.setTime(cache.getTimeStamp().getTime());
            return cache.getValue();
        }
        return null;
    }

    protected String getOperatingRangeToolTipText(
            MonitorPointPresentationModel<?> monitorPointPM) {
        String toolTipText = "Operating range: ";
        IMonitorPoint monitorPoint = monitorPointPM.getMonitorPoint();

        if (monitorPoint.supportsRangeChecking()) {
            Double lowerBound = monitorPoint.getRangeLowerBound()+monitorPoint.getDisplayOffset();
            Double upperBound = monitorPoint.getRangeUpperBound()+monitorPoint.getDisplayOffset();
            toolTipText += String.format("%3.2f to %3.2f", lowerBound, upperBound);
            } else {
            toolTipText += "N/A";
        }

        return toolTipText;
    }
    /**
     * This function removes this monitor point from all MonitorPointPresentationModel change
     * listener lists. If needed, it should be used when an instance of the widget is being
     * removed.
     */
    @SuppressWarnings("unchecked")
    protected void removeListeners(){
        for (MonitorPointPresentationModel q : monitorPointList)
            q.removeListener(this);
    }

    protected abstract void updateAttention(Boolean q);

    protected abstract void updateValue(IMonitorPoint source);


    private class CachedValue<E> {

        public CachedValue(E value, Date newTimeStamp) {
            this.value = value;
            timeStamp = new Date(newTimeStamp.getTime());
        }
        
        public Date getTimeStamp(){
            return new Date(timeStamp.getTime());
        }

        public E getValue() {
            return value;
        }

        public void setValue(Object value, Date newTimeStamp) {
            this.value = (E) value;
            timeStamp.setTime(newTimeStamp.getTime());
        }

        private E value;
        private Date timeStamp;
    }

    
    protected Logger logger;

    private HashMap<IMonitorPoint, CachedValue<?>> cacheMap;
    private ArrayList<MonitorPointPresentationModel> monitorPointList;
    private WidgetPopupMenu popupMenu;
    private static final long serialVersionUID = 1L;
}
