/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;

import java.util.logging.Logger;
import javax.swing.JLabel;
import java.math.BigInteger;

/**
 * This widget takes in an array of 8 ints, treats them as an array of bytes representing a long, and displays it. 
 * 
 * Currently the only place this is useful is with the Ambsi Serial Number.
 * 
 * @author David Hunter     dhunter@nrao.edu
 * @version $Id$
 * @since ALMA 5.0.4
 */
public class AmbsiSnMonitorPointWidget extends MonitorPointWidget {

    public AmbsiSnMonitorPointWidget(Logger logger, MonitorPointPresentationModel<int[]> monitorPointModel) {
        super(logger, true);
        monitorPoint = addMonitorPointPM(monitorPointModel, new int[8]);
        buildWidget();
    }

    @Override
    public void updateAttention(Boolean value) {
        widgetLabel.setForeground(StatusColor.NORMAL.getColor());
    }

    @Override
    public void updateValue(IMonitorPoint source) {
        /* Note: The BigInteger code is used because the serial number is an unsigned 64 bit number. Were we
         * to use a Long, we would improperly report a negative SN if the MSB was a 1.
         */
        final int[] currentValue = getCachedValue(monitorPoint);
        BigInteger newValue = BigInteger.valueOf(0);
        BigInteger tmp;
        int count = 0;
        int c = 0;
        int count2 = 0;
        for (count2 = 7; count2 >= 0; count2--) {
            c = currentValue[count2];
            tmp = BigInteger.valueOf(c);
            newValue = newValue.add(tmp.shiftLeft(count));
            count += 8;
        }
        widgetLabel.setText(newValue.toString());
    }

    
    protected void buildWidget() {
        widgetLabel = new JLabel();
        widgetLabel.setForeground(StatusColor.NORMAL.getColor());
        add(widgetLabel);
        addPopupMenuToComponent(widgetLabel);
    }


    // TODO - serial version UID
    private IMonitorPoint monitorPoint;
    private static final long serialVersionUID = 1L;
    private JLabel widgetLabel;
}

//
// O_o
