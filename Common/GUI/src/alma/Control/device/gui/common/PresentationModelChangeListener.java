/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common;

/**
 * An interface for listening for changes to a <code>PresentationModel</code>.
 * 
 * @author  Scott Rankin  srankin@nrao.edu   
 * @version $Id$
 * @since   ALMA 5.1.1
 */
public interface PresentationModelChangeListener {

	public abstract void handlePresentationModelChangeEvent(PresentationModelChangeEvent<?> event);
	
}

//
// O_o
