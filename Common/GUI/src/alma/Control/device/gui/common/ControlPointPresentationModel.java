/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common;

import alma.Control.HardwareDeviceOperations;

import java.util.logging.Logger;

/**
 * Common behavior and properties for all Control Point Presentation Models.
 * 
 * @author  Scott Rankin  srankin@nrao.edu
 * @author  Steve Harrington sharring@nrao.edu
 * @version $Id$
 * @since   ALMA 5.0.3
 */
public abstract class ControlPointPresentationModel<T> extends PresentationModel<T> {

    protected IControlPoint controlPoint;
    @SuppressWarnings("unchecked") // Class may not be parameterized at this point. 
	protected Class controlPointClass;
  
    @SuppressWarnings("unchecked") // Class may not be parameterized at this point. 
	public ControlPointPresentationModel(
    	Logger logger, 
    	DevicePresentationModel containingDevicePM, 
    	IControlPoint controlPoint, 
    	Class controlPointClass
    ) {
    	super(logger, containingDevicePM);
        this.controlPoint = controlPoint;
        this.controlPointClass = controlPointClass;
    }
    
    public IControlPoint getControlPoint() {
    	return controlPoint;
    } 
    
    @Override
	public void setDeviceComponent(HardwareDeviceOperations deviceComponent) {
        this.deviceComponent = deviceComponent;
        setUpdateMethod();
    }

    public abstract void setValue(T value);
    public abstract void setValue();
}

//
// O_o
