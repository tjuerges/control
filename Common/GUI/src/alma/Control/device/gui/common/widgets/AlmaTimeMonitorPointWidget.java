/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;
import alma.acs.time.TimeHelper;

import java.util.GregorianCalendar;
import java.util.logging.Logger;

import javax.swing.JLabel;

/**
 * @author David Hunter dhunter@nrao.edu
 * @version $Id$
 * @since ALMA 5.0.4
 * 
 *        AlmaTimeMonitorPointWidget converts an alma time stamp into a human redable time and
 *        displays it.
 */
public class AlmaTimeMonitorPointWidget extends MonitorPointWidget {
    

    public AlmaTimeMonitorPointWidget(Logger logger,
            MonitorPointPresentationModel<Integer> monitorPointModel) {
        super(logger, true, false);
        monitorPoint = addMonitorPointPM(monitorPointModel, Integer.valueOf(0));
        toolTipText = getOperatingRangeToolTipText(monitorPointModel);

        buildWidget();
    }

    protected void buildWidget() {
        widgetLabel = new JLabel();
        widgetLabel.setText("0");
        if (monitorPoint.supportsRangeChecking())
            widgetLabel.setForeground(StatusColor.FAILURE.getColor());
        else
            widgetLabel.setForeground(StatusColor.NORMAL.getColor());
        widgetLabel.setToolTipText(toolTipText);
        add(widgetLabel);
        addPopupMenuToComponent(widgetLabel);
    }

    @Override
    protected void updateAttention(Boolean q) {
        if (q.booleanValue()) {
            widgetLabel.setForeground(StatusColor.FAILURE.getColor());
        } else {
            widgetLabel.setForeground(StatusColor.NORMAL.getColor());
        }
    }

    @Override
    protected void updateValue(IMonitorPoint source) {
        GregorianCalendar g = new GregorianCalendar();
        g.setTimeInMillis(TimeHelper.utcOmgToJava((Long)getCachedValue(monitorPoint)));
        widgetLabel.setText(String.format("%tc", g.getTime()));
    }

    private IMonitorPoint monitorPoint;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private String toolTipText;
    private JLabel widgetLabel;
}

//
// O_o
