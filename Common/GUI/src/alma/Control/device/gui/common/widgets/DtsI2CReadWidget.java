/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;
import alma.Control.device.gui.common.DevicePresentationModel;

import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.SwingWorker;

import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * @author David Hunter     dhunter@nrao.edu
 * @version $Id$
 * @since ALMA 5.0.4
 * 
 * DtsI2CReadWidget provides a widget that reads from the DTX & DRX I2C interface using the
 * GET_I2C_DATA as specified in the ICD for said devices. 
 */
public class DtsI2CReadWidget extends MonitorPointWidget {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private JLabel firstValue;
    private JLabel secondValue;
    private JLabel thirdValue;
    private JButton readValue;
    private IMonitorPoint monitorPoint;
    DevicePresentationModel devicePresentationModel;  //We need this for the enable/disable button to work.
    
    /**Creates an I2C interface widget attached to the given monitor point in the given device.
     * @param logger The logger that all logging messages generated in this class should go to.
     * @param dp The device presentation model for the current device.
     * @param monitorPointModel The monitor point that this widget should be attached to.
     */
    public DtsI2CReadWidget(Logger logger, DevicePresentationModel dp,
            MonitorPointPresentationModel<int[]> monitorPointModel){
        super(logger, true);
        monitorPoint = addMonitorPointPM(monitorPointModel, new int[8]);
        dp.moveMonitorPointToIdleGroup(monitorPoint);
        devicePresentationModel=dp;
        //This button is not added to the disable controls stuff because it does not go to a control point.
        readValue=new JButton("Read I2C");
        readValue.addActionListener(new readAction());
        devicePresentationModel.addControl(readValue);
        buildWidget();
    }

    /*
     * Required by parent class. Unused because this point has no range checking.
     * @see alma.Control.device.gui.common.widgets.MonitorPointWidget#updateAttention(java.lang.Boolean)
     */
    @Override
    public void updateAttention(Boolean value) {
        //Not applicable to the DTS I2C monitor point
    }

    /**Required by parent class; unused because this widget only reads upon request.
     * @see alma.Control.device.gui.common.widgets.MonitorPointWidget#updateValue(alma.Control.device.gui.common.IMonitorPoint)
     */
    @Override
    public void updateValue(IMonitorPoint source) {
    }

    /* This function sets up the widget panel and initializes the widget to a default value of 0.
     * It should only be called by the constructor. 
     */
    protected void buildWidget() {
        this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Read I2C"),
                BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridy=0;
        c.gridx=0;
        firstValue=new JLabel("0x 00-00-00-00-00-00-00-00");
        add(firstValue, c);
        c.gridy++;
        secondValue=new JLabel("0x 00-00-00-00-00-00-00-00");
        add(secondValue, c);
        c.gridy++;
        thirdValue=new JLabel("0x 00-00-00-00-00-00-00-00");
        add(thirdValue, c);
        c.gridy++;
        add(readValue, c);
    }

    /** The action that gets attached to the read data button.
     * Simple spawns off a readDataWorker to do the work.
     * @author dhunter
     *
     */
    private class readAction implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            SwingWorker<Object, Object> myWorker = new readDataWorker();
            myWorker.execute();
        }
    }
    
    /**A swing worker that reads the data.
     * Reading the I2C buss takes long enough that we want it done in a worker thread.
     * Uses the I2C commands as descriped in the device ICD.
     * 
     * Note: there is no progress bar. It is assumed that this will complete quickly enough that we don't
     * need one. If it does not, then we may need to add a progress bar.
     * @author dhunter
     */
    private class readDataWorker extends SwingWorker<Object, Object>{
        protected Object doInBackground() throws Exception {
            try{                //We need to wait at least 100 ms between set and read. 
                Thread.sleep(100);    //This should be enough.
            }catch (InterruptedException eeek){
                logger.warning("DtsEepromReadWidget: setvalue, failure to sleep. I2C data may be bad.");
            }
            devicePresentationModel.manuallyPollIdleMonitorPoint(monitorPoint);
            int[] currentValue=getCachedValue(monitorPoint);
            int s;
            String newText="0x ";
            boolean start=true;
            for (int count=0; count<currentValue.length; count++){
                s=currentValue[count];
                if (start)
                    start=false;
                else
                    newText += "-";
                newText += String.format("%02X", s);
            }
            final String text1=newText;
            EventQueue.invokeLater(new Runnable(){@Override public void run() {firstValue.setText(text1);}});
            int length=currentValue[1];
            //Check if we need 2 reads, do a 2nd if needed.
            if (length>6)
            {
                devicePresentationModel.manuallyPollIdleMonitorPoint(monitorPoint);
                try {
                    Thread.sleep(48);
                } catch (InterruptedException eeek) {
                    logger.severe("DtsEepromReadWidget: setvalue, failure to sleep. I2C data may be bad.");
                }
                currentValue=getCachedValue(monitorPoint);
                newText="0x ";
                start=true;
                for (int count=0; count<currentValue.length; count++){
                    s=currentValue[count];
                    if (start)
                        start=false;
                    else
                        newText += "-";
                    newText += String.format("%02X", s);
                }
            } else
                newText="0x 00-00-00-00-00-00-00-00";
            final String text2 = newText;
            EventQueue.invokeLater(new Runnable()
                                  {@Override public void run() {secondValue.setText(text2);}});
            //Check if we need 3 reads, do a 3rd if needed.
            if (length>14)
            {
                devicePresentationModel.manuallyPollIdleMonitorPoint(monitorPoint);
                try {Thread.sleep(48);}
                catch (InterruptedException eeek)
                    {logger.severe("DtsEepromReadWidget: setvalue, failure to sleep. I2C data may be bad.");}
                currentValue=getCachedValue(monitorPoint);
                newText="0x ";
                start=true;
                for (int count=0; count<currentValue.length; count++){
                    s=currentValue[count];
                    if (start)
                        start=false;
                    else
                        newText += "-";
                    newText += String.format("%02X", s);
                }
            } else {
                newText="0x 00-00-00-00-00-00-00-00";
            }
            final String text3 = newText;
            EventQueue.invokeLater(new Runnable(){@Override public void run() {thirdValue.setText(text3);}});
            return null;
        }
    }
}

//
// O_o
