/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.HardwareDevicePackage.HwState;

/**
 * List all Monitor Points implemented in this device.
 *
 * See IMonitorPoint for docs.
 * 
 * @author  Scott Rankin  srankin@nrao.edu
 * @author  David Hunter  dhunter@nrao.edu
 * @since   ALMA 5.0.3
 */
 
public enum CommonIdlMonitorPoints implements IMonitorPoint {


    getHwState(HwState.class, "operational"),
    inErrorState(boolean.class, "operational"),
    getErrorMessage(String.class, "operational");
    
    @SuppressWarnings("unchecked") // Class can not be parameterized at this point. 
	private CommonIdlMonitorPoints (
        Class classType, 
        String operatingMode
    ) {
        this.classType = classType;
        this.operatingMode = operatingMode;
    }
    
    public int getArrayLength() {
        return 0;
    }

    public String getDisplayName() {
        return this.commonName;
    }

    public double getDisplayOffset() {
        return this.displayOffset;
    }

    public double getDisplayScale() {
        return this.displayScale;
    }
    
    public String getDisplayUnits() {
        return this.displayUnits;
    }

    public Boolean getExpectedBooleanValue() {
        return Boolean.FALSE;
    }
    
    public String getOperatingMode() {
        return operatingMode;
    }

    public String getLowerGraphYLabel() {
        return lowerGraphYLabel;
    }

    
    public Double getRangeLowerBound() {
        return null;
    }

    public Double getRangeUpperBound() {
        return null;
    }
    
    @SuppressWarnings("unchecked") // Class can not be parameterized at this point. 
    public Class getTypeClass() {
        return classType;
    }

    public String getUnits() {
        return null;
    }
    
    public String getUpperGraphYLabel() {
        return upperGraphYLabel;
    }
    
    public void setDisplayName(String newCommonName) {
        this.commonName=newCommonName;
    }

    public void setDisplayScale (double newScale) {
        this.displayScale = newScale;
    }

    public void setDisplayOffset (double newOffset) {
        this.displayOffset = newOffset;
    }
    
    public void setDisplayUnits (String newUnits){
        this.displayUnits=newUnits;
    }

    public void setLowerGraphYLabel(String newLowerGraphYLabel) {
        this.lowerGraphYLabel = newLowerGraphYLabel;
    }

    public void setRangeLowerBound(Double newRangeLowerBound) {
        // Range checking is not supported.  Do nothing.
    }
    
    public void setRangeUpperBound(Double newRangeUpperBound) {
        // Range checking is not supported.  Do nothing.
    }

    public void setUpperGraphYLabel(String newUpperGraphYLabel) {
        this.upperGraphYLabel = newUpperGraphYLabel;
    }

    public boolean supportsRangeChecking() {
        return false;
    }
    
    public boolean supportsUnits() {
        return false;
    }

    @SuppressWarnings("unchecked") // Class can not be parameterized at this point. 
    private final Class classType;
    private String commonName = "Default common name";          //This tells us if we forgot to set the name.
    private double displayOffset = 0;
    private double displayScale = 1;
    private String displayUnits = "";
    private String lowerGraphYLabel = "";
    private final String operatingMode;
    private String upperGraphYLabel = "";

}

//
// O_o
        
