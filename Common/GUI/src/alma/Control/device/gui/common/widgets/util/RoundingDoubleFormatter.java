/* ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets.util;

import java.text.ParseException;

import javax.swing.text.DefaultFormatter;

/**
 * 
 * @author David Hunter dhunter@nrao.edu
 * @version 
 * @since ALMA 8.0.0
 */
public class RoundingDoubleFormatter extends DefaultFormatter {

    /**
     * Create and instance of this class with defaults.
     */
    public RoundingDoubleFormatter() {
        super();
    }

    /**
     * Create an instance of this class with the given range.
     *
     * @param min
     *            the closed lower limit of the range interval. Must be at least 2; will be set
     *            to 2 if the given value is less than 2.
     * @param max
     *            the closed upper limit of the range interval.
     */
    public RoundingDoubleFormatter(final double min, final double max) {
        super();
        upperBound = max;
        lowerBound = min;
    }
    
    /**
     * Create an instance of this class with the given range, which rounds to the given step size.
     *
     * @param min
     *            the closed lower limit of the range interval. Must be at least 2; will be set
     *            to 2 if the given value is less than 2.
     * @param max
     *            the closed upper limit of the range interval.
     * @param theStepSize
     *            the size of the steps to round the number to. Note: when rounding, the steps start
     *            counting from 0.
     */
    public RoundingDoubleFormatter(final double min, final double max, final double theStepSize) {
        super();
        stepSize=theStepSize;
        upperBound = max;
        lowerBound = min;
    }

    /**
     * Convert a string to a double.
     *
     * Ensure the double is in range for this instance.
     *
     * @param text
     *            the string to convert.
     * @return the integer value of the string.
     * @throws ParseException
     *            if the string does not contain a valid integer.
     */
    @Override
    public Object stringToValue(final String text) throws ParseException {
        double result;
        
        //Parse the result and check bounds
        //TODO: exceptions here can end up in the logs.
        result = Double.parseDouble(text);
        if (result > upperBound)
            result=upperBound;
        else if (result < lowerBound)
            result=lowerBound;

        result=Math.round(result/stepSize)*stepSize;
            
        return result;
    }

    /**
     * Convert an integer to a String.
     *
     * Ensure the integer is in range for this instance.
     *
     * @param value
     *            the integer to convert to a String
     * @return a String representing the integer value.
     * @throws ParseException
     *             if value is not an integer or if the value is out of range.
     */
    @Override
    public String valueToString(final Object value) throws ParseException {
        if (value==null) {
            throw new ParseException("Null value", 0);
        }

        // We do this because < and > don't work on Objects.
        double checkValue = (Double) value;
        if (checkValue > upperBound || checkValue < lowerBound) {
            throw new ParseException("Value out of bounds", 0);
        }
        return String.format("%2.1f", value);
    }

    private double stepSize;
    /**
     * The closed lower limit of the range interval. Defaults to 1, since this formatter will
     * only produce integers - no floats and no complex numbers.
     */
    private double lowerBound = Double.MIN_VALUE;

    /**
     * The closed upper limit of the range interval.
     */
    private double upperBound = Double.MAX_VALUE;

    /**
     * TODO: Correct handling of <code>serialVersionUID</code>.
     */
    private static final long serialVersionUID = 1L;
}
