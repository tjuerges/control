/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;

import java.util.logging.Logger;

/**
 * <code>EightDipSwitchesMonitorPointWidget</code> displays 8 boolean monitor points that
 * represent the 8 bits of a byte given too it. It is intended for use in displaying the
 * status of the DRX on-board dip switches.
 * 
 * @author  Scott Rankin  srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 5.0.4
 * 
 * TODO: Document
 * TODO: Factor out common behavior with other widgets to a superclass.
 */
public class EightDipSwitchesMonitorPointWidget extends MonitorPointWidget {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private Status falseStatus;
    private String falseToolTipText;
    private StatusIndicator[] statusIndicators;
    private Status trueStatus;
    private String trueToolTipText;
    private IMonitorPoint monitorPoint;

    /**
     * <code>EightDipSwitchesMonitorPointWidget</code> is the constructor.
     * 
     * @param logger logger to be used for logging
     * @param model monitor point to listen for
     * @param falseStatus status to be used to indicate a false value read from the monitor point
     * @param falseToolTipText tool tip text to be used to indicate a false value was read
     * from the monitor point
     * @param trueStatus status to be used to indicate a true value was read from the monitor point
     * @param trueToolTipText tool tip text to be used to indicate a true value was read
     * from the monitor point
     */
    public EightDipSwitchesMonitorPointWidget (
            Logger logger,
            MonitorPointPresentationModel<Byte> monitorPointModel,
            Status falseStatus,
            String falseToolTipText,
            Status trueStatus,
            String trueToolTipText
    ) {
        super(logger, true);
        this.falseStatus = falseStatus;
        this.falseToolTipText = falseToolTipText;
        this.trueStatus = trueStatus;
        this.trueToolTipText = trueToolTipText;
        statusIndicators = new StatusIndicator[8];
        for (int count=0; count<8; count++) {
            statusIndicators[count] = new StatusIndicator(trueStatus, falseToolTipText);
        }
        Byte newByte=7;
        monitorPoint = addMonitorPointPM(monitorPointModel, newByte);
        buildWidget();
    }

    /**
     * buildWidget is a private method which is used to construct the entire widget
     * to be displayed on a panel
     */
    private void buildWidget() {		
        for (int count=0; count<8; count++){
            add(statusIndicators[count]);
            addPopupMenuToComponent(statusIndicators[count]);
        }
    }

    @Override
    protected void updateAttention(Boolean value) {
        // Nothing to do in this class.
    }

    @Override
    protected void updateValue(IMonitorPoint aMonitorPoint) {
        //The data we care about it a ubyte which ends up being given to as a java int
        //So we just ignore the upper bits.
        Integer myByte=getCachedValue(aMonitorPoint);
        boolean[] switchStatus = new boolean[8];
        int count=0;
        for (count=0; count<switchStatus.length; count++)
            switchStatus[7 - count] = ((myByte&(1<<count)) != 0);
        count=0;
        for (StatusIndicator s: statusIndicators){
            if (switchStatus[count])
                s.setStatus(trueStatus, trueToolTipText);
            else
                s.setStatus(falseStatus, falseToolTipText);
            count++;
        }
    }  
}

//
// O_o

