
/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

/**
 * This class just provides a grab data function. It takes in a file name (including
 * the path) and grabs all monitor test data out of that file. The data is then returned
 * as an array of TestMonitorDataPackets.
 *
 *See TestMonitorPoints for a description of the file format.
 *
 * @author  David Hunter    dhunter@nrao.edu
 * @version 
 * @since 7.1.0
 */

package alma.Control.device.gui.common;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;

import alma.Control.device.gui.common.TestMonitorDataPacket;

public class GrabMonitorData {
    /**
     * The grab data function. It takes in a file name (including the path) and
     * grabs all monitor test data out of that file. The data is then returned
     * as an array of TestMonitorDataPackets.
     * 
     * @return The array of data from the file.
     * @throws FileNotFoundException Thrown if the given file can not be found.
     */
    static public TestMonitorDataPacket[] grabMonitorData(String fileName) throws FileNotFoundException{
        ArrayList<TestMonitorDataPacket> data = new ArrayList<TestMonitorDataPacket>(50);
        
        //Make a new file scanner to read in our file.
        Scanner input = new Scanner(new FileReader(fileName));
        
        //Set up things for the read-in loop
        boolean notDone=true;
        String nextLine;
        String[] tokens;
        TestMonitorDataPacket nextPacket;
        
        //Read in data until we are done reading data
        while (notDone){
            try{
                nextLine=input.nextLine();
                if (nextLine.charAt(0)!='#')
                {
                    //One line form a CSV file, so we split around the commas.
                    tokens = nextLine.split(",");
                    nextPacket=new TestMonitorDataPacket();

                    //First element is the name of the monitor point to set.
                    nextPacket.monitorPointToSet=tokens[0].trim();

                    //Second element is the RCA. It should be a hex, though this will also
                    //accept decimal and octal.
                    nextPacket.rcaToSet=Integer.decode(tokens[1].trim());
                    
                    //Now get the input array
                    nextPacket.valueToSet=parseIntArray(tokens[2]);

                    //Now for the name of the monitor point to check.
                    nextPacket.monitorPointToCheck=tokens[3].trim();

                    //And now for the value to get. It is a little tricky to decode the proper type
                    String nextToken=tokens[4].trim();
                    try {
                        //First, see if it is an integer. Doubles, &, and booleans won't parse...
                        nextPacket.expectedValue = Long.decode(nextToken);
                    } catch (NumberFormatException e) {
                        try {
                            //Booleans & arrays won't parse as a double...
                            nextPacket.expectedValue = Double.parseDouble(nextToken);
                        } catch (NumberFormatException e2) {
                            try {
                                //Try for an array of ints - double arrays and booleans won't parse
                                nextPacket.expectedValue = parseLongArray(nextToken);
                            }    //ClassCastException
                            catch (NumberFormatException e3){
                                try{
                                    //A boolean won't parse here
                                    nextPacket.expectedValue=parseDoubleArray(nextToken);
                                }
                                catch (NumberFormatException e4){
                                    if (nextToken.contains("|"))
                                        //If we have an array, parse as one
                                        nextPacket.expectedValue = parseBooleanArray(nextToken);
                                    else
                                        //And if we get here it had better be a boolean
                                        nextPacket.expectedValue = Boolean.parseBoolean(nextToken);
                                }
                            }
                        }
                    }
                    data.add(nextPacket);
                }
            }
            //We are done reading when we run out of data to read.
            catch (NoSuchElementException e) {
                notDone=false;}
        }
        input.close();
        return data.toArray(new TestMonitorDataPacket[0]);
    }
    
    private static boolean[] parseBooleanArray(String token){
        String[] rawData = token.split("\\|");
        int l = rawData.length;
        boolean[] someRawData = new boolean[l];
        for (int c=0; c<l; c++)
              someRawData[c]=(Boolean.parseBoolean(rawData[c].trim()));
        return someRawData;
    }
    
    private static double[] parseDoubleArray(String token){
        String[] rawData = token.split("\\|");
        int l = rawData.length;
        double[] someRawData = new double[l];
        for (int c=0; c<l; c++)
              someRawData[c]=(Double.parseDouble(rawData[c].trim()));
        return someRawData;
    }
    
    private static int[] parseIntArray(String token){
        String[] rawData = token.split("\\|");
        int l = rawData.length;
        int[] someRawData = new int[l];
        for (int c=0; c<l; c++)
              someRawData[c]=(Integer.decode(rawData[c].trim()));
        return someRawData;
    }
    
    private static long[] parseLongArray(String token){
        String[] rawData = token.split("\\|");
        int l = rawData.length;
        long[] someRawData = new long[l];
        for (int c=0; c<l; c++)
              someRawData[c]=(Long.decode(rawData[c].trim()));
        return someRawData;
    }
}
