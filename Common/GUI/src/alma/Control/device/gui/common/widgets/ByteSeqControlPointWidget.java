/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.ControlPointPresentationModel;
import alma.Control.device.gui.common.widgets.util.HexFormatter;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.JLabel;

public class ByteSeqControlPointWidget extends JPanel implements ActionListener {

    private static final long serialVersionUID = 1L;
    private ControlPointPresentationModel<int[]> controlPointModel;
    int[] controlValue;
    int numBytes;
    private Logger logger;
    JButton controlButton;
    JFormattedTextField[] inputFields;
    String[] labels;

    public ByteSeqControlPointWidget(Logger logger, ControlPointPresentationModel<int[]> controlPointModel,
            String buttonText, int size){
        this(logger, controlPointModel, buttonText, size, null);
    }
    
    public ByteSeqControlPointWidget(Logger logger, ControlPointPresentationModel<int[]> controlPointModel,
            String buttonText, int size, String[] theLabels) {
        this.logger = logger;
        this.controlPointModel = controlPointModel;
        numBytes = size;
        controlValue = new int[numBytes];
        controlPointModel.getContainingDevicePM().addControl(this);
        labels = new String[numBytes];
        if (theLabels != null) {
            labels = theLabels;
        } else {
            for (int count = 0 ; count < numBytes; count++) {
                labels[count] = "";
            }
        }
        inputFields = new JFormattedTextField[numBytes];
        for (int count = 0; count < numBytes; count++) {
            inputFields[count] = new JFormattedTextField(new HexFormatter(0, 0xFF));
            inputFields[count].setValue(0);
            controlPointModel.getContainingDevicePM().addControl(inputFields[count]);
        }
        controlButton = new JButton(buttonText);
        controlButton.addActionListener(this);
        controlPointModel.getContainingDevicePM().addControl(this.controlButton);
        buildWidget(buttonText);
    }

    public void actionPerformed(ActionEvent event) {
        for (int count = 0; count < numBytes; count++) {
            controlValue[count] = (Integer)inputFields[count].getValue();
        }
        controlPointModel.setValue(controlValue);
    }

    public void buildWidget(String buttonText) {
        setBorder(BorderFactory.createEtchedBorder(1));
        setLayout(new GridBagLayout());
        final GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty = 100;
        c.weightx = 100;
        c.gridx = 1;
        c.gridy = 0;
        for (int count = 0; count < numBytes; count++) {
            addLeftLabel(labels[count], c);
            add(inputFields[count], c);
            c.gridy++;
        }
        c.gridx--;
        c.gridwidth = 2;
        add(controlButton, c);
    }

    /*
     * A simple helper function that allows you to add a label to the left of the current location with
     * grid bag layout.
     */
    private void addLeftLabel(String s, GridBagConstraints c) {
        c.gridx--;
        add(new JLabel(s), c);
        c.gridx++;
    }
}
