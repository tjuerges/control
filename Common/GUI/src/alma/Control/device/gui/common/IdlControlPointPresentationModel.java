/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common;

import alma.Control.HardwareDeviceOperations;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Logger;

/**
 * Common behavior and properties for all Control Point Presentation Models that must be hand coded.
 * 
 * @author  Scott Rankin  srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 5.1.1
 * 
 * TODO: Add ExcLog to logged exceptions.
 */
public abstract class IdlControlPointPresentationModel<T> extends ControlPointPresentationModel<T> {

    protected String controlPointName;
    protected Method controlPointUpdateMethod;
    protected String controlPointUpdateMethodName;
  
    @SuppressWarnings("unchecked") // Class can not be parameterized at this point. 
	public IdlControlPointPresentationModel(
    	Logger logger, 
    	DevicePresentationModel containingDevicePM, 
    	IControlPoint controlPoint, 
    	Class controlPointClass
    ) {
    	super(logger, containingDevicePM, controlPoint, controlPointClass);
        this.logger = logger;
        this.controlPointName = controlPoint.toString();
        this.controlPointUpdateMethodName = "SET_" + controlPointName; 
    }
    
    @Override
	public void setDeviceComponent(HardwareDeviceOperations deviceComponent) {
        this.deviceComponent = deviceComponent;
        setUpdateMethod();
    }

    @Override
	protected abstract void setUpdateMethod();
    
    @Override
    /**Send the given value to the control point
     * @param value The value to send.
     */
	public void setValue(T value) {
    	this.value = value;
    	
    	if (null == deviceComponent) { 
    		logger.severe("IcdControlPointPresentationModel.setValue() - deviceComponent == null");
    		return;
    	}
    	if (null == controlPointUpdateMethod) {
    		logger.severe("IcdControlPointPresentationModel.setValue() - controlPointUpdateMethod == null!");
    		return;
    	}

    	try {
    	    this.controlPointUpdateMethod.invoke(deviceComponent, value);
		} catch (IllegalArgumentException ex) {
        	logger.severe(
        	    "IcdControlPointPresentationModel.setValue() - illegal arguments for method: " + 
        	    controlPointName);
			ex.printStackTrace();
		} catch (IllegalAccessException ex) {
        	logger.severe(
        		"IcdControlPointPresentationModel.setValue() - illegal access to method: " + 
        		controlPointName);
			ex.printStackTrace();
		} catch (InvocationTargetException ex) {
        	logger.severe(
        		"IcdControlPointPresentationModel.setValue() - illegal access to object when calling: " + 
        		controlPointName);
			ex.printStackTrace();
		}
    }
    @Override
    /**Call the given control point but don't send any value.
     * Used with control points that do not require or accept any values.
     */
    public void setValue() {
        if (null == deviceComponent) { 
            logger.severe("IcdControlPointPresentationModel.setValue() - deviceComponent == null");
            return;
        }
        if (null == controlPointUpdateMethod) {
            logger.severe("IcdControlPointPresentationModel.setValue() - controlPointUpdateMethod == null!");
            return;
        }
        try {
            this.controlPointUpdateMethod.invoke(deviceComponent);
        } catch (IllegalArgumentException ex) {
            //This exception should never happen, but it is thrown by invoke so we have to catch it.
            logger.severe(
                    "IcdControlPointPresentationModel.setValue() - illegal arguments for method: " + 
                controlPointName);
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            logger.severe(
                "IcdControlPointPresentationModel.setValue() - illegal access to method: " + 
                controlPointName);
            ex.printStackTrace();
        } catch (InvocationTargetException ex) {
            logger.severe(
                "IcdControlPointPresentationModel.setValue() - illegal access to object when calling: " + 
                controlPointName);
            ex.printStackTrace();
        }
    }
}

//
// O_o
