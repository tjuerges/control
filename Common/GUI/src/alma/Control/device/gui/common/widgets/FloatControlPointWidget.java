/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.ControlPointPresentationModel;
import alma.Control.device.gui.common.IControlPoint;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

/**
 * <code>SpinnerControlPointWidget</code> displays a button and spinner box
 * to allow the user to control its associated control point
 * 
 * @author  Scott Rankin  srankin@nrao.edu
 * @author  David Hunter  dhunter@nrao.edu
 * @version $Id$
 * @since   ALMA 5.0.4
 * 
 * TODO: Document
 * TODO: Factor out common behavior with other widgets to a superclass.
 */

public class FloatControlPointWidget extends JPanel implements ActionListener {

    /**
     * Build a control for an integer control point.
     * @param logger
     * @param controlPointModel
     * @param spinnerStepSize
     * @param buttonText
     */
    public FloatControlPointWidget(Logger logger,
            ControlPointPresentationModel<Integer> controlPointModel, String buttonText){
        this.logger = logger;
        this.intControlPointModel = controlPointModel;
        isInt=true;

        int spinnerMinValue = Integer.MIN_VALUE;
        int spinnerMaxValue = Integer.MAX_VALUE;
        int spinnerStartValue = 0;
        IControlPoint cp = controlPointModel.getControlPoint();
        if (cp.supportsRangeChecking()) {
            spinnerMinValue = (int) (scaleFactor*(cp.getRangeLowerBound().doubleValue()));
            spinnerMaxValue = (int) (scaleFactor*(cp.getRangeUpperBound().doubleValue()));
            //Make sure that the start value is within the range of the possible values.
            if (spinnerMinValue > 0.0 || spinnerMaxValue < 0.0)
                spinnerStartValue=spinnerMinValue;
        }

        inputSpinner = new JSpinner(new
                SpinnerNumberModel(spinnerStartValue, spinnerMinValue, spinnerMaxValue, 1));
        controlButton = new JButton(buttonText);
        controlButton.addActionListener(this);
        controlPointModel.getContainingDevicePM().addControl(this.controlButton);
        controlPointModel.getContainingDevicePM().addControl(this.inputSpinner);
        theSpinnerStepSize=1.0;
        buildWidget();
    }
    
    /**
     * Build a control for a float control
     * @param logger
     * @param controlPointModel
     * @param spinnerStepSize
     * @param buttonText
     */
    public FloatControlPointWidget( Logger logger,
            ControlPointPresentationModel<Float> controlPointModel,
            double spinnerStepSize, String buttonText){
            this(logger, controlPointModel, spinnerStepSize, (float)1.0, buttonText);
    }
    
    /**
     * Build a control for a float control with the given step size and scale factor
     * @param logger
     * @param controlPointModel
     * @param spinnerStepSize
     * @param newScaleFactor
     * @param buttonText
     */
    public FloatControlPointWidget( Logger logger,
            ControlPointPresentationModel<Float> controlPointModel,
            double spinnerStepSize, float newScaleFactor, String buttonText){
        this.logger = logger;
        this.controlPointModel = controlPointModel;

        //We use the float bounds as min and max because this widget ends up outputing a float.
        double spinnerMinValue = Float.MIN_VALUE;
        double spinnerMaxValue = Float.MAX_VALUE;
        double spinnerStartValue = 0.0;
        scaleFactor=newScaleFactor;
        IControlPoint cp = controlPointModel.getControlPoint();
        if (cp.supportsRangeChecking()) {
            spinnerMinValue = scaleFactor*(cp.getRangeLowerBound().doubleValue());
            spinnerMaxValue = scaleFactor*(cp.getRangeUpperBound().doubleValue());
            //Make sure that the start value is within the range of the possible values.
            if (spinnerMinValue > 0.0 || spinnerMaxValue < 0.0)
                spinnerStartValue=spinnerMinValue;
        }

        inputSpinner = new JSpinner(new
                SpinnerNumberModel(spinnerStartValue, spinnerMinValue, spinnerMaxValue, spinnerStepSize));
        controlButton = new JButton(buttonText);
        controlButton.addActionListener(this);
        controlPointModel.getContainingDevicePM().addControl(this.controlButton);
        controlPointModel.getContainingDevicePM().addControl(this.inputSpinner);
        theSpinnerStepSize=(float)spinnerStepSize;
        buildWidget();
    }

    /* This rounding algorithm assumes that 0.0 is a valid value on the 
     * input scale. If cases are found where this is not true then this
     * function will need updated to properly handle them.
     */
    public void actionPerformed(ActionEvent e) {
        double newValue=(Double)inputSpinner.getValue();
        long newerValue=Math.round((newValue/theSpinnerStepSize));
        newValue=((double)newerValue)*theSpinnerStepSize;
        inputSpinner.setValue(newValue);
        setValue(newValue/scaleFactor);
    }

    private void buildWidget() {
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));		
        add(inputSpinner);
        add(controlButton);
    }

    public void setValue(double value) {
        //The number given by the use can not be outside the range of a float due to the bounds checking.
        //So this conversion should be safe, and a float is expected outside of this widget.
        float outValue=(float)(value);
        controlPointModel.setValue(outValue);
    }

    private JButton controlButton;
    private ControlPointPresentationModel<Float> controlPointModel;
    private ControlPointPresentationModel<Integer> intControlPointModel;
    private JSpinner inputSpinner;
    private boolean isInt=false;
    private Logger logger;
    private double scaleFactor=1;
    private static final long serialVersionUID = 1L;
    private double theSpinnerStepSize;
}