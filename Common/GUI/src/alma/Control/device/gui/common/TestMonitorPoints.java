/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

/**
 * Test for checking a devices monitor points. This test manually sets the simulator
 * to known raw can values. Then it reads back the converted values and makes sure
 * that they are what they should be. The tests are intended to be enumerated in a test data file.
 * 
 * If the test passes, everything went well. If it fails, a report of the results will be
 * generated, stating all the tests that failed. Note that JUnit thinks there is only 1 test, but
 * actually every test line given in the test data file was run.
 *
 * To implement these tests on a new device, do the following:
 *   1) Extend this class to a device-specific class, which provides a device reference and
 *      test file name. For an example, see:
 *      CONTROL/Device/HardwareDevice/DTX/test/alma/Control/device/gui/DTX/TestDtxMonitorPoints.java
 *   2) Write the test file, as described below. The generic monitor point tests can be copied form
 *      another device. For an example, see:
 *      CONTROL/Device/HardwareDevice/DTX/test/DTXMonitorTestData.txt
 *   3) Update the test/Makefile to compile the tests (jars). Makefiles vary, so compare with other
 *      Makefiles as needed.
 *   4) (optional) Make a simple script to run it, such as runDtxMonitorPointTest. Along with the
 *      traditional #!/bin/bash and alma comments, this scrip as one line:
 *         acsStartJava junit.textui.TestRunner alma.Control.device.gui.DTX.TestDtxMonitorPoints
 *
 * Test File Format:
 * The given file should contain all of the monitor point test cases. The intent is for it to
 * contain a range of monitor points with raw and manually converted values, designed to cover a
 * range of possible problems. For a sample of one of these classes please see:
 * CONTROL/Device/HardwareDevice/DTX/test/alma/Control/device/gui/DTX/TestDtxMonitorPoints.java
 * 
 * The test file should contain one monitor test sample per line. Each monitor test sample should be
 * formatted as:
 * 
 * PARRENT_MONITOR_POINT,   RCA,    array_of_raw_bytes, CHILD_MONITOR_POINT,    Expected value
 * 
 * Note that:
 *      -The array of bytes is between 1 and 8 bytes, separated by |. It specifies the raw data
 *       as it would come off the can bus. Byte 0 in the ICD is the left-most byte in the test file.
 *      -All fields are separated by a , and by as much white space as you want.
 *      -Integers can be entered in octal, decimal, or hexadecimal notation
 *          -Hex is strongly recommended for RCA and raw data entry.
 *      -Currently supported expected values are: int, double, boolean, int[], and double[].
 *          -Arrays of ints and doubles are entered in the same way as the raw bytes.
 *      -The PARRENT_MONITOR_POINT is the name listed in the ICD and spreadsheet. The
 *       CHILD_MONITOR_POINT is the name of the monitor point you are checking. Sometimes the
 *       parent and child are the same. Other times the child is one part of the data provided
 *       by the parent.
 *      -Lines that begin with a # are comments. The # MUST be the first character.
 *      -There should be no extra blank lines (unless they begin with a #)
 *
 * Several example lines:
 *
 * #This is a comment
 * GET_TTX_ALARM_STATUS,           0x2401,         0xFF|0xFF|0xFF|0xFF|0xFF|0x7F,          GET_TTX_ALARM_STATUS_TXFIFOERR_B,       false
 * GET_TTX_LASER_BIAS_CH1,         0x2101,         0x00|0x1F|0x03,                         GET_TTX_LASER_BIAS_CH1,                 0.007939
 * #Another comment here!
 * GET_FR_PAYLOAD_HI_CH3,          0x3006,         0xFF|0xFF|0xFF|0xFF|0xFF|0xF1|0xFF|0xFF,GET_FR_PAYLOAD_HI_CH3,                  0xFF|0xFF|0xFF|0xFF|0xFF|0xF1|0xFF|0xFF
 * GET_FR_FPGA_FW_VER_CH2,         0x2004,         0xBE|0xFF|0xFF|0xFF|0xFF|0xF5|0xF6|0xF7,GET_FR_FPGA_FW_VER_CH2_BE,              0xBE
 *
 * The scanner for the file format will fail if the file is improperly formatted. And the format is
 * rather strict. A more user-friendly scanner could be written. But at the time this test was made
 * it was not deemed worth the effort. Mostly because only a few programmers should ever need to
 * write these files. Should this change a better file scanner should be written.
 *
 * Should a better scanner be written it will be critical to insure that all data files are ported
 * to the new format, or that the old one remains supported. The test cases in those files took a
 * lot of work to write, and it is improbable that anyone will be willing to manually rewrite them.
 *
 * Comments should be included in these as appropriate. Any test that is commented out should have
 * an explanation as to why.
 *
 * For an example file, please see: CONTROL/Device/HardwareDevice/DTX/test/DTXMonitorTestData.txt
 *
 * Common file scanning errors and causes:
 *  *java.lang.NumberFormatException
 *      -Missing a ,
 *      -Invalid number
 *  *Exception when attempting to set monitor value in simulator.
 *      -Incorrect number of bytes given in input array.
 *  *String index out of range: 0
 *      -You stuck in a blank line. Remove it or add a # to the start.
 *  *java.lang.ClassCastException: [some type] cannot be cast to java.lang.Boolean
 *      -GrabMonitorData was unable to parse the expected value as a number, so it defaulted to
 *       Boollean. You probably goofed up the number in that field.
 *  *junit.framework.AssertionFailedError: NoSuchMethodException
 *      -The name of the function to check the value of can not be found.
 *       (This error is not caused by a bad name of a function to set)
 *  *ClassCaseException when attempting to read value!
 *      -The expected value is an int, but a float is expected. Add a .0 to it.
 *
 * @author  David Hunter    dhunter@nrao.edu
 * @version 
 * @since 7.1.0
 */


package alma.Control.device.gui.common;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import org.omg.CORBA.LongHolder;

import alma.Control.HardwareDevice;
import alma.Control.device.gui.common.GrabMonitorData;
import alma.Control.device.gui.common.TestMonitorDataPacket;
import alma.acs.component.client.ComponentClientTestCase;


public abstract class TestMonitorPoints extends ComponentClientTestCase {

    /**
     * The constructor. Just calls super...
     * For some reason the ACS component client test case constructor throws exceptions, so we have
     * to pass them along because Java won't let us handle them!
     * @param name
     * @throws Exception
     */
    public TestMonitorPoints(String name) throws Exception {
        super(name);
    }

    /**
     * Set things up for the test!
     *      -Tell the parent class to setup
     *      -Get the DTX component.
     *      
     * Setup must be called before running any tests!
     *
     * Precondition: There MUST be a simulated DTX up and running before calling setup. This
     * function will fail if there is no DTX running.
     */
    protected void setUp(HardwareDevice aDeviceRef, String aComponentName, String dataFileName)
                   throws Exception {
        //super.setup() should have been called by the child class.
        componentName=aComponentName;
        deviceRef = aDeviceRef;
        testData = GrabMonitorData.grabMonitorData(dataFileName);
        testFailed=false;
        errorLog = new ArrayList<String>();
    }

    /**
     * Release the ACS component then call super.teardown.
     */
    protected void tearDown() throws Exception {
        deviceRef._release();
        super.tearDown();
    }

    /**
     * A function that loops through all the given monitor points. It takes in the names and RCSs,
     * and checks that the proper value is given.
     */
    public void testMonitorPoints() {
        String msg;
        for (int c=0; c<testData.length; c++){
            //Set the simulator value
            try{
            setValue(testData[c].rcaToSet, testData[c].valueToSet, testData[c].monitorPointToSet,
                    deviceRef);
            }
            catch (Exception e){
                fail("Exception when attempting to set monitor value in simulator.");
            }
            //There is a 2 second cache in the monitoring system. So we have to wait for the
            //changes to propagate.
            try {
                System.out.print(String.format("Monitor Test %d, RCA 0x%X. Waiting for changes to" +
                                 " propagate through the simulator.\n", c, testData[c].rcaToSet));
                Thread.sleep(2020);
            } catch (InterruptedException e) {
                fail("Failed to sleep between tests! Slap Java around and try again...");
            }
            //A summary of the test, to be printed if there is an error.
            msg=String.format("\nWrote %s to %s, RCA:0x%X. Checked %s",
                              printIntArray(testData[c].valueToSet), testData[c].monitorPointToSet,
                              testData[c].rcaToSet, testData[c].monitorPointToCheck);
            //Identify the type of point and then call the proper check function.
            //The check function logs the error if there is one and sets testFailed.
            if (testData[c].expectedValue.getClass() == Double.class){
                checkDoubleValue(testData[c].monitorPointToCheck, (Double)testData[c].expectedValue, msg);}
            else if (testData[c].expectedValue.getClass() == Boolean.class){
                checkBooleanValue(testData[c].monitorPointToCheck, (Boolean)testData[c].expectedValue, msg);}
            else if (testData[c].expectedValue.getClass() == Long.class){
                checkLongValue(testData[c].monitorPointToCheck, (Long)testData[c].expectedValue, msg);}
            else if (testData[c].expectedValue.getClass() == long[].class){
                checkLongArray(testData[c].monitorPointToCheck, (long[])testData[c].expectedValue, msg);}
            else if (testData[c].expectedValue.getClass() == double[].class){
                checkDoubleArray(testData[c].monitorPointToCheck, (double[])testData[c].expectedValue, msg);}
            else if (testData[c].expectedValue.getClass() == boolean[].class){
                checkBooleanArray(testData[c].monitorPointToCheck, (boolean[])testData[c].expectedValue, msg);}
            else{
                fail("Unknown type ("+testData[c].expectedValue.getClass()+") given in valuesOut for the return type.");
            }
        }
        //If the test failed, print the error messages and tell JUnit to fail.
        if (testFailed){
        StringBuilder s = new StringBuilder("\nThese failures were found:");
        for (int c=0; c<errorLog.size(); c++)
            s.append(errorLog.get(c));
            fail(s.toString());
        }
        System.out.println(String.format("\nRan %d sets of monitor check values.\n", testData.length));
    }

    /**checkBooleanArray
     * A helper function to make the code a little cleaner.
     * 
     * Checks the given monitor point to see if it returns the given array of ints. Logs the proper
     * error message and sets testFailed if the the given value is not returned.
     */
    private void checkBooleanArray(String monitorName, boolean[] actualValues, String errorMsg){
        boolean[] reportedValues=null;
        boolean failBooleanArrayTest=false;
        try {
            Class c = deviceRef.getClass();
            Method m = c.getMethod(monitorName, new Class[]{LongHolder.class});
            reportedValues=(boolean[]) m.invoke(deviceRef, new LongHolder());
        } catch (SecurityException e) {
            fail("SecurityException in " + monitorName);
        } catch (NoSuchMethodException e) {
            fail("NoSuchMethodException in " + monitorName);
        } catch (IllegalArgumentException e) {
            fail("IllegalArgumentException in " + monitorName);
        } catch (IllegalAccessException e) {
            fail("IllegalAccessException in " + monitorName);
        } catch (InvocationTargetException e) {
            fail("InvocationTargetException in " + monitorName);
        }
        if(reportedValues.length != actualValues.length)
            failBooleanArrayTest=true;
        else
            for (int count=0; count<reportedValues.length; count++)
                if(actualValues[count] != reportedValues[count])
                    failBooleanArrayTest=true;
        if(failBooleanArrayTest){
            testFailed=true;
            errorLog.add(String.format("%s Expected %s but was %s", errorMsg,
                    printBooleanArray(actualValues), printBooleanArray(reportedValues)));
        }


    }
    
    /**checkBooleanValue
     * A helper function to make the code a little cleaner.
     * 
     * Checks the given monitor point to see if it returns the given boolean value. Logs the proper
     * error message and sets testFailed if the the given value is not returned.
     */
    private void checkBooleanValue(String monitorName, boolean actualValue, String errorMsg){
        //Initialize this to something that will fail unless we retrieve the correct value.
        boolean reportedValue=!actualValue;
        try {
            Class c = deviceRef.getClass();
            Method m = c.getMethod(monitorName, new Class[]{LongHolder.class});
            reportedValue=(Boolean) m.invoke(deviceRef, new LongHolder());
        } catch (SecurityException e) {
            fail("SecurityException in " + monitorName);
        } catch (NoSuchMethodException e) {
            fail("NoSuchMethodException in " + monitorName);
        } catch (IllegalArgumentException e) {
            fail("IllegalArgumentException in " + monitorName);
        } catch (IllegalAccessException e) {
            fail("IllegalAccessException in " + monitorName);
        } catch (InvocationTargetException e) {
            fail("InvocationTargetException in " + monitorName);
        }
        if(actualValue != reportedValue){
            testFailed=true;
            errorLog.add(String.format("%s Expected %b but was %b", errorMsg, actualValue, reportedValue));
        }
    }
    
    /**checkDoubleArray
     * A helper function to make the code a little cleaner.
     * 
     * Checks the given monitor point to see if it returns the given array of ints. Logs the proper
     * error message and sets testFailed if the the given value is not returned.
     */
    private void checkDoubleArray(String monitorName, double[] actualValues, String errorMsg){
        double[] reportedDoubleValues=null;
        float[] reportedFloatValues=null;
        boolean failDoubleArrayTest=false;
        try {
            Class c = deviceRef.getClass();
            Method m = c.getMethod(monitorName, new Class[]{LongHolder.class});
            try{
            reportedDoubleValues=(double[]) m.invoke(deviceRef, new LongHolder());
            }
            catch (ClassCastException ccs){
                reportedFloatValues=(float[]) m.invoke(deviceRef, new LongHolder());
            }
        } catch (SecurityException e) {
            fail("SecurityException in " + monitorName);
        } catch (NoSuchMethodException e) {
            fail("NoSuchMethodException in " + monitorName);
        } catch (IllegalArgumentException e) {
            fail("IllegalArgumentException in " + monitorName);
        } catch (IllegalAccessException e) {
            fail("IllegalAccessException in " + monitorName);
        } catch (InvocationTargetException e) {
            fail("InvocationTargetException in " + monitorName);
        }
        
        if (reportedDoubleValues == null){
            //actualValue<reportedValue-sigFigs || actualValue>reportedValue+sigFigs
            if(reportedFloatValues.length != actualValues.length)
                failDoubleArrayTest=true;
            else
                for (int count=0; count<reportedFloatValues.length; count++){
                    double sigFigs;
                    if (actualValues[count]>100)
                        sigFigs=SIGFIGSLARGE;
                    else
                        sigFigs=SIGFIGSSMALL;
                    if(actualValues[count] < reportedFloatValues[count]-sigFigs ||
                            actualValues[count]>reportedFloatValues[count]+sigFigs)
                        failDoubleArrayTest=true;
                }
            if(failDoubleArrayTest){
                testFailed=true;
                errorLog.add(String.format("%s Expected %s but was %s", errorMsg,
                        printDoubleArray(actualValues), printFloatArray(reportedFloatValues)));
            }
        }
        else
        {
            if(reportedDoubleValues.length != actualValues.length)
                failDoubleArrayTest=true;
            else
                for (int count=0; count<reportedFloatValues.length; count++){
                    double sigFigs;
                    if (actualValues[count]>100)
                        sigFigs=SIGFIGSLARGE;
                    else
                        sigFigs=SIGFIGSSMALL;
                    if(actualValues[count] < reportedDoubleValues[count]-sigFigs ||
                            actualValues[count]>reportedDoubleValues[count]+sigFigs)
                        failDoubleArrayTest=true;
                }
            if(failDoubleArrayTest){
                testFailed=true;
                errorLog.add(String.format("%s Expected %s but was %s", errorMsg,
                        printDoubleArray(actualValues), printDoubleArray(reportedDoubleValues)));
            }
        }
    }
    
    /**checkDoubleValue
     * A helper function to make the code a little cleaner.
     * 
     * Checks the given monitor point to see if it returns the given double/flaot value. Logs the
     * proper error message and sets testFailed if the the given value is not returned.
     */
    private void checkDoubleValue(String monitorName, double actualValue, String errorMsg){
        double reportedValue;
        //Initialize this to something that will fail unless we retrieve the correct value.
        if (actualValue != Double.MIN_VALUE)
            reportedValue=Double.MIN_VALUE;
        else
            reportedValue=0;
        try {
            Class c = deviceRef.getClass();
            Method m = c.getMethod(monitorName, new Class[]{LongHolder.class});
            try{
                reportedValue=(Float) m.invoke(deviceRef, new LongHolder());
            }
            catch (ClassCastException cce){
                reportedValue=(Double) m.invoke(deviceRef, new LongHolder());
            }
        } catch (SecurityException e) {
            fail("SecurityException in " + monitorName);
        } catch (NoSuchMethodException e) {
            fail("NoSuchMethodException in " + monitorName);
        } catch (IllegalArgumentException e) {
            fail("IllegalArgumentException in " + monitorName);
        } catch (IllegalAccessException e) {
            fail("IllegalAccessException in " + monitorName);
        } catch (InvocationTargetException e) {
            fail("InvocationTargetException in " + monitorName);
        }
        
        double sigFigs;
        if (actualValue>100 || actualValue <-100)
            if (actualValue>1000000 || actualValue <-1000000)
                sigFigs=SIGFIGSXLARGE;
            else
                sigFigs=SIGFIGSLARGE;
        else
            sigFigs=SIGFIGSSMALL;
        
        if(actualValue<reportedValue-sigFigs || actualValue>reportedValue+sigFigs){
            testFailed=true;
            errorLog.add(String.format("%s Expected %f but was %f", errorMsg, actualValue, reportedValue));
        }
    }
    
    /**checkIntArray
     * A helper function to make the code a little cleaner.
     * 
     * Checks the given monitor point to see if it returns the given array of ints. Logs the proper
     * error message and sets testFailed if the the given value is not returned.
     */
    private void checkLongArray(String monitorName, long[] actualValues, String errorMsg){
        long[] reportedLongValues=null;
        int[] reportedIntValues=null;
        boolean failIntArrayTest=false;
        try {
            Class c = deviceRef.getClass();
            Method m = c.getMethod(monitorName, new Class[]{LongHolder.class});
            try{
                reportedIntValues=(int[]) m.invoke(deviceRef, new LongHolder());
            }
            //The can packets are 8 bytes, so we should not actually be able to get a long[] out
            //of one. But since java has no uints, a uint is put into a long. And we can get a
            //uint32[] off the can. Thus making an array of java longs...
            catch (ClassCastException eeeeeeeek){
                reportedLongValues=(long[]) m.invoke(deviceRef, new LongHolder());
            }
        } catch (SecurityException e) {
            fail("SecurityException in " + monitorName);
        } catch (NoSuchMethodException e) {
            fail("NoSuchMethodException in " + monitorName);
        } catch (IllegalArgumentException e) {
            fail("IllegalArgumentException in " + monitorName);
        } catch (IllegalAccessException e) {
            fail("IllegalAccessException in " + monitorName);
        } catch (InvocationTargetException e) {
            fail("InvocationTargetException in " + monitorName);
        }
        if (reportedIntValues != null){
            if(reportedIntValues.length != actualValues.length)
                failIntArrayTest=true;
            else
                for (int count=0; count<reportedIntValues.length; count++)
                    if(actualValues[count] != reportedIntValues[count])
                        failIntArrayTest=true;
            if(failIntArrayTest){
                testFailed=true;
                errorLog.add(String.format("%s Expected %s but was %s", errorMsg,
                        printLongArray(actualValues), printIntArray(reportedIntValues)));
            }
        }
        else{
            if(reportedLongValues.length != actualValues.length)
                failIntArrayTest=true;
            else
                for (int count=0; count<reportedLongValues.length; count++)
                    if(actualValues[count] != reportedLongValues[count])
                        failIntArrayTest=true;
            if(failIntArrayTest){
                testFailed=true;
                errorLog.add(String.format("%s Expected %s but was %s", errorMsg,
                        printLongArray(actualValues), printLongArray(reportedLongValues)));
            }
        }
    }

    /**checkIntegerValue
     * A helper function to make the code a little cleaner.
     * 
     * Checks the given monitor point to see if it returns the given Integer value. Logs the proper
     * error message and sets testFailed if the the given value is not returned.
     */
    private void checkLongValue(String monitorName, long actualValue, String errorMsg){
        long reportedValue;
        //Initialize this to something that will fail unless we retrieve the correct value.
        if (actualValue != Long.MIN_VALUE)
            reportedValue=Long.MIN_VALUE;
        else
            reportedValue=0;
        try {
            Class c = deviceRef.getClass();
            Method m = c.getMethod(monitorName, new Class[]{LongHolder.class});
            try{
                reportedValue=(Byte) m.invoke(deviceRef, new LongHolder());
            }
            catch (ClassCastException e){
                try{
                    reportedValue=(Short) m.invoke(deviceRef, new LongHolder());
                }
                catch (ClassCastException ee){
                    try{
                        reportedValue=(Integer) m.invoke(deviceRef, new LongHolder());
                    }
                    catch (ClassCastException eee){
                        try{
                            reportedValue=(Long) m.invoke(deviceRef, new LongHolder());
                        }
                        catch (ClassCastException eeek){
                             fail("ClassCaseException when attempting to read value!");
                             eeek.printStackTrace();
                        }
                    }
                }
            }
            } catch (SecurityException e) {
            fail("SecurityException in " + monitorName);
        } catch (NoSuchMethodException e) {
            fail("NoSuchMethodException in " + monitorName);
        } catch (IllegalArgumentException e) {
            fail("IllegalArgumentException in " + monitorName);
        } catch (IllegalAccessException e) {
            fail("IllegalAccessException in " + monitorName);
        } catch (InvocationTargetException e) {
            fail("InvocationTargetException in " + monitorName);
        }
        if(actualValue != reportedValue){
            testFailed=true;
            errorLog.add(String.format("%s Expected 0x%X but was 0x%X", errorMsg, actualValue,
                                                                        reportedValue));
        }
    }

    /**
     * A helper function. Given it an array of integers and it returns a readable string
     * representation of them.
     * 
     * Used to help in creating useful error reports of failures. It puts the array of raw
     * can data in a user friendly format.
     * @param anArray   The array of integers to convert to a string
     * @return   A friendly string representation of the given array.
     */
    private String printBooleanArray(boolean[] anArray){
        StringBuilder s = new StringBuilder("[");
        for (int c =0; c<anArray.length; c++){
            if (c>0 && c<anArray.length)
                s.append(", ");
            s.append(String.format("%b", anArray[c]));
        }
        s.append("]");
        return s.toString();
    }
    
    /**
     * A helper function. Given it an array of integers and it returns a readable string
     * representation of them.
     * 
     * Used to help in creating useful error reports of failures. It puts the array of raw
     * can data in a user friendly format.
     * @param anArray   The array of integers to convert to a string
     * @return   A friendly string representation of the given array.
     */
    private String printDoubleArray(double[] anArray){
        StringBuilder s = new StringBuilder("[");
        for (int c =0; c<anArray.length; c++){
            if (c>0 && c<anArray.length)
                s.append(", ");
            s.append(String.format("%4.4f", anArray[c]));
        }
        s.append("]");
        return s.toString();
    }
    
    /**
     * A helper function. Given it an array of integers and it returns a readable string
     * representation of them.
     * 
     * Used to help in creating useful error reports of failures. It puts the array of raw
     * can data in a user friendly format.
     * @param anArray   The array of integers to convert to a string
     * @return   A friendly string representation of the given array.
     */
    private String printFloatArray(float[] anArray){
        StringBuilder s = new StringBuilder("[");
        for (int c =0; c<anArray.length; c++){
            if (c>0 && c<anArray.length)
                s.append(", ");
            s.append(String.format("%4.4f", anArray[c]));
        }
        s.append("]");
        return s.toString();
    }
    
    /**
     * A helper function. Given it an array of integers and it returns a readable string
     * representation of them.
     * 
     * Used to help in creating useful error reports of failures. It puts the array of raw
     * can data in a user friendly format.
     * @param anArray   The array of integers to convert to a string
     * @return   A friendly string representation of the given array.
     */
    private String printIntArray(int[] anArray){
        //We will print in hex if everything looks like a ubyte
        boolean printHex=true;
        for (int cat=0; cat<anArray.length; cat++)
            if(anArray[cat]>256||anArray[cat]<0)
                printHex=false;
        
        StringBuilder s = new StringBuilder("[");
        for (int c =0; c<anArray.length; c++){
            if (c>0 && c<anArray.length)
                s.append(", ");
            if (printHex)
                s.append(String.format("0x%02X", anArray[c]));
            else
                s.append(String.format("%d", anArray[c]));
        }
        s.append("]");
        return s.toString();
    }
    
    /**
     * A helper function. Given it an array of integers and it returns a readable string
     * representation of them.
     * 
     * Used to help in creating useful error reports of failures. It puts the array of raw
     * can data in a user friendly format.
     * @param anArray   The array of integers to convert to a string
     * @return   A friendly string representation of the given array.
     */
    private String printLongArray(long[] anArray){
        //We will print in hex if everything looks like a ubyte
        boolean printHex=true;
        for (int cat=0; cat<anArray.length; cat++)
            if(anArray[cat]>256||anArray[cat]<0)
                printHex=false;
        
        StringBuilder s = new StringBuilder("[");
        for (int c =0; c<anArray.length; c++){
            if (c>0 && c<anArray.length)
                s.append(", ");
            if (printHex)
                s.append(String.format("0x%02X", anArray[c]));
            else
                s.append(String.format("%d", anArray[c]));
        }
        s.append("]");
        return s.toString();
    }
    
    /**
     * Simple wrapper for set sim value.
     * Tries to set the given rca to the given value. Fails if there is an exception.
     * Has to use reflection. This test can only be run on a simulated device, which means that
     * we have a setSimValue function. But the class HardwareDevice does not have setSimValue...
     * @param rca
     * @param value
     * @param mPName
     * @throws Exception 
     */
    public void setValue(int rca, int[] value, String mPName, HardwareDevice device) throws Exception{
        Class c = device.getClass();
        Method m = c.getMethod("setSimValue", new Class[]{int.class, int[].class});
        m.invoke(deviceRef, rca, value);
    }

    public abstract HardwareDevice narrowToSimObject(final org.omg.CORBA.Object obj);

    private String componentName;
    private HardwareDevice deviceRef;
    private ArrayList<String> errorLog;
    //The number of significant figures to test for if the value is greater than 100
    private static final double SIGFIGSSMALL = 0.00001;
    //The number of significant figures to test for if the value is less than 100.
    private static final double SIGFIGSLARGE = 0.001;
    //The number of significant figures to test for if the value is greather than 1000000
    private static final double SIGFIGSXLARGE = 1000;
    private boolean testFailed;
    private TestMonitorDataPacket[] testData;
}
