/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.DevicePresentationModel;
import alma.Control.device.gui.common.widgets.util.DtsData.DataPacket;
import alma.Control.device.gui.common.widgets.util.DtsData.IDataGrabber;
import alma.Control.device.gui.common.widgets.util.DtsData.InsufficientDataException;

import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingWorker;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author David Hunter     dhunter@nrao.edu
 * @version $Id$
 * @since ALMA 5.0.4
 * 
 * DtsDataCaptureWidget provides a widget that reads from the DTX & DRX I2C interface using the
 * GET_I2C_DATA as specified in the ICD for said devices. 
 */

//NOTE: Throughout this package the lower bits are older and the upper ones newer.

//We don't need to extend MonitorPointWidget because the DataGrabber handles all of the icky monitor point
//details.
public class DtsDataCaptureWidget extends JPanel{
    

    /**
     * Creates a new DTS data capture widget.
     * @param logger
     * @param dPM  The presentation model for the device.
     * @param dataSource A ready to go DTS DataSource
     */
    public DtsDataCaptureWidget(Logger l, DevicePresentationModel dPM,
            IDataGrabber dataSource){
        logger=l;
        //Setup D bit display
        outputD = new JLabel("<No data>");
        outputD.setPreferredSize(new Dimension(320, 14));
        
        //Setup C bit display
        outputC = new JLabel("<No Data>");
        outputC.setPreferredSize(new Dimension(320, 14));

        //Setup B bit display
        outputB = new JLabel("<No Data>");
        outputB.setPreferredSize(new Dimension(320, 14));
        
        //Setup the rest.
        dataGrabber = dataSource;
        devicePresentationModel=dPM;
        //This button is not added to the disable controls stuff because it does not go to a control point.
        captureDataButton=new JButton(captureText);
        captureDataButton.addActionListener(new captureDataAction());
        devicePresentationModel.addControl(captureDataButton);
        sampleSize = new JSpinner(new SpinnerNumberModel(3, 1, 4096, 1));
        devicePresentationModel.addControl(sampleSize);
        sampleNumber = new JSpinner(new SpinnerNumberModel(1, 0, 4096, 1));
        sampleNumber.setEnabled(false);
        sampleNumber.addChangeListener(new updateDisplayAction());
//        devicePresentationModel.addControl(sampleNumber);
        captureProgressBar = new JProgressBar(0, 100);
        captureProgressBar.setEnabled(false);
        captureProgressBar.setStringPainted(true);
        captureProgressBar.setMaximumSize(new Dimension(320, 20));
        captureStatusReport = new JLabel("Ready to capture...");
        dataGrabber.setProgressBar(captureProgressBar);
        devicePresentationModel.addShutdownAction(new ActionListener(){
            public void actionPerformed(ActionEvent e) {dataGrabber.shutdown();}
        });
        buildWidget();
    }

    /* This function sets up the widget panel and initializes the widget to a default value of 0.
     * It should only be called by the constructor. 
     */
    protected void buildWidget() {
        this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder
                ("Capture Payload Data"),BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        //Setup the readouts
        c.gridy=0;
        c.gridx=1;
        c.weightx=100;
        c.weighty=0;
        c.fill=GridBagConstraints.BOTH;
        addLeftLabel("D Bits: ", c);
        add(outputD, c);
        outputD.setVisible(true);
        c.gridy++;
        addLeftLabel("C Bits: ", c);
        add(outputC, c);
        c.gridy++;
        addLeftLabel("B Bits: ", c);
        add(outputB, c);
        c.gridy++;
        //Setup the controls
        c.weightx=0;
        c.weighty=0;
        c.fill=GridBagConstraints.NONE;
        add (new JLabel("View Sample #"), c);
        c.gridy++;
        add(sampleNumber, c);
        c.gridy++;
        add (new JLabel("# of samples"), c);
        c.gridy++;
        add(sampleSize, c);
        c.gridy++;
        add(captureDataButton, c);
        c.gridy++;
        c.fill=GridBagConstraints.BOTH;
        add(captureProgressBar, c);
        c.gridy++;
        add(captureStatusReport, c);
    }

    //A simple helper function.
    private void addLeftLabel(String s, GridBagConstraints c) {
        c.gridx--;
        c.fill=GridBagConstraints.NONE;
        c.anchor=GridBagConstraints.EAST;
        add(new JLabel(s), c);
        c.anchor=GridBagConstraints.WEST;
        c.fill=GridBagConstraints.BOTH;
        c.gridx++;
    }

    
    /**
     * Just writes the output strings to the displays.
     * Should be called after captureData, logs an error if it is not.
     */
    
    private void updateDisplay(){
        //Updating the display during a capture can cause all sorts of trouble, so we
        //makd sure it does not happen.
        if (!captureLock){
            int index=(Integer)sampleNumber.getValue();
            try{
                outputD.setText(String.format("%016X", theData.getRawDataDUpper(index))+
                        String.format("%016X", theData.getRawDataDLower(index)));
                outputC.setText(String.format("%016X", theData.getRawDataCUpper(index))+
                        String.format("%016X", theData.getRawDataCLower(index)));
                outputB.setText(String.format("%016X", theData.getRawDataBUpper(index))+
                        String.format("%016X", theData.getRawDataBLower(index)));
            }
            catch (InsufficientDataException eeek){
                logger.severe("DtsDataCaptureWidget, updateDisplay:" +
                        "Error: InsurricientDataException: %s" + eeek.getMessage());
                outputD.setText("Error! Please try again.");
                outputC.setText("Error! Please try again.");
                outputB.setText("Error! Please try again.");
            }
        }
    }
    
    /**
     * The action listener for the Capture Data button. Spawns a SwingWorker thread that will
     * go and capture the data. Being a slow process, we don't want to do this on the swing update thread...
     * @author dhunter
     */
    private class captureDataAction implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            if (!captureLock){
                
                captureDataButton.setText(stopText);
                if ((Integer)(sampleNumber.getValue())>=((Integer)sampleSize.getValue()))
                    sampleNumber.setValue(Integer.valueOf(0));
                SwingWorker<Object, Object> myWorker = new captureDataWorker();
                myWorker.execute();
            }
            else{
                captureCanceled=true;
                dataGrabber.shutdown();
            }
        }
    }
    
    /**
     * A swing worker thread to capture the data. Sets up the progress bar, then calls captureData(). When
     * the data is captured it then writes the data to the display. It also keeps the report string up to
     * date.
     * @author dhunter
     */
    private class captureDataWorker extends SwingWorker<Object, Object>{
        @Override
        protected Object doInBackground() throws Exception {
            captureLock=true;
            captureCanceled=false;
            final int size = (Integer) sampleSize.getValue();
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    captureProgressBar.setEnabled(true);
                    captureProgressBar.setValue(0);
                    captureStatusReport.setText("Capturing data...");
                }
            });
            theData = new DataPacket();
            //TODO: Hook in a switch to select the data source.
            final int Q = dataGrabber.captureData(theData, size, 0);
            final int precentFinished = Q;
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    captureProgressBar.setValue(precentFinished);
                    try {
                        ((SpinnerNumberModel) sampleNumber.getModel())
                        .setMaximum(theData.getRawDataBLower().length - 1);
                    } catch (InsufficientDataException eeek) {
                        logger.severe("DtsDataCaptureWidget, captureDataWorker, doInBackground:" +
                                "Error: InsurricientDataException: %s" + eeek.getMessage());
                        ((SpinnerNumberModel) sampleNumber.getModel()).setMaximum(0);
                    }
                    sampleNumber.setEnabled(true);
                    if (precentFinished < 100)
                        if (captureCanceled)
                            captureStatusReport.setText("Capture Canceled...");
                        else
                            captureStatusReport.setText("Fifo Empty...");
                    else
                        captureStatusReport.setText("Done!");
                    captureProgressBar.setEnabled(false);
                    captureLock = false;
                    updateDisplay();
                }
            });
            captureDataButton.setText(captureText);
            return null;
        }
    }
    
    private class updateDisplayAction implements ChangeListener {
        @Override
        public void stateChanged(ChangeEvent e) {
            EventQueue.invokeLater(new Runnable(){public void run(){updateDisplay();}});}
    }
    
    //All sorts of variables, alphabetized by last name.
    private JButton captureDataButton;
    private JProgressBar captureProgressBar;
    private boolean captureLock = false;
    private JLabel captureStatusReport;
    private static final String captureText = "Capture Data";
    private boolean captureCanceled;
    private IDataGrabber dataGrabber;
    DevicePresentationModel devicePresentationModel;  //We need this for the enable/disable button to work.
    private Logger logger;
    private JLabel outputB;
    private JLabel outputC;
    private JLabel outputD;
    private JSpinner sampleNumber;
    private JSpinner sampleSize;
    private static final String stopText = "Cancel";
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private DataPacket theData;
}

//
// O_o
