/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.DevicePresentationModel;
import alma.Control.device.gui.common.MonitorPointPresentationModel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.swing.ButtonGroup;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;

/**
 * <code>WidgetPopupMenu</code> provides a <code>JPopupMenu</code> to adjust the functionality
 * of its associated widget's monitor points. This popup menu displays the polling rate adjustments
 * and the graphing options.
 * 
 * @author  Scott Rankin  srankin@nrao.edu
 * @author  David Hunter  dhunter@nrao.edu
 * @version $Id$
 * @since ALMA 5.1.1
 * 
 * TODO add logic to move all monitor points to fast or slow polling by default
 */

public class WidgetPopupMenu extends JPopupMenu implements MouseListener {
    
    /**
     * <code>WidgetPopupMenu</code> constructs a new popup menu for a monitor data GUI widget.
     * 
     * @param newLogger        The logger for severe errors.
     * @param graphable   True is this point is of a graphable type.
     * To be graphable the data type of the monitor point must both be in the GraphingPanell
     * graph creation logic, and there must be a proper graphing widget for it.
     */
    public WidgetPopupMenu(Logger newLogger, boolean graphable) {
        this.logger = newLogger;
        this.monitorPointPMs = new ArrayList<MonitorPointPresentationModel<?>>();
        
        pollingAction thePollingAction = new pollingAction();
        slowPollingItem = new JRadioButtonMenuItem("Slow updates");
        slowPollingItem.addActionListener(thePollingAction);
        slowPollingItem.setSelected(true);
        
        fastPollingItem = new JRadioButtonMenuItem("Fast updates");
        fastPollingItem.addActionListener(thePollingAction);
        
        menuItems = new ButtonGroup();
        menuItems.add(slowPollingItem);
        menuItems.add(fastPollingItem);
        
        add(slowPollingItem);
        add(fastPollingItem);
        
        isGraphable = graphable;
        
        if (isGraphable){
            //TODO: this code really should be here. But it is not due to the way PMs are added.
//            DevicePresentationModel dPM = monitorPointPMs.get(0).getContainingDevicePM();
//            dPM.getGraphingManager().addActionListener(new rebuildGraphMenuAction());
            
            addSeparator();
            JMenuItem graphItem = new JMenuItem("Graph this point!");
            add(graphItem);
            graphItem.addActionListener(new NewGraphWindowAction());
            graphSelectionMenu=new JMenu("Graph in existing window");
            graphSelectionMenu.setEnabled(false);
            add(graphSelectionMenu);
        }
    }
    
    /**
     * This function is called by the various monitor widgets to add their monitor point to the popup menu.
     * Not many widgets have more than one monitor point, but we do support it.
     * @param monitorPointModel     The monitor point to add.
     */
    public void addMonitorPointPM(MonitorPointPresentationModel<?> monitorPointModel) {
        monitorPointPMs.add(monitorPointModel);
        //Register for graph menu updates if needed
        if (!gettingGraphMenuUpdates && isGraphable){
            DevicePresentationModel dPM = monitorPointPMs.get(0).getContainingDevicePM();
            dPM.getGraphingManager().addActionListener(new rebuildGraphMenuAction());
            gettingGraphMenuUpdates=true;
        }
    }
    
    //These functions are Required by MouseListener, but we don't use them.
    //Sadly we can't use an adapter class because we have to extend JPopupMenu.
    public void mouseClicked(MouseEvent e) {}
    public void mouseEntered(MouseEvent e) {}
    public void mouseExited(MouseEvent e) {}
    public void mouseReleased(MouseEvent event) {}
    
    /**
     * <code>mousePressed</code> implements the <code>MouseListener</code> interface.
     * It listens  popup-menu-invoking clicks and then displays the popup menu.
     */
    public void mousePressed(MouseEvent event) {
        if (event.isPopupTrigger())
            show(event.getComponent(), event.getX(), event.getY());
    }
    
    /*
     * A private inner class to handle the graph actions. Launches a graph of the selected monitor point.
     * The number saved by the constructor is used to remember which plot number this action is attached to.
     * Note that the graph number might not match the plot number displayed in the window name. However, it
     * should match the proper graph in the graph list within the GraphManager. These number mismatches are
     * caused by the user closing windows.
     * 
     * For example, after Plot 2 is closed, Plot 3 might be given an internal number of 2. This means it will
     * be in slot 2 in the popup menu, and is stored in slot 2 in the GraphManager. But it retains the name
     * Plot 3 in the title because we don't want to confuse users by changing the number.
     */
    private class AddGraphToWindowAction implements ActionListener {
        AddGraphToWindowAction(int newWindowNumber){
            windowNumber=newWindowNumber;}
        
        public void actionPerformed(ActionEvent e) {
            DevicePresentationModel dPM = monitorPointPMs.get(0).getContainingDevicePM();
            dPM.getGraphingManager().addGraphToPanel(monitorPointPMs.get(0), windowNumber);
        }
        private final int windowNumber;
    }
    /*
     * A private inner class to handle the graph actions.
     * Launches a graph of the selected monitor point.
     */
    private class NewGraphWindowAction implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            DevicePresentationModel dPM = monitorPointPMs.get(0).getContainingDevicePM();
            dPM.getGraphingManager().createNewPanel(monitorPointPMs.get(0));
        }
    }
    /**
     * <code>actionPerformed</code> implements the <code>ActionListener</code> interface.
     * It listens for a selection in the popup menu and changes the polling speed
     * of associated monitor points
     */
    private class pollingAction implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            JRadioButtonMenuItem source = (JRadioButtonMenuItem) event.getSource();
            if (source == slowPollingItem) {
                for (MonitorPointPresentationModel<?> mp : monitorPointPMs) {
                    mp.setFastPolling(false);
                }
            } else if (source == fastPollingItem) {
                for (MonitorPointPresentationModel<?> mp : monitorPointPMs) {
                    mp.setFastPolling(true);
                }
            }
        }
    }
    
    /**
     * This popup menu action provides updating, for enabling and disabling the graph in existing windows
     * option. When the number of GraphPanels changes this action should be called to update the menu.
     */

    private class rebuildGraphMenuAction implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            DevicePresentationModel dPM = monitorPointPMs.get(0).getContainingDevicePM();
            if (dPM.getGraphingManager().getNumberOfWindows()>0){
                if (graphSelectionMenu != null)
                    graphSelectionMenu.removeAll();
                else{
                    logger.severe("WidgetPopupMenu.java:" +
                                  "    Error: graphSelectionMenu null when it should not be!");
                    //We can recover from this error.
                    graphSelectionMenu = new JMenu("Graph in existing window");
                }
                String newName;
                int numWin=dPM.getGraphingManager().getNumberOfWindows();
                JMenuItem newOption;
                for (Integer c=0; c<numWin; c++){
                    newName=dPM.getGraphingManager().getGraphPanelTitle(c);
                    newOption = new JMenuItem(newName);
                    newOption.addActionListener(new AddGraphToWindowAction(c));
                    graphSelectionMenu.add(newOption);
                }
                add(graphSelectionMenu);
                graphSelectionMenu.setEnabled(true);
            }
            else
                graphSelectionMenu.setEnabled(false);
            revalidate();
        }
    }
    
    private JRadioButtonMenuItem fastPollingItem;
    private JMenu graphSelectionMenu;
    private boolean isGraphable;
    private Logger logger;
    private ButtonGroup menuItems;
    private ArrayList<MonitorPointPresentationModel<?>> monitorPointPMs;
    private static final long serialVersionUID = 1L;
    private JRadioButtonMenuItem slowPollingItem;
    
    //TODO: replace the logic that uses this.
    private boolean gettingGraphMenuUpdates=false;
}
