/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.HardwareDevicePackage.HwState;
import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;

import java.util.logging.Logger;

public class CommunicationStatusIndicator extends MonitorPointWidget {
	
	private static final long serialVersionUID = 1L;
	private StatusIndicator statusIndicator;
	
	public CommunicationStatusIndicator(Logger logger, MonitorPointPresentationModel<HwState> monitorPointPM) {
		super(logger);
		addMonitorPointPM(monitorPointPM, HwState.Undefined);
		buildWidget();
	}
	
	public void buildWidget() {
		statusIndicator = new StatusIndicator(Status.COMM_DISCONNECTED, "Disconnected");
		add(statusIndicator);
	}
	
	@Override
	protected void updateAttention(Boolean value) {
		// Nothing to do in this class.
	}

	protected void updateCommunication(Boolean value) {
		if (value.booleanValue()) {
			statusIndicator.setStatus(Status.COMM_DISCONNECTED, "Disconnected");
		} else {
			statusIndicator.setStatus(Status.COMM_CONNECTED, "Connected");
		}
	}
	
	@Override
	protected void updateValue(IMonitorPoint source) {
		// TODO: fill out
	}
}