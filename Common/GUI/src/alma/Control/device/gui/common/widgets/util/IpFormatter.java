/* ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;

import javax.swing.text.DefaultFormatter;

import java.util.logging.Logger;

/**
 * A formatter for IPv4 addresses.
 *
 * This formatter limits a JFormattedTextField to a valid IPV4 address. DNS lookup is attempted
 * for host names.
 *
 * Use of this formatter will cause the JFormattedTextField to reject all other
 * input. See the documentation on JFormattedTextField for the various ways it
 * reacts to invalid input.
 *
 *
 * @author David Hunter dhunter@nrao.edu
 * @version 
 * @since ALMA 8.0.0
 */
public class IpFormatter extends DefaultFormatter {

    /**
     * Create and instance of this class; there is nothing user-setable, but a logger is required.
     */
    public IpFormatter(Logger l) {
        super();
        logger=l;
    }

    /**
     * Check IPs, and do DNS lookup to convert a string to an IP.
     *
     * @param text
     *            the string to convert.
     * @return the integer value of the string.
     * @throws ParseException
     *            if the string does not contain a valid integer.
     */
    @Override
    public Object stringToValue(final String text) throws ParseException {
        InetAddress result;
        //Try decoding with InetAddress; if that can't decode it, then it is not a good address.
        try {
            if (text.isEmpty())
                return InetAddress.getByName("0.0.0.0" );
            result = InetAddress.getByName(text);
        } catch (UnknownHostException n) {
            throw new ParseException("Not a valid IP or host unknown", 0);
        } catch (SecurityException s)
        {
            logger.severe("IpFormatter, stringToValue: Error, " +
                          "Security EXception when attempting host resolution.");
            throw new ParseException("Security EXception when attempting host resolution.", 0);
        }
        InetAddress a;
        //If we got an IP V6 address, reject it - this formatter is V4 only.
        if(result.getAddress().length != 4)
            throw new ParseException("Error: only IP V4 address accepted.", 0);
        try {
            a = InetAddress.getByAddress(result.getAddress());
        } catch (UnknownHostException e) {
            throw new ParseException("Error: unable to get raw address.", 0);
        }
        return a;
    }

    /**
     * Convert an integer to a String.
     *
     * Ensure the integer is in range for this instance.
     *
     * @param value
     *            the integer to convert to a String
     * @return a String representing the integer value.
     * @throws ParseException
     *             if value is not an integer or if the value is out of range.
     */
    @Override
    public String valueToString(final Object value) throws ParseException {
        if (!(value instanceof InetAddress)) {
            throw new ParseException("Not an InetAddress", 0);
        }
        InetAddress theIP = (InetAddress) value;
        return theIP.getHostAddress();
    }

    Logger logger;
    private static final long serialVersionUID = 1L;
}
