/* ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets.util;

import java.net.UnknownHostException;
import java.text.ParseException;
import javax.swing.text.DefaultFormatter;
import java.util.logging.Logger;

/**
 * A formatter for IPv4 subnet masks
 *
 * This formatter limits a JFormattedTextField to a valid IPV4 subnet mask
 *
 * Use of this formatter will cause the JFormattedTextField to reject all other
 * input. See the documentation on JFormattedTextField for the various ways it
 * reacts to invalid input.
 *
 *
 * @author David Hunter dhunter@nrao.edu
 * @version 
 * @since ALMA 8.0.0
 */
public class SubnetMaskFormatter extends DefaultFormatter {

    /**
     * Create and instance of this class; there is nothing user-setable, but a logger is required.
     */
    public SubnetMaskFormatter(Logger l) {
        super();
        logger=l;
    }

    /**
     * Check IPs, and do DNS lookup to convert a string to a mask.
     *
     * @param text
     *            the string to convert.
     * @return the integer value of the string.
     * @throws ParseException
     *            if the string does not contain a valid integer.
     */
    @Override
    public Object stringToValue(final String text) throws ParseException {
        int[] result = new int[4];
        String[] numArray=text.split("\\.");
        if (numArray.length !=4){
            throw new ParseException("Non-valid IPv4 subnet mask: wrong nuber of dots (.)", 0);}
        for (int c=0; c<4; c++)
            try{
                result[c]=Integer.parseInt(numArray[c]);
            } catch (NumberFormatException e){
                throw new ParseException("Non-valid IPv4 subnet mask: nonnumeric input found!", 0);
            }
            
        //If the top byte is not 255, this is not a valid subnet mask.
        if (result[0] != 255){
            throw new ParseException("Non-valid IPv4 subnet mask: top byte is not 255", 0);
        }
        //Combine the bytes into a single number
        int test = result[3];
        test+=result[2]<<8;
        test+=result[1]<<16;
        boolean checkForZeros=true;
        for (int c=0; c<24; c++){
            if(checkForZeros){
               if(test%2==1)
                   checkForZeros=false;
            }
            else
                if(test%2==0){
                    throw new ParseException("Non-valid IPv4 subnet mask: not a mask.", 0);
                }
            test=test>>1;
        }
        return result;
    }

    /**
     * Convert an integer to a String.
     *
     * Ensure the integer is in range for this instance.
     *
     * @param value
     *            the integer to convert to a String
     * @return a String representing the integer value.
     * @throws ParseException
     *             if value is not an integer or if the value is out of range.
     */
    @Override
    public String valueToString(final Object value) throws ParseException {
        StringBuilder s = new StringBuilder();
        if (!(value instanceof int[])) {
            throw new ParseException("Not a subnet mask", 0);
        }
        int[] addr=(int[]) value;
        if (addr.length != 4)
            throw new ParseException("Not a valid IPv4 subnet mask", 0);
        for (int c=0; c<4; c++){
            s.append(addr[c]);
            if (c!=3)
                s.append(".");
        }
        return s.toString();
    }

    Logger logger;
    private static final long serialVersionUID = 1L;
}
