/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.ControlPointPresentationModel;
import alma.Control.device.gui.common.IControlPoint;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.JFormattedTextField.AbstractFormatter;

/**
 * This control point widget displays an integer input field and a control button. When the button is pressed
 * the value is sent to the associated control point. It requires that a formatter be given to validate the
 * user input. That formatter must return integers. Some custom formatters can be found in widgets/util
 * 
 * @author  David Hunter  dhunter@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

public class IntegerControlPointWidget extends JPanel implements ActionListener {

    //Note: if the control point supports range checking, the lower of the given bound and the
    //built in range is used.
    public IntegerControlPointWidget( Logger logger,
            ControlPointPresentationModel<Integer> controlPointModel,
            int newLowerBound, int newUpperBound, AbstractFormatter newFormatter, String buttonText){
        this.logger = logger;
        this.controlPointModel = controlPointModel;
        
        lowerBound=newLowerBound;
        upperBound=newUpperBound;

        IControlPoint cp = controlPointModel.getControlPoint();
        if (cp.supportsRangeChecking()) {
            int tmp = cp.getRangeLowerBound().intValue();
            if (tmp>lowerBound)
                lowerBound=tmp;
            tmp = cp.getRangeUpperBound().intValue();
            if (tmp<upperBound)
                upperBound=tmp;
        }

        inputField = new JFormattedTextField(newFormatter);
        inputField.setPreferredSize(new Dimension(60, 20));
        inputField.setMargin(new Insets(0, 2, 0, 2)); 
        if (lowerBound>0)
            inputField.setValue(lowerBound);
        else
            inputField.setValue(0);
        controlButton = new JButton(buttonText);
        controlButton.addActionListener(this);
        controlButton.setMargin(new Insets(0, 2, 0, 2));
        controlPointModel.getContainingDevicePM().addControl(this.inputField);
        controlPointModel.getContainingDevicePM().addControl(this.controlButton);
        buildWidget();
    }

    /* This rounding algorithm assumes that 0.0 is a valid value on the 
     * input scale. If cases are found where this is not true then this
     * function will need updated to properly handle them.
     */
    public void actionPerformed(ActionEvent e) {
        int newValue=(Integer)inputField.getValue();
        setValue(newValue);
    }

    private void buildWidget() {
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));		
        add(inputField);
        add(controlButton);
    }

    //Potential problem: this needs to run the new value through the formatter
    //Can we just make this private, or maybe remove it?
    public void setValue(int value) {
        controlPointModel.setValue(value);
    }

    private JButton controlButton;
    private ControlPointPresentationModel<Integer> controlPointModel;
    private JFormattedTextField inputField;
    private Logger logger;
    private int lowerBound=Integer.MIN_VALUE;
    private static final long serialVersionUID = 1L;
    private int upperBound=Integer.MAX_VALUE;
}
