/*
ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2010 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets.util.DtsData;

/** 
 * @author  dhunter
 * @version $Id$
 * @since    
 */

/**
 * This class calculates statistics on one polarity of a given set of data from a DTX or DRX. If you need
 * statistics on both polarities simply use 2 instances of DataStatistics.
 * 
 * Input data:
 *   This class expects a fully populated DataPacket to be given to it.
 * 
 * Outputs statistics:
 *     -Amplitude error on all 16 channels.
 *          This is the difference between the expected and actual standard deviation.
 *     -Offset error on all all 16 channels.
 *          This is the difference between the expected and actual mean.
 *     -Probability Density Function.
 *          An array of 7 values, each of which contains the probability of that alma value.
 *          -Slot 0 has data for value -4, slot 1 for -3, etc.
 *           
 * Acronyms:
 *   PDF, or Pdf: Probability Density Function.
 */
public class DataStatistics {
    /**
     * Construct a new DataStatistics object, of which will provide statistics on the given DataPacket for the
     * given polarity. Note that this class assumes does not make a separate copy of the given DataPacket,
     * and it is assumed that the data will not change.
     * @param inputData The data to calculate statistics on.
     * @param polarity The polarity (false for 0, true for 1)
     */
    public DataStatistics(DataPacket inputData, boolean thePolarity){
        dataFromDevice=inputData;
        polarity=thePolarity;
    }
    
    
    /**
     * This function returns the amplitude error for each channel.
     * It is the difference between the theoretical and actual standard deviation.
     * @return an array of 16 doubles, containing the amplitude error for all 16 channels.
     * @throws InsufficientDataException
     */
    public double[] getAmplitudeError() throws InsufficientDataException{
        if(amplitudeError==null)
            calcAmplitudeError();
        return java.util.Arrays.copyOf(amplitudeError, amplitudeError.length);
    }
    
    /**
     * Returns the number of occurrences.
     * Comes as an array, with # of -4s in slot 0, -3s in slot 1, etc.
     * Used mostly to test countOccurances.
     * @return The number of occurrences per channel.
     * @throws InsufficientDataException
     */
    public int[] getOccuranceCount() throws InsufficientDataException{
        if(occurrenceCount==null)
            countOccurances();
        return java.util.Arrays.copyOf(occurrenceCount, occurrenceCount.length);
    }
    
    /**
     * The offset error is the difference between the actual mean and expected mean. This function returns
     * the offset error for each channel. It is presented in an array of 16 doubles, with element 0 being the
     * offset error for channel 0, etc.
     * @return
     * @throws InsufficientDataException
     */
    public double[] getOffsetError() throws InsufficientDataException{
        if(offsetError==null)
            calcOffsetError();
        return java.util.Arrays.copyOf(offsetError, offsetError.length);
    }


    /**
     * Returns the probability of each occurrence.
     * Comes as an array, with the probability of -4s in slot 0, -3s in slot 1, etc.
     * @return An array of 8 doubles with the given probabilities.
     * @throws InsufficientDataException 
     */
    public double[] getPdf() throws InsufficientDataException{
        if(occurrenceCount==null)
            countOccurances();
        double[] probabilities = new double[8];
        for (int c=0; c<8; c++)
            probabilities[c] = ((double)occurrenceCount[c])/numAlmaValues;
        return probabilities;
    }
    
    /**
     * Calculates the mean from a PDF using the sum of (value*probability)
     * This function is intended only for use with arrays of the probability of alma
     * values, so it should always have the probabilities 8 values (-4 to 3).
     * @return
     * @throws InsufficientDataException
     */
    protected double calcMeanFromPdf(double odds[]){
        double mean=0;
        for (int c=0; c<8; c++)
            mean+=odds[c]*(c-4);
        return mean;
    }
    
    /**
     * Calculates the standard deviation of a set of alma values when given the Pdf. This is done based on
     * the following formula: sd=sqrt(integral((x-mean)^2*probability(x)dx)). Since we are dealing with
     * discrete probabilities for the values -4 to 3 we can manually integrate this with a for loop, and
     * take the square root of the result. 
     * @param odds
     * @return the standard deviation
     */
    protected double calcSdFromPdf(double odds[]){
        double theMean = calcMeanFromPdf(odds);
        double result=0;
        for (int c=0; c<8; c++)
            result+=Math.pow(c-4-theMean, 2)*odds[c];
        result=Math.sqrt(result);
        return result;
    }
    
    /**
     * Converts a set of alma gray code bits into an alma value.
     * Uses conversion defined in BEND-50.00.00.00-306-A-MAN
     * @param chD
     * @param chC
     * @param chB
     * @return
     */
    protected byte valueFromGrayCode(boolean chD, boolean chC, boolean chB){
        if (!chD && !chC && !chB)
            return -4;
        if (!chD && !chC && chB)
            return -3;
        if (!chD && chC && chB)
            return -2;
        if (!chD && chC && !chB)
            return -1;
        if (chD && chC && !chB)
            return 0;
        if (chD && chC && chB)
            return 1;
        if (chD && !chC && chB)
            return 2;
        return 3;
    }

    private void calcAmplitudeError() throws InsufficientDataException{
        if(pdfByChannel==null)
            calcPdfByChannel();
        amplitudeError = new double[16];
        double theoreticalSd=calcSdFromPdf(theoreticalPdf);
        for (int c=0; c<16; c++)
            amplitudeError[c]=calcSdFromPdf(pdfByChannel[c])-theoreticalSd;
    }

    /**
     * This function calculates the offset error for each of the 16 channels.
     * @throws InsufficientDataException
     */
    private void calcOffsetError() throws InsufficientDataException{
        if(pdfByChannel==null)
            calcPdfByChannel();
        offsetError=new double[16];
        //The theoretical distribution is a normal curve centered around -0.5.
        double theoreticalMean = -0.5;
        for (int c=0; c<16; c++){
            offsetError[c]=calcMeanFromPdf(pdfByChannel[c])-theoreticalMean;
        }
    }
    
    /**
     * This function counts the number of occurrences of each alma value
     * @throws InsufficientDataException
     */
    private void countOccurances() throws InsufficientDataException{
        byte[][] almaValues;
        if (polarity)
            almaValues = dataFromDevice.getAlmaValuesPol1();
        else
            almaValues = dataFromDevice.getAlmaValuesPol0();
        numSamples = almaValues.length;
        //We have 16 channels per sample, giving us 16 alma values per sample.
        numAlmaValues = numSamples*16;
        occuranceCountByChannel = new int[16][8];
        //Loop through all the data, counting occurrence by channel.
        for (int c=0; c<numSamples; c++)
            //This loop selects the channel
            for (int c2=0; c2<16; c2++)
                //For the current channel increment the slot counting the current value.
                occuranceCountByChannel[c2][almaValues[c][c2]+4]++;
        //Now count total occurrences.
        occurrenceCount = new int[8];
        for (int c=0; c<16; c++)
            for (int c2=0; c2<8; c2++)
                occurrenceCount[c2]+=occuranceCountByChannel[c][c2];
    }
    
    /**
     * Calculate the actual PDF for each of the 16 channels.
     * @throws InsufficientDataException
     */
    private void calcPdfByChannel() throws InsufficientDataException{
        if (occuranceCountByChannel==null)
            countOccurances();
        pdfByChannel=new double[16][8];
        for (int c=0; c<16; c++)
            for (int c2=0; c2<8; c2++)
                pdfByChannel[c][c2] = ((double)occuranceCountByChannel[c][c2])/numSamples;
    }
    
    //The packet of data retrieved from the device.
    private DataPacket dataFromDevice;
    
    //The amplitude and offset error provide information for tuning the digitizer.
    //The values are calculated separately for each of the 16 channels in the DTX.
    private double[] amplitudeError;
    private double[] offsetError;
    
    //The number of samples of alma data that we are working with. Set by countOccurances.
    private int numAlmaValues=-1;
    //The number of raw data samples that we are working with.
    private int numSamples=-1;
    
    //This will store the number of occurrences of each alma value in an array of 8 values.
    //Slot 0 is the occurrences of -4, slot 1 for -3, ..., and slot 7 for 3.
    private int[] occurrenceCount;
    //This will store the number of occurrences of each alma value in each channel.
    //This forma a 2-D array of 16 channels by the 8 alma values.
    private int[][] occuranceCountByChannel;
    
    //This will store the PDF for each channel in a 2-D array, in the form [channel][odds of alma value].
    private double[][] pdfByChannel;
    private final boolean polarity;
    
    //This is the theoretical PDF (probability density function), or what we expect the values to be, assuming
    //that we are seeing incoming boadband Gaussian noise. Used to calculate the offset error and
    //amplitude error.
    //From 50.00.00.00-306-A-MAN
    private final double[] theoreticalPdf = {0.0349931, 0.0785305, 0.1593980, 0.2270780,
                               0.2270780, 0.1593980, 0.0785305, 0.0349931};
}
