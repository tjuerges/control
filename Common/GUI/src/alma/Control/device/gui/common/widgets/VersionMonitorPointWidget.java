/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;

import java.util.logging.Logger;

import javax.swing.JLabel;

/**This monitor point provides a readout of the version information of a hardware device. This
 * is normally a major version number, minor version number, and firmware date. Optionally, a patch can be
 * included, and the date can be excluded.
 * 
 * @author  David Hunter  dhunter@nrao.edu
 * @author  Scott Rankin  srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 5.0.4
 */
public class VersionMonitorPointWidget extends MonitorPointWidget {

    /**Create a new version monitor point widget.
     * @param logger        The logger to log any errors to (currently unused, but may be needed later)
     * @param majorModel    The major version number monitor point (type integer)
     * @param minorModel    The minor version number monitor point (type integer)
     */
    public VersionMonitorPointWidget(Logger logger,
            MonitorPointPresentationModel<Integer> majorModel,
            MonitorPointPresentationModel<Integer> minorModel) {
        this(logger, majorModel, minorModel, null, null, null, null);
    }

    /**Create a new version monitor point widget.
     * @param logger        The logger to log any errors to (currently unused, but may be needed later)
     * @param majorModel    The major version number monitor point (type integer)
     * @param minorModel    The minor version number monitor point (type integer)
     * @param patchModel    The patch level monitor point (type integer)
     */
    public VersionMonitorPointWidget(Logger logger,
            MonitorPointPresentationModel<Integer> majorModel,
            MonitorPointPresentationModel<Integer> minorModel,
            MonitorPointPresentationModel<Integer> patchModel) {
        this(logger, majorModel, minorModel, patchModel, null, null, null);
    }

    /**Create a new version monitor point widget.
     * @param logger        The logger to log any errors to (currently unused, but may be needed later)
     * @param majorModel    The major version number monitor point (type integer)
     * @param minorModel    The minor version number monitor point (type integer)
     * @param dayModel      The firmware compile day monitor point (type integer)
     * @param monthModel    The firmware compile month monitor point (type integer)
     * @param yearModel     The firmware compile year monitor point (type integer)
     */
    public VersionMonitorPointWidget(Logger logger,
            MonitorPointPresentationModel<Integer> majorModel,
            MonitorPointPresentationModel<Integer> minorModel,
            MonitorPointPresentationModel<Integer> dayModel,
            MonitorPointPresentationModel<Integer> monthModel,
            MonitorPointPresentationModel<Integer> yearModel) {
        this(logger, majorModel, minorModel, null, dayModel, monthModel, yearModel);
    }

    /**Create a new version monitor point widget.
     * @param logger        The logger to log any errors to (currently unused, but may be needed later)
     * @param majorModel    The major version number monitor point (type integer)
     * @param minorModel    The minor version number monitor point (type integer)
     * @param patchModel    The patch level monitor point (type integer)
     * @param dayModel      The firmware compile day monitor point (type integer)
     * @param monthModel    The firmware compile month monitor point (type integer)
     * @param yearModel     The firmware compile year monitor point (type integer)
     */
    public VersionMonitorPointWidget(Logger logger,
            MonitorPointPresentationModel<Integer> majorModel,
            MonitorPointPresentationModel<Integer> minorModel,
            MonitorPointPresentationModel<Integer> patchModel,
            MonitorPointPresentationModel<Integer> dayModel,
            MonitorPointPresentationModel<Integer> monthModel,
            MonitorPointPresentationModel<Integer> yearModel) {
        super(logger, true);

        majorMonitorPoint = addMonitorPointPM(majorModel, Integer.valueOf(0));
        minorMonitorPoint = addMonitorPointPM(minorModel, Integer.valueOf(0));

        if (null != patchModel) {
            patchMonitorPoint = addMonitorPointPM(patchModel, Integer.valueOf(0));
        }
        if (null != dayModel) {
            dayMonitorPoint = addMonitorPointPM(dayModel, Integer.valueOf(0));
        }
        if (null != monthModel) {
            monthMonitorPoint = addMonitorPointPM(monthModel, Integer.valueOf(0));
        }
        if (null != yearModel) {
            yearMonitorPoint = addMonitorPointPM(yearModel, Integer.valueOf(0));
        }
        buildWidget();
    }

    protected void buildWidget() {
        widgetLabel = new JLabel();
        widgetLabel.setForeground(StatusColor.NORMAL.getColor());
        widgetLabel.setText("0.0.0 (00/00/00)");
        add(widgetLabel);
        addPopupMenuToComponent(widgetLabel);
    }

    @Override
    protected void updateAttention(Boolean q) {
        //We are not doing error checking on version information.
    }

    @Override
    protected void updateValue(IMonitorPoint source) {
        Integer majorValue = getCachedValue(majorMonitorPoint);
        Integer minorValue = getCachedValue(minorMonitorPoint);
        Integer patchValue = getCachedValue(patchMonitorPoint);
        Integer dayValue = getCachedValue(dayMonitorPoint);
        Integer monthValue = getCachedValue(monthMonitorPoint);
        Integer yearValue = getCachedValue(yearMonitorPoint);

        String s = String.format("%d.%d", majorValue, minorValue);

        if (null != patchValue) {
            s += String.format(".%d", getCachedValue(patchMonitorPoint));
        }
        if (null != dayValue && null != monthValue && null != yearValue) {
            //If the year is just 2 digets, we assume that it is 20xx. Otherwise we
            //assume that it is the full year.
            if (yearValue<100)
                yearValue+=2000;
            s += String.format("(%04d/%02d/%02d)", yearValue, monthValue, dayValue);
        }
        widgetLabel.setText(s);
    }

    private IMonitorPoint dayMonitorPoint = null;
    private IMonitorPoint majorMonitorPoint = null;
    private IMonitorPoint minorMonitorPoint = null;
    private IMonitorPoint monthMonitorPoint = null;
    private IMonitorPoint patchMonitorPoint = null;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private JLabel widgetLabel;
    private IMonitorPoint yearMonitorPoint = null;
}

//
// O_o
