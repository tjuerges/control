/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;

import java.util.logging.Logger;

import javax.swing.JLabel;

/**
 * @author David Hunter dhunter@nrao.edu
 * @version $Id$
 * @since 
 * 
 *        BendMonitorPointWidget offers a JPanel containing a textual
 *        representation of an integer (in hex or dec format) from a monitor
 *        point with range and communication verification/alarms.
 * 
 *        It is a simple variant of IntegerMonitorPointWidget intended for use with some Back End
 *        serial numbers. These numbers indicate an unknown serial number by reading 0xFFFF. This
 *        widget correctly displays "S/N unknown" when that happens. It also removes the
 *        integer functionality that is not useful in displaying a s/n. E.g. units.
 */
public class BendSerialMonitorPointWidget extends MonitorPointWidget {
    
    /**Construct a new BendMonitorPointWidget
     * @param logger The logger that all logs should be written to.
     * @param monitorPointModel The monitor point to display.
     * @param includeUnits True to include the units as given by the monitor point
     * @param displayInHex True to display the value in hexadecimal format.
     */
    public BendSerialMonitorPointWidget(Logger logger,
            MonitorPointPresentationModel<Integer> monitorPointModel) {
        super(logger, true, true);
        monitorPoint = addMonitorPointPM(monitorPointModel, Integer.valueOf(0));

        buildWidget();
    }

    protected void buildWidget() {
        widgetLabel = new JLabel();
        widgetLabel.setText("0");
        widgetLabel.setForeground(StatusColor.NORMAL.getColor());
        add(widgetLabel);
        addPopupMenuToComponent(widgetLabel);
    }

    /**We don't support range checking on S/Ns, other than the S/N not found note.
     * 
     */
    @Override
    protected void updateAttention(Boolean q) {
    }

    @Override
    protected void updateValue(IMonitorPoint source) {
        String newValue="";
        int value=(Integer)getCachedValue(monitorPoint);
        if (value != 0xFFFF)
            newValue=String.format("%d", value);
        else
            newValue="S/N unknown!";
        widgetLabel.setText(newValue);
    }

    private IMonitorPoint monitorPoint;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private JLabel widgetLabel;
}

//
// O_o
