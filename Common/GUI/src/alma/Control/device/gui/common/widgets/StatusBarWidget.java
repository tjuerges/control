/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.CommonIdlMonitorPoints;
import alma.Control.device.gui.common.DevicePresentationModel;
import alma.exec.extension.subsystemplugin.PluginContainerServices;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
/**
 * @author Scott Rankin srankin@nrao.edu
 * @author David Hunter dhunter@nrao.edu
 * @version 
 * @since ALMA 5.0.4
 * Containing
 */
public class StatusBarWidget extends JPanel {

    private static final long serialVersionUID = 1L;
    private Logger logger;
    protected DevicePresentationModel devicePresentationModel;

    public StatusBarWidget(Logger logger, DevicePresentationModel devicePresentationModel) {
        this.logger = logger;
        this.devicePresentationModel = devicePresentationModel;
        buildWidget();
    }

    public void buildWidget() {
//        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        setLayout (new GridBagLayout());
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        GridBagConstraints c = new GridBagConstraints();
        c.fill=GridBagConstraints.BOTH;
        c.anchor=GridBagConstraints.WEST;
        c.weightx=1;
        c.weighty=1;
        c.gridx=0;
        c.gridy=0;
        DeviceStatusBarWidget d = new DeviceStatusBarWidget(logger,
                devicePresentationModel.getMonitorPointPM(CommonIdlMonitorPoints.getHwState),
                devicePresentationModel.getMonitorPointPM(CommonIdlMonitorPoints.inErrorState));
        d.setMinimumSize(new Dimension(80, 25));
        add(d, c);
        c.gridx++;
        c.weightx=100;
        c.fill=GridBagConstraints.NONE;
        add(new MessageStatusBarWidget(logger,
                devicePresentationModel.getMonitorPointPM(CommonIdlMonitorPoints.getErrorMessage)), c);
        c.weightx=1;
        c.fill=GridBagConstraints.BOTH;
        c.gridx++;
        ToggleControlsButton t = new ToggleControlsButton();
        t.setMinimumSize(new Dimension(140, 25));
        add(t, c);
        c.gridx++;
        AboutBoxButton boxButton = new AboutBoxButton(new JFrame());
        c.insets=new Insets(0,3,0,3);
        add(boxButton, c);
        c.gridx++;
        CommunicationStatusIndicator i = new CommunicationStatusIndicator(logger,
                devicePresentationModel.getMonitorPointPM(CommonIdlMonitorPoints.getHwState));
        i.setMinimumSize(new Dimension(25, 25));
        add(i, c);
        setVisible(true);
    }
    
    private class ToggleControlsButton extends JButton implements ActionListener {

        private static final long serialVersionUID = 1L;

        public ToggleControlsButton() {
            setButtonText();
            addActionListener(this);
            if (devicePresentationModel.allowControles())
                this.setEnabled(true);
            else
                this.setEnabled(false);
        }

        public void actionPerformed(ActionEvent event) {
            devicePresentationModel.setControlsEnabled(!devicePresentationModel.getControlsEnabled());
            setButtonText();
        }

        private void setButtonText() {
            if (!devicePresentationModel.getControlsEnabled())
                setText("Enable Controls");
            else
                setText("Disable Controls");            
        }
    }
    
    private class AboutBoxButton extends JButton implements ActionListener {

        private static final long serialVersionUID = 1L;

        public AboutBoxButton(JFrame owner) {
            super("About");
            addActionListener(this);
            dialog = new AboutDialog();
            setMargin(new Insets(0, 2, 0, 2));
        }

        public void actionPerformed(ActionEvent event) {
            dialog.setVisible(true);
        }
        
        public class AboutDialog extends JFrame
        {
            public AboutDialog()
            {
                super();
                this.setSize(new Dimension(520, 280));
                this.setTitle("About Control Device GUIs");
                this.setLayout(new GridBagLayout());
                GridBagConstraints c = new GridBagConstraints();
                c.gridx=0;
                c.gridy=0;
                add (new JLabel("Control Device GUIs"), c);
                c.gridy++;
                JTextArea notice = new JTextArea(
                    "By David Hunter <dhunter@nrao.edu>\n" +
                    "\n" +
                    "These GUIs provide a low level GUI for Alma hardware devcies. They are\n" +
                    "intended for use in checking the status of hardware devices, making corrections\n" +
                    "to devices, and for debugging.\n" +
                    "\n" +
                    "Instructions can be found at:\n" +
                    "http://almasw.hq.eso.org/almasw/bin/view/CONTROL/DeviceGUIUsersDocsALMA800\n" +
                    "\n" +
                    "If, after reading the instructions, you have questions, comments, or suggestions,\n"+
                    "please contact the authors. Bugs should be reported in a Jira ticket, assigned\n" +
                    "to Computing/CONTROL. David Hunter should be included as an additional user to\n" +
                    "email.");
                notice.setEditable(false);
                add(notice, c);
                c.gridy++;
                JButton ok = new JButton("Ok");
                ok.addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        setVisible(false);
                    }
                });
                add(ok, c);
            }
            
            private static final long serialVersionUID = 1L;
        }
        
        AboutDialog dialog;
    }
}
