/* ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets.util;

import java.sql.Time;
import java.text.ParseException;
import java.util.logging.Logger;

import javax.swing.text.DefaultFormatter;

/**
 * A formatter for inputting a time in the format hh:mm:ss.
 *
 * This formatter will limit the input to a time string in the format:
 * [0-2][0-4]:[0-5][0-9]:[0-5][0-9].
 *
 * Use of this formatter will cause the JFormattedTextField to reject all other
 * input. See the documentation on JFormattedTextField for the various ways it
 * reacts to invalid input.
 *
 * Sample usage:
 *
 * //Text field to input one byte
 * JFormattedTextField timeField = new JFormattedTextField(new TimeFormatter());
 *
 * @author David Hunter dhunter@nrao.edu
 * @version $Id$
 * @since ALMA 7.1.0
 */
public class TimeFormatter extends DefaultFormatter {

    /**
     * Create and instance of this class with defaults.
     */
    public TimeFormatter(Logger newLogger) {
        super();
        logger=newLogger;
    }

    /**
     * Convert a string to an integer.
     *
     * Ensure the integer is in range for this instance.
     *
     * @param text
     *            the string to convert.
     * @return the integer value of the string.
     * @throws ParseException
     *            if the string does not contain a valid integer.
     */
    @Override
    public Object stringToValue(final String text) throws ParseException {
        int result=0;
        try {
            result = (int) Time.valueOf(text).getTime();
        } catch (Exception eeek) {
            //This will happen any time the user enters invalid input, so we don't normally log it.
//            logger.info("Given input text was not accepted.");
            throw new ParseException("Not a valid time input", 0);
        }
        if (result > 89999000 || result < 1) {
            //This will happen any time the user enters an out of range input, so we don't normally log it.
//          logger.info("Given input text was not accepted.");
            throw new ParseException("Value out of bounds", 0);
        }
        return result;
    }

    /**
     * Convert an integer to a String.
     *
     * Ensure the integer is in range for this instance.
     *
     * @param value
     *            the integer to convert to a String
     * @return a String representing the integer value.
     * @throws ParseException
     *             if value is not an integer or if the value is out of range.
     */
    @Override
    public String valueToString(final Object value) throws ParseException {
        if (value == null){
            //TODO: Should this be logged? Or is null a input common?
//            logger.severe("Time formatter.java   Error: given value is null");
            throw new ParseException("Error: input is null", 0);
        }

        int checkValue = 0;
        try{
            checkValue =(Integer) value;
        }
        catch (Exception eeek){
            //This could be a common happening, given how formatters work. So we don't normally log it.
//            logger.info("TimeFormatter.java: Value can not be cast to Integer");
            throw new ParseException("Not a Integer", 0);
        }
        if (checkValue > 89999000 || checkValue < 1) {
            //This could be a common happening, given how formatters work. So we don't normally log it.
          logger.info("TimeFormatter.java: Value can not be cast to Integer");
            throw new ParseException("Value out of bounds", 0);
        }
        Time theTime = new Time(checkValue);
        String returnTime = theTime.toString();
        //time.toString will insert 00 when we want 24, so we manually fix that here.
        //TODO: Is there a cleaner way to do this?
        if (checkValue>86399999){
            char[] tmp = returnTime.toCharArray();
            tmp[0]='2';
            tmp[1]='4';
            returnTime = new String(tmp);
        }
        return (returnTime);
    }

    private Logger logger;
    //TODO: Correct handling of <code>serialVersionUID</code>.
    private static final long serialVersionUID = 1L;
}
