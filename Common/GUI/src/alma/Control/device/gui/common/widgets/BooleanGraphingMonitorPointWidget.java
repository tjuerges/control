/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;
import alma.Control.device.gui.common.widgets.util.AbstractGraphingMonitorPointWidget;

import info.monitorenter.gui.chart.rangepolicies.RangePolicyFixedViewport;
import info.monitorenter.util.Range;

import java.util.Date;
import java.util.logging.Logger;


/**
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since ALMA 7.1.0
 * 
 * This monitor point graphs the data coming from the monitor point that it is attached to.
 * 
 *        TODO: Document
 *        TODO: Add indication of out of range data.
 */
public class BooleanGraphingMonitorPointWidget extends AbstractGraphingMonitorPointWidget {

    
    public BooleanGraphingMonitorPointWidget(Logger logger,
                                             MonitorPointPresentationModel<Boolean> monitorPointModel) {
        super(logger, 180000);
        super.monitorPoint = addMonitorPointPM(monitorPointModel, Boolean.valueOf(false));

        //Automatically set this monitor point to fast polling so that we get a better graph.
        //TODO: come up with a better way to handle the polling rate on monitor points.
        if (!monitorPointModel.fastPolling())
            monitorPointModel.setFastPolling(true);
        
        //Properly set up the labels for the graph, and determine if 0 or 1 should be up.
        //This needs done before buildWidget is called, which should set the labels.
        //We do not save the position because if a 2nd graph is made it needs to know which way 0 is.
        String upperYLabel=monitorPoint.getUpperGraphYLabel();
        String lowerYLabel=monitorPoint.getLowerGraphYLabel();
        if (lowerYLabel.equals("GOOD") || lowerYLabel == "COMM_CONNECTED"){
            String tmp = upperYLabel;
            upperYLabel=lowerYLabel;
            lowerYLabel=tmp;
            zeroIsUp=true;
        }
        yLabel="<-- "+lowerYLabel+"   "+upperYLabel+" -->";
        buildWidget();
        chart.getAxisY().setRangePolicy(new RangePolicyFixedViewport(new Range(-0.1, 1.1)));
        chart.getAxisY().setPaintScale(false);
    }

    @Override
    protected void updateValue(IMonitorPoint source) {
        Date timeStamp = new Date(0);
        /*The lastValue business is for making the graph look like a square wave.
         *The graph is high if zero is not up and the MP is true, or if
         *zero is up and the MP is false.
         *The first point business is to prevent the appearance of a value change when
         *the graph first starts up.
         */
        if (((Boolean) getCachedValue(super.monitorPoint, timeStamp)) != zeroIsUp)
        {
            if (!firstPoint)
                super.data.addPoint(timeStamp.getTime(), lastValue);
            else
                firstPoint=false;
            super.data.addPoint(timeStamp.getTime(), 1);
            lastValue=1;
        }else
        {
            if (!firstPoint)
                super.data.addPoint(timeStamp.getTime(), lastValue);
            else
                firstPoint=false;
            super.data.addPoint(timeStamp.getTime(), 0);
            lastValue=0;
        }
        fireStateChanged();
        revalidate();
    }
    private int lastValue=0;
    private boolean zeroIsUp=false;
    private boolean firstPoint = true;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
}

//
// O_o
