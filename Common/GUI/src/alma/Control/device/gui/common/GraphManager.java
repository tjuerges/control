package alma.Control.device.gui.common;
/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JScrollPane;

import alma.Control.device.gui.common.widgets.util.GraphingPanel;

/**
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since ALMA 7.1.0
 * 
 *     The graph manager is intended to manage all the graphs for a given instance of a given
 * device. It maintains a list of the all the graph windows, sends them updates as needed, and
 * Creates new ones as needed. Old windows should be closed by the user, and inform the manager
 * that they closed.
 * 
 * Note: The GraphManager should be able to handle graphs from several devices. Should a later decision
 * be made to have it do so, it is simply a matter of having them all use the same graph manager.
 */

public class GraphManager {
    GraphManager(Logger newLogger, DevicePresentationModel aDevicePM){
        super();
        logger=newLogger;
        graphPanels = new ArrayList<GraphingPanel>(10);
        listenerList = new ArrayList<ActionListener>(100);
        devicePM = aDevicePM;
    }

    public void addGraphToPanel(final MonitorPointPresentationModel<?> monitorPPM, int graphPanelNumber){
        graphPanels.get(graphPanelNumber).addGraph(logger, monitorPPM);}
    
    /**
     * Widget popup menus can register with the graph manager to hear when the list of
     * graph windows changes.
     * @param newListener       The listener to notify.
     */
    public void addActionListener(ActionListener newListener) {
        listenerList.add(newListener);
    }

    public void createNewPanel(final MonitorPointPresentationModel<?> monitorPPM){
        JFrame newWindow = new JFrame();
        String newWindowTitle = "Plot "+nextNumber.toString()
                                + ", "+monitorPPM.getContainingDevicePM().getDeviceDescription();
        GraphingPanel graphPanel = new GraphingPanel(logger, newWindow, newWindowTitle, devicePM);
        newWindow.setTitle(newWindowTitle);
        newWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        newWindow.setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        graphPanels.add(graphPanel);
        graphPanel.addGraph(logger, monitorPPM);
        newWindow.add(new JScrollPane(graphPanel));
        newWindow.addWindowListener(new WindowCloseAction(graphPanel));
        nextNumber++;
        newWindow.setVisible(true);
        fireStateChanged();
    }
    
    /**
     * Insert comment here
     */
    protected void fireStateChanged() {
        for (int c=0; c<listenerList.size(); c++)
            listenerList.get(c).actionPerformed(new ActionEvent(this, 1, "UpdateGraphMenuEvent"));
    }   
    
    /**
     * This function is used to get the title of a given graph window. The windows are numbered
     * from 0 to getNumberOfWindows(), and each should have a different title. These numbers may or may
     * not match the plot number in the window title.
     * @param panelNumber The graph window to get the number of.
     * @return The title of requested graph window.
     */
    public String getGraphPanelTitle(int panelNumber) {return graphPanels.get(panelNumber).getTitle();}
    
    /**
     * Find out how many graph windows are open; used to request the name of each window.
     * @return The number of open graph windows.
     */
    public int getNumberOfWindows() {return graphPanels.size();}
    
    /**
     * This listens for window closes and removes that window from the list of windows.
     */
    private class WindowCloseAction extends WindowAdapter {
        //We need to remember the graphing panel so that we can remove it from the list on close.
        //This was easier than attempting to somehow get the panel from the event.
        WindowCloseAction(GraphingPanel J){myGraphingPanel=J;}
        
        @Override
        public void windowClosed(WindowEvent e) {
            int removalIndex = graphPanels.indexOf(myGraphingPanel);
            //Only remove it if the window is found.
            //This was put in because of some ArrayIndexOutOfBoundsExceptions caused when graphs were
            //open and parent device was closed. It appeared that the graphs ended up getting removed
            //twice, but the source of the issue was not found. This solves the problem, but may or may not
            //be the best solution.
            //TODO: Is there a better way to handle this issue?
            if (removalIndex>=0){
                graphPanels.remove(removalIndex);
                myGraphingPanel.destroy();
                fireStateChanged();
            }
        }
        private final GraphingPanel myGraphingPanel;
    }
    private final int DEFAULT_HEIGHT=500;
    private final int DEFAULT_WIDTH=1000;
    private DevicePresentationModel devicePM;
    private ArrayList<GraphingPanel> graphPanels;
    private ArrayList<ActionListener> listenerList;
    private Logger logger;
    private Integer nextNumber = 0;
}
