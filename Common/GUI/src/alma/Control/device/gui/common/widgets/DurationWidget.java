/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import java.text.NumberFormat;

import javax.swing.JFormattedTextField;
import javax.swing.SwingConstants;

/**
 * A text widget that allows users to enter and edit time durations.  
 * 
 * Durations are returned by the <code>getDuration()</code> method.
 * 
 * For event driven applications, clients must implement <code>PropertyChangeListener</code> and listen for
 * <code>PropertyChangeEvent</code>s with the property name <code>value</code>.
 * 
 * @see DurationWidgetTest for an example of a client.
 * 
 * @author Scott Rankin  srankin@nrao.edu
 * @since  ALMA 7.0.0
 * @version $Id$
 */
public class DurationWidget extends JFormattedTextField {

    /**
     * TODO: Correct handling of <code>serialVersionUID</code>.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Create a widget that allows users to enter a time duration.
     *  
     * @param d An initial value for the widget.
     * @param w Field width in characters.
     */
    public DurationWidget(long d, int w) {
        super(NumberFormat.getNumberInstance());
        setValue(new Long(d));
        setColumns(w);
        setHorizontalAlignment(SwingConstants.TRAILING);
    }
    
    /**
     * Get the duration stored in the widget.
     * 
     * @return duration.
     */
    public long getDuration() {
        return Long.parseLong(getValue().toString());
    }
}

//
// O_o
