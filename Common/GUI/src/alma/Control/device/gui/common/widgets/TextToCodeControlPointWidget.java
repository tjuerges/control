/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.ControlPointPresentationModel;

import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;

/**
 * <code>TextToCodeControlPointWidget.java</code> presents the user with a list of choices in the form of
 * a drop down menu. The choices are proved as an array of strings when the widget is built. When an
 * option is selected and the control button pressed, the widget then selects the corresponding code from an
 * array of ints, which is also given at construction time. That code is then sent to the attached control
 * point.
 * 
 * @author  David Hunter  dhunter@nrao.edu
 * @author  Scott Rankin  srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 5.0.4
 * 
 * TODO: Document
 * TODO: Factor out common behavior with other widgets to a superclass.
 */

public class TextToCodeControlPointWidget extends JPanel implements ActionListener {
    
    private static final long serialVersionUID = 1L;
    private Logger logger;
    private ControlPointPresentationModel<Integer> controlPointModel;
    private int[] optionCodes;
    private String[] optionList;
    private JComboBox optionMenu;
    private JButton controlButton;
    
    /**Construct a new TextToCodeControlPointWidget. This widget should be used with control points that
     * perform several functions, of which should be presented to the user as an option in a drop down
     * menu.
     * 
     * @param logger the logger that all logs should be directed to.
     * @param controlPointModel The control point that this widget should be attached to.
     * @param buttonText The text that should go on the control button.
     * @param newOptions An array of strings representing the options to present to the user.
     * @param newCodes The corresponding codes for newOptions, with the code for newOption[x]
     *                 saved in newCodes[x]. Must be the same length as newOptions.
     */
    public TextToCodeControlPointWidget( Logger logger,
            ControlPointPresentationModel<Integer> controlPointModel,
            String buttonText, String[] newOptions, int[] newCodes) {
        setLayout(new GridBagLayout());
        this.logger = logger;
        if (newOptions.length != newCodes.length)
            logger.severe("Error: the number of codes given does not equal the number of options given");
        this.controlPointModel = controlPointModel;
        controlButton=new JButton(buttonText);
        optionList=newOptions;
        optionCodes=newCodes;
        optionMenu = new JComboBox();
        optionMenu.setEditable(false);
        for (String s: newOptions) optionMenu.addItem(s);
        controlButton.addActionListener(this);
        controlPointModel.getContainingDevicePM().addControl(controlButton);
        controlPointModel.getContainingDevicePM().addControl(optionMenu);
        buildWidget(buttonText);
    }
    
    /** When an actionEvent occurs (e.g the control button is pressed) check the value in the
     *  menu. Find the corresponding code and send it to the control point.
     */
    //TODO: Check if there is a better way to look up values. Maybe a hash?
    public void actionPerformed(ActionEvent event) {
        String newValue=(String)optionMenu.getSelectedItem();
        int count=0;
        try{
            while (newValue != optionList[count])
                count++;
            controlPointModel.setValue(optionCodes[count]);
        }
        catch (ArrayIndexOutOfBoundsException Eeeek){
            logger.severe("ERROR: Text given by user not found in optionList!");
        }
    }
    
    /** Just add the option menu and control button to this panel.
     * @param buttonText The text to put on the control button.
     */
    private void buildWidget(String buttonText) {
        add(optionMenu);
        add(controlButton);
    }
}
