/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.ControlPointPresentationModel;
import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.JComboBox;

/**
 * <code>BooleanMonitorControlPointWidget</code> offers a <code>JComboBox</code>
 * to both display a monitor point and control and control point.
 * 
 * @author Scott Rankin srankin@nrao.edu
 * @author Steve Harrington sharring@nrao.edu
 * @author Justin Kenworthy jkenwort@nrao.edu
 * @version $Id: BooleanMonitorControlPointWidget.java,v 1.19 2009/07/31
 *          18:46:03 srankin Exp $
 * @since ALMA 5.0.4
 * 
 *        TODO: Document 
 *        TODO: Factor out common behavior with other widgets to
 *        a superclass.
 */

public class BooleanMonitorControlPointWidget extends MonitorPointWidget
implements ActionListener {

    private static final long serialVersionUID = 1L;
    private boolean commandIssued;
    private ControlPointPresentationModel<Boolean> controlPointModel;
    private String falseItem;
    private String trueItem;
    private JComboBox widgetComboBox;
    private IMonitorPoint monitorPoint;

    /**
     * <code>BooleanMonitorControlPointWidget</code> is the constructor for this
     * class
     * 
     * @param logger
     *            logger to use for logging
     * @param monitorPointModel
     *            monitor point to display and listen for
     * @param controlPointModel
     *            control point to modify upon combo box selection
     * @param falseItem
     *            character string to be displayed as the "false" selection
     * @param trueItem
     *            character string to be displayed as the "true" selection
     */
    public BooleanMonitorControlPointWidget(Logger logger,
            MonitorPointPresentationModel<Boolean> monitorPointModel,
            ControlPointPresentationModel<Boolean> controlPointModel,
            String falseItem, String trueItem) {

        super(logger, true);
        this.logger = logger;
        this.controlPointModel = controlPointModel;
        this.falseItem = falseItem;
        this.trueItem = trueItem;

        monitorPoint = addMonitorPointPM(monitorPointModel, Boolean.FALSE);
        commandIssued = false;

        buildWidget();
        this.controlPointModel.getContainingDevicePM().addControl(widgetComboBox);
    }

    /**
     * <code>actionPerformed</code> implements the <code>ActionListener</code>
     * interface. It listens for a selection in the widget's JComboBox and sets
     * the value of the control point to be true or false depending on the
     * selection.
     */
    public void actionPerformed(ActionEvent e) {
        final JComboBox cb = (JComboBox) e.getSource();
        final String s = (String) cb.getSelectedItem();

        if (s.equals(falseItem)) {
            controlPointModel.setValue(Boolean.FALSE);
        } else if (s.equals(trueItem)) {
            controlPointModel.setValue(Boolean.TRUE);
        }

        commandIssued = true;
        widgetComboBox.setEnabled(false);
    }

    /**
     * buildWidget is a private method which is used to construct the entire
     * widget to be displayed on a panel
     */
    private void buildWidget() {
        widgetComboBox = new JComboBox();
        widgetComboBox.addItem(falseItem);
        widgetComboBox.addItem(trueItem);
        widgetComboBox.setSelectedItem(falseItem);
        widgetComboBox.addActionListener(this);
        add(widgetComboBox);

        addPopupMenuToComponent(widgetComboBox);
    }

    @Override
    protected void updateAttention(Boolean value) {
        // TODO: remove
    }

    /**
     * <code>updateValue</code> alters the value of this widget to be either
     * true or false.
     * 
     * @param source
     *            the source of the data for the widget to display.
     */
    @Override
    public void updateValue(IMonitorPoint source) {
        if ((Boolean) getCachedValue(monitorPoint)) {
            widgetComboBox.setSelectedItem(trueItem);
        } else {
            widgetComboBox.setSelectedItem(falseItem);
        }

        if (commandIssued) {
            if (controlPointModel.getContainingDevicePM().getControlsEnabled()) {
                widgetComboBox.setEnabled(true);
            }
            commandIssued = false;
        }
    }
}
