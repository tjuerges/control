/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;
import alma.Control.device.gui.common.widgets.util.AbstractGraphingMonitorPointWidget;

import info.monitorenter.gui.chart.labelformatters.LabelFormatterNumber;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.logging.Logger;


/**
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since ALMA 7.1.0
 * 
 * This monitor point graphs the data coming from the monitor point that it is attached to.
 * 
 *        TODO: Document
 *        TODO: Add indication of out of range data.
 */
public class FloatGraphingMonitorPointWidget extends AbstractGraphingMonitorPointWidget {
    /*
     * The scale factor is often used to convert the value to a different SI prefix. For example,
     * displaying watts as nanowatts. Likewise, the offset is often used to display in �C and when the
     * internal units are in �K. Note that the offset is added after the scale factor is applied.
     */
    public FloatGraphingMonitorPointWidget(Logger logger,
                                           MonitorPointPresentationModel<Float> monitorPointModel){
        super(logger);
        super.monitorPoint = addMonitorPointPM(monitorPointModel, Float.valueOf(0.0F));
        
        //Automatically set this monitor point to fast polling so that we get a better graph.
        //TODO: come up with a better way to handle the polling rate on monitor points.
        monitorPointModel.setFastPolling(true);
        buildWidget();
        chart.getAxisY().setFormatter(new LabelFormatterNumber(new DecimalFormat("0.00")));
    }

    @Override
    protected void updateValue(IMonitorPoint source) {
        Date theTime= new Date(0);
        float newValue=(Float) getCachedValue(super.monitorPoint, theTime);
        super.data.addPoint(theTime.getTime(), newValue*scaleFactor+offsetFactor);
        fireStateChanged();
    }
    
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
}

//
// O_o
