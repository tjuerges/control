/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.panels;

import alma.Control.device.gui.common.ControlPointPresentationModel;
import alma.Control.device.gui.common.DevicePresentationModel;
import alma.Control.device.gui.common.MonitorPointPresentationModel;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.IntegerMonitorPointWidget;
import alma.Control.device.gui.common.widgets.FloatMonitorPointWidget;
import alma.Control.device.gui.common.widgets.AmbsiSnMonitorPointWidget;
import alma.Control.device.gui.common.widgets.LongMonitorPointWidget;
import alma.Control.device.gui.common.widgets.VersionMonitorPointWidget;
import alma.Control.device.gui.common.widgets.BooleanControlPointWidget;
import alma.Control.device.gui.common.widgets.CodeToTextMonitorPointWidget;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized to keep related data together and separate unrelated data.
 * 
 * This class contains the Detail data.
 * 
 * Tabbed panels are used to group related data together and separate unrelated data. These are created as
 * required for each GUI.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class AmbsiPanel extends DevicePanel {

    /**
     * Construct a new Ambsi panel. This panel displays all of the information associated with the
     * Ambsi unit, and provides a reset button.
     * 
     * @param logger                    The logger to log errors to.
     * @param aDevicePresentationModel  The presentation model for the device this ambsi is in
     * @param ambsiSerialNumberMP
     * @param ambsiTempMP
     * @param canTransactionNumberMP
     * @param canErrorCodeMP
     * @param canErrorCountMP
     * @param canLastErrorMP
     * @param majorRevLevelMP
     * @param minorRevLevelMP
     * @param patchLevelMP
     * @param protocolMajorRevLevelMP
     * @param protocolMinorRevLevelMP
     * @param protocolPatchLevelMP
     * @param resetAmbsiCP
     * 
     * Usage: create an ambsi panel, give the constructor all the proper points for your ambsi, and add the
     * panel where you want it to go. Creation of the panel should look something like this:
     * 
//      ambsiPanel = new AmbsiPanel(logger, aDevicePM,
//                                  devicePM.getMonitorPointPM(IcdMonitorPoints.SERIAL_NUMBER),
//                                  devicePM.getMonitorPointPM(IdlMonitorPoints.GET_AMBIENT_TEMPERATURE),
//                                  devicePM.getMonitorPointPM(IcdMonitorPoints.TRANS_NUM),
//                                  devicePM.getMonitorPointPM(IcdMonitorPoints.INTERNAL_SLAVE_ERROR_CODE),
//                                  devicePM.getMonitorPointPM(IcdMonitorPoints.CAN_ERROR_COUNT),
//                                  devicePM.getMonitorPointPM(IcdMonitorPoints.ERROR_CODE_LAST_CAN_ERROR),
//                                  devicePM.getMonitorPointPM(IcdMonitorPoints.MAJOR_REV_LEVEL),
//                                  devicePM.getMonitorPointPM(IcdMonitorPoints.MINOR_REV_LEVEL),
//                                  devicePM.getMonitorPointPM(IcdMonitorPoints.PATCH_LEVEL),
//                                  devicePM.getMonitorPointPM(IcdMonitorPoints.PROTOCOL_MAJOR_REV_LEVEL),
//                                  devicePM.getMonitorPointPM(IcdMonitorPoints.PROTOCOL_MINOR_REV_LEVEL),
//                                  devicePM.getMonitorPointPM(IcdMonitorPoints.PROTOCOL_PATCH_LEVEL),
//                                  devicePM.getControlPointPM(IcdControlPoints.RESET_AMBSI));
     */
    public AmbsiPanel(Logger logger, DevicePresentationModel aDevicePresentationModel,
                      MonitorPointPresentationModel<int[]> ambsiSerialNumberMP,
                      MonitorPointPresentationModel<Float> ambsiTempMP,
                      MonitorPointPresentationModel<Long> canTransactionNumberMP,
                      MonitorPointPresentationModel<Integer> canErrorCodeMP,
                      MonitorPointPresentationModel<Integer> canErrorCountMP,
                      MonitorPointPresentationModel<Integer> canLastErrorMP,
                      MonitorPointPresentationModel<Integer> majorRevLevelMP,
                      MonitorPointPresentationModel<Integer> minorRevLevelMP,
                      MonitorPointPresentationModel<Integer> patchLevelMP,
                      MonitorPointPresentationModel<Integer> protocolMajorRevLevelMP,
                      MonitorPointPresentationModel<Integer> protocolMinorRevLevelMP,
                      MonitorPointPresentationModel<Integer> protocolPatchLevelMP,
                      ControlPointPresentationModel<Boolean> resetAmbsiCP
                      ) {
        super(logger, aDevicePresentationModel);
        ambsiTemp = ambsiTempMP;
        canTransNum = canTransactionNumberMP;
        canErrorCount = canErrorCountMP;
        canErrorCode = canErrorCodeMP;
        canLastError = canLastErrorMP;
        majorRevLevel = majorRevLevelMP;
        minorRevLevel = minorRevLevelMP;
        patchLevel = patchLevelMP;
        protocolMajorRevLevel = protocolMajorRevLevelMP;
        protocolMinorRevLevel = protocolMinorRevLevelMP;
        protocolPatchLevel = protocolPatchLevelMP;
        resetAmbsi = resetAmbsiCP;
        serialNumber = ambsiSerialNumberMP;
        buildSubPanels(logger, aDevicePresentationModel);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePresentationModel aDevicePresentationModel) {
        ambsiCanPanel = new AmbsiCanPanel(logger, aDevicePresentationModel);
        ambsiInfoPanel = new AmbsiInfoPanel(logger, aDevicePresentationModel);
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Ambsi Information"),
                BorderFactory.createEmptyBorder(1, 1, 1, 1)));

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;
        add(ambsiInfoPanel, c);
        c.gridy++;
        add(ambsiCanPanel, c);
        c.gridy++;
        c.fill = GridBagConstraints.NONE;
        c.weighty=1;
        c.weightx=1;
        add(new BooleanControlPointWidget(logger, resetAmbsi, "Reset Ambsi", true), c);
        /*
         * Note: There is also a Reset Device as an optional generic control point. If it is used, it will
         * be placed in a location suitable for a device reset control. Chances are that that will not be
         * the Ambsi panel, so there is not a template for that point here.
         */

        setVisible(true);
    }

    /*
     * The Ambsi Information Panel. This private sub-class provides information on the Ambsi itself.
     * S/N,temperature, etc. It does not provide information on the status of the Can bus.
     */
    private class AmbsiInfoPanel extends DevicePanel {

        public AmbsiInfoPanel(Logger logger, DevicePresentationModel aDevicePresentationModel) {
            super(logger, aDevicePresentationModel);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createEtchedBorder(1));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            // c.fill = GridBagConstraints.HORIZONTAL;

            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 1;
            c.gridy = 0;
            c.anchor=GridBagConstraints.WEST;
            // The first group of points is info about the Ambsi itself
            addLeftEastLabel("Serial Number:", c);
            add(new AmbsiSnMonitorPointWidget(logger, serialNumber), c);

            c.gridy++;
            addLeftEastLabel("Software Revision:", c);
            add(new VersionMonitorPointWidget(logger, majorRevLevel, minorRevLevel, patchLevel), c);

            c.gridy++;
            addLeftEastLabel("Protocol Revision:", c);
            add(new VersionMonitorPointWidget(logger, protocolMajorRevLevel, protocolMinorRevLevel,
                                              protocolPatchLevel), c);
            c.gridy++;
            addLeftEastLabel("Temperature:", c);
            add(new FloatMonitorPointWidget(logger, ambsiTemp, true, 1), c);
        }

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }

    /*
     * The Can Info Panel This panel provides information on the current status of the can bus as seen by
     * the Ambsi unit.
     */
    private class AmbsiCanPanel extends DevicePanel {

        public AmbsiCanPanel(Logger logger, DevicePresentationModel aDevicePresentationModel) {
            super(logger, aDevicePresentationModel);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createEtchedBorder(1));

            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();

            c.gridx = 1;
            c.gridy = 0;
            // The second group is info about the Can bus
            addLeftEastLabel("Can Transaction #", c);
            add(new LongMonitorPointWidget(logger, canTransNum), c);

            c.gridy++;
            addLeftEastLabel("Can Error Count:", c);
            add(new IntegerMonitorPointWidget(logger, canErrorCount), c);

            c.gridy++;
            // Sub-point of Mandatory Generic Monitor Point
            addLeftEastLabel("Error Code:", c);
            String[] internalErrorList={"No errors", "Duplicate slave address detected", "No DS1820 device found",
                               "No serial number read", "CRC error on a 1-Wire bus transaction"};
            add(new CodeToTextMonitorPointWidget(logger, canErrorCode, internalErrorList,
                                                 "Unknown error code!!!"), c);

            c.gridy++;
            // Sub-point of Mandatory Generic Monitor Point
            //We still need to break out the last 5 bits into separate boolean points.
            addLeftEastLabel("Last Error:", c);
            String[] lastErrorCodes={"No errors", "Stuff error", "Form error", "Ack error", "Bit 1 error",
                                     "Bit 0 error", "CRC error", "Undefined error"};
            add(new CodeToTextMonitorPointWidget(logger, canLastError, lastErrorCodes,
                                                 "Unknown error code!!!"), c);
        }

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }
    
    private AmbsiCanPanel ambsiCanPanel;
    private AmbsiInfoPanel ambsiInfoPanel;
    private MonitorPointPresentationModel<Float> ambsiTemp;
    private MonitorPointPresentationModel<Long> canTransNum;
    private MonitorPointPresentationModel<Integer> canErrorCount;
    private MonitorPointPresentationModel<Integer> canErrorCode;
    private MonitorPointPresentationModel<Integer> canLastError;
    private MonitorPointPresentationModel<Integer> majorRevLevel;
    private MonitorPointPresentationModel<Integer> minorRevLevel;
    private MonitorPointPresentationModel<Integer> patchLevel;
    private MonitorPointPresentationModel<Integer> protocolMajorRevLevel;
    private MonitorPointPresentationModel<Integer> protocolMinorRevLevel;
    private MonitorPointPresentationModel<Integer> protocolPatchLevel;
    private ControlPointPresentationModel<Boolean> resetAmbsi;
    private MonitorPointPresentationModel<int[]> serialNumber;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
}

//
// O_o

