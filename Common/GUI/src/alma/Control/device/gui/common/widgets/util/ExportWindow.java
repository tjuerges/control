/*
ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2009 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*/
package alma.Control.device.gui.common.widgets.util;

import java.awt.AWTException;
import java.awt.Component;
import java.awt.GraphicsDevice;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.io.File;

/** 
 * @author  dhunter  dhunter@nrao.edu
 * @version $Id$
 * @since   7.1.0
 */

/* A private inner class to handle the print button.
 * Uses the snapShot function in JChart2D and the Java
 * image IO stuff to save the image.
 * 
 * Note that this will save the file locally. In cases of remote access,
 * some users may prefer to use the screen shot function on their computer
 * to generate a local snapshot.
 * 
 * The required component should be the java component to center the window on. The
 * JFileChooser requires this.
 */
public class ExportWindow implements ActionListener{
    ExportWindow (Logger newLogger, Component windowToExport){
        logger=newLogger;
        fileChooser = new JFileChooser();
        if (windowToExport == null)
            logger.severe("Error in ExportWindow.java: windowToExport is null!");
        //The next 3 lines attempt to restrict the user to jpg files.
        FileNameExtensionFilter filter = new FileNameExtensionFilter("png", "PNG");
        fileChooser.addChoosableFileFilter(filter);
        fileChooser.setAcceptAllFileFilterUsed(false);
        c=windowToExport;     //Needed to give the file chooser.
    }
    public void actionPerformed(ActionEvent e){
        boolean doIt=false;
        BufferedImage screencapture = null;
        GraphicsDevice d = c.getGraphicsConfiguration().getDevice();
        try {
            screencapture = new Robot(d).createScreenCapture(
                    new Rectangle(c.getLocationOnScreen(), c.getSize()) );
        } catch (HeadlessException e1) {
            logger.severe("ExportWindow, actionPerformed; " +
            "Error: HeadlessException when attempting to create screen capture robot.");
            return;
        } catch (AWTException e1) {
            logger.severe("ExportWindow, actionPerformed; " +
            "Error: AWTException when attempting to create screen capture robot.");
            return;
        }
        int result = fileChooser.showSaveDialog(c);
        if (result==JFileChooser.APPROVE_OPTION){
            if (fileChooser.getSelectedFile().exists()){
                String message=fileChooser.getSelectedFile()+" exists. Do you want to overwrite it?";
                int selection = JOptionPane.showConfirmDialog(c, message, "Confirm Overwrite",
                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (selection == JOptionPane.OK_OPTION)
                    doIt=true;
                else
                    doIt=false;
            }
            else
                doIt=true;
            if (doIt){
                File outFile = fileChooser.getSelectedFile();
                try {
                    ImageIO.write(screencapture, "png", outFile);
                } catch (IOException e1) {
                    logger.severe("ExportWindow, actionPerformed; " +
                    "Error: IOException when attempting to save screen capture.");
                    return;
                }
            }
        }
        else if (result==JFileChooser.ERROR_OPTION)
            logger.severe("Error: unknown JFileChooser error.");
    }
    private Component c;
    private Logger logger;
    private JFileChooser fileChooser;
}
