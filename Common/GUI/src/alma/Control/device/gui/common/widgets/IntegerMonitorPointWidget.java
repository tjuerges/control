/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;

import java.util.logging.Logger;

import javax.swing.JLabel;

/**
 * @author Scott Rankin srankin@nrao.edu
 * @author David Hunter dhunter@nrao.edu
 * @version $Id$
 * @since ALMA 5.0.4
 * 
 *        IntegerMonitorPointWidget offers a JPanel containing a textual
 *        representation of an integer (in hex or dec format) from a monitor
 *        point with range and communication verification/alarms.
 * 
 *        TODO: Document 
 *        TODO: Factor out common behavior with BooleanMonitorPointWidget to a superclass. 
 *        TODO: Add indication of out of range data.
 */
public class IntegerMonitorPointWidget extends MonitorPointWidget {

    //Default to hex display, no units
    /** Construct a new IntegerMonitorPointWidget
     * This constructor defaults to a decimal display with no units.
     * 
     * @param logger The logger that all logs should be written to
     * @param monitorPointModel The monitor point to display.
     */
    public IntegerMonitorPointWidget(Logger logger,
            MonitorPointPresentationModel<Integer> monitorPointModel) {
        this(logger, monitorPointModel, false, false);
    }

    //Default to decimal display
    /**Construct a new IntegerMonitorPointWidget
     * This constructor defaults to a decimal display
     * 
     * @param logger The logger that all logs should be written to
     * @param monitorPointModel The monitor point to display.
     * @param includeUnits True to include the units as given by the monitor point
     */
    public IntegerMonitorPointWidget(Logger logger,
            MonitorPointPresentationModel<Integer> monitorPointModel,
            boolean includeUnits) {
        this(logger, monitorPointModel, includeUnits, false);
    }
    
    /**Construct a new IntegerMonitorPointWidget
     * @param logger The logger that all logs should be written to.
     * @param monitorPointModel The monitor point to display.
     * @param includeUnits True to include the units as given by the monitor point
     * @param displayInHex True to display the value in hexadecimal format.
     */
    public IntegerMonitorPointWidget(Logger logger,
            MonitorPointPresentationModel<Integer> monitorPointModel,
            boolean includeUnits, boolean displayInHex) {
        super(logger, true, true);
        monitorPoint = addMonitorPointPM(monitorPointModel, Integer.valueOf(0));
        toolTipText = getOperatingRangeToolTipText(monitorPointModel);
        this.isHex=displayInHex;
        this.includeUnits = includeUnits;

        buildWidget();
    }

    protected void buildWidget() {
        widgetLabel = new JLabel();
        widgetLabel.setText("0");
        if (monitorPoint.supportsRangeChecking())
            widgetLabel.setForeground(StatusColor.FAILURE.getColor());
        else
            widgetLabel.setForeground(StatusColor.NORMAL.getColor());
        widgetLabel.setToolTipText(toolTipText);
        add(widgetLabel);
        addPopupMenuToComponent(widgetLabel);
    }

    @Override
    protected void updateAttention(Boolean q) {
        if (q.booleanValue()) {
            widgetLabel.setForeground(StatusColor.FAILURE.getColor());
        } else {
            widgetLabel.setForeground(StatusColor.NORMAL.getColor());
        }
    }

    @Override
    protected void updateValue(IMonitorPoint source) {
        String newValue="";
        if (isHex)
            newValue=String.format("%X", (Integer)getCachedValue(monitorPoint));
        else
            newValue=String.format("%d", (Integer)getCachedValue(monitorPoint));
        if (includeUnits && source.supportsUnits()) {
            newValue += " " + source.getUnits();
        }
        widgetLabel.setText(newValue);
    }

    private boolean includeUnits;
    private boolean isHex;
    private IMonitorPoint monitorPoint;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private String toolTipText;
    private JLabel widgetLabel;
}

//
// O_o
