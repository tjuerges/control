/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common;

import alma.Control.HardwareDeviceOperations;

import java.util.logging.Logger;

/**
 * Common behavior and properties for all Control Point and Monitor Point Presentation Models.
 * 
 * @author  Scott Rankin  srankin@nrao.edu   
 * @version $Id$
 * @since   ALMA 5.1.1
 */
public abstract class PresentationModel<T> {
	
    /**
     * All PresentationModels need to know:
     * 
     * @param aLogger the logger to log to.
     * @param containerDPM the DevicePresentationModel containing this presentation model.
     */
	public PresentationModel(Logger aLogger, DevicePresentationModel aDevicePM) {
		this.logger = aLogger;
        this.containingDevicePM = aDevicePM;
	}
    
	/**
	 * Get the device presentation model that contains this presentation model. 
	 * 
	 * @return the containing device presentation model.
	 */
    public DevicePresentationModel getContainingDevicePM() {
    	return containingDevicePM;
    }
    
	/**
     * Get the current value held by this presentation model.
     * 
     * @return the current value held by the model.
     */
    public T getValue() { 
    	return value; 
    }
    
    /**
     * Store a reference to the Hardware Device that this presentation model works 
     * with.  This is not known when the presentation model is constructed, so it 
     * must be set later.
     * 
     * @param deviceComponent
     */
    public void setDeviceComponent(HardwareDeviceOperations deviceComponent) {
        this.deviceComponent = deviceComponent;
        setUpdateMethod();
    }
    
    /**
     * At runtime, find the method to call on a Hardware Device to read data 
     * from a monitor point or write data to a control point. 
     */
    protected abstract void setUpdateMethod();
    
    protected DevicePresentationModel containingDevicePM;    
    protected HardwareDeviceOperations deviceComponent;
    protected Logger logger;

    protected T value;
}

//
//O_o
