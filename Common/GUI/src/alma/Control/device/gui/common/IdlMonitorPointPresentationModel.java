/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common;

import alma.acs.logging.AcsLogger;
import alma.acs.logging.RepeatGuardLogger;
import alma.common.log.ExcLog;

import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.omg.CORBA.LongHolder;

/**
 * Common behavior and properties for all Monitor Point Presentation Models that must be hand coded.
 * 
 * @author  Scott Rankin  srankin@nrao.edu   
 * @version $Id$
 * @since   ALMA 5.1.1
 */
public abstract class IdlMonitorPointPresentationModel<T> extends MonitorPointPresentationModel<T> {
    
    public IdlMonitorPointPresentationModel(Logger logger, DevicePresentationModel container,
                                            IMonitorPoint monitorPoint)
    {
        super(logger, container, monitorPoint);
        createCommonEvents();
        AcsLogger acsLogger = AcsLogger.fromJdkLogger(logger, null);
        exceptionRepeatGuard = RepeatGuardLogger.createCounterBasedRepeatGuardLogger(acsLogger, 1);
    }

    @SuppressWarnings("unchecked") // PresentationModelChangeEvent can not be parameterized at this point. 
	@Override
	public void checkDeviceForUpdates() {
        T newValue;
        Date timeStamp = new Date(0);
        ArrayList<PresentationModelChangeEvent> events = 
            new ArrayList<PresentationModelChangeEvent>();
        
        if (null != deviceComponent) {
            try {
                newValue = getValueFromDevice(timeStamp);
                if (null == newValue) {
                    logger.warning("IdlMonitorPointPresentationModel.checkDeviceForUpdates()" + 
                                   "lost communication with device");
                } else {
                    if (value != newValue) {
                        value = newValue;
                        events.add(newChangeEvent(PresentationModelChangeEvents.VALUE_CHANGE,
                                                  value, timeStamp));
                        if (monitorPoint.supportsRangeChecking()) {
                            if (value instanceof java.lang.Number) {
                                if (((Number)value).doubleValue() < monitorPoint.getRangeLowerBound() || 
                                        ((Number)value).doubleValue() > monitorPoint.getRangeUpperBound()) {
                                    events.add(rangeAttention);
                                } else {
                                    events.add(rangeOK);
                                }
                            }
                            if (value instanceof java.lang.Boolean) {
                                if ((Boolean)value != monitorPoint.getExpectedBooleanValue()) 
                                    events.add(rangeAttention);
                                else
                                    events.add(rangeOK);
                            }
                        }
                    } else {
                        // No change found, nothing to do.
                    }
                }
            } catch (Exception e) {
                exceptionRepeatGuard.log(Level.FINE,
                                    "IdlMonitorPointPresentationModel.checkDeviceForUpdates() - Exception:" +
                                    ExcLog.details(e));
            }
        }
        notifyListeners(events);
    }
    
    /**
     * Get the current value from the hardware device property this presentation model represents.
     * 
     * @return the current value held by the hardware device.
     */
    protected abstract T getValueFromDevice();
    
    /**
     * Get the current value from the hardware device property this presentation model represents.
     * 
     * @param timeStamp A long holder which will be set to the time stamp associated with the value.
     * @return the current value held by the hardware device.
     */
    protected abstract T getValueFromDevice(Date timeStamp);
    
    /**
     * Update methods for IDL monitor point presentation models are known at compile time. 
     */
    @Override
    protected void setUpdateMethod() {
        // Intentionally empty.
    }

    private RepeatGuardLogger exceptionRepeatGuard;
}

//
// O_o
