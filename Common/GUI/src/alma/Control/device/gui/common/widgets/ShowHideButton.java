/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2011
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

/**
 * ShowHideButton that toggles target's visibility.
 * 
 * @param <T>
 *            The target whose visibility is to be toggled.
 * 
 * @version $Id$
 * @author Kohji Nakamura k.nakamura@nao.ac.jp
 * @since ALMA 8.1.0
 */
public class ShowHideButton<T extends Component> extends JButton implements
		ActionListener {
	/**
	 * Show/hide button's constructor.
	 * 
	 * @param target
	 *            The target whose visibility will be toggled.
	 * @param showLabel
	 *            label displayed when this button makes the target visible.
	 * @param hideLabel
	 *            label displayed when this button makes the target invisible.
	 */
	public ShowHideButton(T target, final String showLabel,
			final String hideLabel) {
		this(target, new String[] { showLabel, hideLabel });
	}

	/**
	 * Show/hide button's constructor.
	 * 
	 * @param target
	 *            The target whose visibility will be toggled.
	 * @param labels
	 *            labels should be {\"show label\", \"hide label\"} form.
	 */
	public ShowHideButton(T target, final String labels[]) {
		super();
		if (null == target) {
			throw new IllegalArgumentException("target should not be null.");
		}
		if (labels == null || labels.length != 2 || labels[0] == null
				|| labels[1] == null) {
			throw new IllegalArgumentException(
					"labels should be {\"show label\", \"hide label\"} form.");
		}
		this.target = target;
		this.labels = labels;
		addActionListener(this);
		updateAppearance();
	}

	/**
	 * Returns target Component whose visibility is to be changed.
	 * 
	 * @return Returns target Component whose visibility is to be changed.
	 */
	public T getTarget() {
		return target;
	}

	/**
	 * Update appearance of this widget based on target's visibility.
	 */
	public void updateAppearance() {
		int index = target.isVisible() ? 1 : 0;
		setText(labels[index]);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		target.setVisible(!target.isVisible());
		updateAppearance();
	}

	final T target;
	final String labels[];
	// TODO - serial version UID
	private static final long serialVersionUID = 1L;
}