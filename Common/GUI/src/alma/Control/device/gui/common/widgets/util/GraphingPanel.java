/*
ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2009 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*/
package alma.Control.device.gui.common.widgets.util;

import info.monitorenter.gui.chart.IRangePolicy;
import info.monitorenter.gui.chart.axis.AAxis;
import info.monitorenter.util.Range;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import java.util.ArrayList;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;

import alma.Control.device.gui.common.DevicePresentationModel;
import alma.Control.device.gui.common.MonitorPointPresentationModel;
import alma.Control.device.gui.common.widgets.BooleanGraphingMonitorPointWidget;
import alma.Control.device.gui.common.widgets.FloatGraphingMonitorPointWidget;
import alma.Control.device.gui.common.widgets.DoubleGraphingMonitorPointWidget;
import alma.Control.device.gui.common.widgets.IntegerGraphingMonitorPointWidget;
import alma.Control.device.gui.common.widgets.util.ExportWindow;

import alma.acs.gui.standards.StandardIcons;

/** 
 * @author  dhunter  dhunter@nrao.edu
 * @version $Id$
 * @since   7.1.0
 */

/**
 *     This is a panel that displays graphs of N monitor points. The intent is to allow the user to
 * view graphs of several monitor points in the same window with the same time (X) scale. Graphs are
 * added by an outside request, which should come from the graph manager. Graphs can be removed by
 * the user. The window will close if all the graphs are removed.
 *     At this time, this panel is only setup to be used by the GraphManager, which is driven by user
 * requests from the WidgetPopupMenu. However, it should be possible to insert a GraphingPanel directly
 * into a device GUI, and manually manage it. This has not been tested as there has not yet been a
 * requirement that we do so. Some minor updates to GraphingPanel might or might not be needed if we decide
 * to do this.
 */

public class GraphingPanel extends JLayeredPane {
    /**
     * Construct a new GraphingPanel
     * @param logger  The logger that we would like to send logs to.
     * @param newTitle  The title of the window. This is not actually set by GraphingPanel; it is set by
     * the GraphManager. It is saved here because it needs to be saved somewhere, and this seems like the
     * best place.
     */
    public GraphingPanel (Logger newLogger, JFrame newContainingWindow, String newTitle,
                          DevicePresentationModel aDevicePM){
        super();
        devicePM = aDevicePM;
        
        //Register this graph with the devicePM to get closed on shutdown.
        shuttingDownAction = new ShuttingDownAction();
        devicePM.addShutdownAction(shuttingDownAction);
        
        if (newContainingWindow == null)
            logger.severe("Error in GraphingPanel.java: newContainingWindow is null");
        containingWindow=newContainingWindow;
        logger=newLogger;
        title=newTitle;
        graphList = new ArrayList<AbstractGraphingMonitorPointWidget>();
        removeButtons = new ArrayList<JButton>();
        setLayout(new GridBagLayout());
        //c is the constraints for adding graphs.
        c = new GridBagConstraints();
        c.gridx=0;
        c.gridy=0;
        c.weightx=100;
        c.weighty=100;
        c.gridwidth=100;
        c.fill=GridBagConstraints.BOTH;
        
        //b is the constraints for adding buttons.
        b = new GridBagConstraints();
        b.gridx=99;
        b.gridy=0;
        b.weightx=0;
        b.weighty=0;
        b.anchor = GridBagConstraints.NORTHEAST;
        b.insets = new Insets(8, 0, 0, 3);
        
        pauseButton = new JButton("Pause");
        pauseButton.addActionListener(new PauseButtonAction());
        exportButton = new JButton("Export Graphs");
        exportButton.addActionListener(new ExportWindow(logger, containingWindow));
        
        TimeFormatter T = new TimeFormatter(logger);
        T.setOverwriteMode(false);
        widthInput = new JFormattedTextField (T);
        widthInput.setPreferredSize(new Dimension(70, 20));
        widthInput.setValue(new Integer(300000));
        
        widthInputButton = new JButton("Reset Width");
        widthInputButton.addActionListener(new ZoomOutAction());
        leftInterfaceBar = new JPanel();
        rightInterfaceBar = new JPanel();
        drawInterfaceBar();
        
        graphWidth = (Integer)widthInput.getValue();
        setVisible(true);
    }
    
    /**Add a graph of the given monitor point to this widget.
     * @param newGraph The monitor point to add a graph of.
     * 
     * Not all monitor point types are supported. Providing an unsupported type will result
     * in an error being logged and no graph. The currently supported types are Float, Integer, and Boolean.
     */
    public void addGraph(Logger logger, final MonitorPointPresentationModel<?> monitorPPM){
        //newGraph is set here to make eclipse shutup.
        RemoveButtonAction removeButtonAct=new RemoveButtonAction(logger);
        AbstractGraphingMonitorPointWidget newGraph = null;
        JButton newRemoveButton = new JButton(StandardIcons.ACTION_DELETE.icon);
        newRemoveButton.setPreferredSize(new Dimension(StandardIcons.ACTION_DELETE.icon.getIconWidth(),
                                                       StandardIcons.ACTION_DELETE.icon.getIconHeight()));
        
        //Try to select the proper type of graph widget for the monitor point type.
        //If it is not found all we can do is log the error.
        if (monitorPPM.getMonitorPoint().getTypeClass()==float.class){
            newGraph = new FloatGraphingMonitorPointWidget(logger,
                                 (MonitorPointPresentationModel<Float>) monitorPPM);
            add(newGraph, c);
        }
        //TODO: long.class was added in here due to the Ambsi Trans # monitor point. Is this really safe?
        //Note: making a separate LongGraphingMonitorPointWidget did not help things. In fact, this might
        //be a problem with the type being provided for the monitor point.
        //For now, Longs are not graphable, and there is only one (on the DRX DFR Status panel). So this is
        //a minor concern.
        else if (monitorPPM.getMonitorPoint().getTypeClass() == int.class ||
                monitorPPM.getMonitorPoint().getTypeClass() == long.class){
           newGraph = new IntegerGraphingMonitorPointWidget(logger,
                                 (MonitorPointPresentationModel<Integer>) monitorPPM);
           add(newGraph, c);
        }
        
        else if (monitorPPM.getMonitorPoint().getTypeClass() == double.class){
           newGraph = new DoubleGraphingMonitorPointWidget(logger,
                                 (MonitorPointPresentationModel<Double>) monitorPPM);
           add(newGraph, c);
        }
        else if (monitorPPM.getMonitorPoint().getTypeClass() == boolean.class){
            newGraph = new BooleanGraphingMonitorPointWidget(logger,
                                 (MonitorPointPresentationModel<Boolean>) monitorPPM);
            c.weighty=50;       //Boolean graphs aren't that interesting so we give them less space.
            add(newGraph, c);
            c.weighty=100;
        }
        else
            //This probably means that a programmer has goofed up somewhere and provided GraphingPanel with
            //an unsupported type.
            logger.severe("GraphingPanel, addGraph;" +
                          "Error: monitor point type not supported by graphing system!" );
        
        if (newGraph != null){
            //Remove the interface bar - the graph is going to go here
            remove(leftInterfaceBar);
            remove(rightInterfaceBar);
            setLayer(newGraph, new Integer(0));
            newRemoveButton.addActionListener(removeButtonAct);
            removeButtonAct.setGB(newGraph, newRemoveButton);
            removeButtons.add(newRemoveButton);
            add(newRemoveButton, b);
            setLayer(newRemoveButton, new Integer(100));
            c.gridy++;
            b.gridy++;
            newGraph.chart.getAxisX().addPropertyChangeListener(AAxis.PROPERTY_RANGEPOLICY, new AxisWatcher());
            graphList.add(newGraph);
            //If this is the first graph we need to update the bounds of all graphs when it is updated.
            if (graphList.size()==1)
                graphList.get(0).addActionListener(new BoundsResetAction());
            //Put the interface bar at the end of the window.
            GridBagConstraints i = new GridBagConstraints();
            i.gridy=c.gridy+1;
            add(leftInterfaceBar, i);
            i.gridx=99;
            i.anchor = GridBagConstraints.EAST;
            add(rightInterfaceBar, i);
            revalidate();
        }
    }
    
    /**
     * Clean up all of the graphs.
     * This simply calls destroy on each graphing widget, which in turn calls destroy on the contained Chart2D
     * This is necessary because of how Chart2D works.
     * CAUTION: Do not call until after the containing window is closed and this panel is no
     * longer being updated.
     */
    public void destroy(){
        for (AbstractGraphingMonitorPointWidget g : graphList)
            g.destroy();
    }
    
    /**
     * @return  The title of this GraphPanel, as set by the constructor.
     */
    public String getTitle(){return title;}
    
    /**
     * Rebuild and redraw the panel.
     * This method should be called after a graph is removed from graphList.
     */
    private void redraw(){
        //TODO: Is the use of the dispose method OK with whatever we will be doing  in the OMC?
        if (graphList.size()==0 && containingWindow != null)
        {
            //Remove the shutdown action as this window is now closing
            devicePM.removeShutdownAction(shuttingDownAction);
            //Close the window becasue there are no more graphs
            containingWindow.dispose();
        }
        this.removeAll();
        //These constraints are used to place each graph.
        c.gridx=0;
        c.gridy=0;
        //These constraints are used to overlay the clost buttons on each graph
        b.gridx=99;
        b.gridy=0;
        for (int count=0; count<graphList.size(); count++){
            //Maintain the smaller size of boolean graphs
            if ((graphList.get(count)) instanceof BooleanGraphingMonitorPointWidget){
                c.weighty=50;
                add(graphList.get(count), c);
                c.weighty=100;
            } else
                add(graphList.get(count), c);
            add(removeButtons.get(count), b);
            b.gridy++;
            c.gridy++;
        }
        //Put the interface bar at the end of the window.
        GridBagConstraints i = new GridBagConstraints();
        i.gridy=c.gridy+1;
        add(leftInterfaceBar, i);
        i.anchor = GridBagConstraints.EAST;
        i.gridx=99;
        add(rightInterfaceBar, i);
        revalidate();
    }
    
    
    /**
     * This function is used to draw the user interface bar at the bottom of the window.
     * Should only be used internally.
     */
    private void drawInterfaceBar(){
        leftInterfaceBar.add(pauseButton);
        leftInterfaceBar.add(exportButton);
        rightInterfaceBar.add(new JLabel("Graph Width (hh:mm:ss)"));
        rightInterfaceBar.add(widthInput);
        rightInterfaceBar.add(widthInputButton);
    }
    
    private void zoomAll(long newMin, long newMax, boolean zoomY){
        for (int count=0; count<graphList.size(); count++){
            graphList.get(count).setXRange(newMin, newMax);
            if (zoomY)
                graphList.get(count).setAutoYRange();
        }
    }
    
    private void zoomOut(){
        //Get the current time to use as the upper bound
        long currentTime = System.currentTimeMillis();
        //Then we set all the bounds to (time, time-graphWidth)
        zoomAll(currentTime-graphWidth, currentTime, true);
        //We know we are not zoomed at this point.
        //TODO: A lot of AxisWatchers will be thrown by the resetting.
        //      Is there any chance of their setting zoomed=true after we unset it?
        zoomed=false;
    }
    
    private class AxisWatcher implements PropertyChangeListener {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            IRangePolicy a = (IRangePolicy) evt.getNewValue();
            Range r = a.getRange();
            double actualRange = r.getMax()-r.getMin();
            if (graphWidth != actualRange && !AxisWatchLock){
                //AxisWatchLock is used to prevent an infinite recursive stack overflow.
                //This would otherwise happen because zoomAll will trigger lots of AxisWatchers
                AxisWatchLock = true;
                zoomed=true;
                long min = (long) r.getMin();
                long max = (long) r.getMax();
                zoomAll(min, max, false);
                revalidate();
                AxisWatchLock = false;
            }
        }
    }
    

    
    /** This action is used when the first graph is updated.
     *  It calls zoomOut() to update all the other graphs, unless
     *  we are zoomed in, in which case we do nothing 
     * @author dhunter
     */
    private class BoundsResetAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (!zoomed)
                zoomOut();
        }
    }
    
    private class ShuttingDownAction implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            containingWindow.dispose();
        }
    }
    
    private class PauseButtonAction implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (pauseButton.getText().equals("Resume"))
            {
                for (AbstractGraphingMonitorPointWidget g : graphList)
                    g.pause(false);
                pauseButton.setText("Pause");
            }
            else
            {
                for (AbstractGraphingMonitorPointWidget g : graphList)
                    g.pause(true);
                pauseButton.setText("Resume");
            }
        }
    }
    
    /**
     * An ActionListener for removing graphs, to be attached to the graph's remove button.
     * Simply removes the given graph and cleans up afterwords.
     * @author dhunter
     */
    private class RemoveButtonAction implements ActionListener{
        RemoveButtonAction(Logger l){
            logger=l;
        }
        public void setGB(AbstractGraphingMonitorPointWidget newG, JButton newB){
            g=newG;
            b=newB;
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            if (g != null){
                if (graphList.get(0)==g)       //If we are removing the first graph we need to move the graph
                    if (graphList.size()>1)    //update bounds listener to the new first graph.
                        graphList.get(1).addActionListener(new BoundsResetAction());
                graphList.remove(g);
                removeButtons.remove(b);
                g.destroy();
                redraw();
            }
            else
                logger.severe("GraphingPanel, RemoveButtonAction, actionPerformed:" +
                              "Error: graph reference never set!");
        }
        private JButton b;
        private AbstractGraphingMonitorPointWidget g;
        private Logger logger;
    }
    
    /**
     * This action updates the bounds of all the graphs.
     * Lisitens for changes in the bounds via the update button
     * and then changes all the bounds on the graphs.
     * TODO: make only change bounds on zoom or bounds change
     * TODO: make play nice with Zoomable Graph stuff
     * @author dhunter
     *
     */
    private class ZoomOutAction implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            //Read the new width from the user
            graphWidth = (Integer) widthInput.getValue();
            //Then zoom out
            zoomOut();
        }
    }
    
    private boolean AxisWatchLock = false;
    private GridBagConstraints b;
    private GridBagConstraints c;
    private JFrame containingWindow = null;
    private DevicePresentationModel devicePM;
    private JButton exportButton;
    private ArrayList<AbstractGraphingMonitorPointWidget> graphList;
    private long graphWidth;
    private JPanel leftInterfaceBar;
    private JButton pauseButton;
    private ArrayList<JButton> removeButtons;
    private JPanel rightInterfaceBar;
    private ShuttingDownAction shuttingDownAction;
    private final String title;
    private JButton widthInputButton;
    private JFormattedTextField widthInput;
    private Logger logger;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private boolean zoomed = false;
}
