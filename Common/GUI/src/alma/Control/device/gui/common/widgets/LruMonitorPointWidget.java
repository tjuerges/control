/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;

import java.util.logging.Logger;

import javax.swing.JLabel;

/**
 * @author  David Hunter  dhunter@nrao.edu
 * @author  Scott Rankin  srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 7.2.0
 *
 * This monitor point widget is used to display LRU CINs that are not easily displayed
 * by integer or byte sequence monitor point widgets. 
 * 
 * At this time this monitor widget only has a constructor to match the data format used by
 * a number of Back End modules, such as the DGCK. Other constructors for different formats
 * should be added if appropriate.
 *
 *        TODO: Factor out common behavior with BooleanMonitorPointWidget to a superclass.
 */
public class LruMonitorPointWidget extends MonitorPointWidget {

    public LruMonitorPointWidget(Logger logger,
            MonitorPointPresentationModel<Integer> digitOneModel,
            MonitorPointPresentationModel<Integer> digitTwoModel,
            MonitorPointPresentationModel<Integer> digitFourModel,
            MonitorPointPresentationModel<Integer> digitSixModel) {
        super(logger, true);

        digitOne = addMonitorPointPM(digitOneModel, Integer.valueOf(0));
        digitTwo = addMonitorPointPM(digitTwoModel, Integer.valueOf(0));
        digitFour = addMonitorPointPM(digitFourModel, Integer.valueOf(0));
        digitSix = addMonitorPointPM(digitSixModel, Integer.valueOf(0));

        buildWidget();
    }

    protected void buildWidget() {
        widgetLabel = new JLabel();
        widgetLabel.setForeground(StatusColor.NORMAL.getColor());
        widgetLabel.setText("00.00.00.00)");
        add(widgetLabel);
        addPopupMenuToComponent(widgetLabel);
    }

    @Override
    protected void updateAttention(Boolean q) {
        if (q.booleanValue()) {
            widgetLabel.setForeground(StatusColor.FAILURE.getColor());
        } else {
            widgetLabel.setForeground(StatusColor.NORMAL.getColor());
        }
    }

    @Override
    protected void updateValue(IMonitorPoint source) {
        Integer digitOneValue = getCachedValue(digitOne);
        Integer digitTwoValue = getCachedValue(digitTwo);
        Integer digitFourValue= getCachedValue(digitFour);
        Integer digitSixValue = getCachedValue(digitSix);

        String s = String.format("%1X%1X.0%1X.0%1X.00", digitOneValue, digitTwoValue,
                                                        digitFourValue, digitSixValue);

        widgetLabel.setText(s);
    }

    private IMonitorPoint digitFour = null;
    private IMonitorPoint digitOne = null;
    private IMonitorPoint digitSix = null;
    private IMonitorPoint digitTwo = null;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private JLabel widgetLabel;
}

//
// O_o
