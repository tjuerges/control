package alma.Control.gui.hardwaredevice.common;

import java.util.logging.Logger;

import alma.common.gui.chessboard.ChessboardDetailsPlugin;
import alma.common.gui.chessboard.ChessboardDetailsPluginListener;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

/**
 * Abstract base class for all device details plugins within control.
 * @author sharring
 *
 */
public abstract class HardwareDeviceChessboardDetailsPlugin extends ChessboardDetailsPlugin {
	
	protected HardwareDevicePresentationModel presentationModel;
	protected Logger logger = null;
	protected final static String STANDALONE_LOGGER_NAME = "HardwareDeviceChessboardDetailsPluginLogger";

	/**
	 * Constructor
	 * @param antennaNames the names of the antennas that were selected
	 * @param owningChessboard the chessboard which 'owns' this plugin 
	 * (in other words, the chessboard which launched this details plugin).
	 * @param services the plugin container services.
	 * @param pluginTitle the unique title of the plugin (to be used in the title bar of the plugin window on screen) and
	 *        by the docking framework of EXEC to give the plugin a name which must be unique.
	 * @param selectionTitle the title of the cell that was selected in the chessboard when this details plugin was launched;
	 *        this may not be unique if, for example, multiple instances of a details display have been shown for a single 
	 *        cell in the chessboard; say, cell S1 might have selectionTitle value of 'S1', pluginTitle values of:
	 *        'S1 - 1', 'S1 - 2', 'S1 - 3', etc.
	 * @param model the presentation model being used to interact with the control subsystem for the details plugin in question.
	 */
	public HardwareDeviceChessboardDetailsPlugin(String antennaNames[], ChessboardDetailsPluginListener owningChessboard,
			PluginContainerServices services, String pluginTitle, String selectionTitle, HardwareDevicePresentationModel model)
	{
		super(antennaNames, owningChessboard, services, pluginTitle, selectionTitle);
		initializeLogger();
		this.presentationModel = model;
		this.presentationModel.setServices(services);
	}
	
	@Override
	public void specializedStop() {
		if(null != presentationModel) {
			presentationModel.stop();
		}
	}

   @Override 
   public void specializedStart() {
      // NOOP - child classes may override, as needed
   }
	
	/**
	 * Required method of the SubsystemPlugin interface
	 */
	public void setServices(PluginContainerServices containerServices) {
		super.setServices(containerServices);
		initializeLogger();			
	}
	
	private void initializeLogger()
	{
		// initialize the logger which will be used for all of our logging message.
		// if we don't have a pluginContainerServices reference, then just
		// use the global logger - this will usually only be true if we're testing
		// e.g. running outside of the EXEC plugin infrastructure.
		logger = (pluginContainerServices == null) ? 
			Logger.getLogger(STANDALONE_LOGGER_NAME) : 
			pluginContainerServices.getLogger();
	}

}
