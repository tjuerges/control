package alma.Control.gui.hardwaredevice.common.util;

import java.awt.Dimension;

import javax.swing.JLabel;
import java.awt.Color;

/**
 * This class represents a label for indicating 'questionable' values (e.g. when we've lost 
 * communication with the backend and want to display a question mark next to our data).
 * It contains logic to keep it a fixed size in the display.
 * 
 * @author sharring
 * @author srankin
 */
public class QuestionMarkLabel extends JLabel
{
	// TODO - serial version uid
	private final static long serialVersionUID = 0;
	
	private boolean questionMarkShown = true;
	private Dimension dimension = null;
	private final static String QUESTION_MARK_TEXT = "?";
	private final static String NO_QUESTION_MARK_TEXT = " ";
	
	/**
	 * Constructor.
	 * @param questionMarkShown
	 */
	public QuestionMarkLabel(boolean questionMarkShown)
	{
		super();
		this.setText(QUESTION_MARK_TEXT);
		this.dimension = this.getPreferredSize();
		this.setQuestionMarkShown(questionMarkShown);
	}

	/**
	 * Constructor.
	 * @param questionMarkShown
	 * @param fgColor
	 * @param bgColor
	 */
	public QuestionMarkLabel(boolean questionMarkShown, Color fgColor, Color bgColor)
	{
		this(questionMarkShown);
		this.setForeground(fgColor);
		this.setBackground(bgColor);
	}
	
	/**
	 * Sets whether or not the label will display its
	 * questionable text (e.g. "?" or not).
	 * 
	 * @param questionable boolean indicating whether to display
	 * the questionable text (e.g. "?") or not.
	 */
	public void setQuestionMarkShown(boolean questionable)
	{
		this.questionMarkShown = questionable;
		if(questionMarkShown) {
			this.setText(QUESTION_MARK_TEXT);
		}
		else {
			this.setText(NO_QUESTION_MARK_TEXT);
			this.setSize(dimension);
		}
	}
}
