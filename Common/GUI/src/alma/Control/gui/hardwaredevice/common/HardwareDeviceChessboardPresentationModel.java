package alma.Control.gui.hardwaredevice.common;

import alma.Control.Common.Name;
import alma.Control.ControlDevice;
import alma.Control.ControlDeviceHelper;
import alma.Control.HardwareController;
import alma.Control.HardwareControllerHelper;
import alma.ControlExceptions.IllegalParameterErrorEx;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.common.gui.chessboard.DefaultChessboardStatus;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

/**
 * Class containing the logic to talk with the control subsystem (related
 * to hardware device chessboards), so as
 * to isolate the places in the GUI code (more specifically, in this context, 
 * the GUI code related to chessboards for hardware devices)
 * which interact with the control system.
 * 
 * @author sharring
 */
public class HardwareDeviceChessboardPresentationModel extends ControlChessboardPresentationModel
{
	private String deviceName;
	private ControlDevice hwDevice = null;
	private String deviceComponentName = null;
	private boolean deviceComponentNameHasBeenInitialized = false;
	private boolean hasDeviceInstalled = false;
	private int getComponentRetryCounter = 0;
	private final static int RETRY_THRESHOLD = 100;
	
	/**
	 * Constructor to create a chessboard presentation model for a hardware device.
	 * 
	 * @param services the plugin's container services for use in things like getComponent calls, etc.
	 * @param deviceName the name of the device for which the chessboard will present status.
	 */
	public HardwareDeviceChessboardPresentationModel(PluginContainerServices services, String deviceName) 
	{
		super(services);
		this.pluginContainerServices = services;
		this.deviceName = deviceName;
	}
	
	/**
	 * stops the presentation model
	 */
	public void stop()
	{
		super.stop();
		if(hwDevice != null && deviceComponentName != null) {
			pluginContainerServices.releaseComponent(deviceComponentName);		
			hwDevice = null;
		}
	}

	/**
	 * Gets the name of the device component, suitable for use in calls to getComponent, etc.
	 * @param pluginContainerServices the container services to use for things like getComponent calls, etc.
	 * @param antennaName the name of the antenna for which we want the device's component name.
	 * 
	 * NOTE: this method is static so that it can be used as a utility method w/o an instance, by other classes, since
	 * it appears that it is useful in several contexts.
	 * 
	 * @param deviceName the name of the device for which we wish to know if it is installed on the specified antenna.
	 * @return the name of the component for the device, for the antenna in question, or null if 
	 *         there is no such device installed.
	 */
	public static String getComponentNameForDevice(PluginContainerServices pluginContainerServices, String antennaName, String deviceName)
	{
		String retVal = null;
		
		if(null != pluginContainerServices && null != antennaName && null != deviceName)
		{
			HardwareController hwController = null;
			String fullComponentName = Name.ControlPrefix + antennaName;
			try {
				// get the corba object for the antenna
				org.omg.CORBA.Object antennaObject = pluginContainerServices.getComponentNonSticky(fullComponentName);

				// get the antenna component 'narrowed' to a HardwareController
				hwController = HardwareControllerHelper.narrow(antennaObject);
				retVal = hwController.getSubdeviceName(deviceName);
			} 
			catch(IllegalParameterErrorEx e) {
				// antenna does not have device installed
				pluginContainerServices.getLogger().
					warning("HardwareDeviceChessboardPresentationModel.getComponentNameForDevice device: " + deviceName + " not found in Antenna");
				retVal = null;
			}
			catch (AcsJContainerServicesEx e) {
				// problem encountered; log it
				pluginContainerServices.getLogger().
					warning("HardwareDeviceChessboardPresentationModel.getComponentNameForDevice problem looking up device: " 
							+ deviceName + " for antenna: " + antennaName + " - assuming device not installed");
				retVal = null;
			}
			catch (Throwable thrown) {
				// problem encountered; log it
				pluginContainerServices.getLogger().
					warning("HardwareDeviceChessboardPresentationModel.getComponentNameForDevice problem looking up device: " 
							+ deviceName + " for antenna: " + antennaName + " - assuming device not installed; throwable was: " + thrown.getStackTrace());
				retVal = null;
			}
		}
		return retVal;
	}
	
	/**
	 * Gets the status of the device.
	 * @param antennaName the name of the antenna for which we want to get the device's status.
	 * @return the status of the device for the given antenna.
	 * 
	 * 	 * TODO(?) using TMCDB (when it contains the necessary information) - differentiate between the cases of:
	 * 		1) device not present && IS NOT expected (from TMCDB) - normal 'offline' / not installed state.
	 * 		2) device not present && IS expected (from TMCDB) - erroneous condition.
	 * 
	 */
	protected DefaultChessboardStatus getStatusForCell(String antennaName)
	{
		DefaultChessboardStatus retVal = DefaultChessboardStatus.ANTENNA_NOT_INSTALLED;
 
		// we try to get the device component name either a) the first time through
		// or b) every so often if we haven't been able to get it previously
		if( (null == deviceComponentName && false == deviceComponentNameHasBeenInitialized) || 
				(null == deviceComponentName && true == deviceComponentNameHasBeenInitialized 
						&& getComponentRetryCounter > RETRY_THRESHOLD)) 
		{
			deviceComponentName = getComponentNameForDevice(pluginContainerServices, antennaName, deviceName);
			deviceComponentNameHasBeenInitialized = true;
			if(deviceComponentName != null) {
				hasDeviceInstalled = true;
			}
			getComponentRetryCounter = 0;
		} 
		else if(null == deviceComponentName) {
			getComponentRetryCounter++;
		}

		if(false == hasDeviceInstalled) {
			// antenna installed, but no device present in antenna
			retVal = DefaultChessboardStatus.DEVICE_NOT_INSTALLED;
		}
		else if(!isAntennaOnline(antennaName) && true == hasDeviceInstalled) {
			// antenna offline, but device present in antenna
			retVal = DefaultChessboardStatus.ANTENNA_OFFLINE;
		} 
		else {
			// antenna installed and device present in antenna
			if(null == hwDevice) {
				try 
				{
					// get the hw device as a generic corba object
					org.omg.CORBA.Object deviceObject = pluginContainerServices.getComponent(deviceComponentName);

					// get the device 'narrowed' to a HardwareController
					hwDevice = ControlDeviceHelper.narrow(deviceObject);
				}
				catch (AcsJContainerServicesEx e) {
					// problem encountered; log it
					pluginContainerServices.getLogger().
					    warning("HardwareDeviceChessboardPresentationModel.getStatusForDevice problem looking up device component");
					hwDevice = null;
					retVal = DefaultChessboardStatus.FATAL_ERROR;
				}
			} 
			try {
				if(null != hwDevice && hwDevice.inErrorState()) {
					retVal = DefaultChessboardStatus.SEVERE_ERROR;
				}
				else if(null != hwDevice) {
					retVal = DefaultChessboardStatus.NORMAL;
				}
			} 
			catch(Exception ex) {
				// problem encountered; log it
				pluginContainerServices.getLogger().
				    warning("HardwareDeviceChessboardPresentationModel.getStatusForDevice problem querying device for error state.");
				if(hwDevice != null && deviceComponentName != null) {
					pluginContainerServices.releaseComponent(deviceComponentName);		
					hwDevice = null;
				}
				retVal = DefaultChessboardStatus.FATAL_ERROR;
			}
		}
		return retVal;
	} 	
}
