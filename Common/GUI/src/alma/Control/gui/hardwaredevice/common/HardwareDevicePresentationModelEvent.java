/*******************************************************************************
*    ALMA - Atacama Large Millimiter Array
*    (c) Associated Universities Inc., 2002 
*    (c) European Southern Observatory, 2002
*    Copyright by ESO (in the framework of the ALMA collaboration)
*    and Cosylab 2002, All rights reserved
*
*    This library is free software; you can redistribute it and/or
*    modify it under the terms of the GNU Lesser General Public
*    License as published by the Free Software Foundation; either
*    version 2.1 of the License, or (at your option) any later version.
*
*    This library is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    Lesser General Public License for more details.
*
*    You should have received a copy of the GNU Lesser General Public
*    License along with this library; if not, write to the Free Software
*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
*/
package alma.Control.gui.hardwaredevice.common;

/**
 * @author Steve Harrington
 * @author Scott Rankin
 */
public class HardwareDevicePresentationModelEvent 
{
	private Enum enumeration;
	private Object value;
	
	/**
	 * Constructor.
	 * @param enumeration the enumeration that indicates what has changed.
	 * @param value the new value of the item that changed.
	 */
	public HardwareDevicePresentationModelEvent(Enum enumeration, Object value)
	{
		this.enumeration = enumeration;
		this.value = value;
	}

	/**
	 * Getter for the enumeration (indicating what was changed).
	 * @return the enumeration indicating what changed.
	 */
	public Enum getEnumeration() {
		return enumeration;
	}

	/**
	 * Getter for the value - indicating what the new value is.
	 * @return the new value.
	 */
	public Object getValue() {
		return value;
	}
}
