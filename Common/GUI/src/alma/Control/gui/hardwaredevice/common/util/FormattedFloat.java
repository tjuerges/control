package alma.Control.gui.hardwaredevice.common.util;

/**
 * Class which encapsulates a float value + formatting capabilities, 
 * for use in the GUI.
 * 
 * @author sharring
 */
public class FormattedFloat 
{
	private Float floatObject;
	private String format;
	private final static String DEFAULT_FORMAT_STRING = "%9.4f";
	
	/**
	 * Constructor which will use a default formatting string.
	 * @param floatObj a float object for the value.
	 */
	public FormattedFloat(Float floatObj)
	{
		this(floatObj, DEFAULT_FORMAT_STRING);
	}
	
	/**
	 * Constructor which will use a specific formatting string.
	 * @param floatObj the float value.
	 * @param format the string to use in formatting.
	 * @see java.util.Formatter
	 */
	public FormattedFloat(Float floatObj, String format) 
	{
		this.floatObject = floatObj;
		this.format = format;
	}
	
	/**
	 * GUI can call this method to get a formatted representation of the value.
	 */
	public String toString()
	{
		String formattedString = String.format(format, floatObject);
		return formattedString;
	}	
}
