/*******************************************************************************
*    ALMA - Atacama Large Millimiter Array
*    (c) Associated Universities Inc., 2002 
*    (c) European Southern Observatory, 2002
*    Copyright by ESO (in the framework of the ALMA collaboration)
*    and Cosylab 2002, All rights reserved
*
*    This library is free software; you can redistribute it and/or
*    modify it under the terms of the GNU Lesser General Public
*    License as published by the Free Software Foundation; either
*    version 2.1 of the License, or (at your option) any later version.
*
*    This library is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    Lesser General Public License for more details.
*
*    You should have received a copy of the GNU Lesser General Public
*    License along with this library; if not, write to the Free Software
*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
*/
package alma.Control.gui.hardwaredevice.common;

import javax.swing.JPanel;

/**
 * This is an abstract base class for all UI-related panels. 
 * It defines methods and infrastructure which will be used
 * to allow a proper MVC style architecture for control GUI's.
 * 
 * TODO: refactor to use Scott's version of this class.
 * 
 * @author Steve Harrington
 * @author Scott Rankin
 *
 */
public abstract class HardwareDeviceAmbsiPanel extends JPanel implements HardwareDevicePresentationModelListener
{
	protected boolean hasCommunication = false;
	
	/**
	 * Default constructor.
	 * @param presentationModel the presentation model which will be used to communicate to the
	 * control/backend (and which we will register as a listener in order to receive updates).
	 */
	public HardwareDeviceAmbsiPanel(HardwareDevicePresentationModel presentationModel)
	{
		presentationModel.addPresentationModelListener(this);
	}
	
	public final void processChange(HardwareDevicePresentationModelEvent evt) 
	{
		// TODO - switch for base class enum values
		// TODO: Scott to flesh this out
		specializedProcessChange(evt);
	}
	
	public final void communicationLost()
	{
		hasCommunication = false;
		specializedCommunicationLost();
	}
	
	public final void communicationEstablished()
	{
		hasCommunication = true;
		specializedCommunicationEstablished();
	}
	
	/**
	 * Method which is called by <code>communicationLost</code> in order to have specialized handling
	 * of a <code>HardwareDevicePresentationModel</code> losing communication with the backend. Typically,
	 * this will involve something like displaying question marks (?) next to numerical values in the GUI,
	 * for example.
	 */
	protected abstract void specializedCommunicationLost();
	
	/**
	 * Method which is called by <code>communicationEstablished</code> in order to have specialized handling
	 * of a <code>HardwareDevicePresentationModel</code> regaining communication with the backend. Typically,
	 * this will involve something like removing question marks (?) next to numerical values in the GUI,
	 * for example.
	 */
	protected abstract void specializedCommunicationEstablished();
	
	/**
	 * Method which is called by <code>processChange</code> in order to have specialized handling
	 * (if necessary) of a <code>HardwareDevicePresentationModel</code> change within subclasses. It is 
	 * abstract and protected;it must be implemented by subclasses.
	 * 
	 * @param evt the event containing information for the listener, 
	 * indicating what changed and the new value
	 */
	protected abstract void specializedProcessChange(HardwareDevicePresentationModelEvent evt);
}
