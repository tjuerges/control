package alma.Control.gui.hardwaredevice.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;

import alma.acs.logging.AcsLogLevel;

import alma.Control.ArrayMonitor;
import alma.Control.ArrayMonitorHelper;
import alma.Control.ControlMaster;
import alma.Control.ControlMasterHelper;
import alma.Control.InaccessibleException;
import alma.Control.Common.Name;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.TMCDB.Access;
import alma.TMCDB.AccessHelper;
import alma.TmcdbErrType.TmcdbErrorEx;
import alma.TMCDB_IDL.StartupAntennaIDL;
import alma.common.gui.chessboard.ChessboardEntry;
import alma.common.gui.chessboard.ChessboardStatus;
import alma.common.gui.chessboard.ChessboardStatusAdapter;
import alma.common.gui.chessboard.ChessboardStatusEvent;
import alma.common.gui.chessboard.ChessboardStatusProvider;
import alma.common.gui.chessboard.ChessboardUtilities;
import alma.common.gui.chessboard.DefaultChessboardStatus;
import alma.common.log.ExcLog;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

/**
 * Base class for the presentation model for a chessboard; subclasses must implement the abstract 
 * method <code>getStatusForCell</code>. A presentation model class is used to isolate things that 
 * talk to CONTROL (rather than having things in many classes all talking to CONTROL).
 * 
 * @author sharring
 */
public abstract class ControlChessboardPresentationModel extends ChessboardStatusAdapter
{
	/**
	 * The interval at which to poll the state for cells in the chessboard.
	 */
	public static final int POLLING_THREAD_INTERVAL_MILLISECS = 5000;
	protected final static int PREFERRED_ROWS = 7;
	protected final static int PREFERRED_COLUMNS = 10;
	protected static final String UNINSTALLED_TOTAL_POWER_ANTENNA_PREFIX = "ZZ";
	protected static final String UNINSTALLED_SEVEN_METER_ANTENNA_PREFIX = "YY";
	protected static final String UNINSTALLED_TWELVE_METER_ANTENNA_PREFIX = "XX";
	protected static final String MASTER_CURL = Name.Master;
	// TODO - does CONTROL have constants defined for this? If so, use it!
	protected static final String TMCDBCOMPONENT_IDL_STRING = "IDL:alma/TMCDB/TMCDBComponent:1.0";
	
	// TODO - does EXEC have constants defined for these? If so, use them!
        protected final static String ARRAY_NAME_SESSION_PROPERTY_KEY = "array.compname"; // We want a component name.
	protected final static int NUMBER_OF_ANTENNAS = 66;
	protected final static int NUMBER_OF_TWELVE_METER_ANTENNAS = 50;
	protected final static int NUMBER_OF_SEVEN_METER_ANTENNAS = 12;
	
	protected PluginContainerServices pluginContainerServices;
	protected String allPossibleAntennaNames[] = new String[NUMBER_OF_ANTENNAS];
	protected Map<String, Integer> installedAntennasMap;
	protected List<String> antennasInArray;
	protected String arrayName;
	protected ChessboardEntry[][] currentAntennaStates = new ChessboardEntry[PREFERRED_ROWS][PREFERRED_COLUMNS];
	protected MaintainStateThread maintainStateThread;
	protected Logger logger = null;
	protected ControlMaster controlMaster = null;
	
	/**
	 * Constructor to create a control chessboard presentation model.
	 * 
	 * @param services the plugin's container services for use in things like getComponent calls, etc.
	 */
	public ControlChessboardPresentationModel(PluginContainerServices services) 
	{
		this.pluginContainerServices = services;
		this.logger = services.getLogger();
		
		alma.exec.extension.subsystemplugin.SessionProperties sessionProperties = pluginContainerServices.getSessionProperties();
		this.arrayName = sessionProperties.getProperty(ARRAY_NAME_SESSION_PROPERTY_KEY); 
		 
		initialize();
	}

	/**
	 * @see ChessboardStatusProvider 
	 * @return an array of chessboard entry objects, indicating the current state.
	 */
	public ChessboardEntry[][] getStates()
	{
		return currentAntennaStates;
	}
	
	/**
	 * stops the presentation model, e.g. any polling threads.
	 */
	public void stop()
	{
		this.maintainStateThread.setShouldStop(true);
	}
	
	/**
	 * Protected method to initialize the presentation model.
	 */
	protected void initialize()
	{
		initAllPossibleAntennaNames();
		initAntennaNamesForStates();
		
		// create a thread which will poll for state changes
		maintainStateThread = new MaintainStateThread();
		maintainStateThread.start();
	}
	
	/**
	 * This method must be implemented by subclasses - it contains the logic to determine the state of a given
	 * cell (antenna) in the chessboard.
	 * 
	 * @param antennaId the name of the antenna for the cell.
	 * @return the status of the cell (antenna).
	 */
	protected abstract ChessboardStatus getStatusForCell(String antennaId);
	
	/**
	 * Protected method to get all of the antenna names in a particular array.
	 * @return a collection of names of the antennas in the system.
	 * @throws AcsJContainerServicesEx if an error happens while getting the ArrayMonitor component
	 */
	protected List<String> getAntennaNamesAssignedToArray() throws AcsJContainerServicesEx
	{
		List<String> retVal;
		String arrayMonitorName = pluginContainerServices.getSessionProperties().getProperty(ARRAY_NAME_SESSION_PROPERTY_KEY);
		if (arrayMonitorName==null || arrayMonitorName.isEmpty()) {
			return new ArrayList<String>();
		}
		org.omg.CORBA.Object obj= pluginContainerServices.getComponent(arrayMonitorName);
		ArrayMonitor monitor= ArrayMonitorHelper.narrow(obj);
		String[] antennasFromComponent=monitor.getAntennas();
		pluginContainerServices.releaseComponent(arrayMonitorName);
		retVal=Arrays.asList(antennasFromComponent);
		return retVal;
	}
	
	/**
	 * Protected method to get all of the antenna names known to the system (i.e. defined in the TMCDB).
	 * @return a map (mapping componentNames to their display location offset desired in the GUI) 
	 * of the antennas known to the system, as defined in the TMCDB.
	 */
	protected Map<String, Integer> getAllAntennaNamesInTMCDB()
	{
		Map<String, Integer> retVal = new HashMap<String, Integer>();
		Access tmcdb = null;
		try 
		{
		    tmcdb = AccessHelper.narrow(pluginContainerServices.getDefaultComponent("IDL:alma/TMCDB/Access:1.0"));
            try 
            {
            	StartupAntennaIDL[] antennas = tmcdb.getStartupAntennasInfo();
            	for(StartupAntennaIDL antennaIDL : antennas) 
            	{
            		int displayLocation = antennaIDL.uiDisplayOrder;
            		
            		pluginContainerServices.getLogger().finest("TMCDB's uiDisplayOrder for antenna: " 
            				+ antennaIDL.antennaName + " is: " + antennaIDL.uiDisplayOrder);
            		
            		if(displayLocation < 1 || displayLocation > NUMBER_OF_ANTENNAS) {
            			pluginContainerServices.getLogger().
            				severe("TMCDB is not configured correctly; uiDisplayOrder attribute for antenna must be >=1 and <=" 
            						+ NUMBER_OF_ANTENNAS + "; skipping antenna: " + antennaIDL.antennaName);
            		} else {
            			// subtract one because uiDisplayOrder in TMCDB starts counting at 1, not 0
            			retVal.put(antennaIDL.antennaName, displayLocation  - 1);
            		}
            	}
            }
            catch(TmcdbErrorEx err) {
            	// problem encountered; log it
				pluginContainerServices.getLogger().
					severe("ControlChessboardPresentationModel.getAllAntennaNamesInTMCDB: problem querying TMCDB component");
            }
        } 
		catch (Exception ex) {
			// problem encountered; log it
			pluginContainerServices.getLogger().
				severe("ControlChessboardPresentationModel.getAllAntennaNamesInTMCDB: problem getting TMCDB component");
        }
		finally {
			if(null != tmcdb) {
				pluginContainerServices.releaseComponent(tmcdb.name());
			}
		}
		return retVal;
	}
	
	/**
	 * Protected method to determine if an antenna is online.
	 * @param antennaName the name of the antenna.
	 * @return boolean indicating whether the antenna is online (true) or not (false).
	 */
	protected boolean isAntennaOnline(String antennaName)
	{
		boolean retVal = false;
		assert antennaName != null;
		
		if(null != pluginContainerServices)
		{
			// if we haven't already gotten the control master reference, get it now
			if(null == controlMaster) {
				try {
					controlMaster = ControlMasterHelper.narrow(pluginContainerServices.getComponentNonSticky(MASTER_CURL));
				}
				catch (AcsJContainerServicesEx e) {
					// problem encountered; log it
					pluginContainerServices.getLogger().severe("ControlChessboardPresentationModel.isAntennaOnline problem looking up control master");
					controlMaster = null;
					retVal = false;
				}
			}
			
			// using the control master reference, query for the offline antennas
			if(null != controlMaster) {
				try {
					String[] offlineAntennas = controlMaster.getOfflineAntennas();
					List<String> offlineAntennasList = Arrays.asList(offlineAntennas);
					if(!offlineAntennasList.contains(antennaName)) {
						// if the antenna is NOT in the list of 
						// offline antennas, then the antenna is online
						retVal = true;
					}
				}
				catch (InaccessibleException e) {
					// problem encountered; log it
					pluginContainerServices.getLogger().severe("ControlChessboardPresentationModel.isAntennaOnline control master inaccessible");
					retVal = false;
				}
			}
		}
		return retVal;
	}
	
	/**
	 * Protected method to tell whether an antenna is in the array or not.
	 * @param antennaName the name of the antenna.
	 * @return boolean indicating whether the antenna is in the array (true) or not (false).
	 */
	protected boolean isAntennaInArray(String antennaName)
	{
		boolean retVal = false;
		if(arrayName == null || arrayName.trim().isEmpty() || antennasInArray.contains(antennaName)) {
			retVal = true;
		}
		return retVal;
	}
	
	/**
	 * Protected method to tell whether an antenna is installed or not.
	 * @param antennaName the name of the antenna.
	 * @return boolean indicating whther the antenna is installed (i.e. in TMCDB) or not.
	 */
	protected boolean isAntennaInstalled(String antennaName)
	{
		boolean retVal = false;
		
		if(installedAntennasMap.get(antennaName) != null) {
			retVal = true;
		}
		return retVal;
	}

	private void initAllPossibleAntennaNames()
	{
		for(int i = 0; i < NUMBER_OF_ANTENNAS; i++)
		{
			if(i < NUMBER_OF_TWELVE_METER_ANTENNAS ) {
				if(i < 9) {
					// special case to make sure we use 2 digit numbers even for 1-9 (e.g. 01, 02, 03, etc.)
					allPossibleAntennaNames[i] = UNINSTALLED_TWELVE_METER_ANTENNA_PREFIX + "0" + Integer.toString(i + 1);
				} else {
					allPossibleAntennaNames[i] = UNINSTALLED_TWELVE_METER_ANTENNA_PREFIX + Integer.toString(i + 1);	
				}

			} else if(i < NUMBER_OF_TWELVE_METER_ANTENNAS + NUMBER_OF_SEVEN_METER_ANTENNAS) {
				if(i - NUMBER_OF_TWELVE_METER_ANTENNAS < 9) {
					// special case to make sure we use 2 digit numbers even for 1-9 (e.g. 01, 02, 03, etc.)
					allPossibleAntennaNames[i] = UNINSTALLED_SEVEN_METER_ANTENNA_PREFIX + "0" + 
					Integer.toString(i - NUMBER_OF_TWELVE_METER_ANTENNAS + 1);
				} else {
					allPossibleAntennaNames[i] = UNINSTALLED_SEVEN_METER_ANTENNA_PREFIX + 
					Integer.toString(i - NUMBER_OF_TWELVE_METER_ANTENNAS + 1);
				}
			} else {
				if(i - NUMBER_OF_TWELVE_METER_ANTENNAS - NUMBER_OF_SEVEN_METER_ANTENNAS < 9) {
					// special case to make sure we use 2 digit numbers even for 1-9 (e.g. 01, 02, 03, etc.)
					allPossibleAntennaNames[i] = UNINSTALLED_TOTAL_POWER_ANTENNA_PREFIX + "0" + 
					Integer.toString(i - NUMBER_OF_TWELVE_METER_ANTENNAS - NUMBER_OF_SEVEN_METER_ANTENNAS + 1);
				} else {
					allPossibleAntennaNames[i] = UNINSTALLED_TOTAL_POWER_ANTENNA_PREFIX + 
					Integer.toString(i - NUMBER_OF_TWELVE_METER_ANTENNAS - NUMBER_OF_SEVEN_METER_ANTENNAS + 1);
				}
			}
			// If we wanted blank cells for antennas not in TMCDB, it would be a 1-liner:
			// allPossibleAntennaNames[i] = null;
			
			pluginContainerServices.getLogger().finest("initialized all possible antenna names[" + i + "] to: " 
					+ allPossibleAntennaNames[i]);
		}
	}
	
	private void initAntennaNamesForStates()
	{
		for(int i = 0; i < PREFERRED_ROWS; i++) 
		{
			for(int j = 0; j < PREFERRED_COLUMNS; j++) 
			{
				if(i * PREFERRED_COLUMNS + j < NUMBER_OF_ANTENNAS) 
				{
					String name = allPossibleAntennaNames[i * PREFERRED_COLUMNS + j];
					if(null == currentAntennaStates[i][j]) 
					{
						currentAntennaStates[i][j] = new ChessboardEntry(DefaultChessboardStatus.ANTENNA_NOT_INSTALLED, name);			
					}
					else 
					{
						currentAntennaStates[i][j].setDisplayName(name);
					}
				}
				else {
					currentAntennaStates[i][j] = null;
				}
			}
		}	
	}
	
	/**
	 * Private method which interrogates the control system to determine the state of the cells
	 * for a chessboard. This will be called from a thread periodically. The thread 
	 * should not be a swing thread, because this querying could take a long time or hang, resulting
	 * in a non-responsive GUI.
	 */
	private void queryState()
	{
		ChessboardEntry[] entryArray = new ChessboardEntry[NUMBER_OF_ANTENNAS];
		ChessboardStatus status = DefaultChessboardStatus.ANTENNA_NOT_INSTALLED;
		int count = 0;
		for(String antennaId : allPossibleAntennaNames) 
		{
			status = getStatus(antennaId);
			entryArray[count++] = new ChessboardEntry(status, antennaId, null);
		}
		ArrayList<ChessboardStatusEvent> changes = new ArrayList<ChessboardStatusEvent>();
		ChessboardEntry[][] newAntennaStates = (ChessboardUtilities.convertOneDimensionalToTwoDimensional(entryArray, PREFERRED_ROWS, PREFERRED_COLUMNS));
		for(int i = 0; i < newAntennaStates.length; i ++) 
		{
			for(int j = 0; j < newAntennaStates[i].length; j++) 
			{
				if(null!= newAntennaStates[i][j] && null != currentAntennaStates[i][j] && 
				   !newAntennaStates[i][j].equals(currentAntennaStates[i][j])) 
				{
					ChessboardStatusEvent event = 
						new ChessboardStatusEvent(currentAntennaStates[i][j].getDisplayName(),
								newAntennaStates[i][j].getCurrentStatus(), 
								newAntennaStates[i][j].getToolTipText());
					changes.add(event);
					currentAntennaStates[i][j] = newAntennaStates[i][j];
				}
			}
		}
		if(!changes.isEmpty()) 
		{
			ExecuteOnSwingThread swingThread = new ExecuteOnSwingThread(changes);
			SwingUtilities.invokeLater(swingThread);
		}
	}
	
	/**
	 * Gets the status of a cell for the chessboard; this method is protected and can be overridden if needed,
	 * but those circumstances should be rare - normally this method will never need to be overridden and would
	 * normally be private and of no interest outside this class. One example of an exception which will
	 * override this method is an antenna status chessboard which can manipulate antennas across ALL arrays,
	 * so the logic for marking an antenna as not in the array will differ. Almost all other chessboards will
	 * merely use this implementation as is, and for those uses you can effectively consider this a private method
	 * the details of which you should not care about.
	 * 
	 * @param antennaName the name of the antenna for which we want to get the device's status.
	 * @return the status of the device for the given antenna.
	 */
	protected ChessboardStatus getStatus(String antennaName)
	{
		ChessboardStatus retVal = DefaultChessboardStatus.ANTENNA_NOT_INSTALLED;

		if(!isAntennaInstalled(antennaName)) {
			// antenna not installed
			retVal = DefaultChessboardStatus.ANTENNA_NOT_INSTALLED;
		} 
		else if(!isAntennaInArray(antennaName)) {
			// antenna installed but not in array
			retVal = DefaultChessboardStatus.ANTENNA_NOT_IN_ARRAY;
		}
		else {
			// otherwise, subclass will have to tell us the state
			retVal = getStatusForCell(antennaName);
		}
		return retVal;
	}

	/**
	 * Class to periodically query the state of the cells for the chessboard (and save the state to a local
	 * variable which can then be queried by the GUI). The querying for the chessboard cells' states 
	 * is run in this separate, non-swing thread in order to keep the GUI responsive. There will be a separate 
	 * GUI/Swing thread which will then run and periodically call the <code>getStates</code> method on the 
	 * presentation model, which will simply (and immediately) return its current state.
	 * 
	 * @author sharring
	 */
	private class MaintainStateThread extends Thread
	{
		private boolean shouldStop = false;
		
		/**
		 * Constructor for the thread which will maintain the state of the chessboard cells.
		 */
		public MaintainStateThread()
		{
			this.setDaemon(true);
			this.setName("ControlChessboard maintain state");
		}
		
		/**
		 * Sets a flag indicating whether the polling thread should stop/exit.
		 * @param shouldStop whether the thread should stop (true) or not (false).
		 */
		public void setShouldStop(boolean shouldStop)
		{
			this.shouldStop = shouldStop;
		}
		
		public void run()
		{
			// needed only once: intializing antenna information
			initializeAntennaStates();
			
			// needed periodically: querying for state changes
			while(false == shouldStop) 
			{
				try {
					queryState();
					try {
						Thread.sleep(ControlChessboardPresentationModel.POLLING_THREAD_INTERVAL_MILLISECS);
					}
					catch(InterruptedException ex) {
						// noop
					}
				} catch(Throwable th) {
					logger.warning("ControlChessboardPresentationModel caught an exception while querying chessboard state" + ExcLog.details(th));
				}
			}
		}
		
		private void initializeAntennaStates()
		{
			installedAntennasMap = getAllAntennaNamesInTMCDB();
			
			ArrayList<ChessboardStatusEvent> changes = new ArrayList<ChessboardStatusEvent>();
			for(String newNameFromTMCDB: installedAntennasMap.keySet())
			{
				int uiDisplayOrder = installedAntennasMap.get(newNameFromTMCDB);
				pluginContainerServices.getLogger().finest("ControlChessboardPresentationModel.initializeAntennaInfo adding name: " + newNameFromTMCDB 
					+ " at location: " + uiDisplayOrder + " from TMCDB");
				

				allPossibleAntennaNames[uiDisplayOrder] = newNameFromTMCDB;
				
				// we have changed a name from the 'default' to something from the TMCDB; we need to
				// construct a change event to notify the chessboard of the update and trigger a redraw
				// of the affected cell.
				int row = uiDisplayOrder / PREFERRED_COLUMNS;
				int column = uiDisplayOrder % PREFERRED_COLUMNS;
				
				pluginContainerServices.getLogger().finest("changing antenna name for uiDisplayOrder: " + uiDisplayOrder + " in row: " + row 
						+ " and column: " + column + " from: " + currentAntennaStates[row][column].getDisplayName() + " to: " + newNameFromTMCDB);				
				
				ChessboardStatusEvent event = 
					new ChessboardStatusEvent(currentAntennaStates[row][column].getDisplayName(),
							newNameFromTMCDB, currentAntennaStates[row][column].getCurrentStatus(), 
							null);
				
				changes.add(event);
			}
			try {
				antennasInArray = getAntennaNamesAssignedToArray();
			} catch (Throwable t) {
				logger.log(AcsLogLevel.WARNING,"Error getting antenna names", t);
				antennasInArray=new ArrayList<String>();
			}

			initAntennaNamesForStates();
			
			// if any names were changed from info in the TMCDB, notify the chessboard 
			// so that it can update the display accordingly
			if(!changes.isEmpty()) 
			{
				ExecuteOnSwingThread swingThread = new ExecuteOnSwingThread(changes);
				SwingUtilities.invokeLater(swingThread);
			}
		}
	}
	
	/**
	 * Private class to execute updates on the GUI on a swing thread.
	 * @author sharring
	 */
	private class ExecuteOnSwingThread extends Thread
	{
		private ArrayList<ChessboardStatusEvent> changes;
		
		ExecuteOnSwingThread(ArrayList<ChessboardStatusEvent> changes)
		{
			this.changes = changes;
			this.setName("ControlChessboard swing thread");
		}
		
		public void run()
		{
			if(null != changes) {
				for(ChessboardStatusEvent change: changes) {
					notifyListeners(change);	
				}
			}
		}
	}
}
