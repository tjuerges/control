package alma.Control.gui.hardwaredevice.common;

/**
 * Simple interface describing the listener for changes to a <code>HardwareDevicePresentationModel</code>.
 * 
 * @author Steve Harrington
 * @author Scott Rankin
 */
public interface HardwareDevicePresentationModelListener 
{
	/**
	 * Method which is called on the listener when something in the <code>HardwareDevicePresentationModel</code> has changed.
	 * @param evt event containing the information about the change (what changed, and the new value).
	 */
	public abstract void processChange(HardwareDevicePresentationModelEvent evt);
	
	/**
	 * Method which is called on the listener when communication with the backend is lost.
	 */
	public abstract void communicationLost();
	
	/**
	 * Method which is called on the listener when communication with the backend is established.
	 */
	public abstract void communicationEstablished();
	
}
