package alma.Control.gui.hardwaredevice.common.util;

/**
 * Class for handling double values within the hardware device gui infrastructure - 
 * this class contains logic to aid in display / formatting.
 * 
 * @author sharring
 */
public class FormattedDouble
{
	private Double doubleObject;
	private String format;
	private final static String DEFAULT_FORMAT_STRING = "%12.6f";
	
	/**
	 * Constructor which will use a default formatting string.
	 * @param doubleObj a double object for the value.
	 */
	public FormattedDouble(Double doubleObj)
	{
		this(doubleObj, DEFAULT_FORMAT_STRING);
	}

	/**
	 * Constructor which will use a specific formatting string.
	 * @param doubleObj the double value.
	 * @param format the string to use in formatting.
	 * @see java.util.Formatter
	 */
	public FormattedDouble(Double doubleObj, String format) 
	{
		this.doubleObject = doubleObj;
		this.format = format;
	}
	
	
	/**
	 * GUI can call this method to get a formatted representation of the value.
	 */
	public String toString()
	{
		String formattedString = String.format(format, doubleObject);
		return formattedString;
	}	
}
