/*******************************************************************************
*    ALMA - Atacama Large Millimiter Array
*    (c) Associated Universities Inc., 2002 
*    (c) European Southern Observatory, 2002
*    Copyright by ESO (in the framework of the ALMA collaboration)
*    and Cosylab 2002, All rights reserved
*
*    This library is free software; you can redistribute it and/or
*    modify it under the terms of the GNU Lesser General Public
*    License as published by the Free Software Foundation; either
*    version 2.1 of the License, or (at your option) any later version.
*
*    This library is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    Lesser General Public License for more details.
*
*    You should have received a copy of the GNU Lesser General Public
*    License along with this library; if not, write to the Free Software
*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
*/
package alma.Control.gui.hardwaredevice.common;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;

import alma.common.log.ExcLog;
import alma.Control.HardwareDevice;
import alma.Control.HardwareDevicePackage.HwState;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

/**
 * Base class for the presentation model (client side model of the backend data/state) 
 * for an ambsi device. All ambsi-type devices will have a user interface which should
 * contain a presentation model which derives from this base class.
 * 
 * @author Steve Harrington
 * @author Scott Rankin
 *
 */
public abstract class HardwareDevicePresentationModel
{
	protected Logger logger = null;
	protected PluginContainerServices pluginContainerServices;
	private final static int DEFAULT_POLLING_THREAD_INTERVAL_MILLISECS = 750;
	private String testProperty;
	private ArrayList<HardwareDevicePresentationModelListener> listeners;
	private final static String STANDALONE_LOGGER_NAME = "HardwareDevicePresentationModelLogger";
	private MaintainStateThread maintainStateThread;
	private int pollingThreadIntervalMilliseconds = DEFAULT_POLLING_THREAD_INTERVAL_MILLISECS;
	private boolean hasCommunications = false;
	
	/**
	 * An enum containing the properties for the ambsi-base class.
	 * 
	 * TODO: Scott to flesh this out with real values
	 * 
	 * @author Steve Harrington
	 * @author Scott Rankin
	 */
	public enum AmbsiProperties 
	{
		// Some properties common to all (ambsi) devices
		// TODO: Scott will flesh these out...
		/** 
		 * Examples...
		 */
		TESTPROPERTY, 
		
		/**
		 * Examples
		 * 
		 */
		TEST2PROPERTY, 

		/**
		 * Examples...
		 */
		TEST3PROPERTY
	}
	
	/**
	 * Default constructor.
	 *
	 */
	public HardwareDevicePresentationModel()
	{
		this.listeners = new ArrayList<HardwareDevicePresentationModelListener>();
		this.testProperty = "changeMe";
		
		// create a thread which will poll for state changes
		maintainStateThread = new MaintainStateThread();
		maintainStateThread.start();
	}
	
	/**
	 * Setter for the container services for the plugin - this is used
	 * for things like getting a logger, getting components, etc.
	 * 
	 * @param services
	 */
	public void setServices(PluginContainerServices services)
	{
		this.pluginContainerServices = services;
		
		logger = (pluginContainerServices == null) ? 
				Logger.getLogger(STANDALONE_LOGGER_NAME) : 
				pluginContainerServices.getLogger();				
	}
	
	/**
	 * Method to add a listener to the list of registered listeners which are notified
	 * upon changes to the model.
	 * 
	 * @param listener the listener to add
	 */
	public void addPresentationModelListener(HardwareDevicePresentationModelListener listener)
	{
		listeners.add(listener);
	}
	
	/**
	 * Method to cleanly shutdown the presentation model, stop all threads, etc.
	 */
	public synchronized final void stop()
	{
		Thread stopperThread = new Thread() 
		{
			public void run() 
			{
				// notify the maintain state thread that it needs to stop
				if(null != maintainStateThread) 
				{
					maintainStateThread.setShouldStop(true);
				}
				
				// perform any specialized stop/cleanup needed by subclasses
				specializedStop();
				
				// release the device component
				releaseDeviceComponent();
			}
		};
		stopperThread.start();
	}
	
	/**
	 * Used for testing...
	 * 
	 * TODO: Scott will flesh out real properties
	 * @param testProperty
	 */
	public void setTestProperty(String testProperty) 
	{
		HardwareDevicePresentationModelEvent event = new HardwareDevicePresentationModelEvent(HardwareDevicePresentationModel.AmbsiProperties.TESTPROPERTY, testProperty);
		notifyPresentationModelListeners(event);
		this.testProperty = testProperty;
	}
	
	/**
	 * Used for testing...
	 * 
	 * TODO: Scott will flesh out the actual ambsi-related properties.
	 * 
	 * @return test property
	 */
	public String getTestProperty() {
		return testProperty;
	}

	/**
	 * Getter for the polling interval for the thread that maintains/queries for state changes.
	 * @return the polling interval, in milliseconds.
	 */
	public int getPollingThreadIntervalMilliseconds()
	{
		return this.pollingThreadIntervalMilliseconds;
	}
	
	/**
	 * Setter for the polling interval for the thread that maintains/queries for state changes.
	 * @param newPollIntervalMs the new poll interval, in milliseconds.
	 */
	public void setPollingThreadIntervalMilliseconds(int newPollIntervalMs)
	{
		this.pollingThreadIntervalMilliseconds = newPollIntervalMs;
	}
	
	/**
	 * Sub-classes must implement this method to query the backend/control for any
	 * changes in state. This method will be called automatically, periodically, 
	 * and any changes that exist will then be propagated automatically to the
	 * registered listeners so that the GUI may be updated.
	 * 
	 * @return list of HardwareDevicePresentationModelEvent objects indicating changes in state; null if no changes.
	 */
	protected abstract List<HardwareDevicePresentationModelEvent> queryForStateChanges();
	
	/**
	 * Method to release the device component 
	 * for example via the releaseComponent method 
	 * of PluginContainerServices.
	 */
	protected abstract void releaseDeviceComponent();

	/**
	 * Method to get the device component 
	 * for example via the getComponent method 
	 * of PluginContainerServices.
	 */
	protected abstract HardwareDevice setDeviceComponent() throws AcsJContainerServicesEx;
	
	/**
	 * Sub-classes can, optionally, override this method for any sub-class specific stop logic.
	 *
	 */
	protected void specializedStop() 
	{ 
		// default is nothing; subclasses may override if needed
	}
	
	/**
	 * This method will be called when the model has changed, so that all listeners (views)
	 * will know about the change and can react accordingly (e.g. redrawing information on
	 * the display in response to the item which changed).
	 * 
	 * @param evt the event which contains information about the new value and the item which changed.
	 */
	protected void notifyPresentationModelListeners(HardwareDevicePresentationModelEvent evt)
	{
		for(HardwareDevicePresentationModelListener listener : listeners) 
		{
			listener.processChange(evt);
		}
	}
	
	/**
	 * Private class used to notify GUI listeners of changes to the model; this
	 * must be done on the swing event thread via SwingUtilities.invokeLater()
	 * or SwingUtilities.invokeAndWait().
	 * 
	 * @author sharring
	 */
	private class ExecuteChangesOnSwingThread extends Thread
	{
		List<HardwareDevicePresentationModelEvent> changes;
		
		ExecuteChangesOnSwingThread(List<HardwareDevicePresentationModelEvent> changes) {
			this.changes = changes;
		}
		
		public void run()
		{
			if(null != changes && !changes.isEmpty()) 
			{
				for(HardwareDevicePresentationModelEvent change: changes) {
					notifyPresentationModelListeners(change);	
				}
			}
		}
	}
	
	/**
	 * Private class used to notify GUI listeners of losing/regaining communications
	 * with the backend. This must be done on the swing event thread via SwingUtilities.invokeLater()
	 * or SwingUtilities.invokeAndWait().
	 * 
	 * @author sharring
	 */
	private class ExecuteCommunicationUpdateOnSwingThread extends Thread
	{
		boolean communicationsEstablished;
		
		ExecuteCommunicationUpdateOnSwingThread(boolean communicationsEstablished) {
			this.communicationsEstablished = communicationsEstablished;
		}
		
		public void run()
		{
			for(HardwareDevicePresentationModelListener listener: listeners) 
			{
				if(true == communicationsEstablished)
				{
					listener.communicationEstablished();
				}
				else 
				{
					listener.communicationLost();
				}
			}
		}
	}
	
	/**
	 * Class to periodically query the state of the cells for the chessboard (and save the state to a local
	 * variable which can then be queried by the GUI). The querying for the chessboard cells' states 
	 * is run in a separate, non-swing thread in order to keep the GUI responsive. There will be a separate 
	 * GUI/Swing thread which will then run and periodically call the <code>getStates</code> method on the 
	 * presentation model, which will simply (and immediately) return its current state.
	 * 
	 * @author sharring
	 */
	private class MaintainStateThread extends Thread
	{
		private boolean shouldStop = false;
		private HardwareDevice device = null;
		
		/**
		 * Private method inside private thread class used to test the ambsi device
		 * for proper functioning.
		 * 
		 * @return boolean indicating if the device is working (true) or not (false).
		 * @throws AcsJContainerServicesEx if there was a problem getting/setting the device.
		 */
		private boolean isAmbDeviceWorking() throws AcsJContainerServicesEx
		{
			boolean returnValue = false;
			
			// first, get the device reference, if necessary
			if(null == device)
			{
				device = setDeviceComponent();
			}
			
			// next, determine the state of the device
			if(null != device && device.getHwState() == HwState.Operational)
			{
				returnValue = true;
			}
			
			return returnValue;
		}
	
		/**
		 * Constructor for the thread which will maintain the state of the chessboard cells.
		 */
		public MaintainStateThread()
		{
			this.setDaemon(true);
		}
		
		/**
		 * Sets a flag indicating whether the polling thread should stop/exit.
		 * @param shouldStop whether the thread should stop (true) or not (false).
		 */
		public void setShouldStop(boolean shouldStop)
		{
			this.shouldStop = shouldStop;
		}
		
		public void run()
		{
			while(false == shouldStop) 
			{
				// sleep for a bit before executing the loop (again)
				try {
					Thread.sleep(pollingThreadIntervalMilliseconds);
				}
				catch(InterruptedException ex) {
					// noop
				}

				try 
				{
					boolean deviceIsWorking = isAmbDeviceWorking();
					
					// if the device is working properly (and a non-null device returned
					// from the setDeviceComponent method), then we can proceed with our 
					// efforts to poll. Otherwise, we will not query for state changes
					// unless/until the device is properly working.
					if(deviceIsWorking)
					{
						// if we haven't previously established communications, and the device is working properly,
						// notify listeners that communications are established. This needs only to be done the 
						// first time that we've come through the loop w/o communications established, subsequent
						// iterations may bypass this logic unless/until communications are lost, at which point 
						// we can again excercise this logic...
						if(false == hasCommunications)
						{
							ExecuteCommunicationUpdateOnSwingThread updateListenersThread = new ExecuteCommunicationUpdateOnSwingThread(true);
							updateListenersThread.start();
							hasCommunications = true;
						}	
					
						// with a functioning device, we can query for state changes and notify listeners
						// if any differences were detected from the last polled value.
						List<HardwareDevicePresentationModelEvent> changes = queryForStateChanges();	
						if(null != changes && !changes.isEmpty()) {
							ExecuteChangesOnSwingThread swingThread = new ExecuteChangesOnSwingThread(changes);
							SwingUtilities.invokeLater(swingThread);
						}
					}
				}
				// TODO - figure out the proper exception to catch for lost connectivity to backend...
				catch(Throwable ex) 
				{
					// If we have previously established communications, then we must notify listeners
					// that the communication is now lost. If we haven't previously established communication
					// (or if we have already notified listeners of lost communications), this logic need not
					// be executed.
					if(true == hasCommunications && false == shouldStop) 
					{
						ExecuteCommunicationUpdateOnSwingThread updateListenersThread = new ExecuteCommunicationUpdateOnSwingThread(false);
						updateListenersThread.start();
						releaseDeviceComponent();
						hasCommunications = false;
					}
					
					if(false == shouldStop)
					{
						logger.warning("HardwareDevicePresentationModel caught an exception in MaintainStateThread " + ExcLog.details(ex));
					}
				}
			}
		}
	}
}
