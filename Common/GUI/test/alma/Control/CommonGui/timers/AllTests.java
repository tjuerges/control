package alma.Control.CommonGui.timers;
	
import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

    public static Test suite() {
        TestSuite suite = new TestSuite("Tests for alma.Control.ArrayStatusGui.timer");
        suite.addTestSuite(PeriodicTimerManagerTests.class);
        return suite;
    }
}
