/*
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.CommonGui.timers;

import java.util.ArrayList;
import java.util.Date;

import junit.framework.TestCase;

/**
 * Tests for <code>alma.Control.ArrayStatusGui.timer.PeriodicTimerManager</code>.
 * <p>
 * As much as possible, test to the IPeriodicTimerManager interface.  Where necessary, test to the 
 * PeriodicTimerManager implementation.
 */
public class PeriodicTimerManagerTests extends TestCase {
    
    /**
     * A minimal <code>TimerListener</code> for testing.
     */
    private class TestListener implements ITimerListener {
        
        /**
         * These are only used for logging.  They have no influence on any behavior.
         */
        private long period;
        private String name;
        
        /**
         * Log of messages produced by the timerExpired() method.
         */
        private ArrayList<String> log = new ArrayList<String>(); 
        
        public TestListener (long period, String name) {
            this.period = period;
            this.name = name;
        }

        /**
         * Dump the internal log for debugging.
         */
        public void dumpLog() {
            System.out.println("TestListener internal log size: " + log.size());
            for (String line: log) {
                System.out.println(line);
            }
        }
        
        /**
         * Report the internal log size.  For crude test result checking.
         * 
         * @return the size of the internal log.
         */
        public int logSize() {
            return log.size();
        }

        /**
         * The reason for the TimerListener to exist.
         * 
         * This method just logs when it was called.
         */
        @Override
        public void timerExpired() {
            log.add(period + " millisecond timer " + name +  ": " + new Date().getTime());
        }
        
    } // TestListener
    
    /**
     * The PeriodicTimerManager being tested.
     */
    private PeriodicTimerManager testPTM;
    
    public PeriodicTimerManagerTests(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() {
        testPTM = new PeriodicTimerManager();
    }
    
    /**
     * Ensure PeriodicTimerManager.notifyEvery() enforces restrictions listener parameter.
     */
    public void testBoundsCheckingOnListener() {

        final String METHOD_NAME =  getClass().getName() + ".testBoundsCheckingOnListener()";

        // Debugging only. Comment out before release.
        // System.out.println("\n" + METHOD_NAME + ": begin.");

        Exception expectedException = null;
        try { // Listener null.  Exception expected.
            testPTM.notifyEvery(PeriodicTimerManager.PERIOD_LOWER_BOUND, null);
        } catch (IllegalArgumentException e) {
            expectedException = e;
        }
        assertNotNull(METHOD_NAME + ": did not receive expected exception.", expectedException);
        
        expectedException = null;
        TestListener tl = new TestListener(PeriodicTimerManager.PERIOD_LOWER_BOUND, "A");        
        try { // Listener not null.  No exception expected.
            testPTM.notifyEvery(PeriodicTimerManager.PERIOD_LOWER_BOUND, tl);
        } catch (IllegalArgumentException e) {
            expectedException = e;
        }
        assertNull(METHOD_NAME + ": received unexpected exception.", expectedException);

        testPTM.stopAllNotifications();

        // Debugging only. Comment out before release.
        // System.out.println(METHOD_NAME + ": end.");
    }
    
    /**
     * Ensure PeriodicTimerManager.notifyEvery() enforces restrictions on duration parameter.
     */
    public void testBoundsCheckingOnPeriod() {

        final String METHOD_NAME = getClass().getName() + ".testBoundsCheckingOnPeriod()";

        // Debugging only. Comment out before release.
        // System.out.println("\n" + METHOD_NAME + ": begin.");

        Exception expectedException = null;
        TestListener tl = new TestListener(PeriodicTimerManager.PERIOD_LOWER_BOUND, "A");

        try { // More out of bounds. Exception expected.
            // Note: use PeriodicTimerManager.PERIOD_LOWER_BOUND to test an implementation detail.
            testPTM.notifyEvery(PeriodicTimerManager.PERIOD_LOWER_BOUND - 10, tl);
        } catch (IllegalArgumentException e) {
            expectedException = e;
        }

        expectedException = null;
        try { // Out of bounds at boundary. Exception expected.
            // Note: use PeriodicTimerManager.PERIOD_LOWER_BOUND to test an implementation detail.
            testPTM.notifyEvery(PeriodicTimerManager.PERIOD_LOWER_BOUND - 1, tl);
        } catch (IllegalArgumentException e) {
            expectedException = e;
        }
        assertNotNull(METHOD_NAME + ": did not receive expected exception.", expectedException);

        expectedException = null;
        try { // In bounds at boundary. No exception expected.
            // Note: use PeriodicTimerManager.PERIOD_LOWER_BOUND to test an implementation detail.
            testPTM.notifyEvery(PeriodicTimerManager.PERIOD_LOWER_BOUND, tl);
        } catch (IllegalArgumentException e) {
            expectedException = e;
        }
        assertNull(METHOD_NAME + ": received unexpected exception.", expectedException);

        testPTM.stopAllNotifications();

        // Debugging only. Comment out before release.
        // System.out.println(METHOD_NAME + ": end.");
    }
    
    /**
     * Ensure the class constructor creates an instance without any tasks.
     */
    public void testConstructor() {

        final String METHOD_NAME = getClass().getName() + ".testConstructor()";

        // Debugging only. Comment out before release.
        // System.out.println("\n" + METHOD_NAME + ": begin.");

        assertEquals(METHOD_NAME + ": newly created PeriodicTimerManager was not empty.", 
                0, testPTM.getTaskCount());
        
        testPTM.stopAllNotifications();
        
        // Debugging only. Comment out before release.
        // System.out.println(METHOD_NAME + ": end.");
    }

    /**
     * Ensure we can create one listener with a 1s period.
     * Ensure it triggers 10 times in 10s.
     */
    public void testOneListener() {

        final String METHOD_NAME = getClass().getName() + ".testOneListener()";

        // Debugging only. Comment out before release.
        // System.out.println("\n" + METHOD_NAME + ": begin.");

        TestListener tl = new TestListener(1 * IPeriodicTimerManager.SECONDS, "A");
        testPTM.notifyEvery(1 * IPeriodicTimerManager.SECONDS, tl);
        
        try { // Sleep 10 seconds to allow for 10 notifications.
            Thread.sleep((10 * IPeriodicTimerManager.SECONDS));
        } catch (InterruptedException e) {
            fail(METHOD_NAME + ": Thread.sleep interrupted.");
        }
        
        testPTM.stopNotifying(tl);
        
        try { // To ensure reproducible results, sleep to allow for any remaining notifications.
            Thread.sleep((5 * IPeriodicTimerManager.SECONDS));
        } catch (InterruptedException e) {
            fail(METHOD_NAME + ": Thread.sleep interrupted.");
        }

        // For debugging if assertion fails.
        // tl.dumpLog();
        assertEquals(METHOD_NAME + ": log sizes do not match.", 10, tl.logSize());
        
        testPTM.stopAllNotifications();
        
        // Debugging only. Comment out before release.
        // System.out.println(METHOD_NAME + ": end.");
    }

    /**
     * Ensure we can create one listener with a 1s period.  
     * Ensure we can not create a duplicate.
     * Ensure it triggers 10 times in 10s.
     */
    public void testOneListenerRegisteredTwice() {
        
        final String METHOD_NAME = getClass().getName() + ".testOneListenerRegisteredTwice()";

        // Debugging only. Comment out before release.
        // System.out.println("\n" + METHOD_NAME + ": begin.");

        TestListener tl = new TestListener(1 * IPeriodicTimerManager.SECONDS, "A");
        testPTM.notifyEvery(1 * IPeriodicTimerManager.SECONDS, tl);
        testPTM.notifyEvery(1 * IPeriodicTimerManager.SECONDS, tl);
        
        try { // Sleep 10 seconds to allow for 10 notifications.
            Thread.sleep((10 * IPeriodicTimerManager.SECONDS));
        } catch (InterruptedException e) {
            fail(METHOD_NAME + ": Thread.sleep interrupted.");
        }
        
        testPTM.stopNotifying(tl);
        
        try { // To ensure reproducible results, sleep to allow for any remaining notifications.
            Thread.sleep((5 * IPeriodicTimerManager.SECONDS));
        } catch (InterruptedException e) {
            fail(METHOD_NAME + ": Thread.sleep interrupted.");
        }

        // For debugging if assertion fails.
        // tl.dumpLog();
        assertEquals(METHOD_NAME + ": log sizes do not match.", 10, tl.logSize());

        testPTM.stopAllNotifications();

        // Debugging only. Comment out before release.
        // System.out.println(METHOD_NAME + ": end.");
    }

    /**
     * Ensure the TestListener defined in this class can store internal log messages and report its 
     * log contents.
     */
    public void testTestListener() {
        
        final String METHOD_NAME = getClass().getName() + ".testTestListener()";

        // Debugging only. Comment out before release.
        // System.out.println("\n" + METHOD_NAME + ": begin.");

        TestListener tl = new TestListener(1 * IPeriodicTimerManager.SECONDS, "A");
        tl.timerExpired();
        tl.timerExpired();
        tl.timerExpired();
        // For debugging if assertion fails.
        // tl.dumpLog();
        assertEquals(METHOD_NAME + ": log sizes do not match.", 3, tl.logSize());
        
        // Debugging only. Comment out before release.
        // System.out.println(METHOD_NAME + ": end.");
    }

    /**
     * Ensure we can create two listeners with different periods.
     * Ensure it triggers 10 times in 10s.
     */
    public void testTwoListenersDifferentPeriods() {
        
        final String METHOD_NAME = getClass().getName() + ".testTwoListenersDifferentPeriods()";

        // Debugging only. Comment out before release.
        // System.out.println("\n" + METHOD_NAME + ": begin.");

        TestListener tlA = new TestListener(1 * IPeriodicTimerManager.SECONDS, "A");
        TestListener tlB = new TestListener(3 * IPeriodicTimerManager.SECONDS, "B");
        testPTM.notifyEvery(1 * IPeriodicTimerManager.SECONDS, tlA);
        testPTM.notifyEvery(3 * IPeriodicTimerManager.SECONDS, tlB);
        
        try { // Sleep 10 seconds to allow for 10 notifications on 1s listener.
            Thread.sleep((10 * IPeriodicTimerManager.SECONDS));
        } catch (InterruptedException e) {
            fail(METHOD_NAME + ": Thread.sleep interrupted.");
        }
        
        testPTM.stopNotifying(tlA);
        testPTM.stopNotifying(tlB);
        
        try { // To ensure reproducible results, sleep to allow for any remaining notifications.
            Thread.sleep((5 * IPeriodicTimerManager.SECONDS));
        } catch (InterruptedException e) {
            fail(METHOD_NAME + ": Thread.sleep interrupted.");
        }

        // For debugging if assertion fails.
        // tlA.dumpLog();
        // tlB.dumpLog();
        assertEquals(METHOD_NAME + ": log sizes do not match.", 10, tlA.logSize());
        assertEquals(METHOD_NAME + ": log sizes do not match.", 4, tlB.logSize());
        
        testPTM.stopAllNotifications();
        
        // Debugging only. Comment out before release.
        // System.out.println(METHOD_NAME + ": end.");
    }

    /**
     * Ensure we can create two listeners with different periods.  
     * Ensure we can not create a duplicate.
     * Ensure it triggers 10 times in 10s.
     */
    public void testTwoListenersDifferentPeriodsRegisteredTwice() {
        
        final String METHOD_NAME = getClass().getName() + 
            ".testTwoListenersDifferentPeriodsRegisteredTwice()";

        // Debugging only. Comment out before release.
        // System.out.println("\n" + METHOD_NAME + ": begin.");

        TestListener tlA = new TestListener(1 * IPeriodicTimerManager.SECONDS, "A");
        TestListener tlB = new TestListener(3 * IPeriodicTimerManager.SECONDS, "B");
        testPTM.notifyEvery(1 * IPeriodicTimerManager.SECONDS, tlA);
        testPTM.notifyEvery(3 * IPeriodicTimerManager.SECONDS, tlB);
        testPTM.notifyEvery(1 * IPeriodicTimerManager.SECONDS, tlA);
        testPTM.notifyEvery(3 * IPeriodicTimerManager.SECONDS, tlB);
        
        try { // Sleep 10 seconds to allow for 10 notifications.
            Thread.sleep((10 * IPeriodicTimerManager.SECONDS));
        } catch (InterruptedException e) {
            fail(METHOD_NAME + ": Thread.sleep interrupted.");
        }
        
        testPTM.stopNotifying(tlA);
        testPTM.stopNotifying(tlB);
        
        try { // To ensure reproducible results, sleep to allow for any remaining notifications.
            Thread.sleep((5 * IPeriodicTimerManager.SECONDS));
        } catch (InterruptedException e) {
            fail(METHOD_NAME + ": Thread.sleep interrupted.");
        }

        // For debugging if assertion fails.
        // tlA.dumpLog();
        // tlB.dumpLog();
        assertEquals(METHOD_NAME + ": log sizes do not match.", 10, tlA.logSize());
        assertEquals(METHOD_NAME + ": log sizes do not match.", 4, tlB.logSize());

        testPTM.stopAllNotifications();

        // Debugging only. Comment out before release.
        // System.out.println(METHOD_NAME + ": end.");
    }

    /**
     * Ensure we can create two listeners with different periods.
     * Ensure it triggers 10 times in 10s.
     * Ensure we can stop notification after 10s.
     */
    public void testTwoListenersDifferentPeriodsStopNotification() {
        
        final String METHOD_NAME = getClass().getName() + 
            ".testTwoListenersDifferentPeriodsStopNotification()";

        // Debugging only. Comment out before release.
        // System.out.println("\n" + METHOD_NAME + ": begin.");

        TestListener tlA = new TestListener(1 * IPeriodicTimerManager.SECONDS, "A");
        TestListener tlB = new TestListener(3 * IPeriodicTimerManager.SECONDS, "B");
        testPTM.notifyEvery(1 * IPeriodicTimerManager.SECONDS, tlA);
        testPTM.notifyEvery(3 * IPeriodicTimerManager.SECONDS, tlB);
        
        try { // Sleep 10 seconds to allow for 10 notifications.
            Thread.sleep((10 * IPeriodicTimerManager.SECONDS));
        } catch (InterruptedException e) {
            fail(METHOD_NAME + ": Thread.sleep interrupted.");
        }
        
        testPTM.stopNotifying(tlA);
        
        try { // Sleep 5 seconds to allow for additional notifications on tlB.
            Thread.sleep((5 * IPeriodicTimerManager.SECONDS));
        } catch (InterruptedException e) {
            fail(METHOD_NAME + ": Thread.sleep interrupted.");
        }
        
        testPTM.stopNotifying(tlB);

        try { // To ensure reproducible results, sleep to allow for any remaining notifications.
            Thread.sleep((5 * IPeriodicTimerManager.SECONDS));
        } catch (InterruptedException e) {
            fail(METHOD_NAME + ": Thread.sleep interrupted.");
        }
        
        // For debugging if assertion fails.
        // tlA.dumpLog();
        // tlB.dumpLog();
        assertEquals(METHOD_NAME + ": log sizes do not match.", 10, tlA.logSize());
        assertEquals(METHOD_NAME + ": log sizes do not match.", 5, tlB.logSize());
        
        testPTM.stopAllNotifications();

        // Debugging only. Comment out before release.
        // System.out.println(METHOD_NAME + ": end.");
    }

    /**
     * Ensure we can create two listeners with a 1s period.
     * Ensure it triggers 10 times in 10s.
     */
    public void testTwoListenersSamePeriod() {
        
        final String METHOD_NAME =  getClass().getName() + ".testTwoListenersSamePeriod()";

        // Debugging only. Comment out before release.
        // System.out.println("\n" + METHOD_NAME + ": begin.");

        TestListener tlA = new TestListener(1 * IPeriodicTimerManager.SECONDS, "A");
        TestListener tlB = new TestListener(1 * IPeriodicTimerManager.SECONDS, "B");
        testPTM.notifyEvery(1 * IPeriodicTimerManager.SECONDS, tlA);
        testPTM.notifyEvery(1 * IPeriodicTimerManager.SECONDS, tlB);
        
        try { // Sleep 10 seconds to allow for 10 notifications.
            Thread.sleep((10 * IPeriodicTimerManager.SECONDS));
        } catch (InterruptedException e) {
            fail(METHOD_NAME + ": Thread.sleep interrupted.");
        }
        
        testPTM.stopNotifying(tlA);
        testPTM.stopNotifying(tlB);
        
        try { // To ensure reproducible results, sleep to allow for any remaining notifications.
            Thread.sleep((5 * IPeriodicTimerManager.SECONDS));
        } catch (InterruptedException e) {
            fail(METHOD_NAME + ": Thread.sleep interrupted.");
        }

        // For debugging if assertion fails.
        // tlA.dumpLog();
        // tlB.dumpLog();
        assertEquals(METHOD_NAME + ": log sizes do not match.", 10, tlA.logSize());
        assertEquals(METHOD_NAME + ": log sizes do not match.", 10, tlB.logSize());
        
        testPTM.stopAllNotifications();
        
        // Debugging only. Comment out before release.
        // System.out.println(METHOD_NAME + ": end.");
    }

    /**
     * Ensure we can create two listeners with a 1s period.  
     * Ensure we can not create a duplicate.
     */
    public void testTwoListenersSamePeriodRegisteredTwice() {
        
        final String METHOD_NAME = getClass().getName() + 
            ".testTwoListenersSamePeriodRegisteredTwice()";

        // Debugging only. Comment out before release.
        // System.out.println("\n" + METHOD_NAME + ": begin.");

        TestListener tlA = new TestListener(1 * IPeriodicTimerManager.SECONDS, "A");
        TestListener tlB = new TestListener(1 * IPeriodicTimerManager.SECONDS, "B");
        testPTM.notifyEvery(1 * IPeriodicTimerManager.SECONDS, tlA);
        testPTM.notifyEvery(1 * IPeriodicTimerManager.SECONDS, tlB);
        testPTM.notifyEvery(1 * IPeriodicTimerManager.SECONDS, tlA);
        testPTM.notifyEvery(1 * IPeriodicTimerManager.SECONDS, tlB);
        
        try { // Sleep 10 seconds to allow for 10 notifications.
            Thread.sleep((10 * IPeriodicTimerManager.SECONDS));
        } catch (InterruptedException e) {
            fail(METHOD_NAME + ": Thread.sleep interrupted.");
        }
        
        testPTM.stopNotifying(tlA);
        testPTM.stopNotifying(tlB);
        
        try { // To ensure reproducible results, sleep to allow for any remaining notifications.
            Thread.sleep((5 * IPeriodicTimerManager.SECONDS));
        } catch (InterruptedException e) {
            fail(METHOD_NAME + ": Thread.sleep interrupted.");
        }

        // For debugging if assertion fails.
        // tlA.dumpLog();
        // tlB.dumpLog();
        assertEquals(METHOD_NAME + ": log sizes do not match.", 10, tlA.logSize());
        assertEquals(METHOD_NAME + ": log sizes do not match.", 10, tlB.logSize());

        testPTM.stopAllNotifications();

        // Debugging only. Comment out before release.
        // System.out.println(METHOD_NAME + ": end.");
    }

    /**
     * Ensure we can create two listeners with a 1s period.
     * Ensure the 1s listener triggers 10 times in 10s.
     * Ensure the 3s listener triggers 5 times in 15s.
     * Ensure we can stop notification after 10s and 15s.
     */
    public void testTwoListenersSamePeriodStopNotification() {

        final String METHOD_NAME = getClass().getName() + 
            ".testTwoListenersSamePeriodStopNotification()";

        // Debugging only. Comment out before release.
        // System.out.println("\n" + METHOD_NAME + ": begin.");

        TestListener tlA = new TestListener(1 * IPeriodicTimerManager.SECONDS, "A");
        TestListener tlB = new TestListener(1 * IPeriodicTimerManager.SECONDS, "B");
        testPTM.notifyEvery(1 * IPeriodicTimerManager.SECONDS, tlA);
        testPTM.notifyEvery(1 * IPeriodicTimerManager.SECONDS, tlB);
        
        try { // Sleep 10 seconds to allow for 10 notifications.
            Thread.sleep((10 * IPeriodicTimerManager.SECONDS));
        } catch (InterruptedException e) {
            fail(METHOD_NAME + ": Thread.sleep interrupted.");
        }
        
        testPTM.stopNotifying(tlA);
        
        try { // Sleep 5 seconds to allow for additional notifications on tlB.
            Thread.sleep((5 * IPeriodicTimerManager.SECONDS));
        } catch (InterruptedException e) {
            fail(METHOD_NAME + ": Thread.sleep interrupted.");
        }
        
        testPTM.stopNotifying(tlB);

        try { // To ensure reproducible results, sleep to allow for any remaining notifications.
            Thread.sleep((5 * IPeriodicTimerManager.SECONDS));
        } catch (InterruptedException e) {
            fail(METHOD_NAME + ": Thread.sleep interrupted.");
        }
        
        // For debugging if assertion fails.
        // tlA.dumpLog();
        // tlB.dumpLog();
        assertEquals(METHOD_NAME + ": log sizes do not match.", 10, tlA.logSize());
        assertEquals(METHOD_NAME + ": log sizes do not match.", 15, tlB.logSize());
        
        testPTM.stopAllNotifications();

        // Debugging only. Comment out before release.
        // System.out.println(METHOD_NAME + ": end.");
    }

} // PeriodicTimerManager

//
// O_o
