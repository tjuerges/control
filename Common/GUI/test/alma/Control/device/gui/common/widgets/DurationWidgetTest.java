/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets;

import java.awt.BorderLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JFrame;
import javax.swing.JLabel;


/**
 * Test a <code>DurationWidget<code> interactively.
 * 
 * @author Scott Rankin  srankin@nrao.edu
 * @since  ALMA 7.0.0
 * @version $Id$
 */
public final class DurationWidgetTest implements PropertyChangeListener {

    /**
     * Display a copy of a <code>DurationWidget</code> for interactive testing.
     * 
     * @param args  are ignored.
     */
    public static void main(String[] args) {
        final DurationWidgetTest durationWidget = new DurationWidgetTest();
        durationWidget.monitorWidgetTest();
    }
    
    /**
     * Respond to property changed events from the <code>DurationWidget</code>.
     */
    @Override
    public void propertyChange(PropertyChangeEvent event) {
        Object source = event.getSource();
        if (source == testDurationWidget) {
            System.out.print("Event: testDurationWidget.getDuration(): ");
            System.out.println(testDurationWidget.getDuration());                    
        }
    }

    /**
     * Create an instance of the test.
     */
    private DurationWidgetTest() {
        buildFrame();
    }

    /**
     * Build the frame containing the test widget.
     */
    private void buildFrame() {
        final long seconds = 1;
        final long initialTestValue = 30 * seconds;

        //
        // Create the frame to hold the test widget.
        final JFrame frame = new JFrame("DurationWidget Test");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //
        // Create the test widget and labels to describe it.
        final String description = " Duration ";
        final String units = " seconds ";
        
        frame.add(new JLabel(description), BorderLayout.WEST);
        
        testDurationWidget = new DurationWidget(initialTestValue, width);
        testDurationWidget.addPropertyChangeListener("value", this);
        frame.add(testDurationWidget, BorderLayout.CENTER);
        
        frame.add(new JLabel(units), BorderLayout.EAST);
        
        //
        // Make the frame visible at a reasonable default size.
        final int defaultX = 200;
        final int defaultY = 40;
        frame.setSize(defaultX, defaultY);
        frame.setVisible(true);
    }

    /**
     * Provide data to use as they test the widget.
     */
    private void monitorWidgetTest() {
        //
        // Play with the widget and watch the console.  The console should display values for
        // testDurationWidget.getValue() that match the contents of the widget.
        final long milliseconds = 1000;
        while (true) {
            System.out.print("Poll:  testDurationWidget.getDuration(): ");
            System.out.println(testDurationWidget.getDuration());
            try {
                Thread.sleep(5 * milliseconds);
            } catch (InterruptedException e) {
                System.out.println("Test completed.");
            }
        }
    }

    /**
     * Width of widget in characters.
     */
    private final int width = 7;
    
    /**
     * Widget under test.
     */
    private DurationWidget testDurationWidget;

//    /**
//     * Report to the user when an ActionEven is received.
//     */
//    @Override
//    public void actionPerformed(ActionEvent e) {
//        System.out.print("Event: testDurationWidget.getDuration(): ");
//        System.out.println(testDurationWidget.getDuration());        
//    }
}

//
// O_o
