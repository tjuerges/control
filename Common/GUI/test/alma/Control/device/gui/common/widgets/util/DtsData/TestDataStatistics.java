package alma.Control.device.gui.common.widgets.util.DtsData;

import junit.framework.TestCase;

public class TestDataStatistics extends TestCase {
    
    public TestDataStatistics(String name) {
        super(name);
        aDataPacket = new DataPacket();
        //First batch of data to test: all 0s
        aDataPacket.addRawData(0L, 0L, 0L, 0L, 0L, 0L);
        //Second batch of data to test: all 1s (java uses 2s complement, which means -1 is all 1s in binary)
        aDataPacket.addRawData(-1L, -1L, -1L, -1L, -1L, -1L);
        //3rd batch of data to test: set BBpr0 to -4 and BBPr1 to 1
        aDataPacket.addRawData(0xFFFF0000FFFF0000L, 0xFFFF0000FFFF0000L, 0xFFFF0000FFFF0000L,
                               0xFFFF0000FFFF0000L, 0xFFFF0000FFFF0000L, 0xFFFF0000FFFF0000L);
        //4th batch of data to test: Just D is 1
        aDataPacket.addRawData(-1L, -1L, 0L, 0L, 0L, 0L);
        //5th batch of data to test: Just C is 1
        aDataPacket.addRawData(0L, 0L, -1L, -1L, 0L, 0L);
        //6th batch of data to test: Just B is 1
        aDataPacket.addRawData(0L, 0L, 0L, 0L, -1L, -1L);
        //7th, 8th, 9th, and 10th batch of data to test: D&C are 1
        aDataPacket.addRawData(-1L, -1L, -1L, -1L, 0, 0);
        aDataPacket.addRawData(-1L, -1L, -1L, -1L, 0, 0);
        aDataPacket.addRawData(-1L, -1L, -1L, -1L, 0, 0);
        aDataPacket.addRawData(-1L, -1L, -1L, -1L, 0, 0);
        statistics0 = new DataStatistics(aDataPacket, false);
        statistics1 = new DataStatistics(aDataPacket, true);
    }
    
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
//    public void testDataStatistics() {
//        fail("Not yet implemented"); // TODO
//    }
    public void testCalcMeanFromPdf(){
        //Since these are odds of alma values, this corresponds to
        //a mean of -0.5.
        assertEquals(-0.5, statistics0.calcMeanFromPdf(
                            new double[] {0.2, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.2}), .0000000001);
        assertEquals(-0.5, statistics0.calcMeanFromPdf(
                            new double[] {0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5}), .0000000001);
        assertEquals(-4, statistics0.calcMeanFromPdf(
                            new double[] {1.0, 0, 0, 0, 0, 0, 0, 0}), .0000000001);
        assertEquals(3, statistics0.calcMeanFromPdf(
                            new double[] {0, 0, 0, 0, 0, 0, 0, 1.0}), .0000000001);
        assertEquals(0, statistics0.calcMeanFromPdf(
                            new double[] {0, 0, 0, 0, 1.0, 0, 0, 0}), .0000000001);
        assertEquals(0, statistics0.calcMeanFromPdf(
                            new double[] {0, 0.1, 0.4, 0, 0, 0, 0.4, 0.1}), .0000000001);
    }
//    public void testGetAmplititudeError() {
//        fail("Not yet implemented"); // TODO
//    }
    
    /**
     * We calculate the standard deviation manually and then compare it with the result given
     * by calcSD. Note that we use the standard method here, and calculate the deviation from the PDF
     * in the calcSD function.This gives us at least some assurance that the standard deviation is working
     * correctly.
     */
    public void testCalcSD(){
        //Get the current data set
        byte[][] data=null;
        double[] pdf=null;
        try {
            data = aDataPacket.getAlmaValuesPol0();
            pdf=statistics0.getPdf();
        } catch (InsufficientDataException e) {
            fail("Unexpected InsufficientDataException");
        }
        //Manually calculate the mean
        double mean=0;
        for (int c=0; c<data.length; c++)
            for (int c2=0; c2<16; c2++)
                mean+=data[c][c2];
        mean/=data.length*16;
        //Check the manual mean against the calculated mean
        assertEquals("Error calculating mean!", mean, statistics0.calcMeanFromPdf(pdf), 0.000000001);
        //Manually calculate the standard deviation
        double sd=0;
        for (int c=0; c<data.length; c++)
            for (int c2=0; c2<16; c2++)
                sd+=Math.pow(data[c][c2]-mean, 2);
        sd=Math.sqrt(sd/(data.length*16));
        assertEquals(sd, statistics0.calcSdFromPdf(pdf), 0.000000001);
    }
    
    public void testGetAmplituedError(){
        double[] sd = new double[16];
        byte[][] data=null;
        double[] ampErrors=null;
        double[] means=new double[16];
        double theoreticalSD=statistics0.calcSdFromPdf(theoreticalPdf);
        
        //Get the current data set
        try {
            data = aDataPacket.getAlmaValuesPol0();
            ampErrors=statistics0.getAmplitudeError();
        } catch (InsufficientDataException e) {
            fail("Unexpected InsufficientDataException");
        }
        
        //Manually find the mean for each channel.
        for (int c=0; c<16; c++){
            for (int c2=0; c2<data.length; c2++)
                means[c]+=data[c2][c];
            means[c]/=data.length;
        }
        
        //Manually calculate the standard deviation for each channel
        for (int c=0; c<16; c++){
            for (int c2=0; c2<data.length; c2++)
                sd[c]+=Math.pow(data[c2][c]-means[c], 2);
            sd[c]=Math.sqrt(sd[c]/data.length);
        }

        //Check if it matches the reported deviation.
        for (int c=0; c<16; c++)
            assertEquals(sd[c]-theoreticalSD, ampErrors[c], .0000001);
    }
    
    public void testGetOffsetError(){
        DataPacket data = new DataPacket();
        //Set all values to -4, giving us a mean of -4, which should make an offset error of -3.5.
        data.addRawData(0L, 0L, 0L, 0L, 0L, 0L);
        DataStatistics stat1 = new DataStatistics(data, false);
        double[] offsets=null;
        try {offsets = stat1.getOffsetError();}
        catch (InsufficientDataException e) {fail("InsufficentDataException when there should not be one!");}
        for (int c=0; c<16; c++)
            assertEquals(-3.5, offsets[c], 0.0000001);
        
        //Use a fresh data packet to avid any errors due to changing the data.
        data = new DataPacket();
        //A batch of -4s and a batch of 3s to give us a mean of -0.5, which means an offset error of 0
        data.addRawData(0L, 0L, 0L, 0L, 0L, 0L);
        data.addRawData(-1L, -1L, 0L, 0L, 0L, 0L);
        stat1 = new DataStatistics(data, false);
        try {offsets = stat1.getOffsetError();}
        catch (InsufficientDataException e) {fail("InsufficentDataException when there should not be one!");}
        for (int c=0; c<16; c++)
            assertEquals(0.0, offsets[c], 0.0000001);
        
        //Use a fresh data packet to avid any errors due to changing the data.
        data = new DataPacket();
        //2 batches of 1, 2, & 3, to give a mean of 2 and offset error of 2.5
        data.addRawData(-1L, -1L, -1L, -1L, -1L, -1L);
        data.addRawData(-1L, -1L, -1L, -1L, -1L, -1L);
        data.addRawData(-1L, -1L, 0L, 0L, -1L, -1L);
        data.addRawData(-1L, -1L, 0L, 0L, -1L, -1L);
        data.addRawData(-1L, -1L, 0L, 0L, 0L, 0L);
        data.addRawData(-1L, -1L, 0L, 0L, 0L, 0L);
        stat1 = new DataStatistics(data, false);
        try {offsets = stat1.getOffsetError();}
        catch (InsufficientDataException e) {fail("InsufficentDataException when there should not be one!");}
        for (int c=0; c<16; c++)
            assertEquals(2.5, offsets[c], 0.0000001);
    }
    public void testGetOccuranceCountPol0() {
        int[] bbpr0 = null;
        try {
            bbpr0 = statistics0.getOccuranceCount();
        } catch (InsufficientDataException e) {
            fail("InsufficentDataException when there should not be one!");
        }
        assertEquals(bbpr0[0], 128);
        assertEquals(bbpr0[1], 64);
        assertEquals(bbpr0[2], 0);
        assertEquals(bbpr0[3], 64);
        assertEquals(bbpr0[4], 256);
        assertEquals(bbpr0[5], 64);
        assertEquals(bbpr0[6], 0);
        assertEquals(bbpr0[7], 64);
    }
    
    public void testGetOccuranceCountPol1() {
        int[] bbpr1 = null;
        try {
            bbpr1 = statistics1.getOccuranceCount();
        } catch (InsufficientDataException e) {
            fail("InsufficentDataException when there should not be one!");
        }
        assertEquals(bbpr1[0], 64);
        assertEquals(bbpr1[1], 64);
        assertEquals(bbpr1[2], 0);
        assertEquals(bbpr1[3], 64);
        assertEquals(bbpr1[4], 256);
        assertEquals(bbpr1[5], 128);
        assertEquals(bbpr1[6], 0);
        assertEquals(bbpr1[7], 64);
    }
    
    public void testGetPdfBBPol0(){
        double[] probabilities = null;
        try {
            probabilities = statistics0.getPdf();
        } catch (InsufficientDataException e) {
            fail("InsufficentDataException when there should not be one!");
        }
        assertEquals(probabilities[0], 0.2);
        assertEquals(probabilities[1], 0.1);
        assertEquals(probabilities[2], 0.0);
        assertEquals(probabilities[3], 0.1);
        assertEquals(probabilities[4], 0.4);
        assertEquals(probabilities[5], 0.1);
        assertEquals(probabilities[6], 0.0);
        assertEquals(probabilities[7], 0.1);
        double totalOdds=0.0;
        for (int c=0; c<8; c++)
            totalOdds+=probabilities[c];
        assertEquals("The total odds are not 1!!!", 1.0, totalOdds, 0.000000001);
    }
    
    public void testGetPdfBBPol1(){
        double[] probabilities = null;
        try {
            probabilities = statistics1.getPdf();
        } catch (InsufficientDataException e) {
            fail("InsufficentDataException when there should not be one!");
        }
        assertEquals(probabilities[0], 0.1);
        assertEquals(probabilities[1], 0.1);
        assertEquals(probabilities[2], 0.0);
        assertEquals(probabilities[3], 0.1);
        assertEquals(probabilities[4], 0.4);
        assertEquals(probabilities[5], 0.2);
        assertEquals(probabilities[6], 0.0);
        assertEquals(probabilities[7], 0.1);
        double totalOdds=0.0;
        for (int c=0; c<8; c++)
            totalOdds+=probabilities[c];
        assertEquals("The total odds are not 1!!!", 1.0, totalOdds, 0.000000001);
    }
    
    private DataPacket aDataPacket;
    private DataStatistics statistics0;
    private DataStatistics statistics1;
    //This is the theoretical PDF (probability density function), or what we expect the values to be, assuming
    //that we are seeing incoming boadband Gaussian noise. Used to calculate the offset error and
    //amplitude error.
    //From 50.00.00.00-306-A-MAN
    private final double[] theoreticalPdf = {0.0349931, 0.0785305, 0.1593980, 0.2270780,
                               0.2270780, 0.1593980, 0.0785305, 0.0349931};
}
