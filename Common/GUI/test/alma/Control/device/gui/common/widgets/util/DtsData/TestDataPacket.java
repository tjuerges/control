/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common.widgets.util.DtsData;

import junit.framework.TestCase;

/**
 * Test the DataPacket class in widgets/util/DtsData
 * @author dhunter
 *
 */
public class TestDataPacket extends TestCase {
    
    /**
     * @param name
     */
    public TestDataPacket(String name) {
        super(name);
        aDataPacket = new DataPacket();
    }
    
    /* (non-Javadoc)
     * @see junit.framework.TestCase#setUp()
     */
    protected void setUp() throws Exception {
        super.setUp();
        //First batch of data to test: all 0s
        aDataPacket.addRawData(0L, 0L, 0L, 0L, 0L, 0L);
        //Second batch of data to test: all 1s (java uses 2s complement, which means -1 is all 1s in binary)
        aDataPacket.addRawData(-1L, -1L, -1L, -1L, -1L, -1L);
        //3rd batch of data to test: set BBpr0 to -4 and BBPr1 to 1
        aDataPacket.addRawData(0xFFFF0000FFFF0000L, 0xFFFF0000FFFF0000L, 0xFFFF0000FFFF0000L,
                               0xFFFF0000FFFF0000L, 0xFFFF0000FFFF0000L, 0xFFFF0000FFFF0000L);
        //4th batch of data to test: Just D is 1
        aDataPacket.addRawData(-1L, -1L, 0L, 0L, 0L, 0L);
        //5th batch of data to test: Just C is 1
        aDataPacket.addRawData(0L, 0L, -1L, -1L, 0L, 0L);
        //6th batch of data to test: Just B is 1
        aDataPacket.addRawData(0L, 0L, 0L, 0L, -1L, -1L);
        //7th batch of data to test: D&C are 1
        aDataPacket.addRawData(-1L, -1L, -1L, -1L, 0, 0);
    }
    
    /* (non-Javadoc)
     * @see junit.framework.TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
//    /**
//     * Test method for {@link alma.Control.device.gui.common.widgets.util.DtsData.DataPacket#DataPacket()}.
//     */
//    public void testDataPacket() {
//        fail("Not yet implemented");
//    }
//    
//    /**
//     * Test method for {@link alma.Control.device.gui.common.widgets.util.DtsData.DataPacket#addRawData(long, long, long, long, long, long)}.
//     */
//    public void testAddRawData() {
//        fail("Not yet implemented");
//    }
//    
    /**
     * Test method for {@link alma.Control.device.gui.common.widgets.util.DtsData.DataPacket#getAlmaValuesPol0()}.
     */
    public void testGetAlmaValuesPol0() {
        byte[][] bbPr0 = null;
        try {
            bbPr0 = aDataPacket.getAlmaValuesPol0();
        } catch (InsufficientDataException e) {
            fail("Unexpected out of data exception!");
        }
        //Our 1st batch of input was all 0s, so all the values should come out to be -4.
        for (Integer c=0; c<4; c++){
            assertEquals(bbPr0[0].length, 16);
            for (Integer c2=0; c2<16; c2++)
                assertEquals("Failed on loop # "+c.toString()+","+c2.toString(), bbPr0[c][c2], -4);
        }
        //Our 2nd batch of input was all 1s, so all the values should come out to be 1
        for (Integer c=4; c<8; c++){
            assertEquals(bbPr0[0].length, 16);
            for (Integer c2=0; c2<16; c2++)
                assertEquals("Failed on loop # "+c.toString()+","+c2.toString(), bbPr0[c][c2], 1);
        }
        //Our 3rd batch of input set BBPr0 to all 0s, so all the values should come out to be -4.
        for (Integer c=8; c<12; c++){
            assertEquals(bbPr0[0].length, 16);
            for (Integer c2=0; c2<16; c2++)
                assertEquals("Failed on loop # "+c.toString()+","+c2.toString(), bbPr0[c][c2], -4);
        }
        //Our 4th batch of input set just D to 1, so all the values should come out to be 3.
        for (Integer c=12; c<16; c++){
            assertEquals(bbPr0[0].length, 16);
            for (Integer c2=0; c2<16; c2++)
                assertEquals("Failed on loop # "+c.toString()+","+c2.toString(), bbPr0[c][c2], 3);
        }
        //Our 5th batch of input set just C to 1, so all the values should come out to be -1.
        for (Integer c=16; c<20; c++){
            assertEquals(bbPr0[0].length, 16);
            for (Integer c2=0; c2<16; c2++)
                assertEquals("Failed on loop # "+c.toString()+","+c2.toString(), bbPr0[c][c2], -1);
        }
        //Our 6th batch of input set just B to 1, so all the values should come out to be -3.
        for (Integer c=20; c<24; c++){
            assertEquals(bbPr0[0].length, 16);
            for (Integer c2=0; c2<16; c2++)
                assertEquals("Failed on loop # "+c.toString()+","+c2.toString(), bbPr0[c][c2], -3);
        }
        //Our 7th batch of input set D&C to 1, so all the values should come out to be 0.
        for (Integer c=24; c<28; c++){
            assertEquals(bbPr0[0].length, 16);
            for (Integer c2=0; c2<16; c2++)
                assertEquals("Failed on loop # "+c.toString()+","+c2.toString(), bbPr0[c][c2], 0);
        }
    }

    /**
     * Test method for
     * {@link alma.Control.device.gui.common.widgets.util.DtsData.DataPacket#getAlmaValues1DPol0()}.
     */
    public void testGetAlmaValues1DPol0() {
        byte[] bbPr0 = null;
        try {
            bbPr0 = aDataPacket.getAlmaValues1DPol0();
        } catch (InsufficientDataException e) {
            fail("Unexpected out of data exception!");
        }
        //NOTE: the *16 is used because we are switched to a 1D array, not 2-D
        //Our 1st batch of input was all 0s, so all the values should come out to be -4.
        for (Integer c=0; c<4*16; c++)
            assertEquals("Failed on loop # "+c.toString(), bbPr0[c], -4);
        //Our 2nd batch of input was all 1s, so all the values should come out to be 1
        for (Integer c=4*16; c<8*16; c++)
            assertEquals("Failed on loop # "+c.toString(), bbPr0[c], 1);
        //Our 3rd batch of input set BBPr0 to all 0s, so all the values should come out to be -4.
        for (Integer c=8*16; c<12*16; c++)
            assertEquals("Failed on loop # "+c.toString(), bbPr0[c], -4);
        //Our 4th batch of input set just D to 1, so all the values should come out to be 3.
        for (Integer c=12*16; c<16*16; c++)
            assertEquals("Failed on loop # "+c.toString(), bbPr0[c], 3);
        //Our 5th batch of input set just C to 1, so all the values should come out to be -1.
        for (Integer c=16*16; c<20*16; c++)
            assertEquals("Failed on loop # "+c.toString(), bbPr0[c], -1);
        //Our 6th batch of input set just B to 1, so all the values should come out to be -3.
        for (Integer c=20*16; c<24*16; c++)
            assertEquals("Failed on loop # "+c.toString(), bbPr0[c], -3);
        //Our 7th batch of input set D&C to 1, so all the values should come out to be 0.
        for (Integer c=24*16; c<28*16; c++)
            assertEquals("Failed on loop # "+c.toString(), bbPr0[c], 0);
    }
    
    /**
     * Test method for {@link alma.Control.device.gui.common.widgets.util.DtsData.DataPacket#getAlmaValuesPol1()}.
     */
    public void testGetAlmaValuesPol1() {
        byte[][] bbPr1 = null;
        try {
            bbPr1 = aDataPacket.getAlmaValuesPol1();
        } catch (InsufficientDataException e) {
            fail("Unexpected out of data exception!");
        }
        //Our 1st batch of input was all 0s, so all the values should come out to be -4.
        for (Integer c=0; c<4; c++){
            assertEquals(bbPr1[0].length, 16);
            for (Integer c2=0; c2<16; c2++)
                assertEquals("Failed on loop # "+c.toString()+","+c2.toString(), bbPr1[c][c2], -4);
        }
        //Our 2nd batch of input was all 1s, so all the values should come out to be 1
        for (Integer c=4; c<8; c++){
            assertEquals(bbPr1[0].length, 16);
            for (Integer c2=0; c2<16; c2++)
                assertEquals("Failed on loop # "+c.toString()+","+c2.toString(), bbPr1[c][c2], 1);
        }
        //Our 3rd batch of input set BBPr1 to all 1s, so all the values should come out to be 1
        for (Integer c=8; c<12; c++){
            assertEquals(bbPr1[0].length, 16);
            for (Integer c2=0; c2<16; c2++)
                assertEquals("Failed on loop # "+c.toString()+","+c2.toString(), bbPr1[c][c2], 1);
        }
        //Our 4th batch of input set just D to 1, so all the values should come out to be 3.
        for (Integer c=12; c<16; c++){
            assertEquals(bbPr1[0].length, 16);
            for (Integer c2=0; c2<16; c2++)
                assertEquals("Failed on loop # "+c.toString()+","+c2.toString(), bbPr1[c][c2], 3);
        }
        //Our 5th batch of input set just C to 1, so all the values should come out to be -1.
        for (Integer c=16; c<20; c++){
            assertEquals(bbPr1[0].length, 16);
            for (Integer c2=0; c2<16; c2++)
                assertEquals("Failed on loop # "+c.toString()+","+c2.toString(), bbPr1[c][c2], -1);
        }
        //Our 4th batch of input set just D to 1, so all the values should come out to be 3.
        for (Integer c=20; c<24; c++){
            assertEquals(bbPr1[0].length, 16);
            for (Integer c2=0; c2<16; c2++)
                assertEquals("Failed on loop # "+c.toString()+","+c2.toString(), bbPr1[c][c2], -3);
        }
        //Our 7th batch of input set D&C to 1, so all the values should come out to be 0.
        for (Integer c=24; c<28; c++){
            assertEquals(bbPr1[0].length, 16);
            for (Integer c2=0; c2<16; c2++)
                assertEquals("Failed on loop # "+c.toString()+","+c2.toString(), bbPr1[c][c2], 0);
        }
    }
    
    /**
     * Test method for
     * {@link alma.Control.device.gui.common.widgets.util.DtsData.DataPacket#getAlmaValues1DPol0()}.
     */
    public void testGetAlmaValues1DPol1() {
        byte[] bbPr1 = null;
        try {
            bbPr1 = aDataPacket.getAlmaValues1DPol1();
        } catch (InsufficientDataException e) {
            fail("Unexpected out of data exception!");
        }
        //NOTE: the *16 is used because we are switched to a 1D array, not 2-D
        //Our 1st batch of input was all 0s, so all the values should come out to be -4.
        for (Integer c=0; c<4*16; c++)
            assertEquals("Failed on loop # "+c.toString(), bbPr1[c], -4);
        //Our 2nd batch of input was all 1s, so all the values should come out to be 1
        for (Integer c=4*16; c<8*16; c++)
            assertEquals("Failed on loop # "+c.toString(), bbPr1[c], 1);
        //Our 3rd batch of input set BBPr0 to all 0s, so all the values should come out to be -4.
        for (Integer c=8*16; c<12*16; c++)
            assertEquals("Failed on loop # "+c.toString(), bbPr1[c], 1);
        //Our 4th batch of input set just D to 1, so all the values should come out to be 3.
        for (Integer c=12*16; c<16*16; c++)
            assertEquals("Failed on loop # "+c.toString(), bbPr1[c], 3);
        //Our 5th batch of input set just C to 1, so all the values should come out to be -1.
        for (Integer c=16*16; c<20*16; c++)
            assertEquals("Failed on loop # "+c.toString(), bbPr1[c], -1);
        //Our 6th batch of input set just B to 1, so all the values should come out to be -3.
        for (Integer c=20*16; c<24*16; c++)
            assertEquals("Failed on loop # "+c.toString(), bbPr1[c], -3);
        //Our 7th batch of input set D&C to 1, so all the values should come out to be 0.
        for (Integer c=24*16; c<28*16; c++)
            assertEquals("Failed on loop # "+c.toString(), bbPr1[c], 0);
    }
    
//    
//    /**
//     * Test method for {@link alma.Control.device.gui.common.widgets.util.DtsData.DataPacket#getRawDataBLower()}.
//     */
//    public void testGetRawDataBLower() {
//        fail("Not yet implemented");
//    }
//    
//    /**
//     * Test method for {@link alma.Control.device.gui.common.widgets.util.DtsData.DataPacket#getRawDataBUpper()}.
//     */
//    public void testGetRawDataBUpper() {
//        fail("Not yet implemented");
//    }
//    
//    /**
//     * Test method for {@link alma.Control.device.gui.common.widgets.util.DtsData.DataPacket#getRawDataCLower()}.
//     */
//    public void testGetRawDataCLower() {
//        fail("Not yet implemented");
//    }
//    
//    /**
//     * Test method for {@link alma.Control.device.gui.common.widgets.util.DtsData.DataPacket#getRawDataCUpper()}.
//     */
//    public void testGetRawDataCUpper() {
//        fail("Not yet implemented");
//    }
//    
//    /**
//     * Test method for {@link alma.Control.device.gui.common.widgets.util.DtsData.DataPacket#getRawDataDLower()}.
//     */
//    public void testGetRawDataDLower() {
//        fail("Not yet implemented");
//    }
//    
//    /**
//     * Test method for {@link alma.Control.device.gui.common.widgets.util.DtsData.DataPacket#getRawDataDUpper()}.
//     */
//    public void testGetRawDataDUpper() {
//        fail("Not yet implemented");
//    }
    
    /**
     * Test method for {@link alma.Control.device.gui.common.widgets.util.DtsData.DataPacket#getBit(int, int)}
     */
    public void testGetBit(){
        assertTrue(aDataPacket.getBit(1L, 0));
        assertTrue(aDataPacket.getBit(Long.MIN_VALUE, 63));
        assertTrue(aDataPacket.getBit(0x1000L, 12));
        assertTrue(aDataPacket.getBit(-1L, 0));
        assertFalse(aDataPacket.getBit(0L, 23));
        assertFalse(aDataPacket.getBit(Long.MAX_VALUE, 63));
        assertFalse(aDataPacket.getBit(-2L, 0));
        assertFalse(aDataPacket.getBit(0xF7FFFFFFL, 27));
    }
    
    /**
     * Test method for {@link alma.Control.device.gui.common.widgets.util.DtsData.DataPacked#valueFromGraycode(boolean, boolean. boolean)}
     * Insures that the value is unpacked according to BEND-50.00.00.00-306-A-MAN
     * Since there are only 8 possible input combinations we just test them all!
     */
    public void testValueFromGrayCode(){
        assertEquals(-4, aDataPacket.valueFromGrayCode(false, false, false));
        assertEquals(-3, aDataPacket.valueFromGrayCode(false, false, true ));
        assertEquals(-2, aDataPacket.valueFromGrayCode(false, true,  true ));
        assertEquals(-1, aDataPacket.valueFromGrayCode(false, true,  false));
        assertEquals( 0, aDataPacket.valueFromGrayCode(true,  true,  false));
        assertEquals( 1, aDataPacket.valueFromGrayCode(true,  true,  true ));
        assertEquals( 2, aDataPacket.valueFromGrayCode(true,  false, true ));
        assertEquals( 3, aDataPacket.valueFromGrayCode(true,  false, false));
    }
    private DataPacket aDataPacket;
}
