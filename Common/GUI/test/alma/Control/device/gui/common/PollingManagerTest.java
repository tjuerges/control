/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/** 
 * Test the PollingManager class with a simplified typical case.
 * 
 * TODO: Convert to a junit test.
 * 
 * @author  Scott Rankin  srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 5.0.4
 */
public class PollingManagerTest implements Pollable {

	private static String currentTime() {
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SSSS");
		Date date = new Date();
		return dateFormat.format(date);
	}

	public static void main(String[] args) {
		System.out.println("main() called.");
		
		PollingManagerTest pmt = new PollingManagerTest();
		PollingManager pm = new PollingManager(pmt);
		
		//
		// Let polling manager poll for at least 55 seconds, then stop it cleanly.
		// After 35 seconds, change the slow polling period from the default to 6 seconds.
		pm.startPolling();
		Date start = new Date();
		System.out.println("main() started polling manager at: " + currentTime());
		Date stop = new Date(start.getTime() + 55 * 1000);
		Date changeSample = new Date(start.getTime() + 35 * 1000);
		System.out.println("main() will stop polling manager after: " + stop);
		Date now = new Date();
		while (now.getTime() < stop.getTime()) {
			if (now.getTime() > changeSample.getTime())
				pm.setSlowPollingPeriod(6);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				System.out.println("main() interrupted for unknown reason in 1st loop");
				e.printStackTrace();
			}
			now = new Date();
		}
		
		//
		// Stop polling manager cleanly.
		pm.stopPolling();
		System.out.println("main() stopped polling manager at: " + currentTime());
		while (pm.nowPolling()) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				System.out.println("main() interrupted for unknown reason in 2st loop");
				e.printStackTrace();
			}
		}
		
		System.out.println("main(args) ended.");
	}
	
	public void pollSlowMonitorPoints() {
		System.out.println("pollSlowMonitorPoints() called at: " + currentTime());
	}

	public void pollFastMonitorPoints() {
		System.out.println("pollFastMonitorPoints() called at:" + currentTime());
	}
	
}

//
// O_o
