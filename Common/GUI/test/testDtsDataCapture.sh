#! /bin/sh
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# dhunter  7 May 2010  created
#

#************************************************************************
#   NAME
#
#   SYNOPSIS
#     Test the DTS data capture subsystem.
#
#   DESCRIPTION
#
#     This calles a serious of JUnit tests that test various aspects of the DTS data capture system. These
#     tests should be used and updated when the related code is changed. They are not intended to be part
#     of the automatic tests.
#
#   FILES
#
#   ENVIRONMENT
#
#   RETURN VALUES
#
#   CAUTIONS
#
#   EXAMPLES
#
#   SEE ALSO
#
#   BUGS
#
#------------------------------------------------------------------------
#


acsStartJava junit.textui.TestRunner alma.Control.device.gui.common.widgets.util.DtsData.TestDataPacket
acsStartJava junit.textui.TestRunner alma.Control.device.gui.common.widgets.util.DtsData.TestDataStatistics

# signal trap (if any)

#
# ___oOo___

