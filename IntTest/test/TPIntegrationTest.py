#!/usr/bin/env python
# @(#) $Id$
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2008 - 2011
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

import sys
import unittest
import math
import Acspy.Clients.SimpleClient
import Acspy.Common.ErrorTrace
import Acspy.Common.Log
import TMCDB_IDL;
import asdmIDLTypes;
import maciErrTypeImpl
import Control
import TestingUtils
import ObservingModeExceptions
import ControlExceptions
import ModeControllerExceptions
import CCL.ScanIntent, ScanIntentMod, SubscanIntentMod
import CalibrationDeviceMod
import CCL.SpectralSpec, CCL.FieldSource
import CCL.Array, CCL.Global

# Add this here as this interface is not always imported by
# $MODROOT/lib/python/site-packages/Control/__init__.py (depending on
# how things are built).  It needs to be imported to get a reference
# to the CONTROL/MASTER component.
import ControlMasterIF_idl

subscanIntents = [SubscanIntentMod.ON_SOURCE]
scanIntents = CCL.ScanIntent.ScanIntent(ScanIntentMod.OBSERVE_TARGET)
scanIntents.setCalibrationDataOrigin('TOTAL_POWER');

# **************************************************
# automated test class
# **************************************************

class automated( unittest.TestCase ):

    def test01( self ):
        '''
        This simply starts the total-power offshoot and shuts it down
        again. Unlike the unit tests in the Array module this uses the
        real control system components (components from other
        subsystems are simulated). It checks the basic lifecycle of
        the total-power observing mode.
        '''
        try:
            arrayName = None
            array = None
            try:
                arrayId = master.createAutomaticArray(\
                    master.getAvailableAntennas(),
                    master.getAvailablePhotonicReferences(),
                    Control.NONE);
                arrayName = arrayId.arrayComponentName
                array = client.getComponent(arrayName)
                self.failUnless(array != None,
                                "Unable to create a automatic array")
                tp = array.getTotalPowerOffshoot()
                self.failUnless(tp != None,
                                "Unable to create a TP observing mode")
            finally:
                if (array != None): client.releaseComponent(arrayName)
                if (arrayName != None): master.destroyArray(arrayName);
        except (ObservingModeExceptions.ObsModeInitErrorEx,
                ControlExceptions.IllegalParameterErrorEx,
                maciErrTypeImpl.CannotGetComponentExImpl), ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace)\
            .log(Acspy.Common.Log.getLogger());
            raise

    def test02( self ):
        '''
        This tests the IDL interface of the total-power observing
        mode. The ability to create this interface is tested in test01
        and here we test its functionality.
        '''
        try:
            arrayName = None
            scheduling = None
            array = None
            try:
                sbId = TestingUtils.setupProject()
                arrayId = master.createManualArray(\
                    master.getAvailableAntennas(),
                    master.getAvailablePhotonicReferences(),
                    Control.NONE);
                arrayName = arrayId.arrayComponentName
                schedulingName = 'SCHEDULING/'+arrayId.arrayName
                scheduling = client.getComponent(schedulingName,
                                                 comp_idl_type="IDL:alma/scheduling/Array:1.0",
                                                 is_dynamic=True)
                TestingUtils.configureScheduling(schedulingName);
                array = client.getComponent(arrayName)
                self.failUnless(array != None,
                                "Unable to create a manual array")
                array.configure(asdmIDLTypes.IDLEntityRef(sbId, "", "", ""))

                array.beginExecution()
                tp = array.getTotalPowerOffshoot()
                self.failUnless(tp != None,
                                "Unable to create a TP observing mode")

                loc = tp.getLocalOscillator()
                self.failUnless(loc != None,
                         "Unable to get a reference to the LO mode controller")

                amc = tp.getArrayMountController()
                self.failUnless(amc != None,
                         "Unable to get a reference to the array mount controller")
                tp.beginScan([scanIntents.getScanIntentData()])
                ss = CCL.SpectralSpec.SpectralSpec().CreateBLCorrelatorSpec()
                ss.BLCorrelatorConfiguration = None
                ss.SquareLawSetup = CCL.APDMSchedBlock.SquareLawSetup()
                ss.SquareLawSetup.integrationDuration.set('1ms')

                source1 = CCL.FieldSource.EquatorialSource("2:0:0", "-80.0.0");
                spectrum1 =  CCL.SpectralSpec.SpectralSpec().CreateBLCorrelatorSpec(baseband = ['BB_1', 'BB_2', 'BB_3', 'BB_4'],
                                                                                    basebandFrequency=["90.01GHz", "91.99GHz", "102.01GHz", "103.99GHz"])
                spectrum1.BLCorrelatorConfiguration = None
                spectrum1.SquareLawSetup = CCL.APDMSchedBlock.SquareLawSetup()
                spectrum1.SquareLawSetup.integrationDuration.set('1ms')
                spectrum2 =  CCL.SpectralSpec.SpectralSpec().CreateBLCorrelatorSpec(baseband = ['BB_1', 'BB_4'],
                                                                                    basebandFrequency=["220GHz", "234GHz"])
                spectrum2 =  CCL.SpectralSpec.SpectralSpec().CreateBLCorrelatorSpec(baseband = ['BB_1', 'BB_3'],
                                                                                    basebandFrequency=["92GHz", "105GHz"])
                spectrum2.BLCorrelatorConfiguration = None
                spectrum2.SquareLawSetup = CCL.APDMSchedBlock.SquareLawSetup()
                spectrum2.SquareLawSetup.integrationDuration.set('2ms')
                offset1 = Control.SourceOffset(0, 0, 0, 0, Control.HORIZON)
                offset2 = Control.SourceOffset(math.radians(1), 0, 0, 0, Control.HORIZON)
                offset3 = Control.SourceOffset(math.radians(-1), 0, 0, 0, Control.HORIZON)
                offset4 = Control.SourceOffset(0, math.radians(1), 0, 0, Control.HORIZON)
                offset5 = Control.SourceOffset(0, math.radians(-1), 0, 0, Control.HORIZON)
                onsrc = [SubscanIntentMod.ON_SOURCE]
                offsrc = [SubscanIntentMod.OFF_SOURCE]
                d3 =  long(3/100E-9)
                d6 = long(6/100E-9)
                subscan0 = Control.SubscanSpec(onsrc, 0, 0, d6,
                           [Control.ArrayOffset(offset1, "Default")],
                           offset1, [], CalibrationDeviceMod.HOT_LOAD, -1)
                subscan1 = Control.SubscanSpec(onsrc, -1, -1, d3,
                           [], offset1, [], CalibrationDeviceMod.COLD_LOAD, -1)
                subscan2 = Control.SubscanSpec(offsrc, -1, 1, d3,
                           [Control.ArrayOffset(offset2, "Default")],
                           offset1, [], CalibrationDeviceMod.AMBIENT_LOAD, -1)
                subscan3 = Control.SubscanSpec(offsrc, -1, -1, d3,
                           [Control.ArrayOffset(offset3, "Default")],
                           offset1, [], CalibrationDeviceMod.NONE, -1)
                subscan4 = Control.SubscanSpec(offsrc, -1, 0, d3,
                           [Control.ArrayOffset(offset4, "Default")],
                           offset1, [], CalibrationDeviceMod.NONE, -1)
                subscan5 = Control.SubscanSpec(offsrc, -1, -1, d3,
                           [Control.ArrayOffset(offset5, "Default")],
                           offset1, [], CalibrationDeviceMod.NONE, -1)
                fivePoint = Control.SubscanSequenceSpec([source1.getXMLString()],
                            [spectrum1.getXMLString(), spectrum2.getXMLString()],
                            [subscan0, subscan1, subscan1, subscan1,
                             subscan2, subscan3, subscan4, subscan5])

                tp.doSubscanSequence(fivePoint)

                # Test the setTarget and doSubscan functions
                fs = CCL.FieldSource.EquatorialSource("2:0:0", "-89.0.0");
                tp.setTarget(fs.getXMLString(), ss.getXMLString())
                tp.doSubscan(long(10.08/100E-9), subscanIntents);
                tp.endScan()

                amc.stop();
                array.endExecution(Control.SUCCESS, \
                                   "Manual mode session completed.")
            finally:
                if (array != None): client.releaseComponent(arrayName)
                if (scheduling != None): client.releaseComponent(schedulingName)
                if (arrayName != None): master.destroyArray(arrayName);
        except (ObservingModeExceptions.ObsModeInitErrorEx,
                ObservingModeExceptions.SubScanDataFlowErrorEx,
                ObservingModeExceptions.ObservingModeErrorEx,
                ObservingModeExceptions.CorrelatorErrorEx,
                ControlExceptions.IllegalParameterErrorEx,
                maciErrTypeImpl.CannotGetComponentExImpl,
                ModeControllerExceptions.HardwareFaultEx), ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace)\
            .log(Acspy.Common.Log.getLogger());
            raise

    def test03( self ):
        '''
        This tests the CCL interface.
        '''
        pass
    
    def test04( self ):
        '''
        This tests the TP Observing mode as used by the code in the
        directories below the SSR module.
        '''
        import Observation.PointingCalTarget
        import Observation.FocusCalTarget
        import Observation.ScienceTarget
        try:
            arrayId = None
            scheduling = None
            array = None
            try:
                sbId = TestingUtils.setupProject()
                TestingUtils.configureObservationControl()
                arrayId = master.createManualArray(\
                    master.getAvailableAntennas(),
                    master.getAvailablePhotonicReferences(),
                    Control.BL);
                schedulingName = 'SCHEDULING/'+arrayId.arrayName
                scheduling = client.getComponent(schedulingName,
                                                 comp_idl_type="IDL:alma/scheduling/Array:1.0",
                                                 is_dynamic=True)
                TestingUtils.configureScheduling(schedulingName);
                array = CCL.Array.Array(arrayId.arrayName);
                CCL.Global.setArrayName(arrayId.arrayName)
            
                self.failUnless(array != None,
                                "Unable to create a manual array")
                array._array.configure(asdmIDLTypes.IDLEntityRef(sbId, "", "", ""))
                array.beginExecution()

                # Create a source and spectral spec.
                src = CCL.FieldSource.EquatorialSource("2:0:0", "-89.0.0");
                ss = CCL.SpectralSpec.SpectralSpec().CreateBLCorrelatorSpec()
                ss.SquareLawSetup = CCL.APDMSchedBlock.SquareLawSetup()
                ss.SquareLawSetup.integrationDuration.set('1ms')

                # Create and configure a pointing cal target
                pt = Observation.PointingCalTarget.PointingCalTarget(src,ss)
                pt.setSubscanDuration(4)
                pt.setDataOrigin('TOTAL_POWER')

                # Run the pointing scan
                pt.execute(array.getTotalPowerObservingMode())

                # Run a focus scan
                ft = Observation.FocusCalTarget.FocusCalTarget(src,ss)
                ft.setSubscanDuration(7)
                ft.execute(array.getTotalPowerObservingMode())

                # Run a science target
                src.Reference.append(CCL.APDMSchedBlock.Reference())
                src.Reference[0].setCoordinates("-1deg", "1deg")
                src.Reference[0].cycleTime.set("30s")
                src.Reference[0].subScanDuration.set("6s");
                src.Reference[0].integrationTime.set("0.15min");
                st = Observation.ScienceTarget.ScienceTarget(src,ss)
                st.setIntegrationTime(7)
                st.setSubscanDuration(3)
                st.execute(array.getTotalPowerObservingMode())

                array.endExecution(Control.SUCCESS, \
                                   "Manual mode session completed.")
            finally:
                if (scheduling != None): client.releaseComponent(schedulingName)
                if (arrayId != None): master.destroyArray(arrayId.arrayName);
        except (ObservingModeExceptions.ObsModeInitErrorEx,
                ObservingModeExceptions.SubScanDataFlowErrorEx,
                ObservingModeExceptions.ObservingModeErrorEx,
                ObservingModeExceptions.CorrelatorErrorEx,
                ControlExceptions.IllegalParameterErrorEx,
                maciErrTypeImpl.CannotGetComponentExImpl,
                ModeControllerExceptions.HardwareFaultEx), ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace)\
            .log(Acspy.Common.Log.getLogger());
            raise

    
        
# **************************************************
# MAIN Program
# **************************************************
if __name__ == '__main__':

# Bring the control subsystem operational
    client = Acspy.Clients.SimpleClient.PySimpleClient()
    master = client.getComponent('CONTROL/MASTER')
    shutdownMasterAtEnd = False;
    if (master.getMasterState() == Control.INACCESSIBLE):
        master.startupPass1()
        master.startupPass2()
        shutdownMasterAtEnd = True;
    if (master.getMasterState() != Control.OPERATIONAL):
        print 'Could not bring the control subsystem to an operational state'
        exit(1)

# Now run the tests
    runner = unittest.TextTestRunner()
    if len( sys.argv ) > 1:
        suite = unittest.makeSuite(automated, sys.argv[1])
    else:
        suite = unittest.makeSuite(automated)
    runner.run(suite)

# Shutdown the control system if we started it up
    if shutdownMasterAtEnd:
        master.shutdownPass1()
        master.shutdownPass2()
    client.releaseComponent(master._get_name()) 
