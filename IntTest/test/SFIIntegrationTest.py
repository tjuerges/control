#!/usr/bin/env python
# @(#) $Id$
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2007 - 2010
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

import sys, math
import unittest
import Acspy.Clients.SimpleClient
import Acspy.Common.ErrorTrace
import Acspy.Common.Log
import xmlentity
import SubscanIntentMod
import asdmIDLTypes;
import TMCDB_IDL;
import maciErrTypeImpl
import ObservingModeExceptions
import ControlExceptions
import ModeControllerExceptions
import Control
import CalibrationDeviceMod
import CCL.SpectralSpec, CCL.FieldSource
import CCL.ScanIntent, ScanIntentMod
import TestingUtils
import CCL.Array
import CCLExceptionsImpl
import CCL.Global

# Add this here as this interface is not always imported by
# $MODROOT/lib/python/site-packages/Control/__init__.py (depending on
# how things are built).  It needs to be imported to get a reference
# to the CONTROL/MASTER component.
import ControlMasterIF_idl

subscanIntents = [SubscanIntentMod.ON_SOURCE]
scanIntents = CCL.ScanIntent.ScanIntent(ScanIntentMod.OBSERVE_TARGET)
scanIntents.setCalibrationDataOrigin('FULL_RESOLUTION_CROSS');

# **************************************************
# automated test class
# **************************************************

class automated( unittest.TestCase ):

    def test01( self ):
        '''
        This simply starts the SFI offshoot and shuts it down
        again. Unlike the unit tests in the Array module this uses the
        real control system components (components from other
        subsystems are simulated). It checks the basic lifecycle of
        the SFI observing mode. This test uses an automatic array.
        '''
        try:
            arrayName = None
            array = None
            try:
                arrayId = master.createAutomaticArray(\
                    master.getAvailableAntennas(),
                    master.getAvailablePhotonicReferences(),
                    Control.BL);
                arrayName = arrayId.arrayComponentName
                array = client.getComponent(arrayName)
                self.failUnless(array != None,
                                "Unable to create a automatic array")
                sfi = array.getSFIOffshoot()
                self.failUnless(sfi != None,
                                "Unable to create an SFI observing mode")
            finally:
                if (array != None): client.releaseComponent(arrayName)
                if (arrayName != None): master.destroyArray(arrayName);
        except (ObservingModeExceptions.ObsModeInitErrorEx,
                ControlExceptions.IllegalParameterErrorEx,
                maciErrTypeImpl.CannotGetComponentExImpl), ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace)\
            .log(Acspy.Common.Log.getLogger());
            raise

    def test02( self ):
        '''
        This tests the IDL interface of the single field
        interferometry observing mode. The ability to create this
        interface is tested in test01 and here we test its
        functionality.
        '''
        try:
            arrayId = None
            scheduling = None
            array = None
            try:
                sbId = TestingUtils.setupProject()
                TestingUtils.configureObservationControl()
                arrayId = master.createManualArray(\
                    master.getAvailableAntennas(),
                    master.getAvailablePhotonicReferences(),
                    Control.BL);
                schedulingName = 'SCHEDULING/'+arrayId.arrayName
                scheduling = client.getComponent(schedulingName,
                                                 comp_idl_type="IDL:alma/scheduling/Array:1.0",
                                                 is_dynamic=True)
                TestingUtils.configureScheduling(schedulingName);
                array = client.getComponent(arrayId.arrayComponentName)
                
                self.failUnless(array != None,
                                "Unable to create a manual array")
                array.configure(asdmIDLTypes.IDLEntityRef(sbId, "", "", ""))
                array.beginExecution()
                sfi = array.getSFIOffshoot()
                self.failUnless(sfi != None,
                                "Unable to create an SFI observing mode")

                loc = sfi.getLocalOscillator()
                self.failUnless(loc != None,
                         "Unable to get a reference to the LO mode controller")

                amc = sfi.getArrayMountController()
                self.failUnless(amc != None,
                                "Unable to get a reference to the array mount controller")
                sfi.beginScan([scanIntents.getScanIntentData()])

                # Test the setPointingDirection function
                fs = CCL.FieldSource.EquatorialSource("2:0:0", "-89.0.0");

                planets = ['Io', 'Callisto', 'Ganymede',
                           'Ceres', 'Pallas', 'Juno', 'Vesta',
                           'mercury', 'venus', 'mars', 'jupiter',
                           'saturn', 'uranus', 'neptune', 'pluto', 'sun', 'moon']
                i = 0; doPlanet = None
                while (doPlanet is None) and (i < len(planets)):
                    cfs = CCL.FieldSource.PlanetSource(planets[i]);
                    if (sfi.isObservable(cfs.getXMLString(), 300)):
                        doPlanet = planets[i]
                    i = i + 1
                if (doPlanet is not None):
                    fs = CCL.FieldSource.PlanetSource(doPlanet);
        
                sfi.setPointingDirection(fs.getXMLString());
                sfi.setPhaseDirection(fs.getXMLString());

                # Test the setSpectrum function
                ss = CCL.SpectralSpec.SpectralSpec().CreateBLCorrelatorSpec()
                sfi.setSpectralSpec(ss.getXMLString());

                # Test the setTarget function
                sfi.setTarget(fs.getXMLString(), ss.getXMLString());

                # Test the doSubscan function
                sfi.doSubscan(long(4.8/100E-9), subscanIntents);

                source1 = fs
                spectrum1 = ss
                spectrum1 = CCL.SpectralSpec.SpectralSpec().CreateBLCorrelatorSpec(baseband = ['BB_1', 'BB_2', 'BB_3', 'BB_4'],
                                                                                    basebandFrequency=["86.01GHz", "87.99GHz", "98.01GHz", "99.99GHz"])
                spectrum1.FrequencySetup.dopplerReference = 'rest'

                spectrum2 =  CCL.SpectralSpec.SpectralSpec().CreateBLCorrelatorSpec(baseband = ['BB_1', 'BB_3'],
                                                                                    basebandFrequency=["92GHz", "105GHz"])

                spectrum2.FrequencySetup.dopplerReference = 'lsrk'
                offset1 = Control.SourceOffset(0, 0, 0, 0, Control.HORIZON)
                offset2 = Control.SourceOffset(math.radians(1), 0, 0, 0, Control.HORIZON)
                offset3 = Control.SourceOffset(math.radians(-1), 0, 0, 0, Control.HORIZON)
                offset4 = Control.SourceOffset(0, math.radians(1), 0, 0, Control.HORIZON)
                onsrc = [SubscanIntentMod.ON_SOURCE]
                offsrc = [SubscanIntentMod.OFF_SOURCE]
                d12 =  long(12/100E-9)
                d6 = long(6/100E-9)

                subscan1 = Control.SubscanSpec(onsrc, 0, 0, d12,
                           [Control.ArrayOffset(offset1, "Default")],
                           offset1, [],  CalibrationDeviceMod.NONE, 0)
                subscan2 = Control.SubscanSpec(offsrc, -1, 1, d6,
                           [Control.ArrayOffset(offset2, "Default")],
                           offset1, [],  CalibrationDeviceMod.NONE, 0)
                subscan3 = Control.SubscanSpec(offsrc, -1, -1, d6,
                           [Control.ArrayOffset(offset3, "Default")],
                           offset1, [],  CalibrationDeviceMod.NONE, 0)
                subscan4 = Control.SubscanSpec(offsrc, -1, -1, d6,
                           [Control.ArrayOffset(offset4, "Default")],
                           offset1, [],  CalibrationDeviceMod.NONE, 0)
                subscan5 = Control.SubscanSpec(offsrc, -1, -1, d6,
                           [Control.ArrayOffset(offset4, "Default")],
                           offset1, [],  CalibrationDeviceMod.NONE, 0)

                fivePoint = Control.SubscanSequenceSpec([source1.getXMLString()],
                            [spectrum1.getXMLString(), spectrum2.getXMLString()],
                            [subscan1, subscan2, subscan3, subscan4, subscan5])

                sfi.doSubscanSequence(fivePoint)
                sfi.endScan()

                amc.stop();
                array.endExecution(Control.SUCCESS, \
                                   "Manual mode session completed.")
            finally:
                if (array != None): client.releaseComponent(arrayId.arrayComponentName)
                if (scheduling != None): client.releaseComponent(schedulingName)
                if (arrayId != None): master.destroyArray(arrayId.arrayName);
        except (ObservingModeExceptions.ObsModeInitErrorEx,
                ObservingModeExceptions.SubScanDataFlowErrorEx,
                ObservingModeExceptions.ObservingModeErrorEx,
                ObservingModeExceptions.CorrelatorErrorEx,
                ControlExceptions.IllegalParameterErrorEx,
                maciErrTypeImpl.CannotGetComponentExImpl,
                ModeControllerExceptions.HardwareFaultEx), ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace)\
            .log(Acspy.Common.Log.getLogger());
            raise

    def test03( self ):
        '''
        This tests the CCL interface of the single field
        interferometry observing mode
        '''
        try:
            arrayId = None
            scheduling = None
            array = None
            try:
                sbId = TestingUtils.setupProject()
                TestingUtils.configureObservationControl()
                arrayId = master.createManualArray(\
                    master.getAvailableAntennas(),
                    master.getAvailablePhotonicReferences(),
                    Control.BL);
                schedulingName = 'SCHEDULING/'+arrayId.arrayName
                scheduling = client.getComponent(schedulingName,
                                                 comp_idl_type="IDL:alma/scheduling/Array:1.0",
                                                 is_dynamic=True)
                TestingUtils.configureScheduling(schedulingName);
                array = CCL.Array.Array(arrayId.arrayName);
                self.failUnless(array != None,
                                "Unable to create a manual array")
                array._array.configure(asdmIDLTypes.IDLEntityRef(sbId, "", "", ""))

                array.beginExecution()
                sfi = array.getSingleFieldInterferometryObservingMode()
                self.failUnless(sfi != None,
                                "Unable to create an SFI observing mode")
    
                loc = sfi.getLOObservingMode()
                self.failUnless(loc != None,
                                "Unable to get a reference to the LO mode controller")
    
                amc = sfi.getArrayMountController()
                self.failUnless(amc != None,
                                "Unable to get a reference to the array mount controller")
                sfi.beginScan(scanIntents)

               # Test the setTarget and doSubscan functions
                fs = CCL.FieldSource.EquatorialSource("2:0:0", "-89.0.0");
                ss = CCL.SpectralSpec.SpectralSpec().CreateBLCorrelatorSpec()

                sfi.setTarget(fs, ss)
                sfi.doSubscan(9.6, subscanIntents);

                sfi.endScan()

                amc.stop();
                array.endExecution(Control.SUCCESS, \
                                   "Manual mode session completed.")
            finally:
                if (scheduling != None): client.releaseComponent(schedulingName)
                if (arrayId != None): master.destroyArray(arrayId.arrayName);
        except (CCLExceptionsImpl.ControllerErrorExImpl,
                ObservingModeExceptions.ObsModeInitErrorEx,
                ObservingModeExceptions.SubScanDataFlowErrorEx,
                ObservingModeExceptions.ObservingModeErrorEx,
                ObservingModeExceptions.CorrelatorErrorEx,
                ControlExceptions.IllegalParameterErrorEx,
                maciErrTypeImpl.CannotGetComponentExImpl,
                ModeControllerExceptions.HardwareFaultEx), ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace)\
            .log(Acspy.Common.Log.getLogger());
            raise

    def test04( self ):
        '''
        This tests the SFI Observing mode as used by the code in the
        directories below the SSR module.
        '''
        import Observation.PointingCalTarget
        import Observation.FocusCalTarget
        import Observation.ScienceTarget
        try:
            arrayId = None
            scheduling = None
            array = None
            try:
                sbId = TestingUtils.setupProject()
                TestingUtils.configureObservationControl()
                arrayId = master.createManualArray(\
                    master.getAvailableAntennas(),
                    master.getAvailablePhotonicReferences(),
                    Control.BL);
                schedulingName = 'SCHEDULING/'+arrayId.arrayName
                scheduling = client.getComponent(schedulingName,
                                                 comp_idl_type="IDL:alma/scheduling/Array:1.0",
                                                 is_dynamic=True)
                TestingUtils.configureScheduling(schedulingName);
                array = CCL.Array.Array(arrayId.arrayName);
                CCL.Global.setArrayName(arrayId.arrayName)
        
                self.failUnless(array != None,
                                "Unable to create a manual array")
                array._array.configure(asdmIDLTypes.IDLEntityRef(sbId, "", "", ""))
                array.beginExecution()

                # Create a source and spectral spec
                src = CCL.FieldSource.EquatorialSource("2:0:0", "-89.0.0");
                ss = CCL.SpectralSpec.SpectralSpec().CreateBLCorrelatorSpec()
                # Create and configure a pointing cal target
                pt = Observation.PointingCalTarget.PointingCalTarget(src,ss)
                pt.setSubscanDuration(9.6)
                pt.setReferenceAntennas(array.antennas()[0])
                pt.setDataOrigin('CHANNEL_AVERAGE_CROSS')

                # Run the pointing scan
                pt.execute(array.getSingleFieldInterferometryObservingMode())

                # Run a focus scan
                ft = Observation.FocusCalTarget.FocusCalTarget(src,ss);
                ft.setSubscanDuration(9.6);
                ft.execute(array.getSingleFieldInterferometryObservingMode())

                # Run a science target
                src.Reference.append(CCL.APDMSchedBlock.Reference())
                src.Reference[0].setCoordinates("-1deg", "1deg")
                src.Reference[0].cycleTime.set("30s")
                src.Reference[0].subScanDuration.set("6s");
                src.Reference[0].integrationTime.set("0.15min");
                st = Observation.ScienceTarget.ScienceTarget(src, ss)
                st.setIntegrationTime(15)
                st.setSubscanDuration(5)
                st.execute(array.getSingleFieldInterferometryObservingMode())

                array.endExecution(Control.SUCCESS, \
                                   "Manual mode session completed.")
            finally:
                if (scheduling != None): client.releaseComponent(schedulingName)
                if (arrayId != None): master.destroyArray(arrayId.arrayName);
        except (ObservingModeExceptions.ObsModeInitErrorEx,
                ObservingModeExceptions.SubScanDataFlowErrorEx,
                ObservingModeExceptions.ObservingModeErrorEx,
                ObservingModeExceptions.CorrelatorErrorEx,
                ControlExceptions.IllegalParameterErrorEx,
                maciErrTypeImpl.CannotGetComponentExImpl,
                ModeControllerExceptions.HardwareFaultEx), ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace)\
            .log(Acspy.Common.Log.getLogger());
            raise

# **************************************************
# MAIN Program
# **************************************************
if __name__ == '__main__':

# Bring the control subsystem operational
    client = Acspy.Clients.SimpleClient.PySimpleClient()
    master = client.getComponent('CONTROL/MASTER')
    shutdownMasterAtEnd = False;
    if (master.getMasterState() == Control.INACCESSIBLE):
        master.startupPass1()
        master.startupPass2()
        shutdownMasterAtEnd = True;
    if (master.getMasterState() != Control.OPERATIONAL):
        print 'Could not bring the control subsystem to an operational state'
        exit(1)

# This component needs to be running when creating a manual array

# Now run the tests
    runner = unittest.TextTestRunner()
    if len( sys.argv ) > 1:
        suite = unittest.makeSuite(automated, sys.argv[1])
    else:
        suite = unittest.makeSuite(automated)
    runner.run(suite)

# Shutdown the control system if we started it up
    if shutdownMasterAtEnd:
        master.shutdownPass1()
        master.shutdownPass2()
    client.releaseComponent(master._get_name()) 
