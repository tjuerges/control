/* @(#) $Id$
 *
 * Copyright (C) 2004, 2005
 * Associated Universities, Inc. Washington DC, USA.
 *
 * Produced for the ALMA project
 *
 * This library is free software; you can redistribute it and/or modify it it 
 * under the terms of the GNU Library General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is distributed in the hope that it will be useful but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public 
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License 
 * along with this library; if not, write to the Free Software Foundation, 
 * Inc., 675 Massachusetts Ave, Cambridge, MA, 02139, USA.
 *
 * Correspondence concerning ALMA should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */

#include <iostream>
#include <TETimeUtil.h>
#include <time.h>
using namespace std;

int main(int argc, char** argv)
{
    time_t mark = static_cast<time_t>(1123565937);
    acstime::Epoch ep = TETimeUtil::unix2epoch(mark);
    cout << "epoch:         " << ep.value << endl;
    cout << "nearest:       " << TETimeUtil::nearestTE(ep).value << endl;
    cout << "floor:         " << TETimeUtil::floorTE(ep).value << endl;
    cout << "ceil:          " << TETimeUtil::ceilTE(ep).value << endl;
    cout << "plus 5:        " << TETimeUtil::plusTE(ep, 5).value << endl;
    cout << "rtControlTime: " << TETimeUtil::rtControlTime(ep, 1).value <<
        endl;
    cout << "rtMonitorTime: " << TETimeUtil::rtMonitorTime(ep, 1).value <<
        endl;
}
