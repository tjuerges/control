#!/usr/bin/env python
# @(#) $Id$
#
# Copyright (C) 2004, 2005
# Associated Universities, Inc. Washington DC, USA.
#
# Produced for the ALMA project
#
# This library is free software; you can redistribute it and/or modify it it 
# under the terms of the GNU Library General Public License as published by 
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful but WITHOUT 
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public 
# License for more details.
#
# You should have received a copy of the GNU Library General Public License 
# along with this library; if not, write to the Free Software Foundation, 
# Inc., 675 Massachusetts Ave, Cambridge, MA, 02139, USA.
#
# Correspondence concerning ALMA should be addressed as follows:
# Internet email: alma-sw-admin@nrao.edu
#

import TETimeUtil

ep = TETimeUtil.unix2epoch(1123565937)

print "epoch:         %d" % ep.value
print "nearest:       %d" % TETimeUtil.nearestTE(ep).value
print "floor:         %d" % TETimeUtil.floorTE(ep).value
print "ceil:          %d" % TETimeUtil.ceilTE(ep).value
print "plus 5:        %d" % TETimeUtil.plusTE(ep, 5).value
print "rtControlTime: %d" % TETimeUtil.rtControlTime(ep, 1).value
print "rtMonitorTime: %d" % TETimeUtil.rtMonitorTime(ep, 1).value
