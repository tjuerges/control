#ifndef TIMEUTIL_H
#define TIMEUTIL_H
/*****************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2004
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free oftware
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * "@(#) $Id$"
 */
////////////////////////////////////////////////////////////////////////
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <acstimeC.h>
#include <acscommonC.h>
#include <time.h>

class EpochHelper;
class DurationHelper;

/** @file TETimeUtil.h
 *  Header file TETimeUtil class.
 */

/**
 * @class TETimeUtil TETimeUtil is a utility class providing static
 * methods to convert between various time systems, and provide common
 * timing-event related functionality.
 *
 */
class TETimeUtil
{
  public:

    /** Length of timing interval, as an acstime::Duration.
     */
    static const acstime::Duration TE_PERIOD_DURATION;

    /**
     * Length of timing interval, in ACS time units.
     */
    ///
    /// TODO
    /// Thomas, Aug 27, 2008
    /// Change the type to ACS::TimeInterval.
    /// If this is needed as ACS::Time, do a static_cast!
    static const ACS::Time TE_PERIOD_ACS;

    /**
     * One second interval.
     */
    ///
    /// TODO
    /// Thomas, Aug 27, 2008
    /// Change the type to ACS::TimeInterval.
    /// If this is needed as ACS::Time, do a static_cast!
    static const ACS::Time ACS_ONE_SECOND;

    /**
     * Six seconds interval.
     *
     */
    ///
    /// TODO
    /// Thomas, Aug 27, 2008
    /// Change the type to ACS::TimeInterval.
    /// If this is needed as ACS::Time, do a static_cast!
    static const ACS::Time ACS_SIX_SECONDS;

    /** Convert unix time to an acstime::Epoch.
     *  @param time unix time_t value
     *  @return epoch
     */
    static acstime::Epoch unix2epoch(const time_t time);

    /** Convert an acstime::Epoch to unix time. NB: there is a loss of
     *  precision involved in this conversion.
     *  @param epoch Epoch
     *  @return time in seconds since 1970-01-01 00:00:00.
     */
    static time_t epoch2unix(const acstime::Epoch& epoch);

    /** Nearest timing event to given time.
     *  @param epoch input time
     *  @return time of nearest timing event
     */
    static acstime::Epoch nearestTE(const acstime::Epoch& epoch);

    /** Nearest timing event on or before given time.
     *  @param epoch input time
     *  @result time of nearest timing event on or before given time
     */
    static acstime::Epoch floorTE(const acstime::Epoch& epoch);
    static ACS::Time floorTE(const ACS::Time& epoch);

    /** Nearest timing event on or after given time.
     *  @param epoch input time
     *  @result time of nearest timing event on or after given time
     */
    static acstime::Epoch ceilTE(const acstime::Epoch& epoch);
    static ACS::Time ceilTE(const ACS::Time& epoch);

    /** Nearest timing event on or before given time plus given number
     *  of timing intervals.
     *  @param epoch input time
     *  @param num_te number of timing intervals
     *  @result time of nearest timing event on or before given time
     *  plus given number of timing intervals
     */
    static acstime::Epoch plusTE(const acstime::Epoch& epoch,
                                 const int num_te);

    /** Calculate the duration whose length is a multiple of a TE
     *  that is nearest to the specified duration.
     *  @param input duration
     *  @return nearest duration that is a multiple of a TE
     */
    static acstime::Duration nearestDuration(const acstime::Duration& duration);

    /** Calculate the duration whose length is a multiple of a TE
     *  that is less than or equal to the specified duration.
     *  @param input duration
     *  @return duration that is a multiple of a TE
     */
    static acstime::Duration floorDuration(const acstime::Duration& duration);

    /** Calculate the duration whose length is a multiple of a TE
     *  that is greater than or equal to the specified duration.
     *  @param input duration
     *  @return duration that is a multiple of a TE
     */
    static acstime::Duration ceilDuration(const acstime::Duration& duration);

    /** Time at which to send a control request on the AMB given the
     *  timing interval during which the request is to take effect
     *  (given as any time in the desired interval), and the latency
     *  of the command effect (in timing intervals; oftentimes, 1).
     *
     *  @param epoch designating interval during which the command
     *  will have an effect
     *
     *  @param latency number of timing intervals after which the
     *  command goes out on the AMB that the effect is produced in
     *  hardware
     *
     *  @return time at which to send control transaction to have the
     *  effect occur at the desired time
     */
    static inline acstime::Epoch rtControlTime(const acstime::Epoch& epoch,
                                               const int latency) {
        return plusTE(epoch, -latency);
    };

    /** Time at which to send a monitor request on the AMB given the
     *  timing interval during which the requested value was acquired
     *  (given as any time in the desired interval), and the delay
     *  between acquisition and readout of the monitored value (in
     *  timing intervals; oftentimes, 1).
     *
     *  @param epoch designating interval during which the monitored
     *  value was acquired
     *
     *  @param delay number of timing intervals from the time the
     *  monitored value was acquired to its readout on the AMB
     *
     *  @return time at which to send monitor transaction to obtain the
     *  monitored value acquired in the requested interval
     */
    static inline acstime::Epoch rtMonitorTime(const acstime::Epoch& epoch,
                                               const int delay) {
        acstime::Epoch result = plusTE(epoch, delay);
        result.value += TETimeUtil::TE_PERIOD_ACS / 2;
        return result;
    }

  /** Convert an ACS time object to a time string. These functions convert from
      an ACS time object (ACS::Time, acstime::Epoch or EpochHelper) to a human
      readable string with the time formatted as HH:MM:SS.sss.
  */
  static std::string toTimeString(const EpochHelper& time);
  static std::string toTimeString(const acstime::Epoch& time);
  static std::string toTimeString(const ACS::Time& time);

  /** Convert an ACS time object to a date & time string. These functions
      convert from an ACS time object (ACS::Time, acstime::Epoch or
      EpochHelper) to a human readable string with the date & time formatted as
      YYYY-MM-DDTHH:MM:SS
  */
  static std::string toTimeDateString(const EpochHelper& time);
  static std::string toTimeDateString(const acstime::Epoch& time);
  static std::string toTimeDateString(const ACS::Time& time);

  /** Convert an ACS duration object to a string. These functions convert from
      an ACS duration object (ACS::TimeInterval, acstime::Duration or
      DurationHelper) to a human readable string with the interval, in seconds,
      formatted with the specified number of digits after the decimal place.
  */
  static std::string toSeconds(const DurationHelper& duration,
                               int precision=3);
  static std::string toSeconds(const acstime::Duration& duration,
                               int precision=3);
  static std::string toSeconds(const ACS::TimeInterval& duration,
                               int precision=3);

  /// Convert a time interval, in seconds, to an ACS::TimeInterval 
  static ACS::TimeInterval toTimeInterval(double intervalInSeconds);
};

#endif
