## @(#) $Id$
#
# Copyright (C) 2004
# Associated Universities, Inc. Washington DC, USA.
#
# Produced for the ALMA project
#
# This library is free software; you can redistribute it and/or modify it it 
# under the terms of the GNU Library General Public License as published by 
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful but WITHOUT 
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public 
# License for more details.
#
# You should have received a copy of the GNU Library General Public License 
# along with this library; if not, write to the Free Software Foundation, 
# Inc., 675 Massachusetts Ave, Cambridge, MA, 02139, USA.
#
# Correspondence concerning ALMA should be addressed as follows:
# Internet email: alma-sw-admin@nrao.edu
##
#----------------------------------------------------------------------------
# This is a direct port of Martins TETimeUtil module to python. 
from Acspy.Common import EpochHelper
from Acspy.Common.TimeHelper import TimeUtil
import acstime
import time

epochHelperObj = EpochHelper.EpochHelper()
timeUtilObj    = TimeUtil()

TE_PERIOD_DURATION = timeUtilObj.py2duration(0.048)

#----------------------------------------------------------------------------
# Although this is exactly like the py2epoch method to keep namespaces the
# same I reimplement it here.
def unix2epoch(time):
    return timeUtilObj.py2epoch(time)

#----------------------------------------------------------------------------
def epoch2unix(epoch):
    assert isinstance(epoch,acstime.Epoch)
    return timeUtilObj.epoch2py(epoch)


#----------------------------------------------------------------------------
def nearestTE(epoch):
    assert isinstance(epoch,acstime.Epoch)
    return acstime.Epoch(int(int((epoch.value+(TE_PERIOD_DURATION.value /2)) /
                            TE_PERIOD_DURATION.value)*TE_PERIOD_DURATION.value))

#----------------------------------------------------------------------------
def floorTE(epoch):
    assert isinstance(epoch,acstime.Epoch)
    return acstime.Epoch(int(int(epoch.value / TE_PERIOD_DURATION.value) *
                         TE_PERIOD_DURATION.value))

#----------------------------------------------------------------------------
def ceilTE(epoch):
    assert isinstance(epoch,acstime.Epoch)
    if epoch.value % TE_PERIOD_DURATION.value == 0 :
        return acstime.Epoch(epoch.value)
    else:
        return acstime.Epoch(int((int(epoch.value / TE_PERIOD_DURATION.value) + 1) * TE_PERIOD_DURATION.value))

#----------------------------------------------------------------------------
def plusTE(epoch, num_te):
    assert isinstance(epoch,acstime.Epoch)
    ep = acstime.Epoch(int(epoch.value + (num_te * TE_PERIOD_DURATION.value)))
    return floorTE(ep)

#----------------------------------------------------------------------------
def rtControlTime(epoch, latency):
    assert isinstance(epoch,acstime.Epoch)
    return(plusTE(epoch,-latency))

#----------------------------------------------------------------------------
def rtMonitorTime(epoch, delay):
    assert isinstance(epoch,acstime.Epoch)
    ep = plusTE(epoch,delay)
    ep.value += int(TE_PERIOD_DURATION.value /2)
    return ep
