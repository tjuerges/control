/* @(#) $Id$
 *
 * Copyright (C) 2004, 2007
 * Associated Universities, Inc. Washington DC, USA.
 *
 * Produced for the ALMA project
 *
 * This library is free software; you can redistribute it and/or modify it it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is distributed in the hope that it will be useful but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 675 Massachusetts Ave, Cambridge, MA, 02139, USA.
 *
 * Correspondence concerning ALMA should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */
//----------------------------------------------------------------------------
#include "TETimeUtil.h"
#include <acstimeTimeUtil.h> // for TimeUtil
#include <acstimeEpochHelper.h> // for EpochHelperHelper
#include <acstimeDurationHelper.h> // for DurationHelper
#include <sstream> // for ostringstream
#include <iomanip> // for std::setprecision and std::fixed

using std::ostringstream;

const acstime::Duration TETimeUtil::TE_PERIOD_DURATION =
  TimeUtil::ace2duration(ACE_Time_Value(0, 48000));
const ACS::Time TETimeUtil::TE_PERIOD_ACS = 480000ULL;

const ACS::Time TETimeUtil::ACS_ONE_SECOND(10000000ULL);
const ACS::Time TETimeUtil::ACS_SIX_SECONDS(60000000ULL);


//------------------------------------------------------------------------------

//----------------------------------------------------------------------------
acstime::Epoch TETimeUtil::unix2epoch(const time_t time) {
  return TimeUtil::ace2epoch(ACE_Time_Value(time));
}

//----------------------------------------------------------------------------
time_t TETimeUtil::epoch2unix(const acstime::Epoch &epoch) {
  return TimeUtil::epoch2ace(epoch).sec();
}

//----------------------------------------------------------------------------
acstime::Epoch TETimeUtil::nearestTE(const acstime::Epoch& epoch) {
  acstime::Epoch result;
  result.value = ((epoch.value + TETimeUtil::TE_PERIOD_ACS / 2) /
                  TETimeUtil::TE_PERIOD_ACS) * TETimeUtil::TE_PERIOD_ACS;
  return result;
}

//----------------------------------------------------------------------------
acstime::Epoch TETimeUtil::floorTE(const acstime::Epoch& epoch) {
  acstime::Epoch result;
  result.value = floorTE(epoch.value);
  return result;
}

//----------------------------------------------------------------------------
ACS::Time TETimeUtil::floorTE(const ACS::Time& t) {
  return (t / TETimeUtil::TE_PERIOD_ACS) * TETimeUtil::TE_PERIOD_ACS;
}

//----------------------------------------------------------------------------
acstime::Epoch TETimeUtil::ceilTE(const acstime::Epoch& epoch) {
  acstime::Epoch result;
  result.value = TETimeUtil::ceilTE(epoch.value);
  return result;
}

ACS::Time TETimeUtil::ceilTE(const ACS::Time& epoch) {
    ACS::Time result;
    if (epoch % TETimeUtil::TE_PERIOD_ACS == 0) {
        result = epoch;
    } else {
        result = (epoch / TETimeUtil::TE_PERIOD_ACS + 1) *
            TETimeUtil::TE_PERIOD_ACS;
    }
    return result;
}

//----------------------------------------------------------------------------
acstime::Epoch
TETimeUtil::plusTE(const acstime::Epoch& epoch, const int num_te) {
  acstime::Epoch future;
  future.value = epoch.value + num_te * TETimeUtil::TE_PERIOD_ACS;
  return TETimeUtil::floorTE(future);
}

//----------------------------------------------------------------------------
acstime::Duration
TETimeUtil::nearestDuration(const acstime::Duration& duration) {
  acstime::Duration result;
  result.value = ((duration.value + TE_PERIOD_DURATION.value / 2) /
          TE_PERIOD_DURATION.value) * TE_PERIOD_DURATION.value;
  return result;
}

//----------------------------------------------------------------------------
acstime::Duration TETimeUtil::floorDuration(const acstime::Duration& duration){
  acstime::Duration result;
  result.value = (duration.value / TE_PERIOD_DURATION.value) *
    TE_PERIOD_DURATION.value;
  return result;
}

//----------------------------------------------------------------------------
acstime::Duration TETimeUtil::ceilDuration(const acstime::Duration& duration) {
  acstime::Duration result;
  if (duration.value % TE_PERIOD_DURATION.value == 0) {
    result.value = duration.value;
  } else {
    result.value = (duration.value / TE_PERIOD_DURATION.value + 1) *
      TE_PERIOD_DURATION.value;
  }
  return result;
}

std::string TETimeUtil::toTimeString(const EpochHelper& time) {
  EpochHelper& ep = const_cast<EpochHelper&>(time);
  std::string timeString =
    ep.toString(acstime::TSArray, "%H:%M:%S.%3q", 0, 0);
  return timeString;
}

std::string TETimeUtil::toTimeString(const acstime::Epoch& time) {
  return toTimeString(time.value);
}

std::string TETimeUtil::toTimeString(const ACS::Time& time) {
  const EpochHelper eph(time);
  return toTimeString(eph);
}

std::string TETimeUtil::toTimeDateString(const EpochHelper& time) {
  EpochHelper& ep = const_cast<EpochHelper&>(time);
  std::string timeString = ep.toString(acstime::TSArray, "", 0, 0);
  return timeString;
}

std::string TETimeUtil::toTimeDateString(const acstime::Epoch& time) {
  return toTimeDateString(time.value);
}

std::string TETimeUtil::toTimeDateString(const ACS::Time& time) {
  const EpochHelper eph(time);
  return toTimeDateString(eph);
}

std::string
TETimeUtil::toSeconds(const DurationHelper& duration, int precision) {
  DurationHelper& dh = const_cast<DurationHelper&>(duration);
  const double timeInSecs = dh.toSeconds();
  ostringstream timeString;
  timeString << std::fixed << std::setprecision(precision) << timeInSecs;
  return timeString.str();
}

std::string
TETimeUtil::toSeconds(const acstime::Duration& duration, int precision) {
  return toSeconds(duration.value, precision);
}

std::string
TETimeUtil::toSeconds(const ACS::TimeInterval& duration, int precision) {
  const DurationHelper dh(duration);
  return toSeconds(dh, precision);
}

ACS::TimeInterval
TETimeUtil::toTimeInterval(double intervalInSeconds) {
  return static_cast<ACS::TimeInterval>(intervalInSeconds/100E-9 + .5);
}
