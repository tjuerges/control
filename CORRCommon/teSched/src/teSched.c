/*******************************************************************************
*
* "@(#) $Id$"
*
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* who       when        what
* --------  ----------  ----------------------------------------------
* ramestic  2006-02-10  created
*/

/************************************************************************
*   NAME
*
*   SYNOPSIS
*
*   DESCRIPTION
*
*   FILES
*
*   ENVIRONMENT
*
*   RETURN VALUES
*
*   CAUTIONS
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS
*
*------------------------------------------------------------------------
*/

/*
 * System stuff
 */
#include <linux/moduleparam.h>
#include <linux/delay.h>
#include <asm/atomic.h>

/*
 * RTAI stuff
 */
#include <rtai_sched.h>
#include <rtai_registry.h>
#include <rtai_sem.h>
#include <rtai_rwl.h>
#include <rtai_mbx.h>

/*
 * ACS stuff
 */
#include <rtLog.h>

/*
 * CORRCommon stuff
 */
#include <rtTools.h>
#include <teHandler.h>

/*
 * Local stuff
 */
#include "teSchedPrivate.h"

MODULE_AUTHOR("Rodrigo Amestica <ramestic@nrao.edu>");
MODULE_DESCRIPTION("teSched");
MODULE_LICENSE("GPL");

/*
 * used for logging
 */
#define modName	"teSched"

/*
 * module parameters
 */
static unsigned int initTimeout = 1000;         /* msec */
static unsigned int cleanUpTimeout = 1000;	/* msec */
module_param(initTimeout, uint, S_IRUGO);
module_param(cleanUpTimeout, uint, S_IRUGO);

/*
 * my local data
 */
RT_TASK	mainTask;
atomic_t                main_task_run = ATOMIC_INIT(0);
atomic_t                main_task_done = ATOMIC_INIT(0);
atomic_t                init_flag = ATOMIC_INIT(0);
atomic_t                cleanup_flag = ATOMIC_INIT(0);
atomic_t                is_resync_on = ATOMIC_INIT(0);
SEM                     access_sem;  /* API must be reentrant */
SEM                     tick_sem;     /* broadcasted at each tick */
RWL                     clock_data_rwl;
MBX                     *te_clk_mbx;
teHandlerClock_t        te_clk;
teSchedNode_t           nodes[TE_SCHED_MAX_NODES];

/** Divides a 64 bits integer by an integer. This is a wrapper
 * to rtai's llimd, here we take care of the sign of the input
 * dividend. The logging statements are a temporal debugging 
 * artifact. I have taken precausions for invoking llimd always
 * with a positive argument, but it seems that happens that at
 * some moment the input value is negative. I'm following that
 * lead. In the long run the logging will be removed, and in the 
 * longer run we will move to 64 bits and the whole thing will
 * become unnecesary.
 */
static inline long long _llimd(long long ll, int mult, int div)
{
    if ( ll >= 0 )
    {
        return llimd(ll, mult, div);
    }
    else
    {
        if ( (unsigned int)((int *)rt_whoami())[2] == RT_TASK_MAGIC )
        {
            rtlogRecord_t logRecord;
            
            RTLOG_WARNING(modName, "got a negative dividend %lld %d %d", ll, mult, div);
        }
        else
        {
            printk("%s - _llimd got a negative dividend %lld %d %d\n", modName, ll, mult, div);
        }
        
        return -llimd(-ll, mult, div);
    }
}

/*
 * used many many times. This same macros are also present in teHandler.c, but
 * here they receive as parameter the cpu frequency. That's because the activation
 * tasks use their own copy of cpu_hz, therfore, it cannot be shared with the main
 * task.
 */
#define COUNT2NS(counts)	_llimd(counts, 1000000000, te_clk.cpu_hz)
#define COUNT2US(counts)	_llimd(counts, 1000000, te_clk.cpu_hz)
#define NS2COUNT(ns)		_llimd(ns, te_clk.cpu_hz, 1000000000)

teSchedNode_t *nodeBook(unsigned int spawn, unsigned int start, RTIME offset,
                        unsigned int repeat)
{
    int i;
    teSchedNode_t *node = 0;
    char name[7];
    rtlogRecord_t logRecord;

    /*
     * get an available node
     */
    for ( i = 0; i < TE_SCHED_MAX_NODES; i++ )
    {
        if ( nodes[i].state == TE_SCHED_NODE_FREE )
        {
            node = &nodes[i];
            break;
        }
    }
    if ( node == 0 )
    {
        RTLOG_ERROR(modName, "not enough nodes (max=%d)", TE_SCHED_MAX_NODES);

        return 0;
    }

    /*
     * initialize node data
     */
    node->spawn = spawn;
    node->start = start;
    node->offset = offset;
    node->repeat = repeat;
    rt_typed_sem_init(&node->sem, 0, BIN_SEM);
    if ( rt_register(node->name, &node->sem, IS_SEM, current) == 0 )
    {
	rt_sem_delete(&node->sem);
	RTLOG_ERROR(modName, "CANNOT REGISTER A SEMAPHORE (%lu)", node->name);

	return 0;
    }

    num2nam(node->name, name);
    RTLOG_DEBUG(modName, "node booked (%s/%u/%u/%lld/%u/%p)",
	  name,
          node->spawn,
	  node->start,
	  node->offset,
	  node->repeat,
	  (void *)&node->sem);

    node->state = TE_SCHED_NODE_WAIT;

    return node;
}

void nodeFree(teSchedNode_t *node)
{
    char name_str[7];
    rtlogRecord_t logRecord;

    RTLOG_DEBUG(modName, "deleting node (node/task/sem=0x%p/0x%p/0x%p",
	  (void *)node,
	  (void *)&(node->task),
	  (void *)&(node->sem));

    switch(node->state)
    {
    case TE_SCHED_NODE_FREE:
        RTLOG_ERROR(modName, "this node is already free (0x%p)", (void *)node);
        return;
    case TE_SCHED_NODE_WAIT:
        RTLOG_ERROR(modName, "not sure how to handle node on wait state (0x%p)", (void *)node);
        return;
    case TE_SCHED_NODE_RUN:
    case TE_SCHED_NODE_END:
        if( rt_task_suspend(&node->task) != 0 )
        {
            /*RTLOG_ERROR(modName, "Cannot suspend node task!");*/
        }
        if ( rt_task_delete(&node->task) != 0 )
        {
            RTLOG_ERROR(modName, "failed to delete node task. Perhaps task not "
                "running already!");
        }
        else
        {
            RTLOG_DEBUG(modName, "node task deleted (0x%p).",
                (void *)&node->task);
        }
        break;
    default:
        RTLOG_ERROR(modName, "bookFree() this cannot happen!");
        break;
    }

    /*
     * de-register semaphore name
     */
    RTLOG_DEBUG(modName, "deregistering semaphore name (name=%lu)",
        node->name);
    rt_drg_on_name(node->name);

    /*
     * delete the semaphore resource
     */
    RTLOG_DEBUG(modName, "deleting semaphore (ptr=0x%p)",
        (void *)&node->sem);
    rt_sem_delete(&node->sem);

    num2nam(node->name, name_str);
    RTLOG_DEBUG(modName, "node freed (%s).", name_str);

    node->state = TE_SCHED_NODE_FREE;
}

/*
 * copy global data clock data into given variable guarding the access
 * by means of a global lock. If the target address equals the global
 * variable then the lock is locked as writer otherwise as reader.
 */
int copyClockData(teHandlerClock_t *from, teHandlerClock_t *to)
{
    int stat;
    rtlogRecord_t logRecord;

    /*
     * get the read/write lock that guards the clock data for reading
     */
    if ( to == &te_clk )
    {
        stat = rt_rwl_wrlock_timed(&clock_data_rwl, NS2COUNT(TE_SCHED_RWL_TO));
    }
    else
    {
        stat = rt_rwl_rdlock_timed(&clock_data_rwl, NS2COUNT(TE_SCHED_RWL_TO));
    }

    /*
     * if failed to lock the lock then return with error
     */
    if ( stat )
    {
        if ( to == &te_clk )
        {
            RTLOG_ERROR(modName, "writer failed to get clock data lock "
                "(err=0x%x)!", stat);
        }
        else
        {
            RTLOG_ERROR(modName, "reader failed to get clock data lock "
                "(err=0x%x)!", stat);
        }

        return 1;
    }

    /*
     * copy clock data to local memory
     */
    *to = *from;

    /*
     * unlock guarding object
     */
    rt_rwl_unlock(&clock_data_rwl);

    return 0;
}

/*
 * Activation tasks requires to have access to the clock data for computing
 * the time at which its associated semaphore must be given. The clock data
 * used for this operation is the following:
 *
 *	cpu_on_next_te: estimated by TE handler as cpu_on_next_te + cpu_hz/TEs
 *	cpu_hz: cpu frequency in hertz
 *
 * The time (cpu counts) until which the task must sleep before giving the semaphore
 * is computed as:
 *
 *	sleep_until = cpu_on_next_te + offset
 *
 * After giving the semaphore, the activation task must update its local copy
 * of the clock data involved in the computations shown above. This data
 * is kept alive by the main task of teSched, and accessing it is guarded by means
 * of an RTAI read/write lock (one writer and multiple readers). If for some reason
 * the activation task reads the clock data before the main task has updated it then
 * the activation task will estimate the next sleep_until value based on the
 * available information. But it should abort when data is not available for a
 * predifined number of consecutive failures to read updated data.
 */
void activationTask(long node_int)
{
    char name[7];
    unsigned int ticks;
    unsigned int cpu_hz;
    RTIME t, cpu_on_next_te;
    teHandlerClock_t clk;
    teSchedNode_t *node = (teSchedNode_t *)node_int;
    unsigned int done = 0;
    rtlogRecord_t logRecord;

    /*
     * flag this node as running
     */
    node->state = TE_SCHED_NODE_RUN;

    /*
     * update my own copy of the clock
     */
    if ( copyClockData(&te_clk, &clk) )
    {
        RTLOG_ERROR(modName, "activation task failed to get clock data!");

        node->state = TE_SCHED_NODE_END;

        return;
    }

    /*
     * for logging sake keep track of sem name
     */
    num2nam(node->name, name);

    RTLOG_DEBUG(modName,
                "activationTask started "
                "name/ticks/spawn/start/offset/rpt=%s/%u/%u/%u/%lld/%u",
                name,
                clk.ticks,
                node->spawn,
                node->start,
                node->offset,
                node->repeat);

    /*
     * copy required clock data into separated memory, this is required as for
     * reading again the clock data without loosing our memory.
     * Note that it could happen that between the moment this task was resumed
     * and 'now' one tick has been handled. We verify that here and setup the
     * first sleeping time-stamp accordingly.
     */
    if ( node->spawn == clk.ticks )
    {
        ticks = clk.ticks;
        cpu_on_next_te = clk.cpu_on_next_te;
    }
    else
    {
        ticks = node->spawn;
        cpu_on_next_te = clk.cpu_on_last_te;
    }
    cpu_hz = clk.cpu_hz;

    /*
     * loop over the configured iterations for this semaphore
     * The loop simply consists on sleeping for a given time and then
     * give the semaphore.
     */
    while ( atomic_read(&main_task_run) == 1 &&
            (node->repeat == 0 || done < node->repeat) )
    {
        /*
         * sleep until this time
         */
        t = cpu_on_next_te + NS2COUNT(node->offset * 100LL);

	/*
	 * sleep until next time...
	 */
	do
	{
            rt_sleep_until(t);

            /*
             * in case the TE handler has just resynchronized then this
             * task was awaken prematurelay and we need to reset its
             * ticking phase.
             * Note: in newer versions of rtai rt_sleep would return
             * a meaninful code signaling that the sleep was broke. 
             * For our current version rtai we do not have that luxory
             * and, therefore, need to have a global flag.
             */
            if ( atomic_read(&is_resync_on) )
            {
                RTLOG_INFO(modName,
                           "activation task resetting phase (name/ticks=%s/%d)",
                           name,
                           ticks);

                break;
            }
	}
	while ( t > rt_get_time() );

        /*
         * get a copy of current clock data
         */
        if ( copyClockData(&te_clk, &clk) )
        {
            RTLOG_ERROR(modName, "activation task failed to get clock data (in-loop)");

            break;
        }

	/*
	 * update our ticks memory
	 */
	ticks++;

	/*
	 * give the user synchronization semaphore, only if we are
         * passed of the starting time.
	 */
        if ( node->start <= ticks )
        {
            int give_it;

            give_it = 1;

            /*
             * when re-phasing the protocol is to give the semaphore if the
             * offset is bigger than the moment the re-phasing is happening.
             * That is, if the semaphore has not being given for this TE window
             * then give it now.
             */
            if ( atomic_read(&is_resync_on) == 1 )
            {
                if ( ticks == clk.ticks - 1 )
                {
                    give_it = 1;

                    ticks++;

                    RTLOG_INFO(modName,
                               "semaphore %s advanced during re-phasing",
                               name);
                }
                else if ( ticks == clk.ticks )
                {
                    give_it = 0;

                    RTLOG_INFO(modName,
                               "semaphore %s skipped during re-phasing %d %d",
                               name, ticks, clk.ticks);
                }
                else
                {
                    /*
                     * I'm not sure how is that this could happen. Let's
                     * try to fix it by giving the semaphore and aligning 
                     * to the clock ticks
                     */
                    give_it = 1;
                    ticks = clk.ticks;

                    RTLOG_INFO(modName,
                               "semaphore %s re-aligned during re-phasing (ticks loc/clk=%d/%d)",
                               name,
                               ticks,
                               clk.ticks);
                }
            }

            if ( give_it && rt_sem_signal(&node->sem) == SEM_ERR )
            {
                RTLOG_ERROR(modName,
                            "activation task failed to signal semaphore (%s)",
                            name);
                
                break;
                
                /*
                 * done represents the number of times the semaphore has
                 * been given.
                 */
                done++;
            }
        }

        /*
         * it cannot be that this task is lagging behind the TE handler.
         * The logic is such that this task always keeps on ticking, if
         * for some reason that's not the case then let's try to understand
         * that bug.
         */
        if ( clk.ticks > ticks )
        {
            RTLOG_ERROR(modName,
                        "clock data unexpectedly in the future (name/clk/loc=%s/%u/%u)",
                        name,
                        clk.ticks,
                        ticks);
            
            break;
        }

	/*
	 * if updated clock data is not available (data being read before it is
	 * updated by the main task) then this activation task will estimate the
	 * timestamp for the next semaphore based on the already available
         * information (as explained above).
	 * But it is unadmissible to estimate based on data that is older than a
	 * predefined number of ticks. Under that condition the task will abort.
	 */
	if ( ticks > clk.ticks + TE_SCHED_MAX_MISSED_UPDATES )
        {
            RTLOG_ERROR(modName,
                        "clock data content is too old (clk/loc=%u/%u)",
                        clk.ticks,
                        ticks);
            
            break;
        }
	
	/*
	 * update our knowledge about the cpu frequency. Note that the value
	 * copied into our local variable could not be the latest frequency
	 * estimated by the TE handler, but is better than having our local
	 * variable frozen to a too old one.
	 * The previous 'if' statement took care of verifying that we are not
	 * too many ticks away from the current estimation.
	 */
	cpu_hz = clk.cpu_hz;

	/*
	 * cpu counts at next tick. Note that if everything is working
	 * as expected then ticks=clk.ticks and, therefore, in that
	 * case we are using exactly what TEhandler is estimating for next tick.
	 * Otherwise, we are the ones estimating base on TE handler data that
	 * is not older than TE_SCHED_MAX_MISSED_UPDATES ticks.
	 */
	cpu_on_next_te =
            clk.cpu_on_next_te + _llimd((ticks - clk.ticks) * TEms, cpu_hz, 1000);

    } /* while ( atomic_read(&main_task_run) == 1 ... */

    RTLOG_DEBUG(modName,
                "activationTask completed (name/run/ticks=%s/%d/%u/%lld)",
                name,
                atomic_read(&main_task_run),
                ticks,
                rt_get_time_ns());

    node->state = TE_SCHED_NODE_END;
}

/*
 * synchronize with TEs by means of mail-box data
 */
int synchronizeWithTE(void)
{
    int recvn, synchRetries = 0;
    rtlogRecord_t logRecord;

    RTLOG_INFO(modName, "synchronizing with TE handler...");

    /*
     * initialize clock frequency to rtai's own, this let's me use NS2COUNT
     * immediately after
     */
    te_clk.cpu_hz = CPU_FREQ;

    /*
     * clean old data in the mailbox.
     */
    rt_mbx_receive_if(te_clk_mbx, (void *)&te_clk, sizeof(te_clk));

    /*
     * synchronize with TE.
     */
    recvn = -1;
    while ( recvn != 0 && synchRetries <= TE_HANDLER_SAMPLE_SIZE )
    {
	/*
	 * wait for data to arrive, wait not much longe than TE's period
	 */
	recvn = rt_mbx_receive_timed(te_clk_mbx,
                                     (void *)&te_clk, sizeof(te_clk),
                                     NS2COUNT(TEns + 50000));
	if ( recvn < 0 )
	{
            RTLOG_ERROR(modName, "failed to read TE handler mailbox (%d). Task "
                "aborted!", recvn);

            return 1;
	}
    }

    /*
     * check synchronization status
     */
    if ( synchRetries > TE_HANDLER_SAMPLE_SIZE )
    {
	RTLOG_ERROR(modName, "cannot rendezvous with TE. Task aborted!");

        return 1;
    }

    RTLOG_INFO(modName, "successfull rendezvous with TE "
        "(t0/ticks/hz=%lld/%u/%u)",
        te_clk.t0, te_clk.ticks, te_clk.cpu_hz);

    return 0;
}

/*
 * main task function
 */
void mainTaskFun(long not_used)
{
    int recvn;
    unsigned int error = 0, errorCnt = 0, resync_count;
    RTIME d1, d1max = 0;
    teHandlerClock_t clk, clk_before_recv;
    /* RT logging Guard to prevent error logs > 1 per 10 seconds */
    RTGUARD(rtGuardUnexpectedTicks, 0, 10, RTGUARD_MODE_TIMER);
    RTGUARD(rtGuardMBXRecv, 0, 10, RTGUARD_MODE_TIMER);
    rtlogRecord_t logRecord;

    RTLOG_INFO(modName, "main task spawned.");

    /*
     * this global flag marks the status of this task.
     */
    atomic_set(&main_task_run, 1);

    /*
     * intialize task local varible
     */
    clk = te_clk;

    /* initialize local copy of the resyncronization count, we need it
     * for detecting TE handler resync events.
     */
    resync_count = clk.resync_count;

    /*
     * init RTGuard timer
     */
    rtGuardMBXRecv.initialTimeStamp = rt_get_time_ns();
    rtGuardUnexpectedTicks.initialTimeStamp = rt_get_time_ns();

    while ( atomic_read(&main_task_run) )
    {
        RTIME wait_until;

        /*
         * when not in HARD mode available data has a much bigger latency.
         * 100us and 500us as experimental values.
         */
        if ( 0 && clk.mode == TE_HANDLER_MODE_HARD )
        {
            wait_until = clk.cpu_on_next_te + NS2COUNT(100000LL);
        }
        else
        {
            wait_until = clk.cpu_on_next_te + NS2COUNT(TEns >> 0x1);
        }

        /*
         * keep aside our current clock data, to be used for validation
         */
	clk_before_recv = clk;

        /*
	 * read the clock data mail box. Read on local clock variable
	 */
	if ( (recvn = rt_mbx_receive_until(te_clk_mbx,
                                           &clk,
                                           sizeof(clk),
                                           wait_until)) != 0 )
	{
            RTLOG_GUARD(&rtGuardMBXRecv,
                        RTLOG_ERROR,
                        "failed to read clock data (ticks/size/recvn=%u/%d/%d)",
                        clk_before_recv.ticks,
                        sizeof(clk),
                        recvn);

            error = 1;
	}
	/*
	 * validate new clock data
	 */
	else if ( clk_before_recv.ticks + 1 != clk.ticks )
	{
            /*
             * if the last attempt to read the mailbox did timeout
             * and now we have got a ticks count that coincides with
             * the missing read-out then it most likely happen that
             * the TE handler was just late. Drop the current readout
             * and continue with the loop.
             */
            if ( clk_before_recv.ticks == clk.ticks && error == 1 )
            {
                /*
                 * copy back our current clock data
                 */
                clk = clk_before_recv;

                /*
                 * reset the error flag
                 */
                error = 0;

                /*
                 * go back to the top of the loop
                 */
                continue;
            }

            RTLOG_GUARD(&rtGuardUnexpectedTicks,
                        RTLOG_ERROR,
                        "unexpected ticks count (expect/recv=%u/%u)!",
                        clk_before_recv.ticks,
                        clk.ticks);

            /*
             * set the error flag
             */
            error = 1;
	}
        else
        {
            /*
             * reset the error flag
             */
            error = 0;
        }

        /*
         * if an error did ocurr while reading the clock data then
         * fake out current memory adding a tick to the involved members
         * in the data structure
         */
        if ( error == 1 )
        {
            clk = clk_before_recv;
            clk.ticks += 1;
            clk.cpu_on_last_te += NS2COUNT(TEns);
            clk.cpu_on_next_te += NS2COUNT(TEns);

            /*
             * increment the total count of this error
             */
            errorCnt += 1;
        }

        /*
         * copy the local clock data variable into global memory
         */
        if ( copyClockData(&clk, &te_clk) )
        {
            RTLOG_ERROR(modName, "main task failed to get clock data lock!");

            goto TheEnd;
        }

        /*
         * broadcast the tick semaphore
         */
        rt_sem_broadcast(&tick_sem);

	/*
	 * record delay between the tick that just happened and this very
	 * moment.
         * DELETEME: in Thomas laptop something happens that would make
         * the TE handler to run 'ahead'. I just want to suppress the _llimd
         * log with the aim of debugging the source of the problem.
	 */
        d1 = rt_get_time();
        if ( d1 >= clk.cpu_on_last_te )
        {
            d1 = llimd(d1 - clk.cpu_on_last_te, 1000000, clk.cpu_hz);
        }
        else
        {
            d1 = -llimd(clk.cpu_on_last_te - d1, 1000000, clk.cpu_hz);
        }
        if ( d1 > d1max )
	{
            d1max = d1;
	}
        /*
        if ( (d1 = _llimd(rt_get_time() - clk.cpu_on_last_te,
                          1000000,
                          clk.cpu_hz))
             > d1max )
	{
            d1max = d1;
	}
        */

        /*
         * if the TE handler has resynchronized since the last tick then
         * we need to do the same by realigning the phase of the activation
         * tasks.
         */
        if ( clk.resync_count > resync_count )
        {
            int i, stat;

            /*
             * update our local copy, we do not whant to keep getting
             * here every time after the first resynchronization.
             */
            resync_count = clk.resync_count;

            /*
             * get node list access semaphore
             */
            stat = rt_sem_wait_timed(&access_sem, TE_SCHED_ACCESS_TO);
            if ( stat == SEM_ERR || stat == SEM_TIMOUT )
            {
                RTLOG_ERROR(modName,
                            "failed to access for resynchronization (stat=%d)",
                            stat);
            }

            /*
             * access the nodes list and awake all those actually running
             */
            for ( i = 0; i < TE_SCHED_MAX_NODES; i++ )
            {
                if ( nodes[i].state != TE_SCHED_NODE_RUN )
                {
                    continue;
                }

                /*
                 * set the flag used by the activation tasks for detecting
                 * an earlier awaking from rt_sleep. Newer versions of
                 * rtai implement an rt_sleep return code that let's 
                 * detect the same withouth the hassle of our extra global
                 * variable
                 */
                atomic_set(&is_resync_on, 1);

                /*
                 * Note: in the future we should use rt_task_wakeup_sleeping
                 */
                if ( rt_task_masked_unblock(&nodes[i].task, RT_SCHED_DELAYED)
                     == -EINVAL )
                {
                    char name[7];

                    num2nam(nodes[i].name, name);
                    RTLOG_ERROR(modName,
                                "failed to awake activation task (%s)",
                                name); 
                }

                /*
                 * given that the activation tasks run at a higher priority of
                 * this task then all them should be ready by now and the
                 * flag can be reset now.
                 */
                atomic_set(&is_resync_on, 0);

            }

            /*
             * ready with the nodes list, give back the access semaphore
             */
            if ( rt_sem_signal(&access_sem) == SEM_ERR )
            {
                RTLOG_ERROR(modName, "mainTaskFun failed to signal access sem");
            }
        }

	/*
	 * some logging is sometimes useful
	 */
	if ( (clk.ticks / 100 ) * 100 == clk.ticks )
	{
            RTLOG_DEBUG(modName, "synced with TE ticks=%u err=%u "
                "d1=%lld/%lld[us] f=%u[hz]",
                clk.ticks,
                errorCnt,
                d1, d1max,
                clk.cpu_hz);
	}
    } /* while(main_task_run) */

TheEnd:
    RTLOG_INFO(modName, "main task has terminated (cpu=%lld)!", rt_get_time());

    /*
     * this flag will indicate to the the cleanup function that this
     * task has really finished.
     */
    atomic_set(&main_task_done, 1);

    rt_task_delete(rt_whoami());
}

/*
 * API
 */
void teSchedRelSem(SEM *sem)
{
    int i, stat = rt_sem_wait_timed(&access_sem, TE_SCHED_ACCESS_TO);
    char name[7];
    rtlogRecord_t logRecord;

    if ( stat == 0xFFFF || stat == 0xFFFE )
    {
	RTLOG_ERROR(modName, "failed to take access semaphore (stat=%d)!", stat);

	return;
    }

    for ( i = 0; i < TE_SCHED_MAX_NODES; i++ )
    {
	if ( &nodes[i].sem == sem )
	{
            num2nam(nodes[i].name, name);

            RTLOG_DEBUG(modName, "requested to release sem (%s)", name);

            nodeFree(&nodes[i]);

            break;
	}
    }

    if ( i == TE_SCHED_MAX_NODES )
    {
	RTLOG_ERROR(modName, "semaphore pointer not found in list (0x%p).",
	    (void *)sem);
    }

    if ( rt_sem_signal(&access_sem) == SEM_ERR )
    {
        RTLOG_ERROR(modName, "teSchedRelSem failed to signal access sem!");
    }

    return;
}

SEM *teSchedGetSem(unsigned long long start,
                   unsigned long long offset,
                   unsigned int repeat)
{
    int stat;
    unsigned int startTick;
    unsigned long res;
    teHandlerClock_t clk;
    teSchedNode_t *node;
    rtlogRecord_t logRecord;

    RTLOG_DEBUG(modName, "teSchedGetSem - start=%lld, offset=%lld, repeat=%d",
        start, offset, repeat);

    /*
     * if main task is not synchronize with TEs then abort
     */
    if ( atomic_read(&main_task_run) != 1 )
    {
	RTLOG_ERROR(modName, "unsynchronized!");
	return 0;
    }

    /*
     * check parameters
     */
    if ( start > TE_HANDLER_ACS_TIME_MAX ||
	 offset > TE_HANDLER_ACS_TIME_MAX )
    {
	RTLOG_ERROR(modName, "teSchedGetSem got incorrect parameters "
	    "(start/offset=%llu/%llu)",
	    start, offset);
	return 0;
    }

    /*
     * get a copy of clock data
     */
    if ( copyClockData(&te_clk, &clk) )
    {
	RTLOG_ERROR(modName, "teSchedGetSem failed to get clock data!");

	return 0;
    }

    /*
     * compute starting tick for this request
     */
    if ( start == 0 )
    {
        /*
         * next tick
         */
	startTick = clk.ticks + 1;
    }
    else
    {
        /*
         * another parameter checking point. The start time cannot be before t0
         */
        if ( start <= clk.t0 )
        {
            RTLOG_ERROR(modName, "invalid start time (t0/start=%llu/%llu)",
                clk.t0, start);

            return 0;
        }

        /*
         * tick immediately before or on start time
         */
        startTick =
            (unsigned int)rtai_ulldiv((unsigned long long)(start - clk.t0),
            TEacs, &res);
    }

    /*
     * yet another param check. Starting tick must not coincide
     * with current one or be smaller.
     */
    if ( clk.ticks >= startTick )
    {
        RTLOG_ERROR(modName, "invalid start time "
            "(t0/start/ticks/startTick=%lluacs/%lluacs/%u/%u)!",
            clk.t0, start, clk.ticks, startTick);
    }

    /*
     * get node list access semaphore
     */
    stat = rt_sem_wait_timed(&access_sem, TE_SCHED_ACCESS_TO);
    if ( stat == SEM_ERR || stat == SEM_TIMOUT )
    {
	RTLOG_ERROR(modName, "failed to take access semaphore (stat=%d)!", stat);

	return 0;
    }

    /*
     * book a node for this request
     */
    if ( (node = nodeBook(clk.ticks, startTick, offset, repeat)) == 0 )
    {
	RTLOG_ERROR(modName, "failed to add request!");

        if ( rt_sem_signal(&access_sem) == SEM_ERR )
        {
            RTLOG_ERROR(modName, "teSchedGetSem failed to signal access sem!");
        }

	return 0;
    }

    /*
     * spawn an activation task for this request
     */
    if ( rt_task_init_cpuid(&node->task,
                            activationTask,
                            (int)node,
                            TE_SCHED_ACT_TASK_STACK,
                            TE_SCHED_ACTIVATION_TASK_PRIO,
                            0, 0, 0x0)
         != 0 )
    {
        RTLOG_ERROR(modName, "CANNOT SPAWN ACTIVATION TASK.");

        /*
         * delete the node
         */
        nodeFree(node);

        if ( rt_sem_signal(&access_sem) == SEM_ERR )
        {
            RTLOG_ERROR(modName, "teSchedGetSem failed to signal access sem!");
        }

        return 0;
    }

    /*
     * ready with the nodes list, give back the access semaphore
     */
    if ( rt_sem_signal(&access_sem) == SEM_ERR )
    {
        RTLOG_ERROR(modName, "teSchedGetSem failed to signal access sem!");
    }

    RTLOG_DEBUG(modName, "activation task initialized (0x%p)",
        (void *)&node->task);

    /*
     * set activation task as ready to run. Given that the activation
     * task has a higher priority than the current task this call shall
     * preempt us!
     */
    rt_task_resume(&node->task);

    RTLOG_DEBUG(modName, "activation task resumed (0x%p)",
        (void *)&node->task);

    return &node->sem;
}

int teSchedSleepUntilNextTick(void)
{
    int stat;
    rtlogRecord_t logRecord;

    /*
     * if main task is not synchronize with TEs then abort
     */
    if ( atomic_read(&main_task_run) != 1 )
    {
	RTLOG_ERROR(modName, "unsynchronized!");

        return 1;
    }

    /*
     * make sure the tick semaphore is unavailable
     */
    if ( rt_sem_wait_if(&tick_sem) == SEM_ERR )
    {
        RTLOG_ERROR(modName, "error on tick semaphore!");

        return 1;
    }

    /*
     * wait on the tick semaphore
     */
    stat = rt_sem_wait_timed(&tick_sem, NS2COUNT(TEns + 200000LL)); /* TE + 100us */
    if ( stat == SEM_ERR || stat == SEM_TIMOUT )
    {
        RTLOG_ERROR(modName, "error while waiting on tick semaphore "
            "(err=0x%x)!", stat);

        return 1;
    }

    return 0;
}

int RTAI_SYSCALL_MODE teSchedSleepUntil(unsigned long long time)
{
    unsigned long long now;
    rtlogRecord_t logRecord;

    /*
     * if received time is zero then just sleep until next tick
     */
    if ( time == 0 )
    {
        return teSchedSleepUntilNextTick();
    }

    /*
     * get current time
     */
    if ( (now = teHandlerGetTime()) == 0 )
    {
        RTLOG_ERROR(modName, "failed to get current time!");

        return 1;
    }

    /*
     * if the target time is in the past just return successfully
     */
    if ( now >= time )
    {
        RTLOG_WARNING(modName,
                      "nothing to sleep (now/target=%llu/%llu [acs])",
                      now, time);

        return 0;
    }

    /*
     * iteration of 48ms sleeps until target time lays within the present
     * TE window
     */
    while ( now + 10 * TEacs < time )
    {
        rt_sleep(nano2count(10 * TEns));

        /*
         * update our current time knowledge, this keeps us synchronized
         * with the TE time, rt_sleep does not guarantee that because
         * it is base on the RTAI frequency
         */
        if ( (now = teHandlerGetTime()) == 0 )
        {
            RTLOG_ERROR(modName, "failed to get current time!");

            return 1;
        }
    }

    /*
     * sleep until target time, at this moment time-now is less than 48ms.
     * Call sleep only if 'time' is still in the future, it can happen 
     * that quiting the previous while-loop took some extra cpu counts.
     * There is no fundamental wrong on calling nano2count nor rt_sleep
     * with a negative argument, it is just a waste of time.
     */
    if ( time > now )
    {
        rt_sleep(nano2count((time - now) * 100));
    }

    return 0;
}

/*
 * intermediate function as for exporting cdp_scheduler_get_sem to user land.
 * (the kernel version of this function returns a pointer to a semaphore)
 */
unsigned int RTAI_SYSCALL_MODE teSchedGetSemLXRT(unsigned long long start,
                                                 unsigned long long offset,
                                                 unsigned int repeat)
{
    return rt_get_name(teSchedGetSem(start, offset, repeat));
}

/*
 * intermediate function as for exporting cdp_scheduler_rel_sem to user land.
 * (the kernel version of this function receives as parameter a pointer)
 */
void RTAI_SYSCALL_MODE teSchedRelSemLXRT(unsigned long name)
{
    teSchedRelSem(rt_get_adr(name));
}

/*
 * exported API
 */
static struct rt_fun_entry teSchedFun[] =
{
    [ TE_SCHED_GETSEM ]     = { 1,	teSchedGetSemLXRT	},
    [ TE_SCHED_RELSEM ]     = { 1,	teSchedRelSemLXRT	},
    [ TE_SCHED_SLEEPUNTIL ] = { 1,	teSchedSleepUntil	}
};
EXPORT_SYMBOL (teSchedGetSem);
EXPORT_SYMBOL (teSchedGetSemLXRT);
EXPORT_SYMBOL (teSchedRelSem);
EXPORT_SYMBOL (teSchedRelSemLXRT);
EXPORT_SYMBOL (teSchedSleepUntil);

/*
 * intialization task
 */
void initTaskFunction(long not_used)
{
    rtlogRecord_t logRecord;

    RTLOG_INFO(modName, "initialization task started");

    /*
     * synchronize with TEs before spawning main task
     */
    if ( synchronizeWithTE() )
    {
        return;
    }

    /*
     * spawn the main task. Set it to run in the first cpu. Note that the
     * cpu id mask seems to be differnt than when using the same function
     * from user space.
     */
    if ( rt_task_init_cpuid(&mainTask, mainTaskFun, 0,
			    TE_SCHED_MAIN_TASK_STACK, TE_SCHED_TASK_PRIO,
			    0, 0, 0x0)
	 != 0 )
    {
	RTLOG_ERROR(modName, "failed to spawn main task!");

	return;
    }

    /*
     * resume the main task
     */
    rt_task_resume(&mainTask);

    RTLOG_INFO(modName, "initialization task function terminating...");

    atomic_set(&init_flag, 1);

    rt_task_delete(rt_whoami());
}

/*
 * clean-up task
 */
void cleanUpTaskFunction(long not_used)
{
    int waitCount = 0;
    rtlogRecord_t logRecord;

    RTLOG_INFO(modName, "clean-up task started");

    /*
     * command the main task to finish
     */
    atomic_set(&main_task_run, 0);

    /*
     * wait for the main task to be gone
     */
    while ( atomic_read(&main_task_done) != 1 && waitCount < 2 * TEms )
    {
	waitCount++;

	/*
	 * sleep for 1ms
	 */
	rt_sleep(NS2COUNT(1000000LL));
    }

    /*
     * if the main task did not finish on time then forcibly remove
     * it for the rtai's scheduler
     */
    if ( waitCount == 2 * TEms )
    {
	rt_task_suspend(&mainTask);
	rt_task_delete(&mainTask);

	RTLOG_ERROR(modName, "main task did not finish on time!");

	return;
    }

    RTLOG_INFO(modName, "clean-up task function terminating...");

    /*
     * set our own done flag to true
     */
    atomic_set(&cleanup_flag, 1);

    rt_task_delete(rt_whoami());

}

int teSched_main(int stage)
{
    int i;
    int status = RT_TOOLS_MODULE_INIT_SUCCESS;
    RT_TASK initTask, cleanUpTask;
    char name[7];
    rtlogRecord_t logRecord;

    if ( stage == RT_TOOLS_MODULE_STAGE_INIT )
    {
	/*
	 * log RCS ID (cvs version)
	 */
	RTLOG_INFO(modName, "initializing module...");
	RTLOG_INFO(modName, "$Id$");
	RTLOG_INFO(modName, "initTimeout        = %ums", initTimeout);
	RTLOG_INFO(modName, "cleanUpTimeout     = %ums", cleanUpTimeout);
	RTLOG_INFO(modName, "rtai extension idx = %d", TE_SCHED_IDX);

	goto Initialization;
    }
    else
    {
	RTLOG_INFO(modName, "cleaning up module...");

	goto FullCleanUp;
    }

Initialization:
    /*
     * gets pointer to TE clock data mailbox
     */
    if ( (te_clk_mbx = (MBX *)rt_get_adr(nam2num(TE_HANDLER_CLK_MBX_NAME))) == 0 )
    {
	RTLOG_ERROR(modName, "CANNOT GET TE CLK MAILBOX");

	status = RT_TOOLS_MODULE_INIT_ERROR;

        goto Exit;
    }

    /*
     * initialize the access control semaphore
     */
    rt_typed_sem_init(&access_sem, 1, RES_SEM);

    /*
     * initialize the tick semaphore
     */
    rt_typed_sem_init(&tick_sem, 1, BIN_SEM);

    /*
     * initialize read/write lock used to guard access to module global clock data
     */
    rt_rwl_init(&clock_data_rwl);

    /*
     * initiallize the statically allocated array of nodes.
     */
    for ( i = 0; i < TE_SCHED_MAX_NODES; i++ )
    {
	nodes[i].state = TE_SCHED_NODE_FREE;
	sprintf(name, "SCH%03d", i);
	nodes[i].name = nam2num(name);
    }

    /*
     * make available to user land the important functions.
     */
    if ( set_rt_fun_ext_index(teSchedFun, TE_SCHED_IDX) )
    {
	RTLOG_ERROR(modName, "rtai extension failure, try with a differnt IDX "
	    "(idx=%d)!",
	    TE_HANDLER_IDX);

	status = RT_TOOLS_MODULE_INIT_ERROR;

	goto Level1CleanUp;
    }
    else
    {
	RTLOG_INFO(modName, "rtai extension installed");
    }

    /*
     * initialization of resources is finished. Now spawn the rtai
     * task that takes care of doing the asynchronous part of the
     * initialization.
     */
    if ( rt_task_init(&initTask,
		      initTaskFunction,
		      0,
		      RT_TOOLS_INIT_TASK_STACK,
		      RT_TOOLS_INIT_TASK_PRIO,
		      0, 0)
	 != 0 )
    {
	RTLOG_ERROR(modName, "failed to spawn initialization task!");

	status  = 1; /* or some other code, defined in ... */

	goto Level2CleanUp;
    }

    /*
     * resume the initialization task
     */
    rt_task_resume(&initTask);

    /*
     * wait for the initialization task to be ready
     */
    if ( rtToolsWaitOnFlag(&init_flag, initTimeout) )
    {
	/*
	 * okay, the init task was not ready on time, now forcibly remove
	 * it for the rtai's scheduler
	 */
	rt_task_suspend(&initTask);
	rt_task_delete(&initTask);

	RTLOG_ERROR(modName, "initialization task did not report back on time!");

	status  = RT_TOOLS_MODULE_NO_INIT_TASK_ERROR;

	goto FullCleanUp;
    }

    /*
     * initialization is finished
     */
    goto Exit;

FullCleanUp:
    /*
     * asynchronous clean-up phase to be handled by this task
     */
    if ( rt_task_init(&cleanUpTask,
                      cleanUpTaskFunction,
                      0,
                      RT_TOOLS_CLEANUP_TASK_STACK,
                      RT_TOOLS_CLEANUP_TASK_PRIO,
                      0, 0)
         == 0 )
    {
        /*
         * resume the clean-up task
         */
        rt_task_resume(&cleanUpTask);

        /*
         * wait for the initialization task to be ready
         */
        if ( rtToolsWaitOnFlag(&cleanup_flag, cleanUpTimeout) )
        {
            /*
             * okay, the init task was not ready on time, now forcibly remove
             * it for the rtai's scheduler
             */
            rt_task_suspend(&cleanUpTask);
            rt_task_delete(&cleanUpTask);

            RTLOG_ERROR(modName, "clean-up task did not report back on time!");

            status  = RT_TOOLS_MODULE_CLEANUP_ERROR;

            goto Exit;
        }
    }
    else
    {
        RTLOG_ERROR(modName, "failed to clean-up module");

        status = RT_TOOLS_MODULE_CLEANUP_ERROR;

        goto Exit;
    }

Level2CleanUp:
    /*
     * remove user land access.
     */
    reset_rt_fun_ext_index(teSchedFun, TE_SCHED_IDX);
    RTLOG_INFO(modName, "rtai extension removed (idx=%d)", TE_SCHED_IDX);

Level1CleanUp:
    /*
     * really delete and unregister resources (no matter what!).
     */
    for ( i = 0; i < TE_SCHED_MAX_NODES; i++ )
    {
        nodeFree(&nodes[i]);
    }

    /*
     * delete read/write lock resource
     */
    if ( rt_rwl_delete(&clock_data_rwl) )
    {
        RTLOG_ERROR(modName, "failed to delete read/write lock object.");
    }

    /*
     * delete the tick semaphore
     */
    rt_sem_delete(&tick_sem);

    /*
     * delete the access control semaphore. This will avoid others to
     * invoke the public API.
     */
    rt_sem_delete(&access_sem);

Exit:
    return status;
}

static int __init teSched_init(void)
{
    int status;
    rtlogRecord_t logRecord;

    if ( (status = teSched_main(RT_TOOLS_MODULE_STAGE_INIT))
	 == RT_TOOLS_MODULE_INIT_SUCCESS )
    {
	RTLOG_INFO(modName, "module initialized successfully");
    }
    else
    {
	RTLOG_ERROR(modName, "failed to initialize module");
    }

    return status;
}

static void __exit teSched_exit(void)
{
    rtlogRecord_t logRecord;

    if ( teSched_main(RT_TOOLS_MODULE_STAGE_EXIT)
	 == RT_TOOLS_MODULE_EXIT_SUCCESS )
    {
	RTLOG_INFO(modName, "module cleaned up successfully");
    }
    else
    {
	RTLOG_ERROR(modName, "failed to clean-up module");
    }
}

module_init(teSched_init);
module_exit(teSched_exit);

/*___oOo___*/
