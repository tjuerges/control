/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* who       when      what
* --------  --------  ----------------------------------------------
* ramestic  2003-10-14  created
*/

/************************************************************************
*   NAME
*	CDP_Scheduler -
*
*   SYNOPSIS
* 
*   DESCRIPTION
*
*   FILES
*
*   ENVIRONMENT
*
*   RETURN VALUES 
*
*   CAUTIONS 
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS   
*
*------------------------------------------------------------------------
*/

/*
 * System stuff
 */
#include <unistd.h>
#include <stdlib.h>
#include <sys/mman.h>

/*
 * RTAI stuff
 */
#include <rtai_lxrt.h>

/*
 * TE handler API
 */
#include <teHandler.h>

/* 
 * Local Headers
 */
#include "teSched.h"

#define N	4
#define OFFSET  2

int main(int argc, char** argv) 
{
    RT_TASK *task;
    RTIME exectime[3], t;
    int i, status;
    unsigned long long cpu_now, start[N], gotten[N];
    SEM *sem[N];

    /*
     * avoid memory paging and allow execution as non-root user.
     */
    rt_allow_nonroot_hrt();
    mlockall(MCL_CURRENT | MCL_FUTURE);
    
    /*
     * creates the buddy
     */
    if ( !(task = rt_task_init_schmod(getpid(), 
				      10,
				      0,
				      0,
				      SCHED_FIFO,
				      0xFF)))
	{
	printf("CANNOT INIT MASTER TASK\n");
	exit(1);
        }

    /*
     * goes into RTAI scheduler.
     */
    rt_make_hard_real_time();
    
    /*
     * schedule semaphores
     */
    cpu_now = teHandlerGetTime();	/* 100ns */

    for ( i = 0; i < N; i++ )
	{
	start[i] = cpu_now + (OFFSET + i) * 10000000LL;
	if ( (sem[i] = teSchedGetSem(start[i], 0, 1)) == 0 )
	    {
	    printf("CANNOT GET SEMAPHORE (%d).\n", i);
	    }
	}
    
    rt_sleep(nano2count(OFFSET * 1000000000LL));

    /*
     * block on each of the semaphores
     */
    for ( i = 0; i < N; i++ )
	{
	if ( sem[i] != 0 )
	    {
	    status = rt_sem_wait_timed(sem[i], nano2count(1000100000LL));
	    gotten[i] = teHandlerGetTime(); /* 100ns */
	    if ( status == 0xFFFF || status == 0xFFFE )
		{
		printf("teSchedTest: failed to get semaphore (%d/%x/%llu/%llu).\n",
		       i, status, start[i], gotten[i]);
		}
	    }
	}
    
    /*
     * release all allocated semaphores
     */
    for ( i = 0; i < N; i++ )
	{
	if ( sem[i] != 0 )
	    {
	    teSchedRelSem(sem[i]);
	    }
	}

    /*
     * sleeps until 
     */
    if ( (t = teHandlerGetTime()) == 0 )
    {
	printf("teHandlerGetTime failed.\n");
    }
    else
    {
	if ( teSchedSleepUntil(t + 10000000LL) )
	{
	    printf("teSchedSleepUntil failed.\n");
	}
	t = (teHandlerGetTime() - (t + 10000000LL)) / 10;
    }

    /*
     * goes back to Linux scheduler
     */
    rt_make_soft_real_time();
    rt_task_delete(task);

    printf("Test - measures time diff between requested time and actual time\n");
    for ( i = 0; i < N; i++ )
	{
	if ( sem[i] != 0 )
	    {
	    printf("teSchedTest --- diff[%2d]=%lld[us]\n",
		   i, (gotten[i] - start[i]) / 10);
	    }
	}
    
    printf("Test - sleep-until precision = %lld[us]\n", t);
    
    /*
     * our statistics and clean-up
     */
    rt_get_exectime(task, exectime);
    printf("\n>>> EXECTIME = %G\n", 
	   (double)exectime[0]/(double)(exectime[2] - exectime[1]));

    return 0;
}

/*___oOo___*/
