DESCRIPTION TEST 1
This is a kernel space task and it exercises the module's API at that
level. The test modules actually performs a series of tests:

       1. sleep until the next time event (repeated N times)
       2. sleep until an absolute time in the future (repeated N times).
          The future time-stamp is choosen as now+1s
       3. request a semaphore and wait for it for N times. One at the time,
          the offset is selected from 0 to 47 milliseconds.
       4. several semaphores are requested at once, all them with
          different offsets. The task then waits N times for each semaphore.
       5. A simple test of trying to add 17 semaphores should fail.

Tests for 1-to-4 statistics are computed (mean, rms, min and max) of
the difference between the time the task was supposed to take the
requested semaphore and the current time as retrieved by means of
teHandlerGetTime. In a 'normal' machine all this figues should be
under 100us. Their values can be read in the kernel log file while the
test is running. The refence file it just contains and okay or
not-okay message, meaning that the test completed all its items
without problems or error otherwise.

DESCRIPTION TEST 2
This test was implementating while troubleshooting timeing issues with
the correlator simulator. It mimics the way the correlator uses the
teScheSleepUntil function. That is, get TE handler time, compute a
timestamp in the future D milisecond from now and sleeps until
then. This test stresses this functionality by spawning T tasks doing
the same thing at the same time for N iterations. There is no output
(aprt from a print out the program parameters) when the test runs
successfully.
  
DESCRIPTION TEST 3
standard endurance test. It loads the base set of modules and then it
loads and unloads teHandler and teSched for 100 times. 
OUTCOME: in /var/log/messages load/unload messages, or error logs.
