/*******************************************************************************
*
* "@(#) $Id$"
*
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* who       when        what
* --------  --------    ----------------------------------------------
* ramestic  2004-03-03  created
*/

/*******************************************************************************
*
* DESCRIPTION
*    this test consist on one single task that performs the following 4 sets
*    of exercises:
*
*       1. sleep until the next time event (repeated N times)
*       2. sleep until an absolute time in the future (repeated N times).
*          The future time-stamp is choosen as now+1s
*       3. request a semaphore and wait for it for N times. One at the time,
*          the offset is selected from 0 to 47 milliseconds.
*       4. several semaphores are requested at once, all them with
*          different offsets. The task then waits N times for each semaphore.
*       5. A simple test of trying to add 17 semaphores should fail.
*
*    Each set collects data for computing statistics on the target difference.
*    That is, the actual time at which the task is waken up minus the expected
*    time for each situation. Reported statistics includes mean, rms, min and
*    max values.
*
*    The test passes successfully when all sets are excuted without error. There
*    is no verification of statistic values.
*
*/

/* 
 * System Headers
 */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/delay.h>

/*
 * RTAI specific
 */
#include <rtai_lxrt.h>

/*
 * ACS or CORR specific
 */
#include <rtLog.h>
#include <teHandler.h>

/* 
 * Local Headers
 */
#include "teSched.h"

MODULE_AUTHOR("Rodrigo Amestica");
MODULE_DESCRIPTION("TE scheduler test");
MODULE_LICENSE("GPL");

#define modName		"teSchedTest_k"
#define N		16

#define SLLIMD(ll, mul, div)	\
ll < 0 ? -llimd((unsigned long long)(-ll), mul, div) : llimd((unsigned long long)ll, mul, div)

RTIME sllimd(RTIME ll, unsigned long mul, unsigned long div)
{
    unsigned long long val;

    if ( ll >= 0 )
    {
        val = (unsigned long long)ll;
    }
    else
    {
        val = (unsigned long long)(-ll);
    }

    val = llimd(val, mul, div);

    if ( ll < 0 )
    {
        return -(RTIME)val;
    }

    return (RTIME)val;
}

/*
 * global variables
 */
int debug = 1;
RT_TASK	task;

RTIME lldSqrt(RTIME _x)
{
    unsigned long long r;
    unsigned long long x;

    if ( _x >= 0 )
    {
        x = _x;
    }
    else
    {
        return 0;
    }
    
    x = (x + 1) >> 1;           /* init x */

    for ( r = 0; x > r; x -= r++ );
    
    return r;
}

/*
 * compute mean and variance as a running value
 * over all available samples. A recursive implementation is used here,
 * therefore, passed parameters must contain already computed values
 * in the previous iteration. That's normally true ecept for n=0, at which
 * moment mean and rms are intialized accordingly.
 *
 * http://mathworld.wolfram.com/SampleVarianceComputation.html
 */
void computeStats(const unsigned int n,
                  const RTIME sample,
                  RTIME *mean, RTIME *var,
                  RTIME *min, RTIME *max)
{
    RTIME mean_n = *mean;
    
    if ( n == 0 )
    {
        *mean = *min = *max = sample;
        *var = 0;    
        
        return;
    }
    
    if ( sample > *max )
    { 
        *max = sample;
    }
    
    if ( sample < *min ) 
    {
        *min = sample; 
    }
    
    *mean += sllimd(sample - *mean, 1, n + 1);
    
    *var =
        llimd(*var, n - 1, n) + (1 + n) * (*mean - mean_n) * (*mean - mean_n);

    return;
}  

int periodicSem(int ticks, unsigned long long offset /* ACS units */)
{
    int i, stat;
    SEM *sem;
    RTIME t_start, min, max, mean, var, rms;
    rtlogRecord_t logRecord;

    /*
     * get synchronized with the stating edge of the following tick
     */
    if ( teSchedSleepUntil(0) )
    {
	RTLOG_ERROR(modName, "failed to sleep until next tick!");

	return 1;	
    }

    /*
     * request a semaphore
     */
    if ( (sem = teSchedGetSem(0, offset, 0)) == 0 )
    {
	RTLOG_ERROR(modName, "cannot get sem");

	return 1;
    }

    /*
     * adjust starting time to next te plus the offset
     */
    t_start = teHandlerGetLast() + TEacs + offset; /* ACS unit */ 

    /*
     * take the semaphore several times
     */
    for ( i = 0; i < ticks; i++ )
    {
	stat = rt_sem_wait_timed(sem,
				 nano2count(TEns + 
					    offset * 100LL + /* ns */
					    500000LL)); /* TE+offset+500us */
	if ( stat == 0xFFFF || stat == 0xFFFE )
	{
	    RTLOG_ERROR(modName, "did not get the semaphore (i/stat/cpu=%d/0x%x/%lld)",
		  i, stat, rt_get_time());

	    teSchedRelSem(sem);

	    return 1;
	}

        computeStats(i,
                     teHandlerGetTime() -
                     (t_start + i * TEacs),
                     &mean, &var,
                     &min, &max);
    }

    teSchedRelSem(sem);

    rms = lldSqrt(llimd(var, 1, ticks - 1));

    RTLOG_INFO(modName, "periodicSem offset/mean/rms/min/max = %lld/%lld/%lld/%lld/%lld [us]",
	  llimd(offset, 1, 10),
	  SLLIMD(mean, 1, 10),
	  SLLIMD(rms, 1, 10),
	  SLLIMD(min, 1, 10),
	  SLLIMD(max, 1, 10));

    return 0;
}

int sleepUntilNextTickSet(int n)
{
    int i;
    unsigned long long targetTime, actualTime;
    RTIME min, max, mean, var, rms;
    rtlogRecord_t logRecord;

    /*
     * start synchronized with a TE boundary
     */
    if ( teSchedSleepUntil(0) )
    {
	RTLOG_ERROR(modName, "failed to synch!");
	return 1;
    }

    /*
     * record time at next tick and sleep until next one, then log the difference
     * between the waking time and the recorded one; this difference will tell us
     * with which latency teSchedSleepUntilNextTick is coming back to us. 
     */
    for ( i = 0; i < n; i++ )
    {
        /*
         * expected wake-up time
         */
	targetTime = teHandlerGetLast() + TEacs;

        /*
         * sleep until next tick
         */
	if ( teSchedSleepUntil(0) )
	{
	    RTLOG_ERROR(modName, "failed to sleep until next tick!");

	    return 1;
	}

        /*
         * actual time at which we are waking up
         */
        actualTime = teHandlerGetTime();

        /*
         * check that we really slept until the next tick
         */
        if ( teHandlerGetLast() == (targetTime - TEacs) )
        {
            RTLOG_ERROR(modName, "repeated time (target/actual=%llu/%llu)!",
                  targetTime, actualTime);
            
            return 1;
	}

        computeStats(i,
                     actualTime - targetTime,
                     &mean, &var,
                     &min, &max);	

	
    }

    rms = lldSqrt(llimd(var, 1, n - 1));

    RTLOG_INFO(modName, "sleepUntilNextTick mean/rms/min/max = %lld/%lld/%lld/%lld [us]",
	  SLLIMD(mean, 1, 10),
	  SLLIMD(rms, 1, 10),
	  SLLIMD(min, 1, 10),
	  SLLIMD(max, 1, 10));

    return 0;
}

int sleepUntilSet(int n, unsigned long long offset)
{
    int i;
    unsigned long long now;
    RTIME min, max, mean, var, rms;
    rtlogRecord_t logRecord;

    for ( i = 0; i < N; i++ )
    {
	now = teHandlerGetTime();
	
	if ( teSchedSleepUntil(now + offset) )
	{
	    RTLOG_ERROR(modName, "failed to sleep until %llu", now + offset);
	    
	    return 1;
	}

        computeStats(i,
                     teHandlerGetTime() - (now + offset),
                     &mean, &var,
                     &min, &max);
    }

    /*
     * log the out-come
     */
    rms = lldSqrt(llimd(var, 1, N - 1));
    RTLOG_INFO(modName, "sleepUntil mean/rms/min/max = %lld/%lld/%lld/%lld [us]",
	  SLLIMD(mean, 1, 10),
	  SLLIMD(rms, 1, 10),
	  SLLIMD(min, 1, 10),
	  SLLIMD(max, 1, 10));

    return 0;
}

int periodicSemaphoresSet(int n)
{
    int i;
    rtlogRecord_t logRecord;

    /*
     * for each offset between 0 and 47ms request a semaphore and
     * then wait N times for it
     */
    for ( i = 0; i < 48; i++ )
    {
	if ( periodicSem(n, i * 10000) ) /* second param in ACS units */
	{
	    RTLOG_ERROR(modName, "periodic sem with offset=%dms has failed", i);

	    return 1;	
	}
    }

    return 0;
}

int multiSemaphoresSet(int ticks, int n_sem, unsigned long long offset)
{
    int i, j, stat;
    SEM *sem[48];
    unsigned long long startTime;
    RTIME min[TE_SCHED_MAX_NODES];
    RTIME max[TE_SCHED_MAX_NODES];
    RTIME mean[TE_SCHED_MAX_NODES];
    RTIME var[TE_SCHED_MAX_NODES];
    RTIME rms;
    rtlogRecord_t logRecord;

    startTime = llimd(teHandlerGetTime() + 50000000ULL, 1, TEacs) * TEacs;

    /*
     * check that params are okay
     */
    if ( n_sem > TE_SCHED_MAX_NODES || (n_sem * offset > TEacs ) )
    {
        RTLOG_ERROR(modName, "multiple sems with invalid params (sems/offset=%d/%lldus",
              n_sem, llimd(offset, 1, 10));

        return 1;
    }
    
    /*
     * request for n_sem periodic semaphores, start time set 5 second
     * in the future. Each semaphore has an offset of i*offset
     */
    for ( j = 0; j < n_sem; j++ )
    {
	if ( (sem[j] = teSchedGetSem(startTime, j * offset, 0)) == 0 )
	{
	    RTLOG_ERROR(modName, "cannot get sem (j=%d)", j);

	    return 1;
	}
    }

    /*
     * sleep until the starting tick
     */
    if ( teSchedSleepUntil(startTime) )
    {
        RTLOG_ERROR(modName, "couldn't sleep until starting time!");
        
        return 1;
    }

    /*
     * wait now n times for each semaphore
     */
    for ( i = 0; i < ticks; i++ )
    {
        for ( j = 0; j < n_sem; j++ )
        {
            stat = rt_sem_wait_timed(sem[j], nano2count(6000000000LL));
            if ( stat == 0xFFFF || stat == 0xFFFE )
            {
                RTLOG_ERROR(modName, "did not get the semaphore (i/j/stat=%d/%d/%d)",
		      i, j, stat);
                
                return 1;
            }

            computeStats(i,
                         teHandlerGetTime() - (startTime + i * TEacs + j * offset),
                         &mean[j], &var[j],
                         &min[j], &max[j]); 
        }
    }

    /*
     * release the semaphores
     */
    for ( j = 0; j < n_sem; j++ )
    {
        teSchedRelSem(sem[j]);
    }

    for ( j = 0; j < n_sem; j++ )
    {
        rms = lldSqrt(llimd(var[j], 1, ticks - 1));
        
        RTLOG_INFO(modName, "multiSemaphoresSet offset/mean/rms/min/max = %lld/%lld/%lld/%lld/%lld [us]",
              llimd(offset * j, 1, 10),
              SLLIMD(mean[j], 1, 10),
              SLLIMD(rms, 1, 10),
              SLLIMD(min[j], 1, 10),
              SLLIMD(max[j], 1, 10));
    }

    return 0;
}

/** This simple test verifies attempts to get 17 semaphores. It should be able to
 ** get 16 & then fail on the 17-th semaphore. If these conditions are met, then
 ** the test is successful and if not, it fails with a log message
 */

int testGetSems(void)
{
    SEM *sem[17];
    int i, j;
    rtlogRecord_t logRecord;

    for(i = 0; i < 16; i++ )
    {
        sem[i] = teSchedGetSem(0, 0, 0);
        if( sem[i] == 0 )
        {
            RTLOG_ERROR(modName, "testGetSems() didn't get semaphore %d of 16",i);
              // Release any allocated sems
            for(  j = 0; j < i; j++ )
                teSchedRelSem(sem[j]);

            return 1;
        }
        else
            RTLOG_INFO(modName, "Got semaphore: %d",i);
    }
    // Now try to the the 17-th semaphore, this should fail
    sem[16] = teSchedGetSem(0, 0, 0);
    if( sem[16] != 0 )
    {
        RTLOG_ERROR(modName, "testGetSems() obtained the (invalid) 17-th semaphore");
        teSchedRelSem(sem[16]);  
        return 1;
    }
    return 0;
}

void test_task(long not_used)
{
    int set = 0;
    rtlogRecord_t logRecord;

    /*
     * just to be sure that insmod will finish first than the beginning of
     * this task.
     */
    rt_sleep(nano2count(1000000));

    RTLOG_INFO(modName, "started at cpu=%lld", rt_get_time());

    /*
     * sleep until next tick for N times
     */
    set++;
    RTLOG_INFO(modName, "set #%d - repeated sleep until next tick (repeat=%d)", set, N);
    if ( sleepUntilNextTickSet(N) )
    {
	RTLOG_ERROR(modName, "set %d failed!", set);
	return;
    }

    /*
     * sleep until next second for N times
     */
    set++;
    RTLOG_INFO(modName, "set #%d - repeated sleep until (repeat/offset=%d/%llus)",
          set, N, llimd(10000000ULL, 1, 10000000));
    if ( sleepUntilSet(N, 10000000ULL) ) /* second parm in ACS unitis */
    {
	RTLOG_ERROR(modName, "set %d failed!", set);
	return;
    }

    /*
     * periodic sems at different offsets
     */
    set++; 
    RTLOG_INFO(modName, "set #%d - periodic sems for different offsets (ticks=%d)",
          set, N);
    if ( periodicSemaphoresSet(N) )
    {
	RTLOG_ERROR(modName, "set %d failed!", set);
	return;
    }

    /*
     * simultaneous periodic sems at different offsets
     */
    set++; 
    RTLOG_INFO(modName, "set #%d - simultaneous periodic sems for different offsets (ticks=%d)",
          set, N);
    if ( multiSemaphoresSet(N, 15, 10000ULL) )
    {
	RTLOG_ERROR(modName, "set %d failed!", set);
	return;
    }

    // Test getting semaphores.
    if( testGetSems() )
    {
	RTLOG_ERROR(modName, "testGetSems() failed!");
	return;
    }

    /*
     * this is the log that goes into the tat reference
     */
    RTLOG_INFO(modName, "RESULT: %d test sets successfully executed", set);

    return;
}

/*
 * kernel module handling
 */
int init_module(void)
{
    rtlogRecord_t logRecord;

    RTLOG_INFO(modName, "LOADING MODULE ...");

    /*
     * spawn the main task
     */
    if ( rt_task_init(&task, test_task, 0, 8000, 10, 1, 0)
	 != 0 )
	{
	RTLOG_INFO(modName, "CANNOT SPAWN THE TEST TASK.");
	return 0;
	}

    /*
     * set the just created task as 'ready to run'
     */
    rt_task_resume(&task);

    RTLOG_INFO(modName, "MODULE LOADED.");

    return 0;
}

/*
 * unload stuff
 */
void cleanup_module(void)
{
    rtlogRecord_t logRecord;

    RTLOG_INFO(modName, "MODULE CLEANING UP...");

    /*
     * delete our main task
     */
    rt_task_delete(&task);

    RTLOG_INFO(modName, "MODULE CLEANED UP.");
}

/*___oOo___*/
