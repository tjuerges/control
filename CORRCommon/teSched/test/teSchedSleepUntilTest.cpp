/*******************************************************************************
*
*
* "@(#) $Id$"
*
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* who       when        what
* --------  ----------  ----------------------------------------------
* ramestic  2006-11-20  created
*/

//
// System stuff
//
#include <stdlib.h>
#include <pthread.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <unistd.h>
#include <stdarg.h>
#include <vector>

//
// RTAI stuff
//
#include <rtai_sem.h>

//
// ACS stuff
//
#include <rtLog.h>

//
// COORCommon stuff 
//
#include <rtToolsFifoCmd.h>
#include <teHandler.h>

// 
// Local Headers
//
#include "teSched.h"

using namespace std;

#define modName "teSchedSleepUntilTest"

//
// program parameters
//
int T = 10;
int N = 100;
int D = 100; /* ms */
bool V = false;

//
// global variables
//
pthread_t tasks[50];
vector<vector<int> > randomSleep;

void *testTask(void *idx)
{
    RT_TASK *task;
    int myid = (int)idx;
    unsigned long long t;
    rtlogRecord_t logRecord;

    if ( V )
    {
        cout << "task starting " << myid << endl;
    }

    //
    // creates the buddy
    //
    if ( !(task = rt_task_init_schmod(myid, 
				      10,
				      0,
				      0,
				      SCHED_FIFO,
				      0x00)))
    {
        cout << "cannot init rt task " << myid << endl;

	return 0;
    }

    //
    // goes into RTAI scheduler.
    //
    rt_make_hard_real_time();

    //
    // loop asking for the time and sleeping until
    //
    for ( int i = 0; i < N; i++ )
    {
        if ( (t = teHandlerGetTime()) == 0 )
        {
            RTLOG_ERROR(modName, "teHandlerGetTime failed. Test aborted.");
            
            break;
        }

        if ( teSchedSleepUntil(t + D * 10000LL) )
        {
            RTLOG_ERROR(modName, "teSchedSleepUntil failed. Test aborted.");
            
            break;
        }

        if ( V )
        {
            RTLOG_INFO("test", "task %d at %llu", myid, t);
        }

        rt_sleep(nano2count(randomSleep[myid][i] * 1000000LL));
    }

    //
    // goes back to Linux scheduler
    //
    rt_make_soft_real_time();

    //
    // deallocate rtai's resource
    //
    rt_task_delete(task);

    if ( V )
    {
        cout << "task " << myid << " terminating" << endl;
    }

    return 0;
}

void usage(void)
{
    cout << "usage: teSchedSleepUntilTest [-t <num-tasks>] [-n <num-iterations>] [-d <sleep-until-delay>] [-v]" << endl;
    cout << "default values:" << endl;
    cout << "\tnum-tasks=10" << endl;
    cout << "\tnum-iterations=100" << endl;
    cout << "\ntsleep-until-delay=100ms" << endl; 
    cout << "\tverbose mode off by default" << endl;
}

int main(int argc, char** argv) 
{
    int i, c;

    //
    // discorver user parameters
    //
    while ( (c = getopt(argc, argv, "t:n:d:v")) != -1 )
    {
	switch (c)
	{
        case 't':
            T = atoi(optarg);
            break; 
        case 'n':
            N = atoi(optarg);
            break;
        case 'd':
            D = atoi(optarg);
            break;
        case 'v':
            V = true;
            break;
	default:
            usage();
	    abort();
	}
    }

    //
    // check params
    //
    if ( T > 50 )
    {
        printf("too many tasks requested (req/max=%d/10)!\n", N);

        return 1;
    }

    //
    // log parameters
    //
    cout << "T=" << T << endl;
    cout << "N=" << N << endl;
    cout << "D=" << D << endl;
    cout << "V=" << (V ? "true" : "false") << endl;

    //
    // require root status for threads going real time
    //
    rt_allow_nonroot_hrt();
    
    //
    // rand is not thread safe, therefore, I need to prepare now the random
    // series of sleeping delays that each task will apply for getting 
    // their executions very intermixed among the other ones.
    //
    randomSleep.resize(T);

    //
    // start the tasks
    //
    for ( i = 0; i < T; i++ )
    {
        randomSleep[i].resize(N);
        for ( int j = 0; j < N; j++ )
        {
            randomSleep[i][j] = int(float(rand()) * 16.0 / float(RAND_MAX));
            //cout << i << "\t" << j << "\t" << randomSleep[i][j] << endl;
        }
 
	if ( pthread_create(&tasks[i],
			    0,
			    testTask,
			    (void *)i)
	     != 0 )
	{
	    printf("Cannot create task (%d)\n", i);

	    return 1;
	}
    }

    //
    // join them all
    //
    for ( i = 0; i < T; i++ )
    {
	if ( V )
        {
            printf("joining task %d\n", i);
        }
	pthread_join(tasks[i], 0);
        if ( V )
        {
            printf("task joined %d\n", i);
        }
    }

    return 0;
}

/*___oOo___*/
