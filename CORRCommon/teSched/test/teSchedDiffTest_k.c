/*******************************************************************************
*
* "@(#) $Id$"
*
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* who       when        what
* --------  --------    ----------------------------------------------
* ramestic  2004-03-03  created
*/

/* 
 * System Headers
 */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/delay.h>

/*
 * RTAI specific
 */
#include <rtai_lxrt.h>

/*
 * ACS or CORR specific
 */
#include <rtLog.h>
#include <teHandler.h>

/* 
 * Local Headers
 */
#include "teSched.h"

MODULE_AUTHOR("Rodrigo Amestica");
MODULE_DESCRIPTION("TE scheduler test");
MODULE_LICENSE("GPL");

#define modName		"teSchedDiffTest_k"
#define N		160

/*
 * global variables
 */
int debug = 1;
RT_TASK	task;

/*
 * helper function
 */
void updateStats(RTIME val, RTIME *min, RTIME *max, RTIME *aver)
{
    if ( val < *min )
    {
	*min = val;
    }
    if ( val > *max )
    {
	*max = val;
    }
    *aver += val;
}

RTIME llfloor(RTIME val, int div)
{
    RTIME res;

    res = llimd(val, 1, div) * div;

    if ( res > val )
    {
	return res - div;
    }

    return res;
}

void test_task(long not_used)
{
    int i, stat;
    SEM *sem1, *sem2;
    RTIME times[N][5];
    RTIME sem1_min = 9999999, sem1_aver = 0, sem1_max = -9999999;
    RTIME sem2_min = 9999999, sem2_aver = 0, sem2_max = -9999999;
    RTIME diff_min = 9999999, diff_aver = 0, diff_max = -9999999;
    rtlogRecord_t logRecord;

    RTLOG_INFO(modName, "started at cpu=%lld", rt_get_time());

    /*
     * request a periodic semaphore with null offset
     */
    if ( (sem1 = teSchedGetSem(0, 0, 0)) == 0 )
    {
	RTLOG_ERROR(modName, "cannot get sem1");
	return;
    }

    /*
     * request a periodic semaphore with 24ms offset
     */
    if ( (sem2 = teSchedGetSem(0, TEacs >> 1, 0)) == 0 )
    {
	RTLOG_ERROR(modName, "cannot get sem2");
	teSchedRelSem(sem1);
	return;
    }

    /*
     * synchronize with next tick
     */
    if ( teSchedSleepUntil(0) )
    {
	RTLOG_ERROR(modName, "cannot synchronize with next tick!");
	return;
    }
    
    /*
     * wait for semaphores for a few times and record related timing data
     */
    for ( i = 0; i < N; i++ )
    {
	/*
	 * take the first semaphore
	 */
	if ( (stat = rt_sem_wait_timed(sem1,
				       nano2count(TEns)))
	     == 0xFFFF || stat == 0xFFFE )
	{
	    RTLOG_ERROR(modName, "did not get sem1!");
	    teSchedRelSem(sem1);
	    teSchedRelSem(sem2);
	    return;
	}

	/*
	 * record timestamp
	 */
	times[i][0] = teHandlerGetTime();
	
	/*
	 * take the second semaphore
	 */
	if ( (stat = rt_sem_wait_timed(sem2,
				       nano2count(TEns)))
	     == 0xFFFF || stat == 0xFFFE )
	{
	    RTLOG_ERROR(modName, "did not get sem2!");
	    teSchedRelSem(sem1);
	    teSchedRelSem(sem2);
	    return;
	}

	/*
	 * record timestamp
	 */
	times[i][1] = teHandlerGetTime();

    }

    /*
     * release semaphores
     */
    teSchedRelSem(sem1);
    teSchedRelSem(sem2);

    /*
     * compute time differences
     *	0: sem1 recorded above
     *	1: sem2 recorded above
     *	2: sem1 offset from previous TE 
     *	3: sem2 offset from previous TE 
     *	4: sem2-sem1
     */
    for ( i = 0; i < N; i++ )
    {
	times[i][2] = times[i][0] - llfloor(times[i][0], TEacs);
	times[i][3] = times[i][1] - llfloor(times[i][1], TEacs);
	times[i][4] = times[i][1] - times[i][0];
	RTLOG_INFO(modName, "%d/%lld/%lld/%lld",
	      i, times[i][2], times[i][3], times[i][4]);
	rt_sleep(nano2count(1000000LL));
    }

    /*
     * compute statistics in micro-seconds
     */
    for ( i = 0; i < N; i++ )
    {
	updateStats(llimd(times[i][2], 1, 10), &sem1_min, &sem1_max, &sem1_aver);
	updateStats(llimd(times[i][3], 1, 10), &sem2_min, &sem2_max, &sem2_aver);
	updateStats(llimd(times[i][4], 1, 10), &diff_min, &diff_max, &diff_aver);
    }
    sem1_aver = llimd(sem1_aver, 1, N);
    sem2_aver = llimd(sem2_aver, 1, N);
    diff_aver = llimd(diff_aver, 1, N);

    RTLOG_INFO(modName, "sem1 min/aver/max=%lld/%lld/%lld [us]",
	  sem1_min, sem1_aver, sem1_max);
    RTLOG_INFO(modName, "sem2 min/aver/max=%lld/%lld/%lld [us]",
	  sem2_min, sem2_aver, sem2_max);
    RTLOG_INFO(modName, "diff min/aver/max=%lld/%lld/%lld [us]",
	  diff_min, diff_aver, diff_max);

}

/*
 * kernel module handling
 */
int init_module(void)
{
    rtlogRecord_t logRecord;

    RTLOG_INFO(modName, "LOADING MODULE ...");

    /*
     * spawn the main task
     */
    if ( rt_task_init(&task, test_task, 0, 8000, 10, 1, 0)
	 != 0 )
    {
	RTLOG_INFO(modName, "CANNOT SPAWN THE TEST TASK.");
	return 0;
    }

    /*
     * set the just created task as 'ready to run'
     */
    rt_task_resume(&task);
    
    RTLOG_INFO(modName, "MODULE LOADED.");

    return 0;
}

//-----------------------------------------------------------------------------
void cleanup_module(void)
{
    rtlogRecord_t logRecord;

    RTLOG_INFO(modName, "MODULE CLEANING UP...");

    /*
     * delete our main task
     */
    rt_task_delete(&task);

    RTLOG_INFO(modName, "MODULE CLEANED UP.");
}

/*___oOo___*/
