#!/bin/bash

max_iter=100 # maximal number of iterations (each 1second) == sleep 100

i=0

while [ "X$result" == "X" -a $i -lt $max_iter ]
do
  sleep 1
  i=`expr $i + 1`
  result=`/bin/dmesg -s 1 | grep teSchedTest_k | grep RESULT`
done

if [ "X$result" != "X" ]
then
    echo $result
else
    echo "No result after $max_iter * 1sec !!"
fi
