#ifndef teSched_H
#define teSched_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* ramestic  2003-10-27  created
*/

/** @file teSched.h
 *  @brief TE scheduler kernel module API.
 *
 * The TE scheduler module provides a facility for tasks scheduling (semaphore
 * signaling) at an Array Time precission.  
 *
 * This module connects to the TE handler mail-box and retrieves updated clock 
 * data on each TE. At the same time is provides an API for user space processes
 * and kernel space threads who need to synchronize activities at given 
 * absolute time stamps (ACS units). This API allows those processes and tasks
 * to request a semaphore (alloctated and initialized by the TE scheduler module
 * itself) that will be given at the requested time stamp, either in a one-shot
 * fashion or periodically every 48ms.
 *
 * The module is physically deployed by means of the following tasks:
 *
 *	1. Main task: it synchronize with the TE handler by reading its clock
 *	   data mail-box. At each tick the data received is copied into local
 *	   memory and used for absolute time convertions to 
 *	2. Activation tasks:
 */

#include <rtai_sem.h>

#ifndef __KERNEL__
  #include <rtai_lxrt.h>
#endif

/*
 * maximum number of simultaneous requests.
 */
#define TE_SCHED_MAX_NODES	16

/*
 * extending user space suport
 */
#define TE_SCHED_GETSEM		0
#define TE_SCHED_RELSEM		1
#define TE_SCHED_SLEEPUNTIL	2

/*
 * public API
 */
#ifndef __KERNEL__

#include <rtTools.h>

SEM *teSchedGetSem(unsigned long long start,unsigned long long offset,
		   unsigned int repeat)
{
    struct 
    {
	unsigned long long _start; 
	unsigned long long _offset;
	unsigned int _repeat;
    } arg = { start, offset, repeat };
    return (SEM *)rt_get_adr(rtai_lxrt(TE_SCHED_IDX,
				       SIZARG,
				       TE_SCHED_GETSEM,
				       &arg).i[LOW]);
}
void teSchedRelSem(SEM *sem)
{
    struct {unsigned long _name;} arg = {rt_get_name(sem)};
    rtai_lxrt(TE_SCHED_IDX,
	      SIZARG,
	      TE_SCHED_RELSEM,
	      &arg).i[LOW];
}
int teSchedSleepUntil(unsigned long long time)
{
    struct {unsigned long long _time;} arg = {time};
    return rtai_lxrt(TE_SCHED_IDX,
		     SIZARG,
		     TE_SCHED_SLEEPUNTIL,
		     &arg).i[LOW];
}
#else
/*
 * the following trick required for letting the module compile on 2.6.15 + 3.3
 */
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,19)
#ifdef RTAI_SYSCALL_MODE
#undef RTAI_SYSCALL_MODE
#endif
#define RTAI_SYSCALL_MODE
#endif

SEM *teSchedGetSem(unsigned long long start,
                                     unsigned long long offset,
                                     unsigned int repeat);
void teSchedRelSem(SEM *sem);
int RTAI_SYSCALL_MODE teSchedSleepUntil(unsigned long long time);
#endif

#endif /*!teSched_H*/
