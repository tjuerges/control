#ifndef teSchedPrivate_H
#define teSchedPrivate_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* ramestic  2003-10-14  created
*/

#include "teSched.h"

/*
 * some timeout and other type of constants
 */
#define TE_SCHED_ACCESS_TO		1000000LL	/* 1ms		*/
#define TE_SCHED_RWL_TO			500000LL	/* 500us	*/
#define TE_SCHED_MAX_MISSED_UPDATES	5


/* task stack sizes */
#define TE_SCHED_MAIN_TASK_STACK	8192
#define TE_SCHED_ACT_TASK_STACK		8192

/* node states */
#define TE_SCHED_NODE_FREE	0	/* unlinked (no task)			*/
#define TE_SCHED_NODE_WAIT	1	/* linked and waiting for task spawning	*/
#define TE_SCHED_NODE_RUN	2	/* unlinked and task running		*/
#define TE_SCHED_NODE_END	3	/* unlinked and task finished		*/

/*
 * scheduling node
 */
typedef struct teSchedNode
{
    int			state;	/**/
    unsigned int	spawn;	/* tick the task is spawned */
    unsigned int	start;  /* tick the sem is given the first time */
    RTIME		offset;	/**/
    unsigned int	repeat;	/**/
    SEM			sem;	/**/
    unsigned long       name;	/* sem name	*/
    RT_TASK		task;	/**/
} teSchedNode_t;

#endif /*!teSchedPrivate_H*/
