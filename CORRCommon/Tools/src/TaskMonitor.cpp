/* @(#) $Id$
 *
 * Copyright (C) 2005
 * Associated Universities, Inc. Washington DC, USA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * Correspondence concerning ALMA Software should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */

#include "TaskMonitor.h"

#include <rtai_sched.h>
#include <rtai_mbx.h>
#include <rtai_lxrt.h>
#include <rtai.h>
#include <iostream>
#include <logging.h>
#include <vector>
#include <string>


using std::vector;
using std::string;
using std::cout;
using std::endl;


pthread_t TaskMonitor::m_pThread;
bool TaskMonitor::m_bRunning;
TaskMonitor::taskStatsVector_t TaskMonitor::m_taskStats;
vector<unsigned long int> TaskMonitor::m_registeredTasks;
int TaskMonitor::m_numRegisteredTasks = 0;

//------------------------------------------------------------------------------
TaskMonitor &TaskMonitor::access( const vector<string> &taskNames)
{

    static TaskMonitor singletonInstance;

    if(taskNames.size()>0 ){

	setNames(taskNames);
    }

    return singletonInstance;
}

//------------------------------------------------------------------------------
TaskMonitor::~TaskMonitor()
{

    if( m_bRunning )
    {
	m_bRunning = false;
    }
}

//------------------------------------------------------------------------------
// private functions
//------------------------------------------------------------------------------
TaskMonitor::TaskMonitor()
{

    m_bRunning = false;
    ACS_SHORT_LOG((LM_DEBUG,"Spawning monitor tasks thread "));
    pthread_create(&m_pThread, NULL, monitorTasksThread, NULL);
    sleep(1);
}

//------------------------------------------------------------------------------
void TaskMonitor::setNames(const vector<string> &taskNames)
{

    RT_TASK *pTask;
    int taskIdx = 0;
    m_numRegisteredTasks = taskNames.size();
    // Create a vector of task stats for each registered task
    m_taskStats = taskStatsVector_t(m_numRegisteredTasks);

    // assign task names as they don't change, i.e., all tasks are static
    for( vector<string>::const_iterator tIter = taskNames.begin();
	 tIter != taskNames.end(); ++tIter )
    {
	strcpy(m_taskStats[taskIdx].taskName, tIter->c_str());
	// get the task pointer
	pTask = (RT_TASK *)rt_get_adr(nam2num(m_taskStats[taskIdx].taskName));
	if( pTask ){

	    m_taskStats[taskIdx].pTask = pTask;
	}
	else{
	    cout <<endl<<"pTask not found"<<endl;
	}
	m_taskStats[taskIdx].cpuUsage = 0.0;
	m_taskStats[taskIdx].taskTime = 0LL;
	taskIdx++;
    }
}

//------------------------------------------------------------------------------
TaskMonitor &TaskMonitor::operator =( const TaskMonitor &rhs)
{
    return *this;
}

//------------------------------------------------------------------------------
void *TaskMonitor::monitorTasksThread(void *args)
{
       
    RTIME exectime[3] = {0LL, 0LL, 0LL};
    int delaySecondsBetweenMeasurements = 10;
 
    m_bRunning = true;
    int priority = 250;  // the lowest priority task
    unsigned long int RTAIResource = nam2num("TSKMON");
    RT_TASK *pMonitorTask = rt_task_init(RTAIResource, priority, 8192,0);

    while( m_bRunning )
    {
	for( taskStatsVector_t::iterator tIter = m_taskStats.begin();
	     tIter != m_taskStats.end(); ++tIter )
	{
	    if( tIter->pTask )
	    {     
		rt_get_exectime(tIter->pTask, exectime);

		if( exectime[2] != exectime[1] )
		{
		    tIter->cpuUsage = 
			(double)(exectime[0] * 100.0 / (double)(exectime[2] - exectime[1]));
		    tIter->taskTime = exectime[0];
		}
	    }
	}

	sleep(delaySecondsBetweenMeasurements);
    }
    rt_task_delete(pMonitorTask);
    pthread_exit(NULL);
}
