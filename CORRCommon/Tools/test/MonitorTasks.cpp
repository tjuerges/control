/* @(#) $Id$
 *
 * Copyright (C) 2005
 * Associated Universities, Inc. Washington DC, USA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * Correspondence concerning ALMA Software should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */

#include <TaskMonitor.h>
#include <iostream>
#include <vector>
#include <string>
#include <signal.h>
#include <rtai_sched.h>
#include <rtai_lxrt.h>
#include <logging.h>

/** This is a stand-alone application which monitors tasks whose names are
 ** provided as command line parameters. It runs indefinitely requesting 
 ** task status every 10 seconds and displaying the results on std. out. 
 ** Ctrl-C ends it.
 **
 ** Usage:
 ** MonitorTasks MTSK BTSK
 */

using namespace std;

bool status = false;
bool bRunning = false;
bool m_Running = false;
bool b_Running = false;
bool c_Running = false;
bool  m_Task = false;
bool  b_Task = false;

static pthread_t m_pThread, b_pThread, c_pThread;

void* mTaskThread(void *args);
void* bTaskThread(void *args);
void* cTaskThread(void *args);

//--------------------------------------------------------------------------------------

void TerminationSignalHandler(int)
{
    m_Running  = false;
    b_Running  = false;
    c_Running  = false;    
    sleep(2);
    status = true;
}

//--------------------------------------------------------------------------------------

int main(int argc, char **argv)
{

    if( argc < 2 )
    {
	cout << "No list of tasks to monitor" << endl;
	return -1;
    }

    rt_set_oneshot_mode();
    start_rt_timer(0);
    pthread_create(&m_pThread, NULL, mTaskThread, NULL);
    pthread_create(&b_pThread, NULL, bTaskThread, NULL);
//    pthread_create(&c_pThread, NULL, cTaskThread, NULL);

/***************************************************************************/  
    // Allow Ctrl-C to end me.

    signal(SIGINT, TerminationSignalHandler);
    vector<string> taskNames;

    TaskMonitor::taskStatsVector_t taskStats;
    for( int t = 1; t < argc; t++ )
	taskNames.push_back(string(argv[t]));

    cout << endl;
    cout << "Monitoring " << taskNames.size() << " tasks, press ^C to exit..." << endl;
    TaskMonitor::access(taskNames);

    bRunning = true;
    cout <<endl;
    cout << "Task Name \tRun Time (ms) \tCPU Usage % " << endl;

    while( bRunning )
    {
	// Get task monitoring stats
        TaskMonitor::access().getTaskStatistics(taskStats);

	for( TaskMonitor::taskStatsVector_t::iterator ntmIter = taskStats.begin();
	     ntmIter != taskStats.end(); ++ntmIter )
	{
	    // Only hard real-time tasks are have non-zero times

	    if( ntmIter->taskTime > 0LL )
	    {
		cout << ntmIter->taskName << " \t\t";
		cout << (int) (ntmIter->taskTime / 1000000LL) << " \t\t";
		cout << ntmIter->cpuUsage << endl;

	    }
	}
	cout <<"--------------------------------------------"<<endl;

	// sleep 10 seconds B-4 requesting the task stats again.	
	sleep(10);

	if(status){
	   if (bRunning){
	       m_Running  = false;
	       b_Running  = false;
	       sleep(2);
	       bRunning = false;
	   }
	}
    }
    sleep(10);
    if ((! m_Task)||(! b_Task)){
        m_Running  = false;
	b_Running  = false;
	sleep(2);
    }
    cout <<endl<<"Monitoring Process Terminated ..........."<<endl;
    TaskMonitor::access().~TaskMonitor();
    sleep(10);
    stop_rt_timer();
    printf("\n Exiting....... \n");
    return 0;
}

/***************************************************************************/
//                    mTaskThread
/***************************************************************************/
// This Task main function is to run the CPU for 50% of the time and be idle
// for 50% of the time
/***************************************************************************/
  
void* mTaskThread(void *args){

    RT_TASK *mTask;

    int m_cpuID = 0;
    unsigned int m_cpuMask = 0x1 << m_cpuID;
    unsigned long int mtsk_name = nam2num("MTSK");
    
    m_Running = true;
    
    if (!(mTask = (RT_TASK*)rt_task_init(mtsk_name, 1, 0, 0)))
    {
        printf("\n Cannot Init Task =  %lx \n", mtsk_name);
	exit(1);
    }
    else{
        cout<<endl<< "MTSK Created" <<endl; 
	rt_set_runnable_on_cpus(mTask, m_cpuMask);
        rt_make_hard_real_time();
     
	while(m_Running){
	    rt_sleep(nano2count(1000000));
	    rt_busy_sleep(1000000);
	}
    }   

    rt_task_delete(mTask);
    m_Task = true;
    cout<<endl<< "(MTSK) pthread Exit......" <<endl<<endl;
    pthread_exit(NULL); 
}

/***************************************************************************/
//                    bTaskThread
/***************************************************************************/
// This Task main function is to run the CPU for 75% of the time and be idle
// for 25% of the time
/***************************************************************************/

void* bTaskThread(void *args){

    RT_TASK *bTask;

    int b_cpuID = 0;
    unsigned int b_cpuMask = 0x1 << b_cpuID;
    unsigned long int btsk_name = nam2num("BTSK");

    b_Running = true;

    if (!(bTask = (RT_TASK*)rt_task_init(btsk_name, 2, 0, 0)))
    {
        printf("\n Cannot Init Task BTSK \n" );
	exit(1);
    }
    else{ 
        cout<<endl<< "BTSK Created" <<endl; 
 	rt_set_runnable_on_cpus(bTask, b_cpuMask);
        rt_make_hard_real_time();

	while(b_Running){
	    rt_sleep(nano2count(1500000));
	    rt_busy_sleep(500000);
	}
    }  
 
    rt_task_delete(bTask);
    b_Task = true;
    cout<<endl<< "(BTSK) pthread Exit......" <<endl;
    pthread_exit(NULL);
}

/***************************************************************************/
//                    cTaskThread
/***************************************************************************/
// This Task main function is to perform some arithmetic calculation for 1 ms
// and sleep for 1 ms
/***************************************************************************/

void* cTaskThread(void *args){

    RT_TASK *cTask;
    double x = 1000000000;
    int counter = 9097;
    double y = 2; 
    int c_cpuID = 0;
    unsigned int c_cpuMask = 0x1 << c_cpuID;
    unsigned long int ctsk_name = nam2num("CTSK");
    RTIME rtime1;
    c_Running = true;

    if (!(cTask = (RT_TASK*)rt_task_init(ctsk_name, 1, 0, 0)))
    {
        printf("\n Cannot Init Task BTSK \n" );
	exit(1);
    }
    else{   
        cout<<endl<< "CTSK Created" <<endl;  
	rt_set_runnable_on_cpus(cTask, c_cpuMask);  
        rt_make_hard_real_time();
	rtime1 = rt_get_time_cpuid(c_cpuMask);

	while(c_Running){

	   while(counter>0){

	      x = (x/y) * sin(0.25);
	      counter--;
	   }
	   rt_sleep(nano2count(1000000));
	   counter = 9097;
	   x = 1000000000;
	}
    }  

    rt_task_delete(cTask);
    cout<<endl<< "(CTSK) pthread Exit......" <<endl;
    pthread_exit(NULL);
}
/***************************************************************************/
