/* @(#) $Id$
 *
 * Copyright (C) 2005
 * Associated Universities, Inc. Washington DC, USA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * Correspondence concerning ALMA Software should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */

#include <TaskMonitor.h>
#include <iostream>
#include <rtai_lxrt.h>
#include <logging.h>

/** This tests the TaskMonitor class by running 2 hard real-time tasks, one
 ** which runs 50% & another which runs 25%. The test requests task status every
 ** 10 seconds and displaying the results on std. out. 
 ** It ends after 5 iterations
 */

using namespace std;

bool status = false;
bool bRunning = false;
bool m_Running = false;
bool b_Running = false;
bool c_Running = false;
bool  m_Task = false;
bool  b_Task = false;

static pthread_t m_pThread, b_pThread;

void* mTaskThread(void *args);
void* bTaskThread(void *args);
void* cTaskThread(void *args);

//--------------------------------------------------------------------------------------
void launchTasks()
{
    TaskMonitor::taskStatsVector_t taskStats;
    vector<string> taskNames;
    
    taskNames.push_back(string("MTSK"));
    taskNames.push_back(string("BTSK"));

    TaskMonitor &tmRef = TaskMonitor::access(taskNames);

    bRunning = true;

    cout << endl << "Task Name \tRun Time (ms) \tCPU Usage % " << endl;
    cout <<"============================================"<<endl;

    for( int i = 0; i < 5; i++ )
    {
	// sleep 10 seconds B-4 requesting the task stats again.	
	sleep(10);

	// Get task monitoring stats
        tmRef.getTaskStatistics(taskStats);

	for( TaskMonitor::taskStatsVector_t::iterator ntmIter = taskStats.begin();
	     ntmIter != taskStats.end(); ++ntmIter )
	{
	    // Only hard real-time tasks are have non-zero times
	    if( ntmIter->taskTime > 0LL )
	    {
		cout << ntmIter->taskName << " \t\t";
		cout << (int) (ntmIter->taskTime / 1000000LL) << " \t\t";
 		cout << (int) (ntmIter->cpuUsage + 0.5) << endl;
	    }
	}
	cout <<"--------------------------------------------"<<endl;

	if(status)
	{
	   if (bRunning)
	   {
	       m_Running  = false;
	       b_Running  = false;
	       sleep(1);
	       bRunning = false;
	   }
	}
    }
    sleep(1);
    if ( (!m_Task) || (!b_Task))
    {
        m_Running  = false;
	b_Running  = false;
	sleep(1);
    }
}

//--------------------------------------------------------------------------------------
int main(int argc, char **argv)
{
    rt_set_oneshot_mode();
    start_rt_timer(0);

    pthread_create(&m_pThread, NULL, mTaskThread, NULL);
    pthread_create(&b_pThread, NULL, bTaskThread, NULL);

    launchTasks();
    m_Running  = false;
    b_Running  = false;
    c_Running  = false;    
    status = true;
    cout <<endl<<"Monitoring Process Terminated ..........."<<endl;
    TaskMonitor::access().~TaskMonitor();
    sleep(1);
    stop_rt_timer();
    return 0;
}

/***************************************************************************/
//                    mTaskThread
/***************************************************************************/
// This Task main function is to run the CPU for 50% of the time and be idle
// for 50% of the time
/***************************************************************************/
  
void* mTaskThread(void *args){

    RT_TASK *mTask;

    int m_cpuID = 0;
    unsigned int m_cpuMask = 0x1 << m_cpuID;
    unsigned long int mtsk_name = nam2num("MTSK");
    
    m_Running = true;
    
    // allow execution as non-root user - required for rt_task_init_schmod()
    rt_allow_nonroot_hrt();

    mlockall(MCL_CURRENT | MCL_FUTURE);

    // start rtai task environment
    mTask = rt_task_init_schmod(mtsk_name,10, 8192, 0, SCHED_FIFO, 1);
    if( !mTask )
    {
        printf("\n Cannot Init Task =  %lx \n", mtsk_name);
	exit(1);
    }
    else{
        cout << "MTSK Created" <<endl; 
	rt_set_runnable_on_cpus(mTask, m_cpuMask);
        rt_make_hard_real_time();
     
	while(m_Running){
	    rt_sleep(nano2count(1000000));
	    rt_busy_sleep(1000000);
	}
    }   

    rt_task_delete(mTask);
    m_Task = true;
    cout << "(MTSK) pthread Exit......" << endl;
    pthread_exit(NULL); 
}

/***************************************************************************/
//                    bTaskThread
/***************************************************************************/
// This Task main function is to run the CPU for 75% of the time and be idle
// for 25% of the time
/***************************************************************************/

void* bTaskThread(void *args){

    RT_TASK *bTask;

    int b_cpuID = 0;
    unsigned int b_cpuMask = 0x1 << b_cpuID;
    unsigned long int btsk_name = nam2num("BTSK");

    b_Running = true;

    // allow execution as non-root user - required for rt_task_init_schmod()
    rt_allow_nonroot_hrt();

    mlockall(MCL_CURRENT | MCL_FUTURE);

    // start rtai task environment
    bTask = rt_task_init_schmod(btsk_name,10, 8192, 0, SCHED_FIFO, 1);
    if( !bTask )
    {
        cout << endl << "Cannot Init Task BTSK" << endl;
	exit(1);
    }
    else{ 
        cout << "BTSK Created" <<endl; 
 	rt_set_runnable_on_cpus(bTask, b_cpuMask);
        rt_make_hard_real_time();

	while(b_Running){
	    rt_sleep(nano2count(1500000));
	    rt_busy_sleep(500000);
	}
    }  
 
    rt_task_delete(bTask);
    b_Task = true;
    cout  << "(BTSK) pthread Exit......" << endl;
    pthread_exit(NULL);
}


