/* @(#) $Id$
 *
 * Copyright (C) 2005
 * Associated Universities, Inc. Washington DC, USA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * Correspondence concerning ALMA Software should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */

#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestCaller.h>
#include <cppunit/ui/text/TestRunner.h>

#include <TaskMonitor.h>
#include <CPUIdle.h>
#include <string.h>
#include <iostream>

using namespace std;

/** This is a test application based on CppUnit to test the task monitor
 ** software. It requires rtai_hal & rtai_lxrt so it cannot be part of TAT at
 ** this time due to the loadLkmModule problem with rtai_lxrt.
 */
class testTaskMonitor : public CppUnit::TestFixture
{
public:
    /// default constructor
    testTaskMonitor()
	: CppUnit::TestFixture(){}

    /// copy constructor
    testTaskMonitor( const testTaskMonitor &toCopy)
	{ *this = toCopy; }

    /// assignment operator
    testTaskMonitor &operator =( const testTaskMonitor &)
         { return *this; }

    /// destructor
    ~testTaskMonitor() {}

    static CppUnit::Test *suite();

    void setUp();
    void tearDown();

private:

    /// Test constructors
    void testCtors();
    
    /// Timing routines
    struct timeval _tstart, _tend;
    struct timezone tz;
    void tstart(void)
	{ gettimeofday(&_tstart, &tz); }
    void tend(void)
	{ gettimeofday(&_tend,&tz); }

    double tval()
    {
        double t1, t2;
        t1 =  (double)_tstart.tv_sec + (double)_tstart.tv_usec/(1000*1000);
        t2 =  (double)_tend.tv_sec + (double)_tend.tv_usec/(1000*1000);
        return t2-t1;
    }
};

/*
Macro examples
    CPPUNIT_ASSERT_EQUAL_MESSAGE("msg", expected value, actual value);
    CPPUNIT_ASSERT_MESSAGE("msg", boolean value );
    CPPUNIT_ASSERT( boolean value ); 
*/
const int NUMBER_OF_TEST_TASKS = 10;
//------------------------------------------------------------------------------
void testTaskMonitor::setUp()
{
    vector<string> vStrings;
    string tStr;
    char buf[7];

    for( int i = 0; i < NUMBER_OF_TEST_TASKS; i++ )
    {
	sprintf(buf,"TASK%02d",i);
	vStrings.push_back(string(buf));
    }
    TaskMonitor::access(vStrings);
}

//------------------------------------------------------------------------------
void testTaskMonitor::tearDown()
{
}

//------------------------------------------------------------------------------
CppUnit::Test *testTaskMonitor::suite()
{
    CppUnit::TestSuite *suiteOfTests = new CppUnit::TestSuite( "testTaskMonitor" );
    suiteOfTests->addTest( new CppUnit::TestCaller<testTaskMonitor>(
                       "testCtors", 
                       &testTaskMonitor::testCtors ) );
    return suiteOfTests;
}

//------------------------------------------------------------------------------
void testTaskMonitor::testCtors()
{
    vector<TaskMonitor::taskStats_t> taskStats;
    TaskMonitor &tmRef = TaskMonitor::access();
    char buf[7];
    tmRef.getTaskStatistics( taskStats);

    CPPUNIT_ASSERT_EQUAL_MESSAGE("Size of task stats vector invalid",
				 NUMBER_OF_TEST_TASKS, (int)taskStats.size());
    int i = 0;
    for(vector<TaskMonitor::taskStats_t>::const_iterator tmIter = taskStats.begin();
	tmIter != taskStats.end(); ++tmIter)
    {
	cout << tmIter->taskName << endl;
	sprintf(buf,"TASK%02d",i++);
	CPPUNIT_ASSERT_MESSAGE("Task name invalid",(strcmp(buf,tmIter->taskName) == 0));
	CPPUNIT_ASSERT_EQUAL_MESSAGE("CPU usage invalid",0.0,tmIter->cpuUsage);
	CPPUNIT_ASSERT_EQUAL_MESSAGE("Task ptr",(RT_TASK *)0,tmIter->pTask);
	CPPUNIT_ASSERT_EQUAL_MESSAGE("Task time",0LL,tmIter->taskTime);
    }
}

//------------------------------------------------------------------------------
int main( int, char ** )
{
    CppUnit::TextUi::TestRunner runner;
    runner.addTest( testTaskMonitor::suite() );
    runner.run();
}
