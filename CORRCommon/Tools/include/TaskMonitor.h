#ifndef NODE_TASK_MONITOR_H
  #define NODE_TASK_MONITOR_H
/* @(#) $Id$
 *
 * Copyright (C) 2005
 * Associated Universities, Inc. Washington DC, USA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * Correspondence concerning ALMA Software should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */

#include <pthread.h>
#include <vector>
#include <string>
#include <rtai_sched.h>

/** This class measures CPU usage for all of the tasks running in hard real-time
 ** mode.
 ** It uses the RTAI function rt_get_exectime() which populates an array of 3
 ** elements:
 ** -# [0] The task's accumulated run time
 ** -# [1] When the task started
 ** -# [2] The current time.
 ** The CPU usage for a task is exectime[0] / (exectime[2] - exectime[1]).
 **
 ** In general, one would start the tasks to monitor before instantiating this
 ** singleton class, but the the tasks to monitor can be changed at any time.
 ** 
 ** \b Important This class requires RTAI LXRT. It can be transferred to a
 ** kernel module, but RT-FIFOs would be required to transmit the CPU usage data
 ** from kernel space to user space & to configure TaskMonitor. RTAI LXRT simplifies
 ** this usage by making it directly callable from user space.
 **
 ** To use the TaskMonitor, simply build up a vector of strings of RTAI task
 ** names which have been registered using rt_register(). Then pass this vector as
 ** a parameter to TaskMonitor::access() which constructs the thread that monitors
 ** those tasks and measures their CPU usage. The task list to monitor can be changed
 ** at any time by providing a new vector of strings of RTAI task names to 
 ** TaskMonitor::access(). The previous list of tasks is discarded and replaced 
 ** with the new list. There is no way to 'add' tasks to the list.
 **
 ** \todo Remove the LXRT dependency and use only kernel modules with RT-FIFOs
 ** \todo Add tasks to monitor to an existing list of tasks.
 */

class TaskMonitor
{
public:
    /** \struct Holds the task name, accumulated CPU time, CPU usage, & a
     ** pointer to the tast (for internal usage).
     */
    struct taskStats_t
    {
	/** The percentage CPU usage of the task calcuated as <verbatim>
	    100.0 x taskRunTime / (taskStartTime - currentTime) </verbatim>
	*/
	double cpuUsage;
	long long int taskTime; ///< Accumulated CPU time a tasks runs in nanoseconds
	RT_TASK *pTask;	   ///< Pointer to task to monitor
	char taskName[7];      ///< RTAI task name - at most 6 characters
    };

    /// \typedef A vector of taskStats_t
    typedef std::vector<taskStats_t> taskStatsVector_t;

    /** This access method returns the task monitoring singleton.
     ** @param taskNames A vector of strings which contains the (up to 6 character) 
     **	   name of the real-time tasks to monitor. If taskNames is not empty, then
     **	   any existing tasks are removed from the monitoring list and replaced
     **	   with those in taskNames. If taskNames is empty, then no replacement
     **	   takes place and the previously registered tasks are monitored.
     */
    static TaskMonitor &access( const std::vector<std::string> &taskNames =  std::vector<std::string>());

    /// destructor
    ~TaskMonitor();


    /** Get the statistics.
     ** @return taskStats - The vector of task statistics. This allows the calling
     **	   function to display the task statistics as it pleases.
     */
    static void getTaskStatistics( taskStatsVector_t &taskStats )
	{ taskStats = m_taskStats; }
    
private:
    /// Thread which runs & collects CPU usage info
    static pthread_t m_pThread;
    /// Flag for running/stopping thread
    static bool m_bRunning;
    /// structure containing usage stats
    static taskStatsVector_t m_taskStats;

    /// Number of registered tasks
    static int m_numRegisteredTasks;

    /// Registered tasks
    static std::vector<unsigned long int> m_registeredTasks;

    /** The underlying function which sets the names to monitor. 
     ** @param taskNames A vector of task names to monitor. Any existing tasks
     ** currently being monitored are replaced with those in taskNames.
     */
    static void setNames(const std::vector<std::string> &taskNames );

    /** This is the pthread function that monitors the requested tasks. It uses
     ** the RTAI rt_get_exectime() function from an LXRT task running in soft
     ** real-time.
     ** @param args Not used, but required by the pthread interface
     ** @return Nothing, again required by the pthread interface
     */
    static void *monitorTasksThread(void *args);

    /// \name Singleton class - no copies allowed
    //@{
    /// default constructor
    TaskMonitor();

    /// copy constructor
    TaskMonitor( const TaskMonitor &toCopy)
	{ *this = toCopy; }

    /// assignment operator
    TaskMonitor &operator =( const TaskMonitor &);
    //@}
};
#endif
