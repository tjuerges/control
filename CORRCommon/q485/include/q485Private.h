#ifndef q485Private_H
#define q485Private_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2004 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when        what
* --------  ----------  ----------------------------------------------
* ramestic  2009-06-19  created
*/

/*
 * System Headers
 */
#include <linux/moduleparam.h>
#include <asm/atomic.h>
#include <linux/pci.h>
#include <linux/fs.h>

/*
 * RTAI specific
 */
#include <rtai_sched.h>

/*
 * ACS stuff
 */
#include <rtLog.h>
#include <rtTools.h>

/*
 * board identification
 */
#define Q485_VENDOR_ID 0x135C
#define Q485_DEVICE_ID 0x01C0

/*
 * registers offset in mapped io region
 */
#define Q485_RBR_REG 0x0
#define Q485_IER_REG 0x1
#define Q485_IIR_REG 0x2
#define Q485_FCR_REG 0x2
#define Q485_LCR_REG 0x3
#define Q485_MCR_REG 0x4
#define Q485_LSR_REG 0x5
#define Q485_MSR_REG 0x6
#define Q485_OPT_REG 0x7

/*
 * device structure
 */
typedef struct
{
    /*
     * basically used (so far) for having access to is_enable during
     * initialization
     */
    struct pci_dev *pci;

    /*
     * mapping of registers to memory.
     */
    void __iomem *regs_mem;

    /*
     * pointer to external TE handler.
     */
    void (*handler)(int);

} q485_device_t;

/*
 * driver structure
 */
typedef struct
{
    struct pci_driver drv;
    q485_device_t dev;
    struct pci_device_id dev_ids[];
} q485_driver_t;

/*
 * probe the board and initialize it. Interrupts not enabled.
 */
int q485Probe(struct pci_dev *dev, const struct pci_device_id *id);

/*
 * remove device and driver
 */
void q485Remove(struct pci_dev *dev);

/*
 * activate interrupts on rising edge of the TE pulse.
 * This prototype is provided here only for the debug option, it is
 * not required for a real life operation of the board with TE handler.
 */
int enableTE(void (*handler)(int));

/*
 * deactive TE interrupts.
 * This prototype is provided here only for the debug option, it is
 * not required for a real life operation of the board with TE handler.
 */
int disableTE(void);

#endif /*!q485Private_H*/

/*___oOo___*/
