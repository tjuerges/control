/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2004 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* 
*
* who       when      what
* --------  --------  ----------------------------------------------
* ramestic  2004-06-22  created
*/

/* 
 * Local stuff
 */
#include "q485Private.h"

#define modName "q485"

extern q485_driver_t *drv_p;

/*
 * interrupt service routine.
 */
void isr(int irq)
{
    unsigned int msr;
    rtlogRecord_t logRecord;

    /*
     * we need to take action when the signal is up, which is indicated by 
     * the CTS bit in the MSR register.
     * Note: reading this register also resets the interrupt.
     */
    if ( (msr = ioread8(drv_p->dev.regs_mem + Q485_MSR_REG)) & 0x10 )
    {
        if ( drv_p->dev.handler )
        {
            /*
             * passing irq means that the rtai uinterrup is reenabled there.
             */
            drv_p->dev.handler(irq);

            return;
        }
        else
        {
            RTLOG_INFO(modName,
                       "irq/now/msr=%d/%lld/0x%x",
                       irq,
                       rt_get_time_ns(),
                       msr);
        }
    }

    rt_enable_irq(irq);
}

/*
 * function used for enabling interrupts.
 */
int enableTE(void (*handler)(int))
{
    int status;
    rtlogRecord_t logRecord;

    /*
     * verify that the board is already enabled.
     */
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,19)
    if ( drv_p->dev.pci == 0 || atomic_read(&drv_p->dev.pci->enable_cnt) == 0 )
#else
    if ( drv_p->dev.pci == 0 || drv_p->dev.pci->is_enabled == 0 )
#endif
    {
        RTLOG_ERROR(modName, "board is not enabled");

        return 1;
    }

    /*
     * install our isr to our interrupt request line
     */
    if ( (status = rt_request_global_irq(drv_p->dev.pci->irq, (void *)isr)) )
    {
        RTLOG_ERROR(modName,
                    "failed to request irq (irq/err=%d/%d)!",
                    drv_p->dev.pci->irq, status);
        
	return 1;
    }

    /*
     * setup interrupt on CTS status change.
     */
    iowrite8(0x0b, drv_p->dev.regs_mem + Q485_MCR_REG); // CTS
    iowrite8(0x06, drv_p->dev.regs_mem + Q485_FCR_REG); // disable fifo
    iowrite8(0x08, drv_p->dev.regs_mem + Q485_IER_REG); // Modem status interrupt

    /*
     * just for logging sake
     */
    RTLOG_INFO(modName, "LSR=0x%x", ioread8(drv_p->dev.regs_mem + Q485_LSR_REG));
    RTLOG_INFO(modName, "RBR=0x%x", ioread8(drv_p->dev.regs_mem + Q485_RBR_REG));
    RTLOG_INFO(modName, "IIR=0x%x", ioread8(drv_p->dev.regs_mem + Q485_IIR_REG));
    RTLOG_INFO(modName, "MSR=0x%x", ioread8(drv_p->dev.regs_mem + Q485_MSR_REG));

    /*
     * external function to call from isr
     */
    drv_p->dev.handler = handler;

    /*
     * enable the PCI interrupt
     */
    rt_startup_irq(drv_p->dev.pci->irq);
    rt_enable_irq(drv_p->dev.pci->irq);

    RTLOG_INFO(modName, "interrupt enabled");

    return 0;
}
EXPORT_SYMBOL(enableTE);

/*___oOo___*/
