/*******************************************************************************
 *
 * "@(#) $Id$"
 *
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2003
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * who       when        what
 * --------  ----------  ----------------------------------------------
 * ramestic  2009-06-19  created
 */

/*
 * System stuff
 */
#include <linux/delay.h>

/*
 * Local stuff
 */
#include "q485Private.h"

/*
 * Used for RTOS logging
 */
#define modName "q485"

MODULE_AUTHOR("Rodrigo Amestica <ramestica@nrao.edu>");
MODULE_DESCRIPTION(modName ":  QUATECH SSCLP-200/300 driver.\n"
                   "Handler for input TE signal on RS-485 bus.");
MODULE_LICENSE("GPL");

/*
 * Module parameters
 */
static unsigned int initTimeout = 1000;         /* msec */
module_param(initTimeout, uint, S_IRUGO);
MODULE_PARM_DESC(initTimeout, "Time out for the real time initialisation task."
                 "If the init taks does not finish in time, the kernel module will consider"
                 "the real time initialisation failed and return an error.");

static unsigned int cleanUpTimeout = 1000;      /* msec */
module_param(cleanUpTimeout, uint, S_IRUGO);
MODULE_PARM_DESC(cleanUpTimeout, "Time out for the real time clean-up task."
                 "If the clean-up taks does not finish in time, the kernel module will "
                 "consider the real time system being \"dirty\" and log an error message.");

static int debug = 0;      /* on/off */
module_param(debug, int, S_IRUGO);
MODULE_PARM_DESC(debug, "Debugging option."
                 "When set then interrupts are enabled at module loading and disabled at mmodule unloading. "
                 "No external handler is provided to enableTE in this case.");
/* The following function is only needed by the ambServer for the CCC and is a dummy */
void resetChannel(int channel)
{
}

EXPORT_SYMBOL(resetChannel);

/*
 * Local data
 */

/*
 * driver data, we initialize with the ids that will let us discover
 * the right board.
 */
q485_driver_t driver =
{
    .dev.pci = 0,
    .dev_ids =
    {
        {.vendor = Q485_VENDOR_ID,
         .device = Q485_DEVICE_ID,
         .subvendor = PCI_ANY_ID,
         .subdevice = PCI_ANY_ID,
         .driver_data=(kernel_ulong_t)&driver
        },
        {}
    },
    .drv =
    { 
        .name = "q485",
        .id_table = driver.dev_ids,
        .probe = q485Probe,
        .remove = q485Remove
    }
};
q485_driver_t *drv_p = &driver;

static int q485_main(int stage)
{
    int status, stat;
    int cleanUpStatus = RT_TOOLS_MODULE_EXIT_SUCCESS;
    int totalWaitedTime = 0;
    rtlogRecord_t logRecord;

    if ( stage == RT_TOOLS_MODULE_STAGE_INIT )
    {
        status = RT_TOOLS_MODULE_INIT_ERROR;

        /*
         * Log CVS version and other configuration stuff.
         */
        RTLOG_INFO(modName, "$Id$");
        RTLOG_INFO(modName, "Initializing module...");
        RTLOG_INFO(modName, "initTimeout    = %dms", initTimeout);
        RTLOG_INFO(modName, "cleanUpTimeout = %dms", cleanUpTimeout);
        RTLOG_INFO(modName, "debug          = %d", debug);

        goto Initialization;
    }
    else
    {
        status = RT_TOOLS_MODULE_EXIT_SUCCESS;

        RTLOG_INFO(modName, "Cleaning up module...");

        goto FullCleanUp;
    }

Initialization:

    status = RT_TOOLS_MODULE_INIT_SUCCESS;

    /*
     * now external handler, so far.
     */
    drv_p->dev.handler = NULL;

    /*
     * register our pci driver
     */
    if ( (stat = pci_register_driver(&driver.drv)) )
    {
        RTLOG_ERROR(modName, "failed to register device (err=%d)", stat);

        status = RT_TOOLS_MODULE_INIT_ERROR;

        goto Exit;
    }
    
    /*
     * wait until the device is marked as enabled.
     */
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,19)
    while ( driver.dev.pci == 0 || atomic_read(&driver.dev.pci->enable_cnt) == 0 )
#else
    while ( driver.dev.pci == 0 || driver.dev.pci->is_enabled != 1 )
#endif
    {
        /*
         * check every 10ms
         */
        mdelay(10);

        /*
         * if waiting time has expaired then fail
         */
        if ( totalWaitedTime > initTimeout )
        {
            /*
             * revert the effect of the previous registering call
             */
            pci_unregister_driver(&driver.drv);
            
            RTLOG_ERROR(modName, "our single expected device did not enable on time %p", driver.dev.pci);
            
            status = RT_TOOLS_MODULE_INIT_ERROR;
            
            goto Exit;
        }

        totalWaitedTime += 10;
    }

    RTLOG_INFO(modName, "pci device detected enabled after %d ms", totalWaitedTime);

    /*
     * for debugging is nice to be able to enable the interrupts right away.
     */
    if ( debug )
    {
        RTLOG_INFO(modName, "enabling interrupts automatically");
        enableTE(NULL);
    }

    goto Exit;

FullCleanUp:

    cleanUpStatus = RT_TOOLS_MODULE_EXIT_SUCCESS;

    /*
     * if they were enabled automatically at module loading then disable
     * them now.
     */
    if ( debug )
    {
        disableTE();
    }

    /*
     * unregister the device only when already registered
     */
    pci_unregister_driver(&driver.drv);
    RTLOG_INFO(modName, "device unregistered");
    
Exit:
    if(stage != RT_TOOLS_MODULE_STAGE_INIT)
    {
        status = cleanUpStatus;
    }

    return status;
}

/*
 * Executed on "modprobe modName" or on "insmod /PATH/modName.ko".
 */
static int __init q485_init(void)
{
    int status;
    rtlogRecord_t logRecord;
    
    if ( (status = q485_main(RT_TOOLS_MODULE_STAGE_INIT)) == RT_TOOLS_MODULE_INIT_SUCCESS )
    {
        RTLOG_INFO(modName, "Module initialized successfully.");
    }
    else
    {
        RTLOG_ERROR(modName, "Failed to initialize module!");
    }

    return status;
}

/*
 * Executed on "rmmod modName"
 */
static void __exit q485_exit(void)
{
    rtlogRecord_t logRecord;

    if ( q485_main(RT_TOOLS_MODULE_STAGE_EXIT) == RT_TOOLS_MODULE_EXIT_SUCCESS )
    {
        RTLOG_INFO(modName, "Module cleaned up successfully.");
    }
    else
    {
        RTLOG_ERROR(modName, "Failed to clean up module!");
    }
}

/* Required for the kernel to allocate space for the init and exit functions. */
module_init(q485_init);
module_exit(q485_exit);

/*___oOo___*/
