/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2004 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* 
*
* who       when      what
* --------  --------  ----------------------------------------------
* ramestic  2004-06-22  created
*/

/* 
 * Local stuff
 */
#include "q485Private.h"

#define modName "q485"

extern q485_driver_t *drv_p;

/*
 * function used for disabling interrupts.
 */
int disableTE(void)
{
    int status;
    rtlogRecord_t logRecord;

    /*
     * verify that the board is already enabled.
     */
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,19)
    if ( drv_p->dev.pci == 0 || atomic_read(&drv_p->dev.pci->enable_cnt) == 0 )
#else
    if ( drv_p->dev.pci == 0 || drv_p->dev.pci->is_enabled == 0 )
#endif
    {
        RTLOG_ERROR(modName, "board is not enabled");

        return 1;
    }

    /*
     * uninstall our isr for this interrupt line
     */
    rt_disable_irq(drv_p->dev.pci->irq);
    if ( (status = rt_free_global_irq(drv_p->dev.pci->irq)) != 0 )
    {
	RTLOG_ERROR(modName,
                    "failed to free interrupt line (irq=%d, err=%d)!", 
                    drv_p->dev.pci->irq, status);

        return 1;
    }

    /*
     * shutdown the irq
     */
    rt_shutdown_irq(drv_p->dev.pci->irq);

    RTLOG_INFO(modName, "interrupt disabled");

    return 0;
}
EXPORT_SYMBOL(disableTE);

/*___oOo___*/
