/*******************************************************************************
*
* "@(#) $Id$"
*
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* who       when        what
* --------  ----------  ----------------------------------------------
* ramestic  2009-06-19  created
*/

/*
 * Local stuff
 */
#include "q485Private.h"

/*
 * Used for RTOS logging
 */
#define modName "q485"

int q485Probe(struct pci_dev *dev, const struct pci_device_id *id)
{
    int stat, i;
    uint16_t pci_command_reg, pci_status_reg;
    uint8_t int_line, int_pin;
    q485_driver_t *drv;
    rtlogRecord_t logRecord;
    
    RTLOG_INFO(modName, "probing device...");

    /*
     * check that the device is what we expect
     */
    if ( !(id->vendor == Q485_VENDOR_ID && id->device == Q485_DEVICE_ID ) )
    {
        RTLOG_ERROR(modName, "this device ids does not look like mine");
        
        return -ENODEV;
    }
    
    /*
     * pointer to our own driver's structure
     */
    drv = (q485_driver_t *)id->driver_data;

    /*
     * enable the device
     */
    if ( (stat = pci_enable_device(dev)) )
    {
        RTLOG_ERROR(modName, "device enabling has failed (err=%d)", stat);
        
        return -ENODEV;
    }

    /*
     * just for logging purposes
     */
    if ( (stat = pci_read_config_byte(dev, PCI_INTERRUPT_LINE, &int_line)) )
    {
        RTLOG_ERROR(modName, "failed to read pci interrupt line (err=%d)", stat);
        
        return -ENODEV;
    }
    if ( (stat = pci_read_config_byte(dev, PCI_INTERRUPT_PIN, &int_pin)) )
    {
        RTLOG_ERROR(modName, "failed to read pci interrupt pin (err=%d)", stat);
        
        return -ENODEV;
    }
    if ( (stat = pci_read_config_word(dev, PCI_COMMAND, &pci_command_reg)) )
    {
        RTLOG_ERROR(modName, "failed to read pci command register (err=%d)", stat);
        
        return -ENODEV;
    }
    if ( (stat = pci_read_config_word(dev, PCI_STATUS, &pci_status_reg)) )
    {
        RTLOG_ERROR(modName, "failed to read pci status register (err=%d)", stat);
        
        return -ENODEV;
    }
    RTLOG_INFO(modName, "irq=%u", dev->irq);
    RTLOG_INFO(modName,
               "pci status cmd/stat/iline/ipin=%u/%u/%u/%u",
               pci_command_reg,
               pci_status_reg,
               int_line,
               int_pin);

    /*
     * inspect BARs
     */
    for ( i = 0; i < 6; i++ )
    {
        unsigned long start, end, flags;

        /*
         * get region params
         */
        start = pci_resource_start(dev, i);
        end = pci_resource_end(dev, i);
        flags = pci_resource_flags(dev, i);

        RTLOG_INFO(modName, "bar/start/end/size/flags=%d/%lx/%lx/%lu/0x%lx", i, start, end, end - start + 1, flags);
    }

    /*
     * request regions under our name
     */
    if ( (stat = pci_request_regions(dev, modName)) )
    {
        RTLOG_ERROR(modName, "failed to request regions (err=%d)", stat);
        
        goto error_regions;
    }

    /*
     * remapping of base addresses, use 2 because this quatech board
     * is like that.
     */
    if ( (drv->dev.regs_mem = pci_iomap(dev, 2, 8)) == NULL )
    {
        RTLOG(modName, RTLOG_ERROR, "failed to map I/O port");
        
        goto error_ioremap;
    }

    /*
     * record our pci_dev structure. So far, used only for checking during
     * initialization whether the probe is finish or not.
     */
    drv->dev.pci = dev;

    RTLOG_INFO(modName, "device probed okay");

    return 0;

error_ioremap:
    pci_release_regions(dev);
error_regions:
    pci_disable_device(dev);

    RTLOG_ERROR(modName, "device probed unsuccessfully!");

    return -ENODEV;
}

/*___oOo___*/
