/*******************************************************************************
*
* "@(#) $Id$"
*
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* who       when        what
* --------  ----------  ----------------------------------------------
* ramestic  2009-06-19  created
*/

/*
 * Local stuff
 */
#include "q485Private.h"

/*
 * Used for RTOS logging
 */
#define modName "q485"

void q485Remove(struct pci_dev *dev)
{
    q485_driver_t *drv = (q485_driver_t *)dev->driver->id_table->driver_data;
    rtlogRecord_t logRecord;

    RTLOG_INFO(modName, "removing device...");

    /*
     * check that a device was enabled at some moment
     */
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,19)
    if ( atomic_read(&dev->enable_cnt) == 0 )
#else
    if ( dev->is_enabled == 0 )
#endif
    {
        RTLOG_ERROR(modName, "this device was not previously enabled!");
        
        return;
    }

    /*
     * unmap I/O memory area
     */
    pci_iounmap(dev, drv->dev.regs_mem);

    /*
     * regions not ours any more
     */
    pci_release_regions(dev);

    /*
     * good bye device
     */
    pci_disable_device(dev);

    RTLOG_INFO(modName, "device removed");
}

/*___oOo___*/
