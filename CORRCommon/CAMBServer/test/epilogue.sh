#! /bin/sh

# Undo the permission change done in the prologue
chmod o-r ../config/testCAMBServer.lkm
chmod o-r ../config/testCAMBServer-preload.lkm
chmod o-r ../config/testCAMBServerLoadUnload.lkm
exit 0
