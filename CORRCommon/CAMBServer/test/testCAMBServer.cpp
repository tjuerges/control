/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jperez  2006-04-27  created
*/

#include <stdio.h>
#include <iostream>
#include <sys/time.h>

#include <tpmc901.h>
#include <rtTools.h>
#include <rtToolsFifoCmd.h>
#include <teHandler.h>
#include <rtai_sched.h>
#include <rtai_fifos.h>
using namespace std;

#include <iostream>
#include <vector>
#include <string>
#include <ambDefs.h>
#include <ambInterface.h>

#define NUMBER_OF_IMMEDIATE_RESPONSE 1000
#define NUMBER_OF_NEXT_TE_RESPONSE 1000
#define NUMBER_OF_NO_RESPONSE 10
#define NUMBER_OF_DELAYED_MSGS 10
#define NUMBER_OF_INCORRECT_MSGS 10
#define NUMBER_OF_GET_NODES 10
#define NUMBER_OF_AMB_MEM_TEST 3080

    /* Test RCAs */
#define CAMB_GET_NODES 0
#define CAMB_IMMEDIATE_RESPONSE  1
#define CAMB_DELAYED_RESPONSE  2
#define CAMB_NO_RESPONSE  3
#define CAMB_INCORRECT_RESPONSE  4
#define CAMB_SHUTDOWN  5
#define CAMB_PASS_THROUGH  6
#define NUM_TEST_RCAS 7
static const char * testTypeName[NUM_TEST_RCAS] = {("CAMB_GET_NODES"),
                           ("CAMB_IMMEDIATE_RESPONSE"),
                           ("CAMB_DELAYED_RESPONSE"),
                           ("CAMB_NO_RESPONSE"),
                           ("CAMB_INCORRECT_RESPONSE"),
                           ("CAMB_SHUTDOWN"),
                           ("CAMB_PASS_THROUGH")};
       /* Command types */
#define CAN_CTRL_TYPE 0
#define CAN_MON_TYPE 1
#define CAN_CTRL_NEXT_TE_TYPE 2
#define CAN_MON_NEXT_TE_TYPE 3
#define CAN_GET_NODES_TYPE 4
#define NUM_CMD_TYPES 5

static const char * msgTypeName[NUM_CMD_TYPES] = {("Control"),
                          ("Monitor"),
                          ("Control at next TE"),
                          ("Monitor at next TE"),
                          ("Get Nodes")};

    /* Test errors */
#define CANSIM_WRITEERR (AMBERR_NOMEM + 1)
#define CANSIM_READERR (AMBERR_NOMEM + 2)

#define ACS_UNIX_TIME_OFFSET 122192928000000000LL

class CAMBServerUnitTest
{
public:
    /// default constructor
    CAMBServerUnitTest()
    {
    try
    {
        m_ambIF = AmbInterface::getInstance();
        printf("AmbIF::AmbIF->Instantiated ambInterface\n");
    }
    catch(...)
    {
        printf("AmbIF::AmbIF->Unable to instantiate ambInterface\n");
        throw;
    }
    }

    /// copy constructor
    CAMBServerUnitTest( const CAMBServerUnitTest &toCopy)
    { *this = toCopy; }

    /// assignment operator
    CAMBServerUnitTest &operator =( const CAMBServerUnitTest &)
    { return *this; }

    /// destructor
    ~CAMBServerUnitTest() { m_ambIF->deleteInstance(); }

    void runTests();


private:

    const AmbInterface *m_ambIF;
    int m_numberOfDelayedMsgs;

    /// This is the FIFO used by tp_write() from which we read commands
    unsigned int m_writeFIFO_FD;

    /// The FIFO used by tp_read() from which we write command responses
    unsigned int m_readFIFO_FD;

    int m_cmdRetCount, m_cmdTxCount;
    int m_monRxCount, m_monTxCount;
    int m_getNodesRxCount, m_getNodesTxCount;
    int m_totalMonRxCount;
    int m_totalMonTxCount;
    int m_totalCmdRetCount;
    int m_totalCmdTxCount;
    int m_totalGetNodesRxCount;
    int m_totalGetNodesTxCount;

    inline AmbRelativeAddr getRCA(const AmbNodeAddr&     baseAddress,
                  const AmbAddr& absoluteAddress)
    {
    return (absoluteAddress & (~((baseAddress + 1) << 18)));
    }

    // Send AMB_CONTROL message to CAMB device synchronously (useSynchLock = true) or
    // asynchronously (useSynchLock = false) at time te (0 is ASAP)
    AmbResponse_t commandTE(const AmbAddr rca,
                vector<unsigned char> data,
                const Time te,
                sem_t *synchLock = NULL);
    // Send AMB_CONTROL message to CAMB device synchronously (useSynchLock = true) or
    // asynchronously (useSynchLock = false) at next te
    AmbResponse_t commandNextTE(AmbAddr rca,
                vector<unsigned char> data,
                sem_t *synchLock = NULL);
    // Send AMB_MONITOR message to CAMB device synchronously at time te
    AmbResponse_t monitorTE(AmbAddr rca, Time te);
    // Send AMB_MONITOR message to CAMB device synchronously at next te
    AmbResponse_t monitorNextTE(AmbAddr rca);
    AmbResponse_t getNodes();
    int getCanMessage(AmbMessage_t &message, int waitForMS);
    int sendCanMessage(TP901_MSG_BUF &msgBlock);
    bool testLoop(bool asap, Time teOffset, int iterations,
          int testType, int &rxCount, int &txCount,
          int &totalRxCount, int &totalTxCount,
          AmbErrorCode_t allowedStatus, int canMsgType,
          Time timeInterval, bool synched = true);
    /// Timing routines
    Time fifoGetTime();
    bool flushTest();
    bool ambMemTest();

       /// Test to verify that illegall addresses are correctly rejected.
       bool  addressErrorTest();
};


Time CAMBServerUnitTest::fifoGetTime()
{
    unsigned long long t;
    rtToolsFifoCmd *cmdFifo;

    //
    // create cmd object
    //
    try
    {
    cmdFifo = new rtToolsFifoCmd(TE_HANDLER_CMD_FIFO_DEV);
    }
    catch(...)
    {
    cout << "failed to create rtToolsFifoCmd instance" << endl;
    return 0;
    }

    //
    // GETTIME
    //
    try
    {
        int cmd = teHandlerCMD_GETTIME;
        cmdFifo->sendRecvCmd(&cmd, sizeof(cmd), (void *)&t, sizeof(t), 60);
    }
    catch(rtToolsFifoCmdErr_t e)
    {
    delete cmdFifo;
    cout << "sendRecvCmd GETTIME has failed (ex=" << e << ")" << endl;
    return 0;
    }

    delete cmdFifo;
    return t;
}

//------------------------------------------------------------------------------
int CAMBServerUnitTest::getCanMessage(AmbMessage_t &message, int waitForMS)
{
    TP901_MSG_BUF rxMsg, txMsg;
    int err, sizeOfMsg = sizeof(TP901_MSG_BUF);
    txMsg.identifier = message.address;
    txMsg.msg_len = message.dataLen;
    for (int i = 0; i < txMsg.msg_len; ++i)
    {
        txMsg.data[i] = message.data[i];
    }
    // read the writeFIFO & display the contents.
    err = rtf_read_timed(m_writeFIFO_FD,(char *)&rxMsg, sizeOfMsg, waitForMS);
    // Extract rca for node #1
    int rca = getRCA(1, rxMsg.identifier);
    if (message.requestType == AMB_CONTROL)
    { // Control commands do not require a response
    return 0;
    }
    else if( (err != 0) && (err != sizeOfMsg) )
    { // We are getting invalid message size from canSimDrv -- get out of here
    printf("Read error on writeFIFO: %d\n",err);
    return err;
    }
    else if (rxMsg.identifier != txMsg.identifier)
    {
      // What was sent to canSimDrv through ambInterface does not match what canSimDrv
      // is writing in the writeFIFO -- get out of here
    printf("Mismatching read/send identifiers --> rxMsg: %lu, txMsg: %lu\n",
           rxMsg.identifier, txMsg.identifier);
    return err;
    }
    else
    { // These must be monitors or get node IDs
    txMsg.timeout = rxMsg.timeout; // ???
    txMsg.extended = rxMsg.extended; // ???
    switch (rca)
    {
            case CAMB_GET_NODES:
                for (int i=1; i < 4; ++i)
                {
                    txMsg.identifier = createAMBAddr(i, rca);
                    txMsg.msg_len = 8;
                    for (int j = 0; j < txMsg.msg_len; ++j)
                    {
                        txMsg.data[j] = j + i;
                    }
                    err = sendCanMessage(txMsg);
                    if (err != sizeof(TP901_MSG_BUF))
                    {
                        printf("Error writing back message CAMB_GET_NODES to canSimDrv!\n");
                    }
                usleep(48000);
                }
                break;
            // The following two cases share  code thus no break for the first
        case CAMB_DELAYED_RESPONSE:
        case CAMB_IMMEDIATE_RESPONSE:
        err = sendCanMessage(txMsg);
        if (err != sizeof(TP901_MSG_BUF))
        {
            printf("Error writing back message CAMB_IMMEDIATE_RESPONSE to canSimDrv!\n");
        }
        break;
        case CAMB_INCORRECT_RESPONSE:
        ++(txMsg.identifier);
        err = sendCanMessage(txMsg);
        if (err != sizeof(TP901_MSG_BUF))
        {
            printf("Error writing back message CAMB_INCORRECT_RESPONSE to canSimDrv!\n");
        }
        break;
        // The following three cases share some code thus no break for the first two
        case CAMB_SHUTDOWN:
        printf("Ending test...\n");
        case CAMB_NO_RESPONSE:
        case CAMB_PASS_THROUGH:
        break;
        default:
        printf("Unknown rca received: %d\n", rca);
        break;
    }
    }
    return err;
}

//------------------------------------------------------------------------------
int CAMBServerUnitTest::sendCanMessage(TP901_MSG_BUF &msgBlock)
{
    int err, sizeOfMsg = sizeof(TP901_MSG_BUF);

    err = write(m_readFIFO_FD,&msgBlock,sizeOfMsg);
    return err;
}

//-----------------------------------------------------------------------------------------------
AmbResponse_t CAMBServerUnitTest::commandTE(AmbAddr rca,
                        vector<unsigned char> data,
                        const Time te,
                        sem_t *synchLock)
{
    AmbResponse_t rv;
    Time sendTime;
    rv.status = AMBERR_NOERR;
    rv.timestamp = te;
    AmbCompletion_t *completion = new AmbCompletion_t;
    AmbMessage_t message;
    // Build an absolute CAN ID for node #1
    AmbAddr canId = createAMBAddr(1, rca);
    memset(completion, 0, sizeof(AmbCompletion_t));

    if (synchLock != NULL)
    {
    sem_init(synchLock, 0, 0);

    /* Build the completion block */
    completion->dataLength_p = &rv.dataLen;
    completion->data_p = rv.data;
    completion->channel_p = &rv.channel;
    completion->address_p = &rv.address;
    completion->type_p = &rv.type;
    completion->timestamp_p  = &rv.timestamp;
    completion->status_p = &rv.status;
    completion->synchLock_p = synchLock;
    }

    /* Build the message block */
    message.requestType  = AMB_CONTROL;
    message.channel = 0;
    message.address = canId;
    message.dataLen = data.size();
    for (int idx = 0; idx < message.dataLen; ++idx)
        message.data[idx] = data[idx];
    message.completion_p = completion;
    message.targetTE = te;
    rv.timestamp = te;

    /* Send the message and wait for a return */
    try
    {
        sendTime = fifoGetTime();
//      printf("commandTE: command sent at: %lld with te: %lld \n", sendTime, te);
    m_ambIF->sendMessage(message);
    ++m_cmdTxCount;
    }
    catch(...)
    {
    printf("commandTE: Got an ambInterface error\n");
        rv.status = static_cast<AmbErrorCode_t>(CANSIM_WRITEERR);
        return rv;
    }
    int err = 0;
    if ((te == 0) || ((te - fifoGetTime()) < 2000) || (te < sendTime))
        err = getCanMessage(message, 2000);
    if (synchLock != NULL)
    {
    sem_wait(synchLock);
    sem_destroy(synchLock);
    }
    return rv;
}

//-----------------------------------------------------------------------------------------------
AmbResponse_t CAMBServerUnitTest::commandNextTE(AmbAddr rca,
                        vector<unsigned char> data,
                        sem_t *synchLock)

{
    AmbResponse_t rv;
    rv.timestamp = 0LL;
    rv.status = AMBERR_NOERR;
    AmbCompletion_t *completion = new AmbCompletion_t;
    memset(completion, 0, sizeof(AmbCompletion_t));
    AmbMessage_t     message;

    if (synchLock != NULL)
    {
    sem_init(synchLock, 0, 0);
    /* Build the completion block */
    completion->timestamp_p  = &rv.timestamp;
    completion->status_p     = &rv.status;
    completion->synchLock_p  = synchLock;
    }

    /* Build the message block */
    message.requestType  = AMB_CONTROL_NEXT;
    message.channel      = 0;
    message.address      = createAMBAddr(1, rca);
    message.dataLen      = data.size();
    for (int idx = 0; idx < message.dataLen; ++idx)
    message.data[idx] = data[idx];

    message.completion_p = completion;
    message.targetTE     = 0;

    /* Send the message and wait for a return */
    try
    {
    m_ambIF->sendMessage(message);
    ++m_cmdTxCount;
    }
    catch(...)
    {
    printf("commandNextTE: Got an ambInterface error\n");
    rv.status = static_cast<AmbErrorCode_t>(CANSIM_WRITEERR);
    return rv;
    }
    getCanMessage(message, 2000);
    if (synchLock != NULL)
    {
    sem_wait(synchLock);
    sem_destroy(synchLock);
    }
    return rv;
}

// Synchronous monitor at specified time
//-----------------------------------------------------------------------------------------------
AmbResponse_t CAMBServerUnitTest::monitorTE(AmbAddr rca, Time te)
{
    /* Synchronous call to get a monitor point */

    AmbResponse_t rv;

    AmbCompletion_t* completion = new AmbCompletion_t;
    AmbMessage_t message;
    // Build an absolute CAN ID for node #1
    AmbAddr canId = createAMBAddr(1, rca);

    sem_t synchLock;
    sem_init(&synchLock, 0, 0);

    /* Build the completion block */
    completion->dataLength_p = &rv.dataLen;
    completion->data_p = rv.data;
    completion->channel_p = &rv.channel;
    completion->address_p = &rv.address;
    completion->type_p = &rv.type;
    completion->timestamp_p  = &rv.timestamp;
    completion->status_p = &rv.status;
    completion->synchLock_p  = &synchLock;
    completion->contLock_p = NULL;

    /* Build the message block */
    message.requestType  = AMB_MONITOR;
    message.channel = 0;
    message.address = canId;
    message.dataLen      = 0;
    message.completion_p = completion;
    message.targetTE = te;

    try
    {
    m_ambIF->sendMessage(message);
    ++m_monTxCount;
    }
    catch(...)
    {
    printf("monitorTE: Got an ambInterface error\n");
    rv.status = static_cast<AmbErrorCode_t>(CANSIM_WRITEERR);
    return rv;
    }
    int err = getCanMessage(message, 2000);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);

    if (err != sizeof(TP901_MSG_BUF))
    {
    rv.status = static_cast<AmbErrorCode_t>(CANSIM_READERR);
    }
    return rv;
}

AmbResponse_t CAMBServerUnitTest::monitorNextTE(AmbAddr rca)
{

    AmbResponse_t rv;
    /* Call to get a monitor point at a specific time*/
    AmbCompletion_t* completion = new AmbCompletion_t;
    AmbMessage_t     message;

    sem_t synchLock;
    sem_init(&synchLock, 0, 0);

    /* Build the completion block */
    completion->dataLength_p = &rv.dataLen;
    completion->data_p       = rv.data;
    completion->channel_p    = &rv.channel;
    completion->address_p    = &rv.address;
    completion->timestamp_p  = &rv.timestamp;
    completion->type_p       = &rv.type;
    completion->status_p     = &rv.status;
    completion->synchLock_p  = &synchLock;
    completion->contLock_p   = NULL;

    /* Build the message block */
    message.requestType  = AMB_MONITOR_NEXT;
    message.channel      = 0;
    message.address      = createAMBAddr(1, rca);
    message.dataLen      = 0;
    message.completion_p = completion;
    message.targetTE     = 0;

    try
    {
    m_ambIF->sendMessage(message);
    ++m_monTxCount;
    }
    catch(...)
    {
    printf("monitorNextTE: Got an ambInterface error\n");
    rv.status = static_cast<AmbErrorCode_t>(CANSIM_WRITEERR);
    return rv;
    }
    int err = getCanMessage(message, 2000);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);

    if (err != sizeof(TP901_MSG_BUF))
    {
    rv.status = static_cast<AmbErrorCode_t>(CANSIM_READERR);
    }
    return rv;
}



// Get node info for all nodes on this channel
//--------------------------------------------------------------------------------
AmbResponse_t  CAMBServerUnitTest::getNodes()
{
    /* Synchronous call to get the nodes on a given channel */

    AmbResponse_t rv;
    AmbCompletion_t* completion = new AmbCompletion_t;
    AmbMessage_t     message;

    sem_t            synchLock;
    sem_t            contLock;
    sem_init(&synchLock, 0, 0);
    sem_init(&contLock, 0, 1);

    /* Build the completion block */
    completion->dataLength_p = &rv.dataLen;
    completion->data_p       = rv.data;
    completion->channel_p    = &rv.channel;
    completion->address_p    = &rv.address;
    completion->timestamp_p  = &rv.timestamp;
    completion->status_p     = &rv.status;
    completion->type_p = &rv.type;
    completion->synchLock_p  = &synchLock;
    completion->contLock_p   = &contLock;

    /* Build the message block */
    message.requestType  = AMB_GET_NODES;
    message.channel      = 0;
    message.address      = 0;
    message.dataLen      = 0;
    for (int i = 0;i < 8; ++i)
        message.data[i]  = 25 + i;
    message.completion_p = completion;

    message.targetTE = 0;

    /* Send the message and wait for a return */
    try
    {
        m_ambIF->sendMessage(message);
        ++m_getNodesTxCount;
    }
    catch(...)
    {
    printf("getNodes: Got an ambInterface error\n");
        rv.status = static_cast<AmbErrorCode_t>(CANSIM_WRITEERR);
        return rv;
    }
    int err = getCanMessage(message, 2000);
    sem_wait(&synchLock);

    int rxCount = 0;
    while (rv.status == AMBERR_CONTINUE)
    { // Continue while there are more nodes responding

        ++m_getNodesRxCount;
        ++rxCount;
        sem_post(&contLock);
        sem_wait(&synchLock);
    }
    sem_destroy(&synchLock);
    sem_destroy(&contLock);
    if ((err != sizeof(TP901_MSG_BUF)) || (rxCount != rv.data[0]))
    {
        printf("getNodes --> err: %d rxCount: %d rv.data[0]: %d\n", err, rxCount, rv.data[0]);
        rv.status = static_cast<AmbErrorCode_t>(CANSIM_READERR);
    }
    return rv;

}

bool CAMBServerUnitTest::testLoop(bool asap, Time teOffset, int iterations,
                                  int testType, int &rxCount, int &txCount,
                  int &totalRxCount, int &totalTxCount,
                  AmbErrorCode_t allowedStatus, int canMsgType,
                  Time timeInterval, bool synched)
{
    AmbResponse_t msgRead;
    vector<unsigned char> ambMsg;
    bool testLoopRet = true;
    sem_t *synchLock;
    Time te, sendTime, startTime, stopTime;
    // initialize ambMsg to testType
    for (int i=0; i<8; ++i)
    ambMsg.push_back(testType);
    rtf_reset(m_writeFIFO_FD);
    rtf_reset(m_readFIFO_FD);
    printf("Starting %s test: %s\n", msgTypeName[canMsgType], testTypeName[testType]);
    startTime = fifoGetTime();
    sendTime = startTime;
    if (asap)
    {
    te = 0;
    }
    else
    {
    if (teOffset == TEacs)
    {
            te = (sendTime / TEacs * TEacs) + TEacs;
    }
    else
    {
        te = sendTime;
    }
    }
    for( int i = 0; i < iterations; ++i )
    {
    te += teOffset;
    switch (canMsgType)
    {
        case CAN_CTRL_TYPE:
        if (synched)
        {
            synchLock = new sem_t;
            msgRead = commandTE(testType, ambMsg, te, synchLock);
            delete synchLock;
        }
        else
        {
            msgRead = commandTE(testType, ambMsg, te, NULL);
            usleep(TEacs / 48);
        }
        break;
        case CAN_MON_TYPE:
        msgRead = monitorTE(testType, te);
        break;
        case CAN_CTRL_NEXT_TE_TYPE:
        if (synched)
        {
            synchLock = new sem_t;
            msgRead = commandNextTE(testType, ambMsg, synchLock);
            delete synchLock;
        }
        else
            msgRead = commandNextTE(testType, ambMsg, NULL);
        break;
        case CAN_MON_NEXT_TE_TYPE:
        msgRead = monitorNextTE(testType);
                break;
            case CAN_GET_NODES_TYPE:
                msgRead = getNodes();
                break;
            default:
                printf("Invalid CAN command type\n");
        return false;
    };

    if (msgRead.status == AMBERR_NOERR)
    {
        if (canMsgType != CAN_GET_NODES_TYPE && teOffset <= timeInterval)
        ++rxCount;
    }
    else
        if (msgRead.status != allowedStatus)
        {
        printf("Error with %s test %s : %d\n", msgTypeName[canMsgType],
               testTypeName[testType], msgRead.status);
        testLoopRet = false;
        break;
        }
        if (te == 0)
        { // Check for turnaround of ASAP or next TE message
                if (static_cast<signed long long>(msgRead.timestamp - sendTime) >
                    static_cast<signed long long>(timeInterval))
                {
                    printf("ASAP: Error with %s test %s : %lld exceeds %lld at iteration: %d at time %lld\n",
                           msgTypeName[canMsgType],
                           testTypeName[testType],
                           static_cast<signed long long>(msgRead.timestamp - sendTime),
                           static_cast<signed long long>(timeInterval), i, fifoGetTime());
                    testLoopRet = false;
                    break;
        }
        }
        else if (te >= sendTime)
        { // Check for how close to TE message was sent
        if (static_cast<signed long long>(msgRead.timestamp - te) >
            static_cast<signed long long>(timeInterval))
        {
            printf("Error with %s test %s : %lld exceeds %lld at iteration: %d\n",
               msgTypeName[canMsgType],
               testTypeName[testType],
               static_cast<signed long long>(msgRead.timestamp - te),
               static_cast<signed long long>(timeInterval), i);
            testLoopRet = false;
            break;
        }
        }
    sendTime = msgRead.timestamp;
    }
    if (testLoopRet)
    {
    stopTime = fifoGetTime();
    printf("SUCCESS: %s: %s  responses received: %d, messages sent: %d in %llu \n",
           msgTypeName[canMsgType], testTypeName[testType], rxCount, txCount,
           stopTime - startTime);
    totalRxCount += rxCount;
    totalTxCount += txCount;
    rxCount = 0;
    txCount = 0;
    }
    try
    {
    te = 0;
    AmbErrorCode_t status = m_ambIF->flush(0, te, te);
    if (status != AMBERR_NOERR)
    {
        printf("ERROR: Unable to flush channel -- error: %d at te: %llu\n", status, te);
    }
    }
    catch(...)
    {
    printf("ERROR: Unable to flush channel\n");
    testLoopRet = false;
    }
    sleep(2);
    return testLoopRet;
}

//------------------------------------------------------------------------------
bool CAMBServerUnitTest::flushTest()
{
    for (int i = 0; i < 1; ++i)
    {
    // Send 3000 commands to be executed at given te far in the future so they'll
    // sit in ambServer queues.  testLoop flushes them when done even if they have not
    // been executed.  The current queues handle 3072 commands max.
    if (!testLoop(false, 10000000LL, 3000, CAMB_IMMEDIATE_RESPONSE,
              m_cmdRetCount, m_cmdTxCount, m_totalCmdRetCount, m_totalCmdTxCount,
              AMBERR_NOERR, CAN_CTRL_TYPE, TEacs, false))
    {
        printf("ERROR: Unsynched command first part of flush test failed\n");
        return false;
    }
    }
    printf("SUCCESS: Flush test passed!\n");
    return true;
}

//------------------------------------------------------------------------------
bool CAMBServerUnitTest::ambMemTest()
{ // This test what happens when memory runs out
    AmbResponse_t rv;
    vector<unsigned char> ambMsg;
    // initialize ambMsg to testType
    for (int i=0; i<8; ++i)
    ambMsg.push_back(CAMB_IMMEDIATE_RESPONSE);
    Time te = fifoGetTime() + 20000LL * TEacs;
    rtf_reset(m_writeFIFO_FD);
    rtf_reset(m_readFIFO_FD);
    for (int i = 1; i < NUMBER_OF_AMB_MEM_TEST; ++i)
    {
    rv = commandTE(CAMB_IMMEDIATE_RESPONSE, ambMsg, te + i, false);
        
    if (i % 1000 == 0)
    {
        printf("ambMemTest: Sent %d messages\n", i);
        sleep(1);
    }
    if (rv.status != AMBERR_NOERR)
    {
        printf("ambMemTest: Sent %d messages\n", i);
        return false;
    }
    }
    rtf_reset(m_writeFIFO_FD);
    rtf_reset(m_readFIFO_FD);
    sleep(2);
    printf("SUCCESS: Memory test passed!\n");
    return true;
}

//------------------------------------------------------------------------------
bool  CAMBServerUnitTest::addressErrorTest()
{ // This tests to make sure that illegal addresses are correctly rejected
  Time timestamp;

  try {
    m_ambIF->flush(0,0x400, 0, 0, timestamp);
    printf("Failed to raise error for Node ID 0x400\n");
    return false;
  } catch (ControlExceptions::CAMBErrorExImpl& ex) {
    /* This is ok, should have thrown and exception */
  }

 try {
    m_ambIF->flush(0,0x0, 0x40000, 0, timestamp);
    printf("Failed to raise error of RCA 0x40000\n");
    return false;
  } catch (ControlExceptions::CAMBErrorExImpl& ex) {
    /* This is ok, should have thrown and exception */
  }
 printf("SUCCESS: Address Error Test Passed!\n");
 return true;
}
//------------------------------------------------------------------------------
void CAMBServerUnitTest::runTests()
{
    m_monRxCount = 0;
    m_monTxCount = 0;
    m_cmdRetCount = 0;
    m_cmdTxCount = 0;
    m_getNodesRxCount = 0;
    m_getNodesTxCount = 0;
    m_totalMonRxCount = 0;
    m_totalMonTxCount = 0;
    m_totalCmdRetCount = 0;
    m_totalCmdTxCount = 0;
    m_totalGetNodesRxCount = 0;
    m_totalGetNodesTxCount = 0;

    if ((m_writeFIFO_FD = open(TP_WRITE_FIFO_DEV,O_RDONLY)) < 0)
    {
        printf("Error opening write FIFO buffer\n");
    return;
    }
    if ((m_readFIFO_FD = open(TP_READ_FIFO_DEV,O_WRONLY)) < 0)
    {
        printf("Error opening read FIFO buffer\n");
    close(m_writeFIFO_FD);
    return;
    }

    if (!testLoop(true, (Time)0, NUMBER_OF_GET_NODES, CAMB_GET_NODES,
                  m_getNodesRxCount, m_getNodesTxCount,
                  m_totalGetNodesRxCount, m_totalGetNodesTxCount,
                  AMBERR_TIMEOUT, CAN_GET_NODES_TYPE, 21000000LL))
    {
        printf("ERROR: Synched command CAMB_GET_NODES test failed\n");
    }

    if (!testLoop(true, (Time)0, NUMBER_OF_IMMEDIATE_RESPONSE, CAMB_IMMEDIATE_RESPONSE,
          m_cmdRetCount, m_cmdTxCount, m_totalCmdRetCount, m_totalCmdTxCount,
          AMBERR_NOERR, CAN_CTRL_TYPE, 11000000LL))
    {
    printf("ERROR: Synched command CAMB_IMMEDIATE_RESPONSE test failed\n");
    }

    if (!testLoop(true, (Time)0, NUMBER_OF_IMMEDIATE_RESPONSE, CAMB_IMMEDIATE_RESPONSE,
          m_cmdRetCount, m_cmdTxCount, m_totalCmdRetCount, m_totalCmdTxCount,
          AMBERR_NOERR, CAN_CTRL_TYPE, 11000000LL, false))
    {
    printf("ERROR: Unsynched command CAMB_IMMEDIATE_RESPONSE test failed\n");
    }

    if (!testLoop(false, TEacs, NUMBER_OF_NEXT_TE_RESPONSE, CAMB_IMMEDIATE_RESPONSE,
          m_cmdRetCount, m_cmdTxCount, m_totalCmdRetCount, m_totalCmdTxCount,
          AMBERR_TIMEOUT, CAN_CTRL_TYPE, 730000LL))
    {
    printf("ERROR: Synched pre-calculated next TE command test failed\n");
    }

    if (!testLoop(true, (Time)0, NUMBER_OF_NEXT_TE_RESPONSE, CAMB_IMMEDIATE_RESPONSE,
          m_cmdRetCount, m_cmdTxCount, m_totalCmdRetCount, m_totalCmdTxCount,
          AMBERR_TIMEOUT, CAN_CTRL_NEXT_TE_TYPE, 730000LL))
    {
    printf("ERROR: Synched ambServer-calculated next TE command test failed\n");
    }

    if (!testLoop(true, (Time)0, NUMBER_OF_NEXT_TE_RESPONSE, CAMB_IMMEDIATE_RESPONSE,
          m_cmdRetCount, m_cmdTxCount, m_totalCmdRetCount, m_totalCmdTxCount,
          AMBERR_NOERR, CAN_CTRL_NEXT_TE_TYPE, 730000LL, false))
    {
    printf("ERROR: Unsynched ambServer-calculated next TE command test failed\n");
    }

    if (!testLoop(false, TEacs, NUMBER_OF_IMMEDIATE_RESPONSE, CAMB_IMMEDIATE_RESPONSE,
          m_monRxCount, m_monTxCount, m_totalMonRxCount, m_totalMonTxCount,
          AMBERR_TIMEOUT, CAN_MON_TYPE, 730000LL))
    {
    printf("ERROR: Monitor pre-calculated next TE test failed\n");
    }

    if (!testLoop(true, (Time)0, NUMBER_OF_NEXT_TE_RESPONSE, CAMB_IMMEDIATE_RESPONSE,
          m_monRxCount, m_monTxCount, m_totalMonRxCount, m_totalMonTxCount,
          AMBERR_TIMEOUT, CAN_MON_NEXT_TE_TYPE, 730000LL))
    {
    printf("ERROR: Monitor ambServer-calculated next TE test failed\n");
    }

    if (!testLoop(false, 1000000LL, NUMBER_OF_DELAYED_MSGS, CAMB_DELAYED_RESPONSE,
          m_monRxCount, m_monTxCount, m_totalMonRxCount, m_totalMonTxCount,
          AMBERR_TIMEOUT, CAN_MON_TYPE, 16000000LL))
    {
    printf("ERROR: Monitor CAMB_DELAYED_RESPONSE test failed\n");
    }

    if (!testLoop(true, (Time)0, NUMBER_OF_INCORRECT_MSGS, CAMB_INCORRECT_RESPONSE,
          m_monRxCount, m_monTxCount, m_totalMonRxCount, m_totalMonTxCount,
          AMBERR_TIMEOUT, CAN_MON_TYPE, 11000000LL))
    {
    printf("ERROR: Monitor CAMB_INCORRECT_RESPONSE test failed\n");
    }

    if (!testLoop(true, (Time)0, NUMBER_OF_NO_RESPONSE, CAMB_NO_RESPONSE,
          m_monRxCount, m_monTxCount, m_totalMonRxCount, m_totalMonTxCount,
          AMBERR_TIMEOUT, CAN_MON_TYPE, 11000000LL))
    {
    printf("ERROR: Monitor CAMB_NO_RESPONSE test failed\n");
    }

    if (!flushTest())
    {
    printf("ERROR: flush test failed\n");
    }

    if (!ambMemTest())
    {
    printf("ERROR: amb memory test failed\n");
    }

    if (!addressErrorTest())
    {
    printf("ERROR: Address Error test failed\n");
    }

    printf(" TOTAL commands sent: %d, successful responses: %d\n",
       m_totalCmdTxCount, m_totalCmdRetCount);
    printf(" TOTAL monitors received: %d, requested: %d\n",
       m_totalMonRxCount, m_totalMonTxCount);
    printf(" TOTAL node ID received: %d, requests: %d\n",
       m_totalGetNodesRxCount, m_totalGetNodesTxCount);
    int writeCount = close(m_writeFIFO_FD);
    int readCount = close(m_readFIFO_FD);
    printf("Items left in fifos --> read: %d, write: %d\n", readCount, writeCount);
}

//------------------------------------------------------------------------------
int main( int argc, char **argv )
{
    try
    {
        CAMBServerUnitTest test;
    test.runTests();
    }
    catch(...)
    {
    printf("Unable to run tests\n");
    }
}
/*___oOo___*/
