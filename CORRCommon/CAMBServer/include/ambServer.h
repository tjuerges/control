#ifndef AMBCANSERVER_H
#define AMBCANSERVER_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2003-11-06  created
*/

/************************************************************************
 * This header file contains the definitions used in the RTAI CAN server
 * module.  That is the only file which should use it.
 *----------------------------------------------------------------------
 */

/* The following piece of code alternates the linkage type to C for all
functions declared within the braces, which is necessary to use the
functions in C++-code.
*/

#ifdef __cplusplus
extern "C" {
#endif

#include <ambDefs.h>
#include <teHandler.h>
#include <rtai_sched.h>
#include <rtai_sem.h>

#define MAX_NUM_AMB_CHANNELS (10)
#define MAX_INT_PER_TE (10)

/* Type Definitions */
typedef enum {
  AMBSIM,  // Simulated bus
  TPMC901
} CANDevice_t;

/**
 * This structure is part of a linked list of te related events
 */
typedef struct {
  AmbMessage_t  msg;       // the actual message that is going to be sent
  int           next;      // The next structure in the list
} AmbMessageList_t;

typedef struct {
  RT_TASK       task_ID_asap;
  RT_TASK       task_ID_te;
  SEM           channelSem;

  /* This defines the mapping to hardware */
  CANDevice_t   deviceType;
  int           deviceID;
  int           deviceChannel;

  /* These synchronize the shutdown routine */
  SEM           asapDone;
  SEM           teDone;

  /* This is queue for this particular ASAP channel */
  int           asapHead;
  int           asapTail;
  SEM           asapSem;
  SEM           asapChainSem;

  /* This is the queue for this particular TE related channel */
  int           teHead;
  SEM           teSem;
  SEM           teChainSem;
} AmbThreadInfo;

void asapThread(long);
void teThread(long);
void dispatcher(long);
void teDist(long);

/* Forward definitions */
int  dispatchSignal(unsigned int fifo, int rw);
void ambMsgProcess(AmbMessage_t* msgIn);
AmbErrorCode_t flush(int channel, AmbAddr mask, AmbAddr address, Time flushTime);

const unsigned long AMB_NODE_BASE_ADDR = 0x40000l;

AmbThreadInfo threadInfo[MAX_NUM_AMB_CHANNELS];

#ifdef __cplusplus
}
#endif

#endif /*AMBCANSERVER_H*/
