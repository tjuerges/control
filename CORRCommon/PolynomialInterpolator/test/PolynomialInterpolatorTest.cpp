/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2006-05-17  created
*/

/************************************************************************
*   NAME
*   
* 
*   SYNOPSIS
*   
* 
*   DESCRIPTION
*
*   FILES
*
*   ENVIRONMENT
*
*   COMMANDS
*
*   RETURN VALUES
*
*   CAUTIONS 
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS   
* 
*------------------------------------------------------------------------
*/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <acstime.h>
#include <PolynomialInterpolatorImpl.h>

const unsigned int MAX_ORDER =7;
const ACS::Time    TIME_SPAN=600000000;
#include <math.h>
#include <algorithm>

double Delay(ACS::Time time){
  return 60E-6 * sin(2*M_PI*(time/1E7)/86400.0);
}


class Polynomial {
public:
  Polynomial(int order) {
    order_m = order;
    coef_m = new double[order+1];
    for (int idx = 0; idx <= order; idx ++) {
      coef_m[idx] = (static_cast<double>(rand())/RAND_MAX);
    }
  };

  ~Polynomial(){ 
    delete[] coef_m;
  };
  
  double getValue(ACS::Time time) {
    double value = 0;
    for (int idx = 0; idx <= order_m; idx ++) {
      value *= static_cast<double>(time)/1E8;
      value += coef_m[idx];
    }
    return value;
  };

private:
  double* coef_m;
  int     order_m;

};


int main(int argc, char *argv[])
{
  /* initialize random generator */
  srand ( time(NULL) );

  for (unsigned int PolynomialOrder = 1; PolynomialOrder <= MAX_ORDER; 
       PolynomialOrder++) {
    

    ACS::Time timeArray[PolynomialOrder+1];
    double    valueArray[PolynomialOrder+1];

    
    for (unsigned int idx = 0; idx <  PolynomialOrder+1; idx++) {
      timeArray[idx] = 
	static_cast<ACS::Time>(TIME_SPAN * 
			       (static_cast<double>(rand())/RAND_MAX));
      valueArray[idx] = Delay(timeArray[idx]);
    }

    
    PolynomialInterpolator polyInt(PolynomialOrder,timeArray,valueArray);
    

    /* Test the GetPhase method */
    double maxError = 0.0;
    for (unsigned int timeIdx = 0; timeIdx < 60000; timeIdx++) {
      ACS::Time tempTime = 
	static_cast<ACS::Time>(TIME_SPAN * ((timeIdx/60000.0)));
      maxError = std::max(maxError,
 			  polyInt.getDelay(tempTime)-Delay(tempTime));
    }
    
    fprintf(stderr,"Polynomial Order: %2d ",PolynomialOrder);
    fprintf(stderr,"has maximum error of %3.3e",maxError);
    if (maxError < 1e-15) {
      fprintf(stderr," error within tolerance\n");
    } else {
      fprintf(stderr," FAILED\n");
    }
  } 
  return 0;
}








