#ifndef POLYNOMIALINTERPOLATORIMPL_H
#define POLYNOMIALINTERPOLATORIMPL_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* rsoto  2007-11-29  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <acstime.h>

class PolynomialInterpolator {
 public:
  PolynomialInterpolator(unsigned int polynomialOrder,
			 ACS::Time*   time,
			 double*      delay);

  ~PolynomialInterpolator();

  double getDelay(ACS::Time Time);

 private:
  double polint(ACS::Time x, ACS::Time* xa, double* ya);

  unsigned int polynomialOrder_m;
  ACS::Time*   time_m;
  double*      delay_m;
  double*      cTerms_m;
  double*      dTerms_m;
};

#endif /*!POLYNOMIALINTERPOLATORIMPL_H*/
