/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* rsoto  2007-11-29  created 
*/

/************************************************************************
*   NAME
*   
* 
*   SYNOPSIS
*   
* 
*   DESCRIPTION
*
*   FILES
*
*   ENVIRONMENT
*
*   COMMANDS
*
*   RETURN VALUES
*
*   CAUTIONS 
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS   
* 
*------------------------------------------------------------------------
*/

#include "PolynomialInterpolatorImpl.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

PolynomialInterpolator::PolynomialInterpolator(unsigned int polynomialOrder,
					       ACS::Time*   time,
					       double*      delay)
{
  polynomialOrder_m = polynomialOrder;
  time_m            = time;
  delay_m           = delay;
  cTerms_m          = new double[polynomialOrder_m+1];
  dTerms_m          = new double[polynomialOrder_m+1];
}

PolynomialInterpolator::~PolynomialInterpolator(){
  delete[] cTerms_m;
  delete[] dTerms_m;

}

double PolynomialInterpolator::getDelay(ACS::Time time) {
  return polint(time, time_m, delay_m);
}

/* This method is almost directly from Numerical Recipes I have done small
   amount of customizing for our purposes
*/
double PolynomialInterpolator::polint(ACS::Time x, ACS::Time* xa, double* ya) {

  int closestX = 0;
  double       y;
  double       dy;

  /* Local Variables */

  double ho;
  double hp;
  double w;
  double den;


  for (unsigned int idx = 0; idx <= polynomialOrder_m; idx++) {

    /* This funny statement is really just:
       if ( abs(x-xa[idx]) < abs(x-xa[closestX]) 
       but handleing the fact that these are unsigned variables
    */

    if ((x > xa[idx]      ? x-xa[idx]      : xa[idx] -x) < 
	(x > xa[closestX] ? x-xa[closestX] : xa[closestX] -x )){
      closestX = idx;
    }
  }
  
  //  std::cerr << "Closest X: " << closestX << std::endl;
  memcpy(cTerms_m,ya,(polynomialOrder_m+1)*sizeof(double));
  memcpy(dTerms_m,ya,(polynomialOrder_m+1)*sizeof(double));

  // Initial Approximation for y
  y=ya[closestX--];

  for (unsigned int approxOrder=1; approxOrder <= polynomialOrder_m;
       approxOrder++) {
    for (unsigned int idx = 0; idx <= polynomialOrder_m - approxOrder; idx++) {
      
      ho= (xa[idx]            >x ? xa[idx]-x            : 
	   - static_cast<double>(x-xa[idx]));
      hp= (xa[idx+approxOrder]>x ? xa[idx+approxOrder]-x:
	   - static_cast<double>(x- xa[idx+approxOrder]));
	
      w = cTerms_m[idx+1]-dTerms_m[idx];

      den = ho-hp;
      if (den == 0) {
	fprintf(stderr,"Hit Error Condition");
	/* We have hit the error condition */
      }
      den = w/den;
      cTerms_m[idx]=ho*den;
      dTerms_m[idx]=hp*den;
    }

    if (2*closestX < static_cast<int>(polynomialOrder_m  - approxOrder)) {
      dy = cTerms_m[closestX+1];
    } else {
      dy = dTerms_m[closestX--];
    }
    y += dy;
  }
  return y;
}





