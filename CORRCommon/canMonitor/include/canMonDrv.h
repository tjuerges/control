#ifndef canMonDrv_h
#define canMonDrv_h
/*
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2010
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * $Id$
 * $Source$
 *
 */

/**
 * General RT CAN driver message buffer
 */
typedef struct
{
    unsigned long Identifier; /*  Message identifier 29-bit or 11-bit*/
    unsigned long Timeout; /*  in clock ticks */
    unsigned char RxQueueNum; /*  Receive queue number ( 1..n ) */
    unsigned char Extended; /*  TRUE if extended identifier */
    unsigned char Status; /*  extra message status field */
    unsigned char MsgLen; /*  CAN message length ( 0..8 ) */
    unsigned char Data[8]; /*  up to 8 message bytes */
} MSG_BUF;

/**
 * CAN message buffer description
 */
typedef struct
{
    MSG_BUF MsgBuf;
    unsigned long long TimeStamp;
} CAN_MON_MSG;
#endif
