//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <iostream>
#include <sstream>
#include <fstream>
#include <ios>
#include <iomanip>
#include <vector>
#include <string>
#include <cerrno>
#include <Get_Opt.h>
#include <Time_Value.h>
#include <ace/OS_NS_sys_select.h>
#include <boost/shared_ptr.hpp>
#include <rtai_fifos.h>
#include <signal.h>
#if __GNUC__ < 4
#include <asm/atomic.h>
#endif
#include "canMonDrv.h"


static unsigned int exitFlag(0);


static void sigHandler(int signal, siginfo_t* info, void* data)
{
    // Set the finish flag atomically.
#if __GNUC__ >= 4
    __sync_add_and_fetch(&exitFlag, 1);
#else
    atomic_set(&exitFlag, 1);
#endif
}


static void createMessage(const std::string& message, CAN_MON_MSG& canMsg)
{
    std::memset(&canMsg, 0, sizeof(CAN_MON_MSG));

    std::stringstream msg(message);
    unsigned short tmpValue(0U);

    msg.ignore(10);
    msg >> canMsg.TimeStamp;
    msg.ignore(12);
    msg >> canMsg.MsgBuf.Identifier;
    msg.ignore(9);
    msg >> canMsg.MsgBuf.Timeout;
    msg.ignore(12);
    msg >> tmpValue;
    canMsg.MsgBuf.RxQueueNum = tmpValue;
    msg.ignore(10);
    msg >> tmpValue;
    canMsg.MsgBuf.Extended = tmpValue;
    msg.ignore(8);
    msg >> tmpValue;
    canMsg.MsgBuf.Status = tmpValue;
    msg.ignore(8);
    msg >> tmpValue;
    canMsg.MsgBuf.MsgLen = tmpValue;
    msg.ignore(5);

    if(canMsg.MsgBuf.MsgLen > 0U)
    {
        for(unsigned short i(0U); i < canMsg.MsgBuf.MsgLen; ++i)
        {
            msg.ignore(1);
            msg >> tmpValue;
            canMsg.MsgBuf.Data[i] = tmpValue;
        }
    }
}
static void printMessage(const CAN_MON_MSG& canMsg, unsigned int msgCount)
{
    std::ostringstream msg;
    msg << std::dec
        << std::setw(12)
        << std::setfill('0')
        << msgCount
        << "    "
        << canMsg.TimeStamp
        << "    0x"
        << std::hex
        << std::setw(8)
        << std::setfill('0')
        << static_cast< unsigned long >(canMsg.MsgBuf.Identifier)
        << "        "
        << std::dec
        << std::setw(1)
        << static_cast< unsigned short >(canMsg.MsgBuf.MsgLen)
        << "                ";

    if(canMsg.MsgBuf.MsgLen > 0U)
    {
        msg << "0x";

        for(unsigned short j(0U); j < canMsg.MsgBuf.MsgLen; ++j)
        {
            msg << std::hex
                << std::setw(2)
                << std::setfill('0')
                << static_cast< unsigned short >(canMsg.MsgBuf.Data[j]);
        }

    }
    else
    {
        msg << "N/A";
    }

    msg << "\n";
    std::cout << msg.str();
}

static void usage(char* argv0)
{
    std::cout << "\n"
        << argv0
        << "::usage:\n"
            "Options:\n"
            "[--help|h]:\n"
            "     You are reading it!\n"
            "[--file=|-f<canMonitor output file>]:\n"
            "      File which contains the CAN bus monitor data that shall be"
            "      replayed.\n\n";
}

int main(int argc, char *argv[])
{
    ACE_Get_Opt options(argc, argv);

    options.long_option("help", 'h');
    options.long_option("channel", 'c', ACE_Get_Opt::ARG_OPTIONAL);
    options.long_option("fileName", 'f', ACE_Get_Opt::ARG_OPTIONAL);

    std::string fileName;
    int c(0);
    while((c = options()) != EOF)
    {
        switch(c)
        {
            case 'f':
            {
                if(options.opt_arg() != 0)
                {
                    fileName = options.opt_arg();
                    std::cout << "Will read the CAN bus messages from the "
                        "file "
                        << fileName
                        << ".\n";
                }
                else
                {
                    std::cout << "\n\n*** You must provide a file name with "
                        "this option! ***\n\n";
                    usage(argv[0]);
                    return -ENOENT;
                }
            }
            break;

            case '?':
            {
                std::cout << "Option -"
                    << options.opt_opt()
                    << "requires an argument!\n";
            }

            // Fall through...
            case 'h':
            default:    // Display help.
            {
                usage(argv[0]);
                return 0;
            }
        }
    }


    boost::shared_ptr< std::ifstream > fileInput;
    if(fileName.empty() == true)
    {
        std::cout << "\n\n*** A file name must be provided! ***\n\n";
        usage(argv[0]);
        return -ENOENT;
    }
    else
    {
        fileInput.reset(new std::ifstream(fileName.c_str(),
            std::ios_base::in));
        if(fileInput->is_open() == false)
        {
            std::cout << "\n\n*** Cannot open the file \""
                << fileName
                << "\".  Cannot proceed. ***\n";
            return -ENOENT;
        }
    }

#define NO_RTOS
#ifndef NO_RTOS
    // Open the CAN message fifos.
    const int fd0(open("/dev/rtf9", O_WRONLY | O_APPEND));
    if(fd0 < 0)
    {
        std::cout << "Failed to open CAN message fifo /dev/rtf9\n";
        return -ENOENT;
    }
#endif

    struct sigaction newAction;
    std::memset(&newAction, 0, sizeof(struct sigaction));

    newAction.sa_sigaction = sigHandler;
    newAction.sa_flags = SA_SIGINFO | SA_RESETHAND;

    if(sigaction(SIGINT, &newAction, 0) != 0)
    {
        std::cout << "Error while installing the new signal handler: "
            << std::strerror(errno)
            << "\n";
    }

    unsigned int index(0U);
    std::string tmpString;
    CAN_MON_MSG message;
    std::vector< CAN_MON_MSG > messages;

    std::cout << "\n\nCount           Time (ns)             CAN id (hex)    "
        "  No of bytes     Data (hex)\n";
#if __GNUC__ >= 4
    while((__sync_fetch_and_or(&exitFlag, 0) != 1)
    && (fileInput->good() == true))
#else
    while((atomic_read(&exitFlag) != 1)
    && (fileInput->good() == true))
#endif
    {
        std::getline(*fileInput, tmpString);
        if(fileInput->eof() == true)
        {
            break;
        }

        createMessage(tmpString, message);
        if(messages.empty() == false)
        {
            if((message.MsgBuf.Identifier
                == (messages.back()).MsgBuf.Identifier)
            &&(message.MsgBuf.MsgLen != 0))
            {
                // This is a reply from the hardware.  Ignore it.
                std::cout << "*** Intercepted a reply from the hardware. "
                    "Therefore this message will not be replayed but simply "
                    "be skipped.  This is the message:\n";
                printMessage(message, index);
                std::cout << "\n\n";

                continue;
            }
            else
            {
                ++index;
                printMessage(message, index);
                messages.push_back(message);
            }
        }
        else
        {
            if(message.MsgBuf.MsgLen == 0)
            {
                ++index;
                printMessage(message, index);
                messages.push_back(message);
            }
            else
            {
                // This is either reply from the hardware or it is a
                // control request.  There is no way to distinguish them
                // as first message.
                std::cout << "*** The first message cannot be identified as "
                    "monitor point request but it seems that it is either "
                    "a reply from the hardware or it is a control request. "
                    "They are not distinguishable without knowing the "
                    "previous transaction, so this message will be skipped.  "
                    "This is the message:\n";
                printMessage(message, index);
                std::cout << "\n\n";

                continue;
            }
        }
    };

    if(messages.empty() == true)
    {
        std::cout << "*** No messages are scheduled for replaying.  "
            "Good bye!\n\n";
        return -ESUCCESS;
    }
    else
    {
        std::cout << "*** Will begin replaying "
            << messages.size()
            << " message now...\n";
    }

    index = 0U;
    std::vector< CAN_MON_MSG >::const_iterator iter(messages.begin());
    ACE_Time_Value waitFor(0ULL);
#ifndef NO_RTOS
    int count(-1);

    count = write(fd0, &(*iter), sizeof(CAN_MON_MSG));
    if(count == sizeof(CAN_MON_MSG))
    {
#endif
        ++iter;
        ++index;
#ifndef NO_RTOS
    }
    else
    {
        std::cout << "*** Could not send a message for replay:\n";
#endif
        printMessage(*iter, index);
#ifndef NO_RTOS
    }
#endif

#if __GNUC__ >= 4
    while((__sync_fetch_and_or(&exitFlag, 0) != 1)
    && (iter != messages.end()))
#else
    while((atomic_read(&exitFlag) != 1)
    && (iter != messages.end()))
#endif
    {
        std::cout << "Sleep "
            << ((*iter).TimeStamp - (*(iter - 1)).TimeStamp) * 1e-7
            << "s\n";
        waitFor.set(((*iter).TimeStamp - (*(iter - 1)).TimeStamp) * 1e-7);
        ACE_OS::select(0, 0, 0, 0, waitFor);

#ifndef NO_RTOS
        count = write(fd0, &(*iter), sizeof(CAN_MON_MSG));
        if(count == sizeof(CAN_MON_MSG))
        {
#endif
            ++iter;
            ++index;
#ifndef NO_RTOS
        }
        else
        {
            std::cout << "*** Could not send a message for replay:\n";
#endif
            printMessage(*iter, index);
#ifndef NO_RTOS
        }
#endif
    }

#ifndef NO_RTOS
    close(fd0);
#endif

    std::cout << "Exiting.  Good bye!\n\n";

    return 0;
}
