//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <iostream>
#include <sstream>
#include <fstream>
#include <ios>
#include <iomanip>
#include <cerrno>
#include <Get_Opt.h>
#include <boost/shared_ptr.hpp>
#include <rtai_fifos.h>
#include <signal.h>
#if __GNUC__ < 4
#include <asm/atomic.h>
#endif
#include "canMonDrv.h"


static unsigned int exitFlag(0);
static const unsigned short defaultChannel(0U);


void sigHandler(int signal, siginfo_t* info, void* data)
{
    // Set the finish flag atomically.
#if __GNUC__ >= 4
    __sync_add_and_fetch(&exitFlag, 1);
#else
    atomic_set(&exitFlag, 1);
#endif
}


static void checkBase(std::istringstream& input)
{
    // Check if the first two letters are 0x or 0X and set
    // the decoding of the std::istringstream accordingly.
    std::string subString(input.str().substr(0, 2));
    if((subString == "0x") || (subString == "0X"))
    {
        input.setf(std::ios::hex, std::ios::basefield);
        return;
    }

    // Now check if they are octal, e.g. the first letter is 0.
    subString = input.str().substr(0, 1);
    if(subString == "0")
    {
        input.setf(std::ios::oct, std::ios::basefield);
        return;
    }
}

static void printMessage(const CAN_MON_MSG& canMsg, unsigned int msgCount)
{
    std::ostringstream msg;
    msg << std::dec
        << std::setw(12)
        << std::setfill('0')
        << msgCount
        << "    "
        << canMsg.TimeStamp
        << "    0x"
        << std::hex
        << std::setw(8)
        << std::setfill('0')
        << static_cast< unsigned long >(canMsg.MsgBuf.Identifier)
        << "        "
        << std::dec
        << std::setw(1)
        << static_cast< unsigned short >(canMsg.MsgBuf.MsgLen)
        << "                ";

    if(canMsg.MsgBuf.MsgLen > 0U)
    {
        msg << "0x";

        for(unsigned short j(0U); j < canMsg.MsgBuf.MsgLen; ++j)
        {
            msg << std::hex
                << std::setw(2)
                << std::setfill('0')
                << static_cast< unsigned short >(canMsg.MsgBuf.Data[j]);
        }

    }
    else
    {
        msg << "N/A";
    }

    msg << "\n";
    std::cout << msg.str();
}

static void usage(char* argv0)
{
    std::cout << "\n"
        << argv0
        << "::usage:\n"
            "Options:\n"
            "[--help|h]:\n"
            "     You are reading it!\n"
            "[--file=|-f<canMonitor output file>]:\n"
            "      File which will contain the CAN bus monitor data.\n"
            "[--channel=|-c<CHANNEL>]:\n"
            "      ABM channel (dec., hex. or ocatal base). Default = "
        << defaultChannel
        << "\n";
}

int main(int argc, char *argv[])
{
    short channel(-1);
    std::string fileName;

    ACE_Get_Opt options(argc, argv);

    options.long_option("help", 'h');
    options.long_option("channel", 'c', ACE_Get_Opt::ARG_OPTIONAL);
    options.long_option("fileName", 'f', ACE_Get_Opt::ARG_OPTIONAL);

    int c(0);
    while((c = options()) != EOF)
    {
        switch(c)
        {
            case 'c':
            {
                if(options.opt_arg() != 0)
                {
                    std::istringstream value;
                    value.str(options.opt_arg());
                    short int val(0);
                    checkBase(value);
                    value >> val;
                    if((val < 0) || (val > 5))
                    {
                        std::cout << "The channel number ("
                            << val
                            << "has to be in the range of [0; 5].  The value "
                               "will not be used.\n";
                    }
                    else
                    {
                        channel = val;
                        std::cout << "The ABM channel has been set to "
                            << channel
                            << ".\n";
                    }
                }
            }
            break;

            case 'f':
            {
                if(options.opt_arg() != 0)
                {
                    fileName = options.opt_arg();
                    std::cout << "Will store the CAN bus messages to the "
                        "file "
                        << fileName
                        << ".\n";
                }
                else
                {
                    std::cout << "\n\n*** You must provide a file name with "
                        "this option! ***\n\n";
                    usage(argv[0]);
                    return -ENOENT;
                }
            }
            break;

            case '?':
            {
                std::cout << "Option -"
                    << options.opt_opt()
                    << "requires an argument!\n";
            }

            // Fall through...
            case 'h':
            default:    // Display help.
            {
                usage(argv[0]);
                return 0;
            }
        }
    }


    // Check for valid channel.
    if(channel == -1)
    {
        // Default value.
        channel = 5;
    }
    else if((channel < 0) || (channel > 5))
    {
        std::cout << "Invalid channel number "
            << channel
            << " for the CAN bus interface.  Valid channels are 0 to 5\n\n";
        return -1;
    }

    // Open the CAN message fifos
    const int fd0(open("/dev/rtf9", O_RDONLY));
    if(fd0 < 0)
    {
        std::cout << "Failed to open CAN message fifo /dev/rtf9\n";

        return -1;
    }

    struct sigaction newAction;
    std::memset(&newAction, 0, sizeof(struct sigaction));

    newAction.sa_sigaction = sigHandler;
    newAction.sa_flags = SA_SIGINFO | SA_RESETHAND;

    if(sigaction(SIGINT, &newAction, 0) != 0)
    {
        std::cout << "Error while installing the new signal handler: "
            << std::strerror(errno)
            << "\n";
    }

    boost::shared_ptr< std::ofstream > fileOutput;
    if(fileName.empty() == false)
    {
        fileOutput.reset(new std::ofstream(fileName.c_str(),
            std::ios_base::out | std::ios_base::app));
        if(!fileOutput.get())
        {
            std::cout << "\n\n*** Cannot create the file \""
                << fileName
                << "\".  Will proceed and output only to the console. ***\n";
            fileName.clear();
        }
    }

    std::cout << "\n\nCount           Time (ns)             CAN id (hex)    "
        "  No of bytes     Data (hex)\n";

    CAN_MON_MSG endMsg;
    std::memset(&endMsg, 0, sizeof(CAN_MON_MSG));
    CAN_MON_MSG canMsg;
    unsigned int count(0U);
    unsigned int msgCount(0U);
    std::ostringstream msg;

#if __GNUC__ >= 4
    while(__sync_fetch_and_or(&exitFlag, 0) != 1)
#else
    while(atomic_read(&exitFlag) != 1)
#endif
    {
        std::memset(&canMsg, 0, sizeof(CAN_MON_MSG));
        if((count = read(fd0, &canMsg, sizeof(CAN_MON_MSG)))
            == sizeof(CAN_MON_MSG))
        {
            if(std::memcmp(&endMsg, &canMsg, sizeof(CAN_MON_MSG)) != 0)
            {
                ++msgCount;
                printMessage(canMsg, msgCount);

                if(fileOutput.get() != 0)
                {
                    *fileOutput << "TimeStamp="
                        << canMsg.TimeStamp
                        << " Identifier="
                        << canMsg.MsgBuf.Identifier
                        << " Timeout="
                        << canMsg.MsgBuf.Timeout
                        << " RxQueueNum="
                        << static_cast< unsigned short >(canMsg.MsgBuf.RxQueueNum)
                        << " Extended="
                        <<  static_cast< unsigned short >(canMsg.MsgBuf.Extended)
                        << " Status="
                        <<  static_cast< unsigned short >(canMsg.MsgBuf.Status)
                        << " MsgLen="
                        <<  static_cast< unsigned short >(canMsg.MsgBuf.MsgLen);

                    if(canMsg.MsgBuf.MsgLen > 0U)
                    {
                        (*fileOutput) << " Data=";
                        for(unsigned short i(0); i < canMsg.MsgBuf.MsgLen; ++i)
                        {
                                (*fileOutput) << static_cast< unsigned short >(canMsg.MsgBuf.Data[i])
                                    << " ";
                        }
                    }

                    (*fileOutput) << "\n";
                }
            }
            else
            {
                std::cout << "*** Received an illegal CAN message! ***\n";
            }
        }
        else
        {
            continue;
        }
    }

    if(fileOutput.get() != 0)
    {
        fileOutput->close();
    }

    close(fd0);

    std::cout << "Exiting.  Good bye!\n\n";

    return 0;
}
