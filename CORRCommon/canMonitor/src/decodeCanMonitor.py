#! /usr/bin/env python
#
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2010
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# $Id$
# $Source$
#

# This tools replaces some data in the output of the canMonitor with human
# readable output.  Example of the original output:
#
#    tjuerges@delphinus canMonitor 65 cat canMonitor.data
#    TimeStamp=135023236725123604 Identifier=16777216 Timeout=10000 RxQueueNum=1 Extended=1 Status=0 MsgLen=8 Data=16 7 43 252 0 8 0 227
#    TimeStamp=135024916027446065 Identifier=262162 Timeout=10000 RxQueueNum=1 Extended=1 Status=0 MsgLen=0
#    TimeStamp=135024916027448012 Identifier=262162 Timeout=10000 RxQueueNum=1 Extended=1 Status=0 MsgLen=8 Data=31 255 189 117 31 255 189 117
#    TimeStamp=135024916027450547 Identifier=262146 Timeout=10000 RxQueueNum=1 Extended=1 Status=0 MsgLen=0
#    TimeStamp=135024916472430498 Identifier=262186 Timeout=10000 RxQueueNum=1 Extended=1 Status=0 MsgLen=6 Data=0 0 0 0 0 0
#
# Example of the "decoded" output which replaces the time stamp and the
# identifier fields:
#
#    tjuerges@delphinus canMonitor 66 ./decodeCanMonitor.py canMonitor.data
#    TimeStamp=2010-08-28T21:27:52.5123604 Node=0x03f RCA=0x00000 Timeout=10000 RxQueueNum=1 Extended=1 Status=0 MsgLen=8 Data=0x10 0x07 0x2b 0xfc 0x00 0x08 0x00 0xe3
#    TimeStamp=2010-08-30T20:06:42.7446065 Node=0x000 RCA=0x00012 Timeout=10000 RxQueueNum=1 Extended=1 Status=0 MsgLen=0
#    TimeStamp=2010-08-30T20:06:42.7448012 Node=0x000 RCA=0x00012
#    Timeout=10000 RxQueueNum=1 Extended=1 Status=0 MsgLen=8 Data=0x1f 0xff 0xbd 0x75 0x1f 0xff 0xbd 0x75
#    TimeStamp=2010-08-30T20:06:42.7450547 Node=0x000 RCA=0x00002 Timeout=10000 RxQueueNum=1 Extended=1 Status=0 MsgLen=0
#    TimeStamp=2010-08-30T20:07:27.2430498 Node=0x000 RCA=0x0002a
#    Timeout=10000 RxQueueNum=1 Extended=1 Status=0 MsgLen=6 Data=0x00 0x00 0x00 0x00 0x00 0x00 
#


import string
import sys
import time
import acstime
import Acspy.Common.TimeHelper


def tj_convertTimeStampArray(tsArray = [], printIt = True):
    for i in range(len(tsArray)):
        acsTimestamp = acstime.Epoch(tsArray[i])
        epoch = Acspy.Common.TimeHelper.TimeUtil()
        epochPy = epoch.epoch2py(acsTimestamp)
        pyEpoch = epoch.py2epoch(epochPy)
        fracSecs = tsArray[i] - pyEpoch.value
        t = time.gmtime(epochPy)

        if printIt == False:
            return (t, fracSecs)

        print "\n%d = %04d-%02d-%02dT%02d:%02d:%02d.%07d\n" \
        % (tsArray[i], t.tm_year, t.tm_mon, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec, fracSecs)
    return


def tj_convertTimeStamp(ts, printIt = True):
    tsArray = []
    tsArray.append(ts)
    return tj_convertTimeStampArray(tsArray, printIt)


def tj_toAddress(nodeId = None, rca = None, printOut = True):
    if nodeId == None or rca == None:
        print "Node ID and/or RCA cannot be empty!"
        return
    
    if nodeId > 0x7ff or rca > 0x3ffff:
        print "Requested address is outside of the allowable range"
    else:
        if printOut == True:
            print "Node ID = 0x%03x, RCA = 0x%05x --> address = " \
            "0x%08x." \
            % (nodeId, rca, ((nodeId + 1) << 18) | rca)

        return ((nodeId + 1) << 18) | rca


def tj_addressToNodeRca(address = None, printOut = True):
    if address == None:
        print "The address cannot be empty!"
        return
    rca = address & 0x3ffff
    node = ((address >> 18) - 1) & 0x7ff
    if printOut == True:
        print "Address = 0x%08x --> Node ID = 0x%03x, RCA = 0x%05x." \
        % (address, node, rca)

    return (node, rca)


def decodeTimeStamp(line):
    timestampString = line.split(" Identifier=")[0]
    timestamp = int(timestampString.split("TimeStamp=")[1])
    (t, fracSecs) = tj_convertTimeStamp(timestamp, False)
    properTimestampString = "%04d-%02d-%02dT%02d:%02d:%02d.%07d" \
    % (t.tm_year, t.tm_mon, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec, fracSecs)
    newline = line.replace(str(timestamp), properTimestampString)
    return newline


def decodeIdentifier(line):
    identifier = line.split("Identifier=")
    address = int(identifier[1].split(" Timeout")[0])
    (node, rca) = tj_addressToNodeRca(address, False)
    oldIdentifier = "Identifier=%d" % (address)
    newIdentifier = "Node=0x%03x RCA=0x%05x" % (node, rca)
    newLine = line.replace(oldIdentifier, newIdentifier)
    return newLine


def dataToHex(line):
    data = line.strip().split("Data=")
    if len(data) == 1:
        # No Data, just return the original line.
        return line
 
    pureData = data[1].split(" ")
    newData = "Data="
    for element in pureData:
        newData += ("0x%02x " % (int(element)))

    newLine = data[0] + newData + "\n"
    return newLine


if len(sys.argv) != 2:
    print "Please provide the name of the canMonitor data file!"
    sys.exit(-1)

dataFile = file(sys.argv[1], "r")

lineNumber = 0
for line in dataFile:
    lineNumber += 1
    try:
        if len(line) < 2:
           raise Exception("Line length < 2!")
        newline = decodeTimeStamp(line)
        newLine = decodeIdentifier(newline)
        newLine = dataToHex(newLine)
        print "%s" % (newLine),
    except:
        print "Could not decode line #%d, skipping it! Lenght of line = %d. " \
            "This is the line before decoding it:\n\"%s\"" \
            % (lineNumber, len(line), line.replace("\n", ""))

dataFile.close()
