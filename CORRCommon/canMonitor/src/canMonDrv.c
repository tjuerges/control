/*
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * $Id$
 * $Source$
 *
 * who      when       what
 * bgustafs 2006-03-20 created
 * tjuerges 2009-01-16 new owner
 */


/**
 * System stuff.
 */
#include <linux/moduleparam.h>
#include <linux/delay.h>
#include <asm/atomic.h>
#include <linux/errno.h>
#include <linux/proc_fs.h>

/**
 * RTAI stuff.
 */
#include <rtai_sched.h>
#include <rtai_registry.h>
#include <rtai_fifos.h>

/**
 * ACS stuff.
 */
#include <rtTools.h>
#include <rtLog.h>

/**
 * CORRCommon stuff.
 */
#include <tpmc901.h>
#include <rtDevDrv.h>
#include <teHandler.h>

/**
 * Local stuff.
 */
#include "canMonDrv.h"


#define moduleName "canModDrv"

MODULE_AUTHOR("Thomas Juerges <tjuerges@nrao.edu>");
MODULE_DESCRIPTION(moduleName ": CAN monitor kernel module.");
MODULE_LICENSE("GPL");


/**
 * Module parameters.
 */
static unsigned int debugChannel = 5;
module_param(debugChannel, uint, S_IRUGO);
static unsigned int initTimeout = 1000; /* msec */
module_param(initTimeout, uint, S_IRUGO);
static unsigned int cleanUpTimeout = 1000; /* msec */
module_param(cleanUpTimeout, uint, S_IRUGO);

/**
 * Local data.
 */
static char softwareVersion[] =
    "$Id$";

static const unsigned int CAN_MON_TASK_STACK = 10000U;
static const unsigned int CAN_MON_TASK_PRIO = 10U;

/**
 * FIFO definition for forwarding the CAN messages to user space.
 */
static const unsigned int CAN_MON_FIFO = 9U;

/**
 * Data structure for the /proc page.
 */
static struct proc_dir_entry* procEntry = 0;

static RT_TASK realTimeTask;
static atomic_t terminate = ATOMIC_INIT(0);
/**
 * The atomic_t will be 32 bits in size, hence the counter will last, if one
 * message per TE is sent or received, for 6about .5 years.  If this is not
 * enough, I can use atomic_long_t as soon as we switch to 64 bit.
 */
static atomic_t numberOfMessages = ATOMIC_INIT(0L);
static atomic_t numberOfErrors = ATOMIC_INIT(0L);
static int deviceID = 0;


/**
 * Proc_fs reading function.
 */
static int readProcFsPage(char* buffer, char** start, off_t offset, int length,
    int* eof, void* data)
{
    int size = 0;

    size += sprintf(buffer + size, "%s kernel module information page\n\n"
        "Software version = %s\n\n"
        "Device ID assigned by rtTools kernel module = %d\n"
        "CAN channel = %u\n"
        "Number of CAN messages = %u\n"
        "Number of faulty CAN messages = %u\n",
        moduleName,
        softwareVersion,
        deviceID,
        debugChannel,
        atomic_read(&numberOfMessages),
        atomic_read(&numberOfErrors));

    size +=  sprintf(buffer + size, "\n");

    return size;
}


/**
 * canSetBusBaudRate - set the baud rate of the CAN bus
 */
static int canSetBusBaudRate(int channel, int deviceID)
{
    int result = 0;

    /* Set the bus baude rate to 1 MBit/s */
    tpmc901IOCTLStruct ioctlRequest;
    TP901_BITTIMING BitTiming;

    BitTiming.timing_value = TP901_1MBIT;
    BitTiming.three_samples = 0;

    ioctlRequest.channel = channel;
    ioctlRequest.arg = (unsigned long)&BitTiming;

    result = rtIoctl(deviceID, TP901_IOCSBITTIMING, (void*)&ioctlRequest);
    if(result == -EACCES)
    {
        /*
         * This means that the CAN bus card has already been initialised and is
         * ready to be used.  Ignore this return value.
         */
        result = 0;
    }

    return result;
}

/**
 * canBusOn - switch CAN bus on
 */
static int canBusOn(int channel, int deviceID)
{
    int result = 0;

    /*
     * Switch the bus on.
     */
    tpmc901IOCTLStruct ioctlRequest;
    ioctlRequest.channel = channel;

    result = rtIoctl(deviceID, TP901_IOCBUSON, (void*)&ioctlRequest);
    return result;
}

/**
 * canBusOff - switch CAN bus off
 */
static int canBusOff(int channel, int deviceID)
{
    int result = 0;

    /*
     * Switch the bus off.
     */
    tpmc901IOCTLStruct ioctlRequest;
    ioctlRequest.channel = channel;

    result = rtIoctl(deviceID, TP901_IOCBUSOFF, (void*)&ioctlRequest);
    return result;
}

/**
 * canSetAcceptanceMask
 */
static int canSetAcceptanceMasks(int channel, int deviceID)
{
    int result = 0;

    /*
     * Set all of the acceptance masks to zero.
     */
    tpmc901IOCTLStruct ioctlRequest;
    TP901_ACCEPT_MASKS AcceptMasks;
    AcceptMasks.global_mask_standard = 0;
    AcceptMasks.global_mask_extended = 0;
    AcceptMasks.message_15_mask = 0;

    ioctlRequest.channel = channel;
    ioctlRequest.arg = (unsigned long)&AcceptMasks;

    result = rtIoctl(deviceID, TP901_IOCSSETFILTER, (void*)&ioctlRequest);
    return result;
}

/**
 * canSetMessageObject15
 */
static int canSetMessageObject15(int channel, int deviceID)
{
    int result = 0;

    /*
     * Set up message object 15 as a receive object for an
     * extended ID of 0 (all messages will be received)
     */
    tpmc901IOCTLStruct ioctlRequest;
    TP901_BUF_DESC BufDesc;
    BufDesc.msg_obj_num = 15;
    BufDesc.extended = 1;
    BufDesc.identifier = 0;
    BufDesc.rx_queue_num = 1;

    ioctlRequest.channel = channel;
    ioctlRequest.arg = (unsigned long)&BufDesc;

    result = rtIoctl(deviceID, TP901_IOCSDEFRXBUF, (void*)&ioctlRequest);
    if(result == -EADDRINUSE)
    {
        /*
         * This means that the CAN bus card has already been initialised and is
         * ready to be used.  Ignore this return value.
         */
        result = 0;
    }

    return result;
}

/**
 * canFlush - flush all receive queues
 */
static int canFlush(int channel, int deviceID)
{
    int result = 0;

    tpmc901IOCTLStruct ioctlRequest;
    ioctlRequest.channel = channel;
    ioctlRequest.arg = 0;

    result = rtIoctl(deviceID, TP901_IOCFLUSH, (void*)&ioctlRequest);
    return result;
}

/**
 * Real-time task.
 */
static void realTimeTaskFunction(long junk __attribute__((unused)))
{
    CAN_MON_MSG canMsg;
    int result = -1;
    int CANReadTimeout = 10000;
    rtlogRecord_t logRecord;

    RTLOG_INFO(moduleName, "Starting the CAN channel eavesdrop.");

    /*
     * Read the responses
     */
    atomic_set(&terminate, 2);
    while(atomic_read(&terminate) != 1)
    {
        canMsg.MsgBuf.Timeout = CANReadTimeout;
        canMsg.MsgBuf.RxQueueNum = 1;
        canMsg.MsgBuf.Extended = 1;
        result = rtRead(deviceID, &debugChannel, (void*)&canMsg.MsgBuf,
            sizeof(canMsg.MsgBuf), CANReadTimeout);

        if((result < 0) && (result != -ETIME))
        {
            atomic_inc(&numberOfErrors);
            RTLOG_ERROR(moduleName, "Failed to read a CAN message.");
        }
        else if(result >= 0)
        {
            atomic_inc(&numberOfMessages);
            canMsg.TimeStamp = teHandlerGetTime();
            rtf_put(CAN_MON_FIFO, (void*)&canMsg, sizeof(CAN_MON_MSG));
        }
    }

    RTLOG_INFO(moduleName, "Main task has terminated.");

    atomic_set(&terminate, 0);

    rt_task_delete(rt_whoami());
}

static int firstInitSection(void)
{
    int status = RT_TOOLS_MODULE_INIT_ERROR;
    int fifoStat = -1;
    rtlogRecord_t logRecord;

    /*
     * Create FIFOs used for communication with the canMonitor user space
     * task.
     */
    if((fifoStat = rtf_create(CAN_MON_FIFO, sizeof(CAN_MON_MSG) * 1000)) < 0)
    {
        RTLOG_ERROR(moduleName, "Cannot create monitor data FIFO, error = "
            "%d).", fifoStat);

    }
    /*
     * Reset the FIFOs just in case of an old buffered value.
     */
    else if((fifoStat = rtf_reset(CAN_MON_FIFO)) != 0)
    {
        RTLOG_ERROR(moduleName, "Failed to reset the monitor data FIFO, error "
            "= %d).", fifoStat);
    }
    /*
     * Open the CAN device
     */
    else if((deviceID = rtOpen(TPMC901_RT_DEV_DRV_NAME)) <= 0)
    {
        RTLOG_ERROR(moduleName, "Device could not be opened, error = %d",
            deviceID);
    }
    else
    {
        status = RT_TOOLS_MODULE_INIT_SUCCESS;
    }

    return status;
}

static int firstCleanUpSection(void)
{
    rtlogRecord_t logRecord;

    if(deviceID > 0)
    {
        rtClose(deviceID);
    }

    RTLOG_INFO(moduleName, "Monitor data FIFO closed, count = %d.",
        rtf_destroy(CAN_MON_FIFO));

    return RT_TOOLS_MODULE_EXIT_SUCCESS;
}


static int secondInitSection(void)
{
    int status = RT_TOOLS_MODULE_INIT_ERROR;
    rtlogRecord_t logRecord;

    /*
     * Set the bus baud rate
     */
    if((status = canSetBusBaudRate(debugChannel, deviceID)) < 0)
    {
        RTLOG_ERROR(moduleName, "Failed to set bit timing of CAN channel %u, "
            "error = %d.",
            debugChannel, status);
    }
    /*
     * Switch the bus on
     */
    else if((status = canBusOn(debugChannel, deviceID)) < 0)
    {
        RTLOG_ERROR(moduleName, "Failed to switch on CAN channel %u, error = "
            "%d.",
            debugChannel, status);

        return status;
    }
    /*
     * Set all of the acceptance masks to zero
     */
    else if((status = canSetAcceptanceMasks(debugChannel, deviceID)) < 0)
    {
        RTLOG_ERROR(moduleName, "Failed to set acceptance mask of CAN "
            "channel %u, error = %d.", debugChannel, status);
    }
    /*
     * Set up message object 15 as a receive object for an
     * extended ID of 0 (all messages will be received)
     */
    else if((status = canSetMessageObject15(debugChannel, deviceID)) < 0)
    {
        RTLOG_ERROR(moduleName, "Failed to set up message object 15 of CAN "
            "channel %u, error = %d.", debugChannel, status);
    }
    /*
     * Flush all receive queues
     */
    else if((status = canFlush(debugChannel, deviceID)) < 0)
    {
        RTLOG_ERROR(moduleName, "Failed to flush CAN channel %u, error = %d.",
            debugChannel, status);
    }
    else
    {
        status = RT_TOOLS_MODULE_INIT_SUCCESS;
    }

    return status;
}

static int secondCleanUpSection(void)
{
    rtlogRecord_t logRecord;

    if(deviceID > 0)
    {
        int status = canBusOff(debugChannel, deviceID);
        if(status < 0)
        {
            RTLOG_ERROR(moduleName, "Failed to switch off CAN channel %u.",
                debugChannel);
        }
    }

    return RT_TOOLS_MODULE_EXIT_SUCCESS;
}

/*
 * Intialization task.
 */
static void initTaskFunction(long flag)
{
    /* status = 1 means OK. */
    int status = 0;
    atomic_t* flag_p = (atomic_t*)flag;
    rtlogRecord_t logRecord;

    RTLOG_INFO(moduleName, "Initialisation task started");

    if((status = firstInitSection()) != RT_TOOLS_MODULE_INIT_SUCCESS)
    {
        RTLOG_ERROR(moduleName, "firstInitSection failed!");
    }
    else if((status = secondInitSection()) != RT_TOOLS_MODULE_INIT_SUCCESS)
    {
        RTLOG_ERROR(moduleName, "secondInitSection failed!");
    }
    /*
     * Spawn the main task. Set it to run in the first cpu.
     */
    else if(rt_task_init_cpuid(&realTimeTask, realTimeTaskFunction, 0,
        CAN_MON_TASK_STACK, CAN_MON_TASK_PRIO, 0, 0, 0) != 0)
    {
        RTLOG_ERROR(moduleName, "Failed to spawn main task!");
    }
    /*
     * Resume the main task.
     */
    else if(rt_task_resume(&realTimeTask) != 0)
    {
        RTLOG_ERROR(moduleName, "Could not resume the main task!");
    }
    else
    {
        status = 1;
    }

    RTLOG_INFO(moduleName, "Initialisation task done.");

    atomic_set(flag_p, status);

    rt_task_delete(rt_whoami());
}

/*
 * clean-up task
 */
static void cleanUpTaskFunction(long flag)
{
    /* status = 1 means OK. */
    int status = 1;
    int waitCount = 0;
    atomic_t* flag_p = (atomic_t*)flag;
    rtlogRecord_t logRecord;

    RTLOG_INFO(moduleName, "Clean-up task started.");

    /*
     * Command the main task to finish if it has ever started.
     */
    if(atomic_read(&terminate) == 2)
    {
        atomic_set(&terminate, 1);

        /*
         * Wait for the main task to be gone.  It will set the
         * terminate flag to 0 when done.
         */
        while((atomic_read(&terminate) != 0) && (waitCount < 100))
        {
            ++waitCount;

            /*
             * Sleep for 1ms.
             */
            rt_sleep(nano2count(1000000LL));
        }
        /*
         * If the main task did not finish on time then forcibly remove
         * it for the rtai's scheduler.
         */
        if(waitCount == 100)
        {
            status = 0;

            rt_task_suspend(&realTimeTask);
            rt_task_delete(&realTimeTask);

            RTLOG_ERROR(moduleName, "Main task did not finish on time!");
        }
    }

    if(secondCleanUpSection() != RT_TOOLS_MODULE_EXIT_SUCCESS)
    {
        status = 0;
       RTLOG_ERROR(moduleName, "secondCleanUpSection failed! Continuing "
           "with clean up.");
    }

    if(firstCleanUpSection() != RT_TOOLS_MODULE_EXIT_SUCCESS)
    {
        status = 0;
       RTLOG_ERROR(moduleName, "firstCleanUpSection failed! Continuing "
           "with clean up.");
    }

    RTLOG_INFO(moduleName, "Clean-up task done.");

    atomic_set(flag_p, status);

    rt_task_delete(rt_whoami());
}

static int canMonDrv_main(int stage)
{
    int status = RT_TOOLS_MODULE_INIT_ERROR;
    int temporaryStatus = RT_TOOLS_MODULE_INIT_EXIT_ERROR;
    atomic_t initFlag = ATOMIC_INIT(0);
    atomic_t cleanUpFlag = ATOMIC_INIT(0);
    RT_TASK initTask, cleanUpTask;
    rtlogRecord_t logRecord;

    if(stage == RT_TOOLS_MODULE_STAGE_INIT)
    {
        status = RT_TOOLS_MODULE_INIT_ERROR;

        /*
         * Log CVS version and other configuration stuff.
         */
        RTLOG_INFO(moduleName, "%s", softwareVersion);

        RTLOG_INFO(moduleName, "Initializing module...");

        RTLOG_INFO(moduleName, "initTimeout = %dms.", initTimeout);
        RTLOG_INFO(moduleName, "cleanUpTimeout = %dms.", cleanUpTimeout);
        RTLOG_INFO(moduleName, "CAN channel = %u.", debugChannel);

        goto Initialization;
    }
    else
    {
        status = RT_TOOLS_MODULE_EXIT_SUCCESS;

        RTLOG_INFO(moduleName, "Cleaning up module...");

        goto FullCleanUp;
    }

Initialization:
    /*
     * Initialization of resources is finished. Now spawn the rtai
     * task that takes care of doing the asynchronous part of the
     * initialization.
     */
    if(rt_task_init(&initTask,
        initTaskFunction,
        (long)&initFlag,
        RT_TOOLS_INIT_TASK_STACK,
        RT_TOOLS_INIT_TASK_PRIO,
        0, 0)
    != 0)
    {
        status = RT_TOOLS_MODULE_NO_INIT_TASK_ERROR;

        RTLOG_ERROR(moduleName, "Failed to spawn initialization task!");

        goto Exit;
    }
    else
    {
        RTLOG_INFO(moduleName, "Created initalisation task, address = 0x%p.",
            &initTask);
    }

    /*
     * Resume the initialization task
     */
    if(rt_task_resume(&initTask) != 0)
    {
        rt_task_delete(&initTask);
        status = RT_TOOLS_MODULE_NO_INIT_TASK_ERROR;

        RTLOG_ERROR(moduleName, "Cannot resume initalisation task!");

        goto Exit;
    }

    /*
     * wait for the initialization task to be ready
     */
    if(rtToolsWaitOnFlag(&initFlag, initTimeout) != 0)
    {
        /*
         * okay, the init task was not ready on time, now forcibly remove
         * it for the rtai's scheduler
         */
        rt_task_suspend(&initTask);
        rt_task_delete(&initTask);

        RTLOG_ERROR(moduleName, "Initialization task did not report back on "
            "time!");

        status  = RT_TOOLS_MODULE_INIT_TIMEOUT;

        goto FullCleanUp;
    }
    else
    {
        status = RT_TOOLS_MODULE_INIT_SUCCESS;

        procEntry = create_proc_entry(moduleName, S_IRUGO | S_IWUSR, NULL);
        if(procEntry == 0)
        {
            RTLOG_DEBUG(moduleName, "Failed to register the proc FS entry.");
        }
        else
        {
            procEntry->read_proc = readProcFsPage;
        }
    }

    goto Exit;


FullCleanUp:
    remove_proc_entry(moduleName, NULL);

    /*
     * Asynchronous clean up phase to be handled by this task.
     */
    if(rt_task_init(&cleanUpTask,
        cleanUpTaskFunction,
        (long)&cleanUpFlag,
        RT_TOOLS_CLEANUP_TASK_STACK,
        RT_TOOLS_CLEANUP_TASK_PRIO,
        0, 0)
    != 0)
    {
        RTLOG_ERROR(moduleName, "Failed to spawn clean up task!");

        temporaryStatus = RT_TOOLS_MODULE_CLEANUP_ERROR;

        goto Exit;
    }
    else
    {
        RTLOG_INFO(moduleName, "Created clean up task, address = 0x%p.",
            &cleanUpTask);
    }

    /*
     * Resume the clean up task.
     */
    if(rt_task_resume(&cleanUpTask) != 0)
    {
        rt_task_delete(&cleanUpTask);
        temporaryStatus = RT_TOOLS_MODULE_CLEANUP_ERROR;

        RTLOG_ERROR(moduleName, "Cannot resume clean up task!");

        goto Exit;
    }

     /*
      * Wait for the initialization task to become ready.
      */
    if(rtToolsWaitOnFlag(&cleanUpFlag, cleanUpTimeout) != 0)
    {
        /*
         * Okay, the clean up task was not ready on time, now forcibly remove
         * it for the rtai's scheduler.
         */
        rt_task_suspend(&cleanUpTask);
        rt_task_delete(&cleanUpTask);

        RTLOG_ERROR(moduleName, "Clean up task did not report back on time!");

        temporaryStatus = RT_TOOLS_MODULE_CLEANUP_ERROR;
     }
    else
    {
        temporaryStatus = RT_TOOLS_MODULE_EXIT_SUCCESS;
    }


Exit:
   if(stage != RT_TOOLS_MODULE_STAGE_INIT)
   {
       status = temporaryStatus;
   }

   return status;
}

/**
 * Module entry point for loading it.
 */
static int __init canMonDrv_init(void)
{
    rtlogRecord_t logRecord;
    int status = RT_TOOLS_MODULE_INIT_ERROR;

    if((status = canMonDrv_main(RT_TOOLS_MODULE_STAGE_INIT))
    == RT_TOOLS_MODULE_INIT_SUCCESS)
    {
        RTLOG_INFO(moduleName, "Module initialized successfully.");
    }
    else
    {
        RTLOG_ERROR(moduleName, "Failed to initialize module!");
    }

    return status;
}

/**
 * Module entry point for unloading it.
 */
static void __exit canMonDrv_exit(void)
{
    rtlogRecord_t logRecord;

    if(canMonDrv_main(RT_TOOLS_MODULE_STAGE_EXIT)
       == RT_TOOLS_MODULE_EXIT_SUCCESS)
    {
        RTLOG_INFO(moduleName, "Module cleaned up successfully.");
    }
    else
    {
        RTLOG_ERROR(moduleName, "Failed to clean up module!");
    }
}

module_init(canMonDrv_init);
module_exit(canMonDrv_exit);
