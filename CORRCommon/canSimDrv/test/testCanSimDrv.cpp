/* @(#) $Id$
 *
 * Copyright (C) 2005
 * Associated Universities, Inc. Washington DC, USA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * Correspondence concerning ALMA Software should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */

#include <stdio.h>
#include <iostream>
#include <sys/time.h>

#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestCaller.h>
#include <cppunit/ui/text/TestRunner.h>

#include <tpmc901.h>
#include <rtTools.h>
#include <rtai_sched.h>
#include <rtai_fifos.h>

#include <iostream>
using namespace std;

/** This unit test tests the functionality of the CanSimDrv kernel module. There
 ** are 2 test files used in this test, this one and a kernel module,
 ** testCanSimDrv_k. This test sends a CAN message to testCanSimDrv_k which
 ** checks the data and returns the message to the sender which checks the
 ** result. Thus we have a closed-loop test of CanSimDrv module.
 ** A caveat: Since the normal CPP_UNIT_ASSERT() macros abort, then any test
 ** failure will most likely require a computer reboot, these macros are
 ** not used and simple error output to stdout is used. The output will
 ** always display 'OK (1 tests)' even if there's an error.
 ** Also, the number of send/receive iterations must be fixed in both files,
 ** otherwise a timeout error will occur.
 **
 ** This test must be run with TAT!
 */
class CanSimDrvUnitTest : public CppUnit::TestFixture
{
public:
    /// default constructor
    CanSimDrvUnitTest()
    : CppUnit::TestFixture(){}

    /// copy constructor
    CanSimDrvUnitTest( const CanSimDrvUnitTest &toCopy)
    { *this = toCopy; }

    /// assignment operator
    CanSimDrvUnitTest &operator =( const CanSimDrvUnitTest &)
    { return *this; }

    /// destructor
    ~CanSimDrvUnitTest() {}

    void setUp();
    void tearDown();

    static CppUnit::Test *suite();

private:

    void runWriteTest();

    /// Timing routines
    struct timeval _tstart, _tend;
    struct timezone tz;
    void tstart(void)
    { gettimeofday(&_tstart, &tz); }
    void tend(void)
    { gettimeofday(&_tend,&tz); }

    double tval()
    {
        double t1, t2;
        t1 =  (double)_tstart.tv_sec + (double)_tstart.tv_usec/(1000*1000);
        t2 =  (double)_tend.tv_sec + (double)_tend.tv_usec/(1000*1000);
        return t2-t1;
    }
};

//------------------------------------------------------------------------------
void CanSimDrvUnitTest::setUp()
{
}

//------------------------------------------------------------------------------
void CanSimDrvUnitTest::tearDown()
{
}

//------------------------------------------------------------------------------
CppUnit::Test *CanSimDrvUnitTest::suite()
{
    CppUnit::TestSuite *suiteOfTests = new CppUnit::TestSuite( "CanSimDrvUnitTest" );

    suiteOfTests->addTest( new CppUnit::TestCaller<CanSimDrvUnitTest>(
                       "runWriteTest", &CanSimDrvUnitTest::runWriteTest ) );
    return suiteOfTests;
}

//------------------------------------------------------------------------------
void CanSimDrvUnitTest::runWriteTest()
{
    TP901_MSG_BUF msg, msgRead;
    // The number of times to send the test message here MUST
    // agree with the expected number of received messages in the
    // testCanSimDrv_k.c file! Otherwise a time-out error will return.
    const int numberOfTimesToSend = 10;
    char eBuf[100];

    // build a test message
    msg.identifier = 1;
    msg.timeout = 2;
    msg.rx_queue_num = 3;
    msg.extended = 4;
    msg.status = 5;
    msg.msg_len = 6;

    // initialize msg data to sequence of 1 - 8
    for (int i=0; i<8; i++)
    msg.data[i] = i+1;
    int n, retVal;
    int readFIFO_FD, readFIFO = TP_READ_FIFO;
    int writeFIFO_FD, writeFIFO = TP_WRITE_FIFO;
    char buf[20];
    int msgSize = sizeof(msg);

    // Open read buffer
    sprintf(buf,"/dev/rtf%d", readFIFO);
    readFIFO_FD = open(buf,O_WRONLY);
    if( readFIFO_FD < 0 )
    {
    sprintf(eBuf,"Error opening %s",buf);
    CPPUNIT_ASSERT_MESSAGE(eBuf,(readFIFO_FD >= 0));
    return;
    }

    // Open write buffer
    sprintf(buf,"/dev/rtf%d", writeFIFO);
    writeFIFO_FD = open(buf,O_WRONLY);
    if( writeFIFO_FD < 0 )
    {
    close(readFIFO_FD);
    sprintf(eBuf,"Error opening %s",buf);
    CPPUNIT_ASSERT_MESSAGE(eBuf,(writeFIFO_FD >= 0));
    }

    // write the test message N times. The output is in /var/log/messages
    for( int i = 0; i < numberOfTimesToSend; i++ )
    {
    // Write the test message
    retVal = rtf_write_timed(readFIFO_FD, (char *)&msg, msgSize,1000);
    if( retVal != msgSize )
    {
        close(writeFIFO_FD);
        close(readFIFO_FD);
        sprintf(eBuf,"Iteration %d write to read RT-FIFO failed",i+1);
        CPPUNIT_ASSERT_EQUAL_MESSAGE(eBuf,msgSize,retVal);
    }

    // init return data to invalid for check
    memset(msgRead.data,255,8);
    retVal = rtf_read_timed(writeFIFO_FD,(char *)&msgRead, msgSize, 1000);
    if( retVal != msgSize )
    {
        close(writeFIFO_FD);
        close(readFIFO_FD);
        sprintf(eBuf,"Iteration %d read from write RT-FIFO failed",i+1);
        CPPUNIT_ASSERT_EQUAL_MESSAGE(eBuf,msgSize,retVal);
    }
    for( int i1 = 0; i1 < 8; i1++ )
    {
        if( msg.data[i1] != msgRead.data[i1] )
        {
        close(writeFIFO_FD);
        close(readFIFO_FD);
        sprintf(eBuf,"Iteration %d read from write RT-FIFO failed",i+1);
        CPPUNIT_ASSERT_EQUAL_MESSAGE(eBuf,msg.data[i1],msgRead.data[i1]);
        }
    }

    // Change msg data on each iteration
    n = 0;
    for (int j = i; j < i+8; j++)
        msg.data[n++] = j+2;
    }
    // Make sure that the timeout mechanism works
    // Write the test message
    retVal = rtf_read_timed(writeFIFO_FD,(char *)&msgRead, msgSize, 1000);
    if( retVal > 0 )
    {
    close(writeFIFO_FD);
    close(readFIFO_FD);
    CPPUNIT_ASSERT_MESSAGE("Read time-out failed, i.e., didn't time out as expected",
                   (retVal > 0));
    }
    close(writeFIFO_FD);
    close(readFIFO_FD);
}

//------------------------------------------------------------------------------
int main( int argc, char **argv )
{
    CppUnit::TextUi::TestRunner runner;
    runner.addTest( CanSimDrvUnitTest::suite() );
    runner.run();
}
