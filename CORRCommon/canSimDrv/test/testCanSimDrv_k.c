/* @(#) $Id$
 *
 * Copyright (C) 2004
 * Associated Universities, Inc. Washington DC, USA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * Correspondence concerning ALMA Software should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <rtai_sched.h>
#include <rtai_fifos.h>
#include <rtai_sem.h>
#include <tpmc901.h>
#include <rtTools.h>
#include <rtDevDrv.h>

MODULE_AUTHOR("Jim Pisano");
MODULE_DESCRIPTION("SetupTestsCmd tester");
MODULE_LICENSE("GPL");

#define RTIME_1_MS    nano2count(1000000LL)
#define RTIME_10_MS    nano2count(10000000LL)
#define RTIME_100_MS    nano2count(100000000LL)
#define RTIME_1000_MS    nano2count(1000000000LL
#define RTIME_10_SEC    nano2count(10000000000LL)

RT_TASK testWriteFIFOTask;

int rtDevHandle = 0;

/** This unit test is for the tp_write() & tp_read() functions. It sends a
 ** NODE ID command to the correlator simulator via the tp_write() function in
 ** the tmpc901sim driver. It then waits for responses from the corr. simulator
 ** printing them to /var/log/messages.
 ** To run this test, one must use TAT!
 */

/** This function waits on the read buffer. It expects msg.Data[0] to run
 ** from 1 - 10. An error is printed if it's wrong.
 ** The same buffer is then sent to the caller on the other FIFO to check
 ** round-trip journey.
 */
//-----------------------------------------------------------------------------
static void waitForWriteFIFO(long not_used)
{
    int readValue, writeRetVal;
    TP901_MSG_BUF msg1;
    int CANChannel = 0;
    int msgCount = 0;
    int checkValue;
    // The number of times to receive the test message here MUST
    // agree with the expected number of sent messages in the
    // testCanSimDrv.cpp file!. Otherwise a time-out error will return.
    const int numberOfTimesToSend = 10;

    readValue = 0;
    // Wait for 10 msgs ignoring timeouts
    while( (msgCount < numberOfTimesToSend) && (readValue >= 0) )
    {
         readValue = rtRead(rtDevHandle, &CANChannel, (void *)&msg1,
                            sizeof(TP901_MSG_BUF), 5000000);
         if( readValue >= 0 )
         {
             msgCount++;
             writeRetVal = rtWrite(rtDevHandle, &CANChannel, (void *)&msg1,
                                   sizeof(TP901_MSG_BUF), 2000000);
             checkValue = (int)msg1.data[0];
//             rt_printk("Read msg # %d from canSimDrv rtRead msg.Data[0]: %d\n",
//                       msgCount, checkValue);
             if( checkValue != msgCount )
                 rt_printk("Error on received data: Expected: %d Actual: %d\n",
                           msgCount, checkValue);
             if( writeRetVal != sizeof(TP901_MSG_BUF) )
                 rt_printk("Error writing back message!\n");
         }
         else
           rt_printk("Timeout...\n");
    }
}

//-----------------------------------------------------------------------------
int init_module(void)
{
    rt_printk("Loading testCanSimDrv_k\n");

    rtDevHandle = rtOpen(AMBSIM_RT_DEV_DRV_NAME);
    if( rtDevHandle < 1 )
    {
        rt_printk("testCanSimDrv_k can't open canSimDrv\n");
        return -1;
    }
    // create a test task for testing out the fifos
    if( rt_task_init(&testWriteFIFOTask, waitForWriteFIFO, 0, 8000, 20, 0, 0) != 0 )
    {
    rt_printk("testCanSimDrv_k: Cannot spawn test task: waitForWriteFIFO .\n");
    return -1;
    }
    // create a test task for testing out the fifos
    rt_task_resume(&testWriteFIFOTask);

    return 0;
}

//-----------------------------------------------------------------------------
void cleanup_module(void)
{
    rt_task_delete(&testWriteFIFOTask);
    rtClose(rtDevHandle);

    rt_printk("Unloading testCanSimDrv_k\n");
}
