/*
 * Copyright (C) 2004
 * Associated Universities, Inc. Washington DC, USA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * Correspondence concerning ALMA Software should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 *
 * $Id$
 *
 */


#include <linux/moduleparam.h>
#include <linux/delay.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <asm/atomic.h>

#include <rtai_sched.h>
#include <rtai_registry.h>
#include <rtai_fifos.h>
#include <rtai_sem.h>

#include <rtLog.h>
#include <rtTools.h>

#include <tpmc901.h>
#include <rtDevDrv.h>

#define modName "canSimDrv"


MODULE_AUTHOR("Jim Pisano <jpisano@nrao.edu>");
MODULE_DESCRIPTION("CAN loop-back simulator");
MODULE_LICENSE("GPL");

static int correlatorMode=false;
module_param(correlatorMode, bool, S_IRUGO);
MODULE_PARM_DESC(correlatorMode, "set to non zero to allow multiple CAN channels (default zero)");

/**
 * This file simulates the tpm901 driver. It provides replacements for
 * tpRead(), tpWrite(), and tpIoctl() function calls which can be used by the
 * CAMBServer & the correlator simulator (corrSIM). CAMBServer can use this
 * driver replacement without any change on itspart as these 3 functions work
 * like the 'real' tmp901 drivers. This device driver uses the rtDevDrv
 * framework.
 * canSimDrvwrite() write CAN message bytes to an RT-FIFO (/dev/rtf7) which are
 * read by the corrSIM as valid can messages.
 * canSimDrvread() reads CAN responses from the corrSIM and returns them as
 * valid messages.
 * canSimDrvioctl() is currently not implemented.
 * This module requires the following RTAI kernel modules:
 * rtai_hal, rtai_lxrt (or rtai_up/smp), rtai_sem, rtai_fifos
 */

/* my local data */
/**
 * An array of message structure exists to buffer read-fifo writes from
 * extracting the messages from the read-fifo.
 */
#define NUMBER_OF_BUFFER_ITEMS    2000
/**
 * Number of fifo elements
 */
#define FIFO_ELEMENTS  1000
/**
 * to convert ns to us
 */
#define TIME_OUT_MULTIPLIER     1000LL
static RTIME IOCTL_TIME_OUT = 10000000LL;

/**
 * maximum channel number supported when running in correlatorMode, 
 */
static const int CORRELATOR_MAX_CHANNEL_NR = 7;

/**
 * the actual maximum channel number
 */
static unsigned char maxChannels = 1;

/**
 * canSimDrvread() waits on this semaphore to know when data has been received
 * by the RT_READ_FIFO
 */
static SEM tpReadSem;

/**
 * These semaphores control access to the read, write, & ioctl functions which
 * are not re-entrant and must be guarded against re-entrancy.
 */
static SEM readFcnSem, writeFcnSem, ioctlFcnSem;

/**
 * tpReadFIFOHandler writes to this global buffer. This is an array of
 * structures to buffer writes.
 */
TP901_MSG_BUF tpReadResponse[NUMBER_OF_BUFFER_ITEMS];

/**
 * canSimDrvread uses this read pointer to read the oldest response
 */
atomic_t readIndex = ATOMIC_INIT(0);
/**
 * tpReadFIFOHandler uses this write pointer to write the latest response
 */
atomic_t writeIndex = ATOMIC_INIT(0);

/**
 * Guard access to tpReadResponse buffer
 */
atomic_t tpReadRespInUse = ATOMIC_INIT(0);

static rtlogRecord_t logRecord;

/**
 * When shutting down this kernel module, we must ensure that no semaphores are
 * waiting when they get deleted otherwise it results in an kernel oops.
 */
atomic_t gShuttingDown = ATOMIC_INIT(0);

/**
 * Dummy function for the higher level interfaces (teHandler, CAMBSever).
 */
static void resetChannel(int channel __attribute__((unused)))
{
}

/**
 * Dummy function for the higher level interfaces (teHandler, CAMBSever).
 */
static int enableTE(void(*handler)(int) __attribute__((unused)))
{
    return 0;
}

/**
 * Dummy function for the higher level interfaces (teHandler, CAMBSever).
 */
static int disableTE(void)
{
    return 0;
}

EXPORT_SYMBOL(resetChannel);
EXPORT_SYMBOL(enableTE);
EXPORT_SYMBOL(disableTE);

/**
 * This RT-FIFO handler is called when a Linux process writes a TP901 message
 * to the TP_READ_FIFO. The CAN message is then copied to a global data
 * structure (managed by resources semaphores). It signals canSimDrvread() to
 * then copy the recieved data into a return structure and return.
 */
static int tpReadFIFOHandler(unsigned int fifo, int rw)
{
    TP901_MSG_BUF tmpMsgBuffer;
    int err;

    // return error if trying to read from this fifo
    if(rw == 'r')
    {
        return -EINVAL;
    }

    // wait for any commands from the user-side requester
    while((err = rtf_get(TP_READ_FIFO, &tmpMsgBuffer, sizeof(TP901_MSG_BUF)))
        == sizeof(TP901_MSG_BUF))
    {
        if(atomic_read(&tpReadRespInUse) == 0)
        {
            atomic_set(&tpReadRespInUse,1);
            // copy tmp msg to global msg buffer
            memcpy(&tpReadResponse[atomic_read(&writeIndex)], &tmpMsgBuffer,
                sizeof(TP901_MSG_BUF));

            RTGUARD(guard, 100, 0, RTGUARD_MODE_COUNTER);
            if(rtGuard_check(&guard) == 1)
                RTLOG_DEBUG(modName, "Copying a message with identifier of "
                    "0x%lx to buffer position %u",
                    tmpMsgBuffer.identifier, atomic_read(&writeIndex));

            // wrap write pointer to the beginning if we're at the end
            if(atomic_add_return(1, &writeIndex) >= NUMBER_OF_BUFFER_ITEMS)
            {
                atomic_set(&writeIndex,0);
            }

            atomic_set(&tpReadRespInUse,0);

            // signal canSimDrvread that it's okay to return the response
            rt_sem_signal(&tpReadSem);
        }
        else
        {
            RTLOG_ERROR(modName, "Concurrency error writing CAN buffer in "
                "tpReadFIFOHandler");
        }
    }

    return 0;
}

/**
 * canSimDrvread() is a blocking function which is called by CAMBserver when a
 * response is expected. canSimDrvRead() blocks on a resource semaphore which
 * is signaled once a TP901_MSG is received on the TP_READ_FIFO.
 * canSimDrvread() copies the received message to the returnMsgPtr.
 */
int canSimDrvRead(void* pChannel, void* returnMsgPtr, size_t count,
    RTIME readTimeoutUS)
{
    int semRet;
    unsigned char channel;
    TP901_MSG_BUF* msgBufPtr;
    // This variable is only used for generating a log message.
    unsigned int writeIdx;

    /*
     * If kernel module is being unloaded, don't allow clients to access the
     * semaphores
     */
    if(atomic_read(&gShuttingDown) == 1)
    {
        return -ECONNREFUSED;
    }

    /*
     * Wait a certain amount of time to attempt to access the read runction access semaphore,
     * if we can't, then return a timeout error
     */
    semRet = rt_sem_wait_timed(&readFcnSem,
        nano2count(readTimeoutUS * TIME_OUT_MULTIPLIER));
    if(semRet == SEM_TIMOUT)
    {
        // Timeout
        RTLOG_ERROR(modName, "canSimDrvRead timeout on function access "
            "semaphore");
        return -ETIME;
    }

    if(semRet == SEM_ERR)
    {
        // Some other semaphore error
        RTLOG_ERROR(modName, "canSimDrvRead error with function access "
            "semaphore");
        return -ECONNREFUSED;
    }

    if(atomic_read(&gShuttingDown) == 1)
    {
        return -ECONNREFUSED;
    }

    channel = *(unsigned char*) pChannel;

    msgBufPtr = (TP901_MSG_BUF*) returnMsgPtr;
    if (channel >= maxChannels)
    {
        RTLOG_ERROR(modName, "Unsupported CAN Channel: %d", channel);
        // Set message length to 0
        msgBufPtr->msg_len = 0;
        rt_sem_signal(&readFcnSem);
        return -ECONNREFUSED;
    }

    /*
     * now wait for the fifo handler to signal that the data have been read
     * from the fifo & copied to the global structure so we can copy the
     * response to the argument
     * Wait for data up to the # of passed-in MS
     */
    semRet = rt_sem_wait_timed(&tpReadSem,
        nano2count(readTimeoutUS * TIME_OUT_MULTIPLIER));
    if(semRet == SEM_TIMOUT)
    {
        RTLOG_ERROR(modName, "canSimDrvRead timeout on tpRead access "
            "semaphore");
        // Set message length to 0
        msgBufPtr->msg_len = 0;
        rt_sem_signal(&readFcnSem);
        return -ETIME;
    }

    // if semaphore error occurs, return 0 bytes read.
    if(semRet == SEM_ERR)
    {
        RTLOG_ERROR(modName, "canSimDrvRead failure on tpRead access "
            "semaphore");
        // Set message length to 0
        msgBufPtr->msg_len = 0;
        rt_sem_signal(&readFcnSem);
        return -1;
    }

    if(atomic_read(&gShuttingDown) == 1)
    {
        return -ECONNREFUSED;
    }

    // now copy msg buffer contents to *returnMsgPtr
    memcpy(returnMsgPtr, &tpReadResponse[atomic_read(&readIndex)],
        sizeof(TP901_MSG_BUF));
    RTGUARD(readGuard, 100, 0, RTGUARD_MODE_COUNTER);
    if(rtGuard_check(&readGuard) == 1)
        RTLOG_DEBUG(modName, "Reading a message with identifier of 0x%lx from "
            "buffer position %u",
            msgBufPtr->identifier, atomic_read(&readIndex));
    // Now log a message if too much of the buffer is being used
    writeIdx = atomic_read(&writeIndex);
    if(writeIdx <= atomic_read(&readIndex))
    {
        writeIdx += NUMBER_OF_BUFFER_ITEMS;
    }

    if(rtGuard_check(&readGuard) == 1)
    {
        RTLOG_DEBUG(modName, "This is %u slots behind where the last message "
            "was written.",
            writeIdx - atomic_read(&readIndex) - 1);
    }

    if((writeIdx - atomic_read(&readIndex) - 1) > (NUMBER_OF_BUFFER_ITEMS / 2))
    {
        if(rtGuard_check(&readGuard) == 1)
        {
            RTLOG_ERROR(modName, "Using more than half of the read-response "
                "buffer (%u positions out of %u). The simulator will not work "
                "properly if this buffer fills up.",
                writeIdx - atomic_read(&readIndex) - 1, NUMBER_OF_BUFFER_ITEMS);
        }
    }

    // wrap read pointer to the beginning if we're at the end
    if(atomic_add_return(1, &readIndex) >= NUMBER_OF_BUFFER_ITEMS)
    {
        atomic_set(&readIndex,0);
    }

    rt_sem_signal(&readFcnSem);
    return sizeof(TP901_MSG_BUF);
}

/**
 * canSimDrvWrite() simply writes valid TP901_MSG messages to the
 * TP_WRITE_FIFO. The correlator simulator reads these commands and processes
 * them accordingly.
 */
int canSimDrvWrite(void* pChannel, void* buf, size_t count,
    RTIME writeTimeoutUS)
{
    int rtfPutRetVal, semRet;
    unsigned char channel;

    /*
     * If kernel module is being unloaded, don't allow clients to access the
     * semaphores
     */
    if(atomic_read(&gShuttingDown) == 1)
    {
        return -ECONNREFUSED;
    }

    /*
     * Wait a certain amount of time to attempt to access the write function
     * access semaphore, if we can't, then return a timeout error
     */
    semRet = rt_sem_wait_timed(&writeFcnSem,
        nano2count(writeTimeoutUS * TIME_OUT_MULTIPLIER));
    if(semRet == SEM_TIMOUT)
    {
        // Timeout or other semaphore error
        RTLOG_ERROR(modName, "canSimDrvWrite timeout on function access "
            "semaphore");
        return -ETIME;
    }

    if(semRet == SEM_ERR)
    {
        // Some other semaphore error
        RTLOG_ERROR(modName, "canSimDrvWrite error with function access "
            "semaphore");
        return -ECONNREFUSED;
    }

    if(atomic_read(&gShuttingDown) == 1)
    {
        return -ECONNREFUSED;
    }

    channel = *(unsigned char*) pChannel;
    if (channel >= maxChannels)
    {
        RTLOG_ERROR(modName, "Unsupported CAN channel: %d.", channel);
        rt_sem_signal(&writeFcnSem);
        return -ECONNREFUSED;
    }
    if (correlatorMode)
    {
        ((TP901_MSG_BUF*)buf)->rx_queue_num = channel;
    }

    if(count != sizeof(TP901_MSG_BUF))
    {
        rt_sem_signal(&writeFcnSem);
        return -EINVAL;
    }

    rtfPutRetVal = rtf_put(TP_WRITE_FIFO, (char*) buf, count);
    rt_sem_signal(&writeFcnSem);
    if(rtfPutRetVal != count)
    {
        RTLOG_ERROR(modName, "canSimDrvWrite didn't write complete message to "
            "FIFO: %d bytes written", rtfPutRetVal);
        return -EINVAL;
    }

    return count;
}

/**
 * canSimDrvioctl() simply returns 0 for all requests on channel 0, else -1 for
 * all requests on any other channels (> 0). This forces a single CAN channel.
 */
int canSimDrvIoctl(int request, void* configData)
{
    int channel, semRet;

    if(atomic_read(&gShuttingDown) == 1)
    {
        return -ECONNREFUSED;
    }

    // wait 10 ms to access this function
    semRet = rt_sem_wait_timed(&ioctlFcnSem, IOCTL_TIME_OUT);
    if(semRet == SEM_TIMOUT)
    {
        // Timeout
        RTLOG_ERROR(modName, "canSimDrvIoctl timeout on function access "
            "semaphore");
        rt_sem_signal(&ioctlFcnSem);
        return -ETIME;
    }

    if(semRet == SEM_ERR)
    {
        // Some other semaphore error
        RTLOG_ERROR(modName, "canSimDrvIoctl error with function access "
            "semaphore");
        return -ECONNREFUSED;
    }

    if(atomic_read(&gShuttingDown) == 1)
    {
        return -ECONNREFUSED;
    }

    channel = ((tpmc901IOCTLStruct*) configData)->channel;
    rt_sem_signal(&ioctlFcnSem);

    // only 1 channel allowed in non correlator mode
    return (channel >= maxChannels ? -1 : 0);
}


int firstInitSection(void)
{
    int retVal;
    RTIME accessControlTimeout;

    IOCTL_TIME_OUT = nano2count(IOCTL_TIME_OUT);

    // fifo constants are defined in rtTools.h
    retVal = rtf_create(TP_WRITE_FIFO, sizeof(TP901_MSG_BUF) * FIFO_ELEMENTS);
    if(retVal != 0)
    {
        RTLOG_ERROR(modName, "Error creating canSimDrv write FIFO");
        return RT_TOOLS_MODULE_INIT_ERROR;
    }

    retVal = rtf_create(TP_READ_FIFO, sizeof(TP901_MSG_BUF) * FIFO_ELEMENTS);
    if(retVal != 0)
    {
        RTLOG_ERROR(modName, "Error creating canSimDrv read FIFO");
        return RT_TOOLS_MODULE_INIT_ERROR;
    }

    /*
     * create canSimDrvread's semaphore which it'll block on. It's a counting
     * sem as multiple fifo writes can come in before canSimDrvread() reads
     * them all
     */
    rt_typed_sem_init(&tpReadSem, 0, CNT_SEM);

    // function access semaphores ready to take
    rt_typed_sem_init(&readFcnSem, 1, RES_SEM);
    rt_typed_sem_init(&writeFcnSem, 1, RES_SEM);
    rt_typed_sem_init(&ioctlFcnSem, 1, RES_SEM);

    // Create fifo handler to receive commands from Unix process
    retVal = rtf_create_handler(TP_READ_FIFO,
        X_FIFO_HANDLER(tpReadFIFOHandler));
    if(retVal != 0)
    {
        RTLOG_ERROR(modName, "Error creating canSimDrv FIFO handler");
        return RT_TOOLS_MODULE_INIT_ERROR;
    }

    accessControlTimeout = 0LL;

    retVal = rtRegisterDriver(AMBSIM_RT_DEV_DRV_NAME, canSimDrvRead,
        canSimDrvWrite, canSimDrvIoctl, accessControlTimeout, 1);
    if(retVal < 0)
    {
        RTLOG_ERROR(modName, "rtRegisterDriver returned %d", retVal);
        return RT_TOOLS_MODULE_INIT_ERROR;
    }
    else
    {
        RTLOG_INFO(modName, "CAN simulator ready Device registration "
            "returned %d", retVal);
    }

    return RT_TOOLS_MODULE_INIT_SUCCESS;
}

//------------------------------------------------------------------------------
int fullCleanUp(void)
{
    int status = RT_TOOLS_MODULE_INIT_SUCCESS;
    int rtfDestroyCount;
    int retVal;

    // If kernel module is being unloaded, don't allow clients to access the semaphores
    atomic_set(&gShuttingDown,1);
    /**
     * Thomas J, 2007-04-20:
     * Now wait until every possible read/write/ioctl-access is finished.
     * We don't have the timeout values at hand, make an educated guess.
     * 1s seems to be more than enough to me.
     * msleep_interruptible allows this sleep to be non-busy, so that other
     * tasks can finish their businesses.
     */
    msleep_interruptible(1000U);

    /**
     * TP WRITE FIFO.
     * First reset it, then destroy it.
     */
    if(rtf_reset(TP_WRITE_FIFO) != 0)
    {
        RTLOG_ERROR(modName, "Failed to reset TP_WRITE_FIFO. Continuig with "
            "its destruction. Problems could occur now.");
    }
    // Clean up
    RTLOG_INFO(modName, "Cleaning up canSimDrv module...");
    rtfDestroyCount = rtf_destroy(TP_WRITE_FIFO);

    /**
     * TP READ FIFO.
     * First reset it, then destroy it.
     */
    if(rtf_reset(TP_READ_FIFO) != 0)
    {
        RTLOG_ERROR(modName, "Failed to reset TP_READ_FIFO. Continuig with its "
            "destruction. Problems could occur now.");
    }
    rtfDestroyCount = rtf_destroy(TP_READ_FIFO);

    /*
    RTLOG(modName, RTLOG_NORMAL,
        "Value of tpReadSem: %d", rt_sem_wait_if(&tpReadSem));
     */
    rt_sem_signal(&tpReadSem);
    if(rt_sem_delete(&tpReadSem) < 0)
    {
        RTLOG_ERROR(modName, "Unable to delete semaphore tpReadSem.");
    }
    else
    {
        RTLOG_INFO(modName, "tpReadSem succesfully deleted");
    }

    // wait .1 second before continuing
    msleep_interruptible(100);

    // function access semaphores ready to take
    /*
        RTLOG(modName, RTLOG_NORMAL,
            "Value of readFcnSem: %d", rt_sem_wait_if(&readFcnSem));
    */
    rt_sem_signal(&readFcnSem);
    if(rt_sem_delete(&readFcnSem) < 0)
    {
        RTLOG_ERROR(modName, "Unable to delete semaphore readFcnSem.");
    }
    else
    {
        RTLOG_INFO(modName, "readFcnSem succesfully deleted");
    }

    /*
    RTLOG(modName, RTLOG_NORMAL,
        "Value of writeFcnSem: %d", rt_sem_wait_if(&writeFcnSem));
    */
    rt_sem_signal(&writeFcnSem);
    if(rt_sem_delete(&writeFcnSem) < 0)
    {
        RTLOG_ERROR(modName, "Unable to delete semaphore writeFcnSem.");
    }
    else
    {
        RTLOG_INFO(modName, "writeFcnSem succesfully deleted");
    }

    /*
    RTLOG(modName, RTLOG_NORMAL,
        "Value of (&ioctlFcnSem): %d", rt_sem_wait_if((&ioctlFcnSem)));
    */
    rt_sem_signal(&ioctlFcnSem);
    if((rt_sem_delete(&ioctlFcnSem)) < 0)
    {
        RTLOG_ERROR(modName, "Unable to delete semaphore ioctlFcnSem.");
    }
    else
    {
        RTLOG_INFO(modName, "ioctlFcnSem succesfully deleted");
    }

    retVal = rtUnregisterDriver(AMBSIM_RT_DEV_DRV_NAME);
    RTLOG_INFO(modName, "Unloading CAN simulator. rtUnregisterDriver returned: "
        "%d", retVal);
    if(retVal != E_RTDD_SUCCESS)
    {
        status = RT_TOOLS_MODULE_CLEANUP_ERROR;
    }

    return status;
}

//------------------------------------------------------------------------------
int canSimDrv_main(int stage)
{
    rtlogRecord_t logRecord;

    if(stage == RT_TOOLS_MODULE_STAGE_INIT)
    {
        // log RCS ID (cvs version)
        RTLOG_INFO(modName, "$Id$");
        RTLOG_INFO(modName, "initializing canSimDrv module...");

        if(firstInitSection() == RT_TOOLS_MODULE_INIT_SUCCESS)
        {
            return RT_TOOLS_MODULE_INIT_SUCCESS;
        }
        // else on failure, just drop through & clean up
    }

    // clean up module
    return fullCleanUp();
}

//------------------------------------------------------------------------------
static int __init canSimDrv_init(void)
{
    int status;
    rtlogRecord_t logRecord;

    if((status = canSimDrv_main(RT_TOOLS_MODULE_STAGE_INIT))
        == RT_TOOLS_MODULE_INIT_SUCCESS)
    {
        RTLOG_INFO(modName, "module initialized successfully");
    }
    else
    {
        RTLOG_ERROR(modName, "failed to initialize module");
    }

    if (correlatorMode)
    {
        maxChannels = CORRELATOR_MAX_CHANNEL_NR;
    }

    return status;
}

//------------------------------------------------------------------------------
static void __exit canSimDrv_exit(void)
{
    rtlogRecord_t logRecord;

    if(canSimDrv_main(RT_TOOLS_MODULE_STAGE_EXIT)
        == RT_TOOLS_MODULE_EXIT_SUCCESS)
    {
        RTLOG_INFO(modName, "module cleaned up successfully");
    }
    else
    {
        RTLOG_ERROR(modName, "failed to clean-up module");
    }
}

module_init(canSimDrv_init);
module_exit(canSimDrv_exit);
