/*
 * File:
 *    gef_kernel_abs.c
 *
 * Description:
 *    OS specific functions used by driver files
 *
 * Copyright:
 *    Copyright 2007 GE Fanuc Embedded Systems, Inc.
 *
 * License:
 *    This file is proprietary to GE Fanuc Embedded Systems, Inc.
 *
 *
 * $Date$
 * $Rev$
 */

#include "gef_kernel_abs.h"

/* Mutex object used by both drivers */
#ifdef USE_RTOS
#include <rtai_sem.h>
#include <rtai_types.h>

SEM vown_lock;
RTIME SEMAPHORE_TIMEOUT;
#else
DECLARE_MUTEX(vown_lock);
#endif

/* Never changed within driver files */
unsigned long uni_flags = 0x00000000;
unsigned long tempe_flags = 0x00000000;

/* Spinlock functions */
void gefUnivSpinlockAcquire(){
    spin_lock_irqsave(&uni_lock, uni_flags);
}
void gefUnivSpinlockRelease(){
    spin_unlock_irqrestore(&uni_lock, uni_flags);
}
void gefTempeSpinlockAcquire(){
    spin_lock_irqsave(&tempe_lock, tempe_flags);
}
void gefTempeSpinlockRelease(){
    spin_unlock_irqrestore(&tempe_lock, tempe_flags);
}

/* Semaphore functions */
int gefSemTakeInterruptible(){
#ifdef USE_RTOS
    return rt_sem_wait_timed(&vown_lock, SEMAPHORE_TIMEOUT);

#else
    return down_interruptible(&vown_lock);
#endif
}
void gefSemTake(){
#ifdef USE_RTOS
    rt_sem_wait(&vown_lock);
#else
    down(&vown_lock);
#endif
}
void gefSemGive(){
#ifdef USE_RTOS
    rt_sem_signal(&vown_lock);
#else
    up(&vown_lock);
#endif
}
