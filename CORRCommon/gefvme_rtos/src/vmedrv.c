/*
 * File:
 *    vmedrv.c
 *
 * Description:
 *    Source code to the VME device platform switch.
 *
 * Copyright:
 *    Copyright 2007-2008 GE Fanuc Embedded Systems, Inc.
 *
 * License:
 *    This file is proprietary to GE Fanuc Embedded Systems, Inc.
 *
 *
 * $Date$
 * $Rev$
 */


/*
Modified to support Linux 2.6 x86 platforms
===============================================================================
*/

//------------------------------------------------------------------------------
//title: PCI-VME Kernel Driver
//version: 3.1
//date: February 2004
//designer:  Tom Armistead
//programmer: Tom Armistead
//platform: Linux 2.4.XX
//language:
//------------------------------------------------------------------------------  
//  Purpose:  Provides generic (non bridge specific) subroutine
//            entry points.
//            Many of these entry points do little more than call Universe
//            or Tempe specific routines and return the result.
//  Docs:                                  
//------------------------------------------------------------------------------  
#include <linux/version.h>
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,5,0)
#define MODULE
#endif

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,18)
#include <linux/config.h>
#endif

#ifdef CONFIG_MODVERSIONS
  #define MODVERSIONS

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,5,0)
  #include <linux/modversions.h>
#endif

#endif

#include <linux/module.h>
#include <linux/mm.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/proc_fs.h>
#include <linux/pci.h>
#include <linux/poll.h>
#include <linux/delay.h>

#include "gef/gefcmn_vme_framework.h"
#include "gef/gefcmn_vme_errno.h"
#include "gef/gefcmn_vme_private.h"
#include "vmedrv.h"
#include "ca91c042.h"

#ifdef USE_RTOS
#include <linux/math64.h>
#include <linux/jiffies.h>
#include <rtai_malloc.h>
#include <rtLog.h>
#endif

//----------------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------------

extern struct pci_dev *vme_pci_dev;
extern struct resource *vmepcimem;
extern spinlock_t tempe_lock;
extern spinlock_t uni_lock;
int vmeSetupLm( vmeLmCfg_t *vmeLm);
struct semaphore dma_buffer_lock;
vmeDmaHandle_t *dma_handle_list = NULL;

int vmeFreeSlaveMemory(int window);

//----------------------------------------------------------------------------
// Types
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Vars and Defines
//----------------------------------------------------------------------------
// Global debug flags
#ifdef DEBUG
GEF_UINT32 gef_vme_debug_flags = 0xFFFFFFFF;
#else
GEF_UINT32 gef_vme_debug_flags = GEF_DEBUG_ERROR;
#endif

// Global VME controller information
GEF_ENDIAN hwEndian = GEF_ENDIAN_BIG; // big endian by default
int vmechip_irq = 0;	// PCI irq
int vmechipIrqOverHeadTicks = 0;	// Interrupt overhead
int vmechip_devid = -1; // PCI devID
int vmeparent_devid = -1;	// VME bridge parent ID
int vmechip_revision = 0;	// PCI revision
int vmechip_bus = 0;	// PCI bus number.
char *vmechip_baseaddr  = 0;	// virtual address of chip registers
int vme_syscon = -1;	// VME sys controller (-1 = unknown)

extern struct resource out_resource[VME_MAX_WINDOWS];

vmeMasterWindow_t vmw[VME_MAX_WINDOWS];     /* vme master windows */
vmeSlaveWindow_t vsw[VME_MAX_WINDOWS];                     /* vme slave windows */
unsigned int out_image_va[VME_MAX_WINDOWS];    // base virtual address


#ifdef USE_RTOS
extern RT_TASK* irq_queue[];
#else
extern wait_queue_head_t irq_queue[];
#endif
extern struct resource *vmepcimem;
extern spinlock_t tempe_lock;
extern spinlock_t uni_lock;
struct semaphore master_window_lock;
struct semaphore slave_window_lock;
struct semaphore lm_window_lock;
struct semaphore vrai_window_lock;

static int locMonCount = 0;
static int vraiCount = 0;
int vown_count = 0;

/*
 * External functions 
 */
extern int uni_init(void);
extern void uni_shutdown(void);

extern int uniSetupLm( vmeLmCfg_t *vmeLm);
extern int uniDoRmw( vmeRmwCfg_t *vmeRmw);
extern int uniDoDma(vmeDmaPacket_t *vmeDma);
extern int uniBusErrorChk(int);


/*CAPI*/
extern int uniGenerateIrq(GEF_VME_DRV_VIRQ_INFO *);
extern int uniSetArbiter(GEF_VME_DRV_BUS_ARB_INFO *bus_arb_info);
extern int uniGetArbiter(GEF_VME_DRV_BUS_ARB_INFO *bus_arb_info);
extern int uniSetReleaseMode(GEF_VME_DRV_BUS_REL_MODE_INFO *bus_release_info);
extern int uniGetReleaseMode(GEF_VME_DRV_BUS_REL_MODE_INFO *bus_release_info);
extern int uniSetRequestMode(GEF_VME_DRV_BUS_REQ_MODE_INFO *bus_request_info);
extern int uniGetRequestMode(GEF_VME_DRV_BUS_REQ_MODE_INFO *bus_request_info);
extern int uniSetRequestLevel(GEF_VME_DRV_BUS_REQ_LEVEL_INFO *bus_req_level_info);
extern int uniGetRequestLevel(GEF_VME_DRV_BUS_REQ_LEVEL_INFO *bus_req_level_info);
extern int uniSetBusTimeout(GEF_VME_DRV_BUS_TIMEOUT_INFO *bus_timeout_info);
extern int uniGetBusTimeout(GEF_VME_DRV_BUS_TIMEOUT_INFO *bus_timeout_info);
extern int uniSetArbTimeout(GEF_VME_DRV_ARB_TIMEOUT_INFO *arb_timeout_info);
extern int uniGetArbTimeout(GEF_VME_DRV_ARB_TIMEOUT_INFO *arb_timeout_info);
extern int uniSetSysFail(GEF_VME_DRV_SYSFAIL_INFO *sysfail_info);
extern int uniGetSysFail(GEF_VME_DRV_SYSFAIL_INFO *sysfail_info);
extern int uniSetMaxRetry(GEF_VME_MAX_RETRY_INFO *max_retry_info);
extern int uniGetMaxRetry(GEF_VME_MAX_RETRY_INFO *max_retry_info);
extern int uniSetPostedWriteCount(GEF_VME_POST_WRITE_COUNT_INFO *pwon_info);
extern int uniGetPostedWriteCount(GEF_VME_POST_WRITE_COUNT_INFO *pwon_info);
extern int uniSysReset(void);
extern int uniAcquireBusOwnership(GEF_VME_DRV_VOWN_INFO *vown_info);
extern int uniReleaseBusOwnership(void);
extern int uniQueryBusOwnership(GEF_BOOL *vown_info);
extern int uniEnableIrq(GEF_VME_INT_INFO *int_info);
extern int uniDisableIrq(GEF_VME_INT_INFO *int_info);
extern int uniFindMasterWindow(GEF_VME_DRV_CREATE_MASTER_WINDOW_INFO *master_info, GEF_UINT32 ctl, GEF_UINT32 *offset);
extern int uniCreateMasterWindow(GEF_VME_DRV_CREATE_MASTER_WINDOW_INFO *master_info, GEF_UINT32 ctl, vmeMasterHandle_t *masterhandle);
extern int uniFillinMasterAttributes(GEF_VME_DRV_CREATE_MASTER_WINDOW_INFO *master_info, GEF_UINT32 *ctl);
extern int uniReleaseWindow(int window);
extern int uniFillinSlaveAttributes(GEF_VME_DRV_CREATE_SLAVE_WINDOW_INFO *master_info, GEF_UINT32 *ctl);
extern int uniFindSlaveWindow(GEF_VME_DRV_CREATE_SLAVE_WINDOW_INFO *slave_info, GEF_UINT32 ctl, GEF_UINT32 *offset);
extern int uniCreateSlaveWindow(GEF_VME_DRV_CREATE_SLAVE_WINDOW_INFO *slave_info, GEF_UINT32 ctl, GEF_UINT32 *window);
extern int uniReleaseSlaveWindow(int window);
extern int uniReleaseLocMonWindow(void);
extern int uniQueryLocMonWindow(GEF_VME_DRV_LOCATION_MONITOR_INFO *lmInfo);
extern int uniReleaseVraiWindow(void);
extern int uniQueryVraiWindow(GEF_VME_DRV_VRAI_INFO *vraiInfo);
extern int uniSetupVrai( GEF_VME_DRV_VRAI_INFO *vraiInfo);
extern int uniTransMasterWindow(GEF_VME_DRV_TRANSLATE_MASTER_WINDOW_INFO *transMasterInfo, vmeMasterWindow_t *masterWindow);

extern int tempe_init(void);
extern void tempe_shutdown(void);

extern int tempeSetupLm( vmeLmCfg_t *vmeLm);
extern int tempeDoRmw( vmeRmwCfg_t *vmeRmw);
extern int tempeDoDma(vmeDmaPacket_t *vmeDma);
extern int tempeBusErrorChk(int);


/*CAPI*/
extern int tempeGenerateIrq(GEF_VME_DRV_VIRQ_INFO *);
extern int tempeSetArbiter(GEF_VME_DRV_BUS_ARB_INFO *bus_arb_info);
extern int tempeGetArbiter(GEF_VME_DRV_BUS_ARB_INFO *bus_arb_info);
extern int tempeSetReleaseMode(GEF_VME_DRV_BUS_REL_MODE_INFO *bus_release_info);
extern int tempeGetReleaseMode(GEF_VME_DRV_BUS_REL_MODE_INFO *bus_release_info);
extern int tempeSetRequestMode(GEF_VME_DRV_BUS_REQ_MODE_INFO *bus_request_info);
extern int tempeGetRequestMode(GEF_VME_DRV_BUS_REQ_MODE_INFO *bus_request_info);
extern int tempeSetRequestLevel(GEF_VME_DRV_BUS_REQ_LEVEL_INFO *bus_req_level_info);
extern int tempeGetRequestLevel(GEF_VME_DRV_BUS_REQ_LEVEL_INFO *bus_req_level_info);
extern int tempeSetBusTimeout(GEF_VME_DRV_BUS_TIMEOUT_INFO *bus_timeout_info);
extern int tempeGetBusTimeout(GEF_VME_DRV_BUS_TIMEOUT_INFO *bus_timeout_info);
extern int tempeSetArbTimeout(GEF_VME_DRV_ARB_TIMEOUT_INFO *arb_timeout_info);
extern int tempeGetArbTimeout(GEF_VME_DRV_ARB_TIMEOUT_INFO *arb_timeout_info);
extern int tempeSetBroadcastId(GEF_VME_DRV_BUS_BROADCAST_INFO *broadcast_id_info);
extern int tempeGetBroadcastId(GEF_VME_DRV_BUS_BROADCAST_INFO *broadcast_id_info);
extern int tempeSetSysFail(GEF_VME_DRV_SYSFAIL_INFO *sysfail_info);
extern int tempeGetSysFail(GEF_VME_DRV_SYSFAIL_INFO *sysfail_info);
extern int tempeSetMaxRetry(GEF_VME_MAX_RETRY_INFO *max_retry_info);
extern int tempeGetMaxRetry(GEF_VME_MAX_RETRY_INFO *max_retry_info);
extern int tempeSysReset(void);
extern int tempeAcquireBusOwnership(GEF_VME_DRV_VOWN_INFO *vown_info);
extern int tempeReleaseBusOwnership(void);
extern int tempeQueryBusOwnership(GEF_BOOL *vown_info);
extern int tempeEnableIrq(GEF_VME_INT_INFO *int_info);
extern int tempeDisableIrq(GEF_VME_INT_INFO *int_info);
extern int tempeFindMasterWindow(GEF_VME_DRV_CREATE_MASTER_WINDOW_INFO *master_info, GEF_UINT32 ctl, GEF_UINT32 *offset);
extern int tempeCreateMasterWindow(GEF_VME_DRV_CREATE_MASTER_WINDOW_INFO *master_info, GEF_UINT32 ctl, vmeMasterHandle_t *masterhandle);
extern int tempeFillinMasterAttributes(GEF_VME_DRV_CREATE_MASTER_WINDOW_INFO *master_info, GEF_UINT32 *ctl);
extern int tempeReleaseWindow(int window);
extern int tempeFillinSlaveAttributes(GEF_VME_DRV_CREATE_SLAVE_WINDOW_INFO *master_info, GEF_UINT32 *ctl);
extern int tempeFindSlaveWindow(GEF_VME_DRV_CREATE_SLAVE_WINDOW_INFO *slave_info, GEF_UINT32 ctl, GEF_UINT32 *offset);
extern int tempeCreateSlaveWindow(GEF_VME_DRV_CREATE_SLAVE_WINDOW_INFO *slave_info, GEF_UINT32 ctl, GEF_UINT32 *window);
extern int tempeReleaseSlaveWindow(int window);
extern int tempeReleaseLocMonWindow(void);
extern int tempeQueryLocMonWindow(GEF_VME_DRV_LOCATION_MONITOR_INFO *lmInfo);
extern int tempeReleaseVraiWindow(void);
extern int tempeQueryVraiWindow(GEF_VME_DRV_VRAI_INFO *vraiInfo);
extern int tempeSetupVrai( GEF_VME_DRV_VRAI_INFO *vraiInfo);
extern int tempeTransMasterWindow(GEF_VME_DRV_TRANSLATE_MASTER_WINDOW_INFO *transMasterInfo, vmeMasterWindow_t *masterWindow);

extern GEF_STATUS tempeGetTimeOnBus(GEF_VME_DRV_TIME_ON_BUS_INFO *time_on_info);
extern GEF_STATUS tempeSetTimeOnBus(GEF_VME_DRV_TIME_ON_BUS_INFO *time_on_info);
extern GEF_STATUS tempeSetTimeOffBus(GEF_VME_DRV_TIME_OFF_BUS_INFO *time_off_info);
extern GEF_STATUS tempeGetTimeOffBus(GEF_VME_DRV_TIME_OFF_BUS_INFO *time_off_info);

extern GEF_UINT32 vme_irqlog[GEF_VME_INT_LEVEL_LAST][0x100];
extern GEF_UINT8  vme_irq_waitcount[];
extern GEF_UINT8  vme_vec_flags[GEF_VME_INT_LEVEL_LAST][0x100];
extern GEF_VME_BERR_INFO vme_irq_berr_info[];
extern GEF_VME_INT_INFO vme_irq_int_info[];
extern GEF_UINT8  vme_berr_write_buf;
extern GEF_UINT8  vme_berr_read_buf;
extern struct semaphore irq_mutex[GEF_VME_INT_LEVEL_LAST];
extern int comm;

#define VME_DMA_MAGIC 0x10e30001


#ifdef USE_RTOS
irqreturn_t linuxPostIRQHandler(
    int irq,
    void* devId,
    struct pt_regs* regs)
{
    rt_enable_irq(irq);

    return IRQ_HANDLED;
}
#endif


int getBridgeType(void)
{
    return vmechip_devid;
}
EXPORT_SYMBOL(getBridgeType);


//----------------------------------------------------------------------------
//  Some kernels don't supply pci_bus_mem_base_phys()... this subroutine
//  provides that functionality.
//----------------------------------------------------------------------------

#define SIMPCIMEMBASE 1
#ifdef	SIMPCIMEMBASE

unsigned int pciBusMemBasePhys=0;
unsigned int
pci_bus_mem_base_phys(int bus)
{
  return(pciBusMemBasePhys);
}

#endif

void 
printMasterWindow(int window)
{
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "**************************************\n");
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Master Window %d (0x%p):\n", vmw[window].number, &vmw[window]);
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vme address lower 0x%x\n", vmw[window].vmeL);
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vme address upper 0x%x\n", vmw[window].vmeU);
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vme size 0x%x\n", vmw[window].size);
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "virtual address 0x%p\n", vmw[window].virtAddr);
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "physical address 0x%p\n", vmw[window].physAddr);
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "**************************************\n");
}

void 
printSlaveWindow(int window)
{
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "**************************************\n");
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Slave Window %d (0x%p):\n", vsw[window].number, &vsw[window]);
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vme address lower 0x%x\n", vsw[window].vmeL);
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vme address upper 0x%x\n", vsw[window].vmeU);
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vme size 0x%x\n", vsw[window].size);
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "virtual address 0x%p\n", vsw[window].virtAddr);
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "physical address 0x%p\n", vsw[window].physAddr);
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "**************************************\n");
}

//----------------------------------------------------------------------------
//  vmeSyncData()
//    Ensure completion of all previously issued loads/stores
//----------------------------------------------------------------------------
void
vmeSyncData(void)
{
#ifdef PPC 
   asm("sync");		// PPC specific.
   asm("eieio");
#else
// On SMP systems, this resolves to mb(). On UP systems, it resolves to
//   barrier.
   smp_mb();
#endif
}

//----------------------------------------------------------------------------
//  vmeGetTime()
//    Return time stamp.
//----------------------------------------------------------------------------
void
vmeGetTime(void)
{
#ifdef PPC
  asm("mfspr 3,268");   // PPC specific.
#endif
}

//----------------------------------------------------------------------------
//  vmeFlushLine(void *ptr)
//    Flush & invalidate a cache line 
//----------------------------------------------------------------------------
void 
vmeFlushLine(void *ptr)
{
#ifdef PPC
  asm("dcbf 0,3");   // PPC specific.
  asm("sync");   // PPC specific.
#endif
}

//----------------------------------------------------------------------------
//  vmeFlushRange()
//    Flush & invalidate a range of memory
//----------------------------------------------------------------------------
void 
vmeFlushRange(char *start, char *end)
{
  while(start < end) {
    vmeFlushLine(start);
    start += LINESIZE;
  }
}

//----------------------------------------------------------------------------
//  vmeBusErrorChk()
//     Return 1 if VME bus error occured, 0 otherwise.
//     Optionally clear VME bus error status.
//----------------------------------------------------------------------------
int
vmeBusErrorChk(int clrflag)
{
  vmeSyncData();
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      return(uniBusErrorChk(clrflag));
      break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
      return(tempeBusErrorChk(clrflag));
      break;
  }
  return(0);
}

//----------------------------------------------------------------------------
//  vmeGetSlotNum
//    Determine the slot number that we are in.  Uses CR/CSR base 
//    address reg if it contains a valid value.  If not, obtains
//    slot number from board level register.
//----------------------------------------------------------------------------
int
vmeGetSlotNum(void)
{
  int slotnum = 0;

  // get slot num from VME chip reg if it is set.
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      slotnum = ioread32(vmechip_baseaddr+VCSR_BS);
#ifdef PPC
      slotnum = (slotnum >> 27) & 0x1F;
#else
      slotnum = longswap((slotnum >> 27) & 0x1F);
#endif
      break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
#ifdef PPC
      slotnum = ioread32(vmechip_baseaddr+0x23C);
      slotnum &= 0x1F;
#else
      slotnum = longswap(ioread32(vmechip_baseaddr+0x23C));
      slotnum &= 0x1F;
#endif
      return(slotnum);
      break;
  }

  // Following is board specific!!

#ifdef	MCG_MVME2600
  if(slotnum == 0)
    slotnum = ~inb(0x1006) & 0x1F;	// Genesis2 slot register
#endif
#ifdef	MCG_MVME2400
  if(slotnum == 0)
    slotnum = ~inb(0x1006) & 0x1F;	// Genesis2 slot register
#endif
#ifdef	MCG_MVME2300
  if(slotnum == 0)
    slotnum = ~inb(0x1006) & 0x1F;	// Genesis2 slot register
#endif
  
  return(slotnum);
}

/*
 * Start of ported functions
 */

/* control */
int
vmeGetArbiter(GEF_VME_DRV_BUS_ARB_INFO *bus_arb_info)
{

  // Only valid for system controller.
  if(vme_syscon == 0){
     return(GEF_STATUS_OPERATION_NOT_ALLOWED);
  } 
  memset(bus_arb_info, 0, sizeof(GEF_VME_DRV_BUS_ARB_INFO));
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      return(uniGetArbiter(bus_arb_info));
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
      return(tempeGetArbiter(bus_arb_info));
  }
  return(GEF_STATUS_NO_SUCH_DEVICE);
}

//----------------------------------------------------------------------------
//  vmeSetArbiter
//    Set VME bus arbitration mode.
//----------------------------------------------------------------------------
int
vmeSetArbiter(GEF_VME_DRV_BUS_ARB_INFO *bus_arb_info)
{
  // Only valid for system controller.
  if(vme_syscon == 0){
     return(GEF_STATUS_OPERATION_NOT_ALLOWED);
  } 
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      return(uniSetArbiter(bus_arb_info));
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
      return(tempeSetArbiter(bus_arb_info));
  }
  return(GEF_STATUS_NO_SUCH_DEVICE);
}

int 
vmeSetReleaseMode(GEF_VME_DRV_BUS_REL_MODE_INFO *bus_release_info)
{
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      return(uniSetReleaseMode(bus_release_info));
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
      return(tempeSetReleaseMode(bus_release_info));
  }
  return(GEF_STATUS_NO_SUCH_DEVICE);
}

int 
vmeGetReleaseMode(GEF_VME_DRV_BUS_REL_MODE_INFO *bus_release_info)
{
  memset(bus_release_info, 0, sizeof(GEF_VME_DRV_BUS_REL_MODE_INFO));
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      return(uniGetReleaseMode(bus_release_info));
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
      return(tempeGetReleaseMode(bus_release_info));
  }
  return(GEF_STATUS_NO_SUCH_DEVICE);
}

int 
vmeSetRequestMode(GEF_VME_DRV_BUS_REQ_MODE_INFO *bus_request_info)
{
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      return(uniSetRequestMode(bus_request_info));
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
      return(tempeSetRequestMode(bus_request_info));
  }
  return(GEF_STATUS_NO_SUCH_DEVICE);
}

int 
vmeGetRequestMode(GEF_VME_DRV_BUS_REQ_MODE_INFO *bus_request_info)
{
  memset(bus_request_info, 0, sizeof(GEF_VME_DRV_BUS_REQ_MODE_INFO));
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      return(uniGetRequestMode(bus_request_info));
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
      return(tempeGetRequestMode(bus_request_info));
  }
  return(GEF_STATUS_NO_SUCH_DEVICE);
}

int
vmeSetRequestLevel(GEF_VME_DRV_BUS_REQ_LEVEL_INFO *bus_req_level_info)
{
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      return(uniSetRequestLevel(bus_req_level_info));
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
      return(tempeSetRequestLevel(bus_req_level_info));
  }
  return(GEF_STATUS_NO_SUCH_DEVICE);
}

int
vmeGetRequestLevel(GEF_VME_DRV_BUS_REQ_LEVEL_INFO *bus_req_level_info)
{
  memset(bus_req_level_info, 0, sizeof(GEF_VME_DRV_BUS_REQ_LEVEL_INFO));
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      return(uniGetRequestLevel(bus_req_level_info));
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
      return(tempeGetRequestLevel(bus_req_level_info));
  }
  return(GEF_STATUS_NO_SUCH_DEVICE);
}

int
vmeSetBusTimeout(GEF_VME_DRV_BUS_TIMEOUT_INFO *bus_timeout_info)
{
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      return(uniSetBusTimeout(bus_timeout_info));
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
      return(tempeSetBusTimeout(bus_timeout_info));
  }
  return(GEF_STATUS_NO_SUCH_DEVICE);
}

int
vmeGetBusTimeout(GEF_VME_DRV_BUS_TIMEOUT_INFO *bus_timeout_info)
{
  memset(bus_timeout_info, 0, sizeof(GEF_VME_DRV_BUS_TIMEOUT_INFO));
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      return(uniGetBusTimeout(bus_timeout_info));
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
      return(tempeGetBusTimeout(bus_timeout_info));
  }
  return(GEF_STATUS_NO_SUCH_DEVICE);
}

int 
vmeSetArbTimeout(GEF_VME_DRV_ARB_TIMEOUT_INFO *arb_timeout_info)
{
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      return(uniSetArbTimeout(arb_timeout_info));
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
      return(tempeSetArbTimeout(arb_timeout_info));
  }
  return(GEF_STATUS_NO_SUCH_DEVICE);
}

int
vmeGetArbTimeout(GEF_VME_DRV_ARB_TIMEOUT_INFO *arb_timeout_info)
{
  memset(arb_timeout_info, 0, sizeof(GEF_VME_DRV_ARB_TIMEOUT_INFO));
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      return(uniGetArbTimeout(arb_timeout_info));
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
      return(tempeGetArbTimeout(arb_timeout_info));
  }
  return(GEF_STATUS_NO_SUCH_DEVICE);
}

int
vmeSetBroadcastId(GEF_VME_DRV_BUS_BROADCAST_INFO *broadcast_id_info)
{
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      return(GEF_STATUS_NOT_SUPPORTED);
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
      return(tempeSetBroadcastId(broadcast_id_info));
  }
  return(GEF_STATUS_NO_SUCH_DEVICE);
}

int
vmeGetBroadcastId(GEF_VME_DRV_BUS_BROADCAST_INFO *broadcast_id_info)
{
  memset(broadcast_id_info, 0, sizeof(GEF_VME_DRV_BUS_BROADCAST_INFO));
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      return(GEF_STATUS_NOT_SUPPORTED);
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
      return(tempeGetBroadcastId(broadcast_id_info));
  }
  return(GEF_STATUS_NO_SUCH_DEVICE);
}

int
vmeSetSysFail(GEF_VME_DRV_SYSFAIL_INFO *sysfail_info)
{
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      return(uniSetSysFail(sysfail_info));
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
      return(tempeSetSysFail(sysfail_info));
  }
  return(GEF_STATUS_NO_SUCH_DEVICE);
}

int
vmeGetSysFail(GEF_VME_DRV_SYSFAIL_INFO *sysfail_info)
{
  memset(sysfail_info, 0, sizeof(GEF_VME_DRV_SYSFAIL_INFO));
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      return(uniGetSysFail(sysfail_info));
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
      return(tempeGetSysFail(sysfail_info));
  }
  return(GEF_STATUS_NO_SUCH_DEVICE);
}

int
vmeSetMaxRetry(GEF_VME_MAX_RETRY_INFO *max_retry_info)
{
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      return(uniSetMaxRetry(max_retry_info));
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
      return(tempeSetMaxRetry(max_retry_info));
  }
  return(GEF_STATUS_NO_SUCH_DEVICE);
}

int
vmeGetMaxRetry(GEF_VME_MAX_RETRY_INFO *max_retry_info)
{
  memset(max_retry_info, 0, sizeof(GEF_VME_MAX_RETRY_INFO));
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      return(uniGetMaxRetry(max_retry_info));
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
      return(tempeGetMaxRetry(max_retry_info));
  }
  return(GEF_STATUS_NO_SUCH_DEVICE);
}

int vmeSetPostedWriteCount(GEF_VME_POST_WRITE_COUNT_INFO *pwon_info)
{
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      return(uniSetPostedWriteCount(pwon_info));
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
    /*
     * All writes are posted on the Tempe.  The size is not configurable.
     * This function shall	return NOT_SUPPORTED_BY_DEVICE on Tempe based 
     * SBC's.
     *
     * Page 36
     *
     * Incoming write transactions from the VMEbus are posted. With 
     * posted write transactions, data is written to a VME Slave write 
     * buffer. The VME Slave write buffer is a 4 Kbyte buffer.
     *
     * Page 37
     *
     * The VME Master has two 4 Kbyte posted write buffers and two 
     * 4 Kbyte prefetch read buffers. These buffers enable the VME 
     * Master to buffer two read or write transactions simultaneously.
     */
      return(GEF_STATUS_NOT_SUPPORTED);
  }
  return(GEF_STATUS_NO_SUCH_DEVICE);
}

int vmeGetPostedWriteCount(GEF_VME_POST_WRITE_COUNT_INFO *pwon_info)
{
  memset(pwon_info, 0, sizeof(GEF_VME_POST_WRITE_COUNT_INFO));
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      return(uniGetPostedWriteCount(pwon_info));
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
     /*
     * All writes are posted on the Tempe.  The size is not configurable.
     * This function shall	return NOT_SUPPORTED_BY_DEVICE on Tempe based 
     * SBC's.
     *
     * Page 36
     *
     * Incoming write transactions from the VMEbus are posted. With 
     * posted write transactions, data is written to a VME Slave write 
     * buffer. The VME Slave write buffer is a 4 Kbyte buffer.
     *
     * Page 37
     *
     * The VME Master has two 4 Kbyte posted write buffers and two 
     * 4 Kbyte prefetch read buffers. These buffers enable the VME 
     * Master to buffer two read or write transactions simultaneously.
     */
      return(GEF_STATUS_NOT_SUPPORTED);
  }
  return(GEF_STATUS_NO_SUCH_DEVICE);
}

int vmeSysReset(void)
{
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      return(uniSysReset());
      break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
      return(tempeSysReset());
      break;
  }
  return(GEF_STATUS_NO_SUCH_DEVICE);
}

int
vmeAcquireBusOwnership(GEF_VME_DRV_VOWN_INFO *vown_info)
{
    switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
        return(uniAcquireBusOwnership(vown_info));
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
        return(tempeAcquireBusOwnership(vown_info));
    }
    return(GEF_STATUS_NO_SUCH_DEVICE);
}

int
vmeReleaseBusOwnership(void)
{
    switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
        return(uniReleaseBusOwnership());
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
        return(tempeReleaseBusOwnership());
    }
    return(GEF_STATUS_NO_SUCH_DEVICE);
}

int
vmeQueryBusOwnership(GEF_BOOL *vown_info)
{
  memset(vown_info, 0, sizeof(GEF_BOOL));
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      return(uniQueryBusOwnership(vown_info));
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
      return(tempeQueryBusOwnership(vown_info));
  }
  return(GEF_STATUS_NO_SUCH_DEVICE);
}

// Make sure this function is called with spin locks enabled
int
vmeEnableIrq(GEF_VME_INT_INFO *int_info)
{
    switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
        return(uniEnableIrq(int_info));
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
        return(tempeEnableIrq(int_info));
    }
    return(GEF_STATUS_NO_SUCH_DEVICE);
}

// Make sure this function is called with spin locks enabled
int
vmeDisableIrq(GEF_VME_INT_INFO *int_info)
{
    switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
        return(uniDisableIrq(int_info));
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
        return(tempeDisableIrq(int_info));
    }
    return(GEF_STATUS_NO_SUCH_DEVICE);
}


void
freeMasterWindow(vmeMasterWindow_t *window)
{
    if (window->virtAddr) {
        iounmap((char*)window->virtAddr);
    }
    window->virtAddr = 0;
    
    switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
        uniReleaseWindow(window->number);
        break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
        tempeReleaseWindow(window->number);
        break;
    default:
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "freeMasterWindow: No such device id 0x%x\n", vmechip_devid);
        break;
    }
    
    if (window->physAddrRes.end - window->physAddrRes.start) {
        if (release_resource(&window->physAddrRes)) {          
        }
        window->physAddrRes.start = 0;
        window->physAddrRes.end = 0;
    }
}

void
freeSlaveWindow(vmeSlaveWindow_t *window)
{
    switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
        uniReleaseSlaveWindow(window->number);
        break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
        tempeReleaseSlaveWindow(window->number);
        break;
    default:
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "freeSlaveWindow: No such device id 0x%x\n", vmechip_devid);
        break;
    }

    vmeFreeSlaveMemory(window->number);
}

int
vmeReleaseMasterWindows(void)
{
    int i = 0;
    vmeMasterHandle_t *vmh = NULL;

    if(down_interruptible(&master_window_lock))
    {
        return (GEF_STATUS_DRIVER_ERR);
    }

    /* go through all windows and remover and free master window handles and then
       release master window
    */
    for(i=0;i<VME_MAX_WINDOWS;i++)
    {
        vmh = vmw[i].handles;
        while(vmh)
        {
            vmw[i].handles = vmh->next;

#ifdef USE_RTOS
            rt_free(vmh);
#else
            kfree(vmh);
#endif
            vmh = vmw[i].handles;
        }
        
        freeMasterWindow(&vmw[i]);
    }

    up(&master_window_lock);
    return 0;
}

int
vmeReleaseSlaveWindows(void)
{
    int i = 0;
    vmeSlaveHandle_t *vsh = NULL;
    
    if(down_interruptible(&slave_window_lock))
    {
        return (GEF_STATUS_DRIVER_ERR);
    }

    /* go through all windows and remover and free master window handles and then
       release master window
    */
    for(i=0;i<VME_MAX_WINDOWS;i++)
    {
        vsh = vsw[i].handles;
        while(vsh)
        {
            vsw[i].handles = vsh->next;
#ifdef USE_RTOS
            rt_free(vsh);
#else
            kfree(vsh);
#endif
            vsh = vsw[i].handles;
        }
        
        freeSlaveWindow(&vsw[i]);
    }

    up(&slave_window_lock);
    return 0;
}

void
removeMasterHandle(GEF_VME_MASTER_OSSPEC_HDL master_osspec_hdl, vmeMasterHandle_t *vmh)
{
    int i = 0;
    vmh = NULL;
    
    for(i=0;i<VME_MAX_WINDOWS;i++)
    {
        vmh = vmw[i].handles;
        while(vmh)
        {
            if(vmh->magic != MASTER_HANDLE_MAGIC)
            {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "removeMasterHandle: ERROR: Magic number is "
        "not correct for window %d: Expect 0x%x, Actual 0x%x",
        i, MASTER_HANDLE_MAGIC, vmh->magic);
#else
                printk("removeMasterHandle: ERROR: Magic number is not correct for window %d: Expect 0x%x, Actual 0x%x\n", i, MASTER_HANDLE_MAGIC, vmh->magic);
#endif
                return;
            }

            if(vmh == (vmeMasterHandle_t*) (GEF_OS_CAST) master_osspec_hdl)
            {   
                if (vmh->prev)	/* This is not the first node */
                    vmh->prev->next = vmh->next;
                else			/* This was the first node */
                    vmh->window->handles = vmh->next;
                
                if (vmh->next)	/* If this is not the last node */
                    vmh->next->prev = vmh->prev;
                
                if( NULL == vmh->window->handles ) {
                    freeMasterWindow(vmh->window);
                    return;
                }
                
                return;               
            }
            else
            {
                vmh = vmh->next;
            }
        }
    }
}

void
removeSlaveHandle(GEF_VME_SLAVE_OSSPEC_HDL slave_osspec_hdl, vmeSlaveHandle_t *vsh)
{
    int i = 0;
    vsh = NULL;
    
    for(i=0;i<VME_MAX_WINDOWS;i++)
    {
        vsh = vsw[i].handles;
        while(vsh)
        {
            if(vsh->magic != SLAVE_HANDLE_MAGIC)
            {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_WARNING("gefvme_rtos", "removeSlaveHandle: WARNING: Magic number is "
        "not correct for window %d: Expect 0x%x, Actual 0x%x",
        i, SLAVE_HANDLE_MAGIC, vsh->magic);
#else
                printk("removeSlaveHandle: WARNING: Magic number is not correct for window %d: Expect 0x%x, Actual 0x%x\n", i, SLAVE_HANDLE_MAGIC, vsh->magic);
#endif
                return;
            }

            if(vsh == (vmeSlaveHandle_t*) (GEF_OS_CAST) slave_osspec_hdl)
            {   
                if (vsh->prev)	/* This is not the first node */
                    vsh->prev->next = vsh->next;
                else			/* This was the first node */
                    vsh->window->handles = vsh->next;
                
                if (vsh->next)	/* If this is not the last node */
                    vsh->next->prev = vsh->prev;
                
                if( NULL == vsh->window->handles ) {
                    freeSlaveWindow(vsh->window);
                    return;
                }
                
                return;               
            }
            else
            {
                vsh = vsh->next;
            }
        }
    }
}

vmeMasterWindow_t *
getWindowFromHandle(GEF_VME_MASTER_OSSPEC_HDL master_osspec_hdl)
{
    int i = 0;
    vmeMasterHandle_t *vmh = NULL;

    for(i=0;i<VME_MAX_WINDOWS;i++)
    {
        vmh = vmw[i].handles;
        while(vmh)
        {
            if(vmh->magic != MASTER_HANDLE_MAGIC)
            {
                GEF_VME_DEBUG(GEF_DEBUG_ERROR, "getWindowFromHandle: ERROR: Magic number is not correct for window %d: Expect 0x%x, Actual 0x%x\n", i, MASTER_HANDLE_MAGIC, vmh->magic);
                return(NULL);
            }

            if(vmh == (vmeMasterHandle_t*) (GEF_OS_CAST) master_osspec_hdl)
            {
                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "getWindowFromHandle: Found window %d (vmh 0x%p)\n", 
                    i, vmh);
                return(&vmw[i]);
            }
            vmh = vmh->next;
        }
    }

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "getWindowFromHandle: No window found for master_osspec_hdl %lx\n", (long unsigned int) master_osspec_hdl);
    return(NULL);
}

void*
getWindowPointerFromHandle(GEF_VME_MASTER_OSSPEC_HDL master_osspec_hdl)
{
    vmeMasterWindow_t *masterWindow = getWindowFromHandle(master_osspec_hdl);

    if(NULL == masterWindow)
    {
        return(NULL);
    }
    else
    {
        return(masterWindow->virtAddr);
    }
}
EXPORT_SYMBOL(getWindowPointerFromHandle);

void*
getWindowPhysPointerFromHandle(GEF_VME_MASTER_OSSPEC_HDL master_osspec_hdl)
{
    vmeMasterWindow_t *masterWindow = getWindowFromHandle(master_osspec_hdl);

    if(NULL == masterWindow)
    {
        return(NULL);
    }
    else
    {
        return(masterWindow->physAddr);
    }
}

void*
getSlaveWindowPhysPointerFromHandle(GEF_VME_SLAVE_OSSPEC_HDL slave_osspec_hdl)
{
    int i = 0;
    vmeSlaveHandle_t *vsh = NULL;
    void *p = 0;

    for(i=0;i<VME_MAX_WINDOWS;i++)
    {
        vsh = vsw[i].handles;
        while(vsh)
        {
            if(vsh->magic != SLAVE_HANDLE_MAGIC)
            {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_WARNING("gefvme_rtos", "getSlaveWindowPhysPointerFromHandle: "
        "WARNING: Magic number is not correct for window %d: Expect 0x%x, "
        "Actual 0x%x",
        i, SLAVE_HANDLE_MAGIC, vsh->magic);
#else
                printk("getSlaveWindowPhysPointerFromHandle: WARNING: Magic number is not correct for window %d: Expect 0x%x, Actual 0x%x\n", i, SLAVE_HANDLE_MAGIC, vsh->magic);
#endif
            }

            if(vsh == (vmeSlaveHandle_t*) (GEF_OS_CAST) slave_osspec_hdl)
            {
                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "getSlaveWindowPhysPointerFromHandle: Found window %d with physical address 0x%x (slave_osspec_hdl %lx)\n", 
                    i, (unsigned int)vsw[i].physAddr, (long unsigned int) slave_osspec_hdl);
                p = vsw[i].physAddr;
                return (GEF_UINT32*)p;
            }
            vsh = vsh->next;
        }
    }
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "getSlaveWindowPhysPointerFromHandle: No window found for slave_osspec_hdl %lx\n", (long unsigned int) slave_osspec_hdl);
    return NULL;
}

void
insertMasterHandle(vmeMasterWindow_t *window, vmeMasterHandle_t *vmh)
{
    vmh->window = window;            /* window associated to this handle*/
    vmh->next = window->handles;     /* current handle is now next handle */
    vmh->prev = NULL;                /* this handle has no previous since it is the head of the list */
    if(vmh->next)
    {
        vmh->next->prev = vmh;     /* next handle now has this handle as previous */
    }
    window->handles = vmh;           /* set this handle to current handle */

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeInsertMasterHandle: Insert window handle (master_osspec_hdl) %p into window %d (%p:0x%x:0x%x)\n", vmh, window->number, window, window->vmeL, window->size);
}

void
insertSlaveHandle(vmeSlaveWindow_t *window, vmeSlaveHandle_t *vsh)
{
    vsh->window = window;               /* window associated to this handle */
    vsh->next = window->handles;        /* current handle is now next handle */
    vsh->prev = NULL;                   /* this handle has no previous since it is the head of the list */
    if(vsh->next)
    {
        vsh->next->prev = vsh;          /* next handle now has this handle as previous */
    }
    window->handles = vsh;              /* set this handle to current handle */

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "insertSlaveHandle: Insert slave_osspec_hdl %p into window %d\n", vsh, window->number);
}

vmeMasterHandle_t *
vmeCreateMasterHandle(GEF_VME_DRV_CREATE_MASTER_WINDOW_INFO *master_info, int window)
{
    vmeMasterHandle_t *masterhandle = NULL;

    if(down_interruptible(&master_window_lock))
    {
        return(NULL);
    }

#ifdef USE_RTOS
    masterhandle = (vmeMasterHandle_t *)rt_malloc(sizeof(vmeMasterHandle_t));
#else
    masterhandle = (vmeMasterHandle_t *)kmalloc(sizeof (vmeMasterHandle_t), GFP_KERNEL);
#endif

    if(masterhandle == NULL)
    {
        return(NULL);
    }
    masterhandle->magic = MASTER_HANDLE_MAGIC;

    master_info->master_osspec_hdl = (GEF_UINT64) (GEF_OS_CAST) masterhandle; // used by the user app to identify this window
    if(window != -1)
    {
        insertMasterHandle(&vmw[window], masterhandle);
    }
    up(&master_window_lock);

    return(masterhandle);
}

int
vmeCreateSlaveHandle(GEF_VME_DRV_CREATE_SLAVE_WINDOW_INFO *slave_info, int window, GEF_UINT32 offset)
{
    vmeSlaveHandle_t *slavehandle = NULL;

    if(down_interruptible(&slave_window_lock))
    {
        return (GEF_STATUS_DRIVER_ERR);
    }

#ifdef USE_RTOS
    slavehandle = (vmeSlaveHandle_t *)rt_malloc(sizeof(vmeSlaveHandle_t));
#else
    slavehandle = (vmeSlaveHandle_t *)kmalloc(sizeof (vmeSlaveHandle_t), GFP_KERNEL);
#endif
    if(slavehandle == NULL)
    {
        up(&slave_window_lock);
        return (GEF_STATUS_NO_MEM);
    }
    slavehandle->magic = SLAVE_HANDLE_MAGIC;
    slavehandle->offset = offset;

    slave_info->slave_osspec_hdl = (GEF_UINT64) (GEF_OS_CAST) slavehandle; // used by the user app to identify this window
    insertSlaveHandle(&vsw[window], slavehandle);
    up(&slave_window_lock);

    return (GEF_SUCCESS);
}

int
findMasterWindow(GEF_VME_DRV_CREATE_MASTER_WINDOW_INFO *master_info, GEF_UINT32 ctl, GEF_UINT32 *window)
{
    vmeMasterHandle_t *masterhandle = NULL;
    GEF_UINT32 offset = 0;

    *window = -1;
        
    switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
        *window = uniFindMasterWindow(master_info, ctl, &offset);
        break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
        *window = tempeFindMasterWindow(master_info, ctl, &offset);
        break;
    default: 
        return(GEF_STATUS_NO_SUCH_DEVICE);
    }

    if(*window != -1)
    {
        masterhandle = vmeCreateMasterHandle(master_info, *window);
        if(NULL == masterhandle)
        {
            return (GEF_STATUS_NO_MEM);
        }

        masterhandle->offset = offset;
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "findMasterWindow: Found window %d with offset 0x%x\n", *window, offset);
    }   

    return (GEF_SUCCESS);
}

int
findSlaveWindow(GEF_VME_DRV_CREATE_SLAVE_WINDOW_INFO *slave_info, GEF_UINT32 ctl, GEF_UINT32 *window)
{
    int status = GEF_SUCCESS;
    GEF_UINT32 offset = 0;

    *window = -1;
        
    switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
        *window = uniFindSlaveWindow(slave_info, ctl, &offset);
        break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
        *window = tempeFindSlaveWindow(slave_info, ctl, &offset);
        break;
    default: 
        return(GEF_STATUS_NO_SUCH_DEVICE);
    }

    if(*window != -1)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "findSlaveWindow: Found window %d with offset 0x%x\n", *window, offset);
        status = vmeCreateSlaveHandle(slave_info, *window, offset);
    }   

    return (GEF_SUCCESS);
}

int
vmeCreateMasterWindow(GEF_VME_DRV_CREATE_MASTER_WINDOW_INFO *master_info)
{
    GEF_UINT32 window = -1;
    GEF_UINT32 ctl = 0;
    GEF_STATUS status = GEF_SUCCESS;
    vmeMasterHandle_t *masterhandle = NULL;

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeCreateMasterWindow: Entry\n");

    if(vmepcimem == 0)
    {
        GEF_VME_DEBUG(GEF_DEBUG_ERROR, "vmeCreateMasterWindow: ERROR: vmepcimem invalid\n");
        return(GEF_STATUS_NO_MEM);
    }
    if(master_info->size == 0)
    {
        GEF_VME_DEBUG(GEF_DEBUG_ERROR, "vmeCreateMasterWindow: ERROR: master_info->size is zero\n");
        return(GEF_STATUS_VME_INVALID_SIZE);
    }

    /* fill in address space and mode */
    switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
        status = uniFillinMasterAttributes(master_info, &ctl);
        if(status != GEF_SUCCESS)
        {
            return (status);
        }
        break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
        status = tempeFillinMasterAttributes(master_info, &ctl);
        if(status != GEF_SUCCESS)
        {
            return (status);
        }
        break;
    default: 
        return(GEF_STATUS_NO_SUCH_DEVICE);
    }

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeCreateMasterWindow: Master control is 0x%x\n", ctl);

    status = findMasterWindow(master_info, ctl, &window);
    if(status != GEF_SUCCESS)
    {
        printMasterWindow(window);
        return (status);
    }

    if(window == -1)
    {
        /* the window does not exist so we will create it */

        masterhandle = vmeCreateMasterHandle(master_info, window);
        if(masterhandle)
        {
            switch(vmechip_devid) {
            case PCI_DEVICE_ID_TUNDRA_CA91C042:
                {
                    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeCreateMasterWindow: Exit: PCI_DEVICE_ID_TUNDRA_CA91C042 device\n");
                    if(down_interruptible(&master_window_lock))
                    {
                        return(GEF_STATUS_DRIVER_ERR);
                    }
                    status = uniCreateMasterWindow(master_info, ctl, masterhandle);
                    if(status == GEF_SUCCESS)
                    {
                        insertMasterHandle(masterhandle->window, masterhandle);
                    }
                    else
                    {
#ifdef USE_RTOS
                        rt_free(masterhandle);
#else
                        kfree(masterhandle);
#endif
                    }
                    up(&master_window_lock);
                    return(status);
                }
            case PCI_DEVICE_ID_TUNDRA_TEMPE:
                {
                    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeCreateMasterWindow: Exit: PCI_DEVICE_ID_TUNDRA_TEMPE device\n");
                    if(down_interruptible(&master_window_lock))
                    {
                        return(GEF_STATUS_DRIVER_ERR);
                    }
                    status = tempeCreateMasterWindow(master_info, ctl, masterhandle);
                    if(status == GEF_SUCCESS)
                    {
                        insertMasterHandle(masterhandle->window, masterhandle);
                    }
                    else
                    {
#ifdef USE_RTOS
                        rt_free(masterhandle);
#else
                        kfree(masterhandle);
#endif
                    }
                    up(&master_window_lock);
                    return(status);
                }
            }
            GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeCreateMasterWindow: Exit: no such device 0x%x\n", vmechip_devid);
            return(GEF_STATUS_NO_SUCH_DEVICE);
        }
        else
        {
            return(GEF_STATUS_NO_MEM);
        }
    }

    /* the window exist */
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeCreateMasterWindow: Found window with pci memory, start %x and end %x\n", 
        (unsigned int)vmw[window].physAddrRes.start, (unsigned int)vmw[window].physAddrRes.end);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeCreateMasterWindow: Exit\n");

    return (GEF_SUCCESS);
}


int
vmeCreateSlaveWindow(GEF_VME_DRV_CREATE_SLAVE_WINDOW_INFO *slave_info)
{
    GEF_UINT32 window = -1;
    GEF_UINT32 ctl = 0;
    GEF_STATUS status = GEF_SUCCESS;
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeCreateSlaveWindow: Entry\n");
    
    if(vmepcimem == 0)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeCreateSlaveWindow: ERROR: vmepcimem invalid\n");
        return(GEF_STATUS_NO_MEM);
    }
    
    if(slave_info->size == 0)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeCreateSlaveWindow: ERROR: slave_info->size invalid\n");
        return(GEF_STATUS_VME_INVALID_SIZE);
    }
    
    /* fill in address space and mode */
    switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
        status = uniFillinSlaveAttributes(slave_info, &ctl);
        if(status != GEF_SUCCESS)
        {
            return (status);
        }
        break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
        status = tempeFillinSlaveAttributes(slave_info, &ctl);
        if(status != GEF_SUCCESS)
        {
            return (status);
        }
        break;
    default: 
        return(GEF_STATUS_NO_SUCH_DEVICE);
    }
    
    status = findSlaveWindow(slave_info, ctl, &window);
    if(status != GEF_SUCCESS)
    {
        return (status);
    }
    
    if(window == -1)
    {
        /* the window does not exist so we will create it */
        
        switch(vmechip_devid) {
        case PCI_DEVICE_ID_TUNDRA_CA91C042:
            {
                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeCreateSlaveWindow: Exit: PCI_DEVICE_ID_TUNDRA_CA91C042 device\n");
                status = uniCreateSlaveWindow(slave_info, ctl, &window);
                if(status == GEF_SUCCESS)
                {
                    status = vmeCreateSlaveHandle(slave_info, window, 0);
                }
                return(status);
            }
        case PCI_DEVICE_ID_TUNDRA_TEMPE:
            {
                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeCreateSlaveWindow: Exit: PCI_DEVICE_ID_TUNDRA_TEMPE device\n");
                status = tempeCreateSlaveWindow(slave_info, ctl, &window);
                if(status == GEF_SUCCESS)
                {
                    status = vmeCreateSlaveHandle(slave_info, window, 0);
                }
                return(status);
            }
        }
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeCreateSlaveWindow: Exit: no such device 0x%x\n", vmechip_devid);
        return(GEF_STATUS_NO_SUCH_DEVICE);
    }
    
    /* the window exist */
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeCreateSlaveWindow: Found window %d with physical address 0x%x and virtual address 0x%x\n", 
        vsw[window].number, (unsigned int)vsw[window].physAddr, (unsigned int)vsw[window].virtAddr);
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeCreateSlaveWindow: Exit\n");
    
    return (GEF_SUCCESS);
}

int
vmeReleaseMasterWindow(GEF_VME_DRV_RELEASE_MASTER_WINDOW_HDL_INFO *rel_master_info)
{
    vmeMasterHandle_t *vmh = NULL;

    if(down_interruptible(&master_window_lock))
    {
        return (GEF_STATUS_DRIVER_ERR);
    }
    removeMasterHandle(rel_master_info->master_osspec_hdl, vmh);
    up(&master_window_lock);
#ifdef USE_RTOS
    rt_free(vmh);
#else
    kfree(vmh);
#endif

    return (GEF_SUCCESS);
}


int
vmeRemoveSlaveWindow(GEF_VME_DRV_RELEASE_SLAVE_WINDOW_HDL_INFO *rel_slave_info)
{
    vmeSlaveHandle_t *vsh = NULL;

    if(down_interruptible(&slave_window_lock))
    {
        return (GEF_STATUS_DRIVER_ERR);
    }
    removeSlaveHandle(rel_slave_info->slave_osspec_hdl, vsh);
    up(&slave_window_lock);
#ifdef USE_RTOS
    rt_free(vsh);
#else
    kfree(vsh);
#endif

    return (GEF_SUCCESS);
}

/* master window read routines */

int
vmeRead(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT8 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements)
{
    GEF_UINT8 *pva = 0;
    GEF_UINT8 *pb = buffer;
    vmeMasterHandle_t* wind_handle = (vmeMasterHandle_t*) (GEF_OS_CAST)handle;

    if( wind_handle->magic != MASTER_HANDLE_MAGIC )
    {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeRead: ERROR: Invalid window handle memory");
#else
        printk("vmeRead: ERROR: Invalid window handle memory\n");
#endif
        return (GEF_STATUS_DRIVER_ERR);
    }

    if( offset > wind_handle->window->size )
    {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeRead: ERROR: Offset 0x%x exceeds window "
        "%d size 0x%x",
        offset, wind_handle->window->number, wind_handle->window->size);
#else
        printk("vmeRead: ERROR: Offset 0x%x exceeds window %d size 0x%x\n", offset, wind_handle->window->number, wind_handle->window->size);
#endif
        return (GEF_STATUS_READ_ERR);
    }

    if( (offset+num_elements) > wind_handle->window->size )
    {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeRead: ERROR: Offset 0x%x and num_elements "
        "0x%x exceed window %d size 0x%x",
        offset, num_elements, wind_handle->window->number,
        wind_handle->window->size);
#else
        printk("vmeRead: ERROR: Offset 0x%x and num_elements 0x%x exceed window %d size 0x%x\n", offset, num_elements, wind_handle->window->number, wind_handle->window->size);
#endif
        return (GEF_STATUS_READ_ERR);
    }

    pva = (GEF_UINT8*)((GEF_UINT32)wind_handle->window->virtAddr + wind_handle->offset + offset);

    if(pva != 0)
    {
        memcpy(pb, pva, num_elements);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeRead: Memcpy from virtual address %p to buffer %p of num_elements 0x%x for user\n", pva, pb, num_elements);
        
    }
    else
    {
        GEF_VME_DEBUG(GEF_DEBUG_ERROR, "vmeRead: ERROR: Master window 0x%X has invalid virtual address (NULL)\n",wind_handle->window->number);
        return (GEF_STATUS_DRIVER_ERR);
    }

    return (GEF_SUCCESS);
}

int
vmeRead8(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT8 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements)
{
    GEF_UINT8 *pva = 0;
    GEF_UINT8 *pb = buffer;
    int i = 0;
    vmeMasterHandle_t* wind_handle = (vmeMasterHandle_t*) (GEF_OS_CAST)handle;

    if( wind_handle->magic != MASTER_HANDLE_MAGIC )
    {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeRead8: ERROR: Invalid window handle memory");
#else
        printk("vmeRead8: ERROR: Invalid window handle memory\n");
#endif
        return (GEF_STATUS_DRIVER_ERR);
    }

    if( offset > wind_handle->window->size )
    {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeRead8: ERROR: Offset 0x%x exceeds window "
        "%d size 0x%x",
        offset, wind_handle->window->number, wind_handle->window->size);
#else
        printk("vmeRead8: ERROR: Offset 0x%x exceeds window %d size 0x%x\n", offset, wind_handle->window->number, wind_handle->window->size);
#endif
        return (GEF_STATUS_READ_ERR);
    }

    if( (offset+num_elements) > wind_handle->window->size )
    {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeRead8: ERROR: Offset 0x%x and num_elements "
        "0x%x exceed window %d size 0x%x",
        offset, num_elements, wind_handle->window->number,
        wind_handle->window->size);
#else
        printk("vmeRead8: ERROR: Offset 0x%x and num_elements 0x%x exceed window %d size 0x%x\n", offset, num_elements, wind_handle->window->number, wind_handle->window->size);
#endif
        return (GEF_STATUS_READ_ERR);
    }

    pva = (GEF_UINT8*)((GEF_UINT32)wind_handle->window->virtAddr + wind_handle->offset + offset);

    if(pva != 0)
    {
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeRead8: Copy from virtual address %p to buffer %p of num_elements 0x%x for user\n", pva, pb, num_elements);
        for(i=0;i<num_elements;i++)
        {
            *pb++ = ioread8(pva++);
        }
    }
    else
    {
    GEF_VME_DEBUG(GEF_DEBUG_ERROR, "vmeRead8: ERROR: Master window 0x%X has invalid virtual address (NULL)\n",wind_handle->window->number);
            return (GEF_STATUS_DRIVER_ERR);
    }

    return (GEF_SUCCESS);
}

int
vmeRead16(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT16 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements)
{
    GEF_UINT16 *pva = 0;
    GEF_INT16 va = 0;
    GEF_UINT16 *pb = buffer;
    int i = 0;
    vmeMasterHandle_t* wind_handle = (vmeMasterHandle_t*) (GEF_OS_CAST)handle;

    if( wind_handle->magic != MASTER_HANDLE_MAGIC )
    {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeRead16: ERROR: Invalid window handle "
        "memory");
#else
        printk("vmeRead16: ERROR: Invalid window handle memory\n");
#endif
        return (GEF_STATUS_DRIVER_ERR);
    }

    if( offset > wind_handle->window->size )
    {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeRead16: ERROR: Offset 0x%x exceeds window "
        "%d size 0x%x",
        offset, wind_handle->window->number, wind_handle->window->size);
#else
        printk("vmeRead16: ERROR: Offset 0x%x exceeds window %d size 0x%x\n", offset, wind_handle->window->number, wind_handle->window->size);
#endif
        return (GEF_STATUS_READ_ERR);
    }

    if( (offset+(num_elements*2)) > wind_handle->window->size )
    {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeRead16: ERROR: Offset 0x%x and num_elements "
        "0x%x exceed window %d size 0x%x",
        offset, num_elements, wind_handle->window->number,
        wind_handle->window->size);
#else
        printk("vmeRead16: ERROR: Offset 0x%x and num_elements 0x%x exceed window %d size 0x%x\n", offset, num_elements, wind_handle->window->number, wind_handle->window->size);
#endif
        return (GEF_STATUS_READ_ERR);
    }

    pva = (GEF_UINT16*)((GEF_UINT32)wind_handle->window->virtAddr + wind_handle->offset + offset);

    if(pva != 0)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "hwEndian=0x%x, wind_handle->window->swEndian=0x%x\n", hwEndian, wind_handle->window->swEndian);
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeRead16: Copy from virtual address %p to buffer %p of num_elements 0x%x for user\n", pva, pb, num_elements);
        for(i=0;i<num_elements;i++)
        {
            if(hwEndian != wind_handle->window->swEndian)
            {
                va = ioread16(pva++);
                *pb++ = SWAP16(va);
            }
            else
            {
                *pb++ = ioread16(pva++);
            }
        }
    }
    else
    {
    GEF_VME_DEBUG(GEF_DEBUG_ERROR, "vmeRead16: ERROR: Master window 0x%X has invalid virtual address (NULL)\n",wind_handle->window->number);
            return (GEF_STATUS_DRIVER_ERR);
    }

    return (GEF_SUCCESS);
}

int
vmeRead32(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT32 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements)
{
    GEF_UINT32 *pva = 0;
    GEF_INT32 va = 0;
    GEF_UINT32 *pb = buffer;
    int i = 0;
    vmeMasterHandle_t* wind_handle = (vmeMasterHandle_t*) (GEF_OS_CAST)handle;

    if( wind_handle->magic != MASTER_HANDLE_MAGIC )
    {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeRead32: ERROR: Invalid window handle "
        "memory");
#else
        printk("vmeRead32: ERROR: Invalid window handle memory\n");
#endif
        return (GEF_STATUS_DRIVER_ERR);
    }

    if( offset > wind_handle->window->size )
    {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeRead32: ERROR: Offset 0x%x exceeds window "
        "%d size 0x%x",
        offset, wind_handle->window->number, wind_handle->window->size);
#else
        printk("vmeRead32: ERROR: Offset 0x%x exceeds window %d size 0x%x\n", offset, wind_handle->window->number, wind_handle->window->size);
#endif
        return (GEF_STATUS_READ_ERR);
    }

    if( (offset+(num_elements*4)) > wind_handle->window->size )
    {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeRead32: ERROR: Offset 0x%x and num_elements "
        "0x%x exceed window %d size 0x%x",
        offset, num_elements, wind_handle->window->number,
        wind_handle->window->size);
#else
        printk("vmeRead32: ERROR: Offset 0x%x and num_elements 0x%x exceed window %d size 0x%x\n", offset, num_elements, wind_handle->window->number, wind_handle->window->size);
#endif
        return (GEF_STATUS_READ_ERR);
    }

    pva = (GEF_UINT32*)((GEF_UINT32)wind_handle->window->virtAddr + wind_handle->offset + offset);

    if(pva != 0)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "hwEndian=0x%x, wind_handle->window->swEndian=0x%x\n", hwEndian, wind_handle->window->swEndian);
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeRead32: Copy from virtual address %p to buffer %p of num_elements 0x%x for user\n", pva, pb, num_elements);
        for(i=0;i<num_elements;i++)
        {
            if(hwEndian != wind_handle->window->swEndian)
            {
                va = ioread32(pva++);
                *pb++ = SWAP32(va);
            }
            else
            {
                *pb++ = ioread32(pva++);
            }
        }
    }
    else
    {
            GEF_VME_DEBUG(GEF_DEBUG_ERROR, "vmeRead32: ERROR: Master window 0x%X has invalid virtual address (NULL)\n",wind_handle->window->number);
            return (GEF_STATUS_DRIVER_ERR);
    }

    return (GEF_SUCCESS);
}

int
vmeRead64(GEF_VME_DRV_READ_WRITE_INFO *read_info)
{
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeRead64: NOT IMPLEMENTED");
#else
    printk("vmeRead64: NOT IMPLEMENTED\n");
#endif
    
    return (GEF_STATUS_NOT_SUPPORTED);
}

/* master window write routines */

int
vmeWrite(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT8 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements)
{
    GEF_UINT8 *pva = 0;
    GEF_UINT8 *pb = buffer;
    vmeMasterHandle_t* wind_handle = (vmeMasterHandle_t*) (GEF_OS_CAST)handle;

    if( wind_handle->magic != MASTER_HANDLE_MAGIC )
    {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeWrite: ERROR: Invalid window handle memory");
#else
        printk("vmeWrite: ERROR: Invalid window handle memory\n");
#endif
        return (GEF_STATUS_DRIVER_ERR);
    }

    if( offset > wind_handle->window->size )
    {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeWrite: ERROR: Offset 0x%x exceeds window "
        "%d size 0x%x",
        offset, wind_handle->window->number, wind_handle->window->size);
#else
        printk("vmeWrite: ERROR: Offset 0x%x exceeds window %d size 0x%x\n", offset, wind_handle->window->number, wind_handle->window->size);
#endif
        return (GEF_STATUS_WRITE_ERR);
    }

    if( (offset+num_elements) > wind_handle->window->size )
    {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeWrite: ERROR: Offset 0x%x and num_elements "
        "0x%x exceed window %d size 0x%x",
        offset, num_elements, wind_handle->window->number,
        wind_handle->window->size);
#else
        printk("vmeWrite: ERROR: Offset 0x%x and num_elements 0x%x exceed window %d size 0x%x\n", offset, num_elements, wind_handle->window->number, wind_handle->window->size);
#endif
        return (GEF_STATUS_WRITE_ERR);
    }

    pva = (GEF_UINT8*)((GEF_UINT32)wind_handle->window->virtAddr + wind_handle->offset + offset);

    if(pva != 0)
    {
        memcpy(pva, pb, num_elements);
        
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeWrite: Memcpy from buffer %p to virtual address %p of num_elements 0x%x for user\n", pva, pb, num_elements);
    }
    else
    {
        GEF_VME_DEBUG(GEF_DEBUG_ERROR, "vmeWrite: ERROR: Master window 0x%X has invalid virtual address (NULL)\n", wind_handle->window->number);
        return (GEF_STATUS_DRIVER_ERR);
    }

    return (GEF_SUCCESS);
}

int
vmeWrite8(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT8 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements)
{
    GEF_UINT8 *pva = 0;
    GEF_UINT8 *pb = buffer;
    int i = 0;
    vmeMasterHandle_t* wind_handle = (vmeMasterHandle_t*) (GEF_OS_CAST)handle;

    if( wind_handle->magic != MASTER_HANDLE_MAGIC )
    {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeWrite8: ERROR: Invalid window handle "
        "memory");
#else
        printk("vmeWrite8: ERROR: Invalid window handle memory\n");
#endif
        return (GEF_STATUS_DRIVER_ERR);
    }

    if( offset > wind_handle->window->size )
    {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeWrite8: ERROR: Offset 0x%x exceeds window "
        "%d size 0x%x",
        offset, wind_handle->window->number, wind_handle->window->size);
#else
        printk("vmeWrite8: ERROR: Offset 0x%x exceeds window %d size 0x%x\n", offset, wind_handle->window->number, wind_handle->window->size);
#endif
        return (GEF_STATUS_WRITE_ERR);
    }

    if( (offset+num_elements) > wind_handle->window->size )
    {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeWrite8: ERROR: Offset 0x%x and num_elements "
        "0x%x exceed window %d size 0x%x",
        offset, num_elements, wind_handle->window->number,
        wind_handle->window->size);
#else
        printk("vmeWrite8: ERROR: Offset 0x%x and num_elements 0x%x exceed window %d size 0x%x\n", offset, num_elements, wind_handle->window->number, wind_handle->window->size);
#endif
        return (GEF_STATUS_WRITE_ERR);
    }

    pva = (GEF_UINT8*)((GEF_UINT32)wind_handle->window->virtAddr + wind_handle->offset + offset);

    if(pva != 0)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeWrite8: Copy from buffer %p to virtual address %p of num_elements 0x%x for user\n", pva, pb, num_elements);
        for(i=0;i<num_elements;i++)
        {
            iowrite8(*pb++, pva++);
        }
    }
    else
    {
            GEF_VME_DEBUG(GEF_DEBUG_ERROR, "vmeWrite8: ERROR: Master window 0x%X has invalid virtual address (NULL)\n", wind_handle->window->number);
            return (GEF_STATUS_DRIVER_ERR);
    }

    return (GEF_SUCCESS);
}

int
vmeWrite16(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT16 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements)
{
    GEF_UINT16 *pva = 0;
    GEF_UINT16 va = 0;
    GEF_UINT16 *pb = buffer;
    int i = 0;
    vmeMasterHandle_t* wind_handle = (vmeMasterHandle_t*) (GEF_OS_CAST)handle;

    if( wind_handle->magic != MASTER_HANDLE_MAGIC )
    {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeWrite16: ERROR: Invalid window handle memory");
#else
        printk("vmeWrite16: ERROR: Invalid window handle memory\n");
#endif
        return (GEF_STATUS_DRIVER_ERR);
    }

    if (offset % 2)
    {
        return(GEF_STATUS_VME_OFFSET_NOT_ALIGNED);
    }

    if( offset > wind_handle->window->size )
    {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeWrite16: ERROR: Offset 0x%x exceeds window "
        "%d size 0x%x",
        offset, wind_handle->window->number, wind_handle->window->size);
#else
        printk("vmeWrite16: ERROR: Offset 0x%x exceeds window %d size 0x%x\n", offset, wind_handle->window->number, wind_handle->window->size);
#endif
        return (GEF_STATUS_WRITE_ERR);
    }

    if( (offset+(num_elements*2)) > wind_handle->window->size )
    {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeWrite16: ERROR: Offset 0x%x and "
        "num_elements 0x%x exceed window %d size 0x%x",
        offset, num_elements, wind_handle->window->number,
        wind_handle->window->size);
#else
        printk("vmeWrite16: ERROR: Offset 0x%x and num_elements 0x%x exceed window %d size 0x%x\n", offset, num_elements, wind_handle->window->number, wind_handle->window->size);
#endif
        return (GEF_STATUS_WRITE_ERR);
    }

    pva = (GEF_UINT16*)((GEF_UINT32)wind_handle->window->virtAddr + wind_handle->offset + offset);

    if(pva != 0)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "hwEndian=0x%x, wind_handle->window->swEndian=0x%x\n", hwEndian, wind_handle->window->swEndian);
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeWrite16: Copy from buffer %p to virtual address %p of num_elements 0x%x for user\n", pva, pb, num_elements);
        for(i=0;i<num_elements;i++)
        {
            if(hwEndian != wind_handle->window->swEndian)
            {
                va = *pb++;
                iowrite16(SWAP16(va), pva++);
            }
            else
            {
                iowrite16(*pb++, pva++);
            }
        }
    }
    else
    {
            GEF_VME_DEBUG(GEF_DEBUG_ERROR, "vmeWrite16: ERROR: Master window 0x%X has invalid virtual address (NULL)\n", wind_handle->window->number);
            return (GEF_STATUS_DRIVER_ERR);
    }

    return (GEF_SUCCESS);
}

int
vmeWrite32(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT32 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements)
{
    GEF_UINT32 *pva = 0;
    GEF_UINT32 va = 0;
    GEF_UINT32 *pb = buffer;
    int i = 0;
    vmeMasterHandle_t* wind_handle = (vmeMasterHandle_t*) (GEF_OS_CAST)handle;

    if( wind_handle->magic != MASTER_HANDLE_MAGIC )
    {
#ifdef USE_RTOS
             rtlogRecord_t logRecord;
             RTLOG_ERROR("gefvme_rtos", "vmeWrite32: ERROR: Invalid window "
                "handle memory");
#else
        printk("vmeWrite32: ERROR: Invalid window handle memory\n");
#endif
        return (GEF_STATUS_DRIVER_ERR);
    }

    if (offset % 4)
    {
        return(GEF_STATUS_VME_OFFSET_NOT_ALIGNED);
    }

    if( offset > wind_handle->window->size )
    {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeWrite32: ERROR: Offset 0x%x exceeds window "
        "%d size 0x%x",
        offset, wind_handle->window->number, wind_handle->window->size);
#else
        printk("vmeWrite32: ERROR: Offset 0x%x exceeds window %d size 0x%x\n", offset, wind_handle->window->number, wind_handle->window->size);
#endif
        return (GEF_STATUS_WRITE_ERR);
    }

    if( (offset+(num_elements*4)) > wind_handle->window->size )
    {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeWrite32: ERROR: Offset 0x%x and "
        "num_elements 0x%x exceed window %d size 0x%x",
        offset, num_elements, wind_handle->window->number,
        wind_handle->window->size);
#else
        printk("vmeWrite32: ERROR: Offset 0x%x and num_elements 0x%x exceed window %d size 0x%x\n", offset, num_elements, wind_handle->window->number, wind_handle->window->size);
#endif
        return (GEF_STATUS_WRITE_ERR);
    }

    pva = (GEF_UINT32*)((GEF_UINT32)wind_handle->window->virtAddr + wind_handle->offset + offset);

    if(pva != 0)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "hwEndian=0x%x, wind_handle->window->swEndian=0x%x\n", hwEndian, wind_handle->window->swEndian);
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeWrite32: Copy from buffer %p to virtual address %p of num_elements 0x%x for user\n", pva, pb, num_elements);
        for(i=0;i<num_elements;i++)
        {
            if(hwEndian != wind_handle->window->swEndian)
            {
                va = *pb++;
                iowrite32(SWAP32(va), pva++);
            }
            else
            {
                iowrite32(*pb++, pva++);
            }
        }
    }
    else
    {
            GEF_VME_DEBUG(GEF_DEBUG_ERROR, "vmeWrite32: ERROR: Master window 0x%X has invalid virtual address (NULL)\n", wind_handle->window->number);
            return (GEF_STATUS_DRIVER_ERR);
    }

    return (GEF_SUCCESS);
}

int
vmeWrite64(GEF_VME_DRV_READ_WRITE_INFO *write_info)
{
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeWrite64: NOT IMPLEMENTED");
#else
    printk("vmeWrite64: NOT IMPLEMENTED\n");
#endif    
    return (GEF_STATUS_NOT_SUPPORTED);
}

int
vmeCreateLocMonWindow(GEF_VME_DRV_LOCATION_MONITOR_INFO *lmInfo)
{
    vmeLmCfg_t vmeLm;
    int status = 0;

    memset(&vmeLm, 0, sizeof(vmeLmCfg_t));

    vmeLm.addrU = lmInfo->addr.upper;
    vmeLm.addr = lmInfo->addr.lower;

    switch(lmInfo->addr.addr_space)
    {
    case GEF_VME_ADDR_SPACE_A16: vmeLm.addrSpace = VME_A16; break;
    case GEF_VME_ADDR_SPACE_A24: vmeLm.addrSpace = VME_A24; break;
    case GEF_VME_ADDR_SPACE_A32: vmeLm.addrSpace = VME_A32; break;
    case GEF_VME_ADDR_SPACE_A64: vmeLm.addrSpace = VME_A64; break;
    case GEF_VME_ADDR_SPACE_CRCSR: vmeLm.addrSpace = VME_CRCSR; break;
    case GEF_VME_ADDR_SPACE_USER1: vmeLm.addrSpace = VME_USER1; break;
    case GEF_VME_ADDR_SPACE_USER2: vmeLm.addrSpace = VME_USER2; break;
    case GEF_VME_ADDR_SPACE_USER3: vmeLm.addrSpace = VME_USER3; break;
    case GEF_VME_ADDR_SPACE_USER4: vmeLm.addrSpace = VME_USER4; break;
    default: return (GEF_STATUS_VME_INVALID_ADDRESS_SPACE);
    }

    if( (lmInfo->addr.addr_mode & GEF_VME_ADDR_MODE_USER) == GEF_VME_ADDR_MODE_USER)
    {
        vmeLm.userAccessType |= VME_USER;
    }

    if( (lmInfo->addr.addr_mode & GEF_VME_ADDR_MODE_SUPER) == GEF_VME_ADDR_MODE_SUPER)
    {
        vmeLm.userAccessType |= VME_SUPER;
    }

    if( (lmInfo->addr.addr_mode & GEF_VME_ADDR_MODE_PROGRAM) == GEF_VME_ADDR_MODE_PROGRAM)
    {
        vmeLm.dataAccessType |= VME_PROG;
    }

    if( (lmInfo->addr.addr_mode & GEF_VME_ADDR_MODE_DATA) == GEF_VME_ADDR_MODE_DATA)
    {
        vmeLm.dataAccessType |= VME_DATA;
    }

    if(down_interruptible(&lm_window_lock))
    {
        return (GEF_STATUS_DRIVER_ERR);
    }

    status = vmeSetupLm(&vmeLm);
    if(status != 0)
    {
        if(status == -EBUSY) {
            locMonCount++;
            up(&lm_window_lock);
            return (GEF_STATUS_DEVICE_IN_USE);
        }

        up(&lm_window_lock);
        return (GEF_STATUS_DRIVER_ERR);
    }

    locMonCount++;
    up(&lm_window_lock);

    return (GEF_SUCCESS);
}

int
vmeReleaseLocMonWindow(GEF_VME_DRV_LOCATION_MONITOR_INFO *lmInfo)
{
    int status = 0;

    if(down_interruptible(&lm_window_lock))
    {
        return (GEF_STATUS_DRIVER_ERR);
    }

    if(locMonCount == 1)
    {
        switch(vmechip_devid) {
        case PCI_DEVICE_ID_TUNDRA_CA91C042:
            status = uniReleaseLocMonWindow();
            if(status != 0)
            {
                up(&lm_window_lock);
                return (GEF_STATUS_DRIVER_ERR);
            }
            break;
        case PCI_DEVICE_ID_TUNDRA_TEMPE:
            status = tempeReleaseLocMonWindow();
            if(status != 0)
            {
                up(&lm_window_lock);
                return (GEF_STATUS_DRIVER_ERR);
            }
            break;
        default:
            up(&lm_window_lock);
            return(GEF_STATUS_NO_SUCH_DEVICE);
        }

        locMonCount = 0;
    }
    else {
        if(locMonCount > 0){
            GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeReleaseLocMonWindow: Location Monitor is still in use with count of %d\n", locMonCount);
            --locMonCount;
        }
        else{
#ifdef USE_RTOS
      rtlogRecord_t logRecord;
      RTLOG_DEBUG("gefvme_rtos", "VME: Redundant Location Monitor release "
        "attempt.");
#else
            printk("VME: Redundant Location Monitor release attempt.\n");
#endif
        }
    }

    up(&lm_window_lock);

    return (GEF_SUCCESS);
}

int
vmeReleaseLocMonWindows(void)
{
    int status = 0;
    
    locMonCount = 0;
    
    switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
        status = uniReleaseLocMonWindow();
        if(status != 0)
        {
            return (GEF_STATUS_DRIVER_ERR);
        }
        break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
        status = tempeReleaseLocMonWindow();
        if(status != 0)
        {
            return (GEF_STATUS_DRIVER_ERR);
        }
        break;
    default:
        return(GEF_STATUS_NO_SUCH_DEVICE);
    }
    
    return (GEF_SUCCESS);
}

int
vmeQueryLocMonWindow(GEF_VME_DRV_LOCATION_MONITOR_INFO *lmInfo)
{
    int status = 0;

    switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
        status = uniQueryLocMonWindow(lmInfo);
        if(status != 0)
        {
            return (GEF_STATUS_DRIVER_ERR);
        }
        break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
        status = tempeQueryLocMonWindow(lmInfo);
        if(status != 0)
        {
            return (GEF_STATUS_DRIVER_ERR);
        }
        break;
    default:
        return(GEF_STATUS_NO_SUCH_DEVICE);
    }

    return (GEF_SUCCESS);
}

int
vmeCreateVraiWindow(GEF_VME_DRV_VRAI_INFO *vraiInfo)
{
    int status = 0;

    switch(vraiInfo->addr.addr_space)
    {
    case GEF_VME_ADDR_SPACE_A16:
    case GEF_VME_ADDR_SPACE_A24:
    case GEF_VME_ADDR_SPACE_A32:
    case GEF_VME_ADDR_SPACE_A64:
    case GEF_VME_ADDR_SPACE_CRCSR:
    case GEF_VME_ADDR_SPACE_USER1:
    case GEF_VME_ADDR_SPACE_USER2:
    case GEF_VME_ADDR_SPACE_USER3:
    case GEF_VME_ADDR_SPACE_USER4:
        break;
    default: return (GEF_STATUS_VME_INVALID_ADDRESS_SPACE);
    }

    if(down_interruptible(&vrai_window_lock))
    {
        return (GEF_STATUS_DRIVER_ERR);
    }

    switch(vmechip_devid) {
      case PCI_DEVICE_ID_TUNDRA_CA91C042:
        status = uniSetupVrai(vraiInfo);
        break;
      case PCI_DEVICE_ID_TUNDRA_TEMPE:
        status = tempeSetupVrai(vraiInfo);
        break;
    }

    if(status != 0)
    {
        if(status == -EBUSY) {
            vraiCount++;
            up(&vrai_window_lock);
            return (GEF_STATUS_DEVICE_IN_USE);
        }

        up(&vrai_window_lock);
        return (GEF_STATUS_DRIVER_ERR);
    }

    vraiCount++;
    up(&vrai_window_lock);

    return (GEF_SUCCESS);
}

int
vmeReleaseVraiWindow(GEF_VME_DRV_VRAI_INFO *vraiInfo)
{
    int status = 0;

    if(down_interruptible(&vrai_window_lock))
    {
        return (GEF_STATUS_DRIVER_ERR);
    }

    if(vraiCount == 1)
    {
        switch(vmechip_devid) {
        case PCI_DEVICE_ID_TUNDRA_CA91C042:
            status = uniReleaseVraiWindow();
            if(status != 0)
            {
                up(&vrai_window_lock);
                return (GEF_STATUS_DRIVER_ERR);
            }
            break;
        case PCI_DEVICE_ID_TUNDRA_TEMPE:
            status = tempeReleaseVraiWindow();
            if(status != 0)
            {
                up(&vrai_window_lock);
                return (GEF_STATUS_DRIVER_ERR);
            }
            break;
        default:
            up(&vrai_window_lock);
            return(GEF_STATUS_NO_SUCH_DEVICE);
        }

        vraiCount = 0;
    }
    else {
        if(vraiCount > 0){
            GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeReleaseVraiWindow: VRAI is still in use with count of %d\n", vraiCount);
            --vraiCount;
        }
        else{
#ifdef USE_RTOS
      rtlogRecord_t logRecord;
      RTLOG_DEBUG("gefvme_rtos", "VME: Redundant VRAI release attempt.");
#else
            printk("VME: Redundant VRAI release attempt.\n");
#endif
        }
    }

    up(&vrai_window_lock);

    return (GEF_SUCCESS);
}

int
vmeReleaseVraiWindows(void)
{
    int status = 0;
    
    vraiCount = 0;
    
    switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
        status = uniReleaseVraiWindow();
        if(status != 0)
        {
            return (GEF_STATUS_DRIVER_ERR);
        }
        break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
        status = tempeReleaseVraiWindow();
        if(status != 0)
        {
            return (GEF_STATUS_DRIVER_ERR);
        }
        break;
    default:
        return(GEF_STATUS_NO_SUCH_DEVICE);
    }
    
    return (GEF_SUCCESS);
}

int
vmeQueryVraiWindow(GEF_VME_DRV_VRAI_INFO *vraiInfo)
{
    int status = 0;

    switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
        status = uniQueryVraiWindow(vraiInfo);
        if(status != 0)
        {
            return (GEF_STATUS_DRIVER_ERR);
        }
        break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
        status = tempeQueryVraiWindow(vraiInfo);
        if(status != 0)
        {
            return (GEF_STATUS_DRIVER_ERR);
        }
        break;
    default:
        return(GEF_STATUS_NO_SUCH_DEVICE);
    }

    return (GEF_SUCCESS);
}

int 
vmeTransMasterWindow(GEF_VME_DRV_TRANSLATE_MASTER_WINDOW_INFO *transMasterInfo)
{
    GEF_STATUS retval = GEF_SUCCESS;

    /* get master window from master osspec handle */
    vmeMasterWindow_t *masterWindow = getWindowFromHandle(transMasterInfo->master_osspec_hdl);

    /* validate master window */
    if(NULL == masterWindow) {
        return(GEF_STATUS_DRIVER_ERR);
    }

    if(GEF_FALSE == masterWindow->exclusive) {
#ifdef USE_RTOS
      rtlogRecord_t logRecord;
      RTLOG_ERROR("gefvme_rtos", "vmeTransMasterWindow: ERROR: The window is "
        "not exclusive");
#else
        printk("vmeTransMasterWindow: ERROR: The window is not exclusive\n");
#endif
        return(GEF_STATUS_DRIVER_ERR);
    }

    /* program master window tramslation offset register to new vmebus address */
    switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
        retval = uniTransMasterWindow(transMasterInfo, masterWindow);
        break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
        retval = tempeTransMasterWindow(transMasterInfo, masterWindow);
        break;
    default:
        return(GEF_STATUS_NO_SUCH_DEVICE);
    }
    
    return(retval);
}

int
vmeSetTimeOnBus(GEF_VME_DRV_TIME_ON_BUS_INFO *timeOnBusInfo)
{
    GEF_STATUS retval = GEF_SUCCESS;

    switch(vmechip_devid) 
    {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
        retval = GEF_STATUS_NOT_SUPPORTED;
        break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeSetTimeOnBus: time on value 0x%x\n", timeOnBusInfo->time_on);
        retval = tempeSetTimeOnBus(timeOnBusInfo);
        break;
    default:
        return(GEF_STATUS_NO_SUCH_DEVICE);
    }

    return(retval);
}

int
vmeGetTimeOnBus(GEF_VME_DRV_TIME_ON_BUS_INFO *timeOnBusInfo)
{
    GEF_STATUS retval = GEF_SUCCESS;

    switch(vmechip_devid) 
    {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
        retval = GEF_STATUS_NOT_SUPPORTED;
        break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
        retval = tempeGetTimeOnBus(timeOnBusInfo);
        break;
    default:
        return(GEF_STATUS_NO_SUCH_DEVICE);
    }

    return(retval);
}

int
vmeSetTimeOffBus(GEF_VME_DRV_TIME_OFF_BUS_INFO *timeOffBusInfo)
{
    GEF_STATUS retval = GEF_SUCCESS;

    switch(vmechip_devid) 
    {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
        retval = GEF_STATUS_NOT_SUPPORTED;
        break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeSetTimeOffBus: time off value 0x%x\n", timeOffBusInfo->time_off);
        retval = tempeSetTimeOffBus(timeOffBusInfo);
        break;
    default:
        return(GEF_STATUS_NO_SUCH_DEVICE);
    }

    return(retval);
}

int
vmeGetTimeOffBus(GEF_VME_DRV_TIME_OFF_BUS_INFO *timeOffBusInfo)
{
    GEF_STATUS retval = GEF_SUCCESS;

    switch(vmechip_devid) 
    {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
        retval = GEF_STATUS_NOT_SUPPORTED;
        break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
        retval = tempeGetTimeOffBus(timeOffBusInfo);
        break;
    default:
        return(GEF_STATUS_NO_SUCH_DEVICE);
    }

    return(retval);
}

/*
 * End of ported functions
 */


//----------------------------------------------------------------------------
//  vmeDoDma()
//     Perform the requested DMA operation.
//     Caller must assure exclusive access to the DMA channel.  
//----------------------------------------------------------------------------
int
vmeDoDma( vmeDmaPacket_t *vmeDma)
{
  int retval = -ENODEV;

  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      retval = uniDoDma(vmeDma);
      break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
      retval = tempeDoDma(vmeDma);
      break;
  }

  return(retval);
}

//----------------------------------------------------------------------------
//  vmeGenerateIrq
//    Generate a VME bus interrupt at the specified level & vector.
//
//  Caller must assure exclusive access to the VIRQ generator.
//----------------------------------------------------------------------------
int
vmeGenerateIrq(GEF_VME_DRV_VIRQ_INFO *virq_info)
{
  int status = GEF_STATUS_NOT_SUPPORTED;

  
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      status = uniGenerateIrq(virq_info);
      break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
      status = tempeGenerateIrq(virq_info);
      break;
  }

  return(status);
}

//----------------------------------------------------------------------------
//  vmeWaitForIrq()
//    Wait for a specific VME bus interrupt to occur.
//----------------------------------------------------------------------------

int vmeWaitForIrq(GEF_VME_DRV_WAIT_INFO *wait_info)
{
  GEF_STATUS status = GEF_SUCCESS;
  GEF_UINT8 level;
  GEF_UINT32 vector = wait_info->int_info.int_vector;
#ifdef USE_RTOS
  RT_TASK* task = 0;
#endif
  struct timeval timeout = wait_info->timeout;
  struct timeval infinite_timeout = GEF_INFINITE_TIMEOUT;

#ifndef USE_RTOS
  wait_queue_t wait;
#endif
  unsigned long timeout_jiffies = 1;
  unsigned long irqflags;

  switch (wait_info->int_info.int_level) {
      
  case GEF_VME_INT_VOWN:  return (GEF_STATUS_NOT_SUPPORTED); break;
  case GEF_VME_INT_VIRQ1: level = DRV_IRQ_LEVEL_VIRQ1; break;
  case GEF_VME_INT_VIRQ2: level = DRV_IRQ_LEVEL_VIRQ2; break;
  case GEF_VME_INT_VIRQ3: level = DRV_IRQ_LEVEL_VIRQ3; break;
  case GEF_VME_INT_VIRQ4: level = DRV_IRQ_LEVEL_VIRQ4; break;
  case GEF_VME_INT_VIRQ5: level = DRV_IRQ_LEVEL_VIRQ5; break;
  case GEF_VME_INT_VIRQ6: level = DRV_IRQ_LEVEL_VIRQ6; break;
  case GEF_VME_INT_VIRQ7: level = DRV_IRQ_LEVEL_VIRQ7; break;
  case GEF_VME_INT_DMA0:  return (GEF_STATUS_NOT_SUPPORTED);
  case GEF_VME_INT_LERR:  level = DRV_IRQ_LEVEL_LERR; vector = 0; break;
  case GEF_VME_INT_BERR:  level = DRV_IRQ_LEVEL_BERR; vector = 0; break;
  case GEF_VME_INT_IRQ_EDGE: return (GEF_STATUS_NOT_SUPPORTED);
  case GEF_VME_INT_SW_IACK:  return (GEF_STATUS_NOT_SUPPORTED);
  case GEF_VME_INT_SW_INT:   return (GEF_STATUS_NOT_SUPPORTED);
  case GEF_VME_INT_SYSFAIL:level = DRV_IRQ_LEVEL_SYSFAIL; vector = 0; break;
  case GEF_VME_INT_ACFAIL:level = DRV_IRQ_LEVEL_ACFAIL; vector = 0; break;
  case GEF_VME_INT_MBOX0: level = DRV_IRQ_LEVEL_MBOX0; vector = 0; break;
  case GEF_VME_INT_MBOX1: level = DRV_IRQ_LEVEL_MBOX1; vector = 0; break;
  case GEF_VME_INT_MBOX2: level = DRV_IRQ_LEVEL_MBOX2; vector = 0; break;
  case GEF_VME_INT_MBOX3: level = DRV_IRQ_LEVEL_MBOX3; vector = 0; break;
  case GEF_VME_INT_LM0:   level = DRV_IRQ_LEVEL_LM0; vector = 0; break;
  case GEF_VME_INT_LM1:   level = DRV_IRQ_LEVEL_LM1; vector = 0; break;
  case GEF_VME_INT_LM2:   level = DRV_IRQ_LEVEL_LM2; vector = 0;  break;
  case GEF_VME_INT_LM3:   level = DRV_IRQ_LEVEL_LM3; vector = 0;  break;
  case GEF_VME_INT_DMA1:  return (GEF_STATUS_NOT_SUPPORTED);
  default: 
      return (GEF_STATUS_NOT_SUPPORTED);             
  }

  if (vector > 255) {
      return (GEF_STATUS_NOT_SUPPORTED);
  }

  switch(vmechip_devid) {
  case PCI_DEVICE_ID_TUNDRA_CA91C042:
#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&uni_lock);
#else
      spin_lock_irqsave(&uni_lock, irqflags);
#endif
      break;
  case PCI_DEVICE_ID_TUNDRA_TEMPE:
#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
      spin_lock_irqsave(&tempe_lock, irqflags);
#endif
      break;  
  default:
      return(GEF_STATUS_NO_SUCH_DEVICE);
  }

  if (vme_vec_flags[level][vector] & GEF_IRQ_WAIT) {

      switch(vmechip_devid) {
      case PCI_DEVICE_ID_TUNDRA_CA91C042:
#ifdef USE_RTOS
          rt_spin_unlock_irqrestore(irqflags, &uni_lock);
#else
          spin_unlock_irqrestore(&uni_lock, irqflags);
#endif
          break;
      case PCI_DEVICE_ID_TUNDRA_TEMPE:
#ifdef USE_RTOS
          rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
          spin_unlock_irqrestore(&tempe_lock, irqflags);
#endif
          break;  
      }
          GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeWaitForIrq: Event already in use for level %d vector %d\n",level,vector);
      return(GEF_STATUS_EVENT_IN_USE);
      
  }

  // Determine if any thread is waiting on this VIRQ level (count > 0)
  if (vme_irq_waitcount[level] == 0) { 
      status = vmeEnableIrq(&wait_info->int_info);
      
      if (status != GEF_SUCCESS) {
          
          switch(vmechip_devid) {
          case PCI_DEVICE_ID_TUNDRA_CA91C042:
#ifdef USE_RTOS
              rt_spin_unlock_irqrestore(irqflags, &uni_lock);
#else
              spin_unlock_irqrestore(&uni_lock, irqflags);
#endif
              break;
          case PCI_DEVICE_ID_TUNDRA_TEMPE:
#ifdef USE_RTOS
              rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
              spin_unlock_irqrestore(&tempe_lock, irqflags);
#endif
              break;  
          }
          
          return(status);
      }
  } 
  
  vme_irq_waitcount[level]++;
  vme_vec_flags[level][vector] |= (GEF_IRQ_WAIT | IRQ_TIMEDOUT);
  vme_vec_flags[level][vector] &= ~(IRQ_RELEASED);

  /* Go to sleep until timing out or receiving the interrupt */
#ifndef USE_RTOS
  init_waitqueue_entry(&wait, currentschedule_timeout);
  add_wait_queue( &irq_queue[level], &wait );
  set_current_state(TASK_INTERRUPTIBLE);
#endif

  switch(vmechip_devid) {
  case PCI_DEVICE_ID_TUNDRA_CA91C042:
#ifdef USE_RTOS
      rt_spin_unlock_irqrestore(irqflags, &uni_lock);
#else
      spin_unlock_irqrestore(&uni_lock, irqflags);
#endif
      break;
  case PCI_DEVICE_ID_TUNDRA_TEMPE:
#ifdef USE_RTOS
      rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
      spin_unlock_irqrestore(&tempe_lock, irqflags);
#endif
      break;
  }

  while (timeout_jiffies && (vme_vec_flags[level][vector] & IRQ_TIMEDOUT) &&
                    (vme_vec_flags[level][vector] & GEF_IRQ_WAIT) &&
                    (!(vme_vec_flags[level][vector] & IRQ_RELEASED))) {
      
      if ((vme_irqlog[level][vector] > 0) || (vme_vec_flags[level][vector] & IRQ_RELEASED)) {
          
          vme_vec_flags[level][vector] &= ~(IRQ_TIMEDOUT);
#ifndef USE_RTOS
          set_current_state(TASK_RUNNING);
#endif
          GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeWaitForIrq received interrupt without sleeping for level %d vector %d\n",
                  level,vector);
          break;       
      }

/*
 * No point in checking on signals while in RT.
 */
#ifndef USE_RTOS
      // Check if we have a signal
      if( signal_pending(current) ) {
          status = GEF_STATUS_INTERRUPTED;
          GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeWaitForIrq interrupted by signal for level %d vector %d\n",level,vector);
          break;		
      }
#endif

      // If interrupt is received for same level, different vector, must reset task state
#ifndef USE_RTOS
      set_current_state(TASK_INTERRUPTIBLE);
#endif

      if (0 == timeval_compare(&timeout, &infinite_timeout)) {
          
          timeout_jiffies = MAX_SCHEDULE_TIMEOUT;
      } else {
          timeout_jiffies  = (timeout.tv_sec * HZ);
          timeout_jiffies += (timeout.tv_usec * HZ) / 1000000UL;
          if (timeout_jiffies == 0) {
              timeout_jiffies = 1;
          }
      }
      
      // schedule_timeout doesn't like negative values.
      timeout_jiffies &= 0x7fffffff;
      
      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeWaitForIrq calling schedule_timeout() for level %d vector %d\n",level,vector);
#ifdef USE_RTOS
    /*
     * task points to the current RT_TASK.  This should not be 0
     * but check for it nevertheless.
     */
    task = rt_whoami();
    if(task != 0)
    {
        irq_queue[level] = task;
        timeout_jiffies = rt_task_suspend_timed(task,
            nano2count(jiffies_to_usecs(timeout_jiffies) * 1000ULL));
        if(timeout_jiffies < 0)
        {
            GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeWaitForIrq cannot suspend this "
            "RT-task.");
            status = GEF_STATUS_OPERATION_NOT_ALLOWED;
            return status;
        }
        else
        {
            timeout_jiffies = usecs_to_jiffies(
                div_s64(count2nano(timeout_jiffies), 1000ULL));
        }
    }
    else
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeWaitForIrq cannot suspend, not an "
            "RT-task.");
        status = GEF_STATUS_OPERATION_NOT_ALLOWED;
        return status;
    }
    /*
     * This RT_TASK sleeps now until it is awoken by the
     * matching IRQ.
     */
#else
      timeout_jiffies = schedule_timeout(timeout_jiffies);
#endif
      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeWaitForIrq resumed from schedule_timeout() for level %d vector %d\n",level,vector);
      
  }

#ifdef USE_RTOS
    /*
     * Woke up.  Clear the task from the IRQ handler list.
     */
    irq_queue[level] = 0;
#else
  remove_wait_queue(&irq_queue[level], &wait);
#endif

  // Acquire spin lock
  switch(vmechip_devid) {
  case PCI_DEVICE_ID_TUNDRA_CA91C042:
#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&uni_lock);
#else
      spin_lock_irqsave(&uni_lock, irqflags);
#endif
      break;
  case PCI_DEVICE_ID_TUNDRA_TEMPE:
#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
      spin_lock_irqsave(&tempe_lock, irqflags);
#endif
      break;  
  }

  if (vme_vec_flags[level][vector] & IRQ_RELEASED) {
      status = GEF_STATUS_INTERRUPTED;

  } else if (vme_vec_flags[level][vector] & IRQ_TIMEDOUT) {
      status = GEF_STATUS_TIMED_OUT;

  } else if (vme_irqlog[level][vector] > 0) {
      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeWaitForIrq: Received interrupt level %d vector %d\n",level,vector);
      
      // Handle BERR case
      if (level == DRV_IRQ_LEVEL_BERR) {
          GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeWaitForIrq: BERR occurred\n");
          
          wait_info->int_info.berr_info = vme_irq_berr_info[vme_berr_read_buf];
#ifdef USE_RTOS
          rtlogRecord_t logRecord;
          RTLOG_WARNING("gefvme_rtos",
              "VME Bus Error Count: %d\n", vme_irqlog[level][vector]);
#else
          printk("VME Bus Error Count: %d\n",vme_irqlog[level][vector]);
#endif
      }

      // Handle MB case
      if ( (level >= GEF_VME_INT_MBOX0) && (level <= GEF_VME_INT_MBOX3) ) {
          GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeWaitForIrq: Mailbox level %d occurred\n", level);
          
          wait_info->int_info.int_vector = vme_irq_int_info[level].int_vector;
          GEF_VME_DEBUG(GEF_DEBUG_TRACE, "VME Mailbox Count: %d\n",vme_irqlog[level][vector]);
          GEF_VME_DEBUG(GEF_DEBUG_TRACE, "VME Mailbox Vector: 0x%x\n",wait_info->int_info.int_vector);
      }
      
      // Must Acknowlege interrupts that are not VIRQ1..VIRQ7
      if ((level < DRV_IRQ_LEVEL_VIRQ1) || (level > DRV_IRQ_LEVEL_VIRQ7)) {
          
          vme_vec_flags[level][vector] &= ~(GEF_IRQ_WAIT | IRQ_RELEASED | IRQ_TIMEDOUT);
          
          // Handle BERR/Error interrupt case due to potentially large number of interrupts
          // while in ISR.  User will actually only be notified on last BERR when waiting.
          if (level == DRV_IRQ_LEVEL_BERR) {
              // For Non-VIRQ error interrupts (i.e., BERR, etc), reset the counts to 0
              vme_irq_waitcount[level] = 0;
              vme_irqlog[level][vector] = 0;
          } else {
              vme_irq_waitcount[level]--;
              vme_irqlog[level][vector]--;
          }
      }
      
  } // Otherwise system signal occurred.

  if (status != GEF_SUCCESS) {
      vme_irq_waitcount[level]--;
      vme_vec_flags[level][vector] &= ~(GEF_IRQ_WAIT | IRQ_RELEASED | IRQ_TIMEDOUT);

      // Determine if any thread waiting (count > 0)
      if (vme_irq_waitcount[level] == 0) {

          GEF_STATUS disable_status;
          
          // Clear interrupt handler log entry
          vme_irqlog[level][vector] = 0;
          
          disable_status = vmeDisableIrq(&wait_info->int_info);

          if (disable_status != GEF_SUCCESS) {
              status = disable_status;
          }
      } 
  }

    // Release spin lock
  switch(vmechip_devid) {
  case PCI_DEVICE_ID_TUNDRA_CA91C042:
#ifdef USE_RTOS
      rt_spin_unlock_irqrestore(irqflags, &uni_lock);
#else
      spin_unlock_irqrestore(&uni_lock, irqflags);
#endif
      break;
  case PCI_DEVICE_ID_TUNDRA_TEMPE:
#ifdef USE_RTOS
      rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
      spin_unlock_irqrestore(&tempe_lock, irqflags);
#endif
      break;  
  }
   
  return(status);
}

//----------------------------------------------------------------------------
//  vmeWaitIrqAck()
//    Acknowledge a specific VME bus interrupt.
//----------------------------------------------------------------------------

int vmeWaitIrqAck(GEF_VME_DRV_WAIT_INFO *wait_info)
{
  GEF_STATUS status = GEF_SUCCESS;
  GEF_UINT8 level;
  GEF_UINT32 vector = wait_info->int_info.int_vector;
  unsigned long irqflags;


  switch (wait_info->int_info.int_level) {

  case GEF_VME_INT_VOWN:  return (GEF_STATUS_NOT_SUPPORTED); break;
  case GEF_VME_INT_VIRQ1: level = DRV_IRQ_LEVEL_VIRQ1; break;
  case GEF_VME_INT_VIRQ2: level = DRV_IRQ_LEVEL_VIRQ2; break;
  case GEF_VME_INT_VIRQ3: level = DRV_IRQ_LEVEL_VIRQ3; break;
  case GEF_VME_INT_VIRQ4: level = DRV_IRQ_LEVEL_VIRQ4; break;
  case GEF_VME_INT_VIRQ5: level = DRV_IRQ_LEVEL_VIRQ5; break;
  case GEF_VME_INT_VIRQ6: level = DRV_IRQ_LEVEL_VIRQ6; break;
  case GEF_VME_INT_VIRQ7: level = DRV_IRQ_LEVEL_VIRQ7; break;
  case GEF_VME_INT_DMA0:  return (GEF_STATUS_NOT_SUPPORTED);
  case GEF_VME_INT_LERR:  level = DRV_IRQ_LEVEL_LERR; vector = 0; break;
  case GEF_VME_INT_BERR:  level = DRV_IRQ_LEVEL_BERR; vector = 0; break;
  case GEF_VME_INT_IRQ_EDGE: return (GEF_STATUS_NOT_SUPPORTED);
  case GEF_VME_INT_SW_IACK:  return (GEF_STATUS_NOT_SUPPORTED);
  case GEF_VME_INT_SW_INT:   return (GEF_STATUS_NOT_SUPPORTED);
  case GEF_VME_INT_SYSFAIL:level = DRV_IRQ_LEVEL_SYSFAIL; vector = 0; break;
  case GEF_VME_INT_ACFAIL:level = DRV_IRQ_LEVEL_ACFAIL; vector = 0; break;
  case GEF_VME_INT_MBOX0: level = DRV_IRQ_LEVEL_MBOX0; vector = 0; break;
  case GEF_VME_INT_MBOX1: level = DRV_IRQ_LEVEL_MBOX1; vector = 0; break;
  case GEF_VME_INT_MBOX2: level = DRV_IRQ_LEVEL_MBOX2; vector = 0; break;
  case GEF_VME_INT_MBOX3: level = DRV_IRQ_LEVEL_MBOX3; vector = 0; break;
  case GEF_VME_INT_LM0:   level = DRV_IRQ_LEVEL_LM0; vector = 0; break;
  case GEF_VME_INT_LM1:   level = DRV_IRQ_LEVEL_LM1; vector = 0; break;
  case GEF_VME_INT_LM2:   level = DRV_IRQ_LEVEL_LM2; vector = 0; break;
  case GEF_VME_INT_LM3:   level = DRV_IRQ_LEVEL_LM3; vector = 0; break;
  case GEF_VME_INT_DMA1:  return (GEF_STATUS_NOT_SUPPORTED);
  default: 
    
      return (GEF_STATUS_NOT_SUPPORTED);             
  }

  if (vector > 255) {
      return (GEF_STATUS_NOT_SUPPORTED);
  }

  // Acknowledge is only necessary for VIRQ interrupts
  if ((level < DRV_IRQ_LEVEL_VIRQ1) || (level > DRV_IRQ_LEVEL_VIRQ7)) {
      return(GEF_SUCCESS);
  }

  switch(vmechip_devid) {
  case PCI_DEVICE_ID_TUNDRA_CA91C042:
#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&uni_lock);
#else
      spin_lock_irqsave(&uni_lock, irqflags);
#endif
      break;
  case PCI_DEVICE_ID_TUNDRA_TEMPE:
#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
      spin_lock_irqsave(&tempe_lock, irqflags);
#endif
      break;  
  default:
      return(GEF_STATUS_NO_SUCH_DEVICE);
  }

  if ((vme_irq_waitcount[level] > 0) && 
      (vme_vec_flags[level][vector] & GEF_IRQ_WAIT) &&
      (vme_irqlog[level][vector] > 0)) {
      
      vme_vec_flags[level][vector] &= ~(GEF_IRQ_WAIT | IRQ_RELEASED | IRQ_TIMEDOUT);
      vme_irq_waitcount[level]--;
      vme_irqlog[level][vector]--;

  // Determine if Wait has completed, Release called, Acknowledge has not yet been called
  } else if (vme_vec_flags[level][vector] & IRQ_RELEASED) {
      vme_vec_flags[level][vector] &= ~(GEF_IRQ_WAIT | IRQ_RELEASED | IRQ_TIMEDOUT);
      vme_irq_waitcount[level]--;
      vme_irqlog[level][vector]--;
      status = GEF_STATUS_EVENT_RELEASED;
  } else {
      switch(vmechip_devid) {
      case PCI_DEVICE_ID_TUNDRA_CA91C042:
#ifdef USE_RTOS
        rt_spin_unlock_irqrestore(irqflags, &uni_lock);
#else
          spin_unlock_irqrestore(&uni_lock, irqflags);
#endif
          break;
      case PCI_DEVICE_ID_TUNDRA_TEMPE:
#ifdef USE_RTOS
        rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
          spin_unlock_irqrestore(&tempe_lock, irqflags);
#endif
          break;  
          
      }
      return (GEF_STATUS_DRIVER_ERR);
  }

  // Determine if any thread is waiting on this level(count > 0)
  if (vme_irq_waitcount[level] > 0) {
      
      // Clear interrupt handler log entry
      vme_irqlog[level][vector] = 0;
      
      status = vmeEnableIrq(&wait_info->int_info);            
  } 
  
  switch(vmechip_devid) {
  case PCI_DEVICE_ID_TUNDRA_CA91C042:
#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &uni_lock);
#else
      spin_unlock_irqrestore(&uni_lock, irqflags);
#endif
      break;
  case PCI_DEVICE_ID_TUNDRA_TEMPE:
#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
      spin_unlock_irqrestore(&tempe_lock, irqflags);
#endif
      break;  
  }
  return(status);
}


int
vmeReleaseIrqWait(GEF_VME_DRV_VIRQ_INFO *virq_info)
{
  int status = GEF_SUCCESS;
  GEF_UINT8 level;
  GEF_UINT32 vector = virq_info->vector;
  unsigned long irqflags;

  switch (virq_info->level) {

  case GEF_VME_INT_VOWN:  return (GEF_STATUS_NOT_SUPPORTED); break;
  case GEF_VME_INT_VIRQ1: level = DRV_IRQ_LEVEL_VIRQ1; break;
  case GEF_VME_INT_VIRQ2: level = DRV_IRQ_LEVEL_VIRQ2; break;
  case GEF_VME_INT_VIRQ3: level = DRV_IRQ_LEVEL_VIRQ3; break;
  case GEF_VME_INT_VIRQ4: level = DRV_IRQ_LEVEL_VIRQ4; break;
  case GEF_VME_INT_VIRQ5: level = DRV_IRQ_LEVEL_VIRQ5; break;
  case GEF_VME_INT_VIRQ6: level = DRV_IRQ_LEVEL_VIRQ6; break;
  case GEF_VME_INT_VIRQ7: level = DRV_IRQ_LEVEL_VIRQ7; break;
  case GEF_VME_INT_DMA0:  return (GEF_STATUS_NOT_SUPPORTED);
  case GEF_VME_INT_LERR:  level = DRV_IRQ_LEVEL_LERR; vector = 0; break;
  case GEF_VME_INT_BERR:  level = DRV_IRQ_LEVEL_BERR; vector = 0; break;
  case GEF_VME_INT_IRQ_EDGE: return (GEF_STATUS_NOT_SUPPORTED);
  case GEF_VME_INT_SW_IACK:  return (GEF_STATUS_NOT_SUPPORTED);
  case GEF_VME_INT_SW_INT:   return (GEF_STATUS_NOT_SUPPORTED);
  case GEF_VME_INT_SYSFAIL:level = DRV_IRQ_LEVEL_SYSFAIL; vector = 0; break;
  case GEF_VME_INT_ACFAIL:level = DRV_IRQ_LEVEL_ACFAIL; vector = 0; break;
  case GEF_VME_INT_MBOX0: level = DRV_IRQ_LEVEL_MBOX0; vector = 0; break;
  case GEF_VME_INT_MBOX1: level = DRV_IRQ_LEVEL_MBOX1; vector = 0; break;
  case GEF_VME_INT_MBOX2: level = DRV_IRQ_LEVEL_MBOX2; vector = 0; break;
  case GEF_VME_INT_MBOX3: level = DRV_IRQ_LEVEL_MBOX3; vector = 0; break;
  case GEF_VME_INT_LM0:   level = DRV_IRQ_LEVEL_LM0; vector = 0; break;
  case GEF_VME_INT_LM1:   level = DRV_IRQ_LEVEL_LM1; vector = 0; break;
  case GEF_VME_INT_LM2:   level = DRV_IRQ_LEVEL_LM2; vector = 0; break;
  case GEF_VME_INT_LM3:   level = DRV_IRQ_LEVEL_LM3; vector = 0; break;
  case GEF_VME_INT_DMA1:  return (GEF_STATUS_NOT_SUPPORTED);    
  default: 
    
      return (GEF_STATUS_NOT_SUPPORTED);             
  }

  if (vector > 255) {
      return (GEF_STATUS_NOT_SUPPORTED);
  }

  switch(vmechip_devid) {
  case PCI_DEVICE_ID_TUNDRA_CA91C042:
#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&uni_lock);
#else
      spin_lock_irqsave(&uni_lock, irqflags);
#endif
      break;
  case PCI_DEVICE_ID_TUNDRA_TEMPE:
#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
      spin_lock_irqsave(&tempe_lock, irqflags);
#endif
      break;  
  default:
      return(GEF_STATUS_NO_SUCH_DEVICE);
  }

  if ((vme_vec_flags[level][vector] & GEF_IRQ_WAIT) == 0) {
            
      status = GEF_STATUS_NO_EVENT_PENDING;
  } else {
      
      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeReleaseIrqWait: releasing level %d vector %d\n",
          level, vector);
      
      vme_vec_flags[level][vector] |= IRQ_RELEASED;
#ifdef USE_RTOS
    RT_TASK* task = irq_queue[level];
    if(task != 0)
    {
        rt_task_resume(task);
    }
#else
      wake_up_interruptible(&irq_queue[level]);
#endif
  }

  switch(vmechip_devid) {
  case PCI_DEVICE_ID_TUNDRA_CA91C042:
#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &uni_lock);
#else
      spin_unlock_irqrestore(&uni_lock, irqflags);
#endif
      break;
  case PCI_DEVICE_ID_TUNDRA_TEMPE:
#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
      spin_unlock_irqrestore(&tempe_lock, irqflags);
#endif
      break;  
  }

  return(status);
}

//----------------------------------------------------------------------------
//  vmeWaitIrqAck()
//    Free waiting threads for irq level and vector combinations
//----------------------------------------------------------------------------

int
vmeFreeIrqWaits(void) {
    
    GEF_VME_INT_LEVEL level;
    GEF_UINT16 vector;
    GEF_VME_DRV_VIRQ_INFO virq_info;
    GEF_UINT16 loop_count = 0;
    GEF_STATUS status;
    int looptimeout;
#ifndef USE_RTOS
    wait_queue_head_t wait_queue;
#endif

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeFreeIrqWaits:\n");
#ifdef USE_RTOS
#else
    init_waitqueue_head(&wait_queue);
#endif

    // Poll for freed threads at 200 times per second rate
    looptimeout = HZ/200; 
    
    if (looptimeout <= 0) {
        looptimeout = 1;
    }
    
    // Free waiting interrupt threads
    for (level = 0; level < GEF_VME_INT_LEVEL_LAST; level++) {
        
        virq_info.level = level;
        
        for (vector = 0; vector <= 255; vector++) {              
            virq_info.vector = vector;
            
            if ((vector > 0) && ((level < 1) || (level > 7))){
                status = GEF_STATUS_NOT_SUPPORTED;
            }
            else {
                
                status = vmeReleaseIrqWait(&virq_info);
                if (status == GEF_SUCCESS) {
                    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeFreeIrqWaits: releasing level %d vector %d wait\n",level,vector);
                    
                    loop_count = 0;
                    while ((loop_count < 200) && (vme_vec_flags[level][vector] & IRQ_RELEASED)) {
#ifdef USE_RTOS
                        rt_sleep(nano2count(
                            jiffies_to_usecs(looptimeout) * 1000ULL));
#else
                        wait_event_interruptible_timeout(wait_queue, 
                            ((vme_vec_flags[level][vector] & IRQ_RELEASED) == 0), looptimeout);
#endif
                        loop_count++;
                    }
                    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeFreeIrqWaits: freed waiting thread, loop_count %d\n",loop_count);
                    if (loop_count >= 200) {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "gefvme: Failed to free level %d vector %d wait",
        level, vector);
#else
                        printk("gefvme: Failed to free level %d vector %d wait\n",level,vector);
#endif
                    }
                }
            }            
        }
    }
    return(0);
}

/*============================================================================
 * Perform a read modify write cycle on the VMEbus
 *
 * Read modify write is performed by reading a value from the VMEbus, the
 * masked values are bitwise compared with the contents of cmp.  All bits that
 * compare true are are swapped with the contents of the swap register.
 *
 * NOTE: To prevent side effects, the window must be created with the
 * GEF_VME_CTL_EXCLUSIVE flag to use this function.  If your application is
 * multi-threaded you should take measures to protect againsts other threads
 * attempting to make access to the same VMEbus address.  Read modify write
 * only works for PCI memory mapped windows.
 */
int vmeReadModifyWrite(GEF_VME_DRV_READ_MODIFY_WRITE_INFO *rmw_info)
{
    vmeRmwCfg_t vmeRmw;
    vmeMasterWindow_t *masterWindow = getWindowFromHandle(rmw_info->master_osspec_hdl);
    vmeMasterHandle_t* wind_handle = (vmeMasterHandle_t*) (GEF_OS_CAST)rmw_info->master_osspec_hdl;
 
    GEF_VME_ADDR vme_addr;
    int drv_xfer_max_dwidth;

    if (NULL == rmw_info)
    {
        return (GEF_STATUS_DRIVER_ERR);
    }

    if (NULL == masterWindow)
    {
        return (GEF_STATUS_DRIVER_ERR);
    }

    vme_addr = masterWindow->vmeAddr;

    vmeRmw.targetAddrU = vme_addr.upper;
    vmeRmw.targetAddr = masterWindow->vmeL + rmw_info->offset;

    vmeRmw.mastWindVmeAddrU = masterWindow->vmeU;
    vmeRmw.mastWindVmeAddrL = masterWindow->vmeL;
    vmeRmw.mastWindBasePciAddr = masterWindow->physAddr + wind_handle->offset;
    vmeRmw.mastWindVirtAddr = masterWindow->virtAddr + wind_handle->offset;
    vmeRmw.mastWindSizeU = masterWindow->sizeU;
    vmeRmw.mastWindSizeL = masterWindow->size;
    vmeRmw.dataWidth = 0;

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Read Modify Write offset 0x%x dw 0x%x Window Addr lower 0x%x\n",
            rmw_info->offset,rmw_info->dw,vme_addr.lower); 
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calculated VME Byte Address for RMW cycle: 0x%x\n", vmeRmw.targetAddr);

    // Will need modified for 64-bit platform support
    if (vmeRmw.targetAddr > (masterWindow->vmeL + masterWindow->size))
    {
        return (GEF_STATUS_INVAL_ADDR);
    }

   /* transfer max width */
    switch(vme_addr.transfer_max_dwidth)
    {
    case GEF_VME_TRANSFER_MAX_DWIDTH_8:  drv_xfer_max_dwidth = VME_D8; break;
    case GEF_VME_TRANSFER_MAX_DWIDTH_16: drv_xfer_max_dwidth = VME_D16; break;
    case GEF_VME_TRANSFER_MAX_DWIDTH_32: drv_xfer_max_dwidth = VME_D32; break;
    case GEF_VME_TRANSFER_MAX_DWIDTH_64: drv_xfer_max_dwidth = VME_D64; break;
    default: return (GEF_STATUS_VME_INVALID_TRANSFER_WIDTH);
    }

	/* Make sure the data width and alignment are valid before proceeding */
    switch (rmw_info->dw) 
    {
    case GEF_VME_DWIDTH_D8:
        /* Always support D8 */
        break;
        
    case GEF_VME_DWIDTH_D16:
        if (vmeRmw.targetAddr % 2) 
        {
            return(GEF_STATUS_INVAL_ADDR); 
        }
        break;
        
    case GEF_VME_DWIDTH_D32:
        if (vmeRmw.targetAddr % 4) 
        {
            return(GEF_STATUS_INVAL_ADDR); 
        }
        /* Make sure requested size is not greater than size supported by window */
        if (VME_D32 > drv_xfer_max_dwidth)
        {
            return(GEF_STATUS_VME_INVALID_TRANSFER_WIDTH); 
        }
        break;
        
    default:
        return(GEF_STATUS_INVAL_ADDR); 
        break;
    }

    if (GEF_FALSE == masterWindow->exclusive)
    {
        return (GEF_STATUS_VME_MASTER_WND_NOT_EXCLUSIVE);
    }

    /* address space */
    switch(vme_addr.addr_space)
    {
    case GEF_VME_ADDR_SPACE_A16:   vmeRmw.addrSpace = VME_A16; break;
    case GEF_VME_ADDR_SPACE_A24:   vmeRmw.addrSpace = VME_A24; break;
    case GEF_VME_ADDR_SPACE_A32:   vmeRmw.addrSpace = VME_A32; break;
    case GEF_VME_ADDR_SPACE_A64:   vmeRmw.addrSpace = VME_A64; break;
    case GEF_VME_ADDR_SPACE_CRCSR: vmeRmw.addrSpace = VME_CRCSR; break;
    case GEF_VME_ADDR_SPACE_USER1: vmeRmw.addrSpace = VME_USER1; break;
    case GEF_VME_ADDR_SPACE_USER2: vmeRmw.addrSpace = VME_USER2; break;
    case GEF_VME_ADDR_SPACE_USER3: vmeRmw.addrSpace = VME_USER3; break;
    case GEF_VME_ADDR_SPACE_USER4: vmeRmw.addrSpace = VME_USER4; break;
    default: return (GEF_STATUS_VME_INVALID_ADDRESS_SPACE);
    }

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeReadModifyWrite inputs: mask 0x%x cmp 0x%x swap 0x%x\n",
               rmw_info->mask,rmw_info->cmp,rmw_info->swap);

	/* Set up RMW mask, compare, and swap values.  For Byte and 16-bit widths, the value
       is masked and repeated in the register, based on the data width requested. */
    switch (rmw_info->dw) 
    {
    case GEF_VME_DWIDTH_D8:
        vmeRmw.enableMask = ((rmw_info->mask & 0xff) | ((rmw_info->mask << 8) & 0xff00) |
                            ((rmw_info->mask << 16) & 0xff0000) | 
                            ((rmw_info->mask << 24) & 0xff000000));

        vmeRmw.compareData = ((rmw_info->cmp & 0xff) | ((rmw_info->cmp << 8) & 0xff00) |
                            ((rmw_info->cmp << 16) & 0xff0000) | 
                            ((rmw_info->cmp << 24) & 0xff000000));

        vmeRmw.swapData = ((rmw_info->swap & 0xff) | ((rmw_info->swap << 8) & 0xff00) |
                            ((rmw_info->swap << 16) & 0xff0000) | 
                            ((rmw_info->swap << 24) & 0xff000000));
                            
        break;
        
    case GEF_VME_DWIDTH_D16:
        if (vmeRmw.targetAddr % 2) 
        {
            return(GEF_STATUS_INVAL_ADDR); 
        }

        /* Make sure requested size is not greater than size supported by window */
        if (VME_D16 > drv_xfer_max_dwidth)
        {
            return(GEF_STATUS_VME_INVALID_TRANSFER_WIDTH); 
        }

        vmeRmw.enableMask = ((rmw_info->mask & 0xffff) | ((rmw_info->mask << 16) & 0xffff0000));
        vmeRmw.compareData = ((rmw_info->cmp & 0xffff) | ((rmw_info->cmp << 16) & 0xffff0000));
        vmeRmw.swapData = ((rmw_info->swap & 0xffff) | ((rmw_info->swap << 16) & 0xffff0000));
        break;
        
    case GEF_VME_DWIDTH_D32:
        if (vmeRmw.targetAddr % 4) 
        {
            return(GEF_STATUS_INVAL_ADDR); 
        }
        /* Make sure requested size is not greater than size supported by window */
        if (VME_D32 > drv_xfer_max_dwidth)
        {
            return(GEF_STATUS_VME_INVALID_TRANSFER_WIDTH); 
        }

        vmeRmw.enableMask = rmw_info->mask;
        vmeRmw.compareData = rmw_info->cmp;
        vmeRmw.swapData = rmw_info->swap;
        break;
        
    default:
        return(GEF_STATUS_INVAL_ADDR); 
        break;
    }


    vmeRmw.dataWidth = rmw_info->dw;

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeReadModifyWrite updates per size: width 0x%x mask 0x%x cmp 0x%x swap 0x%x\n",
               vmeRmw.dataWidth,vmeRmw.enableMask,vmeRmw.compareData,vmeRmw.swapData);

    switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
        return(uniDoRmw(&vmeRmw));
        break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
        return(tempeDoRmw(&vmeRmw));
        break;
    }
    return (GEF_STATUS_NOT_SUPPORTED);
}

//----------------------------------------------------------------------------
//  vmeSetupLm()
//    Setup the VME bus location monitor to detect the specified access.
//----------------------------------------------------------------------------
int
vmeSetupLm( vmeLmCfg_t *vmeLm)
{
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      return(uniSetupLm(vmeLm));
      break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
      return(tempeSetupLm(vmeLm));
      break;
  }
  return(-ENODEV);
}

//----------------------------------------------------------------------------
//  vmeInit()
//    Initialize the VME bridge.
//----------------------------------------------------------------------------
int
vmeInit(void)
{
    int i = 0;

    locMonCount = 0;
    vraiCount = 0;

    sema_init(&master_window_lock, 1);
    sema_init(&slave_window_lock, 1);
    sema_init(&dma_buffer_lock, 1);
    sema_init(&lm_window_lock, 1);
    sema_init(&vrai_window_lock, 1);

    /* initialize master windows */
    for(i=0;i<VME_MAX_WINDOWS;i++)
    {
        vmw[i].handles = NULL;
        vmw[i].virtAddr = 0;
        vmw[i].physAddr = 0;
        vmw[i].exclusive = GEF_FALSE;
        memset(&vmw[i].physAddrRes, 0, sizeof(struct resource));
        vmw[i].number = -1;
        vmw[i].swEndian = GEF_ENDIAN_BIG;  // big endian by default

        if(vmw[i].swEndian == GEF_ENDIAN_BIG)
        {
            GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeInit: Window %d: Software Endian is BIG\n", i);
        }
        else
        {
            GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeInit: Window %d: Software Endian is LITTLE\n", i);
        }
    }

    /* initialize slave windows */
    for(i=0;i<VME_MAX_WINDOWS;i++)
    {
        vsw[i].handles = NULL;
        vsw[i].virtAddr = 0;
        vsw[i].physAddr = 0;
        vsw[i].exclusive = GEF_FALSE;
        memset(&vsw[i].physAddrRes, 0, sizeof(struct resource));
        vsw[i].number = -1;
    }
    
    switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
        return(uni_init());
        break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
        return(tempe_init());
        break;
    }

    if(hwEndian == GEF_ENDIAN_BIG)
        {
            GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeInit: Hardware Endian is BIG\n");
        }
        else
        {
            GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeInit: Hardware Endian is LITTLE\n");
        }

    return(0);
}

int
vmeSetDebugFlags(GEF_UINT32 *debug_flags)
{
    gef_vme_debug_flags = *debug_flags;
    return(0);
}

int
vmeGetDebugFlags(GEF_UINT32 *debug_flags)
{
    *debug_flags = gef_vme_debug_flags;
    return(0);
}

int 
vmeSetSwByteSwap(GEF_VME_DRV_BYTESWAP_INFO *swByteSwap)
{
    vmeMasterWindow_t *p = getWindowFromHandle(swByteSwap->master_osspec_hdl);

    if(p == NULL)
    {
        GEF_VME_DEBUG(GEF_DEBUG_ERROR, "vmeSetSwByteSwap: Invalid window handle 0x%x\n", (unsigned int)swByteSwap->master_osspec_hdl);
        return (GEF_STATUS_DRIVER_ERR);
    }

    p->swEndian = swByteSwap->endian;

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeSetSwByteSwap: Set window %d to Endian=0x%x\n", p->number, p->swEndian);

    return (GEF_SUCCESS);
}

int
vmeGetSwByteSwap(GEF_VME_DRV_BYTESWAP_INFO *swByteSwap)
{
    vmeMasterWindow_t *p = getWindowFromHandle(swByteSwap->master_osspec_hdl);

    if(p == NULL)
    {
        GEF_VME_DEBUG(GEF_DEBUG_ERROR, "vmeSetSwByteSwap: Invalid window handle 0x%x\n", (unsigned int)swByteSwap->master_osspec_hdl);
        return (GEF_STATUS_DRIVER_ERR);
    }

    swByteSwap->endian = p->swEndian;

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeSetSwByteSwap: Get window %d with Endian=0x%x\n", p->number, p->swEndian);

    return (GEF_SUCCESS);
}

//----------------------------------------------------------------------------
//  vmeMapMasterWindow()
//    Return the physical address of the specified Master window.
//----------------------------------------------------------------------------
int
vmeMapMasterWindow(GEF_VME_MAP_MASTER_INFO *map_info)
{
    int status = GEF_SUCCESS;
    vmeMasterHandle_t* wind_handle = (vmeMasterHandle_t*) (GEF_OS_CAST)map_info->master_osspec_hdl;
    
    if( wind_handle->magic != MASTER_HANDLE_MAGIC )
    {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeMapMasterWindow: ERROR: Invalid window "
        "handle memory");
#else
        printk("vmeMapMasterWindow: ERROR: Invalid window handle memory\n");
#endif
        return (GEF_STATUS_DRIVER_ERR);
    }
    
    // We don't fill in the virt_addr field. This will be done later in the OSA 
    // by a call to mmap using the phys_addr we return.  Just make sure virt_addr is
    // a 0
    map_info->virt_addr = 0;
    
    // Get phys_addr from specified Master window
    map_info->phys_addr = (GEF_UINT64) (GEF_OS_CAST) (wind_handle->window->physAddr 
                         + wind_handle->offset);
    
    return(status);
}

//----------------------------------------------------------------------------
//  vmeMapSlaveWindow()
//    Return the physical address of the specified Slave window.
//----------------------------------------------------------------------------
int
vmeMapSlaveWindow(GEF_VME_MAP_SLAVE_INFO *map_info)
{
    int status = GEF_SUCCESS;
    vmeSlaveHandle_t* wind_handle = (vmeSlaveHandle_t*) (GEF_OS_CAST)map_info->slave_osspec_hdl;
    
    if( wind_handle->magic != SLAVE_HANDLE_MAGIC )
    {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeMapSlaveWindow: ERROR: Invalid window "
        "handle memory");
#else
        printk("vmeMapSlaveWindow: ERROR: Invalid window handle memory\n");
#endif
        return (GEF_STATUS_DRIVER_ERR);
    }
    // We don't fill in the virt_addr field. This will be done later in the OSA 
    // by a call to mmap using the phys_addr we return.  Just make sure virt_addr is
    // a 0
    map_info->virt_addr = 0;
    
    // Get phys_addr from specified Slave window
    map_info->phys_addr = (GEF_UINT64) (GEF_OS_CAST) (wind_handle->window->physAddr 
                         + wind_handle->offset);
    
    return(status);
}

//----------------------------------------------------------------------------
//  dmaHandleListAdd
//    Add handle for contiguous kernel-allocated DMA buffer to list.
//    Expects dma_buffer_lock acquired.
//----------------------------------------------------------------------------

static int dmaHandleListAdd(vmeDmaHandle_t *dmahandle)
{
    if (NULL == dmahandle) 
    {
        return(GEF_STATUS_DRIVER_ERR);
    }
    
    if (NULL == dma_handle_list)
    {
        dma_handle_list = dmahandle;
        dmahandle->next = NULL;
    } else
    {
        dmahandle->next = dma_handle_list;
        dma_handle_list = dmahandle;
    }
    
    return GEF_SUCCESS;
}

//----------------------------------------------------------------------------
//  dmaHandleListRemove
//    Remove handle for contiguous kernel-allocated DMA buffer to list.
//    Expects dma_buffer_lock acquired.
//----------------------------------------------------------------------------

static int dmaHandleListRemove(vmeDmaHandle_t *dmahandle)
{
    GEF_STATUS status;
    vmeDmaHandle_t *search_hdl;
    vmeDmaHandle_t *prev_hdl;
    GEF_BOOL found;
    
    if (NULL == dma_handle_list) 
    {
        return(GEF_STATUS_DRIVER_ERR);
    }
    
    search_hdl = dma_handle_list;
    prev_hdl = dma_handle_list;
    
    found = GEF_FALSE;
    while ((GEF_FALSE == found) && (search_hdl != NULL))
    {
        if ((search_hdl == dmahandle))
        {
            found = GEF_TRUE;
        } else
        {
            prev_hdl = search_hdl;
            search_hdl = search_hdl->next;
        }
    }
    
    if (GEF_TRUE == found)
    {
        // Check for head of list
        if (prev_hdl == search_hdl)
        {
            dma_handle_list = search_hdl->next;
        }
        else 
        {
            prev_hdl->next = search_hdl->next;    
        }
        memset(search_hdl, 0, sizeof (vmeDmaHandle_t));
#ifdef USE_RTOS
        rt_free(search_hdl);
#else
        kfree(search_hdl);
#endif
        search_hdl = NULL;
        
        
        status = GEF_SUCCESS;
    } else
    {
        status = GEF_STATUS_DRIVER_ERR;
    }
    
    return (status);
}

//----------------------------------------------------------------------------
//  vmeAllocDmaBuf()
//    Allocate contiguous DMA buffer.
//----------------------------------------------------------------------------

int vmeAllocDmaBuf(GEF_VME_DRV_ALLOC_DMA_BUF_INFO *dma_buf_info) {
    
    vmeDmaHandle_t *dmahandle = NULL;
	struct page *page;
    GEF_STATUS status;

    if (NULL == dma_buf_info)
    {
        return(GEF_STATUS_DRIVER_ERR);
    }

    if(down_interruptible(&dma_buffer_lock))
    {
        return(GEF_STATUS_DRIVER_ERR);
    }
#ifdef USE_RTOS
    dmahandle = (vmeDmaHandle_t *)rt_malloc(sizeof(vmeDmaHandle_t));
#else
    dmahandle = (vmeDmaHandle_t *)kmalloc(sizeof (vmeDmaHandle_t), GFP_KERNEL);
#endif

    if (NULL == dmahandle)
    {
        up(&dma_buffer_lock);
        return(GEF_STATUS_NO_MEM);
    }

    memset(dmahandle, 0, sizeof(vmeDmaHandle_t));
    
    dma_buf_info->dma_osspec_hdl = (GEF_UINT64) (GEF_OS_CAST) dmahandle;
    
    dmahandle->size = dma_buf_info->size;

    dmahandle->vptr = pci_alloc_consistent(vme_pci_dev, dma_buf_info->size,
        &dmahandle->resource);

    if ((NULL == dmahandle->vptr)) 
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "pci_alloc_consistent failed\n");
        up(&dma_buffer_lock);
        return(GEF_STATUS_NO_MEM);
    } 

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "pci_alloc_consistent virtual buffer pointer %#lx\n",(unsigned long)dmahandle->vptr);

    for (page = virt_to_page(dmahandle->vptr);
         page <= virt_to_page(dmahandle->vptr + dma_buf_info->size - 1);
         ++page)
    {
        SetPageReserved(page);		
    }
    
    dmahandle->phys_addr = (void *) virt_to_phys(dmahandle->vptr);
    dmahandle->pci_addr = dmahandle->phys_addr;

    // Set return data for user space
    dma_buf_info->virt_addr = 0;
    dma_buf_info->phys_addr = (GEF_UINT64) (GEF_OS_CAST) dmahandle->phys_addr;

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "%#x byte DMA buffer allocated with physical "
        "addr %#lx pci addr %#lx resource addr %#lx\n", dma_buf_info->size,
        (unsigned long) dmahandle->phys_addr,
        (unsigned long) dmahandle->pci_addr,
        (unsigned long) dmahandle->resource);

	dmahandle->magic = VME_DMA_MAGIC;
    
    // Expects dma_buffer_lock acquired
    status = dmaHandleListAdd(dmahandle);

    up(&dma_buffer_lock);

    return (status);
    
}

//----------------------------------------------------------------------------
//  vmeFreeDmaBuf()
//    Free contiguous DMA buffer.
//----------------------------------------------------------------------------

int vmeFreeDmaBuf(GEF_VME_DRV_FREE_DMA_BUF_INFO *dma_buf_info) {

    GEF_STATUS status;
    struct page *page;
    vmeDmaHandle_t *dmahandle = NULL;
    
    if (NULL == dma_buf_info)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeFreeDmaBuf failure: NULL == dma_buf_info\n"); 
        return(GEF_STATUS_DRIVER_ERR);
    }
    
    dmahandle = (vmeDmaHandle_t *) (GEF_OS_CAST) dma_buf_info->dma_osspec_hdl; 
    
    if (NULL == dmahandle)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeFreeDmaBuf failure: NULL == dmahandle\n"); 
        return(GEF_STATUS_DRIVER_ERR);
    }    
    
    if (VME_DMA_MAGIC != dmahandle->magic)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeFreeDmaBuf failure: VME_DMA_MAGIC != dmahandle->magic\n"); 
        return(GEF_STATUS_DRIVER_ERR);
    }
    
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Freeing %#x byte DMA buffer allocated with physical "
        "addr %#lx pci addr %#lx resource addr %#lx\n", dmahandle->size,
        (unsigned long) dmahandle->phys_addr,
        (unsigned long) dmahandle->pci_addr,
        (unsigned long) dmahandle->resource);
     
    if(down_interruptible(&dma_buffer_lock))
    {
        return(GEF_STATUS_DRIVER_ERR);
    }
    
    for (page = virt_to_page(dmahandle->vptr);
         page <= virt_to_page(dmahandle->vptr + dmahandle->size - 1);
         ++page)              
    {
        
        ClearPageReserved(page);
    }
    
    pci_free_consistent(vme_pci_dev, dmahandle->size,
				    dmahandle->vptr, 
                    dmahandle->resource);
  

    // Expects dma_buffer_lock acquired
    status = dmaHandleListRemove(dmahandle);
    
    up(&dma_buffer_lock);
    
    return (status);
    
}

//----------------------------------------------------------------------------
//  vmeFreeDmaHandles()
//    Frees DMA buffers and handles on driver shutdown.
//----------------------------------------------------------------------------

int vmeFreeDmaHandles(void) 
{
    GEF_STATUS status = GEF_SUCCESS;
    GEF_VME_DRV_FREE_DMA_BUF_INFO dma_buf_info;

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeFreeDmaHandles removing DMA buffers and handles.\n");
    
    while ((NULL != dma_handle_list) && (GEF_SUCCESS == status))
    {
        dma_buf_info.dma_osspec_hdl = (GEF_UINT64) (GEF_OS_CAST) dma_handle_list;
        status = vmeFreeDmaBuf(&dma_buf_info);

        if (GEF_SUCCESS != status)
        {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "VME:DMA handle removal failure: 0x%x", status);
#else
            printk("VME:DMA handle removal failure: 0x%x\n",status);
#endif
        }
    }

    return(GEF_SUCCESS);
}

int
vmeAllocSlaveMemory(GEF_UINT32 size, int window, GEF_UINT32 vme_addrL, GEF_UINT32 vme_addrU)
{
    struct page *page;

    if(down_interruptible(&slave_window_lock))
    {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeAllocSlaveMemory: Failed to lock mutex "
        "for window %d", window);
#else
        printk("vmeAllocSlaveMemory: Failed to lock mutex for window %d\n", window);
#endif
        return(GEF_STATUS_DRIVER_ERR);
    }

    vsw[window].number = window;
    vsw[window].vmeL = vme_addrL;
    vsw[window].vmeU = vme_addrU;
    vsw[window].size = size;

    vsw[window].virtAddr = pci_alloc_consistent(vme_pci_dev, size, &vsw[window].physAddrRes);
    if(NULL == vsw[window].virtAddr) {
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "vmeAllocSlaveMemory: Failed to allocate "
        "virtual address of size 0x%x", size);
#else
        printk("vmeAllocSlaveMemory: Failed to allocate virtual address of size 0x%x\n", size);
#endif
        up(&slave_window_lock);
        return (GEF_STATUS_NO_MEM);
    }

    for(page = virt_to_page(vsw[window].virtAddr); page <= virt_to_page(vsw[window].virtAddr+size-1);++page) {
        SetPageReserved(page);
    }

    vsw[window].physAddr = (void*)virt_to_phys(vsw[window].virtAddr);

    printSlaveWindow(window);

    up(&slave_window_lock);

    return (GEF_SUCCESS);
}

int
vmeFreeSlaveMemory(int window)
{
    struct page *page;

    for(page = virt_to_page(vsw[window].virtAddr); page <= virt_to_page(vsw[window].virtAddr+vsw[window].size-1);++page) {
        ClearPageReserved(page);
    }

    pci_free_consistent(vme_pci_dev, vsw[window].size, vsw[window].virtAddr, vsw[window].physAddrRes);

    memset(&vsw[window], 0, sizeof(vmeSlaveWindow_t));

    return (GEF_SUCCESS);
}
