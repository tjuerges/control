/*
 * File:
 *    tsi148.c
 *
 * Description:
 *    Source code to the Tempe driver.
 *
 * Copyright:
 *    Copyright 2007 GE Fanuc Embedded Systems, Inc.
 *
 * License:
 *    This file is proprietary to GE Fanuc Embedded Systems, Inc.
 *
 *
 * $Date$
 * $Rev$
 */


/*
Modified to support Linux 2.6 x86 platforms
*/

//------------------------------------------------------------------------------  
//title: Tundra TSI148 (Tempe) PCI-VME Kernel Driver
//version: Linux 3.1
//date: February 2004
//designer: Tom Armistead
//programmer: Tom Armistead
//platform: Linux 2.4.x
//------------------------------------------------------------------------------  
//  Purpose: Provide interface to the Tempe chip.
//  Docs:                                  
//------------------------------------------------------------------------------  
//-----------------------------------------------------------------------------
#include <linux/version.h>
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,5,0)
#define MODULE
#endif

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,18)
#include <linux/config.h>
#endif

#ifdef CONFIG_SMP
#define __SMP__
#endif

#ifdef CONFIG_MODVERSIONS
  #define MODVERSIONS

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,5,0)
#include <linux/modversions.h>
#endif

#endif

#if LINUX_VERSION_CODE > KERNEL_VERSION(2,5,0)
#include <linux/interrupt.h>
#endif

#include <linux/module.h>
#include <linux/mm.h>
#include <linux/types.h>
#include <linux/errno.h>
#include <linux/proc_fs.h>
#include <linux/pci.h>
#include <linux/poll.h>
#include <linux/delay.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include <asm/msr.h>
#include "gef/gefcmn_vme_framework.h"
#include "gef/gefcmn_vme_errno.h"
#include "gef/gefcmn_vme_tempe.h"
#include "vmedrv.h"
#include "tsi148.h"
#include "gef_kernel_abs.h"

#ifdef USE_RTOS
#include <rtai_types.h>
#include <rtai_sem.h>
#endif

//----------------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------------
int tempeDisableIrq(GEF_VME_INT_INFO *);
int tempeEnableIrq(GEF_VME_INT_INFO *);
extern const int vmechip_revision;
extern const int vmechip_devid;
extern const int vmechip_irq;
extern int vmechipIrqOverHeadTicks;
extern char *vmechip_baseaddr;
extern int vme_syscon;
extern unsigned int out_image_va[];
extern GEF_UINT32 vme_irqlog[GEF_VME_INT_LEVEL_LAST][0x100];
extern GEF_UINT32 vme_irq_waitcount[];
extern GEF_UINT8  vme_vec_flags[GEF_VME_INT_LEVEL_LAST][0x100];
extern GEF_VME_BERR_INFO vme_irq_berr_info[];
extern GEF_VME_INT_INFO vme_irq_int_info[];
extern GEF_UINT8  vme_berr_write_buf;
extern GEF_UINT8  vme_berr_read_buf;
extern GEF_UINT8  dma_in_progress;
extern GEF_UINT8  dma_berr_failure;
extern struct pci_dev *vme_pci_dev;
extern vmeMasterWindow_t vmw[VME_MAX_WINDOWS];
extern vmeSlaveWindow_t vsw[VME_MAX_WINDOWS];
extern int vmechip_bus;
extern struct resource *vmepcimem;
extern int (*g_pProbeBERRFunc)(GEF_VME_BERR_INFO *pBerrInfo);

extern unsigned int pci_bus_mem_base_phys(int bus);
int tempeSetOutBound(vmeOutWindowCfg_t *vmeOut);
int tempeSetInBound(vmeInWindowCfg_t *vmeIn);
extern int vmeAllocSlaveMemory(GEF_UINT32 size, int window, GEF_UINT32 vme_addrL, GEF_UINT32 vme_addrU);
extern int vmeFreeSlaveMemory(int window);

//----------------------------------------------------------------------------
// Types
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Vars and Defines
//----------------------------------------------------------------------------
#ifdef USE_RTOS
extern irqreturn_t linuxPostIRQHandler(
    int irq,
    void* devId,
    struct pt_regs* regs);
extern RT_TASK* dma_queue[];
#else
extern wait_queue_head_t dma_queue[];
#endif
extern wait_queue_head_t lm_queue;
extern wait_queue_head_t mbox_queue;
#ifdef USE_RTOS
extern RT_TASK* irq_queue[];
#else
extern wait_queue_head_t irq_queue[];
#endif

extern	unsigned int vmeGetTime(void);
extern	void	vmeSyncData(void);
extern	void	vmeFlushLine( char * );
extern	int	tb_speed;

unsigned int	tempeIrqTime;
unsigned int	tempeDmaIrqTime[2];
unsigned int	tempeLmEvent;
extern unsigned int vme_init_flags;
static int vown_count = 0;

spinlock_t tempe_lock;
DECLARE_MUTEX(ctl_lock);

#ifdef USE_RTOS
extern RTIME SEMAPHORE_TIMEOUT;
extern SEM vown_lock;
#else
static DECLARE_MUTEX(vown_lock);
#endif

#define GEF_VME_MAX_WAIT_DHB 1000
#define WAIT_DHB(x)                 \
{                               \
    int count = x;              \
    while(count>0){ --count; }  \
}


//----------------------------------------------------------------------------
//  add64hi - calculate upper 32 bits of 64 bit addition operation.
//----------------------------------------------------------------------------
unsigned int
add64hi(
unsigned int lo0,
unsigned int hi0,
unsigned int lo1,
unsigned int hi1
)
{
   if((lo1 + lo0) < lo1){
      return(hi0 + hi1 + 1);
   }
   return(hi0 + hi1);
}

//----------------------------------------------------------------------------
//  sub64hi - calculate upper 32 bits of 64 bit subtraction operation
//----------------------------------------------------------------------------
int
sub64hi(
unsigned int lo0,
unsigned int hi0,
unsigned int lo1,
unsigned int hi1
)
{
   if(lo0 < lo1){
      return(hi0 - hi1 - 1);
   }
   return(hi0 - hi1);
}


//----------------------------------------------------------------------------
//  tempe_procinfo()
//----------------------------------------------------------------------------
int tempe_procinfo(char *buf, char **start, off_t fpos, int lenght, int *eof, void *data)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  char *p;
  unsigned int x;

  p = buf;

  // Display outbound decoders
  for (x=0;x<8;x+=1) {
    p += sprintf(p,"O%d: %08X %08X:%08X %08X:%08X: %08X:%08X\n",
                 x,
                 longswap(ioread32(&pTempe->lcsr.outboundTranslation[x].otat)),
                 longswap(ioread32(&pTempe->lcsr.outboundTranslation[x].otsau)),
                 longswap(ioread32(&pTempe->lcsr.outboundTranslation[x].otsal)),
                 longswap(ioread32(&pTempe->lcsr.outboundTranslation[x].oteau)),
                 longswap(ioread32(&pTempe->lcsr.outboundTranslation[x].oteal)),
                 longswap(ioread32(&pTempe->lcsr.outboundTranslation[x].otofu)),
                 longswap(ioread32(&pTempe->lcsr.outboundTranslation[x].otofl))
                 );
  }  

  p += sprintf(p,"\n");  

  *eof = 1;
  return p - buf;
}

//-----------------------------------------------------------------------------
// Function   : tempeSetupMasterAttribute
// Description: helper function for calculating attribute register contents for
//              master window.
//-----------------------------------------------------------------------------
static
int
tempeSetupMasterAttribute(
addressMode_t addrSpace, 
int userAccessType, 
int dataAccessType,
dataWidth_t maxDataWidth,
int xferProtocol,
vme2esstRate_t xferRate2esst
)
{
  int tempCtl = 0;

  // Validate & initialize address space field.
  switch(addrSpace){
    case VME_A16:
      tempCtl |= 0x0;
      break;
    case VME_A24:
      tempCtl |= 0x1;
      break;
    case VME_A32:
      tempCtl |= 0x2;
      break;
    case VME_A64:
      tempCtl |= 0x4;
      break;
    case VME_CRCSR:
      tempCtl |= 0x5;
      break;
    case VME_USER1:
      tempCtl |= 0x8;
      break;
    case VME_USER2:
      tempCtl |= 0x9;
      break;
    case VME_USER3:
      tempCtl |= 0xA;
      break;
    case VME_USER4:
      tempCtl |= 0xB;
      break;
    default:
      return(-EINVAL);
  }

  // Setup CTL register.
  if(userAccessType & VME_SUPER)
    tempCtl |= 0x0020;
  if(dataAccessType & VME_PROG)
    tempCtl |= 0x0010;
  if(maxDataWidth == VME_D16)
    tempCtl |= 0x0000;
  if(maxDataWidth == VME_D32)
    tempCtl |= 0x0040;
  switch(xferProtocol){
    case VME_SCT:
      tempCtl |= 0x000;
      break;
    case VME_BLT:
      tempCtl |= 0x100;
      break;
    case VME_MBLT:
      tempCtl |= 0x200;
      break;
    case VME_2eVME:
      tempCtl |= 0x300;
      break;
    case VME_2eSST:
      tempCtl |= 0x400;
      break;
    case VME_2eSSTB:
      tempCtl |= 0x500;
      break;
  }
  switch(xferRate2esst){
    case VME_SSTNONE:
    case VME_SST160:
      tempCtl |= 0x0000;
      break;
    case VME_SST267:
      tempCtl |= 0x800;
      break;
    case VME_SST320:
      tempCtl |= 0x1000;
      break;
  }

  return(tempCtl);
}

//----------------------------------------------------------------------------
//  tempeBusErrorChk()
//  Return zero if no VME bus error has occured, 1 otherwise.
//  Optionally, clear bus error status.
//----------------------------------------------------------------------------
int
tempeBusErrorChk(int clrflag)
{
  unsigned long irqflags;

  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  int tmp;

#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
  spin_lock_irqsave(&tempe_lock, irqflags);
#endif

    tmp = longswap(ioread32(&pTempe->lcsr.veat));
    if (tmp & 0x80000000) {  // VES is Set
      if(clrflag) {
        iowrite32(longswap(0x20000000),&pTempe->lcsr.veat);
      }

#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
      spin_unlock_irqrestore(&tempe_lock, irqflags);
#endif
      return(1);
    }

#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
  spin_unlock_irqrestore(&tempe_lock, irqflags);
#endif
  return(0);
}


//-----------------------------------------------------------------------------
// Function   : DMA_tempe_irqhandler
// Description: Saves DMA completion timestamp and then wakes up DMA queue
//-----------------------------------------------------------------------------
void DMA_tempe_irqhandler(void)
{
    tempeDmaIrqTime[0] = tempeIrqTime;
#ifdef USE_RTOS
    RT_TASK* task = dma_queue[0];
    if(task != 0)
    {
        rt_task_resume(task);
    }
#else
    wake_up(&dma_queue[0]);
#endif
}

//-----------------------------------------------------------------------------
// Function   : LERR_tempe_irqhandler
// Description: Display error & status message when LERR (PCI) exception
//    interrupt occurs.
//-----------------------------------------------------------------------------
void LERR_tempe_irqhandler(void)
{
    GEF_UINT8 level;
    unsigned char iackvec;
    GEF_VME_INT_INFO int_info;
    tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;

    // Disable the VME LERR interrupt
    int_info.int_level = GEF_VME_INT_LERR;
    int_info.int_vector = 0;
    tempeDisableIrq(&int_info);

    level = DRV_IRQ_LEVEL_LERR; 
    iackvec = 0;

    if ((vme_vec_flags[level][iackvec] & GEF_IRQ_WAIT) == 0) {
#ifdef USE_RTOS
        rtlogRecord_t logRecord;
        RTLOG_ERROR("gefvme_rtos", "VME LERR: Target abort encountered on the "
            "PCI Bus.");
        RTLOG_ERROR("gefvme_rtos", " VME PCI Exception at address: "
            "0x%08x:%08x, attributes: %08x",
#else
        printk("VME LERR: Target abort encountered on the PCI Bus.\n");
        printk(" VME PCI Exception at address: 0x%08x:%08x, attributes: %08x\n",
#endif
            longswap(ioread32(&pTempe->lcsr.edpau)), longswap(ioread32(&pTempe->lcsr.edpal)),
            longswap(ioread32(&pTempe->lcsr.edpat)));
#ifdef USE_RTOS
        RTLOG_ERROR("gefvme_rtos", " PCI-X attribute reg: %08x, PCI-X split "
            "completion reg: %08x",
#else
        printk(" PCI-X attribute reg: %08x, PCI-X split completion reg: %08x\n",
#endif
            longswap(ioread32(&pTempe->lcsr.edpxa)), longswap(ioread32(&pTempe->lcsr.edpxs)));
        iowrite32(longswap(0x20000000),&pTempe->lcsr.edpat);
        vmeSyncData();
    } else {
        
        vme_irqlog[level][iackvec]++;
        vme_vec_flags[level][iackvec] &= ~(IRQ_TIMEDOUT);
#ifdef USE_RTOS
        RT_TASK* task = irq_queue[level];
        if(task != 0)
        {
            rt_task_resume(task);
        }
#else
        wake_up_interruptible(&irq_queue[level]);
#endif
    }

    int_info.int_level = GEF_VME_INT_LERR;
    int_info.int_vector = 0;
    tempeEnableIrq(&int_info);

}

//-----------------------------------------------------------------------------
// Function   : VERR_tempe_irqhandler
// Description:  Display error & status when VME error interrupt occurs.
//-----------------------------------------------------------------------------
void VERR_tempe_irqhandler(void)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  GEF_UINT8 level;
  unsigned char iackvec;
  GEF_VME_INT_INFO int_info;
  GEF_UINT32 vmeExceptionAttribute;

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, " VME Exception at address: 0x%08x:%08x, attributes: %08x\n",
     longswap(ioread32(&pTempe->lcsr.veau)), longswap(ioread32(&pTempe->lcsr.veal)), 
     longswap(ioread32(&pTempe->lcsr.veat)));

  // Disable the VME BERR interrupt
  int_info.int_level = GEF_VME_INT_BERR;
  int_info.int_vector = 0;
  tempeDisableIrq(&int_info);

  // Retrieve BERR info from Tempe
  vme_irq_berr_info[vme_berr_write_buf].upper = longswap(ioread32(&pTempe->lcsr.veau));
  vme_irq_berr_info[vme_berr_write_buf].lower = longswap(ioread32(&pTempe->lcsr.veal));

  vmeExceptionAttribute = longswap(ioread32(&pTempe->lcsr.veat));

  // Clear pending VME Bus Error
  iowrite32(longswap(TEMPE_VEAT_VESCL),&pTempe->lcsr.veat);
  vmeSyncData();
  
  vme_irq_berr_info[vme_berr_write_buf].addr_mod = (GEF_VME_ADDR_MOD)
      ((vmeExceptionAttribute & TEMPE_VEAT_AM_MASK) >> 8);
  vme_irq_berr_info[vme_berr_write_buf].extended_addr_mod = (GEF_VME_EXTENDED_ADDR_MOD)
      (vmeExceptionAttribute & TEMPE_VEAT_XAM_MASK);
  
  if (vmeExceptionAttribute & TEMPE_VEAT_IACK) {
      vme_irq_berr_info[vme_berr_write_buf].iack_signal = GEF_TRUE;
  }
  else {
      vme_irq_berr_info[vme_berr_write_buf].iack_signal = GEF_FALSE;
  }
  
  if (vmeExceptionAttribute & TEMPE_VEAT_DS1) {
      vme_irq_berr_info[vme_berr_write_buf].ds1_signal = GEF_TRUE;
  }
  else {
      vme_irq_berr_info[vme_berr_write_buf].ds1_signal = GEF_FALSE;
  }
  
  if (vmeExceptionAttribute & TEMPE_VEAT_DS0) {
      vme_irq_berr_info[vme_berr_write_buf].ds0_signal = GEF_TRUE;
  }
  else {
      vme_irq_berr_info[vme_berr_write_buf].ds0_signal = GEF_FALSE;
  }
  
  if (vmeExceptionAttribute & TEMPE_VEAT_LWORD) {
      vme_irq_berr_info[vme_berr_write_buf].lword_signal = GEF_TRUE;
  }
  else {
      vme_irq_berr_info[vme_berr_write_buf].lword_signal = GEF_FALSE;
  }
  
  if (vmeExceptionAttribute & TEMPE_VEAT_2eOT) {
      vme_irq_berr_info[vme_berr_write_buf].vme_2eOT = GEF_TRUE;
  }
  else {
      vme_irq_berr_info[vme_berr_write_buf].vme_2eOT = GEF_FALSE;
  }
  
  if (vmeExceptionAttribute & TEMPE_VEAT_2eST) {
      vme_irq_berr_info[vme_berr_write_buf].vme_2eST = GEF_TRUE;
  }
  else {
      vme_irq_berr_info[vme_berr_write_buf].vme_2eST = GEF_FALSE;
  }
  
  level = DRV_IRQ_LEVEL_BERR; 
  iackvec = 0;

  do
  {
       /* See if the probe is active */
       if (g_pProbeBERRFunc != NULL)
       {
            /* If it is, call that guy */
            if (g_pProbeBERRFunc(&(vme_irq_berr_info[vme_berr_write_buf])) == 0)
            {
                 /* If this guy returns 0, then he handled the BERR. */
                 break;
            }
            
            /* Otherwise, we'll need to handle it normally, just fall through */
       }
       
       if ((vme_vec_flags[level][iackvec] & GEF_IRQ_WAIT) == 0) {
#ifdef USE_RTOS
             rtlogRecord_t logRecord;
             RTLOG_ERROR("gefvme_rtos","VME Bus Error occurred: ADDR "
               "0x%08x AM 0x%08x.",
#else
           printk("VME Bus Error occurred: ADDR 0x%08x AM 0x%08x.\n",
#endif
               vme_irq_berr_info[vme_berr_write_buf].lower,vme_irq_berr_info[vme_berr_write_buf].addr_mod);
       } else { 
            
            vme_irqlog[level][iackvec]++;
            vme_vec_flags[level][iackvec] &= ~(IRQ_TIMEDOUT);
#ifdef USE_RTOS
            RT_TASK* task = irq_queue[level];
            if(task != 0)
            {
                rt_task_resume(task);
            }
#else
            wake_up_interruptible(&irq_queue[level]);
#endif
       }
  }
  while (0);

  // Update double-buffer pointer for BERR data
  if (vme_berr_write_buf == 0) {
      vme_berr_write_buf = 1;
      vme_berr_read_buf = 0;
  } else {
      vme_berr_write_buf = 0;
      vme_berr_read_buf = 1;
  }
  
  if (GEF_TRUE == dma_in_progress) {
      dma_berr_failure = GEF_TRUE;
      DMA_tempe_irqhandler();
  }
  
  int_info.int_level = GEF_VME_INT_BERR;
  int_info.int_vector = 0;
  tempeEnableIrq(&int_info);

}

//-----------------------------------------------------------------------------
// Function   : SYSFAIL_tempe_irqhandler
// Inputs     : void
// Outputs    : void
// Description: 
//-----------------------------------------------------------------------------
void SYSFAIL_tempe_irqhandler(void)
{
    GEF_UINT8 level;
    unsigned char iackvec;
    GEF_VME_INT_INFO int_info;

    // Disable the VME SYSFAIL interrupt. 
    int_info.int_level = GEF_VME_INT_SYSFAIL;
    int_info.int_vector = 0;
    tempeDisableIrq(&int_info);

    level = DRV_IRQ_LEVEL_SYSFAIL; 
    iackvec = 0;

    if ((vme_vec_flags[level][iackvec] & GEF_IRQ_WAIT) == 0) {
        
#ifdef USE_RTOS
             rtlogRecord_t logRecord;
             RTLOG_ERROR("gefvme_rtos", "VME SYSFAIL: SYSFAIL asserted.");
#else
        printk("VME SYSFAIL: SYSFAIL asserted.\n");
#endif
    } else {
        
        vme_irqlog[level][iackvec]++;
        vme_vec_flags[level][iackvec] &= ~(IRQ_TIMEDOUT);
#ifdef USE_RTOS
        RT_TASK* task = irq_queue[level];
        if(task != 0)
        {
            rt_task_resume(task);
        }
#else
        wake_up_interruptible(&irq_queue[level]);
#endif
    }
}

//-----------------------------------------------------------------------------
// Function   : ACFAIL_tempe_irqhandler
// Inputs     : void
// Outputs    : void
// Description: 
//-----------------------------------------------------------------------------
void ACFAIL_tempe_irqhandler(void)
{
    GEF_UINT8 level;
    unsigned char iackvec;
    GEF_VME_INT_INFO int_info;

    // Disable the VME ACFAIL interrupt. 
    int_info.int_level = GEF_VME_INT_ACFAIL;
    int_info.int_vector = 0;
    tempeDisableIrq(&int_info);

    level = DRV_IRQ_LEVEL_ACFAIL; 
    iackvec = 0;

    if ((vme_vec_flags[level][iackvec] & GEF_IRQ_WAIT) == 0) {
        
#ifdef USE_RTOS
             rtlogRecord_t logRecord;
             RTLOG_ERROR("gefvme_rtos", "VME ACFAIL: Imminent Power Failure "
               "encountered.");
#else
        printk("VME ACFAIL: Imminent Power Failure encountered.\n");
#endif
    } else {
        
        vme_irqlog[level][iackvec]++;
        vme_vec_flags[level][iackvec] &= ~(IRQ_TIMEDOUT);
#ifdef USE_RTOS
        RT_TASK* task = irq_queue[level];
        if(task != 0)
        {
            rt_task_resume(task);
        }
#else
        wake_up_interruptible(&irq_queue[level]);
#endif
    }
}

//-----------------------------------------------------------------------------
// Function   : MB_tempe_irqhandler
// Description: Wake up mail box queue.
//-----------------------------------------------------------------------------
void MB_tempe_irqhandler(long mboxMask)
{

    GEF_UINT8 level;
    unsigned char iackvec;
    GEF_VME_INT_INFO int_info;
    GEF_UINT32 mbData = 0;
    tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;

    if (NULL == vmechip_baseaddr) {
        return;
    }

    // Determine if initialization calibration is being performed
    if (vmechipIrqOverHeadTicks == 0) {
        return;
    }
    
    // Disable the VME mailbox interrupt and read the data
    switch (mboxMask) {
    case 1: 
        int_info.int_level = GEF_VME_INT_MBOX0; 
        level = DRV_IRQ_LEVEL_MBOX0;
        mbData = longswap(ioread32(&pTempe->gcsr.mbox[0]));
        break;
    case 2: 
        int_info.int_level = GEF_VME_INT_MBOX1;
        level = DRV_IRQ_LEVEL_MBOX1;
        mbData = longswap(ioread32(&pTempe->gcsr.mbox[1]));
        break;
    case 4: 
        int_info.int_level = GEF_VME_INT_MBOX2; 
        level = DRV_IRQ_LEVEL_MBOX2;
        mbData = longswap(ioread32(&pTempe->gcsr.mbox[2]));
        break; 
    case 8: 
        int_info.int_level = GEF_VME_INT_MBOX3; 
        level = DRV_IRQ_LEVEL_MBOX3;
        mbData = longswap(ioread32(&pTempe->gcsr.mbox[3]));
        break; 
    default:
        return;
    }
    
    int_info.int_vector = 0;
    tempeDisableIrq(&int_info);
    
    iackvec = 0;       
    vme_irqlog[level][iackvec]++;
    vme_vec_flags[level][iackvec] &= ~(IRQ_TIMEDOUT);
    vme_irq_int_info[level].int_vector = mbData;
#ifdef USE_RTOS
        RT_TASK* task = irq_queue[level];
        if(task != 0)
        {
            rt_task_resume(task);
        }
#else
    wake_up_interruptible(&irq_queue[level]);
#endif
    
    tempeEnableIrq(&int_info);
}

//-----------------------------------------------------------------------------
// Function   : LM_tempe_irqhandler
// Description: Wake up location monitor
//-----------------------------------------------------------------------------
void LM_tempe_irqhandler(long lmMask)
{
    GEF_UINT8 level;
    unsigned char iackvec;
    GEF_VME_INT_INFO int_info;
    
    // Disable the VME LM interrupt
    switch (lmMask) {
    case 1: 
        int_info.int_level = GEF_VME_INT_LM0; 
        level = DRV_IRQ_LEVEL_LM0;
        break;
    case 2: 
        int_info.int_level = GEF_VME_INT_LM1;
        level = DRV_IRQ_LEVEL_LM1;
        break;
    case 4: 
        int_info.int_level = GEF_VME_INT_LM2; 
        level = DRV_IRQ_LEVEL_LM2;
        break; 
    case 8: 
        int_info.int_level = GEF_VME_INT_LM3; 
        level = DRV_IRQ_LEVEL_LM3;
        break; 
    default:
        return;
    }
    
    int_info.int_vector = 0;
    tempeDisableIrq(&int_info);
    
    iackvec = 0;       
    vme_irqlog[level][iackvec]++;
    vme_vec_flags[level][iackvec] &= ~(IRQ_TIMEDOUT);
#ifdef USE_RTOS
    RT_TASK* task = irq_queue[level];
    if(task != 0)
    {
        rt_task_resume(task);
    }
#else
    wake_up_interruptible(&irq_queue[level]);
#endif

    tempeEnableIrq(&int_info);

}

//-----------------------------------------------------------------------------
// Function   : VIRQ_tempe_irqhandler
// Description: Record the level & vector from the VME bus interrupt.
//-----------------------------------------------------------------------------
void VIRQ_tempe_irqhandler(long virqMask)
{
    tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
    unsigned char iackvec;
    int i;
    GEF_UINT8 level = 0;
    GEF_VME_INT_INFO int_info;
    
    for(i = 7; i > 0; i--){
        if(virqMask & (1 << i)){
            iackvec = ioread8(&pTempe->lcsr.viack[(i*4)+3]);
            switch (i) {
                
            case DRV_IRQ_LEVEL_VIRQ1: level = GEF_VME_INT_VIRQ1; break;
            case DRV_IRQ_LEVEL_VIRQ2: level = GEF_VME_INT_VIRQ2; break;
            case DRV_IRQ_LEVEL_VIRQ3: level = GEF_VME_INT_VIRQ3; break;
            case DRV_IRQ_LEVEL_VIRQ4: level = GEF_VME_INT_VIRQ4; break;
            case DRV_IRQ_LEVEL_VIRQ5: level = GEF_VME_INT_VIRQ5; break;
            case DRV_IRQ_LEVEL_VIRQ6: level = GEF_VME_INT_VIRQ6; break;
            case DRV_IRQ_LEVEL_VIRQ7: level = GEF_VME_INT_VIRQ7; break;
            }
                       
            int_info.int_level = level;
            int_info.int_vector = iackvec;

            if ((vme_vec_flags[level][iackvec] & GEF_IRQ_WAIT) == 0) {
                
#ifdef USE_RTOS
             rtlogRecord_t logRecord;
             RTLOG_ERROR("gefvme_rtos", "VME: Spurious Interrupt received for "
               "level 0x%x vector 0x%x.",
#else
                printk("VME: Spurious Interrupt received for level 0x%x vector 0x%x.\n",
#endif
                    level,iackvec);
                
            } else
            {
                
                tempeDisableIrq(&int_info);
                vme_irqlog[i][iackvec]++;
                vme_vec_flags[i][iackvec] &= ~(IRQ_TIMEDOUT);
#ifdef USE_RTOS
                RT_TASK* task = irq_queue[level];
                if(task != 0)
                {
                    rt_task_resume(task);
                }
#else
                wake_up_interruptible(&irq_queue[i]);
#endif
            }
        }
    }
}

//-----------------------------------------------------------------------------
// Function   : tempe_irqhandler
// Description: Top level interrupt handler.  Clears appropriate interrupt
// status bits and then calls appropriate sub handler(s).
//-----------------------------------------------------------------------------
#ifdef USE_RTOS
static void tempe_irqhandler(void)
#else
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,5,0)
static void tempe_irqhandler(int irq, void *dev_id, struct pt_regs *regs)
#elif LINUX_VERSION_CODE < KERNEL_VERSION(2,6,21)
static int tempe_irqhandler(int irq, void *dev_id, struct pt_regs *regs)
#else
static irqreturn_t tempe_irqhandler(int irq, void *dev_id)
#endif
#endif
{
    tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
    long stat, enable;
    int handled = 0;
    unsigned long irqflags;
    
    
#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
    spin_lock_irqsave(&tempe_lock, irqflags);
#endif

    // GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  tsi148: tempe_irqhandler irq: %02X\n", irq);
    
    // Save time that this IRQ occurred at.
#ifdef PPC
    tempeIrqTime = vmeGetTime();
#else
    rdtscl(tempeIrqTime);
#endif
    
    // Determine which interrupts are unmasked and active.  
    enable = longswap(ioread32(&pTempe->lcsr.inteo));
    stat = longswap(ioread32(&pTempe->lcsr.ints));
    stat = stat & enable;
    
    // Clear them.
    iowrite32(longswap(stat),&pTempe->lcsr.intc);
    vmeSyncData();
    
    // Call subhandlers as appropriate
    if (stat & 0x03000000)  // DMA irqs
    {
#ifndef USE_RTOS
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  tsi148: DMA_tempe_irqhandler irq: %02X\n", irq);
#endif
        DMA_tempe_irqhandler();
        handled = 1;
    }

    if (stat & 0x02000)     // PCI bus error.
    {
#ifndef USE_RTOS
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  tsi148: LERR_tempe_irqhandler irq: %02X\n", irq);
#endif
        LERR_tempe_irqhandler();
        handled = 1;
    }

    if (stat & 0x01000)     // VME bus error.
    {
#ifndef USE_RTOS
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  tsi148: VERR_tempe_irqhandler irq: %02X\n", irq);
#endif
        VERR_tempe_irqhandler();
        handled = 1;
    }

    if (stat & 0x00200)     // Sys Fail.
    {
#ifndef USE_RTOS
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  tsi148: SYSFAIL_tempe_irqhandler irq: %02X\n", irq);
#endif
        SYSFAIL_tempe_irqhandler();
        handled = 1;
    }

    if (stat & 0x00100)     // AC Fail.
    {
#ifndef USE_RTOS
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  tsi148: ACFAIL_tempe_irqhandler irq: %02X\n", irq);
#endif
        ACFAIL_tempe_irqhandler();
        handled = 1;
    }

    if (stat & 0xF0000)     // Mail box irqs
    {
#ifndef USE_RTOS
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  tsi148: MB_tempe_irqhandler irq: %02X\n", irq);
#endif
        MB_tempe_irqhandler((stat & 0xF0000) >> 16);
        handled = 1;
    }

    if (stat & 0xF00000)    // Location monitor irqs.
    {
#ifndef USE_RTOS
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  tsi148: LM_tempe_irqhandler irq: %02X\n", irq);
#endif
        LM_tempe_irqhandler((stat & 0xF00000) >> 20);
        handled = 1;
    }

    if (stat & 0x0000FE)    // VME bus irqs.
    {
#ifndef USE_RTOS
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  tsi148: VIRQ_tempe_irqhandler irq: %02X\n", irq);
#endif
        VIRQ_tempe_irqhandler(stat & 0x0000FE);
        handled = 1;
    }
    
#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
    spin_unlock_irqrestore(&tempe_lock, irqflags);
#endif
    
#ifdef USE_RTOS
    if(handled != 1)
    {
        rt_mask_and_ack_irq(vme_pci_dev->irq);
        rt_pend_linux_irq(vme_pci_dev->irq);
    }
else
    {
        rt_enable_irq(vme_pci_dev->irq);
    }

#else
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,5,0)
    return IRQ_RETVAL(handled);
#endif
#endif
}

/*============================================================================
* Acquire ownership of the VMEbus.
*
* WARNING: Aquiring ownership is implemented with a counting semaphore. Be
* absolutely sure to make a vme_release_bus_ownership call for every
* vme_acquire_bus_ownership call, otherwise, the VMEbus will remain held.
*/
GEF_STATUS tempeAcquireBusOwnership(GEF_VME_DRV_VOWN_INFO *vown_info)
{
    GEF_UINT32 tempCtl = 0;
    int timeout;
    int looptimeout;
    unsigned long irqflags;

    // Poll for ownership at 200 times per second rate
    looptimeout = HZ/200; 

    if (looptimeout <= 0) {
        looptimeout = 1;
    }
    
    // Calculate total timeout
    timeout  = (vown_info->timeout.tv_sec * HZ);
    timeout += (vown_info->timeout.tv_usec * HZ) / 1000000UL;
    if (timeout == 0) {
        timeout = 1;
    }

    // schedule_timeout doesn't like negative values.
    timeout &= 0x7fffffff;
    
#ifdef USE_RTOS
    int stat = 0;
    stat = rt_sem_wait_timed(&vown_lock, SEMAPHORE_TIMEOUT);

    if((stat == SEM_ERR) || (stat == SEM_TIMOUT))
    {
#else
    if(down_interruptible(&vown_lock)) {
#endif
        return(GEF_STATUS_DRIVER_ERR);
    }
    
    if (0 == vown_count) {
        
#ifdef USE_RTOS
        irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
        spin_lock_irqsave(&tempe_lock, irqflags);
#endif
        
        /* 
        * DWB (Device Wants Bus): When this bit is set, the VME Master 
        * requests the VMEbus.  When VMEbus ownership has been obtained, 
        * the DHB bit is set. VMEbus ownership is maintained until the 
        * DWB bit is cleared. While the DWB bit is set, the PCI/X to VMEbus
        * channel and DMA controllers may access the VMEbus.
        */
        tempCtl = longswap(ioread32(vmechip_baseaddr+TEMPE_VMCTRL));     
        tempCtl |= TEMPE_VMCTRL_DWB;
        iowrite32(longswap(tempCtl),vmechip_baseaddr+TEMPE_VMCTRL);
        vmeSyncData();

#ifdef USE_RTOS
        rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
        spin_unlock_irqrestore(&tempe_lock, irqflags);
#endif
        
        /*
        * DHB (Device Has Bus): When this bit is set, the VME Master has 
        * obtained mastership of the VMEbus in response to the DWB request. 
        * This bit is not set if the VME Master has obtained VMEbus ownership 
        * for any other reason.
        */

        /* Poll the VME Master Control Register until the DHB is set. */
        while((longswap(ioread32(vmechip_baseaddr+TEMPE_VMCTRL)) & TEMPE_VMCTRL_DHB) == 0){
#ifdef USE_RTOS
            rt_sleep(nano2count(jiffies_to_usecs(looptimeout) * 1000ULL));
#else
            set_current_state(TASK_INTERRUPTIBLE);
            schedule_timeout(looptimeout);
#endif
            timeout = timeout - looptimeout;
            if(timeout <= 0){
#ifdef USE_RTOS
                rt_sem_signal(&vown_lock);
#else
                up(&vown_lock);
#endif

                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeAcquireBusOwnership: Acquiring VMEbus timeout\n");
                return(GEF_STATUS_TIMED_OUT);
            }
        }
                
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeAcquireBusOwnership: Acquired VMEbus\n");       
    }
    else {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeAcquireBusOwnership: VMEbus already acquired with count of %d\n", vown_count); 
    }
    
    ++vown_count;
#ifdef USE_RTOS
    rt_sem_signal(&vown_lock);
#else
    up(&vown_lock);
#endif

    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : tempeReleaseBusOwnership
// Description:
//-----------------------------------------------------------------------------
GEF_STATUS tempeReleaseBusOwnership(void)
{
    GEF_UINT32 tempCtl = 0;
    unsigned long irqflags;
    
#ifdef USE_RTOS
    int stat = 0;
    stat = rt_sem_wait_timed(&vown_lock, SEMAPHORE_TIMEOUT);

    if((stat == SEM_ERR) || (stat == SEM_TIMOUT))
    {
#else
    if(down_interruptible(&vown_lock)) {
#endif
        return(GEF_STATUS_DRIVER_ERR);
    }
    
    if (1 == vown_count) {
        
#ifdef USE_RTOS
        irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
        spin_lock_irqsave(&tempe_lock, irqflags);
#endif
        
        /* 
        * DWB (Device Wants Bus): When this bit is set, the VME Master 
        * requests the VMEbus.  When VMEbus ownership has been obtained, 
        * the DHB bit is set. VMEbus ownership is maintained until the 
        * DWB bit is cleared. While the DWB bit is set, the PCI/X to VMEbus
        * channel and DMA controllers may access the VMEbus.
        */
        
        /* Clear the DWB bit */
        tempCtl = longswap(ioread32(vmechip_baseaddr+TEMPE_VMCTRL));     
        tempCtl &= ~TEMPE_VMCTRL_DWB;
        iowrite32(longswap(tempCtl),vmechip_baseaddr+TEMPE_VMCTRL);
        vmeSyncData();
        
#ifdef USE_RTOS
        rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
        spin_unlock_irqrestore(&tempe_lock, irqflags);
#endif
        
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeReleaseBusOwnership: Released VMEbus\n");

        vown_count = 0;
        
    }
    else {
        if(vown_count > 0){
            GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeReleaseBusOwnership: VMEbus still acquired with count of %d\n", vown_count);
            --vown_count;
        }
        else{
#ifdef USE_RTOS
             rtlogRecord_t logRecord;
             RTLOG_ERROR("gefvme_rtos", "VME: Redundant VMEbus ownership release "
               "attempt.");
#else
            printk("VME: Redundant VMEbus ownership release attempt.\n");
#endif
        }
    }
    
#ifdef USE_RTOS
    rt_sem_signal(&vown_lock);
#else
    up(&vown_lock);
#endif

    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : tempeGenerateIrq
// Description: Generate a VME bus interrupt at the requested level & vector.
// Wait for system controller to ack the interrupt.
//-----------------------------------------------------------------------------
int
tempeGenerateIrq(GEF_VME_DRV_VIRQ_INFO *virq_info)
{
    tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
    GEF_INT32 timeout;
    GEF_INT32 looptimeout;
    GEF_UINT32 tmp;
    GEF_UINT32 irqflags;
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeGenerateIrq: level %x vec %x\n",virq_info->level,virq_info->vector);
    
    // Poll for iack at 200 times per second rate
    looptimeout = HZ/200; 

    if (looptimeout <= 0) {
        looptimeout = 1;
    }

    // Calculate total timeout
    timeout  = (virq_info->timeout.tv_sec * HZ);
    timeout += (virq_info->timeout.tv_usec * HZ) / 1000000UL;
    if (timeout == 0) {
        timeout = 1;
    }

    // schedule_timeout doesn't like negative values.
    timeout &= 0x7fffffff;
    
    if (NULL == vmechip_baseaddr) {
        return(GEF_STATUS_DEVICE_NOT_INIT);
    }
    
    // We only allow generation of software interrupts
    if ((GEF_VME_INT_VIRQ1 > virq_info->level) || (GEF_VME_INT_VIRQ7 < virq_info->level)) {
        return(GEF_STATUS_NOT_SUPPORTED);
    }
    
#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
    spin_lock_irqsave(&tempe_lock, irqflags);
#endif
    
    // Validate & setup vector register.
    tmp = longswap(ioread32(&pTempe->lcsr.vicr));
    tmp &= ~0xFF;
    
    iowrite32(longswap(tmp | virq_info->vector),&pTempe->lcsr.vicr);
    vmeSyncData();
    
    // Assert VMEbus IRQ
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "assert irq vicr val 0x%x\n",(tmp | (virq_info->level << 8) |virq_info->vector));
    iowrite32(longswap((tmp | (virq_info->level << 8) |virq_info->vector)),&pTempe->lcsr.vicr);
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vicr after asserted 0x%x\n",longswap(ioread32(&pTempe->lcsr.vicr)));
    vmeSyncData();
    
#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
    spin_unlock_irqrestore (&tempe_lock, irqflags);
#endif
    
    // Wait for syscon to do iack
    while( longswap(ioread32(&pTempe->lcsr.vicr)) & 0x800 ){
#ifdef USE_RTOS
        rt_sleep(nano2count(jiffies_to_usecs(looptimeout) * 1000ULL));
#else
        set_current_state(TASK_INTERRUPTIBLE);
        schedule_timeout(looptimeout);
#endif
        timeout = timeout - looptimeout;
        if(timeout <= 0){
            
           /*
            * IRQC (VMEbus IRQ Clear): When this bit is set high, the IRQL bits are reset and the
            * VMEbus interrupt is removed. This bit should only be used to recover from an error
            * condition. Normally VMEbus interrupts should not be removed. This bit always reads zero
            * and writing a zero has no effect.
            */
            
            tmp = longswap(ioread32(&pTempe->lcsr.vicr));
            tmp |= TEMPE_VICR_IRQC;
            iowrite32(longswap(tmp),&pTempe->lcsr.vicr);
            
            return(GEF_STATUS_TIMED_OUT);
        }
    }
    
    return(GEF_SUCCESS);
}

// Make sure this function is called with spin locks enabled
int
tempeEnableIrq(GEF_VME_INT_INFO *int_info)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  GEF_UINT32 tempCtl = 0;

  if (NULL == vmechip_baseaddr) {
    return(GEF_STATUS_DEVICE_NOT_INIT);
  }

  switch(int_info->int_level)
  {
  case GEF_VME_INT_VIRQ1: break;
  case GEF_VME_INT_VIRQ2: break;
  case GEF_VME_INT_VIRQ3: break;
  case GEF_VME_INT_VIRQ4: break;
  case GEF_VME_INT_VIRQ5: break;
  case GEF_VME_INT_VIRQ6: break;
  case GEF_VME_INT_VIRQ7: break;
  case GEF_VME_INT_DMA0: break;
  case GEF_VME_INT_LERR: break;
  case GEF_VME_INT_BERR: break;
  case GEF_VME_INT_IRQ_EDGE: break;
  case GEF_VME_INT_SW_IACK: break;
  case GEF_VME_INT_SYSFAIL: break;
  case GEF_VME_INT_ACFAIL: break;
  case GEF_VME_INT_MBOX0: break;
  case GEF_VME_INT_MBOX1: break;
  case GEF_VME_INT_MBOX2: break;
  case GEF_VME_INT_MBOX3: break;
  case GEF_VME_INT_LM0: break;
  case GEF_VME_INT_LM1: break;
  case GEF_VME_INT_LM2: break;
  case GEF_VME_INT_LM3: break;
  case GEF_VME_INT_DMA1: break;
  default: return(GEF_STATUS_NOT_SUPPORTED);
  }

  tempCtl = longswap(ioread32(&pTempe->lcsr.inten));

  switch(int_info->int_level)
  {
  case GEF_VME_INT_VIRQ1: tempCtl |= BIT_INT_IRQ1; break;
  case GEF_VME_INT_VIRQ2: tempCtl |= BIT_INT_IRQ2; break;
  case GEF_VME_INT_VIRQ3: tempCtl |= BIT_INT_IRQ3; break;
  case GEF_VME_INT_VIRQ4: tempCtl |= BIT_INT_IRQ4; break;
  case GEF_VME_INT_VIRQ5: tempCtl |= BIT_INT_IRQ5; break;
  case GEF_VME_INT_VIRQ6: tempCtl |= BIT_INT_IRQ6; break;
  case GEF_VME_INT_VIRQ7: tempCtl |= BIT_INT_IRQ7; break;
  case GEF_VME_INT_DMA0: tempCtl |= BIT_INT_DMA_0; break;
  case GEF_VME_INT_LERR: tempCtl |= BIT_INT_PCI_ERR; break;
  case GEF_VME_INT_BERR: tempCtl |= BIT_INT_VME_ERR; break;
  case GEF_VME_INT_IRQ_EDGE: tempCtl |= BIT_INT_VME_IRQ_EDGE; break;
  case GEF_VME_INT_SW_IACK: tempCtl |= BIT_INT_IACK; break;
  case GEF_VME_INT_SYSFAIL: tempCtl |= BIT_INT_SYSFAIL; break;
  case GEF_VME_INT_ACFAIL: tempCtl |= BIT_INT_ACFAIL; break;
  case GEF_VME_INT_MBOX0: tempCtl |= BIT_INT_MBOX_0; break;
  case GEF_VME_INT_MBOX1: tempCtl |= BIT_INT_MBOX_1; break;
  case GEF_VME_INT_MBOX2: tempCtl |= BIT_INT_MBOX_2; break;
  case GEF_VME_INT_MBOX3: tempCtl |= BIT_INT_MBOX_3; break;
  case GEF_VME_INT_LM0: tempCtl |= BIT_INT_LM_0; break;
  case GEF_VME_INT_LM1: tempCtl |= BIT_INT_LM_1; break;
  case GEF_VME_INT_LM2: tempCtl |= BIT_INT_LM_2; break;
  case GEF_VME_INT_LM3: tempCtl |= BIT_INT_LM_3; break;
  case GEF_VME_INT_DMA1: tempCtl |= BIT_INT_DMA_1; break;
  default: break;
  }

  iowrite32(longswap(tempCtl),&pTempe->lcsr.inteo);
  iowrite32(longswap(tempCtl),&pTempe->lcsr.inten);
  vmeSyncData();

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeEnableIrq: Enabled irq level %d\n", int_info->int_level);

  return(GEF_SUCCESS);
}

// Make sure this function is called with spin locks enabled
int
tempeDisableIrq(GEF_VME_INT_INFO *int_info)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  GEF_UINT32 tempCtl = 0;

  if (NULL == vmechip_baseaddr) {
    return(GEF_STATUS_DEVICE_NOT_INIT);
  }

  switch(int_info->int_level)
  {
  case GEF_VME_INT_VIRQ1: break;
  case GEF_VME_INT_VIRQ2: break;
  case GEF_VME_INT_VIRQ3: break;
  case GEF_VME_INT_VIRQ4: break;
  case GEF_VME_INT_VIRQ5: break;
  case GEF_VME_INT_VIRQ6: break;
  case GEF_VME_INT_VIRQ7: break;
  case GEF_VME_INT_DMA0: break;
  case GEF_VME_INT_LERR: break;
  case GEF_VME_INT_BERR: break;
  case GEF_VME_INT_IRQ_EDGE: break;
  case GEF_VME_INT_SW_IACK: break;
  case GEF_VME_INT_SYSFAIL: break;
  case GEF_VME_INT_ACFAIL: break;
  case GEF_VME_INT_MBOX0: break;
  case GEF_VME_INT_MBOX1: break;
  case GEF_VME_INT_MBOX2: break;
  case GEF_VME_INT_MBOX3: break;
  case GEF_VME_INT_LM0: break;
  case GEF_VME_INT_LM1: break;
  case GEF_VME_INT_LM2: break;
  case GEF_VME_INT_LM3: break;
  case GEF_VME_INT_DMA1: break;
  default: return(GEF_STATUS_NOT_SUPPORTED);
  }

  tempCtl = longswap(ioread32(&pTempe->lcsr.inten));

  switch(int_info->int_level)
  {
  case GEF_VME_INT_VIRQ1: tempCtl &= ~BIT_INT_IRQ1; break;
  case GEF_VME_INT_VIRQ2: tempCtl &= ~BIT_INT_IRQ2; break;
  case GEF_VME_INT_VIRQ3: tempCtl &= ~BIT_INT_IRQ3; break;
  case GEF_VME_INT_VIRQ4: tempCtl &= ~BIT_INT_IRQ4; break;
  case GEF_VME_INT_VIRQ5: tempCtl &= ~BIT_INT_IRQ5; break;
  case GEF_VME_INT_VIRQ6: tempCtl &= ~BIT_INT_IRQ6; break;
  case GEF_VME_INT_VIRQ7: tempCtl &= ~BIT_INT_IRQ7; break;
  case GEF_VME_INT_DMA0: tempCtl &= ~BIT_INT_DMA_0; break;
  case GEF_VME_INT_LERR: tempCtl &= ~BIT_INT_PCI_ERR; break;
  case GEF_VME_INT_BERR: tempCtl &= ~BIT_INT_VME_ERR; break;
  case GEF_VME_INT_IRQ_EDGE: tempCtl &= ~BIT_INT_VME_IRQ_EDGE; break;
  case GEF_VME_INT_SW_IACK: tempCtl &= ~BIT_INT_IACK; break;
  case GEF_VME_INT_SYSFAIL: tempCtl &= ~BIT_INT_SYSFAIL; break;
  case GEF_VME_INT_ACFAIL: tempCtl &= ~BIT_INT_ACFAIL; break;
  case GEF_VME_INT_MBOX0: tempCtl &= ~BIT_INT_MBOX_0; break;
  case GEF_VME_INT_MBOX1: tempCtl &= ~BIT_INT_MBOX_1; break;
  case GEF_VME_INT_MBOX2: tempCtl &= ~BIT_INT_MBOX_2; break;
  case GEF_VME_INT_MBOX3: tempCtl &= ~BIT_INT_MBOX_3; break;
  case GEF_VME_INT_LM0: tempCtl &= ~BIT_INT_LM_0; break;
  case GEF_VME_INT_LM1: tempCtl &= ~BIT_INT_LM_1; break;
  case GEF_VME_INT_LM2: tempCtl &= ~BIT_INT_LM_2; break;
  case GEF_VME_INT_LM3: tempCtl &= ~BIT_INT_LM_3; break;
  case GEF_VME_INT_DMA1: tempCtl &= ~BIT_INT_DMA_1; break;
  default: break;
  }

  iowrite32(longswap(tempCtl),&pTempe->lcsr.inteo);
  iowrite32(longswap(tempCtl),&pTempe->lcsr.inten);
  vmeSyncData();

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeDisableIrq: Disable irq %d\n", int_info->int_level);

  return(GEF_SUCCESS);
}


/********************
*   MASTER WINDOW   *
*********************/

static
int
ConvCAPIEnumToDrvEnumForMaster(GEF_VME_DRV_CREATE_MASTER_WINDOW_INFO *master_info, vmeOutWindowCfg_t *vmeOut, int window, GEF_UINT32 size)
{
    vmeOut->windowNbr = window;
    vmeOut->pciBusAddrL = vmw[window].physAddrRes.start - pci_bus_mem_base_phys(vmechip_bus);
    vmeOut->pciBusAddrU = 0; /* add 64bit support */
    vmeOut->windowEnable = 1;
    vmeOut->windowSizeL = size;
    vmeOut->windowSizeU = 0; /* add 64bit support */
    vmeOut->xlatedAddrL = master_info->addr.lower;
    vmeOut->xlatedAddrU = master_info->addr.upper; 

    /* address space */
    switch(master_info->addr.addr_space)
    {
    case GEF_VME_ADDR_SPACE_A16: vmeOut->addrSpace = VME_A16; break;
    case GEF_VME_ADDR_SPACE_A24: vmeOut->addrSpace = VME_A24; break;
    case GEF_VME_ADDR_SPACE_A32: vmeOut->addrSpace = VME_A32; break;
    case GEF_VME_ADDR_SPACE_A64: vmeOut->addrSpace = VME_A64; break;
    case GEF_VME_ADDR_SPACE_CRCSR: vmeOut->addrSpace = VME_CRCSR; break;
    case GEF_VME_ADDR_SPACE_USER1: vmeOut->addrSpace = VME_USER1; break;
    case GEF_VME_ADDR_SPACE_USER2: vmeOut->addrSpace = VME_USER2; break;
    case GEF_VME_ADDR_SPACE_USER3: vmeOut->addrSpace = VME_USER3; break;
    case GEF_VME_ADDR_SPACE_USER4: vmeOut->addrSpace = VME_USER4; break;
    default: return (GEF_STATUS_VME_INVALID_ADDRESS_SPACE);
    }
    
    /* transfer max width */
    switch(master_info->addr.transfer_max_dwidth)
    {
    case GEF_VME_TRANSFER_MAX_DWIDTH_8: vmeOut->maxDataWidth = VME_D8; break;
    case GEF_VME_TRANSFER_MAX_DWIDTH_16: vmeOut->maxDataWidth = VME_D16; break;
    case GEF_VME_TRANSFER_MAX_DWIDTH_32: vmeOut->maxDataWidth = VME_D32; break;
    case GEF_VME_TRANSFER_MAX_DWIDTH_64: vmeOut->maxDataWidth = VME_D64; break;
    default: return (GEF_STATUS_VME_INVALID_TRANSFER_WIDTH);
    }

    /* transfer mode */
    switch(master_info->addr.transfer_mode)
    {
    case GEF_VME_TRANSFER_MODE_SCT: vmeOut->xferProtocol = VME_SCT; break;
    case GEF_VME_TRANSFER_MODE_BLT: vmeOut->xferProtocol = VME_BLT; break;
    case GEF_VME_TRANSFER_MODE_MBLT: vmeOut->xferProtocol = VME_MBLT; break;
    case GEF_VME_TRANSFER_MODE_2eVME: vmeOut->xferProtocol = VME_2eVME; break;
    case GEF_VME_TRANSFER_MODE_2eSST: vmeOut->xferProtocol = VME_2eSST; break;
    case GEF_VME_TRANSFER_MODE_2eSSTB: vmeOut->xferProtocol = VME_2eSSTB; break;
    default: return (GEF_STATUS_VME_INVALID_TRANSFER_MODE);
    }
    
    /* address mode */
    if( (master_info->addr.addr_mode & GEF_VME_ADDR_MODE_USER) == GEF_VME_ADDR_MODE_USER)
    {
        vmeOut->userAccessType |= VME_USER;
    }

    if( (master_info->addr.addr_mode & GEF_VME_ADDR_MODE_SUPER) == GEF_VME_ADDR_MODE_SUPER)
    {
        vmeOut->userAccessType |= VME_SUPER;
    }

    if( (master_info->addr.addr_mode & GEF_VME_ADDR_MODE_DATA) == GEF_VME_ADDR_MODE_DATA)
    {
        vmeOut->dataAccessType |= VME_DATA;
    }

    if( (master_info->addr.addr_mode & GEF_VME_ADDR_MODE_PROGRAM) == GEF_VME_ADDR_MODE_PROGRAM)
    {
        vmeOut->dataAccessType |= VME_PROG;
    }

    /* 2esst rate */
    switch(master_info->addr.vme_2esst_rate)
    {
    case GEF_VME_2ESST_RATE_INVALID: vmeOut->xferRate2esst = VME_SSTNONE; break;
    case GEF_VME_2ESST_RATE_160: vmeOut->xferRate2esst = VME_SST160; break;
    case GEF_VME_2ESST_RATE_267: vmeOut->xferRate2esst = VME_SST267; break;
    case GEF_VME_2ESST_RATE_320: vmeOut->xferRate2esst = VME_SST320; break;
    default: return (GEF_STATUS_VME_INVALID_2eSST_RATE);
    }

    /* broadcast id */
    switch(master_info->addr.broadcast_id)
    {
    case GEF_VME_BROADCAST_ID_DISABLE: vmeOut->bcastSelect2esst = DRV_VME_BROADCAST_ID_DISABLE; break;
    case GEF_VME_BROADCAST_ID_1:       vmeOut->bcastSelect2esst = DRV_VME_BROADCAST_ID_1; break;
    case GEF_VME_BROADCAST_ID_2:       vmeOut->bcastSelect2esst = DRV_VME_BROADCAST_ID_2; break; 
    case GEF_VME_BROADCAST_ID_3:       vmeOut->bcastSelect2esst = DRV_VME_BROADCAST_ID_3; break;
    case GEF_VME_BROADCAST_ID_4:       vmeOut->bcastSelect2esst = DRV_VME_BROADCAST_ID_4; break;
    case GEF_VME_BROADCAST_ID_5:       vmeOut->bcastSelect2esst = DRV_VME_BROADCAST_ID_5; break;
    case GEF_VME_BROADCAST_ID_6:       vmeOut->bcastSelect2esst = DRV_VME_BROADCAST_ID_6; break;
    case GEF_VME_BROADCAST_ID_7:       vmeOut->bcastSelect2esst = DRV_VME_BROADCAST_ID_7; break;
    case GEF_VME_BROADCAST_ID_8:       vmeOut->bcastSelect2esst = DRV_VME_BROADCAST_ID_8; break;
    case GEF_VME_BROADCAST_ID_9:       vmeOut->bcastSelect2esst = DRV_VME_BROADCAST_ID_9; break;
    case GEF_VME_BROADCAST_ID_10:      vmeOut->bcastSelect2esst = DRV_VME_BROADCAST_ID_10; break;
    case GEF_VME_BROADCAST_ID_11:      vmeOut->bcastSelect2esst = DRV_VME_BROADCAST_ID_11; break;
    case GEF_VME_BROADCAST_ID_12:      vmeOut->bcastSelect2esst = DRV_VME_BROADCAST_ID_12; break;
    case GEF_VME_BROADCAST_ID_13:      vmeOut->bcastSelect2esst = DRV_VME_BROADCAST_ID_13; break;
    case GEF_VME_BROADCAST_ID_14:      vmeOut->bcastSelect2esst = DRV_VME_BROADCAST_ID_14; break;
    case GEF_VME_BROADCAST_ID_15:      vmeOut->bcastSelect2esst = DRV_VME_BROADCAST_ID_15; break;
    case GEF_VME_BROADCAST_ID_16:      vmeOut->bcastSelect2esst = DRV_VME_BROADCAST_ID_16; break;
    case GEF_VME_BROADCAST_ID_17:      vmeOut->bcastSelect2esst = DRV_VME_BROADCAST_ID_17; break;
    case GEF_VME_BROADCAST_ID_18:      vmeOut->bcastSelect2esst = DRV_VME_BROADCAST_ID_18; break;
    case GEF_VME_BROADCAST_ID_19:      vmeOut->bcastSelect2esst = DRV_VME_BROADCAST_ID_19; break;
    case GEF_VME_BROADCAST_ID_20:      vmeOut->bcastSelect2esst = DRV_VME_BROADCAST_ID_20; break;
    case GEF_VME_BROADCAST_ID_21:      vmeOut->bcastSelect2esst = DRV_VME_BROADCAST_ID_21; break;
    default: return (GEF_STATUS_VME_INVALID_BROADCAST_ID);
    }

    return (GEF_SUCCESS);
}

static
int
ConvCAPIEnumToDrvEnumForSlave(GEF_VME_DRV_CREATE_SLAVE_WINDOW_INFO *slave_info, vmeInWindowCfg_t *vmeIn, int window)
{
    vmeIn->windowNbr = window;
    vmeIn->pciAddrL = (GEF_UINT32)vsw[window].physAddr;
    vmeIn->pciAddrU = 0; /* add 64bit support */
    vmeIn->windowEnable = 1;
    vmeIn->windowSizeL = slave_info->size;
    vmeIn->windowSizeU = 0; /* add 64bit support */
    vmeIn->vmeAddrL = slave_info->addr.lower;
    vmeIn->vmeAddrU = slave_info->addr.upper;
    
    /* address space */
    switch(slave_info->addr.addr_space)
    {
    case GEF_VME_ADDR_SPACE_A16: vmeIn->addrSpace = VME_A16; break;
    case GEF_VME_ADDR_SPACE_A24: vmeIn->addrSpace = VME_A24; break;
    case GEF_VME_ADDR_SPACE_A32: vmeIn->addrSpace = VME_A32; break;
    case GEF_VME_ADDR_SPACE_A64: vmeIn->addrSpace = VME_A64; break;
    case GEF_VME_ADDR_SPACE_CRCSR: vmeIn->addrSpace = VME_CRCSR; break;
    case GEF_VME_ADDR_SPACE_USER1: vmeIn->addrSpace = VME_USER1; break;
    case GEF_VME_ADDR_SPACE_USER2: vmeIn->addrSpace = VME_USER2; break;
    case GEF_VME_ADDR_SPACE_USER3: vmeIn->addrSpace = VME_USER3; break;
    case GEF_VME_ADDR_SPACE_USER4: vmeIn->addrSpace = VME_USER4; break;
    default: return (GEF_STATUS_VME_INVALID_ADDRESS_SPACE);
    }

    /* transfer mode */
    switch(slave_info->addr.transfer_mode)
    {
    case GEF_VME_TRANSFER_MODE_SCT: vmeIn->xferProtocol = VME_SCT; break;
    case GEF_VME_TRANSFER_MODE_BLT: vmeIn->xferProtocol = VME_BLT; break;
    case GEF_VME_TRANSFER_MODE_MBLT: vmeIn->xferProtocol = VME_MBLT; break;
    case GEF_VME_TRANSFER_MODE_2eVME: vmeIn->xferProtocol = VME_2eVME; break;
    case GEF_VME_TRANSFER_MODE_2eSST: vmeIn->xferProtocol = VME_2eSST; break;
    case GEF_VME_TRANSFER_MODE_2eSSTB: vmeIn->xferProtocol = VME_2eSSTB; break;
    default: return (GEF_STATUS_VME_INVALID_TRANSFER_MODE);
    }
    
    /* address mode */
    if( (slave_info->addr.addr_mode & GEF_VME_ADDR_MODE_USER) == GEF_VME_ADDR_MODE_USER)
    {
        vmeIn->userAccessType |= VME_USER;
    }

    if( (slave_info->addr.addr_mode & GEF_VME_ADDR_MODE_SUPER) == GEF_VME_ADDR_MODE_SUPER)
    {
        vmeIn->userAccessType |= VME_SUPER;
    }

    if( (slave_info->addr.addr_mode & GEF_VME_ADDR_MODE_DATA) == GEF_VME_ADDR_MODE_DATA)
    {
        vmeIn->dataAccessType |= VME_DATA;
    }

    if( (slave_info->addr.addr_mode & GEF_VME_ADDR_MODE_PROGRAM) == GEF_VME_ADDR_MODE_PROGRAM)
    {
        vmeIn->dataAccessType |= VME_PROG;
    }

    /* 2esst rate */
    switch(slave_info->addr.vme_2esst_rate)
    {
    case GEF_VME_2ESST_RATE_INVALID: vmeIn->xferRate2esst = VME_SSTNONE; break;
    case GEF_VME_2ESST_RATE_160: vmeIn->xferRate2esst = VME_SST160; break;
    case GEF_VME_2ESST_RATE_267: vmeIn->xferRate2esst = VME_SST267; break;
    case GEF_VME_2ESST_RATE_320: vmeIn->xferRate2esst = VME_SST320; break;
    default: return (GEF_STATUS_VME_INVALID_2eSST_RATE);
    }

    return (GEF_SUCCESS);
}

int
tempeFindMasterWindow(GEF_VME_DRV_CREATE_MASTER_WINDOW_INFO *master_info, GEF_UINT32 ctl, GEF_UINT32 *offset)
{
    tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
    GEF_UINT32 lsiCtl = 0;
    GEF_UINT32 lsiStartAL = 0;
//    GEF_UINT32 lsiStartAU = 0;
    GEF_UINT32 lsiEndAL = 0;
//    GEF_UINT32 lsiEndAU = 0;
    GEF_UINT32 lsiOffsetL = 0;
//    GEF_UINT32 lsiOffsetU = 0;
    GEF_UINT32 i = 0;
    GEF_BOOL   found = GEF_FALSE;
    GEF_UINT32 phy_addr = 0;
    GEF_UINT32 vmeAddr = master_info->addr.lower;
    
  if (NULL == vmechip_baseaddr) {
    return(GEF_STATUS_DEVICE_NOT_INIT);
  }

    for(i=0;i<VME_MAX_WINDOWS;i++)
    {
        if(vmw[i].exclusive)
        {
            GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeFindMasterWindow: skip window %d because it is an exclusive window\n", i);
            continue;
        }

        phy_addr = vmw[i].physAddrRes.start - pci_bus_mem_base_phys(vmechip_bus);
        lsiCtl = longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otat)) & 0x80003FFF; /* filter reservered and read prefech bits */

            GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeFindMasterWindow: Window[%d] OTAT setting 0x%x, requested control setting 0x%x\n", i, lsiCtl, ctl);

        if(ctl == lsiCtl)
        {
            lsiStartAL = longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otsal));
            /* add lsiStartAU when 64bit is supported */
            lsiEndAL = longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].oteal));
            /* add lsiEndU when 64bit is supported */
            lsiOffsetL = longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otofl));
            /* add lsiOffsetU when 64bit is supported */

            /* If this check fails, then either the window handle
               has gotten corrupted, or a user has modified 
               this window behind this driver's back.
            */
            if ((lsiStartAL != phy_addr)
                || ((lsiStartAL + lsiOffsetL) != vmw[i].vmeL)
                || ((lsiEndAL - lsiStartAL) != (vmw[i].size-0x10000))) {
#ifdef USE_RTOS
             rtlogRecord_t logRecord;
             RTLOG_ERROR("gefvme_rtos", "tempeFindMasterWindow: Master window %d "
               "fails consistancy check", i);
#else
                printk("tempeFindMasterWindow: Master window %d fails consistancy check\n", i);
#endif
           GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeFindMasterWindow: Expected base=0x%x bound=0x%x to=0x%x\n",
                    phy_addr,
                    phy_addr + vmw[i].size,
                    vmw[i].vmeL - phy_addr);
#ifdef USE_RTOS
             RTLOG_ERROR("gefvme_rtos", "tempeFindMasterWindow: Found base=0x%x "
               "bound=0x%x to=0x%x",
#else
                printk("tempeFindMasterWindow: Found base=0x%x bound=0x%x to=0x%x\n",
#endif
                    lsiStartAL, lsiEndAL, lsiOffsetL);
                continue;
            }
            
            if ((master_info->addr.lower >= (lsiStartAL + lsiOffsetL)) &&
                ((master_info->addr.lower + master_info->size -0x10000) <= (lsiEndAL + lsiOffsetL))) {
                found = GEF_TRUE;
                *offset = vmeAddr - vmw[i].vmeL;
                break;
            }
        }
        
        continue;
    }

    if(found == GEF_TRUE)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeFindMasterWindow: Found master window 0x%x\n", i);
        return(i);
    }
    else
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeFindMasterWindow: Did not find master window\n");
        return(-1);
    }
}

int
tempeFindSlaveWindow(GEF_VME_DRV_CREATE_SLAVE_WINDOW_INFO *slave_info, GEF_UINT32 ctl, GEF_UINT32 *offset)
{
    tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
    GEF_UINT32 vsiCtl = 0;
    GEF_UINT32 vsiStartAL = 0;
//    GEF_UINT32 vsiStartAU = 0;
    GEF_UINT32 vsiEndAL = 0;
//    GEF_UINT32 vsiEndAU = 0;
    GEF_UINT32 vsiOffsetL = 0;
//    GEF_UINT32 vsiOffsetU = 0;
    GEF_UINT32 i = 0;
    GEF_BOOL   found = GEF_FALSE;
    GEF_UINT32 phy_addr = 0;
    GEF_UINT32 vmeAddr = slave_info->addr.lower;
    
  if (NULL == vmechip_baseaddr) {
    return(GEF_STATUS_DEVICE_NOT_INIT);
  }

    for(i=0;i<VME_MAX_WINDOWS;i++)
    {
        if(vsw[i].exclusive)
        {
            GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeFindSlaveWindow: skip window %d because it is an exclusive window\n", i);
            continue;
        }

        phy_addr = (GEF_UINT32)vsw[i].physAddr;
        vsiCtl = longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].itat));

            GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeFindSlaveWindow: Window[%d] OTAT settting 0x%x, requested control setting 0x%x\n", i, vsiCtl, ctl);
        
        if(ctl == vsiCtl)
        {
            vsiStartAL = longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].itsal));
            /* add vsiStartAU when 64bit is supported */
            vsiEndAL = longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].iteal));
            /* add vsiEndU when 64bit is supported */
            vsiOffsetL = longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].itofl));
            /* add vsiOffsetU when 64bit is supported */

            *offset = vmeAddr - vsiStartAL;

            /* If this check fails, then either the window handle
               has gotten corrupted, or a user has modified 
               this window behind this driver's back.
            */
            if ((vsiStartAL != vsw[i].vmeL)
                || ((vsiStartAL + vsiOffsetL) != phy_addr)
                || ((vsiEndAL - vsiStartAL) != vsw[i].size)) {
#ifdef USE_RTOS
              rtlogRecord_t logRecord;
             RTLOG_ERROR("gefvme_rtos", "tempeFindSlaveWindow: Master window %d "
               "fails consistancy check", i);
#else
                printk("tempeFindSlaveWindow: Master window %d fails consistancy check\n", i);
#endif
           GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeFindSlaveWindow: Expected base=0x%x bound=0x%x to=0x%x\n",
                    vsw[i].vmeL,
                    vsw[i].vmeL + vsw[i].size,
                    phy_addr-vsw[i].vmeL);
#ifdef USE_RTOS
             RTLOG_ERROR("gefvme_rtos", "tempeFindSlaveWindow: Found base=0x%x "
               "bound=0x%x to=0x%x",
#else
                printk("tempeFindSlaveWindow: Found base=0x%x bound=0x%x to=0x%x\n",
#endif
                    vsiStartAL, vsiEndAL, vsiOffsetL);
                continue;
            }
            
            if ((slave_info->addr.lower >= vsiStartAL) &&
                ((slave_info->addr.lower + slave_info->size) <= vsiEndAL)) {
                found = GEF_TRUE;
                break;
            }
        }
        
        continue;
    }

    if(found == GEF_TRUE)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeFindSlaveWindow: Found master window 0x%x\n", i);
        return(i);
    }
    else
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeFindSlaveWindow: Did not find master window\n");
        return(-1);
    }
}

int
tempeCreateMasterWindow(GEF_VME_DRV_CREATE_MASTER_WINDOW_INFO *master_info, GEF_UINT32 ctl, vmeMasterHandle_t *masterhandle)
{
    tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
    GEF_UINT32 resolution = 0x1000; /* default to 4K */
    GEF_UINT32 i = 0;
    GEF_UINT32 vme_addr_lower = master_info->addr.lower;
    GEF_UINT32 vme_addr_upper = master_info->addr.upper;
    GEF_UINT32 size = master_info->size;
    GEF_UINT32 existingSize;
    GEF_UINT32 lsiCtl = 0;
    GEF_UINT32 off;
    vmeOutWindowCfg_t vmeOut;
    int status = 0;

    memset(&vmeOut, 0, sizeof(vmeOutWindowCfg_t));

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeCreateMasterWindow: Entry: Control 0x%x\n", ctl);

    if (NULL == vmechip_baseaddr) {
        return(GEF_STATUS_DEVICE_NOT_INIT);
    }

    if (NULL == masterhandle) {
        return(GEF_STATUS_NO_MEM);
    }

    /* Look for a window that is not already enabled
    */
    for (i=0; i< VME_MASTER_WINDOWS; i++) {
        lsiCtl = longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otat));
        if(!(lsiCtl & TSI148_LCSR_OTAT_EN)) {
            
            resolution = 0x10000; /* all windows 64k */
            
            off = vme_addr_lower % resolution;                                  /* master window offset */
            size += off;
            size += (size % resolution) ? resolution - (size % resolution) : 0; /* adjust window size */
            
            // Allocate PCI memory space for this window.
            existingSize = vmw[i].physAddrRes.end - vmw[i].physAddrRes.start;           
            if(existingSize != size){
                
                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeCreateMasterWindow: existingSize (0x%x) != size (0x%x)\n",
                    existingSize, size);
                
                if(existingSize != 0){
                    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeCreateMasterWindow: existingSize (0x%x) != 0\n",
                        existingSize);
                    release_resource(&vmw[i].physAddrRes);
                    memset(&vmw[i].physAddrRes, 0, sizeof(struct resource));
                }
                
                /* PCI Resource */
                if(allocate_resource(vmepcimem, &vmw[i].physAddrRes, 
                    size, vmepcimem->start, vmepcimem->end, resolution, 0,0)) {
                    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeCreateMasterWindow: allocation failed for size %x\n", size);
                    return(GEF_STATUS_NO_MEM);
                }
                
                /* map to kernel space and non-cachable */
                vmw[i].virtAddr = 
                    ioremap_nocache(vmw[i].physAddrRes.start, size);
                if(vmw[i].virtAddr == 0){
                    release_resource(&vmw[i].physAddrRes);
                    memset(&vmw[i].physAddrRes, 0, sizeof(struct resource));
                    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeCreateMasterWindow: ioremap failed for start 0x%x and size 0x%x\n", (unsigned int)vmw[i].physAddrRes.start, size);
                    return(GEF_STATUS_NO_MEM);
                }

                if(master_info->addr.flags & GEF_VME_WND_EXCLUSIVE)
                {
                    vmw[i].exclusive = GEF_TRUE;
                }
                vmw[i].number = i;
                vmw[i].physAddr = (void*)((GEF_UINT32)vmw[i].physAddrRes.start - pci_bus_mem_base_phys(vmechip_bus));
                masterhandle->offset = off;
                masterhandle->window = &vmw[i];
                vmw[i].vmeAddr = master_info->addr;
                
                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeCreateMasterWindow: Window [%d], Physical Address (PCI) [0x%p], Virtual Address [0x%p], Resolution [0x%x], Offset [0x%x]\n", 
                    vmw[i].number, vmw[i].physAddr, vmw[i].virtAddr, resolution, masterhandle->offset);
            }
          
            status = ConvCAPIEnumToDrvEnumForMaster(master_info, &vmeOut, i, size);
            if(status != GEF_SUCCESS)
            {
                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeCreateMasterWindow: ConvCAPIEnumToDrvEnumForMaster() not successful\n");
                release_resource(&vmw[i].physAddrRes);
                iounmap((char*)vmw[i].virtAddr);
                memset(&vmw[i], 0, sizeof(vmeMasterWindow_t));
                return (status);
            }

            vmw[i].vmeL = vme_addr_lower;
            vmw[i].vmeU = vme_addr_upper;
            vmw[i].size = size;

            if(tempeSetOutBound(&vmeOut) != 0)
            {
                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeCreateMasterWindow: tempeSetOutBound() not successful\n");
                release_resource(&vmw[i].physAddrRes);
                iounmap((char*)vmw[i].virtAddr);
                memset(&vmw[i], 0, sizeof(vmeMasterWindow_t));
                return (GEF_STATUS_DRIVER_ERR);
            }

                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeCreateMasterWindow: Exit\n");

            return (GEF_SUCCESS);
        }
                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeCreateMasterWindow: WARNING: Windows %d not available\n", i);
    }

                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeCreateMasterWindow: Exit: No memory\n");

    return(GEF_STATUS_NO_MEM);
}

int
tempeCreateSlaveWindow(GEF_VME_DRV_CREATE_SLAVE_WINDOW_INFO *slave_info, GEF_UINT32 ctl, GEF_UINT32 *window)
{
    tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
    GEF_UINT32 tempCtl = 0;
    GEF_UINT32 i = 0;
    vmeInWindowCfg_t vmeIn;
    int status = GEF_SUCCESS;

    memset(&vmeIn, 0, sizeof(vmeInWindowCfg_t));
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeCreateSlaveWindow: Entry\n");
    
    if (NULL == vmechip_baseaddr) {
        return(GEF_STATUS_DEVICE_NOT_INIT);
    }
    
    /* Look for a window that is not already enabled
    */
    for (i=0; i< VME_SLAVE_WINDOWS; i++) {
        tempCtl = longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].itat));
        if(!(tempCtl & TSI148_LCSR_ITAT_EN)) {
            
            *window = i;

            if(vmeAllocSlaveMemory(slave_info->size, *window, slave_info->addr.lower, slave_info->addr.upper) == GEF_SUCCESS)
            {
                
                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeCreateSlaveWindow: Window [%d], Physical RAM [0x%p], Virtual RAM [0x%p]\n", 
                    *window, vsw[*window].physAddr, vsw[*window].virtAddr);
                
                status = ConvCAPIEnumToDrvEnumForSlave(slave_info, &vmeIn, *window);
                if(status != GEF_SUCCESS)
                {
                    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeCreateSlaveWindow: ConvCAPIEnumToDrvEnum() not successful\n");
                    vmeFreeSlaveMemory(*window);
                    return (status);
                }

                if(tempeSetInBound(&vmeIn) != 0)
                {
                    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeCreateSlaveWindow: tempeSetInBound() not successful...check alignment\n");
                    vmeFreeSlaveMemory(*window);
                    return (GEF_STATUS_DRIVER_ERR);
                }


                
                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeCreateSlaveWindow: Exit\n");
                
                return (GEF_SUCCESS);
            }
            else
            {
#ifdef USE_RTOS
              rtlogRecord_t logRecord;
              RTLOG_ERROR("gefvme_rtos", "tempeCreateSlaveWindow: ERROR: Failed "
               "to allocate slave memeory");
#else
                printk("tempeCreateSlaveWindow: ERROR: Failed to allocate slave memeory\n");
#endif
           return (GEF_STATUS_NO_MEM);
            }
        }
    }
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeCreateSlaveWindow: Exit: No windows available\n");
    
    return(GEF_STATUS_NO_MEM);
}

int
tempeFillinMasterAttributes(GEF_VME_DRV_CREATE_MASTER_WINDOW_INFO *master_info, GEF_UINT32 *ctl)
{
    int status = 0;
    vmeOutWindowCfg_t vmeOut;

    memset(&vmeOut, 0, sizeof(vmeOutWindowCfg_t));

    status = ConvCAPIEnumToDrvEnumForMaster(master_info, &vmeOut, -1, master_info->size);
    if(status != GEF_SUCCESS)
    {
        return (status);
    }

    *ctl = tempeSetupMasterAttribute(vmeOut.addrSpace, 
                                     vmeOut.userAccessType, 
                                     vmeOut.dataAccessType,
                                     vmeOut.maxDataWidth,
                                     vmeOut.xferProtocol,
                                     vmeOut.xferRate2esst);

    *ctl |= TEMPE_OTATx_EN;

    return (GEF_SUCCESS);
}

int
tempeFillinSlaveAttributes(GEF_VME_DRV_CREATE_SLAVE_WINDOW_INFO *slave_info, GEF_UINT32 *ctl)
{
    int status = 0;
    vmeInWindowCfg_t vmeIn;

    memset(&vmeIn, 0, sizeof(vmeInWindowCfg_t));

    status = ConvCAPIEnumToDrvEnumForSlave(slave_info, &vmeIn, -1);
    if(status != GEF_SUCCESS)
    {
        return (status);
    }

    *ctl = TEMPE_ITATx_EN;

    /* address space */
    switch(slave_info->addr.addr_space)
    {
    case GEF_VME_ADDR_SPACE_A16: *ctl |= TEMPE_ITATx_AS_A16; break;
    case GEF_VME_ADDR_SPACE_A24: *ctl |= TEMPE_ITATx_AS_A24; break;
    case GEF_VME_ADDR_SPACE_A32: *ctl |= TEMPE_ITATx_AS_A32; break;
    case GEF_VME_ADDR_SPACE_A64: *ctl |= TEMPE_ITATx_AS_A64; break;
    case GEF_VME_ADDR_SPACE_CRCSR:
    case GEF_VME_ADDR_SPACE_USER1:
    case GEF_VME_ADDR_SPACE_USER2:
    case GEF_VME_ADDR_SPACE_USER3:
    case GEF_VME_ADDR_SPACE_USER4:
    default:
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeFillinSlaveAttributes: Invalid address space 0x%x\n", slave_info->addr.addr_space);
        return (GEF_STATUS_VME_INVALID_ADDRESS_SPACE);
    }

    if( (slave_info->addr.addr_mode & GEF_VME_ADDR_MODE_USER) == GEF_VME_ADDR_MODE_USER)
    {
        *ctl |= TEMPE_ITATx_NPRIV;
    }

    if( (slave_info->addr.addr_mode & GEF_VME_ADDR_MODE_SUPER) == GEF_VME_ADDR_MODE_SUPER)
    {
        *ctl |= TEMPE_ITATx_SUPR;
    }

    if( (slave_info->addr.addr_mode & GEF_VME_ADDR_MODE_DATA) == GEF_VME_ADDR_MODE_DATA)
    {
        *ctl |= TEMPE_ITATx_DATA;
    }

    if( (slave_info->addr.addr_mode & GEF_VME_ADDR_MODE_PROGRAM) == GEF_VME_ADDR_MODE_PROGRAM)
    {
        *ctl |= TEMPE_ITATx_PGM;
    }

    return (GEF_SUCCESS);
}

int
tempeReleaseWindow(int window)
{
    tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
    GEF_UINT32 tempCtl = 0;
    GEF_UINT32 irqflags = 0;
    
    if (NULL == vmechip_baseaddr) {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeReleaseWindow: Exist - device not initialized\n");
        return(GEF_STATUS_DEVICE_NOT_INIT);
    }
    
#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
    spin_lock_irqsave (&tempe_lock, irqflags);
#endif
    
    iowrite32(longswap(tempCtl),&pTempe->lcsr.outboundTranslation[window].otat);
    iowrite32(longswap(tempCtl),&pTempe->lcsr.outboundTranslation[window].otsal);
    /* add lsiStartAU when 64bit supported */
    iowrite32(longswap(tempCtl),&pTempe->lcsr.outboundTranslation[window].oteal);
    /* add lsiEndAU when 64bit supported */
    iowrite32(longswap(tempCtl),&pTempe->lcsr.outboundTranslation[window].otofl);
    /* add lsiOffsetU when 64bit supported */
    vmeSyncData();
    
#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
    spin_unlock_irqrestore (&tempe_lock, irqflags);
#endif

    return (GEF_SUCCESS);
}

int
tempeReleaseSlaveWindow(int window)
{
    tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
    GEF_UINT32 tempCtl = 0;
    GEF_UINT32 irqflags = 0;
      
    if (NULL == vmechip_baseaddr) {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeReleaseSlaveWindow: Exist - device not initialized\n");
        return(GEF_STATUS_DEVICE_NOT_INIT);
    }
    
#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
    spin_lock_irqsave (&tempe_lock, irqflags);
#endif
    
    iowrite32(longswap(tempCtl),&pTempe->lcsr.inboundTranslation[window].itat);
    iowrite32(longswap(tempCtl),&pTempe->lcsr.inboundTranslation[window].itsal);
    iowrite32(longswap(tempCtl),&pTempe->lcsr.inboundTranslation[window].itsau);
    iowrite32(longswap(tempCtl),&pTempe->lcsr.inboundTranslation[window].iteal);
    iowrite32(longswap(tempCtl),&pTempe->lcsr.inboundTranslation[window].iteau);
    iowrite32(longswap(tempCtl),&pTempe->lcsr.inboundTranslation[window].itofl);
    iowrite32(longswap(tempCtl),&pTempe->lcsr.inboundTranslation[window].itofu);
    vmeSyncData();
    
#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
    spin_unlock_irqrestore (&tempe_lock, irqflags);
#endif

    return (GEF_SUCCESS);
}

int
tempeTransMasterWindow(GEF_VME_DRV_TRANSLATE_MASTER_WINDOW_INFO *transMasterInfo, vmeMasterWindow_t *masterWindow)
{
    tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
    GEF_UINT32 irqflags = 0;
    GEF_UINT32 tol = 0;
    GEF_UINT32 tou = 0;
    GEF_UINT32 baseL = 0;
    GEF_UINT32 baseU = 0; /* add for 64-bit */
    GEF_UINT32 vme_addrL = 0;
    GEF_UINT32 vme_addrU = 0;
    GEF_UINT32 size = masterWindow->size;
    GEF_UINT32 off = 0;
    GEF_UINT32 resolution = 0;
    vmeMasterHandle_t *handle = (vmeMasterHandle_t *)(GEF_OS_CAST)transMasterInfo->master_osspec_hdl;
    
    if (NULL == vmechip_baseaddr) {
        return(GEF_STATUS_DEVICE_NOT_INIT);
    }

    if(NULL == masterWindow) {
        return(GEF_STATUS_DRIVER_ERR);
    }

    if(handle == NULL) {
        return(GEF_STATUS_DRIVER_ERR);
    }

    vme_addrL = transMasterInfo->addr.lower;
    vme_addrU = transMasterInfo->addr.upper;
    baseL = (GEF_UINT32)masterWindow->physAddr;
    tol = vme_addrL - baseL;
    tou = sub64hi(vme_addrL, vme_addrU, baseL, baseU);

    resolution = 0x10000; /* all windows 64k */
    off = vme_addrL % resolution;                                  /* master window offset */
    size += off;
    size += (size % resolution) ? resolution - (size % resolution) : 0; /* adjust window size */
    
#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
    spin_lock_irqsave (&tempe_lock, irqflags);
#endif
    
    iowrite32(longswap(tol),&pTempe->lcsr.outboundTranslation[masterWindow->number].otofl);
    iowrite32(longswap(tou),&pTempe->lcsr.outboundTranslation[masterWindow->number].otofu);

    masterWindow->vmeL = transMasterInfo->addr.lower;
    masterWindow->vmeU = transMasterInfo->addr.upper;
    handle->offset = off;

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Translated master window #%d: ctl = %08x\n", masterWindow->number,longswap(pTempe->lcsr.outboundTranslation[masterWindow->number].otat));
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "otbs %08x\n", longswap(ioread32(&pTempe->lcsr.outboundTranslation[masterWindow->number].otbs)));
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "otsal %08x\n", longswap(ioread32(&pTempe->lcsr.outboundTranslation[masterWindow->number].otsal)));
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "otsau %08x\n", longswap(ioread32(&pTempe->lcsr.outboundTranslation[masterWindow->number].otsau)));
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "oteal %08x\n", longswap(ioread32(&pTempe->lcsr.outboundTranslation[masterWindow->number].oteal)));
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "oteau %08x\n", longswap(ioread32(&pTempe->lcsr.outboundTranslation[masterWindow->number].oteau)));
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "otofl %08x\n", longswap(ioread32(&pTempe->lcsr.outboundTranslation[masterWindow->number].otofl)));
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "otofu %08x\n", longswap(ioread32(&pTempe->lcsr.outboundTranslation[masterWindow->number].otofu)));
    
#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
    spin_unlock_irqrestore (&tempe_lock, irqflags);
#endif

    return(GEF_SUCCESS);
}

GEF_STATUS 
tempeGetTimeOnBus(GEF_VME_DRV_TIME_ON_BUS_INFO *time_on_info)
{
    GEF_UINT32 tempCtl = 0;

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "--> tempeGetTimeOnBus.\n");
   
    tempCtl = longswap(GEF_IOREAD32(TEMPE_VMCTRL));
    
    tempCtl &= TEMPE_VMCTRL_VTON_MASK;
    
    switch(tempCtl)
    {
    case TEMPE_VMCTRL_VTON_4us_128byte   : time_on_info->time_on = GEF_VME_TIME_ON_BUS_4us  ; break;   
    case TEMPE_VMCTRL_VTON_8us_128byte   : time_on_info->time_on = GEF_VME_TIME_ON_BUS_8us  ; break;      
    case TEMPE_VMCTRL_VTON_16us_128byte  : time_on_info->time_on = GEF_VME_TIME_ON_BUS_16us ; break;     
    case TEMPE_VMCTRL_VTON_32us_256byte  : time_on_info->time_on = GEF_VME_TIME_ON_BUS_32us ; break;     
    case TEMPE_VMCTRL_VTON_64us_512byte  : time_on_info->time_on = GEF_VME_TIME_ON_BUS_64us ; break;     
    case TEMPE_VMCTRL_VTON_128us_1024byte: time_on_info->time_on = GEF_VME_TIME_ON_BUS_128us; break;   
    case TEMPE_VMCTRL_VTON_256us_2048byte: time_on_info->time_on = GEF_VME_TIME_ON_BUS_256us; break;   
    case TEMPE_VMCTRL_VTON_512us_4196byte: time_on_info->time_on = GEF_VME_TIME_ON_BUS_512us; break;   

    default: 
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "<-- tempeGetTimeOnBus: Error unknown time on: VMCTRL 0x%08x\n", longswap(GEF_IOREAD32(TEMPE_VMCTRL)));
        return(GEF_VME_SET_ERROR(GEF_STATUS_DRIVER_ERR));
        break;
    }

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "<-- tempeGetTimeOnBus.\n");
    return GEF_SUCCESS;
}

GEF_STATUS 
tempeSetTimeOnBus(GEF_VME_DRV_TIME_ON_BUS_INFO *time_on_info)  
{
    GEF_UINT32 tempCtl = 0;
    GEF_UINT32 irqflags = 0;

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "--> tempeSetTimeOnBus.\n");
    /* validate parameters */
    switch(time_on_info->time_on)
    {
    case GEF_VME_TIME_ON_BUS_4us  :
    case GEF_VME_TIME_ON_BUS_8us  :
    case GEF_VME_TIME_ON_BUS_16us :
    case GEF_VME_TIME_ON_BUS_32us :
    case GEF_VME_TIME_ON_BUS_64us :
    case GEF_VME_TIME_ON_BUS_128us:
    case GEF_VME_TIME_ON_BUS_256us:
    case GEF_VME_TIME_ON_BUS_512us:
        break;
    default: 
        GEF_VME_DEBUG(GEF_DEBUG_ERROR, "<-- tempeSetTimeOnBus: Error unsupported time on %d\n", time_on_info->time_on);
        return(GEF_VME_SET_ERROR(GEF_STATUS_NOT_SUPPORTED));
    }
    
#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
    spin_lock_irqsave (&tempe_lock, irqflags);
#endif
    
    tempCtl = longswap(GEF_IOREAD32(TEMPE_VMCTRL));
    
    tempCtl = (tempCtl & ~(TEMPE_VMCTRL_VTON_MASK));
    
    switch(time_on_info->time_on)
    {
    case GEF_VME_TIME_ON_BUS_4us  : tempCtl |= TEMPE_VMCTRL_VTON_4us_128byte   ; break;   
    case GEF_VME_TIME_ON_BUS_8us  : tempCtl |= TEMPE_VMCTRL_VTON_8us_128byte   ; break;      
    case GEF_VME_TIME_ON_BUS_16us : tempCtl |= TEMPE_VMCTRL_VTON_16us_128byte  ; break;     
    case GEF_VME_TIME_ON_BUS_32us : tempCtl |= TEMPE_VMCTRL_VTON_32us_256byte  ; break;     
    case GEF_VME_TIME_ON_BUS_64us : tempCtl |= TEMPE_VMCTRL_VTON_64us_512byte  ; break;     
    case GEF_VME_TIME_ON_BUS_128us: tempCtl |= TEMPE_VMCTRL_VTON_128us_1024byte; break;   
    case GEF_VME_TIME_ON_BUS_256us: tempCtl |= TEMPE_VMCTRL_VTON_256us_2048byte; break;   
    case GEF_VME_TIME_ON_BUS_512us: tempCtl |= TEMPE_VMCTRL_VTON_512us_4196byte; break;   
    default: break;
    }
    
    GEF_IOWRITE32(longswap(tempCtl),TEMPE_VMCTRL);
    vmeSyncData();
    
#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
    spin_unlock_irqrestore (&tempe_lock, irqflags);
#endif

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "<-- tempeSetTimeOnBus.\n");
    return GEF_SUCCESS;
}

GEF_STATUS 
tempeGetTimeOffBus(GEF_VME_DRV_TIME_OFF_BUS_INFO *time_off_info)
{
    GEF_UINT32 tempCtl = 0;

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "--> tempeGetTimeOffBus.\n");
    tempCtl = longswap(GEF_IOREAD32(TEMPE_VMCTRL));

    tempCtl &= TEMPE_VMCTRL_VTOFF_MASK;

    switch(tempCtl)
    {
    case TEMPE_VMCTRL_VTOFF_0us : time_off_info->time_off = GEF_VME_TIME_OFF_BUS_0us ; break;   
    case TEMPE_VMCTRL_VTOFF_1us : time_off_info->time_off = GEF_VME_TIME_OFF_BUS_1us ; break;      
    case TEMPE_VMCTRL_VTOFF_2us : time_off_info->time_off = GEF_VME_TIME_OFF_BUS_2us ; break;     
    case TEMPE_VMCTRL_VTOFF_4us : time_off_info->time_off = GEF_VME_TIME_OFF_BUS_4us ; break;     
    case TEMPE_VMCTRL_VTOFF_8us : time_off_info->time_off = GEF_VME_TIME_OFF_BUS_8us ; break;     
    case TEMPE_VMCTRL_VTOFF_16us: time_off_info->time_off = GEF_VME_TIME_OFF_BUS_16us; break;   
    case TEMPE_VMCTRL_VTOFF_32us: time_off_info->time_off = GEF_VME_TIME_OFF_BUS_32us; break;   
    case TEMPE_VMCTRL_VTOFF_64us: time_off_info->time_off = GEF_VME_TIME_OFF_BUS_64us; break;   

    default: 
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "<-- tempeGetTimeOffBus: Error unknown time off: VMCTRL 0x%08x\n", longswap(GEF_IOREAD32(TEMPE_VMCTRL)));
        return(GEF_VME_SET_ERROR(GEF_STATUS_DRIVER_ERR));
        break;
    }

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "<-- tempeGetTimeOffBus.\n");
    return GEF_SUCCESS;
}

GEF_STATUS 
tempeSetTimeOffBus(GEF_VME_DRV_TIME_OFF_BUS_INFO *time_off_info)
{
    GEF_UINT32 tempCtl = 0;
    GEF_UINT32 irqflags = 0;

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "--> tempeSetTimeOffBus.\n");

    /* validate parameters */
    switch(time_off_info->time_off)
    {
    case GEF_VME_TIME_OFF_BUS_0us :
    case GEF_VME_TIME_OFF_BUS_1us :
    case GEF_VME_TIME_OFF_BUS_2us :
    case GEF_VME_TIME_OFF_BUS_4us :
    case GEF_VME_TIME_OFF_BUS_8us :
    case GEF_VME_TIME_OFF_BUS_16us:
    case GEF_VME_TIME_OFF_BUS_32us:
    case GEF_VME_TIME_OFF_BUS_64us:
        break;
    default: 
        GEF_VME_DEBUG(GEF_DEBUG_ERROR, "<-- tempeSetTimeOffBus: Error unsupported time off %d\n", time_off_info->time_off);
        return(GEF_VME_SET_ERROR(GEF_STATUS_NOT_SUPPORTED));
    }
    
#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
    spin_lock_irqsave (&tempe_lock, irqflags);
#endif
    
    tempCtl = longswap(GEF_IOREAD32(TEMPE_VMCTRL));
    
    tempCtl = (tempCtl & ~(TEMPE_VMCTRL_VTOFF_MASK));
    
    switch(time_off_info->time_off)
    {
    case GEF_VME_TIME_OFF_BUS_0us : tempCtl |= TEMPE_VMCTRL_VTOFF_0us ; break;   
    case GEF_VME_TIME_OFF_BUS_1us : tempCtl |= TEMPE_VMCTRL_VTOFF_1us ; break;      
    case GEF_VME_TIME_OFF_BUS_2us : tempCtl |= TEMPE_VMCTRL_VTOFF_2us ; break;     
    case GEF_VME_TIME_OFF_BUS_4us : tempCtl |= TEMPE_VMCTRL_VTOFF_4us ; break;     
    case GEF_VME_TIME_OFF_BUS_8us : tempCtl |= TEMPE_VMCTRL_VTOFF_8us ; break;     
    case GEF_VME_TIME_OFF_BUS_16us: tempCtl |= TEMPE_VMCTRL_VTOFF_16us; break;   
    case GEF_VME_TIME_OFF_BUS_32us: tempCtl |= TEMPE_VMCTRL_VTOFF_32us; break;   
    case GEF_VME_TIME_OFF_BUS_64us: tempCtl |= TEMPE_VMCTRL_VTOFF_64us; break;   
    default: break;
    }
    
    GEF_IOWRITE32(longswap(tempCtl), TEMPE_VMCTRL);
    vmeSyncData();
    
#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
    spin_unlock_irqrestore (&tempe_lock, irqflags);
#endif

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "<-- tempeSetTimeOffBus.\n");
    return GEF_SUCCESS;
}

//-----------------------------------------------------------------------------
// Function   : tempeSetInBound
// Description: Initialize an inbound window with the requested attributes.
//-----------------------------------------------------------------------------
int
tempeSetInBound( vmeInWindowCfg_t *vmeIn)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  int tempCtl = 0;
  unsigned int i,x, granularity = 0x10000;
  unsigned long irqflags;

  if (NULL == vmechip_baseaddr) {
      return(-EINVAL);
  }

  // Verify input data
  if(vmeIn->windowNbr > 7){
    return(-EINVAL);
  }
  i = vmeIn->windowNbr;

  switch(vmeIn->addrSpace){
    case VME_CRCSR:
    case VME_USER1:
    case VME_USER2:
    case VME_USER3:
    case VME_USER4:
      return(-EINVAL);
    case VME_A16:
      granularity = 0x10;
      tempCtl |= 0x00;
      break;
    case VME_A24:
      granularity = 0x1000;
      tempCtl |= 0x10;
      break;
    case VME_A32:
      granularity = 0x10000;
      tempCtl |= 0x20;
      break;
    case VME_A64:
      granularity = 0x10000;
      tempCtl |= 0x40;
      break;
  }

  if((vmeIn->vmeAddrL & (granularity - 1)) ||
     (vmeIn->windowSizeL & (granularity - 1)) ||
     (vmeIn->pciAddrL & (granularity - 1))){
    return(-EINVAL);
  }

#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
  spin_lock_irqsave (&tempe_lock, irqflags);
#endif

  // Disable while we are modifying
  iowrite32(0,&pTempe->lcsr.inboundTranslation[i].itat);
  vmeSyncData();

  iowrite32(longswap(vmeIn->vmeAddrL),&pTempe->lcsr.inboundTranslation[i].itsal);
  iowrite32(longswap(vmeIn->vmeAddrU),&pTempe->lcsr.inboundTranslation[i].itsau);

  iowrite32(longswap((vmeIn->vmeAddrL + vmeIn->windowSizeL-granularity)),
                        &pTempe->lcsr.inboundTranslation[i].iteal);

  iowrite32(longswap(add64hi(
            vmeIn->vmeAddrL, vmeIn->vmeAddrU, 
            vmeIn->windowSizeL-granularity, vmeIn->windowSizeU)),
            &pTempe->lcsr.inboundTranslation[i].iteau); 

  iowrite32(longswap((vmeIn->pciAddrL - vmeIn->vmeAddrL)),&pTempe->lcsr.inboundTranslation[i].itofl);

  iowrite32(longswap(sub64hi(
            vmeIn->pciAddrL, vmeIn->pciAddrU, vmeIn->vmeAddrL, vmeIn->vmeAddrU)),
            &pTempe->lcsr.inboundTranslation[i].itofu);

  // Setup CTL register.
  tempCtl |= (vmeIn->xferProtocol & 0x3E) << 6;
  for(x = 0; x < 4; x++){
    if((64 << x) >= vmeIn->prefetchSize){
       break;
    }
  }
  if(x == 4) x--;
  tempCtl |= (x << 16);

  if(vmeIn->prefetchThreshold)
    tempCtl |= 0x40000;

  switch(vmeIn->xferRate2esst){
    case VME_SSTNONE:
      break;
    case VME_SST160:
      tempCtl |= 0x0400;
      break;
    case VME_SST267:
      tempCtl |= 0x1400;
      break;
    case VME_SST320:
      tempCtl |= 0x2400;
      break;
  }

  if(vmeIn->userAccessType & VME_USER)
    tempCtl |= 0x4;
  if(vmeIn->userAccessType & VME_SUPER)
    tempCtl |= 0x8;
  if(vmeIn->dataAccessType & VME_DATA)
    tempCtl |= 0x1;
  if(vmeIn->dataAccessType & VME_PROG)
    tempCtl |= 0x2;

  // Write ctl reg without enable
  iowrite32(longswap(tempCtl),&pTempe->lcsr.inboundTranslation[i].itat);
  vmeSyncData();

  if(vmeIn->windowEnable)
    tempCtl |= 0x80000000;
 
  iowrite32(longswap(tempCtl),&pTempe->lcsr.inboundTranslation[i].itat);
  vmeSyncData();

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Configuring slave window #%d: ctl = %08x\n", i,longswap(pTempe->lcsr.inboundTranslation[i].itat));
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "itsal %08x\n", longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].itsal)));
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "itsau %08x\n", longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].itsau)));
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "iteal %08x\n", longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].iteal)));
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "iteau %08x\n", longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].iteau)));
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "itofl %08x\n", longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].itofl)));
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "itofu %08x\n", longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].itofu)));

#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
  spin_unlock_irqrestore (&tempe_lock, irqflags);
#endif

  return(0);
}

//-----------------------------------------------------------------------------
// Function   : tempeGetInBound
// Description: Return the attributes of an inbound window.
//-----------------------------------------------------------------------------
int
tempeGetInBound( vmeInWindowCfg_t *vmeIn)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  int tempCtl = 0;
  unsigned int i, vmeEndU, vmeEndL;
  unsigned long irqflags;

  // Verify input data
  if(vmeIn->windowNbr > 7){
    return(-EINVAL);
  }

#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
    spin_lock_irqsave (&tempe_lock, irqflags);
#endif

  i = vmeIn->windowNbr;

  tempCtl = longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].itat));

  // Get Control & BUS attributes
  if(tempCtl & 0x80000000)
     vmeIn->windowEnable = 1;
  vmeIn->xferProtocol = ((tempCtl & 0xF8) >> 6) | VME_SCT;
  vmeIn->prefetchSize = 64 << ((tempCtl >> 16) & 3);
  vmeIn->wrPostEnable = 1;
  vmeIn->prefetchEnable = 1;
  if(tempCtl & 0x40000)
     vmeIn->prefetchThreshold = 1;
  if(tempCtl & 0x4)
    vmeIn->userAccessType |= VME_USER;
  if(tempCtl & 0x8)
    vmeIn->userAccessType |= VME_SUPER;
  if(tempCtl & 0x1)
    vmeIn->dataAccessType |= VME_DATA;
  if(tempCtl & 0x2)
    vmeIn->dataAccessType |= VME_PROG;

  switch((tempCtl & 0x70) >> 4) {
    case 0x0:
      vmeIn->addrSpace = VME_A16;
      break;
    case 0x1:
      vmeIn->addrSpace = VME_A24;
      break;
    case 0x2:
      vmeIn->addrSpace = VME_A32;
      break;
    case 0x4:
      vmeIn->addrSpace = VME_A64;
      break;
  }

  switch((tempCtl & 0x7000) >> 12) {
    case 0x0:
      vmeIn->xferRate2esst = VME_SST160;
      break;
    case 0x1:
      vmeIn->xferRate2esst = VME_SST267;
      break;
    case 0x2:
      vmeIn->xferRate2esst = VME_SST320;
      break;
  }

  // Get VME inbound start & end addresses
  vmeIn->vmeAddrL =  longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].itsal));
  vmeIn->vmeAddrU =  longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].itsau));
  vmeEndL =  longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].iteal));
  vmeEndU =  longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].iteau));

  // Adjust addresses for window granularity
  switch(vmeIn->addrSpace){
      case VME_A16:
          vmeIn->vmeAddrU = 0;
          vmeIn->vmeAddrL &= 0x0000FFF0;
          vmeEndU =  0;
          vmeEndL &=  0x0000FFF0;
          vmeEndL |= 0x10;
      break;
      case VME_A24:
          vmeIn->vmeAddrU = 0;
          vmeIn->vmeAddrL &= 0x00FFF000;
          vmeEndU =  0;
          vmeEndL &=  0x00FFF000;
          vmeEndL |= 0x1000;
      break;
      case VME_A32:
          vmeIn->vmeAddrU = 0;
          vmeIn->vmeAddrL &= 0xFFFF0000;
          vmeEndU =  0;
          vmeEndL &=  0xFFFF0000;
          vmeEndL |= 0x1000;
      default:
          vmeIn->vmeAddrL &= 0xFFFF0000;
          vmeEndL &=  0xFFFF0000;
          vmeEndL |= 0x10000;
      break;
  }

  // Calculate size of window.
  vmeIn->windowSizeL = vmeEndL - vmeIn->vmeAddrL;
  vmeIn->windowSizeU = sub64hi(
                         vmeEndL, vmeEndU,
                         vmeIn->vmeAddrL, vmeIn->vmeAddrU
                       );

  // Calculate coorelating PCI bus address
  vmeIn->pciAddrL = vmeIn->vmeAddrL + longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].itofl));
  vmeIn->pciAddrU = add64hi(
                         vmeIn->vmeAddrL, vmeIn->vmeAddrU,
                         longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].itofl)),
                         longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].itofu))
                       );

#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
  spin_unlock_irqrestore (&tempe_lock, irqflags);
#endif

  return(0);
}

//-----------------------------------------------------------------------------
// Function   : tempeSetOutBound
// Description: Set the attributes of an outbound window.
//-----------------------------------------------------------------------------
int
tempeSetOutBound( vmeOutWindowCfg_t *vmeOut)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  unsigned int tempCtl = 0;
  unsigned int i,x;
  unsigned long irqflags;

  if (NULL == vmechip_baseaddr) {
      return(-EINVAL);
  }

  // Verify input data
  if(vmeOut->windowNbr > 7){
    return(-EINVAL);
  }
  i = vmeOut->windowNbr;

#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
  spin_lock_irqsave (&tempe_lock, irqflags);
#endif

  tempCtl = tempeSetupMasterAttribute(
                         vmeOut->addrSpace, 
                         vmeOut->userAccessType, 
                         vmeOut->dataAccessType,
                         vmeOut->maxDataWidth,
                         vmeOut->xferProtocol,
                         vmeOut->xferRate2esst
             );

  if(vmeOut->prefetchEnable){
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeSetOutBound: Read prefetch enable set for window %d\n", i);
    tempCtl |= 0x40000;
    for(x = 0; x < 4; x++){
      if((2 << x) >= vmeOut->prefetchSize) break;
    }
    if(x == 4) x=3;
    tempCtl |= (x << 16);
  }

  // Disable while we are modifying
  iowrite32(0,&pTempe->lcsr.outboundTranslation[i].otat);
  vmeSyncData();

  iowrite32(longswap(vmeOut->bcastSelect2esst),&pTempe->lcsr.outboundTranslation[i].otbs);
  iowrite32(longswap(vmeOut->pciBusAddrL),&pTempe->lcsr.outboundTranslation[i].otsal);
  iowrite32(longswap(vmeOut->pciBusAddrU),&pTempe->lcsr.outboundTranslation[i].otsau);
#ifdef PPC
  iowrite32(longswap((vmeOut->pciBusAddrL + (vmeOut->windowSizeL-0x10000))),
       &pTempe->lcsr.outboundTranslation[i].oteal);
  iowrite32(longswap(add64hi(
       vmeOut->pciBusAddrL, vmeOut->pciBusAddrU, 
       vmeOut->windowSizeL-0x10000, vmeOut->windowSizeU)),
       &pTempe->lcsr.outboundTranslation[i].oteau);
#else
  iowrite32(longswap((vmeOut->pciBusAddrL + (vmeOut->windowSizeL-0x10000))),
       &pTempe->lcsr.outboundTranslation[i].oteal);
  iowrite32(longswap(add64hi(
       vmeOut->pciBusAddrL, vmeOut->pciBusAddrU, 
       vmeOut->windowSizeL-0x10000, vmeOut->windowSizeU)),
       &pTempe->lcsr.outboundTranslation[i].oteau);
#endif
  iowrite32(longswap((vmeOut->xlatedAddrL - vmeOut->pciBusAddrL)),
       &pTempe->lcsr.outboundTranslation[i].otofl);

  iowrite32(longswap(sub64hi(
      vmeOut->xlatedAddrL, vmeOut->xlatedAddrU,
      vmeOut->pciBusAddrL, vmeOut->pciBusAddrU)),&pTempe->lcsr.outboundTranslation[i].otofu);

  // Write ctl reg without enable
  iowrite32(longswap(tempCtl),&pTempe->lcsr.outboundTranslation[i].otat);
  vmeSyncData();

  if(vmeOut->windowEnable)
    tempCtl |= 0x80000000;
 
  iowrite32(longswap(tempCtl),&pTempe->lcsr.outboundTranslation[i].otat);

  vmeSyncData();

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Configuring master window #%d: ctl = %08x\n", i,longswap(pTempe->lcsr.outboundTranslation[i].otat));
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "otbs %08x\n", longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otbs)));
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "otsal %08x\n", longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otsal)));
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "otsau %08x\n", longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otsau)));
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "oteal %08x\n", longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].oteal)));
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "oteau %08x\n", longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].oteau)));
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "otofl %08x\n", longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otofl)));
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "otofu %08x\n", longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otofu)));

#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
  spin_unlock_irqrestore (&tempe_lock, irqflags);
#endif

  return(0);
}


//-----------------------------------------------------------------------------
// Function   : tempeGetOutBound
// Description: Return the attributes of an outbound window.
//-----------------------------------------------------------------------------
int
tempeGetOutBound( vmeOutWindowCfg_t *vmeOut)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  int tempCtl = 0;
  unsigned int i;
  unsigned long irqflags;

  // Verify input data
  if(vmeOut->windowNbr > 7){
    return(-EINVAL);
  }
  i = vmeOut->windowNbr;

#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
  spin_lock_irqsave (&tempe_lock, irqflags);
#endif

  // Get Control & BUS attributes
  tempCtl = longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otat));
  vmeOut->wrPostEnable = 1;
  if(tempCtl & 0x0020)
    vmeOut->userAccessType = VME_SUPER;
  else
    vmeOut->userAccessType = VME_USER;
  if(tempCtl & 0x0010)
    vmeOut->dataAccessType = VME_PROG;
  else 
    vmeOut->dataAccessType = VME_DATA;
  if(tempCtl & 0x80000000)
     vmeOut->windowEnable = 1;

  switch((tempCtl & 0xC0) >> 6){
    case 0:
      vmeOut->maxDataWidth = VME_D16;
      break;
    case 1:
      vmeOut->maxDataWidth = VME_D32;
      break;
  }
  vmeOut->xferProtocol = 1 << ((tempCtl >> 8) & 7);

  switch(tempCtl & 0xF){
    case 0x0:
      vmeOut->addrSpace = VME_A16;
      break;
    case 0x1:
      vmeOut->addrSpace = VME_A24;
      break;
    case 0x2:
      vmeOut->addrSpace = VME_A32;
      break;
    case 0x4:
      vmeOut->addrSpace = VME_A64;
      break;
    case 0x5:
      vmeOut->addrSpace = VME_CRCSR;
      break;
    case 0x8:
      vmeOut->addrSpace = VME_USER1;
      break;
    case 0x9:
      vmeOut->addrSpace = VME_USER2;
      break;
    case 0xA:
      vmeOut->addrSpace = VME_USER3;
      break;
    case 0xB:
      vmeOut->addrSpace = VME_USER4;
      break;
  }

  vmeOut->xferRate2esst = VME_SSTNONE;
  if(vmeOut->xferProtocol == VME_2eSST){

    switch(tempCtl & 0x1800){
      case 0x000:
        vmeOut->xferRate2esst = VME_SST160;
        break;
      case 0x800:
        vmeOut->xferRate2esst = VME_SST267;
        break;
      case 0x1000:
        vmeOut->xferRate2esst = VME_SST320;
        break;
    }

  }

  // Get Window mappings.

  vmeOut->bcastSelect2esst = longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otbs));
  vmeOut->pciBusAddrL = longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otsal));
  vmeOut->pciBusAddrU = longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otsau));

#ifdef PPC
  vmeOut->windowSizeL =
      (longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].oteal)) + 0x10000) - 
                             vmeOut->pciBusAddrL;
  vmeOut->windowSizeU = sub64hi(
       longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].oteal)) + 0x10000,
       longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].oteau)),
       vmeOut->pciBusAddrL, 
       vmeOut->pciBusAddrU); 
#else
  vmeOut->windowSizeL =
      (longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].oteal))) - 
                             vmeOut->pciBusAddrL;
  vmeOut->windowSizeU = sub64hi(
       longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].oteal)),
       longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].oteau)),
       vmeOut->pciBusAddrL, 
       vmeOut->pciBusAddrU); 
#endif

  vmeOut->xlatedAddrL = longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otofl)) + 
       vmeOut->pciBusAddrL;
  vmeOut->xlatedAddrU = add64hi(
         longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otofl)),
         longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otofu)),
         vmeOut->pciBusAddrL, 
         vmeOut->pciBusAddrU); 

#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
    spin_unlock_irqrestore (&tempe_lock, irqflags);
#endif

  return(0);
}


//-----------------------------------------------------------------------------
// Function   : tempeSetupLm
// Description: Set the attributes of the location monitor
//-----------------------------------------------------------------------------
int
tempeSetupLm( vmeLmCfg_t *vmeLm)
{
    tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
    int tempCtl = TEMPE_LMAT_EN;
    unsigned long irqflags;
    int winCtl = 0;

    if (NULL == vmechip_baseaddr) {
        return(-EINVAL);
    }
    
    // Setup CTL register.
    switch(vmeLm->addrSpace){
    case VME_A16:
        tempCtl |= 0x00;
        break;
    case VME_A24:
        tempCtl |= 0x10;
        break;
    case VME_A32:
        tempCtl |= 0x20;
        break;
    case VME_A64:
        tempCtl |= 0x40;
        break;
    default:
        return(-EINVAL);
    }
    
    if(vmeLm->userAccessType & VME_USER)
        tempCtl |= 0x4;
    if(vmeLm->userAccessType & VME_SUPER)
        tempCtl |= 0x8;
    if(vmeLm->dataAccessType & VME_DATA)
        tempCtl |= 0x1;
    if(vmeLm->dataAccessType & VME_PROG)
        tempCtl |= 0x2;

    winCtl = longswap(ioread32(&pTempe->lcsr.lmat));
    if(TEMPE_LMAT_EN & winCtl) {
        
        tempCtl |= winCtl & (TEMPE_ITATx_CTL__PGM__BOTH |
            TEMPE_ITATx_CTL__SUPER__BOTH);
        winCtl |= tempCtl & (TEMPE_ITATx_CTL__PGM__BOTH |
            TEMPE_ITATx_CTL__SUPER__BOTH);
        
        if ( ( winCtl != tempCtl ) ||
            ( vmeLm->addr != longswap( ioread32( &pTempe->lcsr.lmbal ) ) ) )
        {
            return -EBUSY;
        }
    }
    
#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
    spin_lock_irqsave (&tempe_lock, irqflags);
#endif
    
    // Disable while we are modifying
    iowrite32(0,&pTempe->lcsr.lmat);
    vmeSyncData();
    
    iowrite32(longswap(vmeLm->addr),&pTempe->lcsr.lmbal);
    iowrite32(longswap(vmeLm->addrU),&pTempe->lcsr.lmbau);
    
    tempeLmEvent = 0;
    // Write ctl reg and enable
    iowrite32(longswap(tempCtl),&pTempe->lcsr.lmat);
    vmeSyncData();

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Configuring loc mon window: ctl = %08x\n", longswap(ioread32(&pTempe->lcsr.lmat)));
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "lmbal %08x\n", longswap(ioread32(&pTempe->lcsr.lmbal)));
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "lmbau %08x\n", longswap(ioread32(&pTempe->lcsr.lmbau)));
    
#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
    spin_unlock_irqrestore (&tempe_lock, irqflags);
#endif
    
    return(0);
}

int tempeReleaseLocMonWindow(void)
{
    tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
    GEF_UINT32 irqflags = 0;

    if (NULL == vmechip_baseaddr) {
        return(-EINVAL);
    }

#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
    spin_lock_irqsave(&tempe_lock, irqflags);
#endif
    
    // Disable window
    iowrite32(0x00000000, &pTempe->lcsr.lmat);
    iowrite32(0x00000000, &pTempe->lcsr.lmbal);
    iowrite32(0x00000000, &pTempe->lcsr.lmbau);
    
    vmeSyncData();

#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
    spin_unlock_irqrestore (&tempe_lock, irqflags);
#endif

    return 0;
}

int
tempeQueryLocMonWindow(GEF_VME_DRV_LOCATION_MONITOR_INFO *lmInfo)
{
    tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
    GEF_UINT32 tempCtl = 0;

    if (NULL == vmechip_baseaddr) {
        return(-EINVAL);
    }
    
    tempCtl = longswap(ioread32(&pTempe->lcsr.lmat));
    lmInfo->addr.lower = longswap(ioread32(&pTempe->lcsr.lmbal));
    lmInfo->addr.upper = longswap(ioread32(&pTempe->lcsr.lmbau));

    switch(tempCtl & 0x70)
    {
    case TEMPE_LMAT_AS_A16: lmInfo->addr.addr_space = GEF_VME_ADDR_SPACE_A16; break;
    case TEMPE_LMAT_AS_A24: lmInfo->addr.addr_space = GEF_VME_ADDR_SPACE_A24; break;
    case TEMPE_LMAT_AS_A32: lmInfo->addr.addr_space = GEF_VME_ADDR_SPACE_A32; break;
    case TEMPE_LMAT_AS_A64: lmInfo->addr.addr_space = GEF_VME_ADDR_SPACE_A64; break;
    default: return (GEF_STATUS_VME_INVALID_ADDRESS_SPACE);
    }

    if( (tempCtl & TEMPE_LMAT_NPRIV) == TEMPE_LMAT_NPRIV)
    {
        lmInfo->addr.addr_mode |= GEF_VME_ADDR_MODE_USER;
    }

    if( (tempCtl & TEMPE_LMAT_SUPR) == TEMPE_LMAT_SUPR)
    {
        lmInfo->addr.addr_mode |= GEF_VME_ADDR_MODE_SUPER;
    }

    if( (tempCtl & TEMPE_LMAT_PGM) == TEMPE_LMAT_PGM)
    {
        lmInfo->addr.addr_mode |= GEF_VME_ADDR_MODE_PROGRAM;
    }

    if( (tempCtl & TEMPE_LMAT_DATA) == TEMPE_LMAT_DATA)
    {
        lmInfo->addr.addr_mode |= GEF_VME_ADDR_MODE_DATA;
    }
    
    return 0;
}


//-----------------------------------------------------------------------------
// Function   : tempeSetupVrai
// Description: Set the attributes of the VRAI window (CRGAT)
//-----------------------------------------------------------------------------
int
tempeSetupVrai(GEF_VME_DRV_VRAI_INFO *vraiInfo)
{
    tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
    GEF_VME_ADDR *addr = &(vraiInfo->addr);
    int tempCtl = TSI148_LCSR_CRGAT_EN;
    unsigned long irqflags;
    int winCtl = 0;

    if (NULL == vmechip_baseaddr) {
        return(-EINVAL);
    }

    // Setup CTL register.
    switch(addr->addr_space){
    case GEF_VME_ADDR_SPACE_A16:
        tempCtl |= TSI148_LCSR_CRGAT_AS_16;
        break;
    case GEF_VME_ADDR_SPACE_A24:
        tempCtl |= TSI148_LCSR_CRGAT_AS_24;
        break;
    case GEF_VME_ADDR_SPACE_A32:
        tempCtl |= TSI148_LCSR_CRGAT_AS_32;
        break;
    case GEF_VME_ADDR_SPACE_A64:
        tempCtl |= TSI148_LCSR_CRGAT_AS_64;
        break;
    default:
        return(-EINVAL);
    }

    if(addr->addr_mode & GEF_VME_ADDR_MODE_USER)
        tempCtl |= TSI148_LCSR_CRGAT_NPRIV;
    if(addr->addr_mode & GEF_VME_ADDR_MODE_SUPER)
        tempCtl |= TSI148_LCSR_CRGAT_SUPR;
    if(addr->addr_mode & GEF_VME_ADDR_MODE_DATA)
        tempCtl |= TSI148_LCSR_CRGAT_DATA;
    if(addr->addr_mode & GEF_VME_ADDR_MODE_PROGRAM)
        tempCtl |= TSI148_LCSR_CRGAT_PGM;

#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
    spin_lock_irqsave (&tempe_lock, irqflags);
#endif

    winCtl = longswap(ioread32(&pTempe->lcsr.csrat));
    if(TSI148_LCSR_CRGAT_EN & winCtl) {

        tempCtl |= winCtl & (TEMPE_ITATx_CTL__PGM__BOTH |
            TEMPE_ITATx_CTL__SUPER__BOTH);
        winCtl |= tempCtl & (TEMPE_ITATx_CTL__PGM__BOTH |
            TEMPE_ITATx_CTL__SUPER__BOTH);

        if ( ( winCtl != tempCtl ) ||
            ( addr->lower != longswap( ioread32( &pTempe->lcsr.cbal ) ) ) )
        {
#ifdef USE_RTOS
            rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
            spin_unlock_irqrestore (&tempe_lock, irqflags);
#endif
            return -EBUSY;
        }
    }

    // Disable while we are modifying
    iowrite32(0,&pTempe->lcsr.csrat);
    vmeSyncData();

    iowrite32(longswap(addr->lower),&pTempe->lcsr.cbal);
    iowrite32(longswap(addr->upper),&pTempe->lcsr.cbau);

    // Write ctl reg and enable
    iowrite32(longswap(tempCtl),&pTempe->lcsr.csrat);
    vmeSyncData();

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Configuring VRAI window: ctl = %08x\n", longswap(ioread32(&pTempe->lcsr.csrat)));
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "lmbal %08x\n", longswap(ioread32(&pTempe->lcsr.cbal)));
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "lmbau %08x\n", longswap(ioread32(&pTempe->lcsr.cbau)));

#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
    spin_unlock_irqrestore (&tempe_lock, irqflags);
#endif

    return(0);
}

//-----------------------------------------------------------------------------
// Function   : tempeReleaseVraiWindow
// Description: Disable and release the VRAI window (CRGAT)
//-----------------------------------------------------------------------------
int tempeReleaseVraiWindow(void)
{
    tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
    GEF_UINT32 irqflags = 0;

    if (NULL == vmechip_baseaddr) {
        return(-EINVAL);
    }

#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
    spin_lock_irqsave(&tempe_lock, irqflags);
#endif

    // Disable window
    iowrite32(0x00000000, &pTempe->lcsr.csrat);
    iowrite32(0x00000000, &pTempe->lcsr.cbal);
    iowrite32(0x00000000, &pTempe->lcsr.cbau);

    vmeSyncData();

#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
    spin_unlock_irqrestore (&tempe_lock, irqflags);
#endif

    return 0;
}

//-----------------------------------------------------------------------------
// Function   : tempeQueryVraiWindow
// Description: Return the values used to map the VRAI window (CRGAT)
//-----------------------------------------------------------------------------
int
tempeQueryVraiWindow(GEF_VME_DRV_VRAI_INFO *vraiInfo)
{
    tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
    GEF_VME_ADDR *addr = &(vraiInfo->addr);
    GEF_UINT32 tempCtl = 0;
    GEF_UINT32 irqflags = 0;

    if (NULL == vmechip_baseaddr) {
        return(-EINVAL);
    }

#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
    spin_lock_irqsave(&tempe_lock, irqflags);
#endif

    tempCtl = longswap(ioread32(&pTempe->lcsr.csrat));
    vraiInfo->addr.lower = longswap(ioread32(&pTempe->lcsr.cbal));
    vraiInfo->addr.upper = longswap(ioread32(&pTempe->lcsr.cbau));

#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
    spin_unlock_irqrestore (&tempe_lock, irqflags);
#endif

    switch(tempCtl & 0x70)
    {
    case TSI148_LCSR_CRGAT_AS_16: addr->addr_space = GEF_VME_ADDR_SPACE_A16; break;
    case TSI148_LCSR_CRGAT_AS_24: addr->addr_space = GEF_VME_ADDR_SPACE_A24; break;
    case TSI148_LCSR_CRGAT_AS_32: addr->addr_space = GEF_VME_ADDR_SPACE_A32; break;
    case TSI148_LCSR_CRGAT_AS_64: addr->addr_space = GEF_VME_ADDR_SPACE_A64; break;
    default: return (GEF_STATUS_VME_INVALID_ADDRESS_SPACE);
    }

    if( (tempCtl & TSI148_LCSR_CRGAT_NPRIV) == TSI148_LCSR_CRGAT_NPRIV)
    {
        vraiInfo->addr.addr_mode |= GEF_VME_ADDR_MODE_USER;
    }

    if( (tempCtl & TSI148_LCSR_CRGAT_SUPR) == TSI148_LCSR_CRGAT_SUPR)
    {
        vraiInfo->addr.addr_mode |= GEF_VME_ADDR_MODE_SUPER;
    }

    if( (tempCtl & TSI148_LCSR_CRGAT_PGM) == TSI148_LCSR_CRGAT_PGM)
    {
        vraiInfo->addr.addr_mode |= GEF_VME_ADDR_MODE_PROGRAM;
    }

    if( (tempCtl & TSI148_LCSR_CRGAT_DATA) == TSI148_LCSR_CRGAT_DATA)
    {
        vraiInfo->addr.addr_mode |= GEF_VME_ADDR_MODE_DATA;
    }

    return 0;
}

//-----------------------------------------------------------------------------
// Function   : tempeDoRmw
// Description: Perform an RMW cycle on the VME bus.
//    A VME outbound window must already be setup which maps to the desired 
//    RMW address.
//-----------------------------------------------------------------------------
int
tempeDoRmw( vmeRmwCfg_t *vmeRmw)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;

  unsigned int vmeEndU, vmeEndL;
  void *rmwPciDataPtr = NULL;
  void *vaDataPtr = NULL;
  unsigned long irqflags;
  GEF_UINT8 rmw8;
  GEF_UINT16 rmw16;
  GEF_UINT32 rmw32;

  vmeEndL = vmeRmw->mastWindVmeAddrL + vmeRmw->mastWindSizeL;
    
  vmeEndU = add64hi(vmeRmw->mastWindVmeAddrL, vmeRmw->mastWindVmeAddrU,
      vmeRmw->mastWindSizeL, vmeRmw->mastWindSizeU);
    
  if(sub64hi(vmeEndL, vmeEndU, 
      vmeRmw->targetAddr, vmeRmw->targetAddrU) >= 0){
      
      rmwPciDataPtr = (void *)((unsigned int)vmeRmw->mastWindBasePciAddr + 
          (vmeRmw->targetAddr - vmeRmw->mastWindVmeAddrL));
      vaDataPtr = (void *)((unsigned int)vmeRmw->mastWindVirtAddr + 
          (vmeRmw->targetAddr - vmeRmw->mastWindVmeAddrL));        
  }
  
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Device Read Modify Write rmwPciDataPtr %p vaDataPtr %p\n",
      rmwPciDataPtr,vaDataPtr); 
  
  // If no window - fail.
  if(rmwPciDataPtr == NULL){
      return(-EINVAL);
  }
    
#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
  spin_lock_irqsave (&tempe_lock, irqflags);
#endif

  // Setup the RMW registers.

  iowrite32((ioread32(&pTempe->lcsr.vmctrl) & longswap(~0x100000)),&pTempe->lcsr.vmctrl); 

  vmeSyncData();
  iowrite32(longswap(vmeRmw->enableMask),&pTempe->lcsr.rmwen);
  iowrite32(longswap(vmeRmw->compareData),&pTempe->lcsr.rmwc);
  iowrite32(longswap(vmeRmw->swapData),&pTempe->lcsr.rmws);
  iowrite32(0,&pTempe->lcsr.rmwau);
  iowrite32(longswap((int)rmwPciDataPtr),&pTempe->lcsr.rmwal);

  iowrite32((ioread32(&pTempe->lcsr.vmctrl) | longswap(0x100000)),&pTempe->lcsr.vmctrl); 
  vmeSyncData();


  // Perform RMW cycle
  switch(vmeRmw->dataWidth)
  {
  case GEF_VME_DWIDTH_D8:
      rmw8 = ioread8(vaDataPtr);
      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Performed Byte RMW cycle 0x%x\n",rmw8);
      break;
      
  case GEF_VME_DWIDTH_D16:
      rmw16 = ioread16(vaDataPtr);
      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Performed 16-bit RMW cycle 0x%x\n",rmw16);
      break;
      
  case GEF_VME_DWIDTH_D32:

      rmw32 = ioread32(vaDataPtr);
      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Performed 32-bit RMW cycle 0x%x\n",rmw32);
      break;
      
  default:
      return(GEF_STATUS_INVAL_ADDR); 
      break;
  }

  vmeSyncData();

  iowrite32((ioread32(&pTempe->lcsr.vmctrl) & longswap(~0x100000)),&pTempe->lcsr.vmctrl); 
  vmeSyncData();

#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
  spin_unlock_irqrestore (&tempe_lock, irqflags);
#endif
  return(0);
}


//-----------------------------------------------------------------------------
// Function   : dmaSrcAttr, dmaDstAttr
// Description: Helper functions which setup common portions of the 
//      DMA source and destination attribute registers.
//-----------------------------------------------------------------------------
int
dmaSrcAttr( vmeDmaPacket_t *vmeCur)
{
  
  int dsatreg = 0;
  // calculate source attribute register
  switch(vmeCur->srcBus){
    case VME_DMA_PATTERN_BYTE:
        dsatreg = 0x23000000;
        break;
    case VME_DMA_PATTERN_BYTE_INCREMENT:
        dsatreg = 0x22000000;
        break;
    case VME_DMA_PATTERN_WORD:
        dsatreg = 0x21000000;
        break;
    case VME_DMA_PATTERN_WORD_INCREMENT:
        dsatreg = 0x20000000;
        break;
    case VME_DMA_PCI:
        dsatreg = 0x00000000;
        break;
    case VME_DMA_VME:
        dsatreg = 0x10000000;
        dsatreg |= tempeSetupMasterAttribute(
             vmeCur->srcVmeAttr.addrSpace,
             vmeCur->srcVmeAttr.userAccessType,
             vmeCur->srcVmeAttr.dataAccessType,
             vmeCur->srcVmeAttr.maxDataWidth,
             vmeCur->srcVmeAttr.xferProtocol,
             vmeCur->srcVmeAttr.xferRate2esst);
        break;
    default:
        dsatreg = -EINVAL;
        break;
  }
  return(dsatreg); 
}

int
dmaDstAttr( vmeDmaPacket_t *vmeCur)
{
  int ddatreg = 0;
  // calculate destination attribute register
  switch(vmeCur->dstBus){
    case VME_DMA_PCI:
        ddatreg = 0x00000000;
        break;
    case VME_DMA_VME:
        ddatreg = 0x10000000;
        ddatreg |= tempeSetupMasterAttribute(
             vmeCur->dstVmeAttr.addrSpace,
             vmeCur->dstVmeAttr.userAccessType,
             vmeCur->dstVmeAttr.dataAccessType,
             vmeCur->dstVmeAttr.maxDataWidth,
             vmeCur->dstVmeAttr.xferProtocol,
             vmeCur->dstVmeAttr.xferRate2esst);
        break;
    default:
        ddatreg = -EINVAL;
        break;
  }
  return(ddatreg);
}

//-----------------------------------------------------------------------------
// Function   : tempeStartDma
// Description: Write the DMA controller registers with the contents
//    needed to actually start the DMA operation.
//    returns starting time stamp.
//
//    Starts either direct or chained mode as appropriate (based on dctlreg)
//-----------------------------------------------------------------------------
unsigned int
tempeStartDma(
int channel,
unsigned int dctlreg,
tsi148DmaDescriptor_t *vmeLL
)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  unsigned int val;
  unsigned long irqflags;
    
#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
  spin_lock_irqsave (&tempe_lock, irqflags);
#endif

  // Setup registers as needed for direct or chained.
  if(dctlreg & 0x800000){

     // Write registers 

     iowrite32(vmeLL->dsal,&pTempe->lcsr.dma[channel].dsal);
     iowrite32(vmeLL->dsau,&pTempe->lcsr.dma[channel].dsau);
     iowrite32(vmeLL->ddal,&pTempe->lcsr.dma[channel].ddal);
     iowrite32(vmeLL->ddau,&pTempe->lcsr.dma[channel].ddau);
     iowrite32(vmeLL->dsat,&pTempe->lcsr.dma[channel].dsat);
     iowrite32(vmeLL->ddat,&pTempe->lcsr.dma[channel].ddat);
     iowrite32(vmeLL->dcnt,&pTempe->lcsr.dma[channel].dcnt);
     iowrite32(vmeLL->ddbs,&pTempe->lcsr.dma[channel].ddbs);

     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "REG VALS:\n");
     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "dsau = %08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].dsau)));
     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "dsal = %08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].dsal)));
     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "ddau = %08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].ddau)));
     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "ddal = %08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].ddal)));
     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "dsat = %08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].dsat)));
     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "ddat = %08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].ddat)));
     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "dcnt = %08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].dcnt)));
     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "ddbs = %08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].ddbs)));

  } else {
     iowrite32(0,&pTempe->lcsr.dma[channel].dnlau);
     iowrite32(longswap((unsigned int)vmeLL),&pTempe->lcsr.dma[channel].dnlal);

     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "REG VALS:\n");
     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "dnlau = %08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].dnlau)));
     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "dnlal = %08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].dnlal)));

  }
  vmeSyncData();

  // Start the operation
  dctlreg |=0x2000000;

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "DMA START: dctl val 0x%x\n", dctlreg);

  iowrite32(longswap(dctlreg),&pTempe->lcsr.dma[channel].dctl);

  vmeSyncData();

#ifdef PPC
  val = vmeGetTime();
#else
  rdtscl(val);
#endif
  vmeSyncData();

#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
  spin_unlock_irqrestore (&tempe_lock, irqflags);
#endif

  return(val);
}

//-----------------------------------------------------------------------------
// Function   : tempeSetupDma
// Description: Create a linked list (possibly only 1 element long) of 
//   Tempe DMA descriptors which will perform the DMA operation described
//   by the DMA packet.  Flush descriptors from cache.
//   Return pointer to beginning of list (or 0 on failure.).
//-----------------------------------------------------------------------------
tsi148DmaDescriptor_t *
tempeSetupDma(vmeDmaPacket_t *vmeDma)
{
  vmeDmaPacket_t *vmeCur;
  int	maxPerPage;
  int	currentLLcount;
  tsi148DmaDescriptor_t *startLL;
  tsi148DmaDescriptor_t *currentLL;
  tsi148DmaDescriptor_t *nextLL;
  

  maxPerPage = PAGESIZE/sizeof(tsi148DmaDescriptor_t)-1;
  startLL = (tsi148DmaDescriptor_t *)__get_free_pages(GFP_KERNEL, 0);
  if(startLL == 0){ return(startLL); }

  // First allocate pages for descriptors and create linked list
  vmeCur = vmeDma;
  currentLL = startLL;
  currentLLcount = 0;
  while(vmeCur != 0){
     if(vmeCur->pNextPacket != 0){
        currentLL->dnlau = (unsigned int)0;
        currentLL->dnlal = longswap((unsigned int)(currentLL + 1));

        currentLLcount++;
        if(currentLLcount >= maxPerPage){
              currentLL->dnlal = __get_free_pages(GFP_KERNEL,0);
              currentLL->dnlal = longswap(__get_free_pages(GFP_KERNEL,0));
              currentLL->dnlau = (unsigned int)0;
              currentLLcount = 0;
        } 
        currentLL = (tsi148DmaDescriptor_t *)longswap(currentLL->dnlal);
     } else {
        currentLL->dnlau = (unsigned int)0;
        currentLL->dnlal = (unsigned int)0;
     }
     vmeCur = vmeCur->pNextPacket;
  }
  // Next fill in information for each descriptor
  vmeCur = vmeDma;
  currentLL = startLL;
  while(vmeCur != 0){
     currentLL->dsau = longswap(vmeCur->srcAddrU);
     currentLL->dsal = longswap(vmeCur->srcAddr);
     currentLL->ddau = longswap(vmeCur->dstAddrU);
     currentLL->ddal = longswap(vmeCur->dstAddr);
     currentLL->dsat = longswap(dmaSrcAttr(vmeCur));
     currentLL->ddat = longswap(dmaDstAttr(vmeCur));
     currentLL->dcnt = longswap(vmeCur->byteCount);
     currentLL->ddbs = longswap(vmeCur->bcastSelect2esst);

     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Linked List Entry Fields:\n");
     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "dsal %x\n",longswap(currentLL->dsal));
     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "dsau %x\n",longswap(currentLL->dsau));
     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "ddal %x\n",longswap(currentLL->ddal));
     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "ddau %x\n",longswap(currentLL->ddau));
     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "dsat %x\n",longswap(currentLL->dsat));
     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "ddat %x\n",longswap(currentLL->ddat));
     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "dnlau %x\n",longswap(currentLL->dnlau));
     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "dnlal %x\n",longswap(currentLL->dnlal));
     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "dcnt  %x\n",longswap(currentLL->dcnt));
     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "ddbs  %x\n",longswap(currentLL->ddbs));

     currentLL = (tsi148DmaDescriptor_t *)longswap(currentLL->dnlal);
     vmeCur = vmeCur->pNextPacket;
  }

  
  // Convert Links to PCI addresses.
  currentLL = startLL;
  while(currentLL != 0){
     nextLL = (tsi148DmaDescriptor_t *)longswap(currentLL->dnlal);
     currentLL->dnlal = longswap((unsigned int)virt_to_bus(nextLL));
     if(nextLL == 0) currentLL->dnlal |= longswap(1); 
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Bus address link: currrentLL %x current->dnlal %x\n",
             (unsigned int)currentLL,longswap(currentLL->dnlal));

#ifdef PPC 
     vmeFlushLine((char *)currentLL);
     vmeFlushLine(((char *)currentLL)+LINESIZE);
#else
     vmeSyncData();
#endif
     currentLL = nextLL;
  }
  

  // Return pointer to descriptors list 
  return(startLL);
}

//-----------------------------------------------------------------------------
// Function   : tempeFreeDma
// Description: Free all memory that is used to hold the DMA 
//    descriptor linked list.  
//
//-----------------------------------------------------------------------------
int
tempeFreeDma(tsi148DmaDescriptor_t *startLL)
{
  tsi148DmaDescriptor_t *currentLL;
  tsi148DmaDescriptor_t *prevLL;
  tsi148DmaDescriptor_t *nextLL;
  
  // Convert Links to virtual addresses.
  currentLL = startLL;
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeFreeDma: \n");

  while(currentLL != 0){
     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Freeing 0x%x \n",longswap(currentLL->dnlal));
     if(longswap(currentLL->dnlal) & 1) {
        currentLL->dnlal = 0;
     } else {
        currentLL->dnlal = longswap((unsigned int)bus_to_virt(longswap(currentLL->dnlal)));
     }
     currentLL = (tsi148DmaDescriptor_t *)longswap(currentLL->dnlal);
  }

  // Free all pages associated with the descriptors.
  currentLL = startLL;
  prevLL = currentLL;
  while(currentLL != 0){
     nextLL = (tsi148DmaDescriptor_t *)longswap(currentLL->dnlal);
     if(currentLL+1 != nextLL) {
         free_pages((int)prevLL, 0);
         prevLL = nextLL;
     }
     currentLL = nextLL;
  }

  // Return pointer to descriptors list 
  return(0);
}

//-----------------------------------------------------------------------------
// Function   : tempeDoDma
// Description:  
//   Sanity check the DMA request. 
//   Setup DMA attribute register. 
//   Create linked list of DMA descriptors.
//   Invoke actual DMA operation.
//   Wait for completion.  Record ending time.
//   Free the linked list of DMA descriptor.
//-----------------------------------------------------------------------------
int
tempeDoDma(vmeDmaPacket_t *vmeDma)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  unsigned int dctlreg=0;
  int val;
  char buf[100];
  int channel;
  vmeDmaPacket_t *curDma;
  tsi148DmaDescriptor_t *dmaLL;
    
  // Sanity check the VME chain.
  channel = vmeDma->channel_number;
  if(channel > 1) { return(-EINVAL); }
  curDma = vmeDma;
  while(curDma != 0){
    if(curDma->byteCount == 0){ return(-EINVAL); }
    if(dmaSrcAttr(curDma) < 0) { return(-EINVAL); }
    if(dmaDstAttr(curDma) < 0) { return(-EINVAL); }

    curDma = curDma->pNextPacket;
    if(curDma == vmeDma) {          // Endless Loop!
       return(-EINVAL);
    }
  }

  // calculate control register
  if(vmeDma->pNextPacket != 0){
    dctlreg = 0;
  } else {
    dctlreg = 0x800000;
  }

  /* Validate/Set up VON value. This sets both VBKS and PBKS equal */    
  switch(vmeDma->maxVmeBlockSize)
  {
  case GEF_VME_DMA_VON_0:     /* let default to 32 if not specified */
  case GEF_VME_DMA_VON_32:                       break;
  case GEF_VME_DMA_VON_64:    dctlreg |= 0x1010; break;
  case GEF_VME_DMA_VON_128:   dctlreg |= 0x2020; break;
  case GEF_VME_DMA_VON_256:   dctlreg |= 0x3030; break;
  case GEF_VME_DMA_VON_512:   dctlreg |= 0x4040; break;
  case GEF_VME_DMA_VON_1024:  dctlreg |= 0x5050; break;
  case GEF_VME_DMA_VON_2048:  dctlreg |= 0x6060; break;
  case GEF_VME_DMA_VON_4096:  dctlreg |= 0x7070; break;
      
  default: return (GEF_STATUS_VME_INVALID_VON_VALUE);
  }
 
  /* Validate/Set up VOFF value. This sets both VBOT and PBOT equal */ 
  switch(vmeDma->vmeBackOffTimer)
  {
    case GEF_VME_DMA_VOFF_0us:    break; /* let default to 0us if not specified */ 
    case GEF_VME_DMA_VOFF_1us:    dctlreg |= 0x101; break;
    case GEF_VME_DMA_VOFF_2us:    dctlreg |= 0x202; break;
    case GEF_VME_DMA_VOFF_4us:    dctlreg |= 0x303; break;
    case GEF_VME_DMA_VOFF_8us:    dctlreg |= 0x404; break;
    case GEF_VME_DMA_VOFF_16us:   dctlreg |= 0x505; break;
    case GEF_VME_DMA_VOFF_32us:   dctlreg |= 0x606; break;
    case GEF_VME_DMA_VOFF_64us:   dctlreg |= 0x707; break;
  default: return (GEF_STATUS_VME_INVALID_VOFF_VALUE);
  }

  if(vmeDma->vmeFlushOnAbortRead)
  {
        dctlreg |= 0x10000; // set VFAR
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeDoDma: VFAR is set\n");
  }

  if(vmeDma->pciFlushOnAbortRead)
  {
        dctlreg |= 0x20000; // set PFAR
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeDoDma: PFAR is set\n");
  }

  // Setup the dma chain
  dmaLL = tempeSetupDma(vmeDma);

  // Start the DMA
  if(dctlreg & 0x800000){
      vmeDma->vmeDmaStartTick = 
                      tempeStartDma( channel, dctlreg, dmaLL);
  } else {
      vmeDma->vmeDmaStartTick = 
                      tempeStartDma( channel, dctlreg,
                                 (tsi148DmaDescriptor_t *)virt_to_phys(dmaLL));
  }

  if (GEF_FALSE == dma_berr_failure) {
      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "DMA wait_event_interruptible\n");
#ifdef USE_RTOS
      while(((longswap(ioread32(&pTempe->lcsr.dma[channel].dsta)) & 0x1000000) != 0)
      && (GEF_TRUE != dma_berr_failure))
      {
          RT_TASK* task = rt_whoami();
          if(task != 0)
          {
              rt_task_suspend(rt_whoami());
          }
      }
#else
      wait_event_interruptible(dma_queue[channel], 
          (( longswap(ioread32(&pTempe->lcsr.dma[channel].dsta)) & 0x1000000) == 0) || 
            (GEF_TRUE == dma_berr_failure));
#endif
  } else {
      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "DMA Bypassing wait_event_interruptible due to BERR\n");
  }

      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "DMA Thread Resumed\n");
  val = longswap(ioread32(&pTempe->lcsr.dma[channel].dsta));

  vmeDma->vmeDmaStopTick = tempeDmaIrqTime[channel];
  vmeDma->vmeDmaStatus = val;
  if(vmeDma->vmeDmaStopTick < vmeDma->vmeDmaStartTick){
     vmeDma->vmeDmaElapsedTime = 
        (0xFFFFFFFF - vmeDma->vmeDmaStartTick) + vmeDma->vmeDmaStopTick;
  } else {
     vmeDma->vmeDmaElapsedTime = 
        vmeDma->vmeDmaStopTick - vmeDma->vmeDmaStartTick;
  }
  vmeDma->vmeDmaElapsedTime -= vmechipIrqOverHeadTicks;
#ifdef PPC
  vmeDma->vmeDmaElapsedTime /= (tb_speed/1000000);
#else
  vmeDma->vmeDmaElapsedTime /= (tb_speed/1000);
#endif

  vmeDma->vmeDmaStatus = 0;
  if (val & 0x10000000) {
      sprintf(buf,"<<tsi148>> DMA Error in DMA_tempe_irqhandler DSTA=%08X\n",val);  
#ifdef USE_RTOS
             rtlogRecord_t logRecord;
             RTLOG_ERROR("gefvme_rtos", "%s", buf);
#else
      printk(buf);
#endif
      vmeDma->vmeDmaStatus = val;
  
  }

  // Free the dma chain
  tempeFreeDma(dmaLL);
  
  return(0);
}

//-----------------------------------------------------------------------------
// Function   : tempe_shutdown
// Description: Put VME bridge in quiescent state.  Disable all decoders,
// clear all interrupts.
//-----------------------------------------------------------------------------
void
tempe_shutdown( void )
{
    tsi148_t *pTempe = NULL;
    int i;
    unsigned long irqflags;

    if (!vmechip_baseaddr) {
         GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempe_shutdown: vmechip_baseaddr is NULL.\n");
         return;
    }

   pTempe = (tsi148_t *)vmechip_baseaddr;

#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
   spin_lock_irqsave (&tempe_lock, irqflags);
#endif

   /*
    *  Shutdown all inbound and outbound windows.
    */
    for(i =0; i < 8; i++){
        iowrite32(0,&pTempe->lcsr.inboundTranslation[i].itat);
        iowrite32(0,&pTempe->lcsr.outboundTranslation[i].otat);
    }

   /*
    *  Shutdown Location monitor.
    */
   iowrite32(0,&pTempe->lcsr.lmat);
   
   /*
    *  Shutdown CRG map.
    */
   iowrite32(0,&pTempe->lcsr.csrat);

   /*
    *  Clear error status.
    */
   iowrite32(0xFFFFFFFF,&pTempe->lcsr.edpat);
   iowrite32(0xFFFFFFFF,&pTempe->lcsr.veat);
   iowrite32(longswap(0x07000700),&pTempe->lcsr.pstat);

   /*
    *  Remove VIRQ interrupt (if any)
    */
   if(longswap(ioread32(&pTempe->lcsr.vicr)) & 0x800){
      iowrite32(longswap(0x8000),&pTempe->lcsr.vicr);
   }

   /*
    *  Disable and clear all interrupts.
    */
   iowrite32(0,&pTempe->lcsr.inteo);
   vmeSyncData();
   iowrite32(0xFFFFFFFF,&pTempe->lcsr.intc); 
   vmeSyncData();
   iowrite32(0xFFFFFFFF,&pTempe->lcsr.inten); 
   
   /*
    *  Map all Interrupts to PCI INTA
    */
   iowrite32(0,&pTempe->lcsr.intm1);
   iowrite32(0,&pTempe->lcsr.intm2);

   vmeSyncData();

#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
   spin_unlock_irqrestore (&tempe_lock, irqflags);
#endif

}

extern int tempeSetBusTimeout(GEF_VME_DRV_BUS_TIMEOUT_INFO *bus_timeout_info);

//-----------------------------------------------------------------------------
// Function   : tempe_init()
// Description: Initialize the VME chip as needed by the driver.
//    Quiesce VME bridge.
//    Setup default decoders.
//    Connect IRQ handler and enable interrupts.
//    Conduct quick sanity check of bridge.
//-----------------------------------------------------------------------------
int
tempe_init(void)
{
    tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
    int result;
    unsigned int tmp;
    unsigned int irqOverHeadStart;
    int overHeadTicks;
    unsigned long irqflags;
    GEF_VME_INT_INFO int_info;
    int i = 0;
    GEF_VME_DRV_BUS_TIMEOUT_INFO bus_to_info;
    GEF_STATUS status;

    vown_count = 0;

    spin_lock_init(&tempe_lock);

    tempe_shutdown();
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, " Called tempe_shutdown() in tempe_init()\n");

    // Determine syscon and slot number.
    tmp = longswap(ioread32(&pTempe-> lcsr.vstat));
    if(tmp & 0x100){
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, " vme_syscon=1  in tempe_init() pTempe->lcsr.vstat (0x23C) = 0x%x\n",tmp);
        vme_syscon = 1;
    } else {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, " vme_syscon=0  in tempe_init() pTempe->lcsr.vstat (0x23C) = 0x%x\n",tmp);
        vme_syscon = 0;
    }

    /* clear board fail status */
    iowrite32(longswap(0x8000),&pTempe->lcsr.vstat);
    vmeSyncData();

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, " tempe_init() pTempe->lcsr.vstat after reset (0x23C) = 0x%x\n",
                      longswap(ioread32(&pTempe->lcsr.vstat)));

    // increase VMEbus controller time on
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, " tempe_init() pTempe->lcsr.vmctrl = 0x%x before increase vme time on\n",
                      longswap(ioread32(&pTempe->lcsr.vmctrl)));
    tmp = longswap(ioread32(&pTempe->lcsr.vmctrl));
    iowrite32(longswap(tmp|0x700),&pTempe->lcsr.vmctrl);
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, " tempe_init() pTempe->lcsr.vmctrl = 0x%x after increase vme time on\n",
                      longswap(ioread32(&pTempe->lcsr.vmctrl)));


    // initialize master windows
    for(i=0;i<VME_MAX_WINDOWS;i++)
    {
        iowrite32(0,&pTempe->lcsr.inboundTranslation[i].itat);
    }

    // Connect the interrupt handler.
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    if((result = rt_request_global_irq(
        vmechip_irq, tempe_irqhandler)) != 0)
    {
        RTLOG_ERROR("gefvme_rtos", "RTOS-VME: Failure requesting irq %d",
            vmechip_irq);
    }
    else
    {
        if((result = rt_request_linux_irq(vmechip_irq,
            linuxPostIRQHandler,
            "VMEBus (Tsi148)",
            vme_pci_dev)) != 0)
        {
            rt_free_global_irq(vmechip_irq);
              RTLOG_ERROR("gefvme_rtos", "RTOS-VME: Failure requesting irq %d",
                vmechip_irq);
        }
        else
        {
            rt_startup_irq(vmechip_irq);
        }
    }
#else
#ifdef PPC
    result = request_irq(vmechip_irq, 
                         tempe_irqhandler, 
                         IRQF_DISABLED, 
                         "VMEBus (Tsi148)", NULL);
#else
    result = request_irq(vmechip_irq, 
                         tempe_irqhandler, 
                         IRQF_SHARED, 
                         "VMEBus (Tsi148)", vme_pci_dev);
#endif
#endif

    if (result) {
#ifdef USE_RTOS
        RTLOG_ERROR("gefvme_rtos", "  tsi148: can't get assigned pci irq "
            "vector %02X", vmechip_irq);
#else
      printk("  tsi148: can't get assigned pci irq vector %02X\n", vmechip_irq);
#endif
      return(0);
    } 

    ADD_RESOURCE(vme_init_flags, VME_RSRC_INTR);

    // Enable LERR and DMA Interrupts

    tmp = 0x03002000;

    iowrite32(longswap(tmp),&pTempe->lcsr.inteo);
    iowrite32(longswap(tmp),&pTempe->lcsr.inten);

    // Enable the VME BERR (VERR) interrupt
    int_info.int_level = GEF_VME_INT_BERR;
    int_info.int_vector = 0;
    if (tempeEnableIrq(&int_info)) {
#ifdef USE_RTOS
        RTLOG_ERROR("gefvme_rtos", "  VME tempe: can't enable BERR interrupt");
#else
        printk("  VME tempe: can't enable BERR interrupt\n");
#endif
        return(0);
    }

    // Sanity check register access.
    for(tmp = 1; tmp < 0x80000000; tmp = tmp << 1){
      iowrite32(longswap(tmp),&pTempe->lcsr.rmwen);
      iowrite32(longswap(~tmp),&pTempe->lcsr.rmwc);
      vmeSyncData();
      if(longswap(ioread32(&pTempe->lcsr.rmwen)) != tmp) {
        return(0);
      }
      if(longswap(ioread32(&pTempe->lcsr.rmwc)) != ~tmp) {
        return(0);
      }
    }

    // do a mail box interrupt to calibrate the interrupt overhead.

    // Enable the VME MBOX0 interrupt
    int_info.int_level = GEF_VME_INT_MBOX0;
    int_info.int_vector = 0;
    if (tempeEnableIrq(&int_info)) {
#ifdef USE_RTOS
        RTLOG_ERROR("gefvme_rtos", " VME tempe: can't enable MBOX0 interrupt");
#else
        printk(" VME tempe: can't enable MBOX0 interrupt\n");
#endif
        return(0);
    }

#ifdef PPC
    irqOverHeadStart = vmeGetTime();
#else
    rdtscl(irqOverHeadStart);
#endif

    iowrite32(0,&pTempe->gcsr.mbox[0]);
    vmeSyncData();
#ifdef PPC
    for(tmp = 0; tmp < 10; tmp++) { vmeSyncData(); }
    irqOverHeadStart = vmeGetTime();
#else
    for(tmp = 0; tmp < 10; tmp++) {
      udelay(10);
      vmeSyncData();
    }
    rdtscl(irqOverHeadStart);
#endif


    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "time start val 0x%x\n",irqOverHeadStart);

    // Guarantee first interrupt processing complete
#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&tempe_lock);
#else
    spin_lock_irqsave(&tempe_lock, irqflags);
#endif
    iowrite32(0,&pTempe->gcsr.mbox[0]);
#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &tempe_lock);
#else
    spin_unlock_irqrestore(&tempe_lock, irqflags);
#endif
    vmeSyncData();

#ifdef PPC
    for(tmp = 0; tmp < 10; tmp++) { vmeSyncData(); }
#else
    for(tmp = 0; tmp < 10; tmp++) {
      udelay(10);
      vmeSyncData();
    }
#endif

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calculating overhead\n");
    overHeadTicks = tempeIrqTime - irqOverHeadStart;
    if(overHeadTicks > 0){
       vmechipIrqOverHeadTicks = overHeadTicks;
    } else {
       vmechipIrqOverHeadTicks = 1;
    }

    // Disable the VME MBOX0 interrupt
    int_info.int_level = GEF_VME_INT_MBOX0;
    int_info.int_vector = 0;
    if (tempeDisableIrq(&int_info)) {
#ifdef USE_RTOS
        RTLOG_ERROR("gefvme_rtos", "  VME tempe: can't disable MBOX0 "
            "interrupt");
#else
        printk("  VME tempe: can't disable MBOX0 interrupt\n");
#endif
        return(0);
    }

    /* set VMEbus timeout to 8us to prevent master write PCIbus deadlock */
    bus_to_info.timeout = GEF_VME_BUS_TIMEOUT_8us;
    status = tempeSetBusTimeout(&bus_to_info);
    if(status != GEF_SUCCESS)
    {
#ifdef USE_RTOS
        RTLOG_ERROR("gefvme_rtos", "  VME tempe: Failed to set VMEbus timeout "
            "to 8us with error code 0x%x", status);
#else
         printk("  VME tempe: Failed to set VMEbus timeout to 8us with error code 0x%x\n", status);
#endif
         return(0);
    }
#ifdef USE_RTOS
    RTLOG_INFO("gefvme_rtos", "  VME tempe: Set VMEbus timeout to 8us");
#else
    printk("  VME tempe: Set VMEbus timeout to 8us\n");
#endif

    return(1);
}
