/*
 * File:
 *    vmelinux.c
 *
 * Description:
 *    Source code to the kernel VME driver interface.
 *
 * Copyright:
 *    Copyright 2007 GE Fanuc Embedded Systems, Inc.
 *
 * License:
 *    This file is proprietary to GE Fanuc Embedded Systems, Inc.
 *
 *
 * $Date$
 * $Rev$
 */


/*
Modified to support Linux 2.6 x86 platforms
===============================================================================
*/

//------------------------------------------------------------------------------  
//title: VME PCI-VME Kernel Driver
//version: Linux 3.1
//date: February 2004
//original designer: Michael Wyrick
//original programmer: Michael Wyrick 
//  Signficant restructuring by Tom Armistead to add support of 
//  Tundra TSI148 and additional functionality.
//platform: Linux 2.4.x
//language: GCC 2.95 and 3.0
//module: vmemod.o
//------------------------------------------------------------------------------  
//  Purpose: Provides the device node interface from the user level program to 
//           the VME bridge.  Primary purpose is to convert user space
//           data to kernel and back.
//  Docs:                                  
//    This driver supports both the Tempe, Universe and Universe II chips                                     
//------------------------------------------------------------------------------  
// RCS:
// $Id$
// Log: ca91c042.c,v
// Revision 1.5  2001/10/27 03:50:07  jhuggins
// These changes add support for the extra images offered by the Universe II.
// CVS : ----------------------------------------------------------------------
//
// Revision 1.6  2001/10/16 15:16:53  wyrick
// Minor Cleanup of Comments
//
//
//-----------------------------------------------------------------------------

#include <linux/version.h>
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,5,0)
#define MODULE
#endif

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,18)
#include <linux/config.h>
#endif

#ifdef CONFIG_MODVERSIONS
  #define MODVERSIONS

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,5,0)
#include <linux/modversions.h>
#endif

#endif

#include <linux/module.h>
#include <linux/mm.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/proc_fs.h>
#include <linux/pci.h>
#include <linux/pci_ids.h>
#include <linux/poll.h>

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,5,43)
#include <linux/iobuf.h> // Doesn't exist in 2.6 kernels
#else
#include <linux/mm.h> 
#include <linux/pagemap.h>
#include <linux/page-flags.h>
#endif

#include <linux/highmem.h>
#include <linux/delay.h> 
#include <asm/io.h>
#include <asm/uaccess.h>
#ifdef USE_RTOS
#include <rtai_types.h>
#include <rtai_sem.h>
#else
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2, 6, 26)
#include <linux/semaphore.h>
#else
#include <asm/semaphore.h>
#endif
#endif

#include <asm/ioctl.h>
#include "gef/gefcmn_vme_framework.h"
#include "gef/gefcmn_osa_linux.h"
#include "gef/gefcmn_vme_errno.h"
#include "gef/gefcmn_vme_private.h"

#include "vmedrv.h"
#include "vmivme.h"
#include "ca91c042.h"

#ifndef PCI_VENDOR_ID_VMIC
#define PCI_VENDOR_ID_VMIC 0x114a
#endif

#ifdef USE_RTOS
static unsigned int semaphoreTimeout = 100;    /* usec */
module_param(semaphoreTimeout, uint, S_IRUGO);

extern SEM vown_lock;
extern RTIME SEMAPHORE_TIMEOUT;
#endif

//void calibrate_delay(void);
//----------------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------------
/* No user space interface for the RT version of this kernel module. */
#ifndef USE_RTOS
static int vme_open(struct inode *, struct file *);
static int vme_release(struct inode *, struct file *);
static int vme_ioctl(struct inode *, struct file *, unsigned int, unsigned long);
#endif
static int vme_procinfo(char *, char **, off_t, int, int *,void *);
/* No user space interface for the RT version of this kernel module. */
#ifndef USE_RTOS
static int vme_mmap(struct file *file,struct vm_area_struct *vma);
#endif
extern	int tb_ticks_per_jiffy;

//----------------------------------------------------------------------------
// Types
//----------------------------------------------------------------------------
static struct proc_dir_entry *vme_procdir;
/* No user space interface for the RT version of this kernel module. */
#ifndef USE_RTOS
static struct file_operations vme_fops = 
{
  ioctl:    vme_ioctl,
  mmap:     vme_mmap,
  open:     vme_open,
  release:  vme_release 
};
#endif

//----------------------------------------------------------------------------
// Vars and Defines
//----------------------------------------------------------------------------

#define VME_MAJOR      221
#define MAX_MINOR 	    0xFF

#define DMATYPE_SINGLE  1
#define DMATYPE_LLIST   2

void *vmic_base = NULL; // virtual address of FPGA VME Comm register
#define IOUNMAP_FPGA        \
{                           \
     if (vmic_base) {       \
        iounmap(vmic_base); \
     }                      \
}

static int opened[MAX_MINOR + 1];
struct semaphore devinuse[MAX_MINOR+1];

// Global VME controller information
extern int vmechip_irq;	// PCI irq
extern int vmechip_devid;	// PCI devID of VME bridge
extern int vmeparent_devid;	// PCI devID of VME bridge parent
extern int vmechip_revision;	// PCI revision
extern int vmechip_bus;	// PCI bus number.
extern char *vmechip_baseaddr;	// virtual address of Tundra Universe or Tempe chip registers
extern int vme_syscon;	// VME sys controller (-1 = unknown)
unsigned long physical_addr_regs;

unsigned int vme_init_flags;

struct resource out_resource[0x8];
extern unsigned int out_image_va[8];    // Base virtual address
extern unsigned int out_image_valid[8];    // validity
extern vmeDmaHandle_t *dma_handle_list;
/*
 * External functions
 */
extern void uni_shutdown(void);
extern void tempe_shutdown(void);

extern int vmeInit(void);
extern int vmeBusErrorChk(int);
extern int vmeGetSlotNum(void);
extern int vmeGetOutBound(vmeOutWindowCfg_t *);
extern int vmeSetOutBound(vmeOutWindowCfg_t *);
extern int vmeDoDma(vmeDmaPacket_t *);
extern int vmeGetInBound(vmeInWindowCfg_t *);
extern int vmeSetInBound(vmeInWindowCfg_t *);
extern int vmeSetupLm(vmeLmCfg_t *);
extern int vmeWaitLm(vmeLmCfg_t *);

extern void vmeSyncData(void);
extern void vmeFlushLine(void *);
extern void vmeFlushRange(void *, void *);

/* CAPI */
extern int vmeGenerateIrq(GEF_VME_DRV_VIRQ_INFO *);
EXPORT_SYMBOL(vmeGenerateIrq);
extern int vmeWaitForIrq(GEF_VME_DRV_WAIT_INFO *);
EXPORT_SYMBOL(vmeWaitForIrq);
extern int vmeWaitIrqAck(GEF_VME_DRV_WAIT_INFO *);
EXPORT_SYMBOL(vmeWaitIrqAck);
extern int vmeReleaseIrqWait(GEF_VME_DRV_VIRQ_INFO *);
EXPORT_SYMBOL(vmeReleaseIrqWait);
extern int vmeGetArbiter(GEF_VME_DRV_BUS_ARB_INFO *);
EXPORT_SYMBOL(vmeGetArbiter);
extern int vmeSetArbiter(GEF_VME_DRV_BUS_ARB_INFO *);
EXPORT_SYMBOL(vmeSetArbiter);
extern int vmeSetReleaseMode(GEF_VME_DRV_BUS_REL_MODE_INFO *);
EXPORT_SYMBOL(vmeSetReleaseMode);
extern int vmeGetReleaseMode(GEF_VME_DRV_BUS_REL_MODE_INFO *);
EXPORT_SYMBOL(vmeGetReleaseMode);
extern int vmeSetRequestMode(GEF_VME_DRV_BUS_REQ_MODE_INFO *);
EXPORT_SYMBOL(vmeSetRequestMode);
extern int vmeGetRequestMode(GEF_VME_DRV_BUS_REQ_MODE_INFO *);
EXPORT_SYMBOL(vmeGetRequestMode);
extern int vmeSetRequestLevel(GEF_VME_DRV_BUS_REQ_LEVEL_INFO *);
EXPORT_SYMBOL(vmeSetRequestLevel);
extern int vmeGetRequestLevel(GEF_VME_DRV_BUS_REQ_LEVEL_INFO *);
EXPORT_SYMBOL(vmeGetRequestLevel);
extern int vmeSetBusTimeout(GEF_VME_DRV_BUS_TIMEOUT_INFO *);
EXPORT_SYMBOL(vmeSetBusTimeout);
extern int vmeGetBusTimeout(GEF_VME_DRV_BUS_TIMEOUT_INFO *);
EXPORT_SYMBOL(vmeGetBusTimeout);
extern int vmeSetArbTimeout(GEF_VME_DRV_ARB_TIMEOUT_INFO *);
EXPORT_SYMBOL(vmeSetArbTimeout);
extern int vmeGetArbTimeout(GEF_VME_DRV_ARB_TIMEOUT_INFO *);
EXPORT_SYMBOL(vmeGetArbTimeout);
extern int vmeSetBroadcastId(GEF_VME_DRV_BUS_BROADCAST_INFO *);
EXPORT_SYMBOL(vmeSetBroadcastId);
extern int vmeGetBroadcastId(GEF_VME_DRV_BUS_BROADCAST_INFO *);
EXPORT_SYMBOL(vmeGetBroadcastId);
extern int vmeSetSysFail(GEF_VME_DRV_SYSFAIL_INFO *);
EXPORT_SYMBOL(vmeSetSysFail);
extern int vmeGetSysFail(GEF_VME_DRV_SYSFAIL_INFO *);
EXPORT_SYMBOL(vmeGetSysFail);
extern int vmeSetMaxRetry(GEF_VME_MAX_RETRY_INFO *);
EXPORT_SYMBOL(vmeSetMaxRetry);
extern int vmeGetMaxRetry(GEF_VME_MAX_RETRY_INFO *);
EXPORT_SYMBOL(vmeGetMaxRetry);
extern int vmeSetPostedWriteCount(GEF_VME_POST_WRITE_COUNT_INFO *);
EXPORT_SYMBOL(vmeSetPostedWriteCount);
extern int vmeGetPostedWriteCount(GEF_VME_POST_WRITE_COUNT_INFO *);
EXPORT_SYMBOL(vmeGetPostedWriteCount);
extern int vmeSysReset(void);
EXPORT_SYMBOL(vmeSysReset);
extern int vmeAcquireBusOwnership(GEF_VME_DRV_VOWN_INFO *);
EXPORT_SYMBOL(vmeAcquireBusOwnership);
extern int vmeReleaseBusOwnership(void);
EXPORT_SYMBOL(vmeReleaseBusOwnership);
extern int vmeQueryBusOwnership(GEF_BOOL *);
EXPORT_SYMBOL(vmeQueryBusOwnership);
extern int vmeEnableIrq(GEF_VME_INT_INFO *);
EXPORT_SYMBOL(vmeEnableIrq);
extern int vmeDisableIrq(GEF_VME_INT_INFO *);
EXPORT_SYMBOL(vmeDisableIrq);
extern int vmeFreeIrqWaits(void);
EXPORT_SYMBOL(vmeFreeIrqWaits);
extern int vmeSetDebugFlags(GEF_UINT32 *);
EXPORT_SYMBOL(vmeSetDebugFlags);
extern int vmeGetDebugFlags(GEF_UINT32 *);
EXPORT_SYMBOL(vmeGetDebugFlags);
extern int vmeRead(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT8 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements);
EXPORT_SYMBOL(vmeRead);
extern int vmeRead8(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT8 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements);
EXPORT_SYMBOL(vmeRead8);
extern int vmeRead16(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT16 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements);
EXPORT_SYMBOL(vmeRead16);
extern int vmeRead32(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT32 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements);
EXPORT_SYMBOL(vmeRead32);
extern int vmeRead64(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT64 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements);
EXPORT_SYMBOL(vmeRead64);
extern int vmeCreateMasterWindow(GEF_VME_DRV_CREATE_MASTER_WINDOW_INFO *);
EXPORT_SYMBOL(vmeCreateMasterWindow);
extern int vmeReleaseMasterWindow(GEF_VME_DRV_RELEASE_MASTER_WINDOW_HDL_INFO *);
EXPORT_SYMBOL(vmeReleaseMasterWindow);
extern int vmeReadModifyWrite(GEF_VME_DRV_READ_MODIFY_WRITE_INFO *);
EXPORT_SYMBOL(vmeReadModifyWrite);
extern int vmeWrite(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT8 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements);
EXPORT_SYMBOL(vmeWrite);
extern int vmeWrite8(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT8 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements);
EXPORT_SYMBOL(vmeWrite8);
extern int vmeWrite16(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT16 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements);
EXPORT_SYMBOL(vmeWrite16);
extern int vmeWrite32(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT32 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements);
EXPORT_SYMBOL(vmeWrite32);
extern int vmeWrite64(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT64 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements);
EXPORT_SYMBOL(vmeWrite64);
extern int vmeReleaseMasterWindows(void);
EXPORT_SYMBOL(vmeReleaseMasterWindows);
extern int vmeCreateSlaveWindow(GEF_VME_DRV_CREATE_SLAVE_WINDOW_INFO *);
EXPORT_SYMBOL(vmeCreateSlaveWindow);
extern int vmeRemoveSlaveWindow(GEF_VME_DRV_RELEASE_SLAVE_WINDOW_HDL_INFO *);
EXPORT_SYMBOL(vmeRemoveSlaveWindow);
extern int vmeReleaseSlaveWindows(void);
EXPORT_SYMBOL(vmeReleaseSlaveWindows);
extern int vmeCreateLocMonWindow(GEF_VME_DRV_LOCATION_MONITOR_INFO *);
EXPORT_SYMBOL(vmeCreateLocMonWindow);
extern int vmeReleaseLocMonWindow(GEF_VME_DRV_LOCATION_MONITOR_INFO *);
EXPORT_SYMBOL(vmeReleaseLocMonWindow);
extern int vmeQueryLocMonWindow(GEF_VME_DRV_LOCATION_MONITOR_INFO *);
EXPORT_SYMBOL(vmeQueryLocMonWindow);
extern int vmeCreateVraiWindow(GEF_VME_DRV_VRAI_INFO *);
EXPORT_SYMBOL(vmeCreateVraiWindow);
extern int vmeReleaseVraiWindow(GEF_VME_DRV_VRAI_INFO *);
EXPORT_SYMBOL(vmeReleaseVraiWindow);
extern int vmeQueryVraiWindow(GEF_VME_DRV_VRAI_INFO *);
EXPORT_SYMBOL(vmeQueryVraiWindow);

extern int vmeTransMasterWindow(GEF_VME_DRV_TRANSLATE_MASTER_WINDOW_INFO *);
EXPORT_SYMBOL(vmeTransMasterWindow);
extern int vmeMapMasterWindow(GEF_VME_MAP_MASTER_INFO *);
EXPORT_SYMBOL(vmeMapMasterWindow);
extern int vmeMapSlaveWindow(GEF_VME_MAP_SLAVE_INFO *);
EXPORT_SYMBOL(vmeMapSlaveWindow);

extern int vmeAllocDmaBuf(GEF_VME_DRV_ALLOC_DMA_BUF_INFO *);
EXPORT_SYMBOL(vmeAllocDmaBuf);
extern int vmeFreeDmaBuf(GEF_VME_DRV_FREE_DMA_BUF_INFO *);
EXPORT_SYMBOL(vmeFreeDmaBuf);
extern int vmeFreeDmaHandles(void);
EXPORT_SYMBOL(vmeFreeDmaHandles);

extern int vmeSetMasterHWByteswap(GEF_VME_HW_BYTESWAP_INFO *);
EXPORT_SYMBOL(vmeSetMasterHWByteswap);
extern int vmeGetMasterHWByteswap(GEF_VME_HW_BYTESWAP_INFO *);
EXPORT_SYMBOL(vmeGetMasterHWByteswap);
extern int vmeSetSlaveHWByteswap(GEF_VME_HW_BYTESWAP_INFO *);
EXPORT_SYMBOL(vmeSetSlaveHWByteswap);
extern int vmeGetSlaveHWByteswap(GEF_VME_HW_BYTESWAP_INFO *);
EXPORT_SYMBOL(vmeGetSlaveHWByteswap);
extern int vmeSetSwByteSwap(GEF_VME_DRV_BYTESWAP_INFO *);
EXPORT_SYMBOL(vmeSetSwByteSwap);
extern int vmeGetSwByteSwap(GEF_VME_DRV_BYTESWAP_INFO *);
EXPORT_SYMBOL(vmeGetSwByteSwap);

extern int vmeSetTimeOnBus(GEF_VME_DRV_TIME_ON_BUS_INFO *);
EXPORT_SYMBOL(vmeSetTimeOnBus);
extern int vmeGetTimeOnBus(GEF_VME_DRV_TIME_ON_BUS_INFO *);
EXPORT_SYMBOL(vmeGetTimeOnBus);
extern int vmeSetTimeOffBus(GEF_VME_DRV_TIME_OFF_BUS_INFO *);
EXPORT_SYMBOL(vmeSetTimeOffBus);
extern int vmeGetTimeOffBus(GEF_VME_DRV_TIME_OFF_BUS_INFO *);
EXPORT_SYMBOL(vmeGetTimeOffBus);

extern int vmeReleaseVraiWindows(void);
EXPORT_SYMBOL(vmeReleaseVraiWindows);
extern int vmeReleaseLocMonWindows(void);
EXPORT_SYMBOL(vmeReleaseLocMonWindows);

GEF_UINT32 vme_irqlog[GEF_VME_INT_LEVEL_LAST][0x100];
GEF_UINT8  vme_irq_waitcount[GEF_VME_INT_LEVEL_LAST];
GEF_UINT8  vme_vec_flags[GEF_VME_INT_LEVEL_LAST][0x100];
GEF_VME_BERR_INFO vme_irq_berr_info[2];
GEF_VME_INT_INFO vme_irq_int_info[GEF_VME_INT_LEVEL_LAST];
GEF_UINT8  vme_berr_write_buf;
GEF_UINT8  vme_berr_read_buf;
GEF_UINT8  dma_in_progress;
GEF_UINT8  dma_berr_failure;

#define	DEV_VALID	1
#define	DEV_EXCLUSIVE	2
#define	DEV_RW		4

/* No user space interface for the RT version of this kernel module. */
#ifndef USE_RTOS
static char vme_minor_dev_flags[] = { 
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  DEV_VALID,	/* /dev/vme_ctl */
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0
};
#endif

// Shared global.
#ifdef USE_RTOS
RT_TASK* dma_queue[2];
#else
wait_queue_head_t	dma_queue[2];
#endif
wait_queue_head_t	lm_queue;
wait_queue_head_t	mbox_queue;
#ifdef USE_RTOS
RT_TASK* irq_queue[GEF_VME_INT_LEVEL_LAST];
#else
wait_queue_head_t	irq_queue[GEF_VME_INT_LEVEL_LAST];
#endif

struct resource *vmepcimem;
int	tb_speed;
struct semaphore virq_inuse;
struct pci_dev *vme_pci_dev = NULL;
uint32_t vme_pci_start = 0;
uint32_t vme_pci_end = ~0;
int comm = 0;

/* No user space interface for the RT version of this kernel module. */
#ifndef USE_RTOS
/************************************************************************
*	Info needed by Probe function
************************************************************************/
static	GEF_UINT32	s_bPrbVmeBerr		= GEF_FALSE;
static	GEF_UINT32	s_PrbVmeAddrLo		= 0;
static	GEF_UINT32	s_PrbVmeAddrHi		= 0;
static	GEF_UINT32	s_PrbVmeAddrMode	= 0;
#endif
int (*g_pProbeBERRFunc)(GEF_VME_BERR_INFO *pBerrInfo) = NULL;

extern GEF_ENDIAN hwEndian;

#ifdef MODULE_LICENSE
MODULE_LICENSE("Proprietary");
#endif
//----------------------------------------------------------------------------
// Module parms
//----------------------------------------------------------------------------
#ifdef module_param 
module_param(vmechip_irq, uint, 0000);
module_param(vme_pci_start, uint, 0000);
module_param(vme_pci_end, uint, 0000);
module_param(comm, uint, 0000);
#else
MODULE_PARM(vmechip_irq, "1-255i");
MODULE_PARM(vme_pci_start, "i");
MODULE_PARM(vme_pci_end, "i");
MODULE_PARM(comm, "i");
#endif

/* No user space interface for the RT version of this kernel module. */
#ifndef USE_RTOS
//----------------------------------------------------------------------------
//  vmeGetVmeDriverVersion()
//     return version info for this driver
//----------------------------------------------------------------------------
static int vmeGetVmeDriverVersion(GEF_VME_DRV_VERSION_INFO *versionInfo)
{
    if (NULL == versionInfo)
    {
        return (GEF_STATUS_DRIVER_ERR);
    }

    versionInfo->version.major = GEF_VME_DRV_VER_MAJOR;
    versionInfo->version.minor = GEF_VME_DRV_VER_MINOR;
    versionInfo->version.build = GEF_VME_DRV_VER_BUILD;

    return(GEF_SUCCESS);
}
#endif


/* No user space interface for the RT version of this kernel module. */
#ifndef USE_RTOS
static int
vme_minor_type(int minor)
{
  if(minor > sizeof(vme_minor_dev_flags)){
    return(0);
  }
  return(vme_minor_dev_flags[minor]);
}
#endif

//----------------------------------------------------------------------------
//  vme_procinfo()
//     /proc interface into this driver
//----------------------------------------------------------------------------
static int vme_procinfo(char *buf, char **start, off_t fpos, int lenght, int *eof, void *data)
{
  char *p;

  p = buf;
  p += sprintf(p,"  gefvme driver version (Major.Minor.Build): %d.%d.%d\n",
                GEF_VME_DRV_VER_MAJOR,GEF_VME_DRV_VER_MINOR,GEF_VME_DRV_VER_BUILD);

  p += sprintf(p,"  vme device base addr = %08X\n",(int)vmechip_baseaddr);
  p += sprintf(p,"  vme device id        = %08X\n",(int)vmechip_devid);
  if(vme_syscon)
    p += sprintf(p,"  vme system controller = TRUE\n");
  else
    p += sprintf(p,"  vme system controller = FALSE\n");


  *eof = 1;
  return p - buf;
}

//----------------------------------------------------------------------------
//  register_proc()
//----------------------------------------------------------------------------
static void register_proc( void )
{
  vme_procdir = create_proc_entry("vmeinfo", S_IFREG | S_IRUGO, 0);
  vme_procdir->read_proc = vme_procinfo;
}

//----------------------------------------------------------------------------
//  unregister_proc()
//----------------------------------------------------------------------------
static void unregister_proc( void )
{
  remove_proc_entry("vmeinfo",0);
}

//----------------------------------------------------------------------------
//  vmeChipRegRead
//    Read a VME chip register.
//
//    Note that Tempe swizzles registers at offsets > 0x100 on its own.
//----------------------------------------------------------------------------
unsigned int
vmeChipRegRead(unsigned int *ptr)
{
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      return(ioread32(ptr));
      break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
      if(((char *)ptr) - ((char *)vmechip_baseaddr) > 0x100){
         return(longswap(*ptr));
      }
      return(ioread32(ptr));
      break;
  }
  return(0);
}

int vmeRegRead(GEF_VME_DRV_READ_WRITE_REG_INFO *regInfo)
{
    GEF_UINT32 reg_val;

    if (NULL == regInfo)
    {
        return (GEF_STATUS_DRIVER_ERR);
    }

    if (NULL == vmechip_baseaddr) 
    {
        return(GEF_STATUS_DEVICE_NOT_INIT);
    }

    switch (regInfo->width)
    {
    case GEF_VME_DATA_WIDTH_32:
        reg_val = vmeChipRegRead((unsigned int *)(vmechip_baseaddr+regInfo->offset));
        break;
    default:
        return (GEF_STATUS_NOT_SUPPORTED);
    }
    regInfo->value = reg_val;

    return(GEF_SUCCESS);
}

//----------------------------------------------------------------------------
//  vmeChipRegWrite
//    Write a VME chip register.
//
//    Note that Tempe swizzles registers at offsets > 0x100 on its own.
//----------------------------------------------------------------------------
void
vmeChipRegWrite(unsigned int *ptr, unsigned int value)
{
  switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
      iowrite32(value, ptr);
      break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
      if(((char *)ptr) - ((char *)vmechip_baseaddr) > 0x100){
         *ptr = longswap(value);
      } else {
         iowrite32(value, ptr);
      }
      break;
  }
}

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,5,43)

//NOTE: The following two functions are conditionaled out because the kiobuf
// stuff being used for accessing the user memory for DMA purposes is no longer
// available in the 2.6 kernels.  You have to make use of a function called
// get_user_pages to achieve the equivalent capability now.


//-----------------------------------------------------------------------------
// Function   : vmeIoctlUserFreeDma
// Description: Free the pages that comprise the linked list of DMA packets.
//-----------------------------------------------------------------------------
int
vmeIoctlUserFreeDma(
vmeDmaPacket_t *userStartPkt,
struct kiobuf *srcKiobuf,
struct kiobuf *dstKiobuf
)
{
  vmeDmaPacket_t *currentPkt;
  vmeDmaPacket_t *prevPkt;
  vmeDmaPacket_t *nextPkt;
  int pageno = 0;
  
  // Free all pages associated with the packets.
  currentPkt = userStartPkt;
  prevPkt = currentPkt;
  while(currentPkt != 0){
     if(srcKiobuf != 0){
        kunmap(srcKiobuf->maplist[pageno]);
     }
     if(dstKiobuf != 0){
        kunmap(dstKiobuf->maplist[pageno]);
     }
     nextPkt = currentPkt->pNextPacket;
     if(currentPkt+1 != nextPkt) {
         free_pages((int)prevPkt,0);
         prevPkt = nextPkt;
     }
     currentPkt = nextPkt;
     pageno++;
  }
  return(0);
}

//-----------------------------------------------------------------------------
// Function   : vmeIoctlUserSetupDma
// Description: Create DMA linked list that corresponds to a buffer
// in user space.
//-----------------------------------------------------------------------------
vmeDmaPacket_t *
vmeIoctlUserSetupDma(
vmeDmaPacket_t *vmeDma, 
char *srcAddr,
struct kiobuf *srcKiobuf,
char *dstAddr,
struct kiobuf *dstKiobuf,
int *returnStatus
)
{
  int	maxPerPage;
  int	currentPktcount;
  vmeDmaPacket_t *userStartPkt;
  vmeDmaPacket_t *currentPkt;
  int pageno = 0;
  int remainingbytecount;
  int thispacketbytecount;

  maxPerPage = PAGESIZE/sizeof(vmeDmaPacket_t)-1;
  userStartPkt = (vmeDmaPacket_t *)__get_free_pages(GFP_KERNEL, 0);
  if(userStartPkt == 0){ *returnStatus = -ENOMEM; return(0); }


  // First allocate pages for packets and create linked list
  remainingbytecount = vmeDma->byteCount;
  currentPkt = userStartPkt;
  currentPktcount = 0;
  while(remainingbytecount > 0){
     memcpy(currentPkt, vmeDma, sizeof(vmeDmaPacket_t));
     currentPkt->pNextPacket = 0;

     // Calculate # of bytes for this DMA packet.
     thispacketbytecount = PAGESIZE;
     if(srcKiobuf != 0){
        if(srcKiobuf->offset != 0){
           thispacketbytecount = PAGESIZE - srcKiobuf->offset;
        }
     }
     if(dstKiobuf != 0){
        if(dstKiobuf->offset != 0){
           thispacketbytecount = PAGESIZE - dstKiobuf->offset;
        }
     }
     if(thispacketbytecount > remainingbytecount){
         thispacketbytecount = remainingbytecount;
     }

     // Setup Source address for this packet
     switch(vmeDma->srcBus){
        case VME_DMA_USER:
           currentPkt->srcAddr = (unsigned int)
                   kmap(srcKiobuf->maplist[pageno]) + srcKiobuf->offset;
           srcKiobuf->offset += thispacketbytecount;
           if(srcKiobuf->offset == PAGESIZE){ 
              srcKiobuf->offset = 0;
           }
           currentPkt->srcBus = VME_DMA_KERNEL;
           break;
        case VME_DMA_PATTERN_BYTE:
        case VME_DMA_PATTERN_WORD:
           break;
        case VME_DMA_PATTERN_WORD_INCREMENT:
           currentPkt->srcAddr = (unsigned int)srcAddr;
           srcAddr += (thispacketbytecount/4);
           break;
        case VME_DMA_PATTERN_BYTE_INCREMENT:
        default:
           currentPkt->srcAddr = (unsigned int)srcAddr;
           srcAddr += thispacketbytecount;
           break;
     }
     if(currentPkt->srcBus == VME_DMA_KERNEL) {
        currentPkt->srcAddr = virt_to_bus((void *)currentPkt->srcAddr);
        currentPkt->srcBus = VME_DMA_PCI;
     }

     // Setup destination address for this packet
     switch(vmeDma->dstBus){
        case VME_DMA_USER:
           currentPkt->dstAddr = (unsigned int)
                   kmap(dstKiobuf->maplist[pageno]) + dstKiobuf->offset;
           dstKiobuf->offset += thispacketbytecount;
           if(dstKiobuf->offset == PAGESIZE){ 
              dstKiobuf->offset = 0;
           }
           currentPkt->dstBus = VME_DMA_KERNEL;
        break;
        default:
           currentPkt->dstAddr = (unsigned int)dstAddr;
           dstAddr += thispacketbytecount;
        break;
     }
     if(currentPkt->dstBus == VME_DMA_KERNEL) {
        currentPkt->dstAddr = virt_to_bus((void *)currentPkt->dstAddr);
        currentPkt->dstBus = VME_DMA_PCI;
     }

     currentPkt->byteCount = thispacketbytecount;

     remainingbytecount  -= thispacketbytecount;
     if(remainingbytecount > 0){ 
        currentPkt->pNextPacket = currentPkt + 1;
        currentPktcount++;
        if(currentPktcount >= maxPerPage){
              currentPkt->pNextPacket = 
                   (vmeDmaPacket_t *)__get_free_pages(GFP_KERNEL,0);
              currentPktcount = 0;
        } 
     }
     currentPkt = currentPkt->pNextPacket;
     pageno++;
  }
  
  // Return to packets list 
  *returnStatus = 0;
  return(userStartPkt);
}
#else /* LINUX_VERSION_CODE > KERNEL_VERSION(2,5,43) */

//-----------------------------------------------------------------------------
// Function   : vmeIoctlUserSetupMemMgtDma
// Description: Create DMA linked list that corresponds to a buffer
// in user space based on get_user_pages.
//-----------------------------------------------------------------------------
vmeDmaPacket_t *
vmeIoctlUserSetupMemMgtDma(
vmeDmaPacket_t *vmeDma, 
char *srcAddr,
int src_nr_pages,
struct page **src_pages,
char *dstAddr,
int dst_nr_pages,
struct page **dst_pages,
int *returnStatus
)
{
  int	maxPerPage;
  int	currentPktcount;
  vmeDmaPacket_t *userStartPkt;
  vmeDmaPacket_t *currentPkt;
  int pageno = 0;
  int remainingbytecount = 0;
  int thispacketbytecount = 0;
  int src_offset = 0;
  int dst_offset = 0;

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "In vmeIoctlUserSetupMemMgtDma\n");
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, " vmeDma = 0x%x\n",(unsigned int)vmeDma);
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, " srcAddr = 0x%x\n", (unsigned int)srcAddr);
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, " src_nr_pages = 0x%x\n", src_nr_pages);
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, " src_pages = 0x%x\n", (unsigned int)src_pages);
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, " dstAddr = 0x%x\n", (unsigned int)dstAddr);
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, " dst_nr_pages = 0x%x\n", dst_nr_pages);
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, " dst_pages = 0x%x\n", (unsigned int)dst_pages);

  maxPerPage = PAGESIZE/sizeof(vmeDmaPacket_t)-1;
  userStartPkt = (vmeDmaPacket_t *)__get_free_pages(GFP_KERNEL, 0);
  if(userStartPkt == 0){ *returnStatus = -ENOMEM; return(0); }


  // First allocate pages for packets and create linked list
  remainingbytecount = vmeDma->byteCount;
  currentPkt = userStartPkt;
  currentPktcount = 0;

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "remainingbytecount 0x%x\n",remainingbytecount);

  while(remainingbytecount > 0){
     memcpy(currentPkt, vmeDma, sizeof(vmeDmaPacket_t));
     currentPkt->pNextPacket = 0;

     // Calculate # of bytes for this DMA packet.
     thispacketbytecount = PAGESIZE;

     if(src_nr_pages > 0) {

         src_offset = 0;
         if (pageno==0) {
             src_offset = (unsigned int)srcAddr & ~PAGE_MASK;
         }

         GEF_VME_DEBUG(GEF_DEBUG_TRACE, "source offset 0x%x\n",src_offset);

         if (src_offset) {
             thispacketbytecount = PAGESIZE - src_offset;
         }

         GEF_VME_DEBUG(GEF_DEBUG_TRACE, "src thispacketbytecount 0x%x\n",thispacketbytecount);

     }

     if(dst_nr_pages > 0) {

         dst_offset = 0;
         if (pageno==0) {
	   dst_offset = (unsigned int)dstAddr & ~PAGE_MASK;
         }

         GEF_VME_DEBUG(GEF_DEBUG_TRACE, "dest offset 0x%x\n",dst_offset);

	 if (dst_offset) {
             thispacketbytecount = PAGESIZE - dst_offset;
	 }

         GEF_VME_DEBUG(GEF_DEBUG_TRACE, "dst thispacketbytecount 0x%x\n",thispacketbytecount);

     }

     if(thispacketbytecount > remainingbytecount){
         thispacketbytecount = remainingbytecount;
     }

     // Setup Source address for this packet
     switch(vmeDma->srcBus){
        case VME_DMA_USER:

           currentPkt->srcAddr = (unsigned int)
                   kmap(src_pages[pageno]) + src_offset;

           GEF_VME_DEBUG(GEF_DEBUG_TRACE, "src_pages[%x] = %x,src_offset = %x\n",
                pageno,(unsigned int)page_address(src_pages[pageno]),src_offset);
           printk("kmap currentPkt->srcAddr 0x%x count 0x%x\n",
	              currentPkt->srcAddr,thispacketbytecount);

           src_offset += thispacketbytecount;
		   if(src_offset == PAGESIZE) {
			   src_offset = 0;
		   }
           currentPkt->srcBus = VME_DMA_KERNEL;
           break;
        case VME_DMA_PATTERN_BYTE:
        case VME_DMA_PATTERN_WORD:
           break;
        case VME_DMA_PATTERN_WORD_INCREMENT:
           currentPkt->srcAddr = (unsigned int)srcAddr;
           srcAddr += (thispacketbytecount/4);
           break;
        case VME_DMA_PATTERN_BYTE_INCREMENT:
        default:
           currentPkt->srcAddr = (unsigned int)srcAddr;
           srcAddr += thispacketbytecount;
           break;
     }
     if(currentPkt->srcBus == VME_DMA_KERNEL) {
        currentPkt->srcAddr = virt_to_bus((void *)currentPkt->srcAddr);
        currentPkt->srcBus = VME_DMA_PCI;
     }

     // Setup destination address for this packet
     switch(vmeDma->dstBus){
        case VME_DMA_USER:
           currentPkt->dstAddr = (unsigned int)
                   kmap(dst_pages[pageno]) + dst_offset;

           GEF_VME_DEBUG(GEF_DEBUG_TRACE, "kmap currentPkt->dstAddr 0x%x count 0x%x\n",
	              currentPkt->dstAddr,thispacketbytecount);

           dst_offset += thispacketbytecount;
		   if(dst_offset == PAGESIZE) {
			   dst_offset = 0;
		   }
           currentPkt->dstBus = VME_DMA_KERNEL;
        break;
        default:
           currentPkt->dstAddr = (unsigned int)dstAddr;
           dstAddr += thispacketbytecount;
        break;
     }
     if(currentPkt->dstBus == VME_DMA_KERNEL) {
        currentPkt->dstAddr = virt_to_bus((void *)currentPkt->dstAddr);
        currentPkt->dstBus = VME_DMA_PCI;
     }

     currentPkt->byteCount = thispacketbytecount;

     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "bus currentPkt->srcAddr 0x%x currentPkt->dstAddr 0x%x\n",
	              currentPkt->srcAddr,currentPkt->dstAddr);

     remainingbytecount  -= thispacketbytecount;
     if(remainingbytecount > 0){ 
        currentPkt->pNextPacket = currentPkt + 1;
        currentPktcount++;
        if(currentPktcount >= maxPerPage){
              currentPkt->pNextPacket = 
                   (vmeDmaPacket_t *)__get_free_pages(GFP_KERNEL,0);
              currentPktcount = 0;
        } 
     }
     currentPkt = currentPkt->pNextPacket;
     pageno++;
  }

  // Return to packets list 
  *returnStatus = 0;
  return(userStartPkt);
}

#endif

//-----------------------------------------------------------------------------
// Function   : vmeIoctlDoUserDma
// Description: Setup and perform a VME DMA to/from a buffer in user space.
//-----------------------------------------------------------------------------
int
vmeDoUserDma(vmeDmaPacket_t *startPkt)
{
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,5,43)

   int src_res = 0;
   unsigned long src_start = 0;
   unsigned long src_end = 0;
   int src_nr_pages = 0;
   struct page **src_pages = 0;

   int dst_res = 0;
   unsigned long dst_start = 0;
   unsigned long dst_end = 0;
   int dst_nr_pages = 0;
   struct page **dst_pages = 0;

   int i,j;
   unsigned int max_pages = 113; /* Set limit to 512K - 60K or 0x71000 bytes */
   
   vmeDmaPacket_t *currentPkt;
   vmeDmaPacket_t *prevPkt;
   vmeDmaPacket_t *nextPkt;

#else

   struct kiobuf *srcIobuf = 0;
   struct kiobuf *dstIobuf = 0;

#endif
   int result = 0;
   vmeDmaPacket_t *newstartPkt = 0;  

#if LINUX_VERSION_CODE > KERNEL_VERSION(2,5,43)

   /* Overflow */
   if ((startPkt->srcAddr + startPkt->byteCount) < startPkt->srcAddr) {
	   return(-EINVAL);
   }

   if (startPkt->byteCount == 0) {
	   return(0);
   }

   if (startPkt->srcBus == VME_DMA_USER) {

      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Source Bus = VME_DMA_USER\n"); 

      src_end = (startPkt->srcAddr + startPkt->byteCount + PAGE_SIZE - 1) >> PAGE_SHIFT;
      src_start = startPkt->srcAddr >> PAGE_SHIFT;
      src_nr_pages = src_end - src_start;

      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "startPkt->srcAddr = 0x%x\n",startPkt->srcAddr); 
      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "startPkt->byteCount = 0x%x\n",startPkt->byteCount); 
      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "src_end = 0x%lx\n",src_end); 
      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "src_start = 0x%lx\n",src_start); 
      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "src_nr_pages = 0x%x\n",src_nr_pages); 

      if (src_nr_pages > max_pages) {
          return (-ENOMEM);
      }

      if ((src_pages = kmalloc(max_pages * sizeof(*src_pages), GFP_KERNEL)) == NULL) {
		  return (-ENOMEM);
      }

      down_read(&current->mm->mmap_sem);

      src_res = get_user_pages(
    	   	           current,
                       current->mm,
                       startPkt->srcAddr & PAGE_MASK, /* Page aligned address */
                       src_nr_pages,
                       READ, /* pages mapped for read access, not write */
                       0, /* don't force */
                       src_pages,
                       NULL);

      up_read(&current->mm->mmap_sem);

      if (src_res < src_nr_pages) {

          GEF_VME_DEBUG(GEF_DEBUG_TRACE, "not enough src pages mapped from get_user_pages\n"); 
         
          result = (-ENOMEM);  
          goto dma_unmap;
      }
   }

   if (startPkt->dstBus == VME_DMA_USER) {

      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Dest Bus = VME_DMA_USER\n"); 

      dst_end = (startPkt->dstAddr + startPkt->byteCount + PAGE_SIZE - 1) >> PAGE_SHIFT;
      dst_start = startPkt->dstAddr >> PAGE_SHIFT;
      dst_nr_pages = dst_end - dst_start;

      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "startPkt->dstAddr = 0x%x\n",startPkt->dstAddr); 
      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "startPkt->byteCount = 0x%x\n",startPkt->byteCount); 
      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "dst_end = 0x%lx\n",dst_end); 
      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "dst_start = 0x%lx\n",dst_start); 
      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "dst_nr_pages = 0x%x\n",dst_nr_pages); 

      if (dst_nr_pages > max_pages) {
          return (-ENOMEM);
      }

      if ((dst_pages = kmalloc(max_pages * sizeof(*dst_pages), GFP_KERNEL)) == NULL) {
		  return (-ENOMEM);
      }

      down_read(&current->mm->mmap_sem);

      dst_res = get_user_pages(
    	   	           current,
                       current->mm,
                       startPkt->dstAddr & PAGE_MASK, /* Page aligned address */
                       dst_nr_pages,
                       WRITE, /* pages mapped for write access */
                       0, /* don't force */
                       dst_pages,
                       NULL);

      up_read(&current->mm->mmap_sem);

      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "dst pages mapped from get_user_pages = 0x%x\n",dst_res); 

      if (dst_res < dst_nr_pages) {

      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "not enough dst pages mapped from get_user_pages\n"); 
         
         result = (-ENOMEM);
         goto dma_unmap;
      }
   }

   GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeIoctlUserSetupMemMgtDma\n");
   GEF_VME_DEBUG(GEF_DEBUG_TRACE, " startPkt = 0x%x\n",(unsigned int)startPkt);
   GEF_VME_DEBUG(GEF_DEBUG_TRACE, " startPkt->srcAddr = 0x%x\n", (unsigned int)startPkt->srcAddr);
   GEF_VME_DEBUG(GEF_DEBUG_TRACE, " src_nr_pages = 0x%x\n", src_nr_pages);
   GEF_VME_DEBUG(GEF_DEBUG_TRACE, " src_pages = 0x%x\n", (unsigned int)src_pages);
   GEF_VME_DEBUG(GEF_DEBUG_TRACE, " startPkt->dstAddr = 0x%x\n", (unsigned int)startPkt->dstAddr);
   GEF_VME_DEBUG(GEF_DEBUG_TRACE, " dst_nr_pages = 0x%x\n", dst_nr_pages);
   GEF_VME_DEBUG(GEF_DEBUG_TRACE, " dst_pages = 0x%x\n", (unsigned int)dst_pages);

   newstartPkt = vmeIoctlUserSetupMemMgtDma(startPkt, 
           (char *)startPkt->srcAddr, src_nr_pages, src_pages,
           (char *)startPkt->dstAddr, dst_nr_pages, dst_pages,
           &result);

   vmeSyncData();

   if(result == 0) {
      result = vmeDoDma(newstartPkt);
      startPkt->vmeDmaElapsedTime = newstartPkt->vmeDmaElapsedTime;
   }

   /* Ensure that destination user pages are written back out */
   if (result == 0) {
       if (dst_res > 0) {

           GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Setting user mode dest pages dirty: 0x%x pages\n",dst_res); 

           for (j=0; j < dst_res; j++) {
               if (! PageReserved(dst_pages[j]))
                   SetPageDirty(dst_pages[j]);
           }
       }
   }

dma_unmap:

       GEF_VME_DEBUG(GEF_DEBUG_TRACE, "dma_unmap: kunmap source pages: 0x%x pages\n",src_nr_pages);

   for (i=0;i<src_nr_pages;i++) {
       kunmap (src_pages[i]);
   }

       GEF_VME_DEBUG(GEF_DEBUG_TRACE, "dma_unmap: kunmap dst pages: 0x%x pages\n",dst_nr_pages);

   for (i=0;i<dst_nr_pages;i++) {
       kunmap (dst_pages[i]);
   }
     
   if (src_res > 0) {

           GEF_VME_DEBUG(GEF_DEBUG_TRACE, "dma_unmap: src_pages page_cache_release: 0x%x pages\n",src_res); 

       for (j=0; j < src_res; j++) {
           page_cache_release(src_pages[j]);
       }
   }

   if (src_pages) {

       GEF_VME_DEBUG(GEF_DEBUG_TRACE, "dma_unmap: src_pages kfree \n"); 

       kfree(src_pages);
   }
 
   if (dst_res > 0) {

           GEF_VME_DEBUG(GEF_DEBUG_TRACE, "dma_unmap: dst_pages page_cache_release: 0x%x pages\n",dst_res); 

       for (j=0; j < dst_res; j++) {

           page_cache_release(dst_pages[j]);
       }
   }

   if (dst_pages) {

       GEF_VME_DEBUG(GEF_DEBUG_TRACE, "dma_unmap: dst_pages kfree \n"); 

       kfree(dst_pages);
   }
     
   // Free all pages associated with the packets.
   currentPkt = newstartPkt;
   prevPkt = currentPkt;

   GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Freeing pages for packets\n");

   while(currentPkt != 0){
       nextPkt = currentPkt->pNextPacket;
       if(currentPkt+1 != nextPkt) {
           free_pages((int)prevPkt,0);
           prevPkt = nextPkt;
       }
       
       currentPkt = nextPkt;
   }
   
#else
   // allocate kiovecs as needed for source & destination
   if(startPkt->srcBus == VME_DMA_USER) {
      if(alloc_kiovec(1, &srcIobuf)) {
          return(-ENOMEM);
      }
   }
   if(startPkt->dstBus == VME_DMA_USER) {
      if(alloc_kiovec(1, &dstIobuf)) {
          if(srcIobuf != 0) free_kiovec(1, &srcIobuf);
          return(-ENOMEM);
      }
   }

   if(srcIobuf != 0) result = map_user_kiobuf(WRITE,  srcIobuf, 
                         startPkt->srcAddr, startPkt->byteCount);
   if(result) {
      if(srcIobuf != 0) free_kiovec(1, &srcIobuf);
      if(dstIobuf != 0) free_kiovec(1, &dstIobuf);
      return(-ENOMEM);
   }
   if(dstIobuf != 0) result = map_user_kiobuf(READ, dstIobuf,
                         startPkt->dstAddr, startPkt->byteCount);
   if(result) {
      if(srcIobuf != 0) unmap_kiobuf(srcIobuf);
      if(srcIobuf != 0) free_kiovec(1, &srcIobuf);
      if(dstIobuf != 0) free_kiovec(1, &dstIobuf);
      return(-ENOMEM);
   }

   newstartPkt = vmeIoctlUserSetupDma(startPkt, 
           (char *)startPkt->srcAddr, srcIobuf,
           (char *)startPkt->dstAddr, dstIobuf,
           &result);

   if(result == 0) {
      result = vmeDoDma(newstartPkt);
      memcpy(startPkt, newstartPkt, sizeof(vmeDmaPacket_t));
      vmeIoctlUserFreeDma(newstartPkt, srcIobuf, dstIobuf);
   }

   if(srcIobuf != 0) unmap_kiobuf(srcIobuf);
   if(dstIobuf != 0) unmap_kiobuf(dstIobuf);
   if(srcIobuf != 0) free_kiovec(1, &srcIobuf);
   if(dstIobuf != 0) free_kiovec(1, &dstIobuf);
#endif
   return(result);
}

//-----------------------------------------------------------------------------
// Function   : vmeIoctlFreeDma
// Description: Free the pages associated with the linked list of DMA packets.
//-----------------------------------------------------------------------------
int
vmeIoctlFreeDma(vmeDmaPacket_t *startPkt)
{
  vmeDmaPacket_t *currentPkt;
  vmeDmaPacket_t *prevPkt;
  vmeDmaPacket_t *nextPkt;
  
  // Free all pages associated with the packets.
  currentPkt = startPkt;
  prevPkt = currentPkt;
  while(currentPkt != 0){
     nextPkt = currentPkt->pNextPacket;
     if(currentPkt+1 != nextPkt) {
         free_pages((int)prevPkt,0);
         prevPkt = nextPkt;
     }
     currentPkt = nextPkt;
  }
  return(0);
}


//-----------------------------------------------------------------------------
// Function   : vmeIoctlSetupDma
// Description: Read descriptors from user space and create a linked list
//    of DMA packets.
//  
//-----------------------------------------------------------------------------
vmeDmaPacket_t *
vmeIoctlSetupDma(vmeDmaPacket_t *vmeDma, int *returnStatus)
{
    vmeDmaPacket_t *startPkt;
    vmeDmaPacket_t *currentPkt;
    
    currentPkt = vmeDma;
    startPkt = currentPkt;
    currentPkt->pNextPacket = 0;
    if(currentPkt->srcBus == VME_DMA_KERNEL){
        currentPkt->srcAddr = virt_to_bus((void *)currentPkt->srcAddr);
    }
    if(currentPkt->dstBus == VME_DMA_KERNEL){
        currentPkt->dstAddr = virt_to_bus((void *)currentPkt->dstAddr);
    }
    
    // Return to packets list 
    *returnStatus = 0;
    return(startPkt);
}

//-----------------------------------------------------------------------------
// Function   : vmeIoctlDma()
// Inputs     : 
//   User pointer to DMA packet (possibly chained)
//   DMA channel number.
// Outputs    : returns 0 on success, error code on failure.
// Description: 
//   Copy DMA chain into kernel space.
//   Verify validity of chain.
//   Wait, if needed, for DMA channel to be available.
//   Do the DMA operation.
//   Free the DMA channel.
//   Copy the DMA packet with status back to user space.
//   Free resources allocated by this operation.
// Remarks    : 
//    Note, due to complexity, DMA to/from a user space buffer is dealt with 
//    via a separate routine.
// History    : 
//-----------------------------------------------------------------------------
int
vmeIoctlDma(vmeDmaPacket_t *vmeDma, int channel)
{

  vmeDmaPacket_t *startPkt;
  int status = 0;

  // Read the (possibly linked) descriptors from the user
  startPkt = vmeIoctlSetupDma(vmeDma, &status);
  if(status < 0){
     return(status);
  }

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeIoctlDma channel %x, startPkt->byteCount 0x%x\n",channel,startPkt->byteCount);

  startPkt->channel_number = channel;
  if(startPkt->byteCount <= 0){
     vmeIoctlFreeDma(startPkt);
     return(-EINVAL);
  }

  // If user to user mode, buffers must be page aligned.
  if((startPkt->srcBus == VME_DMA_USER) && 
     (startPkt->dstBus == VME_DMA_USER)){
     if((startPkt->dstAddr & (PAGESIZE-1)) != (startPkt->srcAddr & (PAGESIZE-1))){
         vmeIoctlFreeDma(startPkt);
         return(-EINVAL);
     }
  }

  // user mode DMA can only support one DMA packet.
  if((startPkt->srcBus == VME_DMA_USER) || 
     (startPkt->dstBus == VME_DMA_USER)){
     if(startPkt->pNextPacket != NULL) {
         vmeIoctlFreeDma(startPkt);
         return(-EINVAL);
     }
  }

  // Wait for DMA channel to be available.
  if(down_interruptible(&devinuse[channel+VME_MINOR_DMA])) {
    vmeIoctlFreeDma(startPkt);
    return(-ERESTARTSYS);
  }

  dma_in_progress = GEF_TRUE;
  dma_berr_failure = GEF_FALSE;

  // Do the DMA.
  if((startPkt->srcBus == VME_DMA_USER) || 
     (startPkt->dstBus == VME_DMA_USER)){
     status = vmeDoUserDma(startPkt);
  } else {
     status = vmeDoDma(startPkt);
  }

  // This status set by ISR
  if (GEF_TRUE == dma_berr_failure){
      status = GEF_STATUS_INVAL_ADDR;
      dma_berr_failure = GEF_FALSE;
  }

  dma_in_progress = GEF_FALSE;

  // Free the DMA channel.
  up(&devinuse[channel+VME_MINOR_DMA]);

  return(status);
}

#ifdef SCATTER_GATHER_DMA_SUPPORTED
static int vmeStartWriteDma(GEF_VME_DRV_READ_WRITE_DMA_INFO *write_dma_info) {

    vmeDmaPacket_t vmeDma;
    int channel = 0;
    int status;

    memset(&vmeDma, 0, sizeof(vmeDma));
	vmeDma.maxPciBlockSize = 4096;
	vmeDma.maxVmeBlockSize = 4096;
	vmeDma.byteCount = write_dma_info->length; /* bytecount */
	vmeDma.srcBus = VME_DMA_USER; /* srcbus */
	vmeDma.srcAddr = (unsigned int)write_dma_info->virt_addr; /* srcaddress */
	vmeDma.srcVmeAttr.maxDataWidth = VME_D32;
	vmeDma.srcVmeAttr.addrSpace = VME_A32; /* srcmode */
	vmeDma.srcVmeAttr.userAccessType = VME_SUPER;
	vmeDma.srcVmeAttr.dataAccessType = VME_DATA;
	vmeDma.srcVmeAttr.xferProtocol = VME_SCT; /*VME_MBLT;*/ /* srcprotocol */
	vmeDma.dstBus = VME_DMA_VME; /*dstbus*/
	vmeDma.dstAddr =  (unsigned int)write_dma_info->vme_addr.lower; /*dstaddress*/
	vmeDma.dstVmeAttr.maxDataWidth = VME_D32;
	vmeDma.dstVmeAttr.addrSpace = VME_A32; /*dstmode*/
	vmeDma.dstVmeAttr.userAccessType = VME_SUPER;
	vmeDma.dstVmeAttr.dataAccessType = VME_DATA;
	vmeDma.dstVmeAttr.xferProtocol = VME_SCT; /*VME_MBLT;*/ /*dstprotocol*/

    printk("vmeStartWriteDma calling vmeIoctlDma vmeDma.byteCount %x\n",vmeDma.byteCount);


    status = vmeIoctlDma(&vmeDma, channel);

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeStartWriteDma called vmeIoctlDma: status %x\n",status);

    return(status);
}

static int vmeStartDmaBuf(GEF_VME_DRV_READ_WRITE_DMA_BUF_INFO *dma_buf_info, 
                               int direction) 
{

    vmeDmaPacket_t vmeDma;
    int channel = 0;
    int status;
    DRV_VME_BROADCAST_ID drv_bcast_select_2esst;
    addressMode_t  drv_addr_space;
    dataWidth_t    drv_xfer_max_dwidth;
    vme2esstRate_t drv_xfer_rate_2esst;
    int            drv_xfer_protocol;
    int            drv_user_access_type;
    int            drv_data_access_type;
    GEF_VME_DMA_FLAGS drv_von_value;
    GEF_VME_DMA_FLAGS drv_voff_value;
    struct GEF_VME_DMA_OSSPEC* dma_osspec;

    if (NULL == dma_buf_info)
    {
        return(GEF_STATUS_DRIVER_ERR);
    }

    dma_osspec = (struct GEF_VME_DMA_OSSPEC*) (GEF_OS_CAST) dma_buf_info->dma_osspec_hdl;

    // Determine if DMA transaction exceeds the size of the kernel allocated DMA buffer
    if ((dma_buf_info->offset + dma_buf_info->length) > dma_osspec->size)
    {
        return (GEF_STATUS_VME_INVALID_SIZE);
    }

    /* address space */
    switch(dma_buf_info->addr.addr_space)
    {
    case GEF_VME_ADDR_SPACE_A16:   drv_addr_space = VME_A16; break;
    case GEF_VME_ADDR_SPACE_A24:   drv_addr_space = VME_A24; break;
    case GEF_VME_ADDR_SPACE_A32:   drv_addr_space = VME_A32; break;
    case GEF_VME_ADDR_SPACE_A64:   drv_addr_space = VME_A64; break;
    case GEF_VME_ADDR_SPACE_CRCSR: drv_addr_space = VME_CRCSR; break;
    case GEF_VME_ADDR_SPACE_USER1: drv_addr_space = VME_USER1; break;
    case GEF_VME_ADDR_SPACE_USER2: drv_addr_space = VME_USER2; break;
    case GEF_VME_ADDR_SPACE_USER3: drv_addr_space = VME_USER3; break;
    case GEF_VME_ADDR_SPACE_USER4: drv_addr_space = VME_USER4; break;
    default: return (GEF_STATUS_VME_INVALID_ADDRESS_SPACE);
    }

    /* transfer max width */
    switch(dma_buf_info->addr.transfer_max_dwidth)
    {
    case GEF_VME_TRANSFER_MAX_DWIDTH_8:  drv_xfer_max_dwidth = VME_D8; break;
    case GEF_VME_TRANSFER_MAX_DWIDTH_16: drv_xfer_max_dwidth = VME_D16; break;
    case GEF_VME_TRANSFER_MAX_DWIDTH_32: drv_xfer_max_dwidth = VME_D32; break;
    case GEF_VME_TRANSFER_MAX_DWIDTH_64: drv_xfer_max_dwidth = VME_D64; break;
    default: return (GEF_STATUS_VME_INVALID_TRANSFER_WIDTH);
    }

    /* transfer mode */
    switch(dma_buf_info->addr.transfer_mode)
    {
    case GEF_VME_TRANSFER_MODE_SCT:    drv_xfer_protocol = VME_SCT; break;
    case GEF_VME_TRANSFER_MODE_BLT:    drv_xfer_protocol = VME_BLT; break;
    case GEF_VME_TRANSFER_MODE_MBLT:   drv_xfer_protocol = VME_MBLT; break;
    case GEF_VME_TRANSFER_MODE_2eVME:  drv_xfer_protocol = VME_2eVME; break;
    case GEF_VME_TRANSFER_MODE_2eSST:  drv_xfer_protocol = VME_2eSST; break;
    case GEF_VME_TRANSFER_MODE_2eSSTB: drv_xfer_protocol = VME_2eSSTB; break;
    default: return (GEF_STATUS_VME_INVALID_TRANSFER_MODE);
    }

    /* address mode */
    drv_user_access_type = 0;
    if( (dma_buf_info->addr.addr_mode & GEF_VME_ADDR_MODE_USER) == GEF_VME_ADDR_MODE_USER)
    {
        drv_user_access_type |= VME_USER;
    }
    if( (dma_buf_info->addr.addr_mode & GEF_VME_ADDR_MODE_SUPER) == GEF_VME_ADDR_MODE_SUPER)
    {
        drv_user_access_type |= VME_SUPER;
    }

    drv_data_access_type = 0;
    if( (dma_buf_info->addr.addr_mode & GEF_VME_ADDR_MODE_DATA) == GEF_VME_ADDR_MODE_DATA)
    {
        drv_data_access_type |= VME_DATA;
    }
    if( (dma_buf_info->addr.addr_mode & GEF_VME_ADDR_MODE_PROGRAM) == GEF_VME_ADDR_MODE_PROGRAM)
    {
        drv_data_access_type |= VME_PROG;
    }

    /* broadcast id */
    switch(dma_buf_info->addr.broadcast_id)
    {
    case GEF_VME_BROADCAST_ID_DISABLE: drv_bcast_select_2esst = DRV_VME_BROADCAST_ID_DISABLE; break;
    case GEF_VME_BROADCAST_ID_1:       drv_bcast_select_2esst = DRV_VME_BROADCAST_ID_1; break;
    case GEF_VME_BROADCAST_ID_2:       drv_bcast_select_2esst = DRV_VME_BROADCAST_ID_2; break; 
    case GEF_VME_BROADCAST_ID_3:       drv_bcast_select_2esst = DRV_VME_BROADCAST_ID_3; break;
    case GEF_VME_BROADCAST_ID_4:       drv_bcast_select_2esst = DRV_VME_BROADCAST_ID_4; break;
    case GEF_VME_BROADCAST_ID_5:       drv_bcast_select_2esst = DRV_VME_BROADCAST_ID_5; break;
    case GEF_VME_BROADCAST_ID_6:       drv_bcast_select_2esst = DRV_VME_BROADCAST_ID_6; break;
    case GEF_VME_BROADCAST_ID_7:       drv_bcast_select_2esst = DRV_VME_BROADCAST_ID_7; break;
    case GEF_VME_BROADCAST_ID_8:       drv_bcast_select_2esst = DRV_VME_BROADCAST_ID_8; break;
    case GEF_VME_BROADCAST_ID_9:       drv_bcast_select_2esst = DRV_VME_BROADCAST_ID_9; break;
    case GEF_VME_BROADCAST_ID_10:      drv_bcast_select_2esst = DRV_VME_BROADCAST_ID_10; break;
    case GEF_VME_BROADCAST_ID_11:      drv_bcast_select_2esst = DRV_VME_BROADCAST_ID_11; break;
    case GEF_VME_BROADCAST_ID_12:      drv_bcast_select_2esst = DRV_VME_BROADCAST_ID_12; break;
    case GEF_VME_BROADCAST_ID_13:      drv_bcast_select_2esst = DRV_VME_BROADCAST_ID_13; break;
    case GEF_VME_BROADCAST_ID_14:      drv_bcast_select_2esst = DRV_VME_BROADCAST_ID_14; break;
    case GEF_VME_BROADCAST_ID_15:      drv_bcast_select_2esst = DRV_VME_BROADCAST_ID_15; break;
    case GEF_VME_BROADCAST_ID_16:      drv_bcast_select_2esst = DRV_VME_BROADCAST_ID_16; break;
    case GEF_VME_BROADCAST_ID_17:      drv_bcast_select_2esst = DRV_VME_BROADCAST_ID_17; break;
    case GEF_VME_BROADCAST_ID_18:      drv_bcast_select_2esst = DRV_VME_BROADCAST_ID_18; break;
    case GEF_VME_BROADCAST_ID_19:      drv_bcast_select_2esst = DRV_VME_BROADCAST_ID_19; break;
    case GEF_VME_BROADCAST_ID_20:      drv_bcast_select_2esst = DRV_VME_BROADCAST_ID_20; break;
    case GEF_VME_BROADCAST_ID_21:      drv_bcast_select_2esst = DRV_VME_BROADCAST_ID_21; break;
    default: return (GEF_STATUS_VME_INVALID_BROADCAST_ID);
    }

    /* 2esst rate */
    switch(dma_buf_info->addr.vme_2esst_rate)
    {
    case GEF_VME_2ESST_RATE_INVALID: drv_xfer_rate_2esst = VME_SSTNONE; break;
    case GEF_VME_2ESST_RATE_160:     drv_xfer_rate_2esst = VME_SST160; break;
    case GEF_VME_2ESST_RATE_267:     drv_xfer_rate_2esst = VME_SST267; break;
    case GEF_VME_2ESST_RATE_320:     drv_xfer_rate_2esst = VME_SST320; break;
    default: return (GEF_STATUS_VME_INVALID_2eSST_RATE);
    }

   /* VON value */
    drv_von_value = dma_buf_info->addr.flags & 0x0f0;
    switch(drv_von_value)
    {
    case GEF_VME_DMA_VON_0:     break;
    case GEF_VME_DMA_VON_32:    break;
    case GEF_VME_DMA_VON_64:    break;
    case GEF_VME_DMA_VON_128:   break;
    case GEF_VME_DMA_VON_256:   break;
    case GEF_VME_DMA_VON_512:   break;
    case GEF_VME_DMA_VON_1024:  break;
    case GEF_VME_DMA_VON_2048:  break;
    case GEF_VME_DMA_VON_4096:  break;
    case GEF_VME_DMA_VON_8192:  break;
    case GEF_VME_DMA_VON_16384: break;
    default: return (GEF_STATUS_VME_INVALID_VON_VALUE);
    }

   /* VOFF value */
    drv_voff_value = dma_buf_info->addr.flags & 0x0f00;
    switch(drv_voff_value)
    {
    case GEF_VME_DMA_VOFF_0us:    break;
    case GEF_VME_DMA_VOFF_1us:    break;
    case GEF_VME_DMA_VOFF_2us:    break;
    case GEF_VME_DMA_VOFF_4us:    break;
    case GEF_VME_DMA_VOFF_8us:    break;
    case GEF_VME_DMA_VOFF_16us:   break;
    case GEF_VME_DMA_VOFF_32us:   break;
    case GEF_VME_DMA_VOFF_64us:   break;
    case GEF_VME_DMA_VOFF_128us:  break;
    case GEF_VME_DMA_VOFF_256us:  break;
    case GEF_VME_DMA_VOFF_512us:  break;
    case GEF_VME_DMA_VOFF_1024us: break;
        default: return (GEF_STATUS_VME_INVALID_VOFF_VALUE);
    }

    dma_osspec = (struct GEF_VME_DMA_OSSPEC*)(GEF_OS_CAST)dma_buf_info->dma_osspec_hdl;

    // Determine if 64-bit Transactions are enabled for Universe
    vmeDma.ld64Enabled = dma_buf_info->addr.flags & GEF_VME_DMA_64_BIT;

    memset(&vmeDma, 0, sizeof(vmeDma));

    if (DRV_VME_DMA_WRITE == direction)
    {
        vmeDma.srcBus = VME_DMA_PCI;
        vmeDma.srcAddr = (unsigned int)dma_osspec->resource + 
            (unsigned int)dma_buf_info->offset;
        vmeDma.dstBus = VME_DMA_VME;
        vmeDma.dstAddr = (unsigned int)dma_buf_info->addr.lower;
    } else if (DRV_VME_DMA_READ == direction)
    {
        vmeDma.srcBus = VME_DMA_VME;
        vmeDma.srcAddr = (unsigned int)dma_buf_info->addr.lower;
        vmeDma.dstBus = VME_DMA_PCI;
        vmeDma.dstAddr = (unsigned int)dma_osspec->resource +
            (unsigned int)dma_buf_info->offset;        
    } else
    {
        return (GEF_STATUS_NOT_SUPPORTED);
    }
    
    // VFAR / PFAR
    if((dma_buf_info->addr.flags & GEF_VME_DMA_VFAR) == GEF_VME_DMA_VFAR)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeStartDmaBuf: Set VFAR on\n"); 
        vmeDma.vmeFlushOnAbortRead = VFAR_ON;
    }
    else
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeStartDmaBuf: Set VFAR off\n");
        vmeDma.vmeFlushOnAbortRead = VFAR_OFF;
    }

    if((dma_buf_info->addr.flags & GEF_VME_DMA_PFAR) == GEF_VME_DMA_PFAR)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeStartDmaBuf: Set PFAR on\n");
        vmeDma.pciFlushOnAbortRead = PFAR_ON;
    }
    else
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeStartDmaBuf: Set PFAR off\n");
        vmeDma.pciFlushOnAbortRead = PFAR_OFF;
    }

	vmeDma.maxVmeBlockSize = drv_von_value; 
    vmeDma.vmeBackOffTimer = drv_voff_value;
	vmeDma.byteCount = dma_buf_info->length;
    vmeDma.bcastSelect2esst = drv_bcast_select_2esst;

    vmeDma.srcVmeAttr.maxDataWidth = drv_xfer_max_dwidth;
    vmeDma.srcVmeAttr.xferRate2esst = drv_xfer_rate_2esst;
	vmeDma.srcVmeAttr.addrSpace = drv_addr_space;
	
    vmeDma.srcVmeAttr.userAccessType = drv_user_access_type;
	vmeDma.srcVmeAttr.dataAccessType = drv_data_access_type;
	vmeDma.srcVmeAttr.xferProtocol = drv_xfer_protocol;

    vmeDma.dstVmeAttr.maxDataWidth = drv_xfer_max_dwidth;
    vmeDma.dstVmeAttr.xferRate2esst = drv_xfer_rate_2esst;
	vmeDma.dstVmeAttr.addrSpace = drv_addr_space;

    vmeDma.dstVmeAttr.userAccessType = drv_user_access_type;
	vmeDma.dstVmeAttr.dataAccessType = drv_data_access_type;
	vmeDma.dstVmeAttr.xferProtocol = drv_xfer_protocol;

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeStartDmaBuf calling vmeIoctlDma vmeDma.byteCount %x\n",vmeDma.byteCount);
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Source address %#x Dest address %#x\n",vmeDma.srcAddr,vmeDma.dstAddr);

    status = vmeIoctlDma(&vmeDma, channel);

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeStartDmaBuf called vmeIoctlDma: status %x\n",status);

    return(status);
}

static int vmeStartWriteDmaBuf(GEF_VME_DRV_READ_WRITE_DMA_BUF_INFO *write_dma_buf_info) {
    
    DRV_VME_DMA_DIRECTION direction = DRV_VME_DMA_WRITE;
    int status;

    status = vmeStartDmaBuf(write_dma_buf_info, direction);

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeStartWriteDmaBuf called vmeStartDmaBuf: status %x\n",status);

    return(status);
}

static int vmeStartReadDmaBuf(GEF_VME_DRV_READ_WRITE_DMA_BUF_INFO *read_dma_buf_info) {
    
    DRV_VME_DMA_DIRECTION direction = DRV_VME_DMA_READ;
    int status;

    status = vmeStartDmaBuf(read_dma_buf_info, direction);

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeStartReadDmaBuf called vmeStartDmaBuf: status %x\n",status);

    return(status);
}

//-----------------------------------------------------------------------------
// Function   : vme_open()
// Inputs     : standard Linux device driver open arguments.
// Outputs    : returns 0 on success, error code on failure.
// Description: 
//   Verify device is valid,
//   If device is exclusive use and already open, return -EBUSY
//   increment device use count.
// Remarks    : 
// History    : 
//-----------------------------------------------------------------------------
static int vme_open(struct inode *inode,struct file *file)
{
  unsigned int minor = MINOR(inode->i_rdev);
  int minor_flags;

  /* Check for valid minor num */
  minor_flags = vme_minor_type(minor);
  if (!minor_flags){
    return(-ENODEV);
  }

  /* Restrict exclusive use devices to one open */
  if((minor_flags & DEV_EXCLUSIVE) && opened[minor]){
    return(-EBUSY);
  }

  opened[minor]++;
                
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vme_open called for minor %d, open count %d\n", minor, opened[minor]); 

  return(0);
}

//-----------------------------------------------------------------------------
// Function   : vme_release()
// Inputs     : standard Linux device driver release arguments.
// Outputs    : returns 0.
// Description: 
//  Decrement use count and return.
// Remarks    : 
// History    : 
//-----------------------------------------------------------------------------
static int vme_release(struct inode *inode,struct file *file)
{
  unsigned int minor = MINOR(inode->i_rdev);

  opened[minor]--;

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vme_release called for minor %d, open count %d\n", minor, opened[minor]); 

  /* release master windows on last process release */
  if(opened[minor] == 0)
  {
      vmeReleaseMasterWindows();
      vmeReleaseSlaveWindows();
      vmeReleaseVraiWindows();
      vmeReleaseLocMonWindows();
      
      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vme_release: last process close so close all master windows\n"); 

  }

  return 0;
}

static int vme_mmap(struct file *file,struct vm_area_struct *vma)
{

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "In vme_mmap:addr 0x%lx vma->vm_start 0x%lx vma->vm_end 0x%lx \n",
                    vma->vm_pgoff, vma->vm_start,vma->vm_end);

    /* Don't swap these pages out
    */
    vma->vm_flags |= VM_RESERVED | VM_IO;

    return(remap_pfn_range(vma, vma->vm_start, vma->vm_pgoff, 
        vma->vm_end - vma->vm_start, vma->vm_page_prot));
}

/**************************************************************************
*	This function is used to capture BERRs during the probe,
*	(peek/poke) processing.
**************************************************************************/
static	int	ProbeBERRFunc(GEF_VME_BERR_INFO *pBerrInfo)
{
	int	RetStat;

	/*********************************************************************
	*	If this handler fires, then the probe function must be running.
	*	So assume that we will handle the BERR
	*********************************************************************/
	RetStat = 0;

	/*********************************************************************
	*	It is possible that someone else caused the BERR, so
	*	check for that case.
	*********************************************************************/
	if (	(pBerrInfo->lower == s_PrbVmeAddrLo)
			&& (pBerrInfo->upper == s_PrbVmeAddrHi)	)
	{
          GEF_VME_DEBUG(GEF_DEBUG_TRACE,"ProbeBERRFunc: BERR\n");
		/* The address matches, we got a BERR */
		s_bPrbVmeBerr = GEF_TRUE;

		/* Put the default BERR handler back */
		g_pProbeBERRFunc = NULL;
	}
	else
	{
          GEF_VME_DEBUG(GEF_DEBUG_TRACE,"ProbeBERRFunc: BERR not handled\n");
          GEF_VME_DEBUG(GEF_DEBUG_TRACE,"ProbeBERRFunc: VME Address Lower 0x%x (0x%x)\n", pBerrInfo->lower, s_PrbVmeAddrLo);
          GEF_VME_DEBUG(GEF_DEBUG_TRACE,"ProbeBERRFunc: VME Address Upper 0x%x (0x%x)\n", pBerrInfo->upper, s_PrbVmeAddrHi);
		/* Otherwise, we will not handle this BERR */
		RetStat = 1;
	}
	return(RetStat);
}

//-----------------------------------------------------------------------------
// Function   : vme_ioctl()
// Inputs     : 
//    cmd -> the IOCTL command
//    arg -> pointer (if any) to the user ioctl buffer 
// Outputs    : 0 for sucess or errorcode for failure.
// Description: 
//    Copy in, if needed, the user input buffer pointed to by arg.
//    Call vme driver to perform actual ioctl
//    Copy out, if needed, the user output buffer pointed to be arg.
// Remarks    : 
// History    : 
//-----------------------------------------------------------------------------

static int vme_ioctl(struct inode *inode,struct file *file,unsigned int cmd, unsigned long arg)
{

  GEF_OSA_INTF_DATA data;
  GEF_VME_DRV_VIRQ_INFO virq_info;
  GEF_VME_DRV_WAIT_INFO wait_info;
  GEF_VME_DRV_BUS_ARB_INFO bus_arb_info;
  GEF_VME_DRV_BUS_REL_MODE_INFO bus_release_info;
  GEF_VME_DRV_BUS_REQ_MODE_INFO bus_request_info;
  GEF_VME_DRV_BUS_REQ_LEVEL_INFO bus_req_level_info;
  GEF_VME_DRV_BUS_TIMEOUT_INFO bus_timeout_info;
  GEF_VME_DRV_ARB_TIMEOUT_INFO arb_timeout_info;
  GEF_VME_DRV_BUS_BROADCAST_INFO broadcast_id_info;
  GEF_VME_DRV_SYSFAIL_INFO sysfail_info;
  GEF_VME_MAX_RETRY_INFO max_retry_info;
  GEF_VME_POST_WRITE_COUNT_INFO pwon_info;
  GEF_BOOL vown_info;
  GEF_VME_DRV_CREATE_MASTER_WINDOW_INFO master_info;
  GEF_VME_DRV_READ_WRITE_INFO read_info;
  GEF_UINT32 flag_info;
  GEF_VME_DRV_RELEASE_MASTER_WINDOW_HDL_INFO rel_master_info;
  GEF_VME_DRV_READ_MODIFY_WRITE_INFO read_modify_write_info;
  GEF_VME_DRV_READ_WRITE_INFO write_info;
  GEF_VME_DRV_CREATE_SLAVE_WINDOW_INFO slave_info;
  GEF_VME_DRV_RELEASE_SLAVE_WINDOW_HDL_INFO rel_slave_info;
#ifdef SCATTER_GATHER_DMA_SUPPORTED
  GEF_VME_DRV_READ_WRITE_DMA_INFO write_dma_info;
#endif
  GEF_VME_DRV_ALLOC_DMA_BUF_INFO alloc_dma_buf_info;
  GEF_VME_DRV_FREE_DMA_BUF_INFO free_dma_buf_info;
  GEF_VME_DRV_READ_WRITE_DMA_BUF_INFO write_dma_buf_info;
  GEF_VME_DRV_READ_WRITE_DMA_BUF_INFO read_dma_buf_info;
  GEF_VME_DRV_TRANSLATE_MASTER_WINDOW_INFO transMasterInfo;
  GEF_VME_DRV_READ_WRITE_REG_INFO regInfo;
  GEF_VME_DRV_VERSION_INFO versionInfo;
  GEF_VME_DRV_VOWN_INFO vownInfo;
  GEF_VME_HW_BYTESWAP_INFO byteswapInfo;
  GEF_VME_DRV_BYTESWAP_INFO swbyteswapInfo;
  GEF_VME_DRV_TIME_ON_BUS_INFO time_on_bus_info;
  GEF_VME_DRV_TIME_OFF_BUS_INFO time_off_bus_info;
  GEF_VME_DRV_READ_WRITE_INFO peek_info;
  GEF_VME_DRV_READ_WRITE_INFO poke_info;

  if (cmd != GEF_OSA_IOCTL_XFER_DATA)
  {
     return -EINVAL;
  }

  if (copy_from_user(&data, (void *)arg, sizeof(GEF_OSA_INTF_DATA)))
  {
     return -EFAULT;
  }

  switch (data.command)
  {

     case GEF_VME_FRAMEWORK_CODE_GENERATE_IRQ:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_VIRQ_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_VIRQ_INFO))
        {
           return -EBADMSG;
        }

        if (copy_from_user(&virq_info, (void*)data.dataToDriver, data.sizeToDriver))
        {
           return -EFAULT;
        }

        // Obtain access to VIRQ generator.
        if (down_interruptible(&virq_inuse))
        {
            return -ERESTARTSYS;
        }

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeGenerateIrq from ioctl for level %d vector %d\n",
                                          virq_info.level, virq_info.vector);

        data.result = vmeGenerateIrq(&virq_info);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

        up(&virq_inuse); // Release VIRQ generator.
        
        break;  
     }


     case GEF_VME_FRAMEWORK_CODE_WAIT_FOR_INTERRUPT:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_WAIT_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_WAIT_INFO))
        {
           return -EBADMSG;
        }

        if (copy_from_user(&wait_info, (void*)data.dataToDriver, data.sizeToDriver))
        {
           return -EFAULT;
        }

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeWaitForIrq from ioctl\n");

        data.result = vmeWaitForIrq(&wait_info);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

        if(data.result == 0) 
        {
            if(copy_to_user((void*)data.dataFromDriver, &wait_info, data.sizeFromDriver)) 
            {
                return -EFAULT;
            }
        }       
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_WAIT_ACK:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_WAIT_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_WAIT_INFO))
        {
           return -EBADMSG;
        }

        if (copy_from_user(&wait_info, (void*)data.dataToDriver, data.sizeToDriver))
        {
           return -EFAULT;
        }

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeWaitIrqAck from ioctl\n");

        data.result = vmeWaitIrqAck(&wait_info);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
       
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_RELEASE_WAIT:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_VIRQ_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_VIRQ_INFO))
        {
           return -EBADMSG;
        }

        if (copy_from_user(&virq_info, (void*)data.dataToDriver, data.sizeToDriver))
        {
           return -EFAULT;
        }


        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeReleaseIrqWait from ioctl for level %d vector %d\n",
                                          virq_info.level, virq_info.vector);

        data.result = vmeReleaseIrqWait(&virq_info);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_SET_BUS_ARB_MODE:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_BUS_ARB_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_BUS_ARB_INFO))
        {
           return -EBADMSG;
        }
        if (copy_from_user(&bus_arb_info, (void*)data.dataToDriver, data.sizeToDriver))
        {
           return -EFAULT;
        }

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeSetArbiter from ioctl for mode %d\n",
                                          bus_arb_info.arbitration_mode);

        data.result = vmeSetArbiter(&bus_arb_info);
        
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
        
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_GET_BUS_ARB_MODE:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_BUS_ARB_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_BUS_ARB_INFO))
        {
           return -EBADMSG;
        }

        data.result = vmeGetArbiter(&bus_arb_info);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeGetArbiter from ioctl, mode %d\n",
                                          bus_arb_info.arbitration_mode);
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
 
        if(data.result == 0) 
        {
            if(copy_to_user((void*)data.dataFromDriver, &bus_arb_info, data.sizeFromDriver)) 
            {
                return -EFAULT;
            }
        }
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_SET_BUS_REL_MODE:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_BUS_REL_MODE_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_BUS_REL_MODE_INFO))
        {
           return -EBADMSG;
        }
        if (copy_from_user(&bus_release_info, (void*)data.dataToDriver, data.sizeToDriver))
        {
           return -EFAULT;
        }

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeSetReleaseMode from ioctl for mode %d\n",
                                          bus_release_info.release_mode);

        data.result = vmeSetReleaseMode(&bus_release_info);
        
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
        
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_GET_BUS_REL_MODE:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_BUS_REL_MODE_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_BUS_REL_MODE_INFO))
        {
           return -EBADMSG;
        }

        data.result = vmeGetReleaseMode(&bus_release_info);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeGetReleaseMode from ioctl, mode %d\n",
                                          bus_release_info.release_mode);
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

        if(data.result == 0) 
        {
            if(copy_to_user((void*)data.dataFromDriver, &bus_release_info, data.sizeFromDriver)) 
            {
                return -EFAULT;
            }
        }
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_SET_BUS_REQ_MODE:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_BUS_REQ_MODE_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_BUS_REQ_MODE_INFO))
        {
           return -EBADMSG;
        }
        if (copy_from_user(&bus_request_info, (void*)data.dataToDriver, data.sizeToDriver))
        {
           return -EFAULT;
        }

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeSetRequestMode from ioctl for mode %d\n",
                                          bus_request_info.request_mode);

        data.result = vmeSetRequestMode(&bus_request_info);
        
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
        
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_GET_BUS_REQ_MODE:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_BUS_REQ_MODE_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_BUS_REQ_MODE_INFO))
        {
           return -EBADMSG;
        }

        data.result = vmeGetRequestMode(&bus_request_info);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeGetRequestMode from ioctl, mode %d\n",
                                          bus_request_info.request_mode);
        printk("data.result from call: 0x%x\n",data.result);

        if(data.result == 0) 
        {
            if(copy_to_user((void*)data.dataFromDriver, &bus_request_info, data.sizeFromDriver)) 
            {
                return -EFAULT;
            }
        }
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_SET_BUS_REQ_LEVEL:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_BUS_REQ_LEVEL_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_BUS_REQ_LEVEL_INFO))
        {
           return -EBADMSG;
        }
        if (copy_from_user(&bus_req_level_info, (void*)data.dataToDriver, data.sizeToDriver))
        {
           return -EFAULT;
        }

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeSetRequestLevel from ioctl for mode %d\n",
                                          bus_req_level_info.request_level);

        data.result = vmeSetRequestLevel(&bus_req_level_info);
        
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
        
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_GET_BUS_REQ_LEVEL:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_BUS_REQ_LEVEL_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_BUS_REQ_LEVEL_INFO))
        {
           return -EBADMSG;
        }

        data.result = vmeGetRequestLevel(&bus_req_level_info);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeGetRequestLevel from ioctl, mode %d\n",
                                          bus_req_level_info.request_level);
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

        if(data.result == 0) 
        {
            if(copy_to_user((void*)data.dataFromDriver, &bus_req_level_info, data.sizeFromDriver)) 
            {
                return -EFAULT;
            }
        }
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_SET_BUS_TIMEOUT:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_BUS_TIMEOUT_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_BUS_TIMEOUT_INFO))
        {
           return -EBADMSG;
        }
        if (copy_from_user(&bus_timeout_info, (void*)data.dataToDriver, data.sizeToDriver))
        {
           return -EFAULT;
        }

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeSetBusTimeout from ioctl for mode %d\n",
                                          bus_timeout_info.timeout);

        data.result = vmeSetBusTimeout(&bus_timeout_info);
        
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
        
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_GET_BUS_TIMEOUT:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_BUS_TIMEOUT_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_BUS_TIMEOUT_INFO))
        {
           return -EBADMSG;
        }

        data.result = vmeGetBusTimeout(&bus_timeout_info);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeGetBusTimeout from ioctl, mode %d\n",
                                          bus_timeout_info.timeout);
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

        if(data.result == 0) 
        {
            if(copy_to_user((void*)data.dataFromDriver, &bus_timeout_info, data.sizeFromDriver)) 
            {
                return -EFAULT;
            }
        }
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_SET_ARB_TIMEOUT:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_ARB_TIMEOUT_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_ARB_TIMEOUT_INFO))
        {
           return -EBADMSG;
        }
        if (copy_from_user(&arb_timeout_info, (void*)data.dataToDriver, data.sizeToDriver))
        {
           return -EFAULT;
        }

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeSetArbTimeout from ioctl for mode %d\n",
                                          arb_timeout_info.arb_timeout);

        data.result = vmeSetArbTimeout(&arb_timeout_info);
        
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
        
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_GET_ARB_TIMEOUT:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_ARB_TIMEOUT_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_ARB_TIMEOUT_INFO))
        {
           return -EBADMSG;
        }

        data.result = vmeGetArbTimeout(&arb_timeout_info);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeGetArbTimeout from ioctl, mode %d\n",
                                          arb_timeout_info.arb_timeout);
        printk("data.result from call: 0x%x\n",data.result);

        if(data.result == 0) 
        {
            if(copy_to_user((void*)data.dataFromDriver, &arb_timeout_info, data.sizeFromDriver)) 
            {
                return -EFAULT;
            }
        }
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_SET_BROADCAST_ID:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_BUS_BROADCAST_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_BUS_BROADCAST_INFO))
        {
           return -EBADMSG;
        }
        if (copy_from_user(&broadcast_id_info, (void*)data.dataToDriver, data.sizeToDriver))
        {
           return -EFAULT;
        }

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeSetBroadcastId from ioctl for mode %d\n",
                                          broadcast_id_info.broadcast_id);

        data.result = vmeSetBroadcastId(&broadcast_id_info);
        
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
        
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_GET_BROADCAST_ID:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_BUS_BROADCAST_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_BUS_BROADCAST_INFO))
        {
           return -EBADMSG;
        }

        data.result = vmeGetBroadcastId(&broadcast_id_info);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeGetBroadcastId from ioctl, mode %d\n",
                                          broadcast_id_info.broadcast_id);
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

        if(data.result == 0) 
        {
            if(copy_to_user((void*)data.dataFromDriver, &broadcast_id_info, data.sizeFromDriver)) 
            {
                return -EFAULT;
            }
        }
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_SET_SYSFAIL:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_SYSFAIL_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_SYSFAIL_INFO))
        {
           return -EBADMSG;
        }
        if (copy_from_user(&sysfail_info, (void*)data.dataToDriver, data.sizeToDriver))
        {
           return -EFAULT;
        }

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeSetSysFail from ioctl for mode %d\n",
                                          sysfail_info.sysfail);

        data.result = vmeSetSysFail(&sysfail_info);
        
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
        
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_GET_SYSFAIL:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_SYSFAIL_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_SYSFAIL_INFO))
        {
           return -EBADMSG;
        }

        data.result = vmeGetSysFail(&sysfail_info);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeGetSysFail from ioctl, mode %d\n",
                                          sysfail_info.sysfail);
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

        if(data.result == 0) 
        {
            if(copy_to_user((void*)data.dataFromDriver, &sysfail_info, data.sizeFromDriver)) 
            {
                return -EFAULT;
            }
        }
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_SET_MAX_RETRY:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_MAX_RETRY_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_MAX_RETRY_INFO))
        {
           return -EBADMSG;
        }
        if (copy_from_user(&max_retry_info, (void*)data.dataToDriver, data.sizeToDriver))
        {
           return -EFAULT;
        }

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeSetMaxRetry from ioctl for mode %d\n",
                                          max_retry_info.max_retry);

        data.result = vmeSetMaxRetry(&max_retry_info);
        
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
        
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_GET_MAX_RETRY:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_MAX_RETRY_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_MAX_RETRY_INFO))
        {
           return -EBADMSG;
        }

        data.result = vmeGetMaxRetry(&max_retry_info);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeGetMaxRetry from ioctl, mode %d\n",
                                          max_retry_info.max_retry);
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

        if(data.result == 0) 
        {
            if(copy_to_user((void*)data.dataFromDriver, &max_retry_info, data.sizeFromDriver)) 
            {
                return -EFAULT;
            }
        }
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_SET_POST_WRITE_COUNT:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_POST_WRITE_COUNT_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_POST_WRITE_COUNT_INFO))
        {
           return -EBADMSG;
        }
        if (copy_from_user(&pwon_info, (void*)data.dataToDriver, data.sizeToDriver))
        {
           return -EFAULT;
        }

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeSetPostedWriteCount from ioctl for mode %d\n",
                                          pwon_info.pwon);

        data.result = vmeSetPostedWriteCount(&pwon_info);
        
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
        
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_GET_POST_WRITE_COUNT:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_POST_WRITE_COUNT_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_POST_WRITE_COUNT_INFO))
        {
           return -EBADMSG;
        }

        data.result = vmeGetPostedWriteCount(&pwon_info);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeGetPostedWriteCount from ioctl, mode %d\n",
                                          pwon_info.pwon);
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

        if(data.result == 0) 
        {
            if(copy_to_user((void*)data.dataFromDriver, &pwon_info, data.sizeFromDriver)) 
            {
                return -EFAULT;
            }
        }
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_SYS_RESET:
     {

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeSetSysReset from ioctl\n");

        data.result = vmeSysReset();
        
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
        
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_ACQUIRE_BUS_OWNERSHIP:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_VOWN_INFO))
        {
           return -EBADMSG;
        }

        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_VOWN_INFO))
        {
           return -EBADMSG;
        }

        if (copy_from_user(&vownInfo, (void*)data.dataToDriver, data.sizeToDriver))
        {
           return -EFAULT;
        }

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeAcquireBusOwnership from ioctl\n");

        data.result = vmeAcquireBusOwnership(&vownInfo);
        
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
        
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_RELEASE_BUS_OWNERSHIP:
     {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeReleaseBusOwnership from ioctl\n");

        data.result = vmeReleaseBusOwnership();
        
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
        
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_QUERY_BUS_OWNERSHIP:
     {
        if (data.sizeToDriver != sizeof(GEF_BOOL))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_BOOL))
        {
           return -EBADMSG;
        }

        data.result = vmeQueryBusOwnership(&vown_info);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeQueryBusOwnership from ioctl, mode %d\n", vown_info);
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

        if(data.result == 0) 
        {
            if(copy_to_user((void*)data.dataFromDriver, &vown_info, data.sizeFromDriver)) 
            {
                return -EFAULT;
            }
        }
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_CREATE_MASTER_WINDOW:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_CREATE_MASTER_WINDOW_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_CREATE_MASTER_WINDOW_INFO))
        {
           return -EBADMSG;
        }
        if (copy_from_user(&master_info, (void*)data.dataToDriver, data.sizeToDriver))
        {
            return -EFAULT;
        }

        data.result = vmeCreateMasterWindow(&master_info);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeCreateMasterWindow from ioctl, VMEbus address 0x%x\n", master_info.addr.lower);
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmeCreateMasterWindow: master_info.master_osspec_hdl 0x%lx\n", (unsigned long int)master_info.master_osspec_hdl);
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

        if(data.result == 0)
        {
           if(copy_to_user((void*)data.dataFromDriver, &master_info, data.sizeFromDriver))
           {
              printk("COPY_TO_USER FAILED\n)");
              return -EFAULT;
           }
        }
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_READ:
         {
             if (data.sizeToDriver != sizeof(GEF_VME_DRV_READ_WRITE_INFO))
             {
                 return -EBADMSG;
             }
             if (data.sizeFromDriver != sizeof(GEF_VME_DRV_READ_WRITE_INFO))
             {
                 return -EBADMSG;
             }
             if (copy_from_user(&read_info, (void*)data.dataToDriver, data.sizeToDriver))
             {
                 return -EFAULT;
             }

             switch(read_info.width)
             {
             case GEF_VME_DATA_WIDTH_DIRECT:
                 {
                     GEF_UINT8 *pRd = (GEF_UINT8*)kmalloc(read_info.num_elements, GFP_KERNEL);
                     if (pRd == 0)
                     {
                         return -EFAULT;
                     }
                     if (copy_from_user(pRd, (void*)(GEF_OS_CAST)read_info.buffer, read_info.num_elements))
                     {
                         kfree(pRd);
                         return -EFAULT;
                     }

                     data.result = vmeRead(read_info.master_osspec_hdl, pRd, read_info. offset, read_info.num_elements);

                     if(data.result == 0)
                     {
                         if(copy_to_user((void*)(GEF_OS_CAST)read_info.buffer, pRd, read_info.num_elements))
                         {
                             kfree(pRd);
                             printk("GEF_VME_FRAMEWORK_CODE_READ: GEF_VME_DATA_WIDTH_DIRECT: COPY_TO_USER FAILED\n)");
                             return -EFAULT;
                         }
                     }

                     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeRead from ioctl\n");
                     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

                     kfree(pRd);
                 }
                 break;
             case GEF_VME_DATA_WIDTH_8:
                 {
                     GEF_UINT8 *pRd8 = (GEF_UINT8*)kmalloc(read_info.num_elements, GFP_KERNEL);
                     if (pRd8 == 0)
                     {
                         return -EFAULT;
                     }
                     if (copy_from_user(pRd8, (void*)(GEF_OS_CAST)read_info.buffer, read_info.num_elements))
                     {
                         kfree(pRd8);
                         return -EFAULT;
                     }

                     data.result = vmeRead8(read_info.master_osspec_hdl, pRd8, read_info. offset, read_info.num_elements);

                     if(data.result == 0)
                     {
                         if(copy_to_user((void*)(GEF_OS_CAST)read_info.buffer, pRd8, read_info.num_elements))
                         {
                             kfree(pRd8);
                             printk("GEF_VME_FRAMEWORK_CODE_READ: GEF_VME_DATA_WIDTH_8: COPY_TO_USER FAILED\n)");
                             return -EFAULT;
                         }
                     }

                     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeRead8 from ioctl\n");
                     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

                     kfree(pRd8);
                 }
                 break;
             case GEF_VME_DATA_WIDTH_16:
                 {
                     GEF_UINT16 *pRd16 = (GEF_UINT16*)kmalloc((read_info.num_elements*2), GFP_KERNEL);
                     if (pRd16 == 0)
                     {
                         return -EFAULT;
                     }
                     if (copy_from_user(pRd16, (void*)(GEF_OS_CAST)read_info.buffer, (read_info.num_elements*2)))
                     {
                         kfree(pRd16);
                         return -EFAULT;
                     }

                     data.result = vmeRead16(read_info.master_osspec_hdl, pRd16, read_info. offset, read_info.num_elements);

                     if(data.result == 0)
                     {
                         if(copy_to_user((void*)(GEF_OS_CAST)read_info.buffer, pRd16, read_info.num_elements*2))
                         {
                             kfree(pRd16);
                             printk("GEF_VME_FRAMEWORK_CODE_READ: GEF_VME_DATA_WIDTH_16: COPY_TO_USER FAILED\n)");
                             return -EFAULT;
                         }
                     }

                     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeRead16 from ioctl\n");
                     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

                     kfree(pRd16);
                 }
                 break;
             case GEF_VME_DATA_WIDTH_32:
                 {
                     GEF_UINT32 *pRd32 = (GEF_UINT32*)kmalloc((read_info.num_elements*4), GFP_KERNEL);
                     if (pRd32 == 0)
                     {
                         return -EFAULT;
                     }
                     if (copy_from_user(pRd32, (void*)(GEF_OS_CAST)read_info.buffer, (read_info.num_elements*4)))
                     {
                         kfree(pRd32);
                         return -EFAULT;
                     }

                     data.result = vmeRead32(read_info.master_osspec_hdl, pRd32, read_info. offset, read_info.num_elements);

                     if(data.result == 0)
                     {
                         if(copy_to_user((void*)(GEF_OS_CAST)read_info.buffer, pRd32, (read_info.num_elements*4)))
                         {
                             kfree(pRd32);
                             printk("GEF_VME_FRAMEWORK_CODE_READ: GEF_VME_DATA_WIDTH_32: COPY_TO_USER FAILED\n)");
                             return -EFAULT;
                         }
                     }

                     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeRead32 from ioctl\n");
                     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
   
                     kfree(pRd32);
                 }
                 break;
             case GEF_VME_DATA_WIDTH_64:
                 {
                     GEF_UINT64 *pRd64 = (GEF_UINT64*)kmalloc((read_info.num_elements*8), GFP_KERNEL);
                     if (pRd64 == 0)
                     {
                         return -EFAULT;
                     }
                     if (copy_from_user(pRd64, (void*)(GEF_OS_CAST)read_info.buffer, (read_info.num_elements*8)))
                     {
                         kfree(pRd64);
                         return -EFAULT;
                     }

                     data.result = vmeRead64(read_info.master_osspec_hdl, pRd64, read_info. offset, read_info.num_elements);

                     if(data.result == 0)
                     {
                         if(copy_to_user((void*)(GEF_OS_CAST)read_info.buffer, pRd64, (read_info.num_elements*8)))
                         {
                             kfree(pRd64);
                             printk("GEF_VME_FRAMEWORK_CODE_READ: GEF_VME_DATA_WIDTH_64: COPY_TO_USER FAILED\n)");
                             return -EFAULT;
                         }
                     }

                     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeRead64 from ioctl\n");
                     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

                     kfree(pRd64);
                 }
                 break;
             default: 
                 data.result = GEF_STATUS_DRIVER_ERR; 

                 GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vme_ioctl: ERROR: GEF_VME_FRAMEWORK_CODE_READ: Invalid width %d\n", read_info.width);
                 GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

                 break;
             }
             
             break;  
         }

     case GEF_VME_FRAMEWORK_CODE_WRITE:
         {
             if (data.sizeToDriver != sizeof(GEF_VME_DRV_READ_WRITE_INFO))
             {
                 return -EBADMSG;
             }
             if (data.sizeFromDriver != sizeof(GEF_VME_DRV_READ_WRITE_INFO))
             {
                 return -EBADMSG;
             }
             if (copy_from_user(&write_info, (void*)data.dataToDriver, data.sizeToDriver))
             {
                 return -EFAULT;
             }
             
             switch(write_info.width)
             {
             case GEF_VME_DATA_WIDTH_DIRECT:
                 {
                     GEF_UINT8 *pWr = (GEF_UINT8*)kmalloc(write_info.num_elements, GFP_KERNEL);
                     if (pWr == 0)
                     {
                         return -EFAULT;
                     }
                     if (copy_from_user(pWr, (void*)(GEF_OS_CAST)write_info.buffer, write_info.num_elements))
                     {
                         return -EFAULT;
                     }
                     
                     data.result = vmeWrite(write_info.master_osspec_hdl, pWr, write_info.offset, write_info.num_elements);

                     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeWrite from ioctl\n");
                     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

                     kfree(pWr);
                 }
                 break;
             case GEF_VME_DATA_WIDTH_8:
                 {
                     GEF_UINT8 *pWr8 = (GEF_UINT8*)kmalloc(write_info.num_elements, GFP_KERNEL);
                     if (pWr8 == 0)
                     {
                         return -EFAULT;
                     }
                     if (copy_from_user(pWr8, (void*)(GEF_OS_CAST)write_info.buffer, write_info.num_elements))
                     {
                         return -EFAULT;
                     }
                     
                     data.result = vmeWrite8(write_info.master_osspec_hdl, pWr8, write_info.offset, write_info.num_elements);

                     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeWrite8 from ioctl\n");
                     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

                     kfree(pWr8);
                 }
                 break;
             case GEF_VME_DATA_WIDTH_16:
                 {
                     GEF_UINT16 *pWr16 = (GEF_UINT16*)kmalloc((write_info.num_elements*2), GFP_KERNEL);
                     if (pWr16 == 0)
                     {
                         return -EFAULT;
                     }
                     if (copy_from_user(pWr16, (void*)(GEF_OS_CAST)write_info.buffer, (write_info.num_elements*2)))
                     {
                         return -EFAULT;
                     }
                     
                     data.result = vmeWrite16(write_info.master_osspec_hdl, pWr16, write_info.offset, write_info.num_elements);

                     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeRead16 from ioctl\n");
                     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

                     kfree(pWr16);
                 }
                 break;
             case GEF_VME_DATA_WIDTH_32:
                 {
                     GEF_UINT32 *pWr32 = (GEF_UINT32*)kmalloc((write_info.num_elements*4), GFP_KERNEL);
                     if (pWr32 == 0)
                     {
                         return -EFAULT;
                     }
                     if (copy_from_user(pWr32, (void*)(GEF_OS_CAST)write_info.buffer, (write_info.num_elements*4)))
                     {
                         return -EFAULT;
                     }
                     
                     data.result = vmeWrite32(write_info.master_osspec_hdl, pWr32, write_info.offset, write_info.num_elements); 

                     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeWrite32 from ioctl\n");
                     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
       
                     kfree(pWr32);
                 }
                 break;
             case GEF_VME_DATA_WIDTH_64:
                 {
                     GEF_UINT64 *pWr64 = (GEF_UINT64*)kmalloc((write_info.num_elements*8), GFP_KERNEL);
                     if (pWr64 == 0)
                     {
                         return -EFAULT;
                     }
                     if (copy_from_user(pWr64, (void*)(GEF_OS_CAST)write_info.buffer, (write_info.num_elements*8)))
                     {
                         return -EFAULT;
                     }
                     
                     data.result = vmeWrite64(write_info.master_osspec_hdl, pWr64, write_info.offset, write_info.num_elements);

                     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeWrite64 from ioctl\n");
                     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

                     kfree(pWr64);
                 }
                 break;
             default: 
                 data.result = GEF_STATUS_DRIVER_ERR; 

                 GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vme_ioctl: ERROR: GEF_VME_FRAMEWORK_CODE_WRITE: Invalid width %d\n", read_info.width);
                 GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

                 break;
             }
             
             break;  
         }

/**************************************
 * VME Probe routines
 **************************************/
	case GEF_VME_FRAMEWORK_CODE_PEEK:
	{
		GEF_VME_MASTER_OSSPEC_HDL	hMstr;
		GEF_VME_ADDR				*pVmeAddr;
		size_t						Size;
		void						*pBlob = NULL;
		int							RetStat = 0;

		do
		{
			if (data.sizeToDriver != sizeof(GEF_VME_DRV_READ_WRITE_INFO))
			{
				RetStat = -EBADMSG;
				break;
			}

			if (data.sizeFromDriver != sizeof(GEF_VME_DRV_READ_WRITE_INFO))
			{
				RetStat = -EBADMSG;
				break;
			}

			if (copy_from_user(	&peek_info,
								(void *)(data.dataToDriver),
								data.sizeToDriver	)	)
			{
				RetStat = -EFAULT;
				break;
			}

			/* Calculate the size of the blob */
			Size = peek_info.num_elements * (size_t)(peek_info.width);

			/* Allocate the blob */
			if ((pBlob = kmalloc(Size, GFP_KERNEL)) == NULL)
			{
				RetStat = -EFAULT;
				break;
			}

			/* Copy the user's data into the blob */
			if (copy_from_user(	pBlob,
								(void *)((GEF_OS_CAST)(peek_info.buffer)),
								Size	) != 0)
			{
				RetStat = -EFAULT;
				break;
			}

			/*************************************************************
			*	We need to set up the BERR handler info, get the master
			*	handle and the vme address info out of there.
			*************************************************************/
			hMstr = peek_info.master_osspec_hdl;
			pVmeAddr = &(((vmeMasterHandle_t *)
								((GEF_OS_CAST)(hMstr)))->window->vmeAddr);

			/* Set up the probe info based on that */
			s_bPrbVmeBerr		= GEF_FALSE;
			s_PrbVmeAddrHi		= pVmeAddr->upper;
			s_PrbVmeAddrLo		= pVmeAddr->lower + peek_info.offset;
			s_PrbVmeAddrMode	= pVmeAddr->addr_mode;

			/* Override the default BERR handler */
			g_pProbeBERRFunc = ProbeBERRFunc;

               GEF_VME_DEBUG(GEF_DEBUG_TRACE,"peek_info.width = 0x%x\n", peek_info.width);

			switch (peek_info.width)
			{
				case GEF_VME_DWIDTH_D8:
				{
					/* Read the data as bytes */
					data.result = vmeRead8(	hMstr,
											(GEF_UINT8 *)pBlob,
											peek_info.offset,
											Size	);

					GEF_VME_DEBUG(GEF_DEBUG_TRACE,"Called vmeRead8 from ioctl\n");
					GEF_VME_DEBUG(GEF_DEBUG_TRACE,"data.result from call: 0x%x\n", data.result);

					break;
				}
				case GEF_VME_DWIDTH_D16:
				{
					/* Read the data as words */
					data.result = vmeRead16(	hMstr,
												(GEF_UINT16 *)pBlob,
												peek_info.offset,
												Size	);

					GEF_VME_DEBUG(GEF_DEBUG_TRACE,"Called vmeRead16 from ioctl\n");
					GEF_VME_DEBUG(GEF_DEBUG_TRACE,"data.result from call: 0x%x\n", data.result);

					break;
				}
				case GEF_VME_DWIDTH_D32:
				{
					/* Read the data as long words */
					data.result = vmeRead32(	hMstr,
												(GEF_UINT32 *)pBlob,
												peek_info.offset,
												Size	);

					GEF_VME_DEBUG(GEF_DEBUG_TRACE,"Called vmeRead32 from ioctl\n");
					GEF_VME_DEBUG(GEF_DEBUG_TRACE,"data.result from call: 0x%x\n", data.result);

					break;
				}
				case GEF_VME_DWIDTH_D64:
				{
					/* Read the data as double long words */
					data.result = vmeRead64(	hMstr,
												(GEF_UINT64 *)pBlob,
												peek_info.offset,
												Size	);

					GEF_VME_DEBUG(GEF_DEBUG_TRACE,"Called vmeRead64 from ioctl\n");
					GEF_VME_DEBUG(GEF_DEBUG_TRACE,"data.result from call: 0x%x\n",data.result);

					break;
				}
				default: 
				{
					data.result = GEF_STATUS_DRIVER_ERR; 

					GEF_VME_DEBUG(GEF_DEBUG_TRACE,"vme_ioctl: ERROR: GEF_VME_FRAMEWORK_CODE_READ: Invalid width %d\n", peek_info.width);
					GEF_VME_DEBUG(GEF_DEBUG_TRACE,"data.result from call: 0x%x\n",data.result);

					break;
				}
			}

               mdelay(2);

			/* See if there was a BERR */
			if (s_bPrbVmeBerr == GEF_TRUE)
			{
                    GEF_VME_DEBUG(GEF_DEBUG_TRACE,"Peek BERR\n");
				/* If so, return a read error */
				data.result = GEF_STATUS_READ_ERR;
				break;
			}
               else
               {
                    GEF_VME_DEBUG(GEF_DEBUG_TRACE,"No Peek BERR\n");
               }

			/* If the read passed */
			if (data.result == 0)
			{
				/* Copy the info back to the user */
				if (copy_to_user(	(void*)((GEF_OS_CAST)(peek_info.buffer)),
									pBlob, Size ) != 0)
				{
					printk("GEF_VME_FRAMEWORK_CODE_READ: DATA_WIDTH = %d: COPY_TO_USER FAILED\n)", (size_t)(peek_info.width) );
					RetStat = -EFAULT;
					break;
				}
			}
		}
		while (0);

		/* If the blob was allocated */
		if (pBlob != NULL)
		{
			/* Free it */
			kfree(pBlob);
		}

		/* If there was an error */
		if (RetStat != 0)
		{
			/* Return it */
			return(RetStat);
		}

		/* Break out of outer case */
		break;
	}

	case GEF_VME_FRAMEWORK_CODE_POKE:
	{
		GEF_VME_MASTER_OSSPEC_HDL	hMstr;
		GEF_VME_ADDR				*pVmeAddr;
		size_t						Size;
		void						*pBlob = NULL;
		int							RetStat = 0;

		do
		{
			if (data.sizeToDriver != sizeof(GEF_VME_DRV_READ_WRITE_INFO))
			{
				RetStat = -EBADMSG;
				break;
			}

			if (data.sizeFromDriver != sizeof(GEF_VME_DRV_READ_WRITE_INFO))
			{
				RetStat = -EBADMSG;
				break;
			}

			if (copy_from_user(	&poke_info,
								(void *)(data.dataToDriver),
								data.sizeToDriver	)	)
			{
				RetStat = -EFAULT;
				break;
			}

			/* Calculate the size of the blob */
			Size = poke_info.num_elements * (size_t)(poke_info.width);

			/* Allocate the blob */
			if ((pBlob = kmalloc(Size, GFP_KERNEL)) == NULL)
			{
				RetStat = -EFAULT;
				break;
			}

			/* Copy the user's data into the blob */
			if (copy_from_user(	pBlob,
								(void *)((GEF_OS_CAST)(poke_info.buffer)),
								Size	) != 0)
			{
				RetStat = -EFAULT;
				break;
			}

			/*************************************************************
			*	We need to set up the BERR handler info, get the master
			*	handle and the vme address info out of there.
			*************************************************************/
			hMstr = poke_info.master_osspec_hdl;
			pVmeAddr = &(((vmeMasterHandle_t *)
								((GEF_OS_CAST)(hMstr)))->window->vmeAddr);

			/* Set up the probe info based on that */
			s_bPrbVmeBerr		= GEF_FALSE;
			s_PrbVmeAddrHi		= pVmeAddr->upper;
			s_PrbVmeAddrLo		= pVmeAddr->lower;
			s_PrbVmeAddrMode	= pVmeAddr->addr_mode;

			/* Override the default BERR handler */
			g_pProbeBERRFunc = ProbeBERRFunc;

			switch (poke_info.width)
			{
				case GEF_VME_DWIDTH_D8:
				{
					/* Write the data as bytes */
					data.result = vmeWrite8(	hMstr,
												(GEF_UINT8 *)pBlob,
												poke_info.offset,
												Size	);
					vmeRead8(hMstr, (GEF_UINT8 *)pBlob, poke_info.offset, Size);

					GEF_VME_DEBUG(GEF_DEBUG_TRACE,"Called vmeWrite8 from ioctl\n");
					GEF_VME_DEBUG(GEF_DEBUG_TRACE,"data.result from call: 0x%x\n",data.result);

					break;
				}
				case GEF_VME_DWIDTH_D16:
				{
					/* Write the data as words */
					data.result = vmeWrite16(	hMstr,
												(GEF_UINT16 *)pBlob,
												poke_info.offset,
												Size	);
					vmeRead16(	hMstr,
								(GEF_UINT16 *)pBlob,
								poke_info.offset,
								Size	);

					GEF_VME_DEBUG(GEF_DEBUG_TRACE,"Called vmeRead16 from ioctl\n");
					GEF_VME_DEBUG(GEF_DEBUG_TRACE,"data.result from call: 0x%x\n",data.result);

					break;
				}
				case GEF_VME_DWIDTH_D32:
				{
					/* Write the data as long words */
					data.result = vmeWrite32(	hMstr,
												(GEF_UINT32 *)pBlob,
												poke_info.offset,
												Size	);

					vmeRead32(	hMstr,
								(GEF_UINT32 *)pBlob,
								poke_info.offset,
								Size	);

					GEF_VME_DEBUG(GEF_DEBUG_TRACE,"Called vmeWrite32 from ioctl\n");
					GEF_VME_DEBUG(GEF_DEBUG_TRACE,"data.result from call: 0x%x\n",data.result);

					break;
				}
				case GEF_VME_DWIDTH_D64:
				{
					/* Write the data as double long words */
					data.result = vmeWrite64(	hMstr,
												(GEF_UINT64 *)pBlob,
												poke_info.offset,
												Size	);

					vmeRead64(	hMstr,
								(GEF_UINT64 *)pBlob,
								poke_info.offset,
								Size	);

					GEF_VME_DEBUG(GEF_DEBUG_TRACE,"Called vmeWrite64 from ioctl\n");
					GEF_VME_DEBUG(GEF_DEBUG_TRACE,"data.result from call: 0x%x\n",data.result);

					break;
				}
				default: 
				{
					data.result = GEF_STATUS_DRIVER_ERR; 

					GEF_VME_DEBUG(GEF_DEBUG_TRACE,"vme_ioctl: ERROR: GEF_VME_FRAMEWORK_CODE_WRITE: Invalid width %d\n", read_info.width);
					GEF_VME_DEBUG(GEF_DEBUG_TRACE,"data.result from call: 0x%x\n",data.result);

					break;
				}
			}

               mdelay(2);

			/* See if there was a BERR */
			if (s_bPrbVmeBerr == GEF_TRUE)
			{
                    GEF_VME_DEBUG(GEF_DEBUG_TRACE,"Poke BERR\n");
				/* If so, return a write error */
				data.result = GEF_STATUS_WRITE_ERR;
				break;
			}
               else
               {
                    GEF_VME_DEBUG(GEF_DEBUG_TRACE,"No Poke BERR\n");
               }
		}
		while (0);

		/* If the blob was allocated */
		if (pBlob != NULL)
		{
			/* Free it */
			kfree(pBlob);
		}

		/* If there was an error */
		if (RetStat != 0)
		{
			/* Return it */
			return(RetStat);
		}

		/* Break out of outer case */
		break;
	}
/* VME Probe routines end */

     case GEF_VME_FRAMEWORK_CODE_RELEASE_MASTER_WINDOW:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_RELEASE_MASTER_WINDOW_HDL_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_RELEASE_MASTER_WINDOW_HDL_INFO))
        {
           return -EBADMSG;
        }
        if (copy_from_user(&rel_master_info, (void*)data.dataToDriver, data.sizeToDriver))
        {
            return -EFAULT;
        }

        data.result = vmeReleaseMasterWindow(&rel_master_info);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeReleaseMasterWindow from ioctl\n");
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_READ_MODIFY_WRITE:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_READ_MODIFY_WRITE_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_READ_MODIFY_WRITE_INFO))
        {
           return -EBADMSG;
        }
        if (copy_from_user(&read_modify_write_info, (void*)data.dataToDriver, data.sizeToDriver))
        {
            return -EFAULT;
        }

        data.result = vmeReadModifyWrite(&read_modify_write_info);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeReadModifyWrite from ioctl\n");
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_CREATE_SLAVE_WINDOW:
         {
             if (data.sizeToDriver != sizeof(GEF_VME_DRV_CREATE_SLAVE_WINDOW_INFO))
             {
                 return -EBADMSG;
             }
             if (data.sizeFromDriver != sizeof(GEF_VME_DRV_CREATE_SLAVE_WINDOW_INFO))
             {
                 return -EBADMSG;
             }
             if (copy_from_user(&slave_info, (void*)data.dataToDriver, data.sizeToDriver))
             {
                 return -EFAULT;
             }
             
             data.result = vmeCreateSlaveWindow(&slave_info);
             
             GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeCreateSlaveWindow from ioctl, VMEbus address 0x%x\n", slave_info.addr.lower);
             GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
            
             if(data.result == 0)
             {
                 if(copy_to_user((void*)data.dataFromDriver, &slave_info, data.sizeFromDriver))
                 {
                     return -EFAULT;
                 }
             }
             
             break;  
         }

     case GEF_VME_FRAMEWORK_CODE_REMOVE_SLAVE_WINDOW:
         {
             if (data.sizeToDriver != sizeof(GEF_VME_DRV_RELEASE_SLAVE_WINDOW_HDL_INFO))
             {
                 return -EBADMSG;
             }
             if (data.sizeFromDriver != sizeof(GEF_VME_DRV_RELEASE_SLAVE_WINDOW_HDL_INFO))
             {
                 return -EBADMSG;
             }
             if (copy_from_user(&rel_slave_info, (void*)data.dataToDriver, data.sizeToDriver))
             {
                 return -EFAULT;
             }
             
             data.result = vmeRemoveSlaveWindow(&rel_slave_info);
             
             GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeRemoveSlaveWindow from ioctl\n");
             GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
             
             break;  
         }

     case GEF_VME_FRAMEWORK_CODE_WRITE_DMA:
     {
#ifdef SCATTER_GATHER_DMA_SUPPORTED
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_READ_WRITE_DMA_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_READ_WRITE_DMA_INFO))
        {
           return -EBADMSG;
        }
        if (copy_from_user(&write_dma_info, (void*)data.dataToDriver, data.sizeToDriver))
        {
            return -EFAULT;
        }

        data.result = vmeStartWriteDma(&write_dma_info);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeStartWriteDma from ioctl\n");
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

#else
        data.result = GEF_STATUS_NOT_SUPPORTED;
#endif
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_READ_DMA:
         {
             data.result = GEF_STATUS_NOT_SUPPORTED;
             break;
         }
         
     case GEF_VME_FRAMEWORK_CODE_ALLOC_DMA_BUF:
         {
             if (data.sizeToDriver != sizeof(GEF_VME_DRV_ALLOC_DMA_BUF_INFO))
             {
                 return -EBADMSG;
             }
             if (data.sizeFromDriver != sizeof(GEF_VME_DRV_ALLOC_DMA_BUF_INFO))
             {
                 return -EBADMSG;
             }
             if (copy_from_user(&alloc_dma_buf_info, (void*)data.dataToDriver, data.sizeToDriver))
             {
                 return -EFAULT;
             }
             
             data.result = vmeAllocDmaBuf(&alloc_dma_buf_info);
             
             GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeAllocDmaBuf from ioctl\n");
             GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

             if(data.result == 0) 
             {
                 if(copy_to_user((void*)data.dataFromDriver, &alloc_dma_buf_info, data.sizeFromDriver)) 
                 {
                     return -EFAULT;
                 }
             }
             break;
         }

     case GEF_VME_FRAMEWORK_CODE_FREE_DMA_BUF:
         {
             if (data.sizeToDriver != sizeof(GEF_VME_DRV_FREE_DMA_BUF_INFO))
             {
                 return -EBADMSG;
             }
             if (data.sizeFromDriver != sizeof(GEF_VME_DRV_FREE_DMA_BUF_INFO))
             {
                 return -EBADMSG;
             }
             if (copy_from_user(&free_dma_buf_info, (void*)data.dataToDriver, data.sizeToDriver))
             {
                 return -EFAULT;
             }
             
             data.result = vmeFreeDmaBuf(&free_dma_buf_info);
             
             GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeFreeDmaBuf from ioctl\n");
             GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

             if(data.result == 0) 
             {
                 if(copy_to_user((void*)data.dataFromDriver, &free_dma_buf_info, data.sizeFromDriver)) 
                 {
                     return -EFAULT;
                 }
             }
             break; 
         }

     case GEF_VME_FRAMEWORK_CODE_WRITE_DMA_BUF:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_READ_WRITE_DMA_BUF_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_READ_WRITE_DMA_BUF_INFO))
        {
           return -EBADMSG;
        }
        if (copy_from_user(&write_dma_buf_info, (void*)data.dataToDriver, data.sizeToDriver))
        {
            return -EFAULT;
        }

        data.result = vmeStartWriteDmaBuf(&write_dma_buf_info);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeStartWriteDmaBuf from ioctl\n");
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

        break;  
     }   
     
     case GEF_VME_FRAMEWORK_CODE_READ_DMA_BUF:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_READ_WRITE_DMA_BUF_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_READ_WRITE_DMA_BUF_INFO))
        {
           return -EBADMSG;
        }
        if (copy_from_user(&read_dma_buf_info, (void*)data.dataToDriver, data.sizeToDriver))
        {
            return -EFAULT;
        }

        data.result = vmeStartReadDmaBuf(&read_dma_buf_info);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeStartReadDmaBuf from ioctl\n");
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

        break;  
     }                  
         
     case GEF_VME_FRAMEWORK_CODE_SET_DEBUG_FLAGS:
         {
             if (data.sizeToDriver != sizeof(GEF_UINT32))
             {
                 return -EBADMSG;
             }
             if (data.sizeFromDriver != sizeof(GEF_UINT32))
             {
                 return -EBADMSG;
             }
             if (copy_from_user(&flag_info, (void*)data.dataToDriver, data.sizeToDriver))
             {
                 return -EFAULT;
             }
             
             GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeSetDebugFlags from ioctl for flag %d\n",
                 flag_info);

             data.result = vmeSetDebugFlags(&flag_info);
             
             GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
             break;
         }

     case GEF_VME_FRAMEWORK_CODE_GET_DEBUG_FLAGS:
         {
             if (data.sizeToDriver != sizeof(GEF_UINT32))
             {
                 return -EBADMSG;
             }
             if (data.sizeFromDriver != sizeof(GEF_UINT32))
             {
                 return -EBADMSG;
             }
             
             GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeGetDebugFlags from ioctl\n");
             data.result = vmeGetDebugFlags(&flag_info);

             if(data.result == 0)
             {
                 if(copy_to_user((void*)data.dataFromDriver, &flag_info, data.sizeFromDriver))
                 {
                     printk("COPY_TO_USER FAILED\n)");
                     return -EFAULT;
                 }
             }
             
             GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

             break;
         }

     case GEF_VME_FRAMEWORK_CODE_MAP_MASTER:
     {
        GEF_VME_MAP_MASTER_INFO map_info;

        if (data.sizeToDriver != sizeof(GEF_VME_MAP_MASTER_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_MAP_MASTER_INFO))
        {
           return -EBADMSG;
        }
        if (copy_from_user(&map_info, (void*)data.dataToDriver, data.sizeToDriver))
        {
           return -EFAULT;
        }

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeMapMasterWindow from ioctl\n");

        data.result = vmeMapMasterWindow(&map_info);

        if(data.result == 0)
        {
           data.sizeFromDriver = data.sizeToDriver;
           if(copy_to_user((void*)data.dataFromDriver, &map_info, data.sizeFromDriver))
           {
              return -EFAULT;
           }
        }

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

        break;
     }

     case GEF_VME_FRAMEWORK_CODE_MAP_SLAVE:
     {
        GEF_VME_MAP_SLAVE_INFO map_info;

        if (data.sizeToDriver != sizeof(GEF_VME_MAP_SLAVE_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_MAP_SLAVE_INFO))
        {
           return -EBADMSG;
        }
        if (copy_from_user(&map_info, (void*)data.dataToDriver, data.sizeToDriver))
        {
           return -EFAULT;
        }

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeMapSlaveWindow from ioctl\n");

        data.result = vmeMapSlaveWindow(&map_info);

        if(data.result == 0)
        {
           data.sizeFromDriver = data.sizeToDriver;
           if(copy_to_user((void*)data.dataFromDriver, &map_info, data.sizeFromDriver))
           {
              printk("COPY_TO_USER FAILED\n)");
              return -EFAULT;
           }
        }

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

        break;
     }

     case GEF_VME_FRAMEWORK_CODE_CREATE_LOCATION_MON:
         {
             GEF_VME_DRV_LOCATION_MONITOR_INFO	lmInfo;
             
             if (data.sizeToDriver != sizeof(GEF_VME_DRV_LOCATION_MONITOR_INFO))
             {
                 return -EBADMSG;
             }
             if (data.sizeFromDriver != sizeof(GEF_VME_DRV_LOCATION_MONITOR_INFO))
             {
                 return -EBADMSG;
             }
             if (copy_from_user(&lmInfo, (void*)data.dataToDriver, data.sizeToDriver))
             {
                 return -EFAULT;
             }
             
             data.result = vmeCreateLocMonWindow(&lmInfo);
             
             GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeCreateLocMonWindow from ioctl, VMEbus address 0x%x\n", lmInfo.addr.lower);
             GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

             break;  
         }
         
     case GEF_VME_FRAMEWORK_CODE_RELEASE_LOCATION_MON:
         {
             GEF_VME_DRV_LOCATION_MONITOR_INFO	lmInfo;
             
             if (data.sizeToDriver != sizeof(GEF_VME_DRV_LOCATION_MONITOR_INFO))
             {
                 return -EBADMSG;
             }
             if (data.sizeFromDriver != sizeof(GEF_VME_DRV_LOCATION_MONITOR_INFO))
             {
                 return -EBADMSG;
             }
             if (copy_from_user(&lmInfo, (void*)data.dataToDriver, data.sizeToDriver))
             {
                 return -EFAULT;
             }
             
             data.result = vmeReleaseLocMonWindow(&lmInfo);
             
             GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeReleaseLocMonWindow from ioctl, VMEbus address 0x%x\n", lmInfo.addr.lower);
             GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

             break;  
         }

     case GEF_VME_FRAMEWORK_CODE_QUERY_LOCATION_MON:
         {
             GEF_VME_DRV_LOCATION_MONITOR_INFO	lmInfo;
             
             if (data.sizeToDriver != sizeof(GEF_VME_DRV_LOCATION_MONITOR_INFO))
             {
                 return -EBADMSG;
             }
             if (data.sizeFromDriver != sizeof(GEF_VME_DRV_LOCATION_MONITOR_INFO))
             {
                 return -EBADMSG;
             }
             if (copy_from_user(&lmInfo, (void*)data.dataToDriver, data.sizeToDriver))
             {
                 return -EFAULT;
             }
             
             data.result = vmeQueryLocMonWindow(&lmInfo);
             
             GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeQueryLocMonWindow from ioctl, VMEbus address 0x%x\n", lmInfo.addr.lower);
             GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
             
             if(data.result == 0)
             {
                 if(copy_to_user((void*)data.dataFromDriver, &lmInfo, data.sizeFromDriver))
                 {
                     printk("COPY_TO_USER FAILED\n)");
                     return -EFAULT;
                 }
             }
             break;  
         }

// VRAI functions
    case GEF_VME_FRAMEWORK_CODE_CREATE_VRAI:
    {
        GEF_VME_DRV_VRAI_INFO vraiInfo;

        if (data.sizeToDriver != sizeof(GEF_VME_DRV_VRAI_INFO))
        {
            return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_VRAI_INFO))
        {
            return -EBADMSG;
        }
        if (copy_from_user(&vraiInfo, (void*)data.dataToDriver, data.sizeToDriver))
        {
            return -EFAULT;
        }

        data.result = vmeCreateVraiWindow(&vraiInfo);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeCreateVraiWindow from ioctl, VMEbus address 0x%x\n", vraiInfo.addr.lower);
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

        break;
     }

     case GEF_VME_FRAMEWORK_CODE_REMOVE_VRAI:
     {
          GEF_VME_DRV_VRAI_INFO    vraiInfo;

          if (data.sizeToDriver != sizeof(GEF_VME_DRV_VRAI_INFO))
          {
              return -EBADMSG;
          }
          if (data.sizeFromDriver != sizeof(GEF_VME_DRV_VRAI_INFO))
          {
              return -EBADMSG;
          }
          if (copy_from_user(&vraiInfo, (void*)data.dataToDriver, data.sizeToDriver))
          {
            return -EFAULT;
          }

          data.result = vmeReleaseVraiWindow(&vraiInfo);

          GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeReleaseVraiWindow from ioctl, VMEbus address 0x%x\n", vraiInfo.addr.lower);
          GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

          break;
     }

     case GEF_VME_FRAMEWORK_CODE_QUERY_VRAI:
     {
          GEF_VME_DRV_VRAI_INFO    vraiInfo;

          if (data.sizeToDriver != sizeof(GEF_VME_DRV_VRAI_INFO))
          {
              return -EBADMSG;
          }
          if (data.sizeFromDriver != sizeof(GEF_VME_DRV_VRAI_INFO))
          {
              return -EBADMSG;
          }
          if (copy_from_user(&vraiInfo, (void*)data.dataToDriver, data.sizeToDriver))
          {
              return -EFAULT;
          }

          data.result = vmeQueryVraiWindow(&vraiInfo);

          GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeQueryVraiWindow from ioctl, VMEbus address 0x%x\n", vraiInfo.addr.lower);
          GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

          if(data.result == 0)
          {
              if(copy_to_user((void*)data.dataFromDriver, &vraiInfo, data.sizeFromDriver))
              {
                  return -EFAULT;
              }
          }
          break;
     }
// End VRAI functions

     case GEF_VME_FRAMEWORK_CODE_TRANSLATE_MASTER_WINDOW:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_TRANSLATE_MASTER_WINDOW_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_TRANSLATE_MASTER_WINDOW_INFO))
        {
           return -EBADMSG;
        }
        if (copy_from_user(&transMasterInfo, (void*)data.dataToDriver, data.sizeToDriver))
        {
            return -EFAULT;
        }

        data.result = vmeTransMasterWindow(&transMasterInfo);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeTransMasterWindow from ioctl, VMEbus address 0x%x\n", transMasterInfo.addr.lower);
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_SET_RD_WR_BYTESWAP:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_BYTESWAP_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_BYTESWAP_INFO))
        {
           return -EBADMSG;
        }
        if (copy_from_user(&swbyteswapInfo, (void*)data.dataToDriver, data.sizeToDriver))
        {
           return -EFAULT;
        }

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeSetSwByteSwap from ioctl with endian mode 0x%x\n", swbyteswapInfo.endian);

        data.result = vmeSetSwByteSwap(&swbyteswapInfo);
        
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
        
        break;
     }

     case GEF_VME_FRAMEWORK_CODE_GET_RD_WR_BYTESWAP:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_DRV_BYTESWAP_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_DRV_BYTESWAP_INFO))
        {
           return -EBADMSG;
        }

        data.result = vmeGetSwByteSwap(&swbyteswapInfo);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeGetSwByteSwap from ioctl and returned endian=0x%x\n",
                                          swbyteswapInfo.endian);
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
 
        if(data.result == 0) 
        {
            if(copy_to_user((void*)data.dataFromDriver, &swbyteswapInfo, data.sizeFromDriver)) 
            {
                return -EFAULT;
            }
        }
        break;
     }

     case GEF_VME_FRAMEWORK_CODE_READ_REG:
     {
         if (data.sizeToDriver != sizeof(GEF_VME_DRV_READ_WRITE_REG_INFO))
         {
             return -EBADMSG;
         }
         if (data.sizeFromDriver != sizeof(GEF_VME_DRV_READ_WRITE_REG_INFO))
         {
             return -EBADMSG;
         }
         if (copy_from_user(&regInfo, (void*)data.dataToDriver, data.sizeToDriver))
         {
             return -EFAULT;
         }
         
         data.result = vmeRegRead(&regInfo);
         
         if(data.result == 0)
         {
             if(copy_to_user((void*)data.dataFromDriver, &regInfo, data.sizeFromDriver))
             {
                 return -EFAULT;
             }
         }
         
         break;  

     }

     case GEF_VME_FRAMEWORK_CODE_GET_DRV_VERSION:
     {
         if (data.sizeToDriver != sizeof(GEF_VME_DRV_VERSION_INFO))
         {
             return -EBADMSG;
         }
         if (data.sizeFromDriver != sizeof(GEF_VME_DRV_VERSION_INFO))
         {
             return -EBADMSG;
         }
         if (copy_from_user(&versionInfo, (void*)data.dataToDriver, data.sizeToDriver))
         {
             return -EFAULT;
         }
         
         data.result = vmeGetVmeDriverVersion(&versionInfo);
         
         if(data.result == 0)
         {
             if(copy_to_user((void*)data.dataFromDriver, &versionInfo, data.sizeFromDriver))
             {
                 return -EFAULT;
             }
         }
         
         break;  

     }

// Master and Slave hardware byte swap routines
     case GEF_VME_FRAMEWORK_CODE_SET_MASTER_HW_BYTESWAP:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_HW_BYTESWAP_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_HW_BYTESWAP_INFO))
        {
           return -EBADMSG;
        }
        if (copy_from_user(&byteswapInfo, (void*)data.dataToDriver, data.sizeToDriver))
        {
           return -EFAULT;
        }

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeSetMasterHWByteswap from ioctl with byte swap value 0x%x\n",
                                          byteswapInfo.byteswap);

        data.result = vmeSetMasterHWByteswap(&byteswapInfo);
        
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
        
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_GET_MASTER_HW_BYTESWAP:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_HW_BYTESWAP_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_HW_BYTESWAP_INFO))
        {
           return -EBADMSG;
        }

        data.result = vmeGetMasterHWByteswap(&byteswapInfo);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeGetMasterHWByteswap from ioctl with byte swap value 0x%x\n",
                                          byteswapInfo.byteswap);
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

        if(data.result == 0) 
        {
            if(copy_to_user((void*)data.dataFromDriver, &byteswapInfo, data.sizeFromDriver)) 
            {
                return -EFAULT;
            }
        }
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_SET_SLAVE_HW_BYTESWAP:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_HW_BYTESWAP_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_HW_BYTESWAP_INFO))
        {
           return -EBADMSG;
        }
        if (copy_from_user(&byteswapInfo, (void*)data.dataToDriver, data.sizeToDriver))
        {
           return -EFAULT;
        }

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeSetSlaveHWByteswap from ioctl with byte swap value 0x%x\n",
                                          byteswapInfo.byteswap);

        data.result = vmeSetSlaveHWByteswap(&byteswapInfo);
        
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
        
        break;  
     }

     case GEF_VME_FRAMEWORK_CODE_GET_SLAVE_HW_BYTESWAP:
     {
        if (data.sizeToDriver != sizeof(GEF_VME_HW_BYTESWAP_INFO))
        {
           return -EBADMSG;
        }
        if (data.sizeFromDriver != sizeof(GEF_VME_HW_BYTESWAP_INFO))
        {
           return -EBADMSG;
        }

        data.result = vmeGetSlaveHWByteswap(&byteswapInfo);

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeGetSlaveHWByteswap from ioctl with byte swap value 0x%x\n",
                                          byteswapInfo.byteswap);
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);

        if(data.result == 0) 
        {
            if(copy_to_user((void*)data.dataFromDriver, &byteswapInfo, data.sizeFromDriver)) 
            {
                return -EFAULT;
            }
        }
        break;  
     }
// End of Master and Slave hardware byte swap routines

// Time On / Off Bus routines     
     case GEF_VME_FRAMEWORK_CODE_SET_TIME_ON_BUS:
     {
         if (data.sizeToDriver != sizeof(GEF_VME_DRV_TIME_ON_BUS_INFO))
         {
             return -EBADMSG;
         }
         if (data.sizeFromDriver != sizeof(GEF_VME_DRV_TIME_ON_BUS_INFO))
         {
             return -EBADMSG;
         }
         if (copy_from_user(&time_on_bus_info, (void*)data.dataToDriver, data.sizeToDriver))
         {
             return -EFAULT;
         }
             
         GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeSetTimeOnBus from ioctl with time on value 0x%x\n",
             time_on_bus_info.time_on);
             
         data.result = vmeSetTimeOnBus(&time_on_bus_info);
             
         GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
             
         break;  
     }
         
     case GEF_VME_FRAMEWORK_CODE_GET_TIME_ON_BUS:
     {
         if (data.sizeToDriver != sizeof(GEF_VME_DRV_TIME_ON_BUS_INFO))
         {
             return -EBADMSG;
         }
         if (data.sizeFromDriver != sizeof(GEF_VME_DRV_TIME_ON_BUS_INFO))
         {
             return -EBADMSG;
         }
             
         data.result = vmeGetTimeOnBus(&time_on_bus_info);
             
         GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeGetTimeOnBus from ioctl\n");
         GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
             
         if(data.result == 0) 
         {
             if(copy_to_user((void*)data.dataFromDriver, &time_on_bus_info, data.sizeFromDriver)) 
             {
                 return -EFAULT;
             }
         }
         break;  
     }
         
     case GEF_VME_FRAMEWORK_CODE_GET_TIME_OFF_BUS:
     {
         if (data.sizeToDriver != sizeof(GEF_VME_DRV_TIME_OFF_BUS_INFO))
         {
             return -EBADMSG;
         }
         if (data.sizeFromDriver != sizeof(GEF_VME_DRV_TIME_OFF_BUS_INFO))
         {
             return -EBADMSG;
         }
             
         data.result = vmeGetTimeOffBus(&time_off_bus_info);
             
         GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Called vmeGetTimeOffBus from ioctl\n");
         GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
             
         if(data.result == 0) 
         {
             if(copy_to_user((void*)data.dataFromDriver, &time_off_bus_info, data.sizeFromDriver)) 
             {
                 return -EFAULT;
             }
         }
         break;  
     }
         
     case GEF_VME_FRAMEWORK_CODE_SET_TIME_OFF_BUS:
     {
         if (data.sizeToDriver != sizeof(GEF_VME_DRV_TIME_OFF_BUS_INFO))
         {
             return -EBADMSG;
         }
         if (data.sizeFromDriver != sizeof(GEF_VME_DRV_TIME_OFF_BUS_INFO))
         {
             return -EBADMSG;
         }
         if (copy_from_user(&time_off_bus_info, (void*)data.dataToDriver, data.sizeToDriver))
         {
             return -EFAULT;
         }
             
         GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Calling vmeSetTimeOffBus from ioctl with time off value 0x%x\n", 
             time_off_bus_info.time_off);
             
         data.result = vmeSetTimeOffBus(&time_off_bus_info);
             
         GEF_VME_DEBUG(GEF_DEBUG_TRACE, "data.result from call: 0x%x\n",data.result);
             
         break;  
     }
// End Time On / Off Bus routines 
  
     default:
     {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Invalid data.command from call: 0x%x\n", data.command);

        return -EINVAL;
     }
  }

  if (copy_to_user((void *)arg, &data, sizeof(GEF_OSA_INTF_DATA)))
  {
     return -EFAULT;
  }
  return 0;
}
#endif
   

/*============================================================================
 * Find the FPGA device if present, and configure the VME registers
 */
static int vmic_init_fpga(void)
{
        struct pci_dev *vmic_pci_dev;
        uint32_t base_addr;
        uint16_t board_id;

        if ((vmic_pci_dev = pci_get_device(PCI_VENDOR_ID_VMIC, 0x0004, NULL))
            || (vmic_pci_dev =
                pci_get_device(PCI_VENDOR_ID_VMIC, 0x0005, NULL))
            || (vmic_pci_dev =
                pci_get_device(PCI_VENDOR_ID_VMIC, 0x0006, NULL))) {
                pci_read_config_dword(vmic_pci_dev, 0x10, &base_addr);

                if (NULL == (vmic_base = ioremap_nocache(base_addr, 12))) {
                        printk(KERN_ERR
                               "VME: Failure mapping the VMIC registers\n");
                        return -1;
                }

                pci_read_config_word(vmic_pci_dev, PCI_SUBSYSTEM_ID,
                                     (uint16_t *) & board_id);

                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Board is VMIVME-%x\n", board_id);

                /* Clear any pending BERR
                 */
                iowrite32(ioread32(vmic_base + VMIVMEF_COMM) | VMIVMEF_COMM__BERRST,
                       vmic_base + VMIVMEF_COMM);

                /* Set VMIC comm register
                   1) the user can pass in comm parameter when loading driver
                   2) if comm is zero then no parameter was passed in so use default
                 */
                if (comm)
                {
                    iowrite32(comm, vmic_base + VMIVMEF_COMM);
                }
                else		/* default */
                {
                    iowrite32(VMIVMEF_COMM__BERRI | VMIVMEF_COMM__MEC |
                               VMIVMEF_COMM__SEC | VMIVMEF_COMM__ABLE |
                               VMIVMEF_COMM__VME_EN, vmic_base + VMIVMEF_COMM);
                }
                
                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "COMM Reg val 0x%08x\n",ioread32(vmic_base + VMIVMEF_COMM)); 

        }

        return 0;
}

//-----------------------------------------------------------------------------
// Function   : findVmeBridge()
// Inputs     : void
// Outputs    : failed flag.
// Description: 
//    Search for a supported VME bridge chip.  
//    Initialize VME config registers.
// Remarks    : 
// History    : 
//-----------------------------------------------------------------------------
static
struct pci_dev *
findVmeBridge(void)
{
  struct pci_dev *vme_pci_dev = NULL;
  int pci_command;
  int status;

  if ((vme_pci_dev = pci_get_device(PCI_VENDOR_ID_TUNDRA,
                                     PCI_DEVICE_ID_TUNDRA_CA91C042, 
                                     vme_pci_dev))) {
    vmechip_devid = PCI_DEVICE_ID_TUNDRA_CA91C042;
    status = pci_enable_device(vme_pci_dev);

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  Universe II found.\n");
  }
  if( vme_pci_dev == NULL) {
    if ((vme_pci_dev = pci_get_device(PCI_VENDOR_ID_TUNDRA,
                                       PCI_DEVICE_ID_TUNDRA_TEMPE,
                                       vme_pci_dev))) {
      vmechip_devid = PCI_DEVICE_ID_TUNDRA_TEMPE;
      status = pci_enable_device(vme_pci_dev);

      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  Tempe found.\n");
    }
  }
  if( vme_pci_dev == NULL) {
    printk("  VME bridge not found on PCI Bus.\n");
    return(vme_pci_dev);
  }

  // read revision.
  pci_read_config_dword(vme_pci_dev, PCI_CLASS, &vmechip_revision);

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  PCI_CLASS = %08x\n", vmechip_revision);

  vmechip_revision &= 0xFF;

  // Unless user has already specified it, 
  // determine the VMEchip IRQ number.
  if(vmechip_irq == 0){
#ifdef PPC
    pci_read_config_dword(vme_pci_dev, PCI_INTERRUPT_LINE, &vmechip_irq);
#else
    vmechip_irq = vme_pci_dev->irq;
#endif

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  PCI_INTLINE = %08x\n", vmechip_irq);

    vmechip_irq &= 0x000000FF;                    // Only a byte in size
    if(vmechip_irq == 0) vmechip_irq = 0x00000050;    // Only a byte in size
  }
  if((vmechip_irq == 0) || (vmechip_irq == 0xFF)){
    printk("Bad VME IRQ number: %02x\n", vmechip_irq);
    return(NULL);
  }

  // Ensure Bus mastering, IO Space, and Mem Space are enabled
  pci_read_config_dword(vme_pci_dev, PCI_COMMAND, &pci_command);
  pci_command |= (PCI_COMMAND_MASTER | PCI_COMMAND_IO | PCI_COMMAND_MEMORY);
  pci_write_config_dword(vme_pci_dev, PCI_COMMAND, pci_command);

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  PCI_COMMAND = 0x%x\n",pci_command);

  return(vme_pci_dev);
}

//-----------------------------------------------------------------------------
// Function   : mapInVmeBridge()
// Inputs     : void
// Outputs    : failed flag.
// Description: 
//    Map VME chip registers in.  Perform basic sanity check of chip ID.
// Remarks    : 
// History    : 
//-----------------------------------------------------------------------------
int
mapInVmeBridge( struct pci_dev *vme_pci_dev)
{
  unsigned int temp, ba;

  // Setup VME Bridge Register Space
  // This is a 4k wide memory area that need to be mapped into the kernel
  // virtual memory space so we can access it.
  ba = pci_resource_start (vme_pci_dev, 0);
  physical_addr_regs = ba;

#ifdef PPC 
  vmechip_baseaddr = (char *)ioremap(ba,4096);
#else

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  map VMEchip to Kernel Space, physical_address: %08x\n", (unsigned int)ba);

  vmechip_baseaddr = (char *)ioremap_nocache(ba,4096);
#endif
  if (!vmechip_baseaddr) {
    printk("  ioremap failed to map VMEchip to Kernel Space.\n");
    return 1;
  }

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  mapped VMEchip to Kernel Space, kernel_address: %08x\n", 
            (unsigned int)vmechip_baseaddr);

  // Check to see if the Mapping Worked out
  temp = ioread32(vmechip_baseaddr) & 0x0000FFFF;
  if (temp != PCI_VENDOR_ID_TUNDRA) {
    printk("  VME Chip Failed to Return PCI_ID in Memory Map.\n");
    iounmap(vmechip_baseaddr);
    IOUNMAP_FPGA
    return 1;
  }

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "mapped regs for %x\n",temp); 

  return(0);
}

//-----------------------------------------------------------------------------
// Function   : cleanup_module
// Inputs     : void
// Outputs    : void
// Description: 
//    Initialize VME chip to quiescent state.
//    Free driver resources.
// Remarks    : 
// History    : 
//-----------------------------------------------------------------------------
void cleanup_module(void)
{
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "cleanup_module called\n");

  // Free waiting threads for irq level and vector combinations
  vmeFreeIrqWaits();

  // Free Kernel allocated Dma Buffers and Handles
  vmeFreeDmaHandles();

  if (HAS_RESOURCE(vme_init_flags, VME_RSRC_DEVINIT)){      

      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "cleanup_module: disabling device\n");
      
      // Queisce the hardware.
      switch(vmechip_devid) {
      case PCI_DEVICE_ID_TUNDRA_CA91C042:
          GEF_VME_DEBUG(GEF_DEBUG_TRACE, "cleanup_module: calling uni_shutdown\n");
          uni_shutdown();
          break;
      case PCI_DEVICE_ID_TUNDRA_TEMPE:
          GEF_VME_DEBUG(GEF_DEBUG_TRACE, "cleanup_module calling tempe_shutdown\n");
          tempe_shutdown();
          break;
      }
      
      DEL_RESOURCE(vme_init_flags, VME_RSRC_DEVINIT);
  }
  
  if (HAS_RESOURCE(vme_init_flags, VME_RSRC_PROC)) {
      
      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "cleanup_module: disabling proc\n");

      unregister_proc();
      DEL_RESOURCE(vme_init_flags, VME_RSRC_PROC);
  }

  if (HAS_RESOURCE(vme_init_flags, VME_RSRC_CHR)) {

      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "cleanup_module: disabling char dev\n");
      
      unregister_chrdev(VME_MAJOR, "vme");
      DEL_RESOURCE(vme_init_flags, VME_RSRC_CHR);
  }

  if (HAS_RESOURCE(vme_init_flags, VME_RSRC_INTR)) {   

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "cleanup_module: calling free_irq\n");      

#ifdef USE_RTOS
    rt_free_linux_irq(vmechip_irq, vme_pci_dev);
    rt_free_global_irq(vmechip_irq);

    int ii = 0;
    for(; ii < GEF_VME_INT_LEVEL_LAST; ++ii)
    {
        RT_TASK* task = irq_queue[ii];

        if(task != 0)
        {
            rt_task_suspend(task);
            rt_task_delete(task);
            irq_queue[ii] = 0;
        }
    }
#else
#ifdef PPC
      free_irq(vmechip_irq,NULL);
#else
      free_irq(vmechip_irq,
          vme_pci_dev);
      
#endif
#endif

      DEL_RESOURCE(vme_init_flags, VME_RSRC_INTR);
  }

  if (HAS_RESOURCE(vme_init_flags, VME_RSRC_BRIDGE)){

      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "cleanup_module: unmapping regs\n");
      
      if(vmechip_baseaddr){
          iounmap(vmechip_baseaddr);
      }
      
      DEL_RESOURCE(vme_init_flags, VME_RSRC_BRIDGE);
  }

  if (HAS_RESOURCE(vme_init_flags, VME_RSRC_PCI_EN)){

      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "cleanup_module: disabling pci device\n");

      pci_disable_device(vme_pci_dev);
      DEL_RESOURCE(vme_init_flags, VME_RSRC_PCI_EN);
  }

  if (HAS_RESOURCE(vme_init_flags, VME_RSRC_FPGA)) {

      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "cleanup_module: disabling fpga device\n");
      
      IOUNMAP_FPGA
      DEL_RESOURCE(vme_init_flags, VME_RSRC_FPGA);
  }
  
  if (vme_init_flags != 0) {
      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "cleanup_module: Residual Resources requiring deallocation: 0x%x\n", 
          vme_init_flags);
  }

#ifdef USE_RTOS
    rt_sem_delete(&vown_lock);
#endif
}

//-----------------------------------------------------------------------------
// Function   : init_module()
// Inputs     : void
// Outputs    : module initialization fail flag
// Description: 
//  top level module initialization.
//  find VME bridge
//  initialize VME bridge chip.
//  Sanity check bridge chip.
//  allocate and initialize driver resources.
// Remarks    : 
// History    : 
//-----------------------------------------------------------------------------
int init_module(void)
{
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
#endif
  int x,y;
  unsigned int temp;
  unsigned int status;
  unsigned int baseaddr = 0;
  char vstr[80];
  struct resource pcimemres;
  struct pci_dev *vme_ptp_pci_dev = NULL;

  dma_handle_list = NULL;
  vme_init_flags = 0;
  dma_in_progress = GEF_FALSE;
  dma_berr_failure = GEF_FALSE;

#ifdef USE_RTOS
    RTLOG_INFO("gefvme_rtos", "$Id$");
#endif

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Initializing FPGA\n");

  vmic_base = NULL;
  if (vmic_init_fpga()) {
#ifdef USE_RTOS
    RTLOG_ERROR("gefvme_rtos", "VME:Failure Initializing FPGA");
#else
      printk("VME:Failure Initializing FPGA\n");
#endif
      goto BailOut;
  }
  
  ADD_RESOURCE(vme_init_flags, VME_RSRC_FPGA);

  sprintf(vstr,"\ngefvme: driver version (Major.Minor.Build): %d.%d.%d\n",
                GEF_VME_DRV_VER_MAJOR,GEF_VME_DRV_VER_MINOR,GEF_VME_DRV_VER_BUILD);
#ifdef USE_RTOS
    RTLOG_INFO("gefvme_rtos", "%s", vstr);
#else
  printk(vstr);
#endif
  // find VME bridge
  vme_pci_dev = findVmeBridge();
  if (vme_pci_dev == NULL) {
#ifdef USE_RTOS
        RTLOG_ERROR("gefvme_rtos", "VME:Failure Finding VME Bridge");
#else
      printk("VME:Failure Finding VME Bridge\n");
#endif
      goto BailOut;
  }
  
  ADD_RESOURCE(vme_init_flags, VME_RSRC_PCI_EN);

#ifdef PPC
#ifdef	DISABLE_MV64360_SNOOP_WORKAROUND
{
  unsigned int *mv64360;
  // Disable snoop at 64360.
  if(vmechip_devid == PCI_DEVICE_ID_TUNDRA_TEMPE){
     mv64360 = (unsigned int *)ioremap(0xF1000000,0x100000);
     mv64360[0x1E00/4] = mv64360[0x1E00/4] & ~0x0C000000;
     vmeSyncData();
     iounmap(mv64360);
  }
}
#endif
#endif

  // Get parent PCI resource & verify enough space is available.
  memset(&pcimemres,0,sizeof(pcimemres));
  pcimemres.flags = IORESOURCE_MEM;
  vmepcimem = pci_find_parent_resource(vme_pci_dev, &pcimemres);
  if(vmepcimem == 0){
#ifdef USE_RTOS
    RTLOG_ERROR("gefvme_rtos", "VME:Can't get VME parent device PCI resource");
#else
    printk("VME:Can't get VME parent device PCI resource\n");
#endif
    goto BailOut;
  }

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "initial PCI MEM start %08x\n",(unsigned int)vmepcimem->start); 
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "initial PCI MEM end %08x\n",(unsigned int)vmepcimem->end);
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "initial PCI MEM size %08x\n",
           (unsigned int)(vmepcimem->end - vmepcimem->start)); 

#ifdef USE_RTOS
    RTLOG_INFO("gefvme_rtos", "vme_pci_start %08x vme_pci_end %08x",
        vme_pci_start, vme_pci_end);
#else
  printk("vme_pci_start %08x vme_pci_end %08x\n", vme_pci_start, vme_pci_end);
#endif

  // Increase the base start address at a bounded address to compensate for 4K Tempe register set.
  // This is due to the fact that the memory follows the registers on a Tempe-based board.
  if (vmechip_devid == PCI_DEVICE_ID_TUNDRA_TEMPE){
  
       pci_read_config_dword(vme_pci_dev, 0x10, &baseaddr);
       baseaddr &= 0xfffffff8;

       GEF_VME_DEBUG(GEF_DEBUG_TRACE, "VME BAR0: %08x\n", (unsigned int)baseaddr);

       vmepcimem->start = baseaddr + 0x10000;
  } else {

       if (vme_pci_start == 0){
           vme_pci_start = 0x80000000;
       }
       vmepcimem->start = vme_pci_start;

       if (vme_pci_end == ~0){
           vme_pci_end = 0xfffeffff;
       }
       vmepcimem->end = vme_pci_end;

       // If the VME bridge is behind a PCI bridge, only a meg of PCI MEM space
       // has been allocated.  Increase it, otherwise use the provided start address.
       if((vmepcimem->end - vmepcimem->start + 1 ) == 0x100000){
           // find bridge VME is under info

           uint16_t vme_pci_base;
           vme_pci_base = (vme_pci_start >> 16) & 0xffff;

           GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Only 1 MB: Recalculating vmepcimem->start %x base %x\n",
                 (unsigned int)vmepcimem->start, vme_pci_base);

           vme_ptp_pci_dev = vme_pci_dev->bus->self;
           pci_write_config_word(vme_ptp_pci_dev, PCI_MEMORY_BASE, vme_pci_base);
           vmepcimem->start = vme_pci_start;
       }

       // Bound the upper limit to 512MB for universe-based boards
       if ((vmepcimem->end - vmepcimem->start) > 0x20000000) {
           vmepcimem->end = vmepcimem->start + 0x1fffffff;

           GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Bounding pci upper memory limit: %08x\n", (unsigned int)vmepcimem->end);

       }
  }

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Configured PCI MEM start %08x\n",(unsigned int)vmepcimem->start); 
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Configured PCI MEM end %08x\n",(unsigned int)vmepcimem->end);
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Configured PCI MEM size %08x\n",
           (unsigned int)(vmepcimem->end - vmepcimem->start));

  // On V7865, pci memory options are 64M, 128M, 256M, and 512M for outbound windows. 
  // On other x86 SBC's, the default address starts at 2GB (0x80000000) and grows upward.
  if((vmepcimem->end - vmepcimem->start) < 0x2000000){
#ifdef USE_RTOS
    RTLOG_ERROR("gefvme_rtos", "VME:Not enough PCI memory space available %08x",
#else
    printk("VME:Not enough PCI memory space available %08x\n", 
#endif
           (unsigned int)(vmepcimem->end - vmepcimem->start));
    goto BailOut;
  }

  // Map in VME Bridge registers.
  if(mapInVmeBridge(vme_pci_dev)){
#ifdef USE_RTOS
    RTLOG_ERROR("gefvme_rtos", "VME:Bridge not initialized");
#else
      printk("VME:Bridge not initialized\n");
#endif
    goto BailOut;
  }

  ADD_RESOURCE(vme_init_flags, VME_RSRC_BRIDGE);

  // Initialize wait queues & mutual exclusion flags
#ifndef USE_RTOS
  init_waitqueue_head(&dma_queue[0]);
  init_waitqueue_head(&dma_queue[1]);
#endif
  init_waitqueue_head(&lm_queue);
  init_waitqueue_head(&mbox_queue);
#ifdef USE_RTOS
    SEMAPHORE_TIMEOUT = nano2count(semaphoreTimeout * 1000);
    rt_typed_sem_init(&vown_lock, RES_SEM, 1);

    for(x = 0; x < GEF_VME_INT_LEVEL_LAST; ++x)
    {
        irq_queue[x] = 0;
    }
#else
  for (x=0;x<GEF_VME_INT_LEVEL_LAST;x++) {
      init_waitqueue_head(&irq_queue[x]);
  }
#endif

  sema_init(&virq_inuse, 1);
  for (x=0;x<MAX_MINOR+1;x++) {
    opened[x]    = 0;
    sema_init(&devinuse[x], 1);
  }

  // Initialize interrupt state data
  for (x=0;x<GEF_VME_INT_LEVEL_LAST;x++) { 
      vme_irq_waitcount[x] = 0;
      for (y=0;y<0x100;y++) {
          vme_irqlog[x][y] = 0;
          vme_vec_flags[x][y] = 0;
      }
  }

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Waitqueues and sems initialized\n"); 

  // Display VME information  
  pci_read_config_dword(vme_pci_dev, PCI_CSR, &status);

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  Vendor = %04X  Device = %04X  Revision = %02X Status = %08X\n",
         vme_pci_dev->vendor,vme_pci_dev->device, vmechip_revision, status);
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  Class = %08X\n",vme_pci_dev->class);

  pci_read_config_dword(vme_pci_dev, PCI_MISC0, &temp);

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  Misc0 = %08X\n",temp);      
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  Irq = %04X\n",vmechip_irq);
  
  // Initialize double buffer indices for BERR reporting
  vme_berr_write_buf = 0;
  vme_berr_read_buf = 0;

  // Initialize chip registers and register module.
  if(!vmeInit()){
#ifdef USE_RTOS
    RTLOG_ERROR("gefvme_rtos", "VME:Chip Initialization failed.");
#else
      printk("VME:Chip Initialization failed.\n");
#endif
      goto BailOut;
      
  }

  ADD_RESOURCE(vme_init_flags, VME_RSRC_DEVINIT);

/* No user space interface for the RT version of this kernel module. */
#ifndef USE_RTOS
  if (register_chrdev(VME_MAJOR, "vme", &vme_fops)) {
    printk("  Error getting Major Number for Drivers\n");
    iounmap(vmechip_baseaddr);
    IOUNMAP_FPGA;
    return(-1);
  }
  ADD_RESOURCE(vme_init_flags, VME_RSRC_CHR);
#endif

  if(vme_syscon == 0){
#ifdef USE_RTOS
    RTLOG_INFO("gefvme_rtos", "VME: Not system controller");
#else
     printk("VME: Not system controller\n");
#endif
  } 
  if(vme_syscon == 1){
#ifdef USE_RTOS
    RTLOG_INFO("gefvme_rtos", "VME: Is system controller");
#else
     printk("VME: Is system controller\n");
#endif
  }

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  Tundra VME Loaded.\n\n"); 

  register_proc();

  ADD_RESOURCE(vme_init_flags, VME_RSRC_PROC);

  // Get time base speed.  Needed to support DMA throughput measurements
#ifdef PPC
  tb_speed = tb_ticks_per_jiffy * HZ;
#else
  {
    unsigned long calibrate_start;
    unsigned long calibrate_end;

    rdtscl(calibrate_start);
    udelay(100);
    rdtscl(calibrate_end);

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "time calibration start: %lx end %lx\n", 
                      calibrate_start,calibrate_end);

    /* time calibration converted to ticks per ms */
    if (calibrate_end < calibrate_start) {
       tb_speed = ((0xffffffff - calibrate_start) + calibrate_end + 1) * 10;
    } else {
       tb_speed = (calibrate_end - calibrate_start) * 10;
    }

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "time tb_speed: %x \n",tb_speed);

  }    
  
#endif

  return 0;

BailOut:
  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Bailing out of initialization\n");  
  
  (void)cleanup_module();
  return -1;
}

void
printComm(unsigned long comm)
{
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "***************************\n");
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "* Comm Register (0x%lx)    \n", comm);

    if(comm & VMIVMEF_COMM__VME_EN)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "*    VME_EN: 1\n");
    }
    else
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "*    VME_EN: 0\n");
    }

    if(comm & VMIVMEF_COMM__BYPASS)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "*    BYPASS: 1\n");
    }
    else
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "*    BYPASS: 0\n");
    }

    if(comm & VMIVMEF_COMM__WTDSYS)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "*    WTDSYS: 1\n");
    }
    else
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "*    WTDSYS: 0\n");
    }

    if(comm & VMIVMEF_COMM__BERRST)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "*    BERRST: 1\n");
    }
    else
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "*    BERRST: 0\n");
    }

    if(comm & VMIVMEF_COMM__BERRI)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "*    BERRI: 1\n");
    }
    else
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "*     BERRI: 0\n");
    }

    if(comm & VMIVMEF_COMM__BTOV__1000)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "*    BTOV__1000: 1\n");
    }
    else
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "*    BTOV__1000: 0\n");
    }

    if(comm & VMIVMEF_COMM__BTOV__256)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "*    BTOV__256: 1\n");
    }
    else
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "*    BTOV__256: 0\n");
    }

    if(comm & VMIVMEF_COMM__BTOV__64)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "*    BTOV__64: 1\n");
    }
    else
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "*    BTOV__64: 0\n");
    }

    if(comm & VMIVMEF_COMM__BTOV__16)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "*    BTOV__16: 1\n");
    }
    else
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "*    BTOV__16: 0\n");
    }

    if(comm & VMIVMEF_COMM__BTO)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "*    BTO: 1\n");
    }
    else
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "*    BTO: 0\n");
    }

    if(comm & VMIVMEF_COMM__ABLE)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "*    ABLE: 1\n");
    }
    else
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "*    ABLE: 0\n");
    }

    if(comm & VMIVMEF_COMM__SEC)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "*    SEC: 1\n");
    }
    else
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "*    SEC: 0\n");
    }

    if(comm & VMIVMEF_COMM__MEC)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "*    MEC: 1\n");
    }
    else
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "*    MEC: 0\n");
    }
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "***************************\n");
}

int
vmeSetMasterHWByteswap(GEF_VME_HW_BYTESWAP_INFO *hwbsInfo)
{
    unsigned long comm = 0;

    switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
        if(NULL == vmic_base) {
            return(GEF_STATUS_DEVICE_NOT_INIT);
        }
        break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
        return (GEF_STATUS_NOT_SUPPORTED);
    default:
        return(GEF_STATUS_NO_SUCH_DEVICE);
    }

    comm = ioread32(vmic_base + VMIVMEF_COMM);
    
    switch(hwbsInfo->byteswap)
    {
    case GEF_BYTESWAP_NO_SWAP:
        hwEndian = GEF_ENDIAN_LITTLE;
        iowrite32(comm & ~VMIVMEF_COMM__MEC, vmic_base + VMIVMEF_COMM);
        break;
    case GEF_BYTESWAP_SWAP_D32:
    case GEF_BYTESWAP_SWAP_D16: 
        return(GEF_STATUS_NOT_SUPPORTED);
    case GEF_BYTESWAP_SWAP_ON_SIZE:
        iowrite32(comm | VMIVMEF_COMM__MEC, vmic_base + VMIVMEF_COMM);
        hwEndian = GEF_ENDIAN_BIG;
        break; 
    }

    comm = ioread32(vmic_base + VMIVMEF_COMM);
    printComm(comm);

    return(GEF_SUCCESS);
}

int
vmeGetMasterHWByteswap(GEF_VME_HW_BYTESWAP_INFO *hwbsInfo)
{
    unsigned long comm = 0;

    switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
        if(NULL == vmic_base) {
            return(GEF_STATUS_DEVICE_NOT_INIT);
        }
        break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
        return (GEF_STATUS_NOT_SUPPORTED);
    default:
        return(GEF_STATUS_NO_SUCH_DEVICE);
    }

    comm = ioread32(vmic_base + VMIVMEF_COMM);

    printComm(comm);
    
    switch(comm & VMIVMEF_COMM__MEC)
    {
    case 0: hwbsInfo->byteswap = GEF_BYTESWAP_NO_SWAP; break;
    case VMIVMEF_COMM__MEC: hwbsInfo->byteswap = GEF_BYTESWAP_SWAP_ON_SIZE; break; 
    }

    return(GEF_SUCCESS);
}

int
vmeSetSlaveHWByteswap(GEF_VME_HW_BYTESWAP_INFO *hwbsInfo)
{
    unsigned long comm = 0;

    switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
        if(NULL == vmic_base) {
            return(GEF_STATUS_DEVICE_NOT_INIT);
        }
        break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
        return (GEF_STATUS_NOT_SUPPORTED);
    default:
        return(GEF_STATUS_NO_SUCH_DEVICE);
    }

    comm = ioread32(vmic_base + VMIVMEF_COMM);
    
    switch(hwbsInfo->byteswap)
    {
    case GEF_BYTESWAP_NO_SWAP: 
        iowrite32(comm & ~VMIVMEF_COMM__SEC, vmic_base + VMIVMEF_COMM);
        break;
    case GEF_BYTESWAP_SWAP_D32:
    case GEF_BYTESWAP_SWAP_D16: 
        return(GEF_STATUS_NOT_SUPPORTED);
    case GEF_BYTESWAP_SWAP_ON_SIZE:
        iowrite32(comm | VMIVMEF_COMM__SEC, vmic_base + VMIVMEF_COMM);
        break; 
    }

    comm = ioread32(vmic_base + VMIVMEF_COMM);
    printComm(comm);

    return(GEF_SUCCESS);
}

int
vmeGetSlaveHWByteswap(GEF_VME_HW_BYTESWAP_INFO *hwbsInfo)
{
    unsigned long comm = 0;

    switch(vmechip_devid) {
    case PCI_DEVICE_ID_TUNDRA_CA91C042:
        if(NULL == vmic_base) {
            return(GEF_STATUS_DEVICE_NOT_INIT);
        }
        break;
    case PCI_DEVICE_ID_TUNDRA_TEMPE:
        return (GEF_STATUS_NOT_SUPPORTED);
    default:
        return(GEF_STATUS_NO_SUCH_DEVICE);
    }

    comm = ioread32(vmic_base + VMIVMEF_COMM);

    printComm(comm);
    
    switch(comm & VMIVMEF_COMM__SEC)
    {
    case 0: hwbsInfo->byteswap = GEF_BYTESWAP_NO_SWAP; break;
    case VMIVMEF_COMM__SEC: hwbsInfo->byteswap = GEF_BYTESWAP_SWAP_ON_SIZE; break; 
    }

    return(GEF_SUCCESS);
}

