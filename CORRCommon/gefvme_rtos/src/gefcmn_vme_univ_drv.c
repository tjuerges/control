/*
* File:
*    gefcmn_vme_univ_drv.c
*
* Description:
*    Source code to the VME device platform switch.
*
* Copyright:
*    Copyright 2007 GE Fanuc Embedded Systems, Inc.
*
* License:
*    This file is proprietary to GE Fanuc Embedded Systems, Inc.
*
*
* $Date$
* $Rev: 5609 $
*/

#include "gef_kernel_abs.h"

//-----------------------------------------------------------------------------
// Function   : uniSetArbiter
// Description: 
//-----------------------------------------------------------------------------
GEF_STATUS uniSetArbiter( GEF_VME_DRV_BUS_ARB_INFO *bus_arb_info)
{
    GEF_UINT32 tempCtl = 0;
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniSetArbiter: Set arbitration mode to %d\n", bus_arb_info->arbitration_mode);
    
    // Validate parameters
    switch(bus_arb_info->arbitration_mode)
    {
    case GEF_VME_BUS_ARB_PRIORITY: 
    case GEF_VME_BUS_ARB_ROUND_ROBIN: 
        break;
    default: return(GEF_STATUS_NOT_SUPPORTED);
    }
    
    gefUnivSpinlockAcquire();
    
    tempCtl = GEF_IOREAD32(UNIV_MISC_CTL);
    
    switch(bus_arb_info->arbitration_mode)
    {
    case GEF_VME_BUS_ARB_PRIORITY: tempCtl |= (1 << 26); break;
    case GEF_VME_BUS_ARB_ROUND_ROBIN: tempCtl &= ~(1 << 26); break;
    default: break;
    }
    
    GEF_IOWRITE32(tempCtl, UNIV_MISC_CTL);
    GEF_VME_IO_SYNC();
    
    gefUnivSpinlockRelease();
    
    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : uniGetArbiter
// Description: 
//-----------------------------------------------------------------------------
GEF_STATUS uniGetArbiter(GEF_VME_DRV_BUS_ARB_INFO *bus_arb_info)
{
    GEF_UINT32 tempCtl = 0;
    
    tempCtl = GEF_IOREAD32(UNIV_MISC_CTL);
    
    if(tempCtl & (1 << 26)){
        bus_arb_info->arbitration_mode = GEF_VME_BUS_ARB_PRIORITY;
    } else {
        bus_arb_info->arbitration_mode = GEF_VME_BUS_ARB_ROUND_ROBIN;
    }
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniGetArbiter: arbitration mode: %d\n", bus_arb_info->arbitration_mode);

    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : uniSetReleaseMode
// Description: 
//-----------------------------------------------------------------------------
GEF_UINT32 
uniSetReleaseMode(GEF_VME_DRV_BUS_REL_MODE_INFO *bus_release_info)
{
    GEF_UINT32 tempCtl = 0;
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniSetReleaseMode: Set release mode to %d\n", bus_release_info->release_mode);

    /* validate parameters */
    switch(bus_release_info->release_mode)
    {
    case GEF_VME_BUS_RELEASE_WHEN_DONE: break;
    case GEF_VME_BUS_RELEASE_ON_REQUEST: break;
    default: return(GEF_STATUS_NOT_SUPPORTED);
    }
    
    gefUnivSpinlockAcquire();
    
    tempCtl = GEF_IOREAD32(UNIV_MAST_CTL);
    
    switch(bus_release_info->release_mode)
    {
    case GEF_VME_BUS_RELEASE_WHEN_DONE: tempCtl &=  ~(1 << 20); break;
    case GEF_VME_BUS_RELEASE_ON_REQUEST: tempCtl |=  (1 << 20); break;
    default: break;
    }
    
    GEF_IOWRITE32(tempCtl, UNIV_MAST_CTL);
    GEF_VME_IO_SYNC();
    
    gefUnivSpinlockRelease();
    
    return(GEF_SUCCESS);  
}

//-----------------------------------------------------------------------------
// Function   : uniGetReleaseMode
// Description: 
//-----------------------------------------------------------------------------
GEF_STATUS uniGetReleaseMode(GEF_VME_DRV_BUS_REL_MODE_INFO *bus_release_info)
{
    GEF_UINT32 tempCtl = 0;
    
    tempCtl = GEF_IOREAD32(UNIV_MAST_CTL);
    
    if(tempCtl & (1 << 20)){
        bus_release_info->release_mode = GEF_VME_BUS_RELEASE_ON_REQUEST;
    } else {
        bus_release_info->release_mode = GEF_VME_BUS_RELEASE_WHEN_DONE;
    }
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniGetReleaseMode: release mode: %d\n", bus_release_info->release_mode);

    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : uniSetRequestMode
// Description: 
//-----------------------------------------------------------------------------
GEF_STATUS uniSetRequestMode(GEF_VME_DRV_BUS_REQ_MODE_INFO *bus_request_info)
{
    GEF_UINT32 tempCtl = 0;
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniSetRequestMode: Set request mode to %d\n", bus_request_info->request_mode);

    /* validate parameters */
    switch(bus_request_info->request_mode)
    {
    case GEF_VME_BUS_REQUEST_DEMAND: break;
    case GEF_VME_BUS_REQUEST_FAIR: break;
    default: return(GEF_STATUS_NOT_SUPPORTED);
    }
    
    gefUnivSpinlockAcquire();
    
    tempCtl = GEF_IOREAD32(UNIV_MAST_CTL);
    
    switch(bus_request_info->request_mode)
    {
    case GEF_VME_BUS_REQUEST_DEMAND: tempCtl &=  ~(1 << 21); break;
    case GEF_VME_BUS_REQUEST_FAIR: tempCtl |=  (1 << 21); break;
    default: break;
    }
    
    GEF_IOWRITE32(tempCtl, UNIV_MAST_CTL);
    GEF_VME_IO_SYNC();
    
    gefUnivSpinlockRelease();
    
    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : uniGetRequestMode
// Description: 
//-----------------------------------------------------------------------------
GEF_STATUS uniGetRequestMode(GEF_VME_DRV_BUS_REQ_MODE_INFO *bus_request_info)
{
    GEF_UINT32 tempCtl = 0;
    
    tempCtl = GEF_IOREAD32(UNIV_MAST_CTL);
    
    if(tempCtl & (1 << 21)){
        bus_request_info->request_mode = GEF_VME_BUS_REQUEST_FAIR;
    } else {
        bus_request_info->request_mode = GEF_VME_BUS_REQUEST_DEMAND;
    }

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniGetRequestMode: request mode: %d\n", bus_request_info->request_mode);

    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : uniSetRequestLevel
// Description: 
//-----------------------------------------------------------------------------
GEF_STATUS uniSetRequestLevel(GEF_VME_DRV_BUS_REQ_LEVEL_INFO *bus_req_level_info)
{
    GEF_UINT32 tempCtl = 0;

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniSetRequestLevel: Set request level to %d\n", bus_req_level_info->request_level);
    
    /* validate parameters */
    switch(bus_req_level_info->request_level)
    {
    case GEF_VME_BUS_REQUEST_LEVEL_0: break;
    case GEF_VME_BUS_REQUEST_LEVEL_1: break;
    case GEF_VME_BUS_REQUEST_LEVEL_2: break;
    case GEF_VME_BUS_REQUEST_LEVEL_3: break;
    default: return(GEF_STATUS_NOT_SUPPORTED);
    }
    
    gefUnivSpinlockAcquire();
    
    tempCtl = GEF_IOREAD32(UNIV_MAST_CTL);
    
    tempCtl = (tempCtl & ~(0x03<<22));
    
    switch(bus_req_level_info->request_level)
    {
    case GEF_VME_BUS_REQUEST_LEVEL_0: tempCtl |= UNIV_MAST_CTL__VRL__0; break;
    case GEF_VME_BUS_REQUEST_LEVEL_1: tempCtl |= UNIV_MAST_CTL__VRL__1; break;
    case GEF_VME_BUS_REQUEST_LEVEL_2: tempCtl |= UNIV_MAST_CTL__VRL__2; break; 
    case GEF_VME_BUS_REQUEST_LEVEL_3: tempCtl |= UNIV_MAST_CTL__VRL__3; break; 
    default: break;
    }
    
    GEF_IOWRITE32(tempCtl, UNIV_MAST_CTL);
    GEF_VME_IO_SYNC();
    
    gefUnivSpinlockRelease();
    
    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : uniGetRequestLevel
// Description: 
//-----------------------------------------------------------------------------
GEF_STATUS uniGetRequestLevel(GEF_VME_DRV_BUS_REQ_LEVEL_INFO *bus_req_level_info)
{
    GEF_UINT32 tempCtl = 0;
    
    tempCtl = GEF_IOREAD32(UNIV_MAST_CTL);
    
    bus_req_level_info->request_level = (tempCtl & 0x00C00000);
    
    switch(bus_req_level_info->request_level)
    {
    case UNIV_MAST_CTL__VRL__0: bus_req_level_info->request_level = GEF_VME_BUS_REQUEST_LEVEL_0; break;
    case UNIV_MAST_CTL__VRL__1: bus_req_level_info->request_level = GEF_VME_BUS_REQUEST_LEVEL_1; break;
    case UNIV_MAST_CTL__VRL__2: bus_req_level_info->request_level = GEF_VME_BUS_REQUEST_LEVEL_2; break;
    case UNIV_MAST_CTL__VRL__3: bus_req_level_info->request_level = GEF_VME_BUS_REQUEST_LEVEL_3; break;
    default: break;
    }

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniGetRequestLevel: request level: %d\n", bus_req_level_info->request_level);

    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : uniSetBusTimeout
// Description: 
//-----------------------------------------------------------------------------
GEF_STATUS uniSetBusTimeout(GEF_VME_DRV_BUS_TIMEOUT_INFO *bus_timeout_info)
{
    GEF_UINT32 tempCtl = 0;
    
    GEF_UINT32 vbto = 0;
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniSetBusTimeout: Set bus timeout to %d\n", bus_timeout_info->timeout);
 
    /* validate parameters */
    switch(bus_timeout_info->timeout)
    {
    case GEF_VME_BUS_TIMEOUT_DISABLE: break;
    case GEF_VME_BUS_TIMEOUT_16us: break;
    case GEF_VME_BUS_TIMEOUT_32us: break;
    case GEF_VME_BUS_TIMEOUT_64us: break;
    case GEF_VME_BUS_TIMEOUT_128us: break;
    case GEF_VME_BUS_TIMEOUT_256us: break;
    case GEF_VME_BUS_TIMEOUT_512us: break;
    case GEF_VME_BUS_TIMEOUT_1024us: break;
    default: return(GEF_STATUS_NOT_SUPPORTED);
    }
    
    gefUnivSpinlockAcquire();
    
    tempCtl = GEF_IOREAD32(UNIV_MISC_CTL);
    
    tempCtl = (tempCtl & ~(0xF<<28));
    
    switch(bus_timeout_info->timeout)
    {
    case GEF_VME_BUS_TIMEOUT_DISABLE: vbto = UNIV_MISC_CTL__VBTO__DISABLE; break;
    case GEF_VME_BUS_TIMEOUT_16us: vbto = UNIV_MISC_CTL__VBTO__16; break;
    case GEF_VME_BUS_TIMEOUT_32us: vbto = UNIV_MISC_CTL__VBTO__32; break;
    case GEF_VME_BUS_TIMEOUT_64us: vbto = UNIV_MISC_CTL__VBTO__64; break;
    case GEF_VME_BUS_TIMEOUT_128us: vbto = UNIV_MISC_CTL__VBTO__128; break;
    case GEF_VME_BUS_TIMEOUT_256us: vbto = UNIV_MISC_CTL__VBTO__256; break;
    case GEF_VME_BUS_TIMEOUT_512us: vbto = UNIV_MISC_CTL__VBTO__512; break;
    case GEF_VME_BUS_TIMEOUT_1024us: vbto = UNIV_MISC_CTL__VBTO__1024; break;
    default: break;
    }
    
    tempCtl = (tempCtl | vbto);
    
    GEF_IOWRITE32(tempCtl, UNIV_MISC_CTL);
    GEF_VME_IO_SYNC();
    
    gefUnivSpinlockRelease();
    
    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : uniGetBusTimeout
// Description: 
//-----------------------------------------------------------------------------
GEF_STATUS uniGetBusTimeout(GEF_VME_DRV_BUS_TIMEOUT_INFO *bus_timeout_info)
{
    GEF_UINT32 tempCtl = 0;
    
    tempCtl = GEF_IOREAD32(UNIV_MISC_CTL);
    
    bus_timeout_info->timeout = (tempCtl & 0xF0000000);
    
    switch(bus_timeout_info->timeout)
    {
    case UNIV_MISC_CTL__VBTO__DISABLE: bus_timeout_info->timeout = GEF_VME_BUS_TIMEOUT_DISABLE; break;
    case UNIV_MISC_CTL__VBTO__16: bus_timeout_info->timeout = GEF_VME_BUS_TIMEOUT_16us; break;
    case UNIV_MISC_CTL__VBTO__32: bus_timeout_info->timeout = GEF_VME_BUS_TIMEOUT_32us; break;
    case UNIV_MISC_CTL__VBTO__64: bus_timeout_info->timeout = GEF_VME_BUS_TIMEOUT_64us; break;
    case UNIV_MISC_CTL__VBTO__128: bus_timeout_info->timeout = GEF_VME_BUS_TIMEOUT_128us; break;
    case UNIV_MISC_CTL__VBTO__256: bus_timeout_info->timeout = GEF_VME_BUS_TIMEOUT_256us; break;
    case UNIV_MISC_CTL__VBTO__512: bus_timeout_info->timeout = GEF_VME_BUS_TIMEOUT_512us; break;
    case UNIV_MISC_CTL__VBTO__1024: bus_timeout_info->timeout = GEF_VME_BUS_TIMEOUT_1024us; break;
    default:
        {
            GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniGetBusTimeout: error: bus timeout is unknown: 0x%X\n", bus_timeout_info->timeout);
        }
        return (GEF_STATUS_DRIVER_ERR);
    }
    

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniGetBusTimeout: bus timeout: %d\n", bus_timeout_info->timeout);

    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : uniSetArbTimeout
// Description: 
//-----------------------------------------------------------------------------
GEF_STATUS uniSetArbTimeout(GEF_VME_DRV_ARB_TIMEOUT_INFO *arb_timeout_info)
{
    GEF_UINT32 tempCtl = 0;
    
    GEF_UINT32 varbto = 0;
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniSetArbTimeout: Set arbitration timeout to %d\n", arb_timeout_info->arb_timeout);
  
    /* validate parameters */
    switch(arb_timeout_info->arb_timeout)
    {
    case GEF_VME_ARB_TIMEOUT_DISABLE: break;
    case GEF_VME_ARB_TIMEOUT_16us: break;
    case GEF_VME_ARB_TIMEOUT_256us: break;
    default: return(GEF_STATUS_NOT_SUPPORTED);
    }
    
    gefUnivSpinlockAcquire();
    
    tempCtl = GEF_IOREAD32(UNIV_MISC_CTL);
    
    tempCtl = (tempCtl & ~(0x3<<24));
    
    switch(arb_timeout_info->arb_timeout)
    {
    case GEF_VME_ARB_TIMEOUT_DISABLE: varbto = UNIV_MISC_CTL__VARBTO__DISABLE; break;
    case GEF_VME_ARB_TIMEOUT_16us: varbto = UNIV_MISC_CTL__VARBTO__16; break;
    case GEF_VME_ARB_TIMEOUT_256us: varbto = UNIV_MISC_CTL__VARBTO__256; break;
    default: break;
    }
    
    tempCtl = (tempCtl | varbto);
    
    GEF_IOWRITE32(tempCtl, UNIV_MISC_CTL);
    GEF_VME_IO_SYNC();
    
    gefUnivSpinlockRelease();
    
    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : uniGetArbTimeout
// Description: 
//-----------------------------------------------------------------------------
GEF_STATUS uniGetArbTimeout(GEF_VME_DRV_ARB_TIMEOUT_INFO *arb_timeout_info)
{
    GEF_UINT32 tempCtl = 0;
    
    tempCtl = GEF_IOREAD32(UNIV_MISC_CTL);
    
    arb_timeout_info->arb_timeout = (tempCtl & 0x03000000);
    
    switch(arb_timeout_info->arb_timeout)
    {
    case UNIV_MISC_CTL__VARBTO__DISABLE: arb_timeout_info->arb_timeout = GEF_VME_ARB_TIMEOUT_DISABLE; break;
    case UNIV_MISC_CTL__VARBTO__16: arb_timeout_info->arb_timeout = GEF_VME_ARB_TIMEOUT_16us; break;
    case UNIV_MISC_CTL__VARBTO__256: arb_timeout_info->arb_timeout = GEF_VME_ARB_TIMEOUT_256us; break;
    default:
        {
            GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniGetArbTimeout: error: arbitration timeout is unknown: 0x%X\n", arb_timeout_info->arb_timeout);
        }
        return (GEF_STATUS_DRIVER_ERR);
    }
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniGetArbTimeout: arbitration timeout: %d\n", arb_timeout_info->arb_timeout);

    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : uniSetSysFail
// Description: 
//-----------------------------------------------------------------------------
GEF_STATUS uniSetSysFail(GEF_VME_DRV_SYSFAIL_INFO *sysfail_info)
{
    GEF_UINT32 tempCtl = 0;
        
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniSetSysFail: Set sysfail to %d\n", sysfail_info->sysfail);

    gefUnivSpinlockAcquire();
    
    tempCtl = (tempCtl | (1<<30));
    if(sysfail_info->sysfail)
    {
        GEF_IOWRITE32(tempCtl, UNIV_VCSR_SET);
    }
    else
    {
        GEF_IOWRITE32(tempCtl, UNIV_VCSR_CLR);
    }
    GEF_VME_IO_SYNC();
    
    gefUnivSpinlockRelease();
    
    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : uniGetSysFail
// Description: 
//-----------------------------------------------------------------------------
GEF_STATUS uniGetSysFail(GEF_VME_DRV_SYSFAIL_INFO *sysfail_info)
{
    GEF_UINT32 tempCtl = 0;
    
    tempCtl = GEF_IOREAD32(UNIV_VCSR_CLR);
    
	if ((tempCtl & UNIV_VCSR__SYSFAIL) == 0)
	{
		sysfail_info->sysfail = GEF_FALSE;
	}
	else
	{
		sysfail_info->sysfail = GEF_TRUE;
	}

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniGetSysFail: sysfail: %d\n", sysfail_info->sysfail);

    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : uniSetMaxRetry
// Description: 
//-----------------------------------------------------------------------------
GEF_STATUS uniSetMaxRetry(GEF_VME_MAX_RETRY_INFO *max_retry_info)
{
    GEF_UINT32 tempCtl = 0;
    GEF_UINT32 maxretry = 0;
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniSetMaxRetry: Set max retry to %d\n", max_retry_info->max_retry);
   
    /* validate parameters */
    switch(max_retry_info->max_retry)
    {
    case GEF_VME_MAX_RETRY_INFINITE: break;
    case GEF_VME_MAX_RETRY_64: break;
    case GEF_VME_MAX_RETRY_128: break;
    case GEF_VME_MAX_RETRY_192: break;
    case GEF_VME_MAX_RETRY_256: break;
    case GEF_VME_MAX_RETRY_320: break;
    case GEF_VME_MAX_RETRY_384: break;
    case GEF_VME_MAX_RETRY_448: break;
    case GEF_VME_MAX_RETRY_512: break;
    case GEF_VME_MAX_RETRY_576: break;
    case GEF_VME_MAX_RETRY_640: break;
    case GEF_VME_MAX_RETRY_704: break;
    case GEF_VME_MAX_RETRY_768: break;
    case GEF_VME_MAX_RETRY_832: break;
    case GEF_VME_MAX_RETRY_896: break;
    case GEF_VME_MAX_RETRY_960: break;
    default: return(GEF_STATUS_NOT_SUPPORTED);
    }
    
    gefUnivSpinlockAcquire();
    
    tempCtl = GEF_IOREAD32(UNIV_MAST_CTL);
    
    tempCtl = (tempCtl & ~(0xF<<28));
    
    switch(max_retry_info->max_retry)
    {
    case GEF_VME_MAX_RETRY_INFINITE: maxretry = UNIV_MAST_CTL__MAXRTRY__FOREVER; break;
    case GEF_VME_MAX_RETRY_64: maxretry = UNIV_MAST_CTL__MAXRTRY__64; break;
    case GEF_VME_MAX_RETRY_128: maxretry = UNIV_MAST_CTL__MAXRTRY__128; break;
    case GEF_VME_MAX_RETRY_192: maxretry = UNIV_MAST_CTL__MAXRTRY__192; break;
    case GEF_VME_MAX_RETRY_256: maxretry = UNIV_MAST_CTL__MAXRTRY__256; break;
    case GEF_VME_MAX_RETRY_320: maxretry = UNIV_MAST_CTL__MAXRTRY__320; break;
    case GEF_VME_MAX_RETRY_384: maxretry = UNIV_MAST_CTL__MAXRTRY__384; break;
    case GEF_VME_MAX_RETRY_448: maxretry = UNIV_MAST_CTL__MAXRTRY__448; break;
    case GEF_VME_MAX_RETRY_512: maxretry = UNIV_MAST_CTL__MAXRTRY__512; break;
    case GEF_VME_MAX_RETRY_576: maxretry = UNIV_MAST_CTL__MAXRTRY__576; break;
    case GEF_VME_MAX_RETRY_640: maxretry = UNIV_MAST_CTL__MAXRTRY__640; break;
    case GEF_VME_MAX_RETRY_704: maxretry = UNIV_MAST_CTL__MAXRTRY__704; break;
    case GEF_VME_MAX_RETRY_768: maxretry = UNIV_MAST_CTL__MAXRTRY__768; break;
    case GEF_VME_MAX_RETRY_832: maxretry = UNIV_MAST_CTL__MAXRTRY__832; break;
    case GEF_VME_MAX_RETRY_896: maxretry = UNIV_MAST_CTL__MAXRTRY__896; break;
    case GEF_VME_MAX_RETRY_960: maxretry = UNIV_MAST_CTL__MAXRTRY__960; break;
    default: break;
    }
    
    tempCtl = (tempCtl | maxretry);
    
    GEF_IOWRITE32(tempCtl, UNIV_MAST_CTL);
    GEF_VME_IO_SYNC();
    
    gefUnivSpinlockRelease();
    
    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : uniGetMaxRetry
// Description: 
//-----------------------------------------------------------------------------
GEF_STATUS uniGetMaxRetry(GEF_VME_MAX_RETRY_INFO *max_retry_info)
{
    GEF_UINT32 tempCtl = 0;
    
    tempCtl = GEF_IOREAD32(UNIV_MAST_CTL);
    max_retry_info->max_retry = (tempCtl & 0xF0000000);
    
    switch(max_retry_info->max_retry)
    {
    case UNIV_MAST_CTL__MAXRTRY__FOREVER: max_retry_info->max_retry = GEF_VME_MAX_RETRY_INFINITE; break;
    case UNIV_MAST_CTL__MAXRTRY__64: max_retry_info->max_retry = GEF_VME_MAX_RETRY_64; break;
    case UNIV_MAST_CTL__MAXRTRY__128: max_retry_info->max_retry = GEF_VME_MAX_RETRY_128; break;
    case UNIV_MAST_CTL__MAXRTRY__192: max_retry_info->max_retry = GEF_VME_MAX_RETRY_192; break;
    case UNIV_MAST_CTL__MAXRTRY__256: max_retry_info->max_retry = GEF_VME_MAX_RETRY_256; break;
    case UNIV_MAST_CTL__MAXRTRY__320: max_retry_info->max_retry = GEF_VME_MAX_RETRY_320; break;
    case UNIV_MAST_CTL__MAXRTRY__384: max_retry_info->max_retry = GEF_VME_MAX_RETRY_384; break;
    case UNIV_MAST_CTL__MAXRTRY__448: max_retry_info->max_retry = GEF_VME_MAX_RETRY_448; break;
    case UNIV_MAST_CTL__MAXRTRY__512: max_retry_info->max_retry = GEF_VME_MAX_RETRY_512; break;
    case UNIV_MAST_CTL__MAXRTRY__576: max_retry_info->max_retry = GEF_VME_MAX_RETRY_576; break;
    case UNIV_MAST_CTL__MAXRTRY__640: max_retry_info->max_retry = GEF_VME_MAX_RETRY_640; break;
    case UNIV_MAST_CTL__MAXRTRY__704: max_retry_info->max_retry = GEF_VME_MAX_RETRY_704; break;
    case UNIV_MAST_CTL__MAXRTRY__768: max_retry_info->max_retry = GEF_VME_MAX_RETRY_768; break;
    case UNIV_MAST_CTL__MAXRTRY__832: max_retry_info->max_retry = GEF_VME_MAX_RETRY_832; break;
    case UNIV_MAST_CTL__MAXRTRY__896: max_retry_info->max_retry = GEF_VME_MAX_RETRY_896; break;
    case UNIV_MAST_CTL__MAXRTRY__960: max_retry_info->max_retry = GEF_VME_MAX_RETRY_960; break;
    default: break;
    }
        
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniGetMaxRetry: max retry: %d\n", max_retry_info->max_retry);

    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : uniSetPostedWriteCount
// Description: 
//-----------------------------------------------------------------------------
GEF_STATUS uniSetPostedWriteCount(GEF_VME_POST_WRITE_COUNT_INFO *pwon_info)
{
    GEF_UINT32 tempCtl = 0;
    GEF_UINT32 pwon = 0;
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniSetPostedWriteCount: Set powsted write count to %d\n", pwon_info->pwon);
    
    /* validate parameters */
    switch(pwon_info->pwon)
    {
    case GEF_VME_PWON__EARLY_RELEASE: break;
    case GEF_VME_PWON_128: break;
    case GEF_VME_PWON_256: break;
    case GEF_VME_PWON_512: break;
    case GEF_VME_PWON_1024: break;
    case GEF_VME_PWON_2048: break;
    case GEF_VME_PWON_4096: break;
    default: return(GEF_STATUS_NOT_SUPPORTED);
    }
    
    gefUnivSpinlockAcquire();
    
    tempCtl = GEF_IOREAD32(UNIV_MAST_CTL);
    
    tempCtl = (tempCtl & ~(0xF<<24));
    
    switch(pwon_info->pwon)
    {
    case GEF_VME_PWON__EARLY_RELEASE: pwon = UNIV_MAST_CTL__PWON__EARLY_RELEASE; break;
    case GEF_VME_PWON_128: pwon = UNIV_MAST_CTL__PWON__128; break;
    case GEF_VME_PWON_256: pwon = UNIV_MAST_CTL__PWON__256; break;
    case GEF_VME_PWON_512: pwon = UNIV_MAST_CTL__PWON__512; break;
    case GEF_VME_PWON_1024: pwon = UNIV_MAST_CTL__PWON__1024; break;
    case GEF_VME_PWON_2048: pwon = UNIV_MAST_CTL__PWON__2048; break;
    case GEF_VME_PWON_4096: pwon = UNIV_MAST_CTL__PWON__4096; break;
    default: break;
    }
    
    tempCtl = (tempCtl | pwon);
    
    GEF_IOWRITE32(tempCtl, UNIV_MAST_CTL);
    GEF_VME_IO_SYNC();
    
    gefUnivSpinlockRelease();
    
    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : uniGetPostedWriteCount
// Description: 
//-----------------------------------------------------------------------------
GEF_STATUS uniGetPostedWriteCount(GEF_VME_POST_WRITE_COUNT_INFO *pwon_info)
{
    GEF_UINT32 tempCtl = 0;
    
    tempCtl = GEF_IOREAD32(UNIV_MAST_CTL);
    pwon_info->pwon = (tempCtl & 0x0F000000);
    
    switch(pwon_info->pwon)
    {
    case UNIV_MAST_CTL__PWON__EARLY_RELEASE: pwon_info->pwon = GEF_VME_PWON__EARLY_RELEASE; break;
    case UNIV_MAST_CTL__PWON__128: pwon_info->pwon = GEF_VME_PWON_128; break;
    case UNIV_MAST_CTL__PWON__256: pwon_info->pwon = GEF_VME_PWON_256; break;
    case UNIV_MAST_CTL__PWON__512: pwon_info->pwon = GEF_VME_PWON_512; break;
    case UNIV_MAST_CTL__PWON__1024: pwon_info->pwon = GEF_VME_PWON_1024; break;
    case UNIV_MAST_CTL__PWON__2048: pwon_info->pwon = GEF_VME_PWON_2048; break;
    case UNIV_MAST_CTL__PWON__4096: pwon_info->pwon = GEF_VME_PWON_4096; break;
    default: break;
    }
       
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniGetPostedWriteCount: posted write count: %d\n", pwon_info->pwon);

    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : uniSysReset
// Description: 
//-----------------------------------------------------------------------------
GEF_STATUS uniSysReset(void)
{
    GEF_UINT32 tempCtl = 0;
       
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniSysReset: System reset\n");
  
    gefUnivSpinlockAcquire();
    
    tempCtl = GEF_IOREAD32(UNIV_MISC_CTL);
    
    tempCtl |= UNIV_MISC_CTL__SW_SRST;
    
    GEF_IOWRITE32(tempCtl, UNIV_MISC_CTL);
    GEF_VME_IO_SYNC();
    
    gefUnivSpinlockRelease();
    
    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : uniQueryBusOwnership
// Description: 
//-----------------------------------------------------------------------------
GEF_STATUS uniQueryBusOwnership(GEF_BOOL *vown_info)
{
    *vown_info = (GEF_IOREAD32(UNIV_MAST_CTL) & UNIV_MAST_CTL__VOWN_ACK) ? GEF_TRUE : GEF_FALSE;
    
    return(GEF_SUCCESS);
}
