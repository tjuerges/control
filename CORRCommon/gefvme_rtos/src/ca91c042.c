/*
 * File:
 *    ca91c042.c
 *
 * Description:
 *    Source code to the Universe driver.
 *
 * Copyright:
 *    Copyright 2007 GE Fanuc Embedded Systems, Inc.
 *
 * License:
 *    This file is proprietary to GE Fanuc Embedded Systems, Inc.
 *
 *
 * $Date$
 * $Rev$
 */


/*
Modified to support Linux 2.6 x86 platforms
===============================================================================
*/

//------------------------------------------------------------------------------  
//title: Tundra Universe PCI-VME Kernel Driver
//version: Linux 3.1
//date: February 2004
//original designer: Michael Wyrick
//original programmer: Michael Wyrick
//  Extensively restructured by Tom Armistead in support of the 
//  Tempe/Universe II combo driver and additional functionality.
//platform: Linux 2.4.x
//language: GCC 2.95 and 3.0
//------------------------------------------------------------------------------  
//  Purpose: Provide a Kernel Driver to Linux for the Universe I and II 
//           Universe model number ca91c042
//  Docs:                                  
//------------------------------------------------------------------------------  
// RCS:
// $Id$
// Log: ca91c042.c,v
// Revision 1.5  2001/10/27 03:50:07  jhuggins
// These changes add support for the extra images offered by the Universe II.
// CVS : ----------------------------------------------------------------------
//
// Revision 1.6  2001/10/16 15:16:53  wyrick
// Minor Cleanup of Comments
//
//
//-----------------------------------------------------------------------------
#include <linux/version.h>
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,5,0)
#define MODULE
#endif

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,18)
#include <linux/config.h>
#endif

#ifdef CONFIG_MODVERSIONS
  #define MODVERSIONS

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,5,0)
#include <linux/modversions.h>
#endif

#endif


#if LINUX_VERSION_CODE > KERNEL_VERSION(2,5,0)
#include <linux/interrupt.h>
#endif


#include <linux/module.h>
#include <linux/mm.h>
#include <linux/types.h>
#include <linux/errno.h>
#include <linux/proc_fs.h>
#include <linux/pci.h>
#include <linux/poll.h>
#include <linux/delay.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include <asm/msr.h> 
#include "gef/gefcmn_vme_framework.h"
#include "gef/gefcmn_vme_errno.h"
#include "gef/gefcmn_vme_uni.h"
#include "vmivme.h"
#include "vmedrv.h"
#include "ca91c042.h"

#ifdef USE_RTOS
#include <rtai_types.h>
#include <rtai_sem.h>
#include <rtLog.h>
#endif

//----------------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------------
int uniDisableIrq(GEF_VME_INT_INFO *);
int uniEnableIrq(GEF_VME_INT_INFO *);

extern const int vmechip_revision;
extern const int vmechip_devid;
extern const int vmechip_irq;
extern int vmechipIrqOverHeadTicks;
extern char *vmechip_baseaddr;
extern int vme_syscon;
extern unsigned int out_image_va[];
extern GEF_UINT32 vme_irqlog[GEF_VME_INT_LEVEL_LAST][0x100];
extern GEF_UINT32 vme_irq_waitcount[];
extern GEF_UINT8  vme_vec_flags[GEF_VME_INT_LEVEL_LAST][0x100];
extern GEF_VME_BERR_INFO vme_irq_berr_info[];
extern GEF_VME_INT_INFO vme_irq_int_info[];
extern GEF_UINT8  vme_berr_write_buf;
extern GEF_UINT8  vme_berr_read_buf;
extern GEF_UINT8  dma_in_progress;
extern GEF_UINT8  dma_berr_failure;
extern struct pci_dev *vme_pci_dev;
extern void *vmic_base;
extern struct resource out_resource[VME_MAX_WINDOWS];
extern int vmechip_bus;
extern struct resource *vmepcimem;
extern vmeMasterWindow_t vmw[VME_MAX_WINDOWS];
extern vmeSlaveWindow_t vsw[VME_MAX_WINDOWS];
extern int comm;
extern int (*g_pProbeBERRFunc)(GEF_VME_BERR_INFO *pBerrInfo);

//----------------------------------------------------------------------------
// Types
//----------------------------------------------------------------------------

static int outCTL[] = {LSI0_CTL, LSI1_CTL, LSI2_CTL, LSI3_CTL,
                     LSI4_CTL, LSI5_CTL, LSI6_CTL, LSI7_CTL};
                     
static int outBS[]  = {LSI0_BS,  LSI1_BS,  LSI2_BS,  LSI3_BS,
                     LSI4_BS,  LSI5_BS,  LSI6_BS,  LSI7_BS}; 
                     
static int outBD[]  = {LSI0_BD,  LSI1_BD,  LSI2_BD,  LSI3_BD,
                     LSI4_BD,  LSI5_BD,  LSI6_BD,  LSI7_BD}; 

static int outTO[]  = {LSI0_TO,  LSI1_TO,  LSI2_TO,  LSI3_TO,
                     LSI4_TO,  LSI5_TO,  LSI6_TO,  LSI7_TO}; 

static int vmevec[7]          = {V1_STATID, V2_STATID, V3_STATID, V4_STATID,
                                 V5_STATID, V6_STATID, V7_STATID};


//----------------------------------------------------------------------------
// Vars and Defines
//----------------------------------------------------------------------------
#ifdef USE_RTOS
extern irqreturn_t linuxPostIRQHandler(
    int irq,
    void* devId,
    struct pt_regs* regs);
extern RT_TASK* dma_queue[];
#else
extern wait_queue_head_t dma_queue[];
#endif
extern wait_queue_head_t lm_queue;
extern wait_queue_head_t mbox_queue;
#ifdef USE_RTOS
extern RT_TASK* irq_queue[];
#else
extern wait_queue_head_t irq_queue[];
#endif

extern	unsigned int vmeGetTime(void);
extern  void vmeSyncData(void);
extern  void vmeFlushLine(void *ptr);
extern	int	tb_speed;
extern unsigned int pci_bus_mem_base_phys(int bus);
extern void vmeInsertMasterHandle(vmeMasterWindow_t *, vmeMasterHandle_t *);
extern unsigned int vme_init_flags;
extern int vmeAllocSlaveMemory(GEF_UINT32 size, int window, GEF_UINT32 vme_addrL, GEF_UINT32 vme_addrU);
extern int vmeFreeSlaveMemory(int window);

unsigned int	uniIrqTime;
unsigned int	uniDmaIrqTime;

unsigned int	uniLmEvent;
#ifdef USE_RTOS
extern RTIME SEMAPHORE_TIMEOUT;
extern SEM vown_lock;
extern SEM slave_window_lock;
static RT_TASK* vown_queue;
#else
static DECLARE_MUTEX(vown_lock);
extern struct semaphore slave_window_lock;
static wait_queue_head_t vown_queue;
#endif
spinlock_t uni_lock;
static int vown_count = 0;

#define GEF_VME_MAX_WAIT_VOWN 1000
#define WAIT_VOWN_ACK(x)        \
{                               \
    int count = x;              \
    while(count>0){ --count; }  \
}

//----------------------------------------------------------------------------
//  uni_procinfo()
//----------------------------------------------------------------------------
int uni_procinfo(char *buf, char **start, off_t fpos, int lenght, int *eof, void *data)
{
  char *p;
  unsigned int temp1,temp2,x;

  p = buf;

  for (x=0;x<8;x+=2) {
    temp1 = ioread32(vmechip_baseaddr+outCTL[x]);
    temp2 = ioread32(vmechip_baseaddr+outCTL[x+1]);
    p += sprintf(p,"  LSI%i_CTL = %08X    LSI%i_CTL = %08X\n",x,temp1,x+1,temp2);
    temp1 = ioread32(vmechip_baseaddr+outBS[x]);
    temp2 = ioread32(vmechip_baseaddr+outBS[x+1]);
    p += sprintf(p,"  LSI%i_BS  = %08X    LSI%i_BS  = %08X\n",x,temp1,x+1,temp2);
    temp1 = ioread32(vmechip_baseaddr+outBD[x]);
    temp2 = ioread32(vmechip_baseaddr+outBD[x+1]);
    p += sprintf(p,"  LSI%i_BD  = %08X    LSI%i_BD  = %08X\n",x,temp1,x+1,temp2);
    temp1 = ioread32(vmechip_baseaddr+outTO[x]);
    temp2 = ioread32(vmechip_baseaddr+outTO[x+1]);
    p += sprintf(p,"  LSI%i_TO  = %08X    LSI%i_TO  = %08X\n",x,temp1,x+1,temp2);
  }  

  p += sprintf(p,"\n");  

  *eof = 1;
  return p - buf;
}

//----------------------------------------------------------------------------
//  uniBusErrorChk()
//----------------------------------------------------------------------------
int
uniBusErrorChk(int clrflag)
{
    int tmp;
    tmp = ioread32(vmechip_baseaddr+PCI_CSR);
    if (tmp & 0x08000000) {  // S_TA is Set
      if(clrflag)
        iowrite32(tmp | 0x08000000,vmechip_baseaddr+PCI_CSR);
      return(1);
    }
  return(0);
}

//-----------------------------------------------------------------------------
// Function   : DMA_uni_irqhandler
// Inputs     : void
// Outputs    : void
// Description: Saves DMA completion timestamp and then wakes up DMA queue
//-----------------------------------------------------------------------------
void DMA_uni_irqhandler(void)
{
  uniDmaIrqTime = uniIrqTime;
#ifdef USE_RTOS
    RT_TASK* task = dma_queue[0];
    if(task != 0)
    {
        rt_task_resume(task);
    }
#else
  wake_up(&dma_queue[0]);
#endif
}

//-----------------------------------------------------------------------------
// Function   : LERR_uni_irqhandler
// Inputs     : void
// Outputs    : void
// Description: 
//-----------------------------------------------------------------------------
void LERR_uni_irqhandler(void)
{
    GEF_UINT8 level;
    unsigned char iackvec;
    GEF_VME_INT_INFO int_info;

    // Disable the VME LERR interrupt
    int_info.int_level = GEF_VME_INT_LERR;
    int_info.int_vector = 0;
    uniDisableIrq(&int_info);

    level = DRV_IRQ_LEVEL_LERR; 
    iackvec = 0;

    if ((vme_vec_flags[level][iackvec] & GEF_IRQ_WAIT) == 0) {
        
#ifdef USE_RTOS
        rtlogRecord_t logRecord;
        RTLOG_ERROR("gefvme_rtos", "VME LERR: Target abort encountered on the "
            "PCI Bus.");
#else
        printk("VME LERR: Target abort encountered on the PCI Bus.\n");
#endif
    } else {
        
        vme_irqlog[level][iackvec]++;
        vme_vec_flags[level][iackvec] &= ~(IRQ_TIMEDOUT);
#ifdef USE_RTOS
        RT_TASK* task = irq_queue[level];
        if(task != 0)
        {
            rt_task_resume(task);
        }
#else
        wake_up_interruptible(&irq_queue[level]);
#endif
    }

    int_info.int_level = GEF_VME_INT_LERR;
    int_info.int_vector = 0;
    uniEnableIrq(&int_info);
}

//-----------------------------------------------------------------------------
// Function   : VERR_uni_irqhandler
// Inputs     : void
// Outputs    : void
// Description: 
//-----------------------------------------------------------------------------
void VERR_uni_irqhandler(void)
{
    GEF_UINT8 level;
    unsigned char iackvec;
    GEF_VME_INT_INFO int_info;
    
    
    // Disable the VME BERR interrupt
    int_info.int_level = GEF_VME_INT_BERR;
    int_info.int_vector = 0;
    uniDisableIrq(&int_info);
    
    // Determine if FPGA present
    if (NULL == vmic_base) {
        vme_irq_berr_info[vme_berr_write_buf].lower = 0;
        vme_irq_berr_info[vme_berr_write_buf].addr_mod = 0;
        
    } else {
        // Clear BERR in FPGA
        iowrite32(ioread32(vmic_base + VMIVMEF_COMM) | VMIVMEF_COMM__BERRST,
            vmic_base + VMIVMEF_COMM);
        vmeSyncData();
        
        vme_irq_berr_info[vme_berr_write_buf].lower = ioread32(vmic_base + VMIVMEF_VBAR);
        
        vme_irq_berr_info[vme_berr_write_buf].addr_mod =
            ioread32(vmic_base + VMIVMEF_VBAMR) & VMIVMEF_VBAMR__AM;

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "BERR data from FPGA:\n");
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "address lower: 0x%08x\n",vme_irq_berr_info[vme_berr_write_buf].lower);
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "address modifier: 0x%08x\n",vme_irq_berr_info[vme_berr_write_buf].addr_mod);
        
    } 
        
    // Not applicable to Universe
    vme_irq_berr_info[vme_berr_write_buf].upper = 0;
    vme_irq_berr_info[vme_berr_write_buf].extended_addr_mod = 0;
    vme_irq_berr_info[vme_berr_write_buf].iack_signal = GEF_FALSE;
    vme_irq_berr_info[vme_berr_write_buf].ds1_signal = GEF_FALSE;
    vme_irq_berr_info[vme_berr_write_buf].ds0_signal = GEF_FALSE;
    vme_irq_berr_info[vme_berr_write_buf].lword_signal = GEF_FALSE;
    vme_irq_berr_info[vme_berr_write_buf].vme_2eOT = GEF_FALSE;
    vme_irq_berr_info[vme_berr_write_buf].vme_2eST = GEF_FALSE;
    
    level = DRV_IRQ_LEVEL_BERR; 
    iackvec = 0;

    do
    {
		/* See if the probe is active */
		if (g_pProbeBERRFunc != NULL)
		{
			/* If it is, call that guy */
			if (g_pProbeBERRFunc(&(vme_irq_berr_info[vme_berr_write_buf])) == 0)
			{
				/* If this guy returns 0, then he handled the BERR. */
				break;
			}

			/* Otherwise, we'll need to handle it normally, just fall through */
		}

          if ((vme_vec_flags[level][iackvec] & GEF_IRQ_WAIT) == 0) {
        
#ifdef USE_RTOS
              rtlogRecord_t logRecord;
              RTLOG_ERROR("gefvme_rtos", "VME Bus Error occurred: ADDR 0x%08x "
                "AM 0x%08x.",
#else
               printk("VME Bus Error occurred: ADDR 0x%08x AM 0x%08x.\n",
#endif
               vme_irq_berr_info[vme_berr_write_buf].lower,vme_irq_berr_info[vme_berr_write_buf].addr_mod);
          } else {
        
               vme_irqlog[level][iackvec]++;
               vme_vec_flags[level][iackvec] &= ~(IRQ_TIMEDOUT);
#ifdef USE_RTOS
               RT_TASK* task = irq_queue[level];
               if(task != 0)
               {
                   rt_task_resume(task);
               }
#else
               wake_up_interruptible(&irq_queue[level]);
#endif
          }
    }
    while (0);

    // Update double-buffer pointer for BERR data
    if (vme_berr_write_buf == 0) {
        vme_berr_write_buf = 1;
        vme_berr_read_buf = 0;
    } else {
        vme_berr_write_buf = 0;
        vme_berr_read_buf = 1;
    }

    if (GEF_TRUE == dma_in_progress) {
        dma_berr_failure = GEF_TRUE;
        DMA_uni_irqhandler();
    }

    int_info.int_level = GEF_VME_INT_BERR;
    int_info.int_vector = 0;
    uniEnableIrq(&int_info);
}

//-----------------------------------------------------------------------------
// Function   : SYSFAIL_uni_irqhandler
// Inputs     : void
// Outputs    : void
// Description: 
//-----------------------------------------------------------------------------
void SYSFAIL_uni_irqhandler(void)
{
    GEF_UINT8 level;
    unsigned char iackvec;
    GEF_VME_INT_INFO int_info;

    // Disable the VME SYSFAIL interrupt. 
    int_info.int_level = GEF_VME_INT_SYSFAIL;
    int_info.int_vector = 0;
    uniDisableIrq(&int_info);

    level = DRV_IRQ_LEVEL_SYSFAIL; 
    iackvec = 0;

    if ((vme_vec_flags[level][iackvec] & GEF_IRQ_WAIT) == 0) {
        
#ifdef USE_RTOS
        rtlogRecord_t logRecord;
        RTLOG_ERROR("gefvme_rtos", "VME SYSFAIL: SYSFAIL asserted.");
#else
        printk("VME SYSFAIL: SYSFAIL asserted.\n");
#endif
    } else {
        
        vme_irqlog[level][iackvec]++;
        vme_vec_flags[level][iackvec] &= ~(IRQ_TIMEDOUT);
#ifdef USE_RTOS
        RT_TASK* task = irq_queue[level];
        if(task != 0)
        {
           rt_task_resume(task);
        }
#else
        wake_up_interruptible(&irq_queue[level]);
#endif
    }
}

//-----------------------------------------------------------------------------
// Function   : ACFAIL_uni_irqhandler
// Inputs     : void
// Outputs    : void
// Description: 
//-----------------------------------------------------------------------------
void ACFAIL_uni_irqhandler(void)
{
    GEF_UINT8 level;
    unsigned char iackvec;
    GEF_VME_INT_INFO int_info;

    // Disable the VME ACFAIL interrupt. 
    int_info.int_level = GEF_VME_INT_ACFAIL;
    int_info.int_vector = 0;
    uniDisableIrq(&int_info);

    level = DRV_IRQ_LEVEL_ACFAIL; 
    iackvec = 0;

    if ((vme_vec_flags[level][iackvec] & GEF_IRQ_WAIT) == 0) {
        
#ifdef USE_RTOS
        rtlogRecord_t logRecord;
        RTLOG_ERROR("gefvme_rtos", "VME ACFAIL: Imminent Power Failure "
            "encountered.");
#else
        printk("VME ACFAIL: Imminent Power Failure encountered.\n");
#endif
    } else {
        
        vme_irqlog[level][iackvec]++;
        vme_vec_flags[level][iackvec] &= ~(IRQ_TIMEDOUT);
#ifdef USE_RTOS
        RT_TASK* task = irq_queue[level];
        if(task != 0)
        {
            rt_task_resume(task);
        }
#else
        wake_up_interruptible(&irq_queue[level]);
#endif
    }
}

//-----------------------------------------------------------------------------
// Function   : MB_uni_irqhandler
// Inputs     : void
// Outputs    : void
// Description: 
//-----------------------------------------------------------------------------
void MB_uni_irqhandler( long mboxMask )
{

    GEF_UINT8 level;
    unsigned char iackvec;
    GEF_VME_INT_INFO int_info;
    GEF_UINT32 mbData = 0;

    // Determine if initialization calibration is being performed
    if (vmechipIrqOverHeadTicks == 0) {
        return;
    }
    
    // Disable the VME mailbox interrupt and read the data
    switch (mboxMask) {
    case 1: 
        int_info.int_level = GEF_VME_INT_MBOX0; 
        level = DRV_IRQ_LEVEL_MBOX0;
        mbData = ioread32(vmechip_baseaddr+MBOX0);
        break;
    case 2: 
        int_info.int_level = GEF_VME_INT_MBOX1;
        level = DRV_IRQ_LEVEL_MBOX1;
        mbData = ioread32(vmechip_baseaddr+MBOX1);
        break;
    case 4: 
        int_info.int_level = GEF_VME_INT_MBOX2; 
        level = DRV_IRQ_LEVEL_MBOX2;
        mbData = ioread32(vmechip_baseaddr+MBOX2);
        break; 
    case 8: 
        int_info.int_level = GEF_VME_INT_MBOX3; 
        level = DRV_IRQ_LEVEL_MBOX3;
        mbData = ioread32(vmechip_baseaddr+MBOX3);
        break; 
    default:
        return;
    }
    
    int_info.int_vector = 0;
    uniDisableIrq(&int_info);
    
    iackvec = 0;       
    vme_irqlog[level][iackvec]++;
    vme_vec_flags[level][iackvec] &= ~(IRQ_TIMEDOUT);
    vme_irq_int_info[level].int_vector = mbData;
#ifdef USE_RTOS
    RT_TASK* task = irq_queue[level];
    if(task != 0)
    {
        rt_task_resume(task);
    }
#else
    wake_up_interruptible(&irq_queue[level]);
#endif
    uniEnableIrq(&int_info);
}

//-----------------------------------------------------------------------------
// Function   : LM_uni_irqhandler
// Inputs     : void
// Outputs    : void
// Description: 
//-----------------------------------------------------------------------------
void LM_uni_irqhandler(long lmMask)
{

    GEF_UINT8 level;
    unsigned char iackvec;
    GEF_VME_INT_INFO int_info;
    
    // Disable the VME LM interrupt
    switch (lmMask) {
    case 1: 
        int_info.int_level = GEF_VME_INT_LM0; 
        level = DRV_IRQ_LEVEL_LM0;
        break;
    case 2: 
        int_info.int_level = GEF_VME_INT_LM1;
        level = DRV_IRQ_LEVEL_LM1;
        break;
    case 4: 
        int_info.int_level = GEF_VME_INT_LM2; 
        level = DRV_IRQ_LEVEL_LM2;
        break; 
    case 8: 
        int_info.int_level = GEF_VME_INT_LM3; 
        level = DRV_IRQ_LEVEL_LM3;
        break; 
    default:
        return;
    }
    
    int_info.int_vector = 0;
    uniDisableIrq(&int_info);
    
    iackvec = 0;       
    vme_irqlog[level][iackvec]++;
    vme_vec_flags[level][iackvec] &= ~(IRQ_TIMEDOUT);
#ifdef USE_RTOS
    RT_TASK* task = irq_queue[level];
    if(task != 0)
    {
        rt_task_resume(task);
    }
#else
    wake_up_interruptible(&irq_queue[level]);
#endif
    uniEnableIrq(&int_info);

}

//-----------------------------------------------------------------------------
// Function   : VIRQ_uni_irqhandler
// Inputs     : void
// Outputs    : void
// Description: 
//-----------------------------------------------------------------------------
void VIRQ_uni_irqhandler(long virqMask)
{
    GEF_UINT32 iackvec, i;
    GEF_UINT8 level = 0;
    GEF_VME_INT_INFO int_info;
    
    for(i = 7; i > 0; i--){
        if(virqMask & (1 << i)){
            iackvec = ioread32(vmechip_baseaddr+vmevec[i-1]);
            
            switch (i) {
                                
            case DRV_IRQ_LEVEL_VIRQ1: level = GEF_VME_INT_VIRQ1; break;
            case DRV_IRQ_LEVEL_VIRQ2: level = GEF_VME_INT_VIRQ2; break;
            case DRV_IRQ_LEVEL_VIRQ3: level = GEF_VME_INT_VIRQ3; break;
            case DRV_IRQ_LEVEL_VIRQ4: level = GEF_VME_INT_VIRQ4; break;
            case DRV_IRQ_LEVEL_VIRQ5: level = GEF_VME_INT_VIRQ5; break;
            case DRV_IRQ_LEVEL_VIRQ6: level = GEF_VME_INT_VIRQ6; break;
            case DRV_IRQ_LEVEL_VIRQ7: level = GEF_VME_INT_VIRQ7; break;
            }
                   
            int_info.int_level = level;
            int_info.int_vector = iackvec;

            if ((vme_vec_flags[level][iackvec] & GEF_IRQ_WAIT) == 0) {
                
#ifdef USE_RTOS
                  rtlogRecord_t logRecord;
                  RTLOG_ERROR("gefvme_rtos", "VME: Spurious Interrupt received "
                    "for level 0x%x vector 0x%x.",
#else
                printk("VME: Spurious Interrupt received for level 0x%x vector 0x%x.\n",
#endif
                    level,iackvec);
                
            } else {
                
                uniDisableIrq(&int_info);
                vme_irqlog[i][iackvec]++;
                vme_vec_flags[i][iackvec] &= ~(IRQ_TIMEDOUT);
#ifdef USE_RTOS
                RT_TASK* task = irq_queue[i];
                if(task != 0)
                {
                    rt_task_resume(task);
                }
#else
                wake_up_interruptible(&irq_queue[i]);
#endif
            }
        }
    }
}

//-----------------------------------------------------------------------------
// Function   : VOWN_uni_irqhandler
// Inputs     : void
// Outputs    : void
// Description: Wakes up VOWN queue
//-----------------------------------------------------------------------------
void VOWN_uni_irqhandler(void)
{
#ifdef USE_RTOS
    RT_TASK* task = vown_queue;
    if(task != 0)
    {
        rt_task_resume(task);
    }
#else
  wake_up(&vown_queue);
#endif
}


//-----------------------------------------------------------------------------
// Function   : uni_irqhandler
// Inputs     : int irq, void *dev_id, struct pt_regs *regs
// Outputs    : void
// Description: 
//-----------------------------------------------------------------------------
#ifdef USE_RTOS
static void uni_irqhandler(void)
#else
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,21)
static int uni_irqhandler(int irq, void *dev_id, struct pt_regs *regs)
#elif LINUX_VERSION_CODE < KERNEL_VERSION(2,5,0)
static void uni_irqhandler(int irq, void *dev_id, struct pt_regs *regs)
#else
static irqreturn_t uni_irqhandler(int irq, void *dev_id)
#endif
#endif
{
    GEF_UINT32 stat, enable, berr_stat;
    int handled = 0;
    unsigned long irqflags;

#ifndef USE_RTOS
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  ca91c042: uni_irqhandler irq: %02X\n", irq); 
#endif

#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&uni_lock);
#else
    spin_lock_irqsave(&uni_lock, irqflags);
#endif

#ifdef PPC 
    uniIrqTime = vmeGetTime();
#else
    rdtscl(uniIrqTime);
#endif
    
    enable = ioread32(vmechip_baseaddr+LINT_EN);
    stat = ioread32(vmechip_baseaddr+LINT_STAT);
    stat = stat & enable;

/***************************************************************************
    This routine will be called at the end of the isr.
    The vme interrupt vector needs to be read prior to
    clearing the LINT_STAT.  This will prevent multiple
    IACKs that were observered during a RORA vrfm2g test.

    iowrite32(stat, vmechip_baseaddr+LINT_STAT); // Clear all pending ints
****************************************************************************/
    
    berr_stat = 0;
    
    // Use BERR from FPGA if present
    if (vmic_base) {
        berr_stat = ioread32(vmic_base + VMIVMEF_COMM); 

        if (!(berr_stat & VMIVMEF_COMM__BERRI))
        {
            berr_stat = 0;
        }
    }            
    
    if (stat & 0x0100) {
#ifndef USE_RTOS
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  ca91c042: DMA_uni_irqhandler irq: %02X\n", irq);
#endif
        DMA_uni_irqhandler();
        handled = 1;
    }
    
    if (stat & 0x0200) {
#ifndef USE_RTOS
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  ca91c042: LERR_uni_irqhandler irq: %02X\n", irq);
#endif
        LERR_uni_irqhandler();
        handled = 1;
    }
    
    if ((stat & 0x0400) || (berr_stat & VMIVMEF_COMM__BERRST)) {
#ifndef USE_RTOS
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  ca91c042: VERR_uni_irqhandler irq: %02X\n", irq);
#endif
        VERR_uni_irqhandler();
        handled = 1;
    }

    if (stat & 0x4000) {
#ifndef USE_RTOS
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  ca91c042: SYSFAIL_uni_irqhandler irq: %02X\n", irq);
#endif
        SYSFAIL_uni_irqhandler();
        handled = 1;
    }
    
    if (stat & 0x8000) {
#ifndef USE_RTOS
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  ca91c042: ACFAIL_uni_irqhandler irq: %02X\n", irq);
#endif
        ACFAIL_uni_irqhandler();
        handled = 1;
    }
    
    if (stat & 0xF0000) {
#ifndef USE_RTOS
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  ca91c042: MB_uni_irqhandler irq: %02X\n", irq);
#endif
        MB_uni_irqhandler((stat & 0xF0000) >> 16);
        handled = 1;
    }
    
    if (stat & 0xF00000) {
#ifndef USE_RTOS
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  ca91c042: LM_uni_irqhandler irq: %02X\n", irq);
#endif
        LM_uni_irqhandler((stat & 0xF00000) >> 20);
        handled = 1;
    }
    
    if (stat & 0x0000FE) {
#ifndef USE_RTOS
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  ca91c042: VIRQ_uni_irqhandler irq: %02X\n", irq);
#endif
        VIRQ_uni_irqhandler(stat & 0x0000FE);
        handled = 1;
    }
    
    if (stat & 0x0001) {
#ifndef USE_RTOS
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "  ca91c042: VOWN_uni_irqhandler irq: %02X\n", irq);
#endif
        VOWN_uni_irqhandler();
        handled = 1;
    }

    iowrite32(stat, vmechip_baseaddr+LINT_STAT);                // Clear all pending ints

#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &uni_lock);
#else
    spin_unlock_irqrestore(&uni_lock, irqflags);
#endif

#ifdef USE_RTOS
    if(handled != 1)
    {
        rt_mask_and_ack_irq(vme_pci_dev->irq);
        rt_pend_linux_irq(vme_pci_dev->irq);
    }
else
    {
        rt_enable_irq(vme_pci_dev->irq);
    }

#else
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,5,0)
    return IRQ_RETVAL(handled);
#endif
#endif
}

/*============================================================================
* Acquire ownership of the VMEbus.
*
* WARNING: Aquiring ownership is implemented with a counting semaphore. Be
* absolutely sure to make a vme_release_bus_ownership call for every
* vme_acquire_bus_ownership call, otherwise, the VMEbus will remain held.
*/
GEF_STATUS uniAcquireBusOwnership(GEF_VME_DRV_VOWN_INFO *vown_info)
{
    GEF_UINT32 tempCtl = 0;
    int timeout;
    int looptimeout;
    unsigned long irqflags;

    // Poll for ownership at 200 times per second rate
    looptimeout = HZ/200; 

    if (looptimeout <= 0) {
        looptimeout = 1;
    }
    
    // Calculate total timeout
    timeout  = (vown_info->timeout.tv_sec * HZ);
    timeout += (vown_info->timeout.tv_usec * HZ) / 1000000UL;
    if (timeout == 0) {
        timeout = 1;
    }

    // schedule_timeout doesn't like negative values.
    timeout &= 0x7fffffff;

#ifdef USE_RTOS
    int stat = 0;
    stat = rt_sem_wait_timed(&vown_lock, SEMAPHORE_TIMEOUT);

    if((stat == SEM_ERR) || (stat == SEM_TIMOUT))
    {
#else
    if (down_interruptible(&vown_lock)) {
#endif
        return(GEF_STATUS_DRIVER_ERR);
    }
    
    if (0 == vown_count) {

#ifdef USE_RTOS
        irqflags = rt_spin_lock_irqsave(&uni_lock);
#else
        spin_lock_irqsave(&uni_lock, irqflags);
#endif

        /* VOWN (VME ownership bit): 
        0=release bus, 1=acquire and hold bus
        */
        tempCtl = ioread32(vmechip_baseaddr+UNIV_MAST_CTL);
        tempCtl |= UNIV_MAST_CTL__VOWN;
        iowrite32(tempCtl, vmechip_baseaddr+UNIV_MAST_CTL);
        vmeSyncData();

#ifdef USE_RTOS
        rt_spin_unlock_irqrestore(irqflags, &uni_lock);
#else
        spin_unlock_irqrestore(&uni_lock, irqflags);       
#endif

        /* VOWN_ACK (VME ownership bit acknowledge):
        0=VMEbus not owned, 1=VMEbus acquired and held due to assertion
        of VOWN
        */
        
        /* Poll the VME Master Control Register until the VOWN_ACK is set. */
        while((ioread32(vmechip_baseaddr+UNIV_MAST_CTL) & UNIV_MAST_CTL__VOWN_ACK) != UNIV_MAST_CTL__VOWN_ACK){
#ifdef USE_RTOS
            rt_sleep(nano2count(jiffies_to_usecs(looptimeout) * 1000ULL));
#else
            set_current_state(TASK_INTERRUPTIBLE);
            schedule_timeout(looptimeout);
#endif
            timeout = timeout - looptimeout;
            if(timeout <= 0){
#ifdef USE_RTOS
                rt_sem_signal(&vown_lock);
#else
                up(&vown_lock);
#endif

                /* clear the VOWN bit */
#ifdef USE_RTOS
                irqflags = rt_spin_lock_irqsave(&uni_lock);
#else
                spin_lock_irqsave(&uni_lock, irqflags);
#endif
                tempCtl = ioread32(vmechip_baseaddr+UNIV_MAST_CTL);
                tempCtl &= ~UNIV_MAST_CTL__VOWN;
                iowrite32(tempCtl, vmechip_baseaddr+UNIV_MAST_CTL);
                vmeSyncData();
#ifdef USE_RTOS
                rt_spin_unlock_irqrestore(irqflags, &uni_lock);
#else
                spin_unlock_irqrestore(&uni_lock, irqflags);
#endif

                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniAcquireBusOwnership: Acquiring VMEbus timeout\n");
                return(GEF_STATUS_TIMED_OUT);
            }
        }
                             
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniAcquireBusOwnership: Acquired VMEbus\n");

    }
    else
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniAcquireBusOwnership: Already acquired with count %d\n", vown_count);
    }
    
    ++vown_count;
#ifdef USE_RTOS
    rt_sem_signal(&vown_lock);
#else
    up(&vown_lock);
#endif

    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : uniReleaseBusOwnership
// Description: 
//-----------------------------------------------------------------------------
GEF_STATUS uniReleaseBusOwnership(void)
{
    GEF_UINT32 tempCtl = 0;
    unsigned long irqflags;
        
#ifdef USE_RTOS
    int stat = 0;
    stat = rt_sem_wait_timed(&vown_lock, SEMAPHORE_TIMEOUT);

    if((stat == SEM_ERR) || (stat == SEM_TIMOUT))
    {
#else
    if (down_interruptible(&vown_lock)) {
#endif
        return(GEF_STATUS_DRIVER_ERR);
    }
    
    if (1 == vown_count) {
#ifdef USE_RTOS
        irqflags = rt_spin_lock_irqsave(&uni_lock);
#else
        spin_lock_irqsave(&uni_lock, irqflags);
#endif
        tempCtl = ioread32(vmechip_baseaddr+UNIV_MAST_CTL);
        tempCtl &= ~UNIV_MAST_CTL__VOWN;
        iowrite32(tempCtl, vmechip_baseaddr+UNIV_MAST_CTL);
        vmeSyncData();
#ifdef USE_RTOS
        rt_spin_unlock_irqrestore(irqflags, &uni_lock);
#else
        spin_unlock_irqrestore(&uni_lock, irqflags);
#endif

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniReleaseBusOwnership: Released VMEbus\n");

        vown_count = 0;

    }
    else
    {
        if(vown_count > 0){
            GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniReleaseBusOwnership: VMEbus still acquired with count of %d\n", vown_count);
            --vown_count;
        }
        else{
#ifdef USE_RTOS
              rtlogRecord_t logRecord;
              RTLOG_ERROR("gefvme_rtos", "VME: Redundant VMEbus ownership "
                "release attempt.");
#else
            printk("VME: Redundant VMEbus ownership release attempt.\n");
#endif
        }
    }

#ifdef USE_RTOS
    rt_sem_signal(&vown_lock);
#else
    up(&vown_lock);
#endif

    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : uniGenerateIrq
// Description: Generate a VME bus interrupt at the requested level & vector.
// Wait for system controller to ack the interrupt.
//-----------------------------------------------------------------------------
int
uniGenerateIrq(GEF_VME_DRV_VIRQ_INFO *virq_info)
{
    GEF_INT32 timeout;
    GEF_INT32 looptimeout;
    GEF_UINT32 irqflags;
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniGenerateIrq: level %x vec %x\n",virq_info->level,virq_info->vector);
        
    // Poll for iack at 200 times per second rate
    looptimeout = HZ/200; 

    if (looptimeout <= 0) {
        looptimeout = 1;
    }

    // Calculate total timeout
    timeout  = (virq_info->timeout.tv_sec * HZ);
    timeout += (virq_info->timeout.tv_usec * HZ) / 1000000UL;
    if (timeout == 0) {
        timeout = 1;
    }

    // schedule_timeout doesn't like negative values.
    timeout &= 0x7fffffff;
    
    if (NULL == vmechip_baseaddr) {
        return(GEF_STATUS_DEVICE_NOT_INIT);
    }
    
    // We only allow generation of software interrupts
    if ((GEF_VME_INT_VIRQ1 > virq_info->level) || (GEF_VME_INT_VIRQ7 < virq_info->level)) {
        return(GEF_STATUS_NOT_SUPPORTED);
    }

    // Generated software interrupts must be on even vectors for Universe
    if(virq_info->vector & 1){
        return(GEF_STATUS_NOT_SUPPORTED);
    }

#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&uni_lock);
#else
    spin_lock_irqsave(&uni_lock, irqflags);
#endif

    iowrite32(virq_info->vector << 24, vmechip_baseaddr+STATID);
    
    // Generate the interrupt by clearing all enable bits then reasserting
    // the enable. The Universe chip will clear the interrupt status
    // (software interrupts are ROAK).
    iowrite8(0,(vmechip_baseaddr + VINT_EN + 3));
    iowrite8(ioread8(vmechip_baseaddr + VINT_EN + 3) | (1 << (virq_info->level)),
        (vmechip_baseaddr + VINT_EN + 3));
    
#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &uni_lock);
#else
    spin_unlock_irqrestore (&uni_lock, irqflags);
#endif

    // Wait for syscon to do iack
    while(ioread32(vmechip_baseaddr+VINT_STAT) & (1 << (virq_info->level + 24))){
#ifdef USE_RTOS
        rt_sleep(nano2count(jiffies_to_usecs(looptimeout) * 1000ULL));
#else
        set_current_state(TASK_INTERRUPTIBLE);
        schedule_timeout(looptimeout);
#endif
        timeout = timeout - looptimeout;
        if(timeout <= 0){
            
            
           /* A zero-to-one transition causes the VME software interrupt to be asserted.
            * Subsequent zeroing of this bit causes the interrupt to be masked and the
            * VMEbus interrupt negated, but does not clear the VME software interrupt
            * status bit.
            */
            iowrite8(ioread8(vmechip_baseaddr + VINT_EN + 3) & ~(1 << (virq_info->level)),
                (vmechip_baseaddr + VINT_EN + 3));
            
            /* Write 1 to clear interrupt status */
            iowrite32(ioread32(vmechip_baseaddr+VINT_STAT) | (1 << (virq_info->level + 24)),
                (vmechip_baseaddr + VINT_STAT));
            
            
            return(GEF_STATUS_TIMED_OUT);
        }
    }
        
    return(GEF_SUCCESS);
}

// Make sure this function is called with spin locks enabled
int
uniEnableIrq(GEF_VME_INT_INFO *int_info)
{
  GEF_UINT32 tempCtl = 0;

  if (NULL == vmechip_baseaddr) {
    return(GEF_STATUS_DEVICE_NOT_INIT);
  }

  switch(int_info->int_level)
  {
  case GEF_VME_INT_VOWN: break;
  case GEF_VME_INT_VIRQ1: break;
  case GEF_VME_INT_VIRQ2: break;
  case GEF_VME_INT_VIRQ3: break;
  case GEF_VME_INT_VIRQ4: break;
  case GEF_VME_INT_VIRQ5: break;
  case GEF_VME_INT_VIRQ6: break;
  case GEF_VME_INT_VIRQ7: break;
  case GEF_VME_INT_DMA0: break;
  case GEF_VME_INT_LERR: break;
  case GEF_VME_INT_BERR: break;
  case GEF_VME_INT_SW_IACK: break;
  case GEF_VME_INT_SW_INT: break;
  case GEF_VME_INT_SYSFAIL: break;
  case GEF_VME_INT_ACFAIL: break;
  case GEF_VME_INT_MBOX0: break;
  case GEF_VME_INT_MBOX1: break;
  case GEF_VME_INT_MBOX2: break;
  case GEF_VME_INT_MBOX3: break;
  case GEF_VME_INT_LM0: break;
  case GEF_VME_INT_LM1: break;
  case GEF_VME_INT_LM2: break;
  case GEF_VME_INT_LM3: break;
  default: return(GEF_STATUS_NOT_SUPPORTED);
  }

  tempCtl = ioread32(vmechip_baseaddr+LINT_EN);

  switch(int_info->int_level)
  {
  case GEF_VME_INT_VOWN: tempCtl |= UNIV_LINT__VOWN; break;
  case GEF_VME_INT_VIRQ1: tempCtl |= UNIV_LINT__VIRQ1; break;
  case GEF_VME_INT_VIRQ2: tempCtl |= UNIV_LINT__VIRQ2; break;
  case GEF_VME_INT_VIRQ3: tempCtl |= UNIV_LINT__VIRQ3; break;
  case GEF_VME_INT_VIRQ4: tempCtl |= UNIV_LINT__VIRQ4; break;
  case GEF_VME_INT_VIRQ5: tempCtl |= UNIV_LINT__VIRQ5; break;
  case GEF_VME_INT_VIRQ6: tempCtl |= UNIV_LINT__VIRQ6; break;
  case GEF_VME_INT_VIRQ7: tempCtl |= UNIV_LINT__VIRQ7; break;
  case GEF_VME_INT_DMA0: tempCtl |= UNIV_LINT__DMA; break;
  case GEF_VME_INT_LERR: tempCtl |= UNIV_LINT__LERR; break;
  case GEF_VME_INT_BERR: 
      
      // Initialize VERR if FPGA not present
      if (NULL == vmic_base) {
          tempCtl |= UNIV_LINT__VERR;
      } else {
          // Enable BERR in FPGA
          iowrite32(ioread32(vmic_base + VMIVMEF_COMM) | VMIVMEF_COMM__BERRI,
              vmic_base + VMIVMEF_COMM);
      }            
      break;
      
  case GEF_VME_INT_SW_IACK: tempCtl |= UNIV_LINT__SW_IACK; break;
  case GEF_VME_INT_SW_INT: tempCtl |= UNIV_LINT__SW_INT; break;
  case GEF_VME_INT_SYSFAIL: tempCtl |= UNIV_LINT__SYSFAIL; break;
  case GEF_VME_INT_ACFAIL: tempCtl |= UNIV_LINT__ACFAIL; break;
  case GEF_VME_INT_MBOX0: tempCtl |= UNIV_LINT__MBOX0; break;
  case GEF_VME_INT_MBOX1: tempCtl |= UNIV_LINT__MBOX1; break;
  case GEF_VME_INT_MBOX2: tempCtl |= UNIV_LINT__MBOX2; break;
  case GEF_VME_INT_MBOX3: tempCtl |= UNIV_LINT__MBOX3; break;
  case GEF_VME_INT_LM0: tempCtl |= UNIV_LINT__LM0; break;
  case GEF_VME_INT_LM1: tempCtl |= UNIV_LINT__LM1; break;
  case GEF_VME_INT_LM2: tempCtl |= UNIV_LINT__LM2; break;
  case GEF_VME_INT_LM3: tempCtl |= UNIV_LINT__LM3; break;
  default: break;
  }

  iowrite32(tempCtl, vmechip_baseaddr+LINT_EN);
  vmeSyncData();

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniEnableInt: Enabled irq level %d\n", int_info->int_level);

  return(GEF_SUCCESS);
}

// Make sure this function is called with spin locks enabled
int
uniDisableIrq(GEF_VME_INT_INFO *int_info)
{
  GEF_UINT32 tempCtl = 0;

  if (NULL == vmechip_baseaddr) {
    return(GEF_STATUS_DEVICE_NOT_INIT);
  }

  switch(int_info->int_level)
  {
  case GEF_VME_INT_VOWN: break;
  case GEF_VME_INT_VIRQ1: break;
  case GEF_VME_INT_VIRQ2: break;
  case GEF_VME_INT_VIRQ3: break;
  case GEF_VME_INT_VIRQ4: break;
  case GEF_VME_INT_VIRQ5: break;
  case GEF_VME_INT_VIRQ6: break;
  case GEF_VME_INT_VIRQ7: break;
  case GEF_VME_INT_DMA0: break;
  case GEF_VME_INT_LERR: break;
  case GEF_VME_INT_BERR: break;
  case GEF_VME_INT_SW_IACK: break;
  case GEF_VME_INT_SW_INT: break;
  case GEF_VME_INT_SYSFAIL: break;
  case GEF_VME_INT_ACFAIL: break;
  case GEF_VME_INT_MBOX0: break;
  case GEF_VME_INT_MBOX1: break;
  case GEF_VME_INT_MBOX2: break;
  case GEF_VME_INT_MBOX3: break;
  case GEF_VME_INT_LM0: break;
  case GEF_VME_INT_LM1: break;
  case GEF_VME_INT_LM2: break;
  case GEF_VME_INT_LM3: break;
  default: return(GEF_STATUS_NOT_SUPPORTED);
  }

  tempCtl = ioread32(vmechip_baseaddr+LINT_EN);

  switch(int_info->int_level)
  {
  case GEF_VME_INT_VOWN: tempCtl &= ~UNIV_LINT__VOWN; break;
  case GEF_VME_INT_VIRQ1: tempCtl &= ~UNIV_LINT__VIRQ1; break;
  case GEF_VME_INT_VIRQ2: tempCtl &= ~UNIV_LINT__VIRQ2; break;
  case GEF_VME_INT_VIRQ3: tempCtl &= ~UNIV_LINT__VIRQ3; break;
  case GEF_VME_INT_VIRQ4: tempCtl &= ~UNIV_LINT__VIRQ4; break;
  case GEF_VME_INT_VIRQ5: tempCtl &= ~UNIV_LINT__VIRQ5; break;
  case GEF_VME_INT_VIRQ6: tempCtl &= ~UNIV_LINT__VIRQ6; break;
  case GEF_VME_INT_VIRQ7: tempCtl &= ~UNIV_LINT__VIRQ7; break;
  case GEF_VME_INT_DMA0: tempCtl &= ~UNIV_LINT__DMA; break;
  case GEF_VME_INT_LERR: tempCtl &= ~UNIV_LINT__LERR; break;
  case GEF_VME_INT_BERR: 
      
      // Initialize VERR if FPGA not present
      if (NULL == vmic_base) {
          tempCtl &= ~UNIV_LINT__VERR;
      } else {
          // Disable BERR in FPGA
          iowrite32((ioread32(vmic_base + VMIVMEF_COMM) & ~VMIVMEF_COMM__BERRI),
              vmic_base + VMIVMEF_COMM);
      }            
      break;      
      
  case GEF_VME_INT_SW_IACK: tempCtl &= ~UNIV_LINT__SW_IACK; break;
  case GEF_VME_INT_SW_INT: tempCtl &= ~UNIV_LINT__SW_INT; break;
  case GEF_VME_INT_SYSFAIL: tempCtl &= ~UNIV_LINT__SYSFAIL; break;
  case GEF_VME_INT_ACFAIL: tempCtl &= ~UNIV_LINT__ACFAIL; break;
  case GEF_VME_INT_MBOX0: tempCtl &= ~UNIV_LINT__MBOX0; break;
  case GEF_VME_INT_MBOX1: tempCtl &= ~UNIV_LINT__MBOX1; break;
  case GEF_VME_INT_MBOX2: tempCtl &= ~UNIV_LINT__MBOX2; break;
  case GEF_VME_INT_MBOX3: tempCtl &= ~UNIV_LINT__MBOX3; break;
  case GEF_VME_INT_LM0: tempCtl &= ~UNIV_LINT__LM0; break;
  case GEF_VME_INT_LM1: tempCtl &= ~UNIV_LINT__LM1; break;
  case GEF_VME_INT_LM2: tempCtl &= ~UNIV_LINT__LM2; break;
  case GEF_VME_INT_LM3: tempCtl &= ~UNIV_LINT__LM3; break;
  default: break;
  }

  iowrite32(tempCtl, vmechip_baseaddr+LINT_EN);
  vmeSyncData();

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniDisableIrq: Disable irq %d\n", int_info->int_level);

  return(GEF_SUCCESS);
}

int
uniFindMasterWindow(GEF_VME_DRV_CREATE_MASTER_WINDOW_INFO *master_info, GEF_UINT32 ctl, GEF_UINT32 *offset)
{
    GEF_UINT32 lsiCtl = 0;
    GEF_UINT32 lsiBase = 0;
    GEF_UINT32 lsiBound = 0;
    GEF_UINT32 lsiTO = 0;
    GEF_UINT32 i = 0;
    GEF_BOOL   found = GEF_FALSE;
    void *phy_addr = 0;
    GEF_UINT32 vmeAddr = master_info->addr.lower;
    
    if (NULL == vmechip_baseaddr) {
        return(-1);
    }

    for(i=0;i<VME_MAX_WINDOWS;i++)
    {
        if(vmw[i].exclusive)
        {
            GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniFindMasterWindow: skip window %d because it is an exclusive window\n", i);
            continue;
        }

        phy_addr = vmw[i].physAddr;
        lsiCtl = ioread32(vmechip_baseaddr+UNIV_LSI_CTL(i));
        
        if(ctl == lsiCtl)
        {
            lsiBase = ioread32(vmechip_baseaddr+UNIV_LSI_BS(i));
            lsiBound = ioread32(vmechip_baseaddr+UNIV_LSI_BD(i));
            lsiTO = ioread32(vmechip_baseaddr+UNIV_LSI_TO(i));

            /* If this check fails, then either the window handle
               has gotten corrupted, or a user has modified 
               this window behind this driver's back.
            */
            if ((lsiBase != (GEF_UINT32)phy_addr)
                || ((lsiBase + lsiTO) != vmw[i].vmeL)
                || ((lsiBound - lsiBase) != vmw[i].size)) {
#ifdef USE_RTOS
                  rtlogRecord_t logRecord;
                  RTLOG_ERROR("gefvme_rtos", "uniFindMasterWindow: Master window "
                    "%d fails consistancy check", i);
#else
                printk("uniFindMasterWindow: Master window %d fails consistancy check\n", i);
#endif
                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniFindMasterWindow: Expected base=0x%p bound=0x%p to=0x%x\n",
                    phy_addr,
                    phy_addr + master_info->size,
                    master_info->addr.lower - (GEF_UINT32)phy_addr);
                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniFindMasterWindow: Found base=0x%x bound=0x%x to=0x%x\n",
                    lsiBase, lsiBound, lsiTO);
                continue;
            }
            
            if ((master_info->addr.lower >= (lsiBase + lsiTO)) &&
                ((master_info->addr.lower + master_info->size) <= (lsiBound + lsiTO))) {
                found = GEF_TRUE;
                *offset = vmeAddr - vmw[i].vmeL;
                break;
            }
        }
        
        continue;
    }

    if(found == GEF_TRUE)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniFindMasterWindow: Found master window 0x%x\n", i);
        return(i);
    }
    else
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniFindMasterWindow: Did not find master window\n");
        return(-1);
    }
}

int
uniFindSlaveWindow(GEF_VME_DRV_CREATE_SLAVE_WINDOW_INFO *slave_info, GEF_UINT32 ctl, GEF_UINT32 *offset)
{
    GEF_UINT32 vsiCtl = 0;
    GEF_UINT32 vsiBase = 0;
    GEF_UINT32 vsiBound = 0;
    GEF_UINT32 vsiTO = 0;
    GEF_UINT32 i = 0;
    GEF_BOOL   found = GEF_FALSE;
    GEF_UINT32 phy_addr = 0;
    GEF_UINT32 vmeAddr = slave_info->size;
    
    if (NULL == vmechip_baseaddr) {
        return(-1);
    }

    for(i=0;i<VME_MAX_WINDOWS;i++)
    {
        phy_addr = (GEF_UINT32)(vsw[i].physAddr - pci_bus_mem_base_phys(vmechip_bus));
        vsiCtl = ioread32(vmechip_baseaddr+UNIV_VSI_CTL(i));
        
        if(ctl == vsiCtl)
        {
            vsiBase = ioread32(vmechip_baseaddr+UNIV_VSI_BS(i));
            vsiBound = ioread32(vmechip_baseaddr+UNIV_VSI_BD(i));
            vsiTO = ioread32(vmechip_baseaddr+UNIV_VSI_TO(i));

            *offset = vmeAddr - vsiBase;

            /* If this check fails, then either the window handle
               has gotten corrupted, or a user has modified 
               this window behind this driver's back.
            */
            if ((vsiBase != vsw[i].vmeL)
                || ((vsiBase + vsiTO) != phy_addr)
                || ((vsiBound - vsiBase) != vsw[i].size)) {
#ifdef USE_RTOS
                  rtlogRecord_t logRecord;
                  RTLOG_ERROR("gefvme_rtos", "uniFindMasterWindow: Master window "
                    "%d fails consistancy check", i);
#else
                printk("uniFindMasterWindow: Master window %d fails consistancy check\n", i);
#endif
                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniFindSlaveWindow: Expected base=0x%x bound=0x%x to=0x%x\n",
                    vsw[i].vmeL,
                    vsw[i].vmeL + slave_info->size,
                    phy_addr-vsw[i].vmeL);
                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniFindSlaveWindow: Found base=0x%x bound=0x%x to=0x%x\n",
                    vsiBase, vsiBound, vsiTO);
                continue;
            }
            
            if ((slave_info->addr.lower >= vsiBase) &&
                ((slave_info->addr.lower + slave_info->size) <= vsiBound)) {
                found = GEF_TRUE;
                break;
            }
        }
        
        continue;
    }

    if(found == GEF_TRUE)
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniFindSlaveWindow: Found master window 0x%x\n", i);
        return(i);
    }
    else
    {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniFindSlaveWindow: Did not find master window\n");
        return(-1);
    }
}

int
uniCreateMasterWindow(GEF_VME_DRV_CREATE_MASTER_WINDOW_INFO *master_info, GEF_UINT32 ctl, vmeMasterHandle_t *masterhandle)
{
    GEF_UINT32 tempCtl = 0;
    GEF_UINT32 irqflags = 0;
    GEF_UINT32 resolution = 0x1000; /* default to 4K */
    GEF_UINT32 i = 0;
    GEF_UINT32 vme_addr = master_info->addr.lower;
    GEF_UINT32 size = master_info->size;
    GEF_UINT32 existingSize;
    GEF_UINT32 base;
    GEF_UINT32 bound;
    GEF_UINT32 to;
    GEF_UINT32 off;

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniCreateMasterWindow: Entry: Control 0x%x\n", ctl);

    if (NULL == vmechip_baseaddr) {
        return(GEF_STATUS_DEVICE_NOT_INIT);
    }

    if (NULL == masterhandle) {
        return(GEF_STATUS_NO_MEM);
    }

    /* Look for a window that is not already enabled
    */
    for (i=0; i< VME_MASTER_WINDOWS; i++) {
        tempCtl = ioread32(vmechip_baseaddr+UNIV_LSI_CTL(i));
        if(!(tempCtl & UNIV_LSI_CTL__EN)) {
            resolution = (i % 4) ? 0x10000 : 0x1000; /* 64k (1,2,3,5,6,7) or 4k (0,4) */
            
            off = vme_addr % resolution;                                         /* window offset used during read / write */
            size += off;
            size += (size % resolution) ? resolution - (size % resolution) : 0;  /* adjusted size */
            
            // Allocate PCI memory space for this window.
            existingSize = vmw[i].physAddrRes.end - vmw[i].physAddrRes.start;           
            if(existingSize != size){
                
                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniCreateMasterWindow: existingSize (0x%x) != size (0x%x)\n",
                    existingSize, size);
                
                if(existingSize != 0){
                    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniCreateMasterWindow: existingSize (0x%x) != 0\n",
                        existingSize);
                    release_resource(&vmw[i].physAddrRes);
                    memset(&vmw[i].physAddrRes, 0, sizeof(struct resource));
                }
                
                /* PCI Resource */
                if(allocate_resource(vmepcimem, &vmw[i].physAddrRes, 
                    size, vmepcimem->start, vmepcimem->end, resolution, 0,0)) {
                    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniCreateMasterWindow: allocation failed for %x\n", size);
                    return(GEF_STATUS_NO_MEM);
                }
                
                /* map to kernel space and non-cachable */
                vmw[i].virtAddr = 
                    ioremap_nocache(vmw[i].physAddrRes.start, size);
                if(vmw[i].virtAddr == 0){
                    release_resource(&vmw[i].physAddrRes);
                    memset(&vmw[i].physAddrRes, 0, sizeof(struct resource));
                    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniCreateMasterWindow: ioremap failed for start 0x%x and size 0x%x\n", (unsigned int)vmw[i].physAddrRes.start, size);
                    return(GEF_STATUS_NO_MEM);
                }
            }
            
            if(master_info->addr.flags & GEF_VME_WND_EXCLUSIVE)
            {
                vmw[i].exclusive = GEF_TRUE;
            }
            vmw[i].vmeL = vme_addr;
            vmw[i].vmeU = 0;
            vmw[i].size = size;
            masterhandle->offset = off;
            masterhandle->window = &vmw[i];
            vmw[i].vmeAddr = master_info->addr;
            vmw[i].number = i;
            vmw[i].physAddr = (void*)((GEF_UINT32)vmw[i].physAddrRes.start - pci_bus_mem_base_phys(vmechip_bus));
            
            base = (GEF_UINT32)vmw[i].physAddr;
            bound = vmw[i].physAddrRes.end + 1;
            to = vme_addr - base;

            GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniCreateMasterWindow: Window [%d], PCI [0x%p], Virtual [0x%p], Resolution [0x%x], Offset [0x%x]\n", 
                vmw[i].number, vmw[i].physAddr, vmw[i].virtAddr, resolution, masterhandle->offset);
#ifdef USE_RTOS
            irqflags = rt_spin_lock_irqsave(&uni_lock);
#else
            spin_lock_irqsave (&uni_lock, irqflags);
#endif

            iowrite32(ctl, vmechip_baseaddr+UNIV_LSI_CTL(i));
            iowrite32(base, vmechip_baseaddr+UNIV_LSI_BS(i));
            iowrite32(bound, vmechip_baseaddr+UNIV_LSI_BD(i));
            iowrite32(to, vmechip_baseaddr+UNIV_LSI_TO(i));
            vmeSyncData();

            GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniCreateMasterWindow: Window [%d], PCI [0x%p], Virtual [0x%p], Resolution [0x%x]\n", 
                vmw[i].number, vmw[i].physAddr, vmw[i].virtAddr, resolution);

            GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Configuring master window #%d: CTL = %08x\n", i, ioread32(vmechip_baseaddr+UNIV_LSI_CTL(i)));
            GEF_VME_DEBUG(GEF_DEBUG_TRACE, "BS %08x\n", ioread32(vmechip_baseaddr+UNIV_LSI_BS(i)));
            GEF_VME_DEBUG(GEF_DEBUG_TRACE, "BD %08x\n", ioread32(vmechip_baseaddr+UNIV_LSI_BD(i)));
            GEF_VME_DEBUG(GEF_DEBUG_TRACE, "TO %08x\n", ioread32(vmechip_baseaddr+UNIV_LSI_TO(i)));
         
#ifdef USE_RTOS
            rt_spin_unlock_irqrestore(irqflags, &uni_lock);
#else
            spin_unlock_irqrestore (&uni_lock, irqflags);
#endif

            GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniCreateMasterWindow: Exit\n");
            return (GEF_SUCCESS);
        }
            GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniCreateMasterWindow: WARNING: Windows %d not available\n", i);
    }

            GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniCreateMasterWindow: Exit: No memory\n");

    return(GEF_STATUS_NO_MEM);
}

int
uniCreateSlaveWindow(GEF_VME_DRV_CREATE_SLAVE_WINDOW_INFO *slave_info, GEF_UINT32 ctl, GEF_UINT32 *window)
{
    GEF_UINT32 tempCtl = 0;
    GEF_UINT32 irqflags = 0;
    GEF_UINT32 i = 0;
    GEF_UINT32 vme_addr = slave_info->addr.lower;
    GEF_UINT32 base = 0;
    GEF_UINT32 bound = 0;
    GEF_UINT32 to = 0;
    GEF_UINT32 offset = 0;
    GEF_UINT32 resolution = 0;
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniCreateSlaveWindow: Entry: Control 0x%x\n", ctl);
    
    if (NULL == vmechip_baseaddr) {
        return(GEF_STATUS_DEVICE_NOT_INIT);
    }
    
    /* Look for a window that is not already enabled
    */
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
#endif
    for (i=0; i< VME_SLAVE_WINDOWS; i++) {
        tempCtl = ioread32(vmechip_baseaddr+UNIV_VSI_CTL(i));
        if(!(tempCtl & UNIV_VSI_CTL__EN)) {
            
            *window = i;

            /* check that the vme address and size are aligned to the resolution */
            resolution = (i % 4)? 0x10000 : 0x1000; /* 0 and 4 are 4K and 1,2,3,5,6 and 7 are 64K */
            offset = vme_addr % resolution;
            if(offset > 0)
            {
#ifdef USE_RTOS
        RTLOG_ERROR("gefvme_rtos", "uniCreateSlaveWindow: ERROR: The requested "
            "window alignment is invalid, offset 0x%x, vme address 0x%x, "
            "resoultion 0x%x",
#else
                printk("uniCreateSlaveWindow: ERROR: The requested window alignment is invalid, offset 0x%x, vme address 0x%x, resoultion 0x%x\n", 
#endif
                    offset, vme_addr, resolution);
                return (GEF_STATUS_INVAL_ADDR);
            }
            if( (slave_info->size % resolution) > 0 )
            {
#ifdef USE_RTOS
        RTLOG_ERROR("gefvme_rtos", "uniCreateSlaveWindow: ERROR: The requested "
            "window size 0x%x is not aligned to resolution 0x%x",
#else
                printk("uniCreateSlaveWindow: ERROR: The requested window size 0x%x is not aligned to resolution 0x%x\n", 
#endif
                    slave_info->size, resolution);
                return (GEF_STATUS_VME_INVALID_SIZE);
            }
            
            if(vmeAllocSlaveMemory(slave_info->size, *window, vme_addr, 0) == GEF_SUCCESS)
            {
                
                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniCreateSlaveWindow: Window [%d], Physical RAM [0x%p], Virtual RAM [0x%p]\n", 
                    *window, vsw[*window].physAddr, vsw[*window].virtAddr);
                
                base = vme_addr;
                bound = base + slave_info->size;
                to = (GEF_UINT32)vsw[*window].physAddr - base;

#ifdef USE_RTOS
                irqflags = rt_spin_lock_irqsave(&uni_lock);
#else
                spin_lock_irqsave (&uni_lock, irqflags);
#endif

                iowrite32(ctl, vmechip_baseaddr+UNIV_VSI_CTL(i));
                iowrite32(base, vmechip_baseaddr+UNIV_VSI_BS(i));
                iowrite32(bound, vmechip_baseaddr+UNIV_VSI_BD(i));
                iowrite32(to, vmechip_baseaddr+UNIV_VSI_TO(i));
                vmeSyncData();

                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Configuring slave window #%d: CTL = %08x\n", i, ioread32(vmechip_baseaddr+UNIV_VSI_CTL(i)));
                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "BS %08x\n", ioread32(vmechip_baseaddr+UNIV_VSI_BS(i)));
                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "BD %08x\n", ioread32(vmechip_baseaddr+UNIV_VSI_BD(i)));
                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "TO %08x\n", ioread32(vmechip_baseaddr+UNIV_VSI_TO(i)));

#ifdef USE_RTOS
                rt_spin_unlock_irqrestore(irqflags, &uni_lock);
#else
                spin_unlock_irqrestore (&uni_lock, irqflags);
#endif

                GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniCreateSlaveWindow: Exit\n");
                
                return (GEF_SUCCESS);
            }
            else
            {
#ifdef USE_RTOS
                  RTLOG_ERROR("gefvme_rtos", "uniCreateSlaveWindow: ERROR: "
                    "Failed to allocate slave memory");
#else
                printk("uniCreateSlaveWindow: ERROR: Failed to allocate slave memory\n");
#endif
                return (GEF_STATUS_NO_MEM);
            }
        }
    }
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniCreateSlaveWindow: Exit: No windows available\n");
    
    return(GEF_STATUS_NO_MEM);
}

int
uniFillinMasterAttributes(GEF_VME_DRV_CREATE_MASTER_WINDOW_INFO *master_info, GEF_UINT32 *ctl)
{
    *ctl = UNIV_LSI_CTL__EN;

    /* data width */
    switch(master_info->addr.transfer_max_dwidth)
    {
    case GEF_VME_TRANSFER_MAX_DWIDTH_8: *ctl |= UNIV_LSI_CTL__VDW__D8; break;
    case GEF_VME_TRANSFER_MAX_DWIDTH_16: *ctl |= UNIV_LSI_CTL__VDW__D16; break;
    case GEF_VME_TRANSFER_MAX_DWIDTH_32: *ctl |= UNIV_LSI_CTL__VDW__D32; break;
    case GEF_VME_TRANSFER_MAX_DWIDTH_64: *ctl |= UNIV_LSI_CTL__VDW__D64; break;
    default: 
#ifdef USE_RTOS
    {
        rtlogRecord_t logRecord;
        RTLOG_WARNING("gefvme_rtos", "uniFillinMasterAttributes: WARNING: "
            "GEF_VME_TRANSFER_MAX_DWIDTH is not defined in GEF_VME_ADDR so "
            "default to D32");
    }
#else
        printk("uniFillinMasterAttributes: WARNING: GEF_VME_TRANSFER_MAX_DWIDTH is not defined in GEF_VME_ADDR so default to D32\n"); 
#endif
        *ctl |= UNIV_LSI_CTL__VDW__D32;
        break;
    }

    /* address space */
    switch(master_info->addr.addr_space)
    {
    case GEF_VME_ADDR_SPACE_A16: *ctl |= UNIV_LSI_CTL__VAS__A16; break;
    case GEF_VME_ADDR_SPACE_A24: *ctl |= UNIV_LSI_CTL__VAS__A24; break;
    case GEF_VME_ADDR_SPACE_A32: *ctl |= UNIV_LSI_CTL__VAS__A32; break;
    case GEF_VME_ADDR_SPACE_CRCSR: *ctl |= UNIV_LSI_CTL__VAS__CRCSR; break;
    case GEF_VME_ADDR_SPACE_USER1: *ctl |= UNIV_LSI_CTL__VAS__USER1; break;
    case GEF_VME_ADDR_SPACE_USER2: *ctl |= UNIV_LSI_CTL__VAS__USER2; break;
    case GEF_VME_ADDR_SPACE_A64:
    case GEF_VME_ADDR_SPACE_USER3:
    case GEF_VME_ADDR_SPACE_USER4:
    default:
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniFillinMasterAttributes: Invalid address space 0x%x\n", master_info->addr.addr_space);
        return (GEF_STATUS_VME_INVALID_ADDRESS_SPACE);
    }

    if( (master_info->addr.addr_mode & GEF_VME_ADDR_MODE_USER) == GEF_VME_ADDR_MODE_USER)
    {
        *ctl &= ~UNIV_LSI_CTL__SUPER;
    }

    if( (master_info->addr.addr_mode & GEF_VME_ADDR_MODE_SUPER) == GEF_VME_ADDR_MODE_SUPER)
    {
        *ctl |= UNIV_LSI_CTL__SUPER;
    }

    if( (master_info->addr.addr_mode & GEF_VME_ADDR_MODE_DATA) == GEF_VME_ADDR_MODE_DATA)
    {
        *ctl &= ~UNIV_LSI_CTL__PGM;
    }

    if( (master_info->addr.addr_mode & GEF_VME_ADDR_MODE_PROGRAM) == GEF_VME_ADDR_MODE_PROGRAM)
    {
        *ctl |= UNIV_LSI_CTL__PGM;
    }

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniFillinMasterAttributes: Control 0x%x\n", *ctl);

    return (GEF_SUCCESS);
}

int
uniFillinSlaveAttributes(GEF_VME_DRV_CREATE_SLAVE_WINDOW_INFO *slave_info, GEF_UINT32 *ctl)
{
    *ctl = UNIV_VSI_CTL__EN;

    /* address space */
    switch(slave_info->addr.addr_space)
    {
    case GEF_VME_ADDR_SPACE_A16: *ctl |= UNIV_VSI_CTL__VAS__A16; break;
    case GEF_VME_ADDR_SPACE_A24: *ctl |= UNIV_VSI_CTL__VAS__A24; break;
    case GEF_VME_ADDR_SPACE_A32: *ctl |= UNIV_VSI_CTL__VAS__A32; break;
    case GEF_VME_ADDR_SPACE_USER1: *ctl |= UNIV_VSI_CTL__VAS__USER1; break;
    case GEF_VME_ADDR_SPACE_USER2: *ctl |= UNIV_VSI_CTL__VAS__USER2; break;
    case GEF_VME_ADDR_SPACE_CRCSR:
    case GEF_VME_ADDR_SPACE_A64:
    case GEF_VME_ADDR_SPACE_USER3:
    case GEF_VME_ADDR_SPACE_USER4:
    default:
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniFillinSlaveAttributes: Invalid address space 0x%x\n", slave_info->addr.addr_space);
        return (GEF_STATUS_VME_INVALID_ADDRESS_SPACE);
    }

    if( (slave_info->addr.addr_mode & GEF_VME_ADDR_MODE_USER) == GEF_VME_ADDR_MODE_USER)
    {
        *ctl |= UNIV_VSI_CTL__SUPER__USER;
    }

    if( (slave_info->addr.addr_mode & GEF_VME_ADDR_MODE_SUPER) == GEF_VME_ADDR_MODE_SUPER)
    {
        *ctl |= UNIV_VSI_CTL__SUPER__SUPER;
    }

    if( (slave_info->addr.addr_mode & GEF_VME_ADDR_MODE_DATA) == GEF_VME_ADDR_MODE_DATA)
    {
        *ctl |= UNIV_VSI_CTL__PGM__DATA;
    }

    if( (slave_info->addr.addr_mode & GEF_VME_ADDR_MODE_PROGRAM) == GEF_VME_ADDR_MODE_PROGRAM)
    {
        *ctl |= UNIV_VSI_CTL__PGM__PROGRAM;
    }

        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniFillinSlaveAttributes: Control 0x%x\n", *ctl);

    return (GEF_SUCCESS);
}

int
uniReleaseWindow(int window)
{
    GEF_UINT32 tempCtl = 0;
    GEF_UINT32 irqflags = 0;
        
    if (NULL == vmechip_baseaddr) {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniReleaseWindow: Exist - device not initialized\n");
        return(GEF_STATUS_DEVICE_NOT_INIT);
    }

#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&uni_lock);
#else
    spin_lock_irqsave (&uni_lock, irqflags);
#endif

    iowrite32(tempCtl, vmechip_baseaddr+UNIV_LSI_CTL(window));
    iowrite32(tempCtl, vmechip_baseaddr+UNIV_LSI_BS(window));
    iowrite32(tempCtl, vmechip_baseaddr+UNIV_LSI_BD(window));
    iowrite32(tempCtl, vmechip_baseaddr+UNIV_LSI_TO(window));
    vmeSyncData();

#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &uni_lock);
#else
    spin_unlock_irqrestore (&uni_lock, irqflags);
#endif

    return (GEF_SUCCESS);
}

int
uniReleaseSlaveWindow(int window)
{
    GEF_UINT32 tempCtl = 0;
    GEF_UINT32 irqflags = 0;
    
    if (NULL == vmechip_baseaddr) {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uniReleaseSlaveWindow: Exist - device not initialized\n");
        return(GEF_STATUS_DEVICE_NOT_INIT);
    }
    
#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&uni_lock);
#else
    spin_lock_irqsave (&uni_lock, irqflags);
#endif

    iowrite32(tempCtl, vmechip_baseaddr+UNIV_VSI_CTL(window));
    iowrite32(tempCtl, vmechip_baseaddr+UNIV_VSI_BS(window));
    iowrite32(tempCtl, vmechip_baseaddr+UNIV_VSI_BD(window));
    iowrite32(tempCtl, vmechip_baseaddr+UNIV_VSI_TO(window));
    vmeSyncData();
    
#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &uni_lock);
#else
    spin_unlock_irqrestore (&uni_lock, irqflags);
#endif

    return (GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : uniSetupLm
// Description: 
//-----------------------------------------------------------------------------
int
uniSetupLm( vmeLmCfg_t *vmeLm)
{
    int tempCtl = UNIV_LM_CTL__EN;
    GEF_UINT32 irqflags = 0;
    int winCtl = 0;
    
    if (NULL == vmechip_baseaddr) {
        return(-EINVAL);
    }
    
    if(vmeLm->addrU ) {
        return(-EINVAL);
    }
    
    switch(vmeLm->addrSpace){
    case VME_A64:
    case VME_USER3:
    case VME_USER4:
        return(-EINVAL);
    case VME_A16:
        tempCtl |= 0x00000;
        break;
    case VME_A24:
        tempCtl |= 0x10000;
        break;
    case VME_A32:
        tempCtl |= 0x20000;
        break;
    case VME_CRCSR:
        tempCtl |= 0x50000;
        break;
    case VME_USER1:
        tempCtl |= 0x60000;
        break;
    case VME_USER2:
        tempCtl |= 0x70000;
        break;
    }
    
    // Setup CTL register.
    if(vmeLm->userAccessType & VME_SUPER)
        tempCtl |= 0x00200000;
    if(vmeLm->userAccessType & VME_USER)
        tempCtl |= 0x00100000;
    if(vmeLm->dataAccessType & VME_PROG)
        tempCtl |= 0x00800000;
    if(vmeLm->dataAccessType & VME_DATA)
        tempCtl |= 0x00400000;
    
    winCtl = ioread32(vmechip_baseaddr+LM_CTL);
    if(UNIV_LM_CTL__EN & winCtl) {
        
        tempCtl |= winCtl & (UNIV_LM_CTL__PGM__BOTH |
            UNIV_LM_CTL__SUPER__BOTH);
        winCtl |= tempCtl & (UNIV_LM_CTL__PGM__BOTH |
            UNIV_LM_CTL__SUPER__BOTH);
        
        if ((winCtl != tempCtl) ||
            (vmeLm->addr != ioread32(vmechip_baseaddr + UNIV_LM_BS)))
        {
            return -EBUSY;
        }
    }
    
#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&uni_lock);
#else
    spin_lock_irqsave(&uni_lock, irqflags);
#endif

    // Disable while we are modifying
    iowrite32(0x00000000,vmechip_baseaddr+LM_CTL);
    iowrite32(vmeLm->addr, vmechip_baseaddr+LM_BS);
    
    uniLmEvent = 0;
    
    // Write ctl reg and enable
    iowrite32(tempCtl,vmechip_baseaddr+LM_CTL);
    vmeSyncData();
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Configuring loc mon window: CTL = %08x\n", ioread32(vmechip_baseaddr+LM_CTL));
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "BS %08x\n", ioread32(vmechip_baseaddr+LM_BS));
    
#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &uni_lock);
#else
    spin_unlock_irqrestore (&uni_lock, irqflags);
#endif

    return(0);
}

int uniReleaseLocMonWindow(void)
{
    GEF_UINT32 irqflags = 0;

    if (NULL == vmechip_baseaddr) {
        return(-EINVAL);
    }

#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&uni_lock);
#else
    spin_lock_irqsave(&uni_lock, irqflags);
#endif

    // Disable window
    iowrite32(0x00000000,vmechip_baseaddr+LM_CTL);
    iowrite32(0x00000000, vmechip_baseaddr+LM_BS);
    
    vmeSyncData();

#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &uni_lock);
#else
    spin_unlock_irqrestore (&uni_lock, irqflags);
#endif

    return 0;
}

int
uniQueryLocMonWindow(GEF_VME_DRV_LOCATION_MONITOR_INFO *lmInfo)
{
    GEF_UINT32 tempCtl = 0;

    if (NULL == vmechip_baseaddr) {
        return(GEF_STATUS_DEVICE_NOT_INIT);
    }
    
    tempCtl = ioread32(vmechip_baseaddr+LM_CTL);
    lmInfo->addr.lower = ioread32(vmechip_baseaddr+LM_BS);

    switch(tempCtl & UNIV_LM_CTL__VAS__USER2)
    {
    case UNIV_LM_CTL__VAS__A16: lmInfo->addr.addr_space = GEF_VME_ADDR_SPACE_A16; break;
    case UNIV_LM_CTL__VAS__A24: lmInfo->addr.addr_space = GEF_VME_ADDR_SPACE_A24; break;
    case UNIV_LM_CTL__VAS__A32: lmInfo->addr.addr_space = GEF_VME_ADDR_SPACE_A32; break;
    case UNIV_LM_CTL__VAS__USER1: lmInfo->addr.addr_space = GEF_VME_ADDR_SPACE_USER1; break;
    case UNIV_LM_CTL__VAS__USER2: lmInfo->addr.addr_space = GEF_VME_ADDR_SPACE_USER2; break;
    default: return (GEF_STATUS_VME_INVALID_ADDRESS_SPACE);
    }

    if( (tempCtl & UNIV_LM_CTL__SUPER__USER) == UNIV_LM_CTL__SUPER__USER)
    {
        lmInfo->addr.addr_mode |= GEF_VME_ADDR_MODE_USER;
    }

    if( (tempCtl & UNIV_LM_CTL__SUPER__SUPER) == UNIV_LM_CTL__SUPER__SUPER)
    {
        lmInfo->addr.addr_mode |= GEF_VME_ADDR_MODE_SUPER;
    }

    if( (tempCtl & UNIV_LM_CTL__PGM__PROGRAM) == UNIV_LM_CTL__PGM__PROGRAM)
    {
        lmInfo->addr.addr_mode |= GEF_VME_ADDR_MODE_PROGRAM;
    }

    if( (tempCtl & UNIV_LM_CTL__PGM__DATA) == UNIV_LM_CTL__PGM__DATA)
    {
        lmInfo->addr.addr_mode |= GEF_VME_ADDR_MODE_DATA;
    }
    
    return 0;
}

//-----------------------------------------------------------------------------
// Function   : uniSetupVrai
// Description: Set the attributes of the VRAI window (CRGAT)
//-----------------------------------------------------------------------------
int
uniSetupVrai(GEF_VME_DRV_VRAI_INFO *vraiInfo)
{
    GEF_VME_ADDR *addr = &(vraiInfo->addr);
    int tempCtl = UNIV_VRAI_CTL__EN;
    unsigned long irqflags;
    int winCtl = 0;

    if (NULL == vmechip_baseaddr) {
        return(GEF_STATUS_DEVICE_NOT_INIT);
    }

    // Setup CTL register.
    switch(addr->addr_space){
    case GEF_VME_ADDR_SPACE_A16:
        tempCtl |= UNIV_VRAI_CTL__VAS__A16;
        break;
    case GEF_VME_ADDR_SPACE_A24:
        tempCtl |= UNIV_VRAI_CTL__VAS__A24;
        break;
    case GEF_VME_ADDR_SPACE_A32:
        tempCtl |= UNIV_VRAI_CTL__VAS__A32;
        break;
    default:
        return(-EINVAL);
    }

    if(addr->addr_mode & GEF_VME_ADDR_MODE_USER)
        tempCtl |= UNIV_VRAI_CTL__SUPER__USER;
    if(addr->addr_mode & GEF_VME_ADDR_MODE_SUPER)
        tempCtl |= UNIV_VRAI_CTL__SUPER__SUPER;
    if(addr->addr_mode & GEF_VME_ADDR_MODE_DATA)
        tempCtl |= UNIV_VRAI_CTL__PGM__DATA;
    if(addr->addr_mode & GEF_VME_ADDR_MODE_PROGRAM)
        tempCtl |= UNIV_VRAI_CTL__PGM__PROGRAM;

#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&uni_lock);
#else
    spin_lock_irqsave (&uni_lock, irqflags);
#endif

    winCtl = ioread32(vmechip_baseaddr+VRAI_CTL);
    if(UNIV_VRAI_CTL__EN & winCtl) {
        tempCtl |= winCtl & (UNIV_VRAI_CTL__PGM__BOTH |
            UNIV_VRAI_CTL__SUPER__BOTH);
        winCtl |= tempCtl & (UNIV_VRAI_CTL__PGM__BOTH |
            UNIV_VRAI_CTL__SUPER__BOTH);

        if ( ( winCtl != tempCtl ) ||
            ( addr->lower != ioread32(vmechip_baseaddr+VRAI_BS) ) )
        {
#ifdef USE_RTOS
            rt_spin_unlock_irqrestore(irqflags, &uni_lock);
#else
            spin_unlock_irqrestore (&uni_lock, irqflags);
#endif
    return -EBUSY;
        }
    }

    // Disable while we are modifying
    iowrite32(0,vmechip_baseaddr+VRAI_CTL);
    vmeSyncData();

    iowrite32(addr->lower,vmechip_baseaddr+VRAI_BS);

    // Write ctl reg and enable
    iowrite32(tempCtl, vmechip_baseaddr+VRAI_CTL);
    vmeSyncData();

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Configuring VRAI window: ctl = %08x\n", ioread32(vmechip_baseaddr+VRAI_CTL));
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "bs %08x\n", ioread32(vmechip_baseaddr+VRAI_BS));

#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &uni_lock);
#else
    spin_unlock_irqrestore (&uni_lock, irqflags);
#endif

    return(0);
}

//-----------------------------------------------------------------------------
// Function   : uniReleaseVraiWindow
// Description: Disable and release the VRAI window (CRGAT)
//-----------------------------------------------------------------------------
int uniReleaseVraiWindow(void)
{
    GEF_UINT32 irqflags = 0;

    if (NULL == vmechip_baseaddr) {
        return(GEF_STATUS_DEVICE_NOT_INIT);
    }

#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&uni_lock);
#else
    spin_lock_irqsave(&uni_lock, irqflags);
#endif

    // Disable window
    iowrite32(0,vmechip_baseaddr+VRAI_CTL);
    iowrite32(0,vmechip_baseaddr+VRAI_BS);

    vmeSyncData();

#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &uni_lock);
#else
    spin_unlock_irqrestore (&uni_lock, irqflags);
#endif

    return 0;
}

//-----------------------------------------------------------------------------
// Function   : uniQueryVraiWindow
// Description: Return the values used to map the VRAI window (CRGAT)
//-----------------------------------------------------------------------------
int
uniQueryVraiWindow(GEF_VME_DRV_VRAI_INFO *vraiInfo)
{
    GEF_VME_ADDR *addr = &(vraiInfo->addr);
    GEF_UINT32 tempCtl = 0;
    GEF_UINT32 irqflags = 0;

    if (NULL == vmechip_baseaddr) {
        return(GEF_STATUS_DEVICE_NOT_INIT);
    }

#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&uni_lock);
#else
    spin_lock_irqsave(&uni_lock, irqflags);
#endif

    tempCtl = ioread32(vmechip_baseaddr+VRAI_CTL);
    vraiInfo->addr.lower = ioread32(vmechip_baseaddr+VRAI_BS);
    vraiInfo->addr.upper = 0;

#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &uni_lock);
#else
    spin_unlock_irqrestore (&uni_lock, irqflags);
#endif

    switch(tempCtl & 0x70)
    {
    case UNIV_VRAI_CTL__VAS__A16: addr->addr_space = GEF_VME_ADDR_SPACE_A16; break;
    case UNIV_VRAI_CTL__VAS__A24: addr->addr_space = GEF_VME_ADDR_SPACE_A24; break;
    case UNIV_VRAI_CTL__VAS__A32: addr->addr_space = GEF_VME_ADDR_SPACE_A32; break;
    default: return (GEF_STATUS_VME_INVALID_ADDRESS_SPACE);
    }

    if( (tempCtl & UNIV_VRAI_CTL__SUPER__USER) == UNIV_VRAI_CTL__SUPER__USER)
    {
        vraiInfo->addr.addr_mode |= GEF_VME_ADDR_MODE_USER;
    }

    if( (tempCtl & UNIV_VRAI_CTL__SUPER__SUPER) == UNIV_VRAI_CTL__SUPER__SUPER)
    {
        vraiInfo->addr.addr_mode |= GEF_VME_ADDR_MODE_SUPER;
    }

    if( (tempCtl & GEF_VME_ADDR_MODE_PROGRAM) == GEF_VME_ADDR_MODE_PROGRAM)
    {
        vraiInfo->addr.addr_mode |= GEF_VME_ADDR_MODE_PROGRAM;
    }

    if( (tempCtl & GEF_VME_ADDR_MODE_DATA) == GEF_VME_ADDR_MODE_DATA)
    {
        vraiInfo->addr.addr_mode |= GEF_VME_ADDR_MODE_DATA;
    }

    return 0;
}

int
uniTransMasterWindow(GEF_VME_DRV_TRANSLATE_MASTER_WINDOW_INFO *transMasterInfo, vmeMasterWindow_t *masterWindow)
{
    GEF_UINT32 irqflags = 0;
    GEF_UINT32 to = 0;
    GEF_UINT32 base = 0;
    GEF_UINT32 vme_addr = 0;
    GEF_UINT32 size = masterWindow->size;
    GEF_UINT32 off = 0;
    GEF_UINT32 resolution = 0;
    vmeMasterHandle_t *handle = (vmeMasterHandle_t *)(GEF_OS_CAST)transMasterInfo->master_osspec_hdl;
    
    if (NULL == vmechip_baseaddr) {
        return(GEF_STATUS_DEVICE_NOT_INIT);
    }

    if(NULL == masterWindow) {
        return(GEF_STATUS_DRIVER_ERR);
    }

    if(handle == NULL) {
        return(GEF_STATUS_DRIVER_ERR);
    }

    vme_addr = transMasterInfo->addr.lower;
    base = (GEF_UINT32)masterWindow->physAddr;
    to = vme_addr - base;

    resolution = (masterWindow->number % 4) ? 0x10000 : 0x1000; /* 64k (1,2,3,5,6,7) or 4k (0,4) */
    off = vme_addr % resolution;                                  /* master window offset */
    size += off;
    size += (size % resolution) ? resolution - (size % resolution) : 0; /* adjust window size */
    
#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&uni_lock);
#else
    spin_lock_irqsave(&uni_lock, irqflags);
#endif

    iowrite32(to, vmechip_baseaddr+UNIV_LSI_TO(masterWindow->number));

    masterWindow->vmeL = transMasterInfo->addr.lower;
    handle->offset = off;

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Translated master window #%d: CTL = %08x\n", masterWindow->number, ioread32(vmechip_baseaddr+UNIV_LSI_CTL(masterWindow->number)));
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "BS %08x\n", ioread32(vmechip_baseaddr+UNIV_LSI_BS(masterWindow->number)));
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "BD %08x\n", ioread32(vmechip_baseaddr+UNIV_LSI_BD(masterWindow->number)));
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "TO %08x\n", ioread32(vmechip_baseaddr+UNIV_LSI_TO(masterWindow->number)));
    
#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &uni_lock);
#else
    spin_unlock_irqrestore (&uni_lock, irqflags);
#endif

    return(GEF_SUCCESS);
}

#ifdef PPC
#define	SWIZZLE(X) ( ((X & 0xFF000000) >> 24) | ((X & 0x00FF0000) >>  8) | ((X & 0x0000FF00) <<  8) | ((X & 0x000000FF) << 24))
#else
#define	SWIZZLE(X) (X)
#endif

//-----------------------------------------------------------------------------
// Function   : uniDoRmw
// Description: 
//-----------------------------------------------------------------------------
int
uniDoRmw( vmeRmwCfg_t *vmeRmw)
{

  void *rmwPciDataPtr = NULL;
  void *vaDataPtr = NULL;
  unsigned long irqflags;
  GEF_UINT8 rmw8;
  GEF_UINT16 rmw16;
  GEF_UINT32 rmw32;

  if(vmeRmw->targetAddrU ) {
    return(-EINVAL);
  }

  rmwPciDataPtr = (void *)((unsigned int)vmeRmw->mastWindBasePciAddr + 
      (vmeRmw->targetAddr - vmeRmw->mastWindVmeAddrL));

  vaDataPtr = (void *)((unsigned int)vmeRmw->mastWindVirtAddr + 
      (vmeRmw->targetAddr - vmeRmw->mastWindVmeAddrL));

  GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Device Read Modify Write rmwPciDataPtr %p vaDataPtr %p\n",
      rmwPciDataPtr,vaDataPtr); 

#ifdef USE_RTOS
  irqflags = rt_spin_lock_irqsave(&uni_lock);
#else
  spin_lock_irqsave (&uni_lock, irqflags);
#endif

  // Setup the RMW registers.
  iowrite32(0, vmechip_baseaddr+SCYC_CTL);
  iowrite32(SWIZZLE(vmeRmw->enableMask), vmechip_baseaddr+SCYC_EN);
  iowrite32(SWIZZLE(vmeRmw->compareData), vmechip_baseaddr+SCYC_CMP);
  iowrite32(SWIZZLE(vmeRmw->swapData), vmechip_baseaddr+SCYC_SWP);
  iowrite32((int)rmwPciDataPtr, vmechip_baseaddr+SCYC_ADDR);
  iowrite32(1, vmechip_baseaddr+SCYC_CTL);


  // Perform RMW cycle
  switch(vmeRmw->dataWidth)
  {
  case GEF_VME_DWIDTH_D8:
      rmw8 = ioread8(vaDataPtr);
      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Performed Byte RMW cycle 0x%x\n",rmw8);
      break;
      
  case GEF_VME_DWIDTH_D16:
      rmw16 = ioread16(vaDataPtr);
      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Performed 16-bit RMW cycle 0x%x\n",rmw16);
      break;
      
  case GEF_VME_DWIDTH_D32:

      rmw32 = ioread32(vaDataPtr);
      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Performed 32-bit RMW cycle 0x%x\n",rmw32);
      break;
      
  default:
      return(GEF_STATUS_INVAL_ADDR); 
      break;
  }

  vmeSyncData();

  // Disable RMW cycle
  iowrite32(0, vmechip_baseaddr+SCYC_CTL);
  vmeSyncData();

#ifdef USE_RTOS
  rt_spin_unlock_irqrestore(irqflags, &uni_lock);
#else
  spin_unlock_irqrestore (&uni_lock, irqflags);
#endif

  return(0);
}



//-----------------------------------------------------------------------------
// Function   : uniSetupDctlReg
// Description: 
//-----------------------------------------------------------------------------
int
uniSetupDctlReg(
vmeDmaPacket_t *vmeDma,
int *dctlregreturn
)
{
  unsigned int dctlreg = 0x80;
  struct vmeAttr *vmeAttr;

  if(vmeDma->srcBus == VME_DMA_VME) {
    dctlreg=0;
    vmeAttr = &vmeDma->srcVmeAttr;
  } else {
    dctlreg = 0x80000000;
    vmeAttr = &vmeDma->dstVmeAttr;
  }
  
  switch(vmeAttr->maxDataWidth){
    case VME_D8:
      break;
    case VME_D16:
      dctlreg |= 0x00400000;
      break;
    case VME_D32:
      dctlreg |= 0x00800000;
      break;
    case VME_D64:
      dctlreg |= 0x00C00000;
      break;
  }

  switch(vmeAttr->addrSpace){
    case VME_A16:
      break;
    case VME_A24:
      dctlreg |= 0x00010000;

      // If data width is not 64-bit, set VCT = BLTs on VMEbus
      if ((dctlreg & 0x00C00000) != 0x00C00000) {
          dctlreg |= 0x00000002;
      }

      break;
    case VME_A32:
      dctlreg |= 0x00020000;

      // If data width is not 64-bit, set VCT = BLTs on VMEbus
      if ((dctlreg & 0x00C00000) != 0x00C00000) {
          dctlreg |= 0x00000002;
      }

      break;
    case VME_USER1:
      dctlreg |= 0x00060000;
      break;
    case VME_USER2:
      dctlreg |= 0x00070000;
      break;

    case VME_A64:		// not supported in Universe DMA
    case VME_CRCSR:
    case VME_USER3:
    case VME_USER4:
      return(-EINVAL);
      break;
  }
  if(vmeAttr->dataAccessType == VME_PROG){
      dctlreg |= 0x00004000;
  }
  if(vmeAttr->userAccessType == VME_SUPER){
      dctlreg |= 0x00001000;
  }
  if(vmeAttr->xferProtocol != VME_SCT){
      dctlreg |= 0x00000100;
  }

  if (vmeDma->ld64Enabled){
      dctlreg |= 0x00000001;
  }

  *dctlregreturn = dctlreg;
  return(0);
}

//-----------------------------------------------------------------------------
// Function   : uniStartDma
// Description: 
//-----------------------------------------------------------------------------
unsigned int
uniStartDma(
int channel,
unsigned int dgcsreg,
TDMA_Cmd_Packet *vmeLL
)
{
  unsigned int val;
    

  // Setup registers as needed for direct or chained.
  if(dgcsreg & 0x8000000){
     iowrite32(0, vmechip_baseaddr+DTBC);
     iowrite32((unsigned int)vmeLL, vmechip_baseaddr+DCPP);
  } else {
     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Starting: DGCS = %08x\n", dgcsreg);
     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Starting: DVA  = %08x\n", ioread32(&vmeLL->dva));
     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Starting: DLV  = %08x\n", ioread32(&vmeLL->dlv));
     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Starting: DTBC = %08x\n", ioread32(&vmeLL->dtbc));
     GEF_VME_DEBUG(GEF_DEBUG_TRACE, "Starting: DCTL = %08x\n", ioread32(&vmeLL->dctl));
     // Write registers 
     iowrite32(ioread32(&vmeLL->dva), vmechip_baseaddr+DVA);
     iowrite32(ioread32(&vmeLL->dlv), vmechip_baseaddr+DLA);
     iowrite32(ioread32(&vmeLL->dtbc), vmechip_baseaddr+DTBC);
     iowrite32(ioread32(&vmeLL->dctl), vmechip_baseaddr+DCTL);
     iowrite32(0, vmechip_baseaddr+DCPP);
  }
  vmeSyncData();

  // Start the operation
  iowrite32(dgcsreg, vmechip_baseaddr+DGCS);
  vmeSyncData();
#ifdef PPC 
  val = vmeGetTime();
#else
  rdtscl(val);
#endif
  iowrite32(dgcsreg | 0x8000000F, vmechip_baseaddr+DGCS);
  vmeSyncData();
  return(val);
}

//-----------------------------------------------------------------------------
// Function   : uniSetupDma
// Description: 
//-----------------------------------------------------------------------------
TDMA_Cmd_Packet *
uniSetupDma(vmeDmaPacket_t *vmeDma)
{
  vmeDmaPacket_t *vmeCur;
  int	maxPerPage;
  int	currentLLcount;
  TDMA_Cmd_Packet *startLL;
  TDMA_Cmd_Packet *currentLL;
  TDMA_Cmd_Packet *nextLL;
  unsigned int dctlreg=0;

  maxPerPage = PAGESIZE/sizeof(TDMA_Cmd_Packet)-1;
  startLL = (TDMA_Cmd_Packet *)__get_free_pages(GFP_KERNEL, 0);
  if(startLL == 0){ return(startLL); }

  // First allocate pages for descriptors and create linked list
  vmeCur = vmeDma;
  currentLL = startLL;
  currentLLcount = 0;
  while(vmeCur != 0){
     if(vmeCur->pNextPacket != 0){
        currentLL->dcpp = (unsigned int)(currentLL + 1);
        currentLLcount++;
        if(currentLLcount >= maxPerPage){
              currentLL->dcpp = __get_free_pages(GFP_KERNEL,0);
              currentLLcount = 0;
        } 
        currentLL = (TDMA_Cmd_Packet *)currentLL->dcpp;
     } else {
        currentLL->dcpp = (unsigned int)0;
     }
     vmeCur = vmeCur->pNextPacket;
  }
  
  // Next fill in information for each descriptor
  vmeCur = vmeDma;
  currentLL = startLL;
  while(vmeCur != 0){
     if(vmeCur->srcBus == VME_DMA_VME){
       iowrite32(vmeCur->srcAddr, &currentLL->dva);
       iowrite32(vmeCur->dstAddr, &currentLL->dlv);
     } else {
       iowrite32(vmeCur->srcAddr, &currentLL->dlv);
       iowrite32(vmeCur->dstAddr, &currentLL->dva);
     }
     uniSetupDctlReg(vmeCur,&dctlreg);
     iowrite32(dctlreg, &currentLL->dctl);
     iowrite32(vmeCur->byteCount, &currentLL->dtbc);

     currentLL = (TDMA_Cmd_Packet *)currentLL->dcpp;
     vmeCur = vmeCur->pNextPacket;
  }
  
  // Convert Links to PCI addresses.
  currentLL = startLL;
  while(currentLL != 0){
     nextLL = (TDMA_Cmd_Packet *)currentLL->dcpp;
     if(nextLL == 0) {
        iowrite32(1, &currentLL->dcpp);
     } else {
        iowrite32((unsigned int)virt_to_bus(nextLL), &currentLL->dcpp);
     }
     vmeFlushLine(currentLL);
     currentLL = nextLL;
  }
  
  // Return pointer to descriptors list 
  return(startLL);
}

//-----------------------------------------------------------------------------
// Function   : uniFreeDma
// Description: 
//-----------------------------------------------------------------------------
int
uniFreeDma(TDMA_Cmd_Packet *startLL)
{
  TDMA_Cmd_Packet *currentLL;
  TDMA_Cmd_Packet *prevLL;
  TDMA_Cmd_Packet *nextLL;
  unsigned int dcppreg;
  
  // Convert Links to virtual addresses.
  currentLL = startLL;
  while(currentLL != 0){
     dcppreg = ioread32(&currentLL->dcpp);
     dcppreg &= ~6;
     if(dcppreg & 1) {
        currentLL->dcpp = 0;
     } else {
        currentLL->dcpp = (unsigned int)bus_to_virt(dcppreg);
     }
     currentLL = (TDMA_Cmd_Packet *)currentLL->dcpp;
  }

  // Free all pages associated with the descriptors.
  currentLL = startLL;
  prevLL = currentLL;
  while(currentLL != 0){
     nextLL = (TDMA_Cmd_Packet *)currentLL->dcpp;
     if(currentLL+1 != nextLL) {
         free_pages((int)prevLL, 0);
         prevLL = nextLL;
     }
     currentLL = nextLL;
  }

  // Return pointer to descriptors list 
  return(0);
}

//-----------------------------------------------------------------------------
// Function   : uniDoDma
// Description: 
//-----------------------------------------------------------------------------
int
uniDoDma(vmeDmaPacket_t *vmeDma)
{
  unsigned int dgcsreg=0;
  unsigned int dctlreg=0;
  int val;
  char buf[100];
  int channel;
  vmeDmaPacket_t *curDma;
  TDMA_Cmd_Packet *dmaLL;
    
  // Sanity check the VME chain.
  channel = vmeDma->channel_number;
  if(channel > 0) { return(-EINVAL); }
  curDma = vmeDma;
  while(curDma != 0){
    if(curDma->byteCount == 0){ return(-EINVAL); }
    if(curDma->byteCount >= 0x1000000){ return(-EINVAL); }
    if((curDma->srcAddr & 7) != (curDma->dstAddr & 7)) { return(-EINVAL); }
    switch(curDma->srcBus){
      case VME_DMA_PCI:
        if(curDma->dstBus != VME_DMA_VME) { return(-EINVAL); }
      break;
      case VME_DMA_VME:
        if(curDma->dstBus != VME_DMA_PCI) { return(-EINVAL); }
      break;
      default:
        return(-EINVAL);
      break;
    }
    if(uniSetupDctlReg(curDma, &dctlreg) < 0) { 
        return(-EINVAL); 
    }

    curDma = curDma->pNextPacket;
    if(curDma == vmeDma) {          // Endless Loop!
       return(-EINVAL);
    }
  }


  // calculate control register
  if(vmeDma->pNextPacket != 0){
      dgcsreg = 0x8000000;
  } else {
      dgcsreg = 0;
  }
  
  /* Validate/Set up VON value */    
  switch(vmeDma->maxVmeBlockSize)
  {
  case GEF_VME_DMA_VON_0:                          break;
  case GEF_VME_DMA_VON_256:   dgcsreg |= 0x100000; break;
  case GEF_VME_DMA_VON_512:   dgcsreg |= 0x200000; break;
  case GEF_VME_DMA_VON_1024:  dgcsreg |= 0x300000; break;
  case GEF_VME_DMA_VON_2048:  dgcsreg |= 0x400000; break;
  case GEF_VME_DMA_VON_4096:  dgcsreg |= 0x500000; break;
  case GEF_VME_DMA_VON_8192:  dgcsreg |= 0x600000; break;
  case GEF_VME_DMA_VON_16384: dgcsreg |= 0x700000; break;
  default: return (GEF_STATUS_VME_INVALID_VON_VALUE);
  }
  
  /* Validate/Set up VOFF value */
  switch(vmeDma->vmeBackOffTimer)
  {
  case GEF_VME_DMA_VOFF_0us:                        break;
  case GEF_VME_DMA_VOFF_16us:   dgcsreg |= 0x10000; break;
  case GEF_VME_DMA_VOFF_32us:   dgcsreg |= 0x20000; break;
  case GEF_VME_DMA_VOFF_64us:   dgcsreg |= 0x30000; break;
  case GEF_VME_DMA_VOFF_128us:  dgcsreg |= 0x40000; break;
  case GEF_VME_DMA_VOFF_256us:  dgcsreg |= 0x50000; break;
  case GEF_VME_DMA_VOFF_512us:  dgcsreg |= 0x60000; break;
  case GEF_VME_DMA_VOFF_1024us: dgcsreg |= 0x70000; break;
  default: return (GEF_STATUS_VME_INVALID_VOFF_VALUE);
  }
  
  // Setup the dma chain
  dmaLL = uniSetupDma(vmeDma);

  // Start the DMA
  if(dgcsreg & 0x8000000){
      vmeDma->vmeDmaStartTick = 
                      uniStartDma( channel, dgcsreg,
                                 (TDMA_Cmd_Packet *)virt_to_phys(dmaLL));
  } else {
      vmeDma->vmeDmaStartTick = 
                      uniStartDma( channel, dgcsreg, dmaLL);
  }

  if (GEF_FALSE == dma_berr_failure) {
      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "DMA wait_event_interruptible\n");
#ifdef USE_RTOS
    while((!(ioread32(vmechip_baseaddr+DGCS) & 0x800))
    && (!(GEF_TRUE == dma_berr_failure)))
    {
        /* Check every 10us. */
        rt_sleep(nano2count(10000ULL));
    }
#else
      wait_event_interruptible(dma_queue[0], 
          (ioread32(vmechip_baseaddr+DGCS) & 0x800) || (GEF_TRUE == dma_berr_failure) );
#endif
  } else {
      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "DMA Bypassing wait_event_interruptible due to BERR\n");
  }
      GEF_VME_DEBUG(GEF_DEBUG_TRACE, "DMA Thread Resumed\n");

  val = ioread32(vmechip_baseaddr+DGCS);
  iowrite32(val | 0xF00, vmechip_baseaddr+DGCS);

  vmeDma->vmeDmaStatus = 0;
  vmeDma->vmeDmaStopTick = uniDmaIrqTime;
  if(vmeDma->vmeDmaStopTick < vmeDma->vmeDmaStartTick){
     vmeDma->vmeDmaElapsedTime = 
        (0xFFFFFFFF - vmeDma->vmeDmaStartTick) + vmeDma->vmeDmaStopTick;
  } else {
     vmeDma->vmeDmaElapsedTime = 
        vmeDma->vmeDmaStopTick - vmeDma->vmeDmaStartTick;
  }
  vmeDma->vmeDmaElapsedTime -= vmechipIrqOverHeadTicks;
#ifdef PPC
  vmeDma->vmeDmaElapsedTime /= (tb_speed/1000000);
#else
  vmeDma->vmeDmaElapsedTime /= (tb_speed/1000);
#endif

  if (!(val & 0x00000800)) {
    vmeDma->vmeDmaStatus = val & 0x700;
    sprintf(buf,"<<ca91c042>> DMA Error in DMA_uni_irqhandler DGCS=%08X\n",val);  
#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    RTLOG_ERROR("gefvme_rtos", "%s", buf);
#else
    printk(buf);
#endif
    val = ioread32(vmechip_baseaddr+DCPP);
    sprintf(buf,"<<ca91c042>> DCPP=%08X\n",val);  
#ifdef USE_RTOS
    RTLOG_ERROR("gefvme_rtos", "%s", buf);
#else
    printk(buf);
#endif
    val = ioread32(vmechip_baseaddr+DCTL);
    sprintf(buf,"<<ca91c042>> DCTL=%08X\n",val);  
#ifdef USE_RTOS
    RTLOG_ERROR("gefvme_rtos", "%s", buf);
#else
    printk(buf);
#endif
    val = ioread32(vmechip_baseaddr+DTBC);
    sprintf(buf,"<<ca91c042>> DTBC=%08X\n",val);  
#ifdef USE_RTOS
    RTLOG_ERROR("gefvme_rtos", "%s", buf);
#else
    printk(buf);
#endif
    val = ioread32(vmechip_baseaddr+DLA);
    sprintf(buf,"<<ca91c042>> DLA=%08X\n",val);  
#ifdef USE_RTOS
    RTLOG_ERROR("gefvme_rtos", "%s", buf);
#else
    printk(buf);
#endif
    val = ioread32(vmechip_baseaddr+DVA);
    sprintf(buf,"<<ca91c042>> DVA=%08X\n",val);  
#ifdef USE_RTOS
    RTLOG_ERROR("gefvme_rtos", "%s", buf);
#else
    printk(buf);
#endif
  }

  // Free the dma chain
  uniFreeDma(dmaLL);
  
  return(0);
}

//-----------------------------------------------------------------------------
// Function   : uni_shutdown
// Description: Put VME bridge in quiescent state.
//-----------------------------------------------------------------------------
void
uni_shutdown(void )
{
    unsigned long irqflags;
    
    if (!vmechip_baseaddr) {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, "uni_shutdown: vmechip_baseaddr is NULL.\n");
        return;
    }

#ifdef USE_RTOS
    irqflags = rt_spin_lock_irqsave(&uni_lock);
#else
    spin_lock_irqsave (&uni_lock, irqflags);
#endif

    // Clear VRAI Control register
    iowrite32(0, vmechip_baseaddr+VRAI_CTL);
    
     // Turn off Interrupts
    iowrite32(0,vmechip_baseaddr+LINT_EN);                  
    
    // Turn off the windows
    iowrite32(0x00800000,vmechip_baseaddr+LSI0_CTL);     
    iowrite32(0x00800000,vmechip_baseaddr+LSI1_CTL);     
    iowrite32(0x00800000,vmechip_baseaddr+LSI2_CTL);     
    iowrite32(0x00800000,vmechip_baseaddr+LSI3_CTL);     
    iowrite32(0x00F00000,vmechip_baseaddr+VSI0_CTL);     
    iowrite32(0x00F00000,vmechip_baseaddr+VSI1_CTL);     
    iowrite32(0x00F00000,vmechip_baseaddr+VSI2_CTL);     
    iowrite32(0x00F00000,vmechip_baseaddr+VSI3_CTL);     
    if(vmechip_revision >= 2){
        iowrite32(0x00800000,vmechip_baseaddr+LSI4_CTL);     
        iowrite32(0x00800000,vmechip_baseaddr+LSI5_CTL);     
        iowrite32(0x00800000,vmechip_baseaddr+LSI6_CTL);     
        iowrite32(0x00800000,vmechip_baseaddr+LSI7_CTL);     
        iowrite32(0x00F00000,vmechip_baseaddr+VSI4_CTL);     
        iowrite32(0x00F00000,vmechip_baseaddr+VSI5_CTL);     
        iowrite32(0x00F00000,vmechip_baseaddr+VSI6_CTL);     
        iowrite32(0x00F00000,vmechip_baseaddr+VSI7_CTL);     
    }
    
#ifdef USE_RTOS
    rt_spin_unlock_irqrestore(irqflags, &uni_lock);
#else
    spin_unlock_irqrestore (&uni_lock, irqflags);
#endif
}

//-----------------------------------------------------------------------------
// Function   : uni_init()
// Description: 
//-----------------------------------------------------------------------------
int
uni_init(void)
{
    int result;
    unsigned int tmp;
    unsigned int irqOverHeadStart;
    int overHeadTicks;
    GEF_VME_INT_INFO int_info;

    vown_count = 0;

    spin_lock_init(&uni_lock);

    uni_shutdown();
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, " Called uni_shutdown() in uni_init()\n");
    // Write to Misc Register
    // Set VME Bus Time-out
    //   Arbitration Mode
    //   DTACK Enable
    tmp = ioread32(vmechip_baseaddr+MISC_CTL) & 0x0832BFFF;     
    tmp |= 0x76040000;
    iowrite32(tmp,vmechip_baseaddr+MISC_CTL);     
    if(tmp & 0x20000){
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, " vme_syscon=1  in uni_init()\n");
        vme_syscon = 1;
    } else {
        GEF_VME_DEBUG(GEF_DEBUG_TRACE, " vme_syscon=0  in uni_init()\n");
        vme_syscon = 0;
    }

    // Clear DMA status log
    iowrite32(0x00000F00,vmechip_baseaddr+DGCS);     
    // Clear and enable error log
    iowrite32(0x00800000,vmechip_baseaddr+L_CMDERR);     
    // Turn off location monitor
    iowrite32(0x00000000,vmechip_baseaddr+LM_CTL);     

    // Turn off interrupts
    iowrite32(0x00000000,vmechip_baseaddr+LINT_EN);   // Disable interrupts in the Universe first
    iowrite32(0x00FFFFFF,vmechip_baseaddr+LINT_STAT); // Clear Any Pending Interrupts
    iowrite32(0x00000000,vmechip_baseaddr+VINT_EN);   // Disable interrupts in the Universe first

#ifdef USE_RTOS
    rtlogRecord_t logRecord;
    if((result = rt_request_global_irq(
        vmechip_irq, uni_irqhandler)) != 0)
    {
        RTLOG_ERROR("gefvme_rtos", " RTOS-VME: Failure requesting irq %d\n",
            vmechip_irq);
    }
    else
    {
        if((result = rt_request_linux_irq(vmechip_irq,
            linuxPostIRQHandler,
            "VMEBus (ca91c042)",
            vme_pci_dev)) != 0)
        {
            rt_free_global_irq(vmechip_irq);
              RTLOG_ERROR("gefvme_rtos", " RTOS-VME: Failure requesting irq "
                "%d\n", vmechip_irq);
        }
        else
        {
            rt_startup_irq(vmechip_irq);
        }
    }
#else
#ifdef PPC
    result = request_irq(vmechip_irq, uni_irqhandler, IRQF_DISABLED, "VMEBus (ca91c042)", NULL);
#else
    result = request_irq(vmechip_irq, uni_irqhandler, IRQF_SHARED, "VMEBus (ca91c042)", vme_pci_dev);
#endif
#endif

    if (result) {
#ifdef USE_RTOS
        RTLOG_ERROR("gefvme_rtos", "  ca91c042: can't get assigned pci irq "
            "vector %02X", vmechip_irq);
#else
      printk("  ca91c042: can't get assigned pci irq vector %02X\n", vmechip_irq);
#endif
      return(0);
    } else {
      iowrite32(0x0000, vmechip_baseaddr+LINT_MAP0);    // Map all ints to 0
      iowrite32(0x0000, vmechip_baseaddr+LINT_MAP1);    // Map all ints to 0
    }

    ADD_RESOURCE(vme_init_flags, VME_RSRC_INTR);

    // Enable LERR and DMA Interrupts
     
    tmp = 0x00000300;
    iowrite32(tmp, vmechip_baseaddr+LINT_EN);
    
    // Enable the VME BERR (VERR) interrupt
    int_info.int_level = GEF_VME_INT_BERR;
    int_info.int_vector = 0;
    if (uniEnableIrq(&int_info)) {
#ifdef USE_RTOS
        RTLOG_ERROR("gefvme_rtos", "  ca91c042: can't enable BERR interrupt");
#else
        printk("  ca91c042: can't enable BERR interrupt\n");
#endif
        return(0);
    }
    
    if(ioread32(vmechip_baseaddr+PCI_CLASS) != 0x06800002){
        return(0);
    }

    for(tmp = 1; tmp < 0x80000000; tmp = tmp << 1){
      iowrite32(tmp, vmechip_baseaddr+SCYC_EN);
      iowrite32(~tmp, vmechip_baseaddr+SCYC_CMP);
      if(ioread32(vmechip_baseaddr+SCYC_EN) != tmp){
        return(0);
      }
      if(ioread32(vmechip_baseaddr+SCYC_CMP) != ~tmp){
        return(0);
      }
    }

    // do a mail box interrupt to calibrate the interrupt overhead.

    // Enable the VME MBOX1 interrupt
    int_info.int_level = GEF_VME_INT_MBOX1;
    int_info.int_vector = 0;
    if (uniEnableIrq(&int_info)) {
#ifdef USE_RTOS
        RTLOG_ERROR("gefvme_rtos", "  ca91c042: can't enable MBOX1 interrupt");
#else
        printk("  ca91c042: can't enable MBOX1 interrupt\n");
#endif
        return(0);
    }

#ifdef PPC
    irqOverHeadStart = vmeGetTime();
#else
    rdtscl(irqOverHeadStart);
#endif

    iowrite32(0, vmechip_baseaddr+MBOX1);
#ifdef PPC
    for(tmp = 0; tmp < 10; tmp++) { vmeSyncData(); }
    irqOverHeadStart = vmeGetTime();
#else
    for(tmp = 0; tmp < 10; tmp++) {
      udelay(1);
      vmeSyncData();
    }
    rdtscl(irqOverHeadStart);
#endif

    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "time start val 0x%x\n",irqOverHeadStart);
    iowrite32(0, vmechip_baseaddr+MBOX1);

#ifdef PPC
    for(tmp = 0; tmp < 10; tmp++) { vmeSyncData(); }
#else
    for(tmp = 0; tmp < 10; tmp++) {
      udelay(1);
      vmeSyncData();
    }
#endif

    overHeadTicks = uniIrqTime - irqOverHeadStart;
    if(overHeadTicks > 0){
       vmechipIrqOverHeadTicks = overHeadTicks;
    } else {
       vmechipIrqOverHeadTicks = 1;
    }
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "vmechipIrqOverHeadTicks %x \n", vmechipIrqOverHeadTicks);

    // Disable the VME MBOX1 interrupt
    int_info.int_level = GEF_VME_INT_MBOX1;
    int_info.int_vector = 0;
    if (uniDisableIrq(&int_info)) {
#ifdef USE_RTOS
        RTLOG_ERROR("gefvme_rtos", "  ca91c042: can't disable MBOX1 interrupt");
#else
        printk("  ca91c042: can't disable MBOX1 interrupt\n");
#endif
        return(0);
    }

    return(1);
}
