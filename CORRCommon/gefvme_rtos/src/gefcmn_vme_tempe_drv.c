/*
* File:
*    gefcmn_vme_temp_drv.c
*
* Description:
*    Source code to the VME device platform switch.
*
* Copyright:
*    Copyright 2007 GE Fanuc Embedded Systems, Inc.
*
* License:
*    This file is proprietary to GE Fanuc Embedded Systems, Inc.
*
*
* $Date$
* $Rev: 6466 $
*/

#include "gef_kernel_abs.h"

/* All registers above 0x100 must be byteswapped in the Tempe on x86 platforms */
#define longswap(x) ((GEF_UINT32) ( \
                     ((x<<24) & 0xff000000) | \
                     ((x<<8)  & 0x00ff0000) | \
                     ((x>>8)  & 0x0000ff00) | \
                     ((x>>24) & 0x000000ff)  \
                    ))

//-----------------------------------------------------------------------------
// Function   : tempeSetArbiter
// Description: Set the VME bus arbiter with the requested attributes
//-----------------------------------------------------------------------------
GEF_STATUS tempeSetArbiter(GEF_VME_DRV_BUS_ARB_INFO *bus_arb_info)
{
    GEF_UINT32 tempCtl = 0;
    
    // Validate parameters
    switch(bus_arb_info->arbitration_mode)
    {
    case GEF_VME_BUS_ARB_PRIORITY: 
    case GEF_VME_BUS_ARB_ROUND_ROBIN: 
        break;
    default: return(GEF_STATUS_NOT_SUPPORTED);
    }
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeSetArbiter: Set arbitration mode to %d\n", bus_arb_info->arbitration_mode);

    gefTempeSpinlockAcquire();
    
    tempCtl = longswap(GEF_IOREAD32(TEMPE_VCTRL));
    
    switch(bus_arb_info->arbitration_mode)
    {
    case GEF_VME_BUS_ARB_PRIORITY: tempCtl &= ~(1 << 6); break;
    case GEF_VME_BUS_ARB_ROUND_ROBIN: tempCtl |= (1 << 6); break;
    default: break;
    }
    
    GEF_IOWRITE32(longswap(tempCtl),TEMPE_VCTRL);
    GEF_VME_IO_SYNC();
    
    gefTempeSpinlockRelease();
    
    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : tempeGetArbiter
// Description: Return the attributes of the VME bus arbiter.
//-----------------------------------------------------------------------------
GEF_STATUS tempeGetArbiter(GEF_VME_DRV_BUS_ARB_INFO *bus_arb_info)
{
    GEF_UINT32 tempCtl = 0;
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeGetArbiter: Get arbitration mode to %d\n", bus_arb_info->arbitration_mode);
    
    tempCtl = longswap(GEF_IOREAD32(TEMPE_VCTRL));
    
    if(tempCtl & (1 << 6)){
        bus_arb_info->arbitration_mode = GEF_VME_BUS_ARB_ROUND_ROBIN;
    } else {
        bus_arb_info->arbitration_mode = GEF_VME_BUS_ARB_PRIORITY;
    }
    
    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : tempeSetReleaseMode
// Description:
//-----------------------------------------------------------------------------
GEF_STATUS tempeSetReleaseMode(GEF_VME_DRV_BUS_REL_MODE_INFO *bus_release_info)
{
    GEF_UINT32 tempCtl = 0;
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeSetReleaseMode: Set release mode to %d\n", bus_release_info->release_mode);
    
    /* validate parameters */
    switch(bus_release_info->release_mode)
    {
    case GEF_VME_BUS_RELEASE_WHEN_DONE:
    case GEF_VME_BUS_RELEASE_ON_REQUEST:
    case GEF_VME_BUS_RELEASE_WHEN_DONE_OR_REQUEST:
    case GEF_VME_BUS_RELEASE_WHEN_DONE_OR_BCLR: 
        break;
    default: return(GEF_STATUS_NOT_SUPPORTED);
    }
    
    gefTempeSpinlockAcquire();
    
    tempCtl = longswap(GEF_IOREAD32(TEMPE_VMCTRL));
    
    tempCtl = (tempCtl & ~(0x03<<3));
    
    switch(bus_release_info->release_mode)
    {
    case GEF_VME_BUS_RELEASE_WHEN_DONE:  
    case GEF_VME_BUS_RELEASE_ON_REQUEST: 
    case GEF_VME_BUS_RELEASE_WHEN_DONE_OR_REQUEST: 
    case GEF_VME_BUS_RELEASE_WHEN_DONE_OR_BCLR: 
        tempCtl |= (bus_release_info->release_mode<<3); 
        break; 
    default: break;
    }
    
    GEF_IOWRITE32(longswap(tempCtl),TEMPE_VMCTRL);
    GEF_VME_IO_SYNC();
    
    gefTempeSpinlockRelease();
    
    return(GEF_SUCCESS);  
}

//-----------------------------------------------------------------------------
// Function   : tempeGetReleaseMode
// Description:
//-----------------------------------------------------------------------------
GEF_STATUS tempeGetReleaseMode(GEF_VME_DRV_BUS_REL_MODE_INFO *bus_release_info)
{
    GEF_UINT32 tempCtl = 0;
    
    tempCtl = longswap(GEF_IOREAD32(TEMPE_VMCTRL));
    
    bus_release_info->release_mode = ((tempCtl>>3) & 0x03);
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeGetReleaseMode: Get release mode: %d\n", bus_release_info->release_mode);
    
    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : tempeSetReleaseMode
// Description:
//-----------------------------------------------------------------------------
GEF_STATUS tempeSetRequestMode(GEF_VME_DRV_BUS_REQ_MODE_INFO *bus_request_info)
{
    GEF_UINT32 tempCtl = 0;
     
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeSetRequestMode: Set request mode to %d\n", bus_request_info->request_mode);

    /* validate parameters */
    switch(bus_request_info->request_mode)
    {
    case GEF_VME_BUS_REQUEST_DEMAND: break;
    case GEF_VME_BUS_REQUEST_FAIR: break;
    default: return(GEF_STATUS_NOT_SUPPORTED);
    }
    
    gefTempeSpinlockAcquire();
    
    tempCtl = longswap(GEF_IOREAD32(TEMPE_VMCTRL));
    
    switch(bus_request_info->request_mode)
    {
    case GEF_VME_BUS_REQUEST_DEMAND: tempCtl &=  ~(1 << 2); break;
    case GEF_VME_BUS_REQUEST_FAIR: tempCtl |=  (1 << 2); break;
    default: break;
    }
    
    GEF_IOWRITE32(longswap(tempCtl),TEMPE_VMCTRL);
    GEF_VME_IO_SYNC();
    
    gefTempeSpinlockRelease();
    
    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : tempeGetRequestMode
// Description:
//-----------------------------------------------------------------------------
GEF_STATUS tempeGetRequestMode(GEF_VME_DRV_BUS_REQ_MODE_INFO *bus_request_info)
{
    GEF_UINT32 tempCtl = 0;
    
    tempCtl = longswap(GEF_IOREAD32(TEMPE_VMCTRL));
    
    if(tempCtl & (1 << 2)){
        bus_request_info->request_mode = GEF_VME_BUS_REQUEST_FAIR;
    } else {
        bus_request_info->request_mode = GEF_VME_BUS_REQUEST_DEMAND;
    }
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeGetRequestMode: Get request mode: %d\n", bus_request_info->request_mode);

    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : tempeSetRequestLevel
// Description:
//-----------------------------------------------------------------------------
GEF_STATUS tempeSetRequestLevel(GEF_VME_DRV_BUS_REQ_LEVEL_INFO *bus_req_level_info)
{
    GEF_UINT32 tempCtl = 0;
       
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeSetRequestLevel: Set request level to %d\n", bus_req_level_info->request_level);
   
    /* validate parameters */
    switch(bus_req_level_info->request_level)
    {
    case GEF_VME_BUS_REQUEST_LEVEL_0: break;
    case GEF_VME_BUS_REQUEST_LEVEL_1: break;
    case GEF_VME_BUS_REQUEST_LEVEL_2: break;
    case GEF_VME_BUS_REQUEST_LEVEL_3: break;
    default: return(GEF_STATUS_NOT_SUPPORTED);
    }
    
    gefTempeSpinlockAcquire();
    
    tempCtl = longswap(GEF_IOREAD32(TEMPE_VMCTRL));
    
    tempCtl = (tempCtl & ~(0x03));
    
    switch(bus_req_level_info->request_level)
    {
    case GEF_VME_BUS_REQUEST_LEVEL_0:
    case GEF_VME_BUS_REQUEST_LEVEL_1:
    case GEF_VME_BUS_REQUEST_LEVEL_2:
    case GEF_VME_BUS_REQUEST_LEVEL_3:
        tempCtl |= (bus_req_level_info->request_level); 
        break; 
    default: break;
    }
    
    GEF_IOWRITE32(longswap(tempCtl),TEMPE_VMCTRL);
    GEF_VME_IO_SYNC();
    
    gefTempeSpinlockRelease();
    
    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : tempeGetRequestLevel
// Description:
//-----------------------------------------------------------------------------
GEF_STATUS tempeGetRequestLevel(GEF_VME_DRV_BUS_REQ_LEVEL_INFO *bus_req_level_info)
{
    GEF_UINT32 tempCtl = 0;
    
    tempCtl = longswap(GEF_IOREAD32(TEMPE_VMCTRL));
    
    bus_req_level_info->request_level = (tempCtl & 0x03);
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeGetRequestLevel: Get request level: %d\n", bus_req_level_info->request_level);

    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : tempeSetBusTimeout
// Description:
//-----------------------------------------------------------------------------
GEF_STATUS tempeSetBusTimeout(GEF_VME_DRV_BUS_TIMEOUT_INFO *bus_timeout_info)
{
    GEF_UINT32 tempCtl = 0;
    GEF_UINT32 vbto = 0;
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeSetBusTimeout: Set bus timeout to %d\n", bus_timeout_info->timeout);
    
    /* validate parameters */
    switch(bus_timeout_info->timeout)
    {
    case GEF_VME_BUS_TIMEOUT_DISABLE: break;
    case GEF_VME_BUS_TIMEOUT_8us: break;
    case GEF_VME_BUS_TIMEOUT_16us: break;
    case GEF_VME_BUS_TIMEOUT_32us: break;
    case GEF_VME_BUS_TIMEOUT_64us: break;
    case GEF_VME_BUS_TIMEOUT_128us: break;
    case GEF_VME_BUS_TIMEOUT_256us: break;
    case GEF_VME_BUS_TIMEOUT_512us: break;
    case GEF_VME_BUS_TIMEOUT_1024us: break;
    case GEF_VME_BUS_TIMEOUT_2048us: break;
    default: return(GEF_STATUS_NOT_SUPPORTED);
    }
    
    gefTempeSpinlockAcquire();
    
    tempCtl = longswap(GEF_IOREAD32(TEMPE_VCTRL));
    
    tempCtl = (tempCtl & ~(0xF));
    
    switch(bus_timeout_info->timeout)
    {
    case GEF_VME_BUS_TIMEOUT_DISABLE: vbto = TEMPE_VCTRL_GTO_DISABLED; break;
    case GEF_VME_BUS_TIMEOUT_8us: vbto = TEMPE_VCTRL_GTO_8us; break;
    case GEF_VME_BUS_TIMEOUT_16us: vbto = TEMPE_VCTRL_GTO_16us; break;
    case GEF_VME_BUS_TIMEOUT_32us: vbto = TEMPE_VCTRL_GTO_32us; break;
    case GEF_VME_BUS_TIMEOUT_64us: vbto = TEMPE_VCTRL_GTO_64us; break;
    case GEF_VME_BUS_TIMEOUT_128us: vbto = TEMPE_VCTRL_GTO_128us; break;
    case GEF_VME_BUS_TIMEOUT_256us: vbto = TEMPE_VCTRL_GTO_256us; break;
    case GEF_VME_BUS_TIMEOUT_512us: vbto = TEMPE_VCTRL_GTO_512us; break;
    case GEF_VME_BUS_TIMEOUT_1024us: vbto = TEMPE_VCTRL_GTO_1024us; break;
    case GEF_VME_BUS_TIMEOUT_2048us: vbto = TEMPE_VCTRL_GTO_2048us; break;
    default: break;
    }
    
    tempCtl = (tempCtl | vbto);
    
    GEF_IOWRITE32(longswap(tempCtl),TEMPE_VCTRL);
    GEF_VME_IO_SYNC();
    
    gefTempeSpinlockRelease();
    
    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : tempeGetBusTimeout
// Description:
//-----------------------------------------------------------------------------
GEF_STATUS tempeGetBusTimeout(GEF_VME_DRV_BUS_TIMEOUT_INFO *bus_timeout_info)
{
    GEF_UINT32 tempCtl = 0;
    
    tempCtl = longswap(GEF_IOREAD32(TEMPE_VCTRL));
    
    bus_timeout_info->timeout = (tempCtl & 0xF);
    
    switch(bus_timeout_info->timeout)
    {
    case TEMPE_VCTRL_GTO_DISABLED: bus_timeout_info->timeout = GEF_VME_BUS_TIMEOUT_DISABLE; break;
    case TEMPE_VCTRL_GTO_16us: bus_timeout_info->timeout = GEF_VME_BUS_TIMEOUT_16us; break;
    case TEMPE_VCTRL_GTO_32us: bus_timeout_info->timeout = GEF_VME_BUS_TIMEOUT_32us; break;
    case TEMPE_VCTRL_GTO_64us: bus_timeout_info->timeout = GEF_VME_BUS_TIMEOUT_64us; break;
    case TEMPE_VCTRL_GTO_128us: bus_timeout_info->timeout = GEF_VME_BUS_TIMEOUT_128us; break;
    case TEMPE_VCTRL_GTO_256us: bus_timeout_info->timeout = GEF_VME_BUS_TIMEOUT_256us; break;
    case TEMPE_VCTRL_GTO_512us: bus_timeout_info->timeout = GEF_VME_BUS_TIMEOUT_512us; break;
    case TEMPE_VCTRL_GTO_1024us: bus_timeout_info->timeout = GEF_VME_BUS_TIMEOUT_1024us; break;
    case TEMPE_VCTRL_GTO_2048us: bus_timeout_info->timeout = GEF_VME_BUS_TIMEOUT_2048us; break;
    default: break;
    }
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeGetBusTimeout: bus timeout: %d\n", bus_timeout_info->timeout);

    return(GEF_SUCCESS);
}


//-----------------------------------------------------------------------------
// Function   : tempeSetArbTimeout
// Description:
//-----------------------------------------------------------------------------
GEF_STATUS tempeSetArbTimeout(GEF_VME_DRV_ARB_TIMEOUT_INFO *arb_timeout_info)
{
    GEF_UINT32 tempCtl = 0;
    
    GEF_UINT32 varbto = 0;
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeSetArbTimeout: Set arbitration timeout to %d\n", arb_timeout_info->arb_timeout);
   
    /* validate parameters */
    switch(arb_timeout_info->arb_timeout)
    {
    case GEF_VME_ARB_TIMEOUT_DISABLE: break;
    case GEF_VME_ARB_TIMEOUT_16us: break;
    default: return(GEF_STATUS_NOT_SUPPORTED);
    }
    
    gefTempeSpinlockAcquire();
    
    tempCtl = longswap(GEF_IOREAD32(TEMPE_VCTRL));
    
    tempCtl = (tempCtl & ~(0x80));
    
    switch(arb_timeout_info->arb_timeout)
    {
    case GEF_VME_ARB_TIMEOUT_DISABLE: varbto = 0x00; break;
    case GEF_VME_ARB_TIMEOUT_16us: varbto = 0x80; break;
    default: break;
    }
    
    tempCtl = (tempCtl | varbto);
    
    GEF_IOWRITE32(longswap(tempCtl),TEMPE_VCTRL);
    GEF_VME_IO_SYNC();
    
    gefTempeSpinlockRelease();
    
    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : tempeGetArbTimeout
// Description:
//-----------------------------------------------------------------------------
GEF_STATUS tempeGetArbTimeout(GEF_VME_DRV_ARB_TIMEOUT_INFO *arb_timeout_info)
{
    GEF_UINT32 tempCtl = 0;
   
    tempCtl = longswap(GEF_IOREAD32(TEMPE_VCTRL));
    
    arb_timeout_info->arb_timeout = (tempCtl & 0x80);
    
    switch(arb_timeout_info->arb_timeout)
    {
    case 0x00: arb_timeout_info->arb_timeout = GEF_VME_ARB_TIMEOUT_DISABLE; break;
    case 0x80: arb_timeout_info->arb_timeout = GEF_VME_ARB_TIMEOUT_16us; break;
    default: return(GEF_STATUS_NOT_SUPPORTED);
    }
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeGetArbTimeout: arbitration timeout: %d\n", arb_timeout_info->arb_timeout);

    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : tempeSetBroadcastId
// Description:
//-----------------------------------------------------------------------------
GEF_STATUS tempeSetBroadcastId(GEF_VME_DRV_BUS_BROADCAST_INFO *broadcast_id_info)
{
    GEF_UINT32 tempCtl = 0;
    
    GEF_UINT32 bid = 0;
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeSetBroadcast: Set broadcast id to %d\n", broadcast_id_info->broadcast_id);
   
    /* validate parameters */
    switch(broadcast_id_info->broadcast_id)
    {
    case GEF_VME_BROADCAST_ID_DISABLE: break;
    case GEF_VME_BROADCAST_ID_1: break;
    case GEF_VME_BROADCAST_ID_2: break;
    case GEF_VME_BROADCAST_ID_3: break;
    case GEF_VME_BROADCAST_ID_4: break;
    case GEF_VME_BROADCAST_ID_5: break;
    case GEF_VME_BROADCAST_ID_6: break;
    case GEF_VME_BROADCAST_ID_7: break;
    case GEF_VME_BROADCAST_ID_8: break;
    case GEF_VME_BROADCAST_ID_9: break;
    case GEF_VME_BROADCAST_ID_10: break;
    case GEF_VME_BROADCAST_ID_11: break;
    case GEF_VME_BROADCAST_ID_12: break;
    case GEF_VME_BROADCAST_ID_13: break;
    case GEF_VME_BROADCAST_ID_14: break;
    case GEF_VME_BROADCAST_ID_15: break;
    case GEF_VME_BROADCAST_ID_16: break;
    case GEF_VME_BROADCAST_ID_17: break;
    case GEF_VME_BROADCAST_ID_18: break;
    case GEF_VME_BROADCAST_ID_19: break;
    case GEF_VME_BROADCAST_ID_20: break;
    case GEF_VME_BROADCAST_ID_21: break;
    default: return(GEF_STATUS_NOT_SUPPORTED);
    }
    
    gefTempeSpinlockAcquire();
    
    tempCtl = longswap(GEF_IOREAD32(TEMPE_VCTRL));
    
    tempCtl = tempCtl & ~(0x1F00);
    
    switch(broadcast_id_info->broadcast_id)
    {
    case GEF_VME_BROADCAST_ID_DISABLE: bid = 0x00; break;
    case GEF_VME_BROADCAST_ID_1: bid = 0x01; break;
    case GEF_VME_BROADCAST_ID_2: bid = 0x02; break;
    case GEF_VME_BROADCAST_ID_3: bid = 0x03; break;
    case GEF_VME_BROADCAST_ID_4: bid = 0x04; break;
    case GEF_VME_BROADCAST_ID_5: bid = 0x05; break;
    case GEF_VME_BROADCAST_ID_6: bid = 0x06; break;
    case GEF_VME_BROADCAST_ID_7: bid = 0x07; break;
    case GEF_VME_BROADCAST_ID_8: bid = 0x08; break;
    case GEF_VME_BROADCAST_ID_9: bid = 0x09; break;
    case GEF_VME_BROADCAST_ID_10: bid = 0x0A; break;
    case GEF_VME_BROADCAST_ID_11: bid = 0x0B; break;
    case GEF_VME_BROADCAST_ID_12: bid = 0x0C; break;
    case GEF_VME_BROADCAST_ID_13: bid = 0x0D; break;
    case GEF_VME_BROADCAST_ID_14: bid = 0x0E; break;
    case GEF_VME_BROADCAST_ID_15: bid = 0x0F; break;
    case GEF_VME_BROADCAST_ID_16: bid = 0x10; break;
    case GEF_VME_BROADCAST_ID_17: bid = 0x11; break;
    case GEF_VME_BROADCAST_ID_18: bid = 0x12; break;
    case GEF_VME_BROADCAST_ID_19: bid = 0x13; break;
    case GEF_VME_BROADCAST_ID_20: bid = 0x14; break;
    case GEF_VME_BROADCAST_ID_21: bid = 0x15; break;
    default: break;
    }
    
    tempCtl = (tempCtl | (bid<<8));
    
    GEF_IOWRITE32(longswap(tempCtl),TEMPE_VCTRL);
    GEF_VME_IO_SYNC();
    
    gefTempeSpinlockRelease();
    
    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : tempeGetBroadCastId
// Description:
//-----------------------------------------------------------------------------
GEF_STATUS tempeGetBroadcastId(GEF_VME_DRV_BUS_BROADCAST_INFO *broadcast_id_info)
{
    GEF_UINT32 tempCtl = 0;
    
    tempCtl = longswap(GEF_IOREAD32(TEMPE_VCTRL));
    
    broadcast_id_info->broadcast_id = ((tempCtl>>8) & 0x1F);
    
    switch(broadcast_id_info->broadcast_id)
    {
    case 0x00: broadcast_id_info->broadcast_id = GEF_VME_BROADCAST_ID_DISABLE; break;
    case 0x01: broadcast_id_info->broadcast_id = GEF_VME_BROADCAST_ID_1; break;
    case 0x02: broadcast_id_info->broadcast_id = GEF_VME_BROADCAST_ID_2; break;
    case 0x03: broadcast_id_info->broadcast_id = GEF_VME_BROADCAST_ID_3; break;
    case 0x04: broadcast_id_info->broadcast_id = GEF_VME_BROADCAST_ID_4; break;
    case 0x05: broadcast_id_info->broadcast_id = GEF_VME_BROADCAST_ID_5; break;
    case 0x06: broadcast_id_info->broadcast_id = GEF_VME_BROADCAST_ID_6; break;
    case 0x07: broadcast_id_info->broadcast_id = GEF_VME_BROADCAST_ID_7; break;
    case 0x08: broadcast_id_info->broadcast_id = GEF_VME_BROADCAST_ID_8; break;
    case 0x09: broadcast_id_info->broadcast_id = GEF_VME_BROADCAST_ID_9; break;
    case 0x0A: broadcast_id_info->broadcast_id = GEF_VME_BROADCAST_ID_10; break;
    case 0x0B: broadcast_id_info->broadcast_id = GEF_VME_BROADCAST_ID_11; break;
    case 0x0C: broadcast_id_info->broadcast_id = GEF_VME_BROADCAST_ID_12; break;
    case 0x0D: broadcast_id_info->broadcast_id = GEF_VME_BROADCAST_ID_13; break;
    case 0x0E: broadcast_id_info->broadcast_id = GEF_VME_BROADCAST_ID_14; break;
    case 0x0F: broadcast_id_info->broadcast_id = GEF_VME_BROADCAST_ID_15; break;
    case 0x10: broadcast_id_info->broadcast_id = GEF_VME_BROADCAST_ID_16; break;
    case 0x11: broadcast_id_info->broadcast_id = GEF_VME_BROADCAST_ID_17; break;
    case 0x12: broadcast_id_info->broadcast_id = GEF_VME_BROADCAST_ID_18; break;
    case 0x13: broadcast_id_info->broadcast_id = GEF_VME_BROADCAST_ID_19; break;
    case 0x14: broadcast_id_info->broadcast_id = GEF_VME_BROADCAST_ID_20; break;
    case 0x15: broadcast_id_info->broadcast_id = GEF_VME_BROADCAST_ID_21; break;
    default: return(GEF_STATUS_NOT_SUPPORTED);
    }
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeGetBroadcast: broadcast is: %d\n", broadcast_id_info->broadcast_id);

    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : tempeSetSysFail
// Description:
//-----------------------------------------------------------------------------
GEF_STATUS tempeSetSysFail(GEF_VME_DRV_SYSFAIL_INFO *sysfail_info)
{
    GEF_UINT32 tempCtl = 0;
        
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeSetSysFail: Set sysfail to %d\n", sysfail_info->sysfail);
    
    gefTempeSpinlockAcquire();
    
    tempCtl = tempCtl | (sysfail_info->sysfail<<15);
    GEF_IOWRITE32(longswap(tempCtl),TEMPE_VCTRL);
    GEF_VME_IO_SYNC();
    
    gefTempeSpinlockRelease();
    
    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : tempeGetSysFail
// Description:
//-----------------------------------------------------------------------------
GEF_STATUS tempeGetSysFail(GEF_VME_DRV_SYSFAIL_INFO *sysfail_info)
{
    GEF_UINT32 vstat = 0;
    
    vstat = longswap(GEF_IOREAD32(TEMPE_VSTAT));

	if ((vstat & TEMPE_VSTAT_SYSFLS) == 0)
	{
		sysfail_info->sysfail = GEF_FALSE;
	}
	else
	{
		sysfail_info->sysfail = GEF_TRUE;
	}
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeGetSysFail: sysfail is: %d\n", sysfail_info->sysfail);

    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : tempeSetMaxRetry
// Description:
//-----------------------------------------------------------------------------
GEF_STATUS tempeSetMaxRetry(GEF_VME_MAX_RETRY_INFO *max_retry_info)
{
    GEF_UINT32 tempCtl = 0;
    
    GEF_UINT32 maxretry = 0;
    GEF_UINT32 ret = GEF_SUCCESS;
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeSetMaxRetry: Set max retry to %d\n", max_retry_info->max_retry);
    
    /* validate parameters */
    switch(max_retry_info->max_retry)
    {
    case GEF_VME_MAX_RETRY_INFINITE: break;
    case GEF_VME_MAX_RETRY_COUNT_TEST: break;
    case GEF_VME_MAX_RETRY_64: break;
    case GEF_VME_MAX_RETRY_128: break;
    case GEF_VME_MAX_RETRY_192: break;
    case GEF_VME_MAX_RETRY_256: break;
    case GEF_VME_MAX_RETRY_320: break;
    case GEF_VME_MAX_RETRY_384: break;
    case GEF_VME_MAX_RETRY_448: break;
    case GEF_VME_MAX_RETRY_512: break;
    case GEF_VME_MAX_RETRY_576: break;
    case GEF_VME_MAX_RETRY_640: break;
    case GEF_VME_MAX_RETRY_704: break;
    case GEF_VME_MAX_RETRY_768: break;
    case GEF_VME_MAX_RETRY_832: break;
    case GEF_VME_MAX_RETRY_896: break;
    case GEF_VME_MAX_RETRY_960: break;
    case GEF_VME_MAX_RETRY_MAX: break;  
    default: return(GEF_STATUS_NOT_SUPPORTED);
    }
    
    gefTempeSpinlockAcquire();
    
    tempCtl = longswap(GEF_IOREAD32(TEMPE_PCSR));
    
    tempCtl = tempCtl & ~(TEMPE_PCSR_MRC | TEMPE_PCSR_MRCT);
    
    switch(max_retry_info->max_retry)
    {
    case GEF_VME_MAX_RETRY_INFINITE: 
        break;
    case GEF_VME_MAX_RETRY_COUNT_TEST: 
        maxretry = TEMPE_PCSR_MRC | TEMPE_PCSR_MRCT; 
        break;
    case GEF_VME_MAX_RETRY_64:
    case GEF_VME_MAX_RETRY_128:
    case GEF_VME_MAX_RETRY_192:
    case GEF_VME_MAX_RETRY_256:
    case GEF_VME_MAX_RETRY_320:
    case GEF_VME_MAX_RETRY_384:
    case GEF_VME_MAX_RETRY_448:
    case GEF_VME_MAX_RETRY_512:
    case GEF_VME_MAX_RETRY_576:
    case GEF_VME_MAX_RETRY_640:
    case GEF_VME_MAX_RETRY_704:
    case GEF_VME_MAX_RETRY_768:
    case GEF_VME_MAX_RETRY_832:
    case GEF_VME_MAX_RETRY_896:
    case GEF_VME_MAX_RETRY_960:
        ret |= GEF_SEVERITY_WARNING;
    case GEF_VME_MAX_RETRY_MAX: 
        maxretry = TEMPE_PCSR_MRC; 
        break;
    default: break;
    }
    
    tempCtl = tempCtl | maxretry;
    
    GEF_IOWRITE32(longswap(tempCtl),TEMPE_PCSR);
    GEF_VME_IO_SYNC();
    
    gefTempeSpinlockRelease();
    
    return(ret);
}

//-----------------------------------------------------------------------------
// Function   : tempeGetMaxRetry
// Description:
//-----------------------------------------------------------------------------
GEF_STATUS tempeGetMaxRetry(GEF_VME_MAX_RETRY_INFO *max_retry_info)
{
    GEF_UINT32 tempCtl = 0;
    
    tempCtl = longswap(GEF_IOREAD32(TEMPE_PCSR));
    
    if((tempCtl & (TEMPE_PCSR_MRC | TEMPE_PCSR_MRCT)) == (TEMPE_PCSR_MRC | TEMPE_PCSR_MRCT)) 
    {
        max_retry_info->max_retry = GEF_VME_MAX_RETRY_COUNT_TEST;
    }
    else if (tempCtl & TEMPE_PCSR_MRC ) 
    {
        max_retry_info->max_retry = GEF_VME_MAX_RETRY_MAX;
    }
    else 
    {
        max_retry_info->max_retry = GEF_VME_MAX_RETRY_INFINITE;
    }
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeGetMaxRetry: max retry is: %d\n", max_retry_info->max_retry);

    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : tempeSysReset
// Description:
//-----------------------------------------------------------------------------
GEF_STATUS tempeSysReset(void)
{
    GEF_UINT32 tempCtl = 0;
        
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeSetMaxRetry: VME system reset\n");
    
    gefTempeSpinlockAcquire();
    
    tempCtl = longswap(GEF_IOREAD32(TEMPE_VCTRL));
    
    tempCtl |= TEMPE_VCTRL_SRESET;
    
    GEF_IOWRITE32(longswap(tempCtl),TEMPE_VCTRL);
    GEF_VME_IO_SYNC();
    
    gefTempeSpinlockRelease();
    
    return(GEF_SUCCESS);
}

//-----------------------------------------------------------------------------
// Function   : tempeQueryBusOwnership
// Description:
//-----------------------------------------------------------------------------
GEF_STATUS tempeQueryBusOwnership(GEF_BOOL *vown_info)
{
    GEF_UINT32 tempCtl = 0;
          
    /*
    * DHB (Device Has Bus): When this bit is set, the VME Master has 
    * obtained mastership of the VMEbus in response to the DWB request. 
    * This bit is not set if the VME Master has obtained VMEbus ownership 
    * for any other reason.
    */
    
    tempCtl = longswap(GEF_IOREAD32(TEMPE_VMCTRL));     
    tempCtl &= TEMPE_VMCTRL_DHB;
    
    if(tempCtl) {
        *vown_info = GEF_TRUE;
    }
    else {
        *vown_info = GEF_FALSE;
    }
    
    
    GEF_VME_DEBUG(GEF_DEBUG_TRACE, "tempeQueryBusOwnership: VMEbus ownership is %d\n", *vown_info);
 
    return(GEF_SUCCESS);
}
