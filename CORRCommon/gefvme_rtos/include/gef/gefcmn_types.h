/*
 * File:
 *    gefcmn_types.h
 *
 * Description:
 *    Type Specifications for the Common API Library for
 *    Linux platform.
 *
 * Copyright:
 *    Copyright 2007-2008 GE Fanuc Intelligent Platforms Embedded Systems, Inc.
 *
 * License:
 *    This file is proprietary to GE Fanuc Intelligent Platforms Embedded Systems, Inc.
 */
/*
 * $Date$
 * $Rev$
 */

#ifndef __GEFCMN_TYPES_H
#define __GEFCMN_TYPES_H

#ifdef __KERNEL__
#ifdef USE_RTOS
#include <rtLog.h>
#endif
#include <linux/time.h>
#else
#include <sys/time.h>
#endif

/*****************************************************************************/
/**
 * Title: SBC Common API Types
 *
 * Description:
 *    This header file provides type definitions for basic portable types
 *
 *
 */

/* Provide Type Definitions for basic portable types*/
/**
 * Group: Types and Constants
 */

/**
 * Type: GEF_UINT8
 *
 * Description:
 *    Corresponding basic date type refers to unsigned 1 byte. Eg.unsigned char
 *
 */
typedef unsigned char		GEF_UINT8;
/**
 * Type: GEF_UINT16
 *
 * Description:
 *    Corresponding basic date type refers to unsigned 2 bytes. Eg.unsigned short
 *
 */
typedef unsigned short int	GEF_UINT16;
/**
 * Type: GEF_UINT32
 *
 * Description:
 *    Corresponding basic date type refers to unsigned 4 bytes. Eg.unsigned long
 *
 */
typedef unsigned int		GEF_UINT32;
/**
 * Type: GEF_UINT64
 *
 * Description:
 *    Corresponding basic date type refers to unsigned 8 bytes. Eg.unsigned __int64
 *
 */
typedef unsigned long long int	GEF_UINT64; 
/**
 * Type: GEF_INT8
 *
 * Description:
 *    Corresponding basic date type refers to signed 1 byte. Eg. signed char.
 *
 */
typedef signed char		GEF_INT8;
/**
 * Type: GEF_INT16
 *
 * Description:
 *    Corresponding basic date type refers to signed 2 bytes. Eg. signed short
 *
 */
typedef signed short int	GEF_INT16;
/**
 * Type: GEF_INT32
 *
 * Description:
 *    Corresponding basic date type refers to signed 4 bytes. Eg. unsigned long
 *
 */
typedef signed int		GEF_INT32;
/**
 * Type: GEF_INT64
 *
 * Description:
 *    Corresponding basic date type refers to signed 8 bytes. Eg."unsigned __int64"
 *
 */
typedef signed long long int	GEF_INT64;
/**
 * Type: GEF_BOOL
 *
 * Description:
 *    Standard boolean enumeration.
 *
 *
 */
typedef unsigned char		GEF_BOOL;

/**
 * Type: GEF_MAP_PTR
 *
 * Description:
 *    A volatile void pointer type used to point to memory that can change
 * outside of the control of the program in which it appears.
 *
 */
typedef volatile void*      GEF_MAP_PTR;

/*****************************************************************************/

/* Add base constants for the Boolean */
/**
 * Type: GEF_TRUE
 *
 * Description:
 *     Standard boolean enumeration of type <GEF_BOOL>.
 * True(1)
 *
*/
#define GEF_TRUE  ((GEF_BOOL) 1)
/**
 * Type: GEF_FALSE
 *
 * Description:
 *     Standard boolean enumeration of type <GEF_BOOL>.
 * False(0) 
 *
*/
#define GEF_FALSE ((GEF_BOOL) 0)

/*****************************************************************************/

/**
 * Type: GEF_INFINITE_TIMEOUT
 *
 * Description:
 *     Declaration for infinite timeout.
 *
*/
#define GEF_INFINITE_TIMEOUT { 0xFFFFFFFF, 0 }

/**
 * Type: GEF_NO_WAIT_TIMEOUT
 *
 * Description:
 *     Declaration for a zero timeout.
 *
*/
#define GEF_NO_WAIT_TIMEOUT { 0, 0 }

/*****************************************************************************/

/**
 * Group: Debug macros defined for both kernel and user space.
 */

/**
 * Macro: GEF_WHEN_DEBUG
 *
 * Description:
 *     Return whether a given flag is set.
 *
 */
#define GEF_WHEN_DEBUG(debug_flag, flags) (debug_flag & flags)

/**
 * Macro: GEF_DEBUG_PRINT
 *
 * Description:
 *     Specific print function to use for debugging.
 *
 */

#ifdef __KERNEL__
    #define GEF_DEBUG_PRINT(arg...) printk(arg)
#else
    #define GEF_DEBUG_PRINT(arg...) fprintf(stderr, arg)
#endif

#endif  /* __GEFCMN_TYPES_H */
