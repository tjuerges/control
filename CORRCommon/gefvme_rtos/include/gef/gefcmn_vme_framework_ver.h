/*
 * File:
 *    gefcmn_vme_framework_ver.h
 *
 * Description:
 *    This interface file defines the framework version for the VME facility.
 *
 * Copyright:
 *    Copyright 2007 GE Fanuc Intelligent Platforms Embedded Systems, Inc.
 *
 * License:
 *    This file is proprietary to GE Fanuc Intelligent Platforms Embedded Systems, Inc.
 */
/*
 * $Date$
 * $Rev: 4467 $
 */
#ifndef __GEFCMN_VME_FRAMEWORK_VER_H
#define __GEFCMN_VME_FRAMEWORK_VER_H

/* 
 * GEF_VME_LIB_VER_MAJOR:  Update when API changes, INFO passing structure 
 * changes, must change if driver will not work with older library, etc. 
 */
#define GEF_VME_LIB_VER_MAJOR     3

/* 
 * GEF_VME_LIB_VER_MINOR:  Update for minor changes in library behavior, 
 * that would not affect compatibility between driver and library. 
 */
#define GEF_VME_LIB_VER_MINOR     0

/* 
 * GEF_VME_DRV_VER_MAJOR:  Driver and Library major version shall always 
 * agree as they both use this file which defines the communication between 
 * the two.
 */
#define GEF_VME_DRV_VER_MAJOR     GEF_VME_LIB_VER_MAJOR

#endif /* __GEFCMN_VME_FRAMEWORK_VER_H */
