/*
 * File:
 *    gefcmn_errno.h
 *
 * Description:
 *    This file defines Error codes for the Common API.
 *
 * Copyright:
 *    Copyright 2007 GE Fanuc Intelligent Platforms Embedded Systems, Inc.
 *
 * License:
 *    This file is proprietary to GE Fanuc Intelligent Platforms Embedded Systems, Inc.
 */
/*
 * $Date$
 * $Rev$
 */
#ifndef __GEFCMN_ERRNO_H
#define __GEFCMN_ERRNO_H

#include "gef/gefcmn_types.h"

/**
 * Title: SBC Common API error codes
 *
 * Description:
 *    This file defines Error codes for the Common API.
 *
 * Include Files:
 *    gefcmn_types.h
 *
 */

/*
Error codes for the common API


Proposed return code structure is as follows:

The return code will be an unsigned 32 bit integer, with the following bit fields:

Bits 31-28 Severity Code. Following are the values:
0000 - Success
0001 - Informational
0010 - Warning

27-20 Facility code. Following are the value assignments:
0x01 -  VME All VME devices (Tundra, Tempe)
0x02 -  FPGA All FPGA devices
0x03 -  Temp Sensor
0x04 -  Voltage Sensor
0x05 -  RTC
0x06 -  NVRAM
0x07 -  TIMER
0x08 -  Watchdog Timer
0x09 -  GPIO
0x0A -  EEPROM
0x0B -  Compact PCI Hint Interface
0x0C -  OS Abstraction Layer 
0x0D -  LED
0x0E -  SMBUS

19-0 Error Descriptor
The error descriptor will contain the actual error code
*/

typedef GEF_UINT32 GEF_STATUS;

#define GEF_STATUS_SEVERITY_MASK          ((GEF_UINT32) 0xF0000000)
#define GEF_STATUS_FACIL_MASK             ((GEF_UINT32) 0x0FF00000)
#define GEF_STATUS_ERROR_CODE_MASK        ((GEF_UINT32) 0x000FFFFF)
#define GEF_STATUS_FACIL_ERROR_FIRST      (256)

#define GEF_STATUS_FACIL_SHIFT            (20)

typedef enum 
{
    GEF_SEVERITY_SUCCESS = 0x00000000,
    GEF_SEVERITY_INFO    = 0x10000000,
    GEF_SEVERITY_WARNING = 0x20000000,
    GEF_SEVERITY_ERROR   = 0x30000000,

    GEF_SEVERITY_LAST    = 0x40000000
} GEF_SEVERITY_ENUM;

typedef enum 
{
    GEF_FACIL_VME            = 0x00100000,
    GEF_FACIL_TEMP_SENSOR    = 0x00200000,
    GEF_FACIL_VOLTAGE_SENSOR = 0x00300000,
    GEF_FACIL_RTC            = 0x00400000,
    GEF_FACIL_NVRAM          = 0x00500000,
    GEF_FACIL_TIMER          = 0x00600000,
    GEF_FACIL_WATCHDOG_TIMER = 0x00700000,
    GEF_FACIL_GPIO           = 0x00800000,
    GEF_FACIL_EEPROM         = 0x00900000,
    GEF_FACIL_CPCI_HINT      = 0x00A00000,
    GEF_FACIL_ERR            = 0x00B00000,
    GEF_FACIL_OSA            = 0x00C00000,
    GEF_FACIL_LED            = 0x00D00000,
    GEF_FACIL_SMBUS          = 0x00E00000,

    GEF_FACIL_LAST           = 0x00F00000
} GEF_FACIL_ENUM;

/**
 * Type: GEF_STATUS_ENUM
 *
 * Description:
 *      Enumeration to set the error status.
 *        
 *  GEF_STATUS_SUCCESS                    - Success
 *  GEF_STATUS_NOT_SUPPORTED              - Request not supported
 *  GEF_STATUS_NO_MEM                     - out of memory
 *  GEF_STATUS_INVAL_ADDR                 - Invalid argument
 *  GEF_STATUS_READ_ERR                   - Read Error
 *  GEF_STATUS_WRITE_ERR                  - Write error
 *  GEF_STATUS_DEVICE_NOT_INIT            - Device not initialized
 *  GEF_STATUS_NO_SUCH_DEVICE             - Device not found
 *  GEF_STATUS_DRIVER_ERR                 - Driver returned error
 *  GEF_STATUS_INTERRUPTED                - Request Interrupted
 *  GEF_STATUS_TIMED_OUT                  - Request Timeout 
 *  GEF_STATUS_EVENT_IN_USE               - Callback or Wait for alarm (blocking call) in use
 *  GEF_STATUS_THREAD_CREATION_FAILED     - Thread creation failed
 *  GEF_STATUS_CALLBACK_NOT_ATTACHED      - Callback is not registered
 *  GEF_STATUS_DEVICE_IN_USE              - Device is already in use  	
 *  GEF_STATUS_OPERATION_NOT_ALLOWED      - This operation is not allowed
 *  GEF_STATUS_BAD_PARAMETER_1            - Bad parameter 1
 *  GEF_STATUS_BAD_PARAMETER_2            - Bad parameter 2  
 *  GEF_STATUS_BAD_PARAMETER_3            - Bad parameter 3  
 *  GEF_STATUS_BAD_PARAMETER_4            - Bad parameter 4  
 *  GEF_STATUS_BAD_PARAMETER_5            - Bad parameter 5  
 *  GEF_STATUS_BAD_PARAMETER_6            - Bad parameter 6  
 *  GEF_STATUS_BAD_PARAMETER_7            - Bad parameter 7  
 *  GEF_STATUS_BAD_PARAMETER_8            - Bad parameter 8  
 *  GEF_STATUS_BAD_PARAMETER_9            - Bad parameter 9  
 *  GEF_STATUS_BAD_PARAMETER_10           - Bad parameter 10
 *  GEF_STATUS_BAD_PARAMETER_11           - Bad parameter 11 
 *  GEF_STATUS_INVALID_ERROR_CODE         - Invalid error code
 */
typedef enum 
{
    GEF_STATUS_SUCCESS       = 0x00000000,
    GEF_STATUS_NOT_SUPPORTED,
    GEF_STATUS_NO_MEM,
    GEF_STATUS_INVAL_ADDR,
    GEF_STATUS_READ_ERR,
    GEF_STATUS_WRITE_ERR,
    GEF_STATUS_DEVICE_NOT_INIT,
    GEF_STATUS_NO_SUCH_DEVICE,
    GEF_STATUS_DRIVER_ERR,
    GEF_STATUS_INTERRUPTED,
    GEF_STATUS_TIMED_OUT,
    GEF_STATUS_EVENT_IN_USE,
    GEF_STATUS_THREAD_CREATION_FAILED,
    GEF_STATUS_CALLBACK_NOT_ATTACHED,
    GEF_STATUS_DEVICE_IN_USE,
    GEF_STATUS_OPERATION_NOT_ALLOWED,
    GEF_STATUS_BAD_PARAMETER_1,  /* Function Parameter  1 invalid  */
    GEF_STATUS_BAD_PARAMETER_2,  /* Function Parameter  2 invalid  */
    GEF_STATUS_BAD_PARAMETER_3,  /* Function Parameter  3 invalid  */
    GEF_STATUS_BAD_PARAMETER_4,  /* Function Parameter  4 invalid  */
    GEF_STATUS_BAD_PARAMETER_5,  /* Function Parameter  5 invalid  */
    GEF_STATUS_BAD_PARAMETER_6,  /* Function Parameter  6 invalid  */
    GEF_STATUS_BAD_PARAMETER_7,  /* Function Parameter  7 invalid  */
    GEF_STATUS_BAD_PARAMETER_8,  /* Function Parameter  8 invalid  */
    GEF_STATUS_BAD_PARAMETER_9,  /* Function Parameter  9 invalid  */
    GEF_STATUS_BAD_PARAMETER_10, /* Function Parameter 10 invalid  */
    GEF_STATUS_BAD_PARAMETER_11, /* Function Parameter 11 invalid  */
    GEF_STATUS_INVALID_ERROR_CODE,
    GEF_STATUS_NO_EVENT_PENDING,
    GEF_STATUS_EVENT_RELEASED,
    GEF_STATUS_LAST,
} GEF_STATUS_ENUM;


#define GEF_SUCCESS                       (GEF_STATUS_SUCCESS)

#define GEF_SET_SEVERITY(SEVERITY, FACIL, STATUS) \
                        ((GEF_UINT32) (SEVERITY | FACIL | (GEF_UINT32) (STATUS & GEF_STATUS_ERROR_CODE_MASK)))

#define GEF_SET_INFO(FACIL,STATUS)        (GEF_SET_SEVERITY(GEF_SEVERITY_INFO,    FACIL, STATUS)) 
#define GEF_SET_WARNING(FACIL, STATUS)    (GEF_SET_SEVERITY(GEF_SEVERITY_WARNING, FACIL, STATUS)) 
#define GEF_SET_ERROR(FACIL, STATUS)      (GEF_SET_SEVERITY(GEF_SEVERITY_ERROR,   FACIL, STATUS))

#define GEF_GET_ERROR(STATUS)             ((GEF_UINT32) (STATUS & GEF_STATUS_ERROR_CODE_MASK))
#define GEF_GET_SEVERITY(STATUS)          ((GEF_UINT32) (STATUS & GEF_STATUS_SEVERITY_MASK))
#define GEF_GET_FACIL(STATUS)             ((GEF_UINT32) (STATUS & GEF_STATUS_FACIL_MASK))

/* GEF_DEBUG message class definitions                                           */
#define GEF_DEBUG_ERROR         (1<<0)   /* Report critical errors               */
#define GEF_DEBUG_INIT          (1<<1)   /* Trace device probing and search      */
#define GEF_DEBUG_INTR          (1<<2)   /* Trace interrupt service              */
#define GEF_DEBUG_IOCTL         (1<<3)   /* Trace ioctl system calls             */
#define GEF_DEBUG_MMAP          (1<<4)   /* Trace mmap system calls              */
#define GEF_DEBUG_OPEN          (1<<5)   /* Trace open system calls              */
#define GEF_DEBUG_CLOSE         (1<<6)   /* Trace close system calls             */
#define GEF_DEBUG_READ          (1<<7)   /* Trace read system calls              */ 
#define GEF_DEBUG_WRITE         (1<<8)   /* Trace write system calls             */
#define GEF_DEBUG_PEEK          (1<<9)   /* Trace peeks                          */
#define GEF_DEBUG_POKE          (1<<10)  /* Trace pokes                          */
#define GEF_DEBUG_STRAT         (1<<11)  /* Trace read/write strategy            */
#define GEF_DEBUG_TIMER         (1<<12)  /* Trace interval timer                 */
#define GEF_DEBUG_TRACE         (1<<13)  /* Trace subroutine entry/exit          */
#define GEF_DEBUG_MUTEX         (1<<14)  /* Trace synchronization and locking    */
#define GEF_DEBUG_DMA			(1<<15)  /* Trace DMA Calls					     */

#endif /* __GEFCMN_ERRNO_H */
