#ifndef gefanuc_kernelApi_h
#define gefanuc_kernelApi_h
/**
 * $Id$
 */


#include <gef/gefcmn_vme_framework.h>
#include <gef/gefcmn_vme_defs.h>
#include <gef/gefcmn_errno.h>


#ifndef PCI_VENDOR_ID_TUNDRA
#define PCI_VENDOR_ID_TUNDRA 0x10e3
#endif

#ifndef PCI_DEVICE_ID_TUNDRA_CA91C042
#define PCI_DEVICE_ID_TUNDRA_CA91C042 0x0000
#endif

#ifndef PCI_DEVICE_ID_TUNDRA_CA91C042
#define PCI_DEVICE_ID_TUNDRA_CA91C042 0x0000
#endif

#ifndef PCI_DEVICE_ID_TEMPE_TSI148
#define PCI_DEVICE_ID_TEMPE_TSI148 0x0148
#endif


#ifdef __cplusplus
extern "C" {
#endif
int getBridgeType(void);

void* getWindowPointerFromHandle(GEF_VME_MASTER_OSSPEC_HDL master_osspec_hdl);

int vmeGenerateIrq(GEF_VME_DRV_VIRQ_INFO *);
int vmeWaitForIrq(GEF_VME_DRV_WAIT_INFO *);
int vmeWaitIrqAck(GEF_VME_DRV_WAIT_INFO *);
int vmeReleaseIrqWait(GEF_VME_DRV_VIRQ_INFO *);
int vmeGetArbiter(GEF_VME_DRV_BUS_ARB_INFO *);
int vmeSetArbiter(GEF_VME_DRV_BUS_ARB_INFO *);
int vmeSetReleaseMode(GEF_VME_DRV_BUS_REL_MODE_INFO *);
int vmeGetReleaseMode(GEF_VME_DRV_BUS_REL_MODE_INFO *);
int vmeSetRequestMode(GEF_VME_DRV_BUS_REQ_MODE_INFO *);
int vmeGetRequestMode(GEF_VME_DRV_BUS_REQ_MODE_INFO *);
int vmeSetRequestLevel(GEF_VME_DRV_BUS_REQ_LEVEL_INFO *);
int vmeGetRequestLevel(GEF_VME_DRV_BUS_REQ_LEVEL_INFO *);
int vmeSetBusTimeout(GEF_VME_DRV_BUS_TIMEOUT_INFO *);
int vmeGetBusTimeout(GEF_VME_DRV_BUS_TIMEOUT_INFO *);
int vmeSetArbTimeout(GEF_VME_DRV_ARB_TIMEOUT_INFO *);
int vmeGetArbTimeout(GEF_VME_DRV_ARB_TIMEOUT_INFO *);
int vmeSetBroadcastId(GEF_VME_DRV_BUS_BROADCAST_INFO *);
int vmeGetBroadcastId(GEF_VME_DRV_BUS_BROADCAST_INFO *);
int vmeSetSysFail(GEF_VME_DRV_SYSFAIL_INFO *);
int vmeGetSysFail(GEF_VME_DRV_SYSFAIL_INFO *);
int vmeSetMaxRetry(GEF_VME_MAX_RETRY_INFO *);
int vmeGetMaxRetry(GEF_VME_MAX_RETRY_INFO *);
int vmeSetPostedWriteCount(GEF_VME_POST_WRITE_COUNT_INFO *);
int vmeGetPostedWriteCount(GEF_VME_POST_WRITE_COUNT_INFO *);
int vmeSysReset(void);
int vmeAcquireBusOwnership(GEF_VME_DRV_VOWN_INFO *);
int vmeReleaseBusOwnership(void);
int vmeQueryBusOwnership(GEF_BOOL *);
int vmeEnableIrq(GEF_VME_INT_INFO *);
int vmeDisableIrq(GEF_VME_INT_INFO *);
int vmeFreeIrqWaits(void);
int vmeSetDebugFlags(GEF_UINT32 *);
int vmeGetDebugFlags(GEF_UINT32 *);
int vmeRead(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT8 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements);
int vmeRead8(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT8 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements);
int vmeRead16(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT16 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements);
int vmeRead32(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT32 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements);
int vmeRead64(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT64 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements);
int vmeCreateMasterWindow(GEF_VME_DRV_CREATE_MASTER_WINDOW_INFO *);
int vmeReleaseMasterWindow(GEF_VME_DRV_RELEASE_MASTER_WINDOW_HDL_INFO *);

int vmeReadModifyWrite(GEF_VME_DRV_READ_MODIFY_WRITE_INFO *);
int vmeWrite(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT8 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements);
int vmeWrite8(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT8 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements);
int vmeWrite16(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT16 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements);
int vmeWrite32(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT32 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements);
int vmeWrite64(GEF_VME_MASTER_OSSPEC_HDL handle, GEF_UINT64 *buffer, GEF_UINT32 offset, GEF_UINT32 num_elements);
int vmeReleaseMasterWindows(void);
int vmeCreateSlaveWindow(GEF_VME_DRV_CREATE_SLAVE_WINDOW_INFO *);
int vmeRemoveSlaveWindow(GEF_VME_DRV_RELEASE_SLAVE_WINDOW_HDL_INFO *);
int vmeReleaseSlaveWindows(void);
int vmeCreateLocMonWindow(GEF_VME_DRV_LOCATION_MONITOR_INFO *);
int vmeReleaseLocMonWindow(GEF_VME_DRV_LOCATION_MONITOR_INFO *);
int vmeQueryLocMonWindow(GEF_VME_DRV_LOCATION_MONITOR_INFO *);
int vmeCreateVraiWindow(GEF_VME_DRV_VRAI_INFO *);
int vmeReleaseVraiWindow(GEF_VME_DRV_VRAI_INFO *);
int vmeQueryVraiWindow(GEF_VME_DRV_VRAI_INFO *);

int vmeTransMasterWindow(GEF_VME_DRV_TRANSLATE_MASTER_WINDOW_INFO *);
int vmeMapMasterWindow(GEF_VME_MAP_MASTER_INFO *);
int vmeMapSlaveWindow(GEF_VME_MAP_SLAVE_INFO *);

int vmeAllocDmaBuf(GEF_VME_DRV_ALLOC_DMA_BUF_INFO *);
int vmeFreeDmaBuf(GEF_VME_DRV_FREE_DMA_BUF_INFO *);
int vmeFreeDmaHandles(void);

int vmeSetMasterHWByteswap(GEF_VME_HW_BYTESWAP_INFO *);
int vmeGetMasterHWByteswap(GEF_VME_HW_BYTESWAP_INFO *);
int vmeSetSlaveHWByteswap(GEF_VME_HW_BYTESWAP_INFO *);
int vmeGetSlaveHWByteswap(GEF_VME_HW_BYTESWAP_INFO *);
int vmeSetSwByteSwap(GEF_VME_DRV_BYTESWAP_INFO *);
int vmeGetSwByteSwap(GEF_VME_DRV_BYTESWAP_INFO *);

int vmeSetTimeOnBus(GEF_VME_DRV_TIME_ON_BUS_INFO *);
int vmeGetTimeOnBus(GEF_VME_DRV_TIME_ON_BUS_INFO *);
int vmeSetTimeOffBus(GEF_VME_DRV_TIME_OFF_BUS_INFO *);
int vmeGetTimeOffBus(GEF_VME_DRV_TIME_OFF_BUS_INFO *);

int vmeReleaseVraiWindows(void);
int vmeReleaseLocMonWindows(void);
#ifdef __cplusplus
}
#endif
#endif
