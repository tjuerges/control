/*
 * File:
 *    vmedrv.h
 *
 * Description:
 *    This is the driver header for the VME interface.
 *
 * Copyright:
 *    Copyright 2007 GE Fanuc Embedded Systems, Inc.
 *
 * License:
 *    This file is proprietary to GE Fanuc Embedded Systems, Inc.
 */
/*
 * $Date$
 * $Rev$
 */


/*
 * Module name: vmedrv.h
 *	Application interface to VME device nodes.
 */


#ifndef	VMEDRV_H
#define	VMEDRV_H

#define	VMEDRV_REV	0x0301

#define	VME_MINOR_TYPE_MASK	0xF0
#define	VME_MINOR_DMA	0x10

#ifndef	PAGESIZE
#define	PAGESIZE 4096
#endif
#ifndef	LINESIZE
#define	LINESIZE 0x20
#endif
#define	VME_MAX_WINDOWS	8
#ifndef	PCI_DEVICE_ID_TUNDRA_TEMPE
#define	PCI_DEVICE_ID_TUNDRA_TEMPE 0x148
#endif

/* Resource flag management */

#define	HAS_RESOURCE(u,r)	( (u) &   (r) )
#define	ADD_RESOURCE(u,r)	( (u) |=  (r) )
#define	DEL_RESOURCE(u,r)	( (u) &= ~(r) )
  
#define VME_RSRC_FPGA      (1U << 0)       /* FPGA initialized     */
#define VME_RSRC_PCI_EN    (1U << 1)       /* PCI enabled          */
#define VME_RSRC_BRIDGE    (1U << 2)       /* Bridge initialized   */
#define VME_RSRC_DEVINIT   (1U << 3)       /* Device initialized   */
#define VME_RSRC_INTR      (1U << 4)       /* ISR installed		   */
#define VME_RSRC_CHR       (1U << 5)       /* Char dev initialized */
#define VME_RSRC_PROC      (1U << 6)       /* Proc initialized     */

#define GEF_IRQ_WAIT      (1<<0)     /* User wants synchronous notification   */
#define IRQ_TIMEDOUT  (1<<1)     /* Notification timed out                */
#define IRQ_ENABLED   (1<<2)     /* Interrupt is enabled                  */
#define IRQ_RELEASED  (1<<3)     /* Wait for Event Canceled/Released      */

/* Mapped IRQ values for the driver */
#define DRV_IRQ_LEVEL_VOWN      0
#define DRV_IRQ_LEVEL_VIRQ1     1
#define DRV_IRQ_LEVEL_VIRQ2     2
#define DRV_IRQ_LEVEL_VIRQ3     3
#define DRV_IRQ_LEVEL_VIRQ4     4
#define DRV_IRQ_LEVEL_VIRQ5     5
#define DRV_IRQ_LEVEL_VIRQ6     6
#define DRV_IRQ_LEVEL_VIRQ7     7
#define DRV_IRQ_LEVEL_DMA0      8
#define DRV_IRQ_LEVEL_LERR      9
#define DRV_IRQ_LEVEL_BERR     10
#define DRV_IRQ_LEVEL_IRQ_EDGE 11 /* Tempe Only */
#define DRV_IRQ_LEVEL_SW_IACK  12
#define DRV_IRQ_LEVEL_SW_INT   13 /* Universe Only */
#define DRV_IRQ_LEVEL_SYSFAIL  14
#define DRV_IRQ_LEVEL_ACFAIL   15
#define DRV_IRQ_LEVEL_MBOX0    16
#define DRV_IRQ_LEVEL_MBOX1    17
#define DRV_IRQ_LEVEL_MBOX2    18
#define DRV_IRQ_LEVEL_MBOX3    19
#define DRV_IRQ_LEVEL_LM0      20
#define DRV_IRQ_LEVEL_LM1      21
#define DRV_IRQ_LEVEL_LM2      22
#define DRV_IRQ_LEVEL_LM3      23
#define DRV_IRQ_LEVEL_DMA1     24 /* Tempe Only */

/* All registers above 0x100 must be byteswapped in the Tempe on x86 platforms */
#ifdef PPC
#define longswap(x) (x)
#else
#define longswap(x) ((uint32_t) ( \
                     (((x)<<24) & 0xff000000) | \
                     (((x)<<8)  & 0x00ff0000) | \
                     (((x)>>8)  & 0x0000ff00) | \
                     (((x)>>24) & 0x000000ff)  \
                    ))
#endif

#ifndef iowrite32
  #define iowrite32 writel
#endif
#ifndef iowrite16
  #define iowrite16 writew
#endif
#ifndef iowrite8
  #define iowrite8 writeb
#endif

#ifndef ioread32
  #define ioread32 readl
#endif
#ifndef ioread16
  #define ioread16 readw
#endif
#ifndef ioread8
  #define ioread8 readb
#endif


/************************************************************************
 * COMMON definitions 
 */

/*
 *  VME access type definitions
 */
#define	VME_DATA		1
#define	VME_PROG		2
#define	VME_USER		4
#define	VME_SUPER		8

/*
 *  VME address type definitions
 */
typedef enum {
        VME_A16,
        VME_A24,
        VME_A32,
        VME_A64,
        VME_CRCSR,
        VME_USER1,
        VME_USER2,
        VME_USER3,
        VME_USER4
} addressMode_t;

/*
 *  VME Transfer Protocol Definitions
 */
#define	VME_SCT			0x1
#define	VME_BLT			0x2
#define	VME_MBLT		0x4
#define	VME_2eVME		0x8
#define	VME_2eSST		0x10
#define	VME_2eSSTB		0x20

/*
 *  Data Widths 
 */
typedef enum {
        VME_D8 = 8,
        VME_D16 = 16,
        VME_D32 = 32,
        VME_D64 = 64
} dataWidth_t;

/*
 *  2eSST Data Transfer Rate Codes
 */
typedef enum {
	VME_SSTNONE = 0,
	VME_SST160 = 160,
	VME_SST267 = 267,
	VME_SST320 = 320
} vme2esstRate_t;


/*
 *  Arbitration Scheduling Modes
 */
typedef enum {
	VME_R_ROBIN_MODE,
	VME_PRIORITY_MODE
} vme2ArbMode_t;


typedef enum
{
  DRV_VME_BROADCAST_ID_DISABLE = 0x00000000,   /* Tempe Only */
  DRV_VME_BROADCAST_ID_1       = 0x00000001,   /* Tempe Only */
  DRV_VME_BROADCAST_ID_2       = 0x00000002,   /* Tempe Only */
  DRV_VME_BROADCAST_ID_3       = 0x00000004,   /* Tempe Only */
  DRV_VME_BROADCAST_ID_4       = 0x00000008,   /* Tempe Only */
  DRV_VME_BROADCAST_ID_5       = 0x00000010,   /* Tempe Only */
  DRV_VME_BROADCAST_ID_6       = 0x00000020,   /* Tempe Only */
  DRV_VME_BROADCAST_ID_7       = 0x00000040,   /* Tempe Only */
  DRV_VME_BROADCAST_ID_8       = 0x00000080,   /* Tempe Only */
  DRV_VME_BROADCAST_ID_9       = 0x00000100,   /* Tempe Only */
  DRV_VME_BROADCAST_ID_10      = 0x00000200,   /* Tempe Only */
  DRV_VME_BROADCAST_ID_11      = 0x00000400,   /* Tempe Only */
  DRV_VME_BROADCAST_ID_12      = 0x00000800,   /* Tempe Only */
  DRV_VME_BROADCAST_ID_13      = 0x00001000,   /* Tempe Only */
  DRV_VME_BROADCAST_ID_14      = 0x00002000,   /* Tempe Only */
  DRV_VME_BROADCAST_ID_15      = 0x00004000,   /* Tempe Only */
  DRV_VME_BROADCAST_ID_16      = 0x00008000,   /* Tempe Only */
  DRV_VME_BROADCAST_ID_17      = 0x00010000,   /* Tempe Only */
  DRV_VME_BROADCAST_ID_18      = 0x00020000,   /* Tempe Only */
  DRV_VME_BROADCAST_ID_19      = 0x00040000,   /* Tempe Only */
  DRV_VME_BROADCAST_ID_20      = 0x00080000,   /* Tempe Only */
  DRV_VME_BROADCAST_ID_21      = 0x00100000,   /* Tempe Only */
  DRV_VME_BROADCAST_ID_LAST    = 22, /* Keep as number of elements */
} DRV_VME_BROADCAST_ID;


typedef enum
{
    DRV_VME_DMA_WRITE = 1,
    DRV_VME_DMA_READ  = 2
        
} DRV_VME_DMA_DIRECTION;


/************************************************************************
 *  /dev/vme_m* Outbound Window Ioctl Commands
 */
#define	VME_IOCTL_SET_OUTBOUND		0x10
#define	VME_IOCTL_GET_OUTBOUND		0x11
/*
 *  VMEbus OutBound Window Arg Structure
 *  NOTE:
 *	If pciBusAddr[U,L] are 0, then kernel will dynamically assign
 *	pci start address on PCI bus.
 */
struct	vmeOutWindowCfg
{
	int		windowNbr;	/*  Window Number */
	char		windowEnable;	/*  State of Window */
	unsigned int	pciBusAddrU;	/*  Start Address on the PCI Bus */
	unsigned int	pciBusAddrL;	/*  Start Address on the PCI Bus */
	unsigned int	windowSizeU;	/*  Window Size */
	unsigned int	windowSizeL;	/*  Window Size */
	unsigned int	xlatedAddrU;	/*  Starting Address on the VMEbus */
	unsigned int	xlatedAddrL;	/*  Starting Address on the VMEbus */
	int		bcastSelect2esst;	/*  2eSST Broadcast Select */
	char		wrPostEnable;		/*  Write Post State */
	char		prefetchEnable;	/*  Prefetch Read Enable State */
	int		prefetchSize;	/*  Prefetch Read Size (in Cache Lines) */
	vme2esstRate_t	xferRate2esst;	/*  2eSST Transfer Rate */
	addressMode_t	addrSpace;	/*  Address Space */
	dataWidth_t	maxDataWidth;	/*  Maximum Data Width */
	int		xferProtocol;	/*  Transfer Protocol */
	int		userAccessType;	/*  User/Supervisor Access Type */
	int		dataAccessType;	/*  Data/Program Access Type */

};
typedef	struct	vmeOutWindowCfg	vmeOutWindowCfg_t;



/************************************************************************
 *  /dev/vme_dma* DMA commands
 */
#define	VME_IOCTL_START_DMA		0x30
#define	VME_IOCTL_PAUSE_DMA		0x31
#define	VME_IOCTL_CONTINUE_DMA		0x32
#define	VME_IOCTL_ABORT_DMA		0x33
#define	VME_IOCTL_WAIT_DMA		0x34
#define VFAR_ON                 1
#define VFAR_OFF                0
#define PFAR_ON                 1
#define PFAR_OFF                0

typedef enum {
	/* NOTE: PATTERN entries only valid as source of data */
	VME_DMA_PATTERN_BYTE,
	VME_DMA_PATTERN_BYTE_INCREMENT,
	VME_DMA_PATTERN_WORD,
	VME_DMA_PATTERN_WORD_INCREMENT,
	VME_DMA_USER,
	VME_DMA_KERNEL,
	VME_DMA_PCI,
	VME_DMA_VME
} dmaData_t;

/*
 *  VMEbus Transfer Attributes 
 */
struct	vmeAttr
{
	dataWidth_t	maxDataWidth;      /*  Maximum Data Width */
	vme2esstRate_t  xferRate2esst; /*  2eSST Transfer Rate */
	int		bcastSelect2esst;      /*  2eSST Broadcast Select */
	addressMode_t   addrSpace;     /*  Address Space */
	int		userAccessType;        /*  User/Supervisor Access Type */
	int		dataAccessType;        /*  Data/Program Access Type */
	int		xferProtocol;          /*  Transfer Protocol */
	int		reserved;              /*  For future use */
};
typedef	struct	vmeAttr	vmeAttr_t;

/*
 *  DMA arg info
 *  NOTE: 
 *	structure contents relating to VME are don't care for 
 *       PCI transactions
 *	structure contents relating to PCI are don't care for 
 *       VME transactions
 *	If source or destination is user memory and transaction
 *	will cross page boundary, the DMA request will be split
 *	into multiple DMA transactions.
 */
typedef	struct	vmeDmaPacket
{
    unsigned int    vmeDmaStartTick;   /*  Time DMA started */
    unsigned int    vmeDmaStopTick;    /*  Time DMA stopped */
    unsigned int    vmeDmaElapsedTime; /*  Elapsed time */
    int             vmeDmaStatus;      /*  DMA completion status */
    
    int             byteCount;         /*  Byte Count */
    int             bcastSelect2esst;  /*  2eSST Broadcast Select */
    int             ld64Enabled;       /*  Universe 64-Bit PCI transactions */
    
    /*
     *  DMA Source Data
     */
    dmaData_t           srcBus;
    unsigned int        srcAddrU;
    unsigned int        srcAddr;
    struct vmeAttr      srcVmeAttr;
    /*
     *  DMA Destination Data
     */
    dmaData_t           dstBus;
    unsigned int        dstAddrU;
    unsigned int        dstAddr;
    struct vmeAttr      dstVmeAttr;
        
    /*
     *  BUS usage control
     */
    int                 maxCpuBusBlkSz;         /* CPU Bus Maximum Block Size */
    int                 maxVmeBlockSize;        /* VMEbus Maximum Block Size - VON */
    int                 vmeBackOffTimer;        /* VMEbus Back-Off Timer - VOFF */
    char                vmeFlushOnAbortRead;    /* VMEbus read abort: 1 - transfer remaining fifo to destination
                                                                      0 - discard remaining fifo                 */
    char                pciFlushOnAbortRead;    /* PCI read abort:    1 - transfer remaining fifo to destination
                                                                      0 - discard remaining fifo                 */
    
    int                 channel_number;  /* Channel number */
    /*
     * Ptr to next Packet
     * (NULL == No more Packets)
     */
    struct vmeDmaPacket *pNextPacket;
    
} vmeDmaPacket_t;


/*
 *  VMEbus InBound Window Arg Structure
 *  NOTE:
 *	If pciBusAddr[U,L] and windowSize[U,L] are 0, then kernel 
 *      will dynamically assign inbound window to map to a kernel
 *	supplied buffer.
 */
struct	vmeInWindowCfg
{
	int           windowNbr;    /*  Window Number */
	char          windowEnable; /*  State of Window */
	unsigned int  vmeAddrU;     /*  Start Address responded to on the VMEbus */
	unsigned int  vmeAddrL;     /*  Start Address responded to on the VMEbus */
	unsigned int  windowSizeU;  /*  Window Size */
	unsigned int  windowSizeL;  /*  Window Size */
	unsigned int  pciAddrU;     /*  Start Address appearing on the PCI Bus */
	unsigned int  pciAddrL;     /*  Start Address appearing on the PCI Bus */
	char          wrPostEnable; /*  Write Post State */
	char          prefetchEnable; /*  Prefetch Read State */
	char          prefetchThreshold; /*  Prefetch Read Threshold State */
	int           prefetchSize; /*  Prefetch Read Size */
	char          rmwLock;      /*  Lock PCI during RMW Cycles */
	char          data64BitCapable; /*  non-VMEbus capable of 64-bit Data */
	addressMode_t addrSpace;    /*  Address Space */
	int           userAccessType; /*  User/Supervisor Access Type */
	int           dataAccessType; /*  Data/Program Access Type */
	int           xferProtocol; /*  Transfer Protocol */
	vme2esstRate_t xferRate2esst; /*  2eSST Transfer Rate */
	char          bcastRespond2esst; /*  Respond to 2eSST Broadcast */

};
typedef	struct	vmeInWindowCfg	vmeInWindowCfg_t;

/*
 *  VMEbus RMW Configuration Data
 */
struct	vmeRmwCfg
{
	unsigned int	targetAddrU;       /*  VME Address (Upper) to trigger RMW cycle */
	unsigned int	targetAddr;	       /*  VME Address (Lower) to trigger RMW cycle */
	addressMode_t	addrSpace;	       /*  VME Address Space */
	unsigned int    enableMask;        /*  Bit mask defining the bits of interest */
	unsigned int    compareData;       /*  Data to be compared with the data read */
	unsigned int    swapData;          /*  Data written to the VMEbus on success */
    unsigned int    dataWidth;
    unsigned int    mastWindVmeAddrU;
    unsigned int    mastWindVmeAddrL;
    void *          mastWindBasePciAddr;
    void *          mastWindVirtAddr;
    unsigned int    mastWindSizeU;
    unsigned int    mastWindSizeL;

} ;
typedef	struct vmeRmwCfg	vmeRmwCfg_t;

/*
 *  VMEbus Location Monitor Arg Structure
 */
struct	vmeLmCfg
{
	unsigned int		addrU;	/*  Location Monitor Address upper */
	unsigned int		addr;	/*  Location Monitor Address lower */
	addressMode_t	addrSpace;	/*  Address Space */
	int		userAccessType;	/*  User/Supervisor Access Type */
	int		dataAccessType;	/*  Data/Program Access Type */
	int		lmWait;		/* Time to wait for access */
	int		lmEvents;	/* Lm event mask */
};
typedef	struct	vmeLmCfg	vmeLmCfg_t;

/*
 *  Driver errors reported back to the Application (other than the
 *  stanard Linux errnos...).
 */
#define	VME_ERR_VERR		1	/* VME bus error detected */
#define	VME_ERR_PERR		2	/* PCI bus error detected */

#define MASTER_HANDLE_MAGIC 0x10E30004
#define SLAVE_HANDLE_MAGIC  0x10E30005

#define VME_SLAVE_WINDOWS   8
#define VME_MASTER_WINDOWS  8

/*
    The master window structure is created for each Universe
    or Tempe master window.  It contains the virtual and physical
    pointers, pci resource, exclusive flag, window number, and
    master window handle.  The master window handle is associated
    to each process request for a master window resource.  It is
    a linked list of pointer in the master window structure.  The
    master handle contains a next and prev master handle pointer,
    a magic number. and a pointer to the master window structure.
*/
struct vmeMasterWindow
{
    struct GEF_VME_MASTER_OSSPEC *handles;      /* linked list of master handles */
    void *virtAddr;                             /* virtual address mapped to VMEbus */
    void *physAddr;                             /* physical address mapped to VMEbus */
    struct resource physAddrRes;                /* physical addess resource */
    GEF_BOOL exclusive;                         /* exclusive window flag */
    int number;
    GEF_UINT32 vmeL;                            /* vme address lower 32 bits */
    GEF_UINT32 vmeU;                            /* vme address upper 32 bits */
    GEF_UINT32 size;                            /* window size */
    GEF_UINT32 sizeU;                           /* window upper size */
    GEF_VME_ADDR vmeAddr;                       /* CAPI vme address */
    GEF_ENDIAN swEndian;                        /* software endian flag */
};
typedef	struct vmeMasterWindow vmeMasterWindow_t;

/*
    The slave window structure is created for each Universe
    or Tempe master window.  It contains the virtual and physical
    pointers, pci resource, exclusive flag, window number, and
    slave window handle.  The slave window handle is associated
    to each process request for a slave window resource.  It is
    a linked list of pointer in the slave window structure.
*/
struct vmeSlaveWindow
{
    struct GEF_VME_SLAVE_OSSPEC *handles;       /* linked list of slave handles */
    void *virtAddr;                             /* virtual address mapped to VMEbus */
    void *physAddr;                             /* physical address mapped to VMEbus */
    dma_addr_t physAddrRes;                     /* physical addess resource */
    GEF_BOOL exclusive;                         /* exclusive window flag */
    int number;
    GEF_UINT32 vmeL;                            /* vme address lower 32 bits */
    GEF_UINT32 vmeU;                            /* vme address upper 32 bits */
    GEF_UINT32 size;                            /* window size */
};
typedef	struct vmeSlaveWindow vmeSlaveWindow_t;

/*
  The master handle is created for each process master window request.
  The master handle contains a next and prev master handle pointer,
  a magic number. and a pointer to the master window structure.
*/
struct GEF_VME_MASTER_OSSPEC
{
    struct GEF_VME_MASTER_OSSPEC *next;             /* next master handle */
    struct GEF_VME_MASTER_OSSPEC *prev;             /* previous master handle */
    int magic;                                      /* magic number to validate memory */
    struct vmeMasterWindow *window;                 /* master window */
    GEF_UINT32 offset;                              /* offset from the master window address */
};
typedef	struct GEF_VME_MASTER_OSSPEC vmeMasterHandle_t;

/*
  The slave handle is created for each process slave window request.
  The slave handle contains a next and prev slave handle pointer,
  a magic number. and a pointer to the slave window structure.
*/
struct GEF_VME_SLAVE_OSSPEC
{
    struct GEF_VME_SLAVE_OSSPEC *next;              /* next master handle */
    struct GEF_VME_SLAVE_OSSPEC *prev;              /* previous master handle */
    int magic;                                      /* magic number to validate memory */
    struct vmeSlaveWindow *window;                  /* slave window */
    GEF_UINT32 offset;                              /* offset from the slave window address */
};
typedef	struct GEF_VME_SLAVE_OSSPEC vmeSlaveHandle_t;


struct GEF_VME_DMA_OSSPEC {
	int magic;
	dma_addr_t resource;
	void *vptr;
	void *phys_addr;
	void *pci_addr;
	size_t size;
	struct GEF_VME_DMA_OSSPEC *next;                /* Pointer to the next handle */
};
typedef	struct GEF_VME_DMA_OSSPEC vmeDmaHandle_t;

#endif	/* VMEDRV_H */

