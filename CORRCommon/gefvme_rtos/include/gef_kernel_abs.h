/*
 * File:
 *    gef_kernel_abs.h
 *
 * Description:
 *    OS specific functions used by driver files
 *
 * Copyright:
 *    Copyright 2007 GE Fanuc Embedded Systems, Inc.
 *
 * License:
 *    This file is proprietary to GE Fanuc Embedded Systems, Inc.
 *
 *
 * $Date$
 * $Rev$
 */

/* Struct pt_regs */
#include <asm/uaccess.h>
/* Driver includes */
#include "ca91c042.h"
#include "tsi148.h"
/* Linux function definitions */
#include "gef/gefcmn_vme_framework.h"
/* Standard vme includes */
#include "gef/gefcmn_vme_errno.h"
#include "gef/gefcmn_vme_uni.h"
#include "gef/gefcmn_vme_tempe.h"
/* ioread/iowrite functions */
#include <asm/io.h>
/* Semaphores */
#include <linux/version.h>
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2, 6, 26)
#include <linux/semaphore.h>
#else
#include <asm/semaphore.h>
#endif

extern char *vmechip_baseaddr;
extern void vmeSyncData(void);

//----------------------------------------------------------------------------
//  Common functions for abstraction purposes
//  spinlocks are externed from the driver files
//----------------------------------------------------------------------------
extern spinlock_t tempe_lock;
extern spinlock_t uni_lock;

#define GEF_VME_MAX_WAIT_VOWN 1000
#define WAIT_VOWN_ACK(x)        \
{                               \
    int count = x;              \
    while(count>0){ --count; }  \
}

void gefUnivSpinlockAcquire(void);
void gefUnivSpinlockRelease(void);
void gefTempeSpinlockAcquire(void);
void gefTempeSpinlockRelease(void);

int gefSemTakeInterruptible(void);
void gefSemTake(void);
void gefSemGive(void);

/* Typedefed to readl, readw, readb in vmedrv.h */
#define GEF_IOREAD32(addr) (ioread32(vmechip_baseaddr + addr))
#define GEF_IOREAD16(addr) (ioread16(vmechip_baseaddr + addr))
#define GEF_IOREAD8(addr)  ( ioread8(vmechip_baseaddr + addr))

/* Typedefed to writel, writew, writeb in vmedrv.h */
#define GEF_IOWRITE32(data,addr) (iowrite32(data,vmechip_baseaddr + addr))
#define GEF_IOWRITE16(data,addr) (iowrite16(data,vmechip_baseaddr + addr))
#define GEF_IOWRITE8(data,addr)  ( iowrite8(data,vmechip_baseaddr + addr))


#define GEF_VME_IO_SYNC() (vmeSyncData())
