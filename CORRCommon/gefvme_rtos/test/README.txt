$Id$

DESCRIPTION TEST 1
The gefvme_rtosTest module first initialises the VME system. Then an RTAI
task is started, which waits for an IRQ to be generated. The IRQ is generated
in the same function where the task is created. If the task does not acknowledge
the IRQ, the test fails. After this, the task deletes itself and the test
continous with acquiring the VME bus ownership. After successful acquisition,
the ownership is released and the test passed.
PARAMETER: irqTimeout, default = 10000[us].  This allows to set shorter timeout
values for the IRQ acknowledgement.  This parameter is made available so
that the latency of hardware IRQ handling can be established.  The default value
is a sane choice for both bridges, Tundra Universe II and Tundra Tempe TSI 148.
OUTCOME: Test report in /var/log/messages.

DESCRIPTION TEST 2
Standard endurance test. It loads the base set of modules and then it
loads and unloads the vme_universe_rtai module for 100 times.
OUTCOME: Test report in /var/log/messages.
