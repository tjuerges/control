/* @(#) $Id$
*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2010
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* gefvme_rtosTestModule
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  2010-02-05 created
*/

/************************************************************************
*   NAME
* gefvme_rtosTestModule
*
*   SYNOPSIS
* Kenel module to test the Tundra Universe 2 and the Tundra Tempe TSI 148 VME
* to PCI bus bridges.
*
*   FILES
* gefvme_rtosTestModule.c
*
*   RETURN VALUES
* Returns RT_TOOLS_MODULE_INIT_SUCCESS defined in
* CONTROL/CORRCommon/rtTools/include/rtTools.h if the test is successful.
*
*   SEE ALSO
* CONTROL/CORRCommon/gefvme_rtos
*
*/


#include <linux/module.h>
#include <asm/atomic.h>

#include <gefanuc_kernelApi.h>

#ifdef USE_RTOS
#include <rtai_sched.h>
#include <rtLog.h>
#include <rtTools.h>
#else
#include <linux/kthread.h>
#endif

#include <linux/string.h>


#define moduleName "gefvme_rtosTestModule"


MODULE_AUTHOR("Thomas Juerges <tjuerges@nrao.edu>");
MODULE_DESCRIPTION(moduleName ": Test kernel module for the Tundra Universe 2 "
    "and the Tundra Tempe TSI 148 VME-PCI bridges.");
MODULE_LICENSE("GPL");


static unsigned int irqTimeout = 10000U;
module_param(irqTimeout, uint, S_IRUGO);
MODULE_PARM_DESC(irqTimeout, "Timeout in [us] for a generated IRQ to be "
    "acknowledged, default = 10000[us].");
static const GEF_VME_INT_LEVEL irqLevel = GEF_VME_INT_VIRQ1;
static const GEF_UINT32 irqVector = 20;
/* IRQ timeout {s, us }. */

static struct timeval timeout = {0, 0};
static const unsigned int successfulResults = 6U;
static atomic_t result = ATOMIC_INIT(0), finished = ATOMIC_INIT(1);
static atomic_t IRQFinished = ATOMIC_INIT(1);
#ifdef USE_RTOS
static RT_TASK taskID, irq_task;
#else
static struct task_struct* irq_task;
#endif


/**
 * RTOS task which serves as IRQ handler.
 * vme_interrupt_attach blocks until the specified IRQ is
 * activated.
 */
#ifdef USE_RTOS
static void irq_handler(long junk __attribute__((unused)))
#else
static int irq_handler(void* foo __attribute__((unused)))
#endif
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

    int status = -1;

    GEF_VME_DRV_WAIT_INFO irqInfo;
    memset(&irqInfo, 0, sizeof(GEF_VME_DRV_WAIT_INFO));
    irqInfo.int_info.int_level = irqLevel;
    irqInfo.int_info.int_vector = irqVector;
    irqInfo.timeout = timeout;
 
    #ifdef USE_RTOS
    RTLOG_INFO(moduleName, "IRQ handler: IRQ task active...");
    #else
    RTLOG_INFO(moduleName, "IRQ handler: IRQ kernel thread active...");
    set_current_state(TASK_INTERRUPTIBLE);
    #endif

    RTLOG_INFO(moduleName, "IRQ handler: Calling vmeWaitForIrq...");

    status = vmeWaitForIrq(&irqInfo);
    if(status != GEF_SUCCESS)
    {
        RTLOG_ERROR(moduleName, "vmeWaitForIrq failed");
    }
    else
    {
        /* Acknowledge IRQ. */
        status = vmeWaitIrqAck(&irqInfo);
        if(status != GEF_SUCCESS)
        {
            RTLOG_ERROR(moduleName, "Could not acknowledge the IRQ, status = "
                "0x%x.", status);
        }
        else
        {    
            atomic_add(1, &result);
        }
    }

    atomic_set(&IRQFinished, 0);
    #ifdef USE_RTOS
    rt_task_delete(rt_whoami());
    #else
    return 0;
    #endif
}


#ifdef USE_RTOS
static void runThread(long junk __attribute__((unused)))
#else
static void runThread(void)
#endif
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

    int status = RT_TOOLS_MODULE_INIT_ERROR;
    int geStatus = -1;
    #ifdef USE_RTOS
    const int STACK_SIZE = 3000;
    const int PRIORITY = RT_SCHED_LOWEST_PRIORITY - 1;
    #endif

    RTLOG_INFO(moduleName, "Test task running...");

    int bridgeType = getBridgeType();
    RTLOG_INFO(moduleName, "Tundra bridge type = %s.",
    ((bridgeType == PCI_DEVICE_ID_TUNDRA_CA91C042) ? ("Universe II") : ("Tempe TSI 148")));

    #ifdef USE_RTOS
    RTLOG_INFO(moduleName, "Create IRQ handler task...");
    if(rt_task_init(&irq_task, irq_handler, 0, STACK_SIZE, PRIORITY, 1, 0) != 0)
    {
        RTLOG_ERROR(moduleName, "Could not create the IRQ handler task.");
        goto LeaveTest;
    }
    else
    {        
        if(rt_task_resume(&irq_task) != 0)
        {
            RTLOG_ERROR(moduleName, "Could not resume the IRQ handler task.");
            goto LeaveTest;
        }
        else
        {
            atomic_add(1, &result);
            RTLOG_INFO(moduleName, "IRQ handler task resumed.");
            
        }
    }
    #else
    RTLOG_INFO(moduleName, "Create IRQ handler kernel thread...");
    irq_task = kthread_run(irq_handler, 0, "ALMA-VME-IRQ_handler");
    if(irq_task == ERR_PTR(-ENOMEM))
    {
        RTLOG_ERROR(moduleName, "Could not create the IRQ handler kernel "
            "thread.");
        goto LeaveTest;
    }
    else
    {
        atomic_add(1, &result);
        RTLOG_INFO(moduleName, "IRQ handler kernel thread resumed.");
    }
    #endif

    GEF_VME_DRV_VIRQ_INFO irqInfo;
    memset(&irqInfo, 0, sizeof(GEF_VME_DRV_VIRQ_INFO));
    irqInfo.level = irqLevel;
    irqInfo.vector = irqVector;
    irqInfo.timeout = timeout;

    geStatus = vmeGenerateIrq(&irqInfo);
    if(geStatus != GEF_SUCCESS)
    {
        RTLOG_ERROR(moduleName, "vme_interrupt_generate failed, status = "
            "0x%x.", geStatus);
        goto LeaveTest;
    }
    else
    {
        atomic_add(1, &result);
        RTLOG_INFO(moduleName, "vme_interrupt_generate sucessful.");
    }

    /* Allow 100us for IRQ delivery. */
    #ifdef USE_RTOS
    rt_sleep(nano2count(100000ULL));
    #else
    udelay(100);
    #endif

    if(atomic_read(&IRQFinished) != 0)
    {
        RTLOG_ERROR(moduleName, "VME IRQ handling unsuccessful.");
        goto LeaveTest;
    }
    else
    {
        atomic_add(1, &result);
        RTLOG_INFO(moduleName, "VME IRQ handling successful.");
    }

    GEF_VME_DRV_VOWN_INFO handle;
    memset(&handle, 0, sizeof(GEF_VME_DRV_VOWN_INFO));
    handle.timeout = timeout;

    geStatus = vmeAcquireBusOwnership(&handle);
    if(geStatus != GEF_SUCCESS)
    {
        RTLOG_ERROR(moduleName, "vme_acquire_bus_ownership failed, status = "
            "0x%x.", geStatus);
        goto LeaveTest;
    }
    else
    {
        atomic_add(1, &result);
        RTLOG_INFO(moduleName, "vme_acquire_bus_ownership successful.");
    }

    geStatus = vmeReleaseBusOwnership();
    if(geStatus != GEF_SUCCESS)
    {
        RTLOG_ERROR(moduleName, "vme_release_bus_ownership failed, status = "
            "0x%x.", geStatus);
    }
    else
    {
        atomic_add(1, &result);
        RTLOG_INFO(moduleName, "vme_release_bus_ownership successful.");
    }


LeaveTest:
    if(atomic_read(&result) == successfulResults)
    {
        status = RT_TOOLS_MODULE_INIT_SUCCESS;
    }

    RTLOG_INFO(moduleName, "vme_universe_rtai test %s.",
        (status == RT_TOOLS_MODULE_INIT_SUCCESS ? "SUCCESSFUL" : "FAILED"));

    if(atomic_read(&IRQFinished) != 0)
    {
        #ifdef USE_RTOS
        rt_task_suspend(&irq_task);
        rt_task_delete(&irq_task);
        #else
        kthread_stop(irq_task);
        #endif
    }

    atomic_set(&finished, 0);
    #ifdef USE_RTOS
    rt_task_delete(rt_whoami());
    #endif
};

static int __init gefvme_rtosTestModule_init(void)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

    int stat = RT_TOOLS_MODULE_INIT_ERROR;

    RTLOG_INFO(moduleName, "Running vme_universe_rtai test...");

    memset(&timeout, 0, sizeof(struct timeval));
    timeout.tv_usec = irqTimeout;
    RTLOG_INFO(moduleName, "IRQ timeout = %u[us].", irqTimeout);

    #ifdef USE_RTOS
    /**
     * Initialise the test task.
     */
    if(rt_task_init(&taskID, runThread, 0, 2000, RT_SCHED_LOWEST_PRIORITY,
        0, 0)
    != 0)
    {
        RTLOG_ERROR(moduleName, "Could not start the test task!");
    }
    else
    {
        /**
         * And then start it,
         */
        rt_task_resume(&taskID);

        stat = RT_TOOLS_MODULE_INIT_SUCCESS;

        RTLOG_INFO(moduleName, "Module initialized successfully.");
    }
    #else
    /**
     * Call the test function.
     */
    runThread();
    stat = RT_TOOLS_MODULE_INIT_SUCCESS;
    #endif
    return stat;
};

static void __exit gefvme_rtosTestModule_exit(void)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

    if(atomic_read(&finished) != 0)
    {
        #ifdef USE_RTOS
        rt_task_suspend(&taskID);
        rt_task_delete(&taskID);
        #endif
    }

    RTLOG_INFO(moduleName, "Unloaded.");
};

module_init(gefvme_rtosTestModule_init);
module_exit(gefvme_rtosTestModule_exit);
