/* @(#) $Id$
 * Copyright (C) 2001
 * Associated Universities, Inc. Washington DC, USA.
 *
 * Produced for the ALMA project
 *
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU Library General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is distributed in the hope that it will be useful but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public 
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License 
 * along with this library; if not, write to the Free Software Foundation, 
 * Inc., 675 Massachusetts Ave, Cambridge, MA, 02139, USA.
 *
 * Correspondence concerning ALMA should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */


/*
 * This is a simple test module which verifies that the DEParallelDrv
 *  routine is working
 */


#include <linux/moduleparam.h>
#include <linux/version.h>
#include <linux/time.h>
#include <linux/delay.h>
#include <asm/atomic.h>

#ifdef USE_RTOS
#include <rtai_sem.h>
#include <rtTools.h>
#include <rtLog.h>
#endif

#include <DEParallelDrv.h>


extern int enableTE(void(*)(int));
extern int disableTE(void);


#define moduleName "DEParallelDrvTest"


MODULE_AUTHOR("Thomas Juerges <tjuerges@nrao.edu>");
MODULE_DESCRIPTION(moduleName "Time event interrupt handler test.");
MODULE_LICENSE("GPL");


static int targetCounts = 1000;
module_param(targetCounts, int, S_IRUGO);

static atomic_t IRQcounter = ATOMIC_INIT(0);
static atomic_t finished = ATOMIC_INIT(-1);

static int lower = 0;
static int upper = 0;
static int subtractFromIRQcounter = 0;

/**
 * cpu_khz is an exported symbol.
 */
static unsigned long long cpuSpeed = 0ULL;

static long long tsc_diff = 0LL;
static long long tsc_diff_low = 0LL;
static long long tsc_diff_high = -1LL;
static long long tsc = 0LL;
static long long tsc_previous = 0LL;

static struct timeval start_tv = {0, 0};
static struct timeval end_tv = {0, 0};
static unsigned long long duration = 0ULL;

#ifdef USE_RTOS
static RT_TASK taskID;
#endif


static void callbackFunction(int trash __attribute__((unused)))
{
    atomic_inc(&IRQcounter);

    /**
     * Check the TSC difference.
     */
    #ifdef USE_RTOS
    tsc = rtai_rdtsc();
    #else
    rdtscll(tsc);
    #endif

    if(tsc_previous != 0LL)
    {
        tsc_diff = tsc - tsc_previous;

        if(tsc_diff < tsc_diff_low)
        {
            tsc_diff_low = tsc_diff;
        }

        if(tsc_diff > tsc_diff_high)
        {
            tsc_diff_high = tsc_diff;
        }
    }

    tsc_previous = tsc;
}


#ifdef USE_RTOS
static void runThread(long junk __attribute__((unused)))
#else
static void runThread(void)
#endif
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif
    int i = 0, counter = 0, status = RT_TOOLS_MODULE_INIT_ERROR;

    RTLOG_INFO(moduleName, "Test task (address = 0x%p) running...",
        rt_whoami());

    /**
     * Get current time stamp.
     */
    do_gettimeofday(&start_tv);

    /**
     * Register TE IRQ callback function.
     */
    enableTE(callbackFunction);

    /**
     * Just in case:  subtract the current number of already counted TEs,
     * i.e. we started just before a TE, from the end result.  See below.
     */
    subtractFromIRQcounter = atomic_read(&IRQcounter);

    /**
     * Sleep for targetCounts TEs, each lasting 48000000ns.  The atomic
     * counter "IRQcounter" should then be between targetCounts - 1 and
     * targetCounts + 1.
     */
    for(; ((i < targetCounts) && (atomic_read(&finished) < 1)); ++i)
    {
        #ifdef USE_RTOS
        rt_sleep(nano2count(48000000ULL));
        #else
        msleep_interruptible(48U);
        #endif
    }

    /**
     * Disable the TE counting.
     */
    disableTE();

    /**
     * Get current time stamp.
     */
    do_gettimeofday(&end_tv);

    /**
     * Read the counter and subtract all TEs which happened before the loop.
     */
    counter = atomic_read(&IRQcounter);
    counter -= subtractFromIRQcounter;

    /**
     * Calculate the duration of the run.  Subtract the start_tv.tv_usec last
     * in case there is an underflow and 1s needs to be subtracted, too.
     */
    duration = (end_tv.tv_sec - start_tv.tv_sec) * 1000000ULL;
    duration += end_tv.tv_usec;
    duration -= start_tv.tv_usec;

    /**
     * Now calculate the number of expected TEs during the calculated
     * "duration".  To do so divide "duration" by 48000us.  The 1ULL is just a
     * multiplication factor to make llimd happy.
     */
    targetCounts = (int)llimd(duration, 1ULL, 48000ULL);

    /**
     * If the new number for the expected IRQs is different than the one which
     * was initially used, update the lower and upper bounds for the counted
     * IRQs.
     */
    if((targetCounts + 1) != upper)
    {
        lower = targetCounts - 1;
        upper = targetCounts + 1;
        RTLOG_INFO(moduleName, "The number of expected IRQs has changed due to "
            "different duration than anticipated.  The number of expected IRQs "
            "is now = %d.", targetCounts);
    }

    /**
     * If the counted TEs is within [lower; upper], the test is
     * successful.
     */
    if((lower <= counter) && (counter <= upper))
    {
        status = RT_TOOLS_MODULE_INIT_SUCCESS;
    }

    /**
     * Print out some results.
     */
    RTLOG_INFO(moduleName, "DEParallelDrv test done, ran for %llu[us].",
        duration);
    RTLOG_INFO(moduleName, "TSC-difference:  min = %lld, max = %lld, cpu "
        "speed = %llu.",
        tsc_diff_low, tsc_diff_high, cpuSpeed);

    /**
     * Check the result.
     */
    if(status == RT_TOOLS_MODULE_INIT_SUCCESS)
    {
        RTLOG_INFO(moduleName, "Got %d IRQs, subtracted %d too early IRQs.",
            counter, subtractFromIRQcounter);
    }
    else
    {
        RTLOG_INFO(moduleName, "Did not catch enough or too many IRQs. "
            "Should be between %u and %u, but is %u.",
            lower, upper, counter);
    }

    RTLOG_INFO(moduleName, "DEParallelDrv test %s.",
        (status == RT_TOOLS_MODULE_INIT_SUCCESS ? "SUCCESSFUL" : "FAILED"));

    /**
     * Signal that the test task is done.
     */
    atomic_set(&finished, 0);

    #ifdef USE_RTOS
    rt_task_delete(rt_whoami());
    #else
    return 0;
    #endif
}

static int __init DEParallelDrvTest_init(void)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

    int stat = RT_TOOLS_MODULE_INIT_ERROR;

    RTLOG_INFO(moduleName, "Running DEParallelDrv test...");

    cpuSpeed = cpu_khz * 1000LL;
    tsc_diff_low = cpuSpeed;

    lower = targetCounts - 1;
    upper = targetCounts + 1;
    RTLOG_INFO(moduleName, "This test will count %d TE IRQs.  Please allow "
        "this test to run for at least %d[ms].  CPU speed = %lld.",
        targetCounts, targetCounts * 48, cpuSpeed);
    
    #ifdef USE_RTOS
    /**
     * Initialise the test task.
     */
    if(rt_task_init(&taskID, runThread, 0, 2000, 3, 0, 0) != 0)
    {
        RTLOG_ERROR(moduleName, "Could not start the test task!");
    }
    else
    {
        /**
         * And then start it,
         */
        rt_task_resume(&taskID);

        stat = RT_TOOLS_MODULE_INIT_SUCCESS;

        RTLOG_INFO(moduleName, "Module initialized successfully, test task "
            "(address = 0x%p) started.", &taskID);
    }
    #else
    runThread();
    stat = RT_TOOLS_MODULE_INIT_SUCCESS;
    RTLOG_INFO(moduleName, "Module initialized successfully.");
    #endif

    return stat;
}

static void __exit DEParallelDrvTest_exit(void)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

    if(atomic_read(&finished) != 0)
    {
        /**
         * Tell the test task to finish up and wait one second before
         * forcefully suspending & deleting it.
         */
        atomic_set(&finished, 1);
        msleep_interruptible(1000U);

        #ifdef USE_RTOS
        rt_task_suspend(&taskID);
        rt_task_delete(&taskID);
        #endif

        RTLOG_INFO(moduleName, "IRQcounter = %d, "
            "atomic_read(&finished) = %d", atomic_read(&IRQcounter),
            atomic_read(&finished));

    }

    RTLOG_INFO(moduleName, "Module cleaned up successfully.");
}

module_init(DEParallelDrvTest_init);
module_exit(DEParallelDrvTest_exit);
