$Id$

DESCRIPTION TEST 1
This test registers an IRQ handler function through the avme9670drv module. Then,
the test waits for 48ms * 100 in order to let the handler function increase an
atomic value by one. After this timespan, the handler is unregistered and the
atomic value compared to be in the interval [99; 101]. The values 99 and 101 are
checked, too, to make the test more robust for any kind of timing jitter in the
Additional hardware required! Make sure, a real TE signal is connected to the
VME crate! At the AOC, the TEs are generated through the AMBSI2 test device.
RTAI system.
OUTCOME: Test report in /var/log/messages.

DESCRIPTION TEST 2
Standard endurance test. It loads the base set of modules and then it
loads and unloads the DEParallelDrv module for 100 times.

DESCRIPTION TEST 3
This test is not executed automatically but requires a human.  Load the
resetTest.lkm file and measure pins 1 and 6 on each AMB channel.  The reset
lines should be triggered every 20ms.  No output in /var/log/messages!
After the lines have been measured, just unload the lkm file and the kernel
module will stop triggering the reset lines.
Caveat:  it is not advisable to have an AMBSI connected to any channel while
this test is executed.
OUTCOME: Test report in /var/log/messages.
