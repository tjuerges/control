/* @(#) $Id$
 * Copyright (C) 2001
 * Associated Universities, Inc. Washington DC, USA.
 *
 * Produced for the ALMA project
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is distributed in the hope that it will be useful but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 675 Massachusetts Ave, Cambridge, MA, 02139, USA.
 *
 * Correspondence concerning ALMA should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */

#ifndef DEParallelDrv_h
#define DEParallelDrv_h

/**
 * Forward declarations
 */
void resetChannel(int channel);
#ifdef DEBUG_RESET_CHANNEL
void debugResetChannel(unsigned int channel, unsigned int onOff);
#endif
int enableTE(void (*handler)(int));
int disableTE(void);

#if !defined(USE_RTOS) && !defined(_RTLOG_)
#define RTLOG_ERROR(moduleName, format, args...)\
    {\
        printk(KERN_ERR "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }
#define RTLOG_WARNING(moduleName, format, args...)\
    {\
        printk(KERN_WARNING "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }
#define RTLOG_INFO(moduleName, format, args...)\
    {\
        printk(KERN_INFO "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }
#define RTLOG_DEBUG(moduleName, format, args...)\
    {\
        printk(KERN_DEBUG "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }

#define RT_TOOLS_MODULE_STAGE_INIT 1
#define RT_TOOLS_MODULE_STAGE_EXIT 2
#define RT_TOOLS_MODULE_STAGE_CLEANUP 3

#define RT_TOOLS_MODULE_INIT_SUCCESS 0
#define RT_TOOLS_MODULE_INIT_SUCCESS 0
#define RT_TOOLS_MODULE_EXIT_SUCCESS 0

#define RT_TOOLS_MODULE_ERROR_BASE 256
#define RT_TOOLS_MODULE_INIT_ERROR -(RT_TOOLS_MODULE_ERROR_BASE + 1)
#define RT_TOOLS_MODULE_INIT_TIMEOUT -(RT_TOOLS_MODULE_ERROR_BASE + 2)
#define RT_TOOLS_MODULE_CLEANUP_ERROR -(RT_TOOLS_MODULE_ERROR_BASE + 3)
#define RT_TOOLS_MODULE_NO_INIT_TASK_ERROR -(RT_TOOLS_MODULE_ERROR_BASE + 4)
#define RT_TOOLS_MODULE_INIT_EXIT_ERROR -(RT_TOOLS_MODULE_ERROR_BASE + 5)
#define RT_TOOLS_MODULE_PARAM_ERROR -(RT_TOOLS_MODULE_ERROR_BASE + 6)
#endif
#endif
