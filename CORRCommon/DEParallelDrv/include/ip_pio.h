#ifndef IP_PIO_H
#define IP_PIO_H
/* @(#) $Id$
 * Copyright (C) 2001
 * Associated Universities, Inc. Washington DC, USA.
 *
 * Produced for the ALMA project
 *
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU Library General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is distributed in the hope that it will be useful but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public 
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License 
 * along with this library; if not, write to the Free Software Foundation, 
 * Inc., 675 Massachusetts Ave, Cambridge, MA, 02139, USA.
 *
 * Correspondence concerning ALMA should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */


#include <linux/types.h>


/**
 *  Dynamic Engineering Parallel-IO-3 module relative address map.
 */
typedef struct
{
    uint16_t cntl0;    /* 0x0 */
    uint16_t cntl1;    /* 0x2 */
    uint16_t cntl2;    /* 0x4 */
    uint16_t Base_cntl;    /* 0x6 */
    uint16_t Int_En0;    /* 0x8 */
    uint16_t Int_En1;    /* 0xA */
    uint16_t Int_En2;    /* 0xC */
    uint16_t vector;    /* 0xE */
    uint16_t Int_EdgLvl0;    /* 0x10 */
    uint16_t Int_EdgLvl1;    /* 0x12 */
    uint16_t reserved1;    /* 0x14 */
    uint16_t _unused0;    /* 0x16 */
    uint16_t Int_Pol0;   /* 0x18 */
    uint16_t Int_Pol1;   /* 0x1A */
    uint16_t Int_Pol2;    /* 0x1C */
    uint16_t status;    /* 0x1E */
    uint16_t dat_in_fil0;    /* 0x20 */
    uint16_t dat_in_fil1;    /* 0x22 */
    uint16_t dat_in_fil2;    /* 0x24 */
    uint16_t _unused1;    /* 0x26 */
    uint16_t dat_in_dir0;    /* 0x28 */
    uint16_t dat_in_dir1;    /* 0x2A */
    uint16_t dat_in_dir2;    /* 0x2C */
    uint16_t pre_load_l;    /* 0x2E */
    uint16_t pre_load_u;    /* 0x30 */
    uint16_t mask_l;    /* 0x32 */
    uint16_t mask_u;    /* 0x34 */
    uint16_t rdbk_l;    /* 0x36 */
    uint16_t rdbk_u; /* 0x38 */
    uint16_t _unused2[35];   /* 0x3A */
    uint16_t id;        /* 0x80 */
} PIO_Card;
#endif
