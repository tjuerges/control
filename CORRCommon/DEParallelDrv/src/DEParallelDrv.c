/*******************************************************************************
*
* "@(#) $Id$"
*
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* who       when        what
* --------  ----------  ----------------------------------------------
* $Author$  $Date$      Modified    
*/


/*
 * System stuff
 */
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <asm/atomic.h>
#include <asm/io.h>
#include <linux/delay.h>
#include <linux/proc_fs.h>

/*
 * RTAI stuff
 */
#ifdef USE_RTOS
#include <rtai_sched.h>
#include <rtai_sem.h>
#include <rtLog.h>
#include <rtTools.h>
#else
#include <linux/kthread.h>
#include <linux/delay.h>
#include <asm/semaphore.h>
#endif


/*
 * Local stuff
 */
#include <DEParallelDrv.h>
#include <avme9670drv.h>
#include "ip_pio.h"



/**
 * The VME IRQ used for the Parallel-IO IP board.
 */
#define VME_IRQ VME_INTERRUPT_VIRQ5

/**
 * The VME IRQ vector that is used for the Parallel-IO board.
 */
///
/// TODO
/// Thomas, May 21, 2009
/// I could not find any documentation why vector 200 is used.  It seems to be
/// a random number chosen because some other project or other OS (VxWorks?)
/// had to use vector 200.
///
#define PIO_VECTOR 200

/**
 * PIO card base control register bits
 */
/**
 * The IP card generates only INT0 IRQs and the module
 * is always installed in slot A.  This bit represents this
 * configuration.
 */
#define IP_IRQ IPA_INT0
/**
 * Outputs follow register update bit.
 */
#define AUTO_UPDATE 0x0001U
/**
 * Master interrupt enable bit.
 */
#define INTERRUPT_ENBL 0x0002U
/**
 * Force interrupt.
 */
#define FORCE_INTERRUPT 0x0004U
/**
 * Bit number of the TE IRQ source channel.
 */
#define TE_SOURCE_CHANNEL 15


#define moduleName "DEParallelDrv"


#ifdef USE_RTOS
#define DEBUG_VALUE(a) { rtlogRecord_t logRecord; RTLOG_INFO(moduleName, "value of %s = %xu ", #a , a); }
#else
#define DEBUG_VALUE(a) printk("%s: value of %s = %xu ", moduleName, #a , a);
#endif


MODULE_AUTHOR("Thomas Juerges <tjuerges@nrao.edu>");
MODULE_DESCRIPTION(moduleName "Kernel module for the Dynamic Engineering "
    "Parallel IO-4 IP board.");
MODULE_LICENSE("GPL");


/*
 * Module parameters.
 */
static unsigned int initTimeout = 1000U; /* Unit is ms. */
static unsigned int cleanUpTimeout = 1000U; /* Unit is ms. */
static unsigned int semaphoreTimeout = 100U; /* Unit is us. */
module_param(initTimeout, uint, S_IRUGO);
module_param(cleanUpTimeout, uint, S_IRUGO);
module_param(semaphoreTimeout, uint, S_IRUGO);


/*
 * Local data.
 */
#ifdef USE_RTOS
static RT_TASK te_handler_task;
#define TE_HANDLER_STACK_SIZE 3000
#define TE_HANDLER_PRIORITY 0
static RTIME SEMAPHORE_TIMEOUT = 1000ULL; // 1us

static char softwareVersion[] =
    "$Id$";


/**
 * Data structure for the /proc page.
 */
static struct proc_dir_entry* procEntry = 0;


/**
 * If the single board computer does not do the endian conversion between
 * x86 and VME, I have to do it myself.
 */
static unsigned int wrongEndianess = 0U;

/**
 * Protect VME register access with this semaphore.
 */
static SEM accessSem;

#if __GNUC__ < 4
/**
 * Read/write lock for the pointer to the IRQ handler.  Only used on GCC < 4.
 */
static SEM readWriteLock;
#endif
#else
static struct task_struct* te_handler_task;

static DECLARE_MUTEX(accessSem);
#if __GNUC__ < 4
static DECLARE_MUTEX(readWriteLock);
#endif
#endif

/**
 * Exit flag.
 */
static atomic_t exitISRLoop = ATOMIC_INIT(0);

/**
 * IRQ client function.
 */
static void (*handlerFunction)(int) = 0;

/**
 * Number of TEs since a handler function has been registered.
 */
static atomic_t numberOfTesWithHandler= ATOMIC_INIT(0);
/**
 * Number of TEs without a handler function.
 */
static atomic_t numberOfTesWithoutHandler= ATOMIC_INIT(0);
/**
 * Atomic flag which signals if the TE IRQ handler is executing.
 * Set to 1 if in the handler function, set to 0 if not.
 */
static atomic_t inIrqHandler = ATOMIC_INIT(0);
/**
 * Atomic flag which signals if the TE IRQ handler is still in the
 * registered client function or not.  Set to 1 if in the handler
 * function, set to 0 if not.
 */
static atomic_t inRegisteredHandler = ATOMIC_INIT(0);
/**
 * Atomic counter for "double IRQs".  See below in checkValidIRQ
 * for an explanation of double IRQs.
 */
static atomic_t numberOfDoubleIrqs = ATOMIC_INIT(0);


/**
 * VME carrier board related variables.
 */
static vme_bus_handle_t mbus_handle = 0;
static vme_master_handle_t mwindow_handle = 0;
static vme_resource_level_t vme_resource_level = vme_res_none;
static volatile BOARD_MEMORY_MAP* carrier = 0;
static volatile PIO_Card* pio = 0;
// Disabled because the IRQ is now cleared in the Parallel board during
// the validation of the IRQ.
#if 0
static int int_clear_result = 0;
#endif

/**
 * This is needed to keep track of the init level which the
 * init task successfully finished.  I added this because the
 * VME subsytem is initialised in an RTtask.
 */
static int initLevel = 0;

/**
 * Dynamic Engineering Parallel-IO-4 module identification string.
 */
static unsigned char ip_parallel_id[] =
{
    'I',
    'P',
    'A',
    'H',
    0x1eU,
    0x03U,
    0xa0U,
    0x00U,
    0x00U,
    0x04U,
    0x0cU,
    0xa8U
};



static int readProcFsPage(char* buffer, char** start, off_t offset, int length,
    int* eof, void* data)
{
    int size = 0;
    void (*handlerAddress)(int) = 0;


    #if __GNUC__ >= 4
    /**
     * Atomically read the handler pointer from GCC 4.1.2 on.
     */
    handlerAddress = __sync_fetch_and_or(&handlerFunction, 0);
    #else
    handlerAddress = handlerFunction;
    #endif

    size += sprintf(buffer + size, "%s kernel module information page\n\n"
        "Software version = %s\n\n"
        "Currently executing DEParallelDrv IRQ handler: %s\n"
        "TE IRQ handler address = ", moduleName, softwareVersion,
        ((atomic_read(&inIrqHandler) == 1) ? ("yes") : ("no")));

    if(handlerAddress != 0)
    {
        size += sprintf(buffer + size, "0x%x\nCurrently executing registered "
            "handler function: %s.",
            (unsigned int)handlerAddress,
            ((atomic_read(&inRegisteredHandler) == 1) ? ("yes") : ("no")));
    }
    else
    {
        size += sprintf(buffer + size, "none registered");
    }

    uint32_t doubleIrqs = atomic_read(&numberOfDoubleIrqs);
    uint32_t tesWithout = atomic_read(&numberOfTesWithoutHandler);
    uint32_t tesWith = atomic_read(&numberOfTesWithHandler);

    size += sprintf(buffer + size, "\n\nNumber of \"double IRQs\" (not "
        "handled) = %u.\n"
        "Number of TEs while no handler was registered = %u.\n"
        "Number of TEs while a handler was registered = %u.\n"
        "Total number of TEs = %lu.\n",
        doubleIrqs,
        tesWithout,
        tesWith,
        (unsigned long)tesWithout + tesWith);

    return size;
}


inline static uint16_t correctEndianess(uint16_t value)
{
    uint16_t ret = value;

    if(wrongEndianess == 1U)
    {
        ret = (((value & 0xff00U) >> 8U) | ((value & 0x00ffU) << 8U));
    }

    return ret;
}


/**
 * Check if a "double" IRQ occured.  The hardware should not allow for that,
 * but, well, it is how it is.  This was first seen on the GEFanuc VMIVME 7807
 * 1.8GHz single board computers that I had here for testing.  The 1.1GHz model
 * did not show this behaviour.
 *
 * It happened on these computers that every TE IRQ triggered another TE IRQ
 * in the realtime code just some 10e-6s later, normally about 18e-6s.  It
 * turned out that the 1.8GHz computer is fast enough to recognise the same
 * IRQ again before the command to clear the IRQ has been executed by either
 * the AVME-9670 or the Parallel-IO-4 hardware, keeping the IRQ line active.
 *
 * Here the IRQ pending register of the AVME carrier board is checked for a
 * pending IRQ.  Since the AVME signals on the 1.8GHz system only every other
 * IRQ, which is correct, the culprit seems to be the Parallel-IO-4 board.
 *
 * 2010-02-10:  Added a checked against the IRQ register in the Parallel-IO
 * board.
 */
static int checkValidIRQ(void)
{
    if(((carrier->intPending & correctEndianess(IP_IRQ)) != 0)
    && ((pio->dat_in_fil1 & correctEndianess(
        0x0001U << TE_SOURCE_CHANNEL)) != 0))
    {
        return 1;
    }

    return 0;
}


/**
 * IRQ clearer. Called within the ISR. So, do not use scheduler related stuff
 * like timed semaphores!
 */
static void interrupt_clear(void)
{
// Disabled for the 6200 board.
#if 0
    /**
     * Disable interupts from the PIO CARD.
     */
    pio->Base_cntl &= correctEndianess((uint16_t)~INTERRUPT_ENBL);
    carrier->intEnable &= correctEndianess(~IP_IRQ);
#endif

    /**
     * Clear the interupt.
     */
    carrier->intClear |= correctEndianess(IP_IRQ);

// Disabled for the 6200 board.
#if 0
    /**
     * Turn the interrupts back on.
     */
    carrier->intEnable |= correctEndianess(IP_IRQ);
    pio->Base_cntl |= correctEndianess(INTERRUPT_ENBL);
#endif
}

/*
 * API
 */
void resetChannel(int channel)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;

    int stat = rt_sem_wait_timed(&accessSem, SEMAPHORE_TIMEOUT);
    if((stat == SEM_ERR) || (stat == SEM_TIMOUT))
    #else
    int stat = 0;
    
    if((stat = down_interruptible(&accessSem)) != 0)
    #endif
    {
        RTLOG_WARNING(moduleName, "Failed to take API semaphore (stat = %d) "
            "in function resetChannel. Expect instabilities!", stat);
    }
    else
    {
        /**
         * Hold reset high for 1.5ms (RTOS) or 2ms when running under pure
         * Linux.
         */
        pio->cntl1 &= correctEndianess(~(0x0001U << channel));

        #ifdef USE_RTOS
        rt_sleep(nano2count(1500000ULL));
        #else
        msleep_interruptible(2U);
        #endif

        pio->cntl1 |= correctEndianess((0x0001U << channel));

        /**
         * Only release semaphore if it has been properly acquired.
         */
        #ifdef USE_RTOS
        if((stat != SEM_ERR) && (stat != SEM_TIMOUT))
        {
            rt_sem_signal(&accessSem);
        }
        #else
        if(state == 0)
        {
            up(&accessSem);
        }
        #endif
    }
}

#ifdef DEBUG_RESET_CHANNEL
void debugResetChannel(unsigned int channel, unsigned int onOff)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

    static int stat = 0;

    #ifdef USE_RTOS
    stat = rt_sem_wait_timed(&accessSem, SEMAPHORE_TIMEOUT);
    if((stat == SEM_ERR) || (stat == SEM_TIMOUT))
    #else
    if((stat = down_interruptible(&accessSem)) != 0)
    #endif
    {
        RTLOG_WARNING(moduleName, "Failed to take API semaphore (stat = %d) "
            "in function resetChannel.", stat);
    }
    else
    {
        if(onOff != 0)
        {
            pio->cntl1 |= correctEndianess(0x0001U << channel);
        }
        else
        {
            pio->cntl1 &= correctEndianess(~(0x0001U << channel));
        }

        /**
         * Only release semaphore if it has been properly acquired.
         */
        #ifdef USE_RTOS
        if((stat != SEM_ERR) && (stat != SEM_TIMOUT))
        {
            rt_sem_signal(&accessSem);
        }
        #else
        if(state == 0)
        {
            up(&accessSem);
        }
        #endif
    }
}
#endif

int enableTE(void (*handler)(int))
{
    #if __GNUC__ >= 4
    void* foo = 0;
    #endif

    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

    int stat = 1;

    if(handler == 0)
    {
        RTLOG_ERROR(moduleName, "enableTE: no IRQ handler installed since the "
            "handler address is 0.");
        return stat;
    }

    #if __GNUC__ < 4
    #ifdef USE_RTOS
    stat = rt_sem_wait_timed(&readWriteLock, SEMAPHORE_TIMEOUT);
    if((stat == SEM_ERR) || (stat == SEM_TIMOUT))
    #else
    if((stat = down_interruptible(&readWriteLock)) != 0)
    #endif
    {
        RTLOG_WARNING(moduleName, "Failed to take API semaphore (stat = %d) "
            "in function enableTE. Continuing; expect instabilities!", stat);
    }
    else
    {
    #endif
        interrupt_clear();
        /**
         * Atomically exchange the handler pointer from GCC 4.1.2 on.
         */
        #if __GNUC__ >= 4
        foo = __sync_val_compare_and_swap(
            &handlerFunction, handlerFunction, handler);
        #else
        handlerFunction = handler;
        #endif
        stat = 0;

        RTLOG_INFO(moduleName, "enableTE: IRQ handler for hardware TE IRQs "
            "enabled. IRQ handler address = 0x%p.", handler);

    #if __GNUC__ < 4
        #ifdef USE_RTOS
        if((stat != SEM_ERR) && (stat != SEM_TIMOUT))
        {
            rt_sem_signal(&readWriteLock);
        }
        #else
        if(state == 0)
        {
            up(&readWriteLock);
        }
        #endif
    }
    #endif

    return stat;
}

int disableTE(void)
{
    void* foo = 0;

    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif
    int stat = 1;

    if(handlerFunction == 0)
    {
        RTLOG_INFO(moduleName, "disableTE: cannot disable hardware TE IRQ "
            "handler.  No handler had been registered before. No action "
            "taken.");
        return stat;
    }

    #if __GNUC__ < 4
    #ifdef USE_RTOS
    stat = rt_sem_wait_timed(&readWriteLock, SEMAPHORE_TIMEOUT);
    if((stat == SEM_ERR) || (stat == SEM_TIMOUT))
    #else
    if((stat = down_interruptible(&readWriteLock)) != 0)
    #endif
    {
        RTLOG_WARNING(moduleName, "Failed to take API semaphore (stat = %d) "
            "in function disableTE. Continuing; expect instabilities!", stat);
    }
    else
    {
    #endif
        /**
         * Atomically exchange the handler pointer from GCC 4.1.2 on.
         */
        #if __GNUC__ >= 4
        foo = __sync_val_compare_and_swap(
            &handlerFunction, handlerFunction, 0);
        #else
        foo = handlerFunction;
        handlerFunction = 0;
        #endif
        stat = 0;

        RTLOG_INFO(moduleName, "disableTE: hardware TE IRQ handler, "
            "address = 0x%p, disabled.", foo);

    #if __GNUC__ < 4
        #ifdef USE_RTOS
        if((stat != SEM_ERR) && (stat != SEM_TIMOUT))
        {
            rt_sem_signal(&readWriteLock);
        }
        #else
        if(state == 0)
        {
            up(&readWriteLock);
        }
        #endif
    }
    #endif

    return stat;
}

/*
 * exported API
 */
EXPORT_SYMBOL(disableTE);
EXPORT_SYMBOL(enableTE);
EXPORT_SYMBOL(resetChannel);
#ifdef DEBUG_RESET_CHANNEL
EXPORT_SYMBOL(debugResetChannel);
#endif

/**
 * Local functions.
 */
/**
 * Hardware TE IRQ handler.
 */
#ifdef USE_RTOS
static void time_event_isr_routine(long t __attribute__((unused)))
#else
static int time_event_isr_routine(void* foo __attribute__((unused)))
#endif
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

    void (*localHandler)(int) = 0;
    vme_interrupt_handle_t irq_handle = 0;
    int status = 1;
    int data = 0;

    #ifdef USE_RTOS
    while(atomic_read(&exitISRLoop) != 1)
    #else
    while((!kthread_should_stop())
    && (atomic_read(&exitISRLoop) != 1))
    #endif
    {
        /**
         * This call blocks until an VME IRQ #5 occurs.
         */
        #ifndef USE_RTOS
        set_current_state(TASK_INTERRUPTIBLE);
        #endif

        status = vme_interrupt_attach(mbus_handle,
            &irq_handle,
            VME_IRQ,
            PIO_VECTOR,
            VME_INTERRUPT_BLOCKING,
            &data);

        atomic_set(&inIrqHandler, 1);

        if(unlikely(status > 0))
        {
            RTLOG_ERROR(moduleName, "time_event_isr_routine: error attaching "
                "to the interrupt.  IRQs will NOT be reported from here on!");

            /* Sleep for 200ms. */
            #ifdef USE_RTOS
            rt_sleep(nano2count(200000000ULL));
            #else
            /**
             * TODO
             * Thomas, 2009-05-15
             * Better use the non-interruptible msleep to avaid any other
             * VME IRQs?
             */
            msleep_interruptible(200U);
            #endif

            atomic_set(&inIrqHandler, 0);
            break;
        }

        if(unlikely(checkValidIRQ() == 0))
        {
            atomic_inc(&numberOfDoubleIrqs);
            interrupt_clear();

            /**
             * It is one of those "double" IRQs on the systems which have
             * a fast enough CPU (> 1.1GHz Pentium M).  Just ignore it and
             * continue.
             * Always better to check if it is a proper IRQ anyway.
             */
            atomic_set(&inIrqHandler, 0);
            continue;
        }

        interrupt_clear();

        /**
         * Call the handler function only if it is defined.
         */
        #if __GNUC__ >= 4
        /**
         * Atomically read the handler pointer from GCC 4.1.2 on.
         */
        localHandler = __sync_fetch_and_or(&handlerFunction, 0);
        #else
        localHandler = handlerFunction;
        #endif

        if(likely(localHandler != 0))
        {
            atomic_inc(&numberOfTesWithHandler);

            atomic_set(&inRegisteredHandler, 1);
            localHandler(-1);
            atomic_set(&inRegisteredHandler, 0);
            localHandler = 0;
        }
        else
        {
            atomic_inc(&numberOfTesWithoutHandler);
        }

        atomic_set(&inIrqHandler, 0);
    }

    RTLOG_DEBUG(moduleName, "Leaving the IRQ handler.");

    /**
     * The IRQ handler task shall finish itself. We're done.
     */
    #ifdef USE_RTOS
    rt_task_delete(rt_whoami());
    RTLOG_DEBUG(moduleName, "Done with the IRQ handler.");
    #else
    return 0;
    #endif
}

/**
 * The functions of steps 1 to 5 are called from within
 * the init/clean up tasks. See above comment on the variable initLevel.
 */
static int firstInitSection(void)
{
    #ifdef USE_RTOS
    SEMAPHORE_TIMEOUT *= semaphoreTimeout;

    #if __GNUC__ < 4
    rt_sem_init(&readWriteLock, 1);
    #endif
    rt_sem_init(&accessSem, 1);
    #endif

    return RT_TOOLS_MODULE_INIT_SUCCESS;
}

static int firstCleanUpSection(void)
{
    #ifdef USE_RTOS
    rt_sem_delete(&accessSem);
    #if __GNUC__ < 4
    rt_sem_delete(&readWriteLock);
    #endif
    #endif

    return RT_TOOLS_MODULE_EXIT_SUCCESS;
}


static int secondInitSection(void)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

    int ret = RT_TOOLS_MODULE_INIT_ERROR;

    /**
     * Tell the AVME 9670 carrier board that I want to use the hardware which
     * is installed in one of its slots.
     */
    carrier = vme_resource_acquire(
        &mbus_handle,
        &mwindow_handle,
        &vme_resource_level);

    if(carrier == 0)
    {
        RTLOG_ERROR(moduleName, "Unable to acquire carrier resource.");
    }
    else
    {
        /**
         * Use the AVME 9670 driver to calculate the memory address of the
         * Parallel-IO-4 card.
         */
        pio = (PIO_Card*)(carrier_slot_IO_address('A', carrier));
        ret = RT_TOOLS_MODULE_INIT_SUCCESS;

        RTLOG_INFO(moduleName, "Acquired carrier resource.  Resource = 0x%x, "
            "pio = 0x%x.", (unsigned int)carrier, (unsigned int)pio);
    }

    return ret;
}

static int secondCleanUpSection(void)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

    if(mbus_handle != 0)
    {
        vme_resource_release(
            &mbus_handle,
            &mwindow_handle,
            &vme_resource_level);

        RTLOG_INFO(moduleName, "VME resource released.");
    }

    return RT_TOOLS_MODULE_EXIT_SUCCESS;
}


static int thirdInitSection(void)
{
    /**
     * Tell the AVME 9670 board to initialise itself.
     */
    initializeCarrier(carrier);
    return RT_TOOLS_MODULE_INIT_SUCCESS;
}

static int thirdCleanUpSection(void)
{
    if(carrier != 0)
    {
        releaseCarrier(carrier);
    }

    return RT_TOOLS_MODULE_EXIT_SUCCESS;
}


static int fourthInitSection(void)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

    int ret = RT_TOOLS_MODULE_INIT_ERROR;


    if(checkIP(carrier, ip_parallel_id, sizeof(ip_parallel_id),
        &wrongEndianess) != 0)
    {
        RTLOG_ERROR(moduleName, "Could not find the correct IP on the carrier "
            "board. Is the IP attached to slot A?");
    }
    else
    {
        ret = RT_TOOLS_MODULE_INIT_SUCCESS;
    }

    return ret;
}

static int fourthCleanUpSection(void)
{
    /**
     * Disable interrupts.
     */
    if(pio != 0)
    {
         /**
          * Make reset lines inputs to protect I/O-hardware on the board.
          */
        pio->Base_cntl &= correctEndianess((uint16_t)~AUTO_UPDATE);
        pio->cntl1 = correctEndianess(0x0000U);
        pio->cntl2 = correctEndianess(0x0000U);
        pio->Int_En1 &= correctEndianess(
            (uint16_t)~(0x0001U << TE_SOURCE_CHANNEL));
        pio->Base_cntl &= correctEndianess((uint16_t)~INTERRUPT_ENBL);
        pio->Base_cntl |= correctEndianess(AUTO_UPDATE);
    }

    carrier->controlReg &= correctEndianess(~INT_ENABLE);
    carrier->intLevel = correctEndianess(0x0000U);
    carrier->intEnable = correctEndianess(0x0000U);

    return RT_TOOLS_MODULE_EXIT_SUCCESS;
}


static int fifthInitSection(void)
{
    
    /**
     * Setup the IRQ related information.
     */
    carrier->controlReg &= correctEndianess(~INT_ENABLE);
    carrier->intEnable = correctEndianess(0x0000U);

    pio->Base_cntl &= correctEndianess((uint16_t)~INTERRUPT_ENBL);
    pio->Base_cntl &= correctEndianess((uint16_t)~AUTO_UPDATE);

    pio->vector = correctEndianess(PIO_VECTOR);
    pio->cntl2 = correctEndianess(0x003fU);
    pio->cntl1 = correctEndianess(0x003fU);
    pio->Int_EdgLvl1 = correctEndianess(0x0001U << TE_SOURCE_CHANNEL);
// Added for the 6200 board.
#if 1
    pio->Int_Pol1 = correctEndianess(0x0001U << TE_SOURCE_CHANNEL);
#else
    pio->Int_Pol1 = 0x0000U;
#endif
    pio->Int_En1 = correctEndianess(0x0001U << TE_SOURCE_CHANNEL);

    /**
     * Enable IRQs for the carrier.
     */
    carrier->intLevel = correctEndianess(0x0005U);
    carrier->intClear = correctEndianess(0xffffU);
    carrier->intEnable |= correctEndianess(IP_IRQ);
    carrier->controlReg |= correctEndianess(INT_ENABLE);

    /**
     * Set the IRQ hardware handling to automagic updates of the registers and
     * then enable IRQs on the Parallel-IO board.
     */
    pio->Base_cntl |= correctEndianess(AUTO_UPDATE);
    pio->Base_cntl |= correctEndianess(INTERRUPT_ENBL);

    return RT_TOOLS_MODULE_INIT_SUCCESS;
}

static int fifthCleanUpSection(void)
{
    disableTE();

    /**
     * Tell the IRQ handler task to exit once it has served another IRQ.
     */
    atomic_set(&exitISRLoop, 1);

    /**
     * Generate a synthetic interrupt allowing the IRQ handler task to exit
     * even if there is no TE coming in at the moment.
     */
    #ifdef USE_RTOS
    if(mbus_handle != 0)
    #else
    if((te_handler_task != ERR_PTR(-ENOMEM))
    && (mbus_handle != 0))
    #endif
    {
        vme_interrupt_generate(mbus_handle, VME_IRQ, PIO_VECTOR);
        /**
         * Sleep one and a half TE to let the VME hardware acknowledge
         * everything.
         */
        #ifdef USE_RTOS
        rt_sleep(nano2count(48000000ULL + 24000000ULL));
        #else
        msleep(48U + 24U);
        #endif
    }

    #ifndef USE_RTOS
    if((te_handler_task != ERR_PTR(-ENOMEM))
    && (te_handler_task->state == 0))
    {
        kthread_stop(te_handler_task);
    }
    #endif

    return RT_TOOLS_MODULE_EXIT_SUCCESS;
}



/**
 * Intialisation task.
 */
static void initTaskFunction(long flag)
{
    int status = 0;
    atomic_t* flag_p = (atomic_t*)flag;
    rtlogRecord_t logRecord;

    RTLOG_INFO(moduleName, "Initialisation task started.");

    if((status = firstInitSection()) != RT_TOOLS_MODULE_INIT_SUCCESS)
    {
        RTLOG_ERROR(moduleName, "firstInitSection failed!");
        goto TheEnd;
    }
    else
    {
        initLevel = 1;
    }

    if((status = secondInitSection()) != RT_TOOLS_MODULE_INIT_SUCCESS)
    {
        RTLOG_ERROR(moduleName, "secondInitSection failed!");
        goto TheEnd;
    }
    else
    {
        initLevel = 2;
    }

    if((status = thirdInitSection()) != RT_TOOLS_MODULE_INIT_SUCCESS)
    {
        RTLOG_ERROR(moduleName, "thirdInitSection failed!");
        goto TheEnd;
    }
    else
    {
        initLevel = 3;
    }

    if((status = fourthInitSection()) != RT_TOOLS_MODULE_INIT_SUCCESS)
    {
        RTLOG_ERROR(moduleName, "fourthInitSection failed!");
        goto TheEnd;
    }
    else
    {
        initLevel = 4;
    }

    if((status = fifthInitSection()) != RT_TOOLS_MODULE_INIT_SUCCESS)
    {
        RTLOG_ERROR(moduleName, "fifthInitSection failed!");
        goto TheEnd;
    }
    else
    {
        initLevel = 5;
    }

    status = 1;

TheEnd:
    RTLOG_INFO(moduleName, "Initialisation task function terminating...");

    atomic_set(flag_p, status);

    #ifdef USE_RTOS
    rt_task_delete(rt_whoami());
    #endif
}

/*
 * Clean-up task
 */
static void cleanUpTaskFunction(long flag)
{
    int status = 0;
    atomic_t* flag_p = (atomic_t*)flag;
    rtlogRecord_t logRecord;

    RTLOG_INFO(moduleName, "Clean up task started.");

    if(initLevel == 5)
    {
         --initLevel;

         if(fifthCleanUpSection() != RT_TOOLS_MODULE_EXIT_SUCCESS)
         {
             status = -1;
            RTLOG_ERROR(moduleName, "fifthCleanUpSection failed! Continuing "
                "with clean up.");
         }
    }

    if(initLevel == 4)
    {
         --initLevel;

         if(fourthCleanUpSection() != RT_TOOLS_MODULE_EXIT_SUCCESS)
         {
             status = -1;
            RTLOG_ERROR(moduleName, "fourthCleanUpSection failed! Continuing "
                "with clean up.");
         }
    }

    if(initLevel == 3)
    {
         --initLevel;

         if(thirdCleanUpSection() != RT_TOOLS_MODULE_EXIT_SUCCESS)
         {
             status = -1;
            RTLOG_ERROR(moduleName, "thirdCleanUpSection failed! Continuing "
                "with clean up.");
         }
    }

    if(initLevel == 2)
    {
         --initLevel;

         if(secondCleanUpSection() != RT_TOOLS_MODULE_EXIT_SUCCESS)
         {
             status = -1;
            RTLOG_ERROR(moduleName, "secondCleanUpSection failed! Continuing "
                "with clean up.");
         }
    }

    if(initLevel == 1)
    {
         --initLevel;

         if(firstCleanUpSection() != RT_TOOLS_MODULE_EXIT_SUCCESS)
         {
             status = -1;
            RTLOG_ERROR(moduleName, "firstCleanUpSection failed! Continuing "
                "with clean up.");
         }
    }

    if(status != -1)
    {
        status = 1;
    }
    else
    {
        status = 0;
    }

    RTLOG_INFO(moduleName, "clean-up task function terminating...");

    atomic_set(flag_p, status);

    #ifdef USE_RTOS
    rt_task_delete(rt_whoami());
    #endif
}


static int DEParallelDrv_main(int stage)
{
    int status;
    int temporaryStatus = RT_TOOLS_MODULE_INIT_EXIT_ERROR;
    #ifdef USE_RTOS
    RT_TASK initTask, cleanUpTask;
    #endif
    atomic_t initFlag = ATOMIC_INIT(0);
    atomic_t cleanUpFlag = ATOMIC_INIT(0);
    rtlogRecord_t logRecord;

    if(stage == RT_TOOLS_MODULE_STAGE_INIT)
    {
        status = RT_TOOLS_MODULE_INIT_ERROR;

        /*
         * Log the CVS version and some module parameters.
         */
        RTLOG_INFO(moduleName, "%s", softwareVersion);

        RTLOG_INFO(moduleName, "Initializing module...");

        RTLOG_INFO(moduleName, "initTimeout = %dms.", initTimeout);
        RTLOG_INFO(moduleName, "cleanUpTimeout = %dms.", cleanUpTimeout);

        goto Initialization;
    }
    else
    {
        status = RT_TOOLS_MODULE_EXIT_SUCCESS;

        RTLOG_INFO(moduleName, "Cleaning up module...");

        goto FullCleanUp;
    }

Initialization:
    #ifdef USE_RTOS
    if(rt_task_init(&initTask,
        initTaskFunction,
        (long)&initFlag,
        RT_TOOLS_INIT_TASK_STACK,
        RT_TOOLS_INIT_TASK_PRIO,
        0, 0)
    != 0)
    {
        status = RT_TOOLS_MODULE_NO_INIT_TASK_ERROR;

        RTLOG_ERROR(moduleName, "Failed to spawn initialization task!");

        goto Exit;
    }
    else
    {
        RTLOG_INFO(moduleName, "Created initalisation task, address = 0x%p.",
            &initTask);
    }

    /*
     * resume the initialization task
     */
    if(rt_task_resume(&initTask) != 0)
    {
        rt_task_delete(&initTask);
        status = RT_TOOLS_MODULE_NO_INIT_TASK_ERROR;

        RTLOG_ERROR(moduleName, "Cannot resume initalisation task!");

        goto Exit;
    }

    /*
     * wait for the initialization task to be ready
     */
    if(rtToolsWaitOnFlag(&initFlag, initTimeout) != 0)
    {
        /*
         * okay, the init task was not ready on time, now forcibly remove
         * it for the rtai's scheduler
         */
        rt_task_suspend(&initTask);
        rt_task_delete(&initTask);

        RTLOG_ERROR(moduleName, "Initialization task did not report back on "
            "time!");

        status  = RT_TOOLS_MODULE_INIT_TIMEOUT;

        goto FullCleanUp;
    }
    #else
    initTaskFunction(long)&initFlag);
    if(atomic_read(initFlag) != 1)
    {
        RTLOG_ERROR(moduleName, "Initialization failed!");

        status  = RT_TOOLS_MODULE_INIT_TIMEOUT;

        goto FullCleanUp;
    }
    #endif
    else
    {
        /*
         * Waited successfully for the init task to run through.
         * Now create the IRQ handler task.
         */
        #ifdef USE_RTOS
        if(rt_task_init_cpuid(&te_handler_task,
            time_event_isr_routine,
            0,
            TE_HANDLER_STACK_SIZE,
            TE_HANDLER_PRIORITY,
            0, 0, 0x00)
        == 0)
        {
            /**
             * Let the IRQ handler run.
             */
            if(rt_task_resume(&te_handler_task) == 0)
            {
                RTLOG_INFO(moduleName, "Started IRQ handler task, address = 0x%p.",
                    &te_handler_task);

                status = RT_TOOLS_MODULE_INIT_SUCCESS;
            }
            else
            {
                rt_task_delete(&te_handler_task);

                RTLOG_ERROR(moduleName, "Could not start IRQ handler task. NO "
                    "TEs will be visible.");

                goto FullCleanUp;
            }
        }
        #else
        te_handler_task = kthread_run(time_event_isr_routine, 0,
            "ALMA-TE-IRQ_Handler");
        if(te_handler_task != ERR_PTR(-ENOMEM))
        {
            RTLOG_INFO(moduleName, "Started IRQ handler task, address = 0x%p.",
                te_handler_task);
        }
        #endif
        else
        {
            RTLOG_ERROR(moduleName, "Could not init IRQ handler task! No TEs "
                "will be visible.");

            goto FullCleanUp;
        }
    }

    procEntry = create_proc_entry(moduleName, S_IRUGO | S_IWUSR, NULL);
    if(procEntry == 0)
    {
        RTLOG_DEBUG(moduleName, "Failed to register the proc FS entry.");
    }
    else
    {
        procEntry->read_proc = readProcFsPage;
    }

    goto Exit;


FullCleanUp:
    remove_proc_entry(moduleName, NULL);

    #ifdef USE_RTOS
    /*
     * Asynchronous clean up phase to be handled by this task.
     */
    if(rt_task_init(&cleanUpTask,
        cleanUpTaskFunction,
        (long)&cleanUpFlag,
        RT_TOOLS_CLEANUP_TASK_STACK,
        RT_TOOLS_CLEANUP_TASK_PRIO,
        0, 0)
    != 0)
    {
        RTLOG_ERROR(moduleName, "Failed to spawn clean up task!");

        temporaryStatus = RT_TOOLS_MODULE_CLEANUP_ERROR;

        goto Exit;
    }
    else
    {
        RTLOG_INFO(moduleName, "Created clean up task, address = 0x%p.",
            &cleanUpTask);
    }

    /*
     * Resume the clean up task.
     */
    if(rt_task_resume(&cleanUpTask) != 0)
    {
        rt_task_delete(&cleanUpTask);
        temporaryStatus = RT_TOOLS_MODULE_CLEANUP_ERROR;

        RTLOG_ERROR(moduleName, "Cannot resume clean up task!");

        goto Exit;
    }

     /*
      * Wait for the initialization task to become ready.
      */
    if(rtToolsWaitOnFlag(&cleanUpFlag, cleanUpTimeout) != 0)
    {
        /*
         * Okay, the clean up task was not ready on time, now forcibly remove
         * it for the rtai's scheduler.
         */
        rt_task_suspend(&cleanUpTask);
        rt_task_delete(&cleanUpTask);

        RTLOG_ERROR(moduleName, "Clean up task did not report back on time!");

        temporaryStatus = RT_TOOLS_MODULE_CLEANUP_ERROR;
     }
    #else
    cleanUpTaskFunction(long)&cleanUpFlag);
    if(atomic_read(cleanUpFlag) != 1)
    {
        RTLOG_ERROR(moduleName, "Clean up failed!");

        temporaryStatus = RT_TOOLS_MODULE_CLEANUP_ERROR;
    }
    #endif
     else
     {
         temporaryStatus = RT_TOOLS_MODULE_EXIT_SUCCESS;
     }

Exit:
    if(stage != RT_TOOLS_MODULE_STAGE_INIT)
    {
        status = temporaryStatus;
    }

    return status;
}


static int __init DEParallelDrv_init(void)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

    int status = RT_TOOLS_MODULE_INIT_ERROR;

    if((status = DEParallelDrv_main(RT_TOOLS_MODULE_STAGE_INIT))
    == RT_TOOLS_MODULE_INIT_SUCCESS)
    {
        RTLOG_INFO(moduleName, "Module initialized successfully.");
    }
    else
    {
        RTLOG_ERROR(moduleName, "Failed to initialize module!");
    }

    return status;
}

static void __exit DEParallelDrv_exit(void)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

    if(DEParallelDrv_main(RT_TOOLS_MODULE_STAGE_EXIT)
    == RT_TOOLS_MODULE_EXIT_SUCCESS)
    {
        RTLOG_INFO(moduleName, "Module cleaned up successfully.");
    }
    else
    {
        RTLOG_ERROR(moduleName, "Failed to clean up module!");
    }
}

module_init(DEParallelDrv_init);
module_exit(DEParallelDrv_exit);
