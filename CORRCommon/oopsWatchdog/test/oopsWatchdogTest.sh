#! /bin/bash
# @(#) $Id$

max_iter=5 # maximal number of iterations (each 1 second) == sleep 15

result=1
i=0

while [ $result -ne 0 -a $i -lt $max_iter ]; do
	sleep 1
	let i++
	/bin/dmesg | /usr/bin/tail -n 10 | /bin/egrep -c "This machine \(.*\)just oopsed. It is necessary to reboot it!" > /dev/null
	result=$?
done

if [ $result -eq 1 ]; then
	echo "`basename $0`: test passed."
else
	echo "`basename $0`: test failed. No result after $max_iter sec."
fi
