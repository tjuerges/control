$Id$

DESCRIPTION TEST 1
The test uses the kernel function kallsyms_lookup_name to look the
function with the name "foo"up. Before the test module is loaded, the
oopsWatchdog kernel module is loaded with a parameter that specifies
the kernel function which shall be watched. Since the watchdog is not really
a true watchdog but more a breakpoint, every address in the kernel can be
used to intercept. That is why the test uses kallsyms_lookup_name.
OUTCOME: Test report in /var/log/messages.

DESCRIPTION TEST 2
Standard endurance test. It loads the base set of modules and then it
loads and unloads the oopsWatchdog module for 100 times.
OUTCOME: Test report in /var/log/messages.
