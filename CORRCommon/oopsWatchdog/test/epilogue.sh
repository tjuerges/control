#! /bin/sh
# $Id$

# Undo the permission change done in the prologue
chmod o-r ../config/test.lkm
chmod o-r ../config/testLoadUnload-preload.lkm
chmod o-r ../config/testLoadUnload.lkm

exit 0
