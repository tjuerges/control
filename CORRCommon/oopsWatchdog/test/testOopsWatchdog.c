/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2007 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 *
 * who       when      what
 * --------  --------  ----------------------------------------------
 * tjuerges  Sep 10, 2007  created 
 */


#include <linux/module.h>
#include <linux/sched.h>


#ifdef USE_RTOS
#include <rtLog.h>
#endif


#define moduleName "testOopsWatchdog"


MODULE_AUTHOR ("Thomas Juerges, <tjuerges@nrao.edu>");
MODULE_DESCRIPTION (moduleName ": Test module for the Kernel Oops watchdog");
MODULE_LICENSE("GPL");


static void __exit exit_to_debug(void)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

    RTLOG_INFO(moduleName, "Module exiting.");
}

static int __init init_to_debug(void)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

    RTLOG_INFO(moduleName, "Executing.");
    yield();

    return 0;
}

module_init(init_to_debug);
module_exit(exit_to_debug);
