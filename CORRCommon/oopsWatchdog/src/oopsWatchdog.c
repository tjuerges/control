/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 *
 * who       when      what
 * --------  --------  ----------------------------------------------
 * tjuerges  Sep 10, 2007  created
 */


/*
 * System stuff
 */
#include <linux/module.h>
#include <linux/version.h>
#include <linux/init.h>
#include <linux/moduleparam.h>
#include <linux/kprobes.h>
#include <linux/kallsyms.h>
#include <linux/utsname.h>
#include <linux/errno.h>
#include <linux/version.h>

#ifdef USE_RTOS
#include <rtLog.h>
#include <rtAlarm.h>
#else
#define RTLOG_ERROR(moduleName, format, args...)\
    {\
        printk(KERN_ERR "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }
#define RTLOG_WARNING(moduleName, format, args...)\
    {\
        printk(KERN_WARNING "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }
#define RTLOG_INFO(moduleName, format, args...)\
    {\
        printk(KERN_INFO "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }
#define RTLOG_DEBUG(moduleName, format, args...)\
    {\
        printk(KERN_DEBUG "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }
#endif


#define moduleName "oopsWatchdog"


MODULE_AUTHOR("Thomas Juerges, <tjuerges@nrao.edu>");
MODULE_DESCRIPTION(moduleName ": Kernel Oops watchdog for ALMA.");
MODULE_LICENSE("GPL");


static char softwareVersion[] =
    "$Id$";


/*
 * The name of the kernel function to be intercepted.
 */
#define DEBUGTHIS "do_invalid_op"


/*
 * Make the method name available as parameter.
 */
static char* debugThisName = DEBUGTHIS;
module_param(debugThisName, charp, S_IRUGO);
MODULE_PARM_DESC(debugThisName, "Provide the name of a kernel "
    "function which shall be intercepted before executed. The default "
    "value is " DEBUGTHIS " in order to report kernel Oopses.");


/*
 * Structure which contains the necessary probes, i.e. handler addresses. I use
 * only the pre-execution probe.
 */
static struct kprobe kernelProbe;

/*
 * Struct containing the hostname which is reported in case of an Oops.
 */
static struct new_utsname hostInfo;


#ifdef USE_RTOS
/*
 * RT-alarm source.
 */
static rtAlarmSource_t* alarmSource = 0;
static char* alarmSourceName = moduleName;
#endif


/*
 * This function will be called whenever the kernel calls
 * the function DEBUGTHIS.
 */
static int beforeExecution(struct kprobe* kpr, struct pt_regs* p)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;

    rtAlarmPush(alarmSource, 1, rtAlarm_ACTIVE);
    #endif

    RTLOG_ERROR(moduleName, "This machine (%s) just oopsed. It is necessary to "
        "reboot it!", hostInfo.nodename);
    return 0;
}

/*
 * If ever necessary. activate this probe, too. Don't forget to activate it in
 * kernelProbe, too!
 static int afterExecution(struct kprobe* kpr, struct pt_regs* p, unsigned long flags)
{
    RTLOG_ERROR(
        moduleName, "This machine (%s) just oopsed. It is necessary to reboot it!",
        hostInfo.nodename);
    return 0;
}
 */

static void __exit exit_kprobe(void)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;

    rtAlarmSourceDelete(alarmSource, 0);
    #endif

    RTLOG_DEBUG(moduleName, "Unregister watchdog for the kernel function "
        "\"%s\"...", debugThisName);
    unregister_kprobe(&kernelProbe);

    RTLOG_INFO(moduleName, "Exiting on host %s.", hostInfo.nodename);
}

static int __init init_kprobe(void)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

    int ret = -EINVAL;

    RTLOG_INFO(moduleName, "%s", softwareVersion);

    #ifdef USE_RTOS
    alarmSource = rtAlarmSourceInit(alarmSourceName, 1);
    #endif

    memset(&hostInfo, 0, sizeof(struct new_utsname));
    /*
     * Access the hostname directly. - I know, it is ugly. At least I make it an
     * atomic access.
     */
    #if LINUX_VERSION_CODE >= KERNEL_VERSION(2, 6, 19)
        #if LINUX_VERSION_CODE >= KERNEL_VERSION(2, 6, 29)
    get_uts_ns(&init_uts_ns);
        #else
    down_read(&uts_sem);
        #endif
    strcpy(hostInfo.nodename, init_uts_ns.name.nodename);
        #if LINUX_VERSION_CODE >= KERNEL_VERSION(2, 6, 29)
    put_uts_ns(&init_uts_ns);
        #else
    up_read(&uts_sem);
        #endif
    #else
    strcpy(hostInfo.nodename, system_utsname.nodename);
    #endif

    /* Registering a kprobe */
    RTLOG_DEBUG(moduleName, "Inserting the kprobe, a.k.a. the watchdog, for "
        "symbol %s...", debugThisName);

    /*
     * Searching the address of the function to be probed is not necessary
     * anymore from kernel 2.6.19 on, But: either set the name OR the address!
     * Thus the structere has to be cleared first.
     */
    memset(&kernelProbe, 0, sizeof(struct kprobe));

    #if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,19)
    kernelProbe.symbol_name = debugThisName;
    #else
    kernelProbe.addr =
        (kprobe_opcode_t*) kallsyms_lookup_name(debugThisName);
    #endif

    kernelProbe.pre_handler = beforeExecution;
    /*
     * Do not register a post-probe. In ALMA we do not need it.
     kernelProbe.post_handler = (kprobe_post_handler_t)(afterExecution);
     */

    ret = register_kprobe(&kernelProbe);
    if(ret < 0)
    {
        RTLOG_ERROR(
            moduleName, "Could not register a kprobe for the symbol \"%s\".",
            debugThisName);
    }
    else
    {
        ret = 0;

        RTLOG_INFO(
            moduleName, "Watchdog is installed on host %s.", hostInfo.nodename);
    }

    return ret;
}

module_init(init_kprobe);
module_exit(exit_kprobe);
