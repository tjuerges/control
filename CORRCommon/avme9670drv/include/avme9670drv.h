#ifndef IPCARRIERTOOLS_H
#define IPCARRIERTOOLS_H
/*******************************************************************************
* ALMA - Atacama Large Millimeter Array
* (c) Associated Universities Inc., 2004
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
*/


#include <ip_carrier.h>

/**
 * The following piece of code alternates the linkage type to C for all
 * functions declared within the braces, which is necessary to use the
 * functions in C++-code.
 */

#ifdef __cplusplus
extern "C"
{
#endif
/*
 * These functions are used to interact with the IP carrier board
 */
#include <vme/vme.h>
#include <vme/vme_api.h>


typedef enum
{
    vme_res_none,
    vme_res_init,
    vme_res_win_master_create,
    vme_res_win_master_map
} vme_resource_level_t;

unsigned int checkIP(volatile BOARD_MEMORY_MAP* carrier, unsigned char ipId[],
    size_t ipIdSize, unsigned int* _wrongEndianess);

void* carrier_slot_IO_address(unsigned char slot,
    volatile BOARD_MEMORY_MAP* carrier);

void vme_resource_release(vme_bus_handle_t* bus_handle,
    vme_master_handle_t* window_handle,
    vme_resource_level_t* step);


void* vme_resource_acquire(vme_bus_handle_t* bus_h,
    vme_master_handle_t* window_h,
    vme_resource_level_t* step);

void initializeCarrier(volatile BOARD_MEMORY_MAP* carrier);
void releaseCarrier(volatile BOARD_MEMORY_MAP* carrier);
#ifdef __cplusplus
}
#endif
#endif
