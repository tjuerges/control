$Id$

DESCRIPTION TEST 1
This test tries to acquire the VME resources for the AVME-9670 hardware. After
successful acquisition, it waits one seconds and frees the resources again. If
everything went fine, the test is successful.
One word about the 'simplicity' of the test: since the AVME-9670 IP carrier is
passive, the test can only register the IP carrier and release it. Nothing else
is possible without additional hardware.
OUTCOME: Test report in /var/log/messages.

DESCRIPTION TEST 2
Standard endurance test. It loads the base set of modules and then it
loads and unloads the avme9670drv module for 100 times.
OUTCOME: Test report in /var/log/messages.
