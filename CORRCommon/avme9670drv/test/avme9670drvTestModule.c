/* @(#) $Id$
*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* avme9670drvTestModule
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  2006-02-22  created
*/

/************************************************************************
*   NAME
* avme9670drvTestModule
*   SYNOPSIS
* Kenel module to test the Acromag VME 9670 driver.
*   DESCRIPTION
*
*   FILES
* avme9670drvTestModule.c
*   ENVIRONMENT
* TAT
*   COMMANDS
*
*   RETURN VALUES
* Returns RT_TOOLS_MODULE_INIT_SUCCESS defined in
* CONTROL/CORRCommon/rtTools/include/rtTools.h if the test is successful.
*   CAUTIONS
*
*   EXAMPLES
*
*   SEE ALSO
* CONTROL/CORRCommon/avme9670drv/[src/avme9670drv.c | include/avme9670drv.h]
*   BUGS
*
*------------------------------------------------------------------------
*/


#include <linux/module.h>
#include <linux/moduleparam.h>
#ifdef USE_RTOS
#include <rtai_sched.h>
#include <rtTools.h>
#include <rtLog.h>
#else
#include <linux/delay.h>
#endif


#include <avme9670drv.h>


#define moduleName "avme9670drvTestModule"


MODULE_AUTHOR("Thomas Juerges <tjuerges@nrao.edu>");
MODULE_DESCRIPTION(moduleName ": Test kernel module for the avme9670drv kernel "
    "module.");
MODULE_LICENSE("GPL");


static vme_bus_handle_t mbus_handle = 0;
static vme_master_handle_t mwindow_handle = 0;
static vme_resource_level_t vme_resource_level = vme_res_none;
static volatile BOARD_MEMORY_MAP* carrier = 0;
static atomic_t finished = ATOMIC_INIT(1);
#ifdef USE_RTOS
static RT_TASK taskID;


static void test(long junk __attribute__((unused)))
#else
static int test(void)
#endif
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

	int status = RT_TOOLS_MODULE_INIT_ERROR;

	RTLOG_INFO(moduleName, "Test task running...");

	RTLOG_INFO(moduleName, "Allocating VME carrier resources...");
	carrier = vme_resource_acquire(&mbus_handle,
		&mwindow_handle,
		&vme_resource_level);

    RTLOG_INFO(moduleName, "carrier = 0x%p", carrier);
	if((!carrier) || (mbus_handle == 0) || (mwindow_handle == 0))
	{
		RTLOG_ERROR(moduleName, "Unable to acquire VME carrier resources!");
        goto LeaveTest;
	}
	else
	{
		/**
		 * Give VME system some time to settle down.
		 * Sleep one second.
		 */
         RTLOG_INFO(moduleName, "Acquired VME carrier resiources.");
         //#ifdef USE_RTOS
		 //rt_sleep(nano2count((RTIME)1000000));
         //#else
         //msleep_interruptible(1000U);
         //#endif
	}

	RTLOG_INFO(moduleName, "Releasing VME carrier resources...");
	vme_resource_release(&mbus_handle, &mwindow_handle, &vme_resource_level);
    status = RT_TOOLS_MODULE_INIT_SUCCESS;

LeaveTest:
	RTLOG_INFO(moduleName, "avme9670drv test %s.", (status == RT_TOOLS_MODULE_INIT_SUCCESS ? "SUCCESSFUL" : "FAILED"));

	atomic_set(&finished, 0);
    #ifdef USE_RTOS
	rt_task_delete(rt_whoami());
    #else
    return status;
    #endif
}


static int __init avme9670drvTestModule_init(void)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

	int stat = RT_TOOLS_MODULE_INIT_ERROR;

	RTLOG_INFO(moduleName, "Running avme9670drv test...");

    #ifdef USE_RTOS
	/**
	 * Initialise the test task.
	 */
	if(rt_task_init(&taskID, test, 0, 2000, 3, 0, 0) != 0)
	{
		RTLOG_ERROR(moduleName, "Could not start the test task!");
	}
	else
	{
        RTLOG_INFO(moduleName, "Test task = 0x%p.", &taskID);
		rt_task_resume(&taskID);

		stat = RT_TOOLS_MODULE_INIT_SUCCESS;

		RTLOG_INFO(moduleName, "Module initialized successfully.");
	}
    #else
    if(test() == RT_TOOLS_MODULE_INIT_SUCCESS)
    {
        RTLOG_INFO(moduleName, "Non-RTOS sucessful.");
    }
    else
    {
        RTLOG_INFO(moduleName, "Non-RTOS failed.");
    }
    stat = RT_TOOLS_MODULE_INIT_SUCCESS;
    #endif


	return stat;
};

static void __exit avme9670drvTestModule_exit(void)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

	if(atomic_read(&finished) != 0)
	{
        #ifdef USE_RTOS
		rt_task_suspend(&taskID);
		rt_task_delete(&taskID);
        #endif
    }

	RTLOG_INFO(moduleName, "Unloaded.");
};

module_init(avme9670drvTestModule_init);
module_exit(avme9670drvTestModule_exit);
