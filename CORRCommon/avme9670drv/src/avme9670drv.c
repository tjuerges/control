/*******************************************************************************
*
* "@(#) $Id$"
*
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2008
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* who       when        what
* --------  ----------  ----------------------------------------------
* $Author$  $Date$      Modified    
*/

/**
 * System stuff
 */
#include <linux/moduleparam.h>
#include <linux/delay.h>
#include <asm/atomic.h>
#include <linux/proc_fs.h>
#ifndef USE_RTOS
#include <asm/semaphore.h>
#else
/**
 * RTAI stuff
 */
#include <rtai_sem.h>

/**
 * ACS stuff
 */
#include <rtTools.h>
#include <rtLog.h>
#endif

/**
 * Private includes
 */
#include <avme9670drv.h>
#include <ip_carrier.h>
#include <avme9670drvPrivate.h>


/**
 * The VME IRQ used for the Parallel-IO IP board.
 */
#define VME_IRQ VME_INTERRUPT_VIRQ5

/**
 * Used for logging
 */
#define moduleName "avme9760drv"


#ifdef USE_RTOS
#define DEBUG_VALUE(a) { rtlogRecord_t logRecord; RTLOG_INFO(moduleName, "value of %s = %xu ", #a , a); }
#else
#define DEBUG_VALUE(a) printk("%s: value of %s = %xu ", moduleName, #a , a);
#endif


MODULE_AUTHOR("Thomas Juerges <tjuerges@nrao.edu>");
MODULE_DESCRIPTION(moduleName ": Acromag VME9670 IP Carrier IRQ handler.");
MODULE_LICENSE("GPL");


/**
 * Module parameters
 */
static unsigned int semaphoreTimeout = 100;    /* usec */
module_param(semaphoreTimeout, uint, S_IRUGO);

/**
 * Local data
 */

static char softwareVersion[] =
    "$Id$";

/**
 * Data structure for the /proc page.
 */
static struct proc_dir_entry* procEntry = 0;


/**
 * If the single board computer does not do the endian conversion between
 * x86 and VME, I have to do it myself.
 */
static unsigned int wrongEndianess = 0U;

/**
 * Reference list of carriers which have been initialized here.
 */
static volatile BOARD_MEMORY_MAP* carrierList[CARRIER_LIST_SIZE];
/**
 * Reference counters for the IP carrier memory addresses.
 */
static atomic_t ipCarrierRefCount[CARRIER_LIST_SIZE];

/**
 * Timeout for the API semaphore.
 */
#ifdef USE_RTOS
static RTIME SEMAPHORE_TIMEOUT = 0;

/**
 * API must be reentrant, protect with this semaphore
 */
static SEM apiSemaphore;
#else
static DECLARE_MUTEX(apiSemaphore);
#endif


/**
 * Internal use variables for the VME bus resource acquisition.
 */
static void* client_ptr_base = 0;
static vme_bus_handle_t mbus_handle = 0;
static vme_master_handle_t mwindow_handle = 0;
static vme_resource_level_t vme_resource_level = vme_res_none;


/**
 * Local helper functions.
 */
static int readProcFsPage(char* buffer, char** start, off_t offset, int length,
    int* eof, void* data)
{
    int size = 0;
    int listIndex = 0;
    volatile BOARD_MEMORY_MAP* carrierClient = 0;

    size += sprintf(buffer + size, "%s kernel module information page\n\n"
        "Software version = %s\n\n"
        "VME bus resources base address = 0x%p\n"
        "VME Master bus handle = 0x%x\n"
        "VME Master window handle = 0x%x\n"
        "Endianess conversion is done in %sware.\n"
        "Currently registered carrier board clients: ",
        moduleName, softwareVersion, client_ptr_base,
        (unsigned int)mbus_handle,
        (unsigned int)mwindow_handle,
        ((wrongEndianess == 1) ? ("soft") : ("hard")));

    for(; listIndex < CARRIER_LIST_SIZE; ++listIndex)
    {
        #if __GNUC__ >= 4
        /**
        * Atomically read the handler pointer from GCC 4.1.2 on.
        */
        carrierClient = __sync_fetch_and_or(&(carrierList[listIndex]), 0);
        #else
        carrierClient = carrierList[listIndex];
        #endif

        if(carrierClient != 0)
        {
            size += sprintf(buffer + size, "0x%p ", carrierClient);
        }
    }

    size +=  sprintf(buffer + size, "\n");

    return size;
}


//#define correctEndianess(a) (((a & 0xff00U) >> 8U) | ((a & 0x00ffU) << 8U))
inline static uint16_t correctEndianess(uint16_t value)
{
    uint16_t ret = value;

    if(wrongEndianess == 1U)
    {
        ret = (((value & 0xff00U) >> 8U) | ((value & 0x00ffU) << 8U));
    }

    return ret;
}


static void addCarrierToClientList(volatile BOARD_MEMORY_MAP* carrier)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif
    int stat = 0;
    int listIndex = 0;
   
    #ifdef USE_RTOS
    stat = rt_sem_wait_timed(&apiSemaphore, SEMAPHORE_TIMEOUT);

    if((stat == SEM_ERR) || (stat == SEM_TIMOUT))
    #else
    if((stat = down_interruptible(&apiSemaphore)) != 0)
    #endif
    {
        RTLOG_ERROR(moduleName, "Failed to take API semaphore (stat = %d) in "
            "function addCarrierToClientList!", stat);
    }
    else
    {
        listIndex = findCarrierInClientList(carrier);
        if(listIndex == CARRIER_LIST_SIZE)
        {
            for(listIndex = 0; listIndex < CARRIER_LIST_SIZE; ++listIndex)
            {
                if(carrierList[listIndex] == 0)
                {
                    break;
                }
            }

            if(listIndex == CARRIER_LIST_SIZE)
            {
                RTLOG_ERROR(moduleName, "Could not append the carrier to the "
                    "list of clients! The list is full. Max. %d clients.",
                    CARRIER_LIST_SIZE);
                /**
                 * There's nothing which can be done now. Break up, sorry.
                 */
            }
            else
            {
                carrierList[listIndex] = carrier;
                RTLOG_INFO(moduleName, "Appended the carrier, address 0x%p, to "
                    "the list of clients at position %d.", carrier, listIndex);
            }
        }

        /**
         * Increase the reference counter and read its value.
         * If the value == 1, the it is the first time, that
         * a client wants to initialize the carrier.
         */
        if(atomic_inc_return(&(ipCarrierRefCount[listIndex])) == 1)
        {
            RTLOG_INFO(moduleName, "IP Carrier, address 0x%p, reset.",
                carrier);
            /*
             * reset the carrier board
             */
            carrier->controlReg = correctEndianess(SOFTWARE_RESET);
            /*
             * Give the carrier some time to do the reset.
             */
            #ifdef USE_RTOS
            rt_sleep(nano2count(10000ULL));
            #else
            msleep_interruptible(1U);
            #endif
            /*
             * Set the inteerupt to VME_IRQ.
             */
            carrier->intLevel = correctEndianess(VME_IRQ);
            /*
             * Enable the IRQ
             */
            carrier->controlReg = correctEndianess(
                (AUTO_CLEAR_INT | INT_ENABLE));
        }

        #ifdef USE_RTOS
        rt_sem_signal(&apiSemaphore);
        #else
        up(&apiSemaphore);
        #endif
    }
}

static void removeCarrierFromClientList(volatile BOARD_MEMORY_MAP* carrier)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif
    int stat = 0;
    int listIndex = 0;

    /**
     * The carrier is 0? Then do not release it.
     */
    if(carrier == 0)
    {
        return;
    }

    #ifdef USE_RTOS
    stat = rt_sem_wait_timed(&apiSemaphore, SEMAPHORE_TIMEOUT);

    if((stat == SEM_ERR) || (stat == SEM_TIMOUT))
    #else
    if((stat = down_interruptible(&apiSemaphore)) != 0)
    #endif
    {
        RTLOG_ERROR(moduleName, "Failed to take API semaphore (stat = %d) in "
            "function removeCarrierFromClientList!", stat);
    }
    else
    {
        if((listIndex = findCarrierInClientList(carrier)) == CARRIER_LIST_SIZE)
        {
            RTLOG_ERROR(moduleName, "IP Carrier, address 0x%p, could not be "
                "released. Not in client list.", carrier);
        }
        else
        {
            /**
             * Decrement the reference counter ipCarrierRefCount and test if
             * it is then < 1. This means, the carrier is not referenced by any
             * client.
             */
            if(atomic_dec_return(&(ipCarrierRefCount[listIndex])) < 1)
            {
                /**
                 * Disable IRQs.
                 */
                carrier->intEnable = correctEndianess(0x0000U);

                carrier->controlReg = correctEndianess(AUTO_CLEAR_INT);

                carrierList[listIndex] = 0;

                RTLOG_INFO(moduleName, "IP Carrier, address 0x%p, released and "
                    "removed from position %d in client list.", carrier, listIndex);
            }
            else
            {
                RTLOG_INFO(moduleName, "IP Carrier, address 0x%p, at position %d "
                    "in client list could not be released because it is still in "
                    "use by another driver.", carrier, listIndex);
            }
        }

        #ifdef USE_RTOS
        rt_sem_signal(&apiSemaphore);
        #else
        up(&apiSemaphore);
        #endif
    }
}

static int findCarrierInClientList(volatile BOARD_MEMORY_MAP* carrier)
{
    int listIndex = 0;

    for(; listIndex < CARRIER_LIST_SIZE; ++listIndex)
    {
        if(carrierList[listIndex] == carrier)
        {
            break;
        }
    }

    return listIndex;
}

/**
 * Exported API of avme9670
 */
EXPORT_SYMBOL(checkIP);
EXPORT_SYMBOL(carrier_slot_IO_address);
EXPORT_SYMBOL(initializeCarrier);
EXPORT_SYMBOL(releaseCarrier);
EXPORT_SYMBOL(vme_resource_release);
EXPORT_SYMBOL(vme_resource_acquire);

/**
 * API
 */
/**
 * Check that the right IP card is inserted into the slot A.
 * Return 0 if the right card is found.
 */
unsigned int checkIP(volatile BOARD_MEMORY_MAP* carrier, unsigned char ipId[],
    size_t ipIdSize, unsigned int* _wrongEndianess)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

    size_t i = 0U;
    unsigned char failure = 0U;

    wrongEndianess = 0;
    *_wrongEndianess = 0;

    for (; i < ipIdSize; ++i)
    {
        uint16_t c1 = carrier->IPAid[i];
        uint16_t c1_swapped = ((c1 & 0xff00U) >> 8U) | ((c1 & 0x00ffU) << 8U);
        uint16_t c2 = ipId[i];

        c1 &= 0x00ffU;
        c1_swapped &= 0x00ffU;
        c2 &= 0x00ffU;
        
        if(c1 != c2)
        {
            // Check if bytes are not swapped.
            if(c1_swapped != c2)
            {
                ++failure;
                RTLOG_ERROR(moduleName, "checkIP: Bad IDPROM value at "
                    "position %u (counting from 0). Value = %u, expected "
                    "= %u.",
                    i, c1, c2);
            }
            else
            {
                wrongEndianess = 1;
                *_wrongEndianess = 1;
                RTLOG_WARNING(moduleName, "checkIP: Bad IDPROM at position %u "
                    "(counting from 0). Only the swapped value = %u "
                    "matches the expected value (%u).",
                    i, c1_swapped, c2);
            }
        }
    }

    if(failure > 0)
    {
        RTLOG_ERROR(moduleName, "checkIP: Out of %d bytes %u were wrong.",
            i, failure);
        return 1;

    }
    else
    {
        RTLOG_INFO(moduleName, "checkIP: IDPROM verified.  Manual byte "
            "swapping is %sabled.", ((wrongEndianess == 1U) ? "en" : "dis"));
    }

    return 0;
}


void* carrier_slot_IO_address(unsigned char slot,
    volatile BOARD_MEMORY_MAP* carrier)
{
    switch(slot)
    {
        case 'A':
        {
            return (void*)(carrier->IPAio);
        }
        break;

        case 'B':
        {
            return (void*)(carrier->IPBio);
        }
        break;

        case 'C':
        {
            return (void*)(carrier->IPCio);
        }
        break;

        case 'D':
        {
            return (void*)(carrier->IPDio);
        }
        break;

        default:
        {
            return NULL;
        }
    };

    return NULL;
}

void initializeCarrier(volatile BOARD_MEMORY_MAP* carrier)
{
    addCarrierToClientList(carrier);
}

void releaseCarrier(volatile BOARD_MEMORY_MAP* carrier)
{
    removeCarrierFromClientList(carrier);
}


void* vme_resource_acquire(vme_bus_handle_t* bus_h,
    vme_master_handle_t* window_h,
    vme_resource_level_t* level)
{
    *bus_h = mbus_handle;
    *window_h = mwindow_handle;
    *level = vme_resource_level;

    return client_ptr_base;
}

/**
 * Initialize the VME accesses.
 * Return a pointer to the base address of the window map.
 */
void* __vme_resource_acquire(vme_bus_handle_t* bus_h,
    vme_master_handle_t* window_h,
    vme_resource_level_t* level)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif
    void* ptr_base = 0;
    #ifdef USE_RTOS
    int stat = rt_sem_wait_timed(&apiSemaphore, SEMAPHORE_TIMEOUT);

    if((stat == SEM_ERR) || (stat == SEM_TIMOUT))
    #else
    int stat = 0;

    if((stat = down_interruptible(&apiSemaphore)) != 0)
    #endif
    {
        RTLOG_ERROR(moduleName, "Failed to take API semaphore (stat = %d) in "
            "function __vme_resource_acquire!", stat);

        return 0;
    }

    if(0 > vme_init(bus_h))
    {
        RTLOG_ERROR(moduleName, "Error initialising the VMEbus.");


        #ifdef USE_RTOS
        rt_sem_signal(&apiSemaphore);
        #else
        up(&apiSemaphore);
        #endif

        return 0;
    }
    else
    {
        RTLOG_INFO(moduleName, "VMEbus is initialised.");
    }

    *level = vme_res_init;

    if(0 > vme_master_window_create(*bus_h, window_h, IP_CARRIER_VME_BASE_ADDR,
        VME_A16U, IP_CARRIER_WINMAP_LENGHT,VME_CTL_PWEN, NULL))
    {
        RTLOG_ERROR(moduleName, "Error creating the window");

        vme_resource_release(bus_h, window_h, level);

        #ifdef USE_RTOS
        rt_sem_signal(&apiSemaphore);
        #else
        up(&apiSemaphore);
        #endif

        return 0;
    }
    else
    {
        RTLOG_INFO(moduleName, "VME Master Window created.");
    }

    *level = vme_res_win_master_create;

    if(NULL == (ptr_base = vme_master_window_map(*bus_h, *window_h, 0)))
    {
        RTLOG_ERROR(moduleName, "Error mapping the window");

        vme_resource_release(bus_h, window_h, level);

        #ifdef USE_RTOS
        rt_sem_signal(&apiSemaphore);
        #else
        up(&apiSemaphore);
        #endif

        return 0;
    }
    else
    {
        RTLOG_INFO(moduleName, "VME Master Window mapped.");
    }

    *level = vme_res_win_master_map;

    #ifdef USE_RTOS
    rt_sem_signal(&apiSemaphore);
    #else
    up(&apiSemaphore);
    #endif

    return ptr_base;
}

/**
 * Release VME resources.
 */
void vme_resource_release(vme_bus_handle_t* bus_handle,
    vme_master_handle_t* window_handle,
    vme_resource_level_t* step)
{
}

void __vme_resource_release(vme_bus_handle_t* bus_handle,
    vme_master_handle_t* window_handle,
    vme_resource_level_t* step)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

    #ifdef USE_RTOS
    int stat = rt_sem_wait_timed(&apiSemaphore, SEMAPHORE_TIMEOUT);

    if((stat == SEM_ERR) || (stat == SEM_TIMOUT))
    #else
    int stat = 0;

    if((stat = down_interruptible(&apiSemaphore)) != 0)
    #endif
    {
        RTLOG_ERROR(moduleName, "Failed to take API semaphore (stat = %d) in "
            "function __vme_resource_release!", stat);

        return;
    }

    switch(*step)
    {
        case vme_res_win_master_map:
        {
            if(0 > vme_master_window_unmap(*bus_handle, *window_handle))
            {
                RTLOG_ERROR(moduleName, "Error unmapping the master window.");
            }
        }
        /*
         * fall-through
         */

        case vme_res_win_master_create:
        {
            if(0 > vme_master_window_release(*bus_handle, *window_handle))
            {
                RTLOG_ERROR(moduleName, "Error releasing the master window.");
            }
        }

        /**
         * Fall-through
         */

        case vme_res_init:
        {
            if(0 > vme_term(*bus_handle))
            {
                RTLOG_ERROR(moduleName, "Error terminating VME access.");
            }
        }

        default:
            break;
    }

    *step = vme_res_none;

    #ifdef USE_RTOS
    rt_sem_signal(&apiSemaphore);
    #else
    up(&apiSemaphore);
    #endif

}

static int firstInitSection(void)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif
    int listIndex = 0;

    #ifdef USE_RTOS
    SEMAPHORE_TIMEOUT = nano2count(semaphoreTimeout * 1000);

    rt_sem_init(&apiSemaphore, 1);
    #endif

    for(; listIndex < CARRIER_LIST_SIZE; ++listIndex)
    {
        carrierList[listIndex] = 0;
        atomic_set(&(ipCarrierRefCount[listIndex]), 0);
    }

    RTLOG_INFO(moduleName, "Acquiring VME bus resources...");
    client_ptr_base = __vme_resource_acquire(
        &mbus_handle,
        &mwindow_handle,
        &vme_resource_level);
    if(client_ptr_base != 0)
    {
        RTLOG_INFO(moduleName, "Successfully acuired VME bus resources, base "
            "pointer = 0x%p.", client_ptr_base);
    }
    else
    {
        RTLOG_ERROR(moduleName, "Acquisition of VME bus resources failed!");

        return RT_TOOLS_MODULE_INIT_ERROR;
    }

    return RT_TOOLS_MODULE_INIT_SUCCESS;
}

/**
 * Synchronous cleanup function.
 */
static int firstCleanupSection(void)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif
    volatile BOARD_MEMORY_MAP* carrier = 0;
    int listIndex = 0;
    int status = RT_TOOLS_MODULE_CLEANUP_ERROR;

    RTLOG_INFO(moduleName, "Releasing VME bus resources...");
    __vme_resource_release(&mbus_handle, &mwindow_handle, &vme_resource_level);
    RTLOG_INFO(moduleName, "VME bus resources released.");

    for(; listIndex < CARRIER_LIST_SIZE; ++listIndex)
    {
        carrier = carrierList[listIndex];
        removeCarrierFromClientList(carrier);
    }

    status = RT_TOOLS_MODULE_EXIT_SUCCESS;

    #ifdef USE_RTOS
    rt_sem_delete(&apiSemaphore);
    #endif

    return status;
}

/**
 * Init/Exit function called from init_ or exit_ entry points.
 *
 * Here it is only called from the exit function.
 */
static int avme9670drv_main(const int stage)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif
    int status = RT_TOOLS_MODULE_INIT_ERROR;
    int temporaryStatus = RT_TOOLS_MODULE_CLEANUP_ERROR;

    if(stage == RT_TOOLS_MODULE_STAGE_INIT)
    {
        /*
         * Log CVS version and other configuration stuff.
         */
        RTLOG_INFO(moduleName, "%s", softwareVersion);

        RTLOG_INFO(moduleName, "Initializing module...");

        goto Initialization;
    }
    else
    {
        RTLOG_INFO(moduleName, "Cleaning up module...");

        goto FullCleanUp;
    }


Initialization:

    if((status = firstInitSection()) != RT_TOOLS_MODULE_INIT_SUCCESS)
    {

        goto Level1CleanUp;
    }

    procEntry = create_proc_entry(moduleName, S_IRUGO | S_IWUSR, NULL);
    if(procEntry == 0)
    {
        RTLOG_DEBUG(moduleName, "Failed to register the proc FS entry.");
    }
    else
    {
        procEntry->read_proc = readProcFsPage;
    }

    goto TheEnd;

FullCleanUp:
Level1CleanUp:
    remove_proc_entry(moduleName, NULL);

    /**
     * Do synchronous clean up here. Set status only if
     * (stage == RT_TOOLS_MODULE_STAGE_EXIT).
     */
    temporaryStatus = firstCleanupSection();
    if(stage == RT_TOOLS_MODULE_STAGE_EXIT)
    {
        status = temporaryStatus;
    }

TheEnd:
    return status;
}


/**
 * Module entry point for loading it.
 */
static int __init avme9670drv_init(void)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif
    int status = RT_TOOLS_MODULE_INIT_ERROR;

    if((status = avme9670drv_main(RT_TOOLS_MODULE_STAGE_INIT))
    == RT_TOOLS_MODULE_INIT_SUCCESS)
    {
        RTLOG_INFO(moduleName, "Module initialized successfully.");
    }
    else
    {
        RTLOG_ERROR(moduleName, "Failed to initialize module!");
    }

    return status;
}

/**
 * Module entry point for unloading it.
 */
static void __exit avme9670drv_exit(void)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

    if(avme9670drv_main(RT_TOOLS_MODULE_STAGE_EXIT)
       == RT_TOOLS_MODULE_EXIT_SUCCESS)
    {
        RTLOG_INFO(moduleName, "Module cleaned up successfully.");
    }
    else
    {
        RTLOG_ERROR(moduleName, "Failed to clean up module!");
    }
}

module_init(avme9670drv_init);
module_exit(avme9670drv_exit);
