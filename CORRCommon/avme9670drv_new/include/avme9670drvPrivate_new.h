#ifndef AVME9670DRVPRIVATE_H_
#define AVME9670DRVPRIVATE_H_
/*******************************************************************************
* ALMA - Atacama Large Millimeter Array
* (c) Associated Universities Inc., 2004
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
*/


#define IP_CARRIER_VME_BASE_ADDR 0
#define IP_CARRIER_WINMAP_LENGHT (sizeof(BOARD_MEMORY_MAP))
#define CARRIER_LIST_SIZE 256


/**
 * Local helper functions for avme9670.
 */
static int findCarrierInClientList(volatile BOARD_MEMORY_MAP* carrier);
static void addCarrierToClientList(volatile BOARD_MEMORY_MAP* carrier);
static void removeCarrierFromClientList(volatile BOARD_MEMORY_MAP* carrier);
static int firstInitSection(void);
static int firstCleanupSection(void);
static int avme9670drv_main(const int stage);

#ifndef USE_RTOS
#define RTLOG_ERROR(moduleName, format, args...)\
    {\
        printk(KERN_ERR "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }
#define RTLOG_WARNING(moduleName, format, args...)\
    {\
        printk(KERN_WARNING "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }
#define RTLOG_INFO(moduleName, format, args...)\
    {\
        printk(KERN_INFO "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }
#define RTLOG_DEBUG(moduleName, format, args...)\
    {\
        printk(KERN_DEBUG "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }

#define RT_TOOLS_MODULE_STAGE_INIT 1
#define RT_TOOLS_MODULE_STAGE_EXIT 2
#define RT_TOOLS_MODULE_STAGE_CLEANUP 3

#define RT_TOOLS_MODULE_INIT_SUCCESS 0
#define RT_TOOLS_MODULE_INIT_SUCCESS 0
#define RT_TOOLS_MODULE_EXIT_SUCCESS 0

#define RT_TOOLS_MODULE_ERROR_BASE 256
#define RT_TOOLS_MODULE_INIT_ERROR -(RT_TOOLS_MODULE_ERROR_BASE + 1)
#define RT_TOOLS_MODULE_INIT_TIMEOUT -(RT_TOOLS_MODULE_ERROR_BASE + 2)
#define RT_TOOLS_MODULE_CLEANUP_ERROR -(RT_TOOLS_MODULE_ERROR_BASE + 3)
#define RT_TOOLS_MODULE_NO_INIT_TASK_ERROR -(RT_TOOLS_MODULE_ERROR_BASE + 4)
#define RT_TOOLS_MODULE_INIT_EXIT_ERROR -(RT_TOOLS_MODULE_ERROR_BASE + 5)
#define RT_TOOLS_MODULE_PARAM_ERROR -(RT_TOOLS_MODULE_ERROR_BASE + 6)
#endif
#endif
