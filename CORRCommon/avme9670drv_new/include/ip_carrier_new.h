#ifndef IP_CARRIER_H
#define IP_CARRIER_H
/* @(#) $Id$
 * Copyright (C) 2001
 * Associated Universities, Inc. Washington DC, USA.
 *
 * Produced for the ALMA project
 *
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU Library General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is distributed in the hope that it will be useful but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public 
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License 
 * along with this library; if not, write to the Free Software Foundation, 
 * Inc., 675 Massachusetts Ave, Cambridge, MA, 02139, USA.
 *
 * Correspondence concerning ALMA should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 *
 */


#include <linux/types.h>


/**
 * Definition to program the Acromag AVME9670 carrier board.
 */

/******************************************************************************
 ****                                                                      ****
 ****   All information below here is for the private use of the carrier   ****
 ****   driver only.  A user should have NO use for this information.      ****
 ****                                                                      ****
 ******************************************************************************/
/**
 * AVME9670 register memory map (1024 bytes total).
 */
typedef struct
{
    uint16_t IPAio[64]; /* slot A i/o space */
    uint16_t IPAid[32]; /* slot A ID space */
    uint16_t controlReg; /* status/control register */
    uint16_t intLevel; /* interrupt level register */
    uint16_t ipError; /* IP error register */
    uint16_t ipMemoryEnable; /* IP memory enable register */
    uint16_t _unused1[4];
    uint16_t ipABAS; /* IP A base address & size register */
    uint16_t ipBBAS; /* IP B base address & size register */
    uint16_t ipCBAS; /* IP C base address & size register */
    uint16_t ipDBAS; /* IP D base address & size register */
    uint16_t _unused2[4];
    uint16_t intEnable; /* interrupt enable register */
    uint16_t intPending; /* interrupt pending register */
    uint16_t intClear; /* interrupt clear register */
    uint16_t _unused3[13];
    uint16_t IPBio[64]; /* slot B i/o space */
    uint16_t IPBid[32]; /* slot B ID space */
    uint16_t _unused4[32];
    uint16_t IPCio[64]; /* slot C i/o space */
    uint16_t IPCid[32]; /* slot C ID space */
    uint16_t _unused5[32];
    uint16_t IPDio[64]; /* slot D i/o space */
    uint16_t IPDid[32]; /* slot D ID space */
    uint16_t _unused6[32];
} BOARD_MEMORY_MAP;

/**
 * Status/control register bits.
 */
#define INT_PENDING 0x04	/* global interrupt pending bit */
#define INT_ENABLE 0x08	/* global interrupt enable */
#define SOFTWARE_RESET 0x10	/* reset carrier */
#define AUTO_CLEAR_INT 0x80	/* auto clear pending interrupt */

/**
 * Interrupt enable, pending, and clear register bits.
 */
#define IPA_INT0 0x01	/* IP A Int 0 bit */
#define IPA_INT1 0x02	/* IP A Int 1 bit */
#define IPB_INT0 0x04	/* IP B Int 0 bit */
#define IPB_INT1 0x08	/* IP B Int 1 bit */
#define IPC_INT0 0x10	/* IP C Int 0 bit */
#define IPC_INT1 0x20	/* IP C Int 1 bit */
#define IPD_INT0 0x40	/* IP D Int 0 bit */
#define IPD_INT1 0x80	/* IP D Int 1 bit */

/**
 * A24 memory enable register bits.
 */
#define IPA_MEMENA 0x01	/* IP A bit */
#define IPB_MEMENA 0x02	/* IP B bit */
#define IPC_MEMENA 0x04	/* IP C bit */
#define IPD_MEMENA 0x08	/* IP D bit */
#endif
