/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2007 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 *
 * who       when       what
 * --------  ---------- ----------------------------------------------
 * ramestic  2007-11-29 created (copied and adapte to user space from rtGuardTest.c)
 */

/*
 * This is a simple test program which verifies that the rtGuards are working
 * in user space.
 */

//
// System stuff
//
#include <stdlib.h>
#include <iostream>

//
// ACS stuff
//
#include <rtLog.h>

//
// CORRCommon stuff
//
#include <rtTools.h>

using namespace std;

#define modName "rtGuardUserTest"

/*
 * Sleep time in seconds for the time stamp based guards.
 * A sleep time of 0.5s is sometimes (not reproducible) too short. :(
 */
const float sleepTime = 0.07;

/*
 * Test RTGUARD_STD in RTGUARD_MODE_COUNTER mode.
 */
int test1(void)
{
    const char* testName = "test1";
    rtlogRecord_t logRecord;
    int status = 0;
    unsigned int loop , counter = 0U;

    RTGUARD_STD(10, 0, RTGUARD_MODE_COUNTER);

    for(loop = 1U; loop <= 21U; ++loop)
    {
        if(rtGuard_checkStd() == 1)
        {
            ++counter;
            RTLOG_INFO(modName,
                       "%s: This message should be printed once every 10 "
                       "cycles (current cycle = %u, printed %u times)",
                       testName, loop, counter);
        }
    }

    /*
     * Check the result.
     */
    if(counter == 2U)
    {
        status = 1;
    }

    RTLOG_INFO(modName, "%s done. (%s)", testName, (status ? "SUCCESSFUL" : "FAILED"));

    return status;
}

/*
 * Test RTGUARD in RTGUARD_MODE_COUNTER mode.
 */
int test2(void)
{
    const char* testName = "test2";
    rtlogRecord_t logRecord;
    int status = 0;
    unsigned int loop , counter = 0U;

    RTGUARD(guard, 10, 0, RTGUARD_MODE_COUNTER);

    for(loop = 1U; loop <= 21U; ++loop)
    {
        if(rtGuard_check(&guard) == 1)
        {
            ++counter;
            RTLOG_INFO(
                modName, "%s: This message should be printed once every 10 "
                "cycles (current cycle = %u, printed %u times)",
                testName, loop, counter);
        }
    }

    /*
     * Check the result.
     */
    if(counter == 2U)
    {
        status = 1;
    }

    RTLOG_INFO(modName, "%s done. (%s)", testName, (status ? "SUCCESSFUL" : "FAILED"));

    return status;
}

/*
 * Test RTGUARD_STD in RTGUARD_MODE_TIMER mode.
 */
int test3(void)
{
    const char* testName = "test3";
    rtlogRecord_t logRecord;
    int status = 0;
    unsigned int loop , counter = 0U;

    RTGUARD_STD(0, 0.5, RTGUARD_MODE_TIMER);

    for(loop = 1U; loop <= 21U; ++loop)
    {
        if(rtGuard_checkStd() == 1)
        {
            ++counter;
            RTLOG_INFO(
                modName, "%s: This message should be printed three times every "
                "20 cycles (current cycle = %u, printed %u times)",
                testName, loop, counter);
        }

        rt_sleep(nano2count((RTIME)(sleepTime * 1000000000LL))); 
    }

    /*
     * Check the result.
     */
    if(counter == 3U)
    {
        status = 1;
    }

    RTLOG_INFO(modName, "%s done. (%s)", testName, (status ? "SUCCESSFUL" : "FAILED"));

    return status;
}

/*
 * Test RTGUARD in RTGUARD_MODE_TIMER mode.
 */
int test4(void)
{
    const char* testName = "test4";
    rtlogRecord_t logRecord;
    int status = 0;
    unsigned int loop , counter = 0U;

    RTGUARD(guard, 0, 0.5, RTGUARD_MODE_TIMER);

    for(loop = 1U; loop <= 21U; ++loop)
    {
        if(rtGuard_check(&guard) == 1)
        {
            ++counter;
            RTLOG_INFO(modName,
                       "%s: This message should be printed three times every "
                       "20 cycles (current cycle = %u, printed %u times)",
                       testName, loop, counter);
        }

        rt_sleep(nano2count((RTIME)(sleepTime * 1000000000LL))); 
    }

    /*
     * Check the result.
     */
    if(counter == 3U)
    {
        status = 1;
    }

    RTLOG_INFO(modName, "%s done. (%s)", testName, (status ? "SUCCESSFUL" : "FAILED"));

    return status;
}

/*
 * Test RTGUARD_STD in RTGUARD_MODE_BOTH mode.
 */
int test5(void)
{
    const char* testName = "test5";
    rtlogRecord_t logRecord;
    int status = 0;
    unsigned int loop , counter = 0U;

    RTGUARD_STD(10, 0.5, RTGUARD_MODE_BOTH);

    for(loop = 1U; loop <= 21U; ++loop)
    {
        if(rtGuard_checkStd() == 1)
        {
            ++counter;
            RTLOG_INFO(
                modName, "%s: This message should be printed five times every "
                "20 cycles (current cycle = %u, printed %u times)",
                testName, loop, counter);
        }

        rt_sleep(nano2count((RTIME)(sleepTime * 1000000000LL))); 
    }

    /*
     * Check the result.
     */
    if(counter == 5U)
    {
        status = 1;
    }

    RTLOG_INFO(modName, "%s done. (%s)", testName, (status ? "SUCCESSFUL" : "FAILED"));

   return status;
}

/*
 * Test RTGUARD in RTGUARD_MODE_BOTH mode.
 */
int test6(void)
{
    const char* testName = "test6";
    rtlogRecord_t logRecord;
    int status = 0;
    unsigned int loop , counter = 0U;

    RTGUARD(guard, 10, 0.5, RTGUARD_MODE_BOTH);

    for(loop = 1U; loop <= 21U; ++loop)
    {
        if(rtGuard_check(&guard) == 1)
        {
            ++counter;
            RTLOG_INFO(
                modName, "%s: This message should be printed five times every "
                "20 cycles (current cycle = %u, printed %u times)",
                testName, loop, counter);
        }

        rt_sleep(nano2count((RTIME)(sleepTime * 1000000000LL))); 
    }

    /*
     * Check the result.
     */
    if(counter == 5U)
    {
        status = 1;
    }

    RTLOG_INFO(modName, "%s done. (%s)", testName,
        (status ? "SUCCESSFUL" : "FAILED"));

    return status;
}

/*
 * Test RTGUARD_STD in RTGUARD_MODE_COUNTER mode where
 * one cycle is suppressed due to the initially disabled guard.
 */
int test7(void)
{
    const char* testName = "test7";
    rtlogRecord_t logRecord;
    int status = 0;
    unsigned int loop , counter = 0U;

    RTGUARD_STD(10, 0, RTGUARD_MODE_COUNTER);
    rtGuard_disableStd();

    for(loop = 1U; loop <= 21U; ++loop)
    {
        if(rtGuard_checkStd() == 1)
        {
            ++counter;
            RTLOG_INFO(
                modName, "%s: This message should be printed once every 20 "
                "cycles (current cycle = %u, printed %u times)",
                testName, loop, counter);
        }
    }

    /*
     * Check the result.
     */
    if(counter == 1U)
    {
        status = 1;
    }

    RTLOG_INFO(modName, "%s done. (%s)", testName,
        (status ? "SUCCESSFUL" : "FAILED"));

    return status;
}

/*
 * Test RTGUARD in RTGUARD_MODE_COUNTER mode where
 * one cycle is suppressed due to the initially disabled guard.
 */
int test8(void)
{
    const char* testName = "test8";
    rtlogRecord_t logRecord;
    int status = 0;
    unsigned int loop , counter = 0U;

    RTGUARD(guard, 10, 0, RTGUARD_MODE_COUNTER);
    rtGuard_disable(&guard);

    for(loop = 1U; loop <= 21U; ++loop)
    {
        if(rtGuard_check(&guard) == 1)
        {
            ++counter;
            RTLOG_INFO(
                modName, "%s: This message should be printed once every 20 "
                "cycles (current cycle = %u, printed %u times)",
                testName, loop, counter);
        }
    }

    /*
     * Check the result.
     */
    if(counter == 1U)
    {
        status = 1;
    }

    RTLOG_INFO(modName, "%s done. (%s)", testName,
        (status ? "SUCCESSFUL" : "FAILED"));

    return status;
}

/*
 * Test RTGUARD_STD in RTGUARD_MODE_TIMER mode where
 * one cycle is suppressed due to the initially disabled guard.
 */
int test9(void)
{
    const char* testName = "test9";
    rtlogRecord_t logRecord;
    int status = 0;
    unsigned int loop , counter = 0U;

    RTGUARD_STD(0, 0.5, RTGUARD_MODE_TIMER);
    rtGuard_disableStd();

    for(loop = 1U; loop <= 21U; ++loop)
    {
        if(rtGuard_checkStd() == 1)
        {
            ++counter;
            RTLOG_INFO(
                modName, "%s: This message should be printed twice every 20 "
                "cycles (current cycle = %u, printed %u times)",
                testName, loop, counter);
        }

        rt_sleep(nano2count((RTIME)(sleepTime * 1000000000LL))); 
    }

    /*
     * Check the result.
     */
    if(counter == 2U)
    {
        status = 1;
    }

    RTLOG_INFO(modName, "%s done. (%s)", testName,
        (status ? "SUCCESSFUL" : "FAILED"));

    return status;
}

/*
 * Test RTGUARD in RTGUARD_MODE_TIMER mode where
 * one cycle is suppressed due to the initially disabled guard.
 */
int test10(void)
{
    const char* testName = "test10";
    rtlogRecord_t logRecord;
    int status = 0;
    unsigned int loop , counter = 0U;

    RTGUARD(guard, 0, 0.5, RTGUARD_MODE_TIMER);
    rtGuard_disable(&guard);

    for(loop = 1U; loop <= 21U; ++loop)
    {
        if(rtGuard_check(&guard) == 1)
        {
            ++counter;
            RTLOG_INFO(
                modName, "%s: This message should be printed twice every 20 "
                "cycles (current cycle = %u, printed %u times)",
                testName, loop, counter);
        }

        rt_sleep(nano2count((RTIME)(sleepTime * 1000000000LL))); 
    }

    /*
     * Check the result.
     */
    if(counter == 2U)
    {
        status = 1;
    }

    RTLOG_INFO(modName, "%s done. (%s)", testName,
        (status ? "SUCCESSFUL" : "FAILED"));

    return status;
}

/*
 * Test RTGUARD_STD in RTGUARD_MODE_TIMER mode where
 * one cycle is suppressed due to the initially disabled guard.
 */
int test11(void)
{
    const char* testName = "test11";
    rtlogRecord_t logRecord;
    int status = 0;
    unsigned int loop , counter = 0U;

    RTGUARD_STD(10, 0.5, RTGUARD_MODE_BOTH);
    rtGuard_disableStd();

    for(loop = 1U; loop <= 21U; ++loop)
    {
        if(rtGuard_checkStd() == 1)
        {
            ++counter;
            RTLOG_INFO(
                modName, "%s: This message should be printed four times every "
                "20 cycles (current cycle = %u, printed %u times)",
                testName, loop, counter);
        }

        rt_sleep(nano2count((RTIME)(sleepTime * 1000000000LL))); 
    }

    /*
     * Check the result.
     */
    if(counter == 4U)
    {
        status = 1;
    }

    RTLOG_INFO(modName, "%s done. (%s)", testName, (status ? "SUCCESSFUL" : "FAILED"));

   return status;
}

/*
 * Test RTGUARD in RTGUARD_MODE_TIMER mode where
 * one cycle is suppressed due to the initially disabled guard.
 */
int test12(void)
{
    const char* testName = "test12";
    rtlogRecord_t logRecord;
    int status = 0;
    unsigned int loop , counter = 0U;

    RTGUARD(guard, 10, 0.5, RTGUARD_MODE_BOTH);
    rtGuard_disable(&guard);

    for(loop = 1U; loop <= 21U; ++loop)
    {
        if(rtGuard_check(&guard) == 1)
        {
            ++counter;
            RTLOG_INFO(
                modName, "%s: This message should be printed four times every "
                "20 cycles (current cycle = %u, printed %u times)",
                testName, loop, counter);
        }

        rt_sleep(nano2count((RTIME)(sleepTime * 1000000000LL))); 
    }

    /*
     * Check the result.
     */
    if(counter == 4U)
    {
        status = 1;
    }

    RTLOG_INFO(modName, "%s done. (%s)", testName, (status ? "SUCCESSFUL" : "FAILED"));

    return status;
}

int main(int argc, char** argv) 
{
    int status = 1;
    RT_TASK *task;

    cout << "Test task running..." << endl;

    //
    // initialize our rtai environment
    //
    rt_allow_nonroot_hrt();
    if ( !(task = rt_task_init_schmod(nam2num("TEST"), 
				      10,
				      16384, /* 16K */
				      0,
				      SCHED_FIFO,
				      0x1)) )
    {
        cout << "CANNOT INIT TASK" << endl;

        return 1;
    }

    if ( !test1() )
    {
        status = 0;
    }

    if ( !test2() )
    {
        status = 0;
    }

    if ( !test3() )
    {
        status = 0;
    }

    if ( !test4() )
    {
        status = 0;
    }

    if ( !test5() )
    {
        status = 0;
    }

    if ( !test6() )
    {
        status = 0;
    }

    if ( !test7() )
    {
        status = 0;
    }

    if ( !test8() )
    {
        status = 0;
    }

    if ( !test9() )
    {
        status = 0;
    }

    if ( !test10() )
    {
        status = 0;
    }

    if ( !test11() )
    {
        status = 0;
    }

    if ( !test12() )
    {
        status = 0;
    }

    //
    // deallocate rtai's resource
    //
    rt_task_delete(task);

    //
    // Done.
    //
    cout << "rtGuard test " << (status ? "SUCCESSFUL" : "FAILED") << endl;

    return 0;
}

/*___oOo___*/
