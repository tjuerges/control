/*******************************************************************************
*
* "@(#) $Id$"
*
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* who       when        what
* --------  ----------  ----------------------------------------------
* tjuerges  2008-07-29  created
*/


/*
 * System stuff
 */
#include <linux/module.h>
#include <linux/moduleparam.h>

/*
 * RTOS stuff
 */
#include <rtai_sched.h>

/*
 * ACS stuff
 */
#include <rtLog.h>

#include <rtTools.h>


/*
 * used for logging
 */
#define moduleName	"rtToolsFindSymbolName_k"


MODULE_AUTHOR("Thomas Juerges <tjuerges@nrao.edu>");
MODULE_DESCRIPTION(moduleName ": LKM which tests the symbol name lookup "
    "functionality in the rtTools LKM.");
MODULE_LICENSE("GPL");


/*
 * module parameters
 */
static unsigned int initTimeout = 1000;         /* msec */
static unsigned int cleanUpTimeout = 1000;      /* msec */
module_param(initTimeout, uint, S_IRUGO);
module_param(cleanUpTimeout, uint, S_IRUGO);


/*
 * my local data
 */
RT_TASK findSymNameTask;


static void findSymNameTest(long foo __attribute__((unused)))
{
    const char* module = "rtTools";
    const char* symbol = "rtToolsWaitOnFlag";
    const int MAX_CYCLES = 10;
    int loop = MAX_CYCLES;
    int ret = 0;
    rtlogRecord_t logRecord;


    for(; loop >= 0; --loop)
    {
        if(rtToolsLookupSymbol(module, symbol) == 0)
        {
            ret = -1;
            RTLOG_ERROR(moduleName, "rtToolsLookupSymbol failed after %d "
                "of %d cycles.", loop, MAX_CYCLES);
            break;
        }
    }

    if((ret == 0) && (loop <= 0))
    {
        RTLOG_INFO(moduleName, "findSymNameTest PASSED OKAY.");
    }
    else
    {
        RTLOG_ERROR(moduleName, "findSymNameTest FAILED.");
    }
}

static int rtToolsTest_main(int stage)
{
	int status = RT_TOOLS_MODULE_INIT_ERROR;
	rtlogRecord_t logRecord;

	if(stage == RT_TOOLS_MODULE_STAGE_INIT)
	{
		RTLOG_INFO(moduleName, "$Id$");
		RTLOG_INFO(moduleName, "Initializing module...");
		RTLOG_INFO(moduleName, "initTimeout = %dms", initTimeout);
		RTLOG_INFO(moduleName, "cleanUpTimeout = %dms", cleanUpTimeout);

		goto Initialization;
	}
	else
	{
		RTLOG_INFO(moduleName, "cleaning up module...");

		goto FullCleanUp;
	}

Initialization:

    /*
     * Spawn the test task.
     */
    if(rt_task_init(&findSymNameTask, findSymNameTest, 0, 8000, 10, 1, 0)
        != 0)
    {
        RTLOG_ERROR(moduleName, "Cannot spawn the test task!");
        return status;
    }
    else
    {
        status = RT_TOOLS_MODULE_INIT_SUCCESS;
    }

    /*
     * Resume the test task.
     */
    rt_task_resume(&findSymNameTask);

	goto Exit;

FullCleanUp:
    /*
     * Make sure that the test task has stopped.
     */
    rt_task_delete(&findSymNameTask);
    status = RT_TOOLS_MODULE_EXIT_SUCCESS;
           
Exit:
	return status;
}


static int __init rtToolsFindSymbolName_init(void)
{
	int status;
	rtlogRecord_t logRecord;

	if((status = rtToolsTest_main(RT_TOOLS_MODULE_STAGE_INIT))
	    == RT_TOOLS_MODULE_INIT_SUCCESS )
	{
		RTLOG_INFO(moduleName, "Module initialized successfully.");
	}
	else
	{
		RTLOG_ERROR(moduleName, "Failed to initialize module.");
		return status;
	}

	RTLOG_INFO(moduleName, "Running rtToolsFindSymbolName test...");

	return status;
}

static void __exit rtToolsFindSymbolName_exit(void)
{
	rtlogRecord_t logRecord;

	if(rtToolsTest_main(RT_TOOLS_MODULE_STAGE_EXIT)
	    == RT_TOOLS_MODULE_EXIT_SUCCESS)
	{
		RTLOG_INFO(moduleName, "Module cleaned up successfully.");
	}
	else
	{
		RTLOG_ERROR(moduleName, "Failed to clean-up module.");
	}
}

module_init(rtToolsFindSymbolName_init);
module_exit(rtToolsFindSymbolName_exit);
