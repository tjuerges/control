/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2004
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* ...
*
* who       when        what
* --------  ----------  ----------------------------------------------
* tjuerges  2006-03-20  created
*/

/************************************************************************
*   NAME
*	rtToolsFifoCmdTest.cpp
*
*   SYNOPSIS
*
*   DESCRIPTION
*
*   FILES
*
*   ENVIRONMENT
*
*   COMMANDS
*
*   RETURN VALUES
*
*   CAUTIONS
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS
*
*------------------------------------------------------------------------
*/

#include <iostream>

#include <rtai_lxrt.h>
/**
 * FIXME: TJ, RA
 * Use mailboxes instead of FIFOs.
#include <rtai_mbx.h>
 */ 
#include <rtai_fifos.h>

#include <rtToolsFifoCmd.h>
#include "rtToolsTest.h"


int runTest(void)
{
	my_message_t reply, message;

	reply.dvalue = 0.0;
	reply.fvalue = 0.0;
	reply.ullvalue = 0ULL;

	message.dvalue = 42.4242424242;
	message.fvalue = 24.2424242424;
	message.ullvalue = 0xa0a0a0a055555555ULL;

	for(std::size_t i(0); i< 1000; ++i)
	{
		message.cvalue[i] = static_cast<unsigned char>(i & 0xff);
		reply.cvalue[i] = 0;
	}

	std::cout << message.cvalue[0] << std::endl;

	rtToolsCmdBuffer<my_message_t> cmd(15, message);

	rtToolsFifoCmd* fifoCmd(0);

	try
	{
		fifoCmd = new rtToolsFifoCmd(fifoName);
	}
	catch(const rtToolsFifoCmdErr_t& ex)
	{
		std::cout << "Could not create an rtToolsFifoCmd instance (fifoName = "
			<< fifoName
			<< "). Exception #"
			<< ex
			<< std::endl;

		return -1;
	};

	std::cout << "Using fifo "
		<< fifoName
		<< "."
		<< std::endl;

	const int size(cmd.size());


	for(int i(0); i< 1000; ++i)
	{
		try
		{
			fifoCmd->sendRecvCmd(cmd.pack(), size, &reply, sizeof(my_message_t), 500);
		}
		catch(const rtToolsFifoCmdErr_t& ex)
		{
			std::cout << "Could not send/receive fifo command. Loop #"
				<< i
				<< ". Exception #"
				<< ex
				<< std::endl;

			delete fifoCmd;

			return -1;
		}

		if(reply.dvalue != cmd->dvalue)
		{
			delete fifoCmd;
			std::cout << "The FIFO did not return the correct double value:"
				<< reply.dvalue
				<< ", expected: "
				<< cmd->dvalue
				<< std::endl;
			return -1;
		}

		if(reply.fvalue != cmd->fvalue)
		{
			delete fifoCmd;
			std::cout << "The FIFO did not return the correct float value:"
				<< reply.fvalue
				<< ", expected: "
				<< cmd->fvalue
				<< std::endl;
			return -1;
		}

		if(reply.ullvalue != cmd->ullvalue)
		{
			delete fifoCmd;
			std::cout << "The FIFO did not return the correct ULL value:"
				<< reply.ullvalue
				<< ", expected: "
				<< cmd->ullvalue
				<< std::endl;
			return -1;
		}

		for(int j(0); j < 1000; ++j)
		{
			if(reply.cvalue[j] != cmd->cvalue[j])
			{
				delete fifoCmd;
				std::cout << "The FIFO did not return the correct character:"
					<< reply.cvalue[j]
					<< ", expected: "
					<< cmd->cvalue[j]
					<< ", index = "
					<< j
					<< std::endl;
				return -1;
			}
		}
	}

	delete fifoCmd;
	return 0;
};

int main(int argc, char* argv[])
{
	int ret(-1), numberOfRuns(1);

	if(argc > 1)
	{
		numberOfRuns = std::atoi(argv[1]);

		if(numberOfRuns == 0)
		{
			std::cout << argv[0] << ": Please provide a number!" << std::endl;
			return -1;
		}
	}

	for(int j(0); j < numberOfRuns; ++j)
	{
		ret = runTest();

		if(ret != 0)
		{
			break;
		}
	}

	std::cout << "Test: ";
	if(ret != 0)
	{
		std::cout << "NOT ";
	}

	std::cout << "passed!" << std::endl;

	return ret;
}
