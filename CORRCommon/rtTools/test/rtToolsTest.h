#ifndef RTTOOLSFIFOCMDTEST_H_
#define RTTOOLSFIFOCMDTEST_H_

#include <rtTools.h>

const unsigned int fifoNumber = 9;
const char* fifoName = FIFONAME(9);

typedef struct my_message
{
	double dvalue;
	float fvalue;
	unsigned long long ullvalue;
	char cvalue[1000];
} my_message_t;

#endif /*RTTOOLSFIFOCMDTEST_H_*/
