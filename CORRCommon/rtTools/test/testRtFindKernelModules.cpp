/* @(#) $Id$
 *
 * Copyright (C) 2006
 * Associated Universities, Inc. Washington DC, USA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * Correspondence concerning ALMA Software should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */

#include <iostream>
#include <cstdio>
#include <cstdlib>

#include <sys/time.h>

#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestCaller.h>
#include <cppunit/ui/text/TestRunner.h>

#include "rtFindKernelModules.h"

using namespace std;

class rtFindKernelModulesUnitTest : public CppUnit::TestFixture
{
public:
	/// default constructor
	rtFindKernelModulesUnitTest()
	: CppUnit::TestFixture(){}

	/// copy constructor
	rtFindKernelModulesUnitTest( const rtFindKernelModulesUnitTest &toCopy)
	{ *this = toCopy; }

	/// assignment operator
	rtFindKernelModulesUnitTest &operator =( const rtFindKernelModulesUnitTest &)
	{ return *this; }

	/// destructor
	~rtFindKernelModulesUnitTest() {}

	void setUp();
	void tearDown();

	static CppUnit::Test *suite();

private:

	void runFindKernelModulesTests();

	/// Timing routines
	struct timeval _tstart, _tend;
	struct timezone tz;
	void tstart(void)
	{ gettimeofday(&_tstart, &tz); }
	void tend(void)
	{ gettimeofday(&_tend,&tz); }

	double tval()
	{
		double t1, t2;
		t1 =  (double)_tstart.tv_sec + (double)_tstart.tv_usec/(1000*1000);
		t2 =  (double)_tend.tv_sec + (double)_tend.tv_usec/(1000*1000);
		return t2-t1;
	}
};
/*
Macro examples
	CPPUNIT_ASSERT_EQUAL_MESSAGE("msg", expected value, actual value);
	CPPUNIT_ASSERT_MESSAGE("msg", boolean value );
	CPPUNIT_ASSERT( boolean value );
*/
//------------------------------------------------------------------------------
void rtFindKernelModulesUnitTest::setUp()
{
}

//------------------------------------------------------------------------------
void rtFindKernelModulesUnitTest::tearDown()
{
}

//------------------------------------------------------------------------------
CppUnit::Test *rtFindKernelModulesUnitTest::suite()
{
	CppUnit::TestSuite *suiteOfTests = new CppUnit::TestSuite( "rtFindKernelModulesUnitTest" );

    suiteOfTests->addTest( new CppUnit::TestCaller<rtFindKernelModulesUnitTest>(
			       "runFindKernelModulesTests", 
			       &rtFindKernelModulesUnitTest::runFindKernelModulesTests ) );
    return suiteOfTests;
}

//------------------------------------------------------------------------------
void rtFindKernelModulesUnitTest::runFindKernelModulesTests()
{
    vector<string> goodModules;
    goodModules.push_back("rtTools");
    CPPUNIT_ASSERT_EQUAL_MESSAGE("Can't find rtTools, should exist", true, 
				 findKernelModules(goodModules));
    vector<string> goodAndBadModules;
    goodAndBadModules.push_back("rtTools");
    goodAndBadModules.push_back("non-existantModule0");
    CPPUNIT_ASSERT_EQUAL_MESSAGE("Incorrectly found non-existantModule0", false, 
				 findKernelModules(goodAndBadModules));

    vector<string> badModules;
    badModules.push_back("non-existantModule1");
    badModules.push_back("non-existantModule2");
    CPPUNIT_ASSERT_EQUAL_MESSAGE("Incorrectly found non-existantModule1 & non-existantModule2", false, 
				 findKernelModules(badModules));
}

//------------------------------------------------------------------------------
int main( int argc, char **argv )
{
	int numberOfRuns(1), result(0);

	if(argc > 1)
	{
		numberOfRuns = std::atoi(argv[1]);
		if(numberOfRuns == 0)
		{
			std::cout << argv[0] << ": Please provide a number!" << std::endl;
			return -1;
		}
	}

   	CppUnit::TextUi::TestRunner runner;
   	runner.addTest( rtFindKernelModulesUnitTest::suite() );

	for(int j(0); j < numberOfRuns; ++j)
	{
		if(runner.run() == false)
		{
			result = -1;
			break;
		}
	}

	return result;
}
