/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2007 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 *
 * who       when      what
 * --------  --------  ----------------------------------------------
 * tjuerges  Oct 5, 2007  created 
 */

/*
 * This is a simple test module which verifies that the rtGuards are working.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/time.h>
#include <asm/atomic.h>

#include <rtai.h>
#include <rtai_sched.h>
#include <rtai_sem.h>

#include <rtLog.h>
#include <rtTools.h>


#define modName "rtGuardTest"

MODULE_AUTHOR("Thomas Juerges <tjuerges@nrao.edu>");
MODULE_DESCRIPTION("Real time kernel guard test");
MODULE_LICENSE("GPL");


static atomic_t finished = ATOMIC_INIT(1);
static RT_TASK taskID;

/*
 * Sleep time in seconds for the time stamp based guards.
 * A sleep time of 0.5s is sometimes (not reproducible) too short. :(
 */
static const float sleepTime = 0.07;


/*
 * Test RTGUARD_STD in RTGUARD_MODE_COUNTER mode.
 */
static int test1(void)
{
    const char* testName = "test1";
    rtlogRecord_t logRecord;
    int status = RT_TOOLS_MODULE_INIT_ERROR;
    unsigned int loop , counter = 0U;

    RTGUARD_STD(10, 0, RTGUARD_MODE_COUNTER);

    for(loop = 1U; loop <= 21U; ++loop)
    {
        if(rtGuard_checkStd() == 1)
        {
            ++counter;
            RTLOG_INFO(
                modName, "%s: This message should be printed once every 10 "
                "cycles (current cycle = %u, printed %u times)",
                testName, loop, counter);
        }
    }

    /*
     * Check the result.
     */
    if(counter == 2U)
    {
        status = RT_TOOLS_MODULE_INIT_SUCCESS;
    }

    RTLOG_INFO(modName, "%s done. (%s)", testName,
        (status == RT_TOOLS_MODULE_INIT_SUCCESS ? "SUCCESSFUL" : "FAILED"));

    return status;
}

/*
 * Test RTGUARD in RTGUARD_MODE_COUNTER mode.
 */
static int test2(void)
{
    const char* testName = "test2";
    rtlogRecord_t logRecord;
    int status = RT_TOOLS_MODULE_INIT_ERROR;
    unsigned int loop , counter = 0U;

    RTGUARD(guard, 10, 0, RTGUARD_MODE_COUNTER);

    for(loop = 1U; loop <= 21U; ++loop)
    {
        if(rtGuard_check(&guard) == 1)
        {
            ++counter;
            RTLOG_INFO(
                modName, "%s: This message should be printed once every 10 "
                "cycles (current cycle = %u, printed %u times)",
                testName, loop, counter);
        }
    }

    /*
     * Check the result.
     */
    if(counter == 2U)
    {
        status = RT_TOOLS_MODULE_INIT_SUCCESS;
    }

    RTLOG_INFO(modName, "%s done. (%s)", testName,
        (status == RT_TOOLS_MODULE_INIT_SUCCESS ? "SUCCESSFUL" : "FAILED"));

    return status;
}

/*
 * Test RTGUARD_STD in RTGUARD_MODE_TIMER mode.
 */
static int test3(void)
{
    const char* testName = "test3";
    rtlogRecord_t logRecord;
    int status = RT_TOOLS_MODULE_INIT_ERROR;
    unsigned int loop , counter = 0U;

    RTGUARD_STD(0, 0.5, RTGUARD_MODE_TIMER);

    for(loop = 1U; loop <= 21U; ++loop)
    {
        if(rtGuard_checkStd() == 1)
        {
            ++counter;
            RTLOG_INFO(
                modName, "%s: This message should be printed three times every "
                "20 cycles (current cycle = %u, printed %u times)",
                testName, loop, counter);
        }

        rt_sleep(nano2count((RTIME)(sleepTime * 1000000000LL))); 
    }

    /*
     * Check the result.
     */
    if(counter == 3U)
    {
        status = RT_TOOLS_MODULE_INIT_SUCCESS;
    }

    RTLOG_INFO(modName, "%s done. (%s)", testName,
        (status == RT_TOOLS_MODULE_INIT_SUCCESS ? "SUCCESSFUL" : "FAILED"));

    return status;
}

/*
 * Test RTGUARD in RTGUARD_MODE_TIMER mode.
 */
static int test4(void)
{
    const char* testName = "test4";
    rtlogRecord_t logRecord;
    int status = RT_TOOLS_MODULE_INIT_ERROR;
    unsigned int loop , counter = 0U;

    RTGUARD(guard, 0, 0.5, RTGUARD_MODE_TIMER);

    for(loop = 1U; loop <= 21U; ++loop)
    {
        if(rtGuard_check(&guard) == 1)
        {
            ++counter;
            RTLOG_INFO(
                modName, "%s: This message should be printed three times every "
                "20 cycles (current cycle = %u, printed %u times)",
                testName, loop, counter);
        }

        rt_sleep(nano2count((RTIME)(sleepTime * 1000000000LL))); 
    }

    /*
     * Check the result.
     */
    if(counter == 3U)
    {
        status = RT_TOOLS_MODULE_INIT_SUCCESS;
    }

    RTLOG_INFO(modName, "%s done. (%s)", testName,
        (status == RT_TOOLS_MODULE_INIT_SUCCESS ? "SUCCESSFUL" : "FAILED"));

    return status;
}

/*
 * Test RTGUARD_STD in RTGUARD_MODE_BOTH mode.
 */
static int test5(void)
{
    const char* testName = "test5";
    rtlogRecord_t logRecord;
    int status = RT_TOOLS_MODULE_INIT_ERROR;
    unsigned int loop , counter = 0U;

    RTGUARD_STD(10, 0.5, RTGUARD_MODE_BOTH);

    for(loop = 1U; loop <= 21U; ++loop)
    {
        if(rtGuard_checkStd() == 1)
        {
            ++counter;
            RTLOG_INFO(
                modName, "%s: This message should be printed five times every "
                "20 cycles (current cycle = %u, printed %u times)",
                testName, loop, counter);
        }

        rt_sleep(nano2count((RTIME)(sleepTime * 1000000000LL))); 
    }

    /*
     * Check the result.
     */
    if(counter == 5U)
    {
        status = RT_TOOLS_MODULE_INIT_SUCCESS;
    }

    RTLOG_INFO(modName, "%s done. (%s)", testName,
        (status == RT_TOOLS_MODULE_INIT_SUCCESS ? "SUCCESSFUL" : "FAILED"));

   return status;
}

/*
 * Test RTGUARD in RTGUARD_MODE_BOTH mode.
 */
static int test6(void)
{
    const char* testName = "test6";
    rtlogRecord_t logRecord;
    int status = RT_TOOLS_MODULE_INIT_ERROR;
    unsigned int loop , counter = 0U;

    RTGUARD(guard, 10, 0.5, RTGUARD_MODE_BOTH);

    for(loop = 1U; loop <= 21U; ++loop)
    {
        if(rtGuard_check(&guard) == 1)
        {
            ++counter;
            RTLOG_INFO(
                modName, "%s: This message should be printed five times every "
                "20 cycles (current cycle = %u, printed %u times)",
                testName, loop, counter);
        }

        rt_sleep(nano2count((RTIME)(sleepTime * 1000000000LL))); 
    }

    /*
     * Check the result.
     */
    if(counter == 5U)
    {
        status = RT_TOOLS_MODULE_INIT_SUCCESS;
    }

    RTLOG_INFO(modName, "%s done. (%s)", testName,
        (status == RT_TOOLS_MODULE_INIT_SUCCESS ? "SUCCESSFUL" : "FAILED"));

    return status;
}

/*
 * Test RTGUARD_STD in RTGUARD_MODE_COUNTER mode where
 * one cycle is suppressed due to the initially disabled guard.
 */
static int test7(void)
{
    const char* testName = "test7";
    rtlogRecord_t logRecord;
    int status = RT_TOOLS_MODULE_INIT_ERROR;
    unsigned int loop , counter = 0U;

    RTGUARD_STD(10, 0, RTGUARD_MODE_COUNTER);
    rtGuard_disableStd();

    for(loop = 1U; loop <= 21U; ++loop)
    {
        if(rtGuard_checkStd() == 1)
        {
            ++counter;
            RTLOG_INFO(
                modName, "%s: This message should be printed once every 20 "
                "cycles (current cycle = %u, printed %u times)",
                testName, loop, counter);
        }
    }

    /*
     * Check the result.
     */
    if(counter == 1U)
    {
        status = RT_TOOLS_MODULE_INIT_SUCCESS;
    }

    RTLOG_INFO(modName, "%s done. (%s)", testName,
        (status == RT_TOOLS_MODULE_INIT_SUCCESS ? "SUCCESSFUL" : "FAILED"));

    return status;
}

/*
 * Test RTGUARD in RTGUARD_MODE_COUNTER mode where
 * one cycle is suppressed due to the initially disabled guard.
 */
static int test8(void)
{
    const char* testName = "test8";
    rtlogRecord_t logRecord;
    int status = RT_TOOLS_MODULE_INIT_ERROR;
    unsigned int loop , counter = 0U;

    RTGUARD(guard, 10, 0, RTGUARD_MODE_COUNTER);
    rtGuard_disable(&guard);

    for(loop = 1U; loop <= 21U; ++loop)
    {
        if(rtGuard_check(&guard) == 1)
        {
            ++counter;
            RTLOG_INFO(
                modName, "%s: This message should be printed once every 20 "
                "cycles (current cycle = %u, printed %u times)",
                testName, loop, counter);
        }
    }

    /*
     * Check the result.
     */
    if(counter == 1U)
    {
        status = RT_TOOLS_MODULE_INIT_SUCCESS;
    }

    RTLOG_INFO(modName, "%s done. (%s)", testName,
        (status == RT_TOOLS_MODULE_INIT_SUCCESS ? "SUCCESSFUL" : "FAILED"));

    return status;
}

/*
 * Test RTGUARD_STD in RTGUARD_MODE_TIMER mode where
 * one cycle is suppressed due to the initially disabled guard.
 */
static int test9(void)
{
    const char* testName = "test9";
    rtlogRecord_t logRecord;
    int status = RT_TOOLS_MODULE_INIT_ERROR;
    unsigned int loop , counter = 0U;

    RTGUARD_STD(0, 0.5, RTGUARD_MODE_TIMER);
    rtGuard_disableStd();

    for(loop = 1U; loop <= 21U; ++loop)
    {
        if(rtGuard_checkStd() == 1)
        {
            ++counter;
            RTLOG_INFO(
                modName, "%s: This message should be printed twice every 20 "
                "cycles (current cycle = %u, printed %u times)",
                testName, loop, counter);
        }

        rt_sleep(nano2count((RTIME)(sleepTime * 1000000000LL))); 
    }

    /*
     * Check the result.
     */
    if(counter == 2U)
    {
        status = RT_TOOLS_MODULE_INIT_SUCCESS;
    }

    RTLOG_INFO(modName, "%s done. (%s)", testName,
        (status == RT_TOOLS_MODULE_INIT_SUCCESS ? "SUCCESSFUL" : "FAILED"));

    return status;
}

/*
 * Test RTGUARD in RTGUARD_MODE_TIMER mode where
 * one cycle is suppressed due to the initially disabled guard.
 */
static int test10(void)
{
    const char* testName = "test10";
    rtlogRecord_t logRecord;
    int status = RT_TOOLS_MODULE_INIT_ERROR;
    unsigned int loop , counter = 0U;

    RTGUARD(guard, 0, 0.5, RTGUARD_MODE_TIMER);
    rtGuard_disable(&guard);

    for(loop = 1U; loop <= 21U; ++loop)
    {
        if(rtGuard_check(&guard) == 1)
        {
            ++counter;
            RTLOG_INFO(
                modName, "%s: This message should be printed twice every 20 "
                "cycles (current cycle = %u, printed %u times)",
                testName, loop, counter);
        }

        rt_sleep(nano2count((RTIME)(sleepTime * 1000000000LL))); 
    }

    /*
     * Check the result.
     */
    if(counter == 2U)
    {
        status = RT_TOOLS_MODULE_INIT_SUCCESS;
    }

    RTLOG_INFO(modName, "%s done. (%s)", testName,
        (status == RT_TOOLS_MODULE_INIT_SUCCESS ? "SUCCESSFUL" : "FAILED"));

    return status;
}

/*
 * Test RTGUARD_STD in RTGUARD_MODE_TIMER mode where
 * one cycle is suppressed due to the initially disabled guard.
 */
static int test11(void)
{
    const char* testName = "test11";
    rtlogRecord_t logRecord;
    int status = RT_TOOLS_MODULE_INIT_ERROR;
    unsigned int loop , counter = 0U;

    RTGUARD_STD(10, 0.5, RTGUARD_MODE_BOTH);
    rtGuard_disableStd();

    for(loop = 1U; loop <= 21U; ++loop)
    {
        if(rtGuard_checkStd() == 1)
        {
            ++counter;
            RTLOG_INFO(
                modName, "%s: This message should be printed four times every "
                "20 cycles (current cycle = %u, printed %u times)",
                testName, loop, counter);
        }

        rt_sleep(nano2count((RTIME)(sleepTime * 1000000000LL))); 
    }

    /*
     * Check the result.
     */
    if(counter == 4U)
    {
        status = RT_TOOLS_MODULE_INIT_SUCCESS;
    }

    RTLOG_INFO(modName, "%s done. (%s)", testName,
        (status == RT_TOOLS_MODULE_INIT_SUCCESS ? "SUCCESSFUL" : "FAILED"));

   return status;
}

/*
 * Test RTGUARD in RTGUARD_MODE_TIMER mode where
 * one cycle is suppressed due to the initially disabled guard.
 */
static int test12(void)
{
    const char* testName = "test12";
    rtlogRecord_t logRecord;
    int status = RT_TOOLS_MODULE_INIT_ERROR;
    unsigned int loop , counter = 0U;

    RTGUARD(guard, 10, 0.5, RTGUARD_MODE_BOTH);
    rtGuard_disable(&guard);

    for(loop = 1U; loop <= 21U; ++loop)
    {
        if(rtGuard_check(&guard) == 1)
        {
            ++counter;
            RTLOG_INFO(
                modName, "%s: This message should be printed four times every "
                "20 cycles (current cycle = %u, printed %u times)",
                testName, loop, counter);
        }

        rt_sleep(nano2count((RTIME)(sleepTime * 1000000000LL))); 
    }

    /*
     * Check the result.
     */
    if(counter == 4U)
    {
        status = RT_TOOLS_MODULE_INIT_SUCCESS;
    }

    RTLOG_INFO(modName, "%s done. (%s)", testName,
        (status == RT_TOOLS_MODULE_INIT_SUCCESS ? "SUCCESSFUL" : "FAILED"));

    return status;
}

static void runThread(long junk)
{
    rtlogRecord_t logRecord;
    int status = RT_TOOLS_MODULE_INIT_SUCCESS;
    int success = RT_TOOLS_MODULE_INIT_SUCCESS;

	RTLOG_INFO(modName, "Test task running...");

	success = test1();
	if(success == RT_TOOLS_MODULE_INIT_ERROR)
	{
	    status = success;
	}

	success = test2();
    if(success == RT_TOOLS_MODULE_INIT_ERROR)
    {
        status = success;
    }

    success = test3();
    if(success == RT_TOOLS_MODULE_INIT_ERROR)
    {
        status = success;
    }

    success = test4();
    if(success == RT_TOOLS_MODULE_INIT_ERROR)
    {
        status = success;
    }

    success = test5();
    if(success == RT_TOOLS_MODULE_INIT_ERROR)
    {
        status = success;
    }

    success = test6();
    if(success == RT_TOOLS_MODULE_INIT_ERROR)
    {
        status = success;
    }

    success = test7();
    if(success == RT_TOOLS_MODULE_INIT_ERROR)
    {
        status = success;
    }

    success = test8();
    if(success == RT_TOOLS_MODULE_INIT_ERROR)
    {
        status = success;
    }

    success = test9();
    if(success == RT_TOOLS_MODULE_INIT_ERROR)
    {
        status = success;
    }

    success = test10();
    if(success == RT_TOOLS_MODULE_INIT_ERROR)
    {
        status = success;
    }

    success = test11();
    if(success == RT_TOOLS_MODULE_INIT_ERROR)
    {
        status = success;
    }

    success = test12();
    if(success == RT_TOOLS_MODULE_INIT_ERROR)
    {
        status = success;
    }

    /*
	 * Done.
	 */
	RTLOG_INFO(modName, "rtGuard test %s.", (status == RT_TOOLS_MODULE_INIT_SUCCESS ? "SUCCESSFUL" : "FAILED"));
	
	atomic_set(&finished, 0);

	rt_task_delete(rt_whoami());
}

static int __init rtGuardTest_init(void)
{
	rtlogRecord_t logRecord;
	int stat = RT_TOOLS_MODULE_INIT_ERROR;

 	RTLOG_INFO(modName, "Initializing test for real time kernel guards...");

	/**
	 * Initialise the test task.
	 */
	if(rt_task_init(&taskID, runThread, 0,
	    RT_TOOLS_INIT_TASK_STACK, RT_TOOLS_INIT_TASK_PRIO, 0, 0) != 0)
	{
		RTLOG_ERROR(modName, "Could not start the test task!");
	}
	else
	{
		/**
		 * And then start it,
		 */
		rt_task_resume(&taskID);

		stat = RT_TOOLS_MODULE_INIT_SUCCESS;

		RTLOG_INFO(modName, "Module initialized successfully, test is running.");
	}

	return stat;
}

static void __exit rtGuardTest_exit(void)
{
	rtlogRecord_t logRecord;

	if(atomic_read(&finished) != 0)
	{
		rt_task_suspend(&taskID);
		rt_task_delete(&taskID);
	}

	RTLOG_INFO(modName, "Module cleaned up successfully");
}

module_init(rtGuardTest_init);
module_exit(rtGuardTest_exit);
