/*******************************************************************************
*
* "@(#) $Id$"
*
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* who       when        what
* --------  ----------  ----------------------------------------------
* tjuerges  2006-03-20  created
*/

/************************************************************************
*   NAME
*
*   SYNOPSIS
*
*   DESCRIPTION
*
*   FILES
*
*   ENVIRONMENT
*
*   RETURN VALUES
*
*   CAUTIONS
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS
*
*------------------------------------------------------------------------
*/

/*
 * System stuff
 */
#include <linux/moduleparam.h>
#include <linux/delay.h>
#include <asm/atomic.h>

/*
 * RTAI stuff
 */
#include <rtai_sched.h>
#include <rtai_registry.h>
#include <rtai_sem.h>
#include <rtai_fifos.h>

/*
 * ACS stuff
 */
#include <rtTools.h>
#include <rtLog.h>

/*
 * Local stuff
 */
#include "rtToolsTest.h"


MODULE_AUTHOR("Thomas Juerges <tjuerges@nrao.edu>");
MODULE_DESCRIPTION("rtToolsTest_k");
MODULE_LICENSE("GPL");

/*
 * used for logging
 */
#define modName	"rtToolsTest_k"

/*
 * module parameters
 */
static unsigned int initTimeout = 1000;         /* msec */
static unsigned int cleanUpTimeout = 1000;      /* msec */
module_param(initTimeout, uint, S_IRUGO);
module_param(cleanUpTimeout, uint, S_IRUGO);

/*
 * my local data
 */
RT_TASK llFloorTaskVar;

/*
 * API
 */
static int rtToolsFifoCmdTestFifoHandler(unsigned int fifo, int rw)
{
	rtlogRecord_t logRecord;

	int stat = -1;
	int cmdSize = -1;
	int size = sizeof(my_message_t) + sizeof(int);
	unsigned char cmd[size];
	void* foo;
	my_message_t* bar;
	my_message_t reply;

	if(rw == 'w')
	{
		cmdSize = rtf_get(fifoNumber, (void*)(&cmd), size);

		if(cmdSize != size)
		{
                    RTLOG_ERROR(modName,
				"Cannot get buffer from fifo. Wrong size (%d). Should be %d", cmdSize, size);
			return 1;
		}

		stat = rtf_sem_trywait(fifoNumber);
		if(stat != 0)
		{
			RTLOG_ERROR(modName, "Failed to take access semaphore in fifo write (stat=%d)!", stat);
			return 1;
		}

		/**
		 * Get rid of `command` which is an int.
		 */
		foo = cmd + sizeof(int);
		bar = (my_message_t*)(foo);
		/**
		 * The reply is an exact copy of the data which came in without the command.
		 */
		reply = (*bar);

		stat = rtf_put_if(fifoNumber, &reply, sizeof(reply));
		if(stat != sizeof(my_message_t))
		{
			RTLOG_ERROR(modName,
				"Cannot write reply to fifo (stat=%d, size=%d)",
				stat, sizeof(reply));
		}

		rtf_sem_post(fifoNumber);
	}

	return 0;
}
/*
 * exported API
 */
EXPORT_SYMBOL(rtToolsFifoCmdTestFifoHandler);


static void llFloorTask(long not_used)
{
    int i, div = 48; /* sort of typical divisor, 48ms */
    rtlogRecord_t logRecord;
    RTIME res, expected;

    /*
     * test for negative and positive numbers
     */
    for ( i = -(div * 2) + 1; i < (div * 2); i++ )
    {        
        res = rtToolsLlFloor(i, div);

        /*
         * expected value
         */
        if ( i < -div )
        { 
            expected = -(div * 2);
        }
        else if ( i >= -div && i < 0 )
        {
            expected = -div;
        }
        else if ( i >= 0 && i < div )
        {
            expected = 0;
        }
        else
        {
            expected = div;
        }

        if ( res != expected )
        {
            RTLOG_ERROR(modName, "Problem with rtToolsLlFloor for value = %d "
                "found: result of rtToolsLlFloor = %lld, expected = %lld",
                i,
                res,
                expected);
            
            return;
        }
        
        rt_sleep(nano2count(100000));   /* 100us */
    }

    RTLOG_INFO(modName, "rtToolsLlFloor PASSED OKAY.");
}

static int firstInitSection(void)
{
	rtlogRecord_t logRecord;
	int fifoStat = -1;

	if((fifoStat = rtf_create(fifoNumber, sizeof(my_message_t) * 2)) < 0)
	{
		RTLOG_ERROR(modName, "Cannot create fifo (err=%d).", fifoStat);
		return RT_TOOLS_MODULE_INIT_ERROR;
	}

	if((fifoStat = rtf_reset(fifoNumber)))
	{
		RTLOG_ERROR(modName, "failed to reset the input fifo (err=%d).", fifoStat);
		return RT_TOOLS_MODULE_INIT_ERROR;
	}

	/*
	 * hook a handler function to the fifo
	 */
	if((fifoStat = rtf_create_handler(fifoNumber,
		X_FIFO_HANDLER(rtToolsFifoCmdTestFifoHandler))))
	{
		RTLOG_ERROR(modName, "Cannot hook a handler function to the fifo (err=%d).", fifoStat);
		return RT_TOOLS_MODULE_INIT_ERROR;
	}

	return RT_TOOLS_MODULE_INIT_SUCCESS;
}

static int secondInitSection(void)
{
	rtf_sem_init(fifoNumber, 1);
	return RT_TOOLS_MODULE_INIT_SUCCESS;
}


static int firstCleanUpSection(void)
{
	rtf_destroy(fifoNumber);
	return RT_TOOLS_MODULE_EXIT_SUCCESS;
}

static int secondCleanUpSection(void)
{
	rtf_sem_destroy(fifoNumber);
	return RT_TOOLS_MODULE_EXIT_SUCCESS;
}


static int rtToolsTest_main(int stage)
{
	int status = RT_TOOLS_MODULE_INIT_ERROR;
	int temporaryStatus = RT_TOOLS_MODULE_INIT_ERROR;
	rtlogRecord_t logRecord;

	if(stage == RT_TOOLS_MODULE_STAGE_INIT)
	{
		/*
		 * log RCS ID (cvs version)
		 */
		RTLOG_INFO(modName, "$Id$");

		RTLOG_INFO(modName, "initializing module...");

		RTLOG_INFO(modName, "initTimeout    = %dms", initTimeout);
		RTLOG_INFO(modName, "cleanUpTimeout = %dms", cleanUpTimeout);

		goto Initialization;
	}
	else
	{
		RTLOG_INFO(modName, "cleaning up module...");

		goto FullCleanUp;
	}

Initialization:

	if((status = firstInitSection()) != RT_TOOLS_MODULE_INIT_SUCCESS)
	{
		goto Level1CleanUp;
	}

	if((status = secondInitSection()) != RT_TOOLS_MODULE_INIT_SUCCESS)
	{
		goto Level2CleanUp;
	}

        /*
         * spawn the main task
         */
        if ( rt_task_init(&llFloorTaskVar, llFloorTask, 0, 8000, 10, 1, 0)
             != 0 )
        {
            RTLOG_INFO(modName, "CANNOT SPAWN THE TEST TASK!");
            
            goto Level2CleanUp;
        }
    
        /*
         * set the just created task as 'ready to run'
         */
        rt_task_resume(&llFloorTaskVar);

	goto Exit;

FullCleanUp:
        /*
         * get rid of spawned task
         */
        rt_task_delete(&llFloorTaskVar);
           
Level2CleanUp:
	temporaryStatus = secondCleanUpSection();
	if(temporaryStatus != RT_TOOLS_MODULE_EXIT_SUCCESS)
	{
		if(stage == RT_TOOLS_MODULE_STAGE_EXIT)
		{
			status = temporaryStatus;
		}
	}

Level1CleanUp:
	temporaryStatus = firstCleanUpSection();
	if(stage == RT_TOOLS_MODULE_STAGE_EXIT)
	{
		status = temporaryStatus;
	}

Exit:
	return status;
}


static int __init rtToolsTest_init(void)
{
	int status;
	rtlogRecord_t logRecord;

	if((status = rtToolsTest_main(RT_TOOLS_MODULE_STAGE_INIT))
	== RT_TOOLS_MODULE_INIT_SUCCESS )
	{
		RTLOG_INFO(modName, "module initialized successfully");
	}
	else
	{
		RTLOG_ERROR(modName, "failed to initialize module");
		return status;
	}

	RTLOG_INFO(modName, "Running rtTools test...");

	return status;
}

static void __exit rtToolsTest_exit(void)
{
	rtlogRecord_t logRecord;

	if(rtToolsTest_main(RT_TOOLS_MODULE_STAGE_EXIT)
	== RT_TOOLS_MODULE_EXIT_SUCCESS)
	{
		RTLOG_INFO(modName, "module cleaned up successfully");
	}
	else
	{
		RTLOG_ERROR(modName, "failed to clean-up module");
	}
}

module_init(rtToolsTest_init);
module_exit(rtToolsTest_exit);
