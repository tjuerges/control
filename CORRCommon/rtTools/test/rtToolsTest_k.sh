#! /bin/bash
#
# "@(#) $Id$"
#

max_iter=15 # maximal number of iterations (each 1second) == sleep 15

result=0
i=0

#
# check that the ll-floor part passed successfully
#
while [ $result -ne 0 -a $i -lt $max_iter ]; do
	sleep 1
	let i++
        /bin/dmesg -s 1 | grep -e "rtToolsTest_k.*rtToolsLlFloor PASSED OKAY" > /dev/null
	result=$?
done

if [ $result -eq 0 ]; then
	echo "`basename $0`: kernel module test passed."
else
	echo "`basename $0`: kernel module test failed. No result after $max_iter sec."
fi

testRtFindKernelModules > /dev/null
result=$?
if [ $result -eq 0 ]; then
	echo "`basename $0`: testRtFindKernelModules test passed."
else
	echo "`basename $0`: testRtFindKernelModules test failed."
fi

rtToolsFifoCmdTest > /dev/null
result=$?
if [ $result -eq 0 ]; then
	echo "`basename $0`: rtToolsFifoCmdTest test passed."
else
	echo "`basename $0`: rtToolsFifoCmdTest test failed."
fi
