#! /bin/bash
#
# "@(#) $Id$"
#

max_iter=40 # maximal number of iterations (each 1second)

result=1
i=0

while [ $result -ne 0 -a $i -lt $max_iter ]; do
	sleep 1
	let i++
	/bin/dmesg | /usr/bin/tail -n 10 | /bin/grep -c " test SUCCESSFUL." > /dev/null
	result=$?
done

if [ $result -eq 0 ]; then
	echo "`basename $0`: kernel module test passed."
else
	echo "`basename $0`: kernel module test failed. No result after $max_iter sec."
fi
