/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2004
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* ...
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2004-07-27  created
*
* $Id$
*/

/************************************************************************
*   NAME
*    rtTool
*   SYNOPSIS
*     This is the first kernel module to load it defines functions which are
*    used in more than one place and starts/stops the real time timer.
*------------------------------------------------------------------------
*/

/*
 * System Headers 
 */
#include <linux/version.h>
#include <linux/module.h>
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,19)
#include <linux/kprobes.h>
#else
#endif
#include <linux/kallsyms.h>
#include <linux/string.h>
#include <linux/delay.h>

/*
 * RTAI Headers
 */
#include <rtai.h>
#include <rtai_sched.h>

/*
 * ACS stuff
 */
#include <rtLog.h>

#define moduleName "rtTools"

MODULE_AUTHOR("Jeff Kern <jkern@nrao.edu>, "
    "Rodrigo Amestica <ramestic@nrao.edu>, "
    "Thomas Juerges <tjuerges@nrao.edu>");
MODULE_DESCRIPTION(moduleName ": ALMA Real Time Tools");
MODULE_LICENSE("GPL");

/*
 * Local Headers 
 */
#include "rtTools.h"

/*
 * Exported Functions
 */
EXPORT_SYMBOL(rtToolsWaitOnFlag);
EXPORT_SYMBOL(rtToolsLookupSymbol);
EXPORT_SYMBOL(rtToolsLlFloor);
EXPORT_SYMBOL(rtGuard_check);
EXPORT_SYMBOL(rtGuard_disable);


int rtToolsWaitOnFlag(const atomic_t *flag, const unsigned int to)
{
    unsigned int count = 0;

    while ( atomic_read(flag) != 1 && ++count < to )
    {
        mdelay(1);
    }

    /*
     * on timeout return with error
     */
    if ( count == to )
    {
        return 1;
    }

    return 0;
}

#ifdef FOO_BAR
//#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,19)
/*
 * The kprobes mechanism which we use since kernel 2.6.19 to lookup symbol
 * addresses needs a function which has to be registered. Since this
 * registration is only temporarily, it can be empty.
 */
static int dummyHandlerForkernelProbe(
    struct kprobe* kpr __attribute__((unused)),
    struct pt_regs* p __attribute__((unused)))
{
    return 0;
}
#endif

void* rtToolsLookupSymbol(const char* module, const char* symbol)
{
    /*
     * KSYM_NAME_LEN is defined in linix/kallsyms.h and defines the kernel's
     * maximum size of a symbol.
     */
	char completeName[KSYM_NAME_LEN + 1];
#ifdef FOO_BAR
//#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,19)
    /*
     * Necessary for the symbol name lookup from kernel 2.6.19 on.
     */
    struct kprobe kernelProbe;
    int ret = 0;
#endif
	const size_t size = strlen(module) + strlen(symbol) + 1;
	void* addr = 0;
	rtlogRecord_t logRecord;

	/*
	 * The full symbol name (mod+sym) must fit in the moduleName variable
	 */
	if(size > KSYM_NAME_LEN)
	{
	    RTLOG_ERROR(moduleName, "Symbol name is too long (length = %d)!",
            size - 1);
	    return addr;
	}

	/*
	 * Look for 'module:symbol'
	 */
	memset(completeName, 0, sizeof(completeName));
	strcpy(completeName, module);
#ifndef FOO_BAR
//#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,19)
	completeName[strlen(module)] = ':';
	strcpy(completeName + strlen(module) + 1, symbol);
//    strcpy(completeName, symbol);
#endif

#ifdef FOO_BAR
//#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,19)
    memset(&kernelProbe, 0, sizeof(struct kprobe));
    kernelProbe.symbol_name = completeName;
    kernelProbe.pre_handler = dummyHandlerForkernelProbe;

    /*
     * The kernel does a nice thing when a kprobe is registered with a
     * name:  it performs a kallsyms_lookup_name and tries to find the
     * symbol address.  If it is successful, the symbol address from the
     * kernel symbol table is written to kernelProbe.addr.  Voila!  We
     * got the address of the symbol and can now unregister the probe again.
     */
    ret = register_kprobe(&kernelProbe);
    if(ret < 0)
    {
        RTLOG_ERROR(moduleName, "Symbol not found in kernel. "
            "register_kprobe returned %d for the symbol %s.\n",
            ret, completeName);
    }
    else
    {
        addr = (void*)kernelProbe.addr;
        RTLOG_DEBUG(moduleName, "Symbol %s found in kernel at 0x%p.\n",
            completeName, addr);
        unregister_kprobe(&kernelProbe);
    }
#else
    addr = (void*)kallsyms_lookup_name(completeName);
#endif

	return addr;
}

/*
 * llimd by itself rounds either to the lower or upper boundary. llfloor will
 * always round to the lower boundary.
 */
RTIME rtToolsLlFloor(RTIME val, int div)
{
	RTIME res;

        if ( val >= 0 )
        {
            res = llimd(val, 1, div) * div;
        }
        else
        {
            res = -llimd(-val, 1, div) * div;
        }

	if ( res > val )
	{
	    return res - div;
	}

	return res;
}

unsigned int rtGuard_check(rtToolsRepeatGuard_s* guard)
{
    unsigned char doLog = 0U;
    const RTIME timeStamp = rt_get_time_ns();

    guard->timeStamp =  timeStamp - guard->initialTimeStamp;
    ++(guard->counter);

    /*
     * Check if at least one of the conditions is true.
     */
    if(guard->counter == guard->maxCounter)
    {
        guard->counter = 0ULL;
        doLog |= RTGUARD_MODE_COUNTER;
    }

    if(guard->timeStamp >= guard->minTime)
    {
        guard->initialTimeStamp = timeStamp;
        doLog |= RTGUARD_MODE_TIMER;
    }

    if((doLog & guard->mode) != 0U)
    {
        if(guard->isGuardOn != 1U)
        {
            guard->isGuardOn = 1U;
        }
        else
        {
            return 1;
        }
    }

    return 0;
}

void rtGuard_disable(rtToolsRepeatGuard_s* guard)
{
    guard->isGuardOn = 0U;
}

/*
 * Init and cleanup fuctions
 */
static int __init init_rtTools(void)
{
    rtlogRecord_t logRecord; // For the logging channel

    /*
     * Start the real time timer.
     */
    rt_set_oneshot_mode();
    start_rt_timer_ns(0);

    RTLOG_INFO(moduleName, "$Id$");
    RTLOG_INFO(moduleName, "Real Time Tools module loaded");
    return 0;
}

static void __exit exit_rtTools(void)
{
    rtlogRecord_t logRecord; // For the logging channel
    stop_rt_timer();
    RTLOG_INFO(moduleName, "Real Time Tools module unloaded");
}

module_init(init_rtTools);
module_exit(exit_rtTools);
