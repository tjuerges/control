#!/bin/bash

if [ "x$1" == "x" ] ; then
    echo "i'm missing an lkm file name!"
    exit
fi

LKM=$1

trap "unloadLkmModule $LKM; exit" SIGINT SIGTERM

i=0
while true
do
echo $i
top -b -n 1|awk  '/Mem:/ {print strftime() $2 " " $4 " " $6}' | sed -e 's/k//g'
loadLkmModule $LKM
unloadLkmModule $LKM
i=`expr $i + 1`
done
