/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2004 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* ...
*
* who       when        what
* --------  ----------  ----------------------------------------------
* ramestic  2004-11-01  created
*/

/************************************************************************
*   NAME
*	rtToolsFifoCmd - 
*
*   SYNOPSIS
*
*   DESCRIPTION
*
*   FILES
*
*   ENVIRONMENT
*
*   COMMANDS
*
*   RETURN VALUES
*
*   CAUTIONS 
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS   
* 
*------------------------------------------------------------------------
*/

//
// Local Headers 
//
#include "rtToolsFifoCmd.h"

//
// RTAI Headers (malloc comes from stdlib, and that one is included by
// rtToolsFifoCmd.h, therfore, we need to include rtai_fifos.h after the
// former one. One can say that this is an rtai bug.
//
#include <rtai_fifos.h>

using namespace std;

rtToolsFifoCmd::rtToolsFifoCmd(const char *name)
{
    //
    // open fifo
    //
    fd = open(name, O_RDWR);
    
    //
    // record that we were not able to open the fifo with our own err code
    //
    if ( fd < 0 )
    {
	fd = -RT_TOOLS_CMD_OPEN_ERR;
    }
}

rtToolsFifoCmd::~rtToolsFifoCmd()
{
    //
    // close the fifo
    //
    if ( fd > 0 )
    {
	close(fd);
    }
}

void rtToolsFifoCmd::sendCmd(void *cmd, int len, int to)
{
    //
    // check object status
    //
    if ( fd == -RT_TOOLS_CMD_OPEN_ERR )
    {
	throw RT_TOOLS_CMD_OPEN_ERR;
    }

    //
    // synchronize with other clients
    //
    if ( rtf_sem_timed_wait(fd, to) != 0 )
    {
	throw RT_TOOLS_CMD_SEM_TIMEOUT_ERR;
    }
    
    //
    // reset the fifo for getting rid of whatever was actually there
    //
    rtf_reset(fd);

    //
    // write command into fifo
    //
    if ( rtf_write_if(fd, static_cast<char *>(cmd), len) != len )
    {
        //
        // give back the semaphore before throwing the exception
        //
	rtf_sem_post(fd);

	throw RT_TOOLS_CMD_WRITE_ERR;
    }
}

void rtToolsFifoCmd::recvCmd(void *replay, int len, int to)
{
    int stat;

    //
    // check object status
    //
    if ( fd == -RT_TOOLS_CMD_OPEN_ERR )
    {
	throw RT_TOOLS_CMD_OPEN_ERR;
    }

    //
    // wait for cmd reply
    //
    stat = rtf_read_timed(fd, static_cast<char *>(replay), len, to);

    //
    // now check return status, if negative then we have a read error, if
    // the received count is not what we expected then we have a timeout
    //
    if ( stat < 0 )
    {
        throw RT_TOOLS_CMD_READ_ERR;
    }
    if ( stat != len )
    {
	throw RT_TOOLS_CMD_TIMEOUT_ERR;
    }  

    //
    // give back the synchronization semaphore, originally taken from
    // within sendCmd
    //
    if ( rtf_sem_post(fd) != 0 )
    {
	throw RT_TOOLS_CMD_SEM_POST_ERR;
    }  
}

void rtToolsFifoCmd::sendRecvCmd(void *cmd, int cmdlen,
				 void *reply, int replylen, 
				 int to)
{
    int sendCmdTiming; /* ms */
    struct timeval tv0, tv1;

    //
    // check object status
    //
    if ( fd == -RT_TOOLS_CMD_OPEN_ERR )
    {
	throw RT_TOOLS_CMD_OPEN_ERR;
    }
    
    //
    // get current time
    //
    gettimeofday(&tv0, 0);

    //
    // send cmd
    //
    try
    {
	sendCmd(cmd, cmdlen, to);
    }
    catch(rtToolsFifoCmdErr_t &e)
    {
	throw e;
    }

    //
    // get current time again 
    //
    gettimeofday(&tv1, 0);

    //
    // compute the timing of sendCmd
    //
    sendCmdTiming =
        (tv1.tv_sec - tv0.tv_sec) * 1000 + (tv1.tv_usec - tv0.tv_usec) / 1000;

    //
    // if sendCmd consumed all the timeout already then trigger the exception
    //
    if ( sendCmdTiming >= to )
    {
        //
        // give back the semaphore before exiting
        //
        rtf_sem_post(fd);

        throw RT_TOOLS_CMD_TIMEOUT_ERR;
    }

    //
    // wait for reply
    //
    try
    {
	recvCmd(reply, replylen, to - sendCmdTiming);
    }
    catch(rtToolsFifoCmdErr_t &e)
    {
	throw e;
    }
}

/*___oOo___*/
