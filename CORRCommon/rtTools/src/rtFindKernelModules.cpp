/* @(#) $Id$
 *
 * Copyright (C) 2006
 * Associated Universities, Inc. Washington DC, USA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * Correspondence concerning ALMA Software should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */

#include <string>
#include <logging.h>

using std::string;
using std::vector;


bool findKernelModules( const vector<string> kernelModules )
{
    bool bFoundModule;
    int numModulesFound = 0;
    char line[1024], *pCharsRead;
    // a temporary string which encloses the module name in square brackets
    // as found in kallsyms.
    string kernelModuleStr;    

    // Make sure that we can open the proc file with all the kernel symbols
    FILE *pf = fopen("/proc/kallsyms","r");
    if( pf == 0 )
    {
	ACS_SHORT_LOG((LM_ERROR,"Can't open /proc/kallsyms file!"));
	return false;
    }

    // Search kallsyms for each of the expected kernel modules
    for( vector<string>::const_iterator strIter = kernelModules.begin();
	 strIter != kernelModules.end(); ++strIter)
    {
	// Start from the beginning of the file. Since these are RAM files,
	// rewinding is fast
	fseek(pf, 0L, SEEK_SET);
	bFoundModule = false;

	kernelModuleStr = string("[") + *strIter + string("]");
	// check each line for the requested kernel name
	do
	{
	    pCharsRead = fgets(&line[0], 1023, pf);
	    if( pCharsRead )
	    {
		// if this line contains the kernel name, break out & go to the
		// next module name
		if( strstr(line, kernelModuleStr.c_str()) != 0 )
		{
		    bFoundModule = true;
		    break;
		}
	    }
	}while( pCharsRead );

	// Increment the count of found modules
	if( bFoundModule )
	    numModulesFound++;
	else   // log that the module wasn't found
	    ACS_SHORT_LOG((LM_ERROR,"Module: %s not loaded",kernelModuleStr.c_str()));
    }
    fclose(pf);

    // return true if we found the expected number of modules
    return (numModulesFound == (int)kernelModules.size());
}
