/*
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2011
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * $Id$
 * $Source$
 *
 * who      when     what
 * tjuerges  Jan 12, 2011  created
 */


/*
 * System stuff
 */
#include <linux/moduleparam.h>
#include <linux/proc_fs.h>
#include <linux/delay.h>
#include <asm/atomic.h>

/*
 * RTAI stuff
 */
#include <rtai_sched.h>
#include <rtai_registry.h>
#include <rtai_sem.h>

/*
 * ACS stuff
 */
#include <rtTools.h>
#include <rtLog.h>

/*
 * Local stuff
 */
/*
#include "kernelModuleTemplatePrivate.h"
*/


/*
 * Used for RTOS logging
 */
#define moduleName "kernelModuleTemplate"


MODULE_AUTHOR("pippo <pippo@foo.bar>");
MODULE_DESCRIPTION(moduleName ":  This is the description for this kernel "
"module.  This module does the following:\n"
"- Blah.\n"
"- Blah.");
MODULE_LICENSE("GPL");

/*
 * Module parameters
 */
static unsigned int initTimeout = 1000U;
module_param(initTimeout, uint, S_IRUGO);
MODULE_PARM_DESC(initTimeout, "Time out in [ms] for the real time "
    "initialisation task. If the init taks does not finish in time, the "
    "kernel module will consider the real time initialisation failed and "
    "return an error.");

static unsigned int cleanUpTimeout = 1000U;
module_param(cleanUpTimeout, uint, S_IRUGO);
MODULE_PARM_DESC(cleanUpTimeout, "Time out in [ms] for the real time clean-up "
    "task. If the clean-up taks does not finish in time, the kernel module "
    "will consider the real time system being \"dirty\" and log an error "
    "message.");

static char softwareVersion[] =
    "$Id$";

/**
 * Data structure for the /proc page.
 */
static struct proc_dir_entry* procEntry = 0;


/*
 * Local data
 */
/* API must be reentrant, protect it with this semaphore */
static SEM access_sem;


/*
 * Called whenever somebody accesses /proc/moduleName.  This function will then
 * print informtion out to the console.  Since everything is generated at that
 * moment, this comes very handy for keeping an eye on status information or
 * debugging information that is not often used.  The call will be in parallel
 * to the normal operation of the kernel module.  So make certain that access
 * to variables that get modified in this kernel module is ataomic.
 */
static int readProcFsPage(char* buffer, char** start, off_t offset, int length,
    int* eof, void* data)
{
    int size = 0;

    /*
     * Every printed out line has to increase the variable size by the number
     * of bytes that have been printed out.
     */
    size += sprintf(buffer + size, "%s kernel module information page\n\n"
        "Software version = %s\n\n", moduleName, softwareVersion);

    return size;
}


/*
 * API.
 */
static void kernelModuleTemplateFun1(void)
{
}
static void kernelModuleTemplateFunN(void)
{
}

/*
 * Exported API.
 */
EXPORT_SYMBOL (kernelModuleTemplateFun1);
EXPORT_SYMBOL (kernelModuleTemplateFunN);

/*
 * Intialisation task.
 */
void initTaskFunction(long flag)
{
    int status = 0;
    atomic_t* flag_p = (atomic_t*)flag;
    rtlogRecord_t logRecord;

    RTLOG_INFO(moduleName, "initialisation task started...");

    /*
     * Do the real work here.  If it fails do not set status and go to TheEnd.
     */

    rt_sleep(nano2count(100000000LL));

    status = 1;

TheEnd:
    RTLOG_INFO(moduleName, "initialisation task exit.");

    atomic_set(flag_p, status);

    rt_task_delete(rt_whoami());
}

/*
 * Clean up task.
 */
static void cleanUpTaskFunction(long flag)
{
    int status = 0;
    atomic_t* flag_p = (atomic_t*)flag;
    rtlogRecord_t logRecord;

    RTLOG_INFO(moduleName, "Clean up task started...");

    /*
     * Do the real work here.  If it fails do not set status and go to TheEnd.
     */
    rt_sleep(nano2count(100000000LL));

    status = 1;

TheEnd:
    RTLOG_INFO(moduleName, "Clean up task exit.");

    atomic_set(flag_p, status);

    rt_task_delete(rt_whoami());
}

static int firstInitSection(void)
{
    return RT_TOOLS_MODULE_INIT_SUCCESS;
}
static int nthInitSection(void)
{
    return RT_TOOLS_MODULE_INIT_SUCCESS;
}

static int firstCleanUpSection(void)
{
    return RT_TOOLS_MODULE_EXIT_SUCCESS;
}
static int nthCleanUpSection(void)
{
    return RT_TOOLS_MODULE_EXIT_SUCCESS;
}

static int kernelModuleTemplate_main(int stage)
{
    int status = 0;
    int temporaryStatus = RT_TOOLS_MODULE_CLEANUP_ERROR;
    int cleanUpStatus = RT_TOOLS_MODULE_EXIT_SUCCESS;
    RT_TASK initTask, cleanUpTask;
    atomic_t initFlag = ATOMIC_INIT(0);
    atomic_t cleanUpFlag = ATOMIC_INIT(0);
    rtlogRecord_t logRecord;

    if(stage == RT_TOOLS_MODULE_STAGE_INIT)
    {
        status = RT_TOOLS_MODULE_INIT_ERROR;

        /* Log CVS version and other configuration stuff. */
        RTLOG_INFO(moduleName, "%s", softwareVersion);
        RTLOG_INFO(moduleName, "Initialising module...");
        RTLOG_INFO(moduleName, "initTimeout = %d[ms]", initTimeout);
        RTLOG_INFO(moduleName, "cleanUpTimeout = %d[ms]", cleanUpTimeout);

        goto initialisation;
    }
    else
    {
        status = RT_TOOLS_MODULE_EXIT_SUCCESS;

        RTLOG_INFO(moduleName, "Cleaning up module...");

        goto FullCleanUp;
    }

initialisation:
    /**
     * ATTENTION!!!!!
     * Whenever firstInitSection() fails, take care of jumping to the correct
     * clean-up level. It must not necessarily be that LevelNCleanUp is the
     * clean-up level to jump to after nthInitSection() failed, but (here in
     * this template) Level1CleanUp.
     *
     * The reason for it could be:
     * - The first init level screws up things totally. Then a jump to the first
     *   clean-up level would let the module try to clean up the mess, which is
     *   not possible anymore. A stuck computer could be the result.
     *
     * The right way to do it:
     * The Nth init level fails at a certain point. Then
     * - The Nth init level cleans up everything which was successfully
     *   initialised in this level.
     * - The Nth init level returns to this place with
     *   RT_TOOLS_MODULE_INIT_ERROR.
     * - Here, the code jumps to the higher (one level higher, counting up from
     *   level N to level 1 as the highest) clean-up level. This would be in
     *   this template
     */
    if((status = firstInitSection()) != RT_TOOLS_MODULE_INIT_SUCCESS)
    {
        goto Exit;
    }

    /* ... more initialisation steps ... */

    if((status = nthInitSection()) != RT_TOOLS_MODULE_INIT_SUCCESS)
    {
        goto Level1CleanUp;
    }

    /*
     * Initialisation of resources is finished. Now spawn the rtai
     * task that takes care of doing the asynchronous part of the
     * initialisation.
     */
    if(rt_task_init(&initTask,
        initTaskFunction,
        (long)&initFlag,
        RT_TOOLS_INIT_TASK_STACK,
        RT_TOOLS_INIT_TASK_PRIO,
        0, 0)
    != 0)
    {
        RTLOG_ERROR(moduleName, "Failed to spawn initialisation task!");

        status = RT_TOOLS_MODULE_NO_INIT_TASK_ERROR;

        goto LevelNCleanUp;
    }
    else
    {
        RTLOG_INFO(moduleName, "Created initialisation task, address = 0x%p.",
            &initTask);
    }

    /*
     * Resume the initialisation task.
     */
    if(rt_task_resume(&initTask) != 0)
    {
        rt_task_delete(&initTask);
        status = RT_TOOLS_MODULE_NO_INIT_TASK_ERROR;

        RTLOG_ERROR(moduleName, "Cannot resume initalisation task!");

        goto LevelNCleanUp;
    }

    /*
     * Wait for the initialisation task to be ready.
     */
    if(rtToolsWaitOnFlag(&initFlag, initTimeout) != 0)
    {
        /*
         * Okay, the init task was not ready on time, now forcibly remove
         * it for the RTAI's scheduler.
         */
        rt_task_suspend(&initTask);
        rt_task_delete(&initTask);

        RTLOG_ERROR(moduleName, "initialisation task did not report back on "
            "time!");

        status = RT_TOOLS_MODULE_INIT_TIMEOUT;

        goto FullCleanUp;
    }
    else
    {
        /*
         * Waited successfully for the init task to run through.
         */
        status = RT_TOOLS_MODULE_INIT_SUCCESS;
    }

    procEntry = create_proc_entry(moduleName, S_IRUGO | S_IWUSR, NULL);
    if(procEntry == 0)
    {
        RTLOG_DEBUG(moduleName, "Failed to register the proc FS entry.");
    }
    else
    {
        procEntry->read_proc = readProcFsPage;
    }

    goto Exit;

FullCleanUp:
    remove_proc_entry(moduleName, NULL);

    /*
     * Asynchronous clean up phase to be handled by this task.
     */
    if(rt_task_init(&cleanUpTask,
        cleanUpTaskFunction,
        (long)&cleanUpFlag,
        RT_TOOLS_CLEANUP_TASK_STACK,
        RT_TOOLS_CLEANUP_TASK_PRIO,
        0, 0)
    != 0)
    {
        RTLOG_ERROR(moduleName, "Failed to spawn clean up task!");

        /*
         * Set the status to this error only, if the stage is not the init
         * stage.
         * Whenever it happens, that something went wrong with the init task,
         * we are here, too. Then an error in the clean up is not of inerest,
         * because the init failed. This should then be reported.
         */
        cleanUpStatus = RT_TOOLS_MODULE_CLEANUP_ERROR;

        goto LevelNCleanUp;
    }
    else
    {
        RTLOG_INFO(moduleName, "Created clean up task, address = 0x%p.",
            &cleanUpTask);
    }

     /*
      * Resume the clean up task.
      */
    if(rt_task_resume(&cleanUpTask) != 0)
    {
        rt_task_delete(&cleanUpTask);
        cleanUpStatus = RT_TOOLS_MODULE_CLEANUP_ERROR;

        RTLOG_ERROR(moduleName, "Cannot resume clean up task!");

        goto LevelNCleanUp;
    }

     /*
      * Wait for the clean-up task to be ready.
      */
    if(rtToolsWaitOnFlag(&cleanUpFlag, cleanUpTimeout) != 0)
    {
        /*
         * Okay, the clean-up task was not ready on time, now forcibly remove
         * it for the RTAI's scheduler.
         */
        rt_task_suspend(&cleanUpTask);
        rt_task_delete(&cleanUpTask);

        RTLOG_ERROR(moduleName, "Clean up task did not report back on time!");

        if(stage != RT_TOOLS_MODULE_STAGE_INIT)
        {
            cleanUpStatus = RT_TOOLS_MODULE_CLEANUP_ERROR;
        }
    }
    else
    {
        if(stage != RT_TOOLS_MODULE_STAGE_INIT)
        {
            cleanUpStatus = RT_TOOLS_MODULE_EXIT_SUCCESS;
        }
     }

LevelNCleanUp:
    /*
     * Do synchronous clean up here. Set cleanUpStatus only if
     * (stage == RT_TOOLS_MODULE_STAGE_EXIT).
     *
     * For example:
     */
    temporaryStatus = nthCleanUpSection();
    if(temporaryStatus != RT_TOOLS_MODULE_EXIT_SUCCESS)
    {
        RTLOG_ERROR(moduleName, "Failed to run the nth clean up function! "
            "Continuing anyway with the clean up.");

        if(cleanUpStatus == RT_TOOLS_MODULE_EXIT_SUCCESS)
        {
            cleanUpStatus = temporaryStatus;
        }
    }

Level1CleanUp:
    temporaryStatus = firstCleanUpSection();
    if(temporaryStatus != RT_TOOLS_MODULE_EXIT_SUCCESS)
    {
        RTLOG_ERROR(moduleName, "Failed to run the 1st clean up function! "
            "Continuing anyway with the clean up.");

        if(cleanUpStatus == RT_TOOLS_MODULE_EXIT_SUCCESS)
        {
            cleanUpStatus = temporaryStatus;
        }
    }

Exit:
    if(stage != RT_TOOLS_MODULE_STAGE_INIT)
    {
        status = cleanUpStatus;
    }

    return status;
}

/*
 * Executed on "modprobe moduleName" or on "insmod /PATH/moduleName.ko".
 */
static int __init kernelModuleTemplate_init(void)
{
    int status;
    rtlogRecord_t logRecord;

    if((status = kernelModuleTemplate_main(RT_TOOLS_MODULE_STAGE_INIT))
        == RT_TOOLS_MODULE_INIT_SUCCESS)
    {
        RTLOG_INFO(moduleName, "Module initialized successfully.");
    }
    else
    {
        RTLOG_ERROR(moduleName, "Failed to initialize module!");
    }

    return status;
}

/*
 * Executed on "rmmod moduleName"
 */
static void __exit kernelModuleTemplate_exit(void)
{
    rtlogRecord_t logRecord;

    if(kernelModuleTemplate_main(RT_TOOLS_MODULE_STAGE_EXIT)
        == RT_TOOLS_MODULE_EXIT_SUCCESS)
    {
        RTLOG_INFO(moduleName, "Module cleaned up successfully.");
    }
    else
    {
        RTLOG_ERROR(moduleName, "Failed to clean up module!");
    }
}


/*
 * Required for the kernel to allocate space for the init and exit functions.
 */
module_init(kernelModuleTemplate_init);
module_exit(kernelModuleTemplate_exit);
