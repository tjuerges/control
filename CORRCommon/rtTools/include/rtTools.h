#ifndef RTTOOLS_H
#define RTTOOLS_H
/*******************************************************************************
* ALMA - Atacama Large Millimeter Array
* (c) Associated Universities Inc., 2004 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2004-07-27  created
*/

/************************************************************************
 *  In order to prevent conflicts all of the resources and priorities for
 * "main stream" (i.e. not test modules) are defined here.  Also functions
 * which are used in multiple places can be defined here as well.
 *----------------------------------------------------------------------
 */

/*
 * Needed for rt_fun_entry below (lxrt extensions)
 */
#include <rtai_lxrt.h>

/* The following piece of code alternates the linkage type to C for all
functions declared within the braces, which is necessary to use the
functions in C++-code.
*/

#ifdef __cplusplus
extern "C" {
#endif

/*
 * LXRT IDXes
 */
#define TE_HANDLER_IDX			(13)
#define TE_SCHED_IDX			(14)
#define hpdi32_IDX			(15)

/*
 * Here are the priorities for all of the "main stream" tasks
 */
#define TE_HANDLER_TASK_PRIO		(1)
#define TE_SCHED_ACTIVATION_TASK_PRIO	(2)
#define TE_SCHED_TASK_PRIO		(3)
#define SERVER_INIT_PRIORITY            (4)
#define TE_DIST_PRIORITY		(5)
#define TE_PRIORITY			(6)
#define DISPATCH_PRIORITY		(7)
#define ASAP_PRIORITY			(8)

/*
 * Here are the definitions for all (almost all) of the FIFO's that are in use
 * FIFONAME, FIFONUM2STR and _FIFONUM2STR are just helper macros for automating
 * the naming code. They are not meant to be used by calling clients.
 * Note tha fifos 0 and 1 are not part of this list. They are used by rtlog.
 */
#define _FIFONUM2STR(n) #n
#define FIFONUM2STR(n) _FIFONUM2STR(n)
#define FIFONAME(n) "/dev/rtf" FIFONUM2STR(n)

#define RESPFIFO			2			/* ??? */
const static char RESPFIFO_DEV[10] = FIFONAME(RESPFIFO);	
#define CMDFIFO				3			/* ??? */
const static char CMDFIFO_DEV[10] = FIFONAME(CMDFIFO);
#define TE_HANDLER_CMD_FIFO		4			/* teHandler command */
const static char TE_HANDLER_CMD_FIFO_DEV[10] = FIFONAME(TE_HANDLER_CMD_FIFO);
#define TP_WRITE_FIFO			5			/* tp_write() writes */
const static char TP_WRITE_FIFO_DEV[10] = FIFONAME(TP_WRITE_FIFO);
#define TP_READ_FIFO			6			/* tp_read() reads */
const static char TP_READ_FIFO_DEV[10] = FIFONAME(TP_READ_FIFO);

/*
 * common macros and exported functions (kernel space)
 */
#ifdef __KERNEL__
/*
 * module init/exit task attributes
 */
#define RT_TOOLS_INIT_TASK_STACK	32768
#define RT_TOOLS_INIT_TASK_PRIO		10
#define RT_TOOLS_CLEANUP_TASK_STACK	32768
#define RT_TOOLS_CLEANUP_TASK_PRIO	10

/*
 * RTOS kernel module stages. This value decides, what the main
 * function does.
 */
#define RT_TOOLS_MODULE_STAGE_INIT 1
#define RT_TOOLS_MODULE_STAGE_EXIT 2
#define RT_TOOLS_MODULE_STAGE_CLEANUP 3

/*
 * RTOS kernel module error codes.
 */
#define RT_TOOLS_MODULE_INIT_SUCCESS 0
#define RT_TOOLS_MODULE_EXIT_SUCCESS 0
/*
 * Define an error base which does not conflict with the Linux and
 * kernel errnos.
 */
#define RT_TOOLS_MODULE_ERROR_BASE 256
/*
 * Standard errors.
 */
#define RT_TOOLS_MODULE_INIT_ERROR -(RT_TOOLS_MODULE_ERROR_BASE + 1)
#define RT_TOOLS_MODULE_INIT_TIMEOUT -(RT_TOOLS_MODULE_ERROR_BASE + 2)
#define RT_TOOLS_MODULE_CLEANUP_ERROR -(RT_TOOLS_MODULE_ERROR_BASE + 3)
#define RT_TOOLS_MODULE_NO_INIT_TASK_ERROR -(RT_TOOLS_MODULE_ERROR_BASE + 4)
#define RT_TOOLS_MODULE_INIT_EXIT_ERROR -(RT_TOOLS_MODULE_ERROR_BASE + 5)
#define RT_TOOLS_MODULE_PARAM_ERROR -(RT_TOOLS_MODULE_ERROR_BASE + 6)
    
int rtToolsWaitOnFlag(const atomic_t *flag, const unsigned int to);
int rtToolsSymbolTableSize(const char *modname);
void *rtToolsLookupSymbol(const char *modname, const char *symname);
RTIME rtToolsLlFloor(RTIME val, int div);

#endif /* __KERNEL__ */

/*
 * Kernel and user space repeat guard.
 *
 * Use the "constructors" RTGUARD or RTGUARD_SETUP to create an
 * RTGUARD.
 *
 * ATTENTION!
 * The developer is responsible for serialisation of concurrent accesses to the
 * same guard structure! The use of RT-semaphores is mandatory!
 */

/* Log when?
 *   RTGUARD_MODE_ALWAYS: log always
 *   RTGUARD_MODE_COUNTER: check counter only
 *   RTGUARD_MODE_TIMER: check timestamp only
 *   RTGUARD_MODE_BOTH: check both
 */
#define RTGUARD_MODE_ALWAYS 1 << 0
#define RTGUARD_MODE_COUNTER 1 << 1
#define RTGUARD_MODE_TIMER 1 << 2
#define RTGUARD_MODE_BOTH (RTGUARD_MODE_COUNTER | RTGUARD_MODE_TIMER)

typedef struct
{
    const unsigned long long maxCounter;
    const RTIME minTime;

    /*
     * mode is a flag which makes it possible to selectively log the message.
     * See RTGUARD_MODE_* defines.
     */
    const unsigned char mode;

    /*
     * Can be used to temporarily turn guard off. Will be automatically turned on
     * after the next check of the guard is successful. Simply put: the check of
     * the guard will return 0 although the condition for success is fulfilled.
     */
    unsigned char isGuardOn;

    /*
     * Bookkeeping stuff for the RT-logging system follows.
     * Not initialised by the developer but by the
     * RT-logging system when the structure is created
     */

    /* Count the repetitions. */
    unsigned long long counter;
    /* Last time stamp of log. */
    RTIME timeStamp;
    /* First time stamp of successful log. */
    RTIME initialTimeStamp;
} rtToolsRepeatGuard_s;

/*
 * The "standard constructor" for the rtToolsRepeatGuardLogger_s structure.
 *
 * The developer is responsible for a resonable set up. For instance, setting the
 *  mode to RTGUARD_MODE_ALWAYS | ~RTGUARD_MODE_COUNTER
 * does not make sense. Use RTGUARD_MODE_TIMER if you want to
 * use the timer check only. Set it to one(!) of the RTGUARD_MODE values!
 *
 * Unit of minTime is seconds!
 */
#define RTGUARD_STD(maxCounter, minTime, mode) \
static rtToolsRepeatGuard_s RTGUARDs_own_rtGuard = \
{ \
    maxCounter, (RTIME)(minTime * 1000000000LL), mode, 1U, 0ULL, 0LL, 0LL \
};

/*
 * In case somebody has already a globally defined RTGUARD_STD:
 * Don't define a new rtToolsRepeatGuard_s and use GuardStruct instead as
 * rtToolsRepeatGuard_s variable.
 */
#define RTGUARD(GuardStruct, maxCounter, minTime, mode) \
static rtToolsRepeatGuard_s GuardStruct = \
{ \
    maxCounter, (RTIME)(minTime * 1000000000LL), mode, 1U, 0ULL, 0LL, 0LL \
};

/*
 * Checking functions.
 */
#ifdef __KERNEL__
    unsigned int rtGuard_check(rtToolsRepeatGuard_s*);
#else
inline unsigned int rtGuard_check(rtToolsRepeatGuard_s* guard)
{
    unsigned char doLog = 0U;
    const RTIME timeStamp = rt_get_time_ns();
    
    guard->timeStamp =  timeStamp - guard->initialTimeStamp;
    ++(guard->counter);
    
    /*
     * Check if at least one of the conditions is true.
     */
    if(guard->counter == guard->maxCounter)
    {
        guard->counter = 0ULL;
        doLog |= RTGUARD_MODE_COUNTER;
    }
    
    if(guard->timeStamp >= guard->minTime)
    {
        guard->initialTimeStamp = timeStamp;
        doLog |= RTGUARD_MODE_TIMER;
    }
        
    if((doLog & guard->mode) != 0U)
    {
        if(guard->isGuardOn != 1U)
        {
            guard->isGuardOn = 1U;
        }
        else
        {
            return 1;
        }
    }
        
    return 0;
}
#endif
#define rtGuard_checkStd() rtGuard_check(&RTGUARDs_own_rtGuard)

/*
 * Disable the guard temporarily.
 */
#ifdef __KERNEL__
void rtGuard_disable(rtToolsRepeatGuard_s*);
#else
inline void rtGuard_disable(rtToolsRepeatGuard_s* guard)
{
    guard->isGuardOn = 0U;
}
#endif
#define rtGuard_disableStd() rtGuard_disable(&RTGUARDs_own_rtGuard);

/*
 * Logging macros which use an RTGUARD.
 *
 * Keep the curly braces around the calls!
 *
 * modName needs to be defined as global/local [const] char* and shall
 * contain the module name. If the official ALMA kernel module template
 * is used, this will be the case. 
 */
#define RTLOG_GUARD_STD(type, ...) \
{ \
    if(rtGuard_checkStd() == 1) \
    { \
        RTLOG(modName, type, __VA_ARGS__) \
    } \
}

#define RTLOG_GUARD(guardstruct, type, ...) \
{ \
    if(rtGuard_check(guardstruct) == 1) \
    { \
        RTLOG(modName, type, __VA_ARGS__) \
    } \
}

#ifdef __cplusplus
}
#endif

#endif /*!RTTOOLS_H*/

/*___oOo___*/
