#ifndef FIND_KERNEL_MODULES_H
#define FIND_KERNEL_MODULES_H
/* @(#) $Id$
 *
 * Copyright (C) 2006
 * Associated Universities, Inc. Washington DC, USA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * Correspondence concerning ALMA Software should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */

/** This function can be called from user space to find if kernel modules are
 ** loaded. This replaces query_module() for Linux kernel 2.6. The algorithm is
 ** straight-forward. A vector of kernel module name strings are passed in. The
 ** memory file /proc/kallsyms is opened and read looking for each requested
 ** kernel name. kallsyms lists all of the exported symbols in each kernel
 ** module.
 **
 ** @param kernelModules An STL vector of strings with kernel module names to
 **    look for. They should be of the form: "teSched", "teHandler", i.e., omit
 **    the module extension.
 ** @return True if all requested kernel modules are loaded, else false.
 ** @todo Return which modules were not loaded.
 */

#include <vector>
#include <string>

bool findKernelModules( const std::vector<std::string> kernelModules );

#endif
