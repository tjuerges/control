#ifndef rtToolsFifoCmd_H
#define rtToolsFifoCmd_H
/*******************************************************************************
* ALMA - Atacama Large Millimeter Array
* (c) Associated Universities Inc., 2004 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when        what
* --------  ----------  ----------------------------------------------
* ramestic  2004-11-01  created
*/

/*
 * the following available only for C++ applications.
 */
#ifdef __cplusplus

//
// System stuff 
//
#include <cstdlib> /* needed here do to the malloc in the template class */
#include <vector>

#define RT_TOOLS_CMD_ACCESS_TO	10	/* milli-seconds */

typedef enum
{
    RT_TOOLS_CMD_OPEN_ERR = 1,
    RT_TOOLS_CMD_EMPTY_ERR,
    RT_TOOLS_CMD_DESTROY_ERR,
    RT_TOOLS_CMD_CLOSE_ERR,
    RT_TOOLS_CMD_SEM_TIMEOUT_ERR,
    RT_TOOLS_CMD_WRITE_ERR,
    RT_TOOLS_CMD_READ_ERR,
    RT_TOOLS_CMD_SEM_POST_ERR,
    RT_TOOLS_CMD_TIMEOUT_ERR
}rtToolsFifoCmdErr_t;

template <class Params>
class rtToolsCmdBuffer
{
    /**
     * rtToolsCmdBuffer is a template class that facilitates the handling
     * of different types of commands to be passed to a kernel modules via
     * an rtai fifo. These commands have the form of a command index plus
     * an arbitrary amount of parameters data. Well, not really too arbitrary;
     * the scheme is such that the command index is an integer and the parameters
     * a regular c structure, such that index and params can be binary packed
     * together. The template parameter is the type of the structure that
     * holds together all the information required by the command index.
     */

public:
    /**
     * the constructor associates the object to a given command index
     */
    rtToolsCmdBuffer(int idx)
    {
	cmdIdx = idx;
	buffer.resize(size());
    }

    rtToolsCmdBuffer(int idx, const Params &rParams)
	: myParams(rParams)
    {
	cmdIdx = idx;
	buffer.resize(size());
    }

    void *pack()
    {
	if ( buffer.size() > 0 )
	{
	    std::memcpy(&(*buffer.begin()), (void *)&cmdIdx, sizeof(cmdIdx));
	    std::memcpy((void *)(&(*buffer.begin()) + sizeof(cmdIdx)),
                        (void *)&myParams, sizeof(Params));
	}
        else
        {
            throw RT_TOOLS_CMD_EMPTY_ERR;
        }

	return (void *)&(*buffer.begin());
    }

    int size() {return sizeof(cmdIdx) + sizeof(Params);}

    Params* operator->() { return &myParams; }

private:
    int cmdIdx;
    Params myParams;
    std::vector<char> buffer;
};

class rtToolsFifoCmd
{
    /**
     * rtToolsFifoCmd is a class that let's encapsulate the details required
     * for implementing a command/reply communication pattern between a user
     * space process and a kernel space module. This class is based on the
     * following design restriction: a command is always initiated from user
     * space, and no other commands can be handled until the current one is
     * completed (with error or success).
     * The physical communication medium is an rtai fifo, which must be
     * created from kernel space when the kernel implementing the commands
     * is loaded.
     * RTAI implements each fifo with an associated semaphore, this semaphore
     * is used by this design for synchronizing the access of multiple users to the
     * same kernel module. In this is possible to guaranty that the fifo will
     * written by other users while a current command is in progress.
     */

public:
    /**
     * a default constructor that initialize the fifo file descriptor to invalid
     */
    rtToolsFifoCmd() { fd = -RT_TOOLS_CMD_OPEN_ERR; }

    /**
     * this constructor receives the name of the rtai fifo and opens a file
     * descriptor on it
     */
    rtToolsFifoCmd(const char *name);

    /**
     * close fifo file descriptor
     */
    ~rtToolsFifoCmd();

    /**
     * gets the fifo semaphore and writes the command into the fifo. If any
     * of these actions fail then an exception is thrown.
     * @param cmd pointer to the command data block (it includes the command index)
     * @param len lenght in bytes of the command data block
     * @param to timeout in milliseconds
     * @excep rtToolsFifoCmdErr_t an enumeration representing the error code.
     */
    void sendCmd(void *cmd, int len, int to);

    /**
     * reads from fifo an expected reply. After reading the data from the fifo
     * the semaphore is given, allowing this other users to execute their own
     * commands
     * @param reply pointer to the reply buffer
     * @param len length in bytes of the expected reply
     * @param to timeout in milliseconds
     * @excep rtToolsFifoCmdErr_t an enumeration representing the error code.
     */
    void recvCmd(void *replay, int len, int to);
    
    /**
     * this method is a wrapped for a sendCmd-recvCmd operation. It takes care
     * of decoupling the timeout such that the time required for actually
     * sending the command, waiting for other users of the fifo channel, from
     * the timeout value passed to recvCmd. It is important to note that 
     * using sendCmd and recevCmd just like that is tricky, because the seamphore
     * must be given back in case of errors, otherwise, the channel will remain
     * busy forever.
     * @param cmd pointer to the command data block (it includes the command index)
     * @param cmdlen lenght in bytes of the command data block
     * @param reply pointer to the reply buffer
     * @param replylen length in bytes of the expected reply
     * @param to timeout in milliseconds
     * @excep rtToolsFifoCmdErr_t an enumeration representing the error code.
     */
    void sendRecvCmd(void *cmd, int cmdlen,
		     void *replay, int replylen, 
		     int to);

private:
    int fd;    
};

#endif /* ifdef __cplusplus */

#endif /* rtToolsFifoCmd_H */
