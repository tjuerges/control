/* @(#) $Id$
 * Copyright (C) 2001
 * Associated Universities, Inc. Washington DC, USA.
 *
 * Produced for the ALMA project
 *
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU Library General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is distributed in the hope that it will be useful but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public 
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License 
 * along with this library; if not, write to the Free Software Foundation, 
 * Inc., 675 Massachusetts Ave, Cambridge, MA, 02139, USA.
 *
 * Correspondence concerning ALMA should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */


/*
 * This is a simple test module which triggers the reset line on the
 * Parallel-IO board every 2s until the module gets unloaded.
 */


#include <linux/module.h>
#include <linux/version.h>
#include <linux/delay.h>
#include <asm/atomic.h>

#ifdef USE_RTOS
#include <rtai_sem.h>
#include <rtTools.h>
#include <rtLog.h>
#endif

#include <DEParallelDrv_new.h>


extern int enableTE(void(*)(int));
extern int disableTE(void);


#define moduleName "DEParallelDrvResetTest"


MODULE_AUTHOR("Thomas Juerges <tjuerges@nrao.edu>");
MODULE_DESCRIPTION(moduleName "Reset signal test.");
MODULE_LICENSE("GPL");


static atomic_t finished = ATOMIC_INIT(1);
static atomic_t taskIsFinished = ATOMIC_INIT(0);

#ifdef USE_RTOS
static RT_TASK resetTask;
#else
static struct task_struct* resetTask;
#endif


#ifdef USE_RTOS
static void runThread(long junk __attribute__((unused)))
#else
static void runThread(void)
#endif
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #else
    set_current_state(TASK_INTERRUPTIBLE);
    #endif

    int channel = 0;

    RTLOG_INFO(moduleName, "AMBSI reset test task is running.");

    #ifdef USE_RTOS
    while(atomic_read(&finished) != 0)
    #else
    while((!kthread_should_stop())
    && (atomic_read(&finished) != 0))
    #endif
    {
        /**
         * The duty cycle of the reset signal is 1.5ms.  The test should
         * run every 20ms.  Therefore wait only
         *      20ms - 6 (channels) * 1.5ms =  20ms - 9ms = 11ms.
         */
        #ifdef USE_RTOS
        rt_sleep(nano2count(11000000ULL));
        #else
        msleep_interruptible(11U);
        #endif
        for(channel = 0; channel < 6; ++channel)
        {
            resetChannel(channel);
        }
    }

    /**
     * Signal that the test task is done.
     */
    atomic_set(&taskIsFinished, 1);

    #ifdef USE_RTOS
    rt_task_delete(rt_whoami());
    #else
    return 0;
    #endif
}

static int __init DEParallelDrvResetTest_init(void)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

    int stat = RT_TOOLS_MODULE_INIT_ERROR;

     RTLOG_INFO(moduleName, "Running DEParallelDrv reset test...");

    #ifdef USE_RTOS
    /**
     * Initialise the test task.
     */
    if(rt_task_init(&resetTask, runThread, 0, 2000, 3, 0, 0) != 0)
    {
        RTLOG_ERROR(moduleName, "Could not start the test task!");
    }
    else
    {
        /**
         * And then start it,
         */
        rt_task_resume(&resetTask);

        stat = RT_TOOLS_MODULE_INIT_SUCCESS;

        RTLOG_INFO(moduleName, "Module initialized successfully, test task "
            "(address = 0x%p) started.", &resetTask);
    }
    #else
    resetTask = kthread_run(runThread, 0, "ALMA-AMBSI_reset-Task");
    if(resetTask != ERR_PTR(-ENOMEM))
    {
        RTLOG_INFO(moduleName, "Started AMBSI_reset-Task, address = 0x%p.",
            resetTask);
    }
    stat = RT_TOOLS_MODULE_INIT_SUCCESS;
    RTLOG_INFO(moduleName, "Module initialized successfully.");
    #endif

    return stat;
}

static void __exit DEParallelDrvResetTest_exit(void)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

    if(atomic_read(&taskIsFinished) != 1)
    {
        /**
         * Tell the test task to finish up and wait one second before
         * forcefully suspending & deleting it.
         */
        atomic_set(&finished, 1);
        /**
         * Wait for 200ms.  This gives the reset thread ample time to finish.
         */
        msleep_interruptible(200U);

        #ifdef USE_RTOS
        rt_task_suspend(&resetTask);
        rt_task_delete(&resetTask);
        #else
        if((resetTask != ERR_PTR(-ENOMEM))
        && (resetTask->state == 0))
        {
            kthread_stop(resetTask);
        }
        #endif
    }

    RTLOG_INFO(moduleName, "Module cleaned up successfully.");
}

module_init(DEParallelDrvResetTest_init);
module_exit(DEParallelDrvResetTest_exit);
