/* @(#) $Id$
*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* vmeUniverseRtaiTestModule
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  2006-03-26  created
*/

/************************************************************************
*   NAME
* vmeUniverseRtaiTestModule
*   SYNOPSIS
* Kenel module to test the Tundra Universe 2 VME backplane.
*   DESCRIPTION
*
*   FILES
* vmeUniverseRtaiTestModule.c
*   ENVIRONMENT
* TAT
*   COMMANDS
*
*   RETURN VALUES
* Returns RT_TOOLS_MODULE_INIT_SUCCESS defined in
* CONTROL/CORRCommon/rtTools/include/rtTools.h if the test is successful.
*   CAUTIONS
*
*   EXAMPLES
*
*   SEE ALSO
* CONTROL/CORRCommon/vme_universe_rtai
*   BUGS
*
*------------------------------------------------------------------------
*/


#include <asm/atomic.h>

#include <vme/vme.h>
#include <vme/vme_api.h>

#ifdef USE_RTOS
#include <rtai_sched.h>
#include <rtLog.h>
#include <rtTools.h>
#else
#include <linux/kthread.h>
#endif


#define moduleName "vmeUniverseRtaiTestModule"


MODULE_AUTHOR("Thomas Juerges <tjuerges@nrao.edu>");
MODULE_DESCRIPTION(moduleName ": Test kernel module for the Tundra Universe 2 "
    "VME-PCI bridge.");
MODULE_LICENSE("GPL");


static vme_bus_handle_t handle = 0;
static int level = VME_INTERRUPT_VIRQ1;
static int vector = 0;
static atomic_t result = ATOMIC_INIT(0), finished = ATOMIC_INIT(1);
static atomic_t IRQFinished = ATOMIC_INIT(1);
#ifdef USE_RTOS
static RT_TASK taskID, irq_task;
#else
static struct task_struct* irq_task;
#endif


/**
 * RTOS task which serves as IRQ handler.
 * vme_interrupt_attach blocks until the specified IRQ is
 * activated.
 */
#ifdef USE_RTOS
static void irq_handler(long junk __attribute__((unused)))
#else
static int irq_handler(void* foo __attribute__((unused)))
#endif
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

    vme_interrupt_handle_t irq_handle = 0;
    int data = 0;

    #ifdef USE_RTOS
    RTLOG_INFO(moduleName, "IRQ handler: IRQ task active...");
    #else
    RTLOG_INFO(moduleName, "IRQ handler: IRQ kernel thread active...");
    set_current_state(TASK_INTERRUPTIBLE);
    #endif

    RTLOG_INFO(moduleName, "IRQ handler: Calling vme_attach...");

    if(vme_interrupt_attach(handle,
        &irq_handle,
        level,
        vector,
        VME_INTERRUPT_BLOCKING,
        &data) < 0)
    {
        RTLOG_ERROR(moduleName, "vme_interrupt_attach failed");
    }
    else
    {
        atomic_add(1, &result);
        RTLOG_INFO(moduleName, "received irq: level=0x%x, vector=0x%x",
            (data >> 8) & 0xff , data & 0xff);
    }

    atomic_set(&IRQFinished, 0);
    #ifdef USE_RTOS
    rt_task_delete(rt_whoami());
    #else
    return 0;
    #endif
}


#ifdef USE_RTOS
static void runThread(long junk __attribute__((unused)))
#else
static void runThread(void)
#endif
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

    int status = RT_TOOLS_MODULE_INIT_ERROR;
    #ifdef USE_RTOS
    const int STACK_SIZE = 3000;
    const int PRIORITY = RT_SCHED_LOWEST_PRIORITY - 1;
    #endif

    RTLOG_INFO(moduleName, "Test task running...");

    if(vme_init(&handle) != 0)
    {
        RTLOG_ERROR(moduleName, "vme_init failed.");
        goto LeaveTest;
    }
    else
    {
        atomic_add(1, &result);
        RTLOG_INFO(moduleName, "vme_init successful..");
    }

    #ifdef USE_RTOS
    RTLOG_INFO(moduleName, "Create IRQ handler task...");
    if(rt_task_init(&irq_task, irq_handler, 0, STACK_SIZE, PRIORITY, 1, 0) != 0)
    {
        RTLOG_ERROR(moduleName, "Could not create the IRQ handler task.");
        goto LeaveTest_vmeExit;
    }
    else
    {        
        if(rt_task_resume(&irq_task) != 0)
        {
            RTLOG_ERROR(moduleName, "Could not resume the IRQ handler task.");
            goto LeaveTest_vmeExit;
        }
        else
        {
            atomic_add(1, &result);
            RTLOG_INFO(moduleName, "IRQ handler task resumed.");
            
        }
    }
    #else
    RTLOG_INFO(moduleName, "Create IRQ handler kernel thread...");
    irq_task = kthread_run(irq_handler, 0, "ALMA-VME-IRQ_handler");
    if(irq_task == ERR_PTR(-ENOMEM))
    {
        RTLOG_ERROR(moduleName, "Could not create the IRQ handler kernel "
            "thread.");
        goto LeaveTest_vmeExit;
    }
    else
    {
        atomic_add(1, &result);
        RTLOG_INFO(moduleName, "IRQ handler kernel thread resumed.");
    }
    #endif

    if(vme_interrupt_generate(handle, level, vector) != 0)
    {
        RTLOG_ERROR(moduleName, "vme_interrupt_generate failed.");
        goto LeaveTest_vmeExit;
    }
    else
    {
        atomic_add(1, &result);
        RTLOG_INFO(moduleName, "vme_interrupt_generate sucessful.");
    }

    /* Allow 100us for IRQ delivery. */
    #ifdef USE_RTOS
    rt_sleep(nano2count(100000ULL));
    #else
    udelay(100);
    #endif

    if(atomic_read(&IRQFinished) != 0)
    {
        RTLOG_ERROR(moduleName, "VME IRQ handling unsuccessful.");
        goto LeaveTest_vmeExit;
    }
    else
    {
        atomic_add(1, &result);
        RTLOG_INFO(moduleName, "VME IRQ handling successful.");
    }

    if(vme_acquire_bus_ownership(handle) != 0)
    {
        RTLOG_ERROR(moduleName, "vme_acquire_bus_ownership failed.");
        goto LeaveTest_vmeExit;
    }
    else
    {
        atomic_add(1, &result);
        RTLOG_INFO(moduleName, "vme_acquire_bus_ownership successful.");
    }

    if(vme_release_bus_ownership(handle) != 0)
    {
        RTLOG_ERROR(moduleName, "vme_release_bus_ownership failed.");
    }
    else
    {
        atomic_add(1, &result);
        RTLOG_INFO(moduleName, "vme_release_bus_ownership successful.");
    }


LeaveTest_vmeExit:
    if(vme_term(handle) != 0)
    {
        RTLOG_ERROR(moduleName, "vme_term failed.");
    }
    else
    {
        atomic_add(1, &result);
        RTLOG_INFO(moduleName, "vme_term successful.");
    }

LeaveTest:
    if(atomic_read(&result) == 8)
    {
        status = RT_TOOLS_MODULE_INIT_SUCCESS;
    }

    RTLOG_INFO(moduleName, "vme_universe_rtai test %s.",
        (status == RT_TOOLS_MODULE_INIT_SUCCESS ? "SUCCESSFUL" : "FAILED"));

    if(atomic_read(&IRQFinished) != 0)
    {
        #ifdef USE_RTOS
        rt_task_suspend(&irq_task);
        rt_task_delete(&irq_task);
        #else
        kthread_stop(irq_task);
        #endif
    }

    atomic_set(&finished, 0);
    #ifdef USE_RTOS
    rt_task_delete(rt_whoami());
    #endif
};

static int __init vmeUniverseRtaiTestModule_init(void)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

    int stat = RT_TOOLS_MODULE_INIT_ERROR;

    RTLOG_INFO(moduleName, "Running vme_universe_rtai test...");

    #ifdef USE_RTOS
    /**
     * Initialise the test task.
     */
    if(rt_task_init(&taskID, runThread, 0, 2000, RT_SCHED_LOWEST_PRIORITY,
        0, 0)
    != 0)
    {
        RTLOG_ERROR(moduleName, "Could not start the test task!");
    }
    else
    {
        /**
         * And then start it,
         */
        rt_task_resume(&taskID);

        stat = RT_TOOLS_MODULE_INIT_SUCCESS;

        RTLOG_INFO(moduleName, "Module initialized successfully.");
    }
    #else
    /**
     * Call the test function.
     */
    runThread();
    stat = RT_TOOLS_MODULE_INIT_SUCCESS;
    #endif
    return stat;
};

static void __exit vmeUniverseRtaiTestModule_exit(void)
{
    #ifdef USE_RTOS
    rtlogRecord_t logRecord;
    #endif

    if(atomic_read(&finished) != 0)
    {
        #ifdef USE_RTOS
        rt_task_suspend(&taskID);
        rt_task_delete(&taskID);
        #endif
    }

    RTLOG_INFO(moduleName, "Unloaded.");
};

module_init(vmeUniverseRtaiTestModule_init);
module_exit(vmeUniverseRtaiTestModule_exit);
