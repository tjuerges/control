#
# $Id$
#

global PID

set ACS_TMP $env(PWD)/tatlogs/run$PID
set env(ACS_TMP) $ACS_TMP
set env(ACS_LOG_STDOUT) 4
set VME_PRESENT [exec /sbin/lspci]
set VME_PRESENT [lsearch $VME_PRESENT *Universe*]
set env(VME_PRESENT) $VME_PRESENT
