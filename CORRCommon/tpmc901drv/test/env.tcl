#*******************************************************************************
# E.S.O. - ACS project
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# tjuerges  2003-07-21  created
#

global PID

set ACS_TMP $env(PWD)/tatlogs/run$PID
set env(ACS_TMP) $ACS_TMP
set env(ACS_LOG_STDOUT) 4
set TPMC901_PRESENT [exec /sbin/lspci -d 10b5:9050]
set env(TPMC901_PRESENT) $TPMC901_PRESENT
