/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* bgustafs 2004-07-?? created
*/

#ifndef __KERNEL__
#define __KERNEL__
#endif
#ifndef MODULE
#define MODULE
#endif


#include <linux/module.h>
#include <linux/kernel.h>

#include <rtai.h>
#include <rtai_sched.h>

#include "tpmc901.h"

#include <rtDevDrv.h>

MODULE_LICENSE("GPL");

#define STACK_SIZE  2000

static RT_TASK task;

int channel=0;
module_param(channel, int, S_IRUGO);

static void bfun(long t)
{
    TP901_ACCEPT_MASKS AcceptMasks;
    TP901_BUF_DESC     BufDesc;
    TP901_MSG_BUF      MsgBuf;            /* Read/Write arguments */
    TP901_BITTIMING    BitTiming;
    int result;
    int rtDeviceID;
    tpmc901IOCTLStruct configData;
    RTIME accessControlTimeout = 0L;

    /*    int channel = 0; */
    int i,j;

    printk ("tpmc901drv: Testing tpmc901 board, channel %i\n", channel);

    rtDeviceID = rtOpen(TPMC901_RT_DEV_DRV_NAME);


    /*
     * Set the bus baud rate to 1 MBit/s
     */
    BitTiming.timing_value = TP901_1MBIT;
    BitTiming.three_samples = 0;
    configData.channel = channel;
    configData.arg = (int)&BitTiming;


    //   result = tp901_ioctl(channel, TP901_IOCSBITTIMING, (int)&BitTiming);
    result = rtIoctl(rtDeviceID, TP901_IOCSBITTIMING, &configData);
    if (result < 0) {
        return;
    }

    /*
     * Switch the bus on
     */
//    result = tp901_ioctl(channel, TP901_IOCBUSON, (int)0);
    configData.channel = channel;
    configData.arg = 0;
    result = rtIoctl(rtDeviceID, TP901_IOCBUSON, &configData);
    if (result < 0) {
        return;
    }

    /*
     * Set all of the acceptance masks to zero
     */
    AcceptMasks.global_mask_standard = 0x0;
    AcceptMasks.global_mask_extended = 0x0;
    AcceptMasks.message_15_mask = 0x0;
//    result = tp901_ioctl (channel, TP901_IOCSSETFILTER, (int)&AcceptMasks);
    configData.channel = channel;
    configData.arg = (int)&AcceptMasks;
 result = rtIoctl(rtDeviceID, TP901_IOCSSETFILTER, &configData);
    if (result < 0) {
    return;
    }
    printk ("tpmc901test: acceptance mask set\n");

    /*
     * Set up message object 15 as a receive object for an
     * extended ID of 0x0 (all messages will be received)
     */
    BufDesc.msg_obj_num = 15;
    BufDesc.extended = 1;     /* TRUE */
    BufDesc.identifier = 0x0;
    BufDesc.rx_queue_num = 1;
//    result = tp901_ioctl(channel, TP901_IOCSDEFRXBUF, (int)&BufDesc);
    configData.channel = channel;
    configData.arg = (int)&BufDesc;
    result = rtIoctl(rtDeviceID, TP901_IOCSDEFRXBUF, &configData);
    if (result < 0) {
    return;
    }
    printk ("tpmc901test: message object defined\n");

    /*
     * Flush all receive queues
     */
    //result = tp901_ioctl(channel, TP901_IOCFLUSH, (int)0);
    configData.channel = channel;
    configData.arg = 0;
    result = rtIoctl(rtDeviceID, TP901_IOCFLUSH, &configData);
    if (result < 0) {
      printk ("tpmc901test: failed flushing\n");
        return;
    }

    printk("tpmc901test: buffers flushed\n");

    /*
     * Send the identify broadcast message on CAN ID 0x0
     */
    MsgBuf.timeout = 200;
    MsgBuf.extended = 1;             /* TRUE */
    MsgBuf.identifier = 0x0;
    MsgBuf.msg_len = 0;
    //result = tp901_write(channel, (char*)&MsgBuf, sizeof(MsgBuf));
    result  = rtWrite( rtDeviceID, &channel, (char*)&MsgBuf, sizeof(MsgBuf), accessControlTimeout);
    if (result < 0) {
      printk ("tpmc901test: failed tp_write\n");
    return;
    }
    printk ("tpmc901test: identify message sent\n");

    /*
     * Read the responses
     */
    for (i=0; i<17; i++)
      {
    MsgBuf.timeout = 2000;
    MsgBuf.rx_queue_num = 1;
    MsgBuf.extended = 1;
//    result = tp901_read(channel, (char*)&MsgBuf, sizeof(MsgBuf));
    result = rtRead(rtDeviceID, &channel,(char*)&MsgBuf, sizeof(MsgBuf), accessControlTimeout);
    if (result < 0) {
      printk ("tpmc901test: failed tp_read\n");
      return;
    }

    printk("tpmc901test: read %d bytes, status %x, id %lx data:", MsgBuf.msg_len, MsgBuf.status, MsgBuf.identifier);
    for (j=0; j<MsgBuf.msg_len; j++)
      {
        printk(" %x", MsgBuf.data[j]);
      }
    printk ("\n");
      }
    }


/*****************************************************************************
 *
 * module initialization
 *
 *****************************************************************************/
static int __init tpmc901test_init(void)
{
    rt_task_init(&task, bfun, 0, STACK_SIZE, 0, 0, 0);
/*    start_rt_timer(nano2count(1000000)); */
    rt_task_resume(&task);

    return 0;
}

/*****************************************************************************
 *
 * Called to remove the module
 *
 *****************************************************************************/
static void __exit tpmc901test_exit(void)
{
  rt_task_delete(&task);
//  stop_rt_timer();
}

module_init(tpmc901test_init);
module_exit(tpmc901test_exit);







