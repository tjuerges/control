#! /bin/bash
# "@(#) $Id$"

if [ "x$TPMC901_PRESENT" == "x" ]; then
    echo "This is computer does not have a TEWS TPMC901-10 CAN-bus controller card! This test cannot run here."
    exit -1
fi

# This is needed because, by default, CVS checks this file out so that
# others cannot read it. However config/teHandlerTest.lkm needs to be
# read by root (the unloadLkmModule executable is setuid root) and on
# an NFS filesystem exported with the with root_squash option root has
# no special privileges
chmod o+r ../config/test.lkm
chmod o+r ../config/testLoadUnload-preload.lkm
chmod o+r ../config/testTpmc901DrvLoadUnload.lkm

exit 0
