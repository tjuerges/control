#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$

from ACS import acsFALSE
from ACS import acsTRUE
from Acspy.Clients.SimpleClient import PySimpleClient
import AMBSystem
import Correlator
import os
import sys
import struct
import time
import struct
from TEData import TEData



class CANMonTest:
    """This class performs a simple monitor performance test on the CAN bus.  It repeatedly
    requests a monitor point from a specified RCA at a specified node on a specified channel.
    The component name, node address, channel, and RCA can be passed in from the command line or
    defaulted.  Error statistics are kept and printed on program exit.  To exit program press
    CTRL_C. """
    def __init__(self, componentName, node, channel, RCA):
        """Construct based on argument list.  The component name, node address, channel and RCA
        can be passed from the command line. """

        self.RCA = RCA
        self.node = node
        self.channel = channel
        self.timestamp = 0
        self.errors = 0
        self.channels = 0
        
        # Get reference to the correlator CAN manager component
        try:
            self.componentName = componentName
            self.sc=PySimpleClient.getInstance()
            self.objRef=self.sc.getComponent(self.componentName)
            print 'Obtained reference to ', self.componentName
            print ''
        except Exception, e:
            print "Cannot obtain " + self.componentName +" reference! \n"+str(e)
            return
            
        iteration = 0
        # Loop until Ctrl-C is pressed, get monitor for passed-in channel/node/RCA        
        while 1:
            try:
                self.monitorPt = ""
                (self.monitorPt, self.timestamp) = \
                                        self.objRef.monitor(self.channel, self.node, self.RCA)
                if iteration % 1000 == 0:
                    print time.strftime("%H:%M:%S "), self.timestamp,
                if self.monitorPt == "":
                    self.errors = self.errors + 1
            except KeyboardInterrupt:
                print "Channel: ", self.channel
                print "   Monitor errors: ", self.errors
                print "   Total monitors requested: ", iteration
                return

            except Exception,e:
##                print "Unable to get monitor"
                self.errors = self.errors + 1
##                print e

            iteration = iteration + 1
            if iteration % 1000 == 0:
                print " Monitor: ", iteration, " node: ", self.node, " channel: ", self.channel,
                print " errors: ", self.errors


    def __del__(self):
        self.sc.disconnect()
        
#--------------------------------------------------------------------------
if __name__=='__main__':

        
    if len(sys.argv) > 1:
        node = int(sys.argv[1])
    else:
        node = 20
        
    if len(sys.argv) > 2:
        channel = int(sys.argv[2])
    else:
        channel = 0

    if len(sys.argv) > 3:
        componentName = sys.argv[3]
    else:
        componentName = "CORR_CAN_MNGR"

    if len(sys.argv) > 4:
        RCA = int(sys.argv[4])
    else:
        RCA = 0x20108

    e2e = CANMonTest(componentName, node, channel, RCA)

