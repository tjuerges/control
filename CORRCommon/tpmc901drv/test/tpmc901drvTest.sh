#! /bin/bash
#
# "@(#) $Id$"
#

max_iter=15 # maximal number of iterations (each 1second) == sleep 15

result=1
i=0

while [ $result -ne 0 -a $i -lt $max_iter ]; do
    sleep 1
    let i++
    # Precaution.  In case this driver traps the kernel, the test must fail.
    /bin/dmesg | /usr/bin/tail -n 10 | /bin/grep -c "kernel: Default Trap Handler:" > /dev/null
    result=$?
    if [ $result -eq 0 ]; then
        result=-1
        break
    else
        /bin/dmesg | /usr/bin/tail -n 10 | /bin/grep -c " test SUCCESSFUL." > /dev/null
        result=$?
    fi
done

if [ $result -eq 0 ]; then
	echo "`basename $0`: test passed."
else
	echo "`basename $0`: test failed. No result after $max_iter sec."
fi
