/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006
 *
 * $id$
 *
 * who       when        what
 * --------  ----------  ----------------------------------------------
 * bjeram  2006-04-11  created
 * tjuerges2006-04-12  Extended to threes tasks using three channels
 *                     simulataneously.
 */


#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/delay.h>

#include <rtai.h>
#include <rtai_sched.h>
#include <rtai_sem.h>
#ifdef USE_ALMA
#include <rtTools.h>
#include <rtDevDrv.h>
#include <rtLog.h>
#endif
#include <tpmc901.h>
#include <tpmc901def.h>


MODULE_DESCRIPTION("Tews TPMC901 Send and receive test.");
MODULE_AUTHOR("Bogdan Jeram <bjeram@eso.org>, "
    "Thomas Juerges <tjuerges@nrao.edu>");
MODULE_LICENSE("GPL");


/**
 * Used for the logs.
 */
#define moduleName "tpmc901SendRcvTest"


/**
 * If not in the ALMA framework, these functions must be redefined.
 */
#ifndef USE_ALMA
#define RTLOG_ERROR_IRQ(moduleName, format, args...)\
    {\
        printk(KERN_ERR "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }
#define RTLOG_WARNING_IRQ(moduleName, format, args...)\
    {\
        printk(KERN_WARNING "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }
#define RTLOG_INFO_IRQ(moduleName, format, args...)\
    {\
        printk(KERN_INFO "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }
#define RTLOG_DEBUG_IRQ(moduleName, format, args...)\
    {\
        printk(KERN_DEBUG "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }
#endif


/**
 * Module parameters
 */
/**
 * Timeout values in us for CAN-bus reading and writing. Values are in us.
 * Default values: 480 us.
 */
static unsigned long canReadTimeout = 480UL;
module_param(canReadTimeout, ulong, S_IRUGO);
static unsigned long canWriteTimeout = 480UL;
module_param(canWriteTimeout, ulong, S_IRUGO);

/**
 * This parameter tells the kernel module to stop the testing automagically
 * after a while. Default is set to 10000 which means: run 10000 ms = 10 s,
 * then stop testing. Set it to 0 for continous testing. The unit is ms.
 */
static unsigned long stopAfterSomeTime = 10000UL;
module_param(stopAfterSomeTime, ulong, S_IRUGO);

/**
 * The time each task waits before another cycle of send-receive-compare-
 * sendback-receiveback-compare is executed.
 * Default is 100 us.
 */
static unsigned long waitTime = 100UL;
module_param(waitTime, ulong, S_IRUGO);

/**
 * Channels to test. They are internally paired:
 * first entry and second,
 * third and fourth,
 * fifth and sixth. */
static int numberOfChannels = TPMC901_10_CHANNEL_COUNT;
static int channels[TPMC901_10_CHANNEL_COUNT] = { -1, -1, -1, -1, -1, -1};
module_param_array(channels, int, &numberOfChannels, S_IRUGO);

/**
 * Local data
 */
#ifdef USE_ALMA
int rtDeviceID = -1;
#endif
static RTIME WAIT_BETWEEN_RUNS;
static RT_TASK testTask[3];
static RT_TASK resultsTask;
static SEM resultsBarrier;
static atomic_t testTaskRun = ATOMIC_INIT(1);
static int numberOfStartedTestTasks = 0;


/**
 * Wait some time between the sending of data to the CAN-bus.  The time which
 * shall be waited is a module parameter.
 */
static void call_rt_sleep(void)
{
    rt_sleep(WAIT_BETWEEN_RUNS);
}

/**
 * This is a function pointer which allows to either use the RTOS or the Linux
 * kernel sleep machanisms.
 */
static void(*sleepFunc)(void) = call_rt_sleep;

/**
 * Data structure for each test task, one per channel pair.  The tests tasks
 * store the data of the test in it.  It is later evaluated in the results
 * task.
 */
typedef struct
{
    atomic_t testTaskDone;
    atomic_t activated;
    int channel1;
    int channel2;
    unsigned long canReadTimeout;
    unsigned long canWriteTimeout;
    RTIME stopAfterSomeTime;
    unsigned long roundTripsSuccessful;
    unsigned long sendErrors;
    unsigned long receiveErrors;
    unsigned long comparisonErrors;
} TaskStruct_t;

/**
 * Number of required test task structures.  How many of them will be really
 * used, depends on the module parameter defining the number of used channel
 * pairs.
 *
 */
static TaskStruct_t taskStruct[TPMC901_10_CHANNEL_COUNT / 2];

#ifdef USE_ALMA
/**
 * Open the device in the rtDevDrv framework.
 */
static int tpmc901SendRcvDevInit(void)
{
    return rtOpen(TPMC901_RT_DEV_DRV_NAME);
}

/**
 * Close the device in the rtDevDrv framework.
 */
static void tpmc901SendRcvDevExit(int rtDevDrvID)
{
    rtlogRecord_t logRecord;

    if(rtClose(rtDevDrvID) == E_RTDD_SUCCESS)
    {
        RTLOG_INFO(moduleName, "Device closed successfully.");
    }
    else
    {
        RTLOG_WARNING(moduleName, "Could not close the device driver. Expect weird"
            "system behaviour from now on!");
    }
}
#endif

/*****************************************************************************
 *
 *
 *
 *****************************************************************************/
static int tpmc901SendRcvChannelInit(int channel)
{
#ifdef USE_ALMA
    rtlogRecord_t logRecord;
    tpmc901IOCTLStruct ioctlRequest;
#endif
    TP901_BITTIMING BitTiming;
    TP901_ACCEPT_MASKS AcceptMasks;
    TP901_BUF_DESC BufDesc;
    int retVal = -1;

    RTLOG_INFO(moduleName, "Initialising channel %d...", channel);

    /*
     * Set the bus baud rate to 1 MBit/s
     */
    BitTiming.timing_value = TP901_1MBIT;
    BitTiming.three_samples = 0;

#ifdef USE_ALMA
    ioctlRequest.channel = channel;
    ioctlRequest.arg = (unsigned long) &BitTiming;

    retVal = rtIoctl(rtDeviceID,
        TP901_IOCSBITTIMING,
        (void*) &ioctlRequest);
#else
    retVal = tpmc901_ioctl(channel, TP901_IOCSBITTIMING,
        (unsigned long) &BitTiming);
#endif

    if(retVal < 0)
    {
        RTLOG_ERROR(moduleName,
            "Failed (error code = %d) to set bus baud rate "
                "to 1MBit/s.", retVal);
        return retVal;
    }

    /*
     * Switch the bus on
     */
#ifdef USE_ALMA
    retVal = rtIoctl(rtDeviceID,
        TP901_IOCBUSON,
        (void*) &ioctlRequest);
#else
    retVal = tpmc901_ioctl(channel, TP901_IOCBUSON, (unsigned long) &BitTiming);
#endif

    if(retVal < 0)
    {
        RTLOG_ERROR(moduleName,
            "Failed (error code = %d) to switch the bus on.", retVal);
        return retVal;
    }

    /*
     * Set all of the acceptance masks to zero
     */
    AcceptMasks.global_mask_standard = 0x0;
    AcceptMasks.global_mask_extended = 0x0;
    AcceptMasks.message_15_mask = 0x0;

#ifdef USE_ALMA
    ioctlRequest.arg = (unsigned long) &AcceptMasks;

    retVal = rtIoctl(rtDeviceID,
        TP901_IOCSSETFILTER,
        (void*) &ioctlRequest);
#else
    retVal = tpmc901_ioctl(channel, TP901_IOCSSETFILTER,
        (unsigned long) &AcceptMasks);
#endif

    if(retVal < 0)
    {
        RTLOG_ERROR(moduleName,
            "Failed (error code = %d) to set acceptance mask "
                "to zero.", retVal);
        return retVal;
    }

    /*
     * Set up message object 15 as a receive object for an
     * extended ID of 0x0 (all messages will be received)
     */
    BufDesc.msg_obj_num = 15;
    BufDesc.extended = 1; /* TRUE */
    BufDesc.identifier = 0x0;
    BufDesc.rx_queue_num = 1;

#ifdef USE_ALMA
    ioctlRequest.arg = (unsigned long)&BufDesc;
    retVal = rtIoctl(rtDeviceID,
        TP901_IOCSDEFRXBUF,
        (void*)&ioctlRequest);
#else
    retVal = tpmc901_ioctl(channel, TP901_IOCSDEFRXBUF,
        (unsigned long) &BufDesc);
#endif

    if(retVal < 0)
    {
        RTLOG_ERROR(moduleName, "Failed (error code = %d) to set up message "
            "object 15 as a receive object.", retVal);
        return retVal;
    }

    RTLOG_INFO(moduleName, "Channel %d successfully initialised.", channel);

    return 0;
}/* tpmc901SendRcvChannelInit */

/*****************************************************************************
 *
 *
 *
 *****************************************************************************/

static int tpmc901SendRcvSendMsg(int channel, char* buf, int len,
    unsigned long wrTimeout)
{
#ifdef USE_ALMA
    rtlogRecord_t logRecord;
#endif
    TP901_MSG_BUF msgBuf;
    int retVal;

    if(len > 8)
    {
        RTLOG_ERROR(moduleName,
            "The CAN-bus message (%d bytes, \"%s\") is too "
                "long!", len, buf);
        return -1;
    }

    /*
     * Send the message
     */
    msgBuf.timeout = wrTimeout;
    msgBuf.extended = 1; /* TRUE */
    msgBuf.identifier = 0x00; /* 0x00brodcast ?*/
    msgBuf.msg_len = len;
    memcpy(msgBuf.data, buf, msgBuf.msg_len);

#ifdef USE_ALMA
    retVal = rtWrite(rtDeviceID,
        &channel,
        (void*) &msgBuf,
        sizeof(msgBuf),
        0L/*TBD*/);
#else
    retVal = tpmc901_write(channel, (void*) &msgBuf, sizeof(TP901_MSG_BUF));
#endif

    if(retVal < 0)
    {
        RTLOG_ERROR(moduleName,
            "Failed to send message (\"%s\") on channel %d. "
                "The error code is %d.", buf, channel, retVal);
        return -3;
    }

    return retVal;
}/* tpmc901SendRcvSendMsg */

/*****************************************************************************
 *
 *
 *
 *****************************************************************************/
static void tpmc901SendRcvTask(long param)
{
#ifdef USE_ALMA
    rtlogRecord_t logRecord;
#endif
    TP901_MSG_BUF msgBuf;
    char msg[9];
    int retVal;
    int i = -1; /* that we start with 0 */
    TaskStruct_t* params = (TaskStruct_t*) param;

    if(params->stopAfterSomeTime > 0ULL)
    {
        RTLOG_INFO(moduleName,
            "This test task will stop automagically after %lld "
                "ns.", params->stopAfterSomeTime);

        params->stopAfterSomeTime += rt_get_time_ns();
    }

    atomic_set( &(params->activated), 1);
    /* Let's loop until the module is unloaded. */
    while(atomic_read( &testTaskRun) != 0)
    {
        /**
         * ATTENTION!
         * This test must be here. I put it at the top of this loop. The result
         * was that sometimes the counter for the roundtrips had been increased
         * although the loop should have been left before. Very strange! Seems
         * to be compiler/optimisation related. So, for the time being, I put
         * it here and things are reproducible.
         */
        if((params->stopAfterSomeTime > 0ULL) && (rt_get_time_ns()
            > params->stopAfterSomeTime))
        {
            break;
        }

        ++i;
        i %= 1000; /*we have just 3 places for digits in the message */
        sprintf(msg, "TMsg:%3d", i);

        retVal = tpmc901SendRcvSendMsg(params->channel1, msg, 8,
            params->canWriteTimeout);

        if(retVal < 0)
        {
            ++(params->sendErrors);

            RTLOG_ERROR(moduleName,
                "Failed to send message #%d on channel %d. "
                    "The error code is %d.", i, params->channel1, retVal);
            continue; /* go to the next iteration */
        }

        /* listen on the second (2nd) channel */
        msgBuf.timeout = params->canReadTimeout; /*TBD*/
        msgBuf.rx_queue_num = 1;
        msgBuf.extended = 1;
        memset(msgBuf.data, 0, 8);

#ifdef USE_ALMA
        retVal = rtRead(rtDeviceID,
            &(params->channel2),
            (void*) &msgBuf,
            sizeof(msgBuf),
            0L/*TBD*/);
#else
        retVal
            = tpmc901_read(params->channel2, (void*) &msgBuf, sizeof(msgBuf));
#endif

        if(retVal < 0)
        {
            ++(params->receiveErrors);

            RTLOG_ERROR(moduleName,
                "Failed to receive message #%d on channel %d. "
                    "The error code is %d.", i, params->channel2, retVal);
            continue; /* go to the next iteration */
        }

        if(strncmp(msgBuf.data, msg, 8) != 0)
        {
            ++(params->comparisonErrors);

            RTLOG_ERROR(moduleName,
                "The received message in channel %d (\"%s\") "
                    "is different from the sent (\"%s\")in channel %d with status "
                    "%d.", params->channel2, msgBuf.data, msg,
                params->channel1, msgBuf.status);
        }

        /* send the received message back to the 1st cahnnel */
        retVal = tpmc901SendRcvSendMsg(params->channel2, msgBuf.data, 8,
            params->canWriteTimeout);

        if(retVal < 0)
        {
            ++(params->sendErrors);

            RTLOG_ERROR(moduleName,
                "Could not send message #%d back to channel %d. "
                    "The error code is %d.", i, params->channel2, retVal);
            continue; /* go to the next iteration */
        }

        /* listen for the message on the 1st channel */
        msgBuf.timeout = params->canReadTimeout;
        msgBuf.rx_queue_num = 1;
        msgBuf.extended = 1;
        memset(msgBuf.data, 0, 8);

#ifdef USE_ALMA
        retVal = rtRead(rtDeviceID,
            &(params->channel1),
            (void*) &msgBuf,
            sizeof(msgBuf),
            0L/*TBD*/);
#else
        retVal
            = tpmc901_read(params->channel1, (void*) &msgBuf, sizeof(msgBuf));
#endif

        if(retVal < 0)
        {
            ++(params->receiveErrors);

            RTLOG_ERROR(moduleName, "Failed to receive resent message #%d on "
                "channel %d. The error code is %d.", i, params->channel1,
                retVal);
            continue; /* go to the next iteration */
        }

        if(strncmp(msgBuf.data, msg, 8) != 0)
        {
            ++(params->comparisonErrors);

            RTLOG_ERROR(moduleName,
                "The received resent message on channel %d "
                    "(\"%s\") is different from the sent (\"%s\") on channel %d "
                    "with status %d.", params->channel1, msgBuf.data, msg,
                params->channel2, msgBuf.status);
        }
        else
        {
            /**
             * Increase counter for successful message round trips.
             */
            ++(params->roundTripsSuccessful);
        }

        sleepFunc();
    }/* while */

    RTLOG_INFO(moduleName, "Test task stopped.");

    atomic_set( &(params->testTaskDone), 1);

    /**
     * Signal that this task stopped. After every test task stopped, the
     * results-task will jump in.
     */
    rt_sem_signal( &resultsBarrier);

    rt_task_delete( rt_whoami());
}/* tpmc901SendRcvTask */

static void results(long flag)
{
#ifdef USE_ALMA
    rtlogRecord_t logRecord;
#endif
    int i = (TPMC901_10_CHANNEL_COUNT / 2 - 1);
    int testTasksSuccessful = 0;
    unsigned long roundTrips = 0UL;

    RTLOG_INFO(moduleName, "Waiting for all test tasks to stop...");

    /**
     * This task stops here until all test tasks signal resultsBarrier.
     */
    rt_sem_wait_barrier( &resultsBarrier);

    /**
     * Give everything some time to settle down, i.e. especially give the last
     * IRQs a moment to arrive and be counted.
     */
    rt_sleep(nano2count(1000000000ULL));

    RTLOG_INFO(moduleName, "All test tasks are stopped.");

    for(; i >= 0; --i)
    {
        if(atomic_read( &(taskStruct[i].activated)) == 0)
        {
            // Test is not running. Advance to next task.
            continue;
        }

        roundTrips = taskStruct[i].sendErrors + taskStruct[i].receiveErrors
            + taskStruct[i].comparisonErrors
            + taskStruct[i].roundTripsSuccessful;

        RTLOG_INFO(moduleName, "tpmc901SendRcvTask #%d has exited.", i + 1);
        RTLOG_INFO(moduleName, "Test results for channels %d and %d follow...",
            taskStruct[i].channel1, taskStruct[i].channel2);
        RTLOG_INFO(moduleName, "Number of round trips started = %lu.",
            roundTrips);
        RTLOG_INFO(moduleName, "Number of errors while sending = %lu.",
            taskStruct[i].sendErrors);
        RTLOG_INFO(moduleName, "Number of errors while receiving = %lu.",
            taskStruct[i].receiveErrors);
        RTLOG_INFO(moduleName, "Number of errors in comparison = %lu.",
            taskStruct[i].comparisonErrors);
        RTLOG_INFO(moduleName, "Number of successful round trips = %lu.",
            taskStruct[i].roundTripsSuccessful);

        if(roundTrips == taskStruct[i].roundTripsSuccessful)
        {
            ++testTasksSuccessful;
        }
    }

    RTLOG_INFO(moduleName, "tpmc901drv test done.");

    RTLOG_INFO(moduleName, "tpmc901drv test %s.", (((testTasksSuccessful != 0)
        && (testTasksSuccessful == numberOfStartedTestTasks)) ? "SUCCESSFUL"
        : "FAILED"));

    rt_task_delete( rt_whoami());
}

/*****************************************************************************
 *
 * module initialization 
 *
 *****************************************************************************/
static int __init tpmc901SendRcvTest_init(void)
{
#ifdef USE_ALMA
    rtlogRecord_t logRecord;
#endif
    int ret = 0;
    int activeChannels = (TPMC901_10_CHANNEL_COUNT / 2);
    int i;

    /**
     * If the wait time between runs of the send/receive tasks is 0,
     * then just use the rt_task_yield() function.
     * Otherwise use our own call_rt_sleep which is the default and
     * calls rt_sleep(WAIT_BETWEEN_RUNS).
     */
    if(waitTime == 0ULL)
    {
        RTLOG_INFO(moduleName, "Using rt_task_yield() for sleeping.");
        sleepFunc = rt_task_yield;
    }
    else
    {
        RTLOG_INFO(moduleName, "Using rt_sleep() for sleeping.");
        WAIT_BETWEEN_RUNS = nano2count(waitTime * 1000ULL);
    }

    RTLOG_INFO(moduleName, "CAN-bus read timeout set to %ld us.",
        canReadTimeout);
    RTLOG_INFO(moduleName, "CAN-bus write timeout set to %ld us.",
        canWriteTimeout);

    RTLOG_INFO(moduleName, "Starting test with the following channel being "
        "paired (channels which are set to -1 will be ignored pairwise): "
        "%d and %d, %d and %d, %d and %d.",
        channels[0],
        channels[1],
        channels[2],
        channels[3],
        channels[4],
        channels[5]);

#ifdef USE_ALMA
    rtDeviceID = tpmc901SendRcvDevInit();
    if(rtDeviceID < 1)
    {
        RTLOG_ERROR(moduleName, "Did not receive a valid Device number from "
            "the rtDevDrv framework for a TPMC901 interface. Aborting!");
        return -ENODEV;
    }
    else
    {
        RTLOG_INFO(moduleName, "Device opened successfully through rtDevDrv.");
    }
#endif

    ret = 0;
    for(i = 0; i <= (TPMC901_10_CHANNEL_COUNT / 2 - 1); ++i)
    {
        if((channels[i * 2] == -1)
            || (channels[i * 2 + 1] == -1))
        {
            --activeChannels;
        }
    }

    if(activeChannels <= 0)
    {
#ifdef USE_ALMA
        tpmc901SendRcvDevExit(rtDeviceID);
#endif

        RTLOG_ERROR(moduleName, "No channel pairs are active. No test will be "
            "run.");
        return -EAGAIN;
    }
    else
    {
        RTLOG_INFO(moduleName, "%d active channel pairs detected.",
            activeChannels);
    }

    for(i = 0; i < (TPMC901_10_CHANNEL_COUNT / 2); ++i)
    {
        if((channels[i * 2] != -1)
            && (channels[i * 2 + 1] != -1))
        {
            int j = 0;

            for(; j <= 1; ++j)
            {
                int channel_partner = channels[i * 2 + j];

                ret = tpmc901SendRcvChannelInit(channel_partner);
                if(ret < 0)
                {
#ifdef USE_ALMA
                    tpmc901SendRcvDevExit(rtDeviceID);
#endif

                    RTLOG_ERROR(moduleName, "Failed to initilalise channel %d!",
                        channel_partner);

                    return ret;
                }
            }
        }
    }

    for(i = 0; i < (TPMC901_10_CHANNEL_COUNT / 2); ++i)
    {
        atomic_set(&(taskStruct[i].activated), 0);

        if((channels[i * 2] != -1)
            &&(channels[i * 2 + 1] != -1))
        {
            atomic_set(&(taskStruct[i].testTaskDone), 0);
            taskStruct[i].sendErrors = 0UL;
            taskStruct[i].receiveErrors = 0UL;
            taskStruct[i].comparisonErrors = 0UL;
            taskStruct[i].channel1 = channels[i * 2];
            taskStruct[i].channel2 = channels[i * 2 + 1];
            taskStruct[i].canReadTimeout = canReadTimeout;
            taskStruct[i].canWriteTimeout = canWriteTimeout;
            taskStruct[i].stopAfterSomeTime = stopAfterSomeTime * 1000000ULL;

            if(rt_task_init(&(testTask[i]),
                    tpmc901SendRcvTask,
                    (long)(&(taskStruct[i])),
                    8000,
                    RT_SCHED_LOWEST_PRIORITY,
                    0, 0)
                != 0)
            {
                atomic_set(&testTaskRun, 0);
                /**
                 * Wait a second to give tasks time to finish.
                 */
                mdelay(1000);
#ifdef USE_ALMA
                tpmc901SendRcvDevExit(rtDeviceID);
#endif

                RTLOG_ERROR(moduleName, "Failed to create testTask #%d for "
                    "channels %d and %d.",
                    i,
                    channels[i * 2],
                    channels[i * 2 + 1]);

                return -ECHILD;
            }
            else
            {
                if(rt_task_resume(&(testTask[i])) != 0)
                {
                    rt_task_delete(&(testTask[i]));
                    atomic_set(&testTaskRun, 0);
                    /**
                     * Wait a second to give tasks time to finish.
                     */
                    mdelay(1000);
#ifdef USE_ALMA
                    tpmc901SendRcvDevExit(rtDeviceID);
#endif

                    RTLOG_ERROR(moduleName, "Failed to resume testTask #%d for "
                        "channels %d and %d.",
                        i,
                        channels[i * 2],
                        channels[i * 2 + 1]);
                }
                else
                {
                    ++numberOfStartedTestTasks;

                    RTLOG_INFO(moduleName, "Created task #%d for channels %d "
                        "and %d.",
                        i,
                        channels[i * 2],
                        channels[i * 2 + 1]);
                }
            }
        }
    }

    /**
     * Create a barrier which blocks until numberOfStartedTestTasks tasks signal
     * it.
     */
    rt_sem_init(&resultsBarrier, numberOfStartedTestTasks + 1);

    if(rt_task_init(&resultsTask, results, 0, 8000, RT_SCHED_LOWEST_PRIORITY, 0,
            0) != 0)
    {
        RTLOG_WARNING(moduleName, "Could not create the task which gathers "
            "the results. Check the out put when the module is unloaded.");
    }
    else if(rt_task_resume(&resultsTask) != 0)
    {
        rt_task_delete(&resultsTask);

        RTLOG_WARNING(moduleName, "Could not resume the task which gathers the "
            "results. Check the out put when the module is unloaded.");
    }

    return 0;
}

/*****************************************************************************
 *
 * Called to remove the module
 *
 *****************************************************************************/
static void __exit tpmc901SendRcvTest_exit(void)
{
#ifdef USE_ALMA
    rtlogRecord_t logRecord;
#endif

    atomic_set(&testTaskRun, 0);

    /**
     * Wait one second. Every thing can settle down.
     */
    msleep_interruptible(1000U);
#ifdef USE_ALMA
    tpmc901SendRcvDevExit(rtDeviceID);
#endif

    rt_sem_delete(&resultsBarrier);

    RTLOG_INFO(moduleName, "Module unloaded.");
}

module_init( tpmc901SendRcvTest_init);
module_exit( tpmc901SendRcvTest_exit);
