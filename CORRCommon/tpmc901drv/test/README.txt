$Id$

DESCRIPTION TEST 1
This test sends and receives CAN-bus messages on three channel pairs. The pairs
are defined in the .lkm files in the tpmc901drv/config directory. Besides this
kernel module parameter, others can be defined:

- channels: an array of six (exactly six) numbers. The numbers are paired
together. This means, that the first and second entry give the channels for
the first test task, then the third and fourth for the second, and for the
third test task entries number four and five are paired together.
Pairing together means, that messages are sent out on the first channel of
the task and shall be received on the second. The message is compared and sent
back the other way round. For this to work, an appropriate loopback plug is
necessary.
A value of -1 disables this channel pair. Note: the pair not one channel since
the tasks don't test single channels.

- canReadTimeout, canWriteTimeout: timeout for CAN-bus reads/writes in us.
Default values: 480 us.

- stopAfterSomeTime: Run the test for this time in ms. A value of 0 lets the
test run until the module is unloaded.

- timeout: Every send/receive command is enclosed by taking/givind a timed
access semaphore. This value specifies the timeout in us. The default is
1000 us.

- waitTime: this value specifies how long each test task should wait between
round trips. Giving the tasks no time, i.e. let them run continously without
putting the tasks to sleep from time to time, makes the systems pretty much
unresponsible. The unit of the value is us! The default value is 100 us.

Additional hardware required! As mentioned in the description of the channels
parameter, a special loopback plug is necessary to be able to run this test. A
drawing is available through ALMA EDM. Go to 70.35 Control Software / Hardware/
Vendor Documentation / TPMC901 CAN Controller. There you will find the document
"TPMC901 loopback plug pinout.". It contains a drawing and points to a reference
table in the TPMC901 Users manual.
OUTCOME: Test report in /var/log/messages.

DESCRIPTION TEST 2
Standard endurance test. It loads the base set of modules and then it
loads and unloads the tpmc901drv module for 100 times.
OUTCOME: Test report in /var/log/messages.
