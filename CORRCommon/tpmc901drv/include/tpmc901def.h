/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003
*
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* bgustafs 2004-07-?? created from tpmc901 linux driver from TEWS Datentechnik GmbH
*/

/******************************************************************************
*******************************************************************************
**                                                                           **
**                                                                           **
**                          @@@@@@@@@@@@@@@@@@@@@@@                          **
**                          @                     @                          **
**                          @ T P M C 9 0 1 D E F @                          **
**                          @                     @                          **
**                          @@@@@@@@@@@@@@@@@@@@@@@                          **
**                                                                           **
**                                                                           **
**    Project          Linux - TPMC901 Real-Time Device Driver               **
**                                                                           **
**                                                                           **
**    File             tpmc901def.h                                          **
**                                                                           **
**                                                                           **
**    Function         Driver header file                                    **
**                                                                           **
**                                                                           **
**                                                                           **
**    Owner            TEWS TECHNOLOGIES                                     **
**                     Am Bahnhof 7                                          **
**                     D-25469 Halstenbek                                    **
**                     Germany                                               **
**                                                                           **
**                                                                           **
**                     Tel.: +49(0)4101/4058-0                               **
**                     Fax.: +49(0)4101/4058-19                              **
**                     e-mail: support@tews.com                              **
**                                                                           **
**                                                                           **
**                     Copyright (c) 2004                                    **
**                     TEWS TECHNOLOGIES GmbH                                **
**                                                                           **
**                                                                           **
**    Author           Birger Gustafsson                                     **
**                                                                           **
**    System           RTAI Linux                                            **
**                                                                           **
**                                                                           **
**                                                                           **
**                                                                           **
*******************************************************************************
*******************************************************************************/

#ifndef __TPMC901DEF_H__
#define __TPMC901DEF_H__

#include <linux/autoconf.h>
#include <linux/workqueue.h>
#include <linux/wait.h>
#include <rtai.h>
#include <rtai_sched.h>
#include <rtai_sem.h>
#include "I82527.h"


#ifdef __cplusplus
extern "C"
{
#endif

#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE !FALSE
#endif


/*
**  Defines the major device number for the TPMC901 driver
**
**  A value 0 means dynamic major number allocation
*/
#define TPMC901_MAJOR				0

/*
**  The following definitions and the array tp901_table[] describes the PCI
**  devices we are interested in.
*/
#define TPMC901_VENDOR_ID			PCI_VENDOR_ID_PLX
#define TPMC901_DEVICE_ID			PCI_DEVICE_ID_PLX_9050
#define TPMC901_SUBVENDOR_ID		0x1498
#define TPMC901_SUBDEVICE_ID		0x0385

/*
**  Module types
*/
#define TPMC901_10					2048
#define TPMC901_10_CHANNEL_COUNT	6
#define TPMC901_11					1024
#define TPMC901_11_CHANNEL_COUNT	4
#define TPMC901_12					512
#define TPMC901_12_CHANNEL_COUNT	2

/*
**  PCI resources
*/
#define TPMC901_MEM_BAR_NUM			0x02
#define TPMC901_IO_BAR_NUM			0x03


/*
**  Max. number of channels per TPMC901 device
**  TPMC901-10 -> 6 channels
**  TPMC901-11 -> 4 channels
**  TPMC901-12 -> 2 channels
*/
#define TPMC901_MAX_CHANNEL_COUNT TPMC901_10_CHANNEL_COUNT

/*
**  Size of register space per CAN channel
*/
#define TPMC901_CHANNEL_SPAN		0x100

/*
**  Global INT status register
*/
#define TPMC901_STATREG				0x00

/*
**  Limits
*/
#define MAX_CAN_MSGLEN              8
#define MAX_CAN_OBJ                 15
#define RAW_CAN_FIFO_SIZE           50	/* Defines the depth of the message FIFO inside each receive queue */
#define RX_FIFO_SIZE				100	/*  Defines the maximum number of messages which can be stored in each receive message queue */
#define NUM_RX_QUEUES				3	/*  Defines the number of receive message queues. Possible value 1..15 */


/*
** Bus on/off macro(s)
*/
#define CONTROLLER_IS_BUS_OFF(_dcb) (READ_UCHAR((_dcb)->can_reg + icCONTROL_REG) & icInit)


/*
**  This structure contains the raw CAN message FIFO item read out of
**  the CAN controller in the device ISR.
**  This structure must be packed because the data is read with the
**  READ_REGISTER_BUFFER_XXX() function.
*/

typedef struct
{
	/*  Note. arbitration must be the first element in this structure
	**  because a pointer to the begin of this structure is passed
	**  to the READ_REGISTER_BUFFER_XXX() function to fill this FIFO item.
	*/
	unsigned long arbitration;    /*  the following three fields are copied direct */
	unsigned char msg_conf_reg;   /*  out of the CAN message object */
	unsigned char data[MAX_CAN_MSGLEN];

	unsigned char status;         /*  extra message status (Overrun...) */
	unsigned char msg_obj_num;    /*  message object number 0..14 */

} RAW_CAN_MSG, *PRAW_CAN_MSG;




/*
**  This structure contains a pre-processed CAN message stored in the Rx
**  FIFO's.
**  This structure should be packed to save memory space.
*/

typedef struct
{
	unsigned long identifier;     /*  29- or 11-bit message identifier */
	unsigned char extended;       /*  TRUE if this is an extended (29-bit) message */
	unsigned char msg_len;        /*  message length 0..8 byte */
	unsigned char data[MAX_CAN_MSGLEN];
	unsigned char status;         /*  extra message status */
} CAN_MSG, *PCAN_MSG;




/*
**  Message queue control
*/
typedef struct
{
	wait_queue_head_t wait_queue;
	CAN_MSG msg_fifo[RX_FIFO_SIZE];  /*  CAN message FIFO */
	unsigned long put_index;       /*  Index to the next item to put into the FIFO */
	unsigned long get_index;       /*  Index to the next item to get out of the FIFO */
} RX_QUEUE, *PRX_QUEUE;



/*
**  Types of supported CAN message objects
*/
typedef enum
{
	TRANSMIT, RECV, REMOTE_BUF, OBJ_IDLE
} OBJ_TYPE;

/*
**  This structure describes the characteristics of each message object
*/
typedef struct
{
	OBJ_TYPE obj_type;
	int occupied;
	PRX_QUEUE rx_queue;
} OBJ_DESC, *POBJ_DESC;



/*
** Channel Control Block (CCB)
** This structure is unique for each channel per device and
** contains all necessary information to maintain a minor device.
*/
typedef struct
{
	int minor;        /*  minor device number */
	void* can_reg;    /*  mapped pci memory address of this CAN controller */

	/* object description for all CAN message objects */
	OBJ_DESC obj_desc[MAX_CAN_OBJ];

	/* Message FIFO for raw CAN messages */
	/* Filled in the device ISR, emptied by the Rx DPC */
	RAW_CAN_MSG rx_raw_fifo[RAW_CAN_FIFO_SIZE];
	unsigned long rx_raw_get_index;
	unsigned long rx_raw_put_index;

	RX_QUEUE rx_queue[NUM_RX_QUEUES];

	/* Receive and transmit signalling semaphores */
	SEM rxSem;
	SEM txSem;

	SEM readAccessSem;
	SEM writeAccessSem;
} TP901_CCB, *PTP901_CCB;


/*
** Device Control Block (DCB)
*/
typedef struct
{
	struct pci_dev* dev;            /*  pointer to the attached pci device object */
	unsigned long bar[6];         /*  allocated resources by this driver */

	/* PCI MEM space */
	void* mapped_mem_addr; /*  location of the CAN Controller registers */

	/* PCI I/O space */
	void* io_addr;	     /*  location of the INT. status register */
	int irq;             /*  allocated PCI interrupt */
	int channel_count;   /*  number of available CAN channel 2, 4 or 6 */
	TP901_CCB channel[TPMC901_MAX_CHANNEL_COUNT];
} TP901_DCB, *PTP901_DCB;

#ifdef __cplusplus
}
#endif

#endif /* __TPMC901DEF_H__ */
