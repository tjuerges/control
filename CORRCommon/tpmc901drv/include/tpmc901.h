/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003
*
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* bgustafs 2004-10-21 Added description of public commands
* bgustafs 2004-07-?? created from tpmc901 linux driver from TEWS Datentechnik GmbH
* tjuerges 2007-04-02 Cleaned up wrong model numbers.
*/

/******************************************************************************
*******************************************************************************
**                                                                           **
**                                                                           **
**                          @@@@@@@@@@@@@@@@@@@@@@@                          **
**                          @                     @                          **
**                          @    T P M C 9 0 1    @                          **
**                          @                     @                          **
**                          @@@@@@@@@@@@@@@@@@@@@@@                          **
**                                                                           **
**                                                                           **
**    Project          Linux - TPMC901 Real-time Device Driver               **
**                                                                           **
**                                                                           **
**    File             tpmc901.h                                             **
**                                                                           **
**                                                                           **
**    Function         Application and Driver header file                    **
**                                                                           **
**                                                                           **
**                                                                           **
**    Owner            TEWS TECHNOLOGIES                                     **
**                     Am Bahnhof 7                                          **
**                     D-25469 Halstenbek                                    **
**                     Germany                                               **
**                                                                           **
**                                                                           **
**                     Tel.: +49(0)4101/4058-0                               **
**                     Fax.: +49(0)4101/4058-19                              **
**                     e-mail: support@tews.com                              **
**                                                                           **
**                                                                           **
**                     Copyright (c) 2004                                    **
**                     TEWS TECHNOLOGIES GmbH                                **
**                                                                           **
**                                                                           **
**                                                                           **
**    System           RTAI Linux                                            **
**                                                                           **
**                                                                           **
**                                                                           **
**                                                                           **
*******************************************************************************
*******************************************************************************/
#ifndef __TPMC901_H__
#define __TPMC901_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <linux/types.h>
#include <linux/ioctl.h>
#include <rtai_types.h>

typedef struct
{
    int channel;
    unsigned long arg;
} tpmc901IOCTLStruct;


/* Use 0xB3 (TEWS Technologies IPAC Vendor ID) as magic number */
#define TP901_IOC_MAGIC     0xB3

/*
 * S means "Set" through a ptr,
 * T means "Tell" directly with the argument value
 * G means "Get": reply by setting through a pointer
 * Q means "Query": response is on the return value
 * X means "eXchange": G and S atomically
 * H means "sHift": T and Q atomically
 */
#define TP901_IOCSBITTIMING     _IOW_BAD(TP901_IOC_MAGIC, 1, TP901_BITTIMING)
#define TP901_IOCSSETFILTER     _IOW_BAD(TP901_IOC_MAGIC, 2, TP901_ACCEPT_MASKS)
#define TP901_IOCGGETFILTER     _IOR_BAD(TP901_IOC_MAGIC, 3, TP901_ACCEPT_MASKS)
#define TP901_IOCBUSON          _IO(TP901_IOC_MAGIC, 4)
#define TP901_IOCBUSOFF         _IO(TP901_IOC_MAGIC, 5)
#define TP901_IOCFLUSH          _IO(TP901_IOC_MAGIC, 6)
#define TP901_IOCGCANSTATUS     _IOR_BAD(TP901_IOC_MAGIC, 7, TP901_STATUS)
#define TP901_IOCSDEFRXBUF      _IOW_BAD(TP901_IOC_MAGIC, 8, TP901_BUF_DESC)
#define TP901_IOCSDEFRMTBUF     _IOW_BAD(TP901_IOC_MAGIC, 9, TP901_BUF_DESC)
#define TP901_IOCSUPDATEBUF     _IOW_BAD(TP901_IOC_MAGIC, 10, TP901_BUF_DESC)
#define TP901_IOCTRELEASEBUF    _IO(TP901_IOC_MAGIC, 11)

#define TP901_IOC_MAXNR         11


/*
**  CAN message buffer description
*/
typedef struct
{
    unsigned long  identifier;          /*  Message identifier 29-bit or 11-bit */
    unsigned char  msg_obj_num;         /*  Message object number ( 1..15) */
    unsigned char  rx_queue_num;        /*  Receive queue number ( 1..n ) */
    unsigned char  extended;            /*  TRUE if extended identifier */
    unsigned char  msg_len;             /*  CAN message length ( 0..8 ) */
    unsigned char  data[8];             /*  up to 8 message bytes */

} TP901_BUF_DESC, *PTP901_BUF_DESC;



/*
**  CAN message read/write buffer
*/
#define TP901_SUCCESS           0
#define TP901_FIFO_OVERRUN      (1<<0)
#define TP901_MSGOBJ_OVERRUN    (1<<1)
#define TP901_RAW_FIFO_OVERRUN  (1<<2)

typedef struct
{
    unsigned long  identifier;          /*  Message identifier 29-bit or 11-bit */
    unsigned long  timeout;             /*  in clock ticks */
    unsigned char  rx_queue_num;        /*  Receive queue number ( 1..n ) */
    unsigned char  extended;            /*  TRUE if extended identifier */
    unsigned char  status;              /*  extra message status field */
    unsigned char  msg_len;             /*  CAN message length ( 0..8 ) */
    unsigned char  data[8];             /*  up to 8 message bytes */

} TP901_MSG_BUF, *PTP901_MSG_BUF;



/*
**  CAN bus bit timing
*/

#define TP901_1_6MBIT   0x1100  /* 1.6 Mbit/s   max. distance = 20 m    */
#define TP901_1MBIT     0x1400  /*   1 Mbit/s   max. distance = 40 m    */
#define TP901_500KBIT   0x1c00  /* 500 Kbit/s   max. distance = 130 m   */
#define TP901_250KBIT   0x1c01  /* 250 Kbit/s   max. distance = 270 m   */
#define TP901_125KBIT   0x1c03  /* 125 Kbit/s   max. distance = 530 m   */
#define TP901_100KBIT   0x2f43  /* 100 Kbit/s   max. distance = 620 m   */
#define TP901_50KBIT    0x2f47  /*  50 Kbit/s   max. distance = 1.3 km  */
#define TP901_20KBIT    0x2f53  /*  20 Kbit/s   max. distance = 3.3 km  */
#define TP901_10KBIT    0x2f67  /*  10 Kbit/s   max. distance = 6.7 km  */
#define TP901_5KBIT     0x7f7f  /*   5 Kbit/s   max. distance = 10 km   */
/*                          xx......Bit Timing 0    */
/*                        xx........Bit Timing 1    */

typedef struct
{
    unsigned short  timing_value;   //  CAN bus bit timing
                                    //  bit 0..7  =  bit timing register 0
                                    //  bit 8..15 =  bit timing register 1
    unsigned short  three_samples;  //  TRUE to sample the CAN bus three times
                                    //  per bit time instead of one time

} TP901_BITTIMING, *PTP901_BITTIMING;



/*
**  Acceptance filter masks
*/
typedef struct
{
    unsigned long   message_15_mask;
    unsigned long   global_mask_extended;
    unsigned short  global_mask_standard;

} TP901_ACCEPT_MASKS, *PTP901_ACCEPT_MASKS;


/*
**  status struct
*/
typedef struct
{
    unsigned char   status_reg;
    unsigned char   control_reg;
} TP901_STATUS, *PTP901_STATUS;

/*
 * Functions
 */
/**
 * Read data from CAN
 *
 * Parameters
 *
 *  channel - channel of the board (0 - 5)
 *  buf     - data buffer of type TP901_MSG_BUF
 *  count   - size of data buffer
 *
 * Return value: size of buf or error
 *
 * Errors
 *
 *  -EINVAL       - if count is too small, e.g smaller that the size of TP901_MSG_BUF
 *  -ECONNREFUSED - if the bus is off and the receive fifo is empty
 *  -ETIME        - timeout on wating for a message
 **/
int tpmc901_read(int channel, char* buf, int count);

/**
 * Write data to CAN
 *
 * Parameters
 *
 *  channel - channel of the board (0 - 5)
 *  buf     - data buffer of type TP901_MSG_BUF
 *  count   - size of data buffer
 *
 * Return value: size of buf or error
 *
 * Errors
 *
 *  -EINVAL       - if count is too small, e.g smaller that the size of TP901_MSG_BUF
 *  -ECONNREFUSED - if the bus is off
 *  -ETIME        - timeout on wating for a transmit buffer or waiting for transmit to complete
 **/
int tpmc901_write(int channel, const char* buf, int count);

/**
 * I/O control command
 *
 * Parameters
 *
 *  channel - channel of the board (0 - 5)
 *  cmd     - control command
 *  arg     - argument for the control command as defined above
 *
 * Return value: size of buf or error
 *
 * Errors
 *
 *  -EINVAL     - if command type is wrong or
 *                if an illegal command was given or
 *                for command TP901_IOCSDEFRXBUF if an invalid message object number was given or
 *                                              if an invalid receive queue number was given
 *  -EADDRINUSE - for command TP901_IOCSDEFRXBUF if the selected message object is already in use
 **/
int tpmc901_ioctl(int channel, unsigned int cmd, unsigned long arg);

/**
 * ALMA rtDevDrv framework interface.
 */
int tpmc901_readInterface(void* genericInputData,
    void* bufferToRead,
    const size_t numberOfBytes,
    const RTIME timeoutNS);
int tpmc901_writeInterface(void* genericInputData,
    void* bufferToWrite,
    const size_t numberOfBytes,
    const RTIME timeoutNS);
int tpmc901_ioctlInterface(const int request, void* configData);

#ifdef __cplusplus
}
#endif


#endif  /*  __TPMC901_H__ */
