/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003
*
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* bgustafs 2004-07-?? created from tpmc901 linux driver from TEWS Datentechnik GmbH
*/

/******************************************************************************
*******************************************************************************
**                                                                           **
**                                                                           **
**                          @@@@@@@@@@@@@@@@@@@@@@@                          **
**                          @                     @                          **
**                          @     i 8 2 5 2 7     @                          **
**                          @                     @                          **
**                          @@@@@@@@@@@@@@@@@@@@@@@                          **
**                                                                           **
**                                                                           **
**    Project          Linux - TPMC901 Real-time Device Driver               **
**                                                                           **
**    File             i82527.h                                             **
**                                                                           **
**    Function         Intel 82527 programming model                         **
**                                                                           **
**                                                                           **
**    Owner            TEWS TECHNOLOGIES GmbH                                **
**                     Am Bahnhof 7                                          **
**                     D-25469 Halstenbek                                    **
**                     Germany                                               **
**                                                                           **
**                                                                           **
**                     Tel.: +49 / (0)4101 / 4058-0                          **
**                     Fax.: +49 / (0)4101 / 4058-19                         **
**                     e-mail: support@tews.com                              **
**                                                                           **
**                                                                           **
**                     Copyright (c) 2000-2003                               **
**                     TEWS TECHNOLOGIES GmbH                                **
**                                                                           **
**                                                                           **
**    Author           Birger Gustafsson                                 **
**                                                                           **
**    System           RTAI Linux                                            **
**                                                                           **
**                                                                           **
*******************************************************************************
*******************************************************************************/

#ifndef    __i82527_H__
#define    __i82527_H__

#ifdef    __cplusplus
extern    "C" {
#endif



/*----------------------------------------------------------------------
  Control Register
  ----------------------------------------------------------------------*/
#define    icCCE            (1<<6)        /* Change Configuration Enable */
#define    icEIE            (1<<3)        /* Error Interrupt Enable */
#define    icSIE            (1<<2)        /* Status Change Interrupt Enable */
#define    icIE            (1<<1)        /* Interrupt Enable */
#define    icInit            (1<<0)        /* Initialization */


/*----------------------------------------------------------------------
  Status Register
  ----------------------------------------------------------------------*/
#define    icBOff            (1<<7)        /* Bus Off Status */
#define    icWarn            (1<<6)        /* Warning Status */
#define    icWake            (1<<5)        /* Wake up Status */
#define    icRXOK            (1<<4)        /* Receive Message Successfully */
#define    icTXOK            (1<<3)        /* Transmit Message Successfully */
#define    icLEC            7            /* Last Error Code Mask */


/*----------------------------------------------------------------------
  CPU Interface Register
  ----------------------------------------------------------------------*/
#define    icRstSt            (1<<7)        /* Hardware Reset Status */
#define    icDSC            (1<<6)        /* Devide System Clock */
#define    icDMC            (1<<5)        /* Device Memory Clock */
#define    icPwD            (1<<4)        /* Power Down Mode Enable */
#define    icSleep            (1<<3)        /* Enter Sleep Mode */
#define    icMUX            (1<<2)        /* Multiplex for ISO Low Speed Physical Layer */
#define    icCEn            (1<<0)        /* Clockout enable */


/*----------------------------------------------------------------------
  Bus Configuration Register
  ----------------------------------------------------------------------*/
#define    icCoBy            (1<<6)        /* Comparator Bypass */
#define    icPol            (1<<5)        /* Polarity */
#define    icDcT1            (1<<3)        /* Disconnect TX1 output */
#define    icDcR1            (1<<1)        /* Disconnect RX1 input */
#define    icDcR0            (1<<0)        /* Disconnect RX0 input */


/*----------------------------------------------------------------------
  Control 0/1 Register
  ----------------------------------------------------------------------*/
#define    RESET_BIT        2
#define    SET_BIT            1
#define TEST_BIT        2

#define    SET_MsgVal        (~(SET_BIT << 6))    /* Message Valid */
#define    RESET_MsgVal    (~(RESET_BIT << 6))
#define    TEST_MsgVal        (TEST_BIT<<6)

#define    SET_TXIE        (~(SET_BIT << 4))    /* Transmit Interrupt Enable */
#define    RESET_TXIE        (~(RESET_BIT << 4))
#define    TEST_TXIE        (TEST_BIT << 4)

#define    SET_RXIE        (~(SET_BIT << 2))    /* Receive Interrupt Enable */
#define    RESET_RXIE        (~(RESET_BIT << 2))
#define    TEST_RXIE        (TEST_BIT << 2)

#define    SET_IntPnd        (~(SET_BIT << 0))    /* Interrupt Pending */
#define    RESET_IntPnd    (~(RESET_BIT << 0))
#define    TEST_IntPnd        (TEST_BIT << 0)

#define    SET_RmtPnd        (~(SET_BIT << 6))    /* Remote Frame Pending */
#define    RESET_RmtPnd    (~(RESET_BIT << 6))
#define    TEST_RmtPnd        (TEST_BIT << 6)

#define    SET_TxRqst        (~(SET_BIT << 4))    /* Transmit Request */
#define    RESET_TxRqst    (~(RESET_BIT << 4))
#define    TEST_TxRqst        (TEST_BIT << 4)

#define    SET_MsgLst        (~(SET_BIT << 2))    /* Message Lost */
#define    RESET_MsgLst    (~(RESET_BIT << 2))
#define    TEST_MsgLst        (TEST_BIT << 2)

#define    SET_CPUUpd        (~(SET_BIT << 2))    /* CPU Updating */
#define    RESET_CPUUpd    (~(RESET_BIT << 2))
#define    TEST_CPUUpd        (TEST_BIT << 2)

#define    SET_NewDat        (~(SET_BIT << 0))    /* New Data */
#define    RESET_NewDat    (~(RESET_BIT << 0))
#define    TEST_NewDat        (TEST_BIT << 0)


/*----------------------------------------------------------------------
  Message Configuration Register
  ----------------------------------------------------------------------*/
#define    DLC_Shift        4        /* # shifts for Data Length Code */
#define    icDIRTX            (1<<3)        /* Direction = transmit */
#define    icXtd            (1<<2)        /* Extended Identifier */


/*----------------------------------------------------------------------
  i82527 Register Map ( see also i82527 hardware manual )
  ----------------------------------------------------------------------*/

#define icCONTROL_REG       0
#define icSTATUS_REG        1
#define icINTERFACE_REG     2
#define icHIGH_SPEED_REG    4
#define icGLOBAL_MASK_STD   6
#define icGLOBAL_MASK_EXT   8
#define icMSG_15_MASK       0x0C
#define icCLKOUT_REG        0x1F
#define icBUS_CONFIG_REG    0x2F
#define icBIT_TIMING_0_REG  0x3F
#define icBIT_TIMING_1_REG  0x4F
#define icINTERRUPT_REG     0x5F

#define icOBJ_OFFSET        0x10
#define icOBJ_SPAN          0x10

#define icOBJ_CONTROL_0     0
#define icOBJ_CONTROL_1     1
#define icOBJ_ARBITRATION   2
#define icOBJ_CONFIG        6
#define icOBJ_DATA          7
#define icOBJ_RAW_DATA      2
#define icOBJ_RAW_SIZE      13

#ifdef    __cplusplus
}
#endif

#endif    /* __i82527_H__ */

