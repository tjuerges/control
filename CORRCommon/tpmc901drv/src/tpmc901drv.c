/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003
*
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* bgustafs 2004-07-?? created from tpmc901 linux driver from TEWS Datentechnik GmbH
* tjuerges 2006-03-21 Code review
* tjuerges 2006-12-13 Fix for missing one CAN response in one out of 1e5
*                     CAN messages.
* tjuerges 2008-11    Kernel 2.6.23 and RTAI 3.6.1 port.
*/

/******************************************************************************
*******************************************************************************
**                                                                           **
**                                                                           **
**                          @@@@@@@@@@@@@@@@@@@@@@@                          **
**                          @                     @                          **
**                          @ T P M C 9 0 1 D R V @                          **
**                          @                     @                          **
**                          @@@@@@@@@@@@@@@@@@@@@@@                          **
**                                                                           **
**                                                                           **
**    Project          Real-time Linux - TPMC901 Device Driver               **
**                                                                           **
**                                                                           **
**    File             tpmc901drv.c                                          **
**                                                                           **
**                                                                           **
**    Function         Driver source for TPMC901                             **
**                                                                           **
**                                                                           **
**    Owner            TEWS TECHNOLOGIES                                     **
**                     Am Bahnhof 7                                          **
**                     D-25469 Halstenbek                                    **
**                     Germany                                               **
**                                                                           **
**                                                                           **
**                     Tel.: +49(0)4101/4058-0                               **
**                     Fax.: +49(0)4101/4058-19                              **
**                     e-mail: support@tews.com                              **
**                                                                           **
**                                                                           **
**                     Copyright (c) 2004                                    **
**                     TEWS TECHNOLOGIES GmbH                                **
**                                                                           **
**                                                                           **
**    Author           Thomas Juerges                                        **
**                     Birger Gustafsson                                     **
**                                                                           **
**    System           RTAI Linux                                            **
**                                                                           **
**                                                                           **
*******************************************************************************
*******************************************************************************/


#define moduleName "tpmc901drv"

#define TPMC901_DEBUG_NAME "TPMC901: "
#define DRIVER_NAME "TEWS TECHNOLOGIES - TPMC901"
#define DRIVER_VERSION "1.1.2"
#define DRIVER_BIN_VERSION 0x10102
#define DRIVER_REVDATE "2008-11-14"

/**
 * Linux kernel include files.
 */
#include <linux/autoconf.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/init.h>
#include <linux/mm.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/pci.h>
#include <linux/list.h>
#include <linux/interrupt.h>
#include <linux/proc_fs.h>
#ifndef USE_ALMA
#include <linux/time.h>
#endif

#include <asm/uaccess.h>
#include <asm/system.h>
#include <asm/io.h>
#include <asm/delay.h>


/**
 * RTOS include files.
 */
#include <rtai.h>
#include <rtai_sched.h>


/**
 * ALMA project related include files.
 *
 * Mainly for feeding the logs into the ACS system and the unified RTOS device
 * driver framework (rtDevDrv).
 */
#ifdef USE_ALMA
#include <rtLog.h>
#include <rtTools.h>
#include <rtDevDrv.h>

#ifdef DEBUG_IRQS
#include <DEParallelDrv.h>
#endif
#endif

/**
 * TPMC901 include files.
 */
#include "tpmc901.h"
#include "tpmc901def.h"


/**
 * Module necessities.
 */
MODULE_DESCRIPTION("Tews TPMC901 device driver");
MODULE_AUTHOR("TEWS Technologies <support@tews.com>, "
    "Thomas Juerges <tjuerges@nrao.edu>");

/**
 * Notabene:
 * Since this module is not publicly downloadable, it should perhaps be under
 * Dual BSD/GPL or under Dual MPL/GPL?
 */
#warning Changed the license type from GPL to LGPL. This module does not \
conform to the GPL since it is not publicly available.
MODULE_LICENSE("LGPL");

/**
 * Module parameters.
 */
#if defined USE_ALMA && defined DEBUG_IRQS
/**
 * Module parameter to allow debugging of the IRQ handler.
 * Whenever the handler is entered, the reset pin of
 * the channel resetChannelForDebug on the DEParallel board
 * is switched on/off when entering/leaving the handler.
 * For debugging of the IRQ handler only!
 */
static unsigned int resetChannelForDebug = 2U;
module_param(resetChannelForDebug, uint, S_IRUGO);
#endif

/**
 * Log timeouts? Set to 1 (yes) or 0 (no)
 */
static unsigned int logTimeout = 0U;
module_param(logTimeout, uint, S_IRUGO);

/**
 * The timeout in usec for RTOS semaphores. After the timeout proper error
 * handling is done or - at least - a warning is printed out.
 * The unit is us.
 */
static unsigned int semaphoreTimeout = 1000U;
module_param(semaphoreTimeout, uint, S_IRUGO);

/**
 * Amount of time to busy-sleep inside ISR needed by tpmc901.
 * Theoretically, this time should be 2us. However, in
 * some computers (notably dual Opteron computers) the required time
 * is variable. In dual Opteron computers this time is ~10 us.
 * Unit: ns.
 * Default value: 2000ns.
 */
static uint interruptSleep = 2000U;
module_param(interruptSleep, uint, S_IRUGO);


/**
 * Local data.
 */
static char softwareVersion[] =
    "$Id$";

/* The atomic_t will be 32 bits in size, hence the counter will last, if one
 * message per TE is sent or received, for 6about .5 years.  If this is not
 * enough, I can use atomic_long_t as soon as we switch to 64 bit.
 */
static unsigned int totalNumberOfMessages = 0U;
static unsigned int totalNumberOfErrors = 0U;
static atomic_t numberOfMessagesInInterval = ATOMIC_INIT(0L);
static atomic_t numberOfErrorsInInterval = ATOMIC_INIT(0L);
#ifdef USE_ALMA
static RTIME startTimeStamp = 0ULL;
static RTIME startIntervalTimeStamp = 0ULL;
#else
static s64 startTimeStamp = 0LL;
static s64 startIntervalTimeStamp = 0LL;
#endif
typedef struct
{
    atomic_t flag;
#ifdef USE_ALMA
    RTIME lastTimeStamp;
#else
    s64 lastTimeStamp;
#endif
} problemFlags;
static problemFlags busOffFlag[6];
static problemFlags warnFlag[6];

/**
 * Data structure for the /proc page.
 */
static struct proc_dir_entry* procEntry = 0;


/**
 * These defines make usage of this driver without ALMA's rtLog kernel
 * module possible. The log messages look almost the same but for the
 * missing timestamp.
 */
#ifndef USE_ALMA
#define RTLOG_ERROR(moduleName, format, args...)\
    {\
        printk(KERN_ERR "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }
#define RTLOG_WARNING(moduleName, format, args...)\
    {\
        printk(KERN_WARNING "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }
#define RTLOG_INFO(moduleName, format, args...)\
    {\
        printk(KERN_INFO "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }
#define RTLOG_DEBUG(moduleName, format, args...)\
    {\
        printk(KERN_DEBUG "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }

/**
 * For the IRQ handler.
 */
#define RTLOG_ERROR_IRQ RTLOG_ERROR
#define RTLOG_INFO_IRQ RTLOG_INFO
#define RTLOG_DEBUG_IRQ RTLOG_DEBUG

#else
/**
 * The IRQ handler should not use the ACS logging system.
 * Therefore define some extra logging macros.
 */
#define RTLOG_ERROR_IRQ(moduleName, format, args...)\
    {\
        rt_printk(KERN_ERR "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }
#define RTLOG_WARNING_IRQ(moduleName, format, args...)\
    {\
        rt_printk(KERN_WARNING "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }
#define RTLOG_INFO_IRQ(moduleName, format, args...)\
    {\
        rt_printk(KERN_INFO "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }
#define RTLOG_DEBUG_IRQ(moduleName, format, args...)\
    {\
        rt_printk(KERN_DEBUG "%s%s(%d) - " format "\n", \
            moduleName, __FILE__, __LINE__, ##args);\
    }
#endif

/**
 * Device control block (DCB).
 */
static unsigned char _dcb_space[sizeof(TP901_DCB)];
static const PTP901_DCB dcb = (PTP901_DCB)(_dcb_space);

/**
 * The bit mask for the IRQs in TPMC901_STATREG. By default, it is set
 * to the bit mask for the TPMC901-10 (six channels).
 */
static unsigned char TPMC901_IRQBitMask =
    ((1U << TPMC901_MAX_CHANNEL_COUNT) -1U);

/**
 * Indicates, that the driver has to clean up the device.
 */
static atomic_t cleanedUp = ATOMIC_INIT(FALSE);
/**
 * RTOS Time type for the semaphore timeout.
 */
static RTIME SEMAPHORE_TIMEOUT = 0ULL;

/**
 * Busy-sleep time used in ISR to give tpmc901 time to properly
 * update registers.
 */
static unsigned int INTERRUPT_SLEEP = 0U;

/**
 * Forward declarations.
 */
static void cleanup_device(const int failure);
static void release_obj(PTP901_CCB ccb, const unsigned int num);
static int attach_obj(PTP901_CCB ccb,
    OBJ_TYPE obj_type,
    PRX_QUEUE prx_queue,
    const int num);

/**
 * HW Access functions.
 */
/*---- UCHAR ----*/
static inline unsigned char READ_UCHAR(void* pPort)
{
    return (unsigned char)(ioread8(pPort));
}

static inline void READ_BUFFER_UCHAR(void* pReg,
    unsigned char* pBuf,
    const unsigned long count)
{
    ioread8_rep(pReg, pBuf, count);
}

static inline void READ_REGION_UCHAR(void* pReg,
    unsigned char* pBuf,
    const unsigned long count)
{
    memcpy_fromio((void*)(pBuf), pReg, count);
}

static inline void WRITE_UCHAR(void* pReg, const unsigned char value)
{
    iowrite8((u8)(value), pReg);
}

static inline void WRITE_BUFFER_UCHAR(void* pReg,
    unsigned char* pBuf,
    const unsigned long count)
{
    iowrite8_rep(pReg, (void*)(pBuf), count);
}

static inline void WRITE_REGION_UCHAR(void* pReg,
    unsigned char* pBuf,
    const unsigned long count)
{
    memcpy_toio(pReg, (void*)(pBuf), count);
}

static inline void WRITE_VALUE_TO_REGION_UCHAR(void* pReg,
    const unsigned char value,
    const unsigned long count)
{
    memset_io(pReg, value, count);
}

/*---- USHORT ----*/
static inline unsigned short READ_USHORT(void* pReg)
{
    return ioread16be(pReg);
}

static inline void WRITE_USHORT(void* pReg, const unsigned short value)
{
    iowrite16be((u16)(value), pReg);
}

/*---- ULONG ----*/
static inline unsigned long READ_ULONG(void* pReg)
{
    return ioread32be(pReg);
}

static inline void WRITE_ULONG(void* pReg, const unsigned long value)
{
    iowrite32be((u32)(value), pReg);
}


/**
 * This function creates the proc_fs page which provides some on the fly
 * information.
 */
static int readProcFsPage(char* buffer, char** start, off_t offset, int length,
    int* eof, void* data)
{
    int size = 0;
    unsigned short channel = 0;

    int messages = atomic_read(&numberOfMessagesInInterval);
    atomic_set(&numberOfMessagesInInterval, 0);
    int errors = atomic_read(&numberOfErrorsInInterval);
    atomic_set(&numberOfErrorsInInterval, 0);
    #ifdef USE_ALMA
    RTIME nowTimeStamp = rt_get_real_time_ns();
    #else
    struct timespec now = {0, 0};
    getnstimeofday(&now);
    s64 nowTimeStamp = ((s64) now.tv_sec * NSEC_PER_SEC) + now.tv_nsec;
    #endif

    totalNumberOfErrors += errors;
    totalNumberOfMessages += messages;

    size += sprintf(buffer + size, "%s kernel module information page\n\n"
        "Software version = %s\n\n"
        "Start time stamp of interval = %lld[ns]\n"
        "End time stamp of interval = %lld[ns]\n\n"
        "Length of time interval = %lld[ns]\n\n"
        "Number of CAN messages in the time interval = %u\n"
        "Number of faulty CAN messages in the time interval = %u\n\n"
        "Start time stamp overall = %lld[ns]\n"
        "Total run time = %lld[ns]\n"
        "Total number of good CAN messages = %u\n"
        "Total number of faulty CAN messages = %u\n\n",
        moduleName,
        softwareVersion,
        startIntervalTimeStamp,
        nowTimeStamp,
        nowTimeStamp - startIntervalTimeStamp,
        messages,
        errors,
        startTimeStamp,
        nowTimeStamp - startTimeStamp,
        totalNumberOfMessages,
        totalNumberOfErrors);

    for(; channel < 6U; ++channel)
    {
        size += sprintf(buffer + size, "Warn Flag (set if >= 96 errors on "
            "the CAN-bus), Channel %u = %u\n"
            "Time stamp of the last Warn event = %lld[ns]\n"
            "BOff Flag (bus off, set if >= 256 errors on the CAN-bus), "
            "Channel %u = %u\n"
            "Time stamp of the last BOff event = %lld[ns]\n\n",
            channel,
            atomic_read(&(warnFlag[channel].flag)),
            warnFlag[channel].lastTimeStamp,
            channel,
            atomic_read(&(busOffFlag[channel].flag)),
            busOffFlag[channel].lastTimeStamp);
    }

    startIntervalTimeStamp = nowTimeStamp;

    return size;
}


/**
 * Register dump function for CAN Controller diagnostic.
 */
#ifdef DEBUG_TPMC901
static void dump(TP901_CCB *ccb)
{
    #ifdef USE_ALMA
    rtlogRecord_t logRecord;
    #endif

    /* Memory dump. */
    RTLOG_DEBUG(moduleName, "Control Reg. = 0x%02X",
        READ_UCHAR((void *)(ccb->can_reg + icCONTROL_REG)));
    RTLOG_DEBUG(moduleName, "Status Reg. = 0x%02X",
        READ_UCHAR((void *)(ccb->can_reg + icSTATUS_REG)));
}
#endif

/**
 * attach_obj() returns the index of a free message object (0..13)
 * or -1 if no message object is available.
 *
 * num < 0: find a free object.
 * num >= 0: try to attach the specified object.
 *
 * This function is used only in exclusive (MT & SMP save) code sections.
 */
static int attach_obj(PTP901_CCB ccb,
    OBJ_TYPE obj_type,
    PRX_QUEUE prx_queue,
    const int num)
{
    if(num < 0)
    {
        int i = 0;

        for(; i < 14; ++i)
        {
            if(ccb->obj_desc[i].occupied == FALSE)
            {
                /* Attach this free object for the job. */
                ccb->obj_desc[i].occupied = TRUE;
                ccb->obj_desc[i].obj_type = obj_type;
                ccb->obj_desc[i].rx_queue = prx_queue;
                return i;
            }
        }
    }
    else
    {
        if(ccb->obj_desc[num].occupied == FALSE)
        {
            /* Attach this free and specified object for the job. */
            ccb->obj_desc[num].occupied = TRUE;
            ccb->obj_desc[num].obj_type = obj_type;
            ccb->obj_desc[num].rx_queue = prx_queue;
            return num;
        }
    }

    /* No message object available. */
    return -1;
}


/**
 * release_obj releases the specified message object with index "num"
 * for new tx- or rx-jobs.
 *
 * This function is used only in exclusive (MT & SMP save) code sections.
 */
static void release_obj(PTP901_CCB ccb, const unsigned int num)
{
    ccb->obj_desc[num].occupied = FALSE;
    ccb->obj_desc[num].obj_type = OBJ_IDLE;
    ccb->obj_desc[num].rx_queue = NULL;
}


/**
 * Read interface without ALMA rtDevDrv framework.
 */
int tpmc901_read(int channel, char* buf, int count)
{
    #ifdef USE_ALMA
    rtlogRecord_t logRecord;
    #endif

    PTP901_CCB ccb = &dcb->channel[channel];
    TP901_MSG_BUF msg_buf;
    unsigned char rx_queue_num;
    PRX_QUEUE prx_queue;
    PCAN_MSG pCanMsg;
    int i, result = 0;

    /**
     * The timeout comes from CAMBServer/src/ambCANIO.c.
     */
    unsigned long timeout;


    #ifdef DEBUG_TPMC901
    RTLOG_DEBUG(moduleName, "tpmc901_read() node %d", ccb->minor);
    #endif


    /* Check message buffer size. */
    if(unlikely(count < sizeof(msg_buf)))
    {
        return -EINVAL;
    }

    /*
     * Get the access sem.
     */
    result = rt_sem_wait_timed(&ccb->readAccessSem, SEMAPHORE_TIMEOUT);
    if(unlikely((result == SEM_ERR) || (result == SEM_TIMOUT)))
    {
        RTLOG_WARNING(moduleName, "Failed to take API semaphore (result = %d)"
            " in function tpmc901_read. Timing problem? Continuing as usual.",
            result);
        rt_sem_wait(&ccb->readAccessSem);
    }

    /* Copy message buffer. */
    memcpy(&msg_buf, buf, sizeof(TP901_MSG_BUF));

    /* Is this a valid receive queue number. */
    rx_queue_num = msg_buf.rx_queue_num;
    if(unlikely((rx_queue_num < 1) || (rx_queue_num > NUM_RX_QUEUES)))
    {
        RTLOG_ERROR(moduleName, "For this message has no valid receive "
            "queue number been given! Should be 1 <= number <= %d, but is %d.",
            NUM_RX_QUEUES, rx_queue_num);
        rt_sem_signal(&ccb->readAccessSem);
        return -ECHRNG;
    }
    --rx_queue_num;

    prx_queue = &ccb->rx_queue[rx_queue_num];

    /**
     * If the CAN-Controller is in Bus OFF state and if the FIFO is empty,
     * we return a error.
     */
    if(unlikely((CONTROLLER_IS_BUS_OFF(ccb) != 0)
    && (prx_queue->get_index == prx_queue->put_index)))
    {
        RTLOG_ERROR(moduleName, "CAN controller is off on channel %d.",
            channel);
        rt_sem_signal(&ccb->readAccessSem);
        return -ECONNREFUSED;
    }

    timeout = msg_buf.timeout;

    /* Loop until new data is available. */
    while(prx_queue->get_index == prx_queue->put_index)
    {
        if(timeout != 0UL)
        {
            result = rt_sem_wait_timed(&ccb->rxSem,
                timeout * nano2count(1000ULL));
            if(unlikely(result == SEM_TIMOUT))
            {
                if(logTimeout == 1U)
                {
                    RTLOG_DEBUG(moduleName, "Timeout on CAN read channel %d.",
                        channel);
                }

                rt_sem_signal(&ccb->readAccessSem);
                return -ETIME;
            }
        }
        else
        {
            result = rt_sem_wait_timed(&ccb->rxSem, SEMAPHORE_TIMEOUT);
            if(unlikely((result == SEM_ERR) || (result == SEM_TIMOUT)))
            {
                RTLOG_WARNING(moduleName, "Failed to take API semaphore (result "
                    "= %d) in function tpmc901_read. Timing problem? Continuing as "
                    "usual.", result);
                rt_sem_wait(&ccb->rxSem);
            }
        }

        /* Check if new data available. */
        if(prx_queue->get_index != prx_queue->put_index)
        {
            break;
        }

        if(unlikely(CONTROLLER_IS_BUS_OFF(ccb) != 0))
        {
            /* It makes no sense to wait because the CAN controller is bus off. */
            RTLOG_ERROR(moduleName, "CAN controller is off on channel %d.",
                channel);
            rt_sem_signal(&ccb->readAccessSem);
            return -ECONNREFUSED;
        }
    }

    pCanMsg = &prx_queue->msg_fifo[prx_queue->get_index];

    /* Transfer data from the FIFO to the user buffer. */
    msg_buf.identifier = pCanMsg->identifier;
    msg_buf.extended = pCanMsg->extended;
    msg_buf.msg_len = pCanMsg->msg_len;
    msg_buf.status = pCanMsg->status;
    for(i = 0; i < pCanMsg->msg_len; ++i)
    {
        msg_buf.data[i] = pCanMsg->data[i];
    }

    /* Calculate next get index. */
    if(++prx_queue->get_index >= RX_FIFO_SIZE)
    {
        prx_queue->get_index = 0;
    }

    /* Copy the current message buffer. */
    memcpy(buf, &msg_buf, sizeof(TP901_MSG_BUF));

    rt_sem_signal(&ccb->readAccessSem);
    return sizeof(TP901_MSG_BUF);
}

/**
 * Write interface without ALMA rtDevDrv framework.
 */
int tpmc901_write(int channel, const char* buf, int count)
{
    #ifdef USE_ALMA
    rtlogRecord_t logRecord;
    #endif

    PTP901_CCB ccb = &dcb->channel[channel];
    TP901_MSG_BUF msg_buf;
    unsigned long obj_base;
    int tx_obj_index, result = 0;
    /**
     * The timeout comes from CAMBServer/src/ambCANIO.c.
     */
    unsigned long timeout;


    #ifdef DEBUG_TPMC901
    RTLOG_DEBUG(moduleName, "tpmc901_write() tpmc901_%d", ccb->minor);
    #endif

    /* Check message buffer size. */
    if(unlikely(count < sizeof(TP901_MSG_BUF)))
    {
        atomic_inc(&numberOfErrorsInInterval);

        return -EINVAL;
    }

    /*
     * Get the access sem.
     */
    result = rt_sem_wait_timed(&ccb->writeAccessSem, SEMAPHORE_TIMEOUT);
    if(unlikely((result == SEM_ERR) || (result == SEM_TIMOUT)))
    {
        RTLOG_WARNING(moduleName, "Failed to take API semaphore (result = "
            "%d) in function tpmc901_write. Timing problem? Continuing as "
            "usual.", result);
       rt_sem_wait(&ccb->writeAccessSem);
    }

    /* Copy message buffer. */
    memcpy(&msg_buf, buf, sizeof(TP901_MSG_BUF));

    timeout = msg_buf.timeout;

    /* If the CAN-Controller is in Bus OFF we return immediately with an error. */
    if(unlikely(CONTROLLER_IS_BUS_OFF(ccb) != 0))
    {
        atomic_inc(&numberOfErrorsInInterval);

        RTLOG_ERROR(moduleName, "CAN controller is off on channel %d.",
            channel);
        rt_sem_signal(&ccb->writeAccessSem);
        return -ECONNREFUSED;
    }

    /* Loop until the transmit buffer is free or this job was canceled. */
    while((tx_obj_index = attach_obj(ccb, TRANSMIT, NULL, -1)) < 0)
    {
        if(timeout != 0UL)
        {
            result = rt_sem_wait_timed(&ccb->txSem, timeout * nano2count(1000ULL));
            if(unlikely(result == SEM_TIMOUT))
            {
                if(logTimeout == 1U)
                {
                    RTLOG_ERROR(moduleName, "Timeout on CAN write channel %d.",
                        channel);
                }

                atomic_inc(&numberOfErrorsInInterval);

                rt_sem_signal(&ccb->writeAccessSem);
                return -ETIME;
            }
        }
        else
        {
            result = rt_sem_wait_timed(&ccb->txSem, SEMAPHORE_TIMEOUT);
            if(unlikely((result == SEM_ERR) || (result == SEM_TIMOUT)))
            {
                RTLOG_WARNING(moduleName, "Failed to take API semaphore "
                    "(result = %d) in function tpmc901_write. Timing problem? "
                    "Continuing as usual.", result);
                rt_sem_wait(&ccb->txSem);
            }
        }

        if(unlikely(CONTROLLER_IS_BUS_OFF(ccb) != 0))
        {
            atomic_inc(&numberOfErrorsInInterval);

            /* It makes no sense to wait because the CAN controller is bus
             * off.
             */
            RTLOG_ERROR(moduleName, "CAN controller is off on channel %d.",
                channel);
            rt_sem_signal(&ccb->writeAccessSem);
            return -ECONNREFUSED;
        }
    }

    obj_base = icOBJ_OFFSET + (icOBJ_SPAN * tx_obj_index);

    /* Init this transmit job. */
    WRITE_UCHAR(ccb->can_reg + obj_base + icOBJ_CONTROL_0, SET_MsgVal &
        SET_TXIE & RESET_RXIE & RESET_IntPnd);
    WRITE_UCHAR(ccb->can_reg + obj_base + icOBJ_CONTROL_1,
        (unsigned char)(RESET_RmtPnd & SET_CPUUpd & SET_NewDat));

    if(likely(msg_buf.extended))
    {
        /* Extended message identifier. */
        WRITE_UCHAR(ccb->can_reg + obj_base + icOBJ_CONFIG,
            (unsigned char)((msg_buf.msg_len<< DLC_Shift) | icDIRTX | icXtd));
        WRITE_ULONG(ccb->can_reg + obj_base + icOBJ_ARBITRATION,
            msg_buf.identifier << 3);
    }
    else
    {
        /* Standard message identifier. */
        WRITE_UCHAR(ccb->can_reg + obj_base + icOBJ_CONFIG,
                 (unsigned char)((msg_buf.msg_len<< DLC_Shift) | icDIRTX));
        WRITE_USHORT(ccb->can_reg + obj_base + icOBJ_ARBITRATION,
                  msg_buf.identifier << 5);
    }

    /* Copy message data into the object data buffer. */
    WRITE_REGION_UCHAR(ccb->can_reg + obj_base + icOBJ_DATA,
        msg_buf.data,
        msg_buf.msg_len);

    /* Mark this message object valid and force transmission. */
    WRITE_UCHAR(ccb->can_reg + obj_base + icOBJ_CONTROL_1,
        SET_TxRqst & RESET_CPUUpd);

    result = count;

    /* Loop until the message was transmitted or this job was canceled. */
    while(READ_UCHAR(ccb->can_reg + obj_base + icOBJ_CONTROL_1) & TEST_TxRqst)
    {
        if(timeout != 0UL)
        {
            result = rt_sem_wait_timed(&ccb->txSem, timeout * nano2count(1000ULL));
            if(unlikely(result == SEM_TIMOUT))
            {
                if(logTimeout == 1U)
                {
                    RTLOG_ERROR(moduleName, "Timeout on CAN write channel %d.",
                        channel);
                }

                result = -ETIME;
                break;
            }
        }
        else
        {
            result = rt_sem_wait_timed(&ccb->txSem, SEMAPHORE_TIMEOUT);
            if(unlikely((result == SEM_ERR) || (result == SEM_TIMOUT)))
            {
                RTLOG_WARNING(moduleName, "Failed to take API semaphore "
                    "(result = %d) in function tpmc901_write. Timing problem? "
                    "Continuing as usual.", result);
                rt_sem_wait(&ccb->txSem);
            }
        }

        if(unlikely(CONTROLLER_IS_BUS_OFF(ccb) != 0))
        {
            /* It makes no sense to wait because the CAN controller is bus
             * off.
             */
            RTLOG_ERROR(moduleName, "CAN controller off on channel %d.",
                channel);
            result = -ECONNREFUSED;
            break;
        }
    }

    if(likely(result >= 0))
    {
        atomic_inc(&numberOfMessagesInInterval);
    }
    else
    {
        atomic_inc(&numberOfErrorsInInterval);
    }

    /* Cancel pending transmit request and release the message object(HW). */
    WRITE_UCHAR(ccb->can_reg + obj_base + icOBJ_CONTROL_0,
        (unsigned char)(RESET_MsgVal & RESET_TXIE & RESET_RXIE & RESET_IntPnd));
    WRITE_UCHAR(ccb->can_reg + obj_base + icOBJ_CONTROL_1,
        (unsigned char)(RESET_TxRqst & RESET_RmtPnd & RESET_CPUUpd & RESET_NewDat));

    /* Release the desired message object(SW). */
    release_obj(ccb, tx_obj_index);

    rt_sem_signal(&ccb->writeAccessSem);

    return result;
}

/**
 * Ioctl interface without ALMA rtDevDrv framework.
 */
int tpmc901_ioctl(int channel, unsigned int cmd, unsigned long arg)
{
    #if defined DEBUG_TPMC901 && defined USE_ALMA
    rtlogRecord_t logRecord;
    #endif

    PTP901_CCB ccb = &dcb->channel[channel];
    TP901_BITTIMING BitTiming;
    TP901_ACCEPT_MASKS AcceptMasks;
    TP901_BUF_DESC buf_desc;
    TP901_STATUS status;
    int i;
    unsigned long obj_base;
    unsigned char msg_obj_num;
    unsigned char rx_queue_num;


    #ifdef DEBUG_TPMC901
    RTLOG_DEBUG(moduleName, "tpmc901_ioctl (cmd = 0x%X)", cmd);
    #endif

    /**
     * Extract the type and number bitfields, and don't decode
     * wrong cmds: return EINVAL before verify_area().
     */
    if(unlikely(_IOC_TYPE(cmd) != TP901_IOC_MAGIC))
    {
        return -EINVAL;
    }

    switch(cmd)
    {
        case TP901_IOCSBITTIMING:
        {
            memcpy(&BitTiming, (void*)arg, sizeof(TP901_BITTIMING));

            rt_disable_irq(dcb->irq);

            /**
             * Controller must be in bus off state to change the bit timing.
             */
            if(unlikely(CONTROLLER_IS_BUS_OFF(ccb) == 0))
            {
                rt_enable_irq(dcb->irq);

                return -EACCES;
            }

            /**
             * Setup timing register.
             */
            WRITE_UCHAR(ccb->can_reg + icBIT_TIMING_0_REG,
                (unsigned char)BitTiming.timing_value);

            if(BitTiming.three_samples)
            {
                WRITE_UCHAR(ccb->can_reg + icBIT_TIMING_1_REG,
                    (unsigned char)((BitTiming.timing_value>>8) | (1<<7)));
            }
            else
            {
                WRITE_UCHAR(ccb->can_reg + icBIT_TIMING_1_REG,
                    (unsigned char)(BitTiming.timing_value>>8));
            }

            rt_enable_irq(dcb->irq);
        }
        break;

        case TP901_IOCSSETFILTER:
        {
            memcpy(&AcceptMasks, (void*)arg, sizeof(TP901_ACCEPT_MASKS));

            rt_disable_irq(dcb->irq);

            WRITE_USHORT(ccb->can_reg + icGLOBAL_MASK_STD,
                AcceptMasks.global_mask_standard);
            WRITE_ULONG(ccb->can_reg + icGLOBAL_MASK_EXT,
                AcceptMasks.global_mask_extended);
            WRITE_ULONG(ccb->can_reg + icMSG_15_MASK,
                AcceptMasks.message_15_mask);

            rt_enable_irq(dcb->irq);
        }
        break;

        case TP901_IOCGGETFILTER:
        {
            rt_disable_irq(dcb->irq);

            AcceptMasks.global_mask_standard =
                READ_USHORT(ccb->can_reg + icGLOBAL_MASK_STD);
            AcceptMasks.global_mask_extended =
                READ_ULONG(ccb->can_reg + icGLOBAL_MASK_EXT);
            AcceptMasks.message_15_mask =
                READ_ULONG(ccb->can_reg + icMSG_15_MASK);

            rt_enable_irq(dcb->irq);

            memcpy((void*)arg, &AcceptMasks, sizeof(TP901_ACCEPT_MASKS));
        }
        break;

        case TP901_IOCBUSON:
        {
            /**
             * Enter Bus On state after software initialization.
             */
            rt_disable_irq(dcb->irq);

            WRITE_UCHAR(ccb->can_reg + icCONTROL_REG,
                (unsigned char)(READ_UCHAR(ccb->can_reg +
                    icCONTROL_REG)& ~icCCE & ~icInit));

            rt_enable_irq(dcb->irq);
        }
        break;

        case TP901_IOCBUSOFF:
        {
            /**
             * Enter Bus Off state.
             */
            rt_disable_irq(dcb->irq);

            if(unlikely(CONTROLLER_IS_BUS_OFF(ccb) != 0))
            {
                rt_enable_irq(dcb->irq);

                break;
            }

            WRITE_UCHAR(ccb->can_reg + icCONTROL_REG,
                (unsigned char)(READ_UCHAR((void*)(ccb->can_reg +
                    icCONTROL_REG))) | icCCE | icInit);

            rt_enable_irq(dcb->irq);
        }
        break;

        case TP901_IOCFLUSH:
        {
            /**
             * Flush the specified or all receive FIFO's.
             */
            if(unlikely(arg >= NUM_RX_QUEUES))
            {
                return -EINVAL;
            }

            rt_disable_irq(dcb->irq);

            if(arg == 0)
            {
                /**
                 * rx_queue_num == 0 mean flush the FIFO of all receive queues.
                 */
                for(i = 0; i < NUM_RX_QUEUES; ++i)
                {
                    ccb->rx_queue[i].get_index = ccb->rx_queue[i].put_index;
                }
            }
            else
            {
                /**
                 * Flush only the FIFO of the desired receive queue.
                 */

                /*
                 * Remember the rx_queue_num from the application is
                 * one-based!
                 */
                ccb->rx_queue[arg-1].get_index =
                    ccb->rx_queue[arg-1].put_index;
            }

            rt_enable_irq(dcb->irq);
        }
        break;

        case TP901_IOCGCANSTATUS:
        {
            /**
             * Return the CAN controller status to the user.
             */
            rt_disable_irq(dcb->irq);

            status.status_reg = READ_UCHAR(ccb->can_reg + icSTATUS_REG);
            status.control_reg = READ_UCHAR(ccb->can_reg + icCONTROL_REG);

            rt_enable_irq(dcb->irq);

            memcpy((void*)(arg), &status, sizeof(TP901_STATUS));
        }
        break;

        case TP901_IOCSDEFRXBUF:
        {
            /**
             * Define a new receive message object.
             */
            memcpy(&buf_desc, (void*)(arg), sizeof(TP901_BUF_DESC));


            /* Valid message object number? */
            msg_obj_num = buf_desc.msg_obj_num;
            if(unlikely((msg_obj_num < 1) || (msg_obj_num > 15)))
            {
                return -EINVAL;
            }

            --msg_obj_num;  /* Application msg_obj_num is one-based! */

            /* Valid receive queue number? */
            rx_queue_num = buf_desc.rx_queue_num;
            if(unlikely((rx_queue_num < 1) || (rx_queue_num > NUM_RX_QUEUES)))
            {
                return -EINVAL;
            }

            --rx_queue_num; /* Application rx_queue_num is one-based! */

            /*
             * Try to allocate the specified message object -> do it
             * atomically.
             */
            rt_disable_irq(dcb->irq);

            if(unlikely(attach_obj(
                ccb, RECV, &ccb->rx_queue[rx_queue_num], msg_obj_num) < 0))
            {
                rt_enable_irq(dcb->irq);
                return -EADDRINUSE;
            }

            rt_enable_irq(dcb->irq);

            obj_base = icOBJ_OFFSET + icOBJ_SPAN * (buf_desc.msg_obj_num - 1);

            WRITE_UCHAR(ccb->can_reg + obj_base + icOBJ_CONTROL_0,
                (unsigned char)(RESET_TXIE & SET_RXIE & RESET_IntPnd));
            WRITE_UCHAR(ccb->can_reg + obj_base + icOBJ_CONTROL_1,
                (unsigned char)(RESET_RmtPnd & RESET_MsgLst & RESET_NewDat));

            if(likely(buf_desc.extended != 0))
            {
                /* extended message identifier. */
                WRITE_UCHAR(ccb->can_reg + obj_base + icOBJ_CONFIG,
                    icXtd);
                WRITE_ULONG(ccb->can_reg + obj_base + icOBJ_ARBITRATION,
                    buf_desc.identifier << 3);
            }
            else
            {
                /* Standard message identifier. */
                WRITE_UCHAR(ccb->can_reg + obj_base + icOBJ_CONFIG, 0);
                WRITE_USHORT(ccb->can_reg + obj_base + icOBJ_ARBITRATION,
                    buf_desc.identifier << 5);
            }

            /* Mark this message object valid. */
            WRITE_UCHAR(ccb->can_reg + obj_base + icOBJ_CONTROL_0,
                SET_MsgVal);
        }
        break;

        case TP901_IOCSDEFRMTBUF:
        {
            /*
             * Define a remote/transmit buffer message object.
             */
            memcpy(&buf_desc, (void*)arg, sizeof(TP901_BUF_DESC));

            /**
             * Valid message object number? Only object 1..14 are valid
             * transmit objects.
             */
            msg_obj_num = buf_desc.msg_obj_num;
            if(unlikely((msg_obj_num < 1) || (msg_obj_num > 14)))
            {
                return -EINVAL;
            }

            --msg_obj_num;  /* Application msg_obj_num is one-based! */

            /* Is the message length correct. */
            if(unlikely(buf_desc.msg_len > 8))
            {
                return -EMSGSIZE;  /* Invalid message size. */
            }

            /**
             * Try to allocate the specified message object.
             * -> Do it atomically.
             */
            rt_disable_irq(dcb->irq);

            if(unlikely(attach_obj(ccb, REMOTE_BUF, NULL, msg_obj_num) < 0))
            {
                rt_enable_irq(dcb->irq);
                return -EADDRINUSE;
            }

            rt_enable_irq(dcb->irq);

            obj_base = icOBJ_OFFSET + icOBJ_SPAN * (buf_desc.msg_obj_num - 1);

            WRITE_UCHAR(ccb->can_reg + obj_base + icOBJ_CONTROL_0,
                SET_MsgVal & RESET_TXIE & RESET_RXIE & RESET_IntPnd);
            WRITE_UCHAR(ccb->can_reg + obj_base + icOBJ_CONTROL_1,
                (unsigned char)(RESET_RmtPnd & SET_CPUUpd & SET_NewDat));

            if(likely(buf_desc.extended != 0))
            {
                /* Extended message identifier. */
                WRITE_UCHAR(ccb->can_reg + obj_base + icOBJ_CONFIG,
                    (buf_desc.msg_len<< DLC_Shift) | icDIRTX | icXtd);
                WRITE_ULONG(ccb->can_reg + obj_base + icOBJ_ARBITRATION,
                    buf_desc.identifier << 3);
            }
            else
            {
                /* Standard message identifier. */
                WRITE_UCHAR(ccb->can_reg + obj_base + icOBJ_CONFIG,
                    (buf_desc.msg_len<< DLC_Shift) | icDIRTX);
                WRITE_USHORT(ccb->can_reg + obj_base + icOBJ_ARBITRATION,
                    buf_desc.identifier << 5);
            }

            /* Copy message data into the object data buffer. */
            WRITE_REGION_UCHAR(ccb->can_reg + obj_base + icOBJ_DATA,
                buf_desc.data, buf_desc.msg_len);

            /* Mark this message object valid. */
            WRITE_UCHAR(ccb->can_reg + obj_base + icOBJ_CONTROL_1, RESET_CPUUpd);
        }
        break;

        case TP901_IOCSUPDATEBUF:
        {
            /**
             * Update a previous defined receive or remote transmit object with
             * new data.
             * If this is a receive object new data was requested by sending
             * a remote frame. In remote transmit object only the data was
             * updated.
             */

            memcpy(&buf_desc, (void*)arg, sizeof(TP901_BUF_DESC));

            /* Valid message object number? */
            msg_obj_num = buf_desc.msg_obj_num;
            if(unlikely(
                (buf_desc.msg_obj_num < 1) || (buf_desc.msg_obj_num > 15)))
            {
                return -EINVAL;
            }

            --msg_obj_num;  /* Application msg_obj_num is one-based! */

            rt_disable_irq(dcb->irq);

            /* Is the message object occupied? */
            if(unlikely(!ccb->obj_desc[msg_obj_num].occupied))
            {
                rt_enable_irq(dcb->irq);

                return -EINVAL;
            }

            obj_base = icOBJ_OFFSET + icOBJ_SPAN * (buf_desc.msg_obj_num - 1);

            switch(ccb->obj_desc[buf_desc.msg_obj_num - 1].obj_type)
            {

                case RECV:
                {
                    /* Request the remote buffer object from an other node. */
                    WRITE_UCHAR(ccb->can_reg + obj_base + icOBJ_CONTROL_1,
                        SET_TxRqst);
                }
                break;

                case REMOTE_BUF:
                {
                    /* Is the message length correct. */
                    if(unlikely(buf_desc.msg_len > 8))
                    {
                        return -EMSGSIZE;  /* invalid message size. */
                    }

                    WRITE_UCHAR(ccb->can_reg + obj_base + icOBJ_CONTROL_1,
                        SET_CPUUpd);

                    /* Setup new data length. */
                    WRITE_UCHAR(ccb->can_reg + obj_base + icOBJ_CONFIG,
                        (READ_UCHAR(ccb->can_reg + icOBJ_CONFIG) & 0xf)
                            | (buf_desc.msg_len<< DLC_Shift));

                    /* Copy message data into the object data buffer. */
                    WRITE_REGION_UCHAR(ccb->can_reg + obj_base + icOBJ_DATA,
                        buf_desc.data, buf_desc.msg_len);

                    WRITE_UCHAR(ccb->can_reg + obj_base + icOBJ_CONTROL_1,
                        RESET_CPUUpd);
                }
                break;

                case TRANSMIT:
                /**
                 * Fall through...
                 */
                case OBJ_IDLE:
                {
                    return -EINVAL;
                }
            }

            rt_enable_irq(dcb->irq);
        }
        break;

        case TP901_IOCTRELEASEBUF:
        {
            memcpy(&buf_desc, (void*)arg, sizeof(TP901_BUF_DESC));

            /* Valid message object number? */
            msg_obj_num = buf_desc.msg_obj_num;
            if(unlikely((msg_obj_num < 1) || (buf_desc.msg_obj_num > 15)))
            {
                return -EINVAL;
            }

            --msg_obj_num;  /* Application msg_obj_num is one-based! */

            rt_disable_irq(dcb->irq);

            /* Is the message object occupied? */
            if(unlikely(
                ccb->obj_desc[buf_desc.msg_obj_num - 1].occupied == 0))
            {
                rt_enable_irq(dcb->irq);

                return -EBADMSG;    /* not a valid message object for release. */
            }

            obj_base = icOBJ_OFFSET + icOBJ_SPAN * (buf_desc.msg_obj_num - 1);

            if(ccb->obj_desc[buf_desc.msg_obj_num - 1].obj_type == TRANSMIT)
            {
                if((READ_UCHAR(ccb->can_reg + obj_base + icOBJ_CONTROL_1)
                    & TEST_TxRqst)
                != 0)
                {
                    return -EBUSY;    /* the message object is busy. */
                }
            }

            WRITE_UCHAR(ccb->can_reg + obj_base + icOBJ_CONTROL_0,
                (unsigned char)(RESET_MsgVal & RESET_TXIE & RESET_RXIE
                    & RESET_IntPnd));
            WRITE_UCHAR(ccb->can_reg + obj_base + icOBJ_CONTROL_1,
                (unsigned char)(RESET_TxRqst & RESET_CPUUpd & RESET_RmtPnd
                    & RESET_NewDat));

            release_obj(ccb, msg_obj_num);

            rt_enable_irq(dcb->irq);
        }
        break;


        default:
        {
            return -EINVAL;
        }
    }

    return 0;
}

int tpmc901_readInterface(void* genericInputData,
    void* bufferToWrite,
    const size_t numberOfBytes,
    const RTIME timeoutNS)
{
    return(tpmc901_read(*(int*)genericInputData,
        (char*)bufferToWrite,
        numberOfBytes));
}


int tpmc901_writeInterface(void* genericInputData,
    void* bufferToWrite,
    const size_t numberOfBytes,
    const RTIME timeoutNS)
{
    return(tpmc901_write(*(int*)genericInputData,
        (char*)bufferToWrite,
        numberOfBytes));
}


int tpmc901_ioctlInterface(const int request, void* configData)
{
    return(tpmc901_ioctl(((tpmc901IOCTLStruct*)configData)->channel,
        request,
        ((tpmc901IOCTLStruct*)configData)->arg));
}


/**
 * Exported API.
 */

/**
 * API for non ALMA use.
 */
EXPORT_SYMBOL(tpmc901_read);
EXPORT_SYMBOL(tpmc901_write);
EXPORT_SYMBOL(tpmc901_ioctl);
/**
 * API for ALMA use with the rtDevDrv framework.
 */
EXPORT_SYMBOL(tpmc901_readInterface);
EXPORT_SYMBOL(tpmc901_writeInterface);
EXPORT_SYMBOL(tpmc901_ioctlInterface);


/**
 * This is the tpmc901 driver's generic interrupt routine.
 */
static void tpmc901_interrupt(void)
{
    PTP901_CCB ccb;
    PRAW_CAN_MSG pRawCanMsg;
    unsigned char InterruptReg;
    unsigned char global_int_reg;
    unsigned char msg_obj_num;
    unsigned long obj_base;
    int i, ch;
    PCAN_MSG pCanMsg;
    PRX_QUEUE prx_queue;

    #ifdef DEBUG_TPMC901_INTR
    RTLOG_DEBUG_IRQ(moduleName, "IRQ");
    #endif

    #if defined USE_ALMA && defined DEBUG_IRQS
    debugResetChannel(resetChannelForDebug, 0xff);
    #endif

    /**
     * The status register for pending IRQs on the TPMC901 shows pending
     * IRQs with the bit set to 0! Bits 6 & 7 are always set to 1.
     */
    global_int_reg = (~READ_UCHAR(dcb->io_addr + TPMC901_STATREG))
        & TPMC901_IRQBitMask;

    if(likely(global_int_reg != 0))
    {
        do
        {
            #ifdef DEBUG_TPMC901_INTR
            RTLOG_DEBUG_IRQ(moduleName, "IRQ Status = 0x%02X (1 = pending)",
                global_int_reg);
            #endif

            for(ch = 0; ch < dcb->channel_count; ++ch)
            {
                ///
                /// TODO
                /// Thomas, Aug 25, 2010
                ///
                /// Likely OK?
                if(likely((global_int_reg & (1 << ch)) == 0))
                {
                    continue;
                }

                ccb = &dcb->channel[ch];
                InterruptReg = READ_UCHAR(ccb->can_reg + icINTERRUPT_REG);
                #ifdef DEBUG_TPMC901_INTR
                RTLOG_DEBUG_IRQ(moduleName, "IRQ tpmc901_%d  IReg = 0x%02X",
                    ccb->minor, InterruptReg);
                #endif

                switch(InterruptReg)
                {
                    case 0:
                    {
                        /* ...no interrupt pending, nothing to do for this channel. */
                    }
                    break;

                    case 1:
                    {
                        /**
                         * Status change interrupt.
                         */
                        /* Process only bus off errors. */
                        if(likely((READ_UCHAR(
                            ccb->can_reg + icSTATUS_REG) & icBOff) != 0))
                        {
                            atomic_inc(&(busOffFlag[ch].flag));
#ifdef USE_ALMA
                            busOffFlag[ch].lastTimeStamp =
                                rt_get_real_time_ns();
#else
                            struct timespec now = {0, 0};
                            getnstimeofday(&now);
                            busOffFlag[ch].lastTimeStamp =
                                ((s64) now.tv_sec * NSEC_PER_SEC)
                                + now.tv_nsec;
#endif
                            /**
                             * Wakeup all currently pending read/write tasks.
                             */
                            rt_sem_signal(&ccb->rxSem);
                            rt_sem_signal(&ccb->txSem);
                        }
                        /* If >= 96 errors happened on tje CAN-bus. */
                        else if(unlikely((READ_UCHAR(
                            ccb->can_reg + icSTATUS_REG) & icWarn) != 0))
                        {
                            atomic_inc(&(warnFlag[ch].flag));
#ifdef USE_ALMA
                            warnFlag[ch].lastTimeStamp =
                                rt_get_real_time_ns();
#else
                            struct timespec now = {0, 0};
                            getnstimeofday(&now);
                            warnFlag[ch].lastTimeStamp =
                                ((s64) now.tv_sec * NSEC_PER_SEC)
                                + now.tv_nsec;
#endif
                        }
                    }
                    break;

                    case 2:
                    {
                        /**
                         * Message object 15 interrupt.
                         *
                         * Correct the order of message object numbers in the
                         * interrupt register from 15, 1, 2, ..., 14 to
                         * 1, 2, ..., 15 so we can simply subtract 3 to get the
                         * message address offset.
                         */
                        InterruptReg = 0x11;
                        /* ...continue with the default part...*/
                    }

                    default:
                    {
                        /**
                         * Process interrupt of all message objects.
                         */
                        msg_obj_num = InterruptReg - 3;

                        obj_base = icOBJ_OFFSET + icOBJ_SPAN * msg_obj_num;

                        switch(ccb->obj_desc[msg_obj_num].obj_type)
                        {
                            case TRANSMIT:
                            {
                                #ifdef DEBUG_TPMC901_INTR
                                RTLOG_DEBUG_IRQ(moduleName, "IRQ tpmc901_%d "
                                    "TRANSMIT", ccb->minor);
                                #endif

                                /**
                                 * The standard transmit object has finished
                                 * transmission. Queue the transmit DPC for
                                 * post-processing.
                                 */
                                WRITE_UCHAR(ccb->can_reg + obj_base
                                    + icOBJ_CONTROL_0, RESET_IntPnd);

                                /*
                                 * Wakeup all currently waiting "transmit"
                                 * tasks.
                                 */
                                rt_sem_signal(&ccb->txSem);
                            }
                            break;

                            case RECV:
                            {
                                #ifdef DEBUG_TPMC901_INTR
                                RTLOG_DEBUG_IRQ(moduleName, "IRQ tpmc901_%d "
                                    "RECEIVE", ccb->minor);
                                #endif

                                /**
                                 * A receive object has received a new message.
                                 */
                                pRawCanMsg =
                                    &ccb->rx_raw_fifo[ccb->rx_raw_put_index];
                                pRawCanMsg->status = TP901_SUCCESS;

                                /**
                                 * Read arbitration, message config and all
                                 * necessary data bytes.
                                 */
                                pRawCanMsg->msg_obj_num = msg_obj_num;
                                pRawCanMsg->arbitration =
                                    READ_ULONG(ccb->can_reg + obj_base
                                        + icOBJ_ARBITRATION);
                                pRawCanMsg->msg_conf_reg =
                                    READ_UCHAR(ccb->can_reg + obj_base
                                    + icOBJ_CONFIG);

                                for(i = 0;i < (
                                    pRawCanMsg->msg_conf_reg >> DLC_Shift); ++i)
                                {
                                    pRawCanMsg->data[i] =
                                        READ_UCHAR(ccb->can_reg + obj_base +
                                            icOBJ_DATA + i);
                                }
/* The READ_REGION_UCHAR function does not work properly inside this ISR in
 * some computers (notably dual Opteron computers) so it has been commented
 * out and replaced by the loop above that does the same thing.
 * JPerez 2006-AUG-09
 */
/**
                                READ_REGION_UCHAR(
                                    ccb->can_reg + obj_base + icOBJ_DATA,
                                    pRawCanMsg->data,
                                    pRawCanMsg->msg_conf_reg >> DLC_Shift);
*/

                                if(unlikely(
                                    (READ_UCHAR(ccb->can_reg + obj_base +
                                    icOBJ_CONTROL_1) & TEST_MsgLst)
                                != 0))
                                {
                                    pRawCanMsg->status = TP901_MSGOBJ_OVERRUN;
                                }

                                /*
                                 * Reset interrupt pending and release the
                                 * object.
                                 */
                                WRITE_UCHAR(ccb->can_reg + obj_base +
                                    icOBJ_CONTROL_0,
                                    (unsigned char)(RESET_IntPnd));
                                WRITE_UCHAR(ccb->can_reg + obj_base +
                                    icOBJ_CONTROL_1,
                                    (unsigned char)(RESET_RmtPnd &
                                        RESET_MsgLst & RESET_NewDat));

                                /* Adjust RAW FIFO get index. */
                                if(unlikely(++(ccb->rx_raw_put_index) >=
                                    RAW_CAN_FIFO_SIZE))
                                {
                                    ccb->rx_raw_put_index = 0;
                                }

                                /*
                                 * Check for data overrun in the raw data
                                 * FIFO.
                                 */
                                if(unlikely(ccb->rx_raw_put_index ==
                                    ccb->rx_raw_get_index))
                                {
                                    if(unlikely(++(ccb->rx_raw_get_index) >=
                                        RAW_CAN_FIFO_SIZE))
                                    {
                                        ccb->rx_raw_get_index = 0;
                                    }

                                    ccb->rx_raw_fifo[ccb->rx_raw_get_index].status |=
                                        TP901_RAW_FIFO_OVERRUN;
                                }

                                /**
                                 * What comes here is the bottom half of the
                                 * interrupt routine loop as long as data in
                                 * the raw data FIFO.
                                 */
                                while(ccb->rx_raw_get_index !=
                                    ccb->rx_raw_put_index)
                                {
                                    /**
                                     * Calculate pointer to the oldest can
                                     * message in the raw data FIFO.
                                     */
                                    pRawCanMsg =
                                        &ccb->rx_raw_fifo[ccb->rx_raw_get_index];

                                    /**
                                     * Get the pointer to the corresponding
                                     * receive queue.
                                     */
                                    prx_queue = ccb->
                                        obj_desc[pRawCanMsg->msg_obj_num].rx_queue;

                                    if(likely(prx_queue != 0))
                                    {
                                        pCanMsg = &prx_queue->msg_fifo[
                                            prx_queue->put_index];

                                        pCanMsg->extended =
                                            (pRawCanMsg->msg_conf_reg & icXtd) != 0;
                                        pCanMsg->msg_len =
                                            pRawCanMsg->msg_conf_reg >> DLC_Shift;
                                        pCanMsg->status = pRawCanMsg->status;

                                        if(likely(pCanMsg->extended != 0))
                                        {
                                            pCanMsg->identifier =
                                                pRawCanMsg->arbitration >> 3;
                                        }
                                        else
                                        {
                                            pCanMsg->identifier =
                                                pRawCanMsg->arbitration >> 21;
                                        }

                                        /*
                                         * Copy message data (up to 8 bytes).
                                         */
                                        for(i = 0; i < pCanMsg->msg_len; ++i)
                                        {
                                            pCanMsg->data[i] =
                                                pRawCanMsg->data[i];
                                        }

                                        /* Adjust FIFO put index. */
                                        if(unlikely(++prx_queue->put_index >=
                                            RX_FIFO_SIZE))
                                        {
                                            prx_queue->put_index = 0;
                                        }

                                        /* Check for data overrun. */
                                        if(unlikely(prx_queue->put_index ==
                                            prx_queue->get_index))
                                        {
                                            /**
                                             * Delete the oldest FIFO entry
                                             * and set error status.
                                             */
                                            if(unlikely(
                                                ++prx_queue->get_index >=
                                                RX_FIFO_SIZE))
                                            {
                                                prx_queue->get_index = 0;
                                            }

                                            prx_queue->msg_fifo[prx_queue->get_index].status |=
                                                    TP901_FIFO_OVERRUN;
                                        }
                                    }

                                    /* Adjust RAW FIFO get index. */
                                    if(unlikely(++(ccb->rx_raw_get_index) >=
                                        RAW_CAN_FIFO_SIZE))
                                    {
                                        ccb->rx_raw_get_index = 0;
                                    }

                                    /* Wakeup all pending read jobs for this queue. */
                                    rt_sem_signal(&ccb->rxSem);
                                }
                            }
                            break;

                              case REMOTE_BUF:
                              {
                                #ifdef DEBUG_TPMC901_INTR
                                  RTLOG_DEBUG_IRQ(moduleName, "IRQ tpmc901_%d "
                                      "REMOTE", ccb->minor);
                                #endif

                                /**
                                 * This is an abnormal condition because
                                 * remote/transmit objects do not generate
                                 * interrupts. This should never happen.
                                 */

                                /**
                                 * Reset interrupt pending and disable all
                                 * interrupts for this object.
                                 */
                                WRITE_UCHAR(ccb->can_reg + obj_base +
                                    icOBJ_CONTROL_0, RESET_TXIE & RESET_RXIE
                                        & RESET_IntPnd);
                              }
                            break;

                              case OBJ_IDLE:
                              {
                                #ifdef DEBUG_TPMC901_INTR
                                  RTLOG_DEBUG_IRQ(moduleName, "IRQ tpmc901_%d"
                                      "IDLE", ccb->minor);
                                #endif

                                /**
                                   * This is also an abnormal condition because
                                   * the object is idle and can't generate
                                   * interrupts. This should never happen.
                                   */

                                  /**
                                   * Reset interrupt pending and disable all
                                   * interrupts for this object.
                                   */
                                  WRITE_UCHAR(ccb->can_reg + obj_base +
                                      icOBJ_CONTROL_0,
                                      RESET_TXIE & RESET_RXIE & RESET_IntPnd);
                              }
                            break;
                        }
                    }
                }
            }
        }
        while((global_int_reg =
            (~READ_UCHAR(dcb->io_addr + TPMC901_STATREG)) & TPMC901_IRQBitMask)
        != 0);

        /**
         * The internal interrupt register requires a short delay for update.
         */
        rt_busy_sleep(INTERRUPT_SLEEP);

        rt_ack_irq(dcb->irq);
    }
    else
    {
        rt_ack_irq(dcb->irq);
        rt_pend_linux_irq(dcb->irq);
    }

    #if defined USE_ALMA && defined DEBUG_IRQS
    debugResetChannel(resetChannelForDebug, 0x0);
    #endif
}

static irqreturn_t linuxPostIRQHandler(
    int irq,
    void* devId,
    struct pt_regs* regs)
{
    rt_enable_irq(irq);

    return IRQ_HANDLED;
}

/**
 * Initialize the specified TPMC901 device. Called after this
 * driver had been registered to the kernel for certain PCI devices.
 */
static int __devinit tpmc901_probe(struct pci_dev* dev,
    const struct pci_device_id* id)
{
    #ifdef USE_ALMA
    rtlogRecord_t logRecord;
    #endif

    int pci_enable_ret = 0;
    PTP901_CCB ccb;
    int i, ii, ch;
    unsigned long res_len;
    unsigned char ZeroBuf[icOBJ_RAW_SIZE];

    #ifdef DEBUG_TPMC901
    RTLOG_DEBUG(moduleName, "Probe new device (vendor = 0x%04X, device "
        "= 0x%04X, type = %ld)",
        id->vendor,
        id->device,
        id->driver_data);
    #endif

    if(dcb->dev == dev)
    {
        RTLOG_WARNING(moduleName, "The device has already been initialised "
            "for this TPMC901 module. No action taken.");

        return -EBUSY;
    }
    else if(dcb->dev != 0)
    {
        RTLOG_WARNING(moduleName, "The device has already been initialised "
            "for a different TPMC901 module. The driver supports one TPMC901 "
            "module only! No action taken.");

        return -EBUSY;
    }
    else
    {
        memset(dcb, 0, sizeof(TP901_DCB));
    }

    /**
     * Make pci device available for access. The return value must be != 0 in
     * case of success. Otherwise the PCI subsystem screwed things up and
     * there is no good reason anymore to continue.
     */
    if((pci_enable_ret = pci_enable_device(dev)) != 0)
    {
        RTLOG_ERROR(moduleName, "Could not enable the device; the device is unsusable!");

        return pci_enable_ret;
    }
    else
    {
        dcb->dev = dev;
    }

    /**
     * Try to occupy memory resource of the TPMC901.
     */
    if(pci_request_regions(dev, moduleName) != 0)
    {
        RTLOG_ERROR(moduleName, "Could not acquire PCI memory. Aborting!");

        cleanup_device(FALSE);

        return -EBUSY;
    }

    /**
     * Read the base addresses for the I/O-mapping.
     */
    dcb->bar[TPMC901_MEM_BAR_NUM] = pci_resource_start(dev,
        TPMC901_MEM_BAR_NUM);
    res_len = pci_resource_len(dev, TPMC901_MEM_BAR_NUM);

    if(dcb->bar[TPMC901_MEM_BAR_NUM] == 0)
    {
        RTLOG_ERROR(moduleName, "BAR[%d] memory resource already "
            "occupied!?", TPMC901_MEM_BAR_NUM);

        cleanup_device(FALSE);

        return -ENOMEM;
    }
    else
    {
        /**
         * O.k., the related memory is dirty. Clean up after we left!
         */
        atomic_set(&cleanedUp, FALSE);

        dcb->mapped_mem_addr = ioremap(dcb->bar[TPMC901_MEM_BAR_NUM],
            res_len);

        RTLOG_INFO(moduleName, "I/O-memory has successfully been mapped into "
            "memory.");
    }

    /* Determine module type .*/
    switch(res_len)
    {
        case TPMC901_10:
        {
            dcb->channel_count = TPMC901_10_CHANNEL_COUNT;
        }
        break;

        case TPMC901_11:
        {
            dcb->channel_count = TPMC901_11_CHANNEL_COUNT;
        }
        break;

        case TPMC901_12:
        {
            dcb->channel_count = TPMC901_12_CHANNEL_COUNT;
        }
        break;

        default:
        {
            #ifdef DEBUG_TPMC901
            RTLOG_DEBUG(moduleName, "Can't determine the TPMC901 variant and "
                "number of channels");
            #endif

            dcb->channel_count = TPMC901_12_CHANNEL_COUNT;   /* use default */
        }
        break;
    }

    /**
     * Set the bitmask for the interrupt register.
     */
    TPMC901_IRQBitMask = ((1U << dcb->channel_count) - 1U);

    /**
     * Try to occupy I/O resource of the TPMC901.
     */
    dcb->bar[TPMC901_IO_BAR_NUM] = pci_resource_start(dev,
        TPMC901_IO_BAR_NUM);
    res_len = pci_resource_len(dev, TPMC901_IO_BAR_NUM);

    if(dcb->bar[TPMC901_IO_BAR_NUM] == 0)
    {
        RTLOG_ERROR(moduleName, "BAR[%d] I/O resource already occupied!?",
            TPMC901_IO_BAR_NUM);

        cleanup_device(FALSE);

        return -EBUSY;
    }
    else
    {
        /**
         * Map I/O ports to memory to have the same access functionality as
         * with mapped memory regions.
         */
        dcb->io_addr = ioport_map(dcb->bar[TPMC901_IO_BAR_NUM], res_len);

        RTLOG_INFO(moduleName, "I/O-ports have successfully been mapped to "
            "memory.");
    }

    /**
     * Because the clock output (CLKOUT) of controller (n) is wired to clock input
     * (XTAL1) of controller (n+1) we have to initialize the clock output of
     * controller (n) before we can access controller (n+1).
     */
    for(ch = 0; ch < dcb->channel_count; ++ch)
    {
        void* can_reg = dcb->mapped_mem_addr + ch * TPMC901_CHANNEL_SPAN;

        WRITE_UCHAR(can_reg + icCONTROL_REG, icCCE | icEIE | icInit);
        WRITE_UCHAR(can_reg + icINTERFACE_REG, icCEn);
        WRITE_UCHAR(can_reg + icCLKOUT_REG, 0x20);
    }

    for(ch = 0; ch < dcb->channel_count; ++ch)
    {
        ccb = &dcb->channel[ch];

        /* Initialize CCB structure. */
        ccb->can_reg = dcb->mapped_mem_addr + ch * TPMC901_CHANNEL_SPAN;
        ccb->minor = ch;

        /* Initialize receive and transmit semaphore. */
        rt_typed_sem_init(&ccb->rxSem, 0, BIN_SEM);
        rt_sem_init(&ccb->txSem, 1);

        /* Initialize read and write access semaphore. */
        rt_sem_init(&ccb->readAccessSem, 1);
        rt_sem_init(&ccb->writeAccessSem, 1);

        /**
         * Initialize this CAN channel.
         */
        WRITE_UCHAR(ccb->can_reg + icSTATUS_REG, 0x07);
        WRITE_USHORT(ccb->can_reg + icGLOBAL_MASK_STD, 0xFFFF);
        WRITE_ULONG(ccb->can_reg + icGLOBAL_MASK_EXT, 0xFFFFFFFF);
        WRITE_ULONG(ccb->can_reg + icMSG_15_MASK, 0xFFFFFFFF);
        WRITE_UCHAR(ccb->can_reg + icBUS_CONFIG_REG, icDcT1);
        WRITE_UCHAR(ccb->can_reg + icBIT_TIMING_0_REG, 0);
        WRITE_UCHAR(ccb->can_reg + icBIT_TIMING_1_REG, 0);

        memset(ZeroBuf, 0x00, icOBJ_RAW_SIZE);

        for(ii = 0; ii < MAX_CAN_OBJ; ++ii)
        {
            WRITE_UCHAR(ccb->can_reg + icOBJ_OFFSET + icOBJ_SPAN * ii +
                icOBJ_CONTROL_0, (unsigned char)(RESET_MsgVal & RESET_TXIE &
                    RESET_RXIE & RESET_IntPnd));
            WRITE_UCHAR(ccb->can_reg + icOBJ_OFFSET + icOBJ_SPAN * ii +
                      icOBJ_CONTROL_1, (unsigned char)(RESET_RmtPnd &
                          RESET_TxRqst & RESET_MsgLst & RESET_NewDat));

            /* Init Arbitration, Message Config and Data with 0. */
            WRITE_VALUE_TO_REGION_UCHAR(ccb->can_reg + icOBJ_OFFSET +
                icOBJ_SPAN * ii + icOBJ_RAW_DATA, 0, icOBJ_RAW_SIZE);

            /* Release this message object. */
            release_obj(ccb, ii);
        }

        /* enable interrupts to this minor device. */
        WRITE_UCHAR(ccb->can_reg + icCONTROL_REG, READ_UCHAR(ccb->can_reg +
            icCONTROL_REG) | icIE);

    } /* End for(ch ...). */

    /**
     * Register the ISR for this device.
     */
    i = rt_request_global_irq(dev->irq, tpmc901_interrupt);
    if(i != 0)
    {
        RTLOG_ERROR(moduleName, "Could not allocate RT-interrupt (IRQ = %d).",
              dev->irq);

        cleanup_device(FALSE);

        return -EPERM;
    }
    else if((i = rt_request_linux_irq(dev->irq,
        linuxPostIRQHandler,
        moduleName,
        dev)) != 0)
    {
        RTLOG_ERROR(moduleName, "Could not allocate Linux interrupt "
            "(IRQ = %d).", dev->irq);

        cleanup_device(FALSE);

        return -EPERM;
    }
    else
    {
        rt_startup_irq(dev->irq);

        dcb->irq = dev->irq;

        RTLOG_INFO(moduleName, "IRQ has successfully been allocated (IRQ = "
            "%d).", dcb->irq);
    }

    SEMAPHORE_TIMEOUT = nano2count(semaphoreTimeout * 1000ULL);

    #ifdef USE_ALMA
    i = rtRegisterDriver(TPMC901_RT_DEV_DRV_NAME,
        tpmc901_readInterface,
        tpmc901_writeInterface,
        tpmc901_ioctlInterface,
        0LL,
        1);
    if(likely(i == 0))
    {
        RTLOG_INFO(moduleName, "Driver is registered in the rtDevDrv framework.");
    }
    else
    {
        RTLOG_INFO(moduleName, "rtRegisterDriver returned %d. "
            "Driver could not be registered in the rtDevDrv framework!",
            i);
    }
    #endif

    return 0;
}


/**
 * Remove the specified TPMC901 device from the list of registered PCI
 * drivers.
 */
static void __devexit tpmc901_remove(struct pci_dev* dev)
{
    #ifdef DEBUG_TPMC901
        #ifdef USE_ALMA
        rtlogRecord_t logRecord;
        #endif

    RTLOG_DEBUG(moduleName, "Remove device (vendor = 0x%04X, device = "
        "0x%04X)", dev->vendor, dev->device);
    #endif

    cleanup_device(TRUE);
}


/**
 * Release resources allocated by the specified TPMC901 device. Usually called
 * a) automotically from tpmc901_remove or
 * b) from tpmc901_exit (Exit Module).
 */
static void cleanup_device(const int regularCleanUp)
{
    #ifdef USE_ALMA
    rtlogRecord_t logRecord;
    #endif

    int ch;
    PTP901_CCB ccb;

    #ifdef USE_ALMA
    if(rtUnregisterDriver(TPMC901_RT_DEV_DRV_NAME) == 0)
    {
        RTLOG_INFO(moduleName, "Driver is unregistered in rtDevDrv framework.");
    }
    else
    {
        if(regularCleanUp == TRUE)
        {
            RTLOG_ERROR(moduleName, "Could not unregister the driver in the "
                "rtDevDrv framework. Continuing with the clean up anyway.");
        }
        else
        {
            RTLOG_DEBUG(moduleName, "Could not unregister the driver in the "
                "rtDevDrv framework. This is related to a failure in the "
                "initialisation process. Continuing with the clean up anyway.");
        }
    }
    #endif

    /**
     * Security check, that an already unavailable device is not freed twice.
     */
    if(atomic_read(&cleanedUp) != FALSE)
    {
        RTLOG_INFO(moduleName, "Nothing to clean up, device had never been "
            "initialised.");

        return;
    }

    /**
     * Shutdown the IRQ to make clean-up safe.
     */
    if(dcb->irq != 0)
    {
        rt_free_linux_irq(dcb->irq, dcb->dev);
        rt_free_global_irq(dcb->irq);

        dcb->irq = 0;

        RTLOG_INFO(moduleName, "IRQs successfully released.");
    }
    else
    {
        RTLOG_WARNING(moduleName, "No IRQs to release? Nothing done.");
    }

    /**
     * Remove nodes, clean channels up.
     */
    for(ch = dcb->channel_count - 1; ch >= 0; --ch)
    {
        ccb = &dcb->channel[ch];
        if(ccb != 0)
        {
            rt_sem_delete(&ccb->rxSem);
            rt_sem_delete(&ccb->txSem);
            rt_sem_delete(&ccb->readAccessSem);
            rt_sem_delete(&ccb->writeAccessSem);

            RTLOG_INFO(moduleName, "Node %d removed.", dcb->channel[ch].minor);
        }
        else
        {
            RTLOG_WARNING(moduleName, "Channel/node %d seems to be uninitialised. "
                "No action taken and continuing  with the clean-up.",
                dcb->channel[ch].minor);
        }
    }

    /**
     * Clean the mapped I/O-ports up.
     */
    if(dcb->io_addr != 0)
    {
        ioport_unmap(dcb->io_addr);
        dcb->io_addr = 0;

        RTLOG_INFO(moduleName, "I/O-ports successfully released.");
    }
    else
    {
        RTLOG_WARNING(moduleName, "No I/O-ports to release? Nothing done.");
    }

    /**
     * Clean the mapped I/O-memory up.
     */
    if(dcb->mapped_mem_addr != 0)
    {
        iounmap(dcb->mapped_mem_addr);
        dcb->mapped_mem_addr = 0;

        RTLOG_INFO(moduleName, "Memory successfully released.");
    }
    else
    {
        RTLOG_WARNING(moduleName, "No memory to release? Nothing done.");
    }

    /**
     * Releaase the mapped I/O-ports and I/O-memory.
     */
    if(dcb->dev != 0)
    {
        pci_release_regions(dcb->dev);
        dcb->bar[TPMC901_MEM_BAR_NUM] = 0;
        dcb->bar[TPMC901_IO_BAR_NUM] = 0;
        RTLOG_INFO(moduleName, "PCI-memory & -I/O-ports successfully released.");

        /**
         * Tell kernel, that this driver does not access any memory or I/Os
         * after this point.
         */
        pci_disable_device(dcb->dev);
        dcb->dev = 0;
    }

    /**
     * Clean-up was successful.
     */
    atomic_set(&cleanedUp, TRUE);

    #ifdef DEBUG_TPMC901
    RTLOG_DEBUG(moduleName, "cleanup_device");
    #endif

    return;
}


/**
 * The following definitions and the array tpmc901_table[] describes the PCI
 * devices we are interested in.
 */
static struct pci_device_id __devinitdata tpmc901_table[] =
{
    {
        PCI_DEVICE(TPMC901_VENDOR_ID, TPMC901_DEVICE_ID),
        .subvendor = TPMC901_SUBVENDOR_ID,
        .subdevice = TPMC901_SUBDEVICE_ID,
        .class = PCI_CLASS_NETWORK_OTHER << 8,
        .class_mask = 0xffff00,
        .driver_data = 10
    },
    /* TPMC901-10, -11, -12 */
    {
        0,
    }
};

MODULE_DEVICE_TABLE(pci, tpmc901_table);


/**
 * The 'tpmc901_driver' structure describes our driver and provide the PCI
 * subsystem with callback function pointer. The id_table contains PCI device
 * ID's of devices we are interested in.
 */
static struct pci_driver tpmc901_driver =
{
    .name = DRIVER_NAME,
    .id_table = tpmc901_table,
    .probe = tpmc901_probe,
    .remove = tpmc901_remove,
};



/**
 * Module initialization function.
 *
 * This module will be called during module initialization. The init function
 * allocates necessary resources and initializes internal data structures.
 */
static int __init tpmc901_init(void)
{
    #ifdef USE_ALMA
    rtlogRecord_t logRecord;
    #endif

    int status = -ENODEV;
    unsigned short channel = 0U;

    #ifdef USE_ALMA
    RTLOG_INFO(moduleName, "%s", softwareVersion);
    #endif
    RTLOG_INFO(moduleName, "%s, version %s (%s)",
        DRIVER_NAME, DRIVER_VERSION, DRIVER_REVDATE);

    INTERRUPT_SLEEP = interruptSleep;
    RTLOG_INFO(moduleName, "The IRQ handler will sleep %dns after each IRQ.",
        interruptSleep);

    if(logTimeout == 0U)
    {
        RTLOG_INFO(moduleName, "No timeout will be reported.");
    }

    #if defined USE_ALMA && defined DEBUG_IRQS
    RTLOG_DEBUG(moduleName, "Debugging the IRQ handler is enabled. The reset pin"
        "on channel %u will be triggered on entering/leaving the handler.",
        resetChannelForDebug);
    #endif

    /* Initialise the time stamp for the proc_fs page. */
    #ifdef USE_ALMA
    startTimeStamp = rt_get_real_time_ns();
    #else
    struct timespec now = {0, 0};
    getnstimeofday(&now);
    startTimeStamp = ((s64) now.tv_sec * NSEC_PER_SEC) + now.tv_nsec;
    #endif
    startIntervalTimeStamp = startTimeStamp;

    for(; channel < 6U; ++channel)
    {
        atomic_set(&(busOffFlag[channel].flag), 0);
        busOffFlag[channel].lastTimeStamp = 0LL;
        atomic_set(&(warnFlag[channel].flag), 0);
        warnFlag[channel].lastTimeStamp = 0LL;
    }

    status = pci_register_driver(&tpmc901_driver);

    if(status == 0)
    {
        procEntry = create_proc_entry(moduleName, S_IRUGO | S_IWUSR, NULL);
        if(procEntry == 0)
        {
            RTLOG_DEBUG(moduleName, "Failed to register the proc FS entry.");
        }
        else
        {
            procEntry->read_proc = readProcFsPage;
        }
    }

    #ifdef USE_ALMA
    if(status == 0)
    {
        RTLOG_INFO(moduleName, "Module initialized successfully.");
    }
    else
    {
        RTLOG_ERROR(moduleName, "Failed to initialize module!");
    }
    #endif

    return status;
}


/**
 * Module cleanup.
 *
 * This module will be called before module removal. The cleanup function
 * free all allocated resources.
 */
static void __exit tpmc901_exit(void)
{
    #ifdef USE_ALMA
    rtlogRecord_t logRecord;
    #endif

    remove_proc_entry(moduleName, NULL);
    pci_unregister_driver(&tpmc901_driver);

    RTLOG_INFO(moduleName, DRIVER_NAME " device driver removed.");

    #ifdef USE_ALMA
    RTLOG_INFO(moduleName, "Module cleaned up successfully.");
    #endif
}

module_init(tpmc901_init);
module_exit(tpmc901_exit);
