#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# ramestic  2005-02-17  created
#

#************************************************************************
#   NAME
# 
#   SYNOPSIS
# 
#   DESCRIPTION
#
#   FILES
#
#   ENVIRONMENT
#
#   RETURN VALUES
#
#   CAUTIONS
#
#   EXAMPLES
#
#   SEE ALSO
#
#   BUGS     
#
#------------------------------------------------------------------------
#

from time import sleep
from Acspy.Common.TimeHelper import getTimeStamp
from Acspy.Clients.SimpleClient import PySimpleClient
from Correlator import ArrayTime
from Control import TimeSource

simpleClient = PySimpleClient()

try:
    print "Testing ArrayTime"

    # get refernces to involved components (note that TimeSource would internally
    # get a refernce to a clock object)
    arrayTime = simpleClient.getComponent("CORR_ARRAY_TIME")
    timeSource = simpleClient.getComponent("CONTROL_TIMESOURCE")

    #
    # give some time for the components to be up and running
    #
    sleep(10)

    #
    # printout properties
    #
    print arrayTime._get_name() +\
          ': ' +\
          arrayTime._get_mode().get_sync()[0].__str__() +\
          ' ' +\
          arrayTime._get_type().get_sync()[0].__str__()
    
    #
    # check current synch status (it must by unsynch)
    #
    if arrayTime.isSynchronized():
        print "ERROR: ArrayTime is already synchronized!"
    else:
        print "ArrayTime is not synchronized"

    #
    # command ArrayTime to synchronize to TimeSource
    #
    arrayTime.synchronizeTime(timeSource)
    print "synchronization method completed successfully";
        
    #
    # check current synch status (it must by synch)
    #
    if arrayTime.isSynchronized():
        print "ArrayTime is synchronized"
    else:
        print "ERROR: ArrayTime reports no synchronization after successfull synchronization!"

    #
    # properties are updated every second, therefore, give 1 second
    # before reading them again
    #
    sleep(1)
    
    #
    # printout properties again
    #
    print arrayTime._get_name() +\
          ': ' +\
          arrayTime._get_mode().get_sync()[0].__str__() +\
          ' ' +\
          arrayTime._get_type().get_sync()[0].__str__()

    #
    # release reference
    #
    simpleClient.releaseComponent("CONTROL_TIMESOURCE")
    simpleClient.releaseComponent("CORR_ARRAY_TIME")

    #
    # execRTAItest will unload kernel modules right after we finish here,
    # if array-time is still there with a reference to the teHandler
    # fifo then rtai_fifo will fail to unload. Let's give here some time
    # for that not to happen.
    #
    sleep(5)
    
except Exception, e:
    print "Test FAILED!!!"
    print "The exception was:", e

print "The End"
print "___oOo___"

#
# ___oOo___
