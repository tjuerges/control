#!/bin/bash

TS=`date -u --iso-8601=seconds`
echo $TS "epilogue started" > epilogue.log

#
# do this trick for setting MODROOT here and use unloadLkmModule after
TMP=$PWD
cd ../
export MODROOT=$PWD
cd $TMP

#
# stop test containers and ACS
acsStopContainer java >> epilogue.log 2>&1
acsStopContainer cpp >> epilogue.log 2>&1
acsStop >> epilogue.log 2>&1

# Undo the permission change done in the prologue
chmod o-r ../config/test.lkm

TS=`date -u --iso-8601=seconds`
echo $TS "epilogue terminated" >> epilogue.log

#
# ___oOo___
