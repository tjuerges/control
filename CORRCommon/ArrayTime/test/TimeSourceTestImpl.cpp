// $Id$
//
// Copyright (C) 2004
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Library General Public License
// as published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Library General Public License for more details.
//
// You should have received a copy of the GNU Library General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
//
// Correspondence concerning ALMA should be addressed as follows:
//
//        Internet email: alma-sw-admin@nrao.edu
//

//
// ACS stuff
//
#include <logging.h>
#include <acserr.h>
#include <baci.h>
#include <acsutilTimeStamp.h>

//
// Local stuff
//
#include "TimeSourceTestImpl.h"

//----------------------------------------------------------------------------------
TimeSource::TimeSource(const ACE_CString & name, maci::ContainerServices* cs)
  : acscomponent::ACSComponentImpl(name, cs)
{
    ACS_SHORT_LOG((LM_INFO,"TimeSource::TimeSource"));
}

//----------------------------------------------------------------------------------
void TimeSource::execute()
    throw(acsErrTypeLifeCycle::LifeCycleExImpl)
{
    ACS_SHORT_LOG((LM_INFO, "TimeSource::execute"));
}

//----------------------------------------------------------------------------------
void TimeSource::cleanUp()
{
    ACS_SHORT_LOG((LM_INFO, "TimeSource::cleanUp"));
}

//----------------------------------------------------------------------------------
TimeSource::~TimeSource(void)
{
    ACS_SHORT_LOG((LM_INFO, "TimeSource::~TimeSource"));
}

//----------------------------------------------------------------------------------
ACS::Time TimeSource::timeAtNextTE()
    throw (CORBA::SystemException)
{
    ACS_SHORT_LOG((LM_INFO, "TimeSource::timeAtNextTE"));

    ACS::Time time = ::getTimeStamp();

    return time + 480000 - (time % 480000);
}

/////////////////////////////////////////////////
// MACI DLL support functions
/////////////////////////////////////////////////
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(TimeSource)
