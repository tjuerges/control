#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# ramestic  2005-02-10  created

global PID

set env(ACS_CDB) [pwd]
set ACS_TMP $env(PWD)/tatlogs/run$PID
set env(ACS_TMP) $ACS_TMP

#
# ___oOo___