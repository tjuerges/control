#include <stdlib.h>
#include <sys/time.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

void bail(const char *on_what)
{
    if ( errno )
	printf("Bail on: %s\n", strerror(errno));
    printf("Bailed in %s\n", on_what);
    exit(1);
}

#define N		1
#define SELECT_US	10000

int main()
{
    // Holds result of a function
    int result;
    unsigned int count = N; // we'll count down from 10,0000
    
    struct timeval tv, t0, now;
    tv.tv_sec = 0;
    tv.tv_usec = SELECT_US;
    
    /*printf(
	"This program tests the use of select() as a timing mechanism.\n"
	"As programmed, it should block for 100 usecs at a time, then\n"
	"decrement a counter that starts at 10,000. At the end of 10,000\n"
	"cycles, the counter will be zero, and the program will end.\n"
	"The time it takes to achieve can be used to determine the accuracy\n"
	"of using select(0, NULL, NULL, NULL, &tv) as a high-resolution timer.\n\n"
	"It should probably be called as an argument to time in the shell.\n"
	"[ USAGE:$ time ./btime ]\n"
	"Starting...\n"
	);
    */

    if ( gettimeofday(&t0, 0) )
    {
	printf("gettimeofday failed!\n");
	return 1;
    }

    while (count--)
    {
	result = select(0, NULL, NULL, NULL, &tv);
	if (result < 0) bail("select()");
	tv.tv_sec = 0;
	tv.tv_usec = 10000;
    }
    
    if ( gettimeofday(&now, 0) )
    {
	printf("gettimeofday failed!\n");
	return 1;
    }

    printf("slept/select=%ld/%d[ms]\n",
	   now.tv_sec * 1000 + now.tv_usec / 1000 -
	   t0.tv_sec * 1000 - t0.tv_usec / 1000,
	   N * SELECT_US / 1000);
    //printf("Complete. Check your clocks, please!\n\n");

    return 0;   
}
