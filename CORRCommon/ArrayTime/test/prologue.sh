#!/bin/bash

TS=`date -u --iso-8601=seconds`
echo $TS "prologue started" > prologue.log

#
# do this trick for setting MODROOT here and use loadLkmModule after
TMP=$PWD
cd ../
export MODROOT=$PWD
cd $TMP

# This is needed because, by default, CVS checks this file out so that
# others cannot read it. However config/teHandlerTest.lkm needs to be
# read by root (the unloadLkmModule executable is setuid root) and on
# an NFS filesystem exported with the with root_squash option root has
# no special privileges
chmod o+r ../config/test.lkm

#
# star ACS and test containers
acsStart --noloadifr >> prologue.log 2>&1
acsstartupLoadIFR $(<IDLFilesToLoad) >> prologue.log 2>&1
acsStartContainer -java java >> prologue.log 2>&1 &
acsStartContainer -cpp cpp >> prologue.log 2>&1 &
sleep 15

TS=`date -u --iso-8601=seconds`
echo $TS "prologue terminated" >> prologue.log

#
# ___oOo___
