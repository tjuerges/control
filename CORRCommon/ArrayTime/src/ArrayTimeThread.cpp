/************************************************************************
* "@(#) $Id$"
*
* who       when        what
* --------  ----------  ----------------------------------------------
* ramestic  2005-06-06  created
*/

/*
 * Copyright (C) 2003 Associated Universities Inc
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Correspondence concerning ALMA Software should be addressed as follows:
 *   Internet email: alma-sw-admin@nrao.edu
 */

/************************************************************************
*   NAME
*	ArrayTimeThread - ArrayTime main thread implementation
*
*------------------------------------------------------------------------
*/

//
// ACS stuff
//
#include <stdlib.h> /* need this for rtai headers included from teHandler.h */

//
// COORCommon stuff
//
#include <teHandler.h>

//
// Local stuff
//
#include "ArrayTimeThread.h"

//----------------------------------------------------------------------------------
void ArrayTimeThread::onStart()
{
    ACS_SHORT_LOG((LM_INFO, "ArrayTimeThread::onStart"));

    //
    // create fifo command object for communication with TE handler module
    //
    try
    {
	if ( (m_fifoCmd_p = new rtToolsFifoCmd(TE_HANDLER_CMD_FIFO_DEV)) == NULL )
        {
            ACS_SHORT_LOG((LM_ERROR, "failed to create rtToolsFifoCmd instance"));

            CorrErr::ArrayTimeInternalExImpl ex =
                CorrErr::ArrayTimeInternalExImpl(__FILE__, __LINE__,
                                                 "ArrayTimeThread::onStart");
        
            ex.setReason("failed to create rtToolsFifoCmd instance");

            throw ex;
        }
    }
    catch(...)
    {
	ACS_SHORT_LOG((LM_ERROR,
                       "got exception while creating rtToolsFifoCmd instance"));

        CorrErr::ArrayTimeInternalExImpl ex =
            CorrErr::ArrayTimeInternalExImpl(__FILE__, __LINE__,
                                             "ArrayTimeThread::onStart");
        
        ex.setReason("got exception while creating rtToolsFifoCmd instance");
        
        throw ex;
    }
}

//----------------------------------------------------------------------------------
void ArrayTimeThread::onStop()
{
    ACS_SHORT_LOG((LM_INFO, "ArrayTimeThread::onStop"));

    //
    // get rid of fifo command object
    //
    if ( m_fifoCmd_p != 0 )
    {
	delete m_fifoCmd_p;

        m_fifoCmd_p = 0;
    }
}

//----------------------------------------------------------------------------------
void ArrayTimeThread::runLoop()
{
//
// this code used many times for testing purposes.
//
#if 0
    static int zxz = 1;
    static Correlator::ArrayTimeModes mode = Correlator::HARD;

    if ( (zxz % 30) == 0 )
    {
        if ( mode == Correlator::HARD )
        {
            mode = Correlator::FW;
        }
        else
        {
            mode = Correlator::HARD;
        }
    }
    zxz++;
    
    ACS::Time ts = getTimeStamp();
    m_arrayTime_p->m_mode_p->getDevIO()->write(mode, ts);

    return;
#else
    teHandlerClock_t clk;

    //
    // get teHandler's clock data (it contains the mode)
    //
    try
    {
	int cmd = teHandlerCMD_GETCLK;
	
	m_fifoCmd_p->sendRecvCmd(&cmd, sizeof(cmd), (void *)&clk, sizeof(clk), 50);
    }
    catch ( rtToolsFifoCmdErr_t e )
    {
	ACS_SHORT_LOG((LM_ERROR, "GETCLK has failed (exp=%d)!", e));
	
        return;
    }
    catch ( ... )
    {
        ACS_SHORT_LOG((LM_ERROR, "GETCLK thrown an unexpected exception!"));

        return;
    }

    //
    // update property with current mode
    //
    bool flag = true;
    Correlator::ArrayTimeModes mode;
    switch ( clk.mode )
    {
    case TE_HANDLER_MODE_SOFT:
        mode = Correlator::SOFT;
        break;
    case TE_HANDLER_MODE_FW:
        mode = Correlator::FW;
        break;
    case TE_HANDLER_MODE_HARD:
        mode = Correlator::HARD;
        break;
    default:
        flag = false;
        ACS_SHORT_LOG((LM_ERROR, "unexpected TE handler mode (%d)!", clk.mode));
        break;
    }
    if ( flag )
    {
        ACS::Time ts = getTimeStamp();
        m_arrayTime_p->m_mode_p->getDevIO()->write(mode, ts);
    }

    //
    // update property with current type
    //
    flag = true;
    Correlator::ArrayTimeTypes type;
    switch ( clk.type )
    {
    case TE_HANDLER_TYPE_LOCAL:
        type = Correlator::LOCALCPU;
        break;
    case TE_HANDLER_TYPE_ARRAY:
        type = Correlator::ARRAY;
        break;
    default:
        flag = false;
        ACS_SHORT_LOG((LM_ERROR, "unexpected TE handler type (%d)!", clk.type));
        break;
    }
    if ( flag )
    {
        ACS::Time ts = getTimeStamp();
        m_arrayTime_p->m_type_p->getDevIO()->write(type, ts);
    }

    return;
#endif
}

/*___oOo___*/
