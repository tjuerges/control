#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# who       when      what
# --------  --------  ----------------------------------------------
# rsoto/tshen   2007-07-30  created 
#
# $Id$
#

"""
This module is part of the Control Command Language.
Defines the GPS class.
"""

import Acspy.Clients.SimpleClient
import Acspy.Common.QoS
import Acspy.Util.ACSCorba
import Acspy.Common.Err

class ArrayTime:
    def __init__(self, componentName = None):
        '''
        The ArrayTime class is a python proxy to the ArrayTime
        component. The component can be running before creating this
        proxy but if it is not it will be started i.e., the object
        reference is obtained using a call to getComponent.
        
        ArrayTime is implemented in both the CCC & CDP computers
        in order to synchronize these computers to array time.
        The ACC sends a TimeSource object (defined by the control
        subsystem) which the distributed clock in the CCC or CDP
        uses to obtain the array time at the next timing event.
        Also, the ACC can query the distributed clock to
        determine if its time is synchronized. 
        
        EXAMPLE:
        # Load the necessary defintions
        import CCL.ArrayTime
        # create the object at CCC computer
        at = CCL.ArrayTime.ArrayTime(CORR/ARRAY_TIME/CCC)
        # synchronize the time if this is not synchronized
        if at.isSynchronized() == False:
           at.synchronizeTime()
        # re-synchronize the time with the current signal
        at.resynchronize()
        # Get the TE count and the absolute time.
        (t0, ticks) = at.getClockData()
        #destroy the component
        del(at)
        '''
        
        # initialize the base class
        if componentName == None:
            msg="Component Name must be provided"
            raise msg 
            
        self.__client = Acspy.Clients.SimpleClient.PySimpleClient.getInstance()
        self.__at = self.__client.getComponent(componentName);


    def GET_MODE(self):
        '''
        Returns the value of the "mode" property.
        It can be SOFT, HW or HARD
         
        EXAMPLE:
        # Load the necessary defintions
        import CCL.ArrayTime
        # create the object at CCC computer
        at = CCL.ArrayTime.ArrayTime(CORR/ARRAY_TIME/CCC)
        # Get the current mode of the ArrayTime
        at.GET_MODE()
        # Get the type of the ArrayTime
        at.GET_TYPE()
        '''
        (value, completion) = self.__at._get_mode().get_sync();
        Acspy.Common.Err.addComplHelperMethods(completion);
        if completion.isErrorFree() == True:
            return value;
        else:
            completion.log();

    def GET_TYPE(self):
        '''
        Returns the value of the "type" property
        It can be LOCALCPU or ARRAY

        EXAMPLE:
        See the example for GET_MODE() method
        '''
        (value, completion) = self.__at._get_type().get_sync();
        Acspy.Common.Err.addComplHelperMethods(completion);
        if completion.isErrorFree() == True:
            return value;
        else:
            completion.log();


    def isSynchronized(self):
        '''
        Returns true if array time is synchronized, else false.
        Is the distributed clock synchronized.
        ACC queries the distributed clock to determine if
        its clock is synchronized to array time.
        If not, the ACC can command the distributed clock
        to retry synchronize its clock via synchronizeTime().

        EXAMPLE:
        See the example for the constructor (__init__ function)
        '''
        return self.__at.isSynchronized()

    def resynchronize(self):
        '''
        This method forces TEhandler to re-synchronize with
        current signal while running in FW mode

        EXAMPLE:
        See the example for the constructor (__init__ function)
        '''
        import Correlator
        if self.GET_MODE() == Correlator.SOFT:
        	msg = "Cannot synchronise when in SOFT mode!"
        	raise msg
        
        return self.__at.resynchronize()

    def synchronizeTime(self, timeSourceRef = None):
        '''
        Synchronize the distributed clock time.  ACC (Array Control
        Computer) calls this to provide a TimeSource object which
        the distributed clock uses to set its array time via
        TimeSource.timeAtNextTE().

        EXAMPLE:
        See the example for the constructor (__init__ function)
        '''
        if timeSourceRef == None:
            import CCL.TimeSource
            timeSourceRef = CCL.TimeSource.TimeSource()._TimeSource__timesource
            
        return self.__at.synchronizeTime(timeSourceRef)

    def getClockData(self):
        '''
        Get local TE count and absolute time at that last TE

        EXAMPLE:
        See the example for the constructor (__init__ function)
        '''
        return self.__at.getClockData()
    

   
