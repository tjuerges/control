/*
 * Copyright (C) 2003 Associated Universities Inc
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Correspondence concerning ALMA Software should be addressed as follows:
 *   Internet email: alma-sw-admin@nrao.edu
 */

/************************************************************************
 * "@(#) $Id$"
 *
 * who       when        what
 * --------  ----------  ----------------------------------------------
 * ramestic  2004-02-20  created
 */

/************************************************************************
 *   NAME
 *    ArrayTimeImpl - ArrayTime interface implementation.
 *
 *   SYNOPSIS
 *
 *   PARENT CLASS
 *
 *   DESCRIPTION
 *
 *
 *   PUBLIC METHODS
 *
 *
 *   PUBLIC DATA MEMBERS
 *
 *
 *   PROTECTED METHODS
 *
 *
 *   PROTECTED DATA MEMBERS
 *
 *
 *   PRIVATE METHODS
 *
 *
 *   PRIVATE DATA MEMBERS
 *
 *
 *   FILES
 *
 *   ENVIRONMENT
 *
 *   COMMANDS
 *
 *   RETURN VALUES
 *
 *   CAUTIONS
 *
 *   EXAMPLES
 *
 *   SEE ALSO
 *
 *   BUGS
 *
 *------------------------------------------------------------------------
 */

//
// ACS stuff
//
#include <baciDB.h>
#include <acsutilTimeStamp.h>

//
// COORCommon stuff
//
#include <teHandler.h>

//
// Local stuff
//
#include "ArrayTimeThread.h"
#include "ArrayTimeImpl.h"

#include <maciContainerImpl.h>

using std::stringstream;
//----------------------------------------------------------------------------------
ArrayTimeImpl::ArrayTimeImpl(const ACE_CString &name,
                             maci::ContainerServices *containerServices):
    CharacteristicComponentImpl(name, containerServices),
    m_componentName(name.c_str()),
    m_mode_p(0),
    m_type_p(0),
    m_fifoCmd_p(0)
{
    ACS_TRACE("ArrayTimeImpl::ArrayTimeImpl");
}

//----------------------------------------------------------------------------------
ArrayTimeImpl::~ArrayTimeImpl()
{
    ACS_TRACE("ArrayTimeImpl::~ArrayTimeImpl");
}

//----------------------------------------------------------------------------------
void ArrayTimeImpl::execute()
{
    ACS_SHORT_LOG((LM_INFO, "ArrayTimeImpl::execute"));

    //
    // mode property
    //
    try
    {
        m_mode_p = new ROEnumImpl<ACS_ENUM_T(Correlator::ArrayTimeModes),
            POA_Correlator::ROArrayTimeModes>((m_componentName + ":mode").c_str(),
                                              getComponent());
    }
    catch( ... )
    {
        ACS_SHORT_LOG((LM_ERROR, "failed to create mode property!"));

        acsErrTypeLifeCycle::LifeCycleExImpl ex(__FILE__, __LINE__,
                                                "ArrayTimeImpl::execute()");

        ex.addData("Cause", "failed to create mode property!");

        throw ex;
    }

    //
    // type property
    //
    try
    {
        m_type_p = new ROEnumImpl<ACS_ENUM_T(Correlator::ArrayTimeTypes),
            POA_Correlator::ROArrayTimeTypes>((m_componentName + ":type").c_str(),
                                              getComponent());
        //CHARACTERISTIC_COMPONENT_PROPERTY(type, m_type_p);
    }
    catch( ... )
    {
        ACS_SHORT_LOG((LM_ERROR, "failed to create type property!"));
        
        acsErrTypeLifeCycle::LifeCycleExImpl ex(__FILE__, __LINE__,
                                                "ArrayTimeImpl::execute()");
        
        ex.addData("Cause", "failed to create type property!");
        
        throw ex;
    }

    //
    // create fifo command object for communication with TE handler module
    //
    try
    {
        m_fifoCmd_p = new rtToolsFifoCmd(TE_HANDLER_CMD_FIFO_DEV);
    }
    catch(...)
    {
        ACS_SHORT_LOG((LM_ERROR, "failed to create rtToolsFifoCmd instance!"));
        
        acsErrTypeLifeCycle::LifeCycleExImpl ex(__FILE__, __LINE__,
                                                "ArrayTimeImpl::execute()");
        
        ex.addData("Cause", "failed to create rtToolsFifoCmd instance!");
        
        throw ex;
    }
    
    //
    // temporal variable for holding our thread pointer, we do not need to
    // keep this variable, because handling of the thread is done through
    // the thread manager indirectly
    //
    ArrayTimeThread *thread_p;

    //
    // start the array time thread
    //
    ArrayTimeImpl *selfPtr = this; // 'this' seems to be const and that's not liked
                                   // by 'create'

    try
    {
        thread_p = getContainerServices()->getThreadManager()->
            create<ArrayTimeThread, ArrayTimeImpl *>(ARRAY_TIME_THREAD_NAME,
                                                     selfPtr);
    }
    catch ( ... )
    {
        ACS_SHORT_LOG((LM_ERROR, "failed to create %s thread!",
                       ARRAY_TIME_THREAD_NAME));
        
        acsErrTypeLifeCycle::LifeCycleExImpl ex(__FILE__, __LINE__,
                                                "ArrayTimeImpl::execute()");
        
        ex.addData("Cause", "failed to create thread!");
        
        throw ex;
    }
    
    if ( thread_p == NULL )
    {
        ACS_SHORT_LOG((LM_ERROR, "thread manager returned a null thread pointer!"));
        
        acsErrTypeLifeCycle::LifeCycleExImpl ex(__FILE__, __LINE__,
                                                "ArrayTimeImpl::execute()");
        
        ex.addData("Cause", "thread manager returned a null thread pointer!");
        
        throw ex;
    }
    
    ACS_SHORT_LOG((LM_INFO, "thread %s spawned", ARRAY_TIME_THREAD_NAME));
    
    //
    // resume the just created thread
    //
    try
    {
        thread_p->resume();
    }
    catch(...)
    {
        ACS_SHORT_LOG((LM_ERROR, "failed to resume %s thread!",
                       ARRAY_TIME_THREAD_NAME));
        
        acsErrTypeLifeCycle::LifeCycleExImpl ex(__FILE__, __LINE__,
                                                "ArrayTimeImpl::execute()");
        
        ex.addData("Cause", "failed to resume thread!");
        
        throw ex;
    }
}

//----------------------------------------------------------------------------------
void ArrayTimeImpl::cleanUp()
{
    ACS_SHORT_LOG((LM_TRACE,"ArrayTimeImpl::cleanUp"));

    //
    // stop threads
    //
    getContainerServices()->getThreadManager()->stopAll();

    //
    // destroy mode property
    //
    if ( m_mode_p )
    {
        m_mode_p->destroy();
        m_mode_p = 0;
    }

    //
    // destroy type property
    //
    if ( m_type_p )
    {
        m_type_p->destroy();
        m_type_p = 0;
    }

    //
    // get rid of fifo command object
    //
    if ( m_fifoCmd_p != 0 )
    {
        delete m_fifoCmd_p;

        m_fifoCmd_p = 0;
    }
}

//----------------------------------------------------------------------------------
Correlator::ROArrayTimeModes_ptr ArrayTimeImpl::mode()
{
    Correlator::ROArrayTimeModes_var prop =
        Correlator::ROArrayTimeModes::_narrow(m_mode_p->getCORBAReference());

    return prop._retn();
}

//----------------------------------------------------------------------------------
Correlator::ROArrayTimeTypes_ptr ArrayTimeImpl::type()
{
    Correlator::ROArrayTimeTypes_var prop =
        Correlator::ROArrayTimeTypes::_narrow(m_type_p->getCORBAReference());

    return prop._retn();
}

//----------------------------------------------------------------------------------
void ArrayTimeImpl::synchronizeTime(Control::TimeSource_ptr timeSourceRef)
{
    int reply;
    teHandlerClock_t clk;
    ACS::Time atNextTick, now, startAt;
    ACE_Time_Value tv;

    ACS_SHORT_LOG((LM_INFO, "ArrayTimeImpl::synchronizeTime"));

    //
    // make an effort for starting as close as possible right after
    // the beginning of a 48ms window
    //
    now = startAt = getTimeStamp();
    if ( (now % 480000) > 100000 /* 10ms */ )
    {
        //
        // try to start at next tick
        //
        startAt = now - now % 480000 + 480000;

        //
        // sleep is not very precise, this loops ensures we are not starting
        // before than startAt
        //
        do
        {
            //
            // sleep for 10ms. I choose 10ms not with a very solid background.
            // Somehow, I thought that 10ms is a sort of magig number for linux!
            //
            ACE_OS::sleep(ACE_Time_Value(0, 10000));

            //
            // read the time again
            //
            now = getTimeStamp();

        }
        while ( startAt > now );
    }

    //
    // for debugging
    //
    ACS_SHORT_LOG((LM_INFO,
                   "starting timing now-startAt=%llu[us]",
                   (now - startAt) / 10));

    //
    // get actual clock data with GETCLK
    //
    try
    {
        int cmd = teHandlerCMD_GETCLK;

        m_fifoCmd_p->sendRecvCmd(&cmd, sizeof(cmd), (void *)&clk, sizeof(clk), 50);
    }
    catch ( rtToolsFifoCmdErr_t e )
    {
        stringstream errStr;

        errStr << "GETCLK has failed (exp=" << e << ")";

        ACS_SHORT_LOG((LM_ERROR, errStr.str().c_str()));
        
        CorrErr::ArrayTimeInternalExImpl ex =
            CorrErr::ArrayTimeInternalExImpl(__FILE__, __LINE__, 
                                             "ArrayTimeImpl::synchronizeTime");

        ex.setReason(errStr.str().c_str());

        throw ex.getArrayTimeInternalEx();
    }
    catch ( ... )
    {
        ACS_SHORT_LOG((LM_ERROR, "GETCLK thrown an unexpected exception"));
        
        CorrErr::ArrayTimeInternalExImpl ex =
            CorrErr::ArrayTimeInternalExImpl(__FILE__, __LINE__,
                                             "ArrayTimeImpl::synchronizeTime");
        
        ex.setReason("GETCLK thrown an unexpected exception");

        throw ex.getArrayTimeInternalEx();
    }

    //
    // get time at next TE from TimeSource component
    //
    try
    {
        atNextTick = timeSourceRef->timeAtNextTE();
    }
    catch ( ... )
    {
        ACS_SHORT_LOG((LM_ERROR, "timeAtNextTE thrown an exception"));
        
        CorrErr::ArrayTimeInternalExImpl ex =
            CorrErr::ArrayTimeInternalExImpl(__FILE__, __LINE__, 
                                             "ArrayTimeImpl::synchronizeTime");
        
        ex.setReason("timeAtNextTE thrown an exception");

        throw ex.getArrayTimeInternalEx();
    }

    //
    // reset the TE handler with the new t0 value. SETT0 will fail if the
    // passed ticks count does not coincide with the current one. That is
    // too say, it will fail if between now and the moment the cmd arrives
    // at kernel space a tick has happend.
    //
    try
    {
        rtToolsCmdBuffer<teHandlerSetT0Cmd_t> cmd(teHandlerCMD_SETT0);

        cmd->value = atNextTick;
        cmd->tick = clk.ticks;

        m_fifoCmd_p->sendRecvCmd(cmd.pack(), cmd.size(), &reply, sizeof(reply), 50);

    }
    catch ( rtToolsFifoCmdErr_t e )
    {
        stringstream errStr;

        errStr << "got a problem when sending SETT0 cmd (exp=" << e << ")";

        ACS_SHORT_LOG((LM_ERROR, errStr.str().c_str()));
        
        CorrErr::ArrayTimeInternalExImpl ex =
            CorrErr::ArrayTimeInternalExImpl(__FILE__, __LINE__,
                                             "ArrayTimeImpl::synchronizeTime");

        ex.setReason(errStr.str().c_str());

        throw ex.getArrayTimeInternalEx();
    }
    catch ( ... )
    {
        stringstream errStr;

        errStr << "SETT0 thrown an unexpected exception";

        ACS_SHORT_LOG((LM_ERROR, errStr.str().c_str()));
        
        CorrErr::ArrayTimeInternalExImpl ex =
            CorrErr::ArrayTimeInternalExImpl(__FILE__, __LINE__,
                                             "ArrayTimeImpl::synchronizeTime");
        
        ex.setReason(errStr.str().c_str());
        
        throw ex.getArrayTimeInternalEx();
    }


    //
    // check reply status to SETT0 command
    //
    if ( reply == TE_HANDLER_CMD_STAT_OK )
    {
        ACS_SHORT_LOG((LM_INFO,
                       "SETT0 cmd succeeded (tick/t0=%lu/%llu)",
                       clk.ticks + 1, atNextTick));
    }
    else if ( reply == TE_HANDLER_CMD_STAT_WRONG_TICK )
    {
        ACS_SHORT_LOG((LM_ERROR,
                       "round-trip error (deltaStart/deltaEnd=%llu[ms]/%llu[ms])",
                       (now - startAt) / 10000,
                       (getTimeStamp() - now) / 10000));

        CorrErr::RoundTripTimeErrorExImpl ex(__FILE__, __LINE__,
                                             "ArrayTimeImpl::synchronizeTime");

        throw ex.getRoundTripTimeErrorEx();
    }
    else
    {
        stringstream errStr;

        errStr << "got unknown error code to SETT0 cmd (" << reply << ")";

        ACS_SHORT_LOG((LM_ERROR, errStr.str().c_str()));
        
        CorrErr::ArrayTimeInternalExImpl ex =
            CorrErr::ArrayTimeInternalExImpl(__FILE__, __LINE__,
                                             "ArrayTimeImpl::synchronizeTime");

        ex.setReason(errStr.str().c_str());

        throw ex.getArrayTimeInternalEx();
    }
}

//----------------------------------------------------------------------------------
CORBA::Boolean ArrayTimeImpl::isSynchronized()
{
    ACS_SHORT_LOG((LM_INFO, "ArrayTimeImpl::isSynchronized"));

    try
    {
        int cmd = teHandlerCMD_GETCLK;
        teHandlerClock_t clk;

        m_fifoCmd_p->sendRecvCmd(&cmd, sizeof(cmd), (void *)&clk, sizeof(clk), 50);

        return (clk.type == TE_HANDLER_TYPE_ARRAY) ? true : false;
    }
    catch(rtToolsFifoCmdErr_t &e)
    {
        stringstream errStr;

        errStr << "GETCLK has failed (exp=" << e << ")";

        ACS_SHORT_LOG((LM_ERROR, errStr.str().c_str()));

        CorrErr::ArrayTimeInternalExImpl ex =
            CorrErr::ArrayTimeInternalExImpl(__FILE__, __LINE__,
                                             "ArrayTimeImpl::isSynchronized");

        ex.setReason(errStr.str().c_str());

        throw ex.getArrayTimeInternalEx();
    }

    return false;
}

//----------------------------------------------------------------------------------
void ArrayTimeImpl::resynchronize()
{
    ACS_SHORT_LOG((LM_INFO, "ArrayTimeImpl::resynchronize"));

    int reply;

    //
    // send RESYNC command via fifo to TE handler
    //
    try
    {
        int cmd = teHandlerCMD_RESYNC;

        m_fifoCmd_p->sendRecvCmd(&cmd, sizeof(cmd),
                                 (void *)&reply, sizeof(reply),
                                 60);
    }
    catch(rtToolsFifoCmdErr_t &e)
    {
        stringstream errStr;

        errStr << "RESYN thrown an exception (ex=" << e << ")";

        ACS_SHORT_LOG((LM_ERROR, errStr.str().c_str()));

        CorrErr::ArrayTimeInternalExImpl ex =
            CorrErr::ArrayTimeInternalExImpl(__FILE__, __LINE__,
                                             "ArrayTimeImpl::resynchronize");

        ex.setReason(errStr.str().c_str());

        throw ex.getArrayTimeInternalEx();
    }

    //
    // check reply
    //
    if ( reply == TE_HANDLER_CMD_STAT_OK )
    {
        ACS_SHORT_LOG((LM_INFO, "RESYNC command has succeed"));
    }
    else if ( reply ==  TE_HANDLER_CMD_STAT_RESYNC_FAILED )
    {
        ACS_SHORT_LOG((LM_ERROR, "RESYNC failed to resync the TE handler!"));

        CorrErr::ArrayTimeInternalExImpl ex =
            CorrErr::ArrayTimeInternalExImpl(__FILE__, __LINE__,
                                             "ArrayTimeImpl::resynchronize");

        ex.setReason("RESYNC failed to resync the TE handler!");
        
        throw ex.getArrayTimeInternalEx();
    }
    else
    {
        stringstream errStr;
        
        errStr << "RESYNC replied with unknown code " << reply;
        
        ACS_SHORT_LOG((LM_ERROR, errStr.str().c_str()));
        
        CorrErr::ArrayTimeInternalExImpl ex =
            CorrErr::ArrayTimeInternalExImpl(__FILE__, __LINE__,
                                             "ArrayTimeImpl::resynchronize");
        
        ex.setReason(errStr.str().c_str());
        
        throw ex.getArrayTimeInternalEx();
    }
}

//----------------------------------------------------------------------------------
void ArrayTimeImpl::getClockData(ACS::Time_out receive,
                                 Correlator::ArrayTimeClockData_t_out clockData,
                                 ACS::Time_out transmit)
{
    ACS_SHORT_LOG((LM_INFO, "ArrayTimeImpl::getClockData"));

    //
    // record time-stamp at which this command was actually received
    //
    receive = getTimeStamp();

    try
    {
        int cmd = teHandlerCMD_GETCLK;
        teHandlerClock_t clk;

        //
        // get data from undelaying teHandler
        //
        m_fifoCmd_p->sendRecvCmd(&cmd, sizeof(cmd), (void *)&clk, sizeof(clk), 50);

        //
        // copy data into output parameter
        //
        switch ( clk.mode )
        {
        case TE_HANDLER_MODE_SOFT:
            clockData.mode = Correlator::SOFT;
            break;
        case TE_HANDLER_MODE_HARD:
            clockData.mode = Correlator::HARD;
            break;
        case TE_HANDLER_MODE_FW:
            clockData.mode = Correlator::FW;
            break;
        default:
            CorrErr::ArrayTimeInternalExImpl ex =
                CorrErr::ArrayTimeInternalExImpl(__FILE__, __LINE__,
                                                 "ArrayTimeImpl::getClockData");
        
            ex.setReason("got unexpected mode value from local TE handler");
            
            throw ex.getArrayTimeInternalEx();
        }
        switch ( clk.type )
        {
        case TE_HANDLER_TYPE_LOCAL:
            clockData.type = Correlator::LOCALCPU;
            break;
        case TE_HANDLER_TYPE_ARRAY:
            clockData.type = Correlator::ARRAY;
            break;
        default:
            CorrErr::ArrayTimeInternalExImpl ex =
                CorrErr::ArrayTimeInternalExImpl(__FILE__, __LINE__,
                                                 "ArrayTimeImpl::getClockData");
        
            ex.setReason("got unexpected type value from local TE handler");
            
            throw ex.getArrayTimeInternalEx();
        }
        clockData.t0 = clk.t0;
        clockData.cpu_on_last_te = clk.cpu_on_last_te;
        clockData.cpu_on_next_te = clk.cpu_on_next_te;
        clockData.ticks = clk.ticks;
        clockData.cpu_hz = clk.cpu_hz;
    }
    catch(rtToolsFifoCmdErr_t &e)
    {
        stringstream errStr;

        errStr << "GETCLK has failed (exp=" << e << ")";

        ACS_SHORT_LOG((LM_ERROR, errStr.str().c_str()));
        
        CorrErr::ArrayTimeInternalExImpl ex =
            CorrErr::ArrayTimeInternalExImpl(__FILE__, __LINE__,
                                             "ArrayTimeImpl::getClockData");
        
        ex.setReason(errStr.str().c_str());
        
        throw ex.getArrayTimeInternalEx();
    }

    //
    // record the time-stamp at which this command is being dispatched 
    // back to its originator.
    //
    transmit = getTimeStamp();

}

/////////////////////////////////////////////////
// MACI DLL support functions
/////////////////////////////////////////////////

#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(ArrayTimeImpl)

/* ___oOo___ */
