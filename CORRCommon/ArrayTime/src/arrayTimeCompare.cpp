/*******************************************************************************
 *
 * "@(#) $Id$"
 *
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2007 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * who       when      what
 * --------  --------  ----------------------------------------------
 * ramestic  2007-05-02  created
 */

/************************************************************************
 *   NAME
 *       arrayTimeCompare - check local and remote times   
 * 
 *   SYNOPSIS
 *       arrayTimeCompare <your-array-time-component-name-of-choice>
 * 
 *   DESCRIPTION
 *       this command-prompt tool let's compare the current array-time 
 *       between the local machine where the command is executed and a
 *       remote CORBA ArrayTime component. The command outputs t0 and
 *       the ticks count for both instances (local and remote) and the
 *       command execution time; this last one must report less than 48ms
 *       for giving meaningfulness to the command. If the reported time
 *       figures (t0 + ticks * 48ms, the time at the last tick) are the same
 *       for the local and remote TE handlers and the execution time is less
 *       than 48ms then we can be sure that both clocks are synchronized
 *       within the ambiguity interval of 48ms. The difference between the
 *       remote and local times is reported as 'delta'.
 *
 *       arrayTimeCompare is also useful for detecting a wrong edge type
 *       triggering of the TE interrupt (raising or falling edges). If A and B
 *       are the two distributed clocks being compared and A is attached
 *       to rising edges (as it should be) but B is attached to falling edges
 *       then when arrayTimeCompare is executed from A then delta will be
 *       -48ms. Instead, when the command is executed from B then delta
 *       will be zero. This is a good indication that B is on the wrong
 *       logic (falling edge). For this to be meaningful the command should
 *       take less than 16ms (TE duty cycle). It follows a schematic description
 *       of such a situation:
 *
 *       TE signal
 *            +--+      +--+      +--+
 *            |  |      |  |      |  |
 *          --+  +------+  +------+  +----
 *       
 *       TE interrupts
 *            ^         ^         ^
 *            |         |         |
 *       A  --+---------+---------+----
 *                       |
 *                       command issued at this time
 *       
 *               ^         ^         ^
 *               |         |         |
 *       B  -----+---------+---------+----
 *                          |
 *                          command issue at this time
 *
 *   FILES
 *
 *   ENVIRONMENT
 *
 *   COMMANDS
 *
 *   RETURN VALUES
 *       0: all good, both array-time nodes are synchronized
 *       1: command failed
 *       2: array-time nodes are not synchornized
 *       3: suspicious command timing, better to repeat the command
 *       4: any one of the components is not in HARD mode 
 *
 *   CAUTIONS 
 *
 *   EXAMPLES
 *
 *   SEE ALSO
 *
 *   BUGS   
 * 
 *------------------------------------------------------------------------
 */

//
// System stuff
//
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <sstream>

//
// ACS stuff
//
#include <maciSimpleClient.h>
#include <acsutilTimeStamp.h>

//
// ICD stuff 
//
#include <ArrayTimeC.h>

//
// CONTROL/CORRCommon stuff
//
#include <rtToolsFifoCmd.h>
#include <teHandler.h>

using std::cout;
using std::endl;
using std::string;
using std::istringstream;


//
// just a helper function for getting an string version of the mode and type
//
string getModeAsString(int mode)
{
    switch ( mode )
    {
        case TE_HANDLER_MODE_SOFT:
            return string("SOFT");
        case TE_HANDLER_MODE_HARD:
            return string("HARD");
        case TE_HANDLER_MODE_FW:
            return string("FW");
    }

    return string("???");
}

string getModeAsString(Correlator::ArrayTimeModes mode)
{
    switch ( mode )
    {
        case Correlator::SOFT:
            return getModeAsString(TE_HANDLER_MODE_SOFT);
        case Correlator::HARD:
            return getModeAsString(TE_HANDLER_MODE_HARD);
        case Correlator::FW:
            return getModeAsString(TE_HANDLER_MODE_FW);
    }

    return string("???");
}

string getTypeAsString(int type)
{
    switch ( type )
    {
        case TE_HANDLER_TYPE_LOCAL:
            return string("LOCAL");
        case TE_HANDLER_TYPE_ARRAY:
            return string("ARRAY");
    }

    return string("???");
}

string getTypeAsString(Correlator::ArrayTimeTypes type)
{
    switch ( type )
    {
        case Correlator::LOCALCPU:
            return getTypeAsString(TE_HANDLER_TYPE_LOCAL);
        case Correlator::ARRAY:
            return getTypeAsString(TE_HANDLER_TYPE_ARRAY);
    }

    return string("???");
}


int main(int argc, char *argv[])
{
    int cmd = teHandlerCMD_GETCLK;
    rtToolsFifoCmd fifoCmd(TE_HANDLER_CMD_FIFO_DEV);
    ACS::Time now, startAt, finishAt;
    ACE_Time_Value sleepTime(0, 10000);
    Correlator::ArrayTime_ptr at_p;

    //
    // check that we have recived a component name as parameter
    //
    if ( argc < 2 )
    {
        cout << "usage: " << argv[0] << " <component-name> [start in ms]" << endl;
        
        return 1;
    }

    //
    // creates and initializes the SimpleClient object. Given that I do not
    // want to pollute the client with my own argv param the following
    // 'init' call passes 1 instead of argc. What would happen if this
    // program is executed more than once simultaneusly in the same host?
    //
    maci::SimpleClient client;
    if (client.init(1, argv) == 0)
    {
        return 1;
    }

    //
    // Must log into manager before we can really do anything
    //
    client.login();

    //
    // get reference to array-time
    //
    try
    {
        at_p = client.getComponentNonSticky<Correlator::ArrayTime>(argv[1]);
    }
    catch ( ... )
    {
        cout << "failed to get reference to component " << argv[1] << endl;

        client.logout();

        return 1;
    }

    /**
     * The startDelay lets the query wait until TE + startDelay to give
     * even the antenna ABMs time to receive the current TE.
     */
    double startDelay(0.0);
    if ( argc == 3 )
    {
        istringstream buffer;
        buffer.str(argv[2]);
        buffer >> startDelay;
    }

    cout << endl << "Start delay = " << startDelay << "ms." << endl;

    startDelay *= 10000.0;

    //
    // make an effort for starting as close as possible right after
    // the beginning of a 48ms window
    //
    now = getTimeStamp();

    /**
     * Try to start at next tick.
     */
    startAt = now - now % TEacs + TEacs + static_cast< ACS::Time >(
                                            std::floor(startDelay + 0.5));

    /**
     * Sleep is not very precise, this loop ensures we are not starting
     * before startAt.
     */
    do
    {
        /**
         * Sleep for 10us.
         */
        ACE_OS::select(0, 0, 0, 0, ACE_Time_Value(0, 10U));

        /**
         * Read the time again.
         */
        now = getTimeStamp();

    }
    while(startAt > now);

    //
    // get local clock data
    //
    teHandlerClock_t localClk0;
    try
    {
        fifoCmd.sendRecvCmd(&cmd, sizeof(cmd),
                            (void *)&localClk0, sizeof(localClk0),
                            50);
    }
    catch(rtToolsFifoCmdErr_t &e)
    {
        cout << "local GETCLK (0) has failed (exp=" << e << ")!" << endl;
	
	return 1;
    }
    
    //
    // from RFC-2030:
    //
    // The roundtrip delay d and local clock offset t are defined as
    // d = (T4 - T1) - (T3 - T2)     t = ((T2 - T1) + (T3 - T4)) / 2
    //
    ACS::Time t1, t2, t3, t4;
    t1 = t2 = t3 = t4 = 0; // avoid compilation warning

    //
    // get clock data from array-time component
    //
    Correlator::ArrayTimeClockData_t remoteClk;
    try
    {
        t1 = getTimeStamp();
        at_p->getClockData(t2, remoteClk, t3);
        t4 = getTimeStamp();
    }
    catch ( ... )
    {
        cout << "getClock data on component " << argv[1] << " has failed!" << endl;
    }

    //
    // record time at which we are finishing
    //
    finishAt = getTimeStamp();

    //
    // read again the local clock data, we want to be sure we are still in the
    // same TE window.
    //
    teHandlerClock_t localClk1;
    try
    {
        fifoCmd.sendRecvCmd(&cmd, sizeof(cmd),
                            (void *)&localClk1, sizeof(localClk1),
                            50);
    }
    catch(rtToolsFifoCmdErr_t &e)
    {
        cout << "local GETCLK (1) has failed (exp=" << e << ")!" << endl;
	
        return 1;
    }

    //
    // logout from ACS manager
    //
    client.logout();

    //
    // compute delta time as delta = remote - local
    //
    long long delta = 
        (remoteClk.t0 + (unsigned long long)remoteClk.ticks * TEacs) -
        (localClk0.t0 + (unsigned long long)localClk0.ticks * TEacs);
        
    //
    // printout local and remote figures
    //
    cout << endl;
    cout << "local:\t" << getModeAsString(localClk0.mode) << "/" << getTypeAsString(localClk0.type) << "\tt0=" << localClk0.t0 << "[acs]" << "\t" << "ticks=" << localClk0.ticks << endl;
    cout << "remote:\t" << getModeAsString(remoteClk.mode) << "/" << getTypeAsString(remoteClk.type) << "\tt0=" << remoteClk.t0 << "[acs]" << "\t" << "ticks=" << remoteClk.ticks << endl;
    cout << "roundtrip delay (d)= " << ((t4 - t1) - (t3 - t2)) / 10 << " [us]" << endl;
    cout << "local clock offset (t) = " << ((t2 - t1) + (t3 - t4)) / 2 / 10 << " [us]" << endl;
    cout << "delta = remote-local:\t" << delta << "[acs]" << endl;

    //
    // if the ticks count changed in the mean while then is highly probable 
    // that the command is reporting incorrect data.
    //
    if ( localClk0.ticks !=  localClk1.ticks )
    {
        cout << "#### WARNING: one full tick has already elapsed (old/new="
             << localClk0.ticks
             << "/"
             << localClk1.ticks
             << ")" << endl;
        cout << "#### WARNING: it is recommended to repeat the command!" << endl;
    }

    //
    // print out how long did all this take. It should be less than 48ms
    // as for the data to be meaningful
    //
    cout << "execution time "
         << static_cast< double >(finishAt - startAt) * 1e-4
         << "ms" << endl;

    //
    // return code indicates sync status
    //
    if ( localClk0.mode != TE_HANDLER_MODE_HARD || remoteClk.mode != Correlator::HARD )
    {
        return 4;
    }
    else if ( delta != 0 )
    {
        return 2;
    }
    else if ( static_cast< double >(finishAt - startAt) * 1e-4 >= 48 )
    {
        return 3;
    }

    //
    // all good, array-time nodes are synchronized
    //
    return 0;
}

/*___oOo___*/
