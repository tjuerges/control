#!/usr/bin/env python
#*******************************************************************************
#
# @(#) $Id$
#
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
###############################################################################
#   NAME
#       arrayTimeStatus - log status of distributed array-time components
#
#   SYNOPSIS
#       arrayTimeStatus [<component-name-to-match>]
#
#   DESCRIPTION
#       based on functionality provided by the naming service this script
#       discover all components of type IDL:alma/Correlator/ArrayTime:1.0
#       currently present in the running CDB. Then it goes through a loop
#       reading the mode and type baci properties of those components, sending
#       to standard output its current values. If it is not possible to get
#       a reference to the component then an error text is send to stdout for
#       that specific component.
#
#       Note that if a component is not available at this moment but is container
#       is actually running then the component will be actually started
#       by the script at the moment it gets a refernce to it.
#
#       The optional parameter is used as part of a regular expression for
#       filtering the component names that should be actually polled. This
#       comes handy for getting an output of those components that the user
#       cares about only. If no argument is provided then all ArrayTime
#       components are to be poll.
#
#   FILES
#
#   ENVIRONMENT
#
#   COMMANDS
#
#   RETURN VALUES
#
#   CAUTIONS 
#
#   EXAMPLES
#
#   SEE ALSO
#
#   BUGS   
# 
###############################################################################

#--REGULAR IMPORTS-------------------------------------------------------------
import sys
import re

#--ACS Imports-----------------------------------------------------------------
# to avoid too extensive logs when components are not available I need
# to redirect the stdout here, before importing PySimpleClient
sys.stdout=open('/dev/null', 'w')
from Acspy.Clients.SimpleClient import PySimpleClient
# now revert the redirection, this second redirection is not seen by PySimpleClient
sys.stdout=sys.__stdout__

#------------------------------------------------------------------------------
def main(argv=None):

    #
    # if we get a parameter then use it as a pattern for checking the status
    # of array-time components which name matches that pattern. The received
    # pattern must be a single string without special characters.
    #
    compNamePattern = None
    if len(argv) != 1:
        compNamePattern = '.*' + argv[1] + '.*'

    #
    # i'm a client
    #
    try:
        simpleClient = PySimpleClient()
    except Exception, e:
        print 'cannot get PySimpleClient!'
        return

    #
    # find components name of type ArrayTime
    #
    try:
        names = simpleClient.findComponents(None,'IDL:alma/Correlator/ArrayTime:1.0',activated=1)
    except Exception, e:
        print 'failed to get array-time components names!'
        return

    #
    # iterate over all possible components
    #
    for i in range(len(names)):
        #
        # if we got a pattern to match check the status of components which
        # have a name that matches.
        #
        if compNamePattern != None:
            if re.match(compNamePattern, names[i]) == None:
                continue
            
        #
        # get component reference
        #
        try:
            ref = simpleClient.getComponentNonSticky(names[i])
            if ref == None:
                print names[i] + ': not available'
        except Exception, e:
            print names[i] + ': exception caught when resolving reference!'
            continue

        #
        # access mode and type properties, printing out their current values
        #
        try:
            print names[i] + ': ' + ref._get_mode().get_sync()[0].__str__() + ' ' +\
                  ref._get_type().get_sync()[0].__str__()
        except Exception, e:
            print names[i] + ': failed to get property'

# execute the main function
if __name__ == "__main__":
    sys.exit(main(sys.argv))
