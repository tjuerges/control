#ifndef ArrayTimeImpl_H
#define ArrayTimeImpl_H

/*
 * Copyright (C) 2003 Associated Universities Inc
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Correspondence concerning ALMA Software should be addressed as follows:
 *   Internet email: alma-sw-admin@nrao.edu
 */

/************************************************************************
* "@(#) $Id$"
*
* who       when        what
* --------  ----------  ----------------------------------------------
* ramestic  2004-02-20  created
*/

//
// ACS stuff
//
#include <enumpropROImpl.h>
#include <baciCharacteristicComponentImpl.h>

//
// CORRCommon stuff
//
#include <rtToolsFifoCmd.h>

//
// CORR stuff
//
#include <CorrErr.h>
#include <ArrayTimeS.h>

//
// Local stuff
//
#include "ArrayTimeThread.h"

#define ARRAY_TIME_THREAD_NAME	"arrayTimeThread"

class ArrayTimeImpl : 
    public virtual baci::CharacteristicComponentImpl,
    public POA_Correlator::ArrayTime
{
  public:

    //
    // component constructor and destructor
    //
    ArrayTimeImpl(const ACE_CString &name,
                  maci::ContainerServices *containerServices);
    virtual ~ArrayTimeImpl();

    //
    // life-cycle stuff
    //
    virtual void execute();
    virtual void cleanUp();

    //
    // properties for exposing teHandler's mode and type of synchronization
    //
    Correlator::ROArrayTimeModes_ptr mode();
    Correlator::ROArrayTimeTypes_ptr type();
    
    /** ACC calls this to provide a TimeSource object which the distributed
     ** clock uses to set its array time via TimeSource.timeAtNextTE()
     ** @param timeSourceRef a reference to a TimeSource object which the
     ** distributed clock uses to set the time.
     ** @excep CORBA::SystemException a CORBA call problem.
     ** @excep CorrErr::ArrayTimeInternalEx see CorrErr.xml for details.
     ** @excep CorrErr::RoundTripTimeErrorEx see CorrErr.xml for details.
     ** @excep CorrErr::SynchronizeTimeOutEx see CorrErr.xml for details.
     */
    virtual void synchronizeTime(Control::TimeSource_ptr timeSourceRef);
    
    /** ACC queries the distributed clock to determine if its clock is
     **  synchronized to array time. If not, the ACC can command the 
     **  distributed clock to retry synchronizeTime().
     ** @return true if array time is synchronized, else false.
     ** @excep CORBA::SystemException a CORBA call problem.
     ** @excep CorrErr::ArrayTimeInternalEx see CorrErr.xml for details.
     */
    virtual CORBA::Boolean isSynchronized();

    /** ACC force to re-synchronize while in free-wheeling mode
     ** as requested by CONTROL this method here makes possible to
     ** resynchronize the TE handler with a re-born tick signal that has
     ** shifted its phase compared with the status before going FW.
     ** @excep CORBA::SystemException a CORBA call problem.
     ** @excep CorrErr::ArrayTimeInternalEx see CorrErr.xml for details.
     */
    virtual void resynchronize();

    /** get current clock data structure and timestamps enclosing this action.
     ** @excep CORBA::SystemException a CORBA call problem.
     ** @excep CorrErr::ArrayTimeInternalEx see CorrErr.xml for details.
     */
    virtual void getClockData(ACS::Time_out receive,
                              Correlator::ArrayTimeClockData_t_out clockData,
                              ACS::Time_out transmit);

    //
    // this let thread's 'run' method to have acces to private members
    //
    friend class ArrayTimeThread;

private:
    std::string m_componentName;

    ROEnumImpl<ACS_ENUM_T(Correlator::ArrayTimeModes),
               POA_Correlator::ROArrayTimeModes> *m_mode_p;
    ROEnumImpl<ACS_ENUM_T(Correlator::ArrayTimeTypes),
               POA_Correlator::ROArrayTimeTypes> *m_type_p;

    //
    // fifo command for communication with TE handler
    //
    rtToolsFifoCmd *m_fifoCmd_p;
};  
#endif /* ifndef ArrayTimeImpl_H */
