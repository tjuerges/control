#ifndef ArrayTimeThread_H
#define ArrayTimeThread_H

/************************************************************************
* "@(#) $Id$"
*
* who       when        what
* --------  ----------  ----------------------------------------------
* ramestic  2004-02-20  created
*/

/*
 * Copyright (C) 2003 Associated Universities Inc
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Correspondence concerning ALMA Software should be addressed as follows:
 *   Internet email: alma-sw-admin@nrao.edu
 */

//
// ACS stuff
//
#include <acsThread.h>

//
// CORRCommon stuff
//
#include <rtToolsFifoCmd.h>

//
// CORR stuff
//

//
// Local stuff
//
#include "ArrayTimeImpl.h"

class ArrayTimeThread : public ACS::Thread
{
public:
    ArrayTimeThread(const ACE_CString &name,
		    ArrayTimeImpl *arrayTime_p) :
	ACS::Thread(name, 20000000, 10000000) /* response and sleep 2/1 seconds */
    {
	ACS_TRACE("ArrayTimeThread::ArrayTimeThread");

        m_arrayTime_p = arrayTime_p;
    }

    ~ArrayTimeThread()
    {
	ACS_TRACE("ArrayTimeThread::~ArrayTimeThread");
    }
    
    void onStart();

    void onStop();

    void runLoop();
    
private:
    ArrayTimeImpl *m_arrayTime_p;
    //
    // fifo command for communication with TE handler
    //
    rtToolsFifoCmd *m_fifoCmd_p;
};  
#endif /* ifndef ArrayTimeThread_H */

/*___oOo___*/
