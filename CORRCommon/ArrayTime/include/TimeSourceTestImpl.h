// @(#) $Id$
//
// Copyright (C) 2004
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Library General Public License
// as published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Library General Public License for more details.
//
// You should have received a copy of the GNU Library General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
//
// Correspondence concerning ALMA should be addressed as follows:
//
//        Internet email: alma-sw-admin@alma.org
//

#ifndef TimeSourceTestImpl_H
#define TimeSourceTestImpl_H

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

//
//CORBA servant header
//
#include <ControlInterfacesS.h>

//
//Contains the defintion of the standard superclass for C++ components
//
#include <acscomponentImpl.h>

#include <ControlExceptions.h>

class TimeSource: 
    public virtual acscomponent::ACSComponentImpl,
    public virtual POA_Control::TimeSource
{
public:
    TimeSource(const ACE_CString& name, maci::ContainerServices* cs);

    virtual ~TimeSource(void);

    virtual void execute()
        throw(acsErrTypeLifeCycle::LifeCycleExImpl);

    virtual void cleanUp();

    ACS::Time timeAtNextTE()
        throw (CORBA::SystemException);
};

#endif  /* TimeSourceTestImpl_H */






