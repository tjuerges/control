/*******************************************************************************
*
* "@(#) $Id$"
*
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* who       when        what
* --------  --------    ---------------------------------------------- * ramestic  2004-03-03  created
*/

/************************************************************************
*   NAME
*	TE_HandlerTest_k - kernel space test for TE_Handler
*
*   SYNOPSIS
* 
*   DESCRIPTION
*	This test reads from the clock TBX a serie of data blocks and
*	it records the following times:
*
*		o absolute time at last tick
*		o actual time at reception as computed based on the last received
*		  clock data and rt_get_time 
*		o actual time as returned by te_handler_get_time
*
*	After the N broadcasted clocks are received the test reports the
*	following times for each reception (all them in ns):
*
*		1. difference between actual time (rt_get_time based) and last tick
*		2. difference between rt_get_time based actual time and 
*		   te_handler_get_time
*
*	Therefore, the purpose of the test can be resumed as it follows:
*
*		o exercise the connection to the TE_Handler broadcasted messages
*		o measure the involved delay between reception and the actual
*		  time at which the tick was acknowledged by the handler (item
*		  1 above)
*		o measure the induced overhead when calling  te_handler_get_time
*	          (item 2 above). However, this measurment is not quite tru. Because
*		  te_handler_get_time is called later than rt_get_time. They cannot
*		  be called at the same time, of course.
*
*	The test task runs at priority 10.
*
*   FILES
*
*   ENVIRONMENT
*
*   RETURN VALUES 
*
*   CAUTIONS 
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS   
*
*------------------------------------------------------------------------
*/

/* 
 * System Headers
 */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/delay.h>

/*
 * RTAI specific
 */
#include <rtai_lxrt.h>
#include <rtai_registry.h>
#include <rtai_mbx.h>

/*
 * ACS stuff
 */
#include <rtLog.h>

/*
 * CORRCommon stuff
 */
#include <rtTools.h>

/* 
 * Local Headers
 */
#include "teHandler.h"

MODULE_AUTHOR("Rodrigo Amestica");
MODULE_DESCRIPTION("TE handler Test");
MODULE_LICENSE("GPL");

/*
 * global variables
 */
RT_TASK	task;
#define N	100
int debug = 1;
#define modName	"teHandlerTest_k"

void test_task(long not_used)
{
    int i, stat;
    RTIME t, times[N][4];
    RTIME gmin = 9999, gmax = 0, gaver = 0, rmin = 9999, rmax = 0, raver = 0;
    RTIME tmin = 9999, tmax = 0, taver = 0;
    MBX *te_clk_tbx;    
    teHandlerClock_t te_clk;
    unsigned int last_ticks = 0;
    rtlogRecord_t logRecord;

    RTLOG_INFO(modName, "test task started at cpu=%lld", rt_get_time());

    /*
     * check that teHandler absolute time at last tick is a multiple of TEacs
     */
    t = teHandlerGetLast();
    if ( llimd(t, 1, TEacs) * TEacs != t )
    {
	RTLOG_INFO(modName, "t0 modulus TEacs has a not null rest!");
    }
    else
    {
	RTLOG_INFO(modName, "t0 is an exact multiple of TEacs (%lld)", t);
    }
    
    /* 
     * gets pointer to TE clock mailbox
     */
    if ( (te_clk_tbx = (MBX *)rt_get_adr(nam2num(TE_HANDLER_CLK_MBX_NAME))) == 0 )
    {
	RTLOG_INFO(modName, "CANNOT GET TE CLK MAILBOX");
	return;
    }

    /*
     * receive a few clock data messages
     */
    rt_mbx_receive_if(te_clk_tbx, (void *)&te_clk, sizeof(te_clk));
    for ( i = 0; i < N; i++ )
    {
	/*
	 * wait for TE clock data
	 */
	if ( (stat = rt_mbx_receive_timed(te_clk_tbx, 
					  (void *)&te_clk, sizeof(te_clk),
					  nano2count(TEns + 100000LL)))
	     != 0 )
	{
	    RTLOG_INFO(modName, "mbx error (n/stat/cpu/t=%d/%d/%lld/%lld)",
                       i, stat, rt_get_time(), rt_get_time_ns());
	    break;
	}
	
	/*
	 * record the time diff between the tbx message was received
	 * and the last tick. We need now to convert between conunts to
	 * time units because the frequency is changing. [us]
	 */
	times[i][1] = llimd(rt_get_time() - te_clk.cpu_on_last_te,
			    1000000,
			    te_clk.cpu_hz);

	/*
	 * get the TE handler time [100us]
	 */
	times[i][2] = teHandlerGetTime();

	/*
	 * record the last tick count
	 */
	times[i][0] = te_clk.ticks;
	
	/*
	 * check for missing tick
	 */
	if ( (te_clk.ticks != last_ticks + 1) && last_ticks != 0 )
	{
	    RTLOG_INFO(modName, "tick missed (%u/%u)",
		      last_ticks, te_clk.ticks);
	    break;
	}
	last_ticks = te_clk.ticks;
    }
    
    if ( i == N )
    {
	/*
	 * now transform all recorded times to us:
	 *	0 - te handler at last tick
	 *	1 - tbx received
	 *	2 - get time call
	 */
	for ( i = 0; i < N; i++ )
	{
	    times[i][0] = llimd(te_clk.t0, 1, 10) + times[i][0] * (RTIME)TEus;
	    times[i][1] = times[i][0] + times[i][1];
	    times[i][2] = llimd(times[i][2], 1, 10);
	    times[i][3] = times[i][2] - times[i][0];

	    /* recv stats */
	    if ( rmin > times[i][1]-times[i][0] )
		rmin = times[i][1]-times[i][0];
	    if ( rmax < times[i][1]-times[i][0] )
		rmax = times[i][1]-times[i][0];
	    raver += times[i][1]-times[i][0];

	    /* get stats */
	    if ( gmin > times[i][2]-times[i][1] )
		gmin = times[i][2]-times[i][1];
	    if ( gmax < times[i][2]-times[i][1] )
		gmax = times[i][2]-times[i][1];
	    gaver += times[i][2]-times[i][1];

	    /* total stats */
	    if ( tmin > times[i][2]-times[i][0] )
		tmin = times[i][2]-times[i][0];
	    if ( tmax < times[i][2]-times[i][0] )
		tmax = times[i][2]-times[i][0];
	    taver += times[i][2]-times[i][0];

	    RTLOG_INFO(modName, "[%d] recv/get/total=%lld/%lld/%lld [us]",
		  i,
		  times[i][1]-times[i][0],
		  times[i][2]-times[i][1],
		  times[i][2]-times[i][0]);
	}
	raver = llimd(raver, 1, N);
	gaver = llimd(gaver, 1, N);
	taver = llimd(taver, 1, N);

	RTLOG_INFO(modName, "recv min/aver/max=%lld/%lld/%lld [us]",
	      rmin, raver, rmax);
	RTLOG_INFO(modName, "get min/aver/max=%lld/%lld/%lld [us]",
	      gmin, gaver, gmax);
	RTLOG_INFO(modName, "total min/aver/max=%lld/%lld/%lld [us]",
	      tmin, taver, tmax);

	if ( tmax <= 100 )
	{
	    RTLOG_INFO(modName, "RESULT: test set successfully executed");
	}
	else
	{
	    RTLOG_INFO(modName, "RESULT: test set failed (max=%lld)", tmax);
	}	
    }    
}

/*
 * kernel module handling
 */
int init_module(void)
{
    //void (*setRTLogLevelPtr)(RTLogLevelType); 
    rtlogRecord_t logRecord;
    
    RTLOG_INFO(modName, "MODULE INITIALIZING...");

    /*
     * TRACE logs are too many for this test
     */
    /*if ( (setRTLogLevelPtr = rtToolsLookupSymbol("rtLog", "setRTLogLevel")) == 0 )
    {
        RTLOG_ERROR(modName, "CANNOT GET setRTLogLevel FUNCTION POINTER");

        return 1;
    }
    setRTLogLevelPtr(RTLOG_DEBUG_LEVEL);*/

    /*
     * spawn the main task
     */
    if ( rt_task_init(&task, test_task, 0, 8000, 10, 1, 0)
	 != 0 )
    {
	RTLOG_ERROR(modName, "CANNOT SPAWN THE TEST TASK #1.");
	return 1;
    }
    
    /*
     * set the just created task as 'ready to run'
     */
    rt_task_resume(&task);
    
    RTLOG_INFO(modName, "MODULE INITIALIZED");

    return 0;
}

void cleanup_module(void)
{
    rtlogRecord_t logRecord;

    RTLOG_INFO(modName, "MODULE CLEANING UP...");
    
    /*
     * delete our main task
     */
    rt_task_delete(&task);
    
    RTLOG_INFO(modName, "MODULE CLEANED UP.");
}

/*___oOo___*/
