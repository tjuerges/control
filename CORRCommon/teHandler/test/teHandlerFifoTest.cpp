/*******************************************************************************
*
*
* "@(#) $Id$"
*
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* who       when        what
* --------  ----------  ----------------------------------------------
* ramestic  2006-11-20  created
*/

//
// System stuff
//
#include <stdlib.h>
#include <pthread.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <unistd.h>

//
// RTAI stuff
//
#include <rtai_sem.h>

//
// ACS stuff
//
#include <rtLog.h>

//
// COORCommon stuff 
//
#include <rtToolsFifoCmd.h>

// 
// Local Headers
//
#include "teHandler.h"

using namespace std;

//
// program parameters
//
int T = 10;
int N = 100;
bool V = false;

//
// global variables
//
pthread_t tasks[50];
int ex_count[50];
int unknown_ex_count[50];

void *testTask(void *idx)
{
    int myid = (int)idx;
    rtToolsFifoCmd *cmdFifo;
    int cmd = teHandlerCMD_GETTIME;
    unsigned long long t_te;

    if ( V )
    {
        cout << "task starting " << myid << endl;
    }

    //
    // create cmd object
    //
    try
    {
	cmdFifo = new rtToolsFifoCmd(TE_HANDLER_CMD_FIFO_DEV);
    }
    catch(...)
    {
	cout << "failed to create rtToolsFifoCmd instance!" << endl;

	return 0;
    }

    //
    // reset exception counters
    //
    unknown_ex_count[myid] = 0;
    ex_count[myid] = 0;

    //
    // use GETTIME cmd for I times
    //
    for ( int i = 0; i < N; i++ )
    {
        try
        {
            cmdFifo->sendRecvCmd(&cmd, sizeof(cmd),
                                 (void *)&t_te, sizeof(t_te),
                                 10);
        }
        catch ( rtToolsFifoCmdErr_t &ex )
        {
            if ( V )
            {
                printf("sendRecvCmd thrown exception (id/count/err=%d/%d/%d)!\n",
                       myid, i, (int)ex);
            }
            
            ex_count[myid]++;
        }
        catch ( ... )
        {
            if ( V )
            {
                printf("sendRecvCmd thrown unexpected exception (id/count=%d/%d)!\n",
                       myid, i);
            }

            unknown_ex_count[myid]++;
        }
        
        //
        // this sleeping should give us some ramdomness of the moments at which
        // each task will try to access the fifo increasing the chances that we
        // test simultaneus access to this fifo
        //
        usleep(myid * 1000);

        if ( V )
        {
            cout << "id/loop/time=" << myid << "/" << i << "/" << t_te << endl;
        }
    }

    //
    // delete command fifo object
    //
    delete cmdFifo;

    if ( V )
    {
        cout << "task " << myid << " terminating" << endl;
    }

    return 0;
}

void usage(void)
{
    cout << "usage: teHandlerFifoTest [-t <num-tasks>] [-n <num-iterations>] [-v]" << endl;
    cout << "default values:" << endl;
    cout << "\tnum-tasks=10" << endl;
    cout << "\tnum-iterations=100" << endl;
    cout << "\tverbose mode off by default" << endl;
}

int main(int argc, char** argv) 
{
    int i, c;

    //
    // discorver user parameters
    //
    while ( (c = getopt(argc, argv, "t:n:v")) != -1 )
    {
	switch (c)
	{
        case 't':
            T = atoi(optarg);
            break; 
        case 'n':
            N = atoi(optarg);
            break;
        case 'v':
            V = true;
            break;
	default:
            usage();
	    abort();
	}
    }

    //
    // check params
    //
    if ( T > 50 )
    {
        printf("too many tasks requested (req/max=%d/10)!\n", N);

        return 1;
    }

    //
    // log parameters
    //
    cout << "T=" << T << endl;
    cout << "N=" << N << endl;
    cout << "V=" << (V ? "true" : "false") << endl;

    //
    // start the tasks
    //
    for ( i = 0; i < T; i++ )
    {
	if ( pthread_create(&tasks[i],
			    0,
			    testTask,
			    (void *)i)
	     != 0 )
	{
	    printf("Cannot create task (%d)\n", i);

	    return 1;
	}
    }

    //
    // join them all
    //
    for ( i = 0; i < T; i++ )
    {
	if ( V )
        {
            printf("joining task %d\n", i);
        }
	pthread_join(tasks[i], 0);
        if ( V )
        {
            printf("task joined %d\n", i);
        }
    }

    //
    // print exceptions count
    //
    for ( i = 0; i < T; i++ )
    {
        printf("exception count id/ex/uex=%d/%d/%d\n",
               i, ex_count[i], unknown_ex_count[i]);
    }

    return 0;
}

/*___oOo___*/
