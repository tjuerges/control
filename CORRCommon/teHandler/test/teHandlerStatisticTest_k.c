/*******************************************************************************
*
* "@(#) $Id$"
*
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* who       when        what
* --------  --------    ---------------------------------------------- 
* ramestic  2004-03-03  created
*/

/************************************************************************
*   NAME
*	TE_HandlerTest_k - kernel space test for TE_Handler
*
*   SYNOPSIS
* 
*   DESCRIPTION
*
*   FILES
*
*   ENVIRONMENT
*
*   RETURN VALUES 
*
*   CAUTIONS 
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS   
*
*------------------------------------------------------------------------
*/

/* 
 * System Headers
 */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/delay.h>
#include <linux/vmalloc.h>
#include <math.h>

/*
 * RTAI specific
 */
#include <rtai_lxrt.h>
#include <rtai_registry.h>
#include <rtai_mbx.h>

/*
 * ACS stuff
 */
#include <rtLog.h>

/* 
 * Local Headers
 */
#include "teHandlerPrivate.h"

MODULE_AUTHOR("Rodrigo Amestica");
MODULE_DESCRIPTION("TE handler Test");
MODULE_LICENSE("GPL");

/*
 * global variables
 */
RT_TASK	task;
#define N	(100 * (0x1 << TE_HANDLER_SAMPLE_POWER_SIZE))
int debug = 1;
#define modName	"teHandlerStatisticsTest_k"

int   cpuHz0 = 0;
MODULE_PARM (cpuHz0, "i");

RTIME *times = 0;
RTIME *diff  = 0;

void test_task(long not_used)
{
    int i, stat;
    MBX *te_clk_tbx;    
    RTIME min, max, average, rms;
    teHandlerClock_t te_clk;
    unsigned int last_ticks = 0;
    rtlogRecord_t logRecord;

    RTLOG_INFO(modName, "%lld\n", rt_get_time());

    /* 
     * gets pointer to TE clock mailbox
     */
    if ( (te_clk_tbx = (MBX *)rt_get_adr(nam2num(TE_HANDLER_CLK_MBX_NAME))) == 0 )
    {
	RTLOG_INFO(modName, "CANNOT GET TE CLK MAILBOX\n");
	return;
    }

    /*
     * receive a few clock data messages
     */
    rt_mbx_receive_if(te_clk_tbx, (void *)&te_clk, sizeof(te_clk));
    if ( cpuHz0 == 0 )
    {
	cpuHz0 = te_clk.cpu_hz;
    }
    RTLOG_INFO(modName, "cpuHz0=%d", cpuHz0);

    for ( i = 0; i <= N; i++ ) /* note <= */
    {
	/*
	 * wait for TE clock data
	 */
	if ( i % 100 == 0 )
	{
	    RTLOG_INFO(modName, "waiting on tbx... (cpu/t=%lld/%lld/%lld\n",
	        rt_get_time(), rt_get_time_ns(), nano2count(TEns));
	}
	if ( (stat = rt_mbx_receive_timed(te_clk_tbx, 
					  (void *)&te_clk, sizeof(te_clk),
					  nano2count(TEns + 100000LL)))
	     != 0 )
	{
	    RTLOG_INFO(modName, "mbx error (stat/cpu/t=%d/%lld/%lld).\n",
	        stat, rt_get_time(), rt_get_time_ns());
	    break;
	}
	
	/*
	 * record cpu counts at each tick
	 */
	times[i] = te_clk.cpu_on_last_te;

	/*
	 * check for missing tick
	 */
	if ( (te_clk.ticks != last_ticks + 1) && last_ticks != 0 )
	{
	    RTLOG_INFO(modName, "tick missed (%u/%u)\n",
	        last_ticks, te_clk.ticks);
	    break;
	}
	last_ticks = te_clk.ticks;
    }
    
    if ( i == N + 1 )
    {
	/*
	 * compute cpu differences between ticks
	 */
	min = 999999999LL;
	max = 0;
	average = 0;
	for ( i = 0; i < N; i++ )
	{
	    diff[i] = times[i + 1] - times[i];
	    if ( min > diff[i] )
		min = diff[i];
	    if ( max < diff[i] )
		max = diff[i];
	    average += diff[i];
	}

	average = llimd(average, 1, N);

	/*
	 * compute the rms of the whole sample
	 */
	rms = 0;
	for ( i = 0; i < N; i++ )
	{
	    rms += (diff[i] - average) * (diff[i] - average);
	}
	rms = llimd(rms, 1, N - 1); 
	rms = (RTIME)sqrt((double)rms);
	
	/*
	 * print stats
	 */
	RTLOG_INFO(modName, "average/rms = %lld/%lld[us]",
	    llimd(average, 1000000, cpuHz0),
	    llimd(rms, 1000000, cpuHz0));
	RTLOG_INFO(modName, "min/max = %lld/%lld[us]",
	    llimd(min, 1000000, cpuHz0),
	    llimd(max, 1000000, cpuHz0));
    }
    
    RTLOG_INFO(modName, "task finished!\n");
}

/*
 * kernel module handling
 */
int init_module(void)
{
    rtlogRecord_t logRecord;

    /*
     * allocate memory for data buffers
     */
    if ( (times = vmalloc((N + 1) * sizeof(RTIME))) == 0 )
    {
	RTLOG_ERROR(modName, "cannot allocate times!");
	return 1;
    }
    if ( (diff = vmalloc((N + 1) * sizeof(RTIME))) == 0 )
    {
	RTLOG_ERROR(modName, "cannot allocate diff!");
	return 1;
    }

    /*
     * spawn the main task
     */
    if ( rt_task_init(&task, test_task, 0, 8000, 10, 1, 0)
	 != 0 )
    {
	RTLOG_INFO(modName, "CANNOT SPAWN THE TEST TASK #1.\n");
	return 0;
    }
    
    /*
     * set the just created task as 'ready to run'
     */
    rt_task_resume(&task);
    
    return 0;
}

void cleanup_module(void)
{
    rtlogRecord_t logRecord;

    RTLOG_INFO(modName, "MODULE CLEANING UP...\n");
    
    /*
     * delete our main task
     */
    rt_task_delete(&task);

    /*
     * release allocated memory
     */
    if ( times !=0 ) 
	vfree(times);
    if ( diff !=0 )
	vfree(diff);
    
    RTLOG_INFO(modName, "MODULE CLEANED UP.\n");
}

/*___oOo___*/
