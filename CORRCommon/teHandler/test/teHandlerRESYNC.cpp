/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* who       when      what
* --------  --------  ----------------------------------------------
* ramestic  2003-10-14  created
*/

/************************************************************************
*   NAME
*
*   SYNOPSIS
* 
*   DESCRIPTION
*
*   FILES
*
*   ENVIRONMENT
*
*   RETURN VALUES 
*
*   CAUTIONS 
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS   
*
*------------------------------------------------------------------------
*/

/*
 * System stuff
 */
#include <iostream>
#include <iomanip>

//
// COORCommon stuff 
//
#include <rtToolsFifoCmd.h>

// 
// Local Headers
//
#include "teHandler.h"

using namespace std;

int main(int argc, char** argv) 
{
    rtToolsFifoCmd *cmdFifo;

    //
    // create cmd object
    //
    try
    {
	cmdFifo = new rtToolsFifoCmd(TE_HANDLER_CMD_FIFO_DEV);
    }
    catch(...)
    {
	printf("failed to create rtToolsFifoCmd instance\n");
	return 1;
    }

    //
    // RESYNC, it should succeed (TE_HANDLER_CMD_STAT_OK) because
    // this test runs in SOFT mode and RESYNC is a dummy command under this
    // mode, always returning successfully without doing anything.
    //
    try
    {
	int reply;
        int cmd = teHandlerCMD_RESYNC;

	cmdFifo->sendRecvCmd(&cmd, sizeof(cmd), &reply, sizeof(reply), 2 * TEms);

        switch ( reply )
        {
        case TE_HANDLER_CMD_STAT_OK:
  
            cout << "RESYNC replied with OK" << endl;

            break;

        case TE_HANDLER_CMD_STAT_RESYNC_FAILED:

            cout << "RESYNC replied with the failed error code" << endl;;
            
            break;
        }
    }
    catch(rtToolsFifoCmdErr_t e)
    {
	delete cmdFifo;

	cout << "sendRecvCmd RESYNC thrown exception (" << e << ")" << endl;

	return 1;
    }

    delete cmdFifo;
    
    return 0;
}

/*___oOo___*/
