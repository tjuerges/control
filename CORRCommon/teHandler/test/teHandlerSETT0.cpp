/*******************************************************************************
*
* "@(#) $Id$"
*
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* who       when      what
* --------  --------  ----------------------------------------------
* ramestic  2003-10-14  created
*/

/************************************************************************
*   NAME
*
*   SYNOPSIS
*
*   DESCRIPTION
*
*   FILES
*
*   ENVIRONMENT
*
*   RETURN VALUES
*
*   CAUTIONS
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS
*
*------------------------------------------------------------------------
*/

//
// System stuff
//
#include <ctime>
#include <iostream>
#include <iomanip>
#include <System_Time.h>

//
// COORCommon stuff
//
#include <rtToolsFifoCmd.h>

//
// Local Headers
//
#include "teHandlerPrivate.h"

using namespace std;

int main(int argc, char** argv)
{

    int reply, offset = 0;
    unsigned long long t;
    teHandlerClock_t clk;
    rtToolsFifoCmd *cmdFifo;

    //
    // if there is an argument then take it as an offset for setting
    // t0. The offset is a number of ticks to add to the expected time
    // for the next tick. The net result is that aftre the command the
    // distributed clock will be 'offset' ticks away from its original
    // time.
    //
    if ( argc > 1 )
    {
        offset = atoi(argv[1]);
    }
    cout << "offset ticks = " << offset << endl;

    //
    // create cmd object
    //
    try
    {
        cmdFifo = new rtToolsFifoCmd(TE_HANDLER_CMD_FIFO_DEV);
    }
    catch(...)
    {
        cout << "failed to create rtToolsFifoCmd instance" << endl;

        return -1;
    }

    //
    // GETTIME
    //
    try
    {
        int cmd = teHandlerCMD_GETTIME;

        cmdFifo->sendRecvCmd(&cmd, sizeof(cmd), (void *)&t, sizeof(t), 60);
    }
    catch ( ... )
    {
        delete cmdFifo;

        cout << "GETTIME thrown an exception" << endl;
        
        return -1;
    }

    //
    // sleep until the next TE boundary + 1ms
    //
    ACE_OS::sleep(ACE_Time_Value(0, 1000 + (TEacs - (t % TEacs)) / 10));

    //
    // get actual clock data with GETCLK
    //
    try
    {
        int cmd = teHandlerCMD_GETCLK;

        cmdFifo->sendRecvCmd(&cmd, sizeof(cmd), (void *)&clk, sizeof(clk), 50);
    }
    catch ( ... )
    {
        cout << "GETCLK thrown an exception" << endl;

        return -1;
    }

    //
    // SETT0
    //
    try
    {
        rtToolsCmdBuffer<teHandlerSetT0Cmd_t> cmd(teHandlerCMD_SETT0);

        cmd->value = (clk.ticks + 1 + offset) * TEacs + clk.t0;
        cmd->tick = clk.ticks;

        cmdFifo->sendRecvCmd(cmd.pack(), cmd.size(), &reply, sizeof(reply), 50);
    }
    catch(rtToolsFifoCmdErr_t e)
    {
        delete cmdFifo;

        cout << "sendRecvCmd SETT0 has failed (ex=" << e << ")" << endl;

        return -1;
    }
    catch ( ... )
    {
        delete cmdFifo;

        cout << "sendRecvCmd SETT0 thrown unexpected exception" << endl;

        return -1;
    }

    //
    // check SETT0 reply
    //
    if ( reply != TE_HANDLER_CMD_STAT_OK )
    {
        delete cmdFifo;

        cout << "SETT0 failed with error code " << reply << endl;

        return -1;
    }

    delete cmdFifo;
    
    return 0;
}

/*___oOo___*/
