/*******************************************************************************
*
* $Id$
*
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
*/

/************************************************************************
*   NAME
*	teHandlerTimeTest_k
*
*   SYNOPSIS
* 
*   DESCRIPTION
** Get time from teHandler & O/S. Compare the 2 and look for drift
** This test measures 3 things:
** 1. The average difference between the time obtained via teHandlerGetTime() 
**    and do_gettimeofday().
** 2. The average rate of change of the difference between the teHandler & 
**    OS time.
** 3. The 'distribution' of the time difference. This is basically a count
**    of the number of times the OS time > teHandler time (distribution > 0)
**    or OS time < teHandler time (distribution < 0). If there were no drift,
**    distrbution would be 0.
** These values are displayed in the average difference in microseconds and the 
** rate of change in microseconds per second.
** The output (in /var/log/messages) looks like this
** Mean diff: 41709 usec rateOfChange: 21 (usecs/sec) distribution: 10
** 
** To run this test: loadLkmModule testTeHandlerTimeTest.lkm
*------------------------------------------------------------------------
*/

/* 
 * System Headers
 */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/delay.h>

/*
 * RTAI specific
 */
#include <rtai_lxrt.h>

/*
 * ACS stuff
 */
#include <rtLog.h>

/* 
 * Local Headers
 */
#include "teHandler.h"
#include "teHandlerPrivate.h"

MODULE_AUTHOR("Jim Pisano");
MODULE_DESCRIPTION("TE handler Time Test");
MODULE_LICENSE("GPL");

/*
 * global variables
 */
RT_TASK	task;
int bRunTest;
#define modName	"teHandlerTimeTest_k"
#define RTIME_100_MS	nano2count(100000000LL)

/*
 * return current linux time in acs-units
 */
RTIME getSysTime(void)
{
    struct timeval tv;
    do_gettimeofday(&tv);

    return (RTIME)tv.tv_sec * 10000000LL + (RTIME)tv.tv_usec * 10LL + TE_HANDLER_UNIX_OFFSET;
}

void test_task(long not_used)
{
    RTIME teHandlerTime, osTime;
    long long int timeDiff, averTimeDiff, prevDiff, rateOfChange;
    int loops, distribution;
    rtlogRecord_t logRecord;

    RTLOG_INFO(modName, "%lld\n", rt_get_time());

    timeDiff = averTimeDiff = prevDiff = rateOfChange = 0LL;
    loops = 0;
    // track the count that osTime > teHandlerTime. 
    // If dist > 0 
    //   osTime > teHanderTime over 10 iterations
    // else if dist < 0
    //   osTime < teHandlerTime over 10 iterations
    // else if dist == 0
    //   # of times osTime > teHandlerTime = # of times
    //   osTime < teHandlerTime 
    distribution = 0;

    while( bRunTest )
    {
      // get tehandler's, os time & compute the difference
      teHandlerTime = teHandlerGetTime();
      osTime = getSysTime();
      // as we're subtracting unsigned long longs, we must take into
      // account the magnitudes and add or subtract accordingly to the 
      // running sum
      if( osTime > teHandlerTime )
      {
          timeDiff = (osTime - teHandlerTime);
          // sum up differences to get average over 10 iterations
          averTimeDiff += timeDiff;
          distribution++;
      }
      else
      {
          timeDiff = (teHandlerTime - osTime);
          // sum up differences to get average over 10 iterations
          averTimeDiff -= timeDiff;
          distribution--;
      }
      // Keep track of average rate of change of the difference
      if( prevDiff != 0LL )
          rateOfChange += (timeDiff - prevDiff);

      prevDiff = timeDiff;

      loops++;
      if( !(loops % 10) )
      {
          rt_printk("Mean diff: %lld usec rateOfChange: %lld (usecs/sec) distribution: %d\n",
                     llimd(averTimeDiff,1,100), llimd(rateOfChange, 1,100), distribution);
          averTimeDiff = rateOfChange = 0LL;
          distribution = 0;
      }
      rt_sleep(RTIME_100_MS);
    }
    
    RTLOG_INFO(modName, "teHandlerTimeTest task finished!\n");
}

/*
 * kernel module handling
 */
int init_module(void)
{
    rtlogRecord_t logRecord;

    bRunTest = 1;
    /*
     * spawn the main task
     */
    if ( rt_task_init(&task, test_task, 0, 8000, 10, 1, 0)
	 != 0 )
    {
	RTLOG_INFO(modName, "CANNOT SPAWN THE TEST TASK #1.\n");
	return 0;
    }
    
    /*
     * set the just created task as 'ready to run'
     */
    rt_task_resume(&task);
    
    return 0;
}

void cleanup_module(void)
{
    rtlogRecord_t logRecord;

    bRunTest = 0;
    RTLOG_INFO(modName, "teHandlerTimeTest_k MODULE CLEANING UP...\n");
    
    /*
     * delete our main task
     */
    rt_task_delete(&task);
    
    RTLOG_INFO(modName, "teHandlerTimeTest_k MODULE CLEANED UP.\n");
}
