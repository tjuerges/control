DESCRIPTION TEST 1
user space test that exercises the fifo command interface.
1.1 RESYNC should suceed because we are in SOFT mode.
1.2 GETTIME is used several times and the result compared with the os time
(ACE_OS::gettimeofday). Average, rms, min and max values of the differnce
are then computed and printed out. The sed code replaces this numbers
by dummy characters. These values should normally be smaller than
100us and you can see them by running teHandlerTest manually after
loading the corresponding lkm file.
1.3 It finally exercises the SETT0 command. For this purpose the test
first gets the clock data (GETCLK) and with that information issues
SETT0 with a timestamp that matches the current time at next
tick. Then uses GETCLK again to verify the status has changed to
array-time and that the t0 value has not changed.
OUTCOME: in stdout timing values, otherwise error logs

DESCRIPTION TEST 2
This is a kernel space task and it exercises the module's API at that
level. First it gets the time at last tick and checks that the value
is a multiple of 48ms. Then it receives several messages on the clock
data mailbox and keeps track of the timing for receiving each
message; reportin average, min and max values.
OUTCOME: in /var/log/messages timing values, otherwise error logs.

DESCRIPTION TEST 3
3.1 The idea of this test is to exercise the concurrency capability of the
module's fifo interface. For doing so the program spawns a series of
tasks which start requesting the TE time (GETTIME) for a number of
iterations, all them running 'simultaneously'. The out come can be
read on the stdout of the user space program. There must be no
exceptions reported.
3.2 The same exercise is also applied to the lxrt interface of the
module, that is, instead of using a fifo command for getting the time
the test in second stage uses the lxrt api of the module for
requesting the TE time (teHandlerGetTime).

DESCRIPTION TEST 4
standard endurance test. It loads the base set of modules and then it
loads and unloads teHandler for 100 times. 
OUTCOME: in /var/log/messages load/unload messages, or error logs.
