/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* who       when      what
* --------  --------  ----------------------------------------------
* ramestic  2003-10-14  created
*/

/************************************************************************
*   NAME
*	TE_HandlerTest - user space test for TE_Handler
*
*   SYNOPSIS
* 
*   DESCRIPTION
*	Based on rt_get_time it measures the latency of this task getting unblocked
*	from the TBX since the time the tick was acutally handled by the TE_Handler
*	task. This process waits for N TBX messages, records the time difference
*	between rt_get_time and the received clock data and when finished it outputs
*	the min/mean/max values of this difference.
*
*   FILES
*
*   ENVIRONMENT
*
*   RETURN VALUES 
*
*   CAUTIONS 
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS   
*
*------------------------------------------------------------------------
*/

/*
 * System stuff
 */
#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#include <iostream>
#include <iomanip>
#include <System_Time.h>

/*
 * RTAI stuff
 */
#include <rtai_fifos.h>

//
// COORCommon stuff 
#include <rtToolsFifoCmd.h>

/* 
 * Local Headers
 */
#include "teHandler.h"

using namespace std;

int main(int argc, char** argv) 
{
    struct timeval tv;
    teHandlerClock_t clk;
    rtToolsFifoCmd *cmdFifo;

    //
    // create cmd object
    //
    try
    {
	cmdFifo = new rtToolsFifoCmd(TE_HANDLER_CMD_FIFO_DEV);
    }
    catch(...)
    {
	printf("failed to create rtToolsFifoCmd instance\n");
	return 1;
    }

    printf("teHandlerTest started...\n");

    /*
     * get current time
     */
    if ( gettimeofday(&tv, 0) )
    {
	delete cmdFifo;
	printf("failed to get time of day!");
	return 1;
    }

    //
    // RESYNC, it should succeed (TE_HANDLER_CMD_STAT_OK) because
    // this test runs in SOFT mode and RESYNC is a dummy command under this
    // mode, always returning successfully without doing anything.
    //
    try
    {
	int reply;
        int cmd = teHandlerCMD_RESYNC;

	cmdFifo->sendRecvCmd(&cmd, sizeof(cmd), &reply, sizeof(reply), 1);

        switch ( reply )
        {
        case TE_HANDLER_CMD_STAT_OK:
            printf("RESYNC replied with OK\n");
            break;
        case TE_HANDLER_CMD_STAT_RESYNC_FAILED:
            printf("RESYNC replied with the failed error code\n");
            break;
        }
    }
    catch(rtToolsFifoCmdErr_t e)
    {
	delete cmdFifo;
	printf("sendRecvCmd RESYNC thrown exception (%d)!\n", e);
	return 1;
    }

    //
    // GETTIME
    //
    try
    {
	int N = 50;
	long long diff[N];

	cout << "GETTIME vs gettimeofday:" << endl;

	for ( int i = 0; i < N; i++ )
	{
	    int cmd = teHandlerCMD_GETTIME;
	    ACE_Time_Value t_sys_v;
	    unsigned long long t_te, t_sys = 0;

	    cmdFifo->sendRecvCmd(&cmd, sizeof(cmd), (void *)&t_te, sizeof(t_te), 60);

	    //
	    // Linux system time in ACS units (watch out with the zeros!!!)
	    // Note that ACE msec() works in a wierd way, thefore, not used here!
	    //
	    t_sys_v = ACE_OS::gettimeofday();
	    t_sys = 
		t_sys_v.sec() * 10000000ULL +
		t_sys_v.usec() * 10ULL +
		122192928000000000ULL;
	 
	    if ( t_sys >= t_te )
	    {
		diff[i] = (t_sys - t_te) * 100LL / 1000000LL; /* in ms */
	    }
	    else
	    {
		diff[i] = (t_te - t_sys) * 100LL / 1000000LL; /* in ms */
	    }
	}

	//
	// compute min, max and average of differences
	//
	float min = 99999999, aver = 0, rms = 0, max = -99999999;
	for ( int i = 0; i < N; i++ )
	{
	    if ( diff[i] < min )
	    {
		min = diff[i];
	    }
	    if ( diff[i] > max )
	    {
		max = diff[i];
	    }

	    aver += diff[i];
	}
	aver /= N;

	//
	// compute rms
	//
	for ( int i = 0; i < N; i++ )
	{
	    rms = (diff[i] - aver) * (diff[i] - aver);
	}
	rms = sqrt(rms / (N - 1));
	
	cout << setiosflags(ios::fixed | ios::showpoint) << setprecision(3);
	cout << "GETTIME: " << "min/aver/max=" << min << "/" << aver << "/" << max << " [ms]" << endl;
	cout << "GETTIME: " << "rms=" << rms << " [ms]" << endl;
    }
    catch(rtToolsFifoCmdErr_t e)
    {
	delete cmdFifo;
	printf("sendRecvCmd GETTIME has failed (exp=%d)!\n", e);
	return 1;
    }

    //
    // send command GETCLK
    //
    try
    {
	int cmd = teHandlerCMD_GETCLK;
	cmdFifo->sendRecvCmd(&cmd, sizeof(cmd), (void *)&clk, sizeof(clk), 60);
	printf("GETCLK: received tick/t0=%u/%lld\n", clk.ticks, clk.t0);

	if ( clk.t0 % TEacs == 0 )
	{
	    printf("GETCLK: t0 is a multiple of TE (%lldms)\n", TEms);
	}
	else
	{
	    printf("GETCLK: t0 is NOT a multiple of TE\n");
	}
    }
    catch(rtToolsFifoCmdErr_t e)
    {
	delete cmdFifo;
	printf("sendRecvCmd GETCLK has failed (exp=%d)!\n", e);
	return 1;
    }

    //
    // send command SETT0
    //
    try
    {
	int reply;
	rtToolsCmdBuffer<teHandlerSetT0Cmd_t> cmd(teHandlerCMD_SETT0);

	cmd->value = clk.t0 + (RTIME)(clk.ticks + 1) * (RTIME)TEacs;
	cmd->tick = clk.ticks;
	printf("SETT0 params - tick/t0=%u/%lld\n", cmd->tick, cmd->value);
	cmdFifo->sendRecvCmd(cmd.pack(), cmd.size(), &reply, sizeof(reply), 60);
	printf("SETT0: reply=%d\n", reply);
    }
    catch(rtToolsFifoCmdErr_t e)
    {
	delete cmdFifo;
	printf("sendRecvCmd SETT0 has failed (exp=%d)!\n", e);
	return 1;
    }

    //
    // GETCLK again, just to confirm that t0 is still multiple of TE
    //
    try
    {
	int cmd = teHandlerCMD_GETCLK;
	cmdFifo->sendRecvCmd(&cmd, sizeof(cmd), (void *)&clk, sizeof(clk), 60);
	printf("GETCLK: received tick/t0=%u/%lld\n", clk.ticks, clk.t0);

	if ( clk.t0 % TEacs == 0 )
	{
	    printf("GETCLK: t0 is a multiple of TE (%lldms)\n", TEms);
	}
	else
	{
	    printf("GETCLK: t0 is NOT a multiple of TE\n");
	}
    }
    catch(rtToolsFifoCmdErr_t e)
    {
	delete cmdFifo;
	printf("sendRecvCmd GETCLK has failed (exp=%d)!\n", e);
	return 1;
    }

    delete cmdFifo;
    
    return 0;
}

/*___oOo___*/
