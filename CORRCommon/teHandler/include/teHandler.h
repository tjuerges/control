#ifndef teHandler_H
#define teHandler_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* ramestic  2003-10-14  created
*/

/** @file teHandler.h
 *  @brief TE handler kernel module API.
 *
 * The TE handler module provides a software facility for synchronizing the local
 * host clock to the Array Time.
 *
 * The timing bus delivers TEs at a 48 ms period and are accurate to about 4 ns.
 * Each computer which will utilize this timing bus, must be physically connected
 * so that the pulses generate a software interrupt, which is the way for the TE
 * handler to finally receive those events. The hardware details for connecting to
 * the hardware signal are not part of the TE handler module itself. The TE
 * handler simply provides an interface for configuring the kernel module that will
 * actually implement the hardware connectivity and deliver the TE interrupts. In
 * what it follows, that kernel module is named as the 'hardware module'.
 *
 * The TE handler module is divided into two major components:
 *
 *	1. a TE events handler function, which performs two basic operations at
 *         each hardware interrupt event:
 *
 *		· read current TSC counts, and
 *		· give a dedicated synchronization semaphore
 *
 *	2. Main task, which synchronizes with the ISR via the semaphore
 *	   mentioned above. Each time the semaphore is given by the ISR and taken
 *	   by the main task the CPU frequency is computed and relevant data
 *	   (CPU frequency, TSC at last TE interrupt, etc.) is writen into a
 *	   dedicated RTAI mailbox. It is assumed that this task is running
 *	   alone at the highest real-time priority, anything else could provoke
 *	   time jittering.
 *
 * Hardware Interrupts.
 *
 * The TE handler module does not implement any specific interface with any 
 * type of hardware TE signal. Instead, it provides a software interface for
 * 'plugging-in' the functionality for dealing with hardware ticks implemented
 * by an external hardware module. The TE handler provides a function that
 * must be called at each TE interrupt. This function is very simply and does not
 * include any hardware details. It is meant as a function to be called from
 * interrupt level by the hardware module. However, there is no provision on how
 * exactly this handler function must actually be executed at each TE tick. On one
 * hand, the hardware module could install the handler function directly as a proper
 * ISR. But it is also foreseen that the hardware module will install its own ISR,
 * and within that ISR call the TE handler function when a TE interrupt has 
 * been detected. In this way some flexibility is left open for the hardware
 * module to implement other details that are not necessarily related with
 * the handling of a TE signal. In this case, it is important to warranty that the
 * latency for actually calling the TE handler function is kept to a minimum after
 * each rising edge of the TE signal; otherwise, the whole time keeping mission will
 * be in danger.
 *
 * Note: the TE handler function does not contain any hardware detail,
 * therefore, if the specific hardware at hand would require some interrupt 
 * acknowledgment from within the ISR then the TE handler function could not
 * be used as a proper ISR. At this moment, the only known situation at which
 * the TE handler function could be used as a proper ISR is when the TE signal is
 * connected through the ACK pin of a standard PC parallel port.
 *
 * Clock Data
 * The basic clock parameter measured by the TE handler is the number of CPU
 * clock counts between consecutive TEs, which is computed as the difference
 * between the CPU's TSC - recored by the ISR - at the current tick and the
 * one recored at the previous one. Each measurement is kept by the main task in
 * a ring buffer, which is primed during the initialization phase of the module
 * and updated at each TE. As for filtering out jittering effects provoked by
 * latency variations on the ISR execution time the buffer has a size of 32 samples,
 * and it is the average of this sample set the actual figure used for further
 * computations:
 *
 *	cpu_n[i] TSC difference between consecutive TEs
 *	cpu_n_mean = sum(cpu_n[i]) / N
 *	cpu_n_var = sum((cpu_n[i] - cpu_n_mean)^2) / (N - 1)
 *	cpu_n_std = sqrt(cpu_n_var)
 *	cpu_hz = cpu_n_mean / 48ms 
 *
 * Clock Data Validation
 * In order to add some robustness to the operation of the TE handler is necesary
 * to add a barrier between received time events and the sample buffer. Suspicios
 * TEs must not be processed and an action must be taken. In order to do this
 * the TE handler defines a so called allowable jittering interval, during which
 * the next tick must happen. If we call cpu_on_next_te the expected TSC at which
 * we expect the next tick to occur, and its actual value is extrapolated as
 *
 *	cpu_on_next_te = cpu_on_last_te + cpu_n_mean
 *
 * then the definition of the allowable jittering interval would be the following:
 *
 *	[cpu_on_next_te  - TE_HANDLER_ACCEPTED_DEV_N * cpu_n_std,
 *	 cpu_on_next_tec + TE_HANDLER_ACCEPTED_DEV_N * cpu_n_std]
 *
 * where TE_HANDLER_ACCEPTED_DEV_N its a constant (10).
 *
 * However, this would mean that for better and better TE statistics (lower standard
 * deviation) the allowable jittering interval would get narrower and narrower,
 * bringing the system to a too stringent validation level. To avoid this situation
 * the actual width of the interval is clipped to a minimum value which can be
 * defined at run time during the kernel module installation. For details see the
 * parameters section at the end.
 * 
 * Once the limits of the allowable interval are defined then they are applied for 
 * validation. The function call for waiting on the ISR synchronization semaphore
 * passes as timeout parameter the upper limit of interval, and there are three
 * possible scenarios after blocking on that call:
 *
 *	1. the semaphore is taken but too early, that is to say, at a TSC smaller
 *	   than the lower limit of the interval. In this case, the handler assumes
 *	   that a spurious tick has happened and it is simply discarded without
 *	   further actions (TBD).
 *	2. the semaphore is taken and the current TSC lays within the interval,
 *	   the handler assumes that this is a good tick and the sample buffer is
 *	   updated accordingly, the clock data is computed and written into the
 *	   dedicated mail-box.
 *	3. the call actually times out, in this case the handler assumes that
 *	   a tick has been missed, and the operational mode is switched to
 *	   free-wheeling. See below for more details on this mode.
 *
 * Finally, the TE handler will also apply a validation on the history of the
 * sample buffer. The validation described in the previous paragraphs takes care
 * of very fast abnormal events, like a spurious tick or missing one. But on the
 * other hand, it is also foreseeable to be able to detect slower artifacts in the
 * train of time events. For doing so the TE handler implements a checking on the
 * standard variance of the sample buffer. An extra parameter accepted by the module
 * at loading time is the maximum allowable value for the standard variance; if the
 * current value (after ingesting the present tick) is bigger than that value then
 * the handler will abruptly, but cleanly, abort its operation. See the module 
 * parameters section for further details; in particular, note that it is possible
 * to set the maximum allowable variace in such a way that the TE handler will not
 * abort operation of a too big variance but just logged.
 *
 * Operational Modes
 * The TE handler defines two major modes of operation:
 *
 *	1. SOFT: in this mode there is no hardware source for the TEs, and a
 *         fictitious tick is followed. This fictitious tick agrees with 
 *         each system time boundary at 48ms, which translates in the TE
 *         handler actually following the system time.
 *	    
 *	2. HARD: hardware TEs are delivered to the handler module via the 
 *	   synchronization semaphore.
 *
 *      3. FREE-WHEELING: the TE handler switches to FW from HARD mode when
 *         the hardware signal simply disappears or when its phase shifts
 *         beyond the allowed jittering interval. In this mode, in the same
 *         way as in SOFT mode, the TE handler starts following the system
 *         time.
 *
 * In HARD mode the handler initializes by priming the sample buffer before writing
 * clock data into the dedicated mail-box. Once the buffer is full and computed
 * variables are validated successfully the handler starts writing on the dedicated
 * mail-box. If a tick is missed then handler goes into the FREE_WHEELING sub-mode. 
 *
 * In FREE-WHEELING mode the handler is assuming that for some unknown reason the
 * hardware ticks had stopped to be delivered (unplugged cable, frequency generator
 * switched off, etc.), but is expectable that the ticks should resume shortly. 
 * That is to say, the FREE_WHEELING mode is meant as a solution for intermitent
 * unavailability of the timing signal. While in FREE_WHEELING mode the handler
 * will operate like in SOFT mode, simulating a fictisious ticking, in this case,
 * based on the last available estimation of the CPU frequency.
 *
 * As soon as the hardware signal becomes available again the handler will try to
 * resynchronize and switch back to the HARD mode. Re-synchronization could 
 * actually fail when the new tick comes back outside the allowable jittering
 * interval which is still computed based on the last available cmpu_n_mean value.
 * This case of re-synchronization failure is very likely to happen everytime
 * the timing signal has not been available for a long time, and the inherent
 * drifting of the CPU clock has moved too far away from the hardware ticks. In this
 * case the handler will simply discard the tick and it will remain FREE-WHEELING. 
 *
 * Array Time
 * 
 * Linux Time
 *
 * Kernel Module Parameters
 *
 * @param hwMod a string with the name of an already loaded kernel module (the
 * hardware module as defined before) that implements the interrupt handling of
 * the time event signal. The given kenel module must implement and export two
 * functions with the following exact prototypes and names:
 *
 *   - int enableTE(void (*handler)(int)): enable interrupts
 *   - int disableTE(void): disable interrupts
 *
 * The TE handler does not have any knowledge and does not care about the details
 * involved in disabling and enabling the ISR. The hardware module takes
 * care of those details. However, it must be noticed that the TE handler blocks on
 * a semaphore when waiting for the next tick, therefore, the ISR implementation
 * must take care of invoking the teHandler function ('handler' parameter
 * passed to enableTE) immediately after a tick has arrived. Otherwise, an unknown
 * latency will be introduced into the system. It could also be that the
 * hardware module installs 'handler' as a proper ISR, but that expedient will
 * only work when there is no interrupt acknowlegment to take care of; 'handler'
 * cannot do that because the teHandler has no knowlege of the details of the
 * underalying hardware. If 'handler' is installed as a proper ISR then
 * the received integer parameter will be positive, and the function will
 * re-enabled interrupts (rt_enable_irq(irq)) before returning. If the handler
 * is invoked as a function from within the hardware module ISR then it must be
 * invoked passing a negative number.
 * @param maxJitClip an integer value in microseconds. If set to any positive number
 * then the maximum allowed jittering (timeout on sem) cannot be less than
 * maxJitClip. If this parameter is not passed then apply no clipping
 * (maxJit = N*std). A negative value has no meaning. When the input signal is
 * really stable the computed rms could go really low (close to zero), making the
 * N*rms criteria too much stringent. This parameter helps maintaining a reasonable
 * balance.
 * @param maxStd an integer number in microseconds. When the computed std dev is
 * bigger than this value then validation fails. If the parameter is not passed
 * to the module then use original 10us. If the passed value is negative then just
 * log the event but fake validation to okay.
 * @param setTimePeriod an unsigned integer. Periodicity (in ticks count) at which
 * the system time offset must be measure and eventul correction applied. 1 means
 * at every tick, which could lead to xtime_lock locking problems. It's operational
 * value should be adjusted based on the observed sys time drift.
 */

#include <rtai_types.h>

/*
 * sample buffer size. In power of 2. It is very efficient to adopt this power
 * scheme because division for computing an average over the values in the
 * sample buffer reduces to a bit shifting.
 */
#define TE_HANDLER_SAMPLE_POWER_SIZE	5
#define TE_HANDLER_SAMPLE_SIZE          (0x1 << TE_HANDLER_SAMPLE_POWER_SIZE)

/*
 * RAM - I do not know how to use limit.h, therefore, I'm harcoding this value here.
 */
#define TE_HANDLER_ACS_TIME_MAX	(184467440737095516ULL)

/*
 * public API (LXRT)
 */
#define TE_HANDLER_GETTIME	0
#define TE_HANDLER_GETLAST	1
#define TE_HANDLER_GETMODE	2
#define TE_HANDLER_GETCLK       3
#define TE_HANDLER_GETOFFSET    4

/*
 * some constants used too often
 */
#define TEns			(48000000LL)
#define TEus			(48000LL)
#define TEms			(48LL)
#define TEacs			(480000LL)
#define TEBY2ns			(24000000LL)

/*
 * Typed mailbox used for broadcasting the clock data at each tick.
 */
#define TE_HANDLER_CLK_MBX_NAME	"TEMBX"

/*
 * TE always runs in one of the following modes:
 *	SOFT : no hardware tick received so far
 *	HARD : ticking on hardware ticks
 *	FW : it was HARD at some moment in the past but the ticks has stopped
 */
#define TE_HANDLER_MODE_SOFT		0
#define TE_HANDLER_MODE_HARD		1
#define TE_HANDLER_MODE_FW      	2

/*
 * the actual value of t0 has tow possible sources:
 *	LOCAL : read from the local CPU at the moment the module was loaded
 *	ARRAY : settled by external application via the provided API (te_handler_set_t0)
 */
#define TE_HANDLER_TYPE_LOCAL		0
#define TE_HANDLER_TYPE_ARRAY		1

/*
 * for communication with the external world. This clock data structure 
 * is published into a mail-box each time a TE ticks is handled or an error
 * is detected.
 */
typedef struct teHandlerClock
{
    int			mode;		/* SOFT, HARD or FW			*/
    int			type;		/* LOCAL or ARRAY			*/
    RTIME		t0;		/* abosulte time at tick zero (ACS)	*/
    RTIME		cpu_on_last_te;	/* TSC at last TE			*/
    RTIME		cpu_on_next_te; /* extrapolated TSC at next TE		*/
    unsigned int	ticks;		/* TE counts since initialization	*/
    unsigned int	cpu_hz;	/* estimated CPU frequency		*/
    unsigned int        resync_count; /* RESYNC counter                   */
} teHandlerClock_t;

/*
 * system time offset data, used by normal linux task for periodic
 * adjustments.
 */
typedef struct TE_SysOffset
{
    RTIME cpu;                          /* computed at this cpu timestamp        */
    RTIME sys;                          /* system time at last measurement [ACS] */
    RTIME value;                        /* current offset value (TE-sys) [us]    */
    RTIME start;                        /* first ever measured offset [us]       */
    RTIME rms, min, max;                /* statistic in [us]                     */
    unsigned int set_count;             /* set time counter                      */
    unsigned int set_err_count;         /* settime error counts                  */
    unsigned int access_err_count;      /* failed to access clk data counts      */
    unsigned int glitch_err_count;      /* linux time looks wrong                */
} teHandlerSysOffset_t;

/*
 * possible fifo commands to receive
 */
typedef enum
{
    teHandlerCMD_SETT0 = 0,
    teHandlerCMD_GETCLK,
    teHandlerCMD_GETTIME,
    teHandlerCMD_RESYNC,
    teHandlerCMD_GETOFFSET
}teHandlerCmdIdx;

/*
 * fifo cmd replay values
 */
typedef enum
{
    TE_HANDLER_CMD_STAT_OK = 0,
    TE_HANDLER_CMD_STAT_WRONG_TICK,
    TE_HANDLER_CMD_STAT_INVALID_T0,
    TE_HANDLER_CMD_STAT_RESYNC_FAILED,
    TE_HANDLER_CMD_STAT_UNKNOWN
}teHandlerFifoCmdStat_t;

/*
 * used for receiving a new T0 value from user space via a fifo.
 * 'value' must contain the ACS time stamp for the next tick, and
 * 'tick' must contain the tick at which the command is being issued.
 * That is, value would normally contain the time stamp received back from
 * the TimeSource component and tick is the current ticks count. Tick is
 * used for validation the round trip condition of the command, that is, no
 * tick should have elapsed between the moment the command was issued and the
 * moment the command is actually received by the kernel module. The time stamp
 * value must be a multiple of 48ms.
 */
typedef struct teHandlerSetT0Cmd
{
    RTIME value;
    unsigned int tick;
}teHandlerSetT0Cmd_t;

/*
 * public API
 */
#ifndef __KERNEL__

#include <rtTools.h>

#ifdef __cplusplus
extern "C" {
#endif
    //! gets actual clock data internally kept by the handler task.
    /*!
      This function blocks on the main access semaphore. After acquiring the
      semaphore it reads the internal clock data structure and returns it
      to the caller copying it into the passed parameter pointer.
      This function has a symmetric interface for user and kernel spaces.
     */
    inline int teHandlerGetClock(teHandlerClock_t *data)
    {
        struct
        {
            teHandlerClock_t *data;
            int sz;
        } arg = { data, sizeof(teHandlerClock_t) };

        return rtai_lxrt(TE_HANDLER_IDX, SIZARG, TE_HANDLER_GETCLK, &arg).i[LOW];
    }
    //! gets actual time from clock data internally kept by the handler task
    /*!
      This function blocks on the main access semaphore. After acquiring the
      semaphore it reads the internal clock data structure and computes the
      actual time based on that data and the current TSC count.
      This function has a symmetric interface for user and kernel spaces.
      @return 0 in case of error or the current time in ACS units.
    */
    inline unsigned long long teHandlerGetTime(void)
    {
        struct {int n; } arg = { 0 }; /** mandatory one (unused) element in your struct. */
        return (unsigned long long)rtai_lxrt(TE_HANDLER_IDX, 
                                             SIZARG,
                                             TE_HANDLER_GETTIME,
                                             &arg).rt;
    }
    //! gets time at last time event
    /*!
      This function blocks on the main access semaphore. After acquiring the
      semaphore it reads the internal clock data structure and returns the time
      measured at the last TE interruption. For doing so the respective memory
      in the clock data structore is used.
      This function has a symmetric interface for user and kernel spaces.
      @return 0 in case of error or the time in ACS units at previous TE.
    */
    inline unsigned long long teHandlerGetLast(void)
    {
        struct {int n; } arg = { 0 }; /** mandatory one (unused) element in your struct. */
        return (unsigned long long)rtai_lxrt(TE_HANDLER_IDX, 
                                             SIZARG,
                                             TE_HANDLER_GETLAST,
                                             &arg).rt;
    }
    //! gets the TE handler operational mode
    /*!
      This function blocks on the main access semaphore. After acquiring the
      semaphore it reads the internal clock data structure and returns the mode
      entry of the structure. This mode can have 3 values: SOFT, HARD,
      FREE_WHEELING.
      This function has a symmetric interface for user and kernel spaces.
      @return current operational mode of the TE handler facility.
    */
    inline int teHandlerGetMode(void)
    {
        struct {int n; } arg = { 0 }; /** mandatory one (unused) element in your struct. */
        return rtai_lxrt(TE_HANDLER_IDX, SIZARG, TE_HANDLER_GETMODE, &arg).i[LOW];
    }
    //! gets actual system time offset internally kept by the soft handler task.
    /*!
      This function access the data by means of a soft block on an access
      atomic variable. When the access is successful the data is copied
      from the global variable to the passed user parameter.
      This function has a symmetric interface for user and kernel spaces.
     */
    inline int teHandlerGetSysOffset(teHandlerSysOffset_t *offset)
    {
        struct
        {
            teHandlerSysOffset_t *offset;
            int sz;
        } arg = { offset, sizeof(teHandlerSysOffset_t) };

        return rtai_lxrt(TE_HANDLER_IDX, SIZARG, TE_HANDLER_GETOFFSET, &arg).i[LOW];
    }
#ifdef __cplusplus
}
#endif
#else
/*
 * the following trick required for letting the module compile on 2.6.15 + 3.3
 */
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,19)
#ifdef RTAI_SYSCALL_MODE
#undef RTAI_SYSCALL_MODE
#endif
#define RTAI_SYSCALL_MODE
#endif

int RTAI_SYSCALL_MODE teHandlerGetClock(teHandlerClock_t *clk);
unsigned long long RTAI_SYSCALL_MODE teHandlerGetTime(void);
unsigned long long RTAI_SYSCALL_MODE teHandlerGetLast(void);
int RTAI_SYSCALL_MODE teHandlerGetMode(void);
int RTAI_SYSCALL_MODE teHandlerGetSysOffset(teHandlerSysOffset_t *offset);
#endif

#endif /*!teHandler_H*/
