#ifndef teHandlerPrivate_H
#define teHandlerPrivate_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* ramestic  2003-10-14  created
*/

#include "teHandler.h"

/*
 * stack size
 */
#define TE_HANDLER_TASK_STACK	32768

/*
 * UNIX starting time as seen from Gregorian calendar.
 * (units are hundreds of ns. Note that the value in nanoseconds 
 * does NOT fit into a long long!)
 */
#define TE_HANDLER_UNIX_OFFSET		122192928000000000LL	/* 100ns */
#define TE_HANDLER_MAX_ALLOWED_US_DRIFT	10	/* 500us = 0.5ms */
#define TE_HANDLER_FIFO_PERIOD		2000000000	/* nano seconds */
#define TE_HANDLER_ACCESS_TO		500000	/* nano seconds */
#define TE_HANDLER_ACCEPTED_DEV_N	10	/* num. of std devation */ 
#define TE_HANDLER_XTIME_LOCK_MAX	10	/* max retries on xtime_lock */

/*
 * CPU clock frequency estimation data. Used only by the TE handler task.
 */
typedef struct TE_Statistics
{
    int buffer_full, buffer_idx;
    RTIME cpu_last, cpu_now;
    RTIME cpu_n, cpu_n_sum, cpu_n2_sum, cpu_n_mean;
    RTIME cpu_n_var, cpu_n_std;
    RTIME cpu_n_array[0x1 << TE_HANDLER_SAMPLE_POWER_SIZE];
    RTIME cpu_n2_array[0x1 << TE_HANDLER_SAMPLE_POWER_SIZE];
} teHandlerStatistics_t;

#endif /*!teHandlerPrivate_H*/
