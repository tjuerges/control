/*******************************************************************************
*
* "@(#) $Id$"
*
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2003
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* who       when      what
* --------  --------  ----------------------------------------------
* ramestic  2003-10-14  created
*/

/************************************************************************
*   NAME
*       teHandlerGetTime - prints to stdout the current TE handler time
*
*   SYNOPSIS
*       ramestic@cdp1 > teHandlerGetTime
*       2007-11-01T18:52:00.854
*       HARD LOCAL
*
*   DESCRIPTION
*       This command-line program request from the local TE handler module
*       its current clock data and then it prints to standard output the time
*       in ISO 8601 format (YYYY-MM-DDThh:mm:ss.mmm), followed by the current
*       synchronization status: mode and type. The mode could be SOFT, HARD or
*       FW, while the type could be LOCAL or ARRAY (see teHanlder.h for
*       additional information about the mode and type). The mode and
*       type are made available through baci properties within the ArrayTime
*       component.
*
*       It is important to note that teHandlerGetTime directly queries
*       the local TE handler module for obtaining the data to display. This
*       means that the command must be execute on the machine of interest,
*       if there is no TE handler module loaded then the command will output
*       an error.
*
*   FILES
*
*   ENVIRONMENT
*
*   RETURN VALUES
*
*   CAUTIONS
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS
*
*------------------------------------------------------------------------
*/

//
// System stuff
//
#include <ctime>
#include <iostream>
#include <iomanip>

//
// COORCommon stuff
//
#include <rtToolsFifoCmd.h>

//
// Local Headers
//
#include "teHandlerPrivate.h"

using std::cout;
using std::endl;
using std::setw;
using std::setfill;


unsigned long long fifoGetTime(teHandlerClock_t &clkData,
                               teHandlerSysOffset_t &offset)
{
    unsigned long long t;
    rtToolsFifoCmd *cmdFifo;

    //
    // create cmd object
    //
    try
    {
        cmdFifo = new rtToolsFifoCmd(TE_HANDLER_CMD_FIFO_DEV);
    }
    catch(...)
    {
        cout << "failed to create rtToolsFifoCmd instance" << endl;

        return 0;
    }

    //
    // GETTIME
    //
    try
    {
        int cmd = teHandlerCMD_GETTIME;

        cmdFifo->sendRecvCmd(&cmd, sizeof(cmd), (void *)&t, sizeof(t), 60);

    }
    catch(rtToolsFifoCmdErr_t e)
    {
        delete cmdFifo;

        cout << "sendRecvCmd GETTIME has failed (ex=" << e << ")" << endl;

        return 0;
    }

    //
    // GETCLK
    //
    try
    {
        int cmd = teHandlerCMD_GETCLK;

        cmdFifo->sendRecvCmd(&cmd, sizeof(cmd), (void *)&clkData, sizeof(clkData), 60);

    }
    catch(rtToolsFifoCmdErr_t e)
    {
        delete cmdFifo;

        cout << "sendRecvCmd GETCLK has failed (ex=" << e << ")" << endl;

        return 0;
    }

    //
    // GETOFFSET
    //
    try
    {
        int cmd = teHandlerCMD_GETOFFSET;

        cmdFifo->sendRecvCmd(&cmd, sizeof(cmd), (void *)&offset, sizeof(offset), 60);

    }
    catch(rtToolsFifoCmdErr_t e)
    {
        delete cmdFifo;

        cout << "sendRecvCmd GETOFFSET has failed (ex=" << e << ")" << endl;

        return 0;
    }

    delete cmdFifo;

    return t;
}

unsigned long long apiGetTime(teHandlerClock_t &clkData,
                              teHandlerSysOffset_t &offset)
{
    unsigned long long t;
    RT_TASK *task = 0;

    //
    // create our rtai task environment before invoking the teHandler API
    //
    rt_allow_nonroot_hrt();
    if ( !(task =
           rt_task_init_schmod(nam2num("TEGET"),
                               10,
                               32768,
                               1,
                               SCHED_FIFO,
                               0x1)) )
    {
        cout << "cannot create rtai task" << endl;

        return 0;
    }

    //
    // get the time through provided API
    //
    t = teHandlerGetTime();

    //
    // get the clock data through provided API
    //
    if ( teHandlerGetClock(&clkData) )
    {
        t = 0;
    }

    //
    // get the system time offset through provided API
    //
    if ( teHandlerGetSysOffset(&offset) )
    {
        t = 0;
    }

    // delete the rtai task
    //
    rt_task_delete(task);

    return t;
}

int main(int argc, char** argv)
{
    unsigned long long t, tms;
    teHandlerClock_t clkData;
    teHandlerSysOffset_t offset;
    time_t ts;

    //
    // if no param passed then use the fifo, otherwise
    // the rtai extension
    //
    if ( argc == 1 )
    {
        t = fifoGetTime(clkData, offset);
    }
    else
    {
        t = apiGetTime(clkData, offset);
    }

    //
    // check for error
    //
    if ( t == 0 )
    {
        cout << "ERROR" << endl;

        return 1;
    }

    //
    // miliseconds
    //
    tms = (t % 10000000ULL) / 10000ULL;

    //
    // unix seconds
    //
    ts = (t - TE_HANDLER_UNIX_OFFSET) / 10000000ULL;

    //
    // broken-down time
    //
    struct tm ttm;
    gmtime_r(&ts, &ttm);

    //
    // output time in ISO format
    //
    cout << (1900 + ttm.tm_year) << "-";
    cout << setfill ('0') << setw(2) << (1 + ttm.tm_mon) << "-";
    cout << setfill ('0') << setw(2) << ttm.tm_mday << "T";
    cout << setfill ('0') << setw(2) << ttm.tm_hour << ":";
    cout << setfill ('0') << setw(2) << ttm.tm_min << ":";
    cout << setfill ('0') << setw(2) << ttm.tm_sec << ".";
    cout << setfill ('0') << setw(3) << tms << "\t";

    //
    // output system time offset
    //
    cout << "(system time offset current/start/min/max=" << offset.value << "/" << offset.start << "/" << offset.min << "/" << offset.max << " [us])" << endl;

    //
    // output synchronization mode and type
    //
    cout << "Mode: ";
    switch ( clkData.mode )
    {
    case TE_HANDLER_MODE_SOFT:
        cout << "SOFT";
        break;
    case TE_HANDLER_MODE_HARD:
        cout << "HARD";
        break;
    case TE_HANDLER_MODE_FW:
        cout << "FW";
        break;
    default:
        cout << "???";
    }
    cout << " Type: ";
    switch ( clkData.type )
    {
    case TE_HANDLER_TYPE_LOCAL:
        cout << "LOCAL" << endl;
        break;
    case TE_HANDLER_TYPE_ARRAY:
        cout << "ARRAY" << endl;
        break;
    default:
        cout << "???" << endl;
    }

    return 0;
}

/*___oOo___*/
