/*******************************************************************************
 *
 * "@(#) $Id$"
 *
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2003 
 *
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * who       when        what
 * --------  ----------  ----------------------------------------------
 * ramestic  2006-02-10  created
 */

/************************************************************************
 *   NAME
 *
 *   SYNOPSIS
 * 
 *   DESCRIPTION
 *
 *   FILES
 *
 *   ENVIRONMENT
 *
 *   RETURN VALUES 
 *
 *   CAUTIONS 
 *
 *   EXAMPLES
 *
 *   SEE ALSO
 *
 *   BUGS   
 *
 *------------------------------------------------------------------------
 */

/* TODO
 * - understand and eventually use do_settimeofday
 * - restricting sample size to 2^n would simplify the division by N (use
 *   shift operator).
 * - definition of MODES:
 *      hw te
 *	soft te
 *	local t0
 *	array t0
 * - start/stop commands??? needed???
 * - on each TE it would be possible to compute a time drift based on
 *   the actual estimation of cpu_hz. It is likely to be a representative
 *   quality figure.
 */

/* 
 * System stuff
 */
#include <linux/moduleparam.h>
#include <linux/delay.h>
#include <asm/atomic.h>
#include <linux/kthread.h>

/*
 * RTAI stuff
 */
#include <rtai_sched.h>
#include <rtai_registry.h>
#include <rtai_sem.h>
#include <rtai_mbx.h>
#include <rtai_fifos.h>

/*
 * ACS stuff
 */
#include <rtTools.h>
#include <rtToolsFifoCmd.h>
#include <rtLog.h>

/* 
 * Local stuff
 */
#include "teHandlerPrivate.h"

MODULE_AUTHOR("Rodrigo Amestica");
MODULE_DESCRIPTION("TE Handler");
MODULE_LICENSE("GPL");

/*
 * used for logging
 */
#define modName	"teHandler"

/*
 * module parameters
 */
static unsigned int initTimeout = 5000;	/* ms					*/
static unsigned int cleanUpTimeout = 3000; /* ms			       	*/
static char *hwMod = "";		/* module providing TE hw interrupts	*/
static int   maxJitClip = 100;		/* clip max allowed jittering		*/
static int   maxStd = -50;		/* stats validation maximum std. dev.	*/
static unsigned int setTimePeriod = 21; /* sys time adjustment period (ticks)   */
module_param(initTimeout, uint, S_IRUGO);
module_param(cleanUpTimeout, uint, S_IRUGO);
module_param(hwMod, charp, S_IRUGO);
module_param(maxJitClip, int, S_IRUGO);
module_param(maxStd, int, S_IRUGO);
module_param(setTimePeriod, uint, S_IRUGO);

/*
 * my local data
 */
RT_TASK			mainTask;
RT_TASK			fifoTask;
struct task_struct     *sysTimeAdjustTask; 
atomic_t		main_task_run = ATOMIC_INIT(0);
atomic_t		main_task_done = ATOMIC_INIT(0);
atomic_t		fifo_task_run = ATOMIC_INIT(0);
atomic_t		fifo_task_done = ATOMIC_INIT(0);
atomic_t                init_flag = ATOMIC_INIT(0);
atomic_t                cleanup_flag = ATOMIC_INIT(0);
SEM			sem;
SEM			access_sem;
SEM			fifo_sem;
MBX			clk_tbx;
RTIME			handler_cpu_now;
teHandlerClock_t	te_clk;
teHandlerStatistics_t	te_stats;
int			new_t0_flag = 0;
teHandlerSetT0Cmd_t	new_t0;
atomic_t                resync_flag = ATOMIC_INIT(0);
int			(*enableTE)(void (*)(int)) = 0;
int			(*disableTE)(void) = 0;
RTIME			nsMaxJitClip, nsMaxStd;
int			maxStdFlag;
atomic_t                soft_clk_access = ATOMIC_INIT(1);
atomic_t                soft_offset_access = ATOMIC_INIT(0);
teHandlerSysOffset_t    sys_offset;
int                     fifo_cmd_size;
char                    fifo_cmd_buf[256]; /* a big enough buffer */

/** Divides a 64 bits integer by an integer. This is a wrapper
 * to rtai's llimd, here we take care of the sign of the input
 * dividend. The logging statements are a temporal debugging 
 * artifact. I have taken precausions for invoking llimd always
 * with a positive argument, but it seems that happens that at
 * some moment the input value is negative. I'm following that
 * lead. In the long run the logging will be removed, and in the 
 * longer run we will move to 64 bits and the whole thing will
 * become unnecesary.
 */
static inline long long _llimd(long long ll, int mult, int div)
{
    if ( ll >= 0 )
    {
        return llimd(ll, mult, div);
    }
    else
    {
        if ( (unsigned int)((int *)rt_whoami())[2] == RT_TASK_MAGIC )
        {
            static rtToolsRepeatGuard_s rtGuard =
                {                                                               
                    0, (RTIME)(10 * 1000000000LL), RTGUARD_MODE_TIMER, 1U, 0ULL, 0LL, 0LL
                };
            rtlogRecord_t logRecord;

            RTLOG_GUARD(&rtGuard,
                        RTLOG_WARNING,
                        "got a negative dividend %lld %d %d",
                        ll, mult, div);
        }
        else
        {
            printk("%s - _llimd got a negative dividend %lld %d %d\n", modName, ll, mult, div);
        }
        
        return -llimd(-ll, mult, div);
    }
}

/*
 * used many many times. The idea is to do conversions between time units and
 * cpu counts always based on the local estimated cpu frequency. Given that 
 * cpu_hz is an integer value I'm assuming here that reading/writing its value is
 * always atomic, and, therefore, no guarding is enforced for reading.
 */
#define COUNT2NS(counts)	_llimd(counts, 1000000000, te_clk.cpu_hz)
#define COUNT2ACS(counts)	_llimd(counts, 10000000, te_clk.cpu_hz)
#define COUNT2US(counts)	_llimd(counts, 1000000, te_clk.cpu_hz)
#define NS2COUNT(ns)		_llimd(ns, te_clk.cpu_hz, 1000000000)
#define ACS2COUNT(tacs)	_llimd(tacs, te_clk.cpu_hz, 10000000)

/*
 * signal task termination and wait for the task to aknowledge
 */
static int terminateTask(RT_TASK *task,
                         atomic_t *run_flag, atomic_t *done_flag,
                         unsigned int to /* ms */)
{
    int waitCount = 0;

    /*
     * command the task to finish
     */
    atomic_set(run_flag, 0);

    /*
     * wait for the task to be gone
     */
    while ( atomic_read(done_flag) != 1 && waitCount < to )
    {
	waitCount++;

	/*
	 * sleep for 1ms
	 */
	rt_sleep(nano2count(1000000LL));
    }

    /*
     * if the task did not finish on time then forcibly remove
     * it from the rtai's scheduler
     */
    if ( waitCount == to )
    {
	rt_task_suspend(task);
	rt_task_delete(task);

	return 1;
    }

    return 0;
}

/*
 * ad-hoc guarded reading of 64bits handler_cpu_now
 */
int readISRVariable(RTIME *now)
{
    RTIME t1, t2, t3;
    rtlogRecord_t logRecord;

    t1 = handler_cpu_now;

    if ( (t2 = handler_cpu_now) == t1 )
    {
	*now = t1;
    }
    else if ( (t3 = handler_cpu_now) == t2 )
    {
	*now = t2;
    }
    else
    {
	RTLOG_ERROR(modName, "failed to read good handler_cpu_now value!");

	return 1;
    }

    return 0;
}

/*
 * Returns current linux time in acs-units.
 * This function is meant to be call from an rtai environment. The
 * data to compute the actual system time is retrieved from the 
 * linux thread by means of an atomic guarded read. Reading is
 * attempted at most 2 times only.
 */
int getSysTime(RTIME *sys, unsigned int	cpu_hz)
{
    int access;
    RTIME gcpu, gsys;

    /*
     * read current value of access variable to global offset variable
     */
    access = atomic_read(&soft_offset_access);
    
    /*
     * copy global offset data into local variable
     */
    gcpu = sys_offset.cpu;
    gsys = sys_offset.sys;
    
    /*
     * if the access variable was an odd number when we started 
     * or it has changed since that time then we are not good to go.
     */
    if ( ((access & 0x1) | (atomic_read(&soft_offset_access) ^ access)) )
    {
        gsys = 0;
    }
    
    /*
     * check whether accessing the global structure was a success or not
     * or the time is simply not available at this moment.
     */
    if ( gsys == 0 )
    {
        return 1;
    }

    /*
     * note that we are always running in one single cpu, therefore,
     * rdtsc should be always good.
     */
    *sys = gsys + _llimd(rdtsc() - gcpu, 10000000LL, cpu_hz);

    return 0;
}

/*
 * set system time to current array time, tv must contain the time as seen
 * when the offset was computed, provided the elapsed time since then is
 * 'reasonable' short. We do not read the system time again in this function.
 * This function is meant to be called from a Linux environment only.
 */
int setSysTime(teHandlerClock_t clk, RTIME offset, struct timeval tv)
{
    struct timespec tv_set;

    /*
     * if the offset is not big 'enough' then just return without action.
     */
    if ( (offset < TE_HANDLER_MAX_ALLOWED_US_DRIFT &&
          offset > -TE_HANDLER_MAX_ALLOWED_US_DRIFT) )
    {
	return 0;
    }

    /*
     * if the offset is negative then roll back the seconds
     */
    while ( offset < 0 )
    {
	offset += 1000000LL;
	tv.tv_sec--;
    }

    /*
     * avoid micro-seconds overflow and under-flow!
     */
    tv.tv_usec += offset;
    while ( tv.tv_usec >= 1000000LL )
    {
	tv.tv_usec -= 1000000LL;
	tv.tv_sec++;
    }

    /*
     * since kernel 2.6.14.3 settimeofday takes a
     * timespec rather than a timeval, we need to translate.
     */
    tv_set.tv_sec = tv.tv_sec;
    tv_set.tv_nsec = tv.tv_usec * 1000;

    /*
     * set OS system time
     */
    return do_settimeofday(&tv_set);
}

/*
 * Gets current cpu count and computes current time in ACS units like
 * this:
 *
 *	t = t0 + ticks * TE + (now - last)
 *
 * Note: if the clk structure is such that the 'current' time would
 * be in the past then this function simply does the right arithmetic
 * (avoid negative argument to llimd) and returns a time in the past.
 */
unsigned long long computeCurrentTime(const teHandlerClock_t clk)
{
    unsigned long long t = rdtsc()/*rt_get_time()*/;

    t = _llimd(t - clk.cpu_on_last_te, 10000000, clk.cpu_hz);

    return clk.t0 + (RTIME)clk.ticks * (RTIME)TEacs + t;
}

RTIME teHandlerSqrt(unsigned long long a) 
{
    unsigned long long rem = 0;
    unsigned long long root = 0;
    int   loopIdx;

    for ( loopIdx = 0; loopIdx < (sizeof(a) * 4); loopIdx++ )
    {
	root <<= 1;
	rem = ((rem << 2) + (a >> ((sizeof(a)*8) - 2)));
	a <<= 2;
	root++;
	if (root <=  rem)
	{
	    rem -= root;
	    root++;
	} else
	{
	    root--;
	}
    }
    root >>= 1; 

    return root;
}

/*
 * initialize samples buffer and statistics
 */
void initSample(const RTIME first_period)
{
    te_stats.buffer_full = 0;

    te_stats.buffer_idx = 0;

    te_stats.cpu_n_array[0] = first_period;

    te_stats.cpu_n2_array[0] = 0;

    te_stats.cpu_n_sum = te_stats.cpu_n_array[0];

    te_stats.cpu_n2_sum = te_stats.cpu_n2_array[0];

    te_stats.cpu_n_mean = te_stats.cpu_n_array[0];

    te_stats.cpu_n_var = te_stats.cpu_n2_array[0];

    te_stats.cpu_n_std = teHandlerSqrt(te_stats.cpu_n_var);
}

/*
 * add sample to samples buffer
 */
void addSample(void)
{
    RTIME value, aux;

    /*
     * this is the new sampled value
     */
    value = te_stats.cpu_now - te_stats.cpu_last;

    /*
     * make buffer_idx point to the place where the new sample should be
     * stored. In a circular buffer this is also the place of the oldest sample.
     */
    te_stats.buffer_idx++;

    /*
     * a circular buffer requires here to wrap around the index
     */
    if ( te_stats.buffer_idx == TE_HANDLER_SAMPLE_SIZE )
    {
        te_stats.buffer_idx = 0;

        te_stats.buffer_full = 1;
    }

    /*
     * equation for mean and var depends on whether the buffer is full or not
     */
    if ( te_stats.buffer_full )
    {
        te_stats.cpu_n_sum += (value - te_stats.cpu_n_array[te_stats.buffer_idx]);

        te_stats.cpu_n_mean = (te_stats.cpu_n_sum >> TE_HANDLER_SAMPLE_POWER_SIZE);

        aux = value - te_stats.cpu_n_mean;

        aux *= aux;

        te_stats.cpu_n2_sum += (aux - te_stats.cpu_n2_array[te_stats.buffer_idx]);

        te_stats.cpu_n_var = (te_stats.cpu_n2_sum >> TE_HANDLER_SAMPLE_POWER_SIZE);
    }
    else /* buffer not full yet */
    {
        te_stats.cpu_n_sum += value;

        te_stats.cpu_n_mean = 
            _llimd(te_stats.cpu_n_sum, 1, te_stats.buffer_idx + 1);

        aux = value - te_stats.cpu_n_mean;
        
        aux *= aux;

        te_stats.cpu_n2_sum += aux;

        te_stats.cpu_n_var = 
            _llimd(te_stats.cpu_n2_sum, 1, te_stats.buffer_idx + 1);
    }

    /*
     * record new values for mean and var
     */
    te_stats.cpu_n_array[te_stats.buffer_idx] = value;
    te_stats.cpu_n2_array[te_stats.buffer_idx] = aux;

    /*
     * standard deviation or rms
     */
    te_stats.cpu_n_std = teHandlerSqrt(te_stats.cpu_n_var);
}

/*
 * SETT0 reply handling
 */
void handle_SETT0_reply(int *flag,
                        teHandlerSetT0Cmd_t *t0,
                        teHandlerClock_t *clk)
{
    int stat, reply;
    rtlogRecord_t logRecord;

    if ( *flag )
    {
	RTLOG_INFO(modName,
                   "new SETT0 acknowledged (tick/old/new=%u/%lld/%lld[acs])",
                   clk->ticks,
                   clk->t0,
                   t0->value - (RTIME)clk->ticks * (RTIME)TEacs);

	/*
	 * adjust t0 as to reflect the received value. Note that the ticks count
	 * is a monotonic variable (always increases), therefore, t0 is always
	 * the array time at tick zero, that is, the moment at which the module
	 * was loaded.
	 */
	clk->t0	= t0->value - (RTIME)clk->ticks * (RTIME)TEacs;
	clk->type = TE_HANDLER_TYPE_ARRAY;
	
	/* reset the flag as for future handling */
	*flag = 0;

	/*
	 * reply to SETT0 command
	 */
	reply = TE_HANDLER_CMD_STAT_OK;
	stat = rtf_put_if(TE_HANDLER_CMD_FIFO, &reply, sizeof(reply));
	if ( stat != sizeof(reply) )
	{		
	    RTLOG_ERROR(modName,
                        "cannot write reply in fifo (stat/size=%d/%d)",
                        stat, sizeof(reply));
	}
	else
	{
	    RTLOG_INFO(modName, "new T0 reply sent successfully");
	}
    }
}

/*
 * RESYNC reply handling
 */
void handle_RESYNC_reply(atomic_t *cmd_flag, teHandlerClock_t *clk)
{
    int stat, reply;
    static int count = 0;
    rtlogRecord_t logRecord;

    /*
     * if there is no resync pending then just return
     */
    if ( !atomic_read(cmd_flag) )
    {
        return;
    }

    /*
     * command is okay if we managed to get into HARD mode, otherwise, it has
     * failed. But it can happen that the RESYNC command is received before a
     * software tick and the next re-born tick is to happen after this software
     * tick, this means that we should keep waiting one tick more
     */
    if ( clk->mode == TE_HANDLER_MODE_HARD )
    {
        /*
        * samples buffer must be reset at this point. It is a mistake
        * to mix statistics of the old signal to those in the new signal.
        * The initialization is such that no info is provided, that is,
        * the first cpu-period sample is initialized to exactly 48ms (based
        * on rtai's frequency estimation)
        */
        initSample(_llimd(TEns, CPU_FREQ, 1000000000));

        /*
         * increment the resynchronization events counter
         */
        clk->resync_count++;

        reply = TE_HANDLER_CMD_STAT_OK;

        RTLOG_INFO(modName,
                   "resync command completed successfully (ticks=%u freq=%u[hz])",
                   clk->ticks,
                   clk->cpu_hz);
    }
    else if ( count == 1 )
    {
        reply = TE_HANDLER_CMD_STAT_RESYNC_FAILED;
        
        RTLOG_INFO(modName,
                   "resync command failed to resync (ticks=%u)",
                   clk->ticks);
    }
    else
    {
        RTLOG_INFO(modName,
                   "resync command skipping software tick (ticks=%u/%u)",
                   clk->ticks,
                   clk->cpu_hz);

        count++;

        return;
    }

    /*
     * reset the re-sync command flag, the command is finished (for good or bad)
     */
    atomic_set(cmd_flag, 0);

    /*
     * reset the count of skipped software ticks
     */
    count = 0;

    /*
     * write reply into fifo
     */
    stat = rtf_put_if(TE_HANDLER_CMD_FIFO, &reply, sizeof(reply));
    if ( stat != sizeof(reply) )
    {		
        RTLOG_ERROR(modName,
                    "cannot write RESYNC reply into fifo (stat/size=%d/%d)",
                    stat, sizeof(reply));
    }
}

/*
 * in SOFT and FW we follow the system time. For doing so we need to compute
 * an estimation of the cpu count at which the 'virtual' tick should had
 * happen (aligned with a 48ms boundary of the system time). If the computed
 * cpu counts is in the future then this function sleeps until then.
 * @param timeAtLastTick time associated to the last tick [ACS]
 * @param mean current mean value of cpu counts between ticks
 * @return estimated cpu counts at new soft tick
 */
RTIME adjustToSoftTick(RTIME timeAtLastTick, RTIME mean, unsigned int cpu_hz)
{
    RTIME now, sys_time_delta;
    rtlogRecord_t logRecord;

    if ( getSysTime(&sys_time_delta, cpu_hz) )
    {
        /*
         * this is an error. Let's force delta to zero and log
         * an error. I'm not sure what's the whole impact of
         * forcing it to zero...
         */
        RTLOG_ERROR(modName, "failed to get system time for delta computation");
        
        sys_time_delta = 0;
    }
    else
    {
        /*
         * we have slept for what we think are 48ms. Let's read the
         * system time and check out the difference respect to that
         * time base.
         */
        sys_time_delta -= (timeAtLastTick + TEacs);
    }

    
    /*
      RTLOG_INFO(modName,
      "TEST0 %lld %lld %lld",
      rt_get_time(),
      sys_time_delta,
      sys_time_delta >= 0 ?
      llimd(sys_time_delta, te_stats.cpu_n_mean, TEacs) :
      -llimd(-sys_time_delta, te_stats.cpu_n_mean, TEacs));
    */
    
    /* _llimd is good for negative dividens, but I'm still wondering
     * whether there is or not an expected negative number somewhere
     */
    if ( sys_time_delta >= 0 )
        now = rt_get_time() - _llimd(sys_time_delta, cpu_hz, 10000000);
    else
        now = rt_get_time() + _llimd(-sys_time_delta, cpu_hz, 10000000);

    if ( sys_time_delta < 0 )
    {
        /*
         * isn't there an rtai option that should make this type of
         * loops unnecessary?
         */
        do
        {
            rt_sleep_until(now);
        }
        while ( rt_get_time() < now );
    }

    return now;
}

int validateClockData(void)
{
    RTIME rtai_now;
    static rtToolsRepeatGuard_s rtGuard =
        {                                                               
            0, (RTIME)(10 * 1000000000LL), RTGUARD_MODE_TIMER, 1U, 0ULL, 0LL, 0LL
        };
    /*RTGUARD(rtGuard, 0, 10, RTGUARD_MODE_TIMER);*/
    rtlogRecord_t logRecord;

    /*
     * sanity check, verify that we are always in the future of last tick
     */
    if ( (rtai_now = rt_get_time()) < te_clk.cpu_on_last_te )
    {
	RTLOG_ERROR(modName,
                    "cpu_on_last_te bigger than actual cpu count (ticks/last/now=%u/%lld/%lld)",
                    te_clk.ticks, te_clk.cpu_on_last_te, rtai_now);

	return 1;
    }

    /*
     * validate our data based on the available statistics.
     */
    if ( COUNT2NS(te_stats.cpu_n_std) > nsMaxStd )
    {
        RTLOG_GUARD(&rtGuard,
                    RTLOG_WARNING,
                    "rms of cpu counts too big (tick/last/mean/rms/max=%u/%lld/%lld/%lld/%lld [ns])",
                    te_clk.ticks,
                    te_stats.cpu_n_array[te_stats.buffer_idx],
                    te_stats.cpu_n_mean,
                    COUNT2NS(te_stats.cpu_n_std),
                    nsMaxStd);

        /*
         * if requested then return with error when the rms is too big.
         * The flag is set when maxStd (kernel module parameter) is set
         * to a positive value.
         */
	if ( maxStdFlag )
	{
            unsigned int i;

	    RTLOG_ERROR(modName,
                        "rms of cpu counts too big and flagged to fail!");

            RTLOG_INFO(modName, "cpu_n_sum/cpu_n2_sum/cpu_n_mean/cpu_n_var=%lld/%lld/%lld/%lld", te_stats.cpu_n_sum, te_stats.cpu_n2_sum, te_stats.cpu_n_mean, te_stats.cpu_n_var);
            //
            // te_stats.buffer_idx points to the last inserted sample
            //
            for ( i = te_stats.buffer_idx + 1; i < TE_HANDLER_SAMPLE_SIZE; ++i )
            {
                RTLOG_INFO(modName, "i/cpu_n_array/cpu_n2_array=%u/%lld/%lld", i - (te_stats.buffer_idx + 1), te_stats.cpu_n_array[i], te_stats.cpu_n2_array[i]);
            }

            for ( i = 0; i <= te_stats.buffer_idx; ++i )
            {
                RTLOG_INFO(modName, "i/cpu_n_array/cpu_n2_array=%u/%lld/%lld", TE_HANDLER_SAMPLE_SIZE - te_stats.buffer_idx + i - 1, te_stats.cpu_n_array[i], te_stats.cpu_n2_array[i]);
            }

	    return 1;
	}
    }

    return 0;
}

int updateClockData(teHandlerClock_t clk, int timed_out)
{
    int stat;
    rtlogRecord_t logRecord;

    /*
     * increment our heart beat counter
     */
    clk.ticks++;
    
    /*
     * the timed_out flag is used as for checking a mode change.
     */
    switch(clk.mode)
    {
    case TE_HANDLER_MODE_HARD:
	if ( timed_out )
	{
            RTLOG_INFO(modName,
                       "operational mode changed HARD -> FW (tick=%u)",
                       clk.ticks);

            clk.mode = TE_HANDLER_MODE_FW;
	}
	break;
    case TE_HANDLER_MODE_FW:
        if ( !timed_out )
        {
            RTLOG_INFO(modName,
                       "operational mode changed FW -> HARD (tick=%u)",
                       clk.ticks);

            clk.mode = TE_HANDLER_MODE_HARD;
	}
	break;
    } /* switch(clk.mode) */
    
    /*
     * check for pending commands and reply if that's the case.
     * Note: these function do eventually modify the clock structure
     * passed as parameter.
     * Note: RESYNC in particular may change statistics when the resync-ing
     * tick is detected, it is thus important to call handle_RESYNC_reply
     * before the clock data is updated (look lines below). Otherwise,
     * the frequency in particular would be wrongly estimated based on the last
     * soft tick and the resyncing hardware one and that difference could be
     * anything.
     */
    handle_SETT0_reply(&new_t0_flag, &new_t0, &clk);
    handle_RESYNC_reply(&resync_flag, &clk);

    /*
     * cpu on last TE is updated to the current value as updated by the
     * isr or soft tick.
     */
    clk.cpu_on_last_te = te_stats.cpu_now;

    /*
     * cpu clock frequencie estimated based on the mean count between ticks
     */
    clk.cpu_hz = _llimd(te_stats.cpu_n_mean, 1000, TEms);
    
    /*
     * cpu on next te is extrapolated based on current cpu_hz
     */
    clk.cpu_on_next_te = clk.cpu_on_last_te + _llimd(clk.cpu_hz, TEms, 1000);
    
    /*
     * given that the public API access the clock structure
     * then we must protect its access using the semaphore.
     */
    stat = rt_sem_wait_timed(&access_sem, nano2count(TE_HANDLER_ACCESS_TO));
    if ( stat == 0xFFFF || stat == 0xFFFE )
    {
	RTLOG_ERROR(modName, "failed to take access semaphore (stat=%d)!", stat);

	return 1;
    }
    
    /*
     * copy local copy of the clock data into global variable
     */
    atomic_inc(&soft_clk_access);
    te_clk = clk;
    atomic_inc(&soft_clk_access);

    /*
     * never forget to give back the access semaphore
     */
    rt_sem_signal(&access_sem);

    return 0;
}

/*
 * update access clock data and write it in mail box
 */
int publishClockData(void)
{
    int status;
    rtlogRecord_t logRecord;

    /*
     * broadcast to the world our freshly computed clock data
     * originally the idea was to broadcast to all eventual clients, but
     * tbx'es did not work as expected, so I stopped using them.
     */
    /*rt_tbx_broadcast(&clk_tbx, &te_clk, sizeof(te_clk));*/

    /*
     * write the clock data structure into the mailbox overwriting any thing
     * already in there
     */
    if ( (status = rt_mbx_ovrwr_send(&clk_tbx, &te_clk, sizeof(te_clk))) != 0 )
    {
	RTLOG_ERROR(modName,
                    "failed to send clock data (status/size=%d/%d)!",
                    status, sizeof(te_clk));
	return 1;	    
    }
	
    return 0;
}

/*
 * interrupt handler
 */
void isr(int irq)
{
    rtlogRecord_t logRecord;

    /*
     * cpu count at which this interrupt was handled
     */
    handler_cpu_now  = rt_get_time();

    /* 
     * give TE task semaphore
     */
    if ( rt_sem_signal(&sem) != 0 )
    {
	RTLOG_ERROR(modName, "failed to give user semaphore!");
    }

    /*
     * re-enable the interrupt for further events.
     * Note that RTAI's documentation specify the handler prototype as 
     * having NO parameters, but it actually receives the irq.
     * Also note that is also possible that the TE handler is actually invoked 
     * not as an ISR it self but from within an other ISR (that's the case
     * for hpdi32 and the VME Acromag based implementation). In those cases,
     * the TE handler  must be called passing a negative value.
     */
    if ( irq >= 0 )
    {
	rt_enable_irq(irq);
    }
}

/*
 * Command fifo handler
 */
int fifoHandler(unsigned int fifo, int rw)
{
    /*
     * this fifo command mechanism is such we only receive commands, that is,
     * we do something when user space writes into the fifo. The read case
     * happens when user space reads the reply to the command and in that case
     * this fifo does nothing.
     */
    if ( rw == 'w' )
    {
        /*
         * get command data from fifo
         */
        fifo_cmd_size = rtf_get(TE_HANDLER_CMD_FIFO,
                                (void *)&fifo_cmd_buf,
                                sizeof(fifo_cmd_buf));

        /*
         * signal fifo task that there is a command to handle
         */
        rt_sem_signal(&fifo_sem);
    }

    return 0;
}

/*
 * fifo command task. This task handles the commands received through the fifo
 */
void fifoTaskFun(long not_used)
{
    int stat, reply;
    unsigned long long t;
    RTIME new_t0_delta;
    teHandlerSysOffset_t offset;
    rtlogRecord_t logRecord;

    RTLOG_INFO(modName, "fifo task spawned");

    /*
     * this global flag marks the status of this task.
     */
    atomic_set(&fifo_task_run, 1);

    while ( atomic_read(&fifo_task_run) )
    {	 
        /*
         * just for letting the shutdown of the module to work nicely,
         * sleep on the semaphore with a TE_HANDLER_FIFO_PERIOD granularity
         */
        if ( (stat = rt_sem_wait_timed(&fifo_sem,
                                       nano2count(TE_HANDLER_FIFO_PERIOD)))
             == SEM_ERR )
	{
	    RTLOG_ERROR(modName,
                        "major problem with fifo semaphore (stat=%d)!",
                        stat);
	    
            break;
	}

        /*
         * if the semaphore is not available yet then there is no command to
         * handle, just continue
         */
        if ( stat == SEM_TIMOUT )
        {
            continue;
        }

        /*
         * the following piece of code somehow showed that I will loose
         * commands (messages in the fifo) when rtf_get is not performed
         * from within the handler itself. The sleeping for 1ms shows
         * that rtf_get will return zero when left at this spot an removed
         * from the handler.
         */
        /*
          rt_sleep(nano2count(1000000LL));
          if ( (fifo_cmd_size = rtf_get(TE_HANDLER_CMD_FIFO,
          (void *)&cmd,
          sizeof(cmd)))
          < 0 )
          {
          RTLOG_ERROR(modName, "failed to read from fifo (err=%d)!", cmdSize);

          continue;
          }
        */

        /*
         * a negative value here indicates that rtf_get had some problem from
         * fifoHandler
         */
	if ( fifo_cmd_size < 0 )
        {
            RTLOG_ERROR(modName,
                        "failed to read from fifo (err=%d)!",
                        fifo_cmd_size);

            continue;
        }

        /*
         * the data block must at least contain the cmd index (integer)
         */
	if ( fifo_cmd_size < sizeof(int) )
	{
	    RTLOG_ERROR(modName,
                        "not enough data was retreived from fifo (size=%d)!",
                        fifo_cmd_size);	
            
            continue;
	}

	RTLOG_TRACE(modName,
                    "fifo handler (cmd/size=%d/%d)",
                    *(int *)&fifo_cmd_buf, fifo_cmd_size);

        /*
         * we are eventually accessing the data clock, guard this access
         * with the respective semaphore
         */
	stat = rt_sem_wait_timed(&access_sem, nano2count(TE_HANDLER_ACCESS_TO));
	if ( stat == SEM_ERR || stat == SEM_TIMOUT )
	{
	    RTLOG_ERROR(modName, "failed to take access semaphore (stat=%d)!", stat);

            continue;
	}

        /*
         * handle the command
         */
	switch(*(int *)fifo_cmd_buf)
	{
	case teHandlerCMD_SETT0:
	    RTLOG_TRACE(modName, "received cmd SETT0");

            /*
             * the cmd data block must have the right size for this command
             */
            if ( fifo_cmd_size != (sizeof(int) + sizeof(teHandlerSetT0Cmd_t)) )
            {
                RTLOG_ERROR(modName,
                            "unexpected SETTO command size (size=%d)!",
                            fifo_cmd_size);	
                
                break;
            }

            /*
             * copy from data block the command params
             */
	    new_t0 = *(teHandlerSetT0Cmd_t *)((int *)fifo_cmd_buf + 1);

            /*
             * compute the difference between the received timestamp for the
             * next TE and our own expectation for that value. To be use
             * below for rejecting crazy values.
             */
            new_t0_delta = new_t0.value - ((te_clk.ticks + 1) * TEacs + te_clk.t0);

	    /* 
             * check ticks count
             */
	    if ( (new_t0.tick != te_clk.ticks) || new_t0_flag )
	    {
		RTLOG_ERROR(modName,
                            "invalid tick count or flag status (self/user/flag=%u/%u/%d)",
                            te_clk.ticks, new_t0.tick, new_t0_flag);
		
		reply = TE_HANDLER_CMD_STAT_WRONG_TICK;
		
		stat = rtf_put_if(TE_HANDLER_CMD_FIFO, &reply, sizeof(reply));
		if ( stat != sizeof(reply) )
		{		
		    RTLOG_ERROR(modName,
                                "cannot write reply in fifo (stat/size=%d/%d)",
                                stat, sizeof(reply));
		}
	    }
	    /* check that the time at the next tick is a multiple of TE */
	    else if ( rtToolsLlFloor(new_t0.value, TEacs) != new_t0.value )
	    {
		RTLOG_ERROR(modName,
                            "invalid time stamp for next tick (%lld)",
                            new_t0.value);

		reply = TE_HANDLER_CMD_STAT_INVALID_T0;
		
		stat = rtf_put_if(TE_HANDLER_CMD_FIFO, &reply, sizeof(reply));
		if ( stat != sizeof(reply) )
		{		
		    RTLOG_ERROR(modName,
                                "cannot write reply in fifo (stat/size=%d/%d)",
                                stat, sizeof(reply));
		}
	    }
            /* check that the time at the next tick is 'reasonable' */
            else if ( new_t0_delta > 1250 * TEacs || new_t0_delta < -1250 * TEacs )
            {
                /*
                 * if the TE's period is 48ms then the 1250 figure used above
                 * means 1 full minute. Reject the command when the received
                 * timestamp for the next TE is away from our own time for more
                 * than a minute. Something must be out of business.
                 */
		RTLOG_ERROR(modName,
                            "timestamp for next tick looks strange (user-self=%lld)",
                            new_t0_delta);

		reply = TE_HANDLER_CMD_STAT_INVALID_T0;
		
		stat = rtf_put_if(TE_HANDLER_CMD_FIFO, &reply, sizeof(reply));
		if ( stat != sizeof(reply) )
		{		
		    RTLOG_ERROR(modName,
                                "cannot write reply in fifo (stat/size=%d/%d)",
                                stat, sizeof(reply));
		}                
            }
	    else
	    {
                /*
                 * flag out that we are processing this command. Final handling
                 * will not happen until the next tick.
                 */
		new_t0_flag = 1;
	    }
	    break;

	case teHandlerCMD_GETCLK:
	    RTLOG_TRACE(modName, "received cmd GETCLK");
	    
            /*
             * the cmd data block must have the right size for this command
             */
            if ( fifo_cmd_size != sizeof(int) )
            {
                RTLOG_ERROR(modName,
                            "unexpected GETCLK command size (size=%d)!",
                            fifo_cmd_size);	
                
                break;
            }

            /*
             * write reply into fifo
             */
	    if ( (stat = rtf_put_if(TE_HANDLER_CMD_FIFO, &te_clk, sizeof(te_clk)))
                 != sizeof(te_clk) )
	    {		
		RTLOG_ERROR(modName,
                            "cannot write reply into fifo (size/err=%d/%d)",
                            sizeof(te_clk), stat);
	    }
	    break;

	case teHandlerCMD_GETTIME:
	    RTLOG_TRACE(modName, "received cmd GETTIME");

            /*
             * the cmd data block must have the right size for this command
             */
            if ( fifo_cmd_size != sizeof(int) )
            {
                RTLOG_ERROR(modName,
                            "unexpected GETTIME command size (size=%d)!",
                            fifo_cmd_size);	
                
                break;
            }

            /*
             * compute time based on current clock data
             */
	    t = computeCurrentTime(te_clk);

            /*
             * write reply into fifo
             */	    
	    if ( (stat = rtf_put_if(TE_HANDLER_CMD_FIFO, &t, sizeof(t)))
                 != sizeof(t) )
	    {		
		RTLOG_ERROR(modName,
                            "cannot write reply into fifo (size/err=%d/%d)",
                            sizeof(t), stat);
	    }
	    break;

        case teHandlerCMD_RESYNC:
            RTLOG_TRACE(modName, "received cmd RESYNC");

            /*
             * if we are not free-wheeling then just reply with OK. Even though,
             * for SOFT mode does not make much sense to receive this command
             * let's think that an OK reply is better than err.
             */
            if ( te_clk.mode != TE_HANDLER_MODE_FW )
            {
                RTLOG_WARNING(modName,
                              "RESYNC does not make sense in %s mode. Reply OK",
                              te_clk.mode == TE_HANDLER_MODE_SOFT ?
                              "SOFT" : "HARD");

                reply = TE_HANDLER_CMD_STAT_OK;
		
		stat = rtf_put_if(TE_HANDLER_CMD_FIFO, &reply, sizeof(reply));
		if ( stat != sizeof(reply) )
		{		
		    RTLOG_ERROR(modName,
                                "cannot write reply in fifo (stat/size=%d/%d)",
                                stat, sizeof(reply));
		}
            }
            else
            {
                /*
                 * set the flag that commands to resync and let the rest to reply
                 */
                atomic_set(&resync_flag, 1);
            }

            break;

        case teHandlerCMD_GETOFFSET:
            RTLOG_TRACE(modName, "received cmd GETOFFSET");

            /*
             * the cmd data block must have the right size for this command
             */
            if ( fifo_cmd_size != sizeof(int) )
            {
                RTLOG_ERROR(modName,
                            "unexpected GETCLK command size (size=%d)!",
                            fifo_cmd_size);	
                
                break;
            }

            /*
             * read the data from global structure
             */
            if ( teHandlerGetSysOffset(&offset) )
            {
                RTLOG_ERROR(modName, "failed to read global structure");
                
                break;
            }

            /*
             * write reply into fifo
             */
	    if ( (stat = rtf_put_if(TE_HANDLER_CMD_FIFO, &offset, sizeof(offset)))
                 != sizeof(offset) )
	    {		
		RTLOG_ERROR(modName,
                            "cannot write reply into fifo (size/err=%d/%d)",
                            sizeof(offset), stat);
	    }
            
            break;

	default:
	    RTLOG_ERROR(modName,
                        "received unknown command (cmd=%d)",
                        *(int *)fifo_cmd_buf);

	    reply = TE_HANDLER_CMD_STAT_UNKNOWN;

            /*
             * write error reply into fifo
             */	    
	    if ( (stat = rtf_put_if(TE_HANDLER_CMD_FIFO, &reply, sizeof(reply)))
                 != sizeof(reply) )
	    {		
		RTLOG_ERROR(modName,
                            "cannot write reply into fifo (size/err=%d/%d)",
                            sizeof(reply), stat);
	    }
	    break;	    
	}
	
        /*
         * we are done accessing clock data, give back the access semaphore
         */
	rt_sem_signal(&access_sem);

    } /* while ( atomic_read(&fifo_task_run) ) */
    
    RTLOG_INFO(modName, "fifo task has terminated (cpu=%lld)!", rt_get_time());

    /*
     * this flag will indicate to the the cleanup function that this
     * task has really finished.
     */
    atomic_set(&fifo_task_done, 1);

    rt_task_delete(rt_whoami());
}

/*
 * synchronize to first 2 hard ticks
 */
int clockInitHw(void)
{
    int semStatus;
    RTIME delta;
    rtlogRecord_t logRecord;

    /*
     * enable hw interrupts
     */
    if ( enableTE(isr) )
    {
	/*
	 * if cannot enable interrupts then initialize in SOFT mode.
	 */
	RTLOG_ERROR(modName, "failed to enable hw interrupts!");

	return 1;	
    }

    /*
     * make sure the semaphore is not available before trying to
     * synchronize with the first two ticks.
     */
    if ( rt_sem_wait_if(&sem) == SEM_ERR )
    {
	disableTE();

	RTLOG_ERROR(modName, "failed to make hw sem unavailable!");

	return 1;
    }

    RTLOG_INFO(modName, "synchronizing with 1st tick...");
	
    /*
     * We need to two ticks before proceeding...
     * wait for first tick, if err or time-out on this first tick then
     * initialize in SOFT mode 
     */
    semStatus = rt_sem_wait_timed(&sem , NS2COUNT(2LL * TEns));
    if ( semStatus == SEM_ERR || semStatus == SEM_TIMOUT )
    {
	disableTE();

	RTLOG_ERROR(modName,
                    "failed to catch up with first tick (err=%d)!",
                    semStatus);
	
	return 1;
    }
	
    /*
     * read cpu count that's updated by the ISR on each tick
     */
    if ( readISRVariable(&te_stats.cpu_last) )
    {
	disableTE();

	RTLOG_ERROR(modName, "cannot read ISR cpu counts into cpu_last!");
	
	return 1;
    }

    RTLOG_INFO(modName, "synchronizing with 2nd tick...");

    /*
     * We need two ticks before proceeding...
     * wait for second tick, if err or time-out on this second tick then
     * initialize in SOFT mode 
     */
    semStatus = rt_sem_wait_timed(&sem , NS2COUNT(2LL * TEns));
    if ( semStatus == SEM_ERR || semStatus == SEM_TIMOUT )
    {
	disableTE();

	RTLOG_ERROR(modName, "failed to catch up with second tick (err=%d)!", semStatus);

	return 1;
    }

    /*
     * read cpu count that's updated by the ISR on each tick
     */
    if ( readISRVariable(&te_stats.cpu_now) )
    {
	disableTE();

	RTLOG_ERROR(modName, "cannot read ISR cpu counts into cpu_now");

	return 1;
    }

    /*
     * sanity check
     */
    if ( te_stats.cpu_now <= te_stats.cpu_last )
    {
	disableTE();

	RTLOG_ERROR(modName,
                    "strange cpu count for 1st and 2nd ticks (%lld/%lld)!",
                    te_stats.cpu_last, te_stats.cpu_now);
	
	return 1;
    }

    /*
     * time difference between both ticks
     */
    delta = count2nano(te_stats.cpu_now - te_stats.cpu_last);

    /*
     * if the first 2 ticks show a too different period compared to
     * 48ms then assume here that something is wrong with the signal.
     */
    if ( delta > (TEns + 1000000LL) || delta < (TEns - 1000000LL) )
    {
        RTLOG_ERROR(modName,
                    "1st and 2nd ticks too much away from 48ms period (%lldns)!",
                    delta);
        
        return 1;
    }

    return 0;
}

/*
 * synchronize to hard ticks (if available) and initialize statistics and
 * clock data
 */
int clockInit(void)
{
    RTIME sysTime;
    rtlogRecord_t logRecord;

    /*
     * resynchronization counter starts at zero
     */
    te_clk.resync_count = 0;

    /*
     * initialize our cpu freq estimation to that computed by rtai at the
     * moment the scheduler module was loaded
     */
    te_clk.cpu_hz = CPU_FREQ;

    /*
     * assume this mode, if HW is available will be changed to HARD accordingly.
     */
    te_clk.mode = TE_HANDLER_MODE_SOFT;
    
    /*
     * if hw driver available then try to initialize in HARD mode
     */
    if ( enableTE )
    {
	if ( clockInitHw() == 0 )
	{
	    /*
	     * handling of first 2 ticks went okay, flag that we are
	     * in HARD mode
	     */
	    te_clk.mode = TE_HANDLER_MODE_HARD;
	} 
	else
	{
	    RTLOG_ERROR(modName, "failed to synchronize on HW ticks, forcing SOFT mode!");
	}
    }

    /*
     * get system time, needed for setting t0
     */
    if ( getSysTime(&sysTime, te_clk.cpu_hz) )
    {
        RTLOG_ERROR(modName, "failed to get system time, initialization aborted!");
        
        return 1;
    }

    /*
     * initialize t0 to the current linux-system-time rounded up or down to the
     * closest TE boundary
     */
    te_clk.t0 = rtToolsLlFloor(sysTime, TEacs);
    if ( sysTime - te_clk.t0 > 240000LL )
    {
        te_clk.t0 += TEacs;
    }

    /*
     * in SOFT mode adjust samples based on current cpu count
     */
    if ( te_clk.mode == TE_HANDLER_MODE_SOFT )
    {
        /*
         * take care of properly handling the way t0 was rounded. If it was
         * rounded down from the current system time then the current time
         * is bigger than cpu_now. If it was rounded up then the current time
         * is smaller than cpu_now.
         */
        if ( sysTime >= te_clk.t0 )
        {
            te_stats.cpu_now = rt_get_time() - ACS2COUNT(sysTime - te_clk.t0);
        }
        else
        {
            te_stats.cpu_now = rt_get_time() + ACS2COUNT(te_clk.t0 - sysTime);

            /*
             * cpu_now is in the future, let's sleep until then.
             */
            rt_sleep_until(te_stats.cpu_now);
        }
        
        /*
         * no matter what, cpu_last is always cpu_now minus 48ms counts (SOFT)
         */
        te_stats.cpu_last = te_stats.cpu_now - NS2COUNT(TEns);

    }

    /*
     * initialize cpu on last TE to whatever the hw initialization found
     * out or the whatever the SOFT intialization figured out.
     */
    te_clk.cpu_on_last_te = te_stats.cpu_now;

    /*
     * cpu on next te is extrapolated based on current cpu_hz
     */
    te_clk.cpu_on_next_te = te_clk.cpu_on_last_te + _llimd(te_clk.cpu_hz, TEms, 1000);

    /*
     * log distance between t0 and system time (t0-sysTime)
     * llimd requires the first parameter to be positive, therefore, all this
     * mess for getting the logged value correct.
     * Note: just to avoid the _llimd log check for the sign explicitly (the
     * offset can be negative indeed).
     */
    {
        unsigned long long o = te_clk.t0 >= sysTime ? te_clk.t0 - sysTime : sysTime - te_clk.t0;
  
        RTLOG_INFO(modName,
                   "T0 initialized %lldus away from system time",
                   _llimd(o, 1, 10));
    }

    /*
     * init the sample buffer
     */
    initSample(te_stats.cpu_now - te_stats.cpu_last);

    /*
     * the t0 type is in any case initialized in LOCAL mode
     */
    te_clk.type = TE_HANDLER_TYPE_LOCAL;

    /*
     * our ticks count is also initialized here
     */
    te_clk.ticks = 0;

    /*
     * te_clk is fully initialized now, allow the linux thread
     * to read it.
     * Note: the atomic variable is initialized to one (unavailable)
     * at the beginning of the module.
     */
    atomic_inc(&soft_clk_access);

    /*
     * log some initialization data
     */
    RTLOG_INFO(modName,
               "initialized in %s mode (t0/cpu0/mean/hz=%lld/%lld/%lld/%lld).",
               (te_clk.mode == TE_HANDLER_MODE_SOFT) ? "SOFT" : "HARD",
               te_clk.t0,
               te_stats.cpu_now,
               te_stats.cpu_n_mean,
               _llimd(te_stats.cpu_n_mean, 1000, TEms));

    return 0;
}

/* --- public API --- */
/*
 * returns a copy of the actual clock data
 */
int RTAI_SYSCALL_MODE teHandlerGetClock(teHandlerClock_t *clk)
{
    int stat = rt_sem_wait_timed(&access_sem, nano2count(TE_HANDLER_ACCESS_TO));
    rtlogRecord_t logRecord;

    if ( stat == 0xFFFF || stat == 0xFFFE )
    {
	RTLOG_ERROR(modName,
                    "failed to take access semaphore (stat=%d)!", stat);
	return 1;
    }
 
    *clk = te_clk;

    rt_sem_signal(&access_sem);
    
    return 0;
}

/*
 * returns the current time in ACS units
 */
unsigned long long RTAI_SYSCALL_MODE teHandlerGetTime(void)
{
    int stat = rt_sem_wait_timed(&access_sem, nano2count(TE_HANDLER_ACCESS_TO));
    unsigned long long t;
    rtlogRecord_t logRecord;

    if ( stat == 0xFFFF || stat == 0xFFFE )
    {
	RTLOG_ERROR(modName, 
                    "failed to take access semaphore (stat=%d)!", stat);

	return 0;
    }

    t = computeCurrentTime(te_clk);

    rt_sem_signal(&access_sem);

    return t;
}

/*
 * get the ACS time on last tick
 */
unsigned long long RTAI_SYSCALL_MODE teHandlerGetLast(void)
{
    int stat = rt_sem_wait_timed(&access_sem, nano2count(TE_HANDLER_ACCESS_TO));
    unsigned long long t;
    rtlogRecord_t logRecord;

    if ( stat == 0xFFFF || stat == 0xFFFE )
    {
	RTLOG_ERROR(modName,
                    "failed to take access semaphore (stat=%d)!", stat);

	return 0;
    }

    t = te_clk.t0 + (RTIME)te_clk.ticks * (RTIME)TEacs;
    
    rt_sem_signal(&access_sem);

    return t;
}

/*
 * returns the mode data member of te_clk
 */
int RTAI_SYSCALL_MODE teHandlerGetMode(void)
{
    int stat = rt_sem_wait_timed(&access_sem, nano2count(TE_HANDLER_ACCESS_TO));
    int mode;
    rtlogRecord_t logRecord;

    if ( stat == 0xFFFF || stat == 0xFFFE )
    {
	RTLOG_ERROR(modName,
                    "failed to take access semaphore (stat=%d)!", stat);

	return 1;
    }
    
    mode = te_clk.mode;

    rt_sem_signal(&access_sem);
    
    return mode;
}
int RTAI_SYSCALL_MODE teHandlerGetSysOffset(teHandlerSysOffset_t *offset)
{
    int retry_count = 0;
    int access;

    /*
     * access system time offset structure
     */
    do
    {
        /*
         * read current value of access variable
         */
        access = atomic_read(&soft_offset_access);

        /*
         * copy global clock data into local variable
         */
        *offset = sys_offset;

        /*
         * if the access variable was not an odd number when we started 
         * and it has not changed since that time then we are good to go.
         */
        if ( !((access & 0x1) | (atomic_read(&soft_offset_access) ^ access)) )
        {
            break;
        }
    }
    while ( ++retry_count < 2 );

    /*
     * check whether accessing the global structure was a success or not
     */
    if ( retry_count == 2 )
    {
        return 1;
    }
    
    return 0;
}

/*
 * export the public API
 */
static struct rt_fun_entry teHandlerFun[] = 
{
    [ TE_HANDLER_GETTIME ]   = { 1, teHandlerGetTime            },
    [ TE_HANDLER_GETLAST ]   = { 1, teHandlerGetLast            },
    [ TE_HANDLER_GETMODE ]   = { 1, teHandlerGetMode            },
    [ TE_HANDLER_GETCLK ]    = { 1, teHandlerGetClock           },
    [ TE_HANDLER_GETOFFSET ] = { 1, teHandlerGetSysOffset       },
};

EXPORT_SYMBOL (teHandlerGetTime);
EXPORT_SYMBOL (teHandlerGetClock);
EXPORT_SYMBOL (teHandlerGetLast);
EXPORT_SYMBOL (teHandlerGetMode);
EXPORT_SYMBOL (teHandlerGetSysOffset);

/* --- end of public API --- */

/*
 * System time getter/setter task.
 * At each iteration of its infinite loop this linux task reads the
 * global TE clock data structure and it read the current system time
 * by means of do_gettimeofday. From these two pieces of information
 * it then computes the current time offset (TE - Sys). Using this
 * offset value it then sets the linux time accordingly, by means of
 * do_settimeofday. System time and offset are kept in a global variable
 * that the rtai side of this application can access when requiered by
 * means again of an atomic guarded read.
 * This task iterates every 48ms. 
 */
static int sysTimeAdjustTaskFun(void *not_used)
{
    int status, access;
    unsigned int period_count = 0, resync_count = 0;
    teHandlerClock_t clk;
    RTIME t0 = 0;
    struct timeval tv_get;
    teHandlerSysOffset_t offset;
    unsigned long long slept_cpu = 0;
    rtlogRecord_t logRecord;

    /*
     * initialize our local offset variable
     */
    offset.sys = 0;
    offset.value = 0;
    offset.start = 0;
    offset.min = 999999999;
    offset.max = 0;
    offset.set_count = 0;
    offset.set_err_count = 0;
    offset.access_err_count = 0;
    offset.glitch_err_count = 0;

    /*
     * first thing we should do is to ingest the current system
     * time into the offset structure. This is so because intialization
     * of the real-time task requires to know the current system
     * time to properly set its t0 value.
     */
    do_gettimeofday(&tv_get);
    atomic_inc(&soft_offset_access);
    sys_offset.cpu = rdtsc();
    sys_offset.sys =
        TE_HANDLER_UNIX_OFFSET
        + (RTIME)tv_get.tv_sec * (RTIME)10000000
        + (RTIME)tv_get.tv_usec * (RTIME)10;
    atomic_inc(&soft_offset_access);

    RTLOG_INFO(modName, "system time task started");

    /*
     * keep working hard until the end of days...
     */
    while ( !kthread_should_stop() )
    {
        int retry_count;
        RTIME t, delta;

        /*
         * always increment the loop counter, this counter is used for
         * checking whether is time for applying a clock adjustment or
         * not. The actual periodicity is controlled by setTimePeriod.
         */
        period_count++;

        /*
         * access to the clock data is guarded by means of seqlock type
         * mechanism. The atomic access is incremented by the main task
         * before and after modifying the te_clk variable (its initial
         * value was zero). This means that we need to check that the
         * access variable is not an odd number and that it did not
         * change while the copy of the clock data to a local variable
         * was happening. If it happens that we need to retry then we will
         * retry only a second time.
         */
        retry_count = 0;
        t = 0;
        do
        {
            /*
             * read current value of access variable
             */
            access = atomic_read(&soft_clk_access);

            /*
             * copy global clock data into local variable
             */
            clk = te_clk;

            /*
             * if the access variable was not an odd number when we
             * started and it has not changed since that time then
             * we are good to go.
             */
            if ( !((access & 0x1) | (atomic_read(&soft_clk_access) ^ access)) )
            {
                /* 
                 * use the clock data to compute the current TE time.
                 */
                t = computeCurrentTime(clk);

                break;
            }
        }
        while ( ++retry_count < 2 );

        /*
         * get the unix time and current cpu time
         */
        delta = rdtsc();
        do_gettimeofday(&tv_get);
        offset.cpu = rdtsc();
        delta = offset.cpu - delta;

        /*
         * computing the system time and the offset is required only when 
         * the TE data is actually available.
         */
        if ( t != 0 )
        {
            /* 
             * compute current unix time in ACS units.
             */
            offset.sys =
                TE_HANDLER_UNIX_OFFSET
                + (RTIME)tv_get.tv_sec * (RTIME)10000000
                + (RTIME)tv_get.tv_usec * (RTIME)10;
            
            /*
             * Compute offset. Offset is the difference in microseconds
             * between the time computed out of the clock data (t0, ticks,
             * cpu_hz, and rt_get_time) and the current unix time (as
             * reported before by do_gettimeofday).
             * Note: take explicit care of negative parameter, current version
             * of _llimd is logging when the input is negative.
             */
            offset.value = t >= offset.sys ? llimd(t - offset.sys, 1, 10) : -llimd(offset.sys - t, 1, 10);
        }

        /*
         * the following is used just for trouble shooting, slow laptops
         * specially. Disabled by default.
         */
        if ( 0 && t != 0 )
        {
            /*
             * Check whether the new system time looks 'reasonable'.
             * If it does not look right then drop it and do not update
             * our current memory of it. For some reason, it looks like,
             * calling do_gettimeofday sometimes returns a value which
             * is a couple of milliseconds too much into the future or
             * in the past. Those 'glitches' do not seem to be real, in
             * the sense that dropping their sampling and repeating the
             * measurement 48ms later does not show the big difference
             * again.
             */
            if ( (t - offset.sys > 10000LL) || (offset.sys - t > 10000LL) )
            {
                RTLOG_INFO(modName, "--- big diff %lld %lld %lld %lld %lld %u %u %lld %u %llu %u", t - offset.sys, t, offset.sys, count2nano(delta), clk.t0, clk.ticks, clk.cpu_hz, clk.cpu_on_last_te, offset.set_count, slept_cpu, cpu_khz);
                
                offset.glitch_err_count++;
                
                /*
                 * update offset structure with the information that was
                 * possible to get during this iteration.
                 * Note: we do not need the atomic guard when updating
                 * one single entry which is equal or shorter than the
                 * native bit size (32 bits at the moment of this writting.)
                 */
                sys_offset.glitch_err_count = offset.glitch_err_count;
                
                //goto continue_loop_with_clock_data_error;
            } 
        }

        /*
         * if it was not possible to read the clock data from the
         * real-time task then just log that event, telling
         * that we will try again in the next loop. Repeated occurrences
         * of this event would deteriorate the synchronicity between TE handler
         * and the system time. The cpu and system times look good,
         * therefore, update their values in the global structure.
         * Note: checking for t0 not null on the data structure let's us 
         * filter out those event that happen at the very beginning while the
         * real time task has not started jet.
         */
        if ( t == 0 )
        {
            RTGUARD(rtGuard, 0, 10, RTGUARD_MODE_TIMER);
         
            if ( t0 != 0 )
            {
                RTLOG_GUARD(&rtGuard, RTLOG_WARNING, "clock data not available at this time (t/t0=%lld/%lld)", t, clk.t0);
            }

            offset.access_err_count++;

            /*
             * update offset structure with the information that was
             * possible to get during this iteration.
             * Note: we do not need the atomic guard when updating
             * one single entry which is equal or shorter than the
             * native bit size (32 bits at the moment of this writting.)
             */
            sys_offset.access_err_count = offset.access_err_count;

            goto continue_loop_with_clock_data_error;
        }

        /*
         * Adjust (step) system time based on computed offset.
         * Adjustment actually happens only under the following circumstances
         * (always applicable to HARD mode only and the offset is available):
         *
         *       - the task has never before adjusted the time.
         *       - the adjustmet period has been reached.
         *       - t0 has changed.
         *       - TE handler was forced to resync.
         */
        if ( (clk.mode == TE_HANDLER_MODE_HARD
              &&
              t != 0)
             &&
             (period_count == setTimePeriod
              ||
              clk.t0 != t0
              ||
              clk.resync_count > resync_count) )
        {
            /*
             * setSysTime implements the real work
             */
            if ( (status = setSysTime(clk, offset.value, tv_get)) )
            {
                RTGUARD(rtGuard, 0, 10, RTGUARD_MODE_TIMER);
                
                RTLOG_GUARD(&rtGuard,
                            RTLOG_ERROR,
                            "failed to adjust system time (err=%d)",
                            status);

                offset.set_err_count++;
            }
            else
            {
                /*
                 * increment set counter
                 */
                offset.set_count++;

                /*
                 * if this adjustment was due to an event in the TE handler (new
                 * t0 or resync) then make the perido counter to restart anew.
                 */
                if ( clk.t0 != t0 || clk.resync_count != resync_count )
                {
                    period_count = setTimePeriod;
                }
            }
        }

        /*
         * every time t0 changes (startup pr resync) record the offset 
         * and reset its statistics; otherwise, keep track of min and max
         * observed offset.
         */
        if ( t0 != clk.t0 )
        {
            offset.start = offset.value;

            offset.min = 999999999;
            
            offset.max = 0;

            RTLOG_DEBUG(modName, "system time offset reset %lld [us]", offset.start);
        }
        else
        {
            if ( offset.value < offset.min )
            {
                offset.min = offset.value;
            }
            if ( offset.value > offset.max )
            {
                offset.max = offset.value;
            }
        }

        /*
         * update global variable with current offset figures. Wrap the copy
         * with increments to the global access variable, in this way the reader
         * part (main task) can thus estimate whether its readout is valid
         * or not.
         */
        atomic_inc(&soft_offset_access);
        sys_offset = offset;
        atomic_inc(&soft_offset_access);

        /*
         * t0 is recorded in a separated memory, such that is possible
         * to detect changes of its value. Such a change happens while
         * the SETT0 command is received and in those cases this task 
         * must promptly adjust the time based on the new t0.
         */
        t0 = clk.t0;
            
        /*
         * resync_count is recorded in a separated memory, such that is possible
         * to detect changes of its value. Such a change happens as a consequence
         * ot the RESYNC command. In these case this task must promptly adjust the
         * time based on the new phase.
         */
        resync_count = clk.resync_count;

    continue_loop_with_clock_data_error:
        /*
         * reset the adjust counter
         */
        if ( period_count == setTimePeriod )
        {
            period_count = 0;
        }

        /*
         * sleep until the next time the system time should be adjusted
         */
        slept_cpu = rdtsc();
        msleep(TEms);
        slept_cpu = rdtsc() - slept_cpu;

    } /* while ( !kthread_should_stop() ) */

    return 0;
}

/*
 * main task
 */
void mainTaskFun(long not_used)
{
    int status, free_wheeling_count = 0;
    int timed_out = 0;
    int offset_access_status;
    teHandlerSysOffset_t offset;
    RTIME now = 0, jitter, sem_wait_until, d1, d1max, d2, d2max, dtmax;
    RTIME spurious_phase = 0;
    char mode[10];
    rtLogTimeData_t rtlogData;
    rtlogRecord_t logRecord;

    /*
     * . initialize stat variables
     * . mode = LOCAL
     * . go into endless loop
     *		- if SOFT is on
     *			. if just starting (n = 0) 
     *				then
     *				- t0=do_gettimeofday
     *				- cpu0=f(t0)
     *			. delay=(n+1)TE - (get_time - cpu0)
     *			. sleep for delay
     *			. note that here we have two ticks in any case!
     *			. update cpu_last and cpu_now variables with 'perfect'
     *                    values
     *		- else (using hardware TE)
     *			. pend on handler semaphore with a timeout TEms+1
     *			. update cpu_last and cpu_now variables
     *			. if timed out 
     *				then
     *				- initialize stat variables
     *				- SOFT = TRUE
     *				- log
     *				- continue
     *				else if just starting (n = 0) 
     *					then
     *					continue (we need to have two ticks) 
     *		- compute d1 (delay between interrupt handling and this moment)
     *		- store sample (n++)
     *		- compute statistics
     *		- if n >= SAMPLE_SIZE 
     *			then
     *			. if statistics not okay
     *				then
     *				- initialize stat variables
     *				- log
     *				- continue
     *			else
     *			. populate clock data structure
     *			. put clock data into the mail box
     *		- compute d2
     *		- log delays info (d1, d2, d1+d2,???)
     *		- check for command and if any 
     *			case SETT0: 		
     *				t0 = cmd param		
     *				if ! SOFT
     *					clk 
     *			case SOFT:
     *				initialize stat variables
     *				disable interrupts
     *				SOFT = TRUE
     *			case HARD:
     *				initialize stat variables
     *				enable interrupts
     *				SOFT = FALSE
     *		- continue the endless loop
     */

    /*
     * initialize timing performance figures
     */
    d2 = 0;
    d1max = d2max = dtmax = 0;

    RTLOG_INFO(modName, "main task spawned 0x%x", (unsigned int)((int *)rt_whoami())[2]);

    /*
     * this global flag marks the status of this task.
     */
    atomic_set(&main_task_run, 1);

    RTLOG_INFO(modName,
               "starting infinite loop at cpu/acs=%lld/%llu",
               rt_get_time(),
               teHandlerGetTime());

    /*
     * main loop. It exits on error or when the run flag is reset.
     */
    while ( atomic_read(&main_task_run) )
    {
	/*
	 * while in HARD and FW modes we block on the semaphore expecting an
	 * interrupt to happen.
	 */
	if ( te_clk.mode == TE_HANDLER_MODE_HARD ||
	     te_clk.mode == TE_HANDLER_MODE_FW )
	{
            /*
             * default allowed jittering interval. If timing out beyond
             * this margin or getting a tick before this margin then we
             * assume there is a problem. Either a missed tick or a
             * spurious one.
             */
            jitter = TE_HANDLER_ACCEPTED_DEV_N * te_stats.cpu_n_std;

	    /*
	     * wait for the synchronization semaphore. If there is no hw
	     * interruption happening then we will timeout on this call.
	     * The timeout has been choosen as ...
	     */
	    sem_wait_until = NS2COUNT(nsMaxJitClip);
	    if ( jitter < sem_wait_until )
	    {
		jitter = sem_wait_until;
	    }
	    sem_wait_until = te_stats.cpu_now + te_stats.cpu_n_mean + jitter;
	    
	    /*
	     * it happens in RTAI that rt_sem_wait_until will actually timeout
	     * a few micro-seconds before the given time. With the following
	     * statement we are sure that we have waited until the expected moment.
	     */
	    do 
	    {
		/*
		 * if the sem is invalid then break this loop
		 */
		if ( (status = rt_sem_wait_until(&sem , sem_wait_until))
                     == SEM_ERR )
		{
		    break;
		}
	    }
	    while ( status == SEM_TIMOUT && rt_get_time() < sem_wait_until );

	    /*
	     * if this is a hopeless situation then break the main loop
	     */
	    if ( status == SEM_ERR )
	    {
		RTLOG_ERROR(modName,
                            "invalid synchronization semaphore. Main task aborted!");
		break;
	    }

	    /*
	     * set the timed_out flag. Later on to be passed to the Update function.
	     */
	    timed_out = (status == SEM_TIMOUT) ? 1 : 0;
 
	    /* 
	     * if timed out then set the mode and cpu_now accordingly:
	     *  - a timeout means that there was not handler invokation, therefore,
	     *	  fake the cpu_now as cpu_on_last_te plus TE
	     *	- no timeout means the handler was call and therefore
	     *    handler_cpu_now is correct
	     */
	    if ( timed_out )
	    {
		if ( te_clk.mode == TE_HANDLER_MODE_HARD )
		{
		    RTLOG_INFO(modName, 
                               "FW started (tick/mean/std/jitter=%u/%lld/%lld/%lld[us] freq=%u[hz])",
                               te_clk.ticks,
                               COUNT2US(te_stats.cpu_n_mean),
                               COUNT2US(te_stats.cpu_n_std),
                               COUNT2US(jitter),
                               te_clk.cpu_hz);
		    
		    /*
		     * reset count of free-wheeling counts
		     */
		    free_wheeling_count = 0;
		}

		/*
		 * we are free-wheeling, keep track of the count
		 */
		free_wheeling_count++;

		/*
		 * adjust 'now' as for precise SOFT type timing.
		 */
                now = adjustToSoftTick((RTIME)te_clk.ticks * TEacs + te_clk.t0,
                                       te_stats.cpu_n_mean,
                                       te_clk.cpu_hz);

		d1 = now; /* just for faked performance measurement */
	    }
	    else /* did not timeout on handler semaphore */
	    {
		/*
                 * read cpu count recored by the ISR
                 */
		if ( readISRVariable(&now) )
		{
		    RTLOG_ERROR(modName, "cannot read ISR cpu counts!");
		    
		    break;
		}

                /*
                 * used for performance measurement
                 */
		d1 = rt_get_time();

                /*
                 * keep track of any spurious tick phase. If one is detected
                 * then this variable will contain its phase, otherwise, it
                 * will remain zero.
                 */
                spurious_phase = 0;

		/*
		 * check for a spurious tick
		 */
		if ( now < ( te_stats.cpu_now + te_stats.cpu_n_mean - jitter) )   
		{
                    /*
                     * if a re-syncing command is on curse then take this
                     * 'spurious' as a good one, otherwise log the spurious event
                     * and continue from the top of the while-loop
                     *
                     */
                    if ( atomic_read(&resync_flag) )
                    {
                        RTLOG_INFO(modName,
                                   "re-syncing tick detected "
                                   "(tick/mean/std/slept=%u/%lld/%lld/%lld[us] freq=%u[hz])",
                                   te_clk.ticks,
                                   te_stats.cpu_n_mean,
                                   te_stats.cpu_n_std,
                                   COUNT2US(now - te_stats.cpu_now),
                                   te_clk.cpu_hz);
                    }
                    else
                    {
                        /*
                         * keep track of the spurious tick phase.
                         */
                        spurious_phase = COUNT2US(now - te_stats.cpu_now);

                        /*
                         * log the following only when in HARD mode, otherwise,
                         * an out of phase signal in FW mode would flood the
                         * logging system.
                         */
                        if ( te_clk.mode == TE_HANDLER_MODE_HARD )
                        {
                            RTLOG_DEBUG(modName,
                                        "spurious tick detected "
                                        "(tick/mean/std/slept=%u/%lld/%lld/%lld[us])",
                                        te_clk.ticks,
                                        te_stats.cpu_n_mean,
                                        te_stats.cpu_n_std,
                                        spurious_phase);
                        }
                        
                        /*
                         * wait again for the real tick, we are dropping
                         * this one because it looks spurious.
                         */
                        continue;
                    }
		}
                else if ( te_clk.mode == TE_HANDLER_MODE_FW )
                {
                    RTLOG_INFO(modName, 
                               "rendezvous with TE while in FW mode "
                               "(tick/fw_ticks/cpu_n_mean/cpu_n_std/delta=%u/%d/%lld/%lld/%lld[us])",
                               te_clk.ticks,
                               free_wheeling_count,
                               te_stats.cpu_n_mean,
                               te_stats.cpu_n_std,
                               /* take care of difference sign */
                               now >= te_stats.cpu_now + te_stats.cpu_n_mean ?
                               COUNT2US(now - (te_stats.cpu_now + te_stats.cpu_n_mean)) :
                               -COUNT2US(-now + (te_stats.cpu_now + te_stats.cpu_n_mean)));
                }
	    }
	}
	else /* SOFT mode */
	{
            /*
             * sleep until the next computed tick boundary
             */
	    sem_wait_until = te_stats.cpu_now + te_stats.cpu_n_mean;
	    do
	    {
		rt_sleep_until(sem_wait_until);
	    }
	    while ( rt_get_time() < sem_wait_until );

            /*
             * measure d1 as the time we expend in this 'else' statement
             */
            d1 = rt_get_time();

            /*
             * adjust now to computed estimation of the cpu counts at this
             * new tick.
             * Note: the function sleeps if 'now' is bigger than the current count.
             */
            now = adjustToSoftTick((RTIME)te_clk.ticks * TEacs + te_clk.t0,
                                   te_stats.cpu_n_mean,
                                   te_clk.cpu_hz);

            /*
             * in SOFT mode d1 measures the time we expended within this
             * 'else' statement.
             */
            d1 = rt_get_time() - d1 + now;
	}

	/*
	 * shift statistics memory
	 */    
	te_stats.cpu_last = te_stats.cpu_now;
	te_stats.cpu_now = now;

	/*
	 * record the delay (us) between the interrupt handling and this task
        * being scheduled (sem unblocked). In micro-seconds.
	 */
	d1 = COUNT2US(d1 - te_stats.cpu_now);
	if ( d1 > d1max ) 
	{
	    d1max = d1;
	}

	/*
	 * d2 is the time needed for computing statistics and posting the clock
	 * data into the mbx. Here we record the startin time and see below for its
	 * final computation. In micro-seconds.
	 */
	d2 = rt_get_time();

        /* 
         * In hard mode we keep statistics of the cpu clock and
         * compare them against module's parameters. If there was a timeout
         * then we are doomed to leave HARD mode, therefore, check that
         * flag too.
         */
        if ( te_clk.mode == TE_HANDLER_MODE_HARD && timed_out == 0 )
        {
            /*
             * store sample of our main variable and compute statistics
             */
            addSample();
            
            /*
             * validate our data based on the available statistics.
             */
            if ( validateClockData() != 0 )
            {
                RTLOG_ERROR(modName,
                            "not good statistics (tick/mean/std=%u/%lld/%lld[counts])",
                            te_clk.ticks,
                            te_stats.cpu_n_mean,
                            te_stats.cpu_n_std);
                
                break;
            }
        }

	/*
	 * update clock data based on actual statistics
	 */
	if ( updateClockData(te_clk, timed_out) )
	{
	    RTLOG_ERROR(modName,
                        "failed to update clock data.");
	    break;
	}

	/*
	 * always publish the data
	 */
	if ( publishClockData() )
	{
	    RTLOG_ERROR(modName, "failed to publish clock data.");

	    break;
	}

        /*
         * update system time offset from global variable. Here we need the
         * offset information just for logging sake. If we do not manage to
         * copy successfully from global to local scope then simply log a 
         * flag telling so.
         */
        offset_access_status = atomic_read(&soft_offset_access);
            
        /*
         * copy from global to local scope
         */
        offset = sys_offset;
            
        /*
         * if we failed to read the offset data then flag it as unavailable in
         * the logged info.
         */
        if ( (offset_access_status & 0x1) |
             (atomic_read(&soft_offset_access) ^ offset_access_status) )
        {
            offset_access_status = 0;
        }
        else
        {
            offset_access_status = 1;
        }

	/*
	 * d2 computation.
	 */
	d2 = COUNT2US(rt_get_time() - d2);
	if ( d2 > d2max ) 
	{
	    d2max = d2;
	}

        /*
         * dtmax computation (dt = d1 + d2)
         */
        if ( d1 + d2 > dtmax )
        {
            dtmax = d1 + d2;
        }

	/*
	 * debugging logs and rtlog data
	 */
	if ( te_clk.ticks % 100 == 0 )
	{
            /*
             * update rtlog clock data
             */
            rtlogData.cpu_on_last_te = te_clk.cpu_on_last_te;
            rtlogData.cpu_hz = te_clk.cpu_hz;
            rtlogData.ticks = te_clk.ticks;
            rtlogData.t0 = te_clk.t0;
            setRtLogTimeData(rtlogData);

            /*
             * nice soft mode string
             */
	    switch(te_clk.mode)
	    {
	    case TE_HANDLER_MODE_SOFT:
		strcpy(mode, "SOFT");
		break;
	    case TE_HANDLER_MODE_HARD:
		strcpy(mode, "HARD");
		break;
	    case TE_HANDLER_MODE_FW:
		sprintf(mode, "FW(%d)", free_wheeling_count);
		break;
	    default:
		strcpy(mode, "????");
		break;
	    }

            /*
             * what are we logging here?
             *
             * - mode and type
             * - ticks = number of ticks so far
             * - mean/std = meand and std of ticks period
             * - sys = wheter the logged data is current or not
             *         times that do_settimeofday has failed
             *         times that soft task has failed to access global data
             *         times that soft task has detected a system time glitch
             *         current offset
             *         smallest measured offset (signed value)
             *         biggest measured offset
             *         (where system time offset = te handler time - OS system time)
             * - d1 = current and max value of d1, that is, time it took for
             *        this task to run after the tick interrupt was handled
             * - d2 = current and max value of d2, that is, task processing time
             * - dtmax = max value of d1+d2 
             * - sp = spurious tick phase
             * - f = estimated cpu frequency
             *
             */
	    RTLOG_DEBUG(modName, 
                        "%c%s ticks=%u mean/std=%lld/%lld[us] sys=%d/%u/%u/%u/%lld/%lld/%lld[us] d1=%lld/%lld[us] d2=%lld/%lld[us] dtmax=%lld[us] sp=%lld[us] f=%u[hz]", 
                        (te_clk.type == TE_HANDLER_TYPE_ARRAY) ? '*' : ' ',
                        mode,
                        te_clk.ticks, 
                        COUNT2US(te_stats.cpu_n_mean),
                        COUNT2US(te_stats.cpu_n_std),
                        offset_access_status,
                        offset.set_err_count, offset.access_err_count, offset.glitch_err_count,
                        offset.value, offset.min, offset.max,
                        d1, d1max,
                        d2, d2max,
                        dtmax,
                        spurious_phase,
                        te_clk.cpu_hz);

            /* this log was used for debugging the drifting-mean problem.
               data produced by this log can be inspected here:
               http://almasw.hq.eso.org/almasw/bin/view/CORR/WeeklyReports2007Jul20

               RTLOG_INFO(modName, 
               "TEST14 %c%s ticks=%u mean/std=%lld/%lld/%lld/%lld/%lld/%lld [us] sys=%d/%lld/%lld/%lld[us] d1=%lld/%lld[us] d2=%lld/%lld[us] f=%u[hz]", 
               (te_clk.type == TE_HANDLER_TYPE_ARRAY) ? '*' : ' ',
               mode,
               te_clk.ticks, 
               COUNT2US(te_stats.cpu_n_mean),
               COUNT2US(te_stats.cpu_n_std),
               te_stats.cpu_n_mean,
               te_stats.cpu_n_std,
               te_stats.cpu_last,
               te_stats.cpu_now,
               sys_failures, sys_offset, sys_offset_min, sys_offset_max,
               d1, d1max,
               d2, d2max,
               te_clk.cpu_hz);
            */
	}
    } /* while(1) */

    RTLOG_INFO(modName, "main task has terminated (cpu=%lld)!", rt_get_time());
    
    /*
     * this flag will indicate to the cleanup function that this
     * task has really finished.
     */
    atomic_set(&main_task_done, 1);

    rt_task_delete(rt_whoami());
}

/*
 * intialization task
 */
void initTaskFunction(long not_used)
{
    rtlogRecord_t logRecord;

    RTLOG_INFO(modName, "initialization task started");

    /*
     * initialize statistics and clock data, in HARD mode this will blocks
     * for at least 2 ticks, in SOFT mode is quick
     */
    if ( clockInit() )
    {
	RTLOG_ERROR(modName, "clock initialization has failed!");

	return;
    }

    /*
     * spawn the main task. Set it to run in the first cpu. Note that the
     * cpu id mask seems to be differnt than when using the same function
     * from user space.
     */
    if ( rt_task_init_cpuid(&mainTask, mainTaskFun, 0,
			    TE_HANDLER_TASK_STACK, TE_HANDLER_TASK_PRIO,
			    0, 0, 0x0)
	 != 0 )
    {
	RTLOG_ERROR(modName, "failed to spawn main task!");

	return;
    }
    
    /*
     * resume the main task
     */
    rt_task_resume(&mainTask);
    
    /*
     * the fifo task handles incomming commands, its priority is set very low
     */
    if ( rt_task_init_cpuid(&fifoTask, fifoTaskFun, 0,
			    TE_HANDLER_TASK_STACK, TE_HANDLER_TASK_PRIO + 10,
			    0, 0, 0x0)
	 != 0 )
    {
	RTLOG_ERROR(modName, "failed to spawn fifo task!");

	return;
    }
    
    /*
     * resume the fifo task
     */
    rt_task_resume(&fifoTask);
 
    RTLOG_INFO(modName, "initialization task function terminating...");

    atomic_set(&init_flag, 1);

    rt_task_delete(rt_whoami());
}

/*
 * clean-up task
 */
void cleanUpTaskFunction(long not_used)
{
    rtlogRecord_t logRecord;

    RTLOG_INFO(modName, "clean-up task started");

    /*
     * command the fifo task to finish
     */
    RTLOG_INFO(modName, "terminating fifo task...");
    if ( terminateTask(&fifoTask, &fifo_task_run, &fifo_task_done,
                       TE_HANDLER_FIFO_PERIOD / 1000000) )
    {
        RTLOG_ERROR(modName, "fifo task did not finish on time!");
    }

    /*
     * command the main task to finish
     */
    RTLOG_INFO(modName, "terminating main task...");
    if ( terminateTask(&mainTask, &main_task_run, &main_task_done, 2 * TEms) )
    {
        RTLOG_ERROR(modName, "main task did not finish on time!");
    }
    
    RTLOG_INFO(modName, "clean-up task function terminating...");

    /*
     * set our own done flag to true
     */
    atomic_set(&cleanup_flag, 1);

    rt_task_delete(rt_whoami());
}

int teHandler_main(int stage)
{
    int status = RT_TOOLS_MODULE_INIT_SUCCESS;
    int fifoStat;
    RT_TASK initTask, cleanUpTask;
    rtlogRecord_t logRecord;

    if ( stage == RT_TOOLS_MODULE_STAGE_INIT )
    {
	RTLOG_INFO(modName, "initializing module...");

	/*
	 * log RCS ID (cvs version) and module params
	 */
        RTLOG_INFO(modName, "$Id$");
	RTLOG_INFO(modName, "initTimeout        = %ums", initTimeout);
	RTLOG_INFO(modName, "cleanUpTimeout     = %ums", cleanUpTimeout);
	RTLOG_INFO(modName, "rtai extension idx = %d", TE_HANDLER_IDX);
	RTLOG_INFO(modName, "hwMod              = %s", hwMod);
	RTLOG_INFO(modName, "maxJitClip         = %dus", maxJitClip);
	RTLOG_INFO(modName, "maxStd             = %dus", maxStd);
        RTLOG_INFO(modName, "setTimePeriod      = %u ticks", setTimePeriod);

	goto Initialization; 
    }
    else
    {
	RTLOG_INFO(modName, "cleaning up module...");

	goto FullCleanUp;
    }

Initialization:
    
    /*
     * initialized the system time offset and clock data structures
     */
    memset(&sys_offset, 0x0, sizeof(sys_offset));
    memset(&te_clk, 0x0, sizeof(te_clk));

    /*
     * passed parameters maxJitClip and maxStd come in microseocnds, convert
     * them to nanos.
     */
    if ( maxJitClip < 0 )
    {
	RTLOG_ERROR(modName, "invalid maxJitClip value (%d)", maxJitClip);

	status = RT_TOOLS_MODULE_PARAM_ERROR;

	goto Exit;
    }
    nsMaxJitClip = 1000LL * maxJitClip;

    if ( maxStd == 0 )
    {
	RTLOG_ERROR(modName, "invalid maxStd value (%d)", maxStd);

	status = RT_TOOLS_MODULE_PARAM_ERROR;

	goto Exit;
    }
    else if ( maxStd > 0 )
    {
	nsMaxStd = 1000LL * maxStd;
	maxStdFlag = 1;
    }
    else
    {
	nsMaxStd = 1000LL * (-maxStd);
	maxStdFlag = 0;
    }

    /*
     * check that the sys time adjutment period is valid
     */
    if ( setTimePeriod == 0 )
    {
        RTLOG_ERROR(modName,
                    "invalid setTimePeriod value (%u)!", setTimePeriod);

        goto Exit;
    }

    /*
     * create fifos used for user land communication (set T0 and get time)
     */
    if ( (fifoStat = rtf_create(TE_HANDLER_CMD_FIFO,
                                sizeof(teHandlerClock_t)))
	 < 0 )
    {
	RTLOG_ERROR(modName, 
                    "Cannot create T0 fifo (err=%d).", fifoStat);

	status = RT_TOOLS_MODULE_INIT_ERROR;

	goto Exit;
    }

    /*
     * reset the T0 input fifo (just in case of an old buffered value)
     */
    if ( (fifoStat = rtf_reset(TE_HANDLER_CMD_FIFO)) )
    {
	RTLOG_ERROR(modName,
                    "failed to reset the T0 input fifo (err=%d).", fifoStat);

	status = RT_TOOLS_MODULE_INIT_ERROR;

	goto Level1CleanUp;
    }

    /*
     * hook a handler function to the fifo
     */
    if ( (fifoStat = rtf_create_handler(TE_HANDLER_CMD_FIFO,
                                        X_FIFO_HANDLER(fifoHandler))) )
    {
	RTLOG_ERROR(modName,
                    "Cannot hook a handler function to the fifo (err=%d).", fifoStat);

	status = RT_TOOLS_MODULE_INIT_ERROR;

	goto Level1CleanUp;
    }

    /*
     * initialize the fifo semaphore
     */
    if ( (fifoStat = rtf_sem_init(TE_HANDLER_CMD_FIFO, 1)) )
    {
	RTLOG_ERROR(modName,
                    "cannot initialize cmd fifo semaphore (err=%d)!", fifoStat);
        
	status = RT_TOOLS_MODULE_INIT_ERROR;
        
	goto Level1CleanUp;
    }

    /*
     * get reference to hardware TE interrupt functions
     */
    if ( hwMod != 0 && strlen(hwMod) != 0 )
    {
	if ( (enableTE = (int (*)(void (*)(int)))rtToolsLookupSymbol(hwMod,
								     "enableTE"))
	     == 0 )
	{
	    RTLOG_ERROR(modName, 
                        "cannot resolve TE enable interrupts symbol (sym=%s:%s)!",
                        hwMod, "enableTE");
	    
	    status = RT_TOOLS_MODULE_INIT_ERROR;
	    
	    goto Level1CleanUp;
	}
	if ( (disableTE = (int (*)(void))rtToolsLookupSymbol(hwMod, "disableTE"))
	     == 0 )
	{
	    RTLOG_ERROR(modName, 
                        "cannot resolve TE disable interrupts symbol (sym=%s:%s)!",
                        hwMod, "disableTE");

	    status = RT_TOOLS_MODULE_INIT_ERROR;

	    goto Level1CleanUp;
	}
	RTLOG_INFO(modName, 
                   "HW TE interrupts to be provided by %s module (e=0x%p, d=0x%p).",
                   hwMod, (void *)enableTE, (void *)disableTE);
    }
    
    /*
     * make available to user land the important functions.
     */
    if ( set_rt_fun_ext_index(teHandlerFun, TE_HANDLER_IDX) )
    {
	RTLOG_ERROR(modName,
                    "rtai extension failure, try with a differnt IDX (idx=%d)!",
                    TE_HANDLER_IDX);
	
	status = RT_TOOLS_MODULE_INIT_ERROR;
	
	goto Level2CleanUp;
    }
    else
    {
	RTLOG_INFO(modName, "rtai extension installed");
    }

    /*
     * report here rtai's own estimation for the cpu frequency
     */
    RTLOG_INFO(modName, "CPU_FREQ=%lu", CPU_FREQ);

    /*
     * initialize semaphore and mail-box objects
     */
    rt_typed_sem_init(&access_sem, 1, RES_SEM);
    rt_typed_sem_init(&fifo_sem, 0, BIN_SEM);
    rt_typed_sem_init(&sem, 0, BIN_SEM);
    /*rt_tbx_init(&clk_tbx, sizeof(teHandlerClock_t) + 1, FIFO_Q);*/
    rt_mbx_init(&clk_tbx, sizeof(teHandlerClock_t));
    if ( rt_register(nam2num(TE_HANDLER_CLK_MBX_NAME), &clk_tbx, IS_MBX,
		     current)
	 == 0 )
    {
	RTLOG_ERROR(modName, "cannot register CLK tbx.");
	
	status = RT_TOOLS_MODULE_INIT_ERROR;
	
	goto Level3CleanUp;
    }

    /*
     * spawn the system time adjusting task. create the task object, it won't
     * run until commanded to do so
     */
    sysTimeAdjustTask = kthread_create(sysTimeAdjustTaskFun, NULL, "teHandler");
        
    /*
     * if the task did not create successfully then bail out 
     */
    if ( IS_ERR(sysTimeAdjustTask) )
    {
        goto Level4CleanUp;
    }

    /*
     * bind the task to the same cpu where the main task is running
     */
    kthread_bind(sysTimeAdjustTask, 0);
    
    /*
     * wake up (run) the system time adjusting task
     */
    if ( wake_up_process(sysTimeAdjustTask) == 0 )
    {
        goto Level5CleanUp;
    }

    /*
     * give time to the linux thread to update the system time
     * at leats once.
     */
    msleep(10);
    
    /*
     * initialization of resources is finished. Now spawn the rtai
     * task that takes care of doing the asynchronous part of the 
     * initialization.
     */
    if ( rt_task_init(&initTask,
		      initTaskFunction,
		      0,
		      RT_TOOLS_INIT_TASK_STACK,
		      RT_TOOLS_INIT_TASK_PRIO,
		      0, 0)
	 != 0 )
    {
	RTLOG_ERROR(modName, "failed to spawn initialization task!");

	status  = 1; /* or some other code, defined in ... */

	goto Level5CleanUp;
    }

    /*
     * resume the initialization task
     */
    rt_task_resume(&initTask);

    /*
     * wait for the initialization task to be ready
     */
    if ( rtToolsWaitOnFlag(&init_flag, initTimeout) )
    {
	/*
	 * okay, the init task was not ready on time, now forcibly remove
	 * it for the rtai's scheduler
	 */
	rt_task_suspend(&initTask);
	rt_task_delete(&initTask);

	RTLOG_ERROR(modName,
                    "initialization task did not report back on time!");

	status  = RT_TOOLS_MODULE_NO_INIT_TASK_ERROR;

	goto FullCleanUp;
    }

    goto Exit;

FullCleanUp:
    /*
     * asynchronous clean-up phase to be handled by this task
     */
    if ( rt_task_init(&cleanUpTask,
                      cleanUpTaskFunction,
                      0,
                      RT_TOOLS_CLEANUP_TASK_STACK,
                      RT_TOOLS_CLEANUP_TASK_PRIO,
                      0, 0)
         == 0 )
    {
        /*
         * resume the clean-up task
         */
        rt_task_resume(&cleanUpTask);

        /*
         * wait for the initialization task to be ready
         */
        if ( rtToolsWaitOnFlag(&cleanup_flag, cleanUpTimeout) )
        {
            /*
             * okay, the init task was not ready on time, now forcibly remove
             * it for the rtai's scheduler
             */
            rt_task_suspend(&cleanUpTask);
            rt_task_delete(&cleanUpTask);

            RTLOG_ERROR(modName,
                        "clean-up task did not report back on time!");

            status = RT_TOOLS_MODULE_CLEANUP_ERROR;

            goto Exit;
        }
    }
    else
    {
        RTLOG_ERROR(modName, "failed to spawn clean-up task");

        status = RT_TOOLS_MODULE_CLEANUP_ERROR;

        goto Exit;
    }

Level5CleanUp:
    /*
     * try to stop adjust time task only if it did manage to wake up successfully
     */
    if ( !IS_ERR(sysTimeAdjustTask) )
    {
        RTLOG_INFO(modName, "terminating system time adjusting task...");
        
        if ( kthread_stop(sysTimeAdjustTask) )
        {
            RTLOG_ERROR(modName, "time adjusting task stopped with error");  
        }
        else
        {
            RTLOG_INFO(modName, "system time adjusting task terminated");
        }
    }
    else
    {
        RTLOG_INFO(modName, "system time adjusting task not running");
    }

Level4CleanUp:
    /*
     * deregister used names
     */
    rt_drg_on_adr(&clk_tbx);
     
    /*
     * delete semaphore object
     */
    rt_sem_delete(&access_sem);
    rt_sem_delete(&fifo_sem);
    rt_sem_delete(&sem);
    rt_mbx_delete(&clk_tbx);

    RTLOG_INFO(modName, "sems and mbx deleted");

Level3CleanUp:
    /*
     * remove user land access.
     */
    reset_rt_fun_ext_index(teHandlerFun, TE_HANDLER_IDX);
    RTLOG_INFO(modName,
               "rtai extension removed (idx=%d)", TE_HANDLER_IDX);

Level2CleanUp:
    /*
     * if hardware interrupts available then disable them
     */
    if ( disableTE != 0 )
    {
        if ( disableTE() == 0 )
        {
            RTLOG_INFO(modName, "hardware TEs disabled");
        }
        else
        {
            RTLOG_INFO(modName, "failed to disable hardware TEs!"); 
        }
    }

Level1CleanUp:
    /*
     * destroy the fifo semaphore
     */
    RTLOG_INFO(modName,
               "command fifo semaphore destroyed (stat=%d).",
               rtf_sem_destroy(TE_HANDLER_CMD_FIFO));

    /*
     * close the command fifo. Note that the fifo would not be destroyed until
     * all users has closed the fifo, though.
     */
    RTLOG_INFO(modName,
               "command fifo closed (count=%d).",
               rtf_destroy(TE_HANDLER_CMD_FIFO));

Exit:
    return status;
}

static int __init teHandler_init(void)
{   
    int status;
    rtlogRecord_t logRecord;

    if ( (status = teHandler_main(RT_TOOLS_MODULE_STAGE_INIT))
	 == RT_TOOLS_MODULE_INIT_SUCCESS )
    {
	RTLOG_INFO(modName, "module initialized successfully");
    }
    else
    {
	RTLOG_ERROR(modName, "failed to initialize module");
    }

    return status;
}

static void __exit teHandler_exit(void)
{
    rtlogRecord_t logRecord;

    if ( teHandler_main(RT_TOOLS_MODULE_STAGE_EXIT)
	 == RT_TOOLS_MODULE_EXIT_SUCCESS )
    {
	RTLOG_INFO(modName, "module cleaned up successfully");
    }
    else
    {
	RTLOG_ERROR(modName, "failed to clean-up module");
    }
}

module_init(teHandler_init);
module_exit(teHandler_exit);

/*___oOo___*/
