#ifndef RT_DEV_DRVR_H
#define RT_DEV_DRVR_H
/* @(#) $Id$
 *
 * Copyright (C) 2004
 * Associated Universities, Inc. Washington DC, USA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * Correspondence concerning ALMA Software should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */
/** \mainpage Real-time %Device Driver Framework
 ** This file defines the functions for real-time device driver ('RT
 ** driver') framework which provides a standard device driver
 ** interface for real-time kernel modules. It uses a redirection
 ** mechanism to call standard function names (rtRead(), rtWrite(),
 ** and rtIoctl()) to access the underlying real-time device driver
 ** equivalent functions.
 **
 ** There are three points that this framework addresses:
 ** \li If we have a hardware device driver, e.g., \e tpmc901dev and a
 ** simulated version of of it, \e tpmc901sim. The problem is that
 ** both kernel modules export the functions \c tp_read(), \c
 ** tp_write(), and \c tp_ioctl(). This prevents both device drivers
 ** from being resident simultaneously due to the naming conflict.
 ** \li Another important advantage of this approach is to provide a
 ** clear, standard interface for all RT device drivers. It allows
 ** developers to quickly come up to speed on how to use a device
 ** driver.
 ** \li Finally, it provides a framework for the RT device driver
 ** author to provide good documenation on how to use his/her RT
 ** device driver.
 **
 ** To use this framework the RT driver must register itself with the
 ** real-time device driver framework in the \c init_module() function
 ** (with rtRegisterDriver()) and unregister the RT driver in \c
 ** cleanup_module() (with rtUnregisterDriver()). The RT driver must
 ** have a unique name (it is recommended that the kernel module and
 ** RT driver names are the same) and define 3 functions with unique
 ** names for read(), write(), and ioctl() for which the author must
 ** clearly define the function prototypes. The framework provides
 ** some error checking of the redirection process, but it is left up
 ** to RT driver author to define device-specific error codes which
 ** are 'passed up' to the calling user routine.
 **
 ** The user (in kernel space), must then open this RT driver using
 ** its device name (with rtOpen()), e.g., 'tpmc901drv' which returns
 ** a device ID. Subsequent calls to rtRead(), rtWrite() and rtIoctl()
 ** must use this device ID. Also the user must know the function
 ** prototypes of the underlying RT driver in order to pass the
 ** correct parameters. Finally, when the user is finished with the RT
 ** driver, s/he closes it using rtClose().
 **
 ** Detailed comments at rtDevDrv.h
 ** \sa see the test modules \e rtDevDrvTestModule and \e testRtDevDrv in the
 ** \b test directory for examples,
 ** \author Jim Pisano
 */

#include <rtai_types.h>

//! Typedef for pointers to functions for read() & write()
typedef int (*PF1)(void *, void *, size_t, RTIME);
//! typedef for pointers to function ioctl()
typedef int (*PF2)(int, void *);


/** This function opens the device driver. It returns a rtDeviceID which must
 ** be used for any subsequent rtXXX() function calls.
 ** @param deviceName This is the name of the device driver.
 ** @return Returns a positive number > 0 on success which is used by
 **    rtRead(), rtWrite(), rtIoctl(), rtClose() as the device number and
 **    a negative number on error:
 **    \li \c -#E_RTDD_BUSY means that this device was already opened by another and
 **    cannot be reopened.
 **    \li \c -#E_RTDD_DEVICE_NOT_FOUND is returned if the RT driver
 **     named deviceName is not loaded.
*/
    int rtOpen(const char *deviceName);

/** This function closes the RT device driver preventing further access to it.
 ** It is not meant to unload the driver! Once a device is closed, the rtDeviceID is invalid
 ** and use of it with calls to rtWrite(), rtRead(), rtIoctl() will return an
 ** error.
 ** @param rtDeviceID The device number returned from rtOpen()
 ** @return Returns \c #E_RTDD_SUCCESS on success, else a negative value for errors:
 **    \li \c -#E_RTDD_DEVICE_ID_INVALID  rtDeviceID is invalid, i.e., out-of-range
 **    \li \c -#E_RTDD_DEVICE_NOT_OPENED if the device has not been opened or was already
 **    closed
 */
    int rtClose(int rtDeviceID );

/** rtRead() reads numberOfBytes into a pre-allocated buffer bufferToRead.
 ** The use of a void * allows C-style casting.
 ** @param rtDeviceID The device number returned from rtOpen()
 ** @param genericInputData Pointer to driver's generic data. For example, this could
 **    be the CAN channel from which to read.
 ** @param bufferToRead Holds the data which is 'read' by the device driver.
 **    Essentially, this would replace tp_read() with the data read from the CAN
 **    channel returned through this buffer.
 ** @param  numberOfBytes The number of bytes expected to read.
 ** @param timeoutNS The number of nanoseconds to wait for the read to complete.
 **    A timeout of 0 means to wait (block) forever, although this is not recommended.
 ** @return  If > -1, the number of bytes actually read. If < 0, an error. This function
 **    returns the following errors:
 **    \li \c -#E_RTDD_DEVICE_ID_INVALID rtDeviceID is invalid, i.e., out-of-range
 **    \li \c -#E_RTDD_READ_FNC_INVALID if the underlying \c read() function has not been registered or the device was closed
 **    \li \c -#E_RTDD_DEVICE_NOT_OPENED if the device hasn't been opened or was closed<br>
 **    The RT driver implementation can return :
 **    \li \c -#E_RTDD_INVALID_ARG  for an invalid argument
 **    \li \c -#E_RTDD_TIMEOUT  if the underlying read() call times out
 **    \li \c -#E_RTDD_BUSY  if the underlying read call is still blocking from a previous call<br>
 ** or other negative errors which don't conflict with the E_RTDD_XXX values.
 **
 */
    int rtRead( int rtDeviceID, void *genericInputData, void *bufferToRead,
        size_t numberOfBytes, RTIME timeoutNS );

/** rtWrite() writes data in bufferToWrite. This, for example, would be the
 ** tp_write() equivalent.
 ** @aparam rtDeviceID The device number returned from rtOpen()
 ** @param genericInputData Pointer to driver's generic data. For example, this could
 **    be the CAN channel to which to write.
 ** @param bufferToWrite is a pointer to the buffer to write.
 ** @param numberOfBytes is the size of the buffer to write.
 ** @param timeoutNS The number of nanoseconds to wait for the read to complete.
 **    A timeout of 0 means to wait (block) forever, although this is not recommended.
 ** @return  If > -1, the number of bytes written. If < 0, an error. This framework
 **    returns the following errors:
 **    \li \c -#E_RTDD_DEVICE_ID_INVALID rtDeviceID is invalid, i.e., out-of-range
 **    \li \c -#E_RTDD_WRITE_FNC_INVALID if the underlying \c write() function was not defined in the rtRegister() call or  the device was closed
 **     \li \c -#E_RTDD_DEVICE_NOT_OPENED <br>
 ** The implementation can return:
 **    \li \c -#E_RTDD_INVALID_ARG for an invalid argument
 **    \li \c -#E_RTDD_TIMEOUT if the underlying write() call times out
 **    \li \c -#E_RTDD_BUSY if the underlying write call is still blocking from a previous call<br>
 ** or other negative errors which don't conflict with the E_RTDD_XXX values.
*/
    int rtWrite( int rtDeviceID, void *genericInputData, void *bufferToWrite,
         size_t numberOfBytes, RTIME timeoutNS);

/** This function sends configuration information to the driver. It allows a the
 ** RT driver to define a custom interface where specific data can be sent to or
 ** returned from the RT driver.
 ** @param rtDeviceID The device number returned from rtOpen()
 ** @param configData A void pointer to a buffer of data sent to and/or returned
 **    from the RT driver. This could be a pointer to a structure, int, etc.
 ** @param request is the code of the operation as defined by the RT driver.
 ** @return \c #E_RTDD_SUCCESS on success. If < 0, an error:
 **    \li \c -#E_RTDD_DEVICE_ID_INVALID rtDeviceID is invalid, i.e., out-of-range
 **    \li \c -#E_RTDD_IOCTL_FNC_INVALID if the underlying \c ioctl() function was not defined in the rtRegister() call or was closed
 **    \li \c -#E_RTDD_INVALID_ARG for invalid argument<br>
 ** or other negative errors which don't conflict with the E_RTDD_XXX values.
 */
    int rtIoctl(int rtDeviceID, int request, void *configData);

//! The maximum length of  the RT device driver name
#define MAX_DEVICE_DRIVER_NAME_LENGTH    50

/** \struct %Device driver table. This structure defines the internal table which
 ** holds all of the device driver information.
 */
struct rtDevDrvrTable_t
{
    //! the RT driver name (and recommended kernel module name) of maximum length #MAX_DEVICE_DRIVER_NAME_LENGTH
    char driverName[MAX_DEVICE_DRIVER_NAME_LENGTH+1];
    //! Pointer to the RT driver's read function
    PF1 rtReadFunctionPtr;
    //! Pointer to the RT driver's write function
    PF1 rtWriteFunctionPtr;
    //! Pointer to the RT driver's ioctl function
    PF2 rtIoctlFunctionPtr;
    //! The RT driver can be busy up to this duration in nanoseconds
    RTIME accessControlTimeoutNS;
    //! The RT driver open or closed status
    int status;
    //! The current number of devices which have opened the device
    int numRef;
    //! If 0 then the device can be opend only once, else it can be reopened
    unsigned char allowMultipleAccess;
};

/** Get an entry of the RT driver table. Note that it is valid to return an
 ** entry in the RT driver table which has not been registered. Its device name
 ** and function pointers would be NULL in this case.
 ** @param deviceEntry The device table entry. This is a \b 1-based value
 **    with a maximum value of 50.
 ** @param pDeviceDriverTableEntry A pointer to a preallocated instance of the
 **    struct rtDevDrvrTable_t.
 ** @return \c #E_RTDD_SUCCESS on success, otherwise if deviceEntry is out-of-range
 ** < 1 or > 50, returns \c -#E_RTDD_DEVICE_ID_INVALID
 */
int getDeviceDriverTableInfo( int deviceEntry,
                  struct rtDevDrvrTable_t *pDeviceDriverTableEntry );

/** Each device driver needs to register itself into a common, global data
 ** structure which links specific driver functions to the more generic rtRead(),
 ** rtWrite() and rtIoctl(). It also sets the device name
 ** which can then be used to request an RT driver. This registration must
 ** occur in the \c init_module() of the device driver.
 ** @param deviceName This is the name of the device driver. The device driver
 **    names are standardized (see #defines starting at #RT_DEV_DRV_NAME)
 ** @param readFunctionPtr Pointer to RT driver's read() function
 ** @param writeFunctionPtr Pointer to RT driver's write() function
 ** @param ioctlFunctionPtr Pointer to RT driver's ioctl() function
 ** @param accessControlTimeout is the maximum amount of time which a calling
 **    function will wait (block) to access the specific driver function. See the
 **    discussion section for details. accessControlTimeout = 0 means to wait
 **    forever to obtain access to the driver function (although this is not
 **    recommended).
 ** @return \c #E_RTDD_SUCCESS on success or negative value on error. The available
 **    error responses are:
 **    \li \c -#E_RTDD_DRIVER_NOT_FOUND if the RT driver named deviceName is not loaded.
 **    \li \c -#E_RTDD_DEVICE_NAME_SIZE if the device name is empty or larger then then maximum allowed size (see #MAX_DEVICE_DRIVER_NAME_LENGTH)
 **    \li \c -#E_RTDD_DEVICE_REGISTERED if the device is already registered. This avoids duplicate entries in the internal translation table.
 **    \li \c -#E_RTDD_NO_TABLE_SPACE if no more available slots in the device table exist
*/
int rtRegisterDriver( const char *deviceName,
              PF1 readFunctionPtr,
              PF1 writeFunctionPtr,
              PF2 ioctlFunctionPtr,
              RTIME accessControlTimeout,
              unsigned char allowMultipleAccess);

/** This function can be called to remove an RT driver. This should only be
 ** called in the device driver's \c cleanup_module().
 ** @param deviceName This is the name of the device driver.
 ** @return \c #E_RTDD_SUCCESS on success, else negative values on error.
 */
int rtUnregisterDriver(const char *deviceName);

/// Function call returns successfully
#define E_RTDD_SUCCESS 0
/// Invalid argument
#define E_RTDD_INVALID_ARG        1024
//! Timeout parameter exceeded
#define E_RTDD_TIMEOUT        1025
//! A function has been called multiple times and is still waiting on original call to complete or trying to re-open an already opened device.
#define    E_RTDD_BUSY        1026
//! %Device driver module not found for rtRegister() call
#define E_RTDD_DRIVER_NOT_FOUND     1027
//! No space in the table to register a new driver
#define E_RTDD_NO_TABLE_SPACE     1028
//! %Device name not found for rtOpen() or rtUnregister()
#define E_RTDD_DEVICE_NOT_FOUND     1029
//! %Device name too long (max. length 50 chars)
#define E_RTDD_DEVICE_NAME_SIZE     1030
//! %Device driver name already registered
#define E_RTDD_DEVICE_REGISTERED 1031
//! %Device ID is invalid ( < 1 or > 50)
#define E_RTDD_DEVICE_ID_INVALID 1032
//! rtRead() function pointer is invalid (0), i.e., it wasn't correctly registered or the RT device has been closed
#define E_RTDD_READ_FNC_INVALID     1033
//! rtWrite() function pointer is invalid (0), i.e., it wasn't correctly registered or the RT device has been closed
#define E_RTDD_WRITE_FNC_INVALID 1034
//! rtIoctl() function pointer is invalid (0), i.e., it wasn't correctly registered or the RT device has been closed
#define E_RTDD_IOCTL_FNC_INVALID 1035
//! rtClose() cannot close the RT device because it's not open. It may have been previously closed or never opened.
#define E_RTDD_DEVICE_NOT_OPENED 1036

//! The RT device driver framework name
#define RT_DEV_DRV_NAME            "rtDevDrvr"
//! The TPMC 901 CAN driver
#define TPMC901_RT_DEV_DRV_NAME        "tpmc901"
//! The AMBSim driver
#define AMBSIM_RT_DEV_DRV_NAME        "AMBSim"
//! The TPMC 901 CAN simulator driver used by Correlator
#define TPMC901_SIM_RT_DEV_DRV_NAME    "tpmc901sim"
//! The hpdi32 driver (this driver may not use the RT %Device Driver Framework)
#define HPDI32_RT_DEV_DRV_NAME        "hpdi32"
//! The TE Handler driver
#define TE_HANDLER_DEV_DRV_NAME         "teHandler"
//! The TE Scheduler driver
#define TE_SCHED_DEV_DRV_NAME           "teSched"
//! The RT %Device Driver Framework test driver
#define RT_DEV_DRV_TEST_NAME            "testRtDevDrv"

#endif
