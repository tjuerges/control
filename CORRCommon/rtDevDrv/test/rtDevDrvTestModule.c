/* @(#) $Id$
 *
 * Copyright (C) 2004
 * Associated Universities, Inc. Washington DC, USA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * Correspondence concerning ALMA Software should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */

/* This kernel module is a test modules whose functions rtDevDrvTester accesses.
** Test procedure:
** Load rtTools, rtDevDrv module
** Load testRtDevDrv
** Function prototypes for this test:
 ** rtDevDrvTestRead( int *, char[10], size_t, RTIME);
 ** rtDevDrvTestWrite( int *, char[10], size_t, RTIME);
 ** rtDevDrvTestIoctl( int, int*);
 */

#include <rtDevDrv.h>
#include <rtTools.h>

MODULE_AUTHOR("Jim Pisano");
MODULE_DESCRIPTION("rtDevDrvTest");
MODULE_LICENSE("GPL");

int debug = 1;

//------------------------------------------------------------------------------
int rtDevDrvTestRead( void *genericInputData, void *bufferToRead,
                      size_t numberOfBytes, RTIME timeoutNS )
{
    int *pInputData, inputData;
    long int t;

    pInputData = (int*)genericInputData;
    inputData = *pInputData;
    
    numberOfBytes = 9;
    memcpy(bufferToRead,"TestData",numberOfBytes);
    t = (long int) timeoutNS;
    rt_printk("rtDevDrvTestRead called: inputData: %d bufferToRead: %s size: %d timeout: %ld\n",
              inputData, (char *)bufferToRead, numberOfBytes, t);
    return numberOfBytes;
}

//------------------------------------------------------------------------------
int rtDevDrvTestWrite( void *genericInputData, void *bufferToWrite,
                       size_t numberOfBytes, RTIME timeoutNS )
{
    int *pInputData, inputData;
    long int t;
    
    pInputData = (int*)genericInputData;
    inputData = *pInputData;
    
    t = (long int) timeoutNS;
    rt_printk("rtDevDrvTestWrite called: inputData: %d bufferToRead: %s size: %d timeout: %ld\n",
              inputData, (char *)bufferToWrite, numberOfBytes, t);
    return numberOfBytes;
}

//------------------------------------------------------------------------------
int rtDevDrvTestIoctl( int request, void *configData )
{
    int *pInputData, configDataInt;
    pInputData = (int*)configData;
    configDataInt = *pInputData;
    
    if( (request == 18) && (configDataInt == 43) )
        return E_RTDD_SUCCESS;
    else
        return -1040;
}

//------------------------------------------------------------------------------
int init_module(void)
{

    int retVal;
    RTIME accessControlTimeout;
    accessControlTimeout = 0LL;
    retVal = rtRegisterDriver( RT_DEV_DRV_TEST_NAME, rtDevDrvTestRead,
                               rtDevDrvTestWrite, rtDevDrvTestIoctl,
                               accessControlTimeout, 0);

    rt_printk("rtRegisterDriver returned: %d\n",retVal);

    // Try re-registering same driver, should get an error
    retVal = rtRegisterDriver( RT_DEV_DRV_TEST_NAME, rtDevDrvTestRead,
                               rtDevDrvTestWrite, rtDevDrvTestIoctl,
                               accessControlTimeout, 0);
    
    // Test that we can't re-register device
    if( retVal != -E_RTDD_DEVICE_REGISTERED )
        rt_printk("Re-registered %s! Returns: %d\n", RT_DEV_DRV_TEST_NAME, retVal);

    rt_printk("%s loaded\n", RT_DEV_DRV_TEST_NAME);
    return 0;
}

//------------------------------------------------------------------------------
void cleanup_module(void)
{
    int retVal;
    retVal = rtUnregisterDriver( RT_DEV_DRV_TEST_NAME);
    rt_printk("rtUnregisterDriver returned %d\n",retVal);
    rt_printk("%s unloaded\n",RT_DEV_DRV_TEST_NAME);
}

