/* @(#) $Id$
 *
 * Copyright (C) 2004
 * Associated Universities, Inc. Washington DC, USA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * Correspondence concerning ALMA Software should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */

#include <rtDevDrv.h>
#include <rtTools.h>

#define moduleName "testRtDevDrv"

MODULE_AUTHOR("Jim Pisano");
MODULE_DESCRIPTION(moduleName);
MODULE_LICENSE("GPL");

int debug = 1;

/* This runs the various tests on the rtDevDrv module. It uses rtDevDrvTestModule
 * ("testRtDevDrv") which implements a read(), write(), & ioctl() that is a
 * registered driver and called via this set of tests. This test program tests
 * for valid read, write, & ioctl calls using know data values. It also creates
 * error conditions which return expected errors (see error constants in rtDevDrv.h).
 */


//------------------------------------------------------------------------------
void runTests(void)
{
    int rtDeviceID;
    int retVal, testSuccessful = 1;
    RTIME accessControlTimeout = 0L;
    int deviceIdx;
    struct rtDevDrvrTable_t deviceDriverTableEntry;
    int inputData = 10;
    char bufferData[10];
    size_t bufferSize = 10;
    int expectedSize = 9;
    
    int rtIoctlRequest;
    int rtIoctlConfigData;
    
    memset(bufferData,0,10);
    
    // Open the test driver
    rtDeviceID = rtOpen( RT_DEV_DRV_TEST_NAME);
    
    // If it's not the first entry, error.
    if( rtDeviceID != 1 )
    {
        rt_printk("rtOpen() returned rtDeviceID: %d expected 1\n",rtDeviceID);
        testSuccessful = 0;
    }

    rt_printk("device handle for device %s is: %d\n",
              RT_DEV_DRV_TEST_NAME, rtDeviceID);
    
    // Try reading the test device
    retVal = rtRead( rtDeviceID, &inputData, bufferData, bufferSize, accessControlTimeout);
    
    // Check that it returns 'TestData\0'
    if( memcmp(bufferData,"TestData",expectedSize) )
    {
        rt_printk("rtRead failed to copy response to bufferData. Got %s expected TestData\n",
                  bufferData);
        testSuccessful = 0;
    }
    // And the correct number of bytes
    if( retVal != expectedSize )
    {
        rt_printk("rtRead returns: %d\n", retVal);
        testSuccessful = 0;
    }

    // check reading unregistered deviceID
    retVal = rtRead( 2, &inputData, bufferData, bufferSize, accessControlTimeout);
    if( retVal != -E_RTDD_READ_FNC_INVALID )
    {
        rt_printk("rtRead read from unregistered device: %d\n", retVal);
        testSuccessful = 0;
    }
    
    // check reading from invalid device ID
    retVal = rtRead( 0, &inputData, bufferData, bufferSize, accessControlTimeout);
	if( retVal != -E_RTDD_DEVICE_ID_INVALID )
	{
		rt_printk("rtRead read from unregistered device: %d\n", retVal);
		testSuccessful = 0;
	}


	retVal = rtWrite( rtDeviceID, &inputData, bufferData, bufferSize, accessControlTimeout);
	if( retVal != bufferSize )
	{
		rt_printk("rtWrite returns: %d\n",retVal);
		testSuccessful = 0;
	}

	// check writing to unregistered deviceID
	retVal = rtWrite( 2, &inputData, bufferData, bufferSize, accessControlTimeout);
	if( retVal > -1 )
	{
		rt_printk("rtWrite write to unregistered device: %d\n", retVal);
		testSuccessful = 0;
	}
	// Write to an invalid device ID
	retVal = rtWrite( 0, &inputData, bufferData, bufferSize, accessControlTimeout);
	if( retVal != -E_RTDD_DEVICE_ID_INVALID )
	{
		rt_printk("rtWrite write to invalid deviceID(0): %d\n", retVal);
		testSuccessful = 0;
	}

	// Test rtIoctl().
	// Give the test driver's ioctl() correct test values
	rtIoctlRequest = 18;
	rtIoctlConfigData = 43;
	retVal = rtIoctl( rtDeviceID, rtIoctlRequest, &rtIoctlConfigData);
	if( retVal != E_RTDD_SUCCESS )
	{
		rt_printk("rtIoctl returns: %d for request: %d configData: %d\n",
			  retVal,rtIoctlRequest, rtIoctlConfigData);
		testSuccessful = 0;
	}

	// Now give it incorrect values
	rtIoctlRequest = 19;
	rtIoctlConfigData = 43;
	retVal = rtIoctl( rtDeviceID, rtIoctlRequest, &rtIoctlConfigData);
	if( retVal == E_RTDD_SUCCESS )
	{
		rt_printk("rtIoctl returns: %d for request: %d configData: %d\n",
			  retVal,rtIoctlRequest, rtIoctlConfigData);
		testSuccessful = 0;
	}

	// Try reopening the device driver
	retVal = rtOpen( RT_DEV_DRV_TEST_NAME);
	if( retVal !=  -E_RTDD_BUSY )
	{
		rt_printk("Invalid reopen of %s returns %d\n",RT_DEV_DRV_TEST_NAME,retVal);
		testSuccessful = 0;
	}

	// Test
	deviceIdx = 1;

	retVal = getDeviceDriverTableInfo( deviceIdx, &deviceDriverTableEntry);
	if( (retVal != E_RTDD_SUCCESS) || strcmp(deviceDriverTableEntry.driverName,RT_DEV_DRV_TEST_NAME) ||
		(deviceDriverTableEntry.rtReadFunctionPtr == 0) ||
		(deviceDriverTableEntry.rtWriteFunctionPtr == 0) ||
		(deviceDriverTableEntry.rtIoctlFunctionPtr == 0) )
	{
		rt_printk("Error getDeviceDriverTableInfo[%d]: returns %d\n",
			  deviceIdx,retVal);
		testSuccessful = 0;
	}
	// Now check that remaining 49 entries are empty
	for( deviceIdx = 2; deviceIdx <= 50; deviceIdx++ )
	{
		retVal = getDeviceDriverTableInfo( deviceIdx, &deviceDriverTableEntry);
		if( (retVal != E_RTDD_SUCCESS) || (deviceDriverTableEntry.driverName[0] != 0) ||
			(deviceDriverTableEntry.rtReadFunctionPtr != 0) ||
			(deviceDriverTableEntry.rtWriteFunctionPtr != 0) ||
			(deviceDriverTableEntry.rtIoctlFunctionPtr != 0) )
		{
			rt_printk("Error getDeviceDriverTableInfo[%d]: returns %d\n",
				  deviceIdx,retVal);
			testSuccessful = 0;
		}
	}

	// Now try to get an invalid entry
	retVal = getDeviceDriverTableInfo( 0, &deviceDriverTableEntry);
	if( retVal != -E_RTDD_DEVICE_ID_INVALID )
	{
		rt_printk("Obtained getDeviceDriverTableInfo for invalid deviceID (0): %d\n",retVal);
		testSuccessful = 0;
	}

	// try closing an invalid deviceID
	retVal = rtClose(0);
	if( retVal != -E_RTDD_DEVICE_ID_INVALID )
	{
		rt_printk("Error closing invalid deviceID (0): %d\n",retVal);
		testSuccessful = 0;
	}

	// try closing unopened devicID
	retVal = rtClose(2);
	if( retVal != -E_RTDD_DEVICE_NOT_OPENED )
	{
		rt_printk("Error closing unregistered deviceID (2): %d\n",retVal);
		testSuccessful = 0;
	}

	retVal = rtClose( rtDeviceID );
	if( retVal != 0 )
	{
		rt_printk("Error closing deviceID(%d): %d\n",rtDeviceID, retVal);
		testSuccessful = 0;
	}

	rt_printk("Closing device handle returns: %d. End of %s\n",
		  retVal, moduleName);

	// now that device is closed, try accessing functions
	retVal = rtRead( rtDeviceID, &inputData, bufferData, bufferSize, accessControlTimeout);
	if( retVal != -E_RTDD_DEVICE_NOT_OPENED )
	{
		rt_printk("rtRead on closed device(2) returns: %d\n", retVal);
		testSuccessful = 0;
	}

	if( testSuccessful == 0 )
	{
		rt_printk("Some tests FAILED\n");
	}
	else
	{
		rt_printk("All tests SUCCESSFUL\n");
	}

}
//------------------------------------------------------------------------------
int init_module(void)
{
	runTests();
	return 0;
}

//------------------------------------------------------------------------------
void cleanup_module(void)
{
	rt_printk("Unloading %s",moduleName);
}

