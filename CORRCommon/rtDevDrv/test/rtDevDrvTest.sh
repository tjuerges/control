#!/bin/sh
# $Id$
#
# look for success msg in /var/log/messages
dmesg | tail -1 | grep  "All tests SUCCESSFUL"
# look for successful unload of all kernel modules
dmesg | tail -1 | grep "RTAI\[hal\]\: unmounted\."


