#! /bin/sh
#
# "@(#) $Id$"
#

# This is needed because, by default, CVS checks this file out so that
# others cannot read it. However config/rtDevDrvTest.lkm needs to be
# read by root (the unloadLkmModule executable is setuid root) and on
# an NFS filesystem exported with the with root_squash option root has
# no special privileges
chmod o+r ../config/rtDevDrvTest.lkm

exit 0
