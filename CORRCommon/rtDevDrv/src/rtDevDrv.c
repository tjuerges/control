/* @(#) $Id$
 *
 * Copyright (C) 2004
 * Associated Universities, Inc. Washington DC, USA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * Correspondence concerning ALMA Software should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */
#include <linux/module.h>
#include <rtai_sched.h>
#include <rtTools.h>
#include <rtLog.h>
#include "rtDevDrv.h"

#define modName "rtDevDrv"

MODULE_AUTHOR("Jim Pisano <jpisano@nrao.edu>");
MODULE_DESCRIPTION("Real time device driver framework which provides four basic"
    "functions: open, close, read and write.");
MODULE_LICENSE("GPL");

/* This is a static lookup table which has the read, write, & ioctl() function pointers for
** each registered RTAI device driver.
*/

#define DEV_DRV_TABLE_SIZE	50
static struct rtDevDrvrTable_t _rtDevDriverTable[DEV_DRV_TABLE_SIZE];

// Define status of a table entry
#define STATUS_CLOSED		0
#define STATUS_OPEN		1


//------------------------------------------------------------------------------
int rtRegisterDriver( const char *deviceName,
			  PF1 readFunctionPtr,
			  PF1 writeFunctionPtr,
			  PF2 ioctlFunctionPtr,
			  RTIME accessControlTimeoutNS,
			  unsigned char allowMultipleAccess)
{
	int i, emptySlotFound = -1;
	// first ensure that kernel module is loaded
	//void *rtToolsLookupSymbol(const char *modname, const char *symname);
	// if( module(deviceName) < 0 )
	//     return -E_RTDD_DRIVER_NOT_FOUND;

	// And that kernel & device names are valid
	if( (strlen(deviceName) > MAX_DEVICE_DRIVER_NAME_LENGTH) ||
	(strlen(deviceName) == 0) )
	return -E_RTDD_DEVICE_NAME_SIZE;

	// now search for duplicates & for an empty slot
	for( i = 0; i < DEV_DRV_TABLE_SIZE; i++ )
	{
	if( !strcmp(deviceName,_rtDevDriverTable[i].driverName) )
		return -E_RTDD_DEVICE_REGISTERED;
	if( _rtDevDriverTable[i].driverName[0] == 0 )
	{
		emptySlotFound = i;
		break;
	}
	}
	// Return error if no space in table
	if( emptySlotFound == -1 )
	return -E_RTDD_NO_TABLE_SPACE;

	// emptySlotFound is the index into the _rtDevDriverTable into which goes
	// the new device driver
	strcpy(_rtDevDriverTable[emptySlotFound].driverName, deviceName);
	_rtDevDriverTable[emptySlotFound].rtReadFunctionPtr = readFunctionPtr;
	_rtDevDriverTable[emptySlotFound].rtWriteFunctionPtr = writeFunctionPtr;
	_rtDevDriverTable[emptySlotFound].rtIoctlFunctionPtr = ioctlFunctionPtr;
	_rtDevDriverTable[emptySlotFound].accessControlTimeoutNS = accessControlTimeoutNS;
	_rtDevDriverTable[emptySlotFound].status = STATUS_CLOSED;
	_rtDevDriverTable[emptySlotFound].numRef = 0;
	_rtDevDriverTable[emptySlotFound].allowMultipleAccess = allowMultipleAccess;

	return E_RTDD_SUCCESS;
}

//------------------------------------------------------------------------------
int findDeviceInTable( const char *deviceName )
{
	int i;
	for( i = 0; i < DEV_DRV_TABLE_SIZE; i++ )
	{
	if( !strcmp(deviceName,_rtDevDriverTable[i].driverName) )
		return i;
	}
	return -E_RTDD_DEVICE_NOT_FOUND;
}

//------------------------------------------------------------------------------
int rtOpen(const char *deviceName)
{
	int deviceIdx = findDeviceInTable(deviceName);

	// Device not found
	if( deviceIdx < 0 )
	return deviceIdx;

	if( _rtDevDriverTable[deviceIdx].status == STATUS_OPEN &&
	_rtDevDriverTable[deviceIdx].allowMultipleAccess == 0)
		return -E_RTDD_BUSY;

	_rtDevDriverTable[deviceIdx].status = STATUS_OPEN;
	_rtDevDriverTable[deviceIdx].numRef++;

	// return index + 1 so they are always positive.
	return (deviceIdx+1);
}

//------------------------------------------------------------------------------
int rtClose(int rtDeviceID )
{
	if( (rtDeviceID < 1) || (rtDeviceID > DEV_DRV_TABLE_SIZE) )
	return -E_RTDD_DEVICE_ID_INVALID;

	// adjust rtDeviceID to table index
	rtDeviceID--;

	// User has tried to close an unopened device return error
	if( _rtDevDriverTable[rtDeviceID].status != STATUS_OPEN )
		return -E_RTDD_DEVICE_NOT_OPENED;

	// Decrease the reference count
	_rtDevDriverTable[rtDeviceID].numRef--;

	// If that is the last reference close the device
	if ( _rtDevDriverTable[rtDeviceID].numRef == 0 ) {
	  // set device name  to NULL
	  _rtDevDriverTable[rtDeviceID].status = STATUS_CLOSED;
	}

	return E_RTDD_SUCCESS;
}

//------------------------------------------------------------------------------
int rtRead( int rtDeviceID, void *genericData, void *bufferToRead,
		size_t numberOfBytes, RTIME timeoutNS )
{
	if( (rtDeviceID < 1) || (rtDeviceID > DEV_DRV_TABLE_SIZE) )
	return -E_RTDD_DEVICE_ID_INVALID;

	rtDeviceID--;

	if( _rtDevDriverTable[rtDeviceID].rtReadFunctionPtr == 0 )
	return -E_RTDD_READ_FNC_INVALID;

	if( _rtDevDriverTable[rtDeviceID].status == STATUS_CLOSED )
	return -E_RTDD_DEVICE_NOT_OPENED;

	return _rtDevDriverTable[rtDeviceID].rtReadFunctionPtr(genericData, bufferToRead,
														   numberOfBytes, timeoutNS);
}

//------------------------------------------------------------------------------
int rtWrite( int rtDeviceID, void *genericData, void *bufferToWrite,
		size_t numberOfBytes, RTIME timeoutNS )
{
	if( (rtDeviceID < 1) || (rtDeviceID > DEV_DRV_TABLE_SIZE) )
	return -E_RTDD_DEVICE_ID_INVALID;

	rtDeviceID--;

	if( _rtDevDriverTable[rtDeviceID].rtWriteFunctionPtr == 0 )
	return -E_RTDD_WRITE_FNC_INVALID;

	if( _rtDevDriverTable[rtDeviceID].status == STATUS_CLOSED )
	return -E_RTDD_DEVICE_NOT_OPENED;

	return _rtDevDriverTable[rtDeviceID].rtWriteFunctionPtr(genericData, bufferToWrite,
								 numberOfBytes, timeoutNS);
}

//------------------------------------------------------------------------------
int rtIoctl( int rtDeviceID, int request, void *configData)
{
	if( (rtDeviceID < 1) || (rtDeviceID > DEV_DRV_TABLE_SIZE) )
	return -E_RTDD_DEVICE_ID_INVALID;

	rtDeviceID--;

	if( _rtDevDriverTable[rtDeviceID].rtIoctlFunctionPtr == 0 )
	return -E_RTDD_IOCTL_FNC_INVALID;

	if( _rtDevDriverTable[rtDeviceID].status == STATUS_CLOSED )
	return -E_RTDD_DEVICE_NOT_OPENED;

	return _rtDevDriverTable[rtDeviceID].rtIoctlFunctionPtr(request, configData);
}

//------------------------------------------------------------------------------
int rtUnregisterDriver(const char *deviceName)
{
	int deviceIdx = findDeviceInTable(deviceName);

	// Device not found
	if( deviceIdx < 0 )
	return deviceIdx;

	// unregister by setting all entries to 0 & making it available
	_rtDevDriverTable[deviceIdx].driverName[0] = 0;
	_rtDevDriverTable[deviceIdx].rtReadFunctionPtr = 0;
	_rtDevDriverTable[deviceIdx].rtWriteFunctionPtr = 0;
	_rtDevDriverTable[deviceIdx].rtIoctlFunctionPtr = 0;
	_rtDevDriverTable[deviceIdx].accessControlTimeoutNS = 0;

	return E_RTDD_SUCCESS;
}

//------------------------------------------------------------------------------
int getDeviceDriverTableInfo( int deviceIdx, struct rtDevDrvrTable_t *pDeviceDriverTableEntry )
{
	if( (deviceIdx < 1) || (deviceIdx > DEV_DRV_TABLE_SIZE) )
		return -E_RTDD_DEVICE_ID_INVALID;

	deviceIdx--;
	memcpy(pDeviceDriverTableEntry->driverName, _rtDevDriverTable[deviceIdx].driverName,
		   MAX_DEVICE_DRIVER_NAME_LENGTH+1);
	pDeviceDriverTableEntry->rtReadFunctionPtr =  _rtDevDriverTable[deviceIdx].rtReadFunctionPtr;
	pDeviceDriverTableEntry->rtWriteFunctionPtr = _rtDevDriverTable[deviceIdx].rtWriteFunctionPtr;
	pDeviceDriverTableEntry->rtIoctlFunctionPtr = _rtDevDriverTable[deviceIdx].rtIoctlFunctionPtr;
	pDeviceDriverTableEntry->accessControlTimeoutNS = _rtDevDriverTable[deviceIdx].accessControlTimeoutNS;
	return E_RTDD_SUCCESS;
}

//------------------------------------------------------------------------------
static int __init init_rtDevDrv(void)
{
    rtlogRecord_t logRecord;

    int i;
    RTLOG_INFO(modName, "$Id$");
    for( i = 0; i < DEV_DRV_TABLE_SIZE; i++ )
    {
        _rtDevDriverTable[i].driverName[0] = 0;
        _rtDevDriverTable[i].rtReadFunctionPtr = 0;
        _rtDevDriverTable[i].rtWriteFunctionPtr = 0;
        _rtDevDriverTable[i].rtIoctlFunctionPtr = 0;
        _rtDevDriverTable[i].accessControlTimeoutNS = 0;
		_rtDevDriverTable[i].status = STATUS_CLOSED;
	}

    RTLOG_INFO(modName, "rtDevDrvr loaded");
	return E_RTDD_SUCCESS;
}

//------------------------------------------------------------------------------
static void __exit exit_rtDevDrv(void)
{
	rtlogRecord_t logRecord;

	RTLOG_INFO(modName, "rdDevDrvr unloaded");
}

EXPORT_SYMBOL(getDeviceDriverTableInfo);
EXPORT_SYMBOL(rtRegisterDriver);
EXPORT_SYMBOL(rtUnregisterDriver);
EXPORT_SYMBOL(rtOpen);
EXPORT_SYMBOL(rtClose);
EXPORT_SYMBOL(rtIoctl);
EXPORT_SYMBOL(rtRead);
EXPORT_SYMBOL(rtWrite);

module_init(init_rtDevDrv);
module_exit(exit_rtDevDrv);
