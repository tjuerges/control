/*
 *   SMI workaround for x86.
 *
 *   Cut/Pasted from Vitor Angelo "smi" module.
 *   Adapted by Gilles Chanteperdrix <gilles.chanteperdrix@laposte.net>.
 *   Further adaptation by Alberto Sechi <sechi@aero.polimi.it>.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, Inc., 675 Mass Ave, Cambridge MA 02139,
 *   USA; either version 2 of the License, or (at your option) any later
 *   version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Taken from RTAI 3.7.1.
 * Bug fixes:
 * - Do not call pci_dev_put too often.
 *
 * !!! This version is for ALMA ABM VMIVME-7807 single board computers only. !!!
 *
 * $Id$
 *
 */


#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/version.h>
#include <linux/pci.h>
#include <linux/pci_ids.h>
#include <linux/reboot.h>
#include <asm/io.h>
#include <linux/types.h>

/* Set these as you need. */
#define CONFIG_HW_SMI_ALL 1
#define CONFIG_HW_SMI_INTEL_USB2 0
#define CONFIG_HW_SMI_LEGACY_USB2 0
#define CONFIG_HW_SMI_PERIODIC 0
#define CONFIG_HW_SMI_TCO 0
#define CONFIG_HW_SMI_MC 0
#define CONFIG_HW_SMI_APMC 0
#define CONFIG_HW_SMI_LEGACY_USB 0
#define CONFIG_HW_SMI_BIOS 0


/* In case the Tundra Universe PCI-VME bridge is undefined in the PCI Ids. */
#ifndef PCI_VENDOR_ID_TUNDRA
#define PCI_VENDOR_ID_TUNDRA 0x10e3
#endif

#ifndef PCI_DEVICE_ID_TUNDRA_CA91C042
#define PCI_DEVICE_ID_TUNDRA_CA91C042 0x0000
#endif



/* SMI_EN register: ICH[0](16 bits), ICH[2-5](32 bits) */
#define INTEL_USB2_EN_BIT (0x01 << 18) /* ICH4, ... */
#define LEGACY_USB2_EN_BIT (0x01 << 17) /* ICH4, ... */
#define PERIODIC_EN_BIT (0x01 << 14) /* called 1MIN_ in ICH0 */
#define TCO_EN_BIT (0x01 << 13)
#define MCSMI_EN_BIT (0x01 << 11)
#define SWSMI_TMR_EN_BIT (0x01 << 6)
#define APMC_EN_BIT (0x01 << 5)
#define SLP_EN_BIT (0x01 << 4)
#define LEGACY_USB_EN_BIT (0x01 << 3)
#define BIOS_EN_BIT (0x01 << 2)
#define GBL_SMI_EN_BIT (0x01 << 0)  /* This is reset by a PCI reset event! */


/* Module parameters. */
unsigned int smi_bit_mask = 0x00000000UL
#if CONFIG_HW_SMI_ALL
    | GBL_SMI_EN_BIT
#else
#if CONFIG_HW_SMI_INTEL_USB2
    | INTEL_USB2_EN_BIT
#endif
#if CONFIG_HW_SMI_LEGACY_USB2
    | LEGACY_USB2_EN_BIT
#endif
#if CONFIG_HW_SMI_PERIODIC
    | PERIODIC_EN_BIT
#endif
#if CONFIG_HW_SMI_TCO
    | TCO_EN_BIT
#endif
#if CONFIG_HW_SMI_MC
    | MCSMI_EN_BIT
#endif
#if CONFIG_HW_SMI_APMC
    | APMC_EN_BIT
#endif
#if CONFIG_HW_SMI_LEGACY_USB
    | LEGACY_USB_EN_BIT
#endif
#if CONFIG_HW_SMI_BIOS
    | BIOS_EN_BIT
#endif
#endif
;


module_param(smi_bit_mask, uint, S_IRUGO);

int smiReset = 1;
module_param(smiReset, int, S_IRUGO);


static struct pci_dev* smi_dev = NULL;

/* The PCI device has to provide this device function number.  Only then it is
 * the SM-bus and PM-controller that we are looking for.
 * Device 31, function 0 is used here because according to the document
 * 252516.pdf ("Intel 82801EB ICH5 / 82801ER ICH5R Datasheet", in the doc
 * directory) the
 * "Bus 0:Device 31:Function 0 PCI to LPC Bridge" (= 0xf8U)
 * and "The PCI to LPC bridge contains registers that control LPC,
 * Power Management, System Management, GPIO, processor Interface, RTC,
 * Interrupts, Timers, DMA."
 */
const uint8_t DEVFN = PCI_DEVFN(31, 0);


/* PMBASE-ACPI Base Address Register
 * (LPC I/F-D31:F0)
 * 31:16    Reserved
 * 15:7     Base Address - R/W.  This field provides 128 bytes of I/O space for
 *          ACPI, GPIO, and TCO logic.  This is placed on a 128-byte boundary.
 * 6:1      Reserved
 * 0        Resource Type Indicator (RTE) - RO.  Hardwired to 1 to indicate I/O
 *          space.
 */
static const uint8_t PMBASE_OFFSET = 0x40U;
static uint16_t PMBASE = 0x0000UL;
/*static const uint8_t PMBASE_B0 = 0x40U;
static const uint8_t PMBASE_B1 = 0x41U;
*/
/* Offsets for the Power Management I/O registers. The offset is relative to
 * PMBASE. */
static const uint8_t SMI_EN_OFFSET = 0x30U;
static const uint8_t SMI_STS_OFFSET = 0x34U;
static const uint8_t MON_SMI_OFFSET = 0x40U;

/* Absolute addresses for the Power management I/O registers in the PCI device
 * memory.  These are not real memory addresses! */
static uint16_t SMI_EN = 0x0000UL;
static uint16_t SMI_STS = 0x0000UL;
static uint16_t MON_SMI = 0x0000UL;

static uint32_t smi_saved_bits = 0U;


static struct pci_device_id __devinitdata tundra[] =
{
    {
        PCI_DEVICE(PCI_VENDOR_ID_TUNDRA, PCI_DEVICE_ID_TUNDRA_CA91C042)
    },
    {
        0
    }
};

static struct pci_device_id __devinitdata smbus[] =
{
    {
        PCI_DEVICE(PCI_VENDOR_ID_INTEL, PCI_DEVICE_ID_INTEL_ESB_1)
    },
    {
        0
    }
};



static inline void smi_set_bits(uint32_t bits_to_set, uint16_t port_address)
{
    outl(inl(port_address) | (bits_to_set), port_address);
}

static inline void smi_mask_bits(uint32_t bit_mask, uint16_t port_address)
{
    outl(inl(port_address) & ~(bit_mask), (port_address));
}

static void __devinit calculate_port_addresses(struct pci_dev* dev)
{
    uint32_t PMBASE_tmp = 0x0000U;
    pci_read_config_dword(dev, PMBASE_OFFSET, &PMBASE_tmp);

    /* The PMBASE address is in bits [7; 15]. */
    PMBASE = (PMBASE_tmp >> 7) & 0x03ffU;
    SMI_EN = PMBASE + SMI_EN_OFFSET;
    SMI_STS = PMBASE + SMI_STS_OFFSET;
    MON_SMI = PMBASE + MON_SMI_OFFSET;

    printk("PMBASE address calculation done:  PMBASE = 0x%0x, SMI_EN = 0x%0x, "
        "MON_SMI = 0x%0x.\n", SMI_EN, SMI_STS, MON_SMI);
    return;
}

static void restore_smi_settings(void)
{
    if(SMI_EN != 0U)
    {
        smi_set_bits(smi_saved_bits, SMI_EN);
        printk("SMI enabled.  Restored setting = 0x%0x.\n",
            (unsigned int)smi_saved_bits);
    }
}

static void __devinit disable_smi(void)
{
    if(SMI_EN != 0U)
    {
        smi_saved_bits = inl(SMI_EN) & smi_bit_mask;
        smi_mask_bits(smi_bit_mask, SMI_EN);
        printk("SMI disabled.  Saved SMI setting = 0x%0x, SMI bit mask = "
            "0x%0x.\n", (unsigned int)smi_saved_bits,
            (unsigned int)smi_bit_mask);
    }
}


static int smi_notify_reboot(struct notifier_block* nb,
    unsigned long event, void* p)
{
    switch(event)
    {
        case SYS_DOWN:
        case SYS_HALT:
        case SYS_POWER_OFF:
        {
            if(smi_dev != NULL)
            {
                struct pci_dev* dev = smi_dev;
                smi_dev = NULL;

                restore_smi_settings();
                pci_dev_put(dev);
            }
        }
        default:
        {
        }
    }

    return NOTIFY_DONE;
}

static struct notifier_block smi_reboot_notifier =
{
    .notifier_call = &smi_notify_reboot,
    .next = NULL,
    .priority = 0
};


static int __devinit smi_controller_initialisation(void)
{
    /* Check if it is an AMB which contains the Tundra Universe
     * VME2PCI bridge.
     */
    if(pci_dev_present(tundra) != 1)
    {
        printk("Could not find the Tundra Universe chipset.  This "
            "computer is not an ABM.\n");
        return -ENODEV;
    }

    if(pci_dev_present(smbus) != 1)
    {
        printk("Could not find the Intel 6300ESB SMBus Controller.  This "
            "computer is not an ABM.\n");
        return -ENODEV;
    }

    /* Now get the real device structure.  This increments the reference counter
     * in the kernel for this device. */
    smi_dev = pci_get_device(PCI_VENDOR_ID_INTEL, PCI_DEVICE_ID_INTEL_ESB_1,
        NULL);

    /* Not found?  Then it is definitely not an ABM. */
    if(smi_dev == NULL)
    {
        printk("Could not find an SM-bus controller, this is not an ABM.\n");
        return -ENODEV;
    }
    else
    {
        /* The SM-bus controller has to be on PCI bus #0. */
        if(smi_dev->bus->number != 0)
        {
            pci_dev_put(smi_dev);
            smi_dev = NULL;

            printk("PCI bus number (0x%0x) is not 0, this is not an ABM.\n",
                smi_dev->bus->number);
            return -ENODEV;
        }
        /* It has to be a real SM-bus controller... */
        else if(smi_dev->devfn != DEVFN)
        {
            pci_dev_put(smi_dev);
            smi_dev = NULL;

            printk("The PCI device file number (0x%x) for the SM-bus "
                "controller does not match 0x%x.  This is not an ABM.\n",
                smi_dev->devfn, DEVFN);
            return -ENODEV;
        }
        /* It is the Intel 6300ESB SMBus Controller. */
        else
        {
            printk("This computer is an ABM.  Disabling SMI.\n");

            calculate_port_addresses(smi_dev);
            disable_smi();

            register_reboot_notifier(&smi_reboot_notifier);

            return 0;
        }
    }

    return -ENODEV;
}

static void __devexit smi_controller_exit(void)
{
    if(smi_dev != NULL)
    {
        struct pci_dev* dev = smi_dev;
        smi_dev = NULL;

        unregister_reboot_notifier(&smi_reboot_notifier);

        if(smiReset != 0)
        {

            restore_smi_settings();
            printk("SMI configuration has been reset, mask used = 0x%0x.\n",
                smi_saved_bits);
        }

        pci_dev_put(dev);
    }
}


/************************************************************************/

static int __init init_smi_controller(void)
{
    int retval = -ENXIO;

    printk("smi_controller:  $Id$\n");

    if((retval = smi_controller_initialisation()) == 0)
    {
        printk("SMI controller started.\n");
    }
    else if(retval == -ENODEV)
    {
        printk("Not disabling the SMI, but the SMI controller has been "
            "started.\n");
        retval = 0;
    }
    else
    {
        printk("SMI controller not started due to an error!\n");
    }

    return retval;
}

static void __exit cleanup_smi_controller(void)
{

    smi_controller_exit();
    printk("SMI controller stopped.\n");
    return;
}

module_init(init_smi_controller);
module_exit(cleanup_smi_controller);
MODULE_LICENSE("GPL");
