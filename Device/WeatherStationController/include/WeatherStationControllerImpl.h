// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

#ifndef WEATHERSTATIONCONTROLLERIMPL_H
#define WEATHERSTATIONCONTROLLERIMPL_H

// Base class(es)
#include <hardwareControllerImpl.h>

//CORBA servant header
#include <WeatherStationControllerS.h>

//includes for data members
#include <acsComponentSmartPtr.h> // for maci::SmartPtr

#include <WeatherPriorityList.h>

#include <CurrentWeatherC.h>

// Forward declarations for classes that this component uses
namespace maci {
    class ContainerServices;
}

namespace Control {
    class WeatherStation;
}

namespace Control {
    class WeatherStationControllerImpl:
        public virtual POA_Control::WeatherStationController,
        public Control::HardwareControllerImpl
    {
        public:
        // ========  Constructor & Destructor Interface =================

        /// This constructor connects to the notification channel. The
        /// constructor for any ACS C++ component must have this signature.
        WeatherStationControllerImpl(const ACE_CString& name,
            maci::ContainerServices* cs);

        /// The destructor disconnects from the notification channel. It must
        /// be virtual because this class contains virtual functions.
        virtual ~WeatherStationControllerImpl();

        // ====== Control::HardwareController Interface =================

        /// This function creates all the individual weather stations that are
        /// the children of this controller. Normally this will start these
        /// components. Do not assume all these components will run in the same
        /// container or even on the same computer.
        /// \exception ControlDeviceExceptions::IllegalConfigurationEx
        virtual void createSubdevices(
            const Control::DeviceConfig& configuration,
            const char* parentName);

        // ======== CORBA CurrentWeather interface ======================

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual Control::CurrentWeather::Pressure
        getPressureAtPad(const char* padName);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual Control::CurrentWeather::Pressure
        getPressure(const char* wsName);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual Control::CurrentWeather::WindSpeed
        getWindSpeedAtPad(const char* padName);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual Control::CurrentWeather::WindSpeed
        getWindSpeed(const char* wsName);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual Control::CurrentWeather::WindDirection
        getWindDirectionAtPad(const char* padName);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual Control::CurrentWeather::WindDirection
        getWindDirection(const char* wsName);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual Control::CurrentWeather::Temperature
        getTemperatureAtPad(const char* padName);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual Control::CurrentWeather::Temperature
        getTemperature(const char* wsName);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual Control::CurrentWeather::DewPoint
        getDewPointAtPad(const char* padName);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual Control::CurrentWeather::DewPoint
        getDewPoint(const char* wsName);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual Control::CurrentWeather::Humidity
        getHumidityAtPad(const char* padName);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual Control::CurrentWeather::Humidity
        getHumidity(const char* wsName);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual Control::CurrentWeather::Humidity
        getCalculatedHumidity(const char* wsName);

	/// See the IDL file for a description of this function.
 	virtual Control::CurrentWeather::LocationSeq*
        weatherStationLocations();

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
 	virtual CORBA::Boolean isEnabled(const char* name);

        /// See the IDL file for a description of this function.
 	virtual CORBA::Boolean isPressureSimulated();

        /// See the IDL file for a description of this function.
 	virtual CORBA::Boolean isWindSpeedSimulated();

        /// See the IDL file for a description of this function.
 	virtual CORBA::Boolean isWindDirectionSimulated();

        /// See the IDL file for a description of this function.
 	virtual CORBA::Boolean isTemperatureSimulated();

        /// See the IDL file for a description of this function.
 	virtual CORBA::Boolean isDewPointSimulated();

        /// See the IDL file for a description of this function.
 	virtual CORBA::Boolean isHumiditySimulated();

        // ======== CORBA WeatherStationController interface ===========

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::HardwareErrorEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual CORBA::Boolean enableWeatherStation(const char* name);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::HardwareErrorEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual CORBA::Boolean disableWeatherStation(const char* name);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual void enablePressureSimulation(CORBA::Double simPressure);

        /// See the IDL file for a description of this function.
        virtual void disablePressureSimulation();

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual void enableWindSpeedSimulation(CORBA::Double simSpeed);

        /// See the IDL file for a description of this function.
        virtual void disableWindSpeedSimulation();

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual void enableWindDirectionSimulation(CORBA::Double simDirection);

        /// See the IDL file for a description of this function.
        virtual void disableWindDirectionSimulation();

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual void enableTemperatureSimulation(CORBA::Double simTemp);

        /// See the IDL file for a description of this function.
        virtual void disableTemperatureSimulation();

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual void enableDewPointSimulation(CORBA::Double simDewPoint);

        /// See the IDL file for a description of this function.
        virtual void disableDewPointSimulation();

        /// See the IDL file for a description of this function.
        virtual char* getWeatherStationAssociatedAtPad(const char* padName);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual Control::CurrentWeather::Pressure
        getPressureAtAntenna(const char* antennaName);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual Control::CurrentWeather::WindSpeed
        getWindSpeedAtAntenna(const char* antennaName);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual Control::CurrentWeather::WindDirection
        getWindDirectionAtAntenna(const char* antennaName);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual Control::CurrentWeather::Temperature
        getTemperatureAtAntenna(const char* antennaName);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual Control::CurrentWeather::DewPoint
        getDewPointAtAntenna(const char* antennaName);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual Control::CurrentWeather::Humidity
        getHumidityAtAntenna(const char* antennaName);

        /// Calculate the priority list of weather stations for each pad
	list<PriorityList*> CalculatePadInfo(Location padLoc);
	void GeneratePad2WeatherStationMap();

	/// Overloading controllerOperational method
 	virtual void controllerOperational();

	///
	virtual void reCalculatePriorityList();

    private:
        /// Throw an exception if the supplied weather station name is not
        /// managed by this weather station controller. Matching is case
        /// insensitive. If the weather station is managed the weather station
        /// name is returned in upper case.
        ///  \exception ControlExceptions::IllegalParameterErrorEx
        std::string throwIfUnknownWS(const std::string& wsName);

        /// Returns a default weatherStation for a pad with unknown coordinates
        /// It returns 'WSOSF' at OSF pads and 'WSTB1' for pads at the AOS
	string getDefaultWeatherStation(const string& padName);

        /// Throw an exception if the supplied pad name is not known. Matching
        /// is case insensitive. If the pad name known the pad name is returned
        /// in upper case.
        ///  \exception ControlExceptions::IllegalParameterErrorEx
        std::string throwIfUnknownPad(const std::string& padName);

	/// Return the name of the WeatherStation associated to a specific pad
   	std::string padNameToWeatherStation(string padName);

        /// Throw an exception if there is no pad to the supplied antenn name. Matching
        /// is case insensitive.
        ///  \exception ControlExceptions::IllegalParameterErrorEx
       const char *antennaToPadName(string antennaName);

        /// Returns true if the connection to the OSF weather station web
        /// service can be established.
        bool isValid();

        static double ew(const double& temp, const double& ef);

        /// Simulation state variables
        bool isPressureSimulated_m;
        double simulatedPressure_m;
        bool isWindSpeedSimulated_m;
        double simulatedWindSpeed_m;
        bool isWindDirectionSimulated_m;
        double simulatedWindDirection_m;
        bool isTemperatureSimulated_m;
        double simulatedTemperature_m;
        bool isDewPointSimulated_m;
        double simulatedDewPoint_m;
        bool isHumiditySimulated_m;
        double simulatedHumidity_m;

        // Weather station state variables
        bool isEnabled_m;

	// Pad to Weatherstaion assignation
	std::map<std::string, PadInfo*> pad2wsMap_m;

	//::Control::CurrentWeather::LocationSeq wsLocations_m;

	//  Pads' WeatherStations priorityList
	//map<string, PadInfo*> pad2ws_m;



    };
};
#endif // WEATHERSTATIONCONTROLLERIMPL_H
