#include <string.h>
#include <iostream>
#include <map>
#include <list>
#include <utility>
#include <math.h>

using namespace std;

class PriorityList {
	public:
	string wsName;
	double distance;
	bool isValid;
	PriorityList();
	PriorityList& operator=(const PriorityList& list);
	int operator<(const PriorityList& list) const;
	int operator==(const PriorityList& list) const;
	static bool comp_distance(PriorityList*, PriorityList*);
	static bool comp_max_allowed_distance(PriorityList*);
};

class PadInfo {
	public:
	string name;
    string associatedAntenna;
	list<PriorityList*> wsList;
        PadInfo();
	PadInfo& operator=(const PadInfo&); 
};

struct LocationS{
	double x;
	double y;
	double z;
	string name;
} typedef Location;


