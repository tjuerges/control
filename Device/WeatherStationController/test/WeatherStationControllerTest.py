#!/usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2009
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import sys
import unittest
import shutil
shutil.rmtree("../lib/python/site-packages", True)
import Acspy.Clients.SimpleClient
import TMCDB_IDL;
import asdmIDLTypes;
import Control
import math

def bringControlOperational(antennaNames):
    tmcdb = client.getDefaultComponent(\
                "IDL:alma/TMCDB/TMCDBComponent:1.0");
    antennaAssembly = []
    frontEndAssembly = [];

    sai = [];
    for i in range(len(antennaNames)):
        padName = 'TF' + str(i+1)
        sai.append(TMCDB_IDL.StartupAntennaIDL(antennaNames[i], padName, \
                                               "", i, frontEndAssembly,\
                                               antennaAssembly))
        ai = TMCDB_IDL.AntennaIDL(0, antennaNames[i],  "", \
                                  asdmIDLTypes.IDLLength(12), \
                                  asdmIDLTypes.IDLArrayTime(0), \
                                  asdmIDLTypes.IDLLength(1.0), \
                                  asdmIDLTypes.IDLLength(2.0), \
                                  asdmIDLTypes.IDLLength(10.0), \
                                  asdmIDLTypes.IDLLength(0.0), \
                                  asdmIDLTypes.IDLLength(0.0), \
                                  asdmIDLTypes.IDLLength(0.0), 0)
        tmcdb.setAntennaInfo(antennaNames[i], ai)
        pi = TMCDB_IDL.PadIDL(0, padName,\
                              asdmIDLTypes.IDLArrayTime(0),\
                              asdmIDLTypes.IDLLength( 2202229.615 + i),\
                              asdmIDLTypes.IDLLength(-5445184.762 + 2*i),\
                              asdmIDLTypes.IDLLength(-2485382.116 + 3*i))
        tmcdb.setAntennaPadInfo(antennaNames[i], pi)

    tmcdb.setStartupAntennasInfo(sai)
    master.startupPass1()
    master.startupPass2()
    client.releaseComponent(tmcdb._get_name());
        
# **************************************************
# automated test class
# **************************************************
class automated( unittest.TestCase ):
    def test01( self ):
        '''
        This simple test just loads and unloads the component. It finds
        errors due to missing symbols.
        '''
        wsc = client.getDefaultComponent\
             ('IDL:alma/Control/CurrentWeather:1.0')
        client.releaseComponent(wsc._get_name())

    def test02( self ):
        '''
        This tests the IDL interface.
        '''
        try:
            wsc = client.getDefaultComponent\
                 ('IDL:alma/Control/CurrentWeather:1.0')
            # Assume its working
            self.assertTrue(wsc.isEnabled("wsosf"));

            press = wsc.getPressure("WSOSF")
            self.assertFalse(wsc.isPressureSimulated());
            self.assertTrue(press.valid);
            self.assertTrue(press.value > 500E2 and press.value < 1500E2);
            self.assertTrue(press.timestamp > 131659776000000000L and \
                            press.timestamp < 163217376000000000L);

            speed = wsc.getWindSpeed("WSOSF")
            self.assertFalse(wsc.isWindSpeedSimulated());
            self.assertTrue(speed.valid);
            self.assertTrue(speed.value > 0 and speed.value < 50);
            self.assertTrue(speed.timestamp > 131659776000000000L and \
                            speed.timestamp < 163217376000000000L);

            drn = wsc.getWindDirection("WSOSF")
            self.assertFalse(wsc.isWindDirectionSimulated());
            self.assertTrue(drn.valid);
            self.assertTrue(drn.value > -2*math.pi and drn.value < 2*math.pi);
            self.assertTrue(drn.timestamp > 131659776000000000L and \
                            drn.timestamp < 163217376000000000L);

            temp = wsc.getTemperature("WSOSF")
            self.assertFalse(wsc.isTemperatureSimulated());
            self.assertTrue(temp.valid);
            self.assertTrue(temp.value > -20 and temp.value < 50);
            self.assertTrue(temp.timestamp > 131659776000000000L and \
                            temp.timestamp < 163217376000000000L);

            dp = wsc.getDewPoint("WSOSF")
            self.assertFalse(wsc.isDewPointSimulated());
            self.assertTrue(dp.valid);
            self.assertTrue(dp.value > -50 and dp.value < 50);
            self.assertTrue(dp.timestamp > 131659776000000000L and \
                            dp.timestamp < 163217376000000000L);

            rh = wsc.getHumidity("WSOSF")
            self.assertTrue(rh.valid);
            self.assertTrue(rh.value > 0 and rh.value < 1);
            self.assertTrue(rh.timestamp > 131659776000000000L and \
                            rh.timestamp < 163217376000000000L);

            # Now disable the weather station
            wsc.disableWeatherStation("WsOsf");
            self.assertFalse(wsc.isEnabled("wsosf"));

            self.assertTrue(wsc.isPressureSimulated());
            press = wsc.getPressureAtPad("A001")
            self.assertFalse(press.valid);
            self.assertTrue(press.value > 699E2 and press.value < 701E2);
            self.assertTrue(press.timestamp == 0L)

            self.assertTrue(wsc.isWindSpeedSimulated());
            speed = wsc.getWindSpeedAtPad("A99")
            self.assertFalse(speed.valid);
            self.assertTrue(speed.value > 4.9 and speed.value < 5.1);
            self.assertTrue(speed.timestamp == 0L)

            self.assertTrue(wsc.isWindDirectionSimulated());
            drn = wsc.getWindDirectionAtPad("A101")
            self.assertFalse(drn.valid);
            self.assertTrue(drn.value > math.pi*1.5 - .01 and \
                            drn.value < math.pi*1.5 + .01);
            self.assertTrue(drn.timestamp == 0L)

            self.assertTrue(wsc.isTemperatureSimulated());
            temp = wsc.getTemperatureAtPad("TF1")
            self.assertFalse(temp.valid);
            self.assertTrue(temp.value > 19 and temp.value < 21);
            self.assertTrue(temp.timestamp == 0L)

            self.assertTrue(wsc.isDewPointSimulated());
            dp = wsc.getDewPointAtPad("AE005")
            self.assertFalse(dp.valid);
            self.assertTrue(dp.value > -1 and dp.value < 1);
            self.assertTrue(dp.timestamp == 0L)

            wsc.enableWeatherStation("WSOSF");

            # Now simulate the pressure
            self.assertTrue(wsc.isEnabled("wsosf"));
            wsc.enablePressureSimulation(700.0E2)
            self.assertFalse(wsc.isHumiditySimulated());
            self.assertTrue(wsc.isPressureSimulated());
            press = wsc.getPressure("WSOSF")
            self.assertTrue(press.valid);
            self.assertTrue(press.value > 699E2 and press.value < 701E2);
            self.assertTrue(press.timestamp > 131659776000000000L and \
                            press.timestamp < 163217376000000000L);
            wsc.disablePressureSimulation()
            self.assertFalse(wsc.isPressureSimulated());

            # Now simulate the wind speed
            self.assertTrue(wsc.isEnabled("wsosf"));
            wsc.enableWindSpeedSimulation(20.0)
            self.assertFalse(wsc.isWindDirectionSimulated());
            self.assertTrue(wsc.isWindSpeedSimulated());
            speed = wsc.getWindSpeed("WSOSF")
            self.assertTrue(speed.valid);
            self.assertTrue(speed.value > 19.0 and speed.value < 21);
            self.assertTrue(speed.timestamp > 131659776000000000L and \
                            speed.timestamp < 163217376000000000L);
            wsc.disableWindSpeedSimulation()
            self.assertFalse(wsc.isWindSpeedSimulated());

            # Now simulate the wind direction
            self.assertTrue(wsc.isEnabled("wsosf"));
            wsc.enableWindDirectionSimulation(0.0)
            self.assertFalse(wsc.isWindSpeedSimulated());
            self.assertTrue(wsc.isWindDirectionSimulated());
            drn = wsc.getWindDirection("WSOSF")
            self.assertTrue(drn.valid);
            self.assertTrue(drn.value > -0.01 and drn.value < 0.01);
            self.assertTrue(drn.timestamp > 131659776000000000L and \
                            drn.timestamp < 163217376000000000L);
            wsc.disableWindDirectionSimulation()
            self.assertFalse(wsc.isWindDirectionSimulated());

            # Now simulate the temperature
            self.assertTrue(wsc.isEnabled("wsosf"));
            wsc.enableTemperatureSimulation(10.0)
            self.assertTrue(wsc.isTemperatureSimulated());
            self.assertTrue(wsc.isHumiditySimulated());
            temp = wsc.getTemperature("WSOSF")
            self.assertTrue(temp.valid);
            self.assertTrue(temp.value > 9.9 and temp.value < 10.1);
            self.assertTrue(temp.timestamp > 131659776000000000L and \
                            temp.timestamp < 163217376000000000L);
            wsc.disableTemperatureSimulation()
            self.assertFalse(wsc.isTemperatureSimulated());

            # Now simulate the dew-point
            self.assertTrue(wsc.isEnabled("wsosf"));
            wsc.enableDewPointSimulation(-10.0)
            self.assertTrue(wsc.isDewPointSimulated());
            self.assertTrue(wsc.isHumiditySimulated());
            dp = wsc.getDewPoint("WSOSF")
            self.assertTrue(dp.valid);
            self.assertTrue(dp.value > -10.1 and dp.value < -9.9);
            self.assertTrue(dp.timestamp > 131659776000000000L and \
                            dp.timestamp < 163217376000000000L);
            wsc.disableDewPointSimulation()
            self.assertFalse(wsc.isDewPointSimulated());

            # get the weather station names and locations
            l = wsc.weatherStationLocations();
            self.assertTrue(len(l) == 1)
            self.assertTrue(l[0].name == "WSOSF")
            self.assertTrue(l[0].x >  2200000 and l[0].x <  2300000)
            self.assertTrue(l[0].y < -5400000 and l[0].y > -5500000)
            self.assertTrue(l[0].z < -2400000 and l[0].z > -2500000)
            
        finally:
            client.releaseComponent(wsc._get_name())

    def test03( self ):
        '''
        This tests the CCL interface.
        '''
        pass


# **************************************************
# MAIN Program
# **************************************************
if __name__ == '__main__':

# Bring the control subsystem operational
    client = Acspy.Clients.SimpleClient.PySimpleClient()
    master = client.getComponent('CONTROL/MASTER')
    shutdownMasterAtEnd = False;
    if (master.getMasterState() == Control.INACCESSIBLE):
        bringControlOperational([])
        shutdownMasterAtEnd = True;
    if (master.getMasterState() != Control.OPERATIONAL):
        print 'Could not bring the control subsystem to an operational state'
        exit(1)

# Now run the tests
    runner = unittest.TextTestRunner()
    if len( sys.argv ) > 1:
        suite = unittest.makeSuite(automated, sys.argv[1])
    else:
        suite = unittest.makeSuite(automated)
    runner.run(suite)

# Shutdown the control system if we started it up
    if shutdownMasterAtEnd:
        master.shutdownPass1()
        master.shutdownPass2()
    client.releaseComponent(master._get_name()) 
