#!/usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007, 2009
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import Acspy.Clients.SimpleClient
import TMCDB_IDL;
import asdmIDLTypes;

client = Acspy.Clients.SimpleClient.PySimpleClient.getInstance()
master = client.getComponent('CONTROL/MASTER')
if (str(master.getMasterState()) == 'INACCESSIBLE'):
    antennaName = 'DA41'
    sai = TMCDB_IDL.StartupAntennaIDL(antennaName, "TF10", "", 1, [], \
                                      [])
    tmcdb = client.getDefaultComponent("IDL:alma/TMCDB/TMCDBComponent:1.0");
    tmcdb.setStartupAntennasInfo([sai])
    ai = TMCDB_IDL.AntennaIDL(0, antennaName,  "", \
                              asdmIDLTypes.IDLLength(12), \
                              asdmIDLTypes.IDLArrayTime(0), \
                              asdmIDLTypes.IDLLength(1.0), \
                              asdmIDLTypes.IDLLength(2.0), \
                              asdmIDLTypes.IDLLength(10.0), \
                              asdmIDLTypes.IDLLength(0.0), \
                              asdmIDLTypes.IDLLength(0.0), \
                              asdmIDLTypes.IDLLength(0.0), 0)
    tmcdb.setAntennaInfo(antennaName, ai)
    pi = TMCDB_IDL.PadIDL(0, "TF10", asdmIDLTypes.IDLArrayTime(0), \
                          asdmIDLTypes.IDLLength( 2202229.615), \
                          asdmIDLTypes.IDLLength(-5445184.762), \
                          asdmIDLTypes.IDLLength(-2485382.116))
    tmcdb.setAntennaPadInfo(antennaName, pi)
    master.startupPass1()
    master.startupPass2()
    client.releaseComponent(tmcdb._get_name());
