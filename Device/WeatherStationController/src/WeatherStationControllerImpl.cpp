// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char *rcsId="@(#) $Id$";
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "WeatherStationControllerImpl.h"
#include <loggingMACROS.h> // for AUTO_TRACE
//#include <ControlDeviceC.h>
#include "WeatherStationC.h"
#include <TMCDBAccessIFC.h> // for TMCDB::Access
#include <TmcdbErrType.h> // TMCDB error declarations
#include "acstimeEpochHelper.h"
#include <ostream>
#include <string>
#include <cmath>
#include <LogToAudience.h> // for LOG_TO_{DEVELOPER,OPERATOR}
#include <loggingMACROS.h> // for AUTO_TRACE

// shortcuts to avoid having to clutter the code with namespace
// qualifiers.
using Control::WeatherStationControllerImpl;
using Control::CurrentWeather;
using std::string;
using std::ostringstream;
using log_audience::OPERATOR;
using ControlExceptions::IllegalParameterErrorExImpl;
using ControlExceptions::HardwareErrorExImpl;
using std::log;
using std::exp;
using std::min;


/// Convert the TimeData structure returned by the OSF weather station to a
/// ACS::Time.
//ACS::Time TimeDataToTime(const Control::WeatherStation::TimeData& t) {
//    // the WeatherStation does not specify the date so assume today
//    EpochHelper eph(::getTimeStamp());
//    eph.hour(t.hour);
//    eph.minute(t.minutes);
//    eph.second(t.seconds);
//    return eph.value().value;
//}

//-----------------------------------------------------------------------------
// Constructor & Destructor
//-----------------------------------------------------------------------------
WeatherStationControllerImpl::
WeatherStationControllerImpl(const ACE_CString& name,
                             maci::ContainerServices* cs)
    :Control::HardwareControllerImpl(name, cs),
     isPressureSimulated_m(true),
     simulatedPressure_m(700E2),
     isWindSpeedSimulated_m(true),
     simulatedWindSpeed_m(5.0),
     isWindDirectionSimulated_m(true),
     simulatedWindDirection_m(3*M_PI/2),
     isTemperatureSimulated_m(true),
     simulatedTemperature_m(20.0),
     isDewPointSimulated_m(true),
     simulatedDewPoint_m(0.0),
     isHumiditySimulated_m(true),
     simulatedHumidity_m(10.0),
     isEnabled_m(false)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

WeatherStationControllerImpl::~WeatherStationControllerImpl() {
    AUTO_TRACE(__PRETTY_FUNCTION__);

}

void WeatherStationControllerImpl::createSubdevices(
    const Control::DeviceConfig& configuration, const char* parentName)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    Control::DeviceConfig wsConfig;
    CORBA::String_var cn(name());
    // Go through the sequence of subdevices getting references
    Control::HardwareControllerImpl::createSubdevices(configuration, cn.in());
    isPressureSimulated_m = isWindSpeedSimulated_m =
        isWindDirectionSimulated_m = isTemperatureSimulated_m =
        isDewPointSimulated_m = false;
}

void Control::WeatherStationControllerImpl::controllerOperational()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    Control::HardwareControllerImpl::controllerOperational();

    //generate the map
    GeneratePad2WeatherStationMap();
    ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
       (LM_DEBUG, "Weather Station priority List for all configured antennas (pads)"));
    for(map<string, PadInfo*>::iterator it=pad2wsMap_m.begin(); it!=pad2wsMap_m.end(); ++it) {
	    //cout << (*it).first << endl;
	    ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
               (LM_DEBUG, "  antenna pad : '%s'", (*it).first.c_str()));
            for(list<PriorityList*>::iterator lit=(*it).second->wsList.begin(); lit!=(*it).second->wsList.end(); ++lit) {
                //cout << " " << (*lit)->wsName << " isValid=" << (*lit)->isValid << endl;
                string isValidstr="False";
                if ((*lit)->isValid)
                    isValidstr="True";
	        ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                   (LM_DEBUG, "  Weather Station : '%s', isValid=%s", (*lit)->wsName.c_str(), isValidstr.c_str()));
            }
    }
    // This is a bit of a hack as it assumes that at least one weather station is active.
    isEnabled_m = true;
}


//
// PRESSURE
//
CurrentWeather::Pressure WeatherStationControllerImpl::
getPressureAtPad(const char* padName) {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    Control::CurrentWeather::Pressure data;
    const string name = throwIfUnknownPad(padName);
    const string ws_name = padNameToWeatherStation(padName);

    if(ws_name.length() == 0 ) {
	//pad doen't have a weather station associated
	string msg("pad '");
 	msg += string(padName) + "' does not have a weather station associated. Simulation values will be used.";
        LOG_TO_OPERATOR(LM_ERROR, msg);

        data.value = simulatedPressure_m;
        data.timestamp = ::getTimeStamp();
        data.valid = false;
    } else
    	data = getPressure(ws_name.c_str());

    return data;
}

CurrentWeather::Pressure WeatherStationControllerImpl::
getPressureAtAntenna(const char* antennaName) {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    return getPressureAtPad(antennaToPadName(antennaName));
}

CurrentWeather::Pressure WeatherStationControllerImpl::
getPressure(const char* wsName) {

    AUTO_TRACE(__PRETTY_FUNCTION__);
    //const string name = throwIfUnknownWS(wsName);

    Control::CurrentWeather::Pressure data;

    if (!isEnabled_m || !isValid()) {
        data.value = simulatedPressure_m;
        data.timestamp = 0;
        data.valid = false;
        return data;
    } else if (isPressureSimulated_m) {
        data.value = simulatedPressure_m;
        data.timestamp = ::getTimeStamp();
        data.valid = true;
        return data;
    }

    std::string lruType(wsName);
    std::map< std::string, Control::ControlDevice* >::iterator iter(
        subdevice_m.find(lruType));
    if(iter == subdevice_m.end())
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "No "
            << lruType
            << " component configured.  check system deployment";
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getINACTErrorEx();
    }
    else if(CORBA::is_nil((*iter).second) == true)
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "The device which is configured as "
            << lruType
            << " is not narrowable to a WeatherStation";
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getINACTErrorEx();
    }

    Control::WeatherStation_var wsn(Control::WeatherStation::_nil());
    wsn = Control::WeatherStation::_narrow((*iter).second);
    if(CORBA::is_nil(wsn.in()) == true)
    {
        std::ostringstream output;
        output << "Could not narrow the "
            << lruType
            << " component.";
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getINACTErrorEx();
    }

    try
    {
       data.value = wsn->GET_PRESSURE(data.timestamp);
       data.valid = true;
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedEx& ex)
    {
        string msg("Failed to read PRESSURE sensor from weather station '");
        msg += lruType + "' due to Socket problem.";
        LOG_TO_OPERATOR(LM_ERROR, msg);
    }
    catch(const EthernetDeviceExceptions::IllegalParameterEx& ex)
    {
        string msg("Failed to read PRESSURE sensor from weather station '");
        msg += lruType + "' due to Ilegal Parameter problem.";
        LOG_TO_OPERATOR(LM_ERROR, msg);
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        ControlExceptions::INACTErrorExImpl nex( ex,__FILE__, __LINE__,
            "WeatherStationController::getPressure");

        string msg("Failed to read PRESSURE sensor from weather station '");
        msg += lruType + "' because it is not in hwOperational state.";
        LOG_TO_OPERATOR(LM_ERROR, msg);
	//nex.log();
        //throw nex.getINACTErrorEx();
    }

    return data;
}

//
// WINDSPEED
//
CurrentWeather::WindSpeed WeatherStationControllerImpl::
getWindSpeedAtPad(const char* padName) {
    AUTO_TRACE(__PRETTY_FUNCTION__);

    Control::CurrentWeather::WindSpeed data;
    const string name = throwIfUnknownPad(padName);
    const string ws_name = padNameToWeatherStation(padName);

    if(ws_name.length() == 0 ) {
        //pad doen't have a weather station associated
        string msg("pad '");
        msg += string(padName) + "' does not have a weather station associated. Simulation values will be used.";
        LOG_TO_OPERATOR(LM_ERROR, msg);

        data.value = simulatedWindSpeed_m;
        data.timestamp = ::getTimeStamp();
        data.valid = false;
    } else
        data = getWindSpeed(ws_name.c_str());

    return data;
}

CurrentWeather::WindSpeed WeatherStationControllerImpl::
getWindSpeedAtAntenna(const char* antennaName) {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    return getWindSpeedAtPad(antennaToPadName(antennaName));
}

CurrentWeather::WindSpeed WeatherStationControllerImpl::
getWindSpeed(const char* wsName) {

    AUTO_TRACE(__PRETTY_FUNCTION__);
    //const string name = throwIfUnknownWS(wsName);

    Control::CurrentWeather::WindSpeed data;

    if (!isEnabled_m || !isValid()) {
        data.value = simulatedWindSpeed_m;
        data.timestamp = 0;
        data.valid = false;
        return data;
    } else if (isTemperatureSimulated_m) {
        data.value = simulatedWindSpeed_m;
        data.timestamp = ::getTimeStamp();
        data.valid = true;
        return data;
    }

    std::string lruType(wsName);

    std::map< std::string, Control::ControlDevice* >::iterator iter(
        subdevice_m.find(lruType));
    if(iter == subdevice_m.end())
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "No "
            << lruType
            << " component configured.  check system deployment";
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getINACTErrorEx();
    }
    else if(CORBA::is_nil((*iter).second) == true)
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "The device which is configured as "
            << lruType
            << " is not narrowable to a WeatherStation";
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getINACTErrorEx();
    }

    Control::WeatherStation_var wsn(Control::WeatherStation::_nil());
    wsn = Control::WeatherStation::_narrow((*iter).second);
    if(CORBA::is_nil(wsn.in()) == true)
    {
        std::ostringstream output;
        output << "Could not narrow the "
            << lruType
            << " component.";
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getINACTErrorEx();
    }

    try
    {
       data.value = wsn->GET_WINDSPEED(data.timestamp);
       data.valid = true;
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedEx& ex)
    {
        string msg("Failed to read WINDSPEED sensor from weather station '");
        msg += lruType + "' due to Socket problem.";
        LOG_TO_OPERATOR(LM_ERROR, msg);
    }
    catch(const EthernetDeviceExceptions::IllegalParameterEx& ex)
    {
        string msg("Failed to read WINDSPEED sensor from weather station '");
        msg += lruType + "' due to Ilegal Parameter problem.";
        LOG_TO_OPERATOR(LM_ERROR, msg);
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        ControlExceptions::INACTErrorExImpl nex( ex,__FILE__, __LINE__,
            "WeatherStationController::getWindSpeed");

        string msg("Failed to read WINDSPEED sensor from weather station '");
        msg += lruType + "' because it is not in hwOperational state.";
        LOG_TO_OPERATOR(LM_ERROR, msg);
	//nex.log();
        //throw nex.getINACTErrorEx();
    }

    return data;
}

//
// WIND DIRECTION
//
CurrentWeather::WindDirection WeatherStationControllerImpl::
getWindDirectionAtPad(const char* padName) {
    AUTO_TRACE(__PRETTY_FUNCTION__);

    Control::CurrentWeather::WindDirection data;
    const string name = throwIfUnknownPad(padName);

    if (!isEnabled_m || !isValid()) {
        data.value = simulatedWindDirection_m;
        data.timestamp = 0;
        data.valid = false;
        return data;
    } else if (isTemperatureSimulated_m) {
        data.value = simulatedWindDirection_m;
        data.timestamp = ::getTimeStamp();
        data.valid = true;
        return data;
    }

    const string ws_name = padNameToWeatherStation(padName);

    if(ws_name.length() == 0 ) {
        //pad doen't have a weather station associated
        string msg("pad '");
        msg += string(padName) + "' does not have a weather station associated. Simulation values will be used.";
        LOG_TO_OPERATOR(LM_ERROR, msg);

        data.value = simulatedWindDirection_m;
        data.timestamp = ::getTimeStamp();
        data.valid = false;
    } else
        data = getWindDirection(ws_name.c_str());

    return data;
}


CurrentWeather::WindDirection WeatherStationControllerImpl::
getWindDirectionAtAntenna(const char* antennaName) {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    return getWindDirectionAtPad(antennaToPadName(antennaName));
}

CurrentWeather::WindDirection WeatherStationControllerImpl::
getWindDirection(const char* wsName) {

    AUTO_TRACE(__PRETTY_FUNCTION__);
    //const string name = throwIfUnknownWS(wsName);

    Control::CurrentWeather::WindDirection data;
    std::string lruType(wsName);

    std::map< std::string, Control::ControlDevice* >::iterator iter(
        subdevice_m.find(lruType));
    if(iter == subdevice_m.end())
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "No "
            << lruType
            << " component configured.  check system deployment";
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getINACTErrorEx();
    }
    else if(CORBA::is_nil((*iter).second) == true)
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "The device which is configured as "
            << lruType
            << " is not narrowable to a WeatherStation";
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getINACTErrorEx();
    }

    Control::WeatherStation_var wsn(Control::WeatherStation::_nil());
    wsn = Control::WeatherStation::_narrow((*iter).second);
    if(CORBA::is_nil(wsn.in()) == true)
    {
        std::ostringstream output;
        output << "Could not narrow the "
            << lruType
            << " component.";
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getINACTErrorEx();
    }

    try
    {
       data.value = wsn->GET_WINDDIRECTION(data.timestamp);
       data.valid = true;
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedEx& ex)
    {
        string msg("Failed to read WINDDIRECTION sensor from weather station '");
        msg += lruType + "' due to Socket problem.";
        LOG_TO_OPERATOR(LM_ERROR, msg);
    }
    catch(const EthernetDeviceExceptions::IllegalParameterEx& ex)
    {
        string msg("Failed to read WINDDIRECTION sensor from weather station '");
        msg += lruType + "' due to Ilegal Parameter problem.";
        LOG_TO_OPERATOR(LM_ERROR, msg);
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        ControlExceptions::INACTErrorExImpl nex( ex,__FILE__, __LINE__,
            "WeatherStationController::getWindDirection");

        string msg("Failed to read WINDDIRECTION sensor from weather station '");
        msg += lruType + "' because it is not in hwOperational state.";
        LOG_TO_OPERATOR(LM_ERROR, msg);
	//nex.log();
        //throw nex.getINACTErrorEx();
    }

    return data;
}

//
// TEMPERATURE
//
CurrentWeather::Temperature WeatherStationControllerImpl::
getTemperatureAtPad(const char* padName) {
    AUTO_TRACE(__PRETTY_FUNCTION__);

    Control::CurrentWeather::Temperature data;
    const string name = throwIfUnknownPad(padName);
    const string ws_name = padNameToWeatherStation(padName);

    if(ws_name.length() == 0 ) {
        //pad doen't have a weather station associated
        string msg("pad '");
        msg += string(padName) + "' does not have a weather station associated. Simulation values will be used.";
        LOG_TO_OPERATOR(LM_ERROR, msg);

        data.value = simulatedTemperature_m;
        data.timestamp = ::getTimeStamp();
        data.valid = false;
    } else
        data = getTemperature(ws_name.c_str());

    return data;
}

CurrentWeather::Temperature WeatherStationControllerImpl::
getTemperatureAtAntenna(const char* antennaName) {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    return getTemperatureAtPad(antennaToPadName(antennaName));
}

CurrentWeather::Temperature WeatherStationControllerImpl::
getTemperature(const char* wsName) {

    AUTO_TRACE(__PRETTY_FUNCTION__);
    //const string name = throwIfUnknownWS(wsName);

    Control::CurrentWeather::Temperature data;

    if (!isEnabled_m || !isValid()) {
        data.value = simulatedTemperature_m;
        data.timestamp = 0;
        data.valid = false;
        return data;
    } else if (isTemperatureSimulated_m) {
        data.value = simulatedTemperature_m;
        data.timestamp = ::getTimeStamp();
        data.valid = true;
        return data;
    }

    std::string lruType(wsName);

    std::map< std::string, Control::ControlDevice* >::iterator iter(
        subdevice_m.find(lruType));
    if(iter == subdevice_m.end())
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "No "
            << lruType
            << " component configured.  check system deployment";
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getINACTErrorEx();
    }
    else if(CORBA::is_nil((*iter).second) == true)
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "The device which is configured as "
            << lruType
            << " is not narrowable to a WeatherStation";
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getINACTErrorEx();
    }

    Control::WeatherStation_var wsn(Control::WeatherStation::_nil());
    wsn = Control::WeatherStation::_narrow((*iter).second);
    if(CORBA::is_nil(wsn.in()) == true)
    {
        std::ostringstream output;
        output << "Could not narrow the "
            << lruType
            << " component.";
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getINACTErrorEx();
    }

    try
    {
       data.value = wsn->GET_TEMPERATURE(data.timestamp);
       data.valid = true;
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedEx& ex)
    {
        string msg("Failed to read TEMPERTURE sensor from weather station '");
        msg += lruType + "' due to Socket problem.";
        LOG_TO_OPERATOR(LM_ERROR, msg);
    }
    catch(const EthernetDeviceExceptions::IllegalParameterEx& ex)
    {
        string msg("Failed to read TEMPERATURE sensor from weather station '");
        msg += lruType + "' due to Ilegal Parameter problem.";
        LOG_TO_OPERATOR(LM_ERROR, msg);
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        ControlExceptions::INACTErrorExImpl nex( ex,__FILE__, __LINE__,
            "WeatherStationController::getTemperature");

        string msg("Failed to read TEMPERATURE sensor from weather station '");
        msg += lruType + "' because it is not in hwOperational state.";
        LOG_TO_OPERATOR(LM_ERROR, msg);
	//nex.log();
        //throw nex.getINACTErrorEx();
    }

    return data;
}

//
// DEWPOINT
//
CurrentWeather::DewPoint WeatherStationControllerImpl::
getDewPointAtPad(const char* padName) {
    AUTO_TRACE(__PRETTY_FUNCTION__);

    Control::CurrentWeather::DewPoint data;
    const string name = throwIfUnknownPad(padName);
    const string ws_name = padNameToWeatherStation(padName);

    if(ws_name.length() == 0 ) {
        //pad doen't have a weather station associated
        string msg("pad '");
        msg += string(padName) + "' does not have a weather station associated. Simulation values will be used.";
        LOG_TO_OPERATOR(LM_ERROR, msg);

        data.value = simulatedDewPoint_m;
        data.timestamp = ::getTimeStamp();
        data.valid = false;
    } else
        data = getDewPoint(ws_name.c_str());

    return data;
}

CurrentWeather::DewPoint WeatherStationControllerImpl::
getDewPointAtAntenna(const char* antennaName) {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    return getDewPointAtPad(antennaToPadName(antennaName));
}

CurrentWeather::DewPoint WeatherStationControllerImpl::
getDewPoint(const char* wsName) {

    AUTO_TRACE(__PRETTY_FUNCTION__);
    //const string name = throwIfUnknownWS(wsName);

    Control::CurrentWeather::DewPoint data;

    if (!isEnabled_m || !isValid()) {
        data.value = simulatedDewPoint_m;
        data.timestamp = 0;
        data.valid = false;
        return data;
    } else if (isTemperatureSimulated_m) {
        data.value = simulatedDewPoint_m;
        data.timestamp = ::getTimeStamp();
        data.valid = true;
        return data;
    }

    std::string lruType(wsName);

    std::map< std::string, Control::ControlDevice* >::iterator iter(
        subdevice_m.find(lruType));
    if(iter == subdevice_m.end())
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "No "
            << lruType
            << " component configured.  check system deployment";
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getINACTErrorEx();
    }
    else if(CORBA::is_nil((*iter).second) == true)
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "The device which is configured as "
            << lruType
            << " is not narrowable to a WeatherStation";
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getINACTErrorEx();
    }

    Control::WeatherStation_var wsn(Control::WeatherStation::_nil());
    wsn = Control::WeatherStation::_narrow((*iter).second);
    if(CORBA::is_nil(wsn.in()) == true)
    {
        std::ostringstream output;
        output << "Could not narrow the "
            << lruType
            << " component.";
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getINACTErrorEx();
    }

    try
    {
       data.value = wsn->GET_DEWPOINT(data.timestamp);
       data.valid = true;
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedEx& ex)
    {
        string msg("Failed to read DEWPOINT sensor from weather station '");
        msg += lruType + "' due to Socket problem.";
        LOG_TO_OPERATOR(LM_ERROR, msg);
    }
    catch(const EthernetDeviceExceptions::IllegalParameterEx& ex)
    {
        string msg("Failed to read DEWPOINT sensor from weather station '");
        msg += lruType + "' due to Ilegal Parameter problem.";
        LOG_TO_OPERATOR(LM_ERROR, msg);
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        ControlExceptions::INACTErrorExImpl nex( ex,__FILE__, __LINE__,
            "WeatherStationController::getDewPoint");

        string msg("Failed to read DEWPOINT sensor from weather station '");
        msg += lruType + "' because it is not in hwOperational state.";
        LOG_TO_OPERATOR(LM_ERROR, msg);
	//nex.log();
        //throw nex.getINACTErrorEx();
    }

    return data;
}

//
// HUMIDITY
//
CurrentWeather::Humidity WeatherStationControllerImpl::
getHumidityAtPad(const char* padName) {
    AUTO_TRACE(__PRETTY_FUNCTION__);

    Control::CurrentWeather::Humidity data;
    const string name = throwIfUnknownPad(padName);
    const string ws_name = padNameToWeatherStation(padName);

    if(ws_name.length() == 0 ) {
        //pad doen't have a weather station associated
        string msg("pad '");
        msg += string(padName) + "' does not have a weather station associated. Simulation values will be used.";
        LOG_TO_OPERATOR(LM_ERROR, msg);

        data.value = simulatedHumidity_m;
        data.timestamp = ::getTimeStamp();
        data.valid = false;
    } else
        data = getHumidity(ws_name.c_str());

    return data;
}

CurrentWeather::Humidity WeatherStationControllerImpl::
getHumidityAtAntenna(const char* antennaName) {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    return getHumidityAtPad(antennaToPadName(antennaName));
}

CurrentWeather::Humidity WeatherStationControllerImpl::
getHumidity(const char* wsName) {


    AUTO_TRACE(__PRETTY_FUNCTION__);
    //const string name = throwIfUnknownWS(wsName);

    Control::CurrentWeather::Humidity data;
    if (!isEnabled_m || !isValid()) {
        data.value = simulatedHumidity_m;
        data.timestamp = 0;
        data.valid = false;
        return data;
    } else if (isTemperatureSimulated_m) {
        data.value = simulatedHumidity_m;
        data.timestamp = ::getTimeStamp();
        data.valid = true;
        return data;
    }

    std::string lruType(wsName);

    std::map< std::string, Control::ControlDevice* >::iterator iter(
        subdevice_m.find(lruType));
    if(iter == subdevice_m.end())
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "No "
            << lruType
            << " component configured.  check system deployment";
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getINACTErrorEx();
    }
    else if(CORBA::is_nil((*iter).second) == true)
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "The device which is configured as "
            << lruType
            << " is not narrowable to a WeatherStation";
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getINACTErrorEx();
    }

    Control::WeatherStation_var wsn(Control::WeatherStation::_nil());
    wsn = Control::WeatherStation::_narrow((*iter).second);
    if(CORBA::is_nil(wsn.in()) == true)
    {
        std::ostringstream output;
        output << "Could not narrow the "
            << lruType
            << " component.";
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getINACTErrorEx();
    }

    try
    {
       data.value = wsn->GET_HUMIDITY(data.timestamp);
       data.valid = true;
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedEx& ex)
    {
        string msg("Failed to read HUMIDITY sensor from weather station '");
        msg += lruType + "' due to Socket problem.";
        LOG_TO_OPERATOR(LM_ERROR, msg);
    }
    catch(const EthernetDeviceExceptions::IllegalParameterEx& ex)
    {
        string msg("Failed to read HUMIDTY sensor from weather station '");
        msg += lruType + "' due to Ilegal Parameter problem.";
        LOG_TO_OPERATOR(LM_ERROR, msg);
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        ControlExceptions::INACTErrorExImpl nex( ex,__FILE__, __LINE__,
            "WeatherStationController::getHumidity");

        string msg("Failed to read HUMIDITY sensor from weather station '");
        msg += lruType + "' because it is not in hwOperational state.";
        LOG_TO_OPERATOR(LM_ERROR, msg);
	//nex.log();
        //throw nex.getINACTErrorEx();
    }
    return data;
}

CurrentWeather::Humidity WeatherStationControllerImpl::
getCalculatedHumidity(const char* wsName) {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    const Control::CurrentWeather::Temperature temp = getTemperature(wsName);
    const Control::CurrentWeather::DewPoint dewp = getDewPoint(wsName);
    const Control::CurrentWeather::Pressure pressure = getPressure(wsName);
    Control::CurrentWeather::Humidity data;
    data.timestamp = min(dewp.timestamp, temp.timestamp);
    data.timestamp = min(data.timestamp, pressure.timestamp);
    data.valid = dewp.valid && temp.valid && pressure.valid;

    // This relationship, originally due to Buck, is described in Appendix B of
    // the "ALMA Weather Instrumentation Specification" dated 2006-12-01.
    data.value = ew(dewp.value, pressure.value)/ew(temp.value, pressure.value);
    return data;
}

double WeatherStationControllerImpl::ew(const double& temp, const double& pressure) {
    const double pressureInhPa = pressure/100.0;
    double ef = 0.0383 + 6.4E-6 * temp * temp;
    ef = 2.2 + pressureInhPa * ef;
    ef = 1 + 1E-4 * ef;
    double ew1 = temp/(temp + 257.14);
    double ew2 = 18.678 - temp/234.5;
    return ef * 6.1121 * std::exp(ew1 * ew2);
}

CurrentWeather::LocationSeq* WeatherStationControllerImpl::
weatherStationLocations() {

    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACS::Time timestamp;

    Control::CurrentWeather::LocationSeq_var wsLocations =
        new Control::CurrentWeather::LocationSeq();

    wsLocations->length(subdevice_m.size());

    std::map< string, Control::ControlDevice_ptr >::iterator iter(
        subdevice_m.begin());
    for(int i=0; iter != subdevice_m.end(); ++iter, i++) {
    	ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
           (LM_DEBUG, "Getting location information of weather station : '%s'", (*iter).first.c_str()));
	if(CORBA::is_nil((*iter).second) == true)
        {
	    ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
	    std::ostringstream output;
	    output << "The device which is configured as "
	        << (*iter).first
	        << " is not narrowable to a WeatherStation";
	    ex.addData("Detail", output.str());
	    ex.log();
	    throw ex.getINACTErrorEx();
	}

	Control::WeatherStation_var wsn(Control::WeatherStation::_nil());
	wsn = Control::WeatherStation::_narrow((*iter).second);

	if(CORBA::is_nil(wsn.in()) == true)
        {
 	    std::ostringstream output;
	    output << "Could not narrow the "
	        << (*iter).first
	        << " component.";
	    ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
	    ex.addData("Detail", output.str());
            ex.log();
           throw ex.getINACTErrorEx();
        }

        try
        {
           (*wsLocations)[i].name = CORBA::string_dup((*iter).first.c_str());
	   //initialized x, y , z in case of any of the following call fails
	   (*wsLocations)[i].x = 0;
           (*wsLocations)[i].y = 0;
           (*wsLocations)[i].z = 0;
	   (*wsLocations)[i].x =  wsn->GET_POSITION_X(timestamp);
           (*wsLocations)[i].y =  wsn->GET_POSITION_Y(timestamp);
           (*wsLocations)[i].z =  wsn->GET_POSITION_Z(timestamp);
        }
        catch(const EthernetDeviceExceptions::SocketOperationFailedEx& ex)
        {
            string msg("Failed to read location from weather station '");
            msg += (*iter).first + "' due to Socket problem.";
            LOG_TO_OPERATOR(LM_ERROR, msg);
        }
        catch(const EthernetDeviceExceptions::IllegalParameterEx& ex)
        {
            string msg("Failed to read location from weather station '");
            msg += (*iter).first + "' due to Ilegal Parameter problem.";
            LOG_TO_OPERATOR(LM_ERROR, msg);
        }
        catch(const ControlExceptions::INACTErrorEx& ex)
        {
            ControlExceptions::INACTErrorExImpl nex( ex,__FILE__, __LINE__,
                "WeatherStationController::getHumidity");

            string msg("Failed to read location from weather station '");
            msg += (*iter).first + "' because it is not in hwOperational state.";
            LOG_TO_OPERATOR(LM_ERROR, msg);
    	    //nex.log();
            //throw nex.getINACTErrorEx();
        }
    }

    return wsLocations._retn();
}

CORBA::Boolean  WeatherStationControllerImpl::isEnabled(const char* name) {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    //const string wsName = throwIfUnknownWS(name);
    return isEnabled_m;
}

CORBA::Boolean WeatherStationControllerImpl::isPressureSimulated() {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    return isPressureSimulated_m || !isEnabled_m;
}

CORBA::Boolean WeatherStationControllerImpl::isWindSpeedSimulated() {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    return isWindSpeedSimulated_m || !isEnabled_m;
}

CORBA::Boolean WeatherStationControllerImpl::isWindDirectionSimulated() {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    return isWindDirectionSimulated_m || !isEnabled_m;
}

CORBA::Boolean WeatherStationControllerImpl::isTemperatureSimulated() {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    return isTemperatureSimulated_m || !isEnabled_m;
}

CORBA::Boolean WeatherStationControllerImpl::isDewPointSimulated() {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    return isDewPointSimulated_m || !isEnabled_m;
}

CORBA::Boolean WeatherStationControllerImpl::isHumiditySimulated() {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    return isDewPointSimulated() || isTemperatureSimulated();
}

CORBA::Boolean WeatherStationControllerImpl::
enableWeatherStation(const char* name) {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    const string wsName = throwIfUnknownWS(name);
    if (isValid()) {
        isEnabled_m = true;
        return true;
    }
    isEnabled_m = false;
    return false;
}

CORBA::Boolean WeatherStationControllerImpl::
disableWeatherStation(const char* name) {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    const string wsName = throwIfUnknownWS(name);
    isEnabled_m = false;
    return true;
}

void WeatherStationControllerImpl::
enablePressureSimulation(CORBA::Double simValue) {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    if (simValue < 0.0 || simValue > 2000E2) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ostringstream msg;
        msg << "The specified simulation pressure of " << simValue
            <<  " Pa. is not between 0 and 2000E2 Pa.";
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    simulatedPressure_m = simValue;
    isPressureSimulated_m = true;
}

void WeatherStationControllerImpl::disablePressureSimulation() {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    isPressureSimulated_m = false;
}

void WeatherStationControllerImpl::
enableWindSpeedSimulation(CORBA::Double simValue) {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    if (simValue < 0.0 || simValue > 50) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ostringstream msg;
        msg << "The specified simulation wind speed of " << simValue
            <<  " m/s is not between 0 and 50 m/s.";
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    simulatedWindSpeed_m = simValue;
    isWindSpeedSimulated_m = true;
}

void WeatherStationControllerImpl::disableWindSpeedSimulation() {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    isWindSpeedSimulated_m = false;
}

void WeatherStationControllerImpl::
enableWindDirectionSimulation(CORBA::Double simValue) {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    if (simValue < -2*M_PI || simValue > 2*M_PI) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ostringstream msg;
        msg << "The specified simulation wind direction of " << simValue
            <<  "  radians is not between -2*pi and 2*pi radians.";
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    simulatedWindDirection_m = simValue;
    isWindDirectionSimulated_m = true;
}

void WeatherStationControllerImpl::disableWindDirectionSimulation() {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    isWindDirectionSimulated_m = false;
}

void WeatherStationControllerImpl::
enableTemperatureSimulation(CORBA::Double simValue) {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    if (simValue < -50.0 || simValue > 50.0) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ostringstream msg;
        msg << "The specified simulation temperature of " << simValue
            <<  " deg. C. is not between -50 and +50 deg. C.";
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    simulatedTemperature_m = simValue;
    isTemperatureSimulated_m = true;
}

void WeatherStationControllerImpl::disableTemperatureSimulation() {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    isTemperatureSimulated_m = false;
}

void WeatherStationControllerImpl::
enableDewPointSimulation(CORBA::Double simValue) {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    if (simValue < -50.0 || simValue > 50.0) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ostringstream msg;
        msg << "The specified simulation dew point of " << simValue
            <<  " deg. C. is not between -50 and +50 deg. C.";
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    simulatedDewPoint_m = simValue;
    isDewPointSimulated_m = true;
}

void WeatherStationControllerImpl::disableDewPointSimulation() {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    isDewPointSimulated_m = false;
}

string WeatherStationControllerImpl::throwIfUnknownWS(const string& wsName) {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    // First convert string to upper case.
    string ucName(wsName);
    for (string::iterator p = ucName.begin(); p !=  ucName.end(); ++p) {
        *p = std::toupper(*p);
    }

    if (ucName != "WS1" & ucName != "WS2" & ucName != "WS3") {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        string msg("The weather station called ");
        msg +=  wsName + " is not a part of the configuration.";
        msg += " Only the 'WS' weather station is currently supported.";
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    return ucName;
}

//
//CORBA::Boolean  WeatherStationControllerImpl::GetEnabled(const char* name) {
//    AUTO_TRACE(__PRETTY_FUNCTION__);
//    //const string wsName = throwIfUnknownWS(name);
//    return isEnabled_m;
//}

//Calculate the PriorityList of weather station for each pad
void WeatherStationControllerImpl::GeneratePad2WeatherStationMap() {
	PadInfo *padInfo;
	Location padLoc;

	// From TMCDBComponets get startup antennas info
        try {
            maci::SmartPtr<TMCDB::Access> tmcdb =
                getContainerServices()->getDefaultComponentSmartPtr<TMCDB::Access>
                ("IDL:alma/TMCDB/Access:1.0");
            TMCDB::StartupAntennaSeq_var antennasInfo = tmcdb->getStartupAntennasInfo();
   	    // For each antenna retrieve its pad information
	    for(unsigned int i=0; i<antennasInfo->length(); i++) {
            	TMCDB_IDL::PadIDL_var pi =	tmcdb->getCurrentAntennaPadInfo((*antennasInfo)[i].antennaName);
                padLoc.x = pi->XPosition.value;
                padLoc.y = pi->YPosition.value;
                padLoc.z = pi->ZPosition.value;
                padLoc.name  = pi->PadName;

                padInfo = new PadInfo();
                padInfo->name = padLoc.name;
                padInfo->wsList = CalculatePadInfo(padLoc);
                padInfo->associatedAntenna= (*antennasInfo)[i].antennaName;
                pad2wsMap_m[padInfo->name] = padInfo;
	    }

        } catch (maciErrType::maciErrTypeExImpl& ex) {
            string msg = "Cannot retrieve the antenna location from the database.";
	    ControlDeviceExceptions::IllegalConfigurationExImpl newEx(ex, __FILE__, __LINE__, __func__);
            newEx.addData("Detail", msg);
            newEx.log();
            throw newEx.getIllegalConfigurationEx();
        } catch (TmcdbErrType::TmcdbErrorExImpl& ex) {
            string msg = "Cannot retrieve the antenna location from the database.";
	    ControlDeviceExceptions::IllegalConfigurationExImpl newEx(ex, __FILE__, __LINE__, __func__);
            newEx.addData("Detail", msg);
            newEx.log();
            throw newEx.getIllegalConfigurationEx();
        }
}

list<PriorityList*> WeatherStationControllerImpl::CalculatePadInfo(Location padLoc) {
    	AUTO_TRACE(__PRETTY_FUNCTION__);

	//calculate for the given part the list of
	//available valid weather stations
	PriorityList* ws;
	list<PriorityList*> priorityList;
	list<PriorityList*>::iterator it;
	Control::CurrentWeather::LocationSeq_var WeatherStationsLoc;
	double diffX;
	double diffY;
	double diffZ;

	WeatherStationsLoc = weatherStationLocations();

	//cout << "Calculating distances for pad " + padLoc.name  << endl;
	ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
	   (LM_DEBUG, "Computing distances for pad : '%s'", padLoc.name.c_str()));
	for(unsigned int i=0; i<WeatherStationsLoc->length(); i++) {
		ws = new PriorityList();
		diffX = (*WeatherStationsLoc)[i].x - padLoc.x;
		diffY = (*WeatherStationsLoc)[i].y - padLoc.y;
		diffZ = (*WeatherStationsLoc)[i].z - padLoc.z;

		ws->distance = sqrt(pow(diffX,2) + pow(diffY, 2) + pow(diffZ, 2));
		ws->wsName = WeatherStationsLoc[i].name;
		ws->isValid = true;
		priorityList.push_back(ws);
		//cout << "distance of pad " + padLoc.name + " to " + ws->wsName + " = "  << ws->distance << endl ;
		ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
		   (LM_DEBUG, "   Distance between pad and '%s' = %.2f", ws->wsName.c_str(), ws->distance));
	}

	priorityList.sort(PriorityList::comp_distance);
	priorityList.remove_if(PriorityList::comp_max_allowed_distance);

	//cout << "    Sorting and removing weather station with distance > than 1500 meters ...."  << endl;
	ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
	   (LM_DEBUG, "Sorting and removing weather station with distance > than 1500 meters"));
	for(it=priorityList.begin(); it!=priorityList.end(); ++it) {
		//cout << "    distance of pad " + padLoc.name + " to " + (*it)->wsName + " = "  << (*it)->distance << endl;
		ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
		   (LM_DEBUG, "   Distance between pad and '%s' = %.2f", ws->wsName.c_str(), ws->distance));
	}

	return priorityList;
}

string WeatherStationControllerImpl::padNameToWeatherStation(string padName) {
    AUTO_TRACE(__PRETTY_FUNCTION__);

    list<PriorityList*>::iterator it;
    map<string, PadInfo*>::iterator padInfoIter;
    string bestWeatherStation;

    string ucName(padName);
    for (string::iterator p = ucName.begin(); p !=  ucName.end(); ++p) {
        *p = std::toupper(*p);
    }

    padInfoIter = pad2wsMap_m.find(ucName);
    if(padInfoIter != pad2wsMap_m.end()) {
        for(it=(*padInfoIter).second->wsList.begin(); it!= (*padInfoIter).second->wsList.end(); ++it) {
           if((*it)->isValid)
    	   {
    	       bestWeatherStation = (*it)->wsName;
               ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                   (LM_DEBUG, "pad '%s' is assigned to use weather station '%s'", padName.c_str(), bestWeatherStation.c_str()));
    	       return bestWeatherStation;
    	   }
       }
    }

   //if reaches here then throw exception of no weather station available for this pad
   //instead of returning the default weather station
   string defaultWS = getDefaultWeatherStation(padName);
   string msg = "pad " + padName + " doesn't have a valid weather station, using '"+defaultWS+"' instead";
   LOG_TO_OPERATOR(LM_ERROR, msg);
   return defaultWS;
}

const char *WeatherStationControllerImpl::antennaToPadName(string antennaName) {
    AUTO_TRACE(__PRETTY_FUNCTION__);

    list<PriorityList*>::iterator it;
    map<string, PadInfo*>::iterator padInfoIter;

    string ucName(antennaName);
    for (string::iterator p = ucName.begin(); p !=  ucName.end(); ++p) {
        *p = std::toupper(*p);
    }
    // iterate through the padmap list
     for (padInfoIter= pad2wsMap_m.begin(); padInfoIter !=  pad2wsMap_m.end(); ++padInfoIter) {
         if ( (*padInfoIter).second->associatedAntenna == ucName )
              return (*padInfoIter).second->name.c_str();
    }
    IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __PRETTY_FUNCTION__);
    ostringstream msg;
    msg << "There is no pad associated to antenna" <<  antennaName;
    ex.addData("Detail", msg.str());
    ex.log();
    throw ex.getIllegalParameterErrorEx();
}


string WeatherStationControllerImpl::getDefaultWeatherStation(const string& padName) {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    // First convert string to upper case.
    string ucName(padName);
    for (string::iterator p = ucName.begin(); p !=  ucName.end(); ++p) {
        *p = std::toupper(*p);
    }
    if (ucName.length() > 1) { // All pad names have at least 2 characters.
	// Must check the two letter (OSF) pad names first.
	if (ucName.substr(0, 2) == "TF")  // OSF. Technical Facility
		return string("WSOSF");
	if (ucName.substr(0, 2) == "ME")  // OSF. Melco vendor area
		return string("WSOSF");
	if (ucName.substr(0, 2) == "VX")  // OSF. Vertex vendor area
		return string("WSOSF");
	if (ucName.substr(0, 2) == "AE")  // OSF. AEM vendor area
		return string("WSOSF");
     }
     // if the pad is not in OSF, it should be at AOS
     return string("WSTB1");
}

string WeatherStationControllerImpl::throwIfUnknownPad(const string& padName) {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    // First convert string to upper case.
    string ucName(padName);
    for (string::iterator p = ucName.begin(); p !=  ucName.end(); ++p) {
        *p = std::toupper(*p);
    }

    if (ucName.length() > 1) { // All pad names have at least 2 characters.
        char* last;
        // Must check the two letter (OSF) pad names first.
        if (ucName.substr(0, 2) == "TF") { // OSF. Technical Facility
            long n = strtol(ucName.substr(2).c_str(), &last, 10);
            if ((*last == '\0') && (n >= 1) && n <= 10) return ucName;
        }
        if (ucName.substr(0, 2) == "ME") { // OSF. Melco vendor area
            long n = strtol(ucName.substr(2).c_str(), &last, 10);
            if ((*last == '\0') && (n >= 1) && (n <= 5)) return ucName;
        }
        if (ucName.substr(0, 2) == "VX") { // OSF. Vertex vendor area
            long n = strtol(ucName.substr(2).c_str(), &last, 10);
            if ((*last == '\0') && (n >= 1) && (n <= 3)) return ucName;
        }
        if (ucName.substr(0, 2) == "AE") { // OSF. AEM vendor area
            long n = strtol(ucName.substr(2).c_str(), &last, 10);
            if ((*last == '\0') && (n >= 1) && (n <= 8)) return ucName;
        }
        if (ucName[0] == 'A') { // AOS. Inner array
            long n = strtol(ucName.substr(1).c_str(), &last, 10);
            if ((*last == '\0') && (n >= 1) && (n <= 138)) return ucName;
        }
        if (ucName[0] == 'W') { // AOS. Western arm
            long n = strtol(ucName.substr(1).c_str(), &last, 10);
            if ((*last == '\0') && (n >= 201) && (n <= 210)) return ucName;
        }
        if (ucName[0] == 'S') { // AOS. Southern arm
            long n = strtol(ucName.substr(1).c_str(), &last, 10);
            if ((*last == '\0') && (n >= 301) && (n <= 309)) return ucName;
        }
        if (ucName[0] == 'P') { // AOS. Pampa La Bola arm
            long n = strtol(ucName.substr(1).c_str(), &last, 10);
            if ((*last == '\0') && (n >= 401) && (n <= 413)) return ucName;
        }
        if (ucName[0] == 'J') { // AOS. ACA configuration
            long n = strtol(ucName.substr(1).c_str(), &last, 10);
            if ((*last == '\0') && (n >= 501) && (n <= 512)) return ucName;
        }
        if (ucName[0] == 'N') { // AOS. North/South ACA extension
            long n = strtol(ucName.substr(1).c_str(), &last, 10);
            if ((*last == '\0') && (n >= 601) && (n <= 606)) return ucName;
        }
        if (ucName[0] == 'T') { // AOS. Total Power pads
            long n = strtol(ucName.substr(1).c_str(), &last, 10);
            if ((*last == '\0') && (n >= 701) && (n <= 704)) return ucName;
        }

    }
    IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
    string msg("The pad called ");
    msg +=  padName + " is not a known pad name.";
    msg += " Known pad names at the OSF are 'TF1' to 'TF10'";
    msg += ", 'ME1' to 'ME5', 'VX1' to 'VX2', 'AE1' to 'AE8'";
    msg += " and at the AOS are 'A1' to 'A138', 'W201' to 'W210'";
    msg += ", 'S301' to 'S309', 'P401' to 'P413', 'J501 to J512'";
    msg += ", 'N601' to 'N606', 'T701' to 'T704'.";
    ex.addData("Detail", msg);
    ex.log();
    throw ex.getIllegalParameterErrorEx();
}

bool WeatherStationControllerImpl::isValid() {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    //if (ws_m.isNil()) return false;
    //const string status = ws_m->getStatus();
    //if (status != string("up")) return false;
    //    Note that (ws_m->getStatus() != "up") always returns true as it
    //    compares character pointers rather than the contents of the strings.


    return true;
}

char* WeatherStationControllerImpl::getWeatherStationAssociatedAtPad(const char* padName) {
    AUTO_TRACE(__PRETTY_FUNCTION__);

    const string wsn = padNameToWeatherStation(padName);
    CORBA::String_var retVal(wsn.c_str());
    return retVal._retn();
}

void WeatherStationControllerImpl::reCalculatePriorityList() {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    GeneratePad2WeatherStationMap();
    for(map<string, PadInfo*>::iterator it=pad2wsMap_m.begin(); it!=pad2wsMap_m.end(); ++it) {
	    cout << "Calculation for pad '" << (*it).first << "'" << endl;
            for(list<PriorityList*>::iterator lit=(*it).second->wsList.begin(); lit!=(*it).second->wsList.end(); ++lit) {
                string isValidstr="False";
                if ((*lit)->isValid)
                    isValidstr="True";
                ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                   (LM_DEBUG, "  Weather Station : '%s', isValid=%s", (*lit)->wsName.c_str(), isValidstr.c_str()));
                //cout << " WS:" << (*lit)->wsName << ", isValid=" << (*lit)->isValid << endl;
            }
    }
}

//-------------------------------------------------------------------------
// MACI DLL support functions
//-------------------------------------------------------------------------
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(WeatherStationControllerImpl)
