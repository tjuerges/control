#include <WeatherPriorityList.h>

using namespace std;


PriorityList::PriorityList() { 
	isValid = false;
};

PriorityList& PriorityList::operator=(const PriorityList& list) {
	this->wsName = list.wsName;
	this->distance = list.distance;
	return *this;
};

int PriorityList::operator<(const PriorityList& list) const {
	if( this->distance < list.distance ) 
		return 1;
	else
		return 0;
}

int PriorityList::operator==(const PriorityList& list) const {
	if( this->distance == list.distance ) 
		return 1;
	else 
		return 0;
}


PadInfo::PadInfo() {
}

PadInfo& PadInfo::operator=(const PadInfo&) {
	return *this;
};



bool PriorityList::comp_distance(PriorityList* first, PriorityList* second) {
	if(first->distance < second->distance) return true;
	else return false;
}

bool PriorityList::comp_max_allowed_distance(PriorityList* ws) {
	if(ws->distance >= 1500.0) return true;
	else return false;
}

