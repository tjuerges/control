#!/usr/bin/env python
#
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# $Id$
#


import sys
import unittest
import Acspy.Clients.SimpleClient
import Acspy.Common.QoS
import Acspy.Util.ACSCorba
import Control


# **************************************************
# automated test class
# **************************************************
class automated(unittest.TestCase):
    def setUp(self):
        Acspy.Common.QoS.setObjectTimeout(Acspy.Util.ACSCorba.getManager(), 20000)
        self.client = Acspy.Clients.SimpleClient.PySimpleClient.getInstance()
        self.aosTiming = self.client.getComponent ('CONTROL/AOSTiming');

    def tearDown(self):
        self.client.releaseComponent(self.aosTiming._get_name())
        self.client.disconnect()
        del(self.client)
     
    def test01(self):
        '''
        Bring the controller to operational state and back to shut down state. 
        '''
        self.aosTiming.controllerOperational()
        self.aosTiming.controllerShutdown()

    def test02(self):
        '''
        Execute the synchroniseMasterClock() method.
        '''
        self.aosTiming.controllerOperational()
        self.aosTiming.synchroniseMasterClock()
        self.aosTiming.controllerShutdown()


# **************************************************
# MAIN Program
# **************************************************
if __name__ == '__main__':
    if len(sys.argv) == 1:
        sys.argv.append("automated")

    unittest.main()
