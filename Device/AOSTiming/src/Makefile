#******************************************************************************
# @(#) $Id$
#
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
#******************************************************************************

#
# Libraries (public and local)
# ----------------------------
LIBRARIES = AOSTiming
LIBRARIES += AOSTimingSim

#
# Dependencies of AOSTiming{,Stubs} libraries
AOSTiming_OBJECTS = AOSTimingImpl
AOSTiming_OBJECTS += resynchroniseHardware
AOSTiming_LIBS = hardwareController
AOSTiming_LIBS += AOSTimingStubs
AOSTiming_LIBS += MasterClockStubs
AOSTiming_LIBS += ControlTimeSourceInterfacesStubs
AOSTiming_LIBS += GPSStubs
AOSTiming_LIBS += CRDStubs
AOSTiming_LIBS += ambManagerStubs
AOSTiming_LIBS += rtlogStubs
AOSTiming_LIBS += rtlogErrType
AOSTiming_LIBS += lkmLoaderStubs
AOSTiming_LIBS += lkmErrType
AOSTiming_LIBS += maciErrType
AOSTiming_LIBS += ACSErrTypeCppNative

AOSTimingSim_OBJECTS = AOSTimingSimImpl
AOSTimingSim_LIBS = AOSTiming

# 
# IDL Files and flags
# 
IDL_FILES = AOSTiming
AOSTimingStubs_LIBS = HardwareControllerStubs
AOSTimingStubs_LIBS += ControlExceptions

#
# Python stuff
#
PY_PACKAGES = CCL


#
# list of all possible C-sources (used to create automatic dependencies)
# ------------------------------
CSOURCENAMES = \
	$(foreach exe, $(EXECUTABLES) $(EXECUTABLES_L), $($(exe)_OBJECTS)) \
	$(foreach rtos, $(RTAI_MODULES) , $($(rtos)_OBJECTS)) \
	$(foreach lib, $(LIBRARIES) $(LIBRARIES_L), $($(lib)_OBJECTS))

#
#>>>>> END OF standard rules

#
# INCLUDE STANDARDS
# -----------------

MAKEDIRTMP := $(shell searchFile include/acsMakefile)
ifneq ($(MAKEDIRTMP),\#error\#)
   MAKEDIR := $(MAKEDIRTMP)/include
   include $(MAKEDIR)/acsMakefile
endif

#
# TARGETS
# -------
all: do_all
	@echo " . . . 'all' done"

clean: clean_all
	$(RM) *~ ../include/*~ ../idl/*~ ../*~ core
# The following directory is created by 'make all' and contains
# temporary files. make clean should delete it but, at the
# moment does not. There is an accepted SPR on this topic. Until
# then do it by hand.
	$(RM) ../lib ../rtai
	@echo " . . . clean done"

clean_dist: clean clean_all clean_dist_all
	@echo " . . . clean_dist done"

install: install_all
	@echo " . . . installation done"
