//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  May 1, 2008  created
//


#include <AOSTimingImpl.h>
#include <MasterClockC.h> // for MasterClock synchronisation.
#include <string>
// For AUTO_TRACE.
#include <loggingMACROS.h>


///
/// All methods which do the ARTM resynchronisation with the TE signal are
/// implemented here.


void Control::AOSTimingImpl::synchroniseMasterClock()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        synchMasterClock();
    }
    catch(const ControlExceptions::MasterClockErrorExImpl& ex)
    {
        throw ControlExceptions::MasterClockErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getMasterClockErrorEx();
    }
}

void Control::AOSTimingImpl::synchMasterClock()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

///
/// TODO
/// Thomas, Jun 26, 2008
/// The MasterClock cannot be instanciated as a subdevice. Needs
/// to be investigated!
//    std::string lruType("MasterClock");
//
//    std::map< std::string, Control::ControlDevice* >::iterator iter(
//        subdevice_m.find(lruType));
//    if(iter == subdevice_m.end())
//    {
//        ACS_LOG(LM_SOURCE_INFO, fnName,
//            (LM_DEBUG, "No %s component configured. Not resynchronising.",
//            lruType.c_str()));
//        return;
//    }
//
//    try
//    {
//        Control::MasterClock_var lruRef(Control::MasterClock::_narrow(
//            (*iter).second));
//        if(CORBA::is_nil(lruRef.in()) == true)
//        {
//            std::ostringstream output;
//            output.str("Could not narrow the ");
//            output << lruType
//                << " component.";
//            ControlExceptions::MasterClockErrorExImpl ex(__FILE__, __LINE__,
//                fnName.c_str());
//            ex.addData("Detail", output.str().c_str());
//            throw ex.getMasterClockErrorEx();
//        }
//
//        lruRef->syncArrayTime();
//    }

    try
    {
        if(masterClockRef_sp.isNil() == true)
        {
            ControlExceptions::MasterClockErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            ex.addData("Detail", "The MasterClock component is not available. "
                "No time synchronisation is possible.");
            ex.log();
            throw ex.getMasterClockErrorEx();
        }

        masterClockRef_sp->syncArrayTime();
    }
    catch(const ControlExceptions::MasterClockErrorEx& ex)
    {
        ControlExceptions::MasterClockErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Could not synchronise the MasterClock.");
        throw nex;
    }
    catch(...)
    {
        ControlExceptions::MasterClockErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Caught an unexpected exception. The Master "
            "Clock has not been synchronised!");
        ex.log();
        throw ex;
    }
}
