#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# tjuerges  Jul 25, 2008  created
# nsaez     Jun 13, 2009  getSubdeviceList added


import CCL.logging
import CCL.HardwareController
import CCLExceptionsImpl


class AOSTiming(CCL.HardwareController.HardwareController):
    def __init__(self, stickyFlag = False):
        '''
        The constructor creates a CCL.AOSTiming object. Which communicates
        with the AOSTiming component in the ARTM computer.
        '''
        CCL.HardwareController.HardwareController.__init__(
           self, "CONTROL/AOSTiming", stickyFlag)

        self.__logger = CCL.logging.getLogger()
        self.__logger.logDebug("Creating AOSTiming CCL Object.")

    def __del__(self):
        CCL.HardwareController.HardwareController.__del__(self)

    def synchroniseMasterClock(self):
        '''
        Synchronise the ARTM computer clock with the GPS clock to UTC time. Then
        update T0 in the teHandler kernel module. 
        '''
        self._HardwareController__controller.synchroniseMasterClock()

    def isAmbManagerRunning(self):
        '''
        Check if the AmbManager component for AOSTiming instance is up and
        running.
        '''
        return self._HardwareController__controller.isAmbManagerRunning()

    def startAmbManager(self):
        '''
        Start the AmbManager component for the AOSTiming rack. If the AOSTiming
        is not in controllerOperational state, the start will fail with an
        exception.
        '''
        self._HardwareController__controller.startAmbManager()

    def stopAmbManager(self):
        '''
        Stop the AmbManager component.
        '''
        self._HardwareController__controller.stopAmbManager()

    def getSubdeviceList(self):
        '''
        Return a list of all devices installed at Antenna
        '''
        device_list = []
        device_map = self._HardwareController__controller.getSubdeviceList()
        for device in device_map:
            dev = str(device)
            device_list.append(dev.split('\'')[1])
            print dev.split('\'')[1]
        return device_list

