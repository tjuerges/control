//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Apr 5, 2008  created
//


#include <AOSTimingImpl.h>

// For std::ostringstream.
#include <sstream>

// For std::string.
#include <string>

// For the std::find algorithm.
#include <algorithm>

// For the accumulate algorithm.
#include <numeric>

// For the Control exceptions.
#include <ControlExceptions.h>

// For ACSErrTypeCppNative::ACSErrTypeCppNativeExImpl.
#include <ACSErrTypeCppNative.h>

// For maciErrType::CannotGetComponentExImpl
#include <maciErrType.h>

// For AUTO_TRACE.
#include <loggingMACROS.h>

// For the container services.
#include <maciContainerServices.h>

// For the permanently loaded components
#include <lkmLoaderC.h>
#include <rtlogC.h>
#include <MasterClockC.h>
#include <ControlTimeSourceInterfacesC.h>
#include <ambManagerC.h>

// For the mutex which protects all AmbManager related.
#include <Guard_T.h>


Control::AOSTimingImpl::AOSTimingImpl(const ACE_CString& name,
    maci::ContainerServices* _cs):
    Control::HardwareControllerImpl(name, _cs),
    cs(_cs),
    myName(name.c_str())
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    initErrorMap();
}

void Control::AOSTimingImpl::initErrorMap()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        // Set up the map of possible error messages.
        std::string errorString("The lkmLoader component "
            "(CONTROL/AOSTiming/lkmLoader) could not be instanciated. This is "
            "a fatal error! Please check: "
            "1. The CDB entry for the lkm file points to a valid and "
            "existing file. "
            "2. No kernel modules have been loaded before. "
            "3. The real-time computer did not suffer from a kernel Oops.\n");
        errorMessages.insert(std::make_pair(
            "CONTROL/AOSTiming/lkmLoader", errorString));

        errorString = "The rtlog component (CONTROL/AOSTiming/rtlog) could not "
            "be instanciated. This is not a fatal error but real time logs "
            "will not be visible for this computer!\n";
        errorMessages.insert(std::make_pair("CONTROL/AOSTiming/rtlog",
            errorString));

        errorString = "The MasterClock component "
            "(CONTROL/AOSTiming/MasterClock) could not be instanciated. Time "
            "synchronisation will not be possible.\n";
        errorMessages.insert(std::make_pair("CONTROL/AOSTiming/MasterClock",
            errorString));

        errorString = "The TimeSource component "
            "(CONTROL/AOSTiming/TimeSource) could not be instanciated. Time "
            "synchronisation will not be possible.\n";
        errorMessages.insert(std::make_pair("CONTROL/AOSTiming/TimeSource",
            errorString));
    }
    catch(...)
    {
        ACSErrTypeCppNative::CppAnyExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        const std::string msg("Cannot initialise the error message map. It "
            "will not be possible to retrieve clear text error messages for "
            "this component.");
        ex.addData("Detail", msg);
        ex.log();
    }
}

Control::AOSTimingImpl::~AOSTimingImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

void Control::AOSTimingImpl::releaseAllReferences()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // Stop the AMBManager just in case it has not been stopped yet.  This is
    // a safe guard.
    try
    {
        if(isAmbManagerRunning() == true)
        {
            stopAmbManager();
        }
    }
    catch(...)
    {
        // Catch all exceptions.  There are two exceptions that might be thrown
        // by stopAmbManager, one if the AMBManager is already down, another
        // one if the release of the reference failed.
        // There is nothing I can do about it, so just log it.
        ACSErrTypeCppNative::CppAnyExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        const std::string msg("Could not release the ambMananger reference on "
            "\"ARTM\".");
        ex.addData("Detail", msg);
        ex.log();
    }

    // Release all references.
    ACE_Guard< ACE_Mutex > guard(referencesMutex);
    timeSourceRef_sp.release();
    masterClockRef_sp.release();
    ambManagerRef_sp.release();
    rtLogRef_sp.release();
    lkmLoaderRef_sp.release();
}

/// Sole purpose: release component references.
void Control::AOSTimingImpl::cleanUp()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // Try to stop the AMBManager.
    try
    {
        if(isAmbManagerRunning() == true)
        {
            stopAmbManager();
        }
    }
    catch(...)
    {
        // Catch all exceptions.  There are two exceptions that might be thrown
        // by stopAmbManager, one if the AMBManager is already down, another
        // one if the release of the reference failed.
        // There is nothing I can do about it, so just log it.
        ACSErrTypeCppNative::CppAnyExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        const std::string msg("Could not release the ambMananger reference on "
            "\"ARTM\".");
        ex.addData("Detail", msg);
        ex.log();
    }

    ACE_Guard< ACE_Mutex > guard(referencesMutex);
    timeSourceRef_sp.release();
    masterClockRef_sp.release();
    // Release the lock so that functions called from releaseAllReferences can
    // acquire it again.
    guard.release();

    Control::HardwareControllerImpl::cleanUp();

    releaseAllReferences();
}

void Control::AOSTimingImpl::controllerShutdown()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // Try to stop the AMBManager.
    try
    {
        if(isAmbManagerRunning() == true)
        {
            stopAmbManager();
        }
    }
    catch(...)
    {
        // Catch all exceptions.  There are two exceptions that might be thrown
        // by stopAmbManager, one if the AMBManager is already down, another
        // one if the release of the reference failed.
        // There is nothing I can do about it, so just log it.
        ACSErrTypeCppNative::CppAnyExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        const std::string msg("Could not release the ambMananger reference on "
            "\"ARTM\".");
        ex.addData("Detail", msg);
        ex.log();
    }

    ACE_Guard< ACE_Mutex > guard(referencesMutex);
    timeSourceRef_sp.release();
    masterClockRef_sp.release();
    // Release the lock so that functions called from releaseAllReferences can
    // acquire it again.
    guard.release();

    Control::HardwareControllerImpl::controllerShutdown();

    releaseAllReferences();
}

void Control::AOSTimingImpl::controllerOperational()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // Flag which helps to keep track if the ARTM loaded the kernel
    // modules properly.
    bool noArtmFlag(true);

    // 1. Start things here on ARTM. If now the start up of the lkmLoader
    //    fails, this controller will be put into Shutdown state.
    // 2. Sync the TE.
    // 3. Get a TimeSource reference.

    ACE_Guard< ACE_Mutex > guard(referencesMutex);
    try
    {
        // If the lkmloader component cannot be loaded, an exception will
        // be thrown and the antenna state be set to Shutdown. Also the
        // error state will be set.
        // If any other component could not be loaded, it will not be fatal.
        getInitComponents();
        noArtmFlag = false;
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        // There was a problem sync'ing the MasterClock component.
        setControllerState(Control::HardwareController::Shutdown);

        ControlExceptions::LKMErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Could not load the LKMs for the AOSTiming "
            "component.");
        nex.log();
    }

    // Let the controller bring the subdevices up.
    Control::HardwareControllerImpl::controllerOperational();

    // Only synchronise the MasterClock if the loading of the kernel modules
    // was successful.
    if(noArtmFlag == false)
    {
        try
        {
            getComponentReferencesBeforMasterClockSync();
            synchMasterClock();
        }
        catch(const maciErrType::CannotGetComponentExImpl& ex)
        {
            setControllerState(Control::HardwareController::Degraded);
        }
        catch(const ControlExceptions::MasterClockErrorExImpl& ex)
        {
            // There was a problem sync'ing the MasterClock component set the
            // controller state to degraded if not already in shutdown.
            if(getState() != Control::HardwareController::Shutdown)
            {
                setControllerState(Control::HardwareController::Degraded);
            }
        }
    }

    // Clean up the component references which were used for the MasterClock
    // sync.
    gpsRef = Control::GPS::_nil();
    crdRef = Control::CRD::_nil();

    // Now get the TimeSource reference so that the clients (DMC*, LMC,
    // Antenna*) can access it via non-sticky references.
    try
    {
        activateTimeSourceComponent();
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        setControllerState(Control::HardwareController::Degraded);

        maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Could not get a TimeSource reference. "
            "TimeSource clients will not be able to connect.");
        nex.log();
    }
}

void Control::AOSTimingImpl::getComponentReferencesBeforMasterClockSync()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    std::string lruType("GPS");

    std::map< std::string, Control::ControlDevice* >::iterator iter(
        subdevice_m.find(lruType));
    if(iter == subdevice_m.end())
    {
        maciErrType::CannotGetComponentExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "No "
            << lruType
            << " component configured. Not resynchronising.";
        ex.addData("Detail", output.str());
        throw ex;
    }
    else if(CORBA::is_nil((*iter).second) == true)
    {
        maciErrType::CannotGetComponentExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "The device which is configured as "
            << lruType
            << " is not narrowable to a GPS. Not resynchronising.";
        ex.addData("Detail", output.str());
        throw ex;
    }

    gpsRef = Control::GPS::_narrow((*iter).second);
    if(CORBA::is_nil(gpsRef) == true)
    {
        std::ostringstream output;
        output << "Could not narrow the "
            << lruType
            << " component. Not resynchronising it.";
        maciErrType::CannotGetComponentExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", output.str());
        throw ex;
    }

    try
    {
        // Check if the GPS component is in the right hwState (hwOperational).
        if(gpsRef->getHwState() != Control::HardwareDevice::Operational)
        {
            maciErrType::CannotGetComponentExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            ex.addData("Detail", "GPS component is not in operational state.");
            throw ex;
        }
    }
    catch(const ControlDeviceExceptions::HwLifecycleEx& ex)
    {
        maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Could not bring the GPS component to the "
            "required hardware state. The clock will not be synchronised!");
        throw nex;
    }

    lruType = "CRD";
    iter = subdevice_m.find(lruType);
    if(iter == subdevice_m.end())
    {
        maciErrType::CannotGetComponentExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "No "
            << lruType
            << " component configured. Not resynchronising.";
        ex.addData("Detail", output.str());
        throw ex;
    }
    else if(CORBA::is_nil((*iter).second) == true)
    {
        maciErrType::CannotGetComponentExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "The device which is configured as "
            << lruType
            << " is not narrowable to a CRD. Not resynchronising.";
        ex.addData("Detail", output.str());
        throw ex;
    }

    crdRef = Control::CRD::_narrow((*iter).second);
    if(CORBA::is_nil(crdRef) == true)
    {
        std::ostringstream output;
        output << "Could not narrow the "
            << lruType
            << " component. Not resynchronising it.";
        maciErrType::CannotGetComponentExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", output.str());
        throw ex;
    }

    try
    {
        // Check if the CRD component is in the right hwState (hwOperational).
        if(crdRef->getHwState() != Control::HardwareDevice::Operational)
    {
        maciErrType::CannotGetComponentExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
            ex.addData("Detail", "CRD component is not in operational state.");
        throw ex;
    }
}
            catch(const ControlDeviceExceptions::HwLifecycleEx& ex)
            {
        maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Could not bring the CRD component to the "
            "required hardware state. The clock will not be synchronised!");
        throw nex;
    }
}

void Control::AOSTimingImpl::getInitComponents()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    const std::string aosTimingName("CONTROL/AOSTiming/");
    std::string componentName(aosTimingName);
    componentName += "lkmLoader";

    try
    {
        // Get Reference to the lkmLoader component.
        lkmLoaderRef_sp =
            getComponentReference< ACS_RT::lkmLoader >(componentName);

        // Clear error for this component.
        setErrorState(componentName, false);
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        // It is fatal when the lkmLoader could not be started up.
        // Put this component into Shutdown state and throw an exception.
        setErrorState(componentName, true);
        maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", errorMessages[componentName]);
        throw nex;
    }

    // Get a reference to the rtlog component.
    componentName = aosTimingName + "rtlog";
    try
    {
        rtLogRef_sp =
            getComponentReference< ACS_RT::rtlog >(componentName);

        // Clear error for this component.
        setErrorState(componentName, false);
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        // It is not fatal when the rtLogger could not be started up.
        // Put this component into degraded state and log the exception.
        setControllerState(Control::HardwareController::Degraded);
        setErrorState(componentName, true);
        maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", errorMessages[componentName]);
        nex.log();
    }

    // Get a reference to the MasterClock component.
    componentName = aosTimingName + "MasterClock";
    try
    {
        masterClockRef_sp = getComponentReference< Control::MasterClock >(
            componentName);

        // Clear error for this component.
        setErrorState(componentName, false);
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        // It is not fatal when the MasterClock could not be started up.
        // Put this component into degraded state and log the exception.
        setControllerState(Control::HardwareController::Degraded);
        setErrorState(componentName, true);
        maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", errorMessages[componentName]);
        nex.log();
    }
}

void Control::AOSTimingImpl::activateTimeSourceComponent()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // Get a reference to the TimeSource component.
    const std::string componentName("CONTROL/AOSTiming/TimeSource");
    try
    {
        timeSourceRef_sp = getComponentReference< Control::TimeSource >(
            componentName);

        // Clear error for this component.
        setErrorState(componentName, false);
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        // It is not fatal when the MasterClock could not be started up.
        // Put this component into degraded state and log the exception.
        setControllerState(Control::HardwareController::Degraded);
        setErrorState(componentName, true);
        maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", errorMessages[componentName]);
        throw nex;
    }
}

void Control::AOSTimingImpl::doStartAmbManager()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());
    const std::string componentCode("ambManagerImpl");
    const std::string componentType("IDL:alma/Control/AmbManager:1.0");
    const std::string componentName(tmpName.in() + std::string("/AmbManager"));
    const std::string containerName("*");

    maci::ComponentSpec spec;
    spec.component_name = componentName.c_str();
    spec.component_code = componentCode.c_str();
    spec.component_type = componentType.c_str();
    spec.container_name = containerName.c_str();

    // Thomas, Jul 23, 2008
    // Okay, okay, I know. Two nested try/catch blocks is way uncool but the
    // C++ standard lacks a catch for collected exceptions.
    try
    {
        try
        {
            ambManagerRef_sp =
                cs->getCollocatedComponentSmartPtr< Control::AmbManager >(
                    spec, false, tmpName.in());
        }
        catch(const maciErrType::NoPermissionExImpl& ex)
        {
            throw maciErrType::CannotGetComponentExImpl(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        }
        catch(const maciErrType::IncompleteComponentSpecExImpl& ex)
        {
            throw maciErrType::CannotGetComponentExImpl(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        }
        catch(const maciErrType::InvalidComponentSpecExImpl& ex)
        {
            throw maciErrType::CannotGetComponentExImpl(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        }
        catch(const
            maciErrType::ComponentSpecIncompatibleWithActiveComponentExImpl& ex)
        {
            throw maciErrType::CannotGetComponentExImpl(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        }
        catch(const maciErrType::CannotGetComponentExImpl& ex)
        {
            maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            nex.addData("Detail", "The AmbManager component could not be "
                "started. Is this AOSTiming instance running on ARTM and are "
                "the real-time Kernel modules loaded?");
            throw nex;
        }
        catch(...)
        {
            maciErrType::CannotGetComponentExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            std::ostringstream msg;
            msg << "An unexpected exception has been caught. "
                "Developers: check the source code of maci::ContainerServices::"
                "getColocatedComponentSmartPtr(...)!";
            ex.addData("Detail", msg.str());
            throw ex;
        }
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        // It is not fatal when the AmbManager component could not be started
        // up. Put this component into degraded state and log the exception.
        setControllerState(Control::HardwareController::Degraded);

        maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream msg;
        msg << "The component called '"
            << componentName
            << "' could not be started. This is not a fatal error. Direct "
            "AMB access on host \"ARTM\" will not be available. Component "
            "name = "
            << componentName
            << ", component code = "
            << componentCode
            << ", component type = "
            << componentType
            << ", container name = "
            << containerName
            << ".";
        nex.addData("Detail", msg.str());
        throw nex;
    }
}

void Control::AOSTimingImpl::startAmbManager()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if((currentState_m != Control::HardwareController::Operational)
    && (currentState_m != Control::HardwareController::Degraded))
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Controller is not in Operational or in Degraded "
            "state.");
        ex.log();
        throw ex.getINACTErrorEx();
    }

    if(isAmbManagerRunning() == true)
    {
        ControlExceptions::DeviceBusyExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "The AmbManager for on \"ARTM\" is already running.";
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getDeviceBusyEx();
    }

    try
    {
        ACE_Guard< ACE_Mutex > guard(referencesMutex);
        doStartAmbManager();
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getInvalidRequestEx();
    }
}

void Control::AOSTimingImpl::stopAmbManager()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(isAmbManagerRunning() == false)
    {
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "The AmbManager on \"ARTM\" is not running.";
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getInvalidRequestEx();
    }

    ACE_Guard< ACE_Mutex > guard(referencesMutex);
    ambManagerRef_sp->disableAccess();
    try
    {
        ambManagerRef_sp.release();
    }
    catch(...)
    {
        ControlExceptions::DeviceBusyExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "The AmbManager \"ARTM\" did not shutdown cleanly. "
            "Developers: this may be an issue in the maci::SmartPtr< T > "
            "class.";
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getDeviceBusyEx();
    }
}

bool Control::AOSTimingImpl::isAmbManagerRunning()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    ACE_Guard< ACE_Mutex > guard(referencesMutex);
    return (!ambManagerRef_sp.isNil());
}

void Control::AOSTimingImpl::setErrorState(const std::string& error,
    const bool errorIsActive)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(errorIsActive == false)
    {
        currentErrors.remove(error);
    }
    else
    {
        currentErrors.push_back(error);
    }

    setError(getErrors());
}


std::string Control::AOSTimingImpl::getErrors()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    std::string errorString;

    for(std::list< std::string >::const_iterator iter(currentErrors.begin());
        iter != currentErrors.end(); ++iter)
    {
        errorString.append(errorMessages[(*iter)]);
    }

    return errorString;
}

#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(Control::AOSTimingImpl)
