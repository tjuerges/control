#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef AOSTimingImpl_h
#define AOSTimingImpl_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Apr 5, 2008  created
//


// Base class.
#include <hardwareControllerImpl.h>

// For error strings, etc.
#include <string>
// A map will contain the error messages and is used to keep track of the
// AmbManager, rtLog and lkmLoader references.
#include <map>
// A std::list contains the current error keys as std::strings which serve as
// keys for the error message map.
#include <list>

// For the valid host names vector.
#include <vector>

// Protection Mutex for the start up and stop of the AmbManager.
#include <Mutex.h>

// CORBA servant header
#include <AOSTimingS.h>

// For the temporary _vars.
#include <GPSC.h>
#include <CRDC.h>

// For maci::SmartPtr
#include <acsComponentSmartPtr.h>

// For maciErrType exceptions.
#include <maciErrType.h>

// For AUTO_TRACE.
#include <loggingMACROS.h>

#include <lkmLoaderC.h>
#include <rtlogC.h>
#include <ambManagerC.h>


// Forward declarations for classes that this component uses.
namespace maci
{
    class ContainerServices;
};


namespace Control
{
    /// Forward declarations for classes that this component uses.
    class TimeSource;
    class MasterClock;


    /// The AOSTiming component is a hardware controller for time keeping
    /// related hardware in the the Central LO rack.
    /// controllerOperational is called in init pass 1 and not in init pass 2
    /// as for the other hardware controllers.
    class AOSTimingImpl: public virtual POA_Control::AOSTiming,
        public Control::HardwareControllerImpl
    {
        public:
        /// The constructor for any ACS C++ component must have this signature.
        AOSTimingImpl(const ACE_CString& name, maci::ContainerServices* cs);

        /// The Destructor must be virtual because this class contains virtual
        /// functions.
        virtual ~AOSTimingImpl();

        /// Load the kernel modules on ARTM, then start the rtlog component on
        /// all of those computers, then synchronise the MasterClock on ARTM.
        virtual void controllerOperational();

        /// Implemented so that all component references (lkmLoad, rtLog,
        /// MasterClock, TimeSource) get released. This avoids unpleasant
        /// results during HardwareController::cleanUp().
        virtual void controllerShutdown();

        /// ACS component lifecycle method. Sole purpose is to release
        /// componet references.
        /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
        virtual void cleanUp();

        /// This method tries to resync the MasterClock and all other LRUs which
        /// need to be informed about a change in the TE phase.
        /// \exception ControlExceptions::MasterClockErrorEx
        virtual void synchroniseMasterClock();

        // Interfaces for AmbManager start, stop and querying if it is
        // running.

        /// Start the AmbManager instance. See AOSTiming.midl for a more
        /// detailed description.
        /// \exception ControlExceptions::InvalidRequestEx
        /// \exception ControlExceptions::DeviceBusyEx
        /// \exception ControlExceptions::INACTErrorEx
        void startAmbManager();

        /// Stop the AmbManager instance. See AOSTiming.midl for a more detailed
        /// description.
        /// \exception ControlExceptions::InvalidRequestEx
        /// \exception ControlExceptions::DeviceBusyEx
        void stopAmbManager();

        /// Query if the AmbManager is up and running. See AOSTiming.midl for a
        /// more detailed description.
        /// \return bool: true if the AmbManager is up and running.
        bool isAmbManagerRunning();


        private:
        /// No default constructor.
        AOSTimingImpl();

        /// No copy constructor.
        AOSTimingImpl(const AOSTimingImpl&);

        /// No assignment operator.
        AOSTimingImpl& operator=(const AOSTimingImpl&);

        /// This method does the dirty work for the Master Clock
        /// synchronisation. It is called from synchroniseMasterClock.
        /// \exception ControlExceptions::MasterClockErrorExImpl
        void synchMasterClock();

        /// This method gets references for lkmLoader rtlog and ArrayTime.
        /// \exception maciErrType::CannotGetComponentExImpl
        void getInitComponents();

        /// Get the CRD and the GPS component references and check if they
        /// are in state hwOperational. The MasterClock component cannot do
        /// this is a safe way: it does not know whether the sync has been
        /// initialised from the controllerOperational method here or from the
        /// oustide via CORBA.
        /// \exception maciErrType::CannotGetComponentExImpl
        void getComponentReferencesBeforMasterClockSync();

        /// Activate (get a sticky reference for) the TimeSource component. If
        /// the TimeSource reference cannot be acquired, an exception will
        /// be thrown.
        /// \exception maciErrType::CannotGetComponentExImpl
        void activateTimeSourceComponent();

        /// Helper method which executes the AmbManager start up on the ARTM.
        /// \exception maciErrType::CannotGetComponentExImpl
        void doStartAmbManager();

        /// Fetch a component reference of type \component. If the component
        /// reference cannot be acquired, an exception will be thrown.
        ///
        /// \param componentName The full name of the component, e.g.
        /// CONTROL/DA41/DTXBBpr2.
        /// \param componentType The CORBA component type, e.g.
        /// Control::MasterClock
        /// \exception maciErrType::CannotGetComponentExImpl
        template< typename componentType >
        maci::SmartPtr< componentType > getComponentReference(
            const std::string& fullName)
        {
            AUTO_TRACE(__PRETTY_FUNCTION__);

            maci::SmartPtr< componentType > reference;
            try
            {
                reference = cs->getComponentSmartPtr< componentType >(
                    fullName.c_str());
            }
            catch(const maciErrType::CannotGetComponentExImpl& ex)
            {
                maciErrType::CannotGetComponentExImpl nex(ex, __FILE__,
                    __LINE__, __PRETTY_FUNCTION__);
                std::ostringstream output;
                output << "Got no reference for the "
                    << fullName
                    << " component.";
                nex.addData("Detail", output.str());
                nex.log();
                throw nex;
            }

            return reference;
        };

        /// Initialises the \ref errorMessages map.
        void initErrorMap();

        /// Releases all ambManager, rtLog and lkmLoader references on ARTM.
        void releaseAllReferences();

        /// Sets the error bit in \ref currentErrors according to
        /// \param component which is the key for the error string in
        /// \ref errorMessages.
        /// \param errorIsActive sets (true) or clears the error (false).
        void setErrorState(const std::string& component,
            const bool errorIsActive);

        /// Returns a \return std::string which contains all current error
        /// messages concatenated.
        std::string getErrors();

        /// List of std::strings reflecting the current error state. The
        /// strings serve as keys for \ref errorMessages.
        std::list< std::string > currentErrors;

        /// Map of error strings. The entries are filled in in the constructor.
        std::map< std::string, std::string > errorMessages;

        /// Keep the container services pointer stored locally.
        maci::ContainerServices* cs;

        /// The name of this component.
        const std::string myName;

        /// Reference for the lkmLoader on ARTM which is always started.
        maci::SmartPtr< ACS_RT::lkmLoader > lkmLoaderRef_sp;

        /// Reference for the rtLog on ARTM which is always started.
        maci::SmartPtr< ACS_RT::rtlog > rtLogRef_sp;

        /// Reference for the AmbManager on ARTM.
        maci::SmartPtr< Control::AmbManager > ambManagerRef_sp;

        /// Reference for the MasterClock on ARTM which is always started.
        maci::SmartPtr< Control::MasterClock > masterClockRef_sp;

        /// Reference for the TimeSource on ARTM which is always started.
        maci::SmartPtr< Control::TimeSource > timeSourceRef_sp;

        /// Reference for the GPS component.
        Control::GPS_ptr gpsRef;

        /// Reference for the CRD component.
        Control::CRD_ptr crdRef;

        /// Access mutex which protects the references.
        ACE_Mutex referencesMutex;
    };
};
#endif
