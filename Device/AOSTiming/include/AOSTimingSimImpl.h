#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef AOSTimingSimImpl_h
#define AOSTimingSimImpl_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  May 16, 2008  created
//


// Base class.
#include <AOSTimingImpl.h>


// Forward declarations for classes that this component uses.
namespace maci
{
    class ContainerServices;
}


namespace Control
{
    class AOSTimingSimImpl: public Control::AOSTimingImpl
    {
        public:
        /// Constructor.
        AOSTimingSimImpl(const ACE_CString& name,
            maci::ContainerServices* containerServices);

        /// Destructor.
        virtual ~AOSTimingSimImpl();

        /// \overload Control::AOSTimingImpl::synchroniseMasterClock
        /// It is possible to run the same test code on the simulated
        /// device but without hardware interaction.
        /// See \ref Control::AOSTimingImpl::synchroniseMasterClock.
        /// \exception ControlExceptions::MasterClockErrorEx
        virtual void synchroniseMasterClock();


        protected:
        /// \overload AOSTimingImpl::controllerOperational
        /// This version
        /// makes sure that a simulated AOSTiming component won't try to load
        /// the lkmLoader or the rtLog components.
        virtual void controllerOperational();


        private:
        /// No default constructor.
        AOSTimingSimImpl();

        /// No copy constructor.
        AOSTimingSimImpl(const AOSTimingSimImpl&);

        /// No assignment operator.
        AOSTimingSimImpl& operator=(const AOSTimingSimImpl&);
    };
};
#endif
