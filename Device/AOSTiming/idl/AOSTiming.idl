#ifndef AOSTIMING_IDL
#define AOSTIMING_IDL
// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2007
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

#include <HardwareController.idl>
#include <ControlExceptions.idl>
#include <rtlogErrType.idl>


#pragma prefix "alma"


module Control
{
    /// This component is analogous to the antenna component except that
    /// is starts up all the central electronics equipment. For the most
    /// part this is the equipment at the Array Operation Site that
    /// maintains observatory wide timing (hence the component name).
    /// This includes:
    /// * GPS
    /// * CRD
    /// * MasterClock (this may be a transient component)
    /// * CVR
    interface AOSTiming: Control::HardwareController
    {
        /// Synchronise the local computer clock with the GPS clock and set
        /// T0 in the teHandler kernel module.
        void synchroniseMasterClock()
            raises(ControlExceptions::MasterClockErrorEx);

        /// Start a AMBManager instance on ARTM.  Only one
        /// instance per host can be active at a time. In case another
        /// instance shall be started ControlExceptions::DeviceBusyEx will be
        /// thrown.  If the controller component is not yet in the
        /// ControllerOperational state, this call will result in a
        /// ControlExceptions::INACTErrorEx exception.  In case of any other
        /// error ControlExceptions::InvalidRequestEx will be thrown.
        void startAmbManager() raises(
            ControlExceptions::InvalidRequestEx,
            ControlExceptions::DeviceBusyEx,
            ControlExceptions::INACTErrorEx);

        /// Stop the currently running AmbManager instance on ARTM.
        /// If no instance is running ControlExceptions::InvalidRequestEx will
        /// be thrown.  If the instance gives problem while shutting it down,
        /// ControlExceptions::DeviceBusyEx will be thrown.
        void stopAmbManager() raises(
            ControlExceptions::InvalidRequestEx,
            ControlExceptions::DeviceBusyEx);

        /// Test if the AOSTiming owns a healthy instance of the AmbManager.
        boolean isAmbManagerRunning();
    };
};
#endif // AOSTIMING_IDL
