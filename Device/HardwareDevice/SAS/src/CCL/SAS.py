#! /usr/bin/env python
# "@(#) $Id$"
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
#
"""
This module is part of the Control Command Language.
It contains complementary functionality for SAS devices.
"""
import CCL.SIConverter
import ControlExceptions
from ControlExceptions import InvalidRequestEx
from ControlCommonCallbacks import SimpleCallbackImpl
import CCL.SASBase
from CCL import StatusHelper


class SAS(CCL.SASBase.SASBase):
    '''
    The SAS class inherits from the code generated SASBase
    class and adds specific methods.
    '''
    def __init__(self, \
                 antennaName = None, \
                 componentName = None, \
                 stickyFlag = False):
        '''
        The constructor creates a SAS object using SASBase constructor.
        EXAMPLE:
        from CCL.SAS import SAS
        obj = SAS("DV01")

        '''
        try:
            CCL.SASBase.SASBase.__init__(self, \
                             antennaName, \
                             componentName, \
                             stickyFlag)
        except Exception, e:
            print 'SAS component is not running \n' + str(e)
            raise e

    def __del__(self):
        CCL.SASBase.SASBase.__del__(self)

    def doPolarizationCalibration(self, forceCalibration = False,
                                  optimizationTimeout = 90.0):
        '''
        This method will optimize both polarization adjusters on the
        SAS if necessary.  If the forceCalibration flag is set to
        True, the optimization is always performed.

        The timeout specified is the timeout for each optimization, since
        there could be 2 optimizations, the maximum elapsed time could be
        as long as twice this timeout.

        This method can only be invoked on a CCL object bound to a
        single device.
        '''
        self.doPol1Calibration(forceCalibration, optimizationTimeout)
        self.doPol2Calibration(forceCalibration, optimizationTimeout)

    def startPol1Calibration(self, forceCalibration = False):
        '''
        This method will Optimize the Polarization adjuster 1 if necessary.
        That is it will set Optimize_POL1 is the POL1_OPTM_NEEDED 
        bit is set.

        If the forceCalibration flag is true then the calibration is
        always performed.

        Because the optimization routines take a long time this is 
        implemented as an asynchronous call.  This method will initialize
        the calibration sequence, use the waitForCalibration method to 
        get notification when the calibration is complete.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].\
                              startPol1Calibration(forceCalibration)
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def doPol1Calibration(self, forceCalibration = False,
                          timeout = 90.0):
        '''
        This method will Optimize the Polarization adjuster 1 if necessary.
        That is it will set Optimize_POL1 is the POL1_OPTM_NEEDED 
        bit is set.

        If the forceCalibration flag is true then the calibration is
        always performed.

        Because the optimization routines take a long time this is 
        implemented as an asynchronous call.  This method will initialize
        the calibration sequence, use the waitForCalibration method to 
        get notification when the calibration is complete.

        This method can only be invoked on a CCL object bound to a
        single device.
        '''
        if len(self._devices) > 1:
            raise InvalidRequestEx("Unable to execute: too many devices"+
                                   " associated with CCL object")

        timeout = CCL.SIConverter.toSeconds(timeout)

        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].\
                              startPol1Calibration(forceCalibration)
            if result[antName]:
                self.waitForCalibration(timeout+1)

    def startPol2Calibration(self, forceCalibration = False):
        '''
        This method will Optimize the Polarization adjuster 2 if necessary.
        That is it will set Optimize_POL2 is the POL1_OPTM_NEEDED 
        bit is set.
        
        If the forceCalibration flag is true then the calibration is
        always performed.

        Because the optimization routines take a long time this is 
        implemented as an asynchronous call.  This method will initialize
        the calibration sequence, use the waitForCalibration method to 
        get notification when the calibration is complete.
        '''
        for key, val in self._devices.iteritems():
            self._devices[key].startPol2Calibration(forceCalibration)
            
    def doPol2Calibration(self, forceCalibration = False,
                          timeout = 90.0):
        '''
        This method will Optimize the Polarization adjuster 1 if necessary.
        That is it will set Optimize_POL1 is the POL1_OPTM_NEEDED 
        bit is set.

        If the forceCalibration flag is true then the calibration is
        always performed.

        Because the optimization routines take a long time this is 
        implemented as an asynchronous call.  This method will initialize
        the calibration sequence, use the waitForCalibration method to 
        get notification when the calibration is complete.

        This method can only be invoked on a CCL object bound to a
        single device.
        '''
        if len(self._devices) > 1:
            raise InvalidRequestEx("Unable to execute: too many devices"+
                                   " associated with CCL object")

        timeout = CCL.SIConverter.toSeconds(timeout)

        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].\
                              startPol2Calibration(forceCalibration)
            if result[antName]:
                self.waitForCalibration(timeout+1)

    def abortPolarizationCalibration(self):
        '''
        This method interrupts any onging calibration routine
        '''
        for key, val in self._devices.iteritems():
            self._devices[key].abortPolarizationCalibration()
            
    def waitForCalibration(self, timeout = 45.0):
        '''
        This method will block until the current calibration is complete
        or the method times out.  The timeout parameter is assumed to
        be in seconds, unless a string is specified which explicitly states
        the units.
        
        Note: This method can only be used if a single SAS is interfaced
        to this CCL object.

        EXAMPLE:
        sas = SAS("DV02")
        sas.startPol1CalibrationIfNeeded()
        sas.waitForCalibration("1 min")
        '''

        if len(self._devices) > 1:
            raise InvalidRequestEx("Unable to execute: too many devices"+
                                   " associated with CCL object")

        timeout = CCL.SIConverter.toSeconds(timeout)
        cb = SimpleCallbackImpl.SimpleCallbackImpl()
        for key, val in self._devices.iteritems():
            self._devices[key].\
                   waitForCalibration(cb.getExternalInterface(),
                                      int(timeout * 1E7))
        cb.waitForCompletion(timeout+1) 

    def getPhotonicReference(self):
        '''
        This method will return the currently used photonic reference
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].getPhotonicReference()
        if len(self._devices) == 1:
            return result.values()[0]
        return result
        
    def setPhotonicReference(self,photonicReferenceName):
        '''
        This method sets the photonic reference port based
        on the name of the photonic reference.  Valid options
        are: PhotonicReference[1-6].  
        
        This method will throw an Illegal Parameter Exception if
        the specified string is not legal.
        '''
        for key, val in self._devices.iteritems():
            self._devices[key].setPhotonicReference(photonicReferenceName)
    
    def STATUS(self):
        '''
        Displays the current status of the
        '''
        result = {}
        elements = []
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[2]
            # GENERAL STATUS 
            try:
                get_status = self._devices[key].GET_ROUTINE_STATUS_GENERAL_STATUS()[0]
                get_pbs = self._devices[key].GET_ROUTINE_STATUS_PBS()[0]
                get_ml_ref = self._devices[key].GET_ROUTINE_STATUS_ML_REF()[0]
                #byte 0 
                #   bit 0 => 1 automated routine active
                #            0 automated routine inactive
                #byte 1
                #   bit 0 => PC1 (PBS) optimization routine status
                #            1 active
                #            0 inactive
                #byte 2
                #   bit 0 => PC2 (ML_REF) optimization routine status
                #            1 optimization checker routine active
                #            0 optimization checker routine inactive
                #
                #Others bits , always zero!
                if(get_status):
                    status = "Active"
                else:
                    status = "Inactive"
                if(get_pbs):
                    pbs_routine = "Active"
                else:
                    pbs_routine ="Inactive"
                if(get_ml_ref):
                    ml_ref_routine = "Active"
                else:
                    ml_ref_routine = "Inactive"
            except:
                status = "N/A"
                pbs_routine = "N/A"
                ml_ref_routine = "N/A"
            # GET_POL1_OPTM_NEED
            try:
                get_pol1_need = self._devices[key].GET_POL1_OPTM_NEEDED_PSB()[0]
                if(get_pol1_need):
                    pol1_need = "YES"
                else:
                    pol1_need = "NO"
            except:
                pol1_need  = "N/A"
            # GET_POL2_OPTM_NEED
            try:
                get_pol2_need = self._devices[key].GET_POL2_OPTM_NEEDED_ML_REF()[0]
                if(get_pol2_need):
                    pol2_need = "YES"
                else:
                    pol2_need = "NO"
            except:
                pol2_need  = "N/A"
            # GET_POL1_OPTM_NEEDED_PEAK_LEVEL
            try:
                pol1_peak = self._devices[key].GET_POL1_OPTM_NEEDED_PEAK_LEVEL()[0]
            except:
                pol1_peak  = "N/A"
            # GET_POL2_OPTM_NEEDED_ML_PEAK_LEVEL
            try:
                pol2_peak = self._devices[key].GET_POL2_OPTM_NEEDED_ML_PEAK_LEVEL()[0]
            except:
                pol2_peak  = "N/A"
            # GET_FRAM_BUFFER
            try:
                fram = self._devices[key].GET_FRAM_BUFFER()[0]
            except:
                fram  ="N/A"
            #GET_POL1_V1
            try:
                pol1_v1 = "%.2f" % self._devices[key].GET_POL1_V1()[0]
            except:
                pol1_v1  = "N/A"
            #GET_POL1_V2
            try:
                pol1_v2 = "%.2f" % self._devices[key].GET_POL1_V2()[0]
            except:
                pol1_v2  ="N/A"
            #GET_POL1_V3
            try:
                pol1_v3 = "%.2f" % self._devices[key].GET_POL1_V3()[0]
            except:
                pol1_v3  = "N/A"
            #GET_POL1_V4
            try:
                pol1_v4 = "%.2f" % self._devices[key].GET_POL1_V4()[0]
            except:
                pol1_v4  = "N/A"
            #GET_POL2_V1
            try:
                pol2_v1 = "%.2f" % self._devices[key].GET_POL2_V1()[0]
            except:
                pol2_v1  = "N/A"
            #GET_POL2_V2
            try:
                pol2_v2 = "%.2f" % self._devices[key].GET_POL2_V2()[0]
            except:
                pol2_v2  = "N/A"
            #GET_POL2_V3
            try:
                pol2_v3 = "%.2f" % self._devices[key].GET_POL2_V3()[0]
            except:
                pol2_v3  = "N/A"
            #GET_POL2_V4
            try:
                pol2_v4 = "%.2f" % self._devices[key].GET_POL2_V4()[0]
            except:
                pol2_v4  = "N/A"
            #GET_POL1_TEMP
            try:
                temp1 = "%.2f" % self._devices[key].GET_POL1_TEMP()[0]
            except:
                temp1  = "N/A"
            #GET_POL2_TEMP
            try:
                temp2 = "%.2f" % self._devices[key].GET_POL2_TEMP()[0]
            except:
                temp2  = "N/A"
            #GET_PSB_OPT_DET (PD1)
            try:
                pd1 = "%.2f" % self._devices[key].GET_PBS_OPT_DET()[0]
            except:
                pd1  = "N/A"
            #GET_BEATNOTE_OPT_DET (PD3)
            try:
                pd3 = "%.2f" % self._devices[key].GET_BEATNOTE_OPT_DET()[0]
            except:
                pd3  = "N/A"
            #GET_RETURN_DET (PD2)
            try:
                pd2 = "%.2f" % self._devices[key].GET_RETURN_DET()[0]
            except:
                pd2  = "N/A"
            #GET_SWITCH_PORT
            try:
                port = self._devices[key].GET_SWITCH_PORT()[0]
            except:
                port  = "N/A"
        #>>>>>>>>>>>>>>>>>> >>>>>>>>>> >>>>>>>>>>>> >>>>>>>>>>
        elements.append( StatusHelper.Separator("General Status"))
        elements.append( StatusHelper.Line("Automated Routine", [StatusHelper.ValueUnit(status)]))
        elements.append( StatusHelper.Line("Automated PBS Routine", [StatusHelper.ValueUnit(pbs_routine)]))
        elements.append( StatusHelper.Line("Automated ML_REF Routine", [StatusHelper.ValueUnit(ml_ref_routine)]))
        
        values = []
        values.append(StatusHelper.ValueUnit(pol1_need, label="Needs to be optimized"))
        values.append(StatusHelper.ValueUnit(pol1_peak , label ="  Value"))
        elements.append( StatusHelper.Line("POL1 PC1(PBS)",values))
        values = []
        values.append(StatusHelper.ValueUnit(pol2_need, label="Needs to be optimized"))
        values.append(StatusHelper.ValueUnit(pol2_peak,label ="  Value"))
        elements.append( StatusHelper.Line("POL2 PC2(ML_REF)",values))
        
        elements.append( StatusHelper.Line("Switch Port", [StatusHelper.ValueUnit(port)]))
        
        values = []
        elements.append( StatusHelper.Separator("Polarization Volts (current)") )
        values.append(StatusHelper.ValueUnit(pol1_v1,"V", label="V1"))
        values.append(StatusHelper.ValueUnit(pol1_v2,"V", label="V2"))
        values.append(StatusHelper.ValueUnit(pol1_v3,"V", label="V3"))
        values.append(StatusHelper.ValueUnit(pol1_v4,"V", label="V4"))
        elements.append(StatusHelper.Line("Polarization 1 ", values))
       
        values = []
        values.append(StatusHelper.ValueUnit(pol2_v1,"V", label="V1"))
        values.append(StatusHelper.ValueUnit(pol2_v2,"V", label="V2"))
        values.append(StatusHelper.ValueUnit(pol2_v3,"V", label="V3"))
        values.append(StatusHelper.ValueUnit(pol2_v4,"V", label="V4"))
        elements.append(StatusHelper.Line("Polarization 2 ", values))

        values = []
        elements.append( StatusHelper.Separator("Polarization Temperatures") )
        values.append(StatusHelper.ValueUnit(temp1,"C", label="Temp 1"))
        elements.append(StatusHelper.Line("Polarization 1 ", values))
        values = []
        values.append(StatusHelper.ValueUnit(temp2,"C", label="Temp 2"))
        elements.append(StatusHelper.Line("Polarization 2 ", values))
        
        values = []
        elements.append( StatusHelper.Separator("Detectors (signal/optical)") )
        values.append(StatusHelper.ValueUnit(pd1,"dBm", label="PBS"))
        values.append(StatusHelper.ValueUnit(pd2,"dBm", label="BeatNote"))
        values.append(StatusHelper.ValueUnit(pd3,"dBm", label="Return"))
        elements.append(StatusHelper.Line("Signal Power",values))

        statusFrame = StatusHelper.Frame(compName + "   Ant: " + antName , elements)
        statusFrame.printScreen()
     
#
# ___oOo___
