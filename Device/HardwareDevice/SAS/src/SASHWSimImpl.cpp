
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File SASHWSimImpl.cpp
 *
 * $Id$
 */

#include "SASHWSimImpl.h"

using namespace AMB;

SASHWSimImpl::SASHWSimImpl(node_t node, const std::vector<CAN::byte_t>& serialNumber)
	: SASHWSimBase::SASHWSimBase(node, serialNumber)
{
    initialize(node, serialNumber);
}

const rca_t SASHWSimImpl::GET_SETUP_INFO = 0x20001U;


void SASHWSimImpl::initialize(node_t node, const std::vector<CAN::byte_t>& serialNumber)
{

	long long raw = 0LL;
	if (state_m.find(GET_SETUP_INFO) == state_m.end()) {
		raw = static_cast<long long>(0);
		    std::vector<CAN::byte_t>* vvalue = new std::vector<CAN::byte_t>();
		    TypeConversion::valueToData(*vvalue, static_cast<unsigned char>(raw), 1);
		state_m.insert(std::make_pair(GET_SETUP_INFO, vvalue));
	}
}

std::vector<CAN::byte_t> SASHWSimImpl::monitor(rca_t rca) const
{
	switch (rca) {
		case GET_SETUP_INFO:
			return getSetupInfo();
			break;
		default:
			return SASHWSimBase::monitor(rca);
	}
}

std::vector<CAN::byte_t> SASHWSimImpl::getSetupInfo() const {
	return *(state_m.find(GET_SETUP_INFO)->second);
}

void SASHWSimImpl::setSetupInfo(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,1,"GET_SETUP_INFO");
	if (state_m.find(GET_SETUP_INFO) != state_m.end())
		*(state_m.find(GET_SETUP_INFO)->second) = data;
	else
		throw CAN::Error("Trying to set GET_SETUP_INFO. Member not found.");
}
