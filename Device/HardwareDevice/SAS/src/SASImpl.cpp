/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * MA 02111-1307  USA
 *
 * File SASImpl.cpp
 *
 * $Id$
 */

#include <SASImpl.h>

#include <TETimeUtil.h>
#include <acstimeTimeUtil.h>
#include <walshFunction.h>
#include <configDataAccess.h>
#include <ControlExceptions.h>

#include <iostream>

using std::string;
using std::ostringstream;

/**
 *-----------------------
 * SAS Constructor
 *-----------------------
 */
SASImpl::SASImpl(const ACE_CString& name,
    maci::ContainerServices* cs):
    SASBase(name,cs)
{
  ACS_TRACE(__func__);
}

/**
 *-----------------------
 * SAS Destructor
 *-----------------------
 */
SASImpl::~SASImpl()
{
  ACS_TRACE(__func__);
}

/* ------------------ CORBA Interface ------------------- */
bool SASImpl::startPol1Calibration(bool forceCalibration) {
  ACS_TRACE(__func__);
  ACS::Time timestamp;

  if (forceCalibration || GET_POL1_OPTM_NEEDED_PSB(timestamp)) {
    SET_OPTIMIZE_POL1(true);
    return true;
  }
  return false;
}

bool SASImpl::startPol2Calibration(bool forceCalibration) {
  ACS_TRACE(__func__);
  ACS::Time timestamp;

  if (forceCalibration || GET_POL2_OPTM_NEEDED_ML_REF(timestamp)) {
    SET_OPTIMIZE_POL2(true);
    return true;
  }
  return false;
}

void SASImpl::abortPolarizationCalibration() {
  ACS_TRACE(__func__);
  ACS::Time timestamp;
  
  if (GET_ROUTINE_STATUS_PBS(timestamp)) {
    SET_OPTIMIZE_POL1(false);
  }

  if (GET_ROUTINE_STATUS_ML_REF(timestamp)) {
    SET_OPTIMIZE_POL2(false);
  }
}

void SASImpl::waitForCalibration(Control::SimpleCallback* cb,
                                 ACS::Time timeout) {
  ACS_TRACE(__func__);
  ACS::Time expireTime = ::getTimeStamp() + timeout;
  timespec sleepTime =  {1, 0}; // Sleep for 1 second between polls

  ACSErr::Completion completion;

  try {
    ACS::Time timestamp;
    GET_ROUTINE_STATUS(timestamp); // before call the dependend monitor GET_ROUTINE_STATUS_GENERAL_STATUS
                          // we call the parent method to make sure the value used is not cached
                          // this is for prevent the that waitForCalibration exist inmediately
                          // after being called

    bool status = GET_ROUTINE_STATUS_GENERAL_STATUS(timestamp);
    while (status && (timestamp < expireTime)) {
      nanosleep(&sleepTime, NULL);
      status = GET_ROUTINE_STATUS_GENERAL_STATUS(timestamp);
    }
    
    if (status) {
      /* Timed Out */
      completion = ControlExceptions::TimeoutCompletion(__FILE__,__LINE__,
                                                        __func__);
    } else {
      /* Success */
      completion = ACSErrTypeOK::ACSErrOKCompletion();
    }
  } catch (ControlExceptions::INACTErrorEx& ex) {
    completion = ControlExceptions::INACTErrorCompletion(ex,__FILE__,__LINE__,
                                                         __func__);
  } catch (ControlExceptions::CAMBErrorEx& ex) {
    completion = ControlExceptions::CAMBErrorCompletion(ex,__FILE__,__LINE__,
                                                        __func__);
  }
  cb -> report(completion); 
}

char* SASImpl::getPhotonicReference(){
  ACS_TRACE(__func__);
  
  ACS::Time timestamp;
  long port = GET_SWITCH_PORT(timestamp);
  CORBA::String_var photonicReferenceString;

  switch (port) {
  case 1:
      photonicReferenceString = CORBA::string_dup("PhotonicReference1");
      break;
  case 2:
      photonicReferenceString = CORBA::string_dup("PhotonicReference2");
      break;
  case 3:
      photonicReferenceString = CORBA::string_dup("PhotonicReference3");
      break;
  case 4:
      photonicReferenceString = CORBA::string_dup("PhotonicReference4");
      break;
  case 5:
      photonicReferenceString = CORBA::string_dup("PhotonicReference5");
      break;
  case 6:
      photonicReferenceString = CORBA::string_dup("PhotonicReference6");
      break;
  default:
    ControlExceptions::HardwareErrorExImpl ex(__FILE__,__LINE__,__func__);
    ostringstream msg;
    msg << "The SAS hardware returned an unrecognized port number: "
        << port;
    ex.addData("ErrorMessage", msg.str().c_str());
    throw ex.getHardwareErrorEx();
  }

  return photonicReferenceString._retn();
}

void SASImpl::setPhotonicReference(const char* photonicReferenceName){
  ACS_TRACE(__func__);
  
  /* I am hardcoding this here for now because it is not clear to me that
     we need this to be flexible.  But we can certainly make this more
     flexible later if we need to */
  if (!strcmp(photonicReferenceName, "PhotonicReference1")) {
    SET_SWITCH_PORT(1);
  } else if (!strcmp(photonicReferenceName, "PhotonicReference2")) {
    SET_SWITCH_PORT(2);
  } else if (!strcmp(photonicReferenceName, "PhotonicReference3")) {
    SET_SWITCH_PORT(3);
  } else if (!strcmp(photonicReferenceName, "PhotonicReference4")) {
    SET_SWITCH_PORT(4);
  } else if (!strcmp(photonicReferenceName, "PhotonicReference5")) {
    SET_SWITCH_PORT(5);
  } else if (!strcmp(photonicReferenceName, "PhotonicReference6")) {
    SET_SWITCH_PORT(6);
  } else {
    ControlExceptions::IllegalParameterErrorExImpl
      ex(__FILE__, __LINE__, __func__);
    ostringstream msg;
    msg << "The reference " << photonicReferenceName << " is not recognized";
    ex.addData("Error Message", msg.str().c_str());
    throw ex.getIllegalParameterErrorEx();
  }
}


//----------------------------------------

/**
 *----------------------------
 * MACI DLL support functions
 *----------------------------
 */
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(SASImpl)
