#!/usr/bin/env python
'''
optimizeSAS check if the SAS of all the antennas configured in the system need pbs(POL1) or ML_REF(POL2) optimizations
and allows to the user to running it if it's necessary. 
'''
import time
from time import sleep

debug=False

if debug: print "imports "+time.ctime()
from threading import Thread

from xml.dom import minidom
from os import environ
from sys import exit
from threading import Thread
from optparse import OptionParser
from threading import Thread

if debug: print "importing CCL.SAS "+time.ctime()
from CCL.SAS import SAS

class SASOptimizer():
    """Class for managing SAS optimizations"""
    def getConfiguredAntennas(self):
        startup_antennas = str(environ['ACSROOT']) + '/config/SIMTMCDB/StartupAntenna-'+str(environ['LOCATION'])+'.xml'
        #antenna parser
        try:
            startupAntennasParse = minidom.parse(startup_antennas)
            antennaList = startupAntennasParse.getElementsByTagName('Antenna')
            antennaNames = []
            for antenna in antennaList:
                antennaNames.append(str(antenna.getAttributeNode('name').value))
        except Exception, ex:
            print "\nError: No antennas configured"
            raise ex
        return antennaNames
        
    def checkSASOptimization(self,antenna):
        pbsOptimizationStatus="N/A"
        mlRefOptimizationStatus="N/A"
        # Obtaining the SAS reference
        try:
            sas=self.sasList[antenna]
        except Exception, ex:
            return [pbsOptimizationStatus, mlRefOptimizationStatus]
        # Reading the polarization routine status
        try:
            pbsStatus = sas.GET_ROUTINE_STATUS_PBS()[0]
            mlRefStatus =sas.GET_ROUTINE_STATUS_ML_REF()[0]
        except Exception, ex:
            return [pbsOptimizationStatus, mlRefOptimizationStatus]
        
        # if the pol1 optimization routine is active, pbsOptimizationStatus = running
        if (pbsStatus):
            pbsOptimizationStatus = 'running'
        else:
            try:
                # if not, check if it's necessary to optimize
                if (sas.GET_POL1_OPTM_NEEDED_PSB()[0]==True): 
                    pbsOptimizationStatus = 'needed'
                else:
                    pbsOptimizationStatus = 'not needed'
            except Exception, ex:
                pbsOptimizationStatus = 'N/A' 
        
        # if the pol2 optimization routine is active, mlRefOptimizationStatus = running
        if (mlRefStatus):
            mlRefOptimizationStatus = 'running'
        else:
            try:
                # if not, check if it's necessary to optimize
                if (sas.GET_POL2_OPTM_NEEDED_ML_REF()[0]==True): 
                    mlRefOptimizationStatus = 'needed'
                else:
                    mlRefOptimizationStatus = 'not needed'
            except Exception, ex:
                mlRefOptimizationStatus = 'N/A' 
        return  [pbsOptimizationStatus, mlRefOptimizationStatus]
    
    def runOptimization(self,ask=False):
        for antenna in self.sasStatus:
            if ask:
                if self.sasStatus[antenna][0]=='needed' or self.sasStatus[antenna][1]=='needed':
                    print antenna+" SAS needs to be optimized. Do you want yo run the optimization now? [Y/N]. Default: N:"
                    res=raw_input()
                    if res != '':
                        if res.upper()[0]!='Y':
                            continue
                    else:
                        continue
            thread = Thread(target=self.optimizeSAS, args=(antenna,self.sasList[antenna],))
            thread.start()
        sleep(2)
        print "waiting for all optimization threads to finish..."
        thread.join()
        print "done"

    def optimizeSAS(self,antenna,sas):
        if self.sasStatus[antenna][0]=='needed':
            print 'Running  polarizer calibration for '+antenna+' SAS'
            sas.doPolarizationCalibration(forceCalibration = True)
        elif self.sasStatus[antenna][1]=='needed':
            print 'Running  ML_REF (POL2) optimization for '+antenna+' SAS'
            sas.doPol2Calibration(forceCalibration = True)
                
    def checkAllOptimizations(self):
        for antenna in self.antennaList:
            self.sasStatus[antenna]=self.checkSASOptimization(antenna)
        
    def printStatus(self):
        # Print the status
        print "\n*************************************************"
        print "* Antenna *   POL1 opt(pbs)  * POL2 opt(ML_REF) *"
        print "*************************************************"
        for antenna in self.sasStatus:
            print "*"+antenna.center(9)+"*"+self.sasStatus[antenna][0].center(18)+"*"+self.sasStatus[antenna][1].center(18)+"*"
        print "*************************************************\n"
            
    def optimize(self,force=False):
        # Ask for optimization
        optimizationNeeded=False
        for antenna in self.sasStatus:
            if self.sasStatus[antenna][0]=='needed' or self.sasStatus[antenna][1]=='needed':
                optimizationNeeded=True
        if force == False:
            if optimizationNeeded:
                print "At least one of the SAS need to be optimized."
                print "Do you want to run the optimization now? [(Y)es/(N)o/(A)sk]. Default: No"
                res=raw_input()
                if res != '':
                    if res.upper()[0]=='A':
                        self.runOptimization(ask=True)
                    elif res.upper()[0]=='Y':
                        self.runOptimization(ask=False)
        else:
            self.runOptimization(ask=False)

    def __init__(self):
        if debug: print "getting configured antennas"+time.ctime()
        self.antennaList=self.getConfiguredAntennas()
        self.sasList={}
        self.sasStatus={}
        for antenna in self.antennaList:
            try:
                if debug: print "got SAS for antenna "+antenna+" "+time.ctime()
                self.sasList[antenna]=SAS(antenna)
            except Exception, ex:
                print "There was an error trying to get a reference to SAS("+antenna+"). Removing it from the list."
                self.antennaList.remove(antenna)

if __name__ == '__main__':
    # Options section
    parser = OptionParser()
    parser.add_option("-f", "--force",dest="force", metavar="Force", default=False, action="store_true",
            help = "If present, the optimization will be done without user confirmation")
    parser.add_option("-c", "--check",dest="check", metavar="Check", default=False, action="store_true",
            help = "If present, the script only check if the optimization status is needed")
    (options, args) = parser.parse_args()

    if debug: print "creating SASOptimizer() object "+time.ctime()
    sasOp=SASOptimizer()
    if debug: print "running checkAllOptimizations() "+time.ctime()
    sasOp.checkAllOptimizations()
    if debug: print "running printStatus() "+time.ctime()
    sasOp.printStatus()

    if options.check == False:
        if debug: print "running optimize() "+time.ctime()
        sasOp.optimize(options.force)
        sasOp.printStatus()

