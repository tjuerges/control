
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 * 
 * File SASHWSimImpl.h
 *
 * $Id$
 */
#ifndef SASHWSimImpl_H
#define SASHWSimImpl_H

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include "SASHWSimBase.h"

namespace AMB
{
  /* Please use this class to implement complex functionality for the
   * SASHWSimBase helper functions. Use AMB::TypeConvertion methods
   * for convenient type convertions. */
  class SASHWSimImpl : public SASHWSimBase
  {
  public :
	SASHWSimImpl(node_t node, const std::vector<CAN::byte_t>& serialNumber);

	static const rca_t GET_SETUP_INFO;
  protected:
        virtual void initialize(node_t node, const std::vector<CAN::byte_t>& serialNumber);
        virtual std::vector<CAN::byte_t> monitor(rca_t rca) const;
        
        virtual std::vector<CAN::byte_t> getSetupInfo() const;

        virtual void setSetupInfo(const std::vector<CAN::byte_t>& data);
	
  }; // class SASHWSimImpl

} // namespace AMB

#endif // SASHWSimImpl_H
