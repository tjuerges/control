#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
#
# @(#) $Id$
#------------------------------------------------------------------------------
'''
DESCRIPTION
The SASTest provides a brief functional test of the
component. It is foreseen to be used only for offline
tests and in simulation mode.
'''

# Import unit test
import unittest

# Import system and OS
import sys
import os

# Import the needed ACS modules
import Acspy.Common.QoS
from Acspy.Clients.SimpleClient import PySimpleClient

# Other imports
from time import sleep

# Exceptions
import ControlDeviceExceptions
import exceptions

# For Control and band enums
import Control
from almaEnumerations_IF_idl import _0_ReceiverBandMod

# For check the routines:
import ambManager
import struct

# Automated test class
class automated( unittest.TestCase ):

    def setUp(self):

        Acspy.Common.QoS.setObjectTimeout(Acspy.Util.ACSCorba.getManager(), \
                                          20000)
        self.client = PySimpleClient.getInstance()
    def tearDown(self):

        self.client.disconnect()
        del(self.client)

    def initSAS(self):
        try:
			self.componentName = "CONTROL/DA41/SAS"
			#print "Instantiating SAS component..."
            # Get the component reference
			self.SAS = self.client.getComponent(self.componentName)
			self.SAS.hwStart()
			self.SAS.hwConfigure()
			self.SAS.hwInitialize()
			self.SAS.hwOperational()
			return 'OK'
        except Exception, e:
			#print "Exception: ", str(e)
			return 'FAIL'
    def test01(self):
        '''
        Component instantiation and intialization test.
        '''
        a = self.initSAS()
        self.assertEqual(a,'OK')
        sleep(1)
        #print "Releasing SAS component..."
        self.client.releaseComponent(self.componentName)

# MAIN Program
if __name__ == '__main__':
    client = PySimpleClient.getInstance()
    if len( sys.argv ) == 1:
        sys.argv.append( "automated" )
        
    unittest.main()
