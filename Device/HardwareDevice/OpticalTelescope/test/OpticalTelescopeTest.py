#!/usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007 - 2009
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import sys
import unittest
import shutil
shutil.rmtree("../lib/python/site-packages", True)
import Acspy.Clients.SimpleClient
import Control
import CCL.OpticalTelescope
import time
import math

# **************************************************
# automated test class
# **************************************************
class automated( unittest.TestCase ):

    def test01( self ):
        '''
        This simple test just loads and unloads the component. It finds
        errors due to missing symbols.
        '''
        client = Acspy.Clients.SimpleClient.PySimpleClient()
        opt = client.getComponent('CONTROL/DA41/OpticalTelescope')
        client.releaseComponent(opt._get_name())

    def test02( self ):
        '''
        This tests the IDL interface.
        '''
        client = Acspy.Clients.SimpleClient.PySimpleClient()
        opt = client.getComponent('CONTROL/DA41/OpticalTelescope')
        try:
            # Firstly bring the component operational
            if (opt.getHwState() != Control.HardwareDevice.Start):
                opt.hwStop()
                opt.hwStart()
            opt.hwConfigure();
            opt.hwInitialize();
            opt.hwOperational();
            opt.SET_DoCamPower(False)
            self.assertFalse(opt.GET_QueryCamPower()[0])
            opt.SET_DoCamPower(True)
            self.assertTrue(opt.GET_QueryCamPower()[0])

            opt.SET_DoFocus(.09)
            #TODO. Investigate the need for this sleep
            time.sleep(6.0)
            self.assertAlmostEquals(opt.GET_QueryFocusPos()[0], 0.09);

            opt.SET_DoExpose([5.0, 1.0]);
            #TODO. Investigate the need for this sleep
            time.sleep(6.0)
            opt.doExposure(5.0);
            #TODO. Investigate the need for this sleep
            time.sleep(6.0)
            opt.getLastStarPosition()
            (xSizeInPix, ySizeInPix) = opt.detectorSize()
            self.assertEqual(xSizeInPix, 1024)
            self.assertEqual(ySizeInPix, 1024)
            (xSizeInRad, ySizeInRad, rot) = opt.fieldOfView();
            self.assertTrue(abs(xSizeInRad) < math.pi/180 )
            self.assertTrue(abs(ySizeInRad) < math.pi/180 )
            self.assertTrue(abs(rot) < 2*math.pi )
            f = opt.observingFrequency();
            self.assertTrue(f < 800E12 and f > 400E12 )
            opt.startContinuousExposure(.1)
            time.sleep(6.0)
        finally:
            client.releaseComponent(opt._get_name())

    def test03( self ):
        '''
        This tests the CCL interface.
        '''
        opt = CCL.OpticalTelescope.OpticalTelescope\
              (componentName='CONTROL/DA41/OpticalTelescope',\
               stickyFlag=True)
        if (opt.getHwState() != Control.HardwareDevice.Start):
            opt.hwStop()
            opt.hwStart()
        opt.hwConfigure();
        opt.hwInitialize();
        opt.hwOperational();
        opt.SET_DoCamPower(False)
        self.assertFalse(opt.GET_QueryCamPower()[0])
        opt.SET_DoCamPower(True)
        self.assertTrue(opt.GET_QueryCamPower()[0])
        opt.doExposure(5.0);

# **************************************************
# MAIN Program
# **************************************************
if __name__ == '__main__':

# Now run the tests
    runner = unittest.TextTestRunner()
    if len( sys.argv ) > 1:
        suite = unittest.makeSuite(automated, sys.argv[1])
    else:
        suite = unittest.makeSuite(automated)
    runner.run(suite)
