// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2010
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#ifndef OPTICALPOSITIONCONSUMER_H
#define OPTICALPOSITIONCONSUMER_H

/// This class attaches to the control real time channel and is a consumer
/// for StarPosition Events. 

#include <vector>
#include <acsncSimpleConsumer.h>
#include <OpticalTelescopeC.h>
#include <pthread.h>
#include <acscommonC.h> // for ACS::Time

class OpticalPositionConsumer :
    public nc::SimpleConsumer<Control::OpticalTelescope::StarPosition>

{
public:

    /// Construct a client that will receive the data coming from the specified
    /// antenna. This is the antenna name e.g., DV01
    OpticalPositionConsumer(const std::string& antennaName);

    /// This disconnects from the notification channel
    virtual void disconnect();

    /// Add the specified data to the internal list. 
    void processData(Control::OpticalTelescope::StarPosition data);

    /// The size of the internal positions list.
    unsigned long size();

    /// Empties the internal list.
    void clear();

    /// Cleares all entries in the internal list before the specified time
    void clear(ACS::Time firstTimeToKeep);

    /// Only accumulate that data with a timestamp that is less than or equal
    /// to the start time and strictly less than the stop time. This function
    /// will clear the existing data buffer before starting data
    /// accumulation. This ensures that the buffer only contains data in the
    /// specified time range. Use a start time of zero to means a long time in
    /// the past and a start time of 0 (or a very big number) to mean a long
    /// time in the future ie., to turn of time range filtering. The caller is
    /// responsible for ensuring that startTime <= stopTime.
    void setTimeRange(ACS::Time startTime, ACS::Time stopTime);

    /// returns true if any data has been received that is equal to or more
    /// recent than the current stop time. Always returns false if the stoptime
    /// is zero.
    bool pastStopTime();

    /// This returns true if any data has been received that is equal to or
    /// more recent than the specified stop time.
    bool pastStopTime(ACS::Time stopTime);

    /// This function returns all the position data within the requested time
    /// range. It will return a zero length vector if there is no data in the
    /// specified time range.
    /// \exception ControlExceptions::IllegalParameterErrorExImpl
    /// If startTime > stopTime
    std::vector<Control::OpticalTelescope::StarPosition> 
    getDataRange(const ACS::Time& startTime, const ACS::Time& stopTime);

    /// Returns a copy of all the data. Its sorted beforehand
    std::vector<Control::OpticalTelescope::StarPosition> getData();

    /// Allows you to get any element of the internal list.
    Control::OpticalTelescope::StarPosition operator[](int);

protected:
    class Compare_StarPosition {
    public:
        int operator()(const Control::OpticalTelescope::StarPosition& a1,
                       const Control::OpticalTelescope::StarPosition& a2) const {
            return(a1.timestamp < a2.timestamp);
        }
    };

    class Compare_StarPosition_AcsTime {
    public:
        int operator()(const Control::OpticalTelescope::StarPosition& a1,
                       const ACS::Time& a2) const {
            return(a1.timestamp < a2);
        }

        int operator()(const ACS::Time& a2,
                       const Control::OpticalTelescope::StarPosition& a1) const {
            return(a2 < a1.timestamp);
        }
    };
    
    void sortData();

    std::vector<Control::OpticalTelescope::StarPosition> dataVector_m;
    pthread_mutex_t dataMtx_m;
    bool sorted_m;
    std::string antennaName_m;
    ACS::Time startTime_m;
    ACS::Time stopTime_m;
    bool pastStopTime_m;
};

// Because SimpleConsumer is not object oriented we need an event handler
// method
void OpticalPositionConsumer_eventHandler(Control::OpticalTelescope::StarPosition data,
                                         void *reference);

#endif // POSITIONSTREAMCONSUMER_H
