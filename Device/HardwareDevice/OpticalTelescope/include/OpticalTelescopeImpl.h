// $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2009, 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA

#ifndef OPTICALTELESCOPEIMPL_H
#define OPTICALTELESCOPEIMPL_H

// Base class(es)
#include <OpticalTelescopeBase.h>

//CORBA servant header
#include <OpticalTelescopeS.h>

//includes for data members
#include <acsThread.h>

// Forward declarations for classes that this component uses
namespace maci
{
    class ContainerServices;
};
namespace nc {
    class SimpleSupplier;
}


namespace Control {
    class OpticalTelescopeImpl: public ::OpticalTelescopeBase,
                                virtual public POA_Control::OpticalTelescope {
    public:
        // ------------------- Constructor & Destructor -------------------

        /// The constructor for any ACS C++ component must have this signature
        OpticalTelescopeImpl(const ACE_CString& name,
                             maci::ContainerServices* cs);

        /// The destructor must be virtual because this class contains virtual
        /// functions.
        virtual ~OpticalTelescopeImpl();

        // --------------------- Component LifeCycle interface -----------

        /// This creates the continuous exposure monitoring thread. Its starts
        /// suspended and is activated by the startContinuousExposure method.
        /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
        virtual void initialize();

        /// This terminates the continuous exposure monitoring thread. In
        /// normal cases it should already have been suspended by the
        /// stopContinuousExposure method.
        /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
        virtual void cleanUp();

        // --------------------- Hardware LifeCycle interface -------------

        /// This loads the plate scale parameters from the TMCDB
        virtual void hwConfigureAction();

        /// Shuts down any continuous exposures.
        virtual void hwStopAction();

        // --------------------- CORBA interface --------------------------

        /// See the IDL file for a description of this function.
        virtual void initializeTelescope();

        /// See the IDL file for a description of this function.
        virtual void shutdownTelescope();

        /// See the IDL file for a description of this function.
        virtual void fieldOfView(double& xSizeInRad, double& ySizeInRad,
                         double& rotation);

        /// See the IDL file for a description of this function.
        virtual void detectorSize(short& xSizeInPix, short& ySizeInPix);

        /// See the IDL file for a description of this function.
        virtual double observingFrequency();

        /// See the IDL file for a description of this function.
        virtual Control::OpticalTelescope::StarPosition getLastStarPosition();
        virtual Control::OpticalTelescope::StarPosition getStarPosition(int sequenceNumber);

        /// See the IDL file for a description of this function.
        virtual Control::OctetSeq* getLastImage();

        /// See the IDL file for a description of this function.
        virtual void saveLastExposure(const char* filename);

        /// See the IDL file for a description of this function.
        virtual bool isShutterOpen();

        /// See the IDL file for a description of this function.
        virtual void openShutter();

        /// See the IDL file for a description of this function.
        virtual void closeShutter();

        /// See the IDL file for a description of this function.
        virtual void dayTimeObserving();

        /// See the IDL file for a description of this function.
        virtual void nightTimeObserving();

        /// See the IDL file for a description of this function.
        virtual Control::OpticalTelescope::StarPosition doExposure(double time);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::INACTErrorEx
        /// \exception ControlExceptions::HardwareErrorEx
        virtual void startContinuousExposure(double exposureTime);

        /// See the IDL file for a description of this function.
        virtual void stopContinuousExposure();

        /// See the IDL file for a description of this function.
        virtual bool doingContinuousExposure();

        /// See the IDL file for a description of this function.
        virtual void setSetflatfield(int flatFieldMode);

        /// See the IDL file for a description of this function.
        virtual void setDostop(bool value);

        /// See the IDL file for a description of this function.
        virtual void setDotelheater(bool onOff);

        /// See the IDL file for a description of this function.
        virtual void setDofocusinit(bool value);

        /// See the IDL file for a description of this function.
        virtual void setDofilterinit(bool value);

        /// See the IDL file for a description of this function.
        virtual void setDologfileflush(bool value);

        /// See the IDL file for a description of this function.
        virtual void setDofocus(double value);

        /// See the IDL file for a description of this function.
        double getQueryfocuspos(ACS::Time& timestamp);

    private:
        /// No default ctor.
        OpticalTelescopeImpl();

        /// No copy-ctor.
        OpticalTelescopeImpl(const OpticalTelescopeImpl&);

        /// No assignment optor.
        OpticalTelescopeImpl& operator=(const OpticalTelescopeImpl&);

        bool isShutterOpen(int retries);
        void waitUntilFilterIsInPosition(int counter, int requestedFilter);
        void throwHardwareErrorEx(ControlExceptions::HardwareErrorExImpl& ex,
                                  const std::string& msg);

        double pixelSize_m;
        double rotation_m;

        /// This thread wakes periodically and publishes the centroids of all
        /// exposures after the one with the specified sequence number
        class ContinuousExposureMonitoringThread : public ACS::Thread {
        public:
            ContinuousExposureMonitoringThread(const ACE_CString& name,
                                               OpticalTelescopeImpl& opt,
                                               const ACS::TimeInterval& responseTime, 
                                               const ACS::TimeInterval& responseTime);
            virtual ~ContinuousExposureMonitoringThread();
            // The thread will start dumping all exposures after the one with
            // the specified sequence number. This function should only be
            // called when this thread is suspended as there is no mutex to
            // prevent concurrent access to the lastSeqId_m data member.
            virtual void setLastExposureNumber(int lastNumber);
            virtual void runLoop();
        private:
            OpticalTelescopeImpl& opt_m;
            int lastSeqId_m;
            nc::SimpleSupplier* publisher_m;
        };

        /// The instance of the thread object
        ContinuousExposureMonitoringThread* cemThread_m;
    };
} // end Control namespace
#endif // OPTICALTELESCOPEIMPL_H
