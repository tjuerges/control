#ifndef parseOpts_h
#define parseOpts_h

#define MAXALG 3		/* Number of centroid algorithms */
#define MAXALG_P 3		/* Number of parameters per centroid algorithm */
#define MAXPIX 1600		/* Number of rows/columns in a full image */
#define MAXFITS MAXPIX*MAXPIX*2+10000 /* Maximum no. of bytes in a FITS image */
#define MAXFITSBUF 5		/* Number of images in FITS buffer */

#define MAXERROR 50		/* Size of Error history */
#define MAXCENTROID 200		/* Number of elements in Centroid buffer */
#define MAXLIST 100		/* Maximum size for <list> in state2num() */

/* Directory containing optsN.config files */
#define OPTS_CONFIG_FILE_DIR "./"	

#define FCTNPAR char *text, struct params keyTable, int npar
struct params {
  const char *keyword;
  int numValue;
  int kind;
  void *var;
  int (*fctn)(FCTNPAR);
  int *list;
};

/* Code numbers for various states reported.  Negatives are states that 
   might be assigned to a parameter that can also have a numerical value >0 
   There must be no gaps in the numbering */
enum state {Pending=-7, All=-6, Started=-5, Done=-4, Aborted=-3, 
	    OK=-2, Bad=-1, Off=0, On, Idle, Exposing, 
	    None, Factory, ALMA, At, MovingTo, Stuck, 
	    Clear, Blocked, IRFilt, OtherFilt, 
	    MovingFwd, MovingBack, Stopped, Limit, Unknown, 
	    Over, Tripped, Ready, None1, Zeroed, 
	    Open, Closed, Opening, Closing, Close, 
            Fault, Add, Remove, EZero, EYes, ENo, Power, Stop};

/* Must be in the same order as enum state */
static const int stateEnum[] = {Pending, All, Started, Done, Aborted, 
	    OK, Bad, Off, On, Idle, Exposing, 
	    None, Factory, ALMA, At, MovingTo, Stuck, 
	    Clear, Blocked, IRFilt, OtherFilt, 
	    MovingFwd, MovingBack, Stopped, Limit, Unknown, 
	    Over, Tripped, Ready, None1, Zeroed, 
	    Open, Closed, Opening, Closing, Close,
	    Fault, Add, Remove, EZero, EYes, ENo, Power, Stop};

/* stateName must be the same order as enum state */
static const char *stateName[] = {"Pending", "All", "started", "Done", "aborted", 
		     "OK", "Bad", "Off", "On", "Idle", "Exposing", 
		     "None", "Factory", "ALMA", "At", "Moving to", "Stuck", 
		     "Clear", "Blocked","IR", "Other", 
		     "Moving +", "Moving -", "Stopped", "Limit", "Unknown", 
		     "Over", "Tripped", "Ready", "(none)", "zeroed", 
		     "Open", "Closed", "Opening", "Closing", "Close", 
		     "Fault", "Add", "Remove", "zero", "Yes", "No", "Power",
		     "Stop"};

#define NUMSTATE (int)(sizeof(stateName)/sizeof(char *))

static char *stateName1[NUMSTATE] = {NULL}; /* Array of first words of stateName */
static char *stateName2[NUMSTATE] = {NULL}; /* Array of remaining words (if any) */

static const char *statusName[] = {"CCD", "CamTemp", "FiltPos", "FocusPos", 
			    "Shutter", "SunSensor", "TelTemp"};

#define NUMSTATUS (int)(sizeof(statusName)/sizeof(char *))

/* Centroid data structure */
struct centroid {
  int SeqNo;
  float Peak;
  float CentX;
  float CentY;
  float WidthX;
  float WidthY;
  float RMS;
  int Valid;
  float ExpTime;
  double TimeStamp;
};

struct parmOpt {
  float rotAngle;	 /* CCW Angle from CCD column to +Elevation direction */
  float plateScale;		/* Arcsec per CCD pixel */
  double padLatitude;		/* Latitude of the antenna Pad (deg) */
  double padLongitude;		/* Longitude of the antenna Pad (deg east) */
  char padName[20];		/* Antenna Pad name */
  char antennaName[20];		/* Antenna Name */
  double sinrot;		/* sin(rotAngle) */
  double cosrot;		/* cos(rotAngle) */
};

/* Markers for the <list> in struct params and state2num().  (Values
 * are arbitrary except they must be less than any values in enum state,
 * optTSTAMP must be the smallest, and optEND must be the greatest)
 */
const int optTSTAMP = -104;    	// Marker for a FITS time stamp
const int optNUM = -103;	// Marker for any number
const int optSEP = -102;	// Marker for separator between parameters
const int optEND = -101;	// Marker for end of list

/* Types for "kind" field of struct "params" */

const int optINT = 1;		// Type int
const int optFLOAT = 2;		// Type float
const int optDOUBLE = 3;	// Type double
const int optCHAR = 4;		// Type char*

int state2num(char *input, void *output, int kind, int max, int *list,
              int *found, char **nextValue);
void errState2num(int ret, char *token, int numPar, char *nextValue);
int processOpt(char *response, int len);
void gotFITS(char *fits, int len, int briefFlag);
int loadConfig(char* optsMB, struct params parmList[], int numParmList);

int isnumber(char *string);
char *getNewCtime(int offset);

#define TempCorr		/* Temperature correction implemented */

struct opts {
  double Bias[2];		/* Bias sequence number and timestamp */
  double Blank[2];		/* Blank sequence number and timestamp */
  int CamPower;			/* Camera Power (On or Off) */
  float CamStat[4];		/* Camera status: state (Off, Idle, Exposing)
				                  total time left (seconds)
						  exposures yet to do next 
						  single-exposure time */
  float CamTemp[2];		/* Actual temperature & set point of camera */
  int CentAlgN;			/* Centroid algorithm # in use */
  float CentAlg[MAXALG][MAXALG_P]; /* Centroid algorithm parameters */
  float CenThresh;		/* Centroid threshold */
  char *EngStatus;		/* Engineering status string <keyword>
				   <value> pairs.  (Unparsed) */
  float ExpTime;		/* Default exposure time (sec) */
  int Fan;			/* Fan Power (On or Off) */
  int FiltPos[2];		/* Filter position <state> & filter codes */
  int FlatField;		/* Flat Field option (None, Factory, ALMA) */
  double FocusInit[2];		/* FocusInit correction & timestamp */
  float  FocusPos[2];		/* Focus position and state (MovingFwd, 
				   MovingBack, Stopped, Limit, Unknown) */
  bool anyHotPix;		/* True if there are any hot pixels */
  int HotPix[MAXPIX][MAXPIX];	/* Non-zero is a Hot Pixel */
  char *IDCamera;		/* Camera ID string */
  char *IDTCM;			/* Telescope control module ID string */
  char *MasterMB;		/* Master mailbox name */
  int SeqNo;			/* Sequence number of the last image */
  int NextSeqNo;		/* Sequence number of the next image 
				   (Usually SeqNo+1) */
  int Shutter[2];		/* Shutter status (Open, Closed, Opening, 
				   Closing, Unknown) and fault (if any) */
  int Slew;			/* Slew status (Yes, No) */
  float Status[NUMSTATUS][2];	/* Status code (OK or Bad) & bad value if any 
				   (for each keyword in statusName[])*/
  int SubImage[4];		/* <lo col>, <lo row>, <hi col>, <hi row> */
  float SunSensor[5];		/* Sun sensors' state (OK or Tripped) 
				   Sun Sensor voltages, failsafe state */
  float SunThresh[2];		/* Sun sensor thresholds */
  int TelHeater;		/* Telescope heater state (On or Off) */
  float TelTemp[2];		/* Telescope temperatures */
  int DoBias;			/* Bias code (Pending, OK, Done, Zeroed) */
  int DoBlank;			/* Blank code (Pending, OK, Done, Zeroed) */
  char *ExpCount;		/* "now n, was m" */
  float Expose[2];		/* Exposure time (or Aborted), no. of exposures */
  int DoExpose;			/* Expose code (Pending, OK, Done) */
  int sendExpose;		/* Count of /Send exposes remaining */
  int Filter;			/* Filter requested (Clear, Blocked, 
				   IRFilt, OtherFilt, Done) */
  int DoFilter;			/* Do Filter state (Pending, Done) */
  int DoFilterInit;		/* FilterInit state (Pending, Started, Done) */
  int FlatLoad;			/* FlatLoad state (OK, Done) */
  float Focus;			/* Focus commanded position */ 
  int DoFocus;			/* Focus state (Pending, Done) */
  int DoFocusInit;		/* FocusInit command state (Pending, OK, Done) */
  int DoShutter;		/* Shutter command state (Pending, OK, Done) */
  int DoSlew;			/* Slew command state (Pending, OK, Done) */
  int ShutUp;			/* ShutUp command ([Pending], Done) */
  /* Temperature compensation variables */
  float DxDt;			/* Centroid drift in X per C */
  float DyDt;			/* Centroid drift in Y per C */
};

#endif /* parseOpts.h */
