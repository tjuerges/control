#ifndef Opts_h
#define Opts_h

#include "EthernetDeviceInterface.h"
#include "parseOpts.h"
#include <boost/shared_array.hpp>

// Will wait up to MS_DOZEMAX milliseconds for images to arrive when fetching
// multiple images.  Keep checking at MS_DOZE intervals for the tardy image
#define MS_DOZEMAX 200
#define MS_DOZE 10

class Opts: public EthernetDeviceInterface  {
public:
  // Enumerations for EthernetDeviceInterface monitor() and command() addresses
  enum optsOps {
    // Query Commands (monitor function only)
    QueryBias,
    QueryBlank,
    QueryCamPower,
    QueryCamStat, 
    QueryCamTemp,
    QueryCentAlg,
    QueryCenThresh, 
    QueryExpTime,
    QueryFan,
    QueryFiltPos,
    QueryFlatField,
    QueryFocusInit,
    QueryFocusPos,
    QueryHotPix,
    QueryIDCamera,
    QueryIDTCM,
    QueryMasterMB,
    QuerySeqNo,
    QueryShutter,
    QueryStatus,
    QuerySunSensor,
    QuerySunThresh,
    QueryTelHeater,
    QueryTelTemp,
    QueryAll,
    // Get Commands (monitor function only)
    GetCentroid,
    GetCentroidN,
    GetCentroidNN,
    // Set commands (command function only)
    SetCamTemp,
    SetCentAlg,
    SetCenThresh,
    SetExpCount,
    SetExpTime,
    SetFlatField,
    SetHotPix,
    SetMasterMB, 
    SetNextSeqNo,
    SetSubImage,
    SetSunThresh,
    SetVerbose,
    SetLogFile,
    // Do Commands (command function only)
    DoBias,
    DoBlank,
    DoCamPower,
    DoExit,
    DoExpose,
    DoFan,
    DoFilter,
    DoFilterInit,
    DoFlatLoad,
    DoFocus,
    DoFocusInit,
    DoLogFileFlush,
    DoShutter,
    DoShutUp,
    DoSlew,
    DoStop,
    DoTelHeater,
    DoVideo
  };

  enum logPriority {INFO, ERROR};


  Opts();
  virtual ~Opts();

  // Open a connection to the OPTS Server
  void open(const std::string& hostname, unsigned short port,
        double timeout, unsigned int lingerTime, unsigned short retries,
	    const std::multimap< std::string, std::string >& parameters);
  // Close the connection to the OPTS server
  int close();

  // monitor functions
  void monitor(int address, bool& value);
  void monitor(int address, char& value);
  void monitor(int address, unsigned char& value);
  void monitor(int address, unsigned short& value);
  void monitor(int address, short& value);
  void monitor(int address, unsigned int& value);
  void monitor(int address, int& value);
  void monitor(int address, unsigned long int& value);
  void monitor(int address, long int& value);
  void monitor(int address, unsigned long long& value);
  void monitor(int address, long long& value);
  void monitor(int address, float& value);
  void monitor(int address, double& value);
  void monitor(int address, long double& value);
  void monitor(int address, std::string& value);
  void monitor(int address, std::vector< bool >& value);
  void monitor(int address, std::vector< unsigned char >& value);
  void monitor(int address, std::vector< char >& value);
  void monitor(int address, std::vector< unsigned short >& value);
  void monitor(int address, std::vector< short >& value);
  void monitor(int address, std::vector< unsigned int >& value);
  void monitor(int address, std::vector< int >& value);
  void monitor(int address, std::vector< unsigned long >& value);
  void monitor(int address, std::vector< long >& value);
  void monitor(int address,
	       std::vector< unsigned long long >& value);
  void monitor(int address, std::vector< long long >& value);
  void monitor(int address, std::vector< float >& value);
  void monitor(int address, std::vector< double >& value);
  void monitor(int address, std::vector< long double >& value);
  void monitor(int address, std::vector< std::string >& value);

  // command functions
  void command(int address);
  void command(int address, bool value);
  void command(int address, unsigned char value);
  void command(int address, char value);
  void command(int address, unsigned short value);
  void command(int address, short value);
  void command(int address, unsigned int value);
  void command(int address, int value);
  void command(int address, unsigned long value);
  void command(int address, long value);
  void command(int address, unsigned long long value);
  void command(int address, long long value);
  void command(int address, float value);
  void command(int address, double value);
  void command(int address, long double value);
  void command(int address, const std::string& value);
  void command(int address, const std::vector< bool >& value);
  void command(int address,
	       const std::vector< unsigned char >& value);
  void command(int address, const std::vector< char >& value);
  void command(int address,
	       const std::vector< unsigned short >& value);
  void command(int address, const std::vector< short >& value);
  void command(int address,
	       const std::vector< unsigned int >& value);
  void command(int address, const std::vector< int >& value);
  void command(int address,
	       const std::vector< unsigned long >& value);
  void command(int address, const std::vector< long >& value);
  void command(int address,
	       const std::vector< unsigned long long >& value);
  void command(int address, const std::vector< long long >& value);
  void command(int address, const std::vector< float >& value);
  void command(int address, const std::vector< double >& value);
  void command(int address,
	       const std::vector< long double >& value);
  void command(int address,
	       const std::vector< std::string >& value);

  /* Get pointer to an Image */
  boost::shared_array<char> getImage(int &imageSize);
  boost::shared_array<char> getImage(int seqNo, int &imageSize);

  /* Get a centroid and the pointer to the corresponding Image */
  boost::shared_array<char> getImageCen(int &imageSize, 
					      struct centroid &Centroid);
  boost::shared_array<char> getImageCen(int seqNo, int &imageSize, 
					      struct centroid &Centroid);

  /* Fetch images seqNo through endSeqNo, return the first one */
  boost::shared_array<char> getImages(int seqNo, int endSeqNo, 
					  int &imageSize);

  /* Get the next image in the getImages list */
  boost::shared_array<char> getNextImage(int &imageSize);

  /* Get the name of the specified enum state value */
  const char *state2Name(int enumState);

  /* Get the enum state value corresponding to the state name */
  int name2State(const char* nameOfState);

  /* Get the name of the Query Status item corresponding to <index> */
  const char *getStatusName(int index);

private:
  struct SOCK *opt;
  float sock_timeout;
  char *buffer;
  int verbose;
  std::string logFileName;		/* Path and name of log file, if any */
  FILE *logFile;
  int nextImageNo, endImageNo, lastFetchedImageNo;
  struct timespec doze;

  void CheckBadMon(int address);	// Non-zero if not a monitor address
  void CheckBadCom(int address); // Non-zero if not a command address

  // Read & parse unread messages, then send the request and parse the response
  int doOptOp(const char *request);  

  void doOptOpOnly(const char *request); // doOptOp, but only sends the request

  // Send DoCommand to OPTS & return centroid and buffer with specifed Image
  boost::shared_array<char> ImagePtr(const char *DoCommand, int &imageSize,
					   struct centroid *Centroid);
  /* Look for image in FITS buffer */
  boost::shared_array<char> haveImage(int seqNo, int &imageSize);

  /* Look for image in FITS buffer and centroid in centroid buffer */
  boost::shared_array<char> haveImageCen(int seqNo, int &imageSize,
					 struct centroid *Centroid);

  // Return index of specified image found in FITS buffer
  int findImage(int seqNo);
  // Return biggest seqNo in range found in FITS buffer
  int lastNo(int firstNo, int endNo); 
  // Return biggest seqNo in range missing from FITS buffer
  int lastMissing(int firstNo, int endNo); 


  /* Put the message into the log file (if any) then throw the exception */
  void throwEthernetDeviceException(const std::string &errorMessage);
  void throwSocketOperationFailedException(const std::string& errorMessage);
  void throwIllegalParameterException(const std::string& errorMessage);  
  void logMsg(const int priority, const char* message);
};

#endif   // Opts_h
