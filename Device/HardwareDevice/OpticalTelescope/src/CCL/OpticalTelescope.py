#! /usr/bin/env python
# @(#) $Id$
#
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2009, 2010
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
"""

This module defines the optical telescope CCL interface. The base
class (OpticalTelescopeBase) contains automatically generated functions
and this file contains the remaining functions.

"""

import CCL.OpticalTelescopeBase

class OpticalTelescope(CCL.OpticalTelescopeBase.OpticalTelescopeBase):
    '''
    '''
    def __init__(self, \
                 antennaName = None, \
                 componentName = None, \
                 stickyFlag = False):
        '''
        '''
        CCL.OpticalTelescopeBase.OpticalTelescopeBase.__init__(\
            self, antennaName, componentName, stickyFlag);

    def __del__(self):
        CCL.OpticalTelescopeBase.OpticalTelescopeBase.__del__(self)

    def initializeTelescope(self):
        '''
        Initialize the optical telescope. This turns the power on and, if
        necessary, initializes the focus & filters. 
        '''
        return self._HardwareDevice__hw.shutdownTelescope();

    def shutdownTelescope(self):
        '''
        Shutdown the optical telescope. This closes the shutter.
        '''
        return self._HardwareDevice__hw.shutdownTelescope();

    def fieldOfView(self):
        '''
        '''
        return self._HardwareDevice__hw.fieldOfView();

    def  detectorSize(self):
        '''
        Return the size of the detector in pixels.
        '''
        return self._HardwareDevice__hw.detectorSize();

    def  observingFrequency(self):
        '''
        Returns the weighted mean observing frequency. This is a
        function of both the detector frequency response and, if the
        IR filter is in use, the the passband of this filter.
        '''
        return self._HardwareDevice__hw.observingFrequency();

    def getLastStarPosition(self):
        '''
        Returns the statistics of the star in the last exposure.
        '''
        return self._HardwareDevice__hw.getLastStarPosition();

    def getStarPosition(self, sequenceNumber):
        '''
        Returns the statistics of the star in the specified exposure.
        '''
        return self._HardwareDevice__hw.getStarPosition(sequenceNumber);

    def doExposure(self, exposureTime):
        '''
        Takes an exposure and returns with its statistics
        '''
        return self._HardwareDevice__hw.doExposure(exposureTime);

    def dayTimeObserving(self):
        '''
        Setup for daytime observing by inserting the IR filter 
        '''
        return self._HardwareDevice__hw.dayTimeObserving();
    
    def nightTimeObserving(self):
        '''
        Setup for nighttime observing by removing the IR filter
        '''
        return self._HardwareDevice__hw.nightTimeObserving();
    
    def isShutterOpen(self):
        '''
        Returns true if the shutter is open and false if its
        closed. If the shutter is moving it waits for a short while
        for the motion to complete. If the position does not resolve
        to open or closed a HardwareError exception is thrown.
        '''
        return self._HardwareDevice__hw.isShutterOpen();

    def openShutter(self):
        '''
        Open the shutter. This function does not wait for it to open.
        '''
        return self._HardwareDevice__hw.openShutter();

    def closeShutter(self):
        '''
        Close the shutter. This function does not wait for it to close.
        '''
        return self._HardwareDevice__hw.closeShutter();

    def saveLastExposure(self, filename):
        '''
        Write a FITS image of the last exposure
        '''
        return self._HardwareDevice__hw.saveLastExposure(filename);

    def startContinuousExposure(self, exposureTime):
        '''
        Take a continuous set of exposures each one taking the
        specified amount of time. The centroids and other relevant
        data of each exposure are put onto the CONTROL_REALTIME
        notification channel and can be accesses accessed using the
        OpticalPositionDump command. Normal exposures cannot be taken
        until the stopContinuousExposure function is executed.
        '''
        return self._HardwareDevice__hw.startContinuousExposure(exposureTime);

    def stopContinuousExposure(self):
        '''
        See the documentation for the startContinuousExposure command.
        '''
        return self._HardwareDevice__hw.stopContinuousExposure();

    def doingContinuousExposure(self):
        '''
        Returns true is we are currently taking continuous exposures.
        '''
        return self._HardwareDevice__hw.doingContinuousExposure();
