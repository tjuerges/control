#include <stdlib.h>
#include <stdio.h>
#include <sstream>
#include <string.h>
#include <math.h>
#include "EthernetDeviceInterface.h"
#include "Opts.h"
#include "sock.h"

extern struct opts optState;	// Current state of OPTS
extern int lastCentroid;		       // index of most recent centroid
extern struct centroid centBuf[MAXCENTROID];   // Circular buffer of centroids
extern int lastError;     	  /* Pointer to oldest error in ErrorHistory */
extern char *ErrorHistory[MAXERROR];   /* List of last MAXERROR errors */
extern char *StopText;                 /* Response to last Stop command */
extern int lastFITS;			// Most recent FITS file in the buffer
extern size_t curImageSize[MAXFITSBUF];	// Total size of a FITS image in the buffer
extern char curImage[MAXFITSBUF][MAXFITS+1]; // Space for MAXFITSBUF images
extern int curSeqNo[MAXFITSBUF]; // Sequence number of a FITS image in the buffer

char *getCtime(int offset);

Opts::Opts()
{
  opt = NULL;
  buffer = new char[MAXFITS+1];
  logFile = NULL;
  verbose = 0;
}

Opts::~Opts()
{
  delete[] buffer;
  this->close();
}

void Opts::open(const std::string& hostname, unsigned short port,
        double timeout, unsigned int lingerTime, unsigned short retries,
        const std::multimap< std::string, std::string >& parameters)
{
  int i, n;

  if (opt != NULL) {
    return;			// OPTS is already open
  }
  n = hostname.size();

  char optsMB[n+1];	// Mailbox name
  strcpy(optsMB, hostname.c_str());
  for (i=0; i<n; i++) {
    optsMB[i] = toupper(optsMB[i]);
  }

  if (timeout < 0 || timeout > 60) {
    char errorMsg[100];
    sprintf(errorMsg, "Opts timeout (%g) is out of range", timeout);
    throwIllegalParameterException(errorMsg);
  }
  sock_timeout = timeout;
  opt = sock_connect(optsMB);
  if (opt == NULL) {
    throwSocketOperationFailedException(
		  std::string("Could not connect to " + std::string(optsMB)));
  }
  sock_bufct(MAXFITS);
  doze.tv_sec = 0;
  doze.tv_nsec = (long)(MS_DOZE*1.e6);	// Set up a sleep time of MS_DOZE ms
}

int Opts::close()
{
  if (opt != NULL) {
    sock_close(sock_name(opt));
    opt = NULL;
  }
  if (logFile != NULL) {
    fclose(logFile);
    logFile = NULL;
  }
  return 0;
}


void Opts::monitor(int address, bool& value)
{
  CheckBadMon(address);
  switch (address) {
  case QueryCamPower:
    doOptOp("Query CamPower");
    value = (optState.CamPower == On);
    break;
  case QueryFan:
    doOptOp("Query Fan");
    value = (optState.Fan == On);
    break;
  case QueryHotPix:
    doOptOp("Query HotPix");
    value = optState.anyHotPix;
    break;
  case QueryStatus:
    doOptOp("Query Status");	// Update Status structure
    value = true;
    break;
  case QuerySunSensor:
    doOptOp("Query SunSensor");
    value = (optState.SunSensor[0] == Tripped ||
	     optState.SunSensor[1] == Tripped);
    break;
  case QueryTelHeater:
    doOptOp("Query TelHeater");
    value = (optState.TelHeater == On);
    break;
  case QueryAll:
    doOptOp("Query");		// Update entire optState structure
    value = true;
    break;
  default:
    {
      std::ostringstream buf;
      buf << "Illegal monitor address " << address << std::endl;
      throwIllegalParameterException(buf.str());
    }
  }
}
void Opts::monitor(int address, char& value) {};
void Opts::monitor(int address, unsigned char& value) {};
void Opts::monitor(int address, unsigned short& value) {};
void Opts::monitor(int address, short& value) {};
void Opts::monitor(int address, unsigned int& value) {};
void Opts::monitor(int address, int& value)
{
  CheckBadMon(address);
  switch (address) {
  case QueryCamPower:
    doOptOp("Query CamPower");
    value = optState.CamPower;
    break;
  case QueryFan:
    doOptOp("Query Fan");
    value = optState.Fan;
    break;
  case QueryFlatField:
    doOptOp("Query FlatField");
    value = optState.FlatField;
    break;
  case QuerySeqNo:
    doOptOp("Query SeqNo");
    value = optState.SeqNo;
    break;
  default:
    {
      std::ostringstream buf;
      buf << "Illegal command address " << address << std::endl;
      throwIllegalParameterException(buf.str());
    }
  }
}
void Opts::monitor(int address, unsigned long int& value) {};
void Opts::monitor(int address, long int& value) {};
void Opts::monitor(int address, unsigned long long& value) {};
void Opts::monitor(int address, long long& value) {};
void Opts::monitor(int address, float& value) {};
void Opts::monitor(int address, double& value)
{
  CheckBadMon(address);
  switch (address) {
  case QueryCenThresh:
    doOptOp("Query CenThresh");
    value = optState.CenThresh;
    break;
  case QueryExpTime:
    doOptOp("Query ExpTime");
    value = optState.ExpTime;
    break;
  case QueryFocusPos:
    doOptOp("Query FocusPos");
    value = optState.FocusPos[0];
    break;
  default:
    {
      std::ostringstream buf;
      buf << "Illegal monitor address " << address << std::endl;
      throwIllegalParameterException(buf.str());
    }
  }
}
void Opts::monitor(int address, long double& value) {};
void Opts::monitor(int address, std::string& value)
{
  CheckBadMon(address);
  switch (address) {
  case QueryIDCamera:
    doOptOp("Query IDCamera");
    if (optState.IDCamera != NULL) {
      value.assign(optState.IDCamera);
    } else {
      value.clear();
    }
    break;
  case QueryIDTCM:
    doOptOp("Query IDTCM");
    if (optState.IDTCM != NULL) {
      value.assign(optState.IDTCM);
    } else {
      value.clear();
    }
    break;
  case QueryMasterMB:
    doOptOp("Query MasterMB");
    if (optState.MasterMB != NULL) {
      value.assign(optState.MasterMB);
    } else {
      value.clear();
    }
    break;
  default:
    {
      std::ostringstream buf;
      buf << "Illegal monitor address " << address << std::endl;
      throwIllegalParameterException(buf.str());
    }
  }
}
void Opts::monitor(int address, std::vector< bool >& value) {};
void Opts::monitor(int address, std::vector< unsigned char >& value) {};
void Opts::monitor(int address, std::vector< char >& value) {};
void Opts::monitor(int address, std::vector< unsigned short >& value) {};
void Opts::monitor(int address, std::vector< short >& value) {};
void Opts::monitor(int address, std::vector< unsigned int >& value) {};
void Opts::monitor(int address, std::vector< int >& value)
{
  int n, i;

  n = value.capacity();
  CheckBadMon(address);
  switch (address) {
  case QueryFiltPos:
    doOptOp("Query FiltPos");
    value.resize(2);
    value[0] = optState.FiltPos[0];
    value[1] = optState.FiltPos[1];
    break;
  case QueryShutter:
    doOptOp("Query Shutter");
    value.resize(2);
    value[0] = optState.Shutter[0];
    value[1] = optState.Shutter[1];
    break;
  case QueryStatus:
    doOptOp("Query Status");
    value.resize(NUMSTATUS);
    for (i=0; i<NUMSTATUS; i++) {
      value[i] = (int)optState.Status[i][0];
    }
    break;
  default:
    {
      std::ostringstream buf;
      buf << "Illegal monitor address " << address << std::endl;
      throwIllegalParameterException(buf.str());
    }
  }
}

void Opts::monitor(int address, std::vector< unsigned long >& value) {};
void Opts::monitor(int address, std::vector< long >& value) {};
void Opts::monitor(int address,
		   std::vector< unsigned long long >& value) {};
void Opts::monitor(int address, std::vector< long long >& value) {};
void Opts::monitor(int address, std::vector< float >& value) {};
void Opts::monitor(int address, std::vector< double >& value)
{
  int i, n, nn, need, ret, savLastCentroid;
  char DoCommand[100], errorMsg[100];
  CheckBadMon(address);
  n = value.capacity();
  nn = 0;
  switch (address) {
  case QueryBias:
    doOptOp("Query Bias");
    if (optState.Bias[0] == Zeroed) {
      n = 1;
    } else {
      n = 2;
    }
    value.resize(n);
    for (i=0; i<n; i++) {
      value[i] = optState.Bias[i];
    }
    break;
  case QueryBlank:
    doOptOp("Query Blank");
    if (optState.Blank[0] == Zeroed) {
      n = 1;
    } else {
      n = 2;
    }
    value.resize(n);
    for (i=0; i<n; i++) {
      value[i] = optState.Blank[i];
    }
    break;
  case QueryCamStat:
    doOptOp("Query CamStat");
    value.resize(4);
    for (i=0; i<4; i++) {
      value[i] = optState.CamStat[i];
    }
    break;
  case QueryCamTemp:
    doOptOp("Query CamTemp");
    value.resize(2);
    for (i=0; i<2; i++) {
      value[i] = optState.CamTemp[i];
    }
    break;
  case QueryCentAlg:
    doOptOp("Query CentAlg");
    value.resize(4);
    n = optState.CentAlgN;	// Active Centroid Algorithm number
    value[0] = n;
    n--;
    for (i=0; i<3; i++) {
      value[i+1] = optState.CentAlg[n][i]; // Put in the paremeters
    }
    break;
  case QueryFocusInit:
    doOptOp("Query FocusInit");
    value.resize(2);
    for (i=0; i<2; i++) {
      value[i] = optState.FocusInit[i];
    }
    break;
  case QueryFocusPos:
    doOptOp("Query FocusPos");
    value.resize(2);
    for (i=0; i<2; i++) {
      value[i] = optState.FocusPos[i];
    }
    break;
  case QuerySunSensor:
    doOptOp("Query SunSensor");
    value.resize(5);
    for (i=0; i<5; i++) {
      value[i] = optState.SunSensor[i];
    }
    break;
  case QuerySunThresh:
    doOptOp("Query SunThresh");
    value.resize(5);
    for (i=0; i<5; i++) {
      value[i] = optState.SunThresh[i];
    }
    break;
  case QueryTelTemp:
    doOptOp("Query TelTemp");
    value.resize(2);
    for (i=0; i<2; i++) {
      value[i] = optState.TelTemp[i];
    }
    break;
  case GetCentroidNN:
    // Get centroids seqNo through endSeqNo
    nn = 1;
  case GetCentroidN:
    // Get Centroid seqNo
  case GetCentroid:
    // Get latest centroid
    need = (int)(10*(value[nn] - value[0] + 1));
    value.resize(need);
    need = 0;
    if (address == GetCentroid) {
      strcpy(DoCommand, "Get Centroid");
    } else {
      for (i=0; i<=nn; i++) {
	n = (int)value[i];
	if (n != value[i] || n <= 0 || n > 999999) {
	  sprintf(errorMsg, "GetCentroidN SeqNo in element[%d] is illegal", i);
	  throwIllegalParameterException(errorMsg);
	}
      }
      if (address == GetCentroidN) {
	sprintf(DoCommand, "Get Centroid %d", n);
      } else {
	nn = (int)value[0];
	if (n < nn) {
	  throwIllegalParameterException("GetCentroidNN endSeqNo < SeqNo");
	}
	sprintf(DoCommand, "Get Centroid %d %d", nn, n);
	need = n - nn;		// Number of centroids yet to be read
      }
    }
    if (doOptOp(DoCommand) == 0) {
      value[0] = centBuf[lastCentroid].SeqNo;
      value[1] = centBuf[lastCentroid].Peak;
      value[2] = centBuf[lastCentroid].CentX;
      value[3] = centBuf[lastCentroid].CentY;
      value[4] = centBuf[lastCentroid].WidthX;
      value[5] = centBuf[lastCentroid].WidthY;
      value[6] = centBuf[lastCentroid].RMS;
      value[7] = centBuf[lastCentroid].Valid;
      value[8] = centBuf[lastCentroid].ExpTime;
      value[9] = centBuf[lastCentroid].TimeStamp;
    } else {
      throwIllegalParameterException("Unrecognized server response");
    }
    if (need == 0) {
      return;			// GetCentroidN done
    }
    nn = 0;
    for (i=1; i<=need; i++) {
      // Read remaining centroids
      savLastCentroid = lastCentroid;
      ret = doOptOp("");	// Get next one
      if (ret != 0) {
	break;			// Time out or other abort condition
      }
      if (savLastCentroid == lastCentroid) {
	if (++nn < 5) {		// Must have read some other response
	  i--;			// Back up loop counter, but not indefinitely
	}
	continue;		// Go back for more
      }
      // Store next centroid in vector
      n = i*10;
      value[n] = centBuf[lastCentroid].SeqNo;
      value[n+1] = centBuf[lastCentroid].Peak;
      value[n+2] = centBuf[lastCentroid].CentX;
      value[n+3] = centBuf[lastCentroid].CentY;
      value[n+4] = centBuf[lastCentroid].WidthX;
      value[n+5] = centBuf[lastCentroid].WidthY;
      value[n+6] = centBuf[lastCentroid].RMS;
      value[n+7] = centBuf[lastCentroid].Valid;
      value[n+8] = centBuf[lastCentroid].ExpTime;
      value[n+9] = centBuf[lastCentroid].TimeStamp;
    }
    if (ret == 0) {
      return;			// Successfully read remaining centroids
    }
    if (ret > 0) {
      throwIllegalParameterException("GetCentroidNN error parsing response");
    } else {
      // Some centroids missing, perhaps exposures not yet complete
      // Zero out the missing ones
      for (i=10*i; i < 10*need+10; i++) {
        value[i] = 0;
      }
      value.resize(n+10);
    }
    break;
  default:
    {
      std::ostringstream buf;
      buf << "Illegal monitor address " << address << std::endl;
      throwIllegalParameterException(buf.str());
    }
  }
}
void Opts::monitor(int address, std::vector< long double >& value) {};
void Opts::monitor(int address, std::vector< std::string >& value) {};
void Opts::command(int address)
{
  CheckBadCom(address);
  switch (address) {
  case DoBias:
    optState.DoBias = 0;	// Clear done flag
    doOptOp("Do Bias");
    break;
  case DoBlank:
    optState.DoBlank = 0;	// Clear done flag
    doOptOp("Do Blank");
    break;
  case DoExit:
    doOptOp("Do Exit");
    break;
  case DoFocusInit:
    optState.DoFocusInit = 0;	// Clear done flag
    doOptOp("Do FocusInit");
    break;
  case DoFilterInit:
    optState.DoFilterInit = 0;	// Clear done flag
    doOptOp("Do FilterInit");
    break;
  case DoLogFileFlush:
    if (logFile != NULL) {
      fflush(logFile);
    }
    break;
  case DoStop:
    doOptOp("Do Stop");
    break;
  case DoShutUp:
    doOptOp("Do ShutUp");
    break;
  case DoSlew:
    doOptOp("Do Slew");
    break;
  case DoVideo:
    doOptOp("Do Video");
    break;
  default:
    {
      std::ostringstream buf;
      buf << "Illegal command address " << address << std::endl;
      throwIllegalParameterException(buf.str());
    }
  }
}
void Opts::command(int address, bool value)
{
  CheckBadCom(address);
  switch (address) {
  case DoCamPower:
    if (value) {
      doOptOp("Do CamPower On");
    } else {
      doOptOp("Do CamPower Off");
    }
    break;
  case DoFan:
    if (value) {
      doOptOp("Do Fan On");
    } else {
      doOptOp("Do Fan Off");
    }
    break;
  case DoShutter:
    optState.DoShutter = 0;	// Clear done flag
    if (value) {
      doOptOp("Do Shutter Open");
    } else {
      doOptOp("Do Shutter Close");
    }
    break;
  default:
    {
      std::ostringstream buf;
      buf << "Illegal command address " << address << std::endl;
      throwIllegalParameterException(buf.str());
    }
  }
}
void Opts::command(int address, unsigned char value) {};
void Opts::command(int address, char value) {};
void Opts::command(int address, unsigned short value) {};
void Opts::command(int address, short value) {};
void Opts::command(int address, unsigned int value) {};
void Opts::command(int address, int value)
{
  std::string DoCommand;
  char command[100];
  if (address == SetVerbose) {
    verbose = value;		// Change the verbose flag
    				// (0=no print, 1=print responses,
    return;			// 2=print requests and responses)
  }
  CheckBadCom(address);
  switch (address) {
  case DoBias:
    optState.DoBias = 0;	// Clear done flag
    DoCommand.assign("Do Bias");
    if (value == EZero) {
      DoCommand += " zero";
    }
    doOptOp(DoCommand.c_str());
    break;
  case DoBlank:
    optState.DoBlank = 0;	// Clear done flag
    DoCommand.assign("Do Blank");
    if (value == EZero) {
      DoCommand += " zero";
    }
    doOptOp(DoCommand.c_str());
    break;
  case DoFan:
    DoCommand.assign("Do Fan ");
    DoCommand += Opts::state2Name(value);
    doOptOp(DoCommand.c_str());
    break;

  case DoFilterInit:
    DoCommand.assign("Do FilterInit ");
    if (value != 0) {
      DoCommand += "/Notify";
      optState.DoFilterInit = Pending;
    } else {
      optState.DoFilterInit = 0;		// Clear done flag
    }
    doOptOp(DoCommand.c_str());
    break;
  case DoFilter:
    switch (value) {
    case Clear:
    case Blocked:
    case IRFilt:
    case OtherFilt:
      DoCommand.assign("Do Filter ");
      DoCommand += Opts::state2Name(value);
      optState.DoFilter = 0;	// Clear done flag
      doOptOp(DoCommand.c_str());
      break;
    default:
      char error[50];
      sprintf(error, "No such filter #%d", value);
      throwIllegalParameterException(error);
    }
    break;
  case DoFocusInit:
    DoCommand.assign("Do FocusInit ");
    if (value != 0) {
      DoCommand += "/Notify";
      optState.DoFocusInit = Pending;
    } else {
      optState.DoFocusInit = 0;		// Clear done flag
    }
    doOptOp(DoCommand.c_str());
    break;
  case DoTelHeater:
    DoCommand.assign("Do TelHeater ");
    DoCommand += Opts::state2Name(value);
    doOptOp(DoCommand.c_str());
    break;
  case SetExpCount:
    sprintf(command, "Set ExpCount %d", value);
    if (value < 0) {
      throwIllegalParameterException(std::string("Error: bad ")+command);
    }
    doOptOp(command);
    break;
  case SetNextSeqNo:
    sprintf(command, "Set NextSeqNo %d", value);
    if (value <= 0) {
      throwIllegalParameterException(std::string("Error: bad ")+command);
    }
    doOptOp(command);
    break;
  case SetFlatField:
    DoCommand.assign("Set FlatField ");
    DoCommand += Opts::state2Name(value);
    doOptOp(DoCommand.c_str());
    break;
  case DoVideo:
    if (value == Stop) {
      doOptOp("Do Video Stop");
    } else {
      throwIllegalParameterException("Bad parameter for Do Video");
    }
    break;
  default:
    {
      std::ostringstream buf;
      buf << "Illegal command address " << address << std::endl;
      throwIllegalParameterException(buf.str());
    }
  }
}
void Opts::command(int address, unsigned long value) {};
void Opts::command(int address, long value) {};
void Opts::command(int address, unsigned long long value) {};
void Opts::command(int address, long long value) {};
void Opts::command(int address, float value) {};
void Opts::command(int address, double value)
{
  char DoCommand[100];
  CheckBadCom(address);
  switch(address) {
  case SetCamTemp:
    sprintf(DoCommand, "Set CamTemp %.4g", value);
    doOptOp(DoCommand);
    break;
  case SetCenThresh:
    sprintf(DoCommand, "Set CenThresh %.4g", value);
    doOptOp(DoCommand);
    break;
  case SetExpTime:
    sprintf(DoCommand, "Set ExpTime %.4g", value);
    doOptOp(DoCommand);
    break;
  case SetSunThresh:
    sprintf(DoCommand, "Set SunThresh %.4g %.4g", value, value);
    doOptOp(DoCommand);
    break;
  case DoExpose:
    sprintf(DoCommand, "Do Expose %.4g", value);
    optState.DoExpose = 0;	// Clear done flag
    doOptOp(DoCommand);
    break;
  case DoFocus:
    sprintf(DoCommand, "Do Focus %.4g", value);
    optState.DoFocus = 0;	// Clear done flag
    doOptOp(DoCommand);
    break;
  default:
    {
      std::ostringstream buf;
      buf << "Illegal command address " << address << std::endl;
      throwIllegalParameterException(buf.str());
    }
  }
}
void Opts::command(int address, long double value) {};
void Opts::command(int address, const std::string& value)
{
  if (address == SetLogFile) {
    if (value.empty()) {
      if (logFile != NULL) {
	fclose(logFile);
	logFile = NULL;
      }
    } else {
      logFile = fopen(value.c_str(), "a");
      if (logFile == NULL) {
	throwIllegalParameterException("Could not open log file " + value);
      }
      logFileName = value;
    }
    return;
  }
  CheckBadCom(address);
  switch(address) {
  case SetMasterMB:
    {
      std::string Command("Set MasterMB ");
      Command += value;
      doOptOp(Command.c_str());
      break;
    }
  case DoFlatLoad:
    FILE *flat;
    int nread;
    char request[] = {"Do FlatLoad"};
    char error[100];

    flat = fopen(value.c_str(), "r");
    if (flat == NULL) {
      throwIllegalParameterException("Could not open flat file " + value);
    }
    nread = fread(buffer, 1, MAXFITS, flat);
    fclose(flat);
    if (nread < 2000) {
      sprintf(error, "Read of Flat file got only %d bytes", nread);
      throwIllegalParameterException(error);
    }
    if (logFile != NULL) {
      char msg[100];
      sprintf(msg, "%s Sent: %s\n", getCtime(0), request);
      logMsg(Opts::INFO, msg);
    }
    if (verbose == 2) {
      printf("Sent: %s\n", request);
    }
    if (sock_send(opt, request) == 0) {
      throwSocketOperationFailedException("Send Failed");
    }
    if (sock_write(opt, buffer, nread) == 0) {
      throwSocketOperationFailedException("Send of FITS Failed");
    }
    doOptOp("");		// Get response
    break;
  }
}
void Opts::command(int address, const std::vector< bool >& value) {};
void Opts::command(int address,
		   const std::vector< unsigned char >& value) {};
void Opts::command(int address, const std::vector< char >& value) {};
void Opts::command(int address,
		   const std::vector< unsigned short >& value) {};
void Opts::command(int address, const std::vector< short >& value) {};
void Opts::command(int address,
		   const std::vector< unsigned int >& value) {};
void Opts::command(int address, const std::vector< int >& value)
{
  char DoCommand[220], error[300];
  const char *word, *word2, *word3;
  int i, n, op, len, len0, *doFlag;
  CheckBadCom(address);
  n = value.size();
  switch(address) {
  case SetHotPix:
    if (n < 1) {
      sprintf(error, "SetHotPix: vector size too small: %d", n);
      throwIllegalParameterException(error);
    }
    op = value[0];
    word = state2Name(op);
    switch (op) {
    case Add:
    case Remove:
      break;
    case None:
      doOptOp("Set HotPix None");
      return;
    default:
      if (word[0] == 0) {
	sprintf(DoCommand, "%d is not an enum state value", op);
	word = DoCommand;
      }
      sprintf(error, "Illegal HotPix operation: %s", word);
      throwIllegalParameterException(error);
    }
    if (n < 4) {
      sprintf(error, "SetHotPix: no. of arguments < 4: %d", n);
      throwIllegalParameterException(error);
    }
    if ((n & 1) != 0) {
      sprintf(error, "SetHotPix: odd no. of arguments: %d", n);
      throwIllegalParameterException(error);
    }
    sprintf(DoCommand, "Set HotPix %s", word);
    len0 = strlen(DoCommand);
    len = len0;
    for (i=2 ; i<n; i += 2) {
      if (std::min(value[i], value[i+1]) < 1 ||
	  std::max(value[i], value[i+1]) > MAXPIX) {
	sprintf(error, "HotPix: illegal pixel: %d,%d", value[i], value[i+1]);
	throwIllegalParameterException(error);
      }
      sprintf(DoCommand+len, " %d,%d", value[i], value[i+1]);
      len += strlen(DoCommand+len);
      if (len > 90) {
	doOptOp(DoCommand);	// Do this portion of the HotPix list
	len = len0;
      }
    }
    if (len > len0) {
      doOptOp(DoCommand);	// Do the last part of the HotPix list
    }
    break;
  case SetSubImage:
    if (value[0] == All) {
      doOptOp("Set SubImage All");
      break;
    }
    if (n != 4) {
      sprintf(error, "SetSubImage: wrong number of arguments: %d", n);
      throwIllegalParameterException(error);
    }
    sprintf(DoCommand, "Set SubImage %d,%d %d,%d", value[0], value[1],
	    value[2], value[3]);
    for (i=0; i<4; i++) {
      if (value[i] < 1 || value[i] > MAXPIX) {
	sprintf(error, "Bad Pixel coords in: %s", DoCommand);
	throwIllegalParameterException(error);
      }
    }
    doOptOp(DoCommand);
    break;
  case DoBias:
    word = "Do Bias";
    doFlag = &optState.DoBias;
  case DoBlank:
  case DoFilter:
  case DoShutter:
    if (address == DoBlank) {
      word = "Do Blank";
      doFlag = &optState.DoBlank;
    } else if (address == DoFilter) {
      word = "Do Filter";
      doFlag = &optState.DoFilter;
    } else if (address == DoShutter) {
      word = "Do Shutter";
      doFlag = &optState.DoShutter;
    }
    if (n < 1 || n > 2) {
      sprintf(error, "Wrong %s no. of arguments: %d", word, n);
      throwIllegalParameterException(error);
    }
    if (address == DoFilter || address == DoShutter) {
      word2 = state2Name(value[0]);
      if (word2[0] == 0) {
	sprintf(DoCommand, "%d is not an enum state value", value[0]);
	word2 = DoCommand;
      }
    }
    if (address == DoFilter) {
      switch (value[0]) {
      case Clear:
      case Blocked:
      case IRFilt:
      case OtherFilt:
	break;
      default:
	sprintf(error, "Illegal Do Filter name: %s", word2);
	throwIllegalParameterException(error);
      }
    } else if (address == DoShutter) {
      if (value[0] != Open && value[0] != Close) {
	sprintf(error, "Illegal Do Shutter operation: %s", word2);
        throwIllegalParameterException(error);
      }
    } else {
      // DoBias or DoBlank
      if (value[0] == EZero) {
	word2 = "zero";		// Zero out the Bias/Blank
	n = 1;			// Make sure there's no /Notify on "zero"
      } else {
	word2 = "";
      }
    }
    if (n == 1 || value[1] == 0) {
      word3 = "";
      *doFlag = 0;		// Clear done flag
    } else {
      word3 = "/Notify";
      *doFlag = Pending;	// Initialize the Notify flag
    }
    sprintf(DoCommand, "%s %s %s", word, word2, word3);
    doOptOp(DoCommand);
    break;
  default:
    {
      std::ostringstream buf;
      buf << "Illegal command address " << address << std::endl;
      throwIllegalParameterException(buf.str());
    }
  }
}

void Opts::command(int address,
		   const std::vector< unsigned long >& value) {};
void Opts::command(int address, const std::vector< long >& value) {};
void Opts::command(int address,
		   const std::vector< unsigned long long >& value) {};
void Opts::command(int address, const std::vector< long long >& value) {};
void Opts::command(int address, const std::vector< float >& value) {};
void Opts::command(int address, const std::vector< double >& value)
{
  char DoCommand[100], error[100];
  std::string word;
  float value2;
  int i, n, len, ival, nexpose;

  CheckBadCom(address);
  n = value.size();
  switch(address) {
  case DoExpose:		// Expose with two parameters
    if (n < 1 || n > 3) {
      sprintf(error, "Wrong number of arguments for DoExpose: %d", n);
      throwIllegalParameterException(error);
    }
    if (n > 1) {
      nexpose = (int)value[1];
      if (nexpose != value[1]) {
	sprintf(error, "Non-integral exposure count in DoExpose: %.6g",
		value[1]);
	throwIllegalParameterException(error);
      }
      if (nexpose < 0) {
	sprintf(error, "Bad exposure count in DoExpose: %d", nexpose);
	throwIllegalParameterException(error);
      }
    } else {
      nexpose = 1;
    }
    if (n > 2) {
      if (value[2] > 0) {
	word = "/Notify";
	optState.DoExpose = Pending;
	optState.sendExpose = 0;
      } else if (value[2] < 0) {
	word = "/Send";
	optState.DoExpose = Pending;
	optState.sendExpose = nexpose;
      } else {
	word = "";
	optState.DoExpose = 0;		// Clear done flag
	optState.sendExpose = 0;
      }
    }
    sprintf(DoCommand, "Do Expose %.2g %d %s", value[0], (int)value[1], word.c_str());
    doOptOp(DoCommand);
    break;
  case DoFocus:
    if (n < 1) {
      sprintf(error, "Wrong number of arguments for DoFocus: %d", n);
      throwIllegalParameterException(error);
    }
    if (n > 1 && value[1] != 0) {
      word = "/Notify";
      optState.DoFocus = Pending;
    } else {
      word = "";
      optState.DoFocus = 0;	// Clear done flag
    }
    sprintf(DoCommand, "Do Focus %.4g %s", value[0], word.c_str());
    doOptOp(DoCommand);
    break;
  case SetCentAlg:
    if (n <= 1 || n > MAXALG_P+1) {
      sprintf(error, "Wrong number of arguments for SetCentAlg: %d", n);
      throwIllegalParameterException(error);
    }
    ival = (int)value[0];
    if (ival != value[0]) {
      sprintf(error, "Non-integral centroid algorithm ID #: %.8g", value[0]);
      throwIllegalParameterException(error);
    }
    if (ival < 1 || ival > MAXALG) {
      sprintf(error, "Bad centroid algorithm ID #: %d", ival);
      throwIllegalParameterException(error);
    }
    // Construct the Do CentAlg command
    sprintf(DoCommand, "Set CentAlg %d", (int)ival); // Stem of the command
    len = strlen(DoCommand);
    for (i=1; i<n; i++) {	// Add the parameters
      sprintf(DoCommand+len, " %.8g", value[i]);
      len += strlen(DoCommand+len);
    }
    doOptOp(DoCommand);
    break;
  case SetSunThresh:
    if (n < 1 || n > 2) {
      sprintf(error, "Wrong number of arguments for SetSunThresh: %d", n);
      throwIllegalParameterException(error);
    }
    if (n == 1) {
      value2 = value[0];
    } else {
      value2 = value[1];
    }
    sprintf(DoCommand, "Set SunThresh %.4g %.4g", value[0], value2);
    doOptOp(DoCommand);
    break;
  default:
    {
      std::ostringstream buf;
      buf << "Illegal command address " << address << std::endl;
      throwIllegalParameterException(buf.str());
    }
  }
}
void Opts::command(int address,
		   const std::vector< long double >& value) {};
void Opts::command(int address,
		   const std::vector< std::string >& value) {};

// Get the latest Image

boost::shared_array<char> Opts::getImage(int &imageSize)
{
  return ImagePtr("Get Image", imageSize, NULL);
}

// Get the specified image

boost::shared_array<char> Opts::getImage(int seqNo, int &imageSize)
{
  char DoCommand[100];
  boost::shared_array<char> ret;

  ret = haveImage(seqNo, imageSize);
  if (imageSize != 0) {
    return ret;			// Already have that image
  }
  sprintf(DoCommand, "Get Image %d", seqNo);
  return ImagePtr(DoCommand, imageSize, NULL);
}

// Fetch images seqNo through endSeqNo, return the first one

boost::shared_array<char> Opts::getImages(int seqNo, int endSeqNo,
					  int &imageSize)
{
  int num, first, last;
  char DoCommand[100], error[100];
  boost::shared_array<char> ret;

  num = endSeqNo - seqNo + 1;
  if (num <= 0 || seqNo <= 0) {
    sprintf(error, "Illegal getImages Sequence number range: %d %d",
	    seqNo, endSeqNo);
    throwIllegalParameterException(error);
  }
  // Check for the first image already in the buffer
  ret = haveImage(seqNo, imageSize);
  if (imageSize == 0) {
    first = seqNo;		// Image seqNo isn't in the buffer
  } else {
    int i;			// Already have the first one

    if (seqNo == endSeqNo) {
      return ret;		// A range of one.  Nothing more to fetch
    } else {
      // Asking for more than one.  Do we have any more in the buffer?
      i = lastNo(seqNo+1, endSeqNo);
      if (i > seqNo) {
	// There's more than just image seqNo in the buffer
	lastFetchedImageNo = i; // Save latest fetched for getNextImage
	nextImageNo = seqNo+1;	// And the range remaining
	endImageNo = endSeqNo;
	return ret;		// And return the first one
      }
    }
    first = seqNo+1;	        // Only seqNo is in the buffer; fetch next one
  }
  // At most one image at the beginning of the range is in the buffer
  // Request some/all of the images
  num = endSeqNo - first + 1;	// Number remaining to fetch
  num = std::min(num, MAXFITSBUF); // Don't ask for more than will fit in buffer
  last = first + num - 1;
  last = lastMissing(first, last); // Only ask for ones missing from the buffer
  sprintf(DoCommand, "Get Image %d %d", first, last);
  if (first > seqNo) {
    // Already have the first image
    // Send the request to the OPTS server and don't wait for a response
    doOptOpOnly(DoCommand);
    return ret;			// Return the first image
  }
  ret = ImagePtr(DoCommand, imageSize, NULL); // Fetch this (sub)range
  lastFetchedImageNo = last; 	// Save latest fetched for getNextImage
  if (seqNo+1 <= last) {
    // There's more than just one
    nextImageNo = seqNo+1;	// Record the range remaining for getNextImage
    endImageNo = endSeqNo;
    if (seqNo != last) {
      doOptOp("no-op");		// Just poll for any pending FITS in the pipe
    }
  }
  return ret;			// Return the first image
}

/* Get the next image in the getImages list */
boost::shared_array<char> Opts::getNextImage(int &imageSize)
{
  boost::shared_array<char> ret(0);
  char DoCommand[100];
  int i, num, last;

  if (nextImageNo == 0) {
    imageSize = 0;		// Nothing to do
    return ret;
  }

  doOptOp("no-op");		// Poll for any extra
  ret = haveImage(nextImageNo, imageSize);
  if (imageSize != 0) {
    // The next image has already been fetched
    nextImageNo++;
  }
  if (nextImageNo > endImageNo) {
    nextImageNo = endImageNo = 0; // That was the last one
  } else if (nextImageNo > lastFetchedImageNo) {
    // Need to fetch some more images
    num = std::min(endImageNo - nextImageNo + 1, MAXFITSBUF);
    last = nextImageNo + num - 1;
    // First check the range for images already in the buffer
    i = lastNo(nextImageNo, last);
    if (i < nextImageNo) {
      // There are no images in the beginning of this range in the buffer
      // Adjust the end of the range to prevent fetching any in the buffer
      last = lastMissing(nextImageNo, last);
      sprintf(DoCommand, "Get Image %d %d", nextImageNo, last);
      if (imageSize == 0) {
	doOptOp(DoCommand);	// Get the image(s) and wait for first one
      } else {
	// Already have an image, fetch more but don't wait for any
	doOptOpOnly(DoCommand);
      }
      lastFetchedImageNo = last;
    }
  }
  if (imageSize == 0) {
    // The next image wasn't already in the buffer.  Should be there now
    printf("%s Check again for #%d\n", getCtime(0), nextImageNo);
    doOptOp("no-op");		// Poll for any extra
    ret = haveImage(nextImageNo, imageSize);
    if (imageSize != 0) {
      nextImageNo++; // The next image has now been fetched
    }
    if (imageSize == 0) {
      // Sleep for up to MS_DOZEMAX millisec in naps of MS_DOZE millisec
      for (i=0; i<(MS_DOZEMAX/MS_DOZE); i++) {
	printf("Dozing %s\n", getCtime(0));
	nanosleep(&doze, NULL);	// Still nothing.  Sleep for a few ms
	printf("Wake up: %s\n", getCtime(0));
	printf("%s Check once again for #%d\n", getCtime(0), nextImageNo);
	doOptOp("no-op");		// Look again
	ret = haveImage(nextImageNo, imageSize); // There now?
	if (imageSize != 0) {
	  break;
	}
      }
      if (imageSize == 0) {
	imageSize = -nextImageNo; // Still no image. Return the seqNo missing
      } else {
	nextImageNo++; // The next image has finally been fetched
      }
    }
  }
  return ret;
}

boost::shared_array<char> Opts::getImageCen(int &imageSize,
					    struct centroid &Centroid)
{
  return ImagePtr("Get ImageCen", imageSize, &Centroid);
}
boost::shared_array<char> Opts::getImageCen(int seqNo, int &imageSize,
					    struct centroid &Centroid)
{
  char DoCommand[100];
  boost::shared_array<char> ret;

  ret = haveImageCen(seqNo, imageSize, &Centroid);
  if (imageSize != 0) {
    return ret;			// Already have that image
  }
  sprintf(DoCommand, "Get ImageCen %d", seqNo);
  return ImagePtr(DoCommand, imageSize, &Centroid);
}

const char *Opts::state2Name(int enumValue) {
  // Return the name corresponding to the enum state value
  static char notAnEnum = '\0';

  if (enumValue < stateEnum[0] || enumValue > stateEnum[NUMSTATE-1])
    return &notAnEnum;

  return stateName[enumValue-stateEnum[0]];
}

int Opts::name2State(const char* nameOfState) {
  // Return the enum state value corresponding to the state name, nameOfState
  int i;

  for (i=0; i < NUMSTATE; i++) {
    if (strcmp(nameOfState, stateName[i]) == 0) {
      return stateEnum[i];
    }
  }
  return -999;			// No such name
}

const char *Opts::getStatusName(int index) {
  static char notAstatus = '\0';
  if (index < 0 || index >= NUMSTATUS)
    return &notAstatus;

  return statusName[index];
}

// Private functions
void Opts::CheckBadMon(int address)
{
  if (opt == NULL) {
    throwIllegalParameterException(std::string("Connection to OPT not open"));
  }
  if (address < QueryBias || address > GetCentroidNN) {
    throwIllegalParameterException(std::string("Illegal monitor address"));
  }
}
void Opts::CheckBadCom(int address)
{
  if (opt == NULL) {
    throwIllegalParameterException(std::string("Connection to OPT not open"));
  }
  if (address < SetCamTemp || address > DoVideo) {
    throwIllegalParameterException(std::string("Illegal command address"));
  }
}

// Send the request to the OPT.  Don't check for unread messages or wait for
// a response to it

void Opts::doOptOpOnly(const char *request)
{
  if (logFile != NULL) {
    char msg[strlen(request)+40];
    sprintf(msg, "%s Sent: %s\n", getCtime(0), request);
    logMsg(Opts::INFO, msg);
  }
  if (verbose == 2) {
    printf("Sent: %s\n", request);
  }
  if (sock_send(opt, request) == 0) {
    throwSocketOperationFailedException(std::string("Send failed"));
  }
}

// Send the request to the OPT:  First check for unread messages, send the
// request, wait for a response, then parse it.  Returns non-zero if a
// a parse error occured.
// If the request is "no-op", only check for unread messages.
// If the request is an empty string, only wait for
// a response and parse it, or return -99 upon a time out.

int Opts::doOptOp(const char *request)
{
  int lenMsg;
  int ret = 0, last, lenReq;

  lenReq = strlen(request);
  // If request is empty, skip the next Check and the sock_send following
  if (lenReq > 0) {
// Check for additional messages since the last command was sent
    while (sock_poll (opt, buffer, &lenMsg) > 0) {
      // Some command must have generated additional responses.  Read it
      if (strncmp(buffer, "SIMPLE  =", 9) == 0) {
	/* FITS input */
	gotFITS(buffer, lenMsg, 0); // It's a FITS image.  Store in curImage
	if (logFile != NULL) {
	  char msg[100];
	  sprintf(msg,"%s Got: FITS #%d\n", getCtime(0), curSeqNo[lastFITS]);
	  logMsg(Opts::INFO, msg);
	}
	if (verbose != 0) {
	  printf("Got: FITS #%d\n", curSeqNo[lastFITS]);
	}
      } else {
	if (logFile != NULL) {
	  char msg[lenMsg+100];
	  sprintf(msg,"%s Got: %s\n", getCtime(0), buffer);
	  logMsg(Opts::INFO, msg);
	}
	if (verbose != 0) {
	  printf("Got: %s\n", buffer);
	}
	processOpt(buffer, lenMsg); // Parse it and update optState
      }
    }
    if (strcmp(request, "no-op") == 0) {
      return 0;			// Only polling
    }
    // Send the request to the OPTS server
    doOptOpOnly(request);
  }
  // Read the response from the Server
  lenMsg = sock_only_f(opt, buffer, sock_timeout);
  if (lenMsg < 0 && lenReq > 0) {
    throwEthernetDeviceException(std::string("Response failed"));
  } else if (lenMsg <= 0) {
    return -1;			// Additional response timed out
  }

  // Process the response from the Server
  if (strncmp(buffer, "SIMPLE  =", 9) == 0) {
    /* FITS input */
    gotFITS(buffer, lenMsg, 0); // It's a FITS image.  Store in curImage
    if (logFile != NULL) {
      char msg[100];
      sprintf(msg,"%s Got: FITS #%d\n", getCtime(0), curSeqNo[lastFITS]);
      logMsg(Opts::INFO, msg);
    }
    if (verbose != 0) {
      printf("Got: FITS #%d\n", curSeqNo[lastFITS]);
    }
  } else {
    // Response is text
    if (logFile != NULL) {
      char msg[lenMsg+100];
      sprintf(msg,"%s Got: %s\n", getCtime(0), buffer);
      logMsg(INFO, msg);
    }
    if (verbose != 0) {
      printf("Got: %s\n", buffer);
    }
    // Parse the response
    last = lastError;		// Remember index of oldest error
    ret =  processOpt(buffer, lenMsg);
    if (last != lastError) {
      throwIllegalParameterException(ErrorHistory[lastError]);
    }
  }
  return ret;
}

// Execute the DoCommand to fetch the image specified (and Centroid if not NULL)

boost::shared_array<char> Opts::ImagePtr(const char *DoCommand, int &imageSize,
					 struct centroid *Centroid)
{
  int savLastCentroid, savLastFITS, ret, lenMsg, last;
  savLastCentroid = lastCentroid;
  savLastFITS = lastFITS;
  doOptOp(DoCommand);
  if (Centroid != NULL) {
    // doOptOp should have read the centroid.  Now read the image
    lenMsg = sock_only_f(opt, buffer, sock_timeout);
    if (lenMsg < 0) {
      throwEthernetDeviceException(std::string("Response failed"));
    }
    if (strncmp(buffer, "SIMPLE  =", 9) == 0) {
      /* FITS input */
      gotFITS(buffer, lenMsg, 0); // It's a FITS image.  Store in curImage
      if (logFile != NULL) {
	char msg[100];
	sprintf(msg,"%s Got: FITS #%d\n", getCtime(0), curSeqNo[lastFITS]);
	logMsg(Opts::INFO, msg);
      }
      if (verbose != 0) {
	printf("Got: FITS #%d\n", curSeqNo[lastFITS]);
      }
    } else {
      // No image came.  The response is text.
      if (logFile != NULL) {
	char msg[lenMsg+100];
	sprintf(msg,"%s Got: %s\n", getCtime(0), buffer);
	logMsg(Opts::INFO, msg);
      }
      if (verbose != 0) {
	printf("Got: %s\n", buffer);
      }
      // Parse the response
      last = lastError;			// Remember index of oldest error
      ret =  processOpt(buffer, lenMsg); 	// Parse the response
      if (last != lastError) {
	throwIllegalParameterException(ErrorHistory[lastError]);
      }
    }
    if (lastCentroid == savLastCentroid) {
      printf("%s: centroid fetch failed", DoCommand);
      Centroid->SeqNo = 0;
    } else {
      *Centroid = centBuf[lastCentroid];
    }
  }
  if (savLastFITS == lastFITS) {
    printf("%s: image fetch failed", DoCommand);
    imageSize = 0;
    return boost::shared_array<char> (0);
  }
  imageSize = curImageSize[lastFITS];
  boost::shared_array<char> Ptr (new char[imageSize]);
  memcpy(Ptr.get(), &curImage[lastFITS][0], imageSize);
  return Ptr;
}

// Return the index of image seqNo in the FITS buffer
// If not found, return -1

int Opts::findImage(int seqNo)
{
  int i;

  // Look backwards through the circular buffer
  i = lastFITS;
  do {
    if (curSeqNo[i] == seqNo) {
      break;
    }
    if (--i < 0) {
      i = MAXFITSBUF-1;
    }
  } while (i != lastFITS);

  if (curSeqNo[i] == seqNo) {
    return i;			// Image is at buffer index i
  }
  return -1;			// No such image
}

// Look for images firstNo through endNo in the FITS buffer in ascending order.
// The return value is one less than the first sequence number in the range
// NOT found in the buffer.  If they all exist, returns endNo.
// If none exist or if endNo < firstNo, returns firstNo - 1.

int Opts::lastNo(int firstNo, int endNo)
{
  int i, j;

  if (endNo < firstNo) {
    return firstNo-1;
  }

  for (i=firstNo; i<= endNo; i++) {
    j = findImage(i);
    if (j < 0) {
      break;		// Don't have this one
    }
  }
  i--;			// Make i the last image # found in the buffer
  printf("lastNo %d: first, end = %d %d\n", i, firstNo, endNo);
  printf("curSeqNo: ");
  for (j=0; j<MAXFITSBUF; j++)
    printf("%d ", curSeqNo[j]);
  printf("\n");
  return i;
}

// Reverse of lastNo():
// Look for images firstNo through endNo in the FITS buffer in ascending order.
// The return value is one less than the first sequence number in the range
// found in the buffer.  If none exist, returns endNo.
// If all exist or if endNo < firstNo, returns firstNo - 1.

int Opts::lastMissing(int firstNo, int endNo)
{
  int i, j;

  if (endNo < firstNo) {
    return firstNo-1;
  }

  for (i=firstNo; i <= endNo; i++) {
    j = findImage(i);
    if (j >= 0) {
      break;		// This one exists
    }
  }
  i--;			// Make i the last image # not found in the buffer

  printf("lastMissing %d: first, end = %d %d\n", i, firstNo, endNo);
  printf("curSeqNo: ");
  for (j=0; j<MAXFITSBUF; j++)
    printf("%d ", curSeqNo[j]);
  printf("\n");
  return i;
}

// Look in the FITS buffer for the image seqNo specified
// If not found, imageSize = 0

boost::shared_array<char> Opts::haveImage(int seqNo, int &imageSize)
{
  int i;

  i = findImage(seqNo);		// Look for image number seqNo

  if (i < 0) {
    // No such image.  Return imageSize=0 and an empty shared_array pointer
    imageSize = 0;
    return boost::shared_array<char> (0);
  }

  // Index i holds image seqNo.
  // Return its imageSize and a shared_array pointer to it
  imageSize = curImageSize[i];
  boost::shared_array<char> Ptr (new char[imageSize]);
  memcpy(Ptr.get(), &curImage[i][0], imageSize);
  return Ptr;
}

// Look in the FITS buffer for the image seqNo specified and in the
// Centroid buffer for the centroid seqNo found
// If no imaget is found, imageSize = 0
// If an image is found, but no centroid, read the centroid from the Server


boost::shared_array<char> Opts::haveImageCen(int seqNo, int &imageSize,
					 struct centroid *Centroid)
{
  boost::shared_array<char> ret;
  int i;
  char command[100];

  ret = haveImage(seqNo, imageSize);
  if (imageSize == 0) {
    Centroid->SeqNo = 0;		// No such image
    return ret;
  }

  // That image was found
  // Look backwards through the circular centroid buffer

  i = lastCentroid;
  do {
    if (centBuf[i].SeqNo == seqNo) {
      break;
    }
    if (--i < 0) {
      i = MAXCENTROID-1;
    }
  } while (i != lastCentroid);

  if (curSeqNo[i] == seqNo) {
    *Centroid = centBuf[i];	// Found the centroid, too
    return ret;
  }

  // Centroid not found.  Fetch it
  sprintf(command, "Get Centroid %d", seqNo);
  doOptOp(command);
  if (centBuf[lastCentroid].SeqNo != seqNo) {
    Centroid->SeqNo = 0;         // No such centroid
  } else {
    *Centroid = centBuf[lastCentroid];
  }
  return ret;
}


/* Put the message into the log file (if any) then throwthe exception */

void Opts::throwEthernetDeviceException(const std::string& errorMessage)
{
  if (logFile != NULL) {
    char msg[errorMessage.length()+100];
    sprintf(msg,"%s EthernetDeviceException: %s\n", getCtime(0),
	    errorMessage.c_str());
    logMsg(Opts::ERROR, msg);
  }
  throw EthernetDeviceException(errorMessage);
}

void Opts::throwSocketOperationFailedException(const std::string& errorMessage)
{
  if (logFile != NULL) {
    char msg[errorMessage.length()+100];
    sprintf(msg,"%s SocketOperationFailedException: %s\n", getCtime(0),
	    errorMessage.c_str());
    logMsg(Opts::ERROR, msg);
  }
  throw SocketOperationFailedException(errorMessage);
}

void Opts::throwIllegalParameterException(const std::string& errorMessage)
{
  if (logFile != NULL) {
    char msg[errorMessage.length()+100];
    sprintf(msg,"%s IllegalParameterException: %s\n", getCtime(0),
	    errorMessage.c_str());
    logMsg(Opts::ERROR, msg);
  }
  throw IllegalParameterException(errorMessage);
}

void Opts::logMsg(const int priority, const char *message)
{
  const char *word;
  if (priority > 0) {
    word = " ***************************************************\n";
  } else {
    word = "";
  }
  fprintf(logFile, "%s%s", message, word);
}
