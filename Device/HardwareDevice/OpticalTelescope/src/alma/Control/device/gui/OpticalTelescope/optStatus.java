// Class to get the current status of the OPTS
// Uses a Native Method, checkOpt, to call the C++ class Opts and
// get the status of the OPT

package alma.Control.device.gui.OpticalTelescope;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.util.Locale;


class optStatus {
    // Constructor.  Make a data set to store the status
    public optStatus(int numRow, int numCol, int lenCut, String name) {
	optName = name.getBytes(); // Convert to bytes for C++
	resize(numRow, numCol, lenCut);	// Set up the array sizes
    }

    // Change the size of the image in the optStatus object
    public void resize(int numRow, int numCol, int lenCut) {
	nRow = numRow;
	nCol = numCol;
	image = new BufferedImage(numCol, numRow, BufferedImage.TYPE_BYTE_GRAY);
	if (nCut != lenCut) {
	    starRow = new int[lenCut]; 		// Horizontal cut
	    starCol = new int[lenCut]; 		// Vertical cut
	    nCut = lenCut;
	}
	pixel = new short[numRow*numCol]; 	// Pixels in 1-D array
    }

    // Store the image in the starImage scroll pane
    public void makeImage(ALMAFrame frame) {
	double offset;
	double Zoom = 1.;
	double centX = centroidValue[CENTX];
	double centY = centroidValue[CENTY];
	double widthX = centroidValue[WIDTHX];
	double widthY = centroidValue[WIDTHY];
	double centPeak = centroidValue[CENTPEAK];
	int valid = (int)centroidValue[VALID];
	corrXY[0] = centX - centRawX; // Correction to the CentX
	corrXY[1] = centY - centRawY; // Correction to the CentY
	int[] Pixel = new int[nRow*nCol];

	for (int i=0; i < nRow*nCol; i++) {
	    Pixel[i] = pixel[i]; 		// Convert from short to int
	}

	// Supply the scroll pane with the information describing the image
	frame.displayImage.newStar(Pixel, nCol, nRow, subX+corrXY[0], 
				   subY+corrXY[1], Zoom, centX, centY, starRow, 
				   starCol, (int)pixelMin, (int)pixelMax, centBox,
				   widthX, widthY, centPeak, valid);
	frame.repaint();
    }

    public static native String state2Name(int enumValue);
    // state2Name is a native method that calls the Opts method, state2name
    // This converts the specified enum state integer into the corresponding
    // state name.  (Such values are used to encode the OPT status in the
    // values below.  When they share an array with a float value, such as 
    // in CamStat[], they have been cast with (float).)

    public static native int name2State(String stateName);
    // name2State is a native method that calls the Opts method, name2State
    // This converts the specified state name into the corresponding
    // enum state integer.  (Such values are used to encode the OPT status in the
    // values below.  When they share an array with a float value, such as 
    // in CamStat[], they have been cast with (float).)

    public static native String getStatusName(int index);
    // getStatusName is a native method that calls the Opts method, getStatusName
    // This converts the Status[] array index specified into the corresponding 
    // name of the element.  (They are in the same order as returned by the
    // OPTS command, Query Status, as documented in the ICD.)

    public native int checkOpt(int iWant, float timeOut) 
					throws java.net.SocketException; 

    // checkOpt is a native method which polls the OPTS and stores status
    // information and the most recent image in the optStatus object

    // iWant: which image seqNo is wanted (0 = next new image)
    // timeOut: Seconds to wait if not ready yet
    // Returns: 0 - No new/iWant image is available
    //              (Opt status information is updated, however)
    //          >0 - Number of images available matching the size expected 
    //               in optStatus.  First one is put into optStatus
    //          <0 - Images are available, but the image size doesn't match
    //        -999 - Threw exception

    // Status having to do with the most current image
    public int seqNo;	    // Current sequence number
    public int newRow;	    // Number of rows in current image
    public int newCol;	    // Number of columns in current image
    public int centBox;	    // Current length of the sides of centroid box
    public int subX;	    // X coordinate of the first pixel in the subimage
    public int subY;	    // Y coordinate of the first pixel in the subimage
    public double centroidValue[] = new double[10];
    public double centRawX; // CentX uncorrected for temp. drift
    public double centRawY; // CentY uncorrected for temp. drift
    public double tempTube;	// OPT tube temperature from FITS header
    public double tempFilt;	// OPT filterbox temperature from FITS header
    public double corrXY[] = new double[2];   // CentX - CentRawX, CentY - CentRawY

    // Parameters giving the size in Java for the image 
    public int nRow;	    // Number of rows in image
    public int nCol;	    // Number of columns in image
    public int nCut;	    // Length of starRow & starCol (usually = centBox)
    public int starRow[];   // Horizontal cut through the centroid peak
    public int starCol[];   // Vertical cut throuch the centroid peak
    public short pixel[];   // Pixel values in 1-D array (row1, row2, ...)
    public short pixelMin;	// Minimum pixel value
    public short pixelMax;	// Maximum pixel value

    // Status parameters from checkOPT exceptions: enum state value: OK or Bad
    public int[] OPTrunning = new int[1]; 	// Bad if no OPTS running

    // Other Parameters to checkOPT from Java
    public boolean logFileSelected;   // True: writing a log file
    public String logFile = "checkOPT.log";	   // Name of log file
    public boolean fitsWriteSelected; // True: writing FITS files of the images
    public byte[] optName;                    // Hostname of OPT to query

    // Other OPT status values of interest
    public int CamPower[] = new int[1];	      // Camera Power state (On or Off)
    public float CamStat[] = new float[4];    // Camera state, time left, 
                                              // todo, expTime
    public float CamTemp[] = new float[2];    // CCD temperature, set point
    public float CentAlgP[] = new float[2];   // Centroid algorithm parameters
                                              // Only 2 of 3 parameters are used
    public float CenThresh[] = new float[1];  // Centroid threshold
    public int Fan[] = new int[1];            // Fan state (On or Off)
    public int FiltPos[] = new int[2];        // Filter state, filter
    public int FlatField[] = new int[1];      // Fl.F. kind (None, ALMA, Factory)
    public float FocusInit[] = new float[1];  // FocusInit correction
    public float FocusPos[] = new float[2];   // Focus position, state
    public int Status[] = new int[7];         // Query Status items: OK or bad
    public int Shutter[] = new int[1];        // Shutter position
    public float SunSensor[] = new float[5];  // Sun sensor states(2), 
                                              // volts(2), failsafe state
    public float SunThresh[] = new float[2];  // Sun sensor thresholds
    public int TelHeater[] = new int[1];      // Heater state
    public float TelTemp[] = new float[2];    // Filterbox temp, tube temp
    public float DcDt[] = new float[2];       // Temperature drift rates for X & Y

    public int statusChanged;  // Set non-zero if any above status values changed

    // Other quantities for Java
    public BufferedImage image;

    // Elements in centroidValue array from OPT
    public static final int CENTNUM   = 0;
    public static final int CENTPEAK  = 1;
    public static final int CENTX     = 2;
    public static final int CENTY     = 3;
    public static final int WIDTHX    = 4;
    public static final int WIDTHY    = 5;
    public static final int RMS	      = 6;
    public static final int VALID     = 7;
    public static final int EXPTIME   = 8;
    public static final int TIMESTAMP = 9;

}
