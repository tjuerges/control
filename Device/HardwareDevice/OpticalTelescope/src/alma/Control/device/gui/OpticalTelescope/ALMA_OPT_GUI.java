package alma.Control.device.gui.OpticalTelescope;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.GeneralPath;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.Format;
import java.util.Iterator;
import java.util.Locale;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.SwingConstants;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ALMA_OPT_GUI {
    public static void main(String[] args) {
	final String name;
	if (args.length > 0) {
	    name = args[0];
	} else {
	    name = "No Name";
	}
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                ALMAFrame frame = new ALMAFrame(name);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setVisible(true);
            }
        });
    }
    static
    {
	System.loadLibrary("optStatus");
    }
}

/**
 * ALMA OPT GUI's frame
 */
class ALMAFrame extends JFrame implements ActionListener {
    public ALMAFrame(String name) {
	if (name.equals("No Name")) {
	    name = getOPTname((Component)this, name);
	}
        theFrame = this;
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);

	// Construct an optStatus object initialized to 1024 x 1024 pixels
	// and space for 21-pixel horizontal and vertical cuts through the
	// centroid peak
	statOPT = new optStatus(1024, 1024, 21, name);
	statOPT.centroidValue[0] = -999;
        // add the sample text label

	name = name.substring(0,1).toUpperCase() + name.substring(1);
        setTitle(name + " Monitor");

	// Make a Panel for the North component of the JFrame
	// consisting of JcheckBox's for turning on & off logging and 
	// writing FITS files
	topPanel = new JPanel();
	topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.LINE_AXIS));

	imageLabel = new JLabel("Image 0", JLabel.CENTER);
	imageLabel.setFont(new Font("SansSerif", Font.BOLD, TITLEFONTSIZE));
	topPanel.add(Box.createHorizontalGlue());
	topPanel.add(imageLabel);
	topPanel.add(Box.createHorizontalGlue());

	writeLog = new JCheckBox("Write log file");
	writeLog.setFont(labelFont);
	writeLog.addActionListener(this);
	writeLog.setHorizontalTextPosition(SwingConstants.CENTER);
	writeLog.setVerticalTextPosition(SwingConstants.TOP);
	topPanel.add(writeLog);

	writeFITS = new JCheckBox("Write FITS file");
	writeFITS.setFont(labelFont);
	writeFITS.addActionListener(this);
	writeFITS.setHorizontalTextPosition(SwingConstants.CENTER);
	writeFITS.setVerticalTextPosition(SwingConstants.TOP);
	topPanel.add(writeFITS);
       
	pauseMonitor = new JCheckBox("Pause");
	pauseMonitor.setFont(labelFont);
	pauseMonitor.addActionListener(this);
	pauseMonitor.setHorizontalTextPosition(SwingConstants.CENTER);
	pauseMonitor.setVerticalTextPosition(SwingConstants.TOP);
	topPanel.add(pauseMonitor);
       
	add(topPanel,BorderLayout.NORTH);
        displayImage = new starImage();
        add(displayImage, BorderLayout.CENTER);
	status = new statusInfo(statOPT);	
	add(status, BorderLayout.EAST);
	statPlot = new statusPlot(this, statOPT);
	add(statPlot, BorderLayout.SOUTH);

	// Start a thread that monitors the OPT
        Runnable r = new showOptsRunnable(this);
        Thread t = new Thread(r);
        t.start();
	validate();
    }

    public void actionPerformed(ActionEvent e) {
	if (e.getActionCommand().equals("Write log file")) {
	    statOPT.logFileSelected = ((JCheckBox)e.getSource()).isSelected();
	} else if (e.getActionCommand().equals("Write FITS file")) {
	    statOPT.fitsWriteSelected = ((JCheckBox)e.getSource()).isSelected();
	} else if (e.getActionCommand().equals("Pause")) {
	    pauseOPTmonitor = ((JCheckBox)e.getSource()).isSelected();
	}
    }

    // No OPTS name on command line or bad name specified 
    // Pop up a user dialog.  <name> is "No Name" or a previously tried name
    // which was not a valid OPT name.  Returns the the user selected name
    public String getOPTname(Component parent, String name) {
	String message;
	if (name.equals("No Name")) {
	    message = new String("Pick the OPT to monitor");
	} else {
	    message = new String(name + 
				 " is not a valid OPT name\nChoose another");
	}
	// Present the user a standard list of names
	Object[] optList = { "Opt01", "Opt02", "Opt03", "Opt04", "Opt05", 
			     "Opt06", "OptSim", "None of Above"};
	String selection = (String)JOptionPane.showInputDialog(parent, message, 
				     "Choose OPT", 
				     JOptionPane.PLAIN_MESSAGE,
				     null, optList, optList[0]);
	if (selection == null) {
	    System.exit(1);
	}
	if (selection.equals("None of Above")) {
	    // Give the user the option to type a name not in the standard list
	    selection = (String)JOptionPane.showInputDialog(parent,
					"Type the OPT name",
					"Choose OPT", 
					JOptionPane.PLAIN_MESSAGE,
					null, null, null);
	    if (selection == null) {
		System.exit(1);
	    }
	}
	return selection;
    }

    class showOptsRunnable implements Runnable {
        public showOptsRunnable(ALMAFrame frame) {
            comp = frame;
        }

        public void run() {

	    int nPix = statOPT.nRow * statOPT.nCol;
	    int iWant = 0;
	    float timeOut = .3F;	// Time out for no image ready
	    int ret = 0;
	    statOPT.OPTrunning[0] = 0;
	    String name;

	    // Get the current image, if any
	    do {
		try {
		    if (pauseOPTmonitor) {
			if (statOPT.OPTrunning[0] != IDLE) {
			    statOPT.OPTrunning[0] = IDLE;
			    status.update();
			}
			Thread.sleep(DOZE); // Skip monitoring
			continue;
		    } else {
			// Check the OPT status
			ret = statOPT.checkOpt(iWant, timeOut);
		    }
		}
		catch (InterruptedException e) {
		}
		catch (java.net.SocketException e) {
		    String errMsg = e.getMessage();
		    System.out.print(e.getMessage() + ": ");
		    System.out.println("Socket Exception in checkOpt");
		    ret = -999;
		    if ((errMsg.indexOf("Send failed") >= 0) || 
			(errMsg.indexOf("Response failed") >=0)) {
			// OPT server is not responding or is not running
			if (statOPT.OPTrunning[0] != BAD) {
			    statOPT.OPTrunning[0] = BAD; // Change status to Bad
			    statOPT.statusChanged = 1; // Update status display
			}
		    } else if (errMsg.indexOf("Could not connect to ") >= 0) {
			// An invalid OPT name was specified
			// Convert that name back to a String
			name = new String(statOPT.optName);
			// Ask for another name
			name = getOPTname(comp, name);
			setTitle(name + " Monitor");
			// Convert to bytes for C++
			statOPT.optName = name.getBytes();
			continue;
		    }
		}
		catch (IllegalArgumentException e) {
		    System.out.print(e.getMessage() + " :");
		    System.out.println("Illegal Argument Exception in checkOpt");
		    ret = -999;
		}
		catch (NullPointerException e) {
		    System.out.print(e.getMessage() + " :");
		    System.out.println("Null Pointer Exception in checkOpt");
		    ret = -999;
		}
		if (ret == -999) {
		    if (statOPT.statusChanged != 0) {
			status.update();
		    }
		    try {
			Thread.sleep(DOZE);
		    } catch (InterruptedException e) {
		    }
		    continue;
		} else {
		    if (statOPT.OPTrunning[0] != OK) {
			statOPT.OPTrunning[0] = OK;
			statOPT.statusChanged = 1;
		    }
		}
		if (ret > 0) {
		    iWant = 0;
		    String imageName = new String("Image " + statOPT.seqNo);
		    imageLabel.setText(imageName);

		    // Display the image, applying the Gain, Offset, and Zoom
		    statOPT.makeImage(comp);
		    status.update(); // Update the status display

		    // Add new status items to the arrays and update the plots
		    statPlot.appendData(); 
		} else if (ret < 0) {
		    // There's an image available, but it's the wrong size
		    // Resize the Java arrays
		    statOPT.resize(statOPT.newRow, statOPT.newCol, statOPT.centBox);
		    nPix = statOPT.nRow * statOPT.nCol;
		    iWant = statOPT.seqNo; // Try again for the image
		} else {
		    if (statOPT.statusChanged != 0) {
			// One of the status values changed.  Update the display
			status.update();
		    }
		    iWant = 0;
		}
		if (ret == 0 || ret == 1 || ret == -999) {
		    // Sleep for DOZE milliseconds
		    try {
			Thread.sleep(DOZE);
		    } 
		    catch (InterruptedException e) {
		    }
		}
	    } while (true);
	    

        }

        private ALMAFrame comp;
	private static final int DOZE = 300; // Time between status checks
    }

	    

    public static final int DEFAULT_WIDTH = 845;
    public static final int DEFAULT_HEIGHT = 700;

    private static final int FONTSIZE = 11;
    private static final int TITLEFONTSIZE = 14;
    public static final Font labelFont = 
	new Font("SansSerif", Font.PLAIN, FONTSIZE);
    private static final int BAD = optStatus.name2State("Bad");
    private static final int OK = optStatus.name2State("OK");
    private static final int IDLE = optStatus.name2State("Idle");

    private optStatus statOPT;

    private JPanel topPanel;
    private JCheckBox writeLog ;
    private JCheckBox writeFITS;
    private JCheckBox pauseMonitor;
    private boolean pauseOPTmonitor = false; // Set to pause monitoring of OPT
    private JLabel imageLabel;

    public starImage displayImage;
    public statusInfo status;
    public statusPlot statPlot;

    private ALMAFrame theFrame;
}
