There are two new files, statusPlotALMA.java and statusPlotACE.java,
which only differ in the number of status entries saved for the plots:
1000 and 10000, respectively.  When ACE does overnight tests, more
than 1000 images may be exposed so statusPlotACE.java increases the
array sizes to 10000 and adds extra items to the Data Shown menu.
This will decrease the amount of memory in the Java Virtual Machine
available for zooming and so will decrease the maximum Zoom Factor
allowed for the GUI.

Neither of these is referenced by the Makefile.  Rather, the one
desired should be copied to the file statusPlot.java before doing a
make command.  Currently, statusPlotALMA.java has been copied to that
file.
