/* Pixel scale on OPT image */

/*
 * Copyright (c) 1995 - 2008 Sun Microsystems, Inc.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Sun Microsystems nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 

// package components;
package alma.Control.device.gui.OpticalTelescope;

import java.lang.Math;
import java.awt.*;
import javax.swing.*;

/* Original Rule.java is used by ScrollDemo.java. */

public class Rule extends JComponent {
    public static final int INCH = Toolkit.getDefaultToolkit().
            getScreenResolution(); // Dots per inch on the screen
    public static final int HORIZONTAL = 0;
    public static final int VERTICAL = 1;
    public static final int SIZE = 35; // HORIZONTAL height or VERTICAL Width
    
    public int orientation;	// .HORIZONTAL or .VERTICAL
    public double lowPix;	// Original image's pixel number of first pixel
    public double highPix;	// Original image's pixel number of last pixel
    public double zoomFactor;	// Original pixel size / current size

// Conversion from screen dots to pixel units
// Horizontal: Pixel = lowPix + dots/zoomFactor
// Vertical: Pixel = highPix - dots/zoomFactor 

    private static final double HSPACING = 0.6; // Horiz. labeled tick incr (inch)
    private static final double VSPACING = 0.5; // Vert. labeled ticks incr (inch)
    private static final int LITTLETICK = 7;	// Length of minor tick dash (dots)
    private static final int BIGTICK = 10;	// Length of major tick dash (dots)

    private double tickMinor;	// Increment between minor ticks in pixel units
    private double tickMajor;	// Increment between major ticks in pixel units
    private String tickFmt;	// Printf style format for the tickMajor label

    // Draw a horizontal or vertical ruler marking the pixel numbers

    // Constructor parameters are the orientation code (HORIZONTAL or 
    // VERTICAL), the lowest pixel number, the highest pixel number,
    // and the zoomFactor applied to the original image

    // A full original frame has pixels 1 to 1024, but an image might not be
    // full frame and a zoomFactor may have been applied.  Therefore the
    // first pixel may not be numbered 1 and the increment between pixel
    // numbers in the original image may differ from the image displayed
    // by the the zoomFactor
    
    public Rule(int o, double low, double high, double zFactor) {
        orientation = o;
	lowPix = low;
	highPix = high;
	zoomFactor = zFactor;
        setTickSpacing();	// Set the tick spacings to round pixel units
    }

    // Set the tick spacings to round pixel units
    private void setTickSpacing() {
	// Preferred distance between Major ticks
	double preferred = (orientation == HORIZONTAL) ? HSPACING : VSPACING;
	double pixInc = preferred*INCH/zoomFactor; // No. of pixels in <preferred>
	double[] rounded = roundTick(pixInc);     // Rounded values
	tickMinor = rounded[0];			  // Minor tick interval
	tickMajor = rounded[1];			  // Labeled tick interval
	if (rounded[2] < 0) {
	    tickFmt = String.format("%%.%df", (int)(-rounded[2]));
	} else {
	    tickFmt = "%.0f";
	}
    }

    public int getIncrement() {
        return (int)Math.rint(tickMinor*zoomFactor);
    }

    public void setPreferredHeight(int ph) {
        setPreferredSize(new Dimension(SIZE, ph));
    }

    public void setPreferredWidth(int pw) {
        setPreferredSize(new Dimension(pw, SIZE));
    }

    // Change the Zoom factor
    public void setZoomFactor(double zF) {
	zoomFactor = zF;
        setTickSpacing();	// Set the tick spacings to round pixel units
    }

    // Change the pixel numbering
    public void setLowHigh(double low, double high) {
        lowPix = low;
        highPix = high;
	repaint();		// Redraw this ruler
    }

    protected void paintComponent(Graphics g) {
        Rectangle drawHere = g.getClipBounds();
        // Do the ruler labels in a small font that's black.
        g.setFont(new Font("SansSerif", Font.PLAIN, 10));
        g.setColor(Color.black);
	final FontMetrics fm = g.getFontMetrics();

        // Some vars we need.
        double end = 0;		// Position of last (right or bottom) tick mark
        double start = 0;	// Position of first (left or top) tick mark
	int tickLength = 0;	// Length of tick mark (dots)
        int textLen = 0;	// Length of tick label (dots)
        String text = null;	// Tick label (if any)
        double Pix;		// Original image pixel units
	

        // Use clipping bounds to calculate first and last tick locations.
        if (orientation == HORIZONTAL) {
	    Pix = lowPix + drawHere.x/zoomFactor; // Pixel number at left edge
	    Pix = Math.rint(Pix / tickMinor) * tickMinor;
	    start = (int)Math.rint((Pix - lowPix)*zoomFactor);
	    if (start < 0) {
		start = (int)Math.rint((Pix + tickMinor - lowPix)*zoomFactor);
	    }
	    end = Math.rint((highPix - lowPix)*zoomFactor);
	    end = Math.min(end, drawHere.x + drawHere.width);
        } else {
	    Pix = highPix - drawHere.y/zoomFactor; // Pixel number at top edge
	    Pix = Math.rint(Pix / tickMinor) * tickMinor;
	    start = (int)Math.rint((highPix - Pix)*zoomFactor);
	    if (start < 0) {
		start = (int)Math.rint((tickMinor + highPix - Pix)*zoomFactor);
	    }
	    end = Math.rint((highPix - lowPix)*zoomFactor);
	    end = Math.min(end, drawHere.y + drawHere.height);
        }

        // ticks and labels
	double t;
	int i;
        for (double pos = start; pos < end; pos += tickMinor*zoomFactor) {
	    if (orientation == HORIZONTAL) {
		t = (lowPix + pos/zoomFactor)/tickMajor;	// Labeled tick number
	    } else {
		t = (highPix - pos/zoomFactor)/tickMajor;
	    }
	    if (Math.abs(t-Math.rint(t)) < 0.1)  {
		tickLength = BIGTICK; // Labeled tick
		text = String.format(tickFmt, Math.rint(t)*tickMajor);
		textLen = fm.stringWidth(text);
	    } else {
		tickLength = LITTLETICK;
		text = null;
	    }
	    i = (int)Math.rint(pos);
	    if (orientation == HORIZONTAL) {
		g.drawLine(i, SIZE-1, i, SIZE-tickLength-1);
		if (text != null) {
		    g.drawString(text, i-textLen/2, 21);
		}
	    } else {
		g.drawLine(SIZE-1, i, SIZE-tickLength-1, i);
		if (text != null) {
		    g.drawString(text, Math.max(0, SIZE-tickLength-3-textLen), i+4);
		}
	    }
        }
    }

    // Round the specified majorInc to the nearest (1, 2, or 5) * a power of 10
    // The corresponding minorInc will be (0.5, 1, or 1) * the same power of 10
    // Returned in a double[3] array in elements [1] & [0], respectively
    // Element [3] contains the power of 10
    private double[] roundTick(double majorInc) {
	double[] tickInc= new double[3];
	double x, pwr, facMaj, facMin;	
	final double NEXTPOW = Math.log10(10./7.);

	x = Math.log10(majorInc);
	// Round up to the next power of 10 if have (7 to 9.99) * power of 10
	pwr = (int)Math.floor(x+NEXTPOW);	// Nearest power of 10
	facMaj = Math.pow(10., x-pwr);	// majorInc = x * 10^pwr
	if (facMaj < 1.5) {
	    facMaj = 1;
	    facMin = 0.5;
	} else if (facMaj < 3.5) {
	    facMaj = 2;
	    facMin = 1;
	} else {
	    facMaj = 5;
	    facMin = 1;
	}
	tickInc[0] = facMin*Math.pow(10.,pwr);
	tickInc[1] = facMaj*Math.pow(10.,pwr);
	tickInc[2] = pwr;
       	return tickInc;
    }
}
