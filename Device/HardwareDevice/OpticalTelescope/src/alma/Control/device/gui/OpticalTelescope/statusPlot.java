// TODO: SRankin - resolve compile problems in commented out code.

package alma.Control.device.gui.OpticalTelescope;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.font.*;
import java.awt.geom.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.imageio.*;
import javax.imageio.stream.*;
import java.io.*;
import java.text.DecimalFormat;
import java.text.Format;
import java.util.Iterator;
import java.util.Arrays;
import java.util.Date;
import java.util.Scanner;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
// SRankin
// import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.data.general.Series;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.time.FixedMillisecond;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;


public class statusPlot extends JPanel implements ActionListener {

    public statusPlot(ALMAFrame frame, optStatus newStatOPT) {
	statOPT = newStatOPT;
	setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
	Dimension frameSize = frame.getSize();
	setPreferredSize(new Dimension((int)Math.round(frameSize.width/2.2), 
		(int)Math.round(frameSize.height/2.4)));
	XYDataset plotData;
	
	// Construct concatenated arrays
	int j = 0;
	for (int i=0; i < xArray.length; i++) {
	    arrayPtr[j] = xArray[i];
	    arrayName[j] = xArrayName[i];
	    arrayAxLab[j++] = xArrayAxLab[i];
	}
	for (int i=0; i < yArray.length; i++) {
	    arrayPtr[j] = yArray[i];
	    arrayName[j] = yArrayName[i];
	    arrayAxLab[j++] = yArrayAxLab[i];
	}

	for (int i=0; i < arrayName.length; i++) {
	    arrayIsCentroid[i] = (needsCentroid.indexOf(arrayName[i]) >= 0);
	}

	// Left Plot (# 1)
	Series plotSeries = createSeries(0, defaultPlot1X, defaultPlot1Y);
	if (plotSeries instanceof MySeries) {
	    plotData = new XYSeriesCollection((MySeries)plotSeries);
	    chart1 = createXYChart((XYSeriesCollection)plotData, null, 
			       			plot1Xlab, plot1Ylab);
	} else {
	    plotData = new TimeSeriesCollection((MyTSeries)plotSeries);
	    chart1 = createXYChart((TimeSeriesCollection)plotData, null, 
			       			plot1Xlab, plot1Ylab);
	}
			
	plot1Xlab = arrayAxLab[xIndex];
	plot1Ylab = arrayAxLab[yIndex];

	plot1plot = (XYPlot)chart1.getPlot();
	ChartPanel panel1 = new ChartPanel(chart1);
	// SRankin
	// panel1.setMouseWheelEnabled(true);
	plot1 = new JPanel();
	plot1.setLayout(new BoxLayout(plot1, BoxLayout.PAGE_AXIS));

	// Make titled combo boxes
	x1box = new plotBox('X', 1, xArrayName, yArrayName, "X axis");
	d1box = new plotBox('D', 1, showDataName, null, "Data Shown");
	y1box = new plotBox('Y', 1, yArrayName, null, "left Y axis");
	y1box.comboBox.addItem(xArrayName[0]); // Add SeqNo
	r1box = new plotBox('R', 1, yArrayName, null, "right Y axis");
	r1box.comboBox.addItem(xArrayName[0]); // Add SeqNo
	r1box.comboBox.insertItemAt("None", 0);

	// Initialize combo boxes
	connectPts1 = new JCheckBox("do Lines");
	x1box.comboBox.setSelectedItem(defaultPlot1X);
	d1box.comboBox.setSelectedItem("All");
	y1box.comboBox.setSelectedItem(defaultPlot1Y);
	r1box.comboBox.setSelectedItem("None");

	JPanel comboPanel = new JPanel();
	// Two above the plot
	comboPanel.add(y1box);	// Left Y axis

	// Make a labeled text box for the vertical gap when there are 2 Y axes
	gap1panel = new gapPanel(r1box);
	comboPanel.add(gap1panel);
	comboPanel.add(r1box);	// Right Y axis
	plot1.add(comboPanel);

	plot1.add(panel1);	// Put in the plot

	// Two below the plot
	comboPanel = new JPanel();
	comboPanel.add(d1box);	// Show data amount
	comboPanel.add(x1box);	// X axis
	connectPts1.setHorizontalTextPosition(SwingConstants.CENTER);
	connectPts1.setVerticalTextPosition(SwingConstants.TOP);
	connectPts1.setFont(plotBoxFont);
	connectPts1.addActionListener(this);
	comboPanel.add(connectPts1);
	plot1.add(comboPanel);
	add(plot1);

	// Add a vertical separator
	add(new JSeparator(SwingConstants.VERTICAL));

	// Right Plot (# 2)
	plotSeries = createSeries(0, defaultPlot2X, defaultPlot2Y);
	if (plotSeries instanceof MySeries) {
	    plotData = new XYSeriesCollection((MySeries)plotSeries);
	    chart2 = createXYChart((XYSeriesCollection)plotData, null, 
			       				plot2Xlab, plot2Ylab);
	} else {
	    plotData = new TimeSeriesCollection((MyTSeries)plotSeries);
	    chart2 = createXYChart((TimeSeriesCollection)plotData, null, 
			       				plot2Xlab, plot2Ylab);
	}

	plot2Xlab = arrayAxLab[xIndex];
	plot2Ylab = arrayAxLab[yIndex];

	plot2plot = (XYPlot)chart2.getPlot();
	ChartPanel panel2 = new ChartPanel(chart2);
	// SRankin
	// panel2.setMouseWheelEnabled(true);
	plot2 = new JPanel();
	plot2.setLayout(new BoxLayout(plot2, BoxLayout.PAGE_AXIS));


	// Make titled combo boxes
	x2box = new plotBox('X', 2, xArrayName, yArrayName, "X axis");
	d2box = new plotBox('D', 2, showDataName, null, "Data Shown");
	y2box = new plotBox('Y', 2, yArrayName, null, "left Y axis");
	y2box.comboBox.addItem(xArrayName[0]); // Add SeqNo
	r2box = new plotBox('R', 2, yArrayName, null, "right Y axis");
	r2box.comboBox.addItem(xArrayName[0]); // Add SeqNo
	r2box.comboBox.insertItemAt("None", 0);

	// Initialize combo boxes
	connectPts2 = new JCheckBox("do Lines");
	x2box.comboBox.setSelectedItem(defaultPlot2X);
	d2box.comboBox.setSelectedItem("All");
	y2box.comboBox.setSelectedItem(defaultPlot2Y);
	r2box.comboBox.setSelectedItem("None");
	comboPanel = new JPanel();

	// Two above the plot
	comboPanel.add(y2box);	// Left Y axis

	// Make a labeled text box for the vertical gap when there are 2 Y axes
	gap2panel = new gapPanel(r2box);
	comboPanel.add(gap2panel);
	comboPanel.add(r2box);	// Right Y axis
	plot2.add(comboPanel);

	plot2.add(panel2);	// Put in the plot

	// Two below the plot
	comboPanel = new JPanel();
	comboPanel.add(d2box);	// Show data amount
	comboPanel.add(x2box);	// X axis
	connectPts2.setHorizontalTextPosition(SwingConstants.CENTER);
	connectPts2.setVerticalTextPosition(SwingConstants.TOP);
	connectPts2.setFont(plotBoxFont);
	connectPts2.addActionListener(this);
	comboPanel.add(connectPts2);
	plot2.add(comboPanel);
	add(plot2);
    }

    public void actionPerformed(ActionEvent e) {

	XYLineAndShapeRenderer renderer;
	JCheckBox cb = (JCheckBox)e.getSource();
	if (cb == connectPts1) {
	    renderer = (XYLineAndShapeRenderer)plot1plot.getRenderer(0);
	    renderer.setSeriesLinesVisible(0, cb.isSelected());
	    renderer = (XYLineAndShapeRenderer)plot1plot.getRenderer(1);
	    renderer.setSeriesLinesVisible(0, cb.isSelected());
	} else if (cb == connectPts2) {
	    renderer = (XYLineAndShapeRenderer)plot2plot.getRenderer(0);
	    renderer.setSeriesLinesVisible(0, cb.isSelected());
	    renderer = (XYLineAndShapeRenderer)plot2plot.getRenderer(1);
	    renderer.setSeriesLinesVisible(0, cb.isSelected());
	} else {
	    System.out.println("Ignored ActionEvent from " + 
			       cb.getClass().getName());
	}
    }

    // Return a JFreeChart for the specified dataset
    private JFreeChart createXYChart(XYDataset dataset, String title, 
				   String Xlabel, String Ylabel) {
	int kind;
	JFreeChart chart;
	if (dataset instanceof XYSeriesCollection) {
	    kind = 0;
	    chart = ChartFactory.createXYLineChart(
			title,   			 // chart title
			Xlabel,                          // domain axis label
			Ylabel,                          // range axis label
		        dataset, 			 // data
			PlotOrientation.VERTICAL, 	 // orientation
			true,                            // include legend
			true,                            // tooltips
			false                            // urls
		);
	    xAxis = (NumberAxis)((XYPlot)chart.getPlot()).getDomainAxis();
	    dAxis = new DateAxis();
	} else {
	    kind = 4;
	    chart = ChartFactory.createTimeSeriesChart(
			title,   			 // chart title
			Xlabel,                          // domain axis label
			Ylabel,                          // range axis label
		        dataset, 			 // data
			true,                            // include legend
			true,                            // tooltips
			false                            // urls
		);
	    dAxis = (DateAxis)((XYPlot)chart.getPlot()).getDomainAxis();
	    xAxis = new NumberAxis();
	}
	XYPlot plot = (XYPlot)chart.getPlot();
	XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
	plot.setRenderer(renderer);
	renderer.setBaseShapesVisible(true);
	renderer.setBaseShapesFilled(false);
	renderer.setUseFillPaint(false);
	renderer.setSeriesShapesVisible(0, true);
	renderer.setSeriesOutlineStroke(0, new BasicStroke(1.0f));
	renderer.setSeriesStroke(0, new BasicStroke(1.0f));
	renderer.setSeriesLinesVisible(0, xIndex < xArray.length);	
	XYLineAndShapeRenderer renderer2 = null;
	try {
	    renderer2 = (XYLineAndShapeRenderer)renderer.clone();
	}
	catch (CloneNotSupportedException e) {
	    System.out.println("Could not clone the renderer: " + e.getMessage());
	}
	    
	double symSize = 4.;	// Symbol size for points on the plot
	double symPos = -symSize/2;

	// Dataset 0 uses circles
	renderer.setSeriesShape(0, new Ellipse2D.Double(symPos, symPos, 
							symSize, symSize));
	// Dataset 1 uses squares
	renderer2.setSeriesShape(0, new Rectangle2D.Double(symPos, symPos,
							  symSize, symSize));
	plot.setRenderer(1, renderer2);

	Font axisFont = new Font("Sanserif", Font.PLAIN, 12);
	Font tickFont = new Font("Sanserif", Font.PLAIN, 10);

	// Set up both kinds of X axis
	xAxis.setAutoRangeIncludesZero(false);
	xAxis.setLabelFont(axisFont);
	xAxis.setTickLabelFont(tickFont);

	dAxis.setLabelFont(axisFont);
	dAxis.setTickLabelFont(tickFont);

	if (kind == 0) {
	    plot.setDomainAxis(xAxis);
	} else {
	    plot.setDomainAxis(dAxis);
	}

	// Set up the left Y axis
	NumberAxis yAxis = (NumberAxis)plot.getRangeAxis();
	yAxis.setAutoRangeIncludesZero(false);
	yAxis.setAutoRangeStickyZero(false);
	yAxis.setLabelFont(axisFont);
	yAxis.setTickLabelFont(tickFont);
	defaultMargin = yAxis.getLowerMargin();

	// Set up right Y axis
	NumberAxis rAxis = new NumberAxis();
	rAxis.setVisible(false); // Invisible until its needed
	rAxis.setAutoRangeIncludesZero(false);
	rAxis.setAutoRangeStickyZero(false);
	rAxis.setLabelFont(axisFont);
	rAxis.setTickLabelFont(tickFont);
	rAxis.setAutoRange(true);
        rAxis.setLabelPaint(Color.blue);
        rAxis.setTickLabelPaint(Color.blue);

	plot.setRangeAxis(1, rAxis);
	plot.mapDatasetToRangeAxis(1, 1);
	return chart;
    }

    // Create an XYSeries of data points (plotX, plotY) found in the arrays
    // corresponding to these strings (which must be elements of arrayName[])
    // <first> is the array index of the oldest point in the plot
    private Series createSeries(int first, String plotX, String plotY) {
        MySeries xySeries = null;
        MyTSeries tmSeries = null;
 	int kind = findArrays(plotX, plotY);
	if (kind < 4) {
	    xySeries = new MySeries(plotY);
	    return modifySeries(xySeries, first, plotX, plotY, kind);
	} else {
	    tmSeries = new MyTSeries(plotY);
	    return modifySeries(tmSeries, first, plotX, plotY, kind);
	}
    }

    // Clear an existing MySeries <xySeries> and load it with data points
    // (plotX, plotY) found in the arrays corrersponding to these strings
    // <first> is the array index of the oldest point in the plot
    // kind is the return value of findArrays(plotX, plotY) which also 
    // sets the values of the [f|d|t][X|Y]array variables.
    private Series modifySeries(Series theSeries, int first, 
				String plotX, String plotY, int kind) {
	int i;
	MySeries xySeries = null;
	MyTSeries tmSeries = null;
	boolean yNeedsValid = arrayIsCentroid[yIndex];
	boolean xNeedsValid = arrayIsCentroid[xIndex];

	if (kind < 4) {
	    xySeries = (MySeries)theSeries;
	    xySeries.setKind(kind);
	    xySeries.setIndices(xIndex, yIndex);
	    xySeries.setXCentroid(xNeedsValid);
	    xySeries.setYCentroid(yNeedsValid);
	    xySeries.setKey(plotY);
	    xySeries.setDescription(plotY + " vs " + plotX);
	    xySeries.clear();	// Delete existing items, if any
	} else {
	    tmSeries = (MyTSeries)theSeries;
	    tmSeries.setKind(kind);
	    tmSeries.setIndices(xIndex, yIndex);
	    tmSeries.setXCentroid(xNeedsValid);
	    tmSeries.setYCentroid(yNeedsValid);
	    tmSeries.setKey(plotY);
	    tmSeries.setDescription(plotY + " vs " + plotX);
	    tmSeries.clear();	// Delete existing items, if any
	}
	// Load data from index <first> to the end
	for (i=first; i != arrayIn; i++) {
	    if (i >= MAXPOINTS) {
		i -= MAXPOINTS;
	    }
	    if (xNeedsValid && valid[i] == 0) {
		continue;	// X value is invalid.  Skip both X and Y
	    }
	    if (yNeedsValid && valid[i] == 0) {
		// Y value is invalid
		switch (kind) {
		case 0:		// Two float arrays
		case 2:		// X float, Y double
		    xySeries.add((double) fXarray[i], null);
		    break;
		case 4:		// X Date, Y float
		case 6:		// X Date, Y double
		    tmSeries.addOrUpdate(new FixedMillisecond(tXarray[i]), 
					 			null);
		    break;
		case 1:		// X double, Y float
		case 3:		// Two double arrays
		    xySeries.add(dXarray[i], null);
		    break;
		default:
		    System.out.println("Bad kind");
		    return null; // Bad kind
		}
	    } else {
		switch (kind) {
		case 0:		// Two float arrays
		    xySeries.add((double) fXarray[i], (double)fYarray[i]);
		    break;
		case 1:		// X double, Y float
		    xySeries.add(dXarray[i], (double) fYarray[i]);
		    break;
		case 2:		// X float, Y double
		    xySeries.add((double) fXarray[i], dYarray[i]);
		    break;
		case 3:		// Two double arrays
		    xySeries.add(dXarray[i], dYarray[i]);
		    break;
		case 4:		// X Date, Y float
		    tmSeries.addOrUpdate(new FixedMillisecond(tXarray[i]), 
						 (double) fYarray[i]);
		    break;
		case 6:		// X Date, Y double
		    tmSeries.addOrUpdate(new FixedMillisecond(tXarray[i]), 
					 			dYarray[i]);
		    break;
		default:
		    System.out.println("Bad kind");
		    return null; // Bad kind
		}
	    }
	}
	return theSeries;
    }

    // Set array pointers [f|d|t][X|Y]array to point to the X and Y axis arrays
    // arrays to be plotted specified by the Strings plotX and plotY
    // Return value indicates which are float arrays:
    // 0: both X & Y are float
    // 1: X is double, Y is float
    // 2: X is float, Y is double
    // 3: both X and Y are double
    // 4: X is Date, Y is float
    // 6: X is Date, Y is double
    // -1: One of the String's specified don't correspond to an array
    // plotX and plotY must be strings found in arrayName[]
    // xIndex & yIndex record the position in arrayName where they were found
    int findArrays(String plotX, String plotY) {
	int ret = 0;
	fXarray = null;
	dXarray = null;
	tXarray = null;
	fYarray = null;
	dYarray = null;
	for (xIndex=0; xIndex < arrayName.length; xIndex++) {
	    if (plotX.equals(arrayName[xIndex])) {
		String clName = arrayPtr[xIndex].getClass().getName();
		if (clName.equals("[F")) {
		    fXarray = (float [])arrayPtr[xIndex];
		    ret = 0;
		} else if (clName.equals("[Ljava.util.Date;")) {
		    tXarray = (Date [])arrayPtr[xIndex];
		    ret = 4;
		} else {
		    dXarray = (double [])arrayPtr[xIndex];
		    ret = 1;
		}
		break;
	    }
	}
	if (xIndex == arrayName.length) {
	    System.out.println("Could not find xArray: " + plotX);
	    return -1;
	}
	for (yIndex=0; yIndex < arrayName.length; yIndex++) {
	    if (plotY.equals(arrayName[yIndex])) {
		if (arrayPtr[yIndex].getClass().getName().equals("[F")) {
		    fYarray = (float [])arrayPtr[yIndex];
		} else {
		    dYarray = (double [])arrayPtr[yIndex];
		    ret += 2;
		}
		break;
	    }
	}
	if (yIndex == arrayName.length) {
	    System.out.println("Could not find yArray: " + plotY);
	    return -1;
	}
	return ret;
    }

    // Append the data in the current optStatus object to the status data arrays
    public void appendData() {
	CCD_Temp[arrayIn] = statOPT.CamTemp[0];
	focusPos[arrayIn] = statOPT.FocusPos[0];
	sunSensorVolts1[arrayIn] = statOPT.SunSensor[2];
	sunSensorVolts2[arrayIn] = statOPT.SunSensor[3];
	tubeTemp[arrayIn] = (float)statOPT.tempTube;
	filtTemp[arrayIn] = (float)statOPT.tempFilt;
	seqNo[arrayIn] = (float) statOPT.centroidValue[optStatus.CENTNUM];
	valid[arrayIn] = statOPT.centroidValue[optStatus.VALID];
	centPeak[arrayIn] = statOPT.centroidValue[optStatus.CENTPEAK];
	centX[arrayIn] = statOPT.centroidValue[optStatus.CENTX];
	centY[arrayIn] = statOPT.centroidValue[optStatus.CENTY];
	widthX[arrayIn] = statOPT.centroidValue[optStatus.WIDTHX];
	widthY[arrayIn] = statOPT.centroidValue[optStatus.WIDTHY];
	RMS[arrayIn] = statOPT.centroidValue[optStatus.RMS];
	expTime[arrayIn] = statOPT.centroidValue[optStatus.EXPTIME];
	timeStamp[arrayIn] = 
	    new Date((long)(statOPT.centroidValue[optStatus.TIMESTAMP]*1000.));

	// Advance the insertion index to the place for the next new value
	int oldIn = arrayIn;
        if (++arrayIn >= MAXPOINTS) {
	    arrayIn -= MAXPOINTS; 	// Wrap back to the beginning
	}
	if (arrayIn == arrayOut) {
	    // Buffer is full.  Advance the Out index
	    if (++arrayOut >= MAXPOINTS) {
		arrayOut -= MAXPOINTS;
	    }
	}

	// Append the plots with the new data
	firstPlot1data = append2Plot(oldIn, plot1plot, firstPlot1data);
	firstPlot2data = append2Plot(oldIn, plot2plot, firstPlot2data);
    }

    // Add a data point to each Dataset of the plot
    // oldIn is the index of the new data.  If the oldest point in the plot
    // came from this location, delete that point
    // Returns the index of the (new) oldest point
    public int append2Plot(int oldIn, XYPlot plotPlot, 
			  int firstData) {
	MySeries xySeries = null;
	MyTSeries tmSeries = null;
	boolean xNeedsValid, yNeedsValid;
	int kind, xIndex, yIndex;

	int numDataset = plotPlot.getDatasetCount();
	for (int i=0; i < numDataset; i++) {
	    XYDataset ds = plotPlot.getDataset(i);
	    if (ds == null) {
		continue;
	    }
	    if (ds instanceof XYSeriesCollection) {
		xySeries = (MySeries) ((XYSeriesCollection)ds).getSeries(0);
		xNeedsValid = xySeries.xIsCentroid();
		yNeedsValid = xySeries.yIsCentroid();
		kind = xySeries.getKind();
		xIndex = xySeries.getXindex();
		yIndex = xySeries.getYindex();
	    } else {
		tmSeries = (MyTSeries) ((TimeSeriesCollection)ds).getSeries(0);
		xNeedsValid = tmSeries.xIsCentroid();
		yNeedsValid = tmSeries.yIsCentroid();
		kind = tmSeries.getKind();
		xIndex = tmSeries.getXindex();
		yIndex = tmSeries.getYindex();
	    }

	    // Set the two __array pointers needed
	    switch (kind) {	
	    case 0:		// Two float arrays
		fXarray = ((float[]) arrayPtr[xIndex]);
		fYarray = ((float[]) arrayPtr[yIndex]);
		break;
	    case 1:		// X double, Y float
		dXarray = ((double[]) arrayPtr[xIndex]);
		fYarray = ((float[]) arrayPtr[yIndex]);
		break;
	    case 2:		// X float, Y double
		fXarray = ((float[]) arrayPtr[xIndex]);
		dYarray = ((double[]) arrayPtr[yIndex]);
		break;
	    case 3:		// Two double arrays
		dXarray = ((double[]) arrayPtr[xIndex]);
		dYarray = ((double[]) arrayPtr[yIndex]);
		break;
	    case 4:		// X Date, Y float
		tXarray = ((Date[]) arrayPtr[xIndex]);
		fYarray = ((float[]) arrayPtr[yIndex]);
		break;
	    case 6:		// X Date, Y double
		tXarray = ((Date[]) arrayPtr[xIndex]);
		dYarray = ((double[]) arrayPtr[yIndex]);
	    }
		
	    if (oldIn == firstData && oldIn != arrayOut) {
		// The oldest point is now history
		if (kind < 4) {
		    xySeries.delete(0, 0); 
		} else {
		    tmSeries.delete(0,0);
		}
	    }

	    if (! (xNeedsValid && valid[oldIn] == 0)) {
		if (yNeedsValid && valid[oldIn] == 0) {
		    // Y value is invalid
		    switch (kind) {
		    case 0:		// Two float arrays
		    case 2:		// X float, Y double
			xySeries.add( (double)fXarray[oldIn], null);
			break;
		    case 1:		// X double, Y float
		    case 3:		// Two double arrays
			xySeries.add( dXarray[oldIn], null);
			break;
		    case 4:		// X Date, Y float
		    case 6:		// X Date, Y double
			tmSeries.addOrUpdate(new FixedMillisecond(
							 tXarray[oldIn]), null);
		    }
		} else {
		    switch (kind) {
		    case 0:		// Two float arrays
			xySeries.add( (double)fXarray[oldIn], 
				      		(double)fYarray[oldIn]);
			break;
		    case 1:		// X double, Y float
			xySeries.add( dXarray[oldIn], (double)fYarray[oldIn]);
			break;
		    case 2:		// X float, Y double
			xySeries.add( (double)fXarray[oldIn], dYarray[oldIn]);
			break;
		    case 3:		// Two double arrays
			xySeries.add( dXarray[oldIn], dYarray[oldIn]);
			break;
		    case 4:		// X Date, Y float
			tmSeries.addOrUpdate (new FixedMillisecond(
				       tXarray[oldIn]), (double)fYarray[oldIn]);
			break;
		    case 6:		// X Date, Y double
			tmSeries.addOrUpdate (new FixedMillisecond(
					       tXarray[oldIn]), dYarray[oldIn]);
		    }
		}
	    }
	}
	if (oldIn == firstData && oldIn != arrayOut) { 
	    // Advance to new oldest point
	    if (++firstData >= MAXPOINTS) {
		firstData -= MAXPOINTS;
	    }
	}
	return firstData;
    }
	
    // Remove the number of items specified from the beginning of the 
    // status data arrays.  <= 0 means remove all items
    public void eraseData(int numErase) {
	XYDataset plotData;
	int numHave = arrayIn - arrayOut; // Get the current number of items
	if (numHave < 0) {
	    numHave += MAXPOINTS;
	}
	if (numErase <= 0 || numErase > numHave) {
	    // Erasing all the points
	    arrayIn = arrayOut = firstPlot1data = firstPlot2data = 0;
	    // Delete all the points from the plots
	    plotData = plot1plot.getDataset(0);
	    if (plotData instanceof XYSeriesCollection) {
		((XYSeriesCollection)plotData).getSeries(0).clear();
	    } else {
		((TimeSeriesCollection)plotData).getSeries(0).clear();
	    }
	    plotData = plot2plot.getDataset(0);
	    if (plotData instanceof XYSeriesCollection) {
		((XYSeriesCollection)plotData).getSeries(0).clear();
	    } else {
		((TimeSeriesCollection)plotData).getSeries(0).clear();
	    }
	} else {
	    int oldOut = arrayOut;
	    arrayOut += numErase;
	    if (arrayOut >= MAXPOINTS) {
		arrayOut -= MAXPOINTS;
	    }
	    if (arrayOut == arrayIn) {
		// Erased all the points
		arrayIn = arrayOut = firstPlot1data = firstPlot2data = 0;
		// Delete all the points from the plots
		plotData = plot1plot.getDataset(0);
		if (plotData instanceof XYSeriesCollection) {
		    ((XYSeriesCollection)plotData).getSeries(0).clear();
		} else {
		    ((TimeSeriesCollection)plotData).getSeries(0).clear();
		}
		plotData = plot2plot.getDataset(0);
		if (plotData instanceof XYSeriesCollection) {
		    ((XYSeriesCollection)plotData).getSeries(0).clear();
		} else {
		    ((TimeSeriesCollection)plotData).getSeries(0).clear();
		}
	    } else {
		// Remove points, if any, from the plots
		firstPlot1data = erasePlotData(plot1plot, firstPlot1data, 
					       oldOut, numErase);
		firstPlot2data = erasePlotData(plot2plot, firstPlot2data, 
					       oldOut, numErase);
	    }
	}
    }

    // Erasing <numErase> points from index <out> If any of these are
    // currently in the <plotData>, remove them (The first element of
    // each plotData Series came from index <firstPlotData>).
    // Returns the new firstPlotData value
    private int erasePlotData(XYPlot plotPlot, int firstPlotData, 
			      int out, int numErase) {
	int nextPlotData = firstPlotData;
	if (nextPlotData < out) {
	    nextPlotData += MAXPOINTS;	// Unwrap the index
	}
	int last = out + numErase - 1; // Last point to delete
	if (last < nextPlotData) {
	    return firstPlotData; // No data in this plot is to be deleted
	}

	// Decrement by the number of deleted points not in plot
	numErase -= nextPlotData - out; 
	
	nextPlotData += numErase; // Point after the last one being deleted
	if (nextPlotData >= MAXPOINTS) {
	    nextPlotData -= MAXPOINTS;
	}

	// Delete the required points from the plot
	for (int i=0; i < plotPlot.getDatasetCount(); i++) {
	    XYDataset plotData = plotPlot.getDataset(i);
	    if (plotData == null) {
		continue;
	    }
	    if (plotData instanceof XYSeriesCollection) {
		((XYSeriesCollection)plotData).getSeries(0).delete(
							       0, numErase-1);
	    } else {
		((TimeSeriesCollection)plotData).getSeries(0).delete(
							       0, numErase-1);
	    }
	}
	return nextPlotData;
    }

    // Interpret the showDataName selection numerically:
    // 0 = All
    // >0 = xx*3600 in "Since xx hr"
    // <0 = -xx in "Last xx images"
    private double dataShow(String selection) {
	double ret = 0;
	if (selection.equals("All")) {
	    return ret;
	}
	Scanner s = new Scanner(selection);
	String word = s.next();
	if (word.equals("Since")) {
	    // Get number of hours
	    if (s.hasNextFloat()) {
		ret = 3600.*s.nextFloat();
		return ret;
	    } else {
		return 0;
	    }
	} else if (word.equals("Last")) {
	    if (s.hasNextInt()) {
		ret = -(double)s.nextInt();
		return ret;
	    } else {
		return 0;
	    }
	}
	return 0;
    }

    // Reset the datasets in <plot> so that the plot will contain 
    // the items designated by the showData value specified from the
    // indicated <plotX>, <plotY>, and <plotR> arrays
    // <box> indicates the Combo box type that triggered this call:
    // X: X-axis changed (keep both Y arrays the same as they are)
    // Y: left Y-axis changed (keep the X and right Y arrays the same)
    // R: right Y-axis changed (keep the X and left Y arrays the same)
    // D: the showData value changed (keep all arrays the same)
    private void resetPlot(char box, double showData, 
			     XYPlot plot, String plotX, 
			     String plotY, String plotR) 
					throws CloneNotSupportedException {
	int first = arrayOut;
	int prev;
	int kind, oldKind;
	XYSeriesCollection dataset = null;
	TimeSeriesCollection tDataSet = null;
	MySeries xySeries = null;
	MyTSeries tmSeries = null;

	XYDataset ds = (XYDataset)plot.getDataset(0);
	if (ds instanceof XYSeriesCollection) {
	    dataset = (XYSeriesCollection)ds;
	    oldKind = 0;
	} else {
	    tDataSet = (TimeSeriesCollection)ds;
	    oldKind = 4;
	}
	if (showData < 0) {
	    showData = -showData; // Make positive
	    // Get the current number of items in the arrays
	    int num = arrayIn - arrayOut; 
	    if (num < 0) {
		num += MAXPOINTS;
	    }
	    if (num > showData) {
		// Have more than needed, so only plot the last -showData items
		first = arrayIn - (int)showData;
		if (first < 0) {
		    first += MAXPOINTS;
		}
	    }
	} else if (showData > 0) {
	    // showData is the age in seconds of the oldest item to plot
	    Date now = new Date();
	    long oldest = now.getTime() - (long)showData*1000; // Time of oldest
	    // Look for the first item older than this
	    for (first=arrayIn; first != arrayOut; ) {
		prev = first;
		if (--first < 0) {
		    first += MAXPOINTS;
		}
		if (oldest > timeStamp[first].getTime()) {
		    first = prev;	// This one is too old.  Use previous
		    break;
		}
	    }
	}

	kind = findArrays(plotX, plotY);
	if ((kind < 4) != (oldKind < 4)) {
	    // X axis type has changed
	    if (dataset == null) {
		// Switching from DateAxis to NumberAxis
		xySeries = new MySeries(plotY); 
		dataset = new XYSeriesCollection(xySeries);
		tDataSet = null;
		plot.setDataset(0, dataset);
		plot.setDomainAxis((NumberAxis)xAxis.clone());
		modifySeries(xySeries, first, plotX, plotY, kind);
	    } else {
		// Switching from NumberAxis to DateAxis
		tmSeries = new MyTSeries(plotY);
		tDataSet = new TimeSeriesCollection(tmSeries);
		dataset = null;
		plot.setDataset(0, tDataSet);
     		plot.setDomainAxis((DateAxis) ((DateAxis)dAxis).clone());
		modifySeries(tmSeries, first, plotX, plotY, kind);
	    }
	} else if (box != 'R') {
	    // We need to modify dataset #0 but not the axis type
	    if (kind < 4) {
		xySeries = (MySeries)dataset.getSeries(0);
		modifySeries(xySeries, first, plotX, plotY, kind);
	    } else {
		tmSeries = (MyTSeries)tDataSet.getSeries(0);
		modifySeries(tmSeries, first, plotX, plotY, kind);
	    }
	}

	// Modify dataset #1 if needed
	int n = plot.getDatasetCount();
	if (n != 1 && plot.getDataset(1) == null) {
	    n--;		// The second dataset is null, decrement n
	}
	if (plotR == null) {
	    if (n != 1) {
		// Right Y plot has been removed
		// There's no removeDataset method, so set it to null
		// (Note that this does NOT change the datasetCount)
		plot.setDataset(1, null);
	    }
	} else {
	    // There's a right Y axis.  Modify/create its series
	    kind = findArrays(plotX, plotR);
	    if (n == 1) {
		// Create a right Y series
		if (kind < 4) {
		    dataset = new XYSeriesCollection(
				 (MySeries)createSeries(first, plotX, plotR));
		    plot.setDataset(1, dataset);
		} else {
		    tDataSet = new TimeSeriesCollection(
				 (MyTSeries)createSeries(first, plotX, plotR));
		    plot.setDataset(1, tDataSet);
		}
	    } else {
		// Change the right Y plot
		if (kind < 4) {
		    if (oldKind >= 4) {
			// Switching from DateAxis to NumberAxis
			xySeries = new MySeries(plotR);
			dataset = new XYSeriesCollection(xySeries);
			plot.setDataset(1, dataset);
			plot.setDomainAxis((NumberAxis)xAxis.clone());
			modifySeries(xySeries, first, plotX, plotR, kind);
		    } else {
			// Modifying existing dataset
			dataset = (XYSeriesCollection)plot.getDataset(1);
			modifySeries(dataset.getSeries(0), first, 
				     plotX, plotR, kind);
		    }
		} else {
		    if (oldKind < 4) {
			// Switching from NumberAxis to DateAxis
			tmSeries = new MyTSeries(plotR);
			tDataSet = new TimeSeriesCollection(tmSeries);
			plot.setDataset(1, tDataSet);
			plot.setDomainAxis((DateAxis)((DateAxis)dAxis).clone());
			modifySeries(tmSeries, first, plotX, plotY, kind);
		    } else {
			// Modifying existing dataset
			tDataSet = (TimeSeriesCollection)plot.getDataset(1);
			modifySeries(tDataSet.getSeries(0), first, 
				     plotX, plotR, kind);
		    }
		}
	    }
	}
    }

    private static final int MAXPOINTS = 1000; // Size of status arrays

    private NumberAxis xAxis;	// X axis number template
    private DateAxis dAxis;	// X axis date template
    private double defaultMargin;   // Default Y axis margin (from ChartFactory)

    // Left Plot (#1)
    private JPanel plot1;
    private JFreeChart chart1;
    private int firstPlot1data = 0;	// Index of the oldest item in Plot 1
    private XYPlot plot1plot;
    // Amount of data to show on plot: 
    // 0 = all, < 0 -# of points, > 0 age of oldest (seconds)
    private double plot1show = 0;	

    private String plot1Xlab;	// Axis labels
    private String plot1Ylab;
    private String plot1Rlab;

    // Plot combo boxes
    private plotBox x1box ;
    private plotBox d1box;
    private plotBox y1box;
    private plotBox r1box;
    private gapPanel gap1panel;         	// % gap text box      
    private JCheckBox connectPts1;      	// "do Lines" check box  

    // Right Plot (#2)
    private JPanel plot2;
    private JFreeChart chart2;
    private int firstPlot2data = 0;	// Index of the oldest item in Plot 2
    private XYPlot plot2plot;
    // Amount of data to show on plot: 
    // 0 = all, < 0 -# of points, > 0 age of oldest (seconds)
    private double plot2show = 0;	

    private String plot2Xlab;	// Axis labels
    private String plot2Ylab;
    private String plot2Rlab;

    // Plot combo boxes
    private plotBox x2box ;
    private plotBox d2box;
    private plotBox y2box;
    private plotBox r2box;
    private gapPanel gap2panel;			// % gap text box
    private JCheckBox connectPts2; 		// "do Lines" check box

    private int arrayIn = 0;	// Index to store next status item
    private int arrayOut = 0;	// Index of oldest item in plots

    // Status arrays (Circular buffers running from <arrayOut> to <arrayIn>)
    private float[] seqNo = new float[MAXPOINTS];
    private float[] CCD_Temp = new float[MAXPOINTS];
    private float[] focusPos = new float[MAXPOINTS];
    private float[] sunSensorVolts1 = new float[MAXPOINTS];
    private float[] sunSensorVolts2 = new float[MAXPOINTS];
    private float[] tubeTemp = new float[MAXPOINTS];
    private float[] filtTemp = new float[MAXPOINTS];
    private double[] centPeak = new double[MAXPOINTS];
    private double[] centX = new double[MAXPOINTS];
    private double[] centY = new double[MAXPOINTS];
    private double[] widthX = new double[MAXPOINTS];
    private double[] widthY = new double[MAXPOINTS];
    private double[] RMS = new double[MAXPOINTS];
    private double[] valid = new double[MAXPOINTS];
    private double[] expTime = new double[MAXPOINTS];
    private Date[] timeStamp = new Date[MAXPOINTS];

    // Choices for the X-axis of plots with a chronological X axis
    // (Points are connected by straight lines in such plots)
    private Object[] xArray = { seqNo, timeStamp };
    // Must be in same order as xArray
    private String[] xArrayName = { "seqNo", "timeStamp" };
    // X array axis labels (same order as xArray)
    private String[] xArrayAxLab = { "Image Number", "time of day"}
; 
    // Choices for the Y axis of chronological plots and X or Y axis of others
    private Object[] yArray = { centPeak, centX, centY, widthX, 
				widthY, RMS, expTime, tubeTemp, 
				filtTemp, CCD_Temp, focusPos, 
				sunSensorVolts1, sunSensorVolts2};

    // Must be in same order as yArray
    private String[] yArrayName = { "centPeak", "centX", "centY", "widthX", 
				    "widthY", "RMS", "expTime", "tubeTemp", 
				    "filtTemp", "CCD_Temp", "focusPos", 
				    "sunSensor1", "sunSensor2"};

    // Y array axis labels (same order as yArray)
    private String[] yArrayAxLab = { "Counts", "Position (pix)", 
				     "Position (pix)", "Width (pix)", 
				     "Width (pix)", "Counts", 
				     "Seconds of Time", "Temp (C)", 
				     "Temp (C)", "Temp (C)", "Position (mm)", 
				     "Volts", "Volts"};

    // Combo box items for how much data to show
    private String[] showDataName = {"All", "Since 0.25 hr", "Since 0.5 hr", 
				     "Since 1 hr", "Since 2 hr", "Since 3 hr", 
				     "Since 4 hr", "Since 5 hr", 
				     "Last 10 images", "Last 25 images", 
				     "Last 50 images", "Last 100 images", 
				     "Last 250 images", "Last 500 images"};

    // Names of arrays that require the centroid valid=1 to be meaningful
    private String needsCentroid = "centPeak centX centY widthX widthY RMS";

    // Concatenation of xArray and yArray
    private Object[] arrayPtr = new Object[xArray.length+yArray.length];	 
    // Concatenation of xArrayName and yArrayName
    private String[] arrayName = new String[xArray.length+yArray.length];
    // Concatenation of xArrayAxLab and yArrayAxLab
    private String[] arrayAxLab = new String[xArray.length+yArray.length];
    private boolean[] arrayIsCentroid = new boolean[xArray.length+yArray.length];

    private int xIndex;
    private int yIndex;

    // Default plots on startup
    private String defaultPlot1X = new String("seqNo");
    private String defaultPlot1Y = new String("widthX");
    private String defaultPlot2X = new String("timeStamp");
    private String defaultPlot2Y = new String("centY");

    private float [] fXarray = null;
    private double [] dXarray = null;
    private Date [] tXarray = null;
    private float [] fYarray = null;
    private double [] dYarray = null;
    private static final int FONTSIZE = 11;
    public static final Font labelFont = 
				new Font("SansSerif", Font.PLAIN, FONTSIZE);
    public static final Font plotBoxFont = labelFont.deriveFont(10.0f);

    private optStatus statOPT;

    // Inner Classes:

    // JComboBox with a title and updates the specified axis of the specified plot
    class plotBox extends JPanel implements ActionListener {
	public plotBox(char theAxis, int thePlotNum, String[] listBeg, 
		       String[] listEnd, String title) {
	    axis = theAxis;
	    plotNum = thePlotNum;
	    
	    setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
	    label = new JLabel(title);
	    label.setFont(plotBoxFont);
	    add(label);
	    comboBox = new JComboBox(listBeg);
	    comboBox.setFont(plotBoxFont);
	    if (listEnd != null) {
		// Add listEnd items after those in listBeg
		for (int i=0; i < listEnd.length; i++) {
		    comboBox.addItem(listEnd[i]);
		}
	    }
	    comboBox.setMaximumRowCount(10); // (Uses scrollbar if >)
	    comboBox.addActionListener(this);
	    add(comboBox);
	}
	// 'X', 'Y', 'R" (right Y axis), or 'D' (for amount of data on plot)
	private char axis;
	private int plotNum;
	private JLabel label;
	private JComboBox comboBox;
	
	public void actionPerformed(ActionEvent e) {
	    Object source = e.getSource();
	    if (source instanceof JComboBox) {
		doComboBoxAction((JComboBox)source);
	    }
	}

	// One of the plotBox's JComboBox's triggered an ActionEvent
	private void doComboBoxAction(JComboBox cb) {
	    String newSelection = (String)cb.getSelectedItem();
	    String plotX;
	    String plotR;
	    String plotXlab, plotYlab, plotRlab;
	    double show;
	    XYSeriesCollection dataset = null;
	    XYSeriesCollection dataset2 = null;
	    TimeSeriesCollection tDataSet = null, tDataSet2 = null;
	    XYPlot plot;
	    int xIndex, yIndex, rIndex, kind;
	    String plotY;
	    XYDataset plotData;
	    gapPanel gPanel;

	    if (plotNum == 1) {
		show = plot1show;
		plot = plot1plot;
		gPanel = gap1panel;
		plotData = plot.getDataset(0);
		if (plotData instanceof XYSeriesCollection) {
		    kind = 0;
		    dataset = (XYSeriesCollection) plotData;
		    xIndex = ((MySeries)dataset.getSeries(0)).getXindex();
		    plotY = (String)dataset.getSeriesKey(0); // Left Y array name
		} else {
		    kind = 4;
		    tDataSet = (TimeSeriesCollection)plotData;
		    xIndex = ((MyTSeries)tDataSet.getSeries(0)).getXindex();
		    plotY = (String)tDataSet.getSeriesKey(0); // Left Y array name
		}
	    } else {
		show = plot2show;
		plot = plot2plot;
		gPanel = gap2panel;
		plotData = plot.getDataset(0);
		if (plotData instanceof XYSeriesCollection) {
		    kind = 0;
		    dataset = (XYSeriesCollection) plotData;
		    xIndex = ((MySeries)dataset.getSeries(0)).getXindex();
		    plotY = (String)dataset.getSeriesKey(0); // Left Y array name
		} else {
		    kind = 4;
		    tDataSet = (TimeSeriesCollection)plotData;
		    xIndex = ((MyTSeries)tDataSet.getSeries(0)).getXindex();
		    plotY = (String)tDataSet.getSeriesKey(0); // Left Y array name
		}
		show = plot2show;
		plot = plot2plot;
	    }

	    // Get right Y axis name, if any
	    if (plot.getDataset(1) == null) {
		plotR = null;	// No right Y axis plot
	    } else {
		if (kind == 0) {
		    dataset2 = (XYSeriesCollection)plot.getDataset(1);
		    // Right Y array name
		    plotR = (String)dataset2.getSeriesKey(0); 
		} else {
		    tDataSet2 = (TimeSeriesCollection)plot.getDataset(1);
		    // Right Y array name
		    plotR = (String)tDataSet2.getSeriesKey(0); 
		}
	    }
	    plotX = arrayName[xIndex]; // Current X axis array
	    switch(axis) {
	    case 'X':
		plotX = newSelection; // New X axis array
		break;
	    case 'Y':
		plotY = newSelection; // New Y axis array
		break;
	    case 'R':
		if (newSelection.equals("None")) {
		    plotR = null;
		} else {
		    plotR = newSelection; // New right Y axis array
		}
		break;
	    case 'D':
		// Make the show selection numeric
		show = dataShow(newSelection);
		if (plotNum == 1) {
		    plot1show = show;
		} else {
		    plot2show = show;
		}
		break;
	    default:
		return;
	    }

	    // Change plot to show the specified points of the specfied arrays
	    try {
		resetPlot(axis, show, plot, plotX, plotY, plotR);
	    }
	    catch (CloneNotSupportedException e2) {
		System.out.println("Could not clone X axis template: " + 
				   			   e2.getMessage());
		System.exit(1);
	    }

	    // Update the axis labels and switch to the appropriate axis type
	    XYDataset ds = plot.getDataset(0);
	    if (ds instanceof XYSeriesCollection) {
		dataset = (XYSeriesCollection) ds;
		xIndex = ((MySeries)dataset.getSeries(0)).getXindex();
		yIndex = ((MySeries)dataset.getSeries(0)).getYindex();
		NumberAxis ax = (NumberAxis) plot.getDomainAxis();
		ax.setLabel(arrayAxLab[xIndex]);
	    } else {
		tDataSet = (TimeSeriesCollection) ds;
		xIndex = ((MyTSeries)tDataSet.getSeries(0)).getXindex();
		yIndex = ((MyTSeries)tDataSet.getSeries(0)).getYindex();
		DateAxis ax = (DateAxis) plot.getDomainAxis();
		ax.setLabel(arrayAxLab[xIndex]);
	    }

	    // Connect the points with lines if the X array is chronological
	    XYLineAndShapeRenderer renderer = 
				(XYLineAndShapeRenderer)plot.getRenderer(0);
	    renderer.setSeriesLinesVisible(0, xIndex < xArray.length);
	    renderer = (XYLineAndShapeRenderer)plot.getRenderer(1);
	    renderer.setSeriesLinesVisible(0, xIndex < xArray.length);
	    if (plotNum == 1) {
		connectPts1.setSelected(xIndex < xArray.length);
	    } else {
		connectPts2.setSelected(xIndex < xArray.length);
	    }
	    NumberAxis yAxis = (NumberAxis)plot.getRangeAxis();
	    yAxis.setLabel(arrayAxLab[yIndex]);

	    NumberAxis rAxis = (NumberAxis)plot.getRangeAxis(1);
	    if (plotR != null) {
		if (ds instanceof XYSeriesCollection) {
		    dataset2 = (XYSeriesCollection)plot.getDataset(1);
		    rIndex = ((MySeries)dataset2.getSeries(0)).getYindex();
		} else {
		    tDataSet2 = (TimeSeriesCollection) plot.getDataset(1);
		    rIndex = ((MyTSeries)tDataSet2.getSeries(0)).getYindex();
		}
		rAxis.setLabel(arrayAxLab[rIndex]);

		// Make the right Y axis and gap text box visible
		rAxis.setVisible(true);
		if (gPanel != null) {
		    gPanel.setVisible(true);
		    gPanel.resetMargins();
		}
	    } else {
		// Make the right Y axis and gap text box invisible
		rAxis.setVisible(false);
		if (gPanel != null) {
		    gPanel.setVisible(false);
		}
		yAxis.setLowerMargin(defaultMargin); // Restore normal margins
		yAxis.setUpperMargin(defaultMargin);
	    }
	    return;
	}
    }

    // Make a labeled JFormattedTextField for the vertical gap between
    // the plots when there are two right axes
    // rBox is the plotBox for the right axis
    class gapPanel extends JPanel implements ActionListener {
	public gapPanel(plotBox theBox) {
	    rBox = theBox;

	    // Get 150% of Font size in screen dots 
	    // (Rule.INCH is screen dots per inch)
	    int fontSize = (int)Math.round(1.5*plotBoxFont.getSize2D()/72.*Rule.INCH);
	    Dimension textBoxSize = new Dimension((int)2.6*fontSize, fontSize);
	    
	    // Set up the JFormattedTextField
	    DecimalFormat dFormat = new DecimalFormat();
	    gapText = new JFormattedTextField(dFormat);
	    dFormat.setMinimumFractionDigits(0);
	    gapText.setFont(plotBoxFont);
	    gapText.setSize(textBoxSize);
	    gapText.setValue(new Double(defaultMargin*100.));
	    gapText.addActionListener(this);
	    
	    // Set up the label
	    JLabel gapLabel = new JLabel("% gap");
	    gapLabel.setFont(plotBoxFont);
	    
	    // Put them one above the other in the panel
	    setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
	    add(gapLabel);
	    add(gapText);
	    String rSelection = (String)rBox.comboBox.getSelectedItem();
	    if (rSelection.equals("None")) {
		setVisible(false); // No right Y axis
	    } else {
		setVisible(true); // There's already a right Y axis
	    }
	    resetMargins();
	}

	public void resetMargins() {
	    XYPlot plot;
	    NumberAxis yAxis;	// Left Y axis
	    NumberAxis rAxis;	// Right Y axis
	    double percent = ((Number)gapText.getValue()).doubleValue();

	    // Select the plot
	    if (rBox.plotNum == 1) {
		plot = plot1plot;
	    } else {
		plot = plot2plot;
	    }

	    // Get the two Y axes
	    yAxis = (NumberAxis)plot.getRangeAxis();
	    rAxis = (NumberAxis)plot.getRangeAxis(1);
	    if (percent < 0) {
		percent = -percent/100.; 	// Make it a positive fraction

		// Move the Left Axis plot up and the Right Axis plot down
		rAxis.setLowerMargin(defaultMargin);
		rAxis.setUpperMargin(percent);
		yAxis.setLowerMargin(percent);
		yAxis.setUpperMargin(defaultMargin);
	    } else {
		percent = percent/100.;		// Make it a fraction

		// Move the Left Axis plot down and the Right Axis plot up
		yAxis.setLowerMargin(defaultMargin);
		yAxis.setUpperMargin(percent);
		rAxis.setLowerMargin(percent);
		rAxis.setUpperMargin(defaultMargin);
	    }
	}

	public void actionPerformed(ActionEvent e) {
	    resetMargins();
	}

	private plotBox rBox;
	private JFormattedTextField gapText;
    }
}



// XYSeries with fields (and their corresponding methods) recording the
// indices of arrayPtr[] corresponding to the X and Y values, their kind
// (float or double), and whether it needs the centroid Valid=1 for an item to
// be plotted
class MySeries extends XYSeries {
    public MySeries(Comparable key) {
	super(key);
    }
    public void setKind(int newKind) {
	kind = newKind;
    }
    public int getKind() {
	return kind;
    }
    public void setIndices(int newXindex, int newYindex) {
	xIndex = newXindex;
	yIndex = newYindex;
    }
    public int getXindex() {
	return xIndex;
    }
    public int getYindex() {
	return yIndex;
    }
    public void setXCentroid(boolean isCentroid) {
	xNeedsValid = isCentroid;
    }
    public void setYCentroid(boolean isCentroid) {
	yNeedsValid = isCentroid;
    }
    public boolean xIsCentroid() {
	return xNeedsValid;	// True if Valid must be 1 to plot the item
    }
    public boolean yIsCentroid() {
	return yNeedsValid;	// True if Valid must be 1 to plot the item
    }

    // Kind indicates the type of the X and Y arrays:
    // 0: both X & Y are float
    // 1: X is double, Y is float
    // 2: X is float, Y is double
    // 3: both X and Y are double
    private int kind;
    private int xIndex;
    private int yIndex;
    private boolean xNeedsValid;
    private boolean yNeedsValid;
}

// timeSeries with fields (and their corresponding methods) recording the
// indices of arrayPtr[] corresponding to the X and Y values, their kind
// (float or double), and whether it needs the centroid Valid=1 for an item to
// be plotted
class MyTSeries extends TimeSeries {
    public MyTSeries(Comparable key) {
	// SRankin
	// super(key);
	super("Temporary workaround for broken build.");
    }
    public void setKind(int newKind) {
	kind = newKind;
    }
    public int getKind() {
	return kind;
    }
    public void setIndices(int newXindex, int newYindex) {
	xIndex = newXindex;
	yIndex = newYindex;
    }
    public int getXindex() {
	return xIndex;
    }
    public int getYindex() {
	return yIndex;
    }
    public void setXCentroid(boolean isCentroid) {
	xNeedsValid = isCentroid;
    }
    public void setYCentroid(boolean isCentroid) {
	yNeedsValid = isCentroid;
    }
    public boolean xIsCentroid() {
	return xNeedsValid;	// True if Valid must be 1 to plot the item
    }
    public boolean yIsCentroid() {
	return yNeedsValid;	// True if Valid must be 1 to plot the item
    }

    // Kind indicates the type of the X and Y arrays:
    // 4: X is Date, Y is float
    // 6: X is Date, Y is double
    private int kind;
    private int xIndex;
    private int yIndex ;
    private boolean xNeedsValid;
    private boolean yNeedsValid;
}
