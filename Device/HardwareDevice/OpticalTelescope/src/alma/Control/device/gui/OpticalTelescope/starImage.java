// TODO: SRankin - resolve compile problems in commented out code.

package alma.Control.device.gui.OpticalTelescope;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.geom.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.imageio.*;
import javax.imageio.stream.*;
import java.io.*;
import java.text.DecimalFormat;
import java.text.Format;
import java.util.Iterator;
import java.util.Arrays;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
// SRankin
// import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;


public class starImage extends JPanel
                        implements ActionListener {
    // Set up the panel containing the star image and related buttons
    public starImage() {
	int viewportHeight = 250; // Initial size of scrolling viewport
	int viewportWidth = 200;
	int lowX = 1;		// First pixel number in X
	int lowY = 1;       	// First pixel number in Y
	int highX;		// Last pixel number in X
	int highY;		// Last pixel number in Y

	// Set up a scrolling image of an image 1024 x 1024 pixels
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
	bufStar = new BufferedImage(1024, 1024, BufferedImage.TYPE_BYTE_GRAY);
	graStar = bufStar.createGraphics(); // Graphics2D for drawing on

	// Put the pixel values of the unzoomed image in a 2-dimensional array
	rasterStar = bufStar.getRaster();
	nCol0 = rasterStar.getWidth();
	nRow0 = rasterStar.getHeight();
	pixVal0 = rasterStar.getPixels(0, 0, nCol0, nRow0, pixVal0);
	zPix = pixVal0;

	ImageIcon star = new ImageIcon(bufStar);
	curZoom = 2.;		// Initial Zoom factor

	highX = lowX + nCol0 - 1;
	highY = lowY + nRow0 - 1;

	// Save values for Reset button
	lowX0 = lowX;
	highX0 = highX;
	lowY0 = lowY;
	highY0 = highY;
	Zoom0 = curZoom;

	// Save current lowX and lowY for zoomTo()
	curLowX = lowX;
	curHighY = highY;

        //Create the row and column headers.
        columnView = new Rule(Rule.HORIZONTAL, lowX, highX, curZoom);
        rowView = new Rule(Rule.VERTICAL, lowY, highY, curZoom);
	columnView.setPreferredWidth((int)((highX - lowX + 1)*curZoom));
	rowView.setPreferredHeight((int)((highY - lowY + 1)*curZoom));

        //Set up the scroll pane.
        picture = new ScrollablePicture(star, columnView.getIncrement());
        pictureScrollPane = new JScrollPane(picture);
        pictureScrollPane.setPreferredSize(new Dimension(viewportWidth, 
							 viewportHeight));
        pictureScrollPane.setColumnHeaderView(columnView);
        pictureScrollPane.setRowHeaderView(rowView);
 
        //Put it in this panel.
        add(pictureScrollPane);

	// Set up a Zoom button panel
        JPanel zButtonPanel = new JPanel();
	layout = new BoxLayout(zButtonPanel, BoxLayout.X_AXIS);
	zButtonPanel.setLayout(layout);

	// Make a Zoom In button
	zoomIn = new JButton("Zoom In");
	zoomIn.setFont(new Font("SansSerif", Font.PLAIN, 11));
	zoomIn.setMargin(new Insets(2,2,2,2));
	zoomIn.addActionListener(this);
	zButtonPanel.add(zoomIn);

	// Make a Zoom Out button
	zoomOut = new JButton("Zoom Out");
	zoomOut.setFont(new Font("SansSerif", Font.PLAIN, 11));
	zoomOut.setMargin(new Insets(2,2,2,2));
	zoomOut.addActionListener(this);
	zButtonPanel.add(zoomOut);

	// Add a Zoom Factor text box
	dFormat.setMinimumFractionDigits(4);
	Font textFont = new Font("SansSerif", Font.PLAIN, 11);
	zoomText.setFont(textFont);

	// Get 150% of Font size in screen dots 
	// (Rule.INCH is screen dots per inch)
	int fontSize = (int)Math.round(1.5*textFont.getSize2D()/72.*Rule.INCH);
	Dimension textBoxSize = new Dimension((int)2.8*fontSize, fontSize);

	zoomText.setMaximumSize(textBoxSize);
	zoomText.setValue(new Double(curZoom));
        zoomText.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
		double newZoom;
		double nextZoom = ((Number)zoomText.getValue()).doubleValue();
		do {
		    newZoom = nextZoom;
		    zoomText.setValue(newZoom);
		    // Pick another zoom factor in case this one fails
		    nextZoom = (int)nextZoom - 1; 
		} while (zoomTo(newZoom) != 0); 
		repaint();
            }            
        });
        zButtonPanel.add(zoomText);

	// Make a Reset button
	JButton reset = new JButton("Reset");
	reset.setFont(new Font("SansSerif", Font.PLAIN, 11));
	reset.setMargin(new Insets(2,2,2,2));
	reset.addActionListener(this);
	zButtonPanel.add(reset);
	zButtonPanel.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));

	// Set up a Gain & Offset button panel
        JPanel gButtonPanel = new JPanel();
	layout = new BoxLayout(gButtonPanel, BoxLayout.X_AXIS);
	gButtonPanel.setLayout(layout);

	// Make an Auto Gain check box
        autoGain = new JCheckBox("Auto Gain");
	autoGain.setFont(new Font("SansSerif", Font.PLAIN, 11));
        autoGain.addActionListener(this);
	autoGain.setSelected(true); // Default is Auto
        gButtonPanel.add(autoGain);

	// Make a Gain text box
	dFormat = new DecimalFormat();
        gainText = new JFormattedTextField(dFormat);
	dFormat.setMinimumFractionDigits(4);
	gainText.setFont(textFont);
	gainText.setMaximumSize(textBoxSize);
	gainText.setValue(new Double(1.0));
        gainText.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
		Gain = ((Number)gainText.getValue()).doubleValue();
		autoGain.setSelected(false); // Turn off Auto
		applyGainOffset(); // Apply the new Gain value
            }            
        });
        gButtonPanel.add(gainText);
        Gain = Double.valueOf(gainText.getText());

	// Make an Auto Offset check box
        autoOffset = new JCheckBox("Auto Offset");
	autoOffset.setFont(new Font("SansSerif", Font.PLAIN, 11));
        autoOffset.addActionListener(this);
	autoOffset.setSelected(true); // Default is Auto
        gButtonPanel.add(autoOffset);

	dFormat = new DecimalFormat();
	dFormat.setMinimumFractionDigits(4);
        offsetText = new JFormattedTextField(dFormat);
	offsetText.setFont(textFont);
	offsetText.setMaximumSize(textBoxSize);
	offsetText.setValue(new Double(0.1));
	Offset = ((Double)offsetText.getValue()).doubleValue();
        offsetText.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Offset = ((Number)offsetText.getValue()).doubleValue();
		autoOffset.setSelected(false); // Turn off Auto
		applyGainOffset(); // Apply the new Offset value
            }            
        });        
        gButtonPanel.add(offsetText);
	gButtonPanel.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));

	// Set up a panel to hold both button panels
        buttonPanel = new JPanel();
	layout = new BoxLayout(buttonPanel, BoxLayout.Y_AXIS);
	buttonPanel.setLayout(layout);
	buttonPanel.add(gButtonPanel);
	buttonPanel.add(zButtonPanel);
	Dimension minSize = layout.minimumLayoutSize(buttonPanel);

	// If you don't increase the width by 1.7, not all the panels fit
	buttonPanel.setMaximumSize(new Dimension((int)(1.7*minSize.width), 
						 (int)(minSize.height)));
	add(buttonPanel);

        setBorder(BorderFactory.createEmptyBorder(0,20,20,20));
    }

    // Button actions
    public void actionPerformed(ActionEvent e) {
	double newZoom, rNewZoom;
        if (e.getActionCommand().equals("Zoom In")) {
	    double nextZoom = curZoom*sqr2;
	    do {
		newZoom = nextZoom;
		rNewZoom = Math.rint(newZoom); // Nearest whole number
		if (Math.abs(newZoom-rNewZoom) < 0.1) {
		    newZoom = rNewZoom; // Make it a whole number
		}
		zoomText.setValue(newZoom);
		// Pick another zoom factor in case this one fails
		nextZoom = (int)nextZoom - 1; 
	    } while (zoomTo(newZoom) != 0);
        } else if (e.getActionCommand().equals("Zoom Out")) {
	    newZoom = curZoom/sqr2;
	    rNewZoom = Math.rint(newZoom); // Nearest whole number
	    if (Math.abs(newZoom-rNewZoom) < 0.1) {
		newZoom = rNewZoom; // Make it a whole number
	    }
	    zoomText.setValue(newZoom);
	    zoomTo(newZoom);
        } else if (e.getActionCommand().equals("Reset")) {
	    newZoom = Zoom0;
	    zoomText.setValue(newZoom);
	    zoomTo(newZoom);
        } else if (e.getActionCommand().equals("Auto Gain")) { 
	    if (autoGain.isSelected()) {
		applyGainOffset(); // Calculate a new gain
	    }
	} else if (e.getActionCommand().equals("Auto Offset")) {
	    if (autoOffset.isSelected()) {
		applyGainOffset(); // Calculate a new offset
	    }
	} 
    }

    // Switch to a new image made of the specified <pixels>
    // with a size <width> x <height> with the lower left pixel numbered
    // (lowX, lowY).  (Note that the pixel numbering is 1-based and the first
    // pixel is displayed at the lower left corner NOT the top left corner.)

    // Center the scrollable window at the pixel (cenX, cenY) with
    // the specified <Zoom> factor (Unzoomed: Zoom = 1, Zoom in by 2:
    // Zoom = 2.0, Zoom out by 2: Zoom = 0.5)
    // To keep the current Zoom factor, use Zoom = 0

    // <cutX> and <cutY> are horizontal and vertical cuts through the 
    // centroid peak, <pixMin> and <pixMax> are the minimum and maximum
    // values in <pixels>, <centBox> is the size of the centroid box,
    // <widX> & <widY> are the centroid widths in X and Y, and <peak> its height
    public void newStar(int[] pixels, int width, int height, double lowX, 
			double lowY, double Zoom, double cenX, double cenY,
			int[] cutX, int[] cutY,	int pixMin, int pixMax,
			int centBox, double widX, double widY, 
			double peak, int centValid) {

	// Store the new size and lower left pixel number
	nCol0 = width;
	nRow0 = height;
	lowX0 = lowX;
	lowY0 = lowY;

	centX = cenX;		// Save the centroid coordinates
	centY = cenY;

	starRow = cutX;		// and the cuts through the centroid peak
	starCol = cutY;
	pixelMin = pixMin;
	pixelMax = pixMax;
	sizCent = centBox;
	widthX = widX;
	widthY = widY;
	valid = centValid;

	if (valid != 0) {
	// Convert centroid integrated intensity (PEAK) into intensity at
	// the peak assuming a 2-D gaussian with a FWHM of <widX> & <widY>
	    centPeak = 2.*peakFac*peak/(widX*widY); // x 2 due to BSCALE?
	    vCentX = centX;	// Remember valid centroids
	    vCentY = centY;
	} else {
	    centPeak = 0;
	}

	// Save the pixels in a 1-D array reversing the order of the rows
	pixVal0 = new int[nRow0*nCol0];
	int n = 0;
	int nn;
        for (int i=nRow0 - 1; i >= 0; i--) {
	    nn = i*nCol0;
            for (int j=0; j < nCol0; j++) {
		pixVal0[nn++] = pixels[n++];
            }
        }

        highX0 = lowX + nCol0 - 1; // Set the top right pixel number
        highY0 = lowY + nRow0 - 1;


	// Row and column of the upper left centroid box corner on the screen
	// First FITS coordinates
	int hCut = sizCent/2;
	boxX = (int)Math.round(centX) - hCut;;
	boxY = (int)Math.round(centY) + hCut;

	// Now in screen coordinates
	boxX -= (int)Math.round(lowX0);
	boxY = (int)Math.round(highY0) - boxY;

	// Average the perimeter pixels
	int n1 = boxY*nCol0 + boxX;		// First pixel on top row of box
	int n2 = n1 + sizCent - 1;		// Last pixel on the top row
	int n3 = n1 + (sizCent - 1)*nCol0;	// First pixel on bottom row
	int n4 = n3 + sizCent - 1;		// Last pixel on the bottom row
	
	centSky = 0;
	if (n1 >= 0 && n4 < nCol0*nRow0) {
	    // Entire box perimeter is on the image
	    // Sum top & bottom rows and left & right columns of centroid box
	    for (int i=0; i < sizCent-1; i++) {
		centSky += pixVal0[n1+i] + pixVal0[n2+i*nCol0] + 
		    pixVal0[n3-i*nCol0] +  pixVal0[n4-i];
	    }
	    
	    centSky /= 4*(sizCent - 1); 	// Make it an average
	}


	// Get the Jfreecharts for the horizontal and vertical cuts
	chartX = createChart(createDataset(1), 
				 PlotOrientation.VERTICAL);
	chartY = createChart(createDataset(2), 
						 PlotOrientation.HORIZONTAL);

	if (Zoom <= 0) {
	    Zoom = curZoom;	// Don't change the Zoom factor
	}

	// Zoom the image to the initial zoom factor centered on the specified
	// pixel
	Rectangle viewNow = pictureScrollPane.getViewport().getViewRect();

	while (zoomTo(curZoom, centX, centY, viewNow) != 0) {
	    curZoom -= 1;	// Zoom is too big. Try again
	    zoomText.setValue(curZoom);
	}
	Zoom0 = curZoom;	   // Remember this zoom for the Reset button

    }
	

    // Zoom factor or image has changed.  
    //Must scale the pixels and pixel coordinates

    // Conversion from screen dots to pixel units
    // Horizontal: Pixel = lowX + dots/Zoom
    // Vertical: Pixel = highY - dots/Zoom

    public int zoomTo(double newZoom) {

	// Calculate the current center of the view window in pixel coordinates
	Rectangle viewNow = pictureScrollPane.getViewport().getViewRect();
	double cenX = curLowX + ((viewNow.x+viewNow.width/2.)/curZoom);
	double cenY = curHighY - ((viewNow.y+viewNow.height/2.)/curZoom);
	cenX -= sizCent/2.;	// Adjust for the cut plot centering
	cenY -= sizCent/2.;

	return zoomTo(newZoom, cenX, cenY, viewNow); // Zoom to that center
    }

    // Change the view to the specified zoom factor centered on pixel
    // (cenX, cenY) in image pixel number coordinates
    // <viewNow> is the current viewport rectangle
    // Returns non-zero if there's not enough memory to zoom to that value
    public int zoomTo(double newZoom, double cenX, double cenY, 
		       Rectangle viewNow) {

	int n = 0;
	int nn = 0;
	double lowX = 0;	// New X value for first zoomed pixel
	double highX = 0;	// New X value for last zoomed pixel 
	double lowY = 0;	// New Y value for first zoomed pixel
	double highY = 0;	// New Y value for last zoomed pixel 
	double delPix = 0;	// Pixel displacment from original positio
	int oldSize = nZCol*nZRow; // Size of previous zoomed image

	// Calculate a new image based on the newZoom value
	if (newZoom == 1.) {
	    zPix = pixVal0;	// Set to unZoomed image
	    nZCol = nCol0;
	    nZRow = nRow0;
	    if (oldSize !=  nZCol*nZRow) {
		bufZoom = new BufferedImage(nZCol, nZRow, 
						BufferedImage.TYPE_BYTE_GRAY);
	    }
	} else if (newZoom > 1.) {
	    // Zoom in: change the pixel size to newZoom
	    // Duplicate pixels in their new places
	    nZCol = (int)Math.round(nCol0*newZoom);
	    nZRow = (int)Math.round(nRow0*newZoom);
	    if (oldSize !=  nZCol*nZRow) {
		// Allocate a new zoomed pixel array
		try {
		    zPix = new int[nZCol*nZRow]; // Un-normalized image
		    normPix = new int[nZCol*nZRow];	// Normalized image
		    bufZoom = new BufferedImage(nZCol, nZRow, 
						BufferedImage.TYPE_BYTE_GRAY);
		}
		catch (OutOfMemoryError e) {
		    return 1;
		}
	    }
	    for (int i=0; i < nZRow; i++) {
		// First unzoomed pixel in this row
		nn = ((int)(i/newZoom))*nCol0; 
		for (int j=0; j < nZCol; j++) {
		    zPix[n++] = pixVal0[nn + (int)(j/newZoom)];
		}
	    }
	    // Get the change in the numbering of the first (sub)pixel
	    delPix = -(newZoom - 1.)/(2.*newZoom);
	} else if (newZoom < 1.) {
	    // Zoom out
	    // The new pixels are averages nSum x nSum original pixels
	    int nSum = (int)Math.round(1./newZoom);
	    if (nSum == 1 && curZoom > newZoom) {
		nSum = 2;	// Zoom out one more
	    }
	    nZCol = (int)nCol0/nSum;
	    nZRow = (int)nRow0/nSum;
	    if (oldSize !=  nZCol*nZRow) {
		zPix = new int[nZCol*nZRow]; // Un-normalized image
		normPix = new int[nZCol*nZRow];	// Normalized image
		bufZoom = new BufferedImage(nZCol, nZRow, 
						BufferedImage.TYPE_BYTE_GRAY);
	    }
	    for (int i=0, i0=0; i < nZRow; i++, i0 += nSum) {
		for (int j=0, j0=0; j < nZCol; j++, j0 += nSum) {
		    // Sum nSum x nSum unzoomed pixels for this zoomed pixel
		    zPix[n] = 0; // zPix pixel (i,j)
		    for (int k=0; k < nSum; k++) {
			for (int kk=0; kk < nSum; kk++) {
			    // Get pixVal0 index for pixel (i0+k,j0+kk)
			    nn = (i0 + k)*nCol0 + j0 + kk; 
			    zPix[n] += pixVal0[nn];
			}
		    }
		    zPix[n++] /= (nSum*nSum); // Convert zPix(i,j) to an average
		}
	    }
	    delPix = (nSum - 1.)/2.; // Pixel number change on plot
	    newZoom = 1./(double)nSum;	// Calculate the actual Zoom factor
	    zoomText.setValue(newZoom);	// Also put it in the text box
	}

	rescalePix(zPix);	// Normalize for display, result in normPix

	lowX = lowX0 + delPix;	 // X number at the lowest dot on the screen 
	highX = highX0 - delPix; // X number at the highest dot on the screen
	lowY = lowY0 + delPix;   // Y number at the lowest dot on the screen 
	highY = highY0 - delPix; // Y number at the highest dot on the screen

	curZoom = newZoom;	// Remember the current Zoom factor
	curLowX = lowX;		// and low X and high Y
	curHighY = highY;

	// Set up for the new image
	columnView.setPreferredWidth((int)((highX - lowX + 1)*newZoom));
	rowView.setPreferredHeight((int)((highY - lowY + 1)*newZoom));

	rasterZoom = bufZoom.getRaster();
	rasterZoom.setPixels(0, 0, nZCol, nZRow, normPix);
	graStar = bufZoom.createGraphics(); // Graphics2D for drawing on	
        picture = new ScrollablePicture(new ImageIcon(bufZoom), 
					columnView.getIncrement());
	pictureScrollPane.getViewport().setView(picture);

	columnView.setZoomFactor(newZoom);
	columnView.setLowHigh(lowX, highX);
	rowView.setZoomFactor(newZoom);
	rowView.setLowHigh(lowY, highY);

	// Set the scroll to center the view at the previous center
	// Offset half the box size in X for the vertical cut plot
	// and a similar amount in Y for the horizontal cut plot
	viewNow.x = (int)(Math.round((cenX - lowX + sizCent/2.)*newZoom) - 
			  			viewNow.width/2.);
	// Adjust if view is too small for plot
	viewNow.x = Math.min((int)(nZCol - newZoom*sizCent), viewNow.x);
	viewNow.x = Math.max(0,viewNow.x);
	viewNow.y = (int)Math.round((highY - cenY - sizCent/2.)*newZoom - 
				    		viewNow.height/2.);
	viewNow.y = Math.max(0,viewNow.y);
	picture.scrollRectToVisible(viewNow);
	doStarPlot();
	repaint();
	return 0;
    }

    // Rescale the input pixels by Gain and Offset and store in array normPix
    public void rescalePix(int[] pixel) {
	double offset;
	
	if (autoGain.isSelected()) {
	    if (pixelMax == pixelMin) {
		// No image exists
		for (int i=0; i < pixel.length; i++) {
		    normPix[i] = 0;
		}
		return;
	    }
	    Gain = 256./(pixelMax-pixelMin); // Compute gain from range
	    gainText.setValue(Gain);
	} 
	if (autoOffset.isSelected()) {
	    offset = pixelMin;       // Offset is pixel minimum
	    Offset = ((double)pixelMin)/32758.;
	    offsetText.setValue(Offset);
	} else {
	    offset = 32758.*Offset;    // Use user offset
	}

	// Normalize
	for (int i=0; i < pixel.length; i++) {
	    normPix[i] = (int) Math.round(Gain*(pixel[i]-offset));
	    normPix[i] = (int)normPix[i] < 0 ? 0 : normPix[i];
	    normPix[i] = (int)normPix[i] > 255 ? 255 : normPix[i];
	}
	return;
    }

    public void applyGainOffset() {

	// Get the current scroll view
	Rectangle viewNow = pictureScrollPane.getViewport().getViewRect();

	// Apply the new Gain/Offset to the zoomed pixels
	rescalePix(zPix);

	// Insert them in the BufferedImage
	rasterZoom.setPixels(0, 0, nZCol, nZRow, normPix);

	// Make a new picture
        picture = new ScrollablePicture(new ImageIcon(bufZoom),
                                        columnView.getIncrement());
        pictureScrollPane.getViewport().setView(picture);

	// Reset to the previous scroll view
	picture.scrollRectToVisible(viewNow);
	doStarPlot();
	repaint();
    }

    // Create a jfreechart data set for the X or Y centroid cuts
    // X: which=1 and Y: which=2
    private XYSeriesCollection createDataset(int which) {
	XYSeries cutSeries;
	XYSeries markSeries;

	int hCut = sizCent/2;
	int firstCut;	      // Column or row of the first cutX or cutY element

	if (which == 1) {
	    // Cut in X direction
	    cutSeries  = new XYSeries("Cut X");
	    firstCut = (int)Math.round(centX) - hCut;
	    for (int i=0; i < sizCent; i++) {
		cutSeries.add(firstCut+i, starRow[i]);
	    }
	    // Make series to draw a vertical line at centX, centPeak high
	    // and a horizontal line through its midpoint the length of widthX
	    markSeries = new XYSeries("Mark X");
	    markSeries.add(centX-widthX/2., centSky+centPeak/2);
	    markSeries.add(centX, centSky+centPeak/2);
	    markSeries.add(centX, centSky);
	    markSeries.add(centX, centSky+centPeak);
	    markSeries.add(centX, centSky+centPeak/2);
	    markSeries.add(centX+widthX/2., centSky+centPeak/2);
	} else {
	    // Cut in Y direction
	    cutSeries  = new XYSeries("Cut Y");
	    firstCut = (int)Math.round(centY) - hCut;
	    for (int i=0; i < sizCent; i++) {
		cutSeries.add(firstCut+i, starCol[i]);
	    }
	    // Make series to draw a vertical line at centY, centPeak high
	    // and a horizontal line through its midpoint the length of widthY
	    markSeries = new XYSeries("Mark Y");
	    markSeries.add(centY-widthY/2., centSky+centPeak/2);
	    markSeries.add(centY, centSky+centPeak/2);
	    markSeries.add(centY, centSky);
	    markSeries.add(centY, centSky+centPeak);
	    markSeries.add(centY, centSky+centPeak/2);
	    markSeries.add(centY+widthY/2., centSky+centPeak/2);
	}
	XYSeriesCollection dataSet = new XYSeriesCollection(markSeries);
	dataSet.addSeries(cutSeries);
	return dataSet;
    }


    // Create a JFreeChart object of the specified dataset & orientation
    private JFreeChart createChart(XYSeriesCollection dataset, 
					  PlotOrientation ori) {
	JFreeChart chart = ChartFactory.createXYLineChart(
		null,   			 // chart title
		null,                            // domain axis label
		null,                            // range axis label
	        dataset, 			 // data
		ori,        			 // orientation
		false,                           // include legend
		true,                            // tooltips
		false                            // urls
	);
	XYPlot plot = (XYPlot)chart.getPlot();
	XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();

	// Turn on unfilled small circles for points
	renderer.setSeriesShapesVisible(0, true);
	renderer.setBaseShapesFilled(false);
	renderer.setUseFillPaint(false);
	renderer.setSeriesOutlineStroke(0, new BasicStroke(1.0f));
	renderer.setSeriesStroke(0, new BasicStroke(1.0f));
	double circSize = 2.;
	double circPos = -circSize/2;
	renderer.setSeriesShapesVisible(0, false);
	renderer.setSeriesShape(1, new Ellipse2D.Double(circPos, circPos, 
							circSize, circSize));
	renderer.setSeriesShapesVisible(1, true);
	plot.setRenderer(renderer);

	// Set up the X axis to use the horizontal/vertical cut range
	NumberAxis XAxis = (NumberAxis)plot.getDomainAxis();
	XAxis.setVisible(false);
	XAxis.setTickLabelsVisible(false);
	XAxis.setAutoRangeIncludesZero(false);
	XYSeries cut = dataset.getSeries(1); // The cutSeries
	// SRankin
	// XAxis.setRange(cut.getMinX(), cut.getMaxX());
	XAxis.setLabelInsets(new org.jfree.ui.RectangleInsets(0,0,0,0));
	XAxis.setTickLabelInsets(new org.jfree.ui.RectangleInsets(0,0,0,0));

	// Set up the Y axis to auto range
	NumberAxis YAxis = (NumberAxis)plot.getRangeAxis();
	YAxis.setVisible(false);
	YAxis.setTickLabelsVisible(false);
	YAxis.setAutoRangeIncludesZero(false);
	YAxis.setLabelInsets(new org.jfree.ui.RectangleInsets(0,0,0,0));
	YAxis.setTickLabelInsets(new org.jfree.ui.RectangleInsets(0,0,0,0));

	plot.setBackgroundPaint(null);
	plot.setDomainGridlinesVisible(false);
	plot.setRangeGridlinesVisible(false);
	plot.setInsets(new org.jfree.ui.RectangleInsets(0,0,0,0));
	plot.setAxisOffset(new org.jfree.ui.RectangleInsets(0,0,0,0));
	plot.setOutlineVisible(false);
	// SRankin
	// ChartUtilities.applyCurrentTheme(chart);
        return chart;
    }

    // Draw a box around the centroid region and plot cuts through the star
    private void doStarPlot() {

	if (centX*centY == 0) {
	    return;		// Invalid centroid
	}

	int plotX = (int) Math.round(centX);
	int plotY = (int) Math.round(centY);

	// First draw the box
	int hBox = (int)Math.round(curZoom*sizCent/2.);
	int sBox = (int)Math.round(curZoom*(sizCent+1));

	// Upper left corner of the centroid box in screen units
	int dotX = (int)Math.round((centX - curLowX)*curZoom) - hBox;
	int dotY = (int)Math.round((curHighY - centY)*curZoom) - hBox;
	
	Rectangle centBox = new Rectangle(dotX, dotY, sBox, sBox);
	graStar.setPaint(Color.GREEN);
	graStar.draw(centBox);

	// Now draw the cuts in X and Y through the centroid
	// Horizontal cut
	centBox.setLocation(dotX, dotY - sBox);
	chartX.getPlot().draw(graStar, centBox, null, null, null);

	// Vertical cut
	centBox.setLocation(dotX+sBox, dotY);
	chartY.getPlot().draw(graStar, centBox, null, null, null);

    }

    private Rule columnView;
    private Rule rowView;
    private ScrollablePicture picture;
    private JScrollPane pictureScrollPane;
    private DecimalFormat dFormat = new DecimalFormat();
    private JFormattedTextField zoomText = new JFormattedTextField(dFormat);
    private JButton zoomIn;
    private JButton zoomOut;
    private BufferedImage bufStar;
    private Graphics2D graStar;
    private WritableRaster rasterStar;
    private double lowX0, highX0, lowY0, highY0; // Unzoomed pixel numbers
    private double Zoom0; 	// Save for Reset
    private int[] pixVal0;	// Unzoomed pixels in 1-D array in screen order
    private int[] normPix;	// Pixels normalized to 0-255 for display
    private int nCol0, nRow0;

    private int[] zPix;		// 1-D array of zoomed pixels
    private BufferedImage bufZoom; // Zoomed BufferedImage
    private WritableRaster rasterZoom; // Corresponding WritableRaster
    private int nZRow = 0;		// Zoomed rows and columns
    private int nZCol = 0;

    // Zoom factor and top-left corner pixel number of the current view's Zoom
    private double curZoom;	
    private double curLowX = 0; // Current top left corner pixel coordinates
    private double curHighY = 0;

    private JCheckBox autoGain;
    private JCheckBox autoOffset;
    private JFormattedTextField gainText;
    private JFormattedTextField offsetText;
    private double Gain = 1;
    private double Offset = 0;
    private JPanel buttonPanel;
    private BoxLayout layout;

    public int pixelMin;	// Minimum pixel intensity
    public int pixelMax;	// Maximum pixel intensity

    public int sizCent = 21;	// Size of centroid algorithm region

    public double centX;	// Centroid coordinates
    public double centY;
    public double centPeak;	// Centroid peak and widths
    public double widthX;
    public double widthY;
    public int valid;		// Centroid Valid state
    public double centSky;	// Mean of centroid perimeter pixels
    public int boxX;		// Centroid box corner in screen coordinates
    public int boxY;
    public double vCentX;	// Last valid centroid coordinates
    public double vCentY;

    private JFreeChart chartX;		// Cut through X
    private JFreeChart chartY;		// Cut through Y

    public int[] starRow;
    public int[] starCol;


    private static final double sqr2 = Math.sqrt(2.);
    private static final double peakFac = 4.*Math.log(2.)/Math.PI;
}
