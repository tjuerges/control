package alma.Control.device.gui.OpticalTelescope;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.font.*;
import java.awt.geom.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.imageio.*;
import javax.imageio.stream.*;
import java.io.*;
import java.text.DecimalFormat;
import java.text.Format;
import java.util.Iterator;
import java.util.Arrays;
import java.util.Date;

// Print the status information in the frame
// The status alarm items are printed in green (OK) or red (Bad)

public class statusInfo extends JPanel {

    public statusInfo(optStatus newStatOPT) {
	statOPT = newStatOPT;
	allAlarm = new ALabel[statOPT.Status.length+1]; // One extra ALabel

	// The statusInfo panel will contain two JPanels of status side by side
	setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));

	// Status information that isn't associated with the centroid
	JLabel OPTSinfo = new JLabel("      OPTS Status Information"); 
	Font fBold = labelFont.deriveFont(Font.BOLD);
	OPTSinfo.setFont(fBold);
	FontRenderContext context = getFontMetrics(fBold).getFontRenderContext();
	Rectangle2D titleSize = fBold.getStringBounds(OPTSinfo.getText(),context);
	double fontHeight = titleSize.getHeight();

	// Component for the left-hand status column
	JPanel leftStatusColumn = new JPanel();
	leftStatusColumn.setLayout(new BoxLayout(leftStatusColumn, 
						 BoxLayout.PAGE_AXIS));

	leftStatusColumn.add(Box.createRigidArea(new Dimension(0, 
							  (int)fontHeight)));
	leftStatusColumn.add(OPTSinfo);
	leftStatusColumn.add(Box.createRigidArea(new Dimension(0, 
						          (int)(fontHeight/2))));

	// Make a column of OPTS items to display
	SLabel CamPower = new SLabel("Camera Power", statOPT.CamPower, 
				    new int[] {1});
	CamPower.setBad(optStatus.name2State("Off"));
	leftStatusColumn.add(CamPower);
	allLabel[numLabel++] = CamPower;

	SLabel CamStat = new SLabel("Camera State etc:", statOPT.CamStat, 
				    new int[] {1, 0, 0, 0}, 1);
	leftStatusColumn.add(CamStat);
	allLabel[numLabel++] = CamStat;

	SLabel CamTemp = new SLabel("CCD Temp & Set Pt", statOPT.CamTemp, 
				    new int[] {0, 0}, 0);
	leftStatusColumn.add(CamTemp);
	allLabel[numLabel++] = CamTemp;
	
	SLabel CentAlgP = new SLabel("Centroid Params", statOPT.CentAlgP, 
				     new int[] {0, 0}, 0);
	leftStatusColumn.add(CentAlgP);
	allLabel[numLabel++] = CentAlgP;

	SLabel CenThresh = new SLabel("Centroid Threshold", statOPT.CenThresh, 
				      new int[] {0}, 0);
	leftStatusColumn.add(CenThresh);
	allLabel[numLabel++] = CenThresh;

	SLabel Fan = new SLabel("Fan", statOPT.Fan, new int[] {1});
	leftStatusColumn.add(Fan);
	allLabel[numLabel++] = Fan;

	SLabel FiltPos = new SLabel("Filter State & Position", statOPT.FiltPos, 
				    new int[] {1, 1});
	FiltPos.setBad(optStatus.name2State("Blocked"));

	leftStatusColumn.add(FiltPos);
	allLabel[numLabel++] = FiltPos;

	SLabel FlatField = new SLabel("Applying Flat Field", statOPT.FlatField, 
				      new int[] {1});
	leftStatusColumn.add(FlatField);
	allLabel[numLabel++] = FlatField;

	SLabel FocusInit = new SLabel("FocusInit Correction", statOPT.FocusInit, 
				      new int[] {0}, 2);
	leftStatusColumn.add(FocusInit);
	allLabel[numLabel++] = FocusInit;

	SLabel FocusPos = new SLabel("Focus Position & State", statOPT.FocusPos, 
				     new int[] {0, 1}, 2);
	leftStatusColumn.add(FocusPos);
	allLabel[numLabel++] = FocusPos;

	SLabel Shutter = new SLabel("Shutter", statOPT.Shutter, new int[] {1});
	Shutter.setGood(optStatus.name2State("Open"));

	leftStatusColumn.add(Shutter);
	allLabel[numLabel++] = Shutter;

	SLabel SunSensor = new SLabel("SunSensor 1, 2, FS Status", 
			      statOPT.SunSensor, new int[] {1, 1, -1, -1, 1}, 0);
	leftStatusColumn.add(SunSensor);
	allLabel[numLabel++] = SunSensor;

	SLabel SunSensorV = new SLabel("SunSensor 1, 2 Volts", 
			     statOPT.SunSensor, new int[] {-1, -1, 0, 0, -1}, 2);
	leftStatusColumn.add(SunSensorV);
	allLabel[numLabel++] = SunSensorV;

	SLabel SunThresh = new SLabel("SunSensor 1, 2  Threshold", 
				      statOPT.SunThresh, new int[] {0, 0}, 2);
	leftStatusColumn.add(SunThresh);
	allLabel[numLabel++] = SunThresh;

	SLabel TelHeater = new SLabel("Telescope Heater", statOPT.TelHeater, 
				    new int[] {1});
	leftStatusColumn.add(TelHeater);
	allLabel[numLabel++] = TelHeater;

	SLabel TelTemp = new SLabel("Tube & Filter Temperatures", 
				    statOPT.TelTemp, new int[] {0, 0}, 2);
	leftStatusColumn.add(TelTemp);
	allLabel[numLabel++] = TelTemp;

	SLabel DcDt = new SLabel("Centroid Drifts per dTemp", 
				    statOPT.DcDt, new int[] {0, 0}, 2);
	leftStatusColumn.add(DcDt);
	allLabel[numLabel++] = DcDt;

	add(leftStatusColumn);

	// Component for the righ-hand status column
	JPanel rightStatusColumn = new JPanel();
	rightStatusColumn.setLayout(new BoxLayout(rightStatusColumn, 
						 BoxLayout.PAGE_AXIS));
	// Centroid values
	JLabel OPTScentroid = new JLabel("     OPTS Centroid Information"); 
	OPTScentroid.setFont(fBold);
	rightStatusColumn.add(Box.createRigidArea(new Dimension(0, 
							  (int)fontHeight)));
	rightStatusColumn.add(OPTScentroid);
	rightStatusColumn.add(Box.createRigidArea(new Dimension(0, 
							  (int)(fontHeight/2))));
	
	int[] allOff = new int[] {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
	int[] oneOn;

	oneOn = allOff.clone();
	oneOn[optStatus.CENTPEAK] = 0;
	SLabel CentPeak = new SLabel("Peak", statOPT.centroidValue, oneOn, 0);
	rightStatusColumn.add(CentPeak);
	allLabel[numLabel++] = CentPeak;

	oneOn = allOff.clone();
	oneOn[optStatus.CENTX] = 0;
	SLabel CentX = new SLabel("CentX", statOPT.centroidValue, oneOn, 1);
	rightStatusColumn.add(CentX);
	allLabel[numLabel++] = CentX;

	oneOn = allOff.clone();
	oneOn[optStatus.CENTY] = 0;
	SLabel CentY = new SLabel("CentY", statOPT.centroidValue, oneOn, 1);
	rightStatusColumn.add(CentY);
	allLabel[numLabel++] = CentY;

	oneOn = allOff.clone();
	oneOn[optStatus.WIDTHX] = 0;
	SLabel WidthX = new SLabel("WidthX", statOPT.centroidValue, oneOn, 1);
	rightStatusColumn.add(WidthX);
	allLabel[numLabel++] = WidthX;

	oneOn = allOff.clone();
	oneOn[optStatus.WIDTHY] = 0;
	SLabel WidthY = new SLabel("WidthY", statOPT.centroidValue, oneOn, 1);
	rightStatusColumn.add(WidthY);
	allLabel[numLabel++] = WidthY;

	oneOn = allOff.clone();
	oneOn[optStatus.RMS] = 0;
	SLabel RMS = new SLabel("RMS", statOPT.centroidValue, oneOn, 0);
	rightStatusColumn.add(RMS);
	allLabel[numLabel++] = RMS;

	oneOn = allOff.clone();
	oneOn[optStatus.VALID] = 1;
	SLabel Valid = new SLabel("Valid", statOPT.centroidValue, oneOn, 0);
	Valid.setBad(optStatus.name2State("Off"));
	rightStatusColumn.add(Valid);
	allLabel[numLabel++] = Valid;

	oneOn = allOff.clone();
	oneOn[optStatus.EXPTIME] = 0;
	SLabel ExpTime = new SLabel("Exposure Time", statOPT.centroidValue, 
				    oneOn, 2);
	rightStatusColumn.add(ExpTime);
	allLabel[numLabel++] = ExpTime;

	oneOn = allOff.clone();
	oneOn[optStatus.TIMESTAMP] = 2;
	SLabel TimeStamp = new SLabel("TimeStamp", statOPT.centroidValue, 
				      				       oneOn, 1);
	rightStatusColumn.add(TimeStamp);
	allLabel[numLabel++] = TimeStamp;

	SLabel corrXY = new SLabel("Centroid X & Y Corrections", 
				    statOPT.corrXY, new int[] {0, 0}, 2);
	rightStatusColumn.add(corrXY);
	allLabel[numLabel++] = corrXY;

	// Add Alarm Status
	JLabel OPTSalarm = new JLabel("OPTS Alarm Status"); 
	OPTSalarm.setFont(fBold);
	rightStatusColumn.add(Box.createRigidArea(new Dimension(0, (int)fontHeight)));
	rightStatusColumn.add(OPTSalarm);
	rightStatusColumn.add(Box.createRigidArea(new Dimension(0, (int)(fontHeight/2))));

	// First put in the communication status with the OPTS
	allAlarm[numAlarm] = new ALabel("OPTS Monitor", 0, statOPT.OPTrunning);
	rightStatusColumn.add(allAlarm[numAlarm++]);

	// Add all the Alarm Status items
	for (int i=0; i < statOPT.Status.length; i++) {
	    allAlarm[numAlarm] = new ALabel(i, statOPT.Status);
	    rightStatusColumn.add(allAlarm[numAlarm++]);
	}

	// Put some space between the columns
	add(Box.createRigidArea(new Dimension((int)fontHeight, 0)));

	add(rightStatusColumn);
	update();		// Apply any red colors
    }

    // Refresh the text's of the SLabels and ALabels with the new values
    public void update() {
	int i;
	for (i=0; i < numLabel; i++) {
	    allLabel[i].makeLabel();
	}
	for (i=0; i < numAlarm; i++) {
	    allAlarm[i].update();
	}
    }

    private static final int FONTSIZE = 11;
    private optStatus statOPT;
    public static final Font labelFont = 
				new Font("SansSerif", Font.PLAIN, FONTSIZE);
    private SLabel[] allLabel = new SLabel[MAX_SLABEL];
    private int numLabel = 0;
    private ALabel[] allAlarm;
    private int numAlarm = 0;
    private static final int MAX_SLABEL = 40; // Maximum number of SLabel's
}
 

// This class generates a JLabel with a text consisting of the specified
// <newPreface> followed by the contents of <newValue> (which may be a float[],
// a double[], or an int[]).  <newDec> specifies the number of decimal places to
// display of a double or float element. 

// <eFlag> is the same length as <newValue>.  <eFlag> elements == 1 indicate
// the corresponding <newValue> element is an enum state value instead of a
// literal and should be displayed as a string via the state2Name method of
// optStatus.  == 0 indicates a literal value and == -1 indicates an element
// to skip entirely.  double[] only: == 2 indicates a time stamp (seconds since
// the standard Epoch).

// The text is displayed with the statusInfo.labelFont font

// Method makeLabel() is used to regenerate the text when the elements of the
// newValue array have been changed

class SLabel extends JLabel {
    // Constructor for double[] values
    public SLabel(String newPreface, double[]newValue, int[] eFlag, int newDec) {
	super(newPreface);
	if (newValue.length != eFlag.length) {
	    System.out.println("Value and flag arrays different sizes for " + 
			                                             newPreface);
	    System.exit(1);
	}
	setFont(statusInfo.labelFont);
	dValue = newValue;
	fValue = null;
	iValue = null;
	Flag = eFlag;
	nDec = newDec;
	preface = newPreface;
	fmt = String.format(" %%.%df", nDec);
	makeLabel();		// Make initial value
    }

    // Constructor for float[] values
    public SLabel(String newPreface, float[]newValue, int[] eFlag, int newDec) {
	super(newPreface);
	if (newValue.length != eFlag.length) {
	    System.out.println("Value and flag arrays different sizes for " + 
			                                             newPreface);
	    System.exit(1);
	}
	setFont(statusInfo.labelFont);
	fValue = newValue;
	dValue = null;
	iValue = null;
	Flag = eFlag;
	nDec = newDec;
	preface = newPreface;
	fmt = String.format(" %%.%df", nDec);
	makeLabel();		// Make initial value
    }

    // Constructor for int[] values
    public SLabel(String newPreface, int[]newValue, int[]eFlag) {
	super(newPreface);
	if (newValue.length != eFlag.length) {
	    System.out.println("Value and flag arrays different sizes for " + 
			                                             newPreface);
	    System.exit(1);
	}
	setFont(statusInfo.labelFont);
	Flag = eFlag;
	iValue = newValue;
	dValue = null;
	fValue = null;
	preface = newPreface;
	fmt = " %d";
	makeLabel();		// Make initial value
    }

    // Set the Bad state for the label.  A state with this name will make
    // the color of the label red.  (Only works for Values with Flag=1.)
    // Use an impossible enum value like -999 to cancel a previous setBad()
    public void setBad(int theBadVal) {
	badVal = theBadVal;
    }
	
    // Set the Good state for the label.  A state without this name will make
    // the color of the label red.  (Only works for Values with Flag=1.)
    // Use an impossible enum value like -999 to cancel a previous setGood()
    public void setGood(int theGoodVal) {
	goodVal = theGoodVal;
    }
	
    // Adds the string values of the Value array to the JLabel text
    // <nDec> is the number of decimal points printed 
    public void makeLabel() {
	String text = new String(preface);
	Color labelColor = Color.BLACK;
	if (dValue != null) {
	    for (int i=0; i < dValue.length; i++) {
		if (Flag[i] == 0) {
		    text += String.format(fmt, dValue[i]);
		} else if (Flag[i] == 1) {
		    text += " " + optStatus.state2Name( (int)dValue[i]);
		    if ((int)dValue[i] == badVal || 
				(goodVal != -999 && goodVal != (int)dValue[i])) {
			labelColor = Color.RED;
		    }
		} else if (Flag[i] == 2) {
		    Date timeStamp = new Date( (long)(1000.*dValue[i]));
		    text += String.format(" %tF %tT.%tL %tZ", timeStamp, 
					  timeStamp, timeStamp, timeStamp);
		}
	    }
	} else if (fValue != null) {
	    for (int i=0; i < fValue.length; i++) {
		if (Flag[i] == 0) {
		    text += String.format(fmt, fValue[i]);
		} else if (Flag[i] == 1) {
		    text += " " + optStatus.state2Name( (int)fValue[i]);
		    if ((int)fValue[i] == badVal || 
				(goodVal != -999 && goodVal != (int)fValue[i])) {
			labelColor = Color.RED;
		    }
		}
	    }
	} else if (iValue != null) {
	    for (int i=0; i < iValue.length; i++) {
		if (Flag[i] == 0) {
		    text += String.format(fmt, iValue[i]);
		} else if (Flag[i] == 1) {
		    text += " " + optStatus.state2Name(iValue[i]);
		    if (iValue[i] == badVal || 
				(goodVal != -999 && goodVal != iValue[i])) {
			labelColor = Color.RED;
		    }
		}
	    }
	}
	setForeground(labelColor);
	text += " ";		// Prevents "..." instead of "Off" ??
	setText(text);
    }
    private double[] dValue;
    private float[] fValue;
    private int[] iValue;
    private int[] Flag;
    private int nDec;
    private String preface;
    private String fmt;
    private int badVal = -999;	// Impossible badVal
    private int goodVal = -999; // Impossible goodVal
}
    
// This class generates a JLabel with text consisting of the name of the alarm
// in the OPT status array followed by its state: "Bad" or "OK".  The color
// of a Bad text will be red and of an OK text will be green.

class ALabel extends JLabel {
    public ALabel(int alarmIndex, int[] alarm) {
	Status = alarm;
	index = alarmIndex;
	preface = optStatus.getStatusName(alarmIndex);
	update();
    }

    // Provide a preface that isn't one of the getStatusName()'s
    public ALabel(String thePreface, int alarmIndex, int[] alarm) { 
	Status = alarm;
	index = alarmIndex;
	preface = thePreface;
	update();
    }

    public void update() {
	String value = optStatus.state2Name(Status[index]);
	setText(preface + " " + value);
	if (value.equals("OK")) {
	    setForeground(Color.GREEN);
	} else if (value.equals("Bad")) {
	    setForeground(Color.RED);
	} else {
	    setForeground(Color.ORANGE);
	}
    }
    private int index;
    private int[] Status;
    private String preface;
}
