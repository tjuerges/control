// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2010
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#include <OpticalPositionConsumer.h>
#include <TETimeUtil.h>
#include <sstream> // for ostringstream
#include <ControlBasicInterfacesC.h> // for CHANNELNAME_CONTROLREALTIME
#include <ControlExceptions.h>
#include <LogToAudience.h>

// shortcuts to avoid having to clutter the code with namespace qualifiers
using std::string;
using std::ostringstream;
using std::vector;
using ControlExceptions::IllegalParameterErrorExImpl;
using Control::OpticalTelescope;

OpticalPositionConsumer::OpticalPositionConsumer(const string& antennaName)
    :nc::SimpleConsumer<OpticalTelescope::StarPosition>
(Control::CHANNELNAME_CONTROLREALTIME),
     dataVector_m(0),
     sorted_m(true),
     antennaName_m(antennaName),
     startTime_m(0),
     stopTime_m(0),
     pastStopTime_m(false)
{
    pthread_mutex_init(&dataMtx_m, NULL);
    addSubscription<OpticalTelescope::StarPosition>
        (&OpticalPositionConsumer_eventHandler, dynamic_cast<void*>(this));
    consumerReady();
}

void OpticalPositionConsumer::disconnect() {
    pthread_mutex_destroy(&dataMtx_m);
    nc::SimpleConsumer<OpticalTelescope::StarPosition>::disconnect();
}

void OpticalPositionConsumer_eventHandler(OpticalTelescope::StarPosition data,
                                         void *reference){
    static_cast<OpticalPositionConsumer*>(reference)->processData(data);
}

void 
OpticalPositionConsumer::processData(OpticalTelescope::StarPosition data){

    pthread_mutex_lock(&dataMtx_m);
    //    if (!strcmp(data.antennaName, antennaName_m.c_str())) {
        const ACS::Time thisTime = data.timestamp;
        if ((thisTime >= startTime_m) && 
            ((stopTime_m == 0) || (thisTime < stopTime_m))) {
            if (sorted_m &&
                !dataVector_m.empty() &&
                dataVector_m.back().timestamp > thisTime) {
                sorted_m = false;
            }
            dataVector_m.push_back(data);
        } else if (!pastStopTime_m && thisTime >= stopTime_m) {
            pastStopTime_m = true;
        }
        //    }
    pthread_mutex_unlock(&dataMtx_m);
}

void OpticalPositionConsumer::clear(){
    pthread_mutex_lock(&dataMtx_m);
    dataVector_m.clear();
    sorted_m = true;
    pastStopTime_m = false;
    pthread_mutex_unlock(&dataMtx_m);
}

void OpticalPositionConsumer::clear(ACS::Time firstTimeToKeep) {
    pthread_mutex_lock(&dataMtx_m);
    if (!dataVector_m.empty()) {
        sortData();
        // The firstElem variable points to the element of the dataVector that
        // has the same timestamp as the requested timestamp. If there is no
        // element that has the requested timestamp it points to the element
        // with the next larger timestamp.
        vector<OpticalTelescope::StarPosition>::iterator firstElemToKeep =
            lower_bound(dataVector_m.begin(), dataVector_m.end(), firstTimeToKeep,
                        OpticalPositionConsumer::Compare_StarPosition_AcsTime());
        dataVector_m.erase(dataVector_m.begin(), firstElemToKeep);
    }
    pthread_mutex_unlock(&dataMtx_m);
}

unsigned long OpticalPositionConsumer::size(){
    return dataVector_m.size();
}

void OpticalPositionConsumer::setTimeRange(ACS::Time startTime, 
                                          ACS::Time stopTime) {
    pthread_mutex_lock(&dataMtx_m);
    startTime_m = startTime;
    stopTime_m = stopTime;
    pthread_mutex_unlock(&dataMtx_m);
    clear();
}

bool OpticalPositionConsumer::pastStopTime() {
    bool retVal;
    pthread_mutex_lock(&dataMtx_m);
    retVal = pastStopTime_m;
    pthread_mutex_unlock(&dataMtx_m);
    return retVal;
}

bool OpticalPositionConsumer::pastStopTime(ACS::Time stopTime) {
    bool gotDataAtEnd = false;
    // I need to subtract a TE from the stop time as we may not be collecting
    // any more data beyond the specified stop time. Subtracting a TE ensures
    // we always see the last value we do collect.
    stopTime -= TETimeUtil::TE_PERIOD_DURATION.value;
    pthread_mutex_lock(&dataMtx_m);
    if (!dataVector_m.empty()) {
        sortData();
        const ACS::Time lastTime = dataVector_m.back().timestamp;
        if (lastTime >= stopTime) {
            gotDataAtEnd = true;
        }
    }
    pthread_mutex_unlock(&dataMtx_m);
    return gotDataAtEnd;
}

vector<OpticalTelescope::StarPosition> OpticalPositionConsumer::
getDataRange(const ACS::Time& startTime, const ACS::Time& stopTime) {
    // Check that startTime < stopTime;
    if (startTime >= stopTime) {
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ostringstream msg;
        msg << "Cannot collect pointing data as the start time of " 
            << TETimeUtil::toTimeString(startTime) 
            << " is not less than the stop time of "
            << TETimeUtil::toTimeString(stopTime) ;
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex;
    }

    pthread_mutex_lock(&dataMtx_m);
    sortData();

    // The firstElem variable points to the element of the dataVector that has
    // the same timestamp as the requested timestamp. If there is no element
    // that has the requested timestamp it points to the element with the next
    // larger timestamp.
    vector<OpticalTelescope::StarPosition>::const_iterator firstElem =
        lower_bound(dataVector_m.begin(), dataVector_m.end(), startTime,
                    OpticalPositionConsumer::Compare_StarPosition_AcsTime());

    // The lastElem variable points to the next element of the dataVector that
    // has a timestamp greater than the requested timestamp. It will point one
    // beyond the end of dataVector if all elements in dataVector have a
    // timestamp less than than stoptime
    vector<OpticalTelescope::StarPosition>::const_iterator lastElem =
        upper_bound(dataVector_m.begin(), dataVector_m.end(), stopTime,
                    OpticalPositionConsumer::Compare_StarPosition_AcsTime());

    const vector<OpticalTelescope::StarPosition> retVal(firstElem, lastElem);
    pthread_mutex_unlock(&dataMtx_m);

    return retVal;
}

vector<OpticalTelescope::StarPosition> OpticalPositionConsumer::getData() {
    pthread_mutex_lock(&dataMtx_m);
    sortData();
    vector<OpticalTelescope::StarPosition> retVal = dataVector_m;
    pthread_mutex_unlock(&dataMtx_m);

    return retVal;
}

OpticalTelescope::StarPosition OpticalPositionConsumer::operator[](int idx){
    OpticalTelescope::StarPosition returnValue;
    pthread_mutex_lock(&dataMtx_m);
    sortData();

    returnValue = dataVector_m[idx];
    pthread_mutex_unlock(&dataMtx_m);

    return returnValue;
}

void OpticalPositionConsumer::sortData() {
    if (!sorted_m) {
        sort(dataVector_m.begin(),dataVector_m.end(),
             OpticalPositionConsumer::Compare_StarPosition());
        sorted_m = true;
    }
}


