#! /usr/bin/env python
#******************************************************************************
# "@(#) $Id$"
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2009
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

import time # for 'sleep' command
import math
import optparse
import CCL.OpticalTelescope
import os

if __name__ == '__main__':
    parser = optparse.OptionParser()

    parser.add_option("-a", "--antenna",
                      help="The antenna to use, e.g., DV01, PM03")
    parser.add_option("--beginAt", type = "float",
                      default = "6.0",
                      help="The starting position for the focus scan in mm")
    parser.add_option("--endAt", type = "float",
                      default = "4.0",
                      help="The end position for the focus scan in mm")
    parser.add_option("--stepSize", type = "float",
                      default = "0.1",
                      help=" How far to step when scanning in mm")
    parser.add_option('-d', '--daytime', action="store_true", dest = "dayTime")

    (options, args) = parser.parse_args()

opt = CCL.OpticalTelescope.OpticalTelescope(options.antenna, stickyFlag=True)
opt.SET_DoCamPower(True)
opt.openShutter()

step = abs(options.stepSize);

if (options.beginAt < options.endAt):
    posns = range(int(options.beginAt/step), int(options.endAt/step)+1)
else:
    posns = range(int(options.beginAt/step), int(options.endAt/step)-1, -1)
    step *= -1

cwd = os.getcwd()
# Focus speed is about 1mm/sec
scan = 1;
radToArcsec = 180/math.pi*60*60
f = options.beginAt;
for p in posns:
    opt.SET_DoFocus(f)
    time.sleep(1);
    focus = opt.GET_QueryFocusPos()[0]
    r = opt.doExposure(1.0)
    longWidth = ' None  '
    if r.longWidth > 0:
        longWidth = '%6.3f'%(r.longWidth*radToArcsec)
    latWidth = 'None'
    if r.latWidth > 0:
        latWidth = '%6.3f'%(r.latWidth*radToArcsec)
    print 'Scan', '%2d'%scan, 'Focus:', '%.2f'%focus, 'Width ', longWidth, latWidth
    opt.writeLastImage(cwd + '/focus-' + '%03d'%scan + ".fits");
    f += step
    scan += 1
    
