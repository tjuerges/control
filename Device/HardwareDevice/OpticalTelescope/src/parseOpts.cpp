#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <string>

#include "sock.h"
#include "parseOpts.h"
#include "fitsio.h"

// Current state of the OPTS as derived by parsing its responses to date
struct opts optState;		

int lastCentroid = MAXCENTROID-1;	/* index of most recent centroid */
struct centroid centBuf[MAXCENTROID];	/* Circular buffer of centroids */

int lastFITS = 0;		 /* Index of latest FITS file in buffer */
int curSeqNo[MAXFITSBUF];	 /* Current image sequence numbers */
size_t curImageSize[MAXFITSBUF]; /* Size of the FITS images, including header */
char curImage[MAXFITSBUF][MAXFITS+1];	/* Current FITS images */

int lastError = MAXERROR-1;	/* Pointer to oldest error in ErrorHistory */
char *ErrorHistory[MAXERROR];	/* List of last MAXERROR errors */
char *StopText;			/* Response to last Stop command */

/* Functions found in struct params .fctn Will be called with 3
 * parameters: pointer to the text being parsed (with no NUL's from
 * strtok_r), the corresponding struct params, and the number of
 * parameters parsed
 */
int doCamStat(FCTNPAR);		/* Set [1], [2] = 0 unless [0] = Exposing */
int doCentAlg(FCTNPAR);		/* Copy parameters to opts.CentAlg */
int doEngStatus(FCTNPAR);	/* Copy string to EngStatus. Parse it */
int doHotPix(FCTNPAR);		/* Construct a hotpix array from the pixel list */
int doMasterMB(FCTNPAR);	/* Clear opts.MasterMB if 2nd par is "Done" */
int doSeqNo(FCTNPAR);		/* Zero NextSeqNo if = */
int doSlew(FCTNPAR);            /* Choose between Query Slew & Do Slew response */
int doStatus(FCTNPAR);		/* Process Status keyword-value list */
int doSubImage(FCTNPAR);	/* If first is All, convert to 0,0, MAX,MAX */
int doError(FCTNPAR);		/* Add error to error log */
int doExpCount(FCTNPAR);	/* Update optState.CamStat[2] with new count */
int doCamTemp(FCTNPAR);		/* Move xCamTemp into optState.CamTemp.  If only
				   one parameter received, it goes into [1] */
int doCentroid(FCTNPAR);	/* Parse centroid data & put in buffer */
int doFITS(FCTNPAR);		/* Display Image, complete FITS header 
				   and write to file, if any */
int doBias(FCTNPAR);		/* For implementation of /Notify */
int doBlank(FCTNPAR);		/* For implementation of /Notify */
int doExpose(FCTNPAR);		/* For implementation of /Notify */
int doFilter(FCTNPAR);		/* For implementation of /Notify */
int doFilterInit(FCTNPAR);	/* For implementation of /Notify */
int doFocus(FCTNPAR);		/* For implementation of /Notify */
int doFocusInit(FCTNPAR);	/* For implementation of /Notify */
int doShutter(FCTNPAR);		/* Shutter[1] = OK if not Fault; Do response */
int doStop(FCTNPAR);		/* Save entire Stop text as one char string */

struct centroid xCentroid;	/* Temporary storage for parsing */

float xCamTemp[2];		/* Temp storage.  
				   Response to Set has only 2nd parameter */
float xCentAlg[4];		/* Temp storage.  First number is index i of
				   opts.CentAlg[i][] where the next are stored */
char *xMasterMB[2];		/* Temp Storage.  2nd="Done" => clear MasterMB */
int xSlew;			// Temp Storage for Query Slew & Do Slew respons

int xDoBias;			/* Temp Storage for implementation of /Notify */
int xDoBlank;                   /* /Notify working depends upon an  */
float xDoExpose[2];		/* application setting the Doxxx element of  */
int xDoFilter;			/* struct opt to the enum state <Pending> */
int xDoFilterInit;		/* value prior to sending the Do command with */
float xDoFocus;                 /* For /Notify response or Focus position */
double xDoFocusInit[2];		// Temp storage for Query & Do FocusInit
int xShutter[2];		// For /Notify response or Shutter query

/* List of legal enum state's for various command responses */
int CamPowerList[] = {Off, On, optEND};
int CamStatList[] = {Off, Idle, Exposing, Power, optSEP, Off, On, optNUM, 
		     optSEP, optNUM, optEND};
int FanList[] = {Off, On, OK, optEND};
int FiltPosList[] = {At, MovingTo, Stuck, optSEP,
		     Clear, Blocked, IRFilt, OtherFilt, Unknown, optEND};
int FlatFieldList[] = {None, Factory, ALMA, optEND};
int FocusPosList[] = {optNUM, optSEP, MovingFwd, MovingBack, Stopped, Limit, 
		      Unknown, optEND};
int ShutterList[] = {Open, Closed, Opening, Closing, Unknown, OK, Done, 
		     optSEP, Fault, optEND};
int SubImageList[] = {All, optNUM, optSEP, optNUM, optEND};
int SunSensorList[] = {Tripped, Ready, optSEP, Tripped, Ready, optSEP, optNUM, optSEP, 
		       optNUM, optSEP, Tripped, Ready, optEND};
int TelHeaterList[] = {Off, On, optEND};
int BiasList[] = {OK, Zeroed, Done, optEND};
int BlankList[] = {Zeroed, OK, Done, optEND};
int ExposeList[] = {Aborted, Done, optNUM, optSEP, optNUM, optEND};
int FilterList[] = {Clear, Blocked, IRFilt, OtherFilt, Done, optEND};
int FilterInitList[] = {Started, Done, optEND};
int FlatLoadList[] = {OK, Done, optEND};
int FocusList[] = {Done, optNUM, optEND};
// DoFocusInitList is combination of FocusInitList & the Do FocusInit response
// int FocusInitList[] = {optNUM, optSEP, optTSTAMP, optEND};
int DoFocusInitList[] = {OK, Started, Done, optNUM, optSEP, optTSTAMP, optEND};
int ShutUpList[] = {OK, Done, optEND};
// Combination of responses for Query Slew and Do Slew
int SlewList[] = {ENo, EYes, OK, Done, optEND};


/* Special lists for status command keywords */
int StsCCDList[] = {OK, Bad, optEND};
int StsCamTempList[] = {OK, optNUM, optEND};
int StsFiltPosList[] = {OK, Bad, optEND};
int StsFocusPosList[] = {OK, Bad, optEND};
int StsShutterList[] = {OK, Bad, optEND};
int StsSunSensorList[] = {OK, Tripped, optEND};
int StsTelTempList[] = {OK, optNUM, optEND};

/* statusLists must be in same order as statusName */
int *statusLists[] = {&StsCCDList[0], &StsCamTempList[0], 
		       &StsFiltPosList[0], &StsFocusPosList[0],
		       &StsShutterList[0], 
		       &StsSunSensorList[0], &StsTelTempList[0]};
  
struct params keyTable[] = {
/* Query Command responses */
  {"CamPower", 1, optINT, &optState.CamPower, NULL, &CamPowerList[0]},
  {"CamStat", 4, optFLOAT, &optState.CamStat, doCamStat, &CamStatList[0]},
  {"CamTemp", 2, optFLOAT, &xCamTemp, doCamTemp, NULL},
  {"CentAlg", 4, optFLOAT, &xCentAlg, doCentAlg, NULL},
  {"CenThresh", 1, optFLOAT, &optState.CenThresh, NULL, NULL},

  {"EngStatus", 0, optCHAR, &optState.EngStatus, doEngStatus, NULL},
  {"ExpTime", 1, optFLOAT, &optState.ExpTime, NULL, NULL},
  {"Fan", 1, optINT, &optState.Fan, NULL, &FanList[0]},
  {"FiltPos", 2, optINT, &optState.FiltPos, NULL, &FiltPosList[0]},
  {"FlatField", 1, optINT, &optState.FlatField, NULL, &FlatFieldList[0]},

  {"FocusPos", 2, optFLOAT, &optState.FocusPos, NULL, &FocusPosList[0]},
  {"HotPix", 0, 0, &optState.HotPix, doHotPix, NULL},
  {"IDCamera", 1, optCHAR, &optState.IDCamera, NULL, NULL},
  {"IDTCM", 1, optCHAR, &optState.IDTCM, NULL, NULL},
  {"MasterMB", 2, optCHAR, &xMasterMB, doMasterMB, NULL},

  {"SeqNo", 1, optINT, &optState.SeqNo, doSeqNo, NULL},
  {"Shutter", 2, optINT, xShutter, doShutter, &ShutterList[0]},
  {"Status", 0, 0, &optState.Status, doStatus, NULL},
  {"SubImage", 4, optINT, &optState.SubImage, doSubImage, &SubImageList[0]},
  {"SunSensor", 5, optFLOAT, &optState.SunSensor, NULL, &SunSensorList[0]},
  {"Status", 0, 0, &optState.Status, doStatus, NULL},
  {"SubImage", 4, optINT, &optState.SubImage, doSubImage, &SubImageList[0]},
  {"SunSensor", 5, optFLOAT, &optState.SunSensor, NULL, &SunSensorList[0]},

  {"SunThresh", 2, optFLOAT, &optState.SunThresh, NULL, NULL},
  {"TelHeater", 1, optINT, &optState.TelHeater, NULL, &TelHeaterList[0]},
  {"TelTemp", 2, optFLOAT, &optState.TelTemp, NULL, NULL},
  {"DxDt", 1, optFLOAT, &optState.DxDt, NULL, NULL},
  {"DyDt", 1, optFLOAT, &optState.DyDt, NULL, NULL},

/* Set command responses not above */
  {"ExpCount", 0, 0, NULL, doExpCount, NULL},
  {"Centroid", 1, optINT, &xCentroid.SeqNo, doCentroid, NULL},
  {"SIMPLE", 0, 0, NULL, doFITS, NULL},

/* Do command responses */
  {"Bias", 1, optINT, &xDoBias, doBias, &BiasList[0]},
  {"Blank", 1, optINT, &xDoBlank, doBlank, &BlankList[0]},
  {"Expose", 2, optFLOAT, &xDoExpose, doExpose, &ExposeList[0]},
  {"Filter", 1, optINT, &xDoFilter, doFilter, &FilterList[0]},
  {"FilterInit", 1, optINT, &xDoFilterInit, doFilterInit, &FilterInitList[0]},
  {"FlatLoad", 1, optINT, &optState.FlatLoad, NULL, &FlatLoadList[0]},
  {"Focus", 1, optFLOAT, &xDoFocus, doFocus, &FocusList[0]},
  {"FocusInit", 2, optDOUBLE, &xDoFocusInit, doFocusInit, &DoFocusInitList[0]},
  {"ShutUp", 1, optINT, &optState.ShutUp, NULL, &ShutUpList[0]},
  {"Slew", 1, optINT, &xSlew, doSlew, &SlewList[0]},
  {"Stop", 0, 0, NULL, doStop, NULL},
  {"TelHeater", 1, optINT, &optState.TelHeater, NULL, &TelHeaterList[0]},

/* Error response to one of the commands */
  {"Error", 0, 0, NULL, doError, NULL},

/* Marker for end of table */
  {NULL, 0, 0, NULL, NULL, NULL}
};

int CentTimeStampList[] = {optTSTAMP, optEND};

struct params CentTable[] = {
  {"Peak", 1, optFLOAT, &xCentroid.Peak, NULL, NULL},
  {"CentX", 1, optFLOAT, &xCentroid.CentX, NULL, NULL},
  {"CentY", 1, optFLOAT, &xCentroid.CentY, NULL, NULL},
  {"WidthX", 1, optFLOAT, &xCentroid.WidthX, NULL, NULL},
  {"WidthY", 1, optFLOAT, &xCentroid.WidthY, NULL, NULL},
  {"RMS", 1, optFLOAT, &xCentroid.RMS, NULL, NULL},
  {"Valid", 1, optINT, &xCentroid.Valid, NULL, NULL},
  {"ExpTime", 1, optFLOAT, &xCentroid.ExpTime, NULL, NULL},
  {"TimeStamp", 1, optDOUBLE, &xCentroid.TimeStamp, NULL, 
  							&CentTimeStampList[0]},

/* Marker for end of table */
   {NULL, 0, 0, NULL, NULL, NULL}
};
/* Number of centroid keyword we are expecting */
#define NUMCENTROID (int)(sizeof(CentTable)/sizeof(CentTable[0]) - 1)

char OptsMsg[MAXFITS+1];	    /* Latest stdin/incoming mailbox message */
int lenMsg;			    /* Length of the above */

/* Convert a FITS timestamp into seconds since 00:00:00 UTC, January 1, 1970 
   Error return will be < 0 */
double timestamp2sec(char *timestamp)
{
  struct tm stamp;
  int year, month, day, hour, minute, status = 0, time;
  double second;

  memset(&stamp, 0, sizeof(stamp));

  if (fits_str2time(timestamp, &year, &month, &day, &hour, &minute, 
		    &second, &status) != 0) {
    return -1;			/* Error */
  }
  stamp.tm_year = year - 1900;
  stamp.tm_mon = month - 1;
  stamp.tm_mday = day;
  stamp.tm_hour = hour;
  stamp.tm_min = minute;
  time = timegm(&stamp);
  if (time < 0)
    return -2;			/* Error */
  return time+second;
}

void initState(void);

/* state2num()
 *
 * Scan <input> for up to <max> parameters of the specified <kind>,
 * according to the <list> if any.  <output> points to the location to
 * store the value of the first parameter.  The <input> string is
 * expected to contain parameters separated by white space.  strtok()
 * is used to parse <input> and will replace the white space character
 * following each parameter value by a NUL character.
 *
 * <kind> is an integer code #defined (optINT, optFLOAT, optDOUBLE, or optCHAR).
 * For any but optCHAR, numeric values will be assigned to the
 * parameter(s).  If a parameter equals one of the names found in
 * stateName, the corresponding enum value will be assigned (if it is
 * allowed by the <list>).  Upon success, <found> will be set to the
 * number of parameters found in <input> and the return value will be
 * zero.  <nextValue> points to the position in <input> that follows
 * the value of the last parameter found.  (Or NULL, if nothing
 * follows it.)
 *
 * <list>, if any, is an array of legal enum values for the
 * parameter(s).  The final element of <list> must be the marker, optEND.
 * Other markers are optNUM: a number string allowed by <kind>. optTSTAMP: A
 * FITS format time stamp to be converted into seconds since 1/1/1970.
 * (If both optNUM and optTSTAMP are specified, a optTSTAMP conversion will be
 * tried first.)  optSEP: marks the end of the specifications of the
 * current parameter and should be followed by enum's and/or markers
 * allowed for the following parameter.  If the specification of a
 * parameter is terminated by optEND instead of optSEP, the same
 * specification will be applied to any subsequent parameters.  If <list>
 * is not specified (NULL), it is assumed to be optNUM, optEND.
 *
 * If <kind> is optCHAR, a buffer is allocated for the text of the token
 * which is copied there.  The value of the parameter is a pointer to
 * this buffer.  If the parameter had a buffer pointer already, that
 * space is deallocated first.  <list> is ignored for optCHAR's.
 *
 * Return codes: 
 * 0 - Success 

 * 1 - illegal value for parameter <found>+1.  <nextValue> points to
 * the location within <input> of the offending text.  (parameter
 * <found>+1 is unchanged)

 * -1 - illegal call:
 * <input>, <output>, <found>, and/or <nextValue> are NULL, <max> is
 * <= 0, and/or <kind> is illegal

 * -2 - illegal <list>: optSEP found before any enums or markers (zero
 * length list for a parameter) or no optEND found (same as list longer
 * than MAXLIST)

 * -3 - illegal <list>: Illegal enum value for parameter <found>+1
 *      *nextValue points to the bad value in <list>

 * -4 - Could not allocate space for a character string                     
 */

int state2num(char *input, void *output, int kind, int max, int *list, 
	      int *found, char **nextValue)
{
  int startList = 0, endList = -1, i, nEnum = 0, nl, nFlag, tFlag, totLen;
  double val;
  int defaultList[] = {optNUM, optEND};
  char *token = NULL, *ptr, delim[] = " \t,", *scratch, *arg;

  if (stateName1[0] == NULL)
    initState();		/* Initialize stateName1 & stateName2 */

  if (input == NULL || found == NULL || nextValue == NULL || max < 0)
    return -1;

  if (max > 0 && (output == NULL || kind < optINT || kind > optCHAR))
    return -1;

  totLen = strlen(input);
  if (list == NULL) {
    list = defaultList;
    endList = 1;
  } else if (list[0] == optSEP)
    return -2;			/* Zero length parameter list is illegal */

  *found = 0;
  arg = input;

  while (*found < max) {
/* Process next parameter */
    token = strtok_r(arg, delim, &scratch);
    if (token == NULL)
      break;
    arg = NULL;

    if (kind == optCHAR) {
      if (*(char **)output != NULL)
	delete[] *(char **)output;	/* Return previously allocated space */
      *(char **)output = new char[strlen(token)+1]; /* Get space for new string */
      if (*(char **)output == NULL)
	return -4;
      else
	strcpy(*(char **)output, token);
      output = (void *)(((char **)output)+1);
      (*found)++;
      continue;			/* Skip to end of while */
    }

/* It's a non-CHAR kind */
    if (endList == -1 || list[endList] == optSEP) {
/* Find the list entries for the next parameter */
      startList = endList+1;
      for (endList=startList; endList < MAXLIST && 
	     (list[endList] != optEND) && (list[endList] != optSEP); endList++) {
/* Check that the enums in the list are legal */
	if (list[endList] > optEND || 
	    list[endList] < optTSTAMP) { 	// If not a marker
/* Get the index of the name in stateName (enum state values must be in 
 * numerical order with no gaps, and stateName must be in the same order) */
	  i = list[endList] - stateEnum[0]; 
	  if (i < 0 || i >= NUMSTATE) {
	    *nextValue = (char *)&list[endList];
	    return -3;		/* Illegal enum value in <list> */
	  }
	}
	continue;
      }
      if (endList == MAXLIST || endList == startList)
	return -2;
    }
/* Check for enum values */
    *nextValue = token;	/* In case of error return */
    for (i=startList, nFlag=0, tFlag=0; i<endList; i++) {
      if (list[i] == optNUM)
	nFlag = 1;		/* NUM flag */
      else if (list[i] == optTSTAMP) 
	tFlag = 1;		/* TSTAMP flag */
      else {
	/* Check for first word of stateName */
	nEnum = list[i] - stateEnum[0];
	if (strcasecmp(token, stateName1[nEnum]) == 0) {
	  if (strchr(stateName[nEnum], ' ') == NULL)
	    break;		/* stateName[nEnum] is one word */
	  else {
	    /* State name has other word(s) */
	    ptr = token + strlen(token) + 1; /* Check them */
	    nl = strlen(stateName2[nEnum]);
	    if (strncasecmp(ptr, stateName2[nEnum], nl) == 0) {
	      arg = ptr + nl;	/* Other word(s) match, too */
	      break;		/* Move next strtok_r() past them */
	    }
	  }
	}
      }
    }
    if (i != endList)
      val = stateEnum[nEnum];	/* Found an enum state in the <list> */
    else {			/* Not an enum */
      if (! tFlag && ! nFlag) {	// If not expecting a timestamp or a number,
	return 1;		// Not an enum state in the <list>
      }
      if (tFlag) {
	val = timestamp2sec(token); // It should be a time stamp
	if (val < 0 && nFlag == 0)
	  return 1;		/* Not a timestamp & not expecting a number */
	if (val >= 0)
	  nFlag = 0;		/* TSTAMP OK & takes precedence over NUM */
      }
      if (nFlag) {
	switch(kind) {		// Expecting a number
	case optINT:
	  if (isnumber(token) == 1)
	    val = atoi(token);	// It's an integer
	  else {
	    *nextValue = token;	// Not a valid integer
	    return 1;
	  }
	  break;
	case optFLOAT:
	case optDOUBLE:
	  if (isnumber(token))
	    val = atof(token);	// It's a float/double number
	  else {
	    *nextValue = token;	// Not a valid float/double
	    return 1;
	  }
	  break;
	default:
	  return -1;		// The <kind> is not a numeric one
	}
      } 	       		/* End of if (nFlag) */
    }				/* End of else of if (i != endList) */
    switch(kind) {
    case optINT:
      *(int *)output = (int)val;
      output = (void *)(((int *)output)+1);
      break;
    case optFLOAT:
      *(float *)output = val;
      output = (void *)(((float *)output)+1);
      break;
    case optDOUBLE:
      *(double *)output = val;
      output = (void *)(((double *)output)+1);
      break;
    default:
      return -1;
    }
    (*found)++;
  } 				/* End of while(1) */
  /* Success */
  if (token == NULL)
    *nextValue = NULL;		/* There were fewer than max parameters */
  else {
    ptr = token + strlen(token); /* Point to end of the input parsed */
    if (ptr - input >= totLen)
      *nextValue = NULL;		/* Have parsed all of the <input> */
    else
      *nextValue = ++ptr;		/* Point to the unparsed <input> */
  }
  return 0;
}

/* Initialize stateName1 & stateName2 for function state2num*/
void initState(void)
{
  int i, n;
  char *ptr;

  for (i=0; i<NUMSTATE; i++) {
    ptr = strchr((char *)stateName[i], ' ');
    if (ptr == NULL) {
      stateName1[i] = (char *)stateName[i]; 	   /* A one-word state name */
      stateName2[i] = NULL;
    } else {
      /* There's at least one blank in stateName[i] */
      n = ptr - stateName[i];		/* Length of first word */
      if (n == 0) {
	printf("stateName[%d] starts with blank(s): (%s)\n", i, stateName[i]);
	exit(0);
      }
      stateName1[i] = new char[n+1]; 	/* For first word & terminator */
      memset(stateName1[i], 0, n+1);
      strncpy(stateName1[i], stateName[i], n);
      ptr++;				/* Advance past the space */
      if (*ptr == ' ') {
	printf("stateName[%d] has more than one blank "
	       "between its first 2 words: (%s)\n", i, stateName[i]);
	exit(0);
      }
      n = strlen(ptr);
      stateName2[i] = new char[n+1]; 	/* For rest of stateName[i] */
      memset(stateName2[i], 0, n+1);
      strcpy(stateName2[i], ptr);
    }
  }
}

/* Process error return from state2num */
void errState2num(int ret, char *token, int numPar, char *nextValue)
{
  switch (ret) {
  default:
    break;
  case 1:
    printf("Illegal value for parameter %d of %s: %s\n", numPar+1, token,
	   nextValue);
    break;
  case -1:
    printf("Illegal call to state2num for keyword %s\n", token);
    printf("Input, output, and/or nextValue is NULL, max < 0, and/or "
	   "illegal code for kind\n");
    break;
  case -2:
    printf("Illegal list in call to state2num for keyword %s\n", token);
    break;
  case -3:
    printf("Illegal enum value in list for parameter %d of keyword %s: %d\n",
	   numPar+1, token, *(int *)nextValue);
    break;
  case -4:
    printf("Could not allocate space for a character string for "
	   "parameter %d of keyword %s\n", numPar+1, token);
    break;
  }
  return;
}

// ---------------- Functions in .fctn of struct parms -----------------

/* Set [1], [2] = 0 unlesss [0] = Exposing */
int doCamStat(FCTNPAR)
{
  if (optState.CamStat[0] == Power) {
    // Response is usually "Power Off" but allow for "Power On", too
    optState.CamStat[0] = optState.CamStat[1];	// Put Power state in [0]
  }
  if (optState.CamStat[0] != Exposing) {
    optState.CamStat[1] = optState.CamStat[2] = 0; // Non-zero only if Exposing
  }
  return 0;
}

/* Copy parameters to opts.CentAlg */
int doCentAlg(FCTNPAR)
{
  int i, n;
  n = (int)xCentAlg[0];		/* Index into opts.CentAlg */
  if (n < 1 || n > MAXALG)
    return 1;			/* Illegal algorithm number */
  optState.CentAlgN = n;	// Current algorithm number
  n--;
  for (i=0; i<MAXALG_P; i++)
    optState.CentAlg[n][i] = xCentAlg[i+1];	/* Copy the nth CentAlg's parameters */
  return 0;
}	


/* Copy string to EngStatus. Parse it */
int doEngStatus(FCTNPAR)
{
  if (optState.EngStatus != NULL)
    delete[] optState.EngStatus;	/* Deallocate previous status text */
  optState.EngStatus = new char[strlen(text)+1]; /* Allocate space for the new text */
  if (optState.EngStatus == NULL)
    return 1;			/* Error return */
  strcpy(optState.EngStatus, text);
  /* Parse the string once ACE defines what's in it */
  return 0;
}


/* Construct a hotpix array from the pixel list */
int doHotPix(FCTNPAR)
{
  char *token, *ptr, *scratch, space[] = " ";
  int n, m, ret = 0;
  bool any = false;

  token = strtok_r(text, space, &scratch); 	/* "HotPix" */
  token = strtok_r(NULL, space, &scratch); 	/* First hot pixel */
  if (token == NULL)
    return 1;			/* Error return */
  memset(&optState.HotPix[0], 0, sizeof(optState.HotPix)); /* Clear old array */
  if (strcmp(token, "(none)") == 0) {
    optState.anyHotPix = false;
    return 0;			/* There are no hot pixels */
  }
  while (token != NULL) {
    ptr = strchr(token, ',');
    if (ptr == NULL) {
      ret = 2;			/* No comma; value is not n,m  */
      break;
    }
    n = atoi(token);
    m = atoi(ptr+1);
    if (n < 0 || n >= MAXPIX || m < 0 || m >= MAXPIX) {
      ret = 3;			/* Illegal pixel number */
      break;
    }
    optState.HotPix[n][m] = 1;
    any = true;
    token = strtok_r(NULL, space, &scratch); /* Next pixel, if any */
  }
  optState.anyHotPix = any;
  return ret;			/* Success */
}

/* Clear opts.MasterMB if 2nd par is "Done" otherwise move name into struct opt */
int doMasterMB(FCTNPAR)
{
  if (npar == 2 && strcasecmp(xMasterMB[1], "Done") == 0) {
    if (optState.MasterMB != NULL)
      delete[] optState.MasterMB;	/* Deallocate Master Mailbox name */
    optState.MasterMB = NULL;
  } else {
    if (optState.MasterMB != NULL) 
      delete[] optState.MasterMB;	/* Deallocate previous Master Mailbox name */
    optState.MasterMB = xMasterMB[0]; /* Move pointer to current name into opt */
    xMasterMB[0] = NULL;
  }
  return 0;
}

/* Zero NextSeqNo if = */
int doSeqNo(FCTNPAR)
{
  if (optState.SeqNo == optState.NextSeqNo)
    optState.NextSeqNo = 0;		/* Indicate that Set NextSeqNo is done */
  return 0;
}

// Choose where to store the result: Query Slew or Do Slew location
int doSlew(FCTNPAR)
{
  if (xSlew == EYes || xSlew == ENo) {
    optState.Slew = xSlew;
  } else {
    optState.DoSlew = xSlew;
  }
  return 0;
}

/* Process Status keyword-value list */
int doStatus(FCTNPAR) {
  char tmp[lenMsg-6], *tmpLine, delim[] = " \t", lineDelim[] = "\n";
  char *token, *line, *ptr, *scratchLin, *scratch, *nextValue;
  int i, ret, numPar;

/* This routine processes multiple lines, so we use the original
 * sock_sel buffer, not the one line copy of the "text" parameter
 * And we skip over the first word, "Status"
 */
  memcpy(&tmp, OptsMsg+7, lenMsg-7);	/* Copy for strtok to mangle */
  tmp[lenMsg-7] = '\0';	/* Make sure it ends in a NUL */
  text += strlen(text) + 1;	/* Point to the beginning of the second line */
  *text = '\0';			/* Mark it empty to make processOpt quit */

  line = strtok_r(&tmp[0], lineDelim, &scratchLin); /* First Status line */
  while (line != NULL) {
    /* Process a status line */
    tmpLine = new char[strlen(line)+1];
    if (tmpLine != NULL)
      strcpy(tmpLine, line);	/* Copy for strtok to mangle */
    else {
      printf("Could not allocate storage to process %s\n", line);
      return 1;
    }
    token = strtok_r(tmpLine, delim, &scratch);
/* Search for status name */
    for (i=0; i<NUMSTATUS && strcasecmp(token, statusName[i]) != 0; i++)
      continue;
    if (i == NUMSTATUS) {
      printf("Unknown status %s in %s\n", token, line);
      delete[] tmpLine;
      line = strtok_r(NULL, lineDelim, &scratchLin);
      continue;
    } else {
      ptr = token + strlen(token) + 1; /* Point to argments */
      ret = state2num(ptr, &optState.Status[i][0], optFLOAT, 1, 
		      statusLists[i], &numPar, &nextValue);
      if (ret != 0)
	errState2num(ret, token, numPar, nextValue);
      else if (numPar == 0)
	printf("No parameter for Status %s\n", token);
      else if (optState.Status[i][0] != OK) {
	optState.Status[i][1] = optState.Status[i][0];
	optState.Status[i][0] = Bad;
      }
    }
    delete[] tmpLine;
    line = strtok_r(NULL, lineDelim, &scratchLin);
  } 				/* End of while (line != NULL) */
  return 0;			/* Successfully parsed Status info */
}


/* If first is All, convert to 0,0, MAX,MAX */
int doSubImage(FCTNPAR)
{
  if (npar == 1 && optState.SubImage[0] == All) {
    optState.SubImage[0] = optState.SubImage[1] = 0;
    optState.SubImage[2] = optState.SubImage[3] = MAXPIX-1;
  }
  return 0;
}

/* Add error to error history */
int doError(FCTNPAR)
{
  char *ptr, delim[] = " \t\n", *scratch;
  int n;

  n = strlen(text) + 1;
  char text2[n];
  strcpy(text2, text);		/* Make a copy for strtok_r to mangle */

  ptr = strtok_r(text2, delim, &scratch); /* Get "Error" */
  ptr = strtok_r(NULL, delim, &scratch);	 /* Get first word of complaint */
  if (ptr == NULL)
    return 1;			/* No error text */

  ptr = text + (ptr - text2);	/* Point to first word in original */
  lastError++;			/* Increment to oldest ErrorHistory entry */
  if (lastError >= MAXERROR)
    lastError = 0;		/* Wrap around */
  if (ErrorHistory[lastError] != NULL)
    delete[] ErrorHistory[lastError]; /* Deallocate oldest error text */
  ErrorHistory[lastError] = new char[n - (ptr - text)]; /* Space for new text */
  if (ErrorHistory[lastError] == NULL)
    return 1;			/* Could not get space */
  strcpy(ErrorHistory[lastError], ptr);
  return 0;
}

      
/* Update optState.CamStat[2] with new count */
int doExpCount(FCTNPAR)
{
  int n;
  char *token, *scratch, delim[] = " \t";

/* Expecting "ExpCount now n, was m" */
  token = strtok_r(text, delim, &scratch);
  token = strtok_r(NULL, delim, &scratch);
  token = strtok_r(NULL, delim, &scratch);
  if (token == NULL)
    return 1;
  n = atoi(token);
  optState.CamStat[2] = n;
  return 0;
}


/* Move xCamTemp into optState.CamTemp.  If only one parameter received, 
 * it goes into [1] */
int doCamTemp(FCTNPAR)
{
  if (npar == 1)
    optState.CamTemp[1] = xCamTemp[0];
  else if (npar == 2) {
    optState.CamTemp[0] = xCamTemp[0];
    optState.CamTemp[1] = xCamTemp[1];
  } else
    return 1;			/* Error return */
  return 0;
}


/* Parse centroid data & put in buffer */
int doCentroid(FCTNPAR)
{
  char tmp[lenMsg+1], *tmpLine, delim[] = " \t", lineDelim[] = "\n";
  char *token, *line, *ptr, *scratchLin, *scratch, *nextValue;
  int i, nkeyword = 0, ret, numPar, keyfound[NUMCENTROID], myret = 0;

  memset(keyfound, 0, sizeof(keyfound));

/* This routine processes multiple lines, so we use the original
 * sock_sel buffer, not the one line copy of the "text" parameter
 * And we skip over the first word, "Status"
 */
  memcpy(&tmp, OptsMsg, lenMsg);	/* Copy for strtok to mangle */
  tmp[lenMsg] = '\0';		/* Make sure it ends in a NUL */
  text += strlen(text) + 1;	/* Point to the beginning of the second line */
  *text = '\0';			/* Mark it empty to make processOpt quit */

/* Get centroid the line with the (above) sequence number */
  line = strtok_r(&tmp[0], lineDelim, &scratchLin);
  line = strtok_r(NULL, lineDelim, &scratchLin); /* First keyword-value line */
  while (line != NULL) {
    /* Process a centroid keyword-value line*/
    tmpLine = new char[strlen(line)+1];
    if (tmpLine != NULL)
      strcpy(tmpLine, line);	/* Copy for strtok to mangle */
    else {
      printf("Could not allocate storage to process %s\n", line);
      return 1;
    }
    token = strtok_r(tmpLine, delim, &scratch); /* Centroid keyword */
/* Search for keyword name */
    for (i=0; CentTable[i].keyword != NULL && 
	   strcasecmp(token, CentTable[i].keyword) != 0; i++)
      continue;
    if (CentTable[i].keyword == NULL) {
      printf("Unknown centroid keyword %s in %s\n", token, line);
      delete[] tmpLine;
      line = strtok_r(NULL, lineDelim, &scratchLin);
      continue;
    } else {
      nkeyword++;
      ptr = token + strlen(token) + 1; 		/* Point to the argment(s) */
/* Parse the argument(s) */
      ret = state2num(ptr, CentTable[i].var, CentTable[i].kind,
		      CentTable[i].numValue, CentTable[i].list, &numPar,
		      &nextValue);
      if (ret != 0) {
	errState2num(ret, token, numPar, nextValue); /* Parse error */
	myret = -1;
      }
      else if (numPar == 0) {
	printf("No parameter for centroid %s\n", token);
	myret = 1;
      } else {
	keyfound[i] = 1;	/* Mark this one found */
      }
    }
    delete[] tmpLine;
    line = strtok_r(NULL, lineDelim, &scratchLin);
  } 				/* End of while (line != NULL) */
  if (nkeyword != NUMCENTROID) {
    printf("Centroid %d missing %d keyword(s):", xCentroid.SeqNo, 
	   NUMCENTROID-nkeyword);
    for (i=0; i<NUMCENTROID; i++) {
      if (keyfound[i] == 0)
	printf(" %s", CentTable[i].keyword);
    }
    printf("\n");
    myret = 2;
  } else if (myret == 0) {
/* Advance to the oldest location */
    if (++lastCentroid >= MAXCENTROID)
      lastCentroid = 0;
    centBuf[lastCentroid] = xCentroid;	/* Success.  Put into buffer */
  }
  return myret;			/* Done parsing Centroid values */
}

/* Display Image, complete FITS header and write to file, if any */
int doFITS(FCTNPAR)
{
  gotFITS(OptsMsg, lenMsg, 1);
  return 0;
}

/* If previous value was Pending, only Done will be put in struct opt */
int doBias(FCTNPAR)
{
  if (optState.DoBias != Pending) {
      optState.DoBias = xDoBias;
  } else if (xDoBias == Done) {
    optState.DoBias = Done;
  }
  return 0;
}

/* If previous value was Pending, only Done will be put in struct opt */
int doBlank(FCTNPAR)
{
  if (optState.DoBlank != Pending) {
    optState.DoBlank = xDoBlank;
  } else if (xDoBlank == Done) {
    optState.DoBlank = Done;
  }
  return 0;
}

/* If previous value was Pending, only Done will be put in struct opt */
int doExpose(FCTNPAR)
{
  if (optState.DoExpose == Pending && xDoExpose[0] == Done) {
    optState.DoExpose = Done;
  } else {
    if (xDoExpose[0] == Aborted) {
      optState.Expose[0] = Aborted;
      optState.Expose[1] = 0;
    } else {
      optState.Expose[0] = xDoExpose[0];
      optState.Expose[1] = xDoExpose[1];
    }
  }
  return 0;
}

/* If previous value was Pending, only Done will be put in struct opt */
int doFilter(FCTNPAR)
{
  if (optState.DoFilter == Pending && xDoFilter == Done) {
    optState.DoFilter = Done;
  } else {
    optState.Filter = xDoFilter;
  }
  return 0;
}

/* If previous value was Pending, only Done will be put in struct opt */
int doFilterInit(FCTNPAR)
{
  if (optState.DoFilterInit != Pending) {
    optState.DoFilterInit = xDoFilterInit;
  } else if (xDoFilterInit == Done) {
    optState.DoFilterInit = Done;
  }
  return 0;
}

/* If previous value was Pending, only Done will be put in struct opt */
int doFocus(FCTNPAR)
{
  if (optState.DoFocus == Pending && xDoFocus == Done) {
    optState.DoFocus = Done;
  } else {
    optState.Focus = xDoFocus;
  }
  return 0;
}

/* If previous value was Pending, only Done will be put in struct opt */
int doFocusInit(FCTNPAR)
{
  if (npar == 2) {
    optState.FocusInit[0] = xDoFocusInit[0]; // Response from Query FocusInit
    optState.FocusInit[1] = xDoFocusInit[1];
    return 0;
  }
  if (optState.DoFocusInit == Pending && xDoFocusInit[0] == Done) {
    optState.DoFocusInit = Done; 		// /Notify response
  } else {
    optState.DoFocusInit = (int)xDoFocusInit[0]; // Normal Do FocusInit response
  }
  return 0;
}

/* Set Shutter[1] = OK if only 1 parameter was read */
int doShutter(FCTNPAR)
{
  if (xShutter[0] == Done || xShutter[0] == OK) {
    // Do Shutter response
    if (optState.DoShutter != Pending || xShutter[0] == Done) {
      optState.DoShutter = xShutter[0];
    }
    return 0;
  }
  // Query Shutter response
  optState.Shutter[0] = xShutter[0];
  if (npar == 1) {
    optState.Shutter[1] = OK;
  } else {
    optState.Shutter[1] = xShutter[1];
  }
  return 0;
}

/* Save entire Stop text as one char string */
int doStop(FCTNPAR)
{
  if (StopText != NULL)
    delete[] StopText;		/* Deallocate old Stop text */
  StopText = new char[strlen(text)+1];
  if (StopText == NULL)
    return 1;			/* Could not allocate Stop text */
  strcpy(StopText, text);
  return 0;
}

// ----------------------- Utility routines ------------------------

/* Return non-zero if "string" only contains characters that could be a number:
 * digits, up to one decimal point, optional leading + or - sign.  
 * A single E is allowed if it is followed by an integer.  Returns 1 if 
 * string is an integer, 2 if string has a decimal point, 3 if string is an
 * E-format number.  Otherwise 0 is returned.
 * N.B. string should not contain leading or trailing blanks.
 */

int isnumber(char *string)
{
  const char *digits = "0123456789";
  int n;

  if (string[0] == '+' || string[0] == '-')
    string++;					/* Skip over the sign */

  n = strspn(string, digits);
  if (string[n] == '\0')
    return 1;					/* Only digits were found */

  if (string[n] == '.')	{
    n++;					/* Found a decimal point */
    n += strspn(string+n, digits);		/* More digits can follow */
  }

  if (string[n] == '\0')
    return 2;					/* A decimal number was found */

  if (string[n] != 'e' && string[n] != 'E')
    return 0;					/* Not a number */
  
  n++;						/* letter E was found */
  if (string[n] == '+' || string[n] == '-')
    n++;					/* Exponent has a sign */

  if (string[n] == '\0')
    return 0;					/* Bad: No exponent value */

  n += strspn(string+n, digits);
  if (string[n] == '\0')
    return 3;					/* E-format number was found */
  else
    return 0;					/* Not a number */
}

/* Return the current time in a string dd mmm yy hh:mm:ss.sss */

char *getCtime(int offset)
{
  double timed;
  long usec;
  static char timestr[256];
  static const char *months[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", 
			    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
  };
  struct timeval tvv,*tp;
  struct tm *tmnow;
  long sec, toff;

  tp = &tvv;
  gettimeofday(tp,0);
  tp->tv_usec += 500;               /* Add .0005 sec for rounding to millisec */
  if (tp->tv_usec >= 1000000)
  {
    tp->tv_usec -= 1000000;
    tp->tv_sec++;
  }

  toff = offset * 3600;
  sec = tp->tv_sec + toff;                                      /* add offset */
  tmnow = localtime(&sec);
  timed = ((double)tp->tv_usec / 1000000.0) * 1000.0;     /* Rounded ms */
  usec = (long)timed;

  sprintf(timestr,"%02d %3.3s %02d %02d:%02d:%02d.%03ld ",
	  tmnow->tm_mday, 
	  months[tmnow->tm_mon], 
	  tmnow->tm_year-100,
	  tmnow->tm_hour,
	  tmnow->tm_min,
	  tmnow->tm_sec,
	  usec);
  return(timestr);
}

// Copy FITS image into oldest buffer
void gotFITS(char *OptsMsg, int lenMsg, int briefFlag) {
  fitsfile *fptr;
  int status = 0;
  long imageNum;
  char line[81], *ptr;
  line[80] = '\0';

  if (++lastFITS >= MAXFITSBUF) {
    lastFITS = 0;		// Reset back to beginning
  }
  curImageSize[lastFITS] = lenMsg;
  memcpy(curImage[lastFITS], OptsMsg, lenMsg); /* Copy image to buffer */
  ptr = curImage[lastFITS];
  fits_open_memfile(&fptr, "xxx", READONLY, (void **)&ptr, 
		    &curImageSize[lastFITS], 0, NULL, &status);
  if (status) {
    fits_report_error(stderr, status);
  } else {
    fits_read_key_lng(fptr, (char*)"IMAGENUM", &imageNum, NULL, &status);
    if (status) {
      printf("IMAGENUM not found in FITS header\n");
      curSeqNo[lastFITS] = 0;
    } else {
      curSeqNo[lastFITS] = imageNum;
      if (briefFlag == 1) {
	printf("FITS received: image #%d\n", curSeqNo[lastFITS]);
      }
    }
    if (briefFlag > 1) {
      // Print entire FITS header
      printf("%s FITS received:\n", getCtime(0));
      for (ptr=OptsMsg; strncmp(ptr, "END      ", 9); ptr += 80) {
	strncpy(line, ptr, 80);
	printf("%s\n", line);
      }
      printf("END\n\n");
    }
  }
  fits_close_file(fptr, &status);
  return;
}

int processOpt(char *Msg, int lMsg)
{
  char tmp[lMsg+1], *tmpLine, delim[] = " \t", lineDelim[] = "\n";
  char *token, *line, *ptr, *scratchLin, *scratch, *nextValue;
  int i, ret, numPar, myret = 0;

  lenMsg = lMsg;
  memcpy(OptsMsg, Msg, lenMsg);
  OptsMsg[lMsg] = '\0';
  memcpy(&tmp, OptsMsg, lenMsg);	/* Copy for strtok to mangle */
  tmp[lenMsg] = '\0';			/* Make sure there's a trailing NUL */
  line = strtok_r(&tmp[0], lineDelim, &scratchLin);
  while (line != NULL) {
    /* Process an input line */
    tmpLine = new char[strlen(line)+1];
    if (tmpLine != NULL)
      strcpy(tmpLine, line);	/* Copy for strtok to mangle */
    else {
      printf("Could not allocate storage to process %s\n", line);
      return 1;
    }
    token = strtok_r(tmpLine, delim, &scratch);
    for (i=0; keyTable[i].keyword != NULL; i++) {
      if (strcasecmp(token, keyTable[i].keyword) == 0)
	break;
    }
    if (keyTable[i].keyword == NULL) {
      printf("Unknown keyword %s\n", token);
      myret++;
      delete[] tmpLine;
      line = strtok_r(NULL, lineDelim, &scratchLin);
      continue;
    }
    /* Parse the line for this keyword */
    ptr = token + strlen(token) + 1; /* Point to the parameters */
    if (keyTable[i].numValue > 0) {
      ret = state2num(ptr, keyTable[i].var, keyTable[i].kind,
		    keyTable[i].numValue, keyTable[i].list, &numPar, 
		    &nextValue);
    } else {
      ret = 0;
      numPar = 0;
    }
    if (ret == 0) {
    /* Success */
      if (keyTable[i].fctn != NULL) {
	ret = (* keyTable[i].fctn)(line, keyTable[i], numPar);
	if (ret) {
	  printf("Error %d from fctn of keyword %s\n", ret, token);
	  myret++;
	}
      } 
    } else {
      myret++;
      errState2num(ret, token, numPar, nextValue);
    }
    delete[] tmpLine;
    line = strtok_r(NULL, lineDelim, &scratchLin);
  }			/* End of while (line != NULL) */
  return myret;
}
