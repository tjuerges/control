// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2009, 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA

#include <OpticalTelescopeImpl.h>
#include <TETimeUtil.h>
#include <loggingMACROS.h>
#include <vector>
#include <cmath>
#include <parseOpts.h>
#include <unistd.h> // for sleep
#include <sstream>
#include <ControlExceptions.h>
#include <LogToAudience.h>
#include <fstream>
#include <xmlTmcdbComponent.h>
#include <configDataAccess.h>
#include <acsncSimpleSupplier.h> // for nc::SimpleSupplier
#include <ControlBasicInterfacesC.h> // for CHANNELNAME_CONTROLREALTIME

using Control::OpticalTelescopeImpl;
using ControlExceptions::HardwareErrorExImpl;
using ControlExceptions::XmlParserErrorExImpl;
using std::vector;
using std::ostringstream;
using std::string;

// Declare those variables as static which makes them invisible from other
// C++ files.
static const short xSize = 1024;
static const short ySize = 1024;
static const double degToRad = M_PI / 180;


OpticalTelescopeImpl::OpticalTelescopeImpl(const ACE_CString& name,
    maci::ContainerServices* cs):
    OpticalTelescopeBase(name, cs),
    pixelSize_m(2.5E-6),
    rotation_m(0.0),
    cemThread_m(NULL) {
}

OpticalTelescopeImpl::~OpticalTelescopeImpl() {
    // cannot cleanUp here as it crashes the container. The problem is in the
    // call to OpticalTelescopeBase::cleanup() and needs to be investigated
    // further.
}

void OpticalTelescopeImpl::hwConfigureAction() {

    OpticalTelescopeBase::hwConfigureAction();

    XmlTmcdbComponent tmcdb(getContainerServices());
    const string instanceName = "OpticalTelescopeData_" + hostname_m;
    try {
        ConfigDataAccessor configData(tmcdb.getConfigXml(instanceName),
            tmcdb.getConfigXsd(instanceName));
        pixelSize_m = atof(
            configData.getElement("PixelSize")-> getStringData().c_str());
        rotation_m = atof(
            configData.getElement("Rotation")-> getStringData().c_str());
        ostringstream msg;
        msg << "Using a plate scale of "
            << pixelSize_m
            << " arcsec/pixel and a plate rotation of "
            << rotation_m
            << " degrees obtained from a data base (TMCDB).";
        LOG_TO_OPERATOR(LM_INFO, msg.str());
        pixelSize_m *= degToRad / 3600.0; // convert from arc-sec to radians
        rotation_m *= degToRad; // convert from deg to radians
    } catch (XmlParserErrorExImpl& ex) {
        ex.log();
        ostringstream msg;
        msg << "Cannot obtain the plate scale and rotation from the data base "
            "(TMCDB). Using a default plate scale of "
            << pixelSize_m * 3600 / degToRad
            << " arcsec/pixel and a default plate rotation of "
            << rotation_m / degToRad
            << " degrees.";
        LOG_TO_OPERATOR(LM_ERROR, msg.str());
        setError(msg.str());
    }
}

void OpticalTelescopeImpl::hwStopAction() {
    // suspend the thread that publishes data
    stopContinuousExposure();
    OpticalTelescopeBase::hwStopAction();
}

void OpticalTelescopeImpl::initialize() {
    // Do anything needed by the base class.
    OpticalTelescopeBase::initialize();

    // Now create the thread. Its starts suspended.
    string threadName = CORBA::String_var(name()).in();
    threadName += "/ContinuousExposureMonitoringThread";
    const ACS::TimeInterval loopTime = TETimeUtil::toTimeInterval(1.0);
    LOG_TO_DEVELOPER(LM_DEBUG, "Creating the thread");
    cemThread_m = getContainerServices()->getThreadManager()->
        create<ContinuousExposureMonitoringThread, OpticalTelescopeImpl>
        (threadName.c_str(), *this, loopTime, loopTime);
}

void OpticalTelescopeImpl::cleanUp() {

    // Do anything needed by the base class. This includes calling hwStop.
    OpticalTelescopeBase::cleanUp();

    // Destroy the thread. The OPT should have already been told, in the hwStop
    // function, to stop exposing.
    LOG_TO_DEVELOPER(LM_DEBUG, "Terminating the thread");
    cemThread_m->terminate();
}

void OpticalTelescopeImpl::initializeTelescope() {
    // TODO. Catch the exceptions this throws
    setDocampower(true);
    openShutter();
    nightTimeObserving();
    stopContinuousExposure();
}

void OpticalTelescopeImpl::shutdownTelescope() {
    try {
        stopContinuousExposure();
        // Close the shutter and move the filter wheel to blocked.
        setDoshutter(false);
        setDofilter(Blocked);
        if(isShutterOpen() != false) {
            LOG_TO_OPERATOR(LM_ERROR, "There was a problem shutting down the "
                            "optical telescope. Please check the shutter is closed before "
                            "walking away.");
        }

        waitUntilFilterIsInPosition(20, Blocked);
        // Do not turn the camera power off after each SB to prevent the camera
        // warming up.
    } catch(const HardwareErrorExImpl& ex) {
        HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
                                __PRETTY_FUNCTION__);
        const string msg("There was a problem shutting down the optical "
            "telescope. Please check the shutter is closed before walking "
            "away.");
        LOG_TO_OPERATOR(LM_ERROR, msg);
        nex.addData("Detail", msg);
        nex.log();
    }
}

void OpticalTelescopeImpl::fieldOfView(double& xSizeInRad,
    double& ySizeInRad,
    double& rotation) {
    xSizeInRad = xSize * pixelSize_m;
    ySizeInRad = ySize * pixelSize_m;
    rotation = rotation_m;
}

void OpticalTelescopeImpl::detectorSize(short& xSizeInPix,
    short& ySizeInPix) {
    xSizeInPix = xSize;
    ySizeInPix = ySize;
}

double OpticalTelescopeImpl::observingFrequency() {
    return 476E12;
}

Control::OctetSeq* OpticalTelescopeImpl::getLastImage() {
    Opts* opts_p(
        dynamic_cast<Opts*>(ethernetDeviceIF_mp.get()));
    int imageSize(-1);
    boost::shared_array<char> image(opts_p->getImage(imageSize));
    //     Control::OctetSeq_var
    //         corbaImage(imageSize, imageSize, &(image.get()), false);
    Control::OctetSeq_var corbaImage(imageSize);
    corbaImage->length(imageSize);
    for(int i(0); i < imageSize; ++i) {
        corbaImage[i] = image[i];
    }

    return corbaImage._retn();
}

void OpticalTelescopeImpl::saveLastExposure(const char* filename) {
    Opts* opts_p(
        dynamic_cast<Opts*>(ethernetDeviceIF_mp.get()));
    int imageSizeInBytes(-1);
    boost::shared_array<char> image(opts_p->getImage(imageSizeInBytes));

    std::ofstream fitsFile(filename);
    for(int i(0); i < imageSizeInBytes; ++i)
    {
        fitsFile << image[i];
    }
}

Control::OpticalTelescope::StarPosition
    OpticalTelescopeImpl::getLastStarPosition() {
    ACS::Time ts;
    return getStarPosition(getQueryseqno(ts));
}

Control::OpticalTelescope::StarPosition
    OpticalTelescopeImpl::getStarPosition(int sequenceNumber) {
    vector<double> lastPosition(10);
    lastPosition[0] = sequenceNumber;
    ACS::Time ts;
    monitor(Opts::GetCentroidN, lastPosition, ts);

    // TODO.
    // Check the sequence number is what we expect

    Control::OpticalTelescope::StarPosition star;
    star.xPosition = lastPosition[2];
    star.yPosition = lastPosition[3];

    { // NOTE. The longOffset is not corrected for cos(elevation). Thats done
        // in the OpticalPointing module.
        const double xOffsetInRad = (lastPosition[2] - xSize / 2.0)
            * pixelSize_m;
        const double yOffsetInRad = (lastPosition[3] - ySize / 2.0)
            * pixelSize_m;
        star.longOffset = xOffsetInRad * std::cos(rotation_m) -
            yOffsetInRad * std::sin(rotation_m);
        star.latOffset = xOffsetInRad * std::sin(rotation_m) +
            yOffsetInRad * std::cos(rotation_m);
    }

    {
        const double xWidthInRad = lastPosition[4] * std::abs(pixelSize_m);
        const double yWidthInRad = lastPosition[5] * std::abs(pixelSize_m);
        star.longWidth = xWidthInRad;
        star.latWidth = yWidthInRad;
    }

    star.brightness = lastPosition[1];
    star.noise = lastPosition[6];
    star.exposure = lastPosition[8];
    star.dataIsBad = lastPosition[7] < 0.5 ? true : false;
    star.number = static_cast<long>(lastPosition[0] + 0.5);
    star.timestamp = TETimeUtil::unix2epoch(
        static_cast<time_t>(lastPosition[9])).value;
    star.timestamp += static_cast<ACS::Time>(std::fmod(lastPosition[9], 1.0)
        * TETimeUtil::ACS_ONE_SECOND);

    return star;
}

bool OpticalTelescopeImpl::isShutterOpen() {
    return isShutterOpen(0);
}

bool OpticalTelescopeImpl::isShutterOpen(int counter) {
    if (counter > 20)
    {
        HardwareErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        throwHardwareErrorEx(ex, "Shutter has not stopped moving after 5 "
            "seconds. Its probably stuck.");
    }

    ACS::Time t(0ULL);
    const vector<int> state(getQueryshutter(t));
    if (state.size() != 2) {
        HardwareErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        throwHardwareErrorEx(ex, "Did not get the required number of bytes "
            "from the OPT. This is either a communications failure or a "
            "software fault.");
    }

    if (state[0] == Open && state[1] == OK) {
        return true;
    }

    if (state[0] == Closed && state[1] == OK) {
        return false;
    }

    if(state[1] == Fault) {
        HardwareErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        throwHardwareErrorEx(ex, "Shutter is in a fault state. Its either "
            "stuck or communications inside the OPT has failed.");
    }

    if ((state[0] == Opening) || (state[0] == Closing) ||
        (state[0] == Unknown)) {
        usleep(static_cast<useconds_t>(.25 * 1E6));
        return isShutterOpen(counter++);
    }

    HardwareErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    ostringstream msg;
    msg << "Unexpected value of [" << state[0] << ", " << state[1]
        << "] returned when querying the shutter position.";
    throwHardwareErrorEx(ex, msg.str());
    return false;
}

void OpticalTelescopeImpl::openShutter() {
    setDoshutter(true);
    isShutterOpen();
}

void OpticalTelescopeImpl::closeShutter() {
    setDoshutter(false);
}

void OpticalTelescopeImpl::
waitUntilFilterIsInPosition(int counter, int requestedFilter) {
    if (counter == 0) {
        HardwareErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        throwHardwareErrorEx(ex, "Timed out waiting for the filter to reach "
            "the requested position. Its probably stuck.");
    }

    ACS::Time t(0ULL);
    const vector<int> state(getQueryfiltpos(t));
    if (state.size() != 2) {
        HardwareErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        throwHardwareErrorEx(ex, "Did not get the required number of bytes "
            "from the OPT. This is either a communications failure or a "
            "software fault.");
    }

    if(state[0] == At && state[1] == requestedFilter) {
        return;
    }

    if(state[0] == Stuck) {
        HardwareErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        throwHardwareErrorEx(ex, "Filter is stuck.");
    }

    if((state[0] == MovingTo) || (state[0] == At)) {
        // This covers the case where we are at the previous filter
        // position as it takes a while for the wheel filter to start
        // moving.
        usleep(static_cast<useconds_t>(.25 * 1E6));
        return waitUntilFilterIsInPosition(counter--, requestedFilter);
    }

    HardwareErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    ostringstream msg;
    msg << "Unexpected value of [" << state[0] << ", " << state[1]
        << "] returned when querying the filter position.";
    throwHardwareErrorEx(ex, msg.str());
}

void OpticalTelescopeImpl::dayTimeObserving() {
    setDofilter(IRFilt);
    waitUntilFilterIsInPosition(20, IRFilt);
}

void OpticalTelescopeImpl::nightTimeObserving() {
    setDofilter(Clear);
    waitUntilFilterIsInPosition(20, Clear);
}

Control::OpticalTelescope::StarPosition
OpticalTelescopeImpl::doExposure(double exposureTime) {
    stopContinuousExposure();
    vector<double> exposureParameters(2);
    exposureParameters[0] = exposureTime;
    exposureParameters[1] = 1; // Do just one exposure
    setDoexpose(exposureParameters);
    const double pollingTime = .25; // seconds
    double waitTime = 0;
    bool finished = false;
    vector<double> queryResult;
    ACS::Time ts;
    while(!finished && waitTime < exposureTime + 4) {
        usleep(static_cast<unsigned long>(pollingTime * 1E6));
        queryResult = getQuerycamstat(ts);
        if((queryResult.empty() == false) && (queryResult[0] == Idle)) {
            finished = true;
        }

        waitTime += pollingTime;
    }

    if (finished == false) {
        HardwareErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        throwHardwareErrorEx(ex, "Timed out waiting for the latest exposure "
            "from the OPT. Please check no other clients are requesting "
            "exposures and try aborting this exposure.");
    }

    return getLastStarPosition();
}

void OpticalTelescopeImpl::startContinuousExposure(double exposureTime) {
    stopContinuousExposure();
    
    // get the last sequence number using Query SeqNo and tell the thread about it
    ACS::Time ts;
    cemThread_m->setLastExposureNumber(getQueryseqno(ts));
    // Start continuous exposures
    vector<double> exposureParameters(2);
    exposureParameters[0] = exposureTime;
    // 2**31-1 is the biggest number the OPT can handle. If we take ten
    // exposures per second that that over 6 years of exposures.
    exposureParameters[1] = static_cast<int>(std::pow(2.0, 31) - 1); 
    setDoexpose(exposureParameters);
    // start a thread to poll for centorids and publish the results
    LOG_TO_DEVELOPER(LM_DEBUG, "Resuming the thread.");    
    cemThread_m->resume();
}

void OpticalTelescopeImpl::stopContinuousExposure() {
    if (cemThread_m->isSuspended()) {
        return;
    }
    LOG_TO_DEVELOPER(LM_DEBUG, "Suspending the thread");
    cemThread_m->suspend();
    // tell the OPT to stop exposing    
    setSetexpcount(0);
}

bool OpticalTelescopeImpl::doingContinuousExposure() {
    return !cemThread_m->isSuspended();
}

void OpticalTelescopeImpl::setSetflatfield(int flatFieldMode)
{
    if(isReady() == false)
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        const string msg("Cannot execute control request.  Device is "
          "inactive.");
        ex.addData("Detail", msg);
        ex.log();
        throw ex;
    }

    try
    {
        ACS::Time timestamp(0ULL);
        // None comes from the state enum in parseOpts.h.
        int value(flatFieldMode + None);
        EthernetDevice::command(addressSetflatfield, value, timestamp);
        Control::HardwareDeviceImpl::clearDeviceErrorCounter();
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        throw EthernetDeviceExceptions::SocketOperationFailedExImpl(ex,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
    }
    catch(const EthernetDeviceExceptions::IllegalParameterExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        throw EthernetDeviceExceptions::IllegalParameterExImpl(ex, __FILE__,
        __LINE__, __PRETTY_FUNCTION__);
    }
    catch(const EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(ex, __FILE__,
        __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", "Caught a generic "
            "\"EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl\" "
            "exception.");
        throw nex;
    }
    catch(...)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        EthernetDeviceExceptions::SocketOperationFailedExImpl ex(__FILE__,
        __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", "Caught an unexpected exception.");
        throw ex;
    }
}

void OpticalTelescopeImpl::setDotelheater(bool onOff)
{
    if(isReady() == false)
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        const string msg("Cannot execute control request.  Device is "
          "inactive.");
        ex.addData("Detail", msg);
        ex.log();
        throw ex;
    }

    try
    {
        ACS::Time timestamp(0ULL);

        // On and Off come from the state enum in parseOpts.h.
        int value(Off);
        if(onOff == true)
        {
            value = On;
        }

        EthernetDevice::command(addressDotelheater, value, timestamp);
        Control::HardwareDeviceImpl::clearDeviceErrorCounter();
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        throw EthernetDeviceExceptions::SocketOperationFailedExImpl(ex,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
    }
    catch(const EthernetDeviceExceptions::IllegalParameterExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        throw EthernetDeviceExceptions::IllegalParameterExImpl(ex, __FILE__,
        __LINE__, __PRETTY_FUNCTION__);
    }
    catch(const EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(ex, __FILE__,
        __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", "Caught a generic "
            "\"EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl\" "
            "exception.");
        throw nex;
    }
    catch(...)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        EthernetDeviceExceptions::SocketOperationFailedExImpl ex(__FILE__,
        __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", "Caught an unexpected exception.");
        throw ex;
    }
}

void OpticalTelescopeImpl::setDostop(bool value)
{
    if(isReady() == false)
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        const string msg("Cannot execute control request.  Device is "
          "inactive.");
        ex.addData("Detail", msg);
        ex.log();
        throw ex;
    }

    try
    {
        ACS::Time timestamp(0ULL);
        EthernetDevice::command(addressDostop, timestamp);
        Control::HardwareDeviceImpl::clearDeviceErrorCounter();
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        throw EthernetDeviceExceptions::SocketOperationFailedExImpl(ex,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
    }
    catch(const EthernetDeviceExceptions::IllegalParameterExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        throw EthernetDeviceExceptions::IllegalParameterExImpl(ex, __FILE__,
        __LINE__, __PRETTY_FUNCTION__);
    }
    catch(const EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(ex, __FILE__,
        __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", "Caught a generic "
            "\"EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl\" "
            "exception.");
        throw nex;
    }
    catch(...)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        EthernetDeviceExceptions::SocketOperationFailedExImpl ex(__FILE__,
        __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", "Caught an unexpected exception.");
        throw ex;
    }
}

void OpticalTelescopeImpl::setDofilterinit(bool value)
{
    if(isReady() == false)
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        const string msg("Cannot execute control request.  Device is "
          "inactive.");
        ex.addData("Detail", msg);
        ex.log();
        throw ex;
    }

    try
    {
        ACS::Time timestamp(0ULL);
        EthernetDevice::command(addressDofilterinit, timestamp);
        Control::HardwareDeviceImpl::clearDeviceErrorCounter();
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        throw EthernetDeviceExceptions::SocketOperationFailedExImpl(ex,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
    }
    catch(const EthernetDeviceExceptions::IllegalParameterExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        throw EthernetDeviceExceptions::IllegalParameterExImpl(ex, __FILE__,
        __LINE__, __PRETTY_FUNCTION__);
    }
    catch(const EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(ex, __FILE__,
        __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", "Caught a generic "
            "\"EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl\" "
            "exception.");
        throw nex;
    }
    catch(...)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        EthernetDeviceExceptions::SocketOperationFailedExImpl ex(__FILE__,
        __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", "Caught an unexpected exception.");
        throw ex;
    }
}

void OpticalTelescopeImpl::setDofocusinit(bool value)
{
    if(isReady() == false)
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        const string msg("Cannot execute control request.  Device is "
          "inactive.");
        ex.addData("Detail", msg);
        ex.log();
        throw ex;
    }

    try
    {
        ACS::Time timestamp(0ULL);
        EthernetDevice::command(addressDofocusinit, timestamp);
        Control::HardwareDeviceImpl::clearDeviceErrorCounter();
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        throw EthernetDeviceExceptions::SocketOperationFailedExImpl(ex,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
    }
    catch(const EthernetDeviceExceptions::IllegalParameterExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        throw EthernetDeviceExceptions::IllegalParameterExImpl(ex, __FILE__,
        __LINE__, __PRETTY_FUNCTION__);
    }
    catch(const EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(ex, __FILE__,
        __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", "Caught a generic "
            "\"EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl\" "
            "exception.");
        throw nex;
    }
    catch(...)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        EthernetDeviceExceptions::SocketOperationFailedExImpl ex(__FILE__,
        __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", "Caught an unexpected exception.");
        throw ex;
    }
}

double OpticalTelescopeImpl::getQueryfocuspos(ACS::Time& timestamp)
{
    Control::HardwareDeviceImpl::checkHwStateOrThrow(
        &Control::HardwareDeviceImpl::isReady, __FILE__, __LINE__,
        __PRETTY_FUNCTION__);

    double value(0.0);

    try
    {
        EthernetDevice::monitor(addressQueryfocuspos, value, timestamp);
        Control::HardwareDeviceImpl::clearDeviceErrorCounter();
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        throw EthernetDeviceExceptions::SocketOperationFailedExImpl(ex,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
    }
    catch(const EthernetDeviceExceptions::IllegalParameterExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        throw EthernetDeviceExceptions::IllegalParameterExImpl(ex, __FILE__,
        __LINE__, __PRETTY_FUNCTION__);
    }
    catch(const EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(ex,__FILE__,
        __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", "Caught a generic "
            "\"EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl\" "
            "exception.");
        throw nex;
    }
    catch(...)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        EthernetDeviceExceptions::SocketOperationFailedExImpl ex(__FILE__,
        __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", "Caught an unexpected exception.");
        throw ex;
    }

    // Convert from mm to m.
    return (value / 1000.0);
}

void OpticalTelescopeImpl::setDofocus(double value)
{
    if(isReady() == false)
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        const string msg("Cannot execute control request.  Device is "
          "inactive.");
        ex.addData("Detail", msg);
        ex.log();
        throw ex;
    }

    try
    {
        ACS::Time timestamp(0ULL);
        // Convert from m to mm.
        double parameter(value * 1000.0);
        EthernetDevice::command(addressDofocus, parameter, timestamp);
        Control::HardwareDeviceImpl::clearDeviceErrorCounter();
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        throw EthernetDeviceExceptions::SocketOperationFailedExImpl(ex,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
    }
    catch(const EthernetDeviceExceptions::IllegalParameterExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        throw EthernetDeviceExceptions::IllegalParameterExImpl(ex, __FILE__,
        __LINE__, __PRETTY_FUNCTION__);
    }
    catch(const EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(ex, __FILE__,
        __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", "Caught a generic "
            "\"EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl\" "
            "exception.");
        throw nex;
    }
    catch(...)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        EthernetDeviceExceptions::SocketOperationFailedExImpl ex(__FILE__,
        __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", "Caught an unexpected exception.");
        throw ex;
    }
}

void OpticalTelescopeImpl::setDologfileflush(bool value)
{
    if(isReady() == false)
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        const string msg("Cannot execute control request.  Device is "
          "inactive.");
        ex.addData("Detail", msg);
        ex.log();
        throw ex;
    }

    try
    {
        ACS::Time timestamp(0ULL);
        EthernetDevice::command(addressDologfileflush, timestamp);
        Control::HardwareDeviceImpl::clearDeviceErrorCounter();
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        throw EthernetDeviceExceptions::SocketOperationFailedExImpl(ex,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
    }
    catch(const EthernetDeviceExceptions::IllegalParameterExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        throw EthernetDeviceExceptions::IllegalParameterExImpl(ex, __FILE__,
        __LINE__, __PRETTY_FUNCTION__);
    }
    catch(const EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(ex, __FILE__,
        __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", "Caught a generic "
            "\"EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl\" "
            "exception.");
        throw nex;
    }
    catch(...)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        EthernetDeviceExceptions::SocketOperationFailedExImpl ex(__FILE__,
        __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", "Caught an unexpected exception.");
        throw ex;
    }
}

void OpticalTelescopeImpl::throwHardwareErrorEx(
    HardwareErrorExImpl& ex, const string& msg) {
    ex.addData("Detail", msg);
    ex.log();
    throw ex.getHardwareErrorEx();
}

OpticalTelescopeImpl::ContinuousExposureMonitoringThread::
ContinuousExposureMonitoringThread(const ACE_CString& name,
                                   OpticalTelescopeImpl& opt,
                                   const ACS::TimeInterval& responseTime,
                                   const ACS::TimeInterval& sleepTime)
    :ACS::Thread(name, responseTime, sleepTime),
     opt_m(opt),
     lastSeqId_m(0)
{
    publisher_m =
        new nc::SimpleSupplier(Control::CHANNELNAME_CONTROLREALTIME, &opt);
}

OpticalTelescopeImpl::ContinuousExposureMonitoringThread::
~ContinuousExposureMonitoringThread() {
    if (publisher_m != NULL) {
        publisher_m->disconnect();
        publisher_m = NULL;
    }
}

void OpticalTelescopeImpl::ContinuousExposureMonitoringThread:: setLastExposureNumber(int lastNumber) {
    lastSeqId_m = lastNumber;
}

void OpticalTelescopeImpl::ContinuousExposureMonitoringThread::runLoop() try {
    // get last exposure number from OPT.
    ACS::Time ts;
    int lastExposure = opt_m.getQueryseqno(ts);
    {
        ostringstream msg;
        if ( lastSeqId_m >= lastExposure) {
            msg << "No exposures to process. First one to process is " 
                << lastSeqId_m+1 << " and the last one taken is " 
                << lastExposure;
        } else {
            msg << "Will process exposures from " << lastSeqId_m+1  
                << " to " << lastExposure;
        }
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    }
    for ( ; lastSeqId_m < lastExposure; lastSeqId_m++) {
        // get centroid for exposure lastSeqId_m
        Control::OpticalTelescope::StarPosition star = 
            opt_m.getStarPosition(lastSeqId_m+1);
        
        // publish the data
        publisher_m->
            publishData<Control::OpticalTelescope::StarPosition>(star);
        ostringstream msg;
        msg << " Processed exposure " << star.number;
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    }
 } catch (ACSErr::ACSbaseExImpl& ex) {
    ex.log();
    ostringstream msg;
    msg << "Caught an unexpected ACS exception in the runLoop function.";
    msg << " Ignoring it!";
    LOG_TO_DEVELOPER(LM_ERROR, msg.str());
 } catch (...) {
    ostringstream msg;
    msg << "Caught an unexpected exception in the runLoop function.";
    msg << " Ignoring it!";
    LOG_TO_DEVELOPER(LM_ERROR, msg.str());
 }


#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(OpticalTelescopeImpl)
