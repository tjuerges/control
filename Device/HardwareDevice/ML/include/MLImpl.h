#ifndef MLIMPL_H
#define MLIMPL_H
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2007
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//


// Base class(es)
#include <MLBase.h>

//CORBA servant header
#include <MLS.h>

// For the thread which checks periodically if the laser is still locked.
#include <acsThread.h>


namespace Control
{
    class AlarmSender;
};


class MLImpl;


/// Thread which checks the locking status of the ML.
class MlLockingStatusThread: public ACS::Thread
{
    public:
    /// Constructor.
    MlLockingStatusThread(const ACE_CString& name, MLImpl* _ml);

    /// Destructor.
    virtual ~MlLockingStatusThread();

    /// Executed every 60s and calls
    /// \ref TimeSourceImpl::calculate125MHzJitter.
    virtual void runLoop();


    private:
    /// Pointer to the \ref MLImpl class.
    MLImpl* ml;
};


class MLImpl: public virtual POA_Control::ML, public ::MLBase
{
    public:
    ///
    /// The constructor for any ACS C++ component must have this signature
    ///
    MLImpl(const ACE_CString& name, maci::ContainerServices* cs);

    ///
    /// The destructor must be virtual because this class contains virtual
    /// functions.
    ///
    virtual ~MLImpl();

    /// ACS component lifecycle execute method which initialises the alarms.
    virtual void execute();

    /// ACS component lifecycle cleanUp method which cleans up the alarms.
    virtual void cleanUp();


    private:
    virtual void hwStartAction();
    virtual void hwInitializeAction();
    virtual void hwOperationalAction();
    virtual void hwDiagnosticAction();
    virtual void hwStopAction();


    double convertToPowerTempMon(double value) const;

    virtual double rawToWorldSignalPowerModTempMon(short value) const;
    virtual double rawToWorldSignalOptRefModTempMon(short value) const;
    virtual double rawToWorldSignalLaserModTempMon(short value) const;

    virtual double rawToWorldSignalInfraredPdPwrMon(unsigned short value) const;
    virtual double rawToWorldSignalRedPdPwrMon(unsigned short value) const;
    virtual float rawToWorldLaserSignalFlPumpTemp(float value) const;
    virtual float rawToWorldLaserSignalFlModuleNtcV(float value) const;
    virtual double rawToWorldSignalInfoPidLaserCorrMonVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoPidErrorMonVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoPmDc10vVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoPiezoOutMonVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoPiezoSumMonVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoPowerModTempMonVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoOptRefModTempMonVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoLaserModTempMonVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoErrorPeakMonVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoPmDcPeakMonVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoRinDcMonVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoRinLaserPwrMonVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoRinErrorMonVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoRinDcCorrMonVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoVrefVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoGndVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoPplnTempMonVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoPplnPwrIMonVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoCellTempMonVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoTipTempMonVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoCellPwrIMonVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoInfraredPdPwrMonVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoRedPdPwrMonVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoHvMonVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoPmTempMonVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoFlTempVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoFlFastTempVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoFlSlowTempVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoPplnFastTempVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoPplnSlowTempVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoRbcellFastTempVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoRbcellSlowTempVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoRbcellTipFastTempVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoRbcellTipSlowTempVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoLockmonFastFluoVoltageRange(unsigned char) const;
    virtual double rawToWorldSignalInfoLockmonSlowFluoVoltageRange(unsigned char) const;
    virtual double rawToWorldLlAdcPowerModTempMon(short value) const;
    virtual double rawToWorldLlAdcOptRefModTempMon(short value) const;
    virtual double rawToWorldLlAdcLaserModTempMon(short value) const;
    virtual double rawToWorldLlAdcInfraredPdPwrMon(unsigned short value) const;
    virtual double rawToWorldLlAdcRedPdPwrMon(unsigned short value) const;
    virtual double rawToWorldPplnEfficiencyControlLastpwr(unsigned short value) const;
    virtual double rawToWorldPplnEfficiencyControlPwrPlus(unsigned short value) const;
    virtual double rawToWorldPplnEfficiencyControlPwrMinus(unsigned short value) const;
    virtual double rawToWorldPplnOptMaxRedPowerDuringScan(unsigned char) const;


    private:
    /// No copy constructor.
    MLImpl(const MLImpl&);

    /// No assignment operator.
    MLImpl& operator=(const MLImpl&);

    /// Initialise the helper system for alarms.
    void initialiseAlarmSystem();

    /// Clean up the helper system for alarms.
    void cleanUpAlarmSystem();

    /// Checks if the laser is locked.  Called every 300s by
    /// \ref Control::MlLockingStatusThread::runLoop.  Sets an alarm if it
    /// it found that the ML is not locked anymore.  Clears the alarm if
    /// the laser is locked.
    void checkLockingStatus();

    std::string myName;

    /// Conversion constants.
    const double convertToPowerTempMon_a;
    const double convertToPowerTempMon_b;
    const double convertToPowerTempMon_c;
    const double convertToPowerTempMon_d;

    /// Pointer to the thread which monitors the locking status of the ML.
    ACS::Thread* lockingStatusThread_p;

    /// Pointer for the alarm system helper instance.
    Control::AlarmSender* alarmSender;

    /// Enums for alarm conditions.
    enum LsAlarmConditions
    {
        MlIsNotLocked = 1,
        MlIsNotInAutomaticStartupMode,
        MlDoesColdStart,
        NoMonitorThread
    };

    /// Allow the \ref Control::MlLockingStatusThread to call
    /// \ref checkLockingStatus.
    friend void MlLockingStatusThread::runLoop();
};
#endif
