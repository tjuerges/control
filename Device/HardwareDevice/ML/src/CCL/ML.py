#! /usr/bin/env python
# "@(#) $Id$"
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
#
"""
This module is part of the Control Command Language.
It contains complementary functionality for LLC devices.
"""
import CCL.MLBase
from CCL import StatusHelper
import CCL.SIConverter


class ML(CCL.MLBase.MLBase):
    '''
    The ML class inherits from the code generated MLBase
    class and adds specific methods.
    '''
    def __init__(self, componentName = None, stickyFlag = False):
        '''
        The constructor creates a LLC object using LLCBase constructor.
        EXAMPLE:
        from CCL.ML import ML
        obj = ML()

        '''
        try:
            if componentName is None:
                componentName = "CONTROL/CentralLO/ML"
            CCL.MLBase.MLBase.__init__(self, None, componentName, stickyFlag)
        except Exception, e:
            print 'ML component is not running \n' + str(e)

    def __del__(self):
        CCL.MLBase.MLBase.__del__(self)

    def STATUS(self):
        '''
        Print out the status of the monitor points which inform about the
        locking status of the Master Laser.
        '''
        elements = []

        try:
            tmp, willy = self.GET_LASER_LOCKED()
        except:
            tmp = "N/A"
        elements.append(StatusHelper.Line("Laser Locked", \
            [StatusHelper.ValueUnit(tmp)]))

        try:
            tmp, willy = self.GET_AL_STATE()
            if tmp == 8:
                tmp = "Done, OK"
            elif tmp > 8:
                tmp = "Done, ERROR"
            else:
                tmp = "Busy"
        except:
            tmp = "N/A"
        elements.append(StatusHelper.Line("Autolock routine state" , \
            [StatusHelper.ValueUnit(tmp)]))

        try:
            tmp, willy = self.GET_PEAKS_STATE()
            if tmp == 4:
                tmp = "Done, OK"
            elif tmp > 4:
                tmp = "Done, ERROR"
            else:
                tmp = "Busy"
        except:
            tmp = "N/A"
        elements.append(StatusHelper.Line("Peak scanning state" , \
            [StatusHelper.ValueUnit(tmp)]))

        try:
            tmp, willy = self.GET_LOCKMON_STATE()
            if tmp[0] == 1:
                tmp1 = "Yes"
            else:
                tmp1 = "No"
            if tmp[1] == 0:
                tmp2 = "No"
            else:
                tmp2 = "Yes - ERROR"
        except:
            tmp1 = "N/A"
            tmp2 = "N/A"
        elements.append( StatusHelper.Line("Lock monitor state", \
            [StatusHelper.ValueUnit(tmp1, label = "Autolock successful")]))
        
        elements.append( StatusHelper.Line("                  ", \
            [StatusHelper.ValueUnit( \
                    tmp2, label = "Unlock detector triggered")]))

        try:
            tmp = "%2.5f" % self.GET_LASER_T()[0]
        except:
            tmp = "N/A"
        elements.append(StatusHelper.Line("Laser cell temperature", \
            [StatusHelper.ValueUnit(tmp, "C")]))

        try:
            tmp = "%2.5f" % (self.GET_LASER_PWR()[0] * 1E3)
        except:
            tmp = "N/A"
        elements.append(StatusHelper.Line("Laser power", \
            [StatusHelper.ValueUnit(tmp, "mW")]))

        statusFrame = StatusHelper.Frame("Master Laser", elements)
        statusFrame.printScreen()

    def clearErrors(self):
        '''
        Clear errors.  This function clears some latched error flags. 
        It is executed during activateLockingSequence.
        '''
        self.SET_CLEAR_WARNING_LED()
        self.SET_RESET_LASER_REFL_ALARM()
        self.SET_LOCK_MONINTORING_RESET_TRIGGER()

    def activateLockingSequence(self):
        '''
        Restart the automatic locking procedure.
        '''
        self.clearErrors()
        self.SET_STANDBY_MODE_REQUEST()
        
