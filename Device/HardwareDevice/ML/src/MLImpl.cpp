// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
// @(#) $Id$


#include <MLImpl.h>
#include <string> // for string
#include <loggingMACROS.h> // for AUTO_TRACE
// For audience logs.
#include <sstream>
#include <iomanip>
#include <LogToAudience.h>

// Alarm system stuff
#include <vector>
#include <controlAlarmSender.h>

#include <acstime.h>
#include <cmath>


MLImpl::MLImpl(const ACE_CString& name, maci::ContainerServices* cs):
    ::MLBase(name, cs),
    myName(name.c_str()),
    convertToPowerTempMon_a(3.3540154e-3),
    convertToPowerTempMon_b(2.5627725E-4),
    convertToPowerTempMon_c(2.082921E-6),
    convertToPowerTempMon_d(7.3003206E-8),
    lockingStatusThread_p(0),
    alarmSender(0)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

MLImpl::~MLImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}


void MLImpl::initialiseAlarmSystem()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // alarmSender is expected to be = 0 since the instanciation of this
    // class.  Therefore do not check if it has been set before but just get a
    // new instance of the AlarmSender.
    alarmSender = new Control::AlarmSender;

    std::vector< Control::AlarmInformation > alarmInformation;

    {
        Control::AlarmInformation ai;
        ai.alarmCode = MlIsNotLocked;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = MlIsNotInAutomaticStartupMode;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = MlDoesColdStart;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = NoMonitorThread;
        alarmInformation.push_back(ai);
    }

    alarmSender->initializeAlarms("ML", "CentralLO",
        alarmInformation);

    alarmSender->forceTerminateAllAlarms();
}

void MLImpl::cleanUpAlarmSystem()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(alarmSender != 0)
    {
        // Disable all alarms for 60s.  This allows to terminate all alarms.
        alarmSender->inhibitAlarms(static_cast< ACS::TimeInterval >(60 * 1e7));
        alarmSender->forceTerminateAllAlarms();
        alarmSender = 0;
    }
}


void MLImpl::execute()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        MLBase::execute();
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex,__FILE__,__LINE__,
            __PRETTY_FUNCTION__);
    }

    initialiseAlarmSystem();

    // Create the thread which monitors the locking status of the ML.
    MLImpl* selfPtr(this);
    lockingStatusThread_p = getContainerServices()->getThreadManager()->
        create< MlLockingStatusThread, MLImpl* >(
            std::string(myName + "MlLockingStatusThread").c_str(), selfPtr);

    // The thread shall run every 300s.
    if(lockingStatusThread_p != 0)
    {
        lockingStatusThread_p->setSleepTime(
            300ULL * TETimeUtil::ACS_ONE_SECOND);
    }
    else
    {
        alarmSender->activateAlarm(NoMonitorThread);

        const std::string msg("Failed to create the thread which monitors the "
            "locking status of the master laser.  Will continue without it.");
        LOG_TO_DEVELOPER(LM_DEBUG, msg);
    }
}

void MLImpl::cleanUp()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        Control::HardwareDeviceImpl::hwStop();
    }
    catch(...)
    {
        LOG_TO_OPERATOR(LM_WARNING, "Caught an exception during transition to "
            "the hardware stop state.  Continuing anyway.");
    }

    if(lockingStatusThread_p != 0)
    {
        lockingStatusThread_p->suspend();
        lockingStatusThread_p->terminate();
        lockingStatusThread_p = 0;
    }

    cleanUpAlarmSystem();

    try
    {
        MLBase::cleanUp();
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex,__FILE__,__LINE__,
            __PRETTY_FUNCTION__);
    }
}


void MLImpl::hwStartAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    MLBase::diagnosticModeEnabled = false;
    MLBase::hwStartAction();
}


void MLImpl::hwInitializeAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    ACS::Time timeStamp(0U);

    alarmSender->forceTerminateAllAlarms();

    try
    {
        // Check if laser was set to manual startup mode.  If so, trigger an
        // alarm.
        if(MLBase::getSystemStartupMode(timeStamp) != 0)
        {
            alarmSender->activateAlarm(MlIsNotInAutomaticStartupMode);
        }

        // Check if the startup mode of the ML == 6.
        // Yes: ML is in good condition, warmed up and ready for take off.
        // No: If the master laser does a cold start, then there is no reason
        // to check the auto locking state.
        if(MLBase::getSystemStartupState(timeStamp) != 6U)
        {
            // This means that the ML does a cold start and is not ready yet.
            // Trigger an alarm which informs the operator about the condition.
            alarmSender->activateAlarm(MlDoesColdStart);
        }
        else
        {
            checkLockingStatus();
        }
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Could not determine the ML startup state, the "
            "hardware startup state is not available during hwInitialize.");
        nex.log();
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Could not determine the ML startup state in "
            "hwInitialize. The ML hardware did not return the requested "
            "information.");
        nex.log();
    }

    MLBase::hwInitializeAction();
}


void MLImpl::hwOperationalAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    MLBase::hwOperationalAction();

    // Start the thread which monitors the ML locking.
    if(lockingStatusThread_p != 0)
    {
        lockingStatusThread_p->resume();
    }
}


void MLImpl::hwStopAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // Suspend the thread which monitors the locking.
    if(lockingStatusThread_p != 0)
    {
        lockingStatusThread_p->suspend();
    }

    if(alarmSender != 0)
    {
        alarmSender->forceTerminateAllAlarms();
    }

    MLBase::diagnosticModeEnabled = false;
    MLBase::hwStopAction();
}


void MLImpl::hwDiagnosticAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        MLBase::setCntlManualModeRequest(0U);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Could not switch to manual mode for "
            "diagnostics.");
            nex.log();

        }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Could not read the start up mode.");
        nex.log();
    }

    try
    {
        MLBase::diagnosticModeEnabled = true;
        MLBase::hwDiagnosticAction();
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
}


double MLImpl::convertToPowerTempMon(double value) const
{
    // Calculate a value in [0; 1].  Remember, value is uint16.
    const double scaledValue(value / (8192.0 - value));

    // Calculate the ln of the scaled value.
    const double lnOfScaledValue(std::log(scaledValue));

    // Calculate the factors which build up the approximation.
    const double factorOne(convertToPowerTempMon_b * lnOfScaledValue);
    const double factorTwo(
        convertToPowerTempMon_c * lnOfScaledValue * lnOfScaledValue);
    const double factorThree(convertToPowerTempMon_d *
        lnOfScaledValue * lnOfScaledValue * lnOfScaledValue);

    // Add the whole enchilada together.
    double tempInKelvin(convertToPowerTempMon_a + factorOne
        + factorTwo + factorThree);

    // The value in Kelvin is the reciprocal of the just calclulated thing.
    tempInKelvin = 1.0 / tempInKelvin;

    return tempInKelvin;
}

double MLImpl::rawToWorldSignalPowerModTempMon(short value) const
{
    return convertToPowerTempMon(value);
}

double MLImpl::rawToWorldSignalOptRefModTempMon(short value) const
{
    return convertToPowerTempMon(value);
}

double MLImpl::rawToWorldSignalLaserModTempMon(short value) const
{
    return convertToPowerTempMon(value);
}

double MLImpl::rawToWorldSignalInfraredPdPwrMon(
    unsigned short value) const
{
    // Get the responsivity first.
    // Unfortunately this will trigger a Monitor point read on the CAN-bus
    // since the value is not kept in a BACI property.
    //
    // getIrPdResponsivity is already a scaled value.
    ACS::Time timeStamp(0ULL);
    double power(MLBase::getIrPdResponsivity(timeStamp) * value);
    power *= (2.5 / 65535.0);

    return power;
}

double MLImpl::rawToWorldSignalRedPdPwrMon(unsigned short value) const
{
    // Get the responsivity first.
    // Unfortunately this will trigger a Monitor point read on the CAN-bus
    // since the value is not kept in a BACI property.
    //
    // getRedPdResponsivity is already a scaled value.
    ACS::Time timeStamp(0ULL);
    double power(MLImpl::getRedPdResponsivity(timeStamp) * value);
    power *= (2.5 / 65535.0);

    return power;
}

float MLImpl::rawToWorldLaserSignalFlPumpTemp(float value) const
{
    float newValue(2.048 / value);
    newValue -= 1.0;
    newValue = std::log(newValue);
    newValue *= 0.416;
    newValue /= 3965.0;
    newValue = (1.0 / 298.0) / newValue;
    newValue = 1.0 / newValue;

    return newValue;
}

float MLImpl::rawToWorldLaserSignalFlModuleNtcV(float value) const
{
    float newValue(4.1 / value);
    newValue -= 1.0;
    newValue = std::log(newValue);
    newValue /= 3965.0;
    newValue = (1.0 / 298.0) - newValue;
    newValue = 1.0 / newValue;

    return newValue;
}

double MLImpl::rawToWorldSignalInfoPidLaserCorrMonVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoPidLaserCorrMonBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoPidErrorMonVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoPidErrorMonBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoPmDc10vVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoPmDc10vBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoPiezoOutMonVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoPiezoOutMonBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoPiezoSumMonVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoPiezoSumMonBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoPowerModTempMonVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoPowerModTempMonBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoOptRefModTempMonVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoOptRefModTempMonBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoLaserModTempMonVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoLaserModTempMonBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoErrorPeakMonVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoErrorPeakMonBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoPmDcPeakMonVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoPmDcPeakMonBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoRinDcMonVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoRinDcMonBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoRinLaserPwrMonVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoRinLaserPwrMonBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoRinErrorMonVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoRinErrorMonBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoRinDcCorrMonVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoRinDcCorrMonBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoVrefVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoVrefBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoGndVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoGndBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoPplnTempMonVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoPplnTempMonBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoPplnPwrIMonVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoPplnPwrIMonBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoCellTempMonVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoCellTempMonBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoTipTempMonVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoTipTempMonBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoCellPwrIMonVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoCellPwrIMonBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoInfraredPdPwrMonVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoInfraredPdPwrMonBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoRedPdPwrMonVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoRedPdPwrMonBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoHvMonVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoHvMonBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoPmTempMonVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoPmTempMonBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoFlTempVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoFlTempBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoFlFastTempVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoFlFastTempBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoFlSlowTempVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoFlSlowTempBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoPplnFastTempVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoPplnFastTempBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoPplnSlowTempVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoPplnSlowTempBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoRbcellFastTempVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoRbcellFastTempBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoRbcellSlowTempVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoRbcellSlowTempBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoRbcellTipFastTempVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoRbcellTipFastTempBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoRbcellTipSlowTempVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoRbcellTipSlowTempBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoLockmonFastFluoVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoLockmonFastFluoBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldSignalInfoLockmonSlowFluoVoltageRange(
    unsigned char value) const
{
    double newValue(value / 32768.0 / 1000.0);
    // Check if the signal is unipolar (false) or bipolar (true).
    // Unfortunately this triggers another monitor request.
    // If it is bipolar, divide only by 32768.0, if unipolar then divide by
    // 65536.0.
    ACS::Time timeStamp(0ULL);
    if(MLBase::getSignalInfoLockmonSlowFluoBipolarSignal(timeStamp) == false)
    {
        newValue /= 2.0;
    }

    return newValue;
}

double MLImpl::rawToWorldLlAdcPowerModTempMon(short value) const
{
    return convertToPowerTempMon(value);
}

double MLImpl::rawToWorldLlAdcOptRefModTempMon(short value) const
{
    return convertToPowerTempMon(value);
}

double MLImpl::rawToWorldLlAdcLaserModTempMon(short value) const
{
    return convertToPowerTempMon(value);
}

double MLImpl::rawToWorldLlAdcInfraredPdPwrMon(
    unsigned short value) const
{
    // Get the responsivity first.
    // Unfortunately this will trigger a Monitor point read on the CAN-bus
    // since the value is not kept in a BACI property.
    //
    // getIrPdResponsivity is already a scaled value.
    ACS::Time timeStamp(0ULL);
    double power(MLBase::getIrPdResponsivity(timeStamp) * value);
    power *= (2.5 / 65535.0);

    return power;
}

double MLImpl::rawToWorldLlAdcRedPdPwrMon(unsigned short value) const
{
    // Get the responsivity first.
    // Unfortunately this will trigger a Monitor point read on the CAN-bus
    // since the value is not kept in a BACI property.
    //
    // getIrPdResponsivity is already a scaled value.
    ACS::Time timeStamp(0ULL);
    double power(MLBase::getRedPdResponsivity(timeStamp) * value);
    power *= (2.5 / 65535.0);

    return power;
}

double MLImpl::rawToWorldPplnEfficiencyControlLastpwr(
    unsigned short value) const
{
    // Get the responsivity first.
    // Unfortunately this will trigger a Monitor point read on the CAN-bus
    // since the value is not kept in a BACI property.
    //
    // getIrPdResponsivity is already a scaled value.
    ACS::Time timeStamp(0ULL);
    double power(MLBase::getRedPdResponsivity(timeStamp) * value);
    power *= (2.5 / 65535.0);

    return power;
}

double MLImpl::rawToWorldPplnEfficiencyControlPwrPlus(
    unsigned short value) const
{
    // Get the responsivity first.
    // Unfortunately this will trigger a Monitor point read on the CAN-bus
    // since the value is not kept in a BACI property.
    //
    // getIrPdResponsivity is already a scaled value.
    ACS::Time timeStamp(0ULL);
    double power(MLBase::getRedPdResponsivity(timeStamp) * value);
    power *= (2.5 / 65535.0);

    return power;
}

double MLImpl::rawToWorldPplnEfficiencyControlPwrMinus(
    unsigned short value) const
{
    // Get the responsivity first.
    // Unfortunately this will trigger a Monitor point read on the CAN-bus
    // since the value is not kept in a BACI property.
    //
    // getIrPdResponsivity is already a scaled value.
    ACS::Time timeStamp(0ULL);
    double power(MLBase::getRedPdResponsivity(timeStamp) * value);
    power *= (2.5 / 65535.0);

    return power;
}

double MLImpl::rawToWorldPplnOptMaxRedPowerDuringScan(
    unsigned char value) const
{
    // Get the responsivity first.
    // Unfortunately this will trigger a Monitor point read on the CAN-bus
    // since the value is not kept in a BACI property.
    //
    // getIrPdResponsivity is already a scaled value.
    ACS::Time timeStamp(0ULL);
    double power(MLBase::getRedPdResponsivity(timeStamp) * value);
    power *= (2.5 / 65535.0);

    return power;
}

void MLImpl::checkLockingStatus()
{
    ACS::Time timeStamp(0ULL);

    // Check if the master laser is locked.  If not, tell it do so.  I am
    // checking first the successful finalisation of the auto locking
    // algorithm.  Then I check if the general system state is Operational
    // which means the ML software made the transition from Automatic Lock
    // state to Operational state.
    // Cf. ICD, par. 3.1.1.1 System Modes Description.
    //
    if((MLBase::getAlState(timeStamp) != 8)
    || (MLBase::getSystemState(timeStamp) != 4))
    {
        if(alarmSender->isAlarmSet(MlIsNotLocked) == false)
        {
            alarmSender->activateAlarm(MlIsNotLocked);
        }
    }
    else
    {
        if(alarmSender->isAlarmSet(MlIsNotLocked) == true)
        {
            alarmSender->deactivateAlarm(MlIsNotLocked);
        }
    }
}


MlLockingStatusThread::MlLockingStatusThread(const ACE_CString& name,
    MLImpl* _ml):
    ACS::Thread(name),
    ml(_ml)
{
}

MlLockingStatusThread::~MlLockingStatusThread()
{
}

void MlLockingStatusThread::runLoop()
{
    ml->checkLockingStatus();
}


#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(MLImpl)
