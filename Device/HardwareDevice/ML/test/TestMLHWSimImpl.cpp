/**
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * WARNING! DO NOT MODIFY THIS FILE!
 *  ---------------------------------------------------------
 * | This is generated code!  Do not modify this file.       |
 * | Any changes will be lost when the file is re-generated. |
 *  ---------------------------------------------------------
 *
 * $Id$
 *
 */

#include <limits>
#include <vector>
#include <ambDefs.h>
#include <cppunit/extensions/HelperMacros.h>
#include <TypeConversion.h>
#include <MLHWSimBase.h>

/**
 * A test case for the MLHWSimBase class
 *
 */
class MLHWSimImplTestCase: public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE(MLHWSimImplTestCase);
    CPPUNIT_TEST(test_simulation);
    CPPUNIT_TEST_SUITE_END();

    public:
    void setUp();
    void tearDown();

    protected:
    void test_simulation();
    std::vector< CAN::byte_t > createVector(const AmbDataLength_t size);

    ///  testmonitorPoint_SYSTEM_STATUS
    void testmonitorPoint_SYSTEM_STATUS();

    ///  testmonitorPoint_SYSTEM_ERROR
    void testmonitorPoint_SYSTEM_ERROR();

    ///  testmonitorPoint_SYSTEM_WARNING
    void testmonitorPoint_SYSTEM_WARNING();

    ///  testmonitorPoint_SYSTEM_STARTUP_STATE
    void testmonitorPoint_SYSTEM_STARTUP_STATE();

    ///  testmonitorPoint_SIGNAL_PID_LASER_CORR_MON
    void testmonitorPoint_SIGNAL_PID_LASER_CORR_MON();

    ///  testmonitorPoint_SIGNAL_PID_ERROR_MON
    void testmonitorPoint_SIGNAL_PID_ERROR_MON();

    ///  testmonitorPoint_SIGNAL_PM_DC_10V
    void testmonitorPoint_SIGNAL_PM_DC_10V();

    ///  testmonitorPoint_SIGNAL_PIEZO_OUT_MON
    void testmonitorPoint_SIGNAL_PIEZO_OUT_MON();

    ///  testmonitorPoint_SIGNAL_PIEZO_SUM_MON
    void testmonitorPoint_SIGNAL_PIEZO_SUM_MON();

    ///  testmonitorPoint_SIGNAL_ERROR_PEAK_MON
    void testmonitorPoint_SIGNAL_ERROR_PEAK_MON();

    ///  testmonitorPoint_SIGNAL_PM_DC_PEAK_MON
    void testmonitorPoint_SIGNAL_PM_DC_PEAK_MON();

    ///  testmonitorPoint_SIGNAL_RIN_DC_MON
    void testmonitorPoint_SIGNAL_RIN_DC_MON();

    ///  testmonitorPoint_SIGNAL_RIN_LASER_PWR_MON
    void testmonitorPoint_SIGNAL_RIN_LASER_PWR_MON();

    ///  testmonitorPoint_SIGNAL_RIN_ERROR_MON
    void testmonitorPoint_SIGNAL_RIN_ERROR_MON();

    ///  testmonitorPoint_SIGNAL_RIN_DC_CORR_MON
    void testmonitorPoint_SIGNAL_RIN_DC_CORR_MON();

    ///  testmonitorPoint_SIGNAL_VREF
    void testmonitorPoint_SIGNAL_VREF();

    ///  testmonitorPoint_SIGNAL_GND
    void testmonitorPoint_SIGNAL_GND();

    ///  testmonitorPoint_SIGNAL_PPLN_TEMP_MON
    void testmonitorPoint_SIGNAL_PPLN_TEMP_MON();

    ///  testmonitorPoint_SIGNAL_PPLN_PWR_I_MON
    void testmonitorPoint_SIGNAL_PPLN_PWR_I_MON();

    ///  testmonitorPoint_SIGNAL_CELL_TEMP_MON
    void testmonitorPoint_SIGNAL_CELL_TEMP_MON();

    ///  testmonitorPoint_SIGNAL_TIP_TEMP_MON
    void testmonitorPoint_SIGNAL_TIP_TEMP_MON();

    ///  testmonitorPoint_SIGNAL_CELL_PWR_I_MON
    void testmonitorPoint_SIGNAL_CELL_PWR_I_MON();

    ///  testmonitorPoint_SIGNAL_HV_MON
    void testmonitorPoint_SIGNAL_HV_MON();

    ///  testmonitorPoint_SIGNAL_PM_TEMP_MON
    void testmonitorPoint_SIGNAL_PM_TEMP_MON();

    ///  testmonitorPoint_SIGNAL_FL_TEMP
    void testmonitorPoint_SIGNAL_FL_TEMP();

    ///  testmonitorPoint_SIGNAL_FL_FAST_TEMP
    void testmonitorPoint_SIGNAL_FL_FAST_TEMP();

    ///  testmonitorPoint_SIGNAL_FL_SLOW_TEMP
    void testmonitorPoint_SIGNAL_FL_SLOW_TEMP();

    ///  testmonitorPoint_SIGNAL_PPLN_FAST_TEMP
    void testmonitorPoint_SIGNAL_PPLN_FAST_TEMP();

    ///  testmonitorPoint_SIGNAL_PPLN_SLOW_TEMP
    void testmonitorPoint_SIGNAL_PPLN_SLOW_TEMP();

    ///  testmonitorPoint_SIGNAL_RBCELL_FAST_TEMP
    void testmonitorPoint_SIGNAL_RBCELL_FAST_TEMP();

    ///  testmonitorPoint_SIGNAL_RBCELL_SLOW_TEMP
    void testmonitorPoint_SIGNAL_RBCELL_SLOW_TEMP();

    ///  testmonitorPoint_SIGNAL_RBCELL_TIP_FAST_TEMP
    void testmonitorPoint_SIGNAL_RBCELL_TIP_FAST_TEMP();

    ///  testmonitorPoint_SIGNAL_RBCELL_TIP_SLOW_TEMP
    void testmonitorPoint_SIGNAL_RBCELL_TIP_SLOW_TEMP();

    ///  testmonitorPoint_SIGNAL_LOCKMON_FAST_FLUO
    void testmonitorPoint_SIGNAL_LOCKMON_FAST_FLUO();

    ///  testmonitorPoint_SIGNAL_LOCKMON_SLOW_FLUO
    void testmonitorPoint_SIGNAL_LOCKMON_SLOW_FLUO();

    ///  testmonitorPoint_LASER_SIGNAL_FL_TEMP_MON
    void testmonitorPoint_LASER_SIGNAL_FL_TEMP_MON();

    ///  testmonitorPoint_LASER_SIGNAL_FL_THERMV
    void testmonitorPoint_LASER_SIGNAL_FL_THERMV();

    ///  testmonitorPoint_LASER_SIGNAL_REF_2_048V
    void testmonitorPoint_LASER_SIGNAL_REF_2_048V();

    ///  testmonitorPoint_LASER_SIGNAL_FL_TEC_I
    void testmonitorPoint_LASER_SIGNAL_FL_TEC_I();

    ///  testmonitorPoint_LASER_SIGNAL_FL_OUTPUT_PWR
    void testmonitorPoint_LASER_SIGNAL_FL_OUTPUT_PWR();

    ///  testmonitorPoint_LASER_SIGNAL_FL_PUMP_I
    void testmonitorPoint_LASER_SIGNAL_FL_PUMP_I();

    ///  testmonitorPoint_LASER_SIGNAL_FL_PUMP_I_MON
    void testmonitorPoint_LASER_SIGNAL_FL_PUMP_I_MON();

    ///  testmonitorPoint_LASER_SIGNAL_FL_PUMP_TEC_I
    void testmonitorPoint_LASER_SIGNAL_FL_PUMP_TEC_I();

    ///  testmonitorPoint_LASER_SIGNAL_PWRAMP_INPUT_PWR
    void testmonitorPoint_LASER_SIGNAL_PWRAMP_INPUT_PWR();

    ///  testmonitorPoint_LASER_SIGNAL_PWRAMP_REFL_PWR
    void testmonitorPoint_LASER_SIGNAL_PWRAMP_REFL_PWR();

    ///  testmonitorPoint_LASER_SIGNAL_MODULE_OUTPUT_PWR
    void testmonitorPoint_LASER_SIGNAL_MODULE_OUTPUT_PWR();

    ///  testmonitorPoint_LASER_SIGNAL_PWRAMP_PUMP_I
    void testmonitorPoint_LASER_SIGNAL_PWRAMP_PUMP_I();

    ///  testmonitorPoint_LASER_SIGNAL_FL_TEMP_SETP
    void testmonitorPoint_LASER_SIGNAL_FL_TEMP_SETP();

    ///  testmonitorPoint_LASER_SIGNAL_MODULE_OUTPUT_PWR_SETP
    void testmonitorPoint_LASER_SIGNAL_MODULE_OUTPUT_PWR_SETP();

    ///  testmonitorPoint_LL_ADC_PID_LASER_CORR_MON
    void testmonitorPoint_LL_ADC_PID_LASER_CORR_MON();

    ///  testmonitorPoint_LL_ADC_PID_ERROR_MON
    void testmonitorPoint_LL_ADC_PID_ERROR_MON();

    ///  testmonitorPoint_LL_ADC_PM_DC_10V
    void testmonitorPoint_LL_ADC_PM_DC_10V();

    ///  testmonitorPoint_LL_ADC_PIEZO_OUT_MON
    void testmonitorPoint_LL_ADC_PIEZO_OUT_MON();

    ///  testmonitorPoint_LL_ADC_PIEZO_SUM_MON
    void testmonitorPoint_LL_ADC_PIEZO_SUM_MON();

    ///  testmonitorPoint_LL_ADC_ERROR_PEAK_MON
    void testmonitorPoint_LL_ADC_ERROR_PEAK_MON();

    ///  testmonitorPoint_LL_ADC_PM_DC_PEAK_MON
    void testmonitorPoint_LL_ADC_PM_DC_PEAK_MON();

    ///  testmonitorPoint_LL_ADC_RIN_DC_MON
    void testmonitorPoint_LL_ADC_RIN_DC_MON();

    ///  testmonitorPoint_LL_ADC_RIN_LASER_PWR_MON
    void testmonitorPoint_LL_ADC_RIN_LASER_PWR_MON();

    ///  testmonitorPoint_LL_ADC_RIN_ERROR_MON
    void testmonitorPoint_LL_ADC_RIN_ERROR_MON();

    ///  testmonitorPoint_LL_ADC_RIN_DC_CORR_MON
    void testmonitorPoint_LL_ADC_RIN_DC_CORR_MON();

    ///  testmonitorPoint_LL_ADC_VREF
    void testmonitorPoint_LL_ADC_VREF();

    ///  testmonitorPoint_LL_ADC_GND
    void testmonitorPoint_LL_ADC_GND();

    ///  testmonitorPoint_LL_ADC_PPLN_TEMP_MON
    void testmonitorPoint_LL_ADC_PPLN_TEMP_MON();

    ///  testmonitorPoint_LL_ADC_PPLN_PWR_I_MON
    void testmonitorPoint_LL_ADC_PPLN_PWR_I_MON();

    ///  testmonitorPoint_LL_ADC_CELL_TEMP_MON
    void testmonitorPoint_LL_ADC_CELL_TEMP_MON();

    ///  testmonitorPoint_LL_ADC_TIP_TEMP_MON
    void testmonitorPoint_LL_ADC_TIP_TEMP_MON();

    ///  testmonitorPoint_LL_ADC_CELL_PWR_I_MON
    void testmonitorPoint_LL_ADC_CELL_PWR_I_MON();

    ///  testmonitorPoint_LL_ADC_HV_MON
    void testmonitorPoint_LL_ADC_HV_MON();

    ///  testmonitorPoint_LL_ADC_PM_TEMP_MON
    void testmonitorPoint_LL_ADC_PM_TEMP_MON();

    ///  testmonitorPoint_AL_OPTIMISTIC_CUR_SCAN_RANGE
    void testmonitorPoint_AL_OPTIMISTIC_CUR_SCAN_RANGE();

    ///  testmonitorPoint_AL_ALREADY_SCANNED_RANGE_MIN
    void testmonitorPoint_AL_ALREADY_SCANNED_RANGE_MIN();

    ///  testmonitorPoint_AL_ALREADY_SCANNED_RANGE_MAX
    void testmonitorPoint_AL_ALREADY_SCANNED_RANGE_MAX();

    ///  testmonitorPoint_AL_STATE
    void testmonitorPoint_AL_STATE();

    ///  testmonitorPoint_AL_NB_GROUPS
    void testmonitorPoint_AL_NB_GROUPS();

    ///  testmonitorPoint_AL_ERROR_AMPLITUDE
    void testmonitorPoint_AL_ERROR_AMPLITUDE();

    ///  testmonitorPoint_AL_IDENTIFICATION_VALID_RIGHT_GROUP
    void testmonitorPoint_AL_IDENTIFICATION_VALID_RIGHT_GROUP();

    ///  testmonitorPoint_AL_IDENTIFICATION_VALID_WRONG_GROUP
    void testmonitorPoint_AL_IDENTIFICATION_VALID_WRONG_GROUP();

    ///  testmonitorPoint_AL_IDENTIFICATION_INVALID_GROUP
    void testmonitorPoint_AL_IDENTIFICATION_INVALID_GROUP();

    ///  testmonitorPoint_ORM_INTERLOCK_CLOSED
    void testmonitorPoint_ORM_INTERLOCK_CLOSED();

    ///  testmonitorPoint_PZT_RANGE_CONTROL_STATE
    void testmonitorPoint_PZT_RANGE_CONTROL_STATE();

    ///  testmonitorPoint_PPLN_EFFICIENCY_CONTROL_ACTIVE
    void testmonitorPoint_PPLN_EFFICIENCY_CONTROL_ACTIVE();

    ///  testmonitorPoint_PPLN_STATE
    void testmonitorPoint_PPLN_STATE();

    ///  testmonitorPoint_PEAKS_STATE
    void testmonitorPoint_PEAKS_STATE();

    ///  testmonitorPoint_PEAKS_THRESHOLD_LEVEL
    void testmonitorPoint_PEAKS_THRESHOLD_LEVEL();

    ///  testmonitorPoint_PEAKS_NEW_DETECTION_AVAILABLE
    void testmonitorPoint_PEAKS_NEW_DETECTION_AVAILABLE();

    ///  testmonitorPoint_PEAKS_NB_PEAKS
    void testmonitorPoint_PEAKS_NB_PEAKS();

    ///  testmonitorPoint_PEAKS_SIGNAL_VALIDATION
    void testmonitorPoint_PEAKS_SIGNAL_VALIDATION();

    ///  testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_ZERO
    void testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_ZERO();

    ///  testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_ONE
    void testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_ONE();

    ///  testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_TWO
    void testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_TWO();

    ///  testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_THREE
    void testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_THREE();

    ///  testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_FOUR
    void testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_FOUR();

    ///  testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_FIVE
    void testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_FIVE();

    ///  testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_SIX
    void testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_SIX();

    ///  testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_SEVEN
    void testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_SEVEN();

    ///  testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_EIGHT
    void testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_EIGHT();

    ///  testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_NINE
    void testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_NINE();

    ///  testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_ZERO
    void testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_ZERO();

    ///  testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_ONE
    void testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_ONE();

    ///  testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_TWO
    void testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_TWO();

    ///  testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_THREE
    void testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_THREE();

    ///  testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_FOUR
    void testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_FOUR();

    ///  testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_FIVE
    void testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_FIVE();

    ///  testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_SIX
    void testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_SIX();

    ///  testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_SEVEN
    void testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_SEVEN();

    ///  testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_EIGHT
    void testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_EIGHT();

    ///  testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_NINE
    void testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_NINE();

    ///  testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_ZERO
    void testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_ZERO();

    ///  testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_ONE
    void testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_ONE();

    ///  testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_TWO
    void testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_TWO();

    ///  testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_THREE
    void testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_THREE();

    ///  testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_FOUR
    void testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_FOUR();

    ///  testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_FIVE
    void testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_FIVE();

    ///  testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_SIX
    void testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_SIX();

    ///  testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_SEVEN
    void testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_SEVEN();

    ///  testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_EIGHT
    void testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_EIGHT();

    ///  testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_NINE
    void testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_NINE();

    ///  testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_ZERO
    void testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_ZERO();

    ///  testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_ONE
    void testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_ONE();

    ///  testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_TWO
    void testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_TWO();

    ///  testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_THREE
    void testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_THREE();

    ///  testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_FOUR
    void testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_FOUR();

    ///  testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_FIVE
    void testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_FIVE();

    ///  testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_SIX
    void testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_SIX();

    ///  testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_SEVEN
    void testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_SEVEN();

    ///  testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_EIGHT
    void testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_EIGHT();

    ///  testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_NINE
    void testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_NINE();

    ///  testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_ZERO
    void testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_ZERO();

    ///  testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_ONE
    void testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_ONE();

    ///  testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_TWO
    void testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_TWO();

    ///  testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_THREE
    void testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_THREE();

    ///  testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_FOUR
    void testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_FOUR();

    ///  testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_FIVE
    void testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_FIVE();

    ///  testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_SIX
    void testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_SIX();

    ///  testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_SEVEN
    void testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_SEVEN();

    ///  testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_EIGHT
    void testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_EIGHT();

    ///  testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_NINE
    void testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_NINE();

    ///  testmonitorPoint_SCAN_DELTA_T_STEPS
    void testmonitorPoint_SCAN_DELTA_T_STEPS();

    ///  testmonitorPoint_SCAN_STATE
    void testmonitorPoint_SCAN_STATE();

    ///  testmonitorPoint_SYSTEM_STARTUP_MODE
    void testmonitorPoint_SYSTEM_STARTUP_MODE();

    ///  testmonitorPoint_SYSTEM_STARTUP_PARAM_SET
    void testmonitorPoint_SYSTEM_STARTUP_PARAM_SET();

    ///  testmonitorPoint_LL_DAC_PIEZO_SETP
    void testmonitorPoint_LL_DAC_PIEZO_SETP();

    ///  testmonitorPoint_LL_DAC_RAMP_TRIG_SETP
    void testmonitorPoint_LL_DAC_RAMP_TRIG_SETP();

    ///  testmonitorPoint_LL_DAC_RAMP_SLOPE
    void testmonitorPoint_LL_DAC_RAMP_SLOPE();

    ///  testmonitorPoint_LL_DAC_PID_P_GAIN_SETP
    void testmonitorPoint_LL_DAC_PID_P_GAIN_SETP();

    ///  testmonitorPoint_LL_DAC_PID_OFFSET_CORR
    void testmonitorPoint_LL_DAC_PID_OFFSET_CORR();

    ///  testmonitorPoint_LL_DAC_TRIG_ERROR_SETP
    void testmonitorPoint_LL_DAC_TRIG_ERROR_SETP();

    ///  testmonitorPoint_LL_DAC_RIN_LOCK_SETP
    void testmonitorPoint_LL_DAC_RIN_LOCK_SETP();

    ///  testmonitorPoint_LL_DAC_RIN_DC_OFFSET_P
    void testmonitorPoint_LL_DAC_RIN_DC_OFFSET_P();

    ///  testmonitorPoint_LL_DIGIPOT_DDS_MOD_ADJ
    void testmonitorPoint_LL_DIGIPOT_DDS_MOD_ADJ();

    ///  testmonitorPoint_LL_DIGIPOT_DDS_LO_ADJ
    void testmonitorPoint_LL_DIGIPOT_DDS_LO_ADJ();

    ///  testmonitorPoint_LL_DIGIPOT_RIN_POWER_MON_GAIN
    void testmonitorPoint_LL_DIGIPOT_RIN_POWER_MON_GAIN();

    ///  testmonitorPoint_LL_DIGIPOT_RIN_REFERENCE_MON_GAIN
    void testmonitorPoint_LL_DIGIPOT_RIN_REFERENCE_MON_GAIN();

    ///  testmonitorPoint_LL_DIGIPOT_ORM_PPLN_GAIN
    void testmonitorPoint_LL_DIGIPOT_ORM_PPLN_GAIN();

    ///  testmonitorPoint_LL_DIGIPOT_ORM_CELL_GAIN
    void testmonitorPoint_LL_DIGIPOT_ORM_CELL_GAIN();

    ///  testmonitorPoint_LL_DDS_OSCILLATOR_LOCAL_OSCILLATOR
    void testmonitorPoint_LL_DDS_OSCILLATOR_LOCAL_OSCILLATOR();

    ///  testmonitorPoint_LL_DDS_OSCILLATOR_PHASE_MODULATOR
    void testmonitorPoint_LL_DDS_OSCILLATOR_PHASE_MODULATOR();

    ///  testmonitorPoint_LASER_PZT_TUNING_COEFF
    void testmonitorPoint_LASER_PZT_TUNING_COEFF();

    ///  testmonitorPoint_LASER_T_TUNING_COEFF
    void testmonitorPoint_LASER_T_TUNING_COEFF();

    ///  testmonitorPoint_LASER_T
    void testmonitorPoint_LASER_T();

    ///  testmonitorPoint_LASER_PWR
    void testmonitorPoint_LASER_PWR();

    ///  testmonitorPoint_LASER_PZT_LEVEL
    void testmonitorPoint_LASER_PZT_LEVEL();

    ///  testmonitorPoint_LASER_PWRAMP_ENABLE
    void testmonitorPoint_LASER_PWRAMP_ENABLE();

    ///  testmonitorPoint_LASER_TEMP_CTRL_ENABLE
    void testmonitorPoint_LASER_TEMP_CTRL_ENABLE();

    ///  testmonitorPoint_LASER_TEMPMON_ABS_ERR
    void testmonitorPoint_LASER_TEMPMON_ABS_ERR();

    ///  testmonitorPoint_LASER_TEMPMON_TUNNEL
    void testmonitorPoint_LASER_TEMPMON_TUNNEL();

    ///  testmonitorPoint_LASER_TEMPMON_STABLE_TIME
    void testmonitorPoint_LASER_TEMPMON_STABLE_TIME();

    ///  testmonitorPoint_LASER_TEMPMON_TIMEOUT
    void testmonitorPoint_LASER_TEMPMON_TIMEOUT();

    ///  testmonitorPoint_LASER_TEMPMON_SLOW_FILTA
    void testmonitorPoint_LASER_TEMPMON_SLOW_FILTA();

    ///  testmonitorPoint_LASER_TEMPMON_FAST_FILTA
    void testmonitorPoint_LASER_TEMPMON_FAST_FILTA();

    ///  testmonitorPoint_LASER_TEMPMON_ENABLE
    void testmonitorPoint_LASER_TEMPMON_ENABLE();

    ///  testmonitorPoint_LASER_POWER_MON_TOLERANCE
    void testmonitorPoint_LASER_POWER_MON_TOLERANCE();

    ///  testmonitorPoint_PZT_SWEEP_START_VOLTAGE
    void testmonitorPoint_PZT_SWEEP_START_VOLTAGE();

    ///  testmonitorPoint_PZT_SWEEP_STOP_VOLTAGE
    void testmonitorPoint_PZT_SWEEP_STOP_VOLTAGE();

    ///  testmonitorPoint_PZT_SWEEP_DELAY
    void testmonitorPoint_PZT_SWEEP_DELAY();

    ///  testmonitorPoint_PZT_SWEEP_PERIOD
    void testmonitorPoint_PZT_SWEEP_PERIOD();

    ///  testmonitorPoint_PZT_SWEEP_PERIODIC
    void testmonitorPoint_PZT_SWEEP_PERIODIC();

    ///  testmonitorPoint_PZT_SWEEP_FILTER
    void testmonitorPoint_PZT_SWEEP_FILTER();

    ///  testmonitorPoint_AL_TEMP_MIN
    void testmonitorPoint_AL_TEMP_MIN();

    ///  testmonitorPoint_AL_TEMP_MAX
    void testmonitorPoint_AL_TEMP_MAX();

    ///  testmonitorPoint_AL_START_TEMP
    void testmonitorPoint_AL_START_TEMP();

    ///  testmonitorPoint_AL_OPTIMISTIC_SCAN_RANGE
    void testmonitorPoint_AL_OPTIMISTIC_SCAN_RANGE();

    ///  testmonitorPoint_AL_OPTIMISTIC_LAMBDA_OVERLAP
    void testmonitorPoint_AL_OPTIMISTIC_LAMBDA_OVERLAP();

    ///  testmonitorPoint_AL_MODE
    void testmonitorPoint_AL_MODE();

    ///  testmonitorPoint_AL_DETECTION_P_GAIN
    void testmonitorPoint_AL_DETECTION_P_GAIN();

    ///  testmonitorPoint_AL_LAST_LOCK_TEMPERATURE
    void testmonitorPoint_AL_LAST_LOCK_TEMPERATURE();

    ///  testmonitorPoint_LOCK_PROPORTIONAL_ENABLE
    void testmonitorPoint_LOCK_PROPORTIONAL_ENABLE();

    ///  testmonitorPoint_LOCK_INTEGRATOR_ENABLE
    void testmonitorPoint_LOCK_INTEGRATOR_ENABLE();

    ///  testmonitorPoint_LOCKMON_TOLERANCE
    void testmonitorPoint_LOCKMON_TOLERANCE();

    ///  testmonitorPoint_LOCKMON_TUNNEL
    void testmonitorPoint_LOCKMON_TUNNEL();

    ///  testmonitorPoint_LOCKMON_SLOW_FILTA
    void testmonitorPoint_LOCKMON_SLOW_FILTA();

    ///  testmonitorPoint_LOCKMON_FAST_FILTA
    void testmonitorPoint_LOCKMON_FAST_FILTA();

    ///  testmonitorPoint_LOCKMON_ENABLE
    void testmonitorPoint_LOCKMON_ENABLE();

    ///  testmonitorPoint_LOCKMON_UNLOCK_DETECT_THRESH
    void testmonitorPoint_LOCKMON_UNLOCK_DETECT_THRESH();

    ///  testmonitorPoint_LOCKMON_NOMINAL_FLUO_LEVEL
    void testmonitorPoint_LOCKMON_NOMINAL_FLUO_LEVEL();

    ///  testmonitorPoint_PM_SUPPLY_VOLTAGE
    void testmonitorPoint_PM_SUPPLY_VOLTAGE();

    ///  testmonitorPoint_PM_SUPPLY_ENABLE
    void testmonitorPoint_PM_SUPPLY_ENABLE();

    ///  testmonitorPoint_PM_AC_GAIN
    void testmonitorPoint_PM_AC_GAIN();

    ///  testmonitorPoint_PM_DC_GAIN
    void testmonitorPoint_PM_DC_GAIN();

    ///  testmonitorPoint_RED_PD_RESPONSIVITY
    void testmonitorPoint_RED_PD_RESPONSIVITY();

    ///  testmonitorPoint_IR_PD_RESPONSIVITY
    void testmonitorPoint_IR_PD_RESPONSIVITY();

    ///  testmonitorPoint_INTERLOCK_BYPASS_ENABLE
    void testmonitorPoint_INTERLOCK_BYPASS_ENABLE();

    ///  testmonitorPoint_PZT_RANGE_CONTROL_ENABLE
    void testmonitorPoint_PZT_RANGE_CONTROL_ENABLE();

    ///  testmonitorPoint_PZT_RANGE_CONTROL_V_MIN
    void testmonitorPoint_PZT_RANGE_CONTROL_V_MIN();

    ///  testmonitorPoint_PZT_RANGE_CONTROL_V_MAX
    void testmonitorPoint_PZT_RANGE_CONTROL_V_MAX();

    ///  testmonitorPoint_RB_CELL_TEMP_SETP
    void testmonitorPoint_RB_CELL_TEMP_SETP();

    ///  testmonitorPoint_RB_CELL_TEMP_CTRL_ENABLE
    void testmonitorPoint_RB_CELL_TEMP_CTRL_ENABLE();

    ///  testmonitorPoint_RB_CELL_TEMPMON_ABS_ERR
    void testmonitorPoint_RB_CELL_TEMPMON_ABS_ERR();

    ///  testmonitorPoint_RB_CELL_TIP_TEMPMON_ABS_ERR
    void testmonitorPoint_RB_CELL_TIP_TEMPMON_ABS_ERR();

    ///  testmonitorPoint_RB_CELL_TEMPMON_TUNNEL
    void testmonitorPoint_RB_CELL_TEMPMON_TUNNEL();

    ///  testmonitorPoint_RB_CELL_TEMPMON_STABLE_TIME
    void testmonitorPoint_RB_CELL_TEMPMON_STABLE_TIME();

    ///  testmonitorPoint_RB_CELL_TEMPMON_SLOW_FILTA
    void testmonitorPoint_RB_CELL_TEMPMON_SLOW_FILTA();

    ///  testmonitorPoint_RB_CELL_TEMPMON_FAST_FILTA
    void testmonitorPoint_RB_CELL_TEMPMON_FAST_FILTA();

    ///  testmonitorPoint_RB_CELL_TEMPMON_TIMEOUT
    void testmonitorPoint_RB_CELL_TEMPMON_TIMEOUT();

    ///  testmonitorPoint_RB_CELL_TEMPMON_ENABLE
    void testmonitorPoint_RB_CELL_TEMPMON_ENABLE();

    ///  testmonitorPoint_PPLN_TEMP_SETP
    void testmonitorPoint_PPLN_TEMP_SETP();

    ///  testmonitorPoint_PPLN_TEMP_CTRL_ENABLE
    void testmonitorPoint_PPLN_TEMP_CTRL_ENABLE();

    ///  testmonitorPoint_PPLN_TEMPMON_ABS_ERR
    void testmonitorPoint_PPLN_TEMPMON_ABS_ERR();

    ///  testmonitorPoint_PPLN_TEMPMON_TUNNEL
    void testmonitorPoint_PPLN_TEMPMON_TUNNEL();

    ///  testmonitorPoint_PPLN_TEMPMON_STABLE_TIME
    void testmonitorPoint_PPLN_TEMPMON_STABLE_TIME();

    ///  testmonitorPoint_PPLN_TEMPMON_SLOW_FILTA
    void testmonitorPoint_PPLN_TEMPMON_SLOW_FILTA();

    ///  testmonitorPoint_PPLN_TEMPMON_FAST_FILTA
    void testmonitorPoint_PPLN_TEMPMON_FAST_FILTA();

    ///  testmonitorPoint_PPLN_TEMPMON_TIMEOUT
    void testmonitorPoint_PPLN_TEMPMON_TIMEOUT();

    ///  testmonitorPoint_PPLN_TEMPMON_ENABLE
    void testmonitorPoint_PPLN_TEMPMON_ENABLE();

    ///  testmonitorPoint_PPLN_EFFICIENCY_CTRL_ENABLE
    void testmonitorPoint_PPLN_EFFICIENCY_CTRL_ENABLE();

    ///  testmonitorPoint_PPLN_EFFICIENCY_CTRL_STEP
    void testmonitorPoint_PPLN_EFFICIENCY_CTRL_STEP();

    ///  testmonitorPoint_PPLN_OPT_START_TEMP
    void testmonitorPoint_PPLN_OPT_START_TEMP();

    ///  testmonitorPoint_PPLN_OPT_STOP_TEMP
    void testmonitorPoint_PPLN_OPT_STOP_TEMP();

    ///  testmonitorPoint_PPLN_OPT_TEMP_STEP
    void testmonitorPoint_PPLN_OPT_TEMP_STEP();

    ///  testmonitorPoint_PPLN_REDPWR_MON_THRESHOLD
    void testmonitorPoint_PPLN_REDPWR_MON_THRESHOLD();

    ///  testmonitorPoint_PPLN_MINIMUM_IR_POWER
    void testmonitorPoint_PPLN_MINIMUM_IR_POWER();

    ///  testmonitorPoint_PPLN_MINIMUM_RED_POWER
    void testmonitorPoint_PPLN_MINIMUM_RED_POWER();

    ///  testmonitorPoint_PEAKS_MIN_THRESHOLD
    void testmonitorPoint_PEAKS_MIN_THRESHOLD();

    ///  testmonitorPoint_PEAKS_MAX_THRESHOLD
    void testmonitorPoint_PEAKS_MAX_THRESHOLD();

    ///  testmonitorPoint_PEAKS_THRESHOLD_INCREMENT
    void testmonitorPoint_PEAKS_THRESHOLD_INCREMENT();

    ///  testmonitorPoint_PEAKS_MIN_FLUO_LEVEL
    void testmonitorPoint_PEAKS_MIN_FLUO_LEVEL();

    ///  testmonitorPoint_SCAN_START_TEMP
    void testmonitorPoint_SCAN_START_TEMP();

    ///  testmonitorPoint_SCAN_STOP_TEMP
    void testmonitorPoint_SCAN_STOP_TEMP();

    ///  testmonitorPoint_SCAN_PZT_START_VOLTAGE
    void testmonitorPoint_SCAN_PZT_START_VOLTAGE();

    ///  testmonitorPoint_SCAN_PZT_STOP_VOLTAGE
    void testmonitorPoint_SCAN_PZT_STOP_VOLTAGE();

    ///  testmonitorPoint_SCAN_PZT_DELAY
    void testmonitorPoint_SCAN_PZT_DELAY();

    ///  testmonitorPoint_SCAN_PZT_PERIOD
    void testmonitorPoint_SCAN_PZT_PERIOD();

    ///  testmonitorPoint_SCAN_DELTA_LAMBDA_OVERLAP
    void testmonitorPoint_SCAN_DELTA_LAMBDA_OVERLAP();

    ///  testSET_SAVE_ALL_PARAMS
    void testSET_SAVE_ALL_PARAMS();

    ///  testSET_SAVE_PARAM_BY_ID
    void testSET_SAVE_PARAM_BY_ID();

    ///  testSET_LOAD_ALL_PARAMS
    void testSET_LOAD_ALL_PARAMS();

    ///  testSET_LOAD_PARAM_BY_ID
    void testSET_LOAD_PARAM_BY_ID();

    ///  testSET_RESET_LASER_REFL_ALARM
    void testSET_RESET_LASER_REFL_ALARM();

    ///  testSET_PZT_SWEEP_START
    void testSET_PZT_SWEEP_START();

    ///  testSET_PZT_SWEEP_STOP
    void testSET_PZT_SWEEP_STOP();

    ///  testSET_PZT_SWEEP_RESET
    void testSET_PZT_SWEEP_RESET();

    ///  testSET_ORM_SAVE_PARAMS
    void testSET_ORM_SAVE_PARAMS();

    ///  testSET_ORM_LOAD_PARAMS
    void testSET_ORM_LOAD_PARAMS();

    ///  testSET_MANUAL_MODE_REQUEST
    void testSET_MANUAL_MODE_REQUEST();

    ///  testSET_STANDBY_MODE_REQUEST
    void testSET_STANDBY_MODE_REQUEST();

    ///  testSET_AUTOLOCK_REQUEST
    void testSET_AUTOLOCK_REQUEST();

    ///  testSET_PPLN_OPTIMIZATION_SCAN_START
    void testSET_PPLN_OPTIMIZATION_SCAN_START();

    ///  testSET_PPLN_OPTIMIZATION_SCAN_STOP
    void testSET_PPLN_OPTIMIZATION_SCAN_STOP();

    ///  testSET_LOCK_MONITORING_RESET_TRIGGER
    void testSET_LOCK_MONITORING_RESET_TRIGGER();

    ///  testSET_CLEAR_WARNING_LED
    void testSET_CLEAR_WARNING_LED();

    ///  testSET_PPLN_EFFICIENCY_CTRL_START
    void testSET_PPLN_EFFICIENCY_CTRL_START();

    ///  testSET_PPLN_EFFICIENCY_CTRL_STOP
    void testSET_PPLN_EFFICIENCY_CTRL_STOP();

    ///  testSET_SYSTEM_STICKY_ERROR_STATUS
    void testSET_SYSTEM_STICKY_ERROR_STATUS();

    ///  testSET_SYSTEM_LM_SERIAL_NUMBER
    void testSET_SYSTEM_LM_SERIAL_NUMBER();

    ///  testSET_SYSTEM_ORM_SERIAL_NUMBER
    void testSET_SYSTEM_ORM_SERIAL_NUMBER();

    ///  testSET_SYSTEM_STARTUP_MODE
    void testSET_SYSTEM_STARTUP_MODE();

    ///  testSET_SYSTEM_STARTUP_PARAM_SET
    void testSET_SYSTEM_STARTUP_PARAM_SET();

    ///  testSET_LL_GPIO
    void testSET_LL_GPIO();

    ///  testSET_LL_IOX
    void testSET_LL_IOX();

    ///  testSET_LL_ORM_IOX
    void testSET_LL_ORM_IOX();

    ///  testSET_LL_DAC_PIEZO_SETP
    void testSET_LL_DAC_PIEZO_SETP();

    ///  testSET_LL_DAC_RAMP_TRIG_SETP
    void testSET_LL_DAC_RAMP_TRIG_SETP();

    ///  testSET_LL_DAC_RAMP_SLOPE
    void testSET_LL_DAC_RAMP_SLOPE();

    ///  testSET_LL_DAC_PID_P_GAIN_SETP
    void testSET_LL_DAC_PID_P_GAIN_SETP();

    ///  testSET_LL_DAC_PID_OFFSET_CORR
    void testSET_LL_DAC_PID_OFFSET_CORR();

    ///  testSET_LL_DAC_TRIG_ERROR_SETP
    void testSET_LL_DAC_TRIG_ERROR_SETP();

    ///  testSET_LL_DAC_RIN_LOCK_SETP
    void testSET_LL_DAC_RIN_LOCK_SETP();

    ///  testSET_LL_DAC_RIN_DC_OFFSET_P
    void testSET_LL_DAC_RIN_DC_OFFSET_P();

    ///  testSET_LL_SELECTOR_ADC_MUX
    void testSET_LL_SELECTOR_ADC_MUX();

    ///  testSET_LL_SELECTOR_PID_TAU
    void testSET_LL_SELECTOR_PID_TAU();

    ///  testSET_LL_DIGIPOT_DDS_MOD_ADJ
    void testSET_LL_DIGIPOT_DDS_MOD_ADJ();

    ///  testSET_LL_DIGIPOT_DDS_LO_ADJ
    void testSET_LL_DIGIPOT_DDS_LO_ADJ();

    ///  testSET_LL_DIGIPOT_RIN_POWER_MON_GAIN
    void testSET_LL_DIGIPOT_RIN_POWER_MON_GAIN();

    ///  testSET_LL_DIGIPOT_RIN_REFERENCE_MON_GAIN
    void testSET_LL_DIGIPOT_RIN_REFERENCE_MON_GAIN();

    ///  testSET_LL_DIGIPOT_ORM_PPLN_GAIN
    void testSET_LL_DIGIPOT_ORM_PPLN_GAIN();

    ///  testSET_LL_DIGIPOT_ORM_CELL_GAIN
    void testSET_LL_DIGIPOT_ORM_CELL_GAIN();

    ///  testSET_LL_DDS_OSCILLATOR_LOCAL_OSCILLATOR
    void testSET_LL_DDS_OSCILLATOR_LOCAL_OSCILLATOR();

    ///  testSET_LL_DDS_OSCILLATOR_PHASE_MODULATOR
    void testSET_LL_DDS_OSCILLATOR_PHASE_MODULATOR();

    ///  testSET_LASER_PZT_TUNING_COEFF
    void testSET_LASER_PZT_TUNING_COEFF();

    ///  testSET_LASER_T_TUNING_COEFF
    void testSET_LASER_T_TUNING_COEFF();

    ///  testSET_LASER_T
    void testSET_LASER_T();

    ///  testSET_LASER_PWR
    void testSET_LASER_PWR();

    ///  testSET_LASER_PZT_LEVEL
    void testSET_LASER_PZT_LEVEL();

    ///  testSET_LASER_PWRAMP_ENABLE
    void testSET_LASER_PWRAMP_ENABLE();

    ///  testSET_LASER_TEMP_CTRL_ENABLE
    void testSET_LASER_TEMP_CTRL_ENABLE();

    ///  testSET_LASER_TEMPMON_ABS_ERR
    void testSET_LASER_TEMPMON_ABS_ERR();

    ///  testSET_LASER_TEMPMON_TUNNEL
    void testSET_LASER_TEMPMON_TUNNEL();

    ///  testSET_LASER_TEMPMON_STABLE_TIME
    void testSET_LASER_TEMPMON_STABLE_TIME();

    ///  testSET_LASER_TEMPMON_TIMEOUT
    void testSET_LASER_TEMPMON_TIMEOUT();

    ///  testSET_LASER_TEMPMON_SLOW_FILTA
    void testSET_LASER_TEMPMON_SLOW_FILTA();

    ///  testSET_LASER_TEMPMON_FAST_FILTA
    void testSET_LASER_TEMPMON_FAST_FILTA();

    ///  testSET_LASER_TEMPMON_ENABLE
    void testSET_LASER_TEMPMON_ENABLE();

    ///  testSET_LASER_POWER_MON_TOLERANCE
    void testSET_LASER_POWER_MON_TOLERANCE();

    ///  testSET_PZT_SWEEP_START_VOLTAGE
    void testSET_PZT_SWEEP_START_VOLTAGE();

    ///  testSET_PZT_SWEEP_STOP_VOLTAGE
    void testSET_PZT_SWEEP_STOP_VOLTAGE();

    ///  testSET_PZT_SWEEP_DELAY
    void testSET_PZT_SWEEP_DELAY();

    ///  testSET_PZT_SWEEP_PERIOD
    void testSET_PZT_SWEEP_PERIOD();

    ///  testSET_PZT_SWEEP_PERIODIC
    void testSET_PZT_SWEEP_PERIODIC();

    ///  testSET_PZT_SWEEP_FILTER
    void testSET_PZT_SWEEP_FILTER();

    ///  testSET_AL_TEMP_MIN
    void testSET_AL_TEMP_MIN();

    ///  testSET_AL_TEMP_MAX
    void testSET_AL_TEMP_MAX();

    ///  testSET_AL_START_TEMP
    void testSET_AL_START_TEMP();

    ///  testSET_AL_OPTIMISTIC_SCAN_RANGE
    void testSET_AL_OPTIMISTIC_SCAN_RANGE();

    ///  testSET_AL_OPTIMISTIC_LAMBDA_OVERLAP
    void testSET_AL_OPTIMISTIC_LAMBDA_OVERLAP();

    ///  testSET_AL_MODE
    void testSET_AL_MODE();

    ///  testSET_AL_DETECTION_P_GAIN
    void testSET_AL_DETECTION_P_GAIN();

    ///  testSET_AL_LAST_LOCK_TEMPERATURE
    void testSET_AL_LAST_LOCK_TEMPERATURE();

    ///  testSET_LOCK_PROPORTIONAL_ENABLE
    void testSET_LOCK_PROPORTIONAL_ENABLE();

    ///  testSET_LOCK_INTEGRATOR_ENABLE
    void testSET_LOCK_INTEGRATOR_ENABLE();

    ///  testSET_LOCKMON_TOLERANCE
    void testSET_LOCKMON_TOLERANCE();

    ///  testSET_LOCKMON_TUNNEL
    void testSET_LOCKMON_TUNNEL();

    ///  testSET_LOCKMON_SLOW_FILTA
    void testSET_LOCKMON_SLOW_FILTA();

    ///  testSET_LOCKMON_FAST_FILTA
    void testSET_LOCKMON_FAST_FILTA();

    ///  testSET_LOCKMON_ENABLE
    void testSET_LOCKMON_ENABLE();

    ///  testSET_LOCKMON_UNLOCK_DETECT_THRESH
    void testSET_LOCKMON_UNLOCK_DETECT_THRESH();

    ///  testSET_LOCKMON_NOMINAL_FLUO_LEVEL
    void testSET_LOCKMON_NOMINAL_FLUO_LEVEL();

    ///  testSET_PM_SUPPLY_VOLTAGE
    void testSET_PM_SUPPLY_VOLTAGE();

    ///  testSET_PM_SUPPLY_ENABLE
    void testSET_PM_SUPPLY_ENABLE();

    ///  testSET_PM_AC_GAIN
    void testSET_PM_AC_GAIN();

    ///  testSET_PM_DC_GAIN
    void testSET_PM_DC_GAIN();

    ///  testSET_RED_PD_RESPONSIVITY
    void testSET_RED_PD_RESPONSIVITY();

    ///  testSET_IR_PD_RESPONSIVITY
    void testSET_IR_PD_RESPONSIVITY();

    ///  testSET_INTERLOCK_BYPASS_ENABLE
    void testSET_INTERLOCK_BYPASS_ENABLE();

    ///  testSET_PZT_RANGE_CONTROL_ENABLE
    void testSET_PZT_RANGE_CONTROL_ENABLE();

    ///  testSET_PZT_RANGE_CONTROL_V_MIN
    void testSET_PZT_RANGE_CONTROL_V_MIN();

    ///  testSET_PZT_RANGE_CONTROL_V_MAX
    void testSET_PZT_RANGE_CONTROL_V_MAX();

    ///  testSET_RB_CELL_TEMP_SETP
    void testSET_RB_CELL_TEMP_SETP();

    ///  testSET_RB_CELL_TEMP_CTRL_ENABLE
    void testSET_RB_CELL_TEMP_CTRL_ENABLE();

    ///  testSET_RB_CELL_TEMPMON_ABS_ERR
    void testSET_RB_CELL_TEMPMON_ABS_ERR();

    ///  testSET_RB_CELL_TIP_TEMPMON_ABS_ERR
    void testSET_RB_CELL_TIP_TEMPMON_ABS_ERR();

    ///  testSET_RB_CELL_TEMPMON_TUNNEL
    void testSET_RB_CELL_TEMPMON_TUNNEL();

    ///  testSET_RB_CELL_TEMPMON_STABLE_TIME
    void testSET_RB_CELL_TEMPMON_STABLE_TIME();

    ///  testSET_RB_CELL_TEMPMON_SLOW_FILTA
    void testSET_RB_CELL_TEMPMON_SLOW_FILTA();

    ///  testSET_RB_CELL_TEMPMON_FAST_FILTA
    void testSET_RB_CELL_TEMPMON_FAST_FILTA();

    ///  testSET_RB_CELL_TEMPMON_TIMEOUT
    void testSET_RB_CELL_TEMPMON_TIMEOUT();

    ///  testSET_RB_CELL_TEMPMON_ENABLE
    void testSET_RB_CELL_TEMPMON_ENABLE();

    ///  testSET_PPLN_TEMP_SETP
    void testSET_PPLN_TEMP_SETP();

    ///  testSET_PPLN_TEMP_CTRL_ENABLE
    void testSET_PPLN_TEMP_CTRL_ENABLE();

    ///  testSET_PPLN_TEMPMON_ABS_ERR
    void testSET_PPLN_TEMPMON_ABS_ERR();

    ///  testSET_PPLN_TEMPMON_TUNNEL
    void testSET_PPLN_TEMPMON_TUNNEL();

    ///  testSET_PPLN_TEMPMON_STABLE_TIME
    void testSET_PPLN_TEMPMON_STABLE_TIME();

    ///  testSET_PPLN_TEMPMON_SLOW_FILTA
    void testSET_PPLN_TEMPMON_SLOW_FILTA();

    ///  testSET_PPLN_TEMPMON_FAST_FILTA
    void testSET_PPLN_TEMPMON_FAST_FILTA();

    ///  testSET_PPLN_TEMPMON_TIMEOUT
    void testSET_PPLN_TEMPMON_TIMEOUT();

    ///  testSET_PPLN_TEMPMON_ENABLE
    void testSET_PPLN_TEMPMON_ENABLE();

    ///  testSET_PPLN_EFFICIENCY_CTRL_ENABLE
    void testSET_PPLN_EFFICIENCY_CTRL_ENABLE();

    ///  testSET_PPLN_EFFICIENCY_CTRL_STEP
    void testSET_PPLN_EFFICIENCY_CTRL_STEP();

    ///  testSET_PPLN_OPT_START_TEMP
    void testSET_PPLN_OPT_START_TEMP();

    ///  testSET_PPLN_OPT_STOP_TEMP
    void testSET_PPLN_OPT_STOP_TEMP();

    ///  testSET_PPLN_OPT_TEMP_STEP
    void testSET_PPLN_OPT_TEMP_STEP();

    ///  testSET_PPLN_REDPWR_MON_THRESHOLD
    void testSET_PPLN_REDPWR_MON_THRESHOLD();

    ///  testSET_PPLN_MINIMUM_IR_POWER
    void testSET_PPLN_MINIMUM_IR_POWER();

    ///  testSET_PPLN_MINIMUM_RED_POWER
    void testSET_PPLN_MINIMUM_RED_POWER();

    ///  testSET_PEAKS_MIN_THRESHOLD
    void testSET_PEAKS_MIN_THRESHOLD();

    ///  testSET_PEAKS_MAX_THRESHOLD
    void testSET_PEAKS_MAX_THRESHOLD();

    ///  testSET_PEAKS_THRESHOLD_INCREMENT
    void testSET_PEAKS_THRESHOLD_INCREMENT();

    ///  testSET_PEAKS_MIN_FLUO_LEVEL
    void testSET_PEAKS_MIN_FLUO_LEVEL();

    ///  testSET_SCAN_START_TEMP
    void testSET_SCAN_START_TEMP();

    ///  testSET_SCAN_STOP_TEMP
    void testSET_SCAN_STOP_TEMP();

    ///  testSET_SCAN_PZT_START_VOLTAGE
    void testSET_SCAN_PZT_START_VOLTAGE();

    ///  testSET_SCAN_PZT_STOP_VOLTAGE
    void testSET_SCAN_PZT_STOP_VOLTAGE();

    ///  testSET_SCAN_PZT_DELAY
    void testSET_SCAN_PZT_DELAY();

    ///  testSET_SCAN_PZT_PERIOD
    void testSET_SCAN_PZT_PERIOD();

    ///  testSET_SCAN_DELTA_LAMBDA_OVERLAP
    void testSET_SCAN_DELTA_LAMBDA_OVERLAP();

    AMB::Device* sim_m;
};

CPPUNIT_TEST_SUITE_REGISTRATION(MLHWSimImplTestCase);

///  testmonitorPoint_SYSTEM_STATUS
void MLHWSimImplTestCase::testmonitorPoint_SYSTEM_STATUS()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01100U), AMB::MLHWSimBase::monitorPoint_SYSTEM_STATUS);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SYSTEM_STATUS + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned char raw(0U);

    double value(0);

    raw = static_cast< unsigned char >(value);
    AMB::TypeConversion::valueToData(original, raw, 1U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SYSTEM_STATUS + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SYSTEM_STATUS + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SYSTEM_ERROR
void MLHWSimImplTestCase::testmonitorPoint_SYSTEM_ERROR()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01101U), AMB::MLHWSimBase::monitorPoint_SYSTEM_ERROR);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SYSTEM_ERROR + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SYSTEM_ERROR + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SYSTEM_ERROR + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SYSTEM_WARNING
void MLHWSimImplTestCase::testmonitorPoint_SYSTEM_WARNING()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01102U), AMB::MLHWSimBase::monitorPoint_SYSTEM_WARNING);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SYSTEM_WARNING + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SYSTEM_WARNING + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SYSTEM_WARNING + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SYSTEM_STARTUP_STATE
void MLHWSimImplTestCase::testmonitorPoint_SYSTEM_STARTUP_STATE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01106U), AMB::MLHWSimBase::monitorPoint_SYSTEM_STARTUP_STATE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SYSTEM_STARTUP_STATE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SYSTEM_STARTUP_STATE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SYSTEM_STARTUP_STATE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_PID_LASER_CORR_MON
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_PID_LASER_CORR_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01500U), AMB::MLHWSimBase::monitorPoint_SIGNAL_PID_LASER_CORR_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_PID_LASER_CORR_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    short raw(0U);

    double value(0.0);

    // Convert world to raw.

    value /= 3.0499999999999999E-4;

    raw = static_cast< short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_PID_LASER_CORR_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_PID_LASER_CORR_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_PID_ERROR_MON
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_PID_ERROR_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01501U), AMB::MLHWSimBase::monitorPoint_SIGNAL_PID_ERROR_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_PID_ERROR_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    short raw(0U);

    double value(0.0);

    // Convert world to raw.

    value /= 3.0499999999999999E-4;

    raw = static_cast< short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_PID_ERROR_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_PID_ERROR_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_PM_DC_10V
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_PM_DC_10V()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01502U), AMB::MLHWSimBase::monitorPoint_SIGNAL_PM_DC_10V);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_PM_DC_10V + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    short raw(0U);

    double value(5.0);

    // Convert world to raw.

    value /= 3.0499999999999999E-4;

    raw = static_cast< short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_PM_DC_10V + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_PM_DC_10V + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_PIEZO_OUT_MON
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_PIEZO_OUT_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01503U), AMB::MLHWSimBase::monitorPoint_SIGNAL_PIEZO_OUT_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_PIEZO_OUT_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    short raw(0U);

    double value(42.5);

    // Convert world to raw.

    value /= 3.0500000000000002E-3;

    raw = static_cast< short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_PIEZO_OUT_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_PIEZO_OUT_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_PIEZO_SUM_MON
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_PIEZO_SUM_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01504U), AMB::MLHWSimBase::monitorPoint_SIGNAL_PIEZO_SUM_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_PIEZO_SUM_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    short raw(0U);

    double value(5.0);

    // Convert world to raw.

    value /= 3.0499999999999999E-4;

    raw = static_cast< short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_PIEZO_SUM_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_PIEZO_SUM_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_ERROR_PEAK_MON
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_ERROR_PEAK_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01508U), AMB::MLHWSimBase::monitorPoint_SIGNAL_ERROR_PEAK_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_ERROR_PEAK_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    short raw(0U);

    double value(0.0);

    // Convert world to raw.

    value /= 3.0499999999999999E-4;

    raw = static_cast< short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_ERROR_PEAK_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_ERROR_PEAK_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_PM_DC_PEAK_MON
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_PM_DC_PEAK_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01509U), AMB::MLHWSimBase::monitorPoint_SIGNAL_PM_DC_PEAK_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_PM_DC_PEAK_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    short raw(0U);

    double value(5.0);

    // Convert world to raw.

    value /= 3.0499999999999999E-4;

    raw = static_cast< short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_PM_DC_PEAK_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_PM_DC_PEAK_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_RIN_DC_MON
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_RIN_DC_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0150AU), AMB::MLHWSimBase::monitorPoint_SIGNAL_RIN_DC_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_RIN_DC_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_RIN_DC_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_RIN_DC_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_RIN_LASER_PWR_MON
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_RIN_LASER_PWR_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0150BU), AMB::MLHWSimBase::monitorPoint_SIGNAL_RIN_LASER_PWR_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_RIN_LASER_PWR_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    short raw(0U);

    double value(5.0);

    // Convert world to raw.

    value /= 3.0499999999999999E-4;

    raw = static_cast< short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_RIN_LASER_PWR_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_RIN_LASER_PWR_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_RIN_ERROR_MON
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_RIN_ERROR_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0150CU), AMB::MLHWSimBase::monitorPoint_SIGNAL_RIN_ERROR_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_RIN_ERROR_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_RIN_ERROR_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_RIN_ERROR_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_RIN_DC_CORR_MON
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_RIN_DC_CORR_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0150DU), AMB::MLHWSimBase::monitorPoint_SIGNAL_RIN_DC_CORR_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_RIN_DC_CORR_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_RIN_DC_CORR_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_RIN_DC_CORR_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_VREF
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_VREF()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0150EU), AMB::MLHWSimBase::monitorPoint_SIGNAL_VREF);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_VREF + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    short raw(0U);

    double value(2.5);

    // Convert world to raw.

    value /= 3.0499999999999999E-4;

    raw = static_cast< short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_VREF + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_VREF + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_GND
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_GND()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0150FU), AMB::MLHWSimBase::monitorPoint_SIGNAL_GND);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_GND + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    short raw(0U);

    double value(0.0);

    // Convert world to raw.

    value /= 3.0499999999999999E-4;

    raw = static_cast< short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_GND + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_GND + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_PPLN_TEMP_MON
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_PPLN_TEMP_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01510U), AMB::MLHWSimBase::monitorPoint_SIGNAL_PPLN_TEMP_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_PPLN_TEMP_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned short raw(0U);

    double value(300.0);

    // Convert world to raw.

    value -= 273.15;

    value /= 1.9E-3;

    raw = static_cast< unsigned short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_PPLN_TEMP_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_PPLN_TEMP_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_PPLN_PWR_I_MON
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_PPLN_PWR_I_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01511U), AMB::MLHWSimBase::monitorPoint_SIGNAL_PPLN_PWR_I_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_PPLN_PWR_I_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned short raw(0U);

    double value(0.35);

    // Convert world to raw.

    value /= 3.8146999999999999E-5;

    raw = static_cast< unsigned short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_PPLN_PWR_I_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_PPLN_PWR_I_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_CELL_TEMP_MON
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_CELL_TEMP_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01512U), AMB::MLHWSimBase::monitorPoint_SIGNAL_CELL_TEMP_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_CELL_TEMP_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned short raw(0U);

    double value(340.0);

    // Convert world to raw.

    value -= 273.0;

    value /= 1.9070000000000001E-3;

    raw = static_cast< unsigned short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_CELL_TEMP_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_CELL_TEMP_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_TIP_TEMP_MON
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_TIP_TEMP_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01513U), AMB::MLHWSimBase::monitorPoint_SIGNAL_TIP_TEMP_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_TIP_TEMP_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned short raw(0U);

    double value(340.0);

    // Convert world to raw.

    value -= 273.0;

    value /= 1.9070000000000001E-3;

    raw = static_cast< unsigned short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_TIP_TEMP_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_TIP_TEMP_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_CELL_PWR_I_MON
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_CELL_PWR_I_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01514U), AMB::MLHWSimBase::monitorPoint_SIGNAL_CELL_PWR_I_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_CELL_PWR_I_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned short raw(0U);

    double value(0.6);

    // Convert world to raw.

    value /= 3.8146999999999999E-5;

    raw = static_cast< unsigned short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_CELL_PWR_I_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_CELL_PWR_I_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_HV_MON
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_HV_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01517U), AMB::MLHWSimBase::monitorPoint_SIGNAL_HV_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_HV_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned short raw(0U);

    double value(500.0);

    // Convert world to raw.

    value /= 1.5259999999999999E-2;

    raw = static_cast< unsigned short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_HV_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_HV_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_PM_TEMP_MON
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_PM_TEMP_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01518U), AMB::MLHWSimBase::monitorPoint_SIGNAL_PM_TEMP_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_PM_TEMP_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned short raw(0U);

    double value(300.0);

    // Convert world to raw.

    value -= 273.15;

    value /= 1.91E-3;

    raw = static_cast< unsigned short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_PM_TEMP_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_PM_TEMP_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_FL_TEMP
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_FL_TEMP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01519U), AMB::MLHWSimBase::monitorPoint_SIGNAL_FL_TEMP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_FL_TEMP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned short raw(0U);

    double value(305.0);

    // Convert world to raw.

    value -= 273.15;

    value /= 9.77E-4;

    raw = static_cast< unsigned short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_FL_TEMP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_FL_TEMP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_FL_FAST_TEMP
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_FL_FAST_TEMP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0151AU), AMB::MLHWSimBase::monitorPoint_SIGNAL_FL_FAST_TEMP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_FL_FAST_TEMP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned short raw(0U);

    double value(305.0);

    // Convert world to raw.

    value -= 273.15;

    value /= 9.77E-4;

    raw = static_cast< unsigned short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_FL_FAST_TEMP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_FL_FAST_TEMP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_FL_SLOW_TEMP
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_FL_SLOW_TEMP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0151BU), AMB::MLHWSimBase::monitorPoint_SIGNAL_FL_SLOW_TEMP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_FL_SLOW_TEMP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned short raw(0U);

    double value(305.0);

    // Convert world to raw.

    value -= 273.15;

    value /= 9.77E-4;

    raw = static_cast< unsigned short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_FL_SLOW_TEMP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_FL_SLOW_TEMP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_PPLN_FAST_TEMP
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_PPLN_FAST_TEMP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0151DU), AMB::MLHWSimBase::monitorPoint_SIGNAL_PPLN_FAST_TEMP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_PPLN_FAST_TEMP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned short raw(0U);

    double value(300.0);

    // Convert world to raw.

    value -= 273.15;

    value /= 1.9E-3;

    raw = static_cast< unsigned short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_PPLN_FAST_TEMP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_PPLN_FAST_TEMP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_PPLN_SLOW_TEMP
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_PPLN_SLOW_TEMP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0151EU), AMB::MLHWSimBase::monitorPoint_SIGNAL_PPLN_SLOW_TEMP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_PPLN_SLOW_TEMP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned short raw(0U);

    double value(300.0);

    // Convert world to raw.

    value -= 273.15;

    value /= 1.9E-3;

    raw = static_cast< unsigned short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_PPLN_SLOW_TEMP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_PPLN_SLOW_TEMP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_RBCELL_FAST_TEMP
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_RBCELL_FAST_TEMP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0151FU), AMB::MLHWSimBase::monitorPoint_SIGNAL_RBCELL_FAST_TEMP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_RBCELL_FAST_TEMP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned short raw(0U);

    double value(340.0);

    // Convert world to raw.

    value -= 273.15;

    value /= 1.9070000000000001E-3;

    raw = static_cast< unsigned short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_RBCELL_FAST_TEMP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_RBCELL_FAST_TEMP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_RBCELL_SLOW_TEMP
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_RBCELL_SLOW_TEMP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01520U), AMB::MLHWSimBase::monitorPoint_SIGNAL_RBCELL_SLOW_TEMP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_RBCELL_SLOW_TEMP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned short raw(0U);

    double value(340.0);

    // Convert world to raw.

    value -= 273.15;

    value /= 1.9070000000000001E-3;

    raw = static_cast< unsigned short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_RBCELL_SLOW_TEMP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_RBCELL_SLOW_TEMP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_RBCELL_TIP_FAST_TEMP
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_RBCELL_TIP_FAST_TEMP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01521U), AMB::MLHWSimBase::monitorPoint_SIGNAL_RBCELL_TIP_FAST_TEMP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_RBCELL_TIP_FAST_TEMP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned short raw(0U);

    double value(340.0);

    // Convert world to raw.

    value -= 273.15;

    value /= 1.9070000000000001E-3;

    raw = static_cast< unsigned short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_RBCELL_TIP_FAST_TEMP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_RBCELL_TIP_FAST_TEMP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_RBCELL_TIP_SLOW_TEMP
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_RBCELL_TIP_SLOW_TEMP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01522U), AMB::MLHWSimBase::monitorPoint_SIGNAL_RBCELL_TIP_SLOW_TEMP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_RBCELL_TIP_SLOW_TEMP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned short raw(0U);

    double value(340.0);

    // Convert world to raw.

    value -= 273.15;

    value /= 1.9070000000000001E-3;

    raw = static_cast< unsigned short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_RBCELL_TIP_SLOW_TEMP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_RBCELL_TIP_SLOW_TEMP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_LOCKMON_FAST_FLUO
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_LOCKMON_FAST_FLUO()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01523U), AMB::MLHWSimBase::monitorPoint_SIGNAL_LOCKMON_FAST_FLUO);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_LOCKMON_FAST_FLUO + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned short raw(0U);

    double value(5.0);

    // Convert world to raw.

    value /= 3.0499999999999999E-4;

    raw = static_cast< unsigned short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_LOCKMON_FAST_FLUO + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_LOCKMON_FAST_FLUO + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SIGNAL_LOCKMON_SLOW_FLUO
void MLHWSimImplTestCase::testmonitorPoint_SIGNAL_LOCKMON_SLOW_FLUO()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01524U), AMB::MLHWSimBase::monitorPoint_SIGNAL_LOCKMON_SLOW_FLUO);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_LOCKMON_SLOW_FLUO + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned short raw(0U);

    double value(5.0);

    // Convert world to raw.

    value /= 3.0499999999999999E-4;

    raw = static_cast< unsigned short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SIGNAL_LOCKMON_SLOW_FLUO + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SIGNAL_LOCKMON_SLOW_FLUO + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_SIGNAL_FL_TEMP_MON
void MLHWSimImplTestCase::testmonitorPoint_LASER_SIGNAL_FL_TEMP_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01600U), AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_TEMP_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_TEMP_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    float raw(0U);

    double value(310.0);

    // Convert world to raw.

    value -= 273.15;

    raw = static_cast< float >(value);
    AMB::TypeConversion::valueToData(original, raw, 4U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_TEMP_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_TEMP_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_SIGNAL_FL_THERMV
void MLHWSimImplTestCase::testmonitorPoint_LASER_SIGNAL_FL_THERMV()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01601U), AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_THERMV);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_THERMV + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    float raw(0U);

    double value(2.0);

    raw = static_cast< float >(value);
    AMB::TypeConversion::valueToData(original, raw, 4U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_THERMV + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_THERMV + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_SIGNAL_REF_2_048V
void MLHWSimImplTestCase::testmonitorPoint_LASER_SIGNAL_REF_2_048V()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01602U), AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_REF_2_048V);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_REF_2_048V + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    float raw(0U);

    double value(2.0);

    raw = static_cast< float >(value);
    AMB::TypeConversion::valueToData(original, raw, 4U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_REF_2_048V + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_REF_2_048V + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_SIGNAL_FL_TEC_I
void MLHWSimImplTestCase::testmonitorPoint_LASER_SIGNAL_FL_TEC_I()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01603U), AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_TEC_I);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_TEC_I + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    float raw(0U);

    double value(0.0);

    raw = static_cast< float >(value);
    AMB::TypeConversion::valueToData(original, raw, 4U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_TEC_I + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_TEC_I + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_SIGNAL_FL_OUTPUT_PWR
void MLHWSimImplTestCase::testmonitorPoint_LASER_SIGNAL_FL_OUTPUT_PWR()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01604U), AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_OUTPUT_PWR);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_OUTPUT_PWR + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    float raw(0U);

    double value(2.5000000000000001E-2);

    raw = static_cast< float >(value);
    AMB::TypeConversion::valueToData(original, raw, 4U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_OUTPUT_PWR + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_OUTPUT_PWR + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_SIGNAL_FL_PUMP_I
void MLHWSimImplTestCase::testmonitorPoint_LASER_SIGNAL_FL_PUMP_I()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01605U), AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_PUMP_I);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_PUMP_I + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    float raw(0U);

    double value(0.5);

    raw = static_cast< float >(value);
    AMB::TypeConversion::valueToData(original, raw, 4U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_PUMP_I + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_PUMP_I + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_SIGNAL_FL_PUMP_I_MON
void MLHWSimImplTestCase::testmonitorPoint_LASER_SIGNAL_FL_PUMP_I_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01606U), AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_PUMP_I_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_PUMP_I_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    float raw(0U);

    double value(1.2500000000000001E-2);

    raw = static_cast< float >(value);
    AMB::TypeConversion::valueToData(original, raw, 4U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_PUMP_I_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_PUMP_I_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_SIGNAL_FL_PUMP_TEC_I
void MLHWSimImplTestCase::testmonitorPoint_LASER_SIGNAL_FL_PUMP_TEC_I()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01608U), AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_PUMP_TEC_I);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_PUMP_TEC_I + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    float raw(0U);

    double value(0.0);

    raw = static_cast< float >(value);
    AMB::TypeConversion::valueToData(original, raw, 4U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_PUMP_TEC_I + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_PUMP_TEC_I + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_SIGNAL_PWRAMP_INPUT_PWR
void MLHWSimImplTestCase::testmonitorPoint_LASER_SIGNAL_PWRAMP_INPUT_PWR()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0160AU), AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_PWRAMP_INPUT_PWR);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_PWRAMP_INPUT_PWR + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    float raw(0U);

    double value(0.1);

    raw = static_cast< float >(value);
    AMB::TypeConversion::valueToData(original, raw, 4U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_PWRAMP_INPUT_PWR + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_PWRAMP_INPUT_PWR + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_SIGNAL_PWRAMP_REFL_PWR
void MLHWSimImplTestCase::testmonitorPoint_LASER_SIGNAL_PWRAMP_REFL_PWR()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0160BU), AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_PWRAMP_REFL_PWR);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_PWRAMP_REFL_PWR + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    float raw(0U);

    double value(2.5000000000000001E-2);

    raw = static_cast< float >(value);
    AMB::TypeConversion::valueToData(original, raw, 4U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_PWRAMP_REFL_PWR + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_PWRAMP_REFL_PWR + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_SIGNAL_MODULE_OUTPUT_PWR
void MLHWSimImplTestCase::testmonitorPoint_LASER_SIGNAL_MODULE_OUTPUT_PWR()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0160CU), AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_MODULE_OUTPUT_PWR);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_MODULE_OUTPUT_PWR + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    float raw(0U);

    double value(0.11);

    raw = static_cast< float >(value);
    AMB::TypeConversion::valueToData(original, raw, 4U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_MODULE_OUTPUT_PWR + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_MODULE_OUTPUT_PWR + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_SIGNAL_PWRAMP_PUMP_I
void MLHWSimImplTestCase::testmonitorPoint_LASER_SIGNAL_PWRAMP_PUMP_I()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0160DU), AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_PWRAMP_PUMP_I);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_PWRAMP_PUMP_I + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    float raw(0U);

    double value(3.0);

    raw = static_cast< float >(value);
    AMB::TypeConversion::valueToData(original, raw, 4U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_PWRAMP_PUMP_I + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_PWRAMP_PUMP_I + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_SIGNAL_FL_TEMP_SETP
void MLHWSimImplTestCase::testmonitorPoint_LASER_SIGNAL_FL_TEMP_SETP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0160EU), AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_TEMP_SETP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_TEMP_SETP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    float raw(0U);

    double value(310.0);

    // Convert world to raw.

    value -= 273.15;

    raw = static_cast< float >(value);
    AMB::TypeConversion::valueToData(original, raw, 4U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_TEMP_SETP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_FL_TEMP_SETP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_SIGNAL_MODULE_OUTPUT_PWR_SETP
void MLHWSimImplTestCase::testmonitorPoint_LASER_SIGNAL_MODULE_OUTPUT_PWR_SETP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01600FU), AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_MODULE_OUTPUT_PWR_SETP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_MODULE_OUTPUT_PWR_SETP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    float raw(0U);

    double value(0.11);

    raw = static_cast< float >(value);
    AMB::TypeConversion::valueToData(original, raw, 4U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_MODULE_OUTPUT_PWR_SETP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_SIGNAL_MODULE_OUTPUT_PWR_SETP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_ADC_PID_LASER_CORR_MON
void MLHWSimImplTestCase::testmonitorPoint_LL_ADC_PID_LASER_CORR_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02020U), AMB::MLHWSimBase::monitorPoint_LL_ADC_PID_LASER_CORR_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_PID_LASER_CORR_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    short raw(0U);

    double value(0.0);

    // Convert world to raw.

    value /= 3.0499999999999999E-4;

    raw = static_cast< short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_ADC_PID_LASER_CORR_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_PID_LASER_CORR_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_ADC_PID_ERROR_MON
void MLHWSimImplTestCase::testmonitorPoint_LL_ADC_PID_ERROR_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02021U), AMB::MLHWSimBase::monitorPoint_LL_ADC_PID_ERROR_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_PID_ERROR_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    short raw(0U);

    double value(0.0);

    // Convert world to raw.

    value /= 3.0499999999999999E-4;

    raw = static_cast< short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_ADC_PID_ERROR_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_PID_ERROR_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_ADC_PM_DC_10V
void MLHWSimImplTestCase::testmonitorPoint_LL_ADC_PM_DC_10V()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02022U), AMB::MLHWSimBase::monitorPoint_LL_ADC_PM_DC_10V);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_PM_DC_10V + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    short raw(0U);

    double value(5.0);

    // Convert world to raw.

    value /= 3.0499999999999999E-4;

    raw = static_cast< short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_ADC_PM_DC_10V + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_PM_DC_10V + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_ADC_PIEZO_OUT_MON
void MLHWSimImplTestCase::testmonitorPoint_LL_ADC_PIEZO_OUT_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02023U), AMB::MLHWSimBase::monitorPoint_LL_ADC_PIEZO_OUT_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_PIEZO_OUT_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    short raw(0U);

    double value(42.5);

    // Convert world to raw.

    value /= 3.0500000000000002E-3;

    raw = static_cast< short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_ADC_PIEZO_OUT_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_PIEZO_OUT_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_ADC_PIEZO_SUM_MON
void MLHWSimImplTestCase::testmonitorPoint_LL_ADC_PIEZO_SUM_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02024U), AMB::MLHWSimBase::monitorPoint_LL_ADC_PIEZO_SUM_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_PIEZO_SUM_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    short raw(0U);

    double value(5.0);

    // Convert world to raw.

    value /= 3.0499999999999999E-4;

    raw = static_cast< short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_ADC_PIEZO_SUM_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_PIEZO_SUM_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_ADC_ERROR_PEAK_MON
void MLHWSimImplTestCase::testmonitorPoint_LL_ADC_ERROR_PEAK_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02028U), AMB::MLHWSimBase::monitorPoint_LL_ADC_ERROR_PEAK_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_ERROR_PEAK_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    short raw(0U);

    double value(0.0);

    // Convert world to raw.

    value /= 3.0499999999999999E-4;

    raw = static_cast< short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_ADC_ERROR_PEAK_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_ERROR_PEAK_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_ADC_PM_DC_PEAK_MON
void MLHWSimImplTestCase::testmonitorPoint_LL_ADC_PM_DC_PEAK_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02029U), AMB::MLHWSimBase::monitorPoint_LL_ADC_PM_DC_PEAK_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_PM_DC_PEAK_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    short raw(0U);

    double value(5.0);

    // Convert world to raw.

    value /= 3.0499999999999999E-4;

    raw = static_cast< short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_ADC_PM_DC_PEAK_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_PM_DC_PEAK_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_ADC_RIN_DC_MON
void MLHWSimImplTestCase::testmonitorPoint_LL_ADC_RIN_DC_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0202AU), AMB::MLHWSimBase::monitorPoint_LL_ADC_RIN_DC_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_RIN_DC_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_ADC_RIN_DC_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_RIN_DC_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_ADC_RIN_LASER_PWR_MON
void MLHWSimImplTestCase::testmonitorPoint_LL_ADC_RIN_LASER_PWR_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0202BU), AMB::MLHWSimBase::monitorPoint_LL_ADC_RIN_LASER_PWR_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_RIN_LASER_PWR_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    short raw(0U);

    double value(5.0);

    // Convert world to raw.

    value /= 3.0499999999999999E-4;

    raw = static_cast< short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_ADC_RIN_LASER_PWR_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_RIN_LASER_PWR_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_ADC_RIN_ERROR_MON
void MLHWSimImplTestCase::testmonitorPoint_LL_ADC_RIN_ERROR_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0202CU), AMB::MLHWSimBase::monitorPoint_LL_ADC_RIN_ERROR_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_RIN_ERROR_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_ADC_RIN_ERROR_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_RIN_ERROR_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_ADC_RIN_DC_CORR_MON
void MLHWSimImplTestCase::testmonitorPoint_LL_ADC_RIN_DC_CORR_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0202DU), AMB::MLHWSimBase::monitorPoint_LL_ADC_RIN_DC_CORR_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_RIN_DC_CORR_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_ADC_RIN_DC_CORR_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_RIN_DC_CORR_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_ADC_VREF
void MLHWSimImplTestCase::testmonitorPoint_LL_ADC_VREF()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0202EU), AMB::MLHWSimBase::monitorPoint_LL_ADC_VREF);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_VREF + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    short raw(0U);

    double value(2.5);

    // Convert world to raw.

    value /= 3.0499999999999999E-4;

    raw = static_cast< short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_ADC_VREF + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_VREF + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_ADC_GND
void MLHWSimImplTestCase::testmonitorPoint_LL_ADC_GND()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0202FU), AMB::MLHWSimBase::monitorPoint_LL_ADC_GND);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_GND + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    short raw(0U);

    double value(0.0);

    // Convert world to raw.

    value /= 3.0499999999999999E-4;

    raw = static_cast< short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_ADC_GND + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_GND + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_ADC_PPLN_TEMP_MON
void MLHWSimImplTestCase::testmonitorPoint_LL_ADC_PPLN_TEMP_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02030U), AMB::MLHWSimBase::monitorPoint_LL_ADC_PPLN_TEMP_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_PPLN_TEMP_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned short raw(0U);

    double value(300.0);

    // Convert world to raw.

    value -= 273.15;

    value /= 1.9E-3;

    raw = static_cast< unsigned short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

//    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_ADC_PPLN_TEMP_MON + baseAddress_m, createVector(size)));
//
//    received.clear();
//    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_PPLN_TEMP_MON + baseAddress_m));
//    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
//    for(AmbDataLength_t i(0U); i < size; ++i)
//    {
//        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//    }
}

///  testmonitorPoint_LL_ADC_PPLN_PWR_I_MON
void MLHWSimImplTestCase::testmonitorPoint_LL_ADC_PPLN_PWR_I_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02031U), AMB::MLHWSimBase::monitorPoint_LL_ADC_PPLN_PWR_I_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_PPLN_PWR_I_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned short raw(0U);

    double value(0.35);

    // Convert world to raw.

    value /= 3.8146999999999999E-5;

    raw = static_cast< unsigned short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

//    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_ADC_PPLN_PWR_I_MON + baseAddress_m, createVector(size)));
//
//    received.clear();
//    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_PPLN_PWR_I_MON + baseAddress_m));
//    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
//    for(AmbDataLength_t i(0U); i < size; ++i)
//    {
//        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//    }
}

///  testmonitorPoint_LL_ADC_CELL_TEMP_MON
void MLHWSimImplTestCase::testmonitorPoint_LL_ADC_CELL_TEMP_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02032U), AMB::MLHWSimBase::monitorPoint_LL_ADC_CELL_TEMP_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_CELL_TEMP_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned short raw(0U);

    double value(340.0);

    // Convert world to raw.

    value -= 273.15;

    value /= 1.9070000000000001E-3;

    raw = static_cast< unsigned short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_ADC_CELL_TEMP_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_CELL_TEMP_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_ADC_TIP_TEMP_MON
void MLHWSimImplTestCase::testmonitorPoint_LL_ADC_TIP_TEMP_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02033U), AMB::MLHWSimBase::monitorPoint_LL_ADC_TIP_TEMP_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_TIP_TEMP_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned short raw(0U);

    double value(340.0);

    // Convert world to raw.

    value -= 273.15;

    value /= 1.9070000000000001E-3;

    raw = static_cast< unsigned short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_ADC_TIP_TEMP_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_TIP_TEMP_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_ADC_CELL_PWR_I_MON
void MLHWSimImplTestCase::testmonitorPoint_LL_ADC_CELL_PWR_I_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02034U), AMB::MLHWSimBase::monitorPoint_LL_ADC_CELL_PWR_I_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_CELL_PWR_I_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned short raw(0U);

    double value(0.6);

    // Convert world to raw.

    value /= 3.8146999999999999E-5;

    raw = static_cast< unsigned short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_ADC_CELL_PWR_I_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_CELL_PWR_I_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_ADC_HV_MON
void MLHWSimImplTestCase::testmonitorPoint_LL_ADC_HV_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02037U), AMB::MLHWSimBase::monitorPoint_LL_ADC_HV_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_HV_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned short raw(0U);

    double value(500.0);

    // Convert world to raw.

    value /= 1.5259999999999999E-2;

    raw = static_cast< unsigned short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_ADC_HV_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_HV_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_ADC_PM_TEMP_MON
void MLHWSimImplTestCase::testmonitorPoint_LL_ADC_PM_TEMP_MON()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02038U), AMB::MLHWSimBase::monitorPoint_LL_ADC_PM_TEMP_MON);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_PM_TEMP_MON + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned short raw(0U);

    double value(300.0);

    // Convert world to raw.

    value -= 273.15;

    value /= 1.91E-3;

    raw = static_cast< unsigned short >(value);
    AMB::TypeConversion::valueToData(original, raw, 2U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_ADC_PM_TEMP_MON + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_ADC_PM_TEMP_MON + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_AL_OPTIMISTIC_CUR_SCAN_RANGE
void MLHWSimImplTestCase::testmonitorPoint_AL_OPTIMISTIC_CUR_SCAN_RANGE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04010U), AMB::MLHWSimBase::monitorPoint_AL_OPTIMISTIC_CUR_SCAN_RANGE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_OPTIMISTIC_CUR_SCAN_RANGE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_AL_OPTIMISTIC_CUR_SCAN_RANGE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_OPTIMISTIC_CUR_SCAN_RANGE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_AL_ALREADY_SCANNED_RANGE_MIN
void MLHWSimImplTestCase::testmonitorPoint_AL_ALREADY_SCANNED_RANGE_MIN()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04011U), AMB::MLHWSimBase::monitorPoint_AL_ALREADY_SCANNED_RANGE_MIN);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_ALREADY_SCANNED_RANGE_MIN + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_AL_ALREADY_SCANNED_RANGE_MIN + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_ALREADY_SCANNED_RANGE_MIN + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_AL_ALREADY_SCANNED_RANGE_MAX
void MLHWSimImplTestCase::testmonitorPoint_AL_ALREADY_SCANNED_RANGE_MAX()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04012U), AMB::MLHWSimBase::monitorPoint_AL_ALREADY_SCANNED_RANGE_MAX);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_ALREADY_SCANNED_RANGE_MAX + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_AL_ALREADY_SCANNED_RANGE_MAX + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_ALREADY_SCANNED_RANGE_MAX + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_AL_STATE
void MLHWSimImplTestCase::testmonitorPoint_AL_STATE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04013U), AMB::MLHWSimBase::monitorPoint_AL_STATE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_STATE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_AL_STATE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_STATE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_AL_NB_GROUPS
void MLHWSimImplTestCase::testmonitorPoint_AL_NB_GROUPS()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04015U), AMB::MLHWSimBase::monitorPoint_AL_NB_GROUPS);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_NB_GROUPS + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_AL_NB_GROUPS + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_NB_GROUPS + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_AL_ERROR_AMPLITUDE
void MLHWSimImplTestCase::testmonitorPoint_AL_ERROR_AMPLITUDE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A070U), AMB::MLHWSimBase::monitorPoint_AL_ERROR_AMPLITUDE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_ERROR_AMPLITUDE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_AL_ERROR_AMPLITUDE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_ERROR_AMPLITUDE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_AL_IDENTIFICATION_VALID_RIGHT_GROUP
void MLHWSimImplTestCase::testmonitorPoint_AL_IDENTIFICATION_VALID_RIGHT_GROUP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A040U), AMB::MLHWSimBase::monitorPoint_AL_IDENTIFICATION_VALID_RIGHT_GROUP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_IDENTIFICATION_VALID_RIGHT_GROUP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_AL_IDENTIFICATION_VALID_RIGHT_GROUP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_IDENTIFICATION_VALID_RIGHT_GROUP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_AL_IDENTIFICATION_VALID_WRONG_GROUP
void MLHWSimImplTestCase::testmonitorPoint_AL_IDENTIFICATION_VALID_WRONG_GROUP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A041U), AMB::MLHWSimBase::monitorPoint_AL_IDENTIFICATION_VALID_WRONG_GROUP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_IDENTIFICATION_VALID_WRONG_GROUP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_AL_IDENTIFICATION_VALID_WRONG_GROUP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_IDENTIFICATION_VALID_WRONG_GROUP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_AL_IDENTIFICATION_INVALID_GROUP
void MLHWSimImplTestCase::testmonitorPoint_AL_IDENTIFICATION_INVALID_GROUP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A042U), AMB::MLHWSimBase::monitorPoint_AL_IDENTIFICATION_INVALID_GROUP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_IDENTIFICATION_INVALID_GROUP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_AL_IDENTIFICATION_INVALID_GROUP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_IDENTIFICATION_INVALID_GROUP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_ORM_INTERLOCK_CLOSED
void MLHWSimImplTestCase::testmonitorPoint_ORM_INTERLOCK_CLOSED()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A060U), AMB::MLHWSimBase::monitorPoint_ORM_INTERLOCK_CLOSED);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_ORM_INTERLOCK_CLOSED + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned char raw(0U);

    double value(1);

    raw = static_cast< unsigned char >(value);
    AMB::TypeConversion::valueToData(original, raw, 1U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_ORM_INTERLOCK_CLOSED + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_ORM_INTERLOCK_CLOSED + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PZT_RANGE_CONTROL_STATE
void MLHWSimImplTestCase::testmonitorPoint_PZT_RANGE_CONTROL_STATE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06813U), AMB::MLHWSimBase::monitorPoint_PZT_RANGE_CONTROL_STATE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PZT_RANGE_CONTROL_STATE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PZT_RANGE_CONTROL_STATE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PZT_RANGE_CONTROL_STATE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PPLN_EFFICIENCY_CONTROL_ACTIVE
void MLHWSimImplTestCase::testmonitorPoint_PPLN_EFFICIENCY_CONTROL_ACTIVE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0701DU), AMB::MLHWSimBase::monitorPoint_PPLN_EFFICIENCY_CONTROL_ACTIVE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_EFFICIENCY_CONTROL_ACTIVE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PPLN_EFFICIENCY_CONTROL_ACTIVE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_EFFICIENCY_CONTROL_ACTIVE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PPLN_STATE
void MLHWSimImplTestCase::testmonitorPoint_PPLN_STATE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A053U), AMB::MLHWSimBase::monitorPoint_PPLN_STATE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_STATE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PPLN_STATE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_STATE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_STATE
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_STATE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x08005U), AMB::MLHWSimBase::monitorPoint_PEAKS_STATE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_STATE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_STATE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_STATE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_THRESHOLD_LEVEL
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_THRESHOLD_LEVEL()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x08007U), AMB::MLHWSimBase::monitorPoint_PEAKS_THRESHOLD_LEVEL);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_THRESHOLD_LEVEL + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_THRESHOLD_LEVEL + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_THRESHOLD_LEVEL + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_NEW_DETECTION_AVAILABLE
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_NEW_DETECTION_AVAILABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x08008U), AMB::MLHWSimBase::monitorPoint_PEAKS_NEW_DETECTION_AVAILABLE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_NEW_DETECTION_AVAILABLE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned char raw(0U);

    double value(0);

    raw = static_cast< unsigned char >(value);
    AMB::TypeConversion::valueToData(original, raw, 1U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_NEW_DETECTION_AVAILABLE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_NEW_DETECTION_AVAILABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_NB_PEAKS
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_NB_PEAKS()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x08009U), AMB::MLHWSimBase::monitorPoint_PEAKS_NB_PEAKS);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_NB_PEAKS + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_NB_PEAKS + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_NB_PEAKS + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_SIGNAL_VALIDATION
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_SIGNAL_VALIDATION()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0800AU), AMB::MLHWSimBase::monitorPoint_PEAKS_SIGNAL_VALIDATION);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_SIGNAL_VALIDATION + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_SIGNAL_VALIDATION + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_SIGNAL_VALIDATION + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_ZERO
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_ZERO()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A000U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_ZERO);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_ZERO + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_ZERO + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_ZERO + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_ONE
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_ONE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A005U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_ONE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_ONE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_ONE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_ONE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_TWO
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_TWO()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A00AU), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_TWO);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_TWO + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_TWO + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_TWO + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_THREE
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_THREE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A00FU), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_THREE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_THREE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_THREE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_THREE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_FOUR
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_FOUR()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A014U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_FOUR);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_FOUR + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_FOUR + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_FOUR + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_FIVE
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_FIVE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A019U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_FIVE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_FIVE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_FIVE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_FIVE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_SIX
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_SIX()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A01EU), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_SIX);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_SIX + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_SIX + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_SIX + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_SEVEN
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_SEVEN()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A023U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_SEVEN);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_SEVEN + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_SEVEN + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_SEVEN + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_EIGHT
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_EIGHT()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A028U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_EIGHT);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_EIGHT + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_EIGHT + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_EIGHT + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_NINE
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_NINE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A02DU), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_NINE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_NINE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_NINE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_NINE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_ZERO
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_ZERO()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A001U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_ZERO);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_ZERO + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_ZERO + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_ZERO + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_ONE
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_ONE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A006U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_ONE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_ONE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_ONE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_ONE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_TWO
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_TWO()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A00BU), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_TWO);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_TWO + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_TWO + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_TWO + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_THREE
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_THREE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A010U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_THREE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_THREE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_THREE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_THREE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_FOUR
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_FOUR()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A015U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_FOUR);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_FOUR + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_FOUR + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_FOUR + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_FIVE
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_FIVE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A01AU), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_FIVE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_FIVE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_FIVE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_FIVE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_SIX
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_SIX()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A01FU), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_SIX);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_SIX + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_SIX + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_SIX + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_SEVEN
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_SEVEN()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A024U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_SEVEN);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_SEVEN + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_SEVEN + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_SEVEN + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_EIGHT
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_EIGHT()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A029U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_EIGHT);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_EIGHT + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_EIGHT + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_EIGHT + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_NINE
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_NINE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A02EU), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_NINE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_NINE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_NINE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_NINE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_ZERO
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_ZERO()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A002U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_ZERO);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_ZERO + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_ZERO + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_ZERO + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_ONE
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_ONE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A007U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_ONE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_ONE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_ONE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_ONE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_TWO
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_TWO()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A00CU), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_TWO);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_TWO + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_TWO + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_TWO + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_THREE
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_THREE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A011U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_THREE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_THREE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_THREE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_THREE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_FOUR
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_FOUR()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A016U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_FOUR);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_FOUR + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_FOUR + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_FOUR + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_FIVE
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_FIVE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A01BU), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_FIVE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_FIVE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_FIVE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_FIVE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_SIX
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_SIX()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A020U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_SIX);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_SIX + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_SIX + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_SIX + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_SEVEN
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_SEVEN()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A025U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_SEVEN);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_SEVEN + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_SEVEN + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_SEVEN + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_EIGHT
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_EIGHT()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A02AU), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_EIGHT);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_EIGHT + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_EIGHT + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_EIGHT + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_NINE
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_NINE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A02FU), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_NINE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_NINE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_NINE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_NINE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_ZERO
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_ZERO()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A003U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_ZERO);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_ZERO + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_ZERO + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_ZERO + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_ONE
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_ONE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A008U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_ONE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_ONE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_ONE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_ONE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_TWO
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_TWO()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A00DU), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_TWO);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_TWO + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_TWO + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_TWO + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_THREE
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_THREE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A012U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_THREE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_THREE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_THREE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_THREE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_FOUR
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_FOUR()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A017U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_FOUR);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_FOUR + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_FOUR + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_FOUR + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_FIVE
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_FIVE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A01CU), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_FIVE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_FIVE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_FIVE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_FIVE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_SIX
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_SIX()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A021U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_SIX);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_SIX + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_SIX + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_SIX + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_SEVEN
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_SEVEN()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A026U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_SEVEN);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_SEVEN + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_SEVEN + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_SEVEN + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_EIGHT
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_EIGHT()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A02BU), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_EIGHT);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_EIGHT + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_EIGHT + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_EIGHT + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_NINE
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_NINE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A030U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_NINE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_NINE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_NINE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_NINE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_ZERO
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_ZERO()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A004U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_ZERO);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_ZERO + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_ZERO + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_ZERO + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_ONE
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_ONE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A009U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_ONE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_ONE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_ONE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_ONE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_TWO
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_TWO()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A00EU), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_TWO);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_TWO + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_TWO + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_TWO + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_THREE
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_THREE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A013U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_THREE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_THREE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_THREE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_THREE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_FOUR
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_FOUR()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A018U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_FOUR);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_FOUR + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_FOUR + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_FOUR + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_FIVE
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_FIVE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A01DU), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_FIVE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_FIVE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_FIVE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_FIVE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_SIX
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_SIX()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A022U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_SIX);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_SIX + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_SIX + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_SIX + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_SEVEN
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_SEVEN()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A027U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_SEVEN);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_SEVEN + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_SEVEN + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_SEVEN + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_EIGHT
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_EIGHT()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A02CU), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_EIGHT);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_EIGHT + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_EIGHT + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_EIGHT + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_NINE
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_NINE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A031U), AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_NINE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_NINE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_NINE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_NINE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SCAN_DELTA_T_STEPS
void MLHWSimImplTestCase::testmonitorPoint_SCAN_DELTA_T_STEPS()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x09007U), AMB::MLHWSimBase::monitorPoint_SCAN_DELTA_T_STEPS);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SCAN_DELTA_T_STEPS + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SCAN_DELTA_T_STEPS + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SCAN_DELTA_T_STEPS + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SCAN_STATE
void MLHWSimImplTestCase::testmonitorPoint_SCAN_STATE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x09008U), AMB::MLHWSimBase::monitorPoint_SCAN_STATE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SCAN_STATE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SCAN_STATE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SCAN_STATE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SYSTEM_STARTUP_MODE
void MLHWSimImplTestCase::testmonitorPoint_SYSTEM_STARTUP_MODE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01800U), AMB::MLHWSimBase::monitorPoint_SYSTEM_STARTUP_MODE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SYSTEM_STARTUP_MODE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SYSTEM_STARTUP_MODE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SYSTEM_STARTUP_MODE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SYSTEM_STARTUP_PARAM_SET
void MLHWSimImplTestCase::testmonitorPoint_SYSTEM_STARTUP_PARAM_SET()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01810U), AMB::MLHWSimBase::monitorPoint_SYSTEM_STARTUP_PARAM_SET);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SYSTEM_STARTUP_PARAM_SET + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    unsigned char raw(0U);

    double value(0);

    raw = static_cast< unsigned char >(value);
    AMB::TypeConversion::valueToData(original, raw, 1U);

    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SYSTEM_STARTUP_PARAM_SET + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SYSTEM_STARTUP_PARAM_SET + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_DAC_PIEZO_SETP
void MLHWSimImplTestCase::testmonitorPoint_LL_DAC_PIEZO_SETP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02010U), AMB::MLHWSimBase::monitorPoint_LL_DAC_PIEZO_SETP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DAC_PIEZO_SETP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_DAC_PIEZO_SETP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DAC_PIEZO_SETP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_DAC_RAMP_TRIG_SETP
void MLHWSimImplTestCase::testmonitorPoint_LL_DAC_RAMP_TRIG_SETP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02011U), AMB::MLHWSimBase::monitorPoint_LL_DAC_RAMP_TRIG_SETP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DAC_RAMP_TRIG_SETP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_DAC_RAMP_TRIG_SETP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DAC_RAMP_TRIG_SETP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_DAC_RAMP_SLOPE
void MLHWSimImplTestCase::testmonitorPoint_LL_DAC_RAMP_SLOPE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02012U), AMB::MLHWSimBase::monitorPoint_LL_DAC_RAMP_SLOPE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DAC_RAMP_SLOPE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_DAC_RAMP_SLOPE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DAC_RAMP_SLOPE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_DAC_PID_P_GAIN_SETP
void MLHWSimImplTestCase::testmonitorPoint_LL_DAC_PID_P_GAIN_SETP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02013U), AMB::MLHWSimBase::monitorPoint_LL_DAC_PID_P_GAIN_SETP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DAC_PID_P_GAIN_SETP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_DAC_PID_P_GAIN_SETP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DAC_PID_P_GAIN_SETP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_DAC_PID_OFFSET_CORR
void MLHWSimImplTestCase::testmonitorPoint_LL_DAC_PID_OFFSET_CORR()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02014U), AMB::MLHWSimBase::monitorPoint_LL_DAC_PID_OFFSET_CORR);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DAC_PID_OFFSET_CORR + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_DAC_PID_OFFSET_CORR + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DAC_PID_OFFSET_CORR + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_DAC_TRIG_ERROR_SETP
void MLHWSimImplTestCase::testmonitorPoint_LL_DAC_TRIG_ERROR_SETP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02015U), AMB::MLHWSimBase::monitorPoint_LL_DAC_TRIG_ERROR_SETP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DAC_TRIG_ERROR_SETP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_DAC_TRIG_ERROR_SETP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DAC_TRIG_ERROR_SETP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_DAC_RIN_LOCK_SETP
void MLHWSimImplTestCase::testmonitorPoint_LL_DAC_RIN_LOCK_SETP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02016U), AMB::MLHWSimBase::monitorPoint_LL_DAC_RIN_LOCK_SETP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DAC_RIN_LOCK_SETP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_DAC_RIN_LOCK_SETP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DAC_RIN_LOCK_SETP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_DAC_RIN_DC_OFFSET_P
void MLHWSimImplTestCase::testmonitorPoint_LL_DAC_RIN_DC_OFFSET_P()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02017U), AMB::MLHWSimBase::monitorPoint_LL_DAC_RIN_DC_OFFSET_P);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DAC_RIN_DC_OFFSET_P + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_DAC_RIN_DC_OFFSET_P + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DAC_RIN_DC_OFFSET_P + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_DIGIPOT_DDS_MOD_ADJ
void MLHWSimImplTestCase::testmonitorPoint_LL_DIGIPOT_DDS_MOD_ADJ()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02040U), AMB::MLHWSimBase::monitorPoint_LL_DIGIPOT_DDS_MOD_ADJ);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DIGIPOT_DDS_MOD_ADJ + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_DIGIPOT_DDS_MOD_ADJ + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DIGIPOT_DDS_MOD_ADJ + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_DIGIPOT_DDS_LO_ADJ
void MLHWSimImplTestCase::testmonitorPoint_LL_DIGIPOT_DDS_LO_ADJ()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02041U), AMB::MLHWSimBase::monitorPoint_LL_DIGIPOT_DDS_LO_ADJ);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DIGIPOT_DDS_LO_ADJ + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_DIGIPOT_DDS_LO_ADJ + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DIGIPOT_DDS_LO_ADJ + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_DIGIPOT_RIN_POWER_MON_GAIN
void MLHWSimImplTestCase::testmonitorPoint_LL_DIGIPOT_RIN_POWER_MON_GAIN()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02042U), AMB::MLHWSimBase::monitorPoint_LL_DIGIPOT_RIN_POWER_MON_GAIN);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DIGIPOT_RIN_POWER_MON_GAIN + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_DIGIPOT_RIN_POWER_MON_GAIN + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DIGIPOT_RIN_POWER_MON_GAIN + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_DIGIPOT_RIN_REFERENCE_MON_GAIN
void MLHWSimImplTestCase::testmonitorPoint_LL_DIGIPOT_RIN_REFERENCE_MON_GAIN()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02043U), AMB::MLHWSimBase::monitorPoint_LL_DIGIPOT_RIN_REFERENCE_MON_GAIN);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DIGIPOT_RIN_REFERENCE_MON_GAIN + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_DIGIPOT_RIN_REFERENCE_MON_GAIN + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DIGIPOT_RIN_REFERENCE_MON_GAIN + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_DIGIPOT_ORM_PPLN_GAIN
void MLHWSimImplTestCase::testmonitorPoint_LL_DIGIPOT_ORM_PPLN_GAIN()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02044U), AMB::MLHWSimBase::monitorPoint_LL_DIGIPOT_ORM_PPLN_GAIN);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DIGIPOT_ORM_PPLN_GAIN + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_DIGIPOT_ORM_PPLN_GAIN + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DIGIPOT_ORM_PPLN_GAIN + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_DIGIPOT_ORM_CELL_GAIN
void MLHWSimImplTestCase::testmonitorPoint_LL_DIGIPOT_ORM_CELL_GAIN()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02045U), AMB::MLHWSimBase::monitorPoint_LL_DIGIPOT_ORM_CELL_GAIN);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DIGIPOT_ORM_CELL_GAIN + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_DIGIPOT_ORM_CELL_GAIN + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DIGIPOT_ORM_CELL_GAIN + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_DDS_OSCILLATOR_LOCAL_OSCILLATOR
void MLHWSimImplTestCase::testmonitorPoint_LL_DDS_OSCILLATOR_LOCAL_OSCILLATOR()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02050U), AMB::MLHWSimBase::monitorPoint_LL_DDS_OSCILLATOR_LOCAL_OSCILLATOR);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DDS_OSCILLATOR_LOCAL_OSCILLATOR + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_DDS_OSCILLATOR_LOCAL_OSCILLATOR + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DDS_OSCILLATOR_LOCAL_OSCILLATOR + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LL_DDS_OSCILLATOR_PHASE_MODULATOR
void MLHWSimImplTestCase::testmonitorPoint_LL_DDS_OSCILLATOR_PHASE_MODULATOR()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02051U), AMB::MLHWSimBase::monitorPoint_LL_DDS_OSCILLATOR_PHASE_MODULATOR);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DDS_OSCILLATOR_PHASE_MODULATOR + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LL_DDS_OSCILLATOR_PHASE_MODULATOR + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LL_DDS_OSCILLATOR_PHASE_MODULATOR + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_PZT_TUNING_COEFF
void MLHWSimImplTestCase::testmonitorPoint_LASER_PZT_TUNING_COEFF()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03000U), AMB::MLHWSimBase::monitorPoint_LASER_PZT_TUNING_COEFF);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_PZT_TUNING_COEFF + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_PZT_TUNING_COEFF + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_PZT_TUNING_COEFF + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_T_TUNING_COEFF
void MLHWSimImplTestCase::testmonitorPoint_LASER_T_TUNING_COEFF()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03001U), AMB::MLHWSimBase::monitorPoint_LASER_T_TUNING_COEFF);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_T_TUNING_COEFF + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_T_TUNING_COEFF + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_T_TUNING_COEFF + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_T
void MLHWSimImplTestCase::testmonitorPoint_LASER_T()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03002U), AMB::MLHWSimBase::monitorPoint_LASER_T);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_T + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_T + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_T + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_PWR
void MLHWSimImplTestCase::testmonitorPoint_LASER_PWR()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03003U), AMB::MLHWSimBase::monitorPoint_LASER_PWR);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_PWR + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_PWR + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_PWR + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_PZT_LEVEL
void MLHWSimImplTestCase::testmonitorPoint_LASER_PZT_LEVEL()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03004U), AMB::MLHWSimBase::monitorPoint_LASER_PZT_LEVEL);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_PZT_LEVEL + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_PZT_LEVEL + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_PZT_LEVEL + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_PWRAMP_ENABLE
void MLHWSimImplTestCase::testmonitorPoint_LASER_PWRAMP_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03005U), AMB::MLHWSimBase::monitorPoint_LASER_PWRAMP_ENABLE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_PWRAMP_ENABLE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_PWRAMP_ENABLE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_PWRAMP_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_TEMP_CTRL_ENABLE
void MLHWSimImplTestCase::testmonitorPoint_LASER_TEMP_CTRL_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03006U), AMB::MLHWSimBase::monitorPoint_LASER_TEMP_CTRL_ENABLE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_TEMP_CTRL_ENABLE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_TEMP_CTRL_ENABLE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_TEMP_CTRL_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_TEMPMON_ABS_ERR
void MLHWSimImplTestCase::testmonitorPoint_LASER_TEMPMON_ABS_ERR()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03100U), AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_ABS_ERR);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_ABS_ERR + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_ABS_ERR + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_ABS_ERR + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_TEMPMON_TUNNEL
void MLHWSimImplTestCase::testmonitorPoint_LASER_TEMPMON_TUNNEL()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03101U), AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_TUNNEL);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_TUNNEL + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_TUNNEL + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_TUNNEL + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_TEMPMON_STABLE_TIME
void MLHWSimImplTestCase::testmonitorPoint_LASER_TEMPMON_STABLE_TIME()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03102U), AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_STABLE_TIME);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_STABLE_TIME + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_STABLE_TIME + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_STABLE_TIME + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_TEMPMON_TIMEOUT
void MLHWSimImplTestCase::testmonitorPoint_LASER_TEMPMON_TIMEOUT()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03103U), AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_TIMEOUT);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_TIMEOUT + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_TIMEOUT + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_TIMEOUT + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_TEMPMON_SLOW_FILTA
void MLHWSimImplTestCase::testmonitorPoint_LASER_TEMPMON_SLOW_FILTA()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03104U), AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_SLOW_FILTA);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_SLOW_FILTA + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_SLOW_FILTA + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_SLOW_FILTA + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_TEMPMON_FAST_FILTA
void MLHWSimImplTestCase::testmonitorPoint_LASER_TEMPMON_FAST_FILTA()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03105U), AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_FAST_FILTA);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_FAST_FILTA + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_FAST_FILTA + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_FAST_FILTA + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_TEMPMON_ENABLE
void MLHWSimImplTestCase::testmonitorPoint_LASER_TEMPMON_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03106U), AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_ENABLE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_ENABLE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_ENABLE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_TEMPMON_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LASER_POWER_MON_TOLERANCE
void MLHWSimImplTestCase::testmonitorPoint_LASER_POWER_MON_TOLERANCE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03108U), AMB::MLHWSimBase::monitorPoint_LASER_POWER_MON_TOLERANCE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_POWER_MON_TOLERANCE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LASER_POWER_MON_TOLERANCE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LASER_POWER_MON_TOLERANCE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PZT_SWEEP_START_VOLTAGE
void MLHWSimImplTestCase::testmonitorPoint_PZT_SWEEP_START_VOLTAGE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03200U), AMB::MLHWSimBase::monitorPoint_PZT_SWEEP_START_VOLTAGE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PZT_SWEEP_START_VOLTAGE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PZT_SWEEP_START_VOLTAGE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PZT_SWEEP_START_VOLTAGE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PZT_SWEEP_STOP_VOLTAGE
void MLHWSimImplTestCase::testmonitorPoint_PZT_SWEEP_STOP_VOLTAGE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03201U), AMB::MLHWSimBase::monitorPoint_PZT_SWEEP_STOP_VOLTAGE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PZT_SWEEP_STOP_VOLTAGE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PZT_SWEEP_STOP_VOLTAGE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PZT_SWEEP_STOP_VOLTAGE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PZT_SWEEP_DELAY
void MLHWSimImplTestCase::testmonitorPoint_PZT_SWEEP_DELAY()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03202U), AMB::MLHWSimBase::monitorPoint_PZT_SWEEP_DELAY);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PZT_SWEEP_DELAY + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PZT_SWEEP_DELAY + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PZT_SWEEP_DELAY + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PZT_SWEEP_PERIOD
void MLHWSimImplTestCase::testmonitorPoint_PZT_SWEEP_PERIOD()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03203U), AMB::MLHWSimBase::monitorPoint_PZT_SWEEP_PERIOD);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PZT_SWEEP_PERIOD + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PZT_SWEEP_PERIOD + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PZT_SWEEP_PERIOD + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PZT_SWEEP_PERIODIC
void MLHWSimImplTestCase::testmonitorPoint_PZT_SWEEP_PERIODIC()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03204U), AMB::MLHWSimBase::monitorPoint_PZT_SWEEP_PERIODIC);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PZT_SWEEP_PERIODIC + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PZT_SWEEP_PERIODIC + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PZT_SWEEP_PERIODIC + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PZT_SWEEP_FILTER
void MLHWSimImplTestCase::testmonitorPoint_PZT_SWEEP_FILTER()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03205U), AMB::MLHWSimBase::monitorPoint_PZT_SWEEP_FILTER);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PZT_SWEEP_FILTER + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PZT_SWEEP_FILTER + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PZT_SWEEP_FILTER + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_AL_TEMP_MIN
void MLHWSimImplTestCase::testmonitorPoint_AL_TEMP_MIN()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04000U), AMB::MLHWSimBase::monitorPoint_AL_TEMP_MIN);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_TEMP_MIN + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_AL_TEMP_MIN + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_TEMP_MIN + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_AL_TEMP_MAX
void MLHWSimImplTestCase::testmonitorPoint_AL_TEMP_MAX()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04001U), AMB::MLHWSimBase::monitorPoint_AL_TEMP_MAX);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_TEMP_MAX + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_AL_TEMP_MAX + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_TEMP_MAX + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_AL_START_TEMP
void MLHWSimImplTestCase::testmonitorPoint_AL_START_TEMP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04002U), AMB::MLHWSimBase::monitorPoint_AL_START_TEMP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_START_TEMP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_AL_START_TEMP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_START_TEMP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_AL_OPTIMISTIC_SCAN_RANGE
void MLHWSimImplTestCase::testmonitorPoint_AL_OPTIMISTIC_SCAN_RANGE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04003U), AMB::MLHWSimBase::monitorPoint_AL_OPTIMISTIC_SCAN_RANGE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_OPTIMISTIC_SCAN_RANGE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_AL_OPTIMISTIC_SCAN_RANGE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_OPTIMISTIC_SCAN_RANGE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_AL_OPTIMISTIC_LAMBDA_OVERLAP
void MLHWSimImplTestCase::testmonitorPoint_AL_OPTIMISTIC_LAMBDA_OVERLAP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04004U), AMB::MLHWSimBase::monitorPoint_AL_OPTIMISTIC_LAMBDA_OVERLAP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_OPTIMISTIC_LAMBDA_OVERLAP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_AL_OPTIMISTIC_LAMBDA_OVERLAP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_OPTIMISTIC_LAMBDA_OVERLAP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_AL_MODE
void MLHWSimImplTestCase::testmonitorPoint_AL_MODE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04005U), AMB::MLHWSimBase::monitorPoint_AL_MODE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_MODE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_AL_MODE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_MODE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_AL_DETECTION_P_GAIN
void MLHWSimImplTestCase::testmonitorPoint_AL_DETECTION_P_GAIN()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04006U), AMB::MLHWSimBase::monitorPoint_AL_DETECTION_P_GAIN);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_DETECTION_P_GAIN + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_AL_DETECTION_P_GAIN + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_DETECTION_P_GAIN + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_AL_LAST_LOCK_TEMPERATURE
void MLHWSimImplTestCase::testmonitorPoint_AL_LAST_LOCK_TEMPERATURE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04007U), AMB::MLHWSimBase::monitorPoint_AL_LAST_LOCK_TEMPERATURE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_LAST_LOCK_TEMPERATURE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_AL_LAST_LOCK_TEMPERATURE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_AL_LAST_LOCK_TEMPERATURE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LOCK_PROPORTIONAL_ENABLE
void MLHWSimImplTestCase::testmonitorPoint_LOCK_PROPORTIONAL_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0400EU), AMB::MLHWSimBase::monitorPoint_LOCK_PROPORTIONAL_ENABLE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LOCK_PROPORTIONAL_ENABLE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LOCK_PROPORTIONAL_ENABLE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LOCK_PROPORTIONAL_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LOCK_INTEGRATOR_ENABLE
void MLHWSimImplTestCase::testmonitorPoint_LOCK_INTEGRATOR_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0400FU), AMB::MLHWSimBase::monitorPoint_LOCK_INTEGRATOR_ENABLE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LOCK_INTEGRATOR_ENABLE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LOCK_INTEGRATOR_ENABLE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LOCK_INTEGRATOR_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LOCKMON_TOLERANCE
void MLHWSimImplTestCase::testmonitorPoint_LOCKMON_TOLERANCE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06000U), AMB::MLHWSimBase::monitorPoint_LOCKMON_TOLERANCE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LOCKMON_TOLERANCE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LOCKMON_TOLERANCE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LOCKMON_TOLERANCE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LOCKMON_TUNNEL
void MLHWSimImplTestCase::testmonitorPoint_LOCKMON_TUNNEL()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06001U), AMB::MLHWSimBase::monitorPoint_LOCKMON_TUNNEL);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LOCKMON_TUNNEL + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LOCKMON_TUNNEL + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LOCKMON_TUNNEL + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LOCKMON_SLOW_FILTA
void MLHWSimImplTestCase::testmonitorPoint_LOCKMON_SLOW_FILTA()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06002U), AMB::MLHWSimBase::monitorPoint_LOCKMON_SLOW_FILTA);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LOCKMON_SLOW_FILTA + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LOCKMON_SLOW_FILTA + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LOCKMON_SLOW_FILTA + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LOCKMON_FAST_FILTA
void MLHWSimImplTestCase::testmonitorPoint_LOCKMON_FAST_FILTA()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06003U), AMB::MLHWSimBase::monitorPoint_LOCKMON_FAST_FILTA);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LOCKMON_FAST_FILTA + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LOCKMON_FAST_FILTA + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LOCKMON_FAST_FILTA + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LOCKMON_ENABLE
void MLHWSimImplTestCase::testmonitorPoint_LOCKMON_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06004U), AMB::MLHWSimBase::monitorPoint_LOCKMON_ENABLE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LOCKMON_ENABLE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LOCKMON_ENABLE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LOCKMON_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LOCKMON_UNLOCK_DETECT_THRESH
void MLHWSimImplTestCase::testmonitorPoint_LOCKMON_UNLOCK_DETECT_THRESH()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06005U), AMB::MLHWSimBase::monitorPoint_LOCKMON_UNLOCK_DETECT_THRESH);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LOCKMON_UNLOCK_DETECT_THRESH + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LOCKMON_UNLOCK_DETECT_THRESH + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LOCKMON_UNLOCK_DETECT_THRESH + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_LOCKMON_NOMINAL_FLUO_LEVEL
void MLHWSimImplTestCase::testmonitorPoint_LOCKMON_NOMINAL_FLUO_LEVEL()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06006U), AMB::MLHWSimBase::monitorPoint_LOCKMON_NOMINAL_FLUO_LEVEL);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LOCKMON_NOMINAL_FLUO_LEVEL + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_LOCKMON_NOMINAL_FLUO_LEVEL + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_LOCKMON_NOMINAL_FLUO_LEVEL + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PM_SUPPLY_VOLTAGE
void MLHWSimImplTestCase::testmonitorPoint_PM_SUPPLY_VOLTAGE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06200U), AMB::MLHWSimBase::monitorPoint_PM_SUPPLY_VOLTAGE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PM_SUPPLY_VOLTAGE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PM_SUPPLY_VOLTAGE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PM_SUPPLY_VOLTAGE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PM_SUPPLY_ENABLE
void MLHWSimImplTestCase::testmonitorPoint_PM_SUPPLY_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06201U), AMB::MLHWSimBase::monitorPoint_PM_SUPPLY_ENABLE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PM_SUPPLY_ENABLE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PM_SUPPLY_ENABLE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PM_SUPPLY_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PM_AC_GAIN
void MLHWSimImplTestCase::testmonitorPoint_PM_AC_GAIN()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06202U), AMB::MLHWSimBase::monitorPoint_PM_AC_GAIN);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PM_AC_GAIN + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PM_AC_GAIN + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PM_AC_GAIN + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PM_DC_GAIN
void MLHWSimImplTestCase::testmonitorPoint_PM_DC_GAIN()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06203U), AMB::MLHWSimBase::monitorPoint_PM_DC_GAIN);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PM_DC_GAIN + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PM_DC_GAIN + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PM_DC_GAIN + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_RED_PD_RESPONSIVITY
void MLHWSimImplTestCase::testmonitorPoint_RED_PD_RESPONSIVITY()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06300U), AMB::MLHWSimBase::monitorPoint_RED_PD_RESPONSIVITY);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_RED_PD_RESPONSIVITY + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_RED_PD_RESPONSIVITY + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_RED_PD_RESPONSIVITY + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_IR_PD_RESPONSIVITY
void MLHWSimImplTestCase::testmonitorPoint_IR_PD_RESPONSIVITY()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06301U), AMB::MLHWSimBase::monitorPoint_IR_PD_RESPONSIVITY);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_IR_PD_RESPONSIVITY + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_IR_PD_RESPONSIVITY + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_IR_PD_RESPONSIVITY + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_INTERLOCK_BYPASS_ENABLE
void MLHWSimImplTestCase::testmonitorPoint_INTERLOCK_BYPASS_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A061U), AMB::MLHWSimBase::monitorPoint_INTERLOCK_BYPASS_ENABLE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_INTERLOCK_BYPASS_ENABLE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_INTERLOCK_BYPASS_ENABLE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_INTERLOCK_BYPASS_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PZT_RANGE_CONTROL_ENABLE
void MLHWSimImplTestCase::testmonitorPoint_PZT_RANGE_CONTROL_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06810U), AMB::MLHWSimBase::monitorPoint_PZT_RANGE_CONTROL_ENABLE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PZT_RANGE_CONTROL_ENABLE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PZT_RANGE_CONTROL_ENABLE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PZT_RANGE_CONTROL_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PZT_RANGE_CONTROL_V_MIN
void MLHWSimImplTestCase::testmonitorPoint_PZT_RANGE_CONTROL_V_MIN()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06811U), AMB::MLHWSimBase::monitorPoint_PZT_RANGE_CONTROL_V_MIN);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PZT_RANGE_CONTROL_V_MIN + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PZT_RANGE_CONTROL_V_MIN + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PZT_RANGE_CONTROL_V_MIN + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PZT_RANGE_CONTROL_V_MAX
void MLHWSimImplTestCase::testmonitorPoint_PZT_RANGE_CONTROL_V_MAX()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06812U), AMB::MLHWSimBase::monitorPoint_PZT_RANGE_CONTROL_V_MAX);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PZT_RANGE_CONTROL_V_MAX + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PZT_RANGE_CONTROL_V_MAX + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PZT_RANGE_CONTROL_V_MAX + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_RB_CELL_TEMP_SETP
void MLHWSimImplTestCase::testmonitorPoint_RB_CELL_TEMP_SETP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06900U), AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMP_SETP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMP_SETP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMP_SETP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMP_SETP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_RB_CELL_TEMP_CTRL_ENABLE
void MLHWSimImplTestCase::testmonitorPoint_RB_CELL_TEMP_CTRL_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06901U), AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMP_CTRL_ENABLE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMP_CTRL_ENABLE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMP_CTRL_ENABLE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMP_CTRL_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_RB_CELL_TEMPMON_ABS_ERR
void MLHWSimImplTestCase::testmonitorPoint_RB_CELL_TEMPMON_ABS_ERR()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06902U), AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_ABS_ERR);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_ABS_ERR + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_ABS_ERR + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_ABS_ERR + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_RB_CELL_TIP_TEMPMON_ABS_ERR
void MLHWSimImplTestCase::testmonitorPoint_RB_CELL_TIP_TEMPMON_ABS_ERR()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06903U), AMB::MLHWSimBase::monitorPoint_RB_CELL_TIP_TEMPMON_ABS_ERR);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_RB_CELL_TIP_TEMPMON_ABS_ERR + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_RB_CELL_TIP_TEMPMON_ABS_ERR + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_RB_CELL_TIP_TEMPMON_ABS_ERR + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_RB_CELL_TEMPMON_TUNNEL
void MLHWSimImplTestCase::testmonitorPoint_RB_CELL_TEMPMON_TUNNEL()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06904U), AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_TUNNEL);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_TUNNEL + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_TUNNEL + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_TUNNEL + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_RB_CELL_TEMPMON_STABLE_TIME
void MLHWSimImplTestCase::testmonitorPoint_RB_CELL_TEMPMON_STABLE_TIME()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06905U), AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_STABLE_TIME);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_STABLE_TIME + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_STABLE_TIME + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_STABLE_TIME + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_RB_CELL_TEMPMON_SLOW_FILTA
void MLHWSimImplTestCase::testmonitorPoint_RB_CELL_TEMPMON_SLOW_FILTA()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06906U), AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_SLOW_FILTA);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_SLOW_FILTA + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_SLOW_FILTA + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_SLOW_FILTA + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_RB_CELL_TEMPMON_FAST_FILTA
void MLHWSimImplTestCase::testmonitorPoint_RB_CELL_TEMPMON_FAST_FILTA()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06907U), AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_FAST_FILTA);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_FAST_FILTA + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_FAST_FILTA + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_FAST_FILTA + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_RB_CELL_TEMPMON_TIMEOUT
void MLHWSimImplTestCase::testmonitorPoint_RB_CELL_TEMPMON_TIMEOUT()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06908U), AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_TIMEOUT);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_TIMEOUT + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_TIMEOUT + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_TIMEOUT + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_RB_CELL_TEMPMON_ENABLE
void MLHWSimImplTestCase::testmonitorPoint_RB_CELL_TEMPMON_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06909U), AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_ENABLE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_ENABLE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_ENABLE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_RB_CELL_TEMPMON_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PPLN_TEMP_SETP
void MLHWSimImplTestCase::testmonitorPoint_PPLN_TEMP_SETP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x07000U), AMB::MLHWSimBase::monitorPoint_PPLN_TEMP_SETP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_TEMP_SETP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PPLN_TEMP_SETP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_TEMP_SETP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PPLN_TEMP_CTRL_ENABLE
void MLHWSimImplTestCase::testmonitorPoint_PPLN_TEMP_CTRL_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x07001U), AMB::MLHWSimBase::monitorPoint_PPLN_TEMP_CTRL_ENABLE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_TEMP_CTRL_ENABLE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PPLN_TEMP_CTRL_ENABLE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_TEMP_CTRL_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PPLN_TEMPMON_ABS_ERR
void MLHWSimImplTestCase::testmonitorPoint_PPLN_TEMPMON_ABS_ERR()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x07010U), AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_ABS_ERR);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_ABS_ERR + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_ABS_ERR + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_ABS_ERR + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PPLN_TEMPMON_TUNNEL
void MLHWSimImplTestCase::testmonitorPoint_PPLN_TEMPMON_TUNNEL()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x07011U), AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_TUNNEL);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_TUNNEL + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_TUNNEL + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_TUNNEL + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PPLN_TEMPMON_STABLE_TIME
void MLHWSimImplTestCase::testmonitorPoint_PPLN_TEMPMON_STABLE_TIME()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x07012U), AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_STABLE_TIME);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_STABLE_TIME + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_STABLE_TIME + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_STABLE_TIME + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PPLN_TEMPMON_SLOW_FILTA
void MLHWSimImplTestCase::testmonitorPoint_PPLN_TEMPMON_SLOW_FILTA()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x07013U), AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_SLOW_FILTA);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_SLOW_FILTA + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_SLOW_FILTA + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_SLOW_FILTA + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PPLN_TEMPMON_FAST_FILTA
void MLHWSimImplTestCase::testmonitorPoint_PPLN_TEMPMON_FAST_FILTA()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x07014U), AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_FAST_FILTA);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_FAST_FILTA + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_FAST_FILTA + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_FAST_FILTA + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PPLN_TEMPMON_TIMEOUT
void MLHWSimImplTestCase::testmonitorPoint_PPLN_TEMPMON_TIMEOUT()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x07015U), AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_TIMEOUT);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_TIMEOUT + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_TIMEOUT + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_TIMEOUT + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PPLN_TEMPMON_ENABLE
void MLHWSimImplTestCase::testmonitorPoint_PPLN_TEMPMON_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x07016U), AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_ENABLE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_ENABLE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_ENABLE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_TEMPMON_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PPLN_EFFICIENCY_CTRL_ENABLE
void MLHWSimImplTestCase::testmonitorPoint_PPLN_EFFICIENCY_CTRL_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x07018U), AMB::MLHWSimBase::monitorPoint_PPLN_EFFICIENCY_CTRL_ENABLE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_EFFICIENCY_CTRL_ENABLE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PPLN_EFFICIENCY_CTRL_ENABLE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_EFFICIENCY_CTRL_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PPLN_EFFICIENCY_CTRL_STEP
void MLHWSimImplTestCase::testmonitorPoint_PPLN_EFFICIENCY_CTRL_STEP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x07019U), AMB::MLHWSimBase::monitorPoint_PPLN_EFFICIENCY_CTRL_STEP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_EFFICIENCY_CTRL_STEP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PPLN_EFFICIENCY_CTRL_STEP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_EFFICIENCY_CTRL_STEP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PPLN_OPT_START_TEMP
void MLHWSimImplTestCase::testmonitorPoint_PPLN_OPT_START_TEMP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A050U), AMB::MLHWSimBase::monitorPoint_PPLN_OPT_START_TEMP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_OPT_START_TEMP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PPLN_OPT_START_TEMP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_OPT_START_TEMP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PPLN_OPT_STOP_TEMP
void MLHWSimImplTestCase::testmonitorPoint_PPLN_OPT_STOP_TEMP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A051U), AMB::MLHWSimBase::monitorPoint_PPLN_OPT_STOP_TEMP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_OPT_STOP_TEMP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PPLN_OPT_STOP_TEMP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_OPT_STOP_TEMP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PPLN_OPT_TEMP_STEP
void MLHWSimImplTestCase::testmonitorPoint_PPLN_OPT_TEMP_STEP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A052U), AMB::MLHWSimBase::monitorPoint_PPLN_OPT_TEMP_STEP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_OPT_TEMP_STEP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PPLN_OPT_TEMP_STEP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_OPT_TEMP_STEP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PPLN_REDPWR_MON_THRESHOLD
void MLHWSimImplTestCase::testmonitorPoint_PPLN_REDPWR_MON_THRESHOLD()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A055U), AMB::MLHWSimBase::monitorPoint_PPLN_REDPWR_MON_THRESHOLD);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_REDPWR_MON_THRESHOLD + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PPLN_REDPWR_MON_THRESHOLD + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_REDPWR_MON_THRESHOLD + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PPLN_MINIMUM_IR_POWER
void MLHWSimImplTestCase::testmonitorPoint_PPLN_MINIMUM_IR_POWER()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A056U), AMB::MLHWSimBase::monitorPoint_PPLN_MINIMUM_IR_POWER);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_MINIMUM_IR_POWER + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PPLN_MINIMUM_IR_POWER + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_MINIMUM_IR_POWER + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PPLN_MINIMUM_RED_POWER
void MLHWSimImplTestCase::testmonitorPoint_PPLN_MINIMUM_RED_POWER()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A057U), AMB::MLHWSimBase::monitorPoint_PPLN_MINIMUM_RED_POWER);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_MINIMUM_RED_POWER + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PPLN_MINIMUM_RED_POWER + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PPLN_MINIMUM_RED_POWER + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_MIN_THRESHOLD
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_MIN_THRESHOLD()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x08000U), AMB::MLHWSimBase::monitorPoint_PEAKS_MIN_THRESHOLD);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_MIN_THRESHOLD + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_MIN_THRESHOLD + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_MIN_THRESHOLD + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_MAX_THRESHOLD
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_MAX_THRESHOLD()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x08001U), AMB::MLHWSimBase::monitorPoint_PEAKS_MAX_THRESHOLD);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_MAX_THRESHOLD + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_MAX_THRESHOLD + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_MAX_THRESHOLD + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_THRESHOLD_INCREMENT
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_THRESHOLD_INCREMENT()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x08003U), AMB::MLHWSimBase::monitorPoint_PEAKS_THRESHOLD_INCREMENT);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_THRESHOLD_INCREMENT + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_THRESHOLD_INCREMENT + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_THRESHOLD_INCREMENT + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_PEAKS_MIN_FLUO_LEVEL
void MLHWSimImplTestCase::testmonitorPoint_PEAKS_MIN_FLUO_LEVEL()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x08004U), AMB::MLHWSimBase::monitorPoint_PEAKS_MIN_FLUO_LEVEL);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_MIN_FLUO_LEVEL + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_PEAKS_MIN_FLUO_LEVEL + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_PEAKS_MIN_FLUO_LEVEL + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SCAN_START_TEMP
void MLHWSimImplTestCase::testmonitorPoint_SCAN_START_TEMP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x09000U), AMB::MLHWSimBase::monitorPoint_SCAN_START_TEMP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SCAN_START_TEMP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SCAN_START_TEMP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SCAN_START_TEMP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SCAN_STOP_TEMP
void MLHWSimImplTestCase::testmonitorPoint_SCAN_STOP_TEMP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x09001U), AMB::MLHWSimBase::monitorPoint_SCAN_STOP_TEMP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SCAN_STOP_TEMP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SCAN_STOP_TEMP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SCAN_STOP_TEMP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SCAN_PZT_START_VOLTAGE
void MLHWSimImplTestCase::testmonitorPoint_SCAN_PZT_START_VOLTAGE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x09002U), AMB::MLHWSimBase::monitorPoint_SCAN_PZT_START_VOLTAGE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SCAN_PZT_START_VOLTAGE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SCAN_PZT_START_VOLTAGE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SCAN_PZT_START_VOLTAGE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SCAN_PZT_STOP_VOLTAGE
void MLHWSimImplTestCase::testmonitorPoint_SCAN_PZT_STOP_VOLTAGE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x09003U), AMB::MLHWSimBase::monitorPoint_SCAN_PZT_STOP_VOLTAGE);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SCAN_PZT_STOP_VOLTAGE + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SCAN_PZT_STOP_VOLTAGE + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SCAN_PZT_STOP_VOLTAGE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SCAN_PZT_DELAY
void MLHWSimImplTestCase::testmonitorPoint_SCAN_PZT_DELAY()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x09004U), AMB::MLHWSimBase::monitorPoint_SCAN_PZT_DELAY);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SCAN_PZT_DELAY + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SCAN_PZT_DELAY + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SCAN_PZT_DELAY + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SCAN_PZT_PERIOD
void MLHWSimImplTestCase::testmonitorPoint_SCAN_PZT_PERIOD()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x09005U), AMB::MLHWSimBase::monitorPoint_SCAN_PZT_PERIOD);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SCAN_PZT_PERIOD + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SCAN_PZT_PERIOD + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SCAN_PZT_PERIOD + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testmonitorPoint_SCAN_DELTA_LAMBDA_OVERLAP
void MLHWSimImplTestCase::testmonitorPoint_SCAN_DELTA_LAMBDA_OVERLAP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x09006U), AMB::MLHWSimBase::monitorPoint_SCAN_DELTA_LAMBDA_OVERLAP);

    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SCAN_DELTA_LAMBDA_OVERLAP + baseAddress_m));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::monitorPoint_SCAN_DELTA_LAMBDA_OVERLAP + baseAddress_m, createVector(size)));

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::monitorPoint_SCAN_DELTA_LAMBDA_OVERLAP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

/// Testing specific control points

///  testSET_SAVE_ALL_PARAMS
void MLHWSimImplTestCase::testSET_SAVE_ALL_PARAMS()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(5U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01000U), AMB::MLHWSimBase::controlPoint_SAVE_ALL_PARAMS);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_SAVE_ALL_PARAMS + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_SAVE_ALL_PARAMS + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_SAVE_PARAM_BY_ID
void MLHWSimImplTestCase::testSET_SAVE_PARAM_BY_ID()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(6U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01001U), AMB::MLHWSimBase::controlPoint_SAVE_PARAM_BY_ID);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_SAVE_PARAM_BY_ID + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_SAVE_PARAM_BY_ID + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LOAD_ALL_PARAMS
void MLHWSimImplTestCase::testSET_LOAD_ALL_PARAMS()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01002U), AMB::MLHWSimBase::controlPoint_LOAD_ALL_PARAMS);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LOAD_ALL_PARAMS + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LOAD_ALL_PARAMS + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LOAD_PARAM_BY_ID
void MLHWSimImplTestCase::testSET_LOAD_PARAM_BY_ID()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01003U), AMB::MLHWSimBase::controlPoint_LOAD_PARAM_BY_ID);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LOAD_PARAM_BY_ID + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LOAD_PARAM_BY_ID + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_RESET_LASER_REFL_ALARM
void MLHWSimImplTestCase::testSET_RESET_LASER_REFL_ALARM()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01004U), AMB::MLHWSimBase::controlPoint_RESET_LASER_REFL_ALARM);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_RESET_LASER_REFL_ALARM + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_RESET_LASER_REFL_ALARM + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PZT_SWEEP_START
void MLHWSimImplTestCase::testSET_PZT_SWEEP_START()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01007U), AMB::MLHWSimBase::controlPoint_PZT_SWEEP_START);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PZT_SWEEP_START + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PZT_SWEEP_START + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PZT_SWEEP_STOP
void MLHWSimImplTestCase::testSET_PZT_SWEEP_STOP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01008U), AMB::MLHWSimBase::controlPoint_PZT_SWEEP_STOP);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PZT_SWEEP_STOP + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PZT_SWEEP_STOP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PZT_SWEEP_RESET
void MLHWSimImplTestCase::testSET_PZT_SWEEP_RESET()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01009U), AMB::MLHWSimBase::controlPoint_PZT_SWEEP_RESET);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PZT_SWEEP_RESET + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PZT_SWEEP_RESET + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_ORM_SAVE_PARAMS
void MLHWSimImplTestCase::testSET_ORM_SAVE_PARAMS()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(5U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0100CU), AMB::MLHWSimBase::controlPoint_ORM_SAVE_PARAMS);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_ORM_SAVE_PARAMS + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_ORM_SAVE_PARAMS + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_ORM_LOAD_PARAMS
void MLHWSimImplTestCase::testSET_ORM_LOAD_PARAMS()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0100DU), AMB::MLHWSimBase::controlPoint_ORM_LOAD_PARAMS);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_ORM_LOAD_PARAMS + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_ORM_LOAD_PARAMS + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_MANUAL_MODE_REQUEST
void MLHWSimImplTestCase::testSET_MANUAL_MODE_REQUEST()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0100EU), AMB::MLHWSimBase::controlPoint_MANUAL_MODE_REQUEST);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_MANUAL_MODE_REQUEST + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_MANUAL_MODE_REQUEST + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_STANDBY_MODE_REQUEST
void MLHWSimImplTestCase::testSET_STANDBY_MODE_REQUEST()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0100FU), AMB::MLHWSimBase::controlPoint_STANDBY_MODE_REQUEST);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_STANDBY_MODE_REQUEST + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_STANDBY_MODE_REQUEST + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_AUTOLOCK_REQUEST
void MLHWSimImplTestCase::testSET_AUTOLOCK_REQUEST()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01010U), AMB::MLHWSimBase::controlPoint_AUTOLOCK_REQUEST);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_AUTOLOCK_REQUEST + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_AUTOLOCK_REQUEST + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PPLN_OPTIMIZATION_SCAN_START
void MLHWSimImplTestCase::testSET_PPLN_OPTIMIZATION_SCAN_START()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01011U), AMB::MLHWSimBase::controlPoint_PPLN_OPTIMIZATION_SCAN_START);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PPLN_OPTIMIZATION_SCAN_START + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PPLN_OPTIMIZATION_SCAN_START + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PPLN_OPTIMIZATION_SCAN_STOP
void MLHWSimImplTestCase::testSET_PPLN_OPTIMIZATION_SCAN_STOP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01012U), AMB::MLHWSimBase::controlPoint_PPLN_OPTIMIZATION_SCAN_STOP);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PPLN_OPTIMIZATION_SCAN_STOP + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PPLN_OPTIMIZATION_SCAN_STOP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LOCK_MONITORING_RESET_TRIGGER
void MLHWSimImplTestCase::testSET_LOCK_MONITORING_RESET_TRIGGER()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01013U), AMB::MLHWSimBase::controlPoint_LOCK_MONITORING_RESET_TRIGGER);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LOCK_MONITORING_RESET_TRIGGER + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LOCK_MONITORING_RESET_TRIGGER + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_CLEAR_WARNING_LED
void MLHWSimImplTestCase::testSET_CLEAR_WARNING_LED()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01014U), AMB::MLHWSimBase::controlPoint_CLEAR_WARNING_LED);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_CLEAR_WARNING_LED + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_CLEAR_WARNING_LED + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PPLN_EFFICIENCY_CTRL_START
void MLHWSimImplTestCase::testSET_PPLN_EFFICIENCY_CTRL_START()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01015U), AMB::MLHWSimBase::controlPoint_PPLN_EFFICIENCY_CTRL_START);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PPLN_EFFICIENCY_CTRL_START + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PPLN_EFFICIENCY_CTRL_START + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PPLN_EFFICIENCY_CTRL_STOP
void MLHWSimImplTestCase::testSET_PPLN_EFFICIENCY_CTRL_STOP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01016U), AMB::MLHWSimBase::controlPoint_PPLN_EFFICIENCY_CTRL_STOP);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PPLN_EFFICIENCY_CTRL_STOP + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PPLN_EFFICIENCY_CTRL_STOP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_SYSTEM_STICKY_ERROR_STATUS
void MLHWSimImplTestCase::testSET_SYSTEM_STICKY_ERROR_STATUS()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01105U), AMB::MLHWSimBase::controlPoint_SYSTEM_STICKY_ERROR_STATUS);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_SYSTEM_STICKY_ERROR_STATUS + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_SYSTEM_STICKY_ERROR_STATUS + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_SYSTEM_LM_SERIAL_NUMBER
void MLHWSimImplTestCase::testSET_SYSTEM_LM_SERIAL_NUMBER()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(8U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01200U), AMB::MLHWSimBase::controlPoint_SYSTEM_LM_SERIAL_NUMBER);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_SYSTEM_LM_SERIAL_NUMBER + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_SYSTEM_LM_SERIAL_NUMBER + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_SYSTEM_ORM_SERIAL_NUMBER
void MLHWSimImplTestCase::testSET_SYSTEM_ORM_SERIAL_NUMBER()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(8U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01201U), AMB::MLHWSimBase::controlPoint_SYSTEM_ORM_SERIAL_NUMBER);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_SYSTEM_ORM_SERIAL_NUMBER + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_SYSTEM_ORM_SERIAL_NUMBER + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_SYSTEM_STARTUP_MODE
void MLHWSimImplTestCase::testSET_SYSTEM_STARTUP_MODE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01800U), AMB::MLHWSimBase::controlPoint_SYSTEM_STARTUP_MODE);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_SYSTEM_STARTUP_MODE + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_SYSTEM_STARTUP_MODE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_SYSTEM_STARTUP_PARAM_SET
void MLHWSimImplTestCase::testSET_SYSTEM_STARTUP_PARAM_SET()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01810U), AMB::MLHWSimBase::controlPoint_SYSTEM_STARTUP_PARAM_SET);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_SYSTEM_STARTUP_PARAM_SET + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_SYSTEM_STARTUP_PARAM_SET + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LL_GPIO
void MLHWSimImplTestCase::testSET_LL_GPIO()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02000U), AMB::MLHWSimBase::controlPoint_LL_GPIO);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LL_GPIO + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LL_GPIO + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LL_IOX
void MLHWSimImplTestCase::testSET_LL_IOX()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02001U), AMB::MLHWSimBase::controlPoint_LL_IOX);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LL_IOX + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LL_IOX + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LL_ORM_IOX
void MLHWSimImplTestCase::testSET_LL_ORM_IOX()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02002U), AMB::MLHWSimBase::controlPoint_LL_ORM_IOX);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LL_ORM_IOX + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LL_ORM_IOX + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LL_DAC_PIEZO_SETP
void MLHWSimImplTestCase::testSET_LL_DAC_PIEZO_SETP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02010U), AMB::MLHWSimBase::controlPoint_LL_DAC_PIEZO_SETP);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LL_DAC_PIEZO_SETP + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LL_DAC_PIEZO_SETP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LL_DAC_RAMP_TRIG_SETP
void MLHWSimImplTestCase::testSET_LL_DAC_RAMP_TRIG_SETP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02011U), AMB::MLHWSimBase::controlPoint_LL_DAC_RAMP_TRIG_SETP);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LL_DAC_RAMP_TRIG_SETP + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LL_DAC_RAMP_TRIG_SETP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LL_DAC_RAMP_SLOPE
void MLHWSimImplTestCase::testSET_LL_DAC_RAMP_SLOPE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02012U), AMB::MLHWSimBase::controlPoint_LL_DAC_RAMP_SLOPE);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LL_DAC_RAMP_SLOPE + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LL_DAC_RAMP_SLOPE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LL_DAC_PID_P_GAIN_SETP
void MLHWSimImplTestCase::testSET_LL_DAC_PID_P_GAIN_SETP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02013U), AMB::MLHWSimBase::controlPoint_LL_DAC_PID_P_GAIN_SETP);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LL_DAC_PID_P_GAIN_SETP + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LL_DAC_PID_P_GAIN_SETP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LL_DAC_PID_OFFSET_CORR
void MLHWSimImplTestCase::testSET_LL_DAC_PID_OFFSET_CORR()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02014U), AMB::MLHWSimBase::controlPoint_LL_DAC_PID_OFFSET_CORR);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LL_DAC_PID_OFFSET_CORR + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LL_DAC_PID_OFFSET_CORR + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LL_DAC_TRIG_ERROR_SETP
void MLHWSimImplTestCase::testSET_LL_DAC_TRIG_ERROR_SETP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02015U), AMB::MLHWSimBase::controlPoint_LL_DAC_TRIG_ERROR_SETP);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LL_DAC_TRIG_ERROR_SETP + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LL_DAC_TRIG_ERROR_SETP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LL_DAC_RIN_LOCK_SETP
void MLHWSimImplTestCase::testSET_LL_DAC_RIN_LOCK_SETP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02016U), AMB::MLHWSimBase::controlPoint_LL_DAC_RIN_LOCK_SETP);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LL_DAC_RIN_LOCK_SETP + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LL_DAC_RIN_LOCK_SETP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LL_DAC_RIN_DC_OFFSET_P
void MLHWSimImplTestCase::testSET_LL_DAC_RIN_DC_OFFSET_P()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02017U), AMB::MLHWSimBase::controlPoint_LL_DAC_RIN_DC_OFFSET_P);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LL_DAC_RIN_DC_OFFSET_P + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LL_DAC_RIN_DC_OFFSET_P + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LL_SELECTOR_ADC_MUX
void MLHWSimImplTestCase::testSET_LL_SELECTOR_ADC_MUX()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02030U), AMB::MLHWSimBase::controlPoint_LL_SELECTOR_ADC_MUX);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LL_SELECTOR_ADC_MUX + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LL_SELECTOR_ADC_MUX + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LL_SELECTOR_PID_TAU
void MLHWSimImplTestCase::testSET_LL_SELECTOR_PID_TAU()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02031U), AMB::MLHWSimBase::controlPoint_LL_SELECTOR_PID_TAU);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LL_SELECTOR_PID_TAU + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LL_SELECTOR_PID_TAU + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LL_DIGIPOT_DDS_MOD_ADJ
void MLHWSimImplTestCase::testSET_LL_DIGIPOT_DDS_MOD_ADJ()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02040U), AMB::MLHWSimBase::controlPoint_LL_DIGIPOT_DDS_MOD_ADJ);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LL_DIGIPOT_DDS_MOD_ADJ + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LL_DIGIPOT_DDS_MOD_ADJ + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LL_DIGIPOT_DDS_LO_ADJ
void MLHWSimImplTestCase::testSET_LL_DIGIPOT_DDS_LO_ADJ()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02041U), AMB::MLHWSimBase::controlPoint_LL_DIGIPOT_DDS_LO_ADJ);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LL_DIGIPOT_DDS_LO_ADJ + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LL_DIGIPOT_DDS_LO_ADJ + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LL_DIGIPOT_RIN_POWER_MON_GAIN
void MLHWSimImplTestCase::testSET_LL_DIGIPOT_RIN_POWER_MON_GAIN()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02042U), AMB::MLHWSimBase::controlPoint_LL_DIGIPOT_RIN_POWER_MON_GAIN);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LL_DIGIPOT_RIN_POWER_MON_GAIN + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LL_DIGIPOT_RIN_POWER_MON_GAIN + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LL_DIGIPOT_RIN_REFERENCE_MON_GAIN
void MLHWSimImplTestCase::testSET_LL_DIGIPOT_RIN_REFERENCE_MON_GAIN()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02043U), AMB::MLHWSimBase::controlPoint_LL_DIGIPOT_RIN_REFERENCE_MON_GAIN);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LL_DIGIPOT_RIN_REFERENCE_MON_GAIN + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LL_DIGIPOT_RIN_REFERENCE_MON_GAIN + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LL_DIGIPOT_ORM_PPLN_GAIN
void MLHWSimImplTestCase::testSET_LL_DIGIPOT_ORM_PPLN_GAIN()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02044U), AMB::MLHWSimBase::controlPoint_LL_DIGIPOT_ORM_PPLN_GAIN);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LL_DIGIPOT_ORM_PPLN_GAIN + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LL_DIGIPOT_ORM_PPLN_GAIN + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LL_DIGIPOT_ORM_CELL_GAIN
void MLHWSimImplTestCase::testSET_LL_DIGIPOT_ORM_CELL_GAIN()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02045U), AMB::MLHWSimBase::controlPoint_LL_DIGIPOT_ORM_CELL_GAIN);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LL_DIGIPOT_ORM_CELL_GAIN + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LL_DIGIPOT_ORM_CELL_GAIN + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LL_DDS_OSCILLATOR_LOCAL_OSCILLATOR
void MLHWSimImplTestCase::testSET_LL_DDS_OSCILLATOR_LOCAL_OSCILLATOR()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02050U), AMB::MLHWSimBase::controlPoint_LL_DDS_OSCILLATOR_LOCAL_OSCILLATOR);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LL_DDS_OSCILLATOR_LOCAL_OSCILLATOR + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LL_DDS_OSCILLATOR_LOCAL_OSCILLATOR + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LL_DDS_OSCILLATOR_PHASE_MODULATOR
void MLHWSimImplTestCase::testSET_LL_DDS_OSCILLATOR_PHASE_MODULATOR()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02051U), AMB::MLHWSimBase::controlPoint_LL_DDS_OSCILLATOR_PHASE_MODULATOR);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LL_DDS_OSCILLATOR_PHASE_MODULATOR + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LL_DDS_OSCILLATOR_PHASE_MODULATOR + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LASER_PZT_TUNING_COEFF
void MLHWSimImplTestCase::testSET_LASER_PZT_TUNING_COEFF()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03000U), AMB::MLHWSimBase::controlPoint_LASER_PZT_TUNING_COEFF);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LASER_PZT_TUNING_COEFF + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LASER_PZT_TUNING_COEFF + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LASER_T_TUNING_COEFF
void MLHWSimImplTestCase::testSET_LASER_T_TUNING_COEFF()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03001U), AMB::MLHWSimBase::controlPoint_LASER_T_TUNING_COEFF);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LASER_T_TUNING_COEFF + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LASER_T_TUNING_COEFF + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LASER_T
void MLHWSimImplTestCase::testSET_LASER_T()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03002U), AMB::MLHWSimBase::controlPoint_LASER_T);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LASER_T + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LASER_T + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LASER_PWR
void MLHWSimImplTestCase::testSET_LASER_PWR()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03003U), AMB::MLHWSimBase::controlPoint_LASER_PWR);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LASER_PWR + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LASER_PWR + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LASER_PZT_LEVEL
void MLHWSimImplTestCase::testSET_LASER_PZT_LEVEL()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03004U), AMB::MLHWSimBase::controlPoint_LASER_PZT_LEVEL);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LASER_PZT_LEVEL + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LASER_PZT_LEVEL + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LASER_PWRAMP_ENABLE
void MLHWSimImplTestCase::testSET_LASER_PWRAMP_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03005U), AMB::MLHWSimBase::controlPoint_LASER_PWRAMP_ENABLE);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LASER_PWRAMP_ENABLE + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LASER_PWRAMP_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LASER_TEMP_CTRL_ENABLE
void MLHWSimImplTestCase::testSET_LASER_TEMP_CTRL_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03006U), AMB::MLHWSimBase::controlPoint_LASER_TEMP_CTRL_ENABLE);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LASER_TEMP_CTRL_ENABLE + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LASER_TEMP_CTRL_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LASER_TEMPMON_ABS_ERR
void MLHWSimImplTestCase::testSET_LASER_TEMPMON_ABS_ERR()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03100U), AMB::MLHWSimBase::controlPoint_LASER_TEMPMON_ABS_ERR);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LASER_TEMPMON_ABS_ERR + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LASER_TEMPMON_ABS_ERR + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LASER_TEMPMON_TUNNEL
void MLHWSimImplTestCase::testSET_LASER_TEMPMON_TUNNEL()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03101U), AMB::MLHWSimBase::controlPoint_LASER_TEMPMON_TUNNEL);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LASER_TEMPMON_TUNNEL + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LASER_TEMPMON_TUNNEL + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LASER_TEMPMON_STABLE_TIME
void MLHWSimImplTestCase::testSET_LASER_TEMPMON_STABLE_TIME()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03102U), AMB::MLHWSimBase::controlPoint_LASER_TEMPMON_STABLE_TIME);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LASER_TEMPMON_STABLE_TIME + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LASER_TEMPMON_STABLE_TIME + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LASER_TEMPMON_TIMEOUT
void MLHWSimImplTestCase::testSET_LASER_TEMPMON_TIMEOUT()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03103U), AMB::MLHWSimBase::controlPoint_LASER_TEMPMON_TIMEOUT);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LASER_TEMPMON_TIMEOUT + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LASER_TEMPMON_TIMEOUT + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LASER_TEMPMON_SLOW_FILTA
void MLHWSimImplTestCase::testSET_LASER_TEMPMON_SLOW_FILTA()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03104U), AMB::MLHWSimBase::controlPoint_LASER_TEMPMON_SLOW_FILTA);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LASER_TEMPMON_SLOW_FILTA + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LASER_TEMPMON_SLOW_FILTA + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LASER_TEMPMON_FAST_FILTA
void MLHWSimImplTestCase::testSET_LASER_TEMPMON_FAST_FILTA()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03105U), AMB::MLHWSimBase::controlPoint_LASER_TEMPMON_FAST_FILTA);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LASER_TEMPMON_FAST_FILTA + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LASER_TEMPMON_FAST_FILTA + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LASER_TEMPMON_ENABLE
void MLHWSimImplTestCase::testSET_LASER_TEMPMON_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03106U), AMB::MLHWSimBase::controlPoint_LASER_TEMPMON_ENABLE);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LASER_TEMPMON_ENABLE + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LASER_TEMPMON_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LASER_POWER_MON_TOLERANCE
void MLHWSimImplTestCase::testSET_LASER_POWER_MON_TOLERANCE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03108U), AMB::MLHWSimBase::controlPoint_LASER_POWER_MON_TOLERANCE);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LASER_POWER_MON_TOLERANCE + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LASER_POWER_MON_TOLERANCE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PZT_SWEEP_START_VOLTAGE
void MLHWSimImplTestCase::testSET_PZT_SWEEP_START_VOLTAGE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03200U), AMB::MLHWSimBase::controlPoint_PZT_SWEEP_START_VOLTAGE);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PZT_SWEEP_START_VOLTAGE + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PZT_SWEEP_START_VOLTAGE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PZT_SWEEP_STOP_VOLTAGE
void MLHWSimImplTestCase::testSET_PZT_SWEEP_STOP_VOLTAGE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03201U), AMB::MLHWSimBase::controlPoint_PZT_SWEEP_STOP_VOLTAGE);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PZT_SWEEP_STOP_VOLTAGE + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PZT_SWEEP_STOP_VOLTAGE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PZT_SWEEP_DELAY
void MLHWSimImplTestCase::testSET_PZT_SWEEP_DELAY()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03202U), AMB::MLHWSimBase::controlPoint_PZT_SWEEP_DELAY);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PZT_SWEEP_DELAY + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PZT_SWEEP_DELAY + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PZT_SWEEP_PERIOD
void MLHWSimImplTestCase::testSET_PZT_SWEEP_PERIOD()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03203U), AMB::MLHWSimBase::controlPoint_PZT_SWEEP_PERIOD);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PZT_SWEEP_PERIOD + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PZT_SWEEP_PERIOD + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PZT_SWEEP_PERIODIC
void MLHWSimImplTestCase::testSET_PZT_SWEEP_PERIODIC()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03204U), AMB::MLHWSimBase::controlPoint_PZT_SWEEP_PERIODIC);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PZT_SWEEP_PERIODIC + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PZT_SWEEP_PERIODIC + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PZT_SWEEP_FILTER
void MLHWSimImplTestCase::testSET_PZT_SWEEP_FILTER()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03205U), AMB::MLHWSimBase::controlPoint_PZT_SWEEP_FILTER);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PZT_SWEEP_FILTER + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PZT_SWEEP_FILTER + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_AL_TEMP_MIN
void MLHWSimImplTestCase::testSET_AL_TEMP_MIN()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04000U), AMB::MLHWSimBase::controlPoint_AL_TEMP_MIN);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_AL_TEMP_MIN + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_AL_TEMP_MIN + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_AL_TEMP_MAX
void MLHWSimImplTestCase::testSET_AL_TEMP_MAX()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04001U), AMB::MLHWSimBase::controlPoint_AL_TEMP_MAX);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_AL_TEMP_MAX + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_AL_TEMP_MAX + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_AL_START_TEMP
void MLHWSimImplTestCase::testSET_AL_START_TEMP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04002U), AMB::MLHWSimBase::controlPoint_AL_START_TEMP);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_AL_START_TEMP + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_AL_START_TEMP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_AL_OPTIMISTIC_SCAN_RANGE
void MLHWSimImplTestCase::testSET_AL_OPTIMISTIC_SCAN_RANGE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04003U), AMB::MLHWSimBase::controlPoint_AL_OPTIMISTIC_SCAN_RANGE);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_AL_OPTIMISTIC_SCAN_RANGE + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_AL_OPTIMISTIC_SCAN_RANGE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_AL_OPTIMISTIC_LAMBDA_OVERLAP
void MLHWSimImplTestCase::testSET_AL_OPTIMISTIC_LAMBDA_OVERLAP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04004U), AMB::MLHWSimBase::controlPoint_AL_OPTIMISTIC_LAMBDA_OVERLAP);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_AL_OPTIMISTIC_LAMBDA_OVERLAP + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_AL_OPTIMISTIC_LAMBDA_OVERLAP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_AL_MODE
void MLHWSimImplTestCase::testSET_AL_MODE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04005U), AMB::MLHWSimBase::controlPoint_AL_MODE);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_AL_MODE + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_AL_MODE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_AL_DETECTION_P_GAIN
void MLHWSimImplTestCase::testSET_AL_DETECTION_P_GAIN()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04006U), AMB::MLHWSimBase::controlPoint_AL_DETECTION_P_GAIN);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_AL_DETECTION_P_GAIN + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_AL_DETECTION_P_GAIN + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_AL_LAST_LOCK_TEMPERATURE
void MLHWSimImplTestCase::testSET_AL_LAST_LOCK_TEMPERATURE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04007U), AMB::MLHWSimBase::controlPoint_AL_LAST_LOCK_TEMPERATURE);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_AL_LAST_LOCK_TEMPERATURE + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_AL_LAST_LOCK_TEMPERATURE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LOCK_PROPORTIONAL_ENABLE
void MLHWSimImplTestCase::testSET_LOCK_PROPORTIONAL_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0400EU), AMB::MLHWSimBase::controlPoint_LOCK_PROPORTIONAL_ENABLE);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LOCK_PROPORTIONAL_ENABLE + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LOCK_PROPORTIONAL_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LOCK_INTEGRATOR_ENABLE
void MLHWSimImplTestCase::testSET_LOCK_INTEGRATOR_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0400FU), AMB::MLHWSimBase::controlPoint_LOCK_INTEGRATOR_ENABLE);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LOCK_INTEGRATOR_ENABLE + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LOCK_INTEGRATOR_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LOCKMON_TOLERANCE
void MLHWSimImplTestCase::testSET_LOCKMON_TOLERANCE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06000U), AMB::MLHWSimBase::controlPoint_LOCKMON_TOLERANCE);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LOCKMON_TOLERANCE + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LOCKMON_TOLERANCE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LOCKMON_TUNNEL
void MLHWSimImplTestCase::testSET_LOCKMON_TUNNEL()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06001U), AMB::MLHWSimBase::controlPoint_LOCKMON_TUNNEL);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LOCKMON_TUNNEL + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LOCKMON_TUNNEL + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LOCKMON_SLOW_FILTA
void MLHWSimImplTestCase::testSET_LOCKMON_SLOW_FILTA()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06002U), AMB::MLHWSimBase::controlPoint_LOCKMON_SLOW_FILTA);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LOCKMON_SLOW_FILTA + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LOCKMON_SLOW_FILTA + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LOCKMON_FAST_FILTA
void MLHWSimImplTestCase::testSET_LOCKMON_FAST_FILTA()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06003U), AMB::MLHWSimBase::controlPoint_LOCKMON_FAST_FILTA);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LOCKMON_FAST_FILTA + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LOCKMON_FAST_FILTA + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LOCKMON_ENABLE
void MLHWSimImplTestCase::testSET_LOCKMON_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06004U), AMB::MLHWSimBase::controlPoint_LOCKMON_ENABLE);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LOCKMON_ENABLE + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LOCKMON_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LOCKMON_UNLOCK_DETECT_THRESH
void MLHWSimImplTestCase::testSET_LOCKMON_UNLOCK_DETECT_THRESH()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06005U), AMB::MLHWSimBase::controlPoint_LOCKMON_UNLOCK_DETECT_THRESH);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LOCKMON_UNLOCK_DETECT_THRESH + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LOCKMON_UNLOCK_DETECT_THRESH + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_LOCKMON_NOMINAL_FLUO_LEVEL
void MLHWSimImplTestCase::testSET_LOCKMON_NOMINAL_FLUO_LEVEL()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06006U), AMB::MLHWSimBase::controlPoint_LOCKMON_NOMINAL_FLUO_LEVEL);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_LOCKMON_NOMINAL_FLUO_LEVEL + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_LOCKMON_NOMINAL_FLUO_LEVEL + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PM_SUPPLY_VOLTAGE
void MLHWSimImplTestCase::testSET_PM_SUPPLY_VOLTAGE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06200U), AMB::MLHWSimBase::controlPoint_PM_SUPPLY_VOLTAGE);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PM_SUPPLY_VOLTAGE + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PM_SUPPLY_VOLTAGE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PM_SUPPLY_ENABLE
void MLHWSimImplTestCase::testSET_PM_SUPPLY_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06201U), AMB::MLHWSimBase::controlPoint_PM_SUPPLY_ENABLE);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PM_SUPPLY_ENABLE + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PM_SUPPLY_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PM_AC_GAIN
void MLHWSimImplTestCase::testSET_PM_AC_GAIN()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06202U), AMB::MLHWSimBase::controlPoint_PM_AC_GAIN);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PM_AC_GAIN + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PM_AC_GAIN + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PM_DC_GAIN
void MLHWSimImplTestCase::testSET_PM_DC_GAIN()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06203U), AMB::MLHWSimBase::controlPoint_PM_DC_GAIN);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PM_DC_GAIN + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PM_DC_GAIN + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_RED_PD_RESPONSIVITY
void MLHWSimImplTestCase::testSET_RED_PD_RESPONSIVITY()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06300U), AMB::MLHWSimBase::controlPoint_RED_PD_RESPONSIVITY);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_RED_PD_RESPONSIVITY + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_RED_PD_RESPONSIVITY + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_IR_PD_RESPONSIVITY
void MLHWSimImplTestCase::testSET_IR_PD_RESPONSIVITY()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06301U), AMB::MLHWSimBase::controlPoint_IR_PD_RESPONSIVITY);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_IR_PD_RESPONSIVITY + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_IR_PD_RESPONSIVITY + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_INTERLOCK_BYPASS_ENABLE
void MLHWSimImplTestCase::testSET_INTERLOCK_BYPASS_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A061U), AMB::MLHWSimBase::controlPoint_INTERLOCK_BYPASS_ENABLE);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_INTERLOCK_BYPASS_ENABLE + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_INTERLOCK_BYPASS_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PZT_RANGE_CONTROL_ENABLE
void MLHWSimImplTestCase::testSET_PZT_RANGE_CONTROL_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06810U), AMB::MLHWSimBase::controlPoint_PZT_RANGE_CONTROL_ENABLE);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PZT_RANGE_CONTROL_ENABLE + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PZT_RANGE_CONTROL_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PZT_RANGE_CONTROL_V_MIN
void MLHWSimImplTestCase::testSET_PZT_RANGE_CONTROL_V_MIN()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06811U), AMB::MLHWSimBase::controlPoint_PZT_RANGE_CONTROL_V_MIN);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PZT_RANGE_CONTROL_V_MIN + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PZT_RANGE_CONTROL_V_MIN + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PZT_RANGE_CONTROL_V_MAX
void MLHWSimImplTestCase::testSET_PZT_RANGE_CONTROL_V_MAX()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06812U), AMB::MLHWSimBase::controlPoint_PZT_RANGE_CONTROL_V_MAX);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PZT_RANGE_CONTROL_V_MAX + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PZT_RANGE_CONTROL_V_MAX + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_RB_CELL_TEMP_SETP
void MLHWSimImplTestCase::testSET_RB_CELL_TEMP_SETP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06900U), AMB::MLHWSimBase::controlPoint_RB_CELL_TEMP_SETP);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_RB_CELL_TEMP_SETP + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_RB_CELL_TEMP_SETP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_RB_CELL_TEMP_CTRL_ENABLE
void MLHWSimImplTestCase::testSET_RB_CELL_TEMP_CTRL_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06901U), AMB::MLHWSimBase::controlPoint_RB_CELL_TEMP_CTRL_ENABLE);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_RB_CELL_TEMP_CTRL_ENABLE + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_RB_CELL_TEMP_CTRL_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_RB_CELL_TEMPMON_ABS_ERR
void MLHWSimImplTestCase::testSET_RB_CELL_TEMPMON_ABS_ERR()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06902U), AMB::MLHWSimBase::controlPoint_RB_CELL_TEMPMON_ABS_ERR);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_RB_CELL_TEMPMON_ABS_ERR + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_RB_CELL_TEMPMON_ABS_ERR + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_RB_CELL_TIP_TEMPMON_ABS_ERR
void MLHWSimImplTestCase::testSET_RB_CELL_TIP_TEMPMON_ABS_ERR()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06903U), AMB::MLHWSimBase::controlPoint_RB_CELL_TIP_TEMPMON_ABS_ERR);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_RB_CELL_TIP_TEMPMON_ABS_ERR + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_RB_CELL_TIP_TEMPMON_ABS_ERR + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_RB_CELL_TEMPMON_TUNNEL
void MLHWSimImplTestCase::testSET_RB_CELL_TEMPMON_TUNNEL()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06904U), AMB::MLHWSimBase::controlPoint_RB_CELL_TEMPMON_TUNNEL);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_RB_CELL_TEMPMON_TUNNEL + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_RB_CELL_TEMPMON_TUNNEL + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_RB_CELL_TEMPMON_STABLE_TIME
void MLHWSimImplTestCase::testSET_RB_CELL_TEMPMON_STABLE_TIME()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06905U), AMB::MLHWSimBase::controlPoint_RB_CELL_TEMPMON_STABLE_TIME);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_RB_CELL_TEMPMON_STABLE_TIME + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_RB_CELL_TEMPMON_STABLE_TIME + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_RB_CELL_TEMPMON_SLOW_FILTA
void MLHWSimImplTestCase::testSET_RB_CELL_TEMPMON_SLOW_FILTA()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06906U), AMB::MLHWSimBase::controlPoint_RB_CELL_TEMPMON_SLOW_FILTA);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_RB_CELL_TEMPMON_SLOW_FILTA + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_RB_CELL_TEMPMON_SLOW_FILTA + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_RB_CELL_TEMPMON_FAST_FILTA
void MLHWSimImplTestCase::testSET_RB_CELL_TEMPMON_FAST_FILTA()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06907U), AMB::MLHWSimBase::controlPoint_RB_CELL_TEMPMON_FAST_FILTA);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_RB_CELL_TEMPMON_FAST_FILTA + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_RB_CELL_TEMPMON_FAST_FILTA + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_RB_CELL_TEMPMON_TIMEOUT
void MLHWSimImplTestCase::testSET_RB_CELL_TEMPMON_TIMEOUT()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06908U), AMB::MLHWSimBase::controlPoint_RB_CELL_TEMPMON_TIMEOUT);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_RB_CELL_TEMPMON_TIMEOUT + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_RB_CELL_TEMPMON_TIMEOUT + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_RB_CELL_TEMPMON_ENABLE
void MLHWSimImplTestCase::testSET_RB_CELL_TEMPMON_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06909U), AMB::MLHWSimBase::controlPoint_RB_CELL_TEMPMON_ENABLE);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_RB_CELL_TEMPMON_ENABLE + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_RB_CELL_TEMPMON_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PPLN_TEMP_SETP
void MLHWSimImplTestCase::testSET_PPLN_TEMP_SETP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x07000U), AMB::MLHWSimBase::controlPoint_PPLN_TEMP_SETP);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PPLN_TEMP_SETP + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PPLN_TEMP_SETP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PPLN_TEMP_CTRL_ENABLE
void MLHWSimImplTestCase::testSET_PPLN_TEMP_CTRL_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x07001U), AMB::MLHWSimBase::controlPoint_PPLN_TEMP_CTRL_ENABLE);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PPLN_TEMP_CTRL_ENABLE + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PPLN_TEMP_CTRL_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PPLN_TEMPMON_ABS_ERR
void MLHWSimImplTestCase::testSET_PPLN_TEMPMON_ABS_ERR()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x07010U), AMB::MLHWSimBase::controlPoint_PPLN_TEMPMON_ABS_ERR);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PPLN_TEMPMON_ABS_ERR + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PPLN_TEMPMON_ABS_ERR + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PPLN_TEMPMON_TUNNEL
void MLHWSimImplTestCase::testSET_PPLN_TEMPMON_TUNNEL()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x07011U), AMB::MLHWSimBase::controlPoint_PPLN_TEMPMON_TUNNEL);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PPLN_TEMPMON_TUNNEL + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PPLN_TEMPMON_TUNNEL + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PPLN_TEMPMON_STABLE_TIME
void MLHWSimImplTestCase::testSET_PPLN_TEMPMON_STABLE_TIME()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x07012U), AMB::MLHWSimBase::controlPoint_PPLN_TEMPMON_STABLE_TIME);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PPLN_TEMPMON_STABLE_TIME + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PPLN_TEMPMON_STABLE_TIME + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PPLN_TEMPMON_SLOW_FILTA
void MLHWSimImplTestCase::testSET_PPLN_TEMPMON_SLOW_FILTA()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x07013U), AMB::MLHWSimBase::controlPoint_PPLN_TEMPMON_SLOW_FILTA);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PPLN_TEMPMON_SLOW_FILTA + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PPLN_TEMPMON_SLOW_FILTA + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PPLN_TEMPMON_FAST_FILTA
void MLHWSimImplTestCase::testSET_PPLN_TEMPMON_FAST_FILTA()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x07014U), AMB::MLHWSimBase::controlPoint_PPLN_TEMPMON_FAST_FILTA);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PPLN_TEMPMON_FAST_FILTA + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PPLN_TEMPMON_FAST_FILTA + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PPLN_TEMPMON_TIMEOUT
void MLHWSimImplTestCase::testSET_PPLN_TEMPMON_TIMEOUT()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x07015U), AMB::MLHWSimBase::controlPoint_PPLN_TEMPMON_TIMEOUT);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PPLN_TEMPMON_TIMEOUT + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PPLN_TEMPMON_TIMEOUT + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PPLN_TEMPMON_ENABLE
void MLHWSimImplTestCase::testSET_PPLN_TEMPMON_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x07016U), AMB::MLHWSimBase::controlPoint_PPLN_TEMPMON_ENABLE);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PPLN_TEMPMON_ENABLE + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PPLN_TEMPMON_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PPLN_EFFICIENCY_CTRL_ENABLE
void MLHWSimImplTestCase::testSET_PPLN_EFFICIENCY_CTRL_ENABLE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(1U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x07018U), AMB::MLHWSimBase::controlPoint_PPLN_EFFICIENCY_CTRL_ENABLE);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PPLN_EFFICIENCY_CTRL_ENABLE + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PPLN_EFFICIENCY_CTRL_ENABLE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PPLN_EFFICIENCY_CTRL_STEP
void MLHWSimImplTestCase::testSET_PPLN_EFFICIENCY_CTRL_STEP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x07019U), AMB::MLHWSimBase::controlPoint_PPLN_EFFICIENCY_CTRL_STEP);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PPLN_EFFICIENCY_CTRL_STEP + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PPLN_EFFICIENCY_CTRL_STEP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PPLN_OPT_START_TEMP
void MLHWSimImplTestCase::testSET_PPLN_OPT_START_TEMP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A050U), AMB::MLHWSimBase::controlPoint_PPLN_OPT_START_TEMP);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PPLN_OPT_START_TEMP + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PPLN_OPT_START_TEMP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PPLN_OPT_STOP_TEMP
void MLHWSimImplTestCase::testSET_PPLN_OPT_STOP_TEMP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A051U), AMB::MLHWSimBase::controlPoint_PPLN_OPT_STOP_TEMP);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PPLN_OPT_STOP_TEMP + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PPLN_OPT_STOP_TEMP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PPLN_OPT_TEMP_STEP
void MLHWSimImplTestCase::testSET_PPLN_OPT_TEMP_STEP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A052U), AMB::MLHWSimBase::controlPoint_PPLN_OPT_TEMP_STEP);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PPLN_OPT_TEMP_STEP + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PPLN_OPT_TEMP_STEP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PPLN_REDPWR_MON_THRESHOLD
void MLHWSimImplTestCase::testSET_PPLN_REDPWR_MON_THRESHOLD()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A055U), AMB::MLHWSimBase::controlPoint_PPLN_REDPWR_MON_THRESHOLD);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PPLN_REDPWR_MON_THRESHOLD + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PPLN_REDPWR_MON_THRESHOLD + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PPLN_MINIMUM_IR_POWER
void MLHWSimImplTestCase::testSET_PPLN_MINIMUM_IR_POWER()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A056U), AMB::MLHWSimBase::controlPoint_PPLN_MINIMUM_IR_POWER);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PPLN_MINIMUM_IR_POWER + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PPLN_MINIMUM_IR_POWER + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PPLN_MINIMUM_RED_POWER
void MLHWSimImplTestCase::testSET_PPLN_MINIMUM_RED_POWER()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0A057U), AMB::MLHWSimBase::controlPoint_PPLN_MINIMUM_RED_POWER);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PPLN_MINIMUM_RED_POWER + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PPLN_MINIMUM_RED_POWER + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PEAKS_MIN_THRESHOLD
void MLHWSimImplTestCase::testSET_PEAKS_MIN_THRESHOLD()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x08000U), AMB::MLHWSimBase::controlPoint_PEAKS_MIN_THRESHOLD);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PEAKS_MIN_THRESHOLD + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PEAKS_MIN_THRESHOLD + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PEAKS_MAX_THRESHOLD
void MLHWSimImplTestCase::testSET_PEAKS_MAX_THRESHOLD()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x08001U), AMB::MLHWSimBase::controlPoint_PEAKS_MAX_THRESHOLD);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PEAKS_MAX_THRESHOLD + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PEAKS_MAX_THRESHOLD + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PEAKS_THRESHOLD_INCREMENT
void MLHWSimImplTestCase::testSET_PEAKS_THRESHOLD_INCREMENT()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x08003U), AMB::MLHWSimBase::controlPoint_PEAKS_THRESHOLD_INCREMENT);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PEAKS_THRESHOLD_INCREMENT + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PEAKS_THRESHOLD_INCREMENT + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_PEAKS_MIN_FLUO_LEVEL
void MLHWSimImplTestCase::testSET_PEAKS_MIN_FLUO_LEVEL()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x08004U), AMB::MLHWSimBase::controlPoint_PEAKS_MIN_FLUO_LEVEL);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_PEAKS_MIN_FLUO_LEVEL + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_PEAKS_MIN_FLUO_LEVEL + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_SCAN_START_TEMP
void MLHWSimImplTestCase::testSET_SCAN_START_TEMP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x09000U), AMB::MLHWSimBase::controlPoint_SCAN_START_TEMP);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_SCAN_START_TEMP + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_SCAN_START_TEMP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_SCAN_STOP_TEMP
void MLHWSimImplTestCase::testSET_SCAN_STOP_TEMP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x09001U), AMB::MLHWSimBase::controlPoint_SCAN_STOP_TEMP);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_SCAN_STOP_TEMP + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_SCAN_STOP_TEMP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_SCAN_PZT_START_VOLTAGE
void MLHWSimImplTestCase::testSET_SCAN_PZT_START_VOLTAGE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x09002U), AMB::MLHWSimBase::controlPoint_SCAN_PZT_START_VOLTAGE);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_SCAN_PZT_START_VOLTAGE + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_SCAN_PZT_START_VOLTAGE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_SCAN_PZT_STOP_VOLTAGE
void MLHWSimImplTestCase::testSET_SCAN_PZT_STOP_VOLTAGE()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x09003U), AMB::MLHWSimBase::controlPoint_SCAN_PZT_STOP_VOLTAGE);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_SCAN_PZT_STOP_VOLTAGE + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_SCAN_PZT_STOP_VOLTAGE + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_SCAN_PZT_DELAY
void MLHWSimImplTestCase::testSET_SCAN_PZT_DELAY()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x09004U), AMB::MLHWSimBase::controlPoint_SCAN_PZT_DELAY);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_SCAN_PZT_DELAY + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_SCAN_PZT_DELAY + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_SCAN_PZT_PERIOD
void MLHWSimImplTestCase::testSET_SCAN_PZT_PERIOD()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(4U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x09005U), AMB::MLHWSimBase::controlPoint_SCAN_PZT_PERIOD);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_SCAN_PZT_PERIOD + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_SCAN_PZT_PERIOD + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

///  testSET_SCAN_DELTA_LAMBDA_OVERLAP
void MLHWSimImplTestCase::testSET_SCAN_DELTA_LAMBDA_OVERLAP()
{
    std::vector< CAN::byte_t > received, original;
    const AmbDataLength_t size(2U);

    const unsigned long baseAddress_m(0UL);

    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x09006U), AMB::MLHWSimBase::controlPoint_SCAN_DELTA_LAMBDA_OVERLAP);
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MLHWSimBase::controlPoint_SCAN_DELTA_LAMBDA_OVERLAP + baseAddress_m, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MLHWSimBase::controlPoint_SCAN_DELTA_LAMBDA_OVERLAP + baseAddress_m));
    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());
    for(AmbDataLength_t i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

void MLHWSimImplTestCase::setUp()
{
    AMB::node_t node(0x0U);
    std::vector< CAN::byte_t > sn(8U, 0x0U);

    sim_m = new AMB::MLHWSimBase(node, sn);
}

void MLHWSimImplTestCase::tearDown()
{
    delete sim_m;
    sim_m = 0;
}

void MLHWSimImplTestCase::test_simulation()
{
    /// Testing specific monitor points

    testmonitorPoint_SYSTEM_STATUS();

    testmonitorPoint_SYSTEM_ERROR();

    testmonitorPoint_SYSTEM_WARNING();

    testmonitorPoint_SYSTEM_STARTUP_STATE();

    testmonitorPoint_SIGNAL_PID_LASER_CORR_MON();

    testmonitorPoint_SIGNAL_PID_ERROR_MON();

    testmonitorPoint_SIGNAL_PM_DC_10V();

    testmonitorPoint_SIGNAL_PIEZO_OUT_MON();

    testmonitorPoint_SIGNAL_PIEZO_SUM_MON();

    testmonitorPoint_SIGNAL_ERROR_PEAK_MON();

    testmonitorPoint_SIGNAL_PM_DC_PEAK_MON();

    testmonitorPoint_SIGNAL_RIN_DC_MON();

    testmonitorPoint_SIGNAL_RIN_LASER_PWR_MON();

    testmonitorPoint_SIGNAL_RIN_ERROR_MON();

    testmonitorPoint_SIGNAL_RIN_DC_CORR_MON();

    testmonitorPoint_SIGNAL_VREF();

    testmonitorPoint_SIGNAL_GND();

    testmonitorPoint_SIGNAL_PPLN_TEMP_MON();

    testmonitorPoint_SIGNAL_PPLN_PWR_I_MON();

    testmonitorPoint_SIGNAL_CELL_TEMP_MON();

    testmonitorPoint_SIGNAL_TIP_TEMP_MON();

    testmonitorPoint_SIGNAL_CELL_PWR_I_MON();

    testmonitorPoint_SIGNAL_HV_MON();

    testmonitorPoint_SIGNAL_PM_TEMP_MON();

    testmonitorPoint_SIGNAL_FL_TEMP();

    testmonitorPoint_SIGNAL_FL_FAST_TEMP();

    testmonitorPoint_SIGNAL_FL_SLOW_TEMP();

    testmonitorPoint_SIGNAL_PPLN_FAST_TEMP();

    testmonitorPoint_SIGNAL_PPLN_SLOW_TEMP();

    testmonitorPoint_SIGNAL_RBCELL_FAST_TEMP();

    testmonitorPoint_SIGNAL_RBCELL_SLOW_TEMP();

    testmonitorPoint_SIGNAL_RBCELL_TIP_FAST_TEMP();

    testmonitorPoint_SIGNAL_RBCELL_TIP_SLOW_TEMP();

    testmonitorPoint_SIGNAL_LOCKMON_FAST_FLUO();

    testmonitorPoint_SIGNAL_LOCKMON_SLOW_FLUO();

    testmonitorPoint_LASER_SIGNAL_FL_TEMP_MON();

    testmonitorPoint_LASER_SIGNAL_FL_THERMV();

    testmonitorPoint_LASER_SIGNAL_REF_2_048V();

    testmonitorPoint_LASER_SIGNAL_FL_TEC_I();

    testmonitorPoint_LASER_SIGNAL_FL_OUTPUT_PWR();

    testmonitorPoint_LASER_SIGNAL_FL_PUMP_I();

    testmonitorPoint_LASER_SIGNAL_FL_PUMP_I_MON();

    testmonitorPoint_LASER_SIGNAL_FL_PUMP_TEC_I();

    testmonitorPoint_LASER_SIGNAL_PWRAMP_INPUT_PWR();

    testmonitorPoint_LASER_SIGNAL_PWRAMP_REFL_PWR();

    testmonitorPoint_LASER_SIGNAL_MODULE_OUTPUT_PWR();

    testmonitorPoint_LASER_SIGNAL_PWRAMP_PUMP_I();

    testmonitorPoint_LASER_SIGNAL_FL_TEMP_SETP();

    testmonitorPoint_LASER_SIGNAL_MODULE_OUTPUT_PWR_SETP();

    testmonitorPoint_LL_ADC_PID_LASER_CORR_MON();

    testmonitorPoint_LL_ADC_PID_ERROR_MON();

    testmonitorPoint_LL_ADC_PM_DC_10V();

    testmonitorPoint_LL_ADC_PIEZO_OUT_MON();

    testmonitorPoint_LL_ADC_PIEZO_SUM_MON();

    testmonitorPoint_LL_ADC_ERROR_PEAK_MON();

    testmonitorPoint_LL_ADC_PM_DC_PEAK_MON();

    testmonitorPoint_LL_ADC_RIN_DC_MON();

    testmonitorPoint_LL_ADC_RIN_LASER_PWR_MON();

    testmonitorPoint_LL_ADC_RIN_ERROR_MON();

    testmonitorPoint_LL_ADC_RIN_DC_CORR_MON();

    testmonitorPoint_LL_ADC_VREF();

    testmonitorPoint_LL_ADC_GND();

    testmonitorPoint_LL_ADC_PPLN_TEMP_MON();

    testmonitorPoint_LL_ADC_PPLN_PWR_I_MON();

    testmonitorPoint_LL_ADC_CELL_TEMP_MON();

    testmonitorPoint_LL_ADC_TIP_TEMP_MON();

    testmonitorPoint_LL_ADC_CELL_PWR_I_MON();

    testmonitorPoint_LL_ADC_HV_MON();

    testmonitorPoint_LL_ADC_PM_TEMP_MON();

    testmonitorPoint_AL_OPTIMISTIC_CUR_SCAN_RANGE();

    testmonitorPoint_AL_ALREADY_SCANNED_RANGE_MIN();

    testmonitorPoint_AL_ALREADY_SCANNED_RANGE_MAX();

    testmonitorPoint_AL_STATE();

    testmonitorPoint_AL_NB_GROUPS();

    testmonitorPoint_AL_ERROR_AMPLITUDE();

    testmonitorPoint_AL_IDENTIFICATION_VALID_RIGHT_GROUP();

    testmonitorPoint_AL_IDENTIFICATION_VALID_WRONG_GROUP();

    testmonitorPoint_AL_IDENTIFICATION_INVALID_GROUP();

    testmonitorPoint_ORM_INTERLOCK_CLOSED();

    testmonitorPoint_PZT_RANGE_CONTROL_STATE();

    testmonitorPoint_PPLN_EFFICIENCY_CONTROL_ACTIVE();

    testmonitorPoint_PPLN_STATE();

    testmonitorPoint_PEAKS_STATE();

    testmonitorPoint_PEAKS_THRESHOLD_LEVEL();

    testmonitorPoint_PEAKS_NEW_DETECTION_AVAILABLE();

    testmonitorPoint_PEAKS_NB_PEAKS();

    testmonitorPoint_PEAKS_SIGNAL_VALIDATION();

    testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_ZERO();

    testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_ONE();

    testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_TWO();

    testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_THREE();

    testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_FOUR();

    testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_FIVE();

    testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_SIX();

    testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_SEVEN();

    testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_EIGHT();

    testmonitorPoint_PEAKS_INFORMATION_PM_DC_LEVEL_PEAK_ID_NINE();

    testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_ZERO();

    testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_ONE();

    testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_TWO();

    testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_THREE();

    testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_FOUR();

    testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_FIVE();

    testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_SIX();

    testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_SEVEN();

    testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_EIGHT();

    testmonitorPoint_PEAKS_INFORMATION_ERROR_LEVEL_PEAK_ID_NINE();

    testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_ZERO();

    testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_ONE();

    testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_TWO();

    testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_THREE();

    testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_FOUR();

    testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_FIVE();

    testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_SIX();

    testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_SEVEN();

    testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_EIGHT();

    testmonitorPoint_PEAKS_INFORMATION_TIMESTAMP_PEAK_ID_NINE();

    testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_ZERO();

    testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_ONE();

    testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_TWO();

    testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_THREE();

    testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_FOUR();

    testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_FIVE();

    testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_SIX();

    testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_SEVEN();

    testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_EIGHT();

    testmonitorPoint_PEAKS_INFORMATION_PZT_LEVEL_PEAK_ID_NINE();

    testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_ZERO();

    testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_ONE();

    testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_TWO();

    testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_THREE();

    testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_FOUR();

    testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_FIVE();

    testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_SIX();

    testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_SEVEN();

    testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_EIGHT();

    testmonitorPoint_PEAKS_INFORMATION_GROUP_ID_PEAK_ID_NINE();

    testmonitorPoint_SCAN_DELTA_T_STEPS();

    testmonitorPoint_SCAN_STATE();

    testmonitorPoint_SYSTEM_STARTUP_MODE();

    testmonitorPoint_SYSTEM_STARTUP_PARAM_SET();

    testmonitorPoint_LL_DAC_PIEZO_SETP();

    testmonitorPoint_LL_DAC_RAMP_TRIG_SETP();

    testmonitorPoint_LL_DAC_RAMP_SLOPE();

    testmonitorPoint_LL_DAC_PID_P_GAIN_SETP();

    testmonitorPoint_LL_DAC_PID_OFFSET_CORR();

    testmonitorPoint_LL_DAC_TRIG_ERROR_SETP();

    testmonitorPoint_LL_DAC_RIN_LOCK_SETP();

    testmonitorPoint_LL_DAC_RIN_DC_OFFSET_P();

    testmonitorPoint_LL_DIGIPOT_DDS_MOD_ADJ();

    testmonitorPoint_LL_DIGIPOT_DDS_LO_ADJ();

    testmonitorPoint_LL_DIGIPOT_RIN_POWER_MON_GAIN();

    testmonitorPoint_LL_DIGIPOT_RIN_REFERENCE_MON_GAIN();

    testmonitorPoint_LL_DIGIPOT_ORM_PPLN_GAIN();

    testmonitorPoint_LL_DIGIPOT_ORM_CELL_GAIN();

    testmonitorPoint_LL_DDS_OSCILLATOR_LOCAL_OSCILLATOR();

    testmonitorPoint_LL_DDS_OSCILLATOR_PHASE_MODULATOR();

    testmonitorPoint_LASER_PZT_TUNING_COEFF();

    testmonitorPoint_LASER_T_TUNING_COEFF();

    testmonitorPoint_LASER_T();

    testmonitorPoint_LASER_PWR();

    testmonitorPoint_LASER_PZT_LEVEL();

    testmonitorPoint_LASER_PWRAMP_ENABLE();

    testmonitorPoint_LASER_TEMP_CTRL_ENABLE();

    testmonitorPoint_LASER_TEMPMON_ABS_ERR();

    testmonitorPoint_LASER_TEMPMON_TUNNEL();

    testmonitorPoint_LASER_TEMPMON_STABLE_TIME();

    testmonitorPoint_LASER_TEMPMON_TIMEOUT();

    testmonitorPoint_LASER_TEMPMON_SLOW_FILTA();

    testmonitorPoint_LASER_TEMPMON_FAST_FILTA();

    testmonitorPoint_LASER_TEMPMON_ENABLE();

    testmonitorPoint_LASER_POWER_MON_TOLERANCE();

    testmonitorPoint_PZT_SWEEP_START_VOLTAGE();

    testmonitorPoint_PZT_SWEEP_STOP_VOLTAGE();

    testmonitorPoint_PZT_SWEEP_DELAY();

    testmonitorPoint_PZT_SWEEP_PERIOD();

    testmonitorPoint_PZT_SWEEP_PERIODIC();

    testmonitorPoint_PZT_SWEEP_FILTER();

    testmonitorPoint_AL_TEMP_MIN();

    testmonitorPoint_AL_TEMP_MAX();

    testmonitorPoint_AL_START_TEMP();

    testmonitorPoint_AL_OPTIMISTIC_SCAN_RANGE();

    testmonitorPoint_AL_OPTIMISTIC_LAMBDA_OVERLAP();

    testmonitorPoint_AL_MODE();

    testmonitorPoint_AL_DETECTION_P_GAIN();

    testmonitorPoint_AL_LAST_LOCK_TEMPERATURE();

    testmonitorPoint_LOCK_PROPORTIONAL_ENABLE();

    testmonitorPoint_LOCK_INTEGRATOR_ENABLE();

    testmonitorPoint_LOCKMON_TOLERANCE();

    testmonitorPoint_LOCKMON_TUNNEL();

    testmonitorPoint_LOCKMON_SLOW_FILTA();

    testmonitorPoint_LOCKMON_FAST_FILTA();

    testmonitorPoint_LOCKMON_ENABLE();

    testmonitorPoint_LOCKMON_UNLOCK_DETECT_THRESH();

    testmonitorPoint_LOCKMON_NOMINAL_FLUO_LEVEL();

    testmonitorPoint_PM_SUPPLY_VOLTAGE();

    testmonitorPoint_PM_SUPPLY_ENABLE();

    testmonitorPoint_PM_AC_GAIN();

    testmonitorPoint_PM_DC_GAIN();

    testmonitorPoint_RED_PD_RESPONSIVITY();

    testmonitorPoint_IR_PD_RESPONSIVITY();

    testmonitorPoint_INTERLOCK_BYPASS_ENABLE();

    testmonitorPoint_PZT_RANGE_CONTROL_ENABLE();

    testmonitorPoint_PZT_RANGE_CONTROL_V_MIN();

    testmonitorPoint_PZT_RANGE_CONTROL_V_MAX();

    testmonitorPoint_RB_CELL_TEMP_SETP();

    testmonitorPoint_RB_CELL_TEMP_CTRL_ENABLE();

    testmonitorPoint_RB_CELL_TEMPMON_ABS_ERR();

    testmonitorPoint_RB_CELL_TIP_TEMPMON_ABS_ERR();

    testmonitorPoint_RB_CELL_TEMPMON_TUNNEL();

    testmonitorPoint_RB_CELL_TEMPMON_STABLE_TIME();

    testmonitorPoint_RB_CELL_TEMPMON_SLOW_FILTA();

    testmonitorPoint_RB_CELL_TEMPMON_FAST_FILTA();

    testmonitorPoint_RB_CELL_TEMPMON_TIMEOUT();

    testmonitorPoint_RB_CELL_TEMPMON_ENABLE();

    testmonitorPoint_PPLN_TEMP_SETP();

    testmonitorPoint_PPLN_TEMP_CTRL_ENABLE();

    testmonitorPoint_PPLN_TEMPMON_ABS_ERR();

    testmonitorPoint_PPLN_TEMPMON_TUNNEL();

    testmonitorPoint_PPLN_TEMPMON_STABLE_TIME();

    testmonitorPoint_PPLN_TEMPMON_SLOW_FILTA();

    testmonitorPoint_PPLN_TEMPMON_FAST_FILTA();

    testmonitorPoint_PPLN_TEMPMON_TIMEOUT();

    testmonitorPoint_PPLN_TEMPMON_ENABLE();

    testmonitorPoint_PPLN_EFFICIENCY_CTRL_ENABLE();

    testmonitorPoint_PPLN_EFFICIENCY_CTRL_STEP();

    testmonitorPoint_PPLN_OPT_START_TEMP();

    testmonitorPoint_PPLN_OPT_STOP_TEMP();

    testmonitorPoint_PPLN_OPT_TEMP_STEP();

    testmonitorPoint_PPLN_REDPWR_MON_THRESHOLD();

    testmonitorPoint_PPLN_MINIMUM_IR_POWER();

    testmonitorPoint_PPLN_MINIMUM_RED_POWER();

    testmonitorPoint_PEAKS_MIN_THRESHOLD();

    testmonitorPoint_PEAKS_MAX_THRESHOLD();

    testmonitorPoint_PEAKS_THRESHOLD_INCREMENT();

    testmonitorPoint_PEAKS_MIN_FLUO_LEVEL();

    testmonitorPoint_SCAN_START_TEMP();

    testmonitorPoint_SCAN_STOP_TEMP();

    testmonitorPoint_SCAN_PZT_START_VOLTAGE();

    testmonitorPoint_SCAN_PZT_STOP_VOLTAGE();

    testmonitorPoint_SCAN_PZT_DELAY();

    testmonitorPoint_SCAN_PZT_PERIOD();

    testmonitorPoint_SCAN_DELTA_LAMBDA_OVERLAP();

    /// Testing specific control points

    testSET_SAVE_ALL_PARAMS();

    testSET_SAVE_PARAM_BY_ID();

    testSET_LOAD_ALL_PARAMS();

    testSET_LOAD_PARAM_BY_ID();

    testSET_RESET_LASER_REFL_ALARM();

    testSET_PZT_SWEEP_START();

    testSET_PZT_SWEEP_STOP();

    testSET_PZT_SWEEP_RESET();

    testSET_ORM_SAVE_PARAMS();

    testSET_ORM_LOAD_PARAMS();

    testSET_MANUAL_MODE_REQUEST();

    testSET_STANDBY_MODE_REQUEST();

    testSET_AUTOLOCK_REQUEST();

    testSET_PPLN_OPTIMIZATION_SCAN_START();

    testSET_PPLN_OPTIMIZATION_SCAN_STOP();

    testSET_LOCK_MONITORING_RESET_TRIGGER();

    testSET_CLEAR_WARNING_LED();

    testSET_PPLN_EFFICIENCY_CTRL_START();

    testSET_PPLN_EFFICIENCY_CTRL_STOP();

    testSET_SYSTEM_STICKY_ERROR_STATUS();

    testSET_SYSTEM_LM_SERIAL_NUMBER();

    testSET_SYSTEM_ORM_SERIAL_NUMBER();

    testSET_SYSTEM_STARTUP_MODE();

    testSET_SYSTEM_STARTUP_PARAM_SET();

    testSET_LL_GPIO();

    testSET_LL_IOX();

    testSET_LL_ORM_IOX();

    testSET_LL_DAC_PIEZO_SETP();

    testSET_LL_DAC_RAMP_TRIG_SETP();

    testSET_LL_DAC_RAMP_SLOPE();

    testSET_LL_DAC_PID_P_GAIN_SETP();

    testSET_LL_DAC_PID_OFFSET_CORR();

    testSET_LL_DAC_TRIG_ERROR_SETP();

    testSET_LL_DAC_RIN_LOCK_SETP();

    testSET_LL_DAC_RIN_DC_OFFSET_P();

    testSET_LL_SELECTOR_ADC_MUX();

    testSET_LL_SELECTOR_PID_TAU();

    testSET_LL_DIGIPOT_DDS_MOD_ADJ();

    testSET_LL_DIGIPOT_DDS_LO_ADJ();

    testSET_LL_DIGIPOT_RIN_POWER_MON_GAIN();

    testSET_LL_DIGIPOT_RIN_REFERENCE_MON_GAIN();

    testSET_LL_DIGIPOT_ORM_PPLN_GAIN();

    testSET_LL_DIGIPOT_ORM_CELL_GAIN();

    testSET_LL_DDS_OSCILLATOR_LOCAL_OSCILLATOR();

    testSET_LL_DDS_OSCILLATOR_PHASE_MODULATOR();

    testSET_LASER_PZT_TUNING_COEFF();

    testSET_LASER_T_TUNING_COEFF();

    testSET_LASER_T();

    testSET_LASER_PWR();

    testSET_LASER_PZT_LEVEL();

    testSET_LASER_PWRAMP_ENABLE();

    testSET_LASER_TEMP_CTRL_ENABLE();

    testSET_LASER_TEMPMON_ABS_ERR();

    testSET_LASER_TEMPMON_TUNNEL();

    testSET_LASER_TEMPMON_STABLE_TIME();

    testSET_LASER_TEMPMON_TIMEOUT();

    testSET_LASER_TEMPMON_SLOW_FILTA();

    testSET_LASER_TEMPMON_FAST_FILTA();

    testSET_LASER_TEMPMON_ENABLE();

    testSET_LASER_POWER_MON_TOLERANCE();

    testSET_PZT_SWEEP_START_VOLTAGE();

    testSET_PZT_SWEEP_STOP_VOLTAGE();

    testSET_PZT_SWEEP_DELAY();

    testSET_PZT_SWEEP_PERIOD();

    testSET_PZT_SWEEP_PERIODIC();

    testSET_PZT_SWEEP_FILTER();

    testSET_AL_TEMP_MIN();

    testSET_AL_TEMP_MAX();

    testSET_AL_START_TEMP();

    testSET_AL_OPTIMISTIC_SCAN_RANGE();

    testSET_AL_OPTIMISTIC_LAMBDA_OVERLAP();

    testSET_AL_MODE();

    testSET_AL_DETECTION_P_GAIN();

    testSET_AL_LAST_LOCK_TEMPERATURE();

    testSET_LOCK_PROPORTIONAL_ENABLE();

    testSET_LOCK_INTEGRATOR_ENABLE();

    testSET_LOCKMON_TOLERANCE();

    testSET_LOCKMON_TUNNEL();

    testSET_LOCKMON_SLOW_FILTA();

    testSET_LOCKMON_FAST_FILTA();

    testSET_LOCKMON_ENABLE();

    testSET_LOCKMON_UNLOCK_DETECT_THRESH();

    testSET_LOCKMON_NOMINAL_FLUO_LEVEL();

    testSET_PM_SUPPLY_VOLTAGE();

    testSET_PM_SUPPLY_ENABLE();

    testSET_PM_AC_GAIN();

    testSET_PM_DC_GAIN();

    testSET_RED_PD_RESPONSIVITY();

    testSET_IR_PD_RESPONSIVITY();

    testSET_INTERLOCK_BYPASS_ENABLE();

    testSET_PZT_RANGE_CONTROL_ENABLE();

    testSET_PZT_RANGE_CONTROL_V_MIN();

    testSET_PZT_RANGE_CONTROL_V_MAX();

    testSET_RB_CELL_TEMP_SETP();

    testSET_RB_CELL_TEMP_CTRL_ENABLE();

    testSET_RB_CELL_TEMPMON_ABS_ERR();

    testSET_RB_CELL_TIP_TEMPMON_ABS_ERR();

    testSET_RB_CELL_TEMPMON_TUNNEL();

    testSET_RB_CELL_TEMPMON_STABLE_TIME();

    testSET_RB_CELL_TEMPMON_SLOW_FILTA();

    testSET_RB_CELL_TEMPMON_FAST_FILTA();

    testSET_RB_CELL_TEMPMON_TIMEOUT();

    testSET_RB_CELL_TEMPMON_ENABLE();

    testSET_PPLN_TEMP_SETP();

    testSET_PPLN_TEMP_CTRL_ENABLE();

    testSET_PPLN_TEMPMON_ABS_ERR();

    testSET_PPLN_TEMPMON_TUNNEL();

    testSET_PPLN_TEMPMON_STABLE_TIME();

    testSET_PPLN_TEMPMON_SLOW_FILTA();

    testSET_PPLN_TEMPMON_FAST_FILTA();

    testSET_PPLN_TEMPMON_TIMEOUT();

    testSET_PPLN_TEMPMON_ENABLE();

    testSET_PPLN_EFFICIENCY_CTRL_ENABLE();

    testSET_PPLN_EFFICIENCY_CTRL_STEP();

    testSET_PPLN_OPT_START_TEMP();

    testSET_PPLN_OPT_STOP_TEMP();

    testSET_PPLN_OPT_TEMP_STEP();

    testSET_PPLN_REDPWR_MON_THRESHOLD();

    testSET_PPLN_MINIMUM_IR_POWER();

    testSET_PPLN_MINIMUM_RED_POWER();

    testSET_PEAKS_MIN_THRESHOLD();

    testSET_PEAKS_MAX_THRESHOLD();

    testSET_PEAKS_THRESHOLD_INCREMENT();

    testSET_PEAKS_MIN_FLUO_LEVEL();

    testSET_SCAN_START_TEMP();

    testSET_SCAN_STOP_TEMP();

    testSET_SCAN_PZT_START_VOLTAGE();

    testSET_SCAN_PZT_STOP_VOLTAGE();

    testSET_SCAN_PZT_DELAY();

    testSET_SCAN_PZT_PERIOD();

    testSET_SCAN_DELTA_LAMBDA_OVERLAP();

    /// Testing generic points
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(static_cast< AMB::rca_t >(AMB::MLHWSimBase::monitorPoint_SERIAL_NUMBER), createVector(8U)));
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(static_cast< AMB::rca_t >(AMB::MLHWSimBase::monitorPoint_PROTOCOL_REV_LEVEL), createVector(3U)));
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(static_cast< AMB::rca_t >(AMB::MLHWSimBase::monitorPoint_CAN_ERROR), createVector(4U)));
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(static_cast< AMB::rca_t >(AMB::MLHWSimBase::monitorPoint_TRANS_NUM), createVector(4U)));
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(static_cast< AMB::rca_t >(AMB::MLHWSimBase::monitorPoint_AMBIENT_TEMPERATURE), createVector(4U)));
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(static_cast< AMB::rca_t >(AMB::MLHWSimBase::monitorPoint_SW_REV_LEVEL), createVector(3U)));
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(static_cast< AMB::rca_t >(AMB::MLHWSimBase::GET_HW_REV_LEVEL), createVector(3U)));
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(static_cast< AMB::rca_t >(AMB::MLHWSimBase::RESET_AMBSI), createVector(1U)));
    CPPUNIT_ASSERT_NO_THROW(sim_m->control(static_cast< AMB::rca_t >(AMB::MLHWSimBase::RESET_DEVICE), createVector(1U)));

    CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(static_cast< AMB::rca_t >(AMB::MLHWSimBase::monitorPoint_SERIAL_NUMBER)));
    CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(static_cast< AMB::rca_t >(AMB::MLHWSimBase::monitorPoint_PROTOCOL_REV_LEVEL)));
    CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(static_cast< AMB::rca_t >(AMB::MLHWSimBase::monitorPoint_CAN_ERROR)));
    CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(static_cast< AMB::rca_t >(AMB::MLHWSimBase::monitorPoint_TRANS_NUM)));
    CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(static_cast< AMB::rca_t >(AMB::MLHWSimBase::monitorPoint_AMBIENT_TEMPERATURE)));
    CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(static_cast< AMB::rca_t >(AMB::MLHWSimBase::monitorPoint_SW_REV_LEVEL)));
    CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(static_cast< AMB::rca_t >(AMB::MLHWSimBase::GET_HW_REV_LEVEL)));
    CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(static_cast< AMB::rca_t >(AMB::MLHWSimBase::RESET_AMBSI)));
    CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(static_cast< AMB::rca_t >(AMB::MLHWSimBase::RESET_DEVICE)));

    /// Testing error cases
    CPPUNIT_ASSERT_THROW(sim_m->control(static_cast< AMB::rca_t >(0x99999U), createVector(8U)), CAN::Error);
    CPPUNIT_ASSERT_THROW(sim_m->monitor(static_cast< AMB::rca_t >(0x99999U)), CAN::Error);
}

std::vector< CAN::byte_t > MLHWSimImplTestCase::createVector(const AmbDataLength_t size)
{
    std::vector< CAN::byte_t > data;

    for(AmbDataLength_t count(1U); count <= size; ++count)
    {
        data.push_back(static_cast< CAN::byte_t >(count));
    }

    return data;
}

/**
 * Main function running the tests
 */
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>

int main(int argc, char* argv[])
{
    // Create the event manager and test controller
    CPPUNIT_NS::TestResult controller;

    // Add a listener that colllects test result
    CPPUNIT_NS::TestResultCollector result;
    controller.addListener(&result);

    // Add a listener that print dots as test run.
    CPPUNIT_NS::BriefTestProgressListener progress;
    controller.addListener(&progress);

    // Add the top suite to the test runner
    CPPUNIT_NS::TestRunner runner;
    runner.addTest(CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest());
    runner.run(controller);

    // Print test in a compiler compatible format.
    CPPUNIT_NS::CompilerOutputter outputter(&result, std::cerr);
    outputter.write();

    return result.wasSuccessful() ? 0 : 1;
}
