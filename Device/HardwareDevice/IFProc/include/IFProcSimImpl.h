#ifndef IFPROCSIMIMPL_H_
#define IFPROCSIMIMPL_H_
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Jun 25, 2007  created
*/

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

#include "IFProcSimBase.h"
#include <maciContainerServices.h>
#include <ControlDeviceExceptionsC.h>


class IFProcSimImpl: public IFProcSimBase
{
    public:
    IFProcSimImpl(const ACE_CString& name, maci::ContainerServices* cs);
    virtual ~IFProcSimImpl();

    virtual void hwSimulationAction();
//        throw(CORBA::SystemException, ControlDeviceExceptions::HwLifecycleEx);

    private:
    /**
     * No default constructor.
     */
    IFProcSimImpl();

    /**
     * ALMA coding standards: copy constructor is disabled.
     */
    IFProcSimImpl(const IFProcSimImpl&);

    /**
     * ALMA coding standards: assignment operator is disabled.
     */
    IFProcSimImpl& operator=(const IFProcSimImpl&);
};

#endif /*IFPROCSIMIMPL_H_*/
