#ifndef ETHERNETDATATHREAD_H_
#define ETHERNETDATATHREAD_H_
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Apr 17, 2007  created
*/
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

//ACE includes for the socket connection
#include <ace/INET_Addr.h>
#include <ace/SOCK_Stream.h>
#include <ace/SOCK_Connector.h>

#include <acsThread.h>

//Time conversion util
#include <TETimeUtil.h>
#include <acstime.h>

namespace AMB 
{
class IFProcHWSimImpl;

class EthernetDataThread : public ACS::Thread {
        public:
          EthernetDataThread(const ACE_CString& _name, const IFProcHWSimImpl* _ifproc);

         virtual void runLoop();        
        private:
//          virtual int svc(void);
                    
          bool createSocket(void);
          bool sendDataSocket (const char *s) const;
          void closeSocket(void);

          // create the TCP packet
          const char* createPacket(unsigned long long);
          unsigned char *m_packet;
          int m_numBDBs;
          int m_totalBytes;

          // Referenace to the simulated IFProcHW
          IFProcHWSimImpl* m_ifProcHWSim_p;

          // Socket
          ACE_SOCK_Stream    m_peer;
          ACE_SOCK_Connector m_connector;

    // Initially the device is not sending data nor connected.        
    int m_status;

    bool m_firstCall;
    unsigned long long m_nextExecution;
    unsigned long long m_now;

    timespec m_timeToSleep; 
    timespec m_remainder;
      };
}
#endif
