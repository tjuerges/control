#ifndef IFPROCIMPLMONITORTHREAD_H_
#define IFPROCIMPLMONITORTHREAD_H_
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Apr 17, 2007  created
*/
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

#include <acsThread.h>
#include <string>
#include <acstime.h>
#include <RepeatGuard.h>

class IFProcImpl;

class IFProcImplMonitorThread: public ACS::Thread
{
    public:
    IFProcImplMonitorThread(const ACE_CString& _name,
                            const IFProcImpl* _ifproc);

    virtual void runLoop();

    private:

     ACS::Time timestamp;
     bool wasInError;

    RepeatGuard guard;

    /**
     * Pointer to the IFProcImpl instance. Needed to call
     * IFProcImpl::GET_READ_ALMA_TIME(ACS::Time&).
     */
    IFProcImpl* ifProcDevice_p;

    

};
#endif /*IFPROCIMPLMONITORTHREAD_H_*/
