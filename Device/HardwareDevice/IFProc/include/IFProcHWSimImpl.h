/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 * 
 * File IFProcHWSimImpl.h
 *
 * $Id$
 */
#ifndef IFProcHWSimImpl_H
#define IFProcHWSimImpl_H

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif



#include "IFProcHWSimBase.h"
#include "EthernetDataThread.h"

#include <SharedSimWrapper.h>
#include <SharedSimulatorC.h>
#include <string>

#define STAND_BY_STATE  1
#define READY_STATE     2
#define SENDING_STATE   3

namespace AMB
{
  /* Please use this class to implement complex functionality for the
   * IFProcHWSimBase helper functions. Use AMB::TypeConvertion methods
   * for convenient type convertions. */
  class IFProcHWSimImpl : public IFProcHWSimBase{

    private :
        // When setting the Alma Time to the device, an offset to the actual time
        // of the computer should be stored. So we are able to generate correct
        // timestamps on the sent total power data.
        int timeOffset;

        // boolean variable that tells the socket connection state
        bool connected;

        // boolean variable that tells the data transmission state.
        bool sendingData;

        // thread
        EthernetDataThread * m_ethernetDataThread_p;

        // thread finalization flag
        //bool m_finalizeThread;

        bool m_try_initialize;
        bool m_try_start_data;
        bool m_try_stop_data;

        maci::ContainerServices *m_pCS;

        
  public :
        // Constructor
        friend class EthernetDataThread;

        IFProcHWSimImpl(node_t node, const std::vector<CAN::byte_t>& serialNumber, 
			int antenna=0, 
			SHARED_SIMULATOR::SharedSimulator_var ss=SHARED_SIMULATOR::SharedSimulator::_nil());
        ~IFProcHWSimImpl();

        // custom initialization method.
        void initialize(node_t node, const std::vector<CAN::byte_t>& serialNumber, 
                        maci::ContainerServices* _pCS);

        void finalize();

	//For getting the share simulator reference
	SHARED_SIMULATOR::SharedSimulator_var getSharedSimulatorReference() { return ss_m;};

        //Reset Methods
        void setControlResetTpdBoard(const std::vector<CAN::byte_t>& data);
        void setResetTpdEthernet();
        void resetAll();

        // Total Power and attenuation simulation
	std::vector<CAN::byte_t> getMonitorDataMonitor1() const;
        std::vector<CAN::byte_t> getMonitorDataMonitor2() const; 
        std::vector<CAN::byte_t> getMonitorGains() const;

        void setControlClearTimingError(const std::vector<CAN::byte_t>& data);
        void setControlInitTcpConn(const std::vector<CAN::byte_t>& data);
        void setControlStartData(const std::vector< CAN::byte_t >& data);
        void setControlStopData(const std::vector< CAN::byte_t >& data);
        void setControlSetDataRemap(const std::vector<CAN::byte_t> &data);
        void setControlSetModuleIpAddr(const std::vector<CAN::byte_t> &data);
        void setControlSetDestIpAddr(const std::vector<CAN::byte_t> &data);
        void setControlSetTcpPort(const std::vector<CAN::byte_t> &data);
        void setControlSetBdbPerPacket(const std::vector<CAN::byte_t> &data);
        void setControlSetAlmaTime(const std::vector<CAN::byte_t> &data);
        void setControlSetAverageLength(const std::vector<CAN::byte_t> &data);
        void setControlSetModuleGateway(const std::vector<CAN::byte_t> &data);
        void setControlSetModuleNetmask(const std::vector<CAN::byte_t> &data);
        void setControlSetGains(const std::vector<CAN::byte_t> &data);
        void setControlSetSignalPaths(const std::vector<CAN::byte_t> &data);


 
	SHARED_SIMULATOR::SharedSimulator_var ss_m;
	SharedSimWrapper* ssi_m;

	short polarization_m;

  private:
        /// Copy and assignment are not allowed
        IFProcHWSimImpl(const IFProcHWSimImpl&);
        IFProcHWSimImpl& operator=(const IFProcHWSimImpl&);
	
}; // class IFProcHWSimImpl

} // namespace AMB

#endif // IFProcHWSimImpl_H
