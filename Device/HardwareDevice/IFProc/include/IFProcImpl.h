// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2005 - 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#ifndef IFPROCIMPL_H
#define IFPROCIMPL_H

#include <IFProcBase.h>
#include <IFProcS.h>
#include <IFProcImplMonitorThread.h>
#include <monitorHelper.h>
#include <RepeatGuard.h>
#include <IFProcEthernet.h>

// Allows managing alarm state flags not covered by BACI Alarms
#include <controlAlarmHelper.h>

class ConfigDataAccessor;
class XmlTmcdbComponent;

//  Error State Definitions
enum IFProcErrorCondition {
    NOTPPROC            = 0x001,
    UNKNOWN             = 0x002,
    NOTPPROCCOMM        = 0x004,
    NOCONN              = 0x008,
    NOANTENNA           = 0x010,
    NOPOLARISATION      = 0x020,
    NOIP                = 0x040,
    NOCANCOMM           = 0x080,
    INACTIVE            = 0x100,
    TCPINITFAIL         = 0x200, 
    ADDFAIL             = 0x400,
    TIMINGERRFLAG       = 0x800
};

typedef struct IFProcConfig{
    float slope[6];
    float icept[6];
    float atten[4][64];
} ifprocConfig_t;

class IFProcImpl : public IFProcBase,
                   public MonitorHelper,
                   public Control::AlarmHelper,
                   public virtual POA_Control::IFProc {
    
public:

    // Allow extended simulation to access private members.
    friend class IFProcSimImpl;
    
    // Constructor and Destructor
    IFProcImpl(const ACE_CString& name, maci::ContainerServices* cs);
    virtual ~IFProcImpl();
    
    // ---------------- Component Lifecycle -----------------------
    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void initialize();

    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void cleanUp();

    // ----------------------- IFProc Methods ---------------------------

    /// Collect the total power data for a time interval starting when the
    /// method is executed for duration seconds.
    ///  \exception ControlDeviceExceptions::IllegalConfigurationEx
    virtual Control::IFProc::TotalPowerData* getTpData(ACS::TimeInterval duration);

    /// Monitor of the Side Band Total Power Data in [dbm]. The value is averaged over 
    /// many samples determined by DATA_AVE_LENGTH.
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual Control::FloatSeq* getSBPower(ACS::Time& timestamp);

    /// Monitor of the BaseBand Total Power Data in [dbm]. The value is averaged over 
    /// many samples determined by DATA_AVE_LENGTH.
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual Control::FloatSeq* getBBPower(ACS::Time& timestamp);

    /// Set the Total Power level using a control loop. Don't need calibration values.
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual void setPowerLevel(CORBA::Float level);

    virtual ACS::ROfloat_ptr TOTAL_POWER_SIDEBAND_0_DBM();
    virtual ACS::ROfloat_ptr TOTAL_POWER_SIDEBAND_1_DBM();
    virtual ACS::ROfloat_ptr TOTAL_POWER_A_DBM();
    virtual ACS::ROfloat_ptr TOTAL_POWER_B_DBM();
    virtual ACS::ROfloat_ptr TOTAL_POWER_C_DBM();
    virtual ACS::ROfloat_ptr TOTAL_POWER_D_DBM();
  
    /// Allocate a TPRx-offshoot object.
    /// \exception ControlDeviceExceptions::IllegalConfigurationEx
    virtual void allocate(const char* name);

    /// Free the via allocate controlled TPRx object.
    /// \exception ControlDeviceExceptions::IllegalConfigurationEx
    virtual void deallocate();

    /// Start data acquisition.
    /// \exception ControlDeviceExceptions::IllegalConfigurationEx
    virtual void beginDataAcquisition(ACS::Time timestamp);

    /// Abort data acquisition.
    /// \exception ControlDeviceExceptions::IllegalConfigurationEx
    virtual void abortDataAcquisition();

    /// Method to set the time of the IFProc hardware device.
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    void resyncIFProcTime();

    /// \exception ControlExceptions::INACTErrorEx
    void setSignalPaths(bool baseBandAHigh,
                        bool baseBandBHigh, 
                        bool baseBandCHigh, 
                        bool baseBandDHigh,
                        bool ABUpperSideBand,
                        bool CDUpperSideBand, ACS::Time when);

    void flush(ACS::Time after);

protected:

    // -----------------------Hardware Lifecycle Interface -------------------
    virtual void hwConfigureAction();
    virtual void hwInitializeAction();
    virtual void hwOperationalAction();
    virtual void hwStopAction();

    // --------------------- Monitor and Error Handler Method ---------------------------
    virtual void processRequestResponse(const AMBRequestStruct& response);
 
    // ------------------ Alarm Helper (Error State) Methods ---------------------
    virtual void handleActivateAlarm(int newFlag);
    virtual void handleDeactivateAlarm(int newFlag);
    
    std::vector<Control::AlarmInformation> createAlarmVector();


    /// These methods process the response to methods from the IFProc
    virtual void processSbTp(const MonitorHelper::AMBRequestStruct&);
    virtual void processChTp(const MonitorHelper::AMBRequestStruct&);

    virtual void setPowerLevelCore(float level);

    /// Converts the voltages from TPD into a power measurement.
    std::vector<float> voltageToPower(std::vector<float> BBvoltage);
    float voltageToPower(float voltage, int index);

    /// Read the IFProcessor's configuration (Slope, Intercept and Attenuation values for the current IFProc).
    void readIFProcConfig();

    /// Get the serial number of this IFProc.
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    int getSerialNumberCode();

    /// Get Attenuations for a certain TE.
    /// \exception ControlExceptions::CAMBErrorExImpl
    std::vector< float > getAttenuationTE(ACS::Time epoch);

    /// Get BaseBand voltages for a certain TE
    /// \exception ControlExceptions::CAMBErrorExImpl
    std::vector< float > getBBvoltageTE(ACS::Time epoch);

    /// Synchronous request for a certain TE
    void requestTE(ACS::Time epoch, unsigned long rca, MonitorHelper::AMBRequestStruct* request);

    /// The code generator does not provide a way to do special conversions
    /// for dependent monitor points, so we overload the protected method to
    /// return dBm instead of volt
    virtual float getTotalPowerSideband0Dbm(ACS::Time& timestamp);
    virtual float getTotalPowerSideband1Dbm(ACS::Time& timestamp);
    virtual float getTotalPowerADbm(ACS::Time& timestamp);
    virtual float getTotalPowerBDbm(ACS::Time& timestamp);
    virtual float getTotalPowerCDbm(ACS::Time& timestamp);
    virtual float getTotalPowerDDbm(ACS::Time& timestamp);

private:
    
    XmlTmcdbComponent* tmcdb_m;
    ConfigDataAccessor* configData_m;

    // IFproc calibration configuration
    ifprocConfig_t ifprocConfig;

    // Vectors that will have the Total Power data requested by getTpData
    std::vector< MonitorHelper::AMBRequestStruct > channelsTpData;
    std::vector< MonitorHelper::AMBRequestStruct > sidebandsTpData;
   
    //Flag that allows the threads to add the requested data to TotalPower vectos
    bool processingFlag;

    // Repeat Guard for getTpData
    RepeatGuard getTpData_guard;
    

    IFProcImplMonitorThread* implMonitorThread_p;

    // Pointer to the TotalPowerProcessor.
    Control::NewTPP_var tpProc;

    // The name of the TotalPowerProcessor
    ACE_CString tppName;

    // Total Power Ethernet .
    IFProcEthernet ifpEth;

    // Keep track of allocated and valid total power processor.
    bool tpProcIsAllocated;

    // Keep track of acquisition process.
    bool daqIsActive;

    baci::SmartPropertyPointer<baci::ROfloat> total_power_sideband_0_dbm_sp;
    baci::SmartPropertyPointer<baci::ROfloat> total_power_sideband_1_dbm_sp;
    baci::SmartPropertyPointer<baci::ROfloat> total_power_a_dbm_sp;
    baci::SmartPropertyPointer<baci::ROfloat> total_power_b_dbm_sp;
    baci::SmartPropertyPointer<baci::ROfloat> total_power_c_dbm_sp;
    baci::SmartPropertyPointer<baci::ROfloat> total_power_d_dbm_sp;

    // Access mutex for allocation/deallocation.
    ACE_Mutex accessMutex;

    // Allow monitoring thread access to private methods. By doing this, some
    // CORBA overhead is avoided and the methods which talk directly with the
    // device can be called.
    friend class IFProcImplMonitorThread;
    friend class IFProcEthernet;

    ///////////////////
    /// Implementing pure virtual methods
    //////////////////

    /////////// RAW to WORLD 

    // Convert the raw value of READ_DATA_AVE_LENGTH to a world value.
    virtual unsigned short rawToWorldDataAveLength(const unsigned char raw) const throw();

    // Convert the raw value of READ_SIGNAL_PATHS to a world value.
    virtual std::vector< bool > rawToWorldSignalPaths(const unsigned char raw) const throw();

    /////////// WORLD to RAW

    // Convert the raw value of AVERAGE_LENGTH to a world value.
    virtual unsigned char worldToRawCntlAverageLength(const unsigned short world) const throw();

    // Convert the raw value of SIGNAL_PATHS to a world value.
    virtual std::vector< unsigned char > worldToRawCntlSignalPaths(const std::vector< bool >& world) const throw();

    // Convert the raw value of START_COMM_TEST to a world value.
    virtual std::vector< unsigned char > worldToRawStartCommTest(const std::vector< bool >& world) const throw();

    /// Copy and assignment are not allowed
    IFProcImpl(const IFProcImpl&);
    IFProcImpl& operator=(const IFProcImpl&);
    
protected:
    class TOTAL_POWER_DBM_RODevIO: public DevIO< CORBA::Float >,
                                   public Logging::Loggable

    {
    public:
        TOTAL_POWER_DBM_RODevIO(IFProcImpl* base, const int index);

        TOTAL_POWER_DBM_RODevIO(IFProcImpl* base, const int index,
                                const std::string& loggerName);

        TOTAL_POWER_DBM_RODevIO(IFProcImpl* base, const int index,
                                Logging::Logger::LoggerSmartPtr logger);

        virtual ~TOTAL_POWER_DBM_RODevIO();

        virtual bool initializeValue();

        /// \exception ACSErr::ACSbaseExImpl

        virtual CORBA::Float read(ACS::Time& timestamp);

    private:
        /// No normal constructor.
        TOTAL_POWER_DBM_RODevIO();

        /// ALMA coding standards: copy constructor is disabled.
        TOTAL_POWER_DBM_RODevIO(const TOTAL_POWER_DBM_RODevIO& other);

        /// ALMA coding standards: assignment operator is disabled.
        TOTAL_POWER_DBM_RODevIO& operator=(const TOTAL_POWER_DBM_RODevIO& other);

        IFProcImpl* device;

        int index_m;
    };

};
#endif // IFPROCIMPL_H
