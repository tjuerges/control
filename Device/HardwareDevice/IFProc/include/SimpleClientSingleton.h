/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 *
 */

#ifndef SIMPLECLIENTSINGLETON_H
#define SIMPLECLIENTSINGLETON_H
#include "ace/Thread.h"
#include <maciSimpleClient.h>
#include <ACSErrTypeCommon.h>

class SimpleClientSingleton {

public:
	static SimpleClientSingleton* instance();
        static void release();
	maci::SimpleClient* getClient();
protected:
	SimpleClientSingleton() throw (ACSErrTypeCommon::CouldntPerformActionExImpl);
	~SimpleClientSingleton();
private:
	static SimpleClientSingleton* _instance;
    void initializeClient()
        throw (ACSErrTypeCommon::CouldntPerformActionExImpl);
    static maci::SimpleClient* m_client;
    static unsigned int refCounter;
    static ACE_Recursive_Thread_Mutex mutex;
};

    void* worker(void *arg);

#endif /*!SIMPLECLIENTSINGLETON_H*/
