#ifndef IFPROCETHERNET_H
#define IFPROCETHERNET_H


#include <ControlDeviceExceptions.h>

class IFProcImpl;

class IFProcEthernet
{

  public:

    /* Constructor*/
    IFProcEthernet(const IFProcImpl* _ifproc);
    
    
    /**
     * Initialize the network configuration. Should be called before an 'establishTCPConnection()'.
     */
    void init();
//	throw(ControlDeviceExceptions::IllegalConfigurationExImpl);
    
    
    /**
     * Find the antenna name using the Component name convention
     */
    const std::string findAntennaName();
//    throw(ControlDeviceExceptions::IllegalConfigurationExImpl);
    
    
    /**
     * Find the polarization of this IFProc using the component name convention
     */
    const CORBA::Long findPolarization();
//    throw(ControlDeviceExceptions::IllegalConfigurationExImpl);
    
    
    /**
     * Find the IP address of the module asking the system for this hostname
     */
    const std::vector< unsigned char > findIpAddress(const std::string& antennaName, 
                                                     const CORBA::Long polarization) const;
//        throw(ControlDeviceExceptions::IllegalConfigurationExImpl);
    
    
    /**
     * Find the Netmask to be used.
     */
    const std::vector< unsigned char > findNetmask();
//	    throw(ControlDeviceExceptions::IllegalConfigurationExImpl);
    
    
    /**
     * Find the gateway to be used
     */
    const std::vector< unsigned char > findGateway();
//	    throw(ControlDeviceExceptions::IllegalConfigurationExImpl);
    
    
    /**
     * Set all the network parameters at the IFProc and initialize the TCP connection.
     */
    void establishTCPConnection(std::vector< unsigned char > destinationIpAddress, const CORBA::UShort portToUse);
//        throw(ControlDeviceExceptions::IllegalConfigurationExImpl);
    
    
    /**
     * Reset IFProc network to zero values.
     */
    void disconnectTCPConnection();
//	    throw(ControlDeviceExceptions::IllegalConfigurationExImpl);


private:
    IFProcImpl* ifProcDevice_p;         
    std::string antennaName;
    CORBA::Long polarization;
    std::vector< unsigned char >  ifProcIpAddress;
    std::vector< unsigned char >  netmask;
    std::vector< unsigned char >  gateway;

    // Keep btrack of active TCP-connection.
    bool tcpConnectionIsActive;

};

#endif
