#!/usr/bin/env python

import unittest
import Acspy.Clients.SimpleClient
import sys

# For the exceptions
import maciErrTypeImpl
import ControlDeviceExceptions

# **************************************************
# automated test class
# **************************************************

class automated( unittest.TestCase ):

    def setUp( self ):
        self.ifp = None
        try:    
           self.ifp = client.getComponent('CONTROL/DA41/IFProc0')
        except (maciErrTypeImpl.CannotGetComponentExImpl), ex:
           Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace).log();
           raise
      
    def tearDown( self ):
        if (self.ifp != None):
           client.releaseComponent(self.ifp._get_name())

    def toOperational( self ):
        self.ifp.hwStop()
        self.ifp.hwStart()
        self.ifp.hwConfigure()
        self.ifp.hwInitialize()
        self.ifp.hwOperational()
        
    def test01( self ):
        '''
        This test just loads the CONTROL/DA41/IFProc0 component
        and unload it.
        '''

    def test02( self ):
        '''
        This test loads the CONTROL/DA41/IFProc0 Component and
        bring it to operational state.
        '''
        try:
          self.toOperational()
        except (ControlDeviceExceptions.HwLifecycleEx), ex:
          Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace).log();
          self.fail(ex.errorTrace.shortDescription)
    def test03( self ):
        '''
        This test sets the gains on the IFProc, and check that they
        are correctly stored.
        '''
        try:
          self.toOperational()
          self.ifp.SET_GAINS([15,10,5,0])
          gains = self.ifp.GET_GAINS()[0]
          self.failUnless(gains==[15, 10.0, 5, 0])
        except (ControlDeviceExceptions.HwLifecycleEx), ex:
          #Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace).log();
          self.fail(ex.errorTrace.shortDescription)
        
 
# **************************************************
# MAIN Program
# **************************************************
if __name__ == '__main__':

    # Get a Simple Client
    client = Acspy.Clients.SimpleClient.PySimpleClient()

    # Now run the tests
    runner = unittest.TextTestRunner()
    if len( sys.argv ) > 1:
        suite = unittest.makeSuite(automated, sys.argv[1])
    else:
        suite = unittest.makeSuite(automated)
    runner.run(suite)

