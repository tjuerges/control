import socket
import binascii
from table import *

HOST = ''                 # Symbolic name meaning the local host
PORT = 50007              # Arbitrary non-privileged port

listen_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
listen_socket.bind((HOST, PORT))
listen_socket.listen(1)
connection_socket, addr = listen_socket.accept()
print "Connected by address ", addr
dataList = []
while 1:
#for x in range (10000):

#Send the number of BDB's per packet to the file IFProc_ethernet_data.txt
    data = connection_socket.recv(1)
    if not data: break
    dataList.append(binascii.b2a_hex(data))

ethFile = file("IFProc_ethernet_data.txt", 'w')
BDBCount = -1
for point in dataList:
    if BDBCount == -1:
        BDBnum = BDBtable[point]
        if BDBnum > 0:
            BDBCount = 0
            ByteCount = 0
            ethFile.write("\nBDB count: ")
        ethFile.write("0x" + point + "\n")
    else:
        ByteCount = ByteCount + 1
        if ByteCount == 8:
            BDBCount = BDBCount + 1
            ByteCount = 0
            if BDBCount == (BDBnum + 1):
                BDBCount = -1
ethFile.close()              
connection_socket.close()
