import socket
import binascii
from table import *

HOST = ''                 # Symbolic name meaning the local host
PORT = 50008              # Arbitrary non-privileged port

listen_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
listen_socket.bind((HOST, PORT))
listen_socket.listen(1)
connection_socket, addr = listen_socket.accept()
print "Connected by address ", addr

val = ""
BDBCount = -1
while 1:
#for x in range (10000):
    data = connection_socket.recv(2)
    if not data: break
    point = binascii.b2a_hex(data[0])
    if BDBCount == -1:
        BDBnum = BDBtable[point]
        if BDBnum > 0:
            BDBCount = 0
            ByteCount = 0
            print "\nBDB count: 0x" + point
    else:
        val = val + point
        ByteCount = ByteCount + 1
        if ByteCount == 8:
            BDBCount = BDBCount + 1
            ByteCount = 0
            print "0x" + val
            val = ""
            if BDBCount == (BDBnum + 1):
                BDBCount = -1
                
connection_socket.close()
