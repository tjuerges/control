import socket
import binascii
from table import *

HOST = ''                 # Symbolic name meaning the local host
PORT = 50007              # Arbitrary non-privileged port

listen_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
listen_socket.bind((HOST, PORT))
listen_socket.listen(1)
connection_socket, addr = listen_socket.accept()
print "Connected by address ", addr
BDBCount = -1
while 1:
#for x in range (10000):
    data = connection_socket.recv(1)
    point = binascii.b2a_hex(data)
    if not data: break
    if BDBCount == -1:
        BDBnum = BDBtable[point]
        if BDBnum > 0:
            BDBCount = 0
            ByteCount = 0
            print "BDB count: 0x" + point
    else:
        ByteCount = ByteCount + 1
        if ByteCount == 8:
            BDBCount = BDBCount + 1
            ByteCount = 0
            if BDBCount == (BDBnum + 1):
                BDBCount = -1
connection_socket.close()
