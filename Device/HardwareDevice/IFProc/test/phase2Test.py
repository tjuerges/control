#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$

import sys
import unittest
import struct
import ControlExceptions
from ambManager import *
from time import sleep
import acstime
from TETimeUtil import *

from time import *

class automated(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        unittest.TestCase.__init__(self,methodName)
        self.manager = AmbManager(antenna)
        self.node    = 0x29
        self.channel = 0
     
        self.RWList = []
        self.RWList.append(("DATA_REMAP",    0x82 ,0x06 , 6 , 0, 0x06, 1, 1)) 
        self.RWList.append(("MODULE_IP_ADDR",0x83 ,0x07 , 4 , 0, 0xFF, 1, 0))
        self.RWList.append(("DEST_IP_ADDR",  0x84 ,0x08 , 4 , 0, 0xFF, 1, 0))
        self.RWList.append(("TCP_PORT",      0x85 ,0x0A , 2 , 0, 0xFF, 1, 0))
        self.RWList.append(("BDB_PER_PACKET",0x86 ,0x0B , 1 , 0x20, 0xFF, 1, 0))
        self.RWList.append(("AVERAGE_LENGTH",0x8D ,0x0F , 1 , 0, 0x09, 1, 0))
        self.RWList.append(("GAINS",         0x181,0x101, 6 , 0, 0xFC, 4, 2))
        self.RWList.append(("SIGNAL_PATHS",  0x182,0x103, 1 , 0, 0xFF, 1, 2))


        self.ROList = []
        #Here we add tuples which are Name, RCA, Size
        self.ROList.append(("STATUS"            ,0x01, 4, 0))
        self.ROList.append(("DATA_MONITOR_1"    ,0x03, 4, 0))
        self.ROList.append(("DATA_MONITOR_2"    ,0x04, 8, 0))
        self.ROList.append(("FIFO_DEPTHS"       ,0x05, 4, 0))
        self.ROList.append(("ETHERNET_MAC"      ,0x09, 6, 0))
        self.ROList.append(("48MS_LENGTh"       ,0x0D, 4, 0))
        self.ROList.append(("IFDC_SPI_STATUS"   ,0x10, 2, 0))
        self.ROList.append(("VOLTAGES_1"        ,0x11, 8, 0))
        self.ROList.append(("VOLTAGES_2"        ,0x12, 8, 0))
        self.ROList.append(("TPD_MODULE_CODES"  ,0x79, 8, 0))
        self.ROList.append(("TEMPS_1"           ,0x104,8, 1))
        self.ROList.append(("TEMPS_2"           ,0x105,8, 1))
        self.ROList.append(("IFDC_MODULD_CODES" ,0x179,8, 0))

        self.resetList = []
        # These are NAME, RESET RCA, Monitor RCA, Reset Condition
        self.resetList.append(("TIMING_ERROR",0x8C,0x02,(0,), 1))
        self.resetList.append(("ETHERNET_TIMES",0x8E,0x0E,(0,0,0,0,0,0,0,0), 0))
        self.resetList.append(("AMB_CMD_COUNTER",0x90,0x13,(0,0), 0))
        self.resetList.append(("AMB_INVALID_CMD",0x8F,0x14,(0,0,0,0), 0))
        
        self.validMonitor = []
        for point in self.ROList:
            self.validMonitor.append((point[1], point[3]))
        for point in self.RWList:
            self.validMonitor.append((point[2], point[7]))
        for point in self.resetList:
            self.validMonitor.append((point[2], point[4]))

        self.validMonitor.append((0x00, 0)) #SN Response
        self.validMonitor.append((0x0C, 1)) #Time Point

        self.validMonitor.append((0x30000, 0)) #GET_PROTOCOL_REV_LEVEL
        self.validMonitor.append((0x30001, 0)) #GET_CAN_ERROR
        self.validMonitor.append((0x30002, 0)) #GET_TRANS_NUM
        self.validMonitor.append((0x30003, 0)) #GET_AMBIENT_TEMPERATURE
        self.validMonitor.append((0x30004, 0)) #GET_SW_REV_LEVEL

        self.validControl = []
        for point in self.RWList:
            self.validControl.append((point[0], point[1], point[3]))
        for point in self.resetList:
            self.validControl.append((point[0], point[1], 1))            

        self.validControl.append(("RESET_TPD_BOARD", 0x81, 1)) #RESET_TPD_BOARD
        self.validControl.append(("SET_ALMA_TIME", 0x87, 8)) #SET_ALMA_TIME
        self.validControl.append(("INIT_TCP_CONN", 0x88, 1)) #INIT_TCP_CONN
        self.validControl.append(("START_DATA", 0x89, 1)) #START_DATA
        self.validControl.append(("STOP_DATA", 0x8a, 1)) #STOP_DATA
        self.validControl.append(("START_COMM_TEST", 0x8b, 1)) #START_COMM_TEST        

        self.validControl.append(("RESET_DEVICE", 0x31001, 1)) #RESET_DEVICE        

        self.TEMonitor = []
        self.TEMonitor.append(0x02) #READ_TIMING_ERROR_FLAG
        self.TEMonitor.append(0x06) #READ_DATA_REMAP
        self.TEMonitor.append(0x0c) #READ_ALMA_TIME
        self.TEMonitor.append(0x101) #READ_GAINS
        self.TEMonitor.append(0x103) #READ_SIGNAL_PATHS
        self.TEMonitor.append(0x104) #READ_TEMPS_1
        self.TEMonitor.append(0x105) #READ_TEMPS_2

        self.TEControl = []
        self.TEControl.append((0x82, 6, 0x0f)) #SET_DATA_REMAP
        self.TEControl.append((0x87, 8, 0x55)) #SET_ALMA_TIME
        self.TEControl.append((0x89, 1, 0x00)) #START_DATA
        self.TEControl.append((0x8a, 1, 0x00)) #STOP_DATA
        self.TEControl.append((0x181, 6, 0xfc)) #SET_GAINS
        self.TEControl.append((0x182, 1, 0xff)) #SET_SIGNAL_PATHS        

        self.validIP = []
        self.validIP.append((146, 88, 7, 27))
        self.validIP.append((146, 88, 7, 28))
        self.validIP.append((146, 88, 7, 29))
        self.validIP.append((146, 88, 7, 30))

        self.destIP = (146, 88, 7, 35)

        self.totalIP = []
        self.totalIP.append((0, 0, 0, 0))
        self.totalIP.append((1, 1, 1, 1)) #random invalid IP address
        for point in self.validIP:
            self.totalIP.append(point)

        self.PORT = 50007              # Arbitrary non-privileged port
        self.portlist = [0x0000,]
        self.portlist.append(self.PORT)
        
        self.voltageList1 = []
        self.voltageList1.append(("Digital 1.2V", 1.14, 1.26))
        self.voltageList1.append(("Digital 2.5V", 2.375, 2.625))
        self.voltageList1.append(("Digital 3.3V", 3.135, 3.465))
        self.voltageList1.append(("Ethernet Controller 3.3V", 3.135, 3.465))
        self.voltageList2 = []
        self.voltageList2.append(("Digital 5V", 4.75, 5.25))
        self.voltageList2.append(("Analog 5V", 4.75, 5.25))        
        self.voltageList2.append(("Reference 5V", 4.75, 5.25))
        self.voltageList2.append(("7V Power In", 6.65, 7.35))

        self.dataMonitorList1 = ["TPS0", "TPS1"]
        self.dataMonitorList2 = ["TP-A", "TP-B", "TP-C", "TP-D"]

        self.tempsList1 = []
        self.tempsList1.append("Output Section 1")
        self.tempsList1.append("Output Section 2")
        self.tempsList1.append("Output Section A")
        self.tempsList1.append("Output Section B")
        self.tempsList2 = []
        self.tempsList2.append("Output Section C")
        self.tempsList2.append("Output Section D")
        self.tempsList2.append("Switch Matrix")
        self.tempsList2.append("Communication Module")

        self.invalidNodes = []
        for point in range (2, 16):
            self.invalidNodes.append(point)
        self.invalidNodes.append(22)
        for point in range (24, 27):
            self.invalidNodes.append(point)
        for point in range (73, 80):
            self.invalidNodes.append(point)
        for point in range (88, 96):
            self.invalidNodes.append(point)
        for point in range (112, 156):
            self.invalidNodes.append(point)
        for point in range (576, 640):
            self.invalidNodes.append(point)
        for point in range (768, 2048):
            self.invalidNodes.append(point)        
        
        self.out = ""
        self.failFlag = False

    def __del__(self):
        if self.out != None:
            print self.out
        self.manager = None

    def startTestLog(self, testName):
        self.out+=("\n")
        self.testName = testName
        self.log("Begin")


    def endTestLog(self):
        self.log("End")
        if self.failFlag:
            self.fail()

    def failed(self,msg):
        self.log(msg + " <FAILED>")
        self.failFlag = True
                 
    def log(self,msg):
        self.out += self.testName+": "+msg + "\n"
        
    # --------------------------------------------------
    # Test Definitions

    
    def test01(self):
        '''
        Test one verifies that the device correctly responds to
        broadcast messages and returns the serial number when requested
        by RCA 0x0.

        For recordkeeping the serial number is returned.
        '''
        self.startTestLog("Broadcast Response Test")
	
        try:
            (r,t) = self.manager.getNodes(self.channel)
        except:
            self.failed("Failed to get nodes on bus")          
            
        foundNode = False
        for node in r:
            if node.node == self.node:
                if not foundNode:
                    foundNode = True
                    self.log("Device Serial Number: 0x%x" % node.sn)
                else:
                    self.failed("Node 0x%x responded at least twice" %
                                self.node)

        if foundNode == False:
            self.failed("Node 0x%x did not respond" % self.node)

        try:
            (sn, time) = self.manager.findSN(self.channel,self.node)
        except:
            self.failed("Exception retrieving serial number from node")

        self.endTestLog()


    def test02(self):
        '''
        This test verifies the function of Read/ Set Monitor points
        '''
        self.startTestLog("Read Write Monitor Test")

        for point in self.RWList:
            format = "!%dB" % point[3]
            
            for dataValue in range (point[4], point[5]+1, point[6]):
                dataSeq = []
                for i in range (0, point[3]):
                    dataSeq.append(dataValue)

                try:
                    if (point[7]):
                        t = self.manager.commandTE(self.channel,self.node,point[1], struct.pack(format,*dataSeq))
                    else:
                        t = self.manager.command(self.channel,self.node,point[1], struct.pack(format,*dataSeq))
                except:
                    self.failed("Failed writing to SET_"+point[0])
                
                #Read Data Back in
                try:
                    if (point[7]):
                        (r,t) = self.manager.monitorTE(self.channel,self.node, point[2])   
                    else:
                        (r,t) = self.manager.monitor(self.channel,self.node, point[2])
                    if len(r) != point[3]:
                        self.failed("Incorrect data length returned by: READ_"+point[0])
                    elif (struct.unpack(format,r) != tuple(dataSeq)):
                        #Verify that the Read and Write match
			resp = struct.unpack(format,r)
                        self.failed("Incorrect data returned by: READ_%s (0x%x != 0x%x)"%(point[0],resp[0],dataValue))
                except:
                    self.failed("Error reading from READ_"+point[0])

        self.endTestLog()


    def test03(self):
        '''
        This test verifies our ability to read correct values from
        the Read only Monitor points
        '''
        self.startTestLog("Read Only Monitor Test")

        for point in self.ROList:
            try:
                if (point[3]):
                    (r,t) = self.manager.monitorTE(self.channel, self.node, point[1])
                else:
                    (r,t) = self.manager.monitor(self.channel, self.node, point[1])
                if len(r) != point[2]:
                    self.failed("Incorrect data length returned by: READ_%s (%d != %d)"%(point[0],len(r),point[2]))
            except:
             self.failed("Error reading from READ_"+point[0])   

        self.endTestLog()


    def test04(self):
        '''
        This test tests the READ_DATA_MONITOR points, which should return voltages
        between 0V and 2V.
        '''
        self.startTestLog("READ_DATA_MONITOR Test")

        self.log("READ_DATA_MONITOR_1 voltage test")
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x03 )
            self.failUnless( len( r )==4, "Length of response incorrect for RCA: 0x03" )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated READ_DATA_MONITOR_1 monitor point" )
        resp = struct.unpack( "!2H", r )
        for x in range (0, 2):
            self.log("%s voltage = %fV" % (self.dataMonitorList1[x], resp[x]*2.5/(2**16)))       
            self.failUnless((((resp[x]*2.5/(2**16)) > 0) and ((resp[x]*2.5/(2**16)) < 2)), "%s voltage out of valid range"%(self.dataMonitorList1[x]))
       
        self.log("READ_DATA_MONITOR_2 voltage test")
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x04 )
            self.failUnless( len( r )==8, "Length of response incorrect for RCA: 0x03" )            
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated READ_DATA_MONITOR_2 monitor point" )
        resp = struct.unpack( "!4H", r )
        for x in range (0, 4):
            self.log("%s voltage = %fV" % (self.dataMonitorList2[x], resp[x]*2.5/(2**16)))        
            self.failUnless((((resp[x]*2.5/(2**16)) > 0) and ((resp[x]*2.5/(2**16)) < 2)), "%s voltage out of valid range"%(self.dataMonitorList2[x]))
               
        self.endTestLog()


    def test05(self):
        '''
        This test tests the READ_VOLTAGES points, which should return voltages
        within 5% of their specified voltages.
        '''
        self.startTestLog("READ_VOLTAGES Test")

        self.log("READ_VOLTAGES_1 test")
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x11 )
            self.failUnless( len( r )==8, "Length of response incorrect for RCA: 0x11" )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated READ_VOLTAGES_1 monitor point" )
        resp = struct.unpack( "!4h", r )
        x = 0
        for point in self.voltageList1:
            self.log("%s voltage = %fV" % (point[0], resp[x]*10.0/(2**11)))          
            self.failUnless((((resp[x]*10.0/(2**11)) > point[1]) and ((resp[x]*10/(2**11)) < point[2])), "%s out of valid range"%point[0])
            x = x + 1
            
        self.log("READ_VOLTAGES_2 test")
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x12 )
            self.failUnless( len( r )==8, "Length of response incorrect for RCA: 0x12" )            
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated READ_VOLTAGES_2 monitor point" )
        resp = struct.unpack( "!4h", r )
        x = 0
        for point in self.voltageList2:
            self.log("%s voltage = %fV" % (point[0], resp[x]*10.0/(2**11)))        
            self.failUnless((((resp[x]*10.0/(2**11)) > point[1]) and ((resp[x]*10.0/(2**11)) < point[2])), "%s out of valid range"%point[0])
            x = x + 1
            
        self.endTestLog()


    def test06(self):
        '''
        This test tests the functioning of the READ_48MS_LENGTH monitor point
        '''
        self.startTestLog("READ_48MS_LENGTH Test")
        self.log("Verifies that the 48MS_LENGTH value is exactly 48ms.")
        
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x0d )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated monitoring point 0x0d" )
        resp = struct.unpack( "!I", r )
	time = (resp[0]+1)*8*(10.0**(-6))
        self.log("resp = %d"%resp)
        self.failUnless(resp == 5999999, "The 48ms length is incorrect: %fms" % time)

        self.endTestLog()                     


    def test07(self):
        '''
        This test tests the SET_AVERAGE_LENGTH and
        READ_DATA_AVE_LENGTH control and monitor points
        '''
        self.startTestLog("DATA_AVE_LENGTH Test")
        
        self.log('''Verify that if SET_AVERAGE_LENGTH is set to an incorrect value (>0x9),
                    that the average length is set to the default of 512 samples (0x9).''')

        for data in range (0x0a, 0x100):
            try:
                t = self.manager.command(self.channel, self.node, 0x8d, struct.pack("!B", data))
            except:
                self.failed("Error writing to SET_AVERAGE_LENGTH control point")        
            try:
                ( r, t ) = self.manager.monitor( self.channel, self.node, 0x0f )
                self.failUnless( len( r ) == 1, "Length of response incorrect for RCA:0x0f" )
            except ControlExceptions.CAMBErrorEx:
                self.fail( "Exception generated READ_DATA_AVE_LENGTH monitoring point" )	
            resp = struct.unpack( "!B", r )
	    if (resp[0] != 9):	
            	self.failed("Average length set to %d samples (0x%x) at data value 0x%x"%((2**resp[0]),resp[0],data))        
        
        self.endTestLog()


    def test08(self):
        '''
        This test tests the SET_GAINS and
        READ_GAINS control and monitor points
        '''
        self.startTestLog("GAINS Test")
        
        self.log("Verify that if SET_GAINS is set to an incorrect value,")
        self.log("that the gains are set to the closest correct value.")

        gain = ["GS0", "GS1", "GA", "GB", "GC", "GD"]
        for data in range (0x00, 0x100):
            dataSeq = [data, data, data, data, data, data]   
            try:             
                t = self.manager.commandTE(self.channel, self.node, 0x181, struct.pack("!6B", *dataSeq))
            except:
                self.failed("Error writing to SET_GAINS control point")
            
            try:
                ( r, t ) = self.manager.monitorTE( self.channel, self.node, 0x101 )
                self.failUnless( len( r ) == 6, "Length of response incorrect for RCA:0x101" )
            except ControlExceptions.CAMBErrorEx:
                self.fail( "Exception generated READ_GAINS monitoring point" )	
            resp = struct.unpack( "!6B", r )
            for x in range(0,6):
                if (resp[x] != (data & 0xfc)):
                    self.failed("%s (byte %d) set to wrong gain at data value 0x%x\ndata = 0x%x, should be = 0x%x "%(gain[x],x,data,resp[x],(data&0xfc)))
        
        self.endTestLog()         


    def test09( self ):
	'''
	This test verifies that the RESET_AMB_INVALID_CMD control point
        does not reset the value of the AMB command counter.
	'''
	self.startTestLog( "Test 10: RESET_AMB_INVALID_CMD test" )
	self.log('''Verify that the RESET_AMB_INVALID_CMD control point
                    does not reset the value of the AMB command counter.
                 ''')
        
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x13 )
            self.failUnless( len( r ) == 2, "READ_AMB_CMD_COUNTER returned incorrect length value" )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated monitoring READ_AMB_CMD_COUNTER point" )         
	respb = struct.unpack( "!H", r )
	self.log("Command Counter before reset: %d" % respb)

        data = 0xff
        try:
            t = self.manager.command(self.channel, self.node, 0x8f, struct.pack("!B", data))
        except:
            self.fail("Error writing to RESET_AMB_INVALID_CMD")        

        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x13 )
            self.failUnless( len( r ) == 2, "READ_AMB_CMD_COUNTER returned incorrect length value" )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated monitoring READ_AMB_CMD_COUNTER point" )         
	respa = struct.unpack( "!H", r )
	self.log("Command Counter after reset: %d" % respa)        

        self.failUnless((respb[0] + 1) == respa[0], "RESET_AMB_INVALID_CMD reset the value of the AMB command counter.")
        
	self.endTestLog()


    def test10( self ):
	'''
	This test verifies an appropiate response from the READ_AMB_CMD_COUNTER monitor point.
	'''
	self.startTestLog( "Test 29: READ_AMB_CMD_COUNTER test" )
	self.log('''Verify that the correct number of commands over the AMB bus since startup
                    or RESET_AMB_CMD_COUNTER is printed to the screen.
                 ''')

        self.log("Verify that the following command count is correct")
        respb = self.read_command_counter()

        self.log("Verify that the READ_AMB_CMD_COUNTER monitor point increases by one after a command.")
        d = [0xff, 0xff, 0xff, 0xff]
        try:
            t = self.manager.command(self.channel, self.node, 0x83, struct.pack("!4B", *d))
        except:
            self.failed("Error writing to rca 0x83")   
        respa = self.read_command_counter()
        if respa != (respb + 1):
            self.failed("READ_AMB_CMD_COUNTER did not increase after a command.")
        respb = respa
        
        self.log("Verify that the READ_AMB_CMD_COUNTER monitor point stays the same after a monitor")
        try:
            ( r, t ) = self.manager.monitorTE( self.channel, self.node, 0x11 )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated monitoring point 0x11" ) 
        respa = self.read_command_counter()
        if respa != respb:
            self.failed("READ_AMB_CMD_COUNTER increased after a monitor.")
        respb = respa
        
        self.log("Verify that the READ_AMB_CMD_COUNTER resets after RESET_AMB_CMD_COUNTER")
        try:
            t = self.manager.command( self.channel, self.node, 0x90, struct.pack("!B", 0xff) )
        except:
            self.fail( "Exception generated control point 0x90" ) 
        respb = self.read_command_counter()
        if respb != 0:
            self.failed("RESET_AMB_CMD_COUNTER failed to resed READ_AMB_CMD_COUNTER.")

        self.log("Verify that the READ_AMB_CMD_COUNTER monitor point increases by one after a command.")
        d = [0xff, 0xff, 0xff, 0xff]
        try:
            t = self.manager.command(self.channel, self.node, 0x83, struct.pack("!4B", *d))
        except:
            self.failed("Error writing to rca 0x83")   
        respa = self.read_command_counter()
        if respa != (respb + 1):
            self.failed("READ_AMB_CMD_COUNTER did not increase after a command.")
        respb = respa

        self.log("Verify that the READ_AMB_CMD_COUNTER resets upon reset of the TPD Board")
        try:
            t = self.manager.command( self.channel, self.node, 0x81, struct.pack("!B", 0xff) )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated monitoring point 0x11" )
        respb = self.read_command_counter()
        if respb != 0:
            self.failed("RESET_TPB_BOARD failed to resed READ_AMB_CMD_COUNTER.")        

        self.log("Verify that the READ_AMB_CMD_COUNTER monitor point increases by one after each valid command.")
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x13 )
            self.failUnless( len( r ) == 2, "READ_AMB_CMD_COUNTER returned incorrect length value" )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated monitoring point 0x13" ) 
        respb = struct.unpack( "!H", r )
        for point in self.validControl:
            if ((point[1] != 0x90) and (point[1] != 0x81) and (point[1] != 0x31001)):
                fmt = "!%dB"%point[2]
                d = []
                for x in range(point[2]):
                    d.append(0x01)
                try:
                    t = self.manager.command(self.channel, self.node, point[1], struct.pack(fmt, *d))
                except:
                    self.failed("Error writing to %s control point"%point[0])   
                try:
                    ( r, t ) = self.manager.monitor( self.channel, self.node, 0x13 )
                    self.failUnless( len( r ) == 2, "READ_AMB_CMD_COUNTER returned incorrect length value" )
                except ControlExceptions.CAMBErrorEx:
                    self.fail( "Exception generated monitoring point 0x13" ) 
                respa = struct.unpack( "!H", r )
                if (respa[0] != (respb[0] + 1)):
                    self.failed("%s control point did not correctly increment READ_AMB_CMD_COUNTER"%point[0])
                respb = respa

        self.log("%d valid commands have been generated after reset of TPD board."%(len(self.validControl)-3))
        self.read_command_counter()

        self.log("Verify whether an invalid RCA command increases the command counter")
        try:
            t = self.manager.command(self.channel, self.node, 0x15, struct.pack("!B", 0xff))
        except:
            self.failed("Error writing to rca 0x15")   
        self.read_command_counter()

        self.log("Verify whether an incorrectly formatted command increases the command counter")
        try:
            t = self.manager.command(self.channel, self.node, 0x83, struct.pack("!B", 0xff))
        except:
            self.failed("Error writing to rca 0x83")   
        self.read_command_counter()

        self.log("Verify that the READ_AMB_CMD_COUNTER resets upon commanding RESET_DEVICE")
        try:
            t = self.manager.command( self.channel, self.node, 0x31001, struct.pack("!B", 0xff) )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated monitoring point 0x31001" )
        resp = self.read_command_counter()
        if resp != 0:
            self.failed("RESET_DEVICE failed to resed READ_AMB_CMD_COUNTER.")         
        
	self.endTestLog( )


    def test11( self ):
	'''
	This test verifies an appropiate response from the GET_CAN_ERROR monitor point.
	'''
	self.startTestLog( "Test 7: GET_CAN_ERROR test" )
	
	try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x30001 )
            self.failUnless( len( r ) == 4, "GET_CAN_ERROR returned incorrect length value" )
        except:
            self.fail("Exception generated monitor point 0x30001")
			
        resp = struct.unpack( "!HBB", r )
	self.failUnless( resp[0] == 0x0000, "Bytes 0 and 1 are 0x%x" % resp[0])
	self.failUnless( resp[1] == 0x00, "Byte 2 is 0x%x" % resp[1])
	self.failUnless( (resp[2] == 0x18 or resp[2] == 0x38), "Byte 3 is 0x%x" % resp[2])

	self.endTestLog()   


    def test12( self ):
	'''
	This test verifies an appropiate response from the GET_TRANS_NUM monitor point.
	'''
	self.startTestLog( "Test 19: GET_TRANS_NUM test" )
	self.log('''Verify that the correct number of transactions handled by the IFDC since power-up is printed to the screen.''')

        self.log("Verify that the following transaction count is correct")
        self.read_transaction_counter()

        self.log("Verify that the GET_TRANS_NUM resets after RESET_DEVICE")
        try:
            t = self.manager.command( self.channel, self.node, 0x31001, struct.pack("!B", 0xff) )
        except:
            self.fail( "Exception generated RESET_DEVICE control point" ) 
        resp = self.read_transaction_counter()
        if resp != 0:
            self.failed("RESET_DEVICE did not reset transaction counter")
        
        self.log("Verify that the GET_TRANS_NUM monitor point increases by one after each valid command.")
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x30002 )
            self.failUnless( len( r ) == 4, "GET_TRANS_NUM returned incorrect length value" )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated monitoring point 0x30002" ) 
        respb = struct.unpack( "!I", r )
        for point in self.validControl:
            if ((point[1] != 0x90) and (point[1] != 0x81) and (point[1] != 0x31001)):
                fmt = "!%dB"%point[2]
                d = []
                for x in range(point[2]):
                    d.append(0x01)
                try:
                    t = self.manager.command(self.channel, self.node, point[1], struct.pack(fmt, *d))
                except:
                    self.failed("Error writing to %s control point"%point[0])   
                try:
                    ( r, t ) = self.manager.monitor( self.channel, self.node, 0x30002 )
                    self.failUnless( len( r ) == 4, "GET_TRANS_NUM returned incorrect length value" )
                except ControlExceptions.CAMBErrorEx:
                    self.fail( "Exception generated monitoring point 0x30002" ) 
                respa = struct.unpack( "!I", r )
                if (respa[0] != (respb[0] + 2)):
                    self.failed("%s control point did not correctly increment GET_TRANS_NUM"%point[0])
                respb = respa

        self.log("Verify that the GET_TRANS_NUM monitor point increases by one after each valid monitor.")
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x30002 )
            self.failUnless( len( r ) == 4, "GET_TRANS_NUM returned incorrect length value" )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated monitoring point 0x30002" ) 
        respb = struct.unpack( "!I", r )
        for point in self.validMonitor:
            try:
                (r, t) = self.manager.monitor(self.channel, self.node, point[0])
            except:
                self.failed("Error generating monitor point 0x%x"%point[0])   
            try:
                ( r, t ) = self.manager.monitor( self.channel, self.node, 0x30002 )
                self.failUnless( len( r ) == 4, "GET_TRANS_NUM returned incorrect length value" )
            except ControlExceptions.CAMBErrorEx:
                self.fail( "Exception generated monitoring point 0x30002" ) 
            respa = struct.unpack( "!I", r )
            if (respa[0] != (respb[0] + 2)):
                self.failed("0x%x monitor point did not correctly increment GET_TRANS_NUM"%point[0])
            respb = respa                

        self.read_transaction_counter()
        self.log("Verify whether an invalid RCA command increases the transaction counter")
        try:
            t = self.manager.command(self.channel, self.node, 0x15, struct.pack("!B", 0xff))
        except:
            self.failed("Error writing to rca 0x15")   
        self.read_transaction_counter()

        self.log("Verify whether an incorrectly formatted command increases the command counter")
        try:
            t = self.manager.command(self.channel, self.node, 0x83, struct.pack("!B", 0xff))
        except:
            self.failed("Error writing to rca 0x83")   
        self.read_transaction_counter() 
		
	self.endTestLog()


    def test13( self ):
	'''
	This test tests the generic control point for the IFDC.
	'''
	self.startTestLog( "Test 14: Generic control point test" )
	self.log("Generate control point and verify that the IFDC resets.")

        resp = self.test_for_reset(0)
	self.log("Number of transactions since power-up: %d" % resp)

	try:
            t = self.manager.command(self.channel, self.node, 0x31001, struct.pack("!B", 0x01))
        except:
            self.fail("Exception generating control point 0x31001")
	
        self.log("Verify that the IFDC reset")
        resp = self.test_for_reset(0)
	self.log("Number of transactions since power-up: %d" % resp)
        self.failUnless(resp == 0, "Generic control point did not generate a reset.")
					
	self.endTestLog()


#----------------------------------------------------------------------------
#The following functions are called by some of the test functions above
#----------------------------------------------------------------------------


    def test_for_reset( self, x ):
        if (x == 0):
	    try:
                ( r, t ) = self.manager.monitor( self.channel, self.node, 0x30002 )
                self.failUnless( len( r )==4, "Length of response incorrect for RCA: 0x30002" )
            except ControlExceptions.CAMBErrorEx:
                self.fail( "Exception generated monitoring point 0x30002" )
            resp = struct.unpack( "!I", r )
        else:
            try:
                ( r, t ) = self.manager.monitor( self.channel, self.node, 0x13 )
                self.failUnless( len( r )==2, "Length of response incorrect for RCA: 0x13" )
            except ControlExceptions.CAMBErrorEx:
                self.fail( "Exception generated monitoring point 0x13" )
            resp = struct.unpack( "!H", r )
        return resp


    def read_transaction_counter(self):
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x30002 )
            self.failUnless( len( r )==4, "Length of response incorrect for RCA: 0x30002" )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated monitoring point 0x30002" )
        resp = struct.unpack( "!I", r )
        self.log("Transaction count: %d" % resp)
        return resp[0]


    def read_command_counter(self):
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x13 )
            self.failUnless( len( r ) == 2, "READ_AMB_CMD_COUNTER returned incorrect length value" )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated monitoring point 0x13" ) 
	resp = struct.unpack( "!H", r )
	self.log("Command count: %d" % resp)        
        return resp[0]


    def initialize_switches(self):
        '''
        This is used to initialize the switches and the attenuators.  It needs to be done after bootup, and
        is called by tests 36, 37, and 38.
        '''

        #This exercises the switches set by SET_SIGNAL_PATHS.  It probably doesn't need to be done,
        #but it's done anyways, just to be safe.
        data = [0x00, 0xff]
        for point in data:
            try:
                t = self.manager.command(self.channel, self.node, 0x182, struct.pack("!B", point))
            except:
                self.fail("Exception generating SET_SIGNAL_PATHS control point")

        #This sets all of the gains/attenuations to 31.5dB, and then to 0dB.  The attenuators won't
        #work unless this is done after startup of the IFDC.
        data = [[0xfc, 0xfc, 0xfc, 0xfc, 0xfc, 0xfc], [0x00, 0x00, 0x00, 0x00, 0x00, 0x00]]
        for point in data:
            try:
                t = self.manager.command(self.channel, self.node, 0x181, struct.pack("!6B", *point))
            except:
                self.fail("Exception generating SET_GAINS control point")
        
        
class interactive(automated):
    def __init__(self, methodName="runTest"):
        automated.__init__(self,methodName)

    def tearDown(self):
        self.out = None

    def startTestLog(self, testName):
        print ""
        self.testName = testName
        self.log("Begin")

    def failed(self,msg):
        self.log(msg + " <FAILED>")
        self.failFlag = True
                 
    def log(self,msg):
        #print self.testName+": "+msg
         print msg
         
    # --------------------------------------------------
    # Test Definitions


    def test01(self):
        '''
        Test one verifies that the device correctly responds to
        broadcast messages and returns the serial number when requested
        by RCA 0x0.

        The interactive version of this test ensures that the Bin Location
        is correctly mapped to the Node ID.
        '''
        self.startTestLog("Test 1: Broadcast Response Test")
        testedSN = None;

        for idx in range(3,-1,-1):
            self.log("Place 2nd LO module in bin corresponding to 0x%02x" %
                     (0x40+idx))
            self.log("Move this module with the power off and turn the power on when ready")
            powerOff = True
            while powerOff:
                rv = raw_input("press <enter> when this is done:")
                try:
                    (r,t) = self.manager.getNodes(self.channel)
                except:
                    self.fail("Unable to get nodes on bus")
                if (len(r) == 0):
                    self.log("No nodes found. Did you turn the power on?")
                else:
                    powerOff = False;
            foundNode = False
            for node in r:
                if node.node == self.node+idx:
                    if not foundNode:
                        foundNode = True
                        self.log("Device Serial Number: 0x%x" % node.sn)
                        if testedSN == None:
                            testedSN = node.sn
                        else:
                            self.failUnless(testedSN == node.sn,
                                            "Serial Number changed on device")
                    else:
                        self.fail("Node 0x%x responded at least twice" %
                                  self.node)
                    
            self.failUnless(foundNode, "Node 0x%x did not respond" %
                            (self.node+idx))
            
            try:
                (sn, time) = self.manager.findSN(self.channel,self.node+idx)
            except:
                self.fail("Exception retrieving serial number from node")

        self.endTestLog()


    def test14(self):
        '''
        This test tests the functioning of the reset /RO monitor points
        '''
        self.startTestLog("Reset Point Test")

        self.log('''Before running this test, make sure that all monitor points
                    are not in reset position.  These include the following:
                    TIMING_ERROR_FLAG, MAX_ETHERNET_TIMES, AMB_CMD_COUNTER,
                    and AMB_INVALID_CMD''')

        rv = raw_input("Press <enter> once all monitor points are not in reset position.")
        
        dataSeq = [0x00, 0x01] 
        for point in self.resetList:
            try:
                if (point[4]):
                    (r,t) = self.manager.monitorTE(self.channel,self.node, point[2])
                else:
                    (r,t) = self.manager.monitor(self.channel,self.node, point[2])
                if struct.unpack("!%dB" % len(point[3]),r) == point[3]:
                    self.failed(point[0] + " monitor point already in reset condition")
            except:
                self.failed("Error reading from " + point[0] + "monitor point")

            try:
                t = self.manager.command(self.channel, self.node, point[1], struct.pack("!B", dataSeq[0]))
            except:
                self.failed("Error writing to " + point[0] + "reset point")

            try:
                if (point[4]):
                    (r,t) = self.manager.monitorTE(self.channel,self.node, point[2])
                else:
                    (r,t) = self.manager.monitor(self.channel,self.node, point[2])
                if struct.unpack("!%dB" % len(point[3]),r) == point[3]:
                    self.failed(point[0] + ": Invalid control point caused reset condition")
            except:
                self.failed("Error reading from " + point[0] +" monitor point")

            try:
                t = self.manager.command(self.channel, self.node, point[1], struct.pack("!B", dataSeq[1]))
            except:
                self.failed("Error writing to " + point[0] + "reset point")

            try:
                if (point[4]):
                    (r,t) = self.manager.monitorTE(self.channel,self.node, point[2])
                else:
                    (r,t) = self.manager.monitor(self.channel,self.node, point[2])
                if struct.unpack("!%dB" % len(point[3]),r) != point[3]:
                    self.failed(point[0] + ": Valid control point failed to reset condition")
            except:
                self.failed("Error reading from " + point[0] +" monitor point")                
           
        self.endTestLog()


    def test15( self ):
	'''
	This test verifies an appropiate response from the
        READ_AMB_INVALID_CMD monitor point.
	'''
	self.startTestLog( "Test 09: READ_AMB_INVALID_CMD test" )
	self.log('''Verify that the READ_AMB_INVALID_CMD monitor point returns the correct information
                    for the last invalid command to the IFDC.
                 ''')

        rv = raw_input("Press <enter> to test that valid commands are not reported.")
        try:
            t = self.manager.command(self.channel, self.node, 0x15, struct.pack("!H", 0xffff))
        except:
            self.fail("Error writing to rca 0x15")
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x13 )
            self.failUnless( len( r ) == 2, "READ_AMB_CMD_COUNTER returned incorrect length value" )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated monitoring point 0x13" )
        begin_counter = struct.unpack( "!H", r )            
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x14 )
            self.failUnless(len( r ) == 4, "READ_AMB_INVALID_CMD returned incorrect length value") 
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated monitoring point 0x14" )
        resp = struct.unpack( "!2BH", r )
        self.failUnless(resp[0] == 0x15, "Byte 0 returned incorrect value: rca = 0x15, byte 0 = 0x%x"%resp[0])
        self.failUnless(resp[1] == 0xff, "Byte 1 returned incorrect value: data = 0xff, byte 1 = 0x%x"%resp[1])
        self.failUnless(resp[2] == begin_counter[0], "Bytes 2,3 returned incorrect value: begin_counter = 0x%x, bytes 2,3 = 0x%x"%(begin_counter[0], resp[2]))

        self.log("Beginning values for the READ_INVALID_CMD monitor point:")
        self.log("RCA = 0x%x"%resp[0])
        self.log("Data = 0x%x"%resp[1])
        self.log("Counter = 0x%x"%resp[2])
        self.log("")
    
        for point in self.validControl:
            if (point[1] != 0x8f) and (point[1] != 0x81):
                rv = raw_input("Press <enter> for next valid control point")
                fmt = "!%dB"%point[2]
                data = []
                for x in range(point[2]):
                    data.append(0x01)
                try:
                    t = self.manager.command(self.channel, self.node, point[1], struct.pack(fmt, *data))
                except:
                    self.fail("Error writing to %s"%point[0])

                try:
                    ( r, t ) = self.manager.monitor( self.channel, self.node, 0x13 )
                    self.failUnless( len( r ) == 2, "READ_AMB_CMD_COUNTER returned incorrect length value" )
                except ControlExceptions.CAMBErrorEx:
                    self.fail( "Exception generated monitoring point 0x13" )
                cmd_counter = struct.unpack( "!H", r )
                   
                try:
                    ( r, t ) = self.manager.monitor( self.channel, self.node, 0x14 )
                    self.failUnless(len( r ) == 4, "READ_AMB_INVALID_CMD returned incorrect length value") 
                except ControlExceptions.CAMBErrorEx:
                    self.fail( "Exception generated monitoring point 0x14" )
                resp = struct.unpack( "!2BH", r )

                self.log("%s:"%point[0])
                self.log("Data Sent: RCA = 0x%x, Data = 0x01, Counter = 0x%x"%(point[1],cmd_counter[0]))
                self.log("Data Read: RCA = 0x%x, Data = 0x%x, Counter = 0x%x"%(resp[0],resp[1],resp[2]))
         
        rv = raw_input("Press <enter> to test that invalid commands are reported.")
        tempControl = []
        for point in self.validControl:
            tempControl.append(point[1])

        for rca in range(0x40000):
            if rca not in tempControl:
                data = rca & 0xffff
                try:
                    t = self.manager.command(self.channel, self.node, rca, struct.pack("!H", data))
                except:
                    self.fail("Error writing to rca 0x%x"%rca)
                    
                try:
                    ( r, t ) = self.manager.monitor( self.channel, self.node, 0x13 )
                    self.failUnless( len( r ) == 2, "READ_AMB_CMD_COUNTER returned incorrect length value" )
                except ControlExceptions.CAMBErrorEx:
                    self.fail( "Exception generated monitoring point 0x13" )
                cmd_counter = struct.unpack( "!H", r )

                #The following command is commanded in order to verify that the comand counter
                #in bytes 2,3 of AMB_INVALID_CMD are not just set to the current command counter.
                d = [0x00, 0x00, 0x00, 0x00]
                try:
                    t = self.manager.command(self.channel, self.node, 0x83, struct.pack("!4B", *d))
                except:
                    self.fail("Error writing to rca 0x83")
                    
                try:
                    ( r, t ) = self.manager.monitor( self.channel, self.node, 0x14 )
                    self.failUnless(len( r ) == 4, "READ_AMB_INVALID_CMD returned incorrect length value") 
                except ControlExceptions.CAMBErrorEx:
                    self.fail( "Exception generated monitoring point 0x14" )
                resp = struct.unpack( "!2BH", r )

                if (resp[0] != (rca & 0xff)):
                    self.failed("Byte 0 returned incorrect value: rca = 0x%x, byte 0 = 0x%x"%(rca, resp[0]))                    
                if (resp[1] != (data & 0xff)):
                    self.failed("Byte 1 returned incorrect value: rca = 0x%x, data = 0x%x, byte 1 = 0x%x"%(rca, data, resp[1]))
                if (resp[2] != cmd_counter[0]):
                    self.failed("Bytes 2,3 returned incorrect value: rca = 0x%x, command counter = 0x%x, bytes 2,3 = 0x%x"%(rca, cmd_counter[0], resp[2]))      
        
	self.endTestLog() 

  
    def test16( self ):
        '''
        This test verifies an appropriate response from the READ_STATUS monitor point.
        '''
        self.startTestLog("READ_STATUS Test")

        self.log('''Verify that the bits in READ_STATUS are set correctly.  
                    Some of the bits in byte 0 of READ_STATUS are tested 
                    in other tests.''')
        
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x01 )
            self.failUnless( len( r )==4, "Length of response incorrect for RCA: 0x01" )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated READ_STATUS monitor point" )
        resp = struct.unpack( "!4B", r )
        
        self.log("Verify whether the following byte values are correct:")
        self.log("STATUS monitor returned 0x%x for byte 1"%resp[1])
        self.log("STATUS monitor returned 0x%x for byte 2"%resp[2])
        self.log("STATUS monitor returned 0x%x for byte 3"%resp[3])      

        self.log("Verify the correctness of byte 0")
        if (resp[0] & 0x01) == 0x01:
            self.log("Ethernet connected")
        else:
            self.log("Ethernet not connected")

        tr = ["10 MB/s", "100 MB/s"]
        for x in range(2):
            rv = raw_input ("Set the ethernet transfer rate to %s, and press <enter> to verify that it is set correctly"%(tr[x]))
            try:
                ( r, t ) = self.manager.monitor( self.channel, self.node, 0x01 )
                self.failUnless( len( r )==4, "Length of response incorrect for RCA: 0x01" )
            except ControlExceptions.CAMBErrorEx:
                self.fail( "Exception generated READ_STATUS monitor point" )
            resp = struct.unpack( "!4B", r )
            self.log("Bit 1 (0 -> 10Mb/s, 1 -> 100Mb/s) = %d"%((resp[0]&0x02)>>1))

        rv = raw_input("Press <enter> to establish connection and disconnect TCP by command")
        self.check_for_connection()
        resp = self.establish_connection("!B", [0xff])
        self.failUnless((resp & 0x04) == 0x04, "TCP connection was not made")
        self.failUnless((resp & 0x10) == 0x00, "Data is being transfered before START_DATA was commanded")        
        resp = self.begin_transfer("!B", [0xff])
        self.failUnless((resp & 0x10) == 0x10, "Data did not begin to transfer.")
        
        resp = self.end_transfer("!B", [0xff])
        self.failUnless((resp & 0x10) == 0x00, "Data is being transfered after STOP_DATA was commanded")
        self.failUnless((resp & 0x04) == 0x00, "TCP connection was not closed by STOP_DATA")
        
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x01 )
            self.failUnless( len( r )==4, "Length of response incorrect for RCA: 0x01" )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated READ_STATUS monitor point" )
        resp = struct.unpack( "!4B", r )        
        self.log("Bit 5 should be 1, and bit 6 should be 0")
        self.log("Bit 5 (TCP Disconnected by Command) = %d"%((resp[0]&0x20)>>5))
        self.log("Bit 6 (TCP Disconnected by Error) = %d"%((resp[0]&0x40)>>6))
       

        self.log("Press <enter> to establish connection test the TCP disconnected by error")
        self.check_for_connection()
        resp = self.establish_connection("!B", [0xff])
        self.failUnless((resp & 0x04) == 0x04, "TCP connection was not made")
        print resp
        self.failUnless((resp & 0x10) == 0x00, "Data is being transfered before START_DATA was commanded")
        
        resp = self.begin_transfer("!B", [0xff])
        self.failUnless((resp & 0x10) == 0x10, "Data did not begin to transfer.")
        
        rv = raw_input("Cause the TCP to be disconnected by error, then press <enter>")
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x01 )
            self.failUnless( len( r )==4, "Length of response incorrect for RCA: 0x01" )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated READ_STATUS monitor point" )
        resp = struct.unpack( "!4B", r )
        if (resp & 0x04):
            self.log("TCP is still connected")
        else:
            self.log("TCP is no longer connected")
        if (resp & 0x10):
            self.log("Data transfer is active")
        else:
            self.log("Data transfer is no longer active")
        self.log("Bit 5 (TCP Disconnected by Command) = %d"%((resp[0]&0x20)>>5))
        self.log("Bit 6 (TCP Disconnected by Error) = %d"%((resp[0]&0x40)>>6))
        
        rv = raw_input("Press <enter> to command STOP_DATA")        
        resp = self.end_transfer("!B", [0xff])
        self.failUnless((resp & 0x10) == 0x00, "Data is still being transfered")
        self.failUnless((resp & 0x04) == 0x00, "TCP connection was not closed")               
        self.log("Bit 5 should be 0, and bit 6 should be 1")
        self.log("Bit 5 (TCP Disconnected by Command) = %d"%((resp[0]&0x20)>>5))
        self.log("Bit 6 (TCP Disconnected by Error) = %d"%((resp[0]&0x40)>>6))

        rv = raw_input("Press <enter> to establish connection and test Ethernet Connected (bit 0)")
        self.check_for_connection()
        resp = self.establish_connection("!B", [0xff])
        self.failUnless((resp & 0x04) == 0x04, "TCP connection was not made")
        self.failUnless((resp & 0x10) == 0x00, "Data is being transfered before START_DATA was commanded")        
        resp = self.begin_transfer("!B", [0xff])
        self.failUnless((resp & 0x10) == 0x10, "Data did not begin to transfer.")

        self.log("Unplug the ethernet cable")
        rv = raw_input("Press <enter> to check status")
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x01 )
            self.failUnless( len( r )==4, "Length of response incorrect for RCA: 0x01" )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated READ_STATUS monitor point" )
        resp = struct.unpack( "!4B", r )
        if (resp & 0x04):
            self.log("TCP is still connected")
        else:
            self.log("TCP is no longer connected")
        if (resp & 0x10):
            self.log("Data transfer is active")
        else:
            self.log("Data transfer is no longer active")
        self.log("Bit 5 (TCP Disconnected by Command) = %d"%((resp[0]&0x20)>>5))
        self.log("Bit 6 (TCP Disconnected by Error) = %d"%((resp[0]&0x40)>>6))

        self.log("Plug the ethernet cable back in and verify that the data transfer begins again.")
        rv = raw_input("Press <enter> to check status")
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x01 )
            self.failUnless( len( r )==4, "Length of response incorrect for RCA: 0x01" )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated READ_STATUS monitor point" )
        resp = struct.unpack( "!4B", r )
        if (resp & 0x04):
            self.log("TCP is still connected")
        else:
            self.log("TCP is no longer connected")
        if (resp & 0x10):
            self.log("Data transfer is active")
        else:
            self.log("Data transfer is no longer active")
        self.log("Bit 5 (TCP Disconnected by Command) = %d"%((resp[0]&0x20)>>5))
        self.log("Bit 6 (TCP Disconnected by Error) = %d"%((resp[0]&0x40)>>6))                
        
        resp = self.end_transfer("!B", [0xff])
        self.failUnless((resp & 0x10) == 0x00, "Data is being transfered after STOP_DATA was commanded")
        self.failUnless((resp & 0x04) == 0x00, "TCP connection was not closed by STOP_DATA")               
        
        self.endTestLog()
        

    def test17(self):
        '''
        This test tests the functioning of the RESET_TPD_BOARD control point
        '''
        self.startTestLog("RESET_TPD_BOARD Test")
        self.log('''Verify that the board resets when supposed to, and
                    does not reset when not supposed to by checking the
                    value of the READ_AMB_CMD_COUNTER monitor point.
                 ''')

        rv = raw_input("Press <enter> to generate monitor point")
        resp = self.test_for_reset(1)
        self.log('''Number of transactions = %d''' % resp )

        format = "!B" 
        dataSeq = [0x00, 0x01, 0xfe, 0xff]

        for x in range(0, 4):
            currVal = dataSeq[x]
            try:
                t = self.manager.command(self.channel, self.node, 0x81, struct.pack(format, currVal))
            except:
                self.failed("Error writing to RESET_TPD_BOARD control point")

            self.log('''Data sent with control point: 0x%x''' % currVal)
            rv = raw_input("Press <enter> to generate monitor point")
            resp = self.test_for_reset(1)
            self.log('''Number of transactions = %d''' % resp )
            rv = raw_input("Press <enter> to continue")

        self.endTestLog()


    def test18(self):
        '''
        This test tests the functioning of the SET_ALMA_TIME control point and READ_ALMA_TIME monitor point
        '''
        self.startTestLog("SET/READ_ALMA_TIME Test")
    
        self.log('''Verify that SET_ALMA_TIME sets the timestamp
                    and that READ_ALMA_TIME reads the timestamp.
                    Also verify that this occurs in accordance
                    with the Timing Event.''')

        self.check_for_connection()
        self.establish_connection("!B", [0xff])
        self.begin_transfer("!B", [0xff])
        
        try:
            ( r, t ) = self.manager.monitorTE( self.channel, self.node, 0x0c )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated monitoring point 0x0c" )
	
	rt = struct.unpack("!2I", r)
	resp = rt[1] + (rt[0]<<32)  
	self.log("beginning timestamp = 0x%x (%dns)"%(resp,resp))   
        self.log("Verify that the correct timestamp is at the beginning of each ethernet packet")
        
        format = "!8B"
        dataSeq = []
        dataSeq.append([0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])        
        dataSeq.append([0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55])
        dataSeq.append([0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff])
        for x in range(3):
            rv = raw_input("Press <enter> to continue to next ALMA time value")
            self.log("Wrote 8 bytes of 0x%x to SET_ALMA_TIME control point"%dataSeq[x][0])
            try:
                t = self.manager.commandTE(self.channel, self.node, 0x87, struct.pack(format, *dataSeq[x]))
            except:
                self.failed("Error writing to SET_ALMA_TIME control point")
            
            try:
                ( r, t ) = self.manager.monitorTE( self.channel, self.node, 0x0c )
            except ControlExceptions.CAMBErrorEx:
                self.fail( "Exception generated monitoring point 0x0c" )
            rt = struct.unpack( "!2I", r )
            resp = rt[1] + (rt[0]<<32)
            self.log("new timestamp = 0x%x (%dns)"%(resp,resp))
            self.log("Verify that the correct timestamp is at the beginning of each ethernet packet")        
            rv = raw_input("Press <enter> to see timestamp again")
            try:
                ( r, t ) = self.manager.monitorTE( self.channel, self.node, 0x0c )
            except ControlExceptions.CAMBErrorEx:
                self.fail( "Exception generated monitoring point 0x0c" )
            rt = struct.unpack( "!2I", r )
            resp = rt[1] + (rt[0]<<32)
            self.log("new timestamp = 0x%x (%dns)"%(resp,resp))
            self.log("Verify that the correct timestamp is at the beginning of each ethernet packet")
            
        self.end_transfer("!B", [0xff])
        
        self.endTestLog()        


    def test19(self):
        '''
        This test tests the functioning of the READ_FIFO_DEPTHS monitor point
        '''
        self.startTestLog("READ_FIFO_DEPTHS Test")
        self.log("Verify that the FIFO Depths are not above the maximum allowed values, and that they are reasonable.")
                
	self.check_for_connection()
        self.establish_connection("!B", [0xff])
        self.begin_transfer("!B", [0xff])

        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x01 )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated READ_STATUS monitor point" )
        resp = struct.unpack( "!4B", r )
        self.failUnless((resp[0] & 0x80) == 0, "Clear the FIFO Full flag before running this test.")

        for x in range(2):
            if (x == 1):
                try:
                    ( r, t ) = self.manager.monitor( self.channel, self.node, 0x01 )
                except ControlExceptions.CAMBErrorEx:
                    self.fail( "Exception generated READ_STATUS monitor point" )
                resp = struct.unpack( "!4B", r )
                if (resp[0] & 0x80) == 0:
                    self.failed("Fifo Depths are not full")
            try:
                ( r, t ) = self.manager.monitor( self.channel, self.node, 0x05 )
            except ControlExceptions.CAMBErrorEx:
                self.fail( "Exception generated monitoring point 0x05" )
            resp = struct.unpack( "!2H", r )
            self.log("FPGA FIFO Depth (max of 2047): %d" % resp[0])
            self.log("External FIFO Depth (max of 43689): %d" %resp[1])
            if (x == 0):
                self.log("Disconnect the tcp connection without commanding STOP_DATA to verify")
                self.log("that the FIFO's fill up, and that the  maximum FIFO depths are correct.")
            rv = raw_input("Press <enter> to continue")
            
        self.end_transfer("!B", [0xff])

        self.endTestLog() 


    def test20(self):
        '''
        This test tests the functioning of the READ_TEMPS monitor points
        '''
        self.startTestLog("READ_TEMPS Test")
        self.log("Verify that the temperatures returned by the READ_TEMPS monitor points are plausible.")

        self.log("READ_TEMPS_1 test")
        try:
            ( r, t ) = self.manager.monitorTE( self.channel, self.node, 0x104 )
            self.failUnless( len( r )==8, "Length of response incorrect for RCA: 0x104" )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated READ_TEMPS_1 monitor point" )
        resp = struct.unpack( "!4H", r )
        rv = raw_input("Press <enter> to record and verify that the following temperatures are plausible.")
        for x in range (0, 4):
            self.log("Temp %s = %f degrees Celsius (0x%x)" % (self.tempsList1[x], ((resp[x]*0.4)-50), resp[x]))

        self.log("READ_TEMPS_2 test")
        try:
            ( r, t ) = self.manager.monitorTE( self.channel, self.node, 0x105 )
            self.failUnless( len( r )==8, "Length of response incorrect for RCA: 0x105" )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated READ_TEMPS_2 monitor point" )
        resp = struct.unpack( "!4H", r )
        rv = raw_input("Press <enter> to record and verify that the following temperatures are plausible.")
        for x in range (0, 4):
            self.log("Temp %s = %f degrees Celsius (0x%x)" % (self.tempsList2[x], ((resp[x]*0.4)-50), resp[x]))             
        
        self.endTestLog()       


    def test21(self):
        '''
        This test tests the functionality of the INIT_TCP_CONN control point 
        '''
        self.startTestLog("INIT_TCP_CONN Test")
        self.log("Verify that a TCP connection is established when the correct INIT_TCP_CONN bit is set, and that it is not established when the correct bit is not set.")        

        #verify that the connection is terminated
        self.check_for_connection()

        format = "!B" 
        dataSeq = [0x00, 0x01, 0xfe, 0xff]
        for x in range(0, 4):
            curSeq = [dataSeq[x]]
            self.log("INIT_TCP_CONN will be commanded with data = 0x%x" % curSeq[0])
            resp = self.establish_connection(format, curSeq)

            if ((resp & 0x04) == 0x04):
                self.log("A TCP connection was made")
            elif ((resp & 0x04) == 0x00):
                self.log("A TCP connection was not made")
            else:
                self.fail("STATUS bits were not masked correctly: 0x%x"%resp)
                
            resp = self.end_transfer("!B", [0xff])
            self.failUnless((resp & 0x04) == 0x00, "TCP connection was not closed by STOP_DATA")
            self.failUnless((resp & 0x10) == 0x00, "Data is being transfered after STOP_DATA was commanded")

        self.endTestLog()
        

    def test22(self):
        '''
        This test tests the functionality of the START_DATA control point
        '''
        self.startTestLog("START_DATA Test")
        self.log("Verify that data transfer begins when the correct START_DATA bit is set, and that it doesn't begin when the correct bit is not set.")

                 
        #verify that the connection is terminated
        self.check_for_connection()
        
        format = "!B" 
        dataSeq = [0x00, 0x01, 0xfe, 0xff]
        for x in range(0, 4):

            resp = self.establish_connection("!B", [0xff])
            self.failUnless((resp & 0x04) == 0x04, "TCP connection was not made")
            self.failUnless((resp & 0x10) == 0x00, "Data is being transfered before START_DATA was commanded")
        
            curSeq = [dataSeq[x]]
            self.log("START_DATA will be commanded with data = 0x%x" % curSeq[0])

            resp = self.begin_transfer(format, curSeq)
            if ((resp & 0x10) == 0x10):
                self.log("Data transfer is active")
            elif ((resp & 0x10) == 0x00):
                self.log("Data transfer is not active")
            else:
                self.fail("STATUS bits were not masked correctly: 0x%x"%resp)
                
            resp = self.end_transfer("!B", [0xff])
            self.failUnless((resp & 0x04) == 0x00, "TCP connection was not closed by STOP_DATA")
            self.failUnless((resp & 0x10) == 0x00, "Data is being transfered after STOP_DATA was commanded")
            
        self.endTestLog()
        

    def test23(self):
        '''
        This test tests the functionality of the STOP_DATA control point 
        '''
        self.startTestLog("STOP_DATA Test")
        self.log("Verify that data transfer ends when the correct STOP_DATA bit is set, and that it doesn't end when the correct bit is not set.")

        #verify that the connection is terminated
        self.check_for_connection()

        format = "!B" 
        dataSeq = [0x00, 0x01, 0xfe, 0xff]
        for x in range(0, 4):

            resp = self.establish_connection("!B", [0xff])
            self.failUnless((resp & 0x04) == 0x04, "TCP connection was not made")
            self.failUnless((resp & 0x10) == 0x00, "Data is being transfered before START_DATA was commanded")
            
            curSeq = [dataSeq[x]]
            self.log("STOP_DATA was commanded with data = 0x%x" % curSeq[0])            
            resp = self.end_transfer(format, curSeq)
            if (((resp & 0x04) == 0x00) and ((resp & 0x10) == 0x00)):
                self.log("TCP connection has been closed")
            elif ((resp & 0x04) == 0x04):
                self.log("TCP connection was not closed")
            else:
                self.fail("STATUS bits were not masked correctly, or there is a TCP connection error: 0x%x"%resp)

            rv = raw_input("Press <enter> to continue")
            self.end_transfer("!B", [0xff])            
            
        for x in range(0, 4):

            resp = self.establish_connection("!B", [0xff])
            self.failUnless((resp & 0x04) == 0x04, "TCP connection was not made")
            self.failUnless((resp & 0x10) == 0x00, "Data is being transfered before START_DATA was commanded")
        
            resp = self.begin_transfer("!B", [0xff])
            self.failUnless((resp & 0x10) == 0x10, "Data did not begin to transfer.")
            
            curSeq = [dataSeq[x]]
            self.log("STOP_DATA was commanded with data = 0x%x" % curSeq[0])            
            resp = self.end_transfer(format, curSeq)
            if (((resp & 0x10) == 0x10) and ((resp & 0x04) == 0x04)):
                self.log("Connection is not closed, and data transfer is still active")
            elif (((resp & 0x04) == 0x00) and ((resp & 0x10) == 0x00)):
                self.log("Data transfer has been stopped and TCP connection has been closed")
            else:
                self.fail("STATUS bits were not masked correctly, or there is a TCP connection error: 0x%x"%resp)

            rv = raw_input("Press <enter> to continue")
            self.end_transfer("!B", [0xff])            
            
        self.endTestLog()
        

    def test24(self):
        '''
        This test tests the functionality of the SET_MODULE_IP_ADDR control point 
        '''
        self.startTestLog("SET_MODULE_IP_ADDR Test")
        self.log("Verify that the TCP connection is established when the MODULE_IP_ADDR is valid, and doesn't establish a connection when the MODULE_IP_ADDR is 0.0.0.0")

        format = "!4B"
        dataSeq = self.destIP             
        try:
            t = self.manager.command(self.channel, self.node, 0x84, struct.pack(format, *dataSeq))
        except:
            self.failed("Error writing to SET_DEST_IP_ADDR control point")
        format = "!H"        
        try:
            t = self.manager.command(self.channel, self.node, 0x85, struct.pack(format, self.PORT))
        except:
            self.failed("Error writing to SET_TCP_PORT control point")             

        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x01 )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated READ_STATUS monitor point" )
        resp = struct.unpack( "!4B", r )
        self.failUnless((resp[0] & 0x04) == 0x00, "Terminate TCP connection before running this test")

        format = "!4B" 
        dataSeq = [0x00, 0x00, 0x00, 0x00]             
        try:
            t = self.manager.command(self.channel, self.node, 0x83, struct.pack(format, *dataSeq))
        except:
            self.failed("Error writing to SET_MODULE_IP_ADDR control point")
        self.log("SET_MODULE_IP_ADDR set to 0.0.0.0")
        resp = self.establish_connection("!B", [0xff])
        self.failUnless((resp & 0x04) == 0x00, "Invalid MODULE_IP_ADDR caused a connection to be created")
        
        format = "!4B"
        for dataSeq in self.validIP: 
            try:
                t = self.manager.command(self.channel, self.node, 0x83, struct.pack(format, *dataSeq))
            except:
                self.failed("Error writing to SET_MODULE_IP_ADDR control point")
            self.log("SET_MODULE_IP_ADDR set to %d.%d.%d.%d"%(dataSeq[0], dataSeq[1], dataSeq[2], dataSeq[3]))

            resp = self.establish_connection("!B", [0xff])
            self.failUnless((resp & 0x04) == 0x04, "Valid MODULE_IP_ADDR did not cause a connection to be created")          

            resp = self.end_transfer("!B", [0xff])
            self.failUnless((resp & 0x04) == 0x00, "TCP connection was not closed by STOP_DATA")            
            self.failUnless((resp & 0x10) == 0x00, "Data is being transfered after STOP_DATA was commanded")

        format = "!4B" 
        resp = self.establish_connection("!B", [0xff])
        self.failUnless((resp & 0x04) == 0x04, "Connection failed to be established")
        for dataSeq in self.totalIP:
            try:
                t = self.manager.command(self.channel, self.node, 0x83, struct.pack(format, *dataSeq))
            except:
                self.failed("Error writing to SET_MODULE_IP_ADDR control point")
            self.log("SET_MODULE_IP_ADDR set back to %d, %d, %d, %d"%(dataSeq[0], dataSeq[1], dataSeq[2], dataSeq[3]))
            try:
                (r, t) = self.manager.monitor(self.channel, self.node, 0x07)
            except:
                self.failed("Error reading from READ_MODULE_IP_ADDR monitor point")
            resp = struct.unpack(format, r)
            self.log("READ_MODULE_IP_ADDR read at %d, %d, %d, %d"%(resp[0], resp[1], resp[2], resp[3]))
            rv = raw_input("Press <enter> to verify that the connection has not closed")
            try:
                ( r, t ) = self.manager.monitor( self.channel, self.node, 0x01 )
            except ControlExceptions.CAMBErrorEx:
                self.fail( "Exception generated READ_STATUS monitor point" )
            resp = struct.unpack( "!4B", r )
            self.failUnless((resp[0] & 0x04) == 0x04, "Changing MODULE_IP_ADDR during mid transfer closed connection")
            self.failUnless((resp[0] & 0x10) == 0x00, "Changing MODULE_IP_ADDR during mid transfer began data transfer")
        
        resp = self.begin_transfer("!B", [0xff])
        self.failUnless((resp & 0x10) == 0x10, "START_DATA did not begin data transfer")        
        for dataSeq in self.totalIP:
            try:
                t = self.manager.command(self.channel, self.node, 0x83, struct.pack(format, *dataSeq))
            except:
                self.failed("Error writing to SET_MODULE_IP_ADDR control point")
            self.log("SET_MODULE_IP_ADDR set back to %d, %d, %d, %d"%(dataSeq[0], dataSeq[1], dataSeq[2], dataSeq[3]))
            try:
                (r, t) = self.manager.monitor(self.channel, self.node, 0x07)
            except:
                self.failed("Error reading from READ_MODULE_IP_ADDR monitor point")
            resp = struct.unpack(format, r)
            self.log("READ_MODULE_IP_ADDR read at %d, %d, %d, %d"%(resp[0], resp[1], resp[2], resp[3]))            
            rv = raw_input("Press <enter> to verify that the data transfer has not stopped")
            try:
                ( r, t ) = self.manager.monitor( self.channel, self.node, 0x01 )
            except ControlExceptions.CAMBErrorEx:
                self.fail( "Exception generated READ_STATUS monitor point" )
            resp = struct.unpack( "!4B", r )
            self.failUnless((resp[0] & 0x14) == 0x14, "Changing MODULE_IP_ADDR during mid transfer stopped transfer")        

        resp = self.end_transfer("!B", [0xff])
        self.failUnless((resp & 0x04) == 0x00, "TCP connection was not closed by STOP_DATA")
        self.failUnless((resp & 0x10) == 0x00, "Data is being transfered after STOP_DATA was commanded")
            
        self.endTestLog()


    def test25(self):
        '''
        This test tests the functionality of the SET_DEST_IP_ADDR control point 
        '''
        self.startTestLog("SET_DEST_IP_ADDR Test")
        self.log("Verify that the TCP connection is established when the DEST_IP_ADDR is valid, and doesn't establish a connection when the DEST_IP_ADDR is 0.0.0.0")

        format = "!4B" 
        dataSeq = self.validIP[0]             
        try:
            t = self.manager.command(self.channel, self.node, 0x83, struct.pack(format, *dataSeq))
        except:
            self.failed("Error writing to SET_MODULE_IP_ADDR control point")
        format = "!H"         
        try:
            t = self.manager.command(self.channel, self.node, 0x85, struct.pack(format, self.PORT))
        except:
            self.failed("Error writing to SET_TCP_PORT control point")             

        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x01 )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated READ_STATUS monitor point" )
        resp = struct.unpack( "!4B", r )
        self.failUnless((resp[0] & 0x04) == 0x00, "Terminate TCP connection before running this test")

        format = "!4B" 
        dataSeq = [0x00, 0x00, 0x00, 0x00]             
        try:
            t = self.manager.command(self.channel, self.node, 0x84, struct.pack(format, *dataSeq))
        except:
            self.failed("Error writing to SET_DEST_IP_ADDR control point")
        self.log("SET_DEST_IP_ADDR set to 0.0.0.0")
        resp = self.establish_connection("!B", [0xff])
        self.failUnless((resp & 0x04) == 0x00, "Invalid DEST_IP_ADDR caused a connection to be created")

        format = "!4B" 
        dataSeq = self.destIP             
        try:
            t = self.manager.command(self.channel, self.node, 0x84, struct.pack(format, *dataSeq))
        except:
            self.failed("Error writing to SET_DEST_IP_ADDR control point")
        self.log("SET_DEST_IP_ADDR set to 146.88.7.35")
        resp = self.establish_connection("!B", [0xff])
        self.failUnless((resp & 0x04) == 0x04, "Valid DEST_IP_ADDR did not cause a connection to be created")

        format = "!4B"
        iplist = []
        iplist.append([0, 0, 0, 0])
        iplist.append([1, 1, 1, 1]) #random invalid IP address
        iplist.append(self.destIP)
        for x in range (3):
            dataSeq = iplist[x]
            try:
                t = self.manager.command(self.channel, self.node, 0x84, struct.pack(format, *dataSeq))
            except:
                self.failed("Error writing to SET_DEST_IP_ADDR control point")
            self.log("SET_DEST_IP_ADDR set back to %d.%d.%d.%d"%(dataSeq[0], dataSeq[1], dataSeq[2], dataSeq[3]))
            try:
                (r, t) = self.manager.monitor(self.channel, self.node, 0x08)
            except:
                self.failed("Error reading from READ_DEST_IP_ADDR monitor point")
            resp = struct.unpack(format, r)
            self.log("READ_DEST_IP_ADDR read at %d, %d, %d, %d"%(resp[0], resp[1], resp[2], resp[3]))            
            rv = raw_input("Press <enter> to verify that the connection has not closed")        
            try:
                ( r, t ) = self.manager.monitor( self.channel, self.node, 0x01 )
            except ControlExceptions.CAMBErrorEx:
                self.fail( "Exception generated READ_STATUS monitor point" )
            resp = struct.unpack( "!4B", r )
            self.failUnless((resp[0] & 0x04) == 0x04, "Changing DEST_IP_ADDR during mid transfer closed connection")
            self.failUnless((resp[0] & 0x10) == 0x00, "Changing DEST_IP_ADDR during mid transfer began data transfer")
            
        resp = self.begin_transfer("!B", [0xff])
        self.failUnless((resp & 0x10) == 0x10, "START_DATA did not begin data transfer")
        for x in range (3):
            dataSeq = iplist[x]
            try:
                t = self.manager.command(self.channel, self.node, 0x84, struct.pack(format, *dataSeq))
            except:
                self.failed("Error writing to SET_DEST_IP_ADDR control point")
            self.log("SET_DEST_IP_ADDR set back to %d.%d.%d.%d"%(dataSeq[0], dataSeq[1], dataSeq[2], dataSeq[3]))
            try:
                (r, t) = self.manager.monitor(self.channel, self.node, 0x08)
            except:
                self.failed("Error reading from READ_DEST_IP_ADDR monitor point")
            resp = struct.unpack(format, r)
            self.log("READ_DEST_IP_ADDR read at %d, %d, %d, %d"%(resp[0], resp[1], resp[2], resp[3]))            
            rv = raw_input("Press <enter> to verify that the data transfer has not stopped")        
            try:
                ( r, t ) = self.manager.monitor( self.channel, self.node, 0x01 )
            except ControlExceptions.CAMBErrorEx:
                self.fail( "Exception generated READ_STATUS monitor point" )
            resp = struct.unpack( "!4B", r )
            self.failUnless((resp[0] & 0x14) == 0x14, "Changing DEST_IP_ADDR during mid transfer stopped transfer")
            
        resp = self.end_transfer("!B", [0xff])
        self.failUnless((resp & 0x04) == 0x00, "TCP connection was not closed by STOP_DATA")            
        self.failUnless((resp & 0x10) == 0x00, "Data is being transfered after STOP_DATA was commanded")
        
        self.endTestLog()


    def test26(self):
        '''
        This test tests the functionality of the SET_TCP_PORT control point 
        '''
        self.startTestLog("SET_TCP_PORT Test")

        self.log("Verify that the TCP connection is established when the TCP_PORT is valid, and doesn't establish a connection when the TCP_PORT is 0")

        format = "!4B" 
        dataSeq = self.destIP             
        try:
            t = self.manager.command(self.channel, self.node, 0x84, struct.pack(format, *dataSeq))
        except:
            self.failed("Error writing to SET_DEST_IP_ADDR control point")
        format = "!4B" 
        dataSeq = self.validIP[0]             
        try:
            t = self.manager.command(self.channel, self.node, 0x83, struct.pack(format, *dataSeq))
        except:
            self.failed("Error writing to SET_MODULE_IP_ADDR control point")            
          
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x01 )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated READ_STATUS monitor point" )
        resp = struct.unpack( "!4B", r )
        self.failUnless((resp[0] & 0x04) == 0x00, "Terminate TCP connection before running this test")

        format = "!H" 
        self.PORT = 0x0000        
        try:
            t = self.manager.command(self.channel, self.node, 0x85, struct.pack(format, self.PORT))
        except:
            self.failed("Error writing to SET_TCP_PORT control point") 
        self.log("SET_TCP_PORT set to 0")
        resp = self.establish_connection("!B", [0xff])
        self.failUnless((resp & 0x04) == 0x00, "Invalid TCP_PORT caused a connection to be created")

        format = "!H" 
        self.PORT = 50007        
        try:
            t = self.manager.command(self.channel, self.node, 0x85, struct.pack(format, self.PORT))
        except:
            self.failed("Error writing to SET_TCP_PORT control point") 
        self.log("SET_TCP_PORT set to %d"%self.PORT)
        resp = self.establish_connection("!B", [0xff])
        self.failUnless((resp & 0x04) == 0x04, "Valid TCP_PORT did not cause a connection to be created")
       
        format = "!H"
        for x in range (2):
            try:
                t = self.manager.command(self.channel, self.node, 0x85, struct.pack(format, self.portlist[x]))
            except:
                self.failed("Error writing to SET_TCP_PORT control point")
            self.log("SET_TCP_PORT set back to %d"%self.portlist[x])
            try:
                (r, t) = self.manager.monitor(self.channel, self.node, 0x0a)
            except:
                self.failed("Error reading from READ_TCP_PORT monitor point")
            resp = struct.unpack(format, r)
            self.log("READ_TCP_PORT read at %d"%resp[0])            
            rv = raw_input("Press <enter> to verify that the connection has not closed")        
            try:
                ( r, t ) = self.manager.monitor( self.channel, self.node, 0x01 )
            except ControlExceptions.CAMBErrorEx:
                self.fail( "Exception generated READ_STATUS monitor point" )
            resp = struct.unpack( "!4B", r )
            self.failUnless((resp[0] & 0x04) == 0x04, "Changing TCP_PORT during mid transfer closed connection")        
            self.failUnless((resp[0] & 0x10) == 0x00, "Changing TCP_PORT during mid transfer began data transfer")
            
        resp = self.begin_transfer("!B", [0xff])
        self.failUnless((resp & 0x10) == 0x10, "START_DATA did not begin data transfer")
        for x in range (2):
            try:
                t = self.manager.command(self.channel, self.node, 0x85, struct.pack(format, self.portlist[x]))
            except:
                self.failed("Error writing to SET_TCP_PORT control point")
            self.log("SET_TCP_PORT set back to %d"%self.portlist[x])
            try:
                (r, t) = self.manager.monitor(self.channel, self.node, 0x0a)
            except:
                self.failed("Error reading from READ_TCP_PORT monitor point")
            resp = struct.unpack(format, r)
            self.log("READ_TCP_PORT read at %d"%resp[0])            
            rv = raw_input("Press <enter> to verify that the data transfer has not stopped")        
            try:
                ( r, t ) = self.manager.monitor( self.channel, self.node, 0x01 )
            except ControlExceptions.CAMBErrorEx:
                self.fail( "Exception generated READ_STATUS monitor point" )
            resp = struct.unpack( "!4B", r )
            self.failUnless((resp[0] & 0x14) == 0x14, "Changing TCP_PORT during mid transfer stopped transfer")

        resp = self.end_transfer("!B", [0xff])
        self.failUnless((resp & 0x04) == 0x00, "TCP connection was not closed by STOP_DATA")        
        self.failUnless((resp & 0x10) == 0x00, "Data is being transfered after STOP_DATA was commanded")

        self.endTestLog()       


    def test27(self):
        '''
        This test tests the functionality of the START_COMM_TEST control point 
        '''
        self.startTestLog("START_COMM_TEST Test")

        self.log('''Verify the correctness of the START_COMM_TEST control point.
                    The data received over the ethernet connection will need to
                    be used in order to verify this test.
                 ''')

        #verify that the connection is terminated
        self.check_for_connection()        

        resp = self.establish_connection("!B", [0xff])
        self.failUnless((resp & 0x04) == 0x04, "TCP connection was not made")
        self.failUnless((resp & 0x10) == 0x00, "Data is being transfered before START_DATA was commanded")

        resp = self.begin_transfer("!B", [0xff])
        self.failUnless((resp & 0x10) == 0x10, "Data did not begin to transfer.")            

        format = "!B"
        dataSeq = [0x01, 0x03, 0x07, 0x02, 0x06, 0x04]
        endSeq = [0x00]
        for x in range(0, 6):
            curSeq = dataSeq[x]
            try:
                t = self.manager.command(self.channel, self.node, 0x8b, struct.pack(format, curSeq))
            except:
                self.failed("Error writing to START_COMM_TEST control point")
            try:
                ( r, t ) = self.manager.monitor( self.channel, self.node, 0x01 )
            except ControlExceptions.CAMBErrorEx:
                self.fail( "Exception generated READ_STATUS monitor point" )
            resp = struct.unpack( "!4B", r )
            self.failUnless(resp[3]  == curSeq, "Control data was not read back correctly.")

            self.log("The value 0x%x was sent to the START_COMM_TEST control point" % curSeq)
            self.log("Verify that the correct COMM TEST is being performed correctly")
            rv = raw_input("Press <enter> to continue")
            
        try:
            t = self.manager.command(self.channel, self.node, 0x8b, struct.pack(format, *endSeq))
        except:
            self.failed("Error writing to START_COMM_TEST control point")
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x01 )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated READ_STATUS monitor point" )
        resp = struct.unpack( "!4B", r )
        self.failUnless(resp[3]  == endSeq, "Control data was not read back correctly.")

        rv = raw_input("Verify that the COMM TEST has been disabled correctly")

        resp = self.end_transfer("!B", [0xff])    
        self.failUnless((resp & 0x04) == 0x00, "TCP connection was not closed by STOP_DATA")            
        self.failUnless((resp & 0x10) == 0x00, "Data is being transfered after STOP_DATA was commanded")

        self.endTestLog()                  


    def test28(self):
        '''
        This test tests the SET_DATA_REMAP and READ_DATA_REMAP control and monitor points
        '''
        self.startTestLog("READ_DATA_REMAP Test")

        self.log("Test the READ_DATA_REMAP monitor point at startup to verify that each signal defaults to the correct digitizer.")

        rv = raw_input("Press <enter> to restart the IFDC")
        try:
            t = self.manager.command(self.channel, self.node, 0x81, struct.pack("!B", 0xff))
        except:
            self.failed("Error writing to RESET_TPD_BOARD control point")

        signal = ["S0", "S1", "A", "B", "C", "D"]
        rv = raw_input("Press <enter> to verify that the IFDC restarted")
        resp = self.test_for_reset(1)
        self.log("Number of transactions since power-up: %d" % resp)

        rv = raw_input("Press <enter> to verify that each signal is set to its default digitizer")
        try:
            ( r, t ) = self.manager.monitorTE( self.channel, self.node, 0x06 )
            self.failUnless( len( r ) == 6, "Length of response incorrect for RCA:0x06" )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception monitoring READ_DATA_REMAP point" )	
        resp = struct.unpack( "!6B", r )
        for x in range(0,6):
            self.log("%s set to %d"%(signal[x],resp[x]))
            if (resp[x] != x):
                self.failed("%s (byte %d) set to wrong digitizer %d"%(signal[x],x,resp[x]))

        self.log("Verify that if SET_DATA_REMAP is set to 0xF, or an incorrect value, that each signal defaults to the correct digitizer.")
        rv = raw_input("Press <enter> to begin")

        self.log("All signals will now be set to digitizer 6")
        rv = raw_input("Press <enter> to continue")
        dataSeq = [6, 6, 6, 6, 6, 6]
        try:
            t = self.manager.commandTE(self.channel, self.node, 0x82, struct.pack("!6B", *dataSeq))
        except:
            self.failed("Error writing to SET_DATA_REMAP control point")
        try:
            ( r, t ) = self.manager.monitorTE( self.channel, self.node, 0x06 )
            self.failUnless( len( r ) == 6, "Length of response incorrect for RCA:0x06" )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception monitoring READ_DATA_REMAP point" )	
        resp = struct.unpack( "!6B", r )
        for x in range(0,6):
            self.log("%s set to %d"%(signal[x],resp[x]))
            if (resp[x] != 6):
                self.failed("%s (byte %d) set to wrong digitizer %d at data value 0x%x"%(signal[x],x,resp[x],data))

        self.log("All Singals will now be set to their default digitizers.")
        rv = raw_input("Press <enter> to continue")
        dataSeq = [0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f]
        try:
            t = self.manager.commandTE(self.channel, self.node, 0x82, struct.pack("!6B", *dataSeq))
        except:
            self.failed("Error writing to SET_DATA_REMAP control point")
        try:
            ( r, t ) = self.manager.monitorTE( self.channel, self.node, 0x06 )
            self.failUnless( len( r ) == 6, "Length of response incorrect for RCA:0x06" )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception monitoring READ_DATA_REMAP point" )	
        resp = struct.unpack( "!6B", r )
        for x in range(0,6):
            self.log("%s set to %d"%(signal[x],resp[x]))
            if (resp[x] != x):
                self.failed("%s (byte %d) set to wrong digitizer %d at data value 0x%x"%(signal[x],x,resp[x],data))

        self.log("All Signals will now be set to invalid values.")
        rv = raw_input("Press <enter> to continue")        
        for data in range (0x07, 0x100):
            dataSeq = [data, data, data, data, data, data]
            try:
                t = self.manager.commandTE(self.channel, self.node, 0x82, struct.pack("!6B", *dataSeq))
            except:
                self.failed("Error writing to SET_DATA_REMAP control point")
            try:
                ( r, t ) = self.manager.monitorTE( self.channel, self.node, 0x06 )
                self.failUnless( len( r ) == 6, "Length of response incorrect for RCA:0x06" )
            except ControlExceptions.CAMBErrorEx:
                self.fail( "Exception monitoring READ_DATA_REMAP point" )	
            resp = struct.unpack( "!6B", r )

            for x in range(0,6):
                self.log("%s set to %d"%(signal[x],resp[x]))
                if (resp[x] != x):
                    self.failed("%s (byte %d) set to wrong digitizer %d at data value 0x%x"%(signal[x],x,resp[x],data))
       
        self.endTestLog()
        

    def test29(self):
        '''
        This test tests the READ_ETHERNET_MAC monitor point
        '''
        self.startTestLog("READ_ETHERNET_MAC Test")
        self.log("Verify that the MAC address of the Ethernet controller is correct")
        
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x09 )
            self.failUnless( len( r ) == 6, "Length of response incorrect for RCA:0x09" )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated monitoring point 0x09" )	
        resp = struct.unpack( "!6B", r )
        self.log("Ethernet MAC address (hex) = %x %x %x %x %x %x" % (resp[0], resp[1], resp[2], resp[3], resp[4], resp[5]))

        self.endTestLog()

        
    def test30(self):
        '''
        This test tests the functionality of the SET_BDB_PER_PACKET control point 
        '''
        self.startTestLog("SET_BDB_PER_PACKET Test")

        self.log('''Verify the correctness of the SET_BDB_PER_PACKET control point.
                    Verify that SET_BDB_PER_PACKET can be done during data transfer.
                    Verify that SET_BDB_PER_PACKET has a minimum value of 32.
                 ''')

        #verify that the connection is terminated
        self.check_for_connection()
        
        resp = self.establish_connection("!B", [0xff])
        self.failUnless((resp & 0x04) == 0x04, "TCP connection was not made")
        self.failUnless((resp & 0x10) == 0x00, "Data is being transfered before START_DATA was commanded")

        resp = self.begin_transfer("!B", [0xff])
        self.failUnless((resp & 0x10) == 0x10, "Data did not begin to transfer.")

        try:
            t = self.manager.command(self.channel, self.node, 0x86, struct.pack("!B", 32))
        except:
            self.failed("Error writing to SET_BDB_PER_PACKET control point")
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x0b )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated READ_BDB_PER_PACKET monitor point" )
        resp = struct.unpack( "!B", r )
        self.failUnless(resp[0] == 32, "BDB per packet was not correctly set to 32 at the beginning of this test.")       

        dataSeq = []
        correctData = []
        for x in range (32):
            dataSeq.append(x)
            correctData.append(32)
        for x in range (32, 256):
            dataSeq.append(x)
            correctData.append(x)
        for x in range(256):
            currSeq = dataSeq[x]
            try:
                t = self.manager.command(self.channel, self.node, 0x86, struct.pack("!B", currSeq))
                self.log("%d BDB_PER_PACKET written, should be set to %d" % (currSeq, correctData[x]))
                self.log("Verify this on the data received over the ethernet connection.")
                rv = raw_input("Press <enter> to continue.")
            except:
                self.failed("Error writing to SET_BDB_PER_PACKET control point")
            try:
                ( r, t ) = self.manager.monitor( self.channel, self.node, 0x0b )
            except ControlExceptions.CAMBErrorEx:
                self.fail( "Exception generated READ_BDB_PER_PACKET monitor point" )
            resp = struct.unpack( "!B", r )
            self.log("BDB_PER_PACKET read at %d" % resp[0])
            if (resp[0] != correctData[x]):
                self.failed("Incorrect value: BDB per packet should be %d"%correctData[x])                
            
        resp = self.end_transfer("!B", [0xff])                
        self.failUnless((resp & 0x04) == 0x00, "TCP connection was not closed by STOP_DATA")
        self.failUnless((resp & 0x10) == 0x00, "Data is being transfered after STOP_DATA was commanded")
            
        self.endTestLog()


    def test31(self):
        '''
        This test tests the functionality of the READ_TIMING_ERROR_FLAG monitor point 
        '''
        self.startTestLog("READ_TIMING_ERROR_FLAG Test")

        self.log('''Verify the correctness of the READ_TIMING_ERROR_FLAG monitor point.
                    Cause an error between the 125MHz clock and the 48ms timing event.
                    Verify that the READ_TIMING_ERROR_FLAG is set.
                 ''')
        
        clearData = 0x01
        try:
            t = self.manager.command(self.channel, self.node, 0x8c, struct.pack("!B", clearData))
        except:
            self.failed("Error writing to SET_MODULE_IP_ADDR control point")
        try:
            ( r, t ) = self.manager.monitorTE( self.channel, self.node, 0x02 )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated READ_TIMING_ERROR_FLAG monitor point" )
        resp = struct.unpack( "!B", r )
        self.failUnless(resp == 0x00, "TIMING_ERROR_FLAG was not cleared.")              

        rv = raw_input("Generate timing error.  Then press <enter>.")
        try:
            ( r, t ) = self.manager.monitorTE( self.channel, self.node, 0x02 )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated READ_TIMING_ERROR_FLAG monitor point" )
        resp = struct.unpack( "!B", r )
        self.failUnless(resp == 0x01, "TIMING_ERROR_FLAG was not set.")        

        self.endTestLog()
        

    def test32(self):
        '''
        This test tests the functionality of the READ_MAX_ETHERNET_TIMES monitor point 
        '''
        self.startTestLog("READ_MAX_ETHERNET_TIMES Test")

        self.log('''Verify that the READ_MAX_ETHERNET_TIMES monitor point returns
                    acceptable values.                    
                 ''')
        
        self.check_for_connection()
        self.establish_connection("!B", [0xff])
        self.begin_transfer("!B", [0xff])

        bdb = [16, 255]
        for x in range(2):
            try:
                t = self.manager.command(self.channel, self.node, 0x86, struct.pack("!B", bdb[x]))
                self.log("%d BDB_PER_PACKET written"%(bdb[x]))
            except:
                self.failed("Error writing to SET_BDB_PER_PACKET control point")
            try:
                ( r, t ) = self.manager.monitor( self.channel, self.node, 0x0b )
            except ControlExceptions.CAMBErrorEx:
                self.fail( "Exception generated READ_BDB_PER_PACKET monitor point" )
            resp = struct.unpack( "!B", r )
            self.failUnless(resp[0] == bdb[x], "BDB not correctly set")

            for y in range(5):
                try:
                    ( r, t ) = self.manager.monitor( self.channel, self.node, 0x0e )
                except ControlExceptions.CAMBErrorEx:
                    self.fail( "Exception generated READ_MAX_ETHERNET_TIMES monitor point" )
                resp = struct.unpack( "!2I", r )        
                self.log("Max time taken to read data for a single packet = %dns"%(resp[0]*8))
                self.log("Max Ethernet Transmit plut Ethernet Handling time for a single packet = %dns"%(resp[1]*8))
                rv = raw_input("Press <enter> to continue")
            
        self.end_transfer("!B", [0xff])
        
        self.endTestLog()


    def test33(self):
        '''
        This test tests the functionality of the READ_IFDC_SPI_STATUS monitor point 
        '''
        self.startTestLog("READ_IFDC_SPI_STATUS Test")

        self.log('''Verify that the READ_IFDC_SPI_STATUS monitor point returns
                    acceptable values.                    
                 ''')
        
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x10 )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated READ_IFDC_SPI_STATUS monitor point" )
        resp = struct.unpack( "!2B", r )        

        self.log("returned value = 0x%x, 0x%x"%(resp[1], resp[0]))
        self.log("IFDC Found = %d"%(resp[0] & 0x01))
        self.log("Variable length reads = %d"%((resp[0] & 0x02)>>1))
        self.log("IFDC ADS = %d"%(resp[1] & 0x03))
        self.log("IFDC MCS = %d"%((resp[1] & 0x1c)>>2))
        self.log("IFDC Length = %d"%((resp[1] & 0xe0)>>5))
        
        self.endTestLog()


    def test34(self):
        '''
        This test tests the functionality of the READ_TPD_MODULE_CODES monitor point 
        '''
        self.startTestLog("READ_TPD_MODULE_CODES Test")

        self.log('''Verify that the READ_TPD_MODULE_CODES monitor point returns
                    acceptable values.                    
                 ''')
        
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x79 )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated READ_TPD_MODULE_CODES monitor point" )
        resp = struct.unpack( "!6BH", r )

        self.failUnless(resp[0] == 0x42, "Back End Code (0x%x) is not 0x42"%resp[0])
        self.failUnless(resp[1] == 0x0a, "IFMC-C Code (0x%x) is not 0x0a"%resp[1])
        self.log("Firmware Revision: %d.%d"%(((resp[2]&0xf0)>>4),(resp[2]&0x0f)))
        self.log("Firmware Date (Month/Day/Year): %d/%d/%d"%(resp[4],resp[3],resp[5]))
        self.log("Module Serial Number = 0x%x"%resp[6])
        
        self.endTestLog()


    def test35(self):
        '''
        This test tests the functionality of the READ_MODULE_CODES monitor point 
        '''
        self.startTestLog("READ_IFDC_MODULE_CODES Test")

        self.log('''Verify that the READ_MODULE_CODES monitor point returns
                    acceptable values.                    
                 ''')
        
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x179 )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated READ_MODULE_CODES monitor point" )
        resp = struct.unpack( "!6BH", r )

        self.failUnless(resp[0] == 0x42, "Back End Code (0x%x) is not 0x42"%resp[0])
        self.failUnless(resp[1] == 0x0b, "IFDC Code (0x%x) is not 0x0b"%resp[1])
        self.log("Firmware Revision: %d.%d"%(((resp[2]&0xf0)>>4),(resp[2]&0x0f)))
        self.log("Firmware Date (Month/Day/Year): %d/%d/%d"%(resp[4],resp[3],resp[5]))
        self.log("Module Serial Number = 0x%x"%resp[6])
        
        self.endTestLog()        


    def test36(self):
        '''
        This test is used to verify that the signal paths and gains are correctly set by the SET_SIGNAL_PATHS and
        SET_GAINS commands, for the various output signals (A, B, C, D) and input signals (S0, S1).  This is a long
        test, so it has been divided into three tests.  This is Part 1 of 3.
        It tests bits 4-7 of SIGNAL_PATHS, as well as the individual bytes of the GAINS.
        '''
        self.startTestLog("SET_SIGNAL_PATHS and SET_GAINS Validity Tests Part 1")
        self.log("This test is used to verify that the signal paths and gains are correctly set by the SET_SIGNAL_PATHS and")
        self.log("SET_GAINS commands, for the various output signals (A, B, C, D) and input signals (S0, S1)  This is a long")
        self.log("test, so it has been divided into three tests.  This is part 1 of 3.")
        self.log("It tests bits 4-7 of SIGNAL_PATHS, as well as the individual bytes of the GAINS.")
        self.log("")
        self.log("Set the frequencies of all four LO2's to 8GHz.")

        #Just in case the IFDC has just been booted up, the switches are excercised
        self.initialize_switches()

        signal = ["A", "B", "C", "D"]

        self.log("Connect a signal generator to input signal S0, and set it to about 5GHz at -20dBm.")
        self.log("Make sure that input signal S1 has no input.")
        for x in range(2):
            self.log("Connect a spectrum analyzer to signal " + signal[x]  + ", and set its output range to about 2GHz - 4GHz.")
            rv = raw_input("Press <enter> when ready")
            path = []
            path.append(("Channel " + signal[x] + " disconnected from S0", "no signal", 0xef))
            path.append(("S0 disconnected from downconverter", "no signal", 0xbf))
            path.append(("Channel " + signal[x] + " connected to S0, and S0 connected to downconverter", "a valid signal", 0xff))
            for point in path:
                self.log(point[0])
                try:
                    t = self.manager.command(self.channel, self.node, 0x182, struct.pack("!B", point[2]))
                except:
                    self.fail("Exception generating SET_SIGNAL_PATHS control point")
                self.log("Verify that there is " + point[1] + " on the spectrum analyzer")
                rv = raw_input("Press <enter> to continue")
            self.test_gains()

        for x in range(2, 4):
            self.log("Connect a spectrum analyzer to signal " + signal[x]  + ", and set its output range to about 2GHz - 4GHz.")
            rv = raw_input("Press <enter> when ready")
            path = []
            path.append(("Channel " + signal[x] + " disconnected from S0", "no signal", 0xdf))
            path.append(("S0 disconnected from downconverter", "no signal", 0xbf))
            path.append(("Channel " + signal[x] + " connected to S0, and S0 connected to downconverter", "a valid signal", 0xff))
            for point in path:
                self.log(point[0])
                try:
                    t = self.manager.command(self.channel, self.node, 0x182, struct.pack("!B", point[2]))
                except:
                    self.fail("Exception generating SET_SIGNAL_PATHS control point")
                self.log("Verify that there is " + point[1] + " on the spectrum analyzer")
                rv = raw_input("Press <enter> to continue")
            self.test_gains()

        self.log("Connect a signal generator to input signal S1, and set it to about 5GHz at -20dBm.")
        self.log("Make sure that input signal S0 has no input.")
        for x in range(2):
            self.log("Connect a spectrum analyzer to signal " + signal[x]  + ", and set its output range to about 2GHz - 4GHz.")
            rv = raw_input("Press <enter> when ready")
            path = []
            path.append(("Channel " + signal[x] + " disconnected from S1", "no signal", 0xdf))
            path.append(("S1 disconnected from downconverter", "no signal", 0x4f))
            path.append(("Channel " + signal[x] + " connected to S1, and S1 connected to downconverter", "a valid signal", 0xcf))
            for point in path:
                self.log(point[0])
                try:
                    t = self.manager.command(self.channel, self.node, 0x182, struct.pack("!B", point[2]))
                except:
                    self.fail("Exception generating SET_SIGNAL_PATHS control point")
                self.log("Verify that there is " + point[1] + " on the spectrum analyzer")
                rv = raw_input("Press <enter> to continue")
            self.test_gains()

        for x in range(2, 4):
            self.log("Connect a spectrum analyzer to signal " + signal[x]  + ", and set its output range to about 2GHz - 4GHz.")
            rv = raw_input("Press <enter> when ready")
            path = []
            path.append(("Channel " + signal[x] + " disconnected from S1", "no signal", 0xef))
            path.append(("S1 disconnected from downconverter", "no signal", 0x4f))
            path.append(("Channel " + signal[x] + " connected to S1, and S1 connected to downconverter", "a valid signal", 0xcf))
            for point in path:
                self.log(point[0])
                try:
                    t = self.manager.command(self.channel, self.node, 0x182, struct.pack("!B", point[2]))
                except:
                    self.fail("Exception generating SET_SIGNAL_PATHS control point")
                self.log("Verify that there is " + point[1] + " on the spectrum analyzer")
                rv = raw_input("Press <enter> to continue")
            self.test_gains()        
            
        self.endTestLog()


    def test37(self):
        '''
        This test is used to verify that the signal paths and gains are correctly set by the SET_SIGNAL_PATHS and
        SET_GAINS commands, for the various output signals (A, B, C, D) and input signals (S0, S1).  This is a long
        test, so it has been divided into three tests.  This is Part 2 of 3.
        It tests the individual bits of the GAINS.
        '''
        self.startTestLog("SET_SIGNAL_PATHS and SET_GAINS Validity Tests Part 2")
        self.log("This test is used to verify that the signal paths and gains are correctly set by the SET_SIGNAL_PATHS and")
        self.log("SET_GAINS commands, for the various output signals (A, B, C, D) and input signals (S0, S1)  This is a long")
        self.log("test, so it has been divided into three tests.  This is part 2 of 3.")
        self.log("It tests the individual bits of the GAINS.")
        self.log("")
        self.log("Set the frequencies of all four LO2's to 8GHz.")

        #Just in case the IFDC has just been booted up, the switches are excercised
        self.initialize_switches()
        
        insignal = ["S0", "S1"]
        outsignal = ["A", "B", "C", "D"]

        self.log("Connect a spectrum analyzer to signal A, and set its output range to about 2GHz - 4GHz.")
        path = [0xff, 0xcf]
        #The gains are set to 120 (15dB) and tested from there.  This way, the amplitude won't go below the noise baseline
        #or above the saturation cutoff
        gain = []
        gain.append(([121, 121, 120, 120, 120, 120], "0", "didn't change"))
        gain.append(([122, 122, 120, 120, 120, 120], "1", "didn't change"))
        gain.append(([124, 124, 120, 120, 120, 120], "2", "increased by 0.5dB"))
        gain.append(([112, 112, 120, 120, 120, 120], "3", "decreased by 1dB"))
        gain.append(([104, 104, 120, 120, 120, 120], "4", "decreased by 2dB"))
        gain.append(([88, 88, 120, 120, 120, 120], "5", "decreased by 4dB"))
        gain.append(([56, 56, 120, 120, 120, 120], "6", "decreased by 8dB"))
        gain.append(([248, 248, 120, 120, 120, 120], "7", "increased by 16dB"))        
        for x in range(2):
            try:
                t = self.manager.command(self.channel, self.node, 0x182, struct.pack("!B", path[x]))
            except:
                self.fail("Exception generating SET_SIGNAL_PATHS control point")
            self.log("Connect a signal generator to input signal " + insignal[x]  + ", and set it to about 5GHz at -20dBm.")
            rv = raw_input("Press <enter> when ready.")
            for point in gain:
                rstgain = [120, 120, 120, 120, 120, 120]
                try:
                    t = self.manager.command(self.channel, self.node, 0x181, struct.pack("!6B", *rstgain))
                except:
                    self.fail("Exception generating SET_GAINS control point")
                rv = raw_input("The signal amplitude now at its starting level.  Press <enter> to continue.")
                try:
                    t = self.manager.command(self.channel, self.node, 0x181, struct.pack("!6B", *point[0]))
                except:
                    self.fail("Exception generating SET_GAINS control point")
                self.log("Modified bit " + point[1] + " of SET_GAINS.")
                self.log("Verify that the signal amplitude " + point[2])
                rv = raw_input("Press <enter> to continue.")

        self.log("Connect a signal generator to input signal S0, and set it to about 5GHz at -20dBm.")
        #The gains are set to 120 (15dB) and tested from there.  This way, the amplitude won't go below the noise baseline
        #or above the saturation cutoff
        gain = []
        gain.append(([120, 120, 121, 121, 121, 121], "0", "didn't change"))
        gain.append(([120, 120, 122, 122, 122, 122], "1", "didn't change"))
        gain.append(([120, 120, 124, 124, 124, 124], "2", "decreased by 0.5dB"))
        gain.append(([120, 120, 112, 112, 112, 112], "3", "increased by 1dB"))
        gain.append(([120, 120, 104, 104, 104, 104], "4", "increased by 2dB"))
        gain.append(([120, 120, 88, 88, 88, 88], "5", "increased by 4dB"))
        gain.append(([120, 120, 56, 56, 56, 56], "6", "increased by 8dB"))
        gain.append(([120, 120, 248, 248, 248, 248], "7", "decreased by 16dB"))
        try:
            t = self.manager.command(self.channel, self.node, 0x182, struct.pack("!B", 0xff))
        except:
            self.fail("Exception generating SET_SIGNAL_PATHS control point")
        for x in range(4):
            self.log("Connect a spectrum analyzer to signal " + outsignal[x]  + ", and set its output range to about 2GHz - 4GHz.")
            rv = raw_input("Press <enter> when ready.")
            for point in gain:
                rstgain = [120, 120, 120, 120, 120, 120]
                try:
                    t = self.manager.command(self.channel, self.node, 0x181, struct.pack("!6B", *rstgain))
                except:
                    self.fail("Exception generating SET_GAINS control point")
                rv = raw_input("The signal amplitude now at its starting level.  Press <enter> to continue.")
                try:
                    t = self.manager.command(self.channel, self.node, 0x181, struct.pack("!6B", *point[0]))
                except:
                    self.fail("Exception generating SET_GAINS control point")
                self.log("Modified bit " + point[1] + " of SET_GAINS.")
                self.log("Verify that the signal amplitude " + point[2])
                rv = raw_input("Press <enter> to continue.")

        self.endTestLog()
                

    def test38(self):
        '''
        This test is used to verify that the signal paths and gains are correctly set by the SET_SIGNAL_PATHS and
        SET_GAINS commands, for the various output signals (A, B, C, D) and input signals (S0, S1).  This is a long
        test, so it has been divided into three tests.  This is Part 3 of 3.
        It tests bits 0-3 of SIGNAL_PATHS.
        '''
        self.startTestLog("SET_SIGNAL_PATHS and SET_GAINS Validity Tests Part 3")
        self.log("This test is used to verify that the signal paths and gains are correctly set by the SET_SIGNAL_PATHS and")
        self.log("SET_GAINS commands, for the various output signals (A, B, C, D) and input signals (S0, S1)  This is a long")
        self.log("test, so it has been divided into three tests.  This is part 3 of 3.")
        self.log("It tests bits 0-3 of SIGNAL_PATHS.")
        self.log("")
        self.log("Set the frequencies of all four LO2's to 14GHz.")

        #Just in case the IFDC has just been booted up, the switches are excercised
        self.initialize_switches()

        signal = ["A", "B", "C", "D"]
        filter = [("A", 0xfe), ("B", 0xfd), ("C", 0xfb), ("D", 0xf7)]
        
        self.log("Connect a signal generator to input signal S0, and set it to about 11GHz at -20dBm.")      
        for x in range(4):
            try:
                t = self.manager.command(self.channel, self.node, 0x182, struct.pack("!B", 0xff))
            except:
                self.fail("Exception generating SET_SIGNAL_PATHS control point")  
            self.log("Connect a spectrum analyzer to signal " + signal[x]  + ", and set its output range to about 2GHz - 4GHz.")
            self.log("Currently, all output signals are connected to their respective high filters.")
            self.log("Verify that there is a valid signal on the spectrum analyzer.")
            self.log("When signal " + signal[x]  + " is connected to its low filter, the signal should significantly decrease")
            self.log("in amplitude.")
            rv = raw_input("Press <enter> when ready")
            for point in filter:
                try:
                    t = self.manager.command(self.channel, self.node, 0x182, struct.pack("!B", point[1]))
                except:
                    self.fail("Exception generating SET_SIGNAL_PATHS control point")                  
                self.log("Signal " + point[0] + " is connected to its low filter, all other signals are connected to their high filters")
                self.log("Verify whether the signal on the spectrum analyzer has significantly decreased, and verify whether or not this is correct.")
                rv = raw_input("Press <enter> to continue")

        self.endTestLog()
                
                
    def test39( self ):
        '''
	This test verifies an appropiate response from the GET_PROTOCOL_REV_LEVEL monitor point.
	'''
	self.startTestLog( "Test 18: GET_PROTOCOL_REV_LEVEL test" )
	self.log('''Verify that the correct revision level is printed to the screen.''')

	( r, t ) = self.manager.monitor( self.channel, self.node, 0x30000 )
	self.failUnless( len( r ) == 3, "GET_PROTOCOL_REV_LEVEL returned incorrect length value" )

	resp = struct.unpack( "!3B", r )

	print "V%x.%x.%x" % (resp[0], resp[1], resp[2])
		
	self.endTestLog()

        
    def test40( self ):
	'''
	This test verifies an appropiate response from the GET_AMBIENT_TEMPERATURE monitor point.
	'''
	self.startTestLog( "Test 20: GET_AMBIENT_TEMPERATURE test" )
	self.log('''Verify that the correct ambient temperature measured by DS1820 on M&C interface board is printed to the screen.''')

	( r, t ) = self.manager.monitor( self.channel, self.node, 0x30003 )
	self.failUnless( len( r ) == 4, "GET_AMBIENT_TEMPERATURE returned incorrect length value" )

	resp = struct.unpack( "!HBB", r )

	print "Bytes 0-1 = 0x%x, Byte 2 = 0x%x. Byte 3 = 0x%x" % (resp[0], resp[1], resp[2])
		
	self.endTestLog()


    def test41( self ):
	'''
	This test verifies an appropiate response from the GET_SW_REV_LEVEL monitor point.
	'''
	self.startTestLog( "Test 21: GET_SW_REV_LEVEL test" )
        self.log("Verify that the correct revision level of the embedded code is printed to the screen.")

	( r, t ) = self.manager.monitor( self.channel, self.node, 0x30004 )
	self.failUnless( len( r ) == 3, "GET_SW_REV_LEVEL returned incorrect length value" )

	resp = struct.unpack( "!3B", r )

	print "V%x.%x.%x" % (resp[0], resp[1], resp[2])

	self.endTestLog()        


    def test42(self):
        '''
        Verify that the hardware only responds to correctly constructed
        commands
        '''
        self.startTestLog("Invalid Command Test")

        self.log('''Verify that the hardware only responds to correctly
                    constructed commands.''')
        
    #tests control points in RW list
        rv = raw_input("Press <enter> to test control points in RW list")
        for point in self.RWList:
            format = "!%dB" % point[3]
            #Set the inital value to highest allowed
            dataSeq = []
            for x in range(0,point[3]):
                dataSeq.append(point[5])
                
            try:
                if (point[7]):
                    t = self.manager.commandTE(self.channel,self.node,point[1], struct.pack(format,*dataSeq))
                else:
                    t = self.manager.command(self.channel,self.node,point[1], struct.pack(format,*dataSeq))
            except:
                self.failed("Failed writing to SET_"+point[0])    

            #Verify that the write suceeded
            try:
                if (point[7]):
                    (r,t) = self.manager.monitorTE(self.channel,self.node, point[2])
                else:
                    (r,t) = self.manager.monitor(self.channel,self.node, point[2])
                if len(r) != point[3]:
                    self.failed("Incorrect data length returned by: READ_"+
                                point[0])
                elif (struct.unpack(format,r) != tuple(dataSeq)):
                    #Verify that the Read and Write match
                    self.failed("Incorrect data returned by: READ_" +
                                point[0])
            except:
                self.failed("Error reading from READ_"+point[0])

            for x in range(0,9):
                if x != point[3]:
                    errfmt = "!%dB" % x
                    errSeq = []
                    for y in range(0,x):
                        errSeq.append(point[4])

                    try:
                        if (point[7]):
                            t = self.manager.commandTE(self.channel,self.node, point[1], struct.pack(errfmt,*errSeq))
                        else:
                            t = self.manager.command(self.channel,self.node, point[1], struct.pack(errfmt,*errSeq))
                    except:
                        self.failed(("Error sending command of length %d"%x) +
                                    " to SET_"+point[0])
                    try:
                        if (point[7]):
                            (r,t) = self.manager.monitorTE(self.channel,self.node, point[2])
                        else:
                            (r,t) = self.manager.monitor(self.channel,self.node, point[2])
                        if len(r) != point[3]:
                            self.failed("Incorrect data length returned by:" +
                                        " READ_"+ point[0])
                        elif (struct.unpack(format,r) != tuple(dataSeq)):
                            #Verify that the Read and Write match
                            self.failed(("SET_%s responded " % point[0]) +
                                        ("to command of length %d"%x))
                    except:
                        self.failed("Error reading from READ_"+point[0])
          
    #tests control points in reset list
        rv = raw_input("Press <enter> to test control points in reset list")
        for point in self.resetList:
            for x in range(0,9):
                if x != 1:
                    errfmt = "!%dB" % x
                    errSeq = []
                    for y in range(0,x):
                        errSeq.append(0x01)

                    self.log("Take " + point[0] + " monitor point out of reset condition")
                    rv = raw_input("Press <enter> when ready")
                    try:
                        if (point[4]):
                            (r,t) = self.manager.monitorTE(self.channel,self.node, point[2])
                        else:
                            (r,t) = self.manager.monitor(self.channel,self.node, point[2])    
                        if struct.unpack("!%dB" % len(point[3]),r) == point[3]:
                            self.failed(point[0] + " monitor point already in reset condition")
                    except:
                        self.failed("Error reading from " + point[0] + " monitor point")

                    try:
                        t = self.manager.command(self.channel, self.node, point[1],
                                         struct.pack(errfmt, *errSeq))
                    except:
                        self.failed("Error writing to " + point[0] + " reset point")

                    try:
                        if (point[4]):
                            (r,t) = self.manager.monitorTE(self.channel,self.node, point[2])
                        else:
                            (r,t) = self.manager.monitor(self.channel,self.node, point[2])
                        if struct.unpack("!%dB" % len(point[3]),r) == point[3]:
                            self.failed(point[0] + " monitor point reset to command of length %d"%x)
                    except:
                        self.failed("Error reading from " + point[0] +" monitor point")

    #tests RESET_TPD_BOARD control point
        rv = raw_input("Press <enter> to test RESET_TPD_BOARD control point")
        for x in range(0,9): 
            if x != 1:
                errfmt = "!%dB" % x
                errSeq = []
                for y in range(0,x):
                    errSeq.append(0x01)
        
                try:
                    t = self.manager.command(self.channel, self.node, 0x81, struct.pack(errfmt, *errSeq))
                except:
                    self.failed("Error writing to RESET_TPD_BOARD control point")

                resp = self.test_for_reset(1)
                self.failUnless(resp != 0, "Reset occurred at RESET_TPD_BOARD command point of length %d" %x)

    #tests SET_ALMA_TIME control point
        rv = raw_input("Press <enter> to test SET_ALMA_TIME control point")
        self.log("Verify that the SET_ALMA_TIME does not reset the timestamp")
        try:
            ( r, t ) = self.manager.monitorTE( self.channel, self.node, 0x0c )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated monitoring point 0x0c" )
	rt = struct.unpack("!2I", r)
	resp = rt[1] + (rt[0]<<32)  
	self.log("beginning timestamp = 0x%x (%dns)"%(resp,resp)) 
        rv = raw_input("Press <enter> to begin")

        for x in range(0,8):
            errfmt = "!%dB" % x
            errSeq = []
            for y in range(0,x):
                errSeq.append(0x01)
             
            try:
                t = self.manager.commandTE(self.channel, self.node, 0x87, struct.pack(errfmt, *errSeq))
            except:
                self.failed("Error writing to SET_ALMA_TIME control point")

            try:
                ( r, t ) = self.manager.monitorTE( self.channel, self.node, 0x0c )
            except ControlExceptions.CAMBErrorEx:
                self.fail( "Exception generated monitoring point 0x0c" )
            rt = struct.unpack("!2I", r)
            resp = rt[1] + (rt[0]<<32)  
            self.log("new timestamp = 0x%x (%dns)"%(resp,resp))                 
            rv = raw_input("Press <enter> to continue")

    #tests INIT_TCP_CONN control point
        rv = raw_input("Press <enter> to test INIT_TCP_CONN control point")
        self.check_for_connection()
        for x in range(0,9):
            if x != 1:
                errfmt = "!%dB" % x
                errSeq = []
                for y in range(0,x):
                    errSeq.append(0xff)

                resp = self.establish_connection(errfmt, errSeq)
                if ((resp & 0x04) != 0x00):
                    self.failed("TCP connection occurred at INIT_TCP_CON control point of length %d"%x)
                    rv = raw_input("Press <enter> to disconnect TCP connection")
                    resp = self.end_transfer("!B", [0xff])
                    self.failUnless((resp & 0x10) == 0x00, "Data is being transfered after STOP_DATA was commanded")
                    self.failUnless((resp & 0x04) == 0x00, "TCP connection was not closed by STOP_DATA")

    #tests START_DATA control point
        rv = raw_input("Press <enter> to test START_DATA control point")
        format = "!B" 
        dataSeq = [0xff]

        resp = self.establish_connection(format, dataSeq)
        self.failUnless((resp & 0x04) == 0x04, "TCP connection was not made")
        self.failUnless((resp & 0x10) == 0x00, "Data is being transfered before START_DATA was commanded")

        for x in range(0,9):
            if x != 1:
                errfmt = "!%dB" % x
                errSeq = []
                for y in range(0,x):
                    errSeq.append(0xff)

                resp = self.begin_transfer(errfmt, errSeq)
                if ((resp & 0x10) != 0x00):
                    self.failed("Data transfer occurred at START_DATA control point of length %d" %x)
                    rv = raw_input("Press <enter> to disconnect, and re-establish connection")
                    resp = self.end_transfer("!B", [0xff])
                    self.failUnless((resp & 0x10) == 0x00, "Data is being transfered after STOP_DATA was commanded")
                    self.failUnless((resp & 0x04) == 0x00, "TCP connection was not closed by STOP_DATA")
                    resp = self.establish_connection(format, dataSeq)
                    self.failUnless((resp & 0x04) == 0x04, "TCP connection was not made")
                    self.failUnless((resp & 0x10) == 0x00, "Data is being transfered before START_DATA was commanded")

    #tests STOP_DATA control point
        rv = raw_input("Press <enter> to test STOP_DATA control point")
        format = "!B" 
        dataSeq = [0xff]
        
        resp = self.begin_transfer(format, dataSeq)
        self.failUnless((resp & 0x10) == 0x10, "Data did not begin to transfer.")

        for x in range(0,9):
            if x != 1:
                errfmt = "!%dB" % x
                errSeq = []
                for y in range(0,x):
                    errSeq.append(0xff)

                resp = self.end_transfer(errfmt, errSeq)
                if ((resp & 0x10) != 0x10 or (resp & 0x04) != 0x04):
                    self.failed("Data transfer stopped, connection closed at STOP_DATA control point of length %d"%x)
                    rv = raw_input("Press <enter> to re-establish connection and begin data transfer.")
                    resp = self.establish_connection(format, dataSeq)
                    self.failUnless((resp & 0x04) == 0x04, "TCP connection was not made")
                    self.failUnless((resp & 0x10) == 0x00, "Data is being transfered before START_DATA was commanded")
                    resp = self.begin_transfer(format, dataSeq)
                    self.failUnless((resp & 0x10) == 0x10, "Data did not begin to transfer.")

    #tests START_COMM_TEST control point
        rv = raw_input("Press <enter> to test START_COMM_TEST control point")
        self.failUnless(resp[3] == 00, "COMM_TEST must not be active to test START_COMM_TEST") 
        dataSeq = [0x01, 0x02, 0x04]
        for z in range (0,3):
            for x in range(0,9):
                if x != 1:
                    errfmt = "!%dB" % x
                    errSeq = []
                    for y in range(0,x):
                        errSeq.append(dataSeq[z])
                        
                    try:
                        t = self.manager.command(self.channel, self.node, 0x8b, struct.pack(errfmt, *errSeq))
                    except:
                        self.failed("Error writing to START_COMM_TEST control point")
                    try:
                        ( r, t ) = self.manager.monitor( self.channel, self.node, 0x01 )
                    except ControlExceptions.CAMBErrorEx:
                        self.fail( "Exception generated READ_STATUS monitor point" )
                    resp = struct.unpack( "!4B", r )
                    self.failUnless(resp[3]  == 0x00, "START_COMM_TEST occurred at control point of length %d" %x)

        self.endTestLog()        


    def test43(self):
        '''
        This test does a CAN monitor at each of the monitor points in
        the ICD.  The timing from the end of the request to the beginning of
        the response should be measured using an oscilloscope to verify that
        the delay is less than 150 us.
        '''
        self.startTestLog("Test 2: Monitor Timing Test")
        self.log('''Connect CAN wires (AMB pin 2 and 7) to oscilliscope and
                    set for single trigger.  Time base should be approximatly
                    200 us.
                    Press <enter> to generate monitor request,
                    n[ext] to process next monitor point.
                 ''')

        rv = raw_input("press <enter> to begin:")
        for point in self.validMonitor:
            rv = ''
            while len(rv) == 0 or rv != "n"[0:len(rv)]:
                try:
                    if (point[1]):
                        (r,t) = self.manager.monitorTE(self.channel,self.node,point[0])
                    else:
                        (r,t) = self.manager.monitor(self.channel,self.node,point[0])
                    rv=raw_input("CAN request sent to RCA: 0x%x "%point[0])
                except ControlExceptions.CAMBErrorEx:
                    self.fail("Exception generated monitoring point 0x%x"%point[0])
                    
        self.endTestLog()

                
    def test44(self):
        '''
        This test tests the TE related control and monitor points
        '''
        self.startTestLog("TE Related Test")
        self.log("Connect CAN wires (AMB pin 2 and 7) and TIM wires (AMB pin 4 and 8) to oscilliscope and set for single trigger.")

        rv = raw_input( "press <enter> to generate C/M point, n[ext] to generate next C/M point:" )

        self.log("Verify that all control points are transmitted between 0ms and 24ms after the occurrence of a TE.")
        for point in self.TEControl:
            format = "!%dB" % point[1]
            dataseq = []
            for i in range(0,point[1]):
                dataSeq.append(point[2])
                
            rv = ''
            while len( rv ) == 0 or rv != "n"[0:len( rv )]:
                try:
                    t = self.manager.commandTE(self.channel, self.node, point[0], struct.pack(format, *dataSeq))
                    rv=raw_input( "Control point sent to RCA: 0x%x " % point[0] )
                except ControlExceptions.CAMBErrorEx:
                    self.fail( "Exception generated control point 0x%x" % point[0] )
                                        
        self.log("Verify that all monitor points are transmitted between 24ms and 4ms before the occurrence of a TE.")
        for rca in self.TEMonitor:
            rv = ''
            while len( rv ) == 0 or rv != "n"[0:len( rv )]:
                try:
                    ( r, t ) = self.manager.monitorTE( self.channel, self.node, rca )
                    rv=raw_input( "Monitor point sent to RCA: 0x%x " % rca )
                except ControlExceptions.CAMBErrorEx:
                    self.fail( "Exception generated monitor point 0x%x" % rca )
                    
        self.endTestLog()


    def test45(self):
        '''
        This test monitors all RCAs between 0 and 0x3FFFF (the full
        range allocated to each device).  It ensures that only
        monitors documented in the ICD respond.
        
        (Although this test could be automated, it takes so long it is
        listed as an interactive test)
        '''
        self.startTestLog("Invalid Monitor Test")
        errorList = []

        for RCA in range (0x00000,0x40000):
            try:
                (r,t) = self.manager.monitor(self.channel, self.node, RCA)
                if RCA not in self.validMonitor:
                    errorList.append((RCA,len(r)))
            except:
                pass

        if len(errorList) != 0:
            self.failed("%d offending responses" % len(errorList))
            if len(errorList) < 20:
                for bad in errorList:
                    self.failed("RCA 0x%x returned %d bytes" % bad)
        self.endTestLog()


    def test46( self ):
	'''
	This test monitors all correct RCAs for incorrect node addresses.
	It ensures that the IFDC does not respond to any of these monitors.
	Although this is an automated test, it is in the interactive section
	because it takes a long time to run.
	'''
	self.startTestLog( "Test 5: Monitor Survey test pt. 2" )
	# errorList = {}
	for node in self.invalidNodes:
	    for point in self.validMonitor:
		try:
                    if (point[1]):
                        ( r, t ) = self.manager.monitorTE( self.channel, node , point[0])
                    else:
                        ( r, t ) = self.manager.monitor( self.channel, node , point[0])
		    self.fail( "IFDC responded at 0x%x RCA for node 0x%x" % ( point[0], node) )
		except:
		    pass
		
	self.endTestLog( )


    def test47( self ):
	'''
	This test toggles the reset wires.  It should be verified that the
	reset pulse is detected and the AMBSI resets.  In addition
	the levels on these lines should be checked using an oscilliscope
	'''
	self.startTestLog( "Test 3: AMB Reset Test" )
	self.log( '''Connect Reset wires (AMB pin 1 and 6) to oscilliscope
                     and set for single trigger.  Time base should be
                     approximatly 200 us
                  ''' )

        rv=raw_input( "Press <enter> to generate monitor point.")

        resp = self.test_for_reset(0)
        self.log("Number of transactions since power-up: %d" % resp)

	rv = raw_input( "Press <enter> to generate reset request, type e[xit] to exit ")

	while len( rv ) == 0 or rv != "e"[0:len( rv )]:
            t = self.manager.reset( self.channel )
            rv = raw_input( "Reset Generated " )

        rv=raw_input( "Press <enter> to generate monitor point.")
        resp = self.test_for_reset(0)
	self.log("Number of transactions since power-up: %d" % resp)

	self.endTestLog( )


    def test48( self ):
	self.log( "\nThis test does not require software. Test the TE levels on TIMA and TIMB \
with differential probe. Then measure the duty cycle (6ms out of 48ms?). Then, with a \
single ended probe, measure voltage between the AMB connector and TIMA/TIMB." )


    def test49( self ):
	'''
	This test tests that the CAN_H and CAN_L signals are at the correct voltages.
	'''
	self.startTestLog( "Test 16: CAN signal test" )
	self.log( '''Connect CAN wires (AMB pin 2 and 7) to oscilliscope and
                     set for single trigger.  Time base should be approximatly
                     200 us.  Verify that the differential voltage is 0V.  Then,
                     with a single ended probe, measure the voltage between the
                     AMB connector and CAN_L/CAN_H.  It should be 2V.  Press
                     <enter> to generate monitor request.  Verify that CAN_H and
                     CAN_L are in anti-phase and should be at 1.5V and 3.5V.
                  ''')

        rv = raw_input( "press <enter> to generate monitor request, and press e[xit] to exit.:" )
	rv = ''
	while len( rv ) == 0 or rv != "e"[0:len( rv )]:
            resp = self.test_for_reset(0)
            rv=raw_input( "CAN request sent to RCA: 0x30002" )

        self.endTestLog( "Test 16: CAN signal test" )        


#----------------------------------------------------------------------------
#The following functions are called by some of the test functions above
#----------------------------------------------------------------------------


    def check_for_connection(self):
        '''
        This function tests for a TCP connection.
        If there is one, fails and tells the user to terminate the connection. 
        '''
        format = "!4B"
        dataSeq = self.destIP             
        try:
            t = self.manager.command(self.channel, self.node, 0x84, struct.pack(format, *dataSeq))
        except:
            self.failed("Error writing to SET_DEST_IP_ADDR control point")
        format = "!4B" 
        dataSeq = self.validIP[0]             
        try:
            t = self.manager.command(self.channel, self.node, 0x83, struct.pack(format, *dataSeq))
        except:
            self.failed("Error writing to SET_MODULE_IP_ADDR control point")            
        format = "!H"         
        try:
            t = self.manager.command(self.channel, self.node, 0x85, struct.pack(format, self.PORT))
        except:
            self.failed("Error writing to SET_TCP_PORT control point")  

        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x01 )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated READ_STATUS monitor point" )
        resp = struct.unpack( "!4B", r )
        self.failUnless((resp[0] & 0x04) == 0x00, "Terminate the TCP connection before beginning this test.")


    def establish_connection(self, format, value):
        '''
        This function attempts to establish a connection
        '''

        self.log('''Run the IFProc_ethernet_connection.py script to create a socket for the
                    ABM so that it may listen for a connection request from the IFProc.''')
        rv = raw_input("Press <enter> when ready.")
        try:
            t = self.manager.command(self.channel, self.node, 0x88, struct.pack(format, *value))
        except:
            self.fail("Error writing to INIT_TCP_CONN control point")
            
        rv = raw_input("Press <enter> to verify whether or not a TCP connection was made.")
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x01 )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated READ_STATUS monitor point" )
        resp = struct.unpack( "!4B", r )
        return resp[0]


    def begin_transfer(self, format, value):
        '''
        This function attempts to begin data transfer
        '''
        try:
            t = self.manager.commandTE(self.channel, self.node, 0x89, struct.pack(format, *value))
        except:
            self.failed("Error writing to START_DATA control point")
        rv = raw_input("Press <enter> to verify whether or not a data transfer has begun.")
        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x01 )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated READ_STATUS monitor point" )
        resp = struct.unpack( "!4B", r )
        return resp[0]


    def end_transfer(self, format, value):
        '''
        This function attempts to end data transfer and close the connection
        '''
        try:
            t = self.manager.commandTE(self.channel, self.node, 0x8a, struct.pack(format, *value))
        except:
            self.fail("Error writing to STOP_DATA control point")
        rv = raw_input("Press <enter> to verify whether or not a the transfer has stopped.")            

        try:
            ( r, t ) = self.manager.monitor( self.channel, self.node, 0x01 )
        except ControlExceptions.CAMBErrorEx:
            self.fail( "Exception generated READ_STATUS monitor point" )
        resp = struct.unpack( "!4B", r )
        return resp[0]


    def test_gains(self):
        data = [0x00, 0x00, 0x00, 0x00, 0x00, 0x00]
        try:
            t = self.manager.command(self.channel, self.node, 0x181, struct.pack("!6B", *data))
        except:
            self.fail("Exception generating SET_GAINS control point")
        gain = []
        gain.append(("G0", [0xff, 0x00, 0x00, 0x00, 0x00, 0x00]))
        gain.append(("G1", [0xff, 0xff, 0x00, 0x00, 0x00, 0x00]))
        gain.append(("GA", [0xff, 0xff, 0xff, 0x00, 0x00, 0x00]))
        gain.append(("GB", [0xff, 0xff, 0xff, 0xff, 0x00, 0x00]))
        gain.append(("GC", [0xff, 0xff, 0xff, 0xff, 0xff, 0x00]))
        gain.append(("GD", [0xff, 0xff, 0xff, 0xff, 0xff, 0xff]))
        for point in gain:
            self.log("Gain " + point[0] + " has now been changed.")
            try:
                t = self.manager.command(self.channel, self.node, 0x181, struct.pack("!6B", *point[1]))
            except:
                self.fail("Exception generating SET_GAINS control point")
            self.log("Record whether the amplitude of the signal changed, and verify whether this is correct.")
            rv = raw_input("Press <enter> to continue")
   
        
# **************************************************
# MAIN Program
if __name__ == '__main__':
    ''' Usage: phase2test <testsuite.testname> <antenna>
    <antenna> defaults to DV01
    <testname> defaults to interactive
    eg., phase2test automated DV01 or
    phase2test.interactive.test01
    '''
    
    # For some reason ACS has problems unless I do this
    client = PySimpleClient.getInstance()
    if len(sys.argv) == 1:
        sys.argv.append("interactive")

    if len(sys.argv) == 2:
        sys.argv.append("DV01")

    antenna = sys.argv[2]
    sys.argv.remove(sys.argv[2])

    unittest.main()

