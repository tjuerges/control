from Acspy.Clients.SimpleClient import PySimpleClient
from CCL.IFProc import IFProc
simpleClient = PySimpleClient()

tppComp = simpleClient.getComponent("TPPTest")

tppOff = tppComp.getTPP()

antennas = ["DA41"]

tppOff.configureArray(antennas)

ifp0 = IFProc("DA41",0, stickyFlag=1)
ifp1 = IFProc("DA41",1, stickyFlag=1)
ifp0.hwConfigure()
ifp0.hwInitialize()
ifp0.hwOperational()
ifp1.hwConfigure()
ifp1.hwInitialize()
ifp1.hwOperational()




#ifp0 = IFProc("DA41",0)
#ifp1 = IFProc("DA41",1)
#ifp2 = IFProc("DV01",0)

ifp0.allocate(tppOff)
ifp1.allocate(tppOff)
#ifp2.allocate(tppOff)

ifp0.beginDataAcquisition(0)
ifp1.beginDataAcquisition(0)
#ifp2.beginDataAcquisition(0)

tppComp.beginSubscan()
tppOff.subscanComplete(60);

ifp0.abortDataAcquisition()
ifp1.abortDataAcquisition()
#ifp2.abortDataAcquisition()
ifp0.deallocate()
ifp1.deallocate()
#ifp2.deallocate()
simpleClient.releaseComponent("TPPTest")

