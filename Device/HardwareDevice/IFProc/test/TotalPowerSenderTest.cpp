/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2007-03-20  created
*/

/************************************************************************
*   NAME
*   
* 
*   SYNOPSIS
*   
* 
*   DESCRIPTION
*
*   FILES
*
*   ENVIRONMENT
*
*   COMMANDS
*
*   RETURN VALUES
*
*   CAUTIONS 
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS   
* 
*------------------------------------------------------------------------
*/


#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <TotalPowerSender.h>
#include <ACSErrTypeCommon.h>


static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

const char* TOTAL_POWER_DISTRIBUTOR_NAME = "ARCHIVE_TOTALPOWER_DISTRIBUTOR";


int basicInstanciationTest() {
  std::cout << "Basic Instanciation Test: " << std::endl;
  try {
    TotalPowerSender mySender(TOTAL_POWER_DISTRIBUTOR_NAME);
  } catch (ACSErrTypeCommon::CouldntPerformActionExImpl& ex) {
    std::cout << "\tFAILED" << std::endl;
    return -1;
  }

  std::cout << "\tPASSED" << std::endl;  
  return 0;
}


int main(int argc, char *argv[])
{
  int returnValue = 0;

  returnValue += basicInstanciationTest();

  std::cout << "Total Power Sender Test: ";
  if (returnValue) {
    std::cout << "FAILED" << std::endl;
  } else {
    std::cout << "PASSED" << std::endl;
  }

  return returnValue;
}








