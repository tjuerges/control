/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
package alma.Control.device.gui.IFProc.interim;

import java.util.Date;

import junit.framework.TestCase;

/**
 * Tests for methods needed to poll and plot data for an IFProc Base Band Pair.
 * 
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 7.0.0
 */
public class SampleUnitTests extends TestCase {
    
    public SampleUnitTests(String testName) {
        super(testName);
    }

    public void testConstructor() {
        assertEquals(TEST_TIME, sample.getTime());
        assertEquals(TEST_TIME_AS_DOUBLE, sample.getTimeAsDouble());
        assertEquals(TEST_VALUE, sample.getValue());
    }
        
    @Override
    protected void setUp() throws Exception {
        sample = new Sample(TEST_TIME, TEST_VALUE);
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    private final Date TEST_TIME = new Date();
    private final double TEST_TIME_AS_DOUBLE = TEST_TIME.getTime() * 1.0d;
    private final double TEST_VALUE = 3.1415927d;
    
    private Sample sample;
}

//
// O_o
