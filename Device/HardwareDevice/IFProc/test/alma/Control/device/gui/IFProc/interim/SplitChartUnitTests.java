/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
package alma.Control.device.gui.IFProc.interim;

import alma.Control.device.gui.IFProc.presentationModels.DevicePM;
import alma.acs.component.client.AdvancedComponentClient;
import alma.acs.container.ContainerServices;

import java.awt.Color;
import java.util.Properties;
import java.util.logging.Logger;

import junit.framework.TestCase;

/**
 * Tests for methods needed to create charts of IFProc data.
 * 
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 7.0.0
 */
public class SplitChartUnitTests extends TestCase {
    
    public SplitChartUnitTests(String testName) {
        super(testName);
    }

    public void testConstructor() {
        assertEquals(TEST_BBPS, dBmChart.getBaseBandPairs());
        assertNotNull(dBmChart.getChartPanel());
        assertEquals(ChartUnits.DBM, dBmChart.getUnits());
        
        assertEquals(TEST_BBPS, voltChart.getBaseBandPairs());
        assertNotNull(voltChart.getChartPanel());
        assertEquals(ChartUnits.VOLTS, voltChart.getUnits());
        
    }
        
    @Override
    protected void setUp() throws Exception {
        final String clientName = getClass().getName();
        String managerLoc = System.getProperty("ACS.manager");
        if (null == managerLoc) {
            fail("BaseBandPairUnitTests.setUp(): Failed to get ACS.manager property from environment.");
        } else {
            managerLoc = managerLoc.trim();
        }

        AdvancedComponentClient client = null;
        try {
            client = new AdvancedComponentClient(TEST_LOGGER, managerLoc, clientName);
        } catch (Exception e) {
            fail("BaseBandPairUnitTests.setUp(): Failed to get reference to client.");
        }
        
        ContainerServices container = client.getContainerServices();
        Properties properties = new Properties();

        final DevicePM TEST_DEVICE_PM = new DevicePM(
                TEST_ANTENNA, 
                new StandAloneContainerServices(container, properties),
                TEST_IFPROC);

        for (int i = 0; i < TEST_BBPS.length; i++) 
            TEST_BBPS[i] = new BaseBandPair(i, Color.BLACK, TEST_DEVICE_PM);
        
        dBmChart = new SplitChart(ChartUnits.DBM, TEST_BBPS, TEST_LOGGER);
        voltChart = new SplitChart(ChartUnits.VOLTS, TEST_BBPS, TEST_LOGGER);
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    private final String TEST_ANTENNA = "DA41";
    private final BaseBandPair[] TEST_BBPS = new BaseBandPair[4];
    private final Logger TEST_LOGGER = Logger.getAnonymousLogger();
    private final int TEST_IFPROC = 0;

    
    private SplitChart dBmChart;
    private SplitChart voltChart;
}

//
// O_o
