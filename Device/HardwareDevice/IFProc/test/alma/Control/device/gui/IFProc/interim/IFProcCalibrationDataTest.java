/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.IFProc.interim;

import alma.Control.device.gui.IFProc.presentationModels.IFProcCalibrationData;

import junit.framework.TestCase;

import java.util.logging.Logger;

/**
 * Tests for methods needed to read IFProc calibration data from $ACS_DATA/TMCDB_DATA
 * 
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 7.0.0
 */
public class IFProcCalibrationDataTest extends TestCase {
    
    public IFProcCalibrationDataTest(String testName) {
        super(testName);
    }

    /**
     * If a calibration file exists for an IFProc with the given serial number, get the 
     * calibration data from the file.
     * 
     * This test depends on the file $ACS_DATA/TMCDB_DATA/ifp_105.xml.  This is included
     * in the CONTROL Simulation CDB.
     */
    public void testConstructorForExistantFile() {
        IFProcCalibrationData calData = new IFProcCalibrationData(TEST_LOGGER, EXISTANT_SERIAL_NUMBER);
      for (int i = 0; i < 4; i++) {
          assertEquals(expectedExistantIntercepts[i], calData.getIntercept(i));
          assertEquals(expectedExistantSlopes[i], calData.getSlope(i));
      }
    }
        
    /**
     * If a calibration file does not exist for an IFProc with the given serial number, use 
     * calibration data.
     * 
     * This test depends on using a serial number that does not exist in the CONTROL Simulation CDB.
     */
    public void testConstructorForNonExistantFile() {
        IFProcCalibrationData calData = new IFProcCalibrationData(TEST_LOGGER, NON_EXISTANT_SERIAL_NUMBER);
      for (int i = 0; i < 4; i++) {
          assertEquals(expectedNonExistantIntercepts[i], calData.getIntercept(i));
          assertEquals(expectedNonExistantSlopes[i], calData.getSlope(i));
      }
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    private final Logger TEST_LOGGER = Logger.getAnonymousLogger();

    private final String EXISTANT_SERIAL_NUMBER = "105";
    private final float[] expectedExistantIntercepts = { 0.028200f, 0.052400f, 0.054800f, 0.062200f };
    private final float[] expectedExistantSlopes = { 1.495000f, 1.510000f, 1.370000f, 0.952500f };
    
    private final String NON_EXISTANT_SERIAL_NUMBER = "731";
    private final float[] expectedNonExistantIntercepts = { -0.117000f,  -0.101000f, -0.121000f, -0.098700f };
    private final float[] expectedNonExistantSlopes = { 1.630000f, 1.600000f, 1.580000f, 1.605000f };
}

//
// O_o
