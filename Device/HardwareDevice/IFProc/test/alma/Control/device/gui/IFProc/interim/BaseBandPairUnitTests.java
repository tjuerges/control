/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.IFProc.interim;

import alma.Control.device.gui.IFProc.presentationModels.DevicePM;
import alma.acs.component.client.AdvancedComponentClient;
import alma.acs.container.ContainerServices;

import info.monitorenter.gui.chart.ITrace2D;

import java.awt.Color;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Logger;

import junit.framework.TestCase;

/**
 * Tests for methods needed to poll and plot data for an IFProc Base Band Pair.
 * 
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 7.0.0
 */
public class BaseBandPairUnitTests extends TestCase {
    
    public BaseBandPairUnitTests(String testName) {
        super(testName);
    }

    public void testConstructor() {
        assertEquals(TEST_COLOR, bbp0.getColor());
        assertEquals(false, bbp0.getEnabled());
        assertNotNull(bbp0.getDBmMonitorPoint());
        assertNotNull(bbp0.getDBmTrace());
        assertEquals(TEST_DURATION_DEFAULT, bbp0.getDuration());
        assertEquals(TEST_NAME, bbp0.getName());
        assertEquals(TEST_BB_NUMBER, bbp0.getNumber());
        assertNotNull(bbp0.getVoltMonitorPoint());        
        assertNotNull(bbp0.getVoltTrace());
    }
    
    /**
     * This test exists to generate data in the container logs.
     * This will probably be removed later.
     * 
     * This test was broken by a design change.  New test TBD.
     */
//    public void testSampleAllMonitorPoints() {
//        bbp0.setEnabled(true);
//        bbp0.sampleMonitorPoints();
//        bbp1.setEnabled(true);
//        bbp1.sampleMonitorPoints();
//        bbp2.setEnabled(true);
//        bbp2.sampleMonitorPoints();
//        bbp3.setEnabled(true);
//        bbp3.sampleMonitorPoints();
//    }
    
   /** 
    * This test was broken by a design change.  New test TBD.
    */
    public void testSampleMonitorPointsWithBbpDisabled() {
//        // When disabled, there should still be updates.  This is to allow us to display useful 
//        // data as soon as the base band pair is enabled. 
//        bbp0.setEnabled(false);
//        
//        final ITrace2D dBmTrace = bbp0.getDBmTrace();
//        final ITrace2D voltTrace = bbp0.getVoltTrace();
//
//        final int dBmTraceItemCountBeforeUptate = dBmTrace.getSize();
//        final int voltTraceItemCountBeforeUpdate = voltTrace.getSize();
//        
////        bbp0.sampleMonitorPoints();
//        bbp0.addSamplesFromBuffersToTraces();
//        
//        final int dBmTraceItemCountAfterUptate = dBmTrace.getSize();
//        final int voltTraceItemCountAfterUpdate = voltTrace.getSize();
//        
//        assertTrue(dBmTraceItemCountAfterUptate > dBmTraceItemCountBeforeUptate);
//        assertTrue(voltTraceItemCountAfterUpdate > voltTraceItemCountBeforeUpdate);
    }
    
    /** 
     * This test was broken by a design change.  New test TBD.
     */
    public void testSampleMonitorPointsWithBbpEnabled() {
//        // When enabled, there should be an update.
//        bbp0.setEnabled(true);
//        
//        final ITrace2D dBmTrace = bbp0.getDBmTrace();
//        final ITrace2D voltTrace = bbp0.getVoltTrace();
//        
//        final int dBmTraceItemCountBeforeUptate = dBmTrace.getSize();
//        final int voltTraceItemCountBeforeUpdate = voltTrace.getSize();
//        
////        bbp0.sampleMonitorPoints();
//        
//        final int dBmTraceItemCountAfterUptate = dBmTrace.getSize();
//        final int voltTraceItemCountAfterUpdate = voltTrace.getSize();
//        
//        assertTrue(dBmTraceItemCountAfterUptate > dBmTraceItemCountBeforeUptate);
//        assertTrue(voltTraceItemCountAfterUpdate > voltTraceItemCountBeforeUpdate);
    }

    public void testSetDuration() {
        bbp0.setDuration(TEST_DURATION_LONG);
        assertEquals(TEST_DURATION_LONG, bbp0.getDuration());
    }

    public void testSetEnable() {
        bbp0.setEnabled(true);
        assertEquals(true, bbp0.getEnabled());
    }
    
    @Override
    protected void setUp() throws Exception {
        
        final String clientName = getClass().getName();
        String managerLoc = System.getProperty("ACS.manager");
        if (null == managerLoc) {
            fail("BaseBandPairUnitTests.setUp(): Failed to get ACS.manager property from environment.");
        } else {
            managerLoc = managerLoc.trim();
        }

        AdvancedComponentClient client = null;
        try {
            client = new AdvancedComponentClient(TEST_LOGGER, managerLoc, clientName);
        } catch (Exception e) {
            fail("BaseBandPairUnitTests.setUp(): Failed to get reference to client.");
        }
        
        ContainerServices container = client.getContainerServices();
        Properties properties = new Properties();


        final DevicePM TEST_DEVICE_PM = new DevicePM(
                TEST_ANTENNA, 
                new StandAloneContainerServices(container, properties),
                TEST_IFPROC);

        bbp0 = new BaseBandPair(0, Color.RED, TEST_DEVICE_PM);       
        bbp1 = new BaseBandPair(1, Color.ORANGE, TEST_DEVICE_PM);       
        bbp2 = new BaseBandPair(2, Color.GREEN, TEST_DEVICE_PM);       
        bbp3 = new BaseBandPair(3, Color.BLUE, TEST_DEVICE_PM);       
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    private final String TEST_ANTENNA = "DA41";
    private final int TEST_BB_NUMBER = 0;
    private final Color TEST_COLOR = Color.RED;
    private final int TEST_DURATION_DEFAULT = 30;
    private final int TEST_DURATION_LONG = 120;
    private final int TEST_IFPROC = 0;
    private final String TEST_NAME = "BBP0"; 
    private final Logger TEST_LOGGER = Logger.getAnonymousLogger();
    
    private BaseBandPair bbp0;
    private BaseBandPair bbp1;
    private BaseBandPair bbp2;
    private BaseBandPair bbp3;
}

//
// O_o
