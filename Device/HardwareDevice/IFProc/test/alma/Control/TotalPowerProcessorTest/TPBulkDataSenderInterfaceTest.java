/**
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2005 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * $Id$
 */

package alma.Control.TotalPowerProcessorTest;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

// import alma.acs.time.TimeHelper;

import alma.Control.TotalPowerProcessor.TPBulkDataSenderInterface;
// import alma.Control.TotalPowerProcessor.AntTPSubscanData;

// import alma.ControlExceptions.DeviceBusyEx;
import alma.Control.TotalPowerProcessor.TPBinaryHeaderInfo;
// import java.lang.Integer;
import alma.Control.TPProcessorPackage.SubscanInfo;
// import java.util.concurrent.Semaphore;
// import java.util.concurrent.TimeUnit;

// import java.io.DataOutputStream;

// import java.nio.ByteBuffer;
// import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * Test the Total Power Reciever
 */

import alma.acs.component.client.ComponentClientTestCase;
import alma.xmlstore.Identifier;
import alma.xmlstore.IdentifierHelper;

public class TPBulkDataSenderInterfaceTest extends ComponentClientTestCase {

    public TPBulkDataSenderInterfaceTest() throws Exception {
        super(TPBulkDataSenderInterfaceTest.class.getName());
    }

    public static void main (String[] args) {
        junit.textui.TestRunner.run (suite());
    }

    public static Test suite() {
        return new TestSuite(TPBulkDataSenderInterfaceTest.class);
    }

    protected void setUp() throws Exception {
        super.setUp();
        Logger.global.setLevel(Level.WARNING);
    }


    public void testFileWriterInterface() throws Exception {

        SubscanInfo subscanInfo = new SubscanInfo();

        subscanInfo.startTime = 0;
        subscanInfo.endTime   = 60000000;
        subscanInfo.dataOID   = "uid://X01/X02/X03";
        subscanInfo.execID    = "uid://X04/X05/X06";
        subscanInfo.scanNumber = 1;
        subscanInfo.subscanNumber = 2;
        subscanInfo.baseband  = new int[] {1,2,3,4};
        subscanInfo.polarization = new int[] {0,1};


        ArrayList<String> antennaList = new ArrayList<String>();
        antennaList.add("DV01"); 
        TPBinaryHeaderInfo headerInfo = new TPBinaryHeaderInfo(subscanInfo,
                1,antennaList);
        float[] tpData  = new float[10];
        int[] tpFlags = new int[10];

        for (int idx = 0; idx < 10; idx++) {
            tpData[idx] = idx * 2.0F;
            tpFlags[idx] = idx;
        }	
        TPBulkDataSenderInterface.writeBlob("./tmp",headerInfo,tpData, tpFlags);

    }

    public void testSendingDataToArchive() throws Exception {
        TPBulkDataSenderInterface senderInterface =
            new TPBulkDataSenderInterface(1);
        Identifier archiveIdentifier = IdentifierHelper.
            narrow(getContainerServices().getComponent("ARCHIVE_IDENTIFIER"));

        String[] uids = archiveIdentifier.getUIDs((short)2);

        SubscanInfo subscanInfo = new SubscanInfo();

        subscanInfo.startTime = 0;
        subscanInfo.endTime   = 60000000;
        subscanInfo.dataOID   = uids[0];
        subscanInfo.execID    = uids[1];
        subscanInfo.scanNumber = 1;
        subscanInfo.subscanNumber = 2;
        subscanInfo.baseband  = new int[] {1,2,3,4};
        subscanInfo.polarization = new int[] {0,1};


        ArrayList<String> antennaList = new ArrayList<String>();
        antennaList.add("DV01"); 

        TPBinaryHeaderInfo headerInfo = new TPBinaryHeaderInfo(subscanInfo,
                1,antennaList);
        float[] tpData  = new float[100];
        int[] tpFlags = new int[100];

        for (int idx = 0; idx < 100; idx++) {
            tpData[idx] = idx * 2.0F;
            tpFlags[idx] = idx;
        }	

        System.out.println("About to call Archive Blob in test!");
        senderInterface.archiveBlob(headerInfo,tpData,tpFlags);

        senderInterface.close();
    }

}
