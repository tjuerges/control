/*
   ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

/** 
 * @author  nbarriga
 * @version $Id$
 * @since    
 */

package alma.Control.TotalPowerProcessorTest;

import alma.Control.TPPTestOperations;
import alma.Control.TPProcessor;
import alma.Control.TPProcessorHelper;
import alma.Control.TPProcessorPOATie;
import alma.Control.TotalPowerProcessor.TotalPowerProcessorImpl;
import alma.Control.TPProcessorPackage.SubscanInfo;

import alma.acs.time.TimeHelper;



import alma.acs.component.ComponentLifecycle;
import alma.acs.component.ComponentLifecycleException;
import alma.acs.container.ContainerServices;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.acs.logging.AcsLogger;
import alma.ACS.ComponentStates;
import alma.archive.components.IdentifierImpl;

import java.util.logging.Logger;

public class TPPTestImpl implements ComponentLifecycle, TPPTestOperations {
    private TotalPowerProcessorImpl totalPowerProc;
    private TPProcessor tppOffshoot;

    private AcsLogger logger;
    private ContainerServices CS;
    private String compName;
    private IdentifierImpl identifier;
    public void initialize(ContainerServices containerServices) throws ComponentLifecycleException{
        CS=containerServices;
        logger=CS.getLogger();
        compName=CS.getName();
        // get identifier instance:
        identifier = new IdentifierImpl();
        identifier.initialize(CS);

    }
    public void execute() throws ComponentLifecycleException{

    }
    public void cleanUp(){

    }
    public void aboutToAbort(){

    }

    public ComponentStates componentState() {
        return CS.getComponentStateManager().getCurrentState();
    }

    public String name() {
        return CS.getName();
    }

    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    public boolean equals(Object arg0) {
        return super.equals(arg0);
    }
    protected void finalize() throws Throwable {
        super.finalize();
    }
    public int hashCode() {
        return super.hashCode();
    }
    public String toString() {
        return super.toString();
    }



    public TPProcessor getTPP(){
        totalPowerProc = new TotalPowerProcessorImpl((Logger)logger);
        try{
            tppOffshoot = TPProcessorHelper.narrow(CS.activateOffShoot(new TPProcessorPOATie(totalPowerProc)));
        }catch(AcsJContainerServicesEx ex){
            ex.log(logger);
        }
        return tppOffshoot;

    }

    public void beginSubscan(){
        try{
            int[] bbSeq = {0,1,2,3};
            int[] polSeq = {0,1};

            SubscanInfo subscanInfo = new SubscanInfo();

            subscanInfo.startTime  = TimeHelper.getTimeStamp().value + 50000000;
            subscanInfo.startTime -= (subscanInfo.startTime % 480000);

            subscanInfo.endTime       = subscanInfo.startTime + 60000000; // 6 Sec subscan

            // get 1 UID, check it's syntax:
            String uids[] = identifier.getUIDs((short) 2);

            subscanInfo.dataOID       = uids[0];
            subscanInfo.execID        = uids[1];
            subscanInfo.scanNumber    = 1;
            subscanInfo.subscanNumber = 1;
            subscanInfo.baseband      = bbSeq;
            subscanInfo.polarization  = polSeq;


            // Set a new frecquency (default 2Khz)
            //totalPowerProc.setSampleRate(500); // 500Hz

            totalPowerProc.beginSubscan(subscanInfo);
        }catch(Exception ex){
            logger.info(ex.toString());
        }


    }

    public SubscanInfo createSubscanInfo(){
        SubscanInfo subscanInfo=null;
        try{
            int[] bbSeq = {0,1,2,3};
            int[] polSeq = {0,1};

            subscanInfo = new SubscanInfo();

            subscanInfo.startTime  = TimeHelper.getTimeStamp().value + 10000000;
            subscanInfo.startTime -= (subscanInfo.startTime % 480000);

            subscanInfo.endTime       = subscanInfo.startTime + 60000000; // 6 Sec subscan

            // get 1 UID, check it's syntax:
            String uids[] = identifier.getUIDs((short) 2);

            subscanInfo.dataOID       = uids[0];
            subscanInfo.execID        = uids[1];
            subscanInfo.scanNumber    = 1;
            subscanInfo.subscanNumber = 1;
            subscanInfo.baseband      = bbSeq;
            subscanInfo.polarization  = polSeq;


        }catch(Exception ex){
            logger.info(ex.toString());
        }
        return subscanInfo;


    }
}
