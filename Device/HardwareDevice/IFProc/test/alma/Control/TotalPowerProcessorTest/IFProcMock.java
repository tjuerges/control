/**
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2005 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * $Id$
 */

package alma.Control.TotalPowerProcessorTest;


import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import alma.acs.time.TimeHelper;

import alma.Control.TotalPowerProcessor.IFProcClient;
import alma.Control.TotalPowerProcessor.AntTPSubscanData;

import alma.ControlExceptions.DeviceBusyEx;

import java.lang.Integer;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import java.io.DataOutputStream;

import java.nio.ByteBuffer;
import java.nio.ShortBuffer;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Test the Total Power Reciever
 */

public  class IFProcMock implements Runnable { 	
    private Socket  deviceSocket;
    private Integer bdbSize = new Integer(192);
    private long    startTime = 0;
    private long    endTime   = 0;
    private short   bdbValue;


    public IFProcMock(int tcpPort) 
        throws Exception
        {
            InetAddress address = InetAddress.getLocalHost();
            deviceSocket = new Socket(address, tcpPort);
        }


    public void close() 
        throws Exception
        {
            deviceSocket.close();
        }


    public void run() {
        DataOutputStream socketOut = null; 
        long currentTime;

        System.out.println("Creating Output Stream");
        try {
            socketOut = 
                new DataOutputStream(deviceSocket.getOutputStream());
        } catch (java.io.IOException ex) {
            System.out.println("Error creating Output Stream");
        }

        long timestamp = TimeHelper.getTimeStamp().value;
        timestamp -= timestamp % 480000;
        timestamp += 240000; // Ensure that the BDBs are misaligned w/ TEs

        try {
            while (! deviceSocket.isClosed()) {
                try {
                    currentTime = TimeHelper.getTimeStamp().value;
                    if (timestamp + (bdbSize * 5000) > currentTime)
                        Thread.sleep((timestamp + (bdbSize * 5000) -
                                    currentTime)/10000);
                    System.out.println("Sending a Basic Data Block");
                    /* Write out a packet */
                    socketOut.writeByte(bdbSize.byteValue());
                    socketOut.writeLong(timestamp * 100); // ACS -> ns
                    socketOut.write(getDataBuffer(timestamp), 0,
                            bdbSize*8);
                    Thread.sleep(50);
                } catch (java.lang.InterruptedException ex) {
                    // Ok, probably just closing
                }
                timestamp += bdbSize * 5000;
            }
        } catch (java.io.IOException ex) {
            try {
                close();
            } catch (Exception e) {
                // Ok no worries
            }
        }
    }

    public byte[] getDataBuffer(long timestamp) {
        byte[] dataBuffer = new byte[bdbSize*8];
        ByteBuffer  byteBuf = ByteBuffer.wrap(dataBuffer);
        ShortBuffer shortBuf = byteBuf.asShortBuffer();

        for (int idx = 0; idx < bdbSize; idx++) {
            if (timestamp + (idx*5000) >= startTime &&
                    timestamp + (idx*5000) < endTime) {
                for (int bbIdx = 0; bbIdx < 4; bbIdx++)
                    shortBuf.put((4 * idx) + bbIdx, bdbValue);
                bdbValue++;
            } else {
                for (int bbIdx = 0; bbIdx < 4; bbIdx++) 
                    shortBuf.put((4 * idx) + bbIdx, (short)0);
            }
        }
        return dataBuffer;
    }

    public void beginSubscan(long startTime,
            long endTime) {
        this.startTime = startTime;
        this.endTime   = endTime;
    }
}
