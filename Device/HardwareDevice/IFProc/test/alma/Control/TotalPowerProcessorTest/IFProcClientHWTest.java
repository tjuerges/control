/**
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2005 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * $Id$
 */

package alma.Control.TotalPowerProcessorTest;


import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import alma.acs.time.TimeHelper;

import alma.Control.TotalPowerProcessor.IFProcClient;
import alma.Control.TotalPowerProcessor.AntTPSubscanData;

import alma.Control.TotalPowerProcessorTest.IFProcMock;

import alma.ControlExceptions.DeviceBusyEx;

import java.lang.Integer;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import java.io.DataOutputStream;

import java.nio.ByteBuffer;
import java.nio.ShortBuffer;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.IOException;

/**
 * Test the Total Power Reciever
 */

public class IFProcClientHWTest {

    

    public static void main (String[] args) {
	
	Thread        clientThread = null;
	IFProcClient  testClient   = null;
	ServerSocket  serverSocket = null;
	int           clientPort;
	AntTPSubscanData subscanData = null;
	try {
	    serverSocket = new ServerSocket(0);
	} catch (IOException ex) {
	    System.out.println("Error opening socket");
	    ex.printStackTrace();
	    return;
	}

	clientPort = serverSocket.getLocalPort();
	testClient = new IFProcClient("DV01", 0, serverSocket, null);
	clientThread = new Thread(testClient);
	clientThread.start();

	try {
	    System.out.println("Please instruct IFProc to connect to:" 
			       + "\n\t IP Address: " + 
			       InetAddress.getLocalHost().getHostAddress() 
			       + "\n\t Port: " + clientPort);
	} catch (java.net.UnknownHostException ex) {
	    ex.printStackTrace();
	    return;
	}
	    
	try {
	    while (! testClient.isConnected() ) {
		Thread.sleep(250);
	    }

	    System.out.println("Socket is now connected");
	    System.out.println("Please instruct IFProc to begin sending data");
	    Thread.sleep(45000);

	    long beginTime = TimeHelper.getTimeStamp().value + 10000000;
	    beginTime      -= (beginTime % 480000);
	    long endTime   = beginTime + 100000000; // 10 Sec subscan
	    
	    System.out.println("Now beginning to acquire a 10 second subscan: "
			       + "\n\t StartTime: " + beginTime 
			       + "\n\t EndTime:   " + endTime);
	    Semaphore doneSignal = new Semaphore(0);
	    
	    testClient.beginSubscan(beginTime, endTime, doneSignal);
	
	    boolean success = doneSignal.tryAcquire(1, 15, TimeUnit.SECONDS);

	    if (success) {
		System.out.println("Successfully acquired subscan.");
	    } else {
		System.out.println("Timed out acquiring subscan.");
		testClient.abortSubscan();
	    }

	    subscanData = testClient.getSubscanData();
	    

	} catch (java.lang.InterruptedException ex) {
	    ex.printStackTrace();
	    return;
	} catch (alma.ControlExceptions.DeviceBusyEx ex) {
	    ex.printStackTrace();
	    return;
	}

	





	testClient.close();
    }

}
