/**
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2005 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * $Id$
 */

package alma.Control.TotalPowerProcessorTest;


import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import alma.acs.time.TimeHelper;

import alma.Control.TotalPowerProcessor.ArrayTPSubscanData;
import alma.Control.TotalPowerProcessor.TotalPowerProcessorImpl;

import alma.Control.TPProcessorPackage.SubscanInfo;
import alma.ControlDeviceExceptions.IllegalConfigurationEx;



import java.util.logging.Logger;
import java.util.logging.Level;
import java.lang.Thread;

/**
 * Test the Total Power Reciever
 */

public class TotalPowerProcessorImplTest extends TestCase {


    private TotalPowerProcessorImpl tpProc = null;

    public static void main (String[] args) {
        junit.textui.TestRunner.run (suite());
    }

    public static Test suite() {
        return new TestSuite(TotalPowerProcessorImplTest.class);
    }


    /**
     * Test case fixture setup.
     * 
     * The following operations are performed:
     * <OL>
     *   <LI> Get a reference to the CONTROL/MASTER component.
     *   <LI> Get references to the ARCHIVE client components.
     * </OL>
     */
    protected void setUp() throws Exception {
        super.setUp();
        Logger.global.setLevel(Level.WARNING);
        tpProc = new TotalPowerProcessorImpl(Logger.global);
    }

    /**
     * Test case fixture clean up.
     */
    protected void tearDown() throws Exception {
        tpProc.shutdown();
        super.tearDown();
    }

    /* Test 1 */
    public void testSimpleLifecycle() throws Exception {
        assertNotNull(tpProc);
        Thread.sleep(2000);
        tpProc.shutdown();

    }

    /* In this case the remote client closes the connection first*/
    /* Test 2 */
    public void testSimpleConnection() throws Exception {
        int clientPort1 = tpProc.addDigitizer("DV01",0);
        int clientPort2 = tpProc.addDigitizer("DV01",1);
        IFProcMock testDevice1 = new IFProcMock(clientPort1);
        IFProcMock testDevice2 = new IFProcMock(clientPort2);
        Thread.sleep(2000);
        testDevice1.close();
        Thread.sleep(500); 
        tpProc.shutdown();
        Thread.sleep(500);
        testDevice2.close();
    }

    /* Test 3 */
    public void testWritingData() throws Exception {
        int        clientPort1 = tpProc.addDigitizer("DV01",0);
        int        clientPort2 = tpProc.addDigitizer("DV01",1);
        IFProcMock testDevice1 = new IFProcMock(clientPort1);
        IFProcMock testDevice2 = new IFProcMock(clientPort2);
        Thread     testThread1 = new Thread(testDevice1);
        Thread     testThread2 = new Thread(testDevice2);
        testThread1.start();
        testThread2.start();
        Thread.sleep(2000);
    }

    /* Test 4 */
    public void testAntennaAllocation() throws Exception {
        int        clientPort1 = tpProc.addDigitizer("DV01",0);
        int        clientPort2 = tpProc.addDigitizer("DV01",1);
        IFProcMock testDevice1 = new IFProcMock(clientPort1);
        IFProcMock testDevice2 = new IFProcMock(clientPort2);
        Thread     testThread1 = new Thread(testDevice1);
        Thread     testThread2 = new Thread(testDevice2);
        testThread1.start();
        testThread2.start();
        String[] antList = {"DV01"};

        int[] bbSeq = {0,2};
        int[] polSeq = {0,1};


        SubscanInfo header = new SubscanInfo();

        header.startTime  = TimeHelper.getTimeStamp().value + 10000000;
        header.startTime -= (header.startTime % 480000);

        header.endTime       = header.startTime + 60000000; // 6 Sec subscan
        header.dataOID       = "uid:\\\\X3791eb59\\X6ad43d3f";
        header.execID        = "uid:\\\\X3791eb59\\X6ad43d3e";;
        header.scanNumber    = 1;
        header.subscanNumber = 1;
        header.baseband      = bbSeq;
        header.polarization  = polSeq;

        try {
            tpProc.beginSubscan(header);
            fail("Error NoAntennas Exception not thrown " 
                    + "that should have been");
        } catch (IllegalConfigurationEx ex ) {
            /* Properly caught exception.  Continue */
        } 
        tpProc.configureArray(antList);

        tpProc.beginSubscan(header);
        tpProc.subscanComplete(10);

    }

    /* Test 5 */
    public void testSubscanInterface() throws Exception {
        int        clientPort1 = tpProc.addDigitizer("DV01",0);
        int        clientPort2 = tpProc.addDigitizer("DV01",1);
        IFProcMock testDevice1 = new IFProcMock(clientPort1);
        IFProcMock testDevice2 = new IFProcMock(clientPort2);
        Thread     testThread1 = new Thread(testDevice1);
        Thread     testThread2 = new Thread(testDevice2);
        testThread1.start();
        testThread2.start();
        String[] antList = {"DV01"};
        tpProc.configureArray(antList);
        int[] bbSeq = {0,2};
        int[] polSeq = {0,1};


        SubscanInfo subscanInfo = new SubscanInfo();

        subscanInfo.startTime  = TimeHelper.getTimeStamp().value + 10000000;
        subscanInfo.startTime -= (subscanInfo.startTime % 480000);

        subscanInfo.endTime       = subscanInfo.startTime + 60000000; // 6 Sec subscan
        subscanInfo.dataOID       = "uid:\\\\X3791eb59\\X6ad43d3f";
        subscanInfo.execID        = "uid:\\\\X3791eb59\\X6ad43d3e";;
        subscanInfo.scanNumber    = 1;
        subscanInfo.subscanNumber = 1;
        subscanInfo.baseband      = bbSeq;
        subscanInfo.polarization  = polSeq;

        testDevice1.beginSubscan(subscanInfo.startTime, subscanInfo.endTime);
        testDevice2.beginSubscan(subscanInfo.startTime, subscanInfo.endTime);

        tpProc.beginSubscan(subscanInfo);
        tpProc.subscanComplete(10);

        ArrayDataChecker dataChecker = 
            new ArrayDataChecker(tpProc.getSubscanData(), antList, 
                    polSeq, bbSeq, tpProc.getSampleRate());

        if (dataChecker.checkDataLength()) {
            fail("Incorrect number of data points in array data!");
        }

        if (dataChecker.checkFlagLength()) {
            fail("Incorrect number of flag points in array data!");
        }

        if (dataChecker.checkDataValue()) {
            fail("There was at least one point which had incorrect data!");
        }   

        if (dataChecker.checkFlagValue()) {
            fail("There was at least one point which had data flagged bad!");
        }
    }

    /* Test 6 */
    public void testAveragingInterface() throws Exception {
        int        clientPort1 = tpProc.addDigitizer("DV01",0);
        int        clientPort2 = tpProc.addDigitizer("DV01",1);
        IFProcMock testDevice1 = new IFProcMock(clientPort1);
        IFProcMock testDevice2 = new IFProcMock(clientPort2);
        Thread     testThread1 = new Thread(testDevice1);
        Thread     testThread2 = new Thread(testDevice2);
        testThread1.start();
        testThread2.start();
        String[] antList = {"DV01"};
        tpProc.configureArray(antList);
        int[] bbSeq = {0,2};
        int[] polSeq = {0,1};

        tpProc.setSampleRate(500.0);
        double sampleRate = tpProc.getSampleRate();
        assertEquals("Error Setting sample rate to 500.0 Hz returned rate.",
                500.0, sampleRate);

        SubscanInfo subscanInfo = new SubscanInfo();

        subscanInfo.startTime  = TimeHelper.getTimeStamp().value + 10000000;
        subscanInfo.startTime -= (subscanInfo.startTime % 480000);

        subscanInfo.endTime       = subscanInfo.startTime + 60000000; // 6 S
        subscanInfo.dataOID       = "uid:\\\\X3791eb59\\X6ad43d3f";
        subscanInfo.execID        = "uid:\\\\X3791eb59\\X6ad43d3e";;
        subscanInfo.scanNumber    = 1;
        subscanInfo.subscanNumber = 1;
        subscanInfo.baseband      = bbSeq;
        subscanInfo.polarization  = polSeq;

        testDevice1.beginSubscan(subscanInfo.startTime, subscanInfo.endTime);
        testDevice2.beginSubscan(subscanInfo.startTime, subscanInfo.endTime);

        tpProc.beginSubscan(subscanInfo);
        tpProc.subscanComplete(10);

        ArrayDataChecker dataChecker = 
            new ArrayDataChecker(tpProc.getSubscanData(), antList,
                    polSeq, bbSeq, tpProc.getSampleRate());

        if (dataChecker.checkDataLength()) {
            fail("Incorrect number of data points in array data!");
        }

        if (dataChecker.checkFlagLength()) {
            fail("Incorrect number of flag points in array data!");
        }

        if (dataChecker.checkDataValue()) {
            fail("There was at least one point which had incorrect data!");
        }   

        if (dataChecker.checkFlagValue()) {
            fail("There was at least one point which had data flagged bad!");
        }
    }


    /* Test 7 */
    public void testSubscanMissingData() throws Exception {
        int        clientPort1 = tpProc.addDigitizer("DV01",0);
        int        clientPort2 = tpProc.addDigitizer("DV01",1);
        IFProcMock testDevice1 = new IFProcMock(clientPort1);
        Thread     testThread1 = new Thread(testDevice1);
        testThread1.start();
        String[] antList = {"DV01"};
        tpProc.configureArray(antList);

        int[] bbSeq = {0,2};
        int[] polSeq = {0,1};


        SubscanInfo header = new SubscanInfo();

        header.startTime  = TimeHelper.getTimeStamp().value + 10000000;
        header.startTime -= (header.startTime % 480000);

        header.endTime       = header.startTime + 60000000; // 6 Sec subscan
        header.dataOID       = "uid:\\\\X3791eb59\\X6ad43d3f";
        header.execID        = "uid:\\\\X3791eb59\\X6ad43d3e";;
        header.scanNumber    = 1;
        header.subscanNumber = 1;
        header.baseband      = bbSeq;
        header.polarization  = polSeq;

        tpProc.beginSubscan(header);
        tpProc.subscanComplete(10);
    }


    /* Test that we can abort a subscan in progress */
    /* Test 8*/
    public void testSubscanAbort() throws Exception {
        int        clientPort1 = tpProc.addDigitizer("DV01",0);
        int        clientPort2 = tpProc.addDigitizer("DV01",1);
        IFProcMock testDevice1 = new IFProcMock(clientPort1);
        IFProcMock testDevice2 = new IFProcMock(clientPort2);
        Thread     testThread1 = new Thread(testDevice1);
        Thread     testThread2 = new Thread(testDevice2);
        testThread1.start();
        testThread2.start();
        String[] antList = {"DV01"};
        tpProc.configureArray(antList);

        int[] bbSeq = {0,2};
        int[] polSeq = {0,1};


        SubscanInfo header = new SubscanInfo();

        header.startTime  = TimeHelper.getTimeStamp().value + 10000000;
        header.startTime -= (header.startTime % 480000);

        header.endTime       = header.startTime + 600000000; // 60 Sec subscan
        header.dataOID       = "uid:\\\\X3791eb59\\X6ad43d3f";
        header.execID        = "uid:\\\\X3791eb59\\X6ad43d3e";;
        header.scanNumber    = 1;
        header.subscanNumber = 1;
        header.baseband      = bbSeq;
        header.polarization  = polSeq;

        tpProc.beginSubscan(header);

        tpProc.abortSubscan();
        tpProc.subscanComplete(10); 

    }


    /* Test proper timeout waiting for data */
    /* Test 9*/
    public void testSubscanTimeout() throws Exception {
        int        clientPort1 = tpProc.addDigitizer("DV01",0);
        int        clientPort2 = tpProc.addDigitizer("DV01",1);
        IFProcMock testDevice1 = new IFProcMock(clientPort1);
        IFProcMock testDevice2 = new IFProcMock(clientPort2);
        Thread     testThread1 = new Thread(testDevice1);
        Thread     testThread2 = new Thread(testDevice2);
        testThread1.start();
        testThread2.start();
        String[] antList = {"DV01"};
        tpProc.configureArray(antList);

        int[] bbSeq = {0,2};
        int[] polSeq = {0,1};


        SubscanInfo header = new SubscanInfo();

        header.startTime  = TimeHelper.getTimeStamp().value + 10000000;
        header.startTime -= (header.startTime % 480000);

        header.endTime       = header.startTime + 600000000; // 60 Sec subscan
        header.dataOID       = "uid:\\\\X3791eb59\\X6ad43d3f";
        header.execID        = "uid:\\\\X3791eb59\\X6ad43d3e";;
        header.scanNumber    = 1;
        header.subscanNumber = 1;
        header.baseband      = bbSeq;
        header.polarization  = polSeq;

        tpProc.beginSubscan(header);
        //try
        tpProc.subscanComplete(10); // This should timeout!
        //catch
        tpProc.abortSubscan(); // so it ends;
        return;
        // else
        // fail
    }

    /* Test proper rejection of incorrect data */
    /* Test 10*/
    public void testRejectWrongData() throws Exception {
        int        clientPort1 = tpProc.addDigitizer("DV01",0);
        int        clientPort2 = tpProc.addDigitizer("DV01",1);
        int        clientPort3 = tpProc.addDigitizer("DV01",2);
        int        clientPort4 = tpProc.addDigitizer("DV01",3);
        IFProcMock testDevice1 = new IFProcMock(clientPort1);
        IFProcMock testDevice2 = new IFProcMock(clientPort2);
        IFProcMock testDevice3 = new IFProcMock(clientPort3);
        IFProcMock testDevice4 = new IFProcMock(clientPort4);
        Thread     testThread1 = new Thread(testDevice1);
        Thread     testThread2 = new Thread(testDevice2);
        Thread     testThread3 = new Thread(testDevice1);
        Thread     testThread4 = new Thread(testDevice2);
        testThread1.start();
        testThread2.start();
        testThread3.start();
        testThread4.start();
        String[] antList = {"DV01"};
        tpProc.configureArray(antList);
        int[] bbSeq = {0,2};
        int[] polSeq = {0,1};


        SubscanInfo header = new SubscanInfo();

        header.startTime  = TimeHelper.getTimeStamp().value + 10000000;
        header.startTime -= (header.startTime % 480000);

        header.endTime       = header.startTime + 60000000; // 6 Sec subscan
        header.dataOID       = "uid:\\\\X3791eb59\\X6ad43d3f";
        header.execID        = "uid:\\\\X3791eb59\\X6ad43d3e";;
        header.scanNumber    = 1;
        header.subscanNumber = 1;
        header.baseband      = bbSeq;
        header.polarization  = polSeq;

        tpProc.beginSubscan(header);
        tpProc.subscanComplete(10);
    }

    /* Test the sample rate interface */
    /* Test 11*/
    public void testSampleRateInterface() throws Exception {
        double sampleRate;

        sampleRate = tpProc.getSampleRate();
        assertEquals("Default Sample Rate not expected value: " + sampleRate,
                2000.0, sampleRate);

        tpProc.setSampleRate(1000.0);
        sampleRate = tpProc.getSampleRate();
        assertEquals("Error Setting sample rate to 1000.0 Hz returned rate: " 
                + sampleRate, 1000.0 , sampleRate);

        tpProc.setSampleRate(666.0);
        sampleRate = tpProc.getSampleRate();
        assertEquals("Error Setting sample rate to 666.0 Hz returned rate: " 
                + sampleRate, 2000.0/3 , sampleRate);

        tpProc.setSampleRate(0.25);
        sampleRate = tpProc.getSampleRate();
        assertEquals("Error Setting sample rate to 0.25 Hz returned rate: " 
                + sampleRate, 0.25 , sampleRate);

        tpProc.setSampleRate(2000.0);
        sampleRate = tpProc.getSampleRate();
        assertEquals("Error Setting sample rate to 2000.0 Hz returned rate: " 
                + sampleRate, 2000.0 , sampleRate);
    }


    private class ArrayDataChecker {
        private ArrayTPSubscanData arrayData;
        private String[] antList;
        private int[]    polSeq;
        private int[]    bbSeq;
        private int      avgLength;

        public ArrayDataChecker(ArrayTPSubscanData arrayData, 
                String[] antList, 
                int[]    polSeq, 
                int[]    bbSeq,
                double   sampleRate) {
            this.arrayData = arrayData;
            this.antList = antList;
            this.polSeq = polSeq;
            this.bbSeq = bbSeq;
            this.avgLength = (int)(2000.0/sampleRate);
        }	    


        public boolean checkDataLength() {
            return arrayData.tpData.length != 
                arrayData.headerInfo.numIntegration * antList.length * 
                bbSeq.length * polSeq.length;
        }

        public boolean checkFlagLength() {
            return arrayData.tpFlags.length != 
                arrayData.headerInfo.numIntegration * antList.length * 
                bbSeq.length * polSeq.length;
        }

        public boolean checkDataValue() {
            double predictedValue;
            int intOffset;
            int antOffset;
            int bbOffset;

            for (int idx = 0; idx <arrayData.headerInfo.numIntegration; idx++){
                predictedValue = ((avgLength-1)/2.0) + (avgLength*idx);
                intOffset = idx * antList.length;
                for (int antIdx = 0; antIdx < antList.length; antIdx++) {
                    antOffset = (intOffset + antIdx) * bbSeq.length;
                    for (int bbIdx = 0; bbIdx < bbSeq.length; bbIdx++) {
                        bbOffset = (antOffset + bbIdx) * polSeq.length;
                        for (int polIdx = 0; polIdx < polSeq.length; polIdx++){
                            if (predictedValue != 
                                    arrayData.tpData[bbOffset+polIdx]) {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        public boolean checkFlagValue() {
            for (int idx = 0; idx < arrayData.tpFlags.length; idx++) {
                if (arrayData.tpFlags[idx] != 0)
                    return true;
            }
            return false;
        }


    }

}

