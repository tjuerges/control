/**
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2005 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * $Id$
 */

package alma.Control.TotalPowerProcessorTest;


import java.net.ServerSocket;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import junit.framework.Test;
import junit.framework.TestSuite;
import alma.Control.TotalPowerProcessor.AntTPSubscanData;
import alma.Control.TotalPowerProcessor.IFProcClient;
import alma.ControlExceptions.DeviceBusyEx;
import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.time.TimeHelper;


/**
 * Test the Total Power Reciever
 */

public class IFProcClientTest extends ComponentClientTestCase {

	private Thread        clientThread;
    private IFProcClient  testClient;
    private int           clientPort;

    public IFProcClientTest(String name) throws Exception {
		super(name);
	}

    public IFProcClientTest() throws Exception {
		super(IFProcClientTest.class.getName());
	}    
    
    public static void main (String[] args) {
        junit.textui.TestRunner.run (suite());
    }

    public static Test suite() {
        return new TestSuite(IFProcClientTest.class);
    }


    /**
     * Test case fixture setup.
     * 
     * The following operations are performed:
     * <OL>
     *   <LI> Get a reference to the CONTROL/MASTER component.
     *   <LI> Get references to the ARCHIVE client components.
     * </OL>
     */
    protected void setUp() throws Exception {
        super.setUp();

        ServerSocket serverSocket = new ServerSocket(0);
        clientPort = serverSocket.getLocalPort();
        testClient = new IFProcClient("DV01", 0, serverSocket, getContainerServices().getLogger());
        clientThread = new Thread(testClient);
        clientThread.start();
    }

    /**
     * Test case fixture clean up.
     */
    protected void tearDown() throws Exception {
        testClient.close();
        //clientThread.interrupt();
        super.tearDown();
    }

    public void testSimpleLifecycle() throws Exception {
        assertNotNull(testClient);
        Thread.sleep(2000);
    }

    public void testSimpleConnection() throws Exception {
        IFProcMock testDevice = new IFProcMock(clientPort);
        Thread.sleep(2000);
        testDevice.close();
    }

    public void testWritingData() throws Exception {
        IFProcMock testDevice = new IFProcMock(clientPort);
        Thread testThread = new Thread(testDevice);
        testThread.start();
        Thread.sleep(2000);
        testDevice.close();
    }

    public void testInterruptData() throws Exception {
        IFProcMock testDevice = new IFProcMock(clientPort);
        Thread testThread = new Thread(testDevice);
        testThread.start();
        Thread.sleep(2000);
        testClient.close();
    }

    public void testSubscanInterface() throws Exception {
        IFProcMock testDevice = new IFProcMock(clientPort);
        Thread testThread = new Thread(testDevice);
        testThread.start();

        Semaphore doneSignal = new Semaphore(0);
        long beginTime = TimeHelper.getTimeStamp().value + 10000000;
        beginTime      -= (beginTime % 480000);
        long endTime   = beginTime + 60000000; // 6 Sec subscan

        testClient.beginSubscan(beginTime, endTime, doneSignal);

        boolean success = doneSignal.tryAcquire(1, 9, TimeUnit.SECONDS);
        assertTrue(success);
    }

    public void testDeviceBusyEx() throws Exception {
        IFProcMock testDevice = new IFProcMock(clientPort);
        Thread testThread = new Thread(testDevice);
        testThread.start();

        Semaphore doneSignal = new Semaphore(0);
        long beginTime = TimeHelper.getTimeStamp().value + 10000000;
        beginTime      -= (beginTime % 480000);
        long endTime   = beginTime + 60000000; // 6 Sec subscan

        testClient.beginSubscan(beginTime, endTime, doneSignal);

        try {
            testClient.beginSubscan(beginTime, endTime, new Semaphore(0));
        } catch (DeviceBusyEx ex) {
            /* This is good we should have caught an exception */
            boolean success = doneSignal.tryAcquire(1, 9, TimeUnit.SECONDS);
            assertTrue(success);
            return;
        }
        fail();
    }

    public void testAbortSubscan() throws Exception {
        IFProcMock testDevice = new IFProcMock(clientPort);
        Thread testThread = new Thread(testDevice);
        testThread.start();

        Semaphore firstSignal  = new Semaphore(0);
        Semaphore secondSignal = new Semaphore(0);
        long beginTime = TimeHelper.getTimeStamp().value+ 10000000;
        beginTime      -= (beginTime % 480000);
        long endTime   = beginTime + 60000000; // 6 Sec subscan

        testClient.beginSubscan(beginTime, endTime+40000000, firstSignal);
        testClient.abortSubscan();
        Thread.yield();
        testClient.beginSubscan(beginTime, endTime-40000000, secondSignal);

        boolean success = firstSignal.tryAcquire(1, 1, TimeUnit.SECONDS);
        success        &= secondSignal.tryAcquire(1, 4, TimeUnit.SECONDS);
        assertTrue(success);
    }

    public void testDataIntegrity() throws Exception {
        IFProcMock testDevice = new IFProcMock(clientPort);
        Thread testThread = new Thread(testDevice);
        testThread.start();

        Semaphore doneSignal  = new Semaphore(0);

        long beginTime = TimeHelper.getTimeStamp().value+ 10000000;
        beginTime      -= (beginTime % 480000);
        long endTime   = beginTime + 60000000; // 6 Sec subscan

        testClient.beginSubscan(beginTime, endTime, doneSignal);
        testDevice.beginSubscan(beginTime, endTime);

        boolean success = doneSignal.tryAcquire(1, 9, TimeUnit.SECONDS);
        assertTrue(success);

        /* Now get the data and ensure it has correct values */
        AntTPSubscanData subscanData = testClient.getSubscanData();
        assertEquals("Reported Antenna Name is not expected value\n" +
                "\tExpected: " + "DV01" + "\n" +
                "\tReturned: " + subscanData.antennaName,
                "DV01", subscanData.antennaName);

        assertEquals("Reported Polarization Number is not expected value\n" +
                "\tExpected: " + 0 + "\n" +
                "\tReturned: " + subscanData.polarizationNum,
                0, subscanData.polarizationNum);

        assertEquals("Reported Subscan Begin Time is not expected value\n" +
                "\tExpected: " + beginTime + "\n" +
                "\tReturned: " + subscanData.subscanBeginTime,
                beginTime, subscanData.subscanBeginTime);

        assertEquals("Reported Subscan End Time is not expected value\n" +
                "\tExpected: " + endTime + "\n" +
                "\tReturned: " + subscanData.subscanEndTime,
                endTime, subscanData.subscanEndTime);

        for (int idx = 0; idx < subscanData.dataFlags.length; idx++) 
            for (int bandID = 0; bandID < 4; bandID++)
                assertEquals("Error retrieved data is incorrect",
                        idx,subscanData.dataBuffer.get((4*idx) + bandID));
    }

    public void testBusyException() throws Exception {
        IFProcMock testDevice = new IFProcMock(clientPort);
        Thread testThread = new Thread(testDevice);
        testThread.start();

        Semaphore doneSignal  = new Semaphore(0);

        long beginTime = TimeHelper.getTimeStamp().value+ 10000000;
        beginTime      -= (beginTime % 480000);
        long endTime   = beginTime + 150000000; // 15 Sec subscan

        testClient.beginSubscan(beginTime, endTime, doneSignal);
        testDevice.beginSubscan(beginTime, endTime);

        /* This should fail */
        try {
            AntTPSubscanData subscanData = testClient.getSubscanData();
            fail("Device Busy Exception failed to be thrown");
        } catch (DeviceBusyEx ex) {
            /* This is ok exactly what is supposed to happen */
        }

        /* Now call abort */
        testClient.abortSubscan();

        /* This should work */
        AntTPSubscanData subscanData = testClient.getSubscanData();

    }
}

