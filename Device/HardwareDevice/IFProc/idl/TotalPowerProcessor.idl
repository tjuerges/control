#ifndef TotalPowerProcessor_IDL
#define TotalPowerProcessor_IDL
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2004 
*
*This library is free software; you can redistribute it and/or
*modify it under the terms of the GNU Lesser General Public
*License as published by the Free Software Foundation; either
*version 2.1 of the License, or (at your option) any later version.
*
*This library is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*Lesser General Public License for more details.
*
*You should have received a copy of the GNU Lesser General Public
*License along with this library; if not, write to the Free Software
*Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2004-11-10  created
*/

/************************************************************************
 *  This Component handles all of the data coming from TotalPowerDetectors
 * and is responable for formatting the data and sending it to Data Capture
 * In addition this component maintains the resources for both the IP address
 * space of the Total Power Digitizers and the Port usage on this end.
 *----------------------------------------------------------------------
 */

#include <acscommon.idl>           // for ACS::OffShoot since ACS 9.1
#include <acscomponent.idl>
#include <ControlAntennaInterfaces.idl>
#include <ControlDeviceExceptions.idl>
#include <TotalPowerProcessorExceptions.idl>

#pragma prefix "alma"

module Control {
  interface TPProcessor : ACS::OffShoot {
    

    typedef sequence < octet, 4 > IpAddress;
    typedef sequence < long > basebandSeq;
    typedef sequence < long > polarizationSeq;

    struct SubscanInfo {
      long long       startTime;
      long long       endTime;
      string          dataOID;
      string          execID;
      long            scanNumber;
      long            subscanNumber;
      basebandSeq     baseband;
      polarizationSeq polarization;
    };


    /**
       Method getIPAddress returns the IP address of the total power
       processor.  This allows the IFProcessor devices to determine
       where to connect.
    */
    IpAddress getIPAddress();
    
    /** 
	Method addDigitizer notifies the TPProcessor of an incoming 
	connection from a IFProcesor.  
	@param antennaName: The name of the antenna the IFProc 
	   is connected to.
	@param polarization: The polarization number of the IFPRoc
	@return returns the port number allocated to this IFProc.
	@note The allocated port is a one time use, if it is closed for
	any reason, the digitizer must again be added to the TPProcessor.
    */
    long addDigitizer(in string antennaName,
		      in long   polarization);
    
    boolean isConnected(in string antennaName,
		                in long polarization);


    void interruptClient(in string antennaName,
		                 in long polarization);


    /** 
	Method abortSubscan 
	Stops any running subscan, if no subscan is currently running it
	just returns.
	In the advent of something going very wrong a ControlException can
	be thrown.  In this case assume the Subscan was not aborted.
    */
    void abortSubscan()
      raises (ControlExceptions::ControlExceptionsEx);


    /**
       Method configureArray
       This specifies the antenna's which are expected to report data to
       this TPProcessor.  The order of the antennas is used to create the
       binary data file, and must match the Configuration information in
       the ASDM.
    */
    void configureArray(in Control::AntennaSeq antennaList);
    
    /**
       Method beginSubscan(in SubscanInfo info)
       This method initiates a subscan then returns.  If no
       antennas are allocated to the subarray a ANTErrorEx is thrown.
    */
    void beginSubscan(in SubscanInfo info)
      raises (ControlDeviceExceptions::IllegalConfigurationEx,
	      ControlExceptions::DeviceBusyEx);


    /**
       Method subscanComplete
       @param timeout. The number of seconds to wait for successful completion
       before returning.  
       @ Returns true when the subscan is complete or if no subscan is active.
                 false if the subscan did not end before the timeout (or the
		 pending thread was interrupted).
    */
    void subscanComplete(in long timeout)
      raises (TotalPowerProcessorExceptions::IFProcClientTimeOutEx,
              TotalPowerProcessorExceptions::ProcessingThreadInterruptedEx,
              TotalPowerProcessorExceptions::BulkSenderEx);
    /**
       Method setIntegrationDuration
       Method to sets the integration time for the data stored to the archive
       Sample integration times must be a multiple of 0.5ms as the underlying 
       hardware sample rate is 2 kHz. This number is rounded to the nearest value.
       @param sampleRate (double) the desired sample rate (in Hertz).
    */
    void setIntegrationDuration(in double integrationDuration);

    /** 
	Method getIntegrationDuration 
        Returns the current sample period in seconds. This will always
        be a multiple of 0.5 ms.
    */
    double getIntegrationDuration();

    void shutdown();

  };
}; 
#endif //TotalPowerDataClient_IDL
