/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Apr 17, 2007  created
*/

#include <IFProcImplMonitorThread.h>
#include <IFProcImpl.h>
#include <ambDefs.h>


IFProcImplMonitorThread::IFProcImplMonitorThread(const ACE_CString& _name,
        const IFProcImpl* _ifproc): 
    ACS::Thread(_name),
    wasInError(false),
    guard(10000000ULL * 10ULL, 0), // 10 seconds
    ifProcDevice_p(const_cast<IFProcImpl*>(_ifproc)){
        ACS_TRACE("IFProcImplMonitorThread::IFProcImplMonitorThread");
        timestamp = ACS::Time(0ULL);
    }

void IFProcImplMonitorThread::runLoop(){
    try{
        if(ifProcDevice_p->getTimingErrorFlag(timestamp) == true){
            if(guard.check() == true){
                ACS_STATIC_LOG(LM_SOURCE_INFO, __FUNCTION__, (LM_DEBUG,
                            "IFProcImplMonitorThread::runLoop: Device time does not "
                            "match. Setting it."));
            }
            //ntorncos 2010-10-14: Removed auto resyncing. Deemed dangerous.
            //                     If out of sync let people know and deal with it.
            //ifProcDevice_p->resyncIFProcTime();

                ifProcDevice_p->activateAlarm(TIMINGERRFLAG);
        }else{
            ifProcDevice_p->deactivateAlarm(TIMINGERRFLAG);
        }
    }
    catch(const ControlExceptions::INACTErrorExImpl&){
        /**
         * If this thread runs for whatever reasons too early although
         * I tried to take any precautions possible, well, just ignore
         * this exception.
         * In the case of a stopped component, well, this thread should
         * be suspended anyway.
         */
    }
    catch(ControlExceptions::CAMBErrorExImpl& ex){
        /**
         * I might happen, that the CAMBserver returns with AMBERR_TIMOUT.
         * In this case the code generated methods re-throw the exception.
         * Simply consume it. There is nothing I can do about it.
         *
         * In the case of a different AMBERR, log it.
         */
        if(guard.check() == true){
            int status(static_cast< int >(AMBERR_NOERR));
            ex.getMemberValue("ErrorCode", status);
            if(status != static_cast< int >(AMBERR_TIMEOUT)){
                ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                        "IFProcImplMonitorThread::run");
                nex.log(LM_ERROR);
            }
        }
    }
    catch(const ControlDeviceExceptions::IllegalConfigurationExImpl&){
        /**
         * Just ignore the exception. This one could be thrown only
         * when this thread had been started before the component is
         * operational. Very unlikely from the code in the Impl.cpp.
         */
    }
}
