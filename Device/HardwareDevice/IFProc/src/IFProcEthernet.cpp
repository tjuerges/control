//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Apr 10, 2007  created
//


#include <IFProcEthernet.h>
#include <ostream>
#include <IFProcImpl.h>
#include <ambDefs.h>
#include <loggingMACROS.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>


IFProcEthernet::IFProcEthernet(const IFProcImpl* _ifproc) :
    ifProcDevice_p(const_cast< IFProcImpl* > (_ifproc)), ifProcIpAddress(4, 0),
        tcpConnectionIsActive(false)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

void IFProcEthernet::init()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        antennaName = findAntennaName();
        polarization = findPolarization();
        ifProcIpAddress = findIpAddress(antennaName, polarization);
        netmask = findNetmask();
        gateway = findGateway();
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        throw ControlDeviceExceptions::IllegalConfigurationExImpl(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
    }
    catch(const ControlDeviceExceptions::IllegalConfigurationEx& ex)
    {
        throw ControlDeviceExceptions::IllegalConfigurationExImpl(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
    }
}

/**
 * Find the antenna name using the Component name convention
 */
//    throw(ControlDeviceExceptions::IllegalConfigurationExImpl)
const std::string IFProcEthernet::findAntennaName()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // Using the Control naming convention this component should be called
    // CONTROL/AAAA/IFProc{0,1}.  This should be changed for a cleaner
    // method that calls the parent and ask for the Antenna name.
    CORBA::String_var _myName(ifProcDevice_p->name());
    const std::string myName(_myName.in());
    const std::string antennaName(myName.begin() + 8, myName.begin() + 12);

    if(antennaName.empty() == true)
    {
        ControlDeviceExceptions::IllegalConfigurationExImpl ex(__FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", "Cannot determine the antenna name");
        throw ex;
    }

    std::ostringstream msg;
    msg << "Antenna name found: "
        << antennaName;
    LOG_TO_DEVELOPER(LM_DEBUG, msg.str());

    return antennaName;
}

/**
 * Find the polarization of this IFProc using the component name convention
 */
//    throw(ControlDeviceExceptions::IllegalConfigurationExImpl)
const CORBA::Long IFProcEthernet::findPolarization()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    CORBA::Long polarizationNumber(-1);
    CORBA::String_var _myName(ifProcDevice_p->name());
    const std::string myName(_myName.in());

    // Find the polarization digit. It is the last digit of the component name.
    const std::string polarizationString(myName.end() - 1, myName.end());
    std::istringstream polarization(polarizationString);
    polarization >> polarizationNumber;

    if(polarizationNumber < 0)
    {
        ControlDeviceExceptions::IllegalConfigurationExImpl ex(__FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", "Could not determine the polarization.");
        throw ex;
    }

    std::ostringstream msg;
    msg << "Polarization found: "
        << polarizationNumber;
    LOG_TO_DEVELOPER(LM_DEBUG, msg.str());

    return polarizationNumber;
}

/**
 * Find the IP address of the module asking the system for this hostname
 */
const std::vector< unsigned char > IFProcEthernet::findIpAddress(
    const std::string& antennaName, const CORBA::Long polarization) const
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    std::string hostName(antennaName);
    std::ostringstream polarizationPart;
    polarizationPart << std::string("-ifpr-");
    polarizationPart << polarization;
    hostName.append(polarizationPart.str());

    struct addrinfo* resolvedAddress;

    if(getaddrinfo(hostName.c_str(), 0, 0, &resolvedAddress))
    {
        ControlDeviceExceptions::IllegalConfigurationExImpl ex(__FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        std::string info("Could not resolve the IFProc name into an IP "
            "address using hostname ");
        info.append(hostName);
        ex.addData("Detail", info);
        ex.log();

        throw ex;
    }

    //Ok, got the IP-address. Convert it to a std::vector< unsigned char >.
    std::vector< unsigned char > ipAddress(4, 0);

    // In the copy step the "idx+2" term is because the sa_data is designed
    // to support the IPv6 protocol, all we need are the last 4 bytes.
    for(int idx(0); idx < 4; ++idx)
    {
        ipAddress[idx] = static_cast< unsigned char >(
            resolvedAddress->ai_addr->sa_data[idx + 2]);
    }

    freeaddrinfo(resolvedAddress);

    std::ostringstream msg;
    msg << "Module IP address found: "
        << ipAddress[0]
        << "."
        << ipAddress[1]
        << "."
        << ipAddress[2]
        << "."
        << ipAddress[3];
    LOG_TO_DEVELOPER(LM_DEBUG, msg.str());

    return ipAddress;
}

/**
 * Find the Netmask to be used.
 */
const std::vector< unsigned char > IFProcEthernet::findNetmask()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // This netmask is a class C, after the L3 networking project
    // on July 17, 2009
    std::vector< unsigned char > netmask(4, 0);
    netmask[0] = 255;
    netmask[1] = 255;
    netmask[2] = 255;
    netmask[3] = 0;

    std::ostringstream msg;
    msg << "Using Netmask: "
        << netmask[0]
        << "."
        << netmask[1]
        << "."
        << netmask[2]
        << "."
        << netmask[3];
    LOG_TO_DEVELOPER(LM_DEBUG, msg.str());


    return netmask;
}

/**
 * Find the gateway to be used
 */
const std::vector< unsigned char > IFProcEthernet::findGateway()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // The gateway should correspond to the one installed at the antenna.
    // This change was made during the L3 networking project on July 17, 2009
    // The gateway has always the xx.xx.xx.254 ip address on the C class network
    std::vector< unsigned char > gateway(4, 0U);
    for(int idx(0); idx < 3; ++idx)
    {
        gateway[idx] = ifProcIpAddress[idx];
    }

    gateway[3] = 254;

    std::ostringstream msg;
    msg << "Using Gateway: "
        << gateway[0]
        << "."
        << gateway[1]
        << "."
        << gateway[2]
        << "."
        << gateway[3];
    LOG_TO_DEVELOPER(LM_DEBUG, msg.str());

    return gateway;
}

/**
 * Set all the network parameters at the IFProc and initialize the TCP connection.
 */
void IFProcEthernet::establishTCPConnection(
    std::vector< unsigned char > destinationIpAddress,
    const CORBA::UShort portToUse)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(tcpConnectionIsActive)
    {
        ControlDeviceExceptions::IllegalConfigurationExImpl ex(__FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", "TCP connection is already active");
        throw ex;
    }

    try
    {
        // Stop the data transmition, if it was active
        ifProcDevice_p->setStopData();

        //Resetting the Ethernet interface
        ACS::Time timestamp(0ULL);
        std::vector< unsigned char > status(ifProcDevice_p->getStatus(
            timestamp));
        ifProcDevice_p->setResetTpdEthernet();
        status = ifProcDevice_p->getStatus(timestamp);
        int timeStep = 200; //  polling period
        // we poll for 7 sec waiting for the TPD finish to reboot
        for(int msElapsed = 0; msElapsed <= 7000; msElapsed += timeStep)
        {
            status = ifProcDevice_p->getStatus(timestamp);
            if(status[0] & 1)
            {
                std::ostringstream msg;
                msg << "ethernetConnected status set after "
                    << msElapsed
                    << " ms";
                LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
                break;
            }
            else
            {
                //timeSleep * 1e3 to convert from ms to us
                usleep(int(timeStep * 1e3));
            }
        }

        status = ifProcDevice_p->getStatus(timestamp);
        // If after 7 sec the ethernet conexion is not ready, we have a
        // problem!  So we raise an exception.
        if(!(status[0] & 1))
        {
            ControlDeviceExceptions::IllegalConfigurationExImpl ex(__FILE__,
                __LINE__, __PRETTY_FUNCTION__);
            ex.addData("Detail", "The IFProc TPD didn't finish its reboot "
                "after 7 sec");
            throw ex;
        }

        ifProcDevice_p->setCntlModuleGateway(gateway);
        ifProcDevice_p->setCntlModuleNetmask(netmask);
        ifProcDevice_p->setCntlModuleIpAddr(ifProcIpAddress);
        ifProcDevice_p->setCntlDestIpAddr(destinationIpAddress);
        ifProcDevice_p->setCntlTcpPort(portToUse);
        ifProcDevice_p->setInitTcpConn();
    }
    catch(const ControlExceptions::CAMBErrorEx& ex)
    {
        throw ControlDeviceExceptions::IllegalConfigurationExImpl(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        throw ControlDeviceExceptions::IllegalConfigurationExImpl(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
    }
    catch(...)
    {
        throw ControlDeviceExceptions::IllegalConfigurationExImpl(__FILE__,
            __LINE__, __PRETTY_FUNCTION__);
    }

    LOG_TO_DEVELOPER(LM_DEBUG, "TCP connection established.");
}

/**
 * Reset IFProc network to zero values.
 */
void IFProcEthernet::disconnectTCPConnection()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    std::vector< unsigned char > zeroIpAddress(4, 0U);
    try
    {
        tcpConnectionIsActive = false;
        // Disable all TCP-connections.
        ifProcDevice_p->setCntlModuleIpAddr(zeroIpAddress);
        ifProcDevice_p->setCntlDestIpAddr(zeroIpAddress);
        ifProcDevice_p->setCntlTcpPort(0U);
    }
    catch(const ControlExceptions::CAMBErrorEx& ex)
    {
        throw ControlDeviceExceptions::IllegalConfigurationExImpl(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        throw ControlDeviceExceptions::IllegalConfigurationExImpl(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
    }
    catch(...)
    {
        throw ControlDeviceExceptions::IllegalConfigurationExImpl(__FILE__,
            __LINE__, __PRETTY_FUNCTION__);
    }
}

