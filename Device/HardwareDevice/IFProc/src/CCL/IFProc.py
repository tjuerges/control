#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# tjuerges  Nov 15, 2007  created
#

"""
This module is part of the Control Command Language.
It contains complementary functionality for an IFProc (IFDC) device.
"""

import CCL.IFProcBase
from CCL import StatusHelper

from Acspy.Common.Log import getLogger
import ACSLog
from log_audience import OPERATOR


class IFProc(CCL.IFProcBase.IFProcBase):
    def __init__(self, antennaName = None, polarization = None, componentName = None, stickyFlag = False):

        '''
        The constructor creates an IFProc object by making use of
        the IFProcBase constructor.
        '''
        if componentName == None:
            if ((antennaName == None) or (polarization == None)):
                raise NameError, "missing component name or the antennaName and polarization tuple"
            if (isinstance(antennaName, list) == True):
                if (len(antennaName) > 0):
                    componentName=[]
                    for idx in range (0, len(antennaName)):
                        componentName.append("CONTROL/" + antennaName[idx] + "/IFProc" + str(polarization))
            else:
                componentName = "CONTROL/"+antennaName+"/IFProc"+str(polarization)

        CCL.IFProcBase.IFProcBase.__init__(self, None, componentName, stickyFlag)

        self.loggers = {}
        for key, val in self._devices.iteritems():
            self.loggers[key] = ""
            self.loggers[key] = getLogger(key.replace('/', '-') + "-audienceLogger")

    def __del__(self):
        CCL.IFProcBase.IFProcBase.__del__(self)

    def getTpData(self, duration):
        '''
        Get Total Power data for a duration in seconds.(between 0.048 and 30s)
        '''
        
        duration = duration * 10000000
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            logger = self.loggers[key]
            logger.logNotSoTypeSafe(ACSLog.ACS_LOG_INFO, key + \
                                     ": Retrieving total power data for a duration of " + \
                                     str(duration)+" seconds.", OPERATOR, antenna=antName)
            result[antName] = self._devices[key].getTpData(long(duration))
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def getSBPower(self):
        '''
        Get SB Total Power data in [dbm].
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].getSBPower()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def getBBPower(self):
        '''
        Get BB Total Power data in [dbm].
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].getBBPower()
        if len(self._devices) == 1:
            return result.values()[0]
        return result


    def setPowerLevel(self, level):
        '''
        Set the attenuations, by giving a desired power level.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            logger = self.loggers[key]
            logger.logNotSoTypeSafe(ACSLog.ACS_LOG_INFO, key + \
                                     ": Setting the power of the basebands to a level of " + \
                                     str(level)+" db.", OPERATOR, antenna=antName)
            result[antName] = self._devices[key].setPowerLevel(level)
        if len(self._devices) == 1:
            return  result.values()[0]
        return result

    def allocate(self, tprx):
        '''
        Allocate a TPRx-offshoot object.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            logger = self.loggers[key]
            logger.logNotSoTypeSafe(ACSLog.ACS_LOG_INFO, key + \
                                         ": Allocating the IFProcessor to achieve Total Power" +
                                         " transmition trough ethernet.", OPERATOR, antenna=antName)
            result[antName] = self._devices[key].allocate(tprx)
        if len(self._devices) == 1:
            return  result.values()[0]
        return result

    def deallocate(self):
        '''
        Free the via allocate controlled TPRx object.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            logger = self.loggers[key]
            logger.logNotSoTypeSafe(ACSLog.ACS_LOG_INFO, key + \
                                     ": Deallocating the IFProcessor.", OPERATOR, antenna=antName)
        result[antName] = self._devices[key].deallocate()
        if len(self._devices) == 1:
            return  result.values()[0]
        return result

    def beginDataAcquisition(self, timestamp):
        '''
        Start data acquisition.
        '''

        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            logger = self.loggers[key]
            logger.logNotSoTypeSafe(ACSLog.ACS_LOG_INFO, key + \
                                     ": Begin the data transmition through ethernet.",
                                     OPERATOR, antenna=antName)
            result[antName] = self._HardwareDevice__hw.beginDataAcquisition(timestamp)
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def abortDataAcquisition(self):
        '''
        Abort data acquisition.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            logger = self.loggers[key]
            logger.logNotSoTypeSafe(ACSLog.ACS_LOG_INFO, key + \
                                     ": Abort the data transmition through ethernet.",
                                     OPERATOR, antenna=antName)
            result[antName] = self._HardwareDevice__hw.abortDataAcquisition()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def GET_STATUS(self):
        '''
        GET_STATUS in hexadecimal format
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            output = self._HardwareDevice__hw.GET_STATUS()
            hexOutput = []
            for i in output[0]:
                hexOutput.append(hex(i))
            result[antName] = (hexOutput, output[1])
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def GET_SPI_ERRORS(self):
        '''
        GET_SPI_ERRORS
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            output = self._HardwareDevice__hw.GET_SPI_ERRORS()
            addr = output[0][0]<<16 | output[0][1]<<8 | output[0][2]
            count = output[0][3]<<8 | output[0][4]
            result[antName] = ([addr, count], output[1])
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def GET_TPD_MODULE_CODES(self):
        '''
        GET_TPD_MODULE_CODES in a pretty print mode
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]

            value = self._HardwareDevice__hw.GET_TPD_MODULE_CODES()
            ppFormat = []
            ppFormat.append( "%.2x"%value[0][0] )
            ppFormat.append( "%.2x"%value[0][1] )
            ppFormat.append( "Rev:"+str(value[0][2]>>4)+'.'+str(((value[0][2]<<4)&0xFF)>>4) )#Revision
            ppFormat.append( '20'+"%.2d"%value[0][5]+'-'+"%.2d"%value[0][4]+'-'+"%.2d"%value[0][3] )
            ppFormat.append( "SN:"+str(value[0][6]<<8 | value[0][7]) )
            result[antName] = (ppFormat, value[1])
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def GET_MODULE_CODES(self):
        '''
        GET_MODULE_CODES in a pretty print mode
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]

            value = self._HardwareDevice__hw.GET_MODULE_CODES()
            ppFormat = []
            ppFormat.append( "%.2x"%value[0][0] )
            ppFormat.append( "%.2x"%value[0][1] )
            ppFormat.append( "Rev:"+str(value[0][2]>>4)+'.'+str(((value[0][2]<<4)&0xFF)>>4) )#Revision
            ppFormat.append( '20'+"%.2d"%value[0][5]+'-'+"%.2d"%value[0][4]+'-'+"%.2d"%value[0][3] )
            ppFormat.append( "SN:"+str(value[0][6]<<8 | value[0][7]) )
            result[antName] = (ppFormat, value[1])
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def STATUS(self):
        '''
        This method print the Status of the IFProc
        '''
        result = {}
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[2]

            # List of status elements
            elements = []
    

            # Device Status
            try:
                value = self._devices[key].GET_STATUS()[0];
            except:
                value = -1;
                tmp = "N/A"
           
            # Ethernet Status
            if value!=-1:
              if (value[0] & 1): tmp = "connected" 
              else:  tmp = "disconnected"
            eth_conn_l     = StatusHelper.Line("Ethernet", [StatusHelper.ValueUnit(tmp)]);
    
            if value!=-1:
              if (value[0] & 2): tmp = "Not found"
              else: tmp = "found"
            no_router_l    = StatusHelper.Line("Router", [StatusHelper.ValueUnit(tmp)]);
    
            if value!=-1:
              if (value[0] & 4): tmp = "yes"
              else: tmp = "no"
            tcp_conn_l     = StatusHelper.Line("TCP connected", [StatusHelper.ValueUnit(tmp)]);
    
            if value!=-1:
              if (value[0] & 8): tmp = "yes"
              else: tmp = "no"
            arp_full_l     = StatusHelper.Line("ARP table full", [StatusHelper.ValueUnit(tmp)]);
    
            if value!=-1:
              if (value[0] & 16): tmp = "active"
              else: tmp = "inactive"
            trans_act_l    = StatusHelper.Line("Data Transfer", [StatusHelper.ValueUnit(tmp)]);
    
            if value!=-1:
              if (value[0] & 32): tmp = "yes"
              else: tmp = "no"
            tcp_disc_cmd_l = StatusHelper.Line("TCP disconn. by cmd", [StatusHelper.ValueUnit(tmp)]);
    
            if value!=-1:
              if (value[0] & 64): tmp = "yes"
              else: tmp = "no"
            tcp_disc_err_l = StatusHelper.Line("TCP disconn. by err", [StatusHelper.ValueUnit(tmp)]);
    
            if value!=-1:
              if (value[0] & 128): tmp = "yes, lost data"
              else: tmp = "no"
            fifo_full_l    = StatusHelper.Line("FIFO full", [StatusHelper.ValueUnit(tmp)]); 
    
            #eth_status_g = StatusHelper.Group("Status flags",[eth_conn_l, no_router_l, tcp_conn_l, arp_full_l, trans_act_l, tcp_disc_cmd_l, tcp_disc_err_l, fifo_full_l])
    
            # Voltages Status
            voltages = []
            if value!=-1:
              if (value[1] & 1): tmp = "ok"
              else:  tmp = "ERR"
            voltages.append( StatusHelper.ValueUnit(tmp, label="1.2"));
    
            if value!=-1:
              if (value[1] & 2): tmp = "ok"
              else: tmp = "ERR"
            voltages.append( StatusHelper.ValueUnit(tmp, label=" 2.5"));
    
            if value!=-1:
              if (value[1] & 4): tmp = "ok"
              else: tmp = "ERR"
            voltages.append( StatusHelper.ValueUnit(tmp, label=" 3.3"));
    
            if value!=-1:
              if (value[1] & 8): tmp = "ok"
              else: tmp = "ERR"
            voltages.append( StatusHelper.ValueUnit(tmp, label=" 5"));
    
            if value!=-1:
              if (value[1] & 16): tmp = "ok"
              else: tmp = "ERR"
            voltages.append( StatusHelper.ValueUnit(tmp, label=" eth"));
    
            if value!=-1:
              if (value[1] & 32): tmp = "ok"
              else: tmp = "ERR"
            voltages.append( StatusHelper.ValueUnit(tmp, label=" analog 5"));

    
            # Vref Status
            vreferences = []
            if value!=-1:
              if (value[2] & 1): tmp = "ok"
              else:  tmp = "ERR"
            vreferences.append( StatusHelper.ValueUnit(tmp, label="USB"));
    
            if value!=-1:
              if (value[2] & 2): tmp = "ok"
              else: tmp = "ERR"
            vreferences.append( StatusHelper.ValueUnit(tmp, label=" LSB"));
    
            if value!=-1:
              if (value[2] & 4): tmp = "ok"
              else: tmp = "ERR"
            vreferences.append( StatusHelper.ValueUnit(tmp, label=" A"));
    
            if value!=-1:
              if (value[2] & 8): tmp = "ok"
              else: tmp = "ERR"
            vreferences.append( StatusHelper.ValueUnit(tmp, label=" B"));
    
            if value!=-1:
              if (value[2] & 16): tmp = "ok"
              else: tmp = "ERR"
            vreferences.append( StatusHelper.ValueUnit(tmp, label=" C"));
    
            if value!=-1:
              if (value[2] & 32): tmp = "ok"
              else: tmp = "ERR"
            vreferences.append( StatusHelper.ValueUnit(tmp, label=" D"));
    
            status_vref_l = StatusHelper.Line("Vref present", vreferences);



            ########################################## Signal ######################################################
            elements.append( StatusHelper.Separator("Signal") )

            # DATA_MONITOR_1
            try:
                value = self._devices[key].GET_DATA_MONITOR_1()[0];
                value = ["%+.3f"%value[0], "%+.3f"%value[1]];
            except:
                value = ["N/A"]*2;
            elements.append( StatusHelper.Line("Data Monitor 1" , [ StatusHelper.ValueUnit(value[0], "V", "USB"),
                                                                    StatusHelper.ValueUnit(value[1], "V", "LSB")]) )
            # DATA_MONITOR_1 in power
            try:
                value = self._devices[key].getSBPower()[0];
                value = ["%+.3f"%value[0], "%+.3f"%value[1]];
            except:
                value = ["N/A"]*2;
            elements.append( StatusHelper.Line("Data Monitor 1" , [ StatusHelper.ValueUnit(value[0], "dbm", "USB"),
                                                                    StatusHelper.ValueUnit(value[1], "dbm", "LSB")]) )


            # DATA_MONITOR_2
            try:
                value = self._devices[key].GET_DATA_MONITOR_2()[0];
                value = ["%+.3f"%value[0], "%+.3f"%value[1], "%+.3f"%value[2], "%+.3f"%value[3]];
            except:
                value = ["N/A"]*4;
            elements.append( StatusHelper.Line("Data Monitor 2" , [ StatusHelper.ValueUnit(value[0], "V", "A"),
                                                                    StatusHelper.ValueUnit(value[1], "V", "B"),
                                                                    StatusHelper.ValueUnit(value[2], "V", "C"),
                                                                    StatusHelper.ValueUnit(value[3], "V", "D")]) ) 
            # DATA_MONITOR_2 in Power
            try:
                value = self._devices[key].getBBPower()[0];
                value = ["%+.3f"%value[0], "%+.3f"%value[1], "%+.3f"%value[2], "%+.3f"%value[3]];
            except:
                value = ["N/A"]*4;
            elements.append( StatusHelper.Line("Data Monitor 2" , [ StatusHelper.ValueUnit(value[0], "dbm", "A"),
                                                                    StatusHelper.ValueUnit(value[1], "dbm", "B"),
                                                                    StatusHelper.ValueUnit(value[2], "dbm", "C"),
                                                                    StatusHelper.ValueUnit(value[3], "dbm", "D")]) ) 


            # GAINS
            try:
                value = self._devices[key].GET_GAINS()[0]
                value = ["%+.1f"%value[0], "%+.1f"%value[1], "%+.1f"%value[2], "%+.1f"%value[3]];
            except:
                value = ["N/A"]*6;
            elements.append( StatusHelper.Line("Gains (Att.)", [ StatusHelper.ValueUnit(value[0], label="A"),
                                                                 StatusHelper.ValueUnit(value[1], label=" B"),
                                                                 StatusHelper.ValueUnit(value[2], label=" C"),
                                                                 StatusHelper.ValueUnit(value[3], label=" D") ]) )


            # SIGNAL_PATHS
            try:
                value = self._devices[key].GET_SIGNAL_PATHS()[0]
                value = [value[0]*1, value[1]*1, value[2]*1, value[3]*1, value[4]*1, value[5]*1];
            except:
                value = ["N/A"]*6;
            elements.append( StatusHelper.Line("Signal Paths",
                [ StatusHelper.ValueUnit("LOW" if value[0] == 0 else ( "HIGH" if value[0] == 1 else value[0]), label="A"),
                  StatusHelper.ValueUnit("LOW" if value[1] == 0 else ( "HIGH" if value[1] == 1 else value[0]), label=" B"),
                  StatusHelper.ValueUnit("LOW" if value[2] == 0 else ( "HIGH" if value[2] == 1 else value[0]), label=" C"),
                  StatusHelper.ValueUnit("LOW" if value[3] == 0 else ( "HIGH" if value[3] == 1 else value[0]), label=" D"),
                  StatusHelper.ValueUnit("LSB" if value[4] == 0 else ( "USB"  if value[4] == 1 else value[0]), label=" AB"),
                  StatusHelper.ValueUnit("LSB" if value[5] == 0 else ( "USB"  if value[5] == 1 else value[0]), label=" CD") ]) )


            # DATA_REMAP
            try:
                value = self._devices[key].GET_DATA_REMAP()[0];
            except:
                value = ["N/A"]*6;
            elements.append( StatusHelper.Line("Data Remap" , [ StatusHelper.ValueUnit(value[0], label="USB"),
                                                                StatusHelper.ValueUnit(value[1], label=" LSB"),
                                                                StatusHelper.ValueUnit(value[2], label=" A"),
                                                                StatusHelper.ValueUnit(value[3], label=" B"),
                                                                StatusHelper.ValueUnit(value[4], label=" C"),
                                                                StatusHelper.ValueUnit(value[5], label=" D")]) )



            # DATA_AVE_LENGTH
            try:
                value = self._devices[key].GET_DATA_AVE_LENGTH()[0]
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Data avg. length", [StatusHelper.ValueUnit(value)]) )



            ############################################ Ref Signal ###############################################
            elements.append( StatusHelper.Separator("Reference Signals") )
            
            # LENGTH_48MS
            try:
                value = self._devices[key].GET_LENGTH_48MS()[0]
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("48ms length", [StatusHelper.ValueUnit(value)]) )


            # TIMING_ERROR_FLAG
            try:
                value = self._devices[key].GET_TIMING_ERROR_FLAG()[0];
            except:
                value = "N/A";

            if value!="N/A":
              if (value & 1): tmp = "DETECTED" 
              else: tmp = "ok"
            elements.append( StatusHelper.Line("Timing Error Flag", [StatusHelper.ValueUnit(value)]) )


            # ALMA_TIME
            try:
                value = self._devices[key].GET_ALMA_TIME()[0]
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Alma Time", [StatusHelper.ValueUnit(value)]) )

            ##################################### Operative data ##################################################
#            elements.append( StatusHelper.Separator("Operative Data") )
            sep = StatusHelper.Separator("Operative Data")
            sep.delimiter="="
            elements.append( sep )

            # Voltages status
            elements.append( StatusHelper.Line("Voltages", voltages))

            # Vref Status
            elements.append(status_vref_l)

            # VOLTAGES_1
            try:
                value = self._devices[key].GET_VOLTAGES_1()[0];
                value = ["%+.3f"%value[0], "%+.3f"%value[1], "%+.3f"%value[2], "%+.3f"%value[3]];
            except:
                value = ["N/A"]*4;

            voltages1_l  = StatusHelper.Line("Voltages1", [ StatusHelper.ValueUnit(value[0], label="1.2"), \
                                                            StatusHelper.ValueUnit(value[1], label=" 2.5"),\
                                                            StatusHelper.ValueUnit(value[2], label=" 3.3"),\
                                                            StatusHelper.ValueUnit(value[3], label=" eth")])
            elements.append( voltages1_l )


            # VOLTAGES_2
            try:
                value = self._devices[key].GET_VOLTAGES_2()[0];
                value = ["%+.3f"%value[0], "%+.3f"%value[1], "%+.3f"%value[2], "%+.3f"%value[3]];
            except:
                value = ["N/A"]*4;
    
            voltages2_l  = StatusHelper.Line("Voltages2", [ StatusHelper.ValueUnit(value[0], label="d_5"),
                                                            StatusHelper.ValueUnit(value[1], label=" a_5"),
                                                            StatusHelper.ValueUnit(value[2], label=" ref_5"),
                                                            StatusHelper.ValueUnit(value[3], label=" 7")])
            elements.append( voltages2_l )


            # 10V CURRENTS
            try:
                value = self._devices[key].GET_CURRENTS_10V()[0]
                value = ["%+.3f"%value[0], "%+.3f"%value[1], "%+.3f"%value[2], "%+.3f"%value[3]];
            except:
                value = ["N/A"]*4;
            currents_10v_l = StatusHelper.Line("10V Currents [A]", [ StatusHelper.ValueUnit(value[0], label="1"),
                                                                     StatusHelper.ValueUnit(value[1], label=" 2"),
                                                                     StatusHelper.ValueUnit(value[2], label=" M1"),
                                                                     StatusHelper.ValueUnit(value[3], label=" M2")])
            elements.append( currents_10v_l )


            # 6.5V CURRENTS
            try:
                value = self._devices[key].GET_CURRENTS_6_5V()[0]
                value = ["%+.3f"%value[0], "%+.3f"%value[1], "%+.3f"%value[2], "%+.3f"%value[3]];
            except:
                value = ["N/A"]*4;
            currents_6_5v_l = StatusHelper.Line("6.5V Currents [A]", [ StatusHelper.ValueUnit(value[0], label="A"),
                                                                       StatusHelper.ValueUnit(value[1], label=" B"),
                                                                       StatusHelper.ValueUnit(value[2], label=" C"),
                                                                       StatusHelper.ValueUnit(value[3], label=" D")])
            elements.append( currents_6_5v_l )


            # 8V CURRENTS
            try:
                value = self._devices[key].GET_CURRENTS_8V()[0]
                value = ["%+.3f"%value[0], "%+.3f"%value[1], "%+.3f"%value[2], "%+.3f"%value[3]];
            except:
                value = ["N/A"]*4;
            currents_8v_l = StatusHelper.Line("8V Currents [A]", [ StatusHelper.ValueUnit(value[0], label="A"),
                                                                   StatusHelper.ValueUnit(value[1], label=" B"),
                                                                   StatusHelper.ValueUnit(value[2], label=" C"),
                                                                   StatusHelper.ValueUnit(value[3], label=" D")])
            elements.append( currents_8v_l )


            # TEMPS_1
            try:
                value = self._devices[key].GET_TEMPS_1()[0]
                value = ["%+.3f"%value[0], "%+.3f"%value[1], "%+.3f"%value[2], "%+.3f"%value[3]];
            except:
                value = ["N/A"]*4;
            temps1_l = StatusHelper.Line("Temps 1 [C]", [ StatusHelper.ValueUnit(value[0], label="1"),
                                                          StatusHelper.ValueUnit(value[1], label=" 2"),
                                                          StatusHelper.ValueUnit(value[2], label=" A"),
                                                          StatusHelper.ValueUnit(value[3], label=" B")])
            elements.append( temps1_l )


            # TEMPS_2
            try:
                value = self._devices[key].GET_TEMPS_2()[0]
                value = ["%+.3f"%value[0], "%+.3f"%value[1], "%+.3f"%value[2], "%+.3f"%value[3]];
            except:
                value = ["N/A"]*4;
            temp2_l = StatusHelper.Line("Temps 2 [C]", [ StatusHelper.ValueUnit(value[0], label="C"),
                                                         StatusHelper.ValueUnit(value[1], label=" D"),
                                                         StatusHelper.ValueUnit(value[2], label=" M1"),
                                                         StatusHelper.ValueUnit(value[3], label=" M2")])
            elements.append( temp2_l )


            # TEMPS_3
            try:
                value = self._devices[key].GET_TEMPS_3()[0]
                value = "%+.3f"%value;
            except:
                value = "N/A";
            tempComm_l = StatusHelper.Line("Comm. Module temp.", [StatusHelper.ValueUnit(value, "C")])
            elements.append( tempComm_l )


            ######################################### Ethernet #####################################################
            elements.append( StatusHelper.Separator("Ethernet Connection") )
    
            # Ethernet Status
            elements.append( eth_conn_l ) 
            elements.append( no_router_l )
            elements.append( tcp_conn_l )
            elements.append( arp_full_l )
            elements.append( trans_act_l )
            elements.append( tcp_disc_cmd_l ) 
            elements.append( tcp_disc_err_l )
            elements.append( fifo_full_l )

            # FIFO_DEPTHS
            try:
                value = self._devices[key].GET_FIFO_DEPTHS()[0];
            except:
                value = ["N/A"]*2;
            elements.append( StatusHelper.Line("Fifo Depths" , [ StatusHelper.ValueUnit(value[0], label="FPGA"),
                                                                 StatusHelper.ValueUnit(value[1], label="Ext.")]) )



            # MODULE_IP_ADDR
            try:
                value = self._devices[key].GET_MODULE_IP_ADDR()[0]
                value = str(value[0])+'.'+str(value[1])+'.'+str(value[2])+'.'+str(value[3])
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Module IP Addr.", [StatusHelper.ValueUnit(value)]) )


            # DEST_IP_ADDR
            try:
                value = self._devices[key].GET_DEST_IP_ADDR()[0]
                value = str(value[0])+'.'+str(value[1])+'.'+str(value[2])+'.'+str(value[3])
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Destination IP Addr.", [StatusHelper.ValueUnit(value)]) )


            # GATEWAY_IP_ADDR
            try:
                value = self._devices[key].GET_MODULE_GATEWAY()[0]
                value = str(value[0])+'.'+str(value[1])+'.'+str(value[2])+'.'+str(value[3])
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Gateway IP Addr.", [StatusHelper.ValueUnit(value)]) )


            # NETMASK
            try:
                value = self._devices[key].GET_MODULE_NETMASK()[0]
                value = str(value[0])+'.'+str(value[1])+'.'+str(value[2])+'.'+str(value[3])
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Module Netmask", [StatusHelper.ValueUnit(value)]) ) 


            # ETHERNET_MAC
            try:
                value = self._devices[key].GET_ETHERNET_MAC()[0]
                value = "%.2x"%value[0]+':'+"%.2x"%value[1]+':'+"%.2x"%value[2]+':'+ \
                        "%.2x"%value[3]+':'+"%.2x"%value[4]+':'+"%.2x"%value[5]
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Module MAC Addr.", [StatusHelper.ValueUnit(value)]) )


            # TCP_PORT
            try:
                value = self._devices[key].GET_TCP_PORT()[0]
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("TCP port", [StatusHelper.ValueUnit(value)]) )


            # BDB_PER_PACKET
            try:
                value = self._devices[key].GET_BDB_PER_PACKET()[0]
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("BDB per Packet", [StatusHelper.ValueUnit(value)]) )


            # MAX_ETHERNET_TIMES
            try:
                value = self._devices[key].GET_MAX_ETHERNET_TIMES()[0];
                value = ["%+.3f"%value[0], "%+.3f"%value[1]];
            except:
                value = ["N/A"]*2;
            elements.append( StatusHelper.Line("Max. Eth times", [ StatusHelper.ValueUnit(value[0]), \
                                                                 StatusHelper.ValueUnit(value[1])]) )


            ######################################### CAN & AMB ###############################################
            elements.append( StatusHelper.Separator("CAN & AMB") )

            # AMB_CMD_COUNTER
            try:
                value = self._devices[key].GET_AMB_CMD_COUNTER()[0];
            except:
                value = ["N/A"];

            elements.append( StatusHelper.Line("AMB Cmd counter", [ StatusHelper.ValueUnit(value) ]) )


            # AMB_INVALID_CMD
            try:
                value = self._devices[key].GET_AMB_INVALID_CMD()[0];
                value[2] = (value[2]<<8) | value[3]
            except:
                value = ["N/A"]*3;

            elements.append( StatusHelper.Line("AMB Invalid Cmd.", [ StatusHelper.ValueUnit(value[0], label="RCA"),
                                                                     StatusHelper.ValueUnit(value[1], label=" cmd"),
                                                                     StatusHelper.ValueUnit(value[2], label=" num") ]))


            # IFDC_SPI_STATUS
            try:
                value = self._devices[key].GET_IFDC_SPI_STATUS()[0];
            except:
                value = ["N/A"]*2;
            elements.append( StatusHelper.Line("SPI Status", [ StatusHelper.ValueUnit(value[0]), \
                                                             StatusHelper.ValueUnit(value[1])]) )


            # SPI_ERRORS
            try:
                value = self._devices[key].GET_SPI_ERRORS()[0];
                value[0] = value[0]<<16 | value[1]<<8 | value[2]
                value[1] = value[3]<<8 | value[4]
            except:
                value = ["N/A"]*2;

            elements.append( StatusHelper.Line("SPI errors", [ StatusHelper.ValueUnit(value[0], label="Addr."),
                                                               StatusHelper.ValueUnit(value[1], label=" Count") ]) )


            ######################################### FIRMWARE  ###############################################
            elements.append( StatusHelper.Separator("Firmware") )
             
            # TPD_MODULE_CODES
            try:
                value = self._devices[key].GET_TPD_MODULE_CODES()[0];
                value[0] = "%.2x"%value[0]
                value[1] = "%.2x"%value[1]
                value[2] = str(value[2]>>4)+'.'+str(((value[2]<<4)&0xFF)>>4) #Revision
                value[3] = '20'+"%.2d"%value[5]+'-'+"%.2d"%value[4]+'-'+"%.2d"%value[3]
                value[4] = value[6]<<8 | value[7]
            except:
                value = ["N/A"]*5;

            elements.append( StatusHelper.Line("TPD module codes", [ StatusHelper.ValueUnit(value[0], label="code"),
                                                                     StatusHelper.ValueUnit(value[1], label=" IFMC"),
                                                                     StatusHelper.ValueUnit(value[2], label=" rev"),
                                                                     StatusHelper.ValueUnit(value[3], label=" date"),
                                                                     StatusHelper.ValueUnit(value[4], label=" SN") ]) )


            # IFDC_MODULE_CODES
            try:
                value = self._devices[key].GET_MODULE_CODES()[0];
                value[0] = "%.2x"%value[0]
                value[1] = "%.2x"%value[1]
                value[2] = str(value[2]>>4)+'.'+str(((value[2]<<4)&0xFF)>>4) #Revision
                value[3] = '20'+"%.2d"%value[5]+'-'+"%.2d"%value[4]+'-'+"%.2d"%value[3]
                value[4] = value[6]<<8 | value[7]
            except:
                value = ["N/A"]*5;

            elements.append( StatusHelper.Line("Module Codes", [ StatusHelper.ValueUnit(value[0], label="code"),
                                                                 StatusHelper.ValueUnit(value[1], label=" IFMC"),
                                                                 StatusHelper.ValueUnit(value[2], label=" rev"),
                                                                 StatusHelper.ValueUnit(value[3], label=" date"),
                                                                 StatusHelper.ValueUnit(value[4], label=" SN") ]) )

                        
            statusFrame = StatusHelper.Frame(compName + "   Ant: " + antName, elements)
            statusFrame.printScreen()
