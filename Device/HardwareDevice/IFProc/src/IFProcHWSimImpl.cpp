/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File IFProcHWSimImpl.cpp
 *
 * $Id$
 */

#include "IFProcHWSimImpl.h"

using namespace AMB;

/* Please use this class to implement complex functionality for the
 * IFProcHWSimBase helper functions. Use AMB::TypeConvertion methods
 * for convenient type convertions. */



IFProcHWSimImpl::IFProcHWSimImpl(node_t node, 
				 const std::vector<CAN::byte_t>& serialNumber, 
				 int antenna,
				 SHARED_SIMULATOR::SharedSimulator_var ss)
  : IFProcHWSimBase::IFProcHWSimBase(node, serialNumber), 
    m_try_initialize(false),
    m_try_start_data(false),
    m_try_stop_data(false),
    ssi_m(NULL)
{
  if (node == 0x0)
    polarization_m = 0;
  else
    polarization_m = 1;
  
  ss_m = ss;
  if(!CORBA::is_nil(ss.in())){
    cout << "Using Shared simulator for the IFProc..." << std::endl;    
    ssi_m = new SharedSimWrapper(antenna, ss_m);
  }


}

IFProcHWSimImpl::~IFProcHWSimImpl()
{
    // suspend the thread 
    m_ethernetDataThread_p->suspend();


    if (ssi_m) delete ssi_m;
}

void IFProcHWSimImpl::finalize() 
{
    m_ethernetDataThread_p->suspend();
    m_ethernetDataThread_p->stop();
}

void IFProcHWSimImpl::initialize(node_t node, const std::vector<CAN::byte_t>& serialNumber,
                                 maci::ContainerServices* _pCS){

   //Set the default initialization first
   IFProcHWSimBase::initialize(node, serialNumber);
   m_pCS = _pCS;

   std::vector<CAN::byte_t> macAddr(6);

   if (serialNumber[7] == 0x12){
       // Set Ethernet MAC
       macAddr[0] = 0xAA;
       macAddr[1] = 0x11;
       macAddr[2] = 0x22;
       macAddr[3] = 0x22;
       macAddr[4] = 0x33;
       macAddr[5] = 0x44;
          
   } else if (serialNumber[7] == 0x12){
       macAddr[0] = 0xBB;
       macAddr[1] = 0x11;
       macAddr[2] = 0x22;
       macAddr[3] = 0x22;
       macAddr[4] = 0x33;
       macAddr[5] = 0x44;
       
   }
   
   IFProcHWSimBase::setMonitorEthernetMac(macAddr); 
   IFProcHWSimImpl::resetAll();
   ACE_CString tname("EthernetDataThread");
   tname+=m_pCS->getName();
   m_ethernetDataThread_p = m_pCS->getThreadManager()->
       create<EthernetDataThread, IFProcHWSimImpl* const>(tname.c_str(), this);
   m_ethernetDataThread_p->resume();

}

/**
 *--------------------------------------------
 * Monitor point needed for Shared Simulator 
 *--------------------------------------------
 */

std::vector<CAN::byte_t> IFProcHWSimImpl::getMonitorGains() const{
  if (ssi_m !=NULL && ssi_m->haveSimulatorRef()) {
    return ssi_m->getGains(polarization_m);
  } else {
    return IFProcHWSimBase::getMonitorGains();
  }
}

/**
 *-----------------------------------------------------------------------------------------
 *  Control points linked to a monitor point value
 *-----------------------------------------------------------------------------------------
 */

// Set signal paths
void IFProcHWSimImpl::setControlSetSignalPaths(const std::vector<CAN::byte_t>& data){
  if (ssi_m!=NULL && ssi_m->haveSimulatorRef()) {
    ssi_m->setSignalPaths(polarization_m, data);
  } else {
    IFProcHWSimBase::setControlSetSignalPaths(data);
    IFProcHWSimBase::setMonitorSignalPaths(data);
  }
}

// Set GAINS
void IFProcHWSimImpl::setControlSetGains(const std::vector<CAN::byte_t>& data){
   if (ssi_m!=NULL && ssi_m->haveSimulatorRef()) {
     ssi_m->setGains(polarization_m, data);
   } else {
     IFProcHWSimBase::setControlSetGains(data);
     IFProcHWSimBase::setMonitorGains(data);
   }
}

// Set data remap
void IFProcHWSimImpl::setControlSetDataRemap(const std::vector<CAN::byte_t>& data){
    IFProcHWSimBase::setControlSetDataRemap(data);
    IFProcHWSimBase::setMonitorDataRemap(data);
}

// Set average length
void IFProcHWSimImpl::setControlSetAverageLength(const std::vector<CAN::byte_t>& data){
    IFProcHWSimBase::setControlSetAverageLength(data);
    IFProcHWSimBase::setMonitorDataAveLength(data);
}

// Set BDB per packet
void IFProcHWSimImpl::setControlSetBdbPerPacket(const std::vector<CAN::byte_t>& data){
    IFProcHWSimBase::setControlSetBdbPerPacket(data);
    IFProcHWSimBase::setMonitorBdbPerPacket(data);
}

// Set ALMA time
void IFProcHWSimImpl::setControlSetAlmaTime(const std::vector<CAN::byte_t>& data){
    IFProcHWSimBase::setControlSetAlmaTime(data);
    // It is not necessary to write on the monitor point, because it will be computed
    // everytime the Alma time is requested.

    // TODO:Compute the offset between the commanded time and the actual time, and store it.
    timeOffset = 0;
}

// In the case of the network paraameters, they are just pre-set

// Set module's IP address
void IFProcHWSimImpl::setControlSetModuleIpAddr(const std::vector<CAN::byte_t>& data){
    IFProcHWSimBase::setControlSetModuleIpAddr(data);
}

// Set dest. IP address
void IFProcHWSimImpl::setControlSetDestIpAddr(const std::vector<CAN::byte_t>& data){
    IFProcHWSimBase::setControlSetDestIpAddr(data);
}

// Set TCP port
void IFProcHWSimImpl::setControlSetTcpPort(const std::vector<CAN::byte_t>& data){
    IFProcHWSimBase::setControlSetTcpPort(data);
}

// Set module's GATEWAY
void IFProcHWSimImpl::setControlSetModuleGateway(const std::vector<CAN::byte_t>& data){
    IFProcHWSimBase::setControlSetModuleGateway(data);
}

// Set module's Netmask
void IFProcHWSimImpl::setControlSetModuleNetmask(const std::vector<CAN::byte_t>& data){
    IFProcHWSimBase::setControlSetModuleNetmask(data);
}

void IFProcHWSimImpl::setControlClearTimingError(const std::vector<CAN::byte_t>& data){
    IFProcHWSimBase::setControlClearTimingError(data);

    // Clearing the value
    std::vector<CAN::byte_t> tmp(1,0);
    IFProcHWSimBase::setMonitorTimingErrorFlag(tmp);
}

/**
 *-----------------------------------------------------------------------------------------
 *       RESET TPD BOARD and ETHERNET
 *-----------------------------------------------------------------------------------------
 */
void IFProcHWSimImpl::setControlResetTpdBoard(const std::vector<CAN::byte_t>& data){

    IFProcHWSimBase::setControlResetTpdBoard(data);

    // TPD Board RESET
    if (data[0] == 1){
        cout << "IFProc: Reseting FPGA and Ethernet..."<< endl;
        IFProcHWSimImpl::resetAll();
    } 
    // FIFO's and Ethernet reset
    else if (data[0] == 2){
        cout << "IFProc: Reseting ethernet..."<< endl;
        IFProcHWSimImpl::setResetTpdEthernet();
    }
}


void IFProcHWSimImpl::resetAll(){

    // set timing error to 1
    std::vector<CAN::byte_t> tmp(1,1);
    IFProcHWSimBase::setMonitorTimingErrorFlag(tmp);

    // set Signal Paths to 0
    std::vector<CAN::byte_t> zeroPaths(1,0);
    IFProcHWSimBase::setMonitorSignalPaths(zeroPaths);

    // set gains to 0
    std::vector<CAN::byte_t> zeroGains(4,0);
    IFProcHWSimBase::setMonitorGains(zeroGains);
    
    // set BDB to 255
    std::vector<CAN::byte_t> bdbs(1,255);
    IFProcHWSimBase::setMonitorBdbPerPacket(bdbs);

    // set data averaging legth to 128
    std::vector<CAN::byte_t> aveLength(1,7);
    IFProcHWSimBase::setMonitorDataAveLength(aveLength);


    //reset Ethernet also
    IFProcHWSimImpl::setResetTpdEthernet();
}



void IFProcHWSimImpl::setResetTpdEthernet(){
    // set all ethernet values to zero
    std::vector<CAN::byte_t> status(4,0);
    status[0]=3;  // router found, eth connected. Data transfer inactive. All errors cleared.
    std::vector<CAN::byte_t> zeros(4,0);
    std::vector<CAN::byte_t> zero(2,0);

    IFProcHWSimBase::setMonitorStatus(status);
    IFProcHWSimBase::setMonitorModuleIpAddr(zeros);
    IFProcHWSimBase::setMonitorDestIpAddr(zeros);
    IFProcHWSimBase::setMonitorTcpPort(zero);
    IFProcHWSimBase::setMonitorModuleGateway(zeros);
    IFProcHWSimBase::setMonitorModuleNetmask(zeros);

}



/**
 *-----------------------------------------------------------------------------------------
 *       TOTAL POWER SIMULATION
 *-----------------------------------------------------------------------------------------
 */



//std::vector<CAN::byte_t> IFProcHWSimImpl::getMonitorDataMonitor1() {
//    
//    unsigned short SB[2];
//    std::vector<CAN::byte_t> dataMon1(4);
//   
//    std::vector<CAN::byte_t> gains = IFProcHWSimBase::getGains();
//    
//    // Estimate a voltage reading for a given gain.  
//    for (int i=0;i<2;i++)
//        SB[i] = static_cast<unsigned short>( pow(10,gain_m[i]*-0.05+0.39794)*26214.4); 
//
//    // random number between 0 and 52428.8
//    // (655.36 * pow(10, gain_m[idx]/5.0)/2.5));
//
//    //pushing the uint16 values into a data vector 
//    for(int idx=0 ; idx < 4 ; idx++){
//        unsigned char* byteptr = reinterpret_cast< unsigned char* >(&TP[idx]);
//        for (int i=1 ; i>=0; i--)
//            tmp.push_back(byteptr[i]);
//    }
//    
//    IFProcHWSimBase::setDataMonitor1(dataMon1);
//    return IFProcHWSimBase::getMonitorDataMonitor1();
//}

std::vector<CAN::byte_t> IFProcHWSimImpl::getMonitorDataMonitor1() const {
  if (ssi_m!=NULL && ssi_m->haveSimulatorRef())
     {
       return ssi_m->getDownConvPowers(polarization_m);
     }
  else {
    return IFProcHWSimBase::getMonitorDataMonitor1();
  }
}


std::vector<CAN::byte_t> IFProcHWSimImpl::getMonitorDataMonitor2() const {
  
  if (ssi_m!=NULL && ssi_m->haveSimulatorRef()) {
    return ssi_m->getBaseBandPowers(polarization_m);
  } else {
    unsigned short BB[4];
    std::vector<CAN::byte_t> dataMon2(8);
    std::vector<CAN::byte_t> gains = getMonitorGains();
    
    IFProcHWSimImpl* ifp = const_cast<IFProcHWSimImpl*> (this);
    
    // Estimate a voltage reading for a given gain.  
    for (int i=0;i<4;i++){
      BB[i] = static_cast<unsigned short>( pow(10, 0.4 - gains[i]*0.001587302) *65536/2.5); 
      //cout << "Generated voltage: " << BB[i] << endl;
    }
    
    //pushing the uint16 values into a data vector 
    for(int idx=0 ; idx < 4 ; idx++){
      unsigned char* byteptr = reinterpret_cast< unsigned char* >(&BB[idx]);
      for (int i=1 ; i>=0; i--)
	dataMon2[idx*2+i] = byteptr[i];
    }
    
    const std::vector<CAN::byte_t> tmp = dataMon2; 
    ifp->setMonitorDataMonitor2(tmp);
    return IFProcHWSimBase::getMonitorDataMonitor2();
    
  }
}
  
/**
 *-----------------------------------------------------------------------------------------
 *       NETWORK SIMULATION
 *-----------------------------------------------------------------------------------------
 */


void IFProcHWSimImpl::setControlInitTcpConn(const std::vector<CAN::byte_t>& data){
    IFProcHWSimBase::setControlInitTcpConn(data);

    // Set all the preseted network values
    IFProcHWSimBase::setMonitorModuleIpAddr(IFProcHWSimBase::getControlSetModuleIpAddr());
    IFProcHWSimBase::setMonitorDestIpAddr(IFProcHWSimBase::getControlSetDestIpAddr());
    IFProcHWSimBase::setMonitorTcpPort(IFProcHWSimBase::getControlSetTcpPort());
    IFProcHWSimBase::setMonitorModuleGateway(IFProcHWSimBase::getControlSetModuleGateway());
    IFProcHWSimBase::setMonitorModuleNetmask(IFProcHWSimBase::getControlSetModuleNetmask());
   
    // Try to initialize a TCP connection
    m_try_initialize = true;
}


void IFProcHWSimImpl::setControlStartData(const std::vector< CAN::byte_t >& data){
    IFProcHWSimBase::setControlStartData(data);

    //Try to start the data transmition
    m_try_start_data = true;
}

void IFProcHWSimImpl::setControlStopData(const std::vector< CAN::byte_t >& data){
    IFProcHWSimBase::setControlStopData(data);
    //Try to stop the data transmission
    m_try_stop_data = true;
}


//           THREAD
//-----------------------------

//void IFProcHWSimImpl::stopThread(void){
//    cout << "Stoppping the thread..."<< std::endl;
//    // Trigger the thread finalization
//    m_finalizeThread = true;
//    // suspend the thread 
//    ethernetDataThread_p.suspend();
//
//}




//------------------------------------------------------------------------------------------------------


