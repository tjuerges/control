/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.IFProc.panels;

import alma.Control.device.gui.IFProc.IcdMonitorPoints;
import alma.Control.device.gui.IFProc.presentationModels.DevicePM;
import alma.Control.device.gui.IFProc.widgets.SignalPathConfigWidget;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.CodeToTextMonitorPointWidget;
import alma.Control.device.gui.IFProc.widgets.DataRemapConfigWidget;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;
import javax.swing.BorderFactory;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized in panels and tabs to group related data together.
 * 
 * TODO: Update this comment to reflect what this pane does.
 * 
 * Tabbed panels are used to group related data together and separate unrelated data. These are created as
 * required for each GUI.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class DebugDataPanel extends DevicePanel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;

    public DebugDataPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildSubPanels(logger, aDevicePM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM aDevicePM){
        //TODO: rename sub-panels as appropriate
        dataRemapMonitorPane = new DataRemapMonitorPane(logger, aDevicePM);
        dataRemapConfigWidget = new DataRemapConfigWidget(logger, aDevicePM);
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));
//        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("PanelTitle"),
//                BorderFactory.createEmptyBorder(1, 1, 1, 1)));

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;

        add(dataRemapMonitorPane, c);
        c.gridx++;
        add(dataRemapConfigWidget, c);

        setVisible(true);
    }

    /*
     * The panel provides a readout of all the data remap settings.
     */
    private class DataRemapMonitorPane extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public DataRemapMonitorPane(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
          setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder
                  ("Data Remap Status"), BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.anchor=GridBagConstraints.WEST;

            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 1;
            c.gridy = 0;

            addLeftEastLabel("Ch A:", c);
            c.gridy++;
            addLeftEastLabel("Ch B:", c);
            c.gridy++;
            addLeftEastLabel("Ch C:", c);
            c.gridy++;
            addLeftEastLabel("Ch D:", c);
            c.gridy++;
            addLeftEastLabel("USB:", c);
            c.gridy++;
            addLeftEastLabel("LSB:", c);
            
            c.gridx++;
            c.gridy=0;
            int[] numbers = {0, 1, 2, 3, 4, 5, 0x0F};
            String[] codesA = {"Digitizer 0", "Digitizer 1", "Digitizer 2", "Digitizer 3",
                              "Digitizer 4", "Digitizer 5", "Digitizer 3"};
            //Ch A
            add(new CodeToTextMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DATA_REMAP_IFDC_A), codesA,
                    "Error: unknown digitizer", 0xFFFFFFFF, numbers), c);
            c.gridy++;
            //We make a new string each time because if we change the old one, it might change it
            //for all the widgets (pass by reference vs value)
            
            //Ch B
            String[] codesB = {"Digitizer 0", "Digitizer 1", "Digitizer 2", "Digitizer 3",
                    "Digitizer 4", "Digitizer 5", "Digitizer 2"};
            add(new CodeToTextMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DATA_REMAP_IFDC_B), codesB,
                    "Error: unknown digitizer", 0xFFFFFFFF, numbers), c);
            c.gridy++;
            //Ch C
            String[] codesC = {"Digitizer 0", "Digitizer 1", "Digitizer 2", "Digitizer 3",
                    "Digitizer 4", "Digitizer 5", "Digitizer 1"};
            add(new CodeToTextMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DATA_REMAP_IFDC_C), codesC,
                    "Error: unknown digitizer", 0xFFFFFFFF, numbers), c);
            c.gridy++;
            //Ch D
            String[] codesD = {"Digitizer 0", "Digitizer 1", "Digitizer 2", "Digitizer 3",
                    "Digitizer 4", "Digitizer 5", "Digitizer 0"};
            add(new CodeToTextMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DATA_REMAP_IFDC_D), codesD,
                    "Error: unknown digitizer", 0xFFFFFFFF, numbers), c);
            c.gridy++;
            //USB
            String[] codesUSB = {"Digitizer 0", "Digitizer 1", "Digitizer 2", "Digitizer 3",
                    "Digitizer 4", "Digitizer 5", "Digitizer 5"};
            add(new CodeToTextMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DATA_REMAP_SIDEBAND_0), codesUSB,
                    "Error: unknown digitizer", 0xFFFFFFFF, numbers), c);
            c.gridy++;
            //LSB
            String[] codesLSB = {"Digitizer 0", "Digitizer 1", "Digitizer 2", "Digitizer 3",
                    "Digitizer 4", "Digitizer 5", "Digitizer 4"};
            add(new CodeToTextMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DATA_REMAP_SIDEBAND_1), codesLSB,
                    "Error: unknown digitizer", 0xFFFFFFFF, numbers), c);
        }
    }

    DataRemapMonitorPane dataRemapMonitorPane;
    DataRemapConfigWidget dataRemapConfigWidget;
}

//
// O_o
