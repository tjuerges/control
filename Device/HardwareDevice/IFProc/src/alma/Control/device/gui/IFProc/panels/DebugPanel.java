/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.IFProc.panels;

import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.IFProc.presentationModels.DevicePM;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JTabbedPane;
import javax.swing.JScrollPane;

/**
 * This panel provides an assortment of information of which is not expected to be regularly needed.
 * But it will be necessary for debugging operations.
 * 
 * As this includes a lot of monitors and controls, it is divided into sub-groups. At the moment
 * this is done as another level of tabs. It might be worth finding a better way to do this, but
 * we have not yet managed to do so.;
 * 
 * @author David Hunter      dhunter@nrao.edu
 * @version $Id$
 * @since 
 */

public class DebugPanel extends DevicePanel {

    public DebugPanel(Logger logger, DevicePM dPM) {
        super(logger, dPM);
        buildSubPanels(logger, dPM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
        debugDataPanel = new DebugDataPanel (logger, aDevicePM);
        debugEthernetPanel = new DebugEthernetPanel (logger, aDevicePM);
      debugRfPanel = new DebugRfPanel (logger, aDevicePM);
    }

    protected void buildPanel()
    {
        setBorder(BorderFactory.createEtchedBorder(1));

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.weighty=100;
        c.weightx=100;
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.CENTER;

        tabs = new JTabbedPane();
        tabs.add("Ethernet", new JScrollPane(debugEthernetPanel));
        tabs.add("RF", new JScrollPane(debugRfPanel));
        tabs.add("Data", new JScrollPane(debugDataPanel));
        
        c.gridx = 0;
        c.gridy = 0;
        c.anchor = GridBagConstraints.CENTER;
        add(tabs, c);

        this.setVisible(true);
    }

    DebugDataPanel debugDataPanel;
    DebugEthernetPanel debugEthernetPanel;
    DebugRfPanel debugRfPanel;
    JTabbedPane tabs;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
}

//
// O_o

