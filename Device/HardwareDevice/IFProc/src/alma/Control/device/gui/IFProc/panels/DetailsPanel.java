/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.IFProc.panels;

import alma.Control.device.gui.IFProc.IcdMonitorPoints;
import alma.Control.device.gui.IFProc.IdlMonitorPoints;
import alma.Control.device.gui.IFProc.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JTabbedPane;
import javax.swing.JButton;
import javax.swing.JScrollPane;

/**
 * Hardware device GUI data is divided into Summary data and Detail data.
 * Summary data is always visible on the GUI. Detail data is organized in panels
 * and tabs to group related data together.
 * 
 * This class contains the Detail data. It also provides a Show/Hide button for
 * the data.
 * 
 * Tabed panels are used to group related data together and separate unrelated
 * data. These are created as required for each GUI.
 * 
 * @author Scott Rankin srankin@nrao.edu
 * @author David Hunter dhunter@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class DetailsPanel extends DevicePanel {

    public DetailsPanel(Logger logger, DevicePM pm) {
        super(logger, pm);
        buildSubPanels(logger, pm);
        buildPanel();
    }

    protected void buildPanel() {
        this.setBorder(BorderFactory.createCompoundBorder(BorderFactory
                .createTitledBorder("Details"), BorderFactory
                .createEmptyBorder(1, 1, 1, 1)));

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.LINE_END;
        c.weighty = 1;
        c.weightx = 1;
        c.gridx = 0;
        c.gridy = 0;

        add(new ShowHideButton(), c);
        tabs = new JTabbedPane();
        tabs.add("Status", new JScrollPane(statusPanel));
        tabs.add("Ethernet", new JScrollPane(enetPanel));
        tabs.add("Power", new JScrollPane(powerPanel));
        tabs.add("Debug", new JScrollPane(debugPanel));
        tabs.add("Device Info", new JScrollPane(deviceInfoPanel));

        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.CENTER;
        c.weighty = 100;
        c.weightx = 100;
        customizeMonitorPoints();
        c.gridy++;
        add(tabs, c);

        this.setVisible(true);
    }

    /**
     * It is necessary to build all sub-panels before buildPanel() is called.
     * 
     * @param logger to be used by the sub-panels.
     * @param dPM the device presentation model for the device containing the panel.
     */
    private void buildSubPanels(Logger logger, DevicePM dPM) {
        deviceInfoPanel = new DeviceInfoPanel (logger, dPM);
        statusPanel = new StatusPanel(logger, dPM);
        powerPanel = new PowerPanel(logger, dPM);
        debugPanel = new DebugPanel(logger, dPM);
        enetPanel = new EnetPanel(logger, dPM);
    }
    
    /**
     * This function sets any customizations needed for displaying the monitor data in a user friendly format.
     * E.g degree C instead of K.
     */
    private void customizeMonitorPoints() {
        devicePM.getMonitorPointPM(IdlMonitorPoints.GET_AMBIENT_TEMPERATURE).getMonitorPoint().setDisplayOffset(-273.15);
        devicePM.getMonitorPointPM(IdlMonitorPoints.GET_AMBIENT_TEMPERATURE).getMonitorPoint().setDisplayUnits("\u00B0C");
        devicePM.getMonitorPointPM(IcdMonitorPoints.TEMP_SEC_1).getMonitorPoint().setDisplayOffset(-273.15);
        devicePM.getMonitorPointPM(IcdMonitorPoints.TEMP_SEC_1).getMonitorPoint().setDisplayUnits("\u00B0C");
        devicePM.getMonitorPointPM(IcdMonitorPoints.TEMP_SEC_2).getMonitorPoint().setDisplayOffset(-273.15);
        devicePM.getMonitorPointPM(IcdMonitorPoints.TEMP_SEC_2).getMonitorPoint().setDisplayUnits("\u00B0C");
        devicePM.getMonitorPointPM(IcdMonitorPoints.TEMP_SEC_A).getMonitorPoint().setDisplayOffset(-273.15);
        devicePM.getMonitorPointPM(IcdMonitorPoints.TEMP_SEC_A).getMonitorPoint().setDisplayUnits("\u00B0C");
        devicePM.getMonitorPointPM(IcdMonitorPoints.TEMP_SEC_B).getMonitorPoint().setDisplayOffset(-273.15);
        devicePM.getMonitorPointPM(IcdMonitorPoints.TEMP_SEC_B).getMonitorPoint().setDisplayUnits("\u00B0C");
        devicePM.getMonitorPointPM(IcdMonitorPoints.TEMP_SEC_C).getMonitorPoint().setDisplayOffset(-273.15);
        devicePM.getMonitorPointPM(IcdMonitorPoints.TEMP_SEC_C).getMonitorPoint().setDisplayUnits("\u00B0C");
        devicePM.getMonitorPointPM(IcdMonitorPoints.TEMP_SEC_D).getMonitorPoint().setDisplayOffset(-273.15);
        devicePM.getMonitorPointPM(IcdMonitorPoints.TEMP_SEC_D).getMonitorPoint().setDisplayUnits("\u00B0C");
        devicePM.getMonitorPointPM(IcdMonitorPoints.TEMP_SWITCH_MATRIX_1).getMonitorPoint().setDisplayOffset(-273.15);
        devicePM.getMonitorPointPM(IcdMonitorPoints.TEMP_SWITCH_MATRIX_1).getMonitorPoint().setDisplayUnits("\u00B0C");
        devicePM.getMonitorPointPM(IcdMonitorPoints.TEMP_SWITCH_MATRIX_2).getMonitorPoint().setDisplayOffset(-273.15);
        devicePM.getMonitorPointPM(IcdMonitorPoints.TEMP_SWITCH_MATRIX_2).getMonitorPoint().setDisplayUnits("\u00B0C");
        devicePM.getMonitorPointPM(IcdMonitorPoints.TEMPS_3).getMonitorPoint().setDisplayOffset(-273.15);
        devicePM.getMonitorPointPM(IcdMonitorPoints.TEMPS_3).getMonitorPoint().setDisplayUnits("\u00B0C");
        devicePM.getMonitorPointPM(IcdMonitorPoints.GA).getMonitorPoint().setDisplayUnits("dB");
        devicePM.getMonitorPointPM(IcdMonitorPoints.GB).getMonitorPoint().setDisplayUnits("dB");
        devicePM.getMonitorPointPM(IcdMonitorPoints.GC).getMonitorPoint().setDisplayUnits("dB");
        devicePM.getMonitorPointPM(IcdMonitorPoints.GD).getMonitorPoint().setDisplayUnits("dB");
        devicePM.getMonitorPointPM(IcdMonitorPoints.MAX_TIME_SINGLE_PACKET).getMonitorPoint().setDisplayUnits("mS");
        devicePM.getMonitorPointPM(IcdMonitorPoints.MAX_TIME_SINGLE_PACKET).getMonitorPoint().setDisplayScale(1000);
        devicePM.getMonitorPointPM(IcdMonitorPoints.MAX_ETHER_TRANSMIT_AND_ETHER_HANDLING).getMonitorPoint().setDisplayUnits("mS");
        devicePM.getMonitorPointPM(IcdMonitorPoints.MAX_ETHER_TRANSMIT_AND_ETHER_HANDLING).getMonitorPoint().setDisplayScale(1000);
        devicePM.getMonitorPointPM(IcdMonitorPoints.LENGTH_48MS).getMonitorPoint().setDisplayUnits("mS");
        devicePM.getMonitorPointPM(IcdMonitorPoints.LENGTH_48MS).getMonitorPoint().setDisplayScale(1000);
    }
    private class ShowHideButton extends JButton implements ActionListener {

        public ShowHideButton() {
            hideButtonText = "Hide Details";
            showButtonText = "Show Details";
            hideActionCommand = "HIDE_DETAILS";
            showActionCommand = "SHOW_DETAILS";

            setText(hideButtonText);
            setActionCommand(hideActionCommand);
            addActionListener(this);
        }

        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand() == hideActionCommand) {
                tabs.setVisible(false);
                setText(showButtonText);
                setActionCommand(showActionCommand);
            } else if (e.getActionCommand() == showActionCommand) {
                tabs.setVisible(true);
                setText(hideButtonText);
                setActionCommand(hideActionCommand);
            }
        }

        private String hideActionCommand;
        private String hideButtonText;
        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
        private String showActionCommand;
        private String showButtonText;
    }
    
    private JTabbedPane tabs;
    private DeviceInfoPanel deviceInfoPanel;
    private DebugPanel debugPanel;
    private EnetPanel enetPanel;
    private PowerPanel powerPanel;
    private StatusPanel statusPanel;

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
}

//
// O_o
