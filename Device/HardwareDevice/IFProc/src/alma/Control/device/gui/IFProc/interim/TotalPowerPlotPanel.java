/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
package alma.Control.device.gui.IFProc.interim;

import alma.Control.device.gui.IFProc.IdlMonitorPoints;
import alma.Control.device.gui.IFProc.presentationModels.DevicePM;
import alma.Control.device.gui.common.MonitorPointPresentationModel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.logging.Logger;

import javax.swing.JPanel;

/**
 * Display a Total Power plot.
 * 
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 7.0.0
 */
public class TotalPowerPlotPanel extends JPanel {

    private static final int DEFAULT_HEIGHT = 300;
    private static final int DEFAULT_WIDTH = 400;
    
    /**
     * Create an instance of a plot for an antenna and an IFProc instance.
     * 
     * @param aDevicePM a IFProc device presentation model.
     */
    public TotalPowerPlotPanel(DevicePM aDevicePM) {
        super();
                
        logger = aDevicePM.getLogger();
        final String METHOD_NAME = "TotalPowerPlotPanel";
        devicePM = aDevicePM;
        initBaseBandPairs();
        setPanelDefaults();        
        buildPanel();
        
        dataMonitor2 = devicePM.getMonitorPointPM(IdlMonitorPoints.GET_DATA_MONITOR_2);
        
        startPollingMonitorPoints();
    }

//    public String getAntenna() { return devicePM.getAntenna(); }
    public Color getBbpColor(int i) { return bbPairs[i].getColor(); }
    public boolean getBbpEnabled(int i) { return bbPairs[i].getEnabled(); }
    public int getChartDuration() { return chartDuration; }
    public int getChartDurationLowerBound() { return CHART_DURATION_LOWER_BOUND; }
    public int getChartDurationUpperBound() { return CHART_DURATION_UPPER_BOUND; }
    public int getIfProcNum() { return devicePM.getInstance(); }
    public ChartType getPlotType() { return chartType; }
    public ChartUnits getPlotUnits() { return chartUnits; }
    
    public void setBbpEnabled(int i, boolean b) {
        final String METHOD_NAME = "setBbpEnabled";
        bbPairs[i].setEnabled(b);
        updatePlot();
    }
    
    public void setChartDuration(int duration) {
        final String METHOD_NAME = "setChartDuration";
        // See if there is anything to do.
        if (chartDuration == duration) return;
        
        // Save cached value.
        chartDuration = duration;
        
        // Push value through to each base band pair.  Don't check if the time series is enabled.
        // We want this value if the time series is enabled later.
        for (BaseBandPair bbp: bbPairs) bbp.setDuration(chartDuration);
        updatePlot();
    }

    public void setChartType(ChartType type) {
        final String METHOD_NAME = "setChartType";
        // Save cached value.
        chartType = type;
        updatePlot();
    }

    public void setChartUnits(ChartUnits units) {
        final String METHOD_NAME = "setChartUnits";
        // See if there is anything to do.
        if (chartUnits != null && chartUnits.equals(units)) return;
        
        // Save cached value.
        chartUnits = units;
        updatePlot();
    }

    private void buildPanel() {
        final String METHOD_NAME = "buildPanel";
        setLayout(new BorderLayout());
        setPreferredSize(new java.awt.Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT));
        
        createNewCombinedPlotPanel();
        add(chartPanel, BorderLayout.CENTER);
        revalidate();
    }
    
    /**
     * Create a chart with all data in one plot.
     */
    private void createNewCombinedPlotPanel() {
        final String METHOD_NAME = "createNewCombinedPlotPanel";
        CombinedChart combinedChart = new CombinedChart(chartUnits, bbPairs, logger);
        chartPanel = combinedChart.getChartPanel();
    }

    /**
     * Create a chart with data split into separate plots.
     * 
     * Terminology gets confusing here.  JFreeChart calls this a combined domain plot.
     */
    private void createNewSplitPlotPanel() {
        final String METHOD_NAME = "createNewSplitPlotPanel";
        SplitChart splitChart = new SplitChart(chartUnits, bbPairs, logger);
        chartPanel = splitChart.getChartPanel();
    }
    
    private void initBaseBandPairs() {
        final String METHOD_NAME = "initBaseBandPairs";
        bbPairs[0] = new BaseBandPair(0, Color.RED, devicePM);
        bbPairs[1] = new BaseBandPair(1, Color.ORANGE, devicePM);
        bbPairs[2] = new BaseBandPair(2, Color.GREEN, devicePM);
        bbPairs[3] = new BaseBandPair(3, Color.BLUE, devicePM);
    }

    /**
     * 
     */
    private void pollMonitorPoints() {
        final String METHOD_NAME = "pollMonitorPoints";
        // for (BaseBandPair bbp: bbPairs) bbp.sampleMonitorPoints();
        dataMonitor2.checkDeviceForUpdates();
    }


    private void setPanelDefaults() {
        final String METHOD_NAME = "setPanelDefaults";
        for (BaseBandPair bbp: bbPairs) bbp.setEnabled(true); 
        setChartDuration(30);
        setChartUnits(ChartUnits.VOLTS);
        setChartType(ChartType.COMBINED);
    }
    
    private void startPollingMonitorPoints() {
        final String METHOD_NAME = "startPollingMonitorPoints";
        final int SAMPLE_PERIOD_IN_MS = 250; 
        
        pollingThread = new Thread() {
            public void run() {
                while(true) {
                    pollMonitorPoints();
                    try {
                        Thread.sleep(SAMPLE_PERIOD_IN_MS);
                    } catch (InterruptedException e) {
                        logger.severe(CLASS_NAME + "." + METHOD_NAME + 
                                " pollingThread: Something interrupted a sleep.");
                        e.printStackTrace();
                    }
                }
            }
        };
        pollingThread.start();
    }

    private void updatePlot() {
        final String METHOD_NAME = "updatePlot";
        // This method may be called before chartPanel has been initialized. 
        if (chartPanel == null) return;
        
        // Save the old panel reference to remove later.
        JPanel oldPanel = chartPanel;
        
        // Create a new plot panel.
        switch (chartType.ord()) {
        case 0: createNewCombinedPlotPanel(); break;
        case 1: createNewSplitPlotPanel(); break;
        }
        
        // Swap old and new plot panels.
        if (null != oldPanel) remove(oldPanel);
        add(chartPanel, BorderLayout.CENTER);
        revalidate();
    }
 
    /**
     * Upper and lower bounds of chart durations, in seconds.
     */
    private static final int CHART_DURATION_LOWER_BOUND = 10;
    private static final int CHART_DURATION_UPPER_BOUND = 300;

    private static final String CLASS_NAME = "TotalPowerPlotPanel";
    
    /**
     * TODO: correct serialization.
     */
    private static final long serialVersionUID = 1L;
    
    private final BaseBandPair[] bbPairs = new BaseBandPair[4];
    private int chartDuration;
    private JPanel chartPanel;
    private ChartType chartType;
    private ChartUnits chartUnits;
    private MonitorPointPresentationModel<float[]> dataMonitor2;
    private DevicePM devicePM;
    private Logger logger;
    private Thread pollingThread;
}

//
// O_o
