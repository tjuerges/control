/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.IFProc.panels;
import alma.Control.device.gui.IFProc.IcdMonitorPoints;
import alma.Control.device.gui.IFProc.presentationModels.DevicePM;
import alma.Control.device.gui.common.MonitorPointPresentationModel;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.Status;
import alma.Control.device.gui.common.widgets.CombinedBooleanMonitorPointWidget;

import java.util.logging.Logger;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.BorderFactory;


/**
 * Hardware device GUI data is divided into Summary data and Detail data.  Summary data is always visible 
 * on the GUI.  Detail data is organized to keep related data together and separate unrelated data.
 * 
 * This class contains the Summary data.
 * TODO: Replace IFProc with the proper device name
 * 
 * @author Scott Rankin      srankin@nrao.edu
 * @author David Hunter      dhunter@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */
public class SummaryPanel extends DevicePanel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    
    public SummaryPanel(Logger logger, DevicePM pm) {
        super(logger, pm);
        buildPanel();
    }
    
    protected void buildPanel() {
        this.setBorder(BorderFactory.createCompoundBorder( 
                BorderFactory.createTitledBorder(devicePM.getDeviceDescription()),
                BorderFactory.createEmptyBorder(1,1,1,1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor=GridBagConstraints.WEST;
        c.weightx=100;
        c.weighty=100;
        c.gridx=1;
        c.gridy=0;
        addLeftEastLabel("Voltages", c);
        add (new CombinedBooleanMonitorPointWidget(logger,
                           new MonitorPointPresentationModel[]
                               {devicePM.getMonitorPointPM(IcdMonitorPoints.DIGITAL_1DOT2_GOOD),
                               devicePM.getMonitorPointPM(IcdMonitorPoints.DIGITAL_2DOT5_GOOD),
                               devicePM.getMonitorPointPM(IcdMonitorPoints.DIGITAL_3DOT3_GOOD),
                               devicePM.getMonitorPointPM(IcdMonitorPoints.DIGITAL_5_GOOD),
                               devicePM.getMonitorPointPM(IcdMonitorPoints.ETHER_CONT_VOLTAGE_GOOD),
                               devicePM.getMonitorPointPM(IcdMonitorPoints.ANALOG_5_GOOD)},
                           new boolean[] {true, true, true, true, true, true},
                           Status.FAILURE, "Voltage error", Status.GOOD, "All voltages OK"), c);
        c.gridx+=2;
        addLeftEastLabel("Ethernet", c);
        add (new CombinedBooleanMonitorPointWidget(logger,
                new MonitorPointPresentationModel[]
                    {devicePM.getMonitorPointPM(IcdMonitorPoints.ETHERNET_CONNECTED),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.ROUTER_NOT_FOUND),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.ARP_FULL),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TCP_DISCONN_ERROR),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FIFO_FULL)},
                new boolean[] {true, false,false,false, false},
                Status.FAILURE, "Voltage error", Status.GOOD, "All voltages OK"), c);
        c.gridx+=2;
        addLeftEastLabel("Vref", c);
        add (new CombinedBooleanMonitorPointWidget(logger,
                new MonitorPointPresentationModel[]
                    {devicePM.getMonitorPointPM(IcdMonitorPoints.S0_VREF_GOOD),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.S1_VREF_GOOD),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.A_VREF_GOOD),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.B_VREF_GOOD),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.C_VREF_GOOD),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.D_VREF_GOOD)},
                new boolean[] {true, true, true, true, true, true},
                Status.FAILURE, "Voltage error", Status.GOOD, "All voltages OK"), c);
        c.gridx+=2;
        addLeftEastLabel("Timing", c);
        add (new BooleanMonitorPointWidget(logger, 
                devicePM.getMonitorPointPM(IcdMonitorPoints.TIMING_ERROR_OCCURRED),
                Status.GOOD, "Timming between 125MHz clock and 48ms TE OK.",
                Status.FAILURE, "Timming Error between the 125MHz clock and the 48ms TE"), c);
        this.setVisible(true);
    }
}

//
// O_o
