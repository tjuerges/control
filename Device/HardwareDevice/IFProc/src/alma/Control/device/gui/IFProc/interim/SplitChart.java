/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
package alma.Control.device.gui.IFProc.interim;

import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.IAxis;
import info.monitorenter.gui.chart.IAxis.AxisTitle;
import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.labelformatters.LabelFormatterDate;
import info.monitorenter.gui.chart.labelformatters.LabelFormatterNumber;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

/**
 * A combined chart displays all data in one chart panel. 
 * 
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 7.0.0
 */
public class SplitChart {
    
    private static final String X_AXIS_LABEL = "Time";

    /** 
     * Create an instance with the given values.
     * 
     * @param aUnit units for this chart.
     * @param bbPs base band pairs for this chart.
     * @param aLogger a logger to log to.
     */
    public SplitChart(ChartUnits aUnit, BaseBandPair[] bbPs, Logger aLogger) {
        final String METHOD_NAME = "CombinedChart";
        logger = aLogger;        
        bbPairs = bbPs;
        units = aUnit;
        
        createChartPanel();
    }
    
    /**
     * Get the chartPanel to add to a panel.
     * 
     * @return the contained chartPanel instance.
     */
    public JPanel getChartPanel() {
        return chartPanel;
    }
    
    /**
     * The following methods exist only for testing.  Protection is set to package.
     */
    
    /**
     * Get the base band pairs for this chartPanel.
     * 
     * @return the array of base band pairs.
     */
    BaseBandPair[] getBaseBandPairs() {
        return bbPairs;
    }
    
    /**
     * Get the units for this chartPanel.
     * 
     * @return units.
     */
    ChartUnits getUnits() {
        return units;
    }
    
    private void createChartPanel() {
        final String METHOD_NAME = "createChartPanel";
        final String Y_AXIS_LABEL = (ChartUnits.DBM == units) ? "dBm" : "Volts";

        chartPanel = new JPanel();
        chartPanel.setLayout(new BoxLayout(chartPanel, BoxLayout.Y_AXIS));
        
        Chart2D chart;
        IAxis xAxis = null;
        IAxis yAxis = null;

        ITrace2D trace;

        for (BaseBandPair bbp: bbPairs) {
            if (bbp.getEnabled()) {
                chart = new Chart2D();

                xAxis = chart.getAxisX();
                xAxis.setFormatter(new LabelFormatterDate(new SimpleDateFormat("mm:ss")));
                xAxis.setAxisTitle(new AxisTitle(""));

                yAxis = chart.getAxisY();
                yAxis.setFormatter(new LabelFormatterNumber(new DecimalFormat("#0.000")));
                yAxis.setAxisTitle(new AxisTitle(Y_AXIS_LABEL + " " + bbp.getName()));

                trace = (units.equals(ChartUnits.DBM)) ?
                    bbp.getDBmTrace() :
                    bbp.getVoltTrace();
                chart.addTrace(trace);

                chartPanel.add(chart);
            }
        }
        
        // xAxis will point to the x axis of the last chart created.  That is the only one 
        // we want to label.
        if (null != xAxis) {
            xAxis.setAxisTitle(new AxisTitle(X_AXIS_LABEL));
        }
    }
    
    private static final String CLASS_NAME = "CombinedChart";

    private BaseBandPair[] bbPairs;
    private JPanel chartPanel;
    private Logger logger;
    private ChartUnits units;
}

//
// O_o
