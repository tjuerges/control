/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.IFProc.panels;

import alma.Control.device.gui.IFProc.IcdControlPoints;
import alma.Control.device.gui.IFProc.IcdMonitorPoints;
import alma.Control.device.gui.IFProc.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.FloatMonitorPointWidget;
import alma.Control.device.gui.common.widgets.BooleanControlPointWidget;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;
import javax.swing.BorderFactory;

/**This is the main status panel. It displayes a lot of summery alarms, and gives a good idea of
 * what the device is doing. While it is more detailed than the summary panel, it is not as detailed
 * as some of the other panels. A lot of the information displayed here is redundant with other
 * panels. The conclusion is that users will find the overview given by this panel to be useful;
 * the other panels are focused on one specific detail.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class StatusPanel extends DevicePanel {

    public StatusPanel(Logger logger, DevicePM devicePM) {
        super(logger, devicePM);
        buildSubPanels(logger, devicePM);
        buildPanel();
    }
    
    private void buildSubPanels(Logger logger, DevicePM aDevicePM){
        enetStatusPanel = new EnetStatusPanel(logger, aDevicePM);
        gainsPanel = new GainsPanel(logger, aDevicePM);
        powerStatusPanel = new PowerStatusPanel(logger, aDevicePM);
        refVoltageStatusPanel = new RefVoltageStatusPanel(logger, aDevicePM);
        signalPathPanel = new SignalPathPanel(logger, aDevicePM);
        tempPanel= new TempPanel(logger, aDevicePM);
    }

    protected void buildPanel() {
        logger.entering("StatusPanel", "buildPanel");
        setBorder(BorderFactory.createEtchedBorder(1));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;

        add(tempPanel, c);
        c.gridy++;
        c.gridheight=2;
        add(signalPathPanel, c);
        c.gridheight=1;
        c.gridx = 1;
        c.gridy = 0;
        add(enetStatusPanel, c);
        c.gridy++;
        add(gainsPanel, c);
        c.gridy++;
        c.fill=GridBagConstraints.NONE;
        add(new BooleanControlPointWidget(logger, devicePM
                .getControlPointPM(IcdControlPoints.RESET_TPD_BOARD), "Reset Module"), c);
        c.gridx++;
        c.gridy=0;
        c.fill=GridBagConstraints.BOTH;
        add(powerStatusPanel, c);
        c.gridy++;
        c.gridheight=2;
        add(refVoltageStatusPanel, c);

        setVisible(true);
        logger.exiting("StatusPanel", "buildPanel");
    }

    /*
     * This panel displays the internal device temperature information from READ_TEMPS_1, 2, and 3.
     */
    private class TempPanel extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public TempPanel(Logger logger, DevicePM devicePM) {
            super(logger, devicePM);
            buildPanel();
        }

        protected void buildPanel() {
            logger.entering("StatusPanel:SubPanel11Name", "buildPanel");
          setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Temperature"),
                  BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            // c.fill = GridBagConstraints.HORIZONTAL;

            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 1;
            c.gridy = 0;
            c.anchor=GridBagConstraints.WEST;
            addLeftEastLabel("Output Section 1:", c);
            add(new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TEMP_SEC_1), true, 1), c);
            c.gridy++;
            addLeftEastLabel("Output Section 2:", c);
            add(new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TEMP_SEC_2), true, 1), c);
            c.gridy++;
            addLeftEastLabel("Output Section A:", c);
            add(new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TEMP_SEC_A), true, 1), c);
            c.gridy++;
            addLeftEastLabel("Output Section B:", c);
            add(new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TEMP_SEC_B), true, 1), c);
            c.gridy++;
            addLeftEastLabel("Output Section C:", c);
            add(new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TEMP_SEC_C), true, 1), c);
            c.gridy++;
            addLeftEastLabel("Output Section D:", c);
            add(new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TEMP_SEC_D), true, 1), c);
            c.gridy++;
            addLeftEastLabel("Switch Matrix 1:", c);
            add(new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TEMP_SWITCH_MATRIX_1), true, 1), c);
            c.gridy++;
            addLeftEastLabel("Switch Matrix 2:", c);
            add(new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TEMP_SWITCH_MATRIX_2), true, 1), c);
            c.gridy++;
            addLeftEastLabel("Communication Module:", c);
            add(new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TEMPS_3), true, 1), c);
        }
    }
    
    private TempPanel tempPanel;
    private EnetStatusPanel enetStatusPanel;
    private GainsPanel gainsPanel;
    private PowerStatusPanel powerStatusPanel;
    private RefVoltageStatusPanel refVoltageStatusPanel;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private SignalPathPanel signalPathPanel;

}

//
// O_o
