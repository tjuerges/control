/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.IFProc.widgets;

import alma.Control.device.gui.IFProc.IcdControlPoints;
import alma.Control.device.gui.IFProc.presentationModels.DevicePM;
import alma.Control.device.gui.common.ControlPointPresentationModel;
import alma.Control.device.gui.common.panels.DevicePanel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

/**This is a custom widget written to set the IFProc signal paths (SET_SIGNAL_PATHS, RCA 0x00182).
 * It provides 6 radio buttons. 4 to set Channels A-D and 2 for the upper and lower sidebands.
 * When the control button is pressed the set path command is sent.
 * 
 * Notes:
 *   -The ICD requires that all the paths be set at the same time. The design of the widget requires
 *    the user to do so as well.
 *    
 *   -The ICD states that this control command should never be sent twice in a 20TE period. A user
 *    could violate this by pressing the button non-stop. So the button locks for 1 second after
 *    each use.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class SignalPathConfigWidget extends DevicePanel {

    public SignalPathConfigWidget(Logger logger, DevicePM ifpDevicePM) {
        super(logger, ifpDevicePM);
        dPM=ifpDevicePM;
        setPathsControl=dPM.getControlPointPM(IcdControlPoints.SIGNAL_PATHS);
        buildPanel();
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder
                ("Set Paths"), BorderFactory.createEmptyBorder(1, 1, 1, 1)));

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.WEST;
        c.gridx = 0;
        c.gridy = 1;
        //Add all the left hand labels
        addLeftEastLabel ("Ch A", c);
        c.gridy++;
        addLeftEastLabel ("Ch B", c);
        c.gridy++;
        addLeftEastLabel ("Ch C", c);
        c.gridy++;
        addLeftEastLabel ("Ch D", c);
        c.gridy++;
        c.gridy++;
        addLeftEastLabel ("SAB", c);
        c.gridy++;
        addLeftEastLabel ("SCD", c);
        c.gridy++;
        c.gridwidth=3;
        setPaths=new JButton("Set Paths");
        setPaths.setMultiClickThreshhold(1000);
        setPaths.addActionListener(new setValuesAction());
        setPaths.setMargin(new Insets(0, 2, 0, 2)); 
        //Add the button to the disable controls button so that it properly responds to
        //a disable or enable controls command.
        setPathsControl.getContainingDevicePM().addControl(setPaths);
        add(setPaths, c);
        c.gridwidth=1;
        c.gridx++;
        c.gridy=0;
        
        //Add all of the low filter selection buttons
        addLabel("Low  ", c);
        c.gridy++;
        aLow = new JRadioButton();
        aGroup = new ButtonGroup();
        aGroup.add(aLow);
        aLow.setSelected(true);
        add(aLow, c);
        c.gridy++;
        bLow = new JRadioButton();
        bGroup = new ButtonGroup();
        bGroup.add(bLow);
        bLow.setSelected(true);
        add(bLow, c);
        c.gridy++;
        cLow = new JRadioButton();
        cGroup = new ButtonGroup();
        cGroup.add(cLow);
        cLow.setSelected(true);
        add(cLow, c);
        c.gridy++;
        dLow = new JRadioButton();
        dGroup = new ButtonGroup();
        dGroup.add(dLow);
        dLow.setSelected(true);
        add(dLow, c);
        //Add the lower sideband selection buttons
        c.gridy++;
        addLabel("Lower  ", c);
        c.gridy++;
        sabLower = new JRadioButton();
        sabGroup = new ButtonGroup();
        sabGroup.add(sabLower);
        sabLower.setSelected(true);
        add(sabLower, c);
        c.gridy++;
        scdLower = new JRadioButton();
        scdGroup = new ButtonGroup();
        scdGroup.add(scdLower);
        scdLower.setSelected(true);
        add(scdLower, c);
        
        //Add the high filter selection buttons
        c.gridx++;
        c.gridy=0;
        addLabel("High", c);
        c.gridy++;
        aHigh = new JRadioButton();
        aGroup.add(aHigh);
        add(aHigh, c);
        c.gridy++;
        bHigh = new JRadioButton();
        bGroup.add(bHigh);
        add(bHigh, c);
        c.gridy++;
        cHigh = new JRadioButton();
        cGroup.add(cHigh);
        add(cHigh, c);
        c.gridy++;
        dHigh = new JRadioButton();
        dGroup.add(dHigh);
        add(dHigh, c);
        //Add the upper sideband selection button
        c.gridy++;
        addLabel("Upper", c);
        c.gridy++;
        sabUpper = new JRadioButton();
        sabGroup.add(sabUpper);
        add(sabUpper, c);
        c.gridy++;
        scdUpper = new JRadioButton();
        scdGroup.add(scdUpper);
        add(scdUpper, c);

        setVisible(true);
    }
    
    private void addLabel(String s, GridBagConstraints c){
        int tmp=c.anchor;
        c.insets = new Insets(5,0,0,0);
        c.anchor=GridBagConstraints.SOUTHWEST;
        add(new JLabel(s), c);
        c.anchor=tmp;
        c.insets = new Insets(0,0,0,0);
    }
    
    /**A simple action listener that launches the set values thread.
     */
    private class setValuesAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Thread t = new Thread(new setValuesThread());
            dPM.getPluginContainerServices().getThreadFactory().newThread(t).start();
        }
    }
    /** The set values thread.
     *  This thread pools the values of the controls and sends the proper
     *  command to the SET_SIGNAL_PATHS control. Then it sleeps 1 second. The control button is
     *  kept disabled during this time becasue the ICD specs a minimum wait of 20TEs between 
     *  control commands. Repeatdly pressing the button would excede this limit. It is possible,
     *  though unlikely, that the Control system enforces this limit. But, even if it does, it needs
     *  to be clear to the user that they can not press the button more often.
     *  
     *  This is, of course, all done in a thread so that the GUI does not lock up during the sleep.
     *
     */
    private class setValuesThread implements Runnable{
        public void run(){
            setPaths.setEnabled(false);
            boolean[] data={false, false, false, false, false, false};
            if (aHigh.isSelected())
                data[0]=true;
            if (bHigh.isSelected())
                data[1]=true;
            if (cHigh.isSelected())
                data[2]=true;
            if (dHigh.isSelected())
                data[3]=true;
            //Note: the IFProc Rev C ICD contains an error.
            //0 is LSB and 1 is USB. This widget uses the correct values, not
            //what is in the ICD. The Rev D ICD corrects this error. Once Computing has
            //fully switched to Rev D, this comment can be removed.
            if (sabUpper.isSelected())
                data[4]=true;
            if (scdUpper.isSelected())
                data[5]=true;
            setPathsControl.setValue(data);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e1) {
                // This is not enough of an error for us to care, but we go ahead and log it.
                logger.severe("SignalPathConfigWidget, setValuesAction Error: failed to sleep 1s" +
                              "after sending SET_VALUES.");
            }
            setPaths.setEnabled(true);
            
        }
    }
    
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    
    private JRadioButton aLow;
    private JRadioButton aHigh;
    private ButtonGroup aGroup;
    private JRadioButton bLow;
    private JRadioButton bHigh;
    private ButtonGroup bGroup;
    private JRadioButton cLow;
    private JRadioButton cHigh;
    private ButtonGroup cGroup;
    private JRadioButton dLow;
    private JRadioButton dHigh;
    private ButtonGroup dGroup;
    private DevicePM dPM;
    private JRadioButton sabLower;
    private JRadioButton sabUpper;
    private ButtonGroup sabGroup;
    private JRadioButton scdLower;
    private JRadioButton scdUpper;
    private ButtonGroup scdGroup;
    private JButton setPaths;
    private ControlPointPresentationModel<boolean[]> setPathsControl;
}

//
// O_o
