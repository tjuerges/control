/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.IFProc.panels;

import alma.Control.device.gui.IFProc.IcdMonitorPoints;
import alma.Control.device.gui.IFProc.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BooleanTextMonitorPointWidget;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;
import javax.swing.BorderFactory;

/**
 * This panel displays the current signal path settings as reported by the IFProc. Note that this
 * information is used and displayed both in normal and debugging operations.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @version 
 * @since 8.0.0
 */
public class SignalPathPanel extends DevicePanel {

    public SignalPathPanel(Logger logger, DevicePM dPM) {
        super(logger, dPM);
        buildPanel();
    }

    protected void buildPanel() {
      setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder
              ("Signal Paths"), BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 1;
        c.gridy = 0;
        c.anchor=GridBagConstraints.WEST;
        addLeftEastLabel("Gain A:", c);
        add(new BooleanTextMonitorPointWidget(logger, 
                devicePM.getMonitorPointPM(IcdMonitorPoints.FILTER_A_HIGH),
                "Low filter", "High Filter"), c);
        c.gridy++;
        addLeftEastLabel("Gain B:", c);
        add(new BooleanTextMonitorPointWidget(logger, 
                devicePM.getMonitorPointPM(IcdMonitorPoints.FILTER_B_HIGH),
                "Low filter", "High Filter"), c);
        c.gridy++;
        addLeftEastLabel("Gain C:", c);
        add(new BooleanTextMonitorPointWidget(logger, 
                devicePM.getMonitorPointPM(IcdMonitorPoints.FILTER_C_HIGH),
                "Low filter", "High Filter"), c);
        c.gridy++;
        addLeftEastLabel("Gain D:", c);
        add(new BooleanTextMonitorPointWidget(logger, 
                devicePM.getMonitorPointPM(IcdMonitorPoints.FILTER_D_HIGH),
                "Low filter", "High Filter"), c);
        c.gridy++;
        addLeftEastLabel("Sideband A&B:", c);
        add(new BooleanTextMonitorPointWidget(logger, 
                devicePM.getMonitorPointPM(IcdMonitorPoints.SIDEBAND_AB_USB),
                "Lower Sideband", "Upper Sideband"), c);
        c.gridy++;
        addLeftEastLabel("Sideband C&D:", c);
        add(new BooleanTextMonitorPointWidget(logger, 
                devicePM.getMonitorPointPM(IcdMonitorPoints.SIDEBAND_CD_USB),
                "Lower Sideband", "Upper Sideband"), c);

    }

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
}
