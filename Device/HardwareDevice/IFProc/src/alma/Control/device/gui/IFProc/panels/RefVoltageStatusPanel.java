/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.IFProc.panels;

import alma.Control.device.gui.IFProc.IcdMonitorPoints;
import alma.Control.device.gui.IFProc.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.Status;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**This panel displays the error flags from the third byte of READ_STATUS. It is used as part of
 * the status panel.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @version 
 * @since 
 */

public class RefVoltageStatusPanel extends DevicePanel {

    public RefVoltageStatusPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildPanel();
    }
    protected void buildPanel() {
      setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder
              ("Reference Voltages"), BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        // Add all the points in a single column, starting at the top, with the labels on the left.
        c.gridx = 1;
        c.gridy = 0;

        addLeftLabel("USB Vref", c);
        add(new BooleanMonitorPointWidget(logger,
                  devicePM.getMonitorPointPM(IcdMonitorPoints.S0_VREF_GOOD),
                  Status.FAILURE, "Error: there is a problem with the USB Vref",
                  Status.GOOD, "USB Vref OK)"), c);
        c.gridy++;
        addLeftLabel("LSB Vref", c);
        add(new BooleanMonitorPointWidget(logger,
                  devicePM.getMonitorPointPM(IcdMonitorPoints.S1_VREF_GOOD),
                  Status.FAILURE, "Error: there is a problem with the LSB Vref",
                  Status.GOOD, "A Vref OK"), c);
        c.gridy=0;
        c.gridx++;
        //Adding a spacer to make things look good.
        add(new JLabel("  "), c);
        c.gridx+=2;
        addLeftLabel("A Vref", c);
        add(new BooleanMonitorPointWidget(logger,
                  devicePM.getMonitorPointPM(IcdMonitorPoints.A_VREF_GOOD),
                  Status.FAILURE, "Error: there is a problem with the A Vref",
                  Status.GOOD, "A Vref OK"), c);
        c.gridy++;
        addLeftLabel("B Vref", c);
        add(new BooleanMonitorPointWidget(logger,
                  devicePM.getMonitorPointPM(IcdMonitorPoints.B_VREF_GOOD),
                  Status.FAILURE, "Error: there is a problem with the B Vref",
                  Status.GOOD, "B Vref OK"), c);
        c.gridy++;
        addLeftLabel("C Vref", c);
        add(new BooleanMonitorPointWidget(logger,
                  devicePM.getMonitorPointPM(IcdMonitorPoints.C_VREF_GOOD),
                  Status.FAILURE, "Error: there is a problem with the C Vref",
                  Status.GOOD, "C Vref OK"), c);
        c.gridy++;
        addLeftLabel("D Vref", c);
        add(new BooleanMonitorPointWidget(logger,
                  devicePM.getMonitorPointPM(IcdMonitorPoints.D_VREF_GOOD),
                  Status.FAILURE, "Error: there is a problem with the D Vref",
                  Status.GOOD, "D Vref OK"), c);
        c.gridy++;
    }
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
}
