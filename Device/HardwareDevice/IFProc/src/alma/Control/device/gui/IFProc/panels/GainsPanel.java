/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.IFProc.panels;

import alma.Control.device.gui.IFProc.IcdMonitorPoints;
import alma.Control.device.gui.IFProc.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.FloatMonitorPointWidget;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;
import javax.swing.BorderFactory;

/**
 * This panel displays the current settings for the IFProc gains. It is displayed in both both
 * debugging and normal data, so it is a separate public panel.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @version 
 * @since 8.0.0
 */

public class GainsPanel extends DevicePanel {

    public GainsPanel(Logger logger, DevicePM dPM) {
        super(logger, dPM);
        buildPanel();
    }

    protected void buildPanel() {
      setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Gains"),
              BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        GridBagConstraints c = new GridBagConstraints();
        setLayout(new GridBagLayout());
        c.gridx = 1;
        c.gridy = 0;
        c.anchor=GridBagConstraints.WEST;
        addLeftEastLabel("Gain A:", c);
        add (new FloatMonitorPointWidget(logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.GA), true, 3), c);
        c.gridy++;
        addLeftEastLabel("Gain B:", c);
        add (new FloatMonitorPointWidget(logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.GB), true, 3), c);
        c.gridy++;
        addLeftEastLabel("Gain C:", c);
        add (new FloatMonitorPointWidget(logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.GC), true, 3), c);
        c.gridy++;
        addLeftEastLabel("Gain D:", c);
        add (new FloatMonitorPointWidget(logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.GD), true, 3), c);
    }

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
}
