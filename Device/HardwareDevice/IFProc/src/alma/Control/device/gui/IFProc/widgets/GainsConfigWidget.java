/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.IFProc.widgets;

import alma.Control.device.gui.IFProc.IcdControlPoints;
import alma.Control.device.gui.IFProc.presentationModels.DevicePM;
import alma.Control.device.gui.common.ControlPointPresentationModel;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.util.RoundingDoubleFormatter;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;

/**
 * This is a widget designed expressly to set the IFProc Gains (SET_GAINS, RCA 0x00181). It provides
 * a small panel that displays 4 gains entry fields and a control button. When the button is pressed
 * it will set all 4 of the given gains.
 * 
 * Notes:
 *   -The ICD requires that all 4 gains be set at the same time, so the GUI forces the user to
 *    do so.
 *    
 *   -The ICD states that this control command should never be sent twice in a 20TE period. A user
 *    could violate this by pressing the button non-stop. So the button locks for 1 second after
 *    each use.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class GainsConfigWidget extends DevicePanel {

    public GainsConfigWidget(Logger logger, DevicePM ifpDevicePM) {
        super(logger, ifpDevicePM);
        dPM=ifpDevicePM;
        setGainsControl=dPM.getControlPointPM(IcdControlPoints.GAINS);
        buildPanel();
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder
                ("Set Gains"), BorderFactory.createEmptyBorder(1, 1, 1, 1)));

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.gridx = 0;
        c.gridy = 0;
        //Add all the left hand labels
        addLabel ("Gain A", c);
        c.gridy++;
        addLabel ("Gain B", c);
        c.gridy++;
        addLabel ("Gain C", c);
        c.gridy++;
        addLabel ("Gain D", c);
        c.gridy++;
        c.gridwidth=3;
        setGains= new JButton("Set Gains");
        setGains.addActionListener(new setValuesAction());
        setGains.setMultiClickThreshhold(1000);
        setGains.setMargin(new Insets(0, 2, 0, 2));
        //Add the button to the disable controls button so that it properly responds to
        //a disable or enable controls command.
        setGainsControl.getContainingDevicePM().addControl(setGains);
        add(setGains, c);
        c.gridwidth=1;
        
        c.gridx++;
        c.gridy=0;
        gainA = new JFormattedTextField(new RoundingDoubleFormatter(0, 31.5, 0.5));
        gainA.setPreferredSize(new Dimension(40, 20));
        gainA.setValue(1.0);
        add (gainA, c);
        c.gridy++;
        gainB = new JFormattedTextField(new RoundingDoubleFormatter(0, 31.5, 0.5));
        gainB.setPreferredSize(new Dimension(40, 20));
        gainB.setValue(1.0);
        add (gainB, c);
        c.gridy++;
        gainC = new JFormattedTextField(new RoundingDoubleFormatter(0, 31.5, 0.5));
        gainC.setPreferredSize(new Dimension(40, 20));
        gainC.setValue(1.0);
        add (gainC, c);
        c.gridy++;
        gainD = new JFormattedTextField(new RoundingDoubleFormatter(0, 31.5, 0.5));
        gainD.setPreferredSize(new Dimension(40, 20));
        gainD.setValue(1.0);
        add (gainD, c);
        
        c.gridx++;
        c.gridy=0;
        addLabel ("dB", c);
        c.gridy++;
        addLabel ("dB", c);
        c.gridy++;
        addLabel ("dB", c);
        c.gridy++;
        addLabel ("dB", c);

        setVisible(true);
    }
    
    private void addLabel(String s, GridBagConstraints c){
        int tmp=c.anchor;
        c.insets = new Insets(5,0,0,0);
        c.anchor=GridBagConstraints.SOUTHWEST;
        add(new JLabel(s), c);
        c.anchor=tmp;
        c.insets = new Insets(0,0,0,0);
    }
    
    /**A simple action listener that launches the set values thread.
     */
    private class setValuesAction implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            Thread t = new Thread(new setValuesThread());
            dPM.getPluginContainerServices().getThreadFactory().newThread(t).start();
        }
    }
    
    /** The set values thread.
     *  This thread pools the values of the controls and sends the proper
     *  command to the SET_GAINS control. Then it sleeps 1 second. The control button is
     *  kept disabled during this time becasue the ICD specs a minimum wait of 20TEs between 
     *  control commands. Repeatdly pressing the button would excede this limit. It is possible,
     *  though unlikely, that the Control system enforces this limit. But, even if it does, it needs
     *  to be clear to the user that they can not press the button more often.
     *  
     *  This is, of course, all done in a thread so that the GUI does not lock up during the sleep.
     *
     */
    private class setValuesThread implements Runnable{
        public void run(){
            setGains.setEnabled(false);
            float[] controlValue = new float[4];
            controlValue[0]=((Double)gainA.getValue()).floatValue();
            controlValue[1]=((Double)gainB.getValue()).floatValue();
            controlValue[2]=((Double)gainC.getValue()).floatValue();
            controlValue[3]=((Double)gainD.getValue()).floatValue();
            setGainsControl.setValue(controlValue);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e1) {
                // This is not enough of an error for us to really care, but we go ahead and log it.
                logger.severe("SignalPathConfigWidget, setValuesAction Error: failed to sleep 1s" +
                              "after sending SET_VALUES.");
            }
            setGains.setEnabled(true);
            
        }
    }
    
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private DevicePM dPM;;
    private JFormattedTextField gainA;
    private JFormattedTextField gainB;
    private JFormattedTextField gainC;
    private JFormattedTextField gainD;
    private JButton setGains;
    private ControlPointPresentationModel<float[]> setGainsControl;
}

//
// O_o
