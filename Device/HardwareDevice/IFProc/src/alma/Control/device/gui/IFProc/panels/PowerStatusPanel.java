/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.IFProc.panels;

import alma.Control.device.gui.IFProc.IcdMonitorPoints;
import alma.Control.device.gui.IFProc.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.Status;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;

/**This panel displays all of the IFProc power summary alarms from READ_STATUS. It is displayed
 * at a couple locations in the IFProc GUI.
 *
 * @author David Hunter dhunter@nrao.edu
 * @version 
 * @since 
 */

public class PowerStatusPanel extends DevicePanel {

    public PowerStatusPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildPanel();
    }

    protected void buildPanel() {
      setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Power Status"),
              BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        // Add all the points in a single column, starting at the top, with the labels on the left.
        c.gridx = 1;
        c.gridy = 0;
        c.anchor=GridBagConstraints.WEST;
        
        addLeftEastLabel("Digital 1.2V", c);
        add(new BooleanMonitorPointWidget(logger,
                  devicePM.getMonitorPointPM(IcdMonitorPoints.DIGITAL_1DOT2_GOOD),
                  Status.FAILURE, "Error: the digital 1.2V supply is out of range",
                  Status.GOOD, "Digital 1.2V supply OK)"), c);
        c.gridy++;
        addLeftEastLabel("Digital 2.5V", c);
        add(new BooleanMonitorPointWidget(logger,
                  devicePM.getMonitorPointPM(IcdMonitorPoints.DIGITAL_2DOT5_GOOD),
                  Status.FAILURE, "Error: the digital 2.5V supply is out of range",
                  Status.GOOD, "Digital 2.5V supply OK)"), c);
        c.gridy++;
        addLeftEastLabel("Digital 3.3V", c);
        add(new BooleanMonitorPointWidget(logger,
                  devicePM.getMonitorPointPM(IcdMonitorPoints.DIGITAL_3DOT3_GOOD),
                  Status.FAILURE, "Error: the digital 3.3V supply is out of range",
                  Status.GOOD, "Digital 3.3V supply OK)"), c);
        c.gridy++;
        addLeftEastLabel("Digital 5V", c);
        add(new BooleanMonitorPointWidget(logger,
                  devicePM.getMonitorPointPM(IcdMonitorPoints.DIGITAL_5_GOOD),
                  Status.FAILURE, "Error: the digital 5V supply is out of range",
                  Status.GOOD, "Digital 5V supply OK)"), c);
        c.gridy++;
        addLeftEastLabel("Ethernet Voltage", c);
        add(new BooleanMonitorPointWidget(logger,
                  devicePM.getMonitorPointPM(IcdMonitorPoints.ETHER_CONT_VOLTAGE_GOOD),
                  Status.FAILURE, "Error: the ethernet power supply voltage is out of range",
                  Status.GOOD, "Ethernet power supply voltage OK)"), c);
        c.gridy++;
        addLeftEastLabel("Analog 5V", c);
        add(new BooleanMonitorPointWidget(logger,
                  devicePM.getMonitorPointPM(IcdMonitorPoints.ANALOG_5_GOOD),
                  Status.FAILURE, "Error: the analog 5v supply is out of range",
                  Status.GOOD, "Analog 5V supply OK)"), c);
        c.gridy++;
    }

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
}
