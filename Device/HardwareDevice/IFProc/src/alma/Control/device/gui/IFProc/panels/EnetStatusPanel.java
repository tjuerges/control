/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.IFProc.panels;

import alma.Control.device.gui.IFProc.IcdMonitorPoints;
import alma.Control.device.gui.IFProc.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.Status;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;

/**
 * This panel provides a readout of the Ethernet related status bits from the IFProc monitor
 * point READ_STATUS (Byte 0, RCA 0x00001). It is displayed in several places, so the information
 * is in a separate panel.
 *
 * @author David Hunter dhunter@nrao.edu
 * @version 
 * @since 
 */

public class EnetStatusPanel extends DevicePanel {

    public EnetStatusPanel(Logger logger, DevicePM dPM) {
        super(logger, dPM);
        buildPanel();
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder
                ("Ethernet Status"), BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 1;
        c.gridy = 0;
        c.anchor=GridBagConstraints.EAST;
        addLeftLabel("Transfer Active", c);
        add(new BooleanMonitorPointWidget(logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.DATA_TRANSFER_ACTIVE),
                Status.OFF, "Total power data is not being transfered over ethernet.",
                Status.ON, "Total power data transfer over ethernet in progress"), c);
        c.gridy++;
        addLeftLabel("Ethernet", c);
        add(new BooleanMonitorPointWidget(logger,
                  devicePM.getMonitorPointPM(IcdMonitorPoints.ETHERNET_CONNECTED),
                  Status.FAILURE, "Error: ethernet not connected",
                  Status.GOOD, "Ethernet connected :)"), c);
        c.gridy++;
        addLeftLabel("Router", c);
        add(new BooleanMonitorPointWidget(logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.ROUTER_NOT_FOUND),
                Status.GOOD, "The router has been found",
                Status.FAILURE, "Error: ethernet router not found"), c);
        c.gridy++;
        addLeftLabel("TCP", c);
        add(new BooleanMonitorPointWidget(logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.TCP_CONNECTED),
                Status.OFF, "TCP is not connected",
                Status.ON, "The TCP is connected"), c);
        c.gridy++;
        addLeftLabel("ARP Table", c);
        add(new BooleanMonitorPointWidget(logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.ARP_FULL),
                Status.GOOD, "The ARP table is not full",
                Status.FAILURE, "Error: The ARP table is full"), c);
        c.gridy++;
        addLeftLabel("TCP Cmd", c);
        add(new BooleanMonitorPointWidget(logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.TCP_DISCONN_CMD),
                Status.OFF, "The TCP was disconnected by a command",
                Status.ON, "No TCP disconnect command received"), c);
        c.gridy++;
        addLeftLabel("TCP Error", c);
        add(new BooleanMonitorPointWidget(logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.TCP_DISCONN_ERROR),
                Status.GOOD, "TCP has not been disconnected by an error",
                Status.FAILURE, "The TCP was disconnected by an error"), c);
        c.gridy++;
        addLeftLabel("Lost data", c);
        add(new BooleanMonitorPointWidget(logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.FIFO_FULL),
                Status.GOOD, "The FIFO is Fine",
                Status.FAILURE, "Error: The FIFO is full. Total Power data has been lost."), c);
    }

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
}
