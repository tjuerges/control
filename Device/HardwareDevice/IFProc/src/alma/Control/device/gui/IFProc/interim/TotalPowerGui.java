/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
package alma.Control.device.gui.IFProc.interim;

import alma.Control.device.gui.IFProc.presentationModels.DevicePM;
import alma.acs.component.client.AdvancedComponentClient;
import alma.acs.container.ContainerServices;
import alma.acs.logging.ClientLogManager;
import alma.acs.gui.standards.GuiStandards;

import java.awt.BorderLayout;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * Display Total Power data from one IFProc in one antenna.
 * 
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 7.0.0
 */
public class TotalPowerGui {

    /**
     * Collect the user's preferences for the GUI from the command line, build the GUI, then show it.
     * 
     * @param args identifying the Antenna and the IFProc the user is interested in.
     */
    public static void main(String[] args) {
        checkArgs(args);
                
        final TotalPowerGui gui = new TotalPowerGui(
                getAntennaFromArgs(args[ANTENNA_ARG_INDEX]),
                getIfProcFromArgs(args[IFPROC_ARG_INDEX]));
                
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                gui.showGui();
            }
        });
    }
    
    /**
     * Build a Total Power GUI.
     * @param antenna 
     * @param ifProcNum
     */
    private TotalPowerGui(String antenna, int ifProcNum) {
        initDevicePresentationModel(antenna, ifProcNum);
        buildGui(antenna, ifProcNum);
    }
    
    /**
     * Build the GUI, but do not make it visible yet.
     */
    private void buildGui(String antenna, int ifProcNum) {
        final String METHOD_NAME = "buildGui";
        GuiStandards.enforce();
        
        plotPanel = new TotalPowerPlotPanel(devicePM);
        controlPanel = new TotalPowerControlPanel(plotPanel, devicePM);
        frame = new JFrame("Average Total Power : " + antenna + " : IFProc : Polarization " + ifProcNum);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(controlPanel, BorderLayout.NORTH);
        frame.add(plotPanel, BorderLayout.CENTER);
    }
    
    /**
     * Ensure required command line arguments are supplied.
     * 
     * @param args to check.
     */
    private static void checkArgs(String[] args) {
        if (args.length !=  REQUIRED_ARGUMENTS) {
            showInsufficientArgumentsMessage();
        } 
    }

    /**
     * Convert the antenna command line argument to an upper case string.
     * 
     * @param antenna
     * @return an upper case copy of antenna.
     */
    private static String getAntennaFromArgs(String antenna) {
        return antenna.toUpperCase();
    }
    
    private static int getIfProcFromArgs(String ifp) {
        int ifProcNum = 0;
        try { 
            ifProcNum = Integer.parseInt(ifp);
        } catch (NumberFormatException nfe) {
            System.out.println("The second argument is not a number.");
            showInvalidIFprocNumberMessage();
        }
        if (ifProcNum != 0 && ifProcNum != 1) {
            System.out.println("The second argument must be either '0' or '1'");
            showInvalidIFprocNumberMessage();            
        }
        return ifProcNum;
    }

    /**
     * Build the plug-in device presentation model.
     */
    private void initDevicePresentationModel(String antenna, int ifProcNum) {
        final String METHOD_NAME = "initDevicePresentationModel";
        
        String clientName = getClass().getName();
        logger = ClientLogManager.getAcsLogManager().getLoggerForApplication(
                clientName, true);
        String managerLoc = System.getProperty("ACS.manager");
        if (managerLoc == null) {
            System.out.println(CLASS_NAME + METHOD_NAME + "(): Error getting ACS.manager property");
            JOptionPane.showMessageDialog(frame,
            	    "Unable to find ACS Manager in UNIX environment.  Has your shell environment been setup?  This GUI will now exit.",
            	    "UNIX Shell Environment Error!",
            	    JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        } else {
            managerLoc = managerLoc.trim();
        }
        
        AdvancedComponentClient client = null;
        try {
            client = new AdvancedComponentClient(logger, managerLoc, clientName);
        } catch (Exception e) {
            System.out.println(CLASS_NAME + METHOD_NAME + "(): Error getting client.");
            JOptionPane.showMessageDialog(frame,
            	    "Unable to connect to ACS Manager.  Is ACS running?  This GUI will now exit.",
            	    "ACS Manager Error!",
            	    JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
            System.exit(-1);
        }
        
        ContainerServices container = client.getContainerServices();
        Properties properties = new Properties();


        devicePM = new DevicePM(
                antenna, 
                new StandAloneContainerServices(container, properties),
                ifProcNum);
    }

    /**
     * Make GUI visible.  Run this methods from a GUI thread.
     */
    private void showGui() {
        final String METHOD_NAME = "showGui";
        frame.pack();
        frame.setVisible(true);
    }
    
    private static void showInsufficientArgumentsMessage() {
        System.out.println("Insufficient arguments.");
        showUseMessage();
    }

    private static void showInvalidIFprocNumberMessage() {
        System.out.println("The second argument must be a single digit, either '0' or a '1'.");
        showUseMessage();
    }

    /**
     * Show program use information and exit.
     */
    private static void showUseMessage() {
        System.out.println("");
        System.out.println("use: TotalPowerGui antenna IFProc");
        System.out.println("     antenna  = one antenna name");
        System.out.println("     IFProc   = a '0' or '1'");
        System.out.println();
        System.out.println("example: TotalPowerGui DA41 0");
        System.exit(INSUFFICIENT_ARGUMENTS);
    }
    
    private static final int ANTENNA_ARG_INDEX = 0;
    private static final String CLASS_NAME = "TotalPowerGui";
    private static final int IFPROC_ARG_INDEX = 1;
    private static final int INSUFFICIENT_ARGUMENTS=-1;
    private static final int REQUIRED_ARGUMENTS=2;

    private TotalPowerControlPanel controlPanel;
    private DevicePM devicePM;
    private static JFrame frame;
    private Logger logger;
    private TotalPowerPlotPanel plotPanel;
}

//
// O_o
