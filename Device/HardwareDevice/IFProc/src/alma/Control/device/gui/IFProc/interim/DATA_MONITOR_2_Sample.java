/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.IFProc.interim;

import org.omg.CORBA.LongHolder;

/**
 * Data from a single read from the GET_DATA_MONITOR_2 monitor point
 * 
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 7.0.0
 */
public class DATA_MONITOR_2_Sample {
    
    public DATA_MONITOR_2_Sample(
            LongHolder aSampleTime,
            float[] someValues) {
        
        this.sampleTime = aSampleTime;
        this.values = someValues;
    }
    
    public LongHolder getSampleTime() { return sampleTime; }
    public float[] getValues() { return values; }
    
    // The time the monitor point was read.
    private LongHolder sampleTime; 
    // The values read from the monitor point.
    //   average Volt total power data in array elements [0:3]
    //   average dBm total power data in array elements [4:7]
    private float[] values;
}
//
// O_o
