/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.IFProc.panels;

import alma.Control.device.gui.IFProc.IcdMonitorPoints;
import alma.Control.device.gui.IFProc.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.ByteSeqMonitorPointWidget;
import alma.Control.device.gui.common.widgets.FloatMonitorPointWidget;
import alma.Control.device.gui.common.widgets.IntegerMonitorPointWidget;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JTextArea;

/** 
 * This panel displays the current IFProc Ethernet TCP/IP setup. It is a separate panel because this data is
 * displayed in both the Ethernet status tab and the Ethernet configuration tab.
 *
 * @author David Hunter dhunter@nrao.edu
 * @version 
 * @since 
 */

public class EnetSettingsPanel extends DevicePanel {

    public EnetSettingsPanel(Logger logger, DevicePM dPM) {
        super(logger, dPM);
        devicePM=dPM;
        buildPanel();
    }

    protected void buildPanel() {
      setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Current" +
                               " Ethernet Setup"), BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth=2;
        c.anchor=GridBagConstraints.CENTER;
        //This is very non-intuitive behavior, so we give the user a warning.
        JTextArea notice = new JTextArea ("Please note that when the ethernet setup is changed\n" +
                "the readback will not update it until an Initalize\n" +
                "TCP Connection command is issued");
        notice.setEditable(false);
        add(notice , c);
        c.gridy++;
        c.gridx++;
        c.gridwidth=1;
        c.anchor=GridBagConstraints.WEST;
        addLeftEastLabel("Module IP: ", c);
        add(new ByteSeqMonitorPointWidget(logger,
                  devicePM.getMonitorPointPM(IcdMonitorPoints.MODULE_IP_ADDR), ".", false, false), c);
        c.gridy++;
        addLeftEastLabel("Destination IP: ", c);
        add(new ByteSeqMonitorPointWidget(logger,
                  devicePM.getMonitorPointPM(IcdMonitorPoints.DEST_IP_ADDR), ".", false, false), c);
        c.gridy++;
        addLeftEastLabel("Netmask: ", c);
        add(new ByteSeqMonitorPointWidget(logger,
                  devicePM.getMonitorPointPM(IcdMonitorPoints.MODULE_NETMASK), ".", false, false), c);
        c.gridy++;
        addLeftEastLabel("Gateway: ", c);
        add(new ByteSeqMonitorPointWidget(logger,
                  devicePM.getMonitorPointPM(IcdMonitorPoints.MODULE_GATEWAY), ".", false, false), c);
        c.gridy++;
        addLeftEastLabel("TCP Port: ", c);
        add(new IntegerMonitorPointWidget(logger,
                  devicePM.getMonitorPointPM(IcdMonitorPoints.TCP_PORT), false, false), c);
        c.gridy++;
        addLeftEastLabel("MAC: ", c);
        add(new ByteSeqMonitorPointWidget(logger,
                  devicePM.getMonitorPointPM(IcdMonitorPoints.ETHERNET_MAC), " ", true, false), c);
        c.gridy++;
        addLeftEastLabel("FPGA FIFO Depth: ", c);
        add(new IntegerMonitorPointWidget(logger,
                 devicePM.getMonitorPointPM(IcdMonitorPoints.FPGA_FIFO_DEPTH)), c);
        c.gridy++;
        addLeftEastLabel("External FIFO Depth: ", c);
        add(new IntegerMonitorPointWidget(logger,
                  devicePM.getMonitorPointPM(IcdMonitorPoints.EXTERNAL_FIFO_DEPTH)), c);
        c.gridy++;
        addLeftEastLabel("BDB Per Packet: ", c);
        add(new IntegerMonitorPointWidget(logger,
                  devicePM.getMonitorPointPM(IcdMonitorPoints.BDB_PER_PACKET)), c);
        c.gridy++;
        addLeftEastLabel("Max reads time: ", c);
        add(new FloatMonitorPointWidget(logger,
                 devicePM.getMonitorPointPM(IcdMonitorPoints.MAX_TIME_SINGLE_PACKET), true, 3), c);
        c.gridy++;
        addLeftEastLabel("Max packet time: ", c);
        add(new FloatMonitorPointWidget(logger,
             devicePM.getMonitorPointPM(IcdMonitorPoints.MAX_ETHER_TRANSMIT_AND_ETHER_HANDLING),
             true, 3), c);
    }
    
    DevicePM devicePM;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
}
