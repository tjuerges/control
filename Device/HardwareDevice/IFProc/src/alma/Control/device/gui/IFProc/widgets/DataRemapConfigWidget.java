/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.IFProc.widgets;
import alma.Control.device.gui.IFProc.IcdControlPoints;
import alma.Control.device.gui.IFProc.presentationModels.DevicePM;
import alma.Control.device.gui.common.ControlPointPresentationModel;
import alma.Control.device.gui.common.panels.DevicePanel;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized in panels and tabs to group related data together.
 * 
 * This class contains a template for a single data pane
 * TODO: Update this comment to reflect what the pane does.
 * 
 * Tabbed panels are used to group related data together and separate unrelated data. These are created as
 * required for each GUI.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class DataRemapConfigWidget extends DevicePanel {

    public DataRemapConfigWidget(Logger logger, DevicePM ifpDevicePM) {
        super(logger, ifpDevicePM);
        dPM=ifpDevicePM;
        setDataRemapControl=dPM.getControlPointPM(IcdControlPoints.DATA_REMAP);
        buildPanel();
    }
    protected void buildPanel() {
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder
                ("Set Data Remap"), BorderFactory.createEmptyBorder(1, 1, 1, 1)));

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.gridx=1;
        c.gridy=0;
        c.gridwidth=6;
        addLabel ("Select Digitizers:", c);
        c.gridwidth=1;
        c.gridy++;
        //Add all the left hand labels
        addLabel("  0", c);
        c.gridx++;
        addLabel("  1", c);
        c.gridx++;
        addLabel("  2", c);
        c.gridx++;
        addLabel("  3", c);
        c.gridx++;
        addLabel("  4", c);
        c.gridx++;
        addLabel("  5", c);

        c.gridx=0;
        c.gridy++;
        addLeftEastLabel ("Ch A", c);
        aGroup = new ButtonGroup();
        c.gridx++;
        a0 = new JRadioButton();
        aGroup.add(a0);
        add(a0, c);
        c.gridx++;
        a1 = new JRadioButton();
        aGroup.add(a1);
        add(a1, c);
        c.gridx++;
        a2 = new JRadioButton();
        aGroup.add(a2);
        add(a2, c);
        c.gridx++;
        a3 = new JRadioButton();
        aGroup.add(a3);
        add(a3, c);
        c.gridx++;
        a4 = new JRadioButton();
        aGroup.add(a4);
        add(a4, c);
        c.gridx++;
        a5 = new JRadioButton();
        aGroup.add(a5);
        add(a5, c);
        a3.setSelected(true);
        
        
        c.gridy++;
        c.gridx=0;
        addLeftEastLabel ("Ch B", c);
        bGroup = new ButtonGroup();
        c.gridx++;
        b0 = new JRadioButton();
        bGroup.add(b0);
        add(b0, c);
        c.gridx++;
        b1 = new JRadioButton();
        bGroup.add(b1);
        add(b1, c);
        c.gridx++;
        b2 = new JRadioButton();
        bGroup.add(b2);
        add(b2, c);
        c.gridx++;
        b3 = new JRadioButton();
        bGroup.add(b3);
        add(b3, c);
        c.gridx++;
        b4 = new JRadioButton();
        bGroup.add(b4);
        add(b4, c);
        c.gridx++;
        b5 = new JRadioButton();
        bGroup.add(b5);
        add(b5, c);
        b2.setSelected(true);
        
        c.gridy++;
        c.gridx=0;
        addLeftEastLabel ("Ch C", c);
        cGroup = new ButtonGroup();
        c.gridx++;
        c0 = new JRadioButton();
        cGroup.add(c0);
        add(c0, c);
        c.gridx++;
        c1 = new JRadioButton();
        cGroup.add(c1);
        add(c1, c);
        c.gridx++;
        c2 = new JRadioButton();
        cGroup.add(c2);
        add(c2, c);
        c.gridx++;
        c3 = new JRadioButton();
        cGroup.add(c3);
        add(c3, c);
        c.gridx++;
        c4 = new JRadioButton();
        cGroup.add(c4);
        add(c4, c);
        c.gridx++;
        c5 = new JRadioButton();
        cGroup.add(c5);
        add(c5, c);
        c1.setSelected(true);
        
        c.gridy++;
        c.gridx=0;
        addLeftEastLabel ("Ch D", c);
        dGroup = new ButtonGroup();
        c.gridx++;
        d0 = new JRadioButton();
        dGroup.add(d0);
        add(d0, c);
        c.gridx++;
        d1 = new JRadioButton();
        dGroup.add(d1);
        add(d1, c);
        c.gridx++;
        d2 = new JRadioButton();
        dGroup.add(d2);
        add(d2, c);
        c.gridx++;
        d3 = new JRadioButton();
        dGroup.add(d3);
        add(d3, c);
        c.gridx++;
        d4 = new JRadioButton();
        dGroup.add(d4);
        add(d4, c);
        c.gridx++;
        d5 = new JRadioButton();
        dGroup.add(d5);
        add(d5, c);
        d0.setSelected(true);
        
        c.gridy++;
        c.gridx=0;
        addLeftEastLabel ("USB", c);
        usbGroup = new ButtonGroup();
        c.gridx++;
        usb0 = new JRadioButton();
        usbGroup.add(usb0);
        add(usb0, c);
        c.gridx++;
        usb1 = new JRadioButton();
        usbGroup.add(usb1);
        add(usb1, c);
        c.gridx++;
        usb2 = new JRadioButton();
        usbGroup.add(usb2);
        add(usb2, c);
        c.gridx++;
        usb3 = new JRadioButton();
        usbGroup.add(usb3);
        add(usb3, c);
        c.gridx++;
        usb4 = new JRadioButton();
        usbGroup.add(usb4);
        add(usb4, c);
        c.gridx++;
        usb5 = new JRadioButton();
        usbGroup.add(usb5);
        add(usb5, c);
        usb5.setSelected(true);
        
        c.gridy++;
        c.gridx=0;
        addLeftEastLabel ("LSB", c);
        lsbGroup = new ButtonGroup();
        c.gridx++;
        lsb0 = new JRadioButton();
        lsbGroup.add(lsb0);
        add(lsb0, c);
        c.gridx++;
        lsb1 = new JRadioButton();
        lsbGroup.add(lsb1);
        add(lsb1, c);
        c.gridx++;
        lsb2 = new JRadioButton();
        lsbGroup.add(lsb2);
        add(lsb2, c);
        c.gridx++;
        lsb3 = new JRadioButton();
        lsbGroup.add(lsb3);
        add(lsb3, c);
        c.gridx++;
        lsb4 = new JRadioButton();
        lsbGroup.add(lsb4);
        add(lsb4, c);
        c.gridx++;
        lsb5 = new JRadioButton();
        lsbGroup.add(lsb5);
        add(lsb5, c);
        lsb4.setSelected(true);
        
        c.gridy++;
        c.gridx=0;
        c.gridwidth=6;
        setDataRemap=new JButton("Set Data Remap");
        setDataRemap.setMultiClickThreshhold(100);
        setDataRemap.addActionListener(new setValuesAction());
        setDataRemap.setMargin(new Insets(0, 2, 0, 2));
        setDataRemapControl.getContainingDevicePM().addControl(setDataRemap);
        add(setDataRemap, c);
        c.gridwidth=1;
        c.gridx++;
        c.gridy=0;
        
        setVisible(true);
    }
    
    private void addLabel(String s, GridBagConstraints c){
        int tmp=c.anchor;
        c.insets = new Insets(5,0,0,0);
        c.anchor=GridBagConstraints.SOUTHWEST;
        add(new JLabel(s), c);
        c.anchor=tmp;
        c.insets = new Insets(0,0,0,0);
    }
    
    /**A simple action listener that launches the set values thread.
     */
    private class setValuesAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Thread t = new Thread(new setValuesThread());
            dPM.getPluginContainerServices().getThreadFactory().newThread(t).start();
        }
    }
    /** The set values thread.
     *  This thread pools the values of the controls and sends the proper
     *  command to the SET_SIGNAL_PATHS control. Then it sleeps 1 second. The control button is
     *  kept disabled during this time because the ICD specs a minimum wait of 20TEs between 
     *  control commands. Repeatedly pressing the button would exceed this limit. It is possible,
     *  though unlikely, that the Control system enforces this limit. But, even if it does, it needs
     *  to be clear to the user that they can not press the button more often.
     *  
     *  This is, of course, all done in a thread so that the GUI does not lock up during the sleep.
     *
     */
    private class setValuesThread implements Runnable{
        public void run(){
            int[] data={0, 0, 0, 0, 0, 0};
            if (a0.isSelected())
                data[2]=0;
            else if (a1.isSelected())
                data[2]=1;
            else if (a2.isSelected())
                data[2]=2;
            else if (a3.isSelected())
                data[2]=3;
            else if (a4.isSelected())
                data[2]=4;
            else
                data[2]=5;
            
            if (b0.isSelected())
                data[3]=0;
            else  if (b1.isSelected())
                data[3]=1;
            else  if (b2.isSelected())
                data[3]=2;
            else  if (b3.isSelected())
                data[3]=3;
            else  if (b4.isSelected())
                data[3]=4;
            else
                data[3]=5;

            if (c0.isSelected())
                data[4]=0;
            else if (c1.isSelected())
                data[4]=1;
            else if (c2.isSelected())
                data[4]=2;
            else if (c3.isSelected())
                data[4]=3;
            else if (c4.isSelected())
                data[4]=4;
            else
                data[4]=5;
            
            if (d0.isSelected())
                data[5]=0;
            else if (d1.isSelected())
                data[5]=1;
            else if (d2.isSelected())
                data[5]=2;
            else if (d3.isSelected())
                data[5]=3;
            else if (d4.isSelected())
                data[5]=4;
            else
                data[5]=5;
            
            if (lsb0.isSelected())
                data[1]=0;
            else if (lsb0.isSelected())
                data[1]=1;
            else if (lsb2.isSelected())
                data[1]=2;
            else if (lsb3.isSelected())
                data[1]=3;
            else if (lsb4.isSelected())
                data[1]=4;
            else
                data[1]=5;
            
            if (usb0.isSelected())
                data[0]=0;
            else if (usb1.isSelected())
                data[0]=1;
            else if (usb2.isSelected())
                data[0]=2;
            else if (usb3.isSelected())
                data[0]=3;
            else if (usb4.isSelected())
                data[0]=4;
            else
                data[0]=5;
            setDataRemapControl.setValue(data);
        }
    }
    
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    
    private JRadioButton a0;
    private JRadioButton a1;
    private JRadioButton a2;
    private JRadioButton a3;
    private JRadioButton a4;
    private JRadioButton a5;
    private ButtonGroup aGroup;
    private JRadioButton b0;
    private JRadioButton b1;
    private JRadioButton b2;
    private JRadioButton b3;
    private JRadioButton b4;
    private JRadioButton b5;
    private ButtonGroup bGroup;
    private JRadioButton c0;
    private JRadioButton c1;
    private JRadioButton c2;
    private JRadioButton c3;
    private JRadioButton c4;
    private JRadioButton c5;
    private ButtonGroup cGroup;
    private JRadioButton d0;
    private JRadioButton d1;
    private JRadioButton d2;
    private JRadioButton d3;
    private JRadioButton d4;
    private JRadioButton d5;
    private ButtonGroup dGroup;
    private DevicePM dPM;
    private JRadioButton lsb0;
    private JRadioButton lsb1;
    private JRadioButton lsb2;
    private JRadioButton lsb3;
    private JRadioButton lsb4;
    private JRadioButton lsb5;
    private ButtonGroup lsbGroup;
    private JRadioButton usb0;
    private JRadioButton usb1;
    private JRadioButton usb2;
    private JRadioButton usb3;
    private JRadioButton usb4;
    private JRadioButton usb5;
    private ButtonGroup usbGroup;
    private JButton setDataRemap;
    private ControlPointPresentationModel<int[]> setDataRemapControl;
}

//
// O_o
