/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
package alma.Control.device.gui.IFProc.interim;

/**
 * Enumerations for types of charts.
 * 
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 7.0.0
 */
public enum ChartType {

    COMBINED(0, "combined"),
    SPLIT(1, "split");

    /**
     * Get a ChartType from an index.
     *
     * TODO: find a better way to do this.
     *
     * @return ChartType value for index.
     */
    public static ChartType index(int i) {
        if (0 == i)
            return COMBINED;
        else
            return SPLIT;
    }

    /**
     * Get the ord of the ChartType.  This local control of the mapping of enum values to ord values.
     *
     * TODO: find a better way to do this.
     *
     * @return the locally stored ord.
     */
    public int ord() {
        return ord;
    }

    /**
     * Overriding the enum toString because it uses a private variable to return the string.
     *
     * @return the locally stored text to return.
     */
    @Override
    public String toString() {
        return text;
    }

    private ChartType(int o, String t) {
        ord = o;
        text = t;
    }

    private int ord;
    private String text;
}

//
// O_o
