/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.IFProc.panels;

import alma.Control.device.gui.IFProc.IcdControlPoints;
import alma.Control.device.gui.IFProc.IcdMonitorPoints;
import alma.Control.device.gui.IFProc.presentationModels.DevicePM;
import alma.Control.device.gui.IFProc.widgets.GainsConfigWidget;
import alma.Control.device.gui.IFProc.widgets.SignalPathConfigWidget;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.AlmaTimeMonitorPointWidget;
import alma.Control.device.gui.common.widgets.FloatMonitorPointWidget;
import alma.Control.device.gui.common.widgets.IntegerControlPointWidget;
import alma.Control.device.gui.common.widgets.IntegerMonitorPointWidget;
import alma.Control.device.gui.common.widgets.BooleanControlPointWidget;
import alma.Control.device.gui.common.widgets.util.PowerOfTwoFormatter;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;

/**This panel displays both monitors and controls for RF related IFProc data. The controls are
 * intended for debugging only, so this panel should be placed accordingly.
 * 
 * The monitor data includes:
 *   -Simple average total power data readouts
 *   -IFProc internal time (used for time-stamping the total power data)
 *   -IFProc 48ms length
 *   -Signal path configuration
 * 
 * The controls include
 *   -Set average total power data sample size
 *   -Clear timing errors
 *   -Set gains
 *   -Set signal paths
 *    
 * Notes:
 * 
 * -The the gains and signal paths are also displayed elsewhere because they are needed in
 * both debugging and operations. The average total power data is currently only displayed here
 * because the stand-alone total power GUI does a better job of displaying the information.
 * 
 * -Caution: this panel may be harmful or fatal if swallowed.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @version $Id$
 * @since 8.0.0
 */

public class DebugRfPanel extends DevicePanel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;

    public DebugRfPanel(Logger logger, DevicePM dPM) {
        super(logger, dPM);
        buildSubPanels(logger, dPM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM dPM){
        dataPanel = new DataPanel(logger, dPM);
        timingPanel = new TimingPanel(logger, dPM);
        gainsConfigWidget = new GainsConfigWidget(logger, dPM);
        gainsPanel = new GainsPanel(logger, dPM);
        signalPathConfigWidget = new SignalPathConfigWidget(logger, dPM);
        signalPathPanel = new SignalPathPanel(logger, dPM);
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;

        add(dataPanel, c);
        c.gridy++;
        c.gridwidth=2;
        add(timingPanel, c);
        c.gridy++;
        add(signalPathPanel, c);
        c.gridx++;
        c.gridy=0;
        c.gridwidth=1;
        add(gainsPanel, c);
        c.gridx++;
        add(gainsConfigWidget, c);
        c.gridy++;
        c.gridheight=2;
        add(signalPathConfigWidget,c);

        setVisible(true);
    }

    /*
     * This panel provides readouts of the average total power over CAN data, along with access to
     * the control to set the sample size.
     */
    private class DataPanel extends DevicePanel {
        public DataPanel(Logger logger, DevicePM dPM) {
            super(logger, dPM);
            buildPanel();
        }

        protected void buildPanel() {
          setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder
                  ("Average Total Power Data"), BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.gridx = 1;
            c.gridy = 0;
            c.anchor=GridBagConstraints.WEST;
            addLeftEastLabel("USB:", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TOTAL_POWER_SIDEBAND_0), true, 6), c);
            c.gridy++;
            addLeftEastLabel("LSB:", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TOTAL_POWER_SIDEBAND_1), true, 6), c);
            c.gridy++;
            addLeftEastLabel("A:", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TOTAL_POWER_A), true, 6), c);
            c.gridy++;
            addLeftEastLabel("B:", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TOTAL_POWER_B), true, 6), c);
            c.gridy++;
            addLeftEastLabel("C:", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TOTAL_POWER_C), true, 6), c);
            c.gridy++;
            addLeftEastLabel("D:", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TOTAL_POWER_D), true, 6), c);
            c.gridy++;
            addLeftEastLabel("Samples Averaged: ", c);
            add (new IntegerMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DATA_AVE_LENGTH)), c);
            c.gridwidth=2;
            c.anchor=GridBagConstraints.CENTER;
            c.gridx--;
            c.gridy++;
            add (new IntegerControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.AVERAGE_LENGTH), 1, 512,
                    new PowerOfTwoFormatter(1, 512),"Set sample size"),c);
        }

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }

    /*
     * This panel provides access to the IFProc timing related monitors and controls. Note that a
     * set time control is not provided. This is because correctly setting the time in the IFProc
     * is a real time operation of which should be carried out by software. A sync time butto
     * could be added here, but there is no known need for one. And one would risk interfering
     * with operations.
     */
    private class TimingPanel extends DevicePanel {

        public TimingPanel(Logger logger, DevicePM dPM) {
            super(logger, dPM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Timing"),
                    BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.fill = GridBagConstraints.NONE;
            c.anchor = GridBagConstraints.WEST;
            c.gridx = 1;
            c.gridy = 0;

            addLeftEastLabel("IFProc Time:", c);
            add(new AlmaTimeMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.ALMA_TIME)), c);
            c.gridy++;
            addLeftEastLabel("48ms Length:", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.LENGTH_48MS), true, 7), c);
            c.gridy++;
            c.gridwidth=2;
            c.gridx--;
            add(new BooleanControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.CLEAR_TIMING_ERROR),
            "Clear Timing Errors"), c);
        }

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }

    DataPanel dataPanel;
    TimingPanel timingPanel;
    GainsConfigWidget gainsConfigWidget;
    GainsPanel gainsPanel;
    SignalPathConfigWidget signalPathConfigWidget;
    SignalPathPanel signalPathPanel;
}

//
// O_o
