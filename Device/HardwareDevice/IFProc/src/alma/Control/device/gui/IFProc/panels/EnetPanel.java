/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.IFProc.panels;

import alma.Control.device.gui.IFProc.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;
import javax.swing.BorderFactory;

/**
 * This panel provides a readout of the status of the IFProc Ethernet. Both the status bits from
 * READ_STATUS (Byte 0, RCA 0x0001) and the settings of the Ethernet (IP, gateway, etc). All of this
 * information is in public sub-panels since it is also displayed elsewhere. As a result, this is
 * class just creates and displays instances of those sub-panels.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @version $Id$
 * @since 
 */

public class EnetPanel extends DevicePanel {

    public EnetPanel(Logger logger, DevicePM dPM) {
        super(logger, dPM);
        buildSubPanels(logger, dPM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM dPM){
        enetStatusPanel = new EnetStatusPanel(logger, dPM);
        enetSettingsPanel = new EnetSettingsPanel(logger, dPM);
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;
        add(enetStatusPanel, c);
        c.gridx = 1;
        add(enetSettingsPanel, c);
        setVisible(true);
    }

    private EnetStatusPanel enetStatusPanel;
    private EnetSettingsPanel enetSettingsPanel;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
}

//
// O_o
