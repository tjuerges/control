/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package alma.Control.device.gui.IFProc.interim;

/**
 * Enumerations for types of chart units.
 * 
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 7.0.0
 */
public enum ChartUnits {

    DBM(0, "dBm"),
    VOLTS(1, "Volts");

    /**
     * Get a ChartType from an index.
     *
     * TODO: find a better way to do this.
     *
     * @return ChartType value for index.
     */
    public static ChartUnits index(int i) {
        if (0 == i)
            return DBM;
        else
            return VOLTS;
    }

    /**
     * Get the ord of the enum.  This local control of the mapping of enum values to ord values.
     *
     * TODO: find a better way to do this.
     * 
     * @return the locally stored ord.
     */
    public int ord() {
        return ord;
    }

    /**
     * Overriding the enum toString because it uses a private variable to return the string.
     *
     * @return the locally stored text to return.
     */
    @Override
    public String toString() {
        return text;
    }

    private ChartUnits(int o, String t) {
        ord = o;
        text = t;
    }

    private int ord;
    private String text;
}

//
// O_o
