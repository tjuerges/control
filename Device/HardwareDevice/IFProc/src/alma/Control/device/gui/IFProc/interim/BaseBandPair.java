/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
package alma.Control.device.gui.IFProc.interim;

import alma.Control.device.gui.IFProc.IdlMonitorPoints;
import alma.Control.device.gui.IFProc.IcdMonitorPoints;
import alma.Control.device.gui.IFProc.presentationModels.DevicePM;
import alma.Control.device.gui.common.IcdMonitorPointPresentationModel;
import alma.Control.device.gui.common.PresentationModelChangeEvent;
import alma.Control.device.gui.common.PresentationModelChangeListener;

import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.TracePoint2D;
import info.monitorenter.gui.chart.traces.Trace2DLtd;

import java.awt.Color;
import java.util.Date;
import java.util.Random;
import java.util.Vector;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;

/**
 * Data and methods needed to poll and plot data for an IFProc Base Band Pair.
 * 
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 7.0.0
 */
public class BaseBandPair implements PresentationModelChangeListener {
    
    final static int DEFAULT_DURATION_IN_SECONDS = 30;
    final static int SAMPLES_PER_SECOND = 4;

    /**
     * Create an instance with the given parameters and reasonable defaults.
     * 
     * @param aName identifies this base band pair.
     * @param aColor identifies this base band pair plot line.
     * @param aLogger to log messages to.
     */
    public BaseBandPair(int aNumber, Color aColor, DevicePM aDevicePM) {
        logger = aDevicePM.getLogger();
        color = aColor;
        devicePM = aDevicePM;
        name = "BBP" + aNumber;
        number = aNumber;

        durationInS = DEFAULT_DURATION_IN_SECONDS;
        enabled = false;
        
        dBmSampleBuffer = new Vector<Sample>();
        voltSampleBuffer = new Vector<Sample>();
        
        moveDataBetweenThreads = new Runnable() {
            public void run() {
                addSamplesFromBuffersToTraces();
            }
        };

        initMonitorPoints();
        initTraces();
        
        registerAsListener();
    }

    public Color getColor() {
        return color;
    }

    public ITrace2D getDBmTrace() {
        return dBmTrace;
    }

    public int getDuration() {
        return durationInS;
    }

    public boolean getEnabled() {
        return enabled;
    }

    public String getName() {
        return name;
    }

    public int getNumber() {
        return number;
    }

    public ITrace2D getVoltTrace() {
        return voltTrace;
    }

    /**
     * Receive events from GET_DATA_MONITOR_2_MonitorPointPresenationModel, extract data for this 
     * base band pair, and pass it along to the chart.
     */
    @Override
    public void handlePresentationModelChangeEvent(PresentationModelChangeEvent<?> event) {
        final String METHOD_NAME = "handlePresentationModelChangeEvent";
//        System.out.println("handlePresentationModelChangeEvent called for BBPr:" + number);
        DATA_MONITOR_2_Sample sample = (DATA_MONITOR_2_Sample) event.getValue();
//        System.out.println("Volt value: " + sample.getValues()[number]);
//        System.out.println("dBm value: " + sample.getValues()[number+4]);
        
        Date sampleTime = new Date(alma.acs.util.UTCUtility.utcOmgToJava(sample.getSampleTime().value));
        
        dBmSample = new Sample(sampleTime, (double)sample.getValues()[number+4]);
        voltSample = new Sample(sampleTime, (double)(sample.getValues()[number]));
        
        dBmSampleBuffer.add(dBmSample);
        voltSampleBuffer.add(voltSample);
        
        SwingUtilities.invokeLater(moveDataBetweenThreads);
    }

    /**
     * Sample both monitor points and add the samples to the appropriate traces.
     */
//    public void sampleMonitorPoints() {
//        final String METHOD_NAME = "sampleMonitorPoints";
//        dBmSample = sampleMonitorPoint(dBmMP);
//        voltSample = sampleMonitorPoint(voltMP);
//        
//        dBmSampleBuffer.add(dBmSample);
//        voltSampleBuffer.add(voltSample);
//        
//        SwingUtilities.invokeLater(moveDataBetweenThreads);
//    }

    /**
     * Set the duration in seconds of data stored in the trace for this base band pair.
     *
     * @param d
     *            is the new duration for data charts.
     */
    public void setDuration(final int d) {
        final String METHOD_NAME = "setDuration";
        durationInS = d;
        int samples = d * SAMPLES_PER_SECOND;
        dBmTrace.setMaxSize(samples);
        voltTrace.setMaxSize(samples);
    }

    /**
     * Enable or disable this base band pair.
     * 
     * @param b
     *            is the new enabled state.
     */
    public void setEnabled(final boolean b) {
        final String METHOD_NAME = "setEnabled";
        enabled = b;
    }

    /**
     * Add a sample to the dBm trace.
     * 
     * This may run on the Swing event dispatching thread, so no logging.
     * 
     * @param sample to add to the trace.
     */
    void addDBmSampleToTrace(final Sample sample) {
        dBmTrace.addPoint(new TracePoint2D(sample.getTimeAsDouble(), sample.getValue()));
    }

    /**
     * Add samples from the sample buffers to the trace.
     *
     * This will run on the Swing event dispatching thread, so no logging.
     */
    void addSamplesFromBuffersToTraces() {
        Sample dBmSample;
        while (!dBmSampleBuffer.isEmpty()) {
            dBmSample = dBmSampleBuffer.firstElement();
            addDBmSampleToTrace(dBmSample);
            dBmSampleBuffer.remove(0);
        }

        Sample voltSample;
        while (!voltSampleBuffer.isEmpty()) {
            voltSample = voltSampleBuffer.firstElement();
            addVoltSampleToTraces(voltSample);
            voltSampleBuffer.remove(0);
        }
    }

    /**
     * Add a sample to the Volt trace.
     *
     * This may run on the Swing event dispatching thread, so no logging.
     * 
     * @param sample to add to the trace.
     */
    void addVoltSampleToTraces(final Sample sample) {
    	// DEBUG
        // Because the IFProc simulator does not update it's internal time quickly enough for
        // our required sample rate, replace the time stamp from the call to getValueFromDevice
        // with the time now.
        voltTrace.addPoint(new TracePoint2D(sample.getTimeAsDouble(), sample.getValue()));
    }
    
    /**
     * The following two methods exist to support testing.
     */
    IcdMonitorPointPresentationModel<Float> getDBmMonitorPoint() {
        return dBmMP;
    }

    IcdMonitorPointPresentationModel<Float> getVoltMonitorPoint() {
        return voltMP;
    }

    /**
     * Initialize the private monitor points.
     */
    private void initMonitorPoints() {
        final String METHOD_NAME = "initMonitorPoints";
        IcdMonitorPoints mpD = null;
        IcdMonitorPoints mpV = null;

        switch (number) {
        case 0: {
            mpD = IcdMonitorPoints.TOTAL_POWER_A_DBM;
            mpV = IcdMonitorPoints.TOTAL_POWER_A;
            break;
        }
        case 1: {
            mpD = IcdMonitorPoints.TOTAL_POWER_B_DBM;
            mpV = IcdMonitorPoints.TOTAL_POWER_B;
            break;
        }
        case 2: {
            mpD = IcdMonitorPoints.TOTAL_POWER_C_DBM;
            mpV = IcdMonitorPoints.TOTAL_POWER_C;
            break;
        }
        case 3: {
            mpD = IcdMonitorPoints.TOTAL_POWER_D_DBM;
            mpV = IcdMonitorPoints.TOTAL_POWER_A;
            break;
        }
        default:
            logger.severe("BaseBandPair.initMonitorPoints(): invalid value for base band pair number!");
        }

        dBmMP = (IcdMonitorPointPresentationModel<Float>) devicePM.getMonitorPointPM(mpD);
        voltMP = (IcdMonitorPointPresentationModel<Float>) devicePM.getMonitorPointPM(mpV);
    }

    /**
     * Initialize the private traces.
     */
    private void initTraces() {
        final String METHOD_NAME = "initTraces";
        dBmTrace = new Trace2DLtd();
        dBmTrace.setColor(color);
        dBmTrace.setName("");
        voltTrace = new Trace2DLtd();
        voltTrace.setColor(color);
        voltTrace.setName("");
    }

    private void registerAsListener() {
        devicePM.getMonitorPointPM(IdlMonitorPoints.GET_DATA_MONITOR_2).addListener(this);        
    }

    /**
     * Sample the given monitor point for this base band pair.
     * 
     * @return a Sample from the Volt monitor point.
     */
    private Sample sampleMonitorPoint(IcdMonitorPointPresentationModel<Float> mp) {
        final String METHOD_NAME = "sampleMonitorPoint";
        Date sampleHolder = new Date();
        float sampleValue = mp.getValueFromDevice(sampleHolder);
        if (Float.isNaN(sampleValue)) {
            logger.severe(CLASS_NAME + "." + METHOD_NAME + "() read a NaN from " + name + " MP " + mp);
            sampleValue = 0.0f;
        }

        // TODO: Remove debug code.
        // DEBUG
        // Add a small random value to the sampled data.
        logger.warning(CLASS_NAME + "." + METHOD_NAME + "() added random number to sampleValue for testing.");
        sampleValue = sampleValue + (float) generator.nextGaussian() / 100;
        // DEBUG

        // TODO: Remove debug code.
        // DEBUG
        // Because the IFProc simulator does not update it's internal time quickly enough for 
        // our required sample rate, replace the time stamp from the call to getValueFromDevice 
        // with the time now.
        logger.warning(CLASS_NAME + "." + METHOD_NAME + "() replaced sampleTime for testing.");
        Date sampleTime = new Date();
        // Date sampleTime = new Date(alma.acs.util.UTCUtility.utcOmgToJava(sampleHolder.value));
        // DEBUG
        Sample sample = new Sample(sampleTime, sampleValue);
        return sample;
    }

    private static final String CLASS_NAME = "BaseBandPair";

    /**
     * Number of the base band pair.
     */
    private int number;

    /**
     * Color to display for this base band pair in a plot.
     */
    private Color color;

    /**
     * Monitor point for dBm data.
     */
    private IcdMonitorPointPresentationModel<Float> dBmMP;

    /**
     * Data sample to add to trace.
     */
    Sample dBmSample;

    /**
     * Sample buffers are used to hold samples before they are added to traces.
     * Vectors are used over ArrayLists because methods on Vectors are synchronized.  This 
     * is required because sample buffers are accessed from a polling thread and the Swing
     * event dispatcher thread. 
     */
    Vector<Sample> dBmSampleBuffer;

    /**
     * Trace for dBm samples for this base band pair.
     */
    private Trace2DLtd dBmTrace;

    /**
     * Duration in ms of data stored in trace.
     */
    private int durationInS;

    /**
     * Enabled state for base band pair.
     */
    private boolean enabled;

    /**
     * Local logger.
     */
    private Logger logger;

    /**
     * Name to display for this base band pair.
     */
    private String name;

    /**
     * The presentation mode for the device containing this base band pair.
     */
    private DevicePM devicePM;
    
    /**
     * Because it is necessary to move data between a polling thread and a Swing event dispatcher
     * thread, we need a runnable to hold the code that will run on the Swing event dispatcher.  
     * See the constructor for the implementation.
     */
    Runnable moveDataBetweenThreads;

    /**
     * Data sample to add to trace.
     */
    Sample voltSample;
    
    /**
     * Sample buffer vectors are used to hold samples before they are added to traces.
     * Vectors are used over ArrayLists because methods on Vectors are synchronized.  This 
     * is required because sample buffers are accessed from a polling thread and the Swing
     * event dispatcher thread. 
     */
    Vector<Sample> voltSampleBuffer;

    /**
     * Trace for Volt samples for this base band pair.
     */
    private Trace2DLtd voltTrace;

    /**
     * Monitor point for voltage data.
     */
    private IcdMonitorPointPresentationModel<Float> voltMP;

    // DEBUG
    // TODO: Remove after debugging is complete.
    private static Random generator = new Random();

}

//
// O_o
