/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.IFProc.presentationModels;

import alma.acs.logging.AcsLogger;
import alma.acs.logging.RepeatGuardLogger;
import alma.acs.time.TimeHelper;

import alma.Control.IFProcOperations;

import alma.Control.device.gui.IFProc.IcdMonitorPoints;
import alma.Control.device.gui.IFProc.interim.DATA_MONITOR_2_Sample;
import alma.Control.device.gui.common.DevicePresentationModel;
import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.IdlMonitorPointPresentationModel;
import alma.Control.device.gui.common.MonitorPointPresentationModel;
import alma.ControlExceptions.CAMBErrorEx;
import alma.ControlExceptions.INACTErrorEx;
import alma.common.log.ExcLog;

import org.omg.CORBA.LongHolder;

import java.lang.Math;
import java.math.BigInteger;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Track a read only property of a hardware device to be displayed in a UI.
 * Isolate the user interface from the implementation details of a hardware
 * device in the Control system.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id: GET_DATA_MONITOR_2_MonitorPointPresentationModel.java,v 1.2
 *          2010/02/12 02:06:54 srankin Exp $
 * @since ALMA 5.1.1
 */
public class GET_DATA_MONITOR_2_MonitorPointPresentationModel extends
        IdlMonitorPointPresentationModel<DATA_MONITOR_2_Sample> {
    
    public GET_DATA_MONITOR_2_MonitorPointPresentationModel(Logger logger, DevicePresentationModel container,
            IMonitorPoint monitorPoint) {
        super(logger, container, monitorPoint);
        AcsLogger acsLogger = AcsLogger.fromJdkLogger(logger, null);
        inactErrorExRepeatGuard = RepeatGuardLogger.createCounterBasedRepeatGuardLogger(acsLogger, 1);
        cambErrorExRepeatGuard = RepeatGuardLogger.createCounterBasedRepeatGuardLogger(acsLogger, 1);
        illegalArgumentExceptionRepeatGuard = RepeatGuardLogger.createCounterBasedRepeatGuardLogger(
                acsLogger, 1);
    }
    
    /**
     * Get the current value from the hardware device property this presentation
     * model represents.
     * 
     * @return the current value held by the hardware device.
     */
    @Override
    protected DATA_MONITOR_2_Sample getValueFromDevice(Date timeStamp) {
        // Note, this will slow down the first read, but not following reads.
        // This is a temporary hack to ensure we can read from the SERIAL_NUMBER
        // monitor point.
        if (serialNumber == null)
            getCalibrationData();
        
        LongHolder l = new LongHolder();
        // GET_DATA_MONITOR_2 returns Volt total power data
        float voltData[] = { 0.0f, 0.0f, 0.0f, 0.0f };
        try {
            IFProcOperations dev = (IFProcOperations) deviceComponent;
            try {
                voltData = dev.GET_DATA_MONITOR_2(l);
            } catch (INACTErrorEx ex) {
                // TODO: Report communication failure
                inactErrorExRepeatGuard.log(Level.FINE,
                        "INACTErrorEx: Failed to read IFProc ambient temperature from GET_DATA_MONITOR_2()");
                // TODO: Report communication failure
            } catch (CAMBErrorEx ex) {
                cambErrorExRepeatGuard.log(Level.FINE,
                        "CAMBErrorEx: Failed to read IFProc ambient temperature from GET_DATA_MONITOR_2()");
            }
            // TODO: Report communication OK
        } catch (IllegalArgumentException e) {
            illegalArgumentExceptionRepeatGuard.log(Level.FINE,
                    "GET_AMBIENT_TEMPERATURE_MonitorPointPresentationModel.getValueFromDevice()"
                            + " - illegal arguments for method" + ExcLog.details(e));
            e.printStackTrace();
        }
        timeStamp.setTime(TimeHelper.utcOmgToJava(l.value));
        // There are 4 samples in the voltData from the monitor point. One for
        // each base band pair.
        final int baseBandPairs = 4;
        
        // Calculate power from volts.
        float powerData[] = { 0.0f, 0.0f, 0.0f, 0.0f };
        for (int i = 0; i < baseBandPairs; i++) {
            powerData[i] = voltageToPower(i, voltData[i]);
        }
        
        // The result consists of one array containing:
        // average Volt total power data in array elements [0:3]
        // average dBm total power data in array elements [4:7]
        // TODO: Using an array is begging for errors. Give this more structure
        // in DATA_MONITOR_2_Sample.
        float res[] = { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
        
        // Copy voltData and powerData to res.
        for (int i = 0; i < baseBandPairs; i++) {
            res[i] = voltData[i];
            res[i + baseBandPairs] = powerData[i];
        }
        return new DATA_MONITOR_2_Sample(l, res);
    }
    
    @Override
    protected DATA_MONITOR_2_Sample getValueFromDevice() {
        return getValueFromDevice(new Date());
    }

    /**
     * Get IFProc calibration data from the TMCDB
     * 
     * TODO: Move this to the DevicePM.
     */
    private void getCalibrationData() {
        serialNumber = getSerialNumber();
        calibrationData = new IFProcCalibrationData(logger, serialNumber);
    }
    
    /**
     * Get the serial number for this IFProc from the IFProc Hardware Device.
     */
    private String getSerialNumber() {
        MonitorPointPresentationModel<int[]> serialNumberPM = containingDevicePM
                .getMonitorPointPM(IcdMonitorPoints.SERIAL_NUMBER);
        serialNumberPM.checkDeviceForUpdates();
        final int[] rawValue = serialNumberPM.getValue();
        BigInteger newValue = BigInteger.valueOf(0);
        BigInteger tmp;
        int count = 0;
        int c = 0;
        int count2 = 0;
        for (count2 = 7; count2 >= 0; count2--) {
            c = rawValue[count2];
            tmp = BigInteger.valueOf(c);
            newValue = newValue.add(tmp.shiftLeft(count));
            count += 8;
        }
        return newValue.toString();
    }
    
    /**
     * Compute power from voltage.
     * 
     * TODO: Add correction from calibration data.
     * 
     * @param voltage
     * @return power
     */
    private float voltageToPower(int index, float voltage) {
        
        float power = voltage * calibrationData.getSlope(index) + calibrationData.getIntercept(index);
        if (power <= 0) {
            power = -20.0f;
        } else {
            power = (float) (10 * Math.log10(power));
        }
        return power;
    }
    
    /**
     * Calibration data for this IFProc instance, to be read from the TMCDB.
     */
    IFProcCalibrationData calibrationData;
    
    private String serialNumber;
    
    private RepeatGuardLogger inactErrorExRepeatGuard;
    private RepeatGuardLogger cambErrorExRepeatGuard;
    private RepeatGuardLogger illegalArgumentExceptionRepeatGuard;
}

//
// O_o
