/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.IFProc.panels;

import alma.Control.device.gui.IFProc.IcdControlPoints;
import alma.Control.device.gui.IFProc.presentationModels.DevicePM;
import alma.Control.device.gui.IFProc.widgets.EnetConfigWidget;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BooleanControlPointWidget;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;
import javax.swing.BorderFactory;

/**
 * This panel provides debugging access to the ethernet configuration controls. This consists of a widget
 * which allows a user to setup/reset the Ethernet TCP/IP settings, and controls for the Ethernet init, start,
 * stop, and reset functions. It also provides another copy of the Ethernet settings to make the controls
 * easier to use.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @version $Id$
 * @since 
 */

public class DebugEthernetPanel extends DevicePanel {

    public DebugEthernetPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildSubPanels(logger, aDevicePM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM aDevicePM){
        enetSettingsPanel = new EnetSettingsPanel(logger, aDevicePM);
        enetConfigWidget = new EnetConfigWidget(logger, aDevicePM);
    }

    /**
     * Most of the functionality is in the external EnetSettingsPanel and EnetConfigWidget. So we just need
     * to add those and the Ethernet related boolean controls.
     */
    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.NONE;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;
        c.gridy++;
        c.fill = GridBagConstraints.BOTH;
        add(enetSettingsPanel, c);
        c.gridy++;
        c.fill=GridBagConstraints.NONE;
        add(new BooleanControlPointWidget(logger, devicePM
                .getControlPointPM(IcdControlPoints.INIT_TCP_CONN),
                "Initialize TCP Connection"), c);
        c.gridy++;
        add(new BooleanControlPointWidget(logger, devicePM
                .getControlPointPM(IcdControlPoints.START_DATA),
                "Start sending TP Data"), c);
        c.gridy++;
        add(new BooleanControlPointWidget(logger, devicePM
                .getControlPointPM(IcdControlPoints.STOP_DATA),
                "Stop sending TP Data"), c);
        c.gridy++;
        add(new BooleanControlPointWidget(logger, devicePM
                .getControlPointPM(IcdControlPoints.RESET_ETHERNET_TIMES),
                "Reset Ethernet times & data"), c);
        c.gridy++;
        add(new BooleanControlPointWidget(logger, devicePM
                .getControlPointPM(IcdControlPoints.RESET_TPD_ETHERNET),
                "Reset Ethernet & clear FIFO"), c);
        c.gridx++;
        c.gridy=0;
        c.fill = GridBagConstraints.BOTH;
        c.gridheight=20;
        add(enetConfigWidget, c);

        setVisible(true);
    }

    //Toast is yummy!
    EnetConfigWidget enetConfigWidget;
    EnetSettingsPanel enetSettingsPanel;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
}

//
// O_o
