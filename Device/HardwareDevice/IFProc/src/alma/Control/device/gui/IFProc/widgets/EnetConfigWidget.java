/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.IFProc.widgets;

import alma.Control.device.gui.IFProc.IcdControlPoints;
import alma.Control.device.gui.IFProc.IcdMonitorPoints;
import alma.Control.device.gui.IFProc.presentationModels.DevicePM;
import alma.Control.device.gui.IFProc.presentationModels.MonitorPointPM;
import alma.Control.device.gui.common.ControlPointPresentationModel;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.util.IpFormatter;
import alma.Control.device.gui.common.widgets.util.SubnetMaskFormatter;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.text.NumberFormatter;

/**This is a panel which provides an interface to setup the IFProc Ethernet. Setting up the Ethernet is
 * something that is best done in one step, with a chance for the user to review the numbers they are sending.
 * In addition, the IFProc will not update the readback of the information until an initialize TCP command is
 * sent. To provide user friendly interface that meets these requirements, this widget:
 *      -Has entry fields for all the TCP/IP setup information
 *      -Only sends the commands when the user clicks the control button
 *      -Allows the user to review all the information they are sending.
 *      -Defaults to the current value (as of widget build time).
 *      -Validates the input
 *      -Sends all the commands together (within CAN interface limitations)
 *      -Provides a read-back of the last command sent, so that the user will know that what was in fact sent
 * 
 * @author David Hunter dhunter@nrao.edu
 * @version $Id$
 * @since 
 */

public class EnetConfigWidget extends DevicePanel {

    public EnetConfigWidget(Logger logger, DevicePM ifpDevicePM) {
        super(logger, ifpDevicePM);
        dPM=ifpDevicePM;
        buildPanel();
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder
                ("Configure Ethernet"), BorderFactory.createEmptyBorder(1, 1, 1, 1)));

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth=2;

        addLabel ("Module IP:", c);
        c.gridy++;
        IpFormatter ipf1 = new IpFormatter(logger);
        ipf1.setOverwriteMode(false);
        ip = new JFormattedTextField(ipf1);
        ip.setPreferredSize(new Dimension(200, 20));
        MonitorPointPM m = (MonitorPointPM) dPM.getMonitorPointPM(IcdMonitorPoints.MODULE_IP_ADDR);
        byte[] b = new byte[4];
        int[] i = (int[]) m.getValueFromDevice();
        for (int count=0; count<4; count++)
            b[count]=(byte) i[count];
        try {
            ip.setValue(InetAddress.getByAddress(b));
        } catch (UnknownHostException e) {
            logger.severe("EnetConfigWidget, buildPanel: unable to convert module IP given by the" +
                    "monitor point into an InetAddress.");
        }
        add(ip, c);
        ipControl=dPM.getControlPointPM(IcdControlPoints.MODULE_IP_ADDR);
        
        c.gridy++;
        addLabel ("Destination IP:", c);
        c.gridy++;
        IpFormatter ipf2 = new IpFormatter(logger);
        ipf2.setOverwriteMode(false);
        destination = new JFormattedTextField(ipf2);
        destination.setPreferredSize(new Dimension(200, 20));
        m = (MonitorPointPM) dPM.getMonitorPointPM(IcdMonitorPoints.DEST_IP_ADDR);
        b = new byte[4];
        i = (int[]) m.getValueFromDevice();
        for (int count=0; count<4; count++)
            b[count]=(byte) i[count];
        try {
            destination.setValue(InetAddress.getByAddress(b));
        } catch (UnknownHostException e) {
            logger.severe("EnetConfigWidget, buildPanel: unable to convert module IP given by the" +
                    "monitor point into an InetAddress.");
        }
        add(destination, c);
        destinationControl=dPM.getControlPointPM(IcdControlPoints.DEST_IP_ADDR);

        c.gridy++;
        addLabel ("Gateway IP:", c);
        c.gridy++;
        IpFormatter ifp3 = new IpFormatter(logger);
        ifp3.setOverwriteMode(false);
        gateway = new JFormattedTextField(ifp3);
        gateway.setPreferredSize(new Dimension(200, 20));
        m = (MonitorPointPM) dPM.getMonitorPointPM(IcdMonitorPoints.MODULE_GATEWAY);
        b = new byte[4];
        i = (int[]) m.getValueFromDevice();
        for (int count=0; count<4; count++)
            b[count]=(byte) i[count];
        try {
            gateway.setValue(InetAddress.getByAddress(b));
        } catch (UnknownHostException e) {
            logger.severe("EnetConfigWidget, buildPanel: unable to convert module IP given by the" +
                    "monitor point into an InetAddress.");
        }
        add(gateway, c);
        gatewayControl=dPM.getControlPointPM(IcdControlPoints.MODULE_GATEWAY);

        c.gridy++;
        addLabel ("Subnet mask:", c);
        c.gridy++;
        SubnetMaskFormatter s = new SubnetMaskFormatter(logger);
        s.setOverwriteMode(false);
        subnetMask = new JFormattedTextField(s);
        subnetMask.setPreferredSize(new Dimension(200, 20));
        m = (MonitorPointPM) dPM.getMonitorPointPM(IcdMonitorPoints.MODULE_NETMASK);
        subnetMask.setValue(m.getValueFromDevice());
        add(subnetMask, c);
        subnetControl=dPM.getControlPointPM(IcdControlPoints.MODULE_NETMASK);
        
        c.gridy++;
        addLabel("TCP port:", c);
        c.gridy++;
        DecimalFormat df1 = new DecimalFormat();
        df1.setParseIntegerOnly(true);
        df1.setMaximumIntegerDigits(5);
        df1.setMinimumIntegerDigits(1);
        NumberFormatter n1 = new NumberFormatter(df1);
        n1.setOverwriteMode(false);
        n1.setMinimum(0);
        n1.setMaximum(0xFFFF);
        tcpPort = new JFormattedTextField(n1);
        tcpPort.setPreferredSize(new Dimension(100, 20));
        m = (MonitorPointPM) dPM.getMonitorPointPM(IcdMonitorPoints.TCP_PORT);
        tcpPort.setValue(m.getValueFromDevice());
        add(tcpPort, c);
        tcpPortControl=dPM.getControlPointPM(IcdControlPoints.TCP_PORT);
        
        c.gridy++;
        addLabel("BDB per packet:", c);
        c.gridy++;
        DecimalFormat df2 = new DecimalFormat();
        df2.setParseIntegerOnly(true);
        df2.setMaximumIntegerDigits(3);
        df2.setMinimumIntegerDigits(2);
        NumberFormatter n2 = new NumberFormatter(df2);
        n2.setOverwriteMode(false);
        n2.setMinimum(32);
        n2.setMaximum(255);
        bdbPerPacket = new JFormattedTextField(n2);
        bdbPerPacket.setPreferredSize(new Dimension(100, 20));
        m = (MonitorPointPM) dPM.getMonitorPointPM(IcdMonitorPoints.BDB_PER_PACKET);
        bdbPerPacket.setValue(m.getValueFromDevice());
        add(bdbPerPacket, c);
        bdbControl=dPM.getControlPointPM(IcdControlPoints.BDB_PER_PACKET);
        
        c.gridy++;
        setValues =new JButton("Reconfigure Ethernet");
        setValues.addActionListener(new setValuesAction());
        tcpPortControl.getContainingDevicePM().addControl(setValues);
        add(setValues, c);

        
        c.gridwidth=1;
        c.gridx=1;
        c.gridy++;
        add(new JLabel("Values sent:"), c);
        c.gridy++;
        addLeftLabel("Module IP:", c);
        ipReadback=new JLabel("-");
        add(ipReadback, c);
        
        c.gridy++;
        addLeftLabel("Desination IP:", c);
        destinationReadback=new JLabel("-");
        add(destinationReadback, c);
        
        c.gridy++;
        addLeftLabel("Gateway IP:", c);
        gatewayReadback=new JLabel("-");
        add(gatewayReadback, c);
        
        c.gridy++;
        addLeftLabel("Subnet Mask:", c);
        subnetMaskReadback=new JLabel("-");
        add(subnetMaskReadback, c);
        
        c.gridy++;
        addLeftLabel("TCP Port:", c);
        tcpPortReadback=new JLabel("-");
        add(tcpPortReadback, c);
        
        c.gridy++;
        addLeftLabel("BDB Per Packet:", c);
        bdbPerPacketReadback=new JLabel("-");
        add(bdbPerPacketReadback, c);
        
        setVisible(true);
    }
    
    /**Add a label with a bit of an inset, located to the lower left.
     * Used for labeling the data entry fields.
     * @param s The text to put in the label
     * @param c Constraints which give the location of where to put the label.
     *          The constraints will be temporarily altered, and then restored to their original properties.
     */
    private void addLabel(String s, GridBagConstraints c){
        int tmp=c.anchor;
        c.insets = new Insets(5,0,0,0);
        c.anchor=GridBagConstraints.SOUTHWEST;
        add(new JLabel(s), c);
        c.anchor=tmp;
        c.insets = new Insets(0,0,0,0);
    }
    
    /**An action listener that checks all the input fields, sends the values to the associated controls,
     * and updates the readback field.
     * 
     * @author dhunter
     *
     */
    private class setValuesAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                byte[] ipInBytes = InetAddress.getByName(ip.getText()).getAddress();
                int[] newIp = new int[4];
                for (int c=0; c<4; c++)
                    newIp[c]=ipInBytes[c];
                ipControl.setValue(newIp);
                ipReadback.setText(ip.getText());
                
                byte[] dVInBytes = InetAddress.getByName(destination.getText()).getAddress();
                int[] dV = new int[4];
                for (int c=0; c<4; c++)
                    dV[c]=dVInBytes[c];
                destinationControl.setValue(dV);
                destinationReadback.setText(destination.getText());
                
                byte[] gwInBytes = InetAddress.getByName(gateway.getText()).getAddress();
                int[] gw = new int[4];
                for (int c=0; c<4; c++)
                    gw[c]=gwInBytes[c];
                gatewayControl.setValue(gw);
                gatewayReadback.setText(gateway.getText());
                
            } catch (UnknownHostException e1) {
                logger.severe("EnetConfigWidget, setValuesAction - unknown address. This should " +
                              "never happen because the IpFormatter is supposd to prevent it.");
                e1.printStackTrace();
            }
            
            subnetControl.setValue((int[])subnetMask.getValue());
            subnetMaskReadback.setText(subnetMask.getText());
            
            tcpPortControl.setValue((Integer) tcpPort.getValue());
            tcpPortReadback.setText(tcpPort.getText());
            
            bdbControl.setValue((Integer) bdbPerPacket.getValue());
            bdbPerPacketReadback.setText(bdbPerPacket.getText());
            revalidate();
        }
    }
    
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;

    private JFormattedTextField bdbPerPacket;
    private ControlPointPresentationModel<Integer> bdbControl;
    private JLabel bdbPerPacketReadback;
    
    private JFormattedTextField destination;
    private ControlPointPresentationModel<int[]> destinationControl;
    private JLabel destinationReadback;
    
    private DevicePM dPM;
    
    private JFormattedTextField gateway;
    private ControlPointPresentationModel<int[]> gatewayControl;
    private JLabel gatewayReadback;
    
    private JFormattedTextField ip;
    private ControlPointPresentationModel<int[]> ipControl;
    private JLabel ipReadback;
    
    private JButton setValues;
    
    private JFormattedTextField subnetMask;
    private ControlPointPresentationModel<int[]> subnetControl;
    private JLabel subnetMaskReadback;
    
    private JFormattedTextField tcpPort;
    private ControlPointPresentationModel<Integer> tcpPortControl;
    private JLabel tcpPortReadback;
}

//
// O_o
