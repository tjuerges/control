/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.IFProc.panels;

import alma.Control.device.gui.IFProc.IcdControlPoints;
import alma.Control.device.gui.IFProc.IcdMonitorPoints;
import alma.Control.device.gui.IFProc.IdlMonitorPoints;
import alma.Control.device.gui.IFProc.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BendSerialMonitorPointWidget;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.IntegerMonitorPointWidget;
import alma.Control.device.gui.common.widgets.BooleanControlPointWidget;
import alma.Control.device.gui.common.widgets.LruMonitorPointWidget;
import alma.Control.device.gui.common.widgets.Status;
import alma.Control.device.gui.common.widgets.VersionMonitorPointWidget;
import alma.Control.device.gui.common.panels.AmbsiPanel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;
import javax.swing.BorderFactory;

/**
 * This panel provides information on the IFProc module codes, ABM status, SPI status,
 * and the AMBSI panel.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class DeviceInfoPanel extends DevicePanel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;

    public DeviceInfoPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildSubPanels(logger, aDevicePM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM dPM){
        ambsiPanel = new AmbsiPanel(logger, dPM,
                devicePM.getMonitorPointPM(IcdMonitorPoints.SERIAL_NUMBER),
                devicePM.getMonitorPointPM(IdlMonitorPoints.GET_AMBIENT_TEMPERATURE),
                devicePM.getMonitorPointPM(IcdMonitorPoints.TRANS_NUM),
                devicePM.getMonitorPointPM(IcdMonitorPoints.INTERNAL_SLAVE_ERROR_CODE),
                devicePM.getMonitorPointPM(IcdMonitorPoints.CAN_ERROR_COUNT),
                devicePM.getMonitorPointPM(IcdMonitorPoints.ERROR_CODE_LAST_CAN_ERROR),
                devicePM.getMonitorPointPM(IcdMonitorPoints.MAJOR_REV_LEVEL),
                devicePM.getMonitorPointPM(IcdMonitorPoints.MINOR_REV_LEVEL),
                devicePM.getMonitorPointPM(IcdMonitorPoints.PATCH_LEVEL),
                devicePM.getMonitorPointPM(IcdMonitorPoints.PROTOCOL_MAJOR_REV_LEVEL),
                devicePM.getMonitorPointPM(IcdMonitorPoints.PROTOCOL_MINOR_REV_LEVEL),
                devicePM.getMonitorPointPM(IcdMonitorPoints.PROTOCOL_PATCH_LEVEL),
                devicePM.getControlPointPM(IcdControlPoints.RESET_AMBSI));
        ambStatusPanel = new AmbStatusPanel(logger, dPM);
        moduleCodesPanel = new ModuleCodesPanel(logger, dPM);
        spiStatusPanel = new SpiStatusPanel(logger, dPM);
        tpdModuleCodesPanel = new TpdModuleCodesPanel(logger, dPM);
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;
        c.gridheight=2;

        add(ambsiPanel, c);
        c.gridheight=1;
        c.gridy+=2;
        add(ambStatusPanel, c);
        c.gridy=0;
        c.gridx++;
        add(moduleCodesPanel, c);
        c.gridy++;
        add(tpdModuleCodesPanel, c);
        c.gridy++;
        add(spiStatusPanel, c);

        setVisible(true);
    }

    /*
     * This sub-panel displays the regular module codes.
     */
    private class ModuleCodesPanel extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public ModuleCodesPanel(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
          setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder
                                   ("Module Codes:"),BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.gridx=1;
            c.gridy=0;
            c.anchor=GridBagConstraints.WEST;
            
            addLeftEastLabel("Serial Number:", c);
            add(new BendSerialMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.MODULE_CODES_SERIAL)), c);
            c.gridy++;
            addLeftEastLabel("Version:", c);
            add(new VersionMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.MODULE_CODES_VERSION_MAJOR),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.MODULE_CODES_VERSION_MINOR),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.MODULE_CODES_CDAY),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.MODULE_CODES_CMONTH),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.MODULE_CODES_YEAR)), c);
            c.gridy++;
            addLeftEastLabel("LRU CIN:", c);
            add(new LruMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.MODULE_CODES_DIG1),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.MODULE_CODES_DIG2),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.MODULE_CODES_DIG4),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.MODULE_CODES_DIG6)), c);
        }
    }

    /*
     * This sub-panel displays the regular module codes.
     */
    private class TpdModuleCodesPanel extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public TpdModuleCodesPanel(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
          setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder
                                ("TPD Module Codes:"),BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.gridx=1;
            c.gridy=0;
            c.anchor=GridBagConstraints.WEST;
            
            addLeftEastLabel("Serial Number:", c);
            add(new BendSerialMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TPD_MODULE_CODES_SERIAL)), c);
            c.gridy++;
            addLeftEastLabel("Version:", c);
            add(new VersionMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TPD_MODULE_CODES_VERSION_MAJOR),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TPD_MODULE_CODES_VERSION_MINOR),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TPD_MODULE_CODES_CDAY),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TPD_MODULE_CODES_CMONTH),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TPD_MODULE_CODES_YEAR)), c);
            c.gridy++;
            addLeftEastLabel("LRU CIN:", c);
            add(new LruMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TPD_MODULE_CODES_DIG1),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TPD_MODULE_CODES_DIG2),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TPD_MODULE_CODES_DIG4),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TPD_MODULE_CODES_DIG6)), c);
        }
    }
    /*
     * This sub-panel displays the status of the SPI and ABM monitor ponts.
     */
    private class SpiStatusPanel extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public SpiStatusPanel(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
          setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder
                                ("SPI Status"),BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.anchor=GridBagConstraints.WEST;
            c.gridx=1;
            c.gridy=0;
            addLeftEastLabel("IFDC Found", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.IFDC_FOUND),
                    Status.FAILURE, "Error: IFDC not communicationg!",
                    Status.GOOD, "IFDC Ok."), c);
            c.gridy++;
            addLeftEastLabel("IFDC variable length reads", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.IFDC_VAR_LENGTH_READS),
                    Status.GOOD, "IFDC/AMBSI SPI properly set to fixed length reads.",
                    Status.FAILURE, "Error: IFDC/AMBSI2 SPI set to variable length reads!"), c);
            c.gridy++;
            addLeftEastLabel("IFDC ADS: ", c);
            add(new IntegerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.IFDC_ADS)), c);
            c.gridy++;
            addLeftEastLabel("IFDC MCS: ", c);
            add(new IntegerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.IFDC_MCS)), c);
            c.gridy++;
            addLeftEastLabel("IFDC Length: ", c);
            add(new IntegerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.IFDC_LENGTH)), c);
            c.gridy++;
            addLeftEastLabel("SPI Errors - last address: ", c);
            add(new IntegerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.LAST_ADDRESS_INTERACTION)), c);
            c.gridy++;
            addLeftEastLabel("SPI Errors - error counter: ", c);
            add(new IntegerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.IFDC_SPI_PROTO_ERR_COUNTER)), c);
        }
    }
    
    /*
     * This sub-panel displays the status of the SPI and ABM monitor ponts.
     */
    private class AmbStatusPanel extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public AmbStatusPanel(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
          setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder
                                ("AMB Status"),BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.anchor=GridBagConstraints.WEST;
            c.gridx=1;
            c.gridy=0;
            
            addLeftEastLabel("AMB Command Counter: ", c);
            add(new IntegerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.AMB_CMD_COUNTER)), c);
            c.gridy++;
            addLeftEastLabel("Last invalid command, RCA: ", c);
            add(new IntegerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.LSBS_8_RCAS)), c);
            c.gridy++;
            addLeftEastLabel("Last invalid command, 1st byte: ", c);
            add(new IntegerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FIRST_BYTE_INVALID)), c);
            c.gridy++;
            addLeftEastLabel("Last invalid command, counter: ", c);
            add(new IntegerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.VAL_READ_AMB_CMD_COUNTER)), c);
            c.gridy++;
            c.gridwidth=2;
            c.gridx--;
            c.anchor=GridBagConstraints.CENTER;
            add(new BooleanControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.RESET_AMB_CMD_COUNTER),
                    "Reset AMB Command Counter"), c);
            c.gridy++;
            add(new BooleanControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.RESET_AMB_INVALID_CMD),
                    "Reset AMB Invalid Command"), c);
        }
    }
    
    AmbStatusPanel ambStatusPanel;
    AmbsiPanel ambsiPanel;
    ModuleCodesPanel moduleCodesPanel;
    SpiStatusPanel spiStatusPanel;
    TpdModuleCodesPanel tpdModuleCodesPanel;
}

//
// O_o
