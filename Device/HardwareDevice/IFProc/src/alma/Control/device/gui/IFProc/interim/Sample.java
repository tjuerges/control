/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
package alma.Control.device.gui.IFProc.interim;

import java.util.Date;
import java.util.Random;

/**
 * Samples collect together data about a single sample taken from a monitor point. 
 * 
 * @version $Id$
 * @since 7.0.0
 */
public class Sample {
    
    /** 
     * Create an instance with the given values.
     * 
     * @param aTime the sample was taken.
     * @param aValue the value of the sample.
     */
    public Sample(Date aTime, double aValue) {
        time = aTime;
        value = aValue;
//        System.out.println("alma.Control.device.gui.IFProc.interim.Sample() adding random number - FOR DEBUG ONLY!");
//        value = aValue + (float)generator.nextGaussian()/100;
    }
    
    /** 
     * Create a new instance of a Sample with the current time and a pseudo random value.
     * 
     * @param a new instance with the current time and a pseudo random value.
     */
    public static Sample getRamdomSample() { 
        System.out.println("alma.Control.device.gui.IFProc.interim.Sample.getRamdomSample() - FOR DEBUG ONLY!");
        return new Sample(new Date(), 5.5f + (float)generator.nextGaussian()/100); 
    }
    
    public Date getTime() { return time; }
    public double getTimeAsDouble() { return time.getTime() * 1.0d; }
    public double getValue() { return value; }
    
    public String toString() {
        return "sample time: " + time + ", value: " + value;
    }
            
    private Date time;
    private double value;

    // DEBUG
    // TODO: Remove after debugging is complete.
    private static Random generator = new Random();
}

//
// O_o
