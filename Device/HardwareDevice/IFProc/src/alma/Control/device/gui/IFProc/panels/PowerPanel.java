/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.IFProc.panels;

import alma.Control.device.gui.IFProc.IcdMonitorPoints;
import alma.Control.device.gui.IFProc.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.FloatMonitorPointWidget;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;

/**This panel displays the IFProc power summary alarms, current readouts, and voltage readouts.
 * It is intended for both operator and debugging use. Note that the power summary alarms are aso
 * displayed in the Status panel.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class PowerPanel extends DevicePanel {

    public PowerPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildSubPanels(logger, aDevicePM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM aDevicePM){
        currentReadouts = new CurrentReadouts(logger, aDevicePM);
        powerStatus = new PowerStatusPanel(logger, aDevicePM);
        voltageReadouts = new VoltageReadouts(logger, aDevicePM);
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;

        add(powerStatus, c);
        c.gridx++;
        add(currentReadouts, c);
        c.gridx++;
        add(voltageReadouts, c);

        setVisible(true);
    }

    /*
     * This panel displays detailed current information from READ_10V_CURRENTS, READ_6.5V_CURRENTS,
     * and READ_8V_CURRENTS.
     */
    private class CurrentReadouts extends DevicePanel {

        public CurrentReadouts(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
          setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Current"),
                  BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.gridx = 1;
            c.gridy = 0;

            c.anchor=GridBagConstraints.WEST;
            addLeftEastLabel("10V, Section 1: ", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.CURRENT_10V_SEC_1), true, 2), c);
            c.gridy++;
            addLeftEastLabel("10V, Section 2: ", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.CURRENT_10V_SEC_2), true, 2), c);
            c.gridy++;
            addLeftEastLabel("10V, Switch Matrix 1: ", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.CURR_10V_SW_MATRIX_1), true, 2), c);
            c.gridy++;
            addLeftEastLabel("10V, Switch Matrix 2: ", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.CURR_10V_SW_MATRIX_2), true, 2), c);
            c.gridy++;
            addLeftEastLabel("6.5V, Section A: ", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.CURRENT_6_5V_SEC_A), true, 2), c);
            c.gridy++;
            addLeftEastLabel("6.5V, Section B: ", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.CURRENT_6_5V_SEC_B), true, 2), c);
            c.gridy++;
            addLeftEastLabel("6.5V, Section C: ", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.CURRENT_6_5V_SEC_C), true, 2), c);
            c.gridy++;
            addLeftEastLabel("6.5V, Section D: ", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.CURRENT_6_5V_SEC_D), true, 2), c);
            c.gridy++;
            addLeftEastLabel("8V, Section A: ", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.CURRENT_8V_SEC_A), true, 2), c);
            c.gridy++;
            addLeftEastLabel("8V, Section B: ", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.CURRENT_8V_SEC_B), true, 2), c);
            c.gridy++;
            addLeftEastLabel("8V, Section C: ", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.CURRENT_8V_SEC_C), true, 2), c);
            c.gridy++;
            addLeftEastLabel("8V, Section D: ", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.CURRENT_8V_SEC_D), true, 2), c);
        }

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }
    
    /*
     * This panel displays detailed voltage readouts from READ_VOLTATES_1 and
     * READ_VOLTAGES_2.
     */
    private class VoltageReadouts extends DevicePanel {
        public VoltageReadouts(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }
        protected void buildPanel() {
          setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(" Voltage"),
                  BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.gridx = 1;
            c.gridy = 0;
    
            c.anchor=GridBagConstraints.WEST;
            addLeftEastLabel("Digital 1.2V: ", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DIG_1DOT2_VOLTAGES), true, 2), c);
            c.gridy++;
            addLeftEastLabel("Digital 2.5V: ", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DIG_2DOT5_VOLTAGES), true, 2), c);
            c.gridy++;
            addLeftEastLabel("Digital 3.3V: ", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DIG_3DOT3_VOLTAGES), true, 2), c);
            c.gridy++;
            addLeftEastLabel("Ethernet power: ", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.ETHER_CONT_VOLTAGES), true, 2), c);
            c.gridy++;
            addLeftEastLabel("Digital 5V: ", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DIG_5_VOLTAGES), true, 2), c);
            c.gridy++;
            addLeftEastLabel("Analog 5V: ", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.ANALOG_5_VOLTAGES), true, 2), c);
            c.gridy++;
            addLeftEastLabel("Reference 5V: ", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.REFERENCE_5_VOLTAGES), true, 2), c);
            c.gridy++;
            addLeftEastLabel("7V Power in: ", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.POWER_IN_7V), true, 2), c);
        }
        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }

    CurrentReadouts currentReadouts;
    PowerStatusPanel powerStatus;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    VoltageReadouts voltageReadouts;
}

//
// O_o
