/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.IFProc.presentationModels;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Provide IFProc calibration data.  Read data from $ACS_CDB/TMCDB_DATA if data exists for the given
 * serial number.  Provide default data if no data found for the given serial number.
 * 
 * Default data taken from CONTROL/config/Simulation/TMCDB_DATA/ifp_default.xml from ALMA-7_0_0-B 
 * as of 2009-12-03.
 *  
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 7.0.0
 */
public class IFProcCalibrationData {
    
    /**
     * A container for calibration data for an IFProc
     * 
     * @param aSerialNumber for the IFProc.
     */
    public IFProcCalibrationData(Logger aLogger, String aSerialNumber) {
        final String METHOD_NAME = "IFProcCalibrationData";
        
        logger = aLogger;
        initChannelToIndex();
        
        Document calibrationData = getCalibrationDocument(aSerialNumber);
        
        if (calibrationData == null) {
            logger.warning(CLASS_NAME + "." + METHOD_NAME + 
                           "(): Could not find calibration data file for IFProc with SN: " + aSerialNumber + 
                           ".  Using default calibration data.");
        } else {
            NodeList detNodes = calibrationData.getElementsByTagName("DET");
            for (int i = 0; i < detNodes.getLength(); i++) {
                Node node = detNodes.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    NamedNodeMap nodeMap = node.getAttributes();
                    String channel = getChannel(nodeMap);
                    // See if channel contains one of the base band pair channels.
                    // If so, save the data.
                    String channels = "ABCD";
                    if (channels.indexOf(channel) != -1) {
                        intercept[channelToIndex(channel)] = getInterceptAttribute(nodeMap); 
                        slope[channelToIndex(channel)] = getSlopeAttribute(nodeMap); 
                    }
                }
            }
        }
    }
    
    /**
     * Get the intercept calibration data for a base band pair.
     * 
     * @param baseBandPair to get data for
     * 
     * @return the intercept value.
     */
    public float getIntercept(int baseBandPair) {
        return intercept[baseBandPair];
    }
    
    /**
     * Get the slope calibration data for a base band pair.
     * 
     * @param baseBandPair to get data for
     * 
     * @return the intercept value.
     */
    public float getSlope(int baseBandPair) {
        return slope[baseBandPair];
    }
    
    /**
     * Get a list of files that may contain calibration data for the device with a given serial number.
     * 
     * @param serialNumber for the device we care about
     * 
     * @return a list of files that may contain the calibration data for this device.
     */
    private ArrayList<String> getCandidateCalibrationFiles(String serialNumber) {
        final String METHOD_NAME = "getCandidateCalibrationFiles";
        ArrayList<String> results = new ArrayList<String>();

        String tmcdbDataDir = getTmcdbDataDirectory();
        if (tmcdbDataDir == null) {
            logger.warning(CLASS_NAME + "." + METHOD_NAME + "(): Could not get the TMCDB_DATA directory");
            return results;
        } 
        
        final String commandLine = "/bin/grep -lr " + serialNumber + " " + tmcdbDataDir;
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(commandLine);
        } catch (IOException e) {
            logger.warning(CLASS_NAME + "." + METHOD_NAME + "(): IOException running command line.");
        }
        
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line = "";
        try {
            while ((line = bufferedReader.readLine()) != null) {
                if (line.endsWith("xml")) 
                    results.add(line);
            };
        } catch (IOException e) {
            logger.warning(CLASS_NAME + "." + METHOD_NAME + "(): IOException reading command line results.");
        }  
        return results;
    }
    
    /**
     * Find the DOM document with calibration data for the device with the the given serial number.
     * 
     * This is designed around the IFProc calibration data files.  It may or may not work for other devices.
     * 
     * @param serialNumber
     * @return A DOM document or null.
     */
    private Document getCalibrationDocument(String serialNumber) {
        final String METHOD_NAME = "getCalibrationDocument";
        ArrayList<String> candidateFileList = getCandidateCalibrationFiles(serialNumber);
        if (candidateFileList.isEmpty()) { 
            logger.warning(CLASS_NAME + "." + METHOD_NAME + "(): No candidates found.");
            return null;
        }
        
        for (String candidateFile: candidateFileList) {
            Document candidateDocument = readDomDocument(candidateFile);
            if (candidateDocument == null) {
                logger.warning(CLASS_NAME + "." + METHOD_NAME + "(): Failed to read DOM document: " + 
                               candidateFile);
                // Continue with the next candidate.
            } else {
                NodeList detNodes = candidateDocument.getElementsByTagName("SN");
                for (int i = 0; i < detNodes.getLength(); i++) {
                    Node node = detNodes.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        NamedNodeMap nodeMap = node.getAttributes();
                        String fileSerialNumber = getValueAttribute(nodeMap);
                        if (serialNumber.equals(fileSerialNumber)) {
                            logger.info(CLASS_NAME + "." + METHOD_NAME + 
                                        "(): Attempting to read calibration data from: " + candidateFile);
                            return candidateDocument;
                        }
                    }
                }
            }
        }
        
        // If we get here, we did not find a calibration file for this serial number.
        logger.warning(CLASS_NAME + "." + METHOD_NAME + 
                    "(): Did not find a file with calibration data for IFProc serial number: " + 
                    serialNumber);
        return null;
    }

    /**
     * Get the value of the "ch" attribute from a node.
     * 
     * @param nodeMap containing attributes from a node
     * 
     * @return the value of the channel attribute
     */
    private String getChannel(NamedNodeMap nodeMap) {
        String result = null;
        
        final Node channel = nodeMap.getNamedItem("ch");
        result = channel.getNodeValue();
        
        return result;
    }
    
    /**
     * Get float attribute from a DOM node.
     * 
     * @param nodeMap containing attributes from a node
     * @param attrName the name of the attribute to get.
     * 
     * @return the value of the attribute
     */
    private float getFloatAttribute(NamedNodeMap nodeMap, String attrName) {
        final String METHOD_NAME = "getFloatAttribute";
        float result = 0.0f; 
        final String valueString = getStringAttribute(nodeMap, attrName);
        try {
            result = Float.parseFloat(valueString);
        } catch (NumberFormatException e) {
            logger.warning(CLASS_NAME + "." + METHOD_NAME + "(): failed for attrName: " + attrName);
        }
        return result;
    }
    
    /**
     * Get the value of the "icept" attribute from a node.
     * 
     * @param nodeMap containing attributes from a node
     * 
     * @return the value of the channel attribute
     */
    private float getInterceptAttribute(NamedNodeMap nodeMap) {

        return getFloatAttribute(nodeMap, "icept");
    }
    
    /**
     * Get the value of the "slope" attribute from a node.
     * 
     * @param nodeMap containing attributes from a node
     * 
     * @return the value of the channel attribute
     */
    private float getSlopeAttribute(NamedNodeMap nodeMap) {

        return getFloatAttribute(nodeMap, "slope");
    }
    
    /**
     * Get string attribute from a DOM node.
     * 
     * @param nodeMap containing attributes from a node
     * @param attrName the name of the attribute to get.
     * 
     * @return the value of the attribute
     */
    private String getStringAttribute(NamedNodeMap nodeMap, String attrName) {
        final String METHOD_NAME = "getStringAttribute";
        String result = null;
        final Node node = nodeMap.getNamedItem(attrName);
        try {
            result = node.getNodeValue();
        } catch (DOMException e) {
            logger.warning(CLASS_NAME + "." + METHOD_NAME + "(): failed for attrName: " + attrName);            
        }
        return result;
    }
    
    /**
     * Get the path to the TMCDB_DATA directory from the shell.
     * 
     * @return the full path to the TMCDB_DATA directory.
     */
    private String getTmcdbDataDirectory() {
        String result = null;
        
        final String acsCdb = System.getenv("ACS_CDB");
        if (acsCdb == null) {
            return null;
        } else {
            result = acsCdb + "/TMCDB_DATA";
        }

        return result;
    }
    
    /**
     * Get the value of the "value" attribute from a node.
     * 
     * @param nodeMap containing attributes from a node
     * 
     * @return the value of the channel attribute
     */
    private String getValueAttribute(NamedNodeMap nodeMap) {
        String result = null;
        
        final Node channel = nodeMap.getNamedItem("value");
        result = channel.getNodeValue();
        
        return result;
    }
   
    /**
     * Get the contents of a file and return a DOM document.
     * 
     * @param filePath the full path to the file
     * 
     * @return a DOM document
     */
    private Document readDomDocument(String filePath) {
        final String METHOD_NAME = "readDomDocument";
        Document result = null;
        
        File file = new File(filePath);
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = null;
        try {
            db = documentBuilderFactory.newDocumentBuilder();
            result = db.parse(file);
        } catch (Exception e) {
            logger.warning(CLASS_NAME + "." + METHOD_NAME + "(): Failed to read data from filePath: " + 
                           filePath);
            return null;
        }
        result.getDocumentElement().normalize();
        return result;
    }
    
    private int channelToIndex(String letter) {
        
        return channelToIndex.get(letter);
    }
    
    private void initChannelToIndex() {
        channelToIndex = new HashMap<String, Integer>();
        channelToIndex.put("A", 0);
        channelToIndex.put("B", 1);
        channelToIndex.put("C", 2);
        channelToIndex.put("D", 3);
    }

    /**
     * Name of class used in logging statements.
     */
    private static final String CLASS_NAME = "IFProcCalibrationData";

    /**
     * Map letters to numbers for indexing into the intercept and slope arrays.
     */
    private HashMap<String, Integer> channelToIndex;
    
    /**
     * Calibration data for this IFProc instance, to be read from the TMCDB. Default values 
     * will be replaced with real values from a calibration data file if possible.
     */
    private float[] intercept = { -0.117000f,  -0.101000f, -0.121000f, -0.098700f };
    private float[] slope = { 1.630000f, 1.600000f, 1.580000f, 1.605000f };

    private Logger logger;
}
//
// O_o