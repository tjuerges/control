/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ArrayComponent.java
 */
package alma.Control.TotalPowerProcessor;

// import java.net.InetAddress;
// import java.net.UnknownHostException;
//import java.net.ServerSocket;
//import java.net.Socket;

// import java.io.DataInputStream;
// import java.io.InterruptedIOException;
// import java.io.IOException;

import java.nio.ByteBuffer;
import java.nio.ShortBuffer;
// import java.nio.channels.Selector;
// import java.nio.channels.SelectionKey;
// import java.nio.channels.SocketChannel;
// import java.nio.channels.ServerSocketChannel;
// import java.nio.channels.ClosedSelectorException;
// import java.nio.channels.ClosedByInterruptException;

// import java.lang.RuntimeException;
// import java.lang.ThreadGroup;
// import java.lang.InterruptedException;
// import java.lang.Thread;
// import java.lang.Runnable;
// import java.io.DataInputStream;
// import java.lang.Math;
// import alma.ControlExceptions.DeviceBusyEx;

// import java.util.concurrent.Semaphore;
// import java.util.Set;
// import java.util.Iterator;
// import java.util.HashMap;

// import alma.Control.TotalPowerDataReceiver.TotalPowerStreamID;
// import alma.Control.TotalPowerDataReceiver.TotalPowerDataSenderInterface;


public class AntTPSubscanData {

    public String       antennaName;
    public int          polarizationNum;
    public long         subscanBeginTime;
    public long         subscanEndTime;
    public ShortBuffer  dataBuffer;
    public boolean[]    dataFlags;

    public AntTPSubscanData (String antennaName,
			     int    polarizationNum,
			     long   subscanBeginTime,
			     long   subscanEndTime,
			     byte[] dataBuffer,
			     boolean[] dataFlags) {
	this.antennaName      = antennaName;
	this.polarizationNum  = polarizationNum;
        this.subscanBeginTime = subscanBeginTime;
	this.subscanEndTime   = subscanEndTime;
	this.dataFlags        = dataFlags;
	this.dataBuffer       = ByteBuffer.wrap(dataBuffer).asShortBuffer();
    }
}
