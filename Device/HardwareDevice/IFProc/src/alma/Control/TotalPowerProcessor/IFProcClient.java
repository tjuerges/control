/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ArrayComponent.java
 */
package alma.Control.TotalPowerProcessor;

import java.net.ServerSocket;
import java.net.Socket;

import java.io.IOException;
import java.io.DataInputStream;

import java.nio.ByteBuffer;

import java.lang.InterruptedException;
import java.lang.Thread;
import java.lang.Runnable;
import java.lang.Math;

import java.util.concurrent.Semaphore;

import alma.Control.Common.Util;
import alma.ControlExceptions.DeviceBusyEx;
import alma.acs.time.TimeHelper;

import java.util.logging.Logger;

public class IFProcClient implements Runnable {

    protected String           antennaName;
    protected int              polarizationNum;
    protected Socket           rxSocket = null;
    protected ServerSocket     serverSocket;
    protected DataInputStream  socketInput;

    private boolean            isClosed = false;
    private final Semaphore    subscanMutex = new Semaphore(1);
    private Semaphore          doneSignal = null;
    private long               beginTime;
    private long               endTime;
    private byte[]             dataBuffer = null;
    private boolean[]          flagBuffer = null;
    private int                bdbToAcquire;
    private int		           numFlags;
    private Logger	           logger;

    private long               maxLatency = 0;

    /* This keeps track of the number of samples interleaved, in this
       case there are 4 basebands all sampled together */
    static protected final int          NumChannelPerSample = 4;

    /* This keeps track of the intrinsic rate of the samplers (2 KHz)
       expressed as an ACS::Time */
    static protected final int          SampleInterval = 5000; //0.5 [ms]


    public IFProcClient(String antennaName,
                        int polarizationNum,
                        ServerSocket serverSocket,
                        Logger logger){
        this.antennaName     = antennaName;
        this.polarizationNum = polarizationNum;
        this.serverSocket    = serverSocket;
        this.logger = logger;
    }

    public void close() {
        try {
            if (rxSocket != null) 
                rxSocket.close();
            if (serverSocket != null)
                serverSocket.close();
        } catch (IOException ex) {
            System.out.println("Caught java IO exception... "
                    + "terminating connection.");
        }
        isClosed = true;
    }


    public boolean isClosed() {
        return isClosed;
    }

    public boolean isConnected() {
        if (rxSocket == null) 
            return false;
        else
            return true;
    }

    public void beginSubscan(long beginTime, long endTime, Semaphore doneSignal)
            throws InterruptedException, DeviceBusyEx {
        subscanMutex.acquire();
        if (bdbToAcquire != 0) {
            /* Subscan already in progress throw exception */
            subscanMutex.release();
            throw new DeviceBusyEx();
        }
        this.doneSignal = doneSignal;
        this.beginTime = beginTime;
        this.endTime = endTime;
        String msg = antennaName + " pol " + polarizationNum + " The "
                + String.format("%.3f", (endTime - beginTime) * 100E-9)
                + " second subscan starts at "
                + Util.acsTimeToString(beginTime) + " & ends at "
                + Util.acsTimeToString(endTime);                   

        // Use system method to initialize memory
        bdbToAcquire = (int) ((endTime - beginTime) / (long) SampleInterval);
        msg += " and contains " + bdbToAcquire + " samples each of " + 
            SampleInterval*100E-9*1E3 + " milli-seconds.";
        logger.info(msg);

        dataBuffer = new byte[bdbToAcquire * 8];
        flagBuffer = new boolean[bdbToAcquire];
        for (int idx = 0; idx < bdbToAcquire; idx++)
            flagBuffer[idx] = true; // We start assuming that all the data is
                                    // Bad.
        numFlags = bdbToAcquire;
        subscanMutex.release();

    }

    public void abortSubscan() 
        throws InterruptedException
        {
            subscanMutex.acquire();
            bdbToAcquire = 0;
            doneSignal.release();
            subscanMutex.release();
        }

    public AntTPSubscanData getSubscanData() 
        throws InterruptedException, DeviceBusyEx
        {
            subscanMutex.acquire();
            if (bdbToAcquire != 0) {
                /* Subscan still in progress throw exception */
                subscanMutex.release();
                throw new DeviceBusyEx();
            }

            AntTPSubscanData rv = new AntTPSubscanData(antennaName, 
                    polarizationNum,
                    beginTime,
                    endTime,
                    dataBuffer,
                    flagBuffer);
            subscanMutex.release();
            return rv;
        }


    private void waitForConnection() throws IOException {
        try {
            rxSocket = serverSocket.accept();
            socketInput = new DataInputStream(rxSocket.getInputStream());
        } catch (IOException ex) {
            logger.severe("Caught io exception closing socketServer..."
                    + " continuing: " + ex.toString());
            throw ex;
        }
    }

    public void run() {
        try {
            /* Wait for connection */
            logger.info("Waiting for connection " + antennaName + " pol " +
                    polarizationNum + " port number " + serverSocket.getLocalPort());
            waitForConnection();
            boolean timeStampSynch = false;
            logger.info(antennaName + " pol " + polarizationNum + " connected!");
            long newBeginTime = 0;
            long newEndTime = 0;
            while (!rxSocket.isClosed()) {
                int packetSize = socketInput.readByte() & 0x00FF;
                int readSize = packetSize;
                // logger.info("Packet size: " + packetSize + " to pol: " +
                 //       polarizationNum + " Acq Size: " + bdbToAcquire);
                /*
                 * The timestamp comes is as ns since ... so we have to be
                 * careful with the most significant bit. Use right shift to
                 * devide by 4 without sign extension. Then devide by 25 to give
                 * ACS units
                 */
                long timestamp = ((socketInput.readLong()) >>> 2) / 25;
                long readTime = timestamp;
                long currentLatency = TimeHelper.getTimeStamp().value
                        - timestamp;
                String msg;
                msg = antennaName + " pol " + polarizationNum + 
                    " read a data block that starts at " + Util.acsTimeToString(timestamp) + 
                    " & stops at " + Util.acsTimeToString(timestamp + (packetSize * SampleInterval));

                if ((currentLatency - maxLatency) > 500000) {
                    maxLatency = currentLatency;
                    logger.info(antennaName + " pol: " + polarizationNum
                            + " Max latency now: " + maxLatency);
                }
                subscanMutex.acquire();

                if (bdbToAcquire <= 0
                        || timestamp + (packetSize * SampleInterval) < beginTime
                        || timestamp > endTime) {
                    msg += " Discarding.";
                    logger.finest(msg);   
                     /*
                     * No subscan running or data is not in subscan ...throw
                     * data away
                     */

                    // TODO Remove this
                    // logger.info("Discarding BDB Timestamp: "+timestamp);
                    // ", packetSize: "+packetSize+", SampleInterval: "+SampleInterval+", beginTime: "+beginTime+", endTime: "+endTime+", bdbToAcquire: "+bdbToAcquire);
                    subscanMutex.release();
                    byte[] junkBuffer = new byte[8 * packetSize];
                    socketInput.readFully(junkBuffer);
                    timeStampSynch = false;
                    continue;
                }

                if (!timeStampSynch) {
                    // New starting time
                    long drift = timestamp
                            + (long) Math.ceil((double) (beginTime - timestamp)
                                    / SampleInterval) * SampleInterval;
                    msg += " This the first data block.";
                    logger.info(msg);
                    //msg = "";
                    newEndTime = endTime + drift - beginTime;
                    newBeginTime = drift;
                    //logger.info("** Offsetting begin: " + Util.acsTimeToString(newBeginTime)
                    //        + " EndTime: " + Util.acsTimeToString(newEndTime));
                    timeStampSynch = true;
                }

                /*
                 * Remove any portion of the packet that is before the subscan
                 */
                if (timestamp < newBeginTime) {
                    //logger.info("***** BDB Timestamp in the Limbo!!: "
                    //        + Util.acsTimeToString(timestamp));
                    int numJunkBlocks = (int) (newBeginTime - timestamp)
                            / SampleInterval;
                    byte[] junkBuffer = new byte[8 * numJunkBlocks];
                    socketInput.readFully(junkBuffer);
                    readSize -= numJunkBlocks;
                    readTime += numJunkBlocks * SampleInterval;
                }

                if (readSize > 0 && readTime < newEndTime) {
  
                    int numGoodBlocks = Math.min((int) (newEndTime - readTime)
                            / SampleInterval, readSize);
                    int bdbIndex = (int) (readTime - newBeginTime)
                            / SampleInterval;
                    socketInput.readFully(dataBuffer, bdbIndex * 8,
                            numGoodBlocks * 8);
                    for (int idx = 0; idx < numGoodBlocks; idx++) {
                        flagBuffer[bdbIndex + idx] = false;
                    }
                    numFlags -= numGoodBlocks;
                    bdbToAcquire -= numGoodBlocks;
                    readSize -= numGoodBlocks;
                    msg += " Got " + numGoodBlocks + " samples. " + bdbToAcquire + " remaining."; 
                }
                if (bdbToAcquire < 256) {
                    logger.info(msg);
                }

                if (bdbToAcquire <= 0) {
                    /* Subscan done, signal semaphore */
                    logger.info("Completed total-power data collection for " + antennaName + " pol " + polarizationNum);
                    timeStampSynch = false;
                    doneSignal.release();
                }

                subscanMutex.release();

                if (readSize > 0) {
                    byte[] junkBuffer = new byte[8 * readSize];
                    socketInput.readFully(junkBuffer);
                }
            }
            logger.info("Pol " + polarizationNum + " exiting while loop");
        } catch (IOException ex) {
            logger.info("Total data flagged when received at socket: "
                    + numFlags);
            logger.info("IO Exception in antenna " + antennaName + 
                    " polarization " + polarizationNum + ":" +ex.toString());
            ex.printStackTrace();
            // Normal Exit path, just go ahead and exit
        } catch (InterruptedException ex) {
            /* Probably closing down somehow */
            logger.warning(antennaName + " pol " + polarizationNum + " interrupted");
//            if (doneSignal != null)
//                doneSignal.release();
            subscanMutex.release();
            rxSocket = null;
            isClosed = true;
            return;
        }
        close(); /* Just to be sure, close */
    }

}

