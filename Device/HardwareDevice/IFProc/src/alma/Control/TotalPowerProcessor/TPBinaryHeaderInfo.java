/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ArrayComponent.java
 */
package alma.Control.TotalPowerProcessor;


import alma.Control.TPProcessorPackage.SubscanInfo;
import java.util.ArrayList;

public class TPBinaryHeaderInfo {
    /* These are the values that actually make it to the header */
    public String dataOID;
    public String execBlockId;
    public long time;
    public long interval;
    public int scanNum;
    public int subscanNum;
    public int numIntegration;
    public int numAntenna;
    public int numBaseband;
    public int numPolarization;
    public int numBins;

    /* These we need to allow us to reduce the data */
    protected int   averagingLength;
    protected int[] basebandOrder;
    protected int[] polarizationOrder;
    protected ArrayList<String> antennaList;
    protected long startTime;
    protected long endTime;


    public TPBinaryHeaderInfo(SubscanInfo       subscanInfo,
			      int               averagingLength,
			      ArrayList<String> antennaList) {
	dataOID      = subscanInfo.dataOID;
	execBlockId  = subscanInfo.execID;
	
	interval     = subscanInfo.endTime - subscanInfo.startTime;
	time         = subscanInfo.startTime + (interval /2);
	
	startTime    = subscanInfo.startTime;
	endTime      = subscanInfo.endTime;


	scanNum         = subscanInfo.scanNumber;
	subscanNum      = subscanInfo.subscanNumber;

	this.averagingLength = averagingLength;
	numIntegration  = (int)(interval / 
				(IFProcClient.SampleInterval * averagingLength));
	
	this.antennaList= antennaList;
	numAntenna      = antennaList.size();
	
	basebandOrder   = subscanInfo.baseband;
	numBaseband     = basebandOrder.length;

	polarizationOrder   = subscanInfo.polarization;
	numPolarization     = polarizationOrder.length;
	
	/* For now we only deal with a single Bin */
	numBins = 1;
    }
}
