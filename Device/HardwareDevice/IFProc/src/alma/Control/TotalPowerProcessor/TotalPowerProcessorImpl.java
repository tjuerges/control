/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ArrayComponent.java
 */
package alma.Control.TotalPowerProcessor;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.net.ServerSocket;
import alma.acs.time.TimeHelper;

import java.io.IOException;

import java.lang.ThreadGroup;
import java.lang.Thread;
import java.lang.Runnable;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;
import java.util.Collections;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.logging.Logger;


import alma.Control.TotalPowerProcessor.IFProcClient;
import alma.Control.TPProcessorPackage.SubscanInfo;
import alma.Control.TPProcessorOperations;

import alma.ControlExceptions.wrappers.AcsJDeviceBusyEx;
import alma.ControlExceptions.DeviceBusyEx;

import alma.ControlDeviceExceptions.IllegalConfigurationEx;
import alma.ControlDeviceExceptions.wrappers.AcsJIllegalConfigurationEx;

import alma.TotalPowerProcessorExceptions.IFProcClientTimeOutEx;
import alma.TotalPowerProcessorExceptions.wrappers.AcsJIFProcClientTimeOutEx;

import alma.TotalPowerProcessorExceptions.ProcessingThreadInterruptedEx;
import alma.TotalPowerProcessorExceptions.wrappers.AcsJProcessingThreadInterruptedEx;

import alma.TotalPowerProcessorExceptions.BulkSenderEx;
import alma.TotalPowerProcessorExceptions.wrappers.AcsJBulkSenderEx;

public class TotalPowerProcessorImpl implements TPProcessorOperations{

    /* This Thread Group contains all of the threads used to handle
       data coming in from the IFProcessors
     */
    protected ThreadGroup rxThreadGroup = new ThreadGroup("RxHandlerGroup");

    /* The Processor Clients are the objects that open and listen on a
       port for incoming data.  This Set is a holder for all of the 
       active ones */
    final private Set<IFProcClient>  processorClients = 
        Collections.synchronizedSet(new HashSet<IFProcClient>());

    class IFProcClientID {
        String antennaName;
        int polNum;
        public IFProcClientID(String antennaName, int polNum) {
            this.antennaName = antennaName;
            this.polNum = polNum;
        }        
    };
    
    final private Map<IFProcClientID, Thread>  processorClientThreads = 
        Collections.synchronizedMap(new HashMap<IFProcClientID, Thread>());
    
    /* This is the length (in number of samples) that we need when averaging
       the data.
       The defalut value of 1 corresponds to no averaging
     */ 
    protected int averagingLength = 1;

    /* Constant of 2000 kHz used to translate between the averaging length
       and the sample rate */
    final private int DEFAULTSAMPLERATE = 10000000/IFProcClient.SampleInterval;

    /* The list of antennas specified for this array, order is important since
       this is the order the data goes into the Binary header in 
     */
    private ArrayList<String> antennaList = null;

    /* Information needed to construct the binary header for the current
       subscan */
    private TPBinaryHeaderInfo tpHeader = null;

    /* BulkSender which stores the Binary attachments */
    private TPBulkDataSenderInterface sender;
    
    /* This is a semaphore which synchronizes access to the internal
       variable of the tpProcessorImpl */
    final private Semaphore tpProcMutex = new Semaphore(1);

    /* A list of the processes waiting to be notified when a subscan compeletes
       a null list indicates that no subscan in is progress*/
    private HashSet<Semaphore> completionSem = null;

    /* A thread pool for threads which actually process the data */
    final private ExecutorService processingThreads =
        Executors.newCachedThreadPool(Executors.defaultThreadFactory());

    /* This is the logger to use */
    private Logger logger = null;

    /** Error codes:
      *  0 - No error
      *  1 - TimeOutError trying to acquire the data from the IFProcs (IFProcClientTimeOutException)
      *  2 - Processing Thread interrupted (ProcessingThreadInterruptedException)
      *  3 - Erro trying to send the data through the BulkSender (BulkSenderException)
      **/

    private int errorCode = 0;

    public TotalPowerProcessorImpl(Logger logger, String bulkDataDistributorName) {
        this.logger = logger;
        logger.info("Creating the BulkSender");
        sender = new TPBulkDataSenderInterface(1, bulkDataDistributorName);
    }

    public void shutdown() {
        /* This method will cause all of the threads spawned to be
           shutdown.  It must be called when the TotalPowerReceiver is
           no longer in use.
         */

        /* Deleting the Bulk sender */ 
        sender.close(); 

        close();
        Thread.yield();
        removeClosedClients();
    }

    /* =============== Thread Lifecycle Methods ============= */

    /**
     * This method will close all IF Processor Clients associated with
     * this method
     */
    private void close() {
        processingThreads.shutdown();
        synchronized(processorClients) {
            Iterator<IFProcClient> iter = processorClients.iterator();
            while (iter.hasNext()) {
                IFProcClient client = iter.next();
                if (! client.isClosed()) {
                    client.close();
                }
            }
        }
    }

    /**
     * This method will remove any clients from the set which 
     * have been closed
     */ 
    private void removeClosedClients() {
        synchronized(processorClients) {
            Iterator<IFProcClient> iter = processorClients.iterator();
            while (iter.hasNext()) {
                IFProcClient client = iter.next();
                if (client.isClosed()) {
                    iter.remove();
                }
            }
        }
    }


    /* ================ External METHODS ===================== */

    public byte[] getIPAddress() {
        /* This method should return the IP address of the 
           computer it is running on.
         */
        byte[] address = null;
        try {
            address = InetAddress.getLocalHost().getAddress();
        } catch (UnknownHostException ex) {
            /* Should probably rethrow, but should never get this error */
            logger.severe("Error getting IP Address.");
        }
        return address;
    }

    synchronized public int addDigitizer(String antennaName,
                                         int polarization) {
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(0);
//            serverSocket.setSoTimeout(60000);
        } catch (IOException ex) {
            logger.severe("Failed to open new serverSocket.");
            return -1;		
        }
        int clientPort = serverSocket.getLocalPort();
        IFProcClient client = new IFProcClient(antennaName, polarization,
                serverSocket, logger);

        tpProcMutex.acquireUninterruptibly();
        processorClients.add(client);

        logger.info("Adding new Processor Client for Antenna: " +
                antennaName + " Polarization: " + polarization);

        Thread clientThread = new Thread(rxThreadGroup, client);
        clientThread.start();
        processorClientThreads.put(new IFProcClientID(antennaName, polarization),
                                   clientThread);
        tpProcMutex.release();

        // Just for debugging.
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        return clientPort;
    }

    synchronized public boolean isConnected(String antennaName, int polarization) {
        synchronized(processorClients) {
            logger.info("isConnected: Looking for antenna name " + antennaName +
                    " and pol " + polarization);
            for(Iterator<IFProcClient> iter = processorClients.iterator();
                iter.hasNext(); ) {
                IFProcClient proc = iter.next();
                if ( (proc.antennaName.equals(antennaName)) && 
                     (proc.polarizationNum == polarization) ) {
                    boolean r = proc.isConnected();
                    logger.info("Found, returning " + r);
                    return r;
                }
            }
        }
        // TODO it should throw an exception instead
        logger.info("Not found");
        return true;
    }

    public void interruptClient(String antennaName, int polarization) {
        synchronized(processorClientThreads) {
            for(Iterator<IFProcClientID> iter = processorClientThreads.keySet().iterator();
                iter.hasNext(); ) {
                IFProcClientID id = iter.next();
                if ( (id.antennaName.equals(antennaName)) && 
                     (id.polNum == polarization) ) {
                    logger.info("Interrupting client " + antennaName + " pol " + polarization);
                    processorClientThreads.get(id).interrupt();
                    synchronized(processorClients) {
                        IFProcClient clientToRemove = null;
                        for(Iterator<IFProcClient> iter2 = processorClients.iterator();
                            iter2.hasNext(); ) {
                            IFProcClient proc = iter2.next();
                            if ( (proc.antennaName.equals(antennaName)) && 
                                 (proc.polarizationNum == polarization) ) {
                                clientToRemove = proc;
                            }
                        }
                        processorClients.remove(clientToRemove);
                    }
                }
            }
        }
    }
    
    public void abortSubscan() 
    {
        tpProcMutex.acquireUninterruptibly();
        if (completionSem == null) {
            /* No subscan running, just return */
            tpProcMutex.release();
            return;
        }

        Semaphore localCompletionSem = new Semaphore(0);
        completionSem.add(localCompletionSem);
        tpProcMutex.release();

        abortSubscanAsynch();

        try {
            if (!localCompletionSem.tryAcquire(1, TimeUnit.SECONDS)) {
                logger.warning("Error: Timed out aborting subscan");
            }
        } catch (InterruptedException ex) {
            logger.warning("Interrupted aborting Subscan!");
        }
    }

    protected void abortSubscanAsynch()
    {
        synchronized(processorClients) {
            Iterator<IFProcClient> iter = processorClients.iterator();
            while (iter.hasNext()) {
                IFProcClient client = iter.next();
                try {
                    client.abortSubscan();
                } catch (InterruptedException ex) {
                    logger.warning("Interrupted exception waiting " +
                                   "for subscan to abort");
                }
            }
        }
    }


    /* Note, aborts all previous subscans */
    public void configureArray(String[] antennaList ) {

        /* All we really need here is the Antenna list _IN_ORDER_ */
        this.antennaList = new ArrayList<String>();
        for (int idx = 0; idx < antennaList.length; idx++){
            this.antennaList.add(idx,antennaList[idx]);
        }
        tpProcMutex.release();
    }



    /* Note, aborts all previous subscans */
    public void beginSubscan(SubscanInfo subscanInfo) 
        throws IllegalConfigurationEx, DeviceBusyEx
        {
            /* Clear the state, all threads should be ready for action at
               this point
             */
            tpProcMutex.acquireUninterruptibly();
            if (antennaList == null ){
                /* Throw Something */
                tpProcMutex.release();
                AcsJIllegalConfigurationEx ex = 
                    new AcsJIllegalConfigurationEx("There are no antennas " + 
                            "specified, did you forget to "+
                            "configure?");
                throw(ex.toIllegalConfigurationEx());
            }

            if (completionSem != null) {
                tpProcMutex.release();
                abortSubscan();
                tpProcMutex.acquireUninterruptibly();
            }

            completionSem = new HashSet<Semaphore>();

            tpHeader = new TPBinaryHeaderInfo(subscanInfo, averagingLength, 
                    antennaList);

            /* Construct the Subscan Data Block */
            Semaphore doneSem = new Semaphore(0);
            synchronized(processorClients) {
                Iterator<IFProcClient> iter = processorClients.iterator();
                /* Start all of the clients collecting data */
                while (iter.hasNext()) {
                    IFProcClient client = iter.next();
                    if (client.isClosed()) {
                        /* Get rid of any disconnected clients */
                        iter.remove();
                    } else {
                        try {
                            client.beginSubscan(subscanInfo.startTime, 
                                                subscanInfo.endTime, 
                                                doneSem);
                        } catch (InterruptedException ex) {
                            /* If this happens something very bad happened */
                            logger.severe("Unable to end client subscan. " +
                                    " This should not have happened... ever");
                        } catch (DeviceBusyEx ex) {
                            /* Ok so that Client is busy, it shouldn't be */
                            logger.severe("Client device was busy. " +
                                    " This should not have happened... ever");
                        }
                    }
                }
            }

            /* Launch the thread to handle the data and write it to the 
               archive */
            processingThreads.
                execute(new ProcessingThread(tpHeader.endTime-TimeHelper.getTimeStamp().value,
                                             doneSem, 
                                             processorClients.size(),
                                             this, logger));
            tpProcMutex.release();

        }

    public void subscanComplete(int timeout)
      throws IFProcClientTimeOutEx, ProcessingThreadInterruptedEx, BulkSenderEx {
        logger.fine("Waiting up to " + timeout + " seconds for all the total-power data.");
        if (errorCode > 0) {
            switch (errorCode) {
            case 1:
                /* IFProcClientTimeOutException */
            {
                AcsJIFProcClientTimeOutEx ex = new AcsJIFProcClientTimeOutEx();
                ex.setProperty("Details", "Timeout problem when trying to acquire the data from the IFProc clients");
                throw ex.toIFProcClientTimeOutEx();
            }
            case 2:
                /* ProcessingThreadInterruptedException */
            {
                AcsJProcessingThreadInterruptedEx ex = new AcsJProcessingThreadInterruptedEx();
                ex.setProperty("Details", "The TotalPower processing thread was interrupted");
                throw ex.toProcessingThreadInterruptedEx();
            }
            case 3:
                /* BulkSenderException */
            {
                AcsJBulkSenderEx ex = new AcsJBulkSenderEx();
                ex.setProperty("Details", "Problems with the BulkSender. NO data is going to be archived");
                throw ex.toBulkSenderEx();
            }
            }
        }


        tpProcMutex.acquireUninterruptibly();
        if (completionSem == null) {
            /* No subscan currently active */
            tpProcMutex.release();
            return;
        }

        Semaphore localCompletionSem = new Semaphore(0);
        completionSem.add(localCompletionSem);
        tpProcMutex.release();

        try {
            if (!localCompletionSem.tryAcquire(timeout,
                    TimeUnit.SECONDS)){
                logger.fine("Timed out waiting for all the total-power data.");
                AcsJIFProcClientTimeOutEx ex = 
                     new AcsJIFProcClientTimeOutEx("Timeout problem when trying to acquire the data from the IFProc clients");
                throw(ex.toIFProcClientTimeOutEx());
            }
        } catch (InterruptedException ex) {
                AcsJProcessingThreadInterruptedEx nex = 
                     new AcsJProcessingThreadInterruptedEx("The TotalPower processing thread was interrupted");
                throw(nex.toProcessingThreadInterruptedEx());
        }
    }

    public ArrayTPSubscanData getSubscanData() 
        throws InterruptedException
        {
            /* This method currently is only available in Java.  Once we know
               what the return should look like as an IDL structure and if it
               is needed it can be turned into a CORBA method.
             */
            HashSet<AntTPSubscanData> rawData = getRawTotalPowerData();
            ArrayTPSubscanData arrayData = reduceSubscanData(tpHeader, rawData);
            return arrayData;
        }

    public void setIntegrationDuration(double integrationDuration) {
        /* Set the averaging timescale.  We only do integer amounts of
         * averaging and the minimum interval is 0.5ms
         */
        averagingLength = (int) Math.round(DEFAULTSAMPLERATE*integrationDuration);
        if (averagingLength < 1) averagingLength = 1;
    }

    public double getIntegrationDuration() {
        return averagingLength / (double) DEFAULTSAMPLERATE;
    }


    /* =========================== Internal methods ============== */
    protected void processSubscanData() {
        TPBinaryHeaderInfo localHeader = tpHeader;
        HashSet<AntTPSubscanData> tpData = null;
        try {
            /* Get all of the data so we have a local copy */
            tpData = getRawTotalPowerData();
            tpProcMutex.acquire();

            /* Release all messages that were pending */
            Iterator<Semaphore> iter = completionSem.iterator();
            while (iter.hasNext()) {
                Semaphore sem = iter.next();
                sem.release();
            }
            completionSem = null;

            tpProcMutex.release();

        } catch (InterruptedException ex) {
            logger.warning("Warning subscan data not processed!");
            /* Error code 2 - Processing thread interrupted */
            errorCode = 2;
            return;
        }

        ArrayTPSubscanData arrayData = reduceSubscanData(localHeader, tpData);

        /* Launch to archiving of the data */
        logger.info("Size of arrayData to be archived:" +arrayData.tpData.length);
        logger.info("Calling archiveblob() on sender"); 
        try{
           sender.archiveBlob(arrayData.headerInfo, arrayData.tpData, arrayData.tpFlags);
        } catch (BulkSenderEx ex){
            /*  3 - Erro trying to send the data through the BulkSender */
            errorCode = 3;
        }
    }

    private HashSet<AntTPSubscanData> getRawTotalPowerData()
        throws InterruptedException
        {
            tpProcMutex.acquire();
            if (completionSem != null) {
                // Throw busy exception
                tpProcMutex.release();
            }
            HashSet<AntTPSubscanData> tpData = new HashSet<AntTPSubscanData>();

            synchronized(processorClients) {
                Iterator<IFProcClient> iter = processorClients.iterator();
                /* Start all of the clients collecting data */
                while (iter.hasNext()) {
                    IFProcClient client = iter.next();
                    try {
                        tpData.add(client.getSubscanData());
                    } catch (InterruptedException ex) {
                        logger.warning("Interrupted getting Subscan data");
                    } catch (DeviceBusyEx ex) {
                        logger.warning("Client reported still being busy!");
                    }
                }
                tpProcMutex.release();
            }
            return tpData;
        }

    private ArrayTPSubscanData reduceSubscanData(TPBinaryHeaderInfo header,
            HashSet<AntTPSubscanData> antDataSet)
    {
        /* The order of the arrays are:
           [numIntegration][numAntenna][numBasebands][numBin][numPolarization]
         */


        /* The integrationSpacing is the distance between the same value in the
           structure on successive integrations */
        int integrationSize = header.numAntenna * header.numBaseband * 
            header.numPolarization;
        int binarySize = header.numIntegration * integrationSize;

        float[] tpData = new float[binarySize];
        int[]   dataFlags = new int[binarySize];

        /* Set the default value of the data flags to the no data error condition
           (using 1 for now) 
         */
        for (int idx = 0; idx < dataFlags.length; idx++)
            dataFlags[idx] = 1; // 1 means that the data is bad, so we start thinking that is not good.


        float[] bbAcume = new float[IFProcClient.NumChannelPerSample];
        int     numSamples;
        Long lost = new Long(0);

        Iterator<AntTPSubscanData> iter = antDataSet.iterator();
        while (iter.hasNext()){
            AntTPSubscanData antData = iter.next(); 

            /* Find the antenna number and Polarization number */
            int antIdx = antennaList.indexOf(antData.antennaName);
            if (antIdx == -1) {
                /* This antenna is not in the specified array discard */
                logger.info("Discarding unrequested antenna: " +
                        antData.antennaName);
                continue;
            }

            int polIdx = -1;
            for (int idx = 0; idx < header.numPolarization; idx++) {
                if (header.polarizationOrder[idx] == antData.polarizationNum) {
                    polIdx = idx;
                    break;
                }
            }
            if (polIdx == -1) {
                /* This antenna is not in the specified array discard */
                logger.info("Discarding unrequested polarization:" + 
                        "Polarization " + antData.polarizationNum);
                continue;
            }

            /* Check that the start and end times match */
            if (header.startTime != antData.subscanBeginTime ||
                    header.endTime   != antData.subscanEndTime) {
                /* Incorrect data discard */
                logger.warning("Discarding data from incorrect time");
                continue;
            }

            /* This is where this particular clients data fits in the 
               integration */
            int clientOffset = (antIdx * header.numBaseband * header.numPolarization)
                + polIdx;
            int dataOffset;
            int flagOffset;

            /* Integrate the correct number of samples and 
               put the result in the array */
            for (int integration = 0; integration < header.numIntegration; 
                    integration++) {
                numSamples = 0;
                flagOffset = integration * header.averagingLength;

                /* Zero the accumulation register */
                for (int bbidx = 0; bbidx < IFProcClient.NumChannelPerSample;
                        bbidx++)
                    bbAcume[bbidx] = 0.0F;

                /* Accumulate the data */
                for (int idx = 0; idx < header.averagingLength; idx++ ) {
                    if (! antData.dataFlags[flagOffset + idx]) {
                        numSamples++;
                        dataOffset = (flagOffset + idx )
                            * IFProcClient.NumChannelPerSample;
                        for (int bbidx = 0;
                                bbidx < IFProcClient.NumChannelPerSample; bbidx++)
                            bbAcume[bbidx] += 0x0000FFFF &
                                antData.dataBuffer.get(dataOffset+bbidx) ;

                    }
                }

                /* Fill the output data buffer */
                for (int bbidx = 0; bbidx < header.numBaseband; bbidx++) {
                    int idx = (integration * integrationSize) + clientOffset + 
                        (bbidx * header.numPolarization);

                    if (numSamples > 0) {
			/* Here the minus 1 is necessar because the baseband
			   order is 1 based and the indexes are 0 based */
                        tpData[idx] = bbAcume[header.basebandOrder[bbidx]-1] / 
			    numSamples;
                        if (numSamples == averagingLength) 
                            dataFlags[idx] = 0;
                        else 
                            dataFlags[idx] = 1;
                    } else {
                        lost++;
                        tpData[idx] = 0.0F;
                        dataFlags[idx] = 1;
                    }
                }
            }
        }
        if  (lost.intValue() != 0){
            logger.info("Total data lost = "+lost.toString());
        }
        ArrayTPSubscanData arrayData = new ArrayTPSubscanData(header,
                tpData,
                dataFlags);
        return arrayData;
    }

    private class ProcessingThread implements Runnable {
        /* Archiving thread:
           waits for semaphore (w/ timeout)
           reports the data to the TotalPowerProcessor
           sends the data to the Archive
           produces Archived Event?
         */

        final private long TOTAL_POWER_TIMEOUT = 2000; // two seconds

        Semaphore               doneSem     = null;
        int                     numClients  = 0;
        TotalPowerProcessorImpl tpProc      = null;
        long                    timeout;
        Logger logger = null;
        long                    beforeAcquiered;
        long                    afterAcquiered;

        public ProcessingThread(long                    subscanDuration,
                Semaphore               doneSem,
                int                     numClients,
                TotalPowerProcessorImpl tpProc,
                Logger logger) {
            this.timeout = (subscanDuration/10000) + TOTAL_POWER_TIMEOUT; //Time in milliseconds
            this.doneSem    = doneSem;
            this.numClients = numClients;  
            this.tpProc     = tpProc;
            this.logger     = logger;
        }

        public void run() {
            logger.fine("The total-power processing thread is waiting for the "+
                        " IF processor to acquire the data. Will time-out after "+
                        String.format("%.3f", timeout*1E-3) +" seconds.");
            beforeAcquiered = TimeHelper.getTimeStamp().value;

            try {
                if (!doneSem.tryAcquire(numClients, timeout, 
                            TimeUnit.MILLISECONDS)) {
                    long crashTime = TimeHelper.getTimeStamp().value;
                    logger.severe("Waited for "+
                                  (new Float((crashTime-beforeAcquiered)/10000000)).toString()+
                                  " seconds, but the IFProc clients couldn't receive the data,"+
                                  " aborting...");
                    /* Error code 1 - TimeOut error trying */
                    /* to acquire the data from the IFProcs */
                    errorCode = 1;
                    tpProc.abortSubscanAsynch();
                    Thread.yield();
                    return;
                    //if (!doneSem.tryAcquire(numClients, TOTAL_POWER_TIMEOUT, 
                    //            TimeUnit.MILLISECONDS)) {
                    //    /* hmmm still failed */
                    //    return;
                    //}
                }

            } catch (InterruptedException ex) {
                /* Error code 2 - Thread interrupted/aborted  */
                errorCode = 2;
            }
            afterAcquiered = TimeHelper.getTimeStamp().value;
            /* Deal with if we were interrupted */
            logger.fine("The IFProc Clients received all the data after " +
                        String.format("%.3f", (afterAcquiered - beforeAcquiered)*100E-9) +
                        " seconds. Starting the post processing...");
            tpProc.processSubscanData();
        }
    }

}
