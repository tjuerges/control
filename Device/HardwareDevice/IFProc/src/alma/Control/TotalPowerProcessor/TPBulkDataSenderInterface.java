/*
 *ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

/** 
 * @author  jkern
 * @version $Id$
 * @since    
 */

package alma.Control.TotalPowerProcessor;

import alma.TotalPowerProcessorExceptions.BulkSenderEx;
import alma.TotalPowerProcessorExceptions.wrappers.AcsJBulkSenderEx;

/* This class encapsulates the interface to the Bulk Data system
   the ACS implementation of Bulk Data is done only in C++ so
   we use JNI to get to it.  The underlying C++ library handles
   the requirements of managing the singleton interface, so this
   class can be relativly straight forward.

*/
public class TPBulkDataSenderInterface {
    
    /* Here is where I load the C++ library for the native calls */
    static {
    	System.out.println("Loading TPBulkDataSender library...");
    	System.loadLibrary("TPBulkDataSender");
    }

    /* The flow number specifies which flow is used */
    private int flowNumber;

    /* This flag lets us know if the interface has been allocated
       to a bulk flow.
    */
    boolean allocated = false;

    String distributorName;
    
    public TPBulkDataSenderInterface(int flowNumber, String distributorName) {
        System.out.println("TPBulkDataSenderInterface constructor called, FlowNumber:  "+flowNumber+" distributorName: "+distributorName);
        this.distributorName = distributorName;
        this.flowNumber = flowNumber;
	
    	allocated = allocateBulkFlow(flowNumber, distributorName);
    	if(allocated) {
    	    System.out.println("allocateBulkFlow() returned True");
        } else {
            System.out.println("allocateBulkFlow() returned False");
            RuntimeException ex = 
                 new RuntimeException("allocateBulkFlow() returned False");
            throw ex;
        }
    }

    public void finalize() {
    	close();
    }

    public void close() {
        if (allocated) {
            allocated = false;
            if (!deallocateBulkFlow(flowNumber, distributorName)) {
                /* There was an error deallocating the flow */
                System.out.println("Error Deallocating");
            }
        } else
            System.out.println("Not allocated when trying to close()");
    }

    public void archiveBlob(TPBinaryHeaderInfo header, float[] tpData, int[] tpFlags) throws BulkSenderEx {
        
        if (! allocated ) {
            AcsJBulkSenderEx ex = 
                 new AcsJBulkSenderEx("BulkSender NOT allocated!!");
            throw(ex.toBulkSenderEx());
        }

        System.out.println("About to archive Blob");
        
        /* Call the archive method */
        int maxSize=10000;
        if(tpData.length <= maxSize){
            if (! archiveBlob(flowNumber,
                        header.dataOID,
                        header.execBlockId,
                        header.time,
                        header.interval,
                        header.scanNum,
                        header.subscanNum,
                        header.numIntegration,
                        header.numAntenna,
                        header.numBaseband,
                        header.numPolarization,
                        header.numBins,
                        tpData,
                        tpFlags,
                        distributorName) ) {

                AcsJBulkSenderEx ex =
                     new AcsJBulkSenderEx("Blob archiving failed");
                throw(ex.toBulkSenderEx());
            }
        }else{
            System.out.println("Too much data, about to spliting it..");
            System.out.println("About to send header");
            if (! sendHeader(flowNumber,
                        header.dataOID,
                        header.execBlockId,
                        header.time,
                        header.interval,
                        header.scanNum,
                        header.subscanNum,
                        header.numIntegration,
                        header.numAntenna,
                        header.numBaseband,
                        header.numPolarization,
                        header.numBins,
                        distributorName) ) {
                /* Throw Someting */
                AcsJBulkSenderEx ex =
                     new AcsJBulkSenderEx("Send Blob header failed");
                throw(ex.toBulkSenderEx());
            }
            for(int i=0;i*maxSize < tpData.length;i++){
                int size=java.lang.Math.min(tpData.length-i*maxSize,maxSize);
                float[] tmpData=new float[size];
                int[] tmpFlags=new int[size];
                System.arraycopy(tpData,i*size,tmpData,0,size);
                System.arraycopy(tpFlags,i*size,tmpFlags,0,size);
                System.out.println("About to send data chunk");
                if (! sendDataChunk(tmpData, tmpFlags, distributorName) ) {
                    /* Throw Someting */
                    AcsJBulkSenderEx ex =
                         new AcsJBulkSenderEx("Send data chunck failed");
                    throw(ex.toBulkSenderEx());
                }
            }
            System.out.println("About to finalize sending");
            if (! finalizeSending( distributorName ) ) {
                /* Throw Someting */
                    AcsJBulkSenderEx ex =
                         new AcsJBulkSenderEx("Finalize sending failed");
                    throw(ex.toBulkSenderEx());
            }

        }
        System.out.println("Archiving seems to have succeded");
    }

    static public void writeBlob(String basePath, TPBinaryHeaderInfo header,
            float[] tpData, int[] tpFlags) {
        
        /* Call the method to write a blob to disk */
        if (! writeBlobToDisk(basePath,
			                  header.dataOID,
			                  header.execBlockId,
			                  header.time,
			                  header.interval,
			                  header.scanNum,
			                  header.subscanNum,
			                  header.numIntegration,
			                  header.numAntenna,
			                  header.numBaseband,
			                  header.numPolarization,
			                  header.numBins,
			                  tpData,tpFlags) ) {
            /* Throw Something */
        }
    }

    /* =============== JNI Interface ==================== */
    private native boolean allocateBulkFlow(int flowNumber, String distributorName);
   
    private native boolean deallocateBulkFlow(int flowNumber, String distributorName);
    
    private native boolean archiveBlob(int flowNumber,
				       String  dataOID,
				       String  execBlockID,
				       long    time,
				       long    interval,
				       int     scanNum,
				       int     subscanNum,
				       int     numIntegration,
				       int     numAntenna,
				       int     numBaseband,
				       int     numPolarization,
				       int     numBins,
				       float[] tpData,
				       int[]   tpFlags,
                                       String distributorName);

    private native boolean sendHeader(int flowNumber,
				      String  dataOID,
				      String  execBlockID,
				      long    time,
				      long    interval,
				      int     scanNum,
				      int     subscanNum,
				      int     numIntegration,
				      int     numAntenna,
				      int     numBaseband,
				      int     numPolarization,
				      int     numBins,
                                      String distributorName);

    private native boolean sendDataChunk(float[] tpData,
				         int[]   tpFlags,
                                         String distributorName);

    private native boolean finalizeSending(String distributorName);

    static public native boolean writeBlobToDisk(String  basePath,
						 String  dataOID,
						 String  execBlockID,
						 long    time,
						 long    interval,
						 int     scanNum,
						 int     subscanNum,
						 int     numIntegration,
						 int     numAntenna,
						 int     numBaseband,
						 int     numPolarization,
						 int     numBins,
						 float[] tpData,
						 int[]   tpFlags);
}
