/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* tjuerges  Jun 25, 2007  created
*/

#include <maciACSComponentDefines.h> // The MACI_DLL_SUPPORT_FUNCTIONS macro.
#include "IFProcSimImpl.h"
#include <logging.h>

/**
 * ----------------------------
 * MACI DLL support functions
 *----------------------------
 */
MACI_DLL_SUPPORT_FUNCTIONS(IFProcSimImpl)


IFProcSimImpl::IFProcSimImpl(const ACE_CString& name, maci::ContainerServices* cs):
    IFProcSimBase::IFProcSimBase(name, cs)
{
    ACS_TRACE("IFProcSimImpl::IFProcSimImpl");
}

IFProcSimImpl::~IFProcSimImpl()
{
    ACS_TRACE("IFProcSimImpl::~IFProcSimImpl");
}

//    throw(CORBA::SystemException, ControlDeviceExceptions::HwLifecycleEx)
void IFProcSimImpl::hwSimulationAction()
{
    ACS_LOG(LM_SOURCE_INFO, __FUNCTION__, (LM_INFO, "IFProcSimImpl::hwSimulationAction"));

    IFProcImpl::hwOperationalAction();
}
