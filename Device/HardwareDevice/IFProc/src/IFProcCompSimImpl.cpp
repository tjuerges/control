
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 */

#include "IFProcCompSimImpl.h"
#include <IFProcHWSimImpl.h>
#include <maciACSComponentDefines.h>
#include <TypeConversion.h>
#include <SimulatedSerialNumber.h>

using std::string;

/* Please use this class to implement alternative component, extending
 * the IFProcCompSimBase class. */

IFProcCompSimImpl::IFProcCompSimImpl(const ACE_CString& name, 
                                     maci::ContainerServices* pCS)
	: IFProcCompSimBase(name,pCS)
{
	const std::string fnName("IFProcCompSimImpl::IFProcCompSimImpl");
	AUTO_TRACE(fnName);
    std::string componentName(name.c_str());
    unsigned long long hashed_sn=AMB::Utils::getSimSerialNumber(componentName,"IFProc");
    std::ostringstream msg;
    msg<< "simSerialNumber for  " << name.c_str() << "  with assembly name " << "IFProc" << " is " << std::hex << "0x" << hashed_sn << std::endl;
    ACS_LOG(LM_SOURCE_INFO, fnName, (LM_DEBUG, msg.str().c_str()));

	string compName(name.c_str());

	std::vector<CAN::byte_t> sn;
	AMB::node_t node;

	// Set sn and node according to polarization 
	short polarization = compName[compName.size()-1];
	if (polarization) {
	  node = 0x01;
	  AMB::TypeConversion::valueToData(sn,hashed_sn, 8U);
	} else {
	  node = 0x00;
	  AMB::TypeConversion::valueToData(sn,hashed_sn, 8U);
	}


	device_m = new AMB::IFProcHWSimImpl(node,sn);
        device_m->initialize(node, sn, pCS);
	simulationIf_m.setSimObj(device_m);

}

IFProcCompSimImpl::~IFProcCompSimImpl()
{
        const char* __METHOD__ = "IFProcCompSimImpl::~IFProcCompSimImpl";
	ACS_TRACE(__METHOD__);
        device_m->finalize();
}


/* --------------- [ MACI DLL support functions ] -----------------*/
MACI_DLL_SUPPORT_FUNCTIONS(IFProcCompSimImpl)
