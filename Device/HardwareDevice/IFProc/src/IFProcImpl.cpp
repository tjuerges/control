// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2005 - 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#include <IFProcImpl.h>
#include <errorStateHelper.h>
#include <TETimeUtil.h>
#include <acstime.h>
#include<configDataAccess.h>
#include <xmlTmcdbComponent.h>
#include <vector>

using std::string;
using std::vector;
using std::ostringstream;
using Control::AlarmInformation;
using ControlExceptions::CAMBErrorExImpl;
using ControlExceptions::CAMBErrorEx;
using ControlExceptions::INACTErrorExImpl;
using ControlExceptions::INACTErrorEx;
using ControlExceptions::XmlParserErrorExImpl;
using ControlDeviceExceptions::HwLifecycleExImpl;
using ControlDeviceExceptions::IllegalConfigurationExImpl;

/**
 *  IFProc Constructor
 */

IFProcImpl::IFProcImpl( const ACE_CString& name,
        maci::ContainerServices* cs)
: IFProcBase::IFProcBase(name, cs),
    MonitorHelper(),
    tmcdb_m(NULL),
    getTpData_guard(10000000ULL * 10ULL, 0), // 10 seconds
    tpProc(Control::NewTPP::_nil()),
    tppName(""),
    ifpEth(this),
    tpProcIsAllocated(false),
    daqIsActive(false),
    total_power_sideband_0_dbm_sp(this),
    total_power_sideband_1_dbm_sp(this),
    total_power_a_dbm_sp(this),
    total_power_b_dbm_sp(this),
    total_power_c_dbm_sp(this),
    total_power_d_dbm_sp(this)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

/**
 *  IFProc Destructor
 */
IFProcImpl::~IFProcImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    if (tmcdb_m != NULL) {
        delete tmcdb_m;
        tmcdb_m = NULL;
    }
}


/**
 * ------------------------------------------------------------------------------------------------
 *  Component Lifecycle Methods
 * ------------------------------------------------------------------------------------------------
 */

//
// throw(acsErrTypeLifeCycle::LifeCycleExImpl)
void IFProcImpl::initialize() {
    AUTO_TRACE(__PRETTY_FUNCTION__);

    CORBA::String_var compName(acscomponent::ACSComponentImpl::name());

    MonitorHelper::initialize(compName.in(), getContainerServices());
    IFProcBase::initialize();

    try {
        const ACE_CString preFix = cdbName_m + ":";
        {
          const ACE_CString propertyName = "TOTAL_POWER_SIDEBAND_0_DBM";
          total_power_sideband_0_dbm_sp =
            new baci::ROfloat(preFix + propertyName, getComponent(),
                         new TOTAL_POWER_DBM_RODevIO(this, 0),
                         true);
        }

        {
          const ACE_CString propertyName = "TOTAL_POWER_SIDEBAND_1_DBM";
          total_power_sideband_1_dbm_sp =
            new baci::ROfloat(preFix + propertyName, getComponent(),
                         new TOTAL_POWER_DBM_RODevIO(this, 1),
                         true);
        }

        {
          const ACE_CString propertyName = "TOTAL_POWER_A_DBM";
          total_power_a_dbm_sp =
            new baci::ROfloat(preFix + propertyName, getComponent(),
                         new TOTAL_POWER_DBM_RODevIO(this, 2),
                         true);
        }

        {
          const ACE_CString propertyName = "TOTAL_POWER_B_DBM";
          total_power_b_dbm_sp =
            new baci::ROfloat(preFix + propertyName, getComponent(),
                         new TOTAL_POWER_DBM_RODevIO(this, 3),
                         true);
        }

        {
          const ACE_CString propertyName = "TOTAL_POWER_C_DBM";
          total_power_c_dbm_sp =
            new baci::ROfloat(preFix + propertyName, getComponent(),
                         new TOTAL_POWER_DBM_RODevIO(this, 4),
                         true);
        }

        {
          const ACE_CString propertyName = "TOTAL_POWER_D_DBM";
          total_power_d_dbm_sp =
            new baci::ROfloat(preFix + propertyName, getComponent(),
                         new TOTAL_POWER_DBM_RODevIO(this, 5),
                         true);
        }

    } catch (XmlParserErrorExImpl& ex) {
    // getElement can throw this exception
      acsErrTypeLifeCycle::LifeCycleExImpl nex(ex, __FILE__, __LINE__,
                                               __PRETTY_FUNCTION__);
      nex.log();
      throw nex;
    }



    std::string threadName(compName.in());
    threadName +=  "implMonitorThread";
    implMonitorThread_p = getContainerServices()->getThreadManager()->
        create<IFProcImplMonitorThread, IFProcImpl* const>(threadName.c_str(), this);
    implMonitorThread_p->setSleepTime(10*TETimeUtil::TE_PERIOD_ACS);    // 10*48ms
    implMonitorThread_p->suspend();

    //register alarms.
    initializeAlarms("IFProc", compName.in(), createAlarmVector());
}

void IFProcImpl::cleanUp()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        Control::HardwareDeviceImpl::hwStop();
    } catch(...) {
        LOG_TO_OPERATOR(LM_WARNING, "Caught an exception during transition to "
            "the hardware stop state.  Continuing anyway.");
    }

    forceTerminateAllAlarms();
    getContainerServices()->getThreadManager()->stopAll();
    try {
        MonitorHelper::cleanUp();
        IFProcBase::cleanUp();
    } catch (acsErrTypeLifeCycle::LifeCycleExImpl& ex) {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    }
}


/**
 * ------------------------------------------------------------------------------------------------
 *  Hardware Lifecycle Methods
 * ------------------------------------------------------------------------------------------------
 */
void IFProcImpl::hwConfigureAction() {
    AUTO_TRACE(__PRETTY_FUNCTION__);

    IFProcBase::hwConfigureAction();
    if (tmcdb_m == NULL)
        tmcdb_m = new XmlTmcdbComponent(getContainerServices());

}

//
// throw (ControlDeviceExceptions::HwLifecycleEx)
void IFProcImpl::hwInitializeAction() {
    AUTO_TRACE(__PRETTY_FUNCTION__);

    IFProcBase::hwInitializeAction();
    MonitorHelper::resume();

    forceTerminateAllAlarms();
    int sn;

    try{
        sn = getSerialNumberCode();
    } catch (const CAMBErrorExImpl& ex) {
        HwLifecycleExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getHwLifecycleEx();
    } catch (const INACTErrorExImpl& ex) {
        HwLifecycleExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getHwLifecycleEx();
    }

    std::stringstream temp_str;
    temp_str << "ifp_";
    temp_str << sn;
    std::string config_file = temp_str.str();

    // Read the Configuration for the IFProcessor instance
    std::string xml = tmcdb_m->getConfigXml(config_file);
    std::string xsd = tmcdb_m->getConfigXsd("ifproc");

    ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_INFO, "XML = %s", xml.c_str()));
    ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_INFO, "XSD = %s", xsd.c_str()));
    if(xml.size() == 0 || xsd.size() == 0) {
      ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_INFO, "XML or XSD data has zero length"));
    }

    try {
        configData_m = new ConfigDataAccessor(xml, xsd);
    } catch (XmlParserErrorExImpl& ex) {
        std::string msg = "Could not access config data. Now trying to load the default values from: ifp_default.xml and ifproc.xsd";
        ex.addData("Detail", msg);
        ex.log(LM_WARNING);
        try {
            // Read a default configuration for the IFProcessor instance
            std::string xml = tmcdb_m->getConfigXml("ifp_default");
            std::string xsd = tmcdb_m->getConfigXsd("ifproc");
            ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_WARNING, "Using default files XML = %s", xml.c_str()));
            ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_WARNING, "Using default files XSD = %s", xsd.c_str()));
            configData_m = new ConfigDataAccessor(xml, xsd);
        } catch (XmlParserErrorExImpl& ex1) {
            std::string msg = "It was impossible to use the default configuration files. Going on anyhow";
            ex1.addData("Detail", msg);
            ex1.log(LM_WARNING);
            configData_m = NULL;
        }
    }

    readIFProcConfig();

    // Set the correct Time at the IFProc
    try{
        resyncIFProcTime();
    } catch (const CAMBErrorExImpl& ex){
        HwLifecycleExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getHwLifecycleEx();
    } catch (const INACTErrorExImpl& ex){
        HwLifecycleExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getHwLifecycleEx();
    }

}

void IFProcImpl::hwOperationalAction(){
    ACE_TRACE(__PRETTY_FUNCTION__);
    IFProcBase::hwOperationalAction();

    //Reset all the error flags and the Alarms
    forceTerminateAllAlarms();

    implMonitorThread_p->resume();
}


void IFProcImpl::hwStopAction(){
    ACE_TRACE(__PRETTY_FUNCTION__);

    AmbErrorCode_t flushStatus;
    ACS::Time      flushTime;
    MonitorHelper::suspend();
    implMonitorThread_p->suspend();

    /* Flush all commands and monitors */
    flushNode(0, &flushTime, &flushStatus);
    if (flushStatus != AMBERR_NOERR) {
        ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,
                (LM_INFO,"Communication failure flushing commands to device"));
    } else {
        ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,
                (LM_DEBUG,"IFProc commands flushed at: %lld",flushTime));
    }
    IFProcBase::hwStopAction();
    forceTerminateAllAlarms();
}


/**
 * ------------------------------------------------------------------------------------------------
 *  Error Handler methods
 * ------------------------------------------------------------------------------------------------
 */

void IFProcImpl::handleActivateAlarm(int newErrorFlag) {
    AUTO_TRACE(__PRETTY_FUNCTION__);

    IFProcBase::setError(createErrorMessage());

}

void IFProcImpl::handleDeactivateAlarm(int newErrorFlag) {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    if (isAlarmSet()) {
        IFProcBase::setError(createErrorMessage());
    } else {
        IFProcBase::clearError();
    }
}

/**
 * ------------------------------------------------------------------------------------------------
 *  Get Total Power Method
 * ------------------------------------------------------------------------------------------------
 */
//throw(CORBA::SystemException,
//      ControlDeviceExceptions::IllegalConfigurationEx)
Control::IFProc::TotalPowerData*  IFProcImpl::getTpData(ACS::TimeInterval duration)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    const long long AcsTimeScale = 10000000LL;

    // Clear the vectors before aquiring data
    channelsTpData.clear();
    sidebandsTpData.clear();


    // Allow the threads to add the requested data to the vectors
    processingFlag = true;

    // the duration must be less than 30 seconds and more than 1 TE (48ms)
    if( (duration < (0.048 * AcsTimeScale)) || (duration > (30LL * AcsTimeScale))  )
    {
        IllegalConfigurationExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", "The duration of the total power data collection cannot "
                "be longer than 30s, or shorter than 48ms.");
        throw ex.getIllegalConfigurationEx();
    }
    else
        ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_DEBUG, "getTpData executed with 'duration' (100s of ns): %llu", duration));

    // Execute until the time stamp now >= start + duration.
    EpochHelper e(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));

    // Start in 20.5 TEs = 1s from now on. Gives time to set the rest up.
    const ACS::Time startTE(TETimeUtil::nearestTE(e.value()).value
            + 20ULL * TETimeUtil::TE_PERIOD_ACS + (TETimeUtil::TE_PERIOD_ACS >> 1));

    e.value(startTE + duration);
    const ACS::Time stopTE(TETimeUtil::nearestTE(e.value()).value);

    ACS::Time targetTE(startTE);

    // Number of data points to be monitored by getTpData
    CORBA::Long numberOfDataPoints = (stopTE - startTE) / TETimeUtil::TE_PERIOD_ACS;

    ACS_LOG(
            LM_SOURCE_INFO, __PRETTY_FUNCTION__, (
                LM_DEBUG, "Start TE = %s, stop TE = %s, # of data points = %ld.",
                TETimeUtil::toTimeDateString(startTE).c_str(),
                TETimeUtil::toTimeDateString(stopTE).c_str(),
                numberOfDataPoints));

    // Initialize and Start the thread that proccess the CAN messages for Total Power data

    // Initializing the semaphores for the vector filling mechanism
    //    sem_init(&semSbTpData, 0 , 0);
    //    sem_init(&semChTpData, 0 , 0);

    //Loop enqueuing all the request for the Channel and SideBand Total Power
    for(int i = 0; i < numberOfDataPoints; i++, targetTE += TETimeUtil::TE_PERIOD_ACS)
    {
        MonitorHelper::AMBRequestStruct* SbTpReq = getRequestStruct();
        MonitorHelper::AMBRequestStruct* ChTpReq = getRequestStruct();

        SbTpReq->RCA        = getMonitorRCADataMonitor1();
        ChTpReq->RCA        = getMonitorRCADataMonitor2();
        SbTpReq->TargetTime = targetTE;
        ChTpReq->TargetTime = targetTE;

        monitorTE(SbTpReq->TargetTime,
                SbTpReq->RCA,
                SbTpReq->DataLength,
                SbTpReq->Data,
                SbTpReq->SynchLock,
                &SbTpReq->Timestamp,
                &SbTpReq->Status);
        queueRequest(SbTpReq);

        monitorTE(ChTpReq->TargetTime,
                ChTpReq->RCA,
                ChTpReq->DataLength,
                ChTpReq->Data,
                ChTpReq->SynchLock,
                &ChTpReq->Timestamp,
                &ChTpReq->Status);
        queueRequest(ChTpReq);
    }


    // Wait for the duration plus 1 second for the startUp and 1 second for delay
    sleep(static_cast<unsigned int>(ceil(duration/AcsTimeScale)+2));
    processingFlag = false;

    std::vector<  MonitorHelper::AMBRequestStruct >::iterator sbIter(sidebandsTpData.begin());
    std::vector<  MonitorHelper::AMBRequestStruct >::iterator chIter(channelsTpData.begin());

    ACS::Time teIndex(0ULL);

    Control::IFProc::TotalPowerData_var totalPower = new Control::IFProc::TotalPowerData ;
    (*totalPower).sideband0.length(numberOfDataPoints);
    (*totalPower).sideband1.length(numberOfDataPoints);
    (*totalPower).channelA.length(numberOfDataPoints);
    (*totalPower).channelB.length(numberOfDataPoints);
    (*totalPower).channelC.length(numberOfDataPoints);
    (*totalPower).channelD.length(numberOfDataPoints);

    // Initialize all the data as wrong data
    for (int i=0;i<numberOfDataPoints;i++){
        (*totalPower).sideband0[i]=-1;
        (*totalPower).sideband1[i]=-1;
        (*totalPower).channelA[i]=-1;
        (*totalPower).channelB[i]= -1;
        (*totalPower).channelC[i]= -1;
        (*totalPower).channelD[i]= -1;
    }

    double tpDataConversionScale = 3.814697266e-5;
    for(; sbIter != sidebandsTpData.end(); sbIter++, chIter++)
    {
        EpochHelper e((*sbIter).Timestamp);
        teIndex = ((*sbIter).TargetTime - startTE)/TETimeUtil::TE_PERIOD_ACS;

        ACS::Time Tstamp = TETimeUtil::nearestTE(e.value()).value - TETimeUtil::TE_PERIOD_ACS/2;
        if( ((*sbIter).Status    == AMBERR_NOERR)
                && ( Tstamp == (startTE + teIndex * TETimeUtil::TE_PERIOD_ACS))  )
        {
            unsigned short tmp;
            unsigned char* byteptr = reinterpret_cast< unsigned char* >(&tmp);
            byteptr[1] = (*sbIter).Data[0];
            byteptr[0] = (*sbIter).Data[1];

            (*totalPower).sideband0[teIndex] = ( 10 * log10((static_cast<double>(tmp) * tpDataConversionScale) * ifprocConfig.slope[0] + ifprocConfig.icept[0]));
            byteptr[1] = (*sbIter).Data[2];
            byteptr[0] = (*sbIter).Data[3];
            (*totalPower).sideband1[teIndex] = ( 10 * log10((static_cast<double>(tmp) * tpDataConversionScale) * ifprocConfig.slope[1] + ifprocConfig.icept[1]));
        }


        e.value((*chIter).Timestamp);
        teIndex = ((*chIter).TargetTime - startTE)/TETimeUtil::TE_PERIOD_ACS;

        Tstamp = TETimeUtil::nearestTE(e.value()).value - TETimeUtil::TE_PERIOD_ACS/2;
        if( ((*chIter).Status    == AMBERR_NOERR)
                && ( Tstamp == (startTE + teIndex * TETimeUtil::TE_PERIOD_ACS)) )
        {
            unsigned short tmp;
            unsigned char* byteptr = reinterpret_cast< unsigned char* >(&tmp);
            byteptr[1] = (*chIter).Data[0];
            byteptr[0] = (*chIter).Data[1];
            (*totalPower).channelA[teIndex] = ( 10 * log10((static_cast<double>(tmp) * tpDataConversionScale) * ifprocConfig.slope[2] + ifprocConfig.icept[2]));
            byteptr[1] = (*chIter).Data[2];
            byteptr[0] = (*chIter).Data[3];
            (*totalPower).channelB[teIndex] = ( 10 * log10((static_cast<double>(tmp) * tpDataConversionScale) * ifprocConfig.slope[3] + ifprocConfig.icept[3]));
            byteptr[1] = (*chIter).Data[4];
            byteptr[0] = (*chIter).Data[5];
            (*totalPower).channelC[teIndex] = ( 10 * log10((static_cast<double>(tmp) * tpDataConversionScale) * ifprocConfig.slope[4] + ifprocConfig.icept[4]));
            byteptr[1] = (*chIter).Data[6];
            byteptr[0] = (*chIter).Data[7];
            (*totalPower).channelD[teIndex] = ( 10 * log10((static_cast<double>(tmp) * tpDataConversionScale) * ifprocConfig.slope[5] + ifprocConfig.icept[5]));
        }

    }
    channelsTpData.clear();
    sidebandsTpData.clear();

    return totalPower._retn();

}

/**
 * ------------------------------------------------------------------------------------------------
 *  MonitorThread response processors
 * ------------------------------------------------------------------------------------------------
 */

void IFProcImpl::processRequestResponse(const MonitorHelper::AMBRequestStruct& response){

    /* Response from SideBands Total Power */
    if (response.RCA == getMonitorRCADataMonitor1()) {
        processSbTp(response);
        return;
    }

    /* Response from Channels Total Power */
    if (response.RCA == getMonitorRCADataMonitor2()) {
        processChTp(response);
        return;
    }

    /* Every time the Alma Time is set to the IFProc, reset the Timing error Flag */
    if (response.RCA == getControlRCACntlAlmaTime()) {
        if (response.Status == AMBERR_NOERR) {
            setClearTimingError();
        }
        //ntroncos 2010-10-14: the thread had been suspended before sending this command.
        //    now we resume.
        implMonitorThread_p->resume();
        return;
    }
}

void IFProcImpl::processSbTp(const MonitorHelper::AMBRequestStruct& response){


    /* Check Status */
    if (response.Status != AMBERR_NOERR) {
        if(getTpData_guard.check() == true)
            ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,
                    (LM_WARNING,"SideBand Total Power Monitor Failed (Status = %d)",
                     response.Status));
    }

    if (processingFlag == true){
        // Add the requested struct to the vector
        sidebandsTpData.push_back(response);
    }
}

void IFProcImpl::processChTp(const MonitorHelper::AMBRequestStruct& response){

    /* Check Status */
    if (response.Status != AMBERR_NOERR) {
        if(getTpData_guard.check() == true)
            ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,
                    (LM_WARNING,"Channels Total Power Monitor Failed (Status = %d)",
                     response.Status));
    }

    if (processingFlag == true){
        // Add the requested struct to the vector
        channelsTpData.push_back(response);
    }
}



/**
 * ------------------------------------------------------------------------------------------------
 *  Side-Band and Base-Band Total Power [dbm] monitors
 * ------------------------------------------------------------------------------------------------
 */
//throw(CORBA::SystemException,
//        ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx)
Control::FloatSeq* IFProcImpl::getSBPower(ACS::Time& timestamp) {

    std::vector< float > SBvoltage;

    try{
        // Request for actual SB voltages
        SBvoltage = getDataMonitor1(timestamp);
    } catch (CAMBErrorExImpl& ex) {
        throw ex.getCAMBErrorEx();
    } catch (INACTErrorExImpl& ex) {
        throw ex.getINACTErrorEx();
    }

    // Convert the voltages into power
    std::vector<float> SBpower = voltageToPower(SBvoltage);

    // Return a CORBA float sequence
    Control::FloatSeq_var x(new Control::FloatSeq());
    x->length(SBpower.size());
    std::size_t i(0U);
    for(std::vector< float >::iterator iter(SBpower.begin()); iter != SBpower.end(); ++iter, ++i){
        x[i] = static_cast< CORBA::Float >(*iter);
    }

    return x._retn();
}
//throw(CORBA::SystemException,
//        ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx)
Control::FloatSeq* IFProcImpl::getBBPower(ACS::Time& timestamp) {

    std::vector< float > BBvoltage;
    try{
        // Request for actual BB voltages
        BBvoltage = getDataMonitor2(timestamp);
    } catch (CAMBErrorExImpl& ex) {
         throw ex.getCAMBErrorEx();
    } catch (INACTErrorExImpl& ex) {
        throw ex.getINACTErrorEx();
    }

    // Convert the voltages into power
    std::vector<float> BBpower = voltageToPower(BBvoltage);

    // Return a CORBA float sequence
    Control::FloatSeq_var x(new Control::FloatSeq());
    x->length(BBpower.size());
    std::size_t i(0U);
    for(std::vector< float >::iterator iter(BBpower.begin()); iter != BBpower.end(); ++iter, ++i){
        x[i] = static_cast< CORBA::Float >(*iter);
    }

    return x._retn();
}


/**
 * ------------------------------------------------------------------------------------------------
 *  Set Power level
 * ------------------------------------------------------------------------------------------------
 */

// throw(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx)
void IFProcImpl::setPowerLevel(CORBA::Float level) {
    //Set the maximum attenuation
    AUTO_TRACE(__PRETTY_FUNCTION__);
    std::vector<float> att;
    att.push_back(31.5);
    att.push_back(31.5);
    att.push_back(31.5);
    att.push_back(31.5);
    IFProcBase::setCntlGains(att);

    //TODO: Maybe we should add a delay here also
    sleep(1);

    // Set the level value - 5dbm
    setPowerLevelCore(level - 5);

    //TODO:  Here we should add a delay this should certainly be
    //       more graceful than this (perhaps a monitor TE).
    sleep(1);

    // Set the desired level value
    setPowerLevelCore(level);

    sleep(1);

    // Set the desired level value
    setPowerLevelCore(level);

}

void IFProcImpl::setPowerLevelCore(float level) {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACS::Time timeStamp;

    float newAttDifference;
    float error, last_error;
    try{

        // Set the AVE_DATA_LENGTH for a better estimation of the power
        IFProcBase::setCntlAverageLength(128);

        // Get the initial attenuations
        std::vector<float> temp_atten = IFProcBase::getGains(timeStamp);

        //This will be the resulting index vector of gains to be set
        std::vector<float> att_idx;

        //Get the real attenuations
        std::vector<float> actual_atten;
        for (int i = 0; i < 4; i++){
           actual_atten.push_back(ifprocConfig.atten[i][int(temp_atten[i]*2)]);

           att_idx.push_back(0.0);
        }


        // Get the BB voltages and convert them into power
        std::vector< float > BBpower = voltageToPower(IFProcBase::getDataMonitor2(timeStamp));

        ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_DEBUG, "The actual BBPower is BB0=%f BB1=%f BB2=%f BB3=%f ; Setting it to a level of %f [dbm]",BBpower[0],BBpower[1],BBpower[2],BBpower[3], level));


        // For each BaseBand, look into the attenuation's table, which is the best one
        for(int bb = 0;bb < 4;bb++){
           newAttDifference = BBpower[bb]-level;

           if (newAttDifference == 0){
              // Do nothing
              att_idx[bb] = temp_atten[bb];
           } // Checking the lower boundary
           else if ( newAttDifference <= (ifprocConfig.atten[bb][0]- actual_atten[bb]) ){
               att_idx[bb] = 0.0;
           } // Checking the upper boundary
           else if ( newAttDifference >= (ifprocConfig.atten[bb][63]- actual_atten[bb]) ){
               att_idx[bb] = 31.5;
           } // The attenuation should be increased
           else if (newAttDifference > 0){
              last_error = 1000;
              int attenuations;
              for(attenuations = int(temp_atten[bb]*2); attenuations < 64; attenuations++){
                 error = fabs((ifprocConfig.atten[bb][attenuations] - actual_atten[bb]) - newAttDifference);
                 if ( error <= last_error){
                    last_error = error;
                 } else {
                   break;
                 }
              }
              ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_DEBUG, "For BB%i, using attenuations idx %i, with an error of %f",bb,(attenuations-1), last_error));
              att_idx[bb] = float((attenuations-1))/2;
           } //The attenuation should be decreased
           else if (newAttDifference < 0){
              last_error = 1000;
              int attenuations;
              for(attenuations = int(temp_atten[bb]*2); attenuations >= 0; attenuations--){
                 error = fabs((ifprocConfig.atten[bb][attenuations] - actual_atten[bb]) - newAttDifference);
                 if ( error <= last_error){
                    last_error = error;
                 } else {
                   break;
                 }
              }
              ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_DEBUG, "For BB%i, using attenuations idx %i, with an error of %f",bb,(attenuations+1), last_error));
              att_idx[bb] = float((attenuations+1))/2;
           }
        }

    ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_DEBUG, "Summary of attenuations used = (%f, %f, %f, %f)",att_idx[0],att_idx[1],att_idx[2],att_idx[3]));
    // Now we can set the resulting gains
    IFProcBase::setCntlGains(att_idx);


    } catch (CAMBErrorExImpl& ex) {
        throw ex.getCAMBErrorEx();
    } catch (INACTErrorExImpl& ex) {
        throw ex.getINACTErrorEx();
    }
}

// get Attenuations for a certain TE
// throw (ControlExceptions::CAMBErrorExImpl)
std::vector< float > IFProcImpl::getAttenuationTE(ACS::Time epoch)
{
    MonitorHelper::AMBRequestStruct* AttenuationReq = getRequestStruct();
    requestTE(epoch, getMonitorRCAGains(), AttenuationReq);


    if (AttenuationReq->Status != AMBERR_NOERR ){
        throw CAMBErrorExImpl(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    }

    std::vector< unsigned char > raw(4);
    int iRawBytes = 0;
    for (int i = 0; i < 4; ++i) {
        raw[i] = AttenuationReq->Data[iRawBytes++];
    }
    return rawToWorldGains(std::vector< unsigned char >(raw));
}


// get BaseBand voltages for a certain TE
// throw (ControlExceptions::CAMBErrorExImpl)
std::vector< float > IFProcImpl::getBBvoltageTE(ACS::Time epoch)
{
    MonitorHelper::AMBRequestStruct* BBvoltageReq = getRequestStruct();;
    requestTE(epoch, getMonitorRCADataMonitor2(), BBvoltageReq);

    if (BBvoltageReq->Status != AMBERR_NOERR ){
        throw CAMBErrorExImpl(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    }

    std::vector< unsigned short > raw(4);
    int iRawBytes = 0;
    for (int i = 0; i < 4; ++i) {
        unsigned char* praw = reinterpret_cast< unsigned char* >(&raw[i]);
        for (int j = 0; j < 2; ++j)
            praw[j] = BBvoltageReq->Data[iRawBytes++];
    }

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.
    for(std::size_t i(0); i < 4; ++i)
    {
        char* p(reinterpret_cast< char* >(&(raw[i])));

        char tmp(p[0]);
        p[0] = p[1];
        p[1] = tmp;

    }
#endif
    return rawToWorldDataMonitor2(std::vector< unsigned short >(raw));
}

// Synchronous request for a certain TE
void IFProcImpl::requestTE(ACS::Time epoch, unsigned long rca, MonitorHelper::AMBRequestStruct* request){
    sem_t    synchLock;
    request->RCA  = rca;
    request->TargetTime = epoch;
    request->SynchLock  = &synchLock;
    sem_init(request->SynchLock, 0, 0);

    monitorTE(request->TargetTime,
            request->RCA,
            request->DataLength,
            request->Data,
            request->SynchLock,
            &request->Timestamp,
            &request->Status);

    sem_wait(request->SynchLock);
    sem_destroy(request->SynchLock);
}


// Convert the voltage measured by TPD, to real power
std::vector<float> IFProcImpl::voltageToPower(std::vector<float> BBvoltage){
    std::vector<float> powers;
    //If the vector is of size 4 its the BaseBands. If not its the sideBands.
    if (BBvoltage.size() == 4){ //BaseBand
        for(int i=0; i < 4; i++){
            powers.push_back(voltageToPower(BBvoltage[i], i+2));
        }
    }else { //Sidebands
        for(int i=0; i < 2; i++){
            powers.push_back(voltageToPower(BBvoltage[i], i));
        }
    }
    return powers;
}

float IFProcImpl::voltageToPower(float voltage, int index){
    float power;
     power = voltage * ifprocConfig.slope[index] + ifprocConfig.icept[index];
     if (power <= 0 ){
        power = -60.0;
     } else{
        power = static_cast<float>(10 * log10(power));
     }
     return power;
}

float IFProcImpl::getTotalPowerSideband0Dbm(ACS::Time& timestamp)
{
    float voltage = IFProcBase::getTotalPowerSideband0Dbm(timestamp);
    return voltageToPower(voltage, 0);
}

float IFProcImpl::getTotalPowerSideband1Dbm(ACS::Time& timestamp)
{
    float voltage = IFProcBase::getTotalPowerSideband1Dbm(timestamp);
    return voltageToPower(voltage, 1);
}

float IFProcImpl::getTotalPowerADbm(ACS::Time& timestamp)
{
    float voltage = IFProcBase::getTotalPowerADbm(timestamp);
    return voltageToPower(voltage, 2);
}

float IFProcImpl::getTotalPowerBDbm(ACS::Time& timestamp)
{
    float voltage = IFProcBase::getTotalPowerBDbm(timestamp);
    return voltageToPower(voltage, 3);
}

float IFProcImpl::getTotalPowerCDbm(ACS::Time& timestamp)
{
    float voltage = IFProcBase::getTotalPowerCDbm(timestamp);
    return voltageToPower(voltage, 4);
}

float IFProcImpl::getTotalPowerDDbm(ACS::Time& timestamp)
{
    float voltage = IFProcBase::getTotalPowerCDbm(timestamp);
    return voltageToPower(voltage, 5);
}


// Lookup table to conver the voltage measured by TPD, to real power
void IFProcImpl::readIFProcConfig(){
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(configData_m != NULL){
        // Read the Slope and icept calibration coeff

        try{
           std::auto_ptr<std::vector<ControlXmlParser> > list = configData_m->getElements("DET");
           for(unsigned int i= 0;i<(*list).size();i++){
              //Slope
              ifprocConfig.slope[i] = (*list)[i].getDoubleAttribute("slope");
              //Intercept
              ifprocConfig.icept[i] = (*list)[i].getDoubleAttribute("icept");
           }

           std::stringstream temp_ss;

           // Read the attenuation coeff
           list = configData_m->getElements("ATT");
           for(unsigned int i = 0;i<(*list).size();i++){
              for(int j = 0;j < 64;j++){
                 temp_ss.str("");
                 temp_ss << "att_";
                 temp_ss << floor(j/2);
                 temp_ss << "_";
                 temp_ss << ((j%2)*5);
                 ifprocConfig.atten[i][j] = (*list)[i].getDoubleAttribute(temp_ss.str().c_str());
              }
           }
           return;
        } catch (XmlParserErrorExImpl& ex) {
            ex.log(LM_ERROR);
        }
    }

    ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
            (LM_WARNING,"No XML file with configuration values was found. Using hardcoded default values!!"));
    // No calibration data for this particular IFProccesor
    // USING DEFAULT DATA (i.e using same as sn=101)
    //Slope
    ifprocConfig.slope[0] = 0.004532;
    ifprocConfig.slope[1] = 0.004747;
    ifprocConfig.slope[2] = 1.316582;
    ifprocConfig.slope[3] = 1.316582;
    ifprocConfig.slope[4] = 1.295269;
    ifprocConfig.slope[5] = 1.302271;
    //Intercept
    ifprocConfig.icept[0] = 0.00005558;
    ifprocConfig.icept[1] = 0.00013814;
    ifprocConfig.icept[2] = 0.68961770;
    ifprocConfig.icept[3] = 0.68961770;
    ifprocConfig.icept[4] = 0.82719556;
    ifprocConfig.icept[5] = 0.72539438;
    // Ideal attenuations
    for (int j = 0;j < 4;j++){
       for (int i = 0;i < 64;i++){
           ifprocConfig.atten[j][i] = i/2;
       }
    }
}


// Get the IFDC serial number, to determine which calibration values must be used.
//    throw(ControlExceptions::CAMBErrorExImpl,
//          ControlExceptions::INACTErrorExImpl)
int IFProcImpl::getSerialNumberCode()
{
        ACS::Time timeStamp;

        unsigned short sn;
        std::vector<unsigned char> moduleCodes = IFProcBase::getModuleCodes(timeStamp);
        unsigned char* byteptr = reinterpret_cast<unsigned char*>(&sn);
        
#if BYTE_ORDER != BIG_ENDIAN
        byteptr[1] = moduleCodes[6];
        byteptr[0] = moduleCodes[7];
#else
        byteptr[0] = moduleCodes[6];
        byteptr[1] = moduleCodes[7];
#endif

        // Hack for prototype IFProcs at the OSF, which report
        // serial number 255 on MODULE_CODES. Correct value should
        // be read from TPD_MODULE_CODES instead:
        if (sn == 255) {
            std::vector<unsigned char> moduleCodes = IFProcBase::getTpdModuleCodes(timeStamp);
            unsigned char* byteptr = reinterpret_cast<unsigned char*>(&sn);
            
#if BYTE_ORDER != BIG_ENDIAN
            byteptr[1] = moduleCodes[6];
            byteptr[0] = moduleCodes[7];
#else
            byteptr[0] = moduleCodes[6];
            byteptr[1] = moduleCodes[7];
#endif
        }

        return sn;
    }

/**
 * ------------------------------------------------------------------------------------------------
 *  Allocate
 * ------------------------------------------------------------------------------------------------
 */

//throw(CORBA::SystemException,
//        ControlDeviceExceptions::IllegalConfigurationEx)
void IFProcImpl::allocate(const char* name) {

    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(tpProcIsAllocated == true){
        // Oops, there is already one allocated. Throw an exception.
        ACS_LOG(LM_SOURCE_INFO, __FUNCTION__, (LM_WARNING, "Oops, there is already one allocated. Deallocating it and trying again."))
            deallocate();
    }

    ACE_Guard< ACE_Mutex > guard(accessMutex);

    if (tpProcIsAllocated == true) {
        IllegalConfigurationExImpl ex(__FILE__,
                __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", "Illegal allocation! There is already a total "
                "power processor allocated.");
        throw ex.getIllegalConfigurationEx();
    }

    // Reseting the daq Status
    daqIsActive = false;

//
//
//    try{
//        destIp = tprx->getIPAddress();
//    }
//    catch(...){
//        ControlDeviceExceptions::IllegalConfigurationExImpl ex(__FILE__, __LINE__, fnName);
//        ex.addData("Detail", "Unable to determine the destination IP address.");
//        throw ex.getIllegalConfigurationEx();
//    }
//
//    if(destIp->length() != 4){
//        ControlDeviceExceptions::IllegalConfigurationExImpl ex(__FILE__, __LINE__, fnName);
//        ex.addData("Detail", "Invalid destination IP address.");
//        throw ex.getIllegalConfigurationEx();
//    }
//
//    // Copy the IP-address into a std::vector.
//    std::size_t i(0);
//    for(std::vector< unsigned char >::iterator iter(
//                destinationIpAddress.begin());
//            iter != destinationIpAddress.end(); ++iter, ++i){
//        (*iter) = static_cast< unsigned char >(destIp[i]);
//    }
//
//    ACS_LOG(LM_SOURCE_INFO, __FUNCTION__, (LM_INFO,
//                "Destination IP address found:  %d %d %d %d",destIp[0],destIp[1],destIp[2],destIp[3]));

    std::string antennaName;
    CORBA::Long polarization(0L);

    // The port to connect to
    CORBA::UShort portToUse(0U);
    // The IP address to connect to
    std::vector< unsigned char > destinationIpAddress(4, 0U);

    // Get the TPP component
    maci::ContainerServices* cs = getContainerServices();
    tpProc=cs->getComponentNonSticky<Control::NewTPP>(name);
    tppName=name;

    try{
        ifpEth.init();
        antennaName  = ifpEth.findAntennaName();
        polarization = ifpEth.findPolarization();

        Control::NewTPP::ConnectionInfo_var tppIPAddr=tpProc->addDigitizer(antennaName.c_str(), polarization);
        portToUse=tppIPAddr->portNumber;
        // Copy the IP-address into a std::vector.
        std::size_t i(0);
        for (int t=0; t<4; t++) {
        	destinationIpAddress.push_back((static_cast< unsigned char >(tppIPAddr->IpAddress[t])));
        }
        for(std::vector< unsigned char >::iterator iter(
        		destinationIpAddress.begin());
        		iter != destinationIpAddress.end(); ++iter, ++i){
        	(*iter) = static_cast< unsigned char >(tppIPAddr->IpAddress[i]);
        }

        ACS_LOG(LM_SOURCE_INFO, __FUNCTION__, (LM_INFO, "Connecting to %d.%d.%d.%d:%d",
        		tppIPAddr->IpAddress[0],
        		tppIPAddr->IpAddress[1],
        		tppIPAddr->IpAddress[2],
        		tppIPAddr->IpAddress[3],
        		portToUse));
        sleep(1);
        ifpEth.establishTCPConnection(destinationIpAddress, portToUse);
        // verifying that the connection was really established

        int retries = 10;
        bool isConnected = false;
        while(retries > 0) {
        	if (tpProc->isConnected(antennaName.c_str(), polarization)) {
        		isConnected = true;
        		break;
        	}
        	sleep(1);
        	retries--;
        }

        ACS::Time timestamp;
        std::vector<unsigned char> status = getStatus(timestamp);
        ACS_LOG(LM_SOURCE_INFO, __FUNCTION__, (LM_INFO, "Connection status: %d", status[0]));

        if (!isConnected) {
        	ACS_LOG(LM_SOURCE_INFO, __FUNCTION__, (LM_INFO, "Not connected, interrupting"));
            // ifpEth.establishTCPConnection(destinationIpAddress, portToUse);
        	tpProc->interruptClient(antennaName.c_str(), polarization);
        }

//        portToUse = static_cast< CORBA::UShort >(tpProc->addDigitizer(antennaName.c_str(), polarization));
//        ACS_LOG(LM_SOURCE_INFO, __FUNCTION__, (LM_INFO, "Port found:  %d",portToUse));
//        sleep(1);
//        ifpEth.establishTCPConnection(destinationIpAddress, portToUse);

    }
    catch (IllegalConfigurationExImpl& ex) {
        tpProc = Control::NewTPP::_nil();
        throw ex.getIllegalConfigurationEx();
    }
    catch(...){
        IllegalConfigurationExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", "Unknown Exception trying to allocate the IFProc.");
        throw ex.getIllegalConfigurationEx();
    }

    ACS_LOG(LM_SOURCE_INFO, __FUNCTION__, (LM_INFO, "IFProc allocated."));
    tpProcIsAllocated = true;
}


//throw(CORBA::SystemException,
//        ControlDeviceExceptions::IllegalConfigurationEx)
void IFProcImpl::deallocate() {
    AUTO_TRACE(__PRETTY_FUNCTION__);

    ACE_Guard< ACE_Mutex > guard(accessMutex);

    if(tpProcIsAllocated == true){
        // Release tpProc.
        tpProcIsAllocated = false;
        tpProc = Control::NewTPP::_nil();
        try{
            ifpEth.disconnectTCPConnection();
        }
        catch(IllegalConfigurationExImpl& ex){
            throw ex.getIllegalConfigurationEx();
        }
        catch(...){
            ACS_LOG(LM_SOURCE_INFO, __FUNCTION__, (LM_INFO,
                        "Unknown Exception trying to deallocate the IFProc."));
            return;
        }

    }
    else{
        /**
         * Oops, there is no total power processor which can be deallocated.
         * Throw an exception.
         */
        ACS_LOG(LM_SOURCE_INFO, __FUNCTION__, (LM_WARNING, "Oops, there is no total power processor which can be deallocated."))
            /*        ControlDeviceExceptions::IllegalConfigurationExImpl ex(__FILE__,
                      __LINE__, __PRETTY_FUNCTION__);
                      ex.addData("Detail", "Illegal deallocation! There is no total power "
                      "processor allocated.");
                      throw ex.getIllegalConfigurationEx();
             */
    }
}
//throw(CORBA::SystemException,
//        ControlDeviceExceptions::IllegalConfigurationEx)
void IFProcImpl::beginDataAcquisition(ACS::Time timestamp) {

    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(daqIsActive == false){
        if(tpProcIsAllocated == true){
            try{
                setStartData();
            }
            catch (const CAMBErrorEx& ex) {
                IllegalConfigurationExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
                throw nex.getIllegalConfigurationEx();
            } catch (const INACTErrorEx& ex) {
                IllegalConfigurationExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
                throw nex.getIllegalConfigurationEx();
            } catch(const CORBA::SystemException& ex){
                IllegalConfigurationExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
                throw ex.getIllegalConfigurationEx();
            } catch(...) {
                ACS_LOG(LM_SOURCE_INFO, __FUNCTION__, (LM_INFO,
                            "Unknown Exception trying to start the data transmission."));
                return;
            }
        } else {
            IllegalConfigurationExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
            ex.addData("Detail", "No total power processor has been allocated.");
            throw ex.getIllegalConfigurationEx();
        }
    } else {
        IllegalConfigurationExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", "The total power data acquisition is already "
                   "running.");
        throw ex.getIllegalConfigurationEx();
    }
    ACS_LOG(LM_SOURCE_INFO, __FUNCTION__, (LM_INFO,
                                           "Data acquisition activated."));
    daqIsActive = true;
}

//throw(CORBA::SystemException,
//        ControlDeviceExceptions::IllegalConfigurationEx)
void IFProcImpl::abortDataAcquisition() {
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if (daqIsActive == false) return;
    daqIsActive = false;

    try{
        setStopData();
    } catch(const CAMBErrorEx& ex) {
        IllegalConfigurationExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getIllegalConfigurationEx();
    } catch (const INACTErrorEx& ex) {
        IllegalConfigurationExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getIllegalConfigurationEx();
    } catch(const CORBA::SystemException& ex) {
        IllegalConfigurationExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw ex.getIllegalConfigurationEx();
    } catch(...){
        ACS_LOG(LM_SOURCE_INFO, __FUNCTION__, (LM_INFO,
                    "Unknown Exception trying to stop the data transmission."));
        return;
    }

    ACS_LOG(LM_SOURCE_INFO, __FUNCTION__, (LM_INFO,
                "Data acquisition aborted."));
}

/**
  * set the IFProcessor's internal time
  */
//throw(CORBA::SystemException,
//        ControlExceptions::CAMBErrorExImpl, ControlExceptions::INACTErrorExImpl)
void IFProcImpl::resyncIFProcTime() {

    AUTO_TRACE(__PRETTY_FUNCTION__);


    if (isStartup() == false){
        INACTErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw ex.getINACTErrorEx();
    }

    //ntroncos 2010-10-14: Suspend the thread that monitors time since
    //                    we will be messing with it. Re enable after
    //                    in the method that process the response queue.
    //                    This method does not throw so its safe.
    implMonitorThread_p->suspend();

    static ACS::Time targetTE(0ULL);

    ACS_LOG(LM_SOURCE_INFO, __FUNCTION__, (LM_DEBUG,
                "Synchronising IFPRoc hardware clock with local time."));

    targetTE = TETimeUtil::nearestTE(TimeUtil::ace2epoch(ACE_OS::gettimeofday())).value;

    // Round the target TE up, it could be right now. Adding 3 TEs
    targetTE += (3 * TETimeUtil::TE_PERIOD_ACS);

    MonitorHelper::AMBRequestStruct* cmdReq;
    cmdReq = getRequestStruct();

    cmdReq->RCA        = getControlRCACntlAlmaTime();

    // Add half a TE for the control point.
    cmdReq->TargetTime = targetTE + TETimeUtil::TE_PERIOD_ACS;
    cmdReq->DataLength = 8;

    unsigned long long raw(0);

    // This time will be set at the IFProc in the next rissing edge of the TE,
    // So we must add  TE to the time
    raw = worldToRawCntlAlmaTime((const unsigned long long) targetTE + TETimeUtil::TE_PERIOD_ACS);


#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    char* p(reinterpret_cast< char* >(&raw));

    char tmp;
    for(int i=0;i<4;i++){
        tmp = p[i];
        p[i] = p[7-i];
        p[7-i] = tmp;
    }
#endif

    unsigned char* praw = reinterpret_cast< unsigned char* >(&raw);
    for (int i = 0; i < 8; ++i)
        cmdReq->Data[i] = praw[i];

    commandTE(cmdReq->TargetTime,
            cmdReq->RCA,
            cmdReq->DataLength,
            cmdReq->Data,
            cmdReq->SynchLock,
            &cmdReq->Timestamp,
            &cmdReq->Status);

    queueRequest(cmdReq);

}


void IFProcImpl::setSignalPaths(bool baseBandAHigh,
                                bool baseBandBHigh, 
                                bool baseBandCHigh, 
                                bool baseBandDHigh,
                                bool ABUpperSideBand,
                                bool CDUpperSideBand, ACS::Time when) {

    checkHwStateOrThrow(&HardwareDeviceImpl::isReady, 
                        __FILE__, __LINE__, __PRETTY_FUNCTION__);

    try {
        vector<bool> world(6);
        world[0] = baseBandAHigh;
        world[1] = baseBandBHigh;
        world[2] = baseBandCHigh;
        world[3] = baseBandDHigh;
        world[4] = ABUpperSideBand;
        world[5] = CDUpperSideBand;    
        
        unsigned char rawBytes[1];
        rawBytes[0] = worldToRawCntlSignalPaths(world)[0];
        const AmbRelativeAddr rca(getControlRCACntlSignalPaths() + AmbDeviceImpl::getBaseAddress());

        ACS::Time targetTE(0ULL);
       // acstime::Epoch target;
       // target = EpochHelper(when).value();
        targetTE = TETimeUtil::nearestTE(EpochHelper(when).value()).value;
        MonitorHelper::AMBRequestStruct* cmdReq;
        cmdReq = getRequestStruct();
        cmdReq->RCA = rca;
        cmdReq->TargetTime = targetTE;
        cmdReq->DataLength =  1U;
        cmdReq->Data[0] = rawBytes[0];

        commandTE(cmdReq->TargetTime,
                cmdReq->RCA,
                cmdReq->DataLength,
                cmdReq->Data,
                cmdReq->SynchLock,
                &cmdReq->Timestamp,
                &cmdReq->Status);
    
        queueRequest(cmdReq);

//        commandEnc(when, rca, 1U, reinterpret_cast< AmbDataMem_t* >(rawBytes));
    } catch (CAMBErrorExImpl& ex){
        throw ex.getCAMBErrorEx();
    }
}

void IFProcImpl::flush(ACS::Time when) {
    const AmbRelativeAddr rca(getControlRCACntlSignalPaths() + AmbDeviceImpl::getBaseAddress());
    ACS::Time timestamp;
    AmbErrorCode_t status;
    flushRCA(when, rca, &timestamp, &status);
}

std::vector<AlarmInformation> IFProcImpl::createAlarmVector()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    std::vector<AlarmInformation> aivector;
    {
        AlarmInformation ai;
        ai.alarmCode = NOTPPROC;
        ai.alarmDescription = "The TPProcessor offshoot reference seems to "
            "be invalid. Trying to retrieve the destination IP-address "
            "from the TPProcessor failed.";
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = UNKNOWN;
        ai.alarmDescription = "An unspecified exception has been caught. No further information is available.";
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = NOTPPROCCOMM;
        ai.alarmDescription = "Received IP-address does not contain 4 bytes.";
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = NOCONN;
        ai.alarmDescription = "Could not establish the TCP-connection.";
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = NOANTENNA;
        ai.alarmDescription = "Could not determine the antenna name.";
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = NOPOLARISATION;
        ai.alarmDescription = "Could not determine the polarization.";
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = NOIP;
        ai.alarmDescription = "Could not determine the IP-address of the IFProc-hardware.";
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = NOCANCOMM;
        ai.alarmDescription = "CAN-bus communication problem with the IFProc module.";
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = INACTIVE;
        ai.alarmDescription = "Control system is inactive.";
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = TCPINITFAIL;
        ai.alarmDescription = "TCP initialization failed.";
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = ADDFAIL;
        ai.alarmDescription = "ADDFAIL, ask Rodrigo Araya <raraya@nrao.edu>.";
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = TIMINGERRFLAG;
        ai.alarmDescription = "Timing Error, TE changed.";
        aivector.push_back(ai);
    }
    return aivector;
}



/**
 * Convert the raw value of READ_DATA_AVE_LENGTH to a world value.
 */
    unsigned short IFProcImpl::rawToWorldDataAveLength(
            const unsigned char raw) const throw()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    unsigned short ret(1U);
    ret = (ret << raw);

    return ret;
}

/**
 * Convert the raw value of READ_SIGNAL_PATHS to a world value.
 */
std::vector< bool > IFProcImpl::rawToWorldSignalPaths(const unsigned char raw) const throw() {
    std::vector< bool > ret(6);
    ret[0] = raw & 0x01;
    ret[1] = raw & 0x02;
    ret[2] = raw & 0x04;
    ret[3] = raw & 0x08;
    ret[4] = raw & 0x10;
    ret[5] = raw & 0x20;
    return ret;
}

/**
 * Convert the raw value of AVERAGE_LENGTH to a world value.
 */
unsigned char IFProcImpl::worldToRawCntlAverageLength(const unsigned short world) const throw() {

    unsigned char  raw(0);

    if((world < 1U) || (world > 512U)){
        ACS_LOG(LM_SOURCE_INFO, __FUNCTION__, (LM_INFO,
                    "Average Lenght out of range. It must be between 1 and 512 (limited to powers of 2)."));
        return raw;
    }else{
        // Must be a power of 2
        raw = short(floor(log(world)/log(2)));
        return raw;
    }
}

/**
 * Convert the raw value of SIGNAL_PATHS to a world value.
 */
std::vector< unsigned char > IFProcImpl::worldToRawCntlSignalPaths(const std::vector< bool >& world) const throw(){
    std::vector< unsigned char > ret(1);
    ret[0] = (0x01 & world[0]);
    ret[0] |= (0x01 & world[1]) << 1;
    ret[0] |= (0x01 & world[2]) << 2;
    ret[0] |= (0x01 & world[3]) << 3;
    ret[0] |= (0x01 & world[4]) << 4;
    ret[0] |= (0x01 & world[5]) << 5;
    return ret;
}


/**
 * Convert the raw value of START_COMM_TEST to a world value.
 */
std::vector< unsigned char > IFProcImpl::worldToRawStartCommTest(const std::vector< bool >& world) const throw(){
    std::vector<unsigned char> ret(1);
    ret[0] = (0x01 & world[0]);
    ret[0] |= (0x01 & world[1])<< 1;
    ret[0] |= (0x01 & world[2])<< 2;
    return ret;
}

IFProcImpl::TOTAL_POWER_DBM_RODevIO::TOTAL_POWER_DBM_RODevIO(IFProcImpl* base, const int index):
     Logging::Loggable(),
     device(base),
     index_m(index)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

IFProcImpl::TOTAL_POWER_DBM_RODevIO::TOTAL_POWER_DBM_RODevIO(
	IFProcImpl* base, const int index, const std::string& loggerName):
     Logging::Loggable(loggerName),
     device(base),
     index_m(index)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

IFProcImpl::TOTAL_POWER_DBM_RODevIO::TOTAL_POWER_DBM_RODevIO(
	IFProcImpl* base, const int index, Logging::Logger::LoggerSmartPtr logger):
     Logging::Loggable(logger),
     device(base),
     index_m(index)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

/**
 * Implement the destructor method in the ACS DevIO class for this BACI
 * property.
 */
IFProcImpl::TOTAL_POWER_DBM_RODevIO::~TOTAL_POWER_DBM_RODevIO()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

bool IFProcImpl::TOTAL_POWER_DBM_RODevIO::initializeValue()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    return false;
}

/**
 * Implement the read method in the ACS DevIO class for this BACI property.
 */

CORBA::Float IFProcImpl::TOTAL_POWER_DBM_RODevIO::read(
	ACS::Time& timestamp)
{

    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {

        CORBA::Float x(-40.0);
        switch(index_m){
        case 0:
            x = device->getTotalPowerSideband0Dbm(timestamp);
            break;
        case 1:
            x = device->getTotalPowerSideband1Dbm(timestamp);
            break;
        case 2:
            x = device->getTotalPowerADbm(timestamp);
            break;
        case 3:
            x = device->getTotalPowerBDbm(timestamp);
            break;
        case 4:
            x = device->getTotalPowerCDbm(timestamp);
            break;
        case 5:
            x = device->getTotalPowerDDbm(timestamp);
            break;
        }
        return x;
    } catch (const CAMBErrorExImpl& ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    } catch (const INACTErrorExImpl& ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    }
}


ACS::ROfloat_ptr IFProcImpl::TOTAL_POWER_SIDEBAND_0_DBM()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACS::ROfloat_var prop(ACS::ROfloat::_narrow(total_power_sideband_0_dbm_sp->getCORBAReference()));
    return prop._retn();
}

ACS::ROfloat_ptr IFProcImpl::TOTAL_POWER_SIDEBAND_1_DBM()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACS::ROfloat_var prop(ACS::ROfloat::_narrow(total_power_sideband_1_dbm_sp->getCORBAReference()));
    return prop._retn();
}

ACS::ROfloat_ptr IFProcImpl::TOTAL_POWER_A_DBM()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACS::ROfloat_var prop(ACS::ROfloat::_narrow(total_power_a_dbm_sp->getCORBAReference()));
    return prop._retn();
}

ACS::ROfloat_ptr IFProcImpl::TOTAL_POWER_B_DBM()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACS::ROfloat_var prop(ACS::ROfloat::_narrow(total_power_b_dbm_sp->getCORBAReference()));
    return prop._retn();
}

ACS::ROfloat_ptr IFProcImpl::TOTAL_POWER_C_DBM()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACS::ROfloat_var prop(ACS::ROfloat::_narrow(total_power_c_dbm_sp->getCORBAReference()));
    return prop._retn();
}

ACS::ROfloat_ptr IFProcImpl::TOTAL_POWER_D_DBM()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACS::ROfloat_var prop(ACS::ROfloat::_narrow(total_power_d_dbm_sp->getCORBAReference()));
    return prop._retn();
}

#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(IFProcImpl)


