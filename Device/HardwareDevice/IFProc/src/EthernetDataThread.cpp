
#include "EthernetDataThread.h"
#include "IFProcHWSimImpl.h"
using namespace AMB;

EthernetDataThread::EthernetDataThread(const ACE_CString& _name, 
                                       const IFProcHWSimImpl* _ifprochwsim) :
    ACS::Thread(_name),
    m_ifProcHWSim_p(const_cast<IFProcHWSimImpl*>(_ifprochwsim))
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    // Initially the device is not sending data nor connected.        
    m_status = STAND_BY_STATE;
    cout << "Going to STAND_BY_STATE..." << std::endl;

    m_firstCall = false;
    m_nextExecution = TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value + 2 * TETimeUtil::TE_PERIOD_ACS; 
}


/* Ethernet Data Thread svc reimplementation */
void EthernetDataThread::runLoop() {
        ACS_TRACE(__PRETTY_FUNCTION__);

        switch(m_status){
            case STAND_BY_STATE:
              // The device is not connected to a tcp socket.
              if (m_ifProcHWSim_p->m_try_initialize){ // INIT_TCP_CONN was executed
                  m_ifProcHWSim_p->m_try_initialize = false;
                  if( createSocket()){ //try create the socket
                      m_status = READY_STATE; // The socket was succesfully created, we are ready now.
                      ACS_LOG(LM_SOURCE_INFO,__func__,
			      (LM_INFO, "Going to READY_STATE..."));
                      m_ifProcHWSim_p->m_try_start_data = false; // Clearing this variable if was set on an incorrect moment.

                      // initialize the memory block for the packet
                      m_numBDBs = 255;//m_ifProcHWSim_p->getBdbPerPacket()[0];
                      m_totalBytes = 1+8+m_numBDBs*8;
                      m_packet = new unsigned char[m_totalBytes];
                  }
                  else{
		    //TODO Set a status error flag
                  }
              }
              break;
            case READY_STATE:
              // The device is connected to a TCP socket, but not sending data.
              if(m_ifProcHWSim_p->m_try_start_data){
                  m_ifProcHWSim_p->m_try_start_data = false; // START_DATA was executed
                  m_status = SENDING_STATE;
                  ACS_LOG(LM_SOURCE_INFO,__func__,
			  (LM_INFO, "Going to SENDING_STATE..."));
                  //TODO Set a status flag: sending data = true
                  m_ifProcHWSim_p->m_try_stop_data = false; // Clearing this variable if was set on an incorrect moment.
                  m_firstCall = true;
              }
              break;
            case SENDING_STATE:
              bool sending = true;
              while(sending){
                 // The device is sending data through the ethernet connection.

                 // We just get the actual time at the first call.
                 // After that the time between packets is known
                 if(!m_firstCall){
                     // Add the amount of time requiered for the # of BDBs (time in 100s of nanoseconds) 
                     m_nextExecution += (m_numBDBs * 5000); 
                 }else{
                     m_firstCall = false;
                     // Get the actual time of the system and add a TE
                     m_nextExecution = TETimeUtil::nearestTE(TimeUtil::ace2epoch(ACE_OS::gettimeofday())).value;
                     m_nextExecution += TETimeUtil::TE_PERIOD_ACS;
                 }

                 // sleep for the requiered time to acquire a certain number of BDBs
		 m_now = TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value;
		 long diff = long(m_nextExecution - m_now);
		 if (diff > 0){
		   // Sleep
		   m_timeToSleep.tv_sec = 0; // in nanoseconds
		   m_timeToSleep.tv_nsec = m_numBDBs*500000; // in nanoseconds
		   nanosleep(&m_timeToSleep, &m_remainder);
		 } 
		 sendDataSocket(createPacket(m_nextExecution));

                 if(m_ifProcHWSim_p->m_try_stop_data){
                     m_ifProcHWSim_p->m_try_stop_data = false; // STOP_DATA was executed
                     closeSocket();
                     delete[] m_packet; 
                     //TODO Set a status flag: sending data = false
                     m_status = STAND_BY_STATE;
                     sending = false;
                     cout << "Going back to STAND_BY_STATE..." << std::endl;
                     m_ifProcHWSim_p->m_try_initialize = false; // Clearing this variable if was set on an incorrect moment.
                 }
              }
              break;
        }
    }


// Create the TCP packet that will contain the numBDBs, timestamp and data;
const char* EthernetDataThread::createPacket(unsigned long long t_stamp){

    // -------------------------------------------------
    // | BDBs | Timestamp | BDB_1 | BDB_2 | ... | BDBn |
    // -------------------------------------------------
    //    1B        8B       8B      8B            8B   

    unsigned long long data      = 0x0001000200030004ULL;
    // The timestamp in the packet comes in ns
    t_stamp = t_stamp * 100;

    m_packet[0] = m_numBDBs;

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.
    
    char* p(reinterpret_cast< char* >(&t_stamp));

    char tmp;
    for(int i=0;i<4;i++){
        tmp = p[i];
        p[i] = p[7-i];
        p[7-i] = tmp;
    }
#endif


    std::memcpy(m_packet+1 , &t_stamp, 8);

    for (int i= 0; i< m_numBDBs ; i++){
        std::memcpy(m_packet + i*8 + 9, &data,8);   
    }
    return (const char *)m_packet;
}


// Threads methods for socket connection

void EthernetDataThread::closeSocket(void){
  m_peer.close();
}

//create the socket
bool EthernetDataThread::createSocket(void){
  std::vector<unsigned char> dest(m_ifProcHWSim_p->getMonitorDestIpAddr());
  std::vector<unsigned char> tmp(m_ifProcHWSim_p->getMonitorTcpPort());
  short port = (tmp[0] << 8) | tmp[1];
  unsigned int ip_address = (dest[0] << 24) | (dest[1] << 16) | (dest[2] << 8)| dest[3];

  cout << "Trying to create the socket... " << std::dec <<
    static_cast<int>(dest[0]) << "." 
       << int(dest[1]) << "." 
       << int(dest[2]) << "." 
       << int(dest[3]) << ":" 
       << int(port) << std::endl;

  ACE_INET_Addr server(port, ip_address);

  ACE_Time_Value* timeout;
  timeout = new ACE_Time_Value(2,0);

  if (m_connector.connect(m_peer, server, timeout) == -1){
      cout << "Error trying to create the socket! " << std::endl;
      return false;
  }
  return true;
}

bool EthernetDataThread::sendDataSocket ( const char *s) const {

// TODO Check for the existance of the socket
//  if (m_sock == -1)
//    return false;
  m_peer.send_n(s, m_totalBytes,0,0);
  return true;
}
