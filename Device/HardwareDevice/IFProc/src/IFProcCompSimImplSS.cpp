/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 */

#include "IFProcCompSimImplSS.h"
#include <IFProcHWSimImpl.h>
#include <maciACSComponentDefines.h>
#include <TypeConversion.h>
#include <SimulatedSerialNumber.h>
#include <SharedSimAntNameMapping.h>


/* Please use this class to implement alternative component, extending
 * the IFProcCompSimBase class. */

IFProcCompSimImpl::IFProcCompSimImpl(const ACE_CString& name,
    maci::ContainerServices* pCS):
    IFProcCompSimBase(name, pCS)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    std::string componentName(name.c_str());
    const unsigned long long hashed_sn(AMB::Utils::getSimSerialNumber(
        componentName, "IFProc"));
    std::ostringstream msg;
    msg << "simSerialNumber for "
        << name.c_str()
        << " with assembly name IFProc is "
        << std::hex
        << "0x"
        << hashed_sn
        << "\n";
    LOG_TO_DEVELOPER(LM_DEBUG, msg.str());

    std::vector< CAN::byte_t > sn;
    AMB::node_t node(0x01);

    // Set sn and node according to polarization
    short polarization(componentName[componentName.size() - 1]);
    if(polarization != 0)
    {
        AMB::TypeConversion::valueToData(sn, hashed_sn, 8U);
    }
    else
    {
        node = 0x00;
        AMB::TypeConversion::valueToData(sn, hashed_sn, 8U);
    }

    // Antenna name conversion for shared simulator.
    SharedSimAntNameMapping ss_antID;

    // Simulation object
    try
    {
        device_m = new AMB::IFProcHWSimImpl(
            node, sn, ss_antID.getAntennaID(componentName),
            getContainerServices()->getDefaultComponent<
                SHARED_SIMULATOR::SharedSimulator >(
                    "IDL:alma/SHARED_SIMULATOR/SharedSimulator:1.0"));
    }
    catch(const maciErrType::CannotGetComponentExImpl &_ex)
    {
        device_m = new AMB::IFProcHWSimImpl(node, sn);
    }

    simulationIf_m.setSimObj(device_m);
}

IFProcCompSimImpl::~IFProcCompSimImpl()
{
    if(CORBA::is_nil(device_m->getSharedSimulatorReference()) == false)
    {
        try
        {
            CORBA::String_var ssName(
                device_m->getSharedSimulatorReference()->name());
            getContainerServices()->releaseComponent(ssName.in());
        }
        catch(const maciErrType::CannotReleaseComponentExImpl& ex)
        {
            getLogger()->log(Logging::BaseLog::LM_INFO,
                "Could not release SS component");
        }
    }

    if(device_m != 0)
    {
        delete device_m;
    }
}


MACI_DLL_SUPPORT_FUNCTIONS(IFProcCompSimImpl)
