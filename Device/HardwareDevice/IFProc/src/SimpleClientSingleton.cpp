/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 *
 */

#include <SimpleClientSingleton.h>
#include <acsThreadBase.h>

SimpleClientSingleton* SimpleClientSingleton::_instance = NULL;
maci::SimpleClient* SimpleClientSingleton::m_client = NULL;
unsigned int SimpleClientSingleton::refCounter = 0; 
ACE_Recursive_Thread_Mutex SimpleClientSingleton::mutex;


SimpleClientSingleton* SimpleClientSingleton::instance() {
        perror("SimpleClientSingleton::Before Mutex");
        ACS::ThreadSyncGuard guard(&mutex);
        perror("SimpleClientSingleton::After Mutex");
	if (_instance == NULL) {
                perror("_instance is NULL");
		_instance = new SimpleClientSingleton;
                perror("_instance is should have the new reference.");
	}
        refCounter++;
         ACS_LOG(LM_SOURCE_INFO,"SimpleClientSingleton::instance()",
                (LM_INFO, "increased refCounter: %d",refCounter));
	return _instance;
}

SimpleClientSingleton::SimpleClientSingleton()
    throw (ACSErrTypeCommon::CouldntPerformActionExImpl)  {
    try {
//         ACS_LOG(LM_SOURCE_INFO,"SimpleClientSingleton::SimpleClientSingleton()",
//                (LM_INFO, "Contructor called"));
        perror("SimpleClientSingleton::Before initializeClient");
        initializeClient();
        perror("SimpleClientSingleton::After initializeClient");
    } catch(ACSErrTypeCommon::CouldntPerformActionExImpl &ex) {
        throw ex;
    }
}

SimpleClientSingleton::~SimpleClientSingleton() {
// FIXME: Waiting for the SimpleClient fix.
//	try {
//         ACS_LOG(LM_SOURCE_INFO,"SimpleClientSingleton::~SimpleClientSingleton()",
//                (LM_INFO, "SimpleClientSingleton destructor"));
//	        m_client->logout();
// 	        m_client->doneCORBA();
//                delete m_client;
//                m_client = NULL;
//	    } catch(...) {
//	        printf("Catching an unknown exception!!!!\n");
//	    }
}

maci::SimpleClient* SimpleClientSingleton::getClient() {
	return m_client;
}

void SimpleClientSingleton::initializeClient()
    throw (ACSErrTypeCommon::CouldntPerformActionExImpl) {
    const char* __METHOD__ = "SimpleClientSingleton::initializeClient";
    //ACS_TRACE(__METHOD__);
    perror("SimpleClientSingleton::initializeClient");

    if(m_client != NULL) {
       perror("Already initialized");
       return;
    }

    m_client = new maci::SimpleClient();
    if(m_client == NULL){
        ACS_LOG(LM_SOURCE_INFO,__METHOD__,(LM_ERROR, "Error Creatin SimpleClient"));
        throw ACSErrTypeCommon::CouldntPerformActionExImpl(__FILE__,__LINE__,__METHOD__);
    }

    int argc = 1;
    char* argv[1] = {"SimpleClientSingleton"};

    perror("SimpleClientSingleton:: before client.init");
    if (!m_client->init(argc,argv)) {
        ACS_LOG(LM_SOURCE_INFO,__METHOD__,(LM_ERROR, "Error initializing client"));
        throw ACSErrTypeCommon::CouldntPerformActionExImpl(__FILE__,__LINE__,__METHOD__);
    }
    perror("SimpleClientSingleton:: before client.init");
    perror("SimpleClientSingleton:: before client.login");
    if (!m_client->login()) {
        ACS_LOG(LM_SOURCE_INFO,__METHOD__,
                (LM_ERROR, "Error logging into manager"));
        throw ACSErrTypeCommon::CouldntPerformActionExImpl(__FILE__,__LINE__,__METHOD__);
    }
    perror("SimpleClientSingleton:: After client.login");

    perror("SimpleClientSingleton:: Before ThreadSpawn");
    if(ACE_Thread::spawn((ACE_THR_FUNC)worker,(void*)m_client)==-1) {
      ACE_DEBUG((LM_DEBUG,"Error in spawning thread\n"));
        throw ACSErrTypeCommon::CouldntPerformActionExImpl(__FILE__,__LINE__,__METHOD__);
     }
    perror("SimpleClientSingleton:: After ThreadSpawn");
}

void* worker(void *arg) {
	((maci::SimpleClient*)arg)->run();
        printf("The worker thread has exited,!!\n");
        return NULL;    
}

void SimpleClientSingleton::release() {
    return;
    ACS::ThreadSyncGuard guard(&mutex);
    const char* __METHOD__ = "SimpleClientSingleton::releaseClient";
    //ACS_TRACE(__METHOD__);
    ACS_LOG(LM_SOURCE_INFO,__METHOD__,
                (LM_INFO, "Reduce refCounter"));
    if (refCounter == 0) {
        printf("Releasing when reference counter is cero!!!!");
        return;
    }

    refCounter--;
    if(refCounter == 0) {
         ACS_LOG(LM_SOURCE_INFO,__METHOD__,
                (LM_INFO, "Releasing scs instance"));
        delete _instance;
        _instance = NULL;
    }
}
