/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2007 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 *
 * who       when      what
 * --------  --------  ----------------------------------------------
 * jkern  2007-03-20  created 
 */

/************************************************************************
 *   NAME
 *   
 * 
 *   SYNOPSIS
 *
 *   
 *   PARENT CLASS
 *
 * 
 *   DESCRIPTION
 *
 *
 *   PUBLIC METHODS
 *
 *
 *   PUBLIC DATA MEMBERS
 *
 *
 *   PROTECTED METHODS
 *
 *
 *   PROTECTED DATA MEMBERS
 *
 *
 *   PRIVATE METHODS
 *
 *
 *   PRIVATE DATA MEMBERS
 *
 *
 *   FILES
 *
 *   ENVIRONMENT
 *
 *   COMMANDS
 *
 *   RETURN VALUES
 *
 *   CAUTIONS 
 *
 *   EXAMPLES
 *
 *   SEE ALSO
 *
 *   BUGS   
 * 
 *------------------------------------------------------------------------
 */

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <jni.h>
#include <TotalPowerSender.h>
#include <jniTPBulkDataSenderInterface.h>
#include <SDMDataObjectWriter.h>
#include <iostream>
#include <string>
#include <map>

using namespace asdmbinaries;

typedef std::map<string, TotalPowerSender*> TotalPowerSenderMap;

/* What we need here is a singleton reference to the TotalPowerSender
 * class.  We protect the single instance we will use at this level 
 * and leave it as an ordinary class 
 */
//TotalPowerSender*   senderInstance = NULL; 
//pthread_mutex_t     instanceMutex = PTHREAD_MUTEX_INITIALIZER;
TotalPowerSenderMap senderInstances;

/* This is the name of the archive component we connect to */
const char* TOTAL_POWER_DISTRIBUTOR_NAME = "ARCHIVE_TOTALPOWER_DISTRIBUTOR";

/**
 * A macro to ease the use of enumvec.
 */
#define ENUMVEC(enumName,strlits) asdmbinaries::Utils::enumvec<enumName, C ## enumName>(strlits)




/* Utility Function which will return a string from a jstring */
std::string getStdString(JNIEnv *env, jstring inputString) {
    const char* str;
    str = env->GetStringUTFChars(inputString, NULL);
    if (str == NULL) {
        return NULL; /* OutOfMemoryError already thrown */
    }
    string myString(str);
    env->ReleaseStringUTFChars(inputString, str);
    return myString;
}

//asdmBinary::TPBinaryBlock
void createBinaryBlock(JNIEnv      *env,
        jstring     jdataOID,
        jstring     jexecBlockID,
        jlong       time,
        jlong       interval,
        jint        scanNum,
        jint        subscanNum,
        jint        numIntegration,
        jint        numAntenna,
        jint        numBaseband,
        jint        numPolarization,
        jint        numBins,
        jfloatArray jtpData,
        jintArray   jtpFlags,
        std::ostringstream& tpBlock) 
{

    // Get all of the relevent data from the input data and format for
    //   the SDMDataObjectWriter
    //
    string dataOID = getStdString(env, jdataOID);
    //  string title   = "ALMA Total Power Data";
    string execBlockID = getStdString(env, jexecBlockID);

    int   tpDataLength = env->GetArrayLength(jtpData);
    vector<float> tpData(tpDataLength); // Its better to tell the vector the exact size, avoiding future memory allocations.
    float tpDataT[tpDataLength];
    env->GetFloatArrayRegion(jtpData, 0, tpDataLength, tpDataT);
    for(int i=0; i<tpDataLength; i++){
        tpData[i] = tpDataT[i];
    }



    int tpFlagsLength = env->GetArrayLength(jtpFlags);
    unsigned int tpFlags[tpFlagsLength];

    env->GetIntArrayRegion(jtpFlags, 0, tpFlagsLength, 
    			 reinterpret_cast<jint*>(tpFlags));

    vector<unsigned int> flags(tpFlagsLength);

    bool useFlags = false;
    for (int idx = 0; idx < tpFlagsLength; idx++) {
      flags[idx] = tpFlags[idx];
      if (tpFlags[idx] != 0) {
        useFlags = true;
      }
    }



    // Prepare the description of the binary data.
    vector<SDMDataObject::Baseband> basebands;
    vector<SDMDataObject::SpectralWindow> spectralWindows;

    vector<StokesParameter> sdPolProducts;
    //for (int pol = 0; pol < numPolarization; pol++) {
    // Note we need to pass in a real description here 
    sdPolProducts.push_back(XX);
    sdPolProducts.push_back(YY);
	//}


    //TODO Hardcoded!!!!!
    NetSideband netSideBand = LSB;
    
    // Creating a Spectran window 
    spectralWindows.push_back(SDMDataObject::SpectralWindow(sdPolProducts, 1,
    							  numBins, netSideBand));

    // Wow this base band stuff needs a lot of work but for now...
    if (numBaseband > 0 ) {
        basebands.push_back(SDMDataObject::Baseband(BB_1, spectralWindows));
    }

    if (numBaseband > 1 ) {
        basebands.push_back(SDMDataObject::Baseband(BB_2, spectralWindows));
    }

    if (numBaseband > 2 ) {
        basebands.push_back(SDMDataObject::Baseband(BB_3, spectralWindows));
    }

    if (numBaseband > 3 ) {
        basebands.push_back(SDMDataObject::Baseband(BB_4, spectralWindows));
    }

    vector<AxisName> axis = ENUMVEC(AxisName, "TIM ANT BAB BIN POL");

    //TODO: Change for reasonable values 
    vector<AxisName> flagsAxes = ENUMVEC(AxisName, "TIM ANT BAB BIN POL");

    if (!useFlags) {
      flags.clear();
    }

    vector<AxisName> actualTimesAxes = ENUMVEC(AxisName, "TIM ANT BAB BIN POL");
    vector<long long> actualTimes;
    vector<AxisName> actualDurationsAxes = ENUMVEC(AxisName, "TIM ANT BAB BIN POL");
    vector<long long> actualDurations;

    SDMDataObjectWriter sdmdow(&tpBlock, dataOID, "ALMA Total Power Data");
    try{
        // New version
        sdmdow.tpData((100L * (time - interval/2) - 8712576000000000000LL),      // startTime in Julian Time. Maybe a utility class should be used
                execBlockID,    // ExecBlock UID (?)
                1,              // execBlockNum
                scanNum,        // scanNum
                subscanNum,     // subscanNum
                numIntegration, // number of integrations
                numAntenna,     // number of antennas
                basebands,      // vector of basebands.
                (100L * time - 8712576000000000000LL),           // time = midpoint 
                (interval * 100),       // interval in [ns]
                flagsAxes,
                flags,
                actualTimesAxes,
                actualTimes,
                actualDurationsAxes,
                actualDurations,
                axis,
                tpData);

    }catch(SDMDataObjectWriterException &ex){
        ACSErrTypeCommon::GenericErrorExImpl *ex2 = new ACSErrTypeCommon::GenericErrorExImpl(__FILE__,__LINE__,"createBinaryBlock");
        ex2->setErrorDesc(ex.getMessage().c_str());
        throw ex2;
    }catch(...){
        ACSErrTypeCommon::GenericErrorExImpl *ex2 = new ACSErrTypeCommon::GenericErrorExImpl(__FILE__,__LINE__,"createBinaryBlock");
        ex2->setErrorDesc("sdmdow.tpData() call failed.");
        throw ex2;
    }
    sdmdow.done();

}


/* Header for class alma_Control_TotalPowerDataReceiver_TotalPowerDataSender */
#ifdef __cplusplus
extern "C" {
#endif

    JNIEXPORT jboolean JNICALL 
        Java_alma_Control_TotalPowerProcessor_TPBulkDataSenderInterface_allocateBulkFlow
        (JNIEnv * env, jobject obj, jint flowNumber, jstring distributorName)
        {
            //pthread_mutex_lock(&instanceMutex);
            perror("Before Monitor\n");
            env->MonitorEnter(obj);
            perror("After Monitor\n");
            
            perror("Before getStdString(env, distributorName)\n");
            string dn = getStdString(env, distributorName);
            perror("After getStdString(env, distributorName)\n");
            perror(dn.c_str());
            
            printf("Allocating BulkSender for distributor %s\n", dn.c_str());
            perror("Before Iterator\n");
            //try {
            std::map<string, TotalPowerSender*>::iterator pos;
            //} catch (...) {
            //     env->MonitorExit(obj);
            //     return JNI_FALSE;
            //}
            perror("After Iterator\n");

            perror("Before Finding instance in senderInstances.find(dn)\n");
            pos = senderInstances.find(dn);
            perror("After Finding instance in senderInstances.find(dn)\n");
            if (pos == senderInstances.end()) {
                perror("Inseide If pos == senderInstances.end() is true\n");
                try {
                    TotalPowerSender* si = NULL;
                    perror("Before new TotalPowerSender(dn.c_str()\n");
                    si = new TotalPowerSender(dn.c_str());
                    perror("After new TotalPowerSender(dn.c_str()\n");
                    if (si == NULL) {
                      perror("shit shit shit\n");
                      //shit shit shit.
                      env->MonitorExit(obj);
                      printf("Could not instantiate TotalPowerSender\n");
                      return JNI_FALSE;
                    }
                    perror("Before senderInstances[dn] = si\n");
                    senderInstances[dn] = si;
                    perror("After senderInstances[dn] = si\n");
                } catch (ACSErrTypeCommon::CouldntPerformActionExImpl &ex) {
                   // pthread_mutex_unlock(&instanceMutex);
                      perror("ACSErrTypeCommon::CouldntPerformActionExImpl\n");
                    env->MonitorExit(obj);
                    printf(ex.toString().c_str());
                    printf("\n");
                    return JNI_FALSE;
                } catch(...){
                      perror("... exception\n");
                    printf("Exception during the allocateBulkFlow\n");
                    env->MonitorExit(obj);
                    return JNI_FALSE;
                }
            } else {
               // pthread_mutex_unlock(&instanceMutex);
                env->MonitorExit(obj);
                printf("WARNING: BulkSender already allocated!");
                return JNI_FALSE;
            }

            /* Eventually we should check here to make sure the flow being choosen is
             * not already in use.  For now ignore that issue.
             */

            //pthread_mutex_unlock(&instanceMutex);
            env->MonitorExit(obj);
            perror("Before Return JNI_TRUE\n");
            return JNI_TRUE;
        }

    JNIEXPORT jboolean JNICALL 
        Java_alma_Control_TotalPowerProcessor_TPBulkDataSenderInterface_deallocateBulkFlow
        (JNIEnv * env, jobject obj, jint flowNumber, jstring distributorName){

            //pthread_mutex_lock(&instanceMutex);
            env->MonitorEnter(obj);

           	string dn = getStdString(env, distributorName);
            printf("DeallocatingBulkFlow for distributor %s\n", dn.c_str());

            std::map<string, TotalPowerSender*>::iterator pos;
            pos = senderInstances.find(dn);
            
            if (pos != senderInstances.end()) {
            	delete pos->second;
            	senderInstances.erase(pos->first);
            } else {
               // pthread_mutex_unlock(&instanceMutex);
                env->MonitorExit(obj);
                printf("WARNING: senderInstance for distributor %s has already been erased!!",
                    dn.c_str());
                return JNI_FALSE;
            }

            env->MonitorExit(obj);
            return JNI_TRUE;
        }


    JNIEXPORT jboolean JNICALL 
        Java_alma_Control_TotalPowerProcessor_TPBulkDataSenderInterface_archiveBlob
        (JNIEnv *env,
         jobject dummyObject,
         jint        flowNumber,
         jstring     jdataOID,
         jstring     jexecBlockID,
         jlong       time,
         jlong       interval,
         jint        scanNum,
         jint        subscanNum,
         jint        numIntegration,
         jint        numAntenna,
         jint        numBaseband,
         jint        numPolarization,
         jint        numBins,
         jfloatArray jtpData,
         jintArray   jtpFlags,
         jstring     distributorName) 
        {
        	string dn = getStdString(env, distributorName);
        	
            //asdmBinary::TPBinaryBlock binaryBlock= 
            std::ostringstream binaryBlock;
            try {
                createBinaryBlock(env, jdataOID, jexecBlockID, time, interval, scanNum, 
                        subscanNum, numIntegration, numAntenna, numBaseband, 
                        numPolarization, numBins, jtpData, jtpFlags, 
                        binaryBlock);
            } catch(ACSErrTypeCommon::GenericErrorExImpl &ex) {
                printf(ex.toString().c_str());
                printf("\n");
                return JNI_FALSE;
            } catch(...) {
                ACSErrTypeCommon::GenericErrorExImpl *ex = new ACSErrTypeCommon::GenericErrorExImpl(__FILE__,__LINE__,"Java_alma_Control_TotalPowerProcessor_TPBulkDataSenderInterface_archiveBlob");
                ex->setErrorDesc("call to createBinaryBlock() failed with unspecified exception");
                printf(ex->toString().c_str());
                printf("\n");
                return JNI_FALSE;
            }
            string dataOID = getStdString(env, jdataOID);

            std::string dataString = binaryBlock.str();
            
            std::map<string, TotalPowerSender*>::iterator pos;
            pos = senderInstances.find(dn);
            if (pos != senderInstances.end()) {
                try {
                	TotalPowerSender* tps = pos->second;
                    tps->sendBlob(flowNumber, dataOID.c_str(),
                                  dataString.c_str(), dataString.size());	 
                } catch (ACSErrTypeCommon::CouldntPerformActionExImpl &ex) {
                    printf(ex.toString().c_str());
                    printf("\n");
                    return JNI_FALSE;
                }
            } else {
            	printf("Wrong distributor name. %s hasn't been allocated!",
            	       dn.c_str());
            	return JNI_FALSE;
            }
            return JNI_TRUE;
        }

    JNIEXPORT jboolean JNICALL 
        Java_alma_Control_TotalPowerProcessor_TPBulkDataSenderInterface_writeBlobToDisk
        (JNIEnv *env,
         jclass dummyClass,
         jstring     jbasePath,
         jstring     jdataOID,
         jstring     jexecBlockID,
         jlong       time,
         jlong       interval,
         jint        scanNum,
         jint        subscanNum,
         jint        numIntegration,
         jint        numAntenna,
         jint        numBaseband,
         jint        numPolarization,
         jint        numBins,
         jfloatArray jtpData,
         jintArray   jtpFlags) 
        {
            try{
                std::ostringstream binaryBlock;

                //asdmBinary::TPBinaryBlock tpBlock = 
                createBinaryBlock(env, jdataOID, jexecBlockID, time, interval, scanNum, 
                        subscanNum, numIntegration, numAntenna, numBaseband, 
                        numPolarization, numBins, jtpData, jtpFlags,
                        binaryBlock);

                string basePath    = getStdString(env, jbasePath);
                string dataOID = getStdString(env, jdataOID);
                //ofstream ofs(basePath + "/" + dataOID);

                //ofs << binaryBlock.str();
                //  tpBlock.toFile(basePath);
            }catch(...){return JNI_FALSE;};
            return JNI_TRUE;
        }

#ifdef __cplusplus
}
#endif



