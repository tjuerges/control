#!/usr/bin/env python
from xml.dom.minidom import parse, parseString
try:
   dom1 = parse('IFP_Cal.xml')
except:
   print "Error trying to open the 'IFP_Cal.xml' file\n"
   exit(1)

ifp_configs = dom1.getElementsByTagName("IFP")

xml_header  = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
xml_header += "<ConfigData\n"
xml_header += "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n"
xml_header += "xsi:noNamespaceSchemaLocation=\"membuffer.xsd\">\n"


for config in ifp_configs:
	sn_m = int(config.getAttribute('SN'))
	detectors_m = []
	attenuators_m = []
	detectors = config.getElementsByTagName("DET")
	for det in detectors:
		tmp = str(det.childNodes[0].data).strip().split(' ')
		coeff = [float(tmp[0]),float(tmp[1])]
		detectors_m.append([str(det.getAttribute('ch')),coeff])
	attenuators = config.getElementsByTagName("ATT")
	for att in attenuators:
		tmp = str(att.childNodes[0].data).strip().split(' ')
		real_att = []
		for rval in tmp:
			real_att.append(float(rval))
		attenuators_m.append([str(att.getAttribute('ch')),real_att])
	# create this xml
	filename = "ifp_%d.xml" %sn_m
        print "Creating.. %s" % filename
	fd = open (filename,'w')	
	fd.write(xml_header)
	fd.write("<SN value=\"%03d\"/>\n" % sn_m)
	for i in detectors_m:
		fd.write("<DET ch=\"%s\" slope=\"%f\" icept=\"%f\"/>\n" % (i[0],i[1][0],i[1][1]))
	for i in attenuators_m:
		fd.write("<ATT ch=\"%s\"" % i[0])
		idx = 0
		for atts in i[1]:
			fd.write(" att_%d_%d=\"%f\"" % (idx/2,(idx%2)*5,atts))
			idx+=1
                #PATCH for now
                if i[1].__len__() != 64:
			fd.write(" att_31_5=\"30.0\"")

		fd.write("/>\n")
	fd.write("</ConfigData>\n")
	fd.close()
