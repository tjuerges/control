#ifndef NUTATORImpl_H
#define NUTATORImpl_H
/**
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File NUTATORImpl.h
 *
 * $Id$
 */
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

// Base class
#include <NUTATORBase.h>
//Servants
#include <NUTATORS.h>

class NUTATORImpl: public NUTATORBase,
                virtual public POA_Control::NUTATOR
{
    public:
    /**
     * Constructor
     */
    NUTATORImpl(const ACE_CString& name, maci::ContainerServices* cs);

    /**
     * Destructor
     */
    virtual ~NUTATORImpl();

    
    /**
     * Start a valid nutator program at the specified time.
     * \param starttime - time in absolute alma time.
     * \exception ControlDeviceExceptions::IllegalConfigurationEx If the
     *            program loaded program is invalid.
     * \exception  ControlExceptions::IllegalParameterErrorEx if startime
     *            is invalid (in the past).
     * \exception ControlExceptions::CAMBErrorEx
     * \exception ControlExceptions::INACTErrorEx

     *
     */
    void startProgamAt(ACS::Time starttime);

    /**
     * Stop a valid nutator program at the specified time.
     * If the time is in the past, it stops ASAP.
     * \param stop - time in absolute alma time.
     *
     */
    void stopProgamAt(ACS::Time stoptime);

};
#endif // NUTATORImpl_H
