#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It contains complementary functionality for a NUTATOR device.
"""

import CCL.NUTATORBase
from CCL import StatusHelper
from CCL.logging import getLogger
from log_audience import OPERATOR
import ACSLog
import os

class NUTATOR(CCL.NUTATORBase.NUTATORBase):
    '''
    The NUTATOR class inherits from the code generated NUTATORBase
    class and adds specific methods.
    '''

    def __init__(self, antennaName = None, componentName = None, stickyFlag = False):
        '''
        The constructor creates a NUTATOR object by making use of
        the NUTATORBase constructor.
        '''
        CCL.NUTATORBase.NUTATORBase.__init__(self, antennaName, componentName,
                                           stickyFlag)

    def STATUS(self):
        '''
        This method will display the status information of the device at
        the standard output.
        '''
        os.system("clear")
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[2]

            elements = []

            ################## Status ###########################
            #elements.append( StatusHelper.Separator("Controller") )

            # State
            try:
                value = self._devices[key].GET_STATUS()[0]
            except:
                value = None

            if value is None:
                value = "N/A"
                elements.append( StatusHelper.Line("Operation Status", [StatusHelper.ValueUnit(value)]) )
                elements.append( StatusHelper.Line("Safety Conditions I", [StatusHelper.ValueUnit(value)]) )
                elements.append( StatusHelper.Line("Safety Conditions II", [StatusHelper.ValueUnit(value)]) )
                elements.append( StatusHelper.Line("Controller Errors I", [StatusHelper.ValueUnit("SPARE byte 3")]) )
                elements.append( StatusHelper.Line("Controller Errors II", [StatusHelper.ValueUnit(value)]) )
                elements.append( StatusHelper.Line("Controller Errors III", [StatusHelper.ValueUnit(value)]) )
                elements.append( StatusHelper.Line("Controller Warings I", [StatusHelper.ValueUnit(value)]) )
                elements.append( StatusHelper.Line("Controller Warings II", [StatusHelper.ValueUnit(value)]) )
            else:
                elements = elements + self.__processSTATUS(value)
            
            
            ############################# Program ########################################
            elements.append( StatusHelper.Separator("Program") )
            try:
                value = self._devices[key].GET_PROGRAM_VALIDITY()[0]
                if value == 0x01:
                    value = "program valid."
                elif value == 0x02:
                    value = "validation in progress"
                elif value == 0x00:
                    value = "program INVALID"
                else:
                    value = "UNKNOWN" 
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Standby Program", [StatusHelper.ValueUnit(value)]) )

            try:
                value = self._devices[key].GET_LOAD_STANDBY_PROGRAM()[0]
                if value == 0x01:
                    value = "program valid."
                elif value == 0x02:
                    value = "validation in progress"
                elif value == 0x00:
                    value = "program INVALID"
                else:
                    value = "UNKNOWN" 
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Load Standby Program", [StatusHelper.ValueUnit(value)]) )

            elements.append( StatusHelper.Line("",[StatusHelper.ValueUnit("Mirror pos"),
                                                   StatusHelper.ValueUnit("Trans time"),
                                                   StatusHelper.ValueUnit("Aux pos"),
                                                   StatusHelper.ValueUnit("Dwell time")])) 
            try:
                value = self._devices[key].GET_ACTIVE_PROG_SEG_0()[0]
            except:
                value = ["N/A","N/A","N/A","N/A"]
            elements.append( StatusHelper.Line("Active  Segment 0", 
                                          [StatusHelper.ValueUnit(value[0]),
                                          StatusHelper.ValueUnit(value[1]),
                                          StatusHelper.ValueUnit(value[2]),
                                          StatusHelper.ValueUnit(value[3])]))

            try:
                value = self._devices[key].GET_ACTIVE_PROG_SEG_1()[0]
            except:
                value = ["N/A","N/A","N/A","N/A"]
            elements.append( StatusHelper.Line("Active  Segment 1", 
                                          [StatusHelper.ValueUnit(value[0]),
                                          StatusHelper.ValueUnit(value[1]),
                                          StatusHelper.ValueUnit(value[2]),
                                          StatusHelper.ValueUnit(value[3])]))

            try:
                value = self._devices[key].GET_ACTIVE_PROG_SEG_2()[0]
            except:
                value = ["N/A","N/A","N/A","N/A"]
            elements.append( StatusHelper.Line("Active  Segment 2", 
                                          [StatusHelper.ValueUnit(value[0]),
                                          StatusHelper.ValueUnit(value[1]),
                                          StatusHelper.ValueUnit(value[2]),
                                          StatusHelper.ValueUnit(value[3])]))

            try:
                value = self._devices[key].GET_ACTIVE_PROG_SEG_3()[0]
            except:
                value = ["N/A","N/A","N/A","N/A"]
            elements.append( StatusHelper.Line("Active  Segment 3", 
                                          [StatusHelper.ValueUnit(value[0]),
                                          StatusHelper.ValueUnit(value[1]),
                                          StatusHelper.ValueUnit(value[2]),
                                          StatusHelper.ValueUnit(value[3])]))

            try:
                value = self._devices[key].GET_ACTIVE_PROG_SEG_4()[0]
            except:
                value = ["N/A","N/A","N/A","N/A"]
            elements.append( StatusHelper.Line("Active  Segment 4", 
                                          [StatusHelper.ValueUnit(value[0]),
                                          StatusHelper.ValueUnit(value[1]),
                                          StatusHelper.ValueUnit(value[2]),
                                          StatusHelper.ValueUnit(value[3])]))

            try:
                value = self._devices[key].GET_ACTIVE_PROG_SEG_5()[0]
            except:
                value = ["N/A","N/A","N/A","N/A"]
            elements.append( StatusHelper.Line("Active  Segment 5", 
                                          [StatusHelper.ValueUnit(value[0]),
                                          StatusHelper.ValueUnit(value[1]),
                                          StatusHelper.ValueUnit(value[2]),
                                          StatusHelper.ValueUnit(value[3])]))

            try:
                value = self._devices[key].GET_ACTIVE_PROG_SEG_6()[0]
            except:
                value = ["N/A","N/A","N/A","N/A"]
            elements.append( StatusHelper.Line("Active  Segment 6", 
                                          [StatusHelper.ValueUnit(value[0]),
                                          StatusHelper.ValueUnit(value[1]),
                                          StatusHelper.ValueUnit(value[2]),
                                          StatusHelper.ValueUnit(value[3])]))

            try:
                value = self._devices[key].GET_ACTIVE_PROG_SEG_7()[0]
            except:
                value = ["N/A","N/A","N/A","N/A"]
            elements.append( StatusHelper.Line("Active  Segment 7", 
                                          [StatusHelper.ValueUnit(value[0]),
                                          StatusHelper.ValueUnit(value[1]),
                                          StatusHelper.ValueUnit(value[2]),
                                          StatusHelper.ValueUnit(value[3])]))

            try:
                value = self._devices[key].GET_ACTIVE_PROG_SEG_8()[0]
            except:
                value = ["N/A","N/A","N/A","N/A"]
            elements.append( StatusHelper.Line("Active  Segment 8", 
                                          [StatusHelper.ValueUnit(value[0]),
                                          StatusHelper.ValueUnit(value[1]),
                                          StatusHelper.ValueUnit(value[2]),
                                          StatusHelper.ValueUnit(value[3])]))
            try:
                value = self._devices[key].GET_ACTIVE_PROG_SEG_9()[0]
            except:
                value = ["N/A","N/A","N/A","N/A"]
            elements.append( StatusHelper.Line("Active  Segment 9", 
                                          [StatusHelper.ValueUnit(value[0]),
                                          StatusHelper.ValueUnit(value[1]),
                                          StatusHelper.ValueUnit(value[2]),
                                          StatusHelper.ValueUnit(value[3])]))
            try:
                value = self._devices[key].GET_ACTIVE_PROG_SEG_I()[0]
            except:
                value = ["N/A","N/A","N/A","N/A"]
            elements.append( StatusHelper.Line("Active  Segment I", 
                                          [StatusHelper.ValueUnit(value[0]),
                                          StatusHelper.ValueUnit(value[1]),
                                          StatusHelper.ValueUnit(value[2]),
                                          StatusHelper.ValueUnit(value[3])]))
            
            elements.append( StatusHelper.Line("",[StatusHelper.ValueUnit("Mirror pos"),
                                                   StatusHelper.ValueUnit("Trans time"),
                                                   StatusHelper.ValueUnit("Aux pos"),
                                                   StatusHelper.ValueUnit("Dwell time")])) 
            try:
                value = self._devices[key].GET_STANDBY_PROG_SEG_0()[0]
            except:
                value = ["N/A","N/A","N/A","N/A"]
            elements.append( StatusHelper.Line("Standby  Segment 0", 
                                          [StatusHelper.ValueUnit(value[0]),
                                          StatusHelper.ValueUnit(value[1]),
                                          StatusHelper.ValueUnit(value[2]),
                                          StatusHelper.ValueUnit(value[3])]))

            try:
                value = self._devices[key].GET_STANDBY_PROG_SEG_1()[0]
            except:
                value = ["N/A","N/A","N/A","N/A"]
            elements.append( StatusHelper.Line("Standby  Segment 1", 
                                          [StatusHelper.ValueUnit(value[0]),
                                          StatusHelper.ValueUnit(value[1]),
                                          StatusHelper.ValueUnit(value[2]),
                                          StatusHelper.ValueUnit(value[3])]))

            try:
                value = self._devices[key].GET_STANDBY_PROG_SEG_2()[0]
            except:
                value = ["N/A","N/A","N/A","N/A"]
            elements.append( StatusHelper.Line("Standby  Segment 2", 
                                          [StatusHelper.ValueUnit(value[0]),
                                          StatusHelper.ValueUnit(value[1]),
                                          StatusHelper.ValueUnit(value[2]),
                                          StatusHelper.ValueUnit(value[3])]))

            try:
                value = self._devices[key].GET_STANDBY_PROG_SEG_3()[0]
            except:
                value = ["N/A","N/A","N/A","N/A"]
            elements.append( StatusHelper.Line("Standby  Segment 3", 
                                          [StatusHelper.ValueUnit(value[0]),
                                          StatusHelper.ValueUnit(value[1]),
                                          StatusHelper.ValueUnit(value[2]),
                                          StatusHelper.ValueUnit(value[3])]))

            try:
                value = self._devices[key].GET_STANDBY_PROG_SEG_4()[0]
            except:
                value = ["N/A","N/A","N/A","N/A"]
            elements.append( StatusHelper.Line("Standby  Segment 4", 
                                          [StatusHelper.ValueUnit(value[0]),
                                          StatusHelper.ValueUnit(value[1]),
                                          StatusHelper.ValueUnit(value[2]),
                                          StatusHelper.ValueUnit(value[3])]))

            try:
                value = self._devices[key].GET_STANDBY_PROG_SEG_5()[0]
            except:
                value = ["N/A","N/A","N/A","N/A"]
            elements.append( StatusHelper.Line("Standby  Segment 5", 
                                          [StatusHelper.ValueUnit(value[0]),
                                          StatusHelper.ValueUnit(value[1]),
                                          StatusHelper.ValueUnit(value[2]),
                                          StatusHelper.ValueUnit(value[3])]))

            try:
                value = self._devices[key].GET_STANDBY_PROG_SEG_6()[0]
            except:
                value = ["N/A","N/A","N/A","N/A"]
            elements.append( StatusHelper.Line("Standby  Segment 6", 
                                          [StatusHelper.ValueUnit(value[0]),
                                          StatusHelper.ValueUnit(value[1]),
                                          StatusHelper.ValueUnit(value[2]),
                                          StatusHelper.ValueUnit(value[3])]))

            try:
                value = self._devices[key].GET_STANDBY_PROG_SEG_7()[0]
            except:
                value = ["N/A","N/A","N/A","N/A"]
            elements.append( StatusHelper.Line("Standby  Segment 7", 
                                          [StatusHelper.ValueUnit(value[0]),
                                          StatusHelper.ValueUnit(value[1]),
                                          StatusHelper.ValueUnit(value[2]),
                                          StatusHelper.ValueUnit(value[3])]))

            try:
                value = self._devices[key].GET_STANDBY_PROG_SEG_8()[0]
            except:
                value = ["N/A","N/A","N/A","N/A"]
            elements.append( StatusHelper.Line("Standby  Segment 8", 
                                          [StatusHelper.ValueUnit(value[0]),
                                          StatusHelper.ValueUnit(value[1]),
                                          StatusHelper.ValueUnit(value[2]),
                                          StatusHelper.ValueUnit(value[3])]))
            try:
                value = self._devices[key].GET_STANDBY_PROG_SEG_9()[0]
            except:
                value = ["N/A","N/A","N/A","N/A"]
            elements.append( StatusHelper.Line("Standby  Segment 9", 
                                          [StatusHelper.ValueUnit(value[0]),
                                          StatusHelper.ValueUnit(value[1]),
                                          StatusHelper.ValueUnit(value[2]),
                                          StatusHelper.ValueUnit(value[3])]))
            try:
                value = self._devices[key].GET_STANDBY_PROG_SEG_I()[0]
            except:
                value = ["N/A","N/A","N/A","N/A"]
            elements.append( StatusHelper.Line("Standby  Segment I", 
                                          [StatusHelper.ValueUnit(value[0]),
                                          StatusHelper.ValueUnit(value[1]),
                                          StatusHelper.ValueUnit(value[2]),
                                          StatusHelper.ValueUnit(value[3])]))

            elements.append( StatusHelper.Separator("Temperature") )

            try:
                value = self._devices[key].GET_NUM_TEMPS()[0]
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Active sensors", [StatusHelper.ValueUnit(value)]))
            
            try:
                value = self._devices[key].GET_TEMPERATURE_0()[0]
                valuec = value - 273.15
            except:
                value = "N/A"
                valuec = "N/A"
            elements.append( StatusHelper.Line("Controller", 
                                                      [StatusHelper.ValueUnit(value, "K"),
                                                      StatusHelper.ValueUnit(valuec, "C")]))
            
            try:
                value = self._devices[key].GET_TEMPERATURE_1()[0]
                valuec = value - 273.15
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Mirror Amplifier",
                                                      [StatusHelper.ValueUnit(value, "K"),
                                                      StatusHelper.ValueUnit(valuec, "C")]))

            try:
                value = self._devices[key].GET_TEMPERATURE_2()[0]
                valuec = value - 273.15
            except:
                value = "N/A"
                valuec = "N/A"
            elements.append( StatusHelper.Line("Rocker Amplifier",
                                                      [StatusHelper.ValueUnit(value, "K"),
                                                      StatusHelper.ValueUnit(valuec, "C")]))
            
            try:
                value = self._devices[key].GET_TEMPERATURE_3()[0]
                valuec = value - 273.15
            except:
                value = "N/A"
                valuec = "N/A"
            elements.append( StatusHelper.Line("Apex PCB",
                                                      [StatusHelper.ValueUnit(value, "K"),
                                                      StatusHelper.ValueUnit(valuec, "C")]))
            
            try:
                value = self._devices[key].GET_TEMPERATURE_4()[0]
                valuec = value - 273.15
            except:
                value = "N/A"
                valuec = "N/A"
            elements.append( StatusHelper.Line("Apex mechanical housing",
                                                      [StatusHelper.ValueUnit(value, "K"),
                                                      StatusHelper.ValueUnit(valuec, "C")]))
            
            try:
                value = self._devices[key].GET_TEMPERATURE_5()[0]
                valuec = value - 273.15
            except:
                value = "N/A"
                valuec = "N/A"
            elements.append( StatusHelper.Line("Left mirror motor",
                                                      [StatusHelper.ValueUnit(value, "K"),
                                                      StatusHelper.ValueUnit(valuec, "C")]))

            try:
                value = self._devices[key].GET_TEMPERATURE_6()[0]
                valuec = value - 273.15
            except:
                value = "N/A"
                valuec = "N/A"
            elements.append( StatusHelper.Line("Right mirror motor",
                                                      [StatusHelper.ValueUnit(value, "K"),
                                                      StatusHelper.ValueUnit(valuec, "C")]))

            try:
                value = self._devices[key].GET_TEMPERATURE_7()[0]
                valuec = value - 273.15
            except:
                value = "N/A"
                valuec = "N/A"
            elements.append( StatusHelper.Line("Left rocker motor",
                                                      [StatusHelper.ValueUnit(value, "K"),
                                                      StatusHelper.ValueUnit(valuec, "C")]))

            try:
                value = self._devices[key].GET_TEMPERATURE_8()[0]
                valuec = value - 273.15
            except:
                value = "N/A"
                valuec = "N/A"
            elements.append( StatusHelper.Line("Right rocker motor",
                                                      [StatusHelper.ValueUnit(value, "K"),
                                                      StatusHelper.ValueUnit(valuec, "C")]))

            elements.append( StatusHelper.Separator("Operation") )
            try:
                value = self._devices[key].GET_DEBUG_NOP()[0]
                if value == 0x5a:
                    value = "Ok"
                else:
                    value = "Error (should be 0x5a)"
            except:
                value = "N/A"
            
            elements.append( StatusHelper.Line("Debug NOP", [StatusHelper.ValueUnit(value)]) )
            try:
                value = self._devices[key].GET_MODE_OPERATION()[0]
                if value == 0x0:
                    value = "two position switching"
                elif value == 0x1:
                    value = "multi step switching"
                elif value == 0x2:
                    value = "triangular trajectory"
                elif value == 0x3:
                    value = "sinusoidal trajectory"
                else:
                    value = "UNKOWN Mode"
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Operation Mode", [StatusHelper.ValueUnit(value)]))

            try:
                value = self._devices[key].GET_EXT48MS_SYNC()[0]
                if value == 0:
                    value = "internal"
                else:
                    value = "external (ABM)"
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("48ms TE pulse", [StatusHelper.ValueUnit(value)]))


            try:
                value = self._devices[key].GET_SELFTEST()[0]
                if value == 0x0:
                    value = "no self-test has yet been commanded"
                elif value == 0x1:
                    value = "self-test is in progress"
                elif value == 0x2:
                    value = "self-test has completed"
                else:
                    value = "Unkown value"
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Self Test", [StatusHelper.ValueUnit(value)]))

            try:
                value = self._devices[key].GET_FEEDFORWARD_GAIN()[0]
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Feed forward gain", [StatusHelper.ValueUnit(value)]))

            try:
                value = self._devices[key].GET_KI_START()[0]
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Ki Start (delay)", [StatusHelper.ValueUnit(value)]))
            try:
                value = self._devices[key].GET_LINAMP_STATUS()[0]
                if value & 1 == 0:
                    mirr = "On"
                else:
                    mirr = "Off"

                if value & 2 == 0:
                    rock = "On"
                else:
                    rock = "Off"
                value = [mirr, rock]
            except:
                value = ["N/A","N/A"]
            elements.append( StatusHelper.Line("Linear amplifier", 
                                         [StatusHelper.ValueUnit(value[0], label="Mirror"), 
                                         StatusHelper.ValueUnit(value[1], label="Rocker")]))

            elements.append( StatusHelper.Separator("Servo Loop Control") )
            try:
                value = self._devices[key].GET_CLOSE_LOOP()[0]
                if value == 0:
                    value = "open"
                else:
                    value = "closed"
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Servo Loop", [StatusHelper.ValueUnit(value)]))


            elements.append( StatusHelper.Separator("     Main Loop") )
            try:
                value = self._devices[key].GET_LOOP1_ENGAGE()[0]
                if value == 0:
                    value = "engaged"
                else:
                    value = "disengage"
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Control", [StatusHelper.ValueUnit(value)]))

            try:
                value = self._devices[key].GET_LOOP1_DAO()[0]
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Amplifier Drive Signal", [StatusHelper.ValueUnit(value)]))

            elements.append( StatusHelper.Line("", [StatusHelper.ValueUnit("Numerator"), StatusHelper.ValueUnit("Denominator")]))
            try:
                value = self._devices[key].GET_LOOP1_P()[0]
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Proportional Coeff", 
                                                [StatusHelper.ValueUnit(value[0]), 
                                                 StatusHelper.ValueUnit(value[1])]))
            try:
                value = self._devices[key].GET_LOOP1_I()[0]
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Integral Coeff", 
                                                [StatusHelper.ValueUnit(value[0]), 
                                                 StatusHelper.ValueUnit(value[1])]))
            try:
                value = self._devices[key].GET_LOOP1_D()[0]
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Derivative Coeff", 
                                                [StatusHelper.ValueUnit(value[0]), 
                                                 StatusHelper.ValueUnit(value[1])]))

            elements.append( StatusHelper.Separator("        Auxiliary Loop") )
            try:
                value = self._devices[key].GET_LOOP2_ENGAGE()[0]
                if value == 0:
                    value = "engaged"
                else:
                    value = "disengage"
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Control", [StatusHelper.ValueUnit(value)]))

            try:
                value = self._devices[key].GET_LOOP2_DAO()[0]
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Amplifier Drive Signal", [StatusHelper.ValueUnit(value)]))

            elements.append( StatusHelper.Line("", [StatusHelper.ValueUnit("Numerator"), StatusHelper.ValueUnit("Denominator")]))
            try:
                value = self._devices[key].GET_LOOP2_P()[0]
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Proportional Coeff", 
                                                [StatusHelper.ValueUnit(value[0]), 
                                                 StatusHelper.ValueUnit(value[1])]))
            try:
                value = self._devices[key].GET_LOOP2_I()[0]
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Integral Coeff", 
                                                [StatusHelper.ValueUnit(value[0]), 
                                                 StatusHelper.ValueUnit(value[1])]))
            try:
                value = self._devices[key].GET_LOOP2_D()[0]
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Derivative Coeff", 
                                                [StatusHelper.ValueUnit(value[0]), 
                                                 StatusHelper.ValueUnit(value[1])]))

            elements.append( StatusHelper.Separator("Position") )
            try:
                value = self._devices[key].GET_POSITION()[0]
            except:
                value = ["N/A", "N/A", "N/A", "N/A"]
            elements.append( StatusHelper.Line("Position", 
                                  [StatusHelper.ValueUnit(self.myHex(value[0]), label="Mirror"),
                                  StatusHelper.ValueUnit(self.myHex(value[2]), label="Rocker")]))
            elements.append( StatusHelper.Line("", 
                                  [StatusHelper.ValueUnit(self.myHex(value[1]), label="Mirror sec"),
                                  StatusHelper.ValueUnit(self.myHex(value[3]), label="Rocker sec")]))

            try:
                value_mirr = self._devices[key].GET_MIRROR_POSITION_MIN()[0]
            except:
                value_mirr = "N/A"
            try:
                value_rock = self._devices[key].GET_ROCKER_POSITION_MIN()[0]
            except:
                value_rock = "N/A"

            elements.append( StatusHelper.Line("Minimum Position",
                                              [StatusHelper.ValueUnit(self.myHex(value_mirr), label="Mirror"),
                                               StatusHelper.ValueUnit(self.myHex(value_rock), label="Rocker")]))

            try:
                value_mirr = self._devices[key].GET_MIRROR_POSITION_MAX()[0]
            except:
                value_mirr = "N/A"
            try:
                value_rock = self._devices[key].GET_ROCKER_POSITION_MAX()[0]
            except:
                value_rock = "N/A"

            elements.append( StatusHelper.Line("Maximum Position",
                                              [StatusHelper.ValueUnit(self.myHex(value_mirr), label="Mirror"),
                                               StatusHelper.ValueUnit(self.myHex(value_rock), label="Rocker")]))

            try:
                value = self._devices[key].GET_REMOVE_MOTOR_PS()[0]
                valuelist = ["disabled", "disabled", "disabled", "disabled"]
                if value & 0x1:
                    valuelist[0] = "enabled"
                if value & 0x2:
                    valuelist[1] = "enabled"
                if value & 0x4:
                    valuelist[2] = "enabled"
                if value & 0x8:
                    valuelist[3] = "enabled"
                    
            except:
                valuelist = ["N/A", "N/A", "N/A", "N/A"]
            elements.append( StatusHelper.Line("Relay Isolate amps",
                                              [StatusHelper.ValueUnit(valuelist[0], label="M1"),
                                               StatusHelper.ValueUnit(valuelist[1], label="M2"),
                                               StatusHelper.ValueUnit(valuelist[2], label="R1"),
                                               StatusHelper.ValueUnit(valuelist[3], label="R2")]))


###################### up has bee checked.
            
            try:
                value = self._devices[key].GET_BOARD_ID()[0]
            except:
                value = "N/A" 
            
            title = compName +"   Ant: " + antName + "   BoardID: "+ str(value)

            statusFrame = StatusHelper.Frame(title, elements)
            statusFrame.printScreen()


    def myHex(self, value):
        if value=="N/A":
            return value
        else:
            return hex(value)

    def __processSTATUS(self, statusValue=[]):
        '''
        This is a helper method for the STATUS method. It will parse the display
        of the status bits.
        '''
        opstatus = {0:"idle or aborted",
                  1:"stopped (holding) at position",
                  3:"running a program",
                  2:"not allowed"}
        operrors = {0:"no error",
                    4:"error"}

        safetyconditionsI = {1:"left mirror coupler broken",
                             2:"right mirror coupler broken",
                             4:"rocker manual lock-out pin inserted in lock seat",
                             8:"rocker manual lock-out pin inserted in out seat",
                            16:"mirror manual lock-out pin inserted in lock seat",
                            32:"mirror manual lock-out pin inserted in out seat",
                            64:"main amplifier disabled",
                           128:"safety condition I bits are on"}

        safetyconditionsII = {1:"left mirror mechanical stow on",
                             2:"right mirror mechanical stow on",
                             4:"left mirror mechanical stow off",
                             8:"right mirror mechanical stow off",
                            16:"left rocker mechanical stow on",
                            32:"right rocker mechanical stow on",
                            64:"left rocker mechanical stow off",
                           128:"right rocker mechanical stow off"}

        controllerErrorII = {1:"mirror sensor below lower software limit",
                             2:"mirror sensor above upper software limit",
                             4:"rocker sensor below lower software limit",
                             8:"rocker sensor above upper software limit",
                            16:"safety condition violation",
                            32:"software error",
                            64:"apex power off",
                           128:"amplifier power off (check LINAMP_STATUS)"}

        controllerErrorIII = {1:"PID computation timeout",
                             2:"mirror motor overload",
                             4:"rocker motor overload",
                             8:"positon timeout. Controller-Apex comunication failure",
                            16:"mirror position below lower hardware limit",
                            32:"mirror position above upper hardware limit",
                            64:"rocker position below lower hardware limit",
                           128:"rocker position above upper hardware limit"}

        controllerWarningsI = {1:"memory allocation failure",
                             2:"SPARE bit 1",
                             4:"SPARE bit 2",
                             8:"SPARE bit 3",
                            16:"SPARE bit 4",
                            32:"SPARE bit 5",
                            64:"SPARE bit 6",
                           128:"SPARE bit 7"}
      
        controllerWarningsII = {1:"undefined CAN message recieved",
                             2:"state timeout(obsolete)",
                             4:"ABM timming pulse missing",
                             8:"invalid program",
                            16:"invalid dwell time",
                            32:"SPARE bit 5",
                            64:"too many program segments",
                           128:"transition time more tha 400 ticks"}
        elements = []
        stat =  opstatus[statusValue[0] & 3]# status & 0b011
        err = operrors[statusValue[0] & 4]#status & 0b100
        elements.append( StatusHelper.Line("Operation Status",
                                            [StatusHelper.ValueUnit(stat),
                                            StatusHelper.ValueUnit(err)]) )
    
        ##################### Safety Conditions I ####################
        if statusValue[1] == 0:
            elements.append( StatusHelper.Separator("Safety Conditions I") )
            elements.append( StatusHelper.Line("", 
                                                [StatusHelper.ValueUnit("no erros")]) )
        else:
            elements.append( StatusHelper.Separator("Safety Conditions I") )
            mask = 1
            for i in range(8):
                val = statusValue[1] & mask
                if val>0:
                    item = safetyconditionsI[val]
                    elements.append( StatusHelper.Line("",[StatusHelper.ValueUnit(item)]))
                mask = mask << 1
        
        ##################### Safety Conditions II ####################
        if statusValue[2] == 0:
            elements.append( StatusHelper.Separator("Safety Conditions II") )
            elements.append( StatusHelper.Line("", [StatusHelper.ValueUnit("no erros")]) )
        else:
            elements.append( StatusHelper.Separator("Safety Conditions II") )
            mask = 1
            for i in range(8):
                val = statusValue[2] & mask
                if val>0:
                    item = safetyconditionsII[val]
                    elements.append( StatusHelper.Line("",[StatusHelper.ValueUnit(item)]))
                mask = mask << 1
        

        ##################### Controller errors II ####################
            elements.append( StatusHelper.Separator("Controller Errors I") )
        elements.append( StatusHelper.Line("", [StatusHelper.ValueUnit("SPARE byte 3")]) )

        ##################### Controller errors II ####################
        if statusValue[4] == 0:
            elements.append( StatusHelper.Separator("Controller Errors II") )
            elements.append( StatusHelper.Line("", [StatusHelper.ValueUnit("no erros")]) )
        else:
            elements.append( StatusHelper.Separator("Controller Errors II") )
            mask = 1
            for i in range(8):
                val = statusValue[4] & mask
                if val>0:
                    item = controllerErrorII[val]
                    elements.append( StatusHelper.Line("",[StatusHelper.ValueUnit(item)]))
                mask = mask << 1

        ##################### Controller errors III ####################
        if statusValue[5] == 0:
            elements.append( StatusHelper.Separator("Controller Errors III") )
            elements.append( StatusHelper.Line("", [StatusHelper.ValueUnit("no erros")]) )
        else:
            elements.append( StatusHelper.Separator("Controller Errors III") )
            mask = 1
            for i in range(8):
                val = statusValue[5] & mask
                if val>0:
                    item = controllerErrorIII[val]
                    elements.append( StatusHelper.Line("",[StatusHelper.ValueUnit(item)]))
                mask = mask << 1

        ##################### Controller warnings I ####################
        if statusValue[6] == 0:
            elements.append( StatusHelper.Separator("Controller Warnings I") )
            elements.append( StatusHelper.Line("", [StatusHelper.ValueUnit("no erros")]) )
        else:
            elements.append( StatusHelper.Separator("Controller Warnings I") )
            mask = 1
            for i in range(8):
                val = statusValue[6] & mask
                if val>0:
                    item = controllerWarningsI[val]
                    elements.append( StatusHelper.Line("",[StatusHelper.ValueUnit(item)]))
                mask = mask << 1

        ##################### Controller warnings II ####################
        if statusValue[7] == 0:
            elements.append( StatusHelper.Separator("Controller Warnings II") )
            elements.append( StatusHelper.Line("", [StatusHelper.ValueUnit("no erros")]) )
        else:
            elements.append( StatusHelper.Separator("Controller Warnings II") )
            mask = 1
            for i in range(8):
                val = statusValue[6] & mask
                if val>0:
                    item = controllerWarningsII[val]
                    elements.append( StatusHelper.Line("",[StatusHelper.ValueUnit(item)]))
                mask = mask << 1

        return elements
            
    
    



