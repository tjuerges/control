#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
#
# @(#) $Id$
#------------------------------------------------------------------------------
'''
DESCRIPTION
The FrontEndTest provides a brief functional test of the
component. It is foreseen to be used only for offline
tests and in simulation mode.
'''

# Import unit test
import unittest

# Import system and OS
import sys
import os

# Import the needed ACS modules
import Acspy.Common.QoS
import Acspy.Util.ACSCorba
from Acspy.Clients.SimpleClient import PySimpleClient


# Other imports
from time import sleep

# Exceptions
import ControlDeviceExceptions
import ControlExceptions
import FrontEndExceptions
import exceptions

# For Control and band enums
import Control

from PyDataModelEnumeration import PyReceiverBand
from PyDataModelEnumeration import PyCalibrationDevice

# Front End Test Class
class NutatorTest( unittest.TestCase ):

    def setUp(self):

        Acspy.Common.QoS.setObjectTimeout(Acspy.Util.ACSCorba.getManager(), \
                                          20000)
        self.client = PySimpleClient.getInstance()
        self.componentName = "CONTROL/DV01/NUTATOR"
        self.nut = self.client.getComponent(self.componentName)


    def test01(self):
        '''
        Functional test
        '''

        try:
            self.nut.hwStop()
            self.nut.hwStart()
            self.nut.hwConfigure()
            self.nut.hwInitialize()
            self.nut.hwOperational()


                    
        except Exception, e:
            print "Exception: ", str(e)
            self.fail()

#    def test02(self):
#        '''
#        CCL Status tests
#        '''
#
#        try:
#            import CCL.NUTATOR
#            nut = CCL.NUTATOR.NUTATOR("DV01", stickyFlag=True)
#            nut.hwStop()
#            nut.hwStart()
#            nut.hwConfigure()
#            nut.hwInitialize()
#            nut.hwOperational()
#            nut.STATUS()
#            del(nut)
#
#
#                    
#        except Exception, e:
#            print "Exception: ", str(e)
#            self.fail()
#


# MAIN Program
if __name__ == '__main__':
    suite = unittest.makeSuite(NutatorTest)
    unittest.TextTestRunner(verbosity=2).run(suite)


