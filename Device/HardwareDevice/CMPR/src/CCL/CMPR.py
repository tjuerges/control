#! /usr/bin/env python
#
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2010
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# $Id$
# $Source$


"""
This module is part of the Control Command Language.
It contains complementary functionality for a MPR device.
"""


import CCL.HardwareDevice
import CCL.logging
import CCL.CMPRBase

from CCL import StatusHelper

class CMPR(CCL.CMPRBase.CMPRBase):
    '''
    The CMPR class inherits from the code generated CMPRBase
    class and adds specific methods.
    '''
    def __init__(self, \
            antennaName = None, \
            componentName = None, \
            stickyFlag = False):
        '''
        The constructor creates a CMPR object using CMPRBase constructor.
        EXAMPLE:
        from CCL.CMPR import CMPR
        obj = CMPR("DA41")

        '''
        # Dictionary of device instances
        self._devices = {}

        # Group of antenna or component names.
        if ((isinstance(antennaName,list) == True)
            or (isinstance(componentName,list) == True)):
            #antenna names
            if isinstance(antennaName,list) == True:
                if len(antennaName) != 0:
                    for idx in range (0,len(antennaName)):
                        self._devices[ \
                            "CONTROL/" + antennaName[idx] + "/CMPR"] = ""
            #component names
            if isinstance(componentName,list) == True:
                if len(componentName) != 0:
                    for idx in range (0,len(componentName)):
                        self._devices[componentName[idx]] = ""
        # One component
        else:
            if not((antennaName == None) ^ (componentName == None)):
                raise NameError, "missing antennaName or componentName"

            if antennaName != None:
                self._devices["CONTROL/" + antennaName + "/CMPR"] = ""

            if componentName != None:
                self._devices[componentName] = ""

        # Initialize the base class
        for key, val in self._devices.iteritems():
            CCL.HardwareDevice.HardwareDevice.__init__(self, \
                key, \
                stickyFlag);
            self._devices[key] = self._HardwareDevice__hw

        self.__logger = CCL.logging.getLogger()

    def __del__(self):

        for key, val in self._devices.iteritems():
            instance = self._devices[key]
            del(instance)
        CCL.CMPRBase.CMPRBase.__del__(self)

    def STATUS(self):
        '''
        Method to print relevant status information to the screen
        '''
        
        '''Get component and antenna name'''
        listNames = self._HardwareDevice__componentName.rsplit("/")
        antName   = listNames[1]
        compName  = listNames[2]

        entities = []

        values = []
        try:
            if (self.GET_COMPRESSOR_DRIVE_INDICATION_ON()[0]):
                values.append(StatusHelper.ValueUnit("ON"))
            else:
                values.append(StatusHelper.ValueUnit("OFF"))
        except:
            values.append(StatusHelper.ValueUnit("N/A"))
        entities.append(StatusHelper.Line("Compressor Drive", values))

        values = []
        try:
            aux = self.GET_COMPRESSOR_PRESSURE()[0]*0.25 -1
            values.append(StatusHelper.ValueUnit("%2.2f" % aux, "MPa"))
        except:
            values.append(StatusHelper.ValueUnit("N/A","MPa"))
        entities.append(StatusHelper.Line("Compressor Pressure", values))

        values = []
        try:
            values.append(StatusHelper.ValueUnit(self.GET_COMPRESSOR_PRESSURE_ALARM()[0]))
        except:
            values.append(StatusHelper.ValueUnit("N/A"))
        entities.append(StatusHelper.Line("Compressor Press. Alarm", values))

        values = []
        for method in [('1',self.GET_COMPRESSOR_AUX_1), ('2',self.GET_COMPRESSOR_AUX_2)]:
            try:
                aux = method[1]()[0]*3.2+4
                values.append(StatusHelper.ValueUnit("%2.2f" %
                                                     aux,
                                                     'mA', method[0]))
            except:
                values.append(StatusHelper.ValueUnit("N/A", 'mA', method[0]))
        entities.append(StatusHelper.Line("Compressor Aux.", values))

        values = []
        try:
            values.append(StatusHelper.ValueUnit(self.GET_COMPRESSOR_TEMP_ALARM()[0]))
        except:
            values.append(StatusHelper.ValueUnit("N/A"))
        entities.append(StatusHelper.Line("Compressor Temp Alarm.", values))


        values = []
        for method in [('1',self.GET_COMPRESSOR_TEMP_1), ('2',self.GET_COMPRESSOR_TEMP_2),
                       ('3',self.GET_COMPRESSOR_TEMP_3), ('4',self.GET_COMPRESSOR_TEMP_4)]:
            try:
                values.append(StatusHelper.ValueUnit("%2.2f" %
                                                     method[1]()[0],
                                                     'C', method[0]))
            except:
                values.append(StatusHelper.ValueUnit("N/A", 'C', method[0]))
        entities.append(StatusHelper.Line("Compressor Temp.", values))
                
        frame = StatusHelper.Frame(compName + "   Ant: " + antName, entities)
        frame.printScreen()


