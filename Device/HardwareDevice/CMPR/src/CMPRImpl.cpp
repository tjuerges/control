//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Jun 7, 2010  created
//


#include <CMPRImpl.h>
// For std::string.
#include <string>
// For AUTO_TRACE.
#include <loggingMACROS.h>
// For audience logs.
#include <sstream>
#include <iomanip>
#include <LogToAudience.h>

// Alarm system stuff
#include <vector>
#include <controlAlarmSender.h>

#include <acstime.h>


CMPRImpl::CMPRImpl(const ACE_CString& name, maci::ContainerServices* cs):
    ::CMPRBase(name, cs),
    myName(name.c_str()),
    alarmSender(0)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

CMPRImpl::~CMPRImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}


void CMPRImpl::initialiseAlarmSystem()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // alarmSender is expected to be = 0 since the instanciation of this
    // class.  Therefore do not check if it has been set before but just get a
    // new instance of the AlarmSender.
    alarmSender = new Control::AlarmSender;

    std::vector< Control::AlarmInformation > alarmInformation;

//    {
//        Control::AlarmInformation ai;
//        ai.alarmCode = CmprFoo;
//        alarmInformation.push_back(ai);
//    }

    alarmSender->initializeAlarms(
        Control::CharacteristicControlDeviceImpl::getAlarmFamilyName("CMPR"),
            Control::CharacteristicControlDeviceImpl::getAlarmMemberName(),
        alarmInformation);

    alarmSender->forceTerminateAllAlarms();
}

void CMPRImpl::cleanUpAlarmSystem()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(alarmSender != 0)
    {
        // Disable all alarms for 60s.  This allows to terminate all alarms.
        alarmSender->inhibitAlarms(static_cast< ACS::TimeInterval >(60 * 1e7));
        alarmSender->forceTerminateAllAlarms();
        alarmSender = 0;
    }
}


void CMPRImpl::execute()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        CMPRBase::execute();
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex,__FILE__,__LINE__,
            __PRETTY_FUNCTION__);
    }

    initialiseAlarmSystem();
}

void CMPRImpl::cleanUp()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    cleanUpAlarmSystem();

    try
    {
        CMPRBase::cleanUp();
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex,__FILE__,__LINE__,
            __PRETTY_FUNCTION__);
    }
}


void CMPRImpl::hwInitializeAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(alarmSender != 0)
    {
        alarmSender->forceTerminateAllAlarms();
    }

    CMPRBase::hwInitializeAction();
}


void CMPRImpl::hwStopAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(alarmSender != 0)
    {
        alarmSender->forceTerminateAllAlarms();
    }

    CMPRBase::hwStopAction();
}


#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(CMPRImpl)
