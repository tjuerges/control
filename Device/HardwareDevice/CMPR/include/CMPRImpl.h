#ifndef CMPR_h
#define CMPR_h
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Jun 7, 2010  created
//


#include <CMPRBase.h>
#include <CMPRS.h>

// For std::string.
#include <string>


namespace maci
{
    class ContainerServices;
};

namespace Control
{
    class AlarmSender;
};


class CMPRImpl: public virtual POA_Control::CMPR, public ::CMPRBase
{
    public:
    /// Ctor.
    CMPRImpl(const ACE_CString& name, maci::ContainerServices* cs);

    /// Dtor.
    virtual ~CMPRImpl();

    /// ACS component lifecycle execute method which initialises the alarms.
    virtual void execute();

    /// ACS component lifecycle cleanUp method which cleans up the alarms.
    virtual void cleanUp();

    /** 
     * The current implementation of CMPR has an old style AMBSI-1 which
     * does not respond to node requests at RCA 0.  So we use the 
     * broadcast based getNodeID method.
     * \exception ControlExceptions::CAMBErrorExImpl
     */
    /*
    virtual void getDeviceUniqueId(std::string& deviceID) {
      ACS_TRACE("CMPRImpl::getDeviceUniqueId"); 
      broadcastBasedDeviceUniqueID(deviceID); 
    }; 
    */

    private:
    /// No copy constructor.
    CMPRImpl(const CMPRImpl&);

    /// No assignment operator.
    CMPRImpl& operator=(const CMPRImpl&);

    virtual void hwInitializeAction();

    virtual void hwStopAction();

    /// Initialise the helper system for alarms.
    void initialiseAlarmSystem();

    /// Clean up the helper system for alarms.
    void cleanUpAlarmSystem();

    std::string myName;

    /// Pointer for the alarm system helper instance.
    Control::AlarmSender* alarmSender;

    /// Enums for alarm conditions.
    enum CmprAlarmConditions
    {
    };
};
#endif
