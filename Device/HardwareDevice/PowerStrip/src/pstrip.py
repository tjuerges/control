#!/usr/bin/env python
# @(#) $Id$
#
# Copyright (C) 2003
# Associated Universities, Inc. Washington DC, USA.
#
# Produced for the ALMA project
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU Library General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) any
# later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
# details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
# ALMA should be addressed as follows:
#
# Internet email: alma-sw-admin@nrao.edu
'''
Power Strip Control

pstrip artmps off 8
pstrip artmps on 8
pstrip artmps cycle 8
'''
import telnetlib
import sys
import time
import os
from traceback import print_exc
import socket

class LoginFailed(Exception):
    pass

def answer(tn, value, expect = None):
    tn.write(value+'\r')
    if expect is None:
        tn.expect(['.*>[ ]+$'])
    else:
        tn.expect([expect], 5)

def login(ps, user, pw):
    '''
    login, and return switch name and telnet object

    Raises: LoginFailed if failed to login
    '''
    #
    # Some switches do not respond quickly after a previous pstrip command.
    # Try 3 times to get connected.
    #
    count = 3
    while count > 0:
        tn = None
        try:
            tn = telnetlib.Telnet(ps, timeout=3)
        except socket.error, exp:
            print "Error while connecting to %s" % (ps)
            print "Is %s there? You should probably update /alma/ste/etc/site.xml." % (ps)
            raise LoginFailed()
            #tn.set_debuglevel(5)
        try:
            tn.expect(['.*User Name :.*'], 5)
            break
        except EOFError:
            count = count-1
            time.sleep(1)
    if not count:
        raise LoginFailed()

    tn.write(user+'\r')
    tn.expect(['.*Password  :.*'], 5)
    tn.write(pw+'\r')
    try:
        (index, match, stuff) = tn.expect(['\n(.*):.*Communication Established'], 5)
        version = "unknown"
        if stuff.find("v1") != -1:
            version = "v1"
        if stuff.find("v2") != -1:
            version = "v2"
        if stuff.find("v3") != -1:
            version = "v3"
        switch = match.groups()[0].strip()
        tn.expect(['.*>[ ]+$'])
    except:
        raise LoginFailed()
    return switch, tn, version

def pstrip(ps, user, pw, action, offset):
    # It seems that some power strips do not respond quickly enough if
    # the pstrip command is issued two times in a row.  Try at least a few 
    # times in a row.
    switch, tn, version = login(ps, user, pw)
    action = {'on':'1', 'off':'2', 'cycle':'3'}[action.lower()]
    answer(tn, '1')
    if switch == 'Switched Rack PDU':
        if version == "v1" or version == "v2":
            answer(tn, '3')
        if version == "v3":
            answer(tn, '2')
            answer(tn, '1')
    answer(tn, str(offset))
    answer(tn, '1')
    answer(tn, action, ".*Enter 'YES' to continue or <ENTER> to cancel :.*")
    answer(tn, 'YES', '.*Press <ENTER> to continue.*')
    answer(tn, '')
    answer(tn, '\x1b')
    answer(tn, '\x1b')
    answer(tn, '\x1b')
    if switch == 'Switched Rack PDU':
        if version == "v1" or version == "v2":
            answer(tn, '\x1b')
        if version == "v3":
            answer(tn, '\x1b')
            answer(tn, '\x1b')

    tn.write('4\r')

def pstrip_list_ports(ps, user, pw):
    switch, tn, version = login(ps, user, pw)
    
    tn.write('1\r')
    if switch == 'Switched Rack PDU':
        tn.expect(['1'], 5)
        if version == "v3":
            tn.write('2\r')
            time.sleep(0.5)
            tn.write('1\r')
        if version == "v1" or version == "v2":
            tn.write('3\r')
    time.sleep(0.5)
    msg = tn.read_very_eager()
    answer(tn, '\x1b')
    if switch == 'Switched Rack PDU':
        if version == "v1" or version == "v2":
            answer(tn, '\x1b')
        if version == "v3":
            answer(tn, '\x1b')
            answer(tn, '\x1b')

    tn.write('4\r')

    pieces = msg.split('\n')
    while 1:
        p = pieces[0]
        if p.find('ON') > -1 or p.find('OFF') > -1: break
        del pieces[0]

    for p in pieces[0:8]:
        print p

    print

from optparse import OptionParser
import zlib

def main():
    usage = '''%prog [options] host action offset

host - power strip name
action - off, on, cycle, list
offset - port offset, integer numbers 1-8 selects a single port.
         9 selects all ports.
         If action is "list", offset is ignored.

Example: 

pstrip -u oper -p xxxx artmps cycle 1
pstrip artmps list
'''

    parser = OptionParser(usage = usage)
    parser.add_option("-u",  None, dest = "user", default = None,
                metavar = " USER",
                help = "user login, default is current user: %s" % (os.getenv('USER')))
    pw = zlib.decompress('x\x9cK,I3\xc9MU+\x05\x00\rH\x02\xdd')
    parser.add_option("-p", None, dest = "pw", 
                default = pw,
                metavar = " PASSPHRASE",
                help = "passphrase, default: %s" % (pw))
    (options, args) = parser.parse_args()
    if options.user is None:
        options.user = os.getenv('USER')
    if not ((len(args) == 2 and args[1] == 'list') or (len(args) == 3)):
        print
        parser.print_help()
        print
        sys.exit(1)

    host = args[0]
    action = args[1]
    try:
        if action == 'list':
            pstrip_list_ports(host, options.user, options.pw)
        else:
            offset = args[2]
            pstrip(host, options.user, options.pw, action, offset)
    except LoginFailed:
        print
        print 'Failed to login to %s as user %s' % \
            (host, options.user)
        print
        sys.exit(1)
    except:
        print 'Unexpected exception!'
        print_exc()
        sys.exit(1)

    sys.exit(0)

if __name__ == "__main__":
    main()
