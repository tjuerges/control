#!/usr/bin/env python
# @(#) $Id$
#
# Copyright (C) 2003
# Associated Universities, Inc. Washington DC, USA.
#
# Produced for the ALMA project
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU Library General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) any
# later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
# details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
# ALMA should be addressed as follows:
#
# Internet email: alma-sw-admin@nrao.edu

import os
import sys
from distutils.core import setup, Extension
from ConfigParser import ConfigParser

scripts=[
    'pstrip'
]

ini_base = None # configurable option
#
# read setup file for configuration options, currently just install-data
#
try:
    parser = ConfigParser()
    parser.read('setup.cfg')
    ini_base = parser.get('install','install-data')
except Exception, e:
    print 'Exception is ', e
    pass
#
# Scripts are more convenient without the .py extension.
# So, the scripts are listed without the .py, and we need
# to go through scripts and make/delete a symbolic link
# to a script name with .py if we install/clean
#
# Problem if *.ini files are write protected, so make them writable
#
if len(sys.argv) > 1 and sys.argv[1] == 'install':
    for n in scripts:
        if not os.path.exists(n):
            try:
                os.symlink(os.path.basename(n+'.py'), n)
            except:
                print 'Failed to create symlink for ', n

#
# install done
#

if len(sys.argv) > 1 and sys.argv[1] == 'clean':
    for n in scripts:
        if os.path.exists(n):
            os.unlink(n)

#
# pre-clean done
#

setup(name="PowerStrip",
      version="1.0",
      description="ALMA Control System",
      maintainer="Ralph Marson",
      maintainer_email="rmarson@nrao.edu",
      url="http://www.alma.nrao.edu",

#      packages=[
#        'PowerStrip', 
#      ],

      scripts=scripts,
    )
