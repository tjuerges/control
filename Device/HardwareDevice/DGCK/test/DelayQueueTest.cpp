/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2006-10-05  created
*/

/************************************************************************
*   NAME
*   
* 
*   SYNOPSIS
*   
* 
*   DESCRIPTION
*
*   FILES
*
*   ENVIRONMENT
*
*   COMMANDS
*
*   RETURN VALUES
*
*   CAUTIONS 
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS   
* 
*------------------------------------------------------------------------
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <iomanip>

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <DelayQueue.h>


class DelayQueueTest : public DelayQueue {
public:
  DelayQueueTest() : DelayQueue() {}
  ~DelayQueueTest() {};

  long size() { return commandQueue_m.size();}
  void clear() { commandQueue_m.clear();}

  void print() {
    std::deque<DelayQueue::CmdStruct>::iterator iter;
    for (iter = commandQueue_m.begin(); iter != commandQueue_m.end();
	 iter++) {
      fprintf(stdout,"%lld: 0x",iter-> commandTime);
      for (int idx =7; idx >= 0; idx--)
	fprintf(stdout,"%02x ",iter->commandData.data1[idx]);
      fprintf(stdout,"   0x");
      for (int idx =7; idx >= 0; idx--)
	fprintf(stdout,"%02x ",iter->commandData.data2[idx]);
      fprintf(stdout,"\n");
    }
  }
};


bool test01(DelayQueueTest& testQueue){
  /* Test 1 tests the correct ingestion of a trivial Event */
  std::cout << "Beginning Test 1" << std::endl;
  Control::AntennaDelayEvent newEvent;
  newEvent.startTime = 15 * 480000;
  newEvent.stopTime = 35 * 480000;
  newEvent.antennaName = "TestAntenna";
  newEvent.coarseDelays.length(0);
  newEvent.fineDelays.length(0);
  newEvent.delayTables.length(0);

  testQueue.delayEvent(newEvent);
  if (testQueue.size() != 20) {
    std::cout << "Incorrect number of events in queue \n\t"  
	      << testQueue.size()  << " reported, should be 20" << std::endl;
    testQueue.clear();
    return false;
  }

  /* Clean up and Return */
  testQueue.clear();
  return true;
}

bool test02(DelayQueueTest& testQueue){
  /* Test 2 tests the correct retreival of a trivial Event */
  std::cout << "Beginning Test 2" << std::endl;
  Control::AntennaDelayEvent newEvent;
  DelayQueue::DelayCommand delayCmd;


  newEvent.startTime = 15 * 480000; // Note this is application time
  newEvent.stopTime = 35 * 480000;
  newEvent.antennaName = "TestAntenna";
  newEvent.coarseDelays.length(0);
  newEvent.fineDelays.length(0);
  newEvent.delayTables.length(0);

  testQueue.delayEvent(newEvent);

  /* At this point there are 20 events in the queue
     starting at time 7200000 and going to 16320000
  */

  /* Test that a get too early returns an exception */
  try {
    delayCmd = testQueue.getCommand(static_cast<ACS::Time>(480000));
    std::cerr << "Illegal return for value before start of range" << std::endl;
    testQueue.clear();
    return false;
  } catch (FTSExceptions::NoValidDelayExImpl& ex) {
    /* This is ok, we expected an exception */
  }
  
  /* Test that reading the first 10 actually removes the entry */
  try {
    for (int idx = 0; idx < 10; idx++) {
      delayCmd = testQueue.getCommand(static_cast<ACS::Time>
                                      ((14+idx) * 480000));
      if (testQueue.size() != 20 - idx - 1 ) {
	std::cout << "Size of queue is not decreasing as expected" 
		  << std::endl;
	testQueue.clear();
	return false;
      }
    }
  } catch (FTSExceptions::NoValidDelayExImpl& ex) {
    std::cout << "Failed to retreive command that should have been present" 
	      << std::endl;
    testQueue.clear();
    return false;
  }

  /* Test that reading for a time that does not exist returns an exception */
  try {
    delayCmd = testQueue.getCommand(static_cast<ACS::Time>(26.5*480000));
    std::cerr << "Illegal return for bad time value." << std::endl;
    testQueue.clear();
    return false;
  } catch (FTSExceptions::NoValidDelayExImpl& ex) {
    /* This is ok, we expected an exception */
  }

  /* Check that scanning to a point removes all previous points */
  try {
    delayCmd = testQueue.getCommand(static_cast<ACS::Time>(32*480000));
  } catch (FTSExceptions::NoValidDelayExImpl& ex) {
    std::cout << "Failed to retreive command that should have been present" 
	      << std::endl;
   testQueue.clear();
   return false;
  }

  if (testQueue.size() != 1 ) {
    std::cout << "Size of queue is not one as expected" << std::endl;
    testQueue.clear();
    return false;
  }

  /* Finally check that getting a time after all the command has expired
     clears the queue and returns an exception */
  try {
    delayCmd = testQueue.getCommand(static_cast<ACS::Time>(40*480000));
    std::cout << "Illegal return for time value later than all commands" 
	      << std::endl;
    testQueue.clear();
    return false;
  } catch (FTSExceptions::NoValidDelayExImpl& ex) {
    /* This is ok, we expected an exception */
  }


  if (testQueue.size() != 0 ) {
    std::cout << "Queue is not empty as expected" << std::endl;
    testQueue.clear();
    return false;
  }

  /* Clean up and Return */
  testQueue.clear();
  return true;
}

bool test03(DelayQueueTest& testQueue){
  /* Test 3 tests the correct ingestion of multiple contigous events */
  std::cout << "Beginning Test 3" << std::endl;
  Control::AntennaDelayEvent newEvent;
  newEvent.startTime = 15 * 480000;
  newEvent.stopTime = 35 * 480000;
  newEvent.antennaName = "TestAntenna";
  newEvent.coarseDelays.length(0);
  newEvent.fineDelays.length(0);
  newEvent.delayTables.length(0);

  testQueue.delayEvent(newEvent);

  newEvent.startTime = 35 * 480000;
  newEvent.stopTime = 55 * 480000;

  testQueue.delayEvent(newEvent);


  if (testQueue.size() != 40) {
    std::cout << "Incorrect number of events in queue \n\t"  
	      << testQueue.size()  << " reported, should be 40" << std::endl;
    testQueue.clear();
    return false;
  }

  /* Clean up and Return */
  testQueue.clear();
  return true;
}

bool test04(DelayQueueTest& testQueue){
  /* Test 4 tests the correct ingestion of multiple events with gaps */
  std::cout << "Beginning Test 4" << std::endl;
  Control::AntennaDelayEvent newEvent;
  DelayQueue::DelayCommand delayCmd;

  newEvent.startTime = 15 * 480000;
  newEvent.stopTime = 35 * 480000;
  newEvent.antennaName = "TestAntenna";
  newEvent.coarseDelays.length(0);
  newEvent.fineDelays.length(0);
  newEvent.delayTables.length(0);

  testQueue.delayEvent(newEvent);

  newEvent.startTime = 55 * 480000;
  newEvent.stopTime = 75 * 480000;

  testQueue.delayEvent(newEvent);

  if (testQueue.size() != 40) {
    std::cout << "Incorrect number of events in queue \n\t"  
	      << testQueue.size()  << " reported, should be 40" << std::endl;
    testQueue.clear();
    return false;
  }

  for (int idx = 14; idx < 74; idx++) {
    try {
      delayCmd = testQueue.getCommand(static_cast<ACS::Time>(idx*480000));
      if (idx > 33 && idx < 54) {
	/* Error should have thrown an exception */
	std::cout << "Illegal value returned for time out of valid range" 
		  << std::endl;
	testQueue.clear();
	return false;
      }
    } catch (FTSExceptions::NoValidDelayExImpl& ex) {
      if (idx < 34 || idx > 53) {
	std::cout << "Failed to return a value which should have been present"
		  << std::endl;
	testQueue.clear();
	return false;
      }
    }
  }

  if (testQueue.size() != 0) {
    std::cout << "Unexpected commands still in queue"  << std::endl;
    testQueue.clear();
    return false;
  }

  /* Clean up and Return */
  testQueue.clear();
  return true;
}

bool test05(DelayQueueTest& testQueue){
   /* Test 5 tests the correct packing of data into the command structure */
   std::cout << "Beginning Test 5" << std::endl;
   Control::AntennaDelayEvent newEvent;
  DelayQueue::DelayCommand delayCmd;

   unsigned long long* bitBuffer1 =  
     reinterpret_cast<unsigned long long*>(delayCmd.data1);

   newEvent.startTime = 15 * 480000;
   newEvent.stopTime = 45 * 480000;
   newEvent.antennaName = "TestAntenna";
   newEvent.coarseDelays.length(0);
   newEvent.delayTables.length(0);

   unsigned char delay  = 0;
   short time = 3;
   while (time < 20*48) {
     int idx = newEvent.fineDelays.length();
     newEvent.fineDelays.length(idx+1);

    short period=0;
    delay += 1;
    delay %= 16; // For now until we get to 16th sample steps
    newEvent.fineDelays[idx].relativeStartTime = time;
    newEvent.fineDelays[idx].fineDelay = delay;

    if (time < 4*48) {         period = 10;
    } else if (time < 8*48) {  period = 20;
    } else if (time < 12*48) { period = 25;
    } else if (time < 16*48) { period = 30;
    } else { period = 50;
    }
    time += period;
  }
  testQueue.delayEvent(newEvent);

  int delayIdx = 0;
  try {
    for (int idx = 14; idx < 44; idx++) {
      /** Note this returns the value at the application time, the
	  actual value is 48 ms later */
      delayCmd = testQueue.getCommand(static_cast<ACS::Time>(idx*480000));
          
      #if BYTE_ORDER != BIG_ENDIAN
      {
        DelayQueue::swapByteOrder(delayCmd);
      }
      #endif

      for (int block = 0; block < delayCmd.data1[8]; block++) {
	ACS::Time appTime_ms = (*bitBuffer1 >> (50 - (10*block)) & 0x3F) +
	  static_cast<ACS::Time>(idx*48); 	
	ACS::Time effTime_ms = newEvent.fineDelays[delayIdx].relativeStartTime+
	  (newEvent.startTime / 10000);
	unsigned int delay  = *bitBuffer1 >> (46 - (10*block)) & 0x0F;
	if (effTime_ms - appTime_ms != 48 ||
	    delay != newEvent.fineDelays[delayIdx].fineDelay) {
          std::cout << "Time or Delay mismatch" << std::endl;
          
          testQueue.clear();
          return false;
        }
	delayIdx++;
      }
    } 
  } catch (FTSExceptions::NoValidDelayExImpl& ex) {
    std::cout << "Failed to return a value which should have been present"
	      << std::endl;
    testQueue.clear();
    return false;
  }

  /* Clean up and Return */
  testQueue.clear();
  return true;
}

bool test06(DelayQueueTest& testQueue){
  /* Test 6 tests the clearing before a given time */
  std::cout << "Beginning Test 6" << std::endl;
  Control::AntennaDelayEvent newEvent;
  DelayQueue::DelayCommand delayCmd;
  //  AmbDataMem_t commandData[8];

  newEvent.startTime = 15 * 480000; // Note this is application time
  newEvent.stopTime = 35 * 480000;
  newEvent.antennaName = "TestAntenna";
  newEvent.coarseDelays.length(0);
  newEvent.fineDelays.length(0);
  newEvent.delayTables.length(0);

  testQueue.delayEvent(newEvent);

  /* At this point there are 20 events in the queue
     starting at time 7200000 and going to 16320000
  */
  if (testQueue.size() != 20) {
    std::cout << "Incorrect number of events in queue \n\t"  
	      << testQueue.size()  << " reported, should be 20" << std::endl;
    testQueue.clear();
    return false;
  }

  testQueue.clearBefore(static_cast<ACS::Time>(25*480000));

  if (testQueue.size() != 10) {
    std::cout << "Incorrect number of events in queue \n\t"  
	      << testQueue.size()  << " reported, should be 10" << std::endl;
    testQueue.clear();
    return false;
  }

  for (int idx = 14; idx < 34; idx++) {
    try {
      delayCmd = testQueue.getCommand(static_cast<ACS::Time>(idx*480000));
      if (idx < 24) {
	/* Error should have thrown an exception */
	std::cout << "Value returned which should have been cleared" 
		  << std::endl;
	std::cout << "index: " << idx << std::endl;
	testQueue.clear();
	return false;
      }
    } catch (FTSExceptions::NoValidDelayExImpl& ex) {
      if (idx > 23) {
	std::cout << "Cleared value which should have been saved"
		  << std::endl;
	testQueue.clear();
	return false;
      }
    }
  }

  /* Clean up and Return */
  testQueue.clear();
  return true;
}

bool test07(DelayQueueTest& testQueue){
  /* Test 7 test the correct clearing after a given time */
  std::cout << "Beginning Test 7" << std::endl;
  Control::AntennaDelayEvent newEvent;
  //AmbDataMem_t commandData[8];
  DelayQueue::DelayCommand delayCmd;

  newEvent.startTime = 15 * 480000; // Note this is application time
  newEvent.stopTime = 35 * 480000;
  newEvent.antennaName = "TestAntenna";
  newEvent.coarseDelays.length(0);
  newEvent.fineDelays.length(0);
  newEvent.delayTables.length(0);

  testQueue.delayEvent(newEvent);

  /* At this point there are 20 events in the queue
     starting at time 7200000 and going to 16320000
  */
  if (testQueue.size() != 20) {
    std::cout << "Incorrect number of events in queue \n\t"  
	      << testQueue.size()  << " reported, should be 20" << std::endl;
    testQueue.clear();
    return false;
  }

  testQueue.clearAfter(static_cast<ACS::Time>(25*480000));

  if (testQueue.size() != 10) {
    std::cout << "Incorrect number of events in queue \n\t"  
	      << testQueue.size()  << " reported, should be 10" << std::endl;
    testQueue.clear();
    return false;
  }

  for (int idx = 14; idx < 34; idx++) {
    try {
      delayCmd = testQueue.getCommand(static_cast<ACS::Time>(idx*480000));
      if (idx > 23) {
	/* Error should have thrown an exception */
	std::cout << "Value returned which should have been cleared" 
		  << std::endl;
	std::cout << "index: " << idx << std::endl;
	testQueue.clear();
	return false;
      }
    } catch (FTSExceptions::NoValidDelayExImpl& ex) {
      if (idx < 24) {
	std::cout << "Cleared value which should have been saved"
		  << std::endl;
	testQueue.clear();
	return false;
      }
    }
  }

  /* Clean up and Return */
  testQueue.clear();
  return true;
}

bool test08(DelayQueueTest& testQueue){
  /* Test 8 tests the correct ingestion of overlapping events */

  std::cout << "Beginning Test 8" << std::endl;
  DelayQueue::DelayCommand delayCmd;

  /* First Event is from 15 to 35 and has one delay in each time */
  {
    Control::AntennaDelayEvent newEvent;
    newEvent.startTime = 15 * 480000; // Note this is application time
    newEvent.stopTime = 35 * 480000;
    newEvent.antennaName = "TestAntenna";
    newEvent.coarseDelays.length(0);
    newEvent.fineDelays.length(0);
    newEvent.delayTables.length(0);
    
    int time = 0;
    while (time < 20*48) {
      int idx = newEvent.fineDelays.length();
      newEvent.fineDelays.length(idx+1);
      newEvent.fineDelays[idx].relativeStartTime = time + 1;
      newEvent.fineDelays[idx].fineDelay = 0;
      time += 48;
    }
    testQueue.delayEvent(newEvent);
  }


  /* Second Event is from 30 to 55 and has two delays in each time */
  {
    Control::AntennaDelayEvent newEvent;
    newEvent.startTime = 30 * 480000; // Note this is application time
    newEvent.stopTime = 55 * 480000;
    newEvent.antennaName = "TestAntenna";
    newEvent.coarseDelays.length(0);
    newEvent.fineDelays.length(0);
    newEvent.delayTables.length(0);
     
    int time = 0;
    while (time < 25*48) {
      int idx = newEvent.fineDelays.length();
      newEvent.fineDelays.length(idx+1);
      newEvent.fineDelays[idx].relativeStartTime = time + 1;
      newEvent.fineDelays[idx].fineDelay = 0;
      time += 24;
    }
    testQueue.delayEvent(newEvent);
  }

  /* third Event is from 20 to 40 and has three delays in each time */
  {
    Control::AntennaDelayEvent newEvent;
    newEvent.startTime = 20 * 480000; // Note this is application time
    newEvent.stopTime = 40 * 480000;
    newEvent.antennaName = "TestAntenna";
    newEvent.coarseDelays.length(0);
    newEvent.fineDelays.length(0);
    newEvent.delayTables.length(0);
     
    int time = 0;
    while (time < 30*48) {
      int idx = newEvent.fineDelays.length();
      newEvent.fineDelays.length(idx+1);
      newEvent.fineDelays[idx].relativeStartTime = time + 1;
      newEvent.fineDelays[idx].fineDelay = 0;
      time += 16;
    }
    testQueue.delayEvent(newEvent);
  }


  if (testQueue.size() != 40) {
    std::cout << "Incorrect number of events in queue \n\t"  
	      << testQueue.size()  << " reported, should be 40" << std::endl;
    testQueue.clear();
    return false;
  }



  try {
    for (int idx = 14; idx < 54; idx++) {
      /** Note this retunrs the value at the application time, the
	  actual value is 48 ms later */
      delayCmd = testQueue.getCommand(static_cast<ACS::Time>(idx*480000));

      #if BYTE_ORDER != BIG_ENDIAN
      {
        DelayQueue::swapByteOrder(delayCmd);
      }
      #endif
      if (idx < 19 && delayCmd.data1[7] != 1) {
	std::cerr << "Incorrect number of commands at time "
		  << static_cast<ACS::Time>(idx*480000) 
		  << " recieved " 
		  << static_cast<unsigned int>(delayCmd.data1[7])
		  << " instead of 1."<< std::endl;
	testQueue.clear();
	return false;
      } else if (idx >= 39 &&   delayCmd.data1[7] != 2) {
	std::cerr << "Incorrect number of commands at time "
		  << static_cast<ACS::Time>(idx*480000) 
		  << " recieved " 
		  << static_cast<unsigned int>(delayCmd.data1[7]) 
		  << " instead of 2."<< std::endl;
	testQueue.clear();
	return false;
      } else if (idx >= 19 && idx < 39 && delayCmd.data1[7] != 3) {
	std::cerr << "Incorrect number of commands at time "
		  << static_cast<ACS::Time>(idx*480000) 
		  << " recieved "
		  << static_cast<unsigned int>(delayCmd.data1[7]) 
		  << " instead of 3." << std::endl;
	testQueue.clear();
	return false;
      }
    } 
  } catch (FTSExceptions::NoValidDelayExImpl& ex) {
    std::cout << "Failed to return a value which should have been present"
	      << std::endl;
    testQueue.clear();
    return false;

  }

  /* Clean up and Return */
  testQueue.clear();
  return true;
}

bool test09() {
  /* Test 9 tests the correct behavior of the packDelayCommand method */
  std::cout << "Beginning Test 9" << std::endl;

  DelayQueue::DelayCommand delayCmd;
  memset(delayCmd.data1,0,8);
  memset(delayCmd.data2,0,8);

  unsigned long long* cmd1 = 
    reinterpret_cast<unsigned long long*>(delayCmd.data1);
  unsigned long long* cmd2 = 
    reinterpret_cast<unsigned long long*>(delayCmd.data2);

  if (*cmd1 != 0 || *cmd2 != 0) return false;

  DelayQueue::packDelayCommand(delayCmd, 1, 1);
  if (*cmd1 != 0x0104400000000000ULL || *cmd2 != 0) return false;

  DelayQueue::packDelayCommand(delayCmd, 2, 2);
  if (*cmd1 != 0x0204422000000000ULL || *cmd2 != 0) return false;

  DelayQueue::packDelayCommand(delayCmd, 3, 3);
  if (*cmd1 != 0x03044220CC000000ULL || *cmd2 != 0) return false;

  DelayQueue::packDelayCommand(delayCmd, 4, 4);
  if (*cmd1 != 0x04044220CC440000ULL || *cmd2 != 0) return false;

  DelayQueue::packDelayCommand(delayCmd, 45, 15);
  if (*cmd1 != 0x05044220CC44B7C0ULL || *cmd2 != 0) return false;

  DelayQueue::packDelayCommand(delayCmd, 1, 1);
  if (*cmd1 != 0x06044220CC44B7C0ULL || *cmd2 != 0x0440000000000000ULL) 
    return false;

  DelayQueue::packDelayCommand(delayCmd, 2, 2);
  if (*cmd1 != 0x07044220CC44B7C0ULL || *cmd2 != 0x0442200000000000ULL) 
    return false;

  DelayQueue::packDelayCommand(delayCmd, 3, 3);
  if (*cmd1 != 0x08044220CC44B7C0ULL || *cmd2 != 0x044220CC00000000ULL)
    return false;

  DelayQueue::packDelayCommand(delayCmd, 4, 4);
  if (*cmd1 != 0x09044220CC44B7C0ULL || *cmd2 != 0x044220CC44000000ULL)
    return false;

  DelayQueue::packDelayCommand(delayCmd, 45, 15);
  if (*cmd1 != 0x0A044220CC44B7C0ULL || *cmd2 != 0x044220CC44B7C000ULL)
    return false;

  return true;
}


int main(int argc, char *argv[])
{
  std::cout << "Beginning Delay Queue Test" << std::endl;

  DelayQueueTest testQueue;

  if (test01(testQueue)) {
    std::cout<<"PASSED"<<std::endl;
  } else {
    std::cout<< "FAILED"<<std::endl;
  }
  
  if (test02(testQueue)) {
    std::cout<<"PASSED"<<std::endl;
  } else {
    std::cout<< "FAILED"<<std::endl;
  }
  
  if (test03(testQueue)) {
    std::cout<<"PASSED"<<std::endl;
  } else {
    std::cout<< "FAILED"<<std::endl;
  }

  if (test04(testQueue)) {
    std::cout<<"PASSED"<<std::endl;
  } else {
    std::cout<< "FAILED"<<std::endl;
  }

  if (test05(testQueue)) {
    std::cout<<"PASSED"<<std::endl;
  } else {
    std::cout<< "FAILED"<<std::endl;
  }

  if (test06(testQueue)) {
    std::cout<<"PASSED"<<std::endl;
  } else {
    std::cout<< "FAILED"<<std::endl;
  }

  if (test07(testQueue)) {
    std::cout<<"PASSED"<<std::endl;
  } else {
    std::cout<< "FAILED"<<std::endl;
  }

  if (test08(testQueue)) {
    std::cout<<"PASSED"<<std::endl;
  } else {
    std::cout<< "FAILED"<<std::endl;
  }

  if (test09()) {
    std::cout<<"PASSED"<<std::endl;
  } else {
    std::cout<< "FAILED"<<std::endl;
  }
  return 0;
}








