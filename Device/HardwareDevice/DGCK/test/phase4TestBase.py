#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$

import sys
import unittest
import struct
import Control
import ControlExceptions
from time import sleep
from Control import HardwareDevice

from ACS import acsFALSE
from ACS import acsTRUE
from Acspy.Clients.SimpleClient import PySimpleClient

import Acspy.Util
from Acspy.Common.QoS import setObjectTimeout

container = ""

class automated(unittest.TestCase):

    def __init__(self, methodName="runTest"):
        self._client=PySimpleClient.getInstance()
        setObjectTimeout(Acspy.Util.ACSCorba.getManager(), 20000)
        self.out= ""
        self._phaseCommands = [(1,1),(2,2),(3,3),(4,4),(5,5)]
	self._componentName = "CONTROL/DV01/DGCK"
        self._componentType = "IDL:alma/Control/DGCK:1.0"
        self._componentCode = "DGCK"
        self._componentContainer = "CONTROL/" + container + "/cppContainer"
        unittest.TestCase.__init__(self,methodName)

    def __del__(self):
        if self.out != None:
            print self.out
        self._client.disconnect()


    def setUp(self):
        self._ref = self._client.getDynamicComponent(
	    self._componentName,
	    self._componentType,
	    self._componentCode,
	    self._componentContainer
	    )
        setObjectTimeout(self._ref, 20000)
        self._ref.hwConfigure()
        self._ref.hwInitialize()
        self._ref.hwOperational()

    def tearDown(self):
        self._ref.hwStop()
        self._client.releaseComponent(self._componentName)
        
    def startTestLog(self, testName):
        self.log("\n")
        self.log("-------------------------------------------------------")
        self.log("Begin "+testName)

    def endTestLog(self, testname):
        self.log("End" + testname)
        self.log("-------------------------------------------------------")
                 
    def log(self,msg):
        self.out += msg + "\n"


    # --------------------------------------------------
    # Test Definitions
    def test01(self):
        '''
        Test to make sure the DGCK can be put in the operational status
        '''
        self.failIf(self._ref.getHwState() != HardwareDevice.Operational,
                    "DGCK Failed to enter Operational State")



    def test02(self):
        '''
        Test the DGCK_STATUS Read Only Property
        '''
        self.startTestLog("Test 1: Property Test")
        prop = self._ref._get_DGCK_STATUS()
        self.failUnlessEqual(prop._get_name(),
                             self._componentName + ':DGCK_STATUS',
                             "DGCK Status Name error");
        returnVal = prop.get_sync()
        self.failIf(returnVal[1].code != 0,"Error reading property");
        self.log("Property "+ prop._get_name()+ " reports value "+
                 str(returnVal[0]))
        self.failIf((returnVal[0] != 0) and (returnVal[0] != 1),
                    "Illegal Value Returned")
        self.endTestLog("Test 1: DGCK Status Property Test")


    def test03(self):
        '''
        Test the MISSED_COMMAND Read Only Property
        '''
        self.startTestLog("Test 2: MISSED_COMMAND Property Test")
        prop = self._ref._get_MISSED_COMMAND()
        self.failUnlessEqual(prop._get_name(),
                             self._componentName + ':MISSED_COMMAND',
                             "MISSED_COMMAND Name error");
        returnVal = prop.get_sync()
        self.failIf(returnVal[1].code != 0,"Error reading property");
        self.log("Property "+ prop._get_name()+ " reports value " +
                 str(returnVal[0]))
        self.failIf((returnVal[0] != 0) and (returnVal[0] != 1))
        self.endTestLog("Test 2: MISSED_COMMAND Property test")


    def test04(self):
        '''
        Test the PS_VOLTAGE_CLOCK Read Only Property
        '''
        self.startTestLog("Test 4: PS_VOLTAGE_CLOCK Property Test")
        prop = self._ref._get_PS_VOLTAGE_CLOCK()
        self.failUnlessEqual(prop._get_name(),
                             self._componentName + ':PS_VOLTAGE_CLOCK',
                             "PS_VOLTAGE_CLOCK Name error");
        returnVal = prop.get_sync()
        self.failIf(returnVal[1].code != 0,"Error reading property");
        self.log("Property "+ prop._get_name()+ " reports value " +
                  str(returnVal[0]))
        self.failIf((returnVal[0] < 5.5) or (returnVal[0] > 7.5),
                    "Return value out of range.")
        self.endTestLog("Test 4: PS_VOLTAGE_CLOCK Property Test")

     
    def test06(self):
        '''
        Test the correct functioning of the missed command reset method
        '''
        self.startTestLog("Test 6: missed command reset method Test")
        
        prop = self._ref._get_MISSED_COMMAND()
        returnVal = prop.get_sync()
        if returnVal[0] != acsTRUE:
            self.log("Unable to complete test, Missed Command Indicator not faulted")
            return

        self._ref.MISSED_COMMAND_RESET()
        returnVal = prop.get_sync()
        self.failIf(returnVal[0] != acsFALSE, "Failed to reset indicator")
        self.endTestLog("Test 6: missed command reset method Test")

    def test07(self):
        '''
        Test the correct functioning of the PLL indicator reset method
        '''
        self.startTestLog("Test 7: PLL indicator reset method Test")
        
        prop = self._ref._get_DGCK_STATUS()
        returnVal = prop.get_sync()
        if returnVal[0] != acsTRUE:
            self.log("Unable to complete test, PLL Indicator not faulted")
            return

        self._ref.PLL_LOCK_RESET()
        returnVal = prop.get_sync()
        self.failIf(returnVal[0] != acsFALSE, "Failed to reset indicator")
        self.endTestLog("Test 7: PLL indicator reset method Test")

    def test08(self):
        '''
        This test verifies the behavior of the LAST_PHASE_COMMAND,
        CURRENT_PHASE and PHASE_COMMAND monitor/control points.
        '''

    

class interactive(automated):
    def __init__(self, methodName="runTest"):
        automated.__init__(self,methodName)

    def tearDown(self):
        self.out = None

    def log(self, msg):
        print msg


    # --------------------------------------------------
    # Test Definitions
    def test06(self):
        '''
        Test the correct functioning of the missed command reset method
        '''
        self.startTestLog("Test 6: missed command reset method Test")
        
        prop = self._ref._get_MISSED_COMMAND()
        returnVal = prop.get_sync()
        if returnVal[0] != acsTRUE:
            raw_input("Please fault the missed command indicator <enter to continue>")
            returnVal = prop.get_sync()
            self.failIf(returnVal[0] != acsTRUE,
                        "Failed to fault missed command indicator");

        self._ref.MISSED_COMMAND_RESET()
        returnVal = prop.get_sync()
        self.failIf(returnVal[0] != acsFALSE, "Failed to reset indicator")

    def test07(self):
        '''
        Test the correct functioning of the PLL indicator reset method
        '''
        self.startTestLog("Test 7: PLL indicator reset method Test")
        
        prop = self._ref._get_DGCK_STATUS()
        returnVal = prop.get_sync()
        if returnVal[0] != acsTRUE:
            raw_input("Please fault the PLL indicator <enter to continue>")
            returnVal = prop.get_sync()
            self.failIf(returnVal[0] != acsTRUE,
                        "Failed to fault PLL indicator");

        self._ref.PLL_LOCK_RESET()
        returnVal = prop.get_sync()
        self.failIf(returnVal[0] != acsFALSE, "Failed to reset indicator")
        self.endTestLog("Test 7: PLL indicator reset method Test")

    def test09(self):
        '''
        Test 5 from doc
        '''
        pass

    def test10(self):
        '''
        Test 6
        '''
        pass	
	

# **************************************************
# MAIN Program
if __name__ == '__main__':
    # For some reason ACS has problems unless I do this
    client = PySimpleClient.getInstance()
    if len(sys.argv) == 1:
        sys.argv.append("interactive")

    if len(sys.argv) == 2:
        sys.argv.append("DV01")

    container = sys.argv[2]
    sys.argv.remove(sys.argv[2])

    unittest.main()
