#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$

#import sys
import unittest
#import struct
#import ControlExceptions
#from ambManager import *
#from time import sleep
#from Acspy.Clients.SimpleClient import PySimpleClient
#from Acspy.Common.QoS import setObjectTimeout
#import Acspy.Util
from TETimeUtil import *
from CCL.DGCK import DGCK
from Control import HardwareDevice

class cclTest(unittest.TestCase):
    
    def setUp(self):
        self.dgck =DGCK("DA41",stickyFlag=True)
        self.failIf(self.dgck == None, "Failed to activate component")
        self.dgck.hwStop()
        self.failUnlessEqual(self.dgck.getHwState(), HardwareDevice.Stop,
                             "Unable to get DGCK to Stop State")

        self.dgck.hwStart() 
        self.failUnlessEqual(self.dgck.getHwState(), HardwareDevice.Start,
                             "Unable to get DGCK to Start State")
        self.dgck.hwConfigure()
        self.failUnlessEqual(self.dgck.getHwState(), HardwareDevice.Configure,
                             "Unable to get DGCK to Configure State")
        self.dgck.hwInitialize()
        self.failUnlessEqual(self.dgck.getHwState(), HardwareDevice.Initialize,
                             "Unable to get DGCK to Initialize State")
        self.dgck.hwOperational()
        self.failUnlessEqual(self.dgck.getHwState(),
                             HardwareDevice.Operational,
                             "Unable to get DGCK to Operational State")

    def tearDown(self):
        self.dgck.hwStop() 
        self.failUnlessEqual(self.dgck.getHwState(), HardwareDevice.Stop,
                             "Unable to get DGCK to Stop State")

    # --------------------------------------------------
    # Test Definitions
    def test01(self):
        '''
        Hardware Lifecycle test.  Move the device to operational and
        Ensure that the device is in operational mode.
        '''
        self.failUnlessEqual(self.dgck.getHwState(),
                             HardwareDevice.Operational,
                             "Unable to get DGCK to Operational State")
        
    def test02(self):
        '''
        Check that we can enable and disable delay tracking
        '''
        self.dgck.enableDelayTracking(False)
        self.failIf(self.dgck.delayTrackingEnabled(),
                    "DGCK has delay tracking enabled should be disabled.")
        self.dgck.enableDelayTracking(True)
        self.failUnless(self.dgck.delayTrackingEnabled(),
                        "DGCK failed to enable delay tracking.")
        self.dgck.enableDelayTracking(False)
        self.failIf(self.dgck.delayTrackingEnabled(),
                    "DGCK faid to disable delay tracking.")
        
    def test03(self):
        '''
        Check that we can properly get and set the delay value
        '''
        # Set the minimum value
        self.dgck.SET_DELAY(8E-9)
        self.failIf(abs(self.dgck.GET_DELAY()[0] - 8E-9) > 1E-14,
                    "Returned Delay does not match set delay.")
        self.dgck.SET_DELAY(80E-9)
        self.failIf(abs(self.dgck.GET_DELAY()[0] - 80E-9) > 1E-14,
                    "Returned Delay does not match set delay.")
        
        
    def test04(self):
        '''
        Check that we can properly set a single phase command
        '''
        self.dgck.setSinglePhase(8,24)
        time.sleep(1)
        cp = self.dgck.getCurrentPhase()
        self.failUnless(len(cp) == 1,
                        "Current Phase returns incorrect length.")
        self.failUnless(cp[0][0] == 24,
                        "Current Phase returns incorrect offset")
        self.failUnless(cp[0][1] == 8,
                        "Current Phase returns incorrect phase")




    def test05(self):
        '''
        Check that we can properly set a phase sequence
        '''
        numDelayCommands = 10
        delayList = []
        for idx in range(numDelayCommands):
            delayList.append([(idx+1) * 4, idx])
        self.dgck.setPhase(delayList)
        time.sleep(5)
        cp = self.dgck.getCurrentPhase()
        self.failUnless(len(cp) == numDelayCommands,
                        "Current Phase returns incorrect length.")
        for idx in range(numDelayCommands):
            self.failUnless(cp[idx][0] == delayList[idx][0],
                            "Current Phase returns incorrect offset")
            self.failUnless(cp[idx][1] == delayList[idx][1],
                            "Current Phase returns incorrect phase")
            
    def test06(self):
        '''
        Check that delay events are properly processed
        '''
        from Control import AntennaDelayEvent
        from Control import FineDelayValue
        
        # Create an AntennaDelayEvent which start now and has
        # 2 fine delay events 6 sec and 12 seconds in the future.
        
        self.dgck.enableDelayTracking(True)
        fds = [FineDelayValue(6001,1),
               FineDelayValue(12002, 3),
               FineDelayValue(12003, 5)]
        
        startEpoch = plusTE(unix2epoch(time.time()),5)
        stopEpoch  = plusTE(startEpoch, 300) # ~ 15 sec
        

        
        ade = AntennaDelayEvent(startEpoch.value, # StartTime 
                                stopEpoch.value,  # StopTime,
                                "DA41",           # Antenna Name
                                [],               # Coarse Delays
                                fds,              # Fine Delays
                                [])               # DelayTables
        self.dgck._HardwareDevice__hw.newDelayEvent(ade)
        
        time.sleep(7)
        cp = self.dgck.getCurrentPhase()
        self.failUnless(len(cp) == 1,
                        "Current Phase returns incorrect length.")
        self.failUnless(cp[0][0] == 1,
                        "Current Phase returns incorrect offset")
        self.failUnless(cp[0][1] == 2,
                        "Current Phase returns incorrect phase")
        
        
        time.sleep(6)
        cp = self.dgck.getCurrentPhase()
        self.failUnless(len(cp) == 2,
                        "Current Phase returns incorrect length.")
        self.failUnless(cp[0][0] == 2,
                        "Current Phase returns incorrect offset")
        self.failUnless(cp[1][0] == 3,
                        "Current Phase returns incorrect offset")
        
        self.failUnless(cp[0][1] == 6,
                        "Current Phase returns incorrect phase")
        self.failUnless(cp[1][1] == 10,
                        "Current Phase returns incorrect phase")
        
        self.dgck.enableDelayTracking(False)
        
        pass
    
# **************************************************
# MAIN Program
if __name__ == '__main__':
    unittest.main()
