#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$

import sys
import unittest
import struct
import ControlExceptions
from ambManager import *
from time import sleep
from Acspy.Clients.SimpleClient import PySimpleClient
from Acspy.Common.QoS import setObjectTimeout
import Acspy.Util
from TETimeUtil import *

class automated(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        unittest.TestCase.__init__(self,methodName)
        setObjectTimeout(Acspy.Util.ACSCorba.getManager(), 20000)
        self.manager = AmbManager(antenna)
        setObjectTimeout(self.manager._AmbManager__mgrRef, 20000)
        self.node    = 0x30
        self.channel = channel
        self.monitor = {0x61:8, 0x62:8, 0x63:1, 0x64:1, 0x70:2, 0x71:2,
                        0x30000:3,0x30001:4,0x30002:4,0x30003:4}
        self.command = {0x83:(0x63,1,"DGCK Status"),
                        0x84:(0x64,1,"Missed Command")}
        self.out = ""

    def __del__(self):
        if self.out != None:
            print self.out
        del(self.manager)


    def startTestLog(self, testName):
        self.log("\n")
        self.log("-------------------------------------------------------")
        self.log("Begin "+testName)

    def endTestLog(self, testname):
        self.log("End" + testname)
        self.log("-------------------------------------------------------")
                 
    def log(self,msg):
        self.out += msg + "\n"
        
    # --------------------------------------------------
    # Test Definitions
    def test01(self):
        '''
        Test one verifies that the device correctly responds to
        broadcast messages and returns the serial number when requested
        by RCA 0x0.

        For recordkeeping the serial number is returned.
        '''
        self.startTestLog("Test 1: Broadcast Response Test")
        try:
            (r,t) = self.manager.getNodes(self.channel)
        except:
            self.fail("Unable to get nodes on bus")

        foundNode = False
        for node in r:
            if node.node == self.node:
                if not foundNode:
                    foundNode = True
                    self.log("Device Serial Number: 0x%x" % node.sn)
                else:
                    self.fail("Node 0x%x responded at least twice" % self.node)

        self.failUnless(foundNode, "Node 0x%x did not respond" % self.node)

        try:
            (sn, time) = self.manager.findSN(self.channel,self.node)
        except:
            self.fail("Exception retrieving serial number from node")

        self.endTestLog("Test 1: Broadcast Response Test")

    def test02(self):
        self.log("\nTest 2 not executed (Interactive Test)")
        
    def test03(self):
        self.log("\nTest 3 does not require software")
        
    def test04(self):
        self.log("\nTest 4 not executed (Interactive Test)")

    def test05(self):
        '''
        Test 5 Verifies an appropiate response from the CURRENT_PHASE and
        LAST_PHASE_COMMAND monitor point.  It also ensures that the
        PHASE_COMMAND is correctly recieved.
        '''
        self.startTestLog("Test 5: Phase Control Tests")

        data = (0x05,0x04,0x42,0x10, 0xCC,0x44,0x15,0x40)
        t = self.manager.commandTE(self.channel, self.node, 0x81,
                                   struct.pack("!8B",*data))
        (r,t) = self.manager.monitor(self.channel,self.node,0x62)
        resp = struct.unpack("!8B",r)
        self.failUnless(resp == data, "Failed to get Current Phase")

        (r,t) = self.manager.monitor(self.channel,self.node,0x61)
        resp = struct.unpack("!8B",r)
        self.failUnless(resp == data, "Failed to get Last Phase Command")
        
        self.endTestLog("Test 5: Phase Control Tests")

    def test06(self):
        '''
        This test verifies the DGCK_STATUS monitor point and that the
        PLL\_Lock\_RESET control point correctly resets it
        '''
        self.startTestLog("Test 6: Digiclock Status monitor test")
        (r,t) = self.manager.monitor(self.channel, self.node, 0x63)
        self.failUnless(len(r) == 1,
                        "Digiclock Status returned incorrect length value")
        (resp,) = struct.unpack("!B",r)
        if resp != 1:
            self.log("Unable to perform PLL Reset test, status is not faulted")
            return

        t = self.manager.command(self.channel, self.node, 0x83,
                                 struct.pack("!B",0x1))
        (r,t) = self.manager.monitor(self.channel, self.node, 0x63)
        (resp,)= struct.unpack("!B",r)
        self.failUnless(resp == 0, "Unable to reset PLL Status");

        self.endTestLog("Test 6: Digiclock Status monitor test")

    def test07(self):
        '''
        This test verifies the MISSED_COMMAND monitor point and the
        MISSED_COMMAND_RESET control point.
        '''
        self.startTestLog("Test 7: Missed Command Tests")
        (r,t) = self.manager.monitor(self.channel, self.node, 0x64)
        self.failUnless(len(r) == 1,
                        "Missed Command returned incorrect length value")
        (resp,) = struct.unpack("!B",r)
        if resp != 1:
            self.log("Unable to perform Missed Reset test, status is not faulted")
            return

        t = self.manager.command(self.channel, self.node, 0x84,
                                 struct.pack("!B",0x1))
        (r,t) = self.manager.monitor(self.channel, self.node, 0x64)
        (resp,)= struct.unpack("!B",r)
        self.failUnless(resp == 0, "Unable to reset Missed Command Status");

        self.endTestLog("Test 7:  Missed Command Tests")

    def test08(self):
        '''
        Verify correct response from LOCK_INDICATOR_VOLTAGE monitor point.
        '''
        self.startTestLog("Test 8: Lock Indicator Voltage Test")
        (r,t) = self.manager.monitor(self.channel, self.node, 0x70)
        self.failUnless(len(r)==2,
                        "Lock Indicator Voltage incorrect response");
        self.endTestLog("Test 8: Lock Indicator Voltage Test")

    def test09(self):
        '''
        Verify correct response PS_VOLTAGE_CLOCK monitor point.
        '''
        self.startTestLog("Test 9: Power Supply Voltage Monitor")
        (r,t) = self.manager.monitor(self.channel, self.node, 0x71)
        self.failUnless(len(r)==2,
                        "PS Voltage Clock incorrect response");
        self.endTestLog("Test 9:  Power Supply Voltage Monitor")

    def test10(self):
        self.log("\nTest 10 not executed (Interactive Test)")

    def test11(self):
        self.log("\nTest 11 not executed (Interactive Test)")

class interactive(automated):
    def __init__(self, methodName="runTest"):
        automated.__init__(self,methodName)

    def tearDown(self):
        self.out = None

    def log(self, msg):
        print msg


    # --------------------------------------------------
    # Test Definitions
    def test02(self):
        '''
        This test does a CAN monitor at each of the monitor points in
        the ICD.  The timing from the end of the request to the beginning of
        the response should be measured using an oscilloscope to verify that
        the delay is less than 150 us.
        '''
        self.startTestLog("Test 2: Monitor Timing Test")
        self.log('''Connect CAN wires (AMB pin 2 and 7) to oscilliscope and
                    set for single trigger.  Time base should be approximatly
                    200 us.
                    Press <enter> to generate monitor request,
                    n[ext] to process next monitor point.
                 ''')

        rv = raw_input("press <enter> to begin:")
        for rca in self.monitor.keys():
            rv = ''
            while len(rv) == 0 or rv != "next"[0:len(rv)]:
                try:
                    (r,t) = self.manager.monitor(self.channel,self.node,rca)
                    self.failUnless(len(r)==self.monitor[rca],
                                    "Length of response incorrect for RCA:0x%x"
                                    % rca)
                    rv=raw_input("CAN request sent to RCA: 0x%x " % rca)
                except ControlExceptions.CAMBErrorEx:
                    self.fail("Exception generated monitoring point 0x%x"%rca)
        self.endTestLog("Test 2: Monitor Timing Test")

    def test04(self):
        '''
        This test toggles the reset wires.  It should be verified that the
        reset pulse is detected and the AMBSI resets.  In addition
        the levels on these lines should be checked using an oscilliscope
        '''
        self.startTestLog("Test 4: AMB Reset Test")
        self.log('''Connect Reset wires (AMB pin 1 and 6) to oscilliscope
                    and set for single trigger.  Time base should be
                    approximatly 200 us

                 ''')

        rv = raw_input("press <enter> to generate reset request, type e[xit] to exit ")
        while  len(rv) == 0 or rv != "exit"[0:len(rv)]:
            t = self.manager.reset(self.channel)
            rv=raw_input("Reset Generated ")

        self.endTestLog("Test 4: AMB Reset Test")

    def test10(self):
        '''
        Test 10 verifies that the device only responds to commands of the
        correct length
        '''

        self.startTestLog("Test 10: Ill formed command test")
        for key in self.command.keys():
            (r,t) = self.manager.monitor(self.channel, self.node,
                                         self.command[key][0])
            (resp,) = struct.unpack("!B",r)
            if resp != 1:
                rv=raw_input("Please force %s to faulted state <enter> to continue" % self.command[key][2])
                (r,t) = self.manager.monitor(self.channel, self.node,
                                             self.command[key][0])
                (resp,) = struct.unpack("!B",r)
                self.failUnless(resp == 1,
                                "%s never placed in faulted state" %
                                self.command[key][2])

            cmd = [0x01]
            while len(cmd) < 8:
                if len(cmd) != self.command[key][1]:
                    t = self.manager.command(self.channel, self.node, key,
                                             struct.pack("!%dB"%len(cmd),*cmd))
            
                    (r,t) = self.manager.monitor(self.channel, self.node,
                                                 self.command[key][0])
                    (resp,) = struct.unpack("!B",r)
                    self.failUnless(resp == 1,
                                    "%s reset with length %d" %
                                    (self.command[key][2], len(cmd)))
                cmd.append(0x01)

            
        self.endTestLog("Test 10: Ill formed command test")

    def test11(self):
        '''
        This test monitors all RCAs between 1 and 0x3FFFF (the full
        range allocated to each device).  It ensures that only
        monitors documented in the ICD respond, and that the response
        is valid (correct number of bytes).
        '''
        self.startTestLog("Test 11: Monitor Survey test")
        errorList = {}
        for RCA in range (0x1,0x3FFFF):
            try:
                (r,t) = self.manager.monitor(self.channel, self.node, RCA)
                if self.monitor.has_key(RCA):
                    if len(r) != self.monitor[RCA][2]:
                        errorList[RCA]=len(r)
                else:
                    errorList[RCA]=len(r)
            except:
                pass

        if len(errorList) != 0:
            self.log("%d offending responses" % len(errorList))
            if len(errorList) < 10:
                for idx in errorList.keys():
                    if self.monitor.has_key(RCA):
                        self.log("RCA 0x%x returned %d bytes (%d expected)" % \
                              (idx, errorList[idx], self.monitor[RCA][2]))
                    else:
                        self.log("RCA 0x%x returned %d bytes" % \
                              (idx, errorList[idx]))
            self.fail("%d Monitor points not in ICD Responeded" %
                      len(errorList))

        self.endTestLog("Test 11: Monitor Survey test")


    def test12(self):
        '''
	Phase Stability Test.
	
	Commands the DigiClock to 
	'''
        self.startTestLog("Test 12: Phase Stability Test")
	self.log(
	    '''
	    Now commanding DigiClock to keep signal phase constant.
	    Press <ctl-C> to end.
	    '''	    )
	phase = (0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00)
	packedPhase = struct.pack("!8B",*phase)
	
	#
        # Capture the ^C use to end the test.
        try:
            t = self.manager.commandTE(self.channel, self.node, 
                                       0x81, packedPhase)
            while (True):
                nextTE = plusTE(acstime.Epoch(t), 1)
		# print "The next phase command will be sent at: %d" % nextTE.value
		t = self.manager.commandTE(self.channel, self.node, 
                                       0x81, packedPhase, nextTE.value)
                (r,t) = self.manager.monitor(self.channel,self.node,0x62)
                resp = struct.unpack("!8B",r)
		# print "Current Phase = ", resp
                self.failUnless(resp == phase, "Failed to get Current Phase")

                (r,t) = self.manager.monitor(self.channel,self.node,0x61)
                # resp = struct.unpack("!8B",r)
		print "Last Phase = ", resp
                self.failUnless(resp == phase, "Failed to get Last Phase Command")
				       
	except KeyboardInterrupt:
            pass
        self.endTestLog("Test 12: Phase Stability Test")


# **************************************************
# MAIN Program
if __name__ == '__main__':
    # For some reason ACS has problems unless I do this
    client = PySimpleClient.getInstance()
    if len(sys.argv) == 1:
        sys.argv.append("interactive")

    if len(sys.argv) == 2:
        sys.argv.append("DV01")

    antenna = sys.argv[2]
    sys.argv.remove(sys.argv[2])

    channel = 0
    if len(sys.argv) == 3 :
        channel = int(sys.argv[2])
	sys.argv.remove(sys.argv[2])

    unittest.main()
