#!/usr/bin/env python

from Acspy.Nc.Consumer import Consumer
from Acspy.Clients.SimpleClient import PySimpleClient
import Control
import time
import sys


dg = None
antennaName = None
def dgckDataHandler(ArrayDelayEvent):
    for antennaEvent in ArrayDelayEvent.antennaDelayEvents:
        if antennaEvent.antennaName == antennaName:
            try:
                dg.newDelayEvent(antennaEvent)
                print "New event sent to DGCK"
            except:
                print "Error sending the event to the DGCK"
               

if __name__ == "__main__":

    if len(sys.argv) != 2:
        print "antenna name must be specified."
        print "Use: DGCKDelayClient.py <antennaName>"
        sys.exit(1)
        
    antennaName = sys.argv[1]
    componentName = "CONTROL/"+antennaName+"/DGCK"

    print componentName

    #Get the reference to the DGCK
    try:
        client = PySimpleClient()
    except:
        print "Error getting client reference, exiting"
        sys.exit(-1)
        
    try:
        dg = client.getComponent(componentName)
    except:
        print "Error getting reference to DGCK component exiting"
        sys.exit(-1)
            
    #Connect to the Notification Channel 
    c = Consumer(Control.CHANNELNAME_CONTROLREALTIME)
    c.addSubscription(Control.ArrayDelayEvent, dgckDataHandler)
    c.consumerReady()
    
    
    print "Connected to Notification Channel, press ENTER to quit client..."
    sys.stdin.readline()
    c.disconnect()
    del(dg)
