#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef DGCKHWSimImpl_h
#define DGCKHWSimImpl_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <DGCKHWSimBase.h>
#include <vector>
// For AMB::node_t and CAN::byte_t
#include <CANTypes.h>


namespace AMB
{
    /**
     * Please use this class to implement complex functionality for the
     * DGCKHWSimBase helper functions. Use AMB::TypeConvertion methods
     * for convenient type convertions.
     */
    class DGCKHWSimImpl: public DGCKHWSimBase
    {
        public:
        /// Constructor.
        DGCKHWSimImpl(AMB::node_t node,
            const std::vector< CAN::byte_t >& serialNumber);

        /// Destructor.
        virtual ~DGCKHWSimImpl()
        {
        };


        protected:
        virtual void setControlSetDelay(
            const std::vector< CAN::byte_t >& data);

        virtual void setControlPhaseCommand1(
            const std::vector< CAN::byte_t >& data);

        virtual void setControlPhaseCommand2(
            const std::vector< CAN::byte_t >& data);


        private:
        /// No copy constructor.
        DGCKHWSimImpl(const DGCKHWSimImpl&);

        /// No assignment operator.
        DGCKHWSimImpl& operator=(const DGCKHWSimImpl&);
    };
}
#endif
