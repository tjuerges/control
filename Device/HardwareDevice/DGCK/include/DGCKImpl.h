#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef DGCKImpl_h
#define DGCKImpl_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
//


// Base class(es)
#include <DGCKS.h>
#include <DGCKBase.h>
#include <DelayQueue.h>


// Allows managing alarm state flags not covered by BACI Alarms
#include <controlAlarmHelper.h>
#include <monitorHelper.h>
#include <RepeatGuard.h>


/// ------------ Writer Thread Class --------------
/// Forward declaration
class DGCKImpl;


class DGCKUpdateThread: public ACS::Thread
{
    public:
    DGCKUpdateThread(const ACE_CString& name, DGCKImpl& DGCK);
    virtual void runLoop();


    protected:
    DGCKImpl& dgckDevice_p;
};


class DGCKImpl: public DGCKBase,
    public MonitorHelper,
    public Control::AlarmHelper,
    virtual public POA_Control::DGCK
{
    public:
    friend class DGCKUpdateThread;

    /// Constructor
    DGCKImpl(const ACE_CString & name, maci::ContainerServices* cs);

    /// Destructor
    virtual ~DGCKImpl();

    /// -------------------- External Interface -------------------

    virtual void enableDelayTracking(bool enableDelayTracking);

    virtual bool delayTrackingEnabled();

    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual Control::DGCK::DelayCommandSeq* GetCurrentPhase(ACS::Time&);

    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual Control::DGCK::DelayCommandSeq* GetLastPhaseCommand(ACS::Time&);

    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual void SetPhaseCommand(const Control::DGCK::DelayCommandSeq&);

    /// ---------------- Component Lifecycle -----------------------
    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void initialize();

    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void cleanUp();

    /// \exception ControlDeviceExceptions::HwLifecycleEx
    virtual void hwDiagnostic();

    /// ---------------- Delay Consumer Methods ------------------------
    virtual void newDelayEvent(const Control::AntennaDelayEvent& event);


    protected:
    /// Error State Enumeration
    enum DGCKErrorCondition
    {
        NoValidDelays = 0x01,
        CommunicationError = 0x02,
        MissedCommandError = 0x04,
        OutOfLock = 0x08,
        BadDelayCommand = 0x10,
        PLLMonitorTimeError = 0x20
    };

    /// ---------------- Hardware Lifecycle Interface ---------------
    virtual void hwInitializeAction();

    virtual void hwOperationalAction();

    virtual void hwStopAction();

    /// ------------------ Monitor Helper Methods -------------------
    virtual void processRequestResponse(const AMBRequestStruct& response);

    /// ------------------ Alarm Helper (Error State) Methods ---------------------
    virtual void handleActivateAlarm(int newFlag);

    virtual void handleDeactivateAlarm(int newFlag);

    /// ---------------- Error State Methods ------------------------
    void updateThreadAction();


    private:
    /// No default ctor.
    DGCKImpl();

    /// No copy ctor.
    DGCKImpl(const DGCKImpl&);

    /// No assignment optor.
    DGCKImpl& operator=(const DGCKImpl&);

    virtual std::vector< Control::AlarmInformation > createAlarmVector();

    /// ========= Hardware queuing methods ===============
    virtual void queueMissedCommandMonitors();

    virtual void queueDelayCommands();

    virtual void queuePLLMonitors();

    virtual void queueMissedCommandReset();

    virtual void queuePLLLockReset();

    /// ========= Hardware Processing Methods ============
    virtual void processMissedCommandFlagMon(
        const MonitorHelper::AMBRequestStruct&);

    virtual void processPhaseCommand(const MonitorHelper::AMBRequestStruct&);

    virtual void processPllLockFlagMon(const MonitorHelper::AMBRequestStruct&);

    virtual void processResetMissedCommandFlagCmd(
        const MonitorHelper::AMBRequestStruct&);

    virtual void processResetPllLockFlagCmd(
        const MonitorHelper::AMBRequestStruct&);

    struct FineDelayQueueStruct
    {
        ACS::Time timeRequest;
        unsigned char delayValue;
    };

    /// This semaphore is used to tell when the update thread is really
    /// running and when it is not
    sem_t updateThreadSem_m;

    DelayQueue delayQueue_m;

    std::deque< FineDelayQueueStruct > delayValueQueue_m;

    DGCKUpdateThread* updateThread_p;

    ACS::Time lastMissedCommandTime_m;

    ACS::Time lastDelayCommandTime_m;

    ACS::Time lastPLLMonitorTime_m;

    /// Some state variables
    bool delayTrackingEnabled_m;

    /// Repeat guard for avoid logging flooding
    RepeatGuard guard;

    DelayQueue::DelayCommand singlePhaseCommand_m;

    const ACS::Time updateThreadCycleTime_m;

    const ACS::Time missedCommandInterval_m;

    /// Conversion Routines
    DelayQueue::DelayCommand phaseCmdWorldToRaw(
        const Control::DGCK::DelayCommandSeq& world);

    /// Conversion Routines
    Control::DGCK::DelayCommandSeq* phaseCmdRawToWorld(
        const DelayQueue::DelayCommand& raw);
};
#endif
