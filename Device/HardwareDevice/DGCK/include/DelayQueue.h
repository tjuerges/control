#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef DelayQueue_h
#define DelayQueue_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// jkern  2006-10-05  created
//


#include <DelayClientC.h>
#include <deque>
#include <FTSExceptions.h>
#include <ambDefs.h>
#include "Recursive_Thread_Mutex.h"


class DelayQueue
{
    public:
    DelayQueue();
    ~DelayQueue();

    struct DelayCommand
    {
        AmbDataMem_t data1[8];
        AmbDataMem_t data2[8];
    };

    void delayEvent(const Control::AntennaDelayEvent& event);

    /// This method returns in commandReturn the CAN command which should
    /// be executed at the request time.  That is this is the command time
    /// not the application time
    /// \exception FTSExceptions::NoValidDelayExImpl
    DelayCommand getCommand(const ACS::Time& requestTime);

    /// Clear all values with application time before the given value
    void clearBefore(ACS::Time clearTime);

    /// Clear all values with application time after or equal to the given
    // value
    void clearAfter(ACS::Time clearTime);

    /// This static method is a utility used to create a delay command
    /// suitable to being sent to the hardware */
    static void packDelayCommand(DelayCommand& cmd, short commandOffset,
        short fineDelay);

#if BYTE_ORDER != BIG_ENDIAN
    /// This method will swap the byte order of the command from host to
    /// network if they are not the same
    static void swapByteOrder(DelayCommand& cmd);
#endif

    protected:
    struct CmdStruct
    {
        ACS::Time commandTime;
        DelayCommand commandData;
    };

    std::deque< DelayQueue::CmdStruct > commandQueue_m;


    private:
    ACE_Recursive_Thread_Mutex syncMutex_m;
};
#endif
