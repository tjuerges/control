/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File DGCK.idl
 */

#ifndef DGCK_IDL
#define DGCK_IDL

/**
 * External device interface to DGCK.
 */

#include <DGCKBase.idl>
#include <DelayClient.idl>

#pragma prefix "alma"

module Control {

  interface DGCK : Control::DGCKBase,
                   Control::DelayClient
  {

    struct DelayCommand {
      short timeOffset;
      octet phase;
    }; 

    typedef sequence<DelayCommand> DelayCommandSeq;
    

    /* This method will turn on or off the delay tracking in the
       Digitizer Clock Module
    */
    void enableDelayTracking(in boolean enableDelayTracking);
    
    /* The method returns true when delay tracking is enabled
       on the Digitizer clock module
    */
    boolean delayTrackingEnabled();

    /**
     * MonitorPoint: GetLastPhaseCommand
     *
     * This monitor point will instruct the DTS Digitizer clock 
     * firmware to return the contents of the data in the last 
     * PHASE_COMMAND sent to it. If no PHASE_COMMAND has ever been 
     * sent then this command will return all zeros. If there has 
     * been some timing events were no phase command has been sent 
     * then the value returned by this command will correspond 
     * to the LAST_COMMAND sent in an earlier interval. It is expected 
     * that this command will only be used for debugging.
     */
    DelayCommandSeq GetLastPhaseCommand(out ACS::Time Timestamp)
      raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);
    
    /**
     * MonitorPoint: GetCurrentPhase
     * This command returns the phase of the Digitizer clock through 
     * the previous timing interval. Normally it will return the 
     * same data as the LAST_PHASE_COMMAND but if no PHASE_COMMAND 
     * was received in the preceding timing interval it will return 
     * data where the first byte is on, the time of the first change 
     * is zero and the phase of this change represents the current 
     * phase of the Digitizer clock.
     */
    DelayCommandSeq GetCurrentPhase(out ACS::Time Timestamp)
      raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

    /**
     * ControlPoint: SetPhaseCommand
     * This command takes a DelayCommandSequence and executes it on the 
     * hardware.  If delay tracking is enabled this method has no effect.
     */
    void SetPhaseCommand(in DelayCommandSeq newCommand)
      raises (ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

  };
};

#endif /* DGCK_IDL */
