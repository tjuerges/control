//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <DGCKHWSimImpl.h>
#include <vector>
// For AMB::node_t and CAN::byte_t
#include <CANTypes.h>


/**
 * Please use this class to implement complex functionality for the
 * DGCKHWSimBase helper functions. Use AMB::TypeConvertion
 * methods for convenient type convertions.
 */

AMB::DGCKHWSimImpl::DGCKHWSimImpl(AMB::node_t node,
    const std::vector< CAN::byte_t >& serialNumber):
    DGCKHWSimBase::DGCKHWSimBase(node, serialNumber)
{
}

void AMB::DGCKHWSimImpl::setControlSetDelay(
    const std::vector< CAN::byte_t >& data)
{
    AMB::DGCKHWSimBase::setControlSetDelay(data);
    AMB::DGCKHWSimBase::setMonitorDelay(
        AMB::DGCKHWSimBase::getControlSetDelay());
}

void AMB::DGCKHWSimImpl::setControlPhaseCommand1(
    const std::vector< CAN::byte_t >& data)
{
    AMB::DGCKHWSimBase::setControlPhaseCommand1(data);
    AMB::DGCKHWSimBase::setMonitorLastPhaseCommand1(data);

    for(unsigned int idx(0U); idx < data.size(); ++idx)
    {
        if(data[idx] != 0x00)
        {
            AMB::DGCKHWSimBase::setMonitorCurrentPhase1(data);

            return;
        }
    }
}

void AMB::DGCKHWSimImpl::setControlPhaseCommand2(
    const std::vector< CAN::byte_t >& data)
{
    AMB::DGCKHWSimBase::setControlPhaseCommand1(data);
    AMB::DGCKHWSimBase::setMonitorLastPhaseCommand2(data);

    for(unsigned int idx(0U); idx < data.size(); ++idx)
    {
        if(data[idx] != 0x00)
        {
            AMB::DGCKHWSimBase::setMonitorCurrentPhase2(data);

            return;
        }
    }
}
