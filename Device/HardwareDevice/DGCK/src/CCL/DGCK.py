#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It contains complementary functionality for a DGCK device.
"""

import CCL.DGCKBase
from CCL import StatusHelper
from CCL.logging import getLogger
from log_audience import OPERATOR
import ACSLog
import os

class DGCK(CCL.DGCKBase.DGCKBase):
    '''
    The DGCK class inherits from the code generated DGCKBase
    class and adds specific methods.
    '''

    # This is a bit hacky but it works to get the DelayCommand object
    # defined in the DGCK class
    from Control import DGCK as DgckInterface
    DelayCommand = DgckInterface.DelayCommand
    del(DgckInterface)

    '''
    The following  internal methods are used for converting from/to
    DelayCommands and a friendly delay sequence.
    '''
    def __seq_to_command(self, delaySeq):
        cmdSeq=[]
        for idx in range(0, len(delaySeq)):
            cmdSeq.append(DGCK.DelayCommand(timeOffset=delaySeq[idx][0],
                                            phase=delaySeq[idx][1]))
        return cmdSeq

    def __command_to_seq(self, delayCmdSeq):
        seq=[]
        for idx in range(0, len(delayCmdSeq[0])):
            seq.append([delayCmdSeq[0][idx].timeOffset,
                        delayCmdSeq[0][idx].phase])
        return seq
  
    '''
    The DGCK class inherits from the code generated DGCKBase
    class and adds specific methods.
    '''
    def __init__(self, antennaName = None, componentName = None, stickyFlag = False):
        '''
        The constructor creates a DGCK object by making use of
        the DGCKBase constructor.
        '''
        CCL.DGCKBase.DGCKBase.__init__(self, antennaName, componentName,
                                           stickyFlag)

    def __del__(self):
        CCL.DGCKBase.DGCKBase.__del__(self)

    def enableDelayTracking(self, enable):
        '''
        This function turns on (True) and off (False) the delay tracking for
        the Digitizer Clock Module.
        EXAMPLE:
        from CCL.DGCK import DGCK
        dgck = DGCK("DV01")
        dgck.enableDelayTracking(True)
        '''
        for key, val in self._devices.iteritems():
            self._devices[key].enableDelayTracking(enable)
            if enable:
                msg = "Delay tracking enabled"
            else:
                msg = "Delay tracking disabled"
            self.logToOperator(ACSLog.ACS_LOG_INFO, msg, key)
            
    def delayTrackingEnabled(self):
        '''
        This function returns the current state of delay tracking on this
        Digitizer Clock.  A return value of True denotes that delay tracking
        is currently enabled.  While a false value denotes that it is not
        enabled.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].delayTrackingEnabled()
        if len(self._devices) == 1:
            return result.values()[0]
        return result
    
    def setSinglePhase(self, newPhase, offset=0):
        '''
        This function will execute exactly one phase command, setting the
        DGCK to introduce a delay of newPhase/16 samples offset ms into
        the timing event.
        NOTE: This method will not have any effect if delay tracking is
        properly functioning.
        '''
        if newPhase < 0 or newPhase > 15 or offset <= 0  or offset > 47:
            #self.__logger
            throw
        self.setPhase([[offset, newPhase]])


    def setPhase(self, delaySequence):
        '''
        This command specifies how the Digitizer clock is to control 
        its phase over the next 48ms interval. DelaySequence is a list
        of Control.DGCK.DelayCommand objects.

        Note: This method will not have any effect if delay tracking is
        enabled.
                
        EXAMPLE:
        # Load the necessary defintions
        import CCL.DGCK
        from Control import DGCK as DgckInterface
        # create the object
        dgck = CCL.DGCK.DGCK("DV01") or dgck = CCL.DGCK.DGCK(["DV01","DA41"])
        # Set a delay sequency at the DGCK with the following syntax:
        # [[timeOff1, phase1], [timOff2, phase2], ..., [timeOff10, phase10]]
        dgck.setPhase([[25,15],[26,14],[27,13]])
        # Get the current phase
        dgck.getCurrentPhase()
        [[25,15],[26,14],[27,13]]
        # Get the last phase command valid
        dgck.getLastPhase()
        [[25,15],[26,14],[27,13]]
        # destroy the component
        del(dgck)
        '''
        if len(delaySequence) > 10:
            # Sequence is too long, reject the command
            throw

        for key, val in self._devices.iteritems():
            self._devices[key].SetPhaseCommand(self.__seq_to_command(delaySequence))
            msg = 'Phase command sent: (%d commands)\n' % len(delaySequence)
            for idx in range (0, len(delaySequence)):
                msg += "\tTime Offset: %02d, Phase Command: %02d\n" % \
                       (delaySequence[idx][0], delaySequence[idx][1])
            self.logToOperator(ACSLog.ACS_LOG_INFO, msg, key)
            

    def getCurrentPhase(self):
        '''
        This method returns the phase of the Digitizer clock through 
        the previous timing interval. Normally it will return the 
        same data as the LAST_PHASE_COMMAND but if no PHASE_COMMAND 
        was received in the preceding timing interval it will return 
        data where the first byte is on, the time of the first change 
        is zero and the phase of this change represents the current 
        phase of the Digitizer clock.
        The value returned by this command corresponds to a list of length
        equal to the number of commands (may be 0) and a DelayCommand object
        for each of the commands.
                
        EXAMPLE:
        See setPhase() command example.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self.__command_to_seq(self._devices[key].GetCurrentPhase())
        if len(self._devices) == 1:
            return result.values()[0]
        return result
                
    def getLastPhase(self):
        '''
        This method will instruct the DTS Digitizer clock 
        firmware to return the contents of the data in the last 
        PHASE_COMMAND sent to it. If no PHASE_COMMAND has ever been 
        sent then this command will return all zeros. If there has 
        been some timing events were no phase command has been sent 
        then the value returned by this command will correspond 
        to the PHASE_COMMAND sent in an earlier interval. It is expected 
        that this command will only be used for debugging.
        The value returned by this command corresponds to a list of length
        equal to the number of commands (may be 0) and a DelayCommand object
        for each of the commands.
                
        
        EXAMPLE:
        See setPhase() command example.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[key] = self.__command_to_seq(self._devices[key].GetLastPhaseCommand())
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def getMonitorList(self):
        '''
        This method extends the monitor list method of base class.
        Monitor points which have not been included by code generator
        should be added to the here
        '''
        missingList = ["LAST_PHASE_COMMAND_1",
                       "LAST_PHASE_COMMAND_2",
                       "CURRENT_PHASE_1",
                       "CURRENT_PHASE_2",
                       "PS_VOLTAGE",
                       "MODULE_CODES"]
                       
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[key] = CCL.DGCKBase.DGCKBase.getMonitorList(self)
            result[key].extend(missingList)
        if len(self._devices) == 1:
            return result.values()[0]
        return result    

    def STATUS(self):
        '''
        This method will display the status information of the device at
        the standard output.
        '''
        os.system("clear")
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[2]

            try:
                liVolt,t = self._devices[key].GET_LOCK_VOLTAGE()
                lLIndVol = StatusHelper.Line("LOCK__VOLTAGE", [StatusHelper.ValueUnit(liVolt,"V")]);
            except:
                lLIndVol = StatusHelper.Line("LOCK_VOLTAGE", [StatusHelper.ValueUnit("N/A")]);
                
            try:
                liPsVolt,t = self._devices[key].GET_PS_VOLTAGE()
                lLIndPsVol = StatusHelper.Line("PS_VOLTAGE", [StatusHelper.ValueUnit(liPsVolt,"V")]);
            except:
                lLIndPsVol = StatusHelper.Line("PS_VOLTAGE", [StatusHelper.ValueUnit("N/A")]);

            try:
                mCommand,t = self._devices[key].GET_MISSED_COMMAND_FLAG()
                if mCommand == 0:
                    lMComm = StatusHelper.Line("MISSED_COMMANDS", [StatusHelper.ValueUnit("n")]);
                else:
                    lMComm = StatusHelper.Line("MISSED_COMMANDS", [StatusHelper.ValueUnit("Y")]);
            except:
                lMComm = StatusHelper.Line("MISSED_COMMANDS", [StatusHelper.ValueUnit("N/A")]);

            try:
                pllLock,t = self._devices[key].GET_PLL_LOCK_FLAG()
                if pllLock == 0:
                    lPLLock = StatusHelper.Line("PLL LOCK", [StatusHelper.ValueUnit("y")]);
                else:
                    lPLLock = StatusHelper.Line("PLL LOCK", [StatusHelper.ValueUnit("N")]);
            except:
                lPLLock = StatusHelper.Line("PLL LOCK", [StatusHelper.ValueUnit("N/A")]);

            frame = StatusHelper.Frame(compName + "   Ant: " + antName,[lLIndVol, lLIndPsVol, lMComm, lPLLock])
            frame.printScreen()


    def logToOperator(self, priority, logMessage, component):
        '''
        Method which logs a report to with the OPERATOR flag set.
        '''
        #self._logger.logNotSoTypeSafe(priority, "%s: %s" %
        #                              (component, logMessage),
        #                              OPERATOR,
        #                              antenna = component.split('/')[1])
        pass

