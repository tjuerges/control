//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <DGCKImpl.h>
#include <vector>
#include <string>
#include <sstream>
#include <loggingMACROS.h>
#include <TETimeUtil.h>
#include <acstimeTimeUtil.h>
#include <acsutilTimeStamp.h>


/**
 *-----------------------
 * DGCK Constructor
 *-----------------------
 */
DGCKImpl::DGCKImpl(const ACE_CString& name, maci::ContainerServices* cs):
    DGCKBase(name, cs),
    MonitorHelper(),
    delayTrackingEnabled_m(false),
    guard(10000000ULL * 10ULL, 0),
    updateThreadCycleTime_m(5 * TETimeUtil::TE_PERIOD_ACS),
    missedCommandInterval_m(104 * TETimeUtil::TE_PERIOD_ACS)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    /* Zero out the single phase command until we need it */
    std::memset(&singlePhaseCommand_m, 0, sizeof(singlePhaseCommand_m));
}

/**
 *-----------------------
 * DGCK Destructor
 *-----------------------
 */
DGCKImpl::~DGCKImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    sem_destroy(&updateThreadSem_m);
}

/**
 *-------------------------
 * External Methods
 *-------------------------
 */
void DGCKImpl::enableDelayTracking(bool enableDelayTracking)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if((isReady() == true) && (delayTrackingEnabled_m != enableDelayTracking))
    {
        // There are probably already commands out there flush them and
        // send new ones
        AmbErrorCode_t flushStatus(AMBERR_PENDING);
        ACS::Time flushTime(0ULL);

        // Wait till the thread is really suspended
        sem_wait(&updateThreadSem_m);

        acstime::Epoch startTime;
        startTime.value = ::getTimeStamp();
        lastDelayCommandTime_m = TETimeUtil::rtControlTime(startTime, -5).value;

        // Flush all commands
        flushRCA(lastDelayCommandTime_m, getControlRCAPhaseCommand1(),
            &flushTime, &flushStatus);
        if(flushStatus != AMBERR_NOERR)
        {
            activateAlarm(CommunicationError);
        }

        flushRCA(lastDelayCommandTime_m, getControlRCAPhaseCommand2(),
            &flushTime, &flushStatus);
        if(flushStatus != AMBERR_NOERR)
        {
            activateAlarm(CommunicationError);
        }

        delayTrackingEnabled_m = enableDelayTracking;
        sem_post(&updateThreadSem_m);

        updateThreadAction();
        updateThread_p->resume();
    }
    else
    {
        delayTrackingEnabled_m = enableDelayTracking;
    }
}

bool DGCKImpl::delayTrackingEnabled()
{
    return delayTrackingEnabled_m;
}

void DGCKImpl::newDelayEvent(const Control::AntennaDelayEvent& event)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // When we are not in operational mode it is possible that we would
    // still be receiving events.  Ensure that this does not grow too much
    // by removing outdoated entries.
    const ACS::Time expTime(::getTimeStamp() - updateThreadCycleTime_m);
    delayQueue_m.clearBefore(expTime);

    // Add the new events to the queue
    delayQueue_m.delayEvent(event);

    // If we are operational check for outdated commands
    if(isReady() == true)
    {
        if(event.startTime <= lastDelayCommandTime_m)
        {
            // There are outdated commands we need to flush
            AmbErrorCode_t flushStatus(AMBERR_PENDING);
            ACS::Time flushTime(0ULL);

            acstime::Epoch startTime;
            startTime.value = ::getTimeStamp();
            lastDelayCommandTime_m = TETimeUtil::rtControlTime(
                startTime, -5).value;

            sem_wait(&updateThreadSem_m);

            // Flush all commands
            flushRCA(lastDelayCommandTime_m, getControlRCAPhaseCommand1(),
                &flushTime, &flushStatus);
            if(flushStatus != AMBERR_NOERR)
            {
                activateAlarm(CommunicationError);
            }

            flushRCA(lastDelayCommandTime_m, getControlRCAPhaseCommand2(),
                &flushTime, &flushStatus);
            if(flushStatus != AMBERR_NOERR)
            {
                activateAlarm(CommunicationError);
            }

            sem_post(&updateThreadSem_m);
            updateThreadAction();
        }

        updateThread_p->resume();
    }
}

Control::DGCK::DelayCommandSeq* DGCKImpl::GetCurrentPhase(
    ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(isReady() == false)
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Device is inactive!");
        throw ex.getINACTErrorEx();
    }

    DelayQueue::DelayCommand currentPhase;
    AmbDataLength_t dataLength[2];
    AmbErrorCode_t status[2];
    sem_t synchLock[2];

    sem_init(&synchLock[0], 0, 0);
    sem_init(&synchLock[1], 0, 0);

    monitorNextTE(getMonitorRCACurrentPhase1(), dataLength[0],
        currentPhase.data1, &synchLock[0], &timestamp, &status[0]);
    monitorNextTE(getMonitorRCACurrentPhase2(), dataLength[1],
        currentPhase.data2, &synchLock[1], &timestamp, &status[1]);

    sem_wait(&synchLock[0]);
    sem_destroy(&synchLock[0]);
    sem_wait(&synchLock[1]);
    sem_destroy(&synchLock[1]);

    if((status[0] != AMBERR_NOERR)
    || (status[1] != AMBERR_NOERR)
    || (dataLength[0] != sizeof(unsigned long long))
    || (dataLength[1] != sizeof(unsigned long long)))
    {
        throw ControlExceptions::CAMBErrorExImpl(__FILE__, __LINE__,
            __PRETTY_FUNCTION__).getCAMBErrorEx();
    }

    Control::DGCK::DelayCommandSeq_var rv(phaseCmdRawToWorld(currentPhase));
    return rv._retn();
}

Control::DGCK::DelayCommandSeq* DGCKImpl::GetLastPhaseCommand(
    ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(isReady() == false)
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Device is inactive!");
        throw ex.getINACTErrorEx();
    }

    DelayQueue::DelayCommand lastCmdData;
    AmbDataLength_t dataLength[2];
    AmbErrorCode_t status[2];
    sem_t synchLock[2];

    sem_init(&synchLock[0], 0, 0);
    sem_init(&synchLock[1], 0, 0);

    monitorNextTE(getMonitorRCALastPhaseCommand1(), dataLength[0],
        lastCmdData.data1, &synchLock[0], &timestamp, &status[0]);
    monitorNextTE(getMonitorRCALastPhaseCommand2(), dataLength[1],
        lastCmdData.data2, &synchLock[1], &timestamp, &status[1]);

    sem_wait(&synchLock[0]);
    sem_destroy(&synchLock[0]);
    sem_wait(&synchLock[1]);
    sem_destroy(&synchLock[1]);

    if((status[0] != AMBERR_NOERR)
    || (status[1] != AMBERR_NOERR)
    || (dataLength[0] != sizeof(unsigned long long))
    || (dataLength[1] != sizeof(unsigned long long)))
    {
        throw ControlExceptions::CAMBErrorExImpl(__FILE__, __LINE__,
            __PRETTY_FUNCTION__).getCAMBErrorEx();
    }

    Control::DGCK::DelayCommandSeq_var rv(phaseCmdRawToWorld(lastCmdData));
    return rv._retn();
}

void DGCKImpl::SetPhaseCommand(const Control::DGCK::DelayCommandSeq& newDelays)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    /**
     * NOTE: Since we always need to send a phase command
     *       regardless of if we have one or not we have the
     *       member variable singlePhaseCommand which will be
     *       used if the delay tracking is not enabled all
     *       we need to do is to store the new values in that
     *       variable and it will be picked up and sent to the
     *       hardware.
     *         JSK March 2008
     */

    if(isReady() == false)
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Device is inactive!");
        throw ex.getINACTErrorEx();
    }

    singlePhaseCommand_m = phaseCmdWorldToRaw(newDelays);
}

/**
 *------------------------
 * Component Lifecycle Methods
 *------------------------
 */
void DGCKImpl::initialize()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    CORBA::String_var compName(acscomponent::ACSComponentImpl::name());

    try
    {
        DGCKBase::initialize();
        MonitorHelper::initialize(compName.in(), getContainerServices());
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }

    if(sem_init(&updateThreadSem_m, 0, 1))
    {
        LOG_TO_OPERATOR(LM_ERROR, "Error Initializing Semaphore");
        throw acsErrTypeLifeCycle::LifeCycleExImpl(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }

    std::string threadName(compName.in());
    threadName += "WriterThread";
    updateThread_p = getContainerServices()->getThreadManager()->create<
        DGCKUpdateThread, DGCKImpl >(threadName.c_str(), *this);
    updateThread_p->setSleepTime(updateThreadCycleTime_m);

    // register alarms.
    initializeAlarms("DGCK", compName.in(), createAlarmVector());
}

void DGCKImpl::cleanUp()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        Control::HardwareDeviceImpl::hwStop();
    }
    catch(...)
    {
        LOG_TO_OPERATOR(LM_WARNING, "Caught an exception during transition to "
            "the hardware stop state.  Continuing anyway.");
    }

    // Shutdown the threads, the threads were suspended by the
    // baseclass call to hwStopAction so we just need to terminate them
    // here.
    updateThread_p->terminate();

    // Clear all the alarms
    forceTerminateAllAlarms();

    try
    {
        MonitorHelper::cleanUp();
        DGCKBase::cleanUp();
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
}

void DGCKImpl::hwDiagnostic()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        DGCKBase::diagnosticModeEnabled = true;
        DGCKBase::hwDiagnostic();
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw ControlDeviceExceptions::HwLifecycleExImpl(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
    }
}

/* ---------------- Hardware Lifecycle Methods -------------- */
void DGCKImpl::hwInitializeAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    DGCKBase::hwInitializeAction();

    // Clear all the alarms
    forceTerminateAllAlarms();

    MonitorHelper::resume();
    updateThread_p ->suspend();
}

void DGCKImpl::hwOperationalAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    DGCKBase::hwOperationalAction();

    acstime::Epoch startTime;
    startTime.value = ::getTimeStamp();
    lastMissedCommandTime_m = TETimeUtil::rtMonitorTime(startTime, 5).value;
    lastDelayCommandTime_m = TETimeUtil::rtControlTime(startTime, -5).value;
    lastPLLMonitorTime_m = TETimeUtil::rtMonitorTime(startTime, 5).value;

    updateThread_p->resume();
}

void DGCKImpl::hwStopAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    AmbErrorCode_t flushStatus(AMBERR_PENDING);
    ACS::Time flushTime(0ULL);

    // De-activate diagnostic mode
    DGCKBase::diagnosticModeEnabled = false;

    // Suspend the threads
    updateThread_p->suspend();

    // Flush all commands
    flushNode(0, &flushTime, &flushStatus);
    if(flushStatus != AMBERR_NOERR)
    {
        LOG_TO_OPERATOR(LM_INFO, "Communication failure flushing commands to "
            "device");
    }
    else
    {
        std::ostringstream msg;
        msg << "All commands and monitors flushed at "
            << flushTime;
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    }

    MonitorHelper::suspend();
    DGCKBase::hwStopAction();
    terminateAllAlarms();
}

/* ================ Monitoring Dispatch Method ================ */
void DGCKImpl::processRequestResponse(const AMBRequestStruct& response)
{
    if(response.Status == AMBERR_FLUSHED)
    {
        // Nothing to do, just return.
        return;
    }

    if(response.RCA == getMonitorRCAMissedCommandFlag())
    {
        // Response from setting the Frequency
        processMissedCommandFlagMon(response);
        return;
    }

    if(response.RCA == getControlRCAPhaseCommand1() || response.RCA
        == getControlRCAPhaseCommand2())
    {
        // Response from setting the Frequency
        processPhaseCommand(response);
        return;
    }

    if(response.RCA == getMonitorRCAPllLockFlag())
    {
        // Response from setting the Frequency
        processPllLockFlagMon(response);
        return;
    }

    if(response.RCA == getControlRCAResetMissedCommandFlag())
    {
        // Response from setting the Frequency
        processResetMissedCommandFlagCmd(response);
        return;
    }

    if(response.RCA == getControlRCAResetPllLockFlag())
    {
        // Response from setting the Frequency
        processResetPllLockFlagCmd(response);
        return;
    }
}

/* ================ Error Handeling Methods ================== */
void DGCKImpl::handleActivateAlarm(int newErrorFlag)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    DGCKBase::setError(createErrorMessage());

}

void DGCKImpl::handleDeactivateAlarm(int newErrorFlag)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    if (isAlarmSet())
    {
        DGCKBase::setError(createErrorMessage());
    }
    else
    {
        DGCKBase::clearError();
    }
}

std::vector< Control::AlarmInformation > DGCKImpl::createAlarmVector()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    std::vector< Control::AlarmInformation > aivector;
    {
        Control::AlarmInformation ai;
        ai.alarmCode = NoValidDelays;
        ai.alarmDescription = "Unable to find delay values for delay tracking";
        aivector.push_back(ai);
    }
    {
        Control::AlarmInformation ai;
        ai.alarmCode = CommunicationError;
        ai.alarmDescription = "AMB Failure Communicating with Hardware";
        aivector.push_back(ai);
    }
    {
        Control::AlarmInformation ai;
        ai.alarmCode = MissedCommandError;
        ai.alarmDescription = "At least one 48 ms tick did not have a command "
            "associated with it";
        aivector.push_back(ai);
    }
    {
        Control::AlarmInformation ai;
        ai.alarmCode = OutOfLock;
        ai.alarmDescription = "Phase Lock Loop is out of Lock";
        aivector.push_back(ai);
    }
    {
        Control::AlarmInformation ai;
        ai.alarmCode = BadDelayCommand;
        ai.alarmDescription = "Delay Command Executed at incorrect time";
        aivector.push_back(ai);
    }
    {
        Control::AlarmInformation ai;
        ai.alarmCode = PLLMonitorTimeError;
        ai.alarmDescription = "Status Monitor Executed at incorrect time";
        aivector.push_back(ai);
    }

    return aivector;
}


/* ================ Hardware Queueing Methods  ================= */

void DGCKImpl::queueMissedCommandMonitors()
{
    MonitorHelper::AMBRequestStruct* monReq;

    while(::getTimeStamp() + (updateThreadCycleTime_m * 2)
        > lastMissedCommandTime_m)
    {
        lastMissedCommandTime_m += missedCommandInterval_m;

        monReq = MonitorHelper::getRequestStruct();

        monReq->RCA = getMonitorRCAMissedCommandFlag();
        monReq->TargetTime = lastMissedCommandTime_m;

        monitorTE(monReq->TargetTime, monReq->RCA, monReq->DataLength,
            monReq->Data, monReq->SynchLock, &monReq->Timestamp,
            &monReq->Status);

        queueRequest(monReq);
    }
}

void DGCKImpl::queueDelayCommands()
{
    MonitorHelper::AMBRequestStruct* cmdReq1(0);
    MonitorHelper::AMBRequestStruct* cmdReq2(0);

    std::deque<std::string> logs;
    sem_wait(&updateThreadSem_m);

    // Queue Status and Delay Commands
    while(::getTimeStamp() + (updateThreadCycleTime_m * 2)
        > lastDelayCommandTime_m)
    {
        std::ostringstream msg;
        // Queue Delay Tracking Command
        lastDelayCommandTime_m += TETimeUtil::TE_PERIOD_ACS;

        DelayQueue::DelayCommand currentCmd = singlePhaseCommand_m;

        if(delayTrackingEnabled_m == true)
        {
            try
            {
                currentCmd = delayQueue_m.getCommand(lastDelayCommandTime_m);
                deactivateAlarm(NoValidDelays);
            }
            catch(const FTSExceptions::NoValidDelayExImpl& ex)
            {
                activateAlarm(NoValidDelays);
            }
        }

        std::memset(&singlePhaseCommand_m, 0, sizeof(singlePhaseCommand_m));

        // We always need to send at least one command do that
        cmdReq1 = MonitorHelper::getRequestStruct();
        std::memcpy(cmdReq1->Data, currentCmd.data1, 8);
        cmdReq1->RCA = getControlRCAPhaseCommand1();
        cmdReq1->TargetTime = lastDelayCommandTime_m;
        cmdReq1->DataLength = 8;

        msg << "At " << TETimeUtil::toTimeString(cmdReq1->TargetTime) 
            << " sending [";

        for (int i = 0; i < 8; i++) {
            msg << std::hex << std::setfill('0') << std::setw(2) 
                << static_cast<unsigned int>(cmdReq1->Data[i]) << " ";
        }
        msg << "] to 0x" 
            << std::hex << cmdReq1->RCA;
        commandTE(cmdReq1->TargetTime, cmdReq1->RCA, cmdReq1->DataLength,
            cmdReq1->Data, cmdReq1->SynchLock, &cmdReq1->Timestamp,
            &cmdReq1->Status);
        queueRequest(cmdReq1);

        // If necessary send a second command as well:
        // Note that Byte 1 of the second command set contains the offset
        // from the TE for the 6th fine delay and is thus the first byte of
        // data2 can only be zero if the entire command is 0.
        // JSK 4/14/08

        if(currentCmd.data2[0] != 0)
        {
            cmdReq2 = MonitorHelper::getRequestStruct();
            cmdReq2->RCA = getControlRCAPhaseCommand2();
            cmdReq2->TargetTime = lastDelayCommandTime_m;
            cmdReq2->DataLength = 8;
            std::memcpy(cmdReq2->Data, currentCmd.data2, 8);
            msg << " and [";
            for (int i = 0; i < 8; i++) {
                msg << std::hex << std::setfill('0') << std::setw(2) 
                    << static_cast<unsigned int>(cmdReq2->Data[i]) << " ";
            }
            msg << "] to 0x" 
                << std::hex << cmdReq2->RCA;
            commandTE(cmdReq2->TargetTime, cmdReq2->RCA, cmdReq2->DataLength,
                cmdReq2->Data, cmdReq2->SynchLock, &cmdReq2->Timestamp,
                &cmdReq2->Status);
            queueRequest(cmdReq2);
        }
        const int numDelays = static_cast<unsigned int>(cmdReq1->Data[0]);
        if (numDelays > 0) {
            int delay = ((cmdReq1->Data[1] & 0x03) << 2) | 
                ((cmdReq1->Data[2] & 0xC0) >> 6);
            msg << std::dec 
                << " (fine delay of " << delay << " * 15.625ps applied "
                << ((cmdReq1->Data[1] & 0xFC) >> 2)
                << " ms after the TE";
            logs.push_back(msg.str());
        } 
    }

    sem_post(&updateThreadSem_m);
    while (!logs.empty()) {
        LOG_TO_DEVELOPER(LM_DEBUG, logs.front());
        logs.pop_front();
    }
}

void DGCKImpl::queuePLLMonitors()
{
    MonitorHelper::AMBRequestStruct* monReq(0);

    while(::getTimeStamp() + (updateThreadCycleTime_m * 2)
        > lastPLLMonitorTime_m)
    {
        lastPLLMonitorTime_m += TETimeUtil::TE_PERIOD_ACS;

        monReq = MonitorHelper::getRequestStruct();

        monReq->RCA = getMonitorRCAPllLockFlag();
        monReq->TargetTime = lastPLLMonitorTime_m;

        monitorTE(monReq->TargetTime, monReq->RCA, monReq->DataLength,
            monReq->Data, monReq->SynchLock, &monReq->Timestamp,
            &monReq->Status);

        queueRequest(monReq);
    }
}

void DGCKImpl::queueMissedCommandReset()
{
    MonitorHelper::AMBRequestStruct* cmdReq(MonitorHelper::getRequestStruct());

    cmdReq->RCA = getControlRCAResetMissedCommandFlag();
    cmdReq->DataLength = 1;
    cmdReq->Data[0] = 0x01;

    command(cmdReq->RCA, cmdReq->DataLength, cmdReq->Data, cmdReq->SynchLock,
        &cmdReq->Timestamp, &cmdReq->Status);

    queueRequest(cmdReq);
}

void DGCKImpl::queuePLLLockReset()
{
    MonitorHelper::AMBRequestStruct* cmdReq(MonitorHelper::getRequestStruct());

    cmdReq->RCA = getControlRCAResetPllLockFlag();
    cmdReq->DataLength = 1;
    cmdReq->Data[0] = 0x01;

    command(cmdReq->RCA, cmdReq->DataLength, cmdReq->Data, cmdReq->SynchLock,
        &cmdReq->Timestamp, &cmdReq->Status);

    queueRequest(cmdReq);
}
/* ================ Hardware Processing Methods =================*/
void DGCKImpl::processMissedCommandFlagMon(
    const MonitorHelper::AMBRequestStruct& response)
{
    // Check Status
    if(response.Status != AMBERR_NOERR)
    {
        if(guard.checkAndIncrement() == true)
        {
            std::ostringstream msg;
            msg << "DGCK Missed Command Monitor Failed (Status = "
                << response.Status
                << ")";
            LOG_TO_OPERATOR(LM_WARNING, msg.str());
        }

        activateAlarm(CommunicationError);

        // The response structure contains garbage.  No point in evaluationg
        // the content.
        return;
    }
    else
    {
        deactivateAlarm(CommunicationError);
    }

    // Check Time
    if(response.Timestamp - response.TargetTime > TETimeUtil::TE_PERIOD_ACS / 2)
    {
        if(guard.checkAndIncrement() == true)
        {
            std::ostringstream msg;
            msg << "DGCK Missed Command Monitor at incorrect time ( "
                << (response.Timestamp - response.TargetTime) * 1e-7
                << "s off)";
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }
    }

    if(response.Data[0] == 0x01)
    {
        activateAlarm(MissedCommandError);
        queueMissedCommandReset();
    }
    else
    {
        deactivateAlarm(MissedCommandError);
    }
}

void DGCKImpl::processPhaseCommand(
    const MonitorHelper::AMBRequestStruct& response)
{
    // Check Status
    if(response.Status != AMBERR_NOERR)
    {
        if(guard.checkAndIncrement() == true)
        {
            std::ostringstream msg;
            msg << "DGCK Phase Command Failed (Status = "
                << response.Status
                << ")";
            LOG_TO_OPERATOR(LM_WARNING, msg.str());
        }

        activateAlarm(CommunicationError);

        // The response structure contains garbage.  No point in evaluationg
        // the content.
        return;
    }
    else
    {
        deactivateAlarm(CommunicationError);
    }

    // Check Time
    if((response.Timestamp - response.TargetTime)
        > (TETimeUtil::TE_PERIOD_ACS / 2))
    {
        if(guard.checkAndIncrement() == true)
        {
            std::ostringstream msg;
            msg << "Phase Command at incorrect time ( "
                << (response.Timestamp - response.TargetTime) * 1E-7
                << " s off)";
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }

        activateAlarm(BadDelayCommand);
    }
    else
    {
        deactivateAlarm(BadDelayCommand);
    }
}

void DGCKImpl::processPllLockFlagMon(
    const MonitorHelper::AMBRequestStruct& response)
{
    if(response.Status != AMBERR_NOERR)
    {
        if(guard.checkAndIncrement() == true)
            ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,
                (LM_WARNING,"Status Monitor Failed (Status = %d)",
                    response.Status));
        activateAlarm(CommunicationError);

        // No point in continuing because the response structure contains
        // garbage.
        return;
    }
    else
    {
        deactivateAlarm(CommunicationError);
    }

    // Check Time
    if((response.Timestamp - response.TargetTime)
        > (TETimeUtil::TE_PERIOD_ACS / 2))
    {
        if(guard.checkAndIncrement() == true)
        {
            std::ostringstream msg;
            msg << "Status Monitor at incorrect time ( "
                << (response.Timestamp - response.TargetTime) * 1E-7
                << " s off)";
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }

        activateAlarm(PLLMonitorTimeError);
    }
    else
    {
        deactivateAlarm(PLLMonitorTimeError);
    }

    if(response.Data[0] & 0x01)
    {
        activateAlarm(OutOfLock);
        queuePLLLockReset();
    }
    else
    {
        deactivateAlarm(OutOfLock);
    }
}

void DGCKImpl::processResetMissedCommandFlagCmd(
    const MonitorHelper::AMBRequestStruct& response)
{
    if(response.Status != AMBERR_NOERR)
    {
        if(guard.checkAndIncrement() == true)
        {
            std::ostringstream msg;
            msg << "Missed Command Reset Failed (Status = "
                << response.Status
                << ")";
            LOG_TO_OPERATOR(LM_WARNING, msg.str());
        }

        activateAlarm(CommunicationError);
    }
    else
    {
        deactivateAlarm(CommunicationError);
    }
}

void DGCKImpl::processResetPllLockFlagCmd(
    const MonitorHelper::AMBRequestStruct& response)
{
    if(response.Status != AMBERR_NOERR)
    {
        if(guard.checkAndIncrement() == true)
        {
            std::ostringstream msg;
            msg << "PLL Lock Reset Command Failed (Status = "
                << response.Status
                << ")";
            LOG_TO_OPERATOR(LM_WARNING, msg.str());
        }

        activateAlarm(CommunicationError);
    }
    else
    {
        deactivateAlarm(CommunicationError);
    }
}

/* ========================================================= */
void DGCKImpl::updateThreadAction()
{
    // Queue Missed Command Monitors
    queueMissedCommandMonitors();

    // Queue Delay Commands
    queueDelayCommands();

    // Queue Status Monitor
    queuePLLMonitors();

}

/*===========================================================*/
DGCKUpdateThread::DGCKUpdateThread(const ACE_CString& name, DGCKImpl& DGCK):
    ACS::Thread(name),
    dgckDevice_p(DGCK)
{
}

void DGCKUpdateThread::runLoop()
{
    dgckDevice_p.updateThreadAction();
}

/* ================ Conversion Routines =================== */
DelayQueue::DelayCommand DGCKImpl::phaseCmdWorldToRaw(
    const Control::DGCK::DelayCommandSeq& world)
{
    DelayQueue::DelayCommand rv;
    std::memset(&rv, 0, sizeof(rv));

    for(unsigned int idx(0); idx < world.length(); ++idx)
    {
        DelayQueue::packDelayCommand(rv, world[idx].timeOffset,
            world[idx].phase);
    }

    DelayQueue::swapByteOrder(rv);

    return rv;
}

Control::DGCK::DelayCommandSeq*
DGCKImpl::phaseCmdRawToWorld(const DelayQueue::DelayCommand& raw)
{
    Control::DGCK::DelayCommandSeq_var rv(new Control::DGCK::DelayCommandSeq);
    unsigned long long dataBuffer(0ULL);
    DelayQueue::DelayCommand localRaw(raw);

    DelayQueue::swapByteOrder(localRaw);
    rv->length(localRaw.data1[7]);
    for(unsigned int idx(0); idx < rv->length(); ++idx)
    {
        if(idx < 5)
        {
            dataBuffer = (*reinterpret_cast< const unsigned long long* >(
                localRaw.data1))
                    >> (46 - (10 * idx));
        }
        else
        {
            dataBuffer = (*reinterpret_cast< const unsigned long long* >(
                localRaw.data2))
                    >> (54 - (10 * (idx - 5)));
        }

        rv[idx].phase = dataBuffer & 0x000F;
        rv[idx].timeOffset = (dataBuffer & 0x03F0) >> 4;
    }

    return rv._retn();
}


#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(DGCKImpl);
