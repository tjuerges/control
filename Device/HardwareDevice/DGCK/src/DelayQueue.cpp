// $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2006 - 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA

#include <DelayQueue.h>
#include <TETimeUtil.h>
#include <iostream>
#include <cstring>

using std::ostringstream;
using std::deque;

DelayQueue::DelayQueue() {
    commandQueue_m.clear();
}

DelayQueue::~DelayQueue() {
}

void DelayQueue::delayEvent(const Control::AntennaDelayEvent& event) {
    {
        ostringstream msg;
        msg << "Initially the command queue ";
        if (commandQueue_m.empty()) {
            msg << "is empty";
        } else if (commandQueue_m.size() == 1) {
            msg << "has one command to be sent at " 
                << TETimeUtil::toTimeString(commandQueue_m.front().commandTime);
        } else {
            msg << "has " << commandQueue_m.size() << " commands to be sent from from "
                << TETimeUtil::toTimeString(commandQueue_m.front().commandTime)
                << " to "
                << TETimeUtil::toTimeString(commandQueue_m.back().commandTime);
        }
        // LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    }

    // We know that events should be on TE boundaries ensure the start and stop
    // times are
    ACS::Time startTime(0ULL);
    ACS::Time stopTime(0ULL);
    {
        acstime::Epoch tempEpoch;
        tempEpoch.value = event.startTime;
        startTime = TETimeUtil::floorTE(tempEpoch).value;
        tempEpoch.value = event.stopTime;
        stopTime = TETimeUtil::ceilTE(tempEpoch).value;
    }
    {
        ostringstream msg;
        msg << "Adding " <<  event.fineDelays.length() << " delay commands from " 
            <<  TETimeUtil::toTimeString(startTime)
            << " up to, but not including, " 
            << TETimeUtil::toTimeString(stopTime);
        // LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    }
    ACS::ThreadSyncGuard guard(&syncMutex_m);
    // Get the queue start time *before* we start erasing elements from it 
    ACS::Time startOfQueue;
    if (!commandQueue_m.empty()) {
        startOfQueue = commandQueue_m.front().commandTime;
    } else {
        startOfQueue = startTime;
    }

    DelayQueue::CmdStruct newCmd;
    const ACS::Time commandStartTime = startTime - TETimeUtil::TE_PERIOD_ACS;
    if (!commandQueue_m.empty() &&
        (commandQueue_m.back().commandTime >= commandStartTime)) {
        // Clear out previous entries from startTime onwards
        deque< DelayQueue::CmdStruct >::iterator newStart(commandQueue_m.begin());
        while ((newStart != commandQueue_m.end()) && 
               (newStart->commandTime < commandStartTime)) {
            ++newStart;
        }

        const ACS::Time commandStopTime = stopTime - TETimeUtil::TE_PERIOD_ACS;
        deque<DelayQueue::CmdStruct>::iterator newEnd(newStart);
        while ((newEnd != commandQueue_m.end()) && 
               (newEnd->commandTime < commandStopTime)) {
            ++newEnd;
        }
        {
            ostringstream msg;
            msg << "Erasing commands to be sent at "
                << TETimeUtil::toTimeString(newStart->commandTime);

            if (newEnd != commandQueue_m.end()) {
                msg << " up to, but not including, the command sent at "
                    << TETimeUtil::toTimeString(newEnd->commandTime);
            } else {
                msg << " up to the end of the queue at "
                    << TETimeUtil::toTimeString(commandQueue_m.back().commandTime);
            }
            // LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }
        commandQueue_m.erase(newStart, newEnd);
    }

    // Populate the queue with events
    unsigned int fineDelayIdx(0U);

    // Work out when to start filling the queue. Normally the delay event will
    // have a start time in the future. But if its in the past then do not
    // bother filling the queue with delays that will not be used. This is
    // needed to ensure that the most recent fine delay from the past, but part
    // of this delay event, gets to the front of the queue and hence is
    // actually sent to the hardware.
    ACS::Time applicationTime(startTime);
    if (!commandQueue_m.empty()) {
        if (startOfQueue > applicationTime) {
            applicationTime = startOfQueue + TETimeUtil::TE_PERIOD_ACS;
        }
    }
    for (;applicationTime < stopTime;
         applicationTime += TETimeUtil::TE_PERIOD_ACS) {
        // applicationOffset_ms cannot be negative as applicationTime >= startTime
        int applicationOffset_ms = (applicationTime - startTime) / 10000;
        newCmd.commandTime = applicationTime - TETimeUtil::TE_PERIOD_ACS;
        // All zeros means "no change". 
        std::memset(newCmd.commandData.data1, 0, 8);
        std::memset(newCmd.commandData.data2, 0, 8);

        // commandOffset may be negative if the delay event arrived in the past
        int commandOffset;
        while ((fineDelayIdx < event.fineDelays.length()) && 
               ((commandOffset = (event.fineDelays[fineDelayIdx].relativeStartTime
                                  - applicationOffset_ms)) <= 48)) {
            const short thisDelay = event.fineDelays[fineDelayIdx].fineDelay;
//             ostringstream msg;
//             msg << "fineDelayIdx: " << fineDelayIdx
//                 << " relativeStartTime: " << event.fineDelays[fineDelayIdx].relativeStartTime
//                 << " applicationOffset_ms: " << applicationOffset_ms
//                 << " Command offset: " << commandOffset
//                 << " Delay Value: " << thisDelay
//                 ;
//             LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
            if (commandOffset > 0) {
                packDelayCommand(newCmd.commandData, commandOffset - 1, thisDelay);
            } else {
                // The commandOffset might be in the past. If so create a
                // command to have the delay sent as soon as possible. This
                // ensures that delay events, that arrived with a start
                // time in the past, send the most recent fine delay change
                // as as soon as possible (see CVS-828).
                // We only want the most recent previous delay command sent so
                // reset the command to zero to prevent the accumulation of
                // multiple fine delays all with a time offset of 0ms.
                std::memset(newCmd.commandData.data1, 0, 8);
                std::memset(newCmd.commandData.data2, 0, 8);

                // If the commandOffset is zero then the fine delay change
                // should have been sent on the previous TE with an offset
                // of 48. As thats too difficult send it on this TE with an
                // offset of 0. TODO. Address this if it ever becomes
                // significant.
                packDelayCommand(newCmd.commandData, 0, thisDelay);
            }
            ++fineDelayIdx;
        }

#if BYTE_ORDER != BIG_ENDIAN
        swapByteOrder(newCmd.commandData);
#endif

        // Usually we will just be inserting at the back, optimize for this
        if ((commandQueue_m.empty() == true) ||
            (commandQueue_m.back().commandTime < newCmd.commandTime)) {
            commandQueue_m.push_back(newCmd);
        } else {
            // Bad (and rare) case, search for a place to insert it
            std::deque< DelayQueue::CmdStruct >::iterator insertPoint(
                commandQueue_m.begin());
            while(insertPoint->commandTime < newCmd.commandTime) {
                ++insertPoint;
            }
            std::deque< DelayQueue::CmdStruct >::iterator 
                previousPt(insertPoint);
            previousPt--;
            commandQueue_m.insert(insertPoint, newCmd);
        }
    }
    {
        ostringstream msg;
        msg << "Finally the command queue ";
        if (commandQueue_m.empty()) {
            msg << "is empty";
        } else if (commandQueue_m.size() == 1) {
            msg << "has  one command to be sent at " 
                << TETimeUtil::toTimeString(commandQueue_m.front().commandTime);
        } else {
            msg << "has " << commandQueue_m.size() << " commands to be sent from from "
                << TETimeUtil::toTimeString(commandQueue_m.front().commandTime)
                << " to "
                << TETimeUtil::toTimeString(commandQueue_m.back().commandTime);
        }
        // LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    }
}

DelayQueue::DelayCommand DelayQueue::getCommand(const ACS::Time& requestTime) {
    ACS::ThreadSyncGuard guard(&syncMutex_m);

    bool gotPreviousCmd = false;
    DelayQueue::DelayCommand lastNonZeroCmd;
    while (!commandQueue_m.empty() && 
           (commandQueue_m.front().commandTime < requestTime)) {
        DelayQueue::DelayCommand frontOfQueue = commandQueue_m.front().commandData;
        if (static_cast<int>(frontOfQueue.data1[0]) != 0) {
            // need to do a deep copy here otherwise the pointers will, when
            // commands are removed from the front of queue (below), point at
            // undefined memory
            memcpy(lastNonZeroCmd.data1, frontOfQueue.data1, 8);
            memcpy(lastNonZeroCmd.data2, frontOfQueue.data2, 8);
            gotPreviousCmd = true;
//             ostringstream msg;
//             msg << "Found an old non-zero command at " 
//                 << TETimeUtil::toTimeString(commandQueue_m.front().commandTime) 
//                 << " [";
//             for (int i = 0; i < 8; i++) {
//                 msg << std::hex << std::setfill('0') << std::setw(2) 
//                     << static_cast<unsigned int>(lastNonZeroCmd.data1[i]) << " ";
//             }
//             msg << "]";
//             LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }
        commandQueue_m.pop_front();
    }

    if(commandQueue_m.empty() == true) {
        throw FTSExceptions::NoValidDelayExImpl(__FILE__, __LINE__,
                                                __PRETTY_FUNCTION__);
    }

    if (commandQueue_m.front().commandTime != requestTime) {
        throw FTSExceptions::NoValidDelayExImpl(__FILE__, __LINE__,
                                                __PRETTY_FUNCTION__);
    }

    DelayQueue::DelayCommand rv(commandQueue_m.front().commandData);
    commandQueue_m.pop_front();

    if (static_cast<int>(rv.data1[0]) == 0 && gotPreviousCmd) {
//         ostringstream msg;
//         msg << "Sending an old command with [";
//         for (int i = 0; i < 8; i++) {
//             msg << std::hex << std::setfill('0') << std::setw(2) 
//                 << static_cast<unsigned int>(lastNonZeroCmd.data1[i]) << " ";
//         }
//         msg << "]";
//         LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        return lastNonZeroCmd;
    }
    
    return rv;
}

void DelayQueue::clearBefore(ACS::Time clearTime)
{
    ACS::ThreadSyncGuard guard(&syncMutex_m);

    // Convert from application to command time
    clearTime -= TETimeUtil::TE_PERIOD_ACS;
    std::deque< DelayQueue::CmdStruct >::iterator iter(commandQueue_m.begin());
    while((iter != commandQueue_m.end()) && (iter->commandTime < clearTime)) {
        ++iter;
    }

    commandQueue_m.erase(commandQueue_m.begin(), iter);
}

void DelayQueue::clearAfter(ACS::Time clearTime)
{
    ACS::ThreadSyncGuard guard(&syncMutex_m);

    // Convert from application to command time
    clearTime -= TETimeUtil::TE_PERIOD_ACS;
    std::deque< DelayQueue::CmdStruct >::iterator iter(commandQueue_m.begin());
    while((iter != commandQueue_m.end()) && (iter->commandTime < clearTime))
    {
        ++iter;
    }

    commandQueue_m.erase(iter, commandQueue_m.end());
}

void DelayQueue::packDelayCommand(DelayCommand& cmdStruct, short commandOffset,
    short fineDelay)
{
    unsigned long long* dataBuf1(
        reinterpret_cast< unsigned long long* >(cmdStruct.data1));
    unsigned long long* dataBuf2(
        reinterpret_cast< unsigned long long* >(cmdStruct.data2));

    ++(cmdStruct.data1[7]);
    if(cmdStruct.data1[7] <= 5)
    {
        *dataBuf1 |= static_cast< unsigned long long >(commandOffset)
            << (50 - (10 * (cmdStruct.data1[7] - 1)));
        *dataBuf1 |= static_cast< unsigned long long >(fineDelay)
            << (46 - (10 * (cmdStruct.data1[7] - 1)));
    }
    else
    {
        *dataBuf2 |= static_cast< unsigned long long >(commandOffset)
            << (58 - (10 * (cmdStruct.data1[7] - 6)));
        *dataBuf2 |= static_cast< unsigned long long >(fineDelay)
            << (54 - (10 * (cmdStruct.data1[7] - 6)));
    }
}

#if BYTE_ORDER != BIG_ENDIAN
void DelayQueue::swapByteOrder(DelayCommand& commandData)
{
    // Swap the bytes
    AmbDataMem_t tmp(0U);
    for(int idx(0); idx < 4; ++idx)
    {
        tmp = commandData.data1[idx];
        commandData.data1[idx] = commandData.data1[7 - idx];
        commandData.data1[7 - idx] = tmp;
        tmp = commandData.data2[idx];
        commandData.data2[idx] = commandData.data2[7 - idx];
        commandData.data2[7 - idx] = tmp;
    }
}
#endif
