/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DGCK.panels;

import alma.Control.device.gui.DGCK.IcdMonitorPoints;
import alma.Control.device.gui.DGCK.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.RangeOkIndicatorWidget;
import alma.Control.device.gui.common.widgets.Status;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**
 * Hardware device GUI data is divided into Summary data and Detail data.
 * Summary data is always visible on the GUI. Detail data is organized to keep
 * related data together and separate unrelated data.
 * 
 * This class contains the Summary data.
 * 
 * @author Scott Rankin srankin@nrao.edu
 * @author David Hunter dhunter@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */
public class SummaryPanel extends DevicePanel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;

    public SummaryPanel(Logger logger, DevicePM dPM) {
        super(logger, dPM);
        buildPanel();
    }

    protected void buildPanel() {
        this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(
                devicePM.getDeviceDescription()),BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.weightx = 100;
        c.weighty = 100;
        c.gridx = 1;
        c.gridy = 0;
        addLeftLabel("PLL Lock", c);
        add(new BooleanMonitorPointWidget(logger, devicePM
                .getMonitorPointPM(IcdMonitorPoints.PLL_LOCK_FLAG),
                Status.GOOD, "PLL Locked", Status.FAILURE, "PLL Lock Error"), c);
        c.gridx+=2;
        addLeftLabel("Phase Command", c);
        add(new BooleanMonitorPointWidget(logger, devicePM
                .getMonitorPointPM(IcdMonitorPoints.MISSED_COMMAND_FLAG),
                Status.GOOD, "Phase Command OK", Status.FAILURE,
                "Phase Command Missed"), c);
        c.gridx+=2;
        addLeftLabel("Lock V", c);
        //This is a debug monitor point. For control software the ICD range is 0 to 5. However,
        //for only the gui, the product engineer requested that we display errors if it is outside
        //of a stricter range.
        (IcdMonitorPoints.LOCK_VOLTAGE).setRangeLowerBound(1.0);
        (IcdMonitorPoints.LOCK_VOLTAGE).setRangeUpperBound(3.5);
        add(new RangeOkIndicatorWidget(logger, devicePM
                .getMonitorPointPM(IcdMonitorPoints.LOCK_VOLTAGE),
                Status.FAILURE, "Lock Voltage out of range", Status.GOOD,
                "Lock Voltage OK"), c);
        c.gridx+=2;
        addLeftLabel("PS V", c);
        add(new RangeOkIndicatorWidget(logger, devicePM
                .getMonitorPointPM(IcdMonitorPoints.PS_VOLTAGE),
                Status.FAILURE, "Power Supply Voltage out of range",
                Status.GOOD, "Power Supply Voltage OK"), c);

        this.setVisible(true);
    }

    /*
     * A simple helper function that allows you to add a label to the left of
     * the current location with grid bag layout.
     */
    protected void addLeftLabel(String s, GridBagConstraints c) {
        c.gridx--;
        c.anchor=GridBagConstraints.EAST;
        add(new JLabel(s), c);
        c.anchor=GridBagConstraints.WEST;
        c.gridx++;
    }
}
//
// O_o
