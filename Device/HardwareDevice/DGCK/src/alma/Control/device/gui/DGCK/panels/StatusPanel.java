/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DGCK.panels;

import alma.Control.device.gui.DGCK.IcdControlPoints;
import alma.Control.device.gui.DGCK.IcdMonitorPoints;
import alma.Control.device.gui.DGCK.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BooleanControlPointWidget;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.FloatControlPointWidget;
import alma.Control.device.gui.common.widgets.FloatMonitorPointWidget;
import alma.Control.device.gui.common.widgets.Status;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized in panels and tabs to group related data together.
 * 
 * TODO: Update this comment to reflect what this pane does.
 * 
 * Tabbed panels are used to group related data together and separate unrelated data. These are created as
 * required for each GUI.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version 
 * @since 5.0.3
 */

public class StatusPanel extends DevicePanel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;

    public StatusPanel(Logger logger, DevicePM dPM) {
        super(logger, dPM);
        buildWidgets(logger, dPM);
        buildSubPanels(logger, dPM);
        buildPanel();
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;

        //TODO: rename sub-panels as appropriate.
        add(mainStatusPanel, c);
        c.gridy++;
        add(resetPanel, c);

        setVisible(true);
    }

    /**
     * It is necessary to build all sub-panels before buildPanel() is called.
     * 
     * @param logger to be used by the sub-panels.
     * @param dPM the device presentation model for the device containing the panel.
     */
    private void buildSubPanels(Logger logger, DevicePM dPM) {
        mainStatusPanel = new MainStatusPanel(logger, dPM);
        resetPanel = new ResetPanel(logger, dPM);
    }

    /**
     * It is necessary to build some widgets before buildPanel() is called.  Where that is necessary, 
     * build them in this method.
     * 
     * If it is not necessary to build a widget before buildPanel() is called, build the widget where
     * it will be used.
     * 
     * @param logger to be used by the sub-panels.
     * @param dPM the device presentation model for the device containing the panel.
     */
    private void buildWidgets(Logger logger, DevicePM dPM) {
        DGCK_INIT_booleanControlPointWidget = new BooleanControlPointWidget(logger, dPM
                .getControlPointPM(IcdControlPoints.DGCK_INIT), "Reset this DGCK");
    }

    /*
     * Has the status info on the DGCK.
     */
    private class MainStatusPanel extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public MainStatusPanel(Logger logger, DevicePM dPM) {
            super(logger, dPM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createEtchedBorder(1));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.fill = GridBagConstraints.HORIZONTAL;
            c.gridx = 1;
            c.gridy = 0;
            addLeftLabel("PLL Lock", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.PLL_LOCK_FLAG), Status.GOOD, "PLL Locked", Status.FAILURE,
            "PLL Lock Error"), c);
            c.gridx++;
            add(new BooleanControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.RESET_PLL_LOCK_FLAG), "Reset PLL Lock"), c);
            c.gridx--;

            c.gridy++;
            addLeftLabel("Phase Command", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.MISSED_COMMAND_FLAG), Status.GOOD, "Phase Command OK",
                    Status.FAILURE, "Phase Command Missed"), c);
            c.gridx++;
            add(new BooleanControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.RESET_MISSED_COMMAND_FLAG), "Reset Phase Command"), c);
            c.gridx--;

            c.gridy++;
            addLeftLabel("Lock Voltage", c);
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.LOCK_VOLTAGE), true), c);

            c.gridy++;
            addLeftLabel("PS Voltage", c);
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.PS_VOLTAGE), true), c);

            c.gridy++;
            addLeftLabel("TE Delay", c);
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DELAY), true, 2), c);

            // TODO Figure out why SET_DELAY is being rejected
            c.gridx++;
            c.gridy++;
            add(new FloatControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.DELAY), 0.008, 1000000, "Set TE Delay (\u03BCs)"), c);
            setVisible(true);
        }
    }

    /*
     * Just the DGCK reset button. Used to clearly separate the reset from everything else.
     */
    private class ResetPanel extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public ResetPanel(Logger logger, DevicePM dPM) {
            super(logger, dPM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createEtchedBorder(1));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.gridx = 1;
            c.gridy = 0;
            add(DGCK_INIT_booleanControlPointWidget, c);
        }
        
        
    }
    
    private BooleanControlPointWidget DGCK_INIT_booleanControlPointWidget;
    private MainStatusPanel mainStatusPanel;
    private ResetPanel resetPanel;
}

//
// O_o

