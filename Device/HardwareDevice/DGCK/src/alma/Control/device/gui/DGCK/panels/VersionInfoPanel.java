/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DGCK.panels;

import alma.Control.device.gui.DGCK.IcdMonitorPoints;
import alma.Control.device.gui.DGCK.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.IntegerMonitorPointWidget;
import alma.Control.device.gui.common.widgets.LruMonitorPointWidget;
import alma.Control.device.gui.common.widgets.VersionMonitorPointWidget;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized in panels and tabs to group related data together.
 *
 * This class contains a template for a single data pane
 * TODO: Replace VersionInfoPanel with the proper class name
 * TODO: Update this comment to reflect what the pane does.
 *
 * Tabbed panels are used to group related data together and separate unrelated data. These are created as
 * required for each GUI.
 *
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class VersionInfoPanel extends DevicePanel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;

    public VersionInfoPanel(Logger logger, DevicePM dPM) {
        super(logger, dPM);
        buildPanel();
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Module Information"),
                BorderFactory.createEmptyBorder(1, 1, 1, 1)));

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.gridx = 0;
        c.gridy = 0;

        //Add more sub-panels as needed
        // Add all the points in a single column, starting at the top, with the labels on the left.
        c.gridx = 1;
        c.gridy = 0;

        // TODO Make sure that 0xFFFF reads something like SN not available
        addLeftLabel("Serial Number:", c);
//Testing to locate the cause of the SN error.
//        add(new SerialNumberMonitorPointWidget(logger, devicePM
//                .getMonitorPointPM(DGCKMonitorPoints.MODULE_CODES_SERIAL)), c);
        add(new IntegerMonitorPointWidget(logger, devicePM
                .getMonitorPointPM(IcdMonitorPoints.MODULE_CODES_SERIAL)), c);
        c.gridy++;
        addLeftLabel("Version:", c);
        add(new VersionMonitorPointWidget(logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.MODULE_CODES_VERSION_MAJOR),
                devicePM.getMonitorPointPM(IcdMonitorPoints.MODULE_CODES_VERSION_MINOR),
                devicePM.getMonitorPointPM(IcdMonitorPoints.MODULE_CODES_CDAY),
                devicePM.getMonitorPointPM(IcdMonitorPoints.MODULE_CODES_CMONTH),
                devicePM.getMonitorPointPM(IcdMonitorPoints.MODULE_CODES_YEAR)), c);
        c.gridy++;
        addLeftLabel("LRU CIN:", c);
        add(new LruMonitorPointWidget(logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.MODULE_CODES_DIG1),
                devicePM.getMonitorPointPM(IcdMonitorPoints.MODULE_CODES_DIG2),
                devicePM.getMonitorPointPM(IcdMonitorPoints.MODULE_CODES_DIG4),
                devicePM.getMonitorPointPM(IcdMonitorPoints.MODULE_CODES_DIG6)), c);


        setVisible(true);
    }
}

//
// O_o

