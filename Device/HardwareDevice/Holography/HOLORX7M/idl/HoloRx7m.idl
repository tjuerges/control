// @(#) $Id$
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2006
// Copyright by AUI (in the framework of the ALMA collaboration),
// All rights reserved.
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY, without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston,
// MA 02111-1307  USA
//

// Copyright (c) 2011 National Astronomical Observatory of Japan

#ifndef HOLORX7M_IDL
#define HOLORX7M_IDL

#include <HardwareDevice.idl>
#include <ControlInterfaces.idl>
#include <HolographyExceptions.idl>
#include <EthernetDeviceExceptions.idl>

#pragma prefix "alma"

module Control
{

  interface HoloRx7m : Control::HardwareDevice
  {

    struct HoloDeviceData {
      ACS::Time          startTime;
      ACS::TimeInterval  exposureDuration;
      HolographyDataSeq  holoData;
    };

    /// See HoloRx.py for a description of this function
    void startSubscan(in ACS::Time startTime)
      raises(ControlExceptions::DeviceBusyEx,
             ControlDeviceExceptions::HwLifecycleEx);

    void stopSubscan(in ACS::Time stopTime)
      raises(HolographyExceptions::NoSubscanEx,
             ControlDeviceExceptions::HwLifecycleEx);

    HoloDeviceData getSubscanData(in double waitTime)
      raises(HolographyExceptions::NoSubscanEx,
             HolographyExceptions::NoSubscanStopEx,
             HolographyExceptions::SubscanAbortedEx);

    void setHwClockOffset(in long clockOffsetInMsec);
    long getHwClockOffset();

    void connectRX()
      raises(ControlExceptions::HardwareErrorEx,
             EthernetDeviceExceptions::SocketOperationFailedEx);
    void disconnectRX()
      raises(ControlExceptions::HardwareErrorEx,
             EthernetDeviceExceptions::SocketOperationFailedEx);

    void checkHwClock()
      raises(ControlExceptions::HardwareErrorEx,
             EthernetDeviceExceptions::SocketOperationFailedEx);

  };
};

#endif // HOLORX7M_IDL
