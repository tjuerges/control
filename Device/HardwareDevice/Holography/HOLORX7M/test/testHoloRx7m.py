#!/usr/bin/env python

import unittest
import os

from Acspy.Clients.SimpleClient import PySimpleClient

#import CCL.HoloRx7m

class HoloRx7mTestCase( unittest.TestCase ):

    def setUp(self):
        self.componentName = "Test/HoloRx7m"
        self.simpleClient = PySimpleClient([])
        try:
            self.testObj = self.simpleClient.getComponent(self.componentName)
        except Exception, e:
            raise Exception, 'Error: Unable to get component '+ self.componentName + ', ' +str(e)

    def tearDown(self):
        self.simpleClient.releaseComponent(self.componentName)

    def testUnitTestMain(self):
        try:
            self.testObj.connectRX()
            self.testObj.disconnectRX()
        except Exception, e:
            raise Exception, 'Error invoking unitTestMain(): '+str(e)

    def reportPrint(self):
        pass

if __name__ == '__main__':

    unittest.main()

