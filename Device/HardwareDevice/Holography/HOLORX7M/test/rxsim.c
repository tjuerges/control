/*
 * 7m Holography Receiver simulator
 */
// Copyright (c) 2011 National Astronomical Observatory of Japan
// All rights reserved.

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <memory.h>
#include <time.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/signal.h>

#define __USE_GNU
#include <sys/poll.h>

/**/

#define PORT_C 10023
#define PORT_D 10024

#define NL "\r\n"

const char welcome_message[] =
    NL
    "Holo7m Rx simulator" NL
    "welcome!" NL
    NL;

/**/


/**/

int sock_d;

int cor = 18;
int tim = 0;
int tim_inited = 0;
int tim_epoch;

volatile int go;
volatile int go_set;
volatile int cor_set;

volatile int data_run;

/*------------------------------------------------------------*/

struct itimerval itv0;
struct timeval tv0;
int numdata;

void it_start()
{
    puts("itimer_start");

    struct itimerval itv;
    itv.it_interval.tv_sec = 0;
    itv.it_interval.tv_usec = cor * 1000;
    itv.it_value = itv.it_interval;
    setitimer(ITIMER_REAL, &itv, 0);
}
void it_stop()
{
    puts("itimer_stop");

    struct itimerval itv;
    itv.it_interval.tv_sec = 0;
    itv.it_interval.tv_usec = 0;
    itv.it_value = itv.it_interval;
    setitimer(ITIMER_REAL, &itv, 0);
}
void send_data()
{
    struct timeval tv;
    struct tm *tm;
    char data[200];
    int r;

    tv = tv0;
    unsigned int msec = numdata++ * cor;
    tv.tv_usec += (msec % 1000) * 1000;
    tv.tv_sec  += (msec / 1000);
    if (tv.tv_usec > 1000000) {
        tv.tv_usec -= 1000000;
        tv.tv_sec++;
    }

    tm = gmtime(&tv.tv_sec);

    strftime(data, sizeof data, "%H%M%S", tm);
    sprintf(data, "%s%03d %s"
        , data, (int)tv.tv_usec/1000, 
        "-1.234872e+02 +1.019394e-01 +1.006089e-01 -1.234872e+02 +1.019394e-01 +1.006089e-01");
    strcat(data, NL);

    r = send(sock_d, data, strlen(data), MSG_NOSIGNAL|MSG_DONTWAIT);
    if (r < 0) {
        perror("dat:write");
        if (errno == EPIPE) {
            puts("dat: peer closed.");
        }
        data_run = 0;
        it_stop();
    }
}

void handler(int sig)
{
    if (go) {
        send_data();
    }
}

int data_loop(int s)
{
    int r;
    struct pollfd fds;

    gettimeofday(&tv0, 0);
    numdata = 0;

    signal(SIGALRM, handler);
    sock_d = s;

    data_run = 1;
    while (data_run) {
        if (go_set || cor_set) {
            if (go)
                it_start();
            else
                it_stop();
            go_set = 0;
            cor_set = 0;
        }
        if (! go) {
            fds.fd = s;
            fds.events = POLLHUP|POLLRDHUP;
            fds.revents = 0;
            r = poll(&fds, 1, 0);
            if (r > 0 && fds.revents) {
                printf("dat:poll -> ");
                if ((fds.revents & (POLLHUP|POLLRDHUP)))
                    printf("POLLHUP|POLLRDHUP");
                else if ((fds.revents & POLLHUP))
                    printf("POLLHUP");
                else if ((fds.revents & POLLRDHUP))
                    printf("POLLRDHUP");
                printf(" %04x\n", fds.revents);
                printf("dat_loop: exit\n");
                data_run = 0;
                break;
            }
        }
        usleep(500000);
    }
    it_stop();
    return 1;
}

void data_port_thread()
{
    int s, ss;
    int r, runrun = 1;
    struct sockaddr_in sin, peer;
    socklen_t peer_len = sizeof peer;
    int on = 1;

    ss = socket(AF_INET, SOCK_STREAM, 0);
    if (ss < 0) {
        perror("dat:socket");
        return;
    }
    r = setsockopt(ss, SOL_SOCKET, SO_REUSEADDR, &on, sizeof on);
    if (r < 0) {
        perror("dat:setsockopt");
        return;
    }
    memset(&sin, 0, sizeof sin);
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = htonl(INADDR_ANY);
    sin.sin_port = htons(PORT_D);
    r = bind(ss, (struct sockaddr *)&sin, sizeof sin);
    if (r < 0) {
        perror("dat:bind");
        return;
    }
    r = listen(ss, 0);
    if (r < 0) {
        perror("dat:listen");
        return;
    }
    while (runrun) {
        printf("dat:waiting(ss)\n");

        while (1) {
            struct pollfd fds;

            if (go)
                goto out;

            fds.fd = ss;
            fds.events = POLLIN;
            fds.revents = 0;
            r = poll(&fds, 1, 500);
            if (r < 0) {
                perror("dat:poll for accept failed.");
                return;
            }
            if (r > 0 && fds.revents) {
                if ((fds.revents & POLLIN)) {
                    printf("dat:poll -> POLLIN\n");
                } else {
                    printf("dat:poll -> %04x\n", fds.revents);
                }
                break;
            }
        }
        s = accept(ss, (struct sockaddr *)&peer, &peer_len);
        if (s < 0) {
            perror("dat:accept");
            continue;
        }
        printf("dat:connected!\n");

        runrun = data_loop(s);

        usleep(1000000);
        shutdown(s, SHUT_RDWR);
        close(s);
    }

out:
    close(ss);
    printf("dat:closed(ss)\n");
}

/*------------------------------------------------------------*/

int recv_cmd(int s, char *buf, int maxlen, int timout_sec)
{
    int r;
    char *p = buf;
    char c;

    while ((p - buf < maxlen)) {
        r = read(s, &c, 1);
        if (r < 0) {        // error
            perror("cmd:read");
            return -1;
        }
        if (r == 0) {       // socket is closed
            puts("cmd: peer closed.");
            return 0;
        }

        switch (c) {
        case '\r':
            break;
        case '\0':
        case '\n':
            *p++ = '\0';
            return p - buf;
        default:
            *p++ = c;
        }
    }
    return maxlen;
}
int send_resp(int s, const char *buf)
{
    int r;
    const char *p = buf;

    while (*p) {
        if (*p == '\n')
            write(s, "\r", 1);
        r = write(s, p, 1);
        if (r < 0) {
            return -1;
        }
        p++;
    }
    return p - buf;
}

#define isnum(c) ('0' <= (c) && (c) <= '9')
int parse_tim_cmd(const char *cmd, struct tm *tm)
{
    int i;
    const char *p = &cmd[6];

    memset(tm, 0, sizeof *tm);

    // "TIM = 20101112003000,0"
    for (i = 0; i < 14; i++) {
        if (! isnum(p[i]))
            return -1;
    }
    if (p[i] != ',' || ! isnum(p[i+1]))
        return -1;

#define nc(i) (p[i] - '0')
#define nn2(i) ((p[i] - '0') * 10 + (p[i+1] - '0'))
    tm->tm_year = (nc(0)*1000 + nc(1)*100 + nc(2)*10 + nc(3)) - 1900;
    tm->tm_mon  = nn2(4) - 1;
    tm->tm_mday = nn2(6);
    tm->tm_hour = nn2(8);
    tm->tm_min  = nn2(10);
    tm->tm_sec  = nn2(12);
#undef nn2
#undef nc
    return 0;
}
int cmd_loop(int s)
{
    int r;
    char cmd[40], rsp[40], resp[100];


    while (1) {
        r = recv_cmd(s, cmd, sizeof cmd-1, 10);
        if (r <= 0) {
            break;
        }
        cmd[r] = '\0';

        if (strcmp(cmd, "end") == 0)
            return 0;

        if (strcmp(cmd, "COR ?") == 0) {
            // puts(cmd);
            sprintf(rsp, "COR = 0,%d", cor);
        } else if (strncmp(cmd, "COR = ", 6) == 0) {
            // puts(cmd);
            int t = atoi(&cmd[6]);
            if (t == 12 || t == 24) {
                if (t != cor) {
                    cor = t;
                    cor_set = 1;
                }
                strcpy(rsp, "COR = 0");
            } else {
                strcpy(rsp, "COR = 1");
            }
        } else if (strcmp(cmd, "TIM ?") == 0) {
            // puts(cmd);
            //if (0) {
            //    strcpy(rsp, "TIM = 5");
            //    usleep(3000000);    // TIM=5 is slow
            //}
            if (! tim_inited) {
                strcpy(rsp, "TIM = 3");
            } else {
                time_t cur = time(0);
                struct tm *tm;

                cur -= tim_epoch;
                tm = gmtime(&cur);
                strftime(rsp, sizeof rsp, "TIM = 0,%Y/%m/%d,%H:%M:%S", tm);
            }
        } else if (strncmp(cmd, "TIM = ", 6) == 0) {
            // puts(cmd);
            struct tm tm;
            r = parse_tim_cmd(cmd, &tm);
            if (r < 0) {
                // XXX bad format
                strcpy(rsp, "TIM = 1");
            } else {
                tim_epoch = time(0) - mktime(&tm);
                printf("%ld %ld %s\n", time(0), mktime(&tm), asctime(&tm));
                strcpy(rsp, "TIM = 0");
                tim_inited = 1;
            }
        } else if (strcmp(cmd, "GO") == 0) {
            // puts(cmd);
            if (! tim_inited) {
                strcpy(rsp, "GO = 3");
            } else {
                // XXX already started ?
                strcpy(rsp, "GO = 0");
                go = 1;
                go_set = 1;
            }
        } else if (strcmp(cmd, "PAUSE") == 0) {
            // puts(cmd);
            // XXX already paused ?
            // XXX time not set ?
            strcpy(rsp, "PAUSE = 0");
            go = 0;
            go_set = 1;
        } else {
            // XXX unknown command
            printf("'%s' unknown command\n", cmd);
            strcpy(rsp, "unknown command");
        }

        printf("'%s' => '%s'\n", cmd, rsp);
        sprintf(resp, "%s\n%s\n\n", cmd, rsp);
        r = send_resp(s, resp);
        if (r < 0)
            break;
    }
    puts("cmd_loop: exit");
    return 1;
}

void cmd_port_thread()
{
    int s, ss;
    int r, runrun = 1;
    struct sockaddr_in sin, peer;
    socklen_t peer_len = sizeof peer;
    int on = 1;

    ss = socket(AF_INET, SOCK_STREAM, 0);
    if (ss < 0) {
        perror("cmd:socket");
        exit(1);
    }
    r = setsockopt(ss, SOL_SOCKET, SO_REUSEADDR, &on, sizeof on);
    if (r < 0) {
        perror("cmd:setsockopt");
        exit(1);
    }
    memset(&sin, 0, sizeof sin);
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = htonl(INADDR_ANY);
    sin.sin_port = htons(PORT_C);
    r = bind(ss, (struct sockaddr *)&sin, sizeof sin);
    if (r < 0) {
        perror("cmd:bind");
        exit(1);
    }
    r = listen(ss, 0);
    if (r < 0) {
        perror("cmd:listen");
        exit(1);
    }
    while (runrun) {
        printf("cmd:waiting(ss)\n");

        s = accept(ss, (struct sockaddr *)&peer, &peer_len);
        if (s < 0) {
            perror("cmd:accept");
            continue;
        }
        printf("cmd:connected!\n");

        // welcome message
        write(s, welcome_message, sizeof welcome_message);

        runrun = cmd_loop(s);

        usleep(1000000);
        shutdown(s, SHUT_RDWR);
        close(s);
    }

    go = 0;

    close(ss);
    printf("cmd:closed(ss)\n");
}

/*------------------------------------------------------------*/

void *t_cmd(void *arg)
{
    cmd_port_thread();
    pthread_exit(0);
}
void *t_data(void *arg)
{
    data_port_thread();
    pthread_exit(0);
}

int main()
{
    int r;
    pthread_t thread_c, thread_d;

    unsetenv("TZ");

    r = pthread_create(&thread_c, NULL, t_cmd, 0);
    if (r) {
        perror("pthread_create(thr_cmd)");
        exit(1);
    }
    r = pthread_create(&thread_d, NULL, t_data, 0);
    if (r) {
        perror("pthread_create(thr_data)");
        exit(1);
    }

    r = pthread_join(thread_c, 0);
    if (r) {
        perror("pthread_join(thr_cmd)");
        exit(1);
    }
    r = pthread_join(thread_d, 0);
    if (r) {
        perror("pthread_join(thr_data)");
        exit(1);
    }

    exit(0);
}

/*------------------------------------------------------------*/
