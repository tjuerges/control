//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2006, 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

// Copyright (c) 2011 National Astronomical Observatory of Japan

static char rcsId[] = "@(#) $Id$";
static void *use_rcsId = ((void)&use_rcsId, (void *)&rcsId);

#include <HoloRx7mImpl.h>
#include <HolographyExceptions.h>
#include <EthernetDeviceExceptions.h>
#include <acstimeDurationHelper.h> // for DurationHelper
#include <acstimeEpochHelper.h> // for EpochHelper
#include <LogToAudience.h> // for AUTO_TRACE & LOG_TO_AUDIENCE
#include <TETimeUtil.h> // for TETimeUtil
#include <Time_Value.h> // for ACE_Time_Value
#include <math.h> // for MAXFLOAT
#include <hardwareDeviceImpl.h>

// For parsing XML
#include <configDataAccess.h>
#include <controlXmlParser.h>

#include <string.h>
#include <time.h>

#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>

using Control::HoloRx7mImpl;
using std::string;
using std::ostringstream;
using std::vector;
using log_audience::DEVELOPER;
using log_audience::OPERATOR;
using ACS::Time;
using ACS::TimeInterval;

//using TETimeUtil::ceilTE;
//using TETimeUtil::toTimeString;

using ControlDeviceExceptions::HwLifecycleExImpl;
using ControlExceptions::HardwareErrorExImpl;
using ControlExceptions::DeviceBusyExImpl;
using HolographyExceptions::NoSubscanExImpl;
using HolographyExceptions::SubscanAbortedExImpl;
using HolographyExceptions::NoSubscanStopExImpl;
using EthernetDeviceExceptions::SocketOperationFailedExImpl;


#define DUMMY_MAC_ADDRESS "00:00:00:00:00:00"

#define MYLOG(X)  ACS_LOG(LM_FULL_INFO, __func__, X)

#define CRLF "\r\n"

#define DELIM " \t\r\n"

#define LEN_RAWDATA 95

// XXX should use standard conversion functions.
#define msec2acstime(t) (static_cast<int64_t>(t) * 10000)
#define acstime2msec(t) ((t) / 10000)
#define sec2acstime(t)  (static_cast<int64_t>(t) * 10000000)

static const int loopTime_msec = 250;
static const TimeInterval loopTime = msec2acstime(loopTime_msec);


//---------------------------------------------------------------------
static inline int isnum(int c)
{
    return '0' <= c && c <= '9';
}
static string mystrerror(int e)
{
    char errmsg[80];
    const char *ep = strerror_r(e, errmsg, sizeof errmsg);
    ostringstream msg;
    msg << " errno=" << e << "/'" << ep << "'";
    return msg.str();
}
static void semdebug(sem_t *sem, const char *str)
{
    char buf[256];
    const unsigned char *p = (const unsigned char *) sem;

    sprintf(buf, "%02x %02x %02x %02x  %02x %02x %02x %02x  "
                 "%02x %02x %02x %02x  %02x %02x %02x %02x"
        , p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7]
        , p[8], p[9], p[10], p[11], p[12], p[13], p[14], p[15]
        );
    MYLOG((LM_INFO, "%s: %s", str, buf));
}

//---------------------------------------------------------------------
HoloRx7mImpl::HoloRx7mImpl(const ACE_CString & name,
			 maci::ContainerServices* cs):
    HardwareDeviceImpl(name, cs),
    AmbCDBAccessor(cs),

    scanAborted_m(false),
    startTime_m(0),
    stopTime_m(0),

    rawdataSetNum_m(2),
    cor_m(12),
    sockC_m(-1), sockD_m(-1),

    hwClockOffset_m(0),

    dataThread_m(NULL),
    indexInSubscan_m(-1)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    isRXconnected = false;
    hasGOcmdIssued = false;
    isSSrunning = false;

    int r = sem_init(&scanEnded_m, 0, 0);
    if (r < 0) {
        MYLOG((LM_ERROR, "sem_init() -> %s", mystrerror(errno).c_str()));
    }
}

HoloRx7mImpl::~HoloRx7mImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    cleanUp();

    sem_destroy(&scanEnded_m);
}

void HoloRx7mImpl::getDeviceUniqueId(string& deviceID)
{
    try {
        std::auto_ptr<const ControlXmlParser> ethConfig(
            ControlXmlParser::getElement("HoloRx7m_EthernetConfig"));
        deviceID = ethConfig->getStringAttribute("macAddress");
        if (macAddress_m != "" && macAddress_m != DUMMY_MAC_ADDRESS) {
            deviceID = macAddress_m;
            return;
        }
    } catch(const ControlExceptions::XmlParserErrorExImpl& ex) {
    }
    deviceID = "00:00:00:00:00:00";
    const string fnName(__PRETTY_FUNCTION__);
    const string error(": Unable to read deviceID from the CDB");
    setError(fnName + error);
}

//---------------------------------------------------------------------

void HoloRx7mImpl::initialize()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try {
        MYLOG((LM_INFO, "configs read from CDB::"));

        std::auto_ptr<const ControlXmlParser> ethConfig(
            ControlXmlParser::getElement("HoloRx7m_EthernetConfig"));

        hostname_m = ethConfig->getStringAttribute("hostname");
        macAddress_m = ethConfig->getStringAttribute("macAddress");
        portC_m = ethConfig->getLongAttribute("port");
        portD_m = ethConfig->getLongAttribute("data_port");
        timeoutRxC_m = ethConfig->getDoubleAttribute("timeoutRx_C");
        timeoutRxD_m = ethConfig->getDoubleAttribute("timeoutRx_D");
        lingerTime_m = ethConfig->getLongAttribute("lingerTime");
        retries_m = ethConfig->getLongAttribute("retries");
        earlyConnect_m = ethConfig->getBoolAttribute("earlyConnect");

        MYLOG((LM_INFO,
            "hostname=%s macAddress=%s "
            "port=%d,%d "
            "timeoutRx=%.3f,%.3f "
            "lingerTime=%d retries=%d "
                , hostname_m.c_str()
                , macAddress_m.c_str()
                , portC_m, portD_m
                , timeoutRxC_m , timeoutRxD_m
                , lingerTime_m, retries_m
                ));

        std::auto_ptr<const ControlXmlParser> rxConfig(
            ControlXmlParser::getElement("HoloRx7m_ReceiverConfig"));

        cor_m = rxConfig->getLongAttribute("cor");
        rawdataSetNum_m = rxConfig->getLongAttribute("dataset"); // 1 or 2
        hwClockOffset_m = rxConfig->getLongAttribute("hwClockOffset");

        MYLOG((LM_INFO,
            "cor=%d(msec) dataset=%d hwClockOffset=%d(msec)"
                , cor_m, rawdataSetNum_m
                , hwClockOffset_m
                ));

        std::auto_ptr<const ControlXmlParser> logConfig(
            ControlXmlParser::getElement("HoloRx7m_LogConfig"));

        logEnable_m = logConfig->getBoolAttribute("log_enable");
        logDir_m  = logConfig->getStringAttribute("directory");
        logFile_m = logConfig->getStringAttribute("filename");

        MYLOG((LM_INFO,
            "log=%s dir=%s file=%s"
                , logEnable_m ? "true" : "false"
                , logDir_m.c_str()
                , logFile_m.c_str()
                ));

    } catch(const ControlExceptions::CDBErrorExImpl& ex) {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getLifeCycleEx();
    } catch(const ControlExceptions::XmlParserErrorExImpl& ex) {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getLifeCycleEx();
    }

    // Create the thread. It starts suspended.
    const ACE_CString compName = name();

    dataThread_m = getContainerServices()->getThreadManager()->
        create<DataThread, HoloRx7mImpl>(compName+"DataThread", *this,
                                          loopTime, loopTime);

    HardwareDeviceImpl::initialize();
}

void HoloRx7mImpl::cleanUp()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    HardwareDeviceImpl::cleanUp();

    if (dataThread_m) {
        dataThread_m->terminate();
    }
}

//---------------------------------------------------------------------

void HoloRx7mImpl::hwStartAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if (earlyConnect_m) {
        try {
            connectRX();
        } catch (...) {
            setError("Holography Receiver connect failed.");
        }
    }
}

void HoloRx7mImpl::hwStopAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try {
        disconnectRX();
    } catch (...) {
    }
}

void HoloRx7mImpl::hwConfigureAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}
void HoloRx7mImpl::hwInitializeAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}
void HoloRx7mImpl::hwOperationalAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

//---------------------------------------------------------------------
void HoloRx7mImpl::connRX()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    struct addrinfo hint;
    memset(&hint, 0, sizeof hint);
    hint.ai_family = AF_INET;
    hint.ai_socktype = SOCK_STREAM;
    hint.ai_protocol = IPPROTO_TCP;

    struct addrinfo *addr;
    int err = getaddrinfo(hostname_m.c_str(), 0, &hint, &addr);
    if (err) {
        ostringstream msg;
        msg << "getaddrinfo('" << hostname_m << "') failed. ";
        msg << "error is " << err << "/'" << gai_strerror(err) << "'";

        if (err == EAI_SYSTEM) {
            msg << mystrerror(errno);
        }

        LOG_TO_OPERATOR(LM_ERROR, msg.str());

        SocketOperationFailedExImpl nex(
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", msg.str());
        nex.log();
        throw nex;
    }

    struct sockaddr_in sin_c, sin_d;
    memcpy(&sin_c, addr->ai_addr, sizeof sin_c);
    memcpy(&sin_d, addr->ai_addr, sizeof sin_d);
    freeaddrinfo(addr);

    ostringstream msg;
    msg << "start trying to connect to Holography Receiver. ";
    msg << "(host=" << hostname_m << ", port=" << portC_m << "," << portD_m <<")";
    LOG_TO_OPERATOR(LM_INFO, msg.str());

    msg.str("");

    try {
        int s = socket(AF_INET, SOCK_STREAM, 0);
        if (s < 0) {
            msg << "socket() call (cmd port) failed.";
            throw 0;
        }
        sin_c.sin_port = htons(portC_m);
        int ret = connect(s, (const struct sockaddr *)&sin_c, sizeof sin_c);
        if (ret < 0) {
            msg << "connect() to '" << hostname_m << ":" <<portC_m << "' (cmd port) failed.";
            throw 0;
        }
        sockC_m = s;
        if (timeoutRxC_m != -1) {
            struct timeval tv;
            tv.tv_sec = static_cast<int>(timeoutRxC_m);
            tv.tv_usec = static_cast<int>(timeoutRxC_m * 1e6) % 1000000;
            ret = setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof tv);
            if (ret < 0) {
                msg << "setsockopt(SO_RCVTIMEO) (cmd port) failed.";
                throw 0;
            }
        }

        usleep(2000000);
        ret = send(sockC_m, CRLF, strlen(CRLF), 0);
        usleep(1000000);

        // read and log 'welcome message' (+ "ERR = 999")
        char buf_c[512];
        ret = recv(sockC_m, buf_c, sizeof buf_c-1, 0);
        if (ret > 0) {
            buf_c[ret] = '\0';
            for (char *p = buf_c; *p; p++) {
                if (*p < 0x20 && *p != '\n' || 0x7f <= *p)
                    *p = '.';
            }
            MYLOG((LM_INFO, "welcome message::\n%s", buf_c));
        } else if (ret <= 0) {
            msg << "error reading welcome message (cmd port).";
            if (ret < 0) {
                msg << mystrerror(errno);
            } else {
                msg << " peer requested shutdown.";
            }
            throw 0;
        }

        s = socket(AF_INET, SOCK_STREAM, 0);
        if (s < 0) {
            msg << "socket() call (data port) failed.";
            throw 0;
        }
        sin_d.sin_port = htons(portD_m);
        ret = connect(s, (const struct sockaddr *)&sin_d, sizeof sin_d);
        if (ret < 0) {
            msg << "connect() to '" << hostname_m << ":" <<portD_m << "' (data port) failed.";
            throw 0;
        }
        sockD_m = s;
        if (timeoutRxD_m != -1) {
            struct timeval tv;
            tv.tv_sec = static_cast<int>(timeoutRxD_m);
            tv.tv_usec = static_cast<int>(timeoutRxD_m * 1e6) % 1000000;
            ret = setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof tv);
            if (ret < 0) {
                msg << "setsockopt(SO_RCVTIMEO) (data port) failed.";
                throw 0;
            }
        }

    } catch (int ex) {

        if (sockC_m >= 0) {
            close(sockC_m);
            sockC_m = -1;
        }
        if (sockD_m >= 0) {
            close(sockD_m);
            sockD_m = -1;
        }
        msg << mystrerror(errno);

        LOG_TO_OPERATOR(LM_ERROR, msg.str());

        SocketOperationFailedExImpl nex(
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", msg.str());
        nex.log();
        throw nex;
    }
}
void HoloRx7mImpl::connectRX()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try {
        ostringstream msg;
        if (isRXconnected) {
            msg << "Holography Receiver already connected. ";
            LOG_TO_OPERATOR(LM_INFO, msg.str());
        } else {
            connRX();
            isRXconnected = true;

            msg << "Holography Receiver connected. ";
            msg << "(fd=" << sockC_m << "," << sockD_m <<")";
            LOG_TO_OPERATOR(LM_INFO, msg.str());
        }
    } catch (...) {
        ostringstream msg;
        msg << "Holography Receiver connect failed. ";
        LOG_TO_OPERATOR(LM_ERROR, msg.str());
        throw;
    }
    try {
        init_RXclock();
        init_COR();
    } catch (...) {
        ostringstream msg;
        msg << "Holography Receiver initialize (TIM, COR) failed. ";
        LOG_TO_OPERATOR(LM_ERROR, msg.str());
        disconnectRX();
        throw;
    }
    try {
        dataport_go();
    } catch (...) {
        ostringstream msg;
        msg << "Holography Receiver initialize (GO) failed. ";
        LOG_TO_OPERATOR(LM_ERROR, msg.str());
        disconnectRX();
        throw;
    }
}
void HoloRx7mImpl::disconnRX()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    {
        if (sockD_m >= 0) {
            usleep(500000);
            shutdown(sockD_m, SHUT_RDWR);
            usleep(500000);
            close(sockD_m);
            sockD_m = -1;
        }
        if (sockC_m >= 0) {
            shutdown(sockC_m, SHUT_RDWR);
            usleep(500000);
            close(sockC_m);
            sockC_m = -1;
        }
    }
}
void HoloRx7mImpl::disconnectRX()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    ostringstream msg;

    {
        ACS::ThreadSyncGuard guard(&scanStateMutex_m);
        if (isSSrunning) {
            isSSrunning = false;
            scanAborted_m = true;
            msgAborted_m = "abort by disconnectRX().";
            msg << "disconnect is requested while subscan is running.";
            LOG_TO_OPERATOR(LM_WARNING, msg.str());
            sem_post(&scanEnded_m);
        }
    }

    {
        if (! isRXconnected) {
            msg << "Holography Receiver is not connected. ";
            LOG_TO_OPERATOR(LM_INFO, msg.str());
        } else {
            dataport_pause();
            disconnRX();
            isRXconnected = false;

            msg << "Holography Receiver disconnected. ";
            LOG_TO_OPERATOR(LM_INFO, msg.str());
        }
    }
}
//--------------------
static string recv_resp(int fd)
{
    char buf[256];
    int nc = 0;

    for (int found = 0; found < 2; ) {
        // to be tuned.
        char c;
        int ret = recv(fd, &c, 1, 0);
        if (ret <= 0) {
            ostringstream msg;
            if (ret < 0) {
                if (errno == EAGAIN) {
                    msg << "timeout receiving cmd response.";
                } else {
                    msg << "error receiving cmd response.";
                    msg << mystrerror(errno);
                }
            } else {
                msg << "error receiving cmd response.";
                msg << " peer requested shutdown.";
            }
            SocketOperationFailedExImpl nex(
                    __FILE__, __LINE__, __PRETTY_FUNCTION__);
            nex.addData("Detail", msg.str());
            nex.log();
            throw nex;
        }

        if (c == '\r')
            continue;
        buf[nc++] = c;

        switch (found) {
        case 0:
            if (c == '\n')
                found++;
            break;
        case 1:
            if (c == '\n')
                found++;
            else
                found = 0;
            break;
        default:
            if (c == '\n')
                found = 1;
            break;
        }
        if (nc >= (int) sizeof buf) {
            // message
            break;
        }
    }
    buf[nc] = '\0';

    string s(buf);

    return s;
}
string HoloRx7mImpl::do_cmd(const char *cmd)
{
    int ret;
    char buf[256];
    ostringstream msg;

    try {
        // clear rx buffer first (should contain prompt.)
        while ((ret = recv(sockC_m, &buf[0], sizeof buf - 1, MSG_DONTWAIT)) > 0) {
            buf[ret] = '\0';
            if (ret == 2 && strcmp(buf, "> ") == 0) {
                // discard prompt
                MYLOG((LM_DEBUG, "prompt'%s'", buf));
            } else {
                MYLOG((LM_WARNING, "unexpected data in rx buffer::\n%s", buf));
            }
        }
        if (ret == 0 || errno != EAGAIN) {
            msg << "error clearing rx buffer.";
            if (ret == 0)
                msg << " peer requested shutdown.";
            else
                msg << mystrerror(errno);
            throw 0;
        }

        strcpy(buf, cmd);
        strcat(buf, CRLF);

        ret = send(sockC_m, buf, strlen(buf), 0);
        if (ret < 0) {
            msg << "error sending command '" << cmd << "'.";
            msg << mystrerror(errno);
            throw 0;
        }

        string resp = recv_resp(sockC_m);

        int len = strlen(cmd);
        if (cmd == resp.substr(0, len) && resp[len] == '\n') {
            string r = resp.substr(len+1);
            len = r.length();
            if (r.substr(len-2) == "\n\n") {
                r.resize(len-2);
            }
            MYLOG((LM_DEBUG, "cmd'%s' resp'%s'", cmd, r.c_str()));
            return r;
        } else {
            msg << "command handshake failed. ";
            msg << " cmd'" << cmd << "' resp'" << resp << "'";
            HardwareErrorExImpl nex(
                __FILE__, __LINE__, __PRETTY_FUNCTION__);
            nex.addData("Detail", msg.str());
            nex.log();
            throw nex.getHardwareErrorEx();
        }

    } catch (int ex) {
        SocketOperationFailedExImpl nex(
                __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", msg.str());
        nex.log();
        throw nex;
    }
}
//---------------------------------------------------------------------
void HoloRx7mImpl::dataport_go()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ostringstream msg;

    if (! dataThread_m) {
        msg << "no dataThread. ";
        HardwareErrorExImpl nex(
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", msg.str());
        nex.log();
        throw nex.getHardwareErrorEx();
    }

    if (! hasGOcmdIssued) {
        rawio.nres = 0;
        rawdataLast_m.ts_raw = 0;
        rawdataLast_m.buf.clear();
        dataStallTimer_m = 0;

        dataThread_m->resume();
    }

    string r = do_cmd("GO");
    if (r == "GO = 0") {
        hasGOcmdIssued = true;
        MYLOG((LM_INFO, "'GO' - ok."));
    } else {
        msg << "'GO' - failed. resp'" << r << "'";
        HardwareErrorExImpl nex(
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", msg.str());
        nex.log();
        throw nex.getHardwareErrorEx();
    }
}
void HoloRx7mImpl::dataport_pause()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try {
        string r;
        r = do_cmd("PAUSE");
        if (r == "PAUSE = 0") {
            MYLOG((LM_INFO, "'PAUSE' - ok."));
            hasGOcmdIssued = false;
        } else {
            MYLOG((LM_ERROR, "'PAUSE' - failed.  resp'%s'"
                , r.c_str()));
        }
    } catch (...) {
        // exception has been logged by do_cmd().
        hasGOcmdIssued = false;
    }
    if (dataThread_m) {
        dataThread_m->suspend();

        // XXX should wait for dataThread to suspend ?
    }
}
//---------------------------------------------------------------------
// parse "TIM ?" response
static time_t parse_tim_q_resp(const string &resp, struct tm *p)
{
    struct tm tm;
    ostringstream msg;

    // "TIM = 0,2011/02/08,10:25:07"
    if (resp.substr(0, 8) == "TIM = 0,") {
        if (resp.length() == 27) {
            char c[20];
            strcpy(c, resp.substr(8).c_str());

            memset(&tm, 0, sizeof tm);

            // "2011/02/08,10:25:07"
            if (c[4] == '/' && c[7] == '/' && c[10] == ','
                    && c[13] == ':' && c[16] == ':') {
                c[4] = c[7] = c[10] = c[13] = c[16] = '\0';
                tm.tm_year = atoi(&c[0]) - 1900;
                tm.tm_mon  = atoi(&c[5]) - 1;
                tm.tm_mday = atoi(&c[8]);
                tm.tm_hour = atoi(&c[11]);
                tm.tm_min  = atoi(&c[14]);
                tm.tm_sec  = atoi(&c[17]);
                if (p)
                    *p = tm;
                return timegm(&tm);
            }
        }
        msg << "error parsing TIM command response. " << resp;
    } else if (resp == "TIM = 3") {
        msg << "receiver clock is not yet set. " << resp;
    } else {
        msg << "unexpected error. " << resp;
    }
    HardwareErrorExImpl nex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    nex.addData("Detail", msg.str());
    nex.log();
    throw nex.getHardwareErrorEx();
}
int HoloRx7mImpl::check_if_RXclock_is_ticking()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // Holography Receiver runs with external 5MHz clock signal
    // which is generated by an oscillator located at Receiver Cabin.
    // This signal is transmitted along 10? meter coaxial cable.
    //
    // Receiver clock stops ticking and data stream stalls,
    // if this clock signal stops.
    //
    // Checking this carefully since we often suffered from this 
    // clock/data stall situation.

    usleep(2000000); // 2sec

    string r = do_cmd("TIM ?");
    time_t t1 = parse_tim_q_resp(r, 0);

    usleep(2000000); // 2sec

    r = do_cmd("TIM ?");
    time_t t2 = parse_tim_q_resp(r, 0);

    if (t1 >= t2) {
        ostringstream msg;
        msg << "Receiver clock seems not ticking. "
            << "t1 = " << t1 << ", " << "t2 = " << t2;
        HardwareErrorExImpl nex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", msg.str());
        nex.log();
        throw nex.getHardwareErrorEx();
    }

    MYLOG((LM_INFO, "clock is ticking - ok"));
    return 0;
}
void HoloRx7mImpl::do_TIM()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    struct timeval tv;
    struct tm tm;
    char buf[80];
    string r;

    struct timeval tv0,tv1,tv2;
    int n0, n1;
    int slp = 0;

    n0 = n1 = 0;
    gettimeofday(&tv, 0);
    tv0 = tv;
    {
        const int T1 = 997000;
        const int T2 = 990000;
        const int NTRY = 3;

        while (n0++ < NTRY && tv.tv_usec < T1) {
            slp = T1 - tv.tv_usec;
            if (slp > 0)
                usleep(slp);
            gettimeofday(&tv, 0);
        }
        if (n0 >= NTRY) {
            slp = T2 - tv.tv_usec;
            if (slp > 0)
                usleep(slp);
            gettimeofday(&tv, 0);
        }
    }
    tv1 = tv;

    time_t sec = tv.tv_sec;
    while (sec == tv.tv_sec) {
        n1++;
        gettimeofday(&tv, 0);
    }
    tv2 = tv;
    sec = tv.tv_sec + 2;    // XXX add 2 seconds
    gmtime_r(&sec, &tm);
    strftime(buf, sizeof buf, "TIM = %Y%m%d%H%M%S,0", &tm);

    r = do_cmd(buf);
    gettimeofday(&tv, 0);

    if (r == "TIM = 0") {
        MYLOG((LM_INFO, "set receiver clock - ok. cmd'%s' at %d.%06d"
                    , buf, tv.tv_sec, tv.tv_usec));
        MYLOG((LM_INFO,
                "n0=%d n1=%d slp=%d\n"
                "tv0=%d.%06d\n"
                "tv1=%d.%06d\n"
                "tv2=%d.%06d\n"
                , n0, n1, slp
                , tv0.tv_sec, tv0.tv_usec
                , tv1.tv_sec, tv1.tv_usec
                , tv2.tv_sec, tv2.tv_usec));
        return;
    }

    ostringstream msg;
    msg << "set receiver clock - failed. cmd'" << buf << "' resp'" << r << "'";
    HardwareErrorExImpl nex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    nex.addData("Detail", msg.str());
    nex.log();
    throw nex.getHardwareErrorEx();
}
void HoloRx7mImpl::init_RXclock()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    char buf[40];
    struct tm tm;
    ostringstream msg;
    int time_set = 0;

    string r = do_cmd("TIM ?");
    if (r == "TIM = 3") {
        do_TIM();
        time_set = 1;

        r = do_cmd("TIM ?");
    } else {
        // XXX time already set - should check time error
    }
    parse_tim_q_resp(r, &tm);
    strftime(buf, sizeof buf, "%y-%m-%d %H:%M:%S", &tm);
    msg << "receiver time: " << buf;

    if (! time_set) {
        msg << " (time already set - not changed)";
    }

    LOG_TO_OPERATOR(LM_INFO, msg.str());

    check_if_RXclock_is_ticking();
}
void HoloRx7mImpl::checkHwClock()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if (! isRXconnected) {
        MYLOG((LM_INFO, "Holography Receiver is not connected."));
        return;
        // XXX throw something.
    }

    check_if_RXclock_is_ticking();
}
//----------
// parse "COR ?" response
static int parse_cor_q_resp(const string &resp)
{
    ostringstream msg;

    string ss = resp.substr(0, 8);
    if (ss == "COR = 0,") {
        int cor = atoi(resp.substr(8).c_str());
        return cor;
    } else {
        msg << "unexpected error. " << resp;
    }
    HardwareErrorExImpl nex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    nex.addData("Detail", msg.str());
    nex.log();
    throw nex.getHardwareErrorEx();
}
void HoloRx7mImpl::do_COR()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    char buf[80];
    string r;

    snprintf(buf, sizeof buf, "COR = %d", cor_m);

    r = do_cmd(buf);
    if (r == "COR = 0") {
        MYLOG((LM_INFO, "set cor - ok. cmd'%s'", buf));
        return;
    }

    ostringstream msg;
    msg << "set cor - failed. cmd'" << buf << "' resp'" << r << "'";
    HardwareErrorExImpl nex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    nex.addData("Detail", msg.str());
    nex.log();
    throw nex.getHardwareErrorEx();
}
void HoloRx7mImpl::init_COR()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ostringstream msg;

    string r = do_cmd("COR ?");
    int cor_prev = parse_cor_q_resp(r);
    if (cor_prev == cor_m) {
        msg << "COR is " << cor_prev << " msec."
            << " (cor already set - not changed)";
    } else {
        do_COR();

        r = do_cmd("COR ?");
        int cor = parse_cor_q_resp(r);
        msg << "COR is " << cor << " msec. (was " << cor_prev << " msec)";
    }

    LOG_TO_OPERATOR(LM_INFO, msg.str());
}
//---------------------------------------------------------------------
void HoloRx7mImpl::startSubscan(Time startTime)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if (! hasGOcmdIssued) {
        ostringstream msg;
        msg << "device is not ready";
        HwLifecycleExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getHwLifecycleEx();
    }

    // Round the start time to the next TE.
    Time teStartTime = TETimeUtil::ceilTE(startTime);
    if (teStartTime != startTime) {
        ostringstream msg;
        msg << "Adjusted the requested start time of ";
        msg << TETimeUtil::toTimeString(startTime) << " to ";
        msg << TETimeUtil::toTimeString(teStartTime)
            << " as it was not on a TE boundry.";
        LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), OPERATOR);
    }

    // Check that the start time is not in the past.
    Time timeNow(TETimeUtil::ceilTE(TimeUtil::ace2epoch(ACE_OS::gettimeofday())).value);
    if (timeNow >= teStartTime) {
        ostringstream msg;
        msg << " The requested start time of this sub-scan is "
            << TETimeUtil::toTimeString(teStartTime)
            << " and the current time is " << TETimeUtil::toTimeString(timeNow)
            << " This is in the past to the current time.";
        // TODO: This should probably be a different sort of exception.
        DeviceBusyExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getDeviceBusyEx();
    }

    ACS::ThreadSyncGuard guard(&scanStateMutex_m);

    // Now check if a scan has already been scheduled.
    if (isSSrunning) {
        ostringstream msg;
        msg << "A sub-scan has already been scheduled."
            << " It will start at " << TETimeUtil::toTimeString(startTime_m)
            << " and stop at "
            << ((stopTime_m == 0) ?
                "an unspecified time." : TETimeUtil::toTimeString(stopTime_m))
            << " Please wait for this sub-scan to complete"
            << " before scheduling a new one.";
        DeviceBusyExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getDeviceBusyEx();
    }

    data_m.clear();
    deviceData_m.clear();

    startTime_m = teStartTime;
    if ((stopTime_m != 0) && (startTime_m > stopTime_m)) {
        stopTime_m = 0;
    }

    MYLOG((LM_INFO, "startTime_m=%llu %s"
        , startTime_m
        , TETimeUtil::toTimeString(startTime_m).c_str()));

    scanAborted_m = false;
    msgAborted_m = "";
    isSSrunning = true;

    int r, n = 0;
    do {
        r = sem_trywait(&scanEnded_m);
        if (r == 0)
            n++;
    } while (r == 0);
    if (n > 0) {
        MYLOG((LM_DEBUG, "trywait count=%d", n));
    }
}

void HoloRx7mImpl::stopSubscan(Time stopTime)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if (! hasGOcmdIssued) {
        ostringstream msg;
        msg << "device is not ready";
        HwLifecycleExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getHwLifecycleEx();
    }

    Time teStopTime = TETimeUtil::ceilTE(stopTime);
    if (teStopTime != stopTime) {
        ostringstream msg;
        msg << "Adjusted the requested stop time of "
            << TETimeUtil::toTimeString(stopTime) << " to "
            << TETimeUtil::toTimeString(teStopTime)
            << " as it was not on a TE boundry.";
        LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), DEVELOPER);
    }

    // check that the stop time is greater than the start time
    if (teStopTime < startTime_m) {
        ostringstream msg;
        msg << "The requested sub-scan stop time of "
            << TETimeUtil::toTimeString(teStopTime)
            << " is less than the sub-scan start time of "
            << TETimeUtil::toTimeString(startTime_m);
        // TODO This should be a different sort of exception.
        NoSubscanExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getNoSubscanEx();
    }

    stopTime_m = teStopTime;

    MYLOG((LM_INFO, "stopTime_m=%llu %s"
        , stopTime_m
        , TETimeUtil::toTimeString(stopTime_m).c_str()));
}

Control::HoloRx7m::HoloDeviceData*
HoloRx7mImpl::getSubscanData(double timeout)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if (startTime_m == 0) {
        const string msg = "Subscan start time is not defined." ;
        NoSubscanExImpl ex(__FILE__,__LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getNoSubscanEx();
    }
    if (stopTime_m == 0) {
        const string msg = "Subscan stop time is not defined." ;
        NoSubscanStopExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.log();
        throw ex.getNoSubscanStopEx();
    }

    if (scanAborted_m) {
        SubscanAbortedExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", msgAborted_m);
        ex.log();
        throw ex.getSubscanAbortedEx();
    }

    // Wait for the ScanScheduled Semaphore to be unlocked.
    {
        if (timeout < 5.0)
            timeout = 5.0;
        ACE_Time_Value timeoutEndPoint;
        timeoutEndPoint.set(timeout);
        timeoutEndPoint += ACE_OS::gettimeofday();
        const timespec ts = timeoutEndPoint;

        MYLOG((LM_DEBUG,
                "waiting for scanEnded semaphore."
                " timeout = (%d.%09d) or %.3f seconds."
                , ts.tv_sec, ts.tv_nsec, timeout));

        semdebug(&scanEnded_m, "sem_timedwait()");

        int res = sem_timedwait(&scanEnded_m, &ts);

        ACS::ThreadSyncGuard guard(&scanStateMutex_m);

        if (res == -1) {
            if (isSSrunning) {
                // stop dataThread->dataCollector()
                isSSrunning = false;
                scanAborted_m = true;
                msgAborted_m = "abort by getSubscanData().";
            }

            ostringstream msg;
            if (errno == ETIMEDOUT) {
                msg << "Timed out waiting for the subscan to complete."
                    << " Timeout was set at " << timeout;
            } else {
                msg << "sem_timedwait() -> " << res << ", "
                    << mystrerror(errno);

                // 2011-04 tmatsui
                // if you see, sem_timedwait() -> -1, errno=ENOSYS,
                // make sure you are calling sem_init@GLIBC_2.1
                //   not @GLIBC_2.0.
                // Or if it happens intermittently, check scanEnded_m
                // is not broken.
            }
            SubscanAbortedExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
            ex.addData("Detail", msg.str());
            ex.log();
            throw ex.getSubscanAbortedEx();
        }

        MYLOG((LM_DEBUG, "Got the scanEnded semaphore."));
    }

    if (scanAborted_m) {
        // DataThread aborted this subscan for some reason.
        SubscanAbortedExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", msgAborted_m);
        ex.log();
        throw ex.getSubscanAbortedEx();
    }

    Time startTime = startTime_m;
    startTime_m = 0;
    stopTime_m = 0;

    MYLOG((LM_INFO, "got data_m[%d]", data_m.size()));

    // This is the data structure that will contain the returned data.
    Control::HoloRx7m::HoloDeviceData_var deviceData = 
        new Control::HoloRx7m::HoloDeviceData();

    deviceData->startTime = startTime;
    deviceData->exposureDuration = msec2acstime(cor_m);
    deviceData_m.resize(data_m.size() - 1);

    vector<DataStruct>::iterator it, it0;

    it0 = data_m.begin();
    it = it0 + 1;
    int gapHoloHwClockInt = acstime2msec(it->timestamp - startTime);

    if (logEnable_m) {
        saveRawData();
    }

    MYLOG((LM_INFO,
        "start of subscanData. startTime=%s, gap=%d ms [0..11]"
        , TETimeUtil::toTimeString(startTime).c_str()
        , gapHoloHwClockInt));

    if (gapHoloHwClockInt == cor_m) {
        gapHoloHwClockInt = 0;
        it = it0;
    }

    for (int idx = 0; it < data_m.end(); idx++, it0 = it++) {
        DataStruct d;

        interpolate(d, *it0, *it, gapHoloHwClockInt);
        if (isnan(d.Ph) || isnan(d.Ref) || isnan(d.Sig))
            d.flag = true;

        calculateSQR(deviceData_m[idx], d);
    }



    const int numData = deviceData_m.size();
    deviceData->holoData.replace(numData,numData,
                                 &deviceData_m[0], 0);

    MYLOG((LM_INFO,
        "returning HoloDeviceData to the caller. deviceData[%d]"
        , numData));
    return deviceData._retn();
}

void HoloRx7mImpl::interpolate(
        DataStruct &interpData,
        const DataStruct &prevData,
        const DataStruct &thisData,
        const int timeGap)
{
//   ----|---------------/\---------------------|-----
//     prevD           interp                 thisD
//                        |------timeGap--------|

    if (timeGap == 0) {
        interpData = thisData;
        return;
    }

    interpData.flag = (prevData.flag || thisData.flag) ? true : false;

    double DiffAB12;
    DiffAB12 = (prevData.Sig - thisData.Sig) / cor_m;
    interpData.Sig = thisData.Sig + (timeGap * DiffAB12);

    DiffAB12 = (prevData.Ref - thisData.Ref) / cor_m;
    interpData.Ref = thisData.Ref + (timeGap * DiffAB12);

    double phA, phB, phI;
    phA = prevData.Ph;
    phB = thisData.Ph;
    if (phA < 0.0 && 0.0 < phB && phB - phA > 180.0) {
        phA += 2 * 180.0;
    }
    if (phB < 0.0 && 0.0 < phA && phA - phB > 180.0) {
        phB += 2 * 180.0;
    }
    DiffAB12 = (phA - phB) / cor_m;
    phI = phB + ( timeGap * DiffAB12 );
    if( phI > 180.0 ) {
        phI -= 2 * 180.0;
    }
    interpData.Ph  = phI;
}
void HoloRx7mImpl::calculateSQR(
        Control::HolographyData &res,
        const DataStruct &d)
{
    res.flag = d.flag;

    double Ph = d.Ph / 180.0 * M_PI;
    double cosPh = cos(Ph);
    double sinPh = sin(Ph);
    double sigref = d.Sig * d.Ref;

    // 20090314 swap these for some reason.
    double tmp;
    tmp = cosPh;  cosPh = sinPh; sinPh = tmp;

    res.ss = d.Sig * d.Sig;

    res.rr = d.Ref * d.Ref;
    res.rs = sigref * cosPh;

    res.qs = d.Sig * d.Sig * sinPh * cosPh;
    res.qr = sigref * sinPh;

    tmp = d.Sig * sinPh;
    res.qq = tmp * tmp;
}
void HoloRx7mImpl::saveRawData()
{
    string path = logDir_m + "/" + logFile_m;
    ofstream ofs;

    ofs.open(path.c_str(), ios::app);
    if (! ofs) {
        MYLOG((LM_WARNING,
            "error saving rawdata file. '%s' - "
            , path.c_str()
            , mystrerror(errno).c_str()));
        return;
    }

    ofs << "#" << data_m.size() << "\n";

    vector<DataStruct>::iterator it;
    for (it = data_m.begin(); it < data_m.end(); it++) {
        ofs << it->rawbuf;
    }
    ofs.close();
}

//---------------------------------------------------------------------
void HoloRx7mImpl::set_acstimeUT0h(int32_t ts_msec)
{
    time_t cur = time(0);
    time_t t = cur / 86400 * 86400;
    int diff_sec = cur - t;
    if (diff_sec < 3600*4) {
        if (ts_msec > 86400/2 * 1000)
            t -= 86400;
    } else if (3600*20 < diff_sec) {
        if (ts_msec < 86400/2 * 1000)
            t += 86400;
    }
    Time prev = acstimeUT0h_m;
    acstimeUT0h_m = sec2acstime(t) + acstime::ACE_BEGIN;

    MYLOG((LM_INFO, "acstimeUT0h_m: %llu -> %llu (ts=%d msec)"
        , prev, acstimeUT0h_m, ts_msec));
}
int HoloRx7mImpl::parse_rawdata_timestamp(const string &line)
{
    char *q, *sav;

    char buf[256];
    strncpy(buf, line.c_str(), sizeof buf);
    buf[sizeof buf - 1] = '\0';

    if ((q = strtok_r(buf, DELIM, &sav)) == NULL)
        return -1;
    if (strlen(q) != 9)
        return -1;
    for (const char *p = q; *p; ) {
        if (! isnum(*p++))
            return -1;
    }

    int ts;
    ts  = ((q[0] - '0') * 10 + (q[1] - '0')) * 3600000;
    ts += ((q[2] - '0') * 10 + (q[3] - '0')) * 60000;
    ts += atoi(&q[4]);
    return ts;
}
void HoloRx7mImpl::parse_rawdata(DataStruct &d, const string &line)
{
    d.Sig = nan("");
    d.Ref = nan("");
    d.Ph = nan("");

    char buf[256];
    strncpy(buf, line.c_str(), sizeof buf);
    buf[sizeof buf - 1] = '\0';

    char *token, *sav;
    const char *pSig, *pRef, *pPh;

    if ((token = strtok_r(buf, DELIM, &sav)) == NULL)
        return;

    if (rawdataSetNum_m > 1) {
        if ((pPh  = strtok_r(NULL, DELIM, &sav)) == NULL)
            return;
        if ((pRef = strtok_r(NULL, DELIM, &sav)) == NULL)
            return;
        if ((pSig = strtok_r(NULL, DELIM, &sav)) == NULL)
            return;
    }

    if ((pPh  = strtok_r(NULL, DELIM, &sav)) == NULL)
        return;
    d.Ph = *pPh == '*' ? 180.0 : atof(pPh);

    if ((pRef = strtok_r(NULL, DELIM, &sav)) == NULL)
        return;
    d.Ref = atof(pRef);

    if ((pSig = strtok_r(NULL, DELIM, &sav)) == NULL)
        return;
    d.Sig = atof(pSig);
}
void HoloRx7mImpl::dataReceiver()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    ostringstream msg;
    string header_or_garbage;
    char *buf = rawio.buf;
    int &nres = rawio.nres;
    const int readsz = (sizeof(rawio.buf) - 1) / LEN_RAWDATA * LEN_RAWDATA;
    const int32_t cor = msec2acstime(cor_m);

    const int osize = rawdata_m.size();

    while (1) {
        int ret;
        ret = recv(sockD_m, buf+nres, readsz-nres, MSG_DONTWAIT);

        if (ret > 0) {
            MYLOG((LM_DEBUG, "recv() -> %d", ret));
            ret += nres;
            buf[ret] = '\0';
            dataStallTimer_m = 0;
        } else {
            if (! header_or_garbage.empty()) {
                MYLOG((LM_INFO, "rawdata: data port header (or garbage).\n%s"
                    , header_or_garbage.c_str()));
                header_or_garbage.clear();
            }
            if (ret < 0 && errno == EAGAIN) {
                MYLOG((LM_DEBUG, "recv() -> %d, EAGAIN", ret));
                break;
            }

            MYLOG((LM_DEBUG, "recv() -> %d", ret));
            msg << "error reading data port.";
            if (ret < 0) {
                msg << mystrerror(errno);
            } else {
                msg << " peer requested shutdown.";
            }
            MYLOG((LM_ERROR, "%s", msg.str().c_str()));

            ACS::ThreadSyncGuard guard(&scanStateMutex_m);
            if (isSSrunning) {
                isSSrunning = false;
                scanAborted_m = true;
                msgAborted_m = "abort - " + msg.str();
                sem_post(&scanEnded_m);
            }
            break;
        }

        char *p, *pp;

        for (p = pp = buf; p < &buf[ret]; pp = p) {
            struct rawdata t;
            t.buf = "";

            int ch = 0;
            while (p < &buf[ret]) {
                ch = *p++;
                if (ch == '\r')
                    continue;
                if (ch == '\0')
                    ch = '.';
                t.buf += ch;
                if (ch == '\n')
                    break;
            }
            if (ch != '\n')
                break;

            int32_t ts_raw_msec = parse_rawdata_timestamp(t.buf);

            if (ts_raw_msec < 0) {
                // timestamp parse error
                header_or_garbage += t.buf;
                continue;
            }
            if (! header_or_garbage.empty()) {
                MYLOG((LM_INFO, "rawdata: data port header (or garbage).\n%s"
                    , header_or_garbage.c_str()));
                header_or_garbage.clear();
            }

            Time ts_prev = rawdataLast_m.ts_raw;
            if (ts_prev == 0) {
                MYLOG((LM_INFO,
                    "first rawdata found (after 'GO'). ts_raw_msec=%d"
                    , ts_raw_msec));
                set_acstimeUT0h(ts_raw_msec);
                t.ts_raw = acstimeUT0h_m + msec2acstime(ts_raw_msec);
                rawdataForLogging_m.clear();
            } else {
                t.ts_raw = acstimeUT0h_m + msec2acstime(ts_raw_msec);
                if (t.ts_raw < ts_prev) {
                    MYLOG((LM_INFO,
                        "UT date changed. ts_raw_msec=%d"
                        , ts_raw_msec));
                    set_acstimeUT0h(ts_raw_msec);
                    t.ts_raw = acstimeUT0h_m + msec2acstime(ts_raw_msec);
                }
                if (t.ts_raw - ts_prev != (Time) cor) {
                    MYLOG((LM_DEBUG,
                        "interval error - prev=%llu now=%llu cor=%d"
                        , ts_prev, t.ts_raw, cor));
                    if (rawdataForLogging_m.empty())
                        rawdataForLogging_m = rawdataLast_m.buf;
                    rawdataForLogging_m += t.buf;
                } else {
                    if (! rawdataForLogging_m.empty()) {
                        MYLOG((LM_WARNING,
                            "rawdata: interval!=cor (%d msec)::\n%s"
                            , cor_m, rawdataForLogging_m.c_str()));
                        rawdataForLogging_m.clear();
                    }
                }
            }
            rawdataLast_m = t;
            rawdata_m.push_back(t);
        }

        nres = p - pp;
        if (nres) {
            memmove(buf, pp, nres);
            buf[nres] = '\0';
            MYLOG((LM_DEBUG, "nres=%d buf[]='%s'", nres, buf));
        }
    }

    if (hasGOcmdIssued) {
        dataStallTimer_m += loopTime_msec;
        if (dataStallTimer_m >= 10000) {
            // see comments in check_if_RXclock_is_ticking().
            msg << "data port has been stalled for "
                << dataStallTimer_m << " msec.";

            MYLOG((LM_ERROR, "%s", msg.str().c_str()));
            setError(msg.str());

            ACS::ThreadSyncGuard guard(&scanStateMutex_m);
            if (isSSrunning) {
                isSSrunning = false;
                scanAborted_m = true;
                msgAborted_m = "abort - " + msg.str();
                sem_post(&scanEnded_m);
            }
            dataStallTimer_m = 0;
        }
    }

    MYLOG((LM_DEBUG, "rawdata_m[%d -> %d]"
            , osize, rawdata_m.size()));
}
void HoloRx7mImpl::dataCollector()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    ostringstream msg;

    if (! isSSrunning) {
        // keep 1sec and discard old data.
        int osize = rawdata_m.size();
        int nkeep = 1000 / cor_m;    // 1000 for 1sec
        if (osize > nkeep) {
            vector<struct rawdata>::iterator it0;
            it0 = rawdata_m.begin() + osize - nkeep;
            rawdata_m.erase(rawdata_m.begin(), it0);
        }
        MYLOG((LM_DEBUG, "noSS - rawdata_m[%d -> %d]"
            , osize, rawdata_m.size()));
    } else {
        // Timestamp represents the beginning time of integration.
        // (confirmed with Kiuchi-san, 2011-04)

        // We will look for the data, which has timestamp between
        // [startTime_m - cor_m , stopTime_m)

        // Number of data to be found (data_m.size()) will be
        //  "(stopTime - startTime) / cor + 1".
        // This extra one data is needed for interpolation.

        MYLOG((LM_DEBUG, "inSS - rawdata_m[%d]"
            , rawdata_m.size()));

        Time startTime = startTime_m;
        Time stopTime  = stopTime_m;
        int32_t cor = msec2acstime(cor_m);

        MYLOG((LM_DEBUG, "index=%d start=%llu stop=%llu cor=%d"
                , indexInSubscan_m, startTime, stopTime, cor));

        vector<struct rawdata>::iterator it, it0;
        it = rawdata_m.begin();
        it0 = rawdata_m.end();
        for ( ; it < rawdata_m.end(); it++) {
            if (it->ts_raw < 0)
                continue;

            Time ts_raw = it->ts_raw + msec2acstime(hwClockOffset_m);

            Time ts;
            if (indexInSubscan_m < 0) {
                ts = ts_raw;
            } else {
                ts = ts_index0_m + cor * indexInSubscan_m;
            }

#if 0
            MYLOG((LM_DEBUG, "%llu < %llu" , ts, startTime));
            MYLOG((LM_DEBUG, "raw:%llu" , it->ts_raw));
#endif
            if (ts < startTime - cor) {
                it0 = it;                   // discard old data
                continue;
            }
            if (ts == ts_raw) {
                MYLOG((LM_DEBUG, " ts=%llu ts=raw", ts));
            } else {
                MYLOG((LM_DEBUG, " ts=%llu raw=%llu", ts, ts_raw));
            }
            if (indexInSubscan_m < 0) {
                if (ts < startTime) {
                    MYLOG((LM_INFO,
                        "rawdata: find 0th. (ts=%llu,start=%llu)"
                        , ts, startTime));
                    indexInSubscan_m = 0;
                    ts_index0_m = ts;
                } else if (startTime <= ts) {
                    msg << "rawdata: missed startTime data";
                    MYLOG((LM_ERROR, "%s", msg.str().c_str()));

                    ACS::ThreadSyncGuard guard(&scanStateMutex_m);
                    if (isSSrunning) {
                        isSSrunning = false;
                        scanAborted_m = true;
                        msgAborted_m = "abort - " + msg.str();
                        sem_post(&scanEnded_m);
                    }
                    break;
                }
            }
            if (indexInSubscan_m >= 0) {
                indexInSubscan_m++;

                HoloRx7mImpl::DataStruct d;
                d.timestamp = ts;
                d.rawbuf = logEnable_m ? it->buf : "";
                if (ts != ts_raw)
                    d.flag = true;
                parse_rawdata(d, it->buf);
                data_m.push_back(d);

                it0 = it;   // consume one data

                if (stopTime && stopTime - cor <= ts) {
                    MYLOG((LM_INFO,
                        "rawdata: find last. (SS success) (ts=%llu,stop=%llu,data_m[%d])"
                        , ts, stopTime, data_m.size()));

                    // assert(indexInSubscan_m == (stopTime-startTime) / cor + 1);
                    // assert(indexInSubscan_m == data_m.size());

                    indexInSubscan_m = -1;
                    ts_index0_m = 0;

                    ACS::ThreadSyncGuard guard(&scanStateMutex_m);
                    if (isSSrunning) {
                        isSSrunning = false;
                        sem_post(&scanEnded_m);
                    }
                    break;
                }
                if (stopTime == 0 && indexInSubscan_m > 30000 / cor_m) {
                    msg << "30 sec since start, and stop time is not yet set";
                    MYLOG((LM_ERROR, "%s", msg.str().c_str()));

                    ACS::ThreadSyncGuard guard(&scanStateMutex_m);
                    if (isSSrunning) {
                        isSSrunning = false;
                        scanAborted_m = true;
                        msgAborted_m = "abort - " + msg.str();
                        sem_post(&scanEnded_m);
                    }
                    break;
                }
            }
        }
        if (it0 != rawdata_m.end()) {
            it0++;
            rawdata_m.erase(rawdata_m.begin(), it0);
        }
    }
}

//---------------------------------------------------------------------
void HoloRx7mImpl::setHwClockOffset(CORBA::Long clockOffsetInMsec)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    hwClockOffset_m = clockOffsetInMsec;
    MYLOG((LM_INFO, "hwClockOffset (%d ms)", hwClockOffset_m));
}
CORBA::Long HoloRx7mImpl::getHwClockOffset()
{
     return hwClockOffset_m;
}

//-------------------------------------------------------------------
HoloRx7mImpl::DataThread::
DataThread(const ACE_CString& name, HoloRx7mImpl& holoRx7m, 
	     const TimeInterval responseTime,
	     const TimeInterval sleepTime):
    ACS::Thread(name, responseTime, sleepTime),
    holoRx7m_m(holoRx7m)
{
}

void HoloRx7mImpl::DataThread::runLoop()
{
    if (holoRx7m_m.hasGOcmdIssued) {
        holoRx7m_m.dataReceiver();
        holoRx7m_m.dataCollector();
    } else {
        ACS::ThreadSyncGuard guard(&holoRx7m_m.scanStateMutex_m);
        if (holoRx7m_m.isSSrunning) {
            // force Abort requested.
            holoRx7m_m.isSSrunning = false;
            holoRx7m_m.scanAborted_m = true;
            holoRx7m_m.msgAborted_m = "abort requested.";
            sem_post(&holoRx7m_m.scanEnded_m);
        }
        holoRx7m_m.rawdata_m.clear();
    }
}

//-------------------------------------------------------------------

/////////////////////////////////////////////////
// MACI DLL support functions
/////////////////////////////////////////////////
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(HoloRx7mImpl)
