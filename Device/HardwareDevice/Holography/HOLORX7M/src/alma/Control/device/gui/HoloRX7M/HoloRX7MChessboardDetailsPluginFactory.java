package alma.Control.device.gui.HoloRX7M;

import alma.common.gui.chessboard.ChessboardDetailsPluginFactory;
import alma.common.gui.chessboard.ChessboardDetailsPlugin;
import alma.common.gui.chessboard.ChessboardDetailsPluginListener;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

public class HoloRX7MChessboardDetailsPluginFactory implements ChessboardDetailsPluginFactory {
        
       private static final int INSTANCES = 1;

        /**
         * @see alma.common.gui.chessboard.ChessboardDetailsPluginFactory
         */
        public ChessboardDetailsPlugin[] instantiateDetailsPlugin(
                        String[] names,
                        PluginContainerServices containerServices,
                        ChessboardDetailsPluginListener listener, String pluginTitle, String selectionTitle)
        {
                HoloRX7MDetailsPlugin[] holorx7mPlugin = new HoloRX7MDetailsPlugin[INSTANCES];
                if(null != names && names.length > 0)
                {
                        holorx7mPlugin[0] =
                                new HoloRX7MDetailsPlugin(names, listener, containerServices, pluginTitle, selectionTitle);
                }
                return holorx7mPlugin;
        }
}

