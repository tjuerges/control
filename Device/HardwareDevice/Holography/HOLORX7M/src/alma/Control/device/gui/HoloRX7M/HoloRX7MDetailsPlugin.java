package alma.Control.device.gui.HoloRX7M ;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import alma.Control.gui.hardwaredevice.common.HardwareDeviceChessboardDetailsPlugin;
import alma.common.gui.chessboard.ChessboardDetailsPluginListener;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

public class HoloRX7MDetailsPlugin extends HardwareDeviceChessboardDetailsPlugin
{
	private final static String HOLORX7M_PLUGIN_NAME = "Holography RX7M: ";
	private static final int STANDARD_INSET = 5;
	private String pluginTitle;
	//private JTabbedPane tabbedPane = null;
        private HoloRX7MDetailsControl controlPanel;
	
	/**
	 * TODO
	 */
	private static final long serialVersionUID = 1L;

	public HoloRX7MDetailsPlugin(String antennaNames[], 
			ChessboardDetailsPluginListener listener,
			PluginContainerServices services, String pluginTitle, String selectionTitle) 
	{
		super(antennaNames, listener, services, pluginTitle, 
				selectionTitle, new HoloRX7MPresentationModel(antennaNames[0], services));
		
		this.pluginTitle = pluginTitle;
		initialize();
	}
	
	/**
	 * Required method of the SubsystemPlugin interface
	 */
	public boolean runRestricted(boolean restricted) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * Required method of the SubsystemPlugin interface
	 */
	public void specializedStart() {
	}

	@Override
	public void replaceContents(String[] newAntennaNames) 
	{
		assert (null != newAntennaNames && newAntennaNames.length > 0) : "newAntennaNames array malformed";
		this.presentationModel = new HoloRX7MPresentationModel(antennaNames[0], this.pluginContainerServices); 
	}

	@Override
	public String getPluginName() {
		return HOLORX7M_PLUGIN_NAME;
	}

	/**
	 * Getter for the title of the plugin, which comprises the name of the plugin + potentially 
	 * some extra characters to make it unique. For example, "Holography RX7M - 1"
	 * @return the pluginTitle
	 */
	public String getTitle() {
		return this.pluginTitle;
	}
		
	/*
	 * Private method to initialize the plugin.
	 */
	private void initialize()
	{
		logger.entering("HoloRX7MDetailsPlugin", "initialize");
		
		this.setLayout(new GridBagLayout());
		JPanel topPanel = new JPanel();
		topPanel.setBorder(BorderFactory.createEmptyBorder(STANDARD_INSET, STANDARD_INSET, STANDARD_INSET, STANDARD_INSET));
                controlPanel = new HoloRX7MDetailsControl(logger, (HoloRX7MPresentationModel)presentationModel);
                
                GridBagConstraints constraints = new GridBagConstraints();
                constraints.gridx = 0;
                constraints.gridy = 0;
                constraints.weightx = 1.0;
                constraints.weighty = 0.0;
                constraints.ipadx = 0;
                constraints.ipady = 5;
                constraints.anchor = GridBagConstraints.NORTHWEST;
                constraints.fill = GridBagConstraints.VERTICAL;

                topPanel.add(controlPanel);
             
                this.add(topPanel, constraints);
		this.requestFocus();
		this.validate();

		logger.exiting("HoloRX7MDetailsPlugin", "initialize");
	}
	
}
