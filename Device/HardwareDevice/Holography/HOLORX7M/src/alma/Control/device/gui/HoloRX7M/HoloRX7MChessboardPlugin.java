package alma.Control.device.gui.HoloRX7M;

import alma.Control.gui.hardwaredevice.common.HardwareDeviceChessboardPresentationModel;
import alma.common.gui.chessboard.ChessboardDetailsPluginFactory;
import alma.common.gui.chessboard.ChessboardPlugin;
import alma.common.gui.chessboard.ChessboardStatusProvider;

@SuppressWarnings("serial")
public class HoloRX7MChessboardPlugin extends ChessboardPlugin {
        /**
         * TODO - serialVersionUID
         */
        private static final long serialVersionUID = 1L;

        /**
         * No-args constructor required by Exec's plugin framework.
         */
        public HoloRX7MChessboardPlugin()
        {
        }

        @Override
        protected ChessboardDetailsPluginFactory getDetailsProvider() {
                detailsProvider = new HoloRX7MChessboardDetailsPluginFactory();
                return detailsProvider;
        }

        @Override
        protected ChessboardStatusProvider getStatusProvider() {
                statusProvider = new HardwareDeviceChessboardPresentationModel(pluginContainerServices,
                                HoloRX7MPresentationModel.HOLORX7M_DEVICE_STRING);
                return statusProvider;
        }
}

