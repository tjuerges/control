package alma.Control.device.gui.HoloRX7M;

import java.util.ArrayList;
import java.util.List;

import alma.ACS.Bool;
import alma.ACSErr.CompletionHolder;
import alma.Control.HoloRx7m;
import alma.Control.HoloRx7mHelper;
import alma.Control.gui.hardwaredevice.common.HardwareDeviceChessboardPresentationModel;
import alma.Control.gui.hardwaredevice.common.HardwareDevicePresentationModel;
import alma.Control.gui.hardwaredevice.common.HardwareDevicePresentationModelEvent;
import alma.Control.gui.hardwaredevice.common.util.FormattedDouble;
import alma.Control.HardwareDevice;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

/**
 * Client-side data model for the optical holorx7m; allows an MVC-like 
 * architecture. This is should be the single point of communication 
 * (i.e. coupling) between the control system (backend and devices)
 * and the holorx7m user interface.
 *  
 * @author Steve Harrington
 * @see HardwareDevicePresentationModel
 */
public class HoloRX7MPresentationModel extends HardwareDevicePresentationModel
{	
	// TODO: find out if there are constants defined somewhere w/in control that we can use for these!
	/**
	 * String to lookup optical holorx7m devices
	 */
	public final static String HOLORX7M_DEVICE_STRING = "HoloRx7m";
	//private final static String TMCDBCOMPONENT_IDL_STRING = "IDL:alma/TMCDB/TMCDBComponent:1.0";

	
	// TODO - determine how to get the component name so that we don't hardcode it. 
	// Will be available from TMCDB in some fashion, per conversation with Allen Farris.
	private String holoRx7mComponentName = null;
	private HoloRx7m holorx7m = null;
	private String antennaName = null;
	
	
	/**
	 * Constructor.
	 * @param antennaName the name of the antenna for which this presentation model will be used.
	 * @param services the plugin's container services.
	 */
	public HoloRX7MPresentationModel(String antennaName, PluginContainerServices services) 
	{	
		this.setServices(services);
		logger.entering("HoloRX7MPresentationModel", "constructor");
		
		this.antennaName = antennaName;
		
		logger.exiting("HoloRX7MPresentationModel", "constructor");
	}
	
	
	/**
	 * Method to detect any changes in the data, since the last polling was performed.
	 * @return a list of events indicating what has changed.
	 */
	protected List<HardwareDevicePresentationModelEvent> queryForStateChanges()
	{
		ArrayList<HardwareDevicePresentationModelEvent> retList = new ArrayList<HardwareDevicePresentationModelEvent>();
		HardwareDevicePresentationModelEvent event = null;
		
		return retList;
	}
	
	/**
	 * Method used to shutdown our connection to control sub system
	 * (e.g. release components, stop threads, etc.). This should
	 * be called when the optical holorx7m plugin is stopping.
	 * 
	 * @see HoloRX7MDetailsPlugin
	 */
	protected void specializedStop()
	{
		logger.entering("HoloRX7MPresentationModel", "stop");

		// nothing to do here at the present time...
		
		logger.exiting("HoloRX7MPresentationModel", "stop");
	}
	
	/**
	 * Method to release the device component, reset the variable to null, etc.
	 */
	protected synchronized void releaseDeviceComponent()
	{
		if(null != holorx7m)
		{
			// TODO - if we use getComponentNonSticky, it will make the release component unnecessary...
			pluginContainerServices.releaseComponent(holoRx7mComponentName);
			holorx7m = null;
			holoRx7mComponentName = null;
		}	
	}
	
	/**
	 * Method to get/set the device component reference.
	 */
	protected synchronized HardwareDevice setDeviceComponent() throws AcsJContainerServicesEx
	{
		// get the optical holorx7m device name, if we haven't done so previously
		if(null == this.holoRx7mComponentName) 
		{
			this.holoRx7mComponentName = HardwareDeviceChessboardPresentationModel.
				getComponentNameForDevice(this.pluginContainerServices, antennaName, HOLORX7M_DEVICE_STRING);
		}
		
		// if we haven't already gotten the holorx7m component, do so now
		if(null == holorx7m && null != holoRx7mComponentName)  
		{
			try 
			{
				// TODO - getComponentNonSticky?
				org.omg.CORBA.Object holorx7mObject = this.pluginContainerServices.getComponent(holoRx7mComponentName);
				this.holorx7m = HoloRx7mHelper.narrow(holorx7mObject);
			}
			catch (AcsJContainerServicesEx ex) 
			{
				// log the error
				logger.severe("HoloRX7MPresentationModel.retrieveHoloRX7MComponent() - unable to obtain holorx7m component: "
						+ holoRx7mComponentName);
				
				// set our device reference to null
				this.holorx7m = null;
				
				// rethrow the exception so that the base class can take action
				throw ex;
			}
		}
		
		// return a reference to the holorx7m device, or null if we couldn't get one
		return holorx7m;
	}
	
}
