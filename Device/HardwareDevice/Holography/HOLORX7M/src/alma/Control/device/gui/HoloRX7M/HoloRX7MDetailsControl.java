package alma.Control.device.gui.HoloRX7M ;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Dimension;

import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.BorderFactory;

import alma.Control.gui.hardwaredevice.common.HardwareDeviceAmbsiPanel;
import alma.Control.gui.hardwaredevice.common.HardwareDevicePresentationModelEvent;

public class HoloRX7MDetailsControl extends HardwareDeviceAmbsiPanel {

	private static final long serialVersionUID = 1L;
	private JCheckBox enableRX7MReadout = null;
	private JCheckBox plotData = null;
	private JCheckBox logDataFile = null;
	private JButton resetRX7M = null;
	private JSpinner updateInterval = null;
	private JTextField logFile = null;
	private JLabel updateIntervalLabel = null;
        private HoloRX7MPresentationModel presentationModel;
        private Logger logger;

        /**
         * Constructor.
         * @param logger <code>Logger</code> to be used for logging messages.
         * @param presentationModel the presentation model used to communicate with the control subsystem for
         * state information.
         */
	public HoloRX7MDetailsControl(Logger logger, HoloRX7MPresentationModel presentationModel) {
		super(presentationModel);
                this.presentationModel = presentationModel;
                this.logger = logger;
                logger.entering("HoloRX7MControlPanel", "constructor");
                initialize();
                logger.exiting("HoloRX7MControlPanel", "constructor");
	}

        @Override
        protected void specializedProcessChange(HardwareDevicePresentationModelEvent evt) {
                logger.entering("HoloRX7MControlPanel", "specializedProcessChange");
                //TODO
                logger.exiting("HoloRX7MControlPanel", "specializedProcessChange");
         }

         @Override
         protected void specializedCommunicationEstablished() {
             //TODO
         }

         @Override
         protected void specializedCommunicationLost() {
             //TODO
         }

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		GridBagConstraints gridBagConstraints5 = new GridBagConstraints();
		gridBagConstraints5.gridx = 3;
		gridBagConstraints5.gridy = 0;
		updateIntervalLabel = new JLabel();
		updateIntervalLabel.setText("RX7M update interval [ms]");
		GridBagConstraints gridBagConstraints4 = new GridBagConstraints();
		gridBagConstraints4.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints4.gridy = 4;
		gridBagConstraints4.weightx = 1.0;
		gridBagConstraints4.gridx = 3;
		GridBagConstraints gridBagConstraints31 = new GridBagConstraints();
		gridBagConstraints31.gridx = 5;
		gridBagConstraints31.gridy = 0;
		GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
		gridBagConstraints3.gridx = 3;
		gridBagConstraints3.gridy = 1;
		GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
		gridBagConstraints2.gridx = 0;
		gridBagConstraints2.anchor = GridBagConstraints.WEST;
		gridBagConstraints2.gridy = 4;
		GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
		gridBagConstraints1.gridx = 0;
		gridBagConstraints1.anchor = GridBagConstraints.WEST;
		gridBagConstraints1.gridy = 2;
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.gridy = 1;
		//this.setSize(381, 205);
                this.setBorder(BorderFactory.createTitledBorder("RX7M Data"));
		this.setLayout(new GridBagLayout());
		this.add(getEnableRX7MReadout(), gridBagConstraints);
		this.add(getPlotData(), gridBagConstraints1);
		this.add(getLogDataFile(), gridBagConstraints2);
		this.add(getResetRX7M(), gridBagConstraints3);
		this.add(getUpdateInterval(), gridBagConstraints31);
		this.add(getLogFile(), gridBagConstraints4);
		this.add(updateIntervalLabel, gridBagConstraints5);
	}

	/**
	 * This method initializes enableRX7MReadout	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getEnableRX7MReadout() {
		if (enableRX7MReadout == null) {
			enableRX7MReadout = new JCheckBox();
			enableRX7MReadout.setText("Enable RX7M readout");
			enableRX7MReadout.setHorizontalAlignment(SwingConstants.TRAILING);
		}
		return enableRX7MReadout;
	}

	/**
	 * This method initializes plotData	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getPlotData() {
		if (plotData == null) {
			plotData = new JCheckBox();
			plotData.setText("Plot data.");
			plotData.setHorizontalAlignment(SwingConstants.TRAILING);
		}
		return plotData;
	}

	/**
	 * This method initializes logDataFile	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getLogDataFile() {
		if (logDataFile == null) {
			logDataFile = new JCheckBox();
			logDataFile.setText("Log data to file.");
			logDataFile.setHorizontalAlignment(SwingConstants.TRAILING);
		}
		return logDataFile;
	}

	/**
	 * This method initializes resetRX7M	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getResetRX7M() {
		if (resetRX7M == null) {
			resetRX7M = new JButton();
			resetRX7M.setText("Reset RX7M");
		}
		return resetRX7M;
	}

	/**
	 * This method initializes updateInterval	
	 * 	
	 * @return javax.swing.JSpinner	
	 */
	private JSpinner getUpdateInterval() {
		if (updateInterval == null) {
			updateInterval = new JSpinner(new SpinnerNumberModel(48,48,30000,1));
			updateInterval.setPreferredSize(new Dimension(50, 24));
		}
		return updateInterval;
	}

	/**
	 * This method initializes logFile	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getLogFile() {
		if (logFile == null) {
			logFile = new JTextField();
			logFile.setText("Log File Stub");
			
		}
		return logFile;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
