//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2006, 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

// Copyright (c) 2011 National Astronomical Observatory of Japan

// $Id$

#include <HoloRx7mCompSimImpl.h>


#define SIM_PATH "Holo7mRawData/simdata.txt"
#define MYLOG(X)  ACS_LOG(LM_FULL_INFO, __func__, X)

using Control::HoloRx7mImpl;
using Control::HoloRx7mCompSimImpl;
using ControlExceptions::HardwareErrorEx;
using ControlExceptions::HardwareErrorExImpl;
using ACS::Time;

using std::vector;
using std::string;
using std::ostringstream;


//---------------------------------------------------------------------
static string mystrerror(int e)
{
    char errmsg[80];
    const char *ep = strerror_r(e, errmsg, sizeof errmsg);
    ostringstream msg;
    msg << " errno=" << e << "/'" << ep << "'";
    return msg.str();
}

//---------------------------------------------------------------------
HoloRx7mCompSimImpl::HoloRx7mCompSimImpl(const ACE_CString & name,
			 maci::ContainerServices* cs):
    HoloRx7mImpl(name, cs),

    simPath_m(SIM_PATH)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

HoloRx7mCompSimImpl::~HoloRx7mCompSimImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    ifs_m.close();
}

//-------------------------------------------------------------------
void HoloRx7mCompSimImpl::connectRX()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    isRXconnected = true;
    hasGOcmdIssued = true;

    setSimPath(0);
}
void HoloRx7mCompSimImpl::disconnectRX()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    hasGOcmdIssued = false;
    isRXconnected = false;

    closeSimPath();
}

//-------------------------------------------------------------------
void HoloRx7mCompSimImpl::setSimPath(const char *path)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    closeSimPath();

    if (path) {
        if (path[0] != '\0')
            simPath_m = path;
        if (simPath_m == ".")
            simPath_m = SIM_PATH;
    }

    ifs_m.open(simPath_m.c_str(), ios::in);
    if (ifs_m) {
        MYLOG((LM_INFO, "simdata: simulation data file '%s' - ok"
            , simPath_m.c_str()));
    } else {
        ostringstream msg;
        msg << "simdata: error opening simulation data file '"
            << simPath_m
            << "' - "
            << mystrerror(errno);
        HardwareErrorExImpl nex(
                __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", msg.str());
        nex.log();
        throw nex.getHardwareErrorEx();
    }
}
void HoloRx7mCompSimImpl::closeSimPath()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if (ifs_m.is_open()) {
        ifs_m.close();
        MYLOG((LM_DEBUG, "simdata: simulation data file closed."));
    }
}

//-------------------------------------------------------------------
void HoloRx7mCompSimImpl::getSS()
{
    char buf[256];

    rawdata_m.clear();

    if (ifs_m.is_open() && ! ifs_m.eof()) {
        // skip comment lines:
        //  lines starting with '#' are comments.
        //  At least one comment line is required
        //  between subscans.
        int c = '#';
        while (c == '#' && ! ifs_m.eof()) {
            c = ifs_m.peek();
            if (c == '#')
                ifs_m.getline(buf, sizeof(buf), '\n');
        }

        while (! ifs_m.eof()) {
            ifs_m.getline(buf, sizeof(buf), '\n');
            if (buf[0] == '#')
                break;

            struct rawdata t;
            t.ts_raw = HoloRx7mImpl::parse_rawdata_timestamp(buf);
            t.buf = buf;

            t.ts_raw = t.ts_raw % 60000;

            rawdata_m.push_back(t);
        }
    }
}

Control::HoloRx7m::HoloDeviceData*
HoloRx7mCompSimImpl::getSubscanData(double timeout)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    uint32_t cor = cor_m * 10000;
    int num = (stopTime_m - startTime_m) / cor + 1;

    int ts_raw;
    int gap;

    getSS();
    if (rawdata_m.empty()) {
        ts_raw = 0;
        gap = 0;
    } else {
        ts_raw = rawdata_m[0].ts_raw;
        gap = (ts_raw + hwClockOffset_m) % cor_m;
    }
    Time ts_index0 = startTime_m - cor + gap * 10000;

    MYLOG((LM_DEBUG,
            "simdata: startTime_m=%llu stopTime_m=%llu cor_m=%d num=%d"
            , startTime_m, stopTime_m, cor_m, num));
    MYLOG((LM_DEBUG,
            "simdata: ts_raw=%d hwClockOffset=%d gap=%d"
            , ts_raw, hwClockOffset_m, gap));

    int nraw = rawdata_m.size();
    vector<struct rawdata>::iterator it;
    it = rawdata_m.begin();

    int index = 0;
    if (nraw > 0) {
        int tnum = num < nraw ? num : nraw;
        for ( ; index < tnum; index++) {
            DataStruct d;
            HoloRx7mImpl::parse_rawdata(d, it->buf);
            d.timestamp = ts_index0 + cor * index;
            d.rawbuf = "";
            d.flag = false;
            data_m.push_back(d);
            it++;
        }
        if (nraw < num) {
            MYLOG((LM_DEBUG,
                "simdata: simulation data shortage. (%d/%d)"
                " reuse last data."
                , nraw, num));
        } else if (nraw > num) {
            MYLOG((LM_DEBUG,
                "simdata: simulation data excess. (%d/%d)"
                " discard some."
                , nraw, num));
        }
    } else {
        MYLOG((LM_DEBUG,
            "simdata: no simulation data for this SS."));
    }
    for ( ; index < num; index++) {
        DataStruct d;
        d.Sig = d.Ref = d.Ph = 0.0;
        if (! data_m.empty())
            d = data_m.back();
        d.timestamp = ts_index0 + cor * index;
        d.rawbuf = "";
        d.flag = false;
        data_m.push_back(d);
    }

    isSSrunning = false;
    sem_post(&scanEnded_m);

    return HoloRx7mImpl::getSubscanData(timeout);
}

//-------------------------------------------------------------------

/////////////////////////////////////////////////
// MACI DLL support functions
/////////////////////////////////////////////////
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(HoloRx7mCompSimImpl)
