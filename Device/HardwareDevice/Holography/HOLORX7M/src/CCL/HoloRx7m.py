#! /usr/bin/env python
#
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# Copyright (c) 2011 National Astronomical Observatory of Japan

"""
This module is part of the Control Command Language.
It defines the HoloRx7m class that provides low level control of the
digital signal processor in holography reciever.
"""

import Acspy.Clients.SimpleClient
import Acspy.Common.QoS
import Acspy.Util.ACSCorba
import CCL.HardwareDevice

class HoloRx7m(CCL.HardwareDevice.HardwareDevice):
    def __init__(self, componentName = None, stickyFlag = False):
        '''
        The HoloRx7m class is a python proxy to the HoloRx7m
        component. The component can be running before creating this
        proxy but if it is not it will be started i.e., the object
        reference is obtained using a call to getComponentNonSticky
        (stickyFlag = False, default) or getComponent (stickyFlag =
        True).
        
        from CCL.HoloRx7m import *
        hrx7m = HoloRx7m("CONTROL/CM03/HoloRx7m");
        if str(hrx7m.getHwState()) == 'Start':
          hrx7m.hwConfigure(); hrx7m.hwInitialize(); hrx7m.hwOperational();
        from time import time
        from TETimeUtil import *
        hrx7m.startSubscan(plusTE(ceilTE(unix2epoch(time.time())), 50).value);
        hrx7m.stopSubscan(plusTE(ceilTE(unix2epoch(time.time())), 230).value);
        del(hrx7m);
        '''
        # initialize the base class
        CCL.HardwareDevice.HardwareDevice.__init__(self, componentName, stickyFlag);

    def __del__(self):
        CCL.HardwareDevice.HardwareDevice.__del__(self);

    def startSubscan(self, startTime):
        return self._HardwareDevice__hw.startSubscan(startTime);

    def stopSubscan(self, stopTime):
        return self._HardwareDevice__hw.stopSubscan(stopTime);

    def getSubscanData(self, timeout):
        return self._HardwareDevice__hw.getSubscanData(timeout);

#
    def setHwClockOffset(self, clockDelay=0):
        return self._HardwareDevice__hw.setHwClockOffset(clockDelay);
    def getHwClockOffset(self):
        return self._HardwareDevice__hw.getHwClockOffset();

    def connectRX(self):
        return self._HardwareDevice__hw.connectRX();
    def disconnectRX(self):
        return self._HardwareDevice__hw.disconnectRX();

#
    def checkHwClock(self):
        '''
        Check if hardware clock of the Receiver is ticking.
        '''
        return self._HardwareDevice__hw.setHwClock(delay);

#
    def setSimPath(self, path=""):
        '''
        Close current simulation datafile (if opened) and
        open 'path' file as datafile to be used next.
        Re-open previous datafile if path is "".
        Open default datafile if path is ".".
        '''
        return self._HardwareDevice__hw.setSimPath(path)

    def closeSimPath(self):
        '''
        close current simulation datafile if opened.
        '''
        return self._HardwareDevice__hw.closeSimPath()

