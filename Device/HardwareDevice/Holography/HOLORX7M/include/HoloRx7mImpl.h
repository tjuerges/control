#ifndef HOLORX7MIMPL_H
#define HOLORX7MIMPL_H
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2006, 2009
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

// Copyright (c) 2011 National Astronomical Observatory of Japan

#include <acscommonC.h>

#include <hardwareDeviceImpl.h>
#include <ambCDBAccess.h>

//CORBA servant header
#include <HoloRx7mS.h>

//includes for data members
#include <vector>
#include <acsThread.h>


namespace Control {

    /**
     *
     */
    class HoloRx7mImpl : 
                        public Control::HardwareDeviceImpl,
                        public AmbCDBAccessor,
                        virtual public POA_Control::HoloRx7m
    {
    public:
        // ------------------- Constructor & Destructor -------------------

        HoloRx7mImpl(const ACE_CString & name, maci::ContainerServices* cs);

        virtual ~HoloRx7mImpl();

        // --------------------- Component LifeCycle interface -------------

        /// This creates the data port thread. Its starts suspended and is
        /// activated by the startSubScan method.
        virtual void initialize();

        /// This terminates the data port thread (and in normal cases it should
        /// already have been suspended).
        virtual void cleanUp();
      
        // --------------------- Hardware LifeCycle interface -------------

        virtual void hwStartAction();
        virtual void hwStopAction();
        virtual void hwConfigureAction();
        virtual void hwInitializeAction();
        virtual void hwOperationalAction();

        // --------------------- CORBA interface --------------------------

        /// See the IDL file for a description of this function.
        virtual void startSubscan(ACS::Time startTime);

        /// See the IDL file for a description of this function.
        virtual void stopSubscan(ACS::Time stopTime);

        /// See the IDL file for a description of this function.
        virtual Control::HoloRx7m::HoloDeviceData* 
            getSubscanData(double waitTime);

        ///
        virtual void getDeviceUniqueId(std::string &deviceID);

        // --------------------- C++ interface --------------------------

        /// set&get hardware clock offset of the Receiver.
        virtual void setHwClockOffset(CORBA::Long clockOffsetInMsec);
        virtual CORBA::Long getHwClockOffset();

        /// Holography Receiver connection, initialization
        /// and dataThread management
        virtual void connectRX();
        virtual void disconnectRX();

        /// check hardware clock of the Receiver.
        virtual void checkHwClock();

    private:
        // status
        bool isRXconnected;
        bool hasGOcmdIssued;

        ACE_Recursive_Thread_Mutex scanStateMutex_m;
        bool isSSrunning;

        // Flag which is set when a subscan is Aborted
        bool scanAborted_m;
        std::string msgAborted_m;

        // Start time of the current subscan 
        ACS::Time startTime_m;

        // Stop time of the current subscan
        ACS::Time stopTime_m;

        // Memory block where data from the last subscan is stored after a call
        // to getSubscanData has been made.
        std::vector<Control::HolographyData> deviceData_m;

        // config params
        std::string hostname_m;
        std::string macAddress_m;
        unsigned short portC_m;     // command port - port number
        unsigned short portD_m;     // data port - port number
        double timeoutRxC_m;
        double timeoutRxD_m;
        unsigned int lingerTime_m;
        unsigned short retries_m;
        bool earlyConnect_m;
        int rawdataSetNum_m;    // raw data has two sets of (Sig,Ref,Ph)
        int cor_m;              // COR value (msec)
        int logEnable_m;
        std::string logDir_m, logFile_m;

        // file descriptors for RX communication
        int sockC_m;
        int sockD_m;

        // dataThread notify getSubscanData() of end of subscan.
        sem_t scanEnded_m;

        // RX comm./control methods. (cmd port)
        std::string do_cmd(const char *cmd);
        void do_TIM();
        void do_COR();
        void init_RXclock();
        void init_COR();
        int check_if_RXclock_is_ticking();

        // Time offset in the Receiver clock.
        int hwClockOffset_m;            // in msec

        // Receiver TCP connection control methods
        void connRX();
        void disconnRX();
        // data port / dataThread control methods
        void dataport_go();
        void dataport_pause();


        // RX data struct and handling methods
        struct DataStruct {
            ACS::Time   timestamp;
            std::string rawbuf;
            bool   flag;
            double Sig;
            double Ref;
            double Ph;
        };
        std::vector<HoloRx7mImpl::DataStruct> data_m;

        void calculateSQR(Control::HolographyData &res, const DataStruct &data);

        void interpolate(DataStruct &interpData,
                        const DataStruct &prevData,
                        const DataStruct &thisData,
                        const int timeGap);

        void saveRawData();


        //------------------------
        // Receive raw data from Holography Receiver

        class DataThread: public ACS::Thread {
        public:
            DataThread(const ACE_CString &name, HoloRx7mImpl &holoRx7m,
                         const ACS::TimeInterval responseTime,
                         const ACS::TimeInterval sleepTime);
            virtual void runLoop();
        private:
            HoloRx7mImpl &holoRx7m_m;
        };
        DataThread *dataThread_m;

        // receive raw data from RX data port.
        void dataReceiver();
        // pick up rawdata while subscan is running.
        void dataCollector();

        // data port receive buffer
        struct {
            int  nres;
            char buf[4096];
        } rawio;

        struct rawdata {
            ACS::Time   ts_raw; // timestamp before offset correction
            std::string buf;
        };
        std::vector<struct rawdata>  rawdata_m;

        // for checking interval error and logging.
        struct rawdata rawdataLast_m;
        std::string rawdataForLogging_m;

        // for checking data port stall.
        int dataStallTimer_m;

        void set_acstimeUT0h(int32_t ts_msec);
        ACS::Time acstimeUT0h_m;        // acs time of UT 0:00

        ACS::Time ts_index0_m;          // 0-th data timestamp in a subscan
        int indexInSubscan_m;

        void parse_rawdata(DataStruct &d, const std::string &line);
        int  parse_rawdata_timestamp(const std::string &line);

        //------------------------

        friend class HoloRx7mCompSimImpl;

    }; // end HoloRx7mImpl class
} // end Control namespace

#endif // HOLORX7MIMPL_H
