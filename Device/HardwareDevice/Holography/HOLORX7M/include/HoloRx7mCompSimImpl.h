#ifndef HoloRx7mCompSimImpl_h
#define HoloRx7mCompSimImpl_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2006, 2009
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

// Copyright (c) 2011 National Astronomical Observatory of Japan

#include <HoloRx7mImpl.h>

#include <HoloRx7mCompSimS.h>

namespace Control {

    /**
     *
     */
    class HoloRx7mCompSimImpl :
                        public HoloRx7mImpl,
                        virtual public POA_Control::HoloRx7mCompSim
    {
    public:

        HoloRx7mCompSimImpl(const ACE_CString & name, maci::ContainerServices* cs);
        virtual ~HoloRx7mCompSimImpl();

        virtual void setSimPath(const char *path);
        virtual void closeSimPath();

        virtual void connectRX();
        virtual void disconnectRX();

        virtual Control::HoloRx7m::HoloDeviceData*
            getSubscanData(double waitTime);

    private:
        HoloRx7mCompSimImpl(const HoloRx7mCompSimImpl&);
        HoloRx7mCompSimImpl& operator = (const HoloRx7mCompSimImpl&);

        std::string simPath_m;
        ifstream ifs_m;

        void getSS();

    };
}

#endif
