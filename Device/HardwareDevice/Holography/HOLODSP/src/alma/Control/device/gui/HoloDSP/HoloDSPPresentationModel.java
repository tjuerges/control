package alma.Control.device.gui.HoloDSP;

import java.util.ArrayList;
import java.util.List;

import alma.ACS.Bool;
import alma.ACSErr.CompletionHolder;
import alma.Control.HOLODSPBase;
import alma.Control.HOLODSPBaseHelper;
import alma.Control.gui.hardwaredevice.common.HardwareDeviceChessboardPresentationModel;
import alma.Control.gui.hardwaredevice.common.HardwareDevicePresentationModel;
import alma.Control.gui.hardwaredevice.common.HardwareDevicePresentationModelEvent;
import alma.Control.gui.hardwaredevice.common.util.FormattedDouble;
import alma.Control.HardwareDevice;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

/**
 * Client-side data model for the optical holodsp; allows an MVC-like 
 * architecture. This is should be the single point of communication 
 * (i.e. coupling) between the control system (backend and devices)
 * and the optical holodsp user interface.
 *  
 * @author Steve Harrington
 * @see HardwareDevicePresentationModel
 */
public class HoloDSPPresentationModel extends HardwareDevicePresentationModel
{	
	// TODO: find out if there are constants defined somewhere w/in control that we can use for these!
	/**
	 * String to lookup optical holodsp devices
	 */
	public final static String HOLODSP_DEVICE_STRING = "HoloDSP";
	//private final static String TMCDBCOMPONENT_IDL_STRING = "IDL:alma/TMCDB/TMCDBComponent:1.0";

	
	// TODO - determine how to get the component name so that we don't hardcode it. 
	// Will be available from TMCDB in some fashion, per conversation with Allen Farris.
	private String holoDspComponentName = null;
	private HOLODSPBase holodsp = null;
	private String antennaName = null;
	
	
	/**
	 * Constructor.
	 * @param antennaName the name of the antenna for which this presentation model will be used.
	 * @param services the plugin's container services.
	 */
	public HoloDSPPresentationModel(String antennaName, PluginContainerServices services) 
	{	
		this.setServices(services);
		this.antennaName = antennaName;
	}
	
	
	/**
	 * Method to detect any changes in the data, since the last polling was performed.
	 * @return a list of events indicating what has changed.
	 */
	protected List<HardwareDevicePresentationModelEvent> queryForStateChanges()
	{
		ArrayList<HardwareDevicePresentationModelEvent> retList = new ArrayList<HardwareDevicePresentationModelEvent>();
		HardwareDevicePresentationModelEvent event = null;
		
		return retList;
	}
	
	/**
	 * Method used to shutdown our connection to control sub system
	 * (e.g. release components, stop threads, etc.). This should
	 * be called when the optical holodsp plugin is stopping.
	 * 
	 * @see HoloDSPDetailsPlugin
	 */
	protected void specializedStop()
	{
		// nothing to do here at the present time...
	}
	
	/**
	 * Method to release the device component, reset the variable to null, etc.
	 */
	protected synchronized void releaseDeviceComponent()
	{
		if(null != holodsp)
		{
			// TODO - if we use getComponentNonSticky, it will make the release component unnecessary...
			pluginContainerServices.releaseComponent(holoDspComponentName);
			holodsp = null;
			holoDspComponentName = null;
		}	
	}
	
	/**
	 * Method to get/set the device component reference.
	 */
	protected synchronized HardwareDevice setDeviceComponent() throws AcsJContainerServicesEx
	{
		// get the optical holodsp device name, if we haven't done so previously
		if(null == this.holoDspComponentName) 
		{
			this.holoDspComponentName = HardwareDeviceChessboardPresentationModel.
				getComponentNameForDevice(this.pluginContainerServices, antennaName, HOLODSP_DEVICE_STRING);
		}
		
		// if we haven't already gotten the holodsp component, do so now
		if(null == holodsp && null != holoDspComponentName)  
		{
			try 
			{
				// TODO - getComponentNonSticky?
				org.omg.CORBA.Object holodspObject = this.pluginContainerServices.getComponent(holoDspComponentName);
				this.holodsp = HOLODSPBaseHelper.narrow(holodspObject);
			}
			catch (AcsJContainerServicesEx ex) 
			{
				// log the error
				logger.severe("HoloDSPPresentationModel.retrieveHoloDSPComponent() - unable to obtain holodsp component: "
						+ holoDspComponentName);
				
				// set our device reference to null
				this.holodsp = null;
				
				// rethrow the exception so that the base class can take action
				throw ex;
			}
		}
		
		// return a reference to the holodsp device, or null if we couldn't get one
		return holodsp;
	}
	
}
