package alma.Control.device.gui.HoloDSP ;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import alma.Control.gui.hardwaredevice.common.HardwareDeviceChessboardDetailsPlugin;
import alma.common.gui.chessboard.ChessboardDetailsPluginListener;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

public class HoloDSPDetailsPlugin extends HardwareDeviceChessboardDetailsPlugin
{
	private final static String HOLODSP_PLUGIN_NAME = "Holography DSP: ";
	private static final int STANDARD_INSET = 5;
	private String pluginTitle;
	//private JTabbedPane tabbedPane = null;
        private HoloDSPDetailsControl controlPanel;
	
	/**
	 * TODO
	 */
	private static final long serialVersionUID = 1L;

	public HoloDSPDetailsPlugin(String antennaNames[], 
			ChessboardDetailsPluginListener listener,
			PluginContainerServices services, String pluginTitle, String selectionTitle) 
	{
		super(antennaNames, listener, services, pluginTitle, 
				selectionTitle, new HoloDSPPresentationModel(antennaNames[0], services));
		
		this.pluginTitle = pluginTitle;
		initialize();
	}
	
	/**
	 * Required method of the SubsystemPlugin interface
	 */
	public boolean runRestricted(boolean restricted) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * Required method of the SubsystemPlugin interface
	 */
	public void specializedStart() {
	}

	@Override
	public void replaceContents(String[] newAntennaNames) 
	{
		assert (null != newAntennaNames && newAntennaNames.length > 0) : "newAntennaNames array malformed";
		this.presentationModel = new HoloDSPPresentationModel(antennaNames[0], this.pluginContainerServices); 
	}

	@Override
	public String getPluginName() {
		return HOLODSP_PLUGIN_NAME;
	}

	/**
	 * Getter for the title of the plugin, which comprises the name of the plugin + potentially 
	 * some extra characters to make it unique. For example, "Holography DSP - 1"
	 * @return the pluginTitle
	 */
	public String getTitle() {
		return this.pluginTitle;
	}
		
	/*
	 * Private method to initialize the plugin.
	 */
	private void initialize()
	{
		this.setLayout(new GridBagLayout());
		JPanel topPanel = new JPanel();
		topPanel.setBorder(BorderFactory.createEmptyBorder(STANDARD_INSET, STANDARD_INSET, STANDARD_INSET, STANDARD_INSET));
                controlPanel = new HoloDSPDetailsControl(logger, (HoloDSPPresentationModel)presentationModel);
                
                GridBagConstraints constraints = new GridBagConstraints();
                constraints.gridx = 0;
                constraints.gridy = 0;
                constraints.weightx = 1.0;
                constraints.weighty = 0.0;
                constraints.ipadx = 0;
                constraints.ipady = 5;
                constraints.anchor = GridBagConstraints.NORTHWEST;
                constraints.fill = GridBagConstraints.VERTICAL;

                topPanel.add(controlPanel);
             
                this.add(topPanel, constraints);
		this.requestFocus();
		this.validate();
	}
	
}
