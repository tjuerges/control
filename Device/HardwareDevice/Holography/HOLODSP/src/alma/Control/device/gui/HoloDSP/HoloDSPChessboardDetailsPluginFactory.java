package alma.Control.device.gui.HoloDSP;

import alma.common.gui.chessboard.ChessboardDetailsPluginFactory;
import alma.common.gui.chessboard.ChessboardDetailsPlugin;
import alma.common.gui.chessboard.ChessboardDetailsPluginListener;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

public class HoloDSPChessboardDetailsPluginFactory implements ChessboardDetailsPluginFactory {

    private static final int INSTANCES = 1;

    /**
     * @see alma.common.gui.chessboard.ChessboardDetailsPluginFactory
     */
    public ChessboardDetailsPlugin[] instantiateDetailsPlugin(
            String[] names,
            PluginContainerServices containerServices,
            ChessboardDetailsPluginListener listener, String pluginTitle, String selectionTitle) {

        HoloDSPDetailsPlugin[] holodspPlugin = new HoloDSPDetailsPlugin[INSTANCES];
        if(null != names && names.length > 0) {
            holodspPlugin[0] = new HoloDSPDetailsPlugin(
                    names, listener, containerServices, pluginTitle, selectionTitle);
        }
        return holodspPlugin;
    }
}

