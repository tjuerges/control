package alma.Control.device.gui.HoloDSP;

import alma.Control.gui.hardwaredevice.common.HardwareDeviceChessboardPresentationModel;
import alma.common.gui.chessboard.ChessboardDetailsPluginFactory;
import alma.common.gui.chessboard.ChessboardPlugin;
import alma.common.gui.chessboard.ChessboardStatusProvider;

@SuppressWarnings("serial")
public class HoloDSPChessboardPlugin extends ChessboardPlugin {
        /**
         * TODO - serialVersionUID
         */
        private static final long serialVersionUID = 1L;

        /**
         * No-args constructor required by Exec's plugin framework.
         */
        public HoloDSPChessboardPlugin()
        {
        }

        @Override
        protected ChessboardDetailsPluginFactory getDetailsProvider() {
                detailsProvider = new HoloDSPChessboardDetailsPluginFactory();
                return detailsProvider;
        }

        @Override
        protected ChessboardStatusProvider getStatusProvider() {
                statusProvider = new HardwareDeviceChessboardPresentationModel(pluginContainerServices,
                                HoloDSPPresentationModel.HOLODSP_DEVICE_STRING);
                return statusProvider;
        }
}

