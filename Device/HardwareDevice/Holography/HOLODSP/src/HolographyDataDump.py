#!/usr/bin/env python
# $Id$
import sys
from optparse import OptionParser

parser = OptionParser()

parser.add_option("-a","--antenna", dest="antenna",
                  metavar="antenna",
                  default = 'PM03',
                  help="The antenna to use")

parser.add_option("-d","--subscanDuration", dest="subscanDuration",
                  metavar="subscanDuration",
                  default=60.0, type = float,
                  help="The subscan duration in seconds")

parser.add_option("-f","--filename", dest="filename",
                  metavar="filename",
                  help="The file to contain the results")

(options, args) = parser.parse_args()

import CCL.HoloDSP

hdsp = CCL.HoloDSP.HoloDSP('CONTROL/' + options.antenna + '/HoloDSP');

import Acspy.Common.TimeHelper
startTime = Acspy.Common.TimeHelper.getTimeStamp().value
startTime += long(2/100E-9)
duration = options.subscanDuration
hdsp.stopSubscan(startTime + long(duration/100E-9))
hdsp.startSubscan(startTime)
ssd = hdsp.getSubscanData(2 + duration + 2)

f = file(options.filename, "w")
print >> f, "   Time             SS           QQ           RR           QR           QS           RS    flag"
startTime = ssd.startTime
for d in ssd.holoData:
    import Acspy.Common.EpochHelper, acstime
    t = Acspy.Common.EpochHelper.EpochHelper(startTime).toString(acstime.TSArray, "%H:%M:%S.%3q", 0, 0)
    print >> f, t, "%12.0f %12.0f %12.0f %12.0f %12.0f %12.0f"%(d.ss, d.qq, d.rr, d.qr, d.qs, d.rs), d.flag
    startTime += ssd.exposureDuration

f.close()

