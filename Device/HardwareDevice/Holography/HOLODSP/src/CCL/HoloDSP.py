#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It defines the HoloDSP class that provides low level control of the
digital signal processor in holography reciever.
"""

import Acspy.Clients.SimpleClient
import Acspy.Common.QoS
import Acspy.Util.ACSCorba
import CCL.HardwareDevice

class HoloDSP(CCL.HardwareDevice.HardwareDevice):
    def __init__(self, componentName = None, stickyFlag = False):
        '''
        The HoloDSP class is a python proxy to the HoloDSP
        component. The component can be running before creating this
        proxy but if it is not it will be started i.e., the object
        reference is obtained using a call to getComponentNonSticky
        (stickyFlag = False, default) or getComponent (stickyFlag =
        True).
        
        from CCL.HoloDSP import *
        hdsp = HoloDSP("CONTROL/DA41/HoloDSP");
        if str(hdsp.getHwState()) == 'Start':
          hdsp.hwConfigure(); hdsp.hwInitialize(); hdsp.hwOperational();
        from time import time
        from TETimeUtil import *
        hdsp.startSubscan(plusTE(ceilTE(unix2epoch(time.time())), 50).value);
        hdsp.stopSubscan(plusTE(ceilTE(unix2epoch(time.time())), 230).value);
        del(hdsp);
        '''
        # initialize the base class
        CCL.HardwareDevice.HardwareDevice.__init__(self, componentName, stickyFlag);

    def __del__(self):
        CCL.HardwareDevice.HardwareDevice.__del__(self);

    def RESET_DSP(self):
        return self._HardwareDevice__hw.RESET_DSP();

    def startSubscan(self, startTime):
        return self._HardwareDevice__hw.startSubscan(startTime);

    def stopSubscan(self, stopTime):
        return self._HardwareDevice__hw.stopSubscan(stopTime);

    def getSubscanData(self, timeout):
        return self._HardwareDevice__hw.getSubscanData(timeout);

