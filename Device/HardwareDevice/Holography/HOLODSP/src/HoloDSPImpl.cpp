// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2006, 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char *rcsId="@(#) $Id$";
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <HoloDSPImpl.h>
#include <string>
#include <HolographyExceptions.h>
#include <acstimeDurationHelper.h> // for DurationHelper
#include <acstimeEpochHelper.h> // for EpochHelper
#include <loggingMACROS.h> // for AUTO_TRACE & LOG_TO_AUDIENCE
#include <TETimeUtil.h> // for TETimeUtil
#include <Time_Value.h> // for ACE_Time_Value
#include <math.h> // for MAXFLOAT

// shortcuts to avoid having to clutter the code with namespace qualifiers
using Control::HoloDSPImpl;
using std::string;
using std::ostringstream;
using log_audience::DEVELOPER;
using log_audience::OPERATOR;
using ACS::Time;
using ACS::TimeInterval;
using ControlExceptions::DeviceBusyExImpl;
using HolographyExceptions::NoSubscanExImpl;
using HolographyExceptions::SubscanAbortedExImpl;
using HolographyExceptions::NoSubscanStopExImpl;

// These values should perhaps go in the CDB

// The maximum sub-scan duration in seconds. If a user requests a sub-scan
// longer than this it is aborted to prevent the sub-scan data using up too
// much memory.
const long double maxSubscanDurationValue = 60*60;
const TimeInterval maxSubscanDurationTimeInterval =
    DurationHelper(maxSubscanDurationValue).value().value;

// How often we sample the correlation data. Its unlikely we will every want to
// change this.
const long double sampleIntervalValue = 0.012;
const TimeInterval sampleIntervalTimeInterval =
    DurationHelper(sampleIntervalValue).value().value;

// This is how often the writer thread runs (once every 20 TE's).
const TimeInterval loopTime = 20*TETimeUtil::TE_PERIOD_DURATION.value;

// time format string.
const char* timeFmt = "%H:%M:%S.%3q";

HoloDSPImpl::HoloDSPImpl(const ACE_CString & name,
			 maci::ContainerServices* cs):
    HOLODSPImplBase(name,cs),
    scanAborted_m(false),
    startTime_m(0),
    stopTime_m(0),
    deviceData_m(),
    lastScheduledTime_m(0),
    sentRequests_m(),
    rcaList_m(),
    writerThread_m(NULL)
{
    AUTO_TRACE(__func__);
    const string msg = "Initializing the scan-ended semaphore.";
    LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg, DEVELOPER);
    sem_init(&scanEndedSemaphore_m, 0, 0);
    sem_init(&scanScheduled_m, 0, 1);
}

HoloDSPImpl::~HoloDSPImpl(){
    AUTO_TRACE(__func__);
    cleanUp();
    sem_destroy(&scanScheduled_m);
    const string msg = "Destroying the scan-ended semaphore.";
    LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg, DEVELOPER);
    sem_destroy(&scanEndedSemaphore_m);
}

void HoloDSPImpl::initialize() {
    AUTO_TRACE(__func__);
    HOLODSPImplBase::initialize();

    rcaList_m.push_back(getRCA_NUM_SAMPLES());
    rcaList_m.push_back(getRCA_BAD_FRAMES_COUNT());
    rcaList_m.push_back(getRCA_QQ_0());
    rcaList_m.push_back(getRCA_QR_0());
    rcaList_m.push_back(getRCA_QS_0());
    rcaList_m.push_back(getRCA_RR_0());
    rcaList_m.push_back(getRCA_RS_0());
    rcaList_m.push_back(getRCA_SS_0());
    rcaList_m.push_back(getRCA_QQ_1());
    rcaList_m.push_back(getRCA_QR_1());
    rcaList_m.push_back(getRCA_QS_1());
    rcaList_m.push_back(getRCA_RR_1());
    rcaList_m.push_back(getRCA_RS_1());
    rcaList_m.push_back(getRCA_SS_1());
    rcaList_m.push_back(getRCA_QQ_2());
    rcaList_m.push_back(getRCA_QR_2());
    rcaList_m.push_back(getRCA_QS_2());
    rcaList_m.push_back(getRCA_RR_2());
    rcaList_m.push_back(getRCA_RS_2());
    rcaList_m.push_back(getRCA_SS_2());
    rcaList_m.push_back(getRCA_QQ_3());
    rcaList_m.push_back(getRCA_QR_3());
    rcaList_m.push_back(getRCA_QS_3());
    rcaList_m.push_back(getRCA_RR_3());
    rcaList_m.push_back(getRCA_RS_3());
    rcaList_m.push_back(getRCA_SS_3()); // This has to be the last one.
    // Its used in getSubScandata to know when to increment a counter.

    // Now create the thread. Its starts suspended.
    const CORBA::String_var compName(acscomponent::ACSComponentImpl::name());
    std::string threadName(compName.in());
    threadName += "WriterThread";
    writerThread_m = getContainerServices()->getThreadManager()->
        create<WriterThread, HoloDSPImpl>(threadName.c_str(), *this,
                                          loopTime, loopTime);
}

void HoloDSPImpl::cleanUp() {
    AUTO_TRACE(__func__);

    // Shutdown the thread
    writerThread_m->terminate();

    HOLODSPImplBase::cleanUp();
}

void HoloDSPImpl::hwStartAction() {
    AUTO_TRACE(__func__);
    HOLODSPImplBase::hwStartAction();

    // In the worst case it is possible that the scanEndedSemaphore might have
    // a count as large as 2*rcaList_m.size() (if both the real-time thread and
    // the hwStopACtion functions do a sem_post).  Ensure that as part of
    // starting up the count is at zero.
    int semMaxCount = 2 * rcaList_m.size();
    {
        const string msg = "Unlocking the scan ended semaphore.";
        LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg, DEVELOPER);
    }
    while (sem_trywait(&scanEndedSemaphore_m) == 0 && semMaxCount >= 0) {
        ostringstream msg;
        msg << "Unlocked the scan-ended semaphore. semMaxCount == "
            << semMaxCount;
        LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), DEVELOPER);
        semMaxCount--;
    }
    if (semMaxCount < 0) {
        const string msg = "Unable to clear the scan-ended semaphore.";
        LOG_TO_AUDIENCE(LM_WARNING, __func__, msg, DEVELOPER);
        // The setError function also logs a message.
        setError(msg);
    }
    scanAborted_m = false;
}

void HoloDSPImpl::hwStopAction() {
    AUTO_TRACE(__func__);
    // Move the thread to suspended mode
    writerThread_m->suspend();

    // Flush the queue
    Time actualFlushTime;
    AmbErrorCode_t status;
    flushNode(0, &actualFlushTime, &status);
    // TODO: Check the status

    if (sem_trywait(&scanScheduled_m) == -1) {
        // There is a scan scheduled so abort it
        // TODO. write the abortSubscan function.
        string msg = "Communications with the hardware was stoppped midway";
        msg += "through a sub-can. This is not currently handled.";
        LOG_TO_AUDIENCE(LM_ERROR, __func__, msg, OPERATOR);
    } else {
        sem_post(&scanScheduled_m);
    }

    // release the scanEndedSemaphore so that if a getSubscanData call is
    // pending it will release.
    for (unsigned int i = 0; i < rcaList_m.size(); i++) {
        ostringstream msg;
        msg << "Unlocking the scan-ended semaphore. Iteration = " << i;
        LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), DEVELOPER);
        sem_post(&scanEndedSemaphore_m);
    }

    HOLODSPImplBase::hwStopAction();
}

void HoloDSPImpl::startSubscan(Time startTime) {
    AUTO_TRACE(__func__);

    // Round the start time to the next TE.
    EpochHelper
        teStartTime(TETimeUtil::ceilTE(EpochHelper(startTime).value()));
    if (teStartTime.value().value != startTime) {
        ostringstream msg;
        msg << "Adjusted the requested start time of ";
        msg << TETimeUtil::toTimeString(startTime) << " to ";
        msg << TETimeUtil::toTimeString(teStartTime)
            << " as it was not on a TE boundry.";
        LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), OPERATOR);
    }

    // Check that the start time is not in the past (or too close to the
    // current time).
    EpochHelper timeNow(TETimeUtil::ceilTE
                        (TimeUtil::ace2epoch(ACE_OS::gettimeofday())));
    EpochHelper earliestStartTime(timeNow.value());
    // The lead time is the writerThread loop time + 2 TE's. TODO Quantify this
    // better.
    DurationHelper leadTime(writerThread_m->getSleepTime());
    leadTime += TETimeUtil::TE_PERIOD_DURATION;
    leadTime += TETimeUtil::TE_PERIOD_DURATION;
    earliestStartTime += leadTime.value();
    if (earliestStartTime >= teStartTime.value()) {
        ostringstream msg;
        msg << "The holography DSP component needs at least "
            << leadTime.toSeconds()
            << " seconds to schedule the sub-scan."
            << " The requested start time of this sub-scan is "
            << TETimeUtil::toTimeString(teStartTime)
            << " and the current time is " << TETimeUtil::toTimeString(timeNow)
            << " This is either in the past or too close to the current time.";
        // TODO: This should probably be a different sort of exception.
        DeviceBusyExImpl ex(__FILE__, __LINE__, __func__);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getDeviceBusyEx();
    }

    // Now check if a scan has already been scheduled.
    if (sem_trywait(&scanScheduled_m) == -1) {
        ostringstream msg;
        msg << "A sub-scan has already been scheduled."
            << " It will start at " << TETimeUtil::toTimeString(startTime_m)
            << " and stop at "
            << ((stopTime_m == 0) ?
                "an unspecified time." : TETimeUtil::toTimeString(stopTime_m))
            << " Please wait for this sub-scan to complete"
            << " before scheduling a new one.";
        DeviceBusyExImpl ex(__FILE__, __LINE__, __func__);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getDeviceBusyEx();
    }

    sentRequests_m.clear();
    deviceData_m.clear();

    startTime_m = teStartTime.value().value;
    lastScheduledTime_m = startTime_m;
    if ((stopTime_m != 0) && (startTime_m > stopTime_m)) {
        stopTime_m = 0;
    }

    // Start the thread. This must be done *after* the st{art.op}Time_m
    // data members are set.
    writerThread_m->resume();
    const string msg = "Resuming the writer thread";
    LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg, DEVELOPER);
}

void HoloDSPImpl::stopSubscan(Time stopTime) {
    AUTO_TRACE(__func__);

    EpochHelper teStopTime(TETimeUtil::ceilTE(EpochHelper(stopTime).value()));
    if (teStopTime.value().value != stopTime) {
        ostringstream msg;
        msg << "Adjusted the requested stop time of "
            << TETimeUtil::toTimeString(stopTime) << " to "
            << TETimeUtil::toTimeString(teStopTime)
            << " as it was not on a TE boundry.";
        LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), DEVELOPER);
    }

    // check that the stop time is greater than the start time
    if (teStopTime < EpochHelper(startTime_m).value()) {
        ostringstream msg;
        msg << "The requested sub-scan stop time of "
            << TETimeUtil::toTimeString(teStopTime)
            << " is less than the sub-scan start time of "
            << TETimeUtil::toTimeString(startTime_m);
        // TODO This should be a different sort of exception.
        NoSubscanExImpl ex(__FILE__, __LINE__, __func__);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getNoSubscanEx();
    }

    stopTime_m = teStopTime.value().value;
}

Control::HOLODSPImpl::HoloDeviceData*
HoloDSPImpl::getSubscanData(double timeout) {
    AUTO_TRACE(__func__);
    if (startTime_m == 0) {
        const string msg = "Subscan start time is not defined." ;
        NoSubscanExImpl ex(__FILE__, __LINE__, __func__);
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getNoSubscanEx();
    }

    if (scanAborted_m) {
        SubscanAbortedExImpl ex(__FILE__, __LINE__, __func__);
        ex.log();
        throw ex.getSubscanAbortedEx();
    }

    if (stopTime_m == 0) {
        NoSubscanStopExImpl ex(__FILE__, __LINE__, __func__);
        ex.log();
        throw ex.getNoSubscanStopEx();
    }

    // Wait for the ScanScheduled Semaphore to be unlocked.
    {
        {
            const string msg = "Trying to get the scanScheduled semaphore.";
            LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg, DEVELOPER);
        }
        ACE_Time_Value timeoutEndPoint;
        timeoutEndPoint.set(timeout);
        timeoutEndPoint += ACE_OS::gettimeofday();
        timespec ts = timeoutEndPoint;
        if (sem_timedwait(&scanScheduled_m, &ts) == -1) {
            ostringstream msg;
            msg << "Timed out waiting for the scanScheduled semaphore.";
            msg << " Timout was set at " << timeout << endl;
            // TODO This is the wrong sort of exception.
            SubscanAbortedExImpl ex(__FILE__, __LINE__, __func__);
            ex.addData("Detail", msg.str());
            ex.log();
            throw ex.getSubscanAbortedEx();
        } else {
            const string msg = "Got the scanScheduled semaphore.";
            LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg, DEVELOPER);
            // TODO. Should we release this semaphore at the end of this
            // function?
            sem_post(&scanScheduled_m);
        }
    }

    // As all monitor points have been scheduled we now just wait for the
    // scanEnded semaphore to be unlocked (24 times). Use a timeout of two
    // times the loopTime (as the writer thread schedules this far in advance)
    // and add a bit of slop. TODO. There is a potential race condition as we
    // may access the sentRequests_m queue while the thread is still updating
    // it.
    if (sentRequests_m.size() > 0) {
        const double timeout = 2*DurationHelper(loopTime).toSeconds() + .1;
        ACE_Time_Value timeoutEndPoint;
        timeoutEndPoint.set(timeout);
        timeoutEndPoint += ACE_OS::gettimeofday();
        const timespec timeoutSpec = timeoutEndPoint;
        {
            const string msg =  "Trying to get the scan-ended semaphore.";
            LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg, DEVELOPER);
        }
        for (unsigned int i = 0; i < rcaList_m.size(); i++) {
            {
                ostringstream msg;
                msg << "Waiting for the scan-ended semaphore to unlock."
                    << " Iteration " << i
                    << ". timeoutSpec = (" << timeoutSpec.tv_sec
                    << ", " << timeoutSpec.tv_nsec
                    << ") or " << timeout << " seconds for now.";
                LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), DEVELOPER);
            }

            if (sem_timedwait(&scanEndedSemaphore_m, &timeoutSpec) == -1) {
                ostringstream msg;
                msg << "Timed out waiting for the scan-ended semaphore.";
                msg << " Timout was set at " << timeout;
                // TODO This is the wrong sort of exception.
                SubscanAbortedExImpl ex(__FILE__, __LINE__, __func__);
                ex.addData("Detail", msg.str());
                ex.log();
                throw ex.getSubscanAbortedEx();
            }
        }
        {
            const string msg = "Got the scan-ended semaphore.";
            LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg, DEVELOPER);
        }
    }

    // This is the data structure that will contain the returned data.
    Control::HOLODSPImpl::HoloDeviceData_var deviceData =
        new Control::HOLODSPImpl::HoloDeviceData();
    deviceData->startTime        = startTime_m;
    deviceData->exposureDuration = sampleInterval();

    // This function empties the sentRequests queue as it transfers the
    // correlation data to the deviceData vector. But if the user calls
    // getSubscanData twice with no scan between we should not clobber the
    // deviceData vector

    if (sentRequests_m.size() > 0) {
        deviceData_m.resize(sentRequests_m.size());
        unsigned int dataIndex = 0;

        deviceData_m[0].flag = false;
        // Now copy the data
        while (sentRequests_m.size() > 0) {
            RequestStruct& thisRequest = sentRequests_m.front();
            // logging disabled as it seriously degrades performance
//             {
//                 ostringstream msg;
//                 msg << "Processing a monitor request at RCA "
//                     << thisRequest.RCA
//                     << " with a target time of "
//                     << TETimeUtil::toTimeString(thisRequest.TargetTime)
//                     << ". Returned status is " << thisRequest.Status;
//                 LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), DEVELOPER);
//             }
            if (thisRequest.Status == AMBERR_NOERR) {
                convertToWorld(thisRequest.DataLength, thisRequest.Data,
                               thisRequest.RCA, deviceData_m[dataIndex]);
                // logging disabled as it seriously degrades performance
//                 {
//                     ostringstream msg;
//                     msg << "Data timestamp "
//                         << TETimeUtil::toTimeString(thisRequest.Timestamp)
//                         << ". Data contents [";
//                     for (int i = 0; i < thisRequest.DataLength; i++) {
//                         msg << static_cast<int>(thisRequest.Data[i]);
//                         if (i == thisRequest.DataLength - 1) {
//                             msg << "]";
//                         } else {
//                             msg << ", ";
//                         }
//                     }
//                     LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), DEVELOPER);
//                 }
            } else if (thisRequest.Status != AMBERR_FLUSHED) {
                {
                    ostringstream msg;
                    msg << "Adding null data because of monitor with RCA "
                        << thisRequest.RCA;
                    LOG_TO_AUDIENCE(LM_WARNING, __func__, msg.str(), OPERATOR);
                }

                addNullData(thisRequest.RCA, deviceData_m[dataIndex]);
                {
                    ostringstream msg;
                    msg << " Error collecting data from hardware. "
                        << " Returned status " << thisRequest.Status
                        << ". RCA: " << thisRequest.RCA
                        << ". Timestamp: "
                        << TETimeUtil::toTimeString(thisRequest.Timestamp);
                    LOG_TO_AUDIENCE(LM_WARNING, __func__, msg.str(),DEVELOPER);
                }
            } else {
                // Discard this data.
                ostringstream msg;
                msg << "The monitor point scheduled to execute at "
                    << TETimeUtil::toTimeString(thisRequest.TargetTime)
                    << " was not executed (it was flushed).";
                LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), DEVELOPER);
            }
            if (static_cast<int>(thisRequest.RCA) == getRCA_SS_0() ||
                static_cast<int>(thisRequest.RCA) == getRCA_SS_1() ||
                static_cast<int>(thisRequest.RCA) == getRCA_SS_2() ||
                static_cast<int>(thisRequest.RCA) == getRCA_SS_3()) {
                dataIndex++;
                if (dataIndex != deviceData_m.size()) {
                    deviceData_m[dataIndex].flag = false;
                }
            }
            sentRequests_m.pop_front();
            if (sentRequests_m.size() == 0) {
                deviceData_m.resize(dataIndex);
            }
        }
    }

    const int numData = deviceData_m.size();
    deviceData->holoData.replace(numData,numData,
                                 &deviceData_m[0], 0);
    return deviceData._retn();
}

void HoloDSPImpl::getDeviceUniqueId(string& deviceID) {
    AUTO_TRACE(__func__);
    broadcastBasedDeviceUniqueID(deviceID);
};

void HoloDSPImpl::queueMonitors() {
    AUTO_TRACE(__func__);

    EpochHelper timeNow(TETimeUtil::ceilTE
                        (TimeUtil::ace2epoch(ACE_OS::gettimeofday())));

    EpochHelper horizon(timeNow.value());
    {
        DurationHelper lookAhead(writerThread_m->getSleepTime());
        lookAhead *= 2;
        horizon.add(lookAhead.value());
    }

    if (horizon <= EpochHelper(lastScheduledTime_m).value()) {
        const string msg ="The start time is too far in the future - waiting.";
        LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg, DEVELOPER);
        return;
    }

    if (stopTime_m == 0) {
        const string msg =
            "The subscan stop time is zero - assuming its infinite";
        LOG_TO_AUDIENCE(LM_WARNING, __func__, msg, DEVELOPER);
    }

    bool doSuspend = false;
    if ((stopTime_m != 0) &&
        (horizon >= EpochHelper(stopTime_m).value())) {
        horizon.value(stopTime_m);
        doSuspend = true;
    }

    {
        DurationHelper queueTime(horizon.difference
                                 (EpochHelper(lastScheduledTime_m).value()));
        const int numTEs =
            queueTime.value().value/TETimeUtil::TE_PERIOD_DURATION.value;
        int monitorsPerTE = rcaList_m.size();
        ostringstream msg;
        const int numRequests = numTEs * monitorsPerTE;
        msg << "Queueing " << numRequests << " monitor requests ("
            << queueTime.toSeconds() << " seconds worth)";
        LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), DEVELOPER);
    }

    RequestStruct thisRequest;
    // Need to schedule the monitor request 24ms after the TE
    thisRequest.TargetTime = lastScheduledTime_m +
        TETimeUtil::TE_PERIOD_DURATION.value/2;
    thisRequest.Status = AMBERR_PENDING;

    const Time setSemaphoreTime = stopTime_m -
        TETimeUtil::TE_PERIOD_DURATION.value;
    const Time lastTime = horizon.value().value;
    while (thisRequest.TargetTime < lastTime) {
        for (uint i = 0; i < rcaList_m.size(); i++) {
            thisRequest.RCA = rcaList_m[i];
            sentRequests_m.push_back(thisRequest);
            // As the push_back function (above) will do a copy this gets a
            // reference to the copy and this is needed as the monitorTE
            // function uses the addresses.
            RequestStruct& nextRequest = sentRequests_m.back();

            if (thisRequest.TargetTime < setSemaphoreTime) {
                monitorTE(nextRequest.TargetTime, nextRequest.RCA,
                          nextRequest.DataLength, nextRequest.Data,
                          NULL, &nextRequest.Timestamp, &nextRequest.Status);
            } else {
                // As this is the last set of monitors so set the
                // queueSemaphore_m. The queue semaphore could be set for every
                // monitor but, as the semaphore is of limited size and the
                // queue could be quite big, we will only set it for the last
                // monitor. The getSubscanData function needs to wait for this
                // last semaphore to be unlocked (24 times) by the real-time
                // threads.
                {
                    ostringstream msg;
                    msg << "Locking the scan-ended semaphore. Iteration = "<<i;
                    LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), DEVELOPER);
                }
                monitorTE(nextRequest.TargetTime, nextRequest.RCA,
                          nextRequest.DataLength, nextRequest.Data,
                          &scanEndedSemaphore_m, &nextRequest.Timestamp,
                          &nextRequest.Status);
            }
        }
        thisRequest.TargetTime += TETimeUtil::TE_PERIOD_DURATION.value;
    }

    lastScheduledTime_m = horizon.value().value;

    if (doSuspend) {
        const string msg = "Suspending the writer thread";
        LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg, DEVELOPER);
        writerThread_m -> suspend();
        sem_post(&scanScheduled_m);
        return;
    }
}

void HoloDSPImpl::convertToWorld(AmbDataLength_t length, AmbDataMem_t* data,
				 AmbRelativeAddr rca,
				 Control::HolographyData& thisData) {
    // logging disabled as it seriously degrades performance
    //  AUTO_TRACE(__func__);

    // Note no check is made to ensure the data is really 40-bits and not
    // 48-bits long. As its sign extended what follows should be OK.
    if (length == 6) {
        long long intValue = 0;
        assert(sizeof(intValue) >= 6); // this calls abort if this is false;

        // This does the sign extension as the top byte will be either zero of
        // 0xFF.
        for (unsigned int i = 0; i < (sizeof(intValue) - 6); i++) {
            intValue  = (intValue << 8) + data[0];
        }
        // This copies the data in a big/little endian independent way
        for (int i = 0; i < 6; i++) {
            intValue  = (intValue << 8) + data[i];
        }
        // If we need to scale the data it is done here.
        double doubleValue = static_cast<double>(intValue);
        if (rca == 280 || rca == 281) {
            ostringstream msg;
            msg << "Found a monitor request of length " << length
                << " with RCA " << rca;
            LOG_TO_AUDIENCE(LM_ERROR, __func__, msg.str(), DEVELOPER);
        }
        demultiplexData(doubleValue, static_cast<int>(rca), thisData);
    } else if (length == 8 && static_cast<int>(rca) == getRCA_NUM_SAMPLES()) {
        // Check that all 4 values are 600. If not flag the data. This is
        // rather inelegant.
        if (static_cast<int>(data[0]) != 2 ||
            static_cast<int>(data[1]) != 88 ||
            static_cast<int>(data[2]) != 2 ||
            static_cast<int>(data[3]) != 88 ||
            static_cast<int>(data[4]) != 2 ||
            static_cast<int>(data[5]) != 88 ||
            static_cast<int>(data[6]) != 2 ||
            static_cast<int>(data[6])) {
            thisData.flag = true;
        }
    } else if (length == 2 &&
               static_cast<int>(rca) == getRCA_BAD_FRAMES_COUNT()) {
        // Check that this is all zero and if not flag the data.
        if (static_cast<int>(data[0]) != 0 || static_cast<int>(data[1]) != 0) {
            thisData.flag = true;
        }
    } else {
        addNullData(rca, thisData);
    }
}

void HoloDSPImpl::addNullData(AmbRelativeAddr rca,
			      Control::HolographyData& thisData) {
    // logging disabled as it seriously degrades performance
    //  AUTO_TRACE(__func__);

    demultiplexData(std::numeric_limits<double>::max(),
                    static_cast<int>(rca), thisData);
    thisData.flag = true;
}

void HoloDSPImpl::demultiplexData(const double& value,
				  const int rca,
				  Control::HolographyData& thisData) {
    // logging disabled as it seriously degrades performance
    //  AUTO_TRACE(__func__);
    if (rca == getRCA_QQ_0() ||
        rca == getRCA_QQ_1() ||
        rca == getRCA_QQ_2() ||
        rca == getRCA_QQ_3()) {
        thisData.qq = value;
        return;
    }
    if (rca == getRCA_QR_0() ||
        rca == getRCA_QR_1() ||
        rca == getRCA_QR_2() ||
        rca == getRCA_QR_3()) {
        thisData.qr = value;
        return;
    }
    if (rca == getRCA_QS_0() ||
        rca == getRCA_QS_1() ||
        rca == getRCA_QS_2() ||
        rca == getRCA_QS_3()) {
        thisData.qs = value;
        return;
    }
    if (rca == getRCA_RR_0() ||
        rca == getRCA_RR_1() ||
        rca == getRCA_RR_2() ||
        rca == getRCA_RR_3()) {
        thisData.rr = value;
        return;
    }
    if (rca == getRCA_RS_0() ||
        rca == getRCA_RS_1() ||
        rca == getRCA_RS_2() ||
        rca == getRCA_RS_3()) {
        thisData.rs = value;
        return;
    }
    if (rca == getRCA_SS_0() ||
        rca == getRCA_SS_1() ||
        rca == getRCA_SS_2() ||
        rca == getRCA_SS_3()) {
        thisData.ss = value;
        return;
    }

    ostringstream msg;
    msg << "Unrecognized RCA (" << rca << ") found - ignoring it.";
    LOG_TO_AUDIENCE(LM_WARNING, __func__, msg.str(), DEVELOPER);
}

TimeInterval HoloDSPImpl::maxSubscanDuration() {
    return maxSubscanDurationTimeInterval;
}

TimeInterval HoloDSPImpl::sampleInterval() {
    return sampleIntervalTimeInterval;
}

HoloDSPImpl::WriterThread::
WriterThread(const ACE_CString& name, HoloDSPImpl& holoDSP,
	     const TimeInterval responseTime,
	     const TimeInterval sleepTime):
    ACS::Thread(name, responseTime, sleepTime),
    holoDSP_m(holoDSP)
{}

void HoloDSPImpl::WriterThread::runLoop() {
    holoDSP_m.queueMonitors();
}

/////////////////////////////////////////////////
// MACI DLL support functions
/////////////////////////////////////////////////
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(HoloDSPImpl)
