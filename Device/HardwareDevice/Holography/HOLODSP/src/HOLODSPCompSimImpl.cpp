
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * WARNING! DO NOT MODIFY THIS FILE!
 *  ---------------------------------------------------------
 * | This is generated code!  Do not modify this file.       |
 * | Any changes will be lost when the file is re-generated. |
 *  ---------------------------------------------------------
 *
 * File HOLODSPCompSimImpl.cpp
 */

#include "HOLODSPCompSimImpl.h"
#include <HolographyDSP.h>
#include <maciACSComponentDefines.h>

using namespace maci;

/* Please use this class to implement alternative component, extending
* the HOLODSPCompSimBase class. */

HOLODSPCompSimImpl::HOLODSPCompSimImpl(const ACE_CString& name, ContainerServices* pCS)
	: HOLODSPCompSimBase(name,pCS)
{
	const char* __METHOD__ = "HOLODSPCompSimImpl::HOLODSPCompSimImpl";
	ACS_TRACE(__METHOD__);

	std::vector<CAN::byte_t> sn(8,0x00);
	AMB::node_t node = 0x00;

	simulationIf_m.setSimObj(new AMB::HolographyDSP(node,sn));
}

/* --------------- [ MACI DLL support functions ] -----------------*/
MACI_DLL_SUPPORT_FUNCTIONS(HOLODSPCompSimImpl)
