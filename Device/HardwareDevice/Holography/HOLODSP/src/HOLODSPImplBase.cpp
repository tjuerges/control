/*
 * $Id$
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 */

#include "HOLODSPImplBase.h"

#include <TETimeUtil.h>

using std::vector;
/////////////////
// Constructor
/////////////////
HOLODSPImplBase::HOLODSPImplBase(const ACE_CString& name, 
    maci::ContainerServices* cs)
    :AmbDeviceImpl(name, cs)

        , QQ_0_scale(1), QQ_0_offset(0)

		, QQ_0_RCA(0x00100)

        , QR_0_scale(1), QR_0_offset(0)

		, QR_0_RCA(0X00101)

        , QS_0_scale(1), QS_0_offset(0)

		, QS_0_RCA(0x00102)

        , RR_0_scale(1), RR_0_offset(0)

		, RR_0_RCA(0x00103)

        , RS_0_scale(1), RS_0_offset(0)

		, RS_0_RCA(0x00104)

        , SS_0_scale(1), SS_0_offset(0)

		, SS_0_RCA(0x00105)

        , QQ_1_scale(1), QQ_1_offset(0)

		, QQ_1_RCA(0x00106)

        , QR_1_scale(1), QR_1_offset(0)

		, QR_1_RCA(0x00107)

        , QS_1_scale(1), QS_1_offset(0)

		, QS_1_RCA(0x00108)

        , RR_1_scale(1), RR_1_offset(0)

		, RR_1_RCA(0x00109)

        , RS_1_scale(1), RS_1_offset(0)

		, RS_1_RCA(0x0010a)

        , SS_1_scale(1), SS_1_offset(0)

		, SS_1_RCA(0x0010b)

        , QQ_2_scale(1), QQ_2_offset(0)

		, QQ_2_RCA(0x0010c)

        , QR_2_scale(1), QR_2_offset(0)

		, QR_2_RCA(0x0010d)

        , QS_2_scale(1), QS_2_offset(0)

		, QS_2_RCA(0x0010e)

        , RR_2_scale(1), RR_2_offset(0)

		, RR_2_RCA(0x0010f)

        , RS_2_scale(1), RS_2_offset(0)

		, RS_2_RCA(0x00110)

        , SS_2_scale(1), SS_2_offset(0)

		, SS_2_RCA(0x00111)

        , QQ_3_scale(1), QQ_3_offset(0)

		, QQ_3_RCA(0x00112)

        , QR_3_scale(1), QR_3_offset(0)

		, QR_3_RCA(0x00113)

        , QS_3_scale(1), QS_3_offset(0)

		, QS_3_RCA(0x00114)

        , RR_3_scale(1), RR_3_offset(0)

		, RR_3_RCA(0x00115)

        , RS_3_scale(1), RS_3_offset(0)

		, RS_3_RCA(0x00116)

        , SS_3_scale(1), SS_3_offset(0)

		, SS_3_RCA(0x00117)

		, NUM_SAMPLES_RCA(0x00118)

		, BAD_FRAMES_COUNT_RCA(0x00119)

		, DSP_TIMER_REGISTER_RCA(0x0011a)

        , RESET_DSP_RCA(0x01001)

{
  ACS_TRACE("HOLODSPImplBase::HOLODSPImplBase");
}

////////////////
// Destructor
////////////////
HOLODSPImplBase::~HOLODSPImplBase() {
	ACS_TRACE("HOLODSPImplBase:~HOLODSPImplBase");
}

/////////////////////////////
// Lifecycle Methods
/////////////////////////////
void HOLODSPImplBase::initialize() {
    ACS_TRACE("HOLODSPImplBase::initialize");

    // Call the base class implementation so it can set up the common properties
    AmbDeviceImpl::initialize();

    // create the properties specific to the HOLODSP
    try { 
        const ACE_CString nameWithSep = cdbName_m + ":";

    } catch (ControlExceptions::CDBErrorExImpl& ex) { 
        // getElement can throw this exception
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            "HOLODSPImpl::startInitialize");
    }
}

///////////////////////////////////
// MonitorPoint: QQ_0 
//
// Get the QQ component of a holography data point. Every nth component 
// is the result of one 12ms integration. There are 4 integration intervals 
// (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
// extended to 48 bits)
///////////////////////////////////

/**
 * Get the current value of QQ_0 from the device.
 */

long long HOLODSPImplBase::get_QQ_0(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_QQ_0()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(QQ_0_RCA);
    AmbDataLength_t length(6);

    unsigned long long raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_QQ_0()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    long long rtn =  convert_QQ_0(raw);

	return rtn;
}

/**
 * Convert the raw value to a world value for this device.
 */
long long HOLODSPImplBase::convert_QQ_0(unsigned long long raw) {

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    	char * p = (char *)&raw;

    	char tmp = p[0]; p[0] = p[5]; p[5] = tmp;
    	tmp = p[1]; p[1] = p[4]; p[4] = tmp;
    	tmp = p[2]; p[2] = p[3]; p[3] = tmp;

    #endif    	

    return raw;

}

///////////////////////////////////
// MonitorPoint: QR_0 
//
// Get the QQ component of a holography data point. Every nth component 
// is the result of one 12ms integration. There are 4 integration intervals 
// (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
// extended to 48 bits)
///////////////////////////////////

/**
 * Get the current value of QR_0 from the device.
 */

long long HOLODSPImplBase::get_QR_0(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_QR_0()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(QR_0_RCA);
    AmbDataLength_t length(6);

    unsigned long long raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_QR_0()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    long long rtn =  convert_QR_0(raw);

	return rtn;
}

/**
 * Convert the raw value to a world value for this device.
 */
long long HOLODSPImplBase::convert_QR_0(unsigned long long raw) {

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    	char * p = (char *)&raw;

    	char tmp = p[0]; p[0] = p[5]; p[5] = tmp;
    	tmp = p[1]; p[1] = p[4]; p[4] = tmp;
    	tmp = p[2]; p[2] = p[3]; p[3] = tmp;

    #endif    	

    return raw;

}

///////////////////////////////////
// MonitorPoint: QS_0 
//
// Get the QQ component of a holography data point. Every nth component 
// is the result of one 12ms integration. There are 4 integration intervals 
// (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
// extended to 48 bits)
///////////////////////////////////

/**
 * Get the current value of QS_0 from the device.
 */

long long HOLODSPImplBase::get_QS_0(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_QS_0()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(QS_0_RCA);
    AmbDataLength_t length(6);

    unsigned long long raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_QS_0()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    long long rtn =  convert_QS_0(raw);

	return rtn;
}

/**
 * Convert the raw value to a world value for this device.
 */
long long HOLODSPImplBase::convert_QS_0(unsigned long long raw) {

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    	char * p = (char *)&raw;

    	char tmp = p[0]; p[0] = p[5]; p[5] = tmp;
    	tmp = p[1]; p[1] = p[4]; p[4] = tmp;
    	tmp = p[2]; p[2] = p[3]; p[3] = tmp;

    #endif    	

    return raw;

}

///////////////////////////////////
// MonitorPoint: RR_0 
//
// Get the QQ component of a holography data point. Every nth component 
// is the result of one 12ms integration. There are 4 integration intervals 
// (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
// extended to 48 bits)
///////////////////////////////////

/**
 * Get the current value of RR_0 from the device.
 */

long long HOLODSPImplBase::get_RR_0(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_RR_0()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(RR_0_RCA);
    AmbDataLength_t length(6);

    unsigned long long raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_RR_0()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    long long rtn =  convert_RR_0(raw);

	return rtn;
}

/**
 * Convert the raw value to a world value for this device.
 */
long long HOLODSPImplBase::convert_RR_0(unsigned long long raw) {

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    	char * p = (char *)&raw;

    	char tmp = p[0]; p[0] = p[5]; p[5] = tmp;
    	tmp = p[1]; p[1] = p[4]; p[4] = tmp;
    	tmp = p[2]; p[2] = p[3]; p[3] = tmp;

    #endif    	

    return raw;

}

///////////////////////////////////
// MonitorPoint: RS_0 
//
// Get the QQ component of a holography data point. Every nth component 
// is the result of one 12ms integration. There are 4 integration intervals 
// (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
// extended to 48 bits)
///////////////////////////////////

/**
 * Get the current value of RS_0 from the device.
 */

long long HOLODSPImplBase::get_RS_0(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_RS_0()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(RS_0_RCA);
    AmbDataLength_t length(6);

    unsigned long long raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_RS_0()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    long long rtn =  convert_RS_0(raw);

	return rtn;
}

/**
 * Convert the raw value to a world value for this device.
 */
long long HOLODSPImplBase::convert_RS_0(unsigned long long raw) {

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    	char * p = (char *)&raw;

    	char tmp = p[0]; p[0] = p[5]; p[5] = tmp;
    	tmp = p[1]; p[1] = p[4]; p[4] = tmp;
    	tmp = p[2]; p[2] = p[3]; p[3] = tmp;

    #endif    	

    return raw;

}

///////////////////////////////////
// MonitorPoint: SS_0 
//
// Get the QQ component of a holography data point. Every nth component 
// is the result of one 12ms integration. There are 4 integration intervals 
// (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
// extended to 48 bits)
///////////////////////////////////

/**
 * Get the current value of SS_0 from the device.
 */

long long HOLODSPImplBase::get_SS_0(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_SS_0()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(SS_0_RCA);
    AmbDataLength_t length(6);

    unsigned long long raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_SS_0()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    long long rtn =  convert_SS_0(raw);

	return rtn;
}

/**
 * Convert the raw value to a world value for this device.
 */
long long HOLODSPImplBase::convert_SS_0(unsigned long long raw) {

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    	char * p = (char *)&raw;

    	char tmp = p[0]; p[0] = p[5]; p[5] = tmp;
    	tmp = p[1]; p[1] = p[4]; p[4] = tmp;
    	tmp = p[2]; p[2] = p[3]; p[3] = tmp;

    #endif    	

    return raw;

}

///////////////////////////////////
// MonitorPoint: QQ_1 
//
// Get the QQ component of a holography data point. Every nth component 
// is the result of one 12ms integration. There are 4 integration intervals 
// (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
// extended to 48 bits)
///////////////////////////////////

/**
 * Get the current value of QQ_1 from the device.
 */

long long HOLODSPImplBase::get_QQ_1(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_QQ_1()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(QQ_1_RCA);
    AmbDataLength_t length(6);

    unsigned long long raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_QQ_1()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    long long rtn =  convert_QQ_1(raw);

	return rtn;
}

/**
 * Convert the raw value to a world value for this device.
 */
long long HOLODSPImplBase::convert_QQ_1(unsigned long long raw) {

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    	char * p = (char *)&raw;

    	char tmp = p[0]; p[0] = p[5]; p[5] = tmp;
    	tmp = p[1]; p[1] = p[4]; p[4] = tmp;
    	tmp = p[2]; p[2] = p[3]; p[3] = tmp;

    #endif    	

    return raw;

}

///////////////////////////////////
// MonitorPoint: QR_1 
//
// Get the QQ component of a holography data point. Every nth component 
// is the result of one 12ms integration. There are 4 integration intervals 
// (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
// extended to 48 bits)
///////////////////////////////////

/**
 * Get the current value of QR_1 from the device.
 */

long long HOLODSPImplBase::get_QR_1(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_QR_1()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(QR_1_RCA);
    AmbDataLength_t length(6);

    unsigned long long raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_QR_1()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    long long rtn =  convert_QR_1(raw);

	return rtn;
}

/**
 * Convert the raw value to a world value for this device.
 */
long long HOLODSPImplBase::convert_QR_1(unsigned long long raw) {

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    	char * p = (char *)&raw;

    	char tmp = p[0]; p[0] = p[5]; p[5] = tmp;
    	tmp = p[1]; p[1] = p[4]; p[4] = tmp;
    	tmp = p[2]; p[2] = p[3]; p[3] = tmp;

    #endif    	

    return raw;

}

///////////////////////////////////
// MonitorPoint: QS_1 
//
// Get the QQ component of a holography data point. Every nth component 
// is the result of one 12ms integration. There are 4 integration intervals 
// (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
// extended to 48 bits)
///////////////////////////////////

/**
 * Get the current value of QS_1 from the device.
 */

long long HOLODSPImplBase::get_QS_1(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_QS_1()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(QS_1_RCA);
    AmbDataLength_t length(6);

    unsigned long long raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_QS_1()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    long long rtn =  convert_QS_1(raw);

	return rtn;
}

/**
 * Convert the raw value to a world value for this device.
 */
long long HOLODSPImplBase::convert_QS_1(unsigned long long raw) {

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    	char * p = (char *)&raw;

    	char tmp = p[0]; p[0] = p[5]; p[5] = tmp;
    	tmp = p[1]; p[1] = p[4]; p[4] = tmp;
    	tmp = p[2]; p[2] = p[3]; p[3] = tmp;

    #endif    	

    return raw;

}

///////////////////////////////////
// MonitorPoint: RR_1 
//
// Get the QQ component of a holography data point. Every nth component 
// is the result of one 12ms integration. There are 4 integration intervals 
// (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
// extended to 48 bits)
///////////////////////////////////

/**
 * Get the current value of RR_1 from the device.
 */

long long HOLODSPImplBase::get_RR_1(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_RR_1()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(RR_1_RCA);
    AmbDataLength_t length(6);

    unsigned long long raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_RR_1()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    long long rtn =  convert_RR_1(raw);

	return rtn;
}

/**
 * Convert the raw value to a world value for this device.
 */
long long HOLODSPImplBase::convert_RR_1(unsigned long long raw) {

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    	char * p = (char *)&raw;

    	char tmp = p[0]; p[0] = p[5]; p[5] = tmp;
    	tmp = p[1]; p[1] = p[4]; p[4] = tmp;
    	tmp = p[2]; p[2] = p[3]; p[3] = tmp;

    #endif    	

    return raw;

}

///////////////////////////////////
// MonitorPoint: RS_1 
//
// Get the QQ component of a holography data point. Every nth component 
// is the result of one 12ms integration. There are 4 integration intervals 
// (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
// extended to 48 bits)
///////////////////////////////////

/**
 * Get the current value of RS_1 from the device.
 */

long long HOLODSPImplBase::get_RS_1(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_RS_1()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(RS_1_RCA);
    AmbDataLength_t length(6);

    unsigned long long raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_RS_1()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    long long rtn =  convert_RS_1(raw);

	return rtn;
}

/**
 * Convert the raw value to a world value for this device.
 */
long long HOLODSPImplBase::convert_RS_1(unsigned long long raw) {

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    	char * p = (char *)&raw;

    	char tmp = p[0]; p[0] = p[5]; p[5] = tmp;
    	tmp = p[1]; p[1] = p[4]; p[4] = tmp;
    	tmp = p[2]; p[2] = p[3]; p[3] = tmp;

    #endif    	

    return raw;

}

///////////////////////////////////
// MonitorPoint: SS_1 
//
// Get the QQ component of a holography data point. Every nth component 
// is the result of one 12ms integration. There are 4 integration intervals 
// (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
// extended to 48 bits)
///////////////////////////////////

/**
 * Get the current value of SS_1 from the device.
 */

long long HOLODSPImplBase::get_SS_1(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_SS_1()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(SS_1_RCA);
    AmbDataLength_t length(6);

    unsigned long long raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_SS_1()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    long long rtn =  convert_SS_1(raw);

	return rtn;
}

/**
 * Convert the raw value to a world value for this device.
 */
long long HOLODSPImplBase::convert_SS_1(unsigned long long raw) {

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    	char * p = (char *)&raw;

    	char tmp = p[0]; p[0] = p[5]; p[5] = tmp;
    	tmp = p[1]; p[1] = p[4]; p[4] = tmp;
    	tmp = p[2]; p[2] = p[3]; p[3] = tmp;

    #endif    	

    return raw;

}

///////////////////////////////////
// MonitorPoint: QQ_2 
//
// Get the QQ component of a holography data point. Every nth component 
// is the result of one 12ms integration. There are 4 integration intervals 
// (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
// extended to 48 bits)
///////////////////////////////////

/**
 * Get the current value of QQ_2 from the device.
 */

long long HOLODSPImplBase::get_QQ_2(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_QQ_2()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(QQ_2_RCA);
    AmbDataLength_t length(6);

    unsigned long long raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_QQ_2()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    long long rtn =  convert_QQ_2(raw);

	return rtn;
}

/**
 * Convert the raw value to a world value for this device.
 */
long long HOLODSPImplBase::convert_QQ_2(unsigned long long raw) {

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    	char * p = (char *)&raw;

    	char tmp = p[0]; p[0] = p[5]; p[5] = tmp;
    	tmp = p[1]; p[1] = p[4]; p[4] = tmp;
    	tmp = p[2]; p[2] = p[3]; p[3] = tmp;

    #endif    	

    return raw;

}

///////////////////////////////////
// MonitorPoint: QR_2 
//
// Get the QQ component of a holography data point. Every nth component 
// is the result of one 12ms integration. There are 4 integration intervals 
// (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
// extended to 48 bits)
///////////////////////////////////

/**
 * Get the current value of QR_2 from the device.
 */

long long HOLODSPImplBase::get_QR_2(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_QR_2()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(QR_2_RCA);
    AmbDataLength_t length(6);

    unsigned long long raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_QR_2()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    long long rtn =  convert_QR_2(raw);

	return rtn;
}

/**
 * Convert the raw value to a world value for this device.
 */
long long HOLODSPImplBase::convert_QR_2(unsigned long long raw) {

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    	char * p = (char *)&raw;

    	char tmp = p[0]; p[0] = p[5]; p[5] = tmp;
    	tmp = p[1]; p[1] = p[4]; p[4] = tmp;
    	tmp = p[2]; p[2] = p[3]; p[3] = tmp;

    #endif    	

    return raw;

}

///////////////////////////////////
// MonitorPoint: QS_2 
//
// Get the QQ component of a holography data point. Every nth component 
// is the result of one 12ms integration. There are 4 integration intervals 
// (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
// extended to 48 bits)
///////////////////////////////////

/**
 * Get the current value of QS_2 from the device.
 */

long long HOLODSPImplBase::get_QS_2(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_QS_2()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(QS_2_RCA);
    AmbDataLength_t length(6);

    unsigned long long raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_QS_2()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    long long rtn =  convert_QS_2(raw);

	return rtn;
}

/**
 * Convert the raw value to a world value for this device.
 */
long long HOLODSPImplBase::convert_QS_2(unsigned long long raw) {

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    	char * p = (char *)&raw;

    	char tmp = p[0]; p[0] = p[5]; p[5] = tmp;
    	tmp = p[1]; p[1] = p[4]; p[4] = tmp;
    	tmp = p[2]; p[2] = p[3]; p[3] = tmp;

    #endif    	

    return raw;

}

///////////////////////////////////
// MonitorPoint: RR_2 
//
// Get the QQ component of a holography data point. Every nth component 
// is the result of one 12ms integration. There are 4 integration intervals 
// (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
// extended to 48 bits)
///////////////////////////////////

/**
 * Get the current value of RR_2 from the device.
 */

long long HOLODSPImplBase::get_RR_2(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_RR_2()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(RR_2_RCA);
    AmbDataLength_t length(6);

    unsigned long long raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_RR_2()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    long long rtn =  convert_RR_2(raw);

	return rtn;
}

/**
 * Convert the raw value to a world value for this device.
 */
long long HOLODSPImplBase::convert_RR_2(unsigned long long raw) {

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    	char * p = (char *)&raw;

    	char tmp = p[0]; p[0] = p[5]; p[5] = tmp;
    	tmp = p[1]; p[1] = p[4]; p[4] = tmp;
    	tmp = p[2]; p[2] = p[3]; p[3] = tmp;

    #endif    	

    return raw;

}

///////////////////////////////////
// MonitorPoint: RS_2 
//
// Get the QQ component of a holography data point. Every nth component 
// is the result of one 12ms integration. There are 4 integration intervals 
// (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
// extended to 48 bits)
///////////////////////////////////

/**
 * Get the current value of RS_2 from the device.
 */

long long HOLODSPImplBase::get_RS_2(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_RS_2()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(RS_2_RCA);
    AmbDataLength_t length(6);

    unsigned long long raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_RS_2()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    long long rtn =  convert_RS_2(raw);

	return rtn;
}

/**
 * Convert the raw value to a world value for this device.
 */
long long HOLODSPImplBase::convert_RS_2(unsigned long long raw) {

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    	char * p = (char *)&raw;

    	char tmp = p[0]; p[0] = p[5]; p[5] = tmp;
    	tmp = p[1]; p[1] = p[4]; p[4] = tmp;
    	tmp = p[2]; p[2] = p[3]; p[3] = tmp;

    #endif    	

    return raw;

}

///////////////////////////////////
// MonitorPoint: SS_2 
//
// Get the QQ component of a holography data point. Every nth component 
// is the result of one 12ms integration. There are 4 integration intervals 
// (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
// extended to 48 bits)
///////////////////////////////////

/**
 * Get the current value of SS_2 from the device.
 */

long long HOLODSPImplBase::get_SS_2(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_SS_2()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(SS_2_RCA);
    AmbDataLength_t length(6);

    unsigned long long raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_SS_2()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    long long rtn =  convert_SS_2(raw);

	return rtn;
}

/**
 * Convert the raw value to a world value for this device.
 */
long long HOLODSPImplBase::convert_SS_2(unsigned long long raw) {

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    	char * p = (char *)&raw;

    	char tmp = p[0]; p[0] = p[5]; p[5] = tmp;
    	tmp = p[1]; p[1] = p[4]; p[4] = tmp;
    	tmp = p[2]; p[2] = p[3]; p[3] = tmp;

    #endif    	

    return raw;

}

///////////////////////////////////
// MonitorPoint: QQ_3 
//
// Get the QQ component of a holography data point. Every nth component 
// is the result of one 12ms integration. There are 4 integration intervals 
// (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
// extended to 48 bits)
///////////////////////////////////

/**
 * Get the current value of QQ_3 from the device.
 */

long long HOLODSPImplBase::get_QQ_3(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_QQ_3()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(QQ_3_RCA);
    AmbDataLength_t length(6);

    unsigned long long raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_QQ_3()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    long long rtn =  convert_QQ_3(raw);

	return rtn;
}

/**
 * Convert the raw value to a world value for this device.
 */
long long HOLODSPImplBase::convert_QQ_3(unsigned long long raw) {

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    	char * p = (char *)&raw;

    	char tmp = p[0]; p[0] = p[5]; p[5] = tmp;
    	tmp = p[1]; p[1] = p[4]; p[4] = tmp;
    	tmp = p[2]; p[2] = p[3]; p[3] = tmp;

    #endif    	

    return raw;

}

///////////////////////////////////
// MonitorPoint: QR_3 
//
// Get the QQ component of a holography data point. Every nth component 
// is the result of one 12ms integration. There are 4 integration intervals 
// (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
// extended to 48 bits)
///////////////////////////////////

/**
 * Get the current value of QR_3 from the device.
 */

long long HOLODSPImplBase::get_QR_3(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_QR_3()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(QR_3_RCA);
    AmbDataLength_t length(6);

    unsigned long long raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_QR_3()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    long long rtn =  convert_QR_3(raw);

	return rtn;
}

/**
 * Convert the raw value to a world value for this device.
 */
long long HOLODSPImplBase::convert_QR_3(unsigned long long raw) {

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    	char * p = (char *)&raw;

    	char tmp = p[0]; p[0] = p[5]; p[5] = tmp;
    	tmp = p[1]; p[1] = p[4]; p[4] = tmp;
    	tmp = p[2]; p[2] = p[3]; p[3] = tmp;

    #endif    	

    return raw;

}

///////////////////////////////////
// MonitorPoint: QS_3 
//
// Get the QQ component of a holography data point. Every nth component 
// is the result of one 12ms integration. There are 4 integration intervals 
// (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
// extended to 48 bits)
///////////////////////////////////

/**
 * Get the current value of QS_3 from the device.
 */

long long HOLODSPImplBase::get_QS_3(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_QS_3()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(QS_3_RCA);
    AmbDataLength_t length(6);

    unsigned long long raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_QS_3()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    long long rtn =  convert_QS_3(raw);

	return rtn;
}

/**
 * Convert the raw value to a world value for this device.
 */
long long HOLODSPImplBase::convert_QS_3(unsigned long long raw) {

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    	char * p = (char *)&raw;

    	char tmp = p[0]; p[0] = p[5]; p[5] = tmp;
    	tmp = p[1]; p[1] = p[4]; p[4] = tmp;
    	tmp = p[2]; p[2] = p[3]; p[3] = tmp;

    #endif    	

    return raw;

}

///////////////////////////////////
// MonitorPoint: RR_3 
//
// Get the QQ component of a holography data point. Every nth component 
// is the result of one 12ms integration. There are 4 integration intervals 
// (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
// extended to 48 bits)
///////////////////////////////////

/**
 * Get the current value of RR_3 from the device.
 */

long long HOLODSPImplBase::get_RR_3(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_RR_3()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(RR_3_RCA);
    AmbDataLength_t length(6);

    unsigned long long raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_RR_3()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    long long rtn =  convert_RR_3(raw);

	return rtn;
}

/**
 * Convert the raw value to a world value for this device.
 */
long long HOLODSPImplBase::convert_RR_3(unsigned long long raw) {

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    	char * p = (char *)&raw;

    	char tmp = p[0]; p[0] = p[5]; p[5] = tmp;
    	tmp = p[1]; p[1] = p[4]; p[4] = tmp;
    	tmp = p[2]; p[2] = p[3]; p[3] = tmp;

    #endif    	

    return raw;

}

///////////////////////////////////
// MonitorPoint: RS_3 
//
// Get the QQ component of a holography data point. Every nth component 
// is the result of one 12ms integration. There are 4 integration intervals 
// (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
// extended to 48 bits)
///////////////////////////////////

/**
 * Get the current value of RS_3 from the device.
 */

long long HOLODSPImplBase::get_RS_3(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_RS_3()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(RS_3_RCA);
    AmbDataLength_t length(6);

    unsigned long long raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_RS_3()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    long long rtn =  convert_RS_3(raw);

	return rtn;
}

/**
 * Convert the raw value to a world value for this device.
 */
long long HOLODSPImplBase::convert_RS_3(unsigned long long raw) {

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    	char * p = (char *)&raw;

    	char tmp = p[0]; p[0] = p[5]; p[5] = tmp;
    	tmp = p[1]; p[1] = p[4]; p[4] = tmp;
    	tmp = p[2]; p[2] = p[3]; p[3] = tmp;

    #endif    	

    return raw;

}

///////////////////////////////////
// MonitorPoint: SS_3 
//
// Get the QQ component of a holography data point. Every nth component 
// is the result of one 12ms integration. There are 4 integration intervals 
// (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
// extended to 48 bits)
///////////////////////////////////

/**
 * Get the current value of SS_3 from the device.
 */

long long HOLODSPImplBase::get_SS_3(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_SS_3()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(SS_3_RCA);
    AmbDataLength_t length(6);

    unsigned long long raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_SS_3()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    long long rtn =  convert_SS_3(raw);

	return rtn;
}

/**
 * Convert the raw value to a world value for this device.
 */
long long HOLODSPImplBase::convert_SS_3(unsigned long long raw) {

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    	char * p = (char *)&raw;

    	char tmp = p[0]; p[0] = p[5]; p[5] = tmp;
    	tmp = p[1]; p[1] = p[4]; p[4] = tmp;
    	tmp = p[2]; p[2] = p[3]; p[3] = tmp;

    #endif    	

    return raw;

}

///////////////////////////////////
// MonitorPoint: NUM_SAMPLES 
//
// Get the number of samples used for each 12 ms integration cycle in 
// the previous 48 ms period.  Number of samples in each 12ms integration 
// cycle. Each 16 bit word represents the number of samples from the 
// corresponding integration cycle. uint16[0] is the number of samples 
// in the first set of integrations and so on. Normally these numbers 
// should be identical for every cycle.
///////////////////////////////////

/**
 * Get the current value of NUM_SAMPLES from the device.
 */

vector<unsigned short> HOLODSPImplBase::get_NUM_SAMPLES(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_NUM_SAMPLES()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(NUM_SAMPLES_RCA);
    AmbDataLength_t length(8);

    unsigned short raw[4];

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_NUM_SAMPLES()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    vector<unsigned short> rtn(4);

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    for (int i = 0; i < 4; ++i) {
    	char * p = (char *)&(raw[i]);

    	char tmp = p[0]; p[0] = p[1]; p[1] = tmp;

    }

    #endif    	

	for (int i = 0; i < 4; ++i)
		rtn[i] = raw[i];

	return rtn;
}

///////////////////////////////////
// MonitorPoint: BAD_FRAMES_COUNT 
//
// Return the number of bad data frames received from the DSP.  Number 
// of bad frames received from the DSP since last time this monitor point 
// was queried. Every time this point is queried, the number of bad frames 
// is returned and the count is cleared.
///////////////////////////////////

/**
 * Get the current value of BAD_FRAMES_COUNT from the device.
 */

unsigned short HOLODSPImplBase::get_BAD_FRAMES_COUNT(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_BAD_FRAMES_COUNT()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(BAD_FRAMES_COUNT_RCA);
    AmbDataLength_t length(2);

    unsigned short raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_BAD_FRAMES_COUNT()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    	char * p = (char *)&raw;

    	char tmp = p[0]; p[0] = p[1]; p[1] = tmp;

    #endif    	

    unsigned short rtn = raw;

	return rtn;
}

///////////////////////////////////
// MonitorPoint: DSP_TIMER_REGISTER 
//
// Return the content of the DSP timer register when the 48 ms interrupt 
// occurs. It should always be the same value.
///////////////////////////////////

/**
 * Get the current value of DSP_TIMER_REGISTER from the device.
 */

unsigned short HOLODSPImplBase::get_DSP_TIMER_REGISTER(Time & timestamp) {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_DSP_TIMER_REGISTER()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(DSP_TIMER_REGISTER_RCA);
    AmbDataLength_t length(2);

    unsigned short raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw, 
        &synchLock, &timestamp, &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLODSPImplBase::get_DSP_TIMER_REGISTER()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    #if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    	char * p = (char *)&raw;

    	char tmp = p[0]; p[0] = p[1]; p[1] = tmp;

    #endif    	

    unsigned short rtn = raw;

	return rtn;
}

/**
 * Control Point: RESET_DSP
 *
 * Send reset pulse to digital signal processor. Wait at least one full 
48ms cycle before monitoring DSP data.
 */
void HOLODSPImplBase::RESET_DSP() {
    AmbRelativeAddr  rca(RESET_DSP_RCA);
    AmbDataLength_t length(1);

    unsigned char raw;

    raw = 0xff;

    Time timestamp;
    AmbErrorCode_t status;
    sem_t synchLock;
    sem_init(&synchLock,0,0);
    command(rca, length, (AmbDataMem_t *)&raw, &synchLock, &timestamp, 
	    &status); 
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = 
	  ControlExceptions::CAMBErrorExImpl(__FILE__, __LINE__, 
					     "HOLODSPImplBase::RESET_DSP()");
        ex.addData("ErrorCode",status);
	// I cannot throw the exception as its not in the declaration. So I'll
	// log it instead.
	ex.log();
	// throw ex.getCAMBErrorEx();
    }
}
