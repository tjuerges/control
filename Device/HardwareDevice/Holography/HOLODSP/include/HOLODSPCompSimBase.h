/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * WARNING! DO NOT MODIFY THIS FILE!
 *  ---------------------------------------------------------
 * | This is generated code!  Do not modify this file.       |
 * | Any changes will be lost when the file is re-generated. |
 *  ---------------------------------------------------------
 *
 * File HOLODSPCompSimBase.h
 */

#ifndef HOLODSPCompSimBase_H
#define HOLODSPCompSimBase_H

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <AmbSimulationInt.h>
#include "HOLODSPCompSimBaseS.h"
#include <HoloDSPImpl.h>

/**
 * The HOLODSPCompSimBase class is the local component for the
 * HOLODSP Unit hardware simulator.
 * <ul>
 * <li> Device:   HOLODSP Unit
 * <li> Assembly: HOLODSP
 * <li> Parent:   root
 * <li> Node:     0x1
 * <li> Channel:  2
 * </ul>
 */
class HOLODSPCompSimBase : public Control::HoloDSPImpl,
	public virtual POA_Control::HOLODSPCompSimBase
{
  public:
	HOLODSPCompSimBase(const ACE_CString& name, maci::ContainerServices* pCS);

	virtual ~HOLODSPCompSimBase();

	virtual void setSimValue(CORBA::Long rca, const Control::HOLODSPCompSimBase::LongSeq& data)
		throw(CORBA::SystemException, ControlExceptions::SimErrorEx);

	virtual void monitor(
			AmbRelativeAddr		RCA,
			AmbDataLength_t&	dataLength, 
			AmbDataMem_t*		data,
			sem_t*				synchLock,
			Time*				timestamp,
			AmbErrorCode_t*		status);

	virtual void command(
			AmbRelativeAddr		RCA,
			AmbDataLength_t		dataLength,
			const AmbDataMem_t*	data,
			sem_t*				synchLock,
			Time*				timestamp,
			AmbErrorCode_t*		status);

	virtual void monitorTE(
			ACS::Time			TimeEvent,
			AmbRelativeAddr		RCA,
			AmbDataLength_t&	dataLength, 
			AmbDataMem_t*		data,
			sem_t*				synchLock,
			Time*				timestamp,
			AmbErrorCode_t*		status);

	virtual void commandTE(
			ACS::Time			TimeEvent,
			AmbRelativeAddr		RCA,
			AmbDataLength_t		dataLength, 
			const AmbDataMem_t*	data,
			sem_t*				synchLock,
			Time*				timestamp,
			AmbErrorCode_t*		status);

	virtual void monitorNextTE(
			AmbRelativeAddr		RCA,
			AmbDataLength_t&	dataLength, 
			AmbDataMem_t*		data,
			sem_t*				synchLock,
			Time*				timestamp,
			AmbErrorCode_t*		status);

	virtual void commandNextTE(
			AMBSystem::AmbRelativeAddr	RCA,
			AmbDataLength_t				dataLength, 
			const AmbDataMem_t*			data,
			sem_t*						synchLock,
			ACS::Time*					timestamp,
			AmbErrorCode_t*				status);

	virtual void flushNode(
			ACS::Time		TimeEvent,
			ACS::Time*		timestamp,
			AmbErrorCode_t*	status);

	virtual void flushRCA(
			ACS::Time		TimeEvent,
			AmbRelativeAddr	RCA,
			ACS::Time*		timestamp,
			AmbErrorCode_t*	status);

  protected:
	virtual void getAmbInterfaceInstance() {};
	virtual void getDeviceUniqueId(std::string& deviceID)
		throw (ControlExceptions::CAMBErrorExImpl);

	AmbSimulationInt simulationIf_m;

  private:
	/// Copy and assignment are not allowed
	HOLODSPCompSimBase(const HOLODSPCompSimBase&);
	HOLODSPCompSimBase& operator = (const HOLODSPCompSimBase&);
};

#endif /* HOLODSPCompSimBase_H */
