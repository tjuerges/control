#ifndef HOLODSPIMPL_H
#define HOLODSPIMPL_H
// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2006, 2009
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

// Base class(es)
#include <HOLODSPImplBase.h>

//CORBA servant header
#include <HOLODSPImplS.h>

// Forward declarations for classes that this component uses

//includes for data members
#include <semaphore.h>
#include <vector>
#include <deque>
#include <acsThread.h>

namespace Control {
    class HoloDSPImpl : public HOLODSPImplBase,
                        virtual public POA_Control::HOLODSPImpl
    {
    public:
        // ------------------- Constructor & Destructor -------------------

        /// The constructor for any ACS C++ component must have this signature
        HoloDSPImpl(const ACE_CString & name, maci::ContainerServices* cs);
    
        /// The destructor does nothing special. It must be virtual because
        /// this class contains virtual functions.
        virtual ~HoloDSPImpl();
    
        // --------------------- Component LifeCycle interface -------------
    
        /// This creates the writer thread. Its starts suspended and is
        /// activated by the startSubScan method.
        /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
        virtual void initialize();

        /// This terminates the writer thread (and in normal cases it should
        /// already have been suspended).
        /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
        virtual void cleanUp();
  
        // --------------------- Hardware LifeCycle interface -------------

        /// This ensures that the scanEnded semaphore is reset
        virtual void hwStartAction();

        /// This suspends the writer thread, flushes the monitor queue, and
        /// releases (posts) the scanEnded semaphore.
        virtual void hwStopAction();

        // --------------------- CORBA interface --------------------------

        /// See the IDL file for a description of this function.
        /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
        /// \exception ControlExceptions::DeviceBusyEx
        virtual void startSubscan(ACS::Time startTime);
    
        /// See the IDL file for a description of this function.
        /// \exception HolographyExceptions::NoSubscanEx
        /// \exception ControlDeviceExceptions::HwLifecycleEx
        virtual void stopSubscan(ACS::Time stopTime);

        /// See the IDL file for a description of this function.
        /// \exception HolographyExceptions::NoSubscanStopEx
        /// \exception HolographyExceptions::NoSubscanEx
        /// \exception HolographyExceptions::SubscanAbortedEx
        virtual Control::HOLODSPImpl::HoloDeviceData* 
        getSubscanData(double waitTime);

        /// The current implementation of HoloDSP has an old style AMBSI-1
        /// which does not respond to node requests at RCA 0.  So we use the
        /// broadcast based getNodeID method.
        /// \exception ControlExceptions::CAMBErrorExImpl
        virtual void getDeviceUniqueId(std::string& deviceID);

        // --------------------- C++ interface --------------------------

        /// returns the maximum allowed subscan duration. Subscans longer than
        /// this should be aborted automtically
        static ACS::TimeInterval maxSubscanDuration();

        /// The sampling period of for getting the correlation data.
        static ACS::TimeInterval sampleInterval();

    protected:
        // These data members are used by the simulation subclass and hence
        // need to be protected.

        // Flag which is set when a subscan is Aborted
        bool scanAborted_m;

        // Start time of the current subscan 
        ACS::Time startTime_m;

        // Stop time of the current subscan
        ACS::Time stopTime_m;

        // Memory block where data from the last subscan is stored after a call
        // to getSubscanData has been made. Before then the data is still in
        // the sentrequests queue.
        std::vector<Control::HolographyData> deviceData_m;

    private:
        // This method will do the monitoring necessary to extract the
        // correlation data. It is run by the writer thread periodically (every
        // second) and queues the monitoring (for the next two seconds).
        void queueMonitors();

        // Convert the specified correlation data from an integer to a double,
        // and add it to the right field in the specified data structure. If
        // there is a problem with the integer data, like it not being 6-bytes
        // long, then addNullData is called.
        void convertToWorld(AmbDataLength_t length, AmbDataMem_t* data, 
                            AmbRelativeAddr rca, 
                            Control::HolographyData& thisData);

        // Add an undefined (but currently very large) data value to the device
        // data. The flag is also set to true.
        void addNullData(AmbRelativeAddr rca,
                         Control::HolographyData& thisData);

        // Copy the specified value to the right field in the
        // Control::HolographyData structure. This function contains the
        // mapping between RCA's and fields.
        void demultiplexData(const double& value, const int rca,
                             Control::HolographyData& thisData);

        // This semaphore is set, by a real-time thread, when the last monitor
        // request in a sub-scan has been done.
        sem_t scanEndedSemaphore_m;

        // Semaphore which is set, by the startSubscan method, when a sub-scan
        // is scheduled. It is released, by the writer thread, when all the
        // monitor requests have been sent (but not necessarily executed).
        sem_t scanScheduled_m;
    
        // Time of the last monitor request sent by the writer thread.
        ACS::Time lastScheduledTime_m;

        // This data structure contains all the relevant information for the a
        // queued monitor request that is used to extract the flagging
        // information.
        struct RequestStruct {
            AmbRelativeAddr RCA;
            ACS::Time       TargetTime;
            AmbDataLength_t DataLength;
            AmbDataMem_t    Data[8];
            ACS::Time       Timestamp;
            AmbErrorCode_t  Status;
        };

        // This queue contains all the monitor requests that have been sent by
        // the writer thread that have not yet been processed ie., the data has
        // not been moved to the deviceData vector. Note this *cannot* be a
        // std::vector container as a vector, if it grows big enough to force a
        // reallocation, will invalidate all pointers to individual
        // elements. These pointers are passed, via the monitorTE function into
        // the real-time software and if they are invalidated the software will
        // crash.
        std::deque<HoloDSPImpl::RequestStruct> sentRequests_m;

        // This is a list of all the RCA's that need to be monitored every TE
        // to get the Holography data from the DSP.
        std::vector<AmbRelativeAddr> rcaList_m;
  
        // This thread wakes up every second and queues more monitor requests
        // to the real-time layers.
        class WriterThread: public ACS::Thread {
        public:
            WriterThread(const ACE_CString& name, HoloDSPImpl& holoDSP,
                         const ACS::TimeInterval responseTime,
                         const ACS::TimeInterval sleepTime);
            virtual void runLoop();
        private:
            HoloDSPImpl& holoDSP_m;
        };

        // The instance of the thread object
        WriterThread* writerThread_m;

    }; // end HoloDSPImpl class
} // end Control namespace
#endif // HOLODSPIMPL_H
