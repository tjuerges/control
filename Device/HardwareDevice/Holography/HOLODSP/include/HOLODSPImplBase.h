/*
 * $Id$
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 */
#ifndef HOLODSPImplBase_CLASS
#define HOLODSPImplBase_CLASS

// Base class(es)
#include <ambDeviceImpl.h>
#include <baciDevIO.h>
#include <acserr.h>
#include <baciROdouble.h>
#include <baciROfloat.h>
#include <baciROlong.h>
#include <acstimeS.h>

// CORBA servant header generated from IDL.
#include "HOLODSPBaseS.h"

// Forward declarations for classes that this component uses
template<class T> class DevIO;
class ACS::ROdouble;
class ACS::ROfloat;
class ACS::ROlong;
class ROdouble_ptr;
class ROfloat_ptr;
class ROlong_ptr;

// Includes needed for data members.
#include <baciSmartPropertyPointer.h>

/**
 * The HOLODSPBase class is the base class for the DSP.
 * <ul> 
 * <li> Device:   DSP
 * <li> Assembly: HOLODSP
 * <li> Parent:   Holography
 * <li> Node:     0x1d
 * <li> Channel:  1
 * </ul>
 * <p>
 * Holography data products.  A holography data point consists of six 
 * 40-bit words composed of integrated products of pairs of the three 
 * signals named S, Q, and R. The products are designated in this document 
 * by QQ, QR, QS, RR, RS, and SS. Each product comprises the data portion 
 * of a single CAN message, and thus six CAN messages are required to 
 * transmit a single holography data point. Another CAN message is required 
 * to transmit the number of samples in the integration period.  The 
 * holography receiver will always obtain 4 data points per timing interval, 
 * and these can be read during the following timing interval. Any that 
 * are not read during that interval will be overwritten.
 */
class HOLODSPImplBase: 
        public AmbDeviceImpl, 
        public virtual POA_Control::HOLODSPBase {
// _________________________________________________________________
//|
//|           PUBLIC members of class HOLODSPImplBase
//|_________________________________________________________________
public:

    /**
     * Constructor
    */
    HOLODSPImplBase(const ACE_CString & name, maci::ContainerServices* cs);

    /**
     * Destructor
     */
    virtual ~HOLODSPImplBase();

    /** 
     * Overriden from the component lifecycle interface.
     */
    virtual void initialize();

    /// Send reset pulse to digital signal processor. Wait at least one full
    /// 48ms cycle before monitoring DSP data.
    /// \exception ControlDeviceExceptions::HwLifecycleEx
    virtual void RESET_DSP();

    /**
     * Get the RCA for this control point.
     */
    virtual int getRCA_RESET_DSP() const {
        return RESET_DSP_RCA;
    }
    /**
     * Override the RCA for this control point.
     */
    virtual void setRCA_RESET_DSP(int rca) {
        RESET_DSP_RCA = rca;
    }

// _________________________________________________________________
//|
//|           PROTECTED members of class HOLODSPImplBase
//|_________________________________________________________________
protected:

    ///////////////////////////////////
    // MonitorPoint: QQ_0 
    //
    // Get the QQ component of a holography data point. Every nth component 
    // is the result of one 12ms integration. There are 4 integration intervals 
    // (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
    // extended to 48 bits)
    ///////////////////////////////////

    /// Get the current value of QQ_0 from the device.
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp
    virtual long long get_QQ_0(Time & timestamp);

    /**
     * Convert the raw value to a world value.
     */
    virtual long long convert_QQ_0(unsigned long long raw);

    /**
     * Override the default conversion factors: scale and offset.
     */
    virtual void setConversion_QQ_0(double scale, double offset) {
        QQ_0_scale = scale;
        QQ_0_offset = offset;
    }    

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_QQ_0() const {
        return QQ_0_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_QQ_0(int rca) {
        QQ_0_RCA = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: QR_0 
    //
    // Get the QQ component of a holography data point. Every nth component 
    // is the result of one 12ms integration. There are 4 integration intervals 
    // (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
    // extended to 48 bits)
    ///////////////////////////////////

    /// Get the current value of QR_0 from the device.
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp
    virtual long long get_QR_0(Time & timestamp);

    /**
     * Convert the raw value to a world value.
     */
    virtual long long convert_QR_0(unsigned long long raw);

    /**
     * Override the default conversion factors: scale and offset.
     */
    virtual void setConversion_QR_0(double scale, double offset) {
        QR_0_scale = scale;
        QR_0_offset = offset;
    }    

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_QR_0() const {
        return QR_0_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_QR_0(int rca) {
        QR_0_RCA = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: QS_0 
    //
    // Get the QQ component of a holography data point. Every nth component 
    // is the result of one 12ms integration. There are 4 integration intervals 
    // (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
    // extended to 48 bits)
    ///////////////////////////////////

    /**
     * Get the current value of QS_0 from the device.
     */
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp

    virtual long long get_QS_0(Time & timestamp);

    /**
     * Convert the raw value to a world value.
     */
    virtual long long convert_QS_0(unsigned long long raw);

    /**
     * Override the default conversion factors: scale and offset.
     */
    virtual void setConversion_QS_0(double scale, double offset) {
        QS_0_scale = scale;
        QS_0_offset = offset;
    }    

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_QS_0() const {
        return QS_0_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_QS_0(int rca) {
        QS_0_RCA = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: RR_0 
    //
    // Get the QQ component of a holography data point. Every nth component 
    // is the result of one 12ms integration. There are 4 integration intervals 
    // (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
    // extended to 48 bits)
    ///////////////////////////////////

    /// Get the current value of RR_0 from the device.
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp
    virtual long long get_RR_0(Time & timestamp);

    /**
     * Convert the raw value to a world value.
     */
    virtual long long convert_RR_0(unsigned long long raw);

    /**
     * Override the default conversion factors: scale and offset.
     */
    virtual void setConversion_RR_0(double scale, double offset) {
        RR_0_scale = scale;
        RR_0_offset = offset;
    }    

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_RR_0() const {
        return RR_0_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_RR_0(int rca) {
        RR_0_RCA = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: RS_0 
    //
    // Get the QQ component of a holography data point. Every nth component 
    // is the result of one 12ms integration. There are 4 integration intervals 
    // (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
    // extended to 48 bits)
    ///////////////////////////////////

    /// Get the current value of RS_0 from the device.
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp
    virtual long long get_RS_0(Time & timestamp);

    /**
     * Convert the raw value to a world value.
     */
    virtual long long convert_RS_0(unsigned long long raw);

    /**
     * Override the default conversion factors: scale and offset.
     */
    virtual void setConversion_RS_0(double scale, double offset) {
        RS_0_scale = scale;
        RS_0_offset = offset;
    }    

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_RS_0() const {
        return RS_0_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_RS_0(int rca) {
        RS_0_RCA = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: SS_0 
    //
    // Get the QQ component of a holography data point. Every nth component 
    // is the result of one 12ms integration. There are 4 integration intervals 
    // (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
    // extended to 48 bits)
    ///////////////////////////////////

    /// Get the current value of SS_0 from the device.
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp
    virtual long long get_SS_0(Time & timestamp);

    /**
     * Convert the raw value to a world value.
     */
    virtual long long convert_SS_0(unsigned long long raw);

    /**
     * Override the default conversion factors: scale and offset.
     */
    virtual void setConversion_SS_0(double scale, double offset) {
        SS_0_scale = scale;
        SS_0_offset = offset;
    }    

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_SS_0() const {
        return SS_0_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_SS_0(int rca) {
        SS_0_RCA = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: QQ_1 
    //
    // Get the QQ component of a holography data point. Every nth component 
    // is the result of one 12ms integration. There are 4 integration intervals 
    // (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
    // extended to 48 bits)
    ///////////////////////////////////

    /// Get the current value of QQ_1 from the device.
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp
    virtual long long get_QQ_1(Time & timestamp);

    /**
     * Convert the raw value to a world value.
     */
    virtual long long convert_QQ_1(unsigned long long raw);

    /**
     * Override the default conversion factors: scale and offset.
     */
    virtual void setConversion_QQ_1(double scale, double offset) {
        QQ_1_scale = scale;
        QQ_1_offset = offset;
    }    

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_QQ_1() const {
        return QQ_1_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_QQ_1(int rca) {
        QQ_1_RCA = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: QR_1 
    //
    // Get the QQ component of a holography data point. Every nth component 
    // is the result of one 12ms integration. There are 4 integration intervals 
    // (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
    // extended to 48 bits)
    ///////////////////////////////////

    /// Get the current value of QR_1 from the device.
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp
    virtual long long get_QR_1(Time & timestamp);

    /**
     * Convert the raw value to a world value.
     */
    virtual long long convert_QR_1(unsigned long long raw);

    /**
     * Override the default conversion factors: scale and offset.
     */
    virtual void setConversion_QR_1(double scale, double offset) {
        QR_1_scale = scale;
        QR_1_offset = offset;
    }    

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_QR_1() const {
        return QR_1_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_QR_1(int rca) {
        QR_1_RCA = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: QS_1 
    //
    // Get the QQ component of a holography data point. Every nth component 
    // is the result of one 12ms integration. There are 4 integration intervals 
    // (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
    // extended to 48 bits)
    ///////////////////////////////////

    /// Get the current value of QS_1 from the device.
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp
    virtual long long get_QS_1(Time & timestamp);

    /**
     * Convert the raw value to a world value.
     */
    virtual long long convert_QS_1(unsigned long long raw);

    /**
     * Override the default conversion factors: scale and offset.
     */
    virtual void setConversion_QS_1(double scale, double offset) {
        QS_1_scale = scale;
        QS_1_offset = offset;
    }    

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_QS_1() const {
        return QS_1_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_QS_1(int rca) {
        QS_1_RCA = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: RR_1 
    //
    // Get the QQ component of a holography data point. Every nth component 
    // is the result of one 12ms integration. There are 4 integration intervals 
    // (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
    // extended to 48 bits)
    ///////////////////////////////////

    /// Get the current value of RR_1 from the device.
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp
    virtual long long get_RR_1(Time & timestamp);

    /**
     * Convert the raw value to a world value.
     */
    virtual long long convert_RR_1(unsigned long long raw);

    /**
     * Override the default conversion factors: scale and offset.
     */
    virtual void setConversion_RR_1(double scale, double offset) {
        RR_1_scale = scale;
        RR_1_offset = offset;
    }    

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_RR_1() const {
        return RR_1_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_RR_1(int rca) {
        RR_1_RCA = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: RS_1 
    //
    // Get the QQ component of a holography data point. Every nth component 
    // is the result of one 12ms integration. There are 4 integration intervals 
    // (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
    // extended to 48 bits)
    ///////////////////////////////////

    /// Get the current value of RS_1 from the device.
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp
    virtual long long get_RS_1(Time & timestamp);

    /**
     * Convert the raw value to a world value.
     */
    virtual long long convert_RS_1(unsigned long long raw);

    /**
     * Override the default conversion factors: scale and offset.
     */
    virtual void setConversion_RS_1(double scale, double offset) {
        RS_1_scale = scale;
        RS_1_offset = offset;
    }    

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_RS_1() const {
        return RS_1_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_RS_1(int rca) {
        RS_1_RCA = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: SS_1 
    //
    // Get the QQ component of a holography data point. Every nth component 
    // is the result of one 12ms integration. There are 4 integration intervals 
    // (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
    // extended to 48 bits)
    ///////////////////////////////////

    /// Get the current value of SS_1 from the device.
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp
    virtual long long get_SS_1(Time & timestamp);

    /**
     * Convert the raw value to a world value.
     */
    virtual long long convert_SS_1(unsigned long long raw);

    /**
     * Override the default conversion factors: scale and offset.
     */
    virtual void setConversion_SS_1(double scale, double offset) {
        SS_1_scale = scale;
        SS_1_offset = offset;
    }    

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_SS_1() const {
        return SS_1_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_SS_1(int rca) {
        SS_1_RCA = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: QQ_2 
    //
    // Get the QQ component of a holography data point. Every nth component 
    // is the result of one 12ms integration. There are 4 integration intervals 
    // (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
    // extended to 48 bits)
    ///////////////////////////////////

    /// Get the current value of QQ_2 from the device.
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp
    virtual long long get_QQ_2(Time & timestamp);

    /**
     * Convert the raw value to a world value.
     */
    virtual long long convert_QQ_2(unsigned long long raw);

    /**
     * Override the default conversion factors: scale and offset.
     */
    virtual void setConversion_QQ_2(double scale, double offset) {
        QQ_2_scale = scale;
        QQ_2_offset = offset;
    }    

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_QQ_2() const {
        return QQ_2_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_QQ_2(int rca) {
        QQ_2_RCA = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: QR_2 
    //
    // Get the QQ component of a holography data point. Every nth component 
    // is the result of one 12ms integration. There are 4 integration intervals 
    // (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
    // extended to 48 bits)
    ///////////////////////////////////

    /// Get the current value of QR_2 from the device.
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp
    virtual long long get_QR_2(Time & timestamp);

    /**
     * Convert the raw value to a world value.
     */
    virtual long long convert_QR_2(unsigned long long raw);

    /**
     * Override the default conversion factors: scale and offset.
     */
    virtual void setConversion_QR_2(double scale, double offset) {
        QR_2_scale = scale;
        QR_2_offset = offset;
    }    

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_QR_2() const {
        return QR_2_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_QR_2(int rca) {
        QR_2_RCA = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: QS_2 
    //
    // Get the QQ component of a holography data point. Every nth component 
    // is the result of one 12ms integration. There are 4 integration intervals 
    // (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
    // extended to 48 bits)
    ///////////////////////////////////

    /// Get the current value of QS_2 from the device.
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp
    virtual long long get_QS_2(Time & timestamp);

    /**
     * Convert the raw value to a world value.
     */
    virtual long long convert_QS_2(unsigned long long raw);

    /**
     * Override the default conversion factors: scale and offset.
     */
    virtual void setConversion_QS_2(double scale, double offset) {
        QS_2_scale = scale;
        QS_2_offset = offset;
    }    

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_QS_2() const {
        return QS_2_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_QS_2(int rca) {
        QS_2_RCA = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: RR_2 
    //
    // Get the QQ component of a holography data point. Every nth component 
    // is the result of one 12ms integration. There are 4 integration intervals 
    // (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
    // extended to 48 bits)
    ///////////////////////////////////

    /// Get the current value of RR_2 from the device.
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp
    virtual long long get_RR_2(Time & timestamp);

    /**
     * Convert the raw value to a world value.
     */
    virtual long long convert_RR_2(unsigned long long raw);

    /**
     * Override the default conversion factors: scale and offset.
     */
    virtual void setConversion_RR_2(double scale, double offset) {
        RR_2_scale = scale;
        RR_2_offset = offset;
    }    

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_RR_2() const {
        return RR_2_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_RR_2(int rca) {
        RR_2_RCA = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: RS_2 
    //
    // Get the QQ component of a holography data point. Every nth component 
    // is the result of one 12ms integration. There are 4 integration intervals 
    // (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
    // extended to 48 bits)
    ///////////////////////////////////

    /// Get the current value of RS_2 from the device.
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp
    virtual long long get_RS_2(Time & timestamp);

    /**
     * Convert the raw value to a world value.
     */
    virtual long long convert_RS_2(unsigned long long raw);

    /**
     * Override the default conversion factors: scale and offset.
     */
    virtual void setConversion_RS_2(double scale, double offset) {
        RS_2_scale = scale;
        RS_2_offset = offset;
    }    

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_RS_2() const {
        return RS_2_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_RS_2(int rca) {
        RS_2_RCA = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: SS_2 
    //
    // Get the QQ component of a holography data point. Every nth component 
    // is the result of one 12ms integration. There are 4 integration intervals 
    // (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
    // extended to 48 bits)
    ///////////////////////////////////

    /// Get the current value of SS_2 from the device.
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp
    virtual long long get_SS_2(Time & timestamp);

    /**
     * Convert the raw value to a world value.
     */
    virtual long long convert_SS_2(unsigned long long raw);

    /**
     * Override the default conversion factors: scale and offset.
     */
    virtual void setConversion_SS_2(double scale, double offset) {
        SS_2_scale = scale;
        SS_2_offset = offset;
    }    

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_SS_2() const {
        return SS_2_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_SS_2(int rca) {
        SS_2_RCA = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: QQ_3 
    //
    // Get the QQ component of a holography data point. Every nth component 
    // is the result of one 12ms integration. There are 4 integration intervals 
    // (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
    // extended to 48 bits)
    ///////////////////////////////////

    /// Get the current value of QQ_3 from the device.
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp
    virtual long long get_QQ_3(Time & timestamp);

    /**
     * Convert the raw value to a world value.
     */
    virtual long long convert_QQ_3(unsigned long long raw);

    /**
     * Override the default conversion factors: scale and offset.
     */
    virtual void setConversion_QQ_3(double scale, double offset) {
        QQ_3_scale = scale;
        QQ_3_offset = offset;
    }    

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_QQ_3() const {
        return QQ_3_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_QQ_3(int rca) {
        QQ_3_RCA = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: QR_3 
    //
    // Get the QQ component of a holography data point. Every nth component 
    // is the result of one 12ms integration. There are 4 integration intervals 
    // (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
    // extended to 48 bits)
    ///////////////////////////////////

    /// Get the current value of QR_3 from the device.
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp
    virtual long long get_QR_3(Time & timestamp);

    /**
     * Convert the raw value to a world value.
     */
    virtual long long convert_QR_3(unsigned long long raw);

    /**
     * Override the default conversion factors: scale and offset.
     */
    virtual void setConversion_QR_3(double scale, double offset) {
        QR_3_scale = scale;
        QR_3_offset = offset;
    }    

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_QR_3() const {
        return QR_3_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_QR_3(int rca) {
        QR_3_RCA = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: QS_3 
    //
    // Get the QQ component of a holography data point. Every nth component 
    // is the result of one 12ms integration. There are 4 integration intervals 
    // (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
    // extended to 48 bits)
    ///////////////////////////////////

    /// Get the current value of QS_3 from the device.
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp
    virtual long long get_QS_3(Time & timestamp);

    /**
     * Convert the raw value to a world value.
     */
    virtual long long convert_QS_3(unsigned long long raw);

    /**
     * Override the default conversion factors: scale and offset.
     */
    virtual void setConversion_QS_3(double scale, double offset) {
        QS_3_scale = scale;
        QS_3_offset = offset;
    }    

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_QS_3() const {
        return QS_3_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_QS_3(int rca) {
        QS_3_RCA = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: RR_3 
    //
    // Get the QQ component of a holography data point. Every nth component 
    // is the result of one 12ms integration. There are 4 integration intervals 
    // (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
    // extended to 48 bits)
    ///////////////////////////////////

    /// Get the current value of RR_3 from the device.
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp
    virtual long long get_RR_3(Time & timestamp);

    /**
     * Convert the raw value to a world value.
     */
    virtual long long convert_RR_3(unsigned long long raw);

    /**
     * Override the default conversion factors: scale and offset.
     */
    virtual void setConversion_RR_3(double scale, double offset) {
        RR_3_scale = scale;
        RR_3_offset = offset;
    }    

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_RR_3() const {
        return RR_3_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_RR_3(int rca) {
        RR_3_RCA = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: RS_3 
    //
    // Get the QQ component of a holography data point. Every nth component 
    // is the result of one 12ms integration. There are 4 integration intervals 
    // (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
    // extended to 48 bits)
    ///////////////////////////////////

    /// Get the current value of RS_3 from the device.
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp
    virtual long long get_RS_3(Time & timestamp);

    /**
     * Convert the raw value to a world value.
     */
    virtual long long convert_RS_3(unsigned long long raw);

    /**
     * Override the default conversion factors: scale and offset.
     */
    virtual void setConversion_RS_3(double scale, double offset) {
        RS_3_scale = scale;
        RS_3_offset = offset;
    }    

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_RS_3() const {
        return RS_3_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_RS_3(int rca) {
        RS_3_RCA = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: SS_3 
    //
    // Get the QQ component of a holography data point. Every nth component 
    // is the result of one 12ms integration. There are 4 integration intervals 
    // (cycles) for each 48 ms timing event.  6 bytes: (40-bit integer sign 
    // extended to 48 bits)
    ///////////////////////////////////

    /// Get the current value of SS_3 from the device.
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp
    virtual long long get_SS_3(Time & timestamp);

    /**
     * Convert the raw value to a world value.
     */
    virtual long long convert_SS_3(unsigned long long raw);

    /**
     * Override the default conversion factors: scale and offset.
     */
    virtual void setConversion_SS_3(double scale, double offset) {
        SS_3_scale = scale;
        SS_3_offset = offset;
    }    

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_SS_3() const {
        return SS_3_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_SS_3(int rca) {
        SS_3_RCA = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: NUM_SAMPLES 
    //
    // Get the number of samples used for each 12 ms integration cycle in 
    // the previous 48 ms period.  Number of samples in each 12ms integration 
    // cycle. Each 16 bit word represents the number of samples from the 
    // corresponding integration cycle. uint16[0] is the number of samples 
    // in the first set of integrations and so on. Normally these numbers 
    // should be identical for every cycle.
    ///////////////////////////////////

    /// Get the current value of NUM_SAMPLES from the device.
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp
    virtual std::vector<unsigned short> get_NUM_SAMPLES(Time & timestamp);

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_NUM_SAMPLES() const {
        return NUM_SAMPLES_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_NUM_SAMPLES(int rca) {
        NUM_SAMPLES_RCA = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: BAD_FRAMES_COUNT 
    //
    // Return the number of bad data frames received from the DSP.  Number 
    // of bad frames received from the DSP since last time this monitor point 
    // was queried. Every time this point is queried, the number of bad frames 
    // is returned and the count is cleared.
    ///////////////////////////////////

    /// Get the current value of BAD_FRAMES_COUNT from the device.
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp
    virtual unsigned short get_BAD_FRAMES_COUNT(Time & timestamp);

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_BAD_FRAMES_COUNT() const {
        return BAD_FRAMES_COUNT_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_BAD_FRAMES_COUNT(int rca) {
        BAD_FRAMES_COUNT_RCA = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: DSP_TIMER_REGISTER 
    //
    // Return the content of the DSP timer register when the 48 ms interrupt 
    // occurs. It should always be the same value.
    ///////////////////////////////////

    /// Get the current value of DSP_TIMER_REGISTER from the device.
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImp
    virtual unsigned short get_DSP_TIMER_REGISTER(Time & timestamp);

    /**
     * Get the RCA for this monitor point.
     */
    virtual int getRCA_DSP_TIMER_REGISTER() const {
        return DSP_TIMER_REGISTER_RCA;
    }
    /**
     * Override the RCA for this monitor point.
     */
    virtual void setRCA_DSP_TIMER_REGISTER(int rca) {
        DSP_TIMER_REGISTER_RCA = rca;
    }

    ////////////////////////////////////////////////////////////////
    // Class definitions needed to implemented the BACI properties. 
    ////////////////////////////////////////////////////////////////

// _________________________________________________________________
//|
//|           PRIVATE members of class HOLODSPImplBase
//|_________________________________________________________________
private:

    // The copy constructor is made private to prevent a compiler generated
    // one from being used. It is not implemented.
    HOLODSPImplBase(const HOLODSPImplBase& other);

    ///////////////////////////////////
    // MonitorPoint: QQ_0 
    ///////////////////////////////////

    /**
     * The scaling factor for converting data from raw to world.
     */
    double QQ_0_scale;
    /**
     * The scaling factor for converting data from raw to world.
     */
    double QQ_0_offset;

    /**
     * The relative can address for the property.
     */
    int QQ_0_RCA;

    ///////////////////////////////////
    // MonitorPoint: QR_0 
    ///////////////////////////////////

    /**
     * The scaling factor for converting data from raw to world.
     */
    double QR_0_scale;
    /**
     * The scaling factor for converting data from raw to world.
     */
    double QR_0_offset;

    /**
     * The relative can address for the property.
     */
    int QR_0_RCA;

    ///////////////////////////////////
    // MonitorPoint: QS_0 
    ///////////////////////////////////

    /**
     * The scaling factor for converting data from raw to world.
     */
    double QS_0_scale;
    /**
     * The scaling factor for converting data from raw to world.
     */
    double QS_0_offset;

    /**
     * The relative can address for the property.
     */
    int QS_0_RCA;

    ///////////////////////////////////
    // MonitorPoint: RR_0 
    ///////////////////////////////////

    /**
     * The scaling factor for converting data from raw to world.
     */
    double RR_0_scale;
    /**
     * The scaling factor for converting data from raw to world.
     */
    double RR_0_offset;

    /**
     * The relative can address for the property.
     */
    int RR_0_RCA;

    ///////////////////////////////////
    // MonitorPoint: RS_0 
    ///////////////////////////////////

    /**
     * The scaling factor for converting data from raw to world.
     */
    double RS_0_scale;
    /**
     * The scaling factor for converting data from raw to world.
     */
    double RS_0_offset;

    /**
     * The relative can address for the property.
     */
    int RS_0_RCA;

    ///////////////////////////////////
    // MonitorPoint: SS_0 
    ///////////////////////////////////

    /**
     * The scaling factor for converting data from raw to world.
     */
    double SS_0_scale;
    /**
     * The scaling factor for converting data from raw to world.
     */
    double SS_0_offset;

    /**
     * The relative can address for the property.
     */
    int SS_0_RCA;

    ///////////////////////////////////
    // MonitorPoint: QQ_1 
    ///////////////////////////////////

    /**
     * The scaling factor for converting data from raw to world.
     */
    double QQ_1_scale;
    /**
     * The scaling factor for converting data from raw to world.
     */
    double QQ_1_offset;

    /**
     * The relative can address for the property.
     */
    int QQ_1_RCA;

    ///////////////////////////////////
    // MonitorPoint: QR_1 
    ///////////////////////////////////

    /**
     * The scaling factor for converting data from raw to world.
     */
    double QR_1_scale;
    /**
     * The scaling factor for converting data from raw to world.
     */
    double QR_1_offset;

    /**
     * The relative can address for the property.
     */
    int QR_1_RCA;

    ///////////////////////////////////
    // MonitorPoint: QS_1 
    ///////////////////////////////////

    /**
     * The scaling factor for converting data from raw to world.
     */
    double QS_1_scale;
    /**
     * The scaling factor for converting data from raw to world.
     */
    double QS_1_offset;

    /**
     * The relative can address for the property.
     */
    int QS_1_RCA;

    ///////////////////////////////////
    // MonitorPoint: RR_1 
    ///////////////////////////////////

    /**
     * The scaling factor for converting data from raw to world.
     */
    double RR_1_scale;
    /**
     * The scaling factor for converting data from raw to world.
     */
    double RR_1_offset;

    /**
     * The relative can address for the property.
     */
    int RR_1_RCA;

    ///////////////////////////////////
    // MonitorPoint: RS_1 
    ///////////////////////////////////

    /**
     * The scaling factor for converting data from raw to world.
     */
    double RS_1_scale;
    /**
     * The scaling factor for converting data from raw to world.
     */
    double RS_1_offset;

    /**
     * The relative can address for the property.
     */
    int RS_1_RCA;

    ///////////////////////////////////
    // MonitorPoint: SS_1 
    ///////////////////////////////////

    /**
     * The scaling factor for converting data from raw to world.
     */
    double SS_1_scale;
    /**
     * The scaling factor for converting data from raw to world.
     */
    double SS_1_offset;

    /**
     * The relative can address for the property.
     */
    int SS_1_RCA;

    ///////////////////////////////////
    // MonitorPoint: QQ_2 
    ///////////////////////////////////

    /**
     * The scaling factor for converting data from raw to world.
     */
    double QQ_2_scale;
    /**
     * The scaling factor for converting data from raw to world.
     */
    double QQ_2_offset;

    /**
     * The relative can address for the property.
     */
    int QQ_2_RCA;

    ///////////////////////////////////
    // MonitorPoint: QR_2 
    ///////////////////////////////////

    /**
     * The scaling factor for converting data from raw to world.
     */
    double QR_2_scale;
    /**
     * The scaling factor for converting data from raw to world.
     */
    double QR_2_offset;

    /**
     * The relative can address for the property.
     */
    int QR_2_RCA;

    ///////////////////////////////////
    // MonitorPoint: QS_2 
    ///////////////////////////////////

    /**
     * The scaling factor for converting data from raw to world.
     */
    double QS_2_scale;
    /**
     * The scaling factor for converting data from raw to world.
     */
    double QS_2_offset;

    /**
     * The relative can address for the property.
     */
    int QS_2_RCA;

    ///////////////////////////////////
    // MonitorPoint: RR_2 
    ///////////////////////////////////

    /**
     * The scaling factor for converting data from raw to world.
     */
    double RR_2_scale;
    /**
     * The scaling factor for converting data from raw to world.
     */
    double RR_2_offset;

    /**
     * The relative can address for the property.
     */
    int RR_2_RCA;

    ///////////////////////////////////
    // MonitorPoint: RS_2 
    ///////////////////////////////////

    /**
     * The scaling factor for converting data from raw to world.
     */
    double RS_2_scale;
    /**
     * The scaling factor for converting data from raw to world.
     */
    double RS_2_offset;

    /**
     * The relative can address for the property.
     */
    int RS_2_RCA;

    ///////////////////////////////////
    // MonitorPoint: SS_2 
    ///////////////////////////////////

    /**
     * The scaling factor for converting data from raw to world.
     */
    double SS_2_scale;
    /**
     * The scaling factor for converting data from raw to world.
     */
    double SS_2_offset;

    /**
     * The relative can address for the property.
     */
    int SS_2_RCA;

    ///////////////////////////////////
    // MonitorPoint: QQ_3 
    ///////////////////////////////////

    /**
     * The scaling factor for converting data from raw to world.
     */
    double QQ_3_scale;
    /**
     * The scaling factor for converting data from raw to world.
     */
    double QQ_3_offset;

    /**
     * The relative can address for the property.
     */
    int QQ_3_RCA;

    ///////////////////////////////////
    // MonitorPoint: QR_3 
    ///////////////////////////////////

    /**
     * The scaling factor for converting data from raw to world.
     */
    double QR_3_scale;
    /**
     * The scaling factor for converting data from raw to world.
     */
    double QR_3_offset;

    /**
     * The relative can address for the property.
     */
    int QR_3_RCA;

    ///////////////////////////////////
    // MonitorPoint: QS_3 
    ///////////////////////////////////

    /**
     * The scaling factor for converting data from raw to world.
     */
    double QS_3_scale;
    /**
     * The scaling factor for converting data from raw to world.
     */
    double QS_3_offset;

    /**
     * The relative can address for the property.
     */
    int QS_3_RCA;

    ///////////////////////////////////
    // MonitorPoint: RR_3 
    ///////////////////////////////////

    /**
     * The scaling factor for converting data from raw to world.
     */
    double RR_3_scale;
    /**
     * The scaling factor for converting data from raw to world.
     */
    double RR_3_offset;

    /**
     * The relative can address for the property.
     */
    int RR_3_RCA;

    ///////////////////////////////////
    // MonitorPoint: RS_3 
    ///////////////////////////////////

    /**
     * The scaling factor for converting data from raw to world.
     */
    double RS_3_scale;
    /**
     * The scaling factor for converting data from raw to world.
     */
    double RS_3_offset;

    /**
     * The relative can address for the property.
     */
    int RS_3_RCA;

    ///////////////////////////////////
    // MonitorPoint: SS_3 
    ///////////////////////////////////

    /**
     * The scaling factor for converting data from raw to world.
     */
    double SS_3_scale;
    /**
     * The scaling factor for converting data from raw to world.
     */
    double SS_3_offset;

    /**
     * The relative can address for the property.
     */
    int SS_3_RCA;

    ///////////////////////////////////
    // MonitorPoint: NUM_SAMPLES 
    ///////////////////////////////////

    /**
     * The relative can address for the property.
     */
    int NUM_SAMPLES_RCA;

    ///////////////////////////////////
    // MonitorPoint: BAD_FRAMES_COUNT 
    ///////////////////////////////////

    /**
     * The relative can address for the property.
     */
    int BAD_FRAMES_COUNT_RCA;

    ///////////////////////////////////
    // MonitorPoint: DSP_TIMER_REGISTER 
    ///////////////////////////////////

    /**
     * The relative can address for the property.
     */
    int DSP_TIMER_REGISTER_RCA;

    ///////////////////////////////////
    // Control point: RESET_DSP 
    ///////////////////////////////////

    /**
     * The world value to which this control point has been set.
     */

    unsigned char RESET_DSP_value;

    /**
     * The relative CAN address for the control point.
     */
    int RESET_DSP_RCA;

}; // class HOLODSPImplBase

#endif /* HOLODSPImplBase_CLASS */
