/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Warning!
 *  -------------------------------------------------------------------- 
 * | This is generated code!  Do not modify this file.                  |
 * | If you do, all changes will be lost when the file is re-generated. |
 *  --------------------------------------------------------------------
 *
 * File HOLODSPBase.idl
 */

#ifndef HOLODSPBase_IDL
#define HOLODSPBase_IDL

/**
 * Device (property) interface to HOLODSP.
 */

#include <HardwareDevice.idl>
#include <baci.idl>

#pragma prefix "alma"

module Control
{

  /**
   * Holography data products.  A holography data point consists of six 
40-bit words composed of integrated products of pairs of the three 
signals named S, Q, and R. The products are designated in this document 
by QQ, QR, QS, RR, RS, and SS. Each product comprises the data portion 
of a single CAN message, and thus six CAN messages are required to 
transmit a single holography data point. Another CAN message is required 
to transmit the number of samples in the integration period.  The 
holography receiver will always obtain 4 data points per timing interval, 
and these can be read during the following timing interval. Any that 
are not read during that interval will be overwritten.
   */
  interface HOLODSPBase : Control::HardwareDevice
  {

	// Monitor Points

 	// Control Points

	/**
	 * Send reset pulse to digital signal processor. Wait at least one full 
48ms cycle before monitoring DSP data.
	 */
	void RESET_DSP() raises(ControlDeviceExceptions::HwLifecycleEx);

  };

};

#endif /* HOLODSPBase_IDL */
