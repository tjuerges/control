#! /usr/bin/env python
# "@(#) $Id$"
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006, 2008
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

import unittest
import shutil
import time
shutil.rmtree("../lib/python/site-packages", True)
import ControlExceptions
import Control
import TETimeUtil
import Acspy.Clients.SimpleClient

class HoloRxSimTest(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        self._client=Acspy.Clients.SimpleClient.PySimpleClient()
        unittest.TestCase.__init__(self,methodName)

    def __del__(self):
        pass

    def setUp(self):
        self._ref= self._client.getComponent("CONTROL/DA41/HoloDSP")
        self._ref.hwStop()
        self._ref.hwStart()
        self._ref.hwConfigure()
        self._ref.hwInitialize()
        self._ref.hwOperational()
		
    def tearDown(self):
        self._ref.hwStop()
        self._ref.hwStart()
        self._client.releaseComponent("CONTROL/DA41/HoloDSP")
		

    # --------------------------------------------------
    # Test Definitions
    def test01(self):
        '''
        Test 1 Verifies that the Simulated device is in the proper state
        '''
        self.failIf(self._ref.getHwState() !=
                    Control.HardwareDevice.Operational,
                    "Device failed to go to simulated state");

        
    def test02(self):
        '''
        Test 2 Test the correct functioning of the Subscan interface
        '''
        startTime = TETimeUtil.unix2epoch(time.time()+2);
        startTime = TETimeUtil.ceilTE(startTime).value
        stopTime  = startTime + 60000000 # 6 Second Run
        
        self._ref.startSubscan(startTime)
        self._ref.stopSubscan(stopTime)
        data = self._ref.getSubscanData(10.0) # 10 Second timeout

        self.assertEqual(data.startTime, startTime);
        self.failIf(data.exposureDuration != 120000,
                    "Incorrect sample interval returned");
        self.failIf(len(data.holoData) != 500,
                    "Incorrect lenghth data vector returned");

    def test03(self):
        '''
        Verify that we can do multiple subscans in a row
        '''
        duration = 60000000
        for iteration in range (0,5):
            startTime = TETimeUtil.unix2epoch(time.time()+2);
            startTime = TETimeUtil.ceilTE(startTime).value
            stopTime  = startTime + duration # 6 Second Run
            
            self._ref.startSubscan(startTime)
            self._ref.stopSubscan(stopTime)
            data = self._ref.getSubscanData(10.0) # 10 Second timeout
            
            self.failIf(data.startTime != startTime,
                        "Start Time does not match commanded startTime");
            self.failIf(data.exposureDuration != 120000,
                        "Incorrect sample interval returned");
            self.failIf(len(data.holoData) != duration/120000,
                        "Incorrect lenghth data vector returned");

# --------------------------------------------------
# Test Definitions
# **************************************************
# MAIN Program
if __name__ == '__main__':
        unittest.main()

