#!/usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006, 2008
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import sys
import unittest
import time
import shutil
shutil.rmtree("../lib/python/site-packages", True)
import TETimeUtil
import CCL.HoloDSP
import Control

class automated(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        unittest.TestCase.__init__(self,methodName)
        self.out = ""

    def __del__(self):
        if self.out != None:
            print self.out

    def startTestLog(self, testName):
        self.log("\n")
        self.log("-------------------------------------------------------")
        self.log("Begin "+testName)

    def endTestLog(self, testname):
        self.log("End" + testname)
        self.log("-------------------------------------------------------")
                 
    def log(self,msg):
        self.out += msg + "\n"

    def makeOperational(self):
        if (hdsp.getHwState() != Control.HardwareDevice.Operational):
            hdsp.hwStop();
            hdsp.hwStart();
            hdsp.hwConfigure();
            hdsp.hwInitialize();
            hdsp.hwOperational();
        
    # --------------------------------------------------
    # Test Definitions
    def test01(self):
        '''
        Test 1 Verifies that the component is in the proper state
        '''
        self.startTestLog("Test 1: Component lifecycle test")
        self.makeOperational();
        self.assertEqual(hdsp.getHwState(), Control.HardwareDevice.Operational)
        self.endTestLog("Test 1: Component lifecycle test")

        
    def test02(self):
        '''
        Test 2 Test the correct functioning of the Subscan interface
        '''
        self.startTestLog("Test 2: Sub-scan interface test")
        self.makeOperational();
        scanTime = 6 # ~6 secs. sub-scan
        startTime = TETimeUtil.nearestTE(TETimeUtil.unix2epoch(time.time()));
        startTime = TETimeUtil.plusTE(startTime, 21*2) # ~2 secs. to setup
        stopTime  = TETimeUtil.plusTE(startTime, 21*scanTime)
        hdsp.startSubscan(startTime.value)
        hdsp.stopSubscan(stopTime.value)
        data = hdsp.getSubscanData(scanTime*1.2)

        self.failIf(data.startTime != startTime.value,
                    "Start Time does not match commanded startTime");
        self.failIf(data.exposureDuration != 120000,
                    "Incorrect sample interval returned");
        self.failIf(len(data.holoData) != scanTime*21*24/6,
                    "Incorrect length data vector returned");
        self.endTestLog("Test 2: Sub-scan interface test")

    def test03(self):
        '''
        Verify that we can do multiple subscans in a row
        '''
        self.startTestLog("Test 3: Multiple sub-scan test")
        self.makeOperational();
        scanTime = 6 # ~6 secs. sub-scan
        for iteration in range (0,5):
            startTime = \
                      TETimeUtil.nearestTE(TETimeUtil.unix2epoch(time.time()));
            startTime = TETimeUtil.plusTE(startTime, 21*2) # ~2 secs. to setup
            stopTime  = TETimeUtil.plusTE(startTime, 21*scanTime)
#            print iteration, startTime, stopTime
            hdsp.startSubscan(startTime.value)
            hdsp.stopSubscan(stopTime.value)
            data = hdsp.getSubscanData(scanTime+30);
          
            self.failIf(data.startTime != startTime.value,
                        "Start Time does not match commanded startTime");
            self.failIf(data.exposureDuration != 120000,
                        "Incorrect sample interval returned");
            self.failIf(len(data.holoData) != scanTime*21*24/6,
                        "Incorrect length data vector returned");
        self.endTestLog("Test 3: Multiple sub-scan test")

class interactive(automated):
    def __init__(self, methodName="runTest"):
        automated.__init__(self, methodName)

    def __del__(self):
        automated.__del__(self);

# **************************************************
# MAIN Program
if __name__ == '__main__':
    ''' Usage: Phase4Test <testsuite.testname> <antenna>
    <antenna> defaults to DV01
    <testsuite> defaults to interactive
    <testname> defaults to all tests
    eg., Phase4test automated DA41 or
         Phase4test.interactive.test01
    '''
    
    if len(sys.argv) == 1:
        sys.argv.append("interactive")

    if len(sys.argv) == 2:
        sys.argv.append("DA41")

    antenna = sys.argv[2]
    sys.argv.remove(sys.argv[2])

# The creation of the component must be done outside the unit test
# framework.
    componentName = 'CONTROL/' + antenna + '/HoloDSP'
    hdsp = CCL.HoloDSP.HoloDSP(componentName, stickyFlag=True)

    unittest.main()
