// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) European Southern Observatory, 2008
// (c) Associated Universities Inc., 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#ifndef HOLORXCOMPSIMBASE_H
#define HOLORXCOMPSIMBASE_H

#include <AmbSimulationInt.h>
#include "HOLORXCompSimBaseS.h"
#include <HoloRxImpl.h>

class HOLORXCompSimBase: public Control::HoloRxImpl,
                          public virtual POA_Control::HOLORXCompSimBase
{
public:
    HOLORXCompSimBase(const ACE_CString& name, maci::ContainerServices* pCS);

    virtual ~HOLORXCompSimBase();

    /// \exception ControlExceptions::SimErrorEx
    virtual void setSimValue(CORBA::Long rca, 
                             const Control::HOLORXCompSimBase::longSeq& data);

    virtual void monitor(AmbRelativeAddr RCA, AmbDataLength_t& dataLength, 
                         AmbDataMem_t* data, sem_t* synchLock,
                         Time* timestamp, AmbErrorCode_t* status);

    virtual void command(AmbRelativeAddr RCA, AmbDataLength_t dataLength,
                         const AmbDataMem_t* data, sem_t* synchLock,
                         Time* timestamp, AmbErrorCode_t* status);

    virtual void monitorTE(ACS::Time TimeEvent, AmbRelativeAddr RCA,
                           AmbDataLength_t& dataLength, AmbDataMem_t* data,
                           sem_t* synchLock, Time* timestamp, 
                           AmbErrorCode_t* status);

    virtual void commandTE(ACS::Time TimeEvent, AmbRelativeAddr RCA,
                           AmbDataLength_t dataLength, 
                           const AmbDataMem_t* data, sem_t* synchLock,
                           Time* timestamp, AmbErrorCode_t* status);

    virtual void monitorNextTE(AmbRelativeAddr RCA, 
                               AmbDataLength_t& dataLength, AmbDataMem_t* data,
                               sem_t* synchLock, Time* timestamp,
                               AmbErrorCode_t* status);

    virtual void commandNextTE(AMBSystem::AmbRelativeAddr RCA,
                               AmbDataLength_t dataLength, 
                               const AmbDataMem_t* data, sem_t* synchLock,
                               ACS::Time* timestamp, AmbErrorCode_t* status);

    virtual void flushNode(ACS::Time TimeEvent, ACS::Time* timestamp,
                           AmbErrorCode_t* status);

    virtual void flushRCA(ACS::Time TimeEvent, AmbRelativeAddr RCA,
                          ACS::Time* timestamp, AmbErrorCode_t* status);

protected:
    virtual void getAmbInterfaceInstance() {};

    /// \exception ControlExceptions::CAMBErrorExImpl
    virtual void getDeviceUniqueId(std::string& deviceID);
    

    AmbSimulationInt simulationIf_m;

private:
    /// Copy and assignment are not allowed
    HOLORXCompSimBase(const HOLORXCompSimBase&);
    HOLORXCompSimBase& operator = (const HOLORXCompSimBase&);
};
#endif // HOLORXCOMPSIMBASE_H 
