// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2006, 2007, 2009
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#ifndef HOLORXIMPL_H
#define HOLORXIMPL_H

// Base class(es)
#include <HOLORXImplBase.h>

//CORBA servant header
#include <HOLORXImplS.h>

// Forward declarations for classes that this component uses

//includes for data members
#include <ace/Semaphore.h>
#include <vector>
#include <deque>
#include <acsThread.h>

namespace Control {
    class HoloRxImpl: public HOLORXImplBase,
                      virtual public POA_Control::HOLORXImpl
    {
    public:
        // ------------------- Constructor & Destructor -------------------

        /// The constructor for any ACS C++ component must have this signature
        HoloRxImpl(const ACE_CString & name, maci::ContainerServices* cs);
    
        /// The destructor does nothing special. It must be virtual because
        /// this class contains virtual functions.
        virtual ~HoloRxImpl();
    
        // --------------------- Component LifeCycle interface ------------
    
        /// This creates the writer thread. Its starts suspended and is
        /// activated by the startSubScan method.
        /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
        virtual void initialize();
    
        /// This terminates the writer thread (and in normal cases it should
        /// already have been suspended).
        /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
        virtual void cleanUp();
    
        // --------------------- Hardware LifeCycle interface -------------

        /// This ensures that the scanEnded semaphore is reset
        virtual void hwStartAction();
    
        /// This suspends the writer thread, flushes the monitor queue, and
        /// releases (posts) the scanEnded semaphore.
        virtual void hwStopAction();

        // --------------------- CORBA interface --------------------------

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::DeviceBusyEx
        /// \exception ControlDeviceExceptions::HwLifecycleEx
        virtual void startSubscan(ACS::Time startTime);
    
        /// See the IDL file for a description of this function.
        /// \exception HolographyExceptions::NoSubscanEx
        /// \exception ControlDeviceExceptions::HwLifecycleEx
        virtual void stopSubscan(ACS::Time stopTime);
    
        /// See the IDL file for a description of this function.
        /// \exception HolographyExceptions::NoSubscanEx
        /// \exception HolographyExceptions::NoSubscanStopEx,
        /// \exception HolographyExceptions::SubscanAbortedEx
        virtual Control::HOLORXImpl::HoloRxFlagData* 
        getSubscanData(double waitTime);
    
        /// See the IDL file for a description of this function.
        /// \exception HolographyExceptions::NotLockedEx,
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        CORBA::Double tuneLow();

        /// See the IDL file for a description of this function.
        /// \exception HolographyExceptions::NotLockedEx,
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        CORBA::Double tuneHigh();

        /// See the IDL file for a description of this function.
        /// ControlExceptions::CAMBErrorEx
        /// ControlExceptions::INACTErrorEx
        CORBA::Double getFrequency();

        /// See the IDL file for a description of this function.
        bool isLocked();

        /// See the IDL file for a description of this function.
        /// ControlExceptions::CAMBErrorEx
        /// ControlExceptions::INACTErrorEx
        void optimizeAttenuators(CORBA::Short& signalAttenuator,
                                 CORBA::Short& referenceAttenuator);

        // --------------------- C++ interface --------------------------

        /// returns the maximum allowed subscan duration. Subscans longer than
        /// this should be aborted automtically
        static ACS::TimeInterval maxSubscanDuration();

        /// The sampling period of for getting the flag data.
        static ACS::TimeInterval flagSampleInterval();

        /// Get the device ID using an AMB broadcast. The current
        /// implementation of HoloRx has an old style AMBSI-1 which does not
        /// respond to node requests at RCA 0.  So we use the broadcast based
        /// getNodeID method.
        /// \exception ControlExceptions::CAMBErrorExImpl;
        virtual void getDeviceUniqueId(std::string& deviceID) {
            ACS_TRACE("HoloRxImpl::getDeviceUniqueId");
            try {
                broadcastBasedDeviceUniqueID(deviceID);
            } catch (ControlExceptions::CAMBErrorExImpl& ex) { 
                throw ControlExceptions::CAMBErrorExImpl(ex,__FILE__,__LINE__,
                                                         "HoloRxImpl::getDeviceUniqueId");
            }
        };

    protected:
        // These data members are used by the simulation subclass and hence
        // need to be protected.
        bool scanAborted_m;
        ACS::Time startTime_m;
        ACS::Time stopTime_m;
        std::vector<bool> flagData_m;

    private:
        /// This method will sweep the Gunn oscillator voltage trying to lock
        /// the Holography receiver. The lock voltage drifts because of
        /// temperature. It returns the actual voltage it used and a negative
        /// number if it could not establish lock.
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        float searchForLock(float minVoltage, float maxVoltage);

        /// This method will do the monitoring necessary to extract the flag
        /// data. It is run by the writer thread periodically (every second)
        /// and queues the monitoring (for the next two seconds).
        void queueMonitors();

        /// This semaphore is set, by a real-time thread, when the last monitor
        /// request in a sub-scan has been done.
        sem_t  scanEndedSemaphore_m;
    
        /// This semaphore which is set, by the startSubscan method, when a
        /// sub-scan is scheduled. It is released, by the writer thread, when
        /// all the monitor requests have been sent (but not necessarily
        /// executed).
        ACE_Semaphore scanScheduled_m;
    
        /// Time of the last monitor request sent by the writer thread. 
        ACS::Time lastScheduledTime_m;

        /// This data structure contains all the relevant information for the a
        /// queued monitor request that is used to extract the flagging
        /// information.
        struct RequestStruct {
            ACS::Time       TargetTime;
            AmbDataLength_t DataLength;
            AmbDataMem_t    Data[8];
            ACS::Time       Timestamp;
            AmbErrorCode_t  Status;
        };
    
        /// This queue contains all the monitor requests that have been sent by
        /// the writer thread that have not yet been processed ie., the data
        /// has not been moved to the flagData vector. Note this *cannot* be a
        /// std::vector container as a vector, if it grows big enough to force
        /// a reallocation, will invalidate all pointers to individual
        /// elements. These pointers are passed, via the monitorTE function
        /// into the real-time software and if they are invalidated the
        /// software will crash.
        std::deque<HoloRxImpl::RequestStruct> sentRequests_m;

        /// This thread wakes up every second and queues more monitor requests
        /// to the real-time layers.
        class WriterThread : public ACS::Thread {
        public:
            WriterThread(const ACE_CString& name, HoloRxImpl& holoRx,
                         const ACS::TimeInterval responseTime,
                         const ACS::TimeInterval sleepTime);
            virtual void runLoop();
        private:
            HoloRxImpl& holoRx_m;
        };

        // The instance of the thread object
        WriterThread* writerThread_m;

    }; // end HoloRxImpl class
} // end Control namespace
#endif // HOLORXIMPL_H
