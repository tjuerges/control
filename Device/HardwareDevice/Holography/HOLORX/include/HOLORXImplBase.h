// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2006, 2008, 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

#ifndef HOLORXIMPLBASE
#define HOLORXIMPLBASE

// Base classes and ACS classes that might be needed.
#include <ambDeviceImpl.h>
#include <baciDevIO.h>
#include <baciROdouble.h>
#include <baciROfloat.h>
#include <baciROlong.h>
#include <acscommonC.h>

// CORBA servant header generated from IDL.
#include "HOLORXBaseS.h"

// Forward declarations for classes that this component uses

// Includes needed for data members.
#include <baciSmartPropertyPointer.h>

/**
 * The HOLORXBase class is the base class for the Holography Receiver.
 * <ul>
 * <li> Device:   Holography Receiver
 * <li> Assembly: HOLORX
 * <li> Parent:   Holography
 * <li> Node:     0x1c
 * <li> Channel:  1
 * </ul>
 * <p>
 * Control and monitor the holography receiver.
 */
class HOLORXImplBase:
    public AmbDeviceImpl,
    public virtual POA_Control::HOLORXBase {
    // _________________________________________________________________
    //|
    //|           PUBLIC members of class HOLORXImplBase
    //|_________________________________________________________________
public:

    /**
     * Constructor
     */
    HOLORXImplBase(const ACE_CString& name, maci::ContainerServices* cs);

    /**
     * Destructor
     */
    virtual ~HOLORXImplBase();

    /// Overriden from the component lifecycle interface.
    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void initialize();

    ///////////////////////////////////
    // MonitorPoint: PLL_STATUS
    //
    // Latched status of the PLL in the previous 48ms (the status is updated
    // to the current at the 48ms edge and the bad statuses are latched
    // until the new 48ms pulse comes along)
    ///////////////////////////////////

    virtual ACS::ROlong_ptr PLL_STATUS();

    /**
     * Get the RCA for this monitor point.
     */
    virtual long getRCA_PLL_STATUS() const {
        return PLL_STATUS_rca;
    }

    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImpl
    virtual unsigned char get_PLL_STATUS(Time & timestamp);

    ///////////////////////////////////
    // MonitorPoint: GUNN_H_VOLTAGE
    //
    // Get tuning voltage of high band Gunn Oscillator.
    ///////////////////////////////////

    virtual ACS::ROfloat_ptr GUNN_H_VOLTAGE();

    /**
     * Get the RCA for this monitor point.
     */
    virtual long getRCA_GUNN_H_VOLTAGE() const {
        return GUNN_H_VOLTAGE_rca;
    }

    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImpl
    virtual float get_GUNN_H_VOLTAGE(Time & timestamp);

    ///////////////////////////////////
    // MonitorPoint: GUNN_L_VOLTAGE
    //
    // Get tuning voltage of low band Gunn Oscillator.
    ///////////////////////////////////

    virtual ACS::ROfloat_ptr GUNN_L_VOLTAGE();

    /**
     * Get the RCA for this monitor point.
     */
    virtual long getRCA_GUNN_L_VOLTAGE() const {
        return GUNN_L_VOLTAGE_rca;
    }

    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImpl
    virtual float get_GUNN_L_VOLTAGE(Time & timestamp);

    ///////////////////////////////////
    // MonitorPoint: LO_DET_OUT
    //
    // Get local oscillator (LO) detector level.
    ///////////////////////////////////

    virtual ACS::ROfloat_ptr LO_DET_OUT();

    /**
     * Get the RCA for this monitor point.
     */
    virtual long getRCA_LO_DET_OUT() const {
        return LO_DET_OUT_rca;
    }

    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImpl
    virtual float get_LO_DET_OUT(Time & timestamp);

    ///////////////////////////////////
    // MonitorPoint: REF_DET_OUT
    //
    // Get reference intermediate frequency (IF) detector level.
    ///////////////////////////////////

    virtual ACS::ROfloat_ptr REF_DET_OUT();

    /**
     * Get the RCA for this monitor point.
     */
    virtual long getRCA_REF_DET_OUT() const {
        return REF_DET_OUT_rca;
    }

    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImpl
    virtual float get_REF_DET_OUT(Time & timestamp);

    ///////////////////////////////////
    // MonitorPoint: SIG_DET_OUT
    //
    // Get signal intermediate frequency (IF) detector level.
    ///////////////////////////////////

    virtual ACS::ROfloat_ptr SIG_DET_OUT();

    /**
     * Get the RCA for this monitor point.
     */
    virtual long getRCA_SIG_DET_OUT() const {
        return SIG_DET_OUT_rca;
    }

    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImpl
    virtual float get_SIG_DET_OUT(Time & timestamp);

    ///////////////////////////////////
    // MonitorPoint: REF_SENSE_I
    //
    // Get the RMS voltage of the Ref. I channel at the Anti-Aliasing module
    ///////////////////////////////////

    virtual ACS::ROfloat_ptr REF_SENSE_I();

    /**
     * Get the RCA for this monitor point.
     */
    virtual long getRCA_REF_SENSE_I() const {
        return REF_SENSE_I_rca;
    }

    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImpl
    virtual float get_REF_SENSE_I(Time & timestamp);

    ///////////////////////////////////
    // MonitorPoint: REF_SENSE_Q
    //
    // Get the RMS voltage of the Ref. Q channel at the Anti-Aliasing module
    ///////////////////////////////////

    virtual ACS::ROfloat_ptr REF_SENSE_Q();

    /**
     * Get the RCA for this monitor point.
     */
    virtual long getRCA_REF_SENSE_Q() const {
        return REF_SENSE_Q_rca;
    }

    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImpl
    virtual float get_REF_SENSE_Q(Time & timestamp);

    ///////////////////////////////////
    // MonitorPoint: SIG_SENSE_I
    //
    // Get the RMS voltage of the Signal I channel at the Anti-Aliasing module
    ///////////////////////////////////

    virtual ACS::ROfloat_ptr SIG_SENSE_I();

    /**
     * Get the RCA for this monitor point.
     */
    virtual long getRCA_SIG_SENSE_I() const {
        return SIG_SENSE_I_rca;
    }

    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImpl
    virtual float get_SIG_SENSE_I(Time & timestamp);

    ///////////////////////////////////
    // MonitorPoint: SIG_SENSE_Q
    //
    // Get the RMS voltage of the Signal Q channel at the Anti-Aliasing module
    // output.
    ///////////////////////////////////

    virtual ACS::ROfloat_ptr SIG_SENSE_Q();

    /**
     * Get the RCA for this monitor point.
     */
    virtual long getRCA_SIG_SENSE_Q() const {
        return SIG_SENSE_Q_rca;
    }

    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImpl
    virtual float get_SIG_SENSE_Q(Time & timestamp);

    ///////////////////////////////////
    // MonitorPoint: SUPPLY_CURRENT
    //
    // Get the supply current in A for the entire holography receiver.
    ///////////////////////////////////

    virtual ACS::ROfloat_ptr SUPPLY_CURRENT();

    /**
     * Get the RCA for this monitor point.
     */
    virtual long getRCA_SUPPLY_CURRENT() const {
        return SUPPLY_CURRENT_rca;
    }

    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImpl
    virtual float get_SUPPLY_CURRENT(Time & timestamp);

    ///////////////////////////////////
    // MonitorPoint: TEMP_POWER_SUPPLY
    //
    // Get the temperature reading in degC from temperature sensor by the
    // power supply.
    ///////////////////////////////////

    virtual ACS::ROfloat_ptr TEMP_POWER_SUPPLY();

    /**
     * Get the RCA for this monitor point.
     */
    virtual long getRCA_TEMP_POWER_SUPPLY() const {
        return TEMP_POWER_SUPPLY_rca;
    }

    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImpl
    virtual float get_TEMP_POWER_SUPPLY(Time & timestamp);

    ///////////////////////////////////
    // MonitorPoint: TEMP_SIG_MIX
    //
    // Get the temperature reading in degC from temperature sensor by the
    // mixer in signal channel.
    ///////////////////////////////////

    virtual ACS::ROfloat_ptr TEMP_SIG_MIX();

    /**
     * Get the RCA for this monitor point.
     */
    virtual long getRCA_TEMP_SIG_MIX() const {
        return TEMP_SIG_MIX_rca;
    }

    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImpl
    virtual float get_TEMP_SIG_MIX(Time & timestamp);

    ///////////////////////////////////
    // MonitorPoint: TEMP_REF_MIX
    //
    // Get the temperature reading in degC from temperature sensor by the
    // mixer in reference channel.
    ///////////////////////////////////

    virtual ACS::ROfloat_ptr TEMP_REF_MIX();

    /**
     * Get the RCA for this monitor point.
     */
    virtual long getRCA_TEMP_REF_MIX() const {
        return TEMP_REF_MIX_rca;
    }

    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImpl
    virtual float get_TEMP_REF_MIX(Time & timestamp);

    ///////////////////////////////////
    // MonitorPoint: TEMP_29MHZ_OCXO
    //
    // Get the temperature reading in degC from temperature sensor by the
    // 29 MHz oven-controlled crystal oscillator.
    ///////////////////////////////////

    virtual ACS::ROfloat_ptr TEMP_29MHZ_OCXO();

    /**
     * Get the RCA for this monitor point.
     */
    virtual long getRCA_TEMP_29MHZ_OCXO() const {
        return TEMP_29MHZ_OCXO_rca;
    }

    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImpl
    virtual float get_TEMP_29MHZ_OCXO(Time & timestamp);

    ///////////////////////////////////
    // MonitorPoint: TEMP_95MHZ_OCXO
    //
    // Get the temperature reading in degC from temperature sensor by the
    // 95 MHz oven-controlled crystal oscillator.
    ///////////////////////////////////

    virtual ACS::ROfloat_ptr TEMP_95MHZ_OCXO();

    /**
     * Get the RCA for this monitor point.
     */
    virtual long getRCA_TEMP_95MHZ_OCXO() const {
        return TEMP_95MHZ_OCXO_rca;
    }

    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImpl
    virtual float get_TEMP_95MHZ_OCXO(Time & timestamp);

    ///////////////////////////////////
    // MonitorPoint: TEMP_LOCK_BOX
    //
    // Get the temperature reading in degC from temperature sensor by the
    // lock box.
    ///////////////////////////////////

    virtual ACS::ROfloat_ptr TEMP_LOCK_BOX();

    /**
     * Get the RCA for this monitor point.
     */
    virtual long getRCA_TEMP_LOCK_BOX() const {
        return TEMP_LOCK_BOX_rca;
    }

    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImpl
    virtual float get_TEMP_LOCK_BOX(Time & timestamp);

    ///////////////////////////////////
    // MonitorPoint: SIG_ATTENUATION
    //
    // See control point.
    ///////////////////////////////////

    /**
     * Get the RCA for this monitor point.
     */
    virtual long getRCA_SIG_ATTENUATION() const {
        return SIG_ATTENUATION_rca;
    }

    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual CORBA::Long get_SIG_ATTENUATION(Time & timestamp);

    ///////////////////////////////////
    // MonitorPoint: REF_ATTENUATION
    //
    // See control point.
    ///////////////////////////////////

    /**
     * Get the RCA for this monitor point.
     */
    virtual long getRCA_REF_ATTENUATION() const {
        return REF_ATTENUATION_rca;
    }

    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual CORBA::Long get_REF_ATTENUATION(Time & timestamp);

    ///////////////////////////////////
    // MonitorPoint: SYNTH_FREQUENCY
    //
    // See control point.
    ///////////////////////////////////

    /**
     * Get the RCA for this monitor point.
     */
    virtual long getRCA_SYNTH_FREQUENCY() const {
        return SYNTH_FREQUENCY_rca;
    }

    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual CORBA::Double get_SYNTH_FREQUENCY(Time & timestamp);

    ///////////////////////////////////
    // MonitorPoint: DRIVE_ENABLE
    //
    // See control point.
    ///////////////////////////////////

    /**
     * Get the RCA for this monitor point.
     */
    virtual long getRCA_DRIVE_ENABLE() const {
        return DRIVE_ENABLE_rca;
    }

    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual CORBA::Long get_DRIVE_ENABLE(Time & timestamp);

    ///////////////////////////////////
    // MonitorPoint: LOOP_ENABLE
    //
    // See control point.
    ///////////////////////////////////

    /**
     * Get the RCA for this monitor point.
     */
    virtual long getRCA_LOOP_ENABLE() const {
        return LOOP_ENABLE_rca;
    }

    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual CORBA::Long get_LOOP_ENABLE(Time & timestamp);

    ///////////////////////////////////
    // MonitorPoint: NOMINAL_VOLTAGE
    //
    // See control point.
    ///////////////////////////////////

    /**
     * Get the RCA for this monitor point.
     */
    virtual long getRCA_NOMINAL_VOLTAGE() const {
        return NOMINAL_VOLTAGE_rca;
    }

    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual CORBA::Float get_NOMINAL_VOLTAGE(Time & timestamp);

    ///////////////////////////////////
    // MonitorPoint: GUNN_LOOP_GAIN
    //
    // See control point.
    ///////////////////////////////////

    /**
     * Get the RCA for this monitor point.
     */
    virtual long getRCA_GUNN_LOOP_GAIN() const {
        return GUNN_LOOP_GAIN_rca;
    }

    /// ControlExceptions::CAMBErrorEx
    /// ControlExceptions::INACTErrorEx
    virtual CORBA::Float get_GUNN_LOOP_GAIN(Time & timestamp);

    ///////////////////////////////////
    // MonitorPoint: GUNN_TUNE_RANGE
    //
    // See control point.
    ///////////////////////////////////

    /**
     * Get the RCA for this monitor point.
     */
    virtual long getRCA_GUNN_TUNE_RANGE() const {
        return GUNN_TUNE_RANGE_rca;
    }

    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual CORBA::Float get_GUNN_TUNE_RANGE(Time & timestamp);

    ///////////////////////////////////
    // MonitorPoint: GUNN_SELECT
    //
    // See control point.
    ///////////////////////////////////

    /**
     * Get the RCA for this monitor point.
     */
    virtual long getRCA_GUNN_SELECT() const {
        return GUNN_SELECT_rca;
    }

    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual CORBA::Long get_GUNN_SELECT(Time & timestamp);

    ///////////////////////////////////
    // ControlPoint: SET_SIG_ATTENUATION
    //
    // Set attenuation of signal channel.
    ///////////////////////////////////
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual void SET_SIG_ATTENUATION(CORBA::Long world);

    /**
     * Get the RCA for this control point.
     */
    virtual int getRCA_SET_SIG_ATTENUATION() const {
        return SET_SIG_ATTENUATION_rca;
    }

    ///////////////////////////////////
    // ControlPoint: SET_REF_ATTENUATION
    //
    // Set attenuation of reference channel.
    ///////////////////////////////////
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual void SET_REF_ATTENUATION(CORBA::Long world);

    /**
     * Get the RCA for this control point.
     */
    virtual int getRCA_SET_REF_ATTENUATION() const {
        return SET_REF_ATTENUATION_rca;
    }

    ///////////////////////////////////
    // ControlPoint: SET_SYNTH_FREQUENCY
    //
    // Set holography synthesizer frequency.  unsigned integer in range [0..5960]
    // representing the synthesizer frequency in range 8.730 – 9.475 GHz
    // in increments of 0.125 MHz.
    ///////////////////////////////////
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual void SET_SYNTH_FREQUENCY(CORBA::Double world);

    /**
     * Get the RCA for this control point.
     */
    virtual int getRCA_SET_SYNTH_FREQUENCY() const {
        return SET_SYNTH_FREQUENCY_rca;
    }

    ///////////////////////////////////
    // ControlPoint: SET_DRIVE_ENABLE_ON
    //
    // Turn the Gunn oscillator bias on.
    ///////////////////////////////////
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual void SET_DRIVE_ENABLE_ON();

    /**
     * Get the RCA for this control point.
     */
    virtual int getRCA_SET_DRIVE_ENABLE_ON() const {
        return SET_DRIVE_ENABLE_ON_rca;
    }

    ///////////////////////////////////
    // ControlPoint: SET_DRIVE_ENABLE_OFF
    //
    // Turn the Gunn oscillator bias off.
    ///////////////////////////////////
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual void SET_DRIVE_ENABLE_OFF();

    /**
     * Get the RCA for this control point.
     */
    virtual int getRCA_SET_DRIVE_ENABLE_OFF() const {
        return SET_DRIVE_ENABLE_OFF_rca;
    }

    ///////////////////////////////////
    // ControlPoint: SET_LOOP_ENABLE_CLOSE
    //
    // close the phased lock loop to the Gunn oscillator
    ///////////////////////////////////
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual void SET_LOOP_ENABLE_CLOSE();

    /**
     * Get the RCA for this control point.
     */
    virtual int getRCA_SET_LOOP_ENABLE_CLOSE() const {
        return SET_LOOP_ENABLE_CLOSE_rca;
    }

    ///////////////////////////////////
    // ControlPoint: SET_LOOP_ENABLE_OPEN
    //
    // open the phased lock loop to the Gunn oscillator
    ///////////////////////////////////
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual void SET_LOOP_ENABLE_OPEN();

    /**
     * Get the RCA for this control point.
     */
    virtual int getRCA_SET_LOOP_ENABLE_OPEN() const {
        return SET_LOOP_ENABLE_OPEN_rca;
    }

    ///////////////////////////////////
    // ControlPoint: SET_NOMINAL_VOLTAGE
    //
    // To set the output voltage of the 12-bit multiplying DAC (LTC8043),
    // which in turn will determine the bias voltage to the Gunn oscillator.
    ///////////////////////////////////
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual void SET_NOMINAL_VOLTAGE(CORBA::Float world);

    /**
     * Get the RCA for this control point.
     */
    virtual int getRCA_SET_NOMINAL_VOLTAGE() const {
        return SET_NOMINAL_VOLTAGE_rca;
    }

    ///////////////////////////////////
    // ControlPoint: SET_GUNN_LOOP_GAIN
    //
    // Set the loop gain of the phased locked Gunn oscillator
    ///////////////////////////////////
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual void SET_GUNN_LOOP_GAIN(CORBA::Float world);

    /**
     * Get the RCA for this control point.
     */
    virtual int getRCA_SET_GUNN_LOOP_GAIN() const {
        return SET_GUNN_LOOP_GAIN_rca;
    }

    ///////////////////////////////////
    // ControlPoint: SET_GUNN_TUNE_RANGE
    //
    // Set tuning range of Gunn oscillator
    ///////////////////////////////////
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual void SET_GUNN_TUNE_RANGE(CORBA::Float world);

    /**
     * Get the RCA for this control point.
     */
    virtual int getRCA_SET_GUNN_TUNE_RANGE() const {
        return SET_GUNN_TUNE_RANGE_rca;
    }

    ///////////////////////////////////
    // ControlPoint: SET_GUNN_SELECT_HIGH
    //
    // Select high-band Gunn (GUNN_H)
    ///////////////////////////////////
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual void SET_GUNN_SELECT_HIGH();

    /**
     * Get the RCA for this control point.
     */
    virtual int getRCA_SET_GUNN_SELECT_HIGH() const {
        return SET_GUNN_SELECT_HIGH_rca;
    }

    ///////////////////////////////////
    // ControlPoint: SET_GUNN_SELECT_LOW
    //
    // Select low-band Gunn (GUNN_L)
    ///////////////////////////////////
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual void SET_GUNN_SELECT_LOW();

    /**
     * Get the RCA for this control point.
     */
    virtual int getRCA_SET_GUNN_SELECT_LOW() const {
        return SET_GUNN_SELECT_LOW_rca;
    }

    // _________________________________________________________________
    //|
    //|           PROTECTED members of class HOLORXImplBase
    //|_________________________________________________________________
protected:
    /**
     * Override the RCA of PLL_STATUS.
     */
    virtual void setRCA_PLL_STATUS(int rca) {
        PLL_STATUS_rca = rca;
    }

    ///////////////////////////////////
    // MonitorPoint: Gunn_Oscillator_Locked
    //
    // Gunn oscillator lock (1:locked, 0:unlocked)
    ///////////////////////////////////

    virtual bool is_Gunn_Oscillator_Locked() {
        ACS::Time timestamp;
        get_PLL_STATUS(timestamp);
        return Gunn_Oscillator_Locked_value;
    }

    ///////////////////////////////////
    // MonitorPoint: Synthesizer_Locked
    //
    // Synthesizer lock (1:locked, 0:unlocked)
    ///////////////////////////////////

    virtual bool is_Synthesizer_Locked() {
        ACS::Time timestamp;
        get_PLL_STATUS(timestamp);
        return Synthesizer_Locked_value;
    }

    ///////////////////////////////////
    // MonitorPoint: REF_Fault
    //
    // REF. Status (1:sufficient power; 0: fault condition)
    ///////////////////////////////////

    virtual bool is_REF_Fault() {

        return REF_Fault_value;
    }

    ///////////////////////////////////
    // MonitorPoint: IF_Fault
    //
    // IF Status (1:sufficient power; 0: fault condition)
    ///////////////////////////////////

    virtual bool is_IF_Fault() {

        return IF_Fault_value;
    }

    ///////////////////////////////////
    // MonitorPoint: Frame_CRC_Error
    //
    // Frame CRC (1:error in the DSP frames; 0:no error)
    ///////////////////////////////////

    virtual bool is_Frame_CRC_Error() {

        return Frame_CRC_Error_value;
    }

    /**
     * Override the RCA of GUNN_H_VOLTAGE.
     */
    virtual void setRCA_GUNN_H_VOLTAGE(int rca) {
        GUNN_H_VOLTAGE_rca = rca;
    }

    /**
     * Override the RCA of GUNN_L_VOLTAGE.
     */
    virtual void setRCA_GUNN_L_VOLTAGE(int rca) {
        GUNN_L_VOLTAGE_rca = rca;
    }

    /**
     * Override the RCA of LO_DET_OUT.
     */
    virtual void setRCA_LO_DET_OUT(int rca) {
        LO_DET_OUT_rca = rca;
    }

    /**
     * Override the RCA of REF_DET_OUT.
     */
    virtual void setRCA_REF_DET_OUT(int rca) {
        REF_DET_OUT_rca = rca;
    }

    /**
     * Override the RCA of SIG_DET_OUT.
     */
    virtual void setRCA_SIG_DET_OUT(int rca) {
        SIG_DET_OUT_rca = rca;
    }

    /**
     * Override the RCA of REF_SENSE_I.
     */
    virtual void setRCA_REF_SENSE_I(int rca) {
        REF_SENSE_I_rca = rca;
    }

    /**
     * Override the RCA of REF_SENSE_Q.
     */
    virtual void setRCA_REF_SENSE_Q(int rca) {
        REF_SENSE_Q_rca = rca;
    }

    /**
     * Override the RCA of SIG_SENSE_I.
     */
    virtual void setRCA_SIG_SENSE_I(int rca) {
        SIG_SENSE_I_rca = rca;
    }

    /**
     * Override the RCA of SIG_SENSE_Q.
     */
    virtual void setRCA_SIG_SENSE_Q(int rca) {
        SIG_SENSE_Q_rca = rca;
    }

    /**
     * Override the RCA of SUPPLY_CURRENT.
     */
    virtual void setRCA_SUPPLY_CURRENT(int rca) {
        SUPPLY_CURRENT_rca = rca;
    }

    /**
     * Override the RCA of TEMP_POWER_SUPPLY.
     */
    virtual void setRCA_TEMP_POWER_SUPPLY(int rca) {
        TEMP_POWER_SUPPLY_rca = rca;
    }

    /**
     * Override the RCA of TEMP_SIG_MIX.
     */
    virtual void setRCA_TEMP_SIG_MIX(int rca) {
        TEMP_SIG_MIX_rca = rca;
    }

    /**
     * Override the RCA of TEMP_REF_MIX.
     */
    virtual void setRCA_TEMP_REF_MIX(int rca) {
        TEMP_REF_MIX_rca = rca;
    }

    /**
     * Override the RCA of TEMP_29MHZ_OCXO.
     */
    virtual void setRCA_TEMP_29MHZ_OCXO(int rca) {
        TEMP_29MHZ_OCXO_rca = rca;
    }

    /**
     * Override the RCA of TEMP_95MHZ_OCXO.
     */
    virtual void setRCA_TEMP_95MHZ_OCXO(int rca) {
        TEMP_95MHZ_OCXO_rca = rca;
    }

    /**
     * Override the RCA of TEMP_LOCK_BOX.
     */
    virtual void setRCA_TEMP_LOCK_BOX(int rca) {
        TEMP_LOCK_BOX_rca = rca;
    }

    /**
     * Override the RCA of SIG_ATTENUATION.
     */
    virtual void setRCA_SIG_ATTENUATION(int rca) {
        SIG_ATTENUATION_rca = rca;
    }

    /**
     * Override the RCA of REF_ATTENUATION.
     */
    virtual void setRCA_REF_ATTENUATION(int rca) {
        REF_ATTENUATION_rca = rca;
    }

    /**
     * Convert the raw value of SYNTH_FREQUENCY to a world value.
     */

    virtual double convert_SYNTH_FREQUENCY(unsigned short raw);

    /**
     * Override the default conversion factors of SYNTH_FREQUENCY: scale and offset.
     */
    virtual void setConversion_SYNTH_FREQUENCY(double scale, double offset) {
        SYNTH_FREQUENCY_scale = scale;
        SYNTH_FREQUENCY_offset = offset;
    }

    /**
     * Override the RCA of SYNTH_FREQUENCY.
     */
    virtual void setRCA_SYNTH_FREQUENCY(int rca) {
        SYNTH_FREQUENCY_rca = rca;
    }

    /**
     * Override the RCA of DRIVE_ENABLE.
     */
    virtual void setRCA_DRIVE_ENABLE(int rca) {
        DRIVE_ENABLE_rca = rca;
    }

    /**
     * Override the RCA of LOOP_ENABLE.
     */
    virtual void setRCA_LOOP_ENABLE(int rca) {
        LOOP_ENABLE_rca = rca;
    }

    /**
     * Override the RCA of NOMINAL_VOLTAGE.
     */
    virtual void setRCA_NOMINAL_VOLTAGE(int rca) {
        NOMINAL_VOLTAGE_rca = rca;
    }

    /**
     * Override the RCA of GUNN_LOOP_GAIN.
     */
    virtual void setRCA_GUNN_LOOP_GAIN(int rca) {
        GUNN_LOOP_GAIN_rca = rca;
    }

    /**
     * Override the RCA of GUNN_TUNE_RANGE.
     */
    virtual void setRCA_GUNN_TUNE_RANGE(int rca) {
        GUNN_TUNE_RANGE_rca = rca;
    }

    /**
     * Override the RCA of GUNN_SELECT.
     */
    virtual void setRCA_GUNN_SELECT(int rca) {
        GUNN_SELECT_rca = rca;
    }

    ///////////////////////////////////
    // ControlPoint: SET_SIG_ATTENUATION
    ///////////////////////////////////

    /**
     * Override the RCA for this control point.
     */
    virtual void setRCA_SET_SIG_ATTENUATION(int rca) {
        SET_SIG_ATTENUATION_rca = rca;
    }

    ///////////////////////////////////
    // ControlPoint: SET_REF_ATTENUATION
    ///////////////////////////////////

    /**
     * Override the RCA for this control point.
     */
    virtual void setRCA_SET_REF_ATTENUATION(int rca) {
        SET_REF_ATTENUATION_rca = rca;
    }

    ///////////////////////////////////
    // ControlPoint: SET_SYNTH_FREQUENCY
    ///////////////////////////////////

    /**
     * Convert the world value to a raw value.
     */
    virtual unsigned short convert_SET_SYNTH_FREQUENCY(double world);

    /**
     * Override the default conversion factors: scale and offset.
     */
    virtual void setConversion_SET_SYNTH_FREQUENCY(double scale, double offset) {
        SET_SYNTH_FREQUENCY_scale = scale;
        SET_SYNTH_FREQUENCY_offset = offset;
    }

    /**
     * Override the RCA for this control point.
     */
    virtual void setRCA_SET_SYNTH_FREQUENCY(int rca) {
        SET_SYNTH_FREQUENCY_rca = rca;
    }

    ///////////////////////////////////
    // ControlPoint: SET_DRIVE_ENABLE_ON
    ///////////////////////////////////

    /**
     * Override the RCA for this control point.
     */
    virtual void setRCA_SET_DRIVE_ENABLE_ON(int rca) {
        SET_DRIVE_ENABLE_ON_rca = rca;
    }

    ///////////////////////////////////
    // ControlPoint: SET_DRIVE_ENABLE_OFF
    ///////////////////////////////////

    /**
     * Override the RCA for this control point.
     */
    virtual void setRCA_SET_DRIVE_ENABLE_OFF(int rca) {
        SET_DRIVE_ENABLE_OFF_rca = rca;
    }

    ///////////////////////////////////
    // ControlPoint: SET_LOOP_ENABLE_CLOSE
    ///////////////////////////////////

    /**
     * Override the RCA for this control point.
     */
    virtual void setRCA_SET_LOOP_ENABLE_CLOSE(int rca) {
        SET_LOOP_ENABLE_CLOSE_rca = rca;
    }

    ///////////////////////////////////
    // ControlPoint: SET_LOOP_ENABLE_OPEN
    ///////////////////////////////////

    /**
     * Override the RCA for this control point.
     */
    virtual void setRCA_SET_LOOP_ENABLE_OPEN(int rca) {
        SET_LOOP_ENABLE_OPEN_rca = rca;
    }

    ///////////////////////////////////
    // ControlPoint: SET_NOMINAL_VOLTAGE
    ///////////////////////////////////

    /**
     * Override the RCA for this control point.
     */
    virtual void setRCA_SET_NOMINAL_VOLTAGE(int rca) {
        SET_NOMINAL_VOLTAGE_rca = rca;
    }

    ///////////////////////////////////
    // ControlPoint: SET_GUNN_LOOP_GAIN
    ///////////////////////////////////

    /**
     * Override the RCA for this control point.
     */
    virtual void setRCA_SET_GUNN_LOOP_GAIN(int rca) {
        SET_GUNN_LOOP_GAIN_rca = rca;
    }

    ///////////////////////////////////
    // ControlPoint: SET_GUNN_TUNE_RANGE
    ///////////////////////////////////

    /**
     * Override the RCA for this control point.
     */
    virtual void setRCA_SET_GUNN_TUNE_RANGE(int rca) {
        SET_GUNN_TUNE_RANGE_rca = rca;
    }

    ///////////////////////////////////
    // ControlPoint: SET_GUNN_SELECT_HIGH
    ///////////////////////////////////

    /**
     * Override the RCA for this control point.
     */
    virtual void setRCA_SET_GUNN_SELECT_HIGH(int rca) {
        SET_GUNN_SELECT_HIGH_rca = rca;
    }

    ///////////////////////////////////
    // ControlPoint: SET_GUNN_SELECT_LOW
    ///////////////////////////////////

    /**
     * Override the RCA for this control point.
     */
    virtual void setRCA_SET_GUNN_SELECT_LOW(int rca) {
        SET_GUNN_SELECT_LOW_rca = rca;
    }

    ////////////////////////////////////////////////////////////////
    // Class definitions needed to implemented the BACI properties.
    ////////////////////////////////////////////////////////////////

    // Class PLL_STATUS_RODevIO needed to implement PLL_STATUS as BACI property.
    class PLL_STATUS_RODevIO : public DevIO<CORBA::Long> {

    public:
        PLL_STATUS_RODevIO (HOLORXImplBase *base) { device = base; }

        /// \exception ACSErr::ACSbaseExImpl
        virtual CORBA::Long read(unsigned long long & timestamp);

        virtual bool initializeValue() { return false; }
    private:
        HOLORXImplBase *device;
    };

    // Class GUNN_H_VOLTAGE_RODevIO needed to implement GUNN_H_VOLTAGE as BACI property.

    class GUNN_H_VOLTAGE_RODevIO : public DevIO<CORBA::Float> {

    public:
        GUNN_H_VOLTAGE_RODevIO (HOLORXImplBase *base) { device = base; }

        /// \exception ACSErr::ACSbaseExImpl
        virtual CORBA::Float read(unsigned long long & timestamp);

        virtual bool initializeValue() { return false; }
    private:
        HOLORXImplBase *device;
    };

    // Class GUNN_L_VOLTAGE_RODevIO needed to implement GUNN_L_VOLTAGE as BACI property.

    class GUNN_L_VOLTAGE_RODevIO : public DevIO<CORBA::Float> {

    public:
        GUNN_L_VOLTAGE_RODevIO (HOLORXImplBase *base) { device = base; }

        /// \exception ACSErr::ACSbaseExImpl
        virtual CORBA::Float read(unsigned long long & timestamp);

        virtual bool initializeValue() { return false; }
    private:
        HOLORXImplBase *device;
    };

    // Class LO_DET_OUT_RODevIO needed to implement LO_DET_OUT as BACI property.

    class LO_DET_OUT_RODevIO : public DevIO<CORBA::Float> {

    public:
        LO_DET_OUT_RODevIO (HOLORXImplBase *base) { device = base; }

        /// \exception ACSErr::ACSbaseExImpl
        virtual CORBA::Float read(unsigned long long & timestamp);

        virtual bool initializeValue() { return false; }
    private:
        HOLORXImplBase *device;
    };

    // Class REF_DET_OUT_RODevIO needed to implement REF_DET_OUT as BACI property.

    class REF_DET_OUT_RODevIO : public DevIO<CORBA::Float> {

    public:
        REF_DET_OUT_RODevIO (HOLORXImplBase *base) { device = base; }

        /// \exception ACSErr::ACSbaseExImpl
        virtual CORBA::Float read(unsigned long long & timestamp);

        virtual bool initializeValue() { return false; }
    private:
        HOLORXImplBase *device;
    };

    // Class SIG_DET_OUT_RODevIO needed to implement SIG_DET_OUT as BACI property.

    class SIG_DET_OUT_RODevIO : public DevIO<CORBA::Float> {

    public:
        SIG_DET_OUT_RODevIO (HOLORXImplBase *base) { device = base; }

        /// \exception ACSErr::ACSbaseExImpl
        virtual CORBA::Float read(unsigned long long & timestamp);

        virtual bool initializeValue() { return false; }
    private:
        HOLORXImplBase *device;
    };

    // Class REF_SENSE_I_RODevIO needed to implement REF_SENSE_I as BACI property.

    class REF_SENSE_I_RODevIO : public DevIO<CORBA::Float> {

    public:
        REF_SENSE_I_RODevIO (HOLORXImplBase *base) { device = base; }

        /// \exception ACSErr::ACSbaseExImpl
        virtual CORBA::Float read(unsigned long long & timestamp);

        virtual bool initializeValue() { return false; }
    private:
        HOLORXImplBase *device;
    };

    // Class REF_SENSE_Q_RODevIO needed to implement REF_SENSE_Q as BACI property.

    class REF_SENSE_Q_RODevIO : public DevIO<CORBA::Float> {

    public:
        REF_SENSE_Q_RODevIO (HOLORXImplBase *base) { device = base; }

        /// \exception ACSErr::ACSbaseExImpl
        virtual CORBA::Float read(unsigned long long & timestamp);

        virtual bool initializeValue() { return false; }
    private:
        HOLORXImplBase *device;
    };

    // Class SIG_SENSE_I_RODevIO needed to implement SIG_SENSE_I as BACI property.

    class SIG_SENSE_I_RODevIO : public DevIO<CORBA::Float> {

    public:
        SIG_SENSE_I_RODevIO (HOLORXImplBase *base) { device = base; }

        /// \exception ACSErr::ACSbaseExImpl
        virtual CORBA::Float read(unsigned long long & timestamp);

        virtual bool initializeValue() { return false; }
    private:
        HOLORXImplBase *device;
    };

    // Class SIG_SENSE_Q_RODevIO needed to implement SIG_SENSE_Q as BACI property.

    class SIG_SENSE_Q_RODevIO : public DevIO<CORBA::Float> {

    public:
        SIG_SENSE_Q_RODevIO (HOLORXImplBase *base) { device = base; }

        /// \exception ACSErr::ACSbaseExImpl
        virtual CORBA::Float read(unsigned long long & timestamp);

        virtual bool initializeValue() { return false; }
    private:
        HOLORXImplBase *device;
    };

    // Class SUPPLY_CURRENT_RODevIO needed to implement SUPPLY_CURRENT as BACI property.

    class SUPPLY_CURRENT_RODevIO : public DevIO<CORBA::Float> {

    public:
        SUPPLY_CURRENT_RODevIO (HOLORXImplBase *base) { device = base; }

        /// \exception ACSErr::ACSbaseExImpl
        virtual CORBA::Float read(unsigned long long & timestamp);

        virtual bool initializeValue() { return false; }
    private:
        HOLORXImplBase *device;
    };

    // Class TEMP_POWER_SUPPLY_RODevIO needed to implement TEMP_POWER_SUPPLY as BACI property.

    class TEMP_POWER_SUPPLY_RODevIO : public DevIO<CORBA::Float> {

    public:
        TEMP_POWER_SUPPLY_RODevIO (HOLORXImplBase *base) { device = base; }

        /// \exception ACSErr::ACSbaseExImpl
        virtual CORBA::Float read(unsigned long long & timestamp);

        virtual bool initializeValue() { return false; }
    private:
        HOLORXImplBase *device;
    };

    // Class TEMP_SIG_MIX_RODevIO needed to implement TEMP_SIG_MIX as BACI property.

    class TEMP_SIG_MIX_RODevIO : public DevIO<CORBA::Float> {

    public:
        TEMP_SIG_MIX_RODevIO (HOLORXImplBase *base) { device = base; }

        /// \exception ACSErr::ACSbaseExImpl
        virtual CORBA::Float read(unsigned long long & timestamp);

        virtual bool initializeValue() { return false; }
    private:
        HOLORXImplBase *device;
    };

    // Class TEMP_REF_MIX_RODevIO needed to implement TEMP_REF_MIX as BACI property.

    class TEMP_REF_MIX_RODevIO : public DevIO<CORBA::Float> {

    public:
        TEMP_REF_MIX_RODevIO (HOLORXImplBase *base) { device = base; }

        /// \exception ACSErr::ACSbaseExImpl
        virtual CORBA::Float read(unsigned long long & timestamp);

        virtual bool initializeValue() { return false; }
    private:
        HOLORXImplBase *device;
    };

    // Class TEMP_29MHZ_OCXO_RODevIO needed to implement TEMP_29MHZ_OCXO as BACI property.

    class TEMP_29MHZ_OCXO_RODevIO : public DevIO<CORBA::Float> {

    public:
        TEMP_29MHZ_OCXO_RODevIO (HOLORXImplBase *base) { device = base; }

        /// \exception ACSErr::ACSbaseExImpl
        virtual CORBA::Float read(unsigned long long & timestamp);

        virtual bool initializeValue() { return false; }
    private:
        HOLORXImplBase *device;
    };

    // Class TEMP_95MHZ_OCXO_RODevIO needed to implement TEMP_95MHZ_OCXO as BACI property.

    class TEMP_95MHZ_OCXO_RODevIO : public DevIO<CORBA::Float> {

    public:
        TEMP_95MHZ_OCXO_RODevIO (HOLORXImplBase *base) { device = base; }

        /// \exception ACSErr::ACSbaseExImpl
        virtual CORBA::Float read(unsigned long long & timestamp);

        virtual bool initializeValue() { return false; }
    private:
        HOLORXImplBase *device;
    };

    // Class TEMP_LOCK_BOX_RODevIO needed to implement TEMP_LOCK_BOX as BACI property.

    class TEMP_LOCK_BOX_RODevIO : public DevIO<CORBA::Float> {

    public:
        TEMP_LOCK_BOX_RODevIO (HOLORXImplBase *base) { device = base; }

        /// \exception ACSErr::ACSbaseExImpl
        virtual CORBA::Float read(unsigned long long & timestamp);

        virtual bool initializeValue() { return false; }
    private:
        HOLORXImplBase *device;
    };

    // _________________________________________________________________
    //|
    //|           PRIVATE members of class HOLORXImplBase
    //|_________________________________________________________________
private:

    // The copy constructor is made private to prevent a compiler generated
    // one from being used. It is not implemented.
    HOLORXImplBase(const HOLORXImplBase& other);

    ///////////////////////////////////
    // MonitorPoint: PLL_STATUS
    ///////////////////////////////////

    /**
     * The ACS smart property pointer for getting the property.
     */

    baci::SmartPropertyPointer<baci::ROlong> PLL_STATUS_spp;

    /**
     * The relative can address for the property.
     */
    int PLL_STATUS_rca;

    ///////////////////////////////////
    // MonitorPoint: Gunn_Oscillator_Locked
    ///////////////////////////////////

    /**
     * The data associated with Gunn_Oscillator_Locked.
     */
    bool Gunn_Oscillator_Locked_value;

    ///////////////////////////////////
    // MonitorPoint: Synthesizer_Locked
    ///////////////////////////////////

    /**
     * The data associated with Synthesizer_Locked.
     */
    bool Synthesizer_Locked_value;

    ///////////////////////////////////
    // MonitorPoint: REF_Fault
    ///////////////////////////////////

    /**
     * The data associated with REF_Fault.
     */
    bool REF_Fault_value;

    ///////////////////////////////////
    // MonitorPoint: IF_Fault
    ///////////////////////////////////

    /**
     * The data associated with IF_Fault.
     */
    bool IF_Fault_value;

    ///////////////////////////////////
    // MonitorPoint: Frame_CRC_Error
    ///////////////////////////////////

    /**
     * The data associated with Frame_CRC_Error.
     */
    bool Frame_CRC_Error_value;

    ///////////////////////////////////
    // MonitorPoint: GUNN_H_VOLTAGE
    ///////////////////////////////////

    /**
     * The ACS smart property pointer for getting the property.
     */

    baci::SmartPropertyPointer<baci::ROfloat> GUNN_H_VOLTAGE_spp;

    /**
     * The relative can address for the property.
     */
    int GUNN_H_VOLTAGE_rca;

    ///////////////////////////////////
    // MonitorPoint: GUNN_L_VOLTAGE
    ///////////////////////////////////

    /**
     * The ACS smart property pointer for getting the property.
     */

    baci::SmartPropertyPointer<baci::ROfloat> GUNN_L_VOLTAGE_spp;

    /**
     * The relative can address for the property.
     */
    int GUNN_L_VOLTAGE_rca;

    ///////////////////////////////////
    // MonitorPoint: LO_DET_OUT
    ///////////////////////////////////

    /**
     * The ACS smart property pointer for getting the property.
     */

    baci::SmartPropertyPointer<baci::ROfloat> LO_DET_OUT_spp;

    /**
     * The relative can address for the property.
     */
    int LO_DET_OUT_rca;

    ///////////////////////////////////
    // MonitorPoint: REF_DET_OUT
    ///////////////////////////////////

    /**
     * The ACS smart property pointer for getting the property.
     */

    baci::SmartPropertyPointer<baci::ROfloat> REF_DET_OUT_spp;

    /**
     * The relative can address for the property.
     */
    int REF_DET_OUT_rca;

    ///////////////////////////////////
    // MonitorPoint: SIG_DET_OUT
    ///////////////////////////////////

    /**
     * The ACS smart property pointer for getting the property.
     */

    baci::SmartPropertyPointer<baci::ROfloat> SIG_DET_OUT_spp;

    /**
     * The relative can address for the property.
     */
    int SIG_DET_OUT_rca;

    ///////////////////////////////////
    // MonitorPoint: REF_SENSE_I
    ///////////////////////////////////

    /**
     * The ACS smart property pointer for getting the property.
     */

    baci::SmartPropertyPointer<baci::ROfloat> REF_SENSE_I_spp;

    /**
     * The relative can address for the property.
     */
    int REF_SENSE_I_rca;

    ///////////////////////////////////
    // MonitorPoint: REF_SENSE_Q
    ///////////////////////////////////

    /**
     * The ACS smart property pointer for getting the property.
     */

    baci::SmartPropertyPointer<baci::ROfloat> REF_SENSE_Q_spp;

    /**
     * The relative can address for the property.
     */
    int REF_SENSE_Q_rca;

    ///////////////////////////////////
    // MonitorPoint: SIG_SENSE_I
    ///////////////////////////////////

    /**
     * The ACS smart property pointer for getting the property.
     */

    baci::SmartPropertyPointer<baci::ROfloat> SIG_SENSE_I_spp;

    /**
     * The relative can address for the property.
     */
    int SIG_SENSE_I_rca;

    ///////////////////////////////////
    // MonitorPoint: SIG_SENSE_Q
    ///////////////////////////////////

    /**
     * The ACS smart property pointer for getting the property.
     */

    baci::SmartPropertyPointer<baci::ROfloat> SIG_SENSE_Q_spp;

    /**
     * The relative can address for the property.
     */
    int SIG_SENSE_Q_rca;

    ///////////////////////////////////
    // MonitorPoint: SUPPLY_CURRENT
    ///////////////////////////////////

    /**
     * The ACS smart property pointer for getting the property.
     */

    baci::SmartPropertyPointer<baci::ROfloat> SUPPLY_CURRENT_spp;

    /**
     * The relative can address for the property.
     */
    int SUPPLY_CURRENT_rca;

    ///////////////////////////////////
    // MonitorPoint: TEMP_POWER_SUPPLY
    ///////////////////////////////////

    /**
     * The ACS smart property pointer for getting the property.
     */

    baci::SmartPropertyPointer<baci::ROfloat> TEMP_POWER_SUPPLY_spp;

    /**
     * The relative can address for the property.
     */
    int TEMP_POWER_SUPPLY_rca;

    ///////////////////////////////////
    // MonitorPoint: TEMP_SIG_MIX
    ///////////////////////////////////

    /**
     * The ACS smart property pointer for getting the property.
     */

    baci::SmartPropertyPointer<baci::ROfloat> TEMP_SIG_MIX_spp;

    /**
     * The relative can address for the property.
     */
    int TEMP_SIG_MIX_rca;

    ///////////////////////////////////
    // MonitorPoint: TEMP_REF_MIX
    ///////////////////////////////////

    /**
     * The ACS smart property pointer for getting the property.
     */

    baci::SmartPropertyPointer<baci::ROfloat> TEMP_REF_MIX_spp;

    /**
     * The relative can address for the property.
     */
    int TEMP_REF_MIX_rca;

    ///////////////////////////////////
    // MonitorPoint: TEMP_29MHZ_OCXO
    ///////////////////////////////////

    /**
     * The ACS smart property pointer for getting the property.
     */

    baci::SmartPropertyPointer<baci::ROfloat> TEMP_29MHZ_OCXO_spp;

    /**
     * The relative can address for the property.
     */
    int TEMP_29MHZ_OCXO_rca;

    ///////////////////////////////////
    // MonitorPoint: TEMP_95MHZ_OCXO
    ///////////////////////////////////

    /**
     * The ACS smart property pointer for getting the property.
     */

    baci::SmartPropertyPointer<baci::ROfloat> TEMP_95MHZ_OCXO_spp;

    /**
     * The relative can address for the property.
     */
    int TEMP_95MHZ_OCXO_rca;

    ///////////////////////////////////
    // MonitorPoint: TEMP_LOCK_BOX
    ///////////////////////////////////

    /**
     * The ACS smart property pointer for getting the property.
     */

    baci::SmartPropertyPointer<baci::ROfloat> TEMP_LOCK_BOX_spp;

    /**
     * The relative can address for the property.
     */
    int TEMP_LOCK_BOX_rca;

    ///////////////////////////////////
    // MonitorPoint: SIG_ATTENUATION
    ///////////////////////////////////

    /**
     * The ACS smart property pointer for getting the property.
     */

    baci::SmartPropertyPointer<baci::ROlong> SIG_ATTENUATION_spp;

    /**
     * The relative can address for the property.
     */
    int SIG_ATTENUATION_rca;

    ///////////////////////////////////
    // MonitorPoint: REF_ATTENUATION
    ///////////////////////////////////

    /**
     * The ACS smart property pointer for getting the property.
     */

    baci::SmartPropertyPointer<baci::ROlong> REF_ATTENUATION_spp;

    /**
     * The relative can address for the property.
     */
    int REF_ATTENUATION_rca;

    ///////////////////////////////////
    // MonitorPoint: SYNTH_FREQUENCY
    ///////////////////////////////////

    /**
     * The ACS smart property pointer for getting the property.
     */

    baci::SmartPropertyPointer<baci::ROdouble> SYNTH_FREQUENCY_spp;

    /**
     * The scaling factor for converting data from raw to world.
     */
    double SYNTH_FREQUENCY_scale;
    /**
     * The scaling factor for converting data from raw to world.
     */
    double SYNTH_FREQUENCY_offset;

    /**
     * The relative can address for the property.
     */
    int SYNTH_FREQUENCY_rca;

    ///////////////////////////////////
    // MonitorPoint: DRIVE_ENABLE
    ///////////////////////////////////

    /**
     * The ACS smart property pointer for getting the property.
     */

    baci::SmartPropertyPointer<baci::ROlong> DRIVE_ENABLE_spp;

    /**
     * The relative can address for the property.
     */
    int DRIVE_ENABLE_rca;

    ///////////////////////////////////
    // MonitorPoint: LOOP_ENABLE
    ///////////////////////////////////

    /**
     * The ACS smart property pointer for getting the property.
     */

    baci::SmartPropertyPointer<baci::ROlong> LOOP_ENABLE_spp;

    /**
     * The relative can address for the property.
     */
    int LOOP_ENABLE_rca;

    ///////////////////////////////////
    // MonitorPoint: NOMINAL_VOLTAGE
    ///////////////////////////////////

    /**
     * The ACS smart property pointer for getting the property.
     */

    baci::SmartPropertyPointer<baci::ROfloat> NOMINAL_VOLTAGE_spp;

    /**
     * The relative can address for the property.
     */
    int NOMINAL_VOLTAGE_rca;

    ///////////////////////////////////
    // MonitorPoint: GUNN_LOOP_GAIN
    ///////////////////////////////////

    /**
     * The ACS smart property pointer for getting the property.
     */

    baci::SmartPropertyPointer<baci::ROfloat> GUNN_LOOP_GAIN_spp;

    /**
     * The relative can address for the property.
     */
    int GUNN_LOOP_GAIN_rca;

    ///////////////////////////////////
    // MonitorPoint: GUNN_TUNE_RANGE
    ///////////////////////////////////

    /**
     * The ACS smart property pointer for getting the property.
     */

    baci::SmartPropertyPointer<baci::ROfloat> GUNN_TUNE_RANGE_spp;

    /**
     * The relative can address for the property.
     */
    int GUNN_TUNE_RANGE_rca;

    ///////////////////////////////////
    // MonitorPoint: GUNN_SELECT
    ///////////////////////////////////

    /**
     * The ACS smart property pointer for getting the property.
     */

    baci::SmartPropertyPointer<baci::ROlong> GUNN_SELECT_spp;

    /**
     * The relative can address for the property.
     */
    int GUNN_SELECT_rca;

    ///////////////////////////////////
    // Control point: SET_SIG_ATTENUATION
    ///////////////////////////////////

    /**
     * The world value to which this control point has been set.
     */

    long SET_SIG_ATTENUATION_value;

    /**
     * The relative CAN address for the control point.
     */
    int SET_SIG_ATTENUATION_rca;

    ///////////////////////////////////
    // Control point: SET_REF_ATTENUATION
    ///////////////////////////////////

    /**
     * The world value to which this control point has been set.
     */

    long SET_REF_ATTENUATION_value;

    /**
     * The relative CAN address for the control point.
     */
    int SET_REF_ATTENUATION_rca;

    ///////////////////////////////////
    // Control point: SET_SYNTH_FREQUENCY
    ///////////////////////////////////

    /**
     * The world value to which this control point has been set.
     */

    double SET_SYNTH_FREQUENCY_value;

    /**
     * The scaling factor for converting data from raw to world.
     */
    double SET_SYNTH_FREQUENCY_scale;
    /**
     * The scaling factor for converting data from raw to world.
     */
    double SET_SYNTH_FREQUENCY_offset;

    /**
     * The relative CAN address for the control point.
     */
    int SET_SYNTH_FREQUENCY_rca;

    ///////////////////////////////////
    // Control point: SET_DRIVE_ENABLE_ON
    ///////////////////////////////////

    /**
     * The relative CAN address for the control point.
     */
    int SET_DRIVE_ENABLE_ON_rca;

    ///////////////////////////////////
    // Control point: SET_DRIVE_ENABLE_OFF
    ///////////////////////////////////

    /**
     * The relative CAN address for the control point.
     */
    int SET_DRIVE_ENABLE_OFF_rca;

    ///////////////////////////////////
    // Control point: SET_LOOP_ENABLE_CLOSE
    ///////////////////////////////////

    /**
     * The relative CAN address for the control point.
     */
    int SET_LOOP_ENABLE_CLOSE_rca;

    ///////////////////////////////////
    // Control point: SET_LOOP_ENABLE_OPEN
    ///////////////////////////////////

    /**
     * The relative CAN address for the control point.
     */
    int SET_LOOP_ENABLE_OPEN_rca;

    ///////////////////////////////////
    // Control point: SET_NOMINAL_VOLTAGE
    ///////////////////////////////////

    /**
     * The world value to which this control point has been set.
     */

    float SET_NOMINAL_VOLTAGE_value;

    /**
     * The relative CAN address for the control point.
     */
    int SET_NOMINAL_VOLTAGE_rca;

    ///////////////////////////////////
    // Control point: SET_GUNN_LOOP_GAIN
    ///////////////////////////////////

    /**
     * The world value to which this control point has been set.
     */

    float SET_GUNN_LOOP_GAIN_value;

    /**
     * The relative CAN address for the control point.
     */
    int SET_GUNN_LOOP_GAIN_rca;

    ///////////////////////////////////
    // Control point: SET_GUNN_TUNE_RANGE
    ///////////////////////////////////

    /**
     * The world value to which this control point has been set.
     */

    float SET_GUNN_TUNE_RANGE_value;

    /**
     * The relative CAN address for the control point.
     */
    int SET_GUNN_TUNE_RANGE_rca;

    ///////////////////////////////////
    // Control point: SET_GUNN_SELECT_HIGH
    ///////////////////////////////////

    /**
     * The relative CAN address for the control point.
     */
    int SET_GUNN_SELECT_HIGH_rca;

    ///////////////////////////////////
    // Control point: SET_GUNN_SELECT_LOW
    ///////////////////////////////////

    /**
     * The relative CAN address for the control point.
     */
    int SET_GUNN_SELECT_LOW_rca;

}; // class HOLORXImplBase

#endif /* HOLORXImplBase_CLASS */
