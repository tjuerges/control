// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2006 - 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char *rcsId="@(#) $Id$";
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <HoloRxImpl.h>
#include <string>
#include <HolographyExceptions.h>
#include <acstimeDurationHelper.h> // for DurationHelper
#include <TETimeUtil.h> // for TETimeUtil
#include <acstimeEpochHelper.h> // for EpochHelper
#include <iomanip> // for std::setprecision and std::fixed
#include <math.h> // for fmaxf

// shortcuts to avoid having to clutter the code with namespace qualifiers
using Control::HoloRxImpl;
using acstime::TSArray;
using std::string;
using std::ostringstream;
using log_audience::OPERATOR;
using log_audience::DEVELOPER;
using ControlExceptions::DeviceBusyExImpl;
using ControlExceptions::INACTErrorExImpl;
using ControlExceptions::CAMBErrorExImpl;
using HolographyExceptions::NoSubscanExImpl;
using HolographyExceptions::SubscanAbortedExImpl;
using HolographyExceptions::NoSubscanStopExImpl;
using HolographyExceptions::NotLockedExImpl;

// These values should perhaps go in the CDB

// The maximum sub-scan duration in seconds. If a user requests a sub-scan
// longer than this it is aborted to prevent the sub-scan data using up too
// much memory.
const long double maxSubscanDurationValue = 60*60;
const ACS::TimeInterval maxSubscanDurationTimeInterval =
    DurationHelper(maxSubscanDurationValue).value().value;

// How often to sample the device to get the flag data. This should be a
// multple of 48ms and 48ms is the fastest we can go. Its unlikely we will
// every want to change this.
const long double flagSampleIntervalValue = 0.048;
const ACS::TimeInterval flagSampleIntervalTimeInterval =
    DurationHelper(flagSampleIntervalValue).value().value;

// This is how often the writer thread runs (once every 20 TE's).
const ACS::TimeInterval loopTime = 20*TETimeUtil::TE_PERIOD_DURATION.value;

// time format string.
const char* timeFmt = "%H:%M:%S.%3q";

// This is the time to wait, after setting a new synthesiser
// frequency, before the lock bits can be trusted. Its been observed
// as being around 4 secs and this value adds a small margin.
const double timeToAcquireLock = 6.0;
const useconds_t lockTimeInUs = static_cast<useconds_t>(timeToAcquireLock*1E6);

// This is the time to wait, after setting a new attenuator value, before the
// IF signal level can be reliably readback. This value has not yet been
// tested.
const double timeToSetAttenuators = .1;
const useconds_t timeToSetAttenuatorsInUs =
    static_cast<useconds_t>(timeToSetAttenuators*1E6);

// This is the maximum allowed voltage of the IF signal level on the signal and
// reference channels. This value has not yet been tested. The attenuators are
// adjusted so that the the maximum value is less than this. Note this differs
// by a factor of 4 (TBC) from the value reported by the LabView GUI.
const float maxAllowedRef = 3.25;
const float maxAllowedSig = maxAllowedRef;

HoloRxImpl::HoloRxImpl(const ACE_CString & name, maci::ContainerServices* cs):
    HOLORXImplBase(name, cs),
    scanAborted_m(false),
    startTime_m(0),
    stopTime_m(0),
    flagData_m(),
    scanScheduled_m(1),
    lastScheduledTime_m(0),
    sentRequests_m(),
    writerThread_m(NULL)
{
    AUTO_TRACE(__func__);
    sem_init(&scanEndedSemaphore_m, 0, 0);
}

HoloRxImpl::~HoloRxImpl() {
    AUTO_TRACE(__func__);
    cleanUp();
    scanScheduled_m.remove();
    sem_destroy(&scanEndedSemaphore_m);
}

void HoloRxImpl::initialize() {
    AUTO_TRACE(__func__);
    HOLORXImplBase::initialize();

    // Now create the thread. Its starts suspended.
    const CORBA::String_var compName(acscomponent::ACSComponentImpl::name());
    std::string threadName(compName.in());
    threadName += "WriterThread";
    writerThread_m = getContainerServices()->getThreadManager()->
        create<WriterThread, HoloRxImpl>(threadName.c_str(), *this,
                                         loopTime, loopTime);
}

void HoloRxImpl::cleanUp() {
    AUTO_TRACE(__func__);

    // Shutdown the thread
    writerThread_m->terminate();

    HOLORXImplBase::cleanUp();
}

void HoloRxImpl::hwStartAction() {
    AUTO_TRACE(__func__);
    HOLORXImplBase::hwStartAction();

    // In the worst case it is possible that the scanEndedSemaphore might have
    // a count as large as 2 (if both the real-time thread and the hwStopACtion
    // functions do a sem_post).  Ensure that as part of starting up the count
    // is at zero.
    int semMaxCount = 2;

    while (sem_trywait(&scanEndedSemaphore_m) == 0 && semMaxCount >= 0) {
        semMaxCount--;
    }
    if (semMaxCount < 0) {
        // The setError function logs a message.
        setError("Unable to clear scanEndedSemaphore");
    }

    scanAborted_m = false;
}

void HoloRxImpl::hwStopAction() {
    AUTO_TRACE(__func__);
    // Move the thread to suspended mode
    writerThread_m->suspend();

    // Flush the queue
    ACS::Time actualFlushTime;
    AmbErrorCode_t status;
    flushNode(0, &actualFlushTime, &status);
    // TODO: Check the status

    if (scanScheduled_m.tryacquire() == -1) {
        // There is a scan scheduled so abort it
        // TODO. write the abortSubscan function.
        string msg = "Communications with the hardware was stoppped midway";
        msg += "through a sub-can. This is not currently handled.";
        LOG_TO_AUDIENCE(LM_ERROR, __func__, msg, OPERATOR);
    } else {
        scanScheduled_m.release();
    }

    // release the scanEndedSemaphore so that if a getSubscanData call is
    // pending it will release.
    sem_post(&scanEndedSemaphore_m);

    HOLORXImplBase::hwStopAction();
}

void HoloRxImpl::startSubscan(ACS::Time startTime) {
    AUTO_TRACE(__func__);

    // Round the start time to the next TE.
    EpochHelper
        teStartTime(TETimeUtil::ceilTE(EpochHelper(startTime).value()));
    if (teStartTime.value().value != startTime) {
        ostringstream msg;
        msg << "Adjusted the requested start time of "
            << TETimeUtil::toTimeString(startTime) << " to "
            << TETimeUtil::toTimeString(teStartTime)
            << " as it was not on a TE boundry.";
        LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), OPERATOR);
    }

    // Check that the start time is not in the past (or too close to the
    // current time).
    EpochHelper
        timeNow(TETimeUtil::ceilTE(TimeUtil::ace2epoch(ACE_OS::gettimeofday())));
    EpochHelper earliestStartTime(timeNow.value());
    // The lead time is the writerThread loop time + 2 TE's. TODO Quantify this
    // better.
    DurationHelper leadTime(writerThread_m->getSleepTime());
    leadTime += TETimeUtil::TE_PERIOD_DURATION;
    leadTime += TETimeUtil::TE_PERIOD_DURATION;
    earliestStartTime += leadTime.value();
    if (earliestStartTime >= teStartTime.value()) {
        ostringstream msg;
        msg << "The holography receiver component needs at least "
            << leadTime.toSeconds() << " seconds to schedule the sub-scan. "
            << "The requested start time of this sub-scan is "
            << TETimeUtil::toTimeString(teStartTime)
            << " and the current time is " << TETimeUtil::toTimeString(timeNow)
            << ". This is either in the past or too close to the current time.";
        // TODO: This should probably be a different sort of exception.
        DeviceBusyExImpl ex(__FILE__, __LINE__, __func__);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getDeviceBusyEx();
    }

    // Now check if a scan has already been scheduled.
    if (scanScheduled_m.tryacquire() == -1) {
        ostringstream msg;
        msg << "A sub-scan has already been scheduled. It will start at "
            << TETimeUtil::toTimeString(startTime_m) << " and stop at ";
        if (stopTime_m == 0) {
            msg << "an unspecified time";
        } else {
            msg << TETimeUtil::toTimeString(stopTime_m);
        }
        msg << ". Please wait for this sub-scan to complete before"
            << " scheduling a new one.";
        DeviceBusyExImpl ex(__FILE__, __LINE__, __func__);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getDeviceBusyEx();
    }

    sentRequests_m.clear();
    flagData_m.clear();


    startTime_m = teStartTime.value().value;
    lastScheduledTime_m = startTime_m;
    if ((stopTime_m != 0) && (startTime_m > stopTime_m)) {
        stopTime_m = 0;
    }

    // Start the thread. This must be done *after* the st{art,op}Time_m
    // data members are set.
    writerThread_m -> resume();
    LOG_TO_AUDIENCE(LM_DEBUG, __func__,
                    "Resuming the writer thread", DEVELOPER);
}

void HoloRxImpl::stopSubscan(ACS::Time stopTime) {
    AUTO_TRACE(__func__);

    EpochHelper teStopTime(TETimeUtil::ceilTE(EpochHelper(stopTime).value()));
    if (teStopTime.value().value != stopTime) {
        ostringstream msg;
        msg << "Adjusted the requested stop time of "
            << TETimeUtil::toTimeString(stopTime) << " to "
            << TETimeUtil::toTimeString(teStopTime)
            << " as it was not on a TE boundry.";
        LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), OPERATOR);
    }

    // check that the stop time is greater than the start time
    if (teStopTime < EpochHelper(startTime_m).value()) {
        ostringstream msg;
        msg << "The requested sub-scan stop time of "
            << TETimeUtil::toTimeString(teStopTime)
            << " is less than the sub-scan start time of "
            << TETimeUtil::toTimeString(startTime_m);
        // TODO This should be a different sort of exception.
        NoSubscanExImpl ex(__FILE__, __LINE__, __func__);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getNoSubscanEx();
    }
    stopTime_m = teStopTime.value().value;
}

Control::HOLORXImpl::HoloRxFlagData*
HoloRxImpl::getSubscanData(double timeout) {
    AUTO_TRACE(__func__);
    if (startTime_m == 0) {
        throw NoSubscanExImpl(__FILE__,__LINE__, __func__).getNoSubscanEx();
    }

    if (scanAborted_m) {
        throw SubscanAbortedExImpl(__FILE__, __LINE__, __func__)
            .getSubscanAbortedEx();
    }

    if (stopTime_m == 0) {
        throw NoSubscanStopExImpl(__FILE__, __LINE__, __func__)
            .getNoSubscanStopEx();
    }

    // Wait for the ScanScheduled Semaphore to be unlocked.
    {
        string msg = "Trying to get the scanScheduled semaphore.";
        LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg, DEVELOPER);
        ACE_Time_Value timeoutEndPoint;
        timeoutEndPoint.set(timeout);
        timeoutEndPoint += ACE_OS::gettimeofday();
        if (scanScheduled_m.acquire(timeoutEndPoint) == -1) {
            ostringstream msg;
            msg << "Timed out waiting for the scanScheduled semaphore.";
            msg << " Timout was set at " << timeout;
            // TODO This is the wrong sort of exception.
            SubscanAbortedExImpl ex(__FILE__, __LINE__, __func__);
            ex.addData("Detail", msg.str());
            ex.log();
            throw ex.getSubscanAbortedEx();
        } else {
            msg = "Got the scanScheduled semaphore.";
            LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg, DEVELOPER);
            // TODO. Should we release this semaphore at the end of this
            // function?
            scanScheduled_m.release();
        }
    }

    // As all monitor points have been scheduled we now just wait for the
    // scanEnded Semaphore to be unlocked. Use a timeout of two times the
    // loopTime (as the writer thread schedules this far in advance) and add a
    // bit of slop.
    {
        timespec timeOutSpec;
        const double timeout = 2*loopTime + .1;
        ACE_Time_Value timeoutEndPoint;
        timeoutEndPoint.set(timeout);
        timeoutEndPoint += ACE_OS::gettimeofday();
        timeOutSpec.tv_sec = timeoutEndPoint.sec();
        timeOutSpec.tv_nsec = timeoutEndPoint.usec() * 1000;

        string msg = "Trying to get the scanEnded semaphore.";
        LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg, DEVELOPER);
        if (sem_timedwait(&scanEndedSemaphore_m, &timeOutSpec) == -1) {
            ostringstream msg;
            msg << "Timed out waiting for the scanEnded semaphore.";
            msg << " Timout was set at " << timeout;
            // TODO This is the wrong sort of exception.
            SubscanAbortedExImpl ex(__FILE__, __LINE__, __func__);
            ex.addData("Detail", msg.str());
            ex.log();
            throw ex.getSubscanAbortedEx();
        } else {
            msg = "Got the scanEnded semaphore.";
            LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg, DEVELOPER);
        }
    }

    Control::HOLORXImpl::HoloRxFlagData_var flagData =
        new Control::HOLORXImpl::HoloRxFlagData();
    flagData->startTime      = startTime_m;
    flagData->sampleInterval = flagSampleInterval();

    if (sentRequests_m.size() > 0) {
        flagData_m.clear();

        // Now copy the data
        while (sentRequests_m.size() > 0) {
            RequestStruct& thisRequest = sentRequests_m.front();
            {
                ostringstream msg;
                msg << "Processing a monitor request at RCA "
                    << getRCA_PLL_STATUS()
                    << " with a target time of "
                    << TETimeUtil::toTimeString(thisRequest.TargetTime)
                    << ". Returned status is " << thisRequest.Status;
                LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), DEVELOPER);
            }
            if (thisRequest.Status == AMBERR_NOERR) {
                bool thisFlag = true;
                if (thisRequest.DataLength == 1) {
                    const int maskedData =
                        static_cast<int>(thisRequest.Data[0]) & 0x1F;
                    if (maskedData == 15) {
                        thisFlag = false;
                    }
                }
                flagData_m.push_back(thisFlag);
                {
                    ostringstream msg;
                    msg << "Data timestamp "
                        << TETimeUtil::toTimeString(thisRequest.Timestamp)
                        << ". Data contents [";
                    for (int i = 0; i < thisRequest.DataLength; i++) {
                        msg << static_cast<int>(thisRequest.Data[i]);
                        if (i == thisRequest.DataLength - 1) {
                            msg << "]";
                        } else {
                            msg << ", ";
                        }
                    }
                    LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), DEVELOPER);
                }
            } else if (thisRequest.Status != AMBERR_FLUSHED) {
                flagData_m.push_back(true);
                ostringstream msg;
                msg << "Error collecting flags from hardware."
                    << " Returned status: " << thisRequest.Status
                    << ". Timestamp: "
                    << TETimeUtil::toTimeString(thisRequest.Timestamp);
                LOG_TO_AUDIENCE(LM_WARNING, __func__, msg.str(), DEVELOPER);
            } else {
                flagData_m.push_back(true);
                ostringstream msg;
                msg << "The monitor point scheduled to execute at "
                    << TETimeUtil::toTimeString(thisRequest.TargetTime)
                    << " was not executed (it was flushed).";
                LOG_TO_AUDIENCE(LM_WARNING, __func__, msg.str(), DEVELOPER);
            }
            sentRequests_m.pop_front();
        }
    }

    // TODO: for reasons I do not understand I could not use the replace
    // function here.
    const unsigned int numData = flagData_m.size();
    flagData->holoFlag.length(numData);
    for (unsigned int i = 0; i < numData; i++) {
        flagData->holoFlag[i] = flagData_m[i];
    }

    return flagData._retn();
}

CORBA::Double HoloRxImpl::tuneHigh() {
    AUTO_TRACE(__func__);

    usleep(lockTimeInUs);
    if (isLocked() && getFrequency() > 90.0E9) {
        /* We are already locked at the High band do nothing */
        const string msg = "Already locked in High band doing nothing";
        LOG_TO_AUDIENCE(LM_INFO, __func__, msg, OPERATOR);
        return getFrequency();
    }

    const float defaultVoltage = 8.90;
    float actualVoltage = -1;
    SET_GUNN_SELECT_HIGH();
    SET_SYNTH_FREQUENCY(static_cast<float>(9445E6));
    SET_LOOP_ENABLE_OPEN();
    SET_GUNN_LOOP_GAIN(1992.1875);
    SET_GUNN_TUNE_RANGE(19921.875);
    SET_DRIVE_ENABLE_ON();
    SET_NOMINAL_VOLTAGE(defaultVoltage);
    SET_LOOP_ENABLE_CLOSE();
    usleep(lockTimeInUs);
    if (!isLocked()) {
        actualVoltage = searchForLock(8.7, 9.5);
    }

    ostringstream msg;
    if (isLocked()) {
        if (actualVoltage < 0.0) {
            msg << "Locked using the default Gunn voltage of " << std::fixed
                << std::setprecision(3) << defaultVoltage << " V.";
            LOG_TO_AUDIENCE(LM_INFO, __func__, msg.str(), OPERATOR);
        } else {
            msg << "Locked using an adjusted Gunn Voltage of " << std::fixed
                << std::setprecision(3) << actualVoltage << " V.";
            LOG_TO_AUDIENCE(LM_WARNING, __func__, msg.str(), OPERATOR);
        }
    } else {
        msg << "Could not lock the holography receiver to the high band.";
        NotLockedExImpl ex(__FILE__, __LINE__, __func__);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getNotLockedEx();
    }
    return getFrequency();
}

CORBA::Double HoloRxImpl::tuneLow() {
    AUTO_TRACE(__func__);

    if (isLocked() && getFrequency() <= 90.0E9) {
        /* We are already locked at the High band do nothing */
        string msg = "Already locked in Low band, doing nothing";
        LOG_TO_AUDIENCE(LM_INFO, __func__, msg, OPERATOR);
        return getFrequency();
    }

    const float defaultVoltage = 9.09668;
    float actualVoltage = -1;
    SET_GUNN_SELECT_LOW();
    SET_SYNTH_FREQUENCY(static_cast<float>(8755E6));
    SET_LOOP_ENABLE_OPEN();
    SET_GUNN_LOOP_GAIN(1992.1875);
    SET_GUNN_TUNE_RANGE(50000.0);
    SET_DRIVE_ENABLE_ON();
    SET_NOMINAL_VOLTAGE(defaultVoltage);
    SET_LOOP_ENABLE_CLOSE();
    usleep(lockTimeInUs);
    if (!isLocked()) {
        actualVoltage = searchForLock(8.4, 9.3);
    }

    ostringstream msg;
    if (isLocked()) {
        if (actualVoltage < 0.0) {
            msg << "Locked using the default Gunn Voltage of " << std::fixed
                << std::setprecision(3) << defaultVoltage << " V.";
            LOG_TO_AUDIENCE(LM_INFO, __func__, msg.str(), OPERATOR);
        } else {
            msg << "Locked using an adjusted Gunn Voltage of " << std::fixed
                << std::setprecision(3) << actualVoltage << " V.";

            LOG_TO_AUDIENCE(LM_WARNING, __func__, msg.str(), OPERATOR);
        }
    } else {
        msg << "Could not lock the holography receiver to the low band.";
        NotLockedExImpl ex(__FILE__, __LINE__, __func__);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getNotLockedEx();
    }
    return getFrequency();
}

CORBA::Double HoloRxImpl::getFrequency() {

    AUTO_TRACE(__func__);
    try {
        int n = 11;
        ACS::Time timestamp;
        const CORBA::Double synthesizerFreq = get_SYNTH_FREQUENCY(timestamp);
        if (synthesizerFreq < 9000E6) n = 9;
        return synthesizerFreq*n+125E6;
    } catch (INACTErrorExImpl& ex) {
        ex.log();
        throw ex.getINACTErrorEx();
    } catch (CAMBErrorExImpl& ex) {
        ex.log();
        throw ex.getCAMBErrorEx();
    }
}

bool HoloRxImpl::isLocked() {
    AUTO_TRACE(__func__);
    return is_Gunn_Oscillator_Locked() && is_Synthesizer_Locked();
}

void HoloRxImpl::optimizeAttenuators(CORBA::Short& signalAttenuator,
                                     CORBA::Short& referenceAttenuator) {
    AUTO_TRACE(__func__);

    // This could be more efficiently done using a binary search but, as this
    // is not done very often, I do not think its worth the effort. Instead
    // I'll just do a linear search.

    signalAttenuator = referenceAttenuator = 0;
    float actI, actQ;
    ACS::Time timestamp;

    try {
        // first do the reference channel
        do {
            SET_REF_ATTENUATION(static_cast<CORBA::Long>(referenceAttenuator));
            usleep(timeToSetAttenuatorsInUs);
            actI = get_REF_SENSE_I(timestamp);
            actQ = get_REF_SENSE_Q(timestamp);
            referenceAttenuator += 1;
        } while (fmaxf(actI, actQ) > maxAllowedRef);
        // Now back off by 1dB.
        referenceAttenuator -= 1;
        SET_REF_ATTENUATION(static_cast<CORBA::Long>(referenceAttenuator));
        {
            usleep(timeToSetAttenuatorsInUs);
            actI = get_REF_SENSE_I(timestamp);
            actQ = get_REF_SENSE_Q(timestamp);
            ostringstream msg;
            msg << "Adjusted the reference attenuator to "
                <<  referenceAttenuator
                << "dB. The voltage on the reference (I, Q) channel is now ("
                << actI << ", " << actQ << ") Volts.";
            LOG_TO_AUDIENCE(LM_INFO, __func__, msg.str(), OPERATOR);
        }

        // Now do the signal channel
        do {
            SET_SIG_ATTENUATION(static_cast<CORBA::Long>(signalAttenuator));
            usleep(timeToSetAttenuatorsInUs);
            actI = get_SIG_SENSE_I(timestamp);
            actQ = get_SIG_SENSE_Q(timestamp);
            signalAttenuator += 1;
        } while (fmaxf(actI, actQ) > maxAllowedSig);
        signalAttenuator -= 1;
        SET_SIG_ATTENUATION(static_cast<CORBA::Long>(signalAttenuator));
        {
            usleep(timeToSetAttenuatorsInUs);
            actI = get_SIG_SENSE_I(timestamp);
            actQ = get_SIG_SENSE_Q(timestamp);
            ostringstream msg;
            msg << "Adjusted the signal attenuator to " <<  signalAttenuator
                << "dB. The voltage on the signal (I, Q) channel is now ("
                << actI << ", " << actQ << ") Volts.";
            LOG_TO_AUDIENCE(LM_INFO, __func__, msg.str(), OPERATOR);
        }
    } catch (INACTErrorExImpl& ex) {
        ex.log();
        throw ex.getINACTErrorEx();
    } catch (CAMBErrorExImpl& ex) {
        ex.log();
        throw ex.getCAMBErrorEx();
    }
}

float HoloRxImpl::searchForLock(float minVoltage, float maxVoltage) {
    AUTO_TRACE(__func__);
    const float stepSize = .05;
    {
        ostringstream msg;
        msg << "Search for a lock with a Gunn voltage between "
            << std::fixed << std::setprecision(2)
            << minVoltage << "V and " << maxVoltage
            << "V with a step size of " << stepSize << "V. "
            << "This will take a few minutes.";
        LOG_TO_AUDIENCE(LM_INFO, __func__, msg.str(), OPERATOR);
    }

    float v = maxVoltage;
    SET_NOMINAL_VOLTAGE(v); usleep(lockTimeInUs);
    {
        ostringstream msg;
        msg << "Searching from the top down.";
        msg << " Tried a Gunn voltage of "
            << std::fixed << std::setprecision(2)
            << v << " Volts. Receiver is ";
        if (isLocked()) {
            msg << "LOCKED.";
        } else {
            msg << "still unlocked.";
        }
        LOG_TO_AUDIENCE(LM_INFO, __func__, msg.str(), OPERATOR);
    }

    while (isLocked() == false && v > minVoltage) {
        v -= stepSize;
        SET_NOMINAL_VOLTAGE(v); usleep(lockTimeInUs);
        {
            ostringstream msg;
            msg << "Tried a Gunn voltage of " << std::fixed
                << std::setprecision(2) << v << " Volts. Receiver is ";
            if (isLocked()) {
                msg << "LOCKED.";
            } else {
                msg << "still unlocked.";
            }
            LOG_TO_AUDIENCE(LM_INFO, __func__, msg.str(), OPERATOR);
        }
    }
    if (isLocked() == false) return -1.0;
    const float upperLimit = v;

    v = minVoltage;
    SET_NOMINAL_VOLTAGE(v); usleep(lockTimeInUs);
    {
        ostringstream msg;
        msg << "Searching from the bottom up.";
        msg << " Tried a Gunn voltage of " << std::fixed
            << std::setprecision(2) << v << " Volts. Receiver is ";
        if (isLocked()) {
            msg << "LOCKED.";
        } else {
            msg << "still unlocked.";
        }
        LOG_TO_AUDIENCE(LM_INFO, __func__, msg.str(), OPERATOR);
    }
    while (isLocked() == false && v < upperLimit) {
        v += stepSize;
        SET_NOMINAL_VOLTAGE(v); usleep(lockTimeInUs);
        {
            ostringstream msg;
            msg << "Tried a Gunn Voltage of " << std::fixed
                << std::setprecision(2) << v << " Volts. Receiver is ";
            if (isLocked()) {
                msg << "LOCKED.";
            } else {
                msg << "still unlocked.";
            }
            LOG_TO_AUDIENCE(LM_INFO, __func__, msg.str(), OPERATOR);
        }
    }
    if (isLocked() == false) return -1.0;

    const float lowerLimit = v;
    const float middleVoltage = (upperLimit+lowerLimit)/2.0;

    {
        ostringstream msg;
        msg << "Lock range is " << std::fixed << std::setprecision(2)
            << lowerLimit << " - " << upperLimit << " Volts.";
        LOG_TO_AUDIENCE(LM_INFO, __func__, msg.str(), OPERATOR);
    }

    SET_NOMINAL_VOLTAGE(middleVoltage); usleep(lockTimeInUs);

    if (isLocked() == false) return -1.0;
    return middleVoltage;
}

void HoloRxImpl::queueMonitors() {
    AUTO_TRACE(__func__);

    EpochHelper
        timeNow(TETimeUtil::ceilTE(TimeUtil::ace2epoch(ACE_OS::gettimeofday())));

    EpochHelper horizon(timeNow.value());
    {
        DurationHelper lookAhead(writerThread_m->getSleepTime());
        lookAhead *= 2;
        horizon.add(lookAhead.value());
    }

    if (horizon < EpochHelper(lastScheduledTime_m).value()) {
        string msg = "The start time is too far in the future - waiting.";
        LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg, OPERATOR);
        return;
    }

    if (stopTime_m == 0) {
        string msg = "The subscan stop time is zero - assuming its infinite";
        LOG_TO_AUDIENCE(LM_WARNING, __func__, msg, OPERATOR);
    }

    bool doSuspend = false;
    if ((stopTime_m != 0) &&
        (horizon >= EpochHelper(stopTime_m).value())) {
        horizon.value(stopTime_m);
        doSuspend = true;
    }

    // Queue some monitors
    {
        DurationHelper
            queueTime(horizon.difference(EpochHelper(lastScheduledTime_m).value()));
        const int numTEs =
            queueTime.value().value/TETimeUtil::TE_PERIOD_DURATION.value;
        ostringstream msg;
        msg << "Queueing " << numTEs << " monitor requests ("
            << queueTime.toSeconds() << " seconds worth)";
        LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), DEVELOPER);
    }

    RequestStruct thisRequest;
    thisRequest.TargetTime = lastScheduledTime_m;
    thisRequest.Status = AMBERR_PENDING;

    ACE_Time_Value timeoutEndPoint;
    timeoutEndPoint.set(DurationHelper(loopTime).toSeconds());
    timeoutEndPoint += ACE_OS::gettimeofday();
    const ACS::Time setSemaphoreTime = stopTime_m -
        TETimeUtil::TE_PERIOD_DURATION.value;
    const ACS::Time lastTime = horizon.value().value;
    while (thisRequest.TargetTime < lastTime) {
        sentRequests_m.push_back(thisRequest);
        // As the push_back function (above) will do a copy this gets a
        // reference to the copy and this is needed as the monitorTE function
        // uses the addresses.
        RequestStruct& nextRequest = sentRequests_m.back();
        if (thisRequest.TargetTime < setSemaphoreTime) {
            monitorTE(nextRequest.TargetTime, getRCA_PLL_STATUS(),
                      nextRequest.DataLength, nextRequest.Data,
                      NULL, &nextRequest.Timestamp, &nextRequest.Status);
        } else {
            // As this is the last monitor, set the queueSemaphore_m. The queue
            // semaphore could be set for every monitor but, as the semaphore
            // is of limited size and the queue could be quite big, we will
            // only set it for the last monitor. The getSubscanData function
            // needs to wait for this last semaphore to be unlocked..
            monitorTE(nextRequest.TargetTime, getRCA_PLL_STATUS(),
                      nextRequest.DataLength, nextRequest.Data,
                      &scanEndedSemaphore_m, &nextRequest.Timestamp,
                      &nextRequest.Status);
        }
        thisRequest.TargetTime += TETimeUtil::TE_PERIOD_DURATION.value;
    }

    lastScheduledTime_m = horizon.value().value;

    if (doSuspend) {
        string msg = "Suspending the writer thread";
        LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg, DEVELOPER);
        writerThread_m -> suspend();
        scanScheduled_m.release();
        return;
    }
}

ACS::TimeInterval HoloRxImpl::maxSubscanDuration() {
    return maxSubscanDurationTimeInterval;
}

ACS::TimeInterval HoloRxImpl::flagSampleInterval() {
    return flagSampleIntervalTimeInterval;
}

HoloRxImpl::WriterThread::
WriterThread(const ACE_CString& name, HoloRxImpl& holoRx,
             const ACS::TimeInterval responseTime,
             const ACS::TimeInterval sleepTime):
    ACS::Thread(name, responseTime, sleepTime),
    holoRx_m(holoRx)
{}

void HoloRxImpl::WriterThread::runLoop() {
    holoRx_m.queueMonitors();
}

/////////////////////////////////////////////////
// MACI DLL support functions
/////////////////////////////////////////////////
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(HoloRxImpl)
