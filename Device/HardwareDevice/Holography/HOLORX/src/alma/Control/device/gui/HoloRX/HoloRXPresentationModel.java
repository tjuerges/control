package alma.Control.device.gui.HoloRX;

import java.util.ArrayList;
import java.util.List;

import alma.ACSErr.CompletionHolder;
import org.omg.CORBA.LongHolder;

import alma.Control.HOLORXBase;
import alma.Control.HOLORXBaseHelper;
import alma.Control.gui.hardwaredevice.common.HardwareDeviceChessboardPresentationModel;
import alma.Control.gui.hardwaredevice.common.HardwareDevicePresentationModel;
import alma.Control.gui.hardwaredevice.common.HardwareDevicePresentationModelEvent;
import alma.Control.gui.hardwaredevice.common.util.FormattedDouble;
import alma.Control.HardwareDevice;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

/**
 * Client-side data model for the optical holorx; allows an MVC-like 
 * architecture. This is should be the single point of communication 
 * (i.e. coupling) between the control system (backend and devices)
 * and the holorx user interface.
 *  
 * @author Nicolas Troncoso 
 * @see HardwareDevicePresentationModel
 */
public class HoloRXPresentationModel extends HardwareDevicePresentationModel
{	
	// TODO: find out if there are constants defined somewhere w/in
	// control that we can use for these!
	/**
	 * String to lookup optical holorx devices
	 */
	public final static String HOLORX_DEVICE_STRING = "HoloRx";
	//private final static String TMCDBCOMPONENT_IDL_STRING = "IDL:alma/TMCDB/TMCDBComponent:1.0";


	// TODO - determine how to get the component name so that we don't
	// hardcode it.  Will be available from TMCDB in some fashion, per
	// conversation with Allen Farris.
	private String holoRxComponentName = null;
	private HOLORXBase holorx = null;
	private String antennaName = null;

	//Monitor points
	private int pllStatus = -1;

	private double gunnHVoltage = -1000.0, 
		gunnLVoltage = -1000.0, 
		loDetOut = -1000.0, 
		refDetOut = -1000.0, 
		sigDetOut = -1000.0,
		refSenseI = -1000.0, 
		refSenseQ = -1000.0, 
		sigSenseI = -1000.0, 
		sigSenseQ = -1000.0, 
		supplyCurrent = -1000.0, 
		tempPowerSupply = -1000.0, 
		tempSigMix = -1000.0, 
		tempRefMix = -1000.0, 
		temp29MhzOcxo = -1000.0, 
		temp95MhzOcxo = -1000.0, 
		tempLockBox = -1000.0;

	//WARNING:
	//Control points (monitor) these are not documented in the ICD (go
	//figure). Aparrently these are only readouts of the las commanded
	//value to the AMBSI in the holography receiver.
	private double nominalVoltage = -1000.0, 
		gunnLoopGain = -1000.0,
		gunnTuneRange = -1000.0,
		synthFrequency = -1000.0;

	private int sigAttenuation = -1000, 
		refAttenuation = -1000, /*synthFrequency,*/
		gunnTuneSelect = -1000, 
		gunnSelect = -1000,
		driveEnable = -1000, 
		loopEnable = -1000;

	public enum MonitorPoint {
		GET_PLL_STATUS,
			GET_GUNN_H_VOLTAGE,
			GET_GUNN_L_VOLTAGE,
			GET_LO_DET_OUT,
			GET_REF_DET_OUT,
			GET_SIG_DET_OUT,
			GET_REF_SENSE_I,
			GET_REF_SENSE_Q,
			GET_SIG_SENSE_I,
			GET_SIG_SENSE_Q,
			GET_SUPPLY_CURRENT,
			GET_TEMP_POWER_SUPPLY,
			GET_TEMP_SIG_MIX,
			GET_TEMP_REF_MIX,
			GET_TEMP_29MHZ_OCXO,
			GET_TEMP_95MHZ_OCXO,
			GET_TEMP_LOCK_BOX,

			/*The folowing Monitor points are not documented in
			 * the ICD.  These monitor points DONOT monitor
			 * properties, hence thier access is diferent. ie
			 * get_SIG_ATTENUATION(org.omg.CORBA.LongHolder
			 * timestamp) */
			GET_SIG_ATTENUATION,
			GET_REF_ATTENUATION,
			GET_SYNTH_FREQUENCY,
			GET_DRIVE_ENABLE,
			GET_LOOP_ENABLE,
			GET_NOMINAL_VOLTAGE,
			GET_GUNN_LOOP_GAIN,
			GET_GUNN_TUNE_RANGE,
			GET_GUNN_SELECT;
	}

	public enum ControlPoint {
		SET_SIG_ATTENUATION,
			SET_REF_ATTENUATION,
			SET_SYNTH_FREQUENCY,
			SET_DRIVE_ENABLE,
			SET_LOOP_ENABLE,
			SET_NOMINAL_VOLTAGE,
			SET_GUNN_LOOP_GAIN,
			SET_GUNN_TUNE_RANGE,
			SET_GUNN_SELECT;
	}


	/**
	 * Constructor.
	 * @param antennaName the name of the antenna for which this
	 * presentation model will be used.
	 * @param services the plugin's container services.
	 */
	public HoloRXPresentationModel(String antennaName, PluginContainerServices services) //throws AcsJContainerServicesEx
	{	
		this.setServices(services);
		this.antennaName = antennaName;
	//	setDeviceComponent();
	}

	/**
	 * Method to apply any changes in the data.
	 * @param ControlPoint 
	 * @param Number
	 * Every control point must know to which number it must cast.
	 * @return void
	 */
	protected void applyStateChange(ControlPoint c, Number value){
		int intValue;
		double doubleValue;
		float floatValue;
		try{
		switch(c){
			case SET_SIG_ATTENUATION:
				intValue = value.intValue();
				if(intValue == sigAttenuation)
					break;
				holorx.SET_SIG_ATTENUATION(intValue);
				break;
			case SET_REF_ATTENUATION:
				intValue = value.intValue();
				if(intValue == refAttenuation)
					break;
				holorx.SET_REF_ATTENUATION(intValue);
				break;
			case SET_SYNTH_FREQUENCY:
				doubleValue = value.doubleValue();
				if(doubleValue == synthFrequency)
					break;
				holorx.SET_SYNTH_FREQUENCY(doubleValue);
				break;
			case SET_DRIVE_ENABLE:
				intValue = value.intValue();
				if(intValue == driveEnable)
					break;
				if(intValue == 1)
					holorx.SET_DRIVE_ENABLE_ON();
				else
					holorx.SET_DRIVE_ENABLE_OFF();
				break;
			case SET_LOOP_ENABLE:
				intValue = value.intValue();
				if(intValue == loopEnable)
					break;
				if(intValue == 1)
					holorx.SET_LOOP_ENABLE_CLOSE();
				else
					holorx.SET_LOOP_ENABLE_OPEN();
				break;
			case SET_NOMINAL_VOLTAGE:
				floatValue = value.floatValue();
				if(floatValue == nominalVoltage)
					break;
				holorx.SET_NOMINAL_VOLTAGE(floatValue);
				break;
			case SET_GUNN_LOOP_GAIN:
				floatValue = value.floatValue();
				if(floatValue == gunnLoopGain)
					break;
				holorx.SET_GUNN_LOOP_GAIN(floatValue);
				break;
			case SET_GUNN_TUNE_RANGE:
				floatValue = value.floatValue();
				if(floatValue == gunnTuneRange)
					break;
				holorx.SET_GUNN_TUNE_RANGE(floatValue);
				break;
			case SET_GUNN_SELECT:
				intValue = value.intValue();
				if(intValue == gunnSelect)
					break;
				if(intValue == 0)
					holorx.SET_GUNN_SELECT_LOW();
				else
					holorx.SET_GUNN_SELECT_HIGH();
				break;
		}
		}//TRY
		catch(alma.ControlExceptions.INACTErrorEx ex){
			//FIXME
		}
		catch(alma.ControlExceptions.CAMBErrorEx ex){
			//FIXME
		}
	}

	/**
	 * Method to detect any changes in the data, since the last polling
	 * was performed.
	 * @return a list of events indicating what has changed.
	 */
	protected List<HardwareDevicePresentationModelEvent> queryForStateChanges()
	{
		ArrayList<HardwareDevicePresentationModelEvent> retList = new ArrayList<HardwareDevicePresentationModelEvent>();
		HardwareDevicePresentationModelEvent event = null;
		boolean boolValue;
		int intValue;
		double doubleValue; 
		CompletionHolder completion = new CompletionHolder();
		LongHolder longHolder = new LongHolder();
		try{

			intValue = holorx.PLL_STATUS().get_sync(completion);
			if(intValue != this.pllStatus){
				this.pllStatus = intValue;
				retList.add(new HardwareDevicePresentationModelEvent(HoloRXPresentationModel.MonitorPoint.GET_PLL_STATUS, intValue)); 
			}


			doubleValue = holorx.GUNN_H_VOLTAGE().get_sync(completion);
			if(doubleValue != this.gunnHVoltage){
				this.gunnHVoltage = doubleValue;
				retList.add(new HardwareDevicePresentationModelEvent(HoloRXPresentationModel.MonitorPoint.GET_GUNN_H_VOLTAGE, doubleValue));
			}


			doubleValue = holorx.GUNN_L_VOLTAGE().get_sync(completion);
			if(doubleValue != this.gunnLVoltage){
				this.gunnHVoltage = doubleValue;
				retList.add(new HardwareDevicePresentationModelEvent(HoloRXPresentationModel.MonitorPoint.GET_GUNN_L_VOLTAGE, doubleValue));
			}


			doubleValue = holorx.LO_DET_OUT().get_sync(completion);
			if(doubleValue != this.loDetOut){
				this.loDetOut = doubleValue;
				retList.add(new HardwareDevicePresentationModelEvent(HoloRXPresentationModel.MonitorPoint.GET_LO_DET_OUT, doubleValue));
			}


			doubleValue = holorx.REF_DET_OUT().get_sync(completion);
			if(doubleValue != this.refDetOut){
				this.refDetOut = doubleValue;
				retList.add(new HardwareDevicePresentationModelEvent(HoloRXPresentationModel.MonitorPoint.GET_REF_DET_OUT, doubleValue));
			}


			doubleValue = holorx.SIG_DET_OUT().get_sync(completion);
			if(doubleValue != this.sigDetOut){
				this.sigDetOut = doubleValue;
				retList.add(new HardwareDevicePresentationModelEvent(HoloRXPresentationModel.MonitorPoint.GET_SIG_DET_OUT, doubleValue));
			}


			doubleValue = holorx.REF_SENSE_I().get_sync(completion);
			if(doubleValue != this.refSenseI){
				this.refSenseI = doubleValue;
				retList.add(new HardwareDevicePresentationModelEvent(HoloRXPresentationModel.MonitorPoint.GET_REF_SENSE_I, doubleValue));
			}


			doubleValue = holorx.REF_SENSE_Q().get_sync(completion);
			if(doubleValue != this.refSenseQ){
				this.refSenseQ = doubleValue;
				retList.add(new HardwareDevicePresentationModelEvent(HoloRXPresentationModel.MonitorPoint.GET_REF_SENSE_Q, doubleValue));
			}


			doubleValue = holorx.SIG_SENSE_I().get_sync(completion);
			if(doubleValue != this.sigSenseI){
				this.sigSenseI = doubleValue;
				retList.add(new HardwareDevicePresentationModelEvent(HoloRXPresentationModel.MonitorPoint.GET_SIG_SENSE_I, doubleValue));
			}

			doubleValue = holorx.SIG_SENSE_Q().get_sync(completion);
			if(doubleValue != this.sigSenseQ){
				this.sigSenseQ = doubleValue;
				retList.add(new HardwareDevicePresentationModelEvent(HoloRXPresentationModel.MonitorPoint.GET_SIG_SENSE_Q, doubleValue));
			}


			doubleValue = holorx.SUPPLY_CURRENT().get_sync(completion);
			if(doubleValue != this.supplyCurrent){
				this.supplyCurrent = doubleValue;
				retList.add(new HardwareDevicePresentationModelEvent(HoloRXPresentationModel.MonitorPoint.GET_SUPPLY_CURRENT, doubleValue));
			}


			doubleValue = holorx.TEMP_POWER_SUPPLY().get_sync(completion);
			if(doubleValue != this.tempPowerSupply){
				this.tempPowerSupply = doubleValue;
				retList.add(new HardwareDevicePresentationModelEvent(HoloRXPresentationModel.MonitorPoint.GET_TEMP_POWER_SUPPLY, doubleValue));
			}


			doubleValue = holorx.TEMP_SIG_MIX().get_sync(completion);
			if(doubleValue != this.tempSigMix){
				this.tempSigMix = doubleValue;
				retList.add(new HardwareDevicePresentationModelEvent(HoloRXPresentationModel.MonitorPoint.GET_TEMP_SIG_MIX, doubleValue));
			}


			doubleValue = holorx.TEMP_REF_MIX().get_sync(completion);
			if(doubleValue != this.tempRefMix){
				this.tempRefMix = doubleValue;
				retList.add(new HardwareDevicePresentationModelEvent(HoloRXPresentationModel.MonitorPoint.GET_TEMP_REF_MIX, doubleValue));
			}


			doubleValue = holorx.TEMP_29MHZ_OCXO().get_sync(completion);
			if(doubleValue != this.temp29MhzOcxo){
				this.temp29MhzOcxo = doubleValue;
				retList.add(new HardwareDevicePresentationModelEvent(HoloRXPresentationModel.MonitorPoint.GET_TEMP_29MHZ_OCXO, doubleValue));
			}


			doubleValue = holorx.TEMP_95MHZ_OCXO().get_sync(completion);
			if(doubleValue != this.temp95MhzOcxo){
				this.temp95MhzOcxo = doubleValue;
				retList.add(new HardwareDevicePresentationModelEvent(HoloRXPresentationModel.MonitorPoint.GET_TEMP_95MHZ_OCXO, doubleValue));
			}


			doubleValue = holorx.TEMP_LOCK_BOX().get_sync(completion);
			if(doubleValue != this.tempLockBox){
				this.tempLockBox = doubleValue;
				retList.add(new HardwareDevicePresentationModelEvent(HoloRXPresentationModel.MonitorPoint.GET_TEMP_LOCK_BOX, doubleValue));
			}


			/*The folowing Monitor points are not documented in the
			 * ICD*/
			intValue = holorx.get_SIG_ATTENUATION(longHolder);
			if(intValue != this.sigAttenuation){
				this.sigAttenuation = intValue;
				retList.add(new HardwareDevicePresentationModelEvent(HoloRXPresentationModel.MonitorPoint.GET_SIG_ATTENUATION, intValue));
			}


			intValue = holorx.get_REF_ATTENUATION(longHolder);
			if(intValue != this.refAttenuation){
				this.refAttenuation = intValue;
				retList.add(new HardwareDevicePresentationModelEvent(HoloRXPresentationModel.MonitorPoint.GET_REF_ATTENUATION, intValue));
			}


			doubleValue = holorx.get_SYNTH_FREQUENCY(longHolder);
			if(doubleValue != this.synthFrequency){
				this.synthFrequency = doubleValue;
				retList.add(new HardwareDevicePresentationModelEvent(HoloRXPresentationModel.MonitorPoint.GET_SYNTH_FREQUENCY, doubleValue));
			}


			intValue = holorx.get_DRIVE_ENABLE(longHolder);
			if(intValue != this.driveEnable){
				this.driveEnable = intValue;
				retList.add(new HardwareDevicePresentationModelEvent(HoloRXPresentationModel.MonitorPoint.GET_DRIVE_ENABLE, intValue));
			}


			intValue = holorx.get_LOOP_ENABLE(longHolder);
			if(intValue != this.loopEnable){
				this.loopEnable = intValue;
				retList.add(new HardwareDevicePresentationModelEvent(HoloRXPresentationModel.MonitorPoint.GET_LOOP_ENABLE, intValue));
			}


			doubleValue = holorx.get_NOMINAL_VOLTAGE(longHolder);
			if(doubleValue != this.nominalVoltage){
				this.nominalVoltage = doubleValue;
				retList.add(new HardwareDevicePresentationModelEvent(HoloRXPresentationModel.MonitorPoint.GET_NOMINAL_VOLTAGE, doubleValue));
			}


			doubleValue = holorx.get_GUNN_LOOP_GAIN(longHolder);
			if(doubleValue != this.gunnLoopGain){
				this.gunnLoopGain = doubleValue;
				retList.add(new HardwareDevicePresentationModelEvent(HoloRXPresentationModel.MonitorPoint.GET_GUNN_LOOP_GAIN, doubleValue));
			}


			doubleValue = holorx.get_GUNN_TUNE_RANGE(longHolder);
			if(doubleValue != this.gunnTuneRange){
				this.gunnTuneRange = doubleValue;
				retList.add(new HardwareDevicePresentationModelEvent(HoloRXPresentationModel.MonitorPoint.GET_GUNN_TUNE_RANGE, doubleValue));
			}


			intValue = holorx.get_GUNN_SELECT(longHolder);
			if(intValue != this.gunnSelect){
				/**
				 *The value retuned should be a enumerated
				 type, but its not, and it is treated as a
				 number. We will do so as well.*/
				this.gunnSelect = intValue;
				retList.add(new HardwareDevicePresentationModelEvent(HoloRXPresentationModel.MonitorPoint.GET_GUNN_SELECT, intValue)); 
				/*values for nominalVoltage, gunnLoopGain,
				 * gunnTuneRange, must be reseted, since we
				 * will be reading an other band.*/
				 this.nominalVoltage = -1000.0;
				 this.gunnLoopGain = -10000.0;
				 this.gunnTuneRange = -10000.0;
			}


		}//TRY
		catch(alma.ControlExceptions.INACTErrorEx ex){
			//FIXME
		}
		catch(alma.ControlExceptions.CAMBErrorEx ex){
			//FIXME
		}
		return retList;
	}
	
	public int getGunnSelect() 
	{
		LongHolder longHolder = new LongHolder();
		int intValue=0;
		try{
			intValue = holorx.get_GUNN_SELECT(longHolder);
		}//TRY
		catch(alma.ControlExceptions.INACTErrorEx ex){
			//FIXME
		}
		catch(alma.ControlExceptions.CAMBErrorEx ex){
			//FIXME
		}
		return intValue;
        }
	public boolean isDriveEnable() 
	{
		LongHolder longHolder = new LongHolder();
		int intValue=0;
		try{
			intValue = holorx.get_DRIVE_ENABLE(longHolder);
		}//TRY
		catch(alma.ControlExceptions.INACTErrorEx ex){
			//FIXME
		}
		catch(alma.ControlExceptions.CAMBErrorEx ex){
			//FIXME
		}
		return intValue==1 ? true : false;
        }
	public boolean isLoopEnable() 
	{
		LongHolder longHolder = new LongHolder();
		int intValue=0;
		try{
			intValue = holorx.get_LOOP_ENABLE(longHolder);
		}//TRY
		catch(alma.ControlExceptions.INACTErrorEx ex){
			//FIXME
		}
		catch(alma.ControlExceptions.CAMBErrorEx ex){
			//FIXME
		}
		return intValue==1 ? true : false;
        }
	/**
	 * Method used to shutdown our connection to control sub system
	 * (e.g. release components, stop threads, etc.). This should
	 * be called when the optical holorx plugin is stopping.
	 * 
	 * @see HoloRXDetailsPlugin
	 */
	protected void specializedStop()
	{
		// nothing to do here at the present time...
	}

	/**
	 * Method to release the device component, reset the variable to null,
	 * etc.
	 */
	protected synchronized void releaseDeviceComponent()
	{
		if(null != holorx)
		{
			// TODO - if we use getComponentNonSticky, it will
			// make the release component unnecessary...  Comented
			// out the release statement since NonSticky is being
			// used.
			// pluginContainerServices.releaseComponent(holoRxComponentName);
			holorx = null;
			holoRxComponentName = null;
		}	
	}

	/**
	 * Method to get/set the device component reference.
	 */
	protected synchronized HardwareDevice setDeviceComponent() throws AcsJContainerServicesEx
	{
		// get the  holorx device name, if we haven't done so
		// previously
		if(null == this.holoRxComponentName) 
		{
			this.holoRxComponentName = HardwareDeviceChessboardPresentationModel.getComponentNameForDevice(this.pluginContainerServices, antennaName, HOLORX_DEVICE_STRING);
		}

		// if we haven't already gotten the holorx component, do so
		// now
		if(null == holorx && null != holoRxComponentName)  
		{
			try 
			{
				org.omg.CORBA.Object holorxObject = this.pluginContainerServices.getComponentNonSticky(holoRxComponentName);
				this.holorx = HOLORXBaseHelper.narrow(holorxObject);
			}
			catch (AcsJContainerServicesEx ex) 
			{
				// log the error
				logger.severe("HoloRXPresentationModel.retrieveHoloRXComponent() - unable to obtain holorx component: "
						+ holoRxComponentName);

				// set our device reference to null
				this.holorx = null;

				// rethrow the exception so that the base
				// class can take action
				throw ex;
			}
		}

		// return a reference to the holorx device, or null if we
		// couldn't get one
		return holorx;
	}

}
