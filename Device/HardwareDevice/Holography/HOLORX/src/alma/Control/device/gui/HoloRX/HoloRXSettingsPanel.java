/*******************************************************************************
*    ALMA - Atacama Large Millimiter Array
*    (c) Associated Universities Inc., 2002 
*    (c) European Southern Observatory, 2002
*    Copyright by ESO (in the framework of the ALMA collaboration)
*    and Cosylab 2002, All rights reserved
*
*    This library is free software; you can redistribute it and/or
*    modify it under the terms of the GNU Lesser General Public
*    License as published by the Free Software Foundation; either
*    version 2.1 of the License, or (at your option) any later version.
*
*    This library is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    Lesser General Public License for more details.
*
*    You should have received a copy of the GNU Lesser General Public
*    License along with this library; if not, write to the Free Software
*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
*/
package alma.Control.device.gui.HoloRX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.text.DefaultFormatter;

import alma.Control.gui.hardwaredevice.common.HardwareDeviceAmbsiPanel;
import alma.Control.gui.hardwaredevice.common.HardwareDevicePresentationModelEvent;
import alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel;


/**
 * Panel that agregates the setting for the Holoraphy Reciever.
 * 
 * @author Nicolas Troncoso
 */
public class HoloRXSettingsPanel extends HardwareDeviceAmbsiPanel
{
	
	/**
	 * TODO - serial version UID
	 */
	private static final long serialVersionUID = 1L;
	private Logger logger = null;
	private JPanel attenuatorsPanel;
	private JPanel synthesizerPanel;
	private JPanel pllgunnPanel;
	private JPanel signalLevelPanel;
	private JPanel environmentalDataPanel;	

	private HoloRXPresentationModel presentationModel;

	/**
	 * Constructor.
	 * @param logger <code>Logger</code> to be used for logging messages.
	 * @param presentationModel the presentation model used to communicate with the control subsystem for
	 * state information.
	 */
	public HoloRXSettingsPanel(Logger logger, HoloRXPresentationModel presentationModel)
	{
		super(presentationModel);
		this.presentationModel = presentationModel;
		this.logger = logger;
		initialize();
	}
	/**
	 * Private method to initialze the panel.
	 */
	private void initialize() 
	{
                this.setLayout(new GridBagLayout());
                
		attenuatorsPanel = new HoloRXAttenuators(logger, presentationModel);
		synthesizerPanel = new HoloRXSynthesizer(logger, presentationModel);
		pllgunnPanel = new HoloRXPLLGunn(logger, presentationModel);
		signalLevelPanel = new HoloRXSignalLevel(logger, presentationModel);
		environmentalDataPanel = new HoloRXEnvironmentalData(logger, presentationModel);

		GridBagConstraints gridBagConstraints00 = new GridBagConstraints();
                gridBagConstraints00.gridx = 0;
                gridBagConstraints00.gridy = 0;
                gridBagConstraints00.anchor = GridBagConstraints.WEST;
		GridBagConstraints gridBagConstraints01 = new GridBagConstraints();
                gridBagConstraints01.gridx = 0;
                gridBagConstraints01.gridy = 1;
                gridBagConstraints01.anchor = GridBagConstraints.WEST;
		GridBagConstraints gridBagConstraints02 = new GridBagConstraints();
                gridBagConstraints02.gridx = 0;
                gridBagConstraints02.gridy = 2;
                gridBagConstraints02.anchor = GridBagConstraints.WEST;
		GridBagConstraints gridBagConstraints10 = new GridBagConstraints();
                gridBagConstraints10.gridx = 0;
                gridBagConstraints10.gridy = 3;
                gridBagConstraints10.anchor = GridBagConstraints.WEST;
		GridBagConstraints gridBagConstraints11 = new GridBagConstraints();
                gridBagConstraints11.gridx = 1;
                gridBagConstraints11.gridy = 1;
                gridBagConstraints11.anchor = GridBagConstraints.WEST;
		GridBagConstraints gridBagConstraints12 = new GridBagConstraints();
                gridBagConstraints12.gridx = 0;
                gridBagConstraints12.gridy = 4;
                gridBagConstraints12.anchor = GridBagConstraints.WEST;
		
		this.add(attenuatorsPanel, gridBagConstraints00);
		this.add(synthesizerPanel, gridBagConstraints01);
		this.add(pllgunnPanel, gridBagConstraints02);
		this.add(signalLevelPanel, gridBagConstraints10);
		this.add(environmentalDataPanel, gridBagConstraints12);
	}

        @Override
        protected void specializedProcessChange(HardwareDevicePresentationModelEvent evt) {
        }

        @Override
        protected void specializedCommunicationEstablished() {
        }

        @Override
        protected void specializedCommunicationLost() {
        }
}
