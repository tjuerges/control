package alma.Control.device.gui.HoloRX;

import alma.common.gui.chessboard.ChessboardDetailsPluginFactory;
import alma.common.gui.chessboard.ChessboardDetailsPlugin;
import alma.common.gui.chessboard.ChessboardDetailsPluginListener;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

public class HoloRXChessboardDetailsPluginFactory implements ChessboardDetailsPluginFactory {
    
    private static final int INSTANCES = 1;
    
    /**
     * @see alma.common.gui.chessboard.ChessboardDetailsPluginFactory
     */
    public ChessboardDetailsPlugin[] instantiateDetailsPlugin(
            String[] names,
            PluginContainerServices containerServices,
            ChessboardDetailsPluginListener listener, String pluginTitle, String selectionTitle) {
        
        HoloRXDetailsPlugin[] holorxPlugin = new HoloRXDetailsPlugin[INSTANCES];
        if(null != names && names.length > 0) {
            holorxPlugin[0] = new HoloRXDetailsPlugin(
                    names, listener, containerServices, pluginTitle, selectionTitle);
        }
        return holorxPlugin;
    }
}

