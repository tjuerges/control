package alma.Control.device.gui.HoloRX;

import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JCheckBox;
import java.awt.GridBagConstraints;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Dimension;
import javax.swing.JSpinner;
import javax.swing.BorderFactory;
import javax.swing.SpinnerNumberModel;

import alma.Control.gui.hardwaredevice.common.HardwareDeviceAmbsiPanel;
import alma.Control.gui.hardwaredevice.common.HardwareDevicePresentationModelEvent;
import alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel;

import java.util.logging.Logger;

public class HoloRXEnvironmentalData extends HardwareDeviceAmbsiPanel {

	private static final long serialVersionUID = 1L;
	private JCheckBox jCheckBoxEnableEnviroment = null;
	private JLabel jLabelSupplyCurrent = null;
	private JLabel jLabelTempPowerSupply = null;
	private QuestionMarkLabel questionableLabelTempPowerSupply = null;
	private JLabel jLabelTempSigMix = null;
	private QuestionMarkLabel questionableLabelTempSigMix = null;
	private JLabel jLabelTempRefMix = null;
	private QuestionMarkLabel questionableLabelTempRefMix = null;
	private JLabel jLabelTemp29MhzOcxo = null;
	private QuestionMarkLabel questionableLabelTemp29MhzOcxo = null;
	private JLabel jLabelTemp95MhzOcxo = null;
	private QuestionMarkLabel questionableLabelTemp95MhzOcxo = null;
	private JLabel jLabelTempLockBox = null;
	private QuestionMarkLabel questionableLabelTempLockBox = null;
	private JCheckBox jCheckBoxLog = null;
	private JCheckBox jCheckBoxLogtoFile = null;
	private JLabel jLabelInterval = null;
	private JSpinner jSpinnerInterval = null;
	private JTextField jTextFieldFile = null;
	private QuestionMarkLabel questionableLabelSupplyCurrent = null;

	private Logger logger = null;
        private HoloRXPresentationModel presentationModel;
	/**
	 * This is the default constructor
	 */
	public HoloRXEnvironmentalData(Logger logger, HoloRXPresentationModel presentationModel) {
		super(presentationModel);
		this.presentationModel = presentationModel;
		this.logger = logger;
		initialize();
	}

	/**
	 * This method initializes the Eniromental data panel.
	 * 
	 * @return void
	 */
	private void initialize() {
		GridBagConstraints gridBagConstraints19 = new GridBagConstraints();
		gridBagConstraints19.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints19.gridy = 1;
		gridBagConstraints19.weightx = 1.0;
		gridBagConstraints19.gridx = 1;
		GridBagConstraints gridBagConstraints13 = new GridBagConstraints();
		gridBagConstraints13.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints13.gridy = 6;
		gridBagConstraints13.weightx = 1.0;
		gridBagConstraints13.gridx = 4;
		GridBagConstraints gridBagConstraints12 = new GridBagConstraints();
		gridBagConstraints12.gridx = 3;
		gridBagConstraints12.anchor = GridBagConstraints.EAST;
		gridBagConstraints12.gridy = 6;
		jLabelTempLockBox = new JLabel();
		jLabelTempLockBox.setText("Lock Box [C]");
		GridBagConstraints gridBagConstraints11 = new GridBagConstraints();
		gridBagConstraints11.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints11.gridy = 5;
		gridBagConstraints11.weightx = 1.0;
		gridBagConstraints11.gridx = 4;
		GridBagConstraints gridBagConstraints10 = new GridBagConstraints();
		gridBagConstraints10.gridx = 3;
		gridBagConstraints10.anchor = GridBagConstraints.EAST;
		gridBagConstraints10.gridy = 5;
		jLabelTemp95MhzOcxo = new JLabel();
		jLabelTemp95MhzOcxo.setText("95Mhz OCX0 [C]");
		GridBagConstraints gridBagConstraints9 = new GridBagConstraints();
		gridBagConstraints9.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints9.gridy = 4;
		gridBagConstraints9.weightx = 1.0;
		gridBagConstraints9.gridx = 4;
		GridBagConstraints gridBagConstraints8 = new GridBagConstraints();
		gridBagConstraints8.gridx = 3;
		gridBagConstraints8.anchor = GridBagConstraints.EAST;
		gridBagConstraints8.gridy = 4;
		jLabelTemp29MhzOcxo = new JLabel();
		jLabelTemp29MhzOcxo.setText("29Mhz OCX0 [C]");
		GridBagConstraints gridBagConstraints7 = new GridBagConstraints();
		gridBagConstraints7.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints7.gridy = 3;
		gridBagConstraints7.weightx = 1.0;
		gridBagConstraints7.gridx = 4;
		GridBagConstraints gridBagConstraints6 = new GridBagConstraints();
		gridBagConstraints6.gridx = 3;
		gridBagConstraints6.anchor = GridBagConstraints.EAST;
		gridBagConstraints6.gridy = 3;
		jLabelTempRefMix = new JLabel();
		jLabelTempRefMix.setText("Reference Mixer [C]");
		GridBagConstraints gridBagConstraints5 = new GridBagConstraints();
		gridBagConstraints5.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints5.gridy = 2;
		gridBagConstraints5.weightx = 1.0;
		gridBagConstraints5.gridx = 4;
		GridBagConstraints gridBagConstraints4 = new GridBagConstraints();
		gridBagConstraints4.gridx = 3;
		gridBagConstraints4.anchor = GridBagConstraints.EAST;
		gridBagConstraints4.gridy = 2;
		jLabelTempSigMix = new JLabel();
		jLabelTempSigMix.setText("Signal Mixer [C]");
		GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
		gridBagConstraints3.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints3.gridy = 1;
		gridBagConstraints3.weightx = 1.0;
		gridBagConstraints3.gridx = 4;
		GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
		gridBagConstraints2.gridx = 3;
		gridBagConstraints2.anchor = GridBagConstraints.EAST;
		gridBagConstraints2.gridy = 1;
		jLabelTempPowerSupply = new JLabel();
		jLabelTempPowerSupply.setText("Power Supply [C]");
		GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
		gridBagConstraints1.gridx = 0;
		gridBagConstraints1.gridy = 1;
		jLabelSupplyCurrent = new JLabel();
		jLabelSupplyCurrent.setText("Supply Current [A]");
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		this.setBorder(BorderFactory.createTitledBorder("Environmental Data"));
		this.setLayout(new GridBagLayout());
		this.add(jLabelSupplyCurrent, gridBagConstraints1);
		this.add(jLabelTempPowerSupply, gridBagConstraints2);
		this.add(getQuestionableLabelTempPowerSupply(), gridBagConstraints3);
		this.add(jLabelTempSigMix, gridBagConstraints4);
		this.add(getQuestionableLabelTempSigMix(), gridBagConstraints5);
		this.add(jLabelTempRefMix, gridBagConstraints6);
		this.add(getQuestionableLabelTempRefMix(), gridBagConstraints7);
		this.add(jLabelTemp29MhzOcxo, gridBagConstraints8);
		this.add(getQuestionableLabelTemp29MhzOcxo(), gridBagConstraints9);
		this.add(jLabelTemp95MhzOcxo, gridBagConstraints10);
		this.add(getQuestionableLabelTemp95MhzOcxo(), gridBagConstraints11);
		this.add(jLabelTempLockBox, gridBagConstraints12);
		this.add(getQuestionableLabelTempLockBox(), gridBagConstraints13);
		this.add(getQuestionableLabelSupplyCurrent(), gridBagConstraints19);
	}

        @Override
	protected void specializedProcessChange(HardwareDevicePresentationModelEvent evt)        {
		HoloRXPresentationModel.MonitorPoint m = (HoloRXPresentationModel.MonitorPoint)evt.getEnumeration();
		switch(m){
			case GET_SUPPLY_CURRENT:
				getQuestionableLabelSupplyCurrent().setText(evt.getValue().toString());
				break;
			case GET_TEMP_POWER_SUPPLY:
				getQuestionableLabelTempPowerSupply().setText(evt.getValue().toString());
				break;
			case GET_TEMP_SIG_MIX:
				getQuestionableLabelTempSigMix().setText(evt.getValue().toString());
				break;
			case GET_TEMP_REF_MIX:
				getQuestionableLabelTempRefMix().setText(evt.getValue().toString());
				break;
			case GET_TEMP_29MHZ_OCXO:
				getQuestionableLabelTemp29MhzOcxo().setText(evt.getValue().toString());
				break;
			case GET_TEMP_95MHZ_OCXO:
				getQuestionableLabelTemp95MhzOcxo().setText(evt.getValue().toString());
				break;
			case GET_TEMP_LOCK_BOX:
				getQuestionableLabelTempLockBox().setText(evt.getValue().toString());
				break;
		}

	}
        @Override
        protected void specializedCommunicationEstablished()        {
		getQuestionableLabelTempPowerSupply().setQuestionMarkShown(false);
		getQuestionableLabelTempSigMix().setQuestionMarkShown(false);
		getQuestionableLabelTempRefMix().setQuestionMarkShown(false);
		getQuestionableLabelTemp29MhzOcxo().setQuestionMarkShown(false);
		getQuestionableLabelTemp95MhzOcxo().setQuestionMarkShown(false);
		getQuestionableLabelTempLockBox().setQuestionMarkShown(false);
		getQuestionableLabelSupplyCurrent().setQuestionMarkShown(false);
         }
        @Override
        protected void specializedCommunicationLost()        {
		getQuestionableLabelTempPowerSupply().setQuestionMarkShown(true);
		getQuestionableLabelTempSigMix().setQuestionMarkShown(true);
		getQuestionableLabelTempRefMix().setQuestionMarkShown(true);
		getQuestionableLabelTemp29MhzOcxo().setQuestionMarkShown(true);
		getQuestionableLabelTemp95MhzOcxo().setQuestionMarkShown(true);
		getQuestionableLabelTempLockBox().setQuestionMarkShown(true);
		getQuestionableLabelSupplyCurrent().setQuestionMarkShown(true);
         }


	/**
	 * This method initializes questionableLabelTempPowerSupply	
	 * 	
	 * @return alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel	
	 */
	private QuestionMarkLabel getQuestionableLabelTempPowerSupply() {
		if (questionableLabelTempPowerSupply == null) {
			questionableLabelTempPowerSupply = new QuestionMarkLabel(!this.hasCommunication);
			questionableLabelTempPowerSupply.setPreferredSize(new Dimension(60, 22));
			questionableLabelTempPowerSupply.setHorizontalAlignment(JLabel.RIGHT);
		}
		return questionableLabelTempPowerSupply;
	}

	/**
	 * This method initializes questionableLabelTempSigMix	
	 * 	
	 * @return alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel	
	 */
	private QuestionMarkLabel getQuestionableLabelTempSigMix() {
		if (questionableLabelTempSigMix == null) {
			questionableLabelTempSigMix = new QuestionMarkLabel(!this.hasCommunication);
			questionableLabelTempSigMix.setPreferredSize(new Dimension(60, 22));
			questionableLabelTempSigMix.setHorizontalAlignment(JLabel.RIGHT);
		}
		return questionableLabelTempSigMix;
	}

	/**
	 * This method initializes questionableLabelTempRefMix	
	 * 	
	 * @return alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel	
	 */
	private QuestionMarkLabel getQuestionableLabelTempRefMix() {
		if (questionableLabelTempRefMix == null) {
			questionableLabelTempRefMix = new QuestionMarkLabel(!this.hasCommunication);
			questionableLabelTempRefMix.setPreferredSize(new Dimension(60, 22));
			questionableLabelTempRefMix.setHorizontalAlignment(JLabel.RIGHT);
		}
		return questionableLabelTempRefMix;
	}

	/**
	 * This method initializes questionableLabelTemp29MhzOcxo	
	 * 	
	 * @return alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel	
	 */
	private QuestionMarkLabel getQuestionableLabelTemp29MhzOcxo() {
		if (questionableLabelTemp29MhzOcxo == null) {
			questionableLabelTemp29MhzOcxo = new QuestionMarkLabel(!this.hasCommunication);
			questionableLabelTemp29MhzOcxo.setPreferredSize(new Dimension(60, 22));
			questionableLabelTemp29MhzOcxo.setHorizontalAlignment(JLabel.RIGHT);
		}
		return questionableLabelTemp29MhzOcxo;
	}

	/**
	 * This method initializes questionableLabelTemp95MhzOcxo	
	 * 	
	 * @return alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel	
	 */
	private QuestionMarkLabel getQuestionableLabelTemp95MhzOcxo() {
		if (questionableLabelTemp95MhzOcxo == null) {
			questionableLabelTemp95MhzOcxo = new QuestionMarkLabel(!this.hasCommunication);
			questionableLabelTemp95MhzOcxo.setPreferredSize(new Dimension(60, 22));
			questionableLabelTemp95MhzOcxo.setHorizontalAlignment(JLabel.RIGHT);
		}
		return questionableLabelTemp95MhzOcxo;
	}

	/**
	 * This method initializes questionableLabelTempLockBox	
	 * 	
	 * @return alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel	
	 */
	private QuestionMarkLabel getQuestionableLabelTempLockBox() {
		if (questionableLabelTempLockBox == null) {
			questionableLabelTempLockBox = new QuestionMarkLabel(!this.hasCommunication);
			questionableLabelTempLockBox.setPreferredSize(new Dimension(60, 22));
			questionableLabelTempLockBox.setHorizontalAlignment(JLabel.RIGHT);
		}
		return questionableLabelTempLockBox;
	}


	/**
	 * This method initializes questionableLabelSupplyCurrent	
	 * 	
	 * @return alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel
	 */
	private QuestionMarkLabel getQuestionableLabelSupplyCurrent() {
		if (questionableLabelSupplyCurrent == null) {
			questionableLabelSupplyCurrent = new QuestionMarkLabel(!this.hasCommunication);
			questionableLabelSupplyCurrent.setPreferredSize(new Dimension(60, 22));
			questionableLabelSupplyCurrent.setHorizontalAlignment(JLabel.RIGHT);
		}
		return questionableLabelSupplyCurrent;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
