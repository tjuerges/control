package alma.Control.device.gui.HoloRX;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JPanel;
import javax.swing.JCheckBox;
import javax.swing.JSpinner;
import javax.swing.JLabel;
import javax.swing.SpinnerNumberModel;

import alma.Control.gui.hardwaredevice.common.HardwareDeviceAmbsiPanel;
import alma.Control.gui.hardwaredevice.common.HardwareDevicePresentationModelEvent;
import alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel;

import java.util.logging.Logger;

public class HoloRXAttenuators extends HardwareDeviceAmbsiPanel {

	private static final long serialVersionUID = 1L;
	private JCheckBox jCheckBoxEnableControl = null;
	private JSpinner jSpinnerSigAttenuation = null;
	private JSpinner jSpinnerRefAttenuation = null;
	private JLabel jLabelSigAttenuation = null;
	private JLabel jLabelRefAttenuation = null;

	private Logger logger = null;
        private HoloRXPresentationModel presentationModel;
	/**
	 * This is the default constructor
	 */
	public HoloRXAttenuators(Logger logger, HoloRXPresentationModel presentationModel) {
		super(presentationModel);
		this.presentationModel = presentationModel;
		this.logger = logger;
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		GridBagConstraints gridBagConstraints8 = new GridBagConstraints();
		gridBagConstraints8.gridx = 2;
		gridBagConstraints8.gridy = 2;
		jLabelRefAttenuation = new JLabel();
		jLabelRefAttenuation.setText("Ref. Attenuation [dB]");
		GridBagConstraints gridBagConstraints7 = new GridBagConstraints();
		gridBagConstraints7.gridx = 0;
		gridBagConstraints7.gridy = 2;
		jLabelSigAttenuation = new JLabel();
		jLabelSigAttenuation.setText("Sig. Attenuation [dB]");
		GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
		gridBagConstraints3.gridx = 3;
		gridBagConstraints3.gridy = 2;
		GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
		gridBagConstraints2.gridx = 1;
		gridBagConstraints2.gridy = 2;
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		this.setBorder(BorderFactory.createTitledBorder("Attenuators"));
		this.setLayout(new GridBagLayout());
		this.add(getJCheckBoxEnableControl(), gridBagConstraints);
		this.add(getJSpinnerSigAttenuation(), gridBagConstraints2);
		this.add(getJSpinnerRefAttenuation(), gridBagConstraints3);
		this.add(jLabelSigAttenuation, gridBagConstraints7);
		this.add(jLabelRefAttenuation, gridBagConstraints8);
		disableControl();
	}

	/**
	 * This method updates the pannel when update events are recieved.
	 * 
	 * @return void
	 */
        @Override
        protected void specializedProcessChange(HardwareDevicePresentationModelEvent evt)        {
		HoloRXPresentationModel.MonitorPoint m = (HoloRXPresentationModel.MonitorPoint)evt.getEnumeration();
		switch(m){
			case GET_SIG_ATTENUATION:
			getJSpinnerSigAttenuation().setValue(((Integer)evt.getValue()).intValue());
			break;
			case GET_REF_ATTENUATION:
			getJSpinnerRefAttenuation().setValue(((Integer)evt.getValue()).intValue());
			break;
		}
         }


	/**
	 * This method enables the control widgets in the panel.
	 * 
	 * @return void
	 */
	private void enableControl()
	{
		getJCheckBoxEnableControl().setSelected(true);		
		getJSpinnerSigAttenuation().setEnabled(true);
		getJSpinnerRefAttenuation().setEnabled(true);
	}

	/**
	 * This method disables the control widgets in the panel.
	 * 
	 * @return void
	 */
	private void disableControl()
	{
               getJCheckBoxEnableControl().setSelected(false);
               getJSpinnerSigAttenuation().setEnabled(false);
               getJSpinnerRefAttenuation().setEnabled(false);
	}	
	

	/**
	 * This method enables all widgets in the panel, when communication is
	 * restablished.
	 * 
	 * @return void
	 */
        @Override
        protected void specializedCommunicationEstablished()        {
		getJCheckBoxEnableControl().setEnabled(true);
		if(getJCheckBoxEnableControl().isSelected()){
			getJSpinnerSigAttenuation().setEnabled(true);
			getJSpinnerRefAttenuation().setEnabled(true);
		}
	}
	/**
	 * This method disables all widgets in the panel, when communication is
	 * lost.
	 * 
	 * @return void
	 */
        @Override
        protected void specializedCommunicationLost()        {
               getJCheckBoxEnableControl().setEnabled(false);
               getJSpinnerSigAttenuation().setEnabled(false);
               getJSpinnerRefAttenuation().setEnabled(false);
         }

	/**
	 * This method initializes jCheckBoxEnableControl	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxEnableControl() {
		if (jCheckBoxEnableControl == null) {
			jCheckBoxEnableControl = new JCheckBox();
			jCheckBoxEnableControl.setText("Enable Control");
			jCheckBoxEnableControl.setEnabled(false);
			jCheckBoxEnableControl.addActionListener(new EnableControlActionListener());
		}
		return jCheckBoxEnableControl;
	}


	/**
	 * This method initializes jSpinnerSigAttenuation	
	 * 	
	 * @return javax.swing.JSpinner	
	 */
	private JSpinner getJSpinnerSigAttenuation() {
		if (jSpinnerSigAttenuation == null) {
			jSpinnerSigAttenuation = new JSpinner(new SpinnerNumberModel(0,0,63,1));
			jSpinnerSigAttenuation.setPreferredSize(new Dimension(50, 24));
			jSpinnerSigAttenuation.setEnabled(false);
			jSpinnerSigAttenuation.addChangeListener(new SetSigAttenuationChangeListener());
		}
		return jSpinnerSigAttenuation;
	}

	/**
	 * This method initializes jSpinnerRefAttenuation	
	 * 	
	 * @return javax.swing.JSpinner	
	 */
	private JSpinner getJSpinnerRefAttenuation() {
		if (jSpinnerRefAttenuation == null) {
			jSpinnerRefAttenuation = new JSpinner(new SpinnerNumberModel(0,0,63,1));
			jSpinnerRefAttenuation.setPreferredSize(new Dimension(50, 24));
			jSpinnerRefAttenuation.setEnabled(false);
			jSpinnerRefAttenuation.addChangeListener(new SetRefAttenuationChangeListener());
		}
		return jSpinnerRefAttenuation;
	}

	/**
	* Private class that holds the actions for the control checkbox.
	*/
	private class EnableControlActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if(getJCheckBoxEnableControl().isSelected())
				enableControl();
			else
				disableControl();
					
		}
	}
	/**
	* Private class that holds the actions for the Signal Attenuation
	* spinner.
	*/
	private class SetSigAttenuationChangeListener implements ChangeListener
	{
		public void stateChanged(ChangeEvent e)
		{
			presentationModel.applyStateChange(HoloRXPresentationModel.ControlPoint.SET_SIG_ATTENUATION, (Number)getJSpinnerSigAttenuation().getValue());
		}
	}
	/**
	* Private class that holds the actions for the Reference Attenuation
	* spinner.
	*/
	private class SetRefAttenuationChangeListener implements ChangeListener
	{
		public void stateChanged(ChangeEvent e)
		{
			presentationModel.applyStateChange(HoloRXPresentationModel.ControlPoint.SET_REF_ATTENUATION, (Number)getJSpinnerRefAttenuation().getValue());	
		}
	}
}  //  @jve:decl-index=0:visual-constraint="25,7"
