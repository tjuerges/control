package alma.Control.device.gui.HoloRX ;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import alma.Control.gui.hardwaredevice.common.HardwareDeviceChessboardDetailsPlugin;
import alma.common.gui.chessboard.ChessboardDetailsPluginListener;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

/**
 * This is the main panel which ties together all of the information for the optical telescope GUI. 
 * It contains several other panels, which it arranges into a view with a top status portion (always
 * visible) and a bottom tabbed pane with tabs for several other views.
 * 
 * @author Nicolas Troncoso
 */
public class HoloRXDetailsPlugin extends HardwareDeviceChessboardDetailsPlugin
{
	private final static String HOLORX_PLUGIN_NAME = "Holography RX: ";
	private static final int STANDARD_INSET = 5;
	private String pluginTitle;
	private JTabbedPane tabbedPane = null;
	
	/**
	 * TODO
	 */
	private static final long serialVersionUID = 1L;

        /**
         * Constructor.
	 * @param antennaNames names of the antennas for which this plugin is
	 * being constructed.
	 * @param listener a <code>ChessboardDetailsPluginListener</code> which
	 * will be notified when this plugin closes.
	 * @param services container services for things like getComponent
	 * calls, etc.
	 * @param pluginTitle the unique title of the plugin (to be used in the
	 * title bar of the plugin window on screen) and by the docking
	 * framework of EXEC in order to give the plugin a unique name. 
	 * @param selectionTitle the title of the cell that was selected in the
	 * chessboard when this details plugin was launched; this may not be
	 * unique if, for example, multiple instances of a details display have
	 * been shown for a single cell in the chessboard; say, cell S1 might
	 * have selectionTitle of 'S1', pluginTitle values (if 3 instances of
	 * the details display have been shown) of: 'S1 - 1', 'S1 - 2', 'S1 -
	 * 3', etc.
         */
	public HoloRXDetailsPlugin(String antennaNames[], 
			ChessboardDetailsPluginListener listener,
			PluginContainerServices services, String pluginTitle, String selectionTitle) 
	{
		super(antennaNames, listener, services, pluginTitle, 
				selectionTitle, new HoloRXPresentationModel(antennaNames[0], services));
		
		this.pluginTitle = pluginTitle;
		initialize();
	}
	
	/**
	 * Required method of the SubsystemPlugin interface
	 */
	public boolean runRestricted(boolean restricted) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * Required method of the SubsystemPlugin interface
	 */
	public void specializedStart() {
	}

	@Override
	public void replaceContents(String[] newAntennaNames) 
	{
		assert (null != newAntennaNames && newAntennaNames.length > 0) : "newAntennaNames array malformed";
		this.presentationModel = new HoloRXPresentationModel(antennaNames[0], this.pluginContainerServices); 
	}

	@Override
	public String getPluginName() {
		return HOLORX_PLUGIN_NAME;
	}

	/**
	 * Getter for the title of the plugin, which comprises the name of the
	 * plugin + potentially some extra characters to make it unique. For
	 * example, "Holography DSP - 1"
	 *
	 * @return the pluginTitle
	 */
	public String getTitle() {
		return this.pluginTitle;
	}
		
	/**
	 * Private method to initialize the plugin.
	 */
	private void initialize()
	{
		this.setLayout(new GridBagLayout());
		JPanel mainPanel = new JPanel();
		mainPanel.setBorder(BorderFactory.createEmptyBorder(STANDARD_INSET, STANDARD_INSET, STANDARD_INSET, STANDARD_INSET));
 		tabbedPane = new JTabbedPane();
                tabbedPane.add(new HoloRXSettingsPanel(logger, (HoloRXPresentationModel)presentationModel), "Settings");
                //tabbedPane.add(new HoloRXTempGraphPanel(logger, (HoloRXPresentationModel)presentationModel), "Temp. Graphs");
//                tabbedPane.add(new HoloRXPLMPanel(logger, (HoloRXPresentationModel)presentationModel), "Path lenght measurement");
                //Dimension dimension = tabbedPane.getPreferredSize();
                //dimension.width = (int)Math.floor(((double)dimension.width * 1.25));
                //tabbedPane.setPreferredSize(dimension);
                GridBagConstraints constraints = new GridBagConstraints();
                constraints.gridx = 0;
                constraints.gridy = 0;
                constraints.weightx = 1.0;
                constraints.weighty = 1.0;
                constraints.anchor = GridBagConstraints.NORTHWEST;
                constraints.fill = GridBagConstraints.BOTH;
                mainPanel.add(tabbedPane, constraints);
                mainPanel.setMinimumSize(mainPanel.getPreferredSize());

      		this.add(mainPanel);
		this.requestFocus();
		this.validate();
	}
	
}
