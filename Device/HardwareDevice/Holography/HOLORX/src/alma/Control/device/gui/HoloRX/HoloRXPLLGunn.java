package alma.Control.device.gui.HoloRX;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.BorderFactory;
import javax.swing.SpinnerNumberModel;

import alma.Control.gui.hardwaredevice.common.HardwareDeviceAmbsiPanel;
import alma.Control.gui.hardwaredevice.common.HardwareDevicePresentationModelEvent;
import alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel;

import java.util.logging.Logger;

public class HoloRXPLLGunn extends HardwareDeviceAmbsiPanel {

	private static final long serialVersionUID = 1L;
	private JCheckBox jEnableControl = null;
	private JComboBox jComboBoxGunnSelect = null;
	private JCheckBox jCheckBoxDriveEnable = null;
	private JComboBox jComboBoxLoopEnable = null;
	private JSpinner jSpinnerNominalVoltage_low = null;
	private JLabel jLabelNominalVoltage_low = null;
	private JLabel jLabelNominalVoltage_high = null;
	private JSpinner jSpinnerNominalVoltage_high = null;
	private JLabel jLabelMonitoredLow = null;
	private JLabel jLabelMonitoredValueHigh = null;
	private JLabel jLabelGunnLoopGain = null;
	private JLabel jLabelGunnHLoopGain = null;
	private JLabel jLabelPPLTuneRangeL = null;
	private JLabel jLabelPPLTuneRangeH = null;
	private JSpinner jSpinnerGunnLoopGain_low = null;
	private JSpinner jSpinnerGunnLoopGain_high = null;
	private JSpinner jSpinnerGunnTuneRange_low = null;
	private JSpinner jSpinnerGunnTuneRange_high = null;
	private QuestionMarkLabel questionableLabelGunnLVoltage = null;
	private QuestionMarkLabel questionableLabelGunnHVoltage = null;

	private Logger logger = null;
	private HoloRXPresentationModel presentationModel;


	/**
	 * This is the default constructor
	 */
	public HoloRXPLLGunn(Logger logger, HoloRXPresentationModel presentationModel) {
		super(presentationModel);
		this.presentationModel = presentationModel;
		this.logger = logger;
		initialize();
	}

	/**
	 * This method initializes the PLL/GUNN panel
	 * 
	 * @return void
	 */
	private void initialize() {
		GridBagConstraints gridBagConstraints121 = new GridBagConstraints();
		gridBagConstraints121.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints121.gridy = 3;
		gridBagConstraints121.weightx = 1.0;
		gridBagConstraints121.gridx = 3;
		GridBagConstraints gridBagConstraints111 = new GridBagConstraints();
		gridBagConstraints111.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints111.gridy = 3;
		gridBagConstraints111.weightx = 1.0;
		gridBagConstraints111.gridx = 1;
		GridBagConstraints gridBagConstraints101 = new GridBagConstraints();
		gridBagConstraints101.gridx = 3;
		gridBagConstraints101.gridy = 5;
		GridBagConstraints gridBagConstraints16 = new GridBagConstraints();
		gridBagConstraints16.gridx = 1;
		gridBagConstraints16.gridy = 5;
		GridBagConstraints gridBagConstraints15 = new GridBagConstraints();
		gridBagConstraints15.gridx = 3;
		gridBagConstraints15.gridy = 4;
		GridBagConstraints gridBagConstraints14 = new GridBagConstraints();
		gridBagConstraints14.gridx = 1;
		gridBagConstraints14.gridy = 4;
		GridBagConstraints gridBagConstraints13 = new GridBagConstraints();
		gridBagConstraints13.gridx = 2;
		gridBagConstraints13.anchor = GridBagConstraints.EAST;
		gridBagConstraints13.gridy = 5;
		jLabelPPLTuneRangeH = new JLabel();
		jLabelPPLTuneRangeH.setText("PPL Tune Range H [0-50000 ohm]");
		GridBagConstraints gridBagConstraints12 = new GridBagConstraints();
		gridBagConstraints12.gridx = 0;
		gridBagConstraints12.anchor = GridBagConstraints.EAST;
		gridBagConstraints12.gridy = 5;
		jLabelPPLTuneRangeL = new JLabel();
		jLabelPPLTuneRangeL.setText("PPL Tune Range L [0-50000 ohm]");
		GridBagConstraints gridBagConstraints10 = new GridBagConstraints();
		gridBagConstraints10.gridx = 2;
		gridBagConstraints10.anchor = GridBagConstraints.EAST;
		gridBagConstraints10.gridy = 4;
		jLabelGunnHLoopGain = new JLabel();
		jLabelGunnHLoopGain.setText("GunnH Loop Gain [0-10 Kohm]");
		GridBagConstraints gridBagConstraints9 = new GridBagConstraints();
		gridBagConstraints9.gridx = 0;
		gridBagConstraints9.anchor = GridBagConstraints.EAST;
		gridBagConstraints9.gridy = 4;
		jLabelGunnLoopGain = new JLabel();
		jLabelGunnLoopGain.setText("GunnL Loop Gain [0-10 Kohm]");
		GridBagConstraints gridBagConstraints11 = new GridBagConstraints();
		gridBagConstraints11.gridx = 2;
		gridBagConstraints11.anchor = GridBagConstraints.EAST;
		gridBagConstraints11.gridy = 3;
		jLabelMonitoredValueHigh = new JLabel();
		jLabelMonitoredValueHigh.setText("Monitored Value");
		GridBagConstraints gridBagConstraints8 = new GridBagConstraints();
		gridBagConstraints8.gridx = 0;
		gridBagConstraints8.anchor = GridBagConstraints.EAST;
		gridBagConstraints8.gridy = 3;
		jLabelMonitoredLow = new JLabel();
		jLabelMonitoredLow.setText("Monitored Value [V]");
		GridBagConstraints gridBagConstraints7 = new GridBagConstraints();
		gridBagConstraints7.gridx = 3;
		gridBagConstraints7.gridy = 2;
		GridBagConstraints gridBagConstraints6 = new GridBagConstraints();
		gridBagConstraints6.gridx = 2;
		gridBagConstraints6.anchor = GridBagConstraints.EAST;
		gridBagConstraints6.gridy = 2;
		jLabelNominalVoltage_high = new JLabel();
		jLabelNominalVoltage_high.setText("High Gunn Bias [V]");
		GridBagConstraints gridBagConstraints5 = new GridBagConstraints();
		gridBagConstraints5.gridx = 0;
		gridBagConstraints5.anchor = GridBagConstraints.EAST;
		gridBagConstraints5.gridy = 2;
		jLabelNominalVoltage_low = new JLabel();
		jLabelNominalVoltage_low.setText("Low Gunn Bias [V]");
		GridBagConstraints gridBagConstraints4 = new GridBagConstraints();
		gridBagConstraints4.gridx = 1;
		gridBagConstraints4.gridy = 2;
		GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
		gridBagConstraints3.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints3.gridy = 1;
		gridBagConstraints3.weightx = 1.0;
		gridBagConstraints3.gridx = 3;
		GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
		gridBagConstraints2.gridx = 2;
		gridBagConstraints2.gridy = 1;
		GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
		gridBagConstraints1.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints1.gridy = 1;
		gridBagConstraints1.weightx = 1.0;
		gridBagConstraints1.gridx = 1;
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		this.setBorder(BorderFactory.createTitledBorder("PLL / Gunn"));
		this.setLayout(new GridBagLayout());
		this.add(getJEnableControl(), gridBagConstraints);
		this.add(getJComboBoxGunnSelect(), gridBagConstraints1);
		this.add(getJCheckBoxDriveEnable(), gridBagConstraints2);
		this.add(getJComboBoxLoopEnable(), gridBagConstraints3);
		this.add(getJSpinnerNominalVoltage_low(), gridBagConstraints4);
		this.add(jLabelNominalVoltage_low, gridBagConstraints5);
		this.add(jLabelNominalVoltage_high, gridBagConstraints6);
		this.add(getJSpinnerNominalVoltage_high(), gridBagConstraints7);
		this.add(jLabelMonitoredLow, gridBagConstraints8);
		this.add(jLabelMonitoredValueHigh, gridBagConstraints11);
		this.add(jLabelGunnLoopGain, gridBagConstraints9);
		this.add(jLabelGunnHLoopGain, gridBagConstraints10);
		this.add(jLabelPPLTuneRangeL, gridBagConstraints12);
		this.add(jLabelPPLTuneRangeH, gridBagConstraints13);
		this.add(getJSpinnerGunnLoopGain_low(), gridBagConstraints14);
		this.add(getJSpinnerGunnLoopGain_high(), gridBagConstraints15);
		this.add(getJSpinnerGunnTuneRange_low(), gridBagConstraints16);
		this.add(getJSpinnerGunnTuneRange_high(), gridBagConstraints101);
		this.add(getQuestionableLabelGunnLVoltage(), gridBagConstraints111);
		this.add(getQuestionableLabelGunnHVoltage(), gridBagConstraints121);
		disableControl();
	}

	@Override
	protected void specializedProcessChange(HardwareDevicePresentationModelEvent evt)        {

		HoloRXPresentationModel.MonitorPoint m = (HoloRXPresentationModel.MonitorPoint)evt.getEnumeration();
		switch(m){
			case GET_PLL_STATUS:
				/** Aparently the code generator is broken, so
				 * instead of getting a sequence of booleans we
				 * get an int. We have to breakit down acording
				 * to the ICD Document.
				 */
				updateDisplayPllStatus(((Integer)evt.getValue()).intValue());
				break;
			case GET_GUNN_H_VOLTAGE:
				getQuestionableLabelGunnHVoltage().setText(evt.getValue().toString());
				break;
			case GET_GUNN_L_VOLTAGE:
				getQuestionableLabelGunnLVoltage().setText(evt.getValue().toString());
				break;
			case GET_DRIVE_ENABLE:
				//The presentation model returns an int
				if((Integer)evt.getValue()==1)
					getJCheckBoxDriveEnable().setSelected(true);
				else
					getJCheckBoxDriveEnable().setSelected(false);
				break;
			case GET_LOOP_ENABLE:
				LoopEnable l = LoopEnable.getLoopEnable((Integer)evt.getValue());
				getJComboBoxLoopEnable().setSelectedItem(l);
				break;
			case GET_NOMINAL_VOLTAGE:
				/** The ambsi cannot monitor the high and low
				 * bands at the same time. Hence it will only
				 * return values for the selected band. For
				 * that reason we use this wraper that decided
				 * which display to update.
				 */
				updateDisplayNominalVoltage(((Double)evt.getValue()).floatValue());
				break;
			case GET_GUNN_LOOP_GAIN:
				/** The ambsi cannot monitor the high and low
				 * bands at the same time. Hence it will only
				 * return values for the selected band. For
				 * that reason we use this wraper that decided
				 * which display to update.
				 */
				updateDisplayGunnLoopGain(((Double)evt.getValue()).floatValue());
				break;
			case GET_GUNN_TUNE_RANGE:
				/** The ambsi cannot monitor the high and low
				 * bands at the same time. Hence it will only
				 * return values for the selected band. For
				 * that reason we use this wraper that decided
				 * which display to update.
				 */
				updateDisplayGunnTuneRange(((Double)evt.getValue()).floatValue());
				break;
			case GET_GUNN_SELECT:
				GunnSelect g = GunnSelect.getGunnSelect(((Integer)evt.getValue()).intValue());
				getJComboBoxGunnSelect().setSelectedItem(g);
				break;

		}
	}
	@Override
	protected void specializedCommunicationEstablished()        {
		getJEnableControl().setEnabled(true);
		if(getJEnableControl().isSelected()){
			getJComboBoxGunnSelect().setEnabled(true);
			getJCheckBoxDriveEnable().setEnabled(true);
			getJComboBoxLoopEnable().setEnabled(true);
		}
		getQuestionableLabelGunnLVoltage().setQuestionMarkShown(false);
		getQuestionableLabelGunnHVoltage().setQuestionMarkShown(false);
		comboGunnSelectChange();
	}

	private void enableControl(){
		getJEnableControl().setSelected(true);
		getJComboBoxGunnSelect().setEnabled(true);
		getJCheckBoxDriveEnable().setEnabled(true);
		getJComboBoxLoopEnable().setEnabled(true);
		comboGunnSelectChange();
	}
	private void disableControl(){
		getJEnableControl().setSelected(false);
		getJComboBoxGunnSelect().setEnabled(false);
		getJCheckBoxDriveEnable().setEnabled(false);
		getJComboBoxLoopEnable().setEnabled(false);
		getJSpinnerNominalVoltage_high().setEnabled(false);
		getJSpinnerGunnLoopGain_high().setEnabled(false);
		getJSpinnerGunnTuneRange_high().setEnabled(false);
		getQuestionableLabelGunnHVoltage().setEnabled(false);
		getJSpinnerGunnLoopGain_low().setEnabled(false);
		getJSpinnerNominalVoltage_low().setEnabled(false);
		getJSpinnerGunnTuneRange_low().setEnabled(false);
		getQuestionableLabelGunnLVoltage().setEnabled(false);
	}

	private void comboGunnSelectChange()
	{
		GunnSelect g =  (GunnSelect)getJComboBoxGunnSelect().getSelectedItem();
		/**
		 * :CAVEAT: if control is not enabled, return an don't activate
		 * the select part of the pannel.
		 */
		if(!getJEnableControl().isSelected())return;
		switch(g){
			case GUNN_SELECT_HIGH:
				getJSpinnerNominalVoltage_high().setEnabled(true);
				getJSpinnerGunnLoopGain_high().setEnabled(true);
				getJSpinnerGunnTuneRange_high().setEnabled(true);
				getQuestionableLabelGunnHVoltage().setEnabled(true);
				getJSpinnerGunnLoopGain_low().setEnabled(false);
				getJSpinnerNominalVoltage_low().setEnabled(false);
				getJSpinnerGunnTuneRange_low().setEnabled(false);
				getQuestionableLabelGunnLVoltage().setEnabled(false);
				break;
			case GUNN_SELECT_LOW:
				getJSpinnerNominalVoltage_high().setEnabled(false);
				getJSpinnerGunnLoopGain_high().setEnabled(false);
				getJSpinnerGunnTuneRange_high().setEnabled(false);
				getQuestionableLabelGunnHVoltage().setEnabled(false);
				getJSpinnerGunnLoopGain_low().setEnabled(true);
				getJSpinnerNominalVoltage_low().setEnabled(true);
				getJSpinnerGunnTuneRange_low().setEnabled(true);
				getQuestionableLabelGunnLVoltage().setEnabled(true);
				break;
		}



	}

	@Override
	protected void specializedCommunicationLost()        {
		getJEnableControl().setEnabled(false);
		getJComboBoxGunnSelect().setEnabled(false);
		getJCheckBoxDriveEnable().setEnabled(false);
		getJComboBoxLoopEnable().setEnabled(false);
		getJSpinnerNominalVoltage_low().setEnabled(false);
		getJSpinnerNominalVoltage_high().setEnabled(false);
		getJSpinnerGunnLoopGain_low().setEnabled(false);
		getJSpinnerGunnLoopGain_high().setEnabled(false);
		getJSpinnerGunnTuneRange_low().setEnabled(false);
		getJSpinnerGunnTuneRange_high().setEnabled(false);
		getQuestionableLabelGunnLVoltage().setQuestionMarkShown(true);
		getQuestionableLabelGunnHVoltage().setQuestionMarkShown(true);
	}

	private void updateDisplayPllStatus(int value){
	}

	/**
	 * This method initializes jEnableControl	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJEnableControl() {
		if (jEnableControl == null) {
			jEnableControl = new JCheckBox();
			jEnableControl.setText("Enable PPL/Gunn Control");
			jEnableControl.addActionListener(new EnableControlActionListener());
		}
		return jEnableControl;
	}

	public enum GunnSelect {
		GUNN_SELECT_HIGH (1,"High Band Gunn"),
		GUNN_SELECT_LOW  (0,"Low Band Gunn");
		private final int intValue;
		private final String name;
		GunnSelect(int intValue, String name){
			this.intValue = intValue;
			this.name = name;
		}
		public String toString() {return name;};
		public int intValue() {return intValue;};
		public static GunnSelect getGunnSelect(boolean value){return value==true?GunnSelect.GUNN_SELECT_HIGH:GunnSelect.GUNN_SELECT_LOW;}
		public static GunnSelect getGunnSelect(int value){return value==1?GunnSelect.GUNN_SELECT_HIGH:GunnSelect.GUNN_SELECT_LOW;}
	}

	/**
	 * This method initializes jComboBoxGunnSelect	
	 * 	
	 * @return javax.swing.JComboBox	
	 */
	private JComboBox getJComboBoxGunnSelect() {
		if (jComboBoxGunnSelect == null) {
			jComboBoxGunnSelect = new JComboBox();
			jComboBoxGunnSelect.addItem(GunnSelect.GUNN_SELECT_HIGH);
			jComboBoxGunnSelect.addItem(GunnSelect.GUNN_SELECT_LOW);
			jComboBoxGunnSelect.addActionListener(new GunnSelectActionListener());
			GunnSelect g = GunnSelect.getGunnSelect(presentationModel.getGunnSelect());
			jComboBoxGunnSelect.setSelectedItem(g);
		}
		return jComboBoxGunnSelect;
	}

	/**
	 * This method initializes jCheckBoxDriveEnable	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxDriveEnable() {
		if (jCheckBoxDriveEnable == null) {
			jCheckBoxDriveEnable = new JCheckBox();
			jCheckBoxDriveEnable.setText("Bias on/off");
			jCheckBoxDriveEnable.setSelected(presentationModel.isDriveEnable());
			jCheckBoxDriveEnable.addActionListener(new DriveEnableActionListener());
		}
		return jCheckBoxDriveEnable;
	}

	public enum LoopEnable {
		LOOP_ENABLE_CLOSE (1,"Closed Loop"),
		LOOP_ENABLE_OPEN  (0,"Open Loop");

		private final int intValue;
		private final String name;
		LoopEnable(int intValue, String name){
			this.intValue = intValue;
			this.name = name;
		}
		public int intValue() {return intValue;};
		public String toString() {return name;};
		public static LoopEnable getLoopEnable(boolean value){return value==true?LoopEnable.LOOP_ENABLE_CLOSE:LoopEnable.LOOP_ENABLE_OPEN;}
		public static LoopEnable getLoopEnable(int value){return value==1?LoopEnable.LOOP_ENABLE_CLOSE:LoopEnable.LOOP_ENABLE_OPEN;}
	}
	/**
	 * This method initializes jComboBoxLoopEnable	
	 * 	
	 * @return javax.swing.JComboBox	
	 */
	private JComboBox getJComboBoxLoopEnable() {
		if (jComboBoxLoopEnable == null) {
			jComboBoxLoopEnable = new JComboBox();
			jComboBoxLoopEnable.addItem(LoopEnable.LOOP_ENABLE_OPEN);
			jComboBoxLoopEnable.addItem(LoopEnable.LOOP_ENABLE_CLOSE);
			LoopEnable l = LoopEnable.getLoopEnable(presentationModel.isLoopEnable());                                
			jComboBoxLoopEnable.setSelectedItem(l);
			jComboBoxLoopEnable.addActionListener(new LoopEnableActionListener());
		}
		return jComboBoxLoopEnable;
	}
	/**
	 * This method updates the Gunn Bias voltage. I does so depending on
	 * which Gunn is selected.
	 * @param float 
	 */
	private void updateDisplayNominalVoltage(float value){
		GunnSelect g = (GunnSelect)getJComboBoxGunnSelect().getSelectedItem();
		switch(g){
			case GUNN_SELECT_HIGH:
				getJSpinnerNominalVoltage_high().setValue(value);
				break;
			case GUNN_SELECT_LOW:
				getJSpinnerNominalVoltage_low().setValue(value);
				break;
		}
	}

	/**
	 * This method initializes jSpinnerNominalVoltage_low	
	 * 	
	 * @return javax.swing.JSpinner	
	 */
	private JSpinner getJSpinnerNominalVoltage_low() {
		if (jSpinnerNominalVoltage_low == null) {
			jSpinnerNominalVoltage_low = new JSpinner(new SpinnerNumberModel(0, 0, 10, 0.001));
			jSpinnerNominalVoltage_low.setPreferredSize(new Dimension(100, 24));
			jSpinnerNominalVoltage_low.addChangeListener(new NominalVoltageChangeListener());
		}
		return jSpinnerNominalVoltage_low;
	}

	/**
	 * This method initializes jSpinnerNominalVoltage_high	
	 * 	
	 * @return javax.swing.JSpinner	
	 */
	private JSpinner getJSpinnerNominalVoltage_high() {
		if (jSpinnerNominalVoltage_high == null) {
			jSpinnerNominalVoltage_high = new JSpinner(new SpinnerNumberModel(0, 0, 10, 0.001));
			jSpinnerNominalVoltage_high.setPreferredSize(new Dimension(100, 24));
			jSpinnerNominalVoltage_high.addChangeListener(new NominalVoltageChangeListener());
		}
		return jSpinnerNominalVoltage_high;
	}

	/**
	 * This method updates the Gunn Loop Gain. It does so depending on
	 * which Gunn is selected.
	 * @param float 
	 */
	private void updateDisplayGunnLoopGain(float value){
		GunnSelect g = (GunnSelect)getJComboBoxGunnSelect().getSelectedItem();
		switch(g){
			case GUNN_SELECT_HIGH:
				getJSpinnerGunnLoopGain_high().setValue(value);
				break;
			case GUNN_SELECT_LOW:
				getJSpinnerGunnLoopGain_low().setValue(value);
				break;
		}
	}
	/**
	 * This method initializes jSpinnerGunnLoopGain_low	
	 * 	
	 * @return javax.swing.JSpinner	
	 */
	private JSpinner getJSpinnerGunnLoopGain_low() {
		if (jSpinnerGunnLoopGain_low == null) {
			jSpinnerGunnLoopGain_low = new JSpinner(new SpinnerNumberModel(0,0,10000,1));
			jSpinnerGunnLoopGain_low.setPreferredSize(new Dimension(100, 24));
			jSpinnerGunnLoopGain_low.addChangeListener(new GunnLoopGainChangeListener());
		}
		return jSpinnerGunnLoopGain_low;
	}

	/**
	 * This method initializes jSpinnerGunnLoopGain_high	
	 * 	
	 * @return javax.swing.JSpinner	
	 */
	private JSpinner getJSpinnerGunnLoopGain_high() {
		if (jSpinnerGunnLoopGain_high == null) {
			jSpinnerGunnLoopGain_high = new JSpinner(new SpinnerNumberModel(0,0,10000,1));
			jSpinnerGunnLoopGain_high.setPreferredSize(new Dimension(100, 24));
			jSpinnerGunnLoopGain_high.addChangeListener(new GunnLoopGainChangeListener());
		}
		return jSpinnerGunnLoopGain_high;
	}

	/**
	 * This method updates the Gunn Tune Range. It does so depending on
	 * which Gunn is selected.
	 * @param float 
	 */
	private void updateDisplayGunnTuneRange(float value){
		GunnSelect g = (GunnSelect)getJComboBoxGunnSelect().getSelectedItem();
		switch(g){
			case GUNN_SELECT_HIGH:
				getJSpinnerGunnTuneRange_high().setValue(value);
				break;
			case GUNN_SELECT_LOW:
				getJSpinnerGunnTuneRange_low().setValue(value);
				break;
		}
	}
	/**
	 * This method initializes jSpinnerGunnTuneRange_low	
	 * 	
	 * @return javax.swing.JSpinner	
	 */
	private JSpinner getJSpinnerGunnTuneRange_low() {
		if (jSpinnerGunnTuneRange_low == null) {
			jSpinnerGunnTuneRange_low = new JSpinner(new SpinnerNumberModel(0,0,50000,1));
			jSpinnerGunnTuneRange_low.setPreferredSize(new Dimension(100, 24));
			jSpinnerGunnTuneRange_low.addChangeListener(new GunnTuneRangeChangeListener());
		}
		return jSpinnerGunnTuneRange_low;
	}

	/**
	 * This method initializes jSpinnerGunnTuneRange_high	
	 * 	
	 * @return javax.swing.JSpinner	
	 */
	private JSpinner getJSpinnerGunnTuneRange_high() {
		if (jSpinnerGunnTuneRange_high == null) {
			jSpinnerGunnTuneRange_high = new JSpinner(new SpinnerNumberModel(0,0,50000,1));
			jSpinnerGunnTuneRange_high.setPreferredSize(new Dimension(100, 24));
			jSpinnerGunnTuneRange_high.addChangeListener(new GunnTuneRangeChangeListener());
		}
		return jSpinnerGunnTuneRange_high;
	}

	/**
	 * This method initializes questionableLabelGunnLVoltage	
	 * 	
	 * @return alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel	
	 */
	private QuestionMarkLabel getQuestionableLabelGunnLVoltage() {
		if (questionableLabelGunnLVoltage == null) {
			questionableLabelGunnLVoltage = new QuestionMarkLabel(!this.hasCommunication);
			questionableLabelGunnLVoltage.setPreferredSize(new Dimension(100, 22));
			questionableLabelGunnLVoltage.setHorizontalAlignment(JLabel.RIGHT);
		}
		return questionableLabelGunnLVoltage;
	}

	/**
	 * This method initializes questionableLabelGunnHVoltage	
	 * 	
	 * @return alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel	
	 */
	private QuestionMarkLabel getQuestionableLabelGunnHVoltage() {
		if (questionableLabelGunnHVoltage == null) {
			questionableLabelGunnHVoltage = new QuestionMarkLabel(!this.hasCommunication);
			questionableLabelGunnHVoltage.setPreferredSize(new Dimension(100, 22));
			questionableLabelGunnHVoltage.setHorizontalAlignment(JLabel.RIGHT);
		}
		return questionableLabelGunnHVoltage;
	}

	/**
	* Private Class EnableControlActionListener implements the action
	* for the Enable control checkbox change, and the selected GUNN
        * Band. 
	*/
	private class EnableControlActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if(getJEnableControl().isSelected())
				enableControl();
			else
				disableControl();
		}
	}
	private class DriveEnableActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			int sendValue;
			if(getJCheckBoxDriveEnable().isSelected())
				sendValue = 1;
			else
				sendValue = 0;
			presentationModel.applyStateChange(HoloRXPresentationModel.ControlPoint.SET_DRIVE_ENABLE, (Number)sendValue);

		}
	}
	private class GunnSelectActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			presentationModel.applyStateChange(HoloRXPresentationModel.ControlPoint.SET_GUNN_SELECT, (Number)((GunnSelect)getJComboBoxGunnSelect().getSelectedItem()).intValue());
		}
	}
	private class LoopEnableActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			presentationModel.applyStateChange(HoloRXPresentationModel.ControlPoint.SET_LOOP_ENABLE, (Number)((LoopEnable)getJComboBoxLoopEnable().getSelectedItem()).intValue());
		}
	}
	/**
	* Private class that holds the actions for the Reference Attenuation
	* spinner.
	*/
	private class NominalVoltageChangeListener implements ChangeListener
	{
		public void stateChanged(ChangeEvent e)
		{
			JSpinner spin = (JSpinner)e.getSource();
			presentationModel.applyStateChange(HoloRXPresentationModel.ControlPoint.SET_NOMINAL_VOLTAGE, (Number)spin.getValue());	
		}
	}
	/**
	* Private class that holds the actions for the Reference Attenuation
	* spinner.
	*/
	private class GunnLoopGainChangeListener implements ChangeListener
	{
		public void stateChanged(ChangeEvent e)
		{
			JSpinner spin = (JSpinner)e.getSource();
			presentationModel.applyStateChange(HoloRXPresentationModel.ControlPoint.SET_GUNN_LOOP_GAIN, (Number)spin.getValue());	
		}
	}
	/**
	* Private class that holds the actions for the Reference Attenuation
	* spinner.
	*/
	private class GunnTuneRangeChangeListener implements ChangeListener
	{
		public void stateChanged(ChangeEvent e)
		{
			JSpinner spin = (JSpinner)e.getSource();
			presentationModel.applyStateChange(HoloRXPresentationModel.ControlPoint.SET_GUNN_TUNE_RANGE, (Number)spin.getValue());	
		}
	}

}  //  @jve:decl-index=0:visual-constraint="13,17"
