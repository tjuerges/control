package alma.Control.device.gui.HoloRX;

import java.awt.GridBagLayout;
import javax.swing.JPanel;
import java.awt.Dimension;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JSpinner;
import javax.swing.JCheckBox;
import alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel;
import javax.swing.BorderFactory;
import javax.swing.SpinnerNumberModel;

import alma.Control.gui.hardwaredevice.common.HardwareDeviceAmbsiPanel;
import alma.Control.gui.hardwaredevice.common.HardwareDevicePresentationModelEvent;

import java.util.logging.Logger;

public class HoloRXSignalLevel extends HardwareDeviceAmbsiPanel {

	private static final long serialVersionUID = 1L;
	private JLabel jLabelUpdateInterval = null;
	private JSpinner jSpinnerUpdateInterval = null;
	private JCheckBox jCheckBoxEnableReadout = null;
	private JLabel jLabelLoDetOut = null;
	private QuestionMarkLabel questionableLabelLoDetOut = null;
	private JLabel jLabelRefDetOut = null;
	private JCheckBox jCheckBoxLO = null;
	private QuestionMarkLabel questionableLabelRefSenseI = null;
	private JCheckBox jCheckBoxREFI = null;
	private JLabel jLabelRefDetOutF = null;
	private QuestionMarkLabel questionableLabelRefDetOut = null;
	private JCheckBox jCheckBoxREFIF = null;
	private JLabel jLabelREFQ = null;
	private QuestionMarkLabel questionableLabelRefSenseQ = null;
	private JCheckBox jCheckBoxREFQ = null;
	private JLabel jLabelSigDetOut = null;
	private QuestionMarkLabel questionableLabelSigDetOut = null;
	private JCheckBox jCheckBoxSIGIF = null;
	private JLabel jLabelSIGI = null;
	private QuestionMarkLabel questionableLabelSigSenseI = null;
	private JCheckBox jCheckBoxSIGI = null;
	private JLabel jLabelSIGQ = null;
	private QuestionMarkLabel questionableLabelSigSenseQ = null;
	private JCheckBox jCheckBoxSIGQ = null;

	private Logger logger = null;
        private HoloRXPresentationModel presentationModel;
	/**
	 * This is the default constructor
	 */
	public HoloRXSignalLevel(Logger logger, HoloRXPresentationModel presentationModel) {
		super(presentationModel);
		this.presentationModel = presentationModel;
		this.logger = logger;
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		GridBagConstraints gridBagConstraints23 = new GridBagConstraints();
		gridBagConstraints23.gridx = 5;
		gridBagConstraints23.gridy = 5;
		GridBagConstraints gridBagConstraints22 = new GridBagConstraints();
		gridBagConstraints22.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints22.gridy = 5;
		gridBagConstraints22.weightx = 1.0;
		gridBagConstraints22.gridx = 4;
		GridBagConstraints gridBagConstraints21 = new GridBagConstraints();
		gridBagConstraints21.gridx = 3;
		gridBagConstraints21.anchor = GridBagConstraints.EAST;
		gridBagConstraints21.gridy = 5;
		jLabelSIGQ = new JLabel();
		jLabelSIGQ.setText(" SIG Q channel [Vp-p]");
		GridBagConstraints gridBagConstraints20 = new GridBagConstraints();
		gridBagConstraints20.gridx = 5;
		gridBagConstraints20.gridy = 4;
		GridBagConstraints gridBagConstraints19 = new GridBagConstraints();
		gridBagConstraints19.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints19.gridy = 4;
		gridBagConstraints19.weightx = 1.0;
		gridBagConstraints19.gridx = 4;
		GridBagConstraints gridBagConstraints18 = new GridBagConstraints();
		gridBagConstraints18.gridx = 3;
		gridBagConstraints18.anchor = GridBagConstraints.EAST;
		gridBagConstraints18.gridy = 4;
		jLabelSIGI = new JLabel();
		jLabelSIGI.setText(" SIG I channel [Vp-p]");
		GridBagConstraints gridBagConstraints17 = new GridBagConstraints();
		gridBagConstraints17.gridx = 2;
		gridBagConstraints17.gridy = 4;
		GridBagConstraints gridBagConstraints16 = new GridBagConstraints();
		gridBagConstraints16.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints16.gridy = 4;
		gridBagConstraints16.weightx = 1.0;
		gridBagConstraints16.gridx = 1;
		GridBagConstraints gridBagConstraints15 = new GridBagConstraints();
		gridBagConstraints15.gridx = 0;
		gridBagConstraints15.anchor = GridBagConstraints.EAST;
		gridBagConstraints15.gridy = 4;
		GridBagConstraints gridBagConstraints14 = new GridBagConstraints();
		gridBagConstraints14.gridx = 5;
		gridBagConstraints14.gridy = 3;
		GridBagConstraints gridBagConstraints13 = new GridBagConstraints();
		gridBagConstraints13.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints13.gridy = 3;
		gridBagConstraints13.weightx = 1.0;
		gridBagConstraints13.gridx = 4;
		GridBagConstraints gridBagConstraints12 = new GridBagConstraints();
		gridBagConstraints12.gridx = 3;
		gridBagConstraints12.anchor = GridBagConstraints.EAST;
		gridBagConstraints12.gridy = 3;
		GridBagConstraints gridBagConstraints11 = new GridBagConstraints();
		gridBagConstraints11.gridx = 2;
		gridBagConstraints11.gridy = 3;
		GridBagConstraints gridBagConstraints10 = new GridBagConstraints();
		gridBagConstraints10.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints10.gridy = 3;
		gridBagConstraints10.weightx = 1.0;
		gridBagConstraints10.gridx = 1;
		GridBagConstraints gridBagConstraints9 = new GridBagConstraints();
		gridBagConstraints9.gridx = 0;
		gridBagConstraints9.anchor = GridBagConstraints.EAST;
		gridBagConstraints9.gridy = 3;
		GridBagConstraints gridBagConstraints8 = new GridBagConstraints();
		gridBagConstraints8.gridx = 5;
		gridBagConstraints8.gridy = 2;
		GridBagConstraints gridBagConstraints7 = new GridBagConstraints();
		gridBagConstraints7.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints7.gridy = 2;
		gridBagConstraints7.weightx = 1.0;
		gridBagConstraints7.gridx = 4;
		GridBagConstraints gridBagConstraints6 = new GridBagConstraints();
		gridBagConstraints6.gridx = 2;
		gridBagConstraints6.gridy = 2;
		GridBagConstraints gridBagConstraints5 = new GridBagConstraints();
		gridBagConstraints5.gridx = 3;
		gridBagConstraints5.anchor = GridBagConstraints.EAST;
		gridBagConstraints5.gridy = 2;
		GridBagConstraints gridBagConstraints4 = new GridBagConstraints();
		gridBagConstraints4.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints4.gridy = 2;
		gridBagConstraints4.weightx = 1.0;
		gridBagConstraints4.gridx = 1;
		GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
		gridBagConstraints3.gridx = 0;
		gridBagConstraints3.anchor = GridBagConstraints.EAST;
		gridBagConstraints3.gridy = 2;
		GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
		gridBagConstraints2.gridx = 3;
		gridBagConstraints2.gridy = 0;
		GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
		gridBagConstraints1.gridx = 1;
		gridBagConstraints1.gridy = 0;
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;

		jLabelUpdateInterval = new JLabel();
		jLabelUpdateInterval.setText("Update interval [ms]");
		jLabelLoDetOut = new JLabel();
		jLabelLoDetOut.setText("LO Level [V]");
		jLabelRefDetOut = new JLabel();
		jLabelRefDetOut.setText(" REF I channel [Vp-p]");
		jLabelRefDetOutF = new JLabel();
		jLabelRefDetOutF.setText(" REF IF level [dBm]");
		jLabelREFQ = new JLabel();
		jLabelREFQ.setText(" REF Q channel [Vp-p]");
		jLabelSigDetOut = new JLabel();
		jLabelSigDetOut.setText("Signal IF level [dBm]");
		//this.setSize(676, 229);
		this.setBorder(BorderFactory.createTitledBorder("Signal Levels"));
		this.setLayout(new GridBagLayout());
//		this.add(jLabelUpdateInterval, gridBagConstraints);
		//this.add(getJSpinnerUpdateInterval(), gridBagConstraints1);
		//this.add(getJCheckBoxEnableReadout(), gridBagConstraints2);
		this.add(jLabelLoDetOut, gridBagConstraints3);
		this.add(getQuestionableLabelLoDetOut(), gridBagConstraints4);
		this.add(jLabelRefDetOut, gridBagConstraints5);
		//this.add(getJCheckBoxLO(), gridBagConstraints6);
		this.add(getQuestionableLabelRefSenseI(), gridBagConstraints7);
		//this.add(getJCheckBoxREFI(), gridBagConstraints8);
		this.add(jLabelRefDetOutF, gridBagConstraints9);
		this.add(getQuestionableLabelRefDetOut(), gridBagConstraints10);
		//this.add(getJCheckBoxREFIF(), gridBagConstraints11);
		this.add(jLabelREFQ, gridBagConstraints12);
		this.add(getQuestionableLabelRefSenseQ(), gridBagConstraints13);
		//this.add(getJCheckBoxREFQ(), gridBagConstraints14);
		this.add(jLabelSigDetOut, gridBagConstraints15);
		this.add(getQuestionableLabelSigDetOut(), gridBagConstraints16);
		//this.add(getJCheckBoxSIGIF(), gridBagConstraints17);
		this.add(jLabelSIGI, gridBagConstraints18);
		this.add(getQuestionableLabelSigSenseI(), gridBagConstraints19);
		//this.add(getJCheckBoxSIGI(), gridBagConstraints20);
		this.add(jLabelSIGQ, gridBagConstraints21);
		this.add(getQuestionableLabelSigSenseQ(), gridBagConstraints22);
		//this.add(getJCheckBoxSIGQ(), gridBagConstraints23);
	}

	@Override
	protected void specializedProcessChange(HardwareDevicePresentationModelEvent evt)        {
		HoloRXPresentationModel.MonitorPoint m = (HoloRXPresentationModel.MonitorPoint)evt.getEnumeration();
		switch(m){

			case GET_LO_DET_OUT:
				getQuestionableLabelLoDetOut().setText(evt.getValue().toString());
				break;
			case GET_REF_DET_OUT:
				getQuestionableLabelRefDetOut().setText(evt.getValue().toString());
				break;
			case GET_SIG_DET_OUT:
				getQuestionableLabelSigDetOut().setText(evt.getValue().toString());
				break;
			case GET_REF_SENSE_I:
				getQuestionableLabelRefSenseI().setText(evt.getValue().toString());
				break;
			case GET_REF_SENSE_Q:
				getQuestionableLabelRefSenseQ().setText(evt.getValue().toString());
				break;
			case GET_SIG_SENSE_I:
				getQuestionableLabelSigSenseI().setText(evt.getValue().toString());
				break;
			case GET_SIG_SENSE_Q:
				getQuestionableLabelSigSenseQ().setText(evt.getValue().toString());
				break;
		}
	}

        @Override
        protected void specializedCommunicationEstablished()        {
		getJSpinnerUpdateInterval().setEnabled(true);
		getJCheckBoxEnableReadout().setEnabled(true);
		getJCheckBoxLO().setEnabled(true);
		getJCheckBoxREFI().setEnabled(true);
		getJCheckBoxREFIF().setEnabled(true);
		getJCheckBoxREFQ().setEnabled(true);
		getJCheckBoxSIGIF().setEnabled(true);
		getJCheckBoxSIGI().setEnabled(true);
		getQuestionableLabelLoDetOut().setQuestionMarkShown(false);
		getQuestionableLabelRefSenseI().setQuestionMarkShown(false);
		getQuestionableLabelRefDetOut().setQuestionMarkShown(false);
		getQuestionableLabelRefSenseQ().setQuestionMarkShown(false);
		getQuestionableLabelSigDetOut().setQuestionMarkShown(false);
		getQuestionableLabelSigSenseI().setQuestionMarkShown(false);
		getQuestionableLabelSigSenseQ().setQuestionMarkShown(false);
         }
        @Override
        protected void specializedCommunicationLost()        {
		getJSpinnerUpdateInterval().setEnabled(false);
		getJCheckBoxEnableReadout().setEnabled(false);
		getJCheckBoxLO().setEnabled(false);
		getJCheckBoxREFI().setEnabled(false);
		getJCheckBoxREFIF().setEnabled(false);
		getJCheckBoxREFQ().setEnabled(false);
		getJCheckBoxSIGIF().setEnabled(false);
		getJCheckBoxSIGI().setEnabled(false);
		getQuestionableLabelLoDetOut().setQuestionMarkShown(true);
		getQuestionableLabelRefSenseI().setQuestionMarkShown(true);
		getQuestionableLabelRefDetOut().setQuestionMarkShown(true);
		getQuestionableLabelRefSenseQ().setQuestionMarkShown(true);
		getQuestionableLabelSigDetOut().setQuestionMarkShown(true);
		getQuestionableLabelSigSenseI().setQuestionMarkShown(true);
		getQuestionableLabelSigSenseQ().setQuestionMarkShown(true);
	
         }

	/**
	 * This method initializes jSpinnerUpdateInterval	
	 * 	
	 * @return javax.swing.JSpinner	
	 */
	private JSpinner getJSpinnerUpdateInterval() {
		if (jSpinnerUpdateInterval == null) {
			jSpinnerUpdateInterval = new JSpinner(new SpinnerNumberModel(48,48,30000,10));
			jSpinnerUpdateInterval.setPreferredSize(new Dimension(50, 24));

		}
		return jSpinnerUpdateInterval;
	}

	/**
	 * This method initializes jCheckBoxEnableReadout	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxEnableReadout() {
		if (jCheckBoxEnableReadout == null) {
			jCheckBoxEnableReadout = new JCheckBox();
			jCheckBoxEnableReadout.setText("Enable Status readout");
		}
		return jCheckBoxEnableReadout;
	}

	/**
	 * This method initializes questionableLabelLoDetOut	
	 * 	
	 * @return alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel	
	 */
	private QuestionMarkLabel getQuestionableLabelLoDetOut() {
		if (questionableLabelLoDetOut == null) {
			questionableLabelLoDetOut = new QuestionMarkLabel(!this.hasCommunication);
			questionableLabelLoDetOut.setPreferredSize(new Dimension(60, 22));
			questionableLabelLoDetOut.setHorizontalAlignment(JLabel.RIGHT);
		}
		return questionableLabelLoDetOut;
	}

	/**
	 * This method initializes jCheckBoxLO	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxLO() {
		if (jCheckBoxLO == null) {
			jCheckBoxLO = new JCheckBox();
		}
		return jCheckBoxLO;
	}

	/**
	 * This method initializes questionableLabelRefSenseI	
	 * 	
	 * @return alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel	
	 */
	private QuestionMarkLabel getQuestionableLabelRefSenseI() {
		if (questionableLabelRefSenseI == null) {
			questionableLabelRefSenseI = new QuestionMarkLabel(!this.hasCommunication);
			questionableLabelRefSenseI.setPreferredSize(new Dimension(60, 22));
			questionableLabelRefSenseI.setHorizontalAlignment(JLabel.RIGHT);
		}
		return questionableLabelRefSenseI;
	}

	/**
	 * This method initializes jCheckBoxREFI	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxREFI() {
		if (jCheckBoxREFI == null) {
			jCheckBoxREFI = new JCheckBox();
		}
		return jCheckBoxREFI;
	}

	/**
	 * This method initializes questionableLabelRefDetOut	
	 * 	
	 * @return alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel	
	 */
	private QuestionMarkLabel getQuestionableLabelRefDetOut() {
		if (questionableLabelRefDetOut == null) {
			questionableLabelRefDetOut = new QuestionMarkLabel(!this.hasCommunication);
			questionableLabelRefDetOut.setPreferredSize(new Dimension(60, 22));
			questionableLabelRefDetOut.setHorizontalAlignment(JLabel.RIGHT);
		}
		return questionableLabelRefDetOut;
	}

	/**
	 * This method initializes jCheckBoxREFIF	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxREFIF() {
		if (jCheckBoxREFIF == null) {
			jCheckBoxREFIF = new JCheckBox();
		}
		return jCheckBoxREFIF;
	}

	/**
	 * This method initializes questionableLabelRefSenseQ	
	 * 	
	 * @return alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel	
	 */
	private QuestionMarkLabel getQuestionableLabelRefSenseQ() {
		if (questionableLabelRefSenseQ == null) {
			questionableLabelRefSenseQ = new QuestionMarkLabel(!this.hasCommunication);
			questionableLabelRefSenseQ.setPreferredSize(new Dimension(60, 22));
			questionableLabelRefSenseQ.setHorizontalAlignment(JLabel.RIGHT);
		}
		return questionableLabelRefSenseQ;
	}

	/**
	 * This method initializes jCheckBoxREFQ	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxREFQ() {
		if (jCheckBoxREFQ == null) {
			jCheckBoxREFQ = new JCheckBox();
		}
		return jCheckBoxREFQ;
	}

	/**
	 * This method initializes questionableLabelSigDetOut	
	 * 	
	 * @return alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel	
	 */
	private QuestionMarkLabel getQuestionableLabelSigDetOut() {
		if (questionableLabelSigDetOut == null) {
			questionableLabelSigDetOut = new QuestionMarkLabel(!this.hasCommunication);
			questionableLabelSigDetOut.setPreferredSize(new Dimension(60, 22));
			questionableLabelSigDetOut.setHorizontalAlignment(JLabel.RIGHT);
		}
		return questionableLabelSigDetOut;
	}

	/**
	 * This method initializes jCheckBoxSIGIF	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxSIGIF() {
		if (jCheckBoxSIGIF == null) {
			jCheckBoxSIGIF = new JCheckBox();
		}
		return jCheckBoxSIGIF;
	}

	/**
	 * This method initializes questionableLabelSigSenseI	
	 * 	
	 * @return alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel	
	 */
	private QuestionMarkLabel getQuestionableLabelSigSenseI() {
		if (questionableLabelSigSenseI == null) {
			questionableLabelSigSenseI = new QuestionMarkLabel(!this.hasCommunication);
			questionableLabelSigSenseI.setPreferredSize(new Dimension(60, 22));
			questionableLabelSigSenseI.setHorizontalAlignment(JLabel.RIGHT);
		}
		return questionableLabelSigSenseI;
	}

	/**
	 * This method initializes jCheckBoxSIGI	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxSIGI() {
		if (jCheckBoxSIGI == null) {
			jCheckBoxSIGI = new JCheckBox();
		}
		return jCheckBoxSIGI;
	}

	/**
	 * This method initializes questionableLabelSigSenseQ	
	 * 	
	 * @return alma.Control.gui.hardwaredevice.common.util.QuestionMarkLabel	
	 */
	private QuestionMarkLabel getQuestionableLabelSigSenseQ() {
		if (questionableLabelSigSenseQ == null) {
			questionableLabelSigSenseQ = new QuestionMarkLabel(!this.hasCommunication);
			questionableLabelSigSenseQ.setPreferredSize(new Dimension(60, 22));
			questionableLabelSigSenseQ.setHorizontalAlignment(JLabel.RIGHT);
		}
		return questionableLabelSigSenseQ;
	}

	/**
	 * This method initializes jCheckBoxSIGQ	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBoxSIGQ() {
		if (jCheckBoxSIGQ == null) {
			jCheckBoxSIGQ = new JCheckBox();
		}
		return jCheckBoxSIGQ;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
