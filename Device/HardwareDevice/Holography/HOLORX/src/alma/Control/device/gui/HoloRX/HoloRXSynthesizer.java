package alma.Control.device.gui.HoloRX;

import java.awt.GridBagConstraints;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JPanel;
import javax.swing.JCheckBox;
import javax.swing.SpinnerNumberModel;
import javax.swing.BorderFactory;

import alma.Control.gui.hardwaredevice.common.HardwareDeviceAmbsiPanel;
import alma.Control.gui.hardwaredevice.common.HardwareDevicePresentationModelEvent;

import java.util.logging.Logger;

public class HoloRXSynthesizer extends HardwareDeviceAmbsiPanel {

	private static final long serialVersionUID = 1L;
	private JCheckBox jEnableControl = null;
	private JLabel jLabelSpinner = null;
	private JSpinner jSpinnerSynthFrequency = null;

	private Logger logger = null;
        private HoloRXPresentationModel presentationModel;

	/**
	 * This is the default constructor
	 */
	public HoloRXSynthesizer(Logger logger, HoloRXPresentationModel presentationModel) {
		super(presentationModel);
		this.presentationModel = presentationModel;
		this.logger = logger;
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
		gridBagConstraints2.gridx = 1;
		gridBagConstraints2.gridy = 1;
		GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
		gridBagConstraints1.gridx = 0;
		gridBagConstraints1.gridy = 1;
		jLabelSpinner = new JLabel();
		jLabelSpinner.setText("Synthesizer freq [Mhz]");
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		//this.setSize(500, 200);
		this.setBorder(BorderFactory.createTitledBorder("Synthesizer"));	
		this.setLayout(new GridBagLayout());
		this.add(getJEnableControl(), gridBagConstraints);
		this.add(jLabelSpinner, gridBagConstraints1);
		this.add(getJSpinnerSynthFrequency(), gridBagConstraints2);
		disableControl();
	}

        @Override
	protected void specializedProcessChange(HardwareDevicePresentationModelEvent evt)        {
		HoloRXPresentationModel.MonitorPoint m = (HoloRXPresentationModel.MonitorPoint)evt.getEnumeration();
		switch(m){
			case GET_SYNTH_FREQUENCY:
				getJSpinnerSynthFrequency().setValue(((Double)evt.getValue()).floatValue());
				break;
		}
	}

	private void enableControl()
	{
		getJEnableControl().setSelected(true);
		getJSpinnerSynthFrequency().setEnabled(true);
	}
	private void disableControl()
	{
		getJEnableControl().setSelected(false);
		getJSpinnerSynthFrequency().setEnabled(false);
	}
				

        @Override
        protected void specializedCommunicationEstablished()        {
		getJEnableControl().setEnabled(true);
		if(getJEnableControl().isSelected())
			getJSpinnerSynthFrequency().setEnabled(true);
         }
        @Override
        protected void specializedCommunicationLost()        {
		getJEnableControl().setEnabled(false);
		getJSpinnerSynthFrequency().setEnabled(false);
         }

	/**
	 * This method initializes jEnableControl	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJEnableControl() {
		if (jEnableControl == null) {
			jEnableControl = new JCheckBox();
			jEnableControl.setText("Enable Synth. Control");
			jEnableControl.addActionListener(new EnableControlActionListener());
		}
		return jEnableControl;
	}

	/**
	 * This method initializes jSpinnerSynthFrequency	
	 * 	
	 * @return javax.swing.JSpinner
	 */
	private JSpinner getJSpinnerSynthFrequency() {
		if (jSpinnerSynthFrequency == null) {
			jSpinnerSynthFrequency = new JSpinner(new SpinnerNumberModel(9445.000, 8730.000, 9475.000, 0.125));
			jSpinnerSynthFrequency.setPreferredSize(new Dimension(100, 24));
			jSpinnerSynthFrequency.addChangeListener(new SetSynthFrequencyChangeListener());
		}
		return jSpinnerSynthFrequency;
	}
	private class EnableControlActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if(getJEnableControl().isSelected())
				enableControl();
			else
				disableControl();
					
		}
	}

	private class SetSynthFrequencyChangeListener implements ChangeListener
	{
		public void stateChanged(ChangeEvent e)
		{
			presentationModel.applyStateChange(HoloRXPresentationModel.ControlPoint.SET_SYNTH_FREQUENCY, (Number)getJSpinnerSynthFrequency().getValue());
		}
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
