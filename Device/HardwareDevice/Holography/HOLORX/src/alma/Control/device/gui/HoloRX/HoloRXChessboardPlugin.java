package alma.Control.device.gui.HoloRX;

import alma.Control.gui.hardwaredevice.common.HardwareDeviceChessboardPresentationModel;
import alma.common.gui.chessboard.ChessboardDetailsPluginFactory;
import alma.common.gui.chessboard.ChessboardPlugin;
import alma.common.gui.chessboard.ChessboardStatusProvider;

@SuppressWarnings("serial")
public class HoloRXChessboardPlugin extends ChessboardPlugin {
        /**
         * TODO - serialVersionUID
         */
        private static final long serialVersionUID = 1L;

        /**
         * No-args constructor required by Exec's plugin framework.
         */
        public HoloRXChessboardPlugin()
        {
        }

        @Override
        protected ChessboardDetailsPluginFactory getDetailsProvider() {
                detailsProvider = new HoloRXChessboardDetailsPluginFactory();
                return detailsProvider;
        }

        @Override
        protected ChessboardStatusProvider getStatusProvider() {
                statusProvider = new HardwareDeviceChessboardPresentationModel(pluginContainerServices,
                                HoloRXPresentationModel.HOLORX_DEVICE_STRING);
                return statusProvider;
        }
}

